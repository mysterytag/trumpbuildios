﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Tacticsoft.TableView
struct TableView_t692333993;
// Tacticsoft.ITableViewDataSource
struct ITableViewDataSource_t1629295493;
// Tacticsoft.TableViewCell
struct TableViewCell_t776419755;
// System.String
struct String_t;
// System.Collections.IEnumerator
struct IEnumerator_t287207039;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String968488902.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_Range938821841.h"
#include "UnityEngine_UnityEngine_Vector23525329788.h"
#include "AssemblyU2DCSharp_Tacticsoft_TableViewCell776419755.h"

// System.Void Tacticsoft.TableView::.ctor()
extern "C"  void TableView__ctor_m744169588 (TableView_t692333993 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Tacticsoft.ITableViewDataSource Tacticsoft.TableView::get_dataSource()
extern "C"  Il2CppObject * TableView_get_dataSource_m3752224790 (TableView_t692333993 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Tacticsoft.TableView::set_dataSource(Tacticsoft.ITableViewDataSource)
extern "C"  void TableView_set_dataSource_m3713631493 (TableView_t692333993 * __this, Il2CppObject * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Tacticsoft.TableViewCell Tacticsoft.TableView::GetReusableCell(System.String)
extern "C"  TableViewCell_t776419755 * TableView_GetReusableCell_m1366329345 (TableView_t692333993 * __this, String_t* ___reuseIdentifier0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Tacticsoft.TableView::get_isEmpty()
extern "C"  bool TableView_get_isEmpty_m3044629912 (TableView_t692333993 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Tacticsoft.TableView::set_isEmpty(System.Boolean)
extern "C"  void TableView_set_isEmpty_m2611253839 (TableView_t692333993 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Tacticsoft.TableView::ReloadData()
extern "C"  void TableView_ReloadData_m94763827 (TableView_t692333993 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Tacticsoft.TableViewCell Tacticsoft.TableView::GetCellAtRow(System.Int32)
extern "C"  TableViewCell_t776419755 * TableView_GetCellAtRow_m3236843492 (TableView_t692333993 * __this, int32_t ___row0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.SocialPlatforms.Range Tacticsoft.TableView::get_visibleRowRange()
extern "C"  Range_t938821841  TableView_get_visibleRowRange_m676010102 (TableView_t692333993 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Tacticsoft.TableView::NotifyCellDimensionsChanged(System.Int32)
extern "C"  void TableView_NotifyCellDimensionsChanged_m4204641599 (TableView_t692333993 * __this, int32_t ___row0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single Tacticsoft.TableView::get_scrollableHeight()
extern "C"  float TableView_get_scrollableHeight_m4034525363 (TableView_t692333993 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single Tacticsoft.TableView::get_scrollY()
extern "C"  float TableView_get_scrollY_m1182917417 (TableView_t692333993 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Tacticsoft.TableView::set_scrollY(System.Single)
extern "C"  void TableView_set_scrollY_m2678675914 (TableView_t692333993 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single Tacticsoft.TableView::GetScrollYForRow(System.Int32,System.Boolean)
extern "C"  float TableView_GetScrollYForRow_m519216745 (TableView_t692333993 * __this, int32_t ___row0, bool ___above1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Tacticsoft.TableView::ScrollViewValueChanged(UnityEngine.Vector2)
extern "C"  void TableView_ScrollViewValueChanged_m3236431893 (TableView_t692333993 * __this, Vector2_t3525329788  ___newScrollValue0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Tacticsoft.TableView::RecalculateVisibleRowsFromScratch()
extern "C"  void TableView_RecalculateVisibleRowsFromScratch_m3061360650 (TableView_t692333993 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Tacticsoft.TableView::ClearAllRows()
extern "C"  void TableView_ClearAllRows_m1590392861 (TableView_t692333993 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Tacticsoft.TableView::Awake()
extern "C"  void TableView_Awake_m981774807 (TableView_t692333993 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Tacticsoft.TableView::Start()
extern "C"  void TableView_Start_m3986274676 (TableView_t692333993 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Tacticsoft.TableView::Update()
extern "C"  void TableView_Update_m3321282841 (TableView_t692333993 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Tacticsoft.TableView::LateUpdate()
extern "C"  void TableView_LateUpdate_m778064479 (TableView_t692333993 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Tacticsoft.TableView::OnEnable()
extern "C"  void TableView_OnEnable_m1006592658 (TableView_t692333993 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Tacticsoft.TableView::OnDisable()
extern "C"  void TableView_OnDisable_m1580538587 (TableView_t692333993 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.SocialPlatforms.Range Tacticsoft.TableView::CalculateCurrentVisibleRowRange()
extern "C"  Range_t938821841  TableView_CalculateCurrentVisibleRowRange_m472595596 (TableView_t692333993 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Tacticsoft.TableView::SetInitialVisibleRows()
extern "C"  void TableView_SetInitialVisibleRows_m3572933851 (TableView_t692333993 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator Tacticsoft.TableView::AsyncLoad(UnityEngine.SocialPlatforms.Range)
extern "C"  Il2CppObject * TableView_AsyncLoad_m83026235 (TableView_t692333993 * __this, Range_t938821841  ___visibleRows0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Tacticsoft.TableView::FindElementPosition()
extern "C"  void TableView_FindElementPosition_m238512670 (TableView_t692333993 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Tacticsoft.TableView::AddRow(System.Int32,System.Boolean)
extern "C"  void TableView_AddRow_m4155754147 (TableView_t692333993 * __this, int32_t ___row0, bool ___atEnd1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Tacticsoft.TableView::RefreshVisibleRows()
extern "C"  void TableView_RefreshVisibleRows_m1221312416 (TableView_t692333993 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Tacticsoft.TableView::HideRow(System.Boolean,System.Boolean)
extern "C"  void TableView_HideRow_m610511068 (TableView_t692333993 * __this, bool ___last0, bool ___clear1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Tacticsoft.TableView::FindIndexOfRowAtY(System.Single)
extern "C"  int32_t TableView_FindIndexOfRowAtY_m1786990123 (TableView_t692333993 * __this, float ___y0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Tacticsoft.TableView::FindIndexOfRowAtY(System.Single,System.Int32,System.Int32)
extern "C"  int32_t TableView_FindIndexOfRowAtY_m3031266251 (TableView_t692333993 * __this, float ___y0, int32_t ___startIndex1, int32_t ___endIndex2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single Tacticsoft.TableView::GetCumulativeRowHeight(System.Int32)
extern "C"  float TableView_GetCumulativeRowHeight_m4166556261 (TableView_t692333993 * __this, int32_t ___row0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Tacticsoft.TableView::StoreCellForReuse(Tacticsoft.TableViewCell,System.Boolean)
extern "C"  void TableView_StoreCellForReuse_m4158202822 (TableView_t692333993 * __this, TableViewCell_t776419755 * ___cell0, bool ___clear1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
