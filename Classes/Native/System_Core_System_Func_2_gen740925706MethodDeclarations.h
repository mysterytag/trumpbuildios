﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "System_Core_System_Func_2_gen2470508636MethodDeclarations.h"

// System.Void System.Func`2<System.Collections.Generic.KeyValuePair`2<Zenject.BindingId,System.Collections.Generic.List`1<Zenject.ProviderBase>>,Zenject.BindingId>::.ctor(System.Object,System.IntPtr)
#define Func_2__ctor_m2021548768(__this, ___object0, ___method1, method) ((  void (*) (Func_2_t740925706 *, Il2CppObject *, IntPtr_t, const MethodInfo*))Func_2__ctor_m1887411215_gshared)(__this, ___object0, ___method1, method)
// TResult System.Func`2<System.Collections.Generic.KeyValuePair`2<Zenject.BindingId,System.Collections.Generic.List`1<Zenject.ProviderBase>>,Zenject.BindingId>::Invoke(T)
#define Func_2_Invoke_m818913302(__this, ___arg10, method) ((  BindingId_t2965794261 * (*) (Func_2_t740925706 *, KeyValuePair_2_t2091358871 , const MethodInfo*))Func_2_Invoke_m3228321655_gshared)(__this, ___arg10, method)
// System.IAsyncResult System.Func`2<System.Collections.Generic.KeyValuePair`2<Zenject.BindingId,System.Collections.Generic.List`1<Zenject.ProviderBase>>,Zenject.BindingId>::BeginInvoke(T,System.AsyncCallback,System.Object)
#define Func_2_BeginInvoke_m3916323017(__this, ___arg10, ___callback1, ___object2, method) ((  Il2CppObject * (*) (Func_2_t740925706 *, KeyValuePair_2_t2091358871 , AsyncCallback_t1363551830 *, Il2CppObject *, const MethodInfo*))Func_2_BeginInvoke_m2365319082_gshared)(__this, ___arg10, ___callback1, ___object2, method)
// TResult System.Func`2<System.Collections.Generic.KeyValuePair`2<Zenject.BindingId,System.Collections.Generic.List`1<Zenject.ProviderBase>>,Zenject.BindingId>::EndInvoke(System.IAsyncResult)
#define Func_2_EndInvoke_m428256638(__this, ___result0, method) ((  BindingId_t2965794261 * (*) (Func_2_t740925706 *, Il2CppObject *, const MethodInfo*))Func_2_EndInvoke_m3637779645_gshared)(__this, ___result0, method)
