﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Subject`1<UnityEngine.Rect>
struct Subject_1_t3463463437;
// System.Exception
struct Exception_t1967233988;
// System.IDisposable
struct IDisposable_t1628921374;
// UniRx.IObserver`1<UnityEngine.Rect>
struct IObserver_1_t3737427720;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Exception1967233988.h"
#include "UnityEngine_UnityEngine_Rect1525428817.h"

// System.Void UniRx.Subject`1<UnityEngine.Rect>::.ctor()
extern "C"  void Subject_1__ctor_m2201538955_gshared (Subject_1_t3463463437 * __this, const MethodInfo* method);
#define Subject_1__ctor_m2201538955(__this, method) ((  void (*) (Subject_1_t3463463437 *, const MethodInfo*))Subject_1__ctor_m2201538955_gshared)(__this, method)
// System.Boolean UniRx.Subject`1<UnityEngine.Rect>::get_HasObservers()
extern "C"  bool Subject_1_get_HasObservers_m3539506281_gshared (Subject_1_t3463463437 * __this, const MethodInfo* method);
#define Subject_1_get_HasObservers_m3539506281(__this, method) ((  bool (*) (Subject_1_t3463463437 *, const MethodInfo*))Subject_1_get_HasObservers_m3539506281_gshared)(__this, method)
// System.Void UniRx.Subject`1<UnityEngine.Rect>::OnCompleted()
extern "C"  void Subject_1_OnCompleted_m1501917397_gshared (Subject_1_t3463463437 * __this, const MethodInfo* method);
#define Subject_1_OnCompleted_m1501917397(__this, method) ((  void (*) (Subject_1_t3463463437 *, const MethodInfo*))Subject_1_OnCompleted_m1501917397_gshared)(__this, method)
// System.Void UniRx.Subject`1<UnityEngine.Rect>::OnError(System.Exception)
extern "C"  void Subject_1_OnError_m478972546_gshared (Subject_1_t3463463437 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method);
#define Subject_1_OnError_m478972546(__this, ___error0, method) ((  void (*) (Subject_1_t3463463437 *, Exception_t1967233988 *, const MethodInfo*))Subject_1_OnError_m478972546_gshared)(__this, ___error0, method)
// System.Void UniRx.Subject`1<UnityEngine.Rect>::OnNext(T)
extern "C"  void Subject_1_OnNext_m1090762099_gshared (Subject_1_t3463463437 * __this, Rect_t1525428817  ___value0, const MethodInfo* method);
#define Subject_1_OnNext_m1090762099(__this, ___value0, method) ((  void (*) (Subject_1_t3463463437 *, Rect_t1525428817 , const MethodInfo*))Subject_1_OnNext_m1090762099_gshared)(__this, ___value0, method)
// System.IDisposable UniRx.Subject`1<UnityEngine.Rect>::Subscribe(UniRx.IObserver`1<T>)
extern "C"  Il2CppObject * Subject_1_Subscribe_m2934956448_gshared (Subject_1_t3463463437 * __this, Il2CppObject* ___observer0, const MethodInfo* method);
#define Subject_1_Subscribe_m2934956448(__this, ___observer0, method) ((  Il2CppObject * (*) (Subject_1_t3463463437 *, Il2CppObject*, const MethodInfo*))Subject_1_Subscribe_m2934956448_gshared)(__this, ___observer0, method)
// System.Void UniRx.Subject`1<UnityEngine.Rect>::Dispose()
extern "C"  void Subject_1_Dispose_m2453309192_gshared (Subject_1_t3463463437 * __this, const MethodInfo* method);
#define Subject_1_Dispose_m2453309192(__this, method) ((  void (*) (Subject_1_t3463463437 *, const MethodInfo*))Subject_1_Dispose_m2453309192_gshared)(__this, method)
// System.Void UniRx.Subject`1<UnityEngine.Rect>::ThrowIfDisposed()
extern "C"  void Subject_1_ThrowIfDisposed_m2147788945_gshared (Subject_1_t3463463437 * __this, const MethodInfo* method);
#define Subject_1_ThrowIfDisposed_m2147788945(__this, method) ((  void (*) (Subject_1_t3463463437 *, const MethodInfo*))Subject_1_ThrowIfDisposed_m2147788945_gshared)(__this, method)
// System.Boolean UniRx.Subject`1<UnityEngine.Rect>::IsRequiredSubscribeOnCurrentThread()
extern "C"  bool Subject_1_IsRequiredSubscribeOnCurrentThread_m3041532192_gshared (Subject_1_t3463463437 * __this, const MethodInfo* method);
#define Subject_1_IsRequiredSubscribeOnCurrentThread_m3041532192(__this, method) ((  bool (*) (Subject_1_t3463463437 *, const MethodInfo*))Subject_1_IsRequiredSubscribeOnCurrentThread_m3041532192_gshared)(__this, method)
