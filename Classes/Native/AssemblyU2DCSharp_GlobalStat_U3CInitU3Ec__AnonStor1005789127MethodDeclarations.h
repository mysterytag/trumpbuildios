﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GlobalStat/<Init>c__AnonStoreyE7
struct U3CInitU3Ec__AnonStoreyE7_t1005789127;

#include "codegen/il2cpp-codegen.h"

// System.Void GlobalStat/<Init>c__AnonStoreyE7::.ctor()
extern "C"  void U3CInitU3Ec__AnonStoreyE7__ctor_m2010858252 (U3CInitU3Ec__AnonStoreyE7_t1005789127 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GlobalStat/<Init>c__AnonStoreyE7::<>m__10D(System.Int32)
extern "C"  void U3CInitU3Ec__AnonStoreyE7_U3CU3Em__10D_m1818891459 (U3CInitU3Ec__AnonStoreyE7_t1005789127 * __this, int32_t ___i0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
