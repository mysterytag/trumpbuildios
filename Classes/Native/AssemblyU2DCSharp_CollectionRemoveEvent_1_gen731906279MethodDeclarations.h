﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_CollectionRemoveEvent_1_gen898259258MethodDeclarations.h"

// System.Void CollectionRemoveEvent`1<DonateButton>::.ctor(System.Int32,T)
#define CollectionRemoveEvent_1__ctor_m797598122(__this, ___index0, ___value1, method) ((  void (*) (CollectionRemoveEvent_1_t731906279 *, int32_t, DonateButton_t670753441 *, const MethodInfo*))CollectionRemoveEvent_1__ctor_m1174003473_gshared)(__this, ___index0, ___value1, method)
// System.Int32 CollectionRemoveEvent`1<DonateButton>::get_Index()
#define CollectionRemoveEvent_1_get_Index_m1514750422(__this, method) ((  int32_t (*) (CollectionRemoveEvent_1_t731906279 *, const MethodInfo*))CollectionRemoveEvent_1_get_Index_m2777665697_gshared)(__this, method)
// System.Void CollectionRemoveEvent`1<DonateButton>::set_Index(System.Int32)
#define CollectionRemoveEvent_1_set_Index_m497760165(__this, ___value0, method) ((  void (*) (CollectionRemoveEvent_1_t731906279 *, int32_t, const MethodInfo*))CollectionRemoveEvent_1_set_Index_m1446049612_gshared)(__this, ___value0, method)
// T CollectionRemoveEvent`1<DonateButton>::get_Value()
#define CollectionRemoveEvent_1_get_Value_m2138368486(__this, method) ((  DonateButton_t670753441 * (*) (CollectionRemoveEvent_1_t731906279 *, const MethodInfo*))CollectionRemoveEvent_1_get_Value_m445974639_gshared)(__this, method)
// System.Void CollectionRemoveEvent`1<DonateButton>::set_Value(T)
#define CollectionRemoveEvent_1_set_Value_m989499019(__this, ___value0, method) ((  void (*) (CollectionRemoveEvent_1_t731906279 *, DonateButton_t670753441 *, const MethodInfo*))CollectionRemoveEvent_1_set_Value_m347083076_gshared)(__this, ___value0, method)
// System.String CollectionRemoveEvent`1<DonateButton>::ToString()
#define CollectionRemoveEvent_1_ToString_m3852871436(__this, method) ((  String_t* (*) (CollectionRemoveEvent_1_t731906279 *, const MethodInfo*))CollectionRemoveEvent_1_ToString_m2292716331_gshared)(__this, method)
