﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Func`3<System.AsyncCallback,System.Object,System.IAsyncResult>
struct Func_3_t3381739440;
// System.Func`2<System.IAsyncResult,System.Object>
struct Func_2_t850390275;
// System.Net.WebRequest
struct WebRequest_t3488810021;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.WebRequestExtensions/<AbortableDeferredAsyncRequest>c__AnonStorey33`1<System.Object>
struct  U3CAbortableDeferredAsyncRequestU3Ec__AnonStorey33_1_t4099961315  : public Il2CppObject
{
public:
	// System.Func`3<System.AsyncCallback,System.Object,System.IAsyncResult> UniRx.WebRequestExtensions/<AbortableDeferredAsyncRequest>c__AnonStorey33`1::begin
	Func_3_t3381739440 * ___begin_0;
	// System.Func`2<System.IAsyncResult,TResult> UniRx.WebRequestExtensions/<AbortableDeferredAsyncRequest>c__AnonStorey33`1::end
	Func_2_t850390275 * ___end_1;
	// System.Net.WebRequest UniRx.WebRequestExtensions/<AbortableDeferredAsyncRequest>c__AnonStorey33`1::request
	WebRequest_t3488810021 * ___request_2;

public:
	inline static int32_t get_offset_of_begin_0() { return static_cast<int32_t>(offsetof(U3CAbortableDeferredAsyncRequestU3Ec__AnonStorey33_1_t4099961315, ___begin_0)); }
	inline Func_3_t3381739440 * get_begin_0() const { return ___begin_0; }
	inline Func_3_t3381739440 ** get_address_of_begin_0() { return &___begin_0; }
	inline void set_begin_0(Func_3_t3381739440 * value)
	{
		___begin_0 = value;
		Il2CppCodeGenWriteBarrier(&___begin_0, value);
	}

	inline static int32_t get_offset_of_end_1() { return static_cast<int32_t>(offsetof(U3CAbortableDeferredAsyncRequestU3Ec__AnonStorey33_1_t4099961315, ___end_1)); }
	inline Func_2_t850390275 * get_end_1() const { return ___end_1; }
	inline Func_2_t850390275 ** get_address_of_end_1() { return &___end_1; }
	inline void set_end_1(Func_2_t850390275 * value)
	{
		___end_1 = value;
		Il2CppCodeGenWriteBarrier(&___end_1, value);
	}

	inline static int32_t get_offset_of_request_2() { return static_cast<int32_t>(offsetof(U3CAbortableDeferredAsyncRequestU3Ec__AnonStorey33_1_t4099961315, ___request_2)); }
	inline WebRequest_t3488810021 * get_request_2() const { return ___request_2; }
	inline WebRequest_t3488810021 ** get_address_of_request_2() { return &___request_2; }
	inline void set_request_2(WebRequest_t3488810021 * value)
	{
		___request_2 = value;
		Il2CppCodeGenWriteBarrier(&___request_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
