﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.InternalUtil.ListObserver`1<UniRx.CollectionAddEvent`1<UniRx.Tuple`2<System.Object,System.Object>>>
struct ListObserver_1_t2243295057;
// UniRx.InternalUtil.ImmutableList`1<UniRx.IObserver`1<UniRx.CollectionAddEvent`1<UniRx.Tuple`2<System.Object,System.Object>>>>
struct ImmutableList_1_t925913492;
// System.Exception
struct Exception_t1967233988;
// UniRx.IObserver`1<UniRx.CollectionAddEvent`1<UniRx.Tuple`2<System.Object,System.Object>>>
struct IObserver_1_t4160676289;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Exception1967233988.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_CollectionAddE1948677386.h"

// System.Void UniRx.InternalUtil.ListObserver`1<UniRx.CollectionAddEvent`1<UniRx.Tuple`2<System.Object,System.Object>>>::.ctor(UniRx.InternalUtil.ImmutableList`1<UniRx.IObserver`1<T>>)
extern "C"  void ListObserver_1__ctor_m2435064037_gshared (ListObserver_1_t2243295057 * __this, ImmutableList_1_t925913492 * ___observers0, const MethodInfo* method);
#define ListObserver_1__ctor_m2435064037(__this, ___observers0, method) ((  void (*) (ListObserver_1_t2243295057 *, ImmutableList_1_t925913492 *, const MethodInfo*))ListObserver_1__ctor_m2435064037_gshared)(__this, ___observers0, method)
// System.Void UniRx.InternalUtil.ListObserver`1<UniRx.CollectionAddEvent`1<UniRx.Tuple`2<System.Object,System.Object>>>::OnCompleted()
extern "C"  void ListObserver_1_OnCompleted_m3674177117_gshared (ListObserver_1_t2243295057 * __this, const MethodInfo* method);
#define ListObserver_1_OnCompleted_m3674177117(__this, method) ((  void (*) (ListObserver_1_t2243295057 *, const MethodInfo*))ListObserver_1_OnCompleted_m3674177117_gshared)(__this, method)
// System.Void UniRx.InternalUtil.ListObserver`1<UniRx.CollectionAddEvent`1<UniRx.Tuple`2<System.Object,System.Object>>>::OnError(System.Exception)
extern "C"  void ListObserver_1_OnError_m2482408458_gshared (ListObserver_1_t2243295057 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method);
#define ListObserver_1_OnError_m2482408458(__this, ___error0, method) ((  void (*) (ListObserver_1_t2243295057 *, Exception_t1967233988 *, const MethodInfo*))ListObserver_1_OnError_m2482408458_gshared)(__this, ___error0, method)
// System.Void UniRx.InternalUtil.ListObserver`1<UniRx.CollectionAddEvent`1<UniRx.Tuple`2<System.Object,System.Object>>>::OnNext(T)
extern "C"  void ListObserver_1_OnNext_m846317307_gshared (ListObserver_1_t2243295057 * __this, CollectionAddEvent_1_t1948677386  ___value0, const MethodInfo* method);
#define ListObserver_1_OnNext_m846317307(__this, ___value0, method) ((  void (*) (ListObserver_1_t2243295057 *, CollectionAddEvent_1_t1948677386 , const MethodInfo*))ListObserver_1_OnNext_m846317307_gshared)(__this, ___value0, method)
// UniRx.IObserver`1<T> UniRx.InternalUtil.ListObserver`1<UniRx.CollectionAddEvent`1<UniRx.Tuple`2<System.Object,System.Object>>>::Add(UniRx.IObserver`1<T>)
extern "C"  Il2CppObject* ListObserver_1_Add_m926992155_gshared (ListObserver_1_t2243295057 * __this, Il2CppObject* ___observer0, const MethodInfo* method);
#define ListObserver_1_Add_m926992155(__this, ___observer0, method) ((  Il2CppObject* (*) (ListObserver_1_t2243295057 *, Il2CppObject*, const MethodInfo*))ListObserver_1_Add_m926992155_gshared)(__this, ___observer0, method)
// UniRx.IObserver`1<T> UniRx.InternalUtil.ListObserver`1<UniRx.CollectionAddEvent`1<UniRx.Tuple`2<System.Object,System.Object>>>::Remove(UniRx.IObserver`1<T>)
extern "C"  Il2CppObject* ListObserver_1_Remove_m4111989328_gshared (ListObserver_1_t2243295057 * __this, Il2CppObject* ___observer0, const MethodInfo* method);
#define ListObserver_1_Remove_m4111989328(__this, ___observer0, method) ((  Il2CppObject* (*) (ListObserver_1_t2243295057 *, Il2CppObject*, const MethodInfo*))ListObserver_1_Remove_m4111989328_gshared)(__this, ___observer0, method)
