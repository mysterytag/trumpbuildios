﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.WhenAllObservable`1/WhenAll<System.Object>
struct WhenAll_t959244232;
// UniRx.IObservable`1<System.Object>[]
struct IObservable_1U5BU5D_t2205425841;
// UniRx.IObserver`1<System.Object[]>
struct IObserver_1_t2223522676;
// System.IDisposable
struct IDisposable_t1628921374;
// System.Object[]
struct ObjectU5BU5D_t11523773;
// System.Exception
struct Exception_t1967233988;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Exception1967233988.h"

// System.Void UniRx.Operators.WhenAllObservable`1/WhenAll<System.Object>::.ctor(UniRx.IObservable`1<T>[],UniRx.IObserver`1<T[]>,System.IDisposable)
extern "C"  void WhenAll__ctor_m3681014254_gshared (WhenAll_t959244232 * __this, IObservable_1U5BU5D_t2205425841* ___sources0, Il2CppObject* ___observer1, Il2CppObject * ___cancel2, const MethodInfo* method);
#define WhenAll__ctor_m3681014254(__this, ___sources0, ___observer1, ___cancel2, method) ((  void (*) (WhenAll_t959244232 *, IObservable_1U5BU5D_t2205425841*, Il2CppObject*, Il2CppObject *, const MethodInfo*))WhenAll__ctor_m3681014254_gshared)(__this, ___sources0, ___observer1, ___cancel2, method)
// System.IDisposable UniRx.Operators.WhenAllObservable`1/WhenAll<System.Object>::Run()
extern "C"  Il2CppObject * WhenAll_Run_m2187571261_gshared (WhenAll_t959244232 * __this, const MethodInfo* method);
#define WhenAll_Run_m2187571261(__this, method) ((  Il2CppObject * (*) (WhenAll_t959244232 *, const MethodInfo*))WhenAll_Run_m2187571261_gshared)(__this, method)
// System.Void UniRx.Operators.WhenAllObservable`1/WhenAll<System.Object>::OnNext(T[])
extern "C"  void WhenAll_OnNext_m1600442485_gshared (WhenAll_t959244232 * __this, ObjectU5BU5D_t11523773* ___value0, const MethodInfo* method);
#define WhenAll_OnNext_m1600442485(__this, ___value0, method) ((  void (*) (WhenAll_t959244232 *, ObjectU5BU5D_t11523773*, const MethodInfo*))WhenAll_OnNext_m1600442485_gshared)(__this, ___value0, method)
// System.Void UniRx.Operators.WhenAllObservable`1/WhenAll<System.Object>::OnError(System.Exception)
extern "C"  void WhenAll_OnError_m3421863974_gshared (WhenAll_t959244232 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method);
#define WhenAll_OnError_m3421863974(__this, ___error0, method) ((  void (*) (WhenAll_t959244232 *, Exception_t1967233988 *, const MethodInfo*))WhenAll_OnError_m3421863974_gshared)(__this, ___error0, method)
// System.Void UniRx.Operators.WhenAllObservable`1/WhenAll<System.Object>::OnCompleted()
extern "C"  void WhenAll_OnCompleted_m1805040761_gshared (WhenAll_t959244232 * __this, const MethodInfo* method);
#define WhenAll_OnCompleted_m1805040761(__this, method) ((  void (*) (WhenAll_t959244232 *, const MethodInfo*))WhenAll_OnCompleted_m1805040761_gshared)(__this, method)
