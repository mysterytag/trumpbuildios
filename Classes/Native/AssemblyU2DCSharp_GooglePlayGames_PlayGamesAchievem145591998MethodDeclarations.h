﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GooglePlayGames.PlayGamesAchievement
struct PlayGamesAchievement_t145591998;
// GooglePlayGames.ReportProgress
struct ReportProgress_t1158862141;
// System.Action`1<System.Boolean>
struct Action_1_t359458046;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_GooglePlayGames_ReportProgress1158862141.h"
#include "mscorlib_System_String968488902.h"
#include "mscorlib_System_DateTime339033936.h"

// System.Void GooglePlayGames.PlayGamesAchievement::.ctor()
extern "C"  void PlayGamesAchievement__ctor_m3384221919 (PlayGamesAchievement_t145591998 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.PlayGamesAchievement::.ctor(GooglePlayGames.ReportProgress)
extern "C"  void PlayGamesAchievement__ctor_m1026446980 (PlayGamesAchievement_t145591998 * __this, ReportProgress_t1158862141 * ___progressCallback0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.PlayGamesAchievement::.cctor()
extern "C"  void PlayGamesAchievement__cctor_m1349568174 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.PlayGamesAchievement::ReportProgress(System.Action`1<System.Boolean>)
extern "C"  void PlayGamesAchievement_ReportProgress_m4053774893 (PlayGamesAchievement_t145591998 * __this, Action_1_t359458046 * ___callback0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String GooglePlayGames.PlayGamesAchievement::get_id()
extern "C"  String_t* PlayGamesAchievement_get_id_m197866988 (PlayGamesAchievement_t145591998 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.PlayGamesAchievement::set_id(System.String)
extern "C"  void PlayGamesAchievement_set_id_m1300436645 (PlayGamesAchievement_t145591998 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Double GooglePlayGames.PlayGamesAchievement::get_percentCompleted()
extern "C"  double PlayGamesAchievement_get_percentCompleted_m2462165463 (PlayGamesAchievement_t145591998 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.PlayGamesAchievement::set_percentCompleted(System.Double)
extern "C"  void PlayGamesAchievement_set_percentCompleted_m870793946 (PlayGamesAchievement_t145591998 * __this, double ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GooglePlayGames.PlayGamesAchievement::get_completed()
extern "C"  bool PlayGamesAchievement_get_completed_m3462469643 (PlayGamesAchievement_t145591998 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GooglePlayGames.PlayGamesAchievement::get_hidden()
extern "C"  bool PlayGamesAchievement_get_hidden_m1598629132 (PlayGamesAchievement_t145591998 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.DateTime GooglePlayGames.PlayGamesAchievement::get_lastReportedDate()
extern "C"  DateTime_t339033936  PlayGamesAchievement_get_lastReportedDate_m802603102 (PlayGamesAchievement_t145591998 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
