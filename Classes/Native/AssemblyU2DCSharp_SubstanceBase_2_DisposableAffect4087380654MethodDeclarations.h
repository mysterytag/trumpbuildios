﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SubstanceBase`2/DisposableAffect<System.Double,System.Double>
struct DisposableAffect_t4087380654;

#include "codegen/il2cpp-codegen.h"

// System.Void SubstanceBase`2/DisposableAffect<System.Double,System.Double>::.ctor()
extern "C"  void DisposableAffect__ctor_m39885334_gshared (DisposableAffect_t4087380654 * __this, const MethodInfo* method);
#define DisposableAffect__ctor_m39885334(__this, method) ((  void (*) (DisposableAffect_t4087380654 *, const MethodInfo*))DisposableAffect__ctor_m39885334_gshared)(__this, method)
// System.Void SubstanceBase`2/DisposableAffect<System.Double,System.Double>::Dispose()
extern "C"  void DisposableAffect_Dispose_m3868350675_gshared (DisposableAffect_t4087380654 * __this, const MethodInfo* method);
#define DisposableAffect_Dispose_m3868350675(__this, method) ((  void (*) (DisposableAffect_t4087380654 *, const MethodInfo*))DisposableAffect_Dispose_m3868350675_gshared)(__this, method)
