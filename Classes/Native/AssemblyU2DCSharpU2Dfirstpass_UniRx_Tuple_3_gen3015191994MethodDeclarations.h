﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Tuple_3_gen2907327732MethodDeclarations.h"

// System.Void UniRx.Tuple`3<UniRx.ICancelable,UniRx.Unit,System.Action`1<UniRx.Unit>>::.ctor(T1,T2,T3)
#define Tuple_3__ctor_m360770039(__this, ___item10, ___item21, ___item32, method) ((  void (*) (Tuple_3_t3015191994 *, Il2CppObject *, Unit_t2558286038 , Action_1_t2706738743 *, const MethodInfo*))Tuple_3__ctor_m2303875472_gshared)(__this, ___item10, ___item21, ___item32, method)
// System.Int32 UniRx.Tuple`3<UniRx.ICancelable,UniRx.Unit,System.Action`1<UniRx.Unit>>::System.IComparable.CompareTo(System.Object)
#define Tuple_3_System_IComparable_CompareTo_m559560357(__this, ___obj0, method) ((  int32_t (*) (Tuple_3_t3015191994 *, Il2CppObject *, const MethodInfo*))Tuple_3_System_IComparable_CompareTo_m2583537726_gshared)(__this, ___obj0, method)
// System.Int32 UniRx.Tuple`3<UniRx.ICancelable,UniRx.Unit,System.Action`1<UniRx.Unit>>::UniRx.IStructuralComparable.CompareTo(System.Object,System.Collections.IComparer)
#define Tuple_3_UniRx_IStructuralComparable_CompareTo_m2867071927(__this, ___other0, ___comparer1, method) ((  int32_t (*) (Tuple_3_t3015191994 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))Tuple_3_UniRx_IStructuralComparable_CompareTo_m536773264_gshared)(__this, ___other0, ___comparer1, method)
// System.Boolean UniRx.Tuple`3<UniRx.ICancelable,UniRx.Unit,System.Action`1<UniRx.Unit>>::UniRx.IStructuralEquatable.Equals(System.Object,System.Collections.IEqualityComparer)
#define Tuple_3_UniRx_IStructuralEquatable_Equals_m3996201390(__this, ___other0, ___comparer1, method) ((  bool (*) (Tuple_3_t3015191994 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))Tuple_3_UniRx_IStructuralEquatable_Equals_m366784903_gshared)(__this, ___other0, ___comparer1, method)
// System.Int32 UniRx.Tuple`3<UniRx.ICancelable,UniRx.Unit,System.Action`1<UniRx.Unit>>::UniRx.IStructuralEquatable.GetHashCode(System.Collections.IEqualityComparer)
#define Tuple_3_UniRx_IStructuralEquatable_GetHashCode_m1982817612(__this, ___comparer0, method) ((  int32_t (*) (Tuple_3_t3015191994 *, Il2CppObject *, const MethodInfo*))Tuple_3_UniRx_IStructuralEquatable_GetHashCode_m1803176659_gshared)(__this, ___comparer0, method)
// System.String UniRx.Tuple`3<UniRx.ICancelable,UniRx.Unit,System.Action`1<UniRx.Unit>>::UniRx.ITuple.ToString()
#define Tuple_3_UniRx_ITuple_ToString_m1814713881(__this, method) ((  String_t* (*) (Tuple_3_t3015191994 *, const MethodInfo*))Tuple_3_UniRx_ITuple_ToString_m364752754_gshared)(__this, method)
// T1 UniRx.Tuple`3<UniRx.ICancelable,UniRx.Unit,System.Action`1<UniRx.Unit>>::get_Item1()
#define Tuple_3_get_Item1_m1922625904(__this, method) ((  Il2CppObject * (*) (Tuple_3_t3015191994 *, const MethodInfo*))Tuple_3_get_Item1_m63303561_gshared)(__this, method)
// T2 UniRx.Tuple`3<UniRx.ICancelable,UniRx.Unit,System.Action`1<UniRx.Unit>>::get_Item2()
#define Tuple_3_get_Item2_m3090823120(__this, method) ((  Unit_t2558286038  (*) (Tuple_3_t3015191994 *, const MethodInfo*))Tuple_3_get_Item2_m2687758249_gshared)(__this, method)
// T3 UniRx.Tuple`3<UniRx.ICancelable,UniRx.Unit,System.Action`1<UniRx.Unit>>::get_Item3()
#define Tuple_3_get_Item3_m4259020336(__this, method) ((  Action_1_t2706738743 * (*) (Tuple_3_t3015191994 *, const MethodInfo*))Tuple_3_get_Item3_m1017245641_gshared)(__this, method)
// System.Boolean UniRx.Tuple`3<UniRx.ICancelable,UniRx.Unit,System.Action`1<UniRx.Unit>>::Equals(System.Object)
#define Tuple_3_Equals_m433368162(__this, ___obj0, method) ((  bool (*) (Tuple_3_t3015191994 *, Il2CppObject *, const MethodInfo*))Tuple_3_Equals_m871068731_gshared)(__this, ___obj0, method)
// System.Int32 UniRx.Tuple`3<UniRx.ICancelable,UniRx.Unit,System.Action`1<UniRx.Unit>>::GetHashCode()
#define Tuple_3_GetHashCode_m302807290(__this, method) ((  int32_t (*) (Tuple_3_t3015191994 *, const MethodInfo*))Tuple_3_GetHashCode_m3500510291_gshared)(__this, method)
// System.String UniRx.Tuple`3<UniRx.ICancelable,UniRx.Unit,System.Action`1<UniRx.Unit>>::ToString()
#define Tuple_3_ToString_m771461240(__this, method) ((  String_t* (*) (Tuple_3_t3015191994 *, const MethodInfo*))Tuple_3_ToString_m2811558527_gshared)(__this, method)
// System.Boolean UniRx.Tuple`3<UniRx.ICancelable,UniRx.Unit,System.Action`1<UniRx.Unit>>::Equals(UniRx.Tuple`3<T1,T2,T3>)
#define Tuple_3_Equals_m2351045305(__this, ___other0, method) ((  bool (*) (Tuple_3_t3015191994 *, Tuple_3_t3015191994 , const MethodInfo*))Tuple_3_Equals_m2139596626_gshared)(__this, ___other0, method)
