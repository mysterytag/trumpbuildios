﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_K3261809038MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<GameAnalyticsSDK.GA_ServerFieldTypes/FieldType,System.String>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define KeyCollection__ctor_m4078489776(__this, ___dictionary0, method) ((  void (*) (KeyCollection_t3393191520 *, Dictionary_2_t1069916240 *, const MethodInfo*))KeyCollection__ctor_m1969919838_gshared)(__this, ___dictionary0, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<GameAnalyticsSDK.GA_ServerFieldTypes/FieldType,System.String>::System.Collections.Generic.ICollection<TKey>.Add(TKey)
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m2270747302(__this, ___item0, method) ((  void (*) (KeyCollection_t3393191520 *, int32_t, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m3509919544_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<GameAnalyticsSDK.GA_ServerFieldTypes/FieldType,System.String>::System.Collections.Generic.ICollection<TKey>.Clear()
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m3510727069(__this, method) ((  void (*) (KeyCollection_t3393191520 *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m875147951_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<GameAnalyticsSDK.GA_ServerFieldTypes/FieldType,System.String>::System.Collections.Generic.ICollection<TKey>.Contains(TKey)
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m4036305512(__this, ___item0, method) ((  bool (*) (KeyCollection_t3393191520 *, int32_t, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m1927735574_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<GameAnalyticsSDK.GA_ServerFieldTypes/FieldType,System.String>::System.Collections.Generic.ICollection<TKey>.Remove(TKey)
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m2065108429(__this, ___item0, method) ((  bool (*) (KeyCollection_t3393191520 *, int32_t, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m3001460731_gshared)(__this, ___item0, method)
// System.Collections.Generic.IEnumerator`1<TKey> System.Collections.Generic.Dictionary`2/KeyCollection<GameAnalyticsSDK.GA_ServerFieldTypes/FieldType,System.String>::System.Collections.Generic.IEnumerable<TKey>.GetEnumerator()
#define KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m3893729071(__this, method) ((  Il2CppObject* (*) (KeyCollection_t3393191520 *, const MethodInfo*))KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m2952570433_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<GameAnalyticsSDK.GA_ServerFieldTypes/FieldType,System.String>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define KeyCollection_System_Collections_ICollection_CopyTo_m3562853007(__this, ___array0, ___index1, method) ((  void (*) (KeyCollection_t3393191520 *, Il2CppArray *, int32_t, const MethodInfo*))KeyCollection_System_Collections_ICollection_CopyTo_m622987425_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2/KeyCollection<GameAnalyticsSDK.GA_ServerFieldTypes/FieldType,System.String>::System.Collections.IEnumerable.GetEnumerator()
#define KeyCollection_System_Collections_IEnumerable_GetEnumerator_m450899038(__this, method) ((  Il2CppObject * (*) (KeyCollection_t3393191520 *, const MethodInfo*))KeyCollection_System_Collections_IEnumerable_GetEnumerator_m3708494576_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<GameAnalyticsSDK.GA_ServerFieldTypes/FieldType,System.String>::System.Collections.Generic.ICollection<TKey>.get_IsReadOnly()
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m3415187401(__this, method) ((  bool (*) (KeyCollection_t3393191520 *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m9073399_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<GameAnalyticsSDK.GA_ServerFieldTypes/FieldType,System.String>::System.Collections.ICollection.get_IsSynchronized()
#define KeyCollection_System_Collections_ICollection_get_IsSynchronized_m1312300987(__this, method) ((  bool (*) (KeyCollection_t3393191520 *, const MethodInfo*))KeyCollection_System_Collections_ICollection_get_IsSynchronized_m395998313_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/KeyCollection<GameAnalyticsSDK.GA_ServerFieldTypes/FieldType,System.String>::System.Collections.ICollection.get_SyncRoot()
#define KeyCollection_System_Collections_ICollection_get_SyncRoot_m2077394541(__this, method) ((  Il2CppObject * (*) (KeyCollection_t3393191520 *, const MethodInfo*))KeyCollection_System_Collections_ICollection_get_SyncRoot_m1074099611_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<GameAnalyticsSDK.GA_ServerFieldTypes/FieldType,System.String>::CopyTo(TKey[],System.Int32)
#define KeyCollection_CopyTo_m1458894693(__this, ___array0, ___index1, method) ((  void (*) (KeyCollection_t3393191520 *, FieldTypeU5BU5D_t1420599677*, int32_t, const MethodInfo*))KeyCollection_CopyTo_m2686252563_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/KeyCollection<GameAnalyticsSDK.GA_ServerFieldTypes/FieldType,System.String>::GetEnumerator()
#define KeyCollection_GetEnumerator_m2827486194(__this, method) ((  Enumerator_t836944182  (*) (KeyCollection_t3393191520 *, const MethodInfo*))KeyCollection_GetEnumerator_m2552633248_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2/KeyCollection<GameAnalyticsSDK.GA_ServerFieldTypes/FieldType,System.String>::get_Count()
#define KeyCollection_get_Count_m2658298357(__this, method) ((  int32_t (*) (KeyCollection_t3393191520 *, const MethodInfo*))KeyCollection_get_Count_m26838179_gshared)(__this, method)
