﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.RefCountDisposable/InnerDisposable
struct InnerDisposable_t3329838134;
// UniRx.RefCountDisposable
struct RefCountDisposable_t3288429070;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_RefCountDispos3288429070.h"

// System.Void UniRx.RefCountDisposable/InnerDisposable::.ctor(UniRx.RefCountDisposable)
extern "C"  void InnerDisposable__ctor_m1585869792 (InnerDisposable_t3329838134 * __this, RefCountDisposable_t3288429070 * ___parent0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.RefCountDisposable/InnerDisposable::Dispose()
extern "C"  void InnerDisposable_Dispose_m1642446385 (InnerDisposable_t3329838134 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
