﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Action`1<System.Object>
struct Action_1_t985559125;
// StreamAPI/<Map>c__AnonStorey130`2<System.Object,System.Object>
struct U3CMapU3Ec__AnonStorey130_2_t2728082812;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// StreamAPI/<Map>c__AnonStorey130`2/<Map>c__AnonStorey131`2<System.Object,System.Object>
struct  U3CMapU3Ec__AnonStorey131_2_t3825613791  : public Il2CppObject
{
public:
	// System.Action`1<T2> StreamAPI/<Map>c__AnonStorey130`2/<Map>c__AnonStorey131`2::reaction
	Action_1_t985559125 * ___reaction_0;
	// StreamAPI/<Map>c__AnonStorey130`2<T,T2> StreamAPI/<Map>c__AnonStorey130`2/<Map>c__AnonStorey131`2::<>f__ref$304
	U3CMapU3Ec__AnonStorey130_2_t2728082812 * ___U3CU3Ef__refU24304_1;

public:
	inline static int32_t get_offset_of_reaction_0() { return static_cast<int32_t>(offsetof(U3CMapU3Ec__AnonStorey131_2_t3825613791, ___reaction_0)); }
	inline Action_1_t985559125 * get_reaction_0() const { return ___reaction_0; }
	inline Action_1_t985559125 ** get_address_of_reaction_0() { return &___reaction_0; }
	inline void set_reaction_0(Action_1_t985559125 * value)
	{
		___reaction_0 = value;
		Il2CppCodeGenWriteBarrier(&___reaction_0, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__refU24304_1() { return static_cast<int32_t>(offsetof(U3CMapU3Ec__AnonStorey131_2_t3825613791, ___U3CU3Ef__refU24304_1)); }
	inline U3CMapU3Ec__AnonStorey130_2_t2728082812 * get_U3CU3Ef__refU24304_1() const { return ___U3CU3Ef__refU24304_1; }
	inline U3CMapU3Ec__AnonStorey130_2_t2728082812 ** get_address_of_U3CU3Ef__refU24304_1() { return &___U3CU3Ef__refU24304_1; }
	inline void set_U3CU3Ef__refU24304_1(U3CMapU3Ec__AnonStorey130_2_t2728082812 * value)
	{
		___U3CU3Ef__refU24304_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__refU24304_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
