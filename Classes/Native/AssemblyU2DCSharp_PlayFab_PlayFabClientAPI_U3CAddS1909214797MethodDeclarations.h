﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.PlayFabClientAPI/<AddSharedGroupMembers>c__AnonStoreyBB
struct U3CAddSharedGroupMembersU3Ec__AnonStoreyBB_t1909214797;
// PlayFab.CallRequestContainer
struct CallRequestContainer_t432318609;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharpU2Dfirstpass_PlayFab_CallRequestCo432318609.h"

// System.Void PlayFab.PlayFabClientAPI/<AddSharedGroupMembers>c__AnonStoreyBB::.ctor()
extern "C"  void U3CAddSharedGroupMembersU3Ec__AnonStoreyBB__ctor_m2597043110 (U3CAddSharedGroupMembersU3Ec__AnonStoreyBB_t1909214797 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/<AddSharedGroupMembers>c__AnonStoreyBB::<>m__A8(PlayFab.CallRequestContainer)
extern "C"  void U3CAddSharedGroupMembersU3Ec__AnonStoreyBB_U3CU3Em__A8_m3140184539 (U3CAddSharedGroupMembersU3Ec__AnonStoreyBB_t1909214797 * __this, CallRequestContainer_t432318609 * ___requestContainer0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
