﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.ClientModels.UserPrivateAccountInfo
struct UserPrivateAccountInfo_t2894244435;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String968488902.h"

// System.Void PlayFab.ClientModels.UserPrivateAccountInfo::.ctor()
extern "C"  void UserPrivateAccountInfo__ctor_m3401108426 (UserPrivateAccountInfo_t2894244435 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PlayFab.ClientModels.UserPrivateAccountInfo::get_Email()
extern "C"  String_t* UserPrivateAccountInfo_get_Email_m84877118 (UserPrivateAccountInfo_t2894244435 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.UserPrivateAccountInfo::set_Email(System.String)
extern "C"  void UserPrivateAccountInfo_set_Email_m2159717179 (UserPrivateAccountInfo_t2894244435 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
