﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Zenject.ProviderBase
struct ProviderBase_t1627494391;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.BindingConditionSetter
struct  BindingConditionSetter_t259147722  : public Il2CppObject
{
public:
	// Zenject.ProviderBase Zenject.BindingConditionSetter::_provider
	ProviderBase_t1627494391 * ____provider_0;

public:
	inline static int32_t get_offset_of__provider_0() { return static_cast<int32_t>(offsetof(BindingConditionSetter_t259147722, ____provider_0)); }
	inline ProviderBase_t1627494391 * get__provider_0() const { return ____provider_0; }
	inline ProviderBase_t1627494391 ** get_address_of__provider_0() { return &____provider_0; }
	inline void set__provider_0(ProviderBase_t1627494391 * value)
	{
		____provider_0 = value;
		Il2CppCodeGenWriteBarrier(&____provider_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
