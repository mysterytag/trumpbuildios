﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.TakeObservable`1/Take_<System.Object>
struct Take__t2468273233;
// UniRx.Operators.TakeObservable`1<System.Object>
struct TakeObservable_1_t3078819489;
// UniRx.IObserver`1<System.Object>
struct IObserver_1_t3049105323;
// System.IDisposable
struct IDisposable_t1628921374;
// System.Object
struct Il2CppObject;
// System.Exception
struct Exception_t1967233988;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_Exception1967233988.h"

// System.Void UniRx.Operators.TakeObservable`1/Take_<System.Object>::.ctor(UniRx.Operators.TakeObservable`1<T>,UniRx.IObserver`1<T>,System.IDisposable)
extern "C"  void Take___ctor_m138741260_gshared (Take__t2468273233 * __this, TakeObservable_1_t3078819489 * ___parent0, Il2CppObject* ___observer1, Il2CppObject * ___cancel2, const MethodInfo* method);
#define Take___ctor_m138741260(__this, ___parent0, ___observer1, ___cancel2, method) ((  void (*) (Take__t2468273233 *, TakeObservable_1_t3078819489 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))Take___ctor_m138741260_gshared)(__this, ___parent0, ___observer1, ___cancel2, method)
// System.IDisposable UniRx.Operators.TakeObservable`1/Take_<System.Object>::Run()
extern "C"  Il2CppObject * Take__Run_m3277526330_gshared (Take__t2468273233 * __this, const MethodInfo* method);
#define Take__Run_m3277526330(__this, method) ((  Il2CppObject * (*) (Take__t2468273233 *, const MethodInfo*))Take__Run_m3277526330_gshared)(__this, method)
// System.Void UniRx.Operators.TakeObservable`1/Take_<System.Object>::Tick()
extern "C"  void Take__Tick_m810389003_gshared (Take__t2468273233 * __this, const MethodInfo* method);
#define Take__Tick_m810389003(__this, method) ((  void (*) (Take__t2468273233 *, const MethodInfo*))Take__Tick_m810389003_gshared)(__this, method)
// System.Void UniRx.Operators.TakeObservable`1/Take_<System.Object>::OnNext(T)
extern "C"  void Take__OnNext_m3600713726_gshared (Take__t2468273233 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Take__OnNext_m3600713726(__this, ___value0, method) ((  void (*) (Take__t2468273233 *, Il2CppObject *, const MethodInfo*))Take__OnNext_m3600713726_gshared)(__this, ___value0, method)
// System.Void UniRx.Operators.TakeObservable`1/Take_<System.Object>::OnError(System.Exception)
extern "C"  void Take__OnError_m1652483853_gshared (Take__t2468273233 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method);
#define Take__OnError_m1652483853(__this, ___error0, method) ((  void (*) (Take__t2468273233 *, Exception_t1967233988 *, const MethodInfo*))Take__OnError_m1652483853_gshared)(__this, ___error0, method)
// System.Void UniRx.Operators.TakeObservable`1/Take_<System.Object>::OnCompleted()
extern "C"  void Take__OnCompleted_m688784864_gshared (Take__t2468273233 * __this, const MethodInfo* method);
#define Take__OnCompleted_m688784864(__this, method) ((  void (*) (Take__t2468273233 *, const MethodInfo*))Take__OnCompleted_m688784864_gshared)(__this, method)
