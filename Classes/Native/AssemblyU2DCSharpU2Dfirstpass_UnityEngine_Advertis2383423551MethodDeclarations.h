﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.Advertisements.MiniJSON.Json/Parser
struct Parser_t2383423552;
// System.String
struct String_t;
// System.Object
struct Il2CppObject;
// System.Collections.Generic.Dictionary`2<System.String,System.Object>
struct Dictionary_2_t2474804324;
// System.Collections.Generic.List`1<System.Object>
struct List_1_t1634065389;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String968488902.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityEngine_Advertisem80003545.h"

// System.Void UnityEngine.Advertisements.MiniJSON.Json/Parser::.ctor(System.String)
extern "C"  void Parser__ctor_m448531979 (Parser_t2383423552 * __this, String_t* ___jsonString0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Advertisements.MiniJSON.Json/Parser::IsWordBreak(System.Char)
extern "C"  bool Parser_IsWordBreak_m4083015937 (Il2CppObject * __this /* static, unused */, uint16_t ___c0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object UnityEngine.Advertisements.MiniJSON.Json/Parser::Parse(System.String)
extern "C"  Il2CppObject * Parser_Parse_m3258239493 (Il2CppObject * __this /* static, unused */, String_t* ___jsonString0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Advertisements.MiniJSON.Json/Parser::Dispose()
extern "C"  void Parser_Dispose_m2812578004 (Parser_t2383423552 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.Dictionary`2<System.String,System.Object> UnityEngine.Advertisements.MiniJSON.Json/Parser::ParseObject()
extern "C"  Dictionary_2_t2474804324 * Parser_ParseObject_m2400284235 (Parser_t2383423552 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<System.Object> UnityEngine.Advertisements.MiniJSON.Json/Parser::ParseArray()
extern "C"  List_1_t1634065389 * Parser_ParseArray_m3181970124 (Parser_t2383423552 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object UnityEngine.Advertisements.MiniJSON.Json/Parser::ParseValue()
extern "C"  Il2CppObject * Parser_ParseValue_m819626102 (Parser_t2383423552 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object UnityEngine.Advertisements.MiniJSON.Json/Parser::ParseByToken(UnityEngine.Advertisements.MiniJSON.Json/Parser/TOKEN)
extern "C"  Il2CppObject * Parser_ParseByToken_m3070971841 (Parser_t2383423552 * __this, int32_t ___token0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String UnityEngine.Advertisements.MiniJSON.Json/Parser::ParseString()
extern "C"  String_t* Parser_ParseString_m3784989692 (Parser_t2383423552 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object UnityEngine.Advertisements.MiniJSON.Json/Parser::ParseNumber()
extern "C"  Il2CppObject * Parser_ParseNumber_m3457349702 (Parser_t2383423552 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Advertisements.MiniJSON.Json/Parser::EatWhitespace()
extern "C"  void Parser_EatWhitespace_m3600468522 (Parser_t2383423552 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Char UnityEngine.Advertisements.MiniJSON.Json/Parser::get_PeekChar()
extern "C"  uint16_t Parser_get_PeekChar_m4058537321 (Parser_t2383423552 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Char UnityEngine.Advertisements.MiniJSON.Json/Parser::get_NextChar()
extern "C"  uint16_t Parser_get_NextChar_m2602820865 (Parser_t2383423552 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String UnityEngine.Advertisements.MiniJSON.Json/Parser::get_NextWord()
extern "C"  String_t* Parser_get_NextWord_m689191536 (Parser_t2383423552 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Advertisements.MiniJSON.Json/Parser/TOKEN UnityEngine.Advertisements.MiniJSON.Json/Parser::get_NextToken()
extern "C"  int32_t Parser_get_NextToken_m2157722235 (Parser_t2383423552 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
