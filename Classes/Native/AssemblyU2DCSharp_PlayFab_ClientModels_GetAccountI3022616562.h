﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// PlayFab.ClientModels.UserAccountInfo
struct UserAccountInfo_t960776096;

#include "AssemblyU2DCSharp_PlayFab_Internal_PlayFabResultCom379512675.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PlayFab.ClientModels.GetAccountInfoResult
struct  GetAccountInfoResult_t3022616562  : public PlayFabResultCommon_t379512675
{
public:
	// PlayFab.ClientModels.UserAccountInfo PlayFab.ClientModels.GetAccountInfoResult::<AccountInfo>k__BackingField
	UserAccountInfo_t960776096 * ___U3CAccountInfoU3Ek__BackingField_2;

public:
	inline static int32_t get_offset_of_U3CAccountInfoU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(GetAccountInfoResult_t3022616562, ___U3CAccountInfoU3Ek__BackingField_2)); }
	inline UserAccountInfo_t960776096 * get_U3CAccountInfoU3Ek__BackingField_2() const { return ___U3CAccountInfoU3Ek__BackingField_2; }
	inline UserAccountInfo_t960776096 ** get_address_of_U3CAccountInfoU3Ek__BackingField_2() { return &___U3CAccountInfoU3Ek__BackingField_2; }
	inline void set_U3CAccountInfoU3Ek__BackingField_2(UserAccountInfo_t960776096 * value)
	{
		___U3CAccountInfoU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier(&___U3CAccountInfoU3Ek__BackingField_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
