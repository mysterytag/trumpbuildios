﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.PlayFabClientAPI/LinkGoogleAccountRequestCallback
struct LinkGoogleAccountRequestCallback_t2572844250;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;
// PlayFab.ClientModels.LinkGoogleAccountRequest
struct LinkGoogleAccountRequest_t117459333;
// System.IAsyncResult
struct IAsyncResult_t537683269;
// System.AsyncCallback
struct AsyncCallback_t1363551830;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_IntPtr676692020.h"
#include "mscorlib_System_String968488902.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_LinkGoogleAc117459333.h"
#include "mscorlib_System_AsyncCallback1363551830.h"

// System.Void PlayFab.PlayFabClientAPI/LinkGoogleAccountRequestCallback::.ctor(System.Object,System.IntPtr)
extern "C"  void LinkGoogleAccountRequestCallback__ctor_m53483721 (LinkGoogleAccountRequestCallback_t2572844250 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/LinkGoogleAccountRequestCallback::Invoke(System.String,System.Int32,PlayFab.ClientModels.LinkGoogleAccountRequest,System.Object)
extern "C"  void LinkGoogleAccountRequestCallback_Invoke_m4125720991 (LinkGoogleAccountRequestCallback_t2572844250 * __this, String_t* ___urlPath0, int32_t ___callId1, LinkGoogleAccountRequest_t117459333 * ___request2, Il2CppObject * ___customData3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
extern "C" void pinvoke_delegate_wrapper_LinkGoogleAccountRequestCallback_t2572844250(Il2CppObject* delegate, String_t* ___urlPath0, int32_t ___callId1, LinkGoogleAccountRequest_t117459333 * ___request2, Il2CppObject * ___customData3);
// System.IAsyncResult PlayFab.PlayFabClientAPI/LinkGoogleAccountRequestCallback::BeginInvoke(System.String,System.Int32,PlayFab.ClientModels.LinkGoogleAccountRequest,System.Object,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * LinkGoogleAccountRequestCallback_BeginInvoke_m1245519642 (LinkGoogleAccountRequestCallback_t2572844250 * __this, String_t* ___urlPath0, int32_t ___callId1, LinkGoogleAccountRequest_t117459333 * ___request2, Il2CppObject * ___customData3, AsyncCallback_t1363551830 * ___callback4, Il2CppObject * ___object5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/LinkGoogleAccountRequestCallback::EndInvoke(System.IAsyncResult)
extern "C"  void LinkGoogleAccountRequestCallback_EndInvoke_m653875033 (LinkGoogleAccountRequestCallback_t2572844250 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
