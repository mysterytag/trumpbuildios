﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "System_System_Collections_Generic_LinkedList_1_gen2577235966MethodDeclarations.h"

// System.Void System.Collections.Generic.LinkedList`1<System.IDisposable>::.ctor()
#define LinkedList_1__ctor_m3255264475(__this, method) ((  void (*) (LinkedList_1_t3369050920 *, const MethodInfo*))LinkedList_1__ctor_m2955457271_gshared)(__this, method)
// System.Void System.Collections.Generic.LinkedList`1<System.IDisposable>::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
#define LinkedList_1__ctor_m2571716508(__this, ___info0, ___context1, method) ((  void (*) (LinkedList_1_t3369050920 *, SerializationInfo_t2995724695 *, StreamingContext_t986364934 , const MethodInfo*))LinkedList_1__ctor_m3369579448_gshared)(__this, ___info0, ___context1, method)
// System.Void System.Collections.Generic.LinkedList`1<System.IDisposable>::System.Collections.Generic.ICollection<T>.Add(T)
#define LinkedList_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m2229720068(__this, ___value0, method) ((  void (*) (LinkedList_1_t3369050920 *, Il2CppObject *, const MethodInfo*))LinkedList_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m3576108392_gshared)(__this, ___value0, method)
// System.Void System.Collections.Generic.LinkedList`1<System.IDisposable>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define LinkedList_1_System_Collections_ICollection_CopyTo_m385941705(__this, ___array0, ___index1, method) ((  void (*) (LinkedList_1_t3369050920 *, Il2CppArray *, int32_t, const MethodInfo*))LinkedList_1_System_Collections_ICollection_CopyTo_m2331638317_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.LinkedList`1<System.IDisposable>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
#define LinkedList_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m3377259557(__this, method) ((  Il2CppObject* (*) (LinkedList_1_t3369050920 *, const MethodInfo*))LinkedList_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m2027502985_gshared)(__this, method)
// System.Collections.IEnumerator System.Collections.Generic.LinkedList`1<System.IDisposable>::System.Collections.IEnumerable.GetEnumerator()
#define LinkedList_1_System_Collections_IEnumerable_GetEnumerator_m1458393348(__this, method) ((  Il2CppObject * (*) (LinkedList_1_t3369050920 *, const MethodInfo*))LinkedList_1_System_Collections_IEnumerable_GetEnumerator_m51916412_gshared)(__this, method)
// System.Boolean System.Collections.Generic.LinkedList`1<System.IDisposable>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
#define LinkedList_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m1233707624(__this, method) ((  bool (*) (LinkedList_1_t3369050920 *, const MethodInfo*))LinkedList_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m2230847288_gshared)(__this, method)
// System.Boolean System.Collections.Generic.LinkedList`1<System.IDisposable>::System.Collections.ICollection.get_IsSynchronized()
#define LinkedList_1_System_Collections_ICollection_get_IsSynchronized_m4189010645(__this, method) ((  bool (*) (LinkedList_1_t3369050920 *, const MethodInfo*))LinkedList_1_System_Collections_ICollection_get_IsSynchronized_m683991045_gshared)(__this, method)
// System.Object System.Collections.Generic.LinkedList`1<System.IDisposable>::System.Collections.ICollection.get_SyncRoot()
#define LinkedList_1_System_Collections_ICollection_get_SyncRoot_m1354294259(__this, method) ((  Il2CppObject * (*) (LinkedList_1_t3369050920 *, const MethodInfo*))LinkedList_1_System_Collections_ICollection_get_SyncRoot_m573420165_gshared)(__this, method)
// System.Void System.Collections.Generic.LinkedList`1<System.IDisposable>::VerifyReferencedNode(System.Collections.Generic.LinkedListNode`1<T>)
#define LinkedList_1_VerifyReferencedNode_m2021559088(__this, ___node0, method) ((  void (*) (LinkedList_1_t3369050920 *, LinkedListNode_1_t1330911994 *, const MethodInfo*))LinkedList_1_VerifyReferencedNode_m3939775124_gshared)(__this, ___node0, method)
// System.Collections.Generic.LinkedListNode`1<T> System.Collections.Generic.LinkedList`1<System.IDisposable>::AddBefore(System.Collections.Generic.LinkedListNode`1<T>,T)
#define LinkedList_1_AddBefore_m555210582(__this, ___node0, ___value1, method) ((  LinkedListNode_1_t1330911994 * (*) (LinkedList_1_t3369050920 *, LinkedListNode_1_t1330911994 *, Il2CppObject *, const MethodInfo*))LinkedList_1_AddBefore_m1086189582_gshared)(__this, ___node0, ___value1, method)
// System.Collections.Generic.LinkedListNode`1<T> System.Collections.Generic.LinkedList`1<System.IDisposable>::AddLast(T)
#define LinkedList_1_AddLast_m2922662396(__this, ___value0, method) ((  LinkedListNode_1_t1330911994 * (*) (LinkedList_1_t3369050920 *, Il2CppObject *, const MethodInfo*))LinkedList_1_AddLast_m4070107716_gshared)(__this, ___value0, method)
// System.Void System.Collections.Generic.LinkedList`1<System.IDisposable>::Clear()
#define LinkedList_1_Clear_m661397766(__this, method) ((  void (*) (LinkedList_1_t3369050920 *, const MethodInfo*))LinkedList_1_Clear_m361590562_gshared)(__this, method)
// System.Boolean System.Collections.Generic.LinkedList`1<System.IDisposable>::Contains(T)
#define LinkedList_1_Contains_m967396748(__this, ___value0, method) ((  bool (*) (LinkedList_1_t3369050920 *, Il2CppObject *, const MethodInfo*))LinkedList_1_Contains_m3484410556_gshared)(__this, ___value0, method)
// System.Void System.Collections.Generic.LinkedList`1<System.IDisposable>::CopyTo(T[],System.Int32)
#define LinkedList_1_CopyTo_m3825005364(__this, ___array0, ___index1, method) ((  void (*) (LinkedList_1_t3369050920 *, IDisposableU5BU5D_t165183403*, int32_t, const MethodInfo*))LinkedList_1_CopyTo_m3470139544_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.LinkedListNode`1<T> System.Collections.Generic.LinkedList`1<System.IDisposable>::Find(T)
#define LinkedList_1_Find_m731659310(__this, ___value0, method) ((  LinkedListNode_1_t1330911994 * (*) (LinkedList_1_t3369050920 *, Il2CppObject *, const MethodInfo*))LinkedList_1_Find_m2643247334_gshared)(__this, ___value0, method)
// System.Collections.Generic.LinkedList`1/Enumerator<T> System.Collections.Generic.LinkedList`1<System.IDisposable>::GetEnumerator()
#define LinkedList_1_GetEnumerator_m4107050716(__this, method) ((  Enumerator_t511663336  (*) (LinkedList_1_t3369050920 *, const MethodInfo*))LinkedList_1_GetEnumerator_m3713737734_gshared)(__this, method)
// System.Void System.Collections.Generic.LinkedList`1<System.IDisposable>::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
#define LinkedList_1_GetObjectData_m352793337(__this, ___info0, ___context1, method) ((  void (*) (LinkedList_1_t3369050920 *, SerializationInfo_t2995724695 *, StreamingContext_t986364934 , const MethodInfo*))LinkedList_1_GetObjectData_m3974480661_gshared)(__this, ___info0, ___context1, method)
// System.Void System.Collections.Generic.LinkedList`1<System.IDisposable>::OnDeserialization(System.Object)
#define LinkedList_1_OnDeserialization_m3644791051(__this, ___sender0, method) ((  void (*) (LinkedList_1_t3369050920 *, Il2CppObject *, const MethodInfo*))LinkedList_1_OnDeserialization_m3445006959_gshared)(__this, ___sender0, method)
// System.Boolean System.Collections.Generic.LinkedList`1<System.IDisposable>::Remove(T)
#define LinkedList_1_Remove_m1403781255(__this, ___value0, method) ((  bool (*) (LinkedList_1_t3369050920 *, Il2CppObject *, const MethodInfo*))LinkedList_1_Remove_m3283493303_gshared)(__this, ___value0, method)
// System.Void System.Collections.Generic.LinkedList`1<System.IDisposable>::Remove(System.Collections.Generic.LinkedListNode`1<T>)
#define LinkedList_1_Remove_m2973981888(__this, ___node0, method) ((  void (*) (LinkedList_1_t3369050920 *, LinkedListNode_1_t1330911994 *, const MethodInfo*))LinkedList_1_Remove_m4034790180_gshared)(__this, ___node0, method)
// System.Void System.Collections.Generic.LinkedList`1<System.IDisposable>::RemoveFirst()
#define LinkedList_1_RemoveFirst_m1642584453(__this, method) ((  void (*) (LinkedList_1_t3369050920 *, const MethodInfo*))LinkedList_1_RemoveFirst_m1652892321_gshared)(__this, method)
// System.Void System.Collections.Generic.LinkedList`1<System.IDisposable>::RemoveLast()
#define LinkedList_1_RemoveLast_m2434159043(__this, method) ((  void (*) (LinkedList_1_t3369050920 *, const MethodInfo*))LinkedList_1_RemoveLast_m2573038887_gshared)(__this, method)
// System.Int32 System.Collections.Generic.LinkedList`1<System.IDisposable>::get_Count()
#define LinkedList_1_get_Count_m2339041167(__this, method) ((  int32_t (*) (LinkedList_1_t3369050920 *, const MethodInfo*))LinkedList_1_get_Count_m1368924491_gshared)(__this, method)
// System.Collections.Generic.LinkedListNode`1<T> System.Collections.Generic.LinkedList`1<System.IDisposable>::get_First()
#define LinkedList_1_get_First_m2067521234(__this, method) ((  LinkedListNode_1_t1330911994 * (*) (LinkedList_1_t3369050920 *, const MethodInfo*))LinkedList_1_get_First_m3278587786_gshared)(__this, method)
// System.Collections.Generic.LinkedListNode`1<T> System.Collections.Generic.LinkedList`1<System.IDisposable>::get_Last()
#define LinkedList_1_get_Last_m3417698006(__this, method) ((  LinkedListNode_1_t1330911994 * (*) (LinkedList_1_t3369050920 *, const MethodInfo*))LinkedList_1_get_Last_m270176030_gshared)(__this, method)
