﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SponsorPay.SPUser/JsonResponse`1<System.Nullable`1<System.Double>>
struct JsonResponse_1_t448454793;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String968488902.h"
#include "mscorlib_System_Nullable_1_gen3420554522.h"

// System.Void SponsorPay.SPUser/JsonResponse`1<System.Nullable`1<System.Double>>::.ctor()
extern "C"  void JsonResponse_1__ctor_m97221841_gshared (JsonResponse_1_t448454793 * __this, const MethodInfo* method);
#define JsonResponse_1__ctor_m97221841(__this, method) ((  void (*) (JsonResponse_1_t448454793 *, const MethodInfo*))JsonResponse_1__ctor_m97221841_gshared)(__this, method)
// System.String SponsorPay.SPUser/JsonResponse`1<System.Nullable`1<System.Double>>::get_key()
extern "C"  String_t* JsonResponse_1_get_key_m823265730_gshared (JsonResponse_1_t448454793 * __this, const MethodInfo* method);
#define JsonResponse_1_get_key_m823265730(__this, method) ((  String_t* (*) (JsonResponse_1_t448454793 *, const MethodInfo*))JsonResponse_1_get_key_m823265730_gshared)(__this, method)
// System.Void SponsorPay.SPUser/JsonResponse`1<System.Nullable`1<System.Double>>::set_key(System.String)
extern "C"  void JsonResponse_1_set_key_m4265333649_gshared (JsonResponse_1_t448454793 * __this, String_t* ___value0, const MethodInfo* method);
#define JsonResponse_1_set_key_m4265333649(__this, ___value0, method) ((  void (*) (JsonResponse_1_t448454793 *, String_t*, const MethodInfo*))JsonResponse_1_set_key_m4265333649_gshared)(__this, ___value0, method)
// T SponsorPay.SPUser/JsonResponse`1<System.Nullable`1<System.Double>>::get_value()
extern "C"  Nullable_1_t3420554522  JsonResponse_1_get_value_m3940125432_gshared (JsonResponse_1_t448454793 * __this, const MethodInfo* method);
#define JsonResponse_1_get_value_m3940125432(__this, method) ((  Nullable_1_t3420554522  (*) (JsonResponse_1_t448454793 *, const MethodInfo*))JsonResponse_1_get_value_m3940125432_gshared)(__this, method)
// System.Void SponsorPay.SPUser/JsonResponse`1<System.Nullable`1<System.Double>>::set_value(T)
extern "C"  void JsonResponse_1_set_value_m3066479707_gshared (JsonResponse_1_t448454793 * __this, Nullable_1_t3420554522  ___value0, const MethodInfo* method);
#define JsonResponse_1_set_value_m3066479707(__this, ___value0, method) ((  void (*) (JsonResponse_1_t448454793 *, Nullable_1_t3420554522 , const MethodInfo*))JsonResponse_1_set_value_m3066479707_gshared)(__this, ___value0, method)
// System.String SponsorPay.SPUser/JsonResponse`1<System.Nullable`1<System.Double>>::get_error()
extern "C"  String_t* JsonResponse_1_get_error_m222230187_gshared (JsonResponse_1_t448454793 * __this, const MethodInfo* method);
#define JsonResponse_1_get_error_m222230187(__this, method) ((  String_t* (*) (JsonResponse_1_t448454793 *, const MethodInfo*))JsonResponse_1_get_error_m222230187_gshared)(__this, method)
// System.Void SponsorPay.SPUser/JsonResponse`1<System.Nullable`1<System.Double>>::set_error(System.String)
extern "C"  void JsonResponse_1_set_error_m4242696456_gshared (JsonResponse_1_t448454793 * __this, String_t* ___value0, const MethodInfo* method);
#define JsonResponse_1_set_error_m4242696456(__this, ___value0, method) ((  void (*) (JsonResponse_1_t448454793 *, String_t*, const MethodInfo*))JsonResponse_1_set_error_m4242696456_gshared)(__this, ___value0, method)
