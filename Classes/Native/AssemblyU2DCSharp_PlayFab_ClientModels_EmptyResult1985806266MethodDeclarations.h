﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.ClientModels.EmptyResult
struct EmptyResult_t1985806266;

#include "codegen/il2cpp-codegen.h"

// System.Void PlayFab.ClientModels.EmptyResult::.ctor()
extern "C"  void EmptyResult__ctor_m1265615919 (EmptyResult_t1985806266 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
