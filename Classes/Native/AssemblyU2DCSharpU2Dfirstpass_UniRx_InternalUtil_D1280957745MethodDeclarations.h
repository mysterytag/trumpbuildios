﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.InternalUtil.DisposedObserver`1<UniRx.Tuple`2<System.Object,UnityEngine.NetworkMessageInfo>>
struct DisposedObserver_1_t1280957745;
// System.Exception
struct Exception_t1967233988;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Exception1967233988.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Tuple_2_gen2106500283.h"

// System.Void UniRx.InternalUtil.DisposedObserver`1<UniRx.Tuple`2<System.Object,UnityEngine.NetworkMessageInfo>>::.ctor()
extern "C"  void DisposedObserver_1__ctor_m772898864_gshared (DisposedObserver_1_t1280957745 * __this, const MethodInfo* method);
#define DisposedObserver_1__ctor_m772898864(__this, method) ((  void (*) (DisposedObserver_1_t1280957745 *, const MethodInfo*))DisposedObserver_1__ctor_m772898864_gshared)(__this, method)
// System.Void UniRx.InternalUtil.DisposedObserver`1<UniRx.Tuple`2<System.Object,UnityEngine.NetworkMessageInfo>>::.cctor()
extern "C"  void DisposedObserver_1__cctor_m2002932093_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define DisposedObserver_1__cctor_m2002932093(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))DisposedObserver_1__cctor_m2002932093_gshared)(__this /* static, unused */, method)
// System.Void UniRx.InternalUtil.DisposedObserver`1<UniRx.Tuple`2<System.Object,UnityEngine.NetworkMessageInfo>>::OnCompleted()
extern "C"  void DisposedObserver_1_OnCompleted_m386387898_gshared (DisposedObserver_1_t1280957745 * __this, const MethodInfo* method);
#define DisposedObserver_1_OnCompleted_m386387898(__this, method) ((  void (*) (DisposedObserver_1_t1280957745 *, const MethodInfo*))DisposedObserver_1_OnCompleted_m386387898_gshared)(__this, method)
// System.Void UniRx.InternalUtil.DisposedObserver`1<UniRx.Tuple`2<System.Object,UnityEngine.NetworkMessageInfo>>::OnError(System.Exception)
extern "C"  void DisposedObserver_1_OnError_m514243047_gshared (DisposedObserver_1_t1280957745 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method);
#define DisposedObserver_1_OnError_m514243047(__this, ___error0, method) ((  void (*) (DisposedObserver_1_t1280957745 *, Exception_t1967233988 *, const MethodInfo*))DisposedObserver_1_OnError_m514243047_gshared)(__this, ___error0, method)
// System.Void UniRx.InternalUtil.DisposedObserver`1<UniRx.Tuple`2<System.Object,UnityEngine.NetworkMessageInfo>>::OnNext(T)
extern "C"  void DisposedObserver_1_OnNext_m2557169368_gshared (DisposedObserver_1_t1280957745 * __this, Tuple_2_t2106500283  ___value0, const MethodInfo* method);
#define DisposedObserver_1_OnNext_m2557169368(__this, ___value0, method) ((  void (*) (DisposedObserver_1_t1280957745 *, Tuple_2_t2106500283 , const MethodInfo*))DisposedObserver_1_OnNext_m2557169368_gshared)(__this, ___value0, method)
