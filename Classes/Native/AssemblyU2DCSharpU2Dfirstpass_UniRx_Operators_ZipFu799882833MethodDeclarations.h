﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.ZipFunc`4<System.Object,System.Object,System.Object,System.Object>
struct ZipFunc_4_t799882833;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t537683269;
// System.AsyncCallback
struct AsyncCallback_t1363551830;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_IntPtr676692020.h"
#include "mscorlib_System_AsyncCallback1363551830.h"

// System.Void UniRx.Operators.ZipFunc`4<System.Object,System.Object,System.Object,System.Object>::.ctor(System.Object,System.IntPtr)
extern "C"  void ZipFunc_4__ctor_m2471014391_gshared (ZipFunc_4_t799882833 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method);
#define ZipFunc_4__ctor_m2471014391(__this, ___object0, ___method1, method) ((  void (*) (ZipFunc_4_t799882833 *, Il2CppObject *, IntPtr_t, const MethodInfo*))ZipFunc_4__ctor_m2471014391_gshared)(__this, ___object0, ___method1, method)
// TR UniRx.Operators.ZipFunc`4<System.Object,System.Object,System.Object,System.Object>::Invoke(T1,T2,T3)
extern "C"  Il2CppObject * ZipFunc_4_Invoke_m1801376154_gshared (ZipFunc_4_t799882833 * __this, Il2CppObject * ___arg10, Il2CppObject * ___arg21, Il2CppObject * ___arg32, const MethodInfo* method);
#define ZipFunc_4_Invoke_m1801376154(__this, ___arg10, ___arg21, ___arg32, method) ((  Il2CppObject * (*) (ZipFunc_4_t799882833 *, Il2CppObject *, Il2CppObject *, Il2CppObject *, const MethodInfo*))ZipFunc_4_Invoke_m1801376154_gshared)(__this, ___arg10, ___arg21, ___arg32, method)
// System.IAsyncResult UniRx.Operators.ZipFunc`4<System.Object,System.Object,System.Object,System.Object>::BeginInvoke(T1,T2,T3,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * ZipFunc_4_BeginInvoke_m3226986410_gshared (ZipFunc_4_t799882833 * __this, Il2CppObject * ___arg10, Il2CppObject * ___arg21, Il2CppObject * ___arg32, AsyncCallback_t1363551830 * ___callback3, Il2CppObject * ___object4, const MethodInfo* method);
#define ZipFunc_4_BeginInvoke_m3226986410(__this, ___arg10, ___arg21, ___arg32, ___callback3, ___object4, method) ((  Il2CppObject * (*) (ZipFunc_4_t799882833 *, Il2CppObject *, Il2CppObject *, Il2CppObject *, AsyncCallback_t1363551830 *, Il2CppObject *, const MethodInfo*))ZipFunc_4_BeginInvoke_m3226986410_gshared)(__this, ___arg10, ___arg21, ___arg32, ___callback3, ___object4, method)
// TR UniRx.Operators.ZipFunc`4<System.Object,System.Object,System.Object,System.Object>::EndInvoke(System.IAsyncResult)
extern "C"  Il2CppObject * ZipFunc_4_EndInvoke_m2106776686_gshared (ZipFunc_4_t799882833 * __this, Il2CppObject * ___result0, const MethodInfo* method);
#define ZipFunc_4_EndInvoke_m2106776686(__this, ___result0, method) ((  Il2CppObject * (*) (ZipFunc_4_t799882833 *, Il2CppObject *, const MethodInfo*))ZipFunc_4_EndInvoke_m2106776686_gshared)(__this, ___result0, method)
