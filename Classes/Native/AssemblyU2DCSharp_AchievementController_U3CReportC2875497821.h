﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// System.Object
struct Il2CppObject;
// AchievementController
struct AchievementController_t3677499403;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AchievementController/<ReportCoroutine>c__Iterator8
struct  U3CReportCoroutineU3Ec__Iterator8_t2875497821  : public Il2CppObject
{
public:
	// System.Single AchievementController/<ReportCoroutine>c__Iterator8::<interval>__0
	float ___U3CintervalU3E__0_0;
	// System.String AchievementController/<ReportCoroutine>c__Iterator8::<id>__1
	String_t* ___U3CidU3E__1_1;
	// System.Int32 AchievementController/<ReportCoroutine>c__Iterator8::$PC
	int32_t ___U24PC_2;
	// System.Object AchievementController/<ReportCoroutine>c__Iterator8::$current
	Il2CppObject * ___U24current_3;
	// AchievementController AchievementController/<ReportCoroutine>c__Iterator8::<>f__this
	AchievementController_t3677499403 * ___U3CU3Ef__this_4;

public:
	inline static int32_t get_offset_of_U3CintervalU3E__0_0() { return static_cast<int32_t>(offsetof(U3CReportCoroutineU3Ec__Iterator8_t2875497821, ___U3CintervalU3E__0_0)); }
	inline float get_U3CintervalU3E__0_0() const { return ___U3CintervalU3E__0_0; }
	inline float* get_address_of_U3CintervalU3E__0_0() { return &___U3CintervalU3E__0_0; }
	inline void set_U3CintervalU3E__0_0(float value)
	{
		___U3CintervalU3E__0_0 = value;
	}

	inline static int32_t get_offset_of_U3CidU3E__1_1() { return static_cast<int32_t>(offsetof(U3CReportCoroutineU3Ec__Iterator8_t2875497821, ___U3CidU3E__1_1)); }
	inline String_t* get_U3CidU3E__1_1() const { return ___U3CidU3E__1_1; }
	inline String_t** get_address_of_U3CidU3E__1_1() { return &___U3CidU3E__1_1; }
	inline void set_U3CidU3E__1_1(String_t* value)
	{
		___U3CidU3E__1_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CidU3E__1_1, value);
	}

	inline static int32_t get_offset_of_U24PC_2() { return static_cast<int32_t>(offsetof(U3CReportCoroutineU3Ec__Iterator8_t2875497821, ___U24PC_2)); }
	inline int32_t get_U24PC_2() const { return ___U24PC_2; }
	inline int32_t* get_address_of_U24PC_2() { return &___U24PC_2; }
	inline void set_U24PC_2(int32_t value)
	{
		___U24PC_2 = value;
	}

	inline static int32_t get_offset_of_U24current_3() { return static_cast<int32_t>(offsetof(U3CReportCoroutineU3Ec__Iterator8_t2875497821, ___U24current_3)); }
	inline Il2CppObject * get_U24current_3() const { return ___U24current_3; }
	inline Il2CppObject ** get_address_of_U24current_3() { return &___U24current_3; }
	inline void set_U24current_3(Il2CppObject * value)
	{
		___U24current_3 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_3, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__this_4() { return static_cast<int32_t>(offsetof(U3CReportCoroutineU3Ec__Iterator8_t2875497821, ___U3CU3Ef__this_4)); }
	inline AchievementController_t3677499403 * get_U3CU3Ef__this_4() const { return ___U3CU3Ef__this_4; }
	inline AchievementController_t3677499403 ** get_address_of_U3CU3Ef__this_4() { return &___U3CU3Ef__this_4; }
	inline void set_U3CU3Ef__this_4(AchievementController_t3677499403 * value)
	{
		___U3CU3Ef__this_4 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__this_4, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
