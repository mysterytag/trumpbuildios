﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.InternalUtil.DisposedObserver`1<UnityEngine.Quaternion>
struct DisposedObserver_1_t1066173441;
// System.Exception
struct Exception_t1967233988;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Exception1967233988.h"
#include "UnityEngine_UnityEngine_Quaternion1891715979.h"

// System.Void UniRx.InternalUtil.DisposedObserver`1<UnityEngine.Quaternion>::.ctor()
extern "C"  void DisposedObserver_1__ctor_m2005339719_gshared (DisposedObserver_1_t1066173441 * __this, const MethodInfo* method);
#define DisposedObserver_1__ctor_m2005339719(__this, method) ((  void (*) (DisposedObserver_1_t1066173441 *, const MethodInfo*))DisposedObserver_1__ctor_m2005339719_gshared)(__this, method)
// System.Void UniRx.InternalUtil.DisposedObserver`1<UnityEngine.Quaternion>::.cctor()
extern "C"  void DisposedObserver_1__cctor_m1553892934_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define DisposedObserver_1__cctor_m1553892934(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))DisposedObserver_1__cctor_m1553892934_gshared)(__this /* static, unused */, method)
// System.Void UniRx.InternalUtil.DisposedObserver`1<UnityEngine.Quaternion>::OnCompleted()
extern "C"  void DisposedObserver_1_OnCompleted_m709505169_gshared (DisposedObserver_1_t1066173441 * __this, const MethodInfo* method);
#define DisposedObserver_1_OnCompleted_m709505169(__this, method) ((  void (*) (DisposedObserver_1_t1066173441 *, const MethodInfo*))DisposedObserver_1_OnCompleted_m709505169_gshared)(__this, method)
// System.Void UniRx.InternalUtil.DisposedObserver`1<UnityEngine.Quaternion>::OnError(System.Exception)
extern "C"  void DisposedObserver_1_OnError_m3512963134_gshared (DisposedObserver_1_t1066173441 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method);
#define DisposedObserver_1_OnError_m3512963134(__this, ___error0, method) ((  void (*) (DisposedObserver_1_t1066173441 *, Exception_t1967233988 *, const MethodInfo*))DisposedObserver_1_OnError_m3512963134_gshared)(__this, ___error0, method)
// System.Void UniRx.InternalUtil.DisposedObserver`1<UnityEngine.Quaternion>::OnNext(T)
extern "C"  void DisposedObserver_1_OnNext_m1521857327_gshared (DisposedObserver_1_t1066173441 * __this, Quaternion_t1891715979  ___value0, const MethodInfo* method);
#define DisposedObserver_1_OnNext_m1521857327(__this, ___value0, method) ((  void (*) (DisposedObserver_1_t1066173441 *, Quaternion_t1891715979 , const MethodInfo*))DisposedObserver_1_OnNext_m1521857327_gshared)(__this, ___value0, method)
