﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Ionic.Zlib.InflateManager
struct InflateManager_t2918548787;
// Ionic.Zlib.ZlibCodec
struct ZlibCodec_t3910383704;
// System.Byte[]
struct ByteU5BU5D_t58506160;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Ionic_Zlib_ZlibCodec3910383704.h"
#include "AssemblyU2DCSharp_Ionic_Zlib_FlushType3984958891.h"

// System.Void Ionic.Zlib.InflateManager::.ctor()
extern "C"  void InflateManager__ctor_m3587924686 (InflateManager_t2918548787 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Ionic.Zlib.InflateManager::.ctor(System.Boolean)
extern "C"  void InflateManager__ctor_m1245583301 (InflateManager_t2918548787 * __this, bool ___expectRfc1950HeaderBytes0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Ionic.Zlib.InflateManager::.cctor()
extern "C"  void InflateManager__cctor_m3369386655 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Ionic.Zlib.InflateManager::get_HandleRfc1950HeaderBytes()
extern "C"  bool InflateManager_get_HandleRfc1950HeaderBytes_m337463775 (InflateManager_t2918548787 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Ionic.Zlib.InflateManager::set_HandleRfc1950HeaderBytes(System.Boolean)
extern "C"  void InflateManager_set_HandleRfc1950HeaderBytes_m3865657022 (InflateManager_t2918548787 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Ionic.Zlib.InflateManager::Reset()
extern "C"  int32_t InflateManager_Reset_m1378348041 (InflateManager_t2918548787 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Ionic.Zlib.InflateManager::End()
extern "C"  int32_t InflateManager_End_m450014933 (InflateManager_t2918548787 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Ionic.Zlib.InflateManager::Initialize(Ionic.Zlib.ZlibCodec,System.Int32)
extern "C"  int32_t InflateManager_Initialize_m3740539059 (InflateManager_t2918548787 * __this, ZlibCodec_t3910383704 * ___codec0, int32_t ___w1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Ionic.Zlib.InflateManager::Inflate(Ionic.Zlib.FlushType)
extern "C"  int32_t InflateManager_Inflate_m2224223994 (InflateManager_t2918548787 * __this, int32_t ___flush0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Ionic.Zlib.InflateManager::SetDictionary(System.Byte[])
extern "C"  int32_t InflateManager_SetDictionary_m2255904503 (InflateManager_t2918548787 * __this, ByteU5BU5D_t58506160* ___dictionary0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Ionic.Zlib.InflateManager::Sync()
extern "C"  int32_t InflateManager_Sync_m1476882499 (InflateManager_t2918548787 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Ionic.Zlib.InflateManager::SyncPoint(Ionic.Zlib.ZlibCodec)
extern "C"  int32_t InflateManager_SyncPoint_m35568347 (InflateManager_t2918548787 * __this, ZlibCodec_t3910383704 * ___z0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
