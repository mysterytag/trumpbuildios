﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.SelectManyObservable`2/SelectManyOuterObserver<System.Object,UniRx.Unit>
struct SelectManyOuterObserver_t1519697883;
// UniRx.Operators.SelectManyObservable`2<System.Object,UniRx.Unit>
struct SelectManyObservable_2_t2284173584;
// UniRx.IObserver`1<UniRx.Unit>
struct IObserver_1_t475317645;
// System.IDisposable
struct IDisposable_t1628921374;
// System.Object
struct Il2CppObject;
// System.Exception
struct Exception_t1967233988;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_Exception1967233988.h"

// System.Void UniRx.Operators.SelectManyObservable`2/SelectManyOuterObserver<System.Object,UniRx.Unit>::.ctor(UniRx.Operators.SelectManyObservable`2<TSource,TResult>,UniRx.IObserver`1<TResult>,System.IDisposable)
extern "C"  void SelectManyOuterObserver__ctor_m2234854385_gshared (SelectManyOuterObserver_t1519697883 * __this, SelectManyObservable_2_t2284173584 * ___parent0, Il2CppObject* ___observer1, Il2CppObject * ___cancel2, const MethodInfo* method);
#define SelectManyOuterObserver__ctor_m2234854385(__this, ___parent0, ___observer1, ___cancel2, method) ((  void (*) (SelectManyOuterObserver_t1519697883 *, SelectManyObservable_2_t2284173584 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))SelectManyOuterObserver__ctor_m2234854385_gshared)(__this, ___parent0, ___observer1, ___cancel2, method)
// System.IDisposable UniRx.Operators.SelectManyObservable`2/SelectManyOuterObserver<System.Object,UniRx.Unit>::Run()
extern "C"  Il2CppObject * SelectManyOuterObserver_Run_m3419845143_gshared (SelectManyOuterObserver_t1519697883 * __this, const MethodInfo* method);
#define SelectManyOuterObserver_Run_m3419845143(__this, method) ((  Il2CppObject * (*) (SelectManyOuterObserver_t1519697883 *, const MethodInfo*))SelectManyOuterObserver_Run_m3419845143_gshared)(__this, method)
// System.Void UniRx.Operators.SelectManyObservable`2/SelectManyOuterObserver<System.Object,UniRx.Unit>::OnNext(TSource)
extern "C"  void SelectManyOuterObserver_OnNext_m2039862230_gshared (SelectManyOuterObserver_t1519697883 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define SelectManyOuterObserver_OnNext_m2039862230(__this, ___value0, method) ((  void (*) (SelectManyOuterObserver_t1519697883 *, Il2CppObject *, const MethodInfo*))SelectManyOuterObserver_OnNext_m2039862230_gshared)(__this, ___value0, method)
// System.Void UniRx.Operators.SelectManyObservable`2/SelectManyOuterObserver<System.Object,UniRx.Unit>::OnError(System.Exception)
extern "C"  void SelectManyOuterObserver_OnError_m1452294080_gshared (SelectManyOuterObserver_t1519697883 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method);
#define SelectManyOuterObserver_OnError_m1452294080(__this, ___error0, method) ((  void (*) (SelectManyOuterObserver_t1519697883 *, Exception_t1967233988 *, const MethodInfo*))SelectManyOuterObserver_OnError_m1452294080_gshared)(__this, ___error0, method)
// System.Void UniRx.Operators.SelectManyObservable`2/SelectManyOuterObserver<System.Object,UniRx.Unit>::OnCompleted()
extern "C"  void SelectManyOuterObserver_OnCompleted_m4050686739_gshared (SelectManyOuterObserver_t1519697883 * __this, const MethodInfo* method);
#define SelectManyOuterObserver_OnCompleted_m4050686739(__this, method) ((  void (*) (SelectManyOuterObserver_t1519697883 *, const MethodInfo*))SelectManyOuterObserver_OnCompleted_m4050686739_gshared)(__this, method)
