﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.PlayFabClientAPI/<GetCharacterStatistics>c__AnonStoreyC8
struct U3CGetCharacterStatisticsU3Ec__AnonStoreyC8_t3144930672;
// PlayFab.CallRequestContainer
struct CallRequestContainer_t432318609;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharpU2Dfirstpass_PlayFab_CallRequestCo432318609.h"

// System.Void PlayFab.PlayFabClientAPI/<GetCharacterStatistics>c__AnonStoreyC8::.ctor()
extern "C"  void U3CGetCharacterStatisticsU3Ec__AnonStoreyC8__ctor_m2337304643 (U3CGetCharacterStatisticsU3Ec__AnonStoreyC8_t3144930672 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/<GetCharacterStatistics>c__AnonStoreyC8::<>m__B5(PlayFab.CallRequestContainer)
extern "C"  void U3CGetCharacterStatisticsU3Ec__AnonStoreyC8_U3CU3Em__B5_m3048592596 (U3CGetCharacterStatisticsU3Ec__AnonStoreyC8_t3144930672 * __this, CallRequestContainer_t432318609 * ___requestContainer0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
