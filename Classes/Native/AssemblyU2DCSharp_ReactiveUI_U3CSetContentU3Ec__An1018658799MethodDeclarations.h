﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ReactiveUI/<SetContent>c__AnonStorey122`1<System.Object>
struct U3CSetContentU3Ec__AnonStorey122_1_t1018658799;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"

// System.Void ReactiveUI/<SetContent>c__AnonStorey122`1<System.Object>::.ctor()
extern "C"  void U3CSetContentU3Ec__AnonStorey122_1__ctor_m57250055_gshared (U3CSetContentU3Ec__AnonStorey122_1_t1018658799 * __this, const MethodInfo* method);
#define U3CSetContentU3Ec__AnonStorey122_1__ctor_m57250055(__this, method) ((  void (*) (U3CSetContentU3Ec__AnonStorey122_1_t1018658799 *, const MethodInfo*))U3CSetContentU3Ec__AnonStorey122_1__ctor_m57250055_gshared)(__this, method)
// System.Void ReactiveUI/<SetContent>c__AnonStorey122`1<System.Object>::<>m__1B3(T)
extern "C"  void U3CSetContentU3Ec__AnonStorey122_1_U3CU3Em__1B3_m2420874858_gshared (U3CSetContentU3Ec__AnonStorey122_1_t1018658799 * __this, Il2CppObject * ___obj0, const MethodInfo* method);
#define U3CSetContentU3Ec__AnonStorey122_1_U3CU3Em__1B3_m2420874858(__this, ___obj0, method) ((  void (*) (U3CSetContentU3Ec__AnonStorey122_1_t1018658799 *, Il2CppObject *, const MethodInfo*))U3CSetContentU3Ec__AnonStorey122_1_U3CU3Em__1B3_m2420874858_gshared)(__this, ___obj0, method)
