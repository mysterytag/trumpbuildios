﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// System.Object
struct Il2CppObject;
// System.Net.HttpWebRequest
struct HttpWebRequest_t171953869;
// PlayFab.PlayFabError
struct PlayFabError_t750598646;
// System.Action`1<PlayFab.CallRequestContainer>
struct Action_1_t580771314;

#include "mscorlib_System_Object837106420.h"
#include "AssemblyU2DCSharpU2Dfirstpass_PlayFab_WebRequestTy2827971030.h"
#include "AssemblyU2DCSharpU2Dfirstpass_PlayFab_CallRequestC1456502626.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PlayFab.CallRequestContainer
struct  CallRequestContainer_t432318609  : public Il2CppObject
{
public:
	// PlayFab.WebRequestType PlayFab.CallRequestContainer::RequestType
	int32_t ___RequestType_0;
	// PlayFab.CallRequestContainer/RequestState PlayFab.CallRequestContainer::State
	int32_t ___State_1;
	// System.String PlayFab.CallRequestContainer::Url
	String_t* ___Url_2;
	// System.Int32 PlayFab.CallRequestContainer::CallId
	int32_t ___CallId_3;
	// System.String PlayFab.CallRequestContainer::Data
	String_t* ___Data_4;
	// System.String PlayFab.CallRequestContainer::AuthType
	String_t* ___AuthType_5;
	// System.String PlayFab.CallRequestContainer::AuthKey
	String_t* ___AuthKey_6;
	// System.Object PlayFab.CallRequestContainer::Request
	Il2CppObject * ___Request_7;
	// System.String PlayFab.CallRequestContainer::ResultStr
	String_t* ___ResultStr_8;
	// System.Object PlayFab.CallRequestContainer::CustomData
	Il2CppObject * ___CustomData_9;
	// System.Net.HttpWebRequest PlayFab.CallRequestContainer::HttpRequest
	HttpWebRequest_t171953869 * ___HttpRequest_10;
	// PlayFab.PlayFabError PlayFab.CallRequestContainer::Error
	PlayFabError_t750598646 * ___Error_11;
	// System.Action`1<PlayFab.CallRequestContainer> PlayFab.CallRequestContainer::Callback
	Action_1_t580771314 * ___Callback_12;

public:
	inline static int32_t get_offset_of_RequestType_0() { return static_cast<int32_t>(offsetof(CallRequestContainer_t432318609, ___RequestType_0)); }
	inline int32_t get_RequestType_0() const { return ___RequestType_0; }
	inline int32_t* get_address_of_RequestType_0() { return &___RequestType_0; }
	inline void set_RequestType_0(int32_t value)
	{
		___RequestType_0 = value;
	}

	inline static int32_t get_offset_of_State_1() { return static_cast<int32_t>(offsetof(CallRequestContainer_t432318609, ___State_1)); }
	inline int32_t get_State_1() const { return ___State_1; }
	inline int32_t* get_address_of_State_1() { return &___State_1; }
	inline void set_State_1(int32_t value)
	{
		___State_1 = value;
	}

	inline static int32_t get_offset_of_Url_2() { return static_cast<int32_t>(offsetof(CallRequestContainer_t432318609, ___Url_2)); }
	inline String_t* get_Url_2() const { return ___Url_2; }
	inline String_t** get_address_of_Url_2() { return &___Url_2; }
	inline void set_Url_2(String_t* value)
	{
		___Url_2 = value;
		Il2CppCodeGenWriteBarrier(&___Url_2, value);
	}

	inline static int32_t get_offset_of_CallId_3() { return static_cast<int32_t>(offsetof(CallRequestContainer_t432318609, ___CallId_3)); }
	inline int32_t get_CallId_3() const { return ___CallId_3; }
	inline int32_t* get_address_of_CallId_3() { return &___CallId_3; }
	inline void set_CallId_3(int32_t value)
	{
		___CallId_3 = value;
	}

	inline static int32_t get_offset_of_Data_4() { return static_cast<int32_t>(offsetof(CallRequestContainer_t432318609, ___Data_4)); }
	inline String_t* get_Data_4() const { return ___Data_4; }
	inline String_t** get_address_of_Data_4() { return &___Data_4; }
	inline void set_Data_4(String_t* value)
	{
		___Data_4 = value;
		Il2CppCodeGenWriteBarrier(&___Data_4, value);
	}

	inline static int32_t get_offset_of_AuthType_5() { return static_cast<int32_t>(offsetof(CallRequestContainer_t432318609, ___AuthType_5)); }
	inline String_t* get_AuthType_5() const { return ___AuthType_5; }
	inline String_t** get_address_of_AuthType_5() { return &___AuthType_5; }
	inline void set_AuthType_5(String_t* value)
	{
		___AuthType_5 = value;
		Il2CppCodeGenWriteBarrier(&___AuthType_5, value);
	}

	inline static int32_t get_offset_of_AuthKey_6() { return static_cast<int32_t>(offsetof(CallRequestContainer_t432318609, ___AuthKey_6)); }
	inline String_t* get_AuthKey_6() const { return ___AuthKey_6; }
	inline String_t** get_address_of_AuthKey_6() { return &___AuthKey_6; }
	inline void set_AuthKey_6(String_t* value)
	{
		___AuthKey_6 = value;
		Il2CppCodeGenWriteBarrier(&___AuthKey_6, value);
	}

	inline static int32_t get_offset_of_Request_7() { return static_cast<int32_t>(offsetof(CallRequestContainer_t432318609, ___Request_7)); }
	inline Il2CppObject * get_Request_7() const { return ___Request_7; }
	inline Il2CppObject ** get_address_of_Request_7() { return &___Request_7; }
	inline void set_Request_7(Il2CppObject * value)
	{
		___Request_7 = value;
		Il2CppCodeGenWriteBarrier(&___Request_7, value);
	}

	inline static int32_t get_offset_of_ResultStr_8() { return static_cast<int32_t>(offsetof(CallRequestContainer_t432318609, ___ResultStr_8)); }
	inline String_t* get_ResultStr_8() const { return ___ResultStr_8; }
	inline String_t** get_address_of_ResultStr_8() { return &___ResultStr_8; }
	inline void set_ResultStr_8(String_t* value)
	{
		___ResultStr_8 = value;
		Il2CppCodeGenWriteBarrier(&___ResultStr_8, value);
	}

	inline static int32_t get_offset_of_CustomData_9() { return static_cast<int32_t>(offsetof(CallRequestContainer_t432318609, ___CustomData_9)); }
	inline Il2CppObject * get_CustomData_9() const { return ___CustomData_9; }
	inline Il2CppObject ** get_address_of_CustomData_9() { return &___CustomData_9; }
	inline void set_CustomData_9(Il2CppObject * value)
	{
		___CustomData_9 = value;
		Il2CppCodeGenWriteBarrier(&___CustomData_9, value);
	}

	inline static int32_t get_offset_of_HttpRequest_10() { return static_cast<int32_t>(offsetof(CallRequestContainer_t432318609, ___HttpRequest_10)); }
	inline HttpWebRequest_t171953869 * get_HttpRequest_10() const { return ___HttpRequest_10; }
	inline HttpWebRequest_t171953869 ** get_address_of_HttpRequest_10() { return &___HttpRequest_10; }
	inline void set_HttpRequest_10(HttpWebRequest_t171953869 * value)
	{
		___HttpRequest_10 = value;
		Il2CppCodeGenWriteBarrier(&___HttpRequest_10, value);
	}

	inline static int32_t get_offset_of_Error_11() { return static_cast<int32_t>(offsetof(CallRequestContainer_t432318609, ___Error_11)); }
	inline PlayFabError_t750598646 * get_Error_11() const { return ___Error_11; }
	inline PlayFabError_t750598646 ** get_address_of_Error_11() { return &___Error_11; }
	inline void set_Error_11(PlayFabError_t750598646 * value)
	{
		___Error_11 = value;
		Il2CppCodeGenWriteBarrier(&___Error_11, value);
	}

	inline static int32_t get_offset_of_Callback_12() { return static_cast<int32_t>(offsetof(CallRequestContainer_t432318609, ___Callback_12)); }
	inline Action_1_t580771314 * get_Callback_12() const { return ___Callback_12; }
	inline Action_1_t580771314 ** get_address_of_Callback_12() { return &___Callback_12; }
	inline void set_Callback_12(Action_1_t580771314 * value)
	{
		___Callback_12 = value;
		Il2CppCodeGenWriteBarrier(&___Callback_12, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
