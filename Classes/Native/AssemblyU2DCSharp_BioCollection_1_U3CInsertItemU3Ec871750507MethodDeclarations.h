﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// BioCollection`1/<InsertItem>c__AnonStoreyF7<System.Object>
struct U3CInsertItemU3Ec__AnonStoreyF7_t871750507;

#include "codegen/il2cpp-codegen.h"

// System.Void BioCollection`1/<InsertItem>c__AnonStoreyF7<System.Object>::.ctor()
extern "C"  void U3CInsertItemU3Ec__AnonStoreyF7__ctor_m1955124201_gshared (U3CInsertItemU3Ec__AnonStoreyF7_t871750507 * __this, const MethodInfo* method);
#define U3CInsertItemU3Ec__AnonStoreyF7__ctor_m1955124201(__this, method) ((  void (*) (U3CInsertItemU3Ec__AnonStoreyF7_t871750507 *, const MethodInfo*))U3CInsertItemU3Ec__AnonStoreyF7__ctor_m1955124201_gshared)(__this, method)
// System.Void BioCollection`1/<InsertItem>c__AnonStoreyF7<System.Object>::<>m__16B()
extern "C"  void U3CInsertItemU3Ec__AnonStoreyF7_U3CU3Em__16B_m3883537197_gshared (U3CInsertItemU3Ec__AnonStoreyF7_t871750507 * __this, const MethodInfo* method);
#define U3CInsertItemU3Ec__AnonStoreyF7_U3CU3Em__16B_m3883537197(__this, method) ((  void (*) (U3CInsertItemU3Ec__AnonStoreyF7_t871750507 *, const MethodInfo*))U3CInsertItemU3Ec__AnonStoreyF7_U3CU3Em__16B_m3883537197_gshared)(__this, method)
