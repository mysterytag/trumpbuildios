﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// System.Collections.Generic.List`1<System.String>
struct List_1_t1765447871;

#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_Nullable_1_gen212373688.h"
#include "mscorlib_System_Nullable_1_gen1438485399.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PlayFab.ClientModels.GameInfo
struct  GameInfo_t1767394288  : public Il2CppObject
{
public:
	// System.Nullable`1<PlayFab.ClientModels.Region> PlayFab.ClientModels.GameInfo::<Region>k__BackingField
	Nullable_1_t212373688  ___U3CRegionU3Ek__BackingField_0;
	// System.String PlayFab.ClientModels.GameInfo::<LobbyID>k__BackingField
	String_t* ___U3CLobbyIDU3Ek__BackingField_1;
	// System.String PlayFab.ClientModels.GameInfo::<BuildVersion>k__BackingField
	String_t* ___U3CBuildVersionU3Ek__BackingField_2;
	// System.String PlayFab.ClientModels.GameInfo::<GameMode>k__BackingField
	String_t* ___U3CGameModeU3Ek__BackingField_3;
	// System.String PlayFab.ClientModels.GameInfo::<StatisticName>k__BackingField
	String_t* ___U3CStatisticNameU3Ek__BackingField_4;
	// System.Nullable`1<System.Int32> PlayFab.ClientModels.GameInfo::<MaxPlayers>k__BackingField
	Nullable_1_t1438485399  ___U3CMaxPlayersU3Ek__BackingField_5;
	// System.Collections.Generic.List`1<System.String> PlayFab.ClientModels.GameInfo::<PlayerUserIds>k__BackingField
	List_1_t1765447871 * ___U3CPlayerUserIdsU3Ek__BackingField_6;
	// System.UInt32 PlayFab.ClientModels.GameInfo::<RunTime>k__BackingField
	uint32_t ___U3CRunTimeU3Ek__BackingField_7;
	// System.String PlayFab.ClientModels.GameInfo::<GameServerState>k__BackingField
	String_t* ___U3CGameServerStateU3Ek__BackingField_8;

public:
	inline static int32_t get_offset_of_U3CRegionU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(GameInfo_t1767394288, ___U3CRegionU3Ek__BackingField_0)); }
	inline Nullable_1_t212373688  get_U3CRegionU3Ek__BackingField_0() const { return ___U3CRegionU3Ek__BackingField_0; }
	inline Nullable_1_t212373688 * get_address_of_U3CRegionU3Ek__BackingField_0() { return &___U3CRegionU3Ek__BackingField_0; }
	inline void set_U3CRegionU3Ek__BackingField_0(Nullable_1_t212373688  value)
	{
		___U3CRegionU3Ek__BackingField_0 = value;
	}

	inline static int32_t get_offset_of_U3CLobbyIDU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(GameInfo_t1767394288, ___U3CLobbyIDU3Ek__BackingField_1)); }
	inline String_t* get_U3CLobbyIDU3Ek__BackingField_1() const { return ___U3CLobbyIDU3Ek__BackingField_1; }
	inline String_t** get_address_of_U3CLobbyIDU3Ek__BackingField_1() { return &___U3CLobbyIDU3Ek__BackingField_1; }
	inline void set_U3CLobbyIDU3Ek__BackingField_1(String_t* value)
	{
		___U3CLobbyIDU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CLobbyIDU3Ek__BackingField_1, value);
	}

	inline static int32_t get_offset_of_U3CBuildVersionU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(GameInfo_t1767394288, ___U3CBuildVersionU3Ek__BackingField_2)); }
	inline String_t* get_U3CBuildVersionU3Ek__BackingField_2() const { return ___U3CBuildVersionU3Ek__BackingField_2; }
	inline String_t** get_address_of_U3CBuildVersionU3Ek__BackingField_2() { return &___U3CBuildVersionU3Ek__BackingField_2; }
	inline void set_U3CBuildVersionU3Ek__BackingField_2(String_t* value)
	{
		___U3CBuildVersionU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier(&___U3CBuildVersionU3Ek__BackingField_2, value);
	}

	inline static int32_t get_offset_of_U3CGameModeU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(GameInfo_t1767394288, ___U3CGameModeU3Ek__BackingField_3)); }
	inline String_t* get_U3CGameModeU3Ek__BackingField_3() const { return ___U3CGameModeU3Ek__BackingField_3; }
	inline String_t** get_address_of_U3CGameModeU3Ek__BackingField_3() { return &___U3CGameModeU3Ek__BackingField_3; }
	inline void set_U3CGameModeU3Ek__BackingField_3(String_t* value)
	{
		___U3CGameModeU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier(&___U3CGameModeU3Ek__BackingField_3, value);
	}

	inline static int32_t get_offset_of_U3CStatisticNameU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(GameInfo_t1767394288, ___U3CStatisticNameU3Ek__BackingField_4)); }
	inline String_t* get_U3CStatisticNameU3Ek__BackingField_4() const { return ___U3CStatisticNameU3Ek__BackingField_4; }
	inline String_t** get_address_of_U3CStatisticNameU3Ek__BackingField_4() { return &___U3CStatisticNameU3Ek__BackingField_4; }
	inline void set_U3CStatisticNameU3Ek__BackingField_4(String_t* value)
	{
		___U3CStatisticNameU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier(&___U3CStatisticNameU3Ek__BackingField_4, value);
	}

	inline static int32_t get_offset_of_U3CMaxPlayersU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(GameInfo_t1767394288, ___U3CMaxPlayersU3Ek__BackingField_5)); }
	inline Nullable_1_t1438485399  get_U3CMaxPlayersU3Ek__BackingField_5() const { return ___U3CMaxPlayersU3Ek__BackingField_5; }
	inline Nullable_1_t1438485399 * get_address_of_U3CMaxPlayersU3Ek__BackingField_5() { return &___U3CMaxPlayersU3Ek__BackingField_5; }
	inline void set_U3CMaxPlayersU3Ek__BackingField_5(Nullable_1_t1438485399  value)
	{
		___U3CMaxPlayersU3Ek__BackingField_5 = value;
	}

	inline static int32_t get_offset_of_U3CPlayerUserIdsU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(GameInfo_t1767394288, ___U3CPlayerUserIdsU3Ek__BackingField_6)); }
	inline List_1_t1765447871 * get_U3CPlayerUserIdsU3Ek__BackingField_6() const { return ___U3CPlayerUserIdsU3Ek__BackingField_6; }
	inline List_1_t1765447871 ** get_address_of_U3CPlayerUserIdsU3Ek__BackingField_6() { return &___U3CPlayerUserIdsU3Ek__BackingField_6; }
	inline void set_U3CPlayerUserIdsU3Ek__BackingField_6(List_1_t1765447871 * value)
	{
		___U3CPlayerUserIdsU3Ek__BackingField_6 = value;
		Il2CppCodeGenWriteBarrier(&___U3CPlayerUserIdsU3Ek__BackingField_6, value);
	}

	inline static int32_t get_offset_of_U3CRunTimeU3Ek__BackingField_7() { return static_cast<int32_t>(offsetof(GameInfo_t1767394288, ___U3CRunTimeU3Ek__BackingField_7)); }
	inline uint32_t get_U3CRunTimeU3Ek__BackingField_7() const { return ___U3CRunTimeU3Ek__BackingField_7; }
	inline uint32_t* get_address_of_U3CRunTimeU3Ek__BackingField_7() { return &___U3CRunTimeU3Ek__BackingField_7; }
	inline void set_U3CRunTimeU3Ek__BackingField_7(uint32_t value)
	{
		___U3CRunTimeU3Ek__BackingField_7 = value;
	}

	inline static int32_t get_offset_of_U3CGameServerStateU3Ek__BackingField_8() { return static_cast<int32_t>(offsetof(GameInfo_t1767394288, ___U3CGameServerStateU3Ek__BackingField_8)); }
	inline String_t* get_U3CGameServerStateU3Ek__BackingField_8() const { return ___U3CGameServerStateU3Ek__BackingField_8; }
	inline String_t** get_address_of_U3CGameServerStateU3Ek__BackingField_8() { return &___U3CGameServerStateU3Ek__BackingField_8; }
	inline void set_U3CGameServerStateU3Ek__BackingField_8(String_t* value)
	{
		___U3CGameServerStateU3Ek__BackingField_8 = value;
		Il2CppCodeGenWriteBarrier(&___U3CGameServerStateU3Ek__BackingField_8, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
