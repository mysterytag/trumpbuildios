﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.SkipUntilObservable`2/SkipUntilOuterObserver<System.Object,System.Object>
struct SkipUntilOuterObserver_t2866202317;
// UniRx.Operators.SkipUntilObservable`2<System.Object,System.Object>
struct SkipUntilObservable_2_t2740161498;
// UniRx.IObserver`1<System.Object>
struct IObserver_1_t3049105323;
// System.IDisposable
struct IDisposable_t1628921374;
// System.Object
struct Il2CppObject;
// System.Exception
struct Exception_t1967233988;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_Exception1967233988.h"

// System.Void UniRx.Operators.SkipUntilObservable`2/SkipUntilOuterObserver<System.Object,System.Object>::.ctor(UniRx.Operators.SkipUntilObservable`2<T,TOther>,UniRx.IObserver`1<T>,System.IDisposable)
extern "C"  void SkipUntilOuterObserver__ctor_m925276218_gshared (SkipUntilOuterObserver_t2866202317 * __this, SkipUntilObservable_2_t2740161498 * ___parent0, Il2CppObject* ___observer1, Il2CppObject * ___cancel2, const MethodInfo* method);
#define SkipUntilOuterObserver__ctor_m925276218(__this, ___parent0, ___observer1, ___cancel2, method) ((  void (*) (SkipUntilOuterObserver_t2866202317 *, SkipUntilObservable_2_t2740161498 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))SkipUntilOuterObserver__ctor_m925276218_gshared)(__this, ___parent0, ___observer1, ___cancel2, method)
// System.IDisposable UniRx.Operators.SkipUntilObservable`2/SkipUntilOuterObserver<System.Object,System.Object>::Run()
extern "C"  Il2CppObject * SkipUntilOuterObserver_Run_m2817850229_gshared (SkipUntilOuterObserver_t2866202317 * __this, const MethodInfo* method);
#define SkipUntilOuterObserver_Run_m2817850229(__this, method) ((  Il2CppObject * (*) (SkipUntilOuterObserver_t2866202317 *, const MethodInfo*))SkipUntilOuterObserver_Run_m2817850229_gshared)(__this, method)
// System.Void UniRx.Operators.SkipUntilObservable`2/SkipUntilOuterObserver<System.Object,System.Object>::OnNext(T)
extern "C"  void SkipUntilOuterObserver_OnNext_m2736822841_gshared (SkipUntilOuterObserver_t2866202317 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define SkipUntilOuterObserver_OnNext_m2736822841(__this, ___value0, method) ((  void (*) (SkipUntilOuterObserver_t2866202317 *, Il2CppObject *, const MethodInfo*))SkipUntilOuterObserver_OnNext_m2736822841_gshared)(__this, ___value0, method)
// System.Void UniRx.Operators.SkipUntilObservable`2/SkipUntilOuterObserver<System.Object,System.Object>::OnError(System.Exception)
extern "C"  void SkipUntilOuterObserver_OnError_m1942168392_gshared (SkipUntilOuterObserver_t2866202317 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method);
#define SkipUntilOuterObserver_OnError_m1942168392(__this, ___error0, method) ((  void (*) (SkipUntilOuterObserver_t2866202317 *, Exception_t1967233988 *, const MethodInfo*))SkipUntilOuterObserver_OnError_m1942168392_gshared)(__this, ___error0, method)
// System.Void UniRx.Operators.SkipUntilObservable`2/SkipUntilOuterObserver<System.Object,System.Object>::OnCompleted()
extern "C"  void SkipUntilOuterObserver_OnCompleted_m3849749147_gshared (SkipUntilOuterObserver_t2866202317 * __this, const MethodInfo* method);
#define SkipUntilOuterObserver_OnCompleted_m3849749147(__this, method) ((  void (*) (SkipUntilOuterObserver_t2866202317 *, const MethodInfo*))SkipUntilOuterObserver_OnCompleted_m3849749147_gshared)(__this, method)
