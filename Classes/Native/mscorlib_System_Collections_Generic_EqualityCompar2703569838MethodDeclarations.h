﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.EqualityComparer`1<GameAnalyticsSDK.GA_ServerFieldTypes/FieldType>
struct EqualityComparer_1_t2703569838;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"

// System.Void System.Collections.Generic.EqualityComparer`1<GameAnalyticsSDK.GA_ServerFieldTypes/FieldType>::.ctor()
extern "C"  void EqualityComparer_1__ctor_m1884607631_gshared (EqualityComparer_1_t2703569838 * __this, const MethodInfo* method);
#define EqualityComparer_1__ctor_m1884607631(__this, method) ((  void (*) (EqualityComparer_1_t2703569838 *, const MethodInfo*))EqualityComparer_1__ctor_m1884607631_gshared)(__this, method)
// System.Void System.Collections.Generic.EqualityComparer`1<GameAnalyticsSDK.GA_ServerFieldTypes/FieldType>::.cctor()
extern "C"  void EqualityComparer_1__cctor_m2106165502_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define EqualityComparer_1__cctor_m2106165502(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))EqualityComparer_1__cctor_m2106165502_gshared)(__this /* static, unused */, method)
// System.Int32 System.Collections.Generic.EqualityComparer`1<GameAnalyticsSDK.GA_ServerFieldTypes/FieldType>::System.Collections.IEqualityComparer.GetHashCode(System.Object)
extern "C"  int32_t EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m3386299936_gshared (EqualityComparer_1_t2703569838 * __this, Il2CppObject * ___obj0, const MethodInfo* method);
#define EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m3386299936(__this, ___obj0, method) ((  int32_t (*) (EqualityComparer_1_t2703569838 *, Il2CppObject *, const MethodInfo*))EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m3386299936_gshared)(__this, ___obj0, method)
// System.Boolean System.Collections.Generic.EqualityComparer`1<GameAnalyticsSDK.GA_ServerFieldTypes/FieldType>::System.Collections.IEqualityComparer.Equals(System.Object,System.Object)
extern "C"  bool EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m3310826206_gshared (EqualityComparer_1_t2703569838 * __this, Il2CppObject * ___x0, Il2CppObject * ___y1, const MethodInfo* method);
#define EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m3310826206(__this, ___x0, ___y1, method) ((  bool (*) (EqualityComparer_1_t2703569838 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m3310826206_gshared)(__this, ___x0, ___y1, method)
// System.Collections.Generic.EqualityComparer`1<T> System.Collections.Generic.EqualityComparer`1<GameAnalyticsSDK.GA_ServerFieldTypes/FieldType>::get_Default()
extern "C"  EqualityComparer_1_t2703569838 * EqualityComparer_1_get_Default_m1572997777_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define EqualityComparer_1_get_Default_m1572997777(__this /* static, unused */, method) ((  EqualityComparer_1_t2703569838 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))EqualityComparer_1_get_Default_m1572997777_gshared)(__this /* static, unused */, method)
