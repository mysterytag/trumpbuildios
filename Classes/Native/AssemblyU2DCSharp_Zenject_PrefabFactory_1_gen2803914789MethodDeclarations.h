﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Zenject.PrefabFactory`1<System.Object>
struct PrefabFactory_1_t2803914789;
// System.Object
struct Il2CppObject;
// UnityEngine.GameObject
struct GameObject_t4012695102;
// System.String
struct String_t;
// System.Collections.Generic.IEnumerable`1<Zenject.ZenjectResolveException>
struct IEnumerable_1_t4073207355;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_GameObject4012695102.h"
#include "mscorlib_System_String968488902.h"

// System.Void Zenject.PrefabFactory`1<System.Object>::.ctor()
extern "C"  void PrefabFactory_1__ctor_m2086357463_gshared (PrefabFactory_1_t2803914789 * __this, const MethodInfo* method);
#define PrefabFactory_1__ctor_m2086357463(__this, method) ((  void (*) (PrefabFactory_1_t2803914789 *, const MethodInfo*))PrefabFactory_1__ctor_m2086357463_gshared)(__this, method)
// T Zenject.PrefabFactory`1<System.Object>::Create(UnityEngine.GameObject)
extern "C"  Il2CppObject * PrefabFactory_1_Create_m3186751104_gshared (PrefabFactory_1_t2803914789 * __this, GameObject_t4012695102 * ___prefab0, const MethodInfo* method);
#define PrefabFactory_1_Create_m3186751104(__this, ___prefab0, method) ((  Il2CppObject * (*) (PrefabFactory_1_t2803914789 *, GameObject_t4012695102 *, const MethodInfo*))PrefabFactory_1_Create_m3186751104_gshared)(__this, ___prefab0, method)
// T Zenject.PrefabFactory`1<System.Object>::Create(System.String)
extern "C"  Il2CppObject * PrefabFactory_1_Create_m1056476922_gshared (PrefabFactory_1_t2803914789 * __this, String_t* ___prefabResourceName0, const MethodInfo* method);
#define PrefabFactory_1_Create_m1056476922(__this, ___prefabResourceName0, method) ((  Il2CppObject * (*) (PrefabFactory_1_t2803914789 *, String_t*, const MethodInfo*))PrefabFactory_1_Create_m1056476922_gshared)(__this, ___prefabResourceName0, method)
// System.Collections.Generic.IEnumerable`1<Zenject.ZenjectResolveException> Zenject.PrefabFactory`1<System.Object>::Validate()
extern "C"  Il2CppObject* PrefabFactory_1_Validate_m624577986_gshared (PrefabFactory_1_t2803914789 * __this, const MethodInfo* method);
#define PrefabFactory_1_Validate_m624577986(__this, method) ((  Il2CppObject* (*) (PrefabFactory_1_t2803914789 *, const MethodInfo*))PrefabFactory_1_Validate_m624577986_gshared)(__this, method)
