﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.ThrowObservable`1/Throw<System.Object>
struct Throw_t4151912527;
// UniRx.IObserver`1<System.Object>
struct IObserver_1_t3049105323;
// System.IDisposable
struct IDisposable_t1628921374;
// System.Object
struct Il2CppObject;
// System.Exception
struct Exception_t1967233988;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_Exception1967233988.h"

// System.Void UniRx.Operators.ThrowObservable`1/Throw<System.Object>::.ctor(UniRx.IObserver`1<T>,System.IDisposable)
extern "C"  void Throw__ctor_m2513031773_gshared (Throw_t4151912527 * __this, Il2CppObject* ___observer0, Il2CppObject * ___cancel1, const MethodInfo* method);
#define Throw__ctor_m2513031773(__this, ___observer0, ___cancel1, method) ((  void (*) (Throw_t4151912527 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))Throw__ctor_m2513031773_gshared)(__this, ___observer0, ___cancel1, method)
// System.Void UniRx.Operators.ThrowObservable`1/Throw<System.Object>::OnNext(T)
extern "C"  void Throw_OnNext_m383148821_gshared (Throw_t4151912527 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Throw_OnNext_m383148821(__this, ___value0, method) ((  void (*) (Throw_t4151912527 *, Il2CppObject *, const MethodInfo*))Throw_OnNext_m383148821_gshared)(__this, ___value0, method)
// System.Void UniRx.Operators.ThrowObservable`1/Throw<System.Object>::OnError(System.Exception)
extern "C"  void Throw_OnError_m3840128036_gshared (Throw_t4151912527 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method);
#define Throw_OnError_m3840128036(__this, ___error0, method) ((  void (*) (Throw_t4151912527 *, Exception_t1967233988 *, const MethodInfo*))Throw_OnError_m3840128036_gshared)(__this, ___error0, method)
// System.Void UniRx.Operators.ThrowObservable`1/Throw<System.Object>::OnCompleted()
extern "C"  void Throw_OnCompleted_m2233761143_gshared (Throw_t4151912527 * __this, const MethodInfo* method);
#define Throw_OnCompleted_m2233761143(__this, method) ((  void (*) (Throw_t4151912527 *, const MethodInfo*))Throw_OnCompleted_m2233761143_gshared)(__this, method)
