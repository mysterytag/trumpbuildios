﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Oper4165376397.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Operators.TakeObservable`1/Take<UniRx.Unit>
struct  Take_t2252432490  : public OperatorObserverBase_2_t4165376397
{
public:
	// System.Int32 UniRx.Operators.TakeObservable`1/Take::rest
	int32_t ___rest_2;

public:
	inline static int32_t get_offset_of_rest_2() { return static_cast<int32_t>(offsetof(Take_t2252432490, ___rest_2)); }
	inline int32_t get_rest_2() const { return ___rest_2; }
	inline int32_t* get_address_of_rest_2() { return &___rest_2; }
	inline void set_rest_2(int32_t value)
	{
		___rest_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
