﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.ClientModels.GetLeaderboardAroundCharacterRequest
struct GetLeaderboardAroundCharacterRequest_t1108200042;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String968488902.h"
#include "mscorlib_System_Nullable_1_gen1438485399.h"

// System.Void PlayFab.ClientModels.GetLeaderboardAroundCharacterRequest::.ctor()
extern "C"  void GetLeaderboardAroundCharacterRequest__ctor_m2917647379 (GetLeaderboardAroundCharacterRequest_t1108200042 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PlayFab.ClientModels.GetLeaderboardAroundCharacterRequest::get_StatisticName()
extern "C"  String_t* GetLeaderboardAroundCharacterRequest_get_StatisticName_m425992294 (GetLeaderboardAroundCharacterRequest_t1108200042 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.GetLeaderboardAroundCharacterRequest::set_StatisticName(System.String)
extern "C"  void GetLeaderboardAroundCharacterRequest_set_StatisticName_m3654759123 (GetLeaderboardAroundCharacterRequest_t1108200042 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PlayFab.ClientModels.GetLeaderboardAroundCharacterRequest::get_CharacterId()
extern "C"  String_t* GetLeaderboardAroundCharacterRequest_get_CharacterId_m314448591 (GetLeaderboardAroundCharacterRequest_t1108200042 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.GetLeaderboardAroundCharacterRequest::set_CharacterId(System.String)
extern "C"  void GetLeaderboardAroundCharacterRequest_set_CharacterId_m4099824842 (GetLeaderboardAroundCharacterRequest_t1108200042 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PlayFab.ClientModels.GetLeaderboardAroundCharacterRequest::get_CharacterType()
extern "C"  String_t* GetLeaderboardAroundCharacterRequest_get_CharacterType_m1873903726 (GetLeaderboardAroundCharacterRequest_t1108200042 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.GetLeaderboardAroundCharacterRequest::set_CharacterType(System.String)
extern "C"  void GetLeaderboardAroundCharacterRequest_set_CharacterType_m590618571 (GetLeaderboardAroundCharacterRequest_t1108200042 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Nullable`1<System.Int32> PlayFab.ClientModels.GetLeaderboardAroundCharacterRequest::get_MaxResultsCount()
extern "C"  Nullable_1_t1438485399  GetLeaderboardAroundCharacterRequest_get_MaxResultsCount_m2778807924 (GetLeaderboardAroundCharacterRequest_t1108200042 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.GetLeaderboardAroundCharacterRequest::set_MaxResultsCount(System.Nullable`1<System.Int32>)
extern "C"  void GetLeaderboardAroundCharacterRequest_set_MaxResultsCount_m2235838109 (GetLeaderboardAroundCharacterRequest_t1108200042 * __this, Nullable_1_t1438485399  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
