﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.IgnoreElementsObservable`1<System.Object>
struct IgnoreElementsObservable_1_t2767931315;
// UniRx.IObservable`1<System.Object>
struct IObservable_1_t595904784;
// System.IDisposable
struct IDisposable_t1628921374;
// UniRx.IObserver`1<System.Object>
struct IObserver_1_t3049105323;

#include "codegen/il2cpp-codegen.h"

// System.Void UniRx.Operators.IgnoreElementsObservable`1<System.Object>::.ctor(UniRx.IObservable`1<T>)
extern "C"  void IgnoreElementsObservable_1__ctor_m1260378830_gshared (IgnoreElementsObservable_1_t2767931315 * __this, Il2CppObject* ___source0, const MethodInfo* method);
#define IgnoreElementsObservable_1__ctor_m1260378830(__this, ___source0, method) ((  void (*) (IgnoreElementsObservable_1_t2767931315 *, Il2CppObject*, const MethodInfo*))IgnoreElementsObservable_1__ctor_m1260378830_gshared)(__this, ___source0, method)
// System.IDisposable UniRx.Operators.IgnoreElementsObservable`1<System.Object>::SubscribeCore(UniRx.IObserver`1<T>,System.IDisposable)
extern "C"  Il2CppObject * IgnoreElementsObservable_1_SubscribeCore_m2243093241_gshared (IgnoreElementsObservable_1_t2767931315 * __this, Il2CppObject* ___observer0, Il2CppObject * ___cancel1, const MethodInfo* method);
#define IgnoreElementsObservable_1_SubscribeCore_m2243093241(__this, ___observer0, ___cancel1, method) ((  Il2CppObject * (*) (IgnoreElementsObservable_1_t2767931315 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))IgnoreElementsObservable_1_SubscribeCore_m2243093241_gshared)(__this, ___observer0, ___cancel1, method)
