﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.AmbObservable`1/AmbOuterObserver<System.Object>
struct AmbOuterObserver_t2827192636;
// UniRx.Operators.AmbObservable`1<System.Object>
struct AmbObservable_1_t4231346232;
// UniRx.IObserver`1<System.Object>
struct IObserver_1_t3049105323;
// System.IDisposable
struct IDisposable_t1628921374;
// System.Object
struct Il2CppObject;
// System.Exception
struct Exception_t1967233988;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_Exception1967233988.h"

// System.Void UniRx.Operators.AmbObservable`1/AmbOuterObserver<System.Object>::.ctor(UniRx.Operators.AmbObservable`1<T>,UniRx.IObserver`1<T>,System.IDisposable)
extern "C"  void AmbOuterObserver__ctor_m971803683_gshared (AmbOuterObserver_t2827192636 * __this, AmbObservable_1_t4231346232 * ___parent0, Il2CppObject* ___observer1, Il2CppObject * ___cancel2, const MethodInfo* method);
#define AmbOuterObserver__ctor_m971803683(__this, ___parent0, ___observer1, ___cancel2, method) ((  void (*) (AmbOuterObserver_t2827192636 *, AmbObservable_1_t4231346232 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))AmbOuterObserver__ctor_m971803683_gshared)(__this, ___parent0, ___observer1, ___cancel2, method)
// System.IDisposable UniRx.Operators.AmbObservable`1/AmbOuterObserver<System.Object>::Run()
extern "C"  Il2CppObject * AmbOuterObserver_Run_m1474616694_gshared (AmbOuterObserver_t2827192636 * __this, const MethodInfo* method);
#define AmbOuterObserver_Run_m1474616694(__this, method) ((  Il2CppObject * (*) (AmbOuterObserver_t2827192636 *, const MethodInfo*))AmbOuterObserver_Run_m1474616694_gshared)(__this, method)
// System.Void UniRx.Operators.AmbObservable`1/AmbOuterObserver<System.Object>::OnNext(T)
extern "C"  void AmbOuterObserver_OnNext_m4113981434_gshared (AmbOuterObserver_t2827192636 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define AmbOuterObserver_OnNext_m4113981434(__this, ___value0, method) ((  void (*) (AmbOuterObserver_t2827192636 *, Il2CppObject *, const MethodInfo*))AmbOuterObserver_OnNext_m4113981434_gshared)(__this, ___value0, method)
// System.Void UniRx.Operators.AmbObservable`1/AmbOuterObserver<System.Object>::OnError(System.Exception)
extern "C"  void AmbOuterObserver_OnError_m2210350857_gshared (AmbOuterObserver_t2827192636 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method);
#define AmbOuterObserver_OnError_m2210350857(__this, ___error0, method) ((  void (*) (AmbOuterObserver_t2827192636 *, Exception_t1967233988 *, const MethodInfo*))AmbOuterObserver_OnError_m2210350857_gshared)(__this, ___error0, method)
// System.Void UniRx.Operators.AmbObservable`1/AmbOuterObserver<System.Object>::OnCompleted()
extern "C"  void AmbOuterObserver_OnCompleted_m130121692_gshared (AmbOuterObserver_t2827192636 * __this, const MethodInfo* method);
#define AmbOuterObserver_OnCompleted_m130121692(__this, method) ((  void (*) (AmbOuterObserver_t2827192636 *, const MethodInfo*))AmbOuterObserver_OnCompleted_m130121692_gshared)(__this, method)
