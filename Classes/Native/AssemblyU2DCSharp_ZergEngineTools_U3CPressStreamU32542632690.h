﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.EventSystems.EventTrigger
struct EventTrigger_t2937500249;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ZergEngineTools/<PressStream>c__AnonStorey14E
struct  U3CPressStreamU3Ec__AnonStorey14E_t2542632690  : public Il2CppObject
{
public:
	// UnityEngine.EventSystems.EventTrigger ZergEngineTools/<PressStream>c__AnonStorey14E::trigger
	EventTrigger_t2937500249 * ___trigger_0;

public:
	inline static int32_t get_offset_of_trigger_0() { return static_cast<int32_t>(offsetof(U3CPressStreamU3Ec__AnonStorey14E_t2542632690, ___trigger_0)); }
	inline EventTrigger_t2937500249 * get_trigger_0() const { return ___trigger_0; }
	inline EventTrigger_t2937500249 ** get_address_of_trigger_0() { return &___trigger_0; }
	inline void set_trigger_0(EventTrigger_t2937500249 * value)
	{
		___trigger_0 = value;
		Il2CppCodeGenWriteBarrier(&___trigger_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
