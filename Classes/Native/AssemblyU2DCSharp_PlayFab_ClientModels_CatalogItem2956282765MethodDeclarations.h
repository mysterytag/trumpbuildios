﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.ClientModels.CatalogItemConsumableInfo
struct CatalogItemConsumableInfo_t2956282765;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Nullable_1_gen3871963234.h"
#include "mscorlib_System_String968488902.h"

// System.Void PlayFab.ClientModels.CatalogItemConsumableInfo::.ctor()
extern "C"  void CatalogItemConsumableInfo__ctor_m107689788 (CatalogItemConsumableInfo_t2956282765 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Nullable`1<System.UInt32> PlayFab.ClientModels.CatalogItemConsumableInfo::get_UsageCount()
extern "C"  Nullable_1_t3871963234  CatalogItemConsumableInfo_get_UsageCount_m4101587403 (CatalogItemConsumableInfo_t2956282765 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.CatalogItemConsumableInfo::set_UsageCount(System.Nullable`1<System.UInt32>)
extern "C"  void CatalogItemConsumableInfo_set_UsageCount_m1306702466 (CatalogItemConsumableInfo_t2956282765 * __this, Nullable_1_t3871963234  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Nullable`1<System.UInt32> PlayFab.ClientModels.CatalogItemConsumableInfo::get_UsagePeriod()
extern "C"  Nullable_1_t3871963234  CatalogItemConsumableInfo_get_UsagePeriod_m3401157223 (CatalogItemConsumableInfo_t2956282765 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.CatalogItemConsumableInfo::set_UsagePeriod(System.Nullable`1<System.UInt32>)
extern "C"  void CatalogItemConsumableInfo_set_UsagePeriod_m504126926 (CatalogItemConsumableInfo_t2956282765 * __this, Nullable_1_t3871963234  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PlayFab.ClientModels.CatalogItemConsumableInfo::get_UsagePeriodGroup()
extern "C"  String_t* CatalogItemConsumableInfo_get_UsagePeriodGroup_m536027537 (CatalogItemConsumableInfo_t2956282765 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.CatalogItemConsumableInfo::set_UsagePeriodGroup(System.String)
extern "C"  void CatalogItemConsumableInfo_set_UsagePeriodGroup_m3129893152 (CatalogItemConsumableInfo_t2956282765 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
