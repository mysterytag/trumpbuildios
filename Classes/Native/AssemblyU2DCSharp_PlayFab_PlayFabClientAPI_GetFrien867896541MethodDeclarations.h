﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.PlayFabClientAPI/GetFriendLeaderboardAroundPlayerResponseCallback
struct GetFriendLeaderboardAroundPlayerResponseCallback_t867896541;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;
// PlayFab.ClientModels.GetFriendLeaderboardAroundPlayerRequest
struct GetFriendLeaderboardAroundPlayerRequest_t16887688;
// PlayFab.ClientModels.GetFriendLeaderboardAroundPlayerResult
struct GetFriendLeaderboardAroundPlayerResult_t589881892;
// PlayFab.PlayFabError
struct PlayFabError_t750598646;
// System.IAsyncResult
struct IAsyncResult_t537683269;
// System.AsyncCallback
struct AsyncCallback_t1363551830;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_IntPtr676692020.h"
#include "mscorlib_System_String968488902.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_GetFriendLead16887688.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_GetFriendLea589881892.h"
#include "AssemblyU2DCSharpU2Dfirstpass_PlayFab_PlayFabError750598646.h"
#include "mscorlib_System_AsyncCallback1363551830.h"

// System.Void PlayFab.PlayFabClientAPI/GetFriendLeaderboardAroundPlayerResponseCallback::.ctor(System.Object,System.IntPtr)
extern "C"  void GetFriendLeaderboardAroundPlayerResponseCallback__ctor_m2922103628 (GetFriendLeaderboardAroundPlayerResponseCallback_t867896541 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/GetFriendLeaderboardAroundPlayerResponseCallback::Invoke(System.String,System.Int32,PlayFab.ClientModels.GetFriendLeaderboardAroundPlayerRequest,PlayFab.ClientModels.GetFriendLeaderboardAroundPlayerResult,PlayFab.PlayFabError,System.Object)
extern "C"  void GetFriendLeaderboardAroundPlayerResponseCallback_Invoke_m1421057465 (GetFriendLeaderboardAroundPlayerResponseCallback_t867896541 * __this, String_t* ___urlPath0, int32_t ___callId1, GetFriendLeaderboardAroundPlayerRequest_t16887688 * ___request2, GetFriendLeaderboardAroundPlayerResult_t589881892 * ___result3, PlayFabError_t750598646 * ___error4, Il2CppObject * ___customData5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
extern "C" void pinvoke_delegate_wrapper_GetFriendLeaderboardAroundPlayerResponseCallback_t867896541(Il2CppObject* delegate, String_t* ___urlPath0, int32_t ___callId1, GetFriendLeaderboardAroundPlayerRequest_t16887688 * ___request2, GetFriendLeaderboardAroundPlayerResult_t589881892 * ___result3, PlayFabError_t750598646 * ___error4, Il2CppObject * ___customData5);
// System.IAsyncResult PlayFab.PlayFabClientAPI/GetFriendLeaderboardAroundPlayerResponseCallback::BeginInvoke(System.String,System.Int32,PlayFab.ClientModels.GetFriendLeaderboardAroundPlayerRequest,PlayFab.ClientModels.GetFriendLeaderboardAroundPlayerResult,PlayFab.PlayFabError,System.Object,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * GetFriendLeaderboardAroundPlayerResponseCallback_BeginInvoke_m3636484262 (GetFriendLeaderboardAroundPlayerResponseCallback_t867896541 * __this, String_t* ___urlPath0, int32_t ___callId1, GetFriendLeaderboardAroundPlayerRequest_t16887688 * ___request2, GetFriendLeaderboardAroundPlayerResult_t589881892 * ___result3, PlayFabError_t750598646 * ___error4, Il2CppObject * ___customData5, AsyncCallback_t1363551830 * ___callback6, Il2CppObject * ___object7, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/GetFriendLeaderboardAroundPlayerResponseCallback::EndInvoke(System.IAsyncResult)
extern "C"  void GetFriendLeaderboardAroundPlayerResponseCallback_EndInvoke_m2585574236 (GetFriendLeaderboardAroundPlayerResponseCallback_t867896541 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
