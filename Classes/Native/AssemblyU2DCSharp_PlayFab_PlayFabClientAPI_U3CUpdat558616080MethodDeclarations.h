﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.PlayFabClientAPI/<UpdatePlayerStatistics>c__AnonStorey97
struct U3CUpdatePlayerStatisticsU3Ec__AnonStorey97_t558616080;
// PlayFab.CallRequestContainer
struct CallRequestContainer_t432318609;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharpU2Dfirstpass_PlayFab_CallRequestCo432318609.h"

// System.Void PlayFab.PlayFabClientAPI/<UpdatePlayerStatistics>c__AnonStorey97::.ctor()
extern "C"  void U3CUpdatePlayerStatisticsU3Ec__AnonStorey97__ctor_m2597727139 (U3CUpdatePlayerStatisticsU3Ec__AnonStorey97_t558616080 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/<UpdatePlayerStatistics>c__AnonStorey97::<>m__84(PlayFab.CallRequestContainer)
extern "C"  void U3CUpdatePlayerStatisticsU3Ec__AnonStorey97_U3CU3Em__84_m155328829 (U3CUpdatePlayerStatisticsU3Ec__AnonStorey97_t558616080 * __this, CallRequestContainer_t432318609 * ___requestContainer0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
