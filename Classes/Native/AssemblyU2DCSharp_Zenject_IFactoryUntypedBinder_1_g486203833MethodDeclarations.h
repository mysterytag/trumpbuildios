﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Zenject.IFactoryUntypedBinder`1<System.Object>
struct IFactoryUntypedBinder_1_t486203833;
// Zenject.DiContainer
struct DiContainer_t2383114449;
// System.String
struct String_t;
// Zenject.BindingConditionSetter
struct BindingConditionSetter_t259147722;
// System.Object
struct Il2CppObject;
// System.Func`3<Zenject.DiContainer,System.Object[],System.Object>
struct Func_3_t2319412757;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Zenject_DiContainer2383114449.h"
#include "mscorlib_System_String968488902.h"
#include "mscorlib_System_Object837106420.h"

// System.Void Zenject.IFactoryUntypedBinder`1<System.Object>::.ctor(Zenject.DiContainer,System.String)
extern "C"  void IFactoryUntypedBinder_1__ctor_m2282437894_gshared (IFactoryUntypedBinder_1_t486203833 * __this, DiContainer_t2383114449 * ___container0, String_t* ___identifier1, const MethodInfo* method);
#define IFactoryUntypedBinder_1__ctor_m2282437894(__this, ___container0, ___identifier1, method) ((  void (*) (IFactoryUntypedBinder_1_t486203833 *, DiContainer_t2383114449 *, String_t*, const MethodInfo*))IFactoryUntypedBinder_1__ctor_m2282437894_gshared)(__this, ___container0, ___identifier1, method)
// Zenject.BindingConditionSetter Zenject.IFactoryUntypedBinder`1<System.Object>::ToInstance(TContract)
extern "C"  BindingConditionSetter_t259147722 * IFactoryUntypedBinder_1_ToInstance_m3479808858_gshared (IFactoryUntypedBinder_1_t486203833 * __this, Il2CppObject * ___instance0, const MethodInfo* method);
#define IFactoryUntypedBinder_1_ToInstance_m3479808858(__this, ___instance0, method) ((  BindingConditionSetter_t259147722 * (*) (IFactoryUntypedBinder_1_t486203833 *, Il2CppObject *, const MethodInfo*))IFactoryUntypedBinder_1_ToInstance_m3479808858_gshared)(__this, ___instance0, method)
// Zenject.BindingConditionSetter Zenject.IFactoryUntypedBinder`1<System.Object>::ToMethod(System.Func`3<Zenject.DiContainer,System.Object[],TContract>)
extern "C"  BindingConditionSetter_t259147722 * IFactoryUntypedBinder_1_ToMethod_m844622821_gshared (IFactoryUntypedBinder_1_t486203833 * __this, Func_3_t2319412757 * ___method0, const MethodInfo* method);
#define IFactoryUntypedBinder_1_ToMethod_m844622821(__this, ___method0, method) ((  BindingConditionSetter_t259147722 * (*) (IFactoryUntypedBinder_1_t486203833 *, Func_3_t2319412757 *, const MethodInfo*))IFactoryUntypedBinder_1_ToMethod_m844622821_gshared)(__this, ___method0, method)
// Zenject.BindingConditionSetter Zenject.IFactoryUntypedBinder`1<System.Object>::ToFactory()
extern "C"  BindingConditionSetter_t259147722 * IFactoryUntypedBinder_1_ToFactory_m3712332303_gshared (IFactoryUntypedBinder_1_t486203833 * __this, const MethodInfo* method);
#define IFactoryUntypedBinder_1_ToFactory_m3712332303(__this, method) ((  BindingConditionSetter_t259147722 * (*) (IFactoryUntypedBinder_1_t486203833 *, const MethodInfo*))IFactoryUntypedBinder_1_ToFactory_m3712332303_gshared)(__this, method)
