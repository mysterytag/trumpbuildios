﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.StartObservable`1/StartObserver<UniRx.Unit>
struct StartObserver_t2543316435;
// UniRx.Operators.StartObservable`1<UniRx.Unit>
struct StartObservable_1_t4083709414;
// UniRx.IObserver`1<UniRx.Unit>
struct IObserver_1_t475317645;
// System.IDisposable
struct IDisposable_t1628921374;
// System.Exception
struct Exception_t1967233988;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Unit2558286038.h"
#include "mscorlib_System_Exception1967233988.h"

// System.Void UniRx.Operators.StartObservable`1/StartObserver<UniRx.Unit>::.ctor(UniRx.Operators.StartObservable`1<T>,UniRx.IObserver`1<T>,System.IDisposable)
extern "C"  void StartObserver__ctor_m828476198_gshared (StartObserver_t2543316435 * __this, StartObservable_1_t4083709414 * ___parent0, Il2CppObject* ___observer1, Il2CppObject * ___cancel2, const MethodInfo* method);
#define StartObserver__ctor_m828476198(__this, ___parent0, ___observer1, ___cancel2, method) ((  void (*) (StartObserver_t2543316435 *, StartObservable_1_t4083709414 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))StartObserver__ctor_m828476198_gshared)(__this, ___parent0, ___observer1, ___cancel2, method)
// System.Void UniRx.Operators.StartObservable`1/StartObserver<UniRx.Unit>::Run()
extern "C"  void StartObserver_Run_m4024216466_gshared (StartObserver_t2543316435 * __this, const MethodInfo* method);
#define StartObserver_Run_m4024216466(__this, method) ((  void (*) (StartObserver_t2543316435 *, const MethodInfo*))StartObserver_Run_m4024216466_gshared)(__this, method)
// System.Void UniRx.Operators.StartObservable`1/StartObserver<UniRx.Unit>::OnNext(T)
extern "C"  void StartObserver_OnNext_m1333910929_gshared (StartObserver_t2543316435 * __this, Unit_t2558286038  ___value0, const MethodInfo* method);
#define StartObserver_OnNext_m1333910929(__this, ___value0, method) ((  void (*) (StartObserver_t2543316435 *, Unit_t2558286038 , const MethodInfo*))StartObserver_OnNext_m1333910929_gshared)(__this, ___value0, method)
// System.Void UniRx.Operators.StartObservable`1/StartObserver<UniRx.Unit>::OnError(System.Exception)
extern "C"  void StartObserver_OnError_m1839380128_gshared (StartObserver_t2543316435 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method);
#define StartObserver_OnError_m1839380128(__this, ___error0, method) ((  void (*) (StartObserver_t2543316435 *, Exception_t1967233988 *, const MethodInfo*))StartObserver_OnError_m1839380128_gshared)(__this, ___error0, method)
// System.Void UniRx.Operators.StartObservable`1/StartObserver<UniRx.Unit>::OnCompleted()
extern "C"  void StartObserver_OnCompleted_m777411059_gshared (StartObserver_t2543316435 * __this, const MethodInfo* method);
#define StartObserver_OnCompleted_m777411059(__this, method) ((  void (*) (StartObserver_t2543316435 *, const MethodInfo*))StartObserver_OnCompleted_m777411059_gshared)(__this, method)
