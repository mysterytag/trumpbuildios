﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_CollectionAddE1948677386MethodDeclarations.h"

// System.Void UniRx.CollectionAddEvent`1<UniRx.Tuple`2<System.String,UnityEngine.Sprite>>::.ctor(System.Int32,T)
#define CollectionAddEvent_1__ctor_m3089750418(__this, ___index0, ___value1, method) ((  void (*) (CollectionAddEvent_1_t3767990510 *, int32_t, Tuple_2_t2188574943 , const MethodInfo*))CollectionAddEvent_1__ctor_m1733868862_gshared)(__this, ___index0, ___value1, method)
// System.Int32 UniRx.CollectionAddEvent`1<UniRx.Tuple`2<System.String,UnityEngine.Sprite>>::get_Index()
#define CollectionAddEvent_1_get_Index_m184735014(__this, method) ((  int32_t (*) (CollectionAddEvent_1_t3767990510 *, const MethodInfo*))CollectionAddEvent_1_get_Index_m4141336102_gshared)(__this, method)
// System.Void UniRx.CollectionAddEvent`1<UniRx.Tuple`2<System.String,UnityEngine.Sprite>>::set_Index(System.Int32)
#define CollectionAddEvent_1_set_Index_m4232861069(__this, ___value0, method) ((  void (*) (CollectionAddEvent_1_t3767990510 *, int32_t, const MethodInfo*))CollectionAddEvent_1_set_Index_m2605776441_gshared)(__this, ___value0, method)
// T UniRx.CollectionAddEvent`1<UniRx.Tuple`2<System.String,UnityEngine.Sprite>>::get_Value()
#define CollectionAddEvent_1_get_Value_m1083911054(__this, method) ((  Tuple_2_t2188574943  (*) (CollectionAddEvent_1_t3767990510 *, const MethodInfo*))CollectionAddEvent_1_get_Value_m4091107292_gshared)(__this, method)
// System.Void UniRx.CollectionAddEvent`1<UniRx.Tuple`2<System.String,UnityEngine.Sprite>>::set_Value(T)
#define CollectionAddEvent_1_set_Value_m1893798307(__this, ___value0, method) ((  void (*) (CollectionAddEvent_1_t3767990510 *, Tuple_2_t2188574943 , const MethodInfo*))CollectionAddEvent_1_set_Value_m1420762231_gshared)(__this, ___value0, method)
// System.String UniRx.CollectionAddEvent`1<UniRx.Tuple`2<System.String,UnityEngine.Sprite>>::ToString()
#define CollectionAddEvent_1_ToString_m3213253988(__this, method) ((  String_t* (*) (CollectionAddEvent_1_t3767990510 *, const MethodInfo*))CollectionAddEvent_1_ToString_m3083878430_gshared)(__this, method)
// System.Int32 UniRx.CollectionAddEvent`1<UniRx.Tuple`2<System.String,UnityEngine.Sprite>>::GetHashCode()
#define CollectionAddEvent_1_GetHashCode_m430568206(__this, method) ((  int32_t (*) (CollectionAddEvent_1_t3767990510 *, const MethodInfo*))CollectionAddEvent_1_GetHashCode_m1678156814_gshared)(__this, method)
// System.Boolean UniRx.CollectionAddEvent`1<UniRx.Tuple`2<System.String,UnityEngine.Sprite>>::Equals(UniRx.CollectionAddEvent`1<T>)
#define CollectionAddEvent_1_Equals_m4258244558(__this, ___other0, method) ((  bool (*) (CollectionAddEvent_1_t3767990510 *, CollectionAddEvent_1_t3767990510 , const MethodInfo*))CollectionAddEvent_1_Equals_m2717942978_gshared)(__this, ___other0, method)
