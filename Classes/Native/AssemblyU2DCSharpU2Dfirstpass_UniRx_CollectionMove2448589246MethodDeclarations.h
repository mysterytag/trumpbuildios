﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_CollectionMove2448589246.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Tuple_2_gen369261819.h"

// System.Void UniRx.CollectionMoveEvent`1<UniRx.Tuple`2<System.Object,System.Object>>::.ctor(System.Int32,System.Int32,T)
extern "C"  void CollectionMoveEvent_1__ctor_m1005665093_gshared (CollectionMoveEvent_1_t2448589246 * __this, int32_t ___oldIndex0, int32_t ___newIndex1, Tuple_2_t369261819  ___value2, const MethodInfo* method);
#define CollectionMoveEvent_1__ctor_m1005665093(__this, ___oldIndex0, ___newIndex1, ___value2, method) ((  void (*) (CollectionMoveEvent_1_t2448589246 *, int32_t, int32_t, Tuple_2_t369261819 , const MethodInfo*))CollectionMoveEvent_1__ctor_m1005665093_gshared)(__this, ___oldIndex0, ___newIndex1, ___value2, method)
// System.Int32 UniRx.CollectionMoveEvent`1<UniRx.Tuple`2<System.Object,System.Object>>::get_OldIndex()
extern "C"  int32_t CollectionMoveEvent_1_get_OldIndex_m1808831945_gshared (CollectionMoveEvent_1_t2448589246 * __this, const MethodInfo* method);
#define CollectionMoveEvent_1_get_OldIndex_m1808831945(__this, method) ((  int32_t (*) (CollectionMoveEvent_1_t2448589246 *, const MethodInfo*))CollectionMoveEvent_1_get_OldIndex_m1808831945_gshared)(__this, method)
// System.Void UniRx.CollectionMoveEvent`1<UniRx.Tuple`2<System.Object,System.Object>>::set_OldIndex(System.Int32)
extern "C"  void CollectionMoveEvent_1_set_OldIndex_m3813399748_gshared (CollectionMoveEvent_1_t2448589246 * __this, int32_t ___value0, const MethodInfo* method);
#define CollectionMoveEvent_1_set_OldIndex_m3813399748(__this, ___value0, method) ((  void (*) (CollectionMoveEvent_1_t2448589246 *, int32_t, const MethodInfo*))CollectionMoveEvent_1_set_OldIndex_m3813399748_gshared)(__this, ___value0, method)
// System.Int32 UniRx.CollectionMoveEvent`1<UniRx.Tuple`2<System.Object,System.Object>>::get_NewIndex()
extern "C"  int32_t CollectionMoveEvent_1_get_NewIndex_m526282800_gshared (CollectionMoveEvent_1_t2448589246 * __this, const MethodInfo* method);
#define CollectionMoveEvent_1_get_NewIndex_m526282800(__this, method) ((  int32_t (*) (CollectionMoveEvent_1_t2448589246 *, const MethodInfo*))CollectionMoveEvent_1_get_NewIndex_m526282800_gshared)(__this, method)
// System.Void UniRx.CollectionMoveEvent`1<UniRx.Tuple`2<System.Object,System.Object>>::set_NewIndex(System.Int32)
extern "C"  void CollectionMoveEvent_1_set_NewIndex_m899306667_gshared (CollectionMoveEvent_1_t2448589246 * __this, int32_t ___value0, const MethodInfo* method);
#define CollectionMoveEvent_1_set_NewIndex_m899306667(__this, ___value0, method) ((  void (*) (CollectionMoveEvent_1_t2448589246 *, int32_t, const MethodInfo*))CollectionMoveEvent_1_set_NewIndex_m899306667_gshared)(__this, ___value0, method)
// T UniRx.CollectionMoveEvent`1<UniRx.Tuple`2<System.Object,System.Object>>::get_Value()
extern "C"  Tuple_2_t369261819  CollectionMoveEvent_1_get_Value_m2394553534_gshared (CollectionMoveEvent_1_t2448589246 * __this, const MethodInfo* method);
#define CollectionMoveEvent_1_get_Value_m2394553534(__this, method) ((  Tuple_2_t369261819  (*) (CollectionMoveEvent_1_t2448589246 *, const MethodInfo*))CollectionMoveEvent_1_get_Value_m2394553534_gshared)(__this, method)
// System.Void UniRx.CollectionMoveEvent`1<UniRx.Tuple`2<System.Object,System.Object>>::set_Value(T)
extern "C"  void CollectionMoveEvent_1_set_Value_m1426460147_gshared (CollectionMoveEvent_1_t2448589246 * __this, Tuple_2_t369261819  ___value0, const MethodInfo* method);
#define CollectionMoveEvent_1_set_Value_m1426460147(__this, ___value0, method) ((  void (*) (CollectionMoveEvent_1_t2448589246 *, Tuple_2_t369261819 , const MethodInfo*))CollectionMoveEvent_1_set_Value_m1426460147_gshared)(__this, ___value0, method)
// System.String UniRx.CollectionMoveEvent`1<UniRx.Tuple`2<System.Object,System.Object>>::ToString()
extern "C"  String_t* CollectionMoveEvent_1_ToString_m3719328820_gshared (CollectionMoveEvent_1_t2448589246 * __this, const MethodInfo* method);
#define CollectionMoveEvent_1_ToString_m3719328820(__this, method) ((  String_t* (*) (CollectionMoveEvent_1_t2448589246 *, const MethodInfo*))CollectionMoveEvent_1_ToString_m3719328820_gshared)(__this, method)
// System.Int32 UniRx.CollectionMoveEvent`1<UniRx.Tuple`2<System.Object,System.Object>>::GetHashCode()
extern "C"  int32_t CollectionMoveEvent_1_GetHashCode_m1029581758_gshared (CollectionMoveEvent_1_t2448589246 * __this, const MethodInfo* method);
#define CollectionMoveEvent_1_GetHashCode_m1029581758(__this, method) ((  int32_t (*) (CollectionMoveEvent_1_t2448589246 *, const MethodInfo*))CollectionMoveEvent_1_GetHashCode_m1029581758_gshared)(__this, method)
// System.Boolean UniRx.CollectionMoveEvent`1<UniRx.Tuple`2<System.Object,System.Object>>::Equals(UniRx.CollectionMoveEvent`1<T>)
extern "C"  bool CollectionMoveEvent_1_Equals_m2890172940_gshared (CollectionMoveEvent_1_t2448589246 * __this, CollectionMoveEvent_1_t2448589246  ___other0, const MethodInfo* method);
#define CollectionMoveEvent_1_Equals_m2890172940(__this, ___other0, method) ((  bool (*) (CollectionMoveEvent_1_t2448589246 *, CollectionMoveEvent_1_t2448589246 , const MethodInfo*))CollectionMoveEvent_1_Equals_m2890172940_gshared)(__this, ___other0, method)
