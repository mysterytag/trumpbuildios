﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Cell`1/<OnChanged>c__AnonStoreyF3<System.Boolean>
struct U3COnChangedU3Ec__AnonStoreyF3_t3587154105;

#include "codegen/il2cpp-codegen.h"

// System.Void Cell`1/<OnChanged>c__AnonStoreyF3<System.Boolean>::.ctor()
extern "C"  void U3COnChangedU3Ec__AnonStoreyF3__ctor_m4242813319_gshared (U3COnChangedU3Ec__AnonStoreyF3_t3587154105 * __this, const MethodInfo* method);
#define U3COnChangedU3Ec__AnonStoreyF3__ctor_m4242813319(__this, method) ((  void (*) (U3COnChangedU3Ec__AnonStoreyF3_t3587154105 *, const MethodInfo*))U3COnChangedU3Ec__AnonStoreyF3__ctor_m4242813319_gshared)(__this, method)
// System.Void Cell`1/<OnChanged>c__AnonStoreyF3<System.Boolean>::<>m__162(T)
extern "C"  void U3COnChangedU3Ec__AnonStoreyF3_U3CU3Em__162_m299397247_gshared (U3COnChangedU3Ec__AnonStoreyF3_t3587154105 * __this, bool ____0, const MethodInfo* method);
#define U3COnChangedU3Ec__AnonStoreyF3_U3CU3Em__162_m299397247(__this, ____0, method) ((  void (*) (U3COnChangedU3Ec__AnonStoreyF3_t3587154105 *, bool, const MethodInfo*))U3COnChangedU3Ec__AnonStoreyF3_U3CU3Em__162_m299397247_gshared)(__this, ____0, method)
