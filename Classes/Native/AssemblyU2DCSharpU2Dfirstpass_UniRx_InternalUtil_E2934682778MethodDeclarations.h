﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.InternalUtil.EmptyObserver`1<UnityEngine.Vector2>
struct EmptyObserver_1_t2934682778;
// System.Exception
struct Exception_t1967233988;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Exception1967233988.h"
#include "UnityEngine_UnityEngine_Vector23525329788.h"

// System.Void UniRx.InternalUtil.EmptyObserver`1<UnityEngine.Vector2>::.ctor()
extern "C"  void EmptyObserver_1__ctor_m4224020210_gshared (EmptyObserver_1_t2934682778 * __this, const MethodInfo* method);
#define EmptyObserver_1__ctor_m4224020210(__this, method) ((  void (*) (EmptyObserver_1_t2934682778 *, const MethodInfo*))EmptyObserver_1__ctor_m4224020210_gshared)(__this, method)
// System.Void UniRx.InternalUtil.EmptyObserver`1<UnityEngine.Vector2>::.cctor()
extern "C"  void EmptyObserver_1__cctor_m1613511419_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define EmptyObserver_1__cctor_m1613511419(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))EmptyObserver_1__cctor_m1613511419_gshared)(__this /* static, unused */, method)
// System.Void UniRx.InternalUtil.EmptyObserver`1<UnityEngine.Vector2>::OnCompleted()
extern "C"  void EmptyObserver_1_OnCompleted_m3020563708_gshared (EmptyObserver_1_t2934682778 * __this, const MethodInfo* method);
#define EmptyObserver_1_OnCompleted_m3020563708(__this, method) ((  void (*) (EmptyObserver_1_t2934682778 *, const MethodInfo*))EmptyObserver_1_OnCompleted_m3020563708_gshared)(__this, method)
// System.Void UniRx.InternalUtil.EmptyObserver`1<UnityEngine.Vector2>::OnError(System.Exception)
extern "C"  void EmptyObserver_1_OnError_m1201224745_gshared (EmptyObserver_1_t2934682778 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method);
#define EmptyObserver_1_OnError_m1201224745(__this, ___error0, method) ((  void (*) (EmptyObserver_1_t2934682778 *, Exception_t1967233988 *, const MethodInfo*))EmptyObserver_1_OnError_m1201224745_gshared)(__this, ___error0, method)
// System.Void UniRx.InternalUtil.EmptyObserver`1<UnityEngine.Vector2>::OnNext(T)
extern "C"  void EmptyObserver_1_OnNext_m3370030362_gshared (EmptyObserver_1_t2934682778 * __this, Vector2_t3525329788  ___value0, const MethodInfo* method);
#define EmptyObserver_1_OnNext_m3370030362(__this, ___value0, method) ((  void (*) (EmptyObserver_1_t2934682778 *, Vector2_t3525329788 , const MethodInfo*))EmptyObserver_1_OnNext_m3370030362_gshared)(__this, ___value0, method)
