﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,PlayFab.UUnit.Region>
struct KeyCollection_t4105386200;
// System.Collections.Generic.Dictionary`2<System.Object,PlayFab.UUnit.Region>
struct Dictionary_2_t1782110920;
// System.Object
struct Il2CppObject;
// System.Collections.Generic.IEnumerator`1<System.Object>
struct IEnumerator_1_t2320212868;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t287207039;
// System.Object[]
struct ObjectU5BU5D_t11523773;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_Array2840145358.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_K1549138861.h"

// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,PlayFab.UUnit.Region>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void KeyCollection__ctor_m3026266106_gshared (KeyCollection_t4105386200 * __this, Dictionary_2_t1782110920 * ___dictionary0, const MethodInfo* method);
#define KeyCollection__ctor_m3026266106(__this, ___dictionary0, method) ((  void (*) (KeyCollection_t4105386200 *, Dictionary_2_t1782110920 *, const MethodInfo*))KeyCollection__ctor_m3026266106_gshared)(__this, ___dictionary0, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,PlayFab.UUnit.Region>::System.Collections.Generic.ICollection<TKey>.Add(TKey)
extern "C"  void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m1360370972_gshared (KeyCollection_t4105386200 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m1360370972(__this, ___item0, method) ((  void (*) (KeyCollection_t4105386200 *, Il2CppObject *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m1360370972_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,PlayFab.UUnit.Region>::System.Collections.Generic.ICollection<TKey>.Clear()
extern "C"  void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m890788243_gshared (KeyCollection_t4105386200 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m890788243(__this, method) ((  void (*) (KeyCollection_t4105386200 *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m890788243_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,PlayFab.UUnit.Region>::System.Collections.Generic.ICollection<TKey>.Contains(TKey)
extern "C"  bool KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m427702322_gshared (KeyCollection_t4105386200 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m427702322(__this, ___item0, method) ((  bool (*) (KeyCollection_t4105386200 *, Il2CppObject *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m427702322_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,PlayFab.UUnit.Region>::System.Collections.Generic.ICollection<TKey>.Remove(TKey)
extern "C"  bool KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m3187609111_gshared (KeyCollection_t4105386200 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m3187609111(__this, ___item0, method) ((  bool (*) (KeyCollection_t4105386200 *, Il2CppObject *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m3187609111_gshared)(__this, ___item0, method)
// System.Collections.Generic.IEnumerator`1<TKey> System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,PlayFab.UUnit.Region>::System.Collections.Generic.IEnumerable<TKey>.GetEnumerator()
extern "C"  Il2CppObject* KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m4176485605_gshared (KeyCollection_t4105386200 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m4176485605(__this, method) ((  Il2CppObject* (*) (KeyCollection_t4105386200 *, const MethodInfo*))KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m4176485605_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,PlayFab.UUnit.Region>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void KeyCollection_System_Collections_ICollection_CopyTo_m2482600325_gshared (KeyCollection_t4105386200 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define KeyCollection_System_Collections_ICollection_CopyTo_m2482600325(__this, ___array0, ___index1, method) ((  void (*) (KeyCollection_t4105386200 *, Il2CppArray *, int32_t, const MethodInfo*))KeyCollection_System_Collections_ICollection_CopyTo_m2482600325_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,PlayFab.UUnit.Region>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * KeyCollection_System_Collections_IEnumerable_GetEnumerator_m275187028_gshared (KeyCollection_t4105386200 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_IEnumerable_GetEnumerator_m275187028(__this, method) ((  Il2CppObject * (*) (KeyCollection_t4105386200 *, const MethodInfo*))KeyCollection_System_Collections_IEnumerable_GetEnumerator_m275187028_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,PlayFab.UUnit.Region>::System.Collections.Generic.ICollection<TKey>.get_IsReadOnly()
extern "C"  bool KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m1586129683_gshared (KeyCollection_t4105386200 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m1586129683(__this, method) ((  bool (*) (KeyCollection_t4105386200 *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m1586129683_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,PlayFab.UUnit.Region>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool KeyCollection_System_Collections_ICollection_get_IsSynchronized_m3725631365_gshared (KeyCollection_t4105386200 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_ICollection_get_IsSynchronized_m3725631365(__this, method) ((  bool (*) (KeyCollection_t4105386200 *, const MethodInfo*))KeyCollection_System_Collections_ICollection_get_IsSynchronized_m3725631365_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,PlayFab.UUnit.Region>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * KeyCollection_System_Collections_ICollection_get_SyncRoot_m2698354551_gshared (KeyCollection_t4105386200 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_ICollection_get_SyncRoot_m2698354551(__this, method) ((  Il2CppObject * (*) (KeyCollection_t4105386200 *, const MethodInfo*))KeyCollection_System_Collections_ICollection_get_SyncRoot_m2698354551_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,PlayFab.UUnit.Region>::CopyTo(TKey[],System.Int32)
extern "C"  void KeyCollection_CopyTo_m798815919_gshared (KeyCollection_t4105386200 * __this, ObjectU5BU5D_t11523773* ___array0, int32_t ___index1, const MethodInfo* method);
#define KeyCollection_CopyTo_m798815919(__this, ___array0, ___index1, method) ((  void (*) (KeyCollection_t4105386200 *, ObjectU5BU5D_t11523773*, int32_t, const MethodInfo*))KeyCollection_CopyTo_m798815919_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,PlayFab.UUnit.Region>::GetEnumerator()
extern "C"  Enumerator_t1549138861  KeyCollection_GetEnumerator_m2079534716_gshared (KeyCollection_t4105386200 * __this, const MethodInfo* method);
#define KeyCollection_GetEnumerator_m2079534716(__this, method) ((  Enumerator_t1549138861  (*) (KeyCollection_t4105386200 *, const MethodInfo*))KeyCollection_GetEnumerator_m2079534716_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,PlayFab.UUnit.Region>::get_Count()
extern "C"  int32_t KeyCollection_get_Count_m1692608575_gshared (KeyCollection_t4105386200 * __this, const MethodInfo* method);
#define KeyCollection_get_Count_m1692608575(__this, method) ((  int32_t (*) (KeyCollection_t4105386200 *, const MethodInfo*))KeyCollection_get_Count_m1692608575_gshared)(__this, method)
