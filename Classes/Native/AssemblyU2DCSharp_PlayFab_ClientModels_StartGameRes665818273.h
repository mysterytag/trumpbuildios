﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "AssemblyU2DCSharp_PlayFab_Internal_PlayFabResultCom379512675.h"
#include "mscorlib_System_Nullable_1_gen1438485399.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PlayFab.ClientModels.StartGameResult
struct  StartGameResult_t665818273  : public PlayFabResultCommon_t379512675
{
public:
	// System.String PlayFab.ClientModels.StartGameResult::<LobbyID>k__BackingField
	String_t* ___U3CLobbyIDU3Ek__BackingField_2;
	// System.String PlayFab.ClientModels.StartGameResult::<ServerHostname>k__BackingField
	String_t* ___U3CServerHostnameU3Ek__BackingField_3;
	// System.Nullable`1<System.Int32> PlayFab.ClientModels.StartGameResult::<ServerPort>k__BackingField
	Nullable_1_t1438485399  ___U3CServerPortU3Ek__BackingField_4;
	// System.String PlayFab.ClientModels.StartGameResult::<Ticket>k__BackingField
	String_t* ___U3CTicketU3Ek__BackingField_5;
	// System.String PlayFab.ClientModels.StartGameResult::<Expires>k__BackingField
	String_t* ___U3CExpiresU3Ek__BackingField_6;
	// System.String PlayFab.ClientModels.StartGameResult::<Password>k__BackingField
	String_t* ___U3CPasswordU3Ek__BackingField_7;

public:
	inline static int32_t get_offset_of_U3CLobbyIDU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(StartGameResult_t665818273, ___U3CLobbyIDU3Ek__BackingField_2)); }
	inline String_t* get_U3CLobbyIDU3Ek__BackingField_2() const { return ___U3CLobbyIDU3Ek__BackingField_2; }
	inline String_t** get_address_of_U3CLobbyIDU3Ek__BackingField_2() { return &___U3CLobbyIDU3Ek__BackingField_2; }
	inline void set_U3CLobbyIDU3Ek__BackingField_2(String_t* value)
	{
		___U3CLobbyIDU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier(&___U3CLobbyIDU3Ek__BackingField_2, value);
	}

	inline static int32_t get_offset_of_U3CServerHostnameU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(StartGameResult_t665818273, ___U3CServerHostnameU3Ek__BackingField_3)); }
	inline String_t* get_U3CServerHostnameU3Ek__BackingField_3() const { return ___U3CServerHostnameU3Ek__BackingField_3; }
	inline String_t** get_address_of_U3CServerHostnameU3Ek__BackingField_3() { return &___U3CServerHostnameU3Ek__BackingField_3; }
	inline void set_U3CServerHostnameU3Ek__BackingField_3(String_t* value)
	{
		___U3CServerHostnameU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier(&___U3CServerHostnameU3Ek__BackingField_3, value);
	}

	inline static int32_t get_offset_of_U3CServerPortU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(StartGameResult_t665818273, ___U3CServerPortU3Ek__BackingField_4)); }
	inline Nullable_1_t1438485399  get_U3CServerPortU3Ek__BackingField_4() const { return ___U3CServerPortU3Ek__BackingField_4; }
	inline Nullable_1_t1438485399 * get_address_of_U3CServerPortU3Ek__BackingField_4() { return &___U3CServerPortU3Ek__BackingField_4; }
	inline void set_U3CServerPortU3Ek__BackingField_4(Nullable_1_t1438485399  value)
	{
		___U3CServerPortU3Ek__BackingField_4 = value;
	}

	inline static int32_t get_offset_of_U3CTicketU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(StartGameResult_t665818273, ___U3CTicketU3Ek__BackingField_5)); }
	inline String_t* get_U3CTicketU3Ek__BackingField_5() const { return ___U3CTicketU3Ek__BackingField_5; }
	inline String_t** get_address_of_U3CTicketU3Ek__BackingField_5() { return &___U3CTicketU3Ek__BackingField_5; }
	inline void set_U3CTicketU3Ek__BackingField_5(String_t* value)
	{
		___U3CTicketU3Ek__BackingField_5 = value;
		Il2CppCodeGenWriteBarrier(&___U3CTicketU3Ek__BackingField_5, value);
	}

	inline static int32_t get_offset_of_U3CExpiresU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(StartGameResult_t665818273, ___U3CExpiresU3Ek__BackingField_6)); }
	inline String_t* get_U3CExpiresU3Ek__BackingField_6() const { return ___U3CExpiresU3Ek__BackingField_6; }
	inline String_t** get_address_of_U3CExpiresU3Ek__BackingField_6() { return &___U3CExpiresU3Ek__BackingField_6; }
	inline void set_U3CExpiresU3Ek__BackingField_6(String_t* value)
	{
		___U3CExpiresU3Ek__BackingField_6 = value;
		Il2CppCodeGenWriteBarrier(&___U3CExpiresU3Ek__BackingField_6, value);
	}

	inline static int32_t get_offset_of_U3CPasswordU3Ek__BackingField_7() { return static_cast<int32_t>(offsetof(StartGameResult_t665818273, ___U3CPasswordU3Ek__BackingField_7)); }
	inline String_t* get_U3CPasswordU3Ek__BackingField_7() const { return ___U3CPasswordU3Ek__BackingField_7; }
	inline String_t** get_address_of_U3CPasswordU3Ek__BackingField_7() { return &___U3CPasswordU3Ek__BackingField_7; }
	inline void set_U3CPasswordU3Ek__BackingField_7(String_t* value)
	{
		___U3CPasswordU3Ek__BackingField_7 = value;
		Il2CppCodeGenWriteBarrier(&___U3CPasswordU3Ek__BackingField_7, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
