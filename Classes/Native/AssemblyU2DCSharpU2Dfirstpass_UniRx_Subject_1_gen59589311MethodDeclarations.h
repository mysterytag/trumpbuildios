﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Subject`1<UniRx.CollectionAddEvent`1<System.Object>>
struct Subject_1_t59589311;
// System.Exception
struct Exception_t1967233988;
// System.IDisposable
struct IDisposable_t1628921374;
// UniRx.IObserver`1<UniRx.CollectionAddEvent`1<System.Object>>
struct IObserver_1_t333553594;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Exception1967233988.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_CollectionAddE2416521987.h"

// System.Void UniRx.Subject`1<UniRx.CollectionAddEvent`1<System.Object>>::.ctor()
extern "C"  void Subject_1__ctor_m3489525238_gshared (Subject_1_t59589311 * __this, const MethodInfo* method);
#define Subject_1__ctor_m3489525238(__this, method) ((  void (*) (Subject_1_t59589311 *, const MethodInfo*))Subject_1__ctor_m3489525238_gshared)(__this, method)
// System.Boolean UniRx.Subject`1<UniRx.CollectionAddEvent`1<System.Object>>::get_HasObservers()
extern "C"  bool Subject_1_get_HasObservers_m873949142_gshared (Subject_1_t59589311 * __this, const MethodInfo* method);
#define Subject_1_get_HasObservers_m873949142(__this, method) ((  bool (*) (Subject_1_t59589311 *, const MethodInfo*))Subject_1_get_HasObservers_m873949142_gshared)(__this, method)
// System.Void UniRx.Subject`1<UniRx.CollectionAddEvent`1<System.Object>>::OnCompleted()
extern "C"  void Subject_1_OnCompleted_m2302693632_gshared (Subject_1_t59589311 * __this, const MethodInfo* method);
#define Subject_1_OnCompleted_m2302693632(__this, method) ((  void (*) (Subject_1_t59589311 *, const MethodInfo*))Subject_1_OnCompleted_m2302693632_gshared)(__this, method)
// System.Void UniRx.Subject`1<UniRx.CollectionAddEvent`1<System.Object>>::OnError(System.Exception)
extern "C"  void Subject_1_OnError_m989430317_gshared (Subject_1_t59589311 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method);
#define Subject_1_OnError_m989430317(__this, ___error0, method) ((  void (*) (Subject_1_t59589311 *, Exception_t1967233988 *, const MethodInfo*))Subject_1_OnError_m989430317_gshared)(__this, ___error0, method)
// System.Void UniRx.Subject`1<UniRx.CollectionAddEvent`1<System.Object>>::OnNext(T)
extern "C"  void Subject_1_OnNext_m1894998814_gshared (Subject_1_t59589311 * __this, CollectionAddEvent_1_t2416521987  ___value0, const MethodInfo* method);
#define Subject_1_OnNext_m1894998814(__this, ___value0, method) ((  void (*) (Subject_1_t59589311 *, CollectionAddEvent_1_t2416521987 , const MethodInfo*))Subject_1_OnNext_m1894998814_gshared)(__this, ___value0, method)
// System.IDisposable UniRx.Subject`1<UniRx.CollectionAddEvent`1<System.Object>>::Subscribe(UniRx.IObserver`1<T>)
extern "C"  Il2CppObject * Subject_1_Subscribe_m1021283445_gshared (Subject_1_t59589311 * __this, Il2CppObject* ___observer0, const MethodInfo* method);
#define Subject_1_Subscribe_m1021283445(__this, ___observer0, method) ((  Il2CppObject * (*) (Subject_1_t59589311 *, Il2CppObject*, const MethodInfo*))Subject_1_Subscribe_m1021283445_gshared)(__this, ___observer0, method)
// System.Void UniRx.Subject`1<UniRx.CollectionAddEvent`1<System.Object>>::Dispose()
extern "C"  void Subject_1_Dispose_m3257545907_gshared (Subject_1_t59589311 * __this, const MethodInfo* method);
#define Subject_1_Dispose_m3257545907(__this, method) ((  void (*) (Subject_1_t59589311 *, const MethodInfo*))Subject_1_Dispose_m3257545907_gshared)(__this, method)
// System.Void UniRx.Subject`1<UniRx.CollectionAddEvent`1<System.Object>>::ThrowIfDisposed()
extern "C"  void Subject_1_ThrowIfDisposed_m2578283324_gshared (Subject_1_t59589311 * __this, const MethodInfo* method);
#define Subject_1_ThrowIfDisposed_m2578283324(__this, method) ((  void (*) (Subject_1_t59589311 *, const MethodInfo*))Subject_1_ThrowIfDisposed_m2578283324_gshared)(__this, method)
// System.Boolean UniRx.Subject`1<UniRx.CollectionAddEvent`1<System.Object>>::IsRequiredSubscribeOnCurrentThread()
extern "C"  bool Subject_1_IsRequiredSubscribeOnCurrentThread_m1258670925_gshared (Subject_1_t59589311 * __this, const MethodInfo* method);
#define Subject_1_IsRequiredSubscribeOnCurrentThread_m1258670925(__this, method) ((  bool (*) (Subject_1_t59589311 *, const MethodInfo*))Subject_1_IsRequiredSubscribeOnCurrentThread_m1258670925_gshared)(__this, method)
