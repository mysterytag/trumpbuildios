﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// IEmptyStream
struct IEmptyStream_t3684082468;
// ICell`1<System.Boolean>
struct ICell_1_t1762636318;

#include "codegen/il2cpp-codegen.h"

// IEmptyStream CellReactiveApi::When(ICell`1<System.Boolean>)
extern "C"  Il2CppObject * CellReactiveApi_When_m864948324 (Il2CppObject * __this /* static, unused */, Il2CppObject* ___cell0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ICell`1<System.Boolean> CellReactiveApi::Not(ICell`1<System.Boolean>)
extern "C"  Il2CppObject* CellReactiveApi_Not_m1728782658 (Il2CppObject * __this /* static, unused */, Il2CppObject* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean CellReactiveApi::<When>m__17E(System.Boolean)
extern "C"  bool CellReactiveApi_U3CWhenU3Em__17E_m2162727733 (Il2CppObject * __this /* static, unused */, bool ___i0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean CellReactiveApi::<Not>m__18F(System.Boolean)
extern "C"  bool CellReactiveApi_U3CNotU3Em__18F_m2417911214 (Il2CppObject * __this /* static, unused */, bool ___val0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
