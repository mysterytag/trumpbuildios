﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Generic.IEqualityComparer`1<TableButtons/StatkaPoTableViev>
struct IEqualityComparer_1_t1537695172;
// UniRx.Subject`1<TableButtons/StatkaPoTableViev>
struct Subject_1_t1151463141;
// System.IDisposable
struct IDisposable_t1628921374;

#include "mscorlib_System_Object837106420.h"
#include "AssemblyU2DCSharp_TableButtons_StatkaPoTableViev3508395817.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.ReactiveProperty`1<TableButtons/StatkaPoTableViev>
struct  ReactiveProperty_1_t4116728855  : public Il2CppObject
{
public:
	// System.Boolean UniRx.ReactiveProperty`1::canPublishValueOnSubscribe
	bool ___canPublishValueOnSubscribe_1;
	// System.Boolean UniRx.ReactiveProperty`1::isDisposed
	bool ___isDisposed_2;
	// T UniRx.ReactiveProperty`1::value
	int32_t ___value_3;
	// UniRx.Subject`1<T> UniRx.ReactiveProperty`1::publisher
	Subject_1_t1151463141 * ___publisher_4;
	// System.IDisposable UniRx.ReactiveProperty`1::sourceConnection
	Il2CppObject * ___sourceConnection_5;

public:
	inline static int32_t get_offset_of_canPublishValueOnSubscribe_1() { return static_cast<int32_t>(offsetof(ReactiveProperty_1_t4116728855, ___canPublishValueOnSubscribe_1)); }
	inline bool get_canPublishValueOnSubscribe_1() const { return ___canPublishValueOnSubscribe_1; }
	inline bool* get_address_of_canPublishValueOnSubscribe_1() { return &___canPublishValueOnSubscribe_1; }
	inline void set_canPublishValueOnSubscribe_1(bool value)
	{
		___canPublishValueOnSubscribe_1 = value;
	}

	inline static int32_t get_offset_of_isDisposed_2() { return static_cast<int32_t>(offsetof(ReactiveProperty_1_t4116728855, ___isDisposed_2)); }
	inline bool get_isDisposed_2() const { return ___isDisposed_2; }
	inline bool* get_address_of_isDisposed_2() { return &___isDisposed_2; }
	inline void set_isDisposed_2(bool value)
	{
		___isDisposed_2 = value;
	}

	inline static int32_t get_offset_of_value_3() { return static_cast<int32_t>(offsetof(ReactiveProperty_1_t4116728855, ___value_3)); }
	inline int32_t get_value_3() const { return ___value_3; }
	inline int32_t* get_address_of_value_3() { return &___value_3; }
	inline void set_value_3(int32_t value)
	{
		___value_3 = value;
	}

	inline static int32_t get_offset_of_publisher_4() { return static_cast<int32_t>(offsetof(ReactiveProperty_1_t4116728855, ___publisher_4)); }
	inline Subject_1_t1151463141 * get_publisher_4() const { return ___publisher_4; }
	inline Subject_1_t1151463141 ** get_address_of_publisher_4() { return &___publisher_4; }
	inline void set_publisher_4(Subject_1_t1151463141 * value)
	{
		___publisher_4 = value;
		Il2CppCodeGenWriteBarrier(&___publisher_4, value);
	}

	inline static int32_t get_offset_of_sourceConnection_5() { return static_cast<int32_t>(offsetof(ReactiveProperty_1_t4116728855, ___sourceConnection_5)); }
	inline Il2CppObject * get_sourceConnection_5() const { return ___sourceConnection_5; }
	inline Il2CppObject ** get_address_of_sourceConnection_5() { return &___sourceConnection_5; }
	inline void set_sourceConnection_5(Il2CppObject * value)
	{
		___sourceConnection_5 = value;
		Il2CppCodeGenWriteBarrier(&___sourceConnection_5, value);
	}
};

struct ReactiveProperty_1_t4116728855_StaticFields
{
public:
	// System.Collections.Generic.IEqualityComparer`1<T> UniRx.ReactiveProperty`1::defaultEqualityComparer
	Il2CppObject* ___defaultEqualityComparer_0;

public:
	inline static int32_t get_offset_of_defaultEqualityComparer_0() { return static_cast<int32_t>(offsetof(ReactiveProperty_1_t4116728855_StaticFields, ___defaultEqualityComparer_0)); }
	inline Il2CppObject* get_defaultEqualityComparer_0() const { return ___defaultEqualityComparer_0; }
	inline Il2CppObject** get_address_of_defaultEqualityComparer_0() { return &___defaultEqualityComparer_0; }
	inline void set_defaultEqualityComparer_0(Il2CppObject* value)
	{
		___defaultEqualityComparer_0 = value;
		Il2CppCodeGenWriteBarrier(&___defaultEqualityComparer_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
