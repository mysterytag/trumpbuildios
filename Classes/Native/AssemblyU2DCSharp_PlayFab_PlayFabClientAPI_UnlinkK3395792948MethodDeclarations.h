﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.PlayFabClientAPI/UnlinkKongregateResponseCallback
struct UnlinkKongregateResponseCallback_t3395792948;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;
// PlayFab.ClientModels.UnlinkKongregateAccountRequest
struct UnlinkKongregateAccountRequest_t2566546368;
// PlayFab.ClientModels.UnlinkKongregateAccountResult
struct UnlinkKongregateAccountResult_t2611791596;
// PlayFab.PlayFabError
struct PlayFabError_t750598646;
// System.IAsyncResult
struct IAsyncResult_t537683269;
// System.AsyncCallback
struct AsyncCallback_t1363551830;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_IntPtr676692020.h"
#include "mscorlib_System_String968488902.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_UnlinkKongr2566546368.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_UnlinkKongr2611791596.h"
#include "AssemblyU2DCSharpU2Dfirstpass_PlayFab_PlayFabError750598646.h"
#include "mscorlib_System_AsyncCallback1363551830.h"

// System.Void PlayFab.PlayFabClientAPI/UnlinkKongregateResponseCallback::.ctor(System.Object,System.IntPtr)
extern "C"  void UnlinkKongregateResponseCallback__ctor_m3386910499 (UnlinkKongregateResponseCallback_t3395792948 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/UnlinkKongregateResponseCallback::Invoke(System.String,System.Int32,PlayFab.ClientModels.UnlinkKongregateAccountRequest,PlayFab.ClientModels.UnlinkKongregateAccountResult,PlayFab.PlayFabError,System.Object)
extern "C"  void UnlinkKongregateResponseCallback_Invoke_m2597132428 (UnlinkKongregateResponseCallback_t3395792948 * __this, String_t* ___urlPath0, int32_t ___callId1, UnlinkKongregateAccountRequest_t2566546368 * ___request2, UnlinkKongregateAccountResult_t2611791596 * ___result3, PlayFabError_t750598646 * ___error4, Il2CppObject * ___customData5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
extern "C" void pinvoke_delegate_wrapper_UnlinkKongregateResponseCallback_t3395792948(Il2CppObject* delegate, String_t* ___urlPath0, int32_t ___callId1, UnlinkKongregateAccountRequest_t2566546368 * ___request2, UnlinkKongregateAccountResult_t2611791596 * ___result3, PlayFabError_t750598646 * ___error4, Il2CppObject * ___customData5);
// System.IAsyncResult PlayFab.PlayFabClientAPI/UnlinkKongregateResponseCallback::BeginInvoke(System.String,System.Int32,PlayFab.ClientModels.UnlinkKongregateAccountRequest,PlayFab.ClientModels.UnlinkKongregateAccountResult,PlayFab.PlayFabError,System.Object,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * UnlinkKongregateResponseCallback_BeginInvoke_m1671678145 (UnlinkKongregateResponseCallback_t3395792948 * __this, String_t* ___urlPath0, int32_t ___callId1, UnlinkKongregateAccountRequest_t2566546368 * ___request2, UnlinkKongregateAccountResult_t2611791596 * ___result3, PlayFabError_t750598646 * ___error4, Il2CppObject * ___customData5, AsyncCallback_t1363551830 * ___callback6, Il2CppObject * ___object7, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/UnlinkKongregateResponseCallback::EndInvoke(System.IAsyncResult)
extern "C"  void UnlinkKongregateResponseCallback_EndInvoke_m475177651 (UnlinkKongregateResponseCallback_t3395792948 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
