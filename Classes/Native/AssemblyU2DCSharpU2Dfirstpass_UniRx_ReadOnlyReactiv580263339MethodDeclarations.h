﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.ReadOnlyReactiveProperty`1/ReadOnlyReactivePropertyObserver<UnityEngine.Color>
struct ReadOnlyReactivePropertyObserver_t580263339;
// UniRx.ReadOnlyReactiveProperty`1<UnityEngine.Color>
struct ReadOnlyReactiveProperty_1_t82610480;
// System.Exception
struct Exception_t1967233988;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Color1588175760.h"
#include "mscorlib_System_Exception1967233988.h"

// System.Void UniRx.ReadOnlyReactiveProperty`1/ReadOnlyReactivePropertyObserver<UnityEngine.Color>::.ctor(UniRx.ReadOnlyReactiveProperty`1<T>)
extern "C"  void ReadOnlyReactivePropertyObserver__ctor_m2616608040_gshared (ReadOnlyReactivePropertyObserver_t580263339 * __this, ReadOnlyReactiveProperty_1_t82610480 * ___parent0, const MethodInfo* method);
#define ReadOnlyReactivePropertyObserver__ctor_m2616608040(__this, ___parent0, method) ((  void (*) (ReadOnlyReactivePropertyObserver_t580263339 *, ReadOnlyReactiveProperty_1_t82610480 *, const MethodInfo*))ReadOnlyReactivePropertyObserver__ctor_m2616608040_gshared)(__this, ___parent0, method)
// System.Void UniRx.ReadOnlyReactiveProperty`1/ReadOnlyReactivePropertyObserver<UnityEngine.Color>::OnNext(T)
extern "C"  void ReadOnlyReactivePropertyObserver_OnNext_m1866437_gshared (ReadOnlyReactivePropertyObserver_t580263339 * __this, Color_t1588175760  ___value0, const MethodInfo* method);
#define ReadOnlyReactivePropertyObserver_OnNext_m1866437(__this, ___value0, method) ((  void (*) (ReadOnlyReactivePropertyObserver_t580263339 *, Color_t1588175760 , const MethodInfo*))ReadOnlyReactivePropertyObserver_OnNext_m1866437_gshared)(__this, ___value0, method)
// System.Void UniRx.ReadOnlyReactiveProperty`1/ReadOnlyReactivePropertyObserver<UnityEngine.Color>::OnError(System.Exception)
extern "C"  void ReadOnlyReactivePropertyObserver_OnError_m971271124_gshared (ReadOnlyReactivePropertyObserver_t580263339 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method);
#define ReadOnlyReactivePropertyObserver_OnError_m971271124(__this, ___error0, method) ((  void (*) (ReadOnlyReactivePropertyObserver_t580263339 *, Exception_t1967233988 *, const MethodInfo*))ReadOnlyReactivePropertyObserver_OnError_m971271124_gshared)(__this, ___error0, method)
// System.Void UniRx.ReadOnlyReactiveProperty`1/ReadOnlyReactivePropertyObserver<UnityEngine.Color>::OnCompleted()
extern "C"  void ReadOnlyReactivePropertyObserver_OnCompleted_m2838969639_gshared (ReadOnlyReactivePropertyObserver_t580263339 * __this, const MethodInfo* method);
#define ReadOnlyReactivePropertyObserver_OnCompleted_m2838969639(__this, method) ((  void (*) (ReadOnlyReactivePropertyObserver_t580263339 *, const MethodInfo*))ReadOnlyReactivePropertyObserver_OnCompleted_m2838969639_gshared)(__this, method)
