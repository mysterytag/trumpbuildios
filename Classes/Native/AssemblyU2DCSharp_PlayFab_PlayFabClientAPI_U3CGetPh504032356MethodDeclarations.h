﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.PlayFabClientAPI/<GetPhotonAuthenticationToken>c__AnonStorey5E
struct U3CGetPhotonAuthenticationTokenU3Ec__AnonStorey5E_t504032356;
// PlayFab.CallRequestContainer
struct CallRequestContainer_t432318609;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharpU2Dfirstpass_PlayFab_CallRequestCo432318609.h"

// System.Void PlayFab.PlayFabClientAPI/<GetPhotonAuthenticationToken>c__AnonStorey5E::.ctor()
extern "C"  void U3CGetPhotonAuthenticationTokenU3Ec__AnonStorey5E__ctor_m3360278479 (U3CGetPhotonAuthenticationTokenU3Ec__AnonStorey5E_t504032356 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/<GetPhotonAuthenticationToken>c__AnonStorey5E::<>m__4B(PlayFab.CallRequestContainer)
extern "C"  void U3CGetPhotonAuthenticationTokenU3Ec__AnonStorey5E_U3CU3Em__4B_m3782810747 (U3CGetPhotonAuthenticationTokenU3Ec__AnonStorey5E_t504032356 * __this, CallRequestContainer_t432318609 * ___requestContainer0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
