﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.PlayFabClientAPI/GetCharacterStatisticsRequestCallback
struct GetCharacterStatisticsRequestCallback_t3212398334;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;
// PlayFab.ClientModels.GetCharacterStatisticsRequest
struct GetCharacterStatisticsRequest_t3373629353;
// System.IAsyncResult
struct IAsyncResult_t537683269;
// System.AsyncCallback
struct AsyncCallback_t1363551830;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_IntPtr676692020.h"
#include "mscorlib_System_String968488902.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_GetCharacte3373629353.h"
#include "mscorlib_System_AsyncCallback1363551830.h"

// System.Void PlayFab.PlayFabClientAPI/GetCharacterStatisticsRequestCallback::.ctor(System.Object,System.IntPtr)
extern "C"  void GetCharacterStatisticsRequestCallback__ctor_m288249997 (GetCharacterStatisticsRequestCallback_t3212398334 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/GetCharacterStatisticsRequestCallback::Invoke(System.String,System.Int32,PlayFab.ClientModels.GetCharacterStatisticsRequest,System.Object)
extern "C"  void GetCharacterStatisticsRequestCallback_Invoke_m3674213597 (GetCharacterStatisticsRequestCallback_t3212398334 * __this, String_t* ___urlPath0, int32_t ___callId1, GetCharacterStatisticsRequest_t3373629353 * ___request2, Il2CppObject * ___customData3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
extern "C" void pinvoke_delegate_wrapper_GetCharacterStatisticsRequestCallback_t3212398334(Il2CppObject* delegate, String_t* ___urlPath0, int32_t ___callId1, GetCharacterStatisticsRequest_t3373629353 * ___request2, Il2CppObject * ___customData3);
// System.IAsyncResult PlayFab.PlayFabClientAPI/GetCharacterStatisticsRequestCallback::BeginInvoke(System.String,System.Int32,PlayFab.ClientModels.GetCharacterStatisticsRequest,System.Object,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * GetCharacterStatisticsRequestCallback_BeginInvoke_m3528111902 (GetCharacterStatisticsRequestCallback_t3212398334 * __this, String_t* ___urlPath0, int32_t ___callId1, GetCharacterStatisticsRequest_t3373629353 * ___request2, Il2CppObject * ___customData3, AsyncCallback_t1363551830 * ___callback4, Il2CppObject * ___object5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/GetCharacterStatisticsRequestCallback::EndInvoke(System.IAsyncResult)
extern "C"  void GetCharacterStatisticsRequestCallback_EndInvoke_m2826311965 (GetCharacterStatisticsRequestCallback_t3212398334 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
