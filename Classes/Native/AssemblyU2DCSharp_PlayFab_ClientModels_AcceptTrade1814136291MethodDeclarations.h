﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.ClientModels.AcceptTradeRequest
struct AcceptTradeRequest_t1814136291;
// System.String
struct String_t;
// System.Collections.Generic.List`1<System.String>
struct List_1_t1765447871;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String968488902.h"

// System.Void PlayFab.ClientModels.AcceptTradeRequest::.ctor()
extern "C"  void AcceptTradeRequest__ctor_m3501037370 (AcceptTradeRequest_t1814136291 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PlayFab.ClientModels.AcceptTradeRequest::get_OfferingPlayerId()
extern "C"  String_t* AcceptTradeRequest_get_OfferingPlayerId_m2443613010 (AcceptTradeRequest_t1814136291 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.AcceptTradeRequest::set_OfferingPlayerId(System.String)
extern "C"  void AcceptTradeRequest_set_OfferingPlayerId_m301693017 (AcceptTradeRequest_t1814136291 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PlayFab.ClientModels.AcceptTradeRequest::get_TradeId()
extern "C"  String_t* AcceptTradeRequest_get_TradeId_m3226848561 (AcceptTradeRequest_t1814136291 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.AcceptTradeRequest::set_TradeId(System.String)
extern "C"  void AcceptTradeRequest_set_TradeId_m2110973928 (AcceptTradeRequest_t1814136291 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<System.String> PlayFab.ClientModels.AcceptTradeRequest::get_AcceptedInventoryInstanceIds()
extern "C"  List_1_t1765447871 * AcceptTradeRequest_get_AcceptedInventoryInstanceIds_m1525545072 (AcceptTradeRequest_t1814136291 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.AcceptTradeRequest::set_AcceptedInventoryInstanceIds(System.Collections.Generic.List`1<System.String>)
extern "C"  void AcceptTradeRequest_set_AcceptedInventoryInstanceIds_m2225062569 (AcceptTradeRequest_t1814136291 * __this, List_1_t1765447871 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
