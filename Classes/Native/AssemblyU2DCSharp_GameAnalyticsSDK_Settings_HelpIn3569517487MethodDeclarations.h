﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"


// Methods for marshaling
struct HelpInfo_t3569517487;
struct HelpInfo_t3569517487_marshaled_pinvoke;

extern "C" void HelpInfo_t3569517487_marshal_pinvoke(const HelpInfo_t3569517487& unmarshaled, HelpInfo_t3569517487_marshaled_pinvoke& marshaled);
extern "C" void HelpInfo_t3569517487_marshal_pinvoke_back(const HelpInfo_t3569517487_marshaled_pinvoke& marshaled, HelpInfo_t3569517487& unmarshaled);
extern "C" void HelpInfo_t3569517487_marshal_pinvoke_cleanup(HelpInfo_t3569517487_marshaled_pinvoke& marshaled);

// Methods for marshaling
struct HelpInfo_t3569517487;
struct HelpInfo_t3569517487_marshaled_com;

extern "C" void HelpInfo_t3569517487_marshal_com(const HelpInfo_t3569517487& unmarshaled, HelpInfo_t3569517487_marshaled_com& marshaled);
extern "C" void HelpInfo_t3569517487_marshal_com_back(const HelpInfo_t3569517487_marshaled_com& marshaled, HelpInfo_t3569517487& unmarshaled);
extern "C" void HelpInfo_t3569517487_marshal_com_cleanup(HelpInfo_t3569517487_marshaled_com& marshaled);
