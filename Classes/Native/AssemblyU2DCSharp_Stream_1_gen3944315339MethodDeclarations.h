﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Stream`1<System.Single>
struct Stream_1_t3944315339;
// System.IDisposable
struct IDisposable_t1628921374;
// System.Action`1<System.Single>
struct Action_1_t1106661726;
// System.Action
struct Action_t437523947;
// IStream`1<System.Single>
struct IStream_1_t1510900012;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Priority3194150340.h"
#include "System_Core_System_Action437523947.h"

// System.Void Stream`1<System.Single>::.ctor()
extern "C"  void Stream_1__ctor_m1692350921_gshared (Stream_1_t3944315339 * __this, const MethodInfo* method);
#define Stream_1__ctor_m1692350921(__this, method) ((  void (*) (Stream_1_t3944315339 *, const MethodInfo*))Stream_1__ctor_m1692350921_gshared)(__this, method)
// System.Void Stream`1<System.Single>::Send(T)
extern "C"  void Stream_1_Send_m221568475_gshared (Stream_1_t3944315339 * __this, float ___value0, const MethodInfo* method);
#define Stream_1_Send_m221568475(__this, ___value0, method) ((  void (*) (Stream_1_t3944315339 *, float, const MethodInfo*))Stream_1_Send_m221568475_gshared)(__this, ___value0, method)
// System.Void Stream`1<System.Single>::SendInTransaction(T,System.Int32)
extern "C"  void Stream_1_SendInTransaction_m166372785_gshared (Stream_1_t3944315339 * __this, float ___value0, int32_t ___p1, const MethodInfo* method);
#define Stream_1_SendInTransaction_m166372785(__this, ___value0, ___p1, method) ((  void (*) (Stream_1_t3944315339 *, float, int32_t, const MethodInfo*))Stream_1_SendInTransaction_m166372785_gshared)(__this, ___value0, ___p1, method)
// System.IDisposable Stream`1<System.Single>::Listen(System.Action`1<T>,Priority)
extern "C"  Il2CppObject * Stream_1_Listen_m89795891_gshared (Stream_1_t3944315339 * __this, Action_1_t1106661726 * ___action0, int32_t ___priority1, const MethodInfo* method);
#define Stream_1_Listen_m89795891(__this, ___action0, ___priority1, method) ((  Il2CppObject * (*) (Stream_1_t3944315339 *, Action_1_t1106661726 *, int32_t, const MethodInfo*))Stream_1_Listen_m89795891_gshared)(__this, ___action0, ___priority1, method)
// System.IDisposable Stream`1<System.Single>::Listen(System.Action,Priority)
extern "C"  Il2CppObject * Stream_1_Listen_m2354454548_gshared (Stream_1_t3944315339 * __this, Action_t437523947 * ___action0, int32_t ___p1, const MethodInfo* method);
#define Stream_1_Listen_m2354454548(__this, ___action0, ___p1, method) ((  Il2CppObject * (*) (Stream_1_t3944315339 *, Action_t437523947 *, int32_t, const MethodInfo*))Stream_1_Listen_m2354454548_gshared)(__this, ___action0, ___p1, method)
// System.Void Stream`1<System.Single>::Dispose()
extern "C"  void Stream_1_Dispose_m2749880262_gshared (Stream_1_t3944315339 * __this, const MethodInfo* method);
#define Stream_1_Dispose_m2749880262(__this, method) ((  void (*) (Stream_1_t3944315339 *, const MethodInfo*))Stream_1_Dispose_m2749880262_gshared)(__this, method)
// System.Void Stream`1<System.Single>::SetInputStream(IStream`1<T>)
extern "C"  void Stream_1_SetInputStream_m3169795543_gshared (Stream_1_t3944315339 * __this, Il2CppObject* ___stream0, const MethodInfo* method);
#define Stream_1_SetInputStream_m3169795543(__this, ___stream0, method) ((  void (*) (Stream_1_t3944315339 *, Il2CppObject*, const MethodInfo*))Stream_1_SetInputStream_m3169795543_gshared)(__this, ___stream0, method)
// System.Void Stream`1<System.Single>::ClearInputStream()
extern "C"  void Stream_1_ClearInputStream_m3480420216_gshared (Stream_1_t3944315339 * __this, const MethodInfo* method);
#define Stream_1_ClearInputStream_m3480420216(__this, method) ((  void (*) (Stream_1_t3944315339 *, const MethodInfo*))Stream_1_ClearInputStream_m3480420216_gshared)(__this, method)
// System.Void Stream`1<System.Single>::TransactionIterationFinished()
extern "C"  void Stream_1_TransactionIterationFinished_m3334163276_gshared (Stream_1_t3944315339 * __this, const MethodInfo* method);
#define Stream_1_TransactionIterationFinished_m3334163276(__this, method) ((  void (*) (Stream_1_t3944315339 *, const MethodInfo*))Stream_1_TransactionIterationFinished_m3334163276_gshared)(__this, method)
// System.Void Stream`1<System.Single>::Unpack(System.Int32)
extern "C"  void Stream_1_Unpack_m3481119070_gshared (Stream_1_t3944315339 * __this, int32_t ___p0, const MethodInfo* method);
#define Stream_1_Unpack_m3481119070(__this, ___p0, method) ((  void (*) (Stream_1_t3944315339 *, int32_t, const MethodInfo*))Stream_1_Unpack_m3481119070_gshared)(__this, ___p0, method)
// System.Void Stream`1<System.Single>::<SetInputStream>m__1BA(T)
extern "C"  void Stream_1_U3CSetInputStreamU3Em__1BA_m532101158_gshared (Stream_1_t3944315339 * __this, float ___val0, const MethodInfo* method);
#define Stream_1_U3CSetInputStreamU3Em__1BA_m532101158(__this, ___val0, method) ((  void (*) (Stream_1_t3944315339 *, float, const MethodInfo*))Stream_1_U3CSetInputStreamU3Em__1BA_m532101158_gshared)(__this, ___val0, method)
