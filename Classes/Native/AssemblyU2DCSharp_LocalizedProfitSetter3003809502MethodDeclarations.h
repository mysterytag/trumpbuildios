﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// LocalizedProfitSetter
struct LocalizedProfitSetter_t3003809502;

#include "codegen/il2cpp-codegen.h"

// System.Void LocalizedProfitSetter::.ctor()
extern "C"  void LocalizedProfitSetter__ctor_m1492800589 (LocalizedProfitSetter_t3003809502 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LocalizedProfitSetter::Awake()
extern "C"  void LocalizedProfitSetter_Awake_m1730405808 (LocalizedProfitSetter_t3003809502 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
