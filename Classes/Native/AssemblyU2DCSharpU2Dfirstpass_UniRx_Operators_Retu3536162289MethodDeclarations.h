﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.ReturnObservable`1<System.Int32>
struct ReturnObservable_1_t3536162289;
// UniRx.IScheduler
struct IScheduler_t2938318244;
// System.IDisposable
struct IDisposable_t1628921374;
// UniRx.IObserver`1<System.Int32>
struct IObserver_1_t764446394;

#include "codegen/il2cpp-codegen.h"

// System.Void UniRx.Operators.ReturnObservable`1<System.Int32>::.ctor(T,UniRx.IScheduler)
extern "C"  void ReturnObservable_1__ctor_m951247135_gshared (ReturnObservable_1_t3536162289 * __this, int32_t ___value0, Il2CppObject * ___scheduler1, const MethodInfo* method);
#define ReturnObservable_1__ctor_m951247135(__this, ___value0, ___scheduler1, method) ((  void (*) (ReturnObservable_1_t3536162289 *, int32_t, Il2CppObject *, const MethodInfo*))ReturnObservable_1__ctor_m951247135_gshared)(__this, ___value0, ___scheduler1, method)
// System.IDisposable UniRx.Operators.ReturnObservable`1<System.Int32>::SubscribeCore(UniRx.IObserver`1<T>,System.IDisposable)
extern "C"  Il2CppObject * ReturnObservable_1_SubscribeCore_m2890325917_gshared (ReturnObservable_1_t3536162289 * __this, Il2CppObject* ___observer0, Il2CppObject * ___cancel1, const MethodInfo* method);
#define ReturnObservable_1_SubscribeCore_m2890325917(__this, ___observer0, ___cancel1, method) ((  Il2CppObject * (*) (ReturnObservable_1_t3536162289 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))ReturnObservable_1_SubscribeCore_m2890325917_gshared)(__this, ___observer0, ___cancel1, method)
