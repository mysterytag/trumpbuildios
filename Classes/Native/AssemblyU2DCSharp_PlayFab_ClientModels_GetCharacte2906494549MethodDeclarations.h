﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.ClientModels.GetCharacterLeaderboardRequest
struct GetCharacterLeaderboardRequest_t2906494549;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String968488902.h"
#include "mscorlib_System_Nullable_1_gen1438485399.h"

// System.Void PlayFab.ClientModels.GetCharacterLeaderboardRequest::.ctor()
extern "C"  void GetCharacterLeaderboardRequest__ctor_m1835956232 (GetCharacterLeaderboardRequest_t2906494549 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PlayFab.ClientModels.GetCharacterLeaderboardRequest::get_CharacterType()
extern "C"  String_t* GetCharacterLeaderboardRequest_get_CharacterType_m3630568227 (GetCharacterLeaderboardRequest_t2906494549 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.GetCharacterLeaderboardRequest::set_CharacterType(System.String)
extern "C"  void GetCharacterLeaderboardRequest_set_CharacterType_m2156484982 (GetCharacterLeaderboardRequest_t2906494549 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PlayFab.ClientModels.GetCharacterLeaderboardRequest::get_StatisticName()
extern "C"  String_t* GetCharacterLeaderboardRequest_get_StatisticName_m2182656795 (GetCharacterLeaderboardRequest_t2906494549 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.GetCharacterLeaderboardRequest::set_StatisticName(System.String)
extern "C"  void GetCharacterLeaderboardRequest_set_StatisticName_m925658238 (GetCharacterLeaderboardRequest_t2906494549 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 PlayFab.ClientModels.GetCharacterLeaderboardRequest::get_StartPosition()
extern "C"  int32_t GetCharacterLeaderboardRequest_get_StartPosition_m1705966518 (GetCharacterLeaderboardRequest_t2906494549 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.GetCharacterLeaderboardRequest::set_StartPosition(System.Int32)
extern "C"  void GetCharacterLeaderboardRequest_set_StartPosition_m1286607877 (GetCharacterLeaderboardRequest_t2906494549 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Nullable`1<System.Int32> PlayFab.ClientModels.GetCharacterLeaderboardRequest::get_MaxResultsCount()
extern "C"  Nullable_1_t1438485399  GetCharacterLeaderboardRequest_get_MaxResultsCount_m1417150697 (GetCharacterLeaderboardRequest_t2906494549 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.GetCharacterLeaderboardRequest::set_MaxResultsCount(System.Nullable`1<System.Int32>)
extern "C"  void GetCharacterLeaderboardRequest_set_MaxResultsCount_m1592714440 (GetCharacterLeaderboardRequest_t2906494549 * __this, Nullable_1_t1438485399  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
