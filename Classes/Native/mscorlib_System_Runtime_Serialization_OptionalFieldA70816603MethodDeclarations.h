﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Runtime.Serialization.OptionalFieldAttribute
struct OptionalFieldAttribute_t70816603;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Runtime.Serialization.OptionalFieldAttribute::.ctor()
extern "C"  void OptionalFieldAttribute__ctor_m2952806175 (OptionalFieldAttribute_t70816603 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
