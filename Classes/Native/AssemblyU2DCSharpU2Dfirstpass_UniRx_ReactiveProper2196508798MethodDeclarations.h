﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.ReactiveProperty`1<UnityEngine.Color>
struct ReactiveProperty_1_t2196508798;
// UniRx.IObservable`1<UnityEngine.Color>
struct IObservable_1_t1346974124;
// System.Collections.Generic.IEqualityComparer`1<UnityEngine.Color>
struct IEqualityComparer_1_t3912442411;
// System.IDisposable
struct IDisposable_t1628921374;
// UniRx.IObserver`1<UnityEngine.Color>
struct IObserver_1_t3800174663;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Color1588175760.h"

// System.Void UniRx.ReactiveProperty`1<UnityEngine.Color>::.ctor()
extern "C"  void ReactiveProperty_1__ctor_m1209776928_gshared (ReactiveProperty_1_t2196508798 * __this, const MethodInfo* method);
#define ReactiveProperty_1__ctor_m1209776928(__this, method) ((  void (*) (ReactiveProperty_1_t2196508798 *, const MethodInfo*))ReactiveProperty_1__ctor_m1209776928_gshared)(__this, method)
// System.Void UniRx.ReactiveProperty`1<UnityEngine.Color>::.ctor(T)
extern "C"  void ReactiveProperty_1__ctor_m3143347774_gshared (ReactiveProperty_1_t2196508798 * __this, Color_t1588175760  ___initialValue0, const MethodInfo* method);
#define ReactiveProperty_1__ctor_m3143347774(__this, ___initialValue0, method) ((  void (*) (ReactiveProperty_1_t2196508798 *, Color_t1588175760 , const MethodInfo*))ReactiveProperty_1__ctor_m3143347774_gshared)(__this, ___initialValue0, method)
// System.Void UniRx.ReactiveProperty`1<UnityEngine.Color>::.ctor(UniRx.IObservable`1<T>)
extern "C"  void ReactiveProperty_1__ctor_m3815475455_gshared (ReactiveProperty_1_t2196508798 * __this, Il2CppObject* ___source0, const MethodInfo* method);
#define ReactiveProperty_1__ctor_m3815475455(__this, ___source0, method) ((  void (*) (ReactiveProperty_1_t2196508798 *, Il2CppObject*, const MethodInfo*))ReactiveProperty_1__ctor_m3815475455_gshared)(__this, ___source0, method)
// System.Void UniRx.ReactiveProperty`1<UnityEngine.Color>::.ctor(UniRx.IObservable`1<T>,T)
extern "C"  void ReactiveProperty_1__ctor_m3064814295_gshared (ReactiveProperty_1_t2196508798 * __this, Il2CppObject* ___source0, Color_t1588175760  ___initialValue1, const MethodInfo* method);
#define ReactiveProperty_1__ctor_m3064814295(__this, ___source0, ___initialValue1, method) ((  void (*) (ReactiveProperty_1_t2196508798 *, Il2CppObject*, Color_t1588175760 , const MethodInfo*))ReactiveProperty_1__ctor_m3064814295_gshared)(__this, ___source0, ___initialValue1, method)
// System.Void UniRx.ReactiveProperty`1<UnityEngine.Color>::.cctor()
extern "C"  void ReactiveProperty_1__cctor_m2661250189_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define ReactiveProperty_1__cctor_m2661250189(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))ReactiveProperty_1__cctor_m2661250189_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.IEqualityComparer`1<T> UniRx.ReactiveProperty`1<UnityEngine.Color>::get_EqualityComparer()
extern "C"  Il2CppObject* ReactiveProperty_1_get_EqualityComparer_m357557557_gshared (ReactiveProperty_1_t2196508798 * __this, const MethodInfo* method);
#define ReactiveProperty_1_get_EqualityComparer_m357557557(__this, method) ((  Il2CppObject* (*) (ReactiveProperty_1_t2196508798 *, const MethodInfo*))ReactiveProperty_1_get_EqualityComparer_m357557557_gshared)(__this, method)
// T UniRx.ReactiveProperty`1<UnityEngine.Color>::get_Value()
extern "C"  Color_t1588175760  ReactiveProperty_1_get_Value_m2447971461_gshared (ReactiveProperty_1_t2196508798 * __this, const MethodInfo* method);
#define ReactiveProperty_1_get_Value_m2447971461(__this, method) ((  Color_t1588175760  (*) (ReactiveProperty_1_t2196508798 *, const MethodInfo*))ReactiveProperty_1_get_Value_m2447971461_gshared)(__this, method)
// System.Void UniRx.ReactiveProperty`1<UnityEngine.Color>::set_Value(T)
extern "C"  void ReactiveProperty_1_set_Value_m3180757900_gshared (ReactiveProperty_1_t2196508798 * __this, Color_t1588175760  ___value0, const MethodInfo* method);
#define ReactiveProperty_1_set_Value_m3180757900(__this, ___value0, method) ((  void (*) (ReactiveProperty_1_t2196508798 *, Color_t1588175760 , const MethodInfo*))ReactiveProperty_1_set_Value_m3180757900_gshared)(__this, ___value0, method)
// System.Void UniRx.ReactiveProperty`1<UnityEngine.Color>::SetValue(T)
extern "C"  void ReactiveProperty_1_SetValue_m1296670891_gshared (ReactiveProperty_1_t2196508798 * __this, Color_t1588175760  ___value0, const MethodInfo* method);
#define ReactiveProperty_1_SetValue_m1296670891(__this, ___value0, method) ((  void (*) (ReactiveProperty_1_t2196508798 *, Color_t1588175760 , const MethodInfo*))ReactiveProperty_1_SetValue_m1296670891_gshared)(__this, ___value0, method)
// System.Void UniRx.ReactiveProperty`1<UnityEngine.Color>::SetValueAndForceNotify(T)
extern "C"  void ReactiveProperty_1_SetValueAndForceNotify_m4182155054_gshared (ReactiveProperty_1_t2196508798 * __this, Color_t1588175760  ___value0, const MethodInfo* method);
#define ReactiveProperty_1_SetValueAndForceNotify_m4182155054(__this, ___value0, method) ((  void (*) (ReactiveProperty_1_t2196508798 *, Color_t1588175760 , const MethodInfo*))ReactiveProperty_1_SetValueAndForceNotify_m4182155054_gshared)(__this, ___value0, method)
// System.IDisposable UniRx.ReactiveProperty`1<UnityEngine.Color>::Subscribe(UniRx.IObserver`1<T>)
extern "C"  Il2CppObject * ReactiveProperty_1_Subscribe_m168590965_gshared (ReactiveProperty_1_t2196508798 * __this, Il2CppObject* ___observer0, const MethodInfo* method);
#define ReactiveProperty_1_Subscribe_m168590965(__this, ___observer0, method) ((  Il2CppObject * (*) (ReactiveProperty_1_t2196508798 *, Il2CppObject*, const MethodInfo*))ReactiveProperty_1_Subscribe_m168590965_gshared)(__this, ___observer0, method)
// System.Void UniRx.ReactiveProperty`1<UnityEngine.Color>::Dispose()
extern "C"  void ReactiveProperty_1_Dispose_m2852740957_gshared (ReactiveProperty_1_t2196508798 * __this, const MethodInfo* method);
#define ReactiveProperty_1_Dispose_m2852740957(__this, method) ((  void (*) (ReactiveProperty_1_t2196508798 *, const MethodInfo*))ReactiveProperty_1_Dispose_m2852740957_gshared)(__this, method)
// System.Void UniRx.ReactiveProperty`1<UnityEngine.Color>::Dispose(System.Boolean)
extern "C"  void ReactiveProperty_1_Dispose_m4191383060_gshared (ReactiveProperty_1_t2196508798 * __this, bool ___disposing0, const MethodInfo* method);
#define ReactiveProperty_1_Dispose_m4191383060(__this, ___disposing0, method) ((  void (*) (ReactiveProperty_1_t2196508798 *, bool, const MethodInfo*))ReactiveProperty_1_Dispose_m4191383060_gshared)(__this, ___disposing0, method)
// System.String UniRx.ReactiveProperty`1<UnityEngine.Color>::ToString()
extern "C"  String_t* ReactiveProperty_1_ToString_m1231972749_gshared (ReactiveProperty_1_t2196508798 * __this, const MethodInfo* method);
#define ReactiveProperty_1_ToString_m1231972749(__this, method) ((  String_t* (*) (ReactiveProperty_1_t2196508798 *, const MethodInfo*))ReactiveProperty_1_ToString_m1231972749_gshared)(__this, method)
// System.Boolean UniRx.ReactiveProperty`1<UnityEngine.Color>::IsRequiredSubscribeOnCurrentThread()
extern "C"  bool ReactiveProperty_1_IsRequiredSubscribeOnCurrentThread_m2357356203_gshared (ReactiveProperty_1_t2196508798 * __this, const MethodInfo* method);
#define ReactiveProperty_1_IsRequiredSubscribeOnCurrentThread_m2357356203(__this, method) ((  bool (*) (ReactiveProperty_1_t2196508798 *, const MethodInfo*))ReactiveProperty_1_IsRequiredSubscribeOnCurrentThread_m2357356203_gshared)(__this, method)
