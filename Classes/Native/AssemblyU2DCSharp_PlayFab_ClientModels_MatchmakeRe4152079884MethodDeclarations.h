﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.ClientModels.MatchmakeRequest
struct MatchmakeRequest_t4152079884;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String968488902.h"
#include "mscorlib_System_Nullable_1_gen212373688.h"
#include "mscorlib_System_Nullable_1_gen3097043249.h"

// System.Void PlayFab.ClientModels.MatchmakeRequest::.ctor()
extern "C"  void MatchmakeRequest__ctor_m2291222449 (MatchmakeRequest_t4152079884 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PlayFab.ClientModels.MatchmakeRequest::get_BuildVersion()
extern "C"  String_t* MatchmakeRequest_get_BuildVersion_m3851768771 (MatchmakeRequest_t4152079884 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.MatchmakeRequest::set_BuildVersion(System.String)
extern "C"  void MatchmakeRequest_set_BuildVersion_m4107888456 (MatchmakeRequest_t4152079884 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Nullable`1<PlayFab.ClientModels.Region> PlayFab.ClientModels.MatchmakeRequest::get_Region()
extern "C"  Nullable_1_t212373688  MatchmakeRequest_get_Region_m676310474 (MatchmakeRequest_t4152079884 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.MatchmakeRequest::set_Region(System.Nullable`1<PlayFab.ClientModels.Region>)
extern "C"  void MatchmakeRequest_set_Region_m3181670925 (MatchmakeRequest_t4152079884 * __this, Nullable_1_t212373688  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PlayFab.ClientModels.MatchmakeRequest::get_GameMode()
extern "C"  String_t* MatchmakeRequest_get_GameMode_m1373031214 (MatchmakeRequest_t4152079884 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.MatchmakeRequest::set_GameMode(System.String)
extern "C"  void MatchmakeRequest_set_GameMode_m1289785917 (MatchmakeRequest_t4152079884 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PlayFab.ClientModels.MatchmakeRequest::get_LobbyId()
extern "C"  String_t* MatchmakeRequest_get_LobbyId_m55543450 (MatchmakeRequest_t4152079884 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.MatchmakeRequest::set_LobbyId(System.String)
extern "C"  void MatchmakeRequest_set_LobbyId_m3127240159 (MatchmakeRequest_t4152079884 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PlayFab.ClientModels.MatchmakeRequest::get_StatisticName()
extern "C"  String_t* MatchmakeRequest_get_StatisticName_m3437508228 (MatchmakeRequest_t4152079884 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.MatchmakeRequest::set_StatisticName(System.String)
extern "C"  void MatchmakeRequest_set_StatisticName_m2029542645 (MatchmakeRequest_t4152079884 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PlayFab.ClientModels.MatchmakeRequest::get_CharacterId()
extern "C"  String_t* MatchmakeRequest_get_CharacterId_m2766741613 (MatchmakeRequest_t4152079884 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.MatchmakeRequest::set_CharacterId(System.String)
extern "C"  void MatchmakeRequest_set_CharacterId_m2158471020 (MatchmakeRequest_t4152079884 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Nullable`1<System.Boolean> PlayFab.ClientModels.MatchmakeRequest::get_EnableQueue()
extern "C"  Nullable_1_t3097043249  MatchmakeRequest_get_EnableQueue_m3725861693 (MatchmakeRequest_t4152079884 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.MatchmakeRequest::set_EnableQueue(System.Nullable`1<System.Boolean>)
extern "C"  void MatchmakeRequest_set_EnableQueue_m2386426920 (MatchmakeRequest_t4152079884 * __this, Nullable_1_t3097043249  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
