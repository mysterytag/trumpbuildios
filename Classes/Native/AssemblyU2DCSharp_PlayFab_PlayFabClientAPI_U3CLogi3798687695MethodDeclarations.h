﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.PlayFabClientAPI/<LoginWithEmailAddress>c__AnonStorey61
struct U3CLoginWithEmailAddressU3Ec__AnonStorey61_t3798687695;
// PlayFab.CallRequestContainer
struct CallRequestContainer_t432318609;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharpU2Dfirstpass_PlayFab_CallRequestCo432318609.h"

// System.Void PlayFab.PlayFabClientAPI/<LoginWithEmailAddress>c__AnonStorey61::.ctor()
extern "C"  void U3CLoginWithEmailAddressU3Ec__AnonStorey61__ctor_m1524317284 (U3CLoginWithEmailAddressU3Ec__AnonStorey61_t3798687695 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/<LoginWithEmailAddress>c__AnonStorey61::<>m__4E(PlayFab.CallRequestContainer)
extern "C"  void U3CLoginWithEmailAddressU3Ec__AnonStorey61_U3CU3Em__4E_m1161721619 (U3CLoginWithEmailAddressU3Ec__AnonStorey61_t3798687695 * __this, CallRequestContainer_t432318609 * ___requestContainer0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
