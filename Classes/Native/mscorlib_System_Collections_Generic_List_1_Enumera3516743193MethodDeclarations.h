﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.List`1<System.DateTime>
struct List_1_t1135992905;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera3516743193.h"
#include "mscorlib_System_DateTime339033936.h"

// System.Void System.Collections.Generic.List`1/Enumerator<System.DateTime>::.ctor(System.Collections.Generic.List`1<T>)
extern "C"  void Enumerator__ctor_m1939593057_gshared (Enumerator_t3516743193 * __this, List_1_t1135992905 * ___l0, const MethodInfo* method);
#define Enumerator__ctor_m1939593057(__this, ___l0, method) ((  void (*) (Enumerator_t3516743193 *, List_1_t1135992905 *, const MethodInfo*))Enumerator__ctor_m1939593057_gshared)(__this, ___l0, method)
// System.Void System.Collections.Generic.List`1/Enumerator<System.DateTime>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m4176503889_gshared (Enumerator_t3516743193 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_Reset_m4176503889(__this, method) ((  void (*) (Enumerator_t3516743193 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m4176503889_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1/Enumerator<System.DateTime>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m2533869053_gshared (Enumerator_t3516743193 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m2533869053(__this, method) ((  Il2CppObject * (*) (Enumerator_t3516743193 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m2533869053_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<System.DateTime>::Dispose()
extern "C"  void Enumerator_Dispose_m1770491270_gshared (Enumerator_t3516743193 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m1770491270(__this, method) ((  void (*) (Enumerator_t3516743193 *, const MethodInfo*))Enumerator_Dispose_m1770491270_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<System.DateTime>::VerifyState()
extern "C"  void Enumerator_VerifyState_m2173508031_gshared (Enumerator_t3516743193 * __this, const MethodInfo* method);
#define Enumerator_VerifyState_m2173508031(__this, method) ((  void (*) (Enumerator_t3516743193 *, const MethodInfo*))Enumerator_VerifyState_m2173508031_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<System.DateTime>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m2147332157_gshared (Enumerator_t3516743193 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m2147332157(__this, method) ((  bool (*) (Enumerator_t3516743193 *, const MethodInfo*))Enumerator_MoveNext_m2147332157_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<System.DateTime>::get_Current()
extern "C"  DateTime_t339033936  Enumerator_get_Current_m4131499510_gshared (Enumerator_t3516743193 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m4131499510(__this, method) ((  DateTime_t339033936  (*) (Enumerator_t3516743193 *, const MethodInfo*))Enumerator_get_Current_m4131499510_gshared)(__this, method)
