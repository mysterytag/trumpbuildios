﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.SkipObservable`1/Skip<System.Boolean>
struct Skip_t1564805481;
// UniRx.Operators.SkipObservable`1<System.Boolean>
struct SkipObservable_1_t197279106;
// UniRx.IObserver`1<System.Boolean>
struct IObserver_1_t2423004244;
// System.IDisposable
struct IDisposable_t1628921374;
// System.Exception
struct Exception_t1967233988;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Exception1967233988.h"

// System.Void UniRx.Operators.SkipObservable`1/Skip<System.Boolean>::.ctor(UniRx.Operators.SkipObservable`1<T>,UniRx.IObserver`1<T>,System.IDisposable)
extern "C"  void Skip__ctor_m3668764960_gshared (Skip_t1564805481 * __this, SkipObservable_1_t197279106 * ___parent0, Il2CppObject* ___observer1, Il2CppObject * ___cancel2, const MethodInfo* method);
#define Skip__ctor_m3668764960(__this, ___parent0, ___observer1, ___cancel2, method) ((  void (*) (Skip_t1564805481 *, SkipObservable_1_t197279106 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))Skip__ctor_m3668764960_gshared)(__this, ___parent0, ___observer1, ___cancel2, method)
// System.Void UniRx.Operators.SkipObservable`1/Skip<System.Boolean>::OnNext(T)
extern "C"  void Skip_OnNext_m1512644194_gshared (Skip_t1564805481 * __this, bool ___value0, const MethodInfo* method);
#define Skip_OnNext_m1512644194(__this, ___value0, method) ((  void (*) (Skip_t1564805481 *, bool, const MethodInfo*))Skip_OnNext_m1512644194_gshared)(__this, ___value0, method)
// System.Void UniRx.Operators.SkipObservable`1/Skip<System.Boolean>::OnError(System.Exception)
extern "C"  void Skip_OnError_m2625123697_gshared (Skip_t1564805481 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method);
#define Skip_OnError_m2625123697(__this, ___error0, method) ((  void (*) (Skip_t1564805481 *, Exception_t1967233988 *, const MethodInfo*))Skip_OnError_m2625123697_gshared)(__this, ___error0, method)
// System.Void UniRx.Operators.SkipObservable`1/Skip<System.Boolean>::OnCompleted()
extern "C"  void Skip_OnCompleted_m517917252_gshared (Skip_t1564805481 * __this, const MethodInfo* method);
#define Skip_OnCompleted_m517917252(__this, method) ((  void (*) (Skip_t1564805481 *, const MethodInfo*))Skip_OnCompleted_m517917252_gshared)(__this, method)
