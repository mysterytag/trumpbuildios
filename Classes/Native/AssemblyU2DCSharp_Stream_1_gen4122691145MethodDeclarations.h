﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Stream_1_gen3823212738MethodDeclarations.h"

// System.Void Stream`1<System.Collections.Generic.ICollection`1<DonateButton>>::.ctor()
#define Stream_1__ctor_m3209592850(__this, method) ((  void (*) (Stream_1_t4122691145 *, const MethodInfo*))Stream_1__ctor_m2034388992_gshared)(__this, method)
// System.Void Stream`1<System.Collections.Generic.ICollection`1<DonateButton>>::Send(T)
#define Stream_1_Send_m1738810404(__this, ___value0, method) ((  void (*) (Stream_1_t4122691145 *, Il2CppObject*, const MethodInfo*))Stream_1_Send_m563606546_gshared)(__this, ___value0, method)
// System.Void Stream`1<System.Collections.Generic.ICollection`1<DonateButton>>::SendInTransaction(T,System.Int32)
#define Stream_1_SendInTransaction_m2857120442(__this, ___value0, ___p1, method) ((  void (*) (Stream_1_t4122691145 *, Il2CppObject*, int32_t, const MethodInfo*))Stream_1_SendInTransaction_m3764966696_gshared)(__this, ___value0, ___p1, method)
// System.IDisposable Stream`1<System.Collections.Generic.ICollection`1<DonateButton>>::Listen(System.Action`1<T>,Priority)
#define Stream_1_Listen_m3385120182(__this, ___action0, ___priority1, method) ((  Il2CppObject * (*) (Stream_1_t4122691145 *, Action_1_t1285037532 *, int32_t, const MethodInfo*))Stream_1_Listen_m889871082_gshared)(__this, ___action0, ___priority1, method)
// System.IDisposable Stream`1<System.Collections.Generic.ICollection`1<DonateButton>>::Listen(System.Action,Priority)
#define Stream_1_Listen_m3484348849(__this, ___action0, ___p1, method) ((  Il2CppObject * (*) (Stream_1_t4122691145 *, Action_t437523947 *, int32_t, const MethodInfo*))Stream_1_Listen_m2133851133_gshared)(__this, ___action0, ___p1, method)
// System.Void Stream`1<System.Collections.Generic.ICollection`1<DonateButton>>::Dispose()
#define Stream_1_Dispose_m530493391(__this, method) ((  void (*) (Stream_1_t4122691145 *, const MethodInfo*))Stream_1_Dispose_m735984701_gshared)(__this, method)
// System.Void Stream`1<System.Collections.Generic.ICollection`1<DonateButton>>::SetInputStream(IStream`1<T>)
#define Stream_1_SetInputStream_m2064248622(__this, ___stream0, method) ((  void (*) (Stream_1_t4122691145 *, Il2CppObject*, const MethodInfo*))Stream_1_SetInputStream_m3106996224_gshared)(__this, ___stream0, method)
// System.Void Stream`1<System.Collections.Generic.ICollection`1<DonateButton>>::ClearInputStream()
#define Stream_1_ClearInputStream_m2927894927(__this, method) ((  void (*) (Stream_1_t4122691145 *, const MethodInfo*))Stream_1_ClearInputStream_m445690081_gshared)(__this, method)
// System.Void Stream`1<System.Collections.Generic.ICollection`1<DonateButton>>::TransactionIterationFinished()
#define Stream_1_TransactionIterationFinished_m1760494307(__this, method) ((  void (*) (Stream_1_t4122691145 *, const MethodInfo*))Stream_1_TransactionIterationFinished_m3113559861_gshared)(__this, method)
// System.Void Stream`1<System.Collections.Generic.ICollection`1<DonateButton>>::Unpack(System.Int32)
#define Stream_1_Unpack_m785293749(__this, ___p0, method) ((  void (*) (Stream_1_t4122691145 *, int32_t, const MethodInfo*))Stream_1_Unpack_m3388253319_gshared)(__this, ___p0, method)
// System.Void Stream`1<System.Collections.Generic.ICollection`1<DonateButton>>::<SetInputStream>m__1BA(T)
#define Stream_1_U3CSetInputStreamU3Em__1BA_m2562121263(__this, ___val0, method) ((  void (*) (Stream_1_t4122691145 *, Il2CppObject*, const MethodInfo*))Stream_1_U3CSetInputStreamU3Em__1BA_m3579563677_gshared)(__this, ___val0, method)
