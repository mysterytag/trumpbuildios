﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.AmbObservable`1<System.Object>
struct AmbObservable_1_t4231346232;
// UniRx.IObservable`1<System.Object>
struct IObservable_1_t595904784;
// System.IDisposable
struct IDisposable_t1628921374;
// UniRx.IObserver`1<System.Object>
struct IObserver_1_t3049105323;

#include "codegen/il2cpp-codegen.h"

// System.Void UniRx.Operators.AmbObservable`1<System.Object>::.ctor(UniRx.IObservable`1<T>,UniRx.IObservable`1<T>)
extern "C"  void AmbObservable_1__ctor_m3833350880_gshared (AmbObservable_1_t4231346232 * __this, Il2CppObject* ___source0, Il2CppObject* ___second1, const MethodInfo* method);
#define AmbObservable_1__ctor_m3833350880(__this, ___source0, ___second1, method) ((  void (*) (AmbObservable_1_t4231346232 *, Il2CppObject*, Il2CppObject*, const MethodInfo*))AmbObservable_1__ctor_m3833350880_gshared)(__this, ___source0, ___second1, method)
// System.IDisposable UniRx.Operators.AmbObservable`1<System.Object>::SubscribeCore(UniRx.IObserver`1<T>,System.IDisposable)
extern "C"  Il2CppObject * AmbObservable_1_SubscribeCore_m1940249880_gshared (AmbObservable_1_t4231346232 * __this, Il2CppObject* ___observer0, Il2CppObject * ___cancel1, const MethodInfo* method);
#define AmbObservable_1_SubscribeCore_m1940249880(__this, ___observer0, ___cancel1, method) ((  Il2CppObject * (*) (AmbObservable_1_t4231346232 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))AmbObservable_1_SubscribeCore_m1940249880_gshared)(__this, ___observer0, ___cancel1, method)
