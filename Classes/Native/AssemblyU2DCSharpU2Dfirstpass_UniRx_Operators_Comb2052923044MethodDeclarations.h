﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.CombineLatestObservable`3/CombineLatest/LeftObserver<System.Boolean,System.Int32,System.Object>
struct LeftObserver_t2052923044;
// UniRx.Operators.CombineLatestObservable`3/CombineLatest<System.Boolean,System.Int32,System.Object>
struct CombineLatest_t2150356605;
// System.Exception
struct Exception_t1967233988;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Exception1967233988.h"

// System.Void UniRx.Operators.CombineLatestObservable`3/CombineLatest/LeftObserver<System.Boolean,System.Int32,System.Object>::.ctor(UniRx.Operators.CombineLatestObservable`3/CombineLatest<TLeft,TRight,TResult>)
extern "C"  void LeftObserver__ctor_m1684636417_gshared (LeftObserver_t2052923044 * __this, CombineLatest_t2150356605 * ___parent0, const MethodInfo* method);
#define LeftObserver__ctor_m1684636417(__this, ___parent0, method) ((  void (*) (LeftObserver_t2052923044 *, CombineLatest_t2150356605 *, const MethodInfo*))LeftObserver__ctor_m1684636417_gshared)(__this, ___parent0, method)
// System.Void UniRx.Operators.CombineLatestObservable`3/CombineLatest/LeftObserver<System.Boolean,System.Int32,System.Object>::OnNext(TLeft)
extern "C"  void LeftObserver_OnNext_m1356048466_gshared (LeftObserver_t2052923044 * __this, bool ___value0, const MethodInfo* method);
#define LeftObserver_OnNext_m1356048466(__this, ___value0, method) ((  void (*) (LeftObserver_t2052923044 *, bool, const MethodInfo*))LeftObserver_OnNext_m1356048466_gshared)(__this, ___value0, method)
// System.Void UniRx.Operators.CombineLatestObservable`3/CombineLatest/LeftObserver<System.Boolean,System.Int32,System.Object>::OnError(System.Exception)
extern "C"  void LeftObserver_OnError_m2602973352_gshared (LeftObserver_t2052923044 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method);
#define LeftObserver_OnError_m2602973352(__this, ___error0, method) ((  void (*) (LeftObserver_t2052923044 *, Exception_t1967233988 *, const MethodInfo*))LeftObserver_OnError_m2602973352_gshared)(__this, ___error0, method)
// System.Void UniRx.Operators.CombineLatestObservable`3/CombineLatest/LeftObserver<System.Boolean,System.Int32,System.Object>::OnCompleted()
extern "C"  void LeftObserver_OnCompleted_m2747705339_gshared (LeftObserver_t2052923044 * __this, const MethodInfo* method);
#define LeftObserver_OnCompleted_m2747705339(__this, method) ((  void (*) (LeftObserver_t2052923044 *, const MethodInfo*))LeftObserver_OnCompleted_m2747705339_gshared)(__this, method)
