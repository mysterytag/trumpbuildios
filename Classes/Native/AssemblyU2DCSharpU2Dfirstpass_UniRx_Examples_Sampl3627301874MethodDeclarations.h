﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.IObservable`1<UniRx.Examples.Sample04_ConvertFromUnityCallback/LogCallback>
struct IObservable_1_t2994461093;
// UnityEngine.Application/LogCallback
struct LogCallback_t3235662729;
// System.Action`1<UniRx.Examples.Sample04_ConvertFromUnityCallback/LogCallback>
struct Action_1_t3384115434;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Application_LogCallback3235662729.h"

// UniRx.IObservable`1<UniRx.Examples.Sample04_ConvertFromUnityCallback/LogCallback> UniRx.Examples.Sample04_ConvertFromUnityCallback/LogHelper::LogCallbackAsObservable()
extern "C"  Il2CppObject* LogHelper_LogCallbackAsObservable_m1038802592 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Application/LogCallback UniRx.Examples.Sample04_ConvertFromUnityCallback/LogHelper::<LogCallbackAsObservable>m__17(System.Action`1<UniRx.Examples.Sample04_ConvertFromUnityCallback/LogCallback>)
extern "C"  LogCallback_t3235662729 * LogHelper_U3CLogCallbackAsObservableU3Em__17_m1808264757 (Il2CppObject * __this /* static, unused */, Action_1_t3384115434 * ___h0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.Examples.Sample04_ConvertFromUnityCallback/LogHelper::<LogCallbackAsObservable>m__18(UnityEngine.Application/LogCallback)
extern "C"  void LogHelper_U3CLogCallbackAsObservableU3Em__18_m436926318 (Il2CppObject * __this /* static, unused */, LogCallback_t3235662729 * ___h0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.Examples.Sample04_ConvertFromUnityCallback/LogHelper::<LogCallbackAsObservable>m__19(UnityEngine.Application/LogCallback)
extern "C"  void LogHelper_U3CLogCallbackAsObservableU3Em__19_m2069730317 (Il2CppObject * __this /* static, unused */, LogCallback_t3235662729 * ___h0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
