﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Observable/<ToAwaitableEnumerator>c__Iterator17`1<System.Int32>
struct U3CToAwaitableEnumeratorU3Ec__Iterator17_1_t3131167031;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void UniRx.Observable/<ToAwaitableEnumerator>c__Iterator17`1<System.Int32>::.ctor()
extern "C"  void U3CToAwaitableEnumeratorU3Ec__Iterator17_1__ctor_m3534264873_gshared (U3CToAwaitableEnumeratorU3Ec__Iterator17_1_t3131167031 * __this, const MethodInfo* method);
#define U3CToAwaitableEnumeratorU3Ec__Iterator17_1__ctor_m3534264873(__this, method) ((  void (*) (U3CToAwaitableEnumeratorU3Ec__Iterator17_1_t3131167031 *, const MethodInfo*))U3CToAwaitableEnumeratorU3Ec__Iterator17_1__ctor_m3534264873_gshared)(__this, method)
// System.Object UniRx.Observable/<ToAwaitableEnumerator>c__Iterator17`1<System.Int32>::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CToAwaitableEnumeratorU3Ec__Iterator17_1_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3561053065_gshared (U3CToAwaitableEnumeratorU3Ec__Iterator17_1_t3131167031 * __this, const MethodInfo* method);
#define U3CToAwaitableEnumeratorU3Ec__Iterator17_1_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3561053065(__this, method) ((  Il2CppObject * (*) (U3CToAwaitableEnumeratorU3Ec__Iterator17_1_t3131167031 *, const MethodInfo*))U3CToAwaitableEnumeratorU3Ec__Iterator17_1_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3561053065_gshared)(__this, method)
// System.Object UniRx.Observable/<ToAwaitableEnumerator>c__Iterator17`1<System.Int32>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CToAwaitableEnumeratorU3Ec__Iterator17_1_System_Collections_IEnumerator_get_Current_m4249194781_gshared (U3CToAwaitableEnumeratorU3Ec__Iterator17_1_t3131167031 * __this, const MethodInfo* method);
#define U3CToAwaitableEnumeratorU3Ec__Iterator17_1_System_Collections_IEnumerator_get_Current_m4249194781(__this, method) ((  Il2CppObject * (*) (U3CToAwaitableEnumeratorU3Ec__Iterator17_1_t3131167031 *, const MethodInfo*))U3CToAwaitableEnumeratorU3Ec__Iterator17_1_System_Collections_IEnumerator_get_Current_m4249194781_gshared)(__this, method)
// System.Boolean UniRx.Observable/<ToAwaitableEnumerator>c__Iterator17`1<System.Int32>::MoveNext()
extern "C"  bool U3CToAwaitableEnumeratorU3Ec__Iterator17_1_MoveNext_m2460421187_gshared (U3CToAwaitableEnumeratorU3Ec__Iterator17_1_t3131167031 * __this, const MethodInfo* method);
#define U3CToAwaitableEnumeratorU3Ec__Iterator17_1_MoveNext_m2460421187(__this, method) ((  bool (*) (U3CToAwaitableEnumeratorU3Ec__Iterator17_1_t3131167031 *, const MethodInfo*))U3CToAwaitableEnumeratorU3Ec__Iterator17_1_MoveNext_m2460421187_gshared)(__this, method)
// System.Void UniRx.Observable/<ToAwaitableEnumerator>c__Iterator17`1<System.Int32>::Dispose()
extern "C"  void U3CToAwaitableEnumeratorU3Ec__Iterator17_1_Dispose_m3302662182_gshared (U3CToAwaitableEnumeratorU3Ec__Iterator17_1_t3131167031 * __this, const MethodInfo* method);
#define U3CToAwaitableEnumeratorU3Ec__Iterator17_1_Dispose_m3302662182(__this, method) ((  void (*) (U3CToAwaitableEnumeratorU3Ec__Iterator17_1_t3131167031 *, const MethodInfo*))U3CToAwaitableEnumeratorU3Ec__Iterator17_1_Dispose_m3302662182_gshared)(__this, method)
// System.Void UniRx.Observable/<ToAwaitableEnumerator>c__Iterator17`1<System.Int32>::Reset()
extern "C"  void U3CToAwaitableEnumeratorU3Ec__Iterator17_1_Reset_m1180697814_gshared (U3CToAwaitableEnumeratorU3Ec__Iterator17_1_t3131167031 * __this, const MethodInfo* method);
#define U3CToAwaitableEnumeratorU3Ec__Iterator17_1_Reset_m1180697814(__this, method) ((  void (*) (U3CToAwaitableEnumeratorU3Ec__Iterator17_1_t3131167031 *, const MethodInfo*))U3CToAwaitableEnumeratorU3Ec__Iterator17_1_Reset_m1180697814_gshared)(__this, method)
