﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.PlayFabClientAPI/RegisterPlayFabUserResponseCallback
struct RegisterPlayFabUserResponseCallback_t2439355553;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;
// PlayFab.ClientModels.RegisterPlayFabUserRequest
struct RegisterPlayFabUserRequest_t2314572612;
// PlayFab.ClientModels.RegisterPlayFabUserResult
struct RegisterPlayFabUserResult_t109811432;
// PlayFab.PlayFabError
struct PlayFabError_t750598646;
// System.IAsyncResult
struct IAsyncResult_t537683269;
// System.AsyncCallback
struct AsyncCallback_t1363551830;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_IntPtr676692020.h"
#include "mscorlib_System_String968488902.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_RegisterPla2314572612.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_RegisterPlay109811432.h"
#include "AssemblyU2DCSharpU2Dfirstpass_PlayFab_PlayFabError750598646.h"
#include "mscorlib_System_AsyncCallback1363551830.h"

// System.Void PlayFab.PlayFabClientAPI/RegisterPlayFabUserResponseCallback::.ctor(System.Object,System.IntPtr)
extern "C"  void RegisterPlayFabUserResponseCallback__ctor_m842745008 (RegisterPlayFabUserResponseCallback_t2439355553 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/RegisterPlayFabUserResponseCallback::Invoke(System.String,System.Int32,PlayFab.ClientModels.RegisterPlayFabUserRequest,PlayFab.ClientModels.RegisterPlayFabUserResult,PlayFab.PlayFabError,System.Object)
extern "C"  void RegisterPlayFabUserResponseCallback_Invoke_m3006671057 (RegisterPlayFabUserResponseCallback_t2439355553 * __this, String_t* ___urlPath0, int32_t ___callId1, RegisterPlayFabUserRequest_t2314572612 * ___request2, RegisterPlayFabUserResult_t109811432 * ___result3, PlayFabError_t750598646 * ___error4, Il2CppObject * ___customData5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
extern "C" void pinvoke_delegate_wrapper_RegisterPlayFabUserResponseCallback_t2439355553(Il2CppObject* delegate, String_t* ___urlPath0, int32_t ___callId1, RegisterPlayFabUserRequest_t2314572612 * ___request2, RegisterPlayFabUserResult_t109811432 * ___result3, PlayFabError_t750598646 * ___error4, Il2CppObject * ___customData5);
// System.IAsyncResult PlayFab.PlayFabClientAPI/RegisterPlayFabUserResponseCallback::BeginInvoke(System.String,System.Int32,PlayFab.ClientModels.RegisterPlayFabUserRequest,PlayFab.ClientModels.RegisterPlayFabUserResult,PlayFab.PlayFabError,System.Object,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * RegisterPlayFabUserResponseCallback_BeginInvoke_m1169027742 (RegisterPlayFabUserResponseCallback_t2439355553 * __this, String_t* ___urlPath0, int32_t ___callId1, RegisterPlayFabUserRequest_t2314572612 * ___request2, RegisterPlayFabUserResult_t109811432 * ___result3, PlayFabError_t750598646 * ___error4, Il2CppObject * ___customData5, AsyncCallback_t1363551830 * ___callback6, Il2CppObject * ___object7, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/RegisterPlayFabUserResponseCallback::EndInvoke(System.IAsyncResult)
extern "C"  void RegisterPlayFabUserResponseCallback_EndInvoke_m1791790784 (RegisterPlayFabUserResponseCallback_t2439355553 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
