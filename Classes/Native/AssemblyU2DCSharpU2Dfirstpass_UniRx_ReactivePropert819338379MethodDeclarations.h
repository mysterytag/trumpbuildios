﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.ReactiveProperty`1<System.Boolean>
struct ReactiveProperty_1_t819338379;
// UniRx.IObservable`1<System.Boolean>
struct IObservable_1_t4264771001;
// System.Collections.Generic.IEqualityComparer`1<System.Boolean>
struct IEqualityComparer_1_t2535271992;
// System.IDisposable
struct IDisposable_t1628921374;
// UniRx.IObserver`1<System.Boolean>
struct IObserver_1_t2423004244;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"

// System.Void UniRx.ReactiveProperty`1<System.Boolean>::.ctor()
extern "C"  void ReactiveProperty_1__ctor_m3742808319_gshared (ReactiveProperty_1_t819338379 * __this, const MethodInfo* method);
#define ReactiveProperty_1__ctor_m3742808319(__this, method) ((  void (*) (ReactiveProperty_1_t819338379 *, const MethodInfo*))ReactiveProperty_1__ctor_m3742808319_gshared)(__this, method)
// System.Void UniRx.ReactiveProperty`1<System.Boolean>::.ctor(T)
extern "C"  void ReactiveProperty_1__ctor_m62942271_gshared (ReactiveProperty_1_t819338379 * __this, bool ___initialValue0, const MethodInfo* method);
#define ReactiveProperty_1__ctor_m62942271(__this, ___initialValue0, method) ((  void (*) (ReactiveProperty_1_t819338379 *, bool, const MethodInfo*))ReactiveProperty_1__ctor_m62942271_gshared)(__this, ___initialValue0, method)
// System.Void UniRx.ReactiveProperty`1<System.Boolean>::.ctor(UniRx.IObservable`1<T>)
extern "C"  void ReactiveProperty_1__ctor_m1217349022_gshared (ReactiveProperty_1_t819338379 * __this, Il2CppObject* ___source0, const MethodInfo* method);
#define ReactiveProperty_1__ctor_m1217349022(__this, ___source0, method) ((  void (*) (ReactiveProperty_1_t819338379 *, Il2CppObject*, const MethodInfo*))ReactiveProperty_1__ctor_m1217349022_gshared)(__this, ___source0, method)
// System.Void UniRx.ReactiveProperty`1<System.Boolean>::.ctor(UniRx.IObservable`1<T>,T)
extern "C"  void ReactiveProperty_1__ctor_m1641311158_gshared (ReactiveProperty_1_t819338379 * __this, Il2CppObject* ___source0, bool ___initialValue1, const MethodInfo* method);
#define ReactiveProperty_1__ctor_m1641311158(__this, ___source0, ___initialValue1, method) ((  void (*) (ReactiveProperty_1_t819338379 *, Il2CppObject*, bool, const MethodInfo*))ReactiveProperty_1__ctor_m1641311158_gshared)(__this, ___source0, ___initialValue1, method)
// System.Void UniRx.ReactiveProperty`1<System.Boolean>::.cctor()
extern "C"  void ReactiveProperty_1__cctor_m3875811982_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define ReactiveProperty_1__cctor_m3875811982(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))ReactiveProperty_1__cctor_m3875811982_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.IEqualityComparer`1<T> UniRx.ReactiveProperty`1<System.Boolean>::get_EqualityComparer()
extern "C"  Il2CppObject* ReactiveProperty_1_get_EqualityComparer_m2360157460_gshared (ReactiveProperty_1_t819338379 * __this, const MethodInfo* method);
#define ReactiveProperty_1_get_EqualityComparer_m2360157460(__this, method) ((  Il2CppObject* (*) (ReactiveProperty_1_t819338379 *, const MethodInfo*))ReactiveProperty_1_get_EqualityComparer_m2360157460_gshared)(__this, method)
// T UniRx.ReactiveProperty`1<System.Boolean>::get_Value()
extern "C"  bool ReactiveProperty_1_get_Value_m3883642950_gshared (ReactiveProperty_1_t819338379 * __this, const MethodInfo* method);
#define ReactiveProperty_1_get_Value_m3883642950(__this, method) ((  bool (*) (ReactiveProperty_1_t819338379 *, const MethodInfo*))ReactiveProperty_1_get_Value_m3883642950_gshared)(__this, method)
// System.Void UniRx.ReactiveProperty`1<System.Boolean>::set_Value(T)
extern "C"  void ReactiveProperty_1_set_Value_m2843367693_gshared (ReactiveProperty_1_t819338379 * __this, bool ___value0, const MethodInfo* method);
#define ReactiveProperty_1_set_Value_m2843367693(__this, ___value0, method) ((  void (*) (ReactiveProperty_1_t819338379 *, bool, const MethodInfo*))ReactiveProperty_1_set_Value_m2843367693_gshared)(__this, ___value0, method)
// System.Void UniRx.ReactiveProperty`1<System.Boolean>::SetValue(T)
extern "C"  void ReactiveProperty_1_SetValue_m3502544650_gshared (ReactiveProperty_1_t819338379 * __this, bool ___value0, const MethodInfo* method);
#define ReactiveProperty_1_SetValue_m3502544650(__this, ___value0, method) ((  void (*) (ReactiveProperty_1_t819338379 *, bool, const MethodInfo*))ReactiveProperty_1_SetValue_m3502544650_gshared)(__this, ___value0, method)
// System.Void UniRx.ReactiveProperty`1<System.Boolean>::SetValueAndForceNotify(T)
extern "C"  void ReactiveProperty_1_SetValueAndForceNotify_m264733517_gshared (ReactiveProperty_1_t819338379 * __this, bool ___value0, const MethodInfo* method);
#define ReactiveProperty_1_SetValueAndForceNotify_m264733517(__this, ___value0, method) ((  void (*) (ReactiveProperty_1_t819338379 *, bool, const MethodInfo*))ReactiveProperty_1_SetValueAndForceNotify_m264733517_gshared)(__this, ___value0, method)
// System.IDisposable UniRx.ReactiveProperty`1<System.Boolean>::Subscribe(UniRx.IObserver`1<T>)
extern "C"  Il2CppObject * ReactiveProperty_1_Subscribe_m1226681278_gshared (ReactiveProperty_1_t819338379 * __this, Il2CppObject* ___observer0, const MethodInfo* method);
#define ReactiveProperty_1_Subscribe_m1226681278(__this, ___observer0, method) ((  Il2CppObject * (*) (ReactiveProperty_1_t819338379 *, Il2CppObject*, const MethodInfo*))ReactiveProperty_1_Subscribe_m1226681278_gshared)(__this, ___observer0, method)
// System.Void UniRx.ReactiveProperty`1<System.Boolean>::Dispose()
extern "C"  void ReactiveProperty_1_Dispose_m1849450876_gshared (ReactiveProperty_1_t819338379 * __this, const MethodInfo* method);
#define ReactiveProperty_1_Dispose_m1849450876(__this, method) ((  void (*) (ReactiveProperty_1_t819338379 *, const MethodInfo*))ReactiveProperty_1_Dispose_m1849450876_gshared)(__this, method)
// System.Void UniRx.ReactiveProperty`1<System.Boolean>::Dispose(System.Boolean)
extern "C"  void ReactiveProperty_1_Dispose_m2167197171_gshared (ReactiveProperty_1_t819338379 * __this, bool ___disposing0, const MethodInfo* method);
#define ReactiveProperty_1_Dispose_m2167197171(__this, ___disposing0, method) ((  void (*) (ReactiveProperty_1_t819338379 *, bool, const MethodInfo*))ReactiveProperty_1_Dispose_m2167197171_gshared)(__this, ___disposing0, method)
// System.String UniRx.ReactiveProperty`1<System.Boolean>::ToString()
extern "C"  String_t* ReactiveProperty_1_ToString_m4110379380_gshared (ReactiveProperty_1_t819338379 * __this, const MethodInfo* method);
#define ReactiveProperty_1_ToString_m4110379380(__this, method) ((  String_t* (*) (ReactiveProperty_1_t819338379 *, const MethodInfo*))ReactiveProperty_1_ToString_m4110379380_gshared)(__this, method)
// System.Boolean UniRx.ReactiveProperty`1<System.Boolean>::IsRequiredSubscribeOnCurrentThread()
extern "C"  bool ReactiveProperty_1_IsRequiredSubscribeOnCurrentThread_m952260580_gshared (ReactiveProperty_1_t819338379 * __this, const MethodInfo* method);
#define ReactiveProperty_1_IsRequiredSubscribeOnCurrentThread_m952260580(__this, method) ((  bool (*) (ReactiveProperty_1_t819338379 *, const MethodInfo*))ReactiveProperty_1_IsRequiredSubscribeOnCurrentThread_m952260580_gshared)(__this, method)
