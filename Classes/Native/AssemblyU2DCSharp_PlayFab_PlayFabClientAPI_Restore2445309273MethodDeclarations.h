﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.PlayFabClientAPI/RestoreIOSPurchasesResponseCallback
struct RestoreIOSPurchasesResponseCallback_t2445309273;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;
// PlayFab.ClientModels.RestoreIOSPurchasesRequest
struct RestoreIOSPurchasesRequest_t2671348620;
// PlayFab.ClientModels.RestoreIOSPurchasesResult
struct RestoreIOSPurchasesResult_t675509664;
// PlayFab.PlayFabError
struct PlayFabError_t750598646;
// System.IAsyncResult
struct IAsyncResult_t537683269;
// System.AsyncCallback
struct AsyncCallback_t1363551830;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_IntPtr676692020.h"
#include "mscorlib_System_String968488902.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_RestoreIOSP2671348620.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_RestoreIOSPu675509664.h"
#include "AssemblyU2DCSharpU2Dfirstpass_PlayFab_PlayFabError750598646.h"
#include "mscorlib_System_AsyncCallback1363551830.h"

// System.Void PlayFab.PlayFabClientAPI/RestoreIOSPurchasesResponseCallback::.ctor(System.Object,System.IntPtr)
extern "C"  void RestoreIOSPurchasesResponseCallback__ctor_m1219109224 (RestoreIOSPurchasesResponseCallback_t2445309273 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/RestoreIOSPurchasesResponseCallback::Invoke(System.String,System.Int32,PlayFab.ClientModels.RestoreIOSPurchasesRequest,PlayFab.ClientModels.RestoreIOSPurchasesResult,PlayFab.PlayFabError,System.Object)
extern "C"  void RestoreIOSPurchasesResponseCallback_Invoke_m2165943545 (RestoreIOSPurchasesResponseCallback_t2445309273 * __this, String_t* ___urlPath0, int32_t ___callId1, RestoreIOSPurchasesRequest_t2671348620 * ___request2, RestoreIOSPurchasesResult_t675509664 * ___result3, PlayFabError_t750598646 * ___error4, Il2CppObject * ___customData5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
extern "C" void pinvoke_delegate_wrapper_RestoreIOSPurchasesResponseCallback_t2445309273(Il2CppObject* delegate, String_t* ___urlPath0, int32_t ___callId1, RestoreIOSPurchasesRequest_t2671348620 * ___request2, RestoreIOSPurchasesResult_t675509664 * ___result3, PlayFabError_t750598646 * ___error4, Il2CppObject * ___customData5);
// System.IAsyncResult PlayFab.PlayFabClientAPI/RestoreIOSPurchasesResponseCallback::BeginInvoke(System.String,System.Int32,PlayFab.ClientModels.RestoreIOSPurchasesRequest,PlayFab.ClientModels.RestoreIOSPurchasesResult,PlayFab.PlayFabError,System.Object,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * RestoreIOSPurchasesResponseCallback_BeginInvoke_m1044250086 (RestoreIOSPurchasesResponseCallback_t2445309273 * __this, String_t* ___urlPath0, int32_t ___callId1, RestoreIOSPurchasesRequest_t2671348620 * ___request2, RestoreIOSPurchasesResult_t675509664 * ___result3, PlayFabError_t750598646 * ___error4, Il2CppObject * ___customData5, AsyncCallback_t1363551830 * ___callback6, Il2CppObject * ___object7, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/RestoreIOSPurchasesResponseCallback::EndInvoke(System.IAsyncResult)
extern "C"  void RestoreIOSPurchasesResponseCallback_EndInvoke_m4241917816 (RestoreIOSPurchasesResponseCallback_t2445309273 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
