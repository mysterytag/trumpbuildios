﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.DistinctObservable`2/Distinct<System.Object,System.Object>
struct Distinct_t1668326987;
// UniRx.Operators.DistinctObservable`2<System.Object,System.Object>
struct DistinctObservable_2_t2396543569;
// UniRx.IObserver`1<System.Object>
struct IObserver_1_t3049105323;
// System.IDisposable
struct IDisposable_t1628921374;
// System.Object
struct Il2CppObject;
// System.Exception
struct Exception_t1967233988;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_Exception1967233988.h"

// System.Void UniRx.Operators.DistinctObservable`2/Distinct<System.Object,System.Object>::.ctor(UniRx.Operators.DistinctObservable`2<T,TKey>,UniRx.IObserver`1<T>,System.IDisposable)
extern "C"  void Distinct__ctor_m630126341_gshared (Distinct_t1668326987 * __this, DistinctObservable_2_t2396543569 * ___parent0, Il2CppObject* ___observer1, Il2CppObject * ___cancel2, const MethodInfo* method);
#define Distinct__ctor_m630126341(__this, ___parent0, ___observer1, ___cancel2, method) ((  void (*) (Distinct_t1668326987 *, DistinctObservable_2_t2396543569 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))Distinct__ctor_m630126341_gshared)(__this, ___parent0, ___observer1, ___cancel2, method)
// System.Void UniRx.Operators.DistinctObservable`2/Distinct<System.Object,System.Object>::OnNext(T)
extern "C"  void Distinct_OnNext_m586424240_gshared (Distinct_t1668326987 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Distinct_OnNext_m586424240(__this, ___value0, method) ((  void (*) (Distinct_t1668326987 *, Il2CppObject *, const MethodInfo*))Distinct_OnNext_m586424240_gshared)(__this, ___value0, method)
// System.Void UniRx.Operators.DistinctObservable`2/Distinct<System.Object,System.Object>::OnError(System.Exception)
extern "C"  void Distinct_OnError_m2900548287_gshared (Distinct_t1668326987 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method);
#define Distinct_OnError_m2900548287(__this, ___error0, method) ((  void (*) (Distinct_t1668326987 *, Exception_t1967233988 *, const MethodInfo*))Distinct_OnError_m2900548287_gshared)(__this, ___error0, method)
// System.Void UniRx.Operators.DistinctObservable`2/Distinct<System.Object,System.Object>::OnCompleted()
extern "C"  void Distinct_OnCompleted_m2626450578_gshared (Distinct_t1668326987 * __this, const MethodInfo* method);
#define Distinct_OnCompleted_m2626450578(__this, method) ((  void (*) (Distinct_t1668326987 *, const MethodInfo*))Distinct_OnCompleted_m2626450578_gshared)(__this, method)
