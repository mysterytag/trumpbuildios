﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Reactor`1/<TransactionUnpackReact>c__AnonStorey141<System.Object>
struct U3CTransactionUnpackReactU3Ec__AnonStorey141_t3165049731;

#include "codegen/il2cpp-codegen.h"

// System.Void Reactor`1/<TransactionUnpackReact>c__AnonStorey141<System.Object>::.ctor()
extern "C"  void U3CTransactionUnpackReactU3Ec__AnonStorey141__ctor_m114301029_gshared (U3CTransactionUnpackReactU3Ec__AnonStorey141_t3165049731 * __this, const MethodInfo* method);
#define U3CTransactionUnpackReactU3Ec__AnonStorey141__ctor_m114301029(__this, method) ((  void (*) (U3CTransactionUnpackReactU3Ec__AnonStorey141_t3165049731 *, const MethodInfo*))U3CTransactionUnpackReactU3Ec__AnonStorey141__ctor_m114301029_gshared)(__this, method)
// System.Void Reactor`1/<TransactionUnpackReact>c__AnonStorey141<System.Object>::<>m__1D6()
extern "C"  void U3CTransactionUnpackReactU3Ec__AnonStorey141_U3CU3Em__1D6_m2063261015_gshared (U3CTransactionUnpackReactU3Ec__AnonStorey141_t3165049731 * __this, const MethodInfo* method);
#define U3CTransactionUnpackReactU3Ec__AnonStorey141_U3CU3Em__1D6_m2063261015(__this, method) ((  void (*) (U3CTransactionUnpackReactU3Ec__AnonStorey141_t3165049731 *, const MethodInfo*))U3CTransactionUnpackReactU3Ec__AnonStorey141_U3CU3Em__1D6_m2063261015_gshared)(__this, method)
