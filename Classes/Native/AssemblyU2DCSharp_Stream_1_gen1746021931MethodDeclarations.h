﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Stream_1_gen1107661009MethodDeclarations.h"

// System.Void Stream`1<UniRx.CollectionAddEvent`1<UpgradeButton>>::.ctor()
#define Stream_1__ctor_m2067137774(__this, method) ((  void (*) (Stream_1_t1746021931 *, const MethodInfo*))Stream_1__ctor_m3235287902_gshared)(__this, method)
// System.Void Stream`1<UniRx.CollectionAddEvent`1<UpgradeButton>>::Send(T)
#define Stream_1_Send_m596355328(__this, ___value0, method) ((  void (*) (Stream_1_t1746021931 *, CollectionAddEvent_1_t3054882909 , const MethodInfo*))Stream_1_Send_m1764505456_gshared)(__this, ___value0, method)
// System.Void Stream`1<UniRx.CollectionAddEvent`1<UpgradeButton>>::SendInTransaction(T,System.Int32)
#define Stream_1_SendInTransaction_m3519093910(__this, ___value0, ___p1, method) ((  void (*) (Stream_1_t1746021931 *, CollectionAddEvent_1_t3054882909 , int32_t, const MethodInfo*))Stream_1_SendInTransaction_m1512275718_gshared)(__this, ___value0, ___p1, method)
// System.IDisposable Stream`1<UniRx.CollectionAddEvent`1<UpgradeButton>>::Listen(System.Action`1<T>,Priority)
#define Stream_1_Listen_m2002736216(__this, ___action0, ___priority1, method) ((  Il2CppObject * (*) (Stream_1_t1746021931 *, Action_1_t3203335614 *, int32_t, const MethodInfo*))Stream_1_Listen_m1890769608_gshared)(__this, ___action0, ___priority1, method)
// System.IDisposable Stream`1<UniRx.CollectionAddEvent`1<UpgradeButton>>::Listen(System.Action,Priority)
#define Stream_1_Listen_m1191466447(__this, ___action0, ___p1, method) ((  Il2CppObject * (*) (Stream_1_t1746021931 *, Action_t437523947 *, int32_t, const MethodInfo*))Stream_1_Listen_m3411183967_gshared)(__this, ___action0, ___p1, method)
// System.Void Stream`1<UniRx.CollectionAddEvent`1<UpgradeButton>>::Dispose()
#define Stream_1_Dispose_m2142793131(__this, method) ((  void (*) (Stream_1_t1746021931 *, const MethodInfo*))Stream_1_Dispose_m3748601883_gshared)(__this, method)
// System.Void Stream`1<UniRx.CollectionAddEvent`1<UpgradeButton>>::SetInputStream(IStream`1<T>)
#define Stream_1_SetInputStream_m2494122962(__this, ___stream0, method) ((  void (*) (Stream_1_t1746021931 *, Il2CppObject*, const MethodInfo*))Stream_1_SetInputStream_m453976930_gshared)(__this, ___stream0, method)
// System.Void Stream`1<UniRx.CollectionAddEvent`1<UpgradeButton>>::ClearInputStream()
#define Stream_1_ClearInputStream_m2183936819(__this, method) ((  void (*) (Stream_1_t1746021931 *, const MethodInfo*))Stream_1_ClearInputStream_m1610565315_gshared)(__this, method)
// System.Void Stream`1<UniRx.CollectionAddEvent`1<UpgradeButton>>::TransactionIterationFinished()
#define Stream_1_TransactionIterationFinished_m2552874631(__this, method) ((  void (*) (Stream_1_t1746021931 *, const MethodInfo*))Stream_1_TransactionIterationFinished_m477624855_gshared)(__this, method)
// System.Void Stream`1<UniRx.CollectionAddEvent`1<UpgradeButton>>::Unpack(System.Int32)
#define Stream_1_Unpack_m3101090393(__this, ___p0, method) ((  void (*) (Stream_1_t1746021931 *, int32_t, const MethodInfo*))Stream_1_Unpack_m1846888937_gshared)(__this, ___p0, method)
// System.Void Stream`1<UniRx.CollectionAddEvent`1<UpgradeButton>>::<SetInputStream>m__1BA(T)
#define Stream_1_U3CSetInputStreamU3Em__1BA_m1639015435(__this, ___val0, method) ((  void (*) (Stream_1_t1746021931 *, CollectionAddEvent_1_t3054882909 , const MethodInfo*))Stream_1_U3CSetInputStreamU3Em__1BA_m4229248635_gshared)(__this, ___val0, method)
