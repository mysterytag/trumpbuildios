﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.PlayFabClientAPI/<LinkCustomID>c__AnonStorey76
struct U3CLinkCustomIDU3Ec__AnonStorey76_t3849201514;
// PlayFab.CallRequestContainer
struct CallRequestContainer_t432318609;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharpU2Dfirstpass_PlayFab_CallRequestCo432318609.h"

// System.Void PlayFab.PlayFabClientAPI/<LinkCustomID>c__AnonStorey76::.ctor()
extern "C"  void U3CLinkCustomIDU3Ec__AnonStorey76__ctor_m776860553 (U3CLinkCustomIDU3Ec__AnonStorey76_t3849201514 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/<LinkCustomID>c__AnonStorey76::<>m__63(PlayFab.CallRequestContainer)
extern "C"  void U3CLinkCustomIDU3Ec__AnonStorey76_U3CU3Em__63_m167660196 (U3CLinkCustomIDU3Ec__AnonStorey76_t3849201514 * __this, CallRequestContainer_t432318609 * ___requestContainer0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
