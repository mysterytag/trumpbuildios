﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.ClientModels.AddUsernamePasswordRequest
struct AddUsernamePasswordRequest_t1599161869;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String968488902.h"

// System.Void PlayFab.ClientModels.AddUsernamePasswordRequest::.ctor()
extern "C"  void AddUsernamePasswordRequest__ctor_m253860944 (AddUsernamePasswordRequest_t1599161869 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PlayFab.ClientModels.AddUsernamePasswordRequest::get_Username()
extern "C"  String_t* AddUsernamePasswordRequest_get_Username_m2021056848 (AddUsernamePasswordRequest_t1599161869 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.AddUsernamePasswordRequest::set_Username(System.String)
extern "C"  void AddUsernamePasswordRequest_set_Username_m1786913563 (AddUsernamePasswordRequest_t1599161869 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PlayFab.ClientModels.AddUsernamePasswordRequest::get_Email()
extern "C"  String_t* AddUsernamePasswordRequest_get_Email_m542831428 (AddUsernamePasswordRequest_t1599161869 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.AddUsernamePasswordRequest::set_Email(System.String)
extern "C"  void AddUsernamePasswordRequest_set_Email_m90716021 (AddUsernamePasswordRequest_t1599161869 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PlayFab.ClientModels.AddUsernamePasswordRequest::get_Password()
extern "C"  String_t* AddUsernamePasswordRequest_get_Password_m965850581 (AddUsernamePasswordRequest_t1599161869 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.AddUsernamePasswordRequest::set_Password(System.String)
extern "C"  void AddUsernamePasswordRequest_set_Password_m3336146614 (AddUsernamePasswordRequest_t1599161869 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
