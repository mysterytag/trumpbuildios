﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.InternalUtil.EmptyObserver`1<UniRx.CollectionAddEvent`1<System.Object>>
struct EmptyObserver_1_t1825874977;
// System.Exception
struct Exception_t1967233988;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Exception1967233988.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_CollectionAddE2416521987.h"

// System.Void UniRx.InternalUtil.EmptyObserver`1<UniRx.CollectionAddEvent`1<System.Object>>::.ctor()
extern "C"  void EmptyObserver_1__ctor_m555827254_gshared (EmptyObserver_1_t1825874977 * __this, const MethodInfo* method);
#define EmptyObserver_1__ctor_m555827254(__this, method) ((  void (*) (EmptyObserver_1_t1825874977 *, const MethodInfo*))EmptyObserver_1__ctor_m555827254_gshared)(__this, method)
// System.Void UniRx.InternalUtil.EmptyObserver`1<UniRx.CollectionAddEvent`1<System.Object>>::.cctor()
extern "C"  void EmptyObserver_1__cctor_m3863646775_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define EmptyObserver_1__cctor_m3863646775(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))EmptyObserver_1__cctor_m3863646775_gshared)(__this /* static, unused */, method)
// System.Void UniRx.InternalUtil.EmptyObserver`1<UniRx.CollectionAddEvent`1<System.Object>>::OnCompleted()
extern "C"  void EmptyObserver_1_OnCompleted_m749531968_gshared (EmptyObserver_1_t1825874977 * __this, const MethodInfo* method);
#define EmptyObserver_1_OnCompleted_m749531968(__this, method) ((  void (*) (EmptyObserver_1_t1825874977 *, const MethodInfo*))EmptyObserver_1_OnCompleted_m749531968_gshared)(__this, method)
// System.Void UniRx.InternalUtil.EmptyObserver`1<UniRx.CollectionAddEvent`1<System.Object>>::OnError(System.Exception)
extern "C"  void EmptyObserver_1_OnError_m2806301805_gshared (EmptyObserver_1_t1825874977 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method);
#define EmptyObserver_1_OnError_m2806301805(__this, ___error0, method) ((  void (*) (EmptyObserver_1_t1825874977 *, Exception_t1967233988 *, const MethodInfo*))EmptyObserver_1_OnError_m2806301805_gshared)(__this, ___error0, method)
// System.Void UniRx.InternalUtil.EmptyObserver`1<UniRx.CollectionAddEvent`1<System.Object>>::OnNext(T)
extern "C"  void EmptyObserver_1_OnNext_m109782366_gshared (EmptyObserver_1_t1825874977 * __this, CollectionAddEvent_1_t2416521987  ___value0, const MethodInfo* method);
#define EmptyObserver_1_OnNext_m109782366(__this, ___value0, method) ((  void (*) (EmptyObserver_1_t1825874977 *, CollectionAddEvent_1_t2416521987 , const MethodInfo*))EmptyObserver_1_OnNext_m109782366_gshared)(__this, ___value0, method)
