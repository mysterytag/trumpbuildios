﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Zenject.BindingId
struct BindingId_t2965794261;
// System.Type[]
struct TypeU5BU5D_t3431720054;
// System.Collections.Generic.List`1<Zenject.ProviderBase>
struct List_1_t2424453360;
// Zenject.InjectContext
struct InjectContext_t3456483891;
// Zenject.ProviderBase
struct ProviderBase_t1627494391;
// Zenject.IValidatableFactory
struct IValidatableFactory_t1679542623;
// System.Type
struct Type_t;
// System.Collections.Generic.IEnumerator`1<Zenject.ZenjectResolveException>
struct IEnumerator_1_t2684159447;
// Zenject.ZenjectResolveException
struct ZenjectResolveException_t1201052999;
// Zenject.IValidatable
struct IValidatable_t3033065077;
// Zenject.DiContainer
struct DiContainer_t2383114449;
// System.Func`2<Zenject.ProviderBase,System.Boolean>
struct Func_2_t3272172530;

#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumerat974100832.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_22091358871.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumerat510236352.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.DiContainer/<ValidateValidatables>c__Iterator4B
struct  U3CValidateValidatablesU3Ec__Iterator4B_t1746344957  : public Il2CppObject
{
public:
	// System.Collections.Generic.List`1/Enumerator<System.Collections.Generic.KeyValuePair`2<Zenject.BindingId,System.Collections.Generic.List`1<Zenject.ProviderBase>>> Zenject.DiContainer/<ValidateValidatables>c__Iterator4B::<$s_282>__0
	Enumerator_t974100832  ___U3CU24s_282U3E__0_0;
	// System.Collections.Generic.KeyValuePair`2<Zenject.BindingId,System.Collections.Generic.List`1<Zenject.ProviderBase>> Zenject.DiContainer/<ValidateValidatables>c__Iterator4B::<pair>__1
	KeyValuePair_2_t2091358871  ___U3CpairU3E__1_1;
	// Zenject.BindingId Zenject.DiContainer/<ValidateValidatables>c__Iterator4B::<bindingId>__2
	BindingId_t2965794261 * ___U3CbindingIdU3E__2_2;
	// System.Type[] Zenject.DiContainer/<ValidateValidatables>c__Iterator4B::ignoreTypes
	TypeU5BU5D_t3431720054* ___ignoreTypes_3;
	// System.Collections.Generic.List`1<Zenject.ProviderBase> Zenject.DiContainer/<ValidateValidatables>c__Iterator4B::<providers>__3
	List_1_t2424453360 * ___U3CprovidersU3E__3_4;
	// System.Collections.Generic.List`1<Zenject.ProviderBase> Zenject.DiContainer/<ValidateValidatables>c__Iterator4B::<validatableFactoryProviders>__4
	List_1_t2424453360 * ___U3CvalidatableFactoryProvidersU3E__4_5;
	// Zenject.InjectContext Zenject.DiContainer/<ValidateValidatables>c__Iterator4B::<injectCtx>__5
	InjectContext_t3456483891 * ___U3CinjectCtxU3E__5_6;
	// System.Collections.Generic.List`1/Enumerator<Zenject.ProviderBase> Zenject.DiContainer/<ValidateValidatables>c__Iterator4B::<$s_283>__6
	Enumerator_t510236352  ___U3CU24s_283U3E__6_7;
	// Zenject.ProviderBase Zenject.DiContainer/<ValidateValidatables>c__Iterator4B::<provider>__7
	ProviderBase_t1627494391 * ___U3CproviderU3E__7_8;
	// Zenject.IValidatableFactory Zenject.DiContainer/<ValidateValidatables>c__Iterator4B::<factory>__8
	Il2CppObject * ___U3CfactoryU3E__8_9;
	// System.Type Zenject.DiContainer/<ValidateValidatables>c__Iterator4B::<type>__9
	Type_t * ___U3CtypeU3E__9_10;
	// System.Type[] Zenject.DiContainer/<ValidateValidatables>c__Iterator4B::<providedArgs>__10
	TypeU5BU5D_t3431720054* ___U3CprovidedArgsU3E__10_11;
	// System.Collections.Generic.IEnumerator`1<Zenject.ZenjectResolveException> Zenject.DiContainer/<ValidateValidatables>c__Iterator4B::<$s_284>__11
	Il2CppObject* ___U3CU24s_284U3E__11_12;
	// Zenject.ZenjectResolveException Zenject.DiContainer/<ValidateValidatables>c__Iterator4B::<error>__12
	ZenjectResolveException_t1201052999 * ___U3CerrorU3E__12_13;
	// System.Collections.Generic.List`1<Zenject.ProviderBase> Zenject.DiContainer/<ValidateValidatables>c__Iterator4B::<validatableProviders>__13
	List_1_t2424453360 * ___U3CvalidatableProvidersU3E__13_14;
	// System.Collections.Generic.List`1/Enumerator<Zenject.ProviderBase> Zenject.DiContainer/<ValidateValidatables>c__Iterator4B::<$s_285>__14
	Enumerator_t510236352  ___U3CU24s_285U3E__14_15;
	// Zenject.ProviderBase Zenject.DiContainer/<ValidateValidatables>c__Iterator4B::<provider>__15
	ProviderBase_t1627494391 * ___U3CproviderU3E__15_16;
	// Zenject.IValidatable Zenject.DiContainer/<ValidateValidatables>c__Iterator4B::<factory>__16
	Il2CppObject * ___U3CfactoryU3E__16_17;
	// System.Collections.Generic.IEnumerator`1<Zenject.ZenjectResolveException> Zenject.DiContainer/<ValidateValidatables>c__Iterator4B::<$s_286>__17
	Il2CppObject* ___U3CU24s_286U3E__17_18;
	// Zenject.ZenjectResolveException Zenject.DiContainer/<ValidateValidatables>c__Iterator4B::<error>__18
	ZenjectResolveException_t1201052999 * ___U3CerrorU3E__18_19;
	// System.Int32 Zenject.DiContainer/<ValidateValidatables>c__Iterator4B::$PC
	int32_t ___U24PC_20;
	// Zenject.ZenjectResolveException Zenject.DiContainer/<ValidateValidatables>c__Iterator4B::$current
	ZenjectResolveException_t1201052999 * ___U24current_21;
	// System.Type[] Zenject.DiContainer/<ValidateValidatables>c__Iterator4B::<$>ignoreTypes
	TypeU5BU5D_t3431720054* ___U3CU24U3EignoreTypes_22;
	// Zenject.DiContainer Zenject.DiContainer/<ValidateValidatables>c__Iterator4B::<>f__this
	DiContainer_t2383114449 * ___U3CU3Ef__this_23;

public:
	inline static int32_t get_offset_of_U3CU24s_282U3E__0_0() { return static_cast<int32_t>(offsetof(U3CValidateValidatablesU3Ec__Iterator4B_t1746344957, ___U3CU24s_282U3E__0_0)); }
	inline Enumerator_t974100832  get_U3CU24s_282U3E__0_0() const { return ___U3CU24s_282U3E__0_0; }
	inline Enumerator_t974100832 * get_address_of_U3CU24s_282U3E__0_0() { return &___U3CU24s_282U3E__0_0; }
	inline void set_U3CU24s_282U3E__0_0(Enumerator_t974100832  value)
	{
		___U3CU24s_282U3E__0_0 = value;
	}

	inline static int32_t get_offset_of_U3CpairU3E__1_1() { return static_cast<int32_t>(offsetof(U3CValidateValidatablesU3Ec__Iterator4B_t1746344957, ___U3CpairU3E__1_1)); }
	inline KeyValuePair_2_t2091358871  get_U3CpairU3E__1_1() const { return ___U3CpairU3E__1_1; }
	inline KeyValuePair_2_t2091358871 * get_address_of_U3CpairU3E__1_1() { return &___U3CpairU3E__1_1; }
	inline void set_U3CpairU3E__1_1(KeyValuePair_2_t2091358871  value)
	{
		___U3CpairU3E__1_1 = value;
	}

	inline static int32_t get_offset_of_U3CbindingIdU3E__2_2() { return static_cast<int32_t>(offsetof(U3CValidateValidatablesU3Ec__Iterator4B_t1746344957, ___U3CbindingIdU3E__2_2)); }
	inline BindingId_t2965794261 * get_U3CbindingIdU3E__2_2() const { return ___U3CbindingIdU3E__2_2; }
	inline BindingId_t2965794261 ** get_address_of_U3CbindingIdU3E__2_2() { return &___U3CbindingIdU3E__2_2; }
	inline void set_U3CbindingIdU3E__2_2(BindingId_t2965794261 * value)
	{
		___U3CbindingIdU3E__2_2 = value;
		Il2CppCodeGenWriteBarrier(&___U3CbindingIdU3E__2_2, value);
	}

	inline static int32_t get_offset_of_ignoreTypes_3() { return static_cast<int32_t>(offsetof(U3CValidateValidatablesU3Ec__Iterator4B_t1746344957, ___ignoreTypes_3)); }
	inline TypeU5BU5D_t3431720054* get_ignoreTypes_3() const { return ___ignoreTypes_3; }
	inline TypeU5BU5D_t3431720054** get_address_of_ignoreTypes_3() { return &___ignoreTypes_3; }
	inline void set_ignoreTypes_3(TypeU5BU5D_t3431720054* value)
	{
		___ignoreTypes_3 = value;
		Il2CppCodeGenWriteBarrier(&___ignoreTypes_3, value);
	}

	inline static int32_t get_offset_of_U3CprovidersU3E__3_4() { return static_cast<int32_t>(offsetof(U3CValidateValidatablesU3Ec__Iterator4B_t1746344957, ___U3CprovidersU3E__3_4)); }
	inline List_1_t2424453360 * get_U3CprovidersU3E__3_4() const { return ___U3CprovidersU3E__3_4; }
	inline List_1_t2424453360 ** get_address_of_U3CprovidersU3E__3_4() { return &___U3CprovidersU3E__3_4; }
	inline void set_U3CprovidersU3E__3_4(List_1_t2424453360 * value)
	{
		___U3CprovidersU3E__3_4 = value;
		Il2CppCodeGenWriteBarrier(&___U3CprovidersU3E__3_4, value);
	}

	inline static int32_t get_offset_of_U3CvalidatableFactoryProvidersU3E__4_5() { return static_cast<int32_t>(offsetof(U3CValidateValidatablesU3Ec__Iterator4B_t1746344957, ___U3CvalidatableFactoryProvidersU3E__4_5)); }
	inline List_1_t2424453360 * get_U3CvalidatableFactoryProvidersU3E__4_5() const { return ___U3CvalidatableFactoryProvidersU3E__4_5; }
	inline List_1_t2424453360 ** get_address_of_U3CvalidatableFactoryProvidersU3E__4_5() { return &___U3CvalidatableFactoryProvidersU3E__4_5; }
	inline void set_U3CvalidatableFactoryProvidersU3E__4_5(List_1_t2424453360 * value)
	{
		___U3CvalidatableFactoryProvidersU3E__4_5 = value;
		Il2CppCodeGenWriteBarrier(&___U3CvalidatableFactoryProvidersU3E__4_5, value);
	}

	inline static int32_t get_offset_of_U3CinjectCtxU3E__5_6() { return static_cast<int32_t>(offsetof(U3CValidateValidatablesU3Ec__Iterator4B_t1746344957, ___U3CinjectCtxU3E__5_6)); }
	inline InjectContext_t3456483891 * get_U3CinjectCtxU3E__5_6() const { return ___U3CinjectCtxU3E__5_6; }
	inline InjectContext_t3456483891 ** get_address_of_U3CinjectCtxU3E__5_6() { return &___U3CinjectCtxU3E__5_6; }
	inline void set_U3CinjectCtxU3E__5_6(InjectContext_t3456483891 * value)
	{
		___U3CinjectCtxU3E__5_6 = value;
		Il2CppCodeGenWriteBarrier(&___U3CinjectCtxU3E__5_6, value);
	}

	inline static int32_t get_offset_of_U3CU24s_283U3E__6_7() { return static_cast<int32_t>(offsetof(U3CValidateValidatablesU3Ec__Iterator4B_t1746344957, ___U3CU24s_283U3E__6_7)); }
	inline Enumerator_t510236352  get_U3CU24s_283U3E__6_7() const { return ___U3CU24s_283U3E__6_7; }
	inline Enumerator_t510236352 * get_address_of_U3CU24s_283U3E__6_7() { return &___U3CU24s_283U3E__6_7; }
	inline void set_U3CU24s_283U3E__6_7(Enumerator_t510236352  value)
	{
		___U3CU24s_283U3E__6_7 = value;
	}

	inline static int32_t get_offset_of_U3CproviderU3E__7_8() { return static_cast<int32_t>(offsetof(U3CValidateValidatablesU3Ec__Iterator4B_t1746344957, ___U3CproviderU3E__7_8)); }
	inline ProviderBase_t1627494391 * get_U3CproviderU3E__7_8() const { return ___U3CproviderU3E__7_8; }
	inline ProviderBase_t1627494391 ** get_address_of_U3CproviderU3E__7_8() { return &___U3CproviderU3E__7_8; }
	inline void set_U3CproviderU3E__7_8(ProviderBase_t1627494391 * value)
	{
		___U3CproviderU3E__7_8 = value;
		Il2CppCodeGenWriteBarrier(&___U3CproviderU3E__7_8, value);
	}

	inline static int32_t get_offset_of_U3CfactoryU3E__8_9() { return static_cast<int32_t>(offsetof(U3CValidateValidatablesU3Ec__Iterator4B_t1746344957, ___U3CfactoryU3E__8_9)); }
	inline Il2CppObject * get_U3CfactoryU3E__8_9() const { return ___U3CfactoryU3E__8_9; }
	inline Il2CppObject ** get_address_of_U3CfactoryU3E__8_9() { return &___U3CfactoryU3E__8_9; }
	inline void set_U3CfactoryU3E__8_9(Il2CppObject * value)
	{
		___U3CfactoryU3E__8_9 = value;
		Il2CppCodeGenWriteBarrier(&___U3CfactoryU3E__8_9, value);
	}

	inline static int32_t get_offset_of_U3CtypeU3E__9_10() { return static_cast<int32_t>(offsetof(U3CValidateValidatablesU3Ec__Iterator4B_t1746344957, ___U3CtypeU3E__9_10)); }
	inline Type_t * get_U3CtypeU3E__9_10() const { return ___U3CtypeU3E__9_10; }
	inline Type_t ** get_address_of_U3CtypeU3E__9_10() { return &___U3CtypeU3E__9_10; }
	inline void set_U3CtypeU3E__9_10(Type_t * value)
	{
		___U3CtypeU3E__9_10 = value;
		Il2CppCodeGenWriteBarrier(&___U3CtypeU3E__9_10, value);
	}

	inline static int32_t get_offset_of_U3CprovidedArgsU3E__10_11() { return static_cast<int32_t>(offsetof(U3CValidateValidatablesU3Ec__Iterator4B_t1746344957, ___U3CprovidedArgsU3E__10_11)); }
	inline TypeU5BU5D_t3431720054* get_U3CprovidedArgsU3E__10_11() const { return ___U3CprovidedArgsU3E__10_11; }
	inline TypeU5BU5D_t3431720054** get_address_of_U3CprovidedArgsU3E__10_11() { return &___U3CprovidedArgsU3E__10_11; }
	inline void set_U3CprovidedArgsU3E__10_11(TypeU5BU5D_t3431720054* value)
	{
		___U3CprovidedArgsU3E__10_11 = value;
		Il2CppCodeGenWriteBarrier(&___U3CprovidedArgsU3E__10_11, value);
	}

	inline static int32_t get_offset_of_U3CU24s_284U3E__11_12() { return static_cast<int32_t>(offsetof(U3CValidateValidatablesU3Ec__Iterator4B_t1746344957, ___U3CU24s_284U3E__11_12)); }
	inline Il2CppObject* get_U3CU24s_284U3E__11_12() const { return ___U3CU24s_284U3E__11_12; }
	inline Il2CppObject** get_address_of_U3CU24s_284U3E__11_12() { return &___U3CU24s_284U3E__11_12; }
	inline void set_U3CU24s_284U3E__11_12(Il2CppObject* value)
	{
		___U3CU24s_284U3E__11_12 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU24s_284U3E__11_12, value);
	}

	inline static int32_t get_offset_of_U3CerrorU3E__12_13() { return static_cast<int32_t>(offsetof(U3CValidateValidatablesU3Ec__Iterator4B_t1746344957, ___U3CerrorU3E__12_13)); }
	inline ZenjectResolveException_t1201052999 * get_U3CerrorU3E__12_13() const { return ___U3CerrorU3E__12_13; }
	inline ZenjectResolveException_t1201052999 ** get_address_of_U3CerrorU3E__12_13() { return &___U3CerrorU3E__12_13; }
	inline void set_U3CerrorU3E__12_13(ZenjectResolveException_t1201052999 * value)
	{
		___U3CerrorU3E__12_13 = value;
		Il2CppCodeGenWriteBarrier(&___U3CerrorU3E__12_13, value);
	}

	inline static int32_t get_offset_of_U3CvalidatableProvidersU3E__13_14() { return static_cast<int32_t>(offsetof(U3CValidateValidatablesU3Ec__Iterator4B_t1746344957, ___U3CvalidatableProvidersU3E__13_14)); }
	inline List_1_t2424453360 * get_U3CvalidatableProvidersU3E__13_14() const { return ___U3CvalidatableProvidersU3E__13_14; }
	inline List_1_t2424453360 ** get_address_of_U3CvalidatableProvidersU3E__13_14() { return &___U3CvalidatableProvidersU3E__13_14; }
	inline void set_U3CvalidatableProvidersU3E__13_14(List_1_t2424453360 * value)
	{
		___U3CvalidatableProvidersU3E__13_14 = value;
		Il2CppCodeGenWriteBarrier(&___U3CvalidatableProvidersU3E__13_14, value);
	}

	inline static int32_t get_offset_of_U3CU24s_285U3E__14_15() { return static_cast<int32_t>(offsetof(U3CValidateValidatablesU3Ec__Iterator4B_t1746344957, ___U3CU24s_285U3E__14_15)); }
	inline Enumerator_t510236352  get_U3CU24s_285U3E__14_15() const { return ___U3CU24s_285U3E__14_15; }
	inline Enumerator_t510236352 * get_address_of_U3CU24s_285U3E__14_15() { return &___U3CU24s_285U3E__14_15; }
	inline void set_U3CU24s_285U3E__14_15(Enumerator_t510236352  value)
	{
		___U3CU24s_285U3E__14_15 = value;
	}

	inline static int32_t get_offset_of_U3CproviderU3E__15_16() { return static_cast<int32_t>(offsetof(U3CValidateValidatablesU3Ec__Iterator4B_t1746344957, ___U3CproviderU3E__15_16)); }
	inline ProviderBase_t1627494391 * get_U3CproviderU3E__15_16() const { return ___U3CproviderU3E__15_16; }
	inline ProviderBase_t1627494391 ** get_address_of_U3CproviderU3E__15_16() { return &___U3CproviderU3E__15_16; }
	inline void set_U3CproviderU3E__15_16(ProviderBase_t1627494391 * value)
	{
		___U3CproviderU3E__15_16 = value;
		Il2CppCodeGenWriteBarrier(&___U3CproviderU3E__15_16, value);
	}

	inline static int32_t get_offset_of_U3CfactoryU3E__16_17() { return static_cast<int32_t>(offsetof(U3CValidateValidatablesU3Ec__Iterator4B_t1746344957, ___U3CfactoryU3E__16_17)); }
	inline Il2CppObject * get_U3CfactoryU3E__16_17() const { return ___U3CfactoryU3E__16_17; }
	inline Il2CppObject ** get_address_of_U3CfactoryU3E__16_17() { return &___U3CfactoryU3E__16_17; }
	inline void set_U3CfactoryU3E__16_17(Il2CppObject * value)
	{
		___U3CfactoryU3E__16_17 = value;
		Il2CppCodeGenWriteBarrier(&___U3CfactoryU3E__16_17, value);
	}

	inline static int32_t get_offset_of_U3CU24s_286U3E__17_18() { return static_cast<int32_t>(offsetof(U3CValidateValidatablesU3Ec__Iterator4B_t1746344957, ___U3CU24s_286U3E__17_18)); }
	inline Il2CppObject* get_U3CU24s_286U3E__17_18() const { return ___U3CU24s_286U3E__17_18; }
	inline Il2CppObject** get_address_of_U3CU24s_286U3E__17_18() { return &___U3CU24s_286U3E__17_18; }
	inline void set_U3CU24s_286U3E__17_18(Il2CppObject* value)
	{
		___U3CU24s_286U3E__17_18 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU24s_286U3E__17_18, value);
	}

	inline static int32_t get_offset_of_U3CerrorU3E__18_19() { return static_cast<int32_t>(offsetof(U3CValidateValidatablesU3Ec__Iterator4B_t1746344957, ___U3CerrorU3E__18_19)); }
	inline ZenjectResolveException_t1201052999 * get_U3CerrorU3E__18_19() const { return ___U3CerrorU3E__18_19; }
	inline ZenjectResolveException_t1201052999 ** get_address_of_U3CerrorU3E__18_19() { return &___U3CerrorU3E__18_19; }
	inline void set_U3CerrorU3E__18_19(ZenjectResolveException_t1201052999 * value)
	{
		___U3CerrorU3E__18_19 = value;
		Il2CppCodeGenWriteBarrier(&___U3CerrorU3E__18_19, value);
	}

	inline static int32_t get_offset_of_U24PC_20() { return static_cast<int32_t>(offsetof(U3CValidateValidatablesU3Ec__Iterator4B_t1746344957, ___U24PC_20)); }
	inline int32_t get_U24PC_20() const { return ___U24PC_20; }
	inline int32_t* get_address_of_U24PC_20() { return &___U24PC_20; }
	inline void set_U24PC_20(int32_t value)
	{
		___U24PC_20 = value;
	}

	inline static int32_t get_offset_of_U24current_21() { return static_cast<int32_t>(offsetof(U3CValidateValidatablesU3Ec__Iterator4B_t1746344957, ___U24current_21)); }
	inline ZenjectResolveException_t1201052999 * get_U24current_21() const { return ___U24current_21; }
	inline ZenjectResolveException_t1201052999 ** get_address_of_U24current_21() { return &___U24current_21; }
	inline void set_U24current_21(ZenjectResolveException_t1201052999 * value)
	{
		___U24current_21 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_21, value);
	}

	inline static int32_t get_offset_of_U3CU24U3EignoreTypes_22() { return static_cast<int32_t>(offsetof(U3CValidateValidatablesU3Ec__Iterator4B_t1746344957, ___U3CU24U3EignoreTypes_22)); }
	inline TypeU5BU5D_t3431720054* get_U3CU24U3EignoreTypes_22() const { return ___U3CU24U3EignoreTypes_22; }
	inline TypeU5BU5D_t3431720054** get_address_of_U3CU24U3EignoreTypes_22() { return &___U3CU24U3EignoreTypes_22; }
	inline void set_U3CU24U3EignoreTypes_22(TypeU5BU5D_t3431720054* value)
	{
		___U3CU24U3EignoreTypes_22 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU24U3EignoreTypes_22, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__this_23() { return static_cast<int32_t>(offsetof(U3CValidateValidatablesU3Ec__Iterator4B_t1746344957, ___U3CU3Ef__this_23)); }
	inline DiContainer_t2383114449 * get_U3CU3Ef__this_23() const { return ___U3CU3Ef__this_23; }
	inline DiContainer_t2383114449 ** get_address_of_U3CU3Ef__this_23() { return &___U3CU3Ef__this_23; }
	inline void set_U3CU3Ef__this_23(DiContainer_t2383114449 * value)
	{
		___U3CU3Ef__this_23 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__this_23, value);
	}
};

struct U3CValidateValidatablesU3Ec__Iterator4B_t1746344957_StaticFields
{
public:
	// System.Func`2<Zenject.ProviderBase,System.Boolean> Zenject.DiContainer/<ValidateValidatables>c__Iterator4B::<>f__am$cache18
	Func_2_t3272172530 * ___U3CU3Ef__amU24cache18_24;
	// System.Func`2<Zenject.ProviderBase,System.Boolean> Zenject.DiContainer/<ValidateValidatables>c__Iterator4B::<>f__am$cache19
	Func_2_t3272172530 * ___U3CU3Ef__amU24cache19_25;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache18_24() { return static_cast<int32_t>(offsetof(U3CValidateValidatablesU3Ec__Iterator4B_t1746344957_StaticFields, ___U3CU3Ef__amU24cache18_24)); }
	inline Func_2_t3272172530 * get_U3CU3Ef__amU24cache18_24() const { return ___U3CU3Ef__amU24cache18_24; }
	inline Func_2_t3272172530 ** get_address_of_U3CU3Ef__amU24cache18_24() { return &___U3CU3Ef__amU24cache18_24; }
	inline void set_U3CU3Ef__amU24cache18_24(Func_2_t3272172530 * value)
	{
		___U3CU3Ef__amU24cache18_24 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache18_24, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache19_25() { return static_cast<int32_t>(offsetof(U3CValidateValidatablesU3Ec__Iterator4B_t1746344957_StaticFields, ___U3CU3Ef__amU24cache19_25)); }
	inline Func_2_t3272172530 * get_U3CU3Ef__amU24cache19_25() const { return ___U3CU3Ef__amU24cache19_25; }
	inline Func_2_t3272172530 ** get_address_of_U3CU3Ef__amU24cache19_25() { return &___U3CU3Ef__amU24cache19_25; }
	inline void set_U3CU3Ef__amU24cache19_25(Func_2_t3272172530 * value)
	{
		___U3CU3Ef__amU24cache19_25 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache19_25, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
