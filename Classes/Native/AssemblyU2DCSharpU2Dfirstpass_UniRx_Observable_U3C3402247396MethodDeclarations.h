﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Observable/<FromAsyncPattern>c__AnonStorey45
struct U3CFromAsyncPatternU3Ec__AnonStorey45_t3402247396;
// System.IAsyncResult
struct IAsyncResult_t537683269;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Unit2558286038.h"

// System.Void UniRx.Observable/<FromAsyncPattern>c__AnonStorey45::.ctor()
extern "C"  void U3CFromAsyncPatternU3Ec__AnonStorey45__ctor_m2339631455 (U3CFromAsyncPatternU3Ec__AnonStorey45_t3402247396 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UniRx.Unit UniRx.Observable/<FromAsyncPattern>c__AnonStorey45::<>m__43(System.IAsyncResult)
extern "C"  Unit_t2558286038  U3CFromAsyncPatternU3Ec__AnonStorey45_U3CU3Em__43_m1918899173 (U3CFromAsyncPatternU3Ec__AnonStorey45_t3402247396 * __this, Il2CppObject * ___iar0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
