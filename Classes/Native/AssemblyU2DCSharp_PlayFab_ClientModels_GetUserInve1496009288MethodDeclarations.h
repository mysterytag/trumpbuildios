﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.ClientModels.GetUserInventoryResult
struct GetUserInventoryResult_t1496009288;
// System.Collections.Generic.List`1<PlayFab.ClientModels.ItemInstance>
struct List_1_t1322103281;
// System.Collections.Generic.Dictionary`2<System.String,System.Int32>
struct Dictionary_2_t190145395;
// System.Collections.Generic.Dictionary`2<System.String,PlayFab.ClientModels.VirtualCurrencyRechargeTime>
struct Dictionary_2_t3222006224;

#include "codegen/il2cpp-codegen.h"

// System.Void PlayFab.ClientModels.GetUserInventoryResult::.ctor()
extern "C"  void GetUserInventoryResult__ctor_m1117540213 (GetUserInventoryResult_t1496009288 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<PlayFab.ClientModels.ItemInstance> PlayFab.ClientModels.GetUserInventoryResult::get_Inventory()
extern "C"  List_1_t1322103281 * GetUserInventoryResult_get_Inventory_m784641069 (GetUserInventoryResult_t1496009288 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.GetUserInventoryResult::set_Inventory(System.Collections.Generic.List`1<PlayFab.ClientModels.ItemInstance>)
extern "C"  void GetUserInventoryResult_set_Inventory_m1822294012 (GetUserInventoryResult_t1496009288 * __this, List_1_t1322103281 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.Dictionary`2<System.String,System.Int32> PlayFab.ClientModels.GetUserInventoryResult::get_VirtualCurrency()
extern "C"  Dictionary_2_t190145395 * GetUserInventoryResult_get_VirtualCurrency_m57097835 (GetUserInventoryResult_t1496009288 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.GetUserInventoryResult::set_VirtualCurrency(System.Collections.Generic.Dictionary`2<System.String,System.Int32>)
extern "C"  void GetUserInventoryResult_set_VirtualCurrency_m1054214002 (GetUserInventoryResult_t1496009288 * __this, Dictionary_2_t190145395 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.Dictionary`2<System.String,PlayFab.ClientModels.VirtualCurrencyRechargeTime> PlayFab.ClientModels.GetUserInventoryResult::get_VirtualCurrencyRechargeTimes()
extern "C"  Dictionary_2_t3222006224 * GetUserInventoryResult_get_VirtualCurrencyRechargeTimes_m33878707 (GetUserInventoryResult_t1496009288 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.GetUserInventoryResult::set_VirtualCurrencyRechargeTimes(System.Collections.Generic.Dictionary`2<System.String,PlayFab.ClientModels.VirtualCurrencyRechargeTime>)
extern "C"  void GetUserInventoryResult_set_VirtualCurrencyRechargeTimes_m1958622040 (GetUserInventoryResult_t1496009288 * __this, Dictionary_2_t3222006224 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
