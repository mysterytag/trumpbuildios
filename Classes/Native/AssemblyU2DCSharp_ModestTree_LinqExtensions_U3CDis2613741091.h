﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Generic.IEqualityComparer`1<System.Object>
struct IEqualityComparer_1_t3161373071;
// System.Collections.Generic.HashSet`1<System.Object>
struct HashSet_1_t3535795091;
// System.Collections.Generic.IEnumerable`1<System.Object>
struct IEnumerable_1_t3709260776;
// System.Collections.Generic.IEnumerator`1<System.Object>
struct IEnumerator_1_t2320212868;
// System.Object
struct Il2CppObject;
// System.Func`2<System.Object,System.Object>
struct Func_2_t2135783352;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ModestTree.LinqExtensions/<DistinctByImpl>c__Iterator3C`2<System.Object,System.Object>
struct  U3CDistinctByImplU3Ec__Iterator3C_2_t2613741091  : public Il2CppObject
{
public:
	// System.Collections.Generic.IEqualityComparer`1<TKey> ModestTree.LinqExtensions/<DistinctByImpl>c__Iterator3C`2::comparer
	Il2CppObject* ___comparer_0;
	// System.Collections.Generic.HashSet`1<TKey> ModestTree.LinqExtensions/<DistinctByImpl>c__Iterator3C`2::<knownKeys>__0
	HashSet_1_t3535795091 * ___U3CknownKeysU3E__0_1;
	// System.Collections.Generic.IEnumerable`1<TSource> ModestTree.LinqExtensions/<DistinctByImpl>c__Iterator3C`2::source
	Il2CppObject* ___source_2;
	// System.Collections.Generic.IEnumerator`1<TSource> ModestTree.LinqExtensions/<DistinctByImpl>c__Iterator3C`2::<$s_239>__1
	Il2CppObject* ___U3CU24s_239U3E__1_3;
	// TSource ModestTree.LinqExtensions/<DistinctByImpl>c__Iterator3C`2::<element>__2
	Il2CppObject * ___U3CelementU3E__2_4;
	// System.Func`2<TSource,TKey> ModestTree.LinqExtensions/<DistinctByImpl>c__Iterator3C`2::keySelector
	Func_2_t2135783352 * ___keySelector_5;
	// System.Int32 ModestTree.LinqExtensions/<DistinctByImpl>c__Iterator3C`2::$PC
	int32_t ___U24PC_6;
	// TSource ModestTree.LinqExtensions/<DistinctByImpl>c__Iterator3C`2::$current
	Il2CppObject * ___U24current_7;
	// System.Collections.Generic.IEqualityComparer`1<TKey> ModestTree.LinqExtensions/<DistinctByImpl>c__Iterator3C`2::<$>comparer
	Il2CppObject* ___U3CU24U3Ecomparer_8;
	// System.Collections.Generic.IEnumerable`1<TSource> ModestTree.LinqExtensions/<DistinctByImpl>c__Iterator3C`2::<$>source
	Il2CppObject* ___U3CU24U3Esource_9;
	// System.Func`2<TSource,TKey> ModestTree.LinqExtensions/<DistinctByImpl>c__Iterator3C`2::<$>keySelector
	Func_2_t2135783352 * ___U3CU24U3EkeySelector_10;

public:
	inline static int32_t get_offset_of_comparer_0() { return static_cast<int32_t>(offsetof(U3CDistinctByImplU3Ec__Iterator3C_2_t2613741091, ___comparer_0)); }
	inline Il2CppObject* get_comparer_0() const { return ___comparer_0; }
	inline Il2CppObject** get_address_of_comparer_0() { return &___comparer_0; }
	inline void set_comparer_0(Il2CppObject* value)
	{
		___comparer_0 = value;
		Il2CppCodeGenWriteBarrier(&___comparer_0, value);
	}

	inline static int32_t get_offset_of_U3CknownKeysU3E__0_1() { return static_cast<int32_t>(offsetof(U3CDistinctByImplU3Ec__Iterator3C_2_t2613741091, ___U3CknownKeysU3E__0_1)); }
	inline HashSet_1_t3535795091 * get_U3CknownKeysU3E__0_1() const { return ___U3CknownKeysU3E__0_1; }
	inline HashSet_1_t3535795091 ** get_address_of_U3CknownKeysU3E__0_1() { return &___U3CknownKeysU3E__0_1; }
	inline void set_U3CknownKeysU3E__0_1(HashSet_1_t3535795091 * value)
	{
		___U3CknownKeysU3E__0_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CknownKeysU3E__0_1, value);
	}

	inline static int32_t get_offset_of_source_2() { return static_cast<int32_t>(offsetof(U3CDistinctByImplU3Ec__Iterator3C_2_t2613741091, ___source_2)); }
	inline Il2CppObject* get_source_2() const { return ___source_2; }
	inline Il2CppObject** get_address_of_source_2() { return &___source_2; }
	inline void set_source_2(Il2CppObject* value)
	{
		___source_2 = value;
		Il2CppCodeGenWriteBarrier(&___source_2, value);
	}

	inline static int32_t get_offset_of_U3CU24s_239U3E__1_3() { return static_cast<int32_t>(offsetof(U3CDistinctByImplU3Ec__Iterator3C_2_t2613741091, ___U3CU24s_239U3E__1_3)); }
	inline Il2CppObject* get_U3CU24s_239U3E__1_3() const { return ___U3CU24s_239U3E__1_3; }
	inline Il2CppObject** get_address_of_U3CU24s_239U3E__1_3() { return &___U3CU24s_239U3E__1_3; }
	inline void set_U3CU24s_239U3E__1_3(Il2CppObject* value)
	{
		___U3CU24s_239U3E__1_3 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU24s_239U3E__1_3, value);
	}

	inline static int32_t get_offset_of_U3CelementU3E__2_4() { return static_cast<int32_t>(offsetof(U3CDistinctByImplU3Ec__Iterator3C_2_t2613741091, ___U3CelementU3E__2_4)); }
	inline Il2CppObject * get_U3CelementU3E__2_4() const { return ___U3CelementU3E__2_4; }
	inline Il2CppObject ** get_address_of_U3CelementU3E__2_4() { return &___U3CelementU3E__2_4; }
	inline void set_U3CelementU3E__2_4(Il2CppObject * value)
	{
		___U3CelementU3E__2_4 = value;
		Il2CppCodeGenWriteBarrier(&___U3CelementU3E__2_4, value);
	}

	inline static int32_t get_offset_of_keySelector_5() { return static_cast<int32_t>(offsetof(U3CDistinctByImplU3Ec__Iterator3C_2_t2613741091, ___keySelector_5)); }
	inline Func_2_t2135783352 * get_keySelector_5() const { return ___keySelector_5; }
	inline Func_2_t2135783352 ** get_address_of_keySelector_5() { return &___keySelector_5; }
	inline void set_keySelector_5(Func_2_t2135783352 * value)
	{
		___keySelector_5 = value;
		Il2CppCodeGenWriteBarrier(&___keySelector_5, value);
	}

	inline static int32_t get_offset_of_U24PC_6() { return static_cast<int32_t>(offsetof(U3CDistinctByImplU3Ec__Iterator3C_2_t2613741091, ___U24PC_6)); }
	inline int32_t get_U24PC_6() const { return ___U24PC_6; }
	inline int32_t* get_address_of_U24PC_6() { return &___U24PC_6; }
	inline void set_U24PC_6(int32_t value)
	{
		___U24PC_6 = value;
	}

	inline static int32_t get_offset_of_U24current_7() { return static_cast<int32_t>(offsetof(U3CDistinctByImplU3Ec__Iterator3C_2_t2613741091, ___U24current_7)); }
	inline Il2CppObject * get_U24current_7() const { return ___U24current_7; }
	inline Il2CppObject ** get_address_of_U24current_7() { return &___U24current_7; }
	inline void set_U24current_7(Il2CppObject * value)
	{
		___U24current_7 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_7, value);
	}

	inline static int32_t get_offset_of_U3CU24U3Ecomparer_8() { return static_cast<int32_t>(offsetof(U3CDistinctByImplU3Ec__Iterator3C_2_t2613741091, ___U3CU24U3Ecomparer_8)); }
	inline Il2CppObject* get_U3CU24U3Ecomparer_8() const { return ___U3CU24U3Ecomparer_8; }
	inline Il2CppObject** get_address_of_U3CU24U3Ecomparer_8() { return &___U3CU24U3Ecomparer_8; }
	inline void set_U3CU24U3Ecomparer_8(Il2CppObject* value)
	{
		___U3CU24U3Ecomparer_8 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU24U3Ecomparer_8, value);
	}

	inline static int32_t get_offset_of_U3CU24U3Esource_9() { return static_cast<int32_t>(offsetof(U3CDistinctByImplU3Ec__Iterator3C_2_t2613741091, ___U3CU24U3Esource_9)); }
	inline Il2CppObject* get_U3CU24U3Esource_9() const { return ___U3CU24U3Esource_9; }
	inline Il2CppObject** get_address_of_U3CU24U3Esource_9() { return &___U3CU24U3Esource_9; }
	inline void set_U3CU24U3Esource_9(Il2CppObject* value)
	{
		___U3CU24U3Esource_9 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU24U3Esource_9, value);
	}

	inline static int32_t get_offset_of_U3CU24U3EkeySelector_10() { return static_cast<int32_t>(offsetof(U3CDistinctByImplU3Ec__Iterator3C_2_t2613741091, ___U3CU24U3EkeySelector_10)); }
	inline Func_2_t2135783352 * get_U3CU24U3EkeySelector_10() const { return ___U3CU24U3EkeySelector_10; }
	inline Func_2_t2135783352 ** get_address_of_U3CU24U3EkeySelector_10() { return &___U3CU24U3EkeySelector_10; }
	inline void set_U3CU24U3EkeySelector_10(Func_2_t2135783352 * value)
	{
		___U3CU24U3EkeySelector_10 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU24U3EkeySelector_10, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
