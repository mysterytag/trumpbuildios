﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.DelaySubscriptionObservable`1/<SubscribeCore>c__AnonStorey5E<System.Object>
struct U3CSubscribeCoreU3Ec__AnonStorey5E_t2206845999;

#include "codegen/il2cpp-codegen.h"

// System.Void UniRx.Operators.DelaySubscriptionObservable`1/<SubscribeCore>c__AnonStorey5E<System.Object>::.ctor()
extern "C"  void U3CSubscribeCoreU3Ec__AnonStorey5E__ctor_m777474035_gshared (U3CSubscribeCoreU3Ec__AnonStorey5E_t2206845999 * __this, const MethodInfo* method);
#define U3CSubscribeCoreU3Ec__AnonStorey5E__ctor_m777474035(__this, method) ((  void (*) (U3CSubscribeCoreU3Ec__AnonStorey5E_t2206845999 *, const MethodInfo*))U3CSubscribeCoreU3Ec__AnonStorey5E__ctor_m777474035_gshared)(__this, method)
