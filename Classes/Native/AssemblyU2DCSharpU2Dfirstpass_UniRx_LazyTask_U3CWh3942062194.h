﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Coroutine[]
struct CoroutineU5BU5D_t1904733000;
// UnityEngine.Coroutine
struct Coroutine_t2246592261;
struct Coroutine_t2246592261_marshaled_pinvoke;
// System.Object
struct Il2CppObject;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.LazyTask/<WhenAllCore>c__Iterator1F
struct  U3CWhenAllCoreU3Ec__Iterator1F_t3942062194  : public Il2CppObject
{
public:
	// UnityEngine.Coroutine[] UniRx.LazyTask/<WhenAllCore>c__Iterator1F::coroutines
	CoroutineU5BU5D_t1904733000* ___coroutines_0;
	// UnityEngine.Coroutine[] UniRx.LazyTask/<WhenAllCore>c__Iterator1F::<$s_339>__0
	CoroutineU5BU5D_t1904733000* ___U3CU24s_339U3E__0_1;
	// System.Int32 UniRx.LazyTask/<WhenAllCore>c__Iterator1F::<$s_340>__1
	int32_t ___U3CU24s_340U3E__1_2;
	// UnityEngine.Coroutine UniRx.LazyTask/<WhenAllCore>c__Iterator1F::<item>__2
	Coroutine_t2246592261 * ___U3CitemU3E__2_3;
	// System.Int32 UniRx.LazyTask/<WhenAllCore>c__Iterator1F::$PC
	int32_t ___U24PC_4;
	// System.Object UniRx.LazyTask/<WhenAllCore>c__Iterator1F::$current
	Il2CppObject * ___U24current_5;
	// UnityEngine.Coroutine[] UniRx.LazyTask/<WhenAllCore>c__Iterator1F::<$>coroutines
	CoroutineU5BU5D_t1904733000* ___U3CU24U3Ecoroutines_6;

public:
	inline static int32_t get_offset_of_coroutines_0() { return static_cast<int32_t>(offsetof(U3CWhenAllCoreU3Ec__Iterator1F_t3942062194, ___coroutines_0)); }
	inline CoroutineU5BU5D_t1904733000* get_coroutines_0() const { return ___coroutines_0; }
	inline CoroutineU5BU5D_t1904733000** get_address_of_coroutines_0() { return &___coroutines_0; }
	inline void set_coroutines_0(CoroutineU5BU5D_t1904733000* value)
	{
		___coroutines_0 = value;
		Il2CppCodeGenWriteBarrier(&___coroutines_0, value);
	}

	inline static int32_t get_offset_of_U3CU24s_339U3E__0_1() { return static_cast<int32_t>(offsetof(U3CWhenAllCoreU3Ec__Iterator1F_t3942062194, ___U3CU24s_339U3E__0_1)); }
	inline CoroutineU5BU5D_t1904733000* get_U3CU24s_339U3E__0_1() const { return ___U3CU24s_339U3E__0_1; }
	inline CoroutineU5BU5D_t1904733000** get_address_of_U3CU24s_339U3E__0_1() { return &___U3CU24s_339U3E__0_1; }
	inline void set_U3CU24s_339U3E__0_1(CoroutineU5BU5D_t1904733000* value)
	{
		___U3CU24s_339U3E__0_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU24s_339U3E__0_1, value);
	}

	inline static int32_t get_offset_of_U3CU24s_340U3E__1_2() { return static_cast<int32_t>(offsetof(U3CWhenAllCoreU3Ec__Iterator1F_t3942062194, ___U3CU24s_340U3E__1_2)); }
	inline int32_t get_U3CU24s_340U3E__1_2() const { return ___U3CU24s_340U3E__1_2; }
	inline int32_t* get_address_of_U3CU24s_340U3E__1_2() { return &___U3CU24s_340U3E__1_2; }
	inline void set_U3CU24s_340U3E__1_2(int32_t value)
	{
		___U3CU24s_340U3E__1_2 = value;
	}

	inline static int32_t get_offset_of_U3CitemU3E__2_3() { return static_cast<int32_t>(offsetof(U3CWhenAllCoreU3Ec__Iterator1F_t3942062194, ___U3CitemU3E__2_3)); }
	inline Coroutine_t2246592261 * get_U3CitemU3E__2_3() const { return ___U3CitemU3E__2_3; }
	inline Coroutine_t2246592261 ** get_address_of_U3CitemU3E__2_3() { return &___U3CitemU3E__2_3; }
	inline void set_U3CitemU3E__2_3(Coroutine_t2246592261 * value)
	{
		___U3CitemU3E__2_3 = value;
		Il2CppCodeGenWriteBarrier(&___U3CitemU3E__2_3, value);
	}

	inline static int32_t get_offset_of_U24PC_4() { return static_cast<int32_t>(offsetof(U3CWhenAllCoreU3Ec__Iterator1F_t3942062194, ___U24PC_4)); }
	inline int32_t get_U24PC_4() const { return ___U24PC_4; }
	inline int32_t* get_address_of_U24PC_4() { return &___U24PC_4; }
	inline void set_U24PC_4(int32_t value)
	{
		___U24PC_4 = value;
	}

	inline static int32_t get_offset_of_U24current_5() { return static_cast<int32_t>(offsetof(U3CWhenAllCoreU3Ec__Iterator1F_t3942062194, ___U24current_5)); }
	inline Il2CppObject * get_U24current_5() const { return ___U24current_5; }
	inline Il2CppObject ** get_address_of_U24current_5() { return &___U24current_5; }
	inline void set_U24current_5(Il2CppObject * value)
	{
		___U24current_5 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_5, value);
	}

	inline static int32_t get_offset_of_U3CU24U3Ecoroutines_6() { return static_cast<int32_t>(offsetof(U3CWhenAllCoreU3Ec__Iterator1F_t3942062194, ___U3CU24U3Ecoroutines_6)); }
	inline CoroutineU5BU5D_t1904733000* get_U3CU24U3Ecoroutines_6() const { return ___U3CU24U3Ecoroutines_6; }
	inline CoroutineU5BU5D_t1904733000** get_address_of_U3CU24U3Ecoroutines_6() { return &___U3CU24U3Ecoroutines_6; }
	inline void set_U3CU24U3Ecoroutines_6(CoroutineU5BU5D_t1904733000* value)
	{
		___U3CU24U3Ecoroutines_6 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU24U3Ecoroutines_6, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
