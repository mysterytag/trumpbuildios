﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.UnityGraphicExtensions/<DirtyLayoutCallbackAsObservable>c__AnonStorey9E/<DirtyLayoutCallbackAsObservable>c__AnonStorey9D
struct U3CDirtyLayoutCallbackAsObservableU3Ec__AnonStorey9D_t2584692430;

#include "codegen/il2cpp-codegen.h"

// System.Void UniRx.UnityGraphicExtensions/<DirtyLayoutCallbackAsObservable>c__AnonStorey9E/<DirtyLayoutCallbackAsObservable>c__AnonStorey9D::.ctor()
extern "C"  void U3CDirtyLayoutCallbackAsObservableU3Ec__AnonStorey9D__ctor_m3348232721 (U3CDirtyLayoutCallbackAsObservableU3Ec__AnonStorey9D_t2584692430 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.UnityGraphicExtensions/<DirtyLayoutCallbackAsObservable>c__AnonStorey9E/<DirtyLayoutCallbackAsObservable>c__AnonStorey9D::<>m__DB()
extern "C"  void U3CDirtyLayoutCallbackAsObservableU3Ec__AnonStorey9D_U3CU3Em__DB_m3029763480 (U3CDirtyLayoutCallbackAsObservableU3Ec__AnonStorey9D_t2584692430 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.UnityGraphicExtensions/<DirtyLayoutCallbackAsObservable>c__AnonStorey9E/<DirtyLayoutCallbackAsObservable>c__AnonStorey9D::<>m__DC()
extern "C"  void U3CDirtyLayoutCallbackAsObservableU3Ec__AnonStorey9D_U3CU3Em__DC_m3029764441 (U3CDirtyLayoutCallbackAsObservableU3Ec__AnonStorey9D_t2584692430 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
