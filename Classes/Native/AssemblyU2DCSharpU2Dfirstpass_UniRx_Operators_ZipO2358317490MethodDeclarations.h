﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.ZipObservable`3/Zip<System.Object,System.Object,System.Object>
struct Zip_t2358317490;
// UniRx.Operators.ZipObservable`3<System.Object,System.Object,System.Object>
struct ZipObservable_3_t2604753989;
// UniRx.IObserver`1<System.Object>
struct IObserver_1_t3049105323;
// System.IDisposable
struct IDisposable_t1628921374;
// System.Object
struct Il2CppObject;
// System.Exception
struct Exception_t1967233988;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_Exception1967233988.h"

// System.Void UniRx.Operators.ZipObservable`3/Zip<System.Object,System.Object,System.Object>::.ctor(UniRx.Operators.ZipObservable`3<TLeft,TRight,TResult>,UniRx.IObserver`1<TResult>,System.IDisposable)
extern "C"  void Zip__ctor_m3960046444_gshared (Zip_t2358317490 * __this, ZipObservable_3_t2604753989 * ___parent0, Il2CppObject* ___observer1, Il2CppObject * ___cancel2, const MethodInfo* method);
#define Zip__ctor_m3960046444(__this, ___parent0, ___observer1, ___cancel2, method) ((  void (*) (Zip_t2358317490 *, ZipObservable_3_t2604753989 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))Zip__ctor_m3960046444_gshared)(__this, ___parent0, ___observer1, ___cancel2, method)
// System.IDisposable UniRx.Operators.ZipObservable`3/Zip<System.Object,System.Object,System.Object>::Run()
extern "C"  Il2CppObject * Zip_Run_m2761737815_gshared (Zip_t2358317490 * __this, const MethodInfo* method);
#define Zip_Run_m2761737815(__this, method) ((  Il2CppObject * (*) (Zip_t2358317490 *, const MethodInfo*))Zip_Run_m2761737815_gshared)(__this, method)
// System.Void UniRx.Operators.ZipObservable`3/Zip<System.Object,System.Object,System.Object>::Dequeue()
extern "C"  void Zip_Dequeue_m797380887_gshared (Zip_t2358317490 * __this, const MethodInfo* method);
#define Zip_Dequeue_m797380887(__this, method) ((  void (*) (Zip_t2358317490 *, const MethodInfo*))Zip_Dequeue_m797380887_gshared)(__this, method)
// System.Void UniRx.Operators.ZipObservable`3/Zip<System.Object,System.Object,System.Object>::OnNext(TResult)
extern "C"  void Zip_OnNext_m3119808020_gshared (Zip_t2358317490 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Zip_OnNext_m3119808020(__this, ___value0, method) ((  void (*) (Zip_t2358317490 *, Il2CppObject *, const MethodInfo*))Zip_OnNext_m3119808020_gshared)(__this, ___value0, method)
// System.Void UniRx.Operators.ZipObservable`3/Zip<System.Object,System.Object,System.Object>::OnError(System.Exception)
extern "C"  void Zip_OnError_m2147207616_gshared (Zip_t2358317490 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method);
#define Zip_OnError_m2147207616(__this, ___error0, method) ((  void (*) (Zip_t2358317490 *, Exception_t1967233988 *, const MethodInfo*))Zip_OnError_m2147207616_gshared)(__this, ___error0, method)
// System.Void UniRx.Operators.ZipObservable`3/Zip<System.Object,System.Object,System.Object>::OnCompleted()
extern "C"  void Zip_OnCompleted_m2287672595_gshared (Zip_t2358317490 * __this, const MethodInfo* method);
#define Zip_OnCompleted_m2287672595(__this, method) ((  void (*) (Zip_t2358317490 *, const MethodInfo*))Zip_OnCompleted_m2287672595_gshared)(__this, method)
// System.Void UniRx.Operators.ZipObservable`3/Zip<System.Object,System.Object,System.Object>::<Run>m__90()
extern "C"  void Zip_U3CRunU3Em__90_m3455080400_gshared (Zip_t2358317490 * __this, const MethodInfo* method);
#define Zip_U3CRunU3Em__90_m3455080400(__this, method) ((  void (*) (Zip_t2358317490 *, const MethodInfo*))Zip_U3CRunU3Em__90_m3455080400_gshared)(__this, method)
