﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,System.Single>
struct KeyCollection_t1973835735;
// System.Collections.Generic.Dictionary`2<System.Object,System.Single>
struct Dictionary_2_t3945527751;
// System.Object
struct Il2CppObject;
// System.Collections.Generic.IEnumerator`1<System.Object>
struct IEnumerator_1_t2320212868;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t287207039;
// System.Object[]
struct ObjectU5BU5D_t11523773;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_Array2840145358.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_K3712555692.h"

// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,System.Single>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void KeyCollection__ctor_m2966486865_gshared (KeyCollection_t1973835735 * __this, Dictionary_2_t3945527751 * ___dictionary0, const MethodInfo* method);
#define KeyCollection__ctor_m2966486865(__this, ___dictionary0, method) ((  void (*) (KeyCollection_t1973835735 *, Dictionary_2_t3945527751 *, const MethodInfo*))KeyCollection__ctor_m2966486865_gshared)(__this, ___dictionary0, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,System.Single>::System.Collections.Generic.ICollection<TKey>.Add(TKey)
extern "C"  void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m3744746341_gshared (KeyCollection_t1973835735 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m3744746341(__this, ___item0, method) ((  void (*) (KeyCollection_t1973835735 *, Il2CppObject *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m3744746341_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,System.Single>::System.Collections.Generic.ICollection<TKey>.Clear()
extern "C"  void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m3127903772_gshared (KeyCollection_t1973835735 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m3127903772(__this, method) ((  void (*) (KeyCollection_t1973835735 *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m3127903772_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,System.Single>::System.Collections.Generic.ICollection<TKey>.Contains(TKey)
extern "C"  bool KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m1936554693_gshared (KeyCollection_t1973835735 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m1936554693(__this, ___item0, method) ((  bool (*) (KeyCollection_t1973835735 *, Il2CppObject *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m1936554693_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,System.Single>::System.Collections.Generic.ICollection<TKey>.Remove(TKey)
extern "C"  bool KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m1106499946_gshared (KeyCollection_t1973835735 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m1106499946(__this, ___item0, method) ((  bool (*) (KeyCollection_t1973835735 *, Il2CppObject *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m1106499946_gshared)(__this, ___item0, method)
// System.Collections.Generic.IEnumerator`1<TKey> System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,System.Single>::System.Collections.Generic.IEnumerable<TKey>.GetEnumerator()
extern "C"  Il2CppObject* KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m1601911768_gshared (KeyCollection_t1973835735 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m1601911768(__this, method) ((  Il2CppObject* (*) (KeyCollection_t1973835735 *, const MethodInfo*))KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m1601911768_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,System.Single>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void KeyCollection_System_Collections_ICollection_CopyTo_m2068684942_gshared (KeyCollection_t1973835735 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define KeyCollection_System_Collections_ICollection_CopyTo_m2068684942(__this, ___array0, ___index1, method) ((  void (*) (KeyCollection_t1973835735 *, Il2CppArray *, int32_t, const MethodInfo*))KeyCollection_System_Collections_ICollection_CopyTo_m2068684942_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,System.Single>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * KeyCollection_System_Collections_IEnumerable_GetEnumerator_m3665360777_gshared (KeyCollection_t1973835735 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_IEnumerable_GetEnumerator_m3665360777(__this, method) ((  Il2CppObject * (*) (KeyCollection_t1973835735 *, const MethodInfo*))KeyCollection_System_Collections_IEnumerable_GetEnumerator_m3665360777_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,System.Single>::System.Collections.Generic.ICollection<TKey>.get_IsReadOnly()
extern "C"  bool KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m4189279462_gshared (KeyCollection_t1973835735 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m4189279462(__this, method) ((  bool (*) (KeyCollection_t1973835735 *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m4189279462_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,System.Single>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool KeyCollection_System_Collections_ICollection_get_IsSynchronized_m4278789400_gshared (KeyCollection_t1973835735 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_ICollection_get_IsSynchronized_m4278789400(__this, method) ((  bool (*) (KeyCollection_t1973835735 *, const MethodInfo*))KeyCollection_System_Collections_ICollection_get_IsSynchronized_m4278789400_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,System.Single>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * KeyCollection_System_Collections_ICollection_get_SyncRoot_m3733356996_gshared (KeyCollection_t1973835735 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_ICollection_get_SyncRoot_m3733356996(__this, method) ((  Il2CppObject * (*) (KeyCollection_t1973835735 *, const MethodInfo*))KeyCollection_System_Collections_ICollection_get_SyncRoot_m3733356996_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,System.Single>::CopyTo(TKey[],System.Int32)
extern "C"  void KeyCollection_CopyTo_m3360156166_gshared (KeyCollection_t1973835735 * __this, ObjectU5BU5D_t11523773* ___array0, int32_t ___index1, const MethodInfo* method);
#define KeyCollection_CopyTo_m3360156166(__this, ___array0, ___index1, method) ((  void (*) (KeyCollection_t1973835735 *, ObjectU5BU5D_t11523773*, int32_t, const MethodInfo*))KeyCollection_CopyTo_m3360156166_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,System.Single>::GetEnumerator()
extern "C"  Enumerator_t3712555692  KeyCollection_GetEnumerator_m4094390505_gshared (KeyCollection_t1973835735 * __this, const MethodInfo* method);
#define KeyCollection_GetEnumerator_m4094390505(__this, method) ((  Enumerator_t3712555692  (*) (KeyCollection_t1973835735 *, const MethodInfo*))KeyCollection_GetEnumerator_m4094390505_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,System.Single>::get_Count()
extern "C"  int32_t KeyCollection_get_Count_m3992691422_gshared (KeyCollection_t1973835735 * __this, const MethodInfo* method);
#define KeyCollection_get_Count_m3992691422(__this, method) ((  int32_t (*) (KeyCollection_t1973835735 *, const MethodInfo*))KeyCollection_get_Count_m3992691422_gshared)(__this, method)
