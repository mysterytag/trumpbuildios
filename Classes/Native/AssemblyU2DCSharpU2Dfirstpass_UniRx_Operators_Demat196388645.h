﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UniRx.Operators.DematerializeObservable`1<System.Object>
struct DematerializeObservable_1_t1297640382;

#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Opera306849478.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Operators.DematerializeObservable`1/Dematerialize<System.Object>
struct  Dematerialize_t196388645  : public OperatorObserverBase_2_t306849478
{
public:
	// UniRx.Operators.DematerializeObservable`1<T> UniRx.Operators.DematerializeObservable`1/Dematerialize::parent
	DematerializeObservable_1_t1297640382 * ___parent_2;

public:
	inline static int32_t get_offset_of_parent_2() { return static_cast<int32_t>(offsetof(Dematerialize_t196388645, ___parent_2)); }
	inline DematerializeObservable_1_t1297640382 * get_parent_2() const { return ___parent_2; }
	inline DematerializeObservable_1_t1297640382 ** get_address_of_parent_2() { return &___parent_2; }
	inline void set_parent_2(DematerializeObservable_1_t1297640382 * value)
	{
		___parent_2 = value;
		Il2CppCodeGenWriteBarrier(&___parent_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
