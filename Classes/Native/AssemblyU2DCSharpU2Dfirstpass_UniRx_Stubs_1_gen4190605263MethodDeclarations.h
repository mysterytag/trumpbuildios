﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"

// System.Void UniRx.Stubs`1<System.Int64>::.cctor()
extern "C"  void Stubs_1__cctor_m1472561512_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define Stubs_1__cctor_m1472561512(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))Stubs_1__cctor_m1472561512_gshared)(__this /* static, unused */, method)
// System.Void UniRx.Stubs`1<System.Int64>::<Ignore>m__71(T)
extern "C"  void Stubs_1_U3CIgnoreU3Em__71_m2168417892_gshared (Il2CppObject * __this /* static, unused */, int64_t ___t0, const MethodInfo* method);
#define Stubs_1_U3CIgnoreU3Em__71_m2168417892(__this /* static, unused */, ___t0, method) ((  void (*) (Il2CppObject * /* static, unused */, int64_t, const MethodInfo*))Stubs_1_U3CIgnoreU3Em__71_m2168417892_gshared)(__this /* static, unused */, ___t0, method)
// T UniRx.Stubs`1<System.Int64>::<Identity>m__72(T)
extern "C"  int64_t Stubs_1_U3CIdentityU3Em__72_m107431032_gshared (Il2CppObject * __this /* static, unused */, int64_t ___t0, const MethodInfo* method);
#define Stubs_1_U3CIdentityU3Em__72_m107431032(__this /* static, unused */, ___t0, method) ((  int64_t (*) (Il2CppObject * /* static, unused */, int64_t, const MethodInfo*))Stubs_1_U3CIdentityU3Em__72_m107431032_gshared)(__this /* static, unused */, ___t0, method)
