﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GameController/<ReleaseMoneyCoroutine>c__Iterator10
struct U3CReleaseMoneyCoroutineU3Ec__Iterator10_t1779307829;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void GameController/<ReleaseMoneyCoroutine>c__Iterator10::.ctor()
extern "C"  void U3CReleaseMoneyCoroutineU3Ec__Iterator10__ctor_m4082229973 (U3CReleaseMoneyCoroutineU3Ec__Iterator10_t1779307829 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object GameController/<ReleaseMoneyCoroutine>c__Iterator10::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CReleaseMoneyCoroutineU3Ec__Iterator10_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m4224370717 (U3CReleaseMoneyCoroutineU3Ec__Iterator10_t1779307829 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object GameController/<ReleaseMoneyCoroutine>c__Iterator10::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CReleaseMoneyCoroutineU3Ec__Iterator10_System_Collections_IEnumerator_get_Current_m3611469233 (U3CReleaseMoneyCoroutineU3Ec__Iterator10_t1779307829 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GameController/<ReleaseMoneyCoroutine>c__Iterator10::MoveNext()
extern "C"  bool U3CReleaseMoneyCoroutineU3Ec__Iterator10_MoveNext_m1125871871 (U3CReleaseMoneyCoroutineU3Ec__Iterator10_t1779307829 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameController/<ReleaseMoneyCoroutine>c__Iterator10::Dispose()
extern "C"  void U3CReleaseMoneyCoroutineU3Ec__Iterator10_Dispose_m1616145874 (U3CReleaseMoneyCoroutineU3Ec__Iterator10_t1779307829 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameController/<ReleaseMoneyCoroutine>c__Iterator10::Reset()
extern "C"  void U3CReleaseMoneyCoroutineU3Ec__Iterator10_Reset_m1728662914 (U3CReleaseMoneyCoroutineU3Ec__Iterator10_t1779307829 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
