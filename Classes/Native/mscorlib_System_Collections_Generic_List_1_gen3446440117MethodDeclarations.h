﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1634065389MethodDeclarations.h"

// System.Void System.Collections.Generic.List`1<OnePF.JSON>::.ctor()
#define List_1__ctor_m3525305678(__this, method) ((  void (*) (List_1_t3446440117 *, const MethodInfo*))List_1__ctor_m348833073_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<OnePF.JSON>::.ctor(System.Collections.Generic.IEnumerable`1<T>)
#define List_1__ctor_m1494286961(__this, ___collection0, method) ((  void (*) (List_1_t3446440117 *, Il2CppObject*, const MethodInfo*))List_1__ctor_m1160795371_gshared)(__this, ___collection0, method)
// System.Void System.Collections.Generic.List`1<OnePF.JSON>::.ctor(System.Int32)
#define List_1__ctor_m2464814495(__this, ___capacity0, method) ((  void (*) (List_1_t3446440117 *, int32_t, const MethodInfo*))List_1__ctor_m2892763214_gshared)(__this, ___capacity0, method)
// System.Void System.Collections.Generic.List`1<OnePF.JSON>::.cctor()
#define List_1__cctor_m1428197407(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))List_1__cctor_m3826137881_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.List`1<OnePF.JSON>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
#define List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m4030610136(__this, method) ((  Il2CppObject* (*) (List_1_t3446440117 *, const MethodInfo*))List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m2808422246_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<OnePF.JSON>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define List_1_System_Collections_ICollection_CopyTo_m200926134(__this, ___array0, ___arrayIndex1, method) ((  void (*) (List_1_t3446440117 *, Il2CppArray *, int32_t, const MethodInfo*))List_1_System_Collections_ICollection_CopyTo_m4034025648_gshared)(__this, ___array0, ___arrayIndex1, method)
// System.Collections.IEnumerator System.Collections.Generic.List`1<OnePF.JSON>::System.Collections.IEnumerable.GetEnumerator()
#define List_1_System_Collections_IEnumerable_GetEnumerator_m2985156805(__this, method) ((  Il2CppObject * (*) (List_1_t3446440117 *, const MethodInfo*))List_1_System_Collections_IEnumerable_GetEnumerator_m1841330603_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<OnePF.JSON>::System.Collections.IList.Add(System.Object)
#define List_1_System_Collections_IList_Add_m1636445208(__this, ___item0, method) ((  int32_t (*) (List_1_t3446440117 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Add_m3794749222_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.List`1<OnePF.JSON>::System.Collections.IList.Contains(System.Object)
#define List_1_System_Collections_IList_Contains_m2984468904(__this, ___item0, method) ((  bool (*) (List_1_t3446440117 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Contains_m2659633254_gshared)(__this, ___item0, method)
// System.Int32 System.Collections.Generic.List`1<OnePF.JSON>::System.Collections.IList.IndexOf(System.Object)
#define List_1_System_Collections_IList_IndexOf_m2837878384(__this, ___item0, method) ((  int32_t (*) (List_1_t3446440117 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_IndexOf_m3431692926_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<OnePF.JSON>::System.Collections.IList.Insert(System.Int32,System.Object)
#define List_1_System_Collections_IList_Insert_m1128212195(__this, ___index0, ___item1, method) ((  void (*) (List_1_t3446440117 *, int32_t, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Insert_m2067529129_gshared)(__this, ___index0, ___item1, method)
// System.Void System.Collections.Generic.List`1<OnePF.JSON>::System.Collections.IList.Remove(System.Object)
#define List_1_System_Collections_IList_Remove_m2108689765(__this, ___item0, method) ((  void (*) (List_1_t3446440117 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Remove_m1644145887_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.List`1<OnePF.JSON>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
#define List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m4020199337(__this, method) ((  bool (*) (List_1_t3446440117 *, const MethodInfo*))List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m1299706087_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<OnePF.JSON>::System.Collections.ICollection.get_IsSynchronized()
#define List_1_System_Collections_ICollection_get_IsSynchronized_m190100660(__this, method) ((  bool (*) (List_1_t3446440117 *, const MethodInfo*))List_1_System_Collections_ICollection_get_IsSynchronized_m3867536694_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<OnePF.JSON>::System.Collections.ICollection.get_SyncRoot()
#define List_1_System_Collections_ICollection_get_SyncRoot_m2655675494(__this, method) ((  Il2CppObject * (*) (List_1_t3446440117 *, const MethodInfo*))List_1_System_Collections_ICollection_get_SyncRoot_m4244374434_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<OnePF.JSON>::System.Collections.IList.get_IsFixedSize()
#define List_1_System_Collections_IList_get_IsFixedSize_m2950655767(__this, method) ((  bool (*) (List_1_t3446440117 *, const MethodInfo*))List_1_System_Collections_IList_get_IsFixedSize_m432946261_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<OnePF.JSON>::System.Collections.IList.get_IsReadOnly()
#define List_1_System_Collections_IList_get_IsReadOnly_m1241927938(__this, method) ((  bool (*) (List_1_t3446440117 *, const MethodInfo*))List_1_System_Collections_IList_get_IsReadOnly_m2961826820_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<OnePF.JSON>::System.Collections.IList.get_Item(System.Int32)
#define List_1_System_Collections_IList_get_Item_m1959187565(__this, ___index0, method) ((  Il2CppObject * (*) (List_1_t3446440117 *, int32_t, const MethodInfo*))List_1_System_Collections_IList_get_Item_m3985478825_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<OnePF.JSON>::System.Collections.IList.set_Item(System.Int32,System.Object)
#define List_1_System_Collections_IList_set_Item_m2494113274(__this, ___index0, ___value1, method) ((  void (*) (List_1_t3446440117 *, int32_t, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_set_Item_m3234554688_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.Generic.List`1<OnePF.JSON>::Add(T)
#define List_1_Add_m3151903089(__this, ___item0, method) ((  void (*) (List_1_t3446440117 *, JSON_t2649481148 *, const MethodInfo*))List_1_Add_m642669291_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<OnePF.JSON>::GrowIfNeeded(System.Int32)
#define List_1_GrowIfNeeded_m1790919980(__this, ___newCount0, method) ((  void (*) (List_1_t3446440117 *, int32_t, const MethodInfo*))List_1_GrowIfNeeded_m4122600870_gshared)(__this, ___newCount0, method)
// System.Void System.Collections.Generic.List`1<OnePF.JSON>::AddCollection(System.Collections.Generic.ICollection`1<T>)
#define List_1_AddCollection_m2725243946(__this, ___collection0, method) ((  void (*) (List_1_t3446440117 *, Il2CppObject*, const MethodInfo*))List_1_AddCollection_m2478449828_gshared)(__this, ___collection0, method)
// System.Void System.Collections.Generic.List`1<OnePF.JSON>::AddEnumerable(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_AddEnumerable_m1986216170(__this, ___enumerable0, method) ((  void (*) (List_1_t3446440117 *, Il2CppObject*, const MethodInfo*))List_1_AddEnumerable_m1739422052_gshared)(__this, ___enumerable0, method)
// System.Void System.Collections.Generic.List`1<OnePF.JSON>::AddRange(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_AddRange_m3017753453(__this, ___collection0, method) ((  void (*) (List_1_t3446440117 *, Il2CppObject*, const MethodInfo*))List_1_AddRange_m2229151411_gshared)(__this, ___collection0, method)
// System.Collections.ObjectModel.ReadOnlyCollection`1<T> System.Collections.Generic.List`1<OnePF.JSON>::AsReadOnly()
#define List_1_AsReadOnly_m2145347960(__this, method) ((  ReadOnlyCollection_1_t1517659200 * (*) (List_1_t3446440117 *, const MethodInfo*))List_1_AsReadOnly_m769820182_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<OnePF.JSON>::Clear()
#define List_1_Clear_m931438969(__this, method) ((  void (*) (List_1_t3446440117 *, const MethodInfo*))List_1_Clear_m454602559_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<OnePF.JSON>::Contains(T)
#define List_1_Contains_m3474805355(__this, ___item0, method) ((  bool (*) (List_1_t3446440117 *, JSON_t2649481148 *, const MethodInfo*))List_1_Contains_m4186092781_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<OnePF.JSON>::CopyTo(T[],System.Int32)
#define List_1_CopyTo_m1165821729(__this, ___array0, ___arrayIndex1, method) ((  void (*) (List_1_t3446440117 *, JSONU5BU5D_t1777688021*, int32_t, const MethodInfo*))List_1_CopyTo_m3988356635_gshared)(__this, ___array0, ___arrayIndex1, method)
// T System.Collections.Generic.List`1<OnePF.JSON>::Find(System.Predicate`1<T>)
#define List_1_Find_m895661445(__this, ___match0, method) ((  JSON_t2649481148 * (*) (List_1_t3446440117 *, Predicate_1_t3220445046 *, const MethodInfo*))List_1_Find_m3379773421_gshared)(__this, ___match0, method)
// System.Void System.Collections.Generic.List`1<OnePF.JSON>::CheckMatch(System.Predicate`1<T>)
#define List_1_CheckMatch_m2230619362(__this /* static, unused */, ___match0, method) ((  void (*) (Il2CppObject * /* static, unused */, Predicate_1_t3220445046 *, const MethodInfo*))List_1_CheckMatch_m3390394152_gshared)(__this /* static, unused */, ___match0, method)
// System.Int32 System.Collections.Generic.List`1<OnePF.JSON>::GetIndex(System.Int32,System.Int32,System.Predicate`1<T>)
#define List_1_GetIndex_m4106533823(__this, ___startIndex0, ___count1, ___match2, method) ((  int32_t (*) (List_1_t3446440117 *, int32_t, int32_t, Predicate_1_t3220445046 *, const MethodInfo*))List_1_GetIndex_m4275988045_gshared)(__this, ___startIndex0, ___count1, ___match2, method)
// System.Void System.Collections.Generic.List`1<OnePF.JSON>::ForEach(System.Action`1<T>)
#define List_1_ForEach_m2762684886(__this, ___action0, method) ((  void (*) (List_1_t3446440117 *, Action_1_t2797933853 *, const MethodInfo*))List_1_ForEach_m2030348444_gshared)(__this, ___action0, method)
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<OnePF.JSON>::GetEnumerator()
#define List_1_GetEnumerator_m106063400(__this, method) ((  Enumerator_t1532223109  (*) (List_1_t3446440117 *, const MethodInfo*))List_1_GetEnumerator_m1820263692_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<OnePF.JSON>::IndexOf(T)
#define List_1_IndexOf_m1839494061(__this, ___item0, method) ((  int32_t (*) (List_1_t3446440117 *, JSON_t2649481148 *, const MethodInfo*))List_1_IndexOf_m1752303327_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<OnePF.JSON>::Shift(System.Int32,System.Int32)
#define List_1_Shift_m3769642104(__this, ___start0, ___delta1, method) ((  void (*) (List_1_t3446440117 *, int32_t, int32_t, const MethodInfo*))List_1_Shift_m3807054194_gshared)(__this, ___start0, ___delta1, method)
// System.Void System.Collections.Generic.List`1<OnePF.JSON>::CheckIndex(System.Int32)
#define List_1_CheckIndex_m912188913(__this, ___index0, method) ((  void (*) (List_1_t3446440117 *, int32_t, const MethodInfo*))List_1_CheckIndex_m3734723819_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<OnePF.JSON>::Insert(System.Int32,T)
#define List_1_Insert_m3947131352(__this, ___index0, ___item1, method) ((  void (*) (List_1_t3446440117 *, int32_t, JSON_t2649481148 *, const MethodInfo*))List_1_Insert_m3427163986_gshared)(__this, ___index0, ___item1, method)
// System.Void System.Collections.Generic.List`1<OnePF.JSON>::CheckCollection(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_CheckCollection_m3851017293(__this, ___collection0, method) ((  void (*) (List_1_t3446440117 *, Il2CppObject*, const MethodInfo*))List_1_CheckCollection_m2905071175_gshared)(__this, ___collection0, method)
// System.Boolean System.Collections.Generic.List`1<OnePF.JSON>::Remove(T)
#define List_1_Remove_m4190744870(__this, ___item0, method) ((  bool (*) (List_1_t3446440117 *, JSON_t2649481148 *, const MethodInfo*))List_1_Remove_m2747911208_gshared)(__this, ___item0, method)
// System.Int32 System.Collections.Generic.List`1<OnePF.JSON>::RemoveAll(System.Predicate`1<T>)
#define List_1_RemoveAll_m2199620528(__this, ___match0, method) ((  int32_t (*) (List_1_t3446440117 *, Predicate_1_t3220445046 *, const MethodInfo*))List_1_RemoveAll_m2933443938_gshared)(__this, ___match0, method)
// System.Void System.Collections.Generic.List`1<OnePF.JSON>::RemoveAt(System.Int32)
#define List_1_RemoveAt_m1820984222(__this, ___index0, method) ((  void (*) (List_1_t3446440117 *, int32_t, const MethodInfo*))List_1_RemoveAt_m1301016856_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<OnePF.JSON>::Reverse()
#define List_1_Reverse_m3422338574(__this, method) ((  void (*) (List_1_t3446440117 *, const MethodInfo*))List_1_Reverse_m449081940_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<OnePF.JSON>::Sort()
#define List_1_Sort_m3677875284(__this, method) ((  void (*) (List_1_t3446440117 *, const MethodInfo*))List_1_Sort_m1168641486_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<OnePF.JSON>::Sort(System.Collections.Generic.IComparer`1<T>)
#define List_1_Sort_m947669008(__this, ___comparer0, method) ((  void (*) (List_1_t3446440117 *, Il2CppObject*, const MethodInfo*))List_1_Sort_m3726677974_gshared)(__this, ___comparer0, method)
// System.Void System.Collections.Generic.List`1<OnePF.JSON>::Sort(System.Comparison`1<T>)
#define List_1_Sort_m1124811175(__this, ___comparison0, method) ((  void (*) (List_1_t3446440117 *, Comparison_1_t1058188728 *, const MethodInfo*))List_1_Sort_m4192185249_gshared)(__this, ___comparison0, method)
// T[] System.Collections.Generic.List`1<OnePF.JSON>::ToArray()
#define List_1_ToArray_m3205917031(__this, method) ((  JSONU5BU5D_t1777688021* (*) (List_1_t3446440117 *, const MethodInfo*))List_1_ToArray_m1046025517_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<OnePF.JSON>::TrimExcess()
#define List_1_TrimExcess_m3629219053(__this, method) ((  void (*) (List_1_t3446440117 *, const MethodInfo*))List_1_TrimExcess_m2451380967_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<OnePF.JSON>::get_Capacity()
#define List_1_get_Capacity_m970509661(__this, method) ((  int32_t (*) (List_1_t3446440117 *, const MethodInfo*))List_1_get_Capacity_m543520655_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<OnePF.JSON>::set_Capacity(System.Int32)
#define List_1_set_Capacity_m3296076094(__this, ___value0, method) ((  void (*) (List_1_t3446440117 *, int32_t, const MethodInfo*))List_1_set_Capacity_m1332789688_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.Generic.List`1<OnePF.JSON>::get_Count()
#define List_1_get_Count_m1007048558(__this, method) ((  int32_t (*) (List_1_t3446440117 *, const MethodInfo*))List_1_get_Count_m2599103100_gshared)(__this, method)
// T System.Collections.Generic.List`1<OnePF.JSON>::get_Item(System.Int32)
#define List_1_get_Item_m19113476(__this, ___index0, method) ((  JSON_t2649481148 * (*) (List_1_t3446440117 *, int32_t, const MethodInfo*))List_1_get_Item_m2771401372_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<OnePF.JSON>::set_Item(System.Int32,T)
#define List_1_set_Item_m2546703535(__this, ___index0, ___value1, method) ((  void (*) (List_1_t3446440117 *, int32_t, JSON_t2649481148 *, const MethodInfo*))List_1_set_Item_m1074271145_gshared)(__this, ___index0, ___value1, method)
