﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Facebook.Unity.Editor.EditorWrapper
struct EditorWrapper_t1892569344;
// Facebook.Unity.IFacebookCallbackHandler
struct IFacebookCallbackHandler_t69135093;
// Facebook.Unity.Utilities/Callback`1<Facebook.Unity.ResultContainer>
struct Callback_1_t1746511022;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String968488902.h"

// System.Void Facebook.Unity.Editor.EditorWrapper::.ctor(Facebook.Unity.IFacebookCallbackHandler)
extern "C"  void EditorWrapper__ctor_m1345629334 (EditorWrapper_t1892569344 * __this, Il2CppObject * ___callbackHandler0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Facebook.Unity.Editor.EditorWrapper::Init()
extern "C"  void EditorWrapper_Init_m3835056175 (EditorWrapper_t1892569344 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Facebook.Unity.Editor.EditorWrapper::ShowLoginMockDialog(Facebook.Unity.Utilities/Callback`1<Facebook.Unity.ResultContainer>,System.String,System.String)
extern "C"  void EditorWrapper_ShowLoginMockDialog_m3860927118 (EditorWrapper_t1892569344 * __this, Callback_1_t1746511022 * ___callback0, String_t* ___callbackId1, String_t* ___permsisions2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Facebook.Unity.Editor.EditorWrapper::ShowAppRequestMockDialog(Facebook.Unity.Utilities/Callback`1<Facebook.Unity.ResultContainer>,System.String)
extern "C"  void EditorWrapper_ShowAppRequestMockDialog_m937448215 (EditorWrapper_t1892569344 * __this, Callback_1_t1746511022 * ___callback0, String_t* ___callbackId1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Facebook.Unity.Editor.EditorWrapper::ShowGameGroupCreateMockDialog(Facebook.Unity.Utilities/Callback`1<Facebook.Unity.ResultContainer>,System.String)
extern "C"  void EditorWrapper_ShowGameGroupCreateMockDialog_m1066671730 (EditorWrapper_t1892569344 * __this, Callback_1_t1746511022 * ___callback0, String_t* ___callbackId1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Facebook.Unity.Editor.EditorWrapper::ShowGameGroupJoinMockDialog(Facebook.Unity.Utilities/Callback`1<Facebook.Unity.ResultContainer>,System.String)
extern "C"  void EditorWrapper_ShowGameGroupJoinMockDialog_m887956612 (EditorWrapper_t1892569344 * __this, Callback_1_t1746511022 * ___callback0, String_t* ___callbackId1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Facebook.Unity.Editor.EditorWrapper::ShowAppInviteMockDialog(Facebook.Unity.Utilities/Callback`1<Facebook.Unity.ResultContainer>,System.String)
extern "C"  void EditorWrapper_ShowAppInviteMockDialog_m688098001 (EditorWrapper_t1892569344 * __this, Callback_1_t1746511022 * ___callback0, String_t* ___callbackId1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Facebook.Unity.Editor.EditorWrapper::ShowPayMockDialog(Facebook.Unity.Utilities/Callback`1<Facebook.Unity.ResultContainer>,System.String)
extern "C"  void EditorWrapper_ShowPayMockDialog_m1045911475 (EditorWrapper_t1892569344 * __this, Callback_1_t1746511022 * ___callback0, String_t* ___callbackId1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Facebook.Unity.Editor.EditorWrapper::ShowMockShareDialog(Facebook.Unity.Utilities/Callback`1<Facebook.Unity.ResultContainer>,System.String,System.String)
extern "C"  void EditorWrapper_ShowMockShareDialog_m3979254892 (EditorWrapper_t1892569344 * __this, Callback_1_t1746511022 * ___callback0, String_t* ___subTitle1, String_t* ___callbackId2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Facebook.Unity.Editor.EditorWrapper::ShowEmptyMockDialog(Facebook.Unity.Utilities/Callback`1<Facebook.Unity.ResultContainer>,System.String,System.String)
extern "C"  void EditorWrapper_ShowEmptyMockDialog_m1325159978 (EditorWrapper_t1892569344 * __this, Callback_1_t1746511022 * ___callback0, String_t* ___callbackId1, String_t* ___title2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
