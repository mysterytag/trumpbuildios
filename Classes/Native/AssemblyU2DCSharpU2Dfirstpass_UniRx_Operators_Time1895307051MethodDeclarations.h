﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.TimeoutObservable`1<System.Object>
struct TimeoutObservable_1_t1895307051;
// UniRx.IObservable`1<System.Object>
struct IObservable_1_t595904784;
// UniRx.IScheduler
struct IScheduler_t2938318244;
// System.IDisposable
struct IDisposable_t1628921374;
// UniRx.IObserver`1<System.Object>
struct IObserver_1_t3049105323;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_TimeSpan763862892.h"
#include "mscorlib_System_DateTimeOffset3712260035.h"

// System.Void UniRx.Operators.TimeoutObservable`1<System.Object>::.ctor(UniRx.IObservable`1<T>,System.TimeSpan,UniRx.IScheduler)
extern "C"  void TimeoutObservable_1__ctor_m3367647542_gshared (TimeoutObservable_1_t1895307051 * __this, Il2CppObject* ___source0, TimeSpan_t763862892  ___dueTime1, Il2CppObject * ___scheduler2, const MethodInfo* method);
#define TimeoutObservable_1__ctor_m3367647542(__this, ___source0, ___dueTime1, ___scheduler2, method) ((  void (*) (TimeoutObservable_1_t1895307051 *, Il2CppObject*, TimeSpan_t763862892 , Il2CppObject *, const MethodInfo*))TimeoutObservable_1__ctor_m3367647542_gshared)(__this, ___source0, ___dueTime1, ___scheduler2, method)
// System.Void UniRx.Operators.TimeoutObservable`1<System.Object>::.ctor(UniRx.IObservable`1<T>,System.DateTimeOffset,UniRx.IScheduler)
extern "C"  void TimeoutObservable_1__ctor_m3458289869_gshared (TimeoutObservable_1_t1895307051 * __this, Il2CppObject* ___source0, DateTimeOffset_t3712260035  ___dueTime1, Il2CppObject * ___scheduler2, const MethodInfo* method);
#define TimeoutObservable_1__ctor_m3458289869(__this, ___source0, ___dueTime1, ___scheduler2, method) ((  void (*) (TimeoutObservable_1_t1895307051 *, Il2CppObject*, DateTimeOffset_t3712260035 , Il2CppObject *, const MethodInfo*))TimeoutObservable_1__ctor_m3458289869_gshared)(__this, ___source0, ___dueTime1, ___scheduler2, method)
// System.IDisposable UniRx.Operators.TimeoutObservable`1<System.Object>::SubscribeCore(UniRx.IObserver`1<T>,System.IDisposable)
extern "C"  Il2CppObject * TimeoutObservable_1_SubscribeCore_m3770527885_gshared (TimeoutObservable_1_t1895307051 * __this, Il2CppObject* ___observer0, Il2CppObject * ___cancel1, const MethodInfo* method);
#define TimeoutObservable_1_SubscribeCore_m3770527885(__this, ___observer0, ___cancel1, method) ((  Il2CppObject * (*) (TimeoutObservable_1_t1895307051 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))TimeoutObservable_1_SubscribeCore_m3770527885_gshared)(__this, ___observer0, ___cancel1, method)
