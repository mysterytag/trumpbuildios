﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Subject`1/Subscription<UniRx.CountChangedStatus>
struct Subscription_t1550474243;
// UniRx.Subject`1<UniRx.CountChangedStatus>
struct Subject_1_t1414295045;
// UniRx.IObserver`1<UniRx.CountChangedStatus>
struct IObserver_1_t1688259328;

#include "codegen/il2cpp-codegen.h"

// System.Void UniRx.Subject`1/Subscription<UniRx.CountChangedStatus>::.ctor(UniRx.Subject`1<T>,UniRx.IObserver`1<T>)
extern "C"  void Subscription__ctor_m3005987492_gshared (Subscription_t1550474243 * __this, Subject_1_t1414295045 * ___parent0, Il2CppObject* ___unsubscribeTarget1, const MethodInfo* method);
#define Subscription__ctor_m3005987492(__this, ___parent0, ___unsubscribeTarget1, method) ((  void (*) (Subscription_t1550474243 *, Subject_1_t1414295045 *, Il2CppObject*, const MethodInfo*))Subscription__ctor_m3005987492_gshared)(__this, ___parent0, ___unsubscribeTarget1, method)
// System.Void UniRx.Subject`1/Subscription<UniRx.CountChangedStatus>::Dispose()
extern "C"  void Subscription_Dispose_m2136949970_gshared (Subscription_t1550474243 * __this, const MethodInfo* method);
#define Subscription_Dispose_m2136949970(__this, method) ((  void (*) (Subscription_t1550474243 *, const MethodInfo*))Subscription_Dispose_m2136949970_gshared)(__this, method)
