﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Object
struct Il2CppObject;
// UniRx.Operators.DelayFrameObservable`1/DelayFrame<System.Object>
struct DelayFrame_t1619463347;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Operators.DelayFrameObservable`1/DelayFrame/<OnCompletedDelay>c__Iterator27<System.Object>
struct  U3COnCompletedDelayU3Ec__Iterator27_t259619244  : public Il2CppObject
{
public:
	// System.Int32 UniRx.Operators.DelayFrameObservable`1/DelayFrame/<OnCompletedDelay>c__Iterator27::<frameCount>__0
	int32_t ___U3CframeCountU3E__0_0;
	// System.Int32 UniRx.Operators.DelayFrameObservable`1/DelayFrame/<OnCompletedDelay>c__Iterator27::$PC
	int32_t ___U24PC_1;
	// System.Object UniRx.Operators.DelayFrameObservable`1/DelayFrame/<OnCompletedDelay>c__Iterator27::$current
	Il2CppObject * ___U24current_2;
	// UniRx.Operators.DelayFrameObservable`1/DelayFrame<T> UniRx.Operators.DelayFrameObservable`1/DelayFrame/<OnCompletedDelay>c__Iterator27::<>f__this
	DelayFrame_t1619463347 * ___U3CU3Ef__this_3;

public:
	inline static int32_t get_offset_of_U3CframeCountU3E__0_0() { return static_cast<int32_t>(offsetof(U3COnCompletedDelayU3Ec__Iterator27_t259619244, ___U3CframeCountU3E__0_0)); }
	inline int32_t get_U3CframeCountU3E__0_0() const { return ___U3CframeCountU3E__0_0; }
	inline int32_t* get_address_of_U3CframeCountU3E__0_0() { return &___U3CframeCountU3E__0_0; }
	inline void set_U3CframeCountU3E__0_0(int32_t value)
	{
		___U3CframeCountU3E__0_0 = value;
	}

	inline static int32_t get_offset_of_U24PC_1() { return static_cast<int32_t>(offsetof(U3COnCompletedDelayU3Ec__Iterator27_t259619244, ___U24PC_1)); }
	inline int32_t get_U24PC_1() const { return ___U24PC_1; }
	inline int32_t* get_address_of_U24PC_1() { return &___U24PC_1; }
	inline void set_U24PC_1(int32_t value)
	{
		___U24PC_1 = value;
	}

	inline static int32_t get_offset_of_U24current_2() { return static_cast<int32_t>(offsetof(U3COnCompletedDelayU3Ec__Iterator27_t259619244, ___U24current_2)); }
	inline Il2CppObject * get_U24current_2() const { return ___U24current_2; }
	inline Il2CppObject ** get_address_of_U24current_2() { return &___U24current_2; }
	inline void set_U24current_2(Il2CppObject * value)
	{
		___U24current_2 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_2, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__this_3() { return static_cast<int32_t>(offsetof(U3COnCompletedDelayU3Ec__Iterator27_t259619244, ___U3CU3Ef__this_3)); }
	inline DelayFrame_t1619463347 * get_U3CU3Ef__this_3() const { return ___U3CU3Ef__this_3; }
	inline DelayFrame_t1619463347 ** get_address_of_U3CU3Ef__this_3() { return &___U3CU3Ef__this_3; }
	inline void set_U3CU3Ef__this_3(DelayFrame_t1619463347 * value)
	{
		___U3CU3Ef__this_3 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__this_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
