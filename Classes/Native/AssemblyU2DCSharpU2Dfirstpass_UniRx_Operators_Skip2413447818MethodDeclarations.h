﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.SkipUntilObservable`2/SkipUntilOuterObserver/SkipUntilOther<System.Object,System.Object>
struct SkipUntilOther_t2413447818;
// UniRx.Operators.SkipUntilObservable`2/SkipUntilOuterObserver<System.Object,System.Object>
struct SkipUntilOuterObserver_t2866202317;
// UniRx.Operators.SkipUntilObservable`2/SkipUntilOuterObserver/SkipUntil<System.Object,System.Object>
struct SkipUntil_t2756386132;
// System.IDisposable
struct IDisposable_t1628921374;
// System.Object
struct Il2CppObject;
// System.Exception
struct Exception_t1967233988;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_Exception1967233988.h"

// System.Void UniRx.Operators.SkipUntilObservable`2/SkipUntilOuterObserver/SkipUntilOther<System.Object,System.Object>::.ctor(UniRx.Operators.SkipUntilObservable`2/SkipUntilOuterObserver<T,TOther>,UniRx.Operators.SkipUntilObservable`2/SkipUntilOuterObserver/SkipUntil<T,TOther>,System.IDisposable)
extern "C"  void SkipUntilOther__ctor_m1048082279_gshared (SkipUntilOther_t2413447818 * __this, SkipUntilOuterObserver_t2866202317 * ___parent0, SkipUntil_t2756386132 * ___sourceObserver1, Il2CppObject * ___subscription2, const MethodInfo* method);
#define SkipUntilOther__ctor_m1048082279(__this, ___parent0, ___sourceObserver1, ___subscription2, method) ((  void (*) (SkipUntilOther_t2413447818 *, SkipUntilOuterObserver_t2866202317 *, SkipUntil_t2756386132 *, Il2CppObject *, const MethodInfo*))SkipUntilOther__ctor_m1048082279_gshared)(__this, ___parent0, ___sourceObserver1, ___subscription2, method)
// System.Void UniRx.Operators.SkipUntilObservable`2/SkipUntilOuterObserver/SkipUntilOther<System.Object,System.Object>::OnNext(TOther)
extern "C"  void SkipUntilOther_OnNext_m93051097_gshared (SkipUntilOther_t2413447818 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define SkipUntilOther_OnNext_m93051097(__this, ___value0, method) ((  void (*) (SkipUntilOther_t2413447818 *, Il2CppObject *, const MethodInfo*))SkipUntilOther_OnNext_m93051097_gshared)(__this, ___value0, method)
// System.Void UniRx.Operators.SkipUntilObservable`2/SkipUntilOuterObserver/SkipUntilOther<System.Object,System.Object>::OnError(System.Exception)
extern "C"  void SkipUntilOther_OnError_m3140934552_gshared (SkipUntilOther_t2413447818 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method);
#define SkipUntilOther_OnError_m3140934552(__this, ___error0, method) ((  void (*) (SkipUntilOther_t2413447818 *, Exception_t1967233988 *, const MethodInfo*))SkipUntilOther_OnError_m3140934552_gshared)(__this, ___error0, method)
// System.Void UniRx.Operators.SkipUntilObservable`2/SkipUntilOuterObserver/SkipUntilOther<System.Object,System.Object>::OnCompleted()
extern "C"  void SkipUntilOther_OnCompleted_m2499818219_gshared (SkipUntilOther_t2413447818 * __this, const MethodInfo* method);
#define SkipUntilOther_OnCompleted_m2499818219(__this, method) ((  void (*) (SkipUntilOther_t2413447818 *, const MethodInfo*))SkipUntilOther_OnCompleted_m2499818219_gshared)(__this, method)
