﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.BufferObservable`1/Buffer_<System.Object>
struct Buffer__t1281669152;
// UniRx.Operators.BufferObservable`1<System.Object>
struct BufferObservable_1_t574294898;
// UniRx.IObserver`1<System.Collections.Generic.IList`1<System.Object>>
struct IObserver_1_t920630341;
// System.IDisposable
struct IDisposable_t1628921374;
// System.Object
struct Il2CppObject;
// System.Exception
struct Exception_t1967233988;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_Exception1967233988.h"

// System.Void UniRx.Operators.BufferObservable`1/Buffer_<System.Object>::.ctor(UniRx.Operators.BufferObservable`1<T>,UniRx.IObserver`1<System.Collections.Generic.IList`1<T>>,System.IDisposable)
extern "C"  void Buffer___ctor_m13317340_gshared (Buffer__t1281669152 * __this, BufferObservable_1_t574294898 * ___parent0, Il2CppObject* ___observer1, Il2CppObject * ___cancel2, const MethodInfo* method);
#define Buffer___ctor_m13317340(__this, ___parent0, ___observer1, ___cancel2, method) ((  void (*) (Buffer__t1281669152 *, BufferObservable_1_t574294898 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))Buffer___ctor_m13317340_gshared)(__this, ___parent0, ___observer1, ___cancel2, method)
// System.IDisposable UniRx.Operators.BufferObservable`1/Buffer_<System.Object>::Run()
extern "C"  Il2CppObject * Buffer__Run_m4082178010_gshared (Buffer__t1281669152 * __this, const MethodInfo* method);
#define Buffer__Run_m4082178010(__this, method) ((  Il2CppObject * (*) (Buffer__t1281669152 *, const MethodInfo*))Buffer__Run_m4082178010_gshared)(__this, method)
// System.Void UniRx.Operators.BufferObservable`1/Buffer_<System.Object>::OnNext(T)
extern "C"  void Buffer__OnNext_m2510311454_gshared (Buffer__t1281669152 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Buffer__OnNext_m2510311454(__this, ___value0, method) ((  void (*) (Buffer__t1281669152 *, Il2CppObject *, const MethodInfo*))Buffer__OnNext_m2510311454_gshared)(__this, ___value0, method)
// System.Void UniRx.Operators.BufferObservable`1/Buffer_<System.Object>::OnError(System.Exception)
extern "C"  void Buffer__OnError_m3391909677_gshared (Buffer__t1281669152 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method);
#define Buffer__OnError_m3391909677(__this, ___error0, method) ((  void (*) (Buffer__t1281669152 *, Exception_t1967233988 *, const MethodInfo*))Buffer__OnError_m3391909677_gshared)(__this, ___error0, method)
// System.Void UniRx.Operators.BufferObservable`1/Buffer_<System.Object>::OnCompleted()
extern "C"  void Buffer__OnCompleted_m2209267200_gshared (Buffer__t1281669152 * __this, const MethodInfo* method);
#define Buffer__OnCompleted_m2209267200(__this, method) ((  void (*) (Buffer__t1281669152 *, const MethodInfo*))Buffer__OnCompleted_m2209267200_gshared)(__this, method)
