﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.PlayFabClientAPI/GetCharacterLeaderboardResponseCallback
struct GetCharacterLeaderboardResponseCallback_t3025239216;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;
// PlayFab.ClientModels.GetCharacterLeaderboardRequest
struct GetCharacterLeaderboardRequest_t2906494549;
// PlayFab.ClientModels.GetCharacterLeaderboardResult
struct GetCharacterLeaderboardResult_t3315494327;
// PlayFab.PlayFabError
struct PlayFabError_t750598646;
// System.IAsyncResult
struct IAsyncResult_t537683269;
// System.AsyncCallback
struct AsyncCallback_t1363551830;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_IntPtr676692020.h"
#include "mscorlib_System_String968488902.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_GetCharacte2906494549.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_GetCharacte3315494327.h"
#include "AssemblyU2DCSharpU2Dfirstpass_PlayFab_PlayFabError750598646.h"
#include "mscorlib_System_AsyncCallback1363551830.h"

// System.Void PlayFab.PlayFabClientAPI/GetCharacterLeaderboardResponseCallback::.ctor(System.Object,System.IntPtr)
extern "C"  void GetCharacterLeaderboardResponseCallback__ctor_m3662324031 (GetCharacterLeaderboardResponseCallback_t3025239216 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/GetCharacterLeaderboardResponseCallback::Invoke(System.String,System.Int32,PlayFab.ClientModels.GetCharacterLeaderboardRequest,PlayFab.ClientModels.GetCharacterLeaderboardResult,PlayFab.PlayFabError,System.Object)
extern "C"  void GetCharacterLeaderboardResponseCallback_Invoke_m1591810750 (GetCharacterLeaderboardResponseCallback_t3025239216 * __this, String_t* ___urlPath0, int32_t ___callId1, GetCharacterLeaderboardRequest_t2906494549 * ___request2, GetCharacterLeaderboardResult_t3315494327 * ___result3, PlayFabError_t750598646 * ___error4, Il2CppObject * ___customData5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
extern "C" void pinvoke_delegate_wrapper_GetCharacterLeaderboardResponseCallback_t3025239216(Il2CppObject* delegate, String_t* ___urlPath0, int32_t ___callId1, GetCharacterLeaderboardRequest_t2906494549 * ___request2, GetCharacterLeaderboardResult_t3315494327 * ___result3, PlayFabError_t750598646 * ___error4, Il2CppObject * ___customData5);
// System.IAsyncResult PlayFab.PlayFabClientAPI/GetCharacterLeaderboardResponseCallback::BeginInvoke(System.String,System.Int32,PlayFab.ClientModels.GetCharacterLeaderboardRequest,PlayFab.ClientModels.GetCharacterLeaderboardResult,PlayFab.PlayFabError,System.Object,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * GetCharacterLeaderboardResponseCallback_BeginInvoke_m3503414671 (GetCharacterLeaderboardResponseCallback_t3025239216 * __this, String_t* ___urlPath0, int32_t ___callId1, GetCharacterLeaderboardRequest_t2906494549 * ___request2, GetCharacterLeaderboardResult_t3315494327 * ___result3, PlayFabError_t750598646 * ___error4, Il2CppObject * ___customData5, AsyncCallback_t1363551830 * ___callback6, Il2CppObject * ___object7, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/GetCharacterLeaderboardResponseCallback::EndInvoke(System.IAsyncResult)
extern "C"  void GetCharacterLeaderboardResponseCallback_EndInvoke_m661031631 (GetCharacterLeaderboardResponseCallback_t3025239216 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
