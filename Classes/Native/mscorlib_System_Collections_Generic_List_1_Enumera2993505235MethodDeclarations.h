﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera4014815677MethodDeclarations.h"

// System.Void System.Collections.Generic.List`1/Enumerator<PlaneInfo>::.ctor(System.Collections.Generic.List`1<T>)
#define Enumerator__ctor_m3693379441(__this, ___l0, method) ((  void (*) (Enumerator_t2993505235 *, List_1_t612754947 *, const MethodInfo*))Enumerator__ctor_m1029849669_gshared)(__this, ___l0, method)
// System.Void System.Collections.Generic.List`1/Enumerator<PlaneInfo>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m2961371713(__this, method) ((  void (*) (Enumerator_t2993505235 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m771996397_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1/Enumerator<PlaneInfo>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m2538146861(__this, method) ((  Il2CppObject * (*) (Enumerator_t2993505235 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m3561903705_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<PlaneInfo>::Dispose()
#define Enumerator_Dispose_m2104441238(__this, method) ((  void (*) (Enumerator_t2993505235 *, const MethodInfo*))Enumerator_Dispose_m2904289642_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<PlaneInfo>::VerifyState()
#define Enumerator_VerifyState_m3265281487(__this, method) ((  void (*) (Enumerator_t2993505235 *, const MethodInfo*))Enumerator_VerifyState_m1522854819_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<PlaneInfo>::MoveNext()
#define Enumerator_MoveNext_m1531225060(__this, method) ((  bool (*) (Enumerator_t2993505235 *, const MethodInfo*))Enumerator_MoveNext_m302222300_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<PlaneInfo>::get_Current()
#define Enumerator_get_Current_m879730860(__this, method) ((  PlaneInfo_t4110763274 * (*) (Enumerator_t2993505235 *, const MethodInfo*))Enumerator_get_Current_m1113061044_gshared)(__this, method)
