﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.ComponentModel.ListSortDescriptionCollection
struct ListSortDescriptionCollection_t718444615;
// System.ComponentModel.ListSortDescription[]
struct ListSortDescriptionU5BU5D_t301780532;
// System.Object
struct Il2CppObject;
// System.Collections.IEnumerator
struct IEnumerator_t287207039;
// System.ComponentModel.ListSortDescription
struct ListSortDescription_t3734381961;
// System.Array
struct Il2CppArray;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "System_System_ComponentModel_ListSortDescription3734381961.h"
#include "mscorlib_System_Array2840145358.h"

// System.Void System.ComponentModel.ListSortDescriptionCollection::.ctor()
extern "C"  void ListSortDescriptionCollection__ctor_m2315647855 (ListSortDescriptionCollection_t718444615 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.ComponentModel.ListSortDescriptionCollection::.ctor(System.ComponentModel.ListSortDescription[])
extern "C"  void ListSortDescriptionCollection__ctor_m3115284132 (ListSortDescriptionCollection_t718444615 * __this, ListSortDescriptionU5BU5D_t301780532* ___sorts0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.ComponentModel.ListSortDescriptionCollection::System.Collections.IList.get_Item(System.Int32)
extern "C"  Il2CppObject * ListSortDescriptionCollection_System_Collections_IList_get_Item_m2820168782 (ListSortDescriptionCollection_t718444615 * __this, int32_t ___index0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.ComponentModel.ListSortDescriptionCollection::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C"  void ListSortDescriptionCollection_System_Collections_IList_set_Item_m927054171 (ListSortDescriptionCollection_t718444615 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.ComponentModel.ListSortDescriptionCollection::System.Collections.IList.get_IsFixedSize()
extern "C"  bool ListSortDescriptionCollection_System_Collections_IList_get_IsFixedSize_m1722079938 (ListSortDescriptionCollection_t718444615 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.ComponentModel.ListSortDescriptionCollection::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool ListSortDescriptionCollection_System_Collections_ICollection_get_IsSynchronized_m25573737 (ListSortDescriptionCollection_t718444615 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.ComponentModel.ListSortDescriptionCollection::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * ListSortDescriptionCollection_System_Collections_ICollection_get_SyncRoot_m1248751751 (ListSortDescriptionCollection_t718444615 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.ComponentModel.ListSortDescriptionCollection::System.Collections.IList.get_IsReadOnly()
extern "C"  bool ListSortDescriptionCollection_System_Collections_IList_get_IsReadOnly_m509559799 (ListSortDescriptionCollection_t718444615 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator System.ComponentModel.ListSortDescriptionCollection::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * ListSortDescriptionCollection_System_Collections_IEnumerable_GetEnumerator_m3062968816 (ListSortDescriptionCollection_t718444615 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.ComponentModel.ListSortDescriptionCollection::System.Collections.IList.Add(System.Object)
extern "C"  int32_t ListSortDescriptionCollection_System_Collections_IList_Add_m1511197901 (ListSortDescriptionCollection_t718444615 * __this, Il2CppObject * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.ComponentModel.ListSortDescriptionCollection::System.Collections.IList.Clear()
extern "C"  void ListSortDescriptionCollection_System_Collections_IList_Clear_m1840622531 (ListSortDescriptionCollection_t718444615 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.ComponentModel.ListSortDescriptionCollection::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C"  void ListSortDescriptionCollection_System_Collections_IList_Insert_m871833220 (ListSortDescriptionCollection_t718444615 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.ComponentModel.ListSortDescriptionCollection::System.Collections.IList.Remove(System.Object)
extern "C"  void ListSortDescriptionCollection_System_Collections_IList_Remove_m479806500 (ListSortDescriptionCollection_t718444615 * __this, Il2CppObject * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.ComponentModel.ListSortDescriptionCollection::System.Collections.IList.RemoveAt(System.Int32)
extern "C"  void ListSortDescriptionCollection_System_Collections_IList_RemoveAt_m3574455316 (ListSortDescriptionCollection_t718444615 * __this, int32_t ___index0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.ComponentModel.ListSortDescriptionCollection::get_Count()
extern "C"  int32_t ListSortDescriptionCollection_get_Count_m3688320035 (ListSortDescriptionCollection_t718444615 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.ComponentModel.ListSortDescription System.ComponentModel.ListSortDescriptionCollection::get_Item(System.Int32)
extern "C"  ListSortDescription_t3734381961 * ListSortDescriptionCollection_get_Item_m711223276 (ListSortDescriptionCollection_t718444615 * __this, int32_t ___index0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.ComponentModel.ListSortDescriptionCollection::set_Item(System.Int32,System.ComponentModel.ListSortDescription)
extern "C"  void ListSortDescriptionCollection_set_Item_m1227571941 (ListSortDescriptionCollection_t718444615 * __this, int32_t ___index0, ListSortDescription_t3734381961 * ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.ComponentModel.ListSortDescriptionCollection::Contains(System.Object)
extern "C"  bool ListSortDescriptionCollection_Contains_m2035030358 (ListSortDescriptionCollection_t718444615 * __this, Il2CppObject * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.ComponentModel.ListSortDescriptionCollection::CopyTo(System.Array,System.Int32)
extern "C"  void ListSortDescriptionCollection_CopyTo_m2225366924 (ListSortDescriptionCollection_t718444615 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.ComponentModel.ListSortDescriptionCollection::IndexOf(System.Object)
extern "C"  int32_t ListSortDescriptionCollection_IndexOf_m48605582 (ListSortDescriptionCollection_t718444615 * __this, Il2CppObject * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
