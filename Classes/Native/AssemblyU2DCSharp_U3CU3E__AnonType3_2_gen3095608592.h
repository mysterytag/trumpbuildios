﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <>__AnonType3`2<System.Boolean,System.Int32>
struct  U3CU3E__AnonType3_2_t3095608592  : public Il2CppObject
{
public:
	// <b>__T <>__AnonType3`2::<b>
	bool ___U3CbU3E_0;
	// <i>__T <>__AnonType3`2::<i>
	int32_t ___U3CiU3E_1;

public:
	inline static int32_t get_offset_of_U3CbU3E_0() { return static_cast<int32_t>(offsetof(U3CU3E__AnonType3_2_t3095608592, ___U3CbU3E_0)); }
	inline bool get_U3CbU3E_0() const { return ___U3CbU3E_0; }
	inline bool* get_address_of_U3CbU3E_0() { return &___U3CbU3E_0; }
	inline void set_U3CbU3E_0(bool value)
	{
		___U3CbU3E_0 = value;
	}

	inline static int32_t get_offset_of_U3CiU3E_1() { return static_cast<int32_t>(offsetof(U3CU3E__AnonType3_2_t3095608592, ___U3CiU3E_1)); }
	inline int32_t get_U3CiU3E_1() const { return ___U3CiU3E_1; }
	inline int32_t* get_address_of_U3CiU3E_1() { return &___U3CiU3E_1; }
	inline void set_U3CiU3E_1(int32_t value)
	{
		___U3CiU3E_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
