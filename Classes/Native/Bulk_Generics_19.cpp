﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

// UniRx.BehaviorSubject`1<System.Object>
struct BehaviorSubject_1_t2662307150;
// System.Object
struct Il2CppObject;
// System.Exception
struct Exception_t1967233988;
// System.IDisposable
struct IDisposable_t1628921374;
// UniRx.IObserver`1<System.Object>
struct IObserver_1_t3049105323;
// System.String
struct String_t;
// UniRx.DictionaryDisposable`2<System.Object,System.Object>
struct DictionaryDisposable_2_t2367950693;
// System.Collections.Generic.IEqualityComparer`1<System.Object>
struct IEqualityComparer_1_t3161373071;
// System.Collections.Generic.ICollection`1<System.Object>
struct ICollection_1_t1302937806;
// System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>[]
struct KeyValuePair_2U5BU5D_t346249057;
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>
struct IEnumerator_1_t501095600;
// System.Collections.IEnumerator
struct IEnumerator_t287207039;
// System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,System.Object>
struct KeyCollection_t1852733134;
// System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,System.Object>
struct ValueCollection_t1451594948;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t2995724695;
// UniRx.DisposableExtensions/<AddTo>c__AnonStorey36`1<System.Object>
struct U3CAddToU3Ec__AnonStorey36_1_t595347342;
// UniRx.EventPattern`1<System.Object>
struct EventPattern_1_t2903003978;
// UniRx.EventPattern`2<System.Object,System.Object>
struct EventPattern_2_t3939142245;
// UniRx.InternalUtil.DisposedObserver`1<System.Boolean>
struct DisposedObserver_1_t3680430099;
// UniRx.InternalUtil.DisposedObserver`1<System.Byte>
struct DisposedObserver_1_t1953151283;
// UniRx.InternalUtil.DisposedObserver`1<System.Double>
struct DisposedObserver_1_t4003941372;
// UniRx.InternalUtil.DisposedObserver`1<System.Int32>
struct DisposedObserver_1_t2021872249;
// UniRx.InternalUtil.DisposedObserver`1<System.Int64>
struct DisposedObserver_1_t2021872344;
// UniRx.InternalUtil.DisposedObserver`1<System.Object>
struct DisposedObserver_1_t11563882;
// UniRx.InternalUtil.DisposedObserver`1<System.Single>
struct DisposedObserver_1_t132666483;
// UniRx.InternalUtil.DisposedObserver`1<TableButtons/StatkaPoTableViev>
struct DisposedObserver_1_t2682853279;
// UniRx.InternalUtil.DisposedObserver`1<UniRx.CollectionAddEvent`1<System.Object>>
struct DisposedObserver_1_t1590979449;
// UniRx.InternalUtil.DisposedObserver`1<UniRx.CollectionAddEvent`1<UniRx.Tuple`2<System.Object,System.Object>>>
struct DisposedObserver_1_t1123134848;
// UniRx.InternalUtil.DisposedObserver`1<UniRx.CollectionMoveEvent`1<System.Object>>
struct DisposedObserver_1_t2090891309;
// UniRx.InternalUtil.DisposedObserver`1<UniRx.CollectionMoveEvent`1<UniRx.Tuple`2<System.Object,System.Object>>>
struct DisposedObserver_1_t1623046708;
// UniRx.InternalUtil.DisposedObserver`1<UniRx.CollectionRemoveEvent`1<System.Object>>
struct DisposedObserver_1_t3712062482;
// UniRx.InternalUtil.DisposedObserver`1<UniRx.CollectionRemoveEvent`1<UniRx.Tuple`2<System.Object,System.Object>>>
struct DisposedObserver_1_t3244217881;
// UniRx.InternalUtil.DisposedObserver`1<UniRx.CollectionReplaceEvent`1<System.Object>>
struct DisposedObserver_1_t1016207998;
// UniRx.InternalUtil.DisposedObserver`1<UniRx.CollectionReplaceEvent`1<UniRx.Tuple`2<System.Object,System.Object>>>
struct DisposedObserver_1_t548363397;
// UniRx.InternalUtil.DisposedObserver`1<UniRx.CountChangedStatus>
struct DisposedObserver_1_t2945685183;
// UniRx.InternalUtil.DisposedObserver`1<UniRx.DictionaryAddEvent`2<System.Object,System.Object>>
struct DisposedObserver_1_t3720405766;
// UniRx.InternalUtil.DisposedObserver`1<UniRx.DictionaryRemoveEvent`2<System.Object,System.Object>>
struct DisposedObserver_1_t2496745753;
// UniRx.InternalUtil.DisposedObserver`1<UniRx.DictionaryReplaceEvent`2<System.Object,System.Object>>
struct DisposedObserver_1_t2953384525;
// UniRx.InternalUtil.DisposedObserver`1<UniRx.Tuple`2<System.Object,System.Int32>>
struct DisposedObserver_1_t1554027648;
// UniRx.InternalUtil.DisposedObserver`1<UniRx.Tuple`2<System.Object,System.Object>>
struct DisposedObserver_1_t3838686577;
// UniRx.InternalUtil.DisposedObserver`1<UniRx.Tuple`2<System.Object,UnityEngine.NetworkMessageInfo>>
struct DisposedObserver_1_t1280957745;
// UniRx.InternalUtil.DisposedObserver`1<UniRx.Unit>
struct DisposedObserver_1_t1732743500;
// UniRx.InternalUtil.DisposedObserver`1<UnityEngine.Bounds>
struct DisposedObserver_1_t2692972440;
// UniRx.InternalUtil.DisposedObserver`1<UnityEngine.Color>
struct DisposedObserver_1_t762633222;
// UniRx.InternalUtil.DisposedObserver`1<UnityEngine.MasterServerEvent>
struct DisposedObserver_1_t1452264952;
// UniRx.InternalUtil.DisposedObserver`1<UnityEngine.NetworkConnectionError>
struct DisposedObserver_1_t198263455;
// UniRx.InternalUtil.DisposedObserver`1<UnityEngine.NetworkDisconnection>
struct DisposedObserver_1_t3808185153;
// UniRx.InternalUtil.DisposedObserver`1<UnityEngine.NetworkMessageInfo>
struct DisposedObserver_1_t1748802346;
// UniRx.InternalUtil.DisposedObserver`1<UnityEngine.NetworkPlayer>
struct DisposedObserver_1_t455594834;
// UniRx.InternalUtil.DisposedObserver`1<UnityEngine.Quaternion>
struct DisposedObserver_1_t1066173441;
// UniRx.InternalUtil.DisposedObserver`1<UnityEngine.Rect>
struct DisposedObserver_1_t699886279;
// UniRx.InternalUtil.DisposedObserver`1<UnityEngine.Vector2>
struct DisposedObserver_1_t2699787250;
// UniRx.InternalUtil.DisposedObserver`1<UnityEngine.Vector3>
struct DisposedObserver_1_t2699787251;
// UniRx.InternalUtil.DisposedObserver`1<UnityEngine.Vector4>
struct DisposedObserver_1_t2699787252;
// UniRx.InternalUtil.EmptyObserver`1<System.Boolean>
struct EmptyObserver_1_t3915325627;
// UniRx.InternalUtil.EmptyObserver`1<System.Byte>
struct EmptyObserver_1_t2188046811;
// UniRx.InternalUtil.EmptyObserver`1<System.Double>
struct EmptyObserver_1_t4238836900;
// UniRx.InternalUtil.EmptyObserver`1<System.Int32>
struct EmptyObserver_1_t2256767777;
// UniRx.InternalUtil.EmptyObserver`1<System.Int64>
struct EmptyObserver_1_t2256767872;
// UniRx.InternalUtil.EmptyObserver`1<System.Object>
struct EmptyObserver_1_t246459410;
// UniRx.InternalUtil.EmptyObserver`1<System.Single>
struct EmptyObserver_1_t367562011;
// UniRx.InternalUtil.EmptyObserver`1<TableButtons/StatkaPoTableViev>
struct EmptyObserver_1_t2917748807;
// UniRx.InternalUtil.EmptyObserver`1<UniRx.CollectionAddEvent`1<System.Object>>
struct EmptyObserver_1_t1825874977;
// UniRx.InternalUtil.EmptyObserver`1<UniRx.CollectionAddEvent`1<UniRx.Tuple`2<System.Object,System.Object>>>
struct EmptyObserver_1_t1358030376;
// UniRx.InternalUtil.EmptyObserver`1<UniRx.CollectionMoveEvent`1<System.Object>>
struct EmptyObserver_1_t2325786837;
// UniRx.InternalUtil.EmptyObserver`1<UniRx.CollectionMoveEvent`1<UniRx.Tuple`2<System.Object,System.Object>>>
struct EmptyObserver_1_t1857942236;
// UniRx.InternalUtil.EmptyObserver`1<UniRx.CollectionRemoveEvent`1<System.Object>>
struct EmptyObserver_1_t3946958010;
// UniRx.InternalUtil.EmptyObserver`1<UniRx.CollectionRemoveEvent`1<UniRx.Tuple`2<System.Object,System.Object>>>
struct EmptyObserver_1_t3479113409;
// UniRx.InternalUtil.EmptyObserver`1<UniRx.CollectionReplaceEvent`1<System.Object>>
struct EmptyObserver_1_t1251103526;
// UniRx.InternalUtil.EmptyObserver`1<UniRx.CollectionReplaceEvent`1<UniRx.Tuple`2<System.Object,System.Object>>>
struct EmptyObserver_1_t783258925;
// UniRx.InternalUtil.EmptyObserver`1<UniRx.CountChangedStatus>
struct EmptyObserver_1_t3180580711;
// UniRx.InternalUtil.EmptyObserver`1<UniRx.DictionaryAddEvent`2<System.Object,System.Object>>
struct EmptyObserver_1_t3955301294;
// UniRx.InternalUtil.EmptyObserver`1<UniRx.DictionaryRemoveEvent`2<System.Object,System.Object>>
struct EmptyObserver_1_t2731641281;
// UniRx.InternalUtil.EmptyObserver`1<UniRx.DictionaryReplaceEvent`2<System.Object,System.Object>>
struct EmptyObserver_1_t3188280053;
// UniRx.InternalUtil.EmptyObserver`1<UniRx.Pair`1<System.Object>>
struct EmptyObserver_1_t3240450572;
// UniRx.InternalUtil.EmptyObserver`1<UniRx.TimeInterval`1<System.Object>>
struct EmptyObserver_1_t117418532;
// UniRx.InternalUtil.EmptyObserver`1<UniRx.Timestamped`1<System.Object>>
struct EmptyObserver_1_t1928537263;
// UniRx.InternalUtil.EmptyObserver`1<UniRx.Tuple`2<System.Object,System.Int32>>
struct EmptyObserver_1_t1788923176;
// UniRx.InternalUtil.EmptyObserver`1<UniRx.Tuple`2<System.Object,System.Object>>
struct EmptyObserver_1_t4073582105;
// UniRx.InternalUtil.EmptyObserver`1<UniRx.Tuple`2<System.Object,UnityEngine.NetworkMessageInfo>>
struct EmptyObserver_1_t1515853273;
// UniRx.InternalUtil.EmptyObserver`1<UniRx.Unit>
struct EmptyObserver_1_t1967639028;
// UniRx.InternalUtil.EmptyObserver`1<UnityEngine.Bounds>
struct EmptyObserver_1_t2927867968;
// UniRx.InternalUtil.EmptyObserver`1<UnityEngine.Color>
struct EmptyObserver_1_t997528750;
// UniRx.InternalUtil.EmptyObserver`1<UnityEngine.MasterServerEvent>
struct EmptyObserver_1_t1687160480;
// UniRx.InternalUtil.EmptyObserver`1<UnityEngine.NetworkConnectionError>
struct EmptyObserver_1_t433158983;
// UniRx.InternalUtil.EmptyObserver`1<UnityEngine.NetworkDisconnection>
struct EmptyObserver_1_t4043080681;
// UniRx.InternalUtil.EmptyObserver`1<UnityEngine.NetworkMessageInfo>
struct EmptyObserver_1_t1983697874;
// UniRx.InternalUtil.EmptyObserver`1<UnityEngine.NetworkPlayer>
struct EmptyObserver_1_t690490362;
// UniRx.InternalUtil.EmptyObserver`1<UnityEngine.Quaternion>
struct EmptyObserver_1_t1301068969;
// UniRx.InternalUtil.EmptyObserver`1<UnityEngine.Rect>
struct EmptyObserver_1_t934781807;
// UniRx.InternalUtil.EmptyObserver`1<UnityEngine.Vector2>
struct EmptyObserver_1_t2934682778;
// UniRx.InternalUtil.EmptyObserver`1<UnityEngine.Vector3>
struct EmptyObserver_1_t2934682779;
// UniRx.InternalUtil.EmptyObserver`1<UnityEngine.Vector4>
struct EmptyObserver_1_t2934682780;
// UniRx.InternalUtil.ImmutableList`1<System.Object>
struct ImmutableList_1_t1897310919;
// System.Object[]
struct ObjectU5BU5D_t11523773;
// UniRx.InternalUtil.ListObserver`1<System.Boolean>
struct ListObserver_1_t505623012;
// UniRx.InternalUtil.ImmutableList`1<UniRx.IObserver`1<System.Boolean>>
struct ImmutableList_1_t3483208743;
// UniRx.IObserver`1<System.Boolean>
struct IObserver_1_t2423004244;
// UniRx.IObserver`1<System.Boolean>[]
struct IObserver_1U5BU5D_t3497092061;
// UniRx.InternalUtil.ListObserver`1<System.Byte>
struct ListObserver_1_t3073311492;
// UniRx.InternalUtil.ImmutableList`1<UniRx.IObserver`1<System.Byte>>
struct ImmutableList_1_t1755929927;
// UniRx.IObserver`1<System.Byte>
struct IObserver_1_t695725428;
// UniRx.IObserver`1<System.Byte>[]
struct IObserver_1U5BU5D_t4045638205;
// UniRx.InternalUtil.ListObserver`1<System.Double>
struct ListObserver_1_t829134285;
// UniRx.InternalUtil.ImmutableList`1<UniRx.IObserver`1<System.Double>>
struct ImmutableList_1_t3806720016;
// UniRx.IObserver`1<System.Double>
struct IObserver_1_t2746515517;
// UniRx.IObserver`1<System.Double>[]
struct IObserver_1U5BU5D_t740445744;
// UniRx.InternalUtil.ListObserver`1<System.Int32>
struct ListObserver_1_t3142032458;
// UniRx.InternalUtil.ImmutableList`1<UniRx.IObserver`1<System.Int32>>
struct ImmutableList_1_t1824650893;
// UniRx.IObserver`1<System.Int32>
struct IObserver_1_t764446394;
// UniRx.IObserver`1<System.Int32>[]
struct IObserver_1U5BU5D_t1502147871;
// UniRx.InternalUtil.ListObserver`1<System.Int64>
struct ListObserver_1_t3142032553;
// UniRx.InternalUtil.ImmutableList`1<UniRx.IObserver`1<System.Int64>>
struct ImmutableList_1_t1824650988;
// UniRx.IObserver`1<System.Int64>
struct IObserver_1_t764446489;
// UniRx.IObserver`1<System.Int64>[]
struct IObserver_1U5BU5D_t445342820;

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array2840145358.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_BehaviorSubjec2662307150.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_BehaviorSubjec2662307150MethodDeclarations.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_Void2779279689.h"
#include "mscorlib_System_Object837106420MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_Em246459410.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_Em246459410MethodDeclarations.h"
#include "mscorlib_System_Exception1967233988.h"
#include "mscorlib_System_Boolean211005341.h"
#include "mscorlib_System_Threading_Monitor2071304733MethodDeclarations.h"
#include "mscorlib_System_ArgumentNullException3214793280MethodDeclarations.h"
#include "mscorlib_System_String968488902.h"
#include "mscorlib_System_ArgumentNullException3214793280.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_BehaviorSubjec2911320238.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_L1131724091.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_L1131724091MethodDeclarations.h"
#include "Assembly-CSharp-firstpass_ArrayTypes.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_I4109309822.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_I4109309822MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_BehaviorSubjec2911320238MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Disposable3736388786.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Disposable3736388786MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_Dis11563882.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_Dis11563882MethodDeclarations.h"
#include "mscorlib_System_ObjectDisposedException973246880MethodDeclarations.h"
#include "mscorlib_System_String968488902MethodDeclarations.h"
#include "mscorlib_System_ObjectDisposedException973246880.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_CollectionAddE2416521987.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_CollectionAddE2416521987MethodDeclarations.h"
#include "mscorlib_System_Int322847414787.h"
#include "mscorlib_System_Int322847414787MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_EqualityCompare271497070MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_EqualityCompare271497070.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_CollectionAddE1948677386.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_CollectionAddE1948677386MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Tuple_2_gen369261819.h"
#include "mscorlib_System_Collections_Generic_EqualityCompar4098619765MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_EqualityCompar4098619765.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_CollectionMove2916433847.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_CollectionMove2916433847MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_CollectionMove2448589246.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_CollectionMove2448589246MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_CollectionRemov242637724.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_CollectionRemov242637724MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_CollectionRemo4069760419.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_CollectionRemo4069760419MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_CollectionRepl1841750536.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_CollectionRepl1841750536MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_CollectionRepl1373905935.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_CollectionRepl1373905935MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_DictionaryAddEv250981008.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_DictionaryAddEv250981008MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_DictionaryDisp2367950693.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_DictionaryDisp2367950693MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g3824425150.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g3824425150MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_K1852733134.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1634065389.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1634065389MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_V1451594948.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_23312956448.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_23312956448MethodDeclarations.h"
#include "mscorlib_ArrayTypes.h"
#include "mscorlib_System_Collections_Generic_List_1_gen4109915417.h"
#include "mscorlib_System_Collections_Generic_List_1_gen4109915417MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera2195698409.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E3591453091.h"
#include "mscorlib_System_NotSupportedException1374155497MethodDeclarations.h"
#include "mscorlib_System_NotSupportedException1374155497.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E3591453091MethodDeclarations.h"
#include "mscorlib_System_Runtime_Serialization_Serializatio2995724695.h"
#include "mscorlib_System_Runtime_Serialization_StreamingCont986364934.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_DictionaryRemo3322288291.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_DictionaryRemo3322288291MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_DictionaryRepl3778927063.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_DictionaryRepl3778927063MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_DisposableExten595347342.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_DisposableExten595347342MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Unit2558286038.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_EventPattern_12903003978.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_EventPattern_12903003978MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_EventPattern_23939142245MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_EventPattern_23939142245.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_D3680430099.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_D3680430099MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_D1953151283.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_D1953151283MethodDeclarations.h"
#include "mscorlib_System_Byte2778693821.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_D4003941372.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_D4003941372MethodDeclarations.h"
#include "mscorlib_System_Double534516614.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_D2021872249.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_D2021872249MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_D2021872344.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_D2021872344MethodDeclarations.h"
#include "mscorlib_System_Int642847414882.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_Di132666483.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_Di132666483MethodDeclarations.h"
#include "mscorlib_System_Single958209021.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_D2682853279.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_D2682853279MethodDeclarations.h"
#include "AssemblyU2DCSharp_TableButtons_StatkaPoTableViev3508395817.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_D1590979449.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_D1590979449MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_D1123134848.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_D1123134848MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_D2090891309.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_D2090891309MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_D1623046708.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_D1623046708MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_D3712062482.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_D3712062482MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_D3244217881.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_D3244217881MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_D1016207998.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_D1016207998MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_Di548363397.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_Di548363397MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_D2945685183.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_D2945685183MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_CountChangedSt3771227721.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_D3720405766.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_D3720405766MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_D2496745753.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_D2496745753MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_D2953384525.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_D2953384525MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_D1554027648.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_D1554027648MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Tuple_2_gen2379570186.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_D3838686577.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_D3838686577MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_D1280957745.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_D1280957745MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Tuple_2_gen2106500283.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_D1732743500.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_D1732743500MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_D2692972440.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_D2692972440MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Bounds3518514978.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_Di762633222.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_Di762633222MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Color1588175760.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_D1452264952.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_D1452264952MethodDeclarations.h"
#include "UnityEngine_UnityEngine_MasterServerEvent2277807490.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_Di198263455.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_Di198263455MethodDeclarations.h"
#include "UnityEngine_UnityEngine_NetworkConnectionError1023805993.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_D3808185153.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_D3808185153MethodDeclarations.h"
#include "UnityEngine_UnityEngine_NetworkDisconnection338760395.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_D1748802346.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_D1748802346MethodDeclarations.h"
#include "UnityEngine_UnityEngine_NetworkMessageInfo2574344884.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_Di455594834.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_Di455594834MethodDeclarations.h"
#include "UnityEngine_UnityEngine_NetworkPlayer1281137372.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_D1066173441.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_D1066173441MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Quaternion1891715979.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_Di699886279.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_Di699886279MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Rect1525428817.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_D2699787250.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_D2699787250MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Vector23525329788.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_D2699787251.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_D2699787251MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Vector33525329789.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_D2699787252.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_D2699787252MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Vector43525329790.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_E3915325627.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_E3915325627MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_E2188046811.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_E2188046811MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_E4238836900.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_E4238836900MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_E2256767777.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_E2256767777MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_E2256767872.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_E2256767872MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_Em367562011.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_Em367562011MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_E2917748807.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_E2917748807MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_E1825874977.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_E1825874977MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_E1358030376.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_E1358030376MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_E2325786837.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_E2325786837MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_E1857942236.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_E1857942236MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_E3946958010.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_E3946958010MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_E3479113409.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_E3479113409MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_E1251103526.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_E1251103526MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_Em783258925.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_Em783258925MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_E3180580711.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_E3180580711MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_E3955301294.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_E3955301294MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_E2731641281.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_E2731641281MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_E3188280053.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_E3188280053MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_E3240450572.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_E3240450572MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Pair_1_gen3831097582.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_Em117418532.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_Em117418532MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_TimeInterval_1_708065542.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_E1928537263.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_E1928537263MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Timestamped_1_2519184273.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_E1788923176.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_E1788923176MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_E4073582105.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_E4073582105MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_E1515853273.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_E1515853273MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_E1967639028.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_E1967639028MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_E2927867968.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_E2927867968MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_Em997528750.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_Em997528750MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_E1687160480.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_E1687160480MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_Em433158983.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_Em433158983MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_E4043080681.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_E4043080681MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_E1983697874.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_E1983697874MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_Em690490362.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_Em690490362MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_E1301068969.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_E1301068969MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_Em934781807.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_Em934781807MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_E2934682778.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_E2934682778MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_E2934682779.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_E2934682779MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_E2934682780.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_E2934682780MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_I1897310919.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_I1897310919MethodDeclarations.h"
#include "mscorlib_System_Array2840145358MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_Li505623012.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_Li505623012MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_I3483208743.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_I3483208743MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_L3073311492.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_L3073311492MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_I1755929927.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_I1755929927MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_Li829134285.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_Li829134285MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_I3806720016.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_I3806720016MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_L3142032458.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_L3142032458MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_I1824650893.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_I1824650893MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_L3142032553.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_L3142032553MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_I1824650988.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_I1824650988MethodDeclarations.h"

// System.Int32 System.Array::IndexOf<System.Object>(!!0[],!!0)
extern "C"  int32_t Array_IndexOf_TisIl2CppObject_m2661005505_gshared (Il2CppObject * __this /* static, unused */, ObjectU5BU5D_t11523773* p0, Il2CppObject * p1, const MethodInfo* method);
#define Array_IndexOf_TisIl2CppObject_m2661005505(__this /* static, unused */, p0, p1, method) ((  int32_t (*) (Il2CppObject * /* static, unused */, ObjectU5BU5D_t11523773*, Il2CppObject *, const MethodInfo*))Array_IndexOf_TisIl2CppObject_m2661005505_gshared)(__this /* static, unused */, p0, p1, method)
// System.Int32 System.Array::IndexOf<UniRx.IObserver`1<System.Boolean>>(!!0[],!!0)
#define Array_IndexOf_TisIObserver_1_t2423004244_m3320621303(__this /* static, unused */, p0, p1, method) ((  int32_t (*) (Il2CppObject * /* static, unused */, IObserver_1U5BU5D_t3497092061*, Il2CppObject*, const MethodInfo*))Array_IndexOf_TisIl2CppObject_m2661005505_gshared)(__this /* static, unused */, p0, p1, method)
// System.Int32 System.Array::IndexOf<UniRx.IObserver`1<System.Byte>>(!!0[],!!0)
#define Array_IndexOf_TisIObserver_1_t695725428_m543936335(__this /* static, unused */, p0, p1, method) ((  int32_t (*) (Il2CppObject * /* static, unused */, IObserver_1U5BU5D_t4045638205*, Il2CppObject*, const MethodInfo*))Array_IndexOf_TisIl2CppObject_m2661005505_gshared)(__this /* static, unused */, p0, p1, method)
// System.Int32 System.Array::IndexOf<UniRx.IObserver`1<System.Double>>(!!0[],!!0)
#define Array_IndexOf_TisIObserver_1_t2746515517_m1073576998(__this /* static, unused */, p0, p1, method) ((  int32_t (*) (Il2CppObject * /* static, unused */, IObserver_1U5BU5D_t740445744*, Il2CppObject*, const MethodInfo*))Array_IndexOf_TisIl2CppObject_m2661005505_gshared)(__this /* static, unused */, p0, p1, method)
// System.Int32 System.Array::IndexOf<UniRx.IObserver`1<System.Int32>>(!!0[],!!0)
#define Array_IndexOf_TisIObserver_1_t764446394_m899310929(__this /* static, unused */, p0, p1, method) ((  int32_t (*) (Il2CppObject * /* static, unused */, IObserver_1U5BU5D_t1502147871*, Il2CppObject*, const MethodInfo*))Array_IndexOf_TisIl2CppObject_m2661005505_gshared)(__this /* static, unused */, p0, p1, method)
// System.Int32 System.Array::IndexOf<UniRx.IObserver`1<System.Int64>>(!!0[],!!0)
#define Array_IndexOf_TisIObserver_1_t764446489_m115881810(__this /* static, unused */, p0, p1, method) ((  int32_t (*) (Il2CppObject * /* static, unused */, IObserver_1U5BU5D_t445342820*, Il2CppObject*, const MethodInfo*))Array_IndexOf_TisIl2CppObject_m2661005505_gshared)(__this /* static, unused */, p0, p1, method)
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void UniRx.BehaviorSubject`1<System.Object>::.ctor(T)
extern Il2CppClass* Il2CppObject_il2cpp_TypeInfo_var;
extern const uint32_t BehaviorSubject_1__ctor_m4100827064_MetadataUsageId;
extern "C"  void BehaviorSubject_1__ctor_m4100827064_gshared (BehaviorSubject_1_t2662307150 * __this, Il2CppObject * ___defaultValue0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (BehaviorSubject_1__ctor_m4100827064_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject * L_0 = (Il2CppObject *)il2cpp_codegen_object_new(Il2CppObject_il2cpp_TypeInfo_var);
		Object__ctor_m1772956182(L_0, /*hidden argument*/NULL);
		__this->set_observerLock_0(L_0);
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		EmptyObserver_1_t246459410 * L_1 = ((EmptyObserver_1_t246459410_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->get_Instance_0();
		__this->set_outObserver_5(L_1);
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		Il2CppObject * L_2 = ___defaultValue0;
		__this->set_lastValue_3(L_2);
		return;
	}
}
// T UniRx.BehaviorSubject`1<System.Object>::get_Value()
extern "C"  Il2CppObject * BehaviorSubject_1_get_Value_m3483490925_gshared (BehaviorSubject_1_t2662307150 * __this, const MethodInfo* method)
{
	{
		NullCheck((BehaviorSubject_1_t2662307150 *)__this);
		((  void (*) (BehaviorSubject_1_t2662307150 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)((BehaviorSubject_1_t2662307150 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		Exception_t1967233988 * L_0 = (Exception_t1967233988 *)__this->get_lastError_4();
		if (!L_0)
		{
			goto IL_0018;
		}
	}
	{
		Exception_t1967233988 * L_1 = (Exception_t1967233988 *)__this->get_lastError_4();
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		Il2CppObject * L_2 = (Il2CppObject *)__this->get_lastValue_3();
		return L_2;
	}
}
// System.Boolean UniRx.BehaviorSubject`1<System.Object>::get_HasObservers()
extern "C"  bool BehaviorSubject_1_get_HasObservers_m3919047526_gshared (BehaviorSubject_1_t2662307150 * __this, const MethodInfo* method)
{
	int32_t G_B4_0 = 0;
	{
		Il2CppObject* L_0 = (Il2CppObject*)__this->get_outObserver_5();
		if (((EmptyObserver_1_t246459410 *)IsInst(L_0, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2))))
		{
			goto IL_0026;
		}
	}
	{
		bool L_1 = (bool)__this->get_isStopped_1();
		if (L_1)
		{
			goto IL_0026;
		}
	}
	{
		bool L_2 = (bool)__this->get_isDisposed_2();
		G_B4_0 = ((((int32_t)L_2) == ((int32_t)0))? 1 : 0);
		goto IL_0027;
	}

IL_0026:
	{
		G_B4_0 = 0;
	}

IL_0027:
	{
		return (bool)G_B4_0;
	}
}
// System.Void UniRx.BehaviorSubject`1<System.Object>::OnCompleted()
extern "C"  void BehaviorSubject_1_OnCompleted_m1128724848_gshared (BehaviorSubject_1_t2662307150 * __this, const MethodInfo* method)
{
	Il2CppObject* V_0 = NULL;
	Il2CppObject * V_1 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_observerLock_0();
		V_1 = (Il2CppObject *)L_0;
		Il2CppObject * L_1 = V_1;
		Monitor_Enter_m476686225(NULL /*static, unused*/, (Il2CppObject *)L_1, /*hidden argument*/NULL);
	}

IL_000d:
	try
	{ // begin try (depth: 1)
		{
			NullCheck((BehaviorSubject_1_t2662307150 *)__this);
			((  void (*) (BehaviorSubject_1_t2662307150 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)((BehaviorSubject_1_t2662307150 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
			bool L_2 = (bool)__this->get_isStopped_1();
			if (!L_2)
			{
				goto IL_0023;
			}
		}

IL_001e:
		{
			IL2CPP_LEAVE(0x4E, FINALLY_0041);
		}

IL_0023:
		{
			Il2CppObject* L_3 = (Il2CppObject*)__this->get_outObserver_5();
			V_0 = (Il2CppObject*)L_3;
			IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
			EmptyObserver_1_t246459410 * L_4 = ((EmptyObserver_1_t246459410_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->get_Instance_0();
			__this->set_outObserver_5(L_4);
			__this->set_isStopped_1((bool)1);
			IL2CPP_LEAVE(0x48, FINALLY_0041);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0041;
	}

FINALLY_0041:
	{ // begin finally (depth: 1)
		Il2CppObject * L_5 = V_1;
		Monitor_Exit_m2088237919(NULL /*static, unused*/, (Il2CppObject *)L_5, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(65)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(65)
	{
		IL2CPP_JUMP_TBL(0x4E, IL_004e)
		IL2CPP_JUMP_TBL(0x48, IL_0048)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0048:
	{
		Il2CppObject* L_6 = V_0;
		NullCheck((Il2CppObject*)L_6);
		InterfaceActionInvoker0::Invoke(0 /* System.Void UniRx.IObserver`1<System.Object>::OnCompleted() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3), (Il2CppObject*)L_6);
	}

IL_004e:
	{
		return;
	}
}
// System.Void UniRx.BehaviorSubject`1<System.Object>::OnError(System.Exception)
extern Il2CppClass* ArgumentNullException_t3214793280_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral96784904;
extern const uint32_t BehaviorSubject_1_OnError_m1905722013_MetadataUsageId;
extern "C"  void BehaviorSubject_1_OnError_m1905722013_gshared (BehaviorSubject_1_t2662307150 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (BehaviorSubject_1_OnError_m1905722013_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Il2CppObject* V_0 = NULL;
	Il2CppObject * V_1 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Exception_t1967233988 * L_0 = ___error0;
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		ArgumentNullException_t3214793280 * L_1 = (ArgumentNullException_t3214793280 *)il2cpp_codegen_object_new(ArgumentNullException_t3214793280_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m135444188(L_1, (String_t*)_stringLiteral96784904, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0011:
	{
		Il2CppObject * L_2 = (Il2CppObject *)__this->get_observerLock_0();
		V_1 = (Il2CppObject *)L_2;
		Il2CppObject * L_3 = V_1;
		Monitor_Enter_m476686225(NULL /*static, unused*/, (Il2CppObject *)L_3, /*hidden argument*/NULL);
	}

IL_001e:
	try
	{ // begin try (depth: 1)
		{
			NullCheck((BehaviorSubject_1_t2662307150 *)__this);
			((  void (*) (BehaviorSubject_1_t2662307150 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)((BehaviorSubject_1_t2662307150 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
			bool L_4 = (bool)__this->get_isStopped_1();
			if (!L_4)
			{
				goto IL_0034;
			}
		}

IL_002f:
		{
			IL2CPP_LEAVE(0x67, FINALLY_0059);
		}

IL_0034:
		{
			Il2CppObject* L_5 = (Il2CppObject*)__this->get_outObserver_5();
			V_0 = (Il2CppObject*)L_5;
			IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
			EmptyObserver_1_t246459410 * L_6 = ((EmptyObserver_1_t246459410_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->get_Instance_0();
			__this->set_outObserver_5(L_6);
			__this->set_isStopped_1((bool)1);
			Exception_t1967233988 * L_7 = ___error0;
			__this->set_lastError_4(L_7);
			IL2CPP_LEAVE(0x60, FINALLY_0059);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0059;
	}

FINALLY_0059:
	{ // begin finally (depth: 1)
		Il2CppObject * L_8 = V_1;
		Monitor_Exit_m2088237919(NULL /*static, unused*/, (Il2CppObject *)L_8, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(89)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(89)
	{
		IL2CPP_JUMP_TBL(0x67, IL_0067)
		IL2CPP_JUMP_TBL(0x60, IL_0060)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0060:
	{
		Il2CppObject* L_9 = V_0;
		Exception_t1967233988 * L_10 = ___error0;
		NullCheck((Il2CppObject*)L_9);
		InterfaceActionInvoker1< Exception_t1967233988 * >::Invoke(1 /* System.Void UniRx.IObserver`1<System.Object>::OnError(System.Exception) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3), (Il2CppObject*)L_9, (Exception_t1967233988 *)L_10);
	}

IL_0067:
	{
		return;
	}
}
// System.Void UniRx.BehaviorSubject`1<System.Object>::OnNext(T)
extern "C"  void BehaviorSubject_1_OnNext_m1107280782_gshared (BehaviorSubject_1_t2662307150 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	Il2CppObject* V_0 = NULL;
	Il2CppObject * V_1 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_observerLock_0();
		V_1 = (Il2CppObject *)L_0;
		Il2CppObject * L_1 = V_1;
		Monitor_Enter_m476686225(NULL /*static, unused*/, (Il2CppObject *)L_1, /*hidden argument*/NULL);
	}

IL_000d:
	try
	{ // begin try (depth: 1)
		{
			bool L_2 = (bool)__this->get_isStopped_1();
			if (!L_2)
			{
				goto IL_001d;
			}
		}

IL_0018:
		{
			IL2CPP_LEAVE(0x3E, FINALLY_0030);
		}

IL_001d:
		{
			Il2CppObject * L_3 = ___value0;
			__this->set_lastValue_3(L_3);
			Il2CppObject* L_4 = (Il2CppObject*)__this->get_outObserver_5();
			V_0 = (Il2CppObject*)L_4;
			IL2CPP_LEAVE(0x37, FINALLY_0030);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0030;
	}

FINALLY_0030:
	{ // begin finally (depth: 1)
		Il2CppObject * L_5 = V_1;
		Monitor_Exit_m2088237919(NULL /*static, unused*/, (Il2CppObject *)L_5, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(48)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(48)
	{
		IL2CPP_JUMP_TBL(0x3E, IL_003e)
		IL2CPP_JUMP_TBL(0x37, IL_0037)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0037:
	{
		Il2CppObject* L_6 = V_0;
		Il2CppObject * L_7 = ___value0;
		NullCheck((Il2CppObject*)L_6);
		InterfaceActionInvoker1< Il2CppObject * >::Invoke(2 /* System.Void UniRx.IObserver`1<System.Object>::OnNext(T) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3), (Il2CppObject*)L_6, (Il2CppObject *)L_7);
	}

IL_003e:
	{
		return;
	}
}
// System.IDisposable UniRx.BehaviorSubject`1<System.Object>::Subscribe(UniRx.IObserver`1<T>)
extern Il2CppClass* ArgumentNullException_t3214793280_il2cpp_TypeInfo_var;
extern Il2CppClass* Il2CppObject_il2cpp_TypeInfo_var;
extern Il2CppClass* Disposable_t3736388786_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral348607190;
extern const uint32_t BehaviorSubject_1_Subscribe_m3151764837_MetadataUsageId;
extern "C"  Il2CppObject * BehaviorSubject_1_Subscribe_m3151764837_gshared (BehaviorSubject_1_t2662307150 * __this, Il2CppObject* ___observer0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (BehaviorSubject_1_Subscribe_m3151764837_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Exception_t1967233988 * V_0 = NULL;
	Il2CppObject * V_1 = NULL;
	Subscription_t2911320240 * V_2 = NULL;
	Il2CppObject * V_3 = NULL;
	ListObserver_1_t1131724091 * V_4 = NULL;
	Il2CppObject* V_5 = NULL;
	Il2CppObject * V_6 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Il2CppObject* L_0 = ___observer0;
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		ArgumentNullException_t3214793280 * L_1 = (ArgumentNullException_t3214793280 *)il2cpp_codegen_object_new(ArgumentNullException_t3214793280_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m135444188(L_1, (String_t*)_stringLiteral348607190, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0011:
	{
		V_0 = (Exception_t1967233988 *)NULL;
		Initobj (Il2CppObject_il2cpp_TypeInfo_var, (&V_6));
		Il2CppObject * L_2 = V_6;
		V_1 = (Il2CppObject *)L_2;
		V_2 = (Subscription_t2911320240 *)NULL;
		Il2CppObject * L_3 = (Il2CppObject *)__this->get_observerLock_0();
		V_3 = (Il2CppObject *)L_3;
		Il2CppObject * L_4 = V_3;
		Monitor_Enter_m476686225(NULL /*static, unused*/, (Il2CppObject *)L_4, /*hidden argument*/NULL);
	}

IL_002d:
	try
	{ // begin try (depth: 1)
		{
			NullCheck((BehaviorSubject_1_t2662307150 *)__this);
			((  void (*) (BehaviorSubject_1_t2662307150 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)((BehaviorSubject_1_t2662307150 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
			bool L_5 = (bool)__this->get_isStopped_1();
			if (L_5)
			{
				goto IL_00b8;
			}
		}

IL_003e:
		{
			Il2CppObject* L_6 = (Il2CppObject*)__this->get_outObserver_5();
			V_4 = (ListObserver_1_t1131724091 *)((ListObserver_1_t1131724091 *)IsInst(L_6, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)));
			ListObserver_1_t1131724091 * L_7 = V_4;
			if (!L_7)
			{
				goto IL_0065;
			}
		}

IL_0052:
		{
			ListObserver_1_t1131724091 * L_8 = V_4;
			Il2CppObject* L_9 = ___observer0;
			NullCheck((ListObserver_1_t1131724091 *)L_8);
			Il2CppObject* L_10 = ((  Il2CppObject* (*) (ListObserver_1_t1131724091 *, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->method)((ListObserver_1_t1131724091 *)L_8, (Il2CppObject*)L_9, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
			__this->set_outObserver_5(L_10);
			goto IL_00a4;
		}

IL_0065:
		{
			Il2CppObject* L_11 = (Il2CppObject*)__this->get_outObserver_5();
			V_5 = (Il2CppObject*)L_11;
			Il2CppObject* L_12 = V_5;
			if (!((EmptyObserver_1_t246459410 *)IsInst(L_12, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2))))
			{
				goto IL_0085;
			}
		}

IL_0079:
		{
			Il2CppObject* L_13 = ___observer0;
			__this->set_outObserver_5(L_13);
			goto IL_00a4;
		}

IL_0085:
		{
			IObserver_1U5BU5D_t3998655818* L_14 = (IObserver_1U5BU5D_t3998655818*)((IObserver_1U5BU5D_t3998655818*)SZArrayNew(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6), (uint32_t)2));
			Il2CppObject* L_15 = V_5;
			NullCheck(L_14);
			IL2CPP_ARRAY_BOUNDS_CHECK(L_14, 0);
			ArrayElementTypeCheck (L_14, L_15);
			(L_14)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject*)L_15);
			IObserver_1U5BU5D_t3998655818* L_16 = (IObserver_1U5BU5D_t3998655818*)L_14;
			Il2CppObject* L_17 = ___observer0;
			NullCheck(L_16);
			IL2CPP_ARRAY_BOUNDS_CHECK(L_16, 1);
			ArrayElementTypeCheck (L_16, L_17);
			(L_16)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject*)L_17);
			ImmutableList_1_t4109309822 * L_18 = (ImmutableList_1_t4109309822 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7));
			((  void (*) (ImmutableList_1_t4109309822 *, IObserver_1U5BU5D_t3998655818*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8)->method)(L_18, (IObserver_1U5BU5D_t3998655818*)L_16, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8));
			ListObserver_1_t1131724091 * L_19 = (ListObserver_1_t1131724091 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
			((  void (*) (ListObserver_1_t1131724091 *, ImmutableList_1_t4109309822 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9)->method)(L_19, (ImmutableList_1_t4109309822 *)L_18, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9));
			__this->set_outObserver_5(L_19);
		}

IL_00a4:
		{
			Il2CppObject * L_20 = (Il2CppObject *)__this->get_lastValue_3();
			V_1 = (Il2CppObject *)L_20;
			Il2CppObject* L_21 = ___observer0;
			Subscription_t2911320240 * L_22 = (Subscription_t2911320240 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10));
			((  void (*) (Subscription_t2911320240 *, BehaviorSubject_1_t2662307150 *, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 11)->method)(L_22, (BehaviorSubject_1_t2662307150 *)__this, (Il2CppObject*)L_21, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 11));
			V_2 = (Subscription_t2911320240 *)L_22;
			goto IL_00bf;
		}

IL_00b8:
		{
			Exception_t1967233988 * L_23 = (Exception_t1967233988 *)__this->get_lastError_4();
			V_0 = (Exception_t1967233988 *)L_23;
		}

IL_00bf:
		{
			IL2CPP_LEAVE(0xCB, FINALLY_00c4);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_00c4;
	}

FINALLY_00c4:
	{ // begin finally (depth: 1)
		Il2CppObject * L_24 = V_3;
		Monitor_Exit_m2088237919(NULL /*static, unused*/, (Il2CppObject *)L_24, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(196)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(196)
	{
		IL2CPP_JUMP_TBL(0xCB, IL_00cb)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_00cb:
	{
		Subscription_t2911320240 * L_25 = V_2;
		if (!L_25)
		{
			goto IL_00da;
		}
	}
	{
		Il2CppObject* L_26 = ___observer0;
		Il2CppObject * L_27 = V_1;
		NullCheck((Il2CppObject*)L_26);
		InterfaceActionInvoker1< Il2CppObject * >::Invoke(2 /* System.Void UniRx.IObserver`1<System.Object>::OnNext(T) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3), (Il2CppObject*)L_26, (Il2CppObject *)L_27);
		Subscription_t2911320240 * L_28 = V_2;
		return L_28;
	}

IL_00da:
	{
		Exception_t1967233988 * L_29 = V_0;
		if (!L_29)
		{
			goto IL_00ec;
		}
	}
	{
		Il2CppObject* L_30 = ___observer0;
		Exception_t1967233988 * L_31 = V_0;
		NullCheck((Il2CppObject*)L_30);
		InterfaceActionInvoker1< Exception_t1967233988 * >::Invoke(1 /* System.Void UniRx.IObserver`1<System.Object>::OnError(System.Exception) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3), (Il2CppObject*)L_30, (Exception_t1967233988 *)L_31);
		goto IL_00f2;
	}

IL_00ec:
	{
		Il2CppObject* L_32 = ___observer0;
		NullCheck((Il2CppObject*)L_32);
		InterfaceActionInvoker0::Invoke(0 /* System.Void UniRx.IObserver`1<System.Object>::OnCompleted() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3), (Il2CppObject*)L_32);
	}

IL_00f2:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Disposable_t3736388786_il2cpp_TypeInfo_var);
		Il2CppObject * L_33 = ((Disposable_t3736388786_StaticFields*)Disposable_t3736388786_il2cpp_TypeInfo_var->static_fields)->get_Empty_0();
		return L_33;
	}
}
// System.Void UniRx.BehaviorSubject`1<System.Object>::Dispose()
extern Il2CppClass* Il2CppObject_il2cpp_TypeInfo_var;
extern const uint32_t BehaviorSubject_1_Dispose_m2469827875_MetadataUsageId;
extern "C"  void BehaviorSubject_1_Dispose_m2469827875_gshared (BehaviorSubject_1_t2662307150 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (BehaviorSubject_1_Dispose_m2469827875_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Il2CppObject * V_0 = NULL;
	Il2CppObject * V_1 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_observerLock_0();
		V_0 = (Il2CppObject *)L_0;
		Il2CppObject * L_1 = V_0;
		Monitor_Enter_m476686225(NULL /*static, unused*/, (Il2CppObject *)L_1, /*hidden argument*/NULL);
	}

IL_000d:
	try
	{ // begin try (depth: 1)
		__this->set_isDisposed_2((bool)1);
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 12));
		DisposedObserver_1_t11563882 * L_2 = ((DisposedObserver_1_t11563882_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 12)->static_fields)->get_Instance_0();
		__this->set_outObserver_5(L_2);
		__this->set_lastError_4((Exception_t1967233988 *)NULL);
		Initobj (Il2CppObject_il2cpp_TypeInfo_var, (&V_1));
		Il2CppObject * L_3 = V_1;
		__this->set_lastValue_3(L_3);
		IL2CPP_LEAVE(0x41, FINALLY_003a);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_003a;
	}

FINALLY_003a:
	{ // begin finally (depth: 1)
		Il2CppObject * L_4 = V_0;
		Monitor_Exit_m2088237919(NULL /*static, unused*/, (Il2CppObject *)L_4, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(58)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(58)
	{
		IL2CPP_JUMP_TBL(0x41, IL_0041)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0041:
	{
		return;
	}
}
// System.Void UniRx.BehaviorSubject`1<System.Object>::ThrowIfDisposed()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* ObjectDisposedException_t973246880_il2cpp_TypeInfo_var;
extern const uint32_t BehaviorSubject_1_ThrowIfDisposed_m642411436_MetadataUsageId;
extern "C"  void BehaviorSubject_1_ThrowIfDisposed_m642411436_gshared (BehaviorSubject_1_t2662307150 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (BehaviorSubject_1_ThrowIfDisposed_m642411436_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = (bool)__this->get_isDisposed_2();
		if (!L_0)
		{
			goto IL_0016;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_1 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		ObjectDisposedException_t973246880 * L_2 = (ObjectDisposedException_t973246880 *)il2cpp_codegen_object_new(ObjectDisposedException_t973246880_il2cpp_TypeInfo_var);
		ObjectDisposedException__ctor_m1180707260(L_2, (String_t*)L_1, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0016:
	{
		return;
	}
}
// System.Boolean UniRx.BehaviorSubject`1<System.Object>::IsRequiredSubscribeOnCurrentThread()
extern "C"  bool BehaviorSubject_1_IsRequiredSubscribeOnCurrentThread_m2153941725_gshared (BehaviorSubject_1_t2662307150 * __this, const MethodInfo* method)
{
	{
		return (bool)0;
	}
}
// System.Void UniRx.CollectionAddEvent`1<System.Object>::.ctor(System.Int32,T)
extern "C"  void CollectionAddEvent_1__ctor_m4121910756_gshared (CollectionAddEvent_1_t2416521987 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method)
{
	{
		int32_t L_0 = ___index0;
		((  void (*) (CollectionAddEvent_1_t2416521987 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((CollectionAddEvent_1_t2416521987 *)__this, (int32_t)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		Il2CppObject * L_1 = ___value1;
		((  void (*) (CollectionAddEvent_1_t2416521987 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)((CollectionAddEvent_1_t2416521987 *)__this, (Il2CppObject *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		return;
	}
}
// System.Int32 UniRx.CollectionAddEvent`1<System.Object>::get_Index()
extern "C"  int32_t CollectionAddEvent_1_get_Index_m1649808760_gshared (CollectionAddEvent_1_t2416521987 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (int32_t)__this->get_U3CIndexU3Ek__BackingField_0();
		return L_0;
	}
}
// System.Void UniRx.CollectionAddEvent`1<System.Object>::set_Index(System.Int32)
extern "C"  void CollectionAddEvent_1_set_Index_m4001500511_gshared (CollectionAddEvent_1_t2416521987 * __this, int32_t ___value0, const MethodInfo* method)
{
	{
		int32_t L_0 = ___value0;
		__this->set_U3CIndexU3Ek__BackingField_0(L_0);
		return;
	}
}
// T UniRx.CollectionAddEvent`1<System.Object>::get_Value()
extern "C"  Il2CppObject * CollectionAddEvent_1_get_Value_m1882365472_gshared (CollectionAddEvent_1_t2416521987 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_U3CValueU3Ek__BackingField_1();
		return L_0;
	}
}
// System.Void UniRx.CollectionAddEvent`1<System.Object>::set_Value(T)
extern "C"  void CollectionAddEvent_1_set_Value_m1389365521_gshared (CollectionAddEvent_1_t2416521987 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___value0;
		__this->set_U3CValueU3Ek__BackingField_1(L_0);
		return;
	}
}
// System.String UniRx.CollectionAddEvent`1<System.Object>::ToString()
extern Il2CppClass* Int32_t2847414787_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2032474014;
extern const uint32_t CollectionAddEvent_1_ToString_m3034630034_MetadataUsageId;
extern "C"  String_t* CollectionAddEvent_1_ToString_m3034630034_gshared (CollectionAddEvent_1_t2416521987 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (CollectionAddEvent_1_ToString_m3034630034_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = ((  int32_t (*) (CollectionAddEvent_1_t2416521987 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)((CollectionAddEvent_1_t2416521987 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		int32_t L_1 = L_0;
		Il2CppObject * L_2 = Box(Int32_t2847414787_il2cpp_TypeInfo_var, &L_1);
		Il2CppObject * L_3 = ((  Il2CppObject * (*) (CollectionAddEvent_1_t2416521987 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)((CollectionAddEvent_1_t2416521987 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_4 = String_Format_m2398979370(NULL /*static, unused*/, (String_t*)_stringLiteral2032474014, (Il2CppObject *)L_2, (Il2CppObject *)L_3, /*hidden argument*/NULL);
		return L_4;
	}
}
// System.Int32 UniRx.CollectionAddEvent`1<System.Object>::GetHashCode()
extern "C"  int32_t CollectionAddEvent_1_GetHashCode_m3912132320_gshared (CollectionAddEvent_1_t2416521987 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		int32_t L_0 = ((  int32_t (*) (CollectionAddEvent_1_t2416521987 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)((CollectionAddEvent_1_t2416521987 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		V_0 = (int32_t)L_0;
		int32_t L_1 = Int32_GetHashCode_m3396943446((int32_t*)(&V_0), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		EqualityComparer_1_t271497070 * L_2 = ((  EqualityComparer_1_t271497070 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->method)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		Il2CppObject * L_3 = ((  Il2CppObject * (*) (CollectionAddEvent_1_t2416521987 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)((CollectionAddEvent_1_t2416521987 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		NullCheck((EqualityComparer_1_t271497070 *)L_2);
		int32_t L_4 = VirtFuncInvoker1< int32_t, Il2CppObject * >::Invoke(8 /* System.Int32 System.Collections.Generic.EqualityComparer`1<System.Object>::GetHashCode(!0) */, (EqualityComparer_1_t271497070 *)L_2, (Il2CppObject *)L_3);
		return ((int32_t)((int32_t)L_1^(int32_t)((int32_t)((int32_t)L_4<<(int32_t)2))));
	}
}
// System.Boolean UniRx.CollectionAddEvent`1<System.Object>::Equals(UniRx.CollectionAddEvent`1<T>)
extern "C"  bool CollectionAddEvent_1_Equals_m2095755040_gshared (CollectionAddEvent_1_t2416521987 * __this, CollectionAddEvent_1_t2416521987  ___other0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B3_0 = 0;
	{
		int32_t L_0 = ((  int32_t (*) (CollectionAddEvent_1_t2416521987 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)((CollectionAddEvent_1_t2416521987 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		V_0 = (int32_t)L_0;
		int32_t L_1 = ((  int32_t (*) (CollectionAddEvent_1_t2416521987 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)((CollectionAddEvent_1_t2416521987 *)(&___other0), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		bool L_2 = Int32_Equals_m3849884467((int32_t*)(&V_0), (int32_t)L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0033;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		EqualityComparer_1_t271497070 * L_3 = ((  EqualityComparer_1_t271497070 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->method)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		Il2CppObject * L_4 = ((  Il2CppObject * (*) (CollectionAddEvent_1_t2416521987 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)((CollectionAddEvent_1_t2416521987 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		Il2CppObject * L_5 = ((  Il2CppObject * (*) (CollectionAddEvent_1_t2416521987 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)((CollectionAddEvent_1_t2416521987 *)(&___other0), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		NullCheck((EqualityComparer_1_t271497070 *)L_3);
		bool L_6 = VirtFuncInvoker2< bool, Il2CppObject *, Il2CppObject * >::Invoke(9 /* System.Boolean System.Collections.Generic.EqualityComparer`1<System.Object>::Equals(!0,!0) */, (EqualityComparer_1_t271497070 *)L_3, (Il2CppObject *)L_4, (Il2CppObject *)L_5);
		G_B3_0 = ((int32_t)(L_6));
		goto IL_0034;
	}

IL_0033:
	{
		G_B3_0 = 0;
	}

IL_0034:
	{
		return (bool)G_B3_0;
	}
}
// System.Void UniRx.CollectionAddEvent`1<UniRx.Tuple`2<System.Object,System.Object>>::.ctor(System.Int32,T)
extern "C"  void CollectionAddEvent_1__ctor_m1733868862_gshared (CollectionAddEvent_1_t1948677386 * __this, int32_t ___index0, Tuple_2_t369261819  ___value1, const MethodInfo* method)
{
	{
		int32_t L_0 = ___index0;
		((  void (*) (CollectionAddEvent_1_t1948677386 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((CollectionAddEvent_1_t1948677386 *)__this, (int32_t)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		Tuple_2_t369261819  L_1 = ___value1;
		((  void (*) (CollectionAddEvent_1_t1948677386 *, Tuple_2_t369261819 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)((CollectionAddEvent_1_t1948677386 *)__this, (Tuple_2_t369261819 )L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		return;
	}
}
// System.Int32 UniRx.CollectionAddEvent`1<UniRx.Tuple`2<System.Object,System.Object>>::get_Index()
extern "C"  int32_t CollectionAddEvent_1_get_Index_m4141336102_gshared (CollectionAddEvent_1_t1948677386 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (int32_t)__this->get_U3CIndexU3Ek__BackingField_0();
		return L_0;
	}
}
// System.Void UniRx.CollectionAddEvent`1<UniRx.Tuple`2<System.Object,System.Object>>::set_Index(System.Int32)
extern "C"  void CollectionAddEvent_1_set_Index_m2605776441_gshared (CollectionAddEvent_1_t1948677386 * __this, int32_t ___value0, const MethodInfo* method)
{
	{
		int32_t L_0 = ___value0;
		__this->set_U3CIndexU3Ek__BackingField_0(L_0);
		return;
	}
}
// T UniRx.CollectionAddEvent`1<UniRx.Tuple`2<System.Object,System.Object>>::get_Value()
extern "C"  Tuple_2_t369261819  CollectionAddEvent_1_get_Value_m4091107292_gshared (CollectionAddEvent_1_t1948677386 * __this, const MethodInfo* method)
{
	{
		Tuple_2_t369261819  L_0 = (Tuple_2_t369261819 )__this->get_U3CValueU3Ek__BackingField_1();
		return L_0;
	}
}
// System.Void UniRx.CollectionAddEvent`1<UniRx.Tuple`2<System.Object,System.Object>>::set_Value(T)
extern "C"  void CollectionAddEvent_1_set_Value_m1420762231_gshared (CollectionAddEvent_1_t1948677386 * __this, Tuple_2_t369261819  ___value0, const MethodInfo* method)
{
	{
		Tuple_2_t369261819  L_0 = ___value0;
		__this->set_U3CValueU3Ek__BackingField_1(L_0);
		return;
	}
}
// System.String UniRx.CollectionAddEvent`1<UniRx.Tuple`2<System.Object,System.Object>>::ToString()
extern Il2CppClass* Int32_t2847414787_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2032474014;
extern const uint32_t CollectionAddEvent_1_ToString_m3083878430_MetadataUsageId;
extern "C"  String_t* CollectionAddEvent_1_ToString_m3083878430_gshared (CollectionAddEvent_1_t1948677386 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (CollectionAddEvent_1_ToString_m3083878430_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = ((  int32_t (*) (CollectionAddEvent_1_t1948677386 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)((CollectionAddEvent_1_t1948677386 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		int32_t L_1 = L_0;
		Il2CppObject * L_2 = Box(Int32_t2847414787_il2cpp_TypeInfo_var, &L_1);
		Tuple_2_t369261819  L_3 = ((  Tuple_2_t369261819  (*) (CollectionAddEvent_1_t1948677386 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)((CollectionAddEvent_1_t1948677386 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		Tuple_2_t369261819  L_4 = L_3;
		Il2CppObject * L_5 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4), &L_4);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_6 = String_Format_m2398979370(NULL /*static, unused*/, (String_t*)_stringLiteral2032474014, (Il2CppObject *)L_2, (Il2CppObject *)L_5, /*hidden argument*/NULL);
		return L_6;
	}
}
// System.Int32 UniRx.CollectionAddEvent`1<UniRx.Tuple`2<System.Object,System.Object>>::GetHashCode()
extern "C"  int32_t CollectionAddEvent_1_GetHashCode_m1678156814_gshared (CollectionAddEvent_1_t1948677386 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		int32_t L_0 = ((  int32_t (*) (CollectionAddEvent_1_t1948677386 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)((CollectionAddEvent_1_t1948677386 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		V_0 = (int32_t)L_0;
		int32_t L_1 = Int32_GetHashCode_m3396943446((int32_t*)(&V_0), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		EqualityComparer_1_t4098619765 * L_2 = ((  EqualityComparer_1_t4098619765 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->method)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		Tuple_2_t369261819  L_3 = ((  Tuple_2_t369261819  (*) (CollectionAddEvent_1_t1948677386 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)((CollectionAddEvent_1_t1948677386 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		NullCheck((EqualityComparer_1_t4098619765 *)L_2);
		int32_t L_4 = VirtFuncInvoker1< int32_t, Tuple_2_t369261819  >::Invoke(8 /* System.Int32 System.Collections.Generic.EqualityComparer`1<UniRx.Tuple`2<System.Object,System.Object>>::GetHashCode(!0) */, (EqualityComparer_1_t4098619765 *)L_2, (Tuple_2_t369261819 )L_3);
		return ((int32_t)((int32_t)L_1^(int32_t)((int32_t)((int32_t)L_4<<(int32_t)2))));
	}
}
// System.Boolean UniRx.CollectionAddEvent`1<UniRx.Tuple`2<System.Object,System.Object>>::Equals(UniRx.CollectionAddEvent`1<T>)
extern "C"  bool CollectionAddEvent_1_Equals_m2717942978_gshared (CollectionAddEvent_1_t1948677386 * __this, CollectionAddEvent_1_t1948677386  ___other0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B3_0 = 0;
	{
		int32_t L_0 = ((  int32_t (*) (CollectionAddEvent_1_t1948677386 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)((CollectionAddEvent_1_t1948677386 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		V_0 = (int32_t)L_0;
		int32_t L_1 = ((  int32_t (*) (CollectionAddEvent_1_t1948677386 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)((CollectionAddEvent_1_t1948677386 *)(&___other0), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		bool L_2 = Int32_Equals_m3849884467((int32_t*)(&V_0), (int32_t)L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0033;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		EqualityComparer_1_t4098619765 * L_3 = ((  EqualityComparer_1_t4098619765 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->method)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		Tuple_2_t369261819  L_4 = ((  Tuple_2_t369261819  (*) (CollectionAddEvent_1_t1948677386 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)((CollectionAddEvent_1_t1948677386 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		Tuple_2_t369261819  L_5 = ((  Tuple_2_t369261819  (*) (CollectionAddEvent_1_t1948677386 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)((CollectionAddEvent_1_t1948677386 *)(&___other0), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		NullCheck((EqualityComparer_1_t4098619765 *)L_3);
		bool L_6 = VirtFuncInvoker2< bool, Tuple_2_t369261819 , Tuple_2_t369261819  >::Invoke(9 /* System.Boolean System.Collections.Generic.EqualityComparer`1<UniRx.Tuple`2<System.Object,System.Object>>::Equals(!0,!0) */, (EqualityComparer_1_t4098619765 *)L_3, (Tuple_2_t369261819 )L_4, (Tuple_2_t369261819 )L_5);
		G_B3_0 = ((int32_t)(L_6));
		goto IL_0034;
	}

IL_0033:
	{
		G_B3_0 = 0;
	}

IL_0034:
	{
		return (bool)G_B3_0;
	}
}
// System.Void UniRx.CollectionMoveEvent`1<System.Object>::.ctor(System.Int32,System.Int32,T)
extern "C"  void CollectionMoveEvent_1__ctor_m539258343_gshared (CollectionMoveEvent_1_t2916433847 * __this, int32_t ___oldIndex0, int32_t ___newIndex1, Il2CppObject * ___value2, const MethodInfo* method)
{
	{
		int32_t L_0 = ___oldIndex0;
		((  void (*) (CollectionMoveEvent_1_t2916433847 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((CollectionMoveEvent_1_t2916433847 *)__this, (int32_t)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_1 = ___newIndex1;
		((  void (*) (CollectionMoveEvent_1_t2916433847 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)((CollectionMoveEvent_1_t2916433847 *)__this, (int32_t)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		Il2CppObject * L_2 = ___value2;
		((  void (*) (CollectionMoveEvent_1_t2916433847 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)((CollectionMoveEvent_1_t2916433847 *)__this, (Il2CppObject *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return;
	}
}
// System.Int32 UniRx.CollectionMoveEvent`1<System.Object>::get_OldIndex()
extern "C"  int32_t CollectionMoveEvent_1_get_OldIndex_m4078874263_gshared (CollectionMoveEvent_1_t2916433847 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (int32_t)__this->get_U3COldIndexU3Ek__BackingField_0();
		return L_0;
	}
}
// System.Void UniRx.CollectionMoveEvent`1<System.Object>::set_OldIndex(System.Int32)
extern "C"  void CollectionMoveEvent_1_set_OldIndex_m1382338918_gshared (CollectionMoveEvent_1_t2916433847 * __this, int32_t ___value0, const MethodInfo* method)
{
	{
		int32_t L_0 = ___value0;
		__this->set_U3COldIndexU3Ek__BackingField_0(L_0);
		return;
	}
}
// System.Int32 UniRx.CollectionMoveEvent`1<System.Object>::get_NewIndex()
extern "C"  int32_t CollectionMoveEvent_1_get_NewIndex_m2796325118_gshared (CollectionMoveEvent_1_t2916433847 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (int32_t)__this->get_U3CNewIndexU3Ek__BackingField_1();
		return L_0;
	}
}
// System.Void UniRx.CollectionMoveEvent`1<System.Object>::set_NewIndex(System.Int32)
extern "C"  void CollectionMoveEvent_1_set_NewIndex_m2763213133_gshared (CollectionMoveEvent_1_t2916433847 * __this, int32_t ___value0, const MethodInfo* method)
{
	{
		int32_t L_0 = ___value0;
		__this->set_U3CNewIndexU3Ek__BackingField_1(L_0);
		return;
	}
}
// T UniRx.CollectionMoveEvent`1<System.Object>::get_Value()
extern "C"  Il2CppObject * CollectionMoveEvent_1_get_Value_m2033004030_gshared (CollectionMoveEvent_1_t2916433847 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_U3CValueU3Ek__BackingField_2();
		return L_0;
	}
}
// System.Void UniRx.CollectionMoveEvent`1<System.Object>::set_Value(T)
extern "C"  void CollectionMoveEvent_1_set_Value_m3017074197_gshared (CollectionMoveEvent_1_t2916433847 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___value0;
		__this->set_U3CValueU3Ek__BackingField_2(L_0);
		return;
	}
}
// System.String UniRx.CollectionMoveEvent`1<System.Object>::ToString()
extern Il2CppClass* Int32_t2847414787_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral272725123;
extern const uint32_t CollectionMoveEvent_1_ToString_m3558423484_MetadataUsageId;
extern "C"  String_t* CollectionMoveEvent_1_ToString_m3558423484_gshared (CollectionMoveEvent_1_t2916433847 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (CollectionMoveEvent_1_ToString_m3558423484_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = ((  int32_t (*) (CollectionMoveEvent_1_t2916433847 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)((CollectionMoveEvent_1_t2916433847 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		int32_t L_1 = L_0;
		Il2CppObject * L_2 = Box(Int32_t2847414787_il2cpp_TypeInfo_var, &L_1);
		int32_t L_3 = ((  int32_t (*) (CollectionMoveEvent_1_t2916433847 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)((CollectionMoveEvent_1_t2916433847 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		int32_t L_4 = L_3;
		Il2CppObject * L_5 = Box(Int32_t2847414787_il2cpp_TypeInfo_var, &L_4);
		Il2CppObject * L_6 = ((  Il2CppObject * (*) (CollectionMoveEvent_1_t2916433847 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->method)((CollectionMoveEvent_1_t2916433847 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_7 = String_Format_m3928391288(NULL /*static, unused*/, (String_t*)_stringLiteral272725123, (Il2CppObject *)L_2, (Il2CppObject *)L_5, (Il2CppObject *)L_6, /*hidden argument*/NULL);
		return L_7;
	}
}
// System.Int32 UniRx.CollectionMoveEvent`1<System.Object>::GetHashCode()
extern "C"  int32_t CollectionMoveEvent_1_GetHashCode_m3735208240_gshared (CollectionMoveEvent_1_t2916433847 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		int32_t L_0 = ((  int32_t (*) (CollectionMoveEvent_1_t2916433847 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)((CollectionMoveEvent_1_t2916433847 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		V_0 = (int32_t)L_0;
		int32_t L_1 = Int32_GetHashCode_m3396943446((int32_t*)(&V_0), /*hidden argument*/NULL);
		int32_t L_2 = ((  int32_t (*) (CollectionMoveEvent_1_t2916433847 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)((CollectionMoveEvent_1_t2916433847 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		V_1 = (int32_t)L_2;
		int32_t L_3 = Int32_GetHashCode_m3396943446((int32_t*)(&V_1), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8));
		EqualityComparer_1_t271497070 * L_4 = ((  EqualityComparer_1_t271497070 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7)->method)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7));
		Il2CppObject * L_5 = ((  Il2CppObject * (*) (CollectionMoveEvent_1_t2916433847 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->method)((CollectionMoveEvent_1_t2916433847 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		NullCheck((EqualityComparer_1_t271497070 *)L_4);
		int32_t L_6 = VirtFuncInvoker1< int32_t, Il2CppObject * >::Invoke(8 /* System.Int32 System.Collections.Generic.EqualityComparer`1<System.Object>::GetHashCode(!0) */, (EqualityComparer_1_t271497070 *)L_4, (Il2CppObject *)L_5);
		return ((int32_t)((int32_t)((int32_t)((int32_t)L_1^(int32_t)((int32_t)((int32_t)L_3<<(int32_t)2))))^(int32_t)((int32_t)((int32_t)L_6>>(int32_t)2))));
	}
}
// System.Boolean UniRx.CollectionMoveEvent`1<System.Object>::Equals(UniRx.CollectionMoveEvent`1<T>)
extern "C"  bool CollectionMoveEvent_1_Equals_m320381798_gshared (CollectionMoveEvent_1_t2916433847 * __this, CollectionMoveEvent_1_t2916433847  ___other0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t G_B4_0 = 0;
	{
		int32_t L_0 = ((  int32_t (*) (CollectionMoveEvent_1_t2916433847 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)((CollectionMoveEvent_1_t2916433847 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		V_0 = (int32_t)L_0;
		int32_t L_1 = ((  int32_t (*) (CollectionMoveEvent_1_t2916433847 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)((CollectionMoveEvent_1_t2916433847 *)(&___other0), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		bool L_2 = Int32_Equals_m3849884467((int32_t*)(&V_0), (int32_t)L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_004d;
		}
	}
	{
		int32_t L_3 = ((  int32_t (*) (CollectionMoveEvent_1_t2916433847 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)((CollectionMoveEvent_1_t2916433847 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		V_1 = (int32_t)L_3;
		int32_t L_4 = ((  int32_t (*) (CollectionMoveEvent_1_t2916433847 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)((CollectionMoveEvent_1_t2916433847 *)(&___other0), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		bool L_5 = Int32_Equals_m3849884467((int32_t*)(&V_1), (int32_t)L_4, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_004d;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8));
		EqualityComparer_1_t271497070 * L_6 = ((  EqualityComparer_1_t271497070 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7)->method)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7));
		Il2CppObject * L_7 = ((  Il2CppObject * (*) (CollectionMoveEvent_1_t2916433847 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->method)((CollectionMoveEvent_1_t2916433847 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		Il2CppObject * L_8 = ((  Il2CppObject * (*) (CollectionMoveEvent_1_t2916433847 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->method)((CollectionMoveEvent_1_t2916433847 *)(&___other0), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		NullCheck((EqualityComparer_1_t271497070 *)L_6);
		bool L_9 = VirtFuncInvoker2< bool, Il2CppObject *, Il2CppObject * >::Invoke(9 /* System.Boolean System.Collections.Generic.EqualityComparer`1<System.Object>::Equals(!0,!0) */, (EqualityComparer_1_t271497070 *)L_6, (Il2CppObject *)L_7, (Il2CppObject *)L_8);
		G_B4_0 = ((int32_t)(L_9));
		goto IL_004e;
	}

IL_004d:
	{
		G_B4_0 = 0;
	}

IL_004e:
	{
		return (bool)G_B4_0;
	}
}
// System.Void UniRx.CollectionMoveEvent`1<UniRx.Tuple`2<System.Object,System.Object>>::.ctor(System.Int32,System.Int32,T)
extern "C"  void CollectionMoveEvent_1__ctor_m1005665093_gshared (CollectionMoveEvent_1_t2448589246 * __this, int32_t ___oldIndex0, int32_t ___newIndex1, Tuple_2_t369261819  ___value2, const MethodInfo* method)
{
	{
		int32_t L_0 = ___oldIndex0;
		((  void (*) (CollectionMoveEvent_1_t2448589246 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((CollectionMoveEvent_1_t2448589246 *)__this, (int32_t)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_1 = ___newIndex1;
		((  void (*) (CollectionMoveEvent_1_t2448589246 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)((CollectionMoveEvent_1_t2448589246 *)__this, (int32_t)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		Tuple_2_t369261819  L_2 = ___value2;
		((  void (*) (CollectionMoveEvent_1_t2448589246 *, Tuple_2_t369261819 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)((CollectionMoveEvent_1_t2448589246 *)__this, (Tuple_2_t369261819 )L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return;
	}
}
// System.Int32 UniRx.CollectionMoveEvent`1<UniRx.Tuple`2<System.Object,System.Object>>::get_OldIndex()
extern "C"  int32_t CollectionMoveEvent_1_get_OldIndex_m1808831945_gshared (CollectionMoveEvent_1_t2448589246 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (int32_t)__this->get_U3COldIndexU3Ek__BackingField_0();
		return L_0;
	}
}
// System.Void UniRx.CollectionMoveEvent`1<UniRx.Tuple`2<System.Object,System.Object>>::set_OldIndex(System.Int32)
extern "C"  void CollectionMoveEvent_1_set_OldIndex_m3813399748_gshared (CollectionMoveEvent_1_t2448589246 * __this, int32_t ___value0, const MethodInfo* method)
{
	{
		int32_t L_0 = ___value0;
		__this->set_U3COldIndexU3Ek__BackingField_0(L_0);
		return;
	}
}
// System.Int32 UniRx.CollectionMoveEvent`1<UniRx.Tuple`2<System.Object,System.Object>>::get_NewIndex()
extern "C"  int32_t CollectionMoveEvent_1_get_NewIndex_m526282800_gshared (CollectionMoveEvent_1_t2448589246 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (int32_t)__this->get_U3CNewIndexU3Ek__BackingField_1();
		return L_0;
	}
}
// System.Void UniRx.CollectionMoveEvent`1<UniRx.Tuple`2<System.Object,System.Object>>::set_NewIndex(System.Int32)
extern "C"  void CollectionMoveEvent_1_set_NewIndex_m899306667_gshared (CollectionMoveEvent_1_t2448589246 * __this, int32_t ___value0, const MethodInfo* method)
{
	{
		int32_t L_0 = ___value0;
		__this->set_U3CNewIndexU3Ek__BackingField_1(L_0);
		return;
	}
}
// T UniRx.CollectionMoveEvent`1<UniRx.Tuple`2<System.Object,System.Object>>::get_Value()
extern "C"  Tuple_2_t369261819  CollectionMoveEvent_1_get_Value_m2394553534_gshared (CollectionMoveEvent_1_t2448589246 * __this, const MethodInfo* method)
{
	{
		Tuple_2_t369261819  L_0 = (Tuple_2_t369261819 )__this->get_U3CValueU3Ek__BackingField_2();
		return L_0;
	}
}
// System.Void UniRx.CollectionMoveEvent`1<UniRx.Tuple`2<System.Object,System.Object>>::set_Value(T)
extern "C"  void CollectionMoveEvent_1_set_Value_m1426460147_gshared (CollectionMoveEvent_1_t2448589246 * __this, Tuple_2_t369261819  ___value0, const MethodInfo* method)
{
	{
		Tuple_2_t369261819  L_0 = ___value0;
		__this->set_U3CValueU3Ek__BackingField_2(L_0);
		return;
	}
}
// System.String UniRx.CollectionMoveEvent`1<UniRx.Tuple`2<System.Object,System.Object>>::ToString()
extern Il2CppClass* Int32_t2847414787_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral272725123;
extern const uint32_t CollectionMoveEvent_1_ToString_m3719328820_MetadataUsageId;
extern "C"  String_t* CollectionMoveEvent_1_ToString_m3719328820_gshared (CollectionMoveEvent_1_t2448589246 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (CollectionMoveEvent_1_ToString_m3719328820_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = ((  int32_t (*) (CollectionMoveEvent_1_t2448589246 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)((CollectionMoveEvent_1_t2448589246 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		int32_t L_1 = L_0;
		Il2CppObject * L_2 = Box(Int32_t2847414787_il2cpp_TypeInfo_var, &L_1);
		int32_t L_3 = ((  int32_t (*) (CollectionMoveEvent_1_t2448589246 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)((CollectionMoveEvent_1_t2448589246 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		int32_t L_4 = L_3;
		Il2CppObject * L_5 = Box(Int32_t2847414787_il2cpp_TypeInfo_var, &L_4);
		Tuple_2_t369261819  L_6 = ((  Tuple_2_t369261819  (*) (CollectionMoveEvent_1_t2448589246 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->method)((CollectionMoveEvent_1_t2448589246 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		Tuple_2_t369261819  L_7 = L_6;
		Il2CppObject * L_8 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6), &L_7);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_9 = String_Format_m3928391288(NULL /*static, unused*/, (String_t*)_stringLiteral272725123, (Il2CppObject *)L_2, (Il2CppObject *)L_5, (Il2CppObject *)L_8, /*hidden argument*/NULL);
		return L_9;
	}
}
// System.Int32 UniRx.CollectionMoveEvent`1<UniRx.Tuple`2<System.Object,System.Object>>::GetHashCode()
extern "C"  int32_t CollectionMoveEvent_1_GetHashCode_m1029581758_gshared (CollectionMoveEvent_1_t2448589246 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		int32_t L_0 = ((  int32_t (*) (CollectionMoveEvent_1_t2448589246 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)((CollectionMoveEvent_1_t2448589246 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		V_0 = (int32_t)L_0;
		int32_t L_1 = Int32_GetHashCode_m3396943446((int32_t*)(&V_0), /*hidden argument*/NULL);
		int32_t L_2 = ((  int32_t (*) (CollectionMoveEvent_1_t2448589246 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)((CollectionMoveEvent_1_t2448589246 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		V_1 = (int32_t)L_2;
		int32_t L_3 = Int32_GetHashCode_m3396943446((int32_t*)(&V_1), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8));
		EqualityComparer_1_t4098619765 * L_4 = ((  EqualityComparer_1_t4098619765 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7)->method)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7));
		Tuple_2_t369261819  L_5 = ((  Tuple_2_t369261819  (*) (CollectionMoveEvent_1_t2448589246 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->method)((CollectionMoveEvent_1_t2448589246 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		NullCheck((EqualityComparer_1_t4098619765 *)L_4);
		int32_t L_6 = VirtFuncInvoker1< int32_t, Tuple_2_t369261819  >::Invoke(8 /* System.Int32 System.Collections.Generic.EqualityComparer`1<UniRx.Tuple`2<System.Object,System.Object>>::GetHashCode(!0) */, (EqualityComparer_1_t4098619765 *)L_4, (Tuple_2_t369261819 )L_5);
		return ((int32_t)((int32_t)((int32_t)((int32_t)L_1^(int32_t)((int32_t)((int32_t)L_3<<(int32_t)2))))^(int32_t)((int32_t)((int32_t)L_6>>(int32_t)2))));
	}
}
// System.Boolean UniRx.CollectionMoveEvent`1<UniRx.Tuple`2<System.Object,System.Object>>::Equals(UniRx.CollectionMoveEvent`1<T>)
extern "C"  bool CollectionMoveEvent_1_Equals_m2890172940_gshared (CollectionMoveEvent_1_t2448589246 * __this, CollectionMoveEvent_1_t2448589246  ___other0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t G_B4_0 = 0;
	{
		int32_t L_0 = ((  int32_t (*) (CollectionMoveEvent_1_t2448589246 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)((CollectionMoveEvent_1_t2448589246 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		V_0 = (int32_t)L_0;
		int32_t L_1 = ((  int32_t (*) (CollectionMoveEvent_1_t2448589246 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)((CollectionMoveEvent_1_t2448589246 *)(&___other0), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		bool L_2 = Int32_Equals_m3849884467((int32_t*)(&V_0), (int32_t)L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_004d;
		}
	}
	{
		int32_t L_3 = ((  int32_t (*) (CollectionMoveEvent_1_t2448589246 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)((CollectionMoveEvent_1_t2448589246 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		V_1 = (int32_t)L_3;
		int32_t L_4 = ((  int32_t (*) (CollectionMoveEvent_1_t2448589246 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)((CollectionMoveEvent_1_t2448589246 *)(&___other0), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		bool L_5 = Int32_Equals_m3849884467((int32_t*)(&V_1), (int32_t)L_4, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_004d;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8));
		EqualityComparer_1_t4098619765 * L_6 = ((  EqualityComparer_1_t4098619765 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7)->method)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7));
		Tuple_2_t369261819  L_7 = ((  Tuple_2_t369261819  (*) (CollectionMoveEvent_1_t2448589246 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->method)((CollectionMoveEvent_1_t2448589246 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		Tuple_2_t369261819  L_8 = ((  Tuple_2_t369261819  (*) (CollectionMoveEvent_1_t2448589246 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->method)((CollectionMoveEvent_1_t2448589246 *)(&___other0), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		NullCheck((EqualityComparer_1_t4098619765 *)L_6);
		bool L_9 = VirtFuncInvoker2< bool, Tuple_2_t369261819 , Tuple_2_t369261819  >::Invoke(9 /* System.Boolean System.Collections.Generic.EqualityComparer`1<UniRx.Tuple`2<System.Object,System.Object>>::Equals(!0,!0) */, (EqualityComparer_1_t4098619765 *)L_6, (Tuple_2_t369261819 )L_7, (Tuple_2_t369261819 )L_8);
		G_B4_0 = ((int32_t)(L_9));
		goto IL_004e;
	}

IL_004d:
	{
		G_B4_0 = 0;
	}

IL_004e:
	{
		return (bool)G_B4_0;
	}
}
// System.Void UniRx.CollectionRemoveEvent`1<System.Object>::.ctor(System.Int32,T)
extern "C"  void CollectionRemoveEvent_1__ctor_m1246874125_gshared (CollectionRemoveEvent_1_t242637724 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method)
{
	{
		int32_t L_0 = ___index0;
		((  void (*) (CollectionRemoveEvent_1_t242637724 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((CollectionRemoveEvent_1_t242637724 *)__this, (int32_t)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		Il2CppObject * L_1 = ___value1;
		((  void (*) (CollectionRemoveEvent_1_t242637724 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)((CollectionRemoveEvent_1_t242637724 *)__this, (Il2CppObject *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		return;
	}
}
// System.Int32 UniRx.CollectionRemoveEvent`1<System.Object>::get_Index()
extern "C"  int32_t CollectionRemoveEvent_1_get_Index_m3125150261_gshared (CollectionRemoveEvent_1_t242637724 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (int32_t)__this->get_U3CIndexU3Ek__BackingField_0();
		return L_0;
	}
}
// System.Void UniRx.CollectionRemoveEvent`1<System.Object>::set_Index(System.Int32)
extern "C"  void CollectionRemoveEvent_1_set_Index_m2755269448_gshared (CollectionRemoveEvent_1_t242637724 * __this, int32_t ___value0, const MethodInfo* method)
{
	{
		int32_t L_0 = ___value0;
		__this->set_U3CIndexU3Ek__BackingField_0(L_0);
		return;
	}
}
// T UniRx.CollectionRemoveEvent`1<System.Object>::get_Value()
extern "C"  Il2CppObject * CollectionRemoveEvent_1_get_Value_m432042667_gshared (CollectionRemoveEvent_1_t242637724 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_U3CValueU3Ek__BackingField_1();
		return L_0;
	}
}
// System.Void UniRx.CollectionRemoveEvent`1<System.Object>::set_Value(T)
extern "C"  void CollectionRemoveEvent_1_set_Value_m2243992264_gshared (CollectionRemoveEvent_1_t242637724 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___value0;
		__this->set_U3CValueU3Ek__BackingField_1(L_0);
		return;
	}
}
// System.String UniRx.CollectionRemoveEvent`1<System.Object>::ToString()
extern Il2CppClass* Int32_t2847414787_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2032474014;
extern const uint32_t CollectionRemoveEvent_1_ToString_m676615279_MetadataUsageId;
extern "C"  String_t* CollectionRemoveEvent_1_ToString_m676615279_gshared (CollectionRemoveEvent_1_t242637724 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (CollectionRemoveEvent_1_ToString_m676615279_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = ((  int32_t (*) (CollectionRemoveEvent_1_t242637724 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)((CollectionRemoveEvent_1_t242637724 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		int32_t L_1 = L_0;
		Il2CppObject * L_2 = Box(Int32_t2847414787_il2cpp_TypeInfo_var, &L_1);
		Il2CppObject * L_3 = ((  Il2CppObject * (*) (CollectionRemoveEvent_1_t242637724 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)((CollectionRemoveEvent_1_t242637724 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_4 = String_Format_m2398979370(NULL /*static, unused*/, (String_t*)_stringLiteral2032474014, (Il2CppObject *)L_2, (Il2CppObject *)L_3, /*hidden argument*/NULL);
		return L_4;
	}
}
// System.Int32 UniRx.CollectionRemoveEvent`1<System.Object>::GetHashCode()
extern "C"  int32_t CollectionRemoveEvent_1_GetHashCode_m81139805_gshared (CollectionRemoveEvent_1_t242637724 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		int32_t L_0 = ((  int32_t (*) (CollectionRemoveEvent_1_t242637724 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)((CollectionRemoveEvent_1_t242637724 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		V_0 = (int32_t)L_0;
		int32_t L_1 = Int32_GetHashCode_m3396943446((int32_t*)(&V_0), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		EqualityComparer_1_t271497070 * L_2 = ((  EqualityComparer_1_t271497070 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->method)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		Il2CppObject * L_3 = ((  Il2CppObject * (*) (CollectionRemoveEvent_1_t242637724 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)((CollectionRemoveEvent_1_t242637724 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		NullCheck((EqualityComparer_1_t271497070 *)L_2);
		int32_t L_4 = VirtFuncInvoker1< int32_t, Il2CppObject * >::Invoke(8 /* System.Int32 System.Collections.Generic.EqualityComparer`1<System.Object>::GetHashCode(!0) */, (EqualityComparer_1_t271497070 *)L_2, (Il2CppObject *)L_3);
		return ((int32_t)((int32_t)L_1^(int32_t)((int32_t)((int32_t)L_4<<(int32_t)2))));
	}
}
// System.Boolean UniRx.CollectionRemoveEvent`1<System.Object>::Equals(UniRx.CollectionRemoveEvent`1<T>)
extern "C"  bool CollectionRemoveEvent_1_Equals_m376446406_gshared (CollectionRemoveEvent_1_t242637724 * __this, CollectionRemoveEvent_1_t242637724  ___other0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B3_0 = 0;
	{
		int32_t L_0 = ((  int32_t (*) (CollectionRemoveEvent_1_t242637724 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)((CollectionRemoveEvent_1_t242637724 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		V_0 = (int32_t)L_0;
		int32_t L_1 = ((  int32_t (*) (CollectionRemoveEvent_1_t242637724 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)((CollectionRemoveEvent_1_t242637724 *)(&___other0), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		bool L_2 = Int32_Equals_m3849884467((int32_t*)(&V_0), (int32_t)L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0033;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		EqualityComparer_1_t271497070 * L_3 = ((  EqualityComparer_1_t271497070 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->method)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		Il2CppObject * L_4 = ((  Il2CppObject * (*) (CollectionRemoveEvent_1_t242637724 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)((CollectionRemoveEvent_1_t242637724 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		Il2CppObject * L_5 = ((  Il2CppObject * (*) (CollectionRemoveEvent_1_t242637724 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)((CollectionRemoveEvent_1_t242637724 *)(&___other0), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		NullCheck((EqualityComparer_1_t271497070 *)L_3);
		bool L_6 = VirtFuncInvoker2< bool, Il2CppObject *, Il2CppObject * >::Invoke(9 /* System.Boolean System.Collections.Generic.EqualityComparer`1<System.Object>::Equals(!0,!0) */, (EqualityComparer_1_t271497070 *)L_3, (Il2CppObject *)L_4, (Il2CppObject *)L_5);
		G_B3_0 = ((int32_t)(L_6));
		goto IL_0034;
	}

IL_0033:
	{
		G_B3_0 = 0;
	}

IL_0034:
	{
		return (bool)G_B3_0;
	}
}
// System.Void UniRx.CollectionRemoveEvent`1<UniRx.Tuple`2<System.Object,System.Object>>::.ctor(System.Int32,T)
extern "C"  void CollectionRemoveEvent_1__ctor_m570772917_gshared (CollectionRemoveEvent_1_t4069760419 * __this, int32_t ___index0, Tuple_2_t369261819  ___value1, const MethodInfo* method)
{
	{
		int32_t L_0 = ___index0;
		((  void (*) (CollectionRemoveEvent_1_t4069760419 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((CollectionRemoveEvent_1_t4069760419 *)__this, (int32_t)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		Tuple_2_t369261819  L_1 = ___value1;
		((  void (*) (CollectionRemoveEvent_1_t4069760419 *, Tuple_2_t369261819 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)((CollectionRemoveEvent_1_t4069760419 *)__this, (Tuple_2_t369261819 )L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		return;
	}
}
// System.Int32 UniRx.CollectionRemoveEvent`1<UniRx.Tuple`2<System.Object,System.Object>>::get_Index()
extern "C"  int32_t CollectionRemoveEvent_1_get_Index_m3875147401_gshared (CollectionRemoveEvent_1_t4069760419 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (int32_t)__this->get_U3CIndexU3Ek__BackingField_0();
		return L_0;
	}
}
// System.Void UniRx.CollectionRemoveEvent`1<UniRx.Tuple`2<System.Object,System.Object>>::set_Index(System.Int32)
extern "C"  void CollectionRemoveEvent_1_set_Index_m1562070256_gshared (CollectionRemoveEvent_1_t4069760419 * __this, int32_t ___value0, const MethodInfo* method)
{
	{
		int32_t L_0 = ___value0;
		__this->set_U3CIndexU3Ek__BackingField_0(L_0);
		return;
	}
}
// T UniRx.CollectionRemoveEvent`1<UniRx.Tuple`2<System.Object,System.Object>>::get_Value()
extern "C"  Tuple_2_t369261819  CollectionRemoveEvent_1_get_Value_m3444388657_gshared (CollectionRemoveEvent_1_t4069760419 * __this, const MethodInfo* method)
{
	{
		Tuple_2_t369261819  L_0 = (Tuple_2_t369261819 )__this->get_U3CValueU3Ek__BackingField_1();
		return L_0;
	}
}
// System.Void UniRx.CollectionRemoveEvent`1<UniRx.Tuple`2<System.Object,System.Object>>::set_Value(T)
extern "C"  void CollectionRemoveEvent_1_set_Value_m2422397472_gshared (CollectionRemoveEvent_1_t4069760419 * __this, Tuple_2_t369261819  ___value0, const MethodInfo* method)
{
	{
		Tuple_2_t369261819  L_0 = ___value0;
		__this->set_U3CValueU3Ek__BackingField_1(L_0);
		return;
	}
}
// System.String UniRx.CollectionRemoveEvent`1<UniRx.Tuple`2<System.Object,System.Object>>::ToString()
extern Il2CppClass* Int32_t2847414787_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2032474014;
extern const uint32_t CollectionRemoveEvent_1_ToString_m1076386913_MetadataUsageId;
extern "C"  String_t* CollectionRemoveEvent_1_ToString_m1076386913_gshared (CollectionRemoveEvent_1_t4069760419 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (CollectionRemoveEvent_1_ToString_m1076386913_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = ((  int32_t (*) (CollectionRemoveEvent_1_t4069760419 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)((CollectionRemoveEvent_1_t4069760419 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		int32_t L_1 = L_0;
		Il2CppObject * L_2 = Box(Int32_t2847414787_il2cpp_TypeInfo_var, &L_1);
		Tuple_2_t369261819  L_3 = ((  Tuple_2_t369261819  (*) (CollectionRemoveEvent_1_t4069760419 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)((CollectionRemoveEvent_1_t4069760419 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		Tuple_2_t369261819  L_4 = L_3;
		Il2CppObject * L_5 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4), &L_4);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_6 = String_Format_m2398979370(NULL /*static, unused*/, (String_t*)_stringLiteral2032474014, (Il2CppObject *)L_2, (Il2CppObject *)L_5, /*hidden argument*/NULL);
		return L_6;
	}
}
// System.Int32 UniRx.CollectionRemoveEvent`1<UniRx.Tuple`2<System.Object,System.Object>>::GetHashCode()
extern "C"  int32_t CollectionRemoveEvent_1_GetHashCode_m3568852913_gshared (CollectionRemoveEvent_1_t4069760419 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		int32_t L_0 = ((  int32_t (*) (CollectionRemoveEvent_1_t4069760419 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)((CollectionRemoveEvent_1_t4069760419 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		V_0 = (int32_t)L_0;
		int32_t L_1 = Int32_GetHashCode_m3396943446((int32_t*)(&V_0), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		EqualityComparer_1_t4098619765 * L_2 = ((  EqualityComparer_1_t4098619765 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->method)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		Tuple_2_t369261819  L_3 = ((  Tuple_2_t369261819  (*) (CollectionRemoveEvent_1_t4069760419 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)((CollectionRemoveEvent_1_t4069760419 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		NullCheck((EqualityComparer_1_t4098619765 *)L_2);
		int32_t L_4 = VirtFuncInvoker1< int32_t, Tuple_2_t369261819  >::Invoke(8 /* System.Int32 System.Collections.Generic.EqualityComparer`1<UniRx.Tuple`2<System.Object,System.Object>>::GetHashCode(!0) */, (EqualityComparer_1_t4098619765 *)L_2, (Tuple_2_t369261819 )L_3);
		return ((int32_t)((int32_t)L_1^(int32_t)((int32_t)((int32_t)L_4<<(int32_t)2))));
	}
}
// System.Boolean UniRx.CollectionRemoveEvent`1<UniRx.Tuple`2<System.Object,System.Object>>::Equals(UniRx.CollectionRemoveEvent`1<T>)
extern "C"  bool CollectionRemoveEvent_1_Equals_m2262826342_gshared (CollectionRemoveEvent_1_t4069760419 * __this, CollectionRemoveEvent_1_t4069760419  ___other0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B3_0 = 0;
	{
		int32_t L_0 = ((  int32_t (*) (CollectionRemoveEvent_1_t4069760419 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)((CollectionRemoveEvent_1_t4069760419 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		V_0 = (int32_t)L_0;
		int32_t L_1 = ((  int32_t (*) (CollectionRemoveEvent_1_t4069760419 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)((CollectionRemoveEvent_1_t4069760419 *)(&___other0), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		bool L_2 = Int32_Equals_m3849884467((int32_t*)(&V_0), (int32_t)L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0033;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		EqualityComparer_1_t4098619765 * L_3 = ((  EqualityComparer_1_t4098619765 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->method)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		Tuple_2_t369261819  L_4 = ((  Tuple_2_t369261819  (*) (CollectionRemoveEvent_1_t4069760419 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)((CollectionRemoveEvent_1_t4069760419 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		Tuple_2_t369261819  L_5 = ((  Tuple_2_t369261819  (*) (CollectionRemoveEvent_1_t4069760419 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)((CollectionRemoveEvent_1_t4069760419 *)(&___other0), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		NullCheck((EqualityComparer_1_t4098619765 *)L_3);
		bool L_6 = VirtFuncInvoker2< bool, Tuple_2_t369261819 , Tuple_2_t369261819  >::Invoke(9 /* System.Boolean System.Collections.Generic.EqualityComparer`1<UniRx.Tuple`2<System.Object,System.Object>>::Equals(!0,!0) */, (EqualityComparer_1_t4098619765 *)L_3, (Tuple_2_t369261819 )L_4, (Tuple_2_t369261819 )L_5);
		G_B3_0 = ((int32_t)(L_6));
		goto IL_0034;
	}

IL_0033:
	{
		G_B3_0 = 0;
	}

IL_0034:
	{
		return (bool)G_B3_0;
	}
}
// System.Void UniRx.CollectionReplaceEvent`1<System.Object>::.ctor(System.Int32,T,T)
extern "C"  void CollectionReplaceEvent_1__ctor_m3222019849_gshared (CollectionReplaceEvent_1_t1841750536 * __this, int32_t ___index0, Il2CppObject * ___oldValue1, Il2CppObject * ___newValue2, const MethodInfo* method)
{
	{
		int32_t L_0 = ___index0;
		((  void (*) (CollectionReplaceEvent_1_t1841750536 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((CollectionReplaceEvent_1_t1841750536 *)__this, (int32_t)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		Il2CppObject * L_1 = ___oldValue1;
		((  void (*) (CollectionReplaceEvent_1_t1841750536 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)((CollectionReplaceEvent_1_t1841750536 *)__this, (Il2CppObject *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		Il2CppObject * L_2 = ___newValue2;
		((  void (*) (CollectionReplaceEvent_1_t1841750536 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)((CollectionReplaceEvent_1_t1841750536 *)__this, (Il2CppObject *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return;
	}
}
// System.Int32 UniRx.CollectionReplaceEvent`1<System.Object>::get_Index()
extern "C"  int32_t CollectionReplaceEvent_1_get_Index_m128297605_gshared (CollectionReplaceEvent_1_t1841750536 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (int32_t)__this->get_U3CIndexU3Ek__BackingField_0();
		return L_0;
	}
}
// System.Void UniRx.CollectionReplaceEvent`1<System.Object>::set_Index(System.Int32)
extern "C"  void CollectionReplaceEvent_1_set_Index_m1732157932_gshared (CollectionReplaceEvent_1_t1841750536 * __this, int32_t ___value0, const MethodInfo* method)
{
	{
		int32_t L_0 = ___value0;
		__this->set_U3CIndexU3Ek__BackingField_0(L_0);
		return;
	}
}
// T UniRx.CollectionReplaceEvent`1<System.Object>::get_OldValue()
extern "C"  Il2CppObject * CollectionReplaceEvent_1_get_OldValue_m609402800_gshared (CollectionReplaceEvent_1_t1841750536 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_U3COldValueU3Ek__BackingField_1();
		return L_0;
	}
}
// System.Void UniRx.CollectionReplaceEvent`1<System.Object>::set_OldValue(T)
extern "C"  void CollectionReplaceEvent_1_set_OldValue_m1043865723_gshared (CollectionReplaceEvent_1_t1841750536 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___value0;
		__this->set_U3COldValueU3Ek__BackingField_1(L_0);
		return;
	}
}
// T UniRx.CollectionReplaceEvent`1<System.Object>::get_NewValue()
extern "C"  Il2CppObject * CollectionReplaceEvent_1_get_NewValue_m3621820951_gshared (CollectionReplaceEvent_1_t1841750536 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_U3CNewValueU3Ek__BackingField_2();
		return L_0;
	}
}
// System.Void UniRx.CollectionReplaceEvent`1<System.Object>::set_NewValue(T)
extern "C"  void CollectionReplaceEvent_1_set_NewValue_m4234515188_gshared (CollectionReplaceEvent_1_t1841750536 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___value0;
		__this->set_U3CNewValueU3Ek__BackingField_2(L_0);
		return;
	}
}
// System.String UniRx.CollectionReplaceEvent`1<System.Object>::ToString()
extern Il2CppClass* Int32_t2847414787_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3696740198;
extern const uint32_t CollectionReplaceEvent_1_ToString_m3965201253_MetadataUsageId;
extern "C"  String_t* CollectionReplaceEvent_1_ToString_m3965201253_gshared (CollectionReplaceEvent_1_t1841750536 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (CollectionReplaceEvent_1_ToString_m3965201253_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = ((  int32_t (*) (CollectionReplaceEvent_1_t1841750536 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)((CollectionReplaceEvent_1_t1841750536 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		int32_t L_1 = L_0;
		Il2CppObject * L_2 = Box(Int32_t2847414787_il2cpp_TypeInfo_var, &L_1);
		Il2CppObject * L_3 = ((  Il2CppObject * (*) (CollectionReplaceEvent_1_t1841750536 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)((CollectionReplaceEvent_1_t1841750536 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		Il2CppObject * L_4 = ((  Il2CppObject * (*) (CollectionReplaceEvent_1_t1841750536 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6)->method)((CollectionReplaceEvent_1_t1841750536 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_5 = String_Format_m3928391288(NULL /*static, unused*/, (String_t*)_stringLiteral3696740198, (Il2CppObject *)L_2, (Il2CppObject *)L_3, (Il2CppObject *)L_4, /*hidden argument*/NULL);
		return L_5;
	}
}
// System.Int32 UniRx.CollectionReplaceEvent`1<System.Object>::GetHashCode()
extern "C"  int32_t CollectionReplaceEvent_1_GetHashCode_m2028793005_gshared (CollectionReplaceEvent_1_t1841750536 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		int32_t L_0 = ((  int32_t (*) (CollectionReplaceEvent_1_t1841750536 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)((CollectionReplaceEvent_1_t1841750536 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		V_0 = (int32_t)L_0;
		int32_t L_1 = Int32_GetHashCode_m3396943446((int32_t*)(&V_0), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8));
		EqualityComparer_1_t271497070 * L_2 = ((  EqualityComparer_1_t271497070 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7)->method)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7));
		Il2CppObject * L_3 = ((  Il2CppObject * (*) (CollectionReplaceEvent_1_t1841750536 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)((CollectionReplaceEvent_1_t1841750536 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		NullCheck((EqualityComparer_1_t271497070 *)L_2);
		int32_t L_4 = VirtFuncInvoker1< int32_t, Il2CppObject * >::Invoke(8 /* System.Int32 System.Collections.Generic.EqualityComparer`1<System.Object>::GetHashCode(!0) */, (EqualityComparer_1_t271497070 *)L_2, (Il2CppObject *)L_3);
		EqualityComparer_1_t271497070 * L_5 = ((  EqualityComparer_1_t271497070 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7)->method)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7));
		Il2CppObject * L_6 = ((  Il2CppObject * (*) (CollectionReplaceEvent_1_t1841750536 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6)->method)((CollectionReplaceEvent_1_t1841750536 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		NullCheck((EqualityComparer_1_t271497070 *)L_5);
		int32_t L_7 = VirtFuncInvoker1< int32_t, Il2CppObject * >::Invoke(8 /* System.Int32 System.Collections.Generic.EqualityComparer`1<System.Object>::GetHashCode(!0) */, (EqualityComparer_1_t271497070 *)L_5, (Il2CppObject *)L_6);
		return ((int32_t)((int32_t)((int32_t)((int32_t)L_1^(int32_t)((int32_t)((int32_t)L_4<<(int32_t)2))))^(int32_t)((int32_t)((int32_t)L_7>>(int32_t)2))));
	}
}
// System.Boolean UniRx.CollectionReplaceEvent`1<System.Object>::Equals(UniRx.CollectionReplaceEvent`1<T>)
extern "C"  bool CollectionReplaceEvent_1_Equals_m2739738362_gshared (CollectionReplaceEvent_1_t1841750536 * __this, CollectionReplaceEvent_1_t1841750536  ___other0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B4_0 = 0;
	{
		int32_t L_0 = ((  int32_t (*) (CollectionReplaceEvent_1_t1841750536 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)((CollectionReplaceEvent_1_t1841750536 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		V_0 = (int32_t)L_0;
		int32_t L_1 = ((  int32_t (*) (CollectionReplaceEvent_1_t1841750536 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)((CollectionReplaceEvent_1_t1841750536 *)(&___other0), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		bool L_2 = Int32_Equals_m3849884467((int32_t*)(&V_0), (int32_t)L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_004f;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8));
		EqualityComparer_1_t271497070 * L_3 = ((  EqualityComparer_1_t271497070 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7)->method)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7));
		Il2CppObject * L_4 = ((  Il2CppObject * (*) (CollectionReplaceEvent_1_t1841750536 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)((CollectionReplaceEvent_1_t1841750536 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		Il2CppObject * L_5 = ((  Il2CppObject * (*) (CollectionReplaceEvent_1_t1841750536 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)((CollectionReplaceEvent_1_t1841750536 *)(&___other0), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		NullCheck((EqualityComparer_1_t271497070 *)L_3);
		bool L_6 = VirtFuncInvoker2< bool, Il2CppObject *, Il2CppObject * >::Invoke(9 /* System.Boolean System.Collections.Generic.EqualityComparer`1<System.Object>::Equals(!0,!0) */, (EqualityComparer_1_t271497070 *)L_3, (Il2CppObject *)L_4, (Il2CppObject *)L_5);
		if (!L_6)
		{
			goto IL_004f;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8));
		EqualityComparer_1_t271497070 * L_7 = ((  EqualityComparer_1_t271497070 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7)->method)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7));
		Il2CppObject * L_8 = ((  Il2CppObject * (*) (CollectionReplaceEvent_1_t1841750536 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6)->method)((CollectionReplaceEvent_1_t1841750536 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		Il2CppObject * L_9 = ((  Il2CppObject * (*) (CollectionReplaceEvent_1_t1841750536 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6)->method)((CollectionReplaceEvent_1_t1841750536 *)(&___other0), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		NullCheck((EqualityComparer_1_t271497070 *)L_7);
		bool L_10 = VirtFuncInvoker2< bool, Il2CppObject *, Il2CppObject * >::Invoke(9 /* System.Boolean System.Collections.Generic.EqualityComparer`1<System.Object>::Equals(!0,!0) */, (EqualityComparer_1_t271497070 *)L_7, (Il2CppObject *)L_8, (Il2CppObject *)L_9);
		G_B4_0 = ((int32_t)(L_10));
		goto IL_0050;
	}

IL_004f:
	{
		G_B4_0 = 0;
	}

IL_0050:
	{
		return (bool)G_B4_0;
	}
}
// System.Void UniRx.CollectionReplaceEvent`1<UniRx.Tuple`2<System.Object,System.Object>>::.ctor(System.Int32,T,T)
extern "C"  void CollectionReplaceEvent_1__ctor_m3073699305_gshared (CollectionReplaceEvent_1_t1373905935 * __this, int32_t ___index0, Tuple_2_t369261819  ___oldValue1, Tuple_2_t369261819  ___newValue2, const MethodInfo* method)
{
	{
		int32_t L_0 = ___index0;
		((  void (*) (CollectionReplaceEvent_1_t1373905935 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((CollectionReplaceEvent_1_t1373905935 *)__this, (int32_t)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		Tuple_2_t369261819  L_1 = ___oldValue1;
		((  void (*) (CollectionReplaceEvent_1_t1373905935 *, Tuple_2_t369261819 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)((CollectionReplaceEvent_1_t1373905935 *)__this, (Tuple_2_t369261819 )L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		Tuple_2_t369261819  L_2 = ___newValue2;
		((  void (*) (CollectionReplaceEvent_1_t1373905935 *, Tuple_2_t369261819 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)((CollectionReplaceEvent_1_t1373905935 *)__this, (Tuple_2_t369261819 )L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return;
	}
}
// System.Int32 UniRx.CollectionReplaceEvent`1<UniRx.Tuple`2<System.Object,System.Object>>::get_Index()
extern "C"  int32_t CollectionReplaceEvent_1_get_Index_m4036036665_gshared (CollectionReplaceEvent_1_t1373905935 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (int32_t)__this->get_U3CIndexU3Ek__BackingField_0();
		return L_0;
	}
}
// System.Void UniRx.CollectionReplaceEvent`1<UniRx.Tuple`2<System.Object,System.Object>>::set_Index(System.Int32)
extern "C"  void CollectionReplaceEvent_1_set_Index_m1583837388_gshared (CollectionReplaceEvent_1_t1373905935 * __this, int32_t ___value0, const MethodInfo* method)
{
	{
		int32_t L_0 = ___value0;
		__this->set_U3CIndexU3Ek__BackingField_0(L_0);
		return;
	}
}
// T UniRx.CollectionReplaceEvent`1<UniRx.Tuple`2<System.Object,System.Object>>::get_OldValue()
extern "C"  Tuple_2_t369261819  CollectionReplaceEvent_1_get_OldValue_m2151637166_gshared (CollectionReplaceEvent_1_t1373905935 * __this, const MethodInfo* method)
{
	{
		Tuple_2_t369261819  L_0 = (Tuple_2_t369261819 )__this->get_U3COldValueU3Ek__BackingField_1();
		return L_0;
	}
}
// System.Void UniRx.CollectionReplaceEvent`1<UniRx.Tuple`2<System.Object,System.Object>>::set_OldValue(T)
extern "C"  void CollectionReplaceEvent_1_set_OldValue_m2601373531_gshared (CollectionReplaceEvent_1_t1373905935 * __this, Tuple_2_t369261819  ___value0, const MethodInfo* method)
{
	{
		Tuple_2_t369261819  L_0 = ___value0;
		__this->set_U3COldValueU3Ek__BackingField_1(L_0);
		return;
	}
}
// T UniRx.CollectionReplaceEvent`1<UniRx.Tuple`2<System.Object,System.Object>>::get_NewValue()
extern "C"  Tuple_2_t369261819  CollectionReplaceEvent_1_get_NewValue_m869088021_gshared (CollectionReplaceEvent_1_t1373905935 * __this, const MethodInfo* method)
{
	{
		Tuple_2_t369261819  L_0 = (Tuple_2_t369261819 )__this->get_U3CNewValueU3Ek__BackingField_2();
		return L_0;
	}
}
// System.Void UniRx.CollectionReplaceEvent`1<UniRx.Tuple`2<System.Object,System.Object>>::set_NewValue(T)
extern "C"  void CollectionReplaceEvent_1_set_NewValue_m1497055700_gshared (CollectionReplaceEvent_1_t1373905935 * __this, Tuple_2_t369261819  ___value0, const MethodInfo* method)
{
	{
		Tuple_2_t369261819  L_0 = ___value0;
		__this->set_U3CNewValueU3Ek__BackingField_2(L_0);
		return;
	}
}
// System.String UniRx.CollectionReplaceEvent`1<UniRx.Tuple`2<System.Object,System.Object>>::ToString()
extern Il2CppClass* Int32_t2847414787_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3696740198;
extern const uint32_t CollectionReplaceEvent_1_ToString_m4234752299_MetadataUsageId;
extern "C"  String_t* CollectionReplaceEvent_1_ToString_m4234752299_gshared (CollectionReplaceEvent_1_t1373905935 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (CollectionReplaceEvent_1_ToString_m4234752299_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = ((  int32_t (*) (CollectionReplaceEvent_1_t1373905935 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)((CollectionReplaceEvent_1_t1373905935 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		int32_t L_1 = L_0;
		Il2CppObject * L_2 = Box(Int32_t2847414787_il2cpp_TypeInfo_var, &L_1);
		Tuple_2_t369261819  L_3 = ((  Tuple_2_t369261819  (*) (CollectionReplaceEvent_1_t1373905935 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)((CollectionReplaceEvent_1_t1373905935 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		Tuple_2_t369261819  L_4 = L_3;
		Il2CppObject * L_5 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5), &L_4);
		Tuple_2_t369261819  L_6 = ((  Tuple_2_t369261819  (*) (CollectionReplaceEvent_1_t1373905935 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6)->method)((CollectionReplaceEvent_1_t1373905935 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		Tuple_2_t369261819  L_7 = L_6;
		Il2CppObject * L_8 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5), &L_7);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_9 = String_Format_m3928391288(NULL /*static, unused*/, (String_t*)_stringLiteral3696740198, (Il2CppObject *)L_2, (Il2CppObject *)L_5, (Il2CppObject *)L_8, /*hidden argument*/NULL);
		return L_9;
	}
}
// System.Int32 UniRx.CollectionReplaceEvent`1<UniRx.Tuple`2<System.Object,System.Object>>::GetHashCode()
extern "C"  int32_t CollectionReplaceEvent_1_GetHashCode_m3564612961_gshared (CollectionReplaceEvent_1_t1373905935 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		int32_t L_0 = ((  int32_t (*) (CollectionReplaceEvent_1_t1373905935 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)((CollectionReplaceEvent_1_t1373905935 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		V_0 = (int32_t)L_0;
		int32_t L_1 = Int32_GetHashCode_m3396943446((int32_t*)(&V_0), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8));
		EqualityComparer_1_t4098619765 * L_2 = ((  EqualityComparer_1_t4098619765 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7)->method)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7));
		Tuple_2_t369261819  L_3 = ((  Tuple_2_t369261819  (*) (CollectionReplaceEvent_1_t1373905935 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)((CollectionReplaceEvent_1_t1373905935 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		NullCheck((EqualityComparer_1_t4098619765 *)L_2);
		int32_t L_4 = VirtFuncInvoker1< int32_t, Tuple_2_t369261819  >::Invoke(8 /* System.Int32 System.Collections.Generic.EqualityComparer`1<UniRx.Tuple`2<System.Object,System.Object>>::GetHashCode(!0) */, (EqualityComparer_1_t4098619765 *)L_2, (Tuple_2_t369261819 )L_3);
		EqualityComparer_1_t4098619765 * L_5 = ((  EqualityComparer_1_t4098619765 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7)->method)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7));
		Tuple_2_t369261819  L_6 = ((  Tuple_2_t369261819  (*) (CollectionReplaceEvent_1_t1373905935 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6)->method)((CollectionReplaceEvent_1_t1373905935 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		NullCheck((EqualityComparer_1_t4098619765 *)L_5);
		int32_t L_7 = VirtFuncInvoker1< int32_t, Tuple_2_t369261819  >::Invoke(8 /* System.Int32 System.Collections.Generic.EqualityComparer`1<UniRx.Tuple`2<System.Object,System.Object>>::GetHashCode(!0) */, (EqualityComparer_1_t4098619765 *)L_5, (Tuple_2_t369261819 )L_6);
		return ((int32_t)((int32_t)((int32_t)((int32_t)L_1^(int32_t)((int32_t)((int32_t)L_4<<(int32_t)2))))^(int32_t)((int32_t)((int32_t)L_7>>(int32_t)2))));
	}
}
// System.Boolean UniRx.CollectionReplaceEvent`1<UniRx.Tuple`2<System.Object,System.Object>>::Equals(UniRx.CollectionReplaceEvent`1<T>)
extern "C"  bool CollectionReplaceEvent_1_Equals_m2277216290_gshared (CollectionReplaceEvent_1_t1373905935 * __this, CollectionReplaceEvent_1_t1373905935  ___other0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B4_0 = 0;
	{
		int32_t L_0 = ((  int32_t (*) (CollectionReplaceEvent_1_t1373905935 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)((CollectionReplaceEvent_1_t1373905935 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		V_0 = (int32_t)L_0;
		int32_t L_1 = ((  int32_t (*) (CollectionReplaceEvent_1_t1373905935 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)((CollectionReplaceEvent_1_t1373905935 *)(&___other0), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		bool L_2 = Int32_Equals_m3849884467((int32_t*)(&V_0), (int32_t)L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_004f;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8));
		EqualityComparer_1_t4098619765 * L_3 = ((  EqualityComparer_1_t4098619765 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7)->method)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7));
		Tuple_2_t369261819  L_4 = ((  Tuple_2_t369261819  (*) (CollectionReplaceEvent_1_t1373905935 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)((CollectionReplaceEvent_1_t1373905935 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		Tuple_2_t369261819  L_5 = ((  Tuple_2_t369261819  (*) (CollectionReplaceEvent_1_t1373905935 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)((CollectionReplaceEvent_1_t1373905935 *)(&___other0), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		NullCheck((EqualityComparer_1_t4098619765 *)L_3);
		bool L_6 = VirtFuncInvoker2< bool, Tuple_2_t369261819 , Tuple_2_t369261819  >::Invoke(9 /* System.Boolean System.Collections.Generic.EqualityComparer`1<UniRx.Tuple`2<System.Object,System.Object>>::Equals(!0,!0) */, (EqualityComparer_1_t4098619765 *)L_3, (Tuple_2_t369261819 )L_4, (Tuple_2_t369261819 )L_5);
		if (!L_6)
		{
			goto IL_004f;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8));
		EqualityComparer_1_t4098619765 * L_7 = ((  EqualityComparer_1_t4098619765 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7)->method)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7));
		Tuple_2_t369261819  L_8 = ((  Tuple_2_t369261819  (*) (CollectionReplaceEvent_1_t1373905935 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6)->method)((CollectionReplaceEvent_1_t1373905935 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		Tuple_2_t369261819  L_9 = ((  Tuple_2_t369261819  (*) (CollectionReplaceEvent_1_t1373905935 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6)->method)((CollectionReplaceEvent_1_t1373905935 *)(&___other0), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		NullCheck((EqualityComparer_1_t4098619765 *)L_7);
		bool L_10 = VirtFuncInvoker2< bool, Tuple_2_t369261819 , Tuple_2_t369261819  >::Invoke(9 /* System.Boolean System.Collections.Generic.EqualityComparer`1<UniRx.Tuple`2<System.Object,System.Object>>::Equals(!0,!0) */, (EqualityComparer_1_t4098619765 *)L_7, (Tuple_2_t369261819 )L_8, (Tuple_2_t369261819 )L_9);
		G_B4_0 = ((int32_t)(L_10));
		goto IL_0050;
	}

IL_004f:
	{
		G_B4_0 = 0;
	}

IL_0050:
	{
		return (bool)G_B4_0;
	}
}
// System.Void UniRx.DictionaryAddEvent`2<System.Object,System.Object>::.ctor(TKey,TValue)
extern "C"  void DictionaryAddEvent_2__ctor_m3319540014_gshared (DictionaryAddEvent_2_t250981008 * __this, Il2CppObject * ___key0, Il2CppObject * ___value1, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___key0;
		((  void (*) (DictionaryAddEvent_2_t250981008 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((DictionaryAddEvent_2_t250981008 *)__this, (Il2CppObject *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		Il2CppObject * L_1 = ___value1;
		((  void (*) (DictionaryAddEvent_2_t250981008 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)((DictionaryAddEvent_2_t250981008 *)__this, (Il2CppObject *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		return;
	}
}
// TKey UniRx.DictionaryAddEvent`2<System.Object,System.Object>::get_Key()
extern "C"  Il2CppObject * DictionaryAddEvent_2_get_Key_m4011898708_gshared (DictionaryAddEvent_2_t250981008 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_U3CKeyU3Ek__BackingField_0();
		return L_0;
	}
}
// System.Void UniRx.DictionaryAddEvent`2<System.Object,System.Object>::set_Key(TKey)
extern "C"  void DictionaryAddEvent_2_set_Key_m1422765115_gshared (DictionaryAddEvent_2_t250981008 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___value0;
		__this->set_U3CKeyU3Ek__BackingField_0(L_0);
		return;
	}
}
// TValue UniRx.DictionaryAddEvent`2<System.Object,System.Object>::get_Value()
extern "C"  Il2CppObject * DictionaryAddEvent_2_get_Value_m3482472916_gshared (DictionaryAddEvent_2_t250981008 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_U3CValueU3Ek__BackingField_1();
		return L_0;
	}
}
// System.Void UniRx.DictionaryAddEvent`2<System.Object,System.Object>::set_Value(TValue)
extern "C"  void DictionaryAddEvent_2_set_Value_m2511497915_gshared (DictionaryAddEvent_2_t250981008 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___value0;
		__this->set_U3CValueU3Ek__BackingField_1(L_0);
		return;
	}
}
// System.String UniRx.DictionaryAddEvent`2<System.Object,System.Object>::ToString()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3592735339;
extern const uint32_t DictionaryAddEvent_2_ToString_m208558471_MetadataUsageId;
extern "C"  String_t* DictionaryAddEvent_2_ToString_m208558471_gshared (DictionaryAddEvent_2_t250981008 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DictionaryAddEvent_2_ToString_m208558471_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject * L_0 = ((  Il2CppObject * (*) (DictionaryAddEvent_2_t250981008 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)((DictionaryAddEvent_2_t250981008 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		Il2CppObject * L_1 = ((  Il2CppObject * (*) (DictionaryAddEvent_2_t250981008 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)((DictionaryAddEvent_2_t250981008 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_2 = String_Format_m2398979370(NULL /*static, unused*/, (String_t*)_stringLiteral3592735339, (Il2CppObject *)L_0, (Il2CppObject *)L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Int32 UniRx.DictionaryAddEvent`2<System.Object,System.Object>::GetHashCode()
extern "C"  int32_t DictionaryAddEvent_2_GetHashCode_m1247635339_gshared (DictionaryAddEvent_2_t250981008 * __this, const MethodInfo* method)
{
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7));
		EqualityComparer_1_t271497070 * L_0 = ((  EqualityComparer_1_t271497070 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6)->method)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		Il2CppObject * L_1 = ((  Il2CppObject * (*) (DictionaryAddEvent_2_t250981008 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)((DictionaryAddEvent_2_t250981008 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		NullCheck((EqualityComparer_1_t271497070 *)L_0);
		int32_t L_2 = VirtFuncInvoker1< int32_t, Il2CppObject * >::Invoke(8 /* System.Int32 System.Collections.Generic.EqualityComparer`1<System.Object>::GetHashCode(!0) */, (EqualityComparer_1_t271497070 *)L_0, (Il2CppObject *)L_1);
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10));
		EqualityComparer_1_t271497070 * L_3 = ((  EqualityComparer_1_t271497070 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9)->method)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9));
		Il2CppObject * L_4 = ((  Il2CppObject * (*) (DictionaryAddEvent_2_t250981008 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)((DictionaryAddEvent_2_t250981008 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		NullCheck((EqualityComparer_1_t271497070 *)L_3);
		int32_t L_5 = VirtFuncInvoker1< int32_t, Il2CppObject * >::Invoke(8 /* System.Int32 System.Collections.Generic.EqualityComparer`1<System.Object>::GetHashCode(!0) */, (EqualityComparer_1_t271497070 *)L_3, (Il2CppObject *)L_4);
		return ((int32_t)((int32_t)L_2^(int32_t)((int32_t)((int32_t)L_5<<(int32_t)2))));
	}
}
// System.Boolean UniRx.DictionaryAddEvent`2<System.Object,System.Object>::Equals(UniRx.DictionaryAddEvent`2<TKey,TValue>)
extern "C"  bool DictionaryAddEvent_2_Equals_m913248142_gshared (DictionaryAddEvent_2_t250981008 * __this, DictionaryAddEvent_2_t250981008  ___other0, const MethodInfo* method)
{
	int32_t G_B3_0 = 0;
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7));
		EqualityComparer_1_t271497070 * L_0 = ((  EqualityComparer_1_t271497070 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6)->method)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		Il2CppObject * L_1 = ((  Il2CppObject * (*) (DictionaryAddEvent_2_t250981008 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)((DictionaryAddEvent_2_t250981008 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		Il2CppObject * L_2 = ((  Il2CppObject * (*) (DictionaryAddEvent_2_t250981008 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)((DictionaryAddEvent_2_t250981008 *)(&___other0), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		NullCheck((EqualityComparer_1_t271497070 *)L_0);
		bool L_3 = VirtFuncInvoker2< bool, Il2CppObject *, Il2CppObject * >::Invoke(9 /* System.Boolean System.Collections.Generic.EqualityComparer`1<System.Object>::Equals(!0,!0) */, (EqualityComparer_1_t271497070 *)L_0, (Il2CppObject *)L_1, (Il2CppObject *)L_2);
		if (!L_3)
		{
			goto IL_0035;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10));
		EqualityComparer_1_t271497070 * L_4 = ((  EqualityComparer_1_t271497070 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9)->method)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9));
		Il2CppObject * L_5 = ((  Il2CppObject * (*) (DictionaryAddEvent_2_t250981008 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)((DictionaryAddEvent_2_t250981008 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		Il2CppObject * L_6 = ((  Il2CppObject * (*) (DictionaryAddEvent_2_t250981008 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)((DictionaryAddEvent_2_t250981008 *)(&___other0), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		NullCheck((EqualityComparer_1_t271497070 *)L_4);
		bool L_7 = VirtFuncInvoker2< bool, Il2CppObject *, Il2CppObject * >::Invoke(9 /* System.Boolean System.Collections.Generic.EqualityComparer`1<System.Object>::Equals(!0,!0) */, (EqualityComparer_1_t271497070 *)L_4, (Il2CppObject *)L_5, (Il2CppObject *)L_6);
		G_B3_0 = ((int32_t)(L_7));
		goto IL_0036;
	}

IL_0035:
	{
		G_B3_0 = 0;
	}

IL_0036:
	{
		return (bool)G_B3_0;
	}
}
// System.Void UniRx.DictionaryDisposable`2<System.Object,System.Object>::.ctor()
extern "C"  void DictionaryDisposable_2__ctor_m1064899917_gshared (DictionaryDisposable_2_t2367950693 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		Dictionary_2_t3824425150 * L_0 = (Dictionary_2_t3824425150 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		((  void (*) (Dictionary_2_t3824425150 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		__this->set_inner_1(L_0);
		return;
	}
}
// System.Void UniRx.DictionaryDisposable`2<System.Object,System.Object>::.ctor(System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C"  void DictionaryDisposable_2__ctor_m3311603524_gshared (DictionaryDisposable_2_t2367950693 * __this, Il2CppObject* ___comparer0, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		Il2CppObject* L_0 = ___comparer0;
		Dictionary_2_t3824425150 * L_1 = (Dictionary_2_t3824425150 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		((  void (*) (Dictionary_2_t3824425150 *, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)(L_1, (Il2CppObject*)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		__this->set_inner_1(L_1);
		return;
	}
}
// System.Boolean UniRx.DictionaryDisposable`2<System.Object,System.Object>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.get_IsReadOnly()
extern "C"  bool DictionaryDisposable_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m1974734417_gshared (DictionaryDisposable_2_t2367950693 * __this, const MethodInfo* method)
{
	{
		Dictionary_2_t3824425150 * L_0 = (Dictionary_2_t3824425150 *)__this->get_inner_1();
		NullCheck((Il2CppObject*)L_0);
		bool L_1 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.Generic.ICollection`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::get_IsReadOnly() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3), (Il2CppObject*)L_0);
		return L_1;
	}
}
// System.Collections.Generic.ICollection`1<TKey> UniRx.DictionaryDisposable`2<System.Object,System.Object>::System.Collections.Generic.IDictionary<TKey,TValue>.get_Keys()
extern "C"  Il2CppObject* DictionaryDisposable_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m1808589137_gshared (DictionaryDisposable_2_t2367950693 * __this, const MethodInfo* method)
{
	Dictionary_2_t3824425150 * V_0 = NULL;
	Il2CppObject* V_1 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Dictionary_2_t3824425150 * L_0 = (Dictionary_2_t3824425150 *)__this->get_inner_1();
		V_0 = (Dictionary_2_t3824425150 *)L_0;
		Dictionary_2_t3824425150 * L_1 = V_0;
		Monitor_Enter_m476686225(NULL /*static, unused*/, (Il2CppObject *)L_1, /*hidden argument*/NULL);
	}

IL_000d:
	try
	{ // begin try (depth: 1)
		{
			Dictionary_2_t3824425150 * L_2 = (Dictionary_2_t3824425150 *)__this->get_inner_1();
			NullCheck((Dictionary_2_t3824425150 *)L_2);
			KeyCollection_t1852733134 * L_3 = ((  KeyCollection_t1852733134 * (*) (Dictionary_2_t3824425150 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)((Dictionary_2_t3824425150 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
			List_1_t1634065389 * L_4 = (List_1_t1634065389 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
			((  void (*) (List_1_t1634065389 *, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6)->method)(L_4, (Il2CppObject*)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
			V_1 = (Il2CppObject*)L_4;
			IL2CPP_LEAVE(0x2F, FINALLY_0028);
		}

IL_0023:
		{
			; // IL_0023: leave IL_002f
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0028;
	}

FINALLY_0028:
	{ // begin finally (depth: 1)
		Dictionary_2_t3824425150 * L_5 = V_0;
		Monitor_Exit_m2088237919(NULL /*static, unused*/, (Il2CppObject *)L_5, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(40)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(40)
	{
		IL2CPP_JUMP_TBL(0x2F, IL_002f)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_002f:
	{
		Il2CppObject* L_6 = V_1;
		return L_6;
	}
}
// System.Collections.Generic.ICollection`1<TValue> UniRx.DictionaryDisposable`2<System.Object,System.Object>::System.Collections.Generic.IDictionary<TKey,TValue>.get_Values()
extern "C"  Il2CppObject* DictionaryDisposable_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Values_m3758170669_gshared (DictionaryDisposable_2_t2367950693 * __this, const MethodInfo* method)
{
	Dictionary_2_t3824425150 * V_0 = NULL;
	Il2CppObject* V_1 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Dictionary_2_t3824425150 * L_0 = (Dictionary_2_t3824425150 *)__this->get_inner_1();
		V_0 = (Dictionary_2_t3824425150 *)L_0;
		Dictionary_2_t3824425150 * L_1 = V_0;
		Monitor_Enter_m476686225(NULL /*static, unused*/, (Il2CppObject *)L_1, /*hidden argument*/NULL);
	}

IL_000d:
	try
	{ // begin try (depth: 1)
		{
			Dictionary_2_t3824425150 * L_2 = (Dictionary_2_t3824425150 *)__this->get_inner_1();
			NullCheck((Dictionary_2_t3824425150 *)L_2);
			ValueCollection_t1451594948 * L_3 = ((  ValueCollection_t1451594948 * (*) (Dictionary_2_t3824425150 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7)->method)((Dictionary_2_t3824425150 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7));
			List_1_t1634065389 * L_4 = (List_1_t1634065389 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8));
			((  void (*) (List_1_t1634065389 *, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9)->method)(L_4, (Il2CppObject*)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9));
			V_1 = (Il2CppObject*)L_4;
			IL2CPP_LEAVE(0x2F, FINALLY_0028);
		}

IL_0023:
		{
			; // IL_0023: leave IL_002f
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0028;
	}

FINALLY_0028:
	{ // begin finally (depth: 1)
		Dictionary_2_t3824425150 * L_5 = V_0;
		Monitor_Exit_m2088237919(NULL /*static, unused*/, (Il2CppObject *)L_5, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(40)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(40)
	{
		IL2CPP_JUMP_TBL(0x2F, IL_002f)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_002f:
	{
		Il2CppObject* L_6 = V_1;
		return L_6;
	}
}
// System.Void UniRx.DictionaryDisposable`2<System.Object,System.Object>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Add(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C"  void DictionaryDisposable_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m3024250580_gshared (DictionaryDisposable_2_t2367950693 * __this, KeyValuePair_2_t3312956448  ___item0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ((  Il2CppObject * (*) (KeyValuePair_2_t3312956448 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10)->method)((KeyValuePair_2_t3312956448 *)(&___item0), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10));
		Il2CppObject * L_1 = ((  Il2CppObject * (*) (KeyValuePair_2_t3312956448 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 11)->method)((KeyValuePair_2_t3312956448 *)(&___item0), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 11));
		NullCheck((DictionaryDisposable_2_t2367950693 *)__this);
		VirtActionInvoker2< Il2CppObject *, Il2CppObject * >::Invoke(14 /* System.Void UniRx.DictionaryDisposable`2<System.Object,System.Object>::Add(TKey,TValue) */, (DictionaryDisposable_2_t2367950693 *)__this, (Il2CppObject *)L_0, (Il2CppObject *)L_1);
		return;
	}
}
// System.Boolean UniRx.DictionaryDisposable`2<System.Object,System.Object>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Contains(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C"  bool DictionaryDisposable_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m3796158668_gshared (DictionaryDisposable_2_t2367950693 * __this, KeyValuePair_2_t3312956448  ___item0, const MethodInfo* method)
{
	Dictionary_2_t3824425150 * V_0 = NULL;
	bool V_1 = false;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Dictionary_2_t3824425150 * L_0 = (Dictionary_2_t3824425150 *)__this->get_inner_1();
		V_0 = (Dictionary_2_t3824425150 *)L_0;
		Dictionary_2_t3824425150 * L_1 = V_0;
		Monitor_Enter_m476686225(NULL /*static, unused*/, (Il2CppObject *)L_1, /*hidden argument*/NULL);
	}

IL_000d:
	try
	{ // begin try (depth: 1)
		{
			Dictionary_2_t3824425150 * L_2 = (Dictionary_2_t3824425150 *)__this->get_inner_1();
			KeyValuePair_2_t3312956448  L_3 = ___item0;
			NullCheck((Il2CppObject*)L_2);
			bool L_4 = InterfaceFuncInvoker1< bool, KeyValuePair_2_t3312956448  >::Invoke(4 /* System.Boolean System.Collections.Generic.ICollection`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::Contains(!0) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3), (Il2CppObject*)L_2, (KeyValuePair_2_t3312956448 )L_3);
			V_1 = (bool)L_4;
			IL2CPP_LEAVE(0x2B, FINALLY_0024);
		}

IL_001f:
		{
			; // IL_001f: leave IL_002b
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0024;
	}

FINALLY_0024:
	{ // begin finally (depth: 1)
		Dictionary_2_t3824425150 * L_5 = V_0;
		Monitor_Exit_m2088237919(NULL /*static, unused*/, (Il2CppObject *)L_5, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(36)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(36)
	{
		IL2CPP_JUMP_TBL(0x2B, IL_002b)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_002b:
	{
		bool L_6 = V_1;
		return L_6;
	}
}
// System.Void UniRx.DictionaryDisposable`2<System.Object,System.Object>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
extern "C"  void DictionaryDisposable_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m3907701176_gshared (DictionaryDisposable_2_t2367950693 * __this, KeyValuePair_2U5BU5D_t346249057* ___array0, int32_t ___arrayIndex1, const MethodInfo* method)
{
	Dictionary_2_t3824425150 * V_0 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Dictionary_2_t3824425150 * L_0 = (Dictionary_2_t3824425150 *)__this->get_inner_1();
		V_0 = (Dictionary_2_t3824425150 *)L_0;
		Dictionary_2_t3824425150 * L_1 = V_0;
		Monitor_Enter_m476686225(NULL /*static, unused*/, (Il2CppObject *)L_1, /*hidden argument*/NULL);
	}

IL_000d:
	try
	{ // begin try (depth: 1)
		Dictionary_2_t3824425150 * L_2 = (Dictionary_2_t3824425150 *)__this->get_inner_1();
		KeyValuePair_2U5BU5D_t346249057* L_3 = ___array0;
		int32_t L_4 = ___arrayIndex1;
		NullCheck((Il2CppObject*)L_2);
		InterfaceActionInvoker2< KeyValuePair_2U5BU5D_t346249057*, int32_t >::Invoke(5 /* System.Void System.Collections.Generic.ICollection`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::CopyTo(!0[],System.Int32) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3), (Il2CppObject*)L_2, (KeyValuePair_2U5BU5D_t346249057*)L_3, (int32_t)L_4);
		IL2CPP_LEAVE(0x26, FINALLY_001f);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_001f;
	}

FINALLY_001f:
	{ // begin finally (depth: 1)
		Dictionary_2_t3824425150 * L_5 = V_0;
		Monitor_Exit_m2088237919(NULL /*static, unused*/, (Il2CppObject *)L_5, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(31)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(31)
	{
		IL2CPP_JUMP_TBL(0x26, IL_0026)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0026:
	{
		return;
	}
}
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<TKey,TValue>> UniRx.DictionaryDisposable`2<System.Object,System.Object>::System.Collections.Generic.IEnumerable<System.Collections.Generic.KeyValuePair<TKey,TValue>>.GetEnumerator()
extern "C"  Il2CppObject* DictionaryDisposable_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m3417616323_gshared (DictionaryDisposable_2_t2367950693 * __this, const MethodInfo* method)
{
	Dictionary_2_t3824425150 * V_0 = NULL;
	Il2CppObject* V_1 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Dictionary_2_t3824425150 * L_0 = (Dictionary_2_t3824425150 *)__this->get_inner_1();
		V_0 = (Dictionary_2_t3824425150 *)L_0;
		Dictionary_2_t3824425150 * L_1 = V_0;
		Monitor_Enter_m476686225(NULL /*static, unused*/, (Il2CppObject *)L_1, /*hidden argument*/NULL);
	}

IL_000d:
	try
	{ // begin try (depth: 1)
		{
			Dictionary_2_t3824425150 * L_2 = (Dictionary_2_t3824425150 *)__this->get_inner_1();
			List_1_t4109915417 * L_3 = (List_1_t4109915417 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 13));
			((  void (*) (List_1_t4109915417 *, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 14)->method)(L_3, (Il2CppObject*)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 14));
			NullCheck((List_1_t4109915417 *)L_3);
			Enumerator_t2195698409  L_4 = ((  Enumerator_t2195698409  (*) (List_1_t4109915417 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 15)->method)((List_1_t4109915417 *)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 15));
			Enumerator_t2195698409  L_5 = L_4;
			Il2CppObject * L_6 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 16), &L_5);
			V_1 = (Il2CppObject*)L_6;
			IL2CPP_LEAVE(0x34, FINALLY_002d);
		}

IL_0028:
		{
			; // IL_0028: leave IL_0034
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_002d;
	}

FINALLY_002d:
	{ // begin finally (depth: 1)
		Dictionary_2_t3824425150 * L_7 = V_0;
		Monitor_Exit_m2088237919(NULL /*static, unused*/, (Il2CppObject *)L_7, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(45)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(45)
	{
		IL2CPP_JUMP_TBL(0x34, IL_0034)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0034:
	{
		Il2CppObject* L_8 = V_1;
		return L_8;
	}
}
// System.Collections.IEnumerator UniRx.DictionaryDisposable`2<System.Object,System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * DictionaryDisposable_2_System_Collections_IEnumerable_GetEnumerator_m785224076_gshared (DictionaryDisposable_2_t2367950693 * __this, const MethodInfo* method)
{
	{
		NullCheck((DictionaryDisposable_2_t2367950693 *)__this);
		Enumerator_t3591453092  L_0 = ((  Enumerator_t3591453092  (*) (DictionaryDisposable_2_t2367950693 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 17)->method)((DictionaryDisposable_2_t2367950693 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 17));
		Enumerator_t3591453092  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 18), &L_1);
		return (Il2CppObject *)L_2;
	}
}
// System.Boolean UniRx.DictionaryDisposable`2<System.Object,System.Object>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Remove(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern Il2CppClass* NotSupportedException_t1374155497_il2cpp_TypeInfo_var;
extern const uint32_t DictionaryDisposable_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m2177282801_MetadataUsageId;
extern "C"  bool DictionaryDisposable_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m2177282801_gshared (DictionaryDisposable_2_t2367950693 * __this, KeyValuePair_2_t3312956448  ___item0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DictionaryDisposable_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m2177282801_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1374155497 * L_0 = (NotSupportedException_t1374155497 *)il2cpp_codegen_object_new(NotSupportedException_t1374155497_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m149930845(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// TValue UniRx.DictionaryDisposable`2<System.Object,System.Object>::get_Item(TKey)
extern "C"  Il2CppObject * DictionaryDisposable_2_get_Item_m3934025632_gshared (DictionaryDisposable_2_t2367950693 * __this, Il2CppObject * ___key0, const MethodInfo* method)
{
	Dictionary_2_t3824425150 * V_0 = NULL;
	Il2CppObject * V_1 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Dictionary_2_t3824425150 * L_0 = (Dictionary_2_t3824425150 *)__this->get_inner_1();
		V_0 = (Dictionary_2_t3824425150 *)L_0;
		Dictionary_2_t3824425150 * L_1 = V_0;
		Monitor_Enter_m476686225(NULL /*static, unused*/, (Il2CppObject *)L_1, /*hidden argument*/NULL);
	}

IL_000d:
	try
	{ // begin try (depth: 1)
		{
			Dictionary_2_t3824425150 * L_2 = (Dictionary_2_t3824425150 *)__this->get_inner_1();
			Il2CppObject * L_3 = ___key0;
			NullCheck((Dictionary_2_t3824425150 *)L_2);
			Il2CppObject * L_4 = VirtFuncInvoker1< Il2CppObject *, Il2CppObject * >::Invoke(22 /* !1 System.Collections.Generic.Dictionary`2<System.Object,System.Object>::get_Item(!0) */, (Dictionary_2_t3824425150 *)L_2, (Il2CppObject *)L_3);
			V_1 = (Il2CppObject *)L_4;
			IL2CPP_LEAVE(0x2B, FINALLY_0024);
		}

IL_001f:
		{
			; // IL_001f: leave IL_002b
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0024;
	}

FINALLY_0024:
	{ // begin finally (depth: 1)
		Dictionary_2_t3824425150 * L_5 = V_0;
		Monitor_Exit_m2088237919(NULL /*static, unused*/, (Il2CppObject *)L_5, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(36)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(36)
	{
		IL2CPP_JUMP_TBL(0x2B, IL_002b)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_002b:
	{
		Il2CppObject * L_6 = V_1;
		return L_6;
	}
}
// System.Void UniRx.DictionaryDisposable`2<System.Object,System.Object>::set_Item(TKey,TValue)
extern Il2CppClass* IDisposable_t1628921374_il2cpp_TypeInfo_var;
extern const uint32_t DictionaryDisposable_2_set_Item_m509837389_MetadataUsageId;
extern "C"  void DictionaryDisposable_2_set_Item_m509837389_gshared (DictionaryDisposable_2_t2367950693 * __this, Il2CppObject * ___key0, Il2CppObject * ___value1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DictionaryDisposable_2_set_Item_m509837389_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Dictionary_2_t3824425150 * V_0 = NULL;
	Il2CppObject * V_1 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Dictionary_2_t3824425150 * L_0 = (Dictionary_2_t3824425150 *)__this->get_inner_1();
		V_0 = (Dictionary_2_t3824425150 *)L_0;
		Dictionary_2_t3824425150 * L_1 = V_0;
		Monitor_Enter_m476686225(NULL /*static, unused*/, (Il2CppObject *)L_1, /*hidden argument*/NULL);
	}

IL_000d:
	try
	{ // begin try (depth: 1)
		{
			bool L_2 = (bool)__this->get_isDisposed_0();
			if (!L_2)
			{
				goto IL_0025;
			}
		}

IL_0018:
		{
			NullCheck((Il2CppObject *)(*(&___value1)));
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, (Il2CppObject *)(*(&___value1)));
		}

IL_0025:
		{
			Il2CppObject * L_3 = ___key0;
			NullCheck((DictionaryDisposable_2_t2367950693 *)__this);
			bool L_4 = VirtFuncInvoker2< bool, Il2CppObject *, Il2CppObject ** >::Invoke(17 /* System.Boolean UniRx.DictionaryDisposable`2<System.Object,System.Object>::TryGetValue(TKey,TValue&) */, (DictionaryDisposable_2_t2367950693 *)__this, (Il2CppObject *)L_3, (Il2CppObject **)(&V_1));
			if (!L_4)
			{
				goto IL_0052;
			}
		}

IL_0033:
		{
			NullCheck((Il2CppObject *)(*(&V_1)));
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, (Il2CppObject *)(*(&V_1)));
			Dictionary_2_t3824425150 * L_5 = (Dictionary_2_t3824425150 *)__this->get_inner_1();
			Il2CppObject * L_6 = ___key0;
			Il2CppObject * L_7 = ___value1;
			NullCheck((Dictionary_2_t3824425150 *)L_5);
			VirtActionInvoker2< Il2CppObject *, Il2CppObject * >::Invoke(23 /* System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Object>::set_Item(!0,!1) */, (Dictionary_2_t3824425150 *)L_5, (Il2CppObject *)L_6, (Il2CppObject *)L_7);
			goto IL_005f;
		}

IL_0052:
		{
			Dictionary_2_t3824425150 * L_8 = (Dictionary_2_t3824425150 *)__this->get_inner_1();
			Il2CppObject * L_9 = ___key0;
			Il2CppObject * L_10 = ___value1;
			NullCheck((Dictionary_2_t3824425150 *)L_8);
			VirtActionInvoker2< Il2CppObject *, Il2CppObject * >::Invoke(23 /* System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Object>::set_Item(!0,!1) */, (Dictionary_2_t3824425150 *)L_8, (Il2CppObject *)L_9, (Il2CppObject *)L_10);
		}

IL_005f:
		{
			IL2CPP_LEAVE(0x6B, FINALLY_0064);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0064;
	}

FINALLY_0064:
	{ // begin finally (depth: 1)
		Dictionary_2_t3824425150 * L_11 = V_0;
		Monitor_Exit_m2088237919(NULL /*static, unused*/, (Il2CppObject *)L_11, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(100)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(100)
	{
		IL2CPP_JUMP_TBL(0x6B, IL_006b)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_006b:
	{
		return;
	}
}
// System.Int32 UniRx.DictionaryDisposable`2<System.Object,System.Object>::get_Count()
extern "C"  int32_t DictionaryDisposable_2_get_Count_m771800135_gshared (DictionaryDisposable_2_t2367950693 * __this, const MethodInfo* method)
{
	Dictionary_2_t3824425150 * V_0 = NULL;
	int32_t V_1 = 0;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Dictionary_2_t3824425150 * L_0 = (Dictionary_2_t3824425150 *)__this->get_inner_1();
		V_0 = (Dictionary_2_t3824425150 *)L_0;
		Dictionary_2_t3824425150 * L_1 = V_0;
		Monitor_Enter_m476686225(NULL /*static, unused*/, (Il2CppObject *)L_1, /*hidden argument*/NULL);
	}

IL_000d:
	try
	{ // begin try (depth: 1)
		{
			Dictionary_2_t3824425150 * L_2 = (Dictionary_2_t3824425150 *)__this->get_inner_1();
			NullCheck((Dictionary_2_t3824425150 *)L_2);
			int32_t L_3 = VirtFuncInvoker0< int32_t >::Invoke(10 /* System.Int32 System.Collections.Generic.Dictionary`2<System.Object,System.Object>::get_Count() */, (Dictionary_2_t3824425150 *)L_2);
			V_1 = (int32_t)L_3;
			IL2CPP_LEAVE(0x2A, FINALLY_0023);
		}

IL_001e:
		{
			; // IL_001e: leave IL_002a
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0023;
	}

FINALLY_0023:
	{ // begin finally (depth: 1)
		Dictionary_2_t3824425150 * L_4 = V_0;
		Monitor_Exit_m2088237919(NULL /*static, unused*/, (Il2CppObject *)L_4, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(35)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(35)
	{
		IL2CPP_JUMP_TBL(0x2A, IL_002a)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_002a:
	{
		int32_t L_5 = V_1;
		return L_5;
	}
}
// System.Collections.Generic.Dictionary`2/KeyCollection<TKey,TValue> UniRx.DictionaryDisposable`2<System.Object,System.Object>::get_Keys()
extern Il2CppClass* NotSupportedException_t1374155497_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1526387355;
extern const uint32_t DictionaryDisposable_2_get_Keys_m1722058918_MetadataUsageId;
extern "C"  KeyCollection_t1852733134 * DictionaryDisposable_2_get_Keys_m1722058918_gshared (DictionaryDisposable_2_t2367950693 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DictionaryDisposable_2_get_Keys_m1722058918_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1374155497 * L_0 = (NotSupportedException_t1374155497 *)il2cpp_codegen_object_new(NotSupportedException_t1374155497_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m133757637(L_0, (String_t*)_stringLiteral1526387355, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Collections.Generic.Dictionary`2/ValueCollection<TKey,TValue> UniRx.DictionaryDisposable`2<System.Object,System.Object>::get_Values()
extern Il2CppClass* NotSupportedException_t1374155497_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2352949321;
extern const uint32_t DictionaryDisposable_2_get_Values_m293030658_MetadataUsageId;
extern "C"  ValueCollection_t1451594948 * DictionaryDisposable_2_get_Values_m293030658_gshared (DictionaryDisposable_2_t2367950693 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DictionaryDisposable_2_get_Values_m293030658_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1374155497 * L_0 = (NotSupportedException_t1374155497 *)il2cpp_codegen_object_new(NotSupportedException_t1374155497_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m133757637(L_0, (String_t*)_stringLiteral2352949321, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void UniRx.DictionaryDisposable`2<System.Object,System.Object>::Add(TKey,TValue)
extern Il2CppClass* IDisposable_t1628921374_il2cpp_TypeInfo_var;
extern const uint32_t DictionaryDisposable_2_Add_m84723208_MetadataUsageId;
extern "C"  void DictionaryDisposable_2_Add_m84723208_gshared (DictionaryDisposable_2_t2367950693 * __this, Il2CppObject * ___key0, Il2CppObject * ___value1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DictionaryDisposable_2_Add_m84723208_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Dictionary_2_t3824425150 * V_0 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Dictionary_2_t3824425150 * L_0 = (Dictionary_2_t3824425150 *)__this->get_inner_1();
		V_0 = (Dictionary_2_t3824425150 *)L_0;
		Dictionary_2_t3824425150 * L_1 = V_0;
		Monitor_Enter_m476686225(NULL /*static, unused*/, (Il2CppObject *)L_1, /*hidden argument*/NULL);
	}

IL_000d:
	try
	{ // begin try (depth: 1)
		{
			bool L_2 = (bool)__this->get_isDisposed_0();
			if (!L_2)
			{
				goto IL_002a;
			}
		}

IL_0018:
		{
			NullCheck((Il2CppObject *)(*(&___value1)));
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, (Il2CppObject *)(*(&___value1)));
			IL2CPP_LEAVE(0x43, FINALLY_003c);
		}

IL_002a:
		{
			Dictionary_2_t3824425150 * L_3 = (Dictionary_2_t3824425150 *)__this->get_inner_1();
			Il2CppObject * L_4 = ___key0;
			Il2CppObject * L_5 = ___value1;
			NullCheck((Dictionary_2_t3824425150 *)L_3);
			VirtActionInvoker2< Il2CppObject *, Il2CppObject * >::Invoke(18 /* System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Object>::Add(!0,!1) */, (Dictionary_2_t3824425150 *)L_3, (Il2CppObject *)L_4, (Il2CppObject *)L_5);
			IL2CPP_LEAVE(0x43, FINALLY_003c);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_003c;
	}

FINALLY_003c:
	{ // begin finally (depth: 1)
		Dictionary_2_t3824425150 * L_6 = V_0;
		Monitor_Exit_m2088237919(NULL /*static, unused*/, (Il2CppObject *)L_6, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(60)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(60)
	{
		IL2CPP_JUMP_TBL(0x43, IL_0043)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0043:
	{
		return;
	}
}
// System.Void UniRx.DictionaryDisposable`2<System.Object,System.Object>::Clear()
extern Il2CppClass* IDisposable_t1628921374_il2cpp_TypeInfo_var;
extern const uint32_t DictionaryDisposable_2_Clear_m2766000504_MetadataUsageId;
extern "C"  void DictionaryDisposable_2_Clear_m2766000504_gshared (DictionaryDisposable_2_t2367950693 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DictionaryDisposable_2_Clear_m2766000504_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Dictionary_2_t3824425150 * V_0 = NULL;
	KeyValuePair_2_t3312956448  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Enumerator_t3591453092  V_2;
	memset(&V_2, 0, sizeof(V_2));
	Il2CppObject * V_3 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Dictionary_2_t3824425150 * L_0 = (Dictionary_2_t3824425150 *)__this->get_inner_1();
		V_0 = (Dictionary_2_t3824425150 *)L_0;
		Dictionary_2_t3824425150 * L_1 = V_0;
		Monitor_Enter_m476686225(NULL /*static, unused*/, (Il2CppObject *)L_1, /*hidden argument*/NULL);
	}

IL_000d:
	try
	{ // begin try (depth: 1)
		{
			Dictionary_2_t3824425150 * L_2 = (Dictionary_2_t3824425150 *)__this->get_inner_1();
			NullCheck((Dictionary_2_t3824425150 *)L_2);
			Enumerator_t3591453092  L_3 = ((  Enumerator_t3591453092  (*) (Dictionary_2_t3824425150 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 25)->method)((Dictionary_2_t3824425150 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 25));
			V_2 = (Enumerator_t3591453092 )L_3;
		}

IL_0019:
		try
		{ // begin try (depth: 2)
			{
				goto IL_003b;
			}

IL_001e:
			{
				KeyValuePair_2_t3312956448  L_4 = ((  KeyValuePair_2_t3312956448  (*) (Enumerator_t3591453092 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 26)->method)((Enumerator_t3591453092 *)(&V_2), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 26));
				V_1 = (KeyValuePair_2_t3312956448 )L_4;
				Il2CppObject * L_5 = ((  Il2CppObject * (*) (KeyValuePair_2_t3312956448 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 11)->method)((KeyValuePair_2_t3312956448 *)(&V_1), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 11));
				V_3 = (Il2CppObject *)L_5;
				NullCheck((Il2CppObject *)(*(&V_3)));
				InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, (Il2CppObject *)(*(&V_3)));
			}

IL_003b:
			{
				bool L_6 = ((  bool (*) (Enumerator_t3591453092 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 27)->method)((Enumerator_t3591453092 *)(&V_2), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 27));
				if (L_6)
				{
					goto IL_001e;
				}
			}

IL_0047:
			{
				IL2CPP_LEAVE(0x58, FINALLY_004c);
			}
		} // end try (depth: 2)
		catch(Il2CppExceptionWrapper& e)
		{
			__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
			goto FINALLY_004c;
		}

FINALLY_004c:
		{ // begin finally (depth: 2)
			Enumerator_t3591453092  L_7 = V_2;
			Enumerator_t3591453092  L_8 = L_7;
			Il2CppObject * L_9 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 18), &L_8);
			NullCheck((Il2CppObject *)L_9);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, (Il2CppObject *)L_9);
			IL2CPP_END_FINALLY(76)
		} // end finally (depth: 2)
		IL2CPP_CLEANUP(76)
		{
			IL2CPP_JUMP_TBL(0x58, IL_0058)
			IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
		}

IL_0058:
		{
			Dictionary_2_t3824425150 * L_10 = (Dictionary_2_t3824425150 *)__this->get_inner_1();
			NullCheck((Dictionary_2_t3824425150 *)L_10);
			VirtActionInvoker0::Invoke(33 /* System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Object>::Clear() */, (Dictionary_2_t3824425150 *)L_10);
			IL2CPP_LEAVE(0x6F, FINALLY_0068);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0068;
	}

FINALLY_0068:
	{ // begin finally (depth: 1)
		Dictionary_2_t3824425150 * L_11 = V_0;
		Monitor_Exit_m2088237919(NULL /*static, unused*/, (Il2CppObject *)L_11, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(104)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(104)
	{
		IL2CPP_JUMP_TBL(0x6F, IL_006f)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_006f:
	{
		return;
	}
}
// System.Boolean UniRx.DictionaryDisposable`2<System.Object,System.Object>::Remove(TKey)
extern Il2CppClass* IDisposable_t1628921374_il2cpp_TypeInfo_var;
extern const uint32_t DictionaryDisposable_2_Remove_m2460413492_MetadataUsageId;
extern "C"  bool DictionaryDisposable_2_Remove_m2460413492_gshared (DictionaryDisposable_2_t2367950693 * __this, Il2CppObject * ___key0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DictionaryDisposable_2_Remove_m2460413492_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Dictionary_2_t3824425150 * V_0 = NULL;
	Il2CppObject * V_1 = NULL;
	bool V_2 = false;
	bool V_3 = false;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Dictionary_2_t3824425150 * L_0 = (Dictionary_2_t3824425150 *)__this->get_inner_1();
		V_0 = (Dictionary_2_t3824425150 *)L_0;
		Dictionary_2_t3824425150 * L_1 = V_0;
		Monitor_Enter_m476686225(NULL /*static, unused*/, (Il2CppObject *)L_1, /*hidden argument*/NULL);
	}

IL_000d:
	try
	{ // begin try (depth: 1)
		{
			Dictionary_2_t3824425150 * L_2 = (Dictionary_2_t3824425150 *)__this->get_inner_1();
			Il2CppObject * L_3 = ___key0;
			NullCheck((Dictionary_2_t3824425150 *)L_2);
			bool L_4 = VirtFuncInvoker2< bool, Il2CppObject *, Il2CppObject ** >::Invoke(21 /* System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Object>::TryGetValue(!0,!1&) */, (Dictionary_2_t3824425150 *)L_2, (Il2CppObject *)L_3, (Il2CppObject **)(&V_1));
			if (!L_4)
			{
				goto IL_0047;
			}
		}

IL_0020:
		{
			Dictionary_2_t3824425150 * L_5 = (Dictionary_2_t3824425150 *)__this->get_inner_1();
			Il2CppObject * L_6 = ___key0;
			NullCheck((Dictionary_2_t3824425150 *)L_5);
			bool L_7 = VirtFuncInvoker1< bool, Il2CppObject * >::Invoke(20 /* System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Object>::Remove(!0) */, (Dictionary_2_t3824425150 *)L_5, (Il2CppObject *)L_6);
			V_2 = (bool)L_7;
			bool L_8 = V_2;
			if (!L_8)
			{
				goto IL_0040;
			}
		}

IL_0033:
		{
			NullCheck((Il2CppObject *)(*(&V_1)));
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, (Il2CppObject *)(*(&V_1)));
		}

IL_0040:
		{
			bool L_9 = V_2;
			V_3 = (bool)L_9;
			IL2CPP_LEAVE(0x5A, FINALLY_0053);
		}

IL_0047:
		{
			V_3 = (bool)0;
			IL2CPP_LEAVE(0x5A, FINALLY_0053);
		}

IL_004e:
		{
			; // IL_004e: leave IL_005a
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0053;
	}

FINALLY_0053:
	{ // begin finally (depth: 1)
		Dictionary_2_t3824425150 * L_10 = V_0;
		Monitor_Exit_m2088237919(NULL /*static, unused*/, (Il2CppObject *)L_10, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(83)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(83)
	{
		IL2CPP_JUMP_TBL(0x5A, IL_005a)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_005a:
	{
		bool L_11 = V_3;
		return L_11;
	}
}
// System.Boolean UniRx.DictionaryDisposable`2<System.Object,System.Object>::ContainsKey(TKey)
extern "C"  bool DictionaryDisposable_2_ContainsKey_m1420555580_gshared (DictionaryDisposable_2_t2367950693 * __this, Il2CppObject * ___key0, const MethodInfo* method)
{
	Dictionary_2_t3824425150 * V_0 = NULL;
	bool V_1 = false;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Dictionary_2_t3824425150 * L_0 = (Dictionary_2_t3824425150 *)__this->get_inner_1();
		V_0 = (Dictionary_2_t3824425150 *)L_0;
		Dictionary_2_t3824425150 * L_1 = V_0;
		Monitor_Enter_m476686225(NULL /*static, unused*/, (Il2CppObject *)L_1, /*hidden argument*/NULL);
	}

IL_000d:
	try
	{ // begin try (depth: 1)
		{
			Dictionary_2_t3824425150 * L_2 = (Dictionary_2_t3824425150 *)__this->get_inner_1();
			Il2CppObject * L_3 = ___key0;
			NullCheck((Dictionary_2_t3824425150 *)L_2);
			bool L_4 = VirtFuncInvoker1< bool, Il2CppObject * >::Invoke(19 /* System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Object>::ContainsKey(!0) */, (Dictionary_2_t3824425150 *)L_2, (Il2CppObject *)L_3);
			V_1 = (bool)L_4;
			IL2CPP_LEAVE(0x2B, FINALLY_0024);
		}

IL_001f:
		{
			; // IL_001f: leave IL_002b
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0024;
	}

FINALLY_0024:
	{ // begin finally (depth: 1)
		Dictionary_2_t3824425150 * L_5 = V_0;
		Monitor_Exit_m2088237919(NULL /*static, unused*/, (Il2CppObject *)L_5, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(36)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(36)
	{
		IL2CPP_JUMP_TBL(0x2B, IL_002b)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_002b:
	{
		bool L_6 = V_1;
		return L_6;
	}
}
// System.Boolean UniRx.DictionaryDisposable`2<System.Object,System.Object>::TryGetValue(TKey,TValue&)
extern "C"  bool DictionaryDisposable_2_TryGetValue_m3346878869_gshared (DictionaryDisposable_2_t2367950693 * __this, Il2CppObject * ___key0, Il2CppObject ** ___value1, const MethodInfo* method)
{
	Dictionary_2_t3824425150 * V_0 = NULL;
	bool V_1 = false;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Dictionary_2_t3824425150 * L_0 = (Dictionary_2_t3824425150 *)__this->get_inner_1();
		V_0 = (Dictionary_2_t3824425150 *)L_0;
		Dictionary_2_t3824425150 * L_1 = V_0;
		Monitor_Enter_m476686225(NULL /*static, unused*/, (Il2CppObject *)L_1, /*hidden argument*/NULL);
	}

IL_000d:
	try
	{ // begin try (depth: 1)
		{
			Dictionary_2_t3824425150 * L_2 = (Dictionary_2_t3824425150 *)__this->get_inner_1();
			Il2CppObject * L_3 = ___key0;
			Il2CppObject ** L_4 = ___value1;
			NullCheck((Dictionary_2_t3824425150 *)L_2);
			bool L_5 = VirtFuncInvoker2< bool, Il2CppObject *, Il2CppObject ** >::Invoke(21 /* System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Object>::TryGetValue(!0,!1&) */, (Dictionary_2_t3824425150 *)L_2, (Il2CppObject *)L_3, (Il2CppObject **)L_4);
			V_1 = (bool)L_5;
			IL2CPP_LEAVE(0x2C, FINALLY_0025);
		}

IL_0020:
		{
			; // IL_0020: leave IL_002c
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0025;
	}

FINALLY_0025:
	{ // begin finally (depth: 1)
		Dictionary_2_t3824425150 * L_6 = V_0;
		Monitor_Exit_m2088237919(NULL /*static, unused*/, (Il2CppObject *)L_6, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(37)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(37)
	{
		IL2CPP_JUMP_TBL(0x2C, IL_002c)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_002c:
	{
		bool L_7 = V_1;
		return L_7;
	}
}
// System.Collections.Generic.Dictionary`2/Enumerator<TKey,TValue> UniRx.DictionaryDisposable`2<System.Object,System.Object>::GetEnumerator()
extern "C"  Enumerator_t3591453092  DictionaryDisposable_2_GetEnumerator_m2934309272_gshared (DictionaryDisposable_2_t2367950693 * __this, const MethodInfo* method)
{
	Dictionary_2_t3824425150 * V_0 = NULL;
	Enumerator_t3591453092  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Dictionary_2_t3824425150 * L_0 = (Dictionary_2_t3824425150 *)__this->get_inner_1();
		V_0 = (Dictionary_2_t3824425150 *)L_0;
		Dictionary_2_t3824425150 * L_1 = V_0;
		Monitor_Enter_m476686225(NULL /*static, unused*/, (Il2CppObject *)L_1, /*hidden argument*/NULL);
	}

IL_000d:
	try
	{ // begin try (depth: 1)
		{
			Dictionary_2_t3824425150 * L_2 = (Dictionary_2_t3824425150 *)__this->get_inner_1();
			Dictionary_2_t3824425150 * L_3 = (Dictionary_2_t3824425150 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
			((  void (*) (Dictionary_2_t3824425150 *, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 32)->method)(L_3, (Il2CppObject*)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 32));
			NullCheck((Dictionary_2_t3824425150 *)L_3);
			Enumerator_t3591453092  L_4 = ((  Enumerator_t3591453092  (*) (Dictionary_2_t3824425150 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 25)->method)((Dictionary_2_t3824425150 *)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 25));
			V_1 = (Enumerator_t3591453092 )L_4;
			IL2CPP_LEAVE(0x2F, FINALLY_0028);
		}

IL_0023:
		{
			; // IL_0023: leave IL_002f
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0028;
	}

FINALLY_0028:
	{ // begin finally (depth: 1)
		Dictionary_2_t3824425150 * L_5 = V_0;
		Monitor_Exit_m2088237919(NULL /*static, unused*/, (Il2CppObject *)L_5, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(40)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(40)
	{
		IL2CPP_JUMP_TBL(0x2F, IL_002f)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_002f:
	{
		Enumerator_t3591453092  L_6 = V_1;
		return L_6;
	}
}
// System.Void UniRx.DictionaryDisposable`2<System.Object,System.Object>::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern Il2CppClass* ISerializable_t1415126241_il2cpp_TypeInfo_var;
extern const uint32_t DictionaryDisposable_2_GetObjectData_m3504898667_MetadataUsageId;
extern "C"  void DictionaryDisposable_2_GetObjectData_m3504898667_gshared (DictionaryDisposable_2_t2367950693 * __this, SerializationInfo_t2995724695 * ___info0, StreamingContext_t986364934  ___context1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DictionaryDisposable_2_GetObjectData_m3504898667_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Dictionary_2_t3824425150 * V_0 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Dictionary_2_t3824425150 * L_0 = (Dictionary_2_t3824425150 *)__this->get_inner_1();
		V_0 = (Dictionary_2_t3824425150 *)L_0;
		Dictionary_2_t3824425150 * L_1 = V_0;
		Monitor_Enter_m476686225(NULL /*static, unused*/, (Il2CppObject *)L_1, /*hidden argument*/NULL);
	}

IL_000d:
	try
	{ // begin try (depth: 1)
		Dictionary_2_t3824425150 * L_2 = (Dictionary_2_t3824425150 *)__this->get_inner_1();
		SerializationInfo_t2995724695 * L_3 = ___info0;
		StreamingContext_t986364934  L_4 = ___context1;
		NullCheck((Il2CppObject *)L_2);
		InterfaceActionInvoker2< SerializationInfo_t2995724695 *, StreamingContext_t986364934  >::Invoke(0 /* System.Void System.Runtime.Serialization.ISerializable::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext) */, ISerializable_t1415126241_il2cpp_TypeInfo_var, (Il2CppObject *)L_2, (SerializationInfo_t2995724695 *)L_3, (StreamingContext_t986364934 )L_4);
		IL2CPP_LEAVE(0x26, FINALLY_001f);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_001f;
	}

FINALLY_001f:
	{ // begin finally (depth: 1)
		Dictionary_2_t3824425150 * L_5 = V_0;
		Monitor_Exit_m2088237919(NULL /*static, unused*/, (Il2CppObject *)L_5, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(31)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(31)
	{
		IL2CPP_JUMP_TBL(0x26, IL_0026)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0026:
	{
		return;
	}
}
// System.Void UniRx.DictionaryDisposable`2<System.Object,System.Object>::OnDeserialization(System.Object)
extern Il2CppClass* IDeserializationCallback_t3135514852_il2cpp_TypeInfo_var;
extern const uint32_t DictionaryDisposable_2_OnDeserialization_m3906628825_MetadataUsageId;
extern "C"  void DictionaryDisposable_2_OnDeserialization_m3906628825_gshared (DictionaryDisposable_2_t2367950693 * __this, Il2CppObject * ___sender0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DictionaryDisposable_2_OnDeserialization_m3906628825_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Dictionary_2_t3824425150 * V_0 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Dictionary_2_t3824425150 * L_0 = (Dictionary_2_t3824425150 *)__this->get_inner_1();
		V_0 = (Dictionary_2_t3824425150 *)L_0;
		Dictionary_2_t3824425150 * L_1 = V_0;
		Monitor_Enter_m476686225(NULL /*static, unused*/, (Il2CppObject *)L_1, /*hidden argument*/NULL);
	}

IL_000d:
	try
	{ // begin try (depth: 1)
		Dictionary_2_t3824425150 * L_2 = (Dictionary_2_t3824425150 *)__this->get_inner_1();
		Il2CppObject * L_3 = ___sender0;
		NullCheck((Il2CppObject *)L_2);
		InterfaceActionInvoker1< Il2CppObject * >::Invoke(0 /* System.Void System.Runtime.Serialization.IDeserializationCallback::OnDeserialization(System.Object) */, IDeserializationCallback_t3135514852_il2cpp_TypeInfo_var, (Il2CppObject *)L_2, (Il2CppObject *)L_3);
		IL2CPP_LEAVE(0x25, FINALLY_001e);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_001e;
	}

FINALLY_001e:
	{ // begin finally (depth: 1)
		Dictionary_2_t3824425150 * L_4 = V_0;
		Monitor_Exit_m2088237919(NULL /*static, unused*/, (Il2CppObject *)L_4, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(30)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(30)
	{
		IL2CPP_JUMP_TBL(0x25, IL_0025)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0025:
	{
		return;
	}
}
// System.Void UniRx.DictionaryDisposable`2<System.Object,System.Object>::Dispose()
extern Il2CppClass* IDisposable_t1628921374_il2cpp_TypeInfo_var;
extern const uint32_t DictionaryDisposable_2_Dispose_m1064886858_MetadataUsageId;
extern "C"  void DictionaryDisposable_2_Dispose_m1064886858_gshared (DictionaryDisposable_2_t2367950693 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DictionaryDisposable_2_Dispose_m1064886858_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Dictionary_2_t3824425150 * V_0 = NULL;
	KeyValuePair_2_t3312956448  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Enumerator_t3591453092  V_2;
	memset(&V_2, 0, sizeof(V_2));
	Il2CppObject * V_3 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Dictionary_2_t3824425150 * L_0 = (Dictionary_2_t3824425150 *)__this->get_inner_1();
		V_0 = (Dictionary_2_t3824425150 *)L_0;
		Dictionary_2_t3824425150 * L_1 = V_0;
		Monitor_Enter_m476686225(NULL /*static, unused*/, (Il2CppObject *)L_1, /*hidden argument*/NULL);
	}

IL_000d:
	try
	{ // begin try (depth: 1)
		{
			bool L_2 = (bool)__this->get_isDisposed_0();
			if (!L_2)
			{
				goto IL_001d;
			}
		}

IL_0018:
		{
			IL2CPP_LEAVE(0x86, FINALLY_007f);
		}

IL_001d:
		{
			__this->set_isDisposed_0((bool)1);
			Dictionary_2_t3824425150 * L_3 = (Dictionary_2_t3824425150 *)__this->get_inner_1();
			NullCheck((Dictionary_2_t3824425150 *)L_3);
			Enumerator_t3591453092  L_4 = ((  Enumerator_t3591453092  (*) (Dictionary_2_t3824425150 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 25)->method)((Dictionary_2_t3824425150 *)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 25));
			V_2 = (Enumerator_t3591453092 )L_4;
		}

IL_0030:
		try
		{ // begin try (depth: 2)
			{
				goto IL_0052;
			}

IL_0035:
			{
				KeyValuePair_2_t3312956448  L_5 = ((  KeyValuePair_2_t3312956448  (*) (Enumerator_t3591453092 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 26)->method)((Enumerator_t3591453092 *)(&V_2), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 26));
				V_1 = (KeyValuePair_2_t3312956448 )L_5;
				Il2CppObject * L_6 = ((  Il2CppObject * (*) (KeyValuePair_2_t3312956448 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 11)->method)((KeyValuePair_2_t3312956448 *)(&V_1), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 11));
				V_3 = (Il2CppObject *)L_6;
				NullCheck((Il2CppObject *)(*(&V_3)));
				InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, (Il2CppObject *)(*(&V_3)));
			}

IL_0052:
			{
				bool L_7 = ((  bool (*) (Enumerator_t3591453092 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 27)->method)((Enumerator_t3591453092 *)(&V_2), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 27));
				if (L_7)
				{
					goto IL_0035;
				}
			}

IL_005e:
			{
				IL2CPP_LEAVE(0x6F, FINALLY_0063);
			}
		} // end try (depth: 2)
		catch(Il2CppExceptionWrapper& e)
		{
			__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
			goto FINALLY_0063;
		}

FINALLY_0063:
		{ // begin finally (depth: 2)
			Enumerator_t3591453092  L_8 = V_2;
			Enumerator_t3591453092  L_9 = L_8;
			Il2CppObject * L_10 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 18), &L_9);
			NullCheck((Il2CppObject *)L_10);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, (Il2CppObject *)L_10);
			IL2CPP_END_FINALLY(99)
		} // end finally (depth: 2)
		IL2CPP_CLEANUP(99)
		{
			IL2CPP_JUMP_TBL(0x6F, IL_006f)
			IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
		}

IL_006f:
		{
			Dictionary_2_t3824425150 * L_11 = (Dictionary_2_t3824425150 *)__this->get_inner_1();
			NullCheck((Dictionary_2_t3824425150 *)L_11);
			VirtActionInvoker0::Invoke(33 /* System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Object>::Clear() */, (Dictionary_2_t3824425150 *)L_11);
			IL2CPP_LEAVE(0x86, FINALLY_007f);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_007f;
	}

FINALLY_007f:
	{ // begin finally (depth: 1)
		Dictionary_2_t3824425150 * L_12 = V_0;
		Monitor_Exit_m2088237919(NULL /*static, unused*/, (Il2CppObject *)L_12, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(127)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(127)
	{
		IL2CPP_JUMP_TBL(0x86, IL_0086)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0086:
	{
		return;
	}
}
// System.Void UniRx.DictionaryRemoveEvent`2<System.Object,System.Object>::.ctor(TKey,TValue)
extern "C"  void DictionaryRemoveEvent_2__ctor_m686634581_gshared (DictionaryRemoveEvent_2_t3322288291 * __this, Il2CppObject * ___key0, Il2CppObject * ___value1, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___key0;
		((  void (*) (DictionaryRemoveEvent_2_t3322288291 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((DictionaryRemoveEvent_2_t3322288291 *)__this, (Il2CppObject *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		Il2CppObject * L_1 = ___value1;
		((  void (*) (DictionaryRemoveEvent_2_t3322288291 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)((DictionaryRemoveEvent_2_t3322288291 *)__this, (Il2CppObject *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		return;
	}
}
// TKey UniRx.DictionaryRemoveEvent`2<System.Object,System.Object>::get_Key()
extern "C"  Il2CppObject * DictionaryRemoveEvent_2_get_Key_m1804595929_gshared (DictionaryRemoveEvent_2_t3322288291 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_U3CKeyU3Ek__BackingField_0();
		return L_0;
	}
}
// System.Void UniRx.DictionaryRemoveEvent`2<System.Object,System.Object>::set_Key(TKey)
extern "C"  void DictionaryRemoveEvent_2_set_Key_m859095732_gshared (DictionaryRemoveEvent_2_t3322288291 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___value0;
		__this->set_U3CKeyU3Ek__BackingField_0(L_0);
		return;
	}
}
// TValue UniRx.DictionaryRemoveEvent`2<System.Object,System.Object>::get_Value()
extern "C"  Il2CppObject * DictionaryRemoveEvent_2_get_Value_m3315363709_gshared (DictionaryRemoveEvent_2_t3322288291 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_U3CValueU3Ek__BackingField_1();
		return L_0;
	}
}
// System.Void UniRx.DictionaryRemoveEvent`2<System.Object,System.Object>::set_Value(TValue)
extern "C"  void DictionaryRemoveEvent_2_set_Value_m625450164_gshared (DictionaryRemoveEvent_2_t3322288291 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___value0;
		__this->set_U3CValueU3Ek__BackingField_1(L_0);
		return;
	}
}
// System.String UniRx.DictionaryRemoveEvent`2<System.Object,System.Object>::ToString()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3592735339;
extern const uint32_t DictionaryRemoveEvent_2_ToString_m1055329428_MetadataUsageId;
extern "C"  String_t* DictionaryRemoveEvent_2_ToString_m1055329428_gshared (DictionaryRemoveEvent_2_t3322288291 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DictionaryRemoveEvent_2_ToString_m1055329428_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject * L_0 = ((  Il2CppObject * (*) (DictionaryRemoveEvent_2_t3322288291 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)((DictionaryRemoveEvent_2_t3322288291 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		Il2CppObject * L_1 = ((  Il2CppObject * (*) (DictionaryRemoveEvent_2_t3322288291 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)((DictionaryRemoveEvent_2_t3322288291 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_2 = String_Format_m2398979370(NULL /*static, unused*/, (String_t*)_stringLiteral3592735339, (Il2CppObject *)L_0, (Il2CppObject *)L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Int32 UniRx.DictionaryRemoveEvent`2<System.Object,System.Object>::GetHashCode()
extern "C"  int32_t DictionaryRemoveEvent_2_GetHashCode_m2076469080_gshared (DictionaryRemoveEvent_2_t3322288291 * __this, const MethodInfo* method)
{
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7));
		EqualityComparer_1_t271497070 * L_0 = ((  EqualityComparer_1_t271497070 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6)->method)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		Il2CppObject * L_1 = ((  Il2CppObject * (*) (DictionaryRemoveEvent_2_t3322288291 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)((DictionaryRemoveEvent_2_t3322288291 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		NullCheck((EqualityComparer_1_t271497070 *)L_0);
		int32_t L_2 = VirtFuncInvoker1< int32_t, Il2CppObject * >::Invoke(8 /* System.Int32 System.Collections.Generic.EqualityComparer`1<System.Object>::GetHashCode(!0) */, (EqualityComparer_1_t271497070 *)L_0, (Il2CppObject *)L_1);
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10));
		EqualityComparer_1_t271497070 * L_3 = ((  EqualityComparer_1_t271497070 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9)->method)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9));
		Il2CppObject * L_4 = ((  Il2CppObject * (*) (DictionaryRemoveEvent_2_t3322288291 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)((DictionaryRemoveEvent_2_t3322288291 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		NullCheck((EqualityComparer_1_t271497070 *)L_3);
		int32_t L_5 = VirtFuncInvoker1< int32_t, Il2CppObject * >::Invoke(8 /* System.Int32 System.Collections.Generic.EqualityComparer`1<System.Object>::GetHashCode(!0) */, (EqualityComparer_1_t271497070 *)L_3, (Il2CppObject *)L_4);
		return ((int32_t)((int32_t)L_2^(int32_t)((int32_t)((int32_t)L_5<<(int32_t)2))));
	}
}
// System.Boolean UniRx.DictionaryRemoveEvent`2<System.Object,System.Object>::Equals(UniRx.DictionaryRemoveEvent`2<TKey,TValue>)
extern "C"  bool DictionaryRemoveEvent_2_Equals_m3721337790_gshared (DictionaryRemoveEvent_2_t3322288291 * __this, DictionaryRemoveEvent_2_t3322288291  ___other0, const MethodInfo* method)
{
	int32_t G_B3_0 = 0;
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7));
		EqualityComparer_1_t271497070 * L_0 = ((  EqualityComparer_1_t271497070 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6)->method)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		Il2CppObject * L_1 = ((  Il2CppObject * (*) (DictionaryRemoveEvent_2_t3322288291 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)((DictionaryRemoveEvent_2_t3322288291 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		Il2CppObject * L_2 = ((  Il2CppObject * (*) (DictionaryRemoveEvent_2_t3322288291 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)((DictionaryRemoveEvent_2_t3322288291 *)(&___other0), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		NullCheck((EqualityComparer_1_t271497070 *)L_0);
		bool L_3 = VirtFuncInvoker2< bool, Il2CppObject *, Il2CppObject * >::Invoke(9 /* System.Boolean System.Collections.Generic.EqualityComparer`1<System.Object>::Equals(!0,!0) */, (EqualityComparer_1_t271497070 *)L_0, (Il2CppObject *)L_1, (Il2CppObject *)L_2);
		if (!L_3)
		{
			goto IL_0035;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10));
		EqualityComparer_1_t271497070 * L_4 = ((  EqualityComparer_1_t271497070 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9)->method)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9));
		Il2CppObject * L_5 = ((  Il2CppObject * (*) (DictionaryRemoveEvent_2_t3322288291 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)((DictionaryRemoveEvent_2_t3322288291 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		Il2CppObject * L_6 = ((  Il2CppObject * (*) (DictionaryRemoveEvent_2_t3322288291 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)((DictionaryRemoveEvent_2_t3322288291 *)(&___other0), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		NullCheck((EqualityComparer_1_t271497070 *)L_4);
		bool L_7 = VirtFuncInvoker2< bool, Il2CppObject *, Il2CppObject * >::Invoke(9 /* System.Boolean System.Collections.Generic.EqualityComparer`1<System.Object>::Equals(!0,!0) */, (EqualityComparer_1_t271497070 *)L_4, (Il2CppObject *)L_5, (Il2CppObject *)L_6);
		G_B3_0 = ((int32_t)(L_7));
		goto IL_0036;
	}

IL_0035:
	{
		G_B3_0 = 0;
	}

IL_0036:
	{
		return (bool)G_B3_0;
	}
}
// System.Void UniRx.DictionaryReplaceEvent`2<System.Object,System.Object>::.ctor(TKey,TValue,TValue)
extern "C"  void DictionaryReplaceEvent_2__ctor_m2258921096_gshared (DictionaryReplaceEvent_2_t3778927063 * __this, Il2CppObject * ___key0, Il2CppObject * ___oldValue1, Il2CppObject * ___newValue2, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___key0;
		((  void (*) (DictionaryReplaceEvent_2_t3778927063 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((DictionaryReplaceEvent_2_t3778927063 *)__this, (Il2CppObject *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		Il2CppObject * L_1 = ___oldValue1;
		((  void (*) (DictionaryReplaceEvent_2_t3778927063 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)((DictionaryReplaceEvent_2_t3778927063 *)__this, (Il2CppObject *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		Il2CppObject * L_2 = ___newValue2;
		((  void (*) (DictionaryReplaceEvent_2_t3778927063 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)((DictionaryReplaceEvent_2_t3778927063 *)__this, (Il2CppObject *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return;
	}
}
// TKey UniRx.DictionaryReplaceEvent`2<System.Object,System.Object>::get_Key()
extern "C"  Il2CppObject * DictionaryReplaceEvent_2_get_Key_m555528929_gshared (DictionaryReplaceEvent_2_t3778927063 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_U3CKeyU3Ek__BackingField_0();
		return L_0;
	}
}
// System.Void UniRx.DictionaryReplaceEvent`2<System.Object,System.Object>::set_Key(TKey)
extern "C"  void DictionaryReplaceEvent_2_set_Key_m2934339144_gshared (DictionaryReplaceEvent_2_t3778927063 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___value0;
		__this->set_U3CKeyU3Ek__BackingField_0(L_0);
		return;
	}
}
// TValue UniRx.DictionaryReplaceEvent`2<System.Object,System.Object>::get_OldValue()
extern "C"  Il2CppObject * DictionaryReplaceEvent_2_get_OldValue_m2992843452_gshared (DictionaryReplaceEvent_2_t3778927063 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_U3COldValueU3Ek__BackingField_1();
		return L_0;
	}
}
// System.Void UniRx.DictionaryReplaceEvent`2<System.Object,System.Object>::set_OldValue(TValue)
extern "C"  void DictionaryReplaceEvent_2_set_OldValue_m78777627_gshared (DictionaryReplaceEvent_2_t3778927063 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___value0;
		__this->set_U3COldValueU3Ek__BackingField_1(L_0);
		return;
	}
}
// TValue UniRx.DictionaryReplaceEvent`2<System.Object,System.Object>::get_NewValue()
extern "C"  Il2CppObject * DictionaryReplaceEvent_2_get_NewValue_m1710294307_gshared (DictionaryReplaceEvent_2_t3778927063 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_U3CNewValueU3Ek__BackingField_2();
		return L_0;
	}
}
// System.Void UniRx.DictionaryReplaceEvent`2<System.Object,System.Object>::set_NewValue(TValue)
extern "C"  void DictionaryReplaceEvent_2_set_NewValue_m1905671746_gshared (DictionaryReplaceEvent_2_t3778927063 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___value0;
		__this->set_U3CNewValueU3Ek__BackingField_2(L_0);
		return;
	}
}
// System.String UniRx.DictionaryReplaceEvent`2<System.Object,System.Object>::ToString()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral263864371;
extern const uint32_t DictionaryReplaceEvent_2_ToString_m3153611802_MetadataUsageId;
extern "C"  String_t* DictionaryReplaceEvent_2_ToString_m3153611802_gshared (DictionaryReplaceEvent_2_t3778927063 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DictionaryReplaceEvent_2_ToString_m3153611802_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject * L_0 = ((  Il2CppObject * (*) (DictionaryReplaceEvent_2_t3778927063 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)((DictionaryReplaceEvent_2_t3778927063 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		Il2CppObject * L_1 = ((  Il2CppObject * (*) (DictionaryReplaceEvent_2_t3778927063 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->method)((DictionaryReplaceEvent_2_t3778927063 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		Il2CppObject * L_2 = ((  Il2CppObject * (*) (DictionaryReplaceEvent_2_t3778927063 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7)->method)((DictionaryReplaceEvent_2_t3778927063 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7));
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_3 = String_Format_m3928391288(NULL /*static, unused*/, (String_t*)_stringLiteral263864371, (Il2CppObject *)L_0, (Il2CppObject *)L_1, (Il2CppObject *)L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
// System.Int32 UniRx.DictionaryReplaceEvent`2<System.Object,System.Object>::GetHashCode()
extern "C"  int32_t DictionaryReplaceEvent_2_GetHashCode_m3666249368_gshared (DictionaryReplaceEvent_2_t3778927063 * __this, const MethodInfo* method)
{
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9));
		EqualityComparer_1_t271497070 * L_0 = ((  EqualityComparer_1_t271497070 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8)->method)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8));
		Il2CppObject * L_1 = ((  Il2CppObject * (*) (DictionaryReplaceEvent_2_t3778927063 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)((DictionaryReplaceEvent_2_t3778927063 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		NullCheck((EqualityComparer_1_t271497070 *)L_0);
		int32_t L_2 = VirtFuncInvoker1< int32_t, Il2CppObject * >::Invoke(8 /* System.Int32 System.Collections.Generic.EqualityComparer`1<System.Object>::GetHashCode(!0) */, (EqualityComparer_1_t271497070 *)L_0, (Il2CppObject *)L_1);
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 12));
		EqualityComparer_1_t271497070 * L_3 = ((  EqualityComparer_1_t271497070 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 11)->method)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 11));
		Il2CppObject * L_4 = ((  Il2CppObject * (*) (DictionaryReplaceEvent_2_t3778927063 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->method)((DictionaryReplaceEvent_2_t3778927063 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		NullCheck((EqualityComparer_1_t271497070 *)L_3);
		int32_t L_5 = VirtFuncInvoker1< int32_t, Il2CppObject * >::Invoke(8 /* System.Int32 System.Collections.Generic.EqualityComparer`1<System.Object>::GetHashCode(!0) */, (EqualityComparer_1_t271497070 *)L_3, (Il2CppObject *)L_4);
		EqualityComparer_1_t271497070 * L_6 = ((  EqualityComparer_1_t271497070 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 11)->method)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 11));
		Il2CppObject * L_7 = ((  Il2CppObject * (*) (DictionaryReplaceEvent_2_t3778927063 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7)->method)((DictionaryReplaceEvent_2_t3778927063 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7));
		NullCheck((EqualityComparer_1_t271497070 *)L_6);
		int32_t L_8 = VirtFuncInvoker1< int32_t, Il2CppObject * >::Invoke(8 /* System.Int32 System.Collections.Generic.EqualityComparer`1<System.Object>::GetHashCode(!0) */, (EqualityComparer_1_t271497070 *)L_6, (Il2CppObject *)L_7);
		return ((int32_t)((int32_t)((int32_t)((int32_t)L_2^(int32_t)((int32_t)((int32_t)L_5<<(int32_t)2))))^(int32_t)((int32_t)((int32_t)L_8>>(int32_t)2))));
	}
}
// System.Boolean UniRx.DictionaryReplaceEvent`2<System.Object,System.Object>::Equals(UniRx.DictionaryReplaceEvent`2<TKey,TValue>)
extern "C"  bool DictionaryReplaceEvent_2_Equals_m4080823976_gshared (DictionaryReplaceEvent_2_t3778927063 * __this, DictionaryReplaceEvent_2_t3778927063  ___other0, const MethodInfo* method)
{
	int32_t G_B4_0 = 0;
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9));
		EqualityComparer_1_t271497070 * L_0 = ((  EqualityComparer_1_t271497070 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8)->method)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8));
		Il2CppObject * L_1 = ((  Il2CppObject * (*) (DictionaryReplaceEvent_2_t3778927063 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)((DictionaryReplaceEvent_2_t3778927063 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		Il2CppObject * L_2 = ((  Il2CppObject * (*) (DictionaryReplaceEvent_2_t3778927063 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)((DictionaryReplaceEvent_2_t3778927063 *)(&___other0), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		NullCheck((EqualityComparer_1_t271497070 *)L_0);
		bool L_3 = VirtFuncInvoker2< bool, Il2CppObject *, Il2CppObject * >::Invoke(9 /* System.Boolean System.Collections.Generic.EqualityComparer`1<System.Object>::Equals(!0,!0) */, (EqualityComparer_1_t271497070 *)L_0, (Il2CppObject *)L_1, (Il2CppObject *)L_2);
		if (!L_3)
		{
			goto IL_0051;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 12));
		EqualityComparer_1_t271497070 * L_4 = ((  EqualityComparer_1_t271497070 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 11)->method)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 11));
		Il2CppObject * L_5 = ((  Il2CppObject * (*) (DictionaryReplaceEvent_2_t3778927063 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->method)((DictionaryReplaceEvent_2_t3778927063 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		Il2CppObject * L_6 = ((  Il2CppObject * (*) (DictionaryReplaceEvent_2_t3778927063 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->method)((DictionaryReplaceEvent_2_t3778927063 *)(&___other0), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		NullCheck((EqualityComparer_1_t271497070 *)L_4);
		bool L_7 = VirtFuncInvoker2< bool, Il2CppObject *, Il2CppObject * >::Invoke(9 /* System.Boolean System.Collections.Generic.EqualityComparer`1<System.Object>::Equals(!0,!0) */, (EqualityComparer_1_t271497070 *)L_4, (Il2CppObject *)L_5, (Il2CppObject *)L_6);
		if (!L_7)
		{
			goto IL_0051;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 12));
		EqualityComparer_1_t271497070 * L_8 = ((  EqualityComparer_1_t271497070 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 11)->method)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 11));
		Il2CppObject * L_9 = ((  Il2CppObject * (*) (DictionaryReplaceEvent_2_t3778927063 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7)->method)((DictionaryReplaceEvent_2_t3778927063 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7));
		Il2CppObject * L_10 = ((  Il2CppObject * (*) (DictionaryReplaceEvent_2_t3778927063 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7)->method)((DictionaryReplaceEvent_2_t3778927063 *)(&___other0), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7));
		NullCheck((EqualityComparer_1_t271497070 *)L_8);
		bool L_11 = VirtFuncInvoker2< bool, Il2CppObject *, Il2CppObject * >::Invoke(9 /* System.Boolean System.Collections.Generic.EqualityComparer`1<System.Object>::Equals(!0,!0) */, (EqualityComparer_1_t271497070 *)L_8, (Il2CppObject *)L_9, (Il2CppObject *)L_10);
		G_B4_0 = ((int32_t)(L_11));
		goto IL_0052;
	}

IL_0051:
	{
		G_B4_0 = 0;
	}

IL_0052:
	{
		return (bool)G_B4_0;
	}
}
// System.Void UniRx.DisposableExtensions/<AddTo>c__AnonStorey36`1<System.Object>::.ctor()
extern "C"  void U3CAddToU3Ec__AnonStorey36_1__ctor_m3746277587_gshared (U3CAddToU3Ec__AnonStorey36_1_t595347342 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UniRx.DisposableExtensions/<AddTo>c__AnonStorey36`1<System.Object>::<>m__3A(UniRx.Unit)
extern Il2CppClass* IDisposable_t1628921374_il2cpp_TypeInfo_var;
extern const uint32_t U3CAddToU3Ec__AnonStorey36_1_U3CU3Em__3A_m2546724526_MetadataUsageId;
extern "C"  void U3CAddToU3Ec__AnonStorey36_1_U3CU3Em__3A_m2546724526_gshared (U3CAddToU3Ec__AnonStorey36_1_t595347342 * __this, Unit_t2558286038  ____0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CAddToU3Ec__AnonStorey36_1_U3CU3Em__3A_m2546724526_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject ** L_0 = (Il2CppObject **)__this->get_address_of_disposable_0();
		NullCheck((Il2CppObject *)(*L_0));
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, (Il2CppObject *)(*L_0));
		return;
	}
}
// System.Void UniRx.EventPattern`1<System.Object>::.ctor(System.Object,TEventArgs)
extern "C"  void EventPattern_1__ctor_m3160314633_gshared (EventPattern_1_t2903003978 * __this, Il2CppObject * ___sender0, Il2CppObject * ___e1, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___sender0;
		Il2CppObject * L_1 = ___e1;
		NullCheck((EventPattern_2_t3939142245 *)__this);
		((  void (*) (EventPattern_2_t3939142245 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((EventPattern_2_t3939142245 *)__this, (Il2CppObject *)L_0, (Il2CppObject *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		return;
	}
}
// System.Void UniRx.EventPattern`2<System.Object,System.Object>::.ctor(TSender,TEventArgs)
extern "C"  void EventPattern_2__ctor_m3419812743_gshared (EventPattern_2_t3939142245 * __this, Il2CppObject * ___sender0, Il2CppObject * ___e1, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		Il2CppObject * L_0 = ___sender0;
		NullCheck((EventPattern_2_t3939142245 *)__this);
		((  void (*) (EventPattern_2_t3939142245 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((EventPattern_2_t3939142245 *)__this, (Il2CppObject *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		Il2CppObject * L_1 = ___e1;
		NullCheck((EventPattern_2_t3939142245 *)__this);
		((  void (*) (EventPattern_2_t3939142245 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)((EventPattern_2_t3939142245 *)__this, (Il2CppObject *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		return;
	}
}
// TSender UniRx.EventPattern`2<System.Object,System.Object>::get_Sender()
extern "C"  Il2CppObject * EventPattern_2_get_Sender_m2972234443_gshared (EventPattern_2_t3939142245 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_U3CSenderU3Ek__BackingField_0();
		return L_0;
	}
}
// System.Void UniRx.EventPattern`2<System.Object,System.Object>::set_Sender(TSender)
extern "C"  void EventPattern_2_set_Sender_m3333931200_gshared (EventPattern_2_t3939142245 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___value0;
		__this->set_U3CSenderU3Ek__BackingField_0(L_0);
		return;
	}
}
// TEventArgs UniRx.EventPattern`2<System.Object,System.Object>::get_EventArgs()
extern "C"  Il2CppObject * EventPattern_2_get_EventArgs_m3420313211_gshared (EventPattern_2_t3939142245 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_U3CEventArgsU3Ek__BackingField_1();
		return L_0;
	}
}
// System.Void UniRx.EventPattern`2<System.Object,System.Object>::set_EventArgs(TEventArgs)
extern "C"  void EventPattern_2_set_EventArgs_m1298775714_gshared (EventPattern_2_t3939142245 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___value0;
		__this->set_U3CEventArgsU3Ek__BackingField_1(L_0);
		return;
	}
}
// System.Boolean UniRx.EventPattern`2<System.Object,System.Object>::Equals(UniRx.EventPattern`2<TSender,TEventArgs>)
extern "C"  bool EventPattern_2_Equals_m2393598654_gshared (EventPattern_2_t3939142245 * __this, EventPattern_2_t3939142245 * ___other0, const MethodInfo* method)
{
	int32_t G_B7_0 = 0;
	{
		EventPattern_2_t3939142245 * L_0 = ___other0;
		bool L_1 = Object_ReferenceEquals_m3695130242(NULL /*static, unused*/, (Il2CppObject *)NULL, (Il2CppObject *)L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_000e;
		}
	}
	{
		return (bool)0;
	}

IL_000e:
	{
		EventPattern_2_t3939142245 * L_2 = ___other0;
		bool L_3 = Object_ReferenceEquals_m3695130242(NULL /*static, unused*/, (Il2CppObject *)__this, (Il2CppObject *)L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_001c;
		}
	}
	{
		return (bool)1;
	}

IL_001c:
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		EqualityComparer_1_t271497070 * L_4 = ((  EqualityComparer_1_t271497070 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		NullCheck((EventPattern_2_t3939142245 *)__this);
		Il2CppObject * L_5 = VirtFuncInvoker0< Il2CppObject * >::Invoke(5 /* TSender UniRx.EventPattern`2<System.Object,System.Object>::get_Sender() */, (EventPattern_2_t3939142245 *)__this);
		EventPattern_2_t3939142245 * L_6 = ___other0;
		NullCheck((EventPattern_2_t3939142245 *)L_6);
		Il2CppObject * L_7 = VirtFuncInvoker0< Il2CppObject * >::Invoke(5 /* TSender UniRx.EventPattern`2<System.Object,System.Object>::get_Sender() */, (EventPattern_2_t3939142245 *)L_6);
		NullCheck((EqualityComparer_1_t271497070 *)L_4);
		bool L_8 = VirtFuncInvoker2< bool, Il2CppObject *, Il2CppObject * >::Invoke(9 /* System.Boolean System.Collections.Generic.EqualityComparer`1<System.Object>::Equals(!0,!0) */, (EqualityComparer_1_t271497070 *)L_4, (Il2CppObject *)L_5, (Il2CppObject *)L_7);
		if (!L_8)
		{
			goto IL_004f;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7));
		EqualityComparer_1_t271497070 * L_9 = ((  EqualityComparer_1_t271497070 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6)->method)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		NullCheck((EventPattern_2_t3939142245 *)__this);
		Il2CppObject * L_10 = VirtFuncInvoker0< Il2CppObject * >::Invoke(6 /* TEventArgs UniRx.EventPattern`2<System.Object,System.Object>::get_EventArgs() */, (EventPattern_2_t3939142245 *)__this);
		EventPattern_2_t3939142245 * L_11 = ___other0;
		NullCheck((EventPattern_2_t3939142245 *)L_11);
		Il2CppObject * L_12 = VirtFuncInvoker0< Il2CppObject * >::Invoke(6 /* TEventArgs UniRx.EventPattern`2<System.Object,System.Object>::get_EventArgs() */, (EventPattern_2_t3939142245 *)L_11);
		NullCheck((EqualityComparer_1_t271497070 *)L_9);
		bool L_13 = VirtFuncInvoker2< bool, Il2CppObject *, Il2CppObject * >::Invoke(9 /* System.Boolean System.Collections.Generic.EqualityComparer`1<System.Object>::Equals(!0,!0) */, (EqualityComparer_1_t271497070 *)L_9, (Il2CppObject *)L_10, (Il2CppObject *)L_12);
		G_B7_0 = ((int32_t)(L_13));
		goto IL_0050;
	}

IL_004f:
	{
		G_B7_0 = 0;
	}

IL_0050:
	{
		return (bool)G_B7_0;
	}
}
// System.Boolean UniRx.EventPattern`2<System.Object,System.Object>::Equals(System.Object)
extern "C"  bool EventPattern_2_Equals_m4062083866_gshared (EventPattern_2_t3939142245 * __this, Il2CppObject * ___obj0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___obj0;
		NullCheck((EventPattern_2_t3939142245 *)__this);
		bool L_1 = VirtFuncInvoker1< bool, EventPattern_2_t3939142245 * >::Invoke(4 /* System.Boolean UniRx.EventPattern`2<System.Object,System.Object>::Equals(UniRx.EventPattern`2<TSender,TEventArgs>) */, (EventPattern_2_t3939142245 *)__this, (EventPattern_2_t3939142245 *)((EventPattern_2_t3939142245 *)IsInst(L_0, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10))));
		return L_1;
	}
}
// System.Int32 UniRx.EventPattern`2<System.Object,System.Object>::GetHashCode()
extern "C"  int32_t EventPattern_2_GetHashCode_m2584518450_gshared (EventPattern_2_t3939142245 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		EqualityComparer_1_t271497070 * L_0 = ((  EqualityComparer_1_t271497070 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		NullCheck((EventPattern_2_t3939142245 *)__this);
		Il2CppObject * L_1 = VirtFuncInvoker0< Il2CppObject * >::Invoke(5 /* TSender UniRx.EventPattern`2<System.Object,System.Object>::get_Sender() */, (EventPattern_2_t3939142245 *)__this);
		NullCheck((EqualityComparer_1_t271497070 *)L_0);
		int32_t L_2 = VirtFuncInvoker1< int32_t, Il2CppObject * >::Invoke(8 /* System.Int32 System.Collections.Generic.EqualityComparer`1<System.Object>::GetHashCode(!0) */, (EqualityComparer_1_t271497070 *)L_0, (Il2CppObject *)L_1);
		V_0 = (int32_t)L_2;
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7));
		EqualityComparer_1_t271497070 * L_3 = ((  EqualityComparer_1_t271497070 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6)->method)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		NullCheck((EventPattern_2_t3939142245 *)__this);
		Il2CppObject * L_4 = VirtFuncInvoker0< Il2CppObject * >::Invoke(6 /* TEventArgs UniRx.EventPattern`2<System.Object,System.Object>::get_EventArgs() */, (EventPattern_2_t3939142245 *)__this);
		NullCheck((EqualityComparer_1_t271497070 *)L_3);
		int32_t L_5 = VirtFuncInvoker1< int32_t, Il2CppObject * >::Invoke(8 /* System.Int32 System.Collections.Generic.EqualityComparer`1<System.Object>::GetHashCode(!0) */, (EqualityComparer_1_t271497070 *)L_3, (Il2CppObject *)L_4);
		V_1 = (int32_t)L_5;
		int32_t L_6 = V_0;
		int32_t L_7 = V_0;
		int32_t L_8 = V_1;
		return ((int32_t)((int32_t)((int32_t)((int32_t)L_6<<(int32_t)5))+(int32_t)((int32_t)((int32_t)L_7^(int32_t)L_8))));
	}
}
// System.Boolean UniRx.EventPattern`2<System.Object,System.Object>::op_Equality(UniRx.EventPattern`2<TSender,TEventArgs>,UniRx.EventPattern`2<TSender,TEventArgs>)
extern "C"  bool EventPattern_2_op_Equality_m1906571143_gshared (Il2CppObject * __this /* static, unused */, EventPattern_2_t3939142245 * ___first0, EventPattern_2_t3939142245 * ___second1, const MethodInfo* method)
{
	{
		EventPattern_2_t3939142245 * L_0 = ___first0;
		EventPattern_2_t3939142245 * L_1 = ___second1;
		bool L_2 = Object_Equals_m3175838359(NULL /*static, unused*/, (Il2CppObject *)L_0, (Il2CppObject *)L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Boolean UniRx.EventPattern`2<System.Object,System.Object>::op_Inequality(UniRx.EventPattern`2<TSender,TEventArgs>,UniRx.EventPattern`2<TSender,TEventArgs>)
extern "C"  bool EventPattern_2_op_Inequality_m3770633282_gshared (Il2CppObject * __this /* static, unused */, EventPattern_2_t3939142245 * ___first0, EventPattern_2_t3939142245 * ___second1, const MethodInfo* method)
{
	{
		EventPattern_2_t3939142245 * L_0 = ___first0;
		EventPattern_2_t3939142245 * L_1 = ___second1;
		bool L_2 = Object_Equals_m3175838359(NULL /*static, unused*/, (Il2CppObject *)L_0, (Il2CppObject *)L_1, /*hidden argument*/NULL);
		return (bool)((((int32_t)L_2) == ((int32_t)0))? 1 : 0);
	}
}
// System.Void UniRx.InternalUtil.DisposedObserver`1<System.Boolean>::.ctor()
extern "C"  void DisposedObserver_1__ctor_m4030990875_gshared (DisposedObserver_1_t3680430099 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UniRx.InternalUtil.DisposedObserver`1<System.Boolean>::.cctor()
extern "C"  void DisposedObserver_1__cctor_m4219536626_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		DisposedObserver_1_t3680430099 * L_0 = (DisposedObserver_1_t3680430099 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		((  void (*) (DisposedObserver_1_t3680430099 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		((DisposedObserver_1_t3680430099_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->static_fields)->set_Instance_0(L_0);
		return;
	}
}
// System.Void UniRx.InternalUtil.DisposedObserver`1<System.Boolean>::OnCompleted()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* ObjectDisposedException_t973246880_il2cpp_TypeInfo_var;
extern const uint32_t DisposedObserver_1_OnCompleted_m2965896549_MetadataUsageId;
extern "C"  void DisposedObserver_1_OnCompleted_m2965896549_gshared (DisposedObserver_1_t3680430099 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DisposedObserver_1_OnCompleted_m2965896549_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_0 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		ObjectDisposedException_t973246880 * L_1 = (ObjectDisposedException_t973246880 *)il2cpp_codegen_object_new(ObjectDisposedException_t973246880_il2cpp_TypeInfo_var);
		ObjectDisposedException__ctor_m1180707260(L_1, (String_t*)L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}
}
// System.Void UniRx.InternalUtil.DisposedObserver`1<System.Boolean>::OnError(System.Exception)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* ObjectDisposedException_t973246880_il2cpp_TypeInfo_var;
extern const uint32_t DisposedObserver_1_OnError_m1913700114_MetadataUsageId;
extern "C"  void DisposedObserver_1_OnError_m1913700114_gshared (DisposedObserver_1_t3680430099 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DisposedObserver_1_OnError_m1913700114_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_0 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		ObjectDisposedException_t973246880 * L_1 = (ObjectDisposedException_t973246880 *)il2cpp_codegen_object_new(ObjectDisposedException_t973246880_il2cpp_TypeInfo_var);
		ObjectDisposedException__ctor_m1180707260(L_1, (String_t*)L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}
}
// System.Void UniRx.InternalUtil.DisposedObserver`1<System.Boolean>::OnNext(T)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* ObjectDisposedException_t973246880_il2cpp_TypeInfo_var;
extern const uint32_t DisposedObserver_1_OnNext_m2552433155_MetadataUsageId;
extern "C"  void DisposedObserver_1_OnNext_m2552433155_gshared (DisposedObserver_1_t3680430099 * __this, bool ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DisposedObserver_1_OnNext_m2552433155_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_0 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		ObjectDisposedException_t973246880 * L_1 = (ObjectDisposedException_t973246880 *)il2cpp_codegen_object_new(ObjectDisposedException_t973246880_il2cpp_TypeInfo_var);
		ObjectDisposedException__ctor_m1180707260(L_1, (String_t*)L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}
}
// System.Void UniRx.InternalUtil.DisposedObserver`1<System.Byte>::.ctor()
extern "C"  void DisposedObserver_1__ctor_m1410718175_gshared (DisposedObserver_1_t1953151283 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UniRx.InternalUtil.DisposedObserver`1<System.Byte>::.cctor()
extern "C"  void DisposedObserver_1__cctor_m300494254_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		DisposedObserver_1_t1953151283 * L_0 = (DisposedObserver_1_t1953151283 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		((  void (*) (DisposedObserver_1_t1953151283 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		((DisposedObserver_1_t1953151283_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->static_fields)->set_Instance_0(L_0);
		return;
	}
}
// System.Void UniRx.InternalUtil.DisposedObserver`1<System.Byte>::OnCompleted()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* ObjectDisposedException_t973246880_il2cpp_TypeInfo_var;
extern const uint32_t DisposedObserver_1_OnCompleted_m3725060649_MetadataUsageId;
extern "C"  void DisposedObserver_1_OnCompleted_m3725060649_gshared (DisposedObserver_1_t1953151283 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DisposedObserver_1_OnCompleted_m3725060649_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_0 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		ObjectDisposedException_t973246880 * L_1 = (ObjectDisposedException_t973246880 *)il2cpp_codegen_object_new(ObjectDisposedException_t973246880_il2cpp_TypeInfo_var);
		ObjectDisposedException__ctor_m1180707260(L_1, (String_t*)L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}
}
// System.Void UniRx.InternalUtil.DisposedObserver`1<System.Byte>::OnError(System.Exception)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* ObjectDisposedException_t973246880_il2cpp_TypeInfo_var;
extern const uint32_t DisposedObserver_1_OnError_m522167766_MetadataUsageId;
extern "C"  void DisposedObserver_1_OnError_m522167766_gshared (DisposedObserver_1_t1953151283 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DisposedObserver_1_OnError_m522167766_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_0 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		ObjectDisposedException_t973246880 * L_1 = (ObjectDisposedException_t973246880 *)il2cpp_codegen_object_new(ObjectDisposedException_t973246880_il2cpp_TypeInfo_var);
		ObjectDisposedException__ctor_m1180707260(L_1, (String_t*)L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}
}
// System.Void UniRx.InternalUtil.DisposedObserver`1<System.Byte>::OnNext(T)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* ObjectDisposedException_t973246880_il2cpp_TypeInfo_var;
extern const uint32_t DisposedObserver_1_OnNext_m1321203911_MetadataUsageId;
extern "C"  void DisposedObserver_1_OnNext_m1321203911_gshared (DisposedObserver_1_t1953151283 * __this, uint8_t ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DisposedObserver_1_OnNext_m1321203911_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_0 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		ObjectDisposedException_t973246880 * L_1 = (ObjectDisposedException_t973246880 *)il2cpp_codegen_object_new(ObjectDisposedException_t973246880_il2cpp_TypeInfo_var);
		ObjectDisposedException__ctor_m1180707260(L_1, (String_t*)L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}
}
// System.Void UniRx.InternalUtil.DisposedObserver`1<System.Double>::.ctor()
extern "C"  void DisposedObserver_1__ctor_m4150059240_gshared (DisposedObserver_1_t4003941372 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UniRx.InternalUtil.DisposedObserver`1<System.Double>::.cctor()
extern "C"  void DisposedObserver_1__cctor_m3615688645_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		DisposedObserver_1_t4003941372 * L_0 = (DisposedObserver_1_t4003941372 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		((  void (*) (DisposedObserver_1_t4003941372 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		((DisposedObserver_1_t4003941372_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->static_fields)->set_Instance_0(L_0);
		return;
	}
}
// System.Void UniRx.InternalUtil.DisposedObserver`1<System.Double>::OnCompleted()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* ObjectDisposedException_t973246880_il2cpp_TypeInfo_var;
extern const uint32_t DisposedObserver_1_OnCompleted_m3620062834_MetadataUsageId;
extern "C"  void DisposedObserver_1_OnCompleted_m3620062834_gshared (DisposedObserver_1_t4003941372 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DisposedObserver_1_OnCompleted_m3620062834_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_0 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		ObjectDisposedException_t973246880 * L_1 = (ObjectDisposedException_t973246880 *)il2cpp_codegen_object_new(ObjectDisposedException_t973246880_il2cpp_TypeInfo_var);
		ObjectDisposedException__ctor_m1180707260(L_1, (String_t*)L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}
}
// System.Void UniRx.InternalUtil.DisposedObserver`1<System.Double>::OnError(System.Exception)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* ObjectDisposedException_t973246880_il2cpp_TypeInfo_var;
extern const uint32_t DisposedObserver_1_OnError_m38821023_MetadataUsageId;
extern "C"  void DisposedObserver_1_OnError_m38821023_gshared (DisposedObserver_1_t4003941372 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DisposedObserver_1_OnError_m38821023_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_0 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		ObjectDisposedException_t973246880 * L_1 = (ObjectDisposedException_t973246880 *)il2cpp_codegen_object_new(ObjectDisposedException_t973246880_il2cpp_TypeInfo_var);
		ObjectDisposedException__ctor_m1180707260(L_1, (String_t*)L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}
}
// System.Void UniRx.InternalUtil.DisposedObserver`1<System.Double>::OnNext(T)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* ObjectDisposedException_t973246880_il2cpp_TypeInfo_var;
extern const uint32_t DisposedObserver_1_OnNext_m1013014928_MetadataUsageId;
extern "C"  void DisposedObserver_1_OnNext_m1013014928_gshared (DisposedObserver_1_t4003941372 * __this, double ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DisposedObserver_1_OnNext_m1013014928_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_0 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		ObjectDisposedException_t973246880 * L_1 = (ObjectDisposedException_t973246880 *)il2cpp_codegen_object_new(ObjectDisposedException_t973246880_il2cpp_TypeInfo_var);
		ObjectDisposedException__ctor_m1180707260(L_1, (String_t*)L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}
}
// System.Void UniRx.InternalUtil.DisposedObserver`1<System.Int32>::.ctor()
extern "C"  void DisposedObserver_1__ctor_m318789761_gshared (DisposedObserver_1_t2021872249 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UniRx.InternalUtil.DisposedObserver`1<System.Int32>::.cctor()
extern "C"  void DisposedObserver_1__cctor_m810451788_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		DisposedObserver_1_t2021872249 * L_0 = (DisposedObserver_1_t2021872249 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		((  void (*) (DisposedObserver_1_t2021872249 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		((DisposedObserver_1_t2021872249_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->static_fields)->set_Instance_0(L_0);
		return;
	}
}
// System.Void UniRx.InternalUtil.DisposedObserver`1<System.Int32>::OnCompleted()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* ObjectDisposedException_t973246880_il2cpp_TypeInfo_var;
extern const uint32_t DisposedObserver_1_OnCompleted_m273508171_MetadataUsageId;
extern "C"  void DisposedObserver_1_OnCompleted_m273508171_gshared (DisposedObserver_1_t2021872249 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DisposedObserver_1_OnCompleted_m273508171_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_0 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		ObjectDisposedException_t973246880 * L_1 = (ObjectDisposedException_t973246880 *)il2cpp_codegen_object_new(ObjectDisposedException_t973246880_il2cpp_TypeInfo_var);
		ObjectDisposedException__ctor_m1180707260(L_1, (String_t*)L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}
}
// System.Void UniRx.InternalUtil.DisposedObserver`1<System.Int32>::OnError(System.Exception)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* ObjectDisposedException_t973246880_il2cpp_TypeInfo_var;
extern const uint32_t DisposedObserver_1_OnError_m3111682040_MetadataUsageId;
extern "C"  void DisposedObserver_1_OnError_m3111682040_gshared (DisposedObserver_1_t2021872249 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DisposedObserver_1_OnError_m3111682040_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_0 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		ObjectDisposedException_t973246880 * L_1 = (ObjectDisposedException_t973246880 *)il2cpp_codegen_object_new(ObjectDisposedException_t973246880_il2cpp_TypeInfo_var);
		ObjectDisposedException__ctor_m1180707260(L_1, (String_t*)L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}
}
// System.Void UniRx.InternalUtil.DisposedObserver`1<System.Int32>::OnNext(T)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* ObjectDisposedException_t973246880_il2cpp_TypeInfo_var;
extern const uint32_t DisposedObserver_1_OnNext_m4244985577_MetadataUsageId;
extern "C"  void DisposedObserver_1_OnNext_m4244985577_gshared (DisposedObserver_1_t2021872249 * __this, int32_t ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DisposedObserver_1_OnNext_m4244985577_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_0 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		ObjectDisposedException_t973246880 * L_1 = (ObjectDisposedException_t973246880 *)il2cpp_codegen_object_new(ObjectDisposedException_t973246880_il2cpp_TypeInfo_var);
		ObjectDisposedException__ctor_m1180707260(L_1, (String_t*)L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}
}
// System.Void UniRx.InternalUtil.DisposedObserver`1<System.Int64>::.ctor()
extern "C"  void DisposedObserver_1__ctor_m1407102496_gshared (DisposedObserver_1_t2021872344 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UniRx.InternalUtil.DisposedObserver`1<System.Int64>::.cctor()
extern "C"  void DisposedObserver_1__cctor_m188408205_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		DisposedObserver_1_t2021872344 * L_0 = (DisposedObserver_1_t2021872344 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		((  void (*) (DisposedObserver_1_t2021872344 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		((DisposedObserver_1_t2021872344_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->static_fields)->set_Instance_0(L_0);
		return;
	}
}
// System.Void UniRx.InternalUtil.DisposedObserver`1<System.Int64>::OnCompleted()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* ObjectDisposedException_t973246880_il2cpp_TypeInfo_var;
extern const uint32_t DisposedObserver_1_OnCompleted_m4283877802_MetadataUsageId;
extern "C"  void DisposedObserver_1_OnCompleted_m4283877802_gshared (DisposedObserver_1_t2021872344 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DisposedObserver_1_OnCompleted_m4283877802_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_0 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		ObjectDisposedException_t973246880 * L_1 = (ObjectDisposedException_t973246880 *)il2cpp_codegen_object_new(ObjectDisposedException_t973246880_il2cpp_TypeInfo_var);
		ObjectDisposedException__ctor_m1180707260(L_1, (String_t*)L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}
}
// System.Void UniRx.InternalUtil.DisposedObserver`1<System.Int64>::OnError(System.Exception)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* ObjectDisposedException_t973246880_il2cpp_TypeInfo_var;
extern const uint32_t DisposedObserver_1_OnError_m236866519_MetadataUsageId;
extern "C"  void DisposedObserver_1_OnError_m236866519_gshared (DisposedObserver_1_t2021872344 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DisposedObserver_1_OnError_m236866519_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_0 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		ObjectDisposedException_t973246880 * L_1 = (ObjectDisposedException_t973246880 *)il2cpp_codegen_object_new(ObjectDisposedException_t973246880_il2cpp_TypeInfo_var);
		ObjectDisposedException__ctor_m1180707260(L_1, (String_t*)L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}
}
// System.Void UniRx.InternalUtil.DisposedObserver`1<System.Int64>::OnNext(T)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* ObjectDisposedException_t973246880_il2cpp_TypeInfo_var;
extern const uint32_t DisposedObserver_1_OnNext_m2141503688_MetadataUsageId;
extern "C"  void DisposedObserver_1_OnNext_m2141503688_gshared (DisposedObserver_1_t2021872344 * __this, int64_t ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DisposedObserver_1_OnNext_m2141503688_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_0 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		ObjectDisposedException_t973246880 * L_1 = (ObjectDisposedException_t973246880 *)il2cpp_codegen_object_new(ObjectDisposedException_t973246880_il2cpp_TypeInfo_var);
		ObjectDisposedException__ctor_m1180707260(L_1, (String_t*)L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}
}
// System.Void UniRx.InternalUtil.DisposedObserver`1<System.Object>::.ctor()
extern "C"  void DisposedObserver_1__ctor_m2745571030_gshared (DisposedObserver_1_t11563882 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UniRx.InternalUtil.DisposedObserver`1<System.Object>::.cctor()
extern "C"  void DisposedObserver_1__cctor_m3026227095_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		DisposedObserver_1_t11563882 * L_0 = (DisposedObserver_1_t11563882 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		((  void (*) (DisposedObserver_1_t11563882 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		((DisposedObserver_1_t11563882_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->static_fields)->set_Instance_0(L_0);
		return;
	}
}
// System.Void UniRx.InternalUtil.DisposedObserver`1<System.Object>::OnCompleted()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* ObjectDisposedException_t973246880_il2cpp_TypeInfo_var;
extern const uint32_t DisposedObserver_1_OnCompleted_m1100894688_MetadataUsageId;
extern "C"  void DisposedObserver_1_OnCompleted_m1100894688_gshared (DisposedObserver_1_t11563882 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DisposedObserver_1_OnCompleted_m1100894688_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_0 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		ObjectDisposedException_t973246880 * L_1 = (ObjectDisposedException_t973246880 *)il2cpp_codegen_object_new(ObjectDisposedException_t973246880_il2cpp_TypeInfo_var);
		ObjectDisposedException__ctor_m1180707260(L_1, (String_t*)L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}
}
// System.Void UniRx.InternalUtil.DisposedObserver`1<System.Object>::OnError(System.Exception)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* ObjectDisposedException_t973246880_il2cpp_TypeInfo_var;
extern const uint32_t DisposedObserver_1_OnError_m150811405_MetadataUsageId;
extern "C"  void DisposedObserver_1_OnError_m150811405_gshared (DisposedObserver_1_t11563882 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DisposedObserver_1_OnError_m150811405_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_0 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		ObjectDisposedException_t973246880 * L_1 = (ObjectDisposedException_t973246880 *)il2cpp_codegen_object_new(ObjectDisposedException_t973246880_il2cpp_TypeInfo_var);
		ObjectDisposedException__ctor_m1180707260(L_1, (String_t*)L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}
}
// System.Void UniRx.InternalUtil.DisposedObserver`1<System.Object>::OnNext(T)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* ObjectDisposedException_t973246880_il2cpp_TypeInfo_var;
extern const uint32_t DisposedObserver_1_OnNext_m4214543358_MetadataUsageId;
extern "C"  void DisposedObserver_1_OnNext_m4214543358_gshared (DisposedObserver_1_t11563882 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DisposedObserver_1_OnNext_m4214543358_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_0 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		ObjectDisposedException_t973246880 * L_1 = (ObjectDisposedException_t973246880 *)il2cpp_codegen_object_new(ObjectDisposedException_t973246880_il2cpp_TypeInfo_var);
		ObjectDisposedException__ctor_m1180707260(L_1, (String_t*)L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}
}
// System.Void UniRx.InternalUtil.DisposedObserver`1<System.Single>::.ctor()
extern "C"  void DisposedObserver_1__ctor_m2403532959_gshared (DisposedObserver_1_t132666483 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UniRx.InternalUtil.DisposedObserver`1<System.Single>::.cctor()
extern "C"  void DisposedObserver_1__cctor_m1012981486_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		DisposedObserver_1_t132666483 * L_0 = (DisposedObserver_1_t132666483 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		((  void (*) (DisposedObserver_1_t132666483 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		((DisposedObserver_1_t132666483_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->static_fields)->set_Instance_0(L_0);
		return;
	}
}
// System.Void UniRx.InternalUtil.DisposedObserver`1<System.Single>::OnCompleted()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* ObjectDisposedException_t973246880_il2cpp_TypeInfo_var;
extern const uint32_t DisposedObserver_1_OnCompleted_m485294313_MetadataUsageId;
extern "C"  void DisposedObserver_1_OnCompleted_m485294313_gshared (DisposedObserver_1_t132666483 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DisposedObserver_1_OnCompleted_m485294313_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_0 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		ObjectDisposedException_t973246880 * L_1 = (ObjectDisposedException_t973246880 *)il2cpp_codegen_object_new(ObjectDisposedException_t973246880_il2cpp_TypeInfo_var);
		ObjectDisposedException__ctor_m1180707260(L_1, (String_t*)L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}
}
// System.Void UniRx.InternalUtil.DisposedObserver`1<System.Single>::OnError(System.Exception)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* ObjectDisposedException_t973246880_il2cpp_TypeInfo_var;
extern const uint32_t DisposedObserver_1_OnError_m1398316182_MetadataUsageId;
extern "C"  void DisposedObserver_1_OnError_m1398316182_gshared (DisposedObserver_1_t132666483 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DisposedObserver_1_OnError_m1398316182_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_0 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		ObjectDisposedException_t973246880 * L_1 = (ObjectDisposedException_t973246880 *)il2cpp_codegen_object_new(ObjectDisposedException_t973246880_il2cpp_TypeInfo_var);
		ObjectDisposedException__ctor_m1180707260(L_1, (String_t*)L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}
}
// System.Void UniRx.InternalUtil.DisposedObserver`1<System.Single>::OnNext(T)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* ObjectDisposedException_t973246880_il2cpp_TypeInfo_var;
extern const uint32_t DisposedObserver_1_OnNext_m1933471623_MetadataUsageId;
extern "C"  void DisposedObserver_1_OnNext_m1933471623_gshared (DisposedObserver_1_t132666483 * __this, float ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DisposedObserver_1_OnNext_m1933471623_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_0 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		ObjectDisposedException_t973246880 * L_1 = (ObjectDisposedException_t973246880 *)il2cpp_codegen_object_new(ObjectDisposedException_t973246880_il2cpp_TypeInfo_var);
		ObjectDisposedException__ctor_m1180707260(L_1, (String_t*)L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}
}
// System.Void UniRx.InternalUtil.DisposedObserver`1<TableButtons/StatkaPoTableViev>::.ctor()
extern "C"  void DisposedObserver_1__ctor_m607057855_gshared (DisposedObserver_1_t2682853279 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UniRx.InternalUtil.DisposedObserver`1<TableButtons/StatkaPoTableViev>::.cctor()
extern "C"  void DisposedObserver_1__cctor_m1156828110_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		DisposedObserver_1_t2682853279 * L_0 = (DisposedObserver_1_t2682853279 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		((  void (*) (DisposedObserver_1_t2682853279 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		((DisposedObserver_1_t2682853279_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->static_fields)->set_Instance_0(L_0);
		return;
	}
}
// System.Void UniRx.InternalUtil.DisposedObserver`1<TableButtons/StatkaPoTableViev>::OnCompleted()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* ObjectDisposedException_t973246880_il2cpp_TypeInfo_var;
extern const uint32_t DisposedObserver_1_OnCompleted_m3582664713_MetadataUsageId;
extern "C"  void DisposedObserver_1_OnCompleted_m3582664713_gshared (DisposedObserver_1_t2682853279 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DisposedObserver_1_OnCompleted_m3582664713_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_0 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		ObjectDisposedException_t973246880 * L_1 = (ObjectDisposedException_t973246880 *)il2cpp_codegen_object_new(ObjectDisposedException_t973246880_il2cpp_TypeInfo_var);
		ObjectDisposedException__ctor_m1180707260(L_1, (String_t*)L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}
}
// System.Void UniRx.InternalUtil.DisposedObserver`1<TableButtons/StatkaPoTableViev>::OnError(System.Exception)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* ObjectDisposedException_t973246880_il2cpp_TypeInfo_var;
extern const uint32_t DisposedObserver_1_OnError_m3173059510_MetadataUsageId;
extern "C"  void DisposedObserver_1_OnError_m3173059510_gshared (DisposedObserver_1_t2682853279 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DisposedObserver_1_OnError_m3173059510_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_0 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		ObjectDisposedException_t973246880 * L_1 = (ObjectDisposedException_t973246880 *)il2cpp_codegen_object_new(ObjectDisposedException_t973246880_il2cpp_TypeInfo_var);
		ObjectDisposedException__ctor_m1180707260(L_1, (String_t*)L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}
}
// System.Void UniRx.InternalUtil.DisposedObserver`1<TableButtons/StatkaPoTableViev>::OnNext(T)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* ObjectDisposedException_t973246880_il2cpp_TypeInfo_var;
extern const uint32_t DisposedObserver_1_OnNext_m2097749671_MetadataUsageId;
extern "C"  void DisposedObserver_1_OnNext_m2097749671_gshared (DisposedObserver_1_t2682853279 * __this, int32_t ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DisposedObserver_1_OnNext_m2097749671_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_0 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		ObjectDisposedException_t973246880 * L_1 = (ObjectDisposedException_t973246880 *)il2cpp_codegen_object_new(ObjectDisposedException_t973246880_il2cpp_TypeInfo_var);
		ObjectDisposedException__ctor_m1180707260(L_1, (String_t*)L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}
}
// System.Void UniRx.InternalUtil.DisposedObserver`1<UniRx.CollectionAddEvent`1<System.Object>>::.ctor()
extern "C"  void DisposedObserver_1__ctor_m2621332788_gshared (DisposedObserver_1_t1590979449 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UniRx.InternalUtil.DisposedObserver`1<UniRx.CollectionAddEvent`1<System.Object>>::.cctor()
extern "C"  void DisposedObserver_1__cctor_m3469808889_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		DisposedObserver_1_t1590979449 * L_0 = (DisposedObserver_1_t1590979449 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		((  void (*) (DisposedObserver_1_t1590979449 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		((DisposedObserver_1_t1590979449_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->static_fields)->set_Instance_0(L_0);
		return;
	}
}
// System.Void UniRx.InternalUtil.DisposedObserver`1<UniRx.CollectionAddEvent`1<System.Object>>::OnCompleted()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* ObjectDisposedException_t973246880_il2cpp_TypeInfo_var;
extern const uint32_t DisposedObserver_1_OnCompleted_m3371424190_MetadataUsageId;
extern "C"  void DisposedObserver_1_OnCompleted_m3371424190_gshared (DisposedObserver_1_t1590979449 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DisposedObserver_1_OnCompleted_m3371424190_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_0 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		ObjectDisposedException_t973246880 * L_1 = (ObjectDisposedException_t973246880 *)il2cpp_codegen_object_new(ObjectDisposedException_t973246880_il2cpp_TypeInfo_var);
		ObjectDisposedException__ctor_m1180707260(L_1, (String_t*)L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}
}
// System.Void UniRx.InternalUtil.DisposedObserver`1<UniRx.CollectionAddEvent`1<System.Object>>::OnError(System.Exception)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* ObjectDisposedException_t973246880_il2cpp_TypeInfo_var;
extern const uint32_t DisposedObserver_1_OnError_m652205547_MetadataUsageId;
extern "C"  void DisposedObserver_1_OnError_m652205547_gshared (DisposedObserver_1_t1590979449 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DisposedObserver_1_OnError_m652205547_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_0 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		ObjectDisposedException_t973246880 * L_1 = (ObjectDisposedException_t973246880 *)il2cpp_codegen_object_new(ObjectDisposedException_t973246880_il2cpp_TypeInfo_var);
		ObjectDisposedException__ctor_m1180707260(L_1, (String_t*)L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}
}
// System.Void UniRx.InternalUtil.DisposedObserver`1<UniRx.CollectionAddEvent`1<System.Object>>::OnNext(T)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* ObjectDisposedException_t973246880_il2cpp_TypeInfo_var;
extern const uint32_t DisposedObserver_1_OnNext_m785709788_MetadataUsageId;
extern "C"  void DisposedObserver_1_OnNext_m785709788_gshared (DisposedObserver_1_t1590979449 * __this, CollectionAddEvent_1_t2416521987  ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DisposedObserver_1_OnNext_m785709788_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_0 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		ObjectDisposedException_t973246880 * L_1 = (ObjectDisposedException_t973246880 *)il2cpp_codegen_object_new(ObjectDisposedException_t973246880_il2cpp_TypeInfo_var);
		ObjectDisposedException__ctor_m1180707260(L_1, (String_t*)L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}
}
// System.Void UniRx.InternalUtil.DisposedObserver`1<UniRx.CollectionAddEvent`1<UniRx.Tuple`2<System.Object,System.Object>>>::.ctor()
extern "C"  void DisposedObserver_1__ctor_m850754732_gshared (DisposedObserver_1_t1123134848 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UniRx.InternalUtil.DisposedObserver`1<UniRx.CollectionAddEvent`1<UniRx.Tuple`2<System.Object,System.Object>>>::.cctor()
extern "C"  void DisposedObserver_1__cctor_m121496705_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		DisposedObserver_1_t1123134848 * L_0 = (DisposedObserver_1_t1123134848 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		((  void (*) (DisposedObserver_1_t1123134848 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		((DisposedObserver_1_t1123134848_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->static_fields)->set_Instance_0(L_0);
		return;
	}
}
// System.Void UniRx.InternalUtil.DisposedObserver`1<UniRx.CollectionAddEvent`1<UniRx.Tuple`2<System.Object,System.Object>>>::OnCompleted()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* ObjectDisposedException_t973246880_il2cpp_TypeInfo_var;
extern const uint32_t DisposedObserver_1_OnCompleted_m390299446_MetadataUsageId;
extern "C"  void DisposedObserver_1_OnCompleted_m390299446_gshared (DisposedObserver_1_t1123134848 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DisposedObserver_1_OnCompleted_m390299446_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_0 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		ObjectDisposedException_t973246880 * L_1 = (ObjectDisposedException_t973246880 *)il2cpp_codegen_object_new(ObjectDisposedException_t973246880_il2cpp_TypeInfo_var);
		ObjectDisposedException__ctor_m1180707260(L_1, (String_t*)L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}
}
// System.Void UniRx.InternalUtil.DisposedObserver`1<UniRx.CollectionAddEvent`1<UniRx.Tuple`2<System.Object,System.Object>>>::OnError(System.Exception)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* ObjectDisposedException_t973246880_il2cpp_TypeInfo_var;
extern const uint32_t DisposedObserver_1_OnError_m3075149667_MetadataUsageId;
extern "C"  void DisposedObserver_1_OnError_m3075149667_gshared (DisposedObserver_1_t1123134848 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DisposedObserver_1_OnError_m3075149667_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_0 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		ObjectDisposedException_t973246880 * L_1 = (ObjectDisposedException_t973246880 *)il2cpp_codegen_object_new(ObjectDisposedException_t973246880_il2cpp_TypeInfo_var);
		ObjectDisposedException__ctor_m1180707260(L_1, (String_t*)L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}
}
// System.Void UniRx.InternalUtil.DisposedObserver`1<UniRx.CollectionAddEvent`1<UniRx.Tuple`2<System.Object,System.Object>>>::OnNext(T)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* ObjectDisposedException_t973246880_il2cpp_TypeInfo_var;
extern const uint32_t DisposedObserver_1_OnNext_m67247188_MetadataUsageId;
extern "C"  void DisposedObserver_1_OnNext_m67247188_gshared (DisposedObserver_1_t1123134848 * __this, CollectionAddEvent_1_t1948677386  ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DisposedObserver_1_OnNext_m67247188_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_0 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		ObjectDisposedException_t973246880 * L_1 = (ObjectDisposedException_t973246880 *)il2cpp_codegen_object_new(ObjectDisposedException_t973246880_il2cpp_TypeInfo_var);
		ObjectDisposedException__ctor_m1180707260(L_1, (String_t*)L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}
}
// System.Void UniRx.InternalUtil.DisposedObserver`1<UniRx.CollectionMoveEvent`1<System.Object>>::.ctor()
extern "C"  void DisposedObserver_1__ctor_m4281705674_gshared (DisposedObserver_1_t2090891309 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UniRx.InternalUtil.DisposedObserver`1<UniRx.CollectionMoveEvent`1<System.Object>>::.cctor()
extern "C"  void DisposedObserver_1__cctor_m3401760803_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		DisposedObserver_1_t2090891309 * L_0 = (DisposedObserver_1_t2090891309 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		((  void (*) (DisposedObserver_1_t2090891309 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		((DisposedObserver_1_t2090891309_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->static_fields)->set_Instance_0(L_0);
		return;
	}
}
// System.Void UniRx.InternalUtil.DisposedObserver`1<UniRx.CollectionMoveEvent`1<System.Object>>::OnCompleted()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* ObjectDisposedException_t973246880_il2cpp_TypeInfo_var;
extern const uint32_t DisposedObserver_1_OnCompleted_m2952829140_MetadataUsageId;
extern "C"  void DisposedObserver_1_OnCompleted_m2952829140_gshared (DisposedObserver_1_t2090891309 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DisposedObserver_1_OnCompleted_m2952829140_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_0 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		ObjectDisposedException_t973246880 * L_1 = (ObjectDisposedException_t973246880 *)il2cpp_codegen_object_new(ObjectDisposedException_t973246880_il2cpp_TypeInfo_var);
		ObjectDisposedException__ctor_m1180707260(L_1, (String_t*)L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}
}
// System.Void UniRx.InternalUtil.DisposedObserver`1<UniRx.CollectionMoveEvent`1<System.Object>>::OnError(System.Exception)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* ObjectDisposedException_t973246880_il2cpp_TypeInfo_var;
extern const uint32_t DisposedObserver_1_OnError_m487107585_MetadataUsageId;
extern "C"  void DisposedObserver_1_OnError_m487107585_gshared (DisposedObserver_1_t2090891309 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DisposedObserver_1_OnError_m487107585_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_0 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		ObjectDisposedException_t973246880 * L_1 = (ObjectDisposedException_t973246880 *)il2cpp_codegen_object_new(ObjectDisposedException_t973246880_il2cpp_TypeInfo_var);
		ObjectDisposedException__ctor_m1180707260(L_1, (String_t*)L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}
}
// System.Void UniRx.InternalUtil.DisposedObserver`1<UniRx.CollectionMoveEvent`1<System.Object>>::OnNext(T)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* ObjectDisposedException_t973246880_il2cpp_TypeInfo_var;
extern const uint32_t DisposedObserver_1_OnNext_m2971186418_MetadataUsageId;
extern "C"  void DisposedObserver_1_OnNext_m2971186418_gshared (DisposedObserver_1_t2090891309 * __this, CollectionMoveEvent_1_t2916433847  ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DisposedObserver_1_OnNext_m2971186418_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_0 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		ObjectDisposedException_t973246880 * L_1 = (ObjectDisposedException_t973246880 *)il2cpp_codegen_object_new(ObjectDisposedException_t973246880_il2cpp_TypeInfo_var);
		ObjectDisposedException__ctor_m1180707260(L_1, (String_t*)L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}
}
// System.Void UniRx.InternalUtil.DisposedObserver`1<UniRx.CollectionMoveEvent`1<UniRx.Tuple`2<System.Object,System.Object>>>::.ctor()
extern "C"  void DisposedObserver_1__ctor_m4024706262_gshared (DisposedObserver_1_t1623046708 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UniRx.InternalUtil.DisposedObserver`1<UniRx.CollectionMoveEvent`1<UniRx.Tuple`2<System.Object,System.Object>>>::.cctor()
extern "C"  void DisposedObserver_1__cctor_m4024713623_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		DisposedObserver_1_t1623046708 * L_0 = (DisposedObserver_1_t1623046708 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		((  void (*) (DisposedObserver_1_t1623046708 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		((DisposedObserver_1_t1623046708_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->static_fields)->set_Instance_0(L_0);
		return;
	}
}
// System.Void UniRx.InternalUtil.DisposedObserver`1<UniRx.CollectionMoveEvent`1<UniRx.Tuple`2<System.Object,System.Object>>>::OnCompleted()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* ObjectDisposedException_t973246880_il2cpp_TypeInfo_var;
extern const uint32_t DisposedObserver_1_OnCompleted_m2124013536_MetadataUsageId;
extern "C"  void DisposedObserver_1_OnCompleted_m2124013536_gshared (DisposedObserver_1_t1623046708 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DisposedObserver_1_OnCompleted_m2124013536_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_0 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		ObjectDisposedException_t973246880 * L_1 = (ObjectDisposedException_t973246880 *)il2cpp_codegen_object_new(ObjectDisposedException_t973246880_il2cpp_TypeInfo_var);
		ObjectDisposedException__ctor_m1180707260(L_1, (String_t*)L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}
}
// System.Void UniRx.InternalUtil.DisposedObserver`1<UniRx.CollectionMoveEvent`1<UniRx.Tuple`2<System.Object,System.Object>>>::OnError(System.Exception)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* ObjectDisposedException_t973246880_il2cpp_TypeInfo_var;
extern const uint32_t DisposedObserver_1_OnError_m1393803533_MetadataUsageId;
extern "C"  void DisposedObserver_1_OnError_m1393803533_gshared (DisposedObserver_1_t1623046708 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DisposedObserver_1_OnError_m1393803533_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_0 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		ObjectDisposedException_t973246880 * L_1 = (ObjectDisposedException_t973246880 *)il2cpp_codegen_object_new(ObjectDisposedException_t973246880_il2cpp_TypeInfo_var);
		ObjectDisposedException__ctor_m1180707260(L_1, (String_t*)L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}
}
// System.Void UniRx.InternalUtil.DisposedObserver`1<UniRx.CollectionMoveEvent`1<UniRx.Tuple`2<System.Object,System.Object>>>::OnNext(T)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* ObjectDisposedException_t973246880_il2cpp_TypeInfo_var;
extern const uint32_t DisposedObserver_1_OnNext_m807887358_MetadataUsageId;
extern "C"  void DisposedObserver_1_OnNext_m807887358_gshared (DisposedObserver_1_t1623046708 * __this, CollectionMoveEvent_1_t2448589246  ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DisposedObserver_1_OnNext_m807887358_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_0 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		ObjectDisposedException_t973246880 * L_1 = (ObjectDisposedException_t973246880 *)il2cpp_codegen_object_new(ObjectDisposedException_t973246880_il2cpp_TypeInfo_var);
		ObjectDisposedException__ctor_m1180707260(L_1, (String_t*)L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}
}
// System.Void UniRx.InternalUtil.DisposedObserver`1<UniRx.CollectionRemoveEvent`1<System.Object>>::.ctor()
extern "C"  void DisposedObserver_1__ctor_m1283383485_gshared (DisposedObserver_1_t3712062482 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UniRx.InternalUtil.DisposedObserver`1<UniRx.CollectionRemoveEvent`1<System.Object>>::.cctor()
extern "C"  void DisposedObserver_1__cctor_m648086160_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		DisposedObserver_1_t3712062482 * L_0 = (DisposedObserver_1_t3712062482 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		((  void (*) (DisposedObserver_1_t3712062482 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		((DisposedObserver_1_t3712062482_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->static_fields)->set_Instance_0(L_0);
		return;
	}
}
// System.Void UniRx.InternalUtil.DisposedObserver`1<UniRx.CollectionRemoveEvent`1<System.Object>>::OnCompleted()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* ObjectDisposedException_t973246880_il2cpp_TypeInfo_var;
extern const uint32_t DisposedObserver_1_OnCompleted_m1757139591_MetadataUsageId;
extern "C"  void DisposedObserver_1_OnCompleted_m1757139591_gshared (DisposedObserver_1_t3712062482 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DisposedObserver_1_OnCompleted_m1757139591_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_0 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		ObjectDisposedException_t973246880 * L_1 = (ObjectDisposedException_t973246880 *)il2cpp_codegen_object_new(ObjectDisposedException_t973246880_il2cpp_TypeInfo_var);
		ObjectDisposedException__ctor_m1180707260(L_1, (String_t*)L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}
}
// System.Void UniRx.InternalUtil.DisposedObserver`1<UniRx.CollectionRemoveEvent`1<System.Object>>::OnError(System.Exception)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* ObjectDisposedException_t973246880_il2cpp_TypeInfo_var;
extern const uint32_t DisposedObserver_1_OnError_m2270425396_MetadataUsageId;
extern "C"  void DisposedObserver_1_OnError_m2270425396_gshared (DisposedObserver_1_t3712062482 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DisposedObserver_1_OnError_m2270425396_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_0 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		ObjectDisposedException_t973246880 * L_1 = (ObjectDisposedException_t973246880 *)il2cpp_codegen_object_new(ObjectDisposedException_t973246880_il2cpp_TypeInfo_var);
		ObjectDisposedException__ctor_m1180707260(L_1, (String_t*)L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}
}
// System.Void UniRx.InternalUtil.DisposedObserver`1<UniRx.CollectionRemoveEvent`1<System.Object>>::OnNext(T)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* ObjectDisposedException_t973246880_il2cpp_TypeInfo_var;
extern const uint32_t DisposedObserver_1_OnNext_m3506618405_MetadataUsageId;
extern "C"  void DisposedObserver_1_OnNext_m3506618405_gshared (DisposedObserver_1_t3712062482 * __this, CollectionRemoveEvent_1_t242637724  ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DisposedObserver_1_OnNext_m3506618405_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_0 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		ObjectDisposedException_t973246880 * L_1 = (ObjectDisposedException_t973246880 *)il2cpp_codegen_object_new(ObjectDisposedException_t973246880_il2cpp_TypeInfo_var);
		ObjectDisposedException__ctor_m1180707260(L_1, (String_t*)L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}
}
// System.Void UniRx.InternalUtil.DisposedObserver`1<UniRx.CollectionRemoveEvent`1<UniRx.Tuple`2<System.Object,System.Object>>>::.ctor()
extern "C"  void DisposedObserver_1__ctor_m581939651_gshared (DisposedObserver_1_t3244217881 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UniRx.InternalUtil.DisposedObserver`1<UniRx.CollectionRemoveEvent`1<UniRx.Tuple`2<System.Object,System.Object>>>::.cctor()
extern "C"  void DisposedObserver_1__cctor_m378163786_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		DisposedObserver_1_t3244217881 * L_0 = (DisposedObserver_1_t3244217881 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		((  void (*) (DisposedObserver_1_t3244217881 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		((DisposedObserver_1_t3244217881_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->static_fields)->set_Instance_0(L_0);
		return;
	}
}
// System.Void UniRx.InternalUtil.DisposedObserver`1<UniRx.CollectionRemoveEvent`1<UniRx.Tuple`2<System.Object,System.Object>>>::OnCompleted()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* ObjectDisposedException_t973246880_il2cpp_TypeInfo_var;
extern const uint32_t DisposedObserver_1_OnCompleted_m246499085_MetadataUsageId;
extern "C"  void DisposedObserver_1_OnCompleted_m246499085_gshared (DisposedObserver_1_t3244217881 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DisposedObserver_1_OnCompleted_m246499085_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_0 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		ObjectDisposedException_t973246880 * L_1 = (ObjectDisposedException_t973246880 *)il2cpp_codegen_object_new(ObjectDisposedException_t973246880_il2cpp_TypeInfo_var);
		ObjectDisposedException__ctor_m1180707260(L_1, (String_t*)L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}
}
// System.Void UniRx.InternalUtil.DisposedObserver`1<UniRx.CollectionRemoveEvent`1<UniRx.Tuple`2<System.Object,System.Object>>>::OnError(System.Exception)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* ObjectDisposedException_t973246880_il2cpp_TypeInfo_var;
extern const uint32_t DisposedObserver_1_OnError_m491834554_MetadataUsageId;
extern "C"  void DisposedObserver_1_OnError_m491834554_gshared (DisposedObserver_1_t3244217881 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DisposedObserver_1_OnError_m491834554_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_0 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		ObjectDisposedException_t973246880 * L_1 = (ObjectDisposedException_t973246880 *)il2cpp_codegen_object_new(ObjectDisposedException_t973246880_il2cpp_TypeInfo_var);
		ObjectDisposedException__ctor_m1180707260(L_1, (String_t*)L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}
}
// System.Void UniRx.InternalUtil.DisposedObserver`1<UniRx.CollectionRemoveEvent`1<UniRx.Tuple`2<System.Object,System.Object>>>::OnNext(T)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* ObjectDisposedException_t973246880_il2cpp_TypeInfo_var;
extern const uint32_t DisposedObserver_1_OnNext_m3728959403_MetadataUsageId;
extern "C"  void DisposedObserver_1_OnNext_m3728959403_gshared (DisposedObserver_1_t3244217881 * __this, CollectionRemoveEvent_1_t4069760419  ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DisposedObserver_1_OnNext_m3728959403_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_0 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		ObjectDisposedException_t973246880 * L_1 = (ObjectDisposedException_t973246880 *)il2cpp_codegen_object_new(ObjectDisposedException_t973246880_il2cpp_TypeInfo_var);
		ObjectDisposedException__ctor_m1180707260(L_1, (String_t*)L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}
}
// System.Void UniRx.InternalUtil.DisposedObserver`1<UniRx.CollectionReplaceEvent`1<System.Object>>::.ctor()
extern "C"  void DisposedObserver_1__ctor_m548762823_gshared (DisposedObserver_1_t1016207998 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UniRx.InternalUtil.DisposedObserver`1<UniRx.CollectionReplaceEvent`1<System.Object>>::.cctor()
extern "C"  void DisposedObserver_1__cctor_m3644649414_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		DisposedObserver_1_t1016207998 * L_0 = (DisposedObserver_1_t1016207998 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		((  void (*) (DisposedObserver_1_t1016207998 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		((DisposedObserver_1_t1016207998_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->static_fields)->set_Instance_0(L_0);
		return;
	}
}
// System.Void UniRx.InternalUtil.DisposedObserver`1<UniRx.CollectionReplaceEvent`1<System.Object>>::OnCompleted()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* ObjectDisposedException_t973246880_il2cpp_TypeInfo_var;
extern const uint32_t DisposedObserver_1_OnCompleted_m3887183633_MetadataUsageId;
extern "C"  void DisposedObserver_1_OnCompleted_m3887183633_gshared (DisposedObserver_1_t1016207998 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DisposedObserver_1_OnCompleted_m3887183633_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_0 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		ObjectDisposedException_t973246880 * L_1 = (ObjectDisposedException_t973246880 *)il2cpp_codegen_object_new(ObjectDisposedException_t973246880_il2cpp_TypeInfo_var);
		ObjectDisposedException__ctor_m1180707260(L_1, (String_t*)L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}
}
// System.Void UniRx.InternalUtil.DisposedObserver`1<UniRx.CollectionReplaceEvent`1<System.Object>>::OnError(System.Exception)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* ObjectDisposedException_t973246880_il2cpp_TypeInfo_var;
extern const uint32_t DisposedObserver_1_OnError_m2201310910_MetadataUsageId;
extern "C"  void DisposedObserver_1_OnError_m2201310910_gshared (DisposedObserver_1_t1016207998 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DisposedObserver_1_OnError_m2201310910_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_0 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		ObjectDisposedException_t973246880 * L_1 = (ObjectDisposedException_t973246880 *)il2cpp_codegen_object_new(ObjectDisposedException_t973246880_il2cpp_TypeInfo_var);
		ObjectDisposedException__ctor_m1180707260(L_1, (String_t*)L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}
}
// System.Void UniRx.InternalUtil.DisposedObserver`1<UniRx.CollectionReplaceEvent`1<System.Object>>::OnNext(T)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* ObjectDisposedException_t973246880_il2cpp_TypeInfo_var;
extern const uint32_t DisposedObserver_1_OnNext_m1910798767_MetadataUsageId;
extern "C"  void DisposedObserver_1_OnNext_m1910798767_gshared (DisposedObserver_1_t1016207998 * __this, CollectionReplaceEvent_1_t1841750536  ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DisposedObserver_1_OnNext_m1910798767_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_0 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		ObjectDisposedException_t973246880 * L_1 = (ObjectDisposedException_t973246880 *)il2cpp_codegen_object_new(ObjectDisposedException_t973246880_il2cpp_TypeInfo_var);
		ObjectDisposedException__ctor_m1180707260(L_1, (String_t*)L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}
}
// System.Void UniRx.InternalUtil.DisposedObserver`1<UniRx.CollectionReplaceEvent`1<UniRx.Tuple`2<System.Object,System.Object>>>::.ctor()
extern "C"  void DisposedObserver_1__ctor_m3694154233_gshared (DisposedObserver_1_t548363397 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UniRx.InternalUtil.DisposedObserver`1<UniRx.CollectionReplaceEvent`1<UniRx.Tuple`2<System.Object,System.Object>>>::.cctor()
extern "C"  void DisposedObserver_1__cctor_m2367535316_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		DisposedObserver_1_t548363397 * L_0 = (DisposedObserver_1_t548363397 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		((  void (*) (DisposedObserver_1_t548363397 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		((DisposedObserver_1_t548363397_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->static_fields)->set_Instance_0(L_0);
		return;
	}
}
// System.Void UniRx.InternalUtil.DisposedObserver`1<UniRx.CollectionReplaceEvent`1<UniRx.Tuple`2<System.Object,System.Object>>>::OnCompleted()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* ObjectDisposedException_t973246880_il2cpp_TypeInfo_var;
extern const uint32_t DisposedObserver_1_OnCompleted_m3049940675_MetadataUsageId;
extern "C"  void DisposedObserver_1_OnCompleted_m3049940675_gshared (DisposedObserver_1_t548363397 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DisposedObserver_1_OnCompleted_m3049940675_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_0 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		ObjectDisposedException_t973246880 * L_1 = (ObjectDisposedException_t973246880 *)il2cpp_codegen_object_new(ObjectDisposedException_t973246880_il2cpp_TypeInfo_var);
		ObjectDisposedException__ctor_m1180707260(L_1, (String_t*)L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}
}
// System.Void UniRx.InternalUtil.DisposedObserver`1<UniRx.CollectionReplaceEvent`1<UniRx.Tuple`2<System.Object,System.Object>>>::OnError(System.Exception)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* ObjectDisposedException_t973246880_il2cpp_TypeInfo_var;
extern const uint32_t DisposedObserver_1_OnError_m1440353136_MetadataUsageId;
extern "C"  void DisposedObserver_1_OnError_m1440353136_gshared (DisposedObserver_1_t548363397 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DisposedObserver_1_OnError_m1440353136_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_0 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		ObjectDisposedException_t973246880 * L_1 = (ObjectDisposedException_t973246880 *)il2cpp_codegen_object_new(ObjectDisposedException_t973246880_il2cpp_TypeInfo_var);
		ObjectDisposedException__ctor_m1180707260(L_1, (String_t*)L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}
}
// System.Void UniRx.InternalUtil.DisposedObserver`1<UniRx.CollectionReplaceEvent`1<UniRx.Tuple`2<System.Object,System.Object>>>::OnNext(T)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* ObjectDisposedException_t973246880_il2cpp_TypeInfo_var;
extern const uint32_t DisposedObserver_1_OnNext_m974967393_MetadataUsageId;
extern "C"  void DisposedObserver_1_OnNext_m974967393_gshared (DisposedObserver_1_t548363397 * __this, CollectionReplaceEvent_1_t1373905935  ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DisposedObserver_1_OnNext_m974967393_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_0 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		ObjectDisposedException_t973246880 * L_1 = (ObjectDisposedException_t973246880 *)il2cpp_codegen_object_new(ObjectDisposedException_t973246880_il2cpp_TypeInfo_var);
		ObjectDisposedException__ctor_m1180707260(L_1, (String_t*)L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}
}
// System.Void UniRx.InternalUtil.DisposedObserver`1<UniRx.CountChangedStatus>::.ctor()
extern "C"  void DisposedObserver_1__ctor_m830530497_gshared (DisposedObserver_1_t2945685183 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UniRx.InternalUtil.DisposedObserver`1<UniRx.CountChangedStatus>::.cctor()
extern "C"  void DisposedObserver_1__cctor_m3789512716_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		DisposedObserver_1_t2945685183 * L_0 = (DisposedObserver_1_t2945685183 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		((  void (*) (DisposedObserver_1_t2945685183 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		((DisposedObserver_1_t2945685183_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->static_fields)->set_Instance_0(L_0);
		return;
	}
}
// System.Void UniRx.InternalUtil.DisposedObserver`1<UniRx.CountChangedStatus>::OnCompleted()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* ObjectDisposedException_t973246880_il2cpp_TypeInfo_var;
extern const uint32_t DisposedObserver_1_OnCompleted_m2324202123_MetadataUsageId;
extern "C"  void DisposedObserver_1_OnCompleted_m2324202123_gshared (DisposedObserver_1_t2945685183 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DisposedObserver_1_OnCompleted_m2324202123_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_0 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		ObjectDisposedException_t973246880 * L_1 = (ObjectDisposedException_t973246880 *)il2cpp_codegen_object_new(ObjectDisposedException_t973246880_il2cpp_TypeInfo_var);
		ObjectDisposedException__ctor_m1180707260(L_1, (String_t*)L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}
}
// System.Void UniRx.InternalUtil.DisposedObserver`1<UniRx.CountChangedStatus>::OnError(System.Exception)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* ObjectDisposedException_t973246880_il2cpp_TypeInfo_var;
extern const uint32_t DisposedObserver_1_OnError_m631552824_MetadataUsageId;
extern "C"  void DisposedObserver_1_OnError_m631552824_gshared (DisposedObserver_1_t2945685183 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DisposedObserver_1_OnError_m631552824_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_0 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		ObjectDisposedException_t973246880 * L_1 = (ObjectDisposedException_t973246880 *)il2cpp_codegen_object_new(ObjectDisposedException_t973246880_il2cpp_TypeInfo_var);
		ObjectDisposedException__ctor_m1180707260(L_1, (String_t*)L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}
}
// System.Void UniRx.InternalUtil.DisposedObserver`1<UniRx.CountChangedStatus>::OnNext(T)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* ObjectDisposedException_t973246880_il2cpp_TypeInfo_var;
extern const uint32_t DisposedObserver_1_OnNext_m2106593833_MetadataUsageId;
extern "C"  void DisposedObserver_1_OnNext_m2106593833_gshared (DisposedObserver_1_t2945685183 * __this, int32_t ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DisposedObserver_1_OnNext_m2106593833_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_0 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		ObjectDisposedException_t973246880 * L_1 = (ObjectDisposedException_t973246880 *)il2cpp_codegen_object_new(ObjectDisposedException_t973246880_il2cpp_TypeInfo_var);
		ObjectDisposedException__ctor_m1180707260(L_1, (String_t*)L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}
}
// System.Void UniRx.InternalUtil.DisposedObserver`1<UniRx.DictionaryAddEvent`2<System.Object,System.Object>>::.ctor()
extern "C"  void DisposedObserver_1__ctor_m15980265_gshared (DisposedObserver_1_t3720405766 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UniRx.InternalUtil.DisposedObserver`1<UniRx.DictionaryAddEvent`2<System.Object,System.Object>>::.cctor()
extern "C"  void DisposedObserver_1__cctor_m13292004_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		DisposedObserver_1_t3720405766 * L_0 = (DisposedObserver_1_t3720405766 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		((  void (*) (DisposedObserver_1_t3720405766 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		((DisposedObserver_1_t3720405766_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->static_fields)->set_Instance_0(L_0);
		return;
	}
}
// System.Void UniRx.InternalUtil.DisposedObserver`1<UniRx.DictionaryAddEvent`2<System.Object,System.Object>>::OnCompleted()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* ObjectDisposedException_t973246880_il2cpp_TypeInfo_var;
extern const uint32_t DisposedObserver_1_OnCompleted_m1253210035_MetadataUsageId;
extern "C"  void DisposedObserver_1_OnCompleted_m1253210035_gshared (DisposedObserver_1_t3720405766 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DisposedObserver_1_OnCompleted_m1253210035_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_0 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		ObjectDisposedException_t973246880 * L_1 = (ObjectDisposedException_t973246880 *)il2cpp_codegen_object_new(ObjectDisposedException_t973246880_il2cpp_TypeInfo_var);
		ObjectDisposedException__ctor_m1180707260(L_1, (String_t*)L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}
}
// System.Void UniRx.InternalUtil.DisposedObserver`1<UniRx.DictionaryAddEvent`2<System.Object,System.Object>>::OnError(System.Exception)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* ObjectDisposedException_t973246880_il2cpp_TypeInfo_var;
extern const uint32_t DisposedObserver_1_OnError_m2583704672_MetadataUsageId;
extern "C"  void DisposedObserver_1_OnError_m2583704672_gshared (DisposedObserver_1_t3720405766 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DisposedObserver_1_OnError_m2583704672_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_0 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		ObjectDisposedException_t973246880 * L_1 = (ObjectDisposedException_t973246880 *)il2cpp_codegen_object_new(ObjectDisposedException_t973246880_il2cpp_TypeInfo_var);
		ObjectDisposedException__ctor_m1180707260(L_1, (String_t*)L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}
}
// System.Void UniRx.InternalUtil.DisposedObserver`1<UniRx.DictionaryAddEvent`2<System.Object,System.Object>>::OnNext(T)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* ObjectDisposedException_t973246880_il2cpp_TypeInfo_var;
extern const uint32_t DisposedObserver_1_OnNext_m1007868753_MetadataUsageId;
extern "C"  void DisposedObserver_1_OnNext_m1007868753_gshared (DisposedObserver_1_t3720405766 * __this, DictionaryAddEvent_2_t250981008  ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DisposedObserver_1_OnNext_m1007868753_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_0 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		ObjectDisposedException_t973246880 * L_1 = (ObjectDisposedException_t973246880 *)il2cpp_codegen_object_new(ObjectDisposedException_t973246880_il2cpp_TypeInfo_var);
		ObjectDisposedException__ctor_m1180707260(L_1, (String_t*)L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}
}
// System.Void UniRx.InternalUtil.DisposedObserver`1<UniRx.DictionaryRemoveEvent`2<System.Object,System.Object>>::.ctor()
extern "C"  void DisposedObserver_1__ctor_m1620369314_gshared (DisposedObserver_1_t2496745753 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UniRx.InternalUtil.DisposedObserver`1<UniRx.DictionaryRemoveEvent`2<System.Object,System.Object>>::.cctor()
extern "C"  void DisposedObserver_1__cctor_m2504712267_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		DisposedObserver_1_t2496745753 * L_0 = (DisposedObserver_1_t2496745753 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		((  void (*) (DisposedObserver_1_t2496745753 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		((DisposedObserver_1_t2496745753_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->static_fields)->set_Instance_0(L_0);
		return;
	}
}
// System.Void UniRx.InternalUtil.DisposedObserver`1<UniRx.DictionaryRemoveEvent`2<System.Object,System.Object>>::OnCompleted()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* ObjectDisposedException_t973246880_il2cpp_TypeInfo_var;
extern const uint32_t DisposedObserver_1_OnCompleted_m432951724_MetadataUsageId;
extern "C"  void DisposedObserver_1_OnCompleted_m432951724_gshared (DisposedObserver_1_t2496745753 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DisposedObserver_1_OnCompleted_m432951724_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_0 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		ObjectDisposedException_t973246880 * L_1 = (ObjectDisposedException_t973246880 *)il2cpp_codegen_object_new(ObjectDisposedException_t973246880_il2cpp_TypeInfo_var);
		ObjectDisposedException__ctor_m1180707260(L_1, (String_t*)L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}
}
// System.Void UniRx.InternalUtil.DisposedObserver`1<UniRx.DictionaryRemoveEvent`2<System.Object,System.Object>>::OnError(System.Exception)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* ObjectDisposedException_t973246880_il2cpp_TypeInfo_var;
extern const uint32_t DisposedObserver_1_OnError_m2794447065_MetadataUsageId;
extern "C"  void DisposedObserver_1_OnError_m2794447065_gshared (DisposedObserver_1_t2496745753 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DisposedObserver_1_OnError_m2794447065_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_0 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		ObjectDisposedException_t973246880 * L_1 = (ObjectDisposedException_t973246880 *)il2cpp_codegen_object_new(ObjectDisposedException_t973246880_il2cpp_TypeInfo_var);
		ObjectDisposedException__ctor_m1180707260(L_1, (String_t*)L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}
}
// System.Void UniRx.InternalUtil.DisposedObserver`1<UniRx.DictionaryRemoveEvent`2<System.Object,System.Object>>::OnNext(T)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* ObjectDisposedException_t973246880_il2cpp_TypeInfo_var;
extern const uint32_t DisposedObserver_1_OnNext_m932485578_MetadataUsageId;
extern "C"  void DisposedObserver_1_OnNext_m932485578_gshared (DisposedObserver_1_t2496745753 * __this, DictionaryRemoveEvent_2_t3322288291  ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DisposedObserver_1_OnNext_m932485578_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_0 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		ObjectDisposedException_t973246880 * L_1 = (ObjectDisposedException_t973246880 *)il2cpp_codegen_object_new(ObjectDisposedException_t973246880_il2cpp_TypeInfo_var);
		ObjectDisposedException__ctor_m1180707260(L_1, (String_t*)L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}
}
// System.Void UniRx.InternalUtil.DisposedObserver`1<UniRx.DictionaryReplaceEvent`2<System.Object,System.Object>>::.ctor()
extern "C"  void DisposedObserver_1__ctor_m448195900_gshared (DisposedObserver_1_t2953384525 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UniRx.InternalUtil.DisposedObserver`1<UniRx.DictionaryReplaceEvent`2<System.Object,System.Object>>::.cctor()
extern "C"  void DisposedObserver_1__cctor_m527074801_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		DisposedObserver_1_t2953384525 * L_0 = (DisposedObserver_1_t2953384525 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		((  void (*) (DisposedObserver_1_t2953384525 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		((DisposedObserver_1_t2953384525_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->static_fields)->set_Instance_0(L_0);
		return;
	}
}
// System.Void UniRx.InternalUtil.DisposedObserver`1<UniRx.DictionaryReplaceEvent`2<System.Object,System.Object>>::OnCompleted()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* ObjectDisposedException_t973246880_il2cpp_TypeInfo_var;
extern const uint32_t DisposedObserver_1_OnCompleted_m3052553158_MetadataUsageId;
extern "C"  void DisposedObserver_1_OnCompleted_m3052553158_gshared (DisposedObserver_1_t2953384525 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DisposedObserver_1_OnCompleted_m3052553158_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_0 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		ObjectDisposedException_t973246880 * L_1 = (ObjectDisposedException_t973246880 *)il2cpp_codegen_object_new(ObjectDisposedException_t973246880_il2cpp_TypeInfo_var);
		ObjectDisposedException__ctor_m1180707260(L_1, (String_t*)L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}
}
// System.Void UniRx.InternalUtil.DisposedObserver`1<UniRx.DictionaryReplaceEvent`2<System.Object,System.Object>>::OnError(System.Exception)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* ObjectDisposedException_t973246880_il2cpp_TypeInfo_var;
extern const uint32_t DisposedObserver_1_OnError_m1000831987_MetadataUsageId;
extern "C"  void DisposedObserver_1_OnError_m1000831987_gshared (DisposedObserver_1_t2953384525 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DisposedObserver_1_OnError_m1000831987_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_0 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		ObjectDisposedException_t973246880 * L_1 = (ObjectDisposedException_t973246880 *)il2cpp_codegen_object_new(ObjectDisposedException_t973246880_il2cpp_TypeInfo_var);
		ObjectDisposedException__ctor_m1180707260(L_1, (String_t*)L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}
}
// System.Void UniRx.InternalUtil.DisposedObserver`1<UniRx.DictionaryReplaceEvent`2<System.Object,System.Object>>::OnNext(T)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* ObjectDisposedException_t973246880_il2cpp_TypeInfo_var;
extern const uint32_t DisposedObserver_1_OnNext_m4050233572_MetadataUsageId;
extern "C"  void DisposedObserver_1_OnNext_m4050233572_gshared (DisposedObserver_1_t2953384525 * __this, DictionaryReplaceEvent_2_t3778927063  ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DisposedObserver_1_OnNext_m4050233572_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_0 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		ObjectDisposedException_t973246880 * L_1 = (ObjectDisposedException_t973246880 *)il2cpp_codegen_object_new(ObjectDisposedException_t973246880_il2cpp_TypeInfo_var);
		ObjectDisposedException__ctor_m1180707260(L_1, (String_t*)L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}
}
// System.Void UniRx.InternalUtil.DisposedObserver`1<UniRx.Tuple`2<System.Object,System.Int32>>::.ctor()
extern "C"  void DisposedObserver_1__ctor_m1907741183_gshared (DisposedObserver_1_t1554027648 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UniRx.InternalUtil.DisposedObserver`1<UniRx.Tuple`2<System.Object,System.Int32>>::.cctor()
extern "C"  void DisposedObserver_1__cctor_m2823305614_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		DisposedObserver_1_t1554027648 * L_0 = (DisposedObserver_1_t1554027648 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		((  void (*) (DisposedObserver_1_t1554027648 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		((DisposedObserver_1_t1554027648_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->static_fields)->set_Instance_0(L_0);
		return;
	}
}
// System.Void UniRx.InternalUtil.DisposedObserver`1<UniRx.Tuple`2<System.Object,System.Int32>>::OnCompleted()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* ObjectDisposedException_t973246880_il2cpp_TypeInfo_var;
extern const uint32_t DisposedObserver_1_OnCompleted_m2929019465_MetadataUsageId;
extern "C"  void DisposedObserver_1_OnCompleted_m2929019465_gshared (DisposedObserver_1_t1554027648 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DisposedObserver_1_OnCompleted_m2929019465_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_0 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		ObjectDisposedException_t973246880 * L_1 = (ObjectDisposedException_t973246880 *)il2cpp_codegen_object_new(ObjectDisposedException_t973246880_il2cpp_TypeInfo_var);
		ObjectDisposedException__ctor_m1180707260(L_1, (String_t*)L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}
}
// System.Void UniRx.InternalUtil.DisposedObserver`1<UniRx.Tuple`2<System.Object,System.Int32>>::OnError(System.Exception)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* ObjectDisposedException_t973246880_il2cpp_TypeInfo_var;
extern const uint32_t DisposedObserver_1_OnError_m1996346870_MetadataUsageId;
extern "C"  void DisposedObserver_1_OnError_m1996346870_gshared (DisposedObserver_1_t1554027648 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DisposedObserver_1_OnError_m1996346870_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_0 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		ObjectDisposedException_t973246880 * L_1 = (ObjectDisposedException_t973246880 *)il2cpp_codegen_object_new(ObjectDisposedException_t973246880_il2cpp_TypeInfo_var);
		ObjectDisposedException__ctor_m1180707260(L_1, (String_t*)L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}
}
// System.Void UniRx.InternalUtil.DisposedObserver`1<UniRx.Tuple`2<System.Object,System.Int32>>::OnNext(T)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* ObjectDisposedException_t973246880_il2cpp_TypeInfo_var;
extern const uint32_t DisposedObserver_1_OnNext_m2218944743_MetadataUsageId;
extern "C"  void DisposedObserver_1_OnNext_m2218944743_gshared (DisposedObserver_1_t1554027648 * __this, Tuple_2_t2379570186  ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DisposedObserver_1_OnNext_m2218944743_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_0 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		ObjectDisposedException_t973246880 * L_1 = (ObjectDisposedException_t973246880 *)il2cpp_codegen_object_new(ObjectDisposedException_t973246880_il2cpp_TypeInfo_var);
		ObjectDisposedException__ctor_m1180707260(L_1, (String_t*)L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}
}
// System.Void UniRx.InternalUtil.DisposedObserver`1<UniRx.Tuple`2<System.Object,System.Object>>::.ctor()
extern "C"  void DisposedObserver_1__ctor_m2529361114_gshared (DisposedObserver_1_t3838686577 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UniRx.InternalUtil.DisposedObserver`1<UniRx.Tuple`2<System.Object,System.Object>>::.cctor()
extern "C"  void DisposedObserver_1__cctor_m618686995_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		DisposedObserver_1_t3838686577 * L_0 = (DisposedObserver_1_t3838686577 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		((  void (*) (DisposedObserver_1_t3838686577 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		((DisposedObserver_1_t3838686577_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->static_fields)->set_Instance_0(L_0);
		return;
	}
}
// System.Void UniRx.InternalUtil.DisposedObserver`1<UniRx.Tuple`2<System.Object,System.Object>>::OnCompleted()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* ObjectDisposedException_t973246880_il2cpp_TypeInfo_var;
extern const uint32_t DisposedObserver_1_OnCompleted_m479175908_MetadataUsageId;
extern "C"  void DisposedObserver_1_OnCompleted_m479175908_gshared (DisposedObserver_1_t3838686577 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DisposedObserver_1_OnCompleted_m479175908_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_0 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		ObjectDisposedException_t973246880 * L_1 = (ObjectDisposedException_t973246880 *)il2cpp_codegen_object_new(ObjectDisposedException_t973246880_il2cpp_TypeInfo_var);
		ObjectDisposedException__ctor_m1180707260(L_1, (String_t*)L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}
}
// System.Void UniRx.InternalUtil.DisposedObserver`1<UniRx.Tuple`2<System.Object,System.Object>>::OnError(System.Exception)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* ObjectDisposedException_t973246880_il2cpp_TypeInfo_var;
extern const uint32_t DisposedObserver_1_OnError_m932585489_MetadataUsageId;
extern "C"  void DisposedObserver_1_OnError_m932585489_gshared (DisposedObserver_1_t3838686577 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DisposedObserver_1_OnError_m932585489_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_0 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		ObjectDisposedException_t973246880 * L_1 = (ObjectDisposedException_t973246880 *)il2cpp_codegen_object_new(ObjectDisposedException_t973246880_il2cpp_TypeInfo_var);
		ObjectDisposedException__ctor_m1180707260(L_1, (String_t*)L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}
}
// System.Void UniRx.InternalUtil.DisposedObserver`1<UniRx.Tuple`2<System.Object,System.Object>>::OnNext(T)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* ObjectDisposedException_t973246880_il2cpp_TypeInfo_var;
extern const uint32_t DisposedObserver_1_OnNext_m2595244290_MetadataUsageId;
extern "C"  void DisposedObserver_1_OnNext_m2595244290_gshared (DisposedObserver_1_t3838686577 * __this, Tuple_2_t369261819  ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DisposedObserver_1_OnNext_m2595244290_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_0 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		ObjectDisposedException_t973246880 * L_1 = (ObjectDisposedException_t973246880 *)il2cpp_codegen_object_new(ObjectDisposedException_t973246880_il2cpp_TypeInfo_var);
		ObjectDisposedException__ctor_m1180707260(L_1, (String_t*)L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}
}
// System.Void UniRx.InternalUtil.DisposedObserver`1<UniRx.Tuple`2<System.Object,UnityEngine.NetworkMessageInfo>>::.ctor()
extern "C"  void DisposedObserver_1__ctor_m772898864_gshared (DisposedObserver_1_t1280957745 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UniRx.InternalUtil.DisposedObserver`1<UniRx.Tuple`2<System.Object,UnityEngine.NetworkMessageInfo>>::.cctor()
extern "C"  void DisposedObserver_1__cctor_m2002932093_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		DisposedObserver_1_t1280957745 * L_0 = (DisposedObserver_1_t1280957745 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		((  void (*) (DisposedObserver_1_t1280957745 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		((DisposedObserver_1_t1280957745_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->static_fields)->set_Instance_0(L_0);
		return;
	}
}
// System.Void UniRx.InternalUtil.DisposedObserver`1<UniRx.Tuple`2<System.Object,UnityEngine.NetworkMessageInfo>>::OnCompleted()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* ObjectDisposedException_t973246880_il2cpp_TypeInfo_var;
extern const uint32_t DisposedObserver_1_OnCompleted_m386387898_MetadataUsageId;
extern "C"  void DisposedObserver_1_OnCompleted_m386387898_gshared (DisposedObserver_1_t1280957745 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DisposedObserver_1_OnCompleted_m386387898_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_0 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		ObjectDisposedException_t973246880 * L_1 = (ObjectDisposedException_t973246880 *)il2cpp_codegen_object_new(ObjectDisposedException_t973246880_il2cpp_TypeInfo_var);
		ObjectDisposedException__ctor_m1180707260(L_1, (String_t*)L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}
}
// System.Void UniRx.InternalUtil.DisposedObserver`1<UniRx.Tuple`2<System.Object,UnityEngine.NetworkMessageInfo>>::OnError(System.Exception)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* ObjectDisposedException_t973246880_il2cpp_TypeInfo_var;
extern const uint32_t DisposedObserver_1_OnError_m514243047_MetadataUsageId;
extern "C"  void DisposedObserver_1_OnError_m514243047_gshared (DisposedObserver_1_t1280957745 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DisposedObserver_1_OnError_m514243047_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_0 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		ObjectDisposedException_t973246880 * L_1 = (ObjectDisposedException_t973246880 *)il2cpp_codegen_object_new(ObjectDisposedException_t973246880_il2cpp_TypeInfo_var);
		ObjectDisposedException__ctor_m1180707260(L_1, (String_t*)L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}
}
// System.Void UniRx.InternalUtil.DisposedObserver`1<UniRx.Tuple`2<System.Object,UnityEngine.NetworkMessageInfo>>::OnNext(T)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* ObjectDisposedException_t973246880_il2cpp_TypeInfo_var;
extern const uint32_t DisposedObserver_1_OnNext_m2557169368_MetadataUsageId;
extern "C"  void DisposedObserver_1_OnNext_m2557169368_gshared (DisposedObserver_1_t1280957745 * __this, Tuple_2_t2106500283  ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DisposedObserver_1_OnNext_m2557169368_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_0 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		ObjectDisposedException_t973246880 * L_1 = (ObjectDisposedException_t973246880 *)il2cpp_codegen_object_new(ObjectDisposedException_t973246880_il2cpp_TypeInfo_var);
		ObjectDisposedException__ctor_m1180707260(L_1, (String_t*)L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}
}
// System.Void UniRx.InternalUtil.DisposedObserver`1<UniRx.Unit>::.ctor()
extern "C"  void DisposedObserver_1__ctor_m3616962254_gshared (DisposedObserver_1_t1732743500 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UniRx.InternalUtil.DisposedObserver`1<UniRx.Unit>::.cctor()
extern "C"  void DisposedObserver_1__cctor_m4269551263_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		DisposedObserver_1_t1732743500 * L_0 = (DisposedObserver_1_t1732743500 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		((  void (*) (DisposedObserver_1_t1732743500 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		((DisposedObserver_1_t1732743500_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->static_fields)->set_Instance_0(L_0);
		return;
	}
}
// System.Void UniRx.InternalUtil.DisposedObserver`1<UniRx.Unit>::OnCompleted()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* ObjectDisposedException_t973246880_il2cpp_TypeInfo_var;
extern const uint32_t DisposedObserver_1_OnCompleted_m1888802776_MetadataUsageId;
extern "C"  void DisposedObserver_1_OnCompleted_m1888802776_gshared (DisposedObserver_1_t1732743500 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DisposedObserver_1_OnCompleted_m1888802776_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_0 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		ObjectDisposedException_t973246880 * L_1 = (ObjectDisposedException_t973246880 *)il2cpp_codegen_object_new(ObjectDisposedException_t973246880_il2cpp_TypeInfo_var);
		ObjectDisposedException__ctor_m1180707260(L_1, (String_t*)L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}
}
// System.Void UniRx.InternalUtil.DisposedObserver`1<UniRx.Unit>::OnError(System.Exception)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* ObjectDisposedException_t973246880_il2cpp_TypeInfo_var;
extern const uint32_t DisposedObserver_1_OnError_m2554496261_MetadataUsageId;
extern "C"  void DisposedObserver_1_OnError_m2554496261_gshared (DisposedObserver_1_t1732743500 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DisposedObserver_1_OnError_m2554496261_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_0 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		ObjectDisposedException_t973246880 * L_1 = (ObjectDisposedException_t973246880 *)il2cpp_codegen_object_new(ObjectDisposedException_t973246880_il2cpp_TypeInfo_var);
		ObjectDisposedException__ctor_m1180707260(L_1, (String_t*)L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}
}
// System.Void UniRx.InternalUtil.DisposedObserver`1<UniRx.Unit>::OnNext(T)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* ObjectDisposedException_t973246880_il2cpp_TypeInfo_var;
extern const uint32_t DisposedObserver_1_OnNext_m4102886902_MetadataUsageId;
extern "C"  void DisposedObserver_1_OnNext_m4102886902_gshared (DisposedObserver_1_t1732743500 * __this, Unit_t2558286038  ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DisposedObserver_1_OnNext_m4102886902_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_0 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		ObjectDisposedException_t973246880 * L_1 = (ObjectDisposedException_t973246880 *)il2cpp_codegen_object_new(ObjectDisposedException_t973246880_il2cpp_TypeInfo_var);
		ObjectDisposedException__ctor_m1180707260(L_1, (String_t*)L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}
}
// System.Void UniRx.InternalUtil.DisposedObserver`1<UnityEngine.Bounds>::.ctor()
extern "C"  void DisposedObserver_1__ctor_m212441886_gshared (DisposedObserver_1_t2692972440 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UniRx.InternalUtil.DisposedObserver`1<UnityEngine.Bounds>::.cctor()
extern "C"  void DisposedObserver_1__cctor_m1808634959_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		DisposedObserver_1_t2692972440 * L_0 = (DisposedObserver_1_t2692972440 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		((  void (*) (DisposedObserver_1_t2692972440 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		((DisposedObserver_1_t2692972440_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->static_fields)->set_Instance_0(L_0);
		return;
	}
}
// System.Void UniRx.InternalUtil.DisposedObserver`1<UnityEngine.Bounds>::OnCompleted()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* ObjectDisposedException_t973246880_il2cpp_TypeInfo_var;
extern const uint32_t DisposedObserver_1_OnCompleted_m867139624_MetadataUsageId;
extern "C"  void DisposedObserver_1_OnCompleted_m867139624_gshared (DisposedObserver_1_t2692972440 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DisposedObserver_1_OnCompleted_m867139624_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_0 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		ObjectDisposedException_t973246880 * L_1 = (ObjectDisposedException_t973246880 *)il2cpp_codegen_object_new(ObjectDisposedException_t973246880_il2cpp_TypeInfo_var);
		ObjectDisposedException__ctor_m1180707260(L_1, (String_t*)L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}
}
// System.Void UniRx.InternalUtil.DisposedObserver`1<UnityEngine.Bounds>::OnError(System.Exception)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* ObjectDisposedException_t973246880_il2cpp_TypeInfo_var;
extern const uint32_t DisposedObserver_1_OnError_m160642389_MetadataUsageId;
extern "C"  void DisposedObserver_1_OnError_m160642389_gshared (DisposedObserver_1_t2692972440 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DisposedObserver_1_OnError_m160642389_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_0 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		ObjectDisposedException_t973246880 * L_1 = (ObjectDisposedException_t973246880 *)il2cpp_codegen_object_new(ObjectDisposedException_t973246880_il2cpp_TypeInfo_var);
		ObjectDisposedException__ctor_m1180707260(L_1, (String_t*)L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}
}
// System.Void UniRx.InternalUtil.DisposedObserver`1<UnityEngine.Bounds>::OnNext(T)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* ObjectDisposedException_t973246880_il2cpp_TypeInfo_var;
extern const uint32_t DisposedObserver_1_OnNext_m828925510_MetadataUsageId;
extern "C"  void DisposedObserver_1_OnNext_m828925510_gshared (DisposedObserver_1_t2692972440 * __this, Bounds_t3518514978  ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DisposedObserver_1_OnNext_m828925510_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_0 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		ObjectDisposedException_t973246880 * L_1 = (ObjectDisposedException_t973246880 *)il2cpp_codegen_object_new(ObjectDisposedException_t973246880_il2cpp_TypeInfo_var);
		ObjectDisposedException__ctor_m1180707260(L_1, (String_t*)L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}
}
// System.Void UniRx.InternalUtil.DisposedObserver`1<UnityEngine.Color>::.ctor()
extern "C"  void DisposedObserver_1__ctor_m816678020_gshared (DisposedObserver_1_t762633222 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UniRx.InternalUtil.DisposedObserver`1<UnityEngine.Color>::.cctor()
extern "C"  void DisposedObserver_1__cctor_m3360085929_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		DisposedObserver_1_t762633222 * L_0 = (DisposedObserver_1_t762633222 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		((  void (*) (DisposedObserver_1_t762633222 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		((DisposedObserver_1_t762633222_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->static_fields)->set_Instance_0(L_0);
		return;
	}
}
// System.Void UniRx.InternalUtil.DisposedObserver`1<UnityEngine.Color>::OnCompleted()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* ObjectDisposedException_t973246880_il2cpp_TypeInfo_var;
extern const uint32_t DisposedObserver_1_OnCompleted_m2837202190_MetadataUsageId;
extern "C"  void DisposedObserver_1_OnCompleted_m2837202190_gshared (DisposedObserver_1_t762633222 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DisposedObserver_1_OnCompleted_m2837202190_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_0 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		ObjectDisposedException_t973246880 * L_1 = (ObjectDisposedException_t973246880 *)il2cpp_codegen_object_new(ObjectDisposedException_t973246880_il2cpp_TypeInfo_var);
		ObjectDisposedException__ctor_m1180707260(L_1, (String_t*)L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}
}
// System.Void UniRx.InternalUtil.DisposedObserver`1<UnityEngine.Color>::OnError(System.Exception)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* ObjectDisposedException_t973246880_il2cpp_TypeInfo_var;
extern const uint32_t DisposedObserver_1_OnError_m1471271227_MetadataUsageId;
extern "C"  void DisposedObserver_1_OnError_m1471271227_gshared (DisposedObserver_1_t762633222 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DisposedObserver_1_OnError_m1471271227_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_0 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		ObjectDisposedException_t973246880 * L_1 = (ObjectDisposedException_t973246880 *)il2cpp_codegen_object_new(ObjectDisposedException_t973246880_il2cpp_TypeInfo_var);
		ObjectDisposedException__ctor_m1180707260(L_1, (String_t*)L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}
}
// System.Void UniRx.InternalUtil.DisposedObserver`1<UnityEngine.Color>::OnNext(T)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* ObjectDisposedException_t973246880_il2cpp_TypeInfo_var;
extern const uint32_t DisposedObserver_1_OnNext_m1679265324_MetadataUsageId;
extern "C"  void DisposedObserver_1_OnNext_m1679265324_gshared (DisposedObserver_1_t762633222 * __this, Color_t1588175760  ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DisposedObserver_1_OnNext_m1679265324_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_0 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		ObjectDisposedException_t973246880 * L_1 = (ObjectDisposedException_t973246880 *)il2cpp_codegen_object_new(ObjectDisposedException_t973246880_il2cpp_TypeInfo_var);
		ObjectDisposedException__ctor_m1180707260(L_1, (String_t*)L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}
}
// System.Void UniRx.InternalUtil.DisposedObserver`1<UnityEngine.MasterServerEvent>::.ctor()
extern "C"  void DisposedObserver_1__ctor_m3476395510_gshared (DisposedObserver_1_t1452264952 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UniRx.InternalUtil.DisposedObserver`1<UnityEngine.MasterServerEvent>::.cctor()
extern "C"  void DisposedObserver_1__cctor_m4206949495_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		DisposedObserver_1_t1452264952 * L_0 = (DisposedObserver_1_t1452264952 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		((  void (*) (DisposedObserver_1_t1452264952 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		((DisposedObserver_1_t1452264952_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->static_fields)->set_Instance_0(L_0);
		return;
	}
}
// System.Void UniRx.InternalUtil.DisposedObserver`1<UnityEngine.MasterServerEvent>::OnCompleted()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* ObjectDisposedException_t973246880_il2cpp_TypeInfo_var;
extern const uint32_t DisposedObserver_1_OnCompleted_m437909760_MetadataUsageId;
extern "C"  void DisposedObserver_1_OnCompleted_m437909760_gshared (DisposedObserver_1_t1452264952 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DisposedObserver_1_OnCompleted_m437909760_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_0 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		ObjectDisposedException_t973246880 * L_1 = (ObjectDisposedException_t973246880 *)il2cpp_codegen_object_new(ObjectDisposedException_t973246880_il2cpp_TypeInfo_var);
		ObjectDisposedException__ctor_m1180707260(L_1, (String_t*)L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}
}
// System.Void UniRx.InternalUtil.DisposedObserver`1<UnityEngine.MasterServerEvent>::OnError(System.Exception)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* ObjectDisposedException_t973246880_il2cpp_TypeInfo_var;
extern const uint32_t DisposedObserver_1_OnError_m1251420717_MetadataUsageId;
extern "C"  void DisposedObserver_1_OnError_m1251420717_gshared (DisposedObserver_1_t1452264952 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DisposedObserver_1_OnError_m1251420717_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_0 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		ObjectDisposedException_t973246880 * L_1 = (ObjectDisposedException_t973246880 *)il2cpp_codegen_object_new(ObjectDisposedException_t973246880_il2cpp_TypeInfo_var);
		ObjectDisposedException__ctor_m1180707260(L_1, (String_t*)L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}
}
// System.Void UniRx.InternalUtil.DisposedObserver`1<UnityEngine.MasterServerEvent>::OnNext(T)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* ObjectDisposedException_t973246880_il2cpp_TypeInfo_var;
extern const uint32_t DisposedObserver_1_OnNext_m2162232094_MetadataUsageId;
extern "C"  void DisposedObserver_1_OnNext_m2162232094_gshared (DisposedObserver_1_t1452264952 * __this, int32_t ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DisposedObserver_1_OnNext_m2162232094_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_0 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		ObjectDisposedException_t973246880 * L_1 = (ObjectDisposedException_t973246880 *)il2cpp_codegen_object_new(ObjectDisposedException_t973246880_il2cpp_TypeInfo_var);
		ObjectDisposedException__ctor_m1180707260(L_1, (String_t*)L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}
}
// System.Void UniRx.InternalUtil.DisposedObserver`1<UnityEngine.NetworkConnectionError>::.ctor()
extern "C"  void DisposedObserver_1__ctor_m2392258917_gshared (DisposedObserver_1_t198263455 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UniRx.InternalUtil.DisposedObserver`1<UnityEngine.NetworkConnectionError>::.cctor()
extern "C"  void DisposedObserver_1__cctor_m663486184_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		DisposedObserver_1_t198263455 * L_0 = (DisposedObserver_1_t198263455 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		((  void (*) (DisposedObserver_1_t198263455 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		((DisposedObserver_1_t198263455_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->static_fields)->set_Instance_0(L_0);
		return;
	}
}
// System.Void UniRx.InternalUtil.DisposedObserver`1<UnityEngine.NetworkConnectionError>::OnCompleted()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* ObjectDisposedException_t973246880_il2cpp_TypeInfo_var;
extern const uint32_t DisposedObserver_1_OnCompleted_m91802927_MetadataUsageId;
extern "C"  void DisposedObserver_1_OnCompleted_m91802927_gshared (DisposedObserver_1_t198263455 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DisposedObserver_1_OnCompleted_m91802927_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_0 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		ObjectDisposedException_t973246880 * L_1 = (ObjectDisposedException_t973246880 *)il2cpp_codegen_object_new(ObjectDisposedException_t973246880_il2cpp_TypeInfo_var);
		ObjectDisposedException__ctor_m1180707260(L_1, (String_t*)L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}
}
// System.Void UniRx.InternalUtil.DisposedObserver`1<UnityEngine.NetworkConnectionError>::OnError(System.Exception)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* ObjectDisposedException_t973246880_il2cpp_TypeInfo_var;
extern const uint32_t DisposedObserver_1_OnError_m1684418524_MetadataUsageId;
extern "C"  void DisposedObserver_1_OnError_m1684418524_gshared (DisposedObserver_1_t198263455 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DisposedObserver_1_OnError_m1684418524_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_0 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		ObjectDisposedException_t973246880 * L_1 = (ObjectDisposedException_t973246880 *)il2cpp_codegen_object_new(ObjectDisposedException_t973246880_il2cpp_TypeInfo_var);
		ObjectDisposedException__ctor_m1180707260(L_1, (String_t*)L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}
}
// System.Void UniRx.InternalUtil.DisposedObserver`1<UnityEngine.NetworkConnectionError>::OnNext(T)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* ObjectDisposedException_t973246880_il2cpp_TypeInfo_var;
extern const uint32_t DisposedObserver_1_OnNext_m3984019149_MetadataUsageId;
extern "C"  void DisposedObserver_1_OnNext_m3984019149_gshared (DisposedObserver_1_t198263455 * __this, int32_t ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DisposedObserver_1_OnNext_m3984019149_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_0 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		ObjectDisposedException_t973246880 * L_1 = (ObjectDisposedException_t973246880 *)il2cpp_codegen_object_new(ObjectDisposedException_t973246880_il2cpp_TypeInfo_var);
		ObjectDisposedException__ctor_m1180707260(L_1, (String_t*)L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}
}
// System.Void UniRx.InternalUtil.DisposedObserver`1<UnityEngine.NetworkDisconnection>::.ctor()
extern "C"  void DisposedObserver_1__ctor_m4055986311_gshared (DisposedObserver_1_t3808185153 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UniRx.InternalUtil.DisposedObserver`1<UnityEngine.NetworkDisconnection>::.cctor()
extern "C"  void DisposedObserver_1__cctor_m699427846_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		DisposedObserver_1_t3808185153 * L_0 = (DisposedObserver_1_t3808185153 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		((  void (*) (DisposedObserver_1_t3808185153 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		((DisposedObserver_1_t3808185153_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->static_fields)->set_Instance_0(L_0);
		return;
	}
}
// System.Void UniRx.InternalUtil.DisposedObserver`1<UnityEngine.NetworkDisconnection>::OnCompleted()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* ObjectDisposedException_t973246880_il2cpp_TypeInfo_var;
extern const uint32_t DisposedObserver_1_OnCompleted_m3980518097_MetadataUsageId;
extern "C"  void DisposedObserver_1_OnCompleted_m3980518097_gshared (DisposedObserver_1_t3808185153 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DisposedObserver_1_OnCompleted_m3980518097_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_0 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		ObjectDisposedException_t973246880 * L_1 = (ObjectDisposedException_t973246880 *)il2cpp_codegen_object_new(ObjectDisposedException_t973246880_il2cpp_TypeInfo_var);
		ObjectDisposedException__ctor_m1180707260(L_1, (String_t*)L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}
}
// System.Void UniRx.InternalUtil.DisposedObserver`1<UnityEngine.NetworkDisconnection>::OnError(System.Exception)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* ObjectDisposedException_t973246880_il2cpp_TypeInfo_var;
extern const uint32_t DisposedObserver_1_OnError_m2502025854_MetadataUsageId;
extern "C"  void DisposedObserver_1_OnError_m2502025854_gshared (DisposedObserver_1_t3808185153 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DisposedObserver_1_OnError_m2502025854_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_0 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		ObjectDisposedException_t973246880 * L_1 = (ObjectDisposedException_t973246880 *)il2cpp_codegen_object_new(ObjectDisposedException_t973246880_il2cpp_TypeInfo_var);
		ObjectDisposedException__ctor_m1180707260(L_1, (String_t*)L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}
}
// System.Void UniRx.InternalUtil.DisposedObserver`1<UnityEngine.NetworkDisconnection>::OnNext(T)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* ObjectDisposedException_t973246880_il2cpp_TypeInfo_var;
extern const uint32_t DisposedObserver_1_OnNext_m803243375_MetadataUsageId;
extern "C"  void DisposedObserver_1_OnNext_m803243375_gshared (DisposedObserver_1_t3808185153 * __this, int32_t ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DisposedObserver_1_OnNext_m803243375_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_0 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		ObjectDisposedException_t973246880 * L_1 = (ObjectDisposedException_t973246880 *)il2cpp_codegen_object_new(ObjectDisposedException_t973246880_il2cpp_TypeInfo_var);
		ObjectDisposedException__ctor_m1180707260(L_1, (String_t*)L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}
}
// System.Void UniRx.InternalUtil.DisposedObserver`1<UnityEngine.NetworkMessageInfo>::.ctor()
extern "C"  void DisposedObserver_1__ctor_m4154606128_gshared (DisposedObserver_1_t1748802346 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UniRx.InternalUtil.DisposedObserver`1<UnityEngine.NetworkMessageInfo>::.cctor()
extern "C"  void DisposedObserver_1__cctor_m3756642173_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		DisposedObserver_1_t1748802346 * L_0 = (DisposedObserver_1_t1748802346 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		((  void (*) (DisposedObserver_1_t1748802346 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		((DisposedObserver_1_t1748802346_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->static_fields)->set_Instance_0(L_0);
		return;
	}
}
// System.Void UniRx.InternalUtil.DisposedObserver`1<UnityEngine.NetworkMessageInfo>::OnCompleted()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* ObjectDisposedException_t973246880_il2cpp_TypeInfo_var;
extern const uint32_t DisposedObserver_1_OnCompleted_m3984527802_MetadataUsageId;
extern "C"  void DisposedObserver_1_OnCompleted_m3984527802_gshared (DisposedObserver_1_t1748802346 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DisposedObserver_1_OnCompleted_m3984527802_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_0 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		ObjectDisposedException_t973246880 * L_1 = (ObjectDisposedException_t973246880 *)il2cpp_codegen_object_new(ObjectDisposedException_t973246880_il2cpp_TypeInfo_var);
		ObjectDisposedException__ctor_m1180707260(L_1, (String_t*)L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}
}
// System.Void UniRx.InternalUtil.DisposedObserver`1<UnityEngine.NetworkMessageInfo>::OnError(System.Exception)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* ObjectDisposedException_t973246880_il2cpp_TypeInfo_var;
extern const uint32_t DisposedObserver_1_OnError_m3259694055_MetadataUsageId;
extern "C"  void DisposedObserver_1_OnError_m3259694055_gshared (DisposedObserver_1_t1748802346 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DisposedObserver_1_OnError_m3259694055_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_0 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		ObjectDisposedException_t973246880 * L_1 = (ObjectDisposedException_t973246880 *)il2cpp_codegen_object_new(ObjectDisposedException_t973246880_il2cpp_TypeInfo_var);
		ObjectDisposedException__ctor_m1180707260(L_1, (String_t*)L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}
}
// System.Void UniRx.InternalUtil.DisposedObserver`1<UnityEngine.NetworkMessageInfo>::OnNext(T)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* ObjectDisposedException_t973246880_il2cpp_TypeInfo_var;
extern const uint32_t DisposedObserver_1_OnNext_m1087607000_MetadataUsageId;
extern "C"  void DisposedObserver_1_OnNext_m1087607000_gshared (DisposedObserver_1_t1748802346 * __this, NetworkMessageInfo_t2574344884  ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DisposedObserver_1_OnNext_m1087607000_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_0 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		ObjectDisposedException_t973246880 * L_1 = (ObjectDisposedException_t973246880 *)il2cpp_codegen_object_new(ObjectDisposedException_t973246880_il2cpp_TypeInfo_var);
		ObjectDisposedException__ctor_m1180707260(L_1, (String_t*)L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}
}
// System.Void UniRx.InternalUtil.DisposedObserver`1<UnityEngine.NetworkPlayer>::.ctor()
extern "C"  void DisposedObserver_1__ctor_m3295416528_gshared (DisposedObserver_1_t455594834 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UniRx.InternalUtil.DisposedObserver`1<UnityEngine.NetworkPlayer>::.cctor()
extern "C"  void DisposedObserver_1__cctor_m2891568349_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		DisposedObserver_1_t455594834 * L_0 = (DisposedObserver_1_t455594834 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		((  void (*) (DisposedObserver_1_t455594834 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		((DisposedObserver_1_t455594834_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->static_fields)->set_Instance_0(L_0);
		return;
	}
}
// System.Void UniRx.InternalUtil.DisposedObserver`1<UnityEngine.NetworkPlayer>::OnCompleted()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* ObjectDisposedException_t973246880_il2cpp_TypeInfo_var;
extern const uint32_t DisposedObserver_1_OnCompleted_m2468046938_MetadataUsageId;
extern "C"  void DisposedObserver_1_OnCompleted_m2468046938_gshared (DisposedObserver_1_t455594834 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DisposedObserver_1_OnCompleted_m2468046938_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_0 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		ObjectDisposedException_t973246880 * L_1 = (ObjectDisposedException_t973246880 *)il2cpp_codegen_object_new(ObjectDisposedException_t973246880_il2cpp_TypeInfo_var);
		ObjectDisposedException__ctor_m1180707260(L_1, (String_t*)L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}
}
// System.Void UniRx.InternalUtil.DisposedObserver`1<UnityEngine.NetworkPlayer>::OnError(System.Exception)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* ObjectDisposedException_t973246880_il2cpp_TypeInfo_var;
extern const uint32_t DisposedObserver_1_OnError_m4093272711_MetadataUsageId;
extern "C"  void DisposedObserver_1_OnError_m4093272711_gshared (DisposedObserver_1_t455594834 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DisposedObserver_1_OnError_m4093272711_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_0 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		ObjectDisposedException_t973246880 * L_1 = (ObjectDisposedException_t973246880 *)il2cpp_codegen_object_new(ObjectDisposedException_t973246880_il2cpp_TypeInfo_var);
		ObjectDisposedException__ctor_m1180707260(L_1, (String_t*)L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}
}
// System.Void UniRx.InternalUtil.DisposedObserver`1<UnityEngine.NetworkPlayer>::OnNext(T)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* ObjectDisposedException_t973246880_il2cpp_TypeInfo_var;
extern const uint32_t DisposedObserver_1_OnNext_m40122232_MetadataUsageId;
extern "C"  void DisposedObserver_1_OnNext_m40122232_gshared (DisposedObserver_1_t455594834 * __this, NetworkPlayer_t1281137372  ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DisposedObserver_1_OnNext_m40122232_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_0 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		ObjectDisposedException_t973246880 * L_1 = (ObjectDisposedException_t973246880 *)il2cpp_codegen_object_new(ObjectDisposedException_t973246880_il2cpp_TypeInfo_var);
		ObjectDisposedException__ctor_m1180707260(L_1, (String_t*)L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}
}
// System.Void UniRx.InternalUtil.DisposedObserver`1<UnityEngine.Quaternion>::.ctor()
extern "C"  void DisposedObserver_1__ctor_m2005339719_gshared (DisposedObserver_1_t1066173441 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UniRx.InternalUtil.DisposedObserver`1<UnityEngine.Quaternion>::.cctor()
extern "C"  void DisposedObserver_1__cctor_m1553892934_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		DisposedObserver_1_t1066173441 * L_0 = (DisposedObserver_1_t1066173441 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		((  void (*) (DisposedObserver_1_t1066173441 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		((DisposedObserver_1_t1066173441_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->static_fields)->set_Instance_0(L_0);
		return;
	}
}
// System.Void UniRx.InternalUtil.DisposedObserver`1<UnityEngine.Quaternion>::OnCompleted()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* ObjectDisposedException_t973246880_il2cpp_TypeInfo_var;
extern const uint32_t DisposedObserver_1_OnCompleted_m709505169_MetadataUsageId;
extern "C"  void DisposedObserver_1_OnCompleted_m709505169_gshared (DisposedObserver_1_t1066173441 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DisposedObserver_1_OnCompleted_m709505169_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_0 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		ObjectDisposedException_t973246880 * L_1 = (ObjectDisposedException_t973246880 *)il2cpp_codegen_object_new(ObjectDisposedException_t973246880_il2cpp_TypeInfo_var);
		ObjectDisposedException__ctor_m1180707260(L_1, (String_t*)L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}
}
// System.Void UniRx.InternalUtil.DisposedObserver`1<UnityEngine.Quaternion>::OnError(System.Exception)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* ObjectDisposedException_t973246880_il2cpp_TypeInfo_var;
extern const uint32_t DisposedObserver_1_OnError_m3512963134_MetadataUsageId;
extern "C"  void DisposedObserver_1_OnError_m3512963134_gshared (DisposedObserver_1_t1066173441 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DisposedObserver_1_OnError_m3512963134_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_0 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		ObjectDisposedException_t973246880 * L_1 = (ObjectDisposedException_t973246880 *)il2cpp_codegen_object_new(ObjectDisposedException_t973246880_il2cpp_TypeInfo_var);
		ObjectDisposedException__ctor_m1180707260(L_1, (String_t*)L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}
}
// System.Void UniRx.InternalUtil.DisposedObserver`1<UnityEngine.Quaternion>::OnNext(T)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* ObjectDisposedException_t973246880_il2cpp_TypeInfo_var;
extern const uint32_t DisposedObserver_1_OnNext_m1521857327_MetadataUsageId;
extern "C"  void DisposedObserver_1_OnNext_m1521857327_gshared (DisposedObserver_1_t1066173441 * __this, Quaternion_t1891715979  ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DisposedObserver_1_OnNext_m1521857327_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_0 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		ObjectDisposedException_t973246880 * L_1 = (ObjectDisposedException_t973246880 *)il2cpp_codegen_object_new(ObjectDisposedException_t973246880_il2cpp_TypeInfo_var);
		ObjectDisposedException__ctor_m1180707260(L_1, (String_t*)L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}
}
// System.Void UniRx.InternalUtil.DisposedObserver`1<UnityEngine.Rect>::.ctor()
extern "C"  void DisposedObserver_1__ctor_m73590925_gshared (DisposedObserver_1_t699886279 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UniRx.InternalUtil.DisposedObserver`1<UnityEngine.Rect>::.cctor()
extern "C"  void DisposedObserver_1__cctor_m1799222464_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		DisposedObserver_1_t699886279 * L_0 = (DisposedObserver_1_t699886279 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		((  void (*) (DisposedObserver_1_t699886279 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		((DisposedObserver_1_t699886279_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->static_fields)->set_Instance_0(L_0);
		return;
	}
}
// System.Void UniRx.InternalUtil.DisposedObserver`1<UnityEngine.Rect>::OnCompleted()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* ObjectDisposedException_t973246880_il2cpp_TypeInfo_var;
extern const uint32_t DisposedObserver_1_OnCompleted_m3964583511_MetadataUsageId;
extern "C"  void DisposedObserver_1_OnCompleted_m3964583511_gshared (DisposedObserver_1_t699886279 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DisposedObserver_1_OnCompleted_m3964583511_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_0 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		ObjectDisposedException_t973246880 * L_1 = (ObjectDisposedException_t973246880 *)il2cpp_codegen_object_new(ObjectDisposedException_t973246880_il2cpp_TypeInfo_var);
		ObjectDisposedException__ctor_m1180707260(L_1, (String_t*)L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}
}
// System.Void UniRx.InternalUtil.DisposedObserver`1<UnityEngine.Rect>::OnError(System.Exception)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* ObjectDisposedException_t973246880_il2cpp_TypeInfo_var;
extern const uint32_t DisposedObserver_1_OnError_m1364436228_MetadataUsageId;
extern "C"  void DisposedObserver_1_OnError_m1364436228_gshared (DisposedObserver_1_t699886279 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DisposedObserver_1_OnError_m1364436228_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_0 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		ObjectDisposedException_t973246880 * L_1 = (ObjectDisposedException_t973246880 *)il2cpp_codegen_object_new(ObjectDisposedException_t973246880_il2cpp_TypeInfo_var);
		ObjectDisposedException__ctor_m1180707260(L_1, (String_t*)L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}
}
// System.Void UniRx.InternalUtil.DisposedObserver`1<UnityEngine.Rect>::OnNext(T)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* ObjectDisposedException_t973246880_il2cpp_TypeInfo_var;
extern const uint32_t DisposedObserver_1_OnNext_m537138165_MetadataUsageId;
extern "C"  void DisposedObserver_1_OnNext_m537138165_gshared (DisposedObserver_1_t699886279 * __this, Rect_t1525428817  ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DisposedObserver_1_OnNext_m537138165_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_0 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		ObjectDisposedException_t973246880 * L_1 = (ObjectDisposedException_t973246880 *)il2cpp_codegen_object_new(ObjectDisposedException_t973246880_il2cpp_TypeInfo_var);
		ObjectDisposedException__ctor_m1180707260(L_1, (String_t*)L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}
}
// System.Void UniRx.InternalUtil.DisposedObserver`1<UnityEngine.Vector2>::.ctor()
extern "C"  void DisposedObserver_1__ctor_m3697696368_gshared (DisposedObserver_1_t2699787250 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UniRx.InternalUtil.DisposedObserver`1<UnityEngine.Vector2>::.cctor()
extern "C"  void DisposedObserver_1__cctor_m2477341501_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		DisposedObserver_1_t2699787250 * L_0 = (DisposedObserver_1_t2699787250 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		((  void (*) (DisposedObserver_1_t2699787250 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		((DisposedObserver_1_t2699787250_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->static_fields)->set_Instance_0(L_0);
		return;
	}
}
// System.Void UniRx.InternalUtil.DisposedObserver`1<UnityEngine.Vector2>::OnCompleted()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* ObjectDisposedException_t973246880_il2cpp_TypeInfo_var;
extern const uint32_t DisposedObserver_1_OnCompleted_m2538405370_MetadataUsageId;
extern "C"  void DisposedObserver_1_OnCompleted_m2538405370_gshared (DisposedObserver_1_t2699787250 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DisposedObserver_1_OnCompleted_m2538405370_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_0 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		ObjectDisposedException_t973246880 * L_1 = (ObjectDisposedException_t973246880 *)il2cpp_codegen_object_new(ObjectDisposedException_t973246880_il2cpp_TypeInfo_var);
		ObjectDisposedException__ctor_m1180707260(L_1, (String_t*)L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}
}
// System.Void UniRx.InternalUtil.DisposedObserver`1<UnityEngine.Vector2>::OnError(System.Exception)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* ObjectDisposedException_t973246880_il2cpp_TypeInfo_var;
extern const uint32_t DisposedObserver_1_OnError_m2337835047_MetadataUsageId;
extern "C"  void DisposedObserver_1_OnError_m2337835047_gshared (DisposedObserver_1_t2699787250 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DisposedObserver_1_OnError_m2337835047_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_0 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		ObjectDisposedException_t973246880 * L_1 = (ObjectDisposedException_t973246880 *)il2cpp_codegen_object_new(ObjectDisposedException_t973246880_il2cpp_TypeInfo_var);
		ObjectDisposedException__ctor_m1180707260(L_1, (String_t*)L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}
}
// System.Void UniRx.InternalUtil.DisposedObserver`1<UnityEngine.Vector2>::OnNext(T)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* ObjectDisposedException_t973246880_il2cpp_TypeInfo_var;
extern const uint32_t DisposedObserver_1_OnNext_m83991832_MetadataUsageId;
extern "C"  void DisposedObserver_1_OnNext_m83991832_gshared (DisposedObserver_1_t2699787250 * __this, Vector2_t3525329788  ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DisposedObserver_1_OnNext_m83991832_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_0 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		ObjectDisposedException_t973246880 * L_1 = (ObjectDisposedException_t973246880 *)il2cpp_codegen_object_new(ObjectDisposedException_t973246880_il2cpp_TypeInfo_var);
		ObjectDisposedException__ctor_m1180707260(L_1, (String_t*)L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}
}
// System.Void UniRx.InternalUtil.DisposedObserver`1<UnityEngine.Vector3>::.ctor()
extern "C"  void DisposedObserver_1__ctor_m1900745009_gshared (DisposedObserver_1_t2699787251 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UniRx.InternalUtil.DisposedObserver`1<UnityEngine.Vector3>::.cctor()
extern "C"  void DisposedObserver_1__cctor_m2606424220_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		DisposedObserver_1_t2699787251 * L_0 = (DisposedObserver_1_t2699787251 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		((  void (*) (DisposedObserver_1_t2699787251 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		((DisposedObserver_1_t2699787251_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->static_fields)->set_Instance_0(L_0);
		return;
	}
}
// System.Void UniRx.InternalUtil.DisposedObserver`1<UnityEngine.Vector3>::OnCompleted()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* ObjectDisposedException_t973246880_il2cpp_TypeInfo_var;
extern const uint32_t DisposedObserver_1_OnCompleted_m3891715067_MetadataUsageId;
extern "C"  void DisposedObserver_1_OnCompleted_m3891715067_gshared (DisposedObserver_1_t2699787251 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DisposedObserver_1_OnCompleted_m3891715067_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_0 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		ObjectDisposedException_t973246880 * L_1 = (ObjectDisposedException_t973246880 *)il2cpp_codegen_object_new(ObjectDisposedException_t973246880_il2cpp_TypeInfo_var);
		ObjectDisposedException__ctor_m1180707260(L_1, (String_t*)L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}
}
// System.Void UniRx.InternalUtil.DisposedObserver`1<UnityEngine.Vector3>::OnError(System.Exception)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* ObjectDisposedException_t973246880_il2cpp_TypeInfo_var;
extern const uint32_t DisposedObserver_1_OnError_m3030936744_MetadataUsageId;
extern "C"  void DisposedObserver_1_OnError_m3030936744_gshared (DisposedObserver_1_t2699787251 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DisposedObserver_1_OnError_m3030936744_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_0 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		ObjectDisposedException_t973246880 * L_1 = (ObjectDisposedException_t973246880 *)il2cpp_codegen_object_new(ObjectDisposedException_t973246880_il2cpp_TypeInfo_var);
		ObjectDisposedException__ctor_m1180707260(L_1, (String_t*)L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}
}
// System.Void UniRx.InternalUtil.DisposedObserver`1<UnityEngine.Vector3>::OnNext(T)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* ObjectDisposedException_t973246880_il2cpp_TypeInfo_var;
extern const uint32_t DisposedObserver_1_OnNext_m4085556121_MetadataUsageId;
extern "C"  void DisposedObserver_1_OnNext_m4085556121_gshared (DisposedObserver_1_t2699787251 * __this, Vector3_t3525329789  ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DisposedObserver_1_OnNext_m4085556121_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_0 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		ObjectDisposedException_t973246880 * L_1 = (ObjectDisposedException_t973246880 *)il2cpp_codegen_object_new(ObjectDisposedException_t973246880_il2cpp_TypeInfo_var);
		ObjectDisposedException__ctor_m1180707260(L_1, (String_t*)L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}
}
// System.Void UniRx.InternalUtil.DisposedObserver`1<UnityEngine.Vector4>::.ctor()
extern "C"  void DisposedObserver_1__ctor_m103793650_gshared (DisposedObserver_1_t2699787252 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UniRx.InternalUtil.DisposedObserver`1<UnityEngine.Vector4>::.cctor()
extern "C"  void DisposedObserver_1__cctor_m2735506939_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		DisposedObserver_1_t2699787252 * L_0 = (DisposedObserver_1_t2699787252 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		((  void (*) (DisposedObserver_1_t2699787252 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		((DisposedObserver_1_t2699787252_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->static_fields)->set_Instance_0(L_0);
		return;
	}
}
// System.Void UniRx.InternalUtil.DisposedObserver`1<UnityEngine.Vector4>::OnCompleted()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* ObjectDisposedException_t973246880_il2cpp_TypeInfo_var;
extern const uint32_t DisposedObserver_1_OnCompleted_m950057468_MetadataUsageId;
extern "C"  void DisposedObserver_1_OnCompleted_m950057468_gshared (DisposedObserver_1_t2699787252 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DisposedObserver_1_OnCompleted_m950057468_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_0 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		ObjectDisposedException_t973246880 * L_1 = (ObjectDisposedException_t973246880 *)il2cpp_codegen_object_new(ObjectDisposedException_t973246880_il2cpp_TypeInfo_var);
		ObjectDisposedException__ctor_m1180707260(L_1, (String_t*)L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}
}
// System.Void UniRx.InternalUtil.DisposedObserver`1<UnityEngine.Vector4>::OnError(System.Exception)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* ObjectDisposedException_t973246880_il2cpp_TypeInfo_var;
extern const uint32_t DisposedObserver_1_OnError_m3724038441_MetadataUsageId;
extern "C"  void DisposedObserver_1_OnError_m3724038441_gshared (DisposedObserver_1_t2699787252 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DisposedObserver_1_OnError_m3724038441_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_0 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		ObjectDisposedException_t973246880 * L_1 = (ObjectDisposedException_t973246880 *)il2cpp_codegen_object_new(ObjectDisposedException_t973246880_il2cpp_TypeInfo_var);
		ObjectDisposedException__ctor_m1180707260(L_1, (String_t*)L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}
}
// System.Void UniRx.InternalUtil.DisposedObserver`1<UnityEngine.Vector4>::OnNext(T)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* ObjectDisposedException_t973246880_il2cpp_TypeInfo_var;
extern const uint32_t DisposedObserver_1_OnNext_m3792153114_MetadataUsageId;
extern "C"  void DisposedObserver_1_OnNext_m3792153114_gshared (DisposedObserver_1_t2699787252 * __this, Vector4_t3525329790  ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DisposedObserver_1_OnNext_m3792153114_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_0 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		ObjectDisposedException_t973246880 * L_1 = (ObjectDisposedException_t973246880 *)il2cpp_codegen_object_new(ObjectDisposedException_t973246880_il2cpp_TypeInfo_var);
		ObjectDisposedException__ctor_m1180707260(L_1, (String_t*)L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}
}
// System.Void UniRx.InternalUtil.EmptyObserver`1<System.Boolean>::.ctor()
extern "C"  void EmptyObserver_1__ctor_m469315673_gshared (EmptyObserver_1_t3915325627 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UniRx.InternalUtil.EmptyObserver`1<System.Boolean>::.cctor()
extern "C"  void EmptyObserver_1__cctor_m1181787764_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		EmptyObserver_1_t3915325627 * L_0 = (EmptyObserver_1_t3915325627 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		((  void (*) (EmptyObserver_1_t3915325627 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		((EmptyObserver_1_t3915325627_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->static_fields)->set_Instance_0(L_0);
		return;
	}
}
// System.Void UniRx.InternalUtil.EmptyObserver`1<System.Boolean>::OnCompleted()
extern "C"  void EmptyObserver_1_OnCompleted_m2102066467_gshared (EmptyObserver_1_t3915325627 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void UniRx.InternalUtil.EmptyObserver`1<System.Boolean>::OnError(System.Exception)
extern "C"  void EmptyObserver_1_OnError_m340529616_gshared (EmptyObserver_1_t3915325627 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void UniRx.InternalUtil.EmptyObserver`1<System.Boolean>::OnNext(T)
extern "C"  void EmptyObserver_1_OnNext_m2871498945_gshared (EmptyObserver_1_t3915325627 * __this, bool ___value0, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void UniRx.InternalUtil.EmptyObserver`1<System.Byte>::.ctor()
extern "C"  void EmptyObserver_1__ctor_m59149409_gshared (EmptyObserver_1_t2188046811 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UniRx.InternalUtil.EmptyObserver`1<System.Byte>::.cctor()
extern "C"  void EmptyObserver_1__cctor_m1351535468_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		EmptyObserver_1_t2188046811 * L_0 = (EmptyObserver_1_t2188046811 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		((  void (*) (EmptyObserver_1_t2188046811 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		((EmptyObserver_1_t2188046811_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->static_fields)->set_Instance_0(L_0);
		return;
	}
}
// System.Void UniRx.InternalUtil.EmptyObserver`1<System.Byte>::OnCompleted()
extern "C"  void EmptyObserver_1_OnCompleted_m731198251_gshared (EmptyObserver_1_t2188046811 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void UniRx.InternalUtil.EmptyObserver`1<System.Byte>::OnError(System.Exception)
extern "C"  void EmptyObserver_1_OnError_m3138223064_gshared (EmptyObserver_1_t2188046811 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void UniRx.InternalUtil.EmptyObserver`1<System.Byte>::OnNext(T)
extern "C"  void EmptyObserver_1_OnNext_m3838710473_gshared (EmptyObserver_1_t2188046811 * __this, uint8_t ___value0, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void UniRx.InternalUtil.EmptyObserver`1<System.Double>::.ctor()
extern "C"  void EmptyObserver_1__ctor_m2372598506_gshared (EmptyObserver_1_t4238836900 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UniRx.InternalUtil.EmptyObserver`1<System.Double>::.cctor()
extern "C"  void EmptyObserver_1__cctor_m54013443_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		EmptyObserver_1_t4238836900 * L_0 = (EmptyObserver_1_t4238836900 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		((  void (*) (EmptyObserver_1_t4238836900 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		((EmptyObserver_1_t4238836900_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->static_fields)->set_Instance_0(L_0);
		return;
	}
}
// System.Void UniRx.InternalUtil.EmptyObserver`1<System.Double>::OnCompleted()
extern "C"  void EmptyObserver_1_OnCompleted_m4146386676_gshared (EmptyObserver_1_t4238836900 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void UniRx.InternalUtil.EmptyObserver`1<System.Double>::OnError(System.Exception)
extern "C"  void EmptyObserver_1_OnError_m1512094241_gshared (EmptyObserver_1_t4238836900 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void UniRx.InternalUtil.EmptyObserver`1<System.Double>::OnNext(T)
extern "C"  void EmptyObserver_1_OnNext_m2270233362_gshared (EmptyObserver_1_t4238836900 * __this, double ___value0, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void UniRx.InternalUtil.EmptyObserver`1<System.Int32>::.ctor()
extern "C"  void EmptyObserver_1__ctor_m1369830975_gshared (EmptyObserver_1_t2256767777 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UniRx.InternalUtil.EmptyObserver`1<System.Int32>::.cctor()
extern "C"  void EmptyObserver_1__cctor_m3327958350_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		EmptyObserver_1_t2256767777 * L_0 = (EmptyObserver_1_t2256767777 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		((  void (*) (EmptyObserver_1_t2256767777 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		((EmptyObserver_1_t2256767777_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->static_fields)->set_Instance_0(L_0);
		return;
	}
}
// System.Void UniRx.InternalUtil.EmptyObserver`1<System.Int32>::OnCompleted()
extern "C"  void EmptyObserver_1_OnCompleted_m1953054345_gshared (EmptyObserver_1_t2256767777 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void UniRx.InternalUtil.EmptyObserver`1<System.Int32>::OnError(System.Exception)
extern "C"  void EmptyObserver_1_OnError_m2605017654_gshared (EmptyObserver_1_t2256767777 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void UniRx.InternalUtil.EmptyObserver`1<System.Int32>::OnNext(T)
extern "C"  void EmptyObserver_1_OnNext_m683310375_gshared (EmptyObserver_1_t2256767777 * __this, int32_t ___value0, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void UniRx.InternalUtil.EmptyObserver`1<System.Int64>::.ctor()
extern "C"  void EmptyObserver_1__ctor_m2458143710_gshared (EmptyObserver_1_t2256767872 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UniRx.InternalUtil.EmptyObserver`1<System.Int64>::.cctor()
extern "C"  void EmptyObserver_1__cctor_m2705914767_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		EmptyObserver_1_t2256767872 * L_0 = (EmptyObserver_1_t2256767872 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		((  void (*) (EmptyObserver_1_t2256767872 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		((EmptyObserver_1_t2256767872_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->static_fields)->set_Instance_0(L_0);
		return;
	}
}
// System.Void UniRx.InternalUtil.EmptyObserver`1<System.Int64>::OnCompleted()
extern "C"  void EmptyObserver_1_OnCompleted_m1668456680_gshared (EmptyObserver_1_t2256767872 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void UniRx.InternalUtil.EmptyObserver`1<System.Int64>::OnError(System.Exception)
extern "C"  void EmptyObserver_1_OnError_m4025169429_gshared (EmptyObserver_1_t2256767872 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void UniRx.InternalUtil.EmptyObserver`1<System.Int64>::OnNext(T)
extern "C"  void EmptyObserver_1_OnNext_m2874795782_gshared (EmptyObserver_1_t2256767872 * __this, int64_t ___value0, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void UniRx.InternalUtil.EmptyObserver`1<System.Object>::.ctor()
extern "C"  void EmptyObserver_1__ctor_m968110296_gshared (EmptyObserver_1_t246459410 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UniRx.InternalUtil.EmptyObserver`1<System.Object>::.cctor()
extern "C"  void EmptyObserver_1__cctor_m3759519189_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		EmptyObserver_1_t246459410 * L_0 = (EmptyObserver_1_t246459410 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		((  void (*) (EmptyObserver_1_t246459410 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		((EmptyObserver_1_t246459410_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->static_fields)->set_Instance_0(L_0);
		return;
	}
}
// System.Void UniRx.InternalUtil.EmptyObserver`1<System.Object>::OnCompleted()
extern "C"  void EmptyObserver_1_OnCompleted_m1627218530_gshared (EmptyObserver_1_t246459410 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void UniRx.InternalUtil.EmptyObserver`1<System.Object>::OnError(System.Exception)
extern "C"  void EmptyObserver_1_OnError_m1624084623_gshared (EmptyObserver_1_t246459410 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void UniRx.InternalUtil.EmptyObserver`1<System.Object>::OnNext(T)
extern "C"  void EmptyObserver_1_OnNext_m1176794496_gshared (EmptyObserver_1_t246459410 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void UniRx.InternalUtil.EmptyObserver`1<System.Single>::.ctor()
extern "C"  void EmptyObserver_1__ctor_m626072225_gshared (EmptyObserver_1_t367562011 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UniRx.InternalUtil.EmptyObserver`1<System.Single>::.cctor()
extern "C"  void EmptyObserver_1__cctor_m1746273580_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		EmptyObserver_1_t367562011 * L_0 = (EmptyObserver_1_t367562011 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		((  void (*) (EmptyObserver_1_t367562011 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		((EmptyObserver_1_t367562011_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->static_fields)->set_Instance_0(L_0);
		return;
	}
}
// System.Void UniRx.InternalUtil.EmptyObserver`1<System.Single>::OnCompleted()
extern "C"  void EmptyObserver_1_OnCompleted_m1011618155_gshared (EmptyObserver_1_t367562011 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void UniRx.InternalUtil.EmptyObserver`1<System.Single>::OnError(System.Exception)
extern "C"  void EmptyObserver_1_OnError_m2871589400_gshared (EmptyObserver_1_t367562011 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void UniRx.InternalUtil.EmptyObserver`1<System.Single>::OnNext(T)
extern "C"  void EmptyObserver_1_OnNext_m3190690057_gshared (EmptyObserver_1_t367562011 * __this, float ___value0, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void UniRx.InternalUtil.EmptyObserver`1<TableButtons/StatkaPoTableViev>::.ctor()
extern "C"  void EmptyObserver_1__ctor_m100393469_gshared (EmptyObserver_1_t2917748807 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UniRx.InternalUtil.EmptyObserver`1<TableButtons/StatkaPoTableViev>::.cctor()
extern "C"  void EmptyObserver_1__cctor_m2630101328_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		EmptyObserver_1_t2917748807 * L_0 = (EmptyObserver_1_t2917748807 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		((  void (*) (EmptyObserver_1_t2917748807 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		((EmptyObserver_1_t2917748807_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->static_fields)->set_Instance_0(L_0);
		return;
	}
}
// System.Void UniRx.InternalUtil.EmptyObserver`1<TableButtons/StatkaPoTableViev>::OnCompleted()
extern "C"  void EmptyObserver_1_OnCompleted_m1883431879_gshared (EmptyObserver_1_t2917748807 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void UniRx.InternalUtil.EmptyObserver`1<TableButtons/StatkaPoTableViev>::OnError(System.Exception)
extern "C"  void EmptyObserver_1_OnError_m3498779252_gshared (EmptyObserver_1_t2917748807 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void UniRx.InternalUtil.EmptyObserver`1<TableButtons/StatkaPoTableViev>::OnNext(T)
extern "C"  void EmptyObserver_1_OnNext_m524579173_gshared (EmptyObserver_1_t2917748807 * __this, int32_t ___value0, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void UniRx.InternalUtil.EmptyObserver`1<UniRx.CollectionAddEvent`1<System.Object>>::.ctor()
extern "C"  void EmptyObserver_1__ctor_m555827254_gshared (EmptyObserver_1_t1825874977 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UniRx.InternalUtil.EmptyObserver`1<UniRx.CollectionAddEvent`1<System.Object>>::.cctor()
extern "C"  void EmptyObserver_1__cctor_m3863646775_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		EmptyObserver_1_t1825874977 * L_0 = (EmptyObserver_1_t1825874977 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		((  void (*) (EmptyObserver_1_t1825874977 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		((EmptyObserver_1_t1825874977_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->static_fields)->set_Instance_0(L_0);
		return;
	}
}
// System.Void UniRx.InternalUtil.EmptyObserver`1<UniRx.CollectionAddEvent`1<System.Object>>::OnCompleted()
extern "C"  void EmptyObserver_1_OnCompleted_m749531968_gshared (EmptyObserver_1_t1825874977 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void UniRx.InternalUtil.EmptyObserver`1<UniRx.CollectionAddEvent`1<System.Object>>::OnError(System.Exception)
extern "C"  void EmptyObserver_1_OnError_m2806301805_gshared (EmptyObserver_1_t1825874977 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void UniRx.InternalUtil.EmptyObserver`1<UniRx.CollectionAddEvent`1<System.Object>>::OnNext(T)
extern "C"  void EmptyObserver_1_OnNext_m109782366_gshared (EmptyObserver_1_t1825874977 * __this, CollectionAddEvent_1_t2416521987  ___value0, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void UniRx.InternalUtil.EmptyObserver`1<UniRx.CollectionAddEvent`1<UniRx.Tuple`2<System.Object,System.Object>>>::.ctor()
extern "C"  void EmptyObserver_1__ctor_m3466252522_gshared (EmptyObserver_1_t1358030376 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UniRx.InternalUtil.EmptyObserver`1<UniRx.CollectionAddEvent`1<UniRx.Tuple`2<System.Object,System.Object>>>::.cctor()
extern "C"  void EmptyObserver_1__cctor_m3892516867_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		EmptyObserver_1_t1358030376 * L_0 = (EmptyObserver_1_t1358030376 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		((  void (*) (EmptyObserver_1_t1358030376 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		((EmptyObserver_1_t1358030376_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->static_fields)->set_Instance_0(L_0);
		return;
	}
}
// System.Void UniRx.InternalUtil.EmptyObserver`1<UniRx.CollectionAddEvent`1<UniRx.Tuple`2<System.Object,System.Object>>>::OnCompleted()
extern "C"  void EmptyObserver_1_OnCompleted_m3466341620_gshared (EmptyObserver_1_t1358030376 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void UniRx.InternalUtil.EmptyObserver`1<UniRx.CollectionAddEvent`1<UniRx.Tuple`2<System.Object,System.Object>>>::OnError(System.Exception)
extern "C"  void EmptyObserver_1_OnError_m3141996577_gshared (EmptyObserver_1_t1358030376 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void UniRx.InternalUtil.EmptyObserver`1<UniRx.CollectionAddEvent`1<UniRx.Tuple`2<System.Object,System.Object>>>::OnNext(T)
extern "C"  void EmptyObserver_1_OnNext_m1004755218_gshared (EmptyObserver_1_t1358030376 * __this, CollectionAddEvent_1_t1948677386  ___value0, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void UniRx.InternalUtil.EmptyObserver`1<UniRx.CollectionMoveEvent`1<System.Object>>::.ctor()
extern "C"  void EmptyObserver_1__ctor_m380576264_gshared (EmptyObserver_1_t2325786837 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UniRx.InternalUtil.EmptyObserver`1<UniRx.CollectionMoveEvent`1<System.Object>>::.cctor()
extern "C"  void EmptyObserver_1__cctor_m2725833381_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		EmptyObserver_1_t2325786837 * L_0 = (EmptyObserver_1_t2325786837 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		((  void (*) (EmptyObserver_1_t2325786837 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		((EmptyObserver_1_t2325786837_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->static_fields)->set_Instance_0(L_0);
		return;
	}
}
// System.Void UniRx.InternalUtil.EmptyObserver`1<UniRx.CollectionMoveEvent`1<System.Object>>::OnCompleted()
extern "C"  void EmptyObserver_1_OnCompleted_m3278548882_gshared (EmptyObserver_1_t2325786837 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void UniRx.InternalUtil.EmptyObserver`1<UniRx.CollectionMoveEvent`1<System.Object>>::OnError(System.Exception)
extern "C"  void EmptyObserver_1_OnError_m2839582143_gshared (EmptyObserver_1_t2325786837 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void UniRx.InternalUtil.EmptyObserver`1<UniRx.CollectionMoveEvent`1<System.Object>>::OnNext(T)
extern "C"  void EmptyObserver_1_OnNext_m3492272816_gshared (EmptyObserver_1_t2325786837 * __this, CollectionMoveEvent_1_t2916433847  ___value0, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void UniRx.InternalUtil.EmptyObserver`1<UniRx.CollectionMoveEvent`1<UniRx.Tuple`2<System.Object,System.Object>>>::.ctor()
extern "C"  void EmptyObserver_1__ctor_m3500759128_gshared (EmptyObserver_1_t1857942236 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UniRx.InternalUtil.EmptyObserver`1<UniRx.CollectionMoveEvent`1<UniRx.Tuple`2<System.Object,System.Object>>>::.cctor()
extern "C"  void EmptyObserver_1__cctor_m667254357_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		EmptyObserver_1_t1857942236 * L_0 = (EmptyObserver_1_t1857942236 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		((  void (*) (EmptyObserver_1_t1857942236 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		((EmptyObserver_1_t1857942236_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->static_fields)->set_Instance_0(L_0);
		return;
	}
}
// System.Void UniRx.InternalUtil.EmptyObserver`1<UniRx.CollectionMoveEvent`1<UniRx.Tuple`2<System.Object,System.Object>>>::OnCompleted()
extern "C"  void EmptyObserver_1_OnCompleted_m2992040418_gshared (EmptyObserver_1_t1857942236 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void UniRx.InternalUtil.EmptyObserver`1<UniRx.CollectionMoveEvent`1<UniRx.Tuple`2<System.Object,System.Object>>>::OnError(System.Exception)
extern "C"  void EmptyObserver_1_OnError_m3466057743_gshared (EmptyObserver_1_t1857942236 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void UniRx.InternalUtil.EmptyObserver`1<UniRx.CollectionMoveEvent`1<UniRx.Tuple`2<System.Object,System.Object>>>::OnNext(T)
extern "C"  void EmptyObserver_1_OnNext_m4100832512_gshared (EmptyObserver_1_t1857942236 * __this, CollectionMoveEvent_1_t2448589246  ___value0, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void UniRx.InternalUtil.EmptyObserver`1<UniRx.CollectionRemoveEvent`1<System.Object>>::.ctor()
extern "C"  void EmptyObserver_1__ctor_m1804469883_gshared (EmptyObserver_1_t3946958010 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UniRx.InternalUtil.EmptyObserver`1<UniRx.CollectionRemoveEvent`1<System.Object>>::.cctor()
extern "C"  void EmptyObserver_1__cctor_m3916862610_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		EmptyObserver_1_t3946958010 * L_0 = (EmptyObserver_1_t3946958010 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		((  void (*) (EmptyObserver_1_t3946958010 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		((EmptyObserver_1_t3946958010_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->static_fields)->set_Instance_0(L_0);
		return;
	}
}
// System.Void UniRx.InternalUtil.EmptyObserver`1<UniRx.CollectionRemoveEvent`1<System.Object>>::OnCompleted()
extern "C"  void EmptyObserver_1_OnCompleted_m1241199045_gshared (EmptyObserver_1_t3946958010 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void UniRx.InternalUtil.EmptyObserver`1<UniRx.CollectionRemoveEvent`1<System.Object>>::OnError(System.Exception)
extern "C"  void EmptyObserver_1_OnError_m3845677938_gshared (EmptyObserver_1_t3946958010 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void UniRx.InternalUtil.EmptyObserver`1<UniRx.CollectionRemoveEvent`1<System.Object>>::OnNext(T)
extern "C"  void EmptyObserver_1_OnNext_m1759473251_gshared (EmptyObserver_1_t3946958010 * __this, CollectionRemoveEvent_1_t242637724  ___value0, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void UniRx.InternalUtil.EmptyObserver`1<UniRx.CollectionRemoveEvent`1<UniRx.Tuple`2<System.Object,System.Object>>>::.ctor()
extern "C"  void EmptyObserver_1__ctor_m3874884805_gshared (EmptyObserver_1_t3479113409 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UniRx.InternalUtil.EmptyObserver`1<UniRx.CollectionRemoveEvent`1<UniRx.Tuple`2<System.Object,System.Object>>>::.cctor()
extern "C"  void EmptyObserver_1__cctor_m3675215752_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		EmptyObserver_1_t3479113409 * L_0 = (EmptyObserver_1_t3479113409 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		((  void (*) (EmptyObserver_1_t3479113409 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		((EmptyObserver_1_t3479113409_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->static_fields)->set_Instance_0(L_0);
		return;
	}
}
// System.Void UniRx.InternalUtil.EmptyObserver`1<UniRx.CollectionRemoveEvent`1<UniRx.Tuple`2<System.Object,System.Object>>>::OnCompleted()
extern "C"  void EmptyObserver_1_OnCompleted_m1196677263_gshared (EmptyObserver_1_t3479113409 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void UniRx.InternalUtil.EmptyObserver`1<UniRx.CollectionRemoveEvent`1<UniRx.Tuple`2<System.Object,System.Object>>>::OnError(System.Exception)
extern "C"  void EmptyObserver_1_OnError_m3358272316_gshared (EmptyObserver_1_t3479113409 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void UniRx.InternalUtil.EmptyObserver`1<UniRx.CollectionRemoveEvent`1<UniRx.Tuple`2<System.Object,System.Object>>>::OnNext(T)
extern "C"  void EmptyObserver_1_OnNext_m2858355245_gshared (EmptyObserver_1_t3479113409 * __this, CollectionRemoveEvent_1_t4069760419  ___value0, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void UniRx.InternalUtil.EmptyObserver`1<UniRx.CollectionReplaceEvent`1<System.Object>>::.ctor()
extern "C"  void EmptyObserver_1__ctor_m3817539273_gshared (EmptyObserver_1_t1251103526 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UniRx.InternalUtil.EmptyObserver`1<UniRx.CollectionReplaceEvent`1<System.Object>>::.cctor()
extern "C"  void EmptyObserver_1__cctor_m1897504260_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		EmptyObserver_1_t1251103526 * L_0 = (EmptyObserver_1_t1251103526 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		((  void (*) (EmptyObserver_1_t1251103526 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		((EmptyObserver_1_t1251103526_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->static_fields)->set_Instance_0(L_0);
		return;
	}
}
// System.Void UniRx.InternalUtil.EmptyObserver`1<UniRx.CollectionReplaceEvent`1<System.Object>>::OnCompleted()
extern "C"  void EmptyObserver_1_OnCompleted_m777928595_gshared (EmptyObserver_1_t1251103526 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void UniRx.InternalUtil.EmptyObserver`1<UniRx.CollectionReplaceEvent`1<System.Object>>::OnError(System.Exception)
extern "C"  void EmptyObserver_1_OnError_m3789499456_gshared (EmptyObserver_1_t1251103526 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void UniRx.InternalUtil.EmptyObserver`1<UniRx.CollectionReplaceEvent`1<System.Object>>::OnNext(T)
extern "C"  void EmptyObserver_1_OnNext_m3583873841_gshared (EmptyObserver_1_t1251103526 * __this, CollectionReplaceEvent_1_t1841750536  ___value0, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void UniRx.InternalUtil.EmptyObserver`1<UniRx.CollectionReplaceEvent`1<UniRx.Tuple`2<System.Object,System.Object>>>::.ctor()
extern "C"  void EmptyObserver_1__ctor_m2696238903_gshared (EmptyObserver_1_t783258925 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UniRx.InternalUtil.EmptyObserver`1<UniRx.CollectionReplaceEvent`1<UniRx.Tuple`2<System.Object,System.Object>>>::.cctor()
extern "C"  void EmptyObserver_1__cctor_m1496931158_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		EmptyObserver_1_t783258925 * L_0 = (EmptyObserver_1_t783258925 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		((  void (*) (EmptyObserver_1_t783258925 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		((EmptyObserver_1_t783258925_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->static_fields)->set_Instance_0(L_0);
		return;
	}
}
// System.Void UniRx.InternalUtil.EmptyObserver`1<UniRx.CollectionReplaceEvent`1<UniRx.Tuple`2<System.Object,System.Object>>>::OnCompleted()
extern "C"  void EmptyObserver_1_OnCompleted_m2440693121_gshared (EmptyObserver_1_t783258925 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void UniRx.InternalUtil.EmptyObserver`1<UniRx.CollectionReplaceEvent`1<UniRx.Tuple`2<System.Object,System.Object>>>::OnError(System.Exception)
extern "C"  void EmptyObserver_1_OnError_m105610542_gshared (EmptyObserver_1_t783258925 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void UniRx.InternalUtil.EmptyObserver`1<UniRx.CollectionReplaceEvent`1<UniRx.Tuple`2<System.Object,System.Object>>>::OnNext(T)
extern "C"  void EmptyObserver_1_OnNext_m4051009567_gshared (EmptyObserver_1_t783258925 * __this, CollectionReplaceEvent_1_t1373905935  ___value0, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void UniRx.InternalUtil.EmptyObserver`1<UniRx.CountChangedStatus>::.ctor()
extern "C"  void EmptyObserver_1__ctor_m14799999_gshared (EmptyObserver_1_t3180580711 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UniRx.InternalUtil.EmptyObserver`1<UniRx.CountChangedStatus>::.cctor()
extern "C"  void EmptyObserver_1__cctor_m4271671054_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		EmptyObserver_1_t3180580711 * L_0 = (EmptyObserver_1_t3180580711 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		((  void (*) (EmptyObserver_1_t3180580711 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		((EmptyObserver_1_t3180580711_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->static_fields)->set_Instance_0(L_0);
		return;
	}
}
// System.Void UniRx.InternalUtil.EmptyObserver`1<UniRx.CountChangedStatus>::OnCompleted()
extern "C"  void EmptyObserver_1_OnCompleted_m1817537737_gshared (EmptyObserver_1_t3180580711 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void UniRx.InternalUtil.EmptyObserver`1<UniRx.CountChangedStatus>::OnError(System.Exception)
extern "C"  void EmptyObserver_1_OnError_m1025390710_gshared (EmptyObserver_1_t3180580711 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void UniRx.InternalUtil.EmptyObserver`1<UniRx.CountChangedStatus>::OnNext(T)
extern "C"  void EmptyObserver_1_OnNext_m4168600423_gshared (EmptyObserver_1_t3180580711 * __this, int32_t ___value0, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void UniRx.InternalUtil.EmptyObserver`1<UniRx.DictionaryAddEvent`2<System.Object,System.Object>>::.ctor()
extern "C"  void EmptyObserver_1__ctor_m2504281707_gshared (EmptyObserver_1_t3955301294 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UniRx.InternalUtil.EmptyObserver`1<UniRx.DictionaryAddEvent`2<System.Object,System.Object>>::.cctor()
extern "C"  void EmptyObserver_1__cctor_m4136192674_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		EmptyObserver_1_t3955301294 * L_0 = (EmptyObserver_1_t3955301294 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		((  void (*) (EmptyObserver_1_t3955301294 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		((EmptyObserver_1_t3955301294_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->static_fields)->set_Instance_0(L_0);
		return;
	}
}
// System.Void UniRx.InternalUtil.EmptyObserver`1<UniRx.DictionaryAddEvent`2<System.Object,System.Object>>::OnCompleted()
extern "C"  void EmptyObserver_1_OnCompleted_m1165477301_gshared (EmptyObserver_1_t3955301294 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void UniRx.InternalUtil.EmptyObserver`1<UniRx.DictionaryAddEvent`2<System.Object,System.Object>>::OnError(System.Exception)
extern "C"  void EmptyObserver_1_OnError_m1581682530_gshared (EmptyObserver_1_t3955301294 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void UniRx.InternalUtil.EmptyObserver`1<UniRx.DictionaryAddEvent`2<System.Object,System.Object>>::OnNext(T)
extern "C"  void EmptyObserver_1_OnNext_m4263737939_gshared (EmptyObserver_1_t3955301294 * __this, DictionaryAddEvent_2_t250981008  ___value0, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void UniRx.InternalUtil.EmptyObserver`1<UniRx.DictionaryRemoveEvent`2<System.Object,System.Object>>::.ctor()
extern "C"  void EmptyObserver_1__ctor_m3768066272_gshared (EmptyObserver_1_t2731641281 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UniRx.InternalUtil.EmptyObserver`1<UniRx.DictionaryRemoveEvent`2<System.Object,System.Object>>::.cctor()
extern "C"  void EmptyObserver_1__cctor_m363841229_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		EmptyObserver_1_t2731641281 * L_0 = (EmptyObserver_1_t2731641281 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		((  void (*) (EmptyObserver_1_t2731641281 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		((EmptyObserver_1_t2731641281_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->static_fields)->set_Instance_0(L_0);
		return;
	}
}
// System.Void UniRx.InternalUtil.EmptyObserver`1<UniRx.DictionaryRemoveEvent`2<System.Object,System.Object>>::OnCompleted()
extern "C"  void EmptyObserver_1_OnCompleted_m2422156394_gshared (EmptyObserver_1_t2731641281 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void UniRx.InternalUtil.EmptyObserver`1<UniRx.DictionaryRemoveEvent`2<System.Object,System.Object>>::OnError(System.Exception)
extern "C"  void EmptyObserver_1_OnError_m1575521943_gshared (EmptyObserver_1_t2731641281 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void UniRx.InternalUtil.EmptyObserver`1<UniRx.DictionaryRemoveEvent`2<System.Object,System.Object>>::OnNext(T)
extern "C"  void EmptyObserver_1_OnNext_m3284960136_gshared (EmptyObserver_1_t2731641281 * __this, DictionaryRemoveEvent_2_t3322288291  ___value0, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void UniRx.InternalUtil.EmptyObserver`1<UniRx.DictionaryReplaceEvent`2<System.Object,System.Object>>::.ctor()
extern "C"  void EmptyObserver_1__ctor_m2602292158_gshared (EmptyObserver_1_t3188280053 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UniRx.InternalUtil.EmptyObserver`1<UniRx.DictionaryReplaceEvent`2<System.Object,System.Object>>::.cctor()
extern "C"  void EmptyObserver_1__cctor_m2879549359_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		EmptyObserver_1_t3188280053 * L_0 = (EmptyObserver_1_t3188280053 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		((  void (*) (EmptyObserver_1_t3188280053 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		((EmptyObserver_1_t3188280053_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->static_fields)->set_Instance_0(L_0);
		return;
	}
}
// System.Void UniRx.InternalUtil.EmptyObserver`1<UniRx.DictionaryReplaceEvent`2<System.Object,System.Object>>::OnCompleted()
extern "C"  void EmptyObserver_1_OnCompleted_m293388488_gshared (EmptyObserver_1_t3188280053 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void UniRx.InternalUtil.EmptyObserver`1<UniRx.DictionaryReplaceEvent`2<System.Object,System.Object>>::OnError(System.Exception)
extern "C"  void EmptyObserver_1_OnError_m1868858869_gshared (EmptyObserver_1_t3188280053 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void UniRx.InternalUtil.EmptyObserver`1<UniRx.DictionaryReplaceEvent`2<System.Object,System.Object>>::OnNext(T)
extern "C"  void EmptyObserver_1_OnNext_m3962500838_gshared (EmptyObserver_1_t3188280053 * __this, DictionaryReplaceEvent_2_t3778927063  ___value0, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void UniRx.InternalUtil.EmptyObserver`1<UniRx.Pair`1<System.Object>>::.ctor()
extern "C"  void EmptyObserver_1__ctor_m4047917427_gshared (EmptyObserver_1_t3240450572 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UniRx.InternalUtil.EmptyObserver`1<UniRx.Pair`1<System.Object>>::.cctor()
extern "C"  void EmptyObserver_1__cctor_m449292442_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		EmptyObserver_1_t3240450572 * L_0 = (EmptyObserver_1_t3240450572 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		((  void (*) (EmptyObserver_1_t3240450572 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		((EmptyObserver_1_t3240450572_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->static_fields)->set_Instance_0(L_0);
		return;
	}
}
// System.Void UniRx.InternalUtil.EmptyObserver`1<UniRx.Pair`1<System.Object>>::OnCompleted()
extern "C"  void EmptyObserver_1_OnCompleted_m1910334141_gshared (EmptyObserver_1_t3240450572 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void UniRx.InternalUtil.EmptyObserver`1<UniRx.Pair`1<System.Object>>::OnError(System.Exception)
extern "C"  void EmptyObserver_1_OnError_m3930120298_gshared (EmptyObserver_1_t3240450572 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void UniRx.InternalUtil.EmptyObserver`1<UniRx.Pair`1<System.Object>>::OnNext(T)
extern "C"  void EmptyObserver_1_OnNext_m1638980443_gshared (EmptyObserver_1_t3240450572 * __this, Pair_1_t3831097582  ___value0, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void UniRx.InternalUtil.EmptyObserver`1<UniRx.TimeInterval`1<System.Object>>::.ctor()
extern "C"  void EmptyObserver_1__ctor_m3507249563_gshared (EmptyObserver_1_t117418532 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UniRx.InternalUtil.EmptyObserver`1<UniRx.TimeInterval`1<System.Object>>::.cctor()
extern "C"  void EmptyObserver_1__cctor_m868457842_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		EmptyObserver_1_t117418532 * L_0 = (EmptyObserver_1_t117418532 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		((  void (*) (EmptyObserver_1_t117418532 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		((EmptyObserver_1_t117418532_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->static_fields)->set_Instance_0(L_0);
		return;
	}
}
// System.Void UniRx.InternalUtil.EmptyObserver`1<UniRx.TimeInterval`1<System.Object>>::OnCompleted()
extern "C"  void EmptyObserver_1_OnCompleted_m2362488037_gshared (EmptyObserver_1_t117418532 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void UniRx.InternalUtil.EmptyObserver`1<UniRx.TimeInterval`1<System.Object>>::OnError(System.Exception)
extern "C"  void EmptyObserver_1_OnError_m1833055890_gshared (EmptyObserver_1_t117418532 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void UniRx.InternalUtil.EmptyObserver`1<UniRx.TimeInterval`1<System.Object>>::OnNext(T)
extern "C"  void EmptyObserver_1_OnNext_m1748205955_gshared (EmptyObserver_1_t117418532 * __this, TimeInterval_1_t708065542  ___value0, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void UniRx.InternalUtil.EmptyObserver`1<UniRx.Timestamped`1<System.Object>>::.ctor()
extern "C"  void EmptyObserver_1__ctor_m3462131422_gshared (EmptyObserver_1_t1928537263 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UniRx.InternalUtil.EmptyObserver`1<UniRx.Timestamped`1<System.Object>>::.cctor()
extern "C"  void EmptyObserver_1__cctor_m3764762767_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		EmptyObserver_1_t1928537263 * L_0 = (EmptyObserver_1_t1928537263 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		((  void (*) (EmptyObserver_1_t1928537263 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		((EmptyObserver_1_t1928537263_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->static_fields)->set_Instance_0(L_0);
		return;
	}
}
// System.Void UniRx.InternalUtil.EmptyObserver`1<UniRx.Timestamped`1<System.Object>>::OnCompleted()
extern "C"  void EmptyObserver_1_OnCompleted_m3116631016_gshared (EmptyObserver_1_t1928537263 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void UniRx.InternalUtil.EmptyObserver`1<UniRx.Timestamped`1<System.Object>>::OnError(System.Exception)
extern "C"  void EmptyObserver_1_OnError_m4200725_gshared (EmptyObserver_1_t1928537263 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void UniRx.InternalUtil.EmptyObserver`1<UniRx.Timestamped`1<System.Object>>::OnNext(T)
extern "C"  void EmptyObserver_1_OnNext_m1339345414_gshared (EmptyObserver_1_t1928537263 * __this, Timestamped_1_t2519184273  ___value0, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void UniRx.InternalUtil.EmptyObserver`1<UniRx.Tuple`2<System.Object,System.Int32>>::.ctor()
extern "C"  void EmptyObserver_1__ctor_m4137202945_gshared (EmptyObserver_1_t1788923176 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UniRx.InternalUtil.EmptyObserver`1<UniRx.Tuple`2<System.Object,System.Int32>>::.cctor()
extern "C"  void EmptyObserver_1__cctor_m3217143500_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		EmptyObserver_1_t1788923176 * L_0 = (EmptyObserver_1_t1788923176 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		((  void (*) (EmptyObserver_1_t1788923176 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		((EmptyObserver_1_t1788923176_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->static_fields)->set_Instance_0(L_0);
		return;
	}
}
// System.Void UniRx.InternalUtil.EmptyObserver`1<UniRx.Tuple`2<System.Object,System.Int32>>::OnCompleted()
extern "C"  void EmptyObserver_1_OnCompleted_m307127243_gshared (EmptyObserver_1_t1788923176 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void UniRx.InternalUtil.EmptyObserver`1<UniRx.Tuple`2<System.Object,System.Int32>>::OnError(System.Exception)
extern "C"  void EmptyObserver_1_OnError_m4150443128_gshared (EmptyObserver_1_t1788923176 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void UniRx.InternalUtil.EmptyObserver`1<UniRx.Tuple`2<System.Object,System.Int32>>::OnNext(T)
extern "C"  void EmptyObserver_1_OnNext_m1543017321_gshared (EmptyObserver_1_t1788923176 * __this, Tuple_2_t2379570186  ___value0, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void UniRx.InternalUtil.EmptyObserver`1<UniRx.Tuple`2<System.Object,System.Object>>::.ctor()
extern "C"  void EmptyObserver_1__ctor_m2923199000_gshared (EmptyObserver_1_t4073582105 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UniRx.InternalUtil.EmptyObserver`1<UniRx.Tuple`2<System.Object,System.Object>>::.cctor()
extern "C"  void EmptyObserver_1__cctor_m4237726869_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		EmptyObserver_1_t4073582105 * L_0 = (EmptyObserver_1_t4073582105 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		((  void (*) (EmptyObserver_1_t4073582105 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		((EmptyObserver_1_t4073582105_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->static_fields)->set_Instance_0(L_0);
		return;
	}
}
// System.Void UniRx.InternalUtil.EmptyObserver`1<UniRx.Tuple`2<System.Object,System.Object>>::OnCompleted()
extern "C"  void EmptyObserver_1_OnCompleted_m804895650_gshared (EmptyObserver_1_t4073582105 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void UniRx.InternalUtil.EmptyObserver`1<UniRx.Tuple`2<System.Object,System.Object>>::OnError(System.Exception)
extern "C"  void EmptyObserver_1_OnError_m3285060047_gshared (EmptyObserver_1_t4073582105 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void UniRx.InternalUtil.EmptyObserver`1<UniRx.Tuple`2<System.Object,System.Object>>::OnNext(T)
extern "C"  void EmptyObserver_1_OnNext_m3116330688_gshared (EmptyObserver_1_t4073582105 * __this, Tuple_2_t369261819  ___value0, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void UniRx.InternalUtil.EmptyObserver`1<UniRx.Tuple`2<System.Object,UnityEngine.NetworkMessageInfo>>::.ctor()
extern "C"  void EmptyObserver_1__ctor_m2926995122_gshared (EmptyObserver_1_t1515853273 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UniRx.InternalUtil.EmptyObserver`1<UniRx.Tuple`2<System.Object,UnityEngine.NetworkMessageInfo>>::.cctor()
extern "C"  void EmptyObserver_1__cctor_m60439355_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		EmptyObserver_1_t1515853273 * L_0 = (EmptyObserver_1_t1515853273 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		((  void (*) (EmptyObserver_1_t1515853273 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		((EmptyObserver_1_t1515853273_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->static_fields)->set_Instance_0(L_0);
		return;
	}
}
// System.Void UniRx.InternalUtil.EmptyObserver`1<UniRx.Tuple`2<System.Object,UnityEngine.NetworkMessageInfo>>::OnCompleted()
extern "C"  void EmptyObserver_1_OnCompleted_m1922190524_gshared (EmptyObserver_1_t1515853273 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void UniRx.InternalUtil.EmptyObserver`1<UniRx.Tuple`2<System.Object,UnityEngine.NetworkMessageInfo>>::OnError(System.Exception)
extern "C"  void EmptyObserver_1_OnError_m1382269929_gshared (EmptyObserver_1_t1515853273 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void UniRx.InternalUtil.EmptyObserver`1<UniRx.Tuple`2<System.Object,UnityEngine.NetworkMessageInfo>>::OnNext(T)
extern "C"  void EmptyObserver_1_OnNext_m2469436634_gshared (EmptyObserver_1_t1515853273 * __this, Tuple_2_t2106500283  ___value0, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void UniRx.InternalUtil.EmptyObserver`1<UniRx.Unit>::.ctor()
extern "C"  void EmptyObserver_1__ctor_m1910795276_gshared (EmptyObserver_1_t1967639028 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UniRx.InternalUtil.EmptyObserver`1<UniRx.Unit>::.cctor()
extern "C"  void EmptyObserver_1__cctor_m2917982497_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		EmptyObserver_1_t1967639028 * L_0 = (EmptyObserver_1_t1967639028 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		((  void (*) (EmptyObserver_1_t1967639028 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		((EmptyObserver_1_t1967639028_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->static_fields)->set_Instance_0(L_0);
		return;
	}
}
// System.Void UniRx.InternalUtil.EmptyObserver`1<UniRx.Unit>::OnCompleted()
extern "C"  void EmptyObserver_1_OnCompleted_m2207868566_gshared (EmptyObserver_1_t1967639028 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void UniRx.InternalUtil.EmptyObserver`1<UniRx.Unit>::OnError(System.Exception)
extern "C"  void EmptyObserver_1_OnError_m4162905795_gshared (EmptyObserver_1_t1967639028 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void UniRx.InternalUtil.EmptyObserver`1<UniRx.Unit>::OnNext(T)
extern "C"  void EmptyObserver_1_OnNext_m858960820_gshared (EmptyObserver_1_t1967639028 * __this, Unit_t2558286038  ___value0, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void UniRx.InternalUtil.EmptyObserver`1<UnityEngine.Bounds>::.ctor()
extern "C"  void EmptyObserver_1__ctor_m1891988060_gshared (EmptyObserver_1_t2927867968 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UniRx.InternalUtil.EmptyObserver`1<UnityEngine.Bounds>::.cctor()
extern "C"  void EmptyObserver_1__cctor_m2334958801_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		EmptyObserver_1_t2927867968 * L_0 = (EmptyObserver_1_t2927867968 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		((  void (*) (EmptyObserver_1_t2927867968 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		((EmptyObserver_1_t2927867968_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->static_fields)->set_Instance_0(L_0);
		return;
	}
}
// System.Void UniRx.InternalUtil.EmptyObserver`1<UnityEngine.Bounds>::OnCompleted()
extern "C"  void EmptyObserver_1_OnCompleted_m51409126_gshared (EmptyObserver_1_t2927867968 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void UniRx.InternalUtil.EmptyObserver`1<UnityEngine.Bounds>::OnError(System.Exception)
extern "C"  void EmptyObserver_1_OnError_m2756376851_gshared (EmptyObserver_1_t2927867968 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void UniRx.InternalUtil.EmptyObserver`1<UnityEngine.Bounds>::OnNext(T)
extern "C"  void EmptyObserver_1_OnNext_m4260062724_gshared (EmptyObserver_1_t2927867968 * __this, Bounds_t3518514978  ___value0, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void UniRx.InternalUtil.EmptyObserver`1<UnityEngine.Color>::.ctor()
extern "C"  void EmptyObserver_1__ctor_m2117782918_gshared (EmptyObserver_1_t997528750 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UniRx.InternalUtil.EmptyObserver`1<UnityEngine.Color>::.cctor()
extern "C"  void EmptyObserver_1__cctor_m744664807_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		EmptyObserver_1_t997528750 * L_0 = (EmptyObserver_1_t997528750 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		((  void (*) (EmptyObserver_1_t997528750 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		((EmptyObserver_1_t997528750_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->static_fields)->set_Instance_0(L_0);
		return;
	}
}
// System.Void UniRx.InternalUtil.EmptyObserver`1<UnityEngine.Color>::OnCompleted()
extern "C"  void EmptyObserver_1_OnCompleted_m3919266960_gshared (EmptyObserver_1_t997528750 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void UniRx.InternalUtil.EmptyObserver`1<UnityEngine.Color>::OnError(System.Exception)
extern "C"  void EmptyObserver_1_OnError_m1832099261_gshared (EmptyObserver_1_t997528750 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void UniRx.InternalUtil.EmptyObserver`1<UnityEngine.Color>::OnNext(T)
extern "C"  void EmptyObserver_1_OnNext_m2205589166_gshared (EmptyObserver_1_t997528750 * __this, Color_t1588175760  ___value0, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void UniRx.InternalUtil.EmptyObserver`1<UnityEngine.MasterServerEvent>::.ctor()
extern "C"  void EmptyObserver_1__ctor_m1797483512_gshared (EmptyObserver_1_t1687160480 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UniRx.InternalUtil.EmptyObserver`1<UnityEngine.MasterServerEvent>::.cctor()
extern "C"  void EmptyObserver_1__cctor_m3700285109_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		EmptyObserver_1_t1687160480 * L_0 = (EmptyObserver_1_t1687160480 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		((  void (*) (EmptyObserver_1_t1687160480 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		((EmptyObserver_1_t1687160480_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->static_fields)->set_Instance_0(L_0);
		return;
	}
}
// System.Void UniRx.InternalUtil.EmptyObserver`1<UnityEngine.MasterServerEvent>::OnCompleted()
extern "C"  void EmptyObserver_1_OnCompleted_m798737794_gshared (EmptyObserver_1_t1687160480 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void UniRx.InternalUtil.EmptyObserver`1<UnityEngine.MasterServerEvent>::OnError(System.Exception)
extern "C"  void EmptyObserver_1_OnError_m2924495791_gshared (EmptyObserver_1_t1687160480 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void UniRx.InternalUtil.EmptyObserver`1<UnityEngine.MasterServerEvent>::OnNext(T)
extern "C"  void EmptyObserver_1_OnNext_m3635505312_gshared (EmptyObserver_1_t1687160480 * __this, int32_t ___value0, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void UniRx.InternalUtil.EmptyObserver`1<UnityEngine.NetworkConnectionError>::.ctor()
extern "C"  void EmptyObserver_1__ctor_m2403898531_gshared (EmptyObserver_1_t433158983 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UniRx.InternalUtil.EmptyObserver`1<UnityEngine.NetworkConnectionError>::.cctor()
extern "C"  void EmptyObserver_1__cctor_m1024314218_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		EmptyObserver_1_t433158983 * L_0 = (EmptyObserver_1_t433158983 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		((  void (*) (EmptyObserver_1_t433158983 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		((EmptyObserver_1_t433158983_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->static_fields)->set_Instance_0(L_0);
		return;
	}
}
// System.Void UniRx.InternalUtil.EmptyObserver`1<UnityEngine.NetworkConnectionError>::OnCompleted()
extern "C"  void EmptyObserver_1_OnCompleted_m856457709_gshared (EmptyObserver_1_t433158983 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void UniRx.InternalUtil.EmptyObserver`1<UnityEngine.NetworkConnectionError>::OnError(System.Exception)
extern "C"  void EmptyObserver_1_OnError_m4081760154_gshared (EmptyObserver_1_t433158983 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void UniRx.InternalUtil.EmptyObserver`1<UnityEngine.NetworkConnectionError>::OnNext(T)
extern "C"  void EmptyObserver_1_OnNext_m2284786315_gshared (EmptyObserver_1_t433158983 * __this, int32_t ___value0, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void UniRx.InternalUtil.EmptyObserver`1<UnityEngine.NetworkDisconnection>::.ctor()
extern "C"  void EmptyObserver_1__ctor_m2482815813_gshared (EmptyObserver_1_t4043080681 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UniRx.InternalUtil.EmptyObserver`1<UnityEngine.NetworkDisconnection>::.cctor()
extern "C"  void EmptyObserver_1__cctor_m3470749960_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		EmptyObserver_1_t4043080681 * L_0 = (EmptyObserver_1_t4043080681 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		((  void (*) (EmptyObserver_1_t4043080681 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		((EmptyObserver_1_t4043080681_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->static_fields)->set_Instance_0(L_0);
		return;
	}
}
// System.Void UniRx.InternalUtil.EmptyObserver`1<UnityEngine.NetworkDisconnection>::OnCompleted()
extern "C"  void EmptyObserver_1_OnCompleted_m3105337103_gshared (EmptyObserver_1_t4043080681 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void UniRx.InternalUtil.EmptyObserver`1<UnityEngine.NetworkDisconnection>::OnError(System.Exception)
extern "C"  void EmptyObserver_1_OnError_m1986085308_gshared (EmptyObserver_1_t4043080681 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void UniRx.InternalUtil.EmptyObserver`1<UnityEngine.NetworkDisconnection>::OnNext(T)
extern "C"  void EmptyObserver_1_OnNext_m814882989_gshared (EmptyObserver_1_t4043080681 * __this, int32_t ___value0, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void UniRx.InternalUtil.EmptyObserver`1<UnityEngine.NetworkMessageInfo>::.ctor()
extern "C"  void EmptyObserver_1__ctor_m3647941742_gshared (EmptyObserver_1_t1983697874 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UniRx.InternalUtil.EmptyObserver`1<UnityEngine.NetworkMessageInfo>::.cctor()
extern "C"  void EmptyObserver_1__cctor_m934948095_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		EmptyObserver_1_t1983697874 * L_0 = (EmptyObserver_1_t1983697874 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		((  void (*) (EmptyObserver_1_t1983697874 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		((EmptyObserver_1_t1983697874_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->static_fields)->set_Instance_0(L_0);
		return;
	}
}
// System.Void UniRx.InternalUtil.EmptyObserver`1<UnityEngine.NetworkMessageInfo>::OnCompleted()
extern "C"  void EmptyObserver_1_OnCompleted_m2285294968_gshared (EmptyObserver_1_t1983697874 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void UniRx.InternalUtil.EmptyObserver`1<UnityEngine.NetworkMessageInfo>::OnError(System.Exception)
extern "C"  void EmptyObserver_1_OnError_m3585413797_gshared (EmptyObserver_1_t1983697874 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void UniRx.InternalUtil.EmptyObserver`1<UnityEngine.NetworkMessageInfo>::OnNext(T)
extern "C"  void EmptyObserver_1_OnNext_m3809403798_gshared (EmptyObserver_1_t1983697874 * __this, NetworkMessageInfo_t2574344884  ___value0, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void UniRx.InternalUtil.EmptyObserver`1<UnityEngine.NetworkPlayer>::.ctor()
extern "C"  void EmptyObserver_1__ctor_m3777574866_gshared (EmptyObserver_1_t690490362 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UniRx.InternalUtil.EmptyObserver`1<UnityEngine.NetworkPlayer>::.cctor()
extern "C"  void EmptyObserver_1__cctor_m658607643_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		EmptyObserver_1_t690490362 * L_0 = (EmptyObserver_1_t690490362 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		((  void (*) (EmptyObserver_1_t690490362 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		((EmptyObserver_1_t690490362_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->static_fields)->set_Instance_0(L_0);
		return;
	}
}
// System.Void UniRx.InternalUtil.EmptyObserver`1<UnityEngine.NetworkPlayer>::OnCompleted()
extern "C"  void EmptyObserver_1_OnCompleted_m3941320156_gshared (EmptyObserver_1_t690490362 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void UniRx.InternalUtil.EmptyObserver`1<UnityEngine.NetworkPlayer>::OnError(System.Exception)
extern "C"  void EmptyObserver_1_OnError_m3417345289_gshared (EmptyObserver_1_t690490362 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void UniRx.InternalUtil.EmptyObserver`1<UnityEngine.NetworkPlayer>::OnNext(T)
extern "C"  void EmptyObserver_1_OnNext_m3832784378_gshared (EmptyObserver_1_t690490362 * __this, NetworkPlayer_t1281137372  ___value0, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void UniRx.InternalUtil.EmptyObserver`1<UnityEngine.Quaternion>::.ctor()
extern "C"  void EmptyObserver_1__ctor_m793319045_gshared (EmptyObserver_1_t1301068969 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UniRx.InternalUtil.EmptyObserver`1<UnityEngine.Quaternion>::.cctor()
extern "C"  void EmptyObserver_1__cctor_m2635957704_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		EmptyObserver_1_t1301068969 * L_0 = (EmptyObserver_1_t1301068969 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		((  void (*) (EmptyObserver_1_t1301068969 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		((EmptyObserver_1_t1301068969_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->static_fields)->set_Instance_0(L_0);
		return;
	}
}
// System.Void UniRx.InternalUtil.EmptyObserver`1<UnityEngine.Quaternion>::OnCompleted()
extern "C"  void EmptyObserver_1_OnCompleted_m2317914703_gshared (EmptyObserver_1_t1301068969 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void UniRx.InternalUtil.EmptyObserver`1<UnityEngine.Quaternion>::OnError(System.Exception)
extern "C"  void EmptyObserver_1_OnError_m4277617916_gshared (EmptyObserver_1_t1301068969 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void UniRx.InternalUtil.EmptyObserver`1<UnityEngine.Quaternion>::OnNext(T)
extern "C"  void EmptyObserver_1_OnNext_m706126829_gshared (EmptyObserver_1_t1301068969 * __this, Quaternion_t1891715979  ___value0, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void UniRx.InternalUtil.EmptyObserver`1<UnityEngine.Rect>::.ctor()
extern "C"  void EmptyObserver_1__ctor_m392656715_gshared (EmptyObserver_1_t934781807 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UniRx.InternalUtil.EmptyObserver`1<UnityEngine.Rect>::.cctor()
extern "C"  void EmptyObserver_1__cctor_m3100327362_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		EmptyObserver_1_t934781807 * L_0 = (EmptyObserver_1_t934781807 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		((  void (*) (EmptyObserver_1_t934781807 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		((EmptyObserver_1_t934781807_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->static_fields)->set_Instance_0(L_0);
		return;
	}
}
// System.Void UniRx.InternalUtil.EmptyObserver`1<UnityEngine.Rect>::OnCompleted()
extern "C"  void EmptyObserver_1_OnCompleted_m2752562837_gshared (EmptyObserver_1_t934781807 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void UniRx.InternalUtil.EmptyObserver`1<UnityEngine.Rect>::OnError(System.Exception)
extern "C"  void EmptyObserver_1_OnError_m1376075842_gshared (EmptyObserver_1_t934781807 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void UniRx.InternalUtil.EmptyObserver`1<UnityEngine.Rect>::OnNext(T)
extern "C"  void EmptyObserver_1_OnNext_m2216684339_gshared (EmptyObserver_1_t934781807 * __this, Rect_t1525428817  ___value0, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void UniRx.InternalUtil.EmptyObserver`1<UnityEngine.Vector2>::.ctor()
extern "C"  void EmptyObserver_1__ctor_m4224020210_gshared (EmptyObserver_1_t2934682778 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UniRx.InternalUtil.EmptyObserver`1<UnityEngine.Vector2>::.cctor()
extern "C"  void EmptyObserver_1__cctor_m1613511419_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		EmptyObserver_1_t2934682778 * L_0 = (EmptyObserver_1_t2934682778 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		((  void (*) (EmptyObserver_1_t2934682778 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		((EmptyObserver_1_t2934682778_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->static_fields)->set_Instance_0(L_0);
		return;
	}
}
// System.Void UniRx.InternalUtil.EmptyObserver`1<UnityEngine.Vector2>::OnCompleted()
extern "C"  void EmptyObserver_1_OnCompleted_m3020563708_gshared (EmptyObserver_1_t2934682778 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void UniRx.InternalUtil.EmptyObserver`1<UnityEngine.Vector2>::OnError(System.Exception)
extern "C"  void EmptyObserver_1_OnError_m1201224745_gshared (EmptyObserver_1_t2934682778 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void UniRx.InternalUtil.EmptyObserver`1<UnityEngine.Vector2>::OnNext(T)
extern "C"  void EmptyObserver_1_OnNext_m3370030362_gshared (EmptyObserver_1_t2934682778 * __this, Vector2_t3525329788  ___value0, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void UniRx.InternalUtil.EmptyObserver`1<UnityEngine.Vector3>::.ctor()
extern "C"  void EmptyObserver_1__ctor_m2427068851_gshared (EmptyObserver_1_t2934682779 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UniRx.InternalUtil.EmptyObserver`1<UnityEngine.Vector3>::.cctor()
extern "C"  void EmptyObserver_1__cctor_m1742594138_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		EmptyObserver_1_t2934682779 * L_0 = (EmptyObserver_1_t2934682779 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		((  void (*) (EmptyObserver_1_t2934682779 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		((EmptyObserver_1_t2934682779_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->static_fields)->set_Instance_0(L_0);
		return;
	}
}
// System.Void UniRx.InternalUtil.EmptyObserver`1<UnityEngine.Vector3>::OnCompleted()
extern "C"  void EmptyObserver_1_OnCompleted_m78906109_gshared (EmptyObserver_1_t2934682779 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void UniRx.InternalUtil.EmptyObserver`1<UnityEngine.Vector3>::OnError(System.Exception)
extern "C"  void EmptyObserver_1_OnError_m1894326442_gshared (EmptyObserver_1_t2934682779 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void UniRx.InternalUtil.EmptyObserver`1<UnityEngine.Vector3>::OnNext(T)
extern "C"  void EmptyObserver_1_OnNext_m3076627355_gshared (EmptyObserver_1_t2934682779 * __this, Vector3_t3525329789  ___value0, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void UniRx.InternalUtil.EmptyObserver`1<UnityEngine.Vector4>::.ctor()
extern "C"  void EmptyObserver_1__ctor_m630117492_gshared (EmptyObserver_1_t2934682780 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UniRx.InternalUtil.EmptyObserver`1<UnityEngine.Vector4>::.cctor()
extern "C"  void EmptyObserver_1__cctor_m1871676857_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		EmptyObserver_1_t2934682780 * L_0 = (EmptyObserver_1_t2934682780 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		((  void (*) (EmptyObserver_1_t2934682780 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		((EmptyObserver_1_t2934682780_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->static_fields)->set_Instance_0(L_0);
		return;
	}
}
// System.Void UniRx.InternalUtil.EmptyObserver`1<UnityEngine.Vector4>::OnCompleted()
extern "C"  void EmptyObserver_1_OnCompleted_m1432215806_gshared (EmptyObserver_1_t2934682780 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void UniRx.InternalUtil.EmptyObserver`1<UnityEngine.Vector4>::OnError(System.Exception)
extern "C"  void EmptyObserver_1_OnError_m2587428139_gshared (EmptyObserver_1_t2934682780 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void UniRx.InternalUtil.EmptyObserver`1<UnityEngine.Vector4>::OnNext(T)
extern "C"  void EmptyObserver_1_OnNext_m2783224348_gshared (EmptyObserver_1_t2934682780 * __this, Vector4_t3525329790  ___value0, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void UniRx.InternalUtil.ImmutableList`1<System.Object>::.ctor()
extern "C"  void ImmutableList_1__ctor_m2467204245_gshared (ImmutableList_1_t1897310919 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		__this->set_data_1(((ObjectU5BU5D_t11523773*)SZArrayNew(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0), (uint32_t)0)));
		return;
	}
}
// System.Void UniRx.InternalUtil.ImmutableList`1<System.Object>::.ctor(T[])
extern "C"  void ImmutableList_1__ctor_m707697735_gshared (ImmutableList_1_t1897310919 * __this, ObjectU5BU5D_t11523773* ___data0, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		ObjectU5BU5D_t11523773* L_0 = ___data0;
		__this->set_data_1(L_0);
		return;
	}
}
// System.Void UniRx.InternalUtil.ImmutableList`1<System.Object>::.cctor()
extern "C"  void ImmutableList_1__cctor_m2986791352_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		ImmutableList_1_t1897310919 * L_0 = (ImmutableList_1_t1897310919 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		((  void (*) (ImmutableList_1_t1897310919 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		((ImmutableList_1_t1897310919_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->static_fields)->set_Empty_0(L_0);
		return;
	}
}
// T[] UniRx.InternalUtil.ImmutableList`1<System.Object>::get_Data()
extern "C"  ObjectU5BU5D_t11523773* ImmutableList_1_get_Data_m1676800965_gshared (ImmutableList_1_t1897310919 * __this, const MethodInfo* method)
{
	{
		ObjectU5BU5D_t11523773* L_0 = (ObjectU5BU5D_t11523773*)__this->get_data_1();
		return L_0;
	}
}
// UniRx.InternalUtil.ImmutableList`1<T> UniRx.InternalUtil.ImmutableList`1<System.Object>::Add(T)
extern "C"  ImmutableList_1_t1897310919 * ImmutableList_1_Add_m948892931_gshared (ImmutableList_1_t1897310919 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	ObjectU5BU5D_t11523773* V_0 = NULL;
	{
		ObjectU5BU5D_t11523773* L_0 = (ObjectU5BU5D_t11523773*)__this->get_data_1();
		NullCheck(L_0);
		V_0 = (ObjectU5BU5D_t11523773*)((ObjectU5BU5D_t11523773*)SZArrayNew(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0), (uint32_t)((int32_t)((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_0)->max_length))))+(int32_t)1))));
		ObjectU5BU5D_t11523773* L_1 = (ObjectU5BU5D_t11523773*)__this->get_data_1();
		ObjectU5BU5D_t11523773* L_2 = V_0;
		ObjectU5BU5D_t11523773* L_3 = (ObjectU5BU5D_t11523773*)__this->get_data_1();
		NullCheck(L_3);
		Array_Copy_m3799309042(NULL /*static, unused*/, (Il2CppArray *)(Il2CppArray *)L_1, (Il2CppArray *)(Il2CppArray *)L_2, (int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_3)->max_length)))), /*hidden argument*/NULL);
		ObjectU5BU5D_t11523773* L_4 = V_0;
		ObjectU5BU5D_t11523773* L_5 = (ObjectU5BU5D_t11523773*)__this->get_data_1();
		NullCheck(L_5);
		Il2CppObject * L_6 = ___value0;
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, (((int32_t)((int32_t)(((Il2CppArray *)L_5)->max_length)))));
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>((((int32_t)((int32_t)(((Il2CppArray *)L_5)->max_length))))), (Il2CppObject *)L_6);
		ObjectU5BU5D_t11523773* L_7 = V_0;
		ImmutableList_1_t1897310919 * L_8 = (ImmutableList_1_t1897310919 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		((  void (*) (ImmutableList_1_t1897310919 *, ObjectU5BU5D_t11523773*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)(L_8, (ObjectU5BU5D_t11523773*)L_7, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		return L_8;
	}
}
// UniRx.InternalUtil.ImmutableList`1<T> UniRx.InternalUtil.ImmutableList`1<System.Object>::Remove(T)
extern "C"  ImmutableList_1_t1897310919 * ImmutableList_1_Remove_m1538917202_gshared (ImmutableList_1_t1897310919 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	ObjectU5BU5D_t11523773* V_2 = NULL;
	{
		Il2CppObject * L_0 = ___value0;
		NullCheck((ImmutableList_1_t1897310919 *)__this);
		int32_t L_1 = ((  int32_t (*) (ImmutableList_1_t1897310919 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->method)((ImmutableList_1_t1897310919 *)__this, (Il2CppObject *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		V_0 = (int32_t)L_1;
		int32_t L_2 = V_0;
		if ((((int32_t)L_2) >= ((int32_t)0)))
		{
			goto IL_0011;
		}
	}
	{
		return __this;
	}

IL_0011:
	{
		ObjectU5BU5D_t11523773* L_3 = (ObjectU5BU5D_t11523773*)__this->get_data_1();
		NullCheck(L_3);
		V_1 = (int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_3)->max_length))));
		int32_t L_4 = V_1;
		if ((!(((uint32_t)L_4) == ((uint32_t)1))))
		{
			goto IL_0027;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		ImmutableList_1_t1897310919 * L_5 = ((ImmutableList_1_t1897310919_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->static_fields)->get_Empty_0();
		return L_5;
	}

IL_0027:
	{
		int32_t L_6 = V_1;
		V_2 = (ObjectU5BU5D_t11523773*)((ObjectU5BU5D_t11523773*)SZArrayNew(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0), (uint32_t)((int32_t)((int32_t)L_6-(int32_t)1))));
		ObjectU5BU5D_t11523773* L_7 = (ObjectU5BU5D_t11523773*)__this->get_data_1();
		ObjectU5BU5D_t11523773* L_8 = V_2;
		int32_t L_9 = V_0;
		Array_Copy_m2598616668(NULL /*static, unused*/, (Il2CppArray *)(Il2CppArray *)L_7, (int32_t)0, (Il2CppArray *)(Il2CppArray *)L_8, (int32_t)0, (int32_t)L_9, /*hidden argument*/NULL);
		ObjectU5BU5D_t11523773* L_10 = (ObjectU5BU5D_t11523773*)__this->get_data_1();
		int32_t L_11 = V_0;
		ObjectU5BU5D_t11523773* L_12 = V_2;
		int32_t L_13 = V_0;
		int32_t L_14 = V_1;
		int32_t L_15 = V_0;
		Array_Copy_m2598616668(NULL /*static, unused*/, (Il2CppArray *)(Il2CppArray *)L_10, (int32_t)((int32_t)((int32_t)L_11+(int32_t)1)), (Il2CppArray *)(Il2CppArray *)L_12, (int32_t)L_13, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_14-(int32_t)L_15))-(int32_t)1)), /*hidden argument*/NULL);
		ObjectU5BU5D_t11523773* L_16 = V_2;
		ImmutableList_1_t1897310919 * L_17 = (ImmutableList_1_t1897310919 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		((  void (*) (ImmutableList_1_t1897310919 *, ObjectU5BU5D_t11523773*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)(L_17, (ObjectU5BU5D_t11523773*)L_16, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		return L_17;
	}
}
// System.Int32 UniRx.InternalUtil.ImmutableList`1<System.Object>::IndexOf(T)
extern "C"  int32_t ImmutableList_1_IndexOf_m3149323628_gshared (ImmutableList_1_t1897310919 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		V_0 = (int32_t)0;
		goto IL_002e;
	}

IL_0007:
	{
		ObjectU5BU5D_t11523773* L_0 = (ObjectU5BU5D_t11523773*)__this->get_data_1();
		int32_t L_1 = V_0;
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, L_1);
		int32_t L_2 = L_1;
		Il2CppObject * L_3 = ___value0;
		bool L_4 = Object_Equals_m3175838359(NULL /*static, unused*/, (Il2CppObject *)((L_0)->GetAt(static_cast<il2cpp_array_size_t>(L_2))), (Il2CppObject *)L_3, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_002a;
		}
	}
	{
		int32_t L_5 = V_0;
		return L_5;
	}

IL_002a:
	{
		int32_t L_6 = V_0;
		V_0 = (int32_t)((int32_t)((int32_t)L_6+(int32_t)1));
	}

IL_002e:
	{
		int32_t L_7 = V_0;
		ObjectU5BU5D_t11523773* L_8 = (ObjectU5BU5D_t11523773*)__this->get_data_1();
		NullCheck(L_8);
		if ((((int32_t)L_7) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_8)->max_length)))))))
		{
			goto IL_0007;
		}
	}
	{
		return (-1);
	}
}
// System.Void UniRx.InternalUtil.ListObserver`1<System.Boolean>::.ctor(UniRx.InternalUtil.ImmutableList`1<UniRx.IObserver`1<T>>)
extern "C"  void ListObserver_1__ctor_m1222637140_gshared (ListObserver_1_t505623012 * __this, ImmutableList_1_t3483208743 * ___observers0, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		ImmutableList_1_t3483208743 * L_0 = ___observers0;
		__this->set__observers_0(L_0);
		return;
	}
}
// System.Void UniRx.InternalUtil.ListObserver`1<System.Boolean>::OnCompleted()
extern "C"  void ListObserver_1_OnCompleted_m928423308_gshared (ListObserver_1_t505623012 * __this, const MethodInfo* method)
{
	IObserver_1U5BU5D_t3497092061* V_0 = NULL;
	int32_t V_1 = 0;
	{
		ImmutableList_1_t3483208743 * L_0 = (ImmutableList_1_t3483208743 *)__this->get__observers_0();
		NullCheck((ImmutableList_1_t3483208743 *)L_0);
		IObserver_1U5BU5D_t3497092061* L_1 = ((  IObserver_1U5BU5D_t3497092061* (*) (ImmutableList_1_t3483208743 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((ImmutableList_1_t3483208743 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		V_0 = (IObserver_1U5BU5D_t3497092061*)L_1;
		V_1 = (int32_t)0;
		goto IL_001f;
	}

IL_0013:
	{
		IObserver_1U5BU5D_t3497092061* L_2 = V_0;
		int32_t L_3 = V_1;
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, L_3);
		int32_t L_4 = L_3;
		NullCheck((Il2CppObject*)((L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_4))));
		InterfaceActionInvoker0::Invoke(0 /* System.Void UniRx.IObserver`1<System.Boolean>::OnCompleted() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), (Il2CppObject*)((L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_4))));
		int32_t L_5 = V_1;
		V_1 = (int32_t)((int32_t)((int32_t)L_5+(int32_t)1));
	}

IL_001f:
	{
		int32_t L_6 = V_1;
		IObserver_1U5BU5D_t3497092061* L_7 = V_0;
		NullCheck(L_7);
		if ((((int32_t)L_6) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_7)->max_length)))))))
		{
			goto IL_0013;
		}
	}
	{
		return;
	}
}
// System.Void UniRx.InternalUtil.ListObserver`1<System.Boolean>::OnError(System.Exception)
extern "C"  void ListObserver_1_OnError_m3450166457_gshared (ListObserver_1_t505623012 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method)
{
	IObserver_1U5BU5D_t3497092061* V_0 = NULL;
	int32_t V_1 = 0;
	{
		ImmutableList_1_t3483208743 * L_0 = (ImmutableList_1_t3483208743 *)__this->get__observers_0();
		NullCheck((ImmutableList_1_t3483208743 *)L_0);
		IObserver_1U5BU5D_t3497092061* L_1 = ((  IObserver_1U5BU5D_t3497092061* (*) (ImmutableList_1_t3483208743 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((ImmutableList_1_t3483208743 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		V_0 = (IObserver_1U5BU5D_t3497092061*)L_1;
		V_1 = (int32_t)0;
		goto IL_0020;
	}

IL_0013:
	{
		IObserver_1U5BU5D_t3497092061* L_2 = V_0;
		int32_t L_3 = V_1;
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, L_3);
		int32_t L_4 = L_3;
		Exception_t1967233988 * L_5 = ___error0;
		NullCheck((Il2CppObject*)((L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_4))));
		InterfaceActionInvoker1< Exception_t1967233988 * >::Invoke(1 /* System.Void UniRx.IObserver`1<System.Boolean>::OnError(System.Exception) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), (Il2CppObject*)((L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_4))), (Exception_t1967233988 *)L_5);
		int32_t L_6 = V_1;
		V_1 = (int32_t)((int32_t)((int32_t)L_6+(int32_t)1));
	}

IL_0020:
	{
		int32_t L_7 = V_1;
		IObserver_1U5BU5D_t3497092061* L_8 = V_0;
		NullCheck(L_8);
		if ((((int32_t)L_7) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_8)->max_length)))))))
		{
			goto IL_0013;
		}
	}
	{
		return;
	}
}
// System.Void UniRx.InternalUtil.ListObserver`1<System.Boolean>::OnNext(T)
extern "C"  void ListObserver_1_OnNext_m1440601514_gshared (ListObserver_1_t505623012 * __this, bool ___value0, const MethodInfo* method)
{
	IObserver_1U5BU5D_t3497092061* V_0 = NULL;
	int32_t V_1 = 0;
	{
		ImmutableList_1_t3483208743 * L_0 = (ImmutableList_1_t3483208743 *)__this->get__observers_0();
		NullCheck((ImmutableList_1_t3483208743 *)L_0);
		IObserver_1U5BU5D_t3497092061* L_1 = ((  IObserver_1U5BU5D_t3497092061* (*) (ImmutableList_1_t3483208743 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((ImmutableList_1_t3483208743 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		V_0 = (IObserver_1U5BU5D_t3497092061*)L_1;
		V_1 = (int32_t)0;
		goto IL_0020;
	}

IL_0013:
	{
		IObserver_1U5BU5D_t3497092061* L_2 = V_0;
		int32_t L_3 = V_1;
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, L_3);
		int32_t L_4 = L_3;
		bool L_5 = ___value0;
		NullCheck((Il2CppObject*)((L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_4))));
		InterfaceActionInvoker1< bool >::Invoke(2 /* System.Void UniRx.IObserver`1<System.Boolean>::OnNext(T) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), (Il2CppObject*)((L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_4))), (bool)L_5);
		int32_t L_6 = V_1;
		V_1 = (int32_t)((int32_t)((int32_t)L_6+(int32_t)1));
	}

IL_0020:
	{
		int32_t L_7 = V_1;
		IObserver_1U5BU5D_t3497092061* L_8 = V_0;
		NullCheck(L_8);
		if ((((int32_t)L_7) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_8)->max_length)))))))
		{
			goto IL_0013;
		}
	}
	{
		return;
	}
}
// UniRx.IObserver`1<T> UniRx.InternalUtil.ListObserver`1<System.Boolean>::Add(UniRx.IObserver`1<T>)
extern "C"  Il2CppObject* ListObserver_1_Add_m3730409674_gshared (ListObserver_1_t505623012 * __this, Il2CppObject* ___observer0, const MethodInfo* method)
{
	{
		ImmutableList_1_t3483208743 * L_0 = (ImmutableList_1_t3483208743 *)__this->get__observers_0();
		Il2CppObject* L_1 = ___observer0;
		NullCheck((ImmutableList_1_t3483208743 *)L_0);
		ImmutableList_1_t3483208743 * L_2 = ((  ImmutableList_1_t3483208743 * (*) (ImmutableList_1_t3483208743 *, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)((ImmutableList_1_t3483208743 *)L_0, (Il2CppObject*)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		ListObserver_1_t505623012 * L_3 = (ListObserver_1_t505623012 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		((  void (*) (ListObserver_1_t505623012 *, ImmutableList_1_t3483208743 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)(L_3, (ImmutableList_1_t3483208743 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		return L_3;
	}
}
// UniRx.IObserver`1<T> UniRx.InternalUtil.ListObserver`1<System.Boolean>::Remove(UniRx.IObserver`1<T>)
extern "C"  Il2CppObject* ListObserver_1_Remove_m789259841_gshared (ListObserver_1_t505623012 * __this, Il2CppObject* ___observer0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		ImmutableList_1_t3483208743 * L_0 = (ImmutableList_1_t3483208743 *)__this->get__observers_0();
		NullCheck((ImmutableList_1_t3483208743 *)L_0);
		IObserver_1U5BU5D_t3497092061* L_1 = ((  IObserver_1U5BU5D_t3497092061* (*) (ImmutableList_1_t3483208743 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((ImmutableList_1_t3483208743 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		Il2CppObject* L_2 = ___observer0;
		int32_t L_3 = ((  int32_t (*) (Il2CppObject * /* static, unused */, IObserver_1U5BU5D_t3497092061*, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->method)(NULL /*static, unused*/, (IObserver_1U5BU5D_t3497092061*)L_1, (Il2CppObject*)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		V_0 = (int32_t)L_3;
		int32_t L_4 = V_0;
		if ((((int32_t)L_4) >= ((int32_t)0)))
		{
			goto IL_001b;
		}
	}
	{
		return __this;
	}

IL_001b:
	{
		ImmutableList_1_t3483208743 * L_5 = (ImmutableList_1_t3483208743 *)__this->get__observers_0();
		NullCheck((ImmutableList_1_t3483208743 *)L_5);
		IObserver_1U5BU5D_t3497092061* L_6 = ((  IObserver_1U5BU5D_t3497092061* (*) (ImmutableList_1_t3483208743 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((ImmutableList_1_t3483208743 *)L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		NullCheck(L_6);
		if ((!(((uint32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_6)->max_length))))) == ((uint32_t)2))))
		{
			goto IL_003e;
		}
	}
	{
		ImmutableList_1_t3483208743 * L_7 = (ImmutableList_1_t3483208743 *)__this->get__observers_0();
		NullCheck((ImmutableList_1_t3483208743 *)L_7);
		IObserver_1U5BU5D_t3497092061* L_8 = ((  IObserver_1U5BU5D_t3497092061* (*) (ImmutableList_1_t3483208743 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((ImmutableList_1_t3483208743 *)L_7, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_9 = V_0;
		NullCheck(L_8);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_8, ((int32_t)((int32_t)1-(int32_t)L_9)));
		int32_t L_10 = ((int32_t)((int32_t)1-(int32_t)L_9));
		return ((L_8)->GetAt(static_cast<il2cpp_array_size_t>(L_10)));
	}

IL_003e:
	{
		ImmutableList_1_t3483208743 * L_11 = (ImmutableList_1_t3483208743 *)__this->get__observers_0();
		Il2CppObject* L_12 = ___observer0;
		NullCheck((ImmutableList_1_t3483208743 *)L_11);
		ImmutableList_1_t3483208743 * L_13 = ((  ImmutableList_1_t3483208743 * (*) (ImmutableList_1_t3483208743 *, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6)->method)((ImmutableList_1_t3483208743 *)L_11, (Il2CppObject*)L_12, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		ListObserver_1_t505623012 * L_14 = (ListObserver_1_t505623012 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		((  void (*) (ListObserver_1_t505623012 *, ImmutableList_1_t3483208743 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)(L_14, (ImmutableList_1_t3483208743 *)L_13, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		return L_14;
	}
}
// System.Void UniRx.InternalUtil.ListObserver`1<System.Byte>::.ctor(UniRx.InternalUtil.ImmutableList`1<UniRx.IObserver`1<T>>)
extern "C"  void ListObserver_1__ctor_m2972945066_gshared (ListObserver_1_t3073311492 * __this, ImmutableList_1_t1755929927 * ___observers0, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		ImmutableList_1_t1755929927 * L_0 = ___observers0;
		__this->set__observers_0(L_0);
		return;
	}
}
// System.Void UniRx.InternalUtil.ListObserver`1<System.Byte>::OnCompleted()
extern "C"  void ListObserver_1_OnCompleted_m3618018146_gshared (ListObserver_1_t3073311492 * __this, const MethodInfo* method)
{
	IObserver_1U5BU5D_t4045638205* V_0 = NULL;
	int32_t V_1 = 0;
	{
		ImmutableList_1_t1755929927 * L_0 = (ImmutableList_1_t1755929927 *)__this->get__observers_0();
		NullCheck((ImmutableList_1_t1755929927 *)L_0);
		IObserver_1U5BU5D_t4045638205* L_1 = ((  IObserver_1U5BU5D_t4045638205* (*) (ImmutableList_1_t1755929927 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((ImmutableList_1_t1755929927 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		V_0 = (IObserver_1U5BU5D_t4045638205*)L_1;
		V_1 = (int32_t)0;
		goto IL_001f;
	}

IL_0013:
	{
		IObserver_1U5BU5D_t4045638205* L_2 = V_0;
		int32_t L_3 = V_1;
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, L_3);
		int32_t L_4 = L_3;
		NullCheck((Il2CppObject*)((L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_4))));
		InterfaceActionInvoker0::Invoke(0 /* System.Void UniRx.IObserver`1<System.Byte>::OnCompleted() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), (Il2CppObject*)((L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_4))));
		int32_t L_5 = V_1;
		V_1 = (int32_t)((int32_t)((int32_t)L_5+(int32_t)1));
	}

IL_001f:
	{
		int32_t L_6 = V_1;
		IObserver_1U5BU5D_t4045638205* L_7 = V_0;
		NullCheck(L_7);
		if ((((int32_t)L_6) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_7)->max_length)))))))
		{
			goto IL_0013;
		}
	}
	{
		return;
	}
}
// System.Void UniRx.InternalUtil.ListObserver`1<System.Byte>::OnError(System.Exception)
extern "C"  void ListObserver_1_OnError_m909459855_gshared (ListObserver_1_t3073311492 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method)
{
	IObserver_1U5BU5D_t4045638205* V_0 = NULL;
	int32_t V_1 = 0;
	{
		ImmutableList_1_t1755929927 * L_0 = (ImmutableList_1_t1755929927 *)__this->get__observers_0();
		NullCheck((ImmutableList_1_t1755929927 *)L_0);
		IObserver_1U5BU5D_t4045638205* L_1 = ((  IObserver_1U5BU5D_t4045638205* (*) (ImmutableList_1_t1755929927 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((ImmutableList_1_t1755929927 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		V_0 = (IObserver_1U5BU5D_t4045638205*)L_1;
		V_1 = (int32_t)0;
		goto IL_0020;
	}

IL_0013:
	{
		IObserver_1U5BU5D_t4045638205* L_2 = V_0;
		int32_t L_3 = V_1;
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, L_3);
		int32_t L_4 = L_3;
		Exception_t1967233988 * L_5 = ___error0;
		NullCheck((Il2CppObject*)((L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_4))));
		InterfaceActionInvoker1< Exception_t1967233988 * >::Invoke(1 /* System.Void UniRx.IObserver`1<System.Byte>::OnError(System.Exception) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), (Il2CppObject*)((L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_4))), (Exception_t1967233988 *)L_5);
		int32_t L_6 = V_1;
		V_1 = (int32_t)((int32_t)((int32_t)L_6+(int32_t)1));
	}

IL_0020:
	{
		int32_t L_7 = V_1;
		IObserver_1U5BU5D_t4045638205* L_8 = V_0;
		NullCheck(L_8);
		if ((((int32_t)L_7) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_8)->max_length)))))))
		{
			goto IL_0013;
		}
	}
	{
		return;
	}
}
// System.Void UniRx.InternalUtil.ListObserver`1<System.Byte>::OnNext(T)
extern "C"  void ListObserver_1_OnNext_m2601395840_gshared (ListObserver_1_t3073311492 * __this, uint8_t ___value0, const MethodInfo* method)
{
	IObserver_1U5BU5D_t4045638205* V_0 = NULL;
	int32_t V_1 = 0;
	{
		ImmutableList_1_t1755929927 * L_0 = (ImmutableList_1_t1755929927 *)__this->get__observers_0();
		NullCheck((ImmutableList_1_t1755929927 *)L_0);
		IObserver_1U5BU5D_t4045638205* L_1 = ((  IObserver_1U5BU5D_t4045638205* (*) (ImmutableList_1_t1755929927 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((ImmutableList_1_t1755929927 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		V_0 = (IObserver_1U5BU5D_t4045638205*)L_1;
		V_1 = (int32_t)0;
		goto IL_0020;
	}

IL_0013:
	{
		IObserver_1U5BU5D_t4045638205* L_2 = V_0;
		int32_t L_3 = V_1;
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, L_3);
		int32_t L_4 = L_3;
		uint8_t L_5 = ___value0;
		NullCheck((Il2CppObject*)((L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_4))));
		InterfaceActionInvoker1< uint8_t >::Invoke(2 /* System.Void UniRx.IObserver`1<System.Byte>::OnNext(T) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), (Il2CppObject*)((L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_4))), (uint8_t)L_5);
		int32_t L_6 = V_1;
		V_1 = (int32_t)((int32_t)((int32_t)L_6+(int32_t)1));
	}

IL_0020:
	{
		int32_t L_7 = V_1;
		IObserver_1U5BU5D_t4045638205* L_8 = V_0;
		NullCheck(L_8);
		if ((((int32_t)L_7) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_8)->max_length)))))))
		{
			goto IL_0013;
		}
	}
	{
		return;
	}
}
// UniRx.IObserver`1<T> UniRx.InternalUtil.ListObserver`1<System.Byte>::Add(UniRx.IObserver`1<T>)
extern "C"  Il2CppObject* ListObserver_1_Add_m2677204562_gshared (ListObserver_1_t3073311492 * __this, Il2CppObject* ___observer0, const MethodInfo* method)
{
	{
		ImmutableList_1_t1755929927 * L_0 = (ImmutableList_1_t1755929927 *)__this->get__observers_0();
		Il2CppObject* L_1 = ___observer0;
		NullCheck((ImmutableList_1_t1755929927 *)L_0);
		ImmutableList_1_t1755929927 * L_2 = ((  ImmutableList_1_t1755929927 * (*) (ImmutableList_1_t1755929927 *, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)((ImmutableList_1_t1755929927 *)L_0, (Il2CppObject*)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		ListObserver_1_t3073311492 * L_3 = (ListObserver_1_t3073311492 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		((  void (*) (ListObserver_1_t3073311492 *, ImmutableList_1_t1755929927 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)(L_3, (ImmutableList_1_t1755929927 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		return L_3;
	}
}
// UniRx.IObserver`1<T> UniRx.InternalUtil.ListObserver`1<System.Byte>::Remove(UniRx.IObserver`1<T>)
extern "C"  Il2CppObject* ListObserver_1_Remove_m3786832825_gshared (ListObserver_1_t3073311492 * __this, Il2CppObject* ___observer0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		ImmutableList_1_t1755929927 * L_0 = (ImmutableList_1_t1755929927 *)__this->get__observers_0();
		NullCheck((ImmutableList_1_t1755929927 *)L_0);
		IObserver_1U5BU5D_t4045638205* L_1 = ((  IObserver_1U5BU5D_t4045638205* (*) (ImmutableList_1_t1755929927 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((ImmutableList_1_t1755929927 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		Il2CppObject* L_2 = ___observer0;
		int32_t L_3 = ((  int32_t (*) (Il2CppObject * /* static, unused */, IObserver_1U5BU5D_t4045638205*, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->method)(NULL /*static, unused*/, (IObserver_1U5BU5D_t4045638205*)L_1, (Il2CppObject*)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		V_0 = (int32_t)L_3;
		int32_t L_4 = V_0;
		if ((((int32_t)L_4) >= ((int32_t)0)))
		{
			goto IL_001b;
		}
	}
	{
		return __this;
	}

IL_001b:
	{
		ImmutableList_1_t1755929927 * L_5 = (ImmutableList_1_t1755929927 *)__this->get__observers_0();
		NullCheck((ImmutableList_1_t1755929927 *)L_5);
		IObserver_1U5BU5D_t4045638205* L_6 = ((  IObserver_1U5BU5D_t4045638205* (*) (ImmutableList_1_t1755929927 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((ImmutableList_1_t1755929927 *)L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		NullCheck(L_6);
		if ((!(((uint32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_6)->max_length))))) == ((uint32_t)2))))
		{
			goto IL_003e;
		}
	}
	{
		ImmutableList_1_t1755929927 * L_7 = (ImmutableList_1_t1755929927 *)__this->get__observers_0();
		NullCheck((ImmutableList_1_t1755929927 *)L_7);
		IObserver_1U5BU5D_t4045638205* L_8 = ((  IObserver_1U5BU5D_t4045638205* (*) (ImmutableList_1_t1755929927 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((ImmutableList_1_t1755929927 *)L_7, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_9 = V_0;
		NullCheck(L_8);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_8, ((int32_t)((int32_t)1-(int32_t)L_9)));
		int32_t L_10 = ((int32_t)((int32_t)1-(int32_t)L_9));
		return ((L_8)->GetAt(static_cast<il2cpp_array_size_t>(L_10)));
	}

IL_003e:
	{
		ImmutableList_1_t1755929927 * L_11 = (ImmutableList_1_t1755929927 *)__this->get__observers_0();
		Il2CppObject* L_12 = ___observer0;
		NullCheck((ImmutableList_1_t1755929927 *)L_11);
		ImmutableList_1_t1755929927 * L_13 = ((  ImmutableList_1_t1755929927 * (*) (ImmutableList_1_t1755929927 *, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6)->method)((ImmutableList_1_t1755929927 *)L_11, (Il2CppObject*)L_12, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		ListObserver_1_t3073311492 * L_14 = (ListObserver_1_t3073311492 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		((  void (*) (ListObserver_1_t3073311492 *, ImmutableList_1_t1755929927 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)(L_14, (ImmutableList_1_t1755929927 *)L_13, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		return L_14;
	}
}
// System.Void UniRx.InternalUtil.ListObserver`1<System.Double>::.ctor(UniRx.InternalUtil.ImmutableList`1<UniRx.IObserver`1<T>>)
extern "C"  void ListObserver_1__ctor_m1930397811_gshared (ListObserver_1_t829134285 * __this, ImmutableList_1_t3806720016 * ___observers0, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		ImmutableList_1_t3806720016 * L_0 = ___observers0;
		__this->set__observers_0(L_0);
		return;
	}
}
// System.Void UniRx.InternalUtil.ListObserver`1<System.Double>::OnCompleted()
extern "C"  void ListObserver_1_OnCompleted_m3831432555_gshared (ListObserver_1_t829134285 * __this, const MethodInfo* method)
{
	IObserver_1U5BU5D_t740445744* V_0 = NULL;
	int32_t V_1 = 0;
	{
		ImmutableList_1_t3806720016 * L_0 = (ImmutableList_1_t3806720016 *)__this->get__observers_0();
		NullCheck((ImmutableList_1_t3806720016 *)L_0);
		IObserver_1U5BU5D_t740445744* L_1 = ((  IObserver_1U5BU5D_t740445744* (*) (ImmutableList_1_t3806720016 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((ImmutableList_1_t3806720016 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		V_0 = (IObserver_1U5BU5D_t740445744*)L_1;
		V_1 = (int32_t)0;
		goto IL_001f;
	}

IL_0013:
	{
		IObserver_1U5BU5D_t740445744* L_2 = V_0;
		int32_t L_3 = V_1;
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, L_3);
		int32_t L_4 = L_3;
		NullCheck((Il2CppObject*)((L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_4))));
		InterfaceActionInvoker0::Invoke(0 /* System.Void UniRx.IObserver`1<System.Double>::OnCompleted() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), (Il2CppObject*)((L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_4))));
		int32_t L_5 = V_1;
		V_1 = (int32_t)((int32_t)((int32_t)L_5+(int32_t)1));
	}

IL_001f:
	{
		int32_t L_6 = V_1;
		IObserver_1U5BU5D_t740445744* L_7 = V_0;
		NullCheck(L_7);
		if ((((int32_t)L_6) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_7)->max_length)))))))
		{
			goto IL_0013;
		}
	}
	{
		return;
	}
}
// System.Void UniRx.InternalUtil.ListObserver`1<System.Double>::OnError(System.Exception)
extern "C"  void ListObserver_1_OnError_m2859331096_gshared (ListObserver_1_t829134285 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method)
{
	IObserver_1U5BU5D_t740445744* V_0 = NULL;
	int32_t V_1 = 0;
	{
		ImmutableList_1_t3806720016 * L_0 = (ImmutableList_1_t3806720016 *)__this->get__observers_0();
		NullCheck((ImmutableList_1_t3806720016 *)L_0);
		IObserver_1U5BU5D_t740445744* L_1 = ((  IObserver_1U5BU5D_t740445744* (*) (ImmutableList_1_t3806720016 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((ImmutableList_1_t3806720016 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		V_0 = (IObserver_1U5BU5D_t740445744*)L_1;
		V_1 = (int32_t)0;
		goto IL_0020;
	}

IL_0013:
	{
		IObserver_1U5BU5D_t740445744* L_2 = V_0;
		int32_t L_3 = V_1;
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, L_3);
		int32_t L_4 = L_3;
		Exception_t1967233988 * L_5 = ___error0;
		NullCheck((Il2CppObject*)((L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_4))));
		InterfaceActionInvoker1< Exception_t1967233988 * >::Invoke(1 /* System.Void UniRx.IObserver`1<System.Double>::OnError(System.Exception) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), (Il2CppObject*)((L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_4))), (Exception_t1967233988 *)L_5);
		int32_t L_6 = V_1;
		V_1 = (int32_t)((int32_t)((int32_t)L_6+(int32_t)1));
	}

IL_0020:
	{
		int32_t L_7 = V_1;
		IObserver_1U5BU5D_t740445744* L_8 = V_0;
		NullCheck(L_8);
		if ((((int32_t)L_7) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_8)->max_length)))))))
		{
			goto IL_0013;
		}
	}
	{
		return;
	}
}
// System.Void UniRx.InternalUtil.ListObserver`1<System.Double>::OnNext(T)
extern "C"  void ListObserver_1_OnNext_m2916812041_gshared (ListObserver_1_t829134285 * __this, double ___value0, const MethodInfo* method)
{
	IObserver_1U5BU5D_t740445744* V_0 = NULL;
	int32_t V_1 = 0;
	{
		ImmutableList_1_t3806720016 * L_0 = (ImmutableList_1_t3806720016 *)__this->get__observers_0();
		NullCheck((ImmutableList_1_t3806720016 *)L_0);
		IObserver_1U5BU5D_t740445744* L_1 = ((  IObserver_1U5BU5D_t740445744* (*) (ImmutableList_1_t3806720016 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((ImmutableList_1_t3806720016 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		V_0 = (IObserver_1U5BU5D_t740445744*)L_1;
		V_1 = (int32_t)0;
		goto IL_0020;
	}

IL_0013:
	{
		IObserver_1U5BU5D_t740445744* L_2 = V_0;
		int32_t L_3 = V_1;
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, L_3);
		int32_t L_4 = L_3;
		double L_5 = ___value0;
		NullCheck((Il2CppObject*)((L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_4))));
		InterfaceActionInvoker1< double >::Invoke(2 /* System.Void UniRx.IObserver`1<System.Double>::OnNext(T) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), (Il2CppObject*)((L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_4))), (double)L_5);
		int32_t L_6 = V_1;
		V_1 = (int32_t)((int32_t)((int32_t)L_6+(int32_t)1));
	}

IL_0020:
	{
		int32_t L_7 = V_1;
		IObserver_1U5BU5D_t740445744* L_8 = V_0;
		NullCheck(L_8);
		if ((((int32_t)L_7) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_8)->max_length)))))))
		{
			goto IL_0013;
		}
	}
	{
		return;
	}
}
// UniRx.IObserver`1<T> UniRx.InternalUtil.ListObserver`1<System.Double>::Add(UniRx.IObserver`1<T>)
extern "C"  Il2CppObject* ListObserver_1_Add_m1205449883_gshared (ListObserver_1_t829134285 * __this, Il2CppObject* ___observer0, const MethodInfo* method)
{
	{
		ImmutableList_1_t3806720016 * L_0 = (ImmutableList_1_t3806720016 *)__this->get__observers_0();
		Il2CppObject* L_1 = ___observer0;
		NullCheck((ImmutableList_1_t3806720016 *)L_0);
		ImmutableList_1_t3806720016 * L_2 = ((  ImmutableList_1_t3806720016 * (*) (ImmutableList_1_t3806720016 *, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)((ImmutableList_1_t3806720016 *)L_0, (Il2CppObject*)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		ListObserver_1_t829134285 * L_3 = (ListObserver_1_t829134285 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		((  void (*) (ListObserver_1_t829134285 *, ImmutableList_1_t3806720016 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)(L_3, (ImmutableList_1_t3806720016 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		return L_3;
	}
}
// UniRx.IObserver`1<T> UniRx.InternalUtil.ListObserver`1<System.Double>::Remove(UniRx.IObserver`1<T>)
extern "C"  Il2CppObject* ListObserver_1_Remove_m1769348304_gshared (ListObserver_1_t829134285 * __this, Il2CppObject* ___observer0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		ImmutableList_1_t3806720016 * L_0 = (ImmutableList_1_t3806720016 *)__this->get__observers_0();
		NullCheck((ImmutableList_1_t3806720016 *)L_0);
		IObserver_1U5BU5D_t740445744* L_1 = ((  IObserver_1U5BU5D_t740445744* (*) (ImmutableList_1_t3806720016 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((ImmutableList_1_t3806720016 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		Il2CppObject* L_2 = ___observer0;
		int32_t L_3 = ((  int32_t (*) (Il2CppObject * /* static, unused */, IObserver_1U5BU5D_t740445744*, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->method)(NULL /*static, unused*/, (IObserver_1U5BU5D_t740445744*)L_1, (Il2CppObject*)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		V_0 = (int32_t)L_3;
		int32_t L_4 = V_0;
		if ((((int32_t)L_4) >= ((int32_t)0)))
		{
			goto IL_001b;
		}
	}
	{
		return __this;
	}

IL_001b:
	{
		ImmutableList_1_t3806720016 * L_5 = (ImmutableList_1_t3806720016 *)__this->get__observers_0();
		NullCheck((ImmutableList_1_t3806720016 *)L_5);
		IObserver_1U5BU5D_t740445744* L_6 = ((  IObserver_1U5BU5D_t740445744* (*) (ImmutableList_1_t3806720016 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((ImmutableList_1_t3806720016 *)L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		NullCheck(L_6);
		if ((!(((uint32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_6)->max_length))))) == ((uint32_t)2))))
		{
			goto IL_003e;
		}
	}
	{
		ImmutableList_1_t3806720016 * L_7 = (ImmutableList_1_t3806720016 *)__this->get__observers_0();
		NullCheck((ImmutableList_1_t3806720016 *)L_7);
		IObserver_1U5BU5D_t740445744* L_8 = ((  IObserver_1U5BU5D_t740445744* (*) (ImmutableList_1_t3806720016 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((ImmutableList_1_t3806720016 *)L_7, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_9 = V_0;
		NullCheck(L_8);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_8, ((int32_t)((int32_t)1-(int32_t)L_9)));
		int32_t L_10 = ((int32_t)((int32_t)1-(int32_t)L_9));
		return ((L_8)->GetAt(static_cast<il2cpp_array_size_t>(L_10)));
	}

IL_003e:
	{
		ImmutableList_1_t3806720016 * L_11 = (ImmutableList_1_t3806720016 *)__this->get__observers_0();
		Il2CppObject* L_12 = ___observer0;
		NullCheck((ImmutableList_1_t3806720016 *)L_11);
		ImmutableList_1_t3806720016 * L_13 = ((  ImmutableList_1_t3806720016 * (*) (ImmutableList_1_t3806720016 *, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6)->method)((ImmutableList_1_t3806720016 *)L_11, (Il2CppObject*)L_12, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		ListObserver_1_t829134285 * L_14 = (ListObserver_1_t829134285 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		((  void (*) (ListObserver_1_t829134285 *, ImmutableList_1_t3806720016 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)(L_14, (ImmutableList_1_t3806720016 *)L_13, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		return L_14;
	}
}
// System.Void UniRx.InternalUtil.ListObserver`1<System.Int32>::.ctor(UniRx.InternalUtil.ImmutableList`1<UniRx.IObserver`1<T>>)
extern "C"  void ListObserver_1__ctor_m943900794_gshared (ListObserver_1_t3142032458 * __this, ImmutableList_1_t1824650893 * ___observers0, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		ImmutableList_1_t1824650893 * L_0 = ___observers0;
		__this->set__observers_0(L_0);
		return;
	}
}
// System.Void UniRx.InternalUtil.ListObserver`1<System.Int32>::OnCompleted()
extern "C"  void ListObserver_1_OnCompleted_m1250157874_gshared (ListObserver_1_t3142032458 * __this, const MethodInfo* method)
{
	IObserver_1U5BU5D_t1502147871* V_0 = NULL;
	int32_t V_1 = 0;
	{
		ImmutableList_1_t1824650893 * L_0 = (ImmutableList_1_t1824650893 *)__this->get__observers_0();
		NullCheck((ImmutableList_1_t1824650893 *)L_0);
		IObserver_1U5BU5D_t1502147871* L_1 = ((  IObserver_1U5BU5D_t1502147871* (*) (ImmutableList_1_t1824650893 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((ImmutableList_1_t1824650893 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		V_0 = (IObserver_1U5BU5D_t1502147871*)L_1;
		V_1 = (int32_t)0;
		goto IL_001f;
	}

IL_0013:
	{
		IObserver_1U5BU5D_t1502147871* L_2 = V_0;
		int32_t L_3 = V_1;
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, L_3);
		int32_t L_4 = L_3;
		NullCheck((Il2CppObject*)((L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_4))));
		InterfaceActionInvoker0::Invoke(0 /* System.Void UniRx.IObserver`1<System.Int32>::OnCompleted() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), (Il2CppObject*)((L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_4))));
		int32_t L_5 = V_1;
		V_1 = (int32_t)((int32_t)((int32_t)L_5+(int32_t)1));
	}

IL_001f:
	{
		int32_t L_6 = V_1;
		IObserver_1U5BU5D_t1502147871* L_7 = V_0;
		NullCheck(L_7);
		if ((((int32_t)L_6) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_7)->max_length)))))))
		{
			goto IL_0013;
		}
	}
	{
		return;
	}
}
// System.Void UniRx.InternalUtil.ListObserver`1<System.Int32>::OnError(System.Exception)
extern "C"  void ListObserver_1_OnError_m2232834911_gshared (ListObserver_1_t3142032458 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method)
{
	IObserver_1U5BU5D_t1502147871* V_0 = NULL;
	int32_t V_1 = 0;
	{
		ImmutableList_1_t1824650893 * L_0 = (ImmutableList_1_t1824650893 *)__this->get__observers_0();
		NullCheck((ImmutableList_1_t1824650893 *)L_0);
		IObserver_1U5BU5D_t1502147871* L_1 = ((  IObserver_1U5BU5D_t1502147871* (*) (ImmutableList_1_t1824650893 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((ImmutableList_1_t1824650893 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		V_0 = (IObserver_1U5BU5D_t1502147871*)L_1;
		V_1 = (int32_t)0;
		goto IL_0020;
	}

IL_0013:
	{
		IObserver_1U5BU5D_t1502147871* L_2 = V_0;
		int32_t L_3 = V_1;
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, L_3);
		int32_t L_4 = L_3;
		Exception_t1967233988 * L_5 = ___error0;
		NullCheck((Il2CppObject*)((L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_4))));
		InterfaceActionInvoker1< Exception_t1967233988 * >::Invoke(1 /* System.Void UniRx.IObserver`1<System.Int32>::OnError(System.Exception) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), (Il2CppObject*)((L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_4))), (Exception_t1967233988 *)L_5);
		int32_t L_6 = V_1;
		V_1 = (int32_t)((int32_t)((int32_t)L_6+(int32_t)1));
	}

IL_0020:
	{
		int32_t L_7 = V_1;
		IObserver_1U5BU5D_t1502147871* L_8 = V_0;
		NullCheck(L_8);
		if ((((int32_t)L_7) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_8)->max_length)))))))
		{
			goto IL_0013;
		}
	}
	{
		return;
	}
}
// System.Void UniRx.InternalUtil.ListObserver`1<System.Int32>::OnNext(T)
extern "C"  void ListObserver_1_OnNext_m981262416_gshared (ListObserver_1_t3142032458 * __this, int32_t ___value0, const MethodInfo* method)
{
	IObserver_1U5BU5D_t1502147871* V_0 = NULL;
	int32_t V_1 = 0;
	{
		ImmutableList_1_t1824650893 * L_0 = (ImmutableList_1_t1824650893 *)__this->get__observers_0();
		NullCheck((ImmutableList_1_t1824650893 *)L_0);
		IObserver_1U5BU5D_t1502147871* L_1 = ((  IObserver_1U5BU5D_t1502147871* (*) (ImmutableList_1_t1824650893 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((ImmutableList_1_t1824650893 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		V_0 = (IObserver_1U5BU5D_t1502147871*)L_1;
		V_1 = (int32_t)0;
		goto IL_0020;
	}

IL_0013:
	{
		IObserver_1U5BU5D_t1502147871* L_2 = V_0;
		int32_t L_3 = V_1;
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, L_3);
		int32_t L_4 = L_3;
		int32_t L_5 = ___value0;
		NullCheck((Il2CppObject*)((L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_4))));
		InterfaceActionInvoker1< int32_t >::Invoke(2 /* System.Void UniRx.IObserver`1<System.Int32>::OnNext(T) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), (Il2CppObject*)((L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_4))), (int32_t)L_5);
		int32_t L_6 = V_1;
		V_1 = (int32_t)((int32_t)((int32_t)L_6+(int32_t)1));
	}

IL_0020:
	{
		int32_t L_7 = V_1;
		IObserver_1U5BU5D_t1502147871* L_8 = V_0;
		NullCheck(L_8);
		if ((((int32_t)L_7) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_8)->max_length)))))))
		{
			goto IL_0013;
		}
	}
	{
		return;
	}
}
// UniRx.IObserver`1<T> UniRx.InternalUtil.ListObserver`1<System.Int32>::Add(UniRx.IObserver`1<T>)
extern "C"  Il2CppObject* ListObserver_1_Add_m1209299248_gshared (ListObserver_1_t3142032458 * __this, Il2CppObject* ___observer0, const MethodInfo* method)
{
	{
		ImmutableList_1_t1824650893 * L_0 = (ImmutableList_1_t1824650893 *)__this->get__observers_0();
		Il2CppObject* L_1 = ___observer0;
		NullCheck((ImmutableList_1_t1824650893 *)L_0);
		ImmutableList_1_t1824650893 * L_2 = ((  ImmutableList_1_t1824650893 * (*) (ImmutableList_1_t1824650893 *, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)((ImmutableList_1_t1824650893 *)L_0, (Il2CppObject*)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		ListObserver_1_t3142032458 * L_3 = (ListObserver_1_t3142032458 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		((  void (*) (ListObserver_1_t3142032458 *, ImmutableList_1_t1824650893 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)(L_3, (ImmutableList_1_t1824650893 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		return L_3;
	}
}
// UniRx.IObserver`1<T> UniRx.InternalUtil.ListObserver`1<System.Int32>::Remove(UniRx.IObserver`1<T>)
extern "C"  Il2CppObject* ListObserver_1_Remove_m481664027_gshared (ListObserver_1_t3142032458 * __this, Il2CppObject* ___observer0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		ImmutableList_1_t1824650893 * L_0 = (ImmutableList_1_t1824650893 *)__this->get__observers_0();
		NullCheck((ImmutableList_1_t1824650893 *)L_0);
		IObserver_1U5BU5D_t1502147871* L_1 = ((  IObserver_1U5BU5D_t1502147871* (*) (ImmutableList_1_t1824650893 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((ImmutableList_1_t1824650893 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		Il2CppObject* L_2 = ___observer0;
		int32_t L_3 = ((  int32_t (*) (Il2CppObject * /* static, unused */, IObserver_1U5BU5D_t1502147871*, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->method)(NULL /*static, unused*/, (IObserver_1U5BU5D_t1502147871*)L_1, (Il2CppObject*)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		V_0 = (int32_t)L_3;
		int32_t L_4 = V_0;
		if ((((int32_t)L_4) >= ((int32_t)0)))
		{
			goto IL_001b;
		}
	}
	{
		return __this;
	}

IL_001b:
	{
		ImmutableList_1_t1824650893 * L_5 = (ImmutableList_1_t1824650893 *)__this->get__observers_0();
		NullCheck((ImmutableList_1_t1824650893 *)L_5);
		IObserver_1U5BU5D_t1502147871* L_6 = ((  IObserver_1U5BU5D_t1502147871* (*) (ImmutableList_1_t1824650893 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((ImmutableList_1_t1824650893 *)L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		NullCheck(L_6);
		if ((!(((uint32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_6)->max_length))))) == ((uint32_t)2))))
		{
			goto IL_003e;
		}
	}
	{
		ImmutableList_1_t1824650893 * L_7 = (ImmutableList_1_t1824650893 *)__this->get__observers_0();
		NullCheck((ImmutableList_1_t1824650893 *)L_7);
		IObserver_1U5BU5D_t1502147871* L_8 = ((  IObserver_1U5BU5D_t1502147871* (*) (ImmutableList_1_t1824650893 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((ImmutableList_1_t1824650893 *)L_7, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_9 = V_0;
		NullCheck(L_8);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_8, ((int32_t)((int32_t)1-(int32_t)L_9)));
		int32_t L_10 = ((int32_t)((int32_t)1-(int32_t)L_9));
		return ((L_8)->GetAt(static_cast<il2cpp_array_size_t>(L_10)));
	}

IL_003e:
	{
		ImmutableList_1_t1824650893 * L_11 = (ImmutableList_1_t1824650893 *)__this->get__observers_0();
		Il2CppObject* L_12 = ___observer0;
		NullCheck((ImmutableList_1_t1824650893 *)L_11);
		ImmutableList_1_t1824650893 * L_13 = ((  ImmutableList_1_t1824650893 * (*) (ImmutableList_1_t1824650893 *, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6)->method)((ImmutableList_1_t1824650893 *)L_11, (Il2CppObject*)L_12, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		ListObserver_1_t3142032458 * L_14 = (ListObserver_1_t3142032458 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		((  void (*) (ListObserver_1_t3142032458 *, ImmutableList_1_t1824650893 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)(L_14, (ImmutableList_1_t1824650893 *)L_13, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		return L_14;
	}
}
// System.Void UniRx.InternalUtil.ListObserver`1<System.Int64>::.ctor(UniRx.InternalUtil.ImmutableList`1<UniRx.IObserver`1<T>>)
extern "C"  void ListObserver_1__ctor_m3306140953_gshared (ListObserver_1_t3142032553 * __this, ImmutableList_1_t1824650988 * ___observers0, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		ImmutableList_1_t1824650988 * L_0 = ___observers0;
		__this->set__observers_0(L_0);
		return;
	}
}
// System.Void UniRx.InternalUtil.ListObserver`1<System.Int64>::OnCompleted()
extern "C"  void ListObserver_1_OnCompleted_m965560209_gshared (ListObserver_1_t3142032553 * __this, const MethodInfo* method)
{
	IObserver_1U5BU5D_t445342820* V_0 = NULL;
	int32_t V_1 = 0;
	{
		ImmutableList_1_t1824650988 * L_0 = (ImmutableList_1_t1824650988 *)__this->get__observers_0();
		NullCheck((ImmutableList_1_t1824650988 *)L_0);
		IObserver_1U5BU5D_t445342820* L_1 = ((  IObserver_1U5BU5D_t445342820* (*) (ImmutableList_1_t1824650988 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((ImmutableList_1_t1824650988 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		V_0 = (IObserver_1U5BU5D_t445342820*)L_1;
		V_1 = (int32_t)0;
		goto IL_001f;
	}

IL_0013:
	{
		IObserver_1U5BU5D_t445342820* L_2 = V_0;
		int32_t L_3 = V_1;
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, L_3);
		int32_t L_4 = L_3;
		NullCheck((Il2CppObject*)((L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_4))));
		InterfaceActionInvoker0::Invoke(0 /* System.Void UniRx.IObserver`1<System.Int64>::OnCompleted() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), (Il2CppObject*)((L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_4))));
		int32_t L_5 = V_1;
		V_1 = (int32_t)((int32_t)((int32_t)L_5+(int32_t)1));
	}

IL_001f:
	{
		int32_t L_6 = V_1;
		IObserver_1U5BU5D_t445342820* L_7 = V_0;
		NullCheck(L_7);
		if ((((int32_t)L_6) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_7)->max_length)))))))
		{
			goto IL_0013;
		}
	}
	{
		return;
	}
}
// System.Void UniRx.InternalUtil.ListObserver`1<System.Int64>::OnError(System.Exception)
extern "C"  void ListObserver_1_OnError_m3652986686_gshared (ListObserver_1_t3142032553 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method)
{
	IObserver_1U5BU5D_t445342820* V_0 = NULL;
	int32_t V_1 = 0;
	{
		ImmutableList_1_t1824650988 * L_0 = (ImmutableList_1_t1824650988 *)__this->get__observers_0();
		NullCheck((ImmutableList_1_t1824650988 *)L_0);
		IObserver_1U5BU5D_t445342820* L_1 = ((  IObserver_1U5BU5D_t445342820* (*) (ImmutableList_1_t1824650988 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((ImmutableList_1_t1824650988 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		V_0 = (IObserver_1U5BU5D_t445342820*)L_1;
		V_1 = (int32_t)0;
		goto IL_0020;
	}

IL_0013:
	{
		IObserver_1U5BU5D_t445342820* L_2 = V_0;
		int32_t L_3 = V_1;
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, L_3);
		int32_t L_4 = L_3;
		Exception_t1967233988 * L_5 = ___error0;
		NullCheck((Il2CppObject*)((L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_4))));
		InterfaceActionInvoker1< Exception_t1967233988 * >::Invoke(1 /* System.Void UniRx.IObserver`1<System.Int64>::OnError(System.Exception) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), (Il2CppObject*)((L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_4))), (Exception_t1967233988 *)L_5);
		int32_t L_6 = V_1;
		V_1 = (int32_t)((int32_t)((int32_t)L_6+(int32_t)1));
	}

IL_0020:
	{
		int32_t L_7 = V_1;
		IObserver_1U5BU5D_t445342820* L_8 = V_0;
		NullCheck(L_8);
		if ((((int32_t)L_7) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_8)->max_length)))))))
		{
			goto IL_0013;
		}
	}
	{
		return;
	}
}
// System.Void UniRx.InternalUtil.ListObserver`1<System.Int64>::OnNext(T)
extern "C"  void ListObserver_1_OnNext_m3172747823_gshared (ListObserver_1_t3142032553 * __this, int64_t ___value0, const MethodInfo* method)
{
	IObserver_1U5BU5D_t445342820* V_0 = NULL;
	int32_t V_1 = 0;
	{
		ImmutableList_1_t1824650988 * L_0 = (ImmutableList_1_t1824650988 *)__this->get__observers_0();
		NullCheck((ImmutableList_1_t1824650988 *)L_0);
		IObserver_1U5BU5D_t445342820* L_1 = ((  IObserver_1U5BU5D_t445342820* (*) (ImmutableList_1_t1824650988 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((ImmutableList_1_t1824650988 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		V_0 = (IObserver_1U5BU5D_t445342820*)L_1;
		V_1 = (int32_t)0;
		goto IL_0020;
	}

IL_0013:
	{
		IObserver_1U5BU5D_t445342820* L_2 = V_0;
		int32_t L_3 = V_1;
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, L_3);
		int32_t L_4 = L_3;
		int64_t L_5 = ___value0;
		NullCheck((Il2CppObject*)((L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_4))));
		InterfaceActionInvoker1< int64_t >::Invoke(2 /* System.Void UniRx.IObserver`1<System.Int64>::OnNext(T) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), (Il2CppObject*)((L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_4))), (int64_t)L_5);
		int32_t L_6 = V_1;
		V_1 = (int32_t)((int32_t)((int32_t)L_6+(int32_t)1));
	}

IL_0020:
	{
		int32_t L_7 = V_1;
		IObserver_1U5BU5D_t445342820* L_8 = V_0;
		NullCheck(L_8);
		if ((((int32_t)L_7) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_8)->max_length)))))))
		{
			goto IL_0013;
		}
	}
	{
		return;
	}
}
// UniRx.IObserver`1<T> UniRx.InternalUtil.ListObserver`1<System.Int64>::Add(UniRx.IObserver`1<T>)
extern "C"  Il2CppObject* ListObserver_1_Add_m2629451023_gshared (ListObserver_1_t3142032553 * __this, Il2CppObject* ___observer0, const MethodInfo* method)
{
	{
		ImmutableList_1_t1824650988 * L_0 = (ImmutableList_1_t1824650988 *)__this->get__observers_0();
		Il2CppObject* L_1 = ___observer0;
		NullCheck((ImmutableList_1_t1824650988 *)L_0);
		ImmutableList_1_t1824650988 * L_2 = ((  ImmutableList_1_t1824650988 * (*) (ImmutableList_1_t1824650988 *, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)((ImmutableList_1_t1824650988 *)L_0, (Il2CppObject*)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		ListObserver_1_t3142032553 * L_3 = (ListObserver_1_t3142032553 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		((  void (*) (ListObserver_1_t3142032553 *, ImmutableList_1_t1824650988 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)(L_3, (ImmutableList_1_t1824650988 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		return L_3;
	}
}
// UniRx.IObserver`1<T> UniRx.InternalUtil.ListObserver`1<System.Int64>::Remove(UniRx.IObserver`1<T>)
extern "C"  Il2CppObject* ListObserver_1_Remove_m2795327452_gshared (ListObserver_1_t3142032553 * __this, Il2CppObject* ___observer0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		ImmutableList_1_t1824650988 * L_0 = (ImmutableList_1_t1824650988 *)__this->get__observers_0();
		NullCheck((ImmutableList_1_t1824650988 *)L_0);
		IObserver_1U5BU5D_t445342820* L_1 = ((  IObserver_1U5BU5D_t445342820* (*) (ImmutableList_1_t1824650988 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((ImmutableList_1_t1824650988 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		Il2CppObject* L_2 = ___observer0;
		int32_t L_3 = ((  int32_t (*) (Il2CppObject * /* static, unused */, IObserver_1U5BU5D_t445342820*, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->method)(NULL /*static, unused*/, (IObserver_1U5BU5D_t445342820*)L_1, (Il2CppObject*)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		V_0 = (int32_t)L_3;
		int32_t L_4 = V_0;
		if ((((int32_t)L_4) >= ((int32_t)0)))
		{
			goto IL_001b;
		}
	}
	{
		return __this;
	}

IL_001b:
	{
		ImmutableList_1_t1824650988 * L_5 = (ImmutableList_1_t1824650988 *)__this->get__observers_0();
		NullCheck((ImmutableList_1_t1824650988 *)L_5);
		IObserver_1U5BU5D_t445342820* L_6 = ((  IObserver_1U5BU5D_t445342820* (*) (ImmutableList_1_t1824650988 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((ImmutableList_1_t1824650988 *)L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		NullCheck(L_6);
		if ((!(((uint32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_6)->max_length))))) == ((uint32_t)2))))
		{
			goto IL_003e;
		}
	}
	{
		ImmutableList_1_t1824650988 * L_7 = (ImmutableList_1_t1824650988 *)__this->get__observers_0();
		NullCheck((ImmutableList_1_t1824650988 *)L_7);
		IObserver_1U5BU5D_t445342820* L_8 = ((  IObserver_1U5BU5D_t445342820* (*) (ImmutableList_1_t1824650988 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((ImmutableList_1_t1824650988 *)L_7, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_9 = V_0;
		NullCheck(L_8);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_8, ((int32_t)((int32_t)1-(int32_t)L_9)));
		int32_t L_10 = ((int32_t)((int32_t)1-(int32_t)L_9));
		return ((L_8)->GetAt(static_cast<il2cpp_array_size_t>(L_10)));
	}

IL_003e:
	{
		ImmutableList_1_t1824650988 * L_11 = (ImmutableList_1_t1824650988 *)__this->get__observers_0();
		Il2CppObject* L_12 = ___observer0;
		NullCheck((ImmutableList_1_t1824650988 *)L_11);
		ImmutableList_1_t1824650988 * L_13 = ((  ImmutableList_1_t1824650988 * (*) (ImmutableList_1_t1824650988 *, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6)->method)((ImmutableList_1_t1824650988 *)L_11, (Il2CppObject*)L_12, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		ListObserver_1_t3142032553 * L_14 = (ListObserver_1_t3142032553 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		((  void (*) (ListObserver_1_t3142032553 *, ImmutableList_1_t1824650988 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)(L_14, (ImmutableList_1_t1824650988 *)L_13, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		return L_14;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
