﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.CastObservable`2/Cast<System.Object,System.Object>
struct Cast_t3935810676;
// UniRx.IObserver`1<System.Object>
struct IObserver_1_t3049105323;
// System.IDisposable
struct IDisposable_t1628921374;
// System.Object
struct Il2CppObject;
// System.Exception
struct Exception_t1967233988;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_Exception1967233988.h"

// System.Void UniRx.Operators.CastObservable`2/Cast<System.Object,System.Object>::.ctor(UniRx.IObserver`1<TResult>,System.IDisposable)
extern "C"  void Cast__ctor_m799924069_gshared (Cast_t3935810676 * __this, Il2CppObject* ___observer0, Il2CppObject * ___cancel1, const MethodInfo* method);
#define Cast__ctor_m799924069(__this, ___observer0, ___cancel1, method) ((  void (*) (Cast_t3935810676 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))Cast__ctor_m799924069_gshared)(__this, ___observer0, ___cancel1, method)
// System.Void UniRx.Operators.CastObservable`2/Cast<System.Object,System.Object>::OnNext(TSource)
extern "C"  void Cast_OnNext_m3153415157_gshared (Cast_t3935810676 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Cast_OnNext_m3153415157(__this, ___value0, method) ((  void (*) (Cast_t3935810676 *, Il2CppObject *, const MethodInfo*))Cast_OnNext_m3153415157_gshared)(__this, ___value0, method)
// System.Void UniRx.Operators.CastObservable`2/Cast<System.Object,System.Object>::OnError(System.Exception)
extern "C"  void Cast_OnError_m2846579999_gshared (Cast_t3935810676 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method);
#define Cast_OnError_m2846579999(__this, ___error0, method) ((  void (*) (Cast_t3935810676 *, Exception_t1967233988 *, const MethodInfo*))Cast_OnError_m2846579999_gshared)(__this, ___error0, method)
// System.Void UniRx.Operators.CastObservable`2/Cast<System.Object,System.Object>::OnCompleted()
extern "C"  void Cast_OnCompleted_m1933412082_gshared (Cast_t3935810676 * __this, const MethodInfo* method);
#define Cast_OnCompleted_m1933412082(__this, method) ((  void (*) (Cast_t3935810676 *, const MethodInfo*))Cast_OnCompleted_m1933412082_gshared)(__this, method)
