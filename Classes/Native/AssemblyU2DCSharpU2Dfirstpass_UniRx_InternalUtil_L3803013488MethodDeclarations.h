﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.InternalUtil.ListObserver`1<TableButtons/StatkaPoTableViev>
struct ListObserver_1_t3803013488;
// UniRx.InternalUtil.ImmutableList`1<UniRx.IObserver`1<TableButtons/StatkaPoTableViev>>
struct ImmutableList_1_t2485631923;
// System.Exception
struct Exception_t1967233988;
// UniRx.IObserver`1<TableButtons/StatkaPoTableViev>
struct IObserver_1_t1425427424;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Exception1967233988.h"
#include "AssemblyU2DCSharp_TableButtons_StatkaPoTableViev3508395817.h"

// System.Void UniRx.InternalUtil.ListObserver`1<TableButtons/StatkaPoTableViev>::.ctor(UniRx.InternalUtil.ImmutableList`1<UniRx.IObserver`1<T>>)
extern "C"  void ListObserver_1__ctor_m620624376_gshared (ListObserver_1_t3803013488 * __this, ImmutableList_1_t2485631923 * ___observers0, const MethodInfo* method);
#define ListObserver_1__ctor_m620624376(__this, ___observers0, method) ((  void (*) (ListObserver_1_t3803013488 *, ImmutableList_1_t2485631923 *, const MethodInfo*))ListObserver_1__ctor_m620624376_gshared)(__this, ___observers0, method)
// System.Void UniRx.InternalUtil.ListObserver`1<TableButtons/StatkaPoTableViev>::OnCompleted()
extern "C"  void ListObserver_1_OnCompleted_m4105867824_gshared (ListObserver_1_t3803013488 * __this, const MethodInfo* method);
#define ListObserver_1_OnCompleted_m4105867824(__this, method) ((  void (*) (ListObserver_1_t3803013488 *, const MethodInfo*))ListObserver_1_OnCompleted_m4105867824_gshared)(__this, method)
// System.Void UniRx.InternalUtil.ListObserver`1<TableButtons/StatkaPoTableViev>::OnError(System.Exception)
extern "C"  void ListObserver_1_OnError_m1017871197_gshared (ListObserver_1_t3803013488 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method);
#define ListObserver_1_OnError_m1017871197(__this, ___error0, method) ((  void (*) (ListObserver_1_t3803013488 *, Exception_t1967233988 *, const MethodInfo*))ListObserver_1_OnError_m1017871197_gshared)(__this, ___error0, method)
// System.Void UniRx.InternalUtil.ListObserver`1<TableButtons/StatkaPoTableViev>::OnNext(T)
extern "C"  void ListObserver_1_OnNext_m3634216014_gshared (ListObserver_1_t3803013488 * __this, int32_t ___value0, const MethodInfo* method);
#define ListObserver_1_OnNext_m3634216014(__this, ___value0, method) ((  void (*) (ListObserver_1_t3803013488 *, int32_t, const MethodInfo*))ListObserver_1_OnNext_m3634216014_gshared)(__this, ___value0, method)
// UniRx.IObserver`1<T> UniRx.InternalUtil.ListObserver`1<TableButtons/StatkaPoTableViev>::Add(UniRx.IObserver`1<T>)
extern "C"  Il2CppObject* ListObserver_1_Add_m1033640302_gshared (ListObserver_1_t3803013488 * __this, Il2CppObject* ___observer0, const MethodInfo* method);
#define ListObserver_1_Add_m1033640302(__this, ___observer0, method) ((  Il2CppObject* (*) (ListObserver_1_t3803013488 *, Il2CppObject*, const MethodInfo*))ListObserver_1_Add_m1033640302_gshared)(__this, ___observer0, method)
// UniRx.IObserver`1<T> UniRx.InternalUtil.ListObserver`1<TableButtons/StatkaPoTableViev>::Remove(UniRx.IObserver`1<T>)
extern "C"  Il2CppObject* ListObserver_1_Remove_m2991137565_gshared (ListObserver_1_t3803013488 * __this, Il2CppObject* ___observer0, const MethodInfo* method);
#define ListObserver_1_Remove_m2991137565(__this, ___observer0, method) ((  Il2CppObject* (*) (ListObserver_1_t3803013488 *, Il2CppObject*, const MethodInfo*))ListObserver_1_Remove_m2991137565_gshared)(__this, ___observer0, method)
