﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "System_Core_System_Func_2_gen4146091814MethodDeclarations.h"

// System.Void System.Func`2<DateTimeCell,System.Int64>::.ctor(System.Object,System.IntPtr)
#define Func_2__ctor_m3021761135(__this, ___object0, ___method1, method) ((  void (*) (Func_2_t831010073 *, Il2CppObject *, IntPtr_t, const MethodInfo*))Func_2__ctor_m4162203778_gshared)(__this, ___object0, ___method1, method)
// TResult System.Func`2<DateTimeCell,System.Int64>::Invoke(T)
#define Func_2_Invoke_m1350747907(__this, ___arg10, method) ((  int64_t (*) (Func_2_t831010073 *, DateTimeCell_t773798845 *, const MethodInfo*))Func_2_Invoke_m1323351588_gshared)(__this, ___arg10, method)
// System.IAsyncResult System.Func`2<DateTimeCell,System.Int64>::BeginInvoke(T,System.AsyncCallback,System.Object)
#define Func_2_BeginInvoke_m3440107442(__this, ___arg10, ___callback1, ___object2, method) ((  Il2CppObject * (*) (Func_2_t831010073 *, DateTimeCell_t773798845 *, AsyncCallback_t1363551830 *, Il2CppObject *, const MethodInfo*))Func_2_BeginInvoke_m934489047_gshared)(__this, ___arg10, ___callback1, ___object2, method)
// TResult System.Func`2<DateTimeCell,System.Int64>::EndInvoke(System.IAsyncResult)
#define Func_2_EndInvoke_m993332401(__this, ___result0, method) ((  int64_t (*) (Func_2_t831010073 *, Il2CppObject *, const MethodInfo*))Func_2_EndInvoke_m658801712_gshared)(__this, ___result0, method)
