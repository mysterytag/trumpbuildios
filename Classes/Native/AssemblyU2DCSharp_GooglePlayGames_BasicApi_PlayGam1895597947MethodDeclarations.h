﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GooglePlayGames.BasicApi.PlayGamesClientConfiguration/Builder
struct Builder_t1895597948;
// GooglePlayGames.BasicApi.InvitationReceivedDelegate
struct InvitationReceivedDelegate_t716623425;
// GooglePlayGames.BasicApi.Multiplayer.MatchDelegate
struct MatchDelegate_t4292352560;
// GooglePlayGames.BasicApi.Multiplayer.Invitation
struct Invitation_t3047929759;
// GooglePlayGames.BasicApi.Multiplayer.TurnBasedMatch
struct TurnBasedMatch_t3733733941;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_GooglePlayGames_BasicApi_Invitati716623425.h"
#include "AssemblyU2DCSharp_GooglePlayGames_BasicApi_Multipl4292352560.h"
#include "AssemblyU2DCSharp_GooglePlayGames_BasicApi_PlayGame962255808.h"
#include "AssemblyU2DCSharp_GooglePlayGames_BasicApi_Multipl3047929759.h"
#include "AssemblyU2DCSharp_GooglePlayGames_BasicApi_Multipl3733733941.h"

// System.Void GooglePlayGames.BasicApi.PlayGamesClientConfiguration/Builder::.ctor()
extern "C"  void Builder__ctor_m964831929 (Builder_t1895597948 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// GooglePlayGames.BasicApi.PlayGamesClientConfiguration/Builder GooglePlayGames.BasicApi.PlayGamesClientConfiguration/Builder::EnableSavedGames()
extern "C"  Builder_t1895597948 * Builder_EnableSavedGames_m3830798215 (Builder_t1895597948 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// GooglePlayGames.BasicApi.PlayGamesClientConfiguration/Builder GooglePlayGames.BasicApi.PlayGamesClientConfiguration/Builder::EnableDeprecatedCloudSave()
extern "C"  Builder_t1895597948 * Builder_EnableDeprecatedCloudSave_m1489294100 (Builder_t1895597948 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// GooglePlayGames.BasicApi.PlayGamesClientConfiguration/Builder GooglePlayGames.BasicApi.PlayGamesClientConfiguration/Builder::WithInvitationDelegate(GooglePlayGames.BasicApi.InvitationReceivedDelegate)
extern "C"  Builder_t1895597948 * Builder_WithInvitationDelegate_m395124669 (Builder_t1895597948 * __this, InvitationReceivedDelegate_t716623425 * ___invitationDelegate0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// GooglePlayGames.BasicApi.PlayGamesClientConfiguration/Builder GooglePlayGames.BasicApi.PlayGamesClientConfiguration/Builder::WithMatchDelegate(GooglePlayGames.BasicApi.Multiplayer.MatchDelegate)
extern "C"  Builder_t1895597948 * Builder_WithMatchDelegate_m1887362318 (Builder_t1895597948 * __this, MatchDelegate_t4292352560 * ___matchDelegate0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GooglePlayGames.BasicApi.PlayGamesClientConfiguration/Builder::HasEnableSaveGames()
extern "C"  bool Builder_HasEnableSaveGames_m502992894 (Builder_t1895597948 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GooglePlayGames.BasicApi.PlayGamesClientConfiguration/Builder::HasEnableDeprecatedCloudSave()
extern "C"  bool Builder_HasEnableDeprecatedCloudSave_m2059255705 (Builder_t1895597948 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// GooglePlayGames.BasicApi.Multiplayer.MatchDelegate GooglePlayGames.BasicApi.PlayGamesClientConfiguration/Builder::GetMatchDelegate()
extern "C"  MatchDelegate_t4292352560 * Builder_GetMatchDelegate_m3448515760 (Builder_t1895597948 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// GooglePlayGames.BasicApi.InvitationReceivedDelegate GooglePlayGames.BasicApi.PlayGamesClientConfiguration/Builder::GetInvitationDelegate()
extern "C"  InvitationReceivedDelegate_t716623425 * Builder_GetInvitationDelegate_m800321655 (Builder_t1895597948 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// GooglePlayGames.BasicApi.PlayGamesClientConfiguration GooglePlayGames.BasicApi.PlayGamesClientConfiguration/Builder::Build()
extern "C"  PlayGamesClientConfiguration_t962255808  Builder_Build_m3779419922 (Builder_t1895597948 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.BasicApi.PlayGamesClientConfiguration/Builder::<mInvitationDelegate>m__1C(GooglePlayGames.BasicApi.Multiplayer.Invitation,System.Boolean)
extern "C"  void Builder_U3CmInvitationDelegateU3Em__1C_m3732945147 (Il2CppObject * __this /* static, unused */, Invitation_t3047929759 * p0, bool p1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.BasicApi.PlayGamesClientConfiguration/Builder::<mMatchDelegate>m__1D(GooglePlayGames.BasicApi.Multiplayer.TurnBasedMatch,System.Boolean)
extern "C"  void Builder_U3CmMatchDelegateU3Em__1D_m1290705192 (Il2CppObject * __this /* static, unused */, TurnBasedMatch_t3733733941 * p0, bool p1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
