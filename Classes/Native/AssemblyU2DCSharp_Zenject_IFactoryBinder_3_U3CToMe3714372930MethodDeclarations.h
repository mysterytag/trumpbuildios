﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Zenject.IFactoryBinder`3/<ToMethod>c__AnonStorey178<System.Object,System.Object,System.Object>
struct U3CToMethodU3Ec__AnonStorey178_t3714372930;
// Zenject.IFactory`3<System.Object,System.Object,System.Object>
struct IFactory_3_t1619999162;
// Zenject.InjectContext
struct InjectContext_t3456483891;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Zenject_InjectContext3456483891.h"

// System.Void Zenject.IFactoryBinder`3/<ToMethod>c__AnonStorey178<System.Object,System.Object,System.Object>::.ctor()
extern "C"  void U3CToMethodU3Ec__AnonStorey178__ctor_m783517254_gshared (U3CToMethodU3Ec__AnonStorey178_t3714372930 * __this, const MethodInfo* method);
#define U3CToMethodU3Ec__AnonStorey178__ctor_m783517254(__this, method) ((  void (*) (U3CToMethodU3Ec__AnonStorey178_t3714372930 *, const MethodInfo*))U3CToMethodU3Ec__AnonStorey178__ctor_m783517254_gshared)(__this, method)
// Zenject.IFactory`3<TParam1,TParam2,TContract> Zenject.IFactoryBinder`3/<ToMethod>c__AnonStorey178<System.Object,System.Object,System.Object>::<>m__29C(Zenject.InjectContext)
extern "C"  Il2CppObject* U3CToMethodU3Ec__AnonStorey178_U3CU3Em__29C_m839474009_gshared (U3CToMethodU3Ec__AnonStorey178_t3714372930 * __this, InjectContext_t3456483891 * ___ctx0, const MethodInfo* method);
#define U3CToMethodU3Ec__AnonStorey178_U3CU3Em__29C_m839474009(__this, ___ctx0, method) ((  Il2CppObject* (*) (U3CToMethodU3Ec__AnonStorey178_t3714372930 *, InjectContext_t3456483891 *, const MethodInfo*))U3CToMethodU3Ec__AnonStorey178_U3CU3Em__29C_m839474009_gshared)(__this, ___ctx0, method)
