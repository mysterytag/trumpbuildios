﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Array
struct Il2CppArray;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen277776696.h"
#include "mscorlib_System_Array2840145358.h"
#include "System_Xml_System_Xml_XPath_XPathResultType370286193.h"

// System.Void System.Array/InternalEnumerator`1<System.Xml.XPath.XPathResultType>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m1927827934_gshared (InternalEnumerator_1_t277776696 * __this, Il2CppArray * ___array0, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m1927827934(__this, ___array0, method) ((  void (*) (InternalEnumerator_1_t277776696 *, Il2CppArray *, const MethodInfo*))InternalEnumerator_1__ctor_m1927827934_gshared)(__this, ___array0, method)
// System.Void System.Array/InternalEnumerator`1<System.Xml.XPath.XPathResultType>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3549571842_gshared (InternalEnumerator_1_t277776696 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3549571842(__this, method) ((  void (*) (InternalEnumerator_1_t277776696 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3549571842_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<System.Xml.XPath.XPathResultType>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2701739950_gshared (InternalEnumerator_1_t277776696 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2701739950(__this, method) ((  Il2CppObject * (*) (InternalEnumerator_1_t277776696 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2701739950_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Xml.XPath.XPathResultType>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m1938487605_gshared (InternalEnumerator_1_t277776696 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m1938487605(__this, method) ((  void (*) (InternalEnumerator_1_t277776696 *, const MethodInfo*))InternalEnumerator_1_Dispose_m1938487605_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<System.Xml.XPath.XPathResultType>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m1858589678_gshared (InternalEnumerator_1_t277776696 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m1858589678(__this, method) ((  bool (*) (InternalEnumerator_1_t277776696 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m1858589678_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<System.Xml.XPath.XPathResultType>::get_Current()
extern "C"  int32_t InternalEnumerator_1_get_Current_m2223718117_gshared (InternalEnumerator_1_t277776696 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m2223718117(__this, method) ((  int32_t (*) (InternalEnumerator_1_t277776696 *, const MethodInfo*))InternalEnumerator_1_get_Current_m2223718117_gshared)(__this, method)
