﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UniRx.IObserver`1<UniRx.CollectionAddEvent`1<UniRx.Tuple`2<System.Object,System.Object>>>
struct IObserver_1_t4160676289;
// UniRx.SingleAssignmentDisposable
struct SingleAssignmentDisposable_t2336378823;
// UniRx.Operators.OperatorObservableBase`1<UniRx.CollectionAddEvent`1<UniRx.Tuple`2<System.Object,System.Object>>>
struct OperatorObservableBase_1_t1012822357;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Operators.OperatorObservableBase`1/<Subscribe>c__AnonStorey5B<UniRx.CollectionAddEvent`1<UniRx.Tuple`2<System.Object,System.Object>>>
struct  U3CSubscribeU3Ec__AnonStorey5B_t1546684115  : public Il2CppObject
{
public:
	// UniRx.IObserver`1<T> UniRx.Operators.OperatorObservableBase`1/<Subscribe>c__AnonStorey5B::observer
	Il2CppObject* ___observer_0;
	// UniRx.SingleAssignmentDisposable UniRx.Operators.OperatorObservableBase`1/<Subscribe>c__AnonStorey5B::subscription
	SingleAssignmentDisposable_t2336378823 * ___subscription_1;
	// UniRx.Operators.OperatorObservableBase`1<T> UniRx.Operators.OperatorObservableBase`1/<Subscribe>c__AnonStorey5B::<>f__this
	OperatorObservableBase_1_t1012822357 * ___U3CU3Ef__this_2;

public:
	inline static int32_t get_offset_of_observer_0() { return static_cast<int32_t>(offsetof(U3CSubscribeU3Ec__AnonStorey5B_t1546684115, ___observer_0)); }
	inline Il2CppObject* get_observer_0() const { return ___observer_0; }
	inline Il2CppObject** get_address_of_observer_0() { return &___observer_0; }
	inline void set_observer_0(Il2CppObject* value)
	{
		___observer_0 = value;
		Il2CppCodeGenWriteBarrier(&___observer_0, value);
	}

	inline static int32_t get_offset_of_subscription_1() { return static_cast<int32_t>(offsetof(U3CSubscribeU3Ec__AnonStorey5B_t1546684115, ___subscription_1)); }
	inline SingleAssignmentDisposable_t2336378823 * get_subscription_1() const { return ___subscription_1; }
	inline SingleAssignmentDisposable_t2336378823 ** get_address_of_subscription_1() { return &___subscription_1; }
	inline void set_subscription_1(SingleAssignmentDisposable_t2336378823 * value)
	{
		___subscription_1 = value;
		Il2CppCodeGenWriteBarrier(&___subscription_1, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__this_2() { return static_cast<int32_t>(offsetof(U3CSubscribeU3Ec__AnonStorey5B_t1546684115, ___U3CU3Ef__this_2)); }
	inline OperatorObservableBase_1_t1012822357 * get_U3CU3Ef__this_2() const { return ___U3CU3Ef__this_2; }
	inline OperatorObservableBase_1_t1012822357 ** get_address_of_U3CU3Ef__this_2() { return &___U3CU3Ef__this_2; }
	inline void set_U3CU3Ef__this_2(OperatorObservableBase_1_t1012822357 * value)
	{
		___U3CU3Ef__this_2 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__this_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
