﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Net.HttpUtility
struct HttpUtility_t1925230350;
// System.String
struct String_t;
// System.Char[]
struct CharU5BU5D_t3416858730;
// System.IO.MemoryStream
struct MemoryStream_t2881531048;
// System.Text.Encoding
struct Encoding_t180559927;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String968488902.h"
#include "mscorlib_System_IO_MemoryStream2881531048.h"
#include "mscorlib_System_Text_Encoding180559927.h"

// System.Void System.Net.HttpUtility::.ctor()
extern "C"  void HttpUtility__ctor_m2035566920 (HttpUtility_t1925230350 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Net.HttpUtility::UrlDecode(System.String)
extern "C"  String_t* HttpUtility_UrlDecode_m3619317602 (Il2CppObject * __this /* static, unused */, String_t* ___s0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Char[] System.Net.HttpUtility::GetChars(System.IO.MemoryStream,System.Text.Encoding)
extern "C"  CharU5BU5D_t3416858730* HttpUtility_GetChars_m2210861724 (Il2CppObject * __this /* static, unused */, MemoryStream_t2881531048 * ___b0, Encoding_t180559927 * ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Net.HttpUtility::UrlDecode(System.String,System.Text.Encoding)
extern "C"  String_t* HttpUtility_UrlDecode_m193935599 (Il2CppObject * __this /* static, unused */, String_t* ___s0, Encoding_t180559927 * ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
