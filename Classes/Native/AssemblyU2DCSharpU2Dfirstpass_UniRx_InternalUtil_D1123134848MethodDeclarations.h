﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.InternalUtil.DisposedObserver`1<UniRx.CollectionAddEvent`1<UniRx.Tuple`2<System.Object,System.Object>>>
struct DisposedObserver_1_t1123134848;
// System.Exception
struct Exception_t1967233988;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Exception1967233988.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_CollectionAddE1948677386.h"

// System.Void UniRx.InternalUtil.DisposedObserver`1<UniRx.CollectionAddEvent`1<UniRx.Tuple`2<System.Object,System.Object>>>::.ctor()
extern "C"  void DisposedObserver_1__ctor_m850754732_gshared (DisposedObserver_1_t1123134848 * __this, const MethodInfo* method);
#define DisposedObserver_1__ctor_m850754732(__this, method) ((  void (*) (DisposedObserver_1_t1123134848 *, const MethodInfo*))DisposedObserver_1__ctor_m850754732_gshared)(__this, method)
// System.Void UniRx.InternalUtil.DisposedObserver`1<UniRx.CollectionAddEvent`1<UniRx.Tuple`2<System.Object,System.Object>>>::.cctor()
extern "C"  void DisposedObserver_1__cctor_m121496705_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define DisposedObserver_1__cctor_m121496705(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))DisposedObserver_1__cctor_m121496705_gshared)(__this /* static, unused */, method)
// System.Void UniRx.InternalUtil.DisposedObserver`1<UniRx.CollectionAddEvent`1<UniRx.Tuple`2<System.Object,System.Object>>>::OnCompleted()
extern "C"  void DisposedObserver_1_OnCompleted_m390299446_gshared (DisposedObserver_1_t1123134848 * __this, const MethodInfo* method);
#define DisposedObserver_1_OnCompleted_m390299446(__this, method) ((  void (*) (DisposedObserver_1_t1123134848 *, const MethodInfo*))DisposedObserver_1_OnCompleted_m390299446_gshared)(__this, method)
// System.Void UniRx.InternalUtil.DisposedObserver`1<UniRx.CollectionAddEvent`1<UniRx.Tuple`2<System.Object,System.Object>>>::OnError(System.Exception)
extern "C"  void DisposedObserver_1_OnError_m3075149667_gshared (DisposedObserver_1_t1123134848 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method);
#define DisposedObserver_1_OnError_m3075149667(__this, ___error0, method) ((  void (*) (DisposedObserver_1_t1123134848 *, Exception_t1967233988 *, const MethodInfo*))DisposedObserver_1_OnError_m3075149667_gshared)(__this, ___error0, method)
// System.Void UniRx.InternalUtil.DisposedObserver`1<UniRx.CollectionAddEvent`1<UniRx.Tuple`2<System.Object,System.Object>>>::OnNext(T)
extern "C"  void DisposedObserver_1_OnNext_m67247188_gshared (DisposedObserver_1_t1123134848 * __this, CollectionAddEvent_1_t1948677386  ___value0, const MethodInfo* method);
#define DisposedObserver_1_OnNext_m67247188(__this, ___value0, method) ((  void (*) (DisposedObserver_1_t1123134848 *, CollectionAddEvent_1_t1948677386 , const MethodInfo*))DisposedObserver_1_OnNext_m67247188_gshared)(__this, ___value0, method)
