﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.PlayFabClientAPI/AndroidDevicePushNotificationRegistrationResponseCallback
struct AndroidDevicePushNotificationRegistrationResponseCallback_t1836889193;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;
// PlayFab.ClientModels.AndroidDevicePushNotificationRegistrationRequest
struct AndroidDevicePushNotificationRegistrationRequest_t28777084;
// PlayFab.ClientModels.AndroidDevicePushNotificationRegistrationResult
struct AndroidDevicePushNotificationRegistrationResult_t3915401392;
// PlayFab.PlayFabError
struct PlayFabError_t750598646;
// System.IAsyncResult
struct IAsyncResult_t537683269;
// System.AsyncCallback
struct AsyncCallback_t1363551830;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_IntPtr676692020.h"
#include "mscorlib_System_String968488902.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_AndroidDevice28777084.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_AndroidDevi3915401392.h"
#include "AssemblyU2DCSharpU2Dfirstpass_PlayFab_PlayFabError750598646.h"
#include "mscorlib_System_AsyncCallback1363551830.h"

// System.Void PlayFab.PlayFabClientAPI/AndroidDevicePushNotificationRegistrationResponseCallback::.ctor(System.Object,System.IntPtr)
extern "C"  void AndroidDevicePushNotificationRegistrationResponseCallback__ctor_m1732844664 (AndroidDevicePushNotificationRegistrationResponseCallback_t1836889193 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/AndroidDevicePushNotificationRegistrationResponseCallback::Invoke(System.String,System.Int32,PlayFab.ClientModels.AndroidDevicePushNotificationRegistrationRequest,PlayFab.ClientModels.AndroidDevicePushNotificationRegistrationResult,PlayFab.PlayFabError,System.Object)
extern "C"  void AndroidDevicePushNotificationRegistrationResponseCallback_Invoke_m1611954857 (AndroidDevicePushNotificationRegistrationResponseCallback_t1836889193 * __this, String_t* ___urlPath0, int32_t ___callId1, AndroidDevicePushNotificationRegistrationRequest_t28777084 * ___request2, AndroidDevicePushNotificationRegistrationResult_t3915401392 * ___result3, PlayFabError_t750598646 * ___error4, Il2CppObject * ___customData5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
extern "C" void pinvoke_delegate_wrapper_AndroidDevicePushNotificationRegistrationResponseCallback_t1836889193(Il2CppObject* delegate, String_t* ___urlPath0, int32_t ___callId1, AndroidDevicePushNotificationRegistrationRequest_t28777084 * ___request2, AndroidDevicePushNotificationRegistrationResult_t3915401392 * ___result3, PlayFabError_t750598646 * ___error4, Il2CppObject * ___customData5);
// System.IAsyncResult PlayFab.PlayFabClientAPI/AndroidDevicePushNotificationRegistrationResponseCallback::BeginInvoke(System.String,System.Int32,PlayFab.ClientModels.AndroidDevicePushNotificationRegistrationRequest,PlayFab.ClientModels.AndroidDevicePushNotificationRegistrationResult,PlayFab.PlayFabError,System.Object,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * AndroidDevicePushNotificationRegistrationResponseCallback_BeginInvoke_m1210998742 (AndroidDevicePushNotificationRegistrationResponseCallback_t1836889193 * __this, String_t* ___urlPath0, int32_t ___callId1, AndroidDevicePushNotificationRegistrationRequest_t28777084 * ___request2, AndroidDevicePushNotificationRegistrationResult_t3915401392 * ___result3, PlayFabError_t750598646 * ___error4, Il2CppObject * ___customData5, AsyncCallback_t1363551830 * ___callback6, Il2CppObject * ___object7, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/AndroidDevicePushNotificationRegistrationResponseCallback::EndInvoke(System.IAsyncResult)
extern "C"  void AndroidDevicePushNotificationRegistrationResponseCallback_EndInvoke_m1137543816 (AndroidDevicePushNotificationRegistrationResponseCallback_t1836889193 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
