﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.ClientModels.PayForPurchaseResult
struct PayForPurchaseResult_t128200207;
// System.String
struct String_t;
// System.Collections.Generic.Dictionary`2<System.String,System.Int32>
struct Dictionary_2_t190145395;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String968488902.h"
#include "mscorlib_System_Nullable_1_gen4258606580.h"

// System.Void PlayFab.ClientModels.PayForPurchaseResult::.ctor()
extern "C"  void PayForPurchaseResult__ctor_m728317454 (PayForPurchaseResult_t128200207 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PlayFab.ClientModels.PayForPurchaseResult::get_OrderId()
extern "C"  String_t* PayForPurchaseResult_get_OrderId_m3418204463 (PayForPurchaseResult_t128200207 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.PayForPurchaseResult::set_OrderId(System.String)
extern "C"  void PayForPurchaseResult_set_OrderId_m496681962 (PayForPurchaseResult_t128200207 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Nullable`1<PlayFab.ClientModels.TransactionStatus> PlayFab.ClientModels.PayForPurchaseResult::get_Status()
extern "C"  Nullable_1_t4258606580  PayForPurchaseResult_get_Status_m4081449471 (PayForPurchaseResult_t128200207 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.PayForPurchaseResult::set_Status(System.Nullable`1<PlayFab.ClientModels.TransactionStatus>)
extern "C"  void PayForPurchaseResult_set_Status_m3509427404 (PayForPurchaseResult_t128200207 * __this, Nullable_1_t4258606580  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.Dictionary`2<System.String,System.Int32> PlayFab.ClientModels.PayForPurchaseResult::get_VCAmount()
extern "C"  Dictionary_2_t190145395 * PayForPurchaseResult_get_VCAmount_m477096607 (PayForPurchaseResult_t128200207 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.PayForPurchaseResult::set_VCAmount(System.Collections.Generic.Dictionary`2<System.String,System.Int32>)
extern "C"  void PayForPurchaseResult_set_VCAmount_m2755823852 (PayForPurchaseResult_t128200207 * __this, Dictionary_2_t190145395 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PlayFab.ClientModels.PayForPurchaseResult::get_PurchaseCurrency()
extern "C"  String_t* PayForPurchaseResult_get_PurchaseCurrency_m1417936110 (PayForPurchaseResult_t128200207 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.PayForPurchaseResult::set_PurchaseCurrency(System.String)
extern "C"  void PayForPurchaseResult_set_PurchaseCurrency_m4018301181 (PayForPurchaseResult_t128200207 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 PlayFab.ClientModels.PayForPurchaseResult::get_PurchasePrice()
extern "C"  uint32_t PayForPurchaseResult_get_PurchasePrice_m4274210150 (PayForPurchaseResult_t128200207 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.PayForPurchaseResult::set_PurchasePrice(System.UInt32)
extern "C"  void PayForPurchaseResult_set_PurchasePrice_m328917699 (PayForPurchaseResult_t128200207 * __this, uint32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 PlayFab.ClientModels.PayForPurchaseResult::get_CreditApplied()
extern "C"  uint32_t PayForPurchaseResult_get_CreditApplied_m2118280418 (PayForPurchaseResult_t128200207 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.PayForPurchaseResult::set_CreditApplied(System.UInt32)
extern "C"  void PayForPurchaseResult_set_CreditApplied_m1028902087 (PayForPurchaseResult_t128200207 * __this, uint32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PlayFab.ClientModels.PayForPurchaseResult::get_ProviderData()
extern "C"  String_t* PayForPurchaseResult_get_ProviderData_m3254285623 (PayForPurchaseResult_t128200207 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.PayForPurchaseResult::set_ProviderData(System.String)
extern "C"  void PayForPurchaseResult_set_ProviderData_m3333024724 (PayForPurchaseResult_t128200207 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PlayFab.ClientModels.PayForPurchaseResult::get_PurchaseConfirmationPageURL()
extern "C"  String_t* PayForPurchaseResult_get_PurchaseConfirmationPageURL_m42263312 (PayForPurchaseResult_t128200207 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.PayForPurchaseResult::set_PurchaseConfirmationPageURL(System.String)
extern "C"  void PayForPurchaseResult_set_PurchaseConfirmationPageURL_m2662293033 (PayForPurchaseResult_t128200207 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.Dictionary`2<System.String,System.Int32> PlayFab.ClientModels.PayForPurchaseResult::get_VirtualCurrency()
extern "C"  Dictionary_2_t190145395 * PayForPurchaseResult_get_VirtualCurrency_m2525469316 (PayForPurchaseResult_t128200207 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.PayForPurchaseResult::set_VirtualCurrency(System.Collections.Generic.Dictionary`2<System.String,System.Int32>)
extern "C"  void PayForPurchaseResult_set_VirtualCurrency_m3225110009 (PayForPurchaseResult_t128200207 * __this, Dictionary_2_t190145395 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
