﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.ClientModels.UserAccountInfo
struct UserAccountInfo_t960776096;
// System.String
struct String_t;
// PlayFab.ClientModels.UserTitleInfo
struct UserTitleInfo_t4099117963;
// PlayFab.ClientModels.UserPrivateAccountInfo
struct UserPrivateAccountInfo_t2894244435;
// PlayFab.ClientModels.UserFacebookInfo
struct UserFacebookInfo_t1491589167;
// PlayFab.ClientModels.UserSteamInfo
struct UserSteamInfo_t2867463811;
// PlayFab.ClientModels.UserGameCenterInfo
struct UserGameCenterInfo_t386684176;
// PlayFab.ClientModels.UserIosDeviceInfo
struct UserIosDeviceInfo_t3585390070;
// PlayFab.ClientModels.UserAndroidDeviceInfo
struct UserAndroidDeviceInfo_t810110808;
// PlayFab.ClientModels.UserKongregateInfo
struct UserKongregateInfo_t155682308;
// PlayFab.ClientModels.UserPsnInfo
struct UserPsnInfo_t260686782;
// PlayFab.ClientModels.UserGoogleInfo
struct UserGoogleInfo_t1689260258;
// PlayFab.ClientModels.UserXboxInfo
struct UserXboxInfo_t1624454844;
// PlayFab.ClientModels.UserCustomIdInfo
struct UserCustomIdInfo_t1941075669;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String968488902.h"
#include "mscorlib_System_DateTime339033936.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_UserTitleIn4099117963.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_UserPrivate2894244435.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_UserFaceboo1491589167.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_UserSteamIn2867463811.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_UserGameCent386684176.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_UserIosDevi3585390070.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_UserAndroidD810110808.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_UserKongrega155682308.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_UserPsnInfo260686782.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_UserGoogleI1689260258.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_UserXboxInf1624454844.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_UserCustomI1941075669.h"

// System.Void PlayFab.ClientModels.UserAccountInfo::.ctor()
extern "C"  void UserAccountInfo__ctor_m463602697 (UserAccountInfo_t960776096 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PlayFab.ClientModels.UserAccountInfo::get_PlayFabId()
extern "C"  String_t* UserAccountInfo_get_PlayFabId_m148465065 (UserAccountInfo_t960776096 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.UserAccountInfo::set_PlayFabId(System.String)
extern "C"  void UserAccountInfo_set_PlayFabId_m334997258 (UserAccountInfo_t960776096 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.DateTime PlayFab.ClientModels.UserAccountInfo::get_Created()
extern "C"  DateTime_t339033936  UserAccountInfo_get_Created_m726476397 (UserAccountInfo_t960776096 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.UserAccountInfo::set_Created(System.DateTime)
extern "C"  void UserAccountInfo_set_Created_m2688886278 (UserAccountInfo_t960776096 * __this, DateTime_t339033936  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PlayFab.ClientModels.UserAccountInfo::get_Username()
extern "C"  String_t* UserAccountInfo_get_Username_m2960291997 (UserAccountInfo_t960776096 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.UserAccountInfo::set_Username(System.String)
extern "C"  void UserAccountInfo_set_Username_m3696391252 (UserAccountInfo_t960776096 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PlayFab.ClientModels.UserTitleInfo PlayFab.ClientModels.UserAccountInfo::get_TitleInfo()
extern "C"  UserTitleInfo_t4099117963 * UserAccountInfo_get_TitleInfo_m3168204932 (UserAccountInfo_t960776096 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.UserAccountInfo::set_TitleInfo(PlayFab.ClientModels.UserTitleInfo)
extern "C"  void UserAccountInfo_set_TitleInfo_m2206070547 (UserAccountInfo_t960776096 * __this, UserTitleInfo_t4099117963 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PlayFab.ClientModels.UserPrivateAccountInfo PlayFab.ClientModels.UserAccountInfo::get_PrivateInfo()
extern "C"  UserPrivateAccountInfo_t2894244435 * UserAccountInfo_get_PrivateInfo_m649220573 (UserAccountInfo_t960776096 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.UserAccountInfo::set_PrivateInfo(PlayFab.ClientModels.UserPrivateAccountInfo)
extern "C"  void UserAccountInfo_set_PrivateInfo_m3136138070 (UserAccountInfo_t960776096 * __this, UserPrivateAccountInfo_t2894244435 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PlayFab.ClientModels.UserFacebookInfo PlayFab.ClientModels.UserAccountInfo::get_FacebookInfo()
extern "C"  UserFacebookInfo_t1491589167 * UserAccountInfo_get_FacebookInfo_m802254094 (UserAccountInfo_t960776096 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.UserAccountInfo::set_FacebookInfo(PlayFab.ClientModels.UserFacebookInfo)
extern "C"  void UserAccountInfo_set_FacebookInfo_m2969129609 (UserAccountInfo_t960776096 * __this, UserFacebookInfo_t1491589167 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PlayFab.ClientModels.UserSteamInfo PlayFab.ClientModels.UserAccountInfo::get_SteamInfo()
extern "C"  UserSteamInfo_t2867463811 * UserAccountInfo_get_SteamInfo_m3915126388 (UserAccountInfo_t960776096 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.UserAccountInfo::set_SteamInfo(PlayFab.ClientModels.UserSteamInfo)
extern "C"  void UserAccountInfo_set_SteamInfo_m168835603 (UserAccountInfo_t960776096 * __this, UserSteamInfo_t2867463811 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PlayFab.ClientModels.UserGameCenterInfo PlayFab.ClientModels.UserAccountInfo::get_GameCenterInfo()
extern "C"  UserGameCenterInfo_t386684176 * UserAccountInfo_get_GameCenterInfo_m4276187502 (UserAccountInfo_t960776096 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.UserAccountInfo::set_GameCenterInfo(PlayFab.ClientModels.UserGameCenterInfo)
extern "C"  void UserAccountInfo_set_GameCenterInfo_m3974520135 (UserAccountInfo_t960776096 * __this, UserGameCenterInfo_t386684176 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PlayFab.ClientModels.UserIosDeviceInfo PlayFab.ClientModels.UserAccountInfo::get_IosDeviceInfo()
extern "C"  UserIosDeviceInfo_t3585390070 * UserAccountInfo_get_IosDeviceInfo_m1537321050 (UserAccountInfo_t960776096 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.UserAccountInfo::set_IosDeviceInfo(PlayFab.ClientModels.UserIosDeviceInfo)
extern "C"  void UserAccountInfo_set_IosDeviceInfo_m2738764019 (UserAccountInfo_t960776096 * __this, UserIosDeviceInfo_t3585390070 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PlayFab.ClientModels.UserAndroidDeviceInfo PlayFab.ClientModels.UserAccountInfo::get_AndroidDeviceInfo()
extern "C"  UserAndroidDeviceInfo_t810110808 * UserAccountInfo_get_AndroidDeviceInfo_m3621561118 (UserAccountInfo_t960776096 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.UserAccountInfo::set_AndroidDeviceInfo(PlayFab.ClientModels.UserAndroidDeviceInfo)
extern "C"  void UserAccountInfo_set_AndroidDeviceInfo_m2208098355 (UserAccountInfo_t960776096 * __this, UserAndroidDeviceInfo_t810110808 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PlayFab.ClientModels.UserKongregateInfo PlayFab.ClientModels.UserAccountInfo::get_KongregateInfo()
extern "C"  UserKongregateInfo_t155682308 * UserAccountInfo_get_KongregateInfo_m1840831470 (UserAccountInfo_t960776096 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.UserAccountInfo::set_KongregateInfo(PlayFab.ClientModels.UserKongregateInfo)
extern "C"  void UserAccountInfo_set_KongregateInfo_m3567434847 (UserAccountInfo_t960776096 * __this, UserKongregateInfo_t155682308 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PlayFab.ClientModels.UserPsnInfo PlayFab.ClientModels.UserAccountInfo::get_PsnInfo()
extern "C"  UserPsnInfo_t260686782 * UserAccountInfo_get_PsnInfo_m762231274 (UserAccountInfo_t960776096 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.UserAccountInfo::set_PsnInfo(PlayFab.ClientModels.UserPsnInfo)
extern "C"  void UserAccountInfo_set_PsnInfo_m396905203 (UserAccountInfo_t960776096 * __this, UserPsnInfo_t260686782 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PlayFab.ClientModels.UserGoogleInfo PlayFab.ClientModels.UserAccountInfo::get_GoogleInfo()
extern "C"  UserGoogleInfo_t1689260258 * UserAccountInfo_get_GoogleInfo_m1936053678 (UserAccountInfo_t960776096 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.UserAccountInfo::set_GoogleInfo(PlayFab.ClientModels.UserGoogleInfo)
extern "C"  void UserAccountInfo_set_GoogleInfo_m2794517539 (UserAccountInfo_t960776096 * __this, UserGoogleInfo_t1689260258 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PlayFab.ClientModels.UserXboxInfo PlayFab.ClientModels.UserAccountInfo::get_XboxInfo()
extern "C"  UserXboxInfo_t1624454844 * UserAccountInfo_get_XboxInfo_m501709166 (UserAccountInfo_t960776096 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.UserAccountInfo::set_XboxInfo(PlayFab.ClientModels.UserXboxInfo)
extern "C"  void UserAccountInfo_set_XboxInfo_m1910920559 (UserAccountInfo_t960776096 * __this, UserXboxInfo_t1624454844 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PlayFab.ClientModels.UserCustomIdInfo PlayFab.ClientModels.UserAccountInfo::get_CustomIdInfo()
extern "C"  UserCustomIdInfo_t1941075669 * UserAccountInfo_get_CustomIdInfo_m3706133070 (UserAccountInfo_t960776096 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.UserAccountInfo::set_CustomIdInfo(PlayFab.ClientModels.UserCustomIdInfo)
extern "C"  void UserAccountInfo_set_CustomIdInfo_m2643221309 (UserAccountInfo_t960776096 * __this, UserCustomIdInfo_t1941075669 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
