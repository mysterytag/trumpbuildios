﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Cell_1_gen1019204052MethodDeclarations.h"

// System.Void Cell`1<ICell`1<System.Int32>>::.ctor()
#define Cell_1__ctor_m3431959539(__this, method) ((  void (*) (Cell_1_t286176100 *, const MethodInfo*))Cell_1__ctor_m1701983586_gshared)(__this, method)
// System.Void Cell`1<ICell`1<System.Int32>>::.ctor(T)
#define Cell_1__ctor_m3311531979(__this, ___initial0, method) ((  void (*) (Cell_1_t286176100 *, Il2CppObject*, const MethodInfo*))Cell_1__ctor_m1221884988_gshared)(__this, ___initial0, method)
// System.Void Cell`1<ICell`1<System.Int32>>::.ctor(ICell`1<T>)
#define Cell_1__ctor_m4253182185(__this, ___other0, method) ((  void (*) (Cell_1_t286176100 *, Il2CppObject*, const MethodInfo*))Cell_1__ctor_m3977204632_gshared)(__this, ___other0, method)
// System.Void Cell`1<ICell`1<System.Int32>>::SetInputCell(ICell`1<T>)
#define Cell_1_SetInputCell_m1521283441(__this, ___cell0, method) ((  void (*) (Cell_1_t286176100 *, Il2CppObject*, const MethodInfo*))Cell_1_SetInputCell_m3952243682_gshared)(__this, ___cell0, method)
// T Cell`1<ICell`1<System.Int32>>::get_value()
#define Cell_1_get_value_m1165203256(__this, method) ((  Il2CppObject* (*) (Cell_1_t286176100 *, const MethodInfo*))Cell_1_get_value_m916594727_gshared)(__this, method)
// System.Void Cell`1<ICell`1<System.Int32>>::set_value(T)
#define Cell_1_set_value_m1564007033(__this, ___value0, method) ((  void (*) (Cell_1_t286176100 *, Il2CppObject*, const MethodInfo*))Cell_1_set_value_m570539626_gshared)(__this, ___value0, method)
// System.Void Cell`1<ICell`1<System.Int32>>::Set(T)
#define Cell_1_Set_m201933515(__this, ___val0, method) ((  void (*) (Cell_1_t286176100 *, Il2CppObject*, const MethodInfo*))Cell_1_Set_m1115959164_gshared)(__this, ___val0, method)
// System.IDisposable Cell`1<ICell`1<System.Int32>>::OnChanged(System.Action,Priority)
#define Cell_1_OnChanged_m2161714910(__this, ___action0, ___p1, method) ((  Il2CppObject * (*) (Cell_1_t286176100 *, Action_t437523947 *, int32_t, const MethodInfo*))Cell_1_OnChanged_m3781809805_gshared)(__this, ___action0, ___p1, method)
// System.Object Cell`1<ICell`1<System.Int32>>::get_valueObject()
#define Cell_1_get_valueObject_m1493578445(__this, method) ((  Il2CppObject * (*) (Cell_1_t286176100 *, const MethodInfo*))Cell_1_get_valueObject_m2686689916_gshared)(__this, method)
// System.IDisposable Cell`1<ICell`1<System.Int32>>::ListenUpdates(System.Action`1<T>,Priority)
#define Cell_1_ListenUpdates_m1745681339(__this, ___reaction0, ___p1, method) ((  Il2CppObject * (*) (Cell_1_t286176100 *, Action_1_t252531173 *, int32_t, const MethodInfo*))Cell_1_ListenUpdates_m3691348204_gshared)(__this, ___reaction0, ___p1, method)
// IStream`1<T> Cell`1<ICell`1<System.Int32>>::get_updates()
#define Cell_1_get_updates_m406786403(__this, method) ((  Il2CppObject* (*) (Cell_1_t286176100 *, const MethodInfo*))Cell_1_get_updates_m786418834_gshared)(__this, method)
// System.IDisposable Cell`1<ICell`1<System.Int32>>::Bind(System.Action`1<T>,Priority)
#define Cell_1_Bind_m1592812583(__this, ___action0, ___p1, method) ((  Il2CppObject * (*) (Cell_1_t286176100 *, Action_1_t252531173 *, int32_t, const MethodInfo*))Cell_1_Bind_m3212907478_gshared)(__this, ___action0, ___p1, method)
// System.Void Cell`1<ICell`1<System.Int32>>::SetInputStream(IStream`1<T>)
#define Cell_1_SetInputStream_m3942042093(__this, ___stream0, method) ((  void (*) (Cell_1_t286176100 *, Il2CppObject*, const MethodInfo*))Cell_1_SetInputStream_m2944642014_gshared)(__this, ___stream0, method)
// System.Void Cell`1<ICell`1<System.Int32>>::ResetInputStream()
#define Cell_1_ResetInputStream_m3035306828(__this, method) ((  void (*) (Cell_1_t286176100 *, const MethodInfo*))Cell_1_ResetInputStream_m3069937277_gshared)(__this, method)
// System.Void Cell`1<ICell`1<System.Int32>>::TransactionIterationFinished()
#define Cell_1_TransactionIterationFinished_m2433755618(__this, method) ((  void (*) (Cell_1_t286176100 *, const MethodInfo*))Cell_1_TransactionIterationFinished_m1709986707_gshared)(__this, method)
// System.Void Cell`1<ICell`1<System.Int32>>::Unpack(System.Int32)
#define Cell_1_Unpack_m2126883700(__this, ___p0, method) ((  void (*) (Cell_1_t286176100 *, int32_t, const MethodInfo*))Cell_1_Unpack_m1047006821_gshared)(__this, ___p0, method)
// System.Boolean Cell`1<ICell`1<System.Int32>>::op_LessThanOrEqual(Cell`1<T>,UnityEngine.UI.Text)
#define Cell_1_op_LessThanOrEqual_m1493040741(__this /* static, unused */, ___cell0, ___text1, method) ((  bool (*) (Il2CppObject * /* static, unused */, Cell_1_t286176100 *, Text_t3286458198 *, const MethodInfo*))Cell_1_op_LessThanOrEqual_m4088946964_gshared)(__this /* static, unused */, ___cell0, ___text1, method)
// System.Boolean Cell`1<ICell`1<System.Int32>>::op_GreaterThanOrEqual(Cell`1<T>,UnityEngine.UI.Text)
#define Cell_1_op_GreaterThanOrEqual_m577595906(__this /* static, unused */, ___cell0, ___text1, method) ((  bool (*) (Il2CppObject * /* static, unused */, Cell_1_t286176100 *, Text_t3286458198 *, const MethodInfo*))Cell_1_op_GreaterThanOrEqual_m38753523_gshared)(__this /* static, unused */, ___cell0, ___text1, method)
