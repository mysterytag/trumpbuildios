﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Facebook.Unity.FB/<Init>c__AnonStorey53
struct U3CInitU3Ec__AnonStorey53_t1005788627;

#include "codegen/il2cpp-codegen.h"

// System.Void Facebook.Unity.FB/<Init>c__AnonStorey53::.ctor()
extern "C"  void U3CInitU3Ec__AnonStorey53__ctor_m1298557770 (U3CInitU3Ec__AnonStorey53_t1005788627 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Facebook.Unity.FB/<Init>c__AnonStorey53::<>m__D()
extern "C"  void U3CInitU3Ec__AnonStorey53_U3CU3Em__D_m1536508787 (U3CInitU3Ec__AnonStorey53_t1005788627 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Facebook.Unity.FB/<Init>c__AnonStorey53::<>m__E()
extern "C"  void U3CInitU3Ec__AnonStorey53_U3CU3Em__E_m1536509748 (U3CInitU3Ec__AnonStorey53_t1005788627 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Facebook.Unity.FB/<Init>c__AnonStorey53::<>m__F()
extern "C"  void U3CInitU3Ec__AnonStorey53_U3CU3Em__F_m1536510709 (U3CInitU3Ec__AnonStorey53_t1005788627 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Facebook.Unity.FB/<Init>c__AnonStorey53::<>m__10()
extern "C"  void U3CInitU3Ec__AnonStorey53_U3CU3Em__10_m386573810 (U3CInitU3Ec__AnonStorey53_t1005788627 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
