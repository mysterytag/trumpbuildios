﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UniRx.IObservable`1<System.Object>
struct IObservable_1_t595904784;
// System.Func`2<System.Object,UniRx.IObservable`1<System.Object>>
struct Func_2_t1894581716;

#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Oper4196218687.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Operators.ContinueWithObservable`2<System.Object,System.Object>
struct  ContinueWithObservable_2_t4007622692  : public OperatorObservableBase_1_t4196218687
{
public:
	// UniRx.IObservable`1<TSource> UniRx.Operators.ContinueWithObservable`2::source
	Il2CppObject* ___source_1;
	// System.Func`2<TSource,UniRx.IObservable`1<TResult>> UniRx.Operators.ContinueWithObservable`2::selector
	Func_2_t1894581716 * ___selector_2;

public:
	inline static int32_t get_offset_of_source_1() { return static_cast<int32_t>(offsetof(ContinueWithObservable_2_t4007622692, ___source_1)); }
	inline Il2CppObject* get_source_1() const { return ___source_1; }
	inline Il2CppObject** get_address_of_source_1() { return &___source_1; }
	inline void set_source_1(Il2CppObject* value)
	{
		___source_1 = value;
		Il2CppCodeGenWriteBarrier(&___source_1, value);
	}

	inline static int32_t get_offset_of_selector_2() { return static_cast<int32_t>(offsetof(ContinueWithObservable_2_t4007622692, ___selector_2)); }
	inline Func_2_t1894581716 * get_selector_2() const { return ___selector_2; }
	inline Func_2_t1894581716 ** get_address_of_selector_2() { return &___selector_2; }
	inline void set_selector_2(Func_2_t1894581716 * value)
	{
		___selector_2 = value;
		Il2CppCodeGenWriteBarrier(&___selector_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
