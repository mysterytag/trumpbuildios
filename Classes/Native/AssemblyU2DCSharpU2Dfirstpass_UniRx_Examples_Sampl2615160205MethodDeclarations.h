﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Examples.Sample07_OrchestratIEnumerator/<AsyncA>c__IteratorA
struct U3CAsyncAU3Ec__IteratorA_t2615160205;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void UniRx.Examples.Sample07_OrchestratIEnumerator/<AsyncA>c__IteratorA::.ctor()
extern "C"  void U3CAsyncAU3Ec__IteratorA__ctor_m2825608152 (U3CAsyncAU3Ec__IteratorA_t2615160205 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object UniRx.Examples.Sample07_OrchestratIEnumerator/<AsyncA>c__IteratorA::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CAsyncAU3Ec__IteratorA_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3934729412 (U3CAsyncAU3Ec__IteratorA_t2615160205 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object UniRx.Examples.Sample07_OrchestratIEnumerator/<AsyncA>c__IteratorA::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CAsyncAU3Ec__IteratorA_System_Collections_IEnumerator_get_Current_m3080929368 (U3CAsyncAU3Ec__IteratorA_t2615160205 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UniRx.Examples.Sample07_OrchestratIEnumerator/<AsyncA>c__IteratorA::MoveNext()
extern "C"  bool U3CAsyncAU3Ec__IteratorA_MoveNext_m3533997804 (U3CAsyncAU3Ec__IteratorA_t2615160205 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.Examples.Sample07_OrchestratIEnumerator/<AsyncA>c__IteratorA::Dispose()
extern "C"  void U3CAsyncAU3Ec__IteratorA_Dispose_m888386069 (U3CAsyncAU3Ec__IteratorA_t2615160205 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.Examples.Sample07_OrchestratIEnumerator/<AsyncA>c__IteratorA::Reset()
extern "C"  void U3CAsyncAU3Ec__IteratorA_Reset_m472041093 (U3CAsyncAU3Ec__IteratorA_t2615160205 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
