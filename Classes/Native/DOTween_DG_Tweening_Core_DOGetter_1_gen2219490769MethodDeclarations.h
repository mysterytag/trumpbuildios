﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// DG.Tweening.Core.DOGetter`1<UnityEngine.Vector2>
struct DOGetter_1_t2219490769;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t537683269;
// System.AsyncCallback
struct AsyncCallback_t1363551830;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_IntPtr676692020.h"
#include "UnityEngine_UnityEngine_Vector23525329788.h"
#include "mscorlib_System_AsyncCallback1363551830.h"

// System.Void DG.Tweening.Core.DOGetter`1<UnityEngine.Vector2>::.ctor(System.Object,System.IntPtr)
extern "C"  void DOGetter_1__ctor_m2194922925_gshared (DOGetter_1_t2219490769 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method);
#define DOGetter_1__ctor_m2194922925(__this, ___object0, ___method1, method) ((  void (*) (DOGetter_1_t2219490769 *, Il2CppObject *, IntPtr_t, const MethodInfo*))DOGetter_1__ctor_m2194922925_gshared)(__this, ___object0, ___method1, method)
// T DG.Tweening.Core.DOGetter`1<UnityEngine.Vector2>::Invoke()
extern "C"  Vector2_t3525329788  DOGetter_1_Invoke_m1502300294_gshared (DOGetter_1_t2219490769 * __this, const MethodInfo* method);
#define DOGetter_1_Invoke_m1502300294(__this, method) ((  Vector2_t3525329788  (*) (DOGetter_1_t2219490769 *, const MethodInfo*))DOGetter_1_Invoke_m1502300294_gshared)(__this, method)
// System.IAsyncResult DG.Tweening.Core.DOGetter`1<UnityEngine.Vector2>::BeginInvoke(System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * DOGetter_1_BeginInvoke_m4042989788_gshared (DOGetter_1_t2219490769 * __this, AsyncCallback_t1363551830 * ___callback0, Il2CppObject * ___object1, const MethodInfo* method);
#define DOGetter_1_BeginInvoke_m4042989788(__this, ___callback0, ___object1, method) ((  Il2CppObject * (*) (DOGetter_1_t2219490769 *, AsyncCallback_t1363551830 *, Il2CppObject *, const MethodInfo*))DOGetter_1_BeginInvoke_m4042989788_gshared)(__this, ___callback0, ___object1, method)
// T DG.Tweening.Core.DOGetter`1<UnityEngine.Vector2>::EndInvoke(System.IAsyncResult)
extern "C"  Vector2_t3525329788  DOGetter_1_EndInvoke_m3639567676_gshared (DOGetter_1_t2219490769 * __this, Il2CppObject * ___result0, const MethodInfo* method);
#define DOGetter_1_EndInvoke_m3639567676(__this, ___result0, method) ((  Vector2_t3525329788  (*) (DOGetter_1_t2219490769 *, Il2CppObject *, const MethodInfo*))DOGetter_1_EndInvoke_m3639567676_gshared)(__this, ___result0, method)
