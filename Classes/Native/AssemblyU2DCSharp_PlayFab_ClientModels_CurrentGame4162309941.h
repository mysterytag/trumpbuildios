﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Generic.List`1<PlayFab.ClientModels.GameInfo>
struct List_1_t2564353257;

#include "AssemblyU2DCSharp_PlayFab_Internal_PlayFabResultCom379512675.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PlayFab.ClientModels.CurrentGamesResult
struct  CurrentGamesResult_t4162309941  : public PlayFabResultCommon_t379512675
{
public:
	// System.Collections.Generic.List`1<PlayFab.ClientModels.GameInfo> PlayFab.ClientModels.CurrentGamesResult::<Games>k__BackingField
	List_1_t2564353257 * ___U3CGamesU3Ek__BackingField_2;
	// System.Int32 PlayFab.ClientModels.CurrentGamesResult::<PlayerCount>k__BackingField
	int32_t ___U3CPlayerCountU3Ek__BackingField_3;
	// System.Int32 PlayFab.ClientModels.CurrentGamesResult::<GameCount>k__BackingField
	int32_t ___U3CGameCountU3Ek__BackingField_4;

public:
	inline static int32_t get_offset_of_U3CGamesU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(CurrentGamesResult_t4162309941, ___U3CGamesU3Ek__BackingField_2)); }
	inline List_1_t2564353257 * get_U3CGamesU3Ek__BackingField_2() const { return ___U3CGamesU3Ek__BackingField_2; }
	inline List_1_t2564353257 ** get_address_of_U3CGamesU3Ek__BackingField_2() { return &___U3CGamesU3Ek__BackingField_2; }
	inline void set_U3CGamesU3Ek__BackingField_2(List_1_t2564353257 * value)
	{
		___U3CGamesU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier(&___U3CGamesU3Ek__BackingField_2, value);
	}

	inline static int32_t get_offset_of_U3CPlayerCountU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(CurrentGamesResult_t4162309941, ___U3CPlayerCountU3Ek__BackingField_3)); }
	inline int32_t get_U3CPlayerCountU3Ek__BackingField_3() const { return ___U3CPlayerCountU3Ek__BackingField_3; }
	inline int32_t* get_address_of_U3CPlayerCountU3Ek__BackingField_3() { return &___U3CPlayerCountU3Ek__BackingField_3; }
	inline void set_U3CPlayerCountU3Ek__BackingField_3(int32_t value)
	{
		___U3CPlayerCountU3Ek__BackingField_3 = value;
	}

	inline static int32_t get_offset_of_U3CGameCountU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(CurrentGamesResult_t4162309941, ___U3CGameCountU3Ek__BackingField_4)); }
	inline int32_t get_U3CGameCountU3Ek__BackingField_4() const { return ___U3CGameCountU3Ek__BackingField_4; }
	inline int32_t* get_address_of_U3CGameCountU3Ek__BackingField_4() { return &___U3CGameCountU3Ek__BackingField_4; }
	inline void set_U3CGameCountU3Ek__BackingField_4(int32_t value)
	{
		___U3CGameCountU3Ek__BackingField_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
