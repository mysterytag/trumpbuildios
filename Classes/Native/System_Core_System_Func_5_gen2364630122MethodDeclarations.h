﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "System_Core_System_Func_5_gen875099911MethodDeclarations.h"

// System.Void System.Func`5<System.Object,System.Object,System.AsyncCallback,System.Object,System.IAsyncResult>::.ctor(System.Object,System.IntPtr)
#define Func_5__ctor_m3902067566(__this, ___object0, ___method1, method) ((  void (*) (Func_5_t2364630122 *, Il2CppObject *, IntPtr_t, const MethodInfo*))Func_5__ctor_m2081941779_gshared)(__this, ___object0, ___method1, method)
// TResult System.Func`5<System.Object,System.Object,System.AsyncCallback,System.Object,System.IAsyncResult>::Invoke(T1,T2,T3,T4)
#define Func_5_Invoke_m3633996062(__this, ___arg10, ___arg21, ___arg32, ___arg43, method) ((  Il2CppObject * (*) (Func_5_t2364630122 *, Il2CppObject *, Il2CppObject *, AsyncCallback_t1363551830 *, Il2CppObject *, const MethodInfo*))Func_5_Invoke_m3831797397_gshared)(__this, ___arg10, ___arg21, ___arg32, ___arg43, method)
// System.IAsyncResult System.Func`5<System.Object,System.Object,System.AsyncCallback,System.Object,System.IAsyncResult>::BeginInvoke(T1,T2,T3,T4,System.AsyncCallback,System.Object)
#define Func_5_BeginInvoke_m1984417541(__this, ___arg10, ___arg21, ___arg32, ___arg43, ___callback4, ___object5, method) ((  Il2CppObject * (*) (Func_5_t2364630122 *, Il2CppObject *, Il2CppObject *, AsyncCallback_t1363551830 *, Il2CppObject *, AsyncCallback_t1363551830 *, Il2CppObject *, const MethodInfo*))Func_5_BeginInvoke_m149906808_gshared)(__this, ___arg10, ___arg21, ___arg32, ___arg43, ___callback4, ___object5, method)
// TResult System.Func`5<System.Object,System.Object,System.AsyncCallback,System.Object,System.IAsyncResult>::EndInvoke(System.IAsyncResult)
#define Func_5_EndInvoke_m539953948(__this, ___result0, method) ((  Il2CppObject * (*) (Func_5_t2364630122 *, Il2CppObject *, const MethodInfo*))Func_5_EndInvoke_m217421125_gshared)(__this, ___result0, method)
