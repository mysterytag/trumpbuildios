﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Prime31.AbstractManager
struct AbstractManager_t2874768250;
// Prime31.LifecycleHelper
struct LifecycleHelper_t3719100935;
// Prime31.ThreadingCallbackHelper
struct ThreadingCallbackHelper_t3423238746;
// UnityEngine.GameObject
struct GameObject_t4012695102;
// System.Type
struct Type_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Type2779229935.h"

// System.Void Prime31.AbstractManager::.ctor()
extern "C"  void AbstractManager__ctor_m3282926219 (AbstractManager_t2874768250 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Prime31.LifecycleHelper Prime31.AbstractManager::get_coroutineSurrogate()
extern "C"  LifecycleHelper_t3719100935 * AbstractManager_get_coroutineSurrogate_m1448002710 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Prime31.LifecycleHelper Prime31.AbstractManager::get_lifecycleHelper()
extern "C"  LifecycleHelper_t3719100935 * AbstractManager_get_lifecycleHelper_m3734877732 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Prime31.ThreadingCallbackHelper Prime31.AbstractManager::getThreadingCallbackHelper()
extern "C"  ThreadingCallbackHelper_t3423238746 * AbstractManager_getThreadingCallbackHelper_m23183125 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Prime31.AbstractManager::createThreadingCallbackHelper()
extern "C"  void AbstractManager_createThreadingCallbackHelper_m461813624 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject Prime31.AbstractManager::getPrime31ManagerGameObject()
extern "C"  GameObject_t4012695102 * AbstractManager_getPrime31ManagerGameObject_m590714625 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Prime31.AbstractManager::initialize(System.Type)
extern "C"  void AbstractManager_initialize_m1689301264 (Il2CppObject * __this /* static, unused */, Type_t * ___type0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Prime31.AbstractManager::Awake()
extern "C"  void AbstractManager_Awake_m3520531438 (AbstractManager_t2874768250 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
