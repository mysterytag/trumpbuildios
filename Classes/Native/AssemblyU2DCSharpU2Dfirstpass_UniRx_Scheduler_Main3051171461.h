﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Action`1<System.Object>
struct Action_1_t985559125;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Scheduler/MainThreadScheduler/QueuedAction`1<UniRx.Unit>
struct  QueuedAction_1_t3051171461  : public Il2CppObject
{
public:

public:
};

struct QueuedAction_1_t3051171461_StaticFields
{
public:
	// System.Action`1<System.Object> UniRx.Scheduler/MainThreadScheduler/QueuedAction`1::Instance
	Action_1_t985559125 * ___Instance_0;

public:
	inline static int32_t get_offset_of_Instance_0() { return static_cast<int32_t>(offsetof(QueuedAction_1_t3051171461_StaticFields, ___Instance_0)); }
	inline Action_1_t985559125 * get_Instance_0() const { return ___Instance_0; }
	inline Action_1_t985559125 ** get_address_of_Instance_0() { return &___Instance_0; }
	inline void set_Instance_0(Action_1_t985559125 * value)
	{
		___Instance_0 = value;
		Il2CppCodeGenWriteBarrier(&___Instance_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
