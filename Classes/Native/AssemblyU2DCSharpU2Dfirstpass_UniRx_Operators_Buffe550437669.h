﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UniRx.IObservable`1<System.Int64>
struct IObservable_1_t2606213246;

#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Oper4078052167.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Operators.BufferObservable`2<System.Int64,System.Int64>
struct  BufferObservable_2_t550437669  : public OperatorObservableBase_1_t4078052167
{
public:
	// UniRx.IObservable`1<TSource> UniRx.Operators.BufferObservable`2::source
	Il2CppObject* ___source_1;
	// UniRx.IObservable`1<TWindowBoundary> UniRx.Operators.BufferObservable`2::windowBoundaries
	Il2CppObject* ___windowBoundaries_2;

public:
	inline static int32_t get_offset_of_source_1() { return static_cast<int32_t>(offsetof(BufferObservable_2_t550437669, ___source_1)); }
	inline Il2CppObject* get_source_1() const { return ___source_1; }
	inline Il2CppObject** get_address_of_source_1() { return &___source_1; }
	inline void set_source_1(Il2CppObject* value)
	{
		___source_1 = value;
		Il2CppCodeGenWriteBarrier(&___source_1, value);
	}

	inline static int32_t get_offset_of_windowBoundaries_2() { return static_cast<int32_t>(offsetof(BufferObservable_2_t550437669, ___windowBoundaries_2)); }
	inline Il2CppObject* get_windowBoundaries_2() const { return ___windowBoundaries_2; }
	inline Il2CppObject** get_address_of_windowBoundaries_2() { return &___windowBoundaries_2; }
	inline void set_windowBoundaries_2(Il2CppObject* value)
	{
		___windowBoundaries_2 = value;
		Il2CppCodeGenWriteBarrier(&___windowBoundaries_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
