﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.PlayFabClientAPI/GetAccountInfoResponseCallback
struct GetAccountInfoResponseCallback_t1892497707;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;
// PlayFab.ClientModels.GetAccountInfoRequest
struct GetAccountInfoRequest_t2417218426;
// PlayFab.ClientModels.GetAccountInfoResult
struct GetAccountInfoResult_t3022616562;
// PlayFab.PlayFabError
struct PlayFabError_t750598646;
// System.IAsyncResult
struct IAsyncResult_t537683269;
// System.AsyncCallback
struct AsyncCallback_t1363551830;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_IntPtr676692020.h"
#include "mscorlib_System_String968488902.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_GetAccountI2417218426.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_GetAccountI3022616562.h"
#include "AssemblyU2DCSharpU2Dfirstpass_PlayFab_PlayFabError750598646.h"
#include "mscorlib_System_AsyncCallback1363551830.h"

// System.Void PlayFab.PlayFabClientAPI/GetAccountInfoResponseCallback::.ctor(System.Object,System.IntPtr)
extern "C"  void GetAccountInfoResponseCallback__ctor_m3566128282 (GetAccountInfoResponseCallback_t1892497707 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/GetAccountInfoResponseCallback::Invoke(System.String,System.Int32,PlayFab.ClientModels.GetAccountInfoRequest,PlayFab.ClientModels.GetAccountInfoResult,PlayFab.PlayFabError,System.Object)
extern "C"  void GetAccountInfoResponseCallback_Invoke_m2065424071 (GetAccountInfoResponseCallback_t1892497707 * __this, String_t* ___urlPath0, int32_t ___callId1, GetAccountInfoRequest_t2417218426 * ___request2, GetAccountInfoResult_t3022616562 * ___result3, PlayFabError_t750598646 * ___error4, Il2CppObject * ___customData5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
extern "C" void pinvoke_delegate_wrapper_GetAccountInfoResponseCallback_t1892497707(Il2CppObject* delegate, String_t* ___urlPath0, int32_t ___callId1, GetAccountInfoRequest_t2417218426 * ___request2, GetAccountInfoResult_t3022616562 * ___result3, PlayFabError_t750598646 * ___error4, Il2CppObject * ___customData5);
// System.IAsyncResult PlayFab.PlayFabClientAPI/GetAccountInfoResponseCallback::BeginInvoke(System.String,System.Int32,PlayFab.ClientModels.GetAccountInfoRequest,PlayFab.ClientModels.GetAccountInfoResult,PlayFab.PlayFabError,System.Object,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * GetAccountInfoResponseCallback_BeginInvoke_m973812148 (GetAccountInfoResponseCallback_t1892497707 * __this, String_t* ___urlPath0, int32_t ___callId1, GetAccountInfoRequest_t2417218426 * ___request2, GetAccountInfoResult_t3022616562 * ___result3, PlayFabError_t750598646 * ___error4, Il2CppObject * ___customData5, AsyncCallback_t1363551830 * ___callback6, Il2CppObject * ___object7, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/GetAccountInfoResponseCallback::EndInvoke(System.IAsyncResult)
extern "C"  void GetAccountInfoResponseCallback_EndInvoke_m1126472618 (GetAccountInfoResponseCallback_t1892497707 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
