﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.InternalUtil.DisposedObserver`1<UniRx.Tuple`2<System.Object,System.Int32>>
struct DisposedObserver_1_t1554027648;
// System.Exception
struct Exception_t1967233988;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Exception1967233988.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Tuple_2_gen2379570186.h"

// System.Void UniRx.InternalUtil.DisposedObserver`1<UniRx.Tuple`2<System.Object,System.Int32>>::.ctor()
extern "C"  void DisposedObserver_1__ctor_m1907741183_gshared (DisposedObserver_1_t1554027648 * __this, const MethodInfo* method);
#define DisposedObserver_1__ctor_m1907741183(__this, method) ((  void (*) (DisposedObserver_1_t1554027648 *, const MethodInfo*))DisposedObserver_1__ctor_m1907741183_gshared)(__this, method)
// System.Void UniRx.InternalUtil.DisposedObserver`1<UniRx.Tuple`2<System.Object,System.Int32>>::.cctor()
extern "C"  void DisposedObserver_1__cctor_m2823305614_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define DisposedObserver_1__cctor_m2823305614(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))DisposedObserver_1__cctor_m2823305614_gshared)(__this /* static, unused */, method)
// System.Void UniRx.InternalUtil.DisposedObserver`1<UniRx.Tuple`2<System.Object,System.Int32>>::OnCompleted()
extern "C"  void DisposedObserver_1_OnCompleted_m2929019465_gshared (DisposedObserver_1_t1554027648 * __this, const MethodInfo* method);
#define DisposedObserver_1_OnCompleted_m2929019465(__this, method) ((  void (*) (DisposedObserver_1_t1554027648 *, const MethodInfo*))DisposedObserver_1_OnCompleted_m2929019465_gshared)(__this, method)
// System.Void UniRx.InternalUtil.DisposedObserver`1<UniRx.Tuple`2<System.Object,System.Int32>>::OnError(System.Exception)
extern "C"  void DisposedObserver_1_OnError_m1996346870_gshared (DisposedObserver_1_t1554027648 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method);
#define DisposedObserver_1_OnError_m1996346870(__this, ___error0, method) ((  void (*) (DisposedObserver_1_t1554027648 *, Exception_t1967233988 *, const MethodInfo*))DisposedObserver_1_OnError_m1996346870_gshared)(__this, ___error0, method)
// System.Void UniRx.InternalUtil.DisposedObserver`1<UniRx.Tuple`2<System.Object,System.Int32>>::OnNext(T)
extern "C"  void DisposedObserver_1_OnNext_m2218944743_gshared (DisposedObserver_1_t1554027648 * __this, Tuple_2_t2379570186  ___value0, const MethodInfo* method);
#define DisposedObserver_1_OnNext_m2218944743(__this, ___value0, method) ((  void (*) (DisposedObserver_1_t1554027648 *, Tuple_2_t2379570186 , const MethodInfo*))DisposedObserver_1_OnNext_m2218944743_gshared)(__this, ___value0, method)
