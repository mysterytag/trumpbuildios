﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.PlayFabClientAPI/LinkKongregateRequestCallback
struct LinkKongregateRequestCallback_t2315286943;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;
// PlayFab.ClientModels.LinkKongregateAccountRequest
struct LinkKongregateAccountRequest_t2234535079;
// System.IAsyncResult
struct IAsyncResult_t537683269;
// System.AsyncCallback
struct AsyncCallback_t1363551830;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_IntPtr676692020.h"
#include "mscorlib_System_String968488902.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_LinkKongreg2234535079.h"
#include "mscorlib_System_AsyncCallback1363551830.h"

// System.Void PlayFab.PlayFabClientAPI/LinkKongregateRequestCallback::.ctor(System.Object,System.IntPtr)
extern "C"  void LinkKongregateRequestCallback__ctor_m887117486 (LinkKongregateRequestCallback_t2315286943 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/LinkKongregateRequestCallback::Invoke(System.String,System.Int32,PlayFab.ClientModels.LinkKongregateAccountRequest,System.Object)
extern "C"  void LinkKongregateRequestCallback_Invoke_m3096356258 (LinkKongregateRequestCallback_t2315286943 * __this, String_t* ___urlPath0, int32_t ___callId1, LinkKongregateAccountRequest_t2234535079 * ___request2, Il2CppObject * ___customData3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
extern "C" void pinvoke_delegate_wrapper_LinkKongregateRequestCallback_t2315286943(Il2CppObject* delegate, String_t* ___urlPath0, int32_t ___callId1, LinkKongregateAccountRequest_t2234535079 * ___request2, Il2CppObject * ___customData3);
// System.IAsyncResult PlayFab.PlayFabClientAPI/LinkKongregateRequestCallback::BeginInvoke(System.String,System.Int32,PlayFab.ClientModels.LinkKongregateAccountRequest,System.Object,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * LinkKongregateRequestCallback_BeginInvoke_m392751401 (LinkKongregateRequestCallback_t2315286943 * __this, String_t* ___urlPath0, int32_t ___callId1, LinkKongregateAccountRequest_t2234535079 * ___request2, Il2CppObject * ___customData3, AsyncCallback_t1363551830 * ___callback4, Il2CppObject * ___object5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/LinkKongregateRequestCallback::EndInvoke(System.IAsyncResult)
extern "C"  void LinkKongregateRequestCallback_EndInvoke_m3126218686 (LinkKongregateRequestCallback_t2315286943 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
