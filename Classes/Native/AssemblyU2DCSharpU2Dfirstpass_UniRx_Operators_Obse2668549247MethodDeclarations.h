﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.ObserveOnObservable`1/ObserveOn_<UniRx.Unit>
struct ObserveOn__t2668549247;
// UniRx.Operators.ObserveOnObservable`1<UniRx.Unit>
struct ObserveOnObservable_1_t3243387575;
// UniRx.ISchedulerQueueing
struct ISchedulerQueueing_t1271699701;
// UniRx.IObserver`1<UniRx.Unit>
struct IObserver_1_t475317645;
// System.IDisposable
struct IDisposable_t1628921374;
// System.Exception
struct Exception_t1967233988;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Unit2558286038.h"
#include "mscorlib_System_Exception1967233988.h"

// System.Void UniRx.Operators.ObserveOnObservable`1/ObserveOn_<UniRx.Unit>::.ctor(UniRx.Operators.ObserveOnObservable`1<T>,UniRx.ISchedulerQueueing,UniRx.IObserver`1<T>,System.IDisposable)
extern "C"  void ObserveOn___ctor_m172035631_gshared (ObserveOn__t2668549247 * __this, ObserveOnObservable_1_t3243387575 * ___parent0, Il2CppObject * ___scheduler1, Il2CppObject* ___observer2, Il2CppObject * ___cancel3, const MethodInfo* method);
#define ObserveOn___ctor_m172035631(__this, ___parent0, ___scheduler1, ___observer2, ___cancel3, method) ((  void (*) (ObserveOn__t2668549247 *, ObserveOnObservable_1_t3243387575 *, Il2CppObject *, Il2CppObject*, Il2CppObject *, const MethodInfo*))ObserveOn___ctor_m172035631_gshared)(__this, ___parent0, ___scheduler1, ___observer2, ___cancel3, method)
// System.IDisposable UniRx.Operators.ObserveOnObservable`1/ObserveOn_<UniRx.Unit>::Run()
extern "C"  Il2CppObject * ObserveOn__Run_m3473698140_gshared (ObserveOn__t2668549247 * __this, const MethodInfo* method);
#define ObserveOn__Run_m3473698140(__this, method) ((  Il2CppObject * (*) (ObserveOn__t2668549247 *, const MethodInfo*))ObserveOn__Run_m3473698140_gshared)(__this, method)
// System.Void UniRx.Operators.ObserveOnObservable`1/ObserveOn_<UniRx.Unit>::OnNext_(T)
extern "C"  void ObserveOn__OnNext__m181846373_gshared (ObserveOn__t2668549247 * __this, Unit_t2558286038  ___value0, const MethodInfo* method);
#define ObserveOn__OnNext__m181846373(__this, ___value0, method) ((  void (*) (ObserveOn__t2668549247 *, Unit_t2558286038 , const MethodInfo*))ObserveOn__OnNext__m181846373_gshared)(__this, ___value0, method)
// System.Void UniRx.Operators.ObserveOnObservable`1/ObserveOn_<UniRx.Unit>::OnError_(System.Exception)
extern "C"  void ObserveOn__OnError__m1977977724_gshared (ObserveOn__t2668549247 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method);
#define ObserveOn__OnError__m1977977724(__this, ___error0, method) ((  void (*) (ObserveOn__t2668549247 *, Exception_t1967233988 *, const MethodInfo*))ObserveOn__OnError__m1977977724_gshared)(__this, ___error0, method)
// System.Void UniRx.Operators.ObserveOnObservable`1/ObserveOn_<UniRx.Unit>::OnCompleted_(UniRx.Unit)
extern "C"  void ObserveOn__OnCompleted__m3391733709_gshared (ObserveOn__t2668549247 * __this, Unit_t2558286038  ____0, const MethodInfo* method);
#define ObserveOn__OnCompleted__m3391733709(__this, ____0, method) ((  void (*) (ObserveOn__t2668549247 *, Unit_t2558286038 , const MethodInfo*))ObserveOn__OnCompleted__m3391733709_gshared)(__this, ____0, method)
// System.Void UniRx.Operators.ObserveOnObservable`1/ObserveOn_<UniRx.Unit>::OnNext(T)
extern "C"  void ObserveOn__OnNext_m1391287798_gshared (ObserveOn__t2668549247 * __this, Unit_t2558286038  ___value0, const MethodInfo* method);
#define ObserveOn__OnNext_m1391287798(__this, ___value0, method) ((  void (*) (ObserveOn__t2668549247 *, Unit_t2558286038 , const MethodInfo*))ObserveOn__OnNext_m1391287798_gshared)(__this, ___value0, method)
// System.Void UniRx.Operators.ObserveOnObservable`1/ObserveOn_<UniRx.Unit>::OnError(System.Exception)
extern "C"  void ObserveOn__OnError_m4245867781_gshared (ObserveOn__t2668549247 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method);
#define ObserveOn__OnError_m4245867781(__this, ___error0, method) ((  void (*) (ObserveOn__t2668549247 *, Exception_t1967233988 *, const MethodInfo*))ObserveOn__OnError_m4245867781_gshared)(__this, ___error0, method)
// System.Void UniRx.Operators.ObserveOnObservable`1/ObserveOn_<UniRx.Unit>::OnCompleted()
extern "C"  void ObserveOn__OnCompleted_m2509316056_gshared (ObserveOn__t2668549247 * __this, const MethodInfo* method);
#define ObserveOn__OnCompleted_m2509316056(__this, method) ((  void (*) (ObserveOn__t2668549247 *, const MethodInfo*))ObserveOn__OnCompleted_m2509316056_gshared)(__this, method)
