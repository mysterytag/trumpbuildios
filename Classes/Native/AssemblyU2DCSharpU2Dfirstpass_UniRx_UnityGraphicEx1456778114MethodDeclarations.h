﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.UnityGraphicExtensions/<DirtyVerticesCallbackAsObservable>c__AnonStoreyA2/<DirtyVerticesCallbackAsObservable>c__AnonStoreyA1
struct U3CDirtyVerticesCallbackAsObservableU3Ec__AnonStoreyA1_t1456778114;

#include "codegen/il2cpp-codegen.h"

// System.Void UniRx.UnityGraphicExtensions/<DirtyVerticesCallbackAsObservable>c__AnonStoreyA2/<DirtyVerticesCallbackAsObservable>c__AnonStoreyA1::.ctor()
extern "C"  void U3CDirtyVerticesCallbackAsObservableU3Ec__AnonStoreyA1__ctor_m189882513 (U3CDirtyVerticesCallbackAsObservableU3Ec__AnonStoreyA1_t1456778114 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.UnityGraphicExtensions/<DirtyVerticesCallbackAsObservable>c__AnonStoreyA2/<DirtyVerticesCallbackAsObservable>c__AnonStoreyA1::<>m__DF()
extern "C"  void U3CDirtyVerticesCallbackAsObservableU3Ec__AnonStoreyA1_U3CU3Em__DF_m102128412 (U3CDirtyVerticesCallbackAsObservableU3Ec__AnonStoreyA1_t1456778114 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.UnityGraphicExtensions/<DirtyVerticesCallbackAsObservable>c__AnonStoreyA2/<DirtyVerticesCallbackAsObservable>c__AnonStoreyA1::<>m__E0()
extern "C"  void U3CDirtyVerticesCallbackAsObservableU3Ec__AnonStoreyA1_U3CU3Em__E0_m102137061 (U3CDirtyVerticesCallbackAsObservableU3Ec__AnonStoreyA1_t1456778114 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
