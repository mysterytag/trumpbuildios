﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.ReactiveProperty`1/ReactivePropertyObserver<System.Int64>
struct ReactivePropertyObserver_t3083397451;
// UniRx.ReactiveProperty`1<System.Int64>
struct ReactiveProperty_1_t3455747920;
// System.Exception
struct Exception_t1967233988;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Exception1967233988.h"

// System.Void UniRx.ReactiveProperty`1/ReactivePropertyObserver<System.Int64>::.ctor(UniRx.ReactiveProperty`1<T>)
extern "C"  void ReactivePropertyObserver__ctor_m1020887772_gshared (ReactivePropertyObserver_t3083397451 * __this, ReactiveProperty_1_t3455747920 * ___parent0, const MethodInfo* method);
#define ReactivePropertyObserver__ctor_m1020887772(__this, ___parent0, method) ((  void (*) (ReactivePropertyObserver_t3083397451 *, ReactiveProperty_1_t3455747920 *, const MethodInfo*))ReactivePropertyObserver__ctor_m1020887772_gshared)(__this, ___parent0, method)
// System.Void UniRx.ReactiveProperty`1/ReactivePropertyObserver<System.Int64>::OnNext(T)
extern "C"  void ReactivePropertyObserver_OnNext_m1984423759_gshared (ReactivePropertyObserver_t3083397451 * __this, int64_t ___value0, const MethodInfo* method);
#define ReactivePropertyObserver_OnNext_m1984423759(__this, ___value0, method) ((  void (*) (ReactivePropertyObserver_t3083397451 *, int64_t, const MethodInfo*))ReactivePropertyObserver_OnNext_m1984423759_gshared)(__this, ___value0, method)
// System.Void UniRx.ReactiveProperty`1/ReactivePropertyObserver<System.Int64>::OnError(System.Exception)
extern "C"  void ReactivePropertyObserver_OnError_m524911710_gshared (ReactivePropertyObserver_t3083397451 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method);
#define ReactivePropertyObserver_OnError_m524911710(__this, ___error0, method) ((  void (*) (ReactivePropertyObserver_t3083397451 *, Exception_t1967233988 *, const MethodInfo*))ReactivePropertyObserver_OnError_m524911710_gshared)(__this, ___error0, method)
// System.Void UniRx.ReactiveProperty`1/ReactivePropertyObserver<System.Int64>::OnCompleted()
extern "C"  void ReactivePropertyObserver_OnCompleted_m191190193_gshared (ReactivePropertyObserver_t3083397451 * __this, const MethodInfo* method);
#define ReactivePropertyObserver_OnCompleted_m191190193(__this, method) ((  void (*) (ReactivePropertyObserver_t3083397451 *, const MethodInfo*))ReactivePropertyObserver_OnCompleted_m191190193_gshared)(__this, method)
