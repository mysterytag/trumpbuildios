﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.ReadOnlyReactiveProperty`1/ReadOnlyReactivePropertyObserver<System.Object>
struct ReadOnlyReactivePropertyObserver_t4124161295;
// UniRx.ReadOnlyReactiveProperty`1<System.Object>
struct ReadOnlyReactiveProperty_1_t3626508436;
// System.Object
struct Il2CppObject;
// System.Exception
struct Exception_t1967233988;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_Exception1967233988.h"

// System.Void UniRx.ReadOnlyReactiveProperty`1/ReadOnlyReactivePropertyObserver<System.Object>::.ctor(UniRx.ReadOnlyReactiveProperty`1<T>)
extern "C"  void ReadOnlyReactivePropertyObserver__ctor_m855956758_gshared (ReadOnlyReactivePropertyObserver_t4124161295 * __this, ReadOnlyReactiveProperty_1_t3626508436 * ___parent0, const MethodInfo* method);
#define ReadOnlyReactivePropertyObserver__ctor_m855956758(__this, ___parent0, method) ((  void (*) (ReadOnlyReactivePropertyObserver_t4124161295 *, ReadOnlyReactiveProperty_1_t3626508436 *, const MethodInfo*))ReadOnlyReactivePropertyObserver__ctor_m855956758_gshared)(__this, ___parent0, method)
// System.Void UniRx.ReadOnlyReactiveProperty`1/ReadOnlyReactivePropertyObserver<System.Object>::OnNext(T)
extern "C"  void ReadOnlyReactivePropertyObserver_OnNext_m458355991_gshared (ReadOnlyReactivePropertyObserver_t4124161295 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define ReadOnlyReactivePropertyObserver_OnNext_m458355991(__this, ___value0, method) ((  void (*) (ReadOnlyReactivePropertyObserver_t4124161295 *, Il2CppObject *, const MethodInfo*))ReadOnlyReactivePropertyObserver_OnNext_m458355991_gshared)(__this, ___value0, method)
// System.Void UniRx.ReadOnlyReactiveProperty`1/ReadOnlyReactivePropertyObserver<System.Object>::OnError(System.Exception)
extern "C"  void ReadOnlyReactivePropertyObserver_OnError_m357499430_gshared (ReadOnlyReactivePropertyObserver_t4124161295 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method);
#define ReadOnlyReactivePropertyObserver_OnError_m357499430(__this, ___error0, method) ((  void (*) (ReadOnlyReactivePropertyObserver_t4124161295 *, Exception_t1967233988 *, const MethodInfo*))ReadOnlyReactivePropertyObserver_OnError_m357499430_gshared)(__this, ___error0, method)
// System.Void UniRx.ReadOnlyReactiveProperty`1/ReadOnlyReactivePropertyObserver<System.Object>::OnCompleted()
extern "C"  void ReadOnlyReactivePropertyObserver_OnCompleted_m3718463097_gshared (ReadOnlyReactivePropertyObserver_t4124161295 * __this, const MethodInfo* method);
#define ReadOnlyReactivePropertyObserver_OnCompleted_m3718463097(__this, method) ((  void (*) (ReadOnlyReactivePropertyObserver_t4124161295 *, const MethodInfo*))ReadOnlyReactivePropertyObserver_OnCompleted_m3718463097_gshared)(__this, method)
