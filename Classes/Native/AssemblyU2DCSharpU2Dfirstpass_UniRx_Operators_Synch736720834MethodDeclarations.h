﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.SynchronizeObservable`1<System.Object>
struct SynchronizeObservable_1_t736720834;
// UniRx.IObservable`1<System.Object>
struct IObservable_1_t595904784;
// System.Object
struct Il2CppObject;
// System.IDisposable
struct IDisposable_t1628921374;
// UniRx.IObserver`1<System.Object>
struct IObserver_1_t3049105323;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"

// System.Void UniRx.Operators.SynchronizeObservable`1<System.Object>::.ctor(UniRx.IObservable`1<T>,System.Object)
extern "C"  void SynchronizeObservable_1__ctor_m1075160077_gshared (SynchronizeObservable_1_t736720834 * __this, Il2CppObject* ___source0, Il2CppObject * ___gate1, const MethodInfo* method);
#define SynchronizeObservable_1__ctor_m1075160077(__this, ___source0, ___gate1, method) ((  void (*) (SynchronizeObservable_1_t736720834 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))SynchronizeObservable_1__ctor_m1075160077_gshared)(__this, ___source0, ___gate1, method)
// System.IDisposable UniRx.Operators.SynchronizeObservable`1<System.Object>::SubscribeCore(UniRx.IObserver`1<T>,System.IDisposable)
extern "C"  Il2CppObject * SynchronizeObservable_1_SubscribeCore_m1097568574_gshared (SynchronizeObservable_1_t736720834 * __this, Il2CppObject* ___observer0, Il2CppObject * ___cancel1, const MethodInfo* method);
#define SynchronizeObservable_1_SubscribeCore_m1097568574(__this, ___observer0, ___cancel1, method) ((  Il2CppObject * (*) (SynchronizeObservable_1_t736720834 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))SynchronizeObservable_1_SubscribeCore_m1097568574_gshared)(__this, ___observer0, ___cancel1, method)
