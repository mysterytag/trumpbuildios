﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.OperatorObservableBase`1<UniRx.TimeInterval`1<System.Object>>
struct OperatorObservableBase_1_t4067177809;
// System.IDisposable
struct IDisposable_t1628921374;
// UniRx.IObserver`1<UniRx.TimeInterval`1<System.Object>>
struct IObserver_1_t2920064445;

#include "codegen/il2cpp-codegen.h"

// System.Void UniRx.Operators.OperatorObservableBase`1<UniRx.TimeInterval`1<System.Object>>::.ctor(System.Boolean)
extern "C"  void OperatorObservableBase_1__ctor_m1003778261_gshared (OperatorObservableBase_1_t4067177809 * __this, bool ___isRequiredSubscribeOnCurrentThread0, const MethodInfo* method);
#define OperatorObservableBase_1__ctor_m1003778261(__this, ___isRequiredSubscribeOnCurrentThread0, method) ((  void (*) (OperatorObservableBase_1_t4067177809 *, bool, const MethodInfo*))OperatorObservableBase_1__ctor_m1003778261_gshared)(__this, ___isRequiredSubscribeOnCurrentThread0, method)
// System.Boolean UniRx.Operators.OperatorObservableBase`1<UniRx.TimeInterval`1<System.Object>>::IsRequiredSubscribeOnCurrentThread()
extern "C"  bool OperatorObservableBase_1_IsRequiredSubscribeOnCurrentThread_m2877731245_gshared (OperatorObservableBase_1_t4067177809 * __this, const MethodInfo* method);
#define OperatorObservableBase_1_IsRequiredSubscribeOnCurrentThread_m2877731245(__this, method) ((  bool (*) (OperatorObservableBase_1_t4067177809 *, const MethodInfo*))OperatorObservableBase_1_IsRequiredSubscribeOnCurrentThread_m2877731245_gshared)(__this, method)
// System.IDisposable UniRx.Operators.OperatorObservableBase`1<UniRx.TimeInterval`1<System.Object>>::Subscribe(UniRx.IObserver`1<T>)
extern "C"  Il2CppObject * OperatorObservableBase_1_Subscribe_m3230356083_gshared (OperatorObservableBase_1_t4067177809 * __this, Il2CppObject* ___observer0, const MethodInfo* method);
#define OperatorObservableBase_1_Subscribe_m3230356083(__this, ___observer0, method) ((  Il2CppObject * (*) (OperatorObservableBase_1_t4067177809 *, Il2CppObject*, const MethodInfo*))OperatorObservableBase_1_Subscribe_m3230356083_gshared)(__this, ___observer0, method)
