﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.TakeWhileObservable`1/TakeWhile<System.Object>
struct TakeWhile_t3591352755;
// UniRx.Operators.TakeWhileObservable`1<System.Object>
struct TakeWhileObservable_1_t3525448012;
// UniRx.IObserver`1<System.Object>
struct IObserver_1_t3049105323;
// System.IDisposable
struct IDisposable_t1628921374;
// System.Object
struct Il2CppObject;
// System.Exception
struct Exception_t1967233988;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_Exception1967233988.h"

// System.Void UniRx.Operators.TakeWhileObservable`1/TakeWhile<System.Object>::.ctor(UniRx.Operators.TakeWhileObservable`1<T>,UniRx.IObserver`1<T>,System.IDisposable)
extern "C"  void TakeWhile__ctor_m2870044218_gshared (TakeWhile_t3591352755 * __this, TakeWhileObservable_1_t3525448012 * ___parent0, Il2CppObject* ___observer1, Il2CppObject * ___cancel2, const MethodInfo* method);
#define TakeWhile__ctor_m2870044218(__this, ___parent0, ___observer1, ___cancel2, method) ((  void (*) (TakeWhile_t3591352755 *, TakeWhileObservable_1_t3525448012 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))TakeWhile__ctor_m2870044218_gshared)(__this, ___parent0, ___observer1, ___cancel2, method)
// System.IDisposable UniRx.Operators.TakeWhileObservable`1/TakeWhile<System.Object>::Run()
extern "C"  Il2CppObject * TakeWhile_Run_m201623875_gshared (TakeWhile_t3591352755 * __this, const MethodInfo* method);
#define TakeWhile_Run_m201623875(__this, method) ((  Il2CppObject * (*) (TakeWhile_t3591352755 *, const MethodInfo*))TakeWhile_Run_m201623875_gshared)(__this, method)
// System.Void UniRx.Operators.TakeWhileObservable`1/TakeWhile<System.Object>::OnNext(T)
extern "C"  void TakeWhile_OnNext_m980988829_gshared (TakeWhile_t3591352755 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define TakeWhile_OnNext_m980988829(__this, ___value0, method) ((  void (*) (TakeWhile_t3591352755 *, Il2CppObject *, const MethodInfo*))TakeWhile_OnNext_m980988829_gshared)(__this, ___value0, method)
// System.Void UniRx.Operators.TakeWhileObservable`1/TakeWhile<System.Object>::OnError(System.Exception)
extern "C"  void TakeWhile_OnError_m1948513452_gshared (TakeWhile_t3591352755 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method);
#define TakeWhile_OnError_m1948513452(__this, ___error0, method) ((  void (*) (TakeWhile_t3591352755 *, Exception_t1967233988 *, const MethodInfo*))TakeWhile_OnError_m1948513452_gshared)(__this, ___error0, method)
// System.Void UniRx.Operators.TakeWhileObservable`1/TakeWhile<System.Object>::OnCompleted()
extern "C"  void TakeWhile_OnCompleted_m1989888511_gshared (TakeWhile_t3591352755 * __this, const MethodInfo* method);
#define TakeWhile_OnCompleted_m1989888511(__this, method) ((  void (*) (TakeWhile_t3591352755 *, const MethodInfo*))TakeWhile_OnCompleted_m1989888511_gshared)(__this, method)
