﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.ObserveOnObservable`1/ObserveOn<UniRx.Unit>
struct ObserveOn_t679350430;
// UniRx.Operators.ObserveOnObservable`1<UniRx.Unit>
struct ObserveOnObservable_1_t3243387575;
// UniRx.IObserver`1<UniRx.Unit>
struct IObserver_1_t475317645;
// System.IDisposable
struct IDisposable_t1628921374;
// System.Exception
struct Exception_t1967233988;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Unit2558286038.h"
#include "mscorlib_System_Exception1967233988.h"

// System.Void UniRx.Operators.ObserveOnObservable`1/ObserveOn<UniRx.Unit>::.ctor(UniRx.Operators.ObserveOnObservable`1<T>,UniRx.IObserver`1<T>,System.IDisposable)
extern "C"  void ObserveOn__ctor_m368439203_gshared (ObserveOn_t679350430 * __this, ObserveOnObservable_1_t3243387575 * ___parent0, Il2CppObject* ___observer1, Il2CppObject * ___cancel2, const MethodInfo* method);
#define ObserveOn__ctor_m368439203(__this, ___parent0, ___observer1, ___cancel2, method) ((  void (*) (ObserveOn_t679350430 *, ObserveOnObservable_1_t3243387575 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))ObserveOn__ctor_m368439203_gshared)(__this, ___parent0, ___observer1, ___cancel2, method)
// System.IDisposable UniRx.Operators.ObserveOnObservable`1/ObserveOn<UniRx.Unit>::Run()
extern "C"  Il2CppObject * ObserveOn_Run_m2434727249_gshared (ObserveOn_t679350430 * __this, const MethodInfo* method);
#define ObserveOn_Run_m2434727249(__this, method) ((  Il2CppObject * (*) (ObserveOn_t679350430 *, const MethodInfo*))ObserveOn_Run_m2434727249_gshared)(__this, method)
// System.Void UniRx.Operators.ObserveOnObservable`1/ObserveOn<UniRx.Unit>::OnNext(T)
extern "C"  void ObserveOn_OnNext_m2580489429_gshared (ObserveOn_t679350430 * __this, Unit_t2558286038  ___value0, const MethodInfo* method);
#define ObserveOn_OnNext_m2580489429(__this, ___value0, method) ((  void (*) (ObserveOn_t679350430 *, Unit_t2558286038 , const MethodInfo*))ObserveOn_OnNext_m2580489429_gshared)(__this, ___value0, method)
// System.Void UniRx.Operators.ObserveOnObservable`1/ObserveOn<UniRx.Unit>::OnError(System.Exception)
extern "C"  void ObserveOn_OnError_m127989220_gshared (ObserveOn_t679350430 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method);
#define ObserveOn_OnError_m127989220(__this, ___error0, method) ((  void (*) (ObserveOn_t679350430 *, Exception_t1967233988 *, const MethodInfo*))ObserveOn_OnError_m127989220_gshared)(__this, ___error0, method)
// System.Void UniRx.Operators.ObserveOnObservable`1/ObserveOn<UniRx.Unit>::OnCompleted()
extern "C"  void ObserveOn_OnCompleted_m1986420535_gshared (ObserveOn_t679350430 * __this, const MethodInfo* method);
#define ObserveOn_OnCompleted_m1986420535(__this, method) ((  void (*) (ObserveOn_t679350430 *, const MethodInfo*))ObserveOn_OnCompleted_m1986420535_gshared)(__this, method)
// System.Void UniRx.Operators.ObserveOnObservable`1/ObserveOn<UniRx.Unit>::<Run>m__7D()
extern "C"  void ObserveOn_U3CRunU3Em__7D_m3860964226_gshared (ObserveOn_t679350430 * __this, const MethodInfo* method);
#define ObserveOn_U3CRunU3Em__7D_m3860964226(__this, method) ((  void (*) (ObserveOn_t679350430 *, const MethodInfo*))ObserveOn_U3CRunU3Em__7D_m3860964226_gshared)(__this, method)
