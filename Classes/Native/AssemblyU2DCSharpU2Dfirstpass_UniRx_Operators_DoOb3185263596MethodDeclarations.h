﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.DoObserverObservable`1/Do<System.Object>
struct Do_t3185263596;
// UniRx.Operators.DoObserverObservable`1<System.Object>
struct DoObserverObservable_1_t2771775371;
// UniRx.IObserver`1<System.Object>
struct IObserver_1_t3049105323;
// System.IDisposable
struct IDisposable_t1628921374;
// System.Object
struct Il2CppObject;
// System.Exception
struct Exception_t1967233988;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_Exception1967233988.h"

// System.Void UniRx.Operators.DoObserverObservable`1/Do<System.Object>::.ctor(UniRx.Operators.DoObserverObservable`1<T>,UniRx.IObserver`1<T>,System.IDisposable)
extern "C"  void Do__ctor_m1276205467_gshared (Do_t3185263596 * __this, DoObserverObservable_1_t2771775371 * ___parent0, Il2CppObject* ___observer1, Il2CppObject * ___cancel2, const MethodInfo* method);
#define Do__ctor_m1276205467(__this, ___parent0, ___observer1, ___cancel2, method) ((  void (*) (Do_t3185263596 *, DoObserverObservable_1_t2771775371 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))Do__ctor_m1276205467_gshared)(__this, ___parent0, ___observer1, ___cancel2, method)
// System.IDisposable UniRx.Operators.DoObserverObservable`1/Do<System.Object>::Run()
extern "C"  Il2CppObject * Do_Run_m1848628975_gshared (Do_t3185263596 * __this, const MethodInfo* method);
#define Do_Run_m1848628975(__this, method) ((  Il2CppObject * (*) (Do_t3185263596 *, const MethodInfo*))Do_Run_m1848628975_gshared)(__this, method)
// System.Void UniRx.Operators.DoObserverObservable`1/Do<System.Object>::OnNext(T)
extern "C"  void Do_OnNext_m2253638537_gshared (Do_t3185263596 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Do_OnNext_m2253638537(__this, ___value0, method) ((  void (*) (Do_t3185263596 *, Il2CppObject *, const MethodInfo*))Do_OnNext_m2253638537_gshared)(__this, ___value0, method)
// System.Void UniRx.Operators.DoObserverObservable`1/Do<System.Object>::OnError(System.Exception)
extern "C"  void Do_OnError_m1233761432_gshared (Do_t3185263596 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method);
#define Do_OnError_m1233761432(__this, ___error0, method) ((  void (*) (Do_t3185263596 *, Exception_t1967233988 *, const MethodInfo*))Do_OnError_m1233761432_gshared)(__this, ___error0, method)
// System.Void UniRx.Operators.DoObserverObservable`1/Do<System.Object>::OnCompleted()
extern "C"  void Do_OnCompleted_m2920319979_gshared (Do_t3185263596 * __this, const MethodInfo* method);
#define Do_OnCompleted_m2920319979(__this, method) ((  void (*) (Do_t3185263596 *, const MethodInfo*))Do_OnCompleted_m2920319979_gshared)(__this, method)
