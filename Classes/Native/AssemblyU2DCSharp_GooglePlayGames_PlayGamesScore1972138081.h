﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GooglePlayGames.PlayGamesScore
struct  PlayGamesScore_t1972138081  : public Il2CppObject
{
public:
	// System.String GooglePlayGames.PlayGamesScore::mLbId
	String_t* ___mLbId_0;
	// System.Int64 GooglePlayGames.PlayGamesScore::mValue
	int64_t ___mValue_1;

public:
	inline static int32_t get_offset_of_mLbId_0() { return static_cast<int32_t>(offsetof(PlayGamesScore_t1972138081, ___mLbId_0)); }
	inline String_t* get_mLbId_0() const { return ___mLbId_0; }
	inline String_t** get_address_of_mLbId_0() { return &___mLbId_0; }
	inline void set_mLbId_0(String_t* value)
	{
		___mLbId_0 = value;
		Il2CppCodeGenWriteBarrier(&___mLbId_0, value);
	}

	inline static int32_t get_offset_of_mValue_1() { return static_cast<int32_t>(offsetof(PlayGamesScore_t1972138081, ___mValue_1)); }
	inline int64_t get_mValue_1() const { return ___mValue_1; }
	inline int64_t* get_address_of_mValue_1() { return &___mValue_1; }
	inline void set_mValue_1(int64_t value)
	{
		___mValue_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
