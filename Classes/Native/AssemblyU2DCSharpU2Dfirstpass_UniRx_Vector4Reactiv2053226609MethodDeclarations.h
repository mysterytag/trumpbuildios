﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Vector4ReactiveProperty
struct Vector4ReactiveProperty_t2053226609;
// System.Collections.Generic.IEqualityComparer`1<UnityEngine.Vector4>
struct IEqualityComparer_1_t1554629145;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Vector43525329790.h"

// System.Void UniRx.Vector4ReactiveProperty::.ctor()
extern "C"  void Vector4ReactiveProperty__ctor_m1746441360 (Vector4ReactiveProperty_t2053226609 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.Vector4ReactiveProperty::.ctor(UnityEngine.Vector4)
extern "C"  void Vector4ReactiveProperty__ctor_m1563865448 (Vector4ReactiveProperty_t2053226609 * __this, Vector4_t3525329790  ___initialValue0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.IEqualityComparer`1<UnityEngine.Vector4> UniRx.Vector4ReactiveProperty::get_EqualityComparer()
extern "C"  Il2CppObject* Vector4ReactiveProperty_get_EqualityComparer_m691891231 (Vector4ReactiveProperty_t2053226609 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
