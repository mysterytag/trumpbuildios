﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.PlayFabClientAPI/<UnlinkIOSDeviceID>c__AnonStorey85
struct U3CUnlinkIOSDeviceIDU3Ec__AnonStorey85_t724665589;
// PlayFab.CallRequestContainer
struct CallRequestContainer_t432318609;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharpU2Dfirstpass_PlayFab_CallRequestCo432318609.h"

// System.Void PlayFab.PlayFabClientAPI/<UnlinkIOSDeviceID>c__AnonStorey85::.ctor()
extern "C"  void U3CUnlinkIOSDeviceIDU3Ec__AnonStorey85__ctor_m3234032126 (U3CUnlinkIOSDeviceIDU3Ec__AnonStorey85_t724665589 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/<UnlinkIOSDeviceID>c__AnonStorey85::<>m__72(PlayFab.CallRequestContainer)
extern "C"  void U3CUnlinkIOSDeviceIDU3Ec__AnonStorey85_U3CU3Em__72_m4021042679 (U3CUnlinkIOSDeviceIDU3Ec__AnonStorey85_t724665589 * __this, CallRequestContainer_t432318609 * ___requestContainer0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
