﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Examples.Sample06_ConvertToCoroutine/<TestNewCustomYieldInstruction>c__Iterator9
struct U3CTestNewCustomYieldInstructionU3Ec__Iterator9_t2930300994;
// System.Object
struct Il2CppObject;
// UnityEngine.Transform
struct Transform_t284553113;
// UniRx.Examples.Sample06_ConvertToCoroutine
struct Sample06_ConvertToCoroutine_t617332884;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Examples_Sample617332884.h"
#include "UnityEngine_UnityEngine_Transform284553113.h"

// System.Void UniRx.Examples.Sample06_ConvertToCoroutine/<TestNewCustomYieldInstruction>c__Iterator9::.ctor()
extern "C"  void U3CTestNewCustomYieldInstructionU3Ec__Iterator9__ctor_m1174907504 (U3CTestNewCustomYieldInstructionU3Ec__Iterator9_t2930300994 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object UniRx.Examples.Sample06_ConvertToCoroutine/<TestNewCustomYieldInstruction>c__Iterator9::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CTestNewCustomYieldInstructionU3Ec__Iterator9_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3558467756 (U3CTestNewCustomYieldInstructionU3Ec__Iterator9_t2930300994 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object UniRx.Examples.Sample06_ConvertToCoroutine/<TestNewCustomYieldInstruction>c__Iterator9::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CTestNewCustomYieldInstructionU3Ec__Iterator9_System_Collections_IEnumerator_get_Current_m2292892736 (U3CTestNewCustomYieldInstructionU3Ec__Iterator9_t2930300994 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UniRx.Examples.Sample06_ConvertToCoroutine/<TestNewCustomYieldInstruction>c__Iterator9::MoveNext()
extern "C"  bool U3CTestNewCustomYieldInstructionU3Ec__Iterator9_MoveNext_m3094489940 (U3CTestNewCustomYieldInstructionU3Ec__Iterator9_t2930300994 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.Examples.Sample06_ConvertToCoroutine/<TestNewCustomYieldInstruction>c__Iterator9::Dispose()
extern "C"  void U3CTestNewCustomYieldInstructionU3Ec__Iterator9_Dispose_m3702962861 (U3CTestNewCustomYieldInstructionU3Ec__Iterator9_t2930300994 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.Examples.Sample06_ConvertToCoroutine/<TestNewCustomYieldInstruction>c__Iterator9::Reset()
extern "C"  void U3CTestNewCustomYieldInstructionU3Ec__Iterator9_Reset_m3116307741 (U3CTestNewCustomYieldInstructionU3Ec__Iterator9_t2930300994 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Transform UniRx.Examples.Sample06_ConvertToCoroutine/<TestNewCustomYieldInstruction>c__Iterator9::<>m__1F(UniRx.Examples.Sample06_ConvertToCoroutine)
extern "C"  Transform_t284553113 * U3CTestNewCustomYieldInstructionU3Ec__Iterator9_U3CU3Em__1F_m2124214084 (Il2CppObject * __this /* static, unused */, Sample06_ConvertToCoroutine_t617332884 * ___x0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UniRx.Examples.Sample06_ConvertToCoroutine/<TestNewCustomYieldInstruction>c__Iterator9::<>m__20(UnityEngine.Transform)
extern "C"  bool U3CTestNewCustomYieldInstructionU3Ec__Iterator9_U3CU3Em__20_m4040796226 (Il2CppObject * __this /* static, unused */, Transform_t284553113 * ___x0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
