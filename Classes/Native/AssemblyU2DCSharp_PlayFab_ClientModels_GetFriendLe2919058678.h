﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_Nullable_1_gen1438485399.h"
#include "mscorlib_System_Nullable_1_gen3097043249.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PlayFab.ClientModels.GetFriendLeaderboardRequest
struct  GetFriendLeaderboardRequest_t2919058678  : public Il2CppObject
{
public:
	// System.String PlayFab.ClientModels.GetFriendLeaderboardRequest::<StatisticName>k__BackingField
	String_t* ___U3CStatisticNameU3Ek__BackingField_0;
	// System.Int32 PlayFab.ClientModels.GetFriendLeaderboardRequest::<StartPosition>k__BackingField
	int32_t ___U3CStartPositionU3Ek__BackingField_1;
	// System.Nullable`1<System.Int32> PlayFab.ClientModels.GetFriendLeaderboardRequest::<MaxResultsCount>k__BackingField
	Nullable_1_t1438485399  ___U3CMaxResultsCountU3Ek__BackingField_2;
	// System.Nullable`1<System.Boolean> PlayFab.ClientModels.GetFriendLeaderboardRequest::<IncludeSteamFriends>k__BackingField
	Nullable_1_t3097043249  ___U3CIncludeSteamFriendsU3Ek__BackingField_3;
	// System.Nullable`1<System.Boolean> PlayFab.ClientModels.GetFriendLeaderboardRequest::<IncludeFacebookFriends>k__BackingField
	Nullable_1_t3097043249  ___U3CIncludeFacebookFriendsU3Ek__BackingField_4;

public:
	inline static int32_t get_offset_of_U3CStatisticNameU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(GetFriendLeaderboardRequest_t2919058678, ___U3CStatisticNameU3Ek__BackingField_0)); }
	inline String_t* get_U3CStatisticNameU3Ek__BackingField_0() const { return ___U3CStatisticNameU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CStatisticNameU3Ek__BackingField_0() { return &___U3CStatisticNameU3Ek__BackingField_0; }
	inline void set_U3CStatisticNameU3Ek__BackingField_0(String_t* value)
	{
		___U3CStatisticNameU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier(&___U3CStatisticNameU3Ek__BackingField_0, value);
	}

	inline static int32_t get_offset_of_U3CStartPositionU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(GetFriendLeaderboardRequest_t2919058678, ___U3CStartPositionU3Ek__BackingField_1)); }
	inline int32_t get_U3CStartPositionU3Ek__BackingField_1() const { return ___U3CStartPositionU3Ek__BackingField_1; }
	inline int32_t* get_address_of_U3CStartPositionU3Ek__BackingField_1() { return &___U3CStartPositionU3Ek__BackingField_1; }
	inline void set_U3CStartPositionU3Ek__BackingField_1(int32_t value)
	{
		___U3CStartPositionU3Ek__BackingField_1 = value;
	}

	inline static int32_t get_offset_of_U3CMaxResultsCountU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(GetFriendLeaderboardRequest_t2919058678, ___U3CMaxResultsCountU3Ek__BackingField_2)); }
	inline Nullable_1_t1438485399  get_U3CMaxResultsCountU3Ek__BackingField_2() const { return ___U3CMaxResultsCountU3Ek__BackingField_2; }
	inline Nullable_1_t1438485399 * get_address_of_U3CMaxResultsCountU3Ek__BackingField_2() { return &___U3CMaxResultsCountU3Ek__BackingField_2; }
	inline void set_U3CMaxResultsCountU3Ek__BackingField_2(Nullable_1_t1438485399  value)
	{
		___U3CMaxResultsCountU3Ek__BackingField_2 = value;
	}

	inline static int32_t get_offset_of_U3CIncludeSteamFriendsU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(GetFriendLeaderboardRequest_t2919058678, ___U3CIncludeSteamFriendsU3Ek__BackingField_3)); }
	inline Nullable_1_t3097043249  get_U3CIncludeSteamFriendsU3Ek__BackingField_3() const { return ___U3CIncludeSteamFriendsU3Ek__BackingField_3; }
	inline Nullable_1_t3097043249 * get_address_of_U3CIncludeSteamFriendsU3Ek__BackingField_3() { return &___U3CIncludeSteamFriendsU3Ek__BackingField_3; }
	inline void set_U3CIncludeSteamFriendsU3Ek__BackingField_3(Nullable_1_t3097043249  value)
	{
		___U3CIncludeSteamFriendsU3Ek__BackingField_3 = value;
	}

	inline static int32_t get_offset_of_U3CIncludeFacebookFriendsU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(GetFriendLeaderboardRequest_t2919058678, ___U3CIncludeFacebookFriendsU3Ek__BackingField_4)); }
	inline Nullable_1_t3097043249  get_U3CIncludeFacebookFriendsU3Ek__BackingField_4() const { return ___U3CIncludeFacebookFriendsU3Ek__BackingField_4; }
	inline Nullable_1_t3097043249 * get_address_of_U3CIncludeFacebookFriendsU3Ek__BackingField_4() { return &___U3CIncludeFacebookFriendsU3Ek__BackingField_4; }
	inline void set_U3CIncludeFacebookFriendsU3Ek__BackingField_4(Nullable_1_t3097043249  value)
	{
		___U3CIncludeFacebookFriendsU3Ek__BackingField_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
