﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.InternalUtil.DisposedObserver`1<System.Int64>
struct DisposedObserver_1_t2021872344;
// System.Exception
struct Exception_t1967233988;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Exception1967233988.h"

// System.Void UniRx.InternalUtil.DisposedObserver`1<System.Int64>::.ctor()
extern "C"  void DisposedObserver_1__ctor_m1407102496_gshared (DisposedObserver_1_t2021872344 * __this, const MethodInfo* method);
#define DisposedObserver_1__ctor_m1407102496(__this, method) ((  void (*) (DisposedObserver_1_t2021872344 *, const MethodInfo*))DisposedObserver_1__ctor_m1407102496_gshared)(__this, method)
// System.Void UniRx.InternalUtil.DisposedObserver`1<System.Int64>::.cctor()
extern "C"  void DisposedObserver_1__cctor_m188408205_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define DisposedObserver_1__cctor_m188408205(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))DisposedObserver_1__cctor_m188408205_gshared)(__this /* static, unused */, method)
// System.Void UniRx.InternalUtil.DisposedObserver`1<System.Int64>::OnCompleted()
extern "C"  void DisposedObserver_1_OnCompleted_m4283877802_gshared (DisposedObserver_1_t2021872344 * __this, const MethodInfo* method);
#define DisposedObserver_1_OnCompleted_m4283877802(__this, method) ((  void (*) (DisposedObserver_1_t2021872344 *, const MethodInfo*))DisposedObserver_1_OnCompleted_m4283877802_gshared)(__this, method)
// System.Void UniRx.InternalUtil.DisposedObserver`1<System.Int64>::OnError(System.Exception)
extern "C"  void DisposedObserver_1_OnError_m236866519_gshared (DisposedObserver_1_t2021872344 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method);
#define DisposedObserver_1_OnError_m236866519(__this, ___error0, method) ((  void (*) (DisposedObserver_1_t2021872344 *, Exception_t1967233988 *, const MethodInfo*))DisposedObserver_1_OnError_m236866519_gshared)(__this, ___error0, method)
// System.Void UniRx.InternalUtil.DisposedObserver`1<System.Int64>::OnNext(T)
extern "C"  void DisposedObserver_1_OnNext_m2141503688_gshared (DisposedObserver_1_t2021872344 * __this, int64_t ___value0, const MethodInfo* method);
#define DisposedObserver_1_OnNext_m2141503688(__this, ___value0, method) ((  void (*) (DisposedObserver_1_t2021872344 *, int64_t, const MethodInfo*))DisposedObserver_1_OnNext_m2141503688_gshared)(__this, ___value0, method)
