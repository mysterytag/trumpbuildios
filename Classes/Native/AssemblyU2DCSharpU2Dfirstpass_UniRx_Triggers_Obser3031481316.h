﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UniRx.Subject`1<UnityEngine.Collider>
struct Subject_1_t2893705245;

#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Triggers_Obser1883237895.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Triggers.ObservableTriggerTrigger
struct  ObservableTriggerTrigger_t3031481316  : public ObservableTriggerBase_t1883237895
{
public:
	// UniRx.Subject`1<UnityEngine.Collider> UniRx.Triggers.ObservableTriggerTrigger::onTriggerEnter
	Subject_1_t2893705245 * ___onTriggerEnter_8;
	// UniRx.Subject`1<UnityEngine.Collider> UniRx.Triggers.ObservableTriggerTrigger::onTriggerExit
	Subject_1_t2893705245 * ___onTriggerExit_9;
	// UniRx.Subject`1<UnityEngine.Collider> UniRx.Triggers.ObservableTriggerTrigger::onTriggerStay
	Subject_1_t2893705245 * ___onTriggerStay_10;

public:
	inline static int32_t get_offset_of_onTriggerEnter_8() { return static_cast<int32_t>(offsetof(ObservableTriggerTrigger_t3031481316, ___onTriggerEnter_8)); }
	inline Subject_1_t2893705245 * get_onTriggerEnter_8() const { return ___onTriggerEnter_8; }
	inline Subject_1_t2893705245 ** get_address_of_onTriggerEnter_8() { return &___onTriggerEnter_8; }
	inline void set_onTriggerEnter_8(Subject_1_t2893705245 * value)
	{
		___onTriggerEnter_8 = value;
		Il2CppCodeGenWriteBarrier(&___onTriggerEnter_8, value);
	}

	inline static int32_t get_offset_of_onTriggerExit_9() { return static_cast<int32_t>(offsetof(ObservableTriggerTrigger_t3031481316, ___onTriggerExit_9)); }
	inline Subject_1_t2893705245 * get_onTriggerExit_9() const { return ___onTriggerExit_9; }
	inline Subject_1_t2893705245 ** get_address_of_onTriggerExit_9() { return &___onTriggerExit_9; }
	inline void set_onTriggerExit_9(Subject_1_t2893705245 * value)
	{
		___onTriggerExit_9 = value;
		Il2CppCodeGenWriteBarrier(&___onTriggerExit_9, value);
	}

	inline static int32_t get_offset_of_onTriggerStay_10() { return static_cast<int32_t>(offsetof(ObservableTriggerTrigger_t3031481316, ___onTriggerStay_10)); }
	inline Subject_1_t2893705245 * get_onTriggerStay_10() const { return ___onTriggerStay_10; }
	inline Subject_1_t2893705245 ** get_address_of_onTriggerStay_10() { return &___onTriggerStay_10; }
	inline void set_onTriggerStay_10(Subject_1_t2893705245 * value)
	{
		___onTriggerStay_10 = value;
		Il2CppCodeGenWriteBarrier(&___onTriggerStay_10, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
