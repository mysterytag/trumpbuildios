﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.SelectManyObservable`3/SelectManyOuterObserver<System.Object,System.Int64,System.Object>
struct SelectManyOuterObserver_t29170705;
// UniRx.Operators.SelectManyObservable`3<System.Object,System.Int64,System.Object>
struct SelectManyObservable_3_t744723289;
// UniRx.IObserver`1<System.Object>
struct IObserver_1_t3049105323;
// System.IDisposable
struct IDisposable_t1628921374;
// System.Object
struct Il2CppObject;
// System.Exception
struct Exception_t1967233988;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_Exception1967233988.h"

// System.Void UniRx.Operators.SelectManyObservable`3/SelectManyOuterObserver<System.Object,System.Int64,System.Object>::.ctor(UniRx.Operators.SelectManyObservable`3<TSource,TCollection,TResult>,UniRx.IObserver`1<TResult>,System.IDisposable)
extern "C"  void SelectManyOuterObserver__ctor_m1128632853_gshared (SelectManyOuterObserver_t29170705 * __this, SelectManyObservable_3_t744723289 * ___parent0, Il2CppObject* ___observer1, Il2CppObject * ___cancel2, const MethodInfo* method);
#define SelectManyOuterObserver__ctor_m1128632853(__this, ___parent0, ___observer1, ___cancel2, method) ((  void (*) (SelectManyOuterObserver_t29170705 *, SelectManyObservable_3_t744723289 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))SelectManyOuterObserver__ctor_m1128632853_gshared)(__this, ___parent0, ___observer1, ___cancel2, method)
// System.IDisposable UniRx.Operators.SelectManyObservable`3/SelectManyOuterObserver<System.Object,System.Int64,System.Object>::Run()
extern "C"  Il2CppObject * SelectManyOuterObserver_Run_m1493694714_gshared (SelectManyOuterObserver_t29170705 * __this, const MethodInfo* method);
#define SelectManyOuterObserver_Run_m1493694714(__this, method) ((  Il2CppObject * (*) (SelectManyOuterObserver_t29170705 *, const MethodInfo*))SelectManyOuterObserver_Run_m1493694714_gshared)(__this, method)
// System.Void UniRx.Operators.SelectManyObservable`3/SelectManyOuterObserver<System.Object,System.Int64,System.Object>::OnNext(TSource)
extern "C"  void SelectManyOuterObserver_OnNext_m4209706233_gshared (SelectManyOuterObserver_t29170705 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define SelectManyOuterObserver_OnNext_m4209706233(__this, ___value0, method) ((  void (*) (SelectManyOuterObserver_t29170705 *, Il2CppObject *, const MethodInfo*))SelectManyOuterObserver_OnNext_m4209706233_gshared)(__this, ___value0, method)
// System.Void UniRx.Operators.SelectManyObservable`3/SelectManyOuterObserver<System.Object,System.Int64,System.Object>::OnError(System.Exception)
extern "C"  void SelectManyOuterObserver_OnError_m320905507_gshared (SelectManyOuterObserver_t29170705 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method);
#define SelectManyOuterObserver_OnError_m320905507(__this, ___error0, method) ((  void (*) (SelectManyOuterObserver_t29170705 *, Exception_t1967233988 *, const MethodInfo*))SelectManyOuterObserver_OnError_m320905507_gshared)(__this, ___error0, method)
// System.Void UniRx.Operators.SelectManyObservable`3/SelectManyOuterObserver<System.Object,System.Int64,System.Object>::OnCompleted()
extern "C"  void SelectManyOuterObserver_OnCompleted_m1737863414_gshared (SelectManyOuterObserver_t29170705 * __this, const MethodInfo* method);
#define SelectManyOuterObserver_OnCompleted_m1737863414(__this, method) ((  void (*) (SelectManyOuterObserver_t29170705 *, const MethodInfo*))SelectManyOuterObserver_OnCompleted_m1737863414_gshared)(__this, method)
