﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// UniRx.IProgress`1<System.Single>
struct IProgress_1_t4173802291;

#include "mscorlib_System_Object837106420.h"
#include "UnityEngine_UnityEngine_Hash1283885020822.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.ObservableWWW/<LoadFromCacheOrDownload>c__AnonStorey90
struct  U3CLoadFromCacheOrDownloadU3Ec__AnonStorey90_t1059502561  : public Il2CppObject
{
public:
	// System.String UniRx.ObservableWWW/<LoadFromCacheOrDownload>c__AnonStorey90::url
	String_t* ___url_0;
	// UnityEngine.Hash128 UniRx.ObservableWWW/<LoadFromCacheOrDownload>c__AnonStorey90::hash128
	Hash128_t3885020822  ___hash128_1;
	// System.UInt32 UniRx.ObservableWWW/<LoadFromCacheOrDownload>c__AnonStorey90::crc
	uint32_t ___crc_2;
	// UniRx.IProgress`1<System.Single> UniRx.ObservableWWW/<LoadFromCacheOrDownload>c__AnonStorey90::progress
	Il2CppObject* ___progress_3;

public:
	inline static int32_t get_offset_of_url_0() { return static_cast<int32_t>(offsetof(U3CLoadFromCacheOrDownloadU3Ec__AnonStorey90_t1059502561, ___url_0)); }
	inline String_t* get_url_0() const { return ___url_0; }
	inline String_t** get_address_of_url_0() { return &___url_0; }
	inline void set_url_0(String_t* value)
	{
		___url_0 = value;
		Il2CppCodeGenWriteBarrier(&___url_0, value);
	}

	inline static int32_t get_offset_of_hash128_1() { return static_cast<int32_t>(offsetof(U3CLoadFromCacheOrDownloadU3Ec__AnonStorey90_t1059502561, ___hash128_1)); }
	inline Hash128_t3885020822  get_hash128_1() const { return ___hash128_1; }
	inline Hash128_t3885020822 * get_address_of_hash128_1() { return &___hash128_1; }
	inline void set_hash128_1(Hash128_t3885020822  value)
	{
		___hash128_1 = value;
	}

	inline static int32_t get_offset_of_crc_2() { return static_cast<int32_t>(offsetof(U3CLoadFromCacheOrDownloadU3Ec__AnonStorey90_t1059502561, ___crc_2)); }
	inline uint32_t get_crc_2() const { return ___crc_2; }
	inline uint32_t* get_address_of_crc_2() { return &___crc_2; }
	inline void set_crc_2(uint32_t value)
	{
		___crc_2 = value;
	}

	inline static int32_t get_offset_of_progress_3() { return static_cast<int32_t>(offsetof(U3CLoadFromCacheOrDownloadU3Ec__AnonStorey90_t1059502561, ___progress_3)); }
	inline Il2CppObject* get_progress_3() const { return ___progress_3; }
	inline Il2CppObject** get_address_of_progress_3() { return &___progress_3; }
	inline void set_progress_3(Il2CppObject* value)
	{
		___progress_3 = value;
		Il2CppCodeGenWriteBarrier(&___progress_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
