﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.PlayFabClientAPI/AndroidDevicePushNotificationRegistrationRequestCallback
struct AndroidDevicePushNotificationRegistrationRequestCallback_t642279121;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;
// PlayFab.ClientModels.AndroidDevicePushNotificationRegistrationRequest
struct AndroidDevicePushNotificationRegistrationRequest_t28777084;
// System.IAsyncResult
struct IAsyncResult_t537683269;
// System.AsyncCallback
struct AsyncCallback_t1363551830;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_IntPtr676692020.h"
#include "mscorlib_System_String968488902.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_AndroidDevice28777084.h"
#include "mscorlib_System_AsyncCallback1363551830.h"

// System.Void PlayFab.PlayFabClientAPI/AndroidDevicePushNotificationRegistrationRequestCallback::.ctor(System.Object,System.IntPtr)
extern "C"  void AndroidDevicePushNotificationRegistrationRequestCallback__ctor_m3855876928 (AndroidDevicePushNotificationRegistrationRequestCallback_t642279121 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/AndroidDevicePushNotificationRegistrationRequestCallback::Invoke(System.String,System.Int32,PlayFab.ClientModels.AndroidDevicePushNotificationRegistrationRequest,System.Object)
extern "C"  void AndroidDevicePushNotificationRegistrationRequestCallback_Invoke_m2763337151 (AndroidDevicePushNotificationRegistrationRequestCallback_t642279121 * __this, String_t* ___urlPath0, int32_t ___callId1, AndroidDevicePushNotificationRegistrationRequest_t28777084 * ___request2, Il2CppObject * ___customData3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
extern "C" void pinvoke_delegate_wrapper_AndroidDevicePushNotificationRegistrationRequestCallback_t642279121(Il2CppObject* delegate, String_t* ___urlPath0, int32_t ___callId1, AndroidDevicePushNotificationRegistrationRequest_t28777084 * ___request2, Il2CppObject * ___customData3);
// System.IAsyncResult PlayFab.PlayFabClientAPI/AndroidDevicePushNotificationRegistrationRequestCallback::BeginInvoke(System.String,System.Int32,PlayFab.ClientModels.AndroidDevicePushNotificationRegistrationRequest,System.Object,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * AndroidDevicePushNotificationRegistrationRequestCallback_BeginInvoke_m3328802888 (AndroidDevicePushNotificationRegistrationRequestCallback_t642279121 * __this, String_t* ___urlPath0, int32_t ___callId1, AndroidDevicePushNotificationRegistrationRequest_t28777084 * ___request2, Il2CppObject * ___customData3, AsyncCallback_t1363551830 * ___callback4, Il2CppObject * ___object5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/AndroidDevicePushNotificationRegistrationRequestCallback::EndInvoke(System.IAsyncResult)
extern "C"  void AndroidDevicePushNotificationRegistrationRequestCallback_EndInvoke_m355145040 (AndroidDevicePushNotificationRegistrationRequestCallback_t642279121 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
