﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CellReactiveApi/<WhenOnce>c__AnonStorey107`1/<WhenOnce>c__AnonStorey109`1<System.Object>
struct U3CWhenOnceU3Ec__AnonStorey109_1_t1841049228;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"

// System.Void CellReactiveApi/<WhenOnce>c__AnonStorey107`1/<WhenOnce>c__AnonStorey109`1<System.Object>::.ctor()
extern "C"  void U3CWhenOnceU3Ec__AnonStorey109_1__ctor_m1065343744_gshared (U3CWhenOnceU3Ec__AnonStorey109_1_t1841049228 * __this, const MethodInfo* method);
#define U3CWhenOnceU3Ec__AnonStorey109_1__ctor_m1065343744(__this, method) ((  void (*) (U3CWhenOnceU3Ec__AnonStorey109_1_t1841049228 *, const MethodInfo*))U3CWhenOnceU3Ec__AnonStorey109_1__ctor_m1065343744_gshared)(__this, method)
// System.Void CellReactiveApi/<WhenOnce>c__AnonStorey107`1/<WhenOnce>c__AnonStorey109`1<System.Object>::<>m__194(T)
extern "C"  void U3CWhenOnceU3Ec__AnonStorey109_1_U3CU3Em__194_m3813401785_gshared (U3CWhenOnceU3Ec__AnonStorey109_1_t1841049228 * __this, Il2CppObject * ___val0, const MethodInfo* method);
#define U3CWhenOnceU3Ec__AnonStorey109_1_U3CU3Em__194_m3813401785(__this, ___val0, method) ((  void (*) (U3CWhenOnceU3Ec__AnonStorey109_1_t1841049228 *, Il2CppObject *, const MethodInfo*))U3CWhenOnceU3Ec__AnonStorey109_1_U3CU3Em__194_m3813401785_gshared)(__this, ___val0, method)
