﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Tacticsoft.ScrollRectWithDrag
struct ScrollRectWithDrag_t261095137;
// UnityEngine.EventSystems.PointerEventData
struct PointerEventData_t3205101634;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_PointerEve3205101634.h"

// System.Void Tacticsoft.ScrollRectWithDrag::.ctor()
extern "C"  void ScrollRectWithDrag__ctor_m1792704180 (ScrollRectWithDrag_t261095137 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Tacticsoft.ScrollRectWithDrag::OnDrag(UnityEngine.EventSystems.PointerEventData)
extern "C"  void ScrollRectWithDrag_OnDrag_m1019549083 (ScrollRectWithDrag_t261095137 * __this, PointerEventData_t3205101634 * ___eventData0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Tacticsoft.ScrollRectWithDrag::OnEndDrag(UnityEngine.EventSystems.PointerEventData)
extern "C"  void ScrollRectWithDrag_OnEndDrag_m3333840540 (ScrollRectWithDrag_t261095137 * __this, PointerEventData_t3205101634 * ___eventData0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Tacticsoft.ScrollRectWithDrag::Awake()
extern "C"  void ScrollRectWithDrag_Awake_m2030309399 (ScrollRectWithDrag_t261095137 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
