﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// PlayFab.ClientModels.UserAccountInfo
struct UserAccountInfo_t960776096;
// System.Collections.Generic.List`1<PlayFab.ClientModels.ItemInstance>
struct List_1_t1322103281;
// System.Collections.Generic.Dictionary`2<System.String,System.Int32>
struct Dictionary_2_t190145395;
// System.Collections.Generic.Dictionary`2<System.String,PlayFab.ClientModels.VirtualCurrencyRechargeTime>
struct Dictionary_2_t3222006224;
// System.Collections.Generic.Dictionary`2<System.String,PlayFab.ClientModels.UserDataRecord>
struct Dictionary_2_t1801537638;

#include "AssemblyU2DCSharp_PlayFab_Internal_PlayFabResultCom379512675.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PlayFab.ClientModels.GetUserCombinedInfoResult
struct  GetUserCombinedInfoResult_t1115681121  : public PlayFabResultCommon_t379512675
{
public:
	// System.String PlayFab.ClientModels.GetUserCombinedInfoResult::<PlayFabId>k__BackingField
	String_t* ___U3CPlayFabIdU3Ek__BackingField_2;
	// PlayFab.ClientModels.UserAccountInfo PlayFab.ClientModels.GetUserCombinedInfoResult::<AccountInfo>k__BackingField
	UserAccountInfo_t960776096 * ___U3CAccountInfoU3Ek__BackingField_3;
	// System.Collections.Generic.List`1<PlayFab.ClientModels.ItemInstance> PlayFab.ClientModels.GetUserCombinedInfoResult::<Inventory>k__BackingField
	List_1_t1322103281 * ___U3CInventoryU3Ek__BackingField_4;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> PlayFab.ClientModels.GetUserCombinedInfoResult::<VirtualCurrency>k__BackingField
	Dictionary_2_t190145395 * ___U3CVirtualCurrencyU3Ek__BackingField_5;
	// System.Collections.Generic.Dictionary`2<System.String,PlayFab.ClientModels.VirtualCurrencyRechargeTime> PlayFab.ClientModels.GetUserCombinedInfoResult::<VirtualCurrencyRechargeTimes>k__BackingField
	Dictionary_2_t3222006224 * ___U3CVirtualCurrencyRechargeTimesU3Ek__BackingField_6;
	// System.Collections.Generic.Dictionary`2<System.String,PlayFab.ClientModels.UserDataRecord> PlayFab.ClientModels.GetUserCombinedInfoResult::<Data>k__BackingField
	Dictionary_2_t1801537638 * ___U3CDataU3Ek__BackingField_7;
	// System.UInt32 PlayFab.ClientModels.GetUserCombinedInfoResult::<DataVersion>k__BackingField
	uint32_t ___U3CDataVersionU3Ek__BackingField_8;
	// System.Collections.Generic.Dictionary`2<System.String,PlayFab.ClientModels.UserDataRecord> PlayFab.ClientModels.GetUserCombinedInfoResult::<ReadOnlyData>k__BackingField
	Dictionary_2_t1801537638 * ___U3CReadOnlyDataU3Ek__BackingField_9;
	// System.UInt32 PlayFab.ClientModels.GetUserCombinedInfoResult::<ReadOnlyDataVersion>k__BackingField
	uint32_t ___U3CReadOnlyDataVersionU3Ek__BackingField_10;

public:
	inline static int32_t get_offset_of_U3CPlayFabIdU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(GetUserCombinedInfoResult_t1115681121, ___U3CPlayFabIdU3Ek__BackingField_2)); }
	inline String_t* get_U3CPlayFabIdU3Ek__BackingField_2() const { return ___U3CPlayFabIdU3Ek__BackingField_2; }
	inline String_t** get_address_of_U3CPlayFabIdU3Ek__BackingField_2() { return &___U3CPlayFabIdU3Ek__BackingField_2; }
	inline void set_U3CPlayFabIdU3Ek__BackingField_2(String_t* value)
	{
		___U3CPlayFabIdU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier(&___U3CPlayFabIdU3Ek__BackingField_2, value);
	}

	inline static int32_t get_offset_of_U3CAccountInfoU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(GetUserCombinedInfoResult_t1115681121, ___U3CAccountInfoU3Ek__BackingField_3)); }
	inline UserAccountInfo_t960776096 * get_U3CAccountInfoU3Ek__BackingField_3() const { return ___U3CAccountInfoU3Ek__BackingField_3; }
	inline UserAccountInfo_t960776096 ** get_address_of_U3CAccountInfoU3Ek__BackingField_3() { return &___U3CAccountInfoU3Ek__BackingField_3; }
	inline void set_U3CAccountInfoU3Ek__BackingField_3(UserAccountInfo_t960776096 * value)
	{
		___U3CAccountInfoU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier(&___U3CAccountInfoU3Ek__BackingField_3, value);
	}

	inline static int32_t get_offset_of_U3CInventoryU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(GetUserCombinedInfoResult_t1115681121, ___U3CInventoryU3Ek__BackingField_4)); }
	inline List_1_t1322103281 * get_U3CInventoryU3Ek__BackingField_4() const { return ___U3CInventoryU3Ek__BackingField_4; }
	inline List_1_t1322103281 ** get_address_of_U3CInventoryU3Ek__BackingField_4() { return &___U3CInventoryU3Ek__BackingField_4; }
	inline void set_U3CInventoryU3Ek__BackingField_4(List_1_t1322103281 * value)
	{
		___U3CInventoryU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier(&___U3CInventoryU3Ek__BackingField_4, value);
	}

	inline static int32_t get_offset_of_U3CVirtualCurrencyU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(GetUserCombinedInfoResult_t1115681121, ___U3CVirtualCurrencyU3Ek__BackingField_5)); }
	inline Dictionary_2_t190145395 * get_U3CVirtualCurrencyU3Ek__BackingField_5() const { return ___U3CVirtualCurrencyU3Ek__BackingField_5; }
	inline Dictionary_2_t190145395 ** get_address_of_U3CVirtualCurrencyU3Ek__BackingField_5() { return &___U3CVirtualCurrencyU3Ek__BackingField_5; }
	inline void set_U3CVirtualCurrencyU3Ek__BackingField_5(Dictionary_2_t190145395 * value)
	{
		___U3CVirtualCurrencyU3Ek__BackingField_5 = value;
		Il2CppCodeGenWriteBarrier(&___U3CVirtualCurrencyU3Ek__BackingField_5, value);
	}

	inline static int32_t get_offset_of_U3CVirtualCurrencyRechargeTimesU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(GetUserCombinedInfoResult_t1115681121, ___U3CVirtualCurrencyRechargeTimesU3Ek__BackingField_6)); }
	inline Dictionary_2_t3222006224 * get_U3CVirtualCurrencyRechargeTimesU3Ek__BackingField_6() const { return ___U3CVirtualCurrencyRechargeTimesU3Ek__BackingField_6; }
	inline Dictionary_2_t3222006224 ** get_address_of_U3CVirtualCurrencyRechargeTimesU3Ek__BackingField_6() { return &___U3CVirtualCurrencyRechargeTimesU3Ek__BackingField_6; }
	inline void set_U3CVirtualCurrencyRechargeTimesU3Ek__BackingField_6(Dictionary_2_t3222006224 * value)
	{
		___U3CVirtualCurrencyRechargeTimesU3Ek__BackingField_6 = value;
		Il2CppCodeGenWriteBarrier(&___U3CVirtualCurrencyRechargeTimesU3Ek__BackingField_6, value);
	}

	inline static int32_t get_offset_of_U3CDataU3Ek__BackingField_7() { return static_cast<int32_t>(offsetof(GetUserCombinedInfoResult_t1115681121, ___U3CDataU3Ek__BackingField_7)); }
	inline Dictionary_2_t1801537638 * get_U3CDataU3Ek__BackingField_7() const { return ___U3CDataU3Ek__BackingField_7; }
	inline Dictionary_2_t1801537638 ** get_address_of_U3CDataU3Ek__BackingField_7() { return &___U3CDataU3Ek__BackingField_7; }
	inline void set_U3CDataU3Ek__BackingField_7(Dictionary_2_t1801537638 * value)
	{
		___U3CDataU3Ek__BackingField_7 = value;
		Il2CppCodeGenWriteBarrier(&___U3CDataU3Ek__BackingField_7, value);
	}

	inline static int32_t get_offset_of_U3CDataVersionU3Ek__BackingField_8() { return static_cast<int32_t>(offsetof(GetUserCombinedInfoResult_t1115681121, ___U3CDataVersionU3Ek__BackingField_8)); }
	inline uint32_t get_U3CDataVersionU3Ek__BackingField_8() const { return ___U3CDataVersionU3Ek__BackingField_8; }
	inline uint32_t* get_address_of_U3CDataVersionU3Ek__BackingField_8() { return &___U3CDataVersionU3Ek__BackingField_8; }
	inline void set_U3CDataVersionU3Ek__BackingField_8(uint32_t value)
	{
		___U3CDataVersionU3Ek__BackingField_8 = value;
	}

	inline static int32_t get_offset_of_U3CReadOnlyDataU3Ek__BackingField_9() { return static_cast<int32_t>(offsetof(GetUserCombinedInfoResult_t1115681121, ___U3CReadOnlyDataU3Ek__BackingField_9)); }
	inline Dictionary_2_t1801537638 * get_U3CReadOnlyDataU3Ek__BackingField_9() const { return ___U3CReadOnlyDataU3Ek__BackingField_9; }
	inline Dictionary_2_t1801537638 ** get_address_of_U3CReadOnlyDataU3Ek__BackingField_9() { return &___U3CReadOnlyDataU3Ek__BackingField_9; }
	inline void set_U3CReadOnlyDataU3Ek__BackingField_9(Dictionary_2_t1801537638 * value)
	{
		___U3CReadOnlyDataU3Ek__BackingField_9 = value;
		Il2CppCodeGenWriteBarrier(&___U3CReadOnlyDataU3Ek__BackingField_9, value);
	}

	inline static int32_t get_offset_of_U3CReadOnlyDataVersionU3Ek__BackingField_10() { return static_cast<int32_t>(offsetof(GetUserCombinedInfoResult_t1115681121, ___U3CReadOnlyDataVersionU3Ek__BackingField_10)); }
	inline uint32_t get_U3CReadOnlyDataVersionU3Ek__BackingField_10() const { return ___U3CReadOnlyDataVersionU3Ek__BackingField_10; }
	inline uint32_t* get_address_of_U3CReadOnlyDataVersionU3Ek__BackingField_10() { return &___U3CReadOnlyDataVersionU3Ek__BackingField_10; }
	inline void set_U3CReadOnlyDataVersionU3Ek__BackingField_10(uint32_t value)
	{
		___U3CReadOnlyDataVersionU3Ek__BackingField_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
