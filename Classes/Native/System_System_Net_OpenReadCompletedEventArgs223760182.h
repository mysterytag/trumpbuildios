﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.IO.Stream
struct Stream_t219029575;

#include "System_System_ComponentModel_AsyncCompletedEventAr2437453489.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.OpenReadCompletedEventArgs
struct  OpenReadCompletedEventArgs_t223760182  : public AsyncCompletedEventArgs_t2437453489
{
public:
	// System.IO.Stream System.Net.OpenReadCompletedEventArgs::result
	Stream_t219029575 * ___result_4;

public:
	inline static int32_t get_offset_of_result_4() { return static_cast<int32_t>(offsetof(OpenReadCompletedEventArgs_t223760182, ___result_4)); }
	inline Stream_t219029575 * get_result_4() const { return ___result_4; }
	inline Stream_t219029575 ** get_address_of_result_4() { return &___result_4; }
	inline void set_result_4(Stream_t219029575 * value)
	{
		___result_4 = value;
		Il2CppCodeGenWriteBarrier(&___result_4, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
