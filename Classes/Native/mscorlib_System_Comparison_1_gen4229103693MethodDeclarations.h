﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Comparison`1<UnityEngine.Rect>
struct Comparison_1_t4229103693;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t537683269;
// System.AsyncCallback
struct AsyncCallback_t1363551830;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_IntPtr676692020.h"
#include "UnityEngine_UnityEngine_Rect1525428817.h"
#include "mscorlib_System_AsyncCallback1363551830.h"

// System.Void System.Comparison`1<UnityEngine.Rect>::.ctor(System.Object,System.IntPtr)
extern "C"  void Comparison_1__ctor_m1767565422_gshared (Comparison_1_t4229103693 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method);
#define Comparison_1__ctor_m1767565422(__this, ___object0, ___method1, method) ((  void (*) (Comparison_1_t4229103693 *, Il2CppObject *, IntPtr_t, const MethodInfo*))Comparison_1__ctor_m1767565422_gshared)(__this, ___object0, ___method1, method)
// System.Int32 System.Comparison`1<UnityEngine.Rect>::Invoke(T,T)
extern "C"  int32_t Comparison_1_Invoke_m3014938954_gshared (Comparison_1_t4229103693 * __this, Rect_t1525428817  ___x0, Rect_t1525428817  ___y1, const MethodInfo* method);
#define Comparison_1_Invoke_m3014938954(__this, ___x0, ___y1, method) ((  int32_t (*) (Comparison_1_t4229103693 *, Rect_t1525428817 , Rect_t1525428817 , const MethodInfo*))Comparison_1_Invoke_m3014938954_gshared)(__this, ___x0, ___y1, method)
// System.IAsyncResult System.Comparison`1<UnityEngine.Rect>::BeginInvoke(T,T,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * Comparison_1_BeginInvoke_m3965055123_gshared (Comparison_1_t4229103693 * __this, Rect_t1525428817  ___x0, Rect_t1525428817  ___y1, AsyncCallback_t1363551830 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method);
#define Comparison_1_BeginInvoke_m3965055123(__this, ___x0, ___y1, ___callback2, ___object3, method) ((  Il2CppObject * (*) (Comparison_1_t4229103693 *, Rect_t1525428817 , Rect_t1525428817 , AsyncCallback_t1363551830 *, Il2CppObject *, const MethodInfo*))Comparison_1_BeginInvoke_m3965055123_gshared)(__this, ___x0, ___y1, ___callback2, ___object3, method)
// System.Int32 System.Comparison`1<UnityEngine.Rect>::EndInvoke(System.IAsyncResult)
extern "C"  int32_t Comparison_1_EndInvoke_m3247388386_gshared (Comparison_1_t4229103693 * __this, Il2CppObject * ___result0, const MethodInfo* method);
#define Comparison_1_EndInvoke_m3247388386(__this, ___result0, method) ((  int32_t (*) (Comparison_1_t4229103693 *, Il2CppObject *, const MethodInfo*))Comparison_1_EndInvoke_m3247388386_gshared)(__this, ___result0, method)
