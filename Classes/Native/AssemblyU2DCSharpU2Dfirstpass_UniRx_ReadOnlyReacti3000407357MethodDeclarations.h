﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.ReadOnlyReactiveProperty`1<System.Boolean>
struct ReadOnlyReactiveProperty_1_t3000407357;
// UniRx.IObservable`1<System.Boolean>
struct IObservable_1_t4264771001;
// System.IDisposable
struct IDisposable_t1628921374;
// UniRx.IObserver`1<System.Boolean>
struct IObserver_1_t2423004244;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"

// System.Void UniRx.ReadOnlyReactiveProperty`1<System.Boolean>::.ctor(UniRx.IObservable`1<T>)
extern "C"  void ReadOnlyReactiveProperty_1__ctor_m2005416796_gshared (ReadOnlyReactiveProperty_1_t3000407357 * __this, Il2CppObject* ___source0, const MethodInfo* method);
#define ReadOnlyReactiveProperty_1__ctor_m2005416796(__this, ___source0, method) ((  void (*) (ReadOnlyReactiveProperty_1_t3000407357 *, Il2CppObject*, const MethodInfo*))ReadOnlyReactiveProperty_1__ctor_m2005416796_gshared)(__this, ___source0, method)
// System.Void UniRx.ReadOnlyReactiveProperty`1<System.Boolean>::.ctor(UniRx.IObservable`1<T>,T)
extern "C"  void ReadOnlyReactiveProperty_1__ctor_m3060197876_gshared (ReadOnlyReactiveProperty_1_t3000407357 * __this, Il2CppObject* ___source0, bool ___initialValue1, const MethodInfo* method);
#define ReadOnlyReactiveProperty_1__ctor_m3060197876(__this, ___source0, ___initialValue1, method) ((  void (*) (ReadOnlyReactiveProperty_1_t3000407357 *, Il2CppObject*, bool, const MethodInfo*))ReadOnlyReactiveProperty_1__ctor_m3060197876_gshared)(__this, ___source0, ___initialValue1, method)
// T UniRx.ReadOnlyReactiveProperty`1<System.Boolean>::get_Value()
extern "C"  bool ReadOnlyReactiveProperty_1_get_Value_m3402680452_gshared (ReadOnlyReactiveProperty_1_t3000407357 * __this, const MethodInfo* method);
#define ReadOnlyReactiveProperty_1_get_Value_m3402680452(__this, method) ((  bool (*) (ReadOnlyReactiveProperty_1_t3000407357 *, const MethodInfo*))ReadOnlyReactiveProperty_1_get_Value_m3402680452_gshared)(__this, method)
// System.IDisposable UniRx.ReadOnlyReactiveProperty`1<System.Boolean>::Subscribe(UniRx.IObserver`1<T>)
extern "C"  Il2CppObject * ReadOnlyReactiveProperty_1_Subscribe_m98383100_gshared (ReadOnlyReactiveProperty_1_t3000407357 * __this, Il2CppObject* ___observer0, const MethodInfo* method);
#define ReadOnlyReactiveProperty_1_Subscribe_m98383100(__this, ___observer0, method) ((  Il2CppObject * (*) (ReadOnlyReactiveProperty_1_t3000407357 *, Il2CppObject*, const MethodInfo*))ReadOnlyReactiveProperty_1_Subscribe_m98383100_gshared)(__this, ___observer0, method)
// System.Void UniRx.ReadOnlyReactiveProperty`1<System.Boolean>::Dispose()
extern "C"  void ReadOnlyReactiveProperty_1_Dispose_m3901694522_gshared (ReadOnlyReactiveProperty_1_t3000407357 * __this, const MethodInfo* method);
#define ReadOnlyReactiveProperty_1_Dispose_m3901694522(__this, method) ((  void (*) (ReadOnlyReactiveProperty_1_t3000407357 *, const MethodInfo*))ReadOnlyReactiveProperty_1_Dispose_m3901694522_gshared)(__this, method)
// System.Void UniRx.ReadOnlyReactiveProperty`1<System.Boolean>::Dispose(System.Boolean)
extern "C"  void ReadOnlyReactiveProperty_1_Dispose_m3297274417_gshared (ReadOnlyReactiveProperty_1_t3000407357 * __this, bool ___disposing0, const MethodInfo* method);
#define ReadOnlyReactiveProperty_1_Dispose_m3297274417(__this, ___disposing0, method) ((  void (*) (ReadOnlyReactiveProperty_1_t3000407357 *, bool, const MethodInfo*))ReadOnlyReactiveProperty_1_Dispose_m3297274417_gshared)(__this, ___disposing0, method)
// System.String UniRx.ReadOnlyReactiveProperty`1<System.Boolean>::ToString()
extern "C"  String_t* ReadOnlyReactiveProperty_1_ToString_m577531254_gshared (ReadOnlyReactiveProperty_1_t3000407357 * __this, const MethodInfo* method);
#define ReadOnlyReactiveProperty_1_ToString_m577531254(__this, method) ((  String_t* (*) (ReadOnlyReactiveProperty_1_t3000407357 *, const MethodInfo*))ReadOnlyReactiveProperty_1_ToString_m577531254_gshared)(__this, method)
// System.Boolean UniRx.ReadOnlyReactiveProperty`1<System.Boolean>::IsRequiredSubscribeOnCurrentThread()
extern "C"  bool ReadOnlyReactiveProperty_1_IsRequiredSubscribeOnCurrentThread_m1727181926_gshared (ReadOnlyReactiveProperty_1_t3000407357 * __this, const MethodInfo* method);
#define ReadOnlyReactiveProperty_1_IsRequiredSubscribeOnCurrentThread_m1727181926(__this, method) ((  bool (*) (ReadOnlyReactiveProperty_1_t3000407357 *, const MethodInfo*))ReadOnlyReactiveProperty_1_IsRequiredSubscribeOnCurrentThread_m1727181926_gshared)(__this, method)
