﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.InternalUtil.DisposedObserver`1<UnityEngine.Vector3>
struct DisposedObserver_1_t2699787251;
// System.Exception
struct Exception_t1967233988;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Exception1967233988.h"
#include "UnityEngine_UnityEngine_Vector33525329789.h"

// System.Void UniRx.InternalUtil.DisposedObserver`1<UnityEngine.Vector3>::.ctor()
extern "C"  void DisposedObserver_1__ctor_m1900745009_gshared (DisposedObserver_1_t2699787251 * __this, const MethodInfo* method);
#define DisposedObserver_1__ctor_m1900745009(__this, method) ((  void (*) (DisposedObserver_1_t2699787251 *, const MethodInfo*))DisposedObserver_1__ctor_m1900745009_gshared)(__this, method)
// System.Void UniRx.InternalUtil.DisposedObserver`1<UnityEngine.Vector3>::.cctor()
extern "C"  void DisposedObserver_1__cctor_m2606424220_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define DisposedObserver_1__cctor_m2606424220(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))DisposedObserver_1__cctor_m2606424220_gshared)(__this /* static, unused */, method)
// System.Void UniRx.InternalUtil.DisposedObserver`1<UnityEngine.Vector3>::OnCompleted()
extern "C"  void DisposedObserver_1_OnCompleted_m3891715067_gshared (DisposedObserver_1_t2699787251 * __this, const MethodInfo* method);
#define DisposedObserver_1_OnCompleted_m3891715067(__this, method) ((  void (*) (DisposedObserver_1_t2699787251 *, const MethodInfo*))DisposedObserver_1_OnCompleted_m3891715067_gshared)(__this, method)
// System.Void UniRx.InternalUtil.DisposedObserver`1<UnityEngine.Vector3>::OnError(System.Exception)
extern "C"  void DisposedObserver_1_OnError_m3030936744_gshared (DisposedObserver_1_t2699787251 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method);
#define DisposedObserver_1_OnError_m3030936744(__this, ___error0, method) ((  void (*) (DisposedObserver_1_t2699787251 *, Exception_t1967233988 *, const MethodInfo*))DisposedObserver_1_OnError_m3030936744_gshared)(__this, ___error0, method)
// System.Void UniRx.InternalUtil.DisposedObserver`1<UnityEngine.Vector3>::OnNext(T)
extern "C"  void DisposedObserver_1_OnNext_m4085556121_gshared (DisposedObserver_1_t2699787251 * __this, Vector3_t3525329789  ___value0, const MethodInfo* method);
#define DisposedObserver_1_OnNext_m4085556121(__this, ___value0, method) ((  void (*) (DisposedObserver_1_t2699787251 *, Vector3_t3525329789 , const MethodInfo*))DisposedObserver_1_OnNext_m4085556121_gshared)(__this, ___value0, method)
