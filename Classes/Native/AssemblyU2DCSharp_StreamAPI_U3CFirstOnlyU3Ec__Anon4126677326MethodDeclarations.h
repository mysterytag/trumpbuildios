﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// StreamAPI/<FirstOnly>c__AnonStorey136`1/<FirstOnly>c__AnonStorey137`1<System.Object>
struct U3CFirstOnlyU3Ec__AnonStorey137_1_t4126677326;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"

// System.Void StreamAPI/<FirstOnly>c__AnonStorey136`1/<FirstOnly>c__AnonStorey137`1<System.Object>::.ctor()
extern "C"  void U3CFirstOnlyU3Ec__AnonStorey137_1__ctor_m630990004_gshared (U3CFirstOnlyU3Ec__AnonStorey137_1_t4126677326 * __this, const MethodInfo* method);
#define U3CFirstOnlyU3Ec__AnonStorey137_1__ctor_m630990004(__this, method) ((  void (*) (U3CFirstOnlyU3Ec__AnonStorey137_1_t4126677326 *, const MethodInfo*))U3CFirstOnlyU3Ec__AnonStorey137_1__ctor_m630990004_gshared)(__this, method)
// System.Void StreamAPI/<FirstOnly>c__AnonStorey136`1/<FirstOnly>c__AnonStorey137`1<System.Object>::<>m__1CE(T)
extern "C"  void U3CFirstOnlyU3Ec__AnonStorey137_1_U3CU3Em__1CE_m1788402118_gshared (U3CFirstOnlyU3Ec__AnonStorey137_1_t4126677326 * __this, Il2CppObject * ___val0, const MethodInfo* method);
#define U3CFirstOnlyU3Ec__AnonStorey137_1_U3CU3Em__1CE_m1788402118(__this, ___val0, method) ((  void (*) (U3CFirstOnlyU3Ec__AnonStorey137_1_t4126677326 *, Il2CppObject *, const MethodInfo*))U3CFirstOnlyU3Ec__AnonStorey137_1_U3CU3Em__1CE_m1788402118_gshared)(__this, ___val0, method)
