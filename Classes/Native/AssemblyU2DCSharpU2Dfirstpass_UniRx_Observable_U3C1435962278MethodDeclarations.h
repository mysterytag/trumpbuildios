﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Observable/<FromAsyncPattern>c__AnonStorey43`3<System.Object,System.Object,System.Object>
struct U3CFromAsyncPatternU3Ec__AnonStorey43_3_t1435962278;
// UniRx.IObservable`1<System.Object>
struct IObservable_1_t595904784;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"

// System.Void UniRx.Observable/<FromAsyncPattern>c__AnonStorey43`3<System.Object,System.Object,System.Object>::.ctor()
extern "C"  void U3CFromAsyncPatternU3Ec__AnonStorey43_3__ctor_m2090340864_gshared (U3CFromAsyncPatternU3Ec__AnonStorey43_3_t1435962278 * __this, const MethodInfo* method);
#define U3CFromAsyncPatternU3Ec__AnonStorey43_3__ctor_m2090340864(__this, method) ((  void (*) (U3CFromAsyncPatternU3Ec__AnonStorey43_3_t1435962278 *, const MethodInfo*))U3CFromAsyncPatternU3Ec__AnonStorey43_3__ctor_m2090340864_gshared)(__this, method)
// UniRx.IObservable`1<TResult> UniRx.Observable/<FromAsyncPattern>c__AnonStorey43`3<System.Object,System.Object,System.Object>::<>m__42(T1,T2)
extern "C"  Il2CppObject* U3CFromAsyncPatternU3Ec__AnonStorey43_3_U3CU3Em__42_m1404897853_gshared (U3CFromAsyncPatternU3Ec__AnonStorey43_3_t1435962278 * __this, Il2CppObject * ___x0, Il2CppObject * ___y1, const MethodInfo* method);
#define U3CFromAsyncPatternU3Ec__AnonStorey43_3_U3CU3Em__42_m1404897853(__this, ___x0, ___y1, method) ((  Il2CppObject* (*) (U3CFromAsyncPatternU3Ec__AnonStorey43_3_t1435962278 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))U3CFromAsyncPatternU3Ec__AnonStorey43_3_U3CU3Em__42_m1404897853_gshared)(__this, ___x0, ___y1, method)
