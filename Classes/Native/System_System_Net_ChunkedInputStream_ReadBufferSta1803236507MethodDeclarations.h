﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Net.ChunkedInputStream/ReadBufferState
struct ReadBufferState_t1803236507;
// System.Byte[]
struct ByteU5BU5D_t58506160;
// System.Net.HttpStreamAsyncResult
struct HttpStreamAsyncResult_t20138427;

#include "codegen/il2cpp-codegen.h"
#include "System_System_Net_HttpStreamAsyncResult20138427.h"

// System.Void System.Net.ChunkedInputStream/ReadBufferState::.ctor(System.Byte[],System.Int32,System.Int32,System.Net.HttpStreamAsyncResult)
extern "C"  void ReadBufferState__ctor_m408970412 (ReadBufferState_t1803236507 * __this, ByteU5BU5D_t58506160* ___buffer0, int32_t ___offset1, int32_t ___count2, HttpStreamAsyncResult_t20138427 * ___ares3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
