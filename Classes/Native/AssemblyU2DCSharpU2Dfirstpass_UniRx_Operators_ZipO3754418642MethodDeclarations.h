﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.ZipObservable`1/Zip<System.Object>
struct Zip_t3754418642;
// UniRx.Operators.ZipObservable`1<System.Object>
struct ZipObservable_1_t2062551787;
// UniRx.IObserver`1<System.Collections.Generic.IList`1<System.Object>>
struct IObserver_1_t920630341;
// System.IDisposable
struct IDisposable_t1628921374;
// System.Collections.Generic.IList`1<System.Object>
struct IList_1_t3003598734;
// System.Exception
struct Exception_t1967233988;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Exception1967233988.h"

// System.Void UniRx.Operators.ZipObservable`1/Zip<System.Object>::.ctor(UniRx.Operators.ZipObservable`1<T>,UniRx.IObserver`1<System.Collections.Generic.IList`1<T>>,System.IDisposable)
extern "C"  void Zip__ctor_m654238358_gshared (Zip_t3754418642 * __this, ZipObservable_1_t2062551787 * ___parent0, Il2CppObject* ___observer1, Il2CppObject * ___cancel2, const MethodInfo* method);
#define Zip__ctor_m654238358(__this, ___parent0, ___observer1, ___cancel2, method) ((  void (*) (Zip_t3754418642 *, ZipObservable_1_t2062551787 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))Zip__ctor_m654238358_gshared)(__this, ___parent0, ___observer1, ___cancel2, method)
// System.IDisposable UniRx.Operators.ZipObservable`1/Zip<System.Object>::Run()
extern "C"  Il2CppObject * Zip_Run_m1113711473_gshared (Zip_t3754418642 * __this, const MethodInfo* method);
#define Zip_Run_m1113711473(__this, method) ((  Il2CppObject * (*) (Zip_t3754418642 *, const MethodInfo*))Zip_Run_m1113711473_gshared)(__this, method)
// System.Void UniRx.Operators.ZipObservable`1/Zip<System.Object>::Dequeue(System.Int32)
extern "C"  void Zip_Dequeue_m119540610_gshared (Zip_t3754418642 * __this, int32_t ___index0, const MethodInfo* method);
#define Zip_Dequeue_m119540610(__this, ___index0, method) ((  void (*) (Zip_t3754418642 *, int32_t, const MethodInfo*))Zip_Dequeue_m119540610_gshared)(__this, ___index0, method)
// System.Void UniRx.Operators.ZipObservable`1/Zip<System.Object>::OnNext(System.Collections.Generic.IList`1<T>)
extern "C"  void Zip_OnNext_m4227229314_gshared (Zip_t3754418642 * __this, Il2CppObject* ___value0, const MethodInfo* method);
#define Zip_OnNext_m4227229314(__this, ___value0, method) ((  void (*) (Zip_t3754418642 *, Il2CppObject*, const MethodInfo*))Zip_OnNext_m4227229314_gshared)(__this, ___value0, method)
// System.Void UniRx.Operators.ZipObservable`1/Zip<System.Object>::OnError(System.Exception)
extern "C"  void Zip_OnError_m3449658458_gshared (Zip_t3754418642 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method);
#define Zip_OnError_m3449658458(__this, ___error0, method) ((  void (*) (Zip_t3754418642 *, Exception_t1967233988 *, const MethodInfo*))Zip_OnError_m3449658458_gshared)(__this, ___error0, method)
// System.Void UniRx.Operators.ZipObservable`1/Zip<System.Object>::OnCompleted()
extern "C"  void Zip_OnCompleted_m1828751021_gshared (Zip_t3754418642 * __this, const MethodInfo* method);
#define Zip_OnCompleted_m1828751021(__this, method) ((  void (*) (Zip_t3754418642 *, const MethodInfo*))Zip_OnCompleted_m1828751021_gshared)(__this, method)
// System.Void UniRx.Operators.ZipObservable`1/Zip<System.Object>::<Run>m__91()
extern "C"  void Zip_U3CRunU3Em__91_m3163182775_gshared (Zip_t3754418642 * __this, const MethodInfo* method);
#define Zip_U3CRunU3Em__91_m3163182775(__this, method) ((  void (*) (Zip_t3754418642 *, const MethodInfo*))Zip_U3CRunU3Em__91_m3163182775_gshared)(__this, method)
