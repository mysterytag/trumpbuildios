﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Object
struct Il2CppObject;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Pair_1_gen3831097582.h"
#include "mscorlib_System_Object837106420.h"

// System.Void UniRx.Pair`1<System.Object>::.ctor(T,T)
extern "C"  void Pair_1__ctor_m821078360_gshared (Pair_1_t3831097582 * __this, Il2CppObject * ___previous0, Il2CppObject * ___current1, const MethodInfo* method);
#define Pair_1__ctor_m821078360(__this, ___previous0, ___current1, method) ((  void (*) (Pair_1_t3831097582 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))Pair_1__ctor_m821078360_gshared)(__this, ___previous0, ___current1, method)
// T UniRx.Pair`1<System.Object>::get_Previous()
extern "C"  Il2CppObject * Pair_1_get_Previous_m3083555079_gshared (Pair_1_t3831097582 * __this, const MethodInfo* method);
#define Pair_1_get_Previous_m3083555079(__this, method) ((  Il2CppObject * (*) (Pair_1_t3831097582 *, const MethodInfo*))Pair_1_get_Previous_m3083555079_gshared)(__this, method)
// T UniRx.Pair`1<System.Object>::get_Current()
extern "C"  Il2CppObject * Pair_1_get_Current_m2282143627_gshared (Pair_1_t3831097582 * __this, const MethodInfo* method);
#define Pair_1_get_Current_m2282143627(__this, method) ((  Il2CppObject * (*) (Pair_1_t3831097582 *, const MethodInfo*))Pair_1_get_Current_m2282143627_gshared)(__this, method)
// System.Int32 UniRx.Pair`1<System.Object>::GetHashCode()
extern "C"  int32_t Pair_1_GetHashCode_m3615362563_gshared (Pair_1_t3831097582 * __this, const MethodInfo* method);
#define Pair_1_GetHashCode_m3615362563(__this, method) ((  int32_t (*) (Pair_1_t3831097582 *, const MethodInfo*))Pair_1_GetHashCode_m3615362563_gshared)(__this, method)
// System.Boolean UniRx.Pair`1<System.Object>::Equals(System.Object)
extern "C"  bool Pair_1_Equals_m3736815979_gshared (Pair_1_t3831097582 * __this, Il2CppObject * ___obj0, const MethodInfo* method);
#define Pair_1_Equals_m3736815979(__this, ___obj0, method) ((  bool (*) (Pair_1_t3831097582 *, Il2CppObject *, const MethodInfo*))Pair_1_Equals_m3736815979_gshared)(__this, ___obj0, method)
// System.Boolean UniRx.Pair`1<System.Object>::Equals(UniRx.Pair`1<T>)
extern "C"  bool Pair_1_Equals_m3273956134_gshared (Pair_1_t3831097582 * __this, Pair_1_t3831097582  ___other0, const MethodInfo* method);
#define Pair_1_Equals_m3273956134(__this, ___other0, method) ((  bool (*) (Pair_1_t3831097582 *, Pair_1_t3831097582 , const MethodInfo*))Pair_1_Equals_m3273956134_gshared)(__this, ___other0, method)
// System.String UniRx.Pair`1<System.Object>::ToString()
extern "C"  String_t* Pair_1_ToString_m4073881743_gshared (Pair_1_t3831097582 * __this, const MethodInfo* method);
#define Pair_1_ToString_m4073881743(__this, method) ((  String_t* (*) (Pair_1_t3831097582 *, const MethodInfo*))Pair_1_ToString_m4073881743_gshared)(__this, method)
