﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.Quaternion>
struct DefaultComparer_t1229669710;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Quaternion1891715979.h"

// System.Void System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.Quaternion>::.ctor()
extern "C"  void DefaultComparer__ctor_m3463046145_gshared (DefaultComparer_t1229669710 * __this, const MethodInfo* method);
#define DefaultComparer__ctor_m3463046145(__this, method) ((  void (*) (DefaultComparer_t1229669710 *, const MethodInfo*))DefaultComparer__ctor_m3463046145_gshared)(__this, method)
// System.Int32 System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.Quaternion>::GetHashCode(T)
extern "C"  int32_t DefaultComparer_GetHashCode_m2608358610_gshared (DefaultComparer_t1229669710 * __this, Quaternion_t1891715979  ___obj0, const MethodInfo* method);
#define DefaultComparer_GetHashCode_m2608358610(__this, ___obj0, method) ((  int32_t (*) (DefaultComparer_t1229669710 *, Quaternion_t1891715979 , const MethodInfo*))DefaultComparer_GetHashCode_m2608358610_gshared)(__this, ___obj0, method)
// System.Boolean System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.Quaternion>::Equals(T,T)
extern "C"  bool DefaultComparer_Equals_m224534038_gshared (DefaultComparer_t1229669710 * __this, Quaternion_t1891715979  ___x0, Quaternion_t1891715979  ___y1, const MethodInfo* method);
#define DefaultComparer_Equals_m224534038(__this, ___x0, ___y1, method) ((  bool (*) (DefaultComparer_t1229669710 *, Quaternion_t1891715979 , Quaternion_t1891715979 , const MethodInfo*))DefaultComparer_Equals_m224534038_gshared)(__this, ___x0, ___y1, method)
