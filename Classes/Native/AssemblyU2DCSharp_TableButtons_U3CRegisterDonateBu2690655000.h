﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Action
struct Action_t437523947;
// TableButtons
struct TableButtons_t1868573683;
// System.Func`3<System.Boolean,System.Boolean,<>__AnonType7`2<System.Boolean,System.Boolean>>
struct Func_3_t3406901219;
// System.Action`1<UniRx.Unit>
struct Action_1_t2706738743;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TableButtons/<RegisterDonateButtons>c__AnonStorey169
struct  U3CRegisterDonateButtonsU3Ec__AnonStorey169_t2690655000  : public Il2CppObject
{
public:
	// System.Action TableButtons/<RegisterDonateButtons>c__AnonStorey169::achievements
	Action_t437523947 * ___achievements_0;
	// System.Action TableButtons/<RegisterDonateButtons>c__AnonStorey169::leaderboards
	Action_t437523947 * ___leaderboards_1;
	// System.Action TableButtons/<RegisterDonateButtons>c__AnonStorey169::startOver
	Action_t437523947 * ___startOver_2;
	// System.Action TableButtons/<RegisterDonateButtons>c__AnonStorey169::soundSwitch
	Action_t437523947 * ___soundSwitch_3;
	// TableButtons TableButtons/<RegisterDonateButtons>c__AnonStorey169::<>f__this
	TableButtons_t1868573683 * ___U3CU3Ef__this_4;

public:
	inline static int32_t get_offset_of_achievements_0() { return static_cast<int32_t>(offsetof(U3CRegisterDonateButtonsU3Ec__AnonStorey169_t2690655000, ___achievements_0)); }
	inline Action_t437523947 * get_achievements_0() const { return ___achievements_0; }
	inline Action_t437523947 ** get_address_of_achievements_0() { return &___achievements_0; }
	inline void set_achievements_0(Action_t437523947 * value)
	{
		___achievements_0 = value;
		Il2CppCodeGenWriteBarrier(&___achievements_0, value);
	}

	inline static int32_t get_offset_of_leaderboards_1() { return static_cast<int32_t>(offsetof(U3CRegisterDonateButtonsU3Ec__AnonStorey169_t2690655000, ___leaderboards_1)); }
	inline Action_t437523947 * get_leaderboards_1() const { return ___leaderboards_1; }
	inline Action_t437523947 ** get_address_of_leaderboards_1() { return &___leaderboards_1; }
	inline void set_leaderboards_1(Action_t437523947 * value)
	{
		___leaderboards_1 = value;
		Il2CppCodeGenWriteBarrier(&___leaderboards_1, value);
	}

	inline static int32_t get_offset_of_startOver_2() { return static_cast<int32_t>(offsetof(U3CRegisterDonateButtonsU3Ec__AnonStorey169_t2690655000, ___startOver_2)); }
	inline Action_t437523947 * get_startOver_2() const { return ___startOver_2; }
	inline Action_t437523947 ** get_address_of_startOver_2() { return &___startOver_2; }
	inline void set_startOver_2(Action_t437523947 * value)
	{
		___startOver_2 = value;
		Il2CppCodeGenWriteBarrier(&___startOver_2, value);
	}

	inline static int32_t get_offset_of_soundSwitch_3() { return static_cast<int32_t>(offsetof(U3CRegisterDonateButtonsU3Ec__AnonStorey169_t2690655000, ___soundSwitch_3)); }
	inline Action_t437523947 * get_soundSwitch_3() const { return ___soundSwitch_3; }
	inline Action_t437523947 ** get_address_of_soundSwitch_3() { return &___soundSwitch_3; }
	inline void set_soundSwitch_3(Action_t437523947 * value)
	{
		___soundSwitch_3 = value;
		Il2CppCodeGenWriteBarrier(&___soundSwitch_3, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__this_4() { return static_cast<int32_t>(offsetof(U3CRegisterDonateButtonsU3Ec__AnonStorey169_t2690655000, ___U3CU3Ef__this_4)); }
	inline TableButtons_t1868573683 * get_U3CU3Ef__this_4() const { return ___U3CU3Ef__this_4; }
	inline TableButtons_t1868573683 ** get_address_of_U3CU3Ef__this_4() { return &___U3CU3Ef__this_4; }
	inline void set_U3CU3Ef__this_4(TableButtons_t1868573683 * value)
	{
		___U3CU3Ef__this_4 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__this_4, value);
	}
};

struct U3CRegisterDonateButtonsU3Ec__AnonStorey169_t2690655000_StaticFields
{
public:
	// System.Func`3<System.Boolean,System.Boolean,<>__AnonType7`2<System.Boolean,System.Boolean>> TableButtons/<RegisterDonateButtons>c__AnonStorey169::<>f__am$cache5
	Func_3_t3406901219 * ___U3CU3Ef__amU24cache5_5;
	// System.Func`3<System.Boolean,System.Boolean,<>__AnonType7`2<System.Boolean,System.Boolean>> TableButtons/<RegisterDonateButtons>c__AnonStorey169::<>f__am$cache6
	Func_3_t3406901219 * ___U3CU3Ef__amU24cache6_6;
	// System.Action`1<UniRx.Unit> TableButtons/<RegisterDonateButtons>c__AnonStorey169::<>f__am$cache7
	Action_1_t2706738743 * ___U3CU3Ef__amU24cache7_7;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache5_5() { return static_cast<int32_t>(offsetof(U3CRegisterDonateButtonsU3Ec__AnonStorey169_t2690655000_StaticFields, ___U3CU3Ef__amU24cache5_5)); }
	inline Func_3_t3406901219 * get_U3CU3Ef__amU24cache5_5() const { return ___U3CU3Ef__amU24cache5_5; }
	inline Func_3_t3406901219 ** get_address_of_U3CU3Ef__amU24cache5_5() { return &___U3CU3Ef__amU24cache5_5; }
	inline void set_U3CU3Ef__amU24cache5_5(Func_3_t3406901219 * value)
	{
		___U3CU3Ef__amU24cache5_5 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache5_5, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache6_6() { return static_cast<int32_t>(offsetof(U3CRegisterDonateButtonsU3Ec__AnonStorey169_t2690655000_StaticFields, ___U3CU3Ef__amU24cache6_6)); }
	inline Func_3_t3406901219 * get_U3CU3Ef__amU24cache6_6() const { return ___U3CU3Ef__amU24cache6_6; }
	inline Func_3_t3406901219 ** get_address_of_U3CU3Ef__amU24cache6_6() { return &___U3CU3Ef__amU24cache6_6; }
	inline void set_U3CU3Ef__amU24cache6_6(Func_3_t3406901219 * value)
	{
		___U3CU3Ef__amU24cache6_6 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache6_6, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache7_7() { return static_cast<int32_t>(offsetof(U3CRegisterDonateButtonsU3Ec__AnonStorey169_t2690655000_StaticFields, ___U3CU3Ef__amU24cache7_7)); }
	inline Action_1_t2706738743 * get_U3CU3Ef__amU24cache7_7() const { return ___U3CU3Ef__amU24cache7_7; }
	inline Action_1_t2706738743 ** get_address_of_U3CU3Ef__amU24cache7_7() { return &___U3CU3Ef__amU24cache7_7; }
	inline void set_U3CU3Ef__amU24cache7_7(Action_1_t2706738743 * value)
	{
		___U3CU3Ef__amU24cache7_7 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache7_7, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
