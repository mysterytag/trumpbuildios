﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// AnalyticsController/<SendAdsRevenue>c__Iterator1E
struct U3CSendAdsRevenueU3Ec__Iterator1E_t1213536727;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void AnalyticsController/<SendAdsRevenue>c__Iterator1E::.ctor()
extern "C"  void U3CSendAdsRevenueU3Ec__Iterator1E__ctor_m802493569 (U3CSendAdsRevenueU3Ec__Iterator1E_t1213536727 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object AnalyticsController/<SendAdsRevenue>c__Iterator1E::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CSendAdsRevenueU3Ec__Iterator1E_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m481446065 (U3CSendAdsRevenueU3Ec__Iterator1E_t1213536727 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object AnalyticsController/<SendAdsRevenue>c__Iterator1E::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CSendAdsRevenueU3Ec__Iterator1E_System_Collections_IEnumerator_get_Current_m826658373 (U3CSendAdsRevenueU3Ec__Iterator1E_t1213536727 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean AnalyticsController/<SendAdsRevenue>c__Iterator1E::MoveNext()
extern "C"  bool U3CSendAdsRevenueU3Ec__Iterator1E_MoveNext_m602477267 (U3CSendAdsRevenueU3Ec__Iterator1E_t1213536727 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AnalyticsController/<SendAdsRevenue>c__Iterator1E::Dispose()
extern "C"  void U3CSendAdsRevenueU3Ec__Iterator1E_Dispose_m2295456894 (U3CSendAdsRevenueU3Ec__Iterator1E_t1213536727 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AnalyticsController/<SendAdsRevenue>c__Iterator1E::Reset()
extern "C"  void U3CSendAdsRevenueU3Ec__Iterator1E_Reset_m2743893806 (U3CSendAdsRevenueU3Ec__Iterator1E_t1213536727 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AnalyticsController/<SendAdsRevenue>c__Iterator1E::<>m__12B(System.Single)
extern "C"  void U3CSendAdsRevenueU3Ec__Iterator1E_U3CU3Em__12B_m958191666 (U3CSendAdsRevenueU3Ec__Iterator1E_t1213536727 * __this, float ___val0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
