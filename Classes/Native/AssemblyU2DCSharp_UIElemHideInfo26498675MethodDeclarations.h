﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UIElemHideInfo
struct UIElemHideInfo_t26498675;

#include "codegen/il2cpp-codegen.h"

// System.Void UIElemHideInfo::.ctor()
extern "C"  void UIElemHideInfo__ctor_m1409288072 (UIElemHideInfo_t26498675 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
