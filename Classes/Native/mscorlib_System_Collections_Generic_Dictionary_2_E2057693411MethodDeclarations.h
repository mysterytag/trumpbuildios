﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2<SponsorPay.SPLogLevel,System.Int32>
struct Dictionary_2_t2290665470;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E2057693411.h"
#include "mscorlib_System_Collections_DictionaryEntry130027246.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_21779196768.h"
#include "AssemblyU2DCSharp_SponsorPay_SPLogLevel220722135.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<SponsorPay.SPLogLevel,System.Int32>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void Enumerator__ctor_m1575905197_gshared (Enumerator_t2057693412 * __this, Dictionary_2_t2290665470 * ___dictionary0, const MethodInfo* method);
#define Enumerator__ctor_m1575905197(__this, ___dictionary0, method) ((  void (*) (Enumerator_t2057693412 *, Dictionary_2_t2290665470 *, const MethodInfo*))Enumerator__ctor_m1575905197_gshared)(__this, ___dictionary0, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<SponsorPay.SPLogLevel,System.Int32>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m3763537118_gshared (Enumerator_t2057693412 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m3763537118(__this, method) ((  Il2CppObject * (*) (Enumerator_t2057693412 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m3763537118_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<SponsorPay.SPLogLevel,System.Int32>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m752293608_gshared (Enumerator_t2057693412 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_Reset_m752293608(__this, method) ((  void (*) (Enumerator_t2057693412 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m752293608_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<SponsorPay.SPLogLevel,System.Int32>::System.Collections.IDictionaryEnumerator.get_Entry()
extern "C"  DictionaryEntry_t130027246  Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m2420301983_gshared (Enumerator_t2057693412 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m2420301983(__this, method) ((  DictionaryEntry_t130027246  (*) (Enumerator_t2057693412 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m2420301983_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<SponsorPay.SPLogLevel,System.Int32>::System.Collections.IDictionaryEnumerator.get_Key()
extern "C"  Il2CppObject * Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m1547084602_gshared (Enumerator_t2057693412 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m1547084602(__this, method) ((  Il2CppObject * (*) (Enumerator_t2057693412 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m1547084602_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<SponsorPay.SPLogLevel,System.Int32>::System.Collections.IDictionaryEnumerator.get_Value()
extern "C"  Il2CppObject * Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m1738054476_gshared (Enumerator_t2057693412 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m1738054476(__this, method) ((  Il2CppObject * (*) (Enumerator_t2057693412 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m1738054476_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<SponsorPay.SPLogLevel,System.Int32>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m305002136_gshared (Enumerator_t2057693412 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m305002136(__this, method) ((  bool (*) (Enumerator_t2057693412 *, const MethodInfo*))Enumerator_MoveNext_m305002136_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<SponsorPay.SPLogLevel,System.Int32>::get_Current()
extern "C"  KeyValuePair_2_t1779196768  Enumerator_get_Current_m3544716580_gshared (Enumerator_t2057693412 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m3544716580(__this, method) ((  KeyValuePair_2_t1779196768  (*) (Enumerator_t2057693412 *, const MethodInfo*))Enumerator_get_Current_m3544716580_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<SponsorPay.SPLogLevel,System.Int32>::get_CurrentKey()
extern "C"  int32_t Enumerator_get_CurrentKey_m3579178273_gshared (Enumerator_t2057693412 * __this, const MethodInfo* method);
#define Enumerator_get_CurrentKey_m3579178273(__this, method) ((  int32_t (*) (Enumerator_t2057693412 *, const MethodInfo*))Enumerator_get_CurrentKey_m3579178273_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<SponsorPay.SPLogLevel,System.Int32>::get_CurrentValue()
extern "C"  int32_t Enumerator_get_CurrentValue_m1443309857_gshared (Enumerator_t2057693412 * __this, const MethodInfo* method);
#define Enumerator_get_CurrentValue_m1443309857(__this, method) ((  int32_t (*) (Enumerator_t2057693412 *, const MethodInfo*))Enumerator_get_CurrentValue_m1443309857_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<SponsorPay.SPLogLevel,System.Int32>::Reset()
extern "C"  void Enumerator_Reset_m1839451775_gshared (Enumerator_t2057693412 * __this, const MethodInfo* method);
#define Enumerator_Reset_m1839451775(__this, method) ((  void (*) (Enumerator_t2057693412 *, const MethodInfo*))Enumerator_Reset_m1839451775_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<SponsorPay.SPLogLevel,System.Int32>::VerifyState()
extern "C"  void Enumerator_VerifyState_m459014984_gshared (Enumerator_t2057693412 * __this, const MethodInfo* method);
#define Enumerator_VerifyState_m459014984(__this, method) ((  void (*) (Enumerator_t2057693412 *, const MethodInfo*))Enumerator_VerifyState_m459014984_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<SponsorPay.SPLogLevel,System.Int32>::VerifyCurrent()
extern "C"  void Enumerator_VerifyCurrent_m1476639920_gshared (Enumerator_t2057693412 * __this, const MethodInfo* method);
#define Enumerator_VerifyCurrent_m1476639920(__this, method) ((  void (*) (Enumerator_t2057693412 *, const MethodInfo*))Enumerator_VerifyCurrent_m1476639920_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<SponsorPay.SPLogLevel,System.Int32>::Dispose()
extern "C"  void Enumerator_Dispose_m710058895_gshared (Enumerator_t2057693412 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m710058895(__this, method) ((  void (*) (Enumerator_t2057693412 *, const MethodInfo*))Enumerator_Dispose_m710058895_gshared)(__this, method)
