﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_Nullable_1_gen417810504.h"
#include "mscorlib_System_DateTime339033936.h"
#include "mscorlib_System_Nullable_1_gen3225071844.h"
#include "mscorlib_System_Nullable_1_gen3097043249.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PlayFab.ClientModels.UserTitleInfo
struct  UserTitleInfo_t4099117963  : public Il2CppObject
{
public:
	// System.String PlayFab.ClientModels.UserTitleInfo::<DisplayName>k__BackingField
	String_t* ___U3CDisplayNameU3Ek__BackingField_0;
	// System.Nullable`1<PlayFab.ClientModels.UserOrigination> PlayFab.ClientModels.UserTitleInfo::<Origination>k__BackingField
	Nullable_1_t417810504  ___U3COriginationU3Ek__BackingField_1;
	// System.DateTime PlayFab.ClientModels.UserTitleInfo::<Created>k__BackingField
	DateTime_t339033936  ___U3CCreatedU3Ek__BackingField_2;
	// System.Nullable`1<System.DateTime> PlayFab.ClientModels.UserTitleInfo::<LastLogin>k__BackingField
	Nullable_1_t3225071844  ___U3CLastLoginU3Ek__BackingField_3;
	// System.Nullable`1<System.DateTime> PlayFab.ClientModels.UserTitleInfo::<FirstLogin>k__BackingField
	Nullable_1_t3225071844  ___U3CFirstLoginU3Ek__BackingField_4;
	// System.Nullable`1<System.Boolean> PlayFab.ClientModels.UserTitleInfo::<isBanned>k__BackingField
	Nullable_1_t3097043249  ___U3CisBannedU3Ek__BackingField_5;

public:
	inline static int32_t get_offset_of_U3CDisplayNameU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(UserTitleInfo_t4099117963, ___U3CDisplayNameU3Ek__BackingField_0)); }
	inline String_t* get_U3CDisplayNameU3Ek__BackingField_0() const { return ___U3CDisplayNameU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CDisplayNameU3Ek__BackingField_0() { return &___U3CDisplayNameU3Ek__BackingField_0; }
	inline void set_U3CDisplayNameU3Ek__BackingField_0(String_t* value)
	{
		___U3CDisplayNameU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier(&___U3CDisplayNameU3Ek__BackingField_0, value);
	}

	inline static int32_t get_offset_of_U3COriginationU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(UserTitleInfo_t4099117963, ___U3COriginationU3Ek__BackingField_1)); }
	inline Nullable_1_t417810504  get_U3COriginationU3Ek__BackingField_1() const { return ___U3COriginationU3Ek__BackingField_1; }
	inline Nullable_1_t417810504 * get_address_of_U3COriginationU3Ek__BackingField_1() { return &___U3COriginationU3Ek__BackingField_1; }
	inline void set_U3COriginationU3Ek__BackingField_1(Nullable_1_t417810504  value)
	{
		___U3COriginationU3Ek__BackingField_1 = value;
	}

	inline static int32_t get_offset_of_U3CCreatedU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(UserTitleInfo_t4099117963, ___U3CCreatedU3Ek__BackingField_2)); }
	inline DateTime_t339033936  get_U3CCreatedU3Ek__BackingField_2() const { return ___U3CCreatedU3Ek__BackingField_2; }
	inline DateTime_t339033936 * get_address_of_U3CCreatedU3Ek__BackingField_2() { return &___U3CCreatedU3Ek__BackingField_2; }
	inline void set_U3CCreatedU3Ek__BackingField_2(DateTime_t339033936  value)
	{
		___U3CCreatedU3Ek__BackingField_2 = value;
	}

	inline static int32_t get_offset_of_U3CLastLoginU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(UserTitleInfo_t4099117963, ___U3CLastLoginU3Ek__BackingField_3)); }
	inline Nullable_1_t3225071844  get_U3CLastLoginU3Ek__BackingField_3() const { return ___U3CLastLoginU3Ek__BackingField_3; }
	inline Nullable_1_t3225071844 * get_address_of_U3CLastLoginU3Ek__BackingField_3() { return &___U3CLastLoginU3Ek__BackingField_3; }
	inline void set_U3CLastLoginU3Ek__BackingField_3(Nullable_1_t3225071844  value)
	{
		___U3CLastLoginU3Ek__BackingField_3 = value;
	}

	inline static int32_t get_offset_of_U3CFirstLoginU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(UserTitleInfo_t4099117963, ___U3CFirstLoginU3Ek__BackingField_4)); }
	inline Nullable_1_t3225071844  get_U3CFirstLoginU3Ek__BackingField_4() const { return ___U3CFirstLoginU3Ek__BackingField_4; }
	inline Nullable_1_t3225071844 * get_address_of_U3CFirstLoginU3Ek__BackingField_4() { return &___U3CFirstLoginU3Ek__BackingField_4; }
	inline void set_U3CFirstLoginU3Ek__BackingField_4(Nullable_1_t3225071844  value)
	{
		___U3CFirstLoginU3Ek__BackingField_4 = value;
	}

	inline static int32_t get_offset_of_U3CisBannedU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(UserTitleInfo_t4099117963, ___U3CisBannedU3Ek__BackingField_5)); }
	inline Nullable_1_t3097043249  get_U3CisBannedU3Ek__BackingField_5() const { return ___U3CisBannedU3Ek__BackingField_5; }
	inline Nullable_1_t3097043249 * get_address_of_U3CisBannedU3Ek__BackingField_5() { return &___U3CisBannedU3Ek__BackingField_5; }
	inline void set_U3CisBannedU3Ek__BackingField_5(Nullable_1_t3097043249  value)
	{
		___U3CisBannedU3Ek__BackingField_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
