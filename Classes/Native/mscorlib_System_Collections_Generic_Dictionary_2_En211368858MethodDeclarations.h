﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E3591453091MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,PlayFab.ClientModels.SharedGroupDataRecord>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define Enumerator__ctor_m1054493826(__this, ___dictionary0, method) ((  void (*) (Enumerator_t211368858 *, Dictionary_2_t444340917 *, const MethodInfo*))Enumerator__ctor_m3920831137_gshared)(__this, ___dictionary0, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.String,PlayFab.ClientModels.SharedGroupDataRecord>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m3948157673(__this, method) ((  Il2CppObject * (*) (Enumerator_t211368858 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m3262087712_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,PlayFab.ClientModels.SharedGroupDataRecord>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m895366899(__this, method) ((  void (*) (Enumerator_t211368858 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m2959141748_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<System.String,PlayFab.ClientModels.SharedGroupDataRecord>::System.Collections.IDictionaryEnumerator.get_Entry()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m2045026858(__this, method) ((  DictionaryEntry_t130027246  (*) (Enumerator_t211368858 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m2279524093_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.String,PlayFab.ClientModels.SharedGroupDataRecord>::System.Collections.IDictionaryEnumerator.get_Key()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m1903412485(__this, method) ((  Il2CppObject * (*) (Enumerator_t211368858 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m1201448700_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.String,PlayFab.ClientModels.SharedGroupDataRecord>::System.Collections.IDictionaryEnumerator.get_Value()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m571766359(__this, method) ((  Il2CppObject * (*) (Enumerator_t211368858 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m294434446_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.String,PlayFab.ClientModels.SharedGroupDataRecord>::MoveNext()
#define Enumerator_MoveNext_m3576059811(__this, method) ((  bool (*) (Enumerator_t211368858 *, const MethodInfo*))Enumerator_MoveNext_m217327200_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<System.String,PlayFab.ClientModels.SharedGroupDataRecord>::get_Current()
#define Enumerator_get_Current_m1524577209(__this, method) ((  KeyValuePair_2_t4227839511  (*) (Enumerator_t211368858 *, const MethodInfo*))Enumerator_get_Current_m4240003024_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<System.String,PlayFab.ClientModels.SharedGroupDataRecord>::get_CurrentKey()
#define Enumerator_get_CurrentKey_m1258747756(__this, method) ((  String_t* (*) (Enumerator_t211368858 *, const MethodInfo*))Enumerator_get_CurrentKey_m3062159917_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.String,PlayFab.ClientModels.SharedGroupDataRecord>::get_CurrentValue()
#define Enumerator_get_CurrentValue_m1586902572(__this, method) ((  SharedGroupDataRecord_t3101610309 * (*) (Enumerator_t211368858 *, const MethodInfo*))Enumerator_get_CurrentValue_m592783249_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,PlayFab.ClientModels.SharedGroupDataRecord>::Reset()
#define Enumerator_Reset_m275989460(__this, method) ((  void (*) (Enumerator_t211368858 *, const MethodInfo*))Enumerator_Reset_m3001375603_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,PlayFab.ClientModels.SharedGroupDataRecord>::VerifyState()
#define Enumerator_VerifyState_m4033963229(__this, method) ((  void (*) (Enumerator_t211368858 *, const MethodInfo*))Enumerator_VerifyState_m4290054460_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,PlayFab.ClientModels.SharedGroupDataRecord>::VerifyCurrent()
#define Enumerator_VerifyCurrent_m1028066565(__this, method) ((  void (*) (Enumerator_t211368858 *, const MethodInfo*))Enumerator_VerifyCurrent_m2318603684_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,PlayFab.ClientModels.SharedGroupDataRecord>::Dispose()
#define Enumerator_Dispose_m1461327780(__this, method) ((  void (*) (Enumerator_t211368858 *, const MethodInfo*))Enumerator_Dispose_m627360643_gshared)(__this, method)
