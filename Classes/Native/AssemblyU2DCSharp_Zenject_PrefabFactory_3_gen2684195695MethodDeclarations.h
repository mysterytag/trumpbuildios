﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Zenject.PrefabFactory`3<System.Object,System.Object,System.Object>
struct PrefabFactory_3_t2684195695;
// System.Object
struct Il2CppObject;
// UnityEngine.GameObject
struct GameObject_t4012695102;
// System.String
struct String_t;
// System.Collections.Generic.IEnumerable`1<Zenject.ZenjectResolveException>
struct IEnumerable_1_t4073207355;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_GameObject4012695102.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_String968488902.h"

// System.Void Zenject.PrefabFactory`3<System.Object,System.Object,System.Object>::.ctor()
extern "C"  void PrefabFactory_3__ctor_m1970864061_gshared (PrefabFactory_3_t2684195695 * __this, const MethodInfo* method);
#define PrefabFactory_3__ctor_m1970864061(__this, method) ((  void (*) (PrefabFactory_3_t2684195695 *, const MethodInfo*))PrefabFactory_3__ctor_m1970864061_gshared)(__this, method)
// T Zenject.PrefabFactory`3<System.Object,System.Object,System.Object>::Create(UnityEngine.GameObject,P1,P2)
extern "C"  Il2CppObject * PrefabFactory_3_Create_m523527129_gshared (PrefabFactory_3_t2684195695 * __this, GameObject_t4012695102 * ___prefab0, Il2CppObject * ___param1, Il2CppObject * ___param22, const MethodInfo* method);
#define PrefabFactory_3_Create_m523527129(__this, ___prefab0, ___param1, ___param22, method) ((  Il2CppObject * (*) (PrefabFactory_3_t2684195695 *, GameObject_t4012695102 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))PrefabFactory_3_Create_m523527129_gshared)(__this, ___prefab0, ___param1, ___param22, method)
// T Zenject.PrefabFactory`3<System.Object,System.Object,System.Object>::Create(System.String,P1,P2)
extern "C"  Il2CppObject * PrefabFactory_3_Create_m818429151_gshared (PrefabFactory_3_t2684195695 * __this, String_t* ___prefabResourceName0, Il2CppObject * ___param1, Il2CppObject * ___param22, const MethodInfo* method);
#define PrefabFactory_3_Create_m818429151(__this, ___prefabResourceName0, ___param1, ___param22, method) ((  Il2CppObject * (*) (PrefabFactory_3_t2684195695 *, String_t*, Il2CppObject *, Il2CppObject *, const MethodInfo*))PrefabFactory_3_Create_m818429151_gshared)(__this, ___prefabResourceName0, ___param1, ___param22, method)
// System.Collections.Generic.IEnumerable`1<Zenject.ZenjectResolveException> Zenject.PrefabFactory`3<System.Object,System.Object,System.Object>::Validate()
extern "C"  Il2CppObject* PrefabFactory_3_Validate_m882209180_gshared (PrefabFactory_3_t2684195695 * __this, const MethodInfo* method);
#define PrefabFactory_3_Validate_m882209180(__this, method) ((  Il2CppObject* (*) (PrefabFactory_3_t2684195695 *, const MethodInfo*))PrefabFactory_3_Validate_m882209180_gshared)(__this, method)
