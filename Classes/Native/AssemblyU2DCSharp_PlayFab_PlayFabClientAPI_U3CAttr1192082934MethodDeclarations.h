﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.PlayFabClientAPI/<AttributeInstall>c__AnonStoreyD6
struct U3CAttributeInstallU3Ec__AnonStoreyD6_t1192082934;
// PlayFab.CallRequestContainer
struct CallRequestContainer_t432318609;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharpU2Dfirstpass_PlayFab_CallRequestCo432318609.h"

// System.Void PlayFab.PlayFabClientAPI/<AttributeInstall>c__AnonStoreyD6::.ctor()
extern "C"  void U3CAttributeInstallU3Ec__AnonStoreyD6__ctor_m180347517 (U3CAttributeInstallU3Ec__AnonStoreyD6_t1192082934 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/<AttributeInstall>c__AnonStoreyD6::<>m__C3(PlayFab.CallRequestContainer)
extern "C"  void U3CAttributeInstallU3Ec__AnonStoreyD6_U3CU3Em__C3_m2368591083 (U3CAttributeInstallU3Ec__AnonStoreyD6_t1192082934 * __this, CallRequestContainer_t432318609 * ___requestContainer0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
