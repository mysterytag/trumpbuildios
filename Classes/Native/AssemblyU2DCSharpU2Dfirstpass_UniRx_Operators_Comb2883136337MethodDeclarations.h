﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.CombineLatestObservable`1<System.Boolean>
struct CombineLatestObservable_1_t2883136337;
// UniRx.IObservable`1<System.Boolean>[]
struct IObservable_1U5BU5D_t1703862084;
// System.IDisposable
struct IDisposable_t1628921374;
// UniRx.IObserver`1<System.Collections.Generic.IList`1<System.Boolean>>
struct IObserver_1_t294529262;

#include "codegen/il2cpp-codegen.h"

// System.Void UniRx.Operators.CombineLatestObservable`1<System.Boolean>::.ctor(UniRx.IObservable`1<T>[])
extern "C"  void CombineLatestObservable_1__ctor_m2219293560_gshared (CombineLatestObservable_1_t2883136337 * __this, IObservable_1U5BU5D_t1703862084* ___sources0, const MethodInfo* method);
#define CombineLatestObservable_1__ctor_m2219293560(__this, ___sources0, method) ((  void (*) (CombineLatestObservable_1_t2883136337 *, IObservable_1U5BU5D_t1703862084*, const MethodInfo*))CombineLatestObservable_1__ctor_m2219293560_gshared)(__this, ___sources0, method)
// System.IDisposable UniRx.Operators.CombineLatestObservable`1<System.Boolean>::SubscribeCore(UniRx.IObserver`1<System.Collections.Generic.IList`1<T>>,System.IDisposable)
extern "C"  Il2CppObject * CombineLatestObservable_1_SubscribeCore_m1252997412_gshared (CombineLatestObservable_1_t2883136337 * __this, Il2CppObject* ___observer0, Il2CppObject * ___cancel1, const MethodInfo* method);
#define CombineLatestObservable_1_SubscribeCore_m1252997412(__this, ___observer0, ___cancel1, method) ((  Il2CppObject * (*) (CombineLatestObservable_1_t2883136337 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))CombineLatestObservable_1_SubscribeCore_m1252997412_gshared)(__this, ___observer0, ___cancel1, method)
