﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Stream`1<System.DateTime>
struct Stream_1_t3325140254;
// System.IDisposable
struct IDisposable_t1628921374;
// System.Action`1<System.DateTime>
struct Action_1_t487486641;
// System.Action
struct Action_t437523947;
// IStream`1<System.DateTime>
struct IStream_1_t891724927;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_DateTime339033936.h"
#include "AssemblyU2DCSharp_Priority3194150340.h"
#include "System_Core_System_Action437523947.h"

// System.Void Stream`1<System.DateTime>::.ctor()
extern "C"  void Stream_1__ctor_m3308497500_gshared (Stream_1_t3325140254 * __this, const MethodInfo* method);
#define Stream_1__ctor_m3308497500(__this, method) ((  void (*) (Stream_1_t3325140254 *, const MethodInfo*))Stream_1__ctor_m3308497500_gshared)(__this, method)
// System.Void Stream`1<System.DateTime>::Send(T)
extern "C"  void Stream_1_Send_m1837715054_gshared (Stream_1_t3325140254 * __this, DateTime_t339033936  ___value0, const MethodInfo* method);
#define Stream_1_Send_m1837715054(__this, ___value0, method) ((  void (*) (Stream_1_t3325140254 *, DateTime_t339033936 , const MethodInfo*))Stream_1_Send_m1837715054_gshared)(__this, ___value0, method)
// System.Void Stream`1<System.DateTime>::SendInTransaction(T,System.Int32)
extern "C"  void Stream_1_SendInTransaction_m1243591812_gshared (Stream_1_t3325140254 * __this, DateTime_t339033936  ___value0, int32_t ___p1, const MethodInfo* method);
#define Stream_1_SendInTransaction_m1243591812(__this, ___value0, ___p1, method) ((  void (*) (Stream_1_t3325140254 *, DateTime_t339033936 , int32_t, const MethodInfo*))Stream_1_SendInTransaction_m1243591812_gshared)(__this, ___value0, ___p1, method)
// System.IDisposable Stream`1<System.DateTime>::Listen(System.Action`1<T>,Priority)
extern "C"  Il2CppObject * Stream_1_Listen_m970467974_gshared (Stream_1_t3325140254 * __this, Action_1_t487486641 * ___action0, int32_t ___priority1, const MethodInfo* method);
#define Stream_1_Listen_m970467974(__this, ___action0, ___priority1, method) ((  Il2CppObject * (*) (Stream_1_t3325140254 *, Action_1_t487486641 *, int32_t, const MethodInfo*))Stream_1_Listen_m970467974_gshared)(__this, ___action0, ___priority1, method)
// System.IDisposable Stream`1<System.DateTime>::Listen(System.Action,Priority)
extern "C"  Il2CppObject * Stream_1_Listen_m2976766689_gshared (Stream_1_t3325140254 * __this, Action_t437523947 * ___action0, int32_t ___p1, const MethodInfo* method);
#define Stream_1_Listen_m2976766689(__this, ___action0, ___p1, method) ((  Il2CppObject * (*) (Stream_1_t3325140254 *, Action_t437523947 *, int32_t, const MethodInfo*))Stream_1_Listen_m2976766689_gshared)(__this, ___action0, ___p1, method)
// System.Void Stream`1<System.DateTime>::Dispose()
extern "C"  void Stream_1_Dispose_m1088581529_gshared (Stream_1_t3325140254 * __this, const MethodInfo* method);
#define Stream_1_Dispose_m1088581529(__this, method) ((  void (*) (Stream_1_t3325140254 *, const MethodInfo*))Stream_1_Dispose_m1088581529_gshared)(__this, method)
// System.Void Stream`1<System.DateTime>::SetInputStream(IStream`1<T>)
extern "C"  void Stream_1_SetInputStream_m1502279460_gshared (Stream_1_t3325140254 * __this, Il2CppObject* ___stream0, const MethodInfo* method);
#define Stream_1_SetInputStream_m1502279460(__this, ___stream0, method) ((  void (*) (Stream_1_t3325140254 *, Il2CppObject*, const MethodInfo*))Stream_1_SetInputStream_m1502279460_gshared)(__this, ___stream0, method)
// System.Void Stream`1<System.DateTime>::ClearInputStream()
extern "C"  void Stream_1_ClearInputStream_m3589965061_gshared (Stream_1_t3325140254 * __this, const MethodInfo* method);
#define Stream_1_ClearInputStream_m3589965061(__this, method) ((  void (*) (Stream_1_t3325140254 *, const MethodInfo*))Stream_1_ClearInputStream_m3589965061_gshared)(__this, method)
// System.Void Stream`1<System.DateTime>::TransactionIterationFinished()
extern "C"  void Stream_1_TransactionIterationFinished_m2874008921_gshared (Stream_1_t3325140254 * __this, const MethodInfo* method);
#define Stream_1_TransactionIterationFinished_m2874008921(__this, method) ((  void (*) (Stream_1_t3325140254 *, const MethodInfo*))Stream_1_TransactionIterationFinished_m2874008921_gshared)(__this, method)
// System.Void Stream`1<System.DateTime>::Unpack(System.Int32)
extern "C"  void Stream_1_Unpack_m1379532715_gshared (Stream_1_t3325140254 * __this, int32_t ___p0, const MethodInfo* method);
#define Stream_1_Unpack_m1379532715(__this, ___p0, method) ((  void (*) (Stream_1_t3325140254 *, int32_t, const MethodInfo*))Stream_1_Unpack_m1379532715_gshared)(__this, ___p0, method)
// System.Void Stream`1<System.DateTime>::<SetInputStream>m__1BA(T)
extern "C"  void Stream_1_U3CSetInputStreamU3Em__1BA_m916834809_gshared (Stream_1_t3325140254 * __this, DateTime_t339033936  ___val0, const MethodInfo* method);
#define Stream_1_U3CSetInputStreamU3Em__1BA_m916834809(__this, ___val0, method) ((  void (*) (Stream_1_t3325140254 *, DateTime_t339033936 , const MethodInfo*))Stream_1_U3CSetInputStreamU3Em__1BA_m916834809_gshared)(__this, ___val0, method)
