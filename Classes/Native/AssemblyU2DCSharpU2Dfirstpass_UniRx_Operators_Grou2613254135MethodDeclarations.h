﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.GroupedObservable`2<System.Object,System.Object>
struct GroupedObservable_2_t2613254135;
// System.Object
struct Il2CppObject;
// UniRx.ISubject`1<System.Object>
struct ISubject_1_t353709935;
// UniRx.RefCountDisposable
struct RefCountDisposable_t3288429070;
// System.IDisposable
struct IDisposable_t1628921374;
// UniRx.IObserver`1<System.Object>
struct IObserver_1_t3049105323;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_RefCountDispos3288429070.h"

// System.Void UniRx.Operators.GroupedObservable`2<System.Object,System.Object>::.ctor(TKey,UniRx.ISubject`1<TElement>,UniRx.RefCountDisposable)
extern "C"  void GroupedObservable_2__ctor_m1743351720_gshared (GroupedObservable_2_t2613254135 * __this, Il2CppObject * ___key0, Il2CppObject* ___subject1, RefCountDisposable_t3288429070 * ___refCount2, const MethodInfo* method);
#define GroupedObservable_2__ctor_m1743351720(__this, ___key0, ___subject1, ___refCount2, method) ((  void (*) (GroupedObservable_2_t2613254135 *, Il2CppObject *, Il2CppObject*, RefCountDisposable_t3288429070 *, const MethodInfo*))GroupedObservable_2__ctor_m1743351720_gshared)(__this, ___key0, ___subject1, ___refCount2, method)
// TKey UniRx.Operators.GroupedObservable`2<System.Object,System.Object>::get_Key()
extern "C"  Il2CppObject * GroupedObservable_2_get_Key_m665794075_gshared (GroupedObservable_2_t2613254135 * __this, const MethodInfo* method);
#define GroupedObservable_2_get_Key_m665794075(__this, method) ((  Il2CppObject * (*) (GroupedObservable_2_t2613254135 *, const MethodInfo*))GroupedObservable_2_get_Key_m665794075_gshared)(__this, method)
// System.IDisposable UniRx.Operators.GroupedObservable`2<System.Object,System.Object>::Subscribe(UniRx.IObserver`1<TElement>)
extern "C"  Il2CppObject * GroupedObservable_2_Subscribe_m3495507058_gshared (GroupedObservable_2_t2613254135 * __this, Il2CppObject* ___observer0, const MethodInfo* method);
#define GroupedObservable_2_Subscribe_m3495507058(__this, ___observer0, method) ((  Il2CppObject * (*) (GroupedObservable_2_t2613254135 *, Il2CppObject*, const MethodInfo*))GroupedObservable_2_Subscribe_m3495507058_gshared)(__this, ___observer0, method)
