﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "AssemblyU2DCSharp_PlayFab_Internal_PlayFabResultCom379512675.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PlayFab.ClientModels.GetPhotonAuthenticationTokenResult
struct  GetPhotonAuthenticationTokenResult_t3031300796  : public PlayFabResultCommon_t379512675
{
public:
	// System.String PlayFab.ClientModels.GetPhotonAuthenticationTokenResult::<PhotonCustomAuthenticationToken>k__BackingField
	String_t* ___U3CPhotonCustomAuthenticationTokenU3Ek__BackingField_2;

public:
	inline static int32_t get_offset_of_U3CPhotonCustomAuthenticationTokenU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(GetPhotonAuthenticationTokenResult_t3031300796, ___U3CPhotonCustomAuthenticationTokenU3Ek__BackingField_2)); }
	inline String_t* get_U3CPhotonCustomAuthenticationTokenU3Ek__BackingField_2() const { return ___U3CPhotonCustomAuthenticationTokenU3Ek__BackingField_2; }
	inline String_t** get_address_of_U3CPhotonCustomAuthenticationTokenU3Ek__BackingField_2() { return &___U3CPhotonCustomAuthenticationTokenU3Ek__BackingField_2; }
	inline void set_U3CPhotonCustomAuthenticationTokenU3Ek__BackingField_2(String_t* value)
	{
		___U3CPhotonCustomAuthenticationTokenU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier(&___U3CPhotonCustomAuthenticationTokenU3Ek__BackingField_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
