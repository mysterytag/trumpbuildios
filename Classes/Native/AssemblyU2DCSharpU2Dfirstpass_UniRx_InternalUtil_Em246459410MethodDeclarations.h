﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.InternalUtil.EmptyObserver`1<System.Object>
struct EmptyObserver_1_t246459410;
// System.Exception
struct Exception_t1967233988;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Exception1967233988.h"
#include "mscorlib_System_Object837106420.h"

// System.Void UniRx.InternalUtil.EmptyObserver`1<System.Object>::.ctor()
extern "C"  void EmptyObserver_1__ctor_m968110296_gshared (EmptyObserver_1_t246459410 * __this, const MethodInfo* method);
#define EmptyObserver_1__ctor_m968110296(__this, method) ((  void (*) (EmptyObserver_1_t246459410 *, const MethodInfo*))EmptyObserver_1__ctor_m968110296_gshared)(__this, method)
// System.Void UniRx.InternalUtil.EmptyObserver`1<System.Object>::.cctor()
extern "C"  void EmptyObserver_1__cctor_m3759519189_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define EmptyObserver_1__cctor_m3759519189(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))EmptyObserver_1__cctor_m3759519189_gshared)(__this /* static, unused */, method)
// System.Void UniRx.InternalUtil.EmptyObserver`1<System.Object>::OnCompleted()
extern "C"  void EmptyObserver_1_OnCompleted_m1627218530_gshared (EmptyObserver_1_t246459410 * __this, const MethodInfo* method);
#define EmptyObserver_1_OnCompleted_m1627218530(__this, method) ((  void (*) (EmptyObserver_1_t246459410 *, const MethodInfo*))EmptyObserver_1_OnCompleted_m1627218530_gshared)(__this, method)
// System.Void UniRx.InternalUtil.EmptyObserver`1<System.Object>::OnError(System.Exception)
extern "C"  void EmptyObserver_1_OnError_m1624084623_gshared (EmptyObserver_1_t246459410 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method);
#define EmptyObserver_1_OnError_m1624084623(__this, ___error0, method) ((  void (*) (EmptyObserver_1_t246459410 *, Exception_t1967233988 *, const MethodInfo*))EmptyObserver_1_OnError_m1624084623_gshared)(__this, ___error0, method)
// System.Void UniRx.InternalUtil.EmptyObserver`1<System.Object>::OnNext(T)
extern "C"  void EmptyObserver_1_OnNext_m1176794496_gshared (EmptyObserver_1_t246459410 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define EmptyObserver_1_OnNext_m1176794496(__this, ___value0, method) ((  void (*) (EmptyObserver_1_t246459410 *, Il2CppObject *, const MethodInfo*))EmptyObserver_1_OnNext_m1176794496_gshared)(__this, ___value0, method)
