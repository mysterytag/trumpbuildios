﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.ClientModels.GetFriendsListResult
struct GetFriendsListResult_t2794171722;
// System.Collections.Generic.List`1<PlayFab.ClientModels.FriendInfo>
struct List_1_t210693525;

#include "codegen/il2cpp-codegen.h"

// System.Void PlayFab.ClientModels.GetFriendsListResult::.ctor()
extern "C"  void GetFriendsListResult__ctor_m476950835 (GetFriendsListResult_t2794171722 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<PlayFab.ClientModels.FriendInfo> PlayFab.ClientModels.GetFriendsListResult::get_Friends()
extern "C"  List_1_t210693525 * GetFriendsListResult_get_Friends_m1569731336 (GetFriendsListResult_t2794171722 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.GetFriendsListResult::set_Friends(System.Collections.Generic.List`1<PlayFab.ClientModels.FriendInfo>)
extern "C"  void GetFriendsListResult_set_Friends_m4063336855 (GetFriendsListResult_t2794171722 * __this, List_1_t210693525 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
