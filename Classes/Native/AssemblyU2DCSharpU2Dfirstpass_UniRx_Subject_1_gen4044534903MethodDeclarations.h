﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Subject`1<UniRx.Tuple`2<System.Object,UnityEngine.NetworkMessageInfo>>
struct Subject_1_t4044534903;
// System.Exception
struct Exception_t1967233988;
// System.IDisposable
struct IDisposable_t1628921374;
// UniRx.IObserver`1<UniRx.Tuple`2<System.Object,UnityEngine.NetworkMessageInfo>>
struct IObserver_1_t23531890;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Exception1967233988.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Tuple_2_gen2106500283.h"

// System.Void UniRx.Subject`1<UniRx.Tuple`2<System.Object,UnityEngine.NetworkMessageInfo>>::.ctor()
extern "C"  void Subject_1__ctor_m1110123634_gshared (Subject_1_t4044534903 * __this, const MethodInfo* method);
#define Subject_1__ctor_m1110123634(__this, method) ((  void (*) (Subject_1_t4044534903 *, const MethodInfo*))Subject_1__ctor_m1110123634_gshared)(__this, method)
// System.Boolean UniRx.Subject`1<UniRx.Tuple`2<System.Object,UnityEngine.NetworkMessageInfo>>::get_HasObservers()
extern "C"  bool Subject_1_get_HasObservers_m856497370_gshared (Subject_1_t4044534903 * __this, const MethodInfo* method);
#define Subject_1_get_HasObservers_m856497370(__this, method) ((  bool (*) (Subject_1_t4044534903 *, const MethodInfo*))Subject_1_get_HasObservers_m856497370_gshared)(__this, method)
// System.Void UniRx.Subject`1<UniRx.Tuple`2<System.Object,UnityEngine.NetworkMessageInfo>>::OnCompleted()
extern "C"  void Subject_1_OnCompleted_m363969148_gshared (Subject_1_t4044534903 * __this, const MethodInfo* method);
#define Subject_1_OnCompleted_m363969148(__this, method) ((  void (*) (Subject_1_t4044534903 *, const MethodInfo*))Subject_1_OnCompleted_m363969148_gshared)(__this, method)
// System.Void UniRx.Subject`1<UniRx.Tuple`2<System.Object,UnityEngine.NetworkMessageInfo>>::OnError(System.Exception)
extern "C"  void Subject_1_OnError_m3943469481_gshared (Subject_1_t4044534903 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method);
#define Subject_1_OnError_m3943469481(__this, ___error0, method) ((  void (*) (Subject_1_t4044534903 *, Exception_t1967233988 *, const MethodInfo*))Subject_1_OnError_m3943469481_gshared)(__this, ___error0, method)
// System.Void UniRx.Subject`1<UniRx.Tuple`2<System.Object,UnityEngine.NetworkMessageInfo>>::OnNext(T)
extern "C"  void Subject_1_OnNext_m212658842_gshared (Subject_1_t4044534903 * __this, Tuple_2_t2106500283  ___value0, const MethodInfo* method);
#define Subject_1_OnNext_m212658842(__this, ___value0, method) ((  void (*) (Subject_1_t4044534903 *, Tuple_2_t2106500283 , const MethodInfo*))Subject_1_OnNext_m212658842_gshared)(__this, ___value0, method)
// System.IDisposable UniRx.Subject`1<UniRx.Tuple`2<System.Object,UnityEngine.NetworkMessageInfo>>::Subscribe(UniRx.IObserver`1<T>)
extern "C"  Il2CppObject * Subject_1_Subscribe_m314425265_gshared (Subject_1_t4044534903 * __this, Il2CppObject* ___observer0, const MethodInfo* method);
#define Subject_1_Subscribe_m314425265(__this, ___observer0, method) ((  Il2CppObject * (*) (Subject_1_t4044534903 *, Il2CppObject*, const MethodInfo*))Subject_1_Subscribe_m314425265_gshared)(__this, ___observer0, method)
// System.Void UniRx.Subject`1<UniRx.Tuple`2<System.Object,UnityEngine.NetworkMessageInfo>>::Dispose()
extern "C"  void Subject_1_Dispose_m1575205935_gshared (Subject_1_t4044534903 * __this, const MethodInfo* method);
#define Subject_1_Dispose_m1575205935(__this, method) ((  void (*) (Subject_1_t4044534903 *, const MethodInfo*))Subject_1_Dispose_m1575205935_gshared)(__this, method)
// System.Void UniRx.Subject`1<UniRx.Tuple`2<System.Object,UnityEngine.NetworkMessageInfo>>::ThrowIfDisposed()
extern "C"  void Subject_1_ThrowIfDisposed_m1410713272_gshared (Subject_1_t4044534903 * __this, const MethodInfo* method);
#define Subject_1_ThrowIfDisposed_m1410713272(__this, method) ((  void (*) (Subject_1_t4044534903 *, const MethodInfo*))Subject_1_ThrowIfDisposed_m1410713272_gshared)(__this, method)
// System.Boolean UniRx.Subject`1<UniRx.Tuple`2<System.Object,UnityEngine.NetworkMessageInfo>>::IsRequiredSubscribeOnCurrentThread()
extern "C"  bool Subject_1_IsRequiredSubscribeOnCurrentThread_m75614033_gshared (Subject_1_t4044534903 * __this, const MethodInfo* method);
#define Subject_1_IsRequiredSubscribeOnCurrentThread_m75614033(__this, method) ((  bool (*) (Subject_1_t4044534903 *, const MethodInfo*))Subject_1_IsRequiredSubscribeOnCurrentThread_m75614033_gshared)(__this, method)
