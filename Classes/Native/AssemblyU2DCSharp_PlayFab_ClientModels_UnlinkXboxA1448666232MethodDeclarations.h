﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.ClientModels.UnlinkXboxAccountRequest
struct UnlinkXboxAccountRequest_t1448666232;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String968488902.h"

// System.Void PlayFab.ClientModels.UnlinkXboxAccountRequest::.ctor()
extern "C"  void UnlinkXboxAccountRequest__ctor_m1909999813 (UnlinkXboxAccountRequest_t1448666232 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PlayFab.ClientModels.UnlinkXboxAccountRequest::get_XboxToken()
extern "C"  String_t* UnlinkXboxAccountRequest_get_XboxToken_m1589018947 (UnlinkXboxAccountRequest_t1448666232 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.UnlinkXboxAccountRequest::set_XboxToken(System.String)
extern "C"  void UnlinkXboxAccountRequest_set_XboxToken_m3920601366 (UnlinkXboxAccountRequest_t1448666232 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
