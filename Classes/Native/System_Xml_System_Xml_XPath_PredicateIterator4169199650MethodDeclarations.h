﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Xml.XPath.PredicateIterator
struct PredicateIterator_t4169199650;
// System.Xml.XPath.BaseIterator
struct BaseIterator_t3696600956;
// System.Xml.XPath.Expression
struct Expression_t4217024437;
// System.Xml.XPath.XPathNodeIterator
struct XPathNodeIterator_t2394191562;
// System.Xml.XPath.XPathNavigator
struct XPathNavigator_t1624538935;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "System_Xml_System_Xml_XPath_BaseIterator3696600956.h"
#include "System_Xml_System_Xml_XPath_Expression4217024437.h"
#include "System_Xml_System_Xml_XPath_PredicateIterator4169199650.h"

// System.Void System.Xml.XPath.PredicateIterator::.ctor(System.Xml.XPath.BaseIterator,System.Xml.XPath.Expression)
extern "C"  void PredicateIterator__ctor_m3004206002 (PredicateIterator_t4169199650 * __this, BaseIterator_t3696600956 * ___iter0, Expression_t4217024437 * ___pred1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Xml.XPath.PredicateIterator::.ctor(System.Xml.XPath.PredicateIterator)
extern "C"  void PredicateIterator__ctor_m1246946409 (PredicateIterator_t4169199650 * __this, PredicateIterator_t4169199650 * ___other0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Xml.XPath.XPathNodeIterator System.Xml.XPath.PredicateIterator::Clone()
extern "C"  XPathNodeIterator_t2394191562 * PredicateIterator_Clone_m2751366551 (PredicateIterator_t4169199650 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Xml.XPath.PredicateIterator::MoveNextCore()
extern "C"  bool PredicateIterator_MoveNextCore_m3008221096 (PredicateIterator_t4169199650 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Xml.XPath.XPathNavigator System.Xml.XPath.PredicateIterator::get_Current()
extern "C"  XPathNavigator_t1624538935 * PredicateIterator_get_Current_m508558563 (PredicateIterator_t4169199650 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Xml.XPath.PredicateIterator::get_ReverseAxis()
extern "C"  bool PredicateIterator_get_ReverseAxis_m1577932535 (PredicateIterator_t4169199650 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Xml.XPath.PredicateIterator::ToString()
extern "C"  String_t* PredicateIterator_ToString_m891628812 (PredicateIterator_t4169199650 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
