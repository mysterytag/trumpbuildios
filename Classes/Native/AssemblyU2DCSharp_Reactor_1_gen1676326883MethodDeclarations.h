﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Reactor`1<UniRx.CollectionAddEvent`1<System.Object>>
struct Reactor_1_t1676326883;
// System.IDisposable
struct IDisposable_t1628921374;
// System.Action`1<UniRx.CollectionAddEvent`1<System.Object>>
struct Action_1_t2564974692;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_CollectionAddE2416521987.h"
#include "AssemblyU2DCSharp_Priority3194150340.h"

// System.Void Reactor`1<UniRx.CollectionAddEvent`1<System.Object>>::.ctor()
extern "C"  void Reactor_1__ctor_m3042049264_gshared (Reactor_1_t1676326883 * __this, const MethodInfo* method);
#define Reactor_1__ctor_m3042049264(__this, method) ((  void (*) (Reactor_1_t1676326883 *, const MethodInfo*))Reactor_1__ctor_m3042049264_gshared)(__this, method)
// System.Void Reactor`1<UniRx.CollectionAddEvent`1<System.Object>>::React(T)
extern "C"  void Reactor_1_React_m3645908785_gshared (Reactor_1_t1676326883 * __this, CollectionAddEvent_1_t2416521987  ___t0, const MethodInfo* method);
#define Reactor_1_React_m3645908785(__this, ___t0, method) ((  void (*) (Reactor_1_t1676326883 *, CollectionAddEvent_1_t2416521987 , const MethodInfo*))Reactor_1_React_m3645908785_gshared)(__this, ___t0, method)
// System.Void Reactor`1<UniRx.CollectionAddEvent`1<System.Object>>::TransactionUnpackReact(T,System.Int32)
extern "C"  void Reactor_1_TransactionUnpackReact_m2959551868_gshared (Reactor_1_t1676326883 * __this, CollectionAddEvent_1_t2416521987  ___t0, int32_t ___i1, const MethodInfo* method);
#define Reactor_1_TransactionUnpackReact_m2959551868(__this, ___t0, ___i1, method) ((  void (*) (Reactor_1_t1676326883 *, CollectionAddEvent_1_t2416521987 , int32_t, const MethodInfo*))Reactor_1_TransactionUnpackReact_m2959551868_gshared)(__this, ___t0, ___i1, method)
// System.Boolean Reactor`1<UniRx.CollectionAddEvent`1<System.Object>>::Empty()
extern "C"  bool Reactor_1_Empty_m1349981671_gshared (Reactor_1_t1676326883 * __this, const MethodInfo* method);
#define Reactor_1_Empty_m1349981671(__this, method) ((  bool (*) (Reactor_1_t1676326883 *, const MethodInfo*))Reactor_1_Empty_m1349981671_gshared)(__this, method)
// System.IDisposable Reactor`1<UniRx.CollectionAddEvent`1<System.Object>>::AddReaction(System.Action`1<T>,Priority)
extern "C"  Il2CppObject * Reactor_1_AddReaction_m243157725_gshared (Reactor_1_t1676326883 * __this, Action_1_t2564974692 * ___reaction0, int32_t ___priority1, const MethodInfo* method);
#define Reactor_1_AddReaction_m243157725(__this, ___reaction0, ___priority1, method) ((  Il2CppObject * (*) (Reactor_1_t1676326883 *, Action_1_t2564974692 *, int32_t, const MethodInfo*))Reactor_1_AddReaction_m243157725_gshared)(__this, ___reaction0, ___priority1, method)
// System.Void Reactor`1<UniRx.CollectionAddEvent`1<System.Object>>::UpgradeToMultyPriority()
extern "C"  void Reactor_1_UpgradeToMultyPriority_m1822749610_gshared (Reactor_1_t1676326883 * __this, const MethodInfo* method);
#define Reactor_1_UpgradeToMultyPriority_m1822749610(__this, method) ((  void (*) (Reactor_1_t1676326883 *, const MethodInfo*))Reactor_1_UpgradeToMultyPriority_m1822749610_gshared)(__this, method)
// System.Void Reactor`1<UniRx.CollectionAddEvent`1<System.Object>>::AddToMultipriority(System.Action`1<T>,Priority)
extern "C"  void Reactor_1_AddToMultipriority_m625386573_gshared (Reactor_1_t1676326883 * __this, Action_1_t2564974692 * ___reaction0, int32_t ___p1, const MethodInfo* method);
#define Reactor_1_AddToMultipriority_m625386573(__this, ___reaction0, ___p1, method) ((  void (*) (Reactor_1_t1676326883 *, Action_1_t2564974692 *, int32_t, const MethodInfo*))Reactor_1_AddToMultipriority_m625386573_gshared)(__this, ___reaction0, ___p1, method)
// System.Void Reactor`1<UniRx.CollectionAddEvent`1<System.Object>>::RemoveReaction(System.Action`1<T>,Priority)
extern "C"  void Reactor_1_RemoveReaction_m3790334913_gshared (Reactor_1_t1676326883 * __this, Action_1_t2564974692 * ___reaction0, int32_t ___p1, const MethodInfo* method);
#define Reactor_1_RemoveReaction_m3790334913(__this, ___reaction0, ___p1, method) ((  void (*) (Reactor_1_t1676326883 *, Action_1_t2564974692 *, int32_t, const MethodInfo*))Reactor_1_RemoveReaction_m3790334913_gshared)(__this, ___reaction0, ___p1, method)
// System.Void Reactor`1<UniRx.CollectionAddEvent`1<System.Object>>::Clear()
extern "C"  void Reactor_1_Clear_m448182555_gshared (Reactor_1_t1676326883 * __this, const MethodInfo* method);
#define Reactor_1_Clear_m448182555(__this, method) ((  void (*) (Reactor_1_t1676326883 *, const MethodInfo*))Reactor_1_Clear_m448182555_gshared)(__this, method)
