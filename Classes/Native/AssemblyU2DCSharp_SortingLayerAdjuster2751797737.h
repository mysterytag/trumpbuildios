﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "UnityEngine_UnityEngine_MonoBehaviour3012272455.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SortingLayerAdjuster
struct  SortingLayerAdjuster_t2751797737  : public MonoBehaviour_t3012272455
{
public:
	// System.String SortingLayerAdjuster::sortingLayer
	String_t* ___sortingLayer_2;

public:
	inline static int32_t get_offset_of_sortingLayer_2() { return static_cast<int32_t>(offsetof(SortingLayerAdjuster_t2751797737, ___sortingLayer_2)); }
	inline String_t* get_sortingLayer_2() const { return ___sortingLayer_2; }
	inline String_t** get_address_of_sortingLayer_2() { return &___sortingLayer_2; }
	inline void set_sortingLayer_2(String_t* value)
	{
		___sortingLayer_2 = value;
		Il2CppCodeGenWriteBarrier(&___sortingLayer_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
