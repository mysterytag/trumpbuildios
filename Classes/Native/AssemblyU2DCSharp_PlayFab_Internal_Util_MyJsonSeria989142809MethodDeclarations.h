﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.Internal.Util/MyJsonSerializerStrategy
struct MyJsonSerializerStrategy_t989142809;
// System.Object
struct Il2CppObject;
// System.Type
struct Type_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_Type2779229935.h"

// System.Void PlayFab.Internal.Util/MyJsonSerializerStrategy::.ctor()
extern "C"  void MyJsonSerializerStrategy__ctor_m2413916747 (MyJsonSerializerStrategy_t989142809 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object PlayFab.Internal.Util/MyJsonSerializerStrategy::DeserializeObject(System.Object,System.Type)
extern "C"  Il2CppObject * MyJsonSerializerStrategy_DeserializeObject_m3637538901 (MyJsonSerializerStrategy_t989142809 * __this, Il2CppObject * ___value0, Type_t * ___type1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PlayFab.Internal.Util/MyJsonSerializerStrategy::TrySerializeKnownTypes(System.Object,System.Object&)
extern "C"  bool MyJsonSerializerStrategy_TrySerializeKnownTypes_m1576050290 (MyJsonSerializerStrategy_t989142809 * __this, Il2CppObject * ___input0, Il2CppObject ** ___output1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
