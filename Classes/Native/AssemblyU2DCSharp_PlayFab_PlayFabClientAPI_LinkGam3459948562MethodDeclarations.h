﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.PlayFabClientAPI/LinkGameCenterAccountResponseCallback
struct LinkGameCenterAccountResponseCallback_t3459948562;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;
// PlayFab.ClientModels.LinkGameCenterAccountRequest
struct LinkGameCenterAccountRequest_t355908787;
// PlayFab.ClientModels.LinkGameCenterAccountResult
struct LinkGameCenterAccountResult_t1155007385;
// PlayFab.PlayFabError
struct PlayFabError_t750598646;
// System.IAsyncResult
struct IAsyncResult_t537683269;
// System.AsyncCallback
struct AsyncCallback_t1363551830;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_IntPtr676692020.h"
#include "mscorlib_System_String968488902.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_LinkGameCent355908787.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_LinkGameCen1155007385.h"
#include "AssemblyU2DCSharpU2Dfirstpass_PlayFab_PlayFabError750598646.h"
#include "mscorlib_System_AsyncCallback1363551830.h"

// System.Void PlayFab.PlayFabClientAPI/LinkGameCenterAccountResponseCallback::.ctor(System.Object,System.IntPtr)
extern "C"  void LinkGameCenterAccountResponseCallback__ctor_m913813409 (LinkGameCenterAccountResponseCallback_t3459948562 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/LinkGameCenterAccountResponseCallback::Invoke(System.String,System.Int32,PlayFab.ClientModels.LinkGameCenterAccountRequest,PlayFab.ClientModels.LinkGameCenterAccountResult,PlayFab.PlayFabError,System.Object)
extern "C"  void LinkGameCenterAccountResponseCallback_Invoke_m3268760740 (LinkGameCenterAccountResponseCallback_t3459948562 * __this, String_t* ___urlPath0, int32_t ___callId1, LinkGameCenterAccountRequest_t355908787 * ___request2, LinkGameCenterAccountResult_t1155007385 * ___result3, PlayFabError_t750598646 * ___error4, Il2CppObject * ___customData5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
extern "C" void pinvoke_delegate_wrapper_LinkGameCenterAccountResponseCallback_t3459948562(Il2CppObject* delegate, String_t* ___urlPath0, int32_t ___callId1, LinkGameCenterAccountRequest_t355908787 * ___request2, LinkGameCenterAccountResult_t1155007385 * ___result3, PlayFabError_t750598646 * ___error4, Il2CppObject * ___customData5);
// System.IAsyncResult PlayFab.PlayFabClientAPI/LinkGameCenterAccountResponseCallback::BeginInvoke(System.String,System.Int32,PlayFab.ClientModels.LinkGameCenterAccountRequest,PlayFab.ClientModels.LinkGameCenterAccountResult,PlayFab.PlayFabError,System.Object,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * LinkGameCenterAccountResponseCallback_BeginInvoke_m1218235245 (LinkGameCenterAccountResponseCallback_t3459948562 * __this, String_t* ___urlPath0, int32_t ___callId1, LinkGameCenterAccountRequest_t355908787 * ___request2, LinkGameCenterAccountResult_t1155007385 * ___result3, PlayFabError_t750598646 * ___error4, Il2CppObject * ___customData5, AsyncCallback_t1363551830 * ___callback6, Il2CppObject * ___object7, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/LinkGameCenterAccountResponseCallback::EndInvoke(System.IAsyncResult)
extern "C"  void LinkGameCenterAccountResponseCallback_EndInvoke_m2324568625 (LinkGameCenterAccountResponseCallback_t3459948562 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
