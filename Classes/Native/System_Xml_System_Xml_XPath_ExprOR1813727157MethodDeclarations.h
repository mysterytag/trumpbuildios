﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Xml.XPath.ExprOR
struct ExprOR_t1813727157;
// System.Xml.XPath.Expression
struct Expression_t4217024437;
// System.String
struct String_t;
// System.Xml.XPath.BaseIterator
struct BaseIterator_t3696600956;

#include "codegen/il2cpp-codegen.h"
#include "System_Xml_System_Xml_XPath_Expression4217024437.h"
#include "System_Xml_System_Xml_XPath_BaseIterator3696600956.h"

// System.Void System.Xml.XPath.ExprOR::.ctor(System.Xml.XPath.Expression,System.Xml.XPath.Expression)
extern "C"  void ExprOR__ctor_m1067593566 (ExprOR_t1813727157 * __this, Expression_t4217024437 * ___left0, Expression_t4217024437 * ___right1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Xml.XPath.ExprOR::get_Operator()
extern "C"  String_t* ExprOR_get_Operator_m510476012 (ExprOR_t1813727157 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Xml.XPath.ExprOR::get_StaticValueAsBoolean()
extern "C"  bool ExprOR_get_StaticValueAsBoolean_m1871030710 (ExprOR_t1813727157 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Xml.XPath.ExprOR::EvaluateBoolean(System.Xml.XPath.BaseIterator)
extern "C"  bool ExprOR_EvaluateBoolean_m2492968597 (ExprOR_t1813727157 * __this, BaseIterator_t3696600956 * ___iter0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
