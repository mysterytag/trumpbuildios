﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.BufferObservable`2<System.Int64,System.Int64>
struct BufferObservable_2_t550437669;
// UniRx.IObservable`1<System.Int64>
struct IObservable_1_t2606213246;
// System.IDisposable
struct IDisposable_t1628921374;
// UniRx.IObserver`1<System.Collections.Generic.IList`1<System.Int64>>
struct IObserver_1_t2930938803;

#include "codegen/il2cpp-codegen.h"

// System.Void UniRx.Operators.BufferObservable`2<System.Int64,System.Int64>::.ctor(UniRx.IObservable`1<TSource>,UniRx.IObservable`1<TWindowBoundary>)
extern "C"  void BufferObservable_2__ctor_m1284182320_gshared (BufferObservable_2_t550437669 * __this, Il2CppObject* ___source0, Il2CppObject* ___windowBoundaries1, const MethodInfo* method);
#define BufferObservable_2__ctor_m1284182320(__this, ___source0, ___windowBoundaries1, method) ((  void (*) (BufferObservable_2_t550437669 *, Il2CppObject*, Il2CppObject*, const MethodInfo*))BufferObservable_2__ctor_m1284182320_gshared)(__this, ___source0, ___windowBoundaries1, method)
// System.IDisposable UniRx.Operators.BufferObservable`2<System.Int64,System.Int64>::SubscribeCore(UniRx.IObserver`1<System.Collections.Generic.IList`1<TSource>>,System.IDisposable)
extern "C"  Il2CppObject * BufferObservable_2_SubscribeCore_m666888389_gshared (BufferObservable_2_t550437669 * __this, Il2CppObject* ___observer0, Il2CppObject * ___cancel1, const MethodInfo* method);
#define BufferObservable_2_SubscribeCore_m666888389(__this, ___observer0, ___cancel1, method) ((  Il2CppObject * (*) (BufferObservable_2_t550437669 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))BufferObservable_2_SubscribeCore_m666888389_gshared)(__this, ___observer0, ___cancel1, method)
