﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Zenject.IFactory`3<System.Object,System.Object,System.Object>
struct IFactory_3_t1619999162;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.FactoryNested`4<System.Object,System.Object,System.Object,System.Object>
struct  FactoryNested_4_t2122089277  : public Il2CppObject
{
public:
	// Zenject.IFactory`3<TParam1,TParam2,TConcrete> Zenject.FactoryNested`4::_concreteFactory
	Il2CppObject* ____concreteFactory_0;

public:
	inline static int32_t get_offset_of__concreteFactory_0() { return static_cast<int32_t>(offsetof(FactoryNested_4_t2122089277, ____concreteFactory_0)); }
	inline Il2CppObject* get__concreteFactory_0() const { return ____concreteFactory_0; }
	inline Il2CppObject** get_address_of__concreteFactory_0() { return &____concreteFactory_0; }
	inline void set__concreteFactory_0(Il2CppObject* value)
	{
		____concreteFactory_0 = value;
		Il2CppCodeGenWriteBarrier(&____concreteFactory_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
