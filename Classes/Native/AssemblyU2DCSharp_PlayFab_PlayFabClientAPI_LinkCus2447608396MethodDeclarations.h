﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.PlayFabClientAPI/LinkCustomIDResponseCallback
struct LinkCustomIDResponseCallback_t2447608396;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;
// PlayFab.ClientModels.LinkCustomIDRequest
struct LinkCustomIDRequest_t3838085433;
// PlayFab.ClientModels.LinkCustomIDResult
struct LinkCustomIDResult_t297504339;
// PlayFab.PlayFabError
struct PlayFabError_t750598646;
// System.IAsyncResult
struct IAsyncResult_t537683269;
// System.AsyncCallback
struct AsyncCallback_t1363551830;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_IntPtr676692020.h"
#include "mscorlib_System_String968488902.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_LinkCustomI3838085433.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_LinkCustomID297504339.h"
#include "AssemblyU2DCSharpU2Dfirstpass_PlayFab_PlayFabError750598646.h"
#include "mscorlib_System_AsyncCallback1363551830.h"

// System.Void PlayFab.PlayFabClientAPI/LinkCustomIDResponseCallback::.ctor(System.Object,System.IntPtr)
extern "C"  void LinkCustomIDResponseCallback__ctor_m3511020859 (LinkCustomIDResponseCallback_t2447608396 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/LinkCustomIDResponseCallback::Invoke(System.String,System.Int32,PlayFab.ClientModels.LinkCustomIDRequest,PlayFab.ClientModels.LinkCustomIDResult,PlayFab.PlayFabError,System.Object)
extern "C"  void LinkCustomIDResponseCallback_Invoke_m422436872 (LinkCustomIDResponseCallback_t2447608396 * __this, String_t* ___urlPath0, int32_t ___callId1, LinkCustomIDRequest_t3838085433 * ___request2, LinkCustomIDResult_t297504339 * ___result3, PlayFabError_t750598646 * ___error4, Il2CppObject * ___customData5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
extern "C" void pinvoke_delegate_wrapper_LinkCustomIDResponseCallback_t2447608396(Il2CppObject* delegate, String_t* ___urlPath0, int32_t ___callId1, LinkCustomIDRequest_t3838085433 * ___request2, LinkCustomIDResult_t297504339 * ___result3, PlayFabError_t750598646 * ___error4, Il2CppObject * ___customData5);
// System.IAsyncResult PlayFab.PlayFabClientAPI/LinkCustomIDResponseCallback::BeginInvoke(System.String,System.Int32,PlayFab.ClientModels.LinkCustomIDRequest,PlayFab.ClientModels.LinkCustomIDResult,PlayFab.PlayFabError,System.Object,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * LinkCustomIDResponseCallback_BeginInvoke_m1470736181 (LinkCustomIDResponseCallback_t2447608396 * __this, String_t* ___urlPath0, int32_t ___callId1, LinkCustomIDRequest_t3838085433 * ___request2, LinkCustomIDResult_t297504339 * ___result3, PlayFabError_t750598646 * ___error4, Il2CppObject * ___customData5, AsyncCallback_t1363551830 * ___callback6, Il2CppObject * ___object7, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/LinkCustomIDResponseCallback::EndInvoke(System.IAsyncResult)
extern "C"  void LinkCustomIDResponseCallback_EndInvoke_m1325348043 (LinkCustomIDResponseCallback_t2447608396 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
