﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.ScheduledNotifier`1/<Report>c__AnonStorey5A<System.Single>
struct U3CReportU3Ec__AnonStorey5A_t1389043775;

#include "codegen/il2cpp-codegen.h"

// System.Void UniRx.ScheduledNotifier`1/<Report>c__AnonStorey5A<System.Single>::.ctor()
extern "C"  void U3CReportU3Ec__AnonStorey5A__ctor_m499607774_gshared (U3CReportU3Ec__AnonStorey5A_t1389043775 * __this, const MethodInfo* method);
#define U3CReportU3Ec__AnonStorey5A__ctor_m499607774(__this, method) ((  void (*) (U3CReportU3Ec__AnonStorey5A_t1389043775 *, const MethodInfo*))U3CReportU3Ec__AnonStorey5A__ctor_m499607774_gshared)(__this, method)
// System.Void UniRx.ScheduledNotifier`1/<Report>c__AnonStorey5A<System.Single>::<>m__6E()
extern "C"  void U3CReportU3Ec__AnonStorey5A_U3CU3Em__6E_m1394942774_gshared (U3CReportU3Ec__AnonStorey5A_t1389043775 * __this, const MethodInfo* method);
#define U3CReportU3Ec__AnonStorey5A_U3CU3Em__6E_m1394942774(__this, method) ((  void (*) (U3CReportU3Ec__AnonStorey5A_t1389043775 *, const MethodInfo*))U3CReportU3Ec__AnonStorey5A_U3CU3Em__6E_m1394942774_gshared)(__this, method)
