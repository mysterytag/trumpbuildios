﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object837106420.h"
#include "UnityEngine_UnityEngine_LogType3529269451.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Examples.Sample04_ConvertFromUnityCallback/LogCallback
struct  LogCallback_t3235662730  : public Il2CppObject
{
public:
	// System.String UniRx.Examples.Sample04_ConvertFromUnityCallback/LogCallback::Condition
	String_t* ___Condition_0;
	// System.String UniRx.Examples.Sample04_ConvertFromUnityCallback/LogCallback::StackTrace
	String_t* ___StackTrace_1;
	// UnityEngine.LogType UniRx.Examples.Sample04_ConvertFromUnityCallback/LogCallback::LogType
	int32_t ___LogType_2;

public:
	inline static int32_t get_offset_of_Condition_0() { return static_cast<int32_t>(offsetof(LogCallback_t3235662730, ___Condition_0)); }
	inline String_t* get_Condition_0() const { return ___Condition_0; }
	inline String_t** get_address_of_Condition_0() { return &___Condition_0; }
	inline void set_Condition_0(String_t* value)
	{
		___Condition_0 = value;
		Il2CppCodeGenWriteBarrier(&___Condition_0, value);
	}

	inline static int32_t get_offset_of_StackTrace_1() { return static_cast<int32_t>(offsetof(LogCallback_t3235662730, ___StackTrace_1)); }
	inline String_t* get_StackTrace_1() const { return ___StackTrace_1; }
	inline String_t** get_address_of_StackTrace_1() { return &___StackTrace_1; }
	inline void set_StackTrace_1(String_t* value)
	{
		___StackTrace_1 = value;
		Il2CppCodeGenWriteBarrier(&___StackTrace_1, value);
	}

	inline static int32_t get_offset_of_LogType_2() { return static_cast<int32_t>(offsetof(LogCallback_t3235662730, ___LogType_2)); }
	inline int32_t get_LogType_2() const { return ___LogType_2; }
	inline int32_t* get_address_of_LogType_2() { return &___LogType_2; }
	inline void set_LogType_2(int32_t value)
	{
		___LogType_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
