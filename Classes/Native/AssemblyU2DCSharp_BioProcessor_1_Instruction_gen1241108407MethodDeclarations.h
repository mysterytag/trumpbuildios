﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// BioProcessor`1/Instruction<System.Object>
struct Instruction_t1241108407;

#include "codegen/il2cpp-codegen.h"

// System.Void BioProcessor`1/Instruction<System.Object>::.ctor()
extern "C"  void Instruction__ctor_m154630601_gshared (Instruction_t1241108407 * __this, const MethodInfo* method);
#define Instruction__ctor_m154630601(__this, method) ((  void (*) (Instruction_t1241108407 *, const MethodInfo*))Instruction__ctor_m154630601_gshared)(__this, method)
