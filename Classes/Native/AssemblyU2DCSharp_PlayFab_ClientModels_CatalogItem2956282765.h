﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_Nullable_1_gen3871963234.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PlayFab.ClientModels.CatalogItemConsumableInfo
struct  CatalogItemConsumableInfo_t2956282765  : public Il2CppObject
{
public:
	// System.Nullable`1<System.UInt32> PlayFab.ClientModels.CatalogItemConsumableInfo::<UsageCount>k__BackingField
	Nullable_1_t3871963234  ___U3CUsageCountU3Ek__BackingField_0;
	// System.Nullable`1<System.UInt32> PlayFab.ClientModels.CatalogItemConsumableInfo::<UsagePeriod>k__BackingField
	Nullable_1_t3871963234  ___U3CUsagePeriodU3Ek__BackingField_1;
	// System.String PlayFab.ClientModels.CatalogItemConsumableInfo::<UsagePeriodGroup>k__BackingField
	String_t* ___U3CUsagePeriodGroupU3Ek__BackingField_2;

public:
	inline static int32_t get_offset_of_U3CUsageCountU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(CatalogItemConsumableInfo_t2956282765, ___U3CUsageCountU3Ek__BackingField_0)); }
	inline Nullable_1_t3871963234  get_U3CUsageCountU3Ek__BackingField_0() const { return ___U3CUsageCountU3Ek__BackingField_0; }
	inline Nullable_1_t3871963234 * get_address_of_U3CUsageCountU3Ek__BackingField_0() { return &___U3CUsageCountU3Ek__BackingField_0; }
	inline void set_U3CUsageCountU3Ek__BackingField_0(Nullable_1_t3871963234  value)
	{
		___U3CUsageCountU3Ek__BackingField_0 = value;
	}

	inline static int32_t get_offset_of_U3CUsagePeriodU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(CatalogItemConsumableInfo_t2956282765, ___U3CUsagePeriodU3Ek__BackingField_1)); }
	inline Nullable_1_t3871963234  get_U3CUsagePeriodU3Ek__BackingField_1() const { return ___U3CUsagePeriodU3Ek__BackingField_1; }
	inline Nullable_1_t3871963234 * get_address_of_U3CUsagePeriodU3Ek__BackingField_1() { return &___U3CUsagePeriodU3Ek__BackingField_1; }
	inline void set_U3CUsagePeriodU3Ek__BackingField_1(Nullable_1_t3871963234  value)
	{
		___U3CUsagePeriodU3Ek__BackingField_1 = value;
	}

	inline static int32_t get_offset_of_U3CUsagePeriodGroupU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(CatalogItemConsumableInfo_t2956282765, ___U3CUsagePeriodGroupU3Ek__BackingField_2)); }
	inline String_t* get_U3CUsagePeriodGroupU3Ek__BackingField_2() const { return ___U3CUsagePeriodGroupU3Ek__BackingField_2; }
	inline String_t** get_address_of_U3CUsagePeriodGroupU3Ek__BackingField_2() { return &___U3CUsagePeriodGroupU3Ek__BackingField_2; }
	inline void set_U3CUsagePeriodGroupU3Ek__BackingField_2(String_t* value)
	{
		___U3CUsagePeriodGroupU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier(&___U3CUsagePeriodGroupU3Ek__BackingField_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
