﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CellReactiveApi/<Join>c__AnonStorey117`1/<Join>c__AnonStorey116`1<System.Object>
struct U3CJoinU3Ec__AnonStorey116_1_t1263154545;
// IStream`1<System.Object>
struct IStream_1_t1389797411;

#include "codegen/il2cpp-codegen.h"

// System.Void CellReactiveApi/<Join>c__AnonStorey117`1/<Join>c__AnonStorey116`1<System.Object>::.ctor()
extern "C"  void U3CJoinU3Ec__AnonStorey116_1__ctor_m3190598077_gshared (U3CJoinU3Ec__AnonStorey116_1_t1263154545 * __this, const MethodInfo* method);
#define U3CJoinU3Ec__AnonStorey116_1__ctor_m3190598077(__this, method) ((  void (*) (U3CJoinU3Ec__AnonStorey116_1_t1263154545 *, const MethodInfo*))U3CJoinU3Ec__AnonStorey116_1__ctor_m3190598077_gshared)(__this, method)
// System.Void CellReactiveApi/<Join>c__AnonStorey117`1/<Join>c__AnonStorey116`1<System.Object>::<>m__19E(IStream`1<T>)
extern "C"  void U3CJoinU3Ec__AnonStorey116_1_U3CU3Em__19E_m1995501581_gshared (U3CJoinU3Ec__AnonStorey116_1_t1263154545 * __this, Il2CppObject* ___innerStream0, const MethodInfo* method);
#define U3CJoinU3Ec__AnonStorey116_1_U3CU3Em__19E_m1995501581(__this, ___innerStream0, method) ((  void (*) (U3CJoinU3Ec__AnonStorey116_1_t1263154545 *, Il2CppObject*, const MethodInfo*))U3CJoinU3Ec__AnonStorey116_1_U3CU3Em__19E_m1995501581_gshared)(__this, ___innerStream0, method)
