﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.PlayFabClientAPI/GetUserPublisherDataRequestCallback
struct GetUserPublisherDataRequestCallback_t3367155727;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;
// PlayFab.ClientModels.GetUserDataRequest
struct GetUserDataRequest_t2084642996;
// System.IAsyncResult
struct IAsyncResult_t537683269;
// System.AsyncCallback
struct AsyncCallback_t1363551830;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_IntPtr676692020.h"
#include "mscorlib_System_String968488902.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_GetUserData2084642996.h"
#include "mscorlib_System_AsyncCallback1363551830.h"

// System.Void PlayFab.PlayFabClientAPI/GetUserPublisherDataRequestCallback::.ctor(System.Object,System.IntPtr)
extern "C"  void GetUserPublisherDataRequestCallback__ctor_m2893267230 (GetUserPublisherDataRequestCallback_t3367155727 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/GetUserPublisherDataRequestCallback::Invoke(System.String,System.Int32,PlayFab.ClientModels.GetUserDataRequest,System.Object)
extern "C"  void GetUserPublisherDataRequestCallback_Invoke_m3213942501 (GetUserPublisherDataRequestCallback_t3367155727 * __this, String_t* ___urlPath0, int32_t ___callId1, GetUserDataRequest_t2084642996 * ___request2, Il2CppObject * ___customData3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
extern "C" void pinvoke_delegate_wrapper_GetUserPublisherDataRequestCallback_t3367155727(Il2CppObject* delegate, String_t* ___urlPath0, int32_t ___callId1, GetUserDataRequest_t2084642996 * ___request2, Il2CppObject * ___customData3);
// System.IAsyncResult PlayFab.PlayFabClientAPI/GetUserPublisherDataRequestCallback::BeginInvoke(System.String,System.Int32,PlayFab.ClientModels.GetUserDataRequest,System.Object,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * GetUserPublisherDataRequestCallback_BeginInvoke_m1445076006 (GetUserPublisherDataRequestCallback_t3367155727 * __this, String_t* ___urlPath0, int32_t ___callId1, GetUserDataRequest_t2084642996 * ___request2, Il2CppObject * ___customData3, AsyncCallback_t1363551830 * ___callback4, Il2CppObject * ___object5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/GetUserPublisherDataRequestCallback::EndInvoke(System.IAsyncResult)
extern "C"  void GetUserPublisherDataRequestCallback_EndInvoke_m27292206 (GetUserPublisherDataRequestCallback_t3367155727 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
