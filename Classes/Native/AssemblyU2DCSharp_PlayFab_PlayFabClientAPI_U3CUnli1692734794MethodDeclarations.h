﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.PlayFabClientAPI/<UnlinkGoogleAccount>c__AnonStorey84
struct U3CUnlinkGoogleAccountU3Ec__AnonStorey84_t1692734794;
// PlayFab.CallRequestContainer
struct CallRequestContainer_t432318609;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharpU2Dfirstpass_PlayFab_CallRequestCo432318609.h"

// System.Void PlayFab.PlayFabClientAPI/<UnlinkGoogleAccount>c__AnonStorey84::.ctor()
extern "C"  void U3CUnlinkGoogleAccountU3Ec__AnonStorey84__ctor_m2882991433 (U3CUnlinkGoogleAccountU3Ec__AnonStorey84_t1692734794 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/<UnlinkGoogleAccount>c__AnonStorey84::<>m__71(PlayFab.CallRequestContainer)
extern "C"  void U3CUnlinkGoogleAccountU3Ec__AnonStorey84_U3CU3Em__71_m3884215233 (U3CUnlinkGoogleAccountU3Ec__AnonStorey84_t1692734794 * __this, CallRequestContainer_t432318609 * ___requestContainer0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
