﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// IStream`1<System.Object>
struct IStream_1_t1389797411;
// System.Func`2<System.Object,System.Object>
struct Func_2_t2135783352;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// StreamAPI/<Map>c__AnonStorey130`2<System.Object,System.Object>
struct  U3CMapU3Ec__AnonStorey130_2_t2728082812  : public Il2CppObject
{
public:
	// IStream`1<T> StreamAPI/<Map>c__AnonStorey130`2::stream
	Il2CppObject* ___stream_0;
	// System.Func`2<T,T2> StreamAPI/<Map>c__AnonStorey130`2::map
	Func_2_t2135783352 * ___map_1;

public:
	inline static int32_t get_offset_of_stream_0() { return static_cast<int32_t>(offsetof(U3CMapU3Ec__AnonStorey130_2_t2728082812, ___stream_0)); }
	inline Il2CppObject* get_stream_0() const { return ___stream_0; }
	inline Il2CppObject** get_address_of_stream_0() { return &___stream_0; }
	inline void set_stream_0(Il2CppObject* value)
	{
		___stream_0 = value;
		Il2CppCodeGenWriteBarrier(&___stream_0, value);
	}

	inline static int32_t get_offset_of_map_1() { return static_cast<int32_t>(offsetof(U3CMapU3Ec__AnonStorey130_2_t2728082812, ___map_1)); }
	inline Func_2_t2135783352 * get_map_1() const { return ___map_1; }
	inline Func_2_t2135783352 ** get_address_of_map_1() { return &___map_1; }
	inline void set_map_1(Func_2_t2135783352 * value)
	{
		___map_1 = value;
		Il2CppCodeGenWriteBarrier(&___map_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
