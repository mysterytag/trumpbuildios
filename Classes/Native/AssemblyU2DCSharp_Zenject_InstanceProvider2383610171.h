﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Object
struct Il2CppObject;
// System.Type
struct Type_t;

#include "AssemblyU2DCSharp_Zenject_ProviderBase1627494391.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.InstanceProvider
struct  InstanceProvider_t2383610171  : public ProviderBase_t1627494391
{
public:
	// System.Object Zenject.InstanceProvider::_instance
	Il2CppObject * ____instance_2;
	// System.Type Zenject.InstanceProvider::_instanceType
	Type_t * ____instanceType_3;

public:
	inline static int32_t get_offset_of__instance_2() { return static_cast<int32_t>(offsetof(InstanceProvider_t2383610171, ____instance_2)); }
	inline Il2CppObject * get__instance_2() const { return ____instance_2; }
	inline Il2CppObject ** get_address_of__instance_2() { return &____instance_2; }
	inline void set__instance_2(Il2CppObject * value)
	{
		____instance_2 = value;
		Il2CppCodeGenWriteBarrier(&____instance_2, value);
	}

	inline static int32_t get_offset_of__instanceType_3() { return static_cast<int32_t>(offsetof(InstanceProvider_t2383610171, ____instanceType_3)); }
	inline Type_t * get__instanceType_3() const { return ____instanceType_3; }
	inline Type_t ** get_address_of__instanceType_3() { return &____instanceType_3; }
	inline void set__instanceType_3(Type_t * value)
	{
		____instanceType_3 = value;
		Il2CppCodeGenWriteBarrier(&____instanceType_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
