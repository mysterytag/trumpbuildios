﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VkConnectorBase/<GetFriends>c__AnonStoreyDB
struct U3CGetFriendsU3Ec__AnonStoreyDB_t472681730;
// UniRx.Tuple`2<System.String,System.String>[]
struct Tuple_2U5BU5D_t787828578;
// System.String
struct String_t;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"

// System.Void VkConnectorBase/<GetFriends>c__AnonStoreyDB::.ctor()
extern "C"  void U3CGetFriendsU3Ec__AnonStoreyDB__ctor_m193129231 (U3CGetFriendsU3Ec__AnonStoreyDB_t472681730 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VkConnectorBase/<GetFriends>c__AnonStoreyDB::<>m__E5(UniRx.Tuple`2<System.String,System.String>[])
extern "C"  void U3CGetFriendsU3Ec__AnonStoreyDB_U3CU3Em__E5_m2927344066 (U3CGetFriendsU3Ec__AnonStoreyDB_t472681730 * __this, Tuple_2U5BU5D_t787828578* ___tuples0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String VkConnectorBase/<GetFriends>c__AnonStoreyDB::<>m__E8(System.Object)
extern "C"  String_t* U3CGetFriendsU3Ec__AnonStoreyDB_U3CU3Em__E8_m208404614 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___o0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
