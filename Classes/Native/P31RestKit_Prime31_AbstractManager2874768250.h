﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Prime31.LifecycleHelper
struct LifecycleHelper_t3719100935;
// Prime31.ThreadingCallbackHelper
struct ThreadingCallbackHelper_t3423238746;
// UnityEngine.GameObject
struct GameObject_t4012695102;

#include "UnityEngine_UnityEngine_MonoBehaviour3012272455.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Prime31.AbstractManager
struct  AbstractManager_t2874768250  : public MonoBehaviour_t3012272455
{
public:

public:
};

struct AbstractManager_t2874768250_StaticFields
{
public:
	// Prime31.LifecycleHelper Prime31.AbstractManager::_prime31LifecycleHelperRef
	LifecycleHelper_t3719100935 * ____prime31LifecycleHelperRef_2;
	// Prime31.ThreadingCallbackHelper Prime31.AbstractManager::_threadingCallbackHelper
	ThreadingCallbackHelper_t3423238746 * ____threadingCallbackHelper_3;
	// UnityEngine.GameObject Prime31.AbstractManager::_prime31GameObject
	GameObject_t4012695102 * ____prime31GameObject_4;

public:
	inline static int32_t get_offset_of__prime31LifecycleHelperRef_2() { return static_cast<int32_t>(offsetof(AbstractManager_t2874768250_StaticFields, ____prime31LifecycleHelperRef_2)); }
	inline LifecycleHelper_t3719100935 * get__prime31LifecycleHelperRef_2() const { return ____prime31LifecycleHelperRef_2; }
	inline LifecycleHelper_t3719100935 ** get_address_of__prime31LifecycleHelperRef_2() { return &____prime31LifecycleHelperRef_2; }
	inline void set__prime31LifecycleHelperRef_2(LifecycleHelper_t3719100935 * value)
	{
		____prime31LifecycleHelperRef_2 = value;
		Il2CppCodeGenWriteBarrier(&____prime31LifecycleHelperRef_2, value);
	}

	inline static int32_t get_offset_of__threadingCallbackHelper_3() { return static_cast<int32_t>(offsetof(AbstractManager_t2874768250_StaticFields, ____threadingCallbackHelper_3)); }
	inline ThreadingCallbackHelper_t3423238746 * get__threadingCallbackHelper_3() const { return ____threadingCallbackHelper_3; }
	inline ThreadingCallbackHelper_t3423238746 ** get_address_of__threadingCallbackHelper_3() { return &____threadingCallbackHelper_3; }
	inline void set__threadingCallbackHelper_3(ThreadingCallbackHelper_t3423238746 * value)
	{
		____threadingCallbackHelper_3 = value;
		Il2CppCodeGenWriteBarrier(&____threadingCallbackHelper_3, value);
	}

	inline static int32_t get_offset_of__prime31GameObject_4() { return static_cast<int32_t>(offsetof(AbstractManager_t2874768250_StaticFields, ____prime31GameObject_4)); }
	inline GameObject_t4012695102 * get__prime31GameObject_4() const { return ____prime31GameObject_4; }
	inline GameObject_t4012695102 ** get_address_of__prime31GameObject_4() { return &____prime31GameObject_4; }
	inline void set__prime31GameObject_4(GameObject_t4012695102 * value)
	{
		____prime31GameObject_4 = value;
		Il2CppCodeGenWriteBarrier(&____prime31GameObject_4, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
