﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Yesl18n
struct Yesl18n_t384464514;

#include "codegen/il2cpp-codegen.h"

// System.Void Yesl18n::.ctor()
extern "C"  void Yesl18n__ctor_m2851007273 (Yesl18n_t384464514 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Yesl18n::Start()
extern "C"  void Yesl18n_Start_m1798145065 (Yesl18n_t384464514 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Yesl18n::Update()
extern "C"  void Yesl18n_Update_m4208741636 (Yesl18n_t384464514 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
