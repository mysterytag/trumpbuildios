﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Xml.XPath.Expression
struct Expression_t4217024437;

#include "System_Xml_System_Xml_XPath_XPathBooleanFunction2634824384.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XPath.XPathFunctionNot
struct  XPathFunctionNot_t1178269691  : public XPathBooleanFunction_t2634824384
{
public:
	// System.Xml.XPath.Expression System.Xml.XPath.XPathFunctionNot::arg0
	Expression_t4217024437 * ___arg0_0;

public:
	inline static int32_t get_offset_of_arg0_0() { return static_cast<int32_t>(offsetof(XPathFunctionNot_t1178269691, ___arg0_0)); }
	inline Expression_t4217024437 * get_arg0_0() const { return ___arg0_0; }
	inline Expression_t4217024437 ** get_address_of_arg0_0() { return &___arg0_0; }
	inline void set_arg0_0(Expression_t4217024437 * value)
	{
		___arg0_0 = value;
		Il2CppCodeGenWriteBarrier(&___arg0_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
