﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_ObjectModel_Collection2806094150MethodDeclarations.h"

// System.Void System.Collections.ObjectModel.Collection`1<UpgradeButton>::.ctor()
#define Collection_1__ctor_m4007994897(__this, method) ((  void (*) (Collection_1_t3444455072 *, const MethodInfo*))Collection_1__ctor_m1690372513_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<UpgradeButton>::.ctor(System.Collections.Generic.IList`1<T>)
#define Collection_1__ctor_m542091172(__this, ___list0, method) ((  void (*) (Collection_1_t3444455072 *, Il2CppObject*, const MethodInfo*))Collection_1__ctor_m3333275156_gshared)(__this, ___list0, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UpgradeButton>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
#define Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m198079562(__this, method) ((  bool (*) (Collection_1_t3444455072 *, const MethodInfo*))Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m1624016570_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<UpgradeButton>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define Collection_1_System_Collections_ICollection_CopyTo_m3644145747(__this, ___array0, ___index1, method) ((  void (*) (Collection_1_t3444455072 *, Il2CppArray *, int32_t, const MethodInfo*))Collection_1_System_Collections_ICollection_CopyTo_m1285013187_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.ObjectModel.Collection`1<UpgradeButton>::System.Collections.IEnumerable.GetEnumerator()
#define Collection_1_System_Collections_IEnumerable_GetEnumerator_m560255118(__this, method) ((  Il2CppObject * (*) (Collection_1_t3444455072 *, const MethodInfo*))Collection_1_System_Collections_IEnumerable_GetEnumerator_m2828471038_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<UpgradeButton>::System.Collections.IList.Add(System.Object)
#define Collection_1_System_Collections_IList_Add_m2726957731(__this, ___value0, method) ((  int32_t (*) (Collection_1_t3444455072 *, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_Add_m1708617267_gshared)(__this, ___value0, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UpgradeButton>::System.Collections.IList.Contains(System.Object)
#define Collection_1_System_Collections_IList_Contains_m2592381833(__this, ___value0, method) ((  bool (*) (Collection_1_t3444455072 *, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_Contains_m504494585_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<UpgradeButton>::System.Collections.IList.IndexOf(System.Object)
#define Collection_1_System_Collections_IList_IndexOf_m57294715(__this, ___value0, method) ((  int32_t (*) (Collection_1_t3444455072 *, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_IndexOf_m1652511499_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.Collection`1<UpgradeButton>::System.Collections.IList.Insert(System.Int32,System.Object)
#define Collection_1_System_Collections_IList_Insert_m932813862(__this, ___index0, ___value1, method) ((  void (*) (Collection_1_t3444455072 *, int32_t, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_Insert_m2187188150_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.ObjectModel.Collection`1<UpgradeButton>::System.Collections.IList.Remove(System.Object)
#define Collection_1_System_Collections_IList_Remove_m357648194(__this, ___value0, method) ((  void (*) (Collection_1_t3444455072 *, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_Remove_m2625864114_gshared)(__this, ___value0, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UpgradeButton>::System.Collections.ICollection.get_IsSynchronized()
#define Collection_1_System_Collections_ICollection_get_IsSynchronized_m1835213619(__this, method) ((  bool (*) (Collection_1_t3444455072 *, const MethodInfo*))Collection_1_System_Collections_ICollection_get_IsSynchronized_m1302589123_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.Collection`1<UpgradeButton>::System.Collections.ICollection.get_SyncRoot()
#define Collection_1_System_Collections_ICollection_get_SyncRoot_m1246471967(__this, method) ((  Il2CppObject * (*) (Collection_1_t3444455072 *, const MethodInfo*))Collection_1_System_Collections_ICollection_get_SyncRoot_m1873829551_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UpgradeButton>::System.Collections.IList.get_IsFixedSize()
#define Collection_1_System_Collections_IList_get_IsFixedSize_m2430985656(__this, method) ((  bool (*) (Collection_1_t3444455072 *, const MethodInfo*))Collection_1_System_Collections_IList_get_IsFixedSize_m2813777960_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UpgradeButton>::System.Collections.IList.get_IsReadOnly()
#define Collection_1_System_Collections_IList_get_IsReadOnly_m116785729(__this, method) ((  bool (*) (Collection_1_t3444455072 *, const MethodInfo*))Collection_1_System_Collections_IList_get_IsReadOnly_m1376059857_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.Collection`1<UpgradeButton>::System.Collections.IList.get_Item(System.Int32)
#define Collection_1_System_Collections_IList_get_Item_m629296358(__this, ___index0, method) ((  Il2CppObject * (*) (Collection_1_t3444455072 *, int32_t, const MethodInfo*))Collection_1_System_Collections_IList_get_Item_m2224513142_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.Collection`1<UpgradeButton>::System.Collections.IList.set_Item(System.Int32,System.Object)
#define Collection_1_System_Collections_IList_set_Item_m3694876285(__this, ___index0, ___value1, method) ((  void (*) (Collection_1_t3444455072 *, int32_t, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_set_Item_m2262756877_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.ObjectModel.Collection`1<UpgradeButton>::Add(T)
#define Collection_1_Add_m4275852366(__this, ___item0, method) ((  void (*) (Collection_1_t3444455072 *, UpgradeButton_t1475467342 *, const MethodInfo*))Collection_1_Add_m321765054_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<UpgradeButton>::Clear()
#define Collection_1_Clear_m1414128188(__this, method) ((  void (*) (Collection_1_t3444455072 *, const MethodInfo*))Collection_1_Clear_m3391473100_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<UpgradeButton>::ClearItems()
#define Collection_1_ClearItems_m1449247814(__this, method) ((  void (*) (Collection_1_t3444455072 *, const MethodInfo*))Collection_1_ClearItems_m2738199222_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UpgradeButton>::Contains(T)
#define Collection_1_Contains_m2810407914(__this, ___item0, method) ((  bool (*) (Collection_1_t3444455072 *, UpgradeButton_t1475467342 *, const MethodInfo*))Collection_1_Contains_m1050871674_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<UpgradeButton>::CopyTo(T[],System.Int32)
#define Collection_1_CopyTo_m3741254846(__this, ___array0, ___index1, method) ((  void (*) (Collection_1_t3444455072 *, UpgradeButtonU5BU5D_t183592635*, int32_t, const MethodInfo*))Collection_1_CopyTo_m1746187054_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.ObjectModel.Collection`1<UpgradeButton>::GetEnumerator()
#define Collection_1_GetEnumerator_m2776829389(__this, method) ((  Il2CppObject* (*) (Collection_1_t3444455072 *, const MethodInfo*))Collection_1_GetEnumerator_m625631581_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<UpgradeButton>::IndexOf(T)
#define Collection_1_IndexOf_m1495638978(__this, ___item0, method) ((  int32_t (*) (Collection_1_t3444455072 *, UpgradeButton_t1475467342 *, const MethodInfo*))Collection_1_IndexOf_m3101447730_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<UpgradeButton>::Insert(System.Int32,T)
#define Collection_1_Insert_m1433612981(__this, ___index0, ___item1, method) ((  void (*) (Collection_1_t3444455072 *, int32_t, UpgradeButton_t1475467342 *, const MethodInfo*))Collection_1_Insert_m1208073509_gshared)(__this, ___index0, ___item1, method)
// System.Void System.Collections.ObjectModel.Collection`1<UpgradeButton>::InsertItem(System.Int32,T)
#define Collection_1_InsertItem_m2419588712(__this, ___index0, ___item1, method) ((  void (*) (Collection_1_t3444455072 *, int32_t, UpgradeButton_t1475467342 *, const MethodInfo*))Collection_1_InsertItem_m714854616_gshared)(__this, ___index0, ___item1, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UpgradeButton>::Remove(T)
#define Collection_1_Remove_m328605285(__this, ___item0, method) ((  bool (*) (Collection_1_t3444455072 *, UpgradeButton_t1475467342 *, const MethodInfo*))Collection_1_Remove_m2181520885_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<UpgradeButton>::RemoveAt(System.Int32)
#define Collection_1_RemoveAt_m3602433147(__this, ___index0, method) ((  void (*) (Collection_1_t3444455072 *, int32_t, const MethodInfo*))Collection_1_RemoveAt_m3376893675_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.Collection`1<UpgradeButton>::RemoveItem(System.Int32)
#define Collection_1_RemoveItem_m3094238683(__this, ___index0, method) ((  void (*) (Collection_1_t3444455072 *, int32_t, const MethodInfo*))Collection_1_RemoveItem_m1099170891_gshared)(__this, ___index0, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<UpgradeButton>::get_Count()
#define Collection_1_get_Count_m3232442873(__this, method) ((  int32_t (*) (Collection_1_t3444455072 *, const MethodInfo*))Collection_1_get_Count_m1472906633_gshared)(__this, method)
// T System.Collections.ObjectModel.Collection`1<UpgradeButton>::get_Item(System.Int32)
#define Collection_1_get_Item_m2581900095(__this, ___index0, method) ((  UpgradeButton_t1475467342 * (*) (Collection_1_t3444455072 *, int32_t, const MethodInfo*))Collection_1_get_Item_m2356360623_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.Collection`1<UpgradeButton>::set_Item(System.Int32,T)
#define Collection_1_set_Item_m827169356(__this, ___index0, ___value1, method) ((  void (*) (Collection_1_t3444455072 *, int32_t, UpgradeButton_t1475467342 *, const MethodInfo*))Collection_1_set_Item_m3127068860_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.ObjectModel.Collection`1<UpgradeButton>::SetItem(System.Int32,T)
#define Collection_1_SetItem_m2808919213(__this, ___index0, ___item1, method) ((  void (*) (Collection_1_t3444455072 *, int32_t, UpgradeButton_t1475467342 *, const MethodInfo*))Collection_1_SetItem_m112162877_gshared)(__this, ___index0, ___item1, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UpgradeButton>::IsValidItem(System.Object)
#define Collection_1_IsValidItem_m3698226434(__this /* static, unused */, ___item0, method) ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))Collection_1_IsValidItem_m1993492338_gshared)(__this /* static, unused */, ___item0, method)
// T System.Collections.ObjectModel.Collection`1<UpgradeButton>::ConvertItem(System.Object)
#define Collection_1_ConvertItem_m3360203422(__this /* static, unused */, ___item0, method) ((  UpgradeButton_t1475467342 * (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))Collection_1_ConvertItem_m1655469326_gshared)(__this /* static, unused */, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<UpgradeButton>::CheckWritable(System.Collections.Generic.IList`1<T>)
#define Collection_1_CheckWritable_m4277708158(__this /* static, unused */, ___list0, method) ((  void (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Collection_1_CheckWritable_m651250670_gshared)(__this /* static, unused */, ___list0, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UpgradeButton>::IsSynchronized(System.Collections.Generic.IList`1<T>)
#define Collection_1_IsSynchronized_m3220782210(__this /* static, unused */, ___list0, method) ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Collection_1_IsSynchronized_m2469749778_gshared)(__this /* static, unused */, ___list0, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UpgradeButton>::IsFixedSize(System.Collections.Generic.IList`1<T>)
#define Collection_1_IsFixedSize_m308816221(__this /* static, unused */, ___list0, method) ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Collection_1_IsFixedSize_m3893865421_gshared)(__this /* static, unused */, ___list0, method)
