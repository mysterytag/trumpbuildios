﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Subject`1<UniRx.Tuple`2<System.Object,System.Int32>>
struct Subject_1_t22637510;
// System.Exception
struct Exception_t1967233988;
// System.IDisposable
struct IDisposable_t1628921374;
// UniRx.IObserver`1<UniRx.Tuple`2<System.Object,System.Int32>>
struct IObserver_1_t296601793;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Exception1967233988.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Tuple_2_gen2379570186.h"

// System.Void UniRx.Subject`1<UniRx.Tuple`2<System.Object,System.Int32>>::.ctor()
extern "C"  void Subject_1__ctor_m2775933633_gshared (Subject_1_t22637510 * __this, const MethodInfo* method);
#define Subject_1__ctor_m2775933633(__this, method) ((  void (*) (Subject_1_t22637510 *, const MethodInfo*))Subject_1__ctor_m2775933633_gshared)(__this, method)
// System.Boolean UniRx.Subject`1<UniRx.Tuple`2<System.Object,System.Int32>>::get_HasObservers()
extern "C"  bool Subject_1_get_HasObservers_m3071120939_gshared (Subject_1_t22637510 * __this, const MethodInfo* method);
#define Subject_1_get_HasObservers_m3071120939(__this, method) ((  bool (*) (Subject_1_t22637510 *, const MethodInfo*))Subject_1_get_HasObservers_m3071120939_gshared)(__this, method)
// System.Void UniRx.Subject`1<UniRx.Tuple`2<System.Object,System.Int32>>::OnCompleted()
extern "C"  void Subject_1_OnCompleted_m1860288907_gshared (Subject_1_t22637510 * __this, const MethodInfo* method);
#define Subject_1_OnCompleted_m1860288907(__this, method) ((  void (*) (Subject_1_t22637510 *, const MethodInfo*))Subject_1_OnCompleted_m1860288907_gshared)(__this, method)
// System.Void UniRx.Subject`1<UniRx.Tuple`2<System.Object,System.Int32>>::OnError(System.Exception)
extern "C"  void Subject_1_OnError_m2333571640_gshared (Subject_1_t22637510 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method);
#define Subject_1_OnError_m2333571640(__this, ___error0, method) ((  void (*) (Subject_1_t22637510 *, Exception_t1967233988 *, const MethodInfo*))Subject_1_OnError_m2333571640_gshared)(__this, ___error0, method)
// System.Void UniRx.Subject`1<UniRx.Tuple`2<System.Object,System.Int32>>::OnNext(T)
extern "C"  void Subject_1_OnNext_m3328233769_gshared (Subject_1_t22637510 * __this, Tuple_2_t2379570186  ___value0, const MethodInfo* method);
#define Subject_1_OnNext_m3328233769(__this, ___value0, method) ((  void (*) (Subject_1_t22637510 *, Tuple_2_t2379570186 , const MethodInfo*))Subject_1_OnNext_m3328233769_gshared)(__this, ___value0, method)
// System.IDisposable UniRx.Subject`1<UniRx.Tuple`2<System.Object,System.Int32>>::Subscribe(UniRx.IObserver`1<T>)
extern "C"  Il2CppObject * Subject_1_Subscribe_m2458390592_gshared (Subject_1_t22637510 * __this, Il2CppObject* ___observer0, const MethodInfo* method);
#define Subject_1_Subscribe_m2458390592(__this, ___observer0, method) ((  Il2CppObject * (*) (Subject_1_t22637510 *, Il2CppObject*, const MethodInfo*))Subject_1_Subscribe_m2458390592_gshared)(__this, ___observer0, method)
// System.Void UniRx.Subject`1<UniRx.Tuple`2<System.Object,System.Int32>>::Dispose()
extern "C"  void Subject_1_Dispose_m395813566_gshared (Subject_1_t22637510 * __this, const MethodInfo* method);
#define Subject_1_Dispose_m395813566(__this, method) ((  void (*) (Subject_1_t22637510 *, const MethodInfo*))Subject_1_Dispose_m395813566_gshared)(__this, method)
// System.Void UniRx.Subject`1<UniRx.Tuple`2<System.Object,System.Int32>>::ThrowIfDisposed()
extern "C"  void Subject_1_ThrowIfDisposed_m4173180487_gshared (Subject_1_t22637510 * __this, const MethodInfo* method);
#define Subject_1_ThrowIfDisposed_m4173180487(__this, method) ((  void (*) (Subject_1_t22637510 *, const MethodInfo*))Subject_1_ThrowIfDisposed_m4173180487_gshared)(__this, method)
// System.Boolean UniRx.Subject`1<UniRx.Tuple`2<System.Object,System.Int32>>::IsRequiredSubscribeOnCurrentThread()
extern "C"  bool Subject_1_IsRequiredSubscribeOnCurrentThread_m597618274_gshared (Subject_1_t22637510 * __this, const MethodInfo* method);
#define Subject_1_IsRequiredSubscribeOnCurrentThread_m597618274(__this, method) ((  bool (*) (Subject_1_t22637510 *, const MethodInfo*))Subject_1_IsRequiredSubscribeOnCurrentThread_m597618274_gshared)(__this, method)
