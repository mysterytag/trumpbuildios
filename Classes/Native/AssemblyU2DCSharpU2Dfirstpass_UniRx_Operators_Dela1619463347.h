﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UniRx.Operators.DelayFrameObservable`1<System.Object>
struct DelayFrameObservable_1_t4117294156;
// UnityEngine.YieldInstruction
struct YieldInstruction_t3557331758;
struct YieldInstruction_t3557331758_marshaled_pinvoke;
// UniRx.BooleanDisposable
struct BooleanDisposable_t3065601722;

#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Oper1187768149.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Operators.DelayFrameObservable`1/DelayFrame<System.Object>
struct  DelayFrame_t1619463347  : public OperatorObserverBase_2_t1187768149
{
public:
	// UniRx.Operators.DelayFrameObservable`1<T> UniRx.Operators.DelayFrameObservable`1/DelayFrame::parent
	DelayFrameObservable_1_t4117294156 * ___parent_2;
	// UnityEngine.YieldInstruction UniRx.Operators.DelayFrameObservable`1/DelayFrame::yieldInstruction
	YieldInstruction_t3557331758 * ___yieldInstruction_3;
	// UniRx.BooleanDisposable UniRx.Operators.DelayFrameObservable`1/DelayFrame::coroutineKey
	BooleanDisposable_t3065601722 * ___coroutineKey_4;

public:
	inline static int32_t get_offset_of_parent_2() { return static_cast<int32_t>(offsetof(DelayFrame_t1619463347, ___parent_2)); }
	inline DelayFrameObservable_1_t4117294156 * get_parent_2() const { return ___parent_2; }
	inline DelayFrameObservable_1_t4117294156 ** get_address_of_parent_2() { return &___parent_2; }
	inline void set_parent_2(DelayFrameObservable_1_t4117294156 * value)
	{
		___parent_2 = value;
		Il2CppCodeGenWriteBarrier(&___parent_2, value);
	}

	inline static int32_t get_offset_of_yieldInstruction_3() { return static_cast<int32_t>(offsetof(DelayFrame_t1619463347, ___yieldInstruction_3)); }
	inline YieldInstruction_t3557331758 * get_yieldInstruction_3() const { return ___yieldInstruction_3; }
	inline YieldInstruction_t3557331758 ** get_address_of_yieldInstruction_3() { return &___yieldInstruction_3; }
	inline void set_yieldInstruction_3(YieldInstruction_t3557331758 * value)
	{
		___yieldInstruction_3 = value;
		Il2CppCodeGenWriteBarrier(&___yieldInstruction_3, value);
	}

	inline static int32_t get_offset_of_coroutineKey_4() { return static_cast<int32_t>(offsetof(DelayFrame_t1619463347, ___coroutineKey_4)); }
	inline BooleanDisposable_t3065601722 * get_coroutineKey_4() const { return ___coroutineKey_4; }
	inline BooleanDisposable_t3065601722 ** get_address_of_coroutineKey_4() { return &___coroutineKey_4; }
	inline void set_coroutineKey_4(BooleanDisposable_t3065601722 * value)
	{
		___coroutineKey_4 = value;
		Il2CppCodeGenWriteBarrier(&___coroutineKey_4, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
