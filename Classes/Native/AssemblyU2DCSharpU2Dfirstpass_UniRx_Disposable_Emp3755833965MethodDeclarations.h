﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Disposable/EmptyDisposable
struct EmptyDisposable_t3755833965;

#include "codegen/il2cpp-codegen.h"

// System.Void UniRx.Disposable/EmptyDisposable::.ctor()
extern "C"  void EmptyDisposable__ctor_m2637963385 (EmptyDisposable_t3755833965 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.Disposable/EmptyDisposable::.cctor()
extern "C"  void EmptyDisposable__cctor_m3985357396 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.Disposable/EmptyDisposable::Dispose()
extern "C"  void EmptyDisposable_Dispose_m950391414 (EmptyDisposable_t3755833965 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
