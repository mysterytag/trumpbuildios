﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Zenject.FactoryNested`4<System.Object,System.Object,System.Object,System.Object>
struct FactoryNested_4_t2122089277;
// Zenject.IFactory`3<System.Object,System.Object,System.Object>
struct IFactory_3_t1619999162;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"

// System.Void Zenject.FactoryNested`4<System.Object,System.Object,System.Object,System.Object>::.ctor(Zenject.IFactory`3<TParam1,TParam2,TConcrete>)
extern "C"  void FactoryNested_4__ctor_m202307378_gshared (FactoryNested_4_t2122089277 * __this, Il2CppObject* ___concreteFactory0, const MethodInfo* method);
#define FactoryNested_4__ctor_m202307378(__this, ___concreteFactory0, method) ((  void (*) (FactoryNested_4_t2122089277 *, Il2CppObject*, const MethodInfo*))FactoryNested_4__ctor_m202307378_gshared)(__this, ___concreteFactory0, method)
// TContract Zenject.FactoryNested`4<System.Object,System.Object,System.Object,System.Object>::Create(TParam1,TParam2)
extern "C"  Il2CppObject * FactoryNested_4_Create_m1181747531_gshared (FactoryNested_4_t2122089277 * __this, Il2CppObject * ___param10, Il2CppObject * ___param21, const MethodInfo* method);
#define FactoryNested_4_Create_m1181747531(__this, ___param10, ___param21, method) ((  Il2CppObject * (*) (FactoryNested_4_t2122089277 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))FactoryNested_4_Create_m1181747531_gshared)(__this, ___param10, ___param21, method)
