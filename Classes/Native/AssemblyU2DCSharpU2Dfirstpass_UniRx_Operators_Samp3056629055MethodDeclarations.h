﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.SampleFrameObservable`1<UniRx.Unit>
struct SampleFrameObservable_1_t3056629055;
// UniRx.IObservable`1<UniRx.Unit>
struct IObservable_1_t2317084402;
// System.IDisposable
struct IDisposable_t1628921374;
// UniRx.IObserver`1<UniRx.Unit>
struct IObserver_1_t475317645;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_FrameCountType167759182.h"

// System.Void UniRx.Operators.SampleFrameObservable`1<UniRx.Unit>::.ctor(UniRx.IObservable`1<T>,System.Int32,UniRx.FrameCountType)
extern "C"  void SampleFrameObservable_1__ctor_m2028588907_gshared (SampleFrameObservable_1_t3056629055 * __this, Il2CppObject* ___source0, int32_t ___frameCount1, int32_t ___frameCountType2, const MethodInfo* method);
#define SampleFrameObservable_1__ctor_m2028588907(__this, ___source0, ___frameCount1, ___frameCountType2, method) ((  void (*) (SampleFrameObservable_1_t3056629055 *, Il2CppObject*, int32_t, int32_t, const MethodInfo*))SampleFrameObservable_1__ctor_m2028588907_gshared)(__this, ___source0, ___frameCount1, ___frameCountType2, method)
// System.IDisposable UniRx.Operators.SampleFrameObservable`1<UniRx.Unit>::SubscribeCore(UniRx.IObserver`1<T>,System.IDisposable)
extern "C"  Il2CppObject * SampleFrameObservable_1_SubscribeCore_m2196986519_gshared (SampleFrameObservable_1_t3056629055 * __this, Il2CppObject* ___observer0, Il2CppObject * ___cancel1, const MethodInfo* method);
#define SampleFrameObservable_1_SubscribeCore_m2196986519(__this, ___observer0, ___cancel1, method) ((  Il2CppObject * (*) (SampleFrameObservable_1_t3056629055 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))SampleFrameObservable_1_SubscribeCore_m2196986519_gshared)(__this, ___observer0, ___cancel1, method)
