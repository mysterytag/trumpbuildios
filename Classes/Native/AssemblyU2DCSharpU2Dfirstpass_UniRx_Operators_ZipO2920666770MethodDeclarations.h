﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.ZipObservable`5/Zip<System.Object,System.Object,System.Object,System.Object,System.Object>
struct Zip_t2920666770;
// UniRx.Operators.ZipObservable`5<System.Object,System.Object,System.Object,System.Object,System.Object>
struct ZipObservable_5_t589463103;
// UniRx.IObserver`1<System.Object>
struct IObserver_1_t3049105323;
// System.IDisposable
struct IDisposable_t1628921374;
// System.Object
struct Il2CppObject;
// System.Exception
struct Exception_t1967233988;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_Exception1967233988.h"

// System.Void UniRx.Operators.ZipObservable`5/Zip<System.Object,System.Object,System.Object,System.Object,System.Object>::.ctor(UniRx.Operators.ZipObservable`5<T1,T2,T3,T4,TR>,UniRx.IObserver`1<TR>,System.IDisposable)
extern "C"  void Zip__ctor_m1446348049_gshared (Zip_t2920666770 * __this, ZipObservable_5_t589463103 * ___parent0, Il2CppObject* ___observer1, Il2CppObject * ___cancel2, const MethodInfo* method);
#define Zip__ctor_m1446348049(__this, ___parent0, ___observer1, ___cancel2, method) ((  void (*) (Zip_t2920666770 *, ZipObservable_5_t589463103 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))Zip__ctor_m1446348049_gshared)(__this, ___parent0, ___observer1, ___cancel2, method)
// System.IDisposable UniRx.Operators.ZipObservable`5/Zip<System.Object,System.Object,System.Object,System.Object,System.Object>::Run()
extern "C"  Il2CppObject * Zip_Run_m1839660349_gshared (Zip_t2920666770 * __this, const MethodInfo* method);
#define Zip_Run_m1839660349(__this, method) ((  Il2CppObject * (*) (Zip_t2920666770 *, const MethodInfo*))Zip_Run_m1839660349_gshared)(__this, method)
// TR UniRx.Operators.ZipObservable`5/Zip<System.Object,System.Object,System.Object,System.Object,System.Object>::GetResult()
extern "C"  Il2CppObject * Zip_GetResult_m668620313_gshared (Zip_t2920666770 * __this, const MethodInfo* method);
#define Zip_GetResult_m668620313(__this, method) ((  Il2CppObject * (*) (Zip_t2920666770 *, const MethodInfo*))Zip_GetResult_m668620313_gshared)(__this, method)
// System.Void UniRx.Operators.ZipObservable`5/Zip<System.Object,System.Object,System.Object,System.Object,System.Object>::OnNext(TR)
extern "C"  void Zip_OnNext_m1591790057_gshared (Zip_t2920666770 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Zip_OnNext_m1591790057(__this, ___value0, method) ((  void (*) (Zip_t2920666770 *, Il2CppObject *, const MethodInfo*))Zip_OnNext_m1591790057_gshared)(__this, ___value0, method)
// System.Void UniRx.Operators.ZipObservable`5/Zip<System.Object,System.Object,System.Object,System.Object,System.Object>::OnError(System.Exception)
extern "C"  void Zip_OnError_m3117026086_gshared (Zip_t2920666770 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method);
#define Zip_OnError_m3117026086(__this, ___error0, method) ((  void (*) (Zip_t2920666770 *, Exception_t1967233988 *, const MethodInfo*))Zip_OnError_m3117026086_gshared)(__this, ___error0, method)
// System.Void UniRx.Operators.ZipObservable`5/Zip<System.Object,System.Object,System.Object,System.Object,System.Object>::OnCompleted()
extern "C"  void Zip_OnCompleted_m1671677817_gshared (Zip_t2920666770 * __this, const MethodInfo* method);
#define Zip_OnCompleted_m1671677817(__this, method) ((  void (*) (Zip_t2920666770 *, const MethodInfo*))Zip_OnCompleted_m1671677817_gshared)(__this, method)
// System.Void UniRx.Operators.ZipObservable`5/Zip<System.Object,System.Object,System.Object,System.Object,System.Object>::<Run>m__93()
extern "C"  void Zip_U3CRunU3Em__93_m525718509_gshared (Zip_t2920666770 * __this, const MethodInfo* method);
#define Zip_U3CRunU3Em__93_m525718509(__this, method) ((  void (*) (Zip_t2920666770 *, const MethodInfo*))Zip_U3CRunU3Em__93_m525718509_gshared)(__this, method)
