﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.SelectManyObservable`3<System.Object,System.Int64,System.Object>
struct SelectManyObservable_3_t744723289;
// UniRx.IObservable`1<System.Object>
struct IObservable_1_t595904784;
// System.Func`2<System.Object,UniRx.IObservable`1<System.Int64>>
struct Func_2_t3904890178;
// System.Func`3<System.Object,System.Int64,System.Object>
struct Func_3_t2633863527;
// System.Func`3<System.Object,System.Int32,UniRx.IObservable`1<System.Int64>>
struct Func_3_t1164808108;
// System.Func`5<System.Object,System.Int32,System.Int64,System.Int32,System.Object>
struct Func_5_t3557632551;
// System.Func`2<System.Object,System.Collections.Generic.IEnumerable`1<System.Int64>>
struct Func_2_t2723278874;
// System.Func`3<System.Object,System.Int32,System.Collections.Generic.IEnumerable`1<System.Int64>>
struct Func_3_t4278164100;
// System.IDisposable
struct IDisposable_t1628921374;
// UniRx.IObserver`1<System.Object>
struct IObserver_1_t3049105323;

#include "codegen/il2cpp-codegen.h"

// System.Void UniRx.Operators.SelectManyObservable`3<System.Object,System.Int64,System.Object>::.ctor(UniRx.IObservable`1<TSource>,System.Func`2<TSource,UniRx.IObservable`1<TCollection>>,System.Func`3<TSource,TCollection,TResult>)
extern "C"  void SelectManyObservable_3__ctor_m3494879194_gshared (SelectManyObservable_3_t744723289 * __this, Il2CppObject* ___source0, Func_2_t3904890178 * ___collectionSelector1, Func_3_t2633863527 * ___resultSelector2, const MethodInfo* method);
#define SelectManyObservable_3__ctor_m3494879194(__this, ___source0, ___collectionSelector1, ___resultSelector2, method) ((  void (*) (SelectManyObservable_3_t744723289 *, Il2CppObject*, Func_2_t3904890178 *, Func_3_t2633863527 *, const MethodInfo*))SelectManyObservable_3__ctor_m3494879194_gshared)(__this, ___source0, ___collectionSelector1, ___resultSelector2, method)
// System.Void UniRx.Operators.SelectManyObservable`3<System.Object,System.Int64,System.Object>::.ctor(UniRx.IObservable`1<TSource>,System.Func`3<TSource,System.Int32,UniRx.IObservable`1<TCollection>>,System.Func`5<TSource,System.Int32,TCollection,System.Int32,TResult>)
extern "C"  void SelectManyObservable_3__ctor_m727539034_gshared (SelectManyObservable_3_t744723289 * __this, Il2CppObject* ___source0, Func_3_t1164808108 * ___collectionSelector1, Func_5_t3557632551 * ___resultSelector2, const MethodInfo* method);
#define SelectManyObservable_3__ctor_m727539034(__this, ___source0, ___collectionSelector1, ___resultSelector2, method) ((  void (*) (SelectManyObservable_3_t744723289 *, Il2CppObject*, Func_3_t1164808108 *, Func_5_t3557632551 *, const MethodInfo*))SelectManyObservable_3__ctor_m727539034_gshared)(__this, ___source0, ___collectionSelector1, ___resultSelector2, method)
// System.Void UniRx.Operators.SelectManyObservable`3<System.Object,System.Int64,System.Object>::.ctor(UniRx.IObservable`1<TSource>,System.Func`2<TSource,System.Collections.Generic.IEnumerable`1<TCollection>>,System.Func`3<TSource,TCollection,TResult>)
extern "C"  void SelectManyObservable_3__ctor_m780576240_gshared (SelectManyObservable_3_t744723289 * __this, Il2CppObject* ___source0, Func_2_t2723278874 * ___collectionSelector1, Func_3_t2633863527 * ___resultSelector2, const MethodInfo* method);
#define SelectManyObservable_3__ctor_m780576240(__this, ___source0, ___collectionSelector1, ___resultSelector2, method) ((  void (*) (SelectManyObservable_3_t744723289 *, Il2CppObject*, Func_2_t2723278874 *, Func_3_t2633863527 *, const MethodInfo*))SelectManyObservable_3__ctor_m780576240_gshared)(__this, ___source0, ___collectionSelector1, ___resultSelector2, method)
// System.Void UniRx.Operators.SelectManyObservable`3<System.Object,System.Int64,System.Object>::.ctor(UniRx.IObservable`1<TSource>,System.Func`3<TSource,System.Int32,System.Collections.Generic.IEnumerable`1<TCollection>>,System.Func`5<TSource,System.Int32,TCollection,System.Int32,TResult>)
extern "C"  void SelectManyObservable_3__ctor_m3946439132_gshared (SelectManyObservable_3_t744723289 * __this, Il2CppObject* ___source0, Func_3_t4278164100 * ___collectionSelector1, Func_5_t3557632551 * ___resultSelector2, const MethodInfo* method);
#define SelectManyObservable_3__ctor_m3946439132(__this, ___source0, ___collectionSelector1, ___resultSelector2, method) ((  void (*) (SelectManyObservable_3_t744723289 *, Il2CppObject*, Func_3_t4278164100 *, Func_5_t3557632551 *, const MethodInfo*))SelectManyObservable_3__ctor_m3946439132_gshared)(__this, ___source0, ___collectionSelector1, ___resultSelector2, method)
// System.IDisposable UniRx.Operators.SelectManyObservable`3<System.Object,System.Int64,System.Object>::SubscribeCore(UniRx.IObserver`1<TResult>,System.IDisposable)
extern "C"  Il2CppObject * SelectManyObservable_3_SubscribeCore_m3873438670_gshared (SelectManyObservable_3_t744723289 * __this, Il2CppObject* ___observer0, Il2CppObject * ___cancel1, const MethodInfo* method);
#define SelectManyObservable_3_SubscribeCore_m3873438670(__this, ___observer0, ___cancel1, method) ((  Il2CppObject * (*) (SelectManyObservable_3_t744723289 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))SelectManyObservable_3_SubscribeCore_m3873438670_gshared)(__this, ___observer0, ___cancel1, method)
