﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.PlayFabClientAPI/<StartPurchase>c__AnonStoreyA9
struct U3CStartPurchaseU3Ec__AnonStoreyA9_t1662913160;
// PlayFab.CallRequestContainer
struct CallRequestContainer_t432318609;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharpU2Dfirstpass_PlayFab_CallRequestCo432318609.h"

// System.Void PlayFab.PlayFabClientAPI/<StartPurchase>c__AnonStoreyA9::.ctor()
extern "C"  void U3CStartPurchaseU3Ec__AnonStoreyA9__ctor_m53109195 (U3CStartPurchaseU3Ec__AnonStoreyA9_t1662913160 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/<StartPurchase>c__AnonStoreyA9::<>m__96(PlayFab.CallRequestContainer)
extern "C"  void U3CStartPurchaseU3Ec__AnonStoreyA9_U3CU3Em__96_m926216646 (U3CStartPurchaseU3Ec__AnonStoreyA9_t1662913160 * __this, CallRequestContainer_t432318609 * ___requestContainer0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
