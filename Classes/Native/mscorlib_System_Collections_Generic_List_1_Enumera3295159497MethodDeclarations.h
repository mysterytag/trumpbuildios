﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera4014815677MethodDeclarations.h"

// System.Void System.Collections.Generic.List`1/Enumerator<System.Action`1<InputEventDescriptor>>::.ctor(System.Collections.Generic.List`1<T>)
#define Enumerator__ctor_m2217971408(__this, ___l0, method) ((  void (*) (Enumerator_t3295159497 *, List_1_t914409209 *, const MethodInfo*))Enumerator__ctor_m1029849669_gshared)(__this, ___l0, method)
// System.Void System.Collections.Generic.List`1/Enumerator<System.Action`1<InputEventDescriptor>>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m2798748546(__this, method) ((  void (*) (Enumerator_t3295159497 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m771996397_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1/Enumerator<System.Action`1<InputEventDescriptor>>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m1658882606(__this, method) ((  Il2CppObject * (*) (Enumerator_t3295159497 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m3561903705_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<System.Action`1<InputEventDescriptor>>::Dispose()
#define Enumerator_Dispose_m358928565(__this, method) ((  void (*) (Enumerator_t3295159497 *, const MethodInfo*))Enumerator_Dispose_m2904289642_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<System.Action`1<InputEventDescriptor>>::VerifyState()
#define Enumerator_VerifyState_m2846305646(__this, method) ((  void (*) (Enumerator_t3295159497 *, const MethodInfo*))Enumerator_VerifyState_m1522854819_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<System.Action`1<InputEventDescriptor>>::MoveNext()
#define Enumerator_MoveNext_m3028262693(__this, method) ((  bool (*) (Enumerator_t3295159497 *, const MethodInfo*))Enumerator_MoveNext_m302222300_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<System.Action`1<InputEventDescriptor>>::get_Current()
#define Enumerator_get_Current_m4285986507(__this, method) ((  Action_1_t117450240 * (*) (Enumerator_t3295159497 *, const MethodInfo*))Enumerator_get_Current_m1113061044_gshared)(__this, method)
