﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.ClientModels.UserPsnInfo
struct UserPsnInfo_t260686782;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String968488902.h"

// System.Void PlayFab.ClientModels.UserPsnInfo::.ctor()
extern "C"  void UserPsnInfo__ctor_m1334133931 (UserPsnInfo_t260686782 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PlayFab.ClientModels.UserPsnInfo::get_PsnAccountId()
extern "C"  String_t* UserPsnInfo_get_PsnAccountId_m926012482 (UserPsnInfo_t260686782 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.UserPsnInfo::set_PsnAccountId(System.String)
extern "C"  void UserPsnInfo_set_PsnAccountId_m967739087 (UserPsnInfo_t260686782 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PlayFab.ClientModels.UserPsnInfo::get_PsnOnlineId()
extern "C"  String_t* UserPsnInfo_get_PsnOnlineId_m2510093334 (UserPsnInfo_t260686782 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.UserPsnInfo::set_PsnOnlineId(System.String)
extern "C"  void UserPsnInfo_set_PsnOnlineId_m967991293 (UserPsnInfo_t260686782 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
