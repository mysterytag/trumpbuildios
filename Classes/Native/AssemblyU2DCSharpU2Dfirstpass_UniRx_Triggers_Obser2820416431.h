﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UniRx.Subject`1<System.Int32>
struct Subject_1_t490482111;
// UniRx.Subject`1<UniRx.Unit>
struct Subject_1_t201353362;

#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Triggers_Obser1883237895.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Triggers.ObservableAnimatorTrigger
struct  ObservableAnimatorTrigger_t2820416431  : public ObservableTriggerBase_t1883237895
{
public:
	// UniRx.Subject`1<System.Int32> UniRx.Triggers.ObservableAnimatorTrigger::onAnimatorIK
	Subject_1_t490482111 * ___onAnimatorIK_8;
	// UniRx.Subject`1<UniRx.Unit> UniRx.Triggers.ObservableAnimatorTrigger::onAnimatorMove
	Subject_1_t201353362 * ___onAnimatorMove_9;

public:
	inline static int32_t get_offset_of_onAnimatorIK_8() { return static_cast<int32_t>(offsetof(ObservableAnimatorTrigger_t2820416431, ___onAnimatorIK_8)); }
	inline Subject_1_t490482111 * get_onAnimatorIK_8() const { return ___onAnimatorIK_8; }
	inline Subject_1_t490482111 ** get_address_of_onAnimatorIK_8() { return &___onAnimatorIK_8; }
	inline void set_onAnimatorIK_8(Subject_1_t490482111 * value)
	{
		___onAnimatorIK_8 = value;
		Il2CppCodeGenWriteBarrier(&___onAnimatorIK_8, value);
	}

	inline static int32_t get_offset_of_onAnimatorMove_9() { return static_cast<int32_t>(offsetof(ObservableAnimatorTrigger_t2820416431, ___onAnimatorMove_9)); }
	inline Subject_1_t201353362 * get_onAnimatorMove_9() const { return ___onAnimatorMove_9; }
	inline Subject_1_t201353362 ** get_address_of_onAnimatorMove_9() { return &___onAnimatorMove_9; }
	inline void set_onAnimatorMove_9(Subject_1_t201353362 * value)
	{
		___onAnimatorMove_9 = value;
		Il2CppCodeGenWriteBarrier(&___onAnimatorMove_9, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
