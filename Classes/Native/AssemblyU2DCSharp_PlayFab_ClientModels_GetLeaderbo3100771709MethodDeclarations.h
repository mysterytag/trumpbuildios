﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.ClientModels.GetLeaderboardAroundCurrentUserResult
struct GetLeaderboardAroundCurrentUserResult_t3100771709;
// System.Collections.Generic.List`1<PlayFab.ClientModels.PlayerLeaderboardEntry>
struct List_1_t3038850335;

#include "codegen/il2cpp-codegen.h"

// System.Void PlayFab.ClientModels.GetLeaderboardAroundCurrentUserResult::.ctor()
extern "C"  void GetLeaderboardAroundCurrentUserResult__ctor_m469563980 (GetLeaderboardAroundCurrentUserResult_t3100771709 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<PlayFab.ClientModels.PlayerLeaderboardEntry> PlayFab.ClientModels.GetLeaderboardAroundCurrentUserResult::get_Leaderboard()
extern "C"  List_1_t3038850335 * GetLeaderboardAroundCurrentUserResult_get_Leaderboard_m3075470537 (GetLeaderboardAroundCurrentUserResult_t3100771709 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.GetLeaderboardAroundCurrentUserResult::set_Leaderboard(System.Collections.Generic.List`1<PlayFab.ClientModels.PlayerLeaderboardEntry>)
extern "C"  void GetLeaderboardAroundCurrentUserResult_set_Leaderboard_m849064226 (GetLeaderboardAroundCurrentUserResult_t3100771709 * __this, List_1_t3038850335 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
