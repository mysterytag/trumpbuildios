﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// BarygaVkController/<InviteFriends>c__AnonStoreyDF
struct U3CInviteFriendsU3Ec__AnonStoreyDF_t3157660603;
// FriendInfo
struct FriendInfo_t236470412;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_FriendInfo236470412.h"

// System.Void BarygaVkController/<InviteFriends>c__AnonStoreyDF::.ctor()
extern "C"  void U3CInviteFriendsU3Ec__AnonStoreyDF__ctor_m576330386 (U3CInviteFriendsU3Ec__AnonStoreyDF_t3157660603 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BarygaVkController/<InviteFriends>c__AnonStoreyDF::<>m__F9(FriendInfo)
extern "C"  void U3CInviteFriendsU3Ec__AnonStoreyDF_U3CU3Em__F9_m3872151426 (U3CInviteFriendsU3Ec__AnonStoreyDF_t3157660603 * __this, FriendInfo_t236470412 * ___friend0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
