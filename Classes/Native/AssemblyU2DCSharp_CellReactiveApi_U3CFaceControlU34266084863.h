﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Func`2<System.Object,System.Boolean>
struct Func_2_t1509682273;
// System.Object
struct Il2CppObject;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CellReactiveApi/<FaceControl>c__AnonStorey101`1<System.Object>
struct  U3CFaceControlU3Ec__AnonStorey101_1_t4266084863  : public Il2CppObject
{
public:
	// System.Func`2<T,System.Boolean> CellReactiveApi/<FaceControl>c__AnonStorey101`1::predicate
	Func_2_t1509682273 * ___predicate_0;
	// T CellReactiveApi/<FaceControl>c__AnonStorey101`1::orphan
	Il2CppObject * ___orphan_1;

public:
	inline static int32_t get_offset_of_predicate_0() { return static_cast<int32_t>(offsetof(U3CFaceControlU3Ec__AnonStorey101_1_t4266084863, ___predicate_0)); }
	inline Func_2_t1509682273 * get_predicate_0() const { return ___predicate_0; }
	inline Func_2_t1509682273 ** get_address_of_predicate_0() { return &___predicate_0; }
	inline void set_predicate_0(Func_2_t1509682273 * value)
	{
		___predicate_0 = value;
		Il2CppCodeGenWriteBarrier(&___predicate_0, value);
	}

	inline static int32_t get_offset_of_orphan_1() { return static_cast<int32_t>(offsetof(U3CFaceControlU3Ec__AnonStorey101_1_t4266084863, ___orphan_1)); }
	inline Il2CppObject * get_orphan_1() const { return ___orphan_1; }
	inline Il2CppObject ** get_address_of_orphan_1() { return &___orphan_1; }
	inline void set_orphan_1(Il2CppObject * value)
	{
		___orphan_1 = value;
		Il2CppCodeGenWriteBarrier(&___orphan_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
