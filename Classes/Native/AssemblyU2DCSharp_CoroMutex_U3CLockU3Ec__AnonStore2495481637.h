﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// CoroMutex/LockProc
struct LockProc_t1973859137;
// CoroMutex
struct CoroMutex_t686805814;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CoroMutex/<Lock>c__AnonStorey148
struct  U3CLockU3Ec__AnonStorey148_t2495481637  : public Il2CppObject
{
public:
	// CoroMutex/LockProc CoroMutex/<Lock>c__AnonStorey148::l
	LockProc_t1973859137 * ___l_0;
	// CoroMutex CoroMutex/<Lock>c__AnonStorey148::<>f__this
	CoroMutex_t686805814 * ___U3CU3Ef__this_1;

public:
	inline static int32_t get_offset_of_l_0() { return static_cast<int32_t>(offsetof(U3CLockU3Ec__AnonStorey148_t2495481637, ___l_0)); }
	inline LockProc_t1973859137 * get_l_0() const { return ___l_0; }
	inline LockProc_t1973859137 ** get_address_of_l_0() { return &___l_0; }
	inline void set_l_0(LockProc_t1973859137 * value)
	{
		___l_0 = value;
		Il2CppCodeGenWriteBarrier(&___l_0, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__this_1() { return static_cast<int32_t>(offsetof(U3CLockU3Ec__AnonStorey148_t2495481637, ___U3CU3Ef__this_1)); }
	inline CoroMutex_t686805814 * get_U3CU3Ef__this_1() const { return ___U3CU3Ef__this_1; }
	inline CoroMutex_t686805814 ** get_address_of_U3CU3Ef__this_1() { return &___U3CU3Ef__this_1; }
	inline void set_U3CU3Ef__this_1(CoroMutex_t686805814 * value)
	{
		___U3CU3Ef__this_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__this_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
