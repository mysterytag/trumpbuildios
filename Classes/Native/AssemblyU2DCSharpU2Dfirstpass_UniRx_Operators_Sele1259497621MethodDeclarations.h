﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.SelectManyObservable`3<System.Object,UniRx.Unit,System.Object>
struct SelectManyObservable_3_t1259497621;
// UniRx.IObservable`1<System.Object>
struct IObservable_1_t595904784;
// System.Func`2<System.Object,UniRx.IObservable`1<UniRx.Unit>>
struct Func_2_t3615761334;
// System.Func`3<System.Object,UniRx.Unit,System.Object>
struct Func_3_t3148637859;
// System.Func`3<System.Object,System.Int32,UniRx.IObservable`1<UniRx.Unit>>
struct Func_3_t875679264;
// System.Func`5<System.Object,System.Int32,UniRx.Unit,System.Int32,System.Object>
struct Func_5_t2200276091;
// System.Func`2<System.Object,System.Collections.Generic.IEnumerable`1<UniRx.Unit>>
struct Func_2_t2434150030;
// System.Func`3<System.Object,System.Int32,System.Collections.Generic.IEnumerable`1<UniRx.Unit>>
struct Func_3_t3989035256;
// System.IDisposable
struct IDisposable_t1628921374;
// UniRx.IObserver`1<System.Object>
struct IObserver_1_t3049105323;

#include "codegen/il2cpp-codegen.h"

// System.Void UniRx.Operators.SelectManyObservable`3<System.Object,UniRx.Unit,System.Object>::.ctor(UniRx.IObservable`1<TSource>,System.Func`2<TSource,UniRx.IObservable`1<TCollection>>,System.Func`3<TSource,TCollection,TResult>)
extern "C"  void SelectManyObservable_3__ctor_m4025892268_gshared (SelectManyObservable_3_t1259497621 * __this, Il2CppObject* ___source0, Func_2_t3615761334 * ___collectionSelector1, Func_3_t3148637859 * ___resultSelector2, const MethodInfo* method);
#define SelectManyObservable_3__ctor_m4025892268(__this, ___source0, ___collectionSelector1, ___resultSelector2, method) ((  void (*) (SelectManyObservable_3_t1259497621 *, Il2CppObject*, Func_2_t3615761334 *, Func_3_t3148637859 *, const MethodInfo*))SelectManyObservable_3__ctor_m4025892268_gshared)(__this, ___source0, ___collectionSelector1, ___resultSelector2, method)
// System.Void UniRx.Operators.SelectManyObservable`3<System.Object,UniRx.Unit,System.Object>::.ctor(UniRx.IObservable`1<TSource>,System.Func`3<TSource,System.Int32,UniRx.IObservable`1<TCollection>>,System.Func`5<TSource,System.Int32,TCollection,System.Int32,TResult>)
extern "C"  void SelectManyObservable_3__ctor_m809482056_gshared (SelectManyObservable_3_t1259497621 * __this, Il2CppObject* ___source0, Func_3_t875679264 * ___collectionSelector1, Func_5_t2200276091 * ___resultSelector2, const MethodInfo* method);
#define SelectManyObservable_3__ctor_m809482056(__this, ___source0, ___collectionSelector1, ___resultSelector2, method) ((  void (*) (SelectManyObservable_3_t1259497621 *, Il2CppObject*, Func_3_t875679264 *, Func_5_t2200276091 *, const MethodInfo*))SelectManyObservable_3__ctor_m809482056_gshared)(__this, ___source0, ___collectionSelector1, ___resultSelector2, method)
// System.Void UniRx.Operators.SelectManyObservable`3<System.Object,UniRx.Unit,System.Object>::.ctor(UniRx.IObservable`1<TSource>,System.Func`2<TSource,System.Collections.Generic.IEnumerable`1<TCollection>>,System.Func`3<TSource,TCollection,TResult>)
extern "C"  void SelectManyObservable_3__ctor_m3485941598_gshared (SelectManyObservable_3_t1259497621 * __this, Il2CppObject* ___source0, Func_2_t2434150030 * ___collectionSelector1, Func_3_t3148637859 * ___resultSelector2, const MethodInfo* method);
#define SelectManyObservable_3__ctor_m3485941598(__this, ___source0, ___collectionSelector1, ___resultSelector2, method) ((  void (*) (SelectManyObservable_3_t1259497621 *, Il2CppObject*, Func_2_t2434150030 *, Func_3_t3148637859 *, const MethodInfo*))SelectManyObservable_3__ctor_m3485941598_gshared)(__this, ___source0, ___collectionSelector1, ___resultSelector2, method)
// System.Void UniRx.Operators.SelectManyObservable`3<System.Object,UniRx.Unit,System.Object>::.ctor(UniRx.IObservable`1<TSource>,System.Func`3<TSource,System.Int32,System.Collections.Generic.IEnumerable`1<TCollection>>,System.Func`5<TSource,System.Int32,TCollection,System.Int32,TResult>)
extern "C"  void SelectManyObservable_3__ctor_m3050088622_gshared (SelectManyObservable_3_t1259497621 * __this, Il2CppObject* ___source0, Func_3_t3989035256 * ___collectionSelector1, Func_5_t2200276091 * ___resultSelector2, const MethodInfo* method);
#define SelectManyObservable_3__ctor_m3050088622(__this, ___source0, ___collectionSelector1, ___resultSelector2, method) ((  void (*) (SelectManyObservable_3_t1259497621 *, Il2CppObject*, Func_3_t3989035256 *, Func_5_t2200276091 *, const MethodInfo*))SelectManyObservable_3__ctor_m3050088622_gshared)(__this, ___source0, ___collectionSelector1, ___resultSelector2, method)
// System.IDisposable UniRx.Operators.SelectManyObservable`3<System.Object,UniRx.Unit,System.Object>::SubscribeCore(UniRx.IObserver`1<TResult>,System.IDisposable)
extern "C"  Il2CppObject * SelectManyObservable_3_SubscribeCore_m245213920_gshared (SelectManyObservable_3_t1259497621 * __this, Il2CppObject* ___observer0, Il2CppObject * ___cancel1, const MethodInfo* method);
#define SelectManyObservable_3_SubscribeCore_m245213920(__this, ___observer0, ___cancel1, method) ((  Il2CppObject * (*) (SelectManyObservable_3_t1259497621 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))SelectManyObservable_3_SubscribeCore_m245213920_gshared)(__this, ___observer0, ___cancel1, method)
