﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_Nullable_1_gen3097043249.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PlayFab.ClientModels.RegisterPlayFabUserRequest
struct  RegisterPlayFabUserRequest_t2314572612  : public Il2CppObject
{
public:
	// System.String PlayFab.ClientModels.RegisterPlayFabUserRequest::<TitleId>k__BackingField
	String_t* ___U3CTitleIdU3Ek__BackingField_0;
	// System.String PlayFab.ClientModels.RegisterPlayFabUserRequest::<Username>k__BackingField
	String_t* ___U3CUsernameU3Ek__BackingField_1;
	// System.String PlayFab.ClientModels.RegisterPlayFabUserRequest::<Email>k__BackingField
	String_t* ___U3CEmailU3Ek__BackingField_2;
	// System.String PlayFab.ClientModels.RegisterPlayFabUserRequest::<Password>k__BackingField
	String_t* ___U3CPasswordU3Ek__BackingField_3;
	// System.Nullable`1<System.Boolean> PlayFab.ClientModels.RegisterPlayFabUserRequest::<RequireBothUsernameAndEmail>k__BackingField
	Nullable_1_t3097043249  ___U3CRequireBothUsernameAndEmailU3Ek__BackingField_4;
	// System.String PlayFab.ClientModels.RegisterPlayFabUserRequest::<DisplayName>k__BackingField
	String_t* ___U3CDisplayNameU3Ek__BackingField_5;
	// System.String PlayFab.ClientModels.RegisterPlayFabUserRequest::<Origination>k__BackingField
	String_t* ___U3COriginationU3Ek__BackingField_6;

public:
	inline static int32_t get_offset_of_U3CTitleIdU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(RegisterPlayFabUserRequest_t2314572612, ___U3CTitleIdU3Ek__BackingField_0)); }
	inline String_t* get_U3CTitleIdU3Ek__BackingField_0() const { return ___U3CTitleIdU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CTitleIdU3Ek__BackingField_0() { return &___U3CTitleIdU3Ek__BackingField_0; }
	inline void set_U3CTitleIdU3Ek__BackingField_0(String_t* value)
	{
		___U3CTitleIdU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier(&___U3CTitleIdU3Ek__BackingField_0, value);
	}

	inline static int32_t get_offset_of_U3CUsernameU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(RegisterPlayFabUserRequest_t2314572612, ___U3CUsernameU3Ek__BackingField_1)); }
	inline String_t* get_U3CUsernameU3Ek__BackingField_1() const { return ___U3CUsernameU3Ek__BackingField_1; }
	inline String_t** get_address_of_U3CUsernameU3Ek__BackingField_1() { return &___U3CUsernameU3Ek__BackingField_1; }
	inline void set_U3CUsernameU3Ek__BackingField_1(String_t* value)
	{
		___U3CUsernameU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CUsernameU3Ek__BackingField_1, value);
	}

	inline static int32_t get_offset_of_U3CEmailU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(RegisterPlayFabUserRequest_t2314572612, ___U3CEmailU3Ek__BackingField_2)); }
	inline String_t* get_U3CEmailU3Ek__BackingField_2() const { return ___U3CEmailU3Ek__BackingField_2; }
	inline String_t** get_address_of_U3CEmailU3Ek__BackingField_2() { return &___U3CEmailU3Ek__BackingField_2; }
	inline void set_U3CEmailU3Ek__BackingField_2(String_t* value)
	{
		___U3CEmailU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier(&___U3CEmailU3Ek__BackingField_2, value);
	}

	inline static int32_t get_offset_of_U3CPasswordU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(RegisterPlayFabUserRequest_t2314572612, ___U3CPasswordU3Ek__BackingField_3)); }
	inline String_t* get_U3CPasswordU3Ek__BackingField_3() const { return ___U3CPasswordU3Ek__BackingField_3; }
	inline String_t** get_address_of_U3CPasswordU3Ek__BackingField_3() { return &___U3CPasswordU3Ek__BackingField_3; }
	inline void set_U3CPasswordU3Ek__BackingField_3(String_t* value)
	{
		___U3CPasswordU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier(&___U3CPasswordU3Ek__BackingField_3, value);
	}

	inline static int32_t get_offset_of_U3CRequireBothUsernameAndEmailU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(RegisterPlayFabUserRequest_t2314572612, ___U3CRequireBothUsernameAndEmailU3Ek__BackingField_4)); }
	inline Nullable_1_t3097043249  get_U3CRequireBothUsernameAndEmailU3Ek__BackingField_4() const { return ___U3CRequireBothUsernameAndEmailU3Ek__BackingField_4; }
	inline Nullable_1_t3097043249 * get_address_of_U3CRequireBothUsernameAndEmailU3Ek__BackingField_4() { return &___U3CRequireBothUsernameAndEmailU3Ek__BackingField_4; }
	inline void set_U3CRequireBothUsernameAndEmailU3Ek__BackingField_4(Nullable_1_t3097043249  value)
	{
		___U3CRequireBothUsernameAndEmailU3Ek__BackingField_4 = value;
	}

	inline static int32_t get_offset_of_U3CDisplayNameU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(RegisterPlayFabUserRequest_t2314572612, ___U3CDisplayNameU3Ek__BackingField_5)); }
	inline String_t* get_U3CDisplayNameU3Ek__BackingField_5() const { return ___U3CDisplayNameU3Ek__BackingField_5; }
	inline String_t** get_address_of_U3CDisplayNameU3Ek__BackingField_5() { return &___U3CDisplayNameU3Ek__BackingField_5; }
	inline void set_U3CDisplayNameU3Ek__BackingField_5(String_t* value)
	{
		___U3CDisplayNameU3Ek__BackingField_5 = value;
		Il2CppCodeGenWriteBarrier(&___U3CDisplayNameU3Ek__BackingField_5, value);
	}

	inline static int32_t get_offset_of_U3COriginationU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(RegisterPlayFabUserRequest_t2314572612, ___U3COriginationU3Ek__BackingField_6)); }
	inline String_t* get_U3COriginationU3Ek__BackingField_6() const { return ___U3COriginationU3Ek__BackingField_6; }
	inline String_t** get_address_of_U3COriginationU3Ek__BackingField_6() { return &___U3COriginationU3Ek__BackingField_6; }
	inline void set_U3COriginationU3Ek__BackingField_6(String_t* value)
	{
		___U3COriginationU3Ek__BackingField_6 = value;
		Il2CppCodeGenWriteBarrier(&___U3COriginationU3Ek__BackingField_6, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
