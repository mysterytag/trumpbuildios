﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Sprite
struct Sprite_t4006040370;

#include "UnityEngine_UnityEngine_MonoBehaviour3012272455.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LocalizedSpriteSetter
struct  LocalizedSpriteSetter_t2536447647  : public MonoBehaviour_t3012272455
{
public:
	// UnityEngine.Sprite LocalizedSpriteSetter::engSprite
	Sprite_t4006040370 * ___engSprite_2;
	// UnityEngine.Sprite LocalizedSpriteSetter::rusSprite
	Sprite_t4006040370 * ___rusSprite_3;

public:
	inline static int32_t get_offset_of_engSprite_2() { return static_cast<int32_t>(offsetof(LocalizedSpriteSetter_t2536447647, ___engSprite_2)); }
	inline Sprite_t4006040370 * get_engSprite_2() const { return ___engSprite_2; }
	inline Sprite_t4006040370 ** get_address_of_engSprite_2() { return &___engSprite_2; }
	inline void set_engSprite_2(Sprite_t4006040370 * value)
	{
		___engSprite_2 = value;
		Il2CppCodeGenWriteBarrier(&___engSprite_2, value);
	}

	inline static int32_t get_offset_of_rusSprite_3() { return static_cast<int32_t>(offsetof(LocalizedSpriteSetter_t2536447647, ___rusSprite_3)); }
	inline Sprite_t4006040370 * get_rusSprite_3() const { return ___rusSprite_3; }
	inline Sprite_t4006040370 ** get_address_of_rusSprite_3() { return &___rusSprite_3; }
	inline void set_rusSprite_3(Sprite_t4006040370 * value)
	{
		___rusSprite_3 = value;
		Il2CppCodeGenWriteBarrier(&___rusSprite_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
