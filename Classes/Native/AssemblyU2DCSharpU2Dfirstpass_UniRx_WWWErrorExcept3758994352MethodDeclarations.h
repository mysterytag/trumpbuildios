﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.WWWErrorException
struct WWWErrorException_t3758994352;
// UnityEngine.WWW
struct WWW_t1522972100;
// System.String
struct String_t;
// System.Collections.Generic.Dictionary`2<System.String,System.String>
struct Dictionary_2_t2606186806;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_WWW1522972100.h"
#include "mscorlib_System_String968488902.h"
#include "System_System_Net_HttpStatusCode2736938801.h"

// System.Void UniRx.WWWErrorException::.ctor(UnityEngine.WWW)
extern "C"  void WWWErrorException__ctor_m2195240225 (WWWErrorException_t3758994352 * __this, WWW_t1522972100 * ___www0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String UniRx.WWWErrorException::get_RawErrorMessage()
extern "C"  String_t* WWWErrorException_get_RawErrorMessage_m3100515952 (WWWErrorException_t3758994352 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.WWWErrorException::set_RawErrorMessage(System.String)
extern "C"  void WWWErrorException_set_RawErrorMessage_m367948041 (WWWErrorException_t3758994352 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UniRx.WWWErrorException::get_HasResponse()
extern "C"  bool WWWErrorException_get_HasResponse_m1677127805 (WWWErrorException_t3758994352 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.WWWErrorException::set_HasResponse(System.Boolean)
extern "C"  void WWWErrorException_set_HasResponse_m1059041444 (WWWErrorException_t3758994352 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String UniRx.WWWErrorException::get_Text()
extern "C"  String_t* WWWErrorException_get_Text_m3655654886 (WWWErrorException_t3758994352 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.WWWErrorException::set_Text(System.String)
extern "C"  void WWWErrorException_set_Text_m2582704325 (WWWErrorException_t3758994352 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Net.HttpStatusCode UniRx.WWWErrorException::get_StatusCode()
extern "C"  int32_t WWWErrorException_get_StatusCode_m715253407 (WWWErrorException_t3758994352 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.WWWErrorException::set_StatusCode(System.Net.HttpStatusCode)
extern "C"  void WWWErrorException_set_StatusCode_m1127295532 (WWWErrorException_t3758994352 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.Dictionary`2<System.String,System.String> UniRx.WWWErrorException::get_ResponseHeaders()
extern "C"  Dictionary_2_t2606186806 * WWWErrorException_get_ResponseHeaders_m1839206209 (WWWErrorException_t3758994352 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.WWWErrorException::set_ResponseHeaders(System.Collections.Generic.Dictionary`2<System.String,System.String>)
extern "C"  void WWWErrorException_set_ResponseHeaders_m3784622504 (WWWErrorException_t3758994352 * __this, Dictionary_2_t2606186806 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.WWW UniRx.WWWErrorException::get_WWW()
extern "C"  WWW_t1522972100 * WWWErrorException_get_WWW_m2013148880 (WWWErrorException_t3758994352 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.WWWErrorException::set_WWW(UnityEngine.WWW)
extern "C"  void WWWErrorException_set_WWW_m3688321737 (WWWErrorException_t3758994352 * __this, WWW_t1522972100 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String UniRx.WWWErrorException::ToString()
extern "C"  String_t* WWWErrorException_ToString_m2841568860 (WWWErrorException_t3758994352 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
