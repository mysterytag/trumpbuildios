﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GooglePlayGames.OurUtils.PlayGamesHelperObject
struct PlayGamesHelperObject_t378230303;
// System.Action
struct Action_t437523947;
// System.Action`1<System.Boolean>
struct Action_1_t359458046;

#include "codegen/il2cpp-codegen.h"
#include "System_Core_System_Action437523947.h"

// System.Void GooglePlayGames.OurUtils.PlayGamesHelperObject::.ctor()
extern "C"  void PlayGamesHelperObject__ctor_m1093222576 (PlayGamesHelperObject_t378230303 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.OurUtils.PlayGamesHelperObject::.cctor()
extern "C"  void PlayGamesHelperObject__cctor_m3343032573 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.OurUtils.PlayGamesHelperObject::CreateObject()
extern "C"  void PlayGamesHelperObject_CreateObject_m281177135 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.OurUtils.PlayGamesHelperObject::Awake()
extern "C"  void PlayGamesHelperObject_Awake_m1330827795 (PlayGamesHelperObject_t378230303 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.OurUtils.PlayGamesHelperObject::OnDisable()
extern "C"  void PlayGamesHelperObject_OnDisable_m574668055 (PlayGamesHelperObject_t378230303 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.OurUtils.PlayGamesHelperObject::RunOnGameThread(System.Action)
extern "C"  void PlayGamesHelperObject_RunOnGameThread_m1608083433 (Il2CppObject * __this /* static, unused */, Action_t437523947 * ___action0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.OurUtils.PlayGamesHelperObject::Update()
extern "C"  void PlayGamesHelperObject_Update_m1257023581 (PlayGamesHelperObject_t378230303 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.OurUtils.PlayGamesHelperObject::OnApplicationFocus(System.Boolean)
extern "C"  void PlayGamesHelperObject_OnApplicationFocus_m3270632242 (PlayGamesHelperObject_t378230303 * __this, bool ___focused0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.OurUtils.PlayGamesHelperObject::OnApplicationPause(System.Boolean)
extern "C"  void PlayGamesHelperObject_OnApplicationPause_m429832272 (PlayGamesHelperObject_t378230303 * __this, bool ___paused0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.OurUtils.PlayGamesHelperObject::SetFocusCallback(System.Action`1<System.Boolean>)
extern "C"  void PlayGamesHelperObject_SetFocusCallback_m3420555396 (Il2CppObject * __this /* static, unused */, Action_1_t359458046 * ___callback0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.OurUtils.PlayGamesHelperObject::SetPauseCallback(System.Action`1<System.Boolean>)
extern "C"  void PlayGamesHelperObject_SetPauseCallback_m408293670 (Il2CppObject * __this /* static, unused */, Action_1_t359458046 * ___callback0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.OurUtils.PlayGamesHelperObject::<Update>m__1E(System.Action)
extern "C"  void PlayGamesHelperObject_U3CUpdateU3Em__1E_m3100315495 (Il2CppObject * __this /* static, unused */, Action_t437523947 * ___a0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
