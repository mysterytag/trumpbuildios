﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_PlayFab_ReflectionUtils_ThreadSa1771086527MethodDeclarations.h"

// System.Void PlayFab.ReflectionUtils/ThreadSafeDictionaryValueFactory`2<System.Type,System.Collections.Generic.IDictionary`2<System.String,PlayFab.ReflectionUtils/GetDelegate>>::.ctor(System.Object,System.IntPtr)
#define ThreadSafeDictionaryValueFactory_2__ctor_m1503944459(__this, ___object0, ___method1, method) ((  void (*) (ThreadSafeDictionaryValueFactory_2_t3142696522 *, Il2CppObject *, IntPtr_t, const MethodInfo*))ThreadSafeDictionaryValueFactory_2__ctor_m191619557_gshared)(__this, ___object0, ___method1, method)
// TValue PlayFab.ReflectionUtils/ThreadSafeDictionaryValueFactory`2<System.Type,System.Collections.Generic.IDictionary`2<System.String,PlayFab.ReflectionUtils/GetDelegate>>::Invoke(TKey)
#define ThreadSafeDictionaryValueFactory_2_Invoke_m1403600122(__this, ___key0, method) ((  Il2CppObject* (*) (ThreadSafeDictionaryValueFactory_2_t3142696522 *, Type_t *, const MethodInfo*))ThreadSafeDictionaryValueFactory_2_Invoke_m808333140_gshared)(__this, ___key0, method)
// System.IAsyncResult PlayFab.ReflectionUtils/ThreadSafeDictionaryValueFactory`2<System.Type,System.Collections.Generic.IDictionary`2<System.String,PlayFab.ReflectionUtils/GetDelegate>>::BeginInvoke(TKey,System.AsyncCallback,System.Object)
#define ThreadSafeDictionaryValueFactory_2_BeginInvoke_m454121283(__this, ___key0, ___callback1, ___object2, method) ((  Il2CppObject * (*) (ThreadSafeDictionaryValueFactory_2_t3142696522 *, Type_t *, AsyncCallback_t1363551830 *, Il2CppObject *, const MethodInfo*))ThreadSafeDictionaryValueFactory_2_BeginInvoke_m3493335453_gshared)(__this, ___key0, ___callback1, ___object2, method)
// TValue PlayFab.ReflectionUtils/ThreadSafeDictionaryValueFactory`2<System.Type,System.Collections.Generic.IDictionary`2<System.String,PlayFab.ReflectionUtils/GetDelegate>>::EndInvoke(System.IAsyncResult)
#define ThreadSafeDictionaryValueFactory_2_EndInvoke_m3752220283(__this, ___result0, method) ((  Il2CppObject* (*) (ThreadSafeDictionaryValueFactory_2_t3142696522 *, Il2CppObject *, const MethodInfo*))ThreadSafeDictionaryValueFactory_2_EndInvoke_m2172200533_gshared)(__this, ___result0, method)
