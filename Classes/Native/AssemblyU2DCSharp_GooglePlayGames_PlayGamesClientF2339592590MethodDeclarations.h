﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GooglePlayGames.PlayGamesClientFactory
struct PlayGamesClientFactory_t2339592590;
// GooglePlayGames.BasicApi.IPlayGamesClient
struct IPlayGamesClient_t3354664049;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_GooglePlayGames_BasicApi_PlayGame962255808.h"

// System.Void GooglePlayGames.PlayGamesClientFactory::.ctor()
extern "C"  void PlayGamesClientFactory__ctor_m1251846287 (PlayGamesClientFactory_t2339592590 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// GooglePlayGames.BasicApi.IPlayGamesClient GooglePlayGames.PlayGamesClientFactory::GetPlatformPlayGamesClient(GooglePlayGames.BasicApi.PlayGamesClientConfiguration)
extern "C"  Il2CppObject * PlayGamesClientFactory_GetPlatformPlayGamesClient_m189021676 (Il2CppObject * __this /* static, unused */, PlayGamesClientConfiguration_t962255808  ___config0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
