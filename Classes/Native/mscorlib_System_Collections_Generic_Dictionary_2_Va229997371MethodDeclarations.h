﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_V1451594948MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<Zenject.BindingId,System.Collections.Generic.List`1<Zenject.ProviderBase>>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define ValueCollection__ctor_m727932030(__this, ___dictionary0, method) ((  void (*) (ValueCollection_t229997371 *, Dictionary_2_t2602827573 *, const MethodInfo*))ValueCollection__ctor_m4177258586_gshared)(__this, ___dictionary0, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<Zenject.BindingId,System.Collections.Generic.List`1<Zenject.ProviderBase>>::System.Collections.Generic.ICollection<TValue>.Add(TValue)
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m4187903988(__this, ___item0, method) ((  void (*) (ValueCollection_t229997371 *, List_1_t2424453360 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m1528225944_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<Zenject.BindingId,System.Collections.Generic.List`1<Zenject.ProviderBase>>::System.Collections.Generic.ICollection<TValue>.Clear()
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m3091619517(__this, method) ((  void (*) (ValueCollection_t229997371 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m2827278689_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<Zenject.BindingId,System.Collections.Generic.List`1<Zenject.ProviderBase>>::System.Collections.Generic.ICollection<TValue>.Contains(TValue)
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m2402078770(__this, ___item0, method) ((  bool (*) (ValueCollection_t229997371 *, List_1_t2424453360 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m2015709838_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<Zenject.BindingId,System.Collections.Generic.List`1<Zenject.ProviderBase>>::System.Collections.Generic.ICollection<TValue>.Remove(TValue)
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m2264943959(__this, ___item0, method) ((  bool (*) (ValueCollection_t229997371 *, List_1_t2424453360 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m3578506931_gshared)(__this, ___item0, method)
// System.Collections.Generic.IEnumerator`1<TValue> System.Collections.Generic.Dictionary`2/ValueCollection<Zenject.BindingId,System.Collections.Generic.List`1<Zenject.ProviderBase>>::System.Collections.Generic.IEnumerable<TValue>.GetEnumerator()
#define ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m1924597387(__this, method) ((  Il2CppObject* (*) (ValueCollection_t229997371 *, const MethodInfo*))ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m4041606511_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<Zenject.BindingId,System.Collections.Generic.List`1<Zenject.ProviderBase>>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define ValueCollection_System_Collections_ICollection_CopyTo_m3413604225(__this, ___array0, ___index1, method) ((  void (*) (ValueCollection_t229997371 *, Il2CppArray *, int32_t, const MethodInfo*))ValueCollection_System_Collections_ICollection_CopyTo_m1709700389_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2/ValueCollection<Zenject.BindingId,System.Collections.Generic.List`1<Zenject.ProviderBase>>::System.Collections.IEnumerable.GetEnumerator()
#define ValueCollection_System_Collections_IEnumerable_GetEnumerator_m316076348(__this, method) ((  Il2CppObject * (*) (ValueCollection_t229997371 *, const MethodInfo*))ValueCollection_System_Collections_IEnumerable_GetEnumerator_m2843387488_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<Zenject.BindingId,System.Collections.Generic.List`1<Zenject.ProviderBase>>::System.Collections.Generic.ICollection<TValue>.get_IsReadOnly()
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m677254629(__this, method) ((  bool (*) (ValueCollection_t229997371 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m290885697_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<Zenject.BindingId,System.Collections.Generic.List`1<Zenject.ProviderBase>>::System.Collections.ICollection.get_IsSynchronized()
#define ValueCollection_System_Collections_ICollection_get_IsSynchronized_m4195502917(__this, method) ((  bool (*) (ValueCollection_t229997371 *, const MethodInfo*))ValueCollection_System_Collections_ICollection_get_IsSynchronized_m1930063777_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection<Zenject.BindingId,System.Collections.Generic.List`1<Zenject.ProviderBase>>::System.Collections.ICollection.get_SyncRoot()
#define ValueCollection_System_Collections_ICollection_get_SyncRoot_m3979255729(__this, method) ((  Il2CppObject * (*) (ValueCollection_t229997371 *, const MethodInfo*))ValueCollection_System_Collections_ICollection_get_SyncRoot_m1874108365_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<Zenject.BindingId,System.Collections.Generic.List`1<Zenject.ProviderBase>>::CopyTo(TValue[],System.Int32)
#define ValueCollection_CopyTo_m2288021829(__this, ___array0, ___index1, method) ((  void (*) (ValueCollection_t229997371 *, List_1U5BU5D_t3788412241*, int32_t, const MethodInfo*))ValueCollection_CopyTo_m1735386657_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/ValueCollection<Zenject.BindingId,System.Collections.Generic.List`1<Zenject.ProviderBase>>::GetEnumerator()
#define ValueCollection_GetEnumerator_m713655336(__this, method) ((  Enumerator_t2369855516  (*) (ValueCollection_t229997371 *, const MethodInfo*))ValueCollection_GetEnumerator_m1204216004_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2/ValueCollection<Zenject.BindingId,System.Collections.Generic.List`1<Zenject.ProviderBase>>::get_Count()
#define ValueCollection_get_Count_m1686115851(__this, method) ((  int32_t (*) (ValueCollection_t229997371 *, const MethodInfo*))ValueCollection_get_Count_m2709231847_gshared)(__this, method)
