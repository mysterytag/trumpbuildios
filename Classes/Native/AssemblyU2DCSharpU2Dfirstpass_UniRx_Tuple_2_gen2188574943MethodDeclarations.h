﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Tuple_2_gen369261819MethodDeclarations.h"

// System.Void UniRx.Tuple`2<System.String,UnityEngine.Sprite>::.ctor(T1,T2)
#define Tuple_2__ctor_m3242450822(__this, ___item10, ___item21, method) ((  void (*) (Tuple_2_t2188574943 *, String_t*, Sprite_t4006040370 *, const MethodInfo*))Tuple_2__ctor_m101027774_gshared)(__this, ___item10, ___item21, method)
// System.Int32 UniRx.Tuple`2<System.String,UnityEngine.Sprite>::System.IComparable.CompareTo(System.Object)
#define Tuple_2_System_IComparable_CompareTo_m763083579(__this, ___obj0, method) ((  int32_t (*) (Tuple_2_t2188574943 *, Il2CppObject *, const MethodInfo*))Tuple_2_System_IComparable_CompareTo_m964946507_gshared)(__this, ___obj0, method)
// System.Int32 UniRx.Tuple`2<System.String,UnityEngine.Sprite>::UniRx.IStructuralComparable.CompareTo(System.Object,System.Collections.IComparer)
#define Tuple_2_UniRx_IStructuralComparable_CompareTo_m3696455629(__this, ___other0, ___comparer1, method) ((  int32_t (*) (Tuple_2_t2188574943 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))Tuple_2_UniRx_IStructuralComparable_CompareTo_m1607455453_gshared)(__this, ___other0, ___comparer1, method)
// System.Boolean UniRx.Tuple`2<System.String,UnityEngine.Sprite>::UniRx.IStructuralEquatable.Equals(System.Object,System.Collections.IEqualityComparer)
#define Tuple_2_UniRx_IStructuralEquatable_Equals_m4229573828(__this, ___other0, ___comparer1, method) ((  bool (*) (Tuple_2_t2188574943 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))Tuple_2_UniRx_IStructuralEquatable_Equals_m2678862280_gshared)(__this, ___other0, ___comparer1, method)
// System.Int32 UniRx.Tuple`2<System.String,UnityEngine.Sprite>::UniRx.IStructuralEquatable.GetHashCode(System.Collections.IEqualityComparer)
#define Tuple_2_UniRx_IStructuralEquatable_GetHashCode_m1676364662(__this, ___comparer0, method) ((  int32_t (*) (Tuple_2_t2188574943 *, Il2CppObject *, const MethodInfo*))Tuple_2_UniRx_IStructuralEquatable_GetHashCode_m2682338918_gshared)(__this, ___comparer0, method)
// System.String UniRx.Tuple`2<System.String,UnityEngine.Sprite>::UniRx.ITuple.ToString()
#define Tuple_2_UniRx_ITuple_ToString_m3376281775(__this, method) ((  String_t* (*) (Tuple_2_t2188574943 *, const MethodInfo*))Tuple_2_UniRx_ITuple_ToString_m2079956805_gshared)(__this, method)
// T1 UniRx.Tuple`2<System.String,UnityEngine.Sprite>::get_Item1()
#define Tuple_2_get_Item1_m706235840(__this, method) ((  String_t* (*) (Tuple_2_t2188574943 *, const MethodInfo*))Tuple_2_get_Item1_m425160818_gshared)(__this, method)
// T2 UniRx.Tuple`2<System.String,UnityEngine.Sprite>::get_Item2()
#define Tuple_2_get_Item2_m3193356576(__this, method) ((  Sprite_t4006040370 * (*) (Tuple_2_t2188574943 *, const MethodInfo*))Tuple_2_get_Item2_m1055620404_gshared)(__this, method)
// System.Boolean UniRx.Tuple`2<System.String,UnityEngine.Sprite>::Equals(System.Object)
#define Tuple_2_Equals_m4095825784(__this, ___obj0, method) ((  bool (*) (Tuple_2_t2188574943 *, Il2CppObject *, const MethodInfo*))Tuple_2_Equals_m2489154684_gshared)(__this, ___obj0, method)
// System.Int32 UniRx.Tuple`2<System.String,UnityEngine.Sprite>::GetHashCode()
#define Tuple_2_GetHashCode_m1498482704(__this, method) ((  int32_t (*) (Tuple_2_t2188574943 *, const MethodInfo*))Tuple_2_GetHashCode_m1023013664_gshared)(__this, method)
// System.String UniRx.Tuple`2<System.String,UnityEngine.Sprite>::ToString()
#define Tuple_2_ToString_m4077220386(__this, method) ((  String_t* (*) (Tuple_2_t2188574943 *, const MethodInfo*))Tuple_2_ToString_m3182481356_gshared)(__this, method)
// System.Boolean UniRx.Tuple`2<System.String,UnityEngine.Sprite>::Equals(UniRx.Tuple`2<T1,T2>)
#define Tuple_2_Equals_m1349791089(__this, ___other0, method) ((  bool (*) (Tuple_2_t2188574943 *, Tuple_2_t2188574943 , const MethodInfo*))Tuple_2_Equals_m1605966829_gshared)(__this, ___other0, method)
