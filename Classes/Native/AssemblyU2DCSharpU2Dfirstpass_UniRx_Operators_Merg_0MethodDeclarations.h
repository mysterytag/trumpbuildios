﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.MergeObservable`1/MergeOuterObserver/Merge<System.Object>
struct Merge_t1753429970;
// UniRx.Operators.MergeObservable`1/MergeOuterObserver<System.Object>
struct MergeOuterObserver_t1613081930;
// System.IDisposable
struct IDisposable_t1628921374;
// System.Object
struct Il2CppObject;
// System.Exception
struct Exception_t1967233988;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_Exception1967233988.h"

// System.Void UniRx.Operators.MergeObservable`1/MergeOuterObserver/Merge<System.Object>::.ctor(UniRx.Operators.MergeObservable`1/MergeOuterObserver<T>,System.IDisposable)
extern "C"  void Merge__ctor_m4128364116_gshared (Merge_t1753429970 * __this, MergeOuterObserver_t1613081930 * ___parent0, Il2CppObject * ___cancel1, const MethodInfo* method);
#define Merge__ctor_m4128364116(__this, ___parent0, ___cancel1, method) ((  void (*) (Merge_t1753429970 *, MergeOuterObserver_t1613081930 *, Il2CppObject *, const MethodInfo*))Merge__ctor_m4128364116_gshared)(__this, ___parent0, ___cancel1, method)
// System.Void UniRx.Operators.MergeObservable`1/MergeOuterObserver/Merge<System.Object>::OnNext(T)
extern "C"  void Merge_OnNext_m3563486975_gshared (Merge_t1753429970 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Merge_OnNext_m3563486975(__this, ___value0, method) ((  void (*) (Merge_t1753429970 *, Il2CppObject *, const MethodInfo*))Merge_OnNext_m3563486975_gshared)(__this, ___value0, method)
// System.Void UniRx.Operators.MergeObservable`1/MergeOuterObserver/Merge<System.Object>::OnError(System.Exception)
extern "C"  void Merge_OnError_m673365006_gshared (Merge_t1753429970 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method);
#define Merge_OnError_m673365006(__this, ___error0, method) ((  void (*) (Merge_t1753429970 *, Exception_t1967233988 *, const MethodInfo*))Merge_OnError_m673365006_gshared)(__this, ___error0, method)
// System.Void UniRx.Operators.MergeObservable`1/MergeOuterObserver/Merge<System.Object>::OnCompleted()
extern "C"  void Merge_OnCompleted_m2215679073_gshared (Merge_t1753429970 * __this, const MethodInfo* method);
#define Merge_OnCompleted_m2215679073(__this, method) ((  void (*) (Merge_t1753429970 *, const MethodInfo*))Merge_OnCompleted_m2215679073_gshared)(__this, method)
