﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.ObservableWWW/<FetchAssetBundle>c__Iterator23
struct U3CFetchAssetBundleU3Ec__Iterator23_t3024943464;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void UniRx.ObservableWWW/<FetchAssetBundle>c__Iterator23::.ctor()
extern "C"  void U3CFetchAssetBundleU3Ec__Iterator23__ctor_m3639018498 (U3CFetchAssetBundleU3Ec__Iterator23_t3024943464 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object UniRx.ObservableWWW/<FetchAssetBundle>c__Iterator23::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CFetchAssetBundleU3Ec__Iterator23_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m2276305936 (U3CFetchAssetBundleU3Ec__Iterator23_t3024943464 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object UniRx.ObservableWWW/<FetchAssetBundle>c__Iterator23::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CFetchAssetBundleU3Ec__Iterator23_System_Collections_IEnumerator_get_Current_m2874789796 (U3CFetchAssetBundleU3Ec__Iterator23_t3024943464 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UniRx.ObservableWWW/<FetchAssetBundle>c__Iterator23::MoveNext()
extern "C"  bool U3CFetchAssetBundleU3Ec__Iterator23_MoveNext_m445323466 (U3CFetchAssetBundleU3Ec__Iterator23_t3024943464 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.ObservableWWW/<FetchAssetBundle>c__Iterator23::Dispose()
extern "C"  void U3CFetchAssetBundleU3Ec__Iterator23_Dispose_m891680703 (U3CFetchAssetBundleU3Ec__Iterator23_t3024943464 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.ObservableWWW/<FetchAssetBundle>c__Iterator23::Reset()
extern "C"  void U3CFetchAssetBundleU3Ec__Iterator23_Reset_m1285451439 (U3CFetchAssetBundleU3Ec__Iterator23_t3024943464 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
