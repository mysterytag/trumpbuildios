﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UniRx.ReactiveProperty`1<UnityEngine.GameObject>
struct ReactiveProperty_1_t326060844;

#include "UnityEngine_UnityEngine_MonoBehaviour3012272455.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TutorialArrow
struct  TutorialArrow_t2479747883  : public MonoBehaviour_t3012272455
{
public:
	// UniRx.ReactiveProperty`1<UnityEngine.GameObject> TutorialArrow::TargetGameObject
	ReactiveProperty_1_t326060844 * ___TargetGameObject_2;

public:
	inline static int32_t get_offset_of_TargetGameObject_2() { return static_cast<int32_t>(offsetof(TutorialArrow_t2479747883, ___TargetGameObject_2)); }
	inline ReactiveProperty_1_t326060844 * get_TargetGameObject_2() const { return ___TargetGameObject_2; }
	inline ReactiveProperty_1_t326060844 ** get_address_of_TargetGameObject_2() { return &___TargetGameObject_2; }
	inline void set_TargetGameObject_2(ReactiveProperty_1_t326060844 * value)
	{
		___TargetGameObject_2 = value;
		Il2CppCodeGenWriteBarrier(&___TargetGameObject_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
