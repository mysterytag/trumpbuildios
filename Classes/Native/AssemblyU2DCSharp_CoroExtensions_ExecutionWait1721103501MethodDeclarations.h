﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CoroExtensions/ExecutionWait
struct ExecutionWait_t1721103501;
// IProcessExecution
struct IProcessExecution_t2362207186;

#include "codegen/il2cpp-codegen.h"

// System.Void CoroExtensions/ExecutionWait::.ctor(IProcessExecution)
extern "C"  void ExecutionWait__ctor_m1391094112 (ExecutionWait_t1721103501 * __this, Il2CppObject * ___exec_0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean CoroExtensions/ExecutionWait::get_finished()
extern "C"  bool ExecutionWait_get_finished_m1591525651 (ExecutionWait_t1721103501 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CoroExtensions/ExecutionWait::Tick(System.Single)
extern "C"  void ExecutionWait_Tick_m3683908074 (ExecutionWait_t1721103501 * __this, float ___dt0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CoroExtensions/ExecutionWait::Dispose()
extern "C"  void ExecutionWait_Dispose_m3352302205 (ExecutionWait_t1721103501 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
