﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UniRx.Operators.CombineLatestObservable`1/CombineLatest<System.Boolean>
struct CombineLatest_t242152888;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Operators.CombineLatestObservable`1/CombineLatest/CombineLatestObserver<System.Boolean>
struct  CombineLatestObserver_t3970536766  : public Il2CppObject
{
public:
	// UniRx.Operators.CombineLatestObservable`1/CombineLatest<T> UniRx.Operators.CombineLatestObservable`1/CombineLatest/CombineLatestObserver::parent
	CombineLatest_t242152888 * ___parent_0;
	// System.Int32 UniRx.Operators.CombineLatestObservable`1/CombineLatest/CombineLatestObserver::index
	int32_t ___index_1;

public:
	inline static int32_t get_offset_of_parent_0() { return static_cast<int32_t>(offsetof(CombineLatestObserver_t3970536766, ___parent_0)); }
	inline CombineLatest_t242152888 * get_parent_0() const { return ___parent_0; }
	inline CombineLatest_t242152888 ** get_address_of_parent_0() { return &___parent_0; }
	inline void set_parent_0(CombineLatest_t242152888 * value)
	{
		___parent_0 = value;
		Il2CppCodeGenWriteBarrier(&___parent_0, value);
	}

	inline static int32_t get_offset_of_index_1() { return static_cast<int32_t>(offsetof(CombineLatestObserver_t3970536766, ___index_1)); }
	inline int32_t get_index_1() const { return ___index_1; }
	inline int32_t* get_address_of_index_1() { return &___index_1; }
	inline void set_index_1(int32_t value)
	{
		___index_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
