﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// OnceEmptyStream
struct OnceEmptyStream_t3081939404;

#include "codegen/il2cpp-codegen.h"

// System.Void OnceEmptyStream::.ctor()
extern "C"  void OnceEmptyStream__ctor_m3370738463 (OnceEmptyStream_t3081939404 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
