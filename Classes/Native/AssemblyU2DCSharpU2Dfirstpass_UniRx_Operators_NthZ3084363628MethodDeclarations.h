﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.NthZipLatestObserverBase`1<System.Object>
struct NthZipLatestObserverBase_1_t3084363628;
// UniRx.IObserver`1<System.Object>
struct IObserver_1_t3049105323;
// System.IDisposable
struct IDisposable_t1628921374;
// System.Exception
struct Exception_t1967233988;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Exception1967233988.h"

// System.Void UniRx.Operators.NthZipLatestObserverBase`1<System.Object>::.ctor(System.Int32,UniRx.IObserver`1<T>,System.IDisposable)
extern "C"  void NthZipLatestObserverBase_1__ctor_m1799730757_gshared (NthZipLatestObserverBase_1_t3084363628 * __this, int32_t ___length0, Il2CppObject* ___observer1, Il2CppObject * ___cancel2, const MethodInfo* method);
#define NthZipLatestObserverBase_1__ctor_m1799730757(__this, ___length0, ___observer1, ___cancel2, method) ((  void (*) (NthZipLatestObserverBase_1_t3084363628 *, int32_t, Il2CppObject*, Il2CppObject *, const MethodInfo*))NthZipLatestObserverBase_1__ctor_m1799730757_gshared)(__this, ___length0, ___observer1, ___cancel2, method)
// System.Void UniRx.Operators.NthZipLatestObserverBase`1<System.Object>::Publish(System.Int32)
extern "C"  void NthZipLatestObserverBase_1_Publish_m2577470350_gshared (NthZipLatestObserverBase_1_t3084363628 * __this, int32_t ___index0, const MethodInfo* method);
#define NthZipLatestObserverBase_1_Publish_m2577470350(__this, ___index0, method) ((  void (*) (NthZipLatestObserverBase_1_t3084363628 *, int32_t, const MethodInfo*))NthZipLatestObserverBase_1_Publish_m2577470350_gshared)(__this, ___index0, method)
// System.Void UniRx.Operators.NthZipLatestObserverBase`1<System.Object>::Done(System.Int32)
extern "C"  void NthZipLatestObserverBase_1_Done_m2636819463_gshared (NthZipLatestObserverBase_1_t3084363628 * __this, int32_t ___index0, const MethodInfo* method);
#define NthZipLatestObserverBase_1_Done_m2636819463(__this, ___index0, method) ((  void (*) (NthZipLatestObserverBase_1_t3084363628 *, int32_t, const MethodInfo*))NthZipLatestObserverBase_1_Done_m2636819463_gshared)(__this, ___index0, method)
// System.Void UniRx.Operators.NthZipLatestObserverBase`1<System.Object>::Fail(System.Exception)
extern "C"  void NthZipLatestObserverBase_1_Fail_m3173892354_gshared (NthZipLatestObserverBase_1_t3084363628 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method);
#define NthZipLatestObserverBase_1_Fail_m3173892354(__this, ___error0, method) ((  void (*) (NthZipLatestObserverBase_1_t3084363628 *, Exception_t1967233988 *, const MethodInfo*))NthZipLatestObserverBase_1_Fail_m3173892354_gshared)(__this, ___error0, method)
