﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.ClientModels.AddSharedGroupMembersRequest
struct AddSharedGroupMembersRequest_t3205265791;
// System.String
struct String_t;
// System.Collections.Generic.List`1<System.String>
struct List_1_t1765447871;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String968488902.h"

// System.Void PlayFab.ClientModels.AddSharedGroupMembersRequest::.ctor()
extern "C"  void AddSharedGroupMembersRequest__ctor_m372278942 (AddSharedGroupMembersRequest_t3205265791 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PlayFab.ClientModels.AddSharedGroupMembersRequest::get_SharedGroupId()
extern "C"  String_t* AddSharedGroupMembersRequest_get_SharedGroupId_m844081867 (AddSharedGroupMembersRequest_t3205265791 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.AddSharedGroupMembersRequest::set_SharedGroupId(System.String)
extern "C"  void AddSharedGroupMembersRequest_set_SharedGroupId_m3404867470 (AddSharedGroupMembersRequest_t3205265791 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<System.String> PlayFab.ClientModels.AddSharedGroupMembersRequest::get_PlayFabIds()
extern "C"  List_1_t1765447871 * AddSharedGroupMembersRequest_get_PlayFabIds_m2319733923 (AddSharedGroupMembersRequest_t3205265791 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.AddSharedGroupMembersRequest::set_PlayFabIds(System.Collections.Generic.List`1<System.String>)
extern "C"  void AddSharedGroupMembersRequest_set_PlayFabIds_m62989020 (AddSharedGroupMembersRequest_t3205265791 * __this, List_1_t1765447871 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
