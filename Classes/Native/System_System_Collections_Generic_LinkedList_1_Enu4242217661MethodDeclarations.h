﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "System_System_Collections_Generic_LinkedList_1_Enu4014815677MethodDeclarations.h"

// System.Void System.Collections.Generic.LinkedList`1/Enumerator<Zenject.TaskUpdater`1/TaskInfo<System.Object>>::.ctor(System.Collections.Generic.LinkedList`1<T>)
#define Enumerator__ctor_m1567603853(__this, ___parent0, method) ((  void (*) (Enumerator_t4242217662 *, LinkedList_1_t2804637950 *, const MethodInfo*))Enumerator__ctor_m857368315_gshared)(__this, ___parent0, method)
// System.Object System.Collections.Generic.LinkedList`1/Enumerator<Zenject.TaskUpdater`1/TaskInfo<System.Object>>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m2876805290(__this, method) ((  Il2CppObject * (*) (Enumerator_t4242217662 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m1753810300_gshared)(__this, method)
// System.Void System.Collections.Generic.LinkedList`1/Enumerator<Zenject.TaskUpdater`1/TaskInfo<System.Object>>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m4219953342(__this, method) ((  void (*) (Enumerator_t4242217662 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m4062113552_gshared)(__this, method)
// T System.Collections.Generic.LinkedList`1/Enumerator<Zenject.TaskUpdater`1/TaskInfo<System.Object>>::get_Current()
#define Enumerator_get_Current_m2107177193(__this, method) ((  TaskInfo_t1064508404 * (*) (Enumerator_t4242217662 *, const MethodInfo*))Enumerator_get_Current_m1124073047_gshared)(__this, method)
// System.Boolean System.Collections.Generic.LinkedList`1/Enumerator<Zenject.TaskUpdater`1/TaskInfo<System.Object>>::MoveNext()
#define Enumerator_MoveNext_m4283822614(__this, method) ((  bool (*) (Enumerator_t4242217662 *, const MethodInfo*))Enumerator_MoveNext_m2358966120_gshared)(__this, method)
// System.Void System.Collections.Generic.LinkedList`1/Enumerator<Zenject.TaskUpdater`1/TaskInfo<System.Object>>::Dispose()
#define Enumerator_Dispose_m1198991865(__this, method) ((  void (*) (Enumerator_t4242217662 *, const MethodInfo*))Enumerator_Dispose_m272587367_gshared)(__this, method)
