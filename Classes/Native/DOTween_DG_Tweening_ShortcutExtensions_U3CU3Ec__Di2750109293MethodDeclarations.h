﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// DG.Tweening.ShortcutExtensions/<>c__DisplayClass27_0
struct U3CU3Ec__DisplayClass27_0_t2750109293;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Vector43525329790.h"

// System.Void DG.Tweening.ShortcutExtensions/<>c__DisplayClass27_0::.ctor()
extern "C"  void U3CU3Ec__DisplayClass27_0__ctor_m766494793 (U3CU3Ec__DisplayClass27_0_t2750109293 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector4 DG.Tweening.ShortcutExtensions/<>c__DisplayClass27_0::<DOVector>b__0()
extern "C"  Vector4_t3525329790  U3CU3Ec__DisplayClass27_0_U3CDOVectorU3Eb__0_m1783707094 (U3CU3Ec__DisplayClass27_0_t2750109293 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DG.Tweening.ShortcutExtensions/<>c__DisplayClass27_0::<DOVector>b__1(UnityEngine.Vector4)
extern "C"  void U3CU3Ec__DisplayClass27_0_U3CDOVectorU3Eb__1_m2928892538 (U3CU3Ec__DisplayClass27_0_t2750109293 * __this, Vector4_t3525329790  ___x0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
