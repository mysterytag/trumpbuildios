﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Text.StringBuilder
struct StringBuilder_t3822575854;
// System.Collections.Generic.List`1<System.String>
struct List_1_t1765447871;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PlayFab.UUnit.UUnitTestResult
struct  UUnitTestResult_t4060832146  : public Il2CppObject
{
public:
	// System.Int32 PlayFab.UUnit.UUnitTestResult::runCount
	int32_t ___runCount_0;
	// System.Int32 PlayFab.UUnit.UUnitTestResult::successCount
	int32_t ___successCount_1;
	// System.Int32 PlayFab.UUnit.UUnitTestResult::failedCount
	int32_t ___failedCount_2;
	// System.Int32 PlayFab.UUnit.UUnitTestResult::skippedCount
	int32_t ___skippedCount_3;
	// System.Collections.Generic.List`1<System.String> PlayFab.UUnit.UUnitTestResult::messages
	List_1_t1765447871 * ___messages_5;

public:
	inline static int32_t get_offset_of_runCount_0() { return static_cast<int32_t>(offsetof(UUnitTestResult_t4060832146, ___runCount_0)); }
	inline int32_t get_runCount_0() const { return ___runCount_0; }
	inline int32_t* get_address_of_runCount_0() { return &___runCount_0; }
	inline void set_runCount_0(int32_t value)
	{
		___runCount_0 = value;
	}

	inline static int32_t get_offset_of_successCount_1() { return static_cast<int32_t>(offsetof(UUnitTestResult_t4060832146, ___successCount_1)); }
	inline int32_t get_successCount_1() const { return ___successCount_1; }
	inline int32_t* get_address_of_successCount_1() { return &___successCount_1; }
	inline void set_successCount_1(int32_t value)
	{
		___successCount_1 = value;
	}

	inline static int32_t get_offset_of_failedCount_2() { return static_cast<int32_t>(offsetof(UUnitTestResult_t4060832146, ___failedCount_2)); }
	inline int32_t get_failedCount_2() const { return ___failedCount_2; }
	inline int32_t* get_address_of_failedCount_2() { return &___failedCount_2; }
	inline void set_failedCount_2(int32_t value)
	{
		___failedCount_2 = value;
	}

	inline static int32_t get_offset_of_skippedCount_3() { return static_cast<int32_t>(offsetof(UUnitTestResult_t4060832146, ___skippedCount_3)); }
	inline int32_t get_skippedCount_3() const { return ___skippedCount_3; }
	inline int32_t* get_address_of_skippedCount_3() { return &___skippedCount_3; }
	inline void set_skippedCount_3(int32_t value)
	{
		___skippedCount_3 = value;
	}

	inline static int32_t get_offset_of_messages_5() { return static_cast<int32_t>(offsetof(UUnitTestResult_t4060832146, ___messages_5)); }
	inline List_1_t1765447871 * get_messages_5() const { return ___messages_5; }
	inline List_1_t1765447871 ** get_address_of_messages_5() { return &___messages_5; }
	inline void set_messages_5(List_1_t1765447871 * value)
	{
		___messages_5 = value;
		Il2CppCodeGenWriteBarrier(&___messages_5, value);
	}
};

struct UUnitTestResult_t4060832146_StaticFields
{
public:
	// System.Text.StringBuilder PlayFab.UUnit.UUnitTestResult::sb
	StringBuilder_t3822575854 * ___sb_4;

public:
	inline static int32_t get_offset_of_sb_4() { return static_cast<int32_t>(offsetof(UUnitTestResult_t4060832146_StaticFields, ___sb_4)); }
	inline StringBuilder_t3822575854 * get_sb_4() const { return ___sb_4; }
	inline StringBuilder_t3822575854 ** get_address_of_sb_4() { return &___sb_4; }
	inline void set_sb_4(StringBuilder_t3822575854 * value)
	{
		___sb_4 = value;
		Il2CppCodeGenWriteBarrier(&___sb_4, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
