﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.UI.Text[]
struct TextU5BU5D_t4157799059;
// GlobalStat
struct GlobalStat_t1134678199;

#include "UnityEngine_UnityEngine_MonoBehaviour3012272455.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TrampMoney
struct  TrampMoney_t3564879354  : public MonoBehaviour_t3012272455
{
public:
	// UnityEngine.UI.Text[] TrampMoney::Digits
	TextU5BU5D_t4157799059* ___Digits_2;
	// GlobalStat TrampMoney::GlobalStat
	GlobalStat_t1134678199 * ___GlobalStat_3;

public:
	inline static int32_t get_offset_of_Digits_2() { return static_cast<int32_t>(offsetof(TrampMoney_t3564879354, ___Digits_2)); }
	inline TextU5BU5D_t4157799059* get_Digits_2() const { return ___Digits_2; }
	inline TextU5BU5D_t4157799059** get_address_of_Digits_2() { return &___Digits_2; }
	inline void set_Digits_2(TextU5BU5D_t4157799059* value)
	{
		___Digits_2 = value;
		Il2CppCodeGenWriteBarrier(&___Digits_2, value);
	}

	inline static int32_t get_offset_of_GlobalStat_3() { return static_cast<int32_t>(offsetof(TrampMoney_t3564879354, ___GlobalStat_3)); }
	inline GlobalStat_t1134678199 * get_GlobalStat_3() const { return ___GlobalStat_3; }
	inline GlobalStat_t1134678199 ** get_address_of_GlobalStat_3() { return &___GlobalStat_3; }
	inline void set_GlobalStat_3(GlobalStat_t1134678199 * value)
	{
		___GlobalStat_3 = value;
		Il2CppCodeGenWriteBarrier(&___GlobalStat_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
