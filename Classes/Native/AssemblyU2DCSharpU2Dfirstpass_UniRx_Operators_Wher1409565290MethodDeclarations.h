﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.WhereObservable`1<System.Boolean>
struct WhereObservable_1_t1409565290;
// UniRx.IObservable`1<System.Boolean>
struct IObservable_1_t4264771001;
// System.Func`2<System.Boolean,System.Boolean>
struct Func_2_t1008118516;
// System.Func`3<System.Boolean,System.Int32,System.Boolean>
struct Func_3_t1068606604;
// System.IDisposable
struct IDisposable_t1628921374;
// UniRx.IObserver`1<System.Boolean>
struct IObserver_1_t2423004244;

#include "codegen/il2cpp-codegen.h"

// System.Void UniRx.Operators.WhereObservable`1<System.Boolean>::.ctor(UniRx.IObservable`1<T>,System.Func`2<T,System.Boolean>)
extern "C"  void WhereObservable_1__ctor_m1875890703_gshared (WhereObservable_1_t1409565290 * __this, Il2CppObject* ___source0, Func_2_t1008118516 * ___predicate1, const MethodInfo* method);
#define WhereObservable_1__ctor_m1875890703(__this, ___source0, ___predicate1, method) ((  void (*) (WhereObservable_1_t1409565290 *, Il2CppObject*, Func_2_t1008118516 *, const MethodInfo*))WhereObservable_1__ctor_m1875890703_gshared)(__this, ___source0, ___predicate1, method)
// System.Void UniRx.Operators.WhereObservable`1<System.Boolean>::.ctor(UniRx.IObservable`1<T>,System.Func`3<T,System.Int32,System.Boolean>)
extern "C"  void WhereObservable_1__ctor_m2958012791_gshared (WhereObservable_1_t1409565290 * __this, Il2CppObject* ___source0, Func_3_t1068606604 * ___predicateWithIndex1, const MethodInfo* method);
#define WhereObservable_1__ctor_m2958012791(__this, ___source0, ___predicateWithIndex1, method) ((  void (*) (WhereObservable_1_t1409565290 *, Il2CppObject*, Func_3_t1068606604 *, const MethodInfo*))WhereObservable_1__ctor_m2958012791_gshared)(__this, ___source0, ___predicateWithIndex1, method)
// UniRx.IObservable`1<T> UniRx.Operators.WhereObservable`1<System.Boolean>::CombinePredicate(System.Func`2<T,System.Boolean>)
extern "C"  Il2CppObject* WhereObservable_1_CombinePredicate_m1427977470_gshared (WhereObservable_1_t1409565290 * __this, Func_2_t1008118516 * ___combinePredicate0, const MethodInfo* method);
#define WhereObservable_1_CombinePredicate_m1427977470(__this, ___combinePredicate0, method) ((  Il2CppObject* (*) (WhereObservable_1_t1409565290 *, Func_2_t1008118516 *, const MethodInfo*))WhereObservable_1_CombinePredicate_m1427977470_gshared)(__this, ___combinePredicate0, method)
// System.IDisposable UniRx.Operators.WhereObservable`1<System.Boolean>::SubscribeCore(UniRx.IObserver`1<T>,System.IDisposable)
extern "C"  Il2CppObject * WhereObservable_1_SubscribeCore_m3984250670_gshared (WhereObservable_1_t1409565290 * __this, Il2CppObject* ___observer0, Il2CppObject * ___cancel1, const MethodInfo* method);
#define WhereObservable_1_SubscribeCore_m3984250670(__this, ___observer0, ___cancel1, method) ((  Il2CppObject * (*) (WhereObservable_1_t1409565290 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))WhereObservable_1_SubscribeCore_m3984250670_gshared)(__this, ___observer0, ___cancel1, method)
