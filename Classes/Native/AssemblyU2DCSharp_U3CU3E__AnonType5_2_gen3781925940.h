﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Object
struct Il2CppObject;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <>__AnonType5`2<System.Object,System.Object>
struct  U3CU3E__AnonType5_2_t3781925940  : public Il2CppObject
{
public:
	// <time>__T <>__AnonType5`2::<time>
	Il2CppObject * ___U3CtimeU3E_0;
	// <_>__T <>__AnonType5`2::<_>
	Il2CppObject * ___U3C_U3E_1;

public:
	inline static int32_t get_offset_of_U3CtimeU3E_0() { return static_cast<int32_t>(offsetof(U3CU3E__AnonType5_2_t3781925940, ___U3CtimeU3E_0)); }
	inline Il2CppObject * get_U3CtimeU3E_0() const { return ___U3CtimeU3E_0; }
	inline Il2CppObject ** get_address_of_U3CtimeU3E_0() { return &___U3CtimeU3E_0; }
	inline void set_U3CtimeU3E_0(Il2CppObject * value)
	{
		___U3CtimeU3E_0 = value;
		Il2CppCodeGenWriteBarrier(&___U3CtimeU3E_0, value);
	}

	inline static int32_t get_offset_of_U3C_U3E_1() { return static_cast<int32_t>(offsetof(U3CU3E__AnonType5_2_t3781925940, ___U3C_U3E_1)); }
	inline Il2CppObject * get_U3C_U3E_1() const { return ___U3C_U3E_1; }
	inline Il2CppObject ** get_address_of_U3C_U3E_1() { return &___U3C_U3E_1; }
	inline void set_U3C_U3E_1(Il2CppObject * value)
	{
		___U3C_U3E_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3C_U3E_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
