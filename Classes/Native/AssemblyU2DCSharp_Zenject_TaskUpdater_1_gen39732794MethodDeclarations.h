﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Zenject_TaskUpdater_1_gen1705995667MethodDeclarations.h"

// System.Void Zenject.TaskUpdater`1<Zenject.ILateTickable>::.ctor(System.Action`1<TTask>)
#define TaskUpdater_1__ctor_m3015656867(__this, ___updateFunc0, method) ((  void (*) (TaskUpdater_1_t39732794 *, Action_1_t3614263548 *, const MethodInfo*))TaskUpdater_1__ctor_m1842795290_gshared)(__this, ___updateFunc0, method)
// System.Collections.Generic.IEnumerable`1<Zenject.TaskUpdater`1/TaskInfo<TTask>> Zenject.TaskUpdater`1<Zenject.ILateTickable>::get_AllTasks()
#define TaskUpdater_1_get_AllTasks_m1862101629(__this, method) ((  Il2CppObject* (*) (TaskUpdater_1_t39732794 *, const MethodInfo*))TaskUpdater_1_get_AllTasks_m2200620774_gshared)(__this, method)
// System.Collections.Generic.IEnumerable`1<Zenject.TaskUpdater`1/TaskInfo<TTask>> Zenject.TaskUpdater`1<Zenject.ILateTickable>::get_ActiveTasks()
#define TaskUpdater_1_get_ActiveTasks_m274654842(__this, method) ((  Il2CppObject* (*) (TaskUpdater_1_t39732794 *, const MethodInfo*))TaskUpdater_1_get_ActiveTasks_m515292529_gshared)(__this, method)
// System.Void Zenject.TaskUpdater`1<Zenject.ILateTickable>::AddTask(TTask,System.Int32)
#define TaskUpdater_1_AddTask_m2889617472(__this, ___task0, ___priority1, method) ((  void (*) (TaskUpdater_1_t39732794 *, Il2CppObject *, int32_t, const MethodInfo*))TaskUpdater_1_AddTask_m720801655_gshared)(__this, ___task0, ___priority1, method)
// System.Void Zenject.TaskUpdater`1<Zenject.ILateTickable>::AddTaskInternal(TTask,System.Int32)
#define TaskUpdater_1_AddTaskInternal_m1884141981(__this, ___task0, ___priority1, method) ((  void (*) (TaskUpdater_1_t39732794 *, Il2CppObject *, int32_t, const MethodInfo*))TaskUpdater_1_AddTaskInternal_m1657606612_gshared)(__this, ___task0, ___priority1, method)
// System.Void Zenject.TaskUpdater`1<Zenject.ILateTickable>::RemoveTask(TTask)
#define TaskUpdater_1_RemoveTask_m3116551690(__this, ___task0, method) ((  void (*) (TaskUpdater_1_t39732794 *, Il2CppObject *, const MethodInfo*))TaskUpdater_1_RemoveTask_m1986106881_gshared)(__this, ___task0, method)
// System.Void Zenject.TaskUpdater`1<Zenject.ILateTickable>::OnFrameStart()
#define TaskUpdater_1_OnFrameStart_m2568083258(__this, method) ((  void (*) (TaskUpdater_1_t39732794 *, const MethodInfo*))TaskUpdater_1_OnFrameStart_m1489365667_gshared)(__this, method)
// System.Void Zenject.TaskUpdater`1<Zenject.ILateTickable>::UpdateAll()
#define TaskUpdater_1_UpdateAll_m1995998260(__this, method) ((  void (*) (TaskUpdater_1_t39732794 *, const MethodInfo*))TaskUpdater_1_UpdateAll_m611209579_gshared)(__this, method)
// System.Void Zenject.TaskUpdater`1<Zenject.ILateTickable>::UpdateRange(System.Int32,System.Int32)
#define TaskUpdater_1_UpdateRange_m2405936342(__this, ___minPriority0, ___maxPriority1, method) ((  void (*) (TaskUpdater_1_t39732794 *, int32_t, int32_t, const MethodInfo*))TaskUpdater_1_UpdateRange_m1084380479_gshared)(__this, ___minPriority0, ___maxPriority1, method)
// System.Void Zenject.TaskUpdater`1<Zenject.ILateTickable>::ClearRemovedTasks(System.Collections.Generic.LinkedList`1<Zenject.TaskUpdater`1/TaskInfo<TTask>>)
#define TaskUpdater_1_ClearRemovedTasks_m1419646217(__this, ___tasks0, method) ((  void (*) (TaskUpdater_1_t39732794 *, LinkedList_1_t1138375077 *, const MethodInfo*))TaskUpdater_1_ClearRemovedTasks_m204003072_gshared)(__this, ___tasks0, method)
// System.Void Zenject.TaskUpdater`1<Zenject.ILateTickable>::AddQueuedTasks()
#define TaskUpdater_1_AddQueuedTasks_m3589532864(__this, method) ((  void (*) (TaskUpdater_1_t39732794 *, const MethodInfo*))TaskUpdater_1_AddQueuedTasks_m2029046249_gshared)(__this, method)
// System.Void Zenject.TaskUpdater`1<Zenject.ILateTickable>::InsertTaskSorted(Zenject.TaskUpdater`1/TaskInfo<TTask>)
#define TaskUpdater_1_InsertTaskSorted_m3610118972(__this, ___task0, method) ((  void (*) (TaskUpdater_1_t39732794 *, TaskInfo_t3693212827 *, const MethodInfo*))TaskUpdater_1_InsertTaskSorted_m3714982387_gshared)(__this, ___task0, method)
// TTask Zenject.TaskUpdater`1<Zenject.ILateTickable>::<AddTaskInternal>m__2E5(Zenject.TaskUpdater`1/TaskInfo<TTask>)
#define TaskUpdater_1_U3CAddTaskInternalU3Em__2E5_m1383306095(__this /* static, unused */, ___x0, method) ((  Il2CppObject * (*) (Il2CppObject * /* static, unused */, TaskInfo_t3693212827 *, const MethodInfo*))TaskUpdater_1_U3CAddTaskInternalU3Em__2E5_m2398513880_gshared)(__this /* static, unused */, ___x0, method)
