﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_PlayFab_PlayFabClientAPI_Process3425006930MethodDeclarations.h"

// System.Void PlayFab.PlayFabClientAPI/ProcessApiCallback`1<PlayFab.ClientModels.GetFriendLeaderboardAroundCurrentUserResult>::.ctor(System.Object,System.IntPtr)
#define ProcessApiCallback_1__ctor_m764504293(__this, ___object0, ___method1, method) ((  void (*) (ProcessApiCallback_1_t3247531673 *, Il2CppObject *, IntPtr_t, const MethodInfo*))ProcessApiCallback_1__ctor_m1266910666_gshared)(__this, ___object0, ___method1, method)
// System.Void PlayFab.PlayFabClientAPI/ProcessApiCallback`1<PlayFab.ClientModels.GetFriendLeaderboardAroundCurrentUserResult>::Invoke(TResult)
#define ProcessApiCallback_1_Invoke_m2941163170(__this, ___result0, method) ((  void (*) (ProcessApiCallback_1_t3247531673 *, GetFriendLeaderboardAroundCurrentUserResult_t659631163 *, const MethodInfo*))ProcessApiCallback_1_Invoke_m1002635741_gshared)(__this, ___result0, method)
// System.IAsyncResult PlayFab.PlayFabClientAPI/ProcessApiCallback`1<PlayFab.ClientModels.GetFriendLeaderboardAroundCurrentUserResult>::BeginInvoke(TResult,System.AsyncCallback,System.Object)
#define ProcessApiCallback_1_BeginInvoke_m2132577169(__this, ___result0, ___callback1, ___object2, method) ((  Il2CppObject * (*) (ProcessApiCallback_1_t3247531673 *, GetFriendLeaderboardAroundCurrentUserResult_t659631163 *, AsyncCallback_t1363551830 *, Il2CppObject *, const MethodInfo*))ProcessApiCallback_1_BeginInvoke_m1317674820_gshared)(__this, ___result0, ___callback1, ___object2, method)
// System.Void PlayFab.PlayFabClientAPI/ProcessApiCallback`1<PlayFab.ClientModels.GetFriendLeaderboardAroundCurrentUserResult>::EndInvoke(System.IAsyncResult)
#define ProcessApiCallback_1_EndInvoke_m1423906165(__this, ___result0, method) ((  void (*) (ProcessApiCallback_1_t3247531673 *, Il2CppObject *, const MethodInfo*))ProcessApiCallback_1_EndInvoke_m3326438618_gshared)(__this, ___result0, method)
