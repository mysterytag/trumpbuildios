﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Object
struct Il2CppObject;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Nullable_1_gen212373688.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_Region1621303076.h"
#include "mscorlib_System_Object837106420.h"

// System.Void System.Nullable`1<PlayFab.ClientModels.Region>::.ctor(T)
extern "C"  void Nullable_1__ctor_m4044488867_gshared (Nullable_1_t212373688 * __this, int32_t ___value0, const MethodInfo* method);
#define Nullable_1__ctor_m4044488867(__this, ___value0, method) ((  void (*) (Nullable_1_t212373688 *, int32_t, const MethodInfo*))Nullable_1__ctor_m4044488867_gshared)(__this, ___value0, method)
// System.Boolean System.Nullable`1<PlayFab.ClientModels.Region>::get_HasValue()
extern "C"  bool Nullable_1_get_HasValue_m456742667_gshared (Nullable_1_t212373688 * __this, const MethodInfo* method);
#define Nullable_1_get_HasValue_m456742667(__this, method) ((  bool (*) (Nullable_1_t212373688 *, const MethodInfo*))Nullable_1_get_HasValue_m456742667_gshared)(__this, method)
// T System.Nullable`1<PlayFab.ClientModels.Region>::get_Value()
extern "C"  int32_t Nullable_1_get_Value_m3554026594_gshared (Nullable_1_t212373688 * __this, const MethodInfo* method);
#define Nullable_1_get_Value_m3554026594(__this, method) ((  int32_t (*) (Nullable_1_t212373688 *, const MethodInfo*))Nullable_1_get_Value_m3554026594_gshared)(__this, method)
// System.Boolean System.Nullable`1<PlayFab.ClientModels.Region>::Equals(System.Object)
extern "C"  bool Nullable_1_Equals_m1011759370_gshared (Nullable_1_t212373688 * __this, Il2CppObject * ___other0, const MethodInfo* method);
#define Nullable_1_Equals_m1011759370(__this, ___other0, method) ((  bool (*) (Nullable_1_t212373688 *, Il2CppObject *, const MethodInfo*))Nullable_1_Equals_m1011759370_gshared)(__this, ___other0, method)
// System.Boolean System.Nullable`1<PlayFab.ClientModels.Region>::Equals(System.Nullable`1<T>)
extern "C"  bool Nullable_1_Equals_m1966930677_gshared (Nullable_1_t212373688 * __this, Nullable_1_t212373688  ___other0, const MethodInfo* method);
#define Nullable_1_Equals_m1966930677(__this, ___other0, method) ((  bool (*) (Nullable_1_t212373688 *, Nullable_1_t212373688 , const MethodInfo*))Nullable_1_Equals_m1966930677_gshared)(__this, ___other0, method)
// System.Int32 System.Nullable`1<PlayFab.ClientModels.Region>::GetHashCode()
extern "C"  int32_t Nullable_1_GetHashCode_m1258440174_gshared (Nullable_1_t212373688 * __this, const MethodInfo* method);
#define Nullable_1_GetHashCode_m1258440174(__this, method) ((  int32_t (*) (Nullable_1_t212373688 *, const MethodInfo*))Nullable_1_GetHashCode_m1258440174_gshared)(__this, method)
// T System.Nullable`1<PlayFab.ClientModels.Region>::GetValueOrDefault()
extern "C"  int32_t Nullable_1_GetValueOrDefault_m87341757_gshared (Nullable_1_t212373688 * __this, const MethodInfo* method);
#define Nullable_1_GetValueOrDefault_m87341757(__this, method) ((  int32_t (*) (Nullable_1_t212373688 *, const MethodInfo*))Nullable_1_GetValueOrDefault_m87341757_gshared)(__this, method)
// System.String System.Nullable`1<PlayFab.ClientModels.Region>::ToString()
extern "C"  String_t* Nullable_1_ToString_m4290986840_gshared (Nullable_1_t212373688 * __this, const MethodInfo* method);
#define Nullable_1_ToString_m4290986840(__this, method) ((  String_t* (*) (Nullable_1_t212373688 *, const MethodInfo*))Nullable_1_ToString_m4290986840_gshared)(__this, method)
// System.Nullable`1<T> System.Nullable`1<PlayFab.ClientModels.Region>::op_Implicit(T)
extern "C"  Nullable_1_t212373688  Nullable_1_op_Implicit_m1769438744_gshared (Il2CppObject * __this /* static, unused */, int32_t ___value0, const MethodInfo* method);
#define Nullable_1_op_Implicit_m1769438744(__this /* static, unused */, ___value0, method) ((  Nullable_1_t212373688  (*) (Il2CppObject * /* static, unused */, int32_t, const MethodInfo*))Nullable_1_op_Implicit_m1769438744_gshared)(__this /* static, unused */, ___value0, method)
