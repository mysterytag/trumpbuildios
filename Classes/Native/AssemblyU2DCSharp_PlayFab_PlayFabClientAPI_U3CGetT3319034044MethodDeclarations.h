﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.PlayFabClientAPI/<GetTitleData>c__AnonStorey9D
struct U3CGetTitleDataU3Ec__AnonStorey9D_t3319034044;
// PlayFab.CallRequestContainer
struct CallRequestContainer_t432318609;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharpU2Dfirstpass_PlayFab_CallRequestCo432318609.h"

// System.Void PlayFab.PlayFabClientAPI/<GetTitleData>c__AnonStorey9D::.ctor()
extern "C"  void U3CGetTitleDataU3Ec__AnonStorey9D__ctor_m2440038007 (U3CGetTitleDataU3Ec__AnonStorey9D_t3319034044 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/<GetTitleData>c__AnonStorey9D::<>m__8A(PlayFab.CallRequestContainer)
extern "C"  void U3CGetTitleDataU3Ec__AnonStorey9D_U3CU3Em__8A_m3376053342 (U3CGetTitleDataU3Ec__AnonStorey9D_t3319034044 * __this, CallRequestContainer_t432318609 * ___requestContainer0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
