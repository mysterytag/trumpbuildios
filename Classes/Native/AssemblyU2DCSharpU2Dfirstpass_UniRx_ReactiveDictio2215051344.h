﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Generic.Dictionary`2<System.Object,System.Object>
struct Dictionary_2_t3824425150;
// UniRx.Subject`1<System.Int32>
struct Subject_1_t490482111;
// UniRx.Subject`1<UniRx.Unit>
struct Subject_1_t201353362;
// UniRx.Subject`1<UniRx.DictionaryAddEvent`2<System.Object,System.Object>>
struct Subject_1_t2189015628;
// UniRx.Subject`1<UniRx.DictionaryRemoveEvent`2<System.Object,System.Object>>
struct Subject_1_t965355615;
// UniRx.Subject`1<UniRx.DictionaryReplaceEvent`2<System.Object,System.Object>>
struct Subject_1_t1421994387;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.ReactiveDictionary`2<System.Object,System.Object>
struct  ReactiveDictionary_2_t2215051344  : public Il2CppObject
{
public:
	// System.Boolean UniRx.ReactiveDictionary`2::isDisposed
	bool ___isDisposed_0;
	// System.Collections.Generic.Dictionary`2<TKey,TValue> UniRx.ReactiveDictionary`2::inner
	Dictionary_2_t3824425150 * ___inner_1;
	// UniRx.Subject`1<System.Int32> UniRx.ReactiveDictionary`2::countChanged
	Subject_1_t490482111 * ___countChanged_2;
	// UniRx.Subject`1<UniRx.Unit> UniRx.ReactiveDictionary`2::collectionReset
	Subject_1_t201353362 * ___collectionReset_3;
	// UniRx.Subject`1<UniRx.DictionaryAddEvent`2<TKey,TValue>> UniRx.ReactiveDictionary`2::dictionaryAdd
	Subject_1_t2189015628 * ___dictionaryAdd_4;
	// UniRx.Subject`1<UniRx.DictionaryRemoveEvent`2<TKey,TValue>> UniRx.ReactiveDictionary`2::dictionaryRemove
	Subject_1_t965355615 * ___dictionaryRemove_5;
	// UniRx.Subject`1<UniRx.DictionaryReplaceEvent`2<TKey,TValue>> UniRx.ReactiveDictionary`2::dictionaryReplace
	Subject_1_t1421994387 * ___dictionaryReplace_6;

public:
	inline static int32_t get_offset_of_isDisposed_0() { return static_cast<int32_t>(offsetof(ReactiveDictionary_2_t2215051344, ___isDisposed_0)); }
	inline bool get_isDisposed_0() const { return ___isDisposed_0; }
	inline bool* get_address_of_isDisposed_0() { return &___isDisposed_0; }
	inline void set_isDisposed_0(bool value)
	{
		___isDisposed_0 = value;
	}

	inline static int32_t get_offset_of_inner_1() { return static_cast<int32_t>(offsetof(ReactiveDictionary_2_t2215051344, ___inner_1)); }
	inline Dictionary_2_t3824425150 * get_inner_1() const { return ___inner_1; }
	inline Dictionary_2_t3824425150 ** get_address_of_inner_1() { return &___inner_1; }
	inline void set_inner_1(Dictionary_2_t3824425150 * value)
	{
		___inner_1 = value;
		Il2CppCodeGenWriteBarrier(&___inner_1, value);
	}

	inline static int32_t get_offset_of_countChanged_2() { return static_cast<int32_t>(offsetof(ReactiveDictionary_2_t2215051344, ___countChanged_2)); }
	inline Subject_1_t490482111 * get_countChanged_2() const { return ___countChanged_2; }
	inline Subject_1_t490482111 ** get_address_of_countChanged_2() { return &___countChanged_2; }
	inline void set_countChanged_2(Subject_1_t490482111 * value)
	{
		___countChanged_2 = value;
		Il2CppCodeGenWriteBarrier(&___countChanged_2, value);
	}

	inline static int32_t get_offset_of_collectionReset_3() { return static_cast<int32_t>(offsetof(ReactiveDictionary_2_t2215051344, ___collectionReset_3)); }
	inline Subject_1_t201353362 * get_collectionReset_3() const { return ___collectionReset_3; }
	inline Subject_1_t201353362 ** get_address_of_collectionReset_3() { return &___collectionReset_3; }
	inline void set_collectionReset_3(Subject_1_t201353362 * value)
	{
		___collectionReset_3 = value;
		Il2CppCodeGenWriteBarrier(&___collectionReset_3, value);
	}

	inline static int32_t get_offset_of_dictionaryAdd_4() { return static_cast<int32_t>(offsetof(ReactiveDictionary_2_t2215051344, ___dictionaryAdd_4)); }
	inline Subject_1_t2189015628 * get_dictionaryAdd_4() const { return ___dictionaryAdd_4; }
	inline Subject_1_t2189015628 ** get_address_of_dictionaryAdd_4() { return &___dictionaryAdd_4; }
	inline void set_dictionaryAdd_4(Subject_1_t2189015628 * value)
	{
		___dictionaryAdd_4 = value;
		Il2CppCodeGenWriteBarrier(&___dictionaryAdd_4, value);
	}

	inline static int32_t get_offset_of_dictionaryRemove_5() { return static_cast<int32_t>(offsetof(ReactiveDictionary_2_t2215051344, ___dictionaryRemove_5)); }
	inline Subject_1_t965355615 * get_dictionaryRemove_5() const { return ___dictionaryRemove_5; }
	inline Subject_1_t965355615 ** get_address_of_dictionaryRemove_5() { return &___dictionaryRemove_5; }
	inline void set_dictionaryRemove_5(Subject_1_t965355615 * value)
	{
		___dictionaryRemove_5 = value;
		Il2CppCodeGenWriteBarrier(&___dictionaryRemove_5, value);
	}

	inline static int32_t get_offset_of_dictionaryReplace_6() { return static_cast<int32_t>(offsetof(ReactiveDictionary_2_t2215051344, ___dictionaryReplace_6)); }
	inline Subject_1_t1421994387 * get_dictionaryReplace_6() const { return ___dictionaryReplace_6; }
	inline Subject_1_t1421994387 ** get_address_of_dictionaryReplace_6() { return &___dictionaryReplace_6; }
	inline void set_dictionaryReplace_6(Subject_1_t1421994387 * value)
	{
		___dictionaryReplace_6 = value;
		Il2CppCodeGenWriteBarrier(&___dictionaryReplace_6, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
