﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2<GameAnalyticsSDK.GA_ServerFieldTypes/FieldType,System.Object>
struct Dictionary_2_t938533758;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Va705561699.h"

// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<GameAnalyticsSDK.GA_ServerFieldTypes/FieldType,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void Enumerator__ctor_m2008071845_gshared (Enumerator_t705561701 * __this, Dictionary_2_t938533758 * ___host0, const MethodInfo* method);
#define Enumerator__ctor_m2008071845(__this, ___host0, method) ((  void (*) (Enumerator_t705561701 *, Dictionary_2_t938533758 *, const MethodInfo*))Enumerator__ctor_m2008071845_gshared)(__this, ___host0, method)
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<GameAnalyticsSDK.GA_ServerFieldTypes/FieldType,System.Object>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m2948455078_gshared (Enumerator_t705561701 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m2948455078(__this, method) ((  Il2CppObject * (*) (Enumerator_t705561701 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m2948455078_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<GameAnalyticsSDK.GA_ServerFieldTypes/FieldType,System.Object>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m2973062384_gshared (Enumerator_t705561701 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_Reset_m2973062384(__this, method) ((  void (*) (Enumerator_t705561701 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m2973062384_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<GameAnalyticsSDK.GA_ServerFieldTypes/FieldType,System.Object>::Dispose()
extern "C"  void Enumerator_Dispose_m4017694855_gshared (Enumerator_t705561701 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m4017694855(__this, method) ((  void (*) (Enumerator_t705561701 *, const MethodInfo*))Enumerator_Dispose_m4017694855_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<GameAnalyticsSDK.GA_ServerFieldTypes/FieldType,System.Object>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m1172360736_gshared (Enumerator_t705561701 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m1172360736(__this, method) ((  bool (*) (Enumerator_t705561701 *, const MethodInfo*))Enumerator_MoveNext_m1172360736_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<GameAnalyticsSDK.GA_ServerFieldTypes/FieldType,System.Object>::get_Current()
extern "C"  Il2CppObject * Enumerator_get_Current_m843908874_gshared (Enumerator_t705561701 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m843908874(__this, method) ((  Il2CppObject * (*) (Enumerator_t705561701 *, const MethodInfo*))Enumerator_get_Current_m843908874_gshared)(__this, method)
