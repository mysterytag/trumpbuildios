﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.CombineLatestObserver`1<System.Object>
struct CombineLatestObserver_1_t123567747;
// System.Object
struct Il2CppObject;
// UniRx.Operators.ICombineLatestObservable
struct ICombineLatestObservable_t195534285;
// System.Exception
struct Exception_t1967233988;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_Exception1967233988.h"

// System.Void UniRx.Operators.CombineLatestObserver`1<System.Object>::.ctor(System.Object,UniRx.Operators.ICombineLatestObservable,System.Int32)
extern "C"  void CombineLatestObserver_1__ctor_m3474797587_gshared (CombineLatestObserver_1_t123567747 * __this, Il2CppObject * ___gate0, Il2CppObject * ___parent1, int32_t ___index2, const MethodInfo* method);
#define CombineLatestObserver_1__ctor_m3474797587(__this, ___gate0, ___parent1, ___index2, method) ((  void (*) (CombineLatestObserver_1_t123567747 *, Il2CppObject *, Il2CppObject *, int32_t, const MethodInfo*))CombineLatestObserver_1__ctor_m3474797587_gshared)(__this, ___gate0, ___parent1, ___index2, method)
// T UniRx.Operators.CombineLatestObserver`1<System.Object>::get_Value()
extern "C"  Il2CppObject * CombineLatestObserver_1_get_Value_m1213727952_gshared (CombineLatestObserver_1_t123567747 * __this, const MethodInfo* method);
#define CombineLatestObserver_1_get_Value_m1213727952(__this, method) ((  Il2CppObject * (*) (CombineLatestObserver_1_t123567747 *, const MethodInfo*))CombineLatestObserver_1_get_Value_m1213727952_gshared)(__this, method)
// System.Void UniRx.Operators.CombineLatestObserver`1<System.Object>::OnNext(T)
extern "C"  void CombineLatestObserver_1_OnNext_m1758727345_gshared (CombineLatestObserver_1_t123567747 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define CombineLatestObserver_1_OnNext_m1758727345(__this, ___value0, method) ((  void (*) (CombineLatestObserver_1_t123567747 *, Il2CppObject *, const MethodInfo*))CombineLatestObserver_1_OnNext_m1758727345_gshared)(__this, ___value0, method)
// System.Void UniRx.Operators.CombineLatestObserver`1<System.Object>::OnError(System.Exception)
extern "C"  void CombineLatestObserver_1_OnError_m3891439040_gshared (CombineLatestObserver_1_t123567747 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method);
#define CombineLatestObserver_1_OnError_m3891439040(__this, ___error0, method) ((  void (*) (CombineLatestObserver_1_t123567747 *, Exception_t1967233988 *, const MethodInfo*))CombineLatestObserver_1_OnError_m3891439040_gshared)(__this, ___error0, method)
// System.Void UniRx.Operators.CombineLatestObserver`1<System.Object>::OnCompleted()
extern "C"  void CombineLatestObserver_1_OnCompleted_m3871078675_gshared (CombineLatestObserver_1_t123567747 * __this, const MethodInfo* method);
#define CombineLatestObserver_1_OnCompleted_m3871078675(__this, method) ((  void (*) (CombineLatestObserver_1_t123567747 *, const MethodInfo*))CombineLatestObserver_1_OnCompleted_m3871078675_gshared)(__this, method)
