﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

// UniRx.Operators.CombineLatestObservable`1<System.Boolean>
struct CombineLatestObservable_1_t2883136337;
// UniRx.IObservable`1<System.Boolean>[]
struct IObservable_1U5BU5D_t1703862084;
// System.IDisposable
struct IDisposable_t1628921374;
// UniRx.IObserver`1<System.Collections.Generic.IList`1<System.Boolean>>
struct IObserver_1_t294529262;
// UniRx.Operators.CombineLatestObservable`1<System.Object>
struct CombineLatestObservable_1_t3509237416;
// UniRx.IObservable`1<System.Object>[]
struct IObservable_1U5BU5D_t2205425841;
// UniRx.IObserver`1<System.Collections.Generic.IList`1<System.Object>>
struct IObserver_1_t920630341;
// UniRx.Operators.CombineLatestObservable`3/CombineLatest/LeftObserver<System.Boolean,System.Boolean,System.Boolean>
struct LeftObserver_t3421766155;
// UniRx.Operators.CombineLatestObservable`3/CombineLatest<System.Boolean,System.Boolean,System.Boolean>
struct CombineLatest_t3519199716;
// System.Exception
struct Exception_t1967233988;
// UniRx.Operators.CombineLatestObservable`3/CombineLatest/LeftObserver<System.Boolean,System.Boolean,System.Object>
struct LeftObserver_t4047867234;
// UniRx.Operators.CombineLatestObservable`3/CombineLatest<System.Boolean,System.Boolean,System.Object>
struct CombineLatest_t4145300795;
// UniRx.Operators.CombineLatestObservable`3/CombineLatest/LeftObserver<System.Boolean,System.Int32,System.Object>
struct LeftObserver_t2052923044;
// UniRx.Operators.CombineLatestObservable`3/CombineLatest<System.Boolean,System.Int32,System.Object>
struct CombineLatest_t2150356605;
// UniRx.Operators.CombineLatestObservable`3/CombineLatest/LeftObserver<System.Object,System.Object,System.Object>
struct LeftObserver_t2250424590;
// UniRx.Operators.CombineLatestObservable`3/CombineLatest<System.Object,System.Object,System.Object>
struct CombineLatest_t2347858151;
// System.Object
struct Il2CppObject;
// UniRx.Operators.CombineLatestObservable`3/CombineLatest/RightObserver<System.Boolean,System.Boolean,System.Boolean>
struct RightObserver_t1984089744;
// UniRx.Operators.CombineLatestObservable`3/CombineLatest/RightObserver<System.Boolean,System.Boolean,System.Object>
struct RightObserver_t2610190823;
// UniRx.Operators.CombineLatestObservable`3/CombineLatest/RightObserver<System.Boolean,System.Int32,System.Object>
struct RightObserver_t615246633;
// UniRx.Operators.CombineLatestObservable`3/CombineLatest/RightObserver<System.Object,System.Object,System.Object>
struct RightObserver_t812748179;
// UniRx.Operators.CombineLatestObservable`3<System.Boolean,System.Boolean,System.Boolean>
struct CombineLatestObservable_3_t3912295671;
// UniRx.IObserver`1<System.Boolean>
struct IObserver_1_t2423004244;
// UniRx.Operators.CombineLatestObservable`3<System.Boolean,System.Boolean,System.Object>
struct CombineLatestObservable_3_t243429454;
// UniRx.IObserver`1<System.Object>
struct IObserver_1_t3049105323;
// UniRx.Operators.CombineLatestObservable`3<System.Boolean,System.Int32,System.Object>
struct CombineLatestObservable_3_t2543452560;
// UniRx.Operators.CombineLatestObservable`3<System.Object,System.Object,System.Object>
struct CombineLatestObservable_3_t2740954106;
// UniRx.IObservable`1<System.Boolean>
struct IObservable_1_t4264771001;
// System.Func`3<System.Boolean,System.Boolean,System.Boolean>
struct Func_3_t3063550794;
// System.Func`3<System.Boolean,System.Boolean,System.Object>
struct Func_3_t3689651873;
// UniRx.IObservable`1<System.Int32>
struct IObservable_1_t2606213151;
// System.Func`3<System.Boolean,System.Int32,System.Object>
struct Func_3_t1694707683;
// UniRx.IObservable`1<System.Object>
struct IObservable_1_t595904784;
// System.Func`3<System.Object,System.Object,System.Object>
struct Func_3_t1892209229;
// UniRx.Operators.CombineLatestObservable`4/CombineLatest<System.Object,System.Object,System.Object,System.Object>
struct CombineLatest_t227530385;
// UniRx.Operators.CombineLatestObservable`4<System.Object,System.Object,System.Object,System.Object>
struct CombineLatestObservable_4_t1056168445;
// UniRx.Operators.CombineLatestFunc`4<System.Object,System.Object,System.Object,System.Object>
struct CombineLatestFunc_4_t382088552;
// UniRx.Operators.CombineLatestObservable`5/CombineLatest<System.Boolean,System.Boolean,System.Boolean,System.Boolean,System.Object>
struct CombineLatest_t2614307975;
// UniRx.Operators.CombineLatestObservable`5<System.Boolean,System.Boolean,System.Boolean,System.Boolean,System.Object>
struct CombineLatestObservable_5_t3418069172;
// UniRx.Operators.CombineLatestObservable`5/CombineLatest<System.Object,System.Object,System.Object,System.Object,System.Object>
struct CombineLatest_t928221951;
// UniRx.Operators.CombineLatestObservable`5<System.Object,System.Object,System.Object,System.Object,System.Object>
struct CombineLatestObservable_5_t1731983148;
// UniRx.Operators.CombineLatestFunc`5<System.Boolean,System.Boolean,System.Boolean,System.Boolean,System.Object>
struct CombineLatestFunc_5_t2231862077;
// UniRx.Operators.CombineLatestFunc`5<System.Object,System.Object,System.Object,System.Object,System.Object>
struct CombineLatestFunc_5_t545776053;
// UniRx.Operators.CombineLatestObservable`6/CombineLatest<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>
struct CombineLatest_t89686297;
// UniRx.Operators.CombineLatestObservable`6<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>
struct CombineLatestObservable_6_t4095141259;
// UniRx.Operators.CombineLatestFunc`6<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>
struct CombineLatestFunc_6_t2285714110;
// UniRx.Operators.CombineLatestObservable`7/CombineLatest<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>
struct CombineLatest_t2495053143;
// UniRx.Operators.CombineLatestObservable`7<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>
struct CombineLatestObservable_7_t1894017470;
// UniRx.Operators.CombineLatestFunc`7<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>
struct CombineLatestFunc_7_t929534559;
// UniRx.Operators.CombineLatestObservable`8/CombineLatest<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>
struct CombineLatest_t1931772513;
// UniRx.Operators.CombineLatestObservable`8<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>
struct CombineLatestObservable_8_t129076473;
// UniRx.Operators.CombineLatestFunc`8<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>
struct CombineLatestFunc_8_t584396980;
// UniRx.Operators.CombineLatestObserver`1<System.Boolean>
struct CombineLatestObserver_1_t3792433964;
// UniRx.Operators.ICombineLatestObservable
struct ICombineLatestObservable_t195534285;
// UniRx.Operators.CombineLatestObserver`1<System.Object>
struct CombineLatestObserver_1_t123567747;
// UniRx.Operators.ConcatObservable`1/<CombineSources>c__Iterator18<System.Object>
struct U3CCombineSourcesU3Ec__Iterator18_t1596040496;
// System.Collections.IEnumerator
struct IEnumerator_t287207039;
// System.Collections.Generic.IEnumerator`1<UniRx.IObservable`1<System.Object>>
struct IEnumerator_1_t2079011232;
// UniRx.Operators.ConcatObservable`1/Concat<System.Object>
struct Concat_t120413325;
// UniRx.Operators.ConcatObservable`1<System.Object>
struct ConcatObservable_1_t1571046694;
// System.Action
struct Action_t437523947;
// System.Collections.Generic.IEnumerable`1<UniRx.IObservable`1<System.Object>>
struct IEnumerable_1_t3468059140;
// UniRx.Operators.ContinueWithObservable`2/ContinueWith<System.Object,System.Object>
struct ContinueWith_t1869588638;
// UniRx.Operators.ContinueWithObservable`2<System.Object,System.Object>
struct ContinueWithObservable_2_t4007622692;
// System.Func`2<System.Object,UniRx.IObservable`1<System.Object>>
struct Func_2_t1894581716;
// UniRx.Operators.CreateObservable`1/Create<System.Boolean>
struct Create_t16300766;
// UniRx.Operators.CreateObservable`1/Create<System.Object>
struct Create_t642401845;
// UniRx.Operators.CreateObservable`1/Create<System.Single>
struct Create_t763504446;
// UniRx.IObserver`1<System.Single>
struct IObserver_1_t3170207924;
// UniRx.Operators.CreateObservable`1/Create<UniRx.Unit>
struct Create_t2363581463;
// UniRx.IObserver`1<UniRx.Unit>
struct IObserver_1_t475317645;
// UniRx.Operators.CreateObservable`1/Create<UnityEngine.Vector2>
struct Create_t3330625213;
// UniRx.IObserver`1<UnityEngine.Vector2>
struct IObserver_1_t1442361395;
// UniRx.Operators.CreateObservable`1<System.Boolean>
struct CreateObservable_1_t1414948727;
// System.Func`2<UniRx.IObserver`1<System.Boolean>,System.IDisposable>
struct Func_2_t2118199298;
// UniRx.Operators.CreateObservable`1<System.Object>
struct CreateObservable_1_t2041049806;
// System.Func`2<UniRx.IObserver`1<System.Object>,System.IDisposable>
struct Func_2_t2619763055;
// UniRx.Operators.CreateObservable`1<System.Single>
struct CreateObservable_1_t2162152407;
// System.Func`2<UniRx.IObserver`1<System.Single>,System.IDisposable>
struct Func_2_t3827670562;
// UniRx.Operators.CreateObservable`1<UniRx.Unit>
struct CreateObservable_1_t3762229424;
// System.Func`2<UniRx.IObserver`1<UniRx.Unit>,System.IDisposable>
struct Func_2_t3876191685;
// UniRx.Operators.CreateObservable`1<UnityEngine.Vector2>
struct CreateObservable_1_t434305878;
// System.Func`2<UniRx.IObserver`1<UnityEngine.Vector2>,System.IDisposable>
struct Func_2_t1054655943;
// UniRx.Operators.CreateSafeObservable`1/CreateSafe<System.Object>
struct CreateSafe_t861772378;
// UniRx.Operators.CreateSafeObservable`1<System.Object>
struct CreateSafeObservable_1_t4182587251;
// UniRx.Operators.DefaultIfEmptyObservable`1/DefaultIfEmpty<System.Object>
struct DefaultIfEmpty_t3912419440;
// UniRx.Operators.DefaultIfEmptyObservable`1<System.Object>
struct DefaultIfEmptyObservable_1_t2718954633;
// UniRx.Operators.DeferObservable`1/Defer<System.Object>
struct Defer_t3471526139;
// UniRx.Operators.DeferObservable`1<System.Object>
struct DeferObservable_1_t3089955988;
// System.Func`1<UniRx.IObservable`1<System.Object>>
struct Func_1_t1738686031;
// UniRx.Operators.DelayFrameObservable`1/DelayFrame/<OnCompletedDelay>c__Iterator27<System.Object>
struct U3COnCompletedDelayU3Ec__Iterator27_t259619244;
// UniRx.Operators.DelayFrameObservable`1/DelayFrame/<OnNextDelay>c__Iterator26<System.Object>
struct U3COnNextDelayU3Ec__Iterator26_t3660093125;
// UniRx.Operators.DelayFrameObservable`1/DelayFrame<System.Object>
struct DelayFrame_t1619463347;
// UniRx.Operators.DelayFrameObservable`1<System.Object>
struct DelayFrameObservable_1_t4117294156;
// UniRx.Operators.DelayFrameSubscriptionObservable`1/<SubscribeCore>c__AnonStorey93<System.Object>
struct U3CSubscribeCoreU3Ec__AnonStorey93_t2770048777;
// UniRx.Operators.DelayFrameSubscriptionObservable`1<System.Object>
struct DelayFrameSubscriptionObservable_1_t2645984001;
// UniRx.IObservable`1<System.Int64>
struct IObservable_1_t2606213246;
// System.Action`1<System.Int64>
struct Action_1_t2995867587;
// UniRx.Operators.DelayObservable`1/Delay<System.Object>
struct Delay_t3380693988;
// UniRx.Operators.DelayObservable`1<System.Object>
struct DelayObservable_1_t2635131389;
// System.Action`1<System.TimeSpan>
struct Action_1_t912315597;
// UniRx.IScheduler
struct IScheduler_t2938318244;
// UniRx.Operators.DelaySubscriptionObservable`1/<SubscribeCore>c__AnonStorey5E<System.Object>
struct U3CSubscribeCoreU3Ec__AnonStorey5E_t2206845999;
// UniRx.Operators.DelaySubscriptionObservable`1/<SubscribeCore>c__AnonStorey5F<System.Object>
struct U3CSubscribeCoreU3Ec__AnonStorey5F_t3873420168;
// UniRx.Operators.DelaySubscriptionObservable`1/<SubscribeCore>c__AnonStorey60<System.Object>
struct U3CSubscribeCoreU3Ec__AnonStorey60_t1692718505;
// UniRx.Operators.DelaySubscriptionObservable`1<System.Object>
struct DelaySubscriptionObservable_1_t1759674162;
// UniRx.Operators.DematerializeObservable`1/Dematerialize<System.Object>
struct Dematerialize_t196388645;
// UniRx.Operators.DematerializeObservable`1<System.Object>
struct DematerializeObservable_1_t1297640382;
// UniRx.Notification`1<System.Object>
struct Notification_1_t38356375;
// UniRx.IObservable`1<UniRx.Notification`1<System.Object>>
struct IObservable_1_t4092122035;
// UniRx.Operators.DistinctObservable`1/Distinct<System.Object>
struct Distinct_t3232980021;
// UniRx.Operators.DistinctObservable`1<System.Object>
struct DistinctObservable_1_t18876622;
// System.Collections.Generic.IEqualityComparer`1<System.Object>
struct IEqualityComparer_1_t3161373071;
// UniRx.Operators.DistinctObservable`2/Distinct<System.Object,System.Object>
struct Distinct_t1668326987;
// UniRx.Operators.DistinctObservable`2<System.Object,System.Object>
struct DistinctObservable_2_t2396543569;
// System.Func`2<System.Object,System.Object>
struct Func_2_t2135783352;
// UniRx.Operators.DistinctUntilChangedObservable`1/DistinctUntilChanged<System.Boolean>
struct DistinctUntilChanged_t1676912132;
// UniRx.Operators.DistinctUntilChangedObservable`1<System.Boolean>
struct DistinctUntilChangedObservable_1_t3199977885;
// UniRx.Operators.DistinctUntilChangedObservable`1/DistinctUntilChanged<System.Int32>
struct DistinctUntilChanged_t18354282;
// UniRx.Operators.DistinctUntilChangedObservable`1<System.Int32>
struct DistinctUntilChangedObservable_1_t1541420035;
// UniRx.IObserver`1<System.Int32>
struct IObserver_1_t764446394;
// UniRx.Operators.DistinctUntilChangedObservable`1/DistinctUntilChanged<System.Object>
struct DistinctUntilChanged_t2303013211;
// UniRx.Operators.DistinctUntilChangedObservable`1<System.Object>
struct DistinctUntilChangedObservable_1_t3826078964;
// System.Collections.Generic.IEqualityComparer`1<System.Boolean>
struct IEqualityComparer_1_t2535271992;
// System.Collections.Generic.IEqualityComparer`1<System.Int32>
struct IEqualityComparer_1_t876714142;
// UniRx.Operators.DistinctUntilChangedObservable`2/DistinctUntilChanged<System.Object,System.Object>
struct DistinctUntilChanged_t2419067341;
// UniRx.Operators.DistinctUntilChangedObservable`2<System.Object,System.Object>
struct DistinctUntilChangedObservable_2_t1596230355;
// UniRx.Operators.DoObservable`1/Do<System.Object>
struct Do_t3185263597;
// UniRx.Operators.DoObservable`1<System.Object>
struct DoObservable_1_t911152645;
// System.Action`1<System.Object>
struct Action_1_t985559125;
// System.Action`1<System.Exception>
struct Action_1_t2115686693;
// UniRx.Operators.DoObserverObservable`1/Do<System.Object>
struct Do_t3185263596;
// UniRx.Operators.DoObserverObservable`1<System.Object>
struct DoObserverObservable_1_t2771775371;
// UniRx.Operators.DoOnCancelObservable`1/DoOnCancel<System.Object>
struct DoOnCancel_t3043692509;
// UniRx.Operators.DoOnCancelObservable`1<System.Object>
struct DoOnCancelObservable_1_t3202318454;
// UniRx.Operators.DoOnCompletedObservable`1/DoOnCompleted<System.Object>
struct DoOnCompleted_t4100057170;
// UniRx.Operators.DoOnCompletedObservable`1<System.Object>
struct DoOnCompletedObservable_1_t3425980779;
// UniRx.Operators.DoOnErrorObservable`1/DoOnError<System.Object>
struct DoOnError_t3716721415;
// UniRx.Operators.DoOnErrorObservable`1<System.Object>
struct DoOnErrorObservable_1_t434875552;
// UniRx.Operators.DoOnSubscribeObservable`1/DoOnSubscribe<System.Object>
struct DoOnSubscribe_t871059449;
// UniRx.Operators.DoOnSubscribeObservable`1<System.Object>
struct DoOnSubscribeObservable_1_t1431598738;
// UniRx.Operators.DoOnTerminateObservable`1/DoOnTerminate<System.Object>
struct DoOnTerminate_t2350010872;
// UniRx.Operators.DoOnTerminateObservable`1<System.Object>
struct DoOnTerminateObservable_1_t3263525393;
// UniRx.Operators.EmptyObservable`1/Empty<System.Double>
struct Empty_t2493334448;
// UniRx.IObserver`1<System.Double>
struct IObserver_1_t2746515517;
// UniRx.Operators.EmptyObservable`1/Empty<System.Int32>
struct Empty_t511265325;
// UniRx.Operators.EmptyObservable`1/Empty<System.Int64>
struct Empty_t511265420;
// UniRx.IObserver`1<System.Int64>
struct IObserver_1_t764446489;
// UniRx.Operators.EmptyObservable`1/Empty<System.Object>
struct Empty_t2795924254;
// UniRx.Operators.EmptyObservable`1/Empty<UniRx.CollectionAddEvent`1<System.Object>>
struct Empty_t80372525;
// UniRx.IObserver`1<UniRx.CollectionAddEvent`1<System.Object>>
struct IObserver_1_t333553594;
// UniRx.Operators.EmptyObservable`1/Empty<UniRx.CollectionAddEvent`1<UniRx.Tuple`2<System.Object,System.Object>>>
struct Empty_t3907495220;
// UniRx.IObserver`1<UniRx.CollectionAddEvent`1<UniRx.Tuple`2<System.Object,System.Object>>>
struct IObserver_1_t4160676289;
// UniRx.Operators.EmptyObservable`1/Empty<UniRx.CollectionMoveEvent`1<System.Object>>
struct Empty_t580284385;
// UniRx.IObserver`1<UniRx.CollectionMoveEvent`1<System.Object>>
struct IObserver_1_t833465454;

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array2840145358.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Comb2883136337.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Comb2883136337MethodDeclarations.h"
#include "mscorlib_System_Void2779279689.h"
#include "Assembly-CSharp-firstpass_ArrayTypes.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Oper1441642626MethodDeclarations.h"
#include "mscorlib_System_Boolean211005341.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Combi242152888.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Combi242152888MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Comb3509237416.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Comb3509237416MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Oper2067743705MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Combi868253967.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Combi868253967MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Comb3421766155.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Comb3421766155MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Comb3519199716.h"
#include "mscorlib_System_Object837106420MethodDeclarations.h"
#include "mscorlib_System_Threading_Monitor2071304733MethodDeclarations.h"
#include "mscorlib_System_Object837106420.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Comb3519199716MethodDeclarations.h"
#include "mscorlib_System_Exception1967233988.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Comb4047867234.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Comb4047867234MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Comb4145300795.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Comb4145300795MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Comb2052923044.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Comb2052923044MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Comb2150356605.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Comb2150356605MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Comb2250424590.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Comb2250424590MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Comb2347858151.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Comb2347858151MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Comb1984089744.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Comb1984089744MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Comb2610190823.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Comb2610190823MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Combi615246633.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Combi615246633MethodDeclarations.h"
#include "mscorlib_System_Int322847414787.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Combi812748179.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Combi812748179MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Comb3912295671.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Operat60103313MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_StableComposit3433635614MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Operat60103313.h"
#include "System_Core_System_Func_3_gen3063550794.h"
#include "System_Core_System_Func_3_gen3063550794MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Combi243429454.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Oper1187768149MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Oper1187768149.h"
#include "System_Core_System_Func_3_gen3689651873.h"
#include "System_Core_System_Func_3_gen3689651873MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Comb2543452560.h"
#include "System_Core_System_Func_3_gen1694707683.h"
#include "System_Core_System_Func_3_gen1694707683MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Comb2740954106.h"
#include "System_Core_System_Func_3_gen1892209229.h"
#include "System_Core_System_Func_3_gen1892209229MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Comb3912295671MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_OptimizedObser2379048080MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_OptimizedObser2379048080.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Oper3570117608MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Combi243429454MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Oper4196218687MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Comb2543452560MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Comb2740954106MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Combi227530385.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Combi227530385MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Comb1056168445.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_NthC2199482330MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Combi123567747.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Combi123567747MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Combi382088552.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Combi382088552MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Comb1056168445MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Comb2614307975.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Comb2614307975MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Comb3418069172.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Comb3792433964.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Comb3792433964MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Comb2231862077.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Comb2231862077MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Combi928221951.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Combi928221951MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Comb1731983148.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Combi545776053.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Combi545776053MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Comb3418069172MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Comb1731983148MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Combin89686297.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Combin89686297MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Comb4095141259.h"
#include "mscorlib_ArrayTypes.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Comb2285714110.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Comb2285714110MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Comb4095141259MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Comb2495053143.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Comb2495053143MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Comb1894017470.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Combi929534559.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Combi929534559MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Comb1894017470MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Comb1931772513.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Comb1931772513MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Combi129076473.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Combi584396980.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Combi584396980MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Combi129076473MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Conc1596040496.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Conc1596040496MethodDeclarations.h"
#include "mscorlib_System_Threading_Interlocked3007803305MethodDeclarations.h"
#include "mscorlib_System_UInt32985925326.h"
#include "mscorlib_System_NotSupportedException1374155497MethodDeclarations.h"
#include "mscorlib_System_NotSupportedException1374155497.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Conca120413325.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Conca120413325MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Conc1571046694.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_SerialDisposab2547852742MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Scheduler_Defa3564383257MethodDeclarations.h"
#include "mscorlib_System_Action_1_gen585976652MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Scheduler103934925MethodDeclarations.h"
#include "System_Core_System_Action437523947MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Disposable3736388786MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_SerialDisposab2547852742.h"
#include "System_Core_System_Action437523947.h"
#include "mscorlib_System_Action_1_gen585976652.h"
#include "mscorlib_System_IntPtr676692020.h"
#include "mscorlib_System_InvalidOperationException2420574324MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_SingleAssignme2336378823MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_SingleAssignme2336378823.h"
#include "mscorlib_System_String968488902.h"
#include "mscorlib_System_InvalidOperationException2420574324.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Conc1571046694MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Cont1869588638.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Cont1869588638MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Cont4007622692.h"
#include "System_Core_System_Func_2_gen1894581716.h"
#include "System_Core_System_Func_2_gen1894581716MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Cont4007622692MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Create16300766.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Create16300766MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Creat642401845.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Creat642401845MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Creat763504446.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Creat763504446MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Oper2516778257MethodDeclarations.h"
#include "mscorlib_System_Single958209021.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Oper2516778257.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Crea2363581463.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Crea2363581463MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Oper4165376397MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Unit2558286038.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Oper4165376397.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Crea3330625213.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Crea3330625213MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Oper2310884405MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Vector23525329788.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Oper2310884405.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Crea1414948727.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Crea1414948727MethodDeclarations.h"
#include "System_Core_System_Func_2_gen2118199298.h"
#include "System_Core_System_Func_2_gen2118199298MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Disposable3736388786.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Crea2041049806.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Crea2041049806MethodDeclarations.h"
#include "System_Core_System_Func_2_gen2619763055.h"
#include "System_Core_System_Func_2_gen2619763055MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Crea2162152407.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Crea2162152407MethodDeclarations.h"
#include "System_Core_System_Func_2_gen3827670562.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Operat22353992MethodDeclarations.h"
#include "System_Core_System_Func_2_gen3827670562MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Crea3762229424.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Crea3762229424MethodDeclarations.h"
#include "System_Core_System_Func_2_gen3876191685.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Oper1622431009MethodDeclarations.h"
#include "System_Core_System_Func_2_gen3876191685MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Creat434305878.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Creat434305878MethodDeclarations.h"
#include "System_Core_System_Func_2_gen1054655943.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Oper2589474759MethodDeclarations.h"
#include "System_Core_System_Func_2_gen1054655943MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Creat861772378.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Creat861772378MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Crea4182587251.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Crea4182587251MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Defa3912419440.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Defa3912419440MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Defa2718954633.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Defa2718954633MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Defe3471526139.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Defe3471526139MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Defe3089955988.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Defe3089955988MethodDeclarations.h"
#include "System_Core_System_Func_1_gen1738686031.h"
#include "System_Core_System_Func_1_gen1738686031MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Observable258071701MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Observable258071701.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Delay259619244.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Delay259619244MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Dela1619463347.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Dela4117294156.h"
#include "UnityEngine_UnityEngine_YieldInstruction3557331758.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_BooleanDisposa3065601722.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_BooleanDisposa3065601722MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Dela3660093125.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Dela3660093125MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Dela1619463347MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_FrameCountType3551601282MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_FrameCountType167759182.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_MainThreadDisp4027217788MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Coroutine2246592261.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Dela4117294156MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Dela2770048777.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Dela2770048777MethodDeclarations.h"
#include "mscorlib_System_Int642847414882.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_MultipleAssign3090232719MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_MultipleAssign3090232719.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Dela2645984001.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Dela2645984001MethodDeclarations.h"
#include "mscorlib_System_Action_1_gen2995867587MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_ObservableExte3095004425MethodDeclarations.h"
#include "mscorlib_System_Action_1_gen2995867587.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_ObservableExte3095004425.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Dela3380693988.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Dela3380693988MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Dela2635131389.h"
#include "mscorlib_System_DateTimeOffset3712260035.h"
#include "System_System_Collections_Generic_Queue_1_gen4227271813.h"
#include "System_System_Collections_Generic_Queue_1_gen4227271813MethodDeclarations.h"
#include "mscorlib_System_TimeSpan763862892.h"
#include "mscorlib_System_DateTimeOffset3712260035MethodDeclarations.h"
#include "mscorlib_System_Action_1_gen1060768302MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Timestamped_1_2519184273.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Timestamped_1_2519184273MethodDeclarations.h"
#include "mscorlib_System_Action_1_gen912315597.h"
#include "mscorlib_System_Action_1_gen1060768302.h"
#include "mscorlib_System_Action_1_gen912315597MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Dela2635131389MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Scheduler103934925.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Dela2206845999.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Dela2206845999MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Dela3873420168.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Dela3873420168MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Dela1759674162.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Dela1692718505.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Dela1692718505MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Dela1759674162MethodDeclarations.h"
#include "mscorlib_System_Nullable_1_gen3649900800MethodDeclarations.h"
#include "mscorlib_System_Nullable_1_gen3649900800.h"
#include "mscorlib_System_Nullable_1_gen2303330647MethodDeclarations.h"
#include "mscorlib_System_Nullable_1_gen2303330647.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Demat196388645.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Demat196388645MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Dema1297640382.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Opera306849478MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Notification_1_g38356375.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_NotificationKi3324123761.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Notification_1_g38356375MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Opera306849478.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Dema1297640382MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Dist3232980021.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Dist3232980021MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Distin18876622.h"
#include "System_Core_System_Collections_Generic_HashSet_1_g3535795091.h"
#include "System_Core_System_Collections_Generic_HashSet_1_g3535795091MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Distin18876622MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Dist1668326987.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Dist1668326987MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Dist2396543569.h"
#include "System_Core_System_Func_2_gen2135783352.h"
#include "System_Core_System_Func_2_gen2135783352MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Dist2396543569MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Dist1676912132.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Dist1676912132MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Dist3199977885.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Distin18354282.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Distin18354282MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Dist1541420035.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Opera701568569MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Opera701568569.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Dist2303013211.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Dist2303013211MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Dist3826078964.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Dist3199977885MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Dist1541420035MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Oper1911559758MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Dist3826078964MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Dist2419067341.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Dist2419067341MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Dist1596230355.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Dist1596230355MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_DoOb_0.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_DoOb_0MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_DoObs911152645.h"
#include "mscorlib_System_Action_1_gen985559125.h"
#include "mscorlib_System_Action_1_gen985559125MethodDeclarations.h"
#include "mscorlib_System_Action_1_gen2115686693MethodDeclarations.h"
#include "mscorlib_System_Action_1_gen2115686693.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_DoObs911152645MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_DoOb3185263596.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_DoOb3185263596MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_DoOb2771775371.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_DoOb2771775371MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_DoOn3043692509.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_DoOn3043692509MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_DoOn3202318454.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_DoOn3202318454MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_DoOn4100057170.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_DoOn4100057170MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_DoOn3425980779.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_DoOn3425980779MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_DoOn3716721415.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_DoOn3716721415MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_DoOnE434875552.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_DoOnE434875552MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_DoOnS871059449.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_DoOnS871059449MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_DoOn1431598738.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_DoOn1431598738MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_DoOn2350010872.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_DoOn2350010872MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_DoOn3263525393.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_DoOn3263525393MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Empt2493334448.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Empt2493334448MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Oper1921935565MethodDeclarations.h"
#include "mscorlib_System_Double534516614.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Oper1921935565.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Empty511265325.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Empty511265325MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Empty511265420.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Empty511265420MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Oper3939730909MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Oper3939730909.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Empt2795924254.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Empt2795924254MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_EmptyO80372525.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_EmptyO80372525MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Oper3545535033MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_CollectionAddE2416521987.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Oper3545535033.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Empt3907495220.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Empt3907495220MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Oper2669133373MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_CollectionAddE1948677386.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Oper2669133373.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Empty580284385.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Empty580284385MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Oper2793497833MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_CollectionMove2916433847.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Oper2793497833.h"

// System.Boolean UniRx.OptimizedObservableExtensions::IsRequiredSubscribeOnCurrentThread<System.Boolean>(UniRx.IObservable`1<!!0>)
extern "C"  bool OptimizedObservableExtensions_IsRequiredSubscribeOnCurrentThread_TisBoolean_t211005341_m545890482_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* p0, const MethodInfo* method);
#define OptimizedObservableExtensions_IsRequiredSubscribeOnCurrentThread_TisBoolean_t211005341_m545890482(__this /* static, unused */, p0, method) ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))OptimizedObservableExtensions_IsRequiredSubscribeOnCurrentThread_TisBoolean_t211005341_m545890482_gshared)(__this /* static, unused */, p0, method)
// System.Boolean UniRx.OptimizedObservableExtensions::IsRequiredSubscribeOnCurrentThread<System.Int32>(UniRx.IObservable`1<!!0>)
extern "C"  bool OptimizedObservableExtensions_IsRequiredSubscribeOnCurrentThread_TisInt32_t2847414787_m3895999500_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* p0, const MethodInfo* method);
#define OptimizedObservableExtensions_IsRequiredSubscribeOnCurrentThread_TisInt32_t2847414787_m3895999500(__this /* static, unused */, p0, method) ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))OptimizedObservableExtensions_IsRequiredSubscribeOnCurrentThread_TisInt32_t2847414787_m3895999500_gshared)(__this /* static, unused */, p0, method)
// System.Boolean UniRx.OptimizedObservableExtensions::IsRequiredSubscribeOnCurrentThread<System.Object>(UniRx.IObservable`1<!!0>)
extern "C"  bool OptimizedObservableExtensions_IsRequiredSubscribeOnCurrentThread_TisIl2CppObject_m1909184259_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* p0, const MethodInfo* method);
#define OptimizedObservableExtensions_IsRequiredSubscribeOnCurrentThread_TisIl2CppObject_m1909184259(__this /* static, unused */, p0, method) ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))OptimizedObservableExtensions_IsRequiredSubscribeOnCurrentThread_TisIl2CppObject_m1909184259_gshared)(__this /* static, unused */, p0, method)
// UniRx.IObservable`1<!!0> UniRx.Observable::Throw<System.Object>(System.Exception)
extern "C"  Il2CppObject* Observable_Throw_TisIl2CppObject_m3997803290_gshared (Il2CppObject * __this /* static, unused */, Exception_t1967233988 * p0, const MethodInfo* method);
#define Observable_Throw_TisIl2CppObject_m3997803290(__this /* static, unused */, p0, method) ((  Il2CppObject* (*) (Il2CppObject * /* static, unused */, Exception_t1967233988 *, const MethodInfo*))Observable_Throw_TisIl2CppObject_m3997803290_gshared)(__this /* static, unused */, p0, method)
// System.IDisposable UniRx.ObservableExtensions::Subscribe<System.Int64>(UniRx.IObservable`1<!!0>,System.Action`1<!!0>)
extern "C"  Il2CppObject * ObservableExtensions_Subscribe_TisInt64_t2847414882_m1751770470_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* p0, Action_1_t2995867587 * p1, const MethodInfo* method);
#define ObservableExtensions_Subscribe_TisInt64_t2847414882_m1751770470(__this /* static, unused */, p0, p1, method) ((  Il2CppObject * (*) (Il2CppObject * /* static, unused */, Il2CppObject*, Action_1_t2995867587 *, const MethodInfo*))ObservableExtensions_Subscribe_TisInt64_t2847414882_m1751770470_gshared)(__this /* static, unused */, p0, p1, method)
// System.Boolean UniRx.OptimizedObservableExtensions::IsRequiredSubscribeOnCurrentThread<UniRx.Notification`1<System.Object>>(UniRx.IObservable`1<!!0>)
#define OptimizedObservableExtensions_IsRequiredSubscribeOnCurrentThread_TisNotification_1_t38356375_m1723816725(__this /* static, unused */, p0, method) ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))OptimizedObservableExtensions_IsRequiredSubscribeOnCurrentThread_TisIl2CppObject_m1909184259_gshared)(__this /* static, unused */, p0, method)
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void UniRx.Operators.CombineLatestObservable`1<System.Boolean>::.ctor(UniRx.IObservable`1<T>[])
extern "C"  void CombineLatestObservable_1__ctor_m2219293560_gshared (CombineLatestObservable_1_t2883136337 * __this, IObservable_1U5BU5D_t1703862084* ___sources0, const MethodInfo* method)
{
	{
		NullCheck((OperatorObservableBase_1_t1441642626 *)__this);
		((  void (*) (OperatorObservableBase_1_t1441642626 *, bool, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((OperatorObservableBase_1_t1441642626 *)__this, (bool)1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		IObservable_1U5BU5D_t1703862084* L_0 = ___sources0;
		__this->set_sources_1(L_0);
		return;
	}
}
// System.IDisposable UniRx.Operators.CombineLatestObservable`1<System.Boolean>::SubscribeCore(UniRx.IObserver`1<System.Collections.Generic.IList`1<T>>,System.IDisposable)
extern "C"  Il2CppObject * CombineLatestObservable_1_SubscribeCore_m1252997412_gshared (CombineLatestObservable_1_t2883136337 * __this, Il2CppObject* ___observer0, Il2CppObject * ___cancel1, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = ___observer0;
		Il2CppObject * L_1 = ___cancel1;
		CombineLatest_t242152888 * L_2 = (CombineLatest_t242152888 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		((  void (*) (CombineLatest_t242152888 *, CombineLatestObservable_1_t2883136337 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)(L_2, (CombineLatestObservable_1_t2883136337 *)__this, (Il2CppObject*)L_0, (Il2CppObject *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		NullCheck((CombineLatest_t242152888 *)L_2);
		Il2CppObject * L_3 = ((  Il2CppObject * (*) (CombineLatest_t242152888 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)((CombineLatest_t242152888 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		return L_3;
	}
}
// System.Void UniRx.Operators.CombineLatestObservable`1<System.Object>::.ctor(UniRx.IObservable`1<T>[])
extern "C"  void CombineLatestObservable_1__ctor_m3644694771_gshared (CombineLatestObservable_1_t3509237416 * __this, IObservable_1U5BU5D_t2205425841* ___sources0, const MethodInfo* method)
{
	{
		NullCheck((OperatorObservableBase_1_t2067743705 *)__this);
		((  void (*) (OperatorObservableBase_1_t2067743705 *, bool, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((OperatorObservableBase_1_t2067743705 *)__this, (bool)1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		IObservable_1U5BU5D_t2205425841* L_0 = ___sources0;
		__this->set_sources_1(L_0);
		return;
	}
}
// System.IDisposable UniRx.Operators.CombineLatestObservable`1<System.Object>::SubscribeCore(UniRx.IObserver`1<System.Collections.Generic.IList`1<T>>,System.IDisposable)
extern "C"  Il2CppObject * CombineLatestObservable_1_SubscribeCore_m3998794463_gshared (CombineLatestObservable_1_t3509237416 * __this, Il2CppObject* ___observer0, Il2CppObject * ___cancel1, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = ___observer0;
		Il2CppObject * L_1 = ___cancel1;
		CombineLatest_t868253967 * L_2 = (CombineLatest_t868253967 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		((  void (*) (CombineLatest_t868253967 *, CombineLatestObservable_1_t3509237416 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)(L_2, (CombineLatestObservable_1_t3509237416 *)__this, (Il2CppObject*)L_0, (Il2CppObject *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		NullCheck((CombineLatest_t868253967 *)L_2);
		Il2CppObject * L_3 = ((  Il2CppObject * (*) (CombineLatest_t868253967 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)((CombineLatest_t868253967 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		return L_3;
	}
}
// System.Void UniRx.Operators.CombineLatestObservable`3/CombineLatest/LeftObserver<System.Boolean,System.Boolean,System.Boolean>::.ctor(UniRx.Operators.CombineLatestObservable`3/CombineLatest<TLeft,TRight,TResult>)
extern "C"  void LeftObserver__ctor_m195482764_gshared (LeftObserver_t3421766155 * __this, CombineLatest_t3519199716 * ___parent0, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		CombineLatest_t3519199716 * L_0 = ___parent0;
		__this->set_parent_0(L_0);
		return;
	}
}
// System.Void UniRx.Operators.CombineLatestObservable`3/CombineLatest/LeftObserver<System.Boolean,System.Boolean,System.Boolean>::OnNext(TLeft)
extern "C"  void LeftObserver_OnNext_m3726988583_gshared (LeftObserver_t3421766155 * __this, bool ___value0, const MethodInfo* method)
{
	Il2CppObject * V_0 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		CombineLatest_t3519199716 * L_0 = (CombineLatest_t3519199716 *)__this->get_parent_0();
		NullCheck(L_0);
		Il2CppObject * L_1 = (Il2CppObject *)L_0->get_gate_3();
		V_0 = (Il2CppObject *)L_1;
		Il2CppObject * L_2 = V_0;
		Monitor_Enter_m476686225(NULL /*static, unused*/, (Il2CppObject *)L_2, /*hidden argument*/NULL);
	}

IL_0012:
	try
	{ // begin try (depth: 1)
		CombineLatest_t3519199716 * L_3 = (CombineLatest_t3519199716 *)__this->get_parent_0();
		NullCheck(L_3);
		L_3->set_leftStarted_5((bool)1);
		CombineLatest_t3519199716 * L_4 = (CombineLatest_t3519199716 *)__this->get_parent_0();
		bool L_5 = ___value0;
		NullCheck(L_4);
		L_4->set_leftValue_4(L_5);
		CombineLatest_t3519199716 * L_6 = (CombineLatest_t3519199716 *)__this->get_parent_0();
		NullCheck((CombineLatest_t3519199716 *)L_6);
		((  void (*) (CombineLatest_t3519199716 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((CombineLatest_t3519199716 *)L_6, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		IL2CPP_LEAVE(0x41, FINALLY_003a);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_003a;
	}

FINALLY_003a:
	{ // begin finally (depth: 1)
		Il2CppObject * L_7 = V_0;
		Monitor_Exit_m2088237919(NULL /*static, unused*/, (Il2CppObject *)L_7, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(58)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(58)
	{
		IL2CPP_JUMP_TBL(0x41, IL_0041)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0041:
	{
		return;
	}
}
// System.Void UniRx.Operators.CombineLatestObservable`3/CombineLatest/LeftObserver<System.Boolean,System.Boolean,System.Boolean>::OnError(System.Exception)
extern "C"  void LeftObserver_OnError_m844779005_gshared (LeftObserver_t3421766155 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method)
{
	Il2CppObject * V_0 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		CombineLatest_t3519199716 * L_0 = (CombineLatest_t3519199716 *)__this->get_parent_0();
		NullCheck(L_0);
		Il2CppObject * L_1 = (Il2CppObject *)L_0->get_gate_3();
		V_0 = (Il2CppObject *)L_1;
		Il2CppObject * L_2 = V_0;
		Monitor_Enter_m476686225(NULL /*static, unused*/, (Il2CppObject *)L_2, /*hidden argument*/NULL);
	}

IL_0012:
	try
	{ // begin try (depth: 1)
		CombineLatest_t3519199716 * L_3 = (CombineLatest_t3519199716 *)__this->get_parent_0();
		Exception_t1967233988 * L_4 = ___error0;
		NullCheck((CombineLatest_t3519199716 *)L_3);
		VirtActionInvoker1< Exception_t1967233988 * >::Invoke(9 /* System.Void UniRx.Operators.CombineLatestObservable`3/CombineLatest<System.Boolean,System.Boolean,System.Boolean>::OnError(System.Exception) */, (CombineLatest_t3519199716 *)L_3, (Exception_t1967233988 *)L_4);
		IL2CPP_LEAVE(0x2A, FINALLY_0023);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0023;
	}

FINALLY_0023:
	{ // begin finally (depth: 1)
		Il2CppObject * L_5 = V_0;
		Monitor_Exit_m2088237919(NULL /*static, unused*/, (Il2CppObject *)L_5, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(35)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(35)
	{
		IL2CPP_JUMP_TBL(0x2A, IL_002a)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_002a:
	{
		return;
	}
}
// System.Void UniRx.Operators.CombineLatestObservable`3/CombineLatest/LeftObserver<System.Boolean,System.Boolean,System.Boolean>::OnCompleted()
extern "C"  void LeftObserver_OnCompleted_m823678160_gshared (LeftObserver_t3421766155 * __this, const MethodInfo* method)
{
	Il2CppObject * V_0 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		CombineLatest_t3519199716 * L_0 = (CombineLatest_t3519199716 *)__this->get_parent_0();
		NullCheck(L_0);
		Il2CppObject * L_1 = (Il2CppObject *)L_0->get_gate_3();
		V_0 = (Il2CppObject *)L_1;
		Il2CppObject * L_2 = V_0;
		Monitor_Enter_m476686225(NULL /*static, unused*/, (Il2CppObject *)L_2, /*hidden argument*/NULL);
	}

IL_0012:
	try
	{ // begin try (depth: 1)
		{
			CombineLatest_t3519199716 * L_3 = (CombineLatest_t3519199716 *)__this->get_parent_0();
			NullCheck(L_3);
			L_3->set_leftCompleted_6((bool)1);
			CombineLatest_t3519199716 * L_4 = (CombineLatest_t3519199716 *)__this->get_parent_0();
			NullCheck(L_4);
			bool L_5 = (bool)L_4->get_rightCompleted_9();
			if (!L_5)
			{
				goto IL_0039;
			}
		}

IL_002e:
		{
			CombineLatest_t3519199716 * L_6 = (CombineLatest_t3519199716 *)__this->get_parent_0();
			NullCheck((CombineLatest_t3519199716 *)L_6);
			VirtActionInvoker0::Invoke(10 /* System.Void UniRx.Operators.CombineLatestObservable`3/CombineLatest<System.Boolean,System.Boolean,System.Boolean>::OnCompleted() */, (CombineLatest_t3519199716 *)L_6);
		}

IL_0039:
		{
			IL2CPP_LEAVE(0x45, FINALLY_003e);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_003e;
	}

FINALLY_003e:
	{ // begin finally (depth: 1)
		Il2CppObject * L_7 = V_0;
		Monitor_Exit_m2088237919(NULL /*static, unused*/, (Il2CppObject *)L_7, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(62)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(62)
	{
		IL2CPP_JUMP_TBL(0x45, IL_0045)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0045:
	{
		return;
	}
}
// System.Void UniRx.Operators.CombineLatestObservable`3/CombineLatest/LeftObserver<System.Boolean,System.Boolean,System.Object>::.ctor(UniRx.Operators.CombineLatestObservable`3/CombineLatest<TLeft,TRight,TResult>)
extern "C"  void LeftObserver__ctor_m1047300263_gshared (LeftObserver_t4047867234 * __this, CombineLatest_t4145300795 * ___parent0, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		CombineLatest_t4145300795 * L_0 = ___parent0;
		__this->set_parent_0(L_0);
		return;
	}
}
// System.Void UniRx.Operators.CombineLatestObservable`3/CombineLatest/LeftObserver<System.Boolean,System.Boolean,System.Object>::OnNext(TLeft)
extern "C"  void LeftObserver_OnNext_m3935101292_gshared (LeftObserver_t4047867234 * __this, bool ___value0, const MethodInfo* method)
{
	Il2CppObject * V_0 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		CombineLatest_t4145300795 * L_0 = (CombineLatest_t4145300795 *)__this->get_parent_0();
		NullCheck(L_0);
		Il2CppObject * L_1 = (Il2CppObject *)L_0->get_gate_3();
		V_0 = (Il2CppObject *)L_1;
		Il2CppObject * L_2 = V_0;
		Monitor_Enter_m476686225(NULL /*static, unused*/, (Il2CppObject *)L_2, /*hidden argument*/NULL);
	}

IL_0012:
	try
	{ // begin try (depth: 1)
		CombineLatest_t4145300795 * L_3 = (CombineLatest_t4145300795 *)__this->get_parent_0();
		NullCheck(L_3);
		L_3->set_leftStarted_5((bool)1);
		CombineLatest_t4145300795 * L_4 = (CombineLatest_t4145300795 *)__this->get_parent_0();
		bool L_5 = ___value0;
		NullCheck(L_4);
		L_4->set_leftValue_4(L_5);
		CombineLatest_t4145300795 * L_6 = (CombineLatest_t4145300795 *)__this->get_parent_0();
		NullCheck((CombineLatest_t4145300795 *)L_6);
		((  void (*) (CombineLatest_t4145300795 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((CombineLatest_t4145300795 *)L_6, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		IL2CPP_LEAVE(0x41, FINALLY_003a);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_003a;
	}

FINALLY_003a:
	{ // begin finally (depth: 1)
		Il2CppObject * L_7 = V_0;
		Monitor_Exit_m2088237919(NULL /*static, unused*/, (Il2CppObject *)L_7, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(58)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(58)
	{
		IL2CPP_JUMP_TBL(0x41, IL_0041)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0041:
	{
		return;
	}
}
// System.Void UniRx.Operators.CombineLatestObservable`3/CombineLatest/LeftObserver<System.Boolean,System.Boolean,System.Object>::OnError(System.Exception)
extern "C"  void LeftObserver_OnError_m3441466050_gshared (LeftObserver_t4047867234 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method)
{
	Il2CppObject * V_0 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		CombineLatest_t4145300795 * L_0 = (CombineLatest_t4145300795 *)__this->get_parent_0();
		NullCheck(L_0);
		Il2CppObject * L_1 = (Il2CppObject *)L_0->get_gate_3();
		V_0 = (Il2CppObject *)L_1;
		Il2CppObject * L_2 = V_0;
		Monitor_Enter_m476686225(NULL /*static, unused*/, (Il2CppObject *)L_2, /*hidden argument*/NULL);
	}

IL_0012:
	try
	{ // begin try (depth: 1)
		CombineLatest_t4145300795 * L_3 = (CombineLatest_t4145300795 *)__this->get_parent_0();
		Exception_t1967233988 * L_4 = ___error0;
		NullCheck((CombineLatest_t4145300795 *)L_3);
		VirtActionInvoker1< Exception_t1967233988 * >::Invoke(9 /* System.Void UniRx.Operators.CombineLatestObservable`3/CombineLatest<System.Boolean,System.Boolean,System.Object>::OnError(System.Exception) */, (CombineLatest_t4145300795 *)L_3, (Exception_t1967233988 *)L_4);
		IL2CPP_LEAVE(0x2A, FINALLY_0023);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0023;
	}

FINALLY_0023:
	{ // begin finally (depth: 1)
		Il2CppObject * L_5 = V_0;
		Monitor_Exit_m2088237919(NULL /*static, unused*/, (Il2CppObject *)L_5, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(35)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(35)
	{
		IL2CPP_JUMP_TBL(0x2A, IL_002a)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_002a:
	{
		return;
	}
}
// System.Void UniRx.Operators.CombineLatestObservable`3/CombineLatest/LeftObserver<System.Boolean,System.Boolean,System.Object>::OnCompleted()
extern "C"  void LeftObserver_OnCompleted_m1031790869_gshared (LeftObserver_t4047867234 * __this, const MethodInfo* method)
{
	Il2CppObject * V_0 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		CombineLatest_t4145300795 * L_0 = (CombineLatest_t4145300795 *)__this->get_parent_0();
		NullCheck(L_0);
		Il2CppObject * L_1 = (Il2CppObject *)L_0->get_gate_3();
		V_0 = (Il2CppObject *)L_1;
		Il2CppObject * L_2 = V_0;
		Monitor_Enter_m476686225(NULL /*static, unused*/, (Il2CppObject *)L_2, /*hidden argument*/NULL);
	}

IL_0012:
	try
	{ // begin try (depth: 1)
		{
			CombineLatest_t4145300795 * L_3 = (CombineLatest_t4145300795 *)__this->get_parent_0();
			NullCheck(L_3);
			L_3->set_leftCompleted_6((bool)1);
			CombineLatest_t4145300795 * L_4 = (CombineLatest_t4145300795 *)__this->get_parent_0();
			NullCheck(L_4);
			bool L_5 = (bool)L_4->get_rightCompleted_9();
			if (!L_5)
			{
				goto IL_0039;
			}
		}

IL_002e:
		{
			CombineLatest_t4145300795 * L_6 = (CombineLatest_t4145300795 *)__this->get_parent_0();
			NullCheck((CombineLatest_t4145300795 *)L_6);
			VirtActionInvoker0::Invoke(10 /* System.Void UniRx.Operators.CombineLatestObservable`3/CombineLatest<System.Boolean,System.Boolean,System.Object>::OnCompleted() */, (CombineLatest_t4145300795 *)L_6);
		}

IL_0039:
		{
			IL2CPP_LEAVE(0x45, FINALLY_003e);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_003e;
	}

FINALLY_003e:
	{ // begin finally (depth: 1)
		Il2CppObject * L_7 = V_0;
		Monitor_Exit_m2088237919(NULL /*static, unused*/, (Il2CppObject *)L_7, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(62)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(62)
	{
		IL2CPP_JUMP_TBL(0x45, IL_0045)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0045:
	{
		return;
	}
}
// System.Void UniRx.Operators.CombineLatestObservable`3/CombineLatest/LeftObserver<System.Boolean,System.Int32,System.Object>::.ctor(UniRx.Operators.CombineLatestObservable`3/CombineLatest<TLeft,TRight,TResult>)
extern "C"  void LeftObserver__ctor_m1684636417_gshared (LeftObserver_t2052923044 * __this, CombineLatest_t2150356605 * ___parent0, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		CombineLatest_t2150356605 * L_0 = ___parent0;
		__this->set_parent_0(L_0);
		return;
	}
}
// System.Void UniRx.Operators.CombineLatestObservable`3/CombineLatest/LeftObserver<System.Boolean,System.Int32,System.Object>::OnNext(TLeft)
extern "C"  void LeftObserver_OnNext_m1356048466_gshared (LeftObserver_t2052923044 * __this, bool ___value0, const MethodInfo* method)
{
	Il2CppObject * V_0 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		CombineLatest_t2150356605 * L_0 = (CombineLatest_t2150356605 *)__this->get_parent_0();
		NullCheck(L_0);
		Il2CppObject * L_1 = (Il2CppObject *)L_0->get_gate_3();
		V_0 = (Il2CppObject *)L_1;
		Il2CppObject * L_2 = V_0;
		Monitor_Enter_m476686225(NULL /*static, unused*/, (Il2CppObject *)L_2, /*hidden argument*/NULL);
	}

IL_0012:
	try
	{ // begin try (depth: 1)
		CombineLatest_t2150356605 * L_3 = (CombineLatest_t2150356605 *)__this->get_parent_0();
		NullCheck(L_3);
		L_3->set_leftStarted_5((bool)1);
		CombineLatest_t2150356605 * L_4 = (CombineLatest_t2150356605 *)__this->get_parent_0();
		bool L_5 = ___value0;
		NullCheck(L_4);
		L_4->set_leftValue_4(L_5);
		CombineLatest_t2150356605 * L_6 = (CombineLatest_t2150356605 *)__this->get_parent_0();
		NullCheck((CombineLatest_t2150356605 *)L_6);
		((  void (*) (CombineLatest_t2150356605 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((CombineLatest_t2150356605 *)L_6, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		IL2CPP_LEAVE(0x41, FINALLY_003a);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_003a;
	}

FINALLY_003a:
	{ // begin finally (depth: 1)
		Il2CppObject * L_7 = V_0;
		Monitor_Exit_m2088237919(NULL /*static, unused*/, (Il2CppObject *)L_7, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(58)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(58)
	{
		IL2CPP_JUMP_TBL(0x41, IL_0041)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0041:
	{
		return;
	}
}
// System.Void UniRx.Operators.CombineLatestObservable`3/CombineLatest/LeftObserver<System.Boolean,System.Int32,System.Object>::OnError(System.Exception)
extern "C"  void LeftObserver_OnError_m2602973352_gshared (LeftObserver_t2052923044 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method)
{
	Il2CppObject * V_0 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		CombineLatest_t2150356605 * L_0 = (CombineLatest_t2150356605 *)__this->get_parent_0();
		NullCheck(L_0);
		Il2CppObject * L_1 = (Il2CppObject *)L_0->get_gate_3();
		V_0 = (Il2CppObject *)L_1;
		Il2CppObject * L_2 = V_0;
		Monitor_Enter_m476686225(NULL /*static, unused*/, (Il2CppObject *)L_2, /*hidden argument*/NULL);
	}

IL_0012:
	try
	{ // begin try (depth: 1)
		CombineLatest_t2150356605 * L_3 = (CombineLatest_t2150356605 *)__this->get_parent_0();
		Exception_t1967233988 * L_4 = ___error0;
		NullCheck((CombineLatest_t2150356605 *)L_3);
		VirtActionInvoker1< Exception_t1967233988 * >::Invoke(9 /* System.Void UniRx.Operators.CombineLatestObservable`3/CombineLatest<System.Boolean,System.Int32,System.Object>::OnError(System.Exception) */, (CombineLatest_t2150356605 *)L_3, (Exception_t1967233988 *)L_4);
		IL2CPP_LEAVE(0x2A, FINALLY_0023);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0023;
	}

FINALLY_0023:
	{ // begin finally (depth: 1)
		Il2CppObject * L_5 = V_0;
		Monitor_Exit_m2088237919(NULL /*static, unused*/, (Il2CppObject *)L_5, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(35)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(35)
	{
		IL2CPP_JUMP_TBL(0x2A, IL_002a)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_002a:
	{
		return;
	}
}
// System.Void UniRx.Operators.CombineLatestObservable`3/CombineLatest/LeftObserver<System.Boolean,System.Int32,System.Object>::OnCompleted()
extern "C"  void LeftObserver_OnCompleted_m2747705339_gshared (LeftObserver_t2052923044 * __this, const MethodInfo* method)
{
	Il2CppObject * V_0 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		CombineLatest_t2150356605 * L_0 = (CombineLatest_t2150356605 *)__this->get_parent_0();
		NullCheck(L_0);
		Il2CppObject * L_1 = (Il2CppObject *)L_0->get_gate_3();
		V_0 = (Il2CppObject *)L_1;
		Il2CppObject * L_2 = V_0;
		Monitor_Enter_m476686225(NULL /*static, unused*/, (Il2CppObject *)L_2, /*hidden argument*/NULL);
	}

IL_0012:
	try
	{ // begin try (depth: 1)
		{
			CombineLatest_t2150356605 * L_3 = (CombineLatest_t2150356605 *)__this->get_parent_0();
			NullCheck(L_3);
			L_3->set_leftCompleted_6((bool)1);
			CombineLatest_t2150356605 * L_4 = (CombineLatest_t2150356605 *)__this->get_parent_0();
			NullCheck(L_4);
			bool L_5 = (bool)L_4->get_rightCompleted_9();
			if (!L_5)
			{
				goto IL_0039;
			}
		}

IL_002e:
		{
			CombineLatest_t2150356605 * L_6 = (CombineLatest_t2150356605 *)__this->get_parent_0();
			NullCheck((CombineLatest_t2150356605 *)L_6);
			VirtActionInvoker0::Invoke(10 /* System.Void UniRx.Operators.CombineLatestObservable`3/CombineLatest<System.Boolean,System.Int32,System.Object>::OnCompleted() */, (CombineLatest_t2150356605 *)L_6);
		}

IL_0039:
		{
			IL2CPP_LEAVE(0x45, FINALLY_003e);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_003e;
	}

FINALLY_003e:
	{ // begin finally (depth: 1)
		Il2CppObject * L_7 = V_0;
		Monitor_Exit_m2088237919(NULL /*static, unused*/, (Il2CppObject *)L_7, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(62)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(62)
	{
		IL2CPP_JUMP_TBL(0x45, IL_0045)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0045:
	{
		return;
	}
}
// System.Void UniRx.Operators.CombineLatestObservable`3/CombineLatest/LeftObserver<System.Object,System.Object,System.Object>::.ctor(UniRx.Operators.CombineLatestObservable`3/CombineLatest<TLeft,TRight,TResult>)
extern "C"  void LeftObserver__ctor_m353944227_gshared (LeftObserver_t2250424590 * __this, CombineLatest_t2347858151 * ___parent0, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		CombineLatest_t2347858151 * L_0 = ___parent0;
		__this->set_parent_0(L_0);
		return;
	}
}
// System.Void UniRx.Operators.CombineLatestObservable`3/CombineLatest/LeftObserver<System.Object,System.Object,System.Object>::OnNext(TLeft)
extern "C"  void LeftObserver_OnNext_m705022704_gshared (LeftObserver_t2250424590 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	Il2CppObject * V_0 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		CombineLatest_t2347858151 * L_0 = (CombineLatest_t2347858151 *)__this->get_parent_0();
		NullCheck(L_0);
		Il2CppObject * L_1 = (Il2CppObject *)L_0->get_gate_3();
		V_0 = (Il2CppObject *)L_1;
		Il2CppObject * L_2 = V_0;
		Monitor_Enter_m476686225(NULL /*static, unused*/, (Il2CppObject *)L_2, /*hidden argument*/NULL);
	}

IL_0012:
	try
	{ // begin try (depth: 1)
		CombineLatest_t2347858151 * L_3 = (CombineLatest_t2347858151 *)__this->get_parent_0();
		NullCheck(L_3);
		L_3->set_leftStarted_5((bool)1);
		CombineLatest_t2347858151 * L_4 = (CombineLatest_t2347858151 *)__this->get_parent_0();
		Il2CppObject * L_5 = ___value0;
		NullCheck(L_4);
		L_4->set_leftValue_4(L_5);
		CombineLatest_t2347858151 * L_6 = (CombineLatest_t2347858151 *)__this->get_parent_0();
		NullCheck((CombineLatest_t2347858151 *)L_6);
		((  void (*) (CombineLatest_t2347858151 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((CombineLatest_t2347858151 *)L_6, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		IL2CPP_LEAVE(0x41, FINALLY_003a);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_003a;
	}

FINALLY_003a:
	{ // begin finally (depth: 1)
		Il2CppObject * L_7 = V_0;
		Monitor_Exit_m2088237919(NULL /*static, unused*/, (Il2CppObject *)L_7, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(58)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(58)
	{
		IL2CPP_JUMP_TBL(0x41, IL_0041)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0041:
	{
		return;
	}
}
// System.Void UniRx.Operators.CombineLatestObservable`3/CombineLatest/LeftObserver<System.Object,System.Object,System.Object>::OnError(System.Exception)
extern "C"  void LeftObserver_OnError_m3574816326_gshared (LeftObserver_t2250424590 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method)
{
	Il2CppObject * V_0 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		CombineLatest_t2347858151 * L_0 = (CombineLatest_t2347858151 *)__this->get_parent_0();
		NullCheck(L_0);
		Il2CppObject * L_1 = (Il2CppObject *)L_0->get_gate_3();
		V_0 = (Il2CppObject *)L_1;
		Il2CppObject * L_2 = V_0;
		Monitor_Enter_m476686225(NULL /*static, unused*/, (Il2CppObject *)L_2, /*hidden argument*/NULL);
	}

IL_0012:
	try
	{ // begin try (depth: 1)
		CombineLatest_t2347858151 * L_3 = (CombineLatest_t2347858151 *)__this->get_parent_0();
		Exception_t1967233988 * L_4 = ___error0;
		NullCheck((CombineLatest_t2347858151 *)L_3);
		VirtActionInvoker1< Exception_t1967233988 * >::Invoke(9 /* System.Void UniRx.Operators.CombineLatestObservable`3/CombineLatest<System.Object,System.Object,System.Object>::OnError(System.Exception) */, (CombineLatest_t2347858151 *)L_3, (Exception_t1967233988 *)L_4);
		IL2CPP_LEAVE(0x2A, FINALLY_0023);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0023;
	}

FINALLY_0023:
	{ // begin finally (depth: 1)
		Il2CppObject * L_5 = V_0;
		Monitor_Exit_m2088237919(NULL /*static, unused*/, (Il2CppObject *)L_5, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(35)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(35)
	{
		IL2CPP_JUMP_TBL(0x2A, IL_002a)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_002a:
	{
		return;
	}
}
// System.Void UniRx.Operators.CombineLatestObservable`3/CombineLatest/LeftObserver<System.Object,System.Object,System.Object>::OnCompleted()
extern "C"  void LeftObserver_OnCompleted_m2096679577_gshared (LeftObserver_t2250424590 * __this, const MethodInfo* method)
{
	Il2CppObject * V_0 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		CombineLatest_t2347858151 * L_0 = (CombineLatest_t2347858151 *)__this->get_parent_0();
		NullCheck(L_0);
		Il2CppObject * L_1 = (Il2CppObject *)L_0->get_gate_3();
		V_0 = (Il2CppObject *)L_1;
		Il2CppObject * L_2 = V_0;
		Monitor_Enter_m476686225(NULL /*static, unused*/, (Il2CppObject *)L_2, /*hidden argument*/NULL);
	}

IL_0012:
	try
	{ // begin try (depth: 1)
		{
			CombineLatest_t2347858151 * L_3 = (CombineLatest_t2347858151 *)__this->get_parent_0();
			NullCheck(L_3);
			L_3->set_leftCompleted_6((bool)1);
			CombineLatest_t2347858151 * L_4 = (CombineLatest_t2347858151 *)__this->get_parent_0();
			NullCheck(L_4);
			bool L_5 = (bool)L_4->get_rightCompleted_9();
			if (!L_5)
			{
				goto IL_0039;
			}
		}

IL_002e:
		{
			CombineLatest_t2347858151 * L_6 = (CombineLatest_t2347858151 *)__this->get_parent_0();
			NullCheck((CombineLatest_t2347858151 *)L_6);
			VirtActionInvoker0::Invoke(10 /* System.Void UniRx.Operators.CombineLatestObservable`3/CombineLatest<System.Object,System.Object,System.Object>::OnCompleted() */, (CombineLatest_t2347858151 *)L_6);
		}

IL_0039:
		{
			IL2CPP_LEAVE(0x45, FINALLY_003e);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_003e;
	}

FINALLY_003e:
	{ // begin finally (depth: 1)
		Il2CppObject * L_7 = V_0;
		Monitor_Exit_m2088237919(NULL /*static, unused*/, (Il2CppObject *)L_7, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(62)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(62)
	{
		IL2CPP_JUMP_TBL(0x45, IL_0045)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0045:
	{
		return;
	}
}
// System.Void UniRx.Operators.CombineLatestObservable`3/CombineLatest/RightObserver<System.Boolean,System.Boolean,System.Boolean>::.ctor(UniRx.Operators.CombineLatestObservable`3/CombineLatest<TLeft,TRight,TResult>)
extern "C"  void RightObserver__ctor_m3652537715_gshared (RightObserver_t1984089744 * __this, CombineLatest_t3519199716 * ___parent0, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		CombineLatest_t3519199716 * L_0 = ___parent0;
		__this->set_parent_0(L_0);
		return;
	}
}
// System.Void UniRx.Operators.CombineLatestObservable`3/CombineLatest/RightObserver<System.Boolean,System.Boolean,System.Boolean>::OnNext(TRight)
extern "C"  void RightObserver_OnNext_m270056495_gshared (RightObserver_t1984089744 * __this, bool ___value0, const MethodInfo* method)
{
	Il2CppObject * V_0 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		CombineLatest_t3519199716 * L_0 = (CombineLatest_t3519199716 *)__this->get_parent_0();
		NullCheck(L_0);
		Il2CppObject * L_1 = (Il2CppObject *)L_0->get_gate_3();
		V_0 = (Il2CppObject *)L_1;
		Il2CppObject * L_2 = V_0;
		Monitor_Enter_m476686225(NULL /*static, unused*/, (Il2CppObject *)L_2, /*hidden argument*/NULL);
	}

IL_0012:
	try
	{ // begin try (depth: 1)
		CombineLatest_t3519199716 * L_3 = (CombineLatest_t3519199716 *)__this->get_parent_0();
		NullCheck(L_3);
		L_3->set_rightStarted_8((bool)1);
		CombineLatest_t3519199716 * L_4 = (CombineLatest_t3519199716 *)__this->get_parent_0();
		bool L_5 = ___value0;
		NullCheck(L_4);
		L_4->set_rightValue_7(L_5);
		CombineLatest_t3519199716 * L_6 = (CombineLatest_t3519199716 *)__this->get_parent_0();
		NullCheck((CombineLatest_t3519199716 *)L_6);
		((  void (*) (CombineLatest_t3519199716 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((CombineLatest_t3519199716 *)L_6, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		IL2CPP_LEAVE(0x41, FINALLY_003a);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_003a;
	}

FINALLY_003a:
	{ // begin finally (depth: 1)
		Il2CppObject * L_7 = V_0;
		Monitor_Exit_m2088237919(NULL /*static, unused*/, (Il2CppObject *)L_7, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(58)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(58)
	{
		IL2CPP_JUMP_TBL(0x41, IL_0041)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0041:
	{
		return;
	}
}
// System.Void UniRx.Operators.CombineLatestObservable`3/CombineLatest/RightObserver<System.Boolean,System.Boolean,System.Boolean>::OnError(System.Exception)
extern "C"  void RightObserver_OnError_m2466157942_gshared (RightObserver_t1984089744 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method)
{
	Il2CppObject * V_0 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		CombineLatest_t3519199716 * L_0 = (CombineLatest_t3519199716 *)__this->get_parent_0();
		NullCheck(L_0);
		Il2CppObject * L_1 = (Il2CppObject *)L_0->get_gate_3();
		V_0 = (Il2CppObject *)L_1;
		Il2CppObject * L_2 = V_0;
		Monitor_Enter_m476686225(NULL /*static, unused*/, (Il2CppObject *)L_2, /*hidden argument*/NULL);
	}

IL_0012:
	try
	{ // begin try (depth: 1)
		CombineLatest_t3519199716 * L_3 = (CombineLatest_t3519199716 *)__this->get_parent_0();
		Exception_t1967233988 * L_4 = ___error0;
		NullCheck((CombineLatest_t3519199716 *)L_3);
		VirtActionInvoker1< Exception_t1967233988 * >::Invoke(9 /* System.Void UniRx.Operators.CombineLatestObservable`3/CombineLatest<System.Boolean,System.Boolean,System.Boolean>::OnError(System.Exception) */, (CombineLatest_t3519199716 *)L_3, (Exception_t1967233988 *)L_4);
		IL2CPP_LEAVE(0x2A, FINALLY_0023);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0023;
	}

FINALLY_0023:
	{ // begin finally (depth: 1)
		Il2CppObject * L_5 = V_0;
		Monitor_Exit_m2088237919(NULL /*static, unused*/, (Il2CppObject *)L_5, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(35)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(35)
	{
		IL2CPP_JUMP_TBL(0x2A, IL_002a)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_002a:
	{
		return;
	}
}
// System.Void UniRx.Operators.CombineLatestObservable`3/CombineLatest/RightObserver<System.Boolean,System.Boolean,System.Boolean>::OnCompleted()
extern "C"  void RightObserver_OnCompleted_m840518089_gshared (RightObserver_t1984089744 * __this, const MethodInfo* method)
{
	Il2CppObject * V_0 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		CombineLatest_t3519199716 * L_0 = (CombineLatest_t3519199716 *)__this->get_parent_0();
		NullCheck(L_0);
		Il2CppObject * L_1 = (Il2CppObject *)L_0->get_gate_3();
		V_0 = (Il2CppObject *)L_1;
		Il2CppObject * L_2 = V_0;
		Monitor_Enter_m476686225(NULL /*static, unused*/, (Il2CppObject *)L_2, /*hidden argument*/NULL);
	}

IL_0012:
	try
	{ // begin try (depth: 1)
		{
			CombineLatest_t3519199716 * L_3 = (CombineLatest_t3519199716 *)__this->get_parent_0();
			NullCheck(L_3);
			L_3->set_rightCompleted_9((bool)1);
			CombineLatest_t3519199716 * L_4 = (CombineLatest_t3519199716 *)__this->get_parent_0();
			NullCheck(L_4);
			bool L_5 = (bool)L_4->get_leftCompleted_6();
			if (!L_5)
			{
				goto IL_0039;
			}
		}

IL_002e:
		{
			CombineLatest_t3519199716 * L_6 = (CombineLatest_t3519199716 *)__this->get_parent_0();
			NullCheck((CombineLatest_t3519199716 *)L_6);
			VirtActionInvoker0::Invoke(10 /* System.Void UniRx.Operators.CombineLatestObservable`3/CombineLatest<System.Boolean,System.Boolean,System.Boolean>::OnCompleted() */, (CombineLatest_t3519199716 *)L_6);
		}

IL_0039:
		{
			IL2CPP_LEAVE(0x45, FINALLY_003e);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_003e;
	}

FINALLY_003e:
	{ // begin finally (depth: 1)
		Il2CppObject * L_7 = V_0;
		Monitor_Exit_m2088237919(NULL /*static, unused*/, (Il2CppObject *)L_7, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(62)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(62)
	{
		IL2CPP_JUMP_TBL(0x45, IL_0045)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0045:
	{
		return;
	}
}
// System.Void UniRx.Operators.CombineLatestObservable`3/CombineLatest/RightObserver<System.Boolean,System.Boolean,System.Object>::.ctor(UniRx.Operators.CombineLatestObservable`3/CombineLatest<TLeft,TRight,TResult>)
extern "C"  void RightObserver__ctor_m466081504_gshared (RightObserver_t2610190823 * __this, CombineLatest_t4145300795 * ___parent0, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		CombineLatest_t4145300795 * L_0 = ___parent0;
		__this->set_parent_0(L_0);
		return;
	}
}
// System.Void UniRx.Operators.CombineLatestObservable`3/CombineLatest/RightObserver<System.Boolean,System.Boolean,System.Object>::OnNext(TRight)
extern "C"  void RightObserver_OnNext_m1921385308_gshared (RightObserver_t2610190823 * __this, bool ___value0, const MethodInfo* method)
{
	Il2CppObject * V_0 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		CombineLatest_t4145300795 * L_0 = (CombineLatest_t4145300795 *)__this->get_parent_0();
		NullCheck(L_0);
		Il2CppObject * L_1 = (Il2CppObject *)L_0->get_gate_3();
		V_0 = (Il2CppObject *)L_1;
		Il2CppObject * L_2 = V_0;
		Monitor_Enter_m476686225(NULL /*static, unused*/, (Il2CppObject *)L_2, /*hidden argument*/NULL);
	}

IL_0012:
	try
	{ // begin try (depth: 1)
		CombineLatest_t4145300795 * L_3 = (CombineLatest_t4145300795 *)__this->get_parent_0();
		NullCheck(L_3);
		L_3->set_rightStarted_8((bool)1);
		CombineLatest_t4145300795 * L_4 = (CombineLatest_t4145300795 *)__this->get_parent_0();
		bool L_5 = ___value0;
		NullCheck(L_4);
		L_4->set_rightValue_7(L_5);
		CombineLatest_t4145300795 * L_6 = (CombineLatest_t4145300795 *)__this->get_parent_0();
		NullCheck((CombineLatest_t4145300795 *)L_6);
		((  void (*) (CombineLatest_t4145300795 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((CombineLatest_t4145300795 *)L_6, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		IL2CPP_LEAVE(0x41, FINALLY_003a);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_003a;
	}

FINALLY_003a:
	{ // begin finally (depth: 1)
		Il2CppObject * L_7 = V_0;
		Monitor_Exit_m2088237919(NULL /*static, unused*/, (Il2CppObject *)L_7, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(58)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(58)
	{
		IL2CPP_JUMP_TBL(0x41, IL_0041)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0041:
	{
		return;
	}
}
// System.Void UniRx.Operators.CombineLatestObservable`3/CombineLatest/RightObserver<System.Boolean,System.Boolean,System.Object>::OnError(System.Exception)
extern "C"  void RightObserver_OnError_m4186505257_gshared (RightObserver_t2610190823 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method)
{
	Il2CppObject * V_0 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		CombineLatest_t4145300795 * L_0 = (CombineLatest_t4145300795 *)__this->get_parent_0();
		NullCheck(L_0);
		Il2CppObject * L_1 = (Il2CppObject *)L_0->get_gate_3();
		V_0 = (Il2CppObject *)L_1;
		Il2CppObject * L_2 = V_0;
		Monitor_Enter_m476686225(NULL /*static, unused*/, (Il2CppObject *)L_2, /*hidden argument*/NULL);
	}

IL_0012:
	try
	{ // begin try (depth: 1)
		CombineLatest_t4145300795 * L_3 = (CombineLatest_t4145300795 *)__this->get_parent_0();
		Exception_t1967233988 * L_4 = ___error0;
		NullCheck((CombineLatest_t4145300795 *)L_3);
		VirtActionInvoker1< Exception_t1967233988 * >::Invoke(9 /* System.Void UniRx.Operators.CombineLatestObservable`3/CombineLatest<System.Boolean,System.Boolean,System.Object>::OnError(System.Exception) */, (CombineLatest_t4145300795 *)L_3, (Exception_t1967233988 *)L_4);
		IL2CPP_LEAVE(0x2A, FINALLY_0023);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0023;
	}

FINALLY_0023:
	{ // begin finally (depth: 1)
		Il2CppObject * L_5 = V_0;
		Monitor_Exit_m2088237919(NULL /*static, unused*/, (Il2CppObject *)L_5, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(35)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(35)
	{
		IL2CPP_JUMP_TBL(0x2A, IL_002a)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_002a:
	{
		return;
	}
}
// System.Void UniRx.Operators.CombineLatestObservable`3/CombineLatest/RightObserver<System.Boolean,System.Boolean,System.Object>::OnCompleted()
extern "C"  void RightObserver_OnCompleted_m478144764_gshared (RightObserver_t2610190823 * __this, const MethodInfo* method)
{
	Il2CppObject * V_0 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		CombineLatest_t4145300795 * L_0 = (CombineLatest_t4145300795 *)__this->get_parent_0();
		NullCheck(L_0);
		Il2CppObject * L_1 = (Il2CppObject *)L_0->get_gate_3();
		V_0 = (Il2CppObject *)L_1;
		Il2CppObject * L_2 = V_0;
		Monitor_Enter_m476686225(NULL /*static, unused*/, (Il2CppObject *)L_2, /*hidden argument*/NULL);
	}

IL_0012:
	try
	{ // begin try (depth: 1)
		{
			CombineLatest_t4145300795 * L_3 = (CombineLatest_t4145300795 *)__this->get_parent_0();
			NullCheck(L_3);
			L_3->set_rightCompleted_9((bool)1);
			CombineLatest_t4145300795 * L_4 = (CombineLatest_t4145300795 *)__this->get_parent_0();
			NullCheck(L_4);
			bool L_5 = (bool)L_4->get_leftCompleted_6();
			if (!L_5)
			{
				goto IL_0039;
			}
		}

IL_002e:
		{
			CombineLatest_t4145300795 * L_6 = (CombineLatest_t4145300795 *)__this->get_parent_0();
			NullCheck((CombineLatest_t4145300795 *)L_6);
			VirtActionInvoker0::Invoke(10 /* System.Void UniRx.Operators.CombineLatestObservable`3/CombineLatest<System.Boolean,System.Boolean,System.Object>::OnCompleted() */, (CombineLatest_t4145300795 *)L_6);
		}

IL_0039:
		{
			IL2CPP_LEAVE(0x45, FINALLY_003e);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_003e;
	}

FINALLY_003e:
	{ // begin finally (depth: 1)
		Il2CppObject * L_7 = V_0;
		Monitor_Exit_m2088237919(NULL /*static, unused*/, (Il2CppObject *)L_7, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(62)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(62)
	{
		IL2CPP_JUMP_TBL(0x45, IL_0045)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0045:
	{
		return;
	}
}
// System.Void UniRx.Operators.CombineLatestObservable`3/CombineLatest/RightObserver<System.Boolean,System.Int32,System.Object>::.ctor(UniRx.Operators.CombineLatestObservable`3/CombineLatest<TLeft,TRight,TResult>)
extern "C"  void RightObserver__ctor_m1518668666_gshared (RightObserver_t615246633 * __this, CombineLatest_t2150356605 * ___parent0, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		CombineLatest_t2150356605 * L_0 = ___parent0;
		__this->set_parent_0(L_0);
		return;
	}
}
// System.Void UniRx.Operators.CombineLatestObservable`3/CombineLatest/RightObserver<System.Boolean,System.Int32,System.Object>::OnNext(TRight)
extern "C"  void RightObserver_OnNext_m76743542_gshared (RightObserver_t615246633 * __this, int32_t ___value0, const MethodInfo* method)
{
	Il2CppObject * V_0 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		CombineLatest_t2150356605 * L_0 = (CombineLatest_t2150356605 *)__this->get_parent_0();
		NullCheck(L_0);
		Il2CppObject * L_1 = (Il2CppObject *)L_0->get_gate_3();
		V_0 = (Il2CppObject *)L_1;
		Il2CppObject * L_2 = V_0;
		Monitor_Enter_m476686225(NULL /*static, unused*/, (Il2CppObject *)L_2, /*hidden argument*/NULL);
	}

IL_0012:
	try
	{ // begin try (depth: 1)
		CombineLatest_t2150356605 * L_3 = (CombineLatest_t2150356605 *)__this->get_parent_0();
		NullCheck(L_3);
		L_3->set_rightStarted_8((bool)1);
		CombineLatest_t2150356605 * L_4 = (CombineLatest_t2150356605 *)__this->get_parent_0();
		int32_t L_5 = ___value0;
		NullCheck(L_4);
		L_4->set_rightValue_7(L_5);
		CombineLatest_t2150356605 * L_6 = (CombineLatest_t2150356605 *)__this->get_parent_0();
		NullCheck((CombineLatest_t2150356605 *)L_6);
		((  void (*) (CombineLatest_t2150356605 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((CombineLatest_t2150356605 *)L_6, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		IL2CPP_LEAVE(0x41, FINALLY_003a);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_003a;
	}

FINALLY_003a:
	{ // begin finally (depth: 1)
		Il2CppObject * L_7 = V_0;
		Monitor_Exit_m2088237919(NULL /*static, unused*/, (Il2CppObject *)L_7, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(58)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(58)
	{
		IL2CPP_JUMP_TBL(0x41, IL_0041)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0041:
	{
		return;
	}
}
// System.Void UniRx.Operators.CombineLatestObservable`3/CombineLatest/RightObserver<System.Boolean,System.Int32,System.Object>::OnError(System.Exception)
extern "C"  void RightObserver_OnError_m1432800207_gshared (RightObserver_t615246633 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method)
{
	Il2CppObject * V_0 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		CombineLatest_t2150356605 * L_0 = (CombineLatest_t2150356605 *)__this->get_parent_0();
		NullCheck(L_0);
		Il2CppObject * L_1 = (Il2CppObject *)L_0->get_gate_3();
		V_0 = (Il2CppObject *)L_1;
		Il2CppObject * L_2 = V_0;
		Monitor_Enter_m476686225(NULL /*static, unused*/, (Il2CppObject *)L_2, /*hidden argument*/NULL);
	}

IL_0012:
	try
	{ // begin try (depth: 1)
		CombineLatest_t2150356605 * L_3 = (CombineLatest_t2150356605 *)__this->get_parent_0();
		Exception_t1967233988 * L_4 = ___error0;
		NullCheck((CombineLatest_t2150356605 *)L_3);
		VirtActionInvoker1< Exception_t1967233988 * >::Invoke(9 /* System.Void UniRx.Operators.CombineLatestObservable`3/CombineLatest<System.Boolean,System.Int32,System.Object>::OnError(System.Exception) */, (CombineLatest_t2150356605 *)L_3, (Exception_t1967233988 *)L_4);
		IL2CPP_LEAVE(0x2A, FINALLY_0023);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0023;
	}

FINALLY_0023:
	{ // begin finally (depth: 1)
		Il2CppObject * L_5 = V_0;
		Monitor_Exit_m2088237919(NULL /*static, unused*/, (Il2CppObject *)L_5, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(35)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(35)
	{
		IL2CPP_JUMP_TBL(0x2A, IL_002a)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_002a:
	{
		return;
	}
}
// System.Void UniRx.Operators.CombineLatestObservable`3/CombineLatest/RightObserver<System.Boolean,System.Int32,System.Object>::OnCompleted()
extern "C"  void RightObserver_OnCompleted_m3743776162_gshared (RightObserver_t615246633 * __this, const MethodInfo* method)
{
	Il2CppObject * V_0 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		CombineLatest_t2150356605 * L_0 = (CombineLatest_t2150356605 *)__this->get_parent_0();
		NullCheck(L_0);
		Il2CppObject * L_1 = (Il2CppObject *)L_0->get_gate_3();
		V_0 = (Il2CppObject *)L_1;
		Il2CppObject * L_2 = V_0;
		Monitor_Enter_m476686225(NULL /*static, unused*/, (Il2CppObject *)L_2, /*hidden argument*/NULL);
	}

IL_0012:
	try
	{ // begin try (depth: 1)
		{
			CombineLatest_t2150356605 * L_3 = (CombineLatest_t2150356605 *)__this->get_parent_0();
			NullCheck(L_3);
			L_3->set_rightCompleted_9((bool)1);
			CombineLatest_t2150356605 * L_4 = (CombineLatest_t2150356605 *)__this->get_parent_0();
			NullCheck(L_4);
			bool L_5 = (bool)L_4->get_leftCompleted_6();
			if (!L_5)
			{
				goto IL_0039;
			}
		}

IL_002e:
		{
			CombineLatest_t2150356605 * L_6 = (CombineLatest_t2150356605 *)__this->get_parent_0();
			NullCheck((CombineLatest_t2150356605 *)L_6);
			VirtActionInvoker0::Invoke(10 /* System.Void UniRx.Operators.CombineLatestObservable`3/CombineLatest<System.Boolean,System.Int32,System.Object>::OnCompleted() */, (CombineLatest_t2150356605 *)L_6);
		}

IL_0039:
		{
			IL2CPP_LEAVE(0x45, FINALLY_003e);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_003e;
	}

FINALLY_003e:
	{ // begin finally (depth: 1)
		Il2CppObject * L_7 = V_0;
		Monitor_Exit_m2088237919(NULL /*static, unused*/, (Il2CppObject *)L_7, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(62)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(62)
	{
		IL2CPP_JUMP_TBL(0x45, IL_0045)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0045:
	{
		return;
	}
}
// System.Void UniRx.Operators.CombineLatestObservable`3/CombineLatest/RightObserver<System.Object,System.Object,System.Object>::.ctor(UniRx.Operators.CombineLatestObservable`3/CombineLatest<TLeft,TRight,TResult>)
extern "C"  void RightObserver__ctor_m187976476_gshared (RightObserver_t812748179 * __this, CombineLatest_t2347858151 * ___parent0, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		CombineLatest_t2347858151 * L_0 = ___parent0;
		__this->set_parent_0(L_0);
		return;
	}
}
// System.Void UniRx.Operators.CombineLatestObservable`3/CombineLatest/RightObserver<System.Object,System.Object,System.Object>::OnNext(TRight)
extern "C"  void RightObserver_OnNext_m1369781400_gshared (RightObserver_t812748179 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	Il2CppObject * V_0 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		CombineLatest_t2347858151 * L_0 = (CombineLatest_t2347858151 *)__this->get_parent_0();
		NullCheck(L_0);
		Il2CppObject * L_1 = (Il2CppObject *)L_0->get_gate_3();
		V_0 = (Il2CppObject *)L_1;
		Il2CppObject * L_2 = V_0;
		Monitor_Enter_m476686225(NULL /*static, unused*/, (Il2CppObject *)L_2, /*hidden argument*/NULL);
	}

IL_0012:
	try
	{ // begin try (depth: 1)
		CombineLatest_t2347858151 * L_3 = (CombineLatest_t2347858151 *)__this->get_parent_0();
		NullCheck(L_3);
		L_3->set_rightStarted_8((bool)1);
		CombineLatest_t2347858151 * L_4 = (CombineLatest_t2347858151 *)__this->get_parent_0();
		Il2CppObject * L_5 = ___value0;
		NullCheck(L_4);
		L_4->set_rightValue_7(L_5);
		CombineLatest_t2347858151 * L_6 = (CombineLatest_t2347858151 *)__this->get_parent_0();
		NullCheck((CombineLatest_t2347858151 *)L_6);
		((  void (*) (CombineLatest_t2347858151 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((CombineLatest_t2347858151 *)L_6, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		IL2CPP_LEAVE(0x41, FINALLY_003a);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_003a;
	}

FINALLY_003a:
	{ // begin finally (depth: 1)
		Il2CppObject * L_7 = V_0;
		Monitor_Exit_m2088237919(NULL /*static, unused*/, (Il2CppObject *)L_7, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(58)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(58)
	{
		IL2CPP_JUMP_TBL(0x41, IL_0041)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0041:
	{
		return;
	}
}
// System.Void UniRx.Operators.CombineLatestObservable`3/CombineLatest/RightObserver<System.Object,System.Object,System.Object>::OnError(System.Exception)
extern "C"  void RightObserver_OnError_m2404643181_gshared (RightObserver_t812748179 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method)
{
	Il2CppObject * V_0 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		CombineLatest_t2347858151 * L_0 = (CombineLatest_t2347858151 *)__this->get_parent_0();
		NullCheck(L_0);
		Il2CppObject * L_1 = (Il2CppObject *)L_0->get_gate_3();
		V_0 = (Il2CppObject *)L_1;
		Il2CppObject * L_2 = V_0;
		Monitor_Enter_m476686225(NULL /*static, unused*/, (Il2CppObject *)L_2, /*hidden argument*/NULL);
	}

IL_0012:
	try
	{ // begin try (depth: 1)
		CombineLatest_t2347858151 * L_3 = (CombineLatest_t2347858151 *)__this->get_parent_0();
		Exception_t1967233988 * L_4 = ___error0;
		NullCheck((CombineLatest_t2347858151 *)L_3);
		VirtActionInvoker1< Exception_t1967233988 * >::Invoke(9 /* System.Void UniRx.Operators.CombineLatestObservable`3/CombineLatest<System.Object,System.Object,System.Object>::OnError(System.Exception) */, (CombineLatest_t2347858151 *)L_3, (Exception_t1967233988 *)L_4);
		IL2CPP_LEAVE(0x2A, FINALLY_0023);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0023;
	}

FINALLY_0023:
	{ // begin finally (depth: 1)
		Il2CppObject * L_5 = V_0;
		Monitor_Exit_m2088237919(NULL /*static, unused*/, (Il2CppObject *)L_5, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(35)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(35)
	{
		IL2CPP_JUMP_TBL(0x2A, IL_002a)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_002a:
	{
		return;
	}
}
// System.Void UniRx.Operators.CombineLatestObservable`3/CombineLatest/RightObserver<System.Object,System.Object,System.Object>::OnCompleted()
extern "C"  void RightObserver_OnCompleted_m3092750400_gshared (RightObserver_t812748179 * __this, const MethodInfo* method)
{
	Il2CppObject * V_0 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		CombineLatest_t2347858151 * L_0 = (CombineLatest_t2347858151 *)__this->get_parent_0();
		NullCheck(L_0);
		Il2CppObject * L_1 = (Il2CppObject *)L_0->get_gate_3();
		V_0 = (Il2CppObject *)L_1;
		Il2CppObject * L_2 = V_0;
		Monitor_Enter_m476686225(NULL /*static, unused*/, (Il2CppObject *)L_2, /*hidden argument*/NULL);
	}

IL_0012:
	try
	{ // begin try (depth: 1)
		{
			CombineLatest_t2347858151 * L_3 = (CombineLatest_t2347858151 *)__this->get_parent_0();
			NullCheck(L_3);
			L_3->set_rightCompleted_9((bool)1);
			CombineLatest_t2347858151 * L_4 = (CombineLatest_t2347858151 *)__this->get_parent_0();
			NullCheck(L_4);
			bool L_5 = (bool)L_4->get_leftCompleted_6();
			if (!L_5)
			{
				goto IL_0039;
			}
		}

IL_002e:
		{
			CombineLatest_t2347858151 * L_6 = (CombineLatest_t2347858151 *)__this->get_parent_0();
			NullCheck((CombineLatest_t2347858151 *)L_6);
			VirtActionInvoker0::Invoke(10 /* System.Void UniRx.Operators.CombineLatestObservable`3/CombineLatest<System.Object,System.Object,System.Object>::OnCompleted() */, (CombineLatest_t2347858151 *)L_6);
		}

IL_0039:
		{
			IL2CPP_LEAVE(0x45, FINALLY_003e);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_003e;
	}

FINALLY_003e:
	{ // begin finally (depth: 1)
		Il2CppObject * L_7 = V_0;
		Monitor_Exit_m2088237919(NULL /*static, unused*/, (Il2CppObject *)L_7, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(62)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(62)
	{
		IL2CPP_JUMP_TBL(0x45, IL_0045)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0045:
	{
		return;
	}
}
// System.Void UniRx.Operators.CombineLatestObservable`3/CombineLatest<System.Boolean,System.Boolean,System.Boolean>::.ctor(UniRx.Operators.CombineLatestObservable`3<TLeft,TRight,TResult>,UniRx.IObserver`1<TResult>,System.IDisposable)
extern Il2CppClass* Il2CppObject_il2cpp_TypeInfo_var;
extern Il2CppClass* Boolean_t211005341_il2cpp_TypeInfo_var;
extern const uint32_t CombineLatest__ctor_m1156244398_MetadataUsageId;
extern "C"  void CombineLatest__ctor_m1156244398_gshared (CombineLatest_t3519199716 * __this, CombineLatestObservable_3_t3912295671 * ___parent0, Il2CppObject* ___observer1, Il2CppObject * ___cancel2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (CombineLatest__ctor_m1156244398_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	bool V_0 = false;
	bool V_1 = false;
	{
		Il2CppObject * L_0 = (Il2CppObject *)il2cpp_codegen_object_new(Il2CppObject_il2cpp_TypeInfo_var);
		Object__ctor_m1772956182(L_0, /*hidden argument*/NULL);
		__this->set_gate_3(L_0);
		Initobj (Boolean_t211005341_il2cpp_TypeInfo_var, (&V_0));
		bool L_1 = V_0;
		__this->set_leftValue_4(L_1);
		Initobj (Boolean_t211005341_il2cpp_TypeInfo_var, (&V_1));
		bool L_2 = V_1;
		__this->set_rightValue_7(L_2);
		Il2CppObject* L_3 = ___observer1;
		Il2CppObject * L_4 = ___cancel2;
		NullCheck((OperatorObserverBase_2_t60103313 *)__this);
		((  void (*) (OperatorObserverBase_2_t60103313 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((OperatorObserverBase_2_t60103313 *)__this, (Il2CppObject*)L_3, (Il2CppObject *)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		CombineLatestObservable_3_t3912295671 * L_5 = ___parent0;
		__this->set_parent_2(L_5);
		return;
	}
}
// System.IDisposable UniRx.Operators.CombineLatestObservable`3/CombineLatest<System.Boolean,System.Boolean,System.Boolean>::Run()
extern "C"  Il2CppObject * CombineLatest_Run_m3023913830_gshared (CombineLatest_t3519199716 * __this, const MethodInfo* method)
{
	Il2CppObject * V_0 = NULL;
	Il2CppObject * V_1 = NULL;
	{
		CombineLatestObservable_3_t3912295671 * L_0 = (CombineLatestObservable_3_t3912295671 *)__this->get_parent_2();
		NullCheck(L_0);
		Il2CppObject* L_1 = (Il2CppObject*)L_0->get_left_1();
		LeftObserver_t3421766155 * L_2 = (LeftObserver_t3421766155 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		((  void (*) (LeftObserver_t3421766155 *, CombineLatest_t3519199716 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)(L_2, (CombineLatest_t3519199716 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		NullCheck((Il2CppObject*)L_1);
		Il2CppObject * L_3 = InterfaceFuncInvoker1< Il2CppObject *, Il2CppObject* >::Invoke(0 /* System.IDisposable UniRx.IObservable`1<System.Boolean>::Subscribe(UniRx.IObserver`1<T>) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4), (Il2CppObject*)L_1, (Il2CppObject*)L_2);
		V_0 = (Il2CppObject *)L_3;
		CombineLatestObservable_3_t3912295671 * L_4 = (CombineLatestObservable_3_t3912295671 *)__this->get_parent_2();
		NullCheck(L_4);
		Il2CppObject* L_5 = (Il2CppObject*)L_4->get_right_2();
		RightObserver_t1984089744 * L_6 = (RightObserver_t1984089744 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		((  void (*) (RightObserver_t1984089744 *, CombineLatest_t3519199716 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6)->method)(L_6, (CombineLatest_t3519199716 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		NullCheck((Il2CppObject*)L_5);
		Il2CppObject * L_7 = InterfaceFuncInvoker1< Il2CppObject *, Il2CppObject* >::Invoke(0 /* System.IDisposable UniRx.IObservable`1<System.Boolean>::Subscribe(UniRx.IObserver`1<T>) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7), (Il2CppObject*)L_5, (Il2CppObject*)L_6);
		V_1 = (Il2CppObject *)L_7;
		Il2CppObject * L_8 = V_0;
		Il2CppObject * L_9 = V_1;
		Il2CppObject * L_10 = StableCompositeDisposable_Create_m3764788535(NULL /*static, unused*/, (Il2CppObject *)L_8, (Il2CppObject *)L_9, /*hidden argument*/NULL);
		return L_10;
	}
}
// System.Void UniRx.Operators.CombineLatestObservable`3/CombineLatest<System.Boolean,System.Boolean,System.Boolean>::Publish()
extern Il2CppClass* Exception_t1967233988_il2cpp_TypeInfo_var;
extern const uint32_t CombineLatest_Publish_m4078538415_MetadataUsageId;
extern "C"  void CombineLatest_Publish_m4078538415_gshared (CombineLatest_t3519199716 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (CombineLatest_Publish_m4078538415_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	bool V_0 = false;
	Exception_t1967233988 * V_1 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		bool L_0 = (bool)__this->get_leftCompleted_6();
		if (!L_0)
		{
			goto IL_0016;
		}
	}
	{
		bool L_1 = (bool)__this->get_leftStarted_5();
		if (!L_1)
		{
			goto IL_002c;
		}
	}

IL_0016:
	{
		bool L_2 = (bool)__this->get_rightCompleted_9();
		if (!L_2)
		{
			goto IL_0046;
		}
	}
	{
		bool L_3 = (bool)__this->get_rightStarted_8();
		if (L_3)
		{
			goto IL_0046;
		}
	}

IL_002c:
	try
	{ // begin try (depth: 1)
		Il2CppObject* L_4 = (Il2CppObject*)((OperatorObserverBase_2_t60103313 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		NullCheck((Il2CppObject*)L_4);
		InterfaceActionInvoker0::Invoke(0 /* System.Void UniRx.IObserver`1<System.Boolean>::OnCompleted() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8), (Il2CppObject*)L_4);
		IL2CPP_LEAVE(0x45, FINALLY_003e);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_003e;
	}

FINALLY_003e:
	{ // begin finally (depth: 1)
		NullCheck((OperatorObserverBase_2_t60103313 *)__this);
		VirtActionInvoker0::Invoke(4 /* System.Void UniRx.Operators.OperatorObserverBase`2<System.Boolean,System.Boolean>::Dispose() */, (OperatorObserverBase_2_t60103313 *)__this);
		IL2CPP_END_FINALLY(62)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(62)
	{
		IL2CPP_JUMP_TBL(0x45, IL_0045)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0045:
	{
		return;
	}

IL_0046:
	{
		bool L_5 = (bool)__this->get_leftStarted_5();
		if (!L_5)
		{
			goto IL_005c;
		}
	}
	{
		bool L_6 = (bool)__this->get_rightStarted_8();
		if (L_6)
		{
			goto IL_005d;
		}
	}

IL_005c:
	{
		return;
	}

IL_005d:
	try
	{ // begin try (depth: 1)
		CombineLatestObservable_3_t3912295671 * L_7 = (CombineLatestObservable_3_t3912295671 *)__this->get_parent_2();
		NullCheck(L_7);
		Func_3_t3063550794 * L_8 = (Func_3_t3063550794 *)L_7->get_selector_3();
		bool L_9 = (bool)__this->get_leftValue_4();
		bool L_10 = (bool)__this->get_rightValue_7();
		NullCheck((Func_3_t3063550794 *)L_8);
		bool L_11 = ((  bool (*) (Func_3_t3063550794 *, bool, bool, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10)->method)((Func_3_t3063550794 *)L_8, (bool)L_9, (bool)L_10, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10));
		V_0 = (bool)L_11;
		goto IL_00a4;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1967233988 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Exception_t1967233988_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_007f;
		throw e;
	}

CATCH_007f:
	{ // begin catch(System.Exception)
		{
			V_1 = (Exception_t1967233988 *)((Exception_t1967233988 *)__exception_local);
		}

IL_0080:
		try
		{ // begin try (depth: 2)
			Il2CppObject* L_12 = (Il2CppObject*)((OperatorObserverBase_2_t60103313 *)__this)->get_observer_0();
			il2cpp_codegen_memory_barrier();
			Exception_t1967233988 * L_13 = V_1;
			NullCheck((Il2CppObject*)L_12);
			InterfaceActionInvoker1< Exception_t1967233988 * >::Invoke(1 /* System.Void UniRx.IObserver`1<System.Boolean>::OnError(System.Exception) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8), (Il2CppObject*)L_12, (Exception_t1967233988 *)L_13);
			IL2CPP_LEAVE(0x9A, FINALLY_0093);
		} // end try (depth: 2)
		catch(Il2CppExceptionWrapper& e)
		{
			__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
			goto FINALLY_0093;
		}

FINALLY_0093:
		{ // begin finally (depth: 2)
			NullCheck((OperatorObserverBase_2_t60103313 *)__this);
			VirtActionInvoker0::Invoke(4 /* System.Void UniRx.Operators.OperatorObserverBase`2<System.Boolean,System.Boolean>::Dispose() */, (OperatorObserverBase_2_t60103313 *)__this);
			IL2CPP_END_FINALLY(147)
		} // end finally (depth: 2)
		IL2CPP_CLEANUP(147)
		{
			IL2CPP_JUMP_TBL(0x9A, IL_009a)
			IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
		}

IL_009a:
		{
			goto IL_00ab;
		}

IL_009f:
		{
			; // IL_009f: leave IL_00a4
		}
	} // end catch (depth: 1)

IL_00a4:
	{
		bool L_14 = V_0;
		NullCheck((CombineLatest_t3519199716 *)__this);
		VirtActionInvoker1< bool >::Invoke(8 /* System.Void UniRx.Operators.CombineLatestObservable`3/CombineLatest<System.Boolean,System.Boolean,System.Boolean>::OnNext(TResult) */, (CombineLatest_t3519199716 *)__this, (bool)L_14);
	}

IL_00ab:
	{
		return;
	}
}
// System.Void UniRx.Operators.CombineLatestObservable`3/CombineLatest<System.Boolean,System.Boolean,System.Boolean>::OnNext(TResult)
extern "C"  void CombineLatest_OnNext_m2661404877_gshared (CombineLatest_t3519199716 * __this, bool ___value0, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = (Il2CppObject*)((OperatorObserverBase_2_t60103313 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		bool L_1 = ___value0;
		NullCheck((Il2CppObject*)L_0);
		InterfaceActionInvoker1< bool >::Invoke(2 /* System.Void UniRx.IObserver`1<System.Boolean>::OnNext(T) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8), (Il2CppObject*)L_0, (bool)L_1);
		return;
	}
}
// System.Void UniRx.Operators.CombineLatestObservable`3/CombineLatest<System.Boolean,System.Boolean,System.Boolean>::OnError(System.Exception)
extern "C"  void CombineLatest_OnError_m183486777_gshared (CombineLatest_t3519199716 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method)
{
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		Il2CppObject* L_0 = (Il2CppObject*)((OperatorObserverBase_2_t60103313 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		Exception_t1967233988 * L_1 = ___error0;
		NullCheck((Il2CppObject*)L_0);
		InterfaceActionInvoker1< Exception_t1967233988 * >::Invoke(1 /* System.Void UniRx.IObserver`1<System.Boolean>::OnError(System.Exception) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8), (Il2CppObject*)L_0, (Exception_t1967233988 *)L_1);
		IL2CPP_LEAVE(0x1A, FINALLY_0013);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0013;
	}

FINALLY_0013:
	{ // begin finally (depth: 1)
		NullCheck((OperatorObserverBase_2_t60103313 *)__this);
		VirtActionInvoker0::Invoke(4 /* System.Void UniRx.Operators.OperatorObserverBase`2<System.Boolean,System.Boolean>::Dispose() */, (OperatorObserverBase_2_t60103313 *)__this);
		IL2CPP_END_FINALLY(19)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(19)
	{
		IL2CPP_JUMP_TBL(0x1A, IL_001a)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_001a:
	{
		return;
	}
}
// System.Void UniRx.Operators.CombineLatestObservable`3/CombineLatest<System.Boolean,System.Boolean,System.Boolean>::OnCompleted()
extern "C"  void CombineLatest_OnCompleted_m2743061004_gshared (CombineLatest_t3519199716 * __this, const MethodInfo* method)
{
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		Il2CppObject* L_0 = (Il2CppObject*)((OperatorObserverBase_2_t60103313 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		NullCheck((Il2CppObject*)L_0);
		InterfaceActionInvoker0::Invoke(0 /* System.Void UniRx.IObserver`1<System.Boolean>::OnCompleted() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8), (Il2CppObject*)L_0);
		IL2CPP_LEAVE(0x19, FINALLY_0012);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0012;
	}

FINALLY_0012:
	{ // begin finally (depth: 1)
		NullCheck((OperatorObserverBase_2_t60103313 *)__this);
		VirtActionInvoker0::Invoke(4 /* System.Void UniRx.Operators.OperatorObserverBase`2<System.Boolean,System.Boolean>::Dispose() */, (OperatorObserverBase_2_t60103313 *)__this);
		IL2CPP_END_FINALLY(18)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(18)
	{
		IL2CPP_JUMP_TBL(0x19, IL_0019)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0019:
	{
		return;
	}
}
// System.Void UniRx.Operators.CombineLatestObservable`3/CombineLatest<System.Boolean,System.Boolean,System.Object>::.ctor(UniRx.Operators.CombineLatestObservable`3<TLeft,TRight,TResult>,UniRx.IObserver`1<TResult>,System.IDisposable)
extern Il2CppClass* Il2CppObject_il2cpp_TypeInfo_var;
extern Il2CppClass* Boolean_t211005341_il2cpp_TypeInfo_var;
extern const uint32_t CombineLatest__ctor_m2104227009_MetadataUsageId;
extern "C"  void CombineLatest__ctor_m2104227009_gshared (CombineLatest_t4145300795 * __this, CombineLatestObservable_3_t243429454 * ___parent0, Il2CppObject* ___observer1, Il2CppObject * ___cancel2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (CombineLatest__ctor_m2104227009_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	bool V_0 = false;
	bool V_1 = false;
	{
		Il2CppObject * L_0 = (Il2CppObject *)il2cpp_codegen_object_new(Il2CppObject_il2cpp_TypeInfo_var);
		Object__ctor_m1772956182(L_0, /*hidden argument*/NULL);
		__this->set_gate_3(L_0);
		Initobj (Boolean_t211005341_il2cpp_TypeInfo_var, (&V_0));
		bool L_1 = V_0;
		__this->set_leftValue_4(L_1);
		Initobj (Boolean_t211005341_il2cpp_TypeInfo_var, (&V_1));
		bool L_2 = V_1;
		__this->set_rightValue_7(L_2);
		Il2CppObject* L_3 = ___observer1;
		Il2CppObject * L_4 = ___cancel2;
		NullCheck((OperatorObserverBase_2_t1187768149 *)__this);
		((  void (*) (OperatorObserverBase_2_t1187768149 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((OperatorObserverBase_2_t1187768149 *)__this, (Il2CppObject*)L_3, (Il2CppObject *)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		CombineLatestObservable_3_t243429454 * L_5 = ___parent0;
		__this->set_parent_2(L_5);
		return;
	}
}
// System.IDisposable UniRx.Operators.CombineLatestObservable`3/CombineLatest<System.Boolean,System.Boolean,System.Object>::Run()
extern "C"  Il2CppObject * CombineLatest_Run_m781429213_gshared (CombineLatest_t4145300795 * __this, const MethodInfo* method)
{
	Il2CppObject * V_0 = NULL;
	Il2CppObject * V_1 = NULL;
	{
		CombineLatestObservable_3_t243429454 * L_0 = (CombineLatestObservable_3_t243429454 *)__this->get_parent_2();
		NullCheck(L_0);
		Il2CppObject* L_1 = (Il2CppObject*)L_0->get_left_1();
		LeftObserver_t4047867234 * L_2 = (LeftObserver_t4047867234 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		((  void (*) (LeftObserver_t4047867234 *, CombineLatest_t4145300795 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)(L_2, (CombineLatest_t4145300795 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		NullCheck((Il2CppObject*)L_1);
		Il2CppObject * L_3 = InterfaceFuncInvoker1< Il2CppObject *, Il2CppObject* >::Invoke(0 /* System.IDisposable UniRx.IObservable`1<System.Boolean>::Subscribe(UniRx.IObserver`1<T>) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4), (Il2CppObject*)L_1, (Il2CppObject*)L_2);
		V_0 = (Il2CppObject *)L_3;
		CombineLatestObservable_3_t243429454 * L_4 = (CombineLatestObservable_3_t243429454 *)__this->get_parent_2();
		NullCheck(L_4);
		Il2CppObject* L_5 = (Il2CppObject*)L_4->get_right_2();
		RightObserver_t2610190823 * L_6 = (RightObserver_t2610190823 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		((  void (*) (RightObserver_t2610190823 *, CombineLatest_t4145300795 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6)->method)(L_6, (CombineLatest_t4145300795 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		NullCheck((Il2CppObject*)L_5);
		Il2CppObject * L_7 = InterfaceFuncInvoker1< Il2CppObject *, Il2CppObject* >::Invoke(0 /* System.IDisposable UniRx.IObservable`1<System.Boolean>::Subscribe(UniRx.IObserver`1<T>) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7), (Il2CppObject*)L_5, (Il2CppObject*)L_6);
		V_1 = (Il2CppObject *)L_7;
		Il2CppObject * L_8 = V_0;
		Il2CppObject * L_9 = V_1;
		Il2CppObject * L_10 = StableCompositeDisposable_Create_m3764788535(NULL /*static, unused*/, (Il2CppObject *)L_8, (Il2CppObject *)L_9, /*hidden argument*/NULL);
		return L_10;
	}
}
// System.Void UniRx.Operators.CombineLatestObservable`3/CombineLatest<System.Boolean,System.Boolean,System.Object>::Publish()
extern Il2CppClass* Exception_t1967233988_il2cpp_TypeInfo_var;
extern const uint32_t CombineLatest_Publish_m505661308_MetadataUsageId;
extern "C"  void CombineLatest_Publish_m505661308_gshared (CombineLatest_t4145300795 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (CombineLatest_Publish_m505661308_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Il2CppObject * V_0 = NULL;
	Exception_t1967233988 * V_1 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		bool L_0 = (bool)__this->get_leftCompleted_6();
		if (!L_0)
		{
			goto IL_0016;
		}
	}
	{
		bool L_1 = (bool)__this->get_leftStarted_5();
		if (!L_1)
		{
			goto IL_002c;
		}
	}

IL_0016:
	{
		bool L_2 = (bool)__this->get_rightCompleted_9();
		if (!L_2)
		{
			goto IL_0046;
		}
	}
	{
		bool L_3 = (bool)__this->get_rightStarted_8();
		if (L_3)
		{
			goto IL_0046;
		}
	}

IL_002c:
	try
	{ // begin try (depth: 1)
		Il2CppObject* L_4 = (Il2CppObject*)((OperatorObserverBase_2_t1187768149 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		NullCheck((Il2CppObject*)L_4);
		InterfaceActionInvoker0::Invoke(0 /* System.Void UniRx.IObserver`1<System.Object>::OnCompleted() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8), (Il2CppObject*)L_4);
		IL2CPP_LEAVE(0x45, FINALLY_003e);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_003e;
	}

FINALLY_003e:
	{ // begin finally (depth: 1)
		NullCheck((OperatorObserverBase_2_t1187768149 *)__this);
		VirtActionInvoker0::Invoke(4 /* System.Void UniRx.Operators.OperatorObserverBase`2<System.Object,System.Object>::Dispose() */, (OperatorObserverBase_2_t1187768149 *)__this);
		IL2CPP_END_FINALLY(62)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(62)
	{
		IL2CPP_JUMP_TBL(0x45, IL_0045)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0045:
	{
		return;
	}

IL_0046:
	{
		bool L_5 = (bool)__this->get_leftStarted_5();
		if (!L_5)
		{
			goto IL_005c;
		}
	}
	{
		bool L_6 = (bool)__this->get_rightStarted_8();
		if (L_6)
		{
			goto IL_005d;
		}
	}

IL_005c:
	{
		return;
	}

IL_005d:
	try
	{ // begin try (depth: 1)
		CombineLatestObservable_3_t243429454 * L_7 = (CombineLatestObservable_3_t243429454 *)__this->get_parent_2();
		NullCheck(L_7);
		Func_3_t3689651873 * L_8 = (Func_3_t3689651873 *)L_7->get_selector_3();
		bool L_9 = (bool)__this->get_leftValue_4();
		bool L_10 = (bool)__this->get_rightValue_7();
		NullCheck((Func_3_t3689651873 *)L_8);
		Il2CppObject * L_11 = ((  Il2CppObject * (*) (Func_3_t3689651873 *, bool, bool, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10)->method)((Func_3_t3689651873 *)L_8, (bool)L_9, (bool)L_10, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10));
		V_0 = (Il2CppObject *)L_11;
		goto IL_00a4;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1967233988 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Exception_t1967233988_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_007f;
		throw e;
	}

CATCH_007f:
	{ // begin catch(System.Exception)
		{
			V_1 = (Exception_t1967233988 *)((Exception_t1967233988 *)__exception_local);
		}

IL_0080:
		try
		{ // begin try (depth: 2)
			Il2CppObject* L_12 = (Il2CppObject*)((OperatorObserverBase_2_t1187768149 *)__this)->get_observer_0();
			il2cpp_codegen_memory_barrier();
			Exception_t1967233988 * L_13 = V_1;
			NullCheck((Il2CppObject*)L_12);
			InterfaceActionInvoker1< Exception_t1967233988 * >::Invoke(1 /* System.Void UniRx.IObserver`1<System.Object>::OnError(System.Exception) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8), (Il2CppObject*)L_12, (Exception_t1967233988 *)L_13);
			IL2CPP_LEAVE(0x9A, FINALLY_0093);
		} // end try (depth: 2)
		catch(Il2CppExceptionWrapper& e)
		{
			__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
			goto FINALLY_0093;
		}

FINALLY_0093:
		{ // begin finally (depth: 2)
			NullCheck((OperatorObserverBase_2_t1187768149 *)__this);
			VirtActionInvoker0::Invoke(4 /* System.Void UniRx.Operators.OperatorObserverBase`2<System.Object,System.Object>::Dispose() */, (OperatorObserverBase_2_t1187768149 *)__this);
			IL2CPP_END_FINALLY(147)
		} // end finally (depth: 2)
		IL2CPP_CLEANUP(147)
		{
			IL2CPP_JUMP_TBL(0x9A, IL_009a)
			IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
		}

IL_009a:
		{
			goto IL_00ab;
		}

IL_009f:
		{
			; // IL_009f: leave IL_00a4
		}
	} // end catch (depth: 1)

IL_00a4:
	{
		Il2CppObject * L_14 = V_0;
		NullCheck((CombineLatest_t4145300795 *)__this);
		VirtActionInvoker1< Il2CppObject * >::Invoke(8 /* System.Void UniRx.Operators.CombineLatestObservable`3/CombineLatest<System.Boolean,System.Boolean,System.Object>::OnNext(TResult) */, (CombineLatest_t4145300795 *)__this, (Il2CppObject *)L_14);
	}

IL_00ab:
	{
		return;
	}
}
// System.Void UniRx.Operators.CombineLatestObservable`3/CombineLatest<System.Boolean,System.Boolean,System.Object>::OnNext(TResult)
extern "C"  void CombineLatest_OnNext_m2474605530_gshared (CombineLatest_t4145300795 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = (Il2CppObject*)((OperatorObserverBase_2_t1187768149 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		Il2CppObject * L_1 = ___value0;
		NullCheck((Il2CppObject*)L_0);
		InterfaceActionInvoker1< Il2CppObject * >::Invoke(2 /* System.Void UniRx.IObserver`1<System.Object>::OnNext(T) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8), (Il2CppObject*)L_0, (Il2CppObject *)L_1);
		return;
	}
}
// System.Void UniRx.Operators.CombineLatestObservable`3/CombineLatest<System.Boolean,System.Boolean,System.Object>::OnError(System.Exception)
extern "C"  void CombineLatest_OnError_m2727397382_gshared (CombineLatest_t4145300795 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method)
{
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		Il2CppObject* L_0 = (Il2CppObject*)((OperatorObserverBase_2_t1187768149 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		Exception_t1967233988 * L_1 = ___error0;
		NullCheck((Il2CppObject*)L_0);
		InterfaceActionInvoker1< Exception_t1967233988 * >::Invoke(1 /* System.Void UniRx.IObserver`1<System.Object>::OnError(System.Exception) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8), (Il2CppObject*)L_0, (Exception_t1967233988 *)L_1);
		IL2CPP_LEAVE(0x1A, FINALLY_0013);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0013;
	}

FINALLY_0013:
	{ // begin finally (depth: 1)
		NullCheck((OperatorObserverBase_2_t1187768149 *)__this);
		VirtActionInvoker0::Invoke(4 /* System.Void UniRx.Operators.OperatorObserverBase`2<System.Object,System.Object>::Dispose() */, (OperatorObserverBase_2_t1187768149 *)__this);
		IL2CPP_END_FINALLY(19)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(19)
	{
		IL2CPP_JUMP_TBL(0x1A, IL_001a)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_001a:
	{
		return;
	}
}
// System.Void UniRx.Operators.CombineLatestObservable`3/CombineLatest<System.Boolean,System.Boolean,System.Object>::OnCompleted()
extern "C"  void CombineLatest_OnCompleted_m1509348441_gshared (CombineLatest_t4145300795 * __this, const MethodInfo* method)
{
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		Il2CppObject* L_0 = (Il2CppObject*)((OperatorObserverBase_2_t1187768149 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		NullCheck((Il2CppObject*)L_0);
		InterfaceActionInvoker0::Invoke(0 /* System.Void UniRx.IObserver`1<System.Object>::OnCompleted() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8), (Il2CppObject*)L_0);
		IL2CPP_LEAVE(0x19, FINALLY_0012);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0012;
	}

FINALLY_0012:
	{ // begin finally (depth: 1)
		NullCheck((OperatorObserverBase_2_t1187768149 *)__this);
		VirtActionInvoker0::Invoke(4 /* System.Void UniRx.Operators.OperatorObserverBase`2<System.Object,System.Object>::Dispose() */, (OperatorObserverBase_2_t1187768149 *)__this);
		IL2CPP_END_FINALLY(18)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(18)
	{
		IL2CPP_JUMP_TBL(0x19, IL_0019)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0019:
	{
		return;
	}
}
// System.Void UniRx.Operators.CombineLatestObservable`3/CombineLatest<System.Boolean,System.Int32,System.Object>::.ctor(UniRx.Operators.CombineLatestObservable`3<TLeft,TRight,TResult>,UniRx.IObserver`1<TResult>,System.IDisposable)
extern Il2CppClass* Il2CppObject_il2cpp_TypeInfo_var;
extern Il2CppClass* Boolean_t211005341_il2cpp_TypeInfo_var;
extern Il2CppClass* Int32_t2847414787_il2cpp_TypeInfo_var;
extern const uint32_t CombineLatest__ctor_m669378075_MetadataUsageId;
extern "C"  void CombineLatest__ctor_m669378075_gshared (CombineLatest_t2150356605 * __this, CombineLatestObservable_3_t2543452560 * ___parent0, Il2CppObject* ___observer1, Il2CppObject * ___cancel2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (CombineLatest__ctor_m669378075_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	bool V_0 = false;
	int32_t V_1 = 0;
	{
		Il2CppObject * L_0 = (Il2CppObject *)il2cpp_codegen_object_new(Il2CppObject_il2cpp_TypeInfo_var);
		Object__ctor_m1772956182(L_0, /*hidden argument*/NULL);
		__this->set_gate_3(L_0);
		Initobj (Boolean_t211005341_il2cpp_TypeInfo_var, (&V_0));
		bool L_1 = V_0;
		__this->set_leftValue_4(L_1);
		Initobj (Int32_t2847414787_il2cpp_TypeInfo_var, (&V_1));
		int32_t L_2 = V_1;
		__this->set_rightValue_7(L_2);
		Il2CppObject* L_3 = ___observer1;
		Il2CppObject * L_4 = ___cancel2;
		NullCheck((OperatorObserverBase_2_t1187768149 *)__this);
		((  void (*) (OperatorObserverBase_2_t1187768149 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((OperatorObserverBase_2_t1187768149 *)__this, (Il2CppObject*)L_3, (Il2CppObject *)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		CombineLatestObservable_3_t2543452560 * L_5 = ___parent0;
		__this->set_parent_2(L_5);
		return;
	}
}
// System.IDisposable UniRx.Operators.CombineLatestObservable`3/CombineLatest<System.Boolean,System.Int32,System.Object>::Run()
extern "C"  Il2CppObject * CombineLatest_Run_m496495875_gshared (CombineLatest_t2150356605 * __this, const MethodInfo* method)
{
	Il2CppObject * V_0 = NULL;
	Il2CppObject * V_1 = NULL;
	{
		CombineLatestObservable_3_t2543452560 * L_0 = (CombineLatestObservable_3_t2543452560 *)__this->get_parent_2();
		NullCheck(L_0);
		Il2CppObject* L_1 = (Il2CppObject*)L_0->get_left_1();
		LeftObserver_t2052923044 * L_2 = (LeftObserver_t2052923044 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		((  void (*) (LeftObserver_t2052923044 *, CombineLatest_t2150356605 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)(L_2, (CombineLatest_t2150356605 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		NullCheck((Il2CppObject*)L_1);
		Il2CppObject * L_3 = InterfaceFuncInvoker1< Il2CppObject *, Il2CppObject* >::Invoke(0 /* System.IDisposable UniRx.IObservable`1<System.Boolean>::Subscribe(UniRx.IObserver`1<T>) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4), (Il2CppObject*)L_1, (Il2CppObject*)L_2);
		V_0 = (Il2CppObject *)L_3;
		CombineLatestObservable_3_t2543452560 * L_4 = (CombineLatestObservable_3_t2543452560 *)__this->get_parent_2();
		NullCheck(L_4);
		Il2CppObject* L_5 = (Il2CppObject*)L_4->get_right_2();
		RightObserver_t615246633 * L_6 = (RightObserver_t615246633 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		((  void (*) (RightObserver_t615246633 *, CombineLatest_t2150356605 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6)->method)(L_6, (CombineLatest_t2150356605 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		NullCheck((Il2CppObject*)L_5);
		Il2CppObject * L_7 = InterfaceFuncInvoker1< Il2CppObject *, Il2CppObject* >::Invoke(0 /* System.IDisposable UniRx.IObservable`1<System.Int32>::Subscribe(UniRx.IObserver`1<T>) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7), (Il2CppObject*)L_5, (Il2CppObject*)L_6);
		V_1 = (Il2CppObject *)L_7;
		Il2CppObject * L_8 = V_0;
		Il2CppObject * L_9 = V_1;
		Il2CppObject * L_10 = StableCompositeDisposable_Create_m3764788535(NULL /*static, unused*/, (Il2CppObject *)L_8, (Il2CppObject *)L_9, /*hidden argument*/NULL);
		return L_10;
	}
}
// System.Void UniRx.Operators.CombineLatestObservable`3/CombineLatest<System.Boolean,System.Int32,System.Object>::Publish()
extern Il2CppClass* Exception_t1967233988_il2cpp_TypeInfo_var;
extern const uint32_t CombineLatest_Publish_m2417128034_MetadataUsageId;
extern "C"  void CombineLatest_Publish_m2417128034_gshared (CombineLatest_t2150356605 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (CombineLatest_Publish_m2417128034_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Il2CppObject * V_0 = NULL;
	Exception_t1967233988 * V_1 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		bool L_0 = (bool)__this->get_leftCompleted_6();
		if (!L_0)
		{
			goto IL_0016;
		}
	}
	{
		bool L_1 = (bool)__this->get_leftStarted_5();
		if (!L_1)
		{
			goto IL_002c;
		}
	}

IL_0016:
	{
		bool L_2 = (bool)__this->get_rightCompleted_9();
		if (!L_2)
		{
			goto IL_0046;
		}
	}
	{
		bool L_3 = (bool)__this->get_rightStarted_8();
		if (L_3)
		{
			goto IL_0046;
		}
	}

IL_002c:
	try
	{ // begin try (depth: 1)
		Il2CppObject* L_4 = (Il2CppObject*)((OperatorObserverBase_2_t1187768149 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		NullCheck((Il2CppObject*)L_4);
		InterfaceActionInvoker0::Invoke(0 /* System.Void UniRx.IObserver`1<System.Object>::OnCompleted() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8), (Il2CppObject*)L_4);
		IL2CPP_LEAVE(0x45, FINALLY_003e);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_003e;
	}

FINALLY_003e:
	{ // begin finally (depth: 1)
		NullCheck((OperatorObserverBase_2_t1187768149 *)__this);
		VirtActionInvoker0::Invoke(4 /* System.Void UniRx.Operators.OperatorObserverBase`2<System.Object,System.Object>::Dispose() */, (OperatorObserverBase_2_t1187768149 *)__this);
		IL2CPP_END_FINALLY(62)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(62)
	{
		IL2CPP_JUMP_TBL(0x45, IL_0045)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0045:
	{
		return;
	}

IL_0046:
	{
		bool L_5 = (bool)__this->get_leftStarted_5();
		if (!L_5)
		{
			goto IL_005c;
		}
	}
	{
		bool L_6 = (bool)__this->get_rightStarted_8();
		if (L_6)
		{
			goto IL_005d;
		}
	}

IL_005c:
	{
		return;
	}

IL_005d:
	try
	{ // begin try (depth: 1)
		CombineLatestObservable_3_t2543452560 * L_7 = (CombineLatestObservable_3_t2543452560 *)__this->get_parent_2();
		NullCheck(L_7);
		Func_3_t1694707683 * L_8 = (Func_3_t1694707683 *)L_7->get_selector_3();
		bool L_9 = (bool)__this->get_leftValue_4();
		int32_t L_10 = (int32_t)__this->get_rightValue_7();
		NullCheck((Func_3_t1694707683 *)L_8);
		Il2CppObject * L_11 = ((  Il2CppObject * (*) (Func_3_t1694707683 *, bool, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10)->method)((Func_3_t1694707683 *)L_8, (bool)L_9, (int32_t)L_10, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10));
		V_0 = (Il2CppObject *)L_11;
		goto IL_00a4;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1967233988 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Exception_t1967233988_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_007f;
		throw e;
	}

CATCH_007f:
	{ // begin catch(System.Exception)
		{
			V_1 = (Exception_t1967233988 *)((Exception_t1967233988 *)__exception_local);
		}

IL_0080:
		try
		{ // begin try (depth: 2)
			Il2CppObject* L_12 = (Il2CppObject*)((OperatorObserverBase_2_t1187768149 *)__this)->get_observer_0();
			il2cpp_codegen_memory_barrier();
			Exception_t1967233988 * L_13 = V_1;
			NullCheck((Il2CppObject*)L_12);
			InterfaceActionInvoker1< Exception_t1967233988 * >::Invoke(1 /* System.Void UniRx.IObserver`1<System.Object>::OnError(System.Exception) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8), (Il2CppObject*)L_12, (Exception_t1967233988 *)L_13);
			IL2CPP_LEAVE(0x9A, FINALLY_0093);
		} // end try (depth: 2)
		catch(Il2CppExceptionWrapper& e)
		{
			__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
			goto FINALLY_0093;
		}

FINALLY_0093:
		{ // begin finally (depth: 2)
			NullCheck((OperatorObserverBase_2_t1187768149 *)__this);
			VirtActionInvoker0::Invoke(4 /* System.Void UniRx.Operators.OperatorObserverBase`2<System.Object,System.Object>::Dispose() */, (OperatorObserverBase_2_t1187768149 *)__this);
			IL2CPP_END_FINALLY(147)
		} // end finally (depth: 2)
		IL2CPP_CLEANUP(147)
		{
			IL2CPP_JUMP_TBL(0x9A, IL_009a)
			IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
		}

IL_009a:
		{
			goto IL_00ab;
		}

IL_009f:
		{
			; // IL_009f: leave IL_00a4
		}
	} // end catch (depth: 1)

IL_00a4:
	{
		Il2CppObject * L_14 = V_0;
		NullCheck((CombineLatest_t2150356605 *)__this);
		VirtActionInvoker1< Il2CppObject * >::Invoke(8 /* System.Void UniRx.Operators.CombineLatestObservable`3/CombineLatest<System.Boolean,System.Int32,System.Object>::OnNext(TResult) */, (CombineLatest_t2150356605 *)__this, (Il2CppObject *)L_14);
	}

IL_00ab:
	{
		return;
	}
}
// System.Void UniRx.Operators.CombineLatestObservable`3/CombineLatest<System.Boolean,System.Int32,System.Object>::OnNext(TResult)
extern "C"  void CombineLatest_OnNext_m3307201088_gshared (CombineLatest_t2150356605 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = (Il2CppObject*)((OperatorObserverBase_2_t1187768149 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		Il2CppObject * L_1 = ___value0;
		NullCheck((Il2CppObject*)L_0);
		InterfaceActionInvoker1< Il2CppObject * >::Invoke(2 /* System.Void UniRx.IObserver`1<System.Object>::OnNext(T) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8), (Il2CppObject*)L_0, (Il2CppObject *)L_1);
		return;
	}
}
// System.Void UniRx.Operators.CombineLatestObservable`3/CombineLatest<System.Boolean,System.Int32,System.Object>::OnError(System.Exception)
extern "C"  void CombineLatest_OnError_m3196643052_gshared (CombineLatest_t2150356605 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method)
{
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		Il2CppObject* L_0 = (Il2CppObject*)((OperatorObserverBase_2_t1187768149 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		Exception_t1967233988 * L_1 = ___error0;
		NullCheck((Il2CppObject*)L_0);
		InterfaceActionInvoker1< Exception_t1967233988 * >::Invoke(1 /* System.Void UniRx.IObserver`1<System.Object>::OnError(System.Exception) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8), (Il2CppObject*)L_0, (Exception_t1967233988 *)L_1);
		IL2CPP_LEAVE(0x1A, FINALLY_0013);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0013;
	}

FINALLY_0013:
	{ // begin finally (depth: 1)
		NullCheck((OperatorObserverBase_2_t1187768149 *)__this);
		VirtActionInvoker0::Invoke(4 /* System.Void UniRx.Operators.OperatorObserverBase`2<System.Object,System.Object>::Dispose() */, (OperatorObserverBase_2_t1187768149 *)__this);
		IL2CPP_END_FINALLY(19)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(19)
	{
		IL2CPP_JUMP_TBL(0x1A, IL_001a)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_001a:
	{
		return;
	}
}
// System.Void UniRx.Operators.CombineLatestObservable`3/CombineLatest<System.Boolean,System.Int32,System.Object>::OnCompleted()
extern "C"  void CombineLatest_OnCompleted_m2368314431_gshared (CombineLatest_t2150356605 * __this, const MethodInfo* method)
{
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		Il2CppObject* L_0 = (Il2CppObject*)((OperatorObserverBase_2_t1187768149 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		NullCheck((Il2CppObject*)L_0);
		InterfaceActionInvoker0::Invoke(0 /* System.Void UniRx.IObserver`1<System.Object>::OnCompleted() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8), (Il2CppObject*)L_0);
		IL2CPP_LEAVE(0x19, FINALLY_0012);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0012;
	}

FINALLY_0012:
	{ // begin finally (depth: 1)
		NullCheck((OperatorObserverBase_2_t1187768149 *)__this);
		VirtActionInvoker0::Invoke(4 /* System.Void UniRx.Operators.OperatorObserverBase`2<System.Object,System.Object>::Dispose() */, (OperatorObserverBase_2_t1187768149 *)__this);
		IL2CPP_END_FINALLY(18)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(18)
	{
		IL2CPP_JUMP_TBL(0x19, IL_0019)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0019:
	{
		return;
	}
}
// System.Void UniRx.Operators.CombineLatestObservable`3/CombineLatest<System.Object,System.Object,System.Object>::.ctor(UniRx.Operators.CombineLatestObservable`3<TLeft,TRight,TResult>,UniRx.IObserver`1<TResult>,System.IDisposable)
extern Il2CppClass* Il2CppObject_il2cpp_TypeInfo_var;
extern const uint32_t CombineLatest__ctor_m375266749_MetadataUsageId;
extern "C"  void CombineLatest__ctor_m375266749_gshared (CombineLatest_t2347858151 * __this, CombineLatestObservable_3_t2740954106 * ___parent0, Il2CppObject* ___observer1, Il2CppObject * ___cancel2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (CombineLatest__ctor_m375266749_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Il2CppObject * V_0 = NULL;
	Il2CppObject * V_1 = NULL;
	{
		Il2CppObject * L_0 = (Il2CppObject *)il2cpp_codegen_object_new(Il2CppObject_il2cpp_TypeInfo_var);
		Object__ctor_m1772956182(L_0, /*hidden argument*/NULL);
		__this->set_gate_3(L_0);
		Initobj (Il2CppObject_il2cpp_TypeInfo_var, (&V_0));
		Il2CppObject * L_1 = V_0;
		__this->set_leftValue_4(L_1);
		Initobj (Il2CppObject_il2cpp_TypeInfo_var, (&V_1));
		Il2CppObject * L_2 = V_1;
		__this->set_rightValue_7(L_2);
		Il2CppObject* L_3 = ___observer1;
		Il2CppObject * L_4 = ___cancel2;
		NullCheck((OperatorObserverBase_2_t1187768149 *)__this);
		((  void (*) (OperatorObserverBase_2_t1187768149 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((OperatorObserverBase_2_t1187768149 *)__this, (Il2CppObject*)L_3, (Il2CppObject *)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		CombineLatestObservable_3_t2740954106 * L_5 = ___parent0;
		__this->set_parent_2(L_5);
		return;
	}
}
// System.IDisposable UniRx.Operators.CombineLatestObservable`3/CombineLatest<System.Object,System.Object,System.Object>::Run()
extern "C"  Il2CppObject * CombineLatest_Run_m338529697_gshared (CombineLatest_t2347858151 * __this, const MethodInfo* method)
{
	Il2CppObject * V_0 = NULL;
	Il2CppObject * V_1 = NULL;
	{
		CombineLatestObservable_3_t2740954106 * L_0 = (CombineLatestObservable_3_t2740954106 *)__this->get_parent_2();
		NullCheck(L_0);
		Il2CppObject* L_1 = (Il2CppObject*)L_0->get_left_1();
		LeftObserver_t2250424590 * L_2 = (LeftObserver_t2250424590 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		((  void (*) (LeftObserver_t2250424590 *, CombineLatest_t2347858151 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)(L_2, (CombineLatest_t2347858151 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		NullCheck((Il2CppObject*)L_1);
		Il2CppObject * L_3 = InterfaceFuncInvoker1< Il2CppObject *, Il2CppObject* >::Invoke(0 /* System.IDisposable UniRx.IObservable`1<System.Object>::Subscribe(UniRx.IObserver`1<T>) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4), (Il2CppObject*)L_1, (Il2CppObject*)L_2);
		V_0 = (Il2CppObject *)L_3;
		CombineLatestObservable_3_t2740954106 * L_4 = (CombineLatestObservable_3_t2740954106 *)__this->get_parent_2();
		NullCheck(L_4);
		Il2CppObject* L_5 = (Il2CppObject*)L_4->get_right_2();
		RightObserver_t812748179 * L_6 = (RightObserver_t812748179 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		((  void (*) (RightObserver_t812748179 *, CombineLatest_t2347858151 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6)->method)(L_6, (CombineLatest_t2347858151 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		NullCheck((Il2CppObject*)L_5);
		Il2CppObject * L_7 = InterfaceFuncInvoker1< Il2CppObject *, Il2CppObject* >::Invoke(0 /* System.IDisposable UniRx.IObservable`1<System.Object>::Subscribe(UniRx.IObserver`1<T>) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7), (Il2CppObject*)L_5, (Il2CppObject*)L_6);
		V_1 = (Il2CppObject *)L_7;
		Il2CppObject * L_8 = V_0;
		Il2CppObject * L_9 = V_1;
		Il2CppObject * L_10 = StableCompositeDisposable_Create_m3764788535(NULL /*static, unused*/, (Il2CppObject *)L_8, (Il2CppObject *)L_9, /*hidden argument*/NULL);
		return L_10;
	}
}
// System.Void UniRx.Operators.CombineLatestObservable`3/CombineLatest<System.Object,System.Object,System.Object>::Publish()
extern Il2CppClass* Exception_t1967233988_il2cpp_TypeInfo_var;
extern const uint32_t CombineLatest_Publish_m193631232_MetadataUsageId;
extern "C"  void CombineLatest_Publish_m193631232_gshared (CombineLatest_t2347858151 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (CombineLatest_Publish_m193631232_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Il2CppObject * V_0 = NULL;
	Exception_t1967233988 * V_1 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		bool L_0 = (bool)__this->get_leftCompleted_6();
		if (!L_0)
		{
			goto IL_0016;
		}
	}
	{
		bool L_1 = (bool)__this->get_leftStarted_5();
		if (!L_1)
		{
			goto IL_002c;
		}
	}

IL_0016:
	{
		bool L_2 = (bool)__this->get_rightCompleted_9();
		if (!L_2)
		{
			goto IL_0046;
		}
	}
	{
		bool L_3 = (bool)__this->get_rightStarted_8();
		if (L_3)
		{
			goto IL_0046;
		}
	}

IL_002c:
	try
	{ // begin try (depth: 1)
		Il2CppObject* L_4 = (Il2CppObject*)((OperatorObserverBase_2_t1187768149 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		NullCheck((Il2CppObject*)L_4);
		InterfaceActionInvoker0::Invoke(0 /* System.Void UniRx.IObserver`1<System.Object>::OnCompleted() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8), (Il2CppObject*)L_4);
		IL2CPP_LEAVE(0x45, FINALLY_003e);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_003e;
	}

FINALLY_003e:
	{ // begin finally (depth: 1)
		NullCheck((OperatorObserverBase_2_t1187768149 *)__this);
		VirtActionInvoker0::Invoke(4 /* System.Void UniRx.Operators.OperatorObserverBase`2<System.Object,System.Object>::Dispose() */, (OperatorObserverBase_2_t1187768149 *)__this);
		IL2CPP_END_FINALLY(62)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(62)
	{
		IL2CPP_JUMP_TBL(0x45, IL_0045)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0045:
	{
		return;
	}

IL_0046:
	{
		bool L_5 = (bool)__this->get_leftStarted_5();
		if (!L_5)
		{
			goto IL_005c;
		}
	}
	{
		bool L_6 = (bool)__this->get_rightStarted_8();
		if (L_6)
		{
			goto IL_005d;
		}
	}

IL_005c:
	{
		return;
	}

IL_005d:
	try
	{ // begin try (depth: 1)
		CombineLatestObservable_3_t2740954106 * L_7 = (CombineLatestObservable_3_t2740954106 *)__this->get_parent_2();
		NullCheck(L_7);
		Func_3_t1892209229 * L_8 = (Func_3_t1892209229 *)L_7->get_selector_3();
		Il2CppObject * L_9 = (Il2CppObject *)__this->get_leftValue_4();
		Il2CppObject * L_10 = (Il2CppObject *)__this->get_rightValue_7();
		NullCheck((Func_3_t1892209229 *)L_8);
		Il2CppObject * L_11 = ((  Il2CppObject * (*) (Func_3_t1892209229 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10)->method)((Func_3_t1892209229 *)L_8, (Il2CppObject *)L_9, (Il2CppObject *)L_10, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10));
		V_0 = (Il2CppObject *)L_11;
		goto IL_00a4;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1967233988 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Exception_t1967233988_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_007f;
		throw e;
	}

CATCH_007f:
	{ // begin catch(System.Exception)
		{
			V_1 = (Exception_t1967233988 *)((Exception_t1967233988 *)__exception_local);
		}

IL_0080:
		try
		{ // begin try (depth: 2)
			Il2CppObject* L_12 = (Il2CppObject*)((OperatorObserverBase_2_t1187768149 *)__this)->get_observer_0();
			il2cpp_codegen_memory_barrier();
			Exception_t1967233988 * L_13 = V_1;
			NullCheck((Il2CppObject*)L_12);
			InterfaceActionInvoker1< Exception_t1967233988 * >::Invoke(1 /* System.Void UniRx.IObserver`1<System.Object>::OnError(System.Exception) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8), (Il2CppObject*)L_12, (Exception_t1967233988 *)L_13);
			IL2CPP_LEAVE(0x9A, FINALLY_0093);
		} // end try (depth: 2)
		catch(Il2CppExceptionWrapper& e)
		{
			__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
			goto FINALLY_0093;
		}

FINALLY_0093:
		{ // begin finally (depth: 2)
			NullCheck((OperatorObserverBase_2_t1187768149 *)__this);
			VirtActionInvoker0::Invoke(4 /* System.Void UniRx.Operators.OperatorObserverBase`2<System.Object,System.Object>::Dispose() */, (OperatorObserverBase_2_t1187768149 *)__this);
			IL2CPP_END_FINALLY(147)
		} // end finally (depth: 2)
		IL2CPP_CLEANUP(147)
		{
			IL2CPP_JUMP_TBL(0x9A, IL_009a)
			IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
		}

IL_009a:
		{
			goto IL_00ab;
		}

IL_009f:
		{
			; // IL_009f: leave IL_00a4
		}
	} // end catch (depth: 1)

IL_00a4:
	{
		Il2CppObject * L_14 = V_0;
		NullCheck((CombineLatest_t2347858151 *)__this);
		VirtActionInvoker1< Il2CppObject * >::Invoke(8 /* System.Void UniRx.Operators.CombineLatestObservable`3/CombineLatest<System.Object,System.Object,System.Object>::OnNext(TResult) */, (CombineLatest_t2347858151 *)__this, (Il2CppObject *)L_14);
	}

IL_00ab:
	{
		return;
	}
}
// System.Void UniRx.Operators.CombineLatestObservable`3/CombineLatest<System.Object,System.Object,System.Object>::OnNext(TResult)
extern "C"  void CombineLatest_OnNext_m441701726_gshared (CombineLatest_t2347858151 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = (Il2CppObject*)((OperatorObserverBase_2_t1187768149 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		Il2CppObject * L_1 = ___value0;
		NullCheck((Il2CppObject*)L_0);
		InterfaceActionInvoker1< Il2CppObject * >::Invoke(2 /* System.Void UniRx.IObserver`1<System.Object>::OnNext(T) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8), (Il2CppObject*)L_0, (Il2CppObject *)L_1);
		return;
	}
}
// System.Void UniRx.Operators.CombineLatestObservable`3/CombineLatest<System.Object,System.Object,System.Object>::OnError(System.Exception)
extern "C"  void CombineLatest_OnError_m4168486026_gshared (CombineLatest_t2347858151 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method)
{
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		Il2CppObject* L_0 = (Il2CppObject*)((OperatorObserverBase_2_t1187768149 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		Exception_t1967233988 * L_1 = ___error0;
		NullCheck((Il2CppObject*)L_0);
		InterfaceActionInvoker1< Exception_t1967233988 * >::Invoke(1 /* System.Void UniRx.IObserver`1<System.Object>::OnError(System.Exception) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8), (Il2CppObject*)L_0, (Exception_t1967233988 *)L_1);
		IL2CPP_LEAVE(0x1A, FINALLY_0013);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0013;
	}

FINALLY_0013:
	{ // begin finally (depth: 1)
		NullCheck((OperatorObserverBase_2_t1187768149 *)__this);
		VirtActionInvoker0::Invoke(4 /* System.Void UniRx.Operators.OperatorObserverBase`2<System.Object,System.Object>::Dispose() */, (OperatorObserverBase_2_t1187768149 *)__this);
		IL2CPP_END_FINALLY(19)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(19)
	{
		IL2CPP_JUMP_TBL(0x1A, IL_001a)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_001a:
	{
		return;
	}
}
// System.Void UniRx.Operators.CombineLatestObservable`3/CombineLatest<System.Object,System.Object,System.Object>::OnCompleted()
extern "C"  void CombineLatest_OnCompleted_m1717288669_gshared (CombineLatest_t2347858151 * __this, const MethodInfo* method)
{
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		Il2CppObject* L_0 = (Il2CppObject*)((OperatorObserverBase_2_t1187768149 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		NullCheck((Il2CppObject*)L_0);
		InterfaceActionInvoker0::Invoke(0 /* System.Void UniRx.IObserver`1<System.Object>::OnCompleted() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8), (Il2CppObject*)L_0);
		IL2CPP_LEAVE(0x19, FINALLY_0012);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0012;
	}

FINALLY_0012:
	{ // begin finally (depth: 1)
		NullCheck((OperatorObserverBase_2_t1187768149 *)__this);
		VirtActionInvoker0::Invoke(4 /* System.Void UniRx.Operators.OperatorObserverBase`2<System.Object,System.Object>::Dispose() */, (OperatorObserverBase_2_t1187768149 *)__this);
		IL2CPP_END_FINALLY(18)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(18)
	{
		IL2CPP_JUMP_TBL(0x19, IL_0019)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0019:
	{
		return;
	}
}
// System.Void UniRx.Operators.CombineLatestObservable`3<System.Boolean,System.Boolean,System.Boolean>::.ctor(UniRx.IObservable`1<TLeft>,UniRx.IObservable`1<TRight>,System.Func`3<TLeft,TRight,TResult>)
extern "C"  void CombineLatestObservable_3__ctor_m1235902814_gshared (CombineLatestObservable_3_t3912295671 * __this, Il2CppObject* ___left0, Il2CppObject* ___right1, Func_3_t3063550794 * ___selector2, const MethodInfo* method)
{
	CombineLatestObservable_3_t3912295671 * G_B2_0 = NULL;
	CombineLatestObservable_3_t3912295671 * G_B1_0 = NULL;
	int32_t G_B3_0 = 0;
	CombineLatestObservable_3_t3912295671 * G_B3_1 = NULL;
	{
		Il2CppObject* L_0 = ___left0;
		bool L_1 = ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)(NULL /*static, unused*/, (Il2CppObject*)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		G_B1_0 = ((CombineLatestObservable_3_t3912295671 *)(__this));
		if (L_1)
		{
			G_B2_0 = ((CombineLatestObservable_3_t3912295671 *)(__this));
			goto IL_0014;
		}
	}
	{
		Il2CppObject* L_2 = ___right1;
		bool L_3 = ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)(NULL /*static, unused*/, (Il2CppObject*)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		G_B3_0 = ((int32_t)(L_3));
		G_B3_1 = ((CombineLatestObservable_3_t3912295671 *)(G_B1_0));
		goto IL_0015;
	}

IL_0014:
	{
		G_B3_0 = 1;
		G_B3_1 = ((CombineLatestObservable_3_t3912295671 *)(G_B2_0));
	}

IL_0015:
	{
		NullCheck((OperatorObservableBase_1_t3570117608 *)G_B3_1);
		((  void (*) (OperatorObservableBase_1_t3570117608 *, bool, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)((OperatorObservableBase_1_t3570117608 *)G_B3_1, (bool)G_B3_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		Il2CppObject* L_4 = ___left0;
		__this->set_left_1(L_4);
		Il2CppObject* L_5 = ___right1;
		__this->set_right_2(L_5);
		Func_3_t3063550794 * L_6 = ___selector2;
		__this->set_selector_3(L_6);
		return;
	}
}
// System.IDisposable UniRx.Operators.CombineLatestObservable`3<System.Boolean,System.Boolean,System.Boolean>::SubscribeCore(UniRx.IObserver`1<TResult>,System.IDisposable)
extern "C"  Il2CppObject * CombineLatestObservable_3_SubscribeCore_m3527518386_gshared (CombineLatestObservable_3_t3912295671 * __this, Il2CppObject* ___observer0, Il2CppObject * ___cancel1, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = ___observer0;
		Il2CppObject * L_1 = ___cancel1;
		CombineLatest_t3519199716 * L_2 = (CombineLatest_t3519199716 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		((  void (*) (CombineLatest_t3519199716 *, CombineLatestObservable_3_t3912295671 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->method)(L_2, (CombineLatestObservable_3_t3912295671 *)__this, (Il2CppObject*)L_0, (Il2CppObject *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		NullCheck((CombineLatest_t3519199716 *)L_2);
		Il2CppObject * L_3 = ((  Il2CppObject * (*) (CombineLatest_t3519199716 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6)->method)((CombineLatest_t3519199716 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		return L_3;
	}
}
// System.Void UniRx.Operators.CombineLatestObservable`3<System.Boolean,System.Boolean,System.Object>::.ctor(UniRx.IObservable`1<TLeft>,UniRx.IObservable`1<TRight>,System.Func`3<TLeft,TRight,TResult>)
extern "C"  void CombineLatestObservable_3__ctor_m1710224925_gshared (CombineLatestObservable_3_t243429454 * __this, Il2CppObject* ___left0, Il2CppObject* ___right1, Func_3_t3689651873 * ___selector2, const MethodInfo* method)
{
	CombineLatestObservable_3_t243429454 * G_B2_0 = NULL;
	CombineLatestObservable_3_t243429454 * G_B1_0 = NULL;
	int32_t G_B3_0 = 0;
	CombineLatestObservable_3_t243429454 * G_B3_1 = NULL;
	{
		Il2CppObject* L_0 = ___left0;
		bool L_1 = ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)(NULL /*static, unused*/, (Il2CppObject*)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		G_B1_0 = ((CombineLatestObservable_3_t243429454 *)(__this));
		if (L_1)
		{
			G_B2_0 = ((CombineLatestObservable_3_t243429454 *)(__this));
			goto IL_0014;
		}
	}
	{
		Il2CppObject* L_2 = ___right1;
		bool L_3 = ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)(NULL /*static, unused*/, (Il2CppObject*)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		G_B3_0 = ((int32_t)(L_3));
		G_B3_1 = ((CombineLatestObservable_3_t243429454 *)(G_B1_0));
		goto IL_0015;
	}

IL_0014:
	{
		G_B3_0 = 1;
		G_B3_1 = ((CombineLatestObservable_3_t243429454 *)(G_B2_0));
	}

IL_0015:
	{
		NullCheck((OperatorObservableBase_1_t4196218687 *)G_B3_1);
		((  void (*) (OperatorObservableBase_1_t4196218687 *, bool, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)((OperatorObservableBase_1_t4196218687 *)G_B3_1, (bool)G_B3_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		Il2CppObject* L_4 = ___left0;
		__this->set_left_1(L_4);
		Il2CppObject* L_5 = ___right1;
		__this->set_right_2(L_5);
		Func_3_t3689651873 * L_6 = ___selector2;
		__this->set_selector_3(L_6);
		return;
	}
}
// System.IDisposable UniRx.Operators.CombineLatestObservable`3<System.Boolean,System.Boolean,System.Object>::SubscribeCore(UniRx.IObserver`1<TResult>,System.IDisposable)
extern "C"  Il2CppObject * CombineLatestObservable_3_SubscribeCore_m3085137065_gshared (CombineLatestObservable_3_t243429454 * __this, Il2CppObject* ___observer0, Il2CppObject * ___cancel1, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = ___observer0;
		Il2CppObject * L_1 = ___cancel1;
		CombineLatest_t4145300795 * L_2 = (CombineLatest_t4145300795 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		((  void (*) (CombineLatest_t4145300795 *, CombineLatestObservable_3_t243429454 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->method)(L_2, (CombineLatestObservable_3_t243429454 *)__this, (Il2CppObject*)L_0, (Il2CppObject *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		NullCheck((CombineLatest_t4145300795 *)L_2);
		Il2CppObject * L_3 = ((  Il2CppObject * (*) (CombineLatest_t4145300795 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6)->method)((CombineLatest_t4145300795 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		return L_3;
	}
}
// System.Void UniRx.Operators.CombineLatestObservable`3<System.Boolean,System.Int32,System.Object>::.ctor(UniRx.IObservable`1<TLeft>,UniRx.IObservable`1<TRight>,System.Func`3<TLeft,TRight,TResult>)
extern "C"  void CombineLatestObservable_3__ctor_m247565635_gshared (CombineLatestObservable_3_t2543452560 * __this, Il2CppObject* ___left0, Il2CppObject* ___right1, Func_3_t1694707683 * ___selector2, const MethodInfo* method)
{
	CombineLatestObservable_3_t2543452560 * G_B2_0 = NULL;
	CombineLatestObservable_3_t2543452560 * G_B1_0 = NULL;
	int32_t G_B3_0 = 0;
	CombineLatestObservable_3_t2543452560 * G_B3_1 = NULL;
	{
		Il2CppObject* L_0 = ___left0;
		bool L_1 = ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)(NULL /*static, unused*/, (Il2CppObject*)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		G_B1_0 = ((CombineLatestObservable_3_t2543452560 *)(__this));
		if (L_1)
		{
			G_B2_0 = ((CombineLatestObservable_3_t2543452560 *)(__this));
			goto IL_0014;
		}
	}
	{
		Il2CppObject* L_2 = ___right1;
		bool L_3 = ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)(NULL /*static, unused*/, (Il2CppObject*)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		G_B3_0 = ((int32_t)(L_3));
		G_B3_1 = ((CombineLatestObservable_3_t2543452560 *)(G_B1_0));
		goto IL_0015;
	}

IL_0014:
	{
		G_B3_0 = 1;
		G_B3_1 = ((CombineLatestObservable_3_t2543452560 *)(G_B2_0));
	}

IL_0015:
	{
		NullCheck((OperatorObservableBase_1_t4196218687 *)G_B3_1);
		((  void (*) (OperatorObservableBase_1_t4196218687 *, bool, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)((OperatorObservableBase_1_t4196218687 *)G_B3_1, (bool)G_B3_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		Il2CppObject* L_4 = ___left0;
		__this->set_left_1(L_4);
		Il2CppObject* L_5 = ___right1;
		__this->set_right_2(L_5);
		Func_3_t1694707683 * L_6 = ___selector2;
		__this->set_selector_3(L_6);
		return;
	}
}
// System.IDisposable UniRx.Operators.CombineLatestObservable`3<System.Boolean,System.Int32,System.Object>::SubscribeCore(UniRx.IObserver`1<TResult>,System.IDisposable)
extern "C"  Il2CppObject * CombineLatestObservable_3_SubscribeCore_m1573197955_gshared (CombineLatestObservable_3_t2543452560 * __this, Il2CppObject* ___observer0, Il2CppObject * ___cancel1, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = ___observer0;
		Il2CppObject * L_1 = ___cancel1;
		CombineLatest_t2150356605 * L_2 = (CombineLatest_t2150356605 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		((  void (*) (CombineLatest_t2150356605 *, CombineLatestObservable_3_t2543452560 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->method)(L_2, (CombineLatestObservable_3_t2543452560 *)__this, (Il2CppObject*)L_0, (Il2CppObject *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		NullCheck((CombineLatest_t2150356605 *)L_2);
		Il2CppObject * L_3 = ((  Il2CppObject * (*) (CombineLatest_t2150356605 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6)->method)((CombineLatest_t2150356605 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		return L_3;
	}
}
// System.Void UniRx.Operators.CombineLatestObservable`3<System.Object,System.Object,System.Object>::.ctor(UniRx.IObservable`1<TLeft>,UniRx.IObservable`1<TRight>,System.Func`3<TLeft,TRight,TResult>)
extern "C"  void CombineLatestObservable_3__ctor_m3043640545_gshared (CombineLatestObservable_3_t2740954106 * __this, Il2CppObject* ___left0, Il2CppObject* ___right1, Func_3_t1892209229 * ___selector2, const MethodInfo* method)
{
	CombineLatestObservable_3_t2740954106 * G_B2_0 = NULL;
	CombineLatestObservable_3_t2740954106 * G_B1_0 = NULL;
	int32_t G_B3_0 = 0;
	CombineLatestObservable_3_t2740954106 * G_B3_1 = NULL;
	{
		Il2CppObject* L_0 = ___left0;
		bool L_1 = ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)(NULL /*static, unused*/, (Il2CppObject*)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		G_B1_0 = ((CombineLatestObservable_3_t2740954106 *)(__this));
		if (L_1)
		{
			G_B2_0 = ((CombineLatestObservable_3_t2740954106 *)(__this));
			goto IL_0014;
		}
	}
	{
		Il2CppObject* L_2 = ___right1;
		bool L_3 = ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)(NULL /*static, unused*/, (Il2CppObject*)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		G_B3_0 = ((int32_t)(L_3));
		G_B3_1 = ((CombineLatestObservable_3_t2740954106 *)(G_B1_0));
		goto IL_0015;
	}

IL_0014:
	{
		G_B3_0 = 1;
		G_B3_1 = ((CombineLatestObservable_3_t2740954106 *)(G_B2_0));
	}

IL_0015:
	{
		NullCheck((OperatorObservableBase_1_t4196218687 *)G_B3_1);
		((  void (*) (OperatorObservableBase_1_t4196218687 *, bool, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)((OperatorObservableBase_1_t4196218687 *)G_B3_1, (bool)G_B3_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		Il2CppObject* L_4 = ___left0;
		__this->set_left_1(L_4);
		Il2CppObject* L_5 = ___right1;
		__this->set_right_2(L_5);
		Func_3_t1892209229 * L_6 = ___selector2;
		__this->set_selector_3(L_6);
		return;
	}
}
// System.IDisposable UniRx.Operators.CombineLatestObservable`3<System.Object,System.Object,System.Object>::SubscribeCore(UniRx.IObserver`1<TResult>,System.IDisposable)
extern "C"  Il2CppObject * CombineLatestObservable_3_SubscribeCore_m2531434021_gshared (CombineLatestObservable_3_t2740954106 * __this, Il2CppObject* ___observer0, Il2CppObject * ___cancel1, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = ___observer0;
		Il2CppObject * L_1 = ___cancel1;
		CombineLatest_t2347858151 * L_2 = (CombineLatest_t2347858151 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		((  void (*) (CombineLatest_t2347858151 *, CombineLatestObservable_3_t2740954106 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->method)(L_2, (CombineLatestObservable_3_t2740954106 *)__this, (Il2CppObject*)L_0, (Il2CppObject *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		NullCheck((CombineLatest_t2347858151 *)L_2);
		Il2CppObject * L_3 = ((  Il2CppObject * (*) (CombineLatest_t2347858151 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6)->method)((CombineLatest_t2347858151 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		return L_3;
	}
}
// System.Void UniRx.Operators.CombineLatestObservable`4/CombineLatest<System.Object,System.Object,System.Object,System.Object>::.ctor(System.Int32,UniRx.Operators.CombineLatestObservable`4<T1,T2,T3,TR>,UniRx.IObserver`1<TR>,System.IDisposable)
extern Il2CppClass* Il2CppObject_il2cpp_TypeInfo_var;
extern const uint32_t CombineLatest__ctor_m2256374533_MetadataUsageId;
extern "C"  void CombineLatest__ctor_m2256374533_gshared (CombineLatest_t227530385 * __this, int32_t ___length0, CombineLatestObservable_4_t1056168445 * ___parent1, Il2CppObject* ___observer2, Il2CppObject * ___cancel3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (CombineLatest__ctor_m2256374533_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject * L_0 = (Il2CppObject *)il2cpp_codegen_object_new(Il2CppObject_il2cpp_TypeInfo_var);
		Object__ctor_m1772956182(L_0, /*hidden argument*/NULL);
		__this->set_gate_7(L_0);
		int32_t L_1 = ___length0;
		Il2CppObject* L_2 = ___observer2;
		Il2CppObject * L_3 = ___cancel3;
		NullCheck((NthCombineLatestObserverBase_1_t2199482330 *)__this);
		((  void (*) (NthCombineLatestObserverBase_1_t2199482330 *, int32_t, Il2CppObject*, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((NthCombineLatestObserverBase_1_t2199482330 *)__this, (int32_t)L_1, (Il2CppObject*)L_2, (Il2CppObject *)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		CombineLatestObservable_4_t1056168445 * L_4 = ___parent1;
		__this->set_parent_6(L_4);
		return;
	}
}
// System.IDisposable UniRx.Operators.CombineLatestObservable`4/CombineLatest<System.Object,System.Object,System.Object,System.Object>::Run()
extern "C"  Il2CppObject * CombineLatest_Run_m2225954772_gshared (CombineLatest_t227530385 * __this, const MethodInfo* method)
{
	Il2CppObject * V_0 = NULL;
	Il2CppObject * V_1 = NULL;
	Il2CppObject * V_2 = NULL;
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_gate_7();
		CombineLatestObserver_1_t123567747 * L_1 = (CombineLatestObserver_1_t123567747 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		((  void (*) (CombineLatestObserver_1_t123567747 *, Il2CppObject *, Il2CppObject *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)(L_1, (Il2CppObject *)L_0, (Il2CppObject *)__this, (int32_t)0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		__this->set_c1_8(L_1);
		Il2CppObject * L_2 = (Il2CppObject *)__this->get_gate_7();
		CombineLatestObserver_1_t123567747 * L_3 = (CombineLatestObserver_1_t123567747 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		((  void (*) (CombineLatestObserver_1_t123567747 *, Il2CppObject *, Il2CppObject *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6)->method)(L_3, (Il2CppObject *)L_2, (Il2CppObject *)__this, (int32_t)1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		__this->set_c2_9(L_3);
		Il2CppObject * L_4 = (Il2CppObject *)__this->get_gate_7();
		CombineLatestObserver_1_t123567747 * L_5 = (CombineLatestObserver_1_t123567747 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7));
		((  void (*) (CombineLatestObserver_1_t123567747 *, Il2CppObject *, Il2CppObject *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8)->method)(L_5, (Il2CppObject *)L_4, (Il2CppObject *)__this, (int32_t)2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8));
		__this->set_c3_10(L_5);
		CombineLatestObservable_4_t1056168445 * L_6 = (CombineLatestObservable_4_t1056168445 *)__this->get_parent_6();
		NullCheck(L_6);
		Il2CppObject* L_7 = (Il2CppObject*)L_6->get_source1_1();
		CombineLatestObserver_1_t123567747 * L_8 = (CombineLatestObserver_1_t123567747 *)__this->get_c1_8();
		NullCheck((Il2CppObject*)L_7);
		Il2CppObject * L_9 = InterfaceFuncInvoker1< Il2CppObject *, Il2CppObject* >::Invoke(0 /* System.IDisposable UniRx.IObservable`1<System.Object>::Subscribe(UniRx.IObserver`1<T>) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9), (Il2CppObject*)L_7, (Il2CppObject*)L_8);
		V_0 = (Il2CppObject *)L_9;
		CombineLatestObservable_4_t1056168445 * L_10 = (CombineLatestObservable_4_t1056168445 *)__this->get_parent_6();
		NullCheck(L_10);
		Il2CppObject* L_11 = (Il2CppObject*)L_10->get_source2_2();
		CombineLatestObserver_1_t123567747 * L_12 = (CombineLatestObserver_1_t123567747 *)__this->get_c2_9();
		NullCheck((Il2CppObject*)L_11);
		Il2CppObject * L_13 = InterfaceFuncInvoker1< Il2CppObject *, Il2CppObject* >::Invoke(0 /* System.IDisposable UniRx.IObservable`1<System.Object>::Subscribe(UniRx.IObserver`1<T>) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10), (Il2CppObject*)L_11, (Il2CppObject*)L_12);
		V_1 = (Il2CppObject *)L_13;
		CombineLatestObservable_4_t1056168445 * L_14 = (CombineLatestObservable_4_t1056168445 *)__this->get_parent_6();
		NullCheck(L_14);
		Il2CppObject* L_15 = (Il2CppObject*)L_14->get_source3_3();
		CombineLatestObserver_1_t123567747 * L_16 = (CombineLatestObserver_1_t123567747 *)__this->get_c3_10();
		NullCheck((Il2CppObject*)L_15);
		Il2CppObject * L_17 = InterfaceFuncInvoker1< Il2CppObject *, Il2CppObject* >::Invoke(0 /* System.IDisposable UniRx.IObservable`1<System.Object>::Subscribe(UniRx.IObserver`1<T>) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 11), (Il2CppObject*)L_15, (Il2CppObject*)L_16);
		V_2 = (Il2CppObject *)L_17;
		Il2CppObject * L_18 = V_0;
		Il2CppObject * L_19 = V_1;
		Il2CppObject * L_20 = V_2;
		Il2CppObject * L_21 = StableCompositeDisposable_Create_m2505029765(NULL /*static, unused*/, (Il2CppObject *)L_18, (Il2CppObject *)L_19, (Il2CppObject *)L_20, /*hidden argument*/NULL);
		return L_21;
	}
}
// TR UniRx.Operators.CombineLatestObservable`4/CombineLatest<System.Object,System.Object,System.Object,System.Object>::GetResult()
extern "C"  Il2CppObject * CombineLatest_GetResult_m1288342640_gshared (CombineLatest_t227530385 * __this, const MethodInfo* method)
{
	{
		CombineLatestObservable_4_t1056168445 * L_0 = (CombineLatestObservable_4_t1056168445 *)__this->get_parent_6();
		NullCheck(L_0);
		CombineLatestFunc_4_t382088552 * L_1 = (CombineLatestFunc_4_t382088552 *)L_0->get_resultSelector_4();
		CombineLatestObserver_1_t123567747 * L_2 = (CombineLatestObserver_1_t123567747 *)__this->get_c1_8();
		NullCheck((CombineLatestObserver_1_t123567747 *)L_2);
		Il2CppObject * L_3 = ((  Il2CppObject * (*) (CombineLatestObserver_1_t123567747 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 12)->method)((CombineLatestObserver_1_t123567747 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 12));
		CombineLatestObserver_1_t123567747 * L_4 = (CombineLatestObserver_1_t123567747 *)__this->get_c2_9();
		NullCheck((CombineLatestObserver_1_t123567747 *)L_4);
		Il2CppObject * L_5 = ((  Il2CppObject * (*) (CombineLatestObserver_1_t123567747 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 13)->method)((CombineLatestObserver_1_t123567747 *)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 13));
		CombineLatestObserver_1_t123567747 * L_6 = (CombineLatestObserver_1_t123567747 *)__this->get_c3_10();
		NullCheck((CombineLatestObserver_1_t123567747 *)L_6);
		Il2CppObject * L_7 = ((  Il2CppObject * (*) (CombineLatestObserver_1_t123567747 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 14)->method)((CombineLatestObserver_1_t123567747 *)L_6, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 14));
		NullCheck((CombineLatestFunc_4_t382088552 *)L_1);
		Il2CppObject * L_8 = ((  Il2CppObject * (*) (CombineLatestFunc_4_t382088552 *, Il2CppObject *, Il2CppObject *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 15)->method)((CombineLatestFunc_4_t382088552 *)L_1, (Il2CppObject *)L_3, (Il2CppObject *)L_5, (Il2CppObject *)L_7, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 15));
		return L_8;
	}
}
// System.Void UniRx.Operators.CombineLatestObservable`4/CombineLatest<System.Object,System.Object,System.Object,System.Object>::OnNext(TR)
extern "C"  void CombineLatest_OnNext_m3410187762_gshared (CombineLatest_t227530385 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = (Il2CppObject*)((OperatorObserverBase_2_t1187768149 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		Il2CppObject * L_1 = ___value0;
		NullCheck((Il2CppObject*)L_0);
		InterfaceActionInvoker1< Il2CppObject * >::Invoke(2 /* System.Void UniRx.IObserver`1<System.Object>::OnNext(T) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 16), (Il2CppObject*)L_0, (Il2CppObject *)L_1);
		return;
	}
}
// System.Void UniRx.Operators.CombineLatestObservable`4/CombineLatest<System.Object,System.Object,System.Object,System.Object>::OnError(System.Exception)
extern "C"  void CombineLatest_OnError_m1889455101_gshared (CombineLatest_t227530385 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method)
{
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		Il2CppObject* L_0 = (Il2CppObject*)((OperatorObserverBase_2_t1187768149 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		Exception_t1967233988 * L_1 = ___error0;
		NullCheck((Il2CppObject*)L_0);
		InterfaceActionInvoker1< Exception_t1967233988 * >::Invoke(1 /* System.Void UniRx.IObserver`1<System.Object>::OnError(System.Exception) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 16), (Il2CppObject*)L_0, (Exception_t1967233988 *)L_1);
		IL2CPP_LEAVE(0x1A, FINALLY_0013);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0013;
	}

FINALLY_0013:
	{ // begin finally (depth: 1)
		NullCheck((OperatorObserverBase_2_t1187768149 *)__this);
		VirtActionInvoker0::Invoke(4 /* System.Void UniRx.Operators.OperatorObserverBase`2<System.Object,System.Object>::Dispose() */, (OperatorObserverBase_2_t1187768149 *)__this);
		IL2CPP_END_FINALLY(19)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(19)
	{
		IL2CPP_JUMP_TBL(0x1A, IL_001a)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_001a:
	{
		return;
	}
}
// System.Void UniRx.Operators.CombineLatestObservable`4/CombineLatest<System.Object,System.Object,System.Object,System.Object>::OnCompleted()
extern "C"  void CombineLatest_OnCompleted_m1135203024_gshared (CombineLatest_t227530385 * __this, const MethodInfo* method)
{
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		Il2CppObject* L_0 = (Il2CppObject*)((OperatorObserverBase_2_t1187768149 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		NullCheck((Il2CppObject*)L_0);
		InterfaceActionInvoker0::Invoke(0 /* System.Void UniRx.IObserver`1<System.Object>::OnCompleted() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 16), (Il2CppObject*)L_0);
		IL2CPP_LEAVE(0x19, FINALLY_0012);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0012;
	}

FINALLY_0012:
	{ // begin finally (depth: 1)
		NullCheck((OperatorObserverBase_2_t1187768149 *)__this);
		VirtActionInvoker0::Invoke(4 /* System.Void UniRx.Operators.OperatorObserverBase`2<System.Object,System.Object>::Dispose() */, (OperatorObserverBase_2_t1187768149 *)__this);
		IL2CPP_END_FINALLY(18)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(18)
	{
		IL2CPP_JUMP_TBL(0x19, IL_0019)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0019:
	{
		return;
	}
}
// System.Void UniRx.Operators.CombineLatestObservable`4<System.Object,System.Object,System.Object,System.Object>::.ctor(UniRx.IObservable`1<T1>,UniRx.IObservable`1<T2>,UniRx.IObservable`1<T3>,UniRx.Operators.CombineLatestFunc`4<T1,T2,T3,TR>)
extern "C"  void CombineLatestObservable_4__ctor_m3167615775_gshared (CombineLatestObservable_4_t1056168445 * __this, Il2CppObject* ___source10, Il2CppObject* ___source21, Il2CppObject* ___source32, CombineLatestFunc_4_t382088552 * ___resultSelector3, const MethodInfo* method)
{
	CombineLatestObservable_4_t1056168445 * G_B4_0 = NULL;
	CombineLatestObservable_4_t1056168445 * G_B1_0 = NULL;
	CombineLatestObservable_4_t1056168445 * G_B2_0 = NULL;
	CombineLatestObservable_4_t1056168445 * G_B3_0 = NULL;
	int32_t G_B5_0 = 0;
	CombineLatestObservable_4_t1056168445 * G_B5_1 = NULL;
	{
		Il2CppObject* L_0 = ___source10;
		bool L_1 = ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)(NULL /*static, unused*/, (Il2CppObject*)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		G_B1_0 = ((CombineLatestObservable_4_t1056168445 *)(__this));
		if (L_1)
		{
			G_B4_0 = ((CombineLatestObservable_4_t1056168445 *)(__this));
			goto IL_0025;
		}
	}
	{
		Il2CppObject* L_2 = ___source21;
		bool L_3 = ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)(NULL /*static, unused*/, (Il2CppObject*)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		G_B2_0 = ((CombineLatestObservable_4_t1056168445 *)(G_B1_0));
		if (L_3)
		{
			G_B4_0 = ((CombineLatestObservable_4_t1056168445 *)(G_B1_0));
			goto IL_0025;
		}
	}
	{
		Il2CppObject* L_4 = ___source32;
		bool L_5 = ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)(NULL /*static, unused*/, (Il2CppObject*)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		G_B3_0 = ((CombineLatestObservable_4_t1056168445 *)(G_B2_0));
		if (L_5)
		{
			G_B4_0 = ((CombineLatestObservable_4_t1056168445 *)(G_B2_0));
			goto IL_0025;
		}
	}
	{
		G_B5_0 = 0;
		G_B5_1 = ((CombineLatestObservable_4_t1056168445 *)(G_B3_0));
		goto IL_0026;
	}

IL_0025:
	{
		G_B5_0 = 1;
		G_B5_1 = ((CombineLatestObservable_4_t1056168445 *)(G_B4_0));
	}

IL_0026:
	{
		NullCheck((OperatorObservableBase_1_t4196218687 *)G_B5_1);
		((  void (*) (OperatorObservableBase_1_t4196218687 *, bool, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)((OperatorObservableBase_1_t4196218687 *)G_B5_1, (bool)G_B5_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		Il2CppObject* L_6 = ___source10;
		__this->set_source1_1(L_6);
		Il2CppObject* L_7 = ___source21;
		__this->set_source2_2(L_7);
		Il2CppObject* L_8 = ___source32;
		__this->set_source3_3(L_8);
		CombineLatestFunc_4_t382088552 * L_9 = ___resultSelector3;
		__this->set_resultSelector_4(L_9);
		return;
	}
}
// System.IDisposable UniRx.Operators.CombineLatestObservable`4<System.Object,System.Object,System.Object,System.Object>::SubscribeCore(UniRx.IObserver`1<TR>,System.IDisposable)
extern "C"  Il2CppObject * CombineLatestObservable_4_SubscribeCore_m2687735457_gshared (CombineLatestObservable_4_t1056168445 * __this, Il2CppObject* ___observer0, Il2CppObject * ___cancel1, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = ___observer0;
		Il2CppObject * L_1 = ___cancel1;
		CombineLatest_t227530385 * L_2 = (CombineLatest_t227530385 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		((  void (*) (CombineLatest_t227530385 *, int32_t, CombineLatestObservable_4_t1056168445 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6)->method)(L_2, (int32_t)3, (CombineLatestObservable_4_t1056168445 *)__this, (Il2CppObject*)L_0, (Il2CppObject *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		NullCheck((CombineLatest_t227530385 *)L_2);
		Il2CppObject * L_3 = ((  Il2CppObject * (*) (CombineLatest_t227530385 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7)->method)((CombineLatest_t227530385 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7));
		return L_3;
	}
}
// System.Void UniRx.Operators.CombineLatestObservable`5/CombineLatest<System.Boolean,System.Boolean,System.Boolean,System.Boolean,System.Object>::.ctor(System.Int32,UniRx.Operators.CombineLatestObservable`5<T1,T2,T3,T4,TR>,UniRx.IObserver`1<TR>,System.IDisposable)
extern Il2CppClass* Il2CppObject_il2cpp_TypeInfo_var;
extern const uint32_t CombineLatest__ctor_m3328766123_MetadataUsageId;
extern "C"  void CombineLatest__ctor_m3328766123_gshared (CombineLatest_t2614307975 * __this, int32_t ___length0, CombineLatestObservable_5_t3418069172 * ___parent1, Il2CppObject* ___observer2, Il2CppObject * ___cancel3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (CombineLatest__ctor_m3328766123_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject * L_0 = (Il2CppObject *)il2cpp_codegen_object_new(Il2CppObject_il2cpp_TypeInfo_var);
		Object__ctor_m1772956182(L_0, /*hidden argument*/NULL);
		__this->set_gate_7(L_0);
		int32_t L_1 = ___length0;
		Il2CppObject* L_2 = ___observer2;
		Il2CppObject * L_3 = ___cancel3;
		NullCheck((NthCombineLatestObserverBase_1_t2199482330 *)__this);
		((  void (*) (NthCombineLatestObserverBase_1_t2199482330 *, int32_t, Il2CppObject*, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((NthCombineLatestObserverBase_1_t2199482330 *)__this, (int32_t)L_1, (Il2CppObject*)L_2, (Il2CppObject *)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		CombineLatestObservable_5_t3418069172 * L_4 = ___parent1;
		__this->set_parent_6(L_4);
		return;
	}
}
// System.IDisposable UniRx.Operators.CombineLatestObservable`5/CombineLatest<System.Boolean,System.Boolean,System.Boolean,System.Boolean,System.Object>::Run()
extern "C"  Il2CppObject * CombineLatest_Run_m2447746175_gshared (CombineLatest_t2614307975 * __this, const MethodInfo* method)
{
	Il2CppObject * V_0 = NULL;
	Il2CppObject * V_1 = NULL;
	Il2CppObject * V_2 = NULL;
	Il2CppObject * V_3 = NULL;
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_gate_7();
		CombineLatestObserver_1_t3792433964 * L_1 = (CombineLatestObserver_1_t3792433964 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		((  void (*) (CombineLatestObserver_1_t3792433964 *, Il2CppObject *, Il2CppObject *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)(L_1, (Il2CppObject *)L_0, (Il2CppObject *)__this, (int32_t)0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		__this->set_c1_8(L_1);
		Il2CppObject * L_2 = (Il2CppObject *)__this->get_gate_7();
		CombineLatestObserver_1_t3792433964 * L_3 = (CombineLatestObserver_1_t3792433964 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		((  void (*) (CombineLatestObserver_1_t3792433964 *, Il2CppObject *, Il2CppObject *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6)->method)(L_3, (Il2CppObject *)L_2, (Il2CppObject *)__this, (int32_t)1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		__this->set_c2_9(L_3);
		Il2CppObject * L_4 = (Il2CppObject *)__this->get_gate_7();
		CombineLatestObserver_1_t3792433964 * L_5 = (CombineLatestObserver_1_t3792433964 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7));
		((  void (*) (CombineLatestObserver_1_t3792433964 *, Il2CppObject *, Il2CppObject *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8)->method)(L_5, (Il2CppObject *)L_4, (Il2CppObject *)__this, (int32_t)2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8));
		__this->set_c3_10(L_5);
		Il2CppObject * L_6 = (Il2CppObject *)__this->get_gate_7();
		CombineLatestObserver_1_t3792433964 * L_7 = (CombineLatestObserver_1_t3792433964 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9));
		((  void (*) (CombineLatestObserver_1_t3792433964 *, Il2CppObject *, Il2CppObject *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10)->method)(L_7, (Il2CppObject *)L_6, (Il2CppObject *)__this, (int32_t)3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10));
		__this->set_c4_11(L_7);
		CombineLatestObservable_5_t3418069172 * L_8 = (CombineLatestObservable_5_t3418069172 *)__this->get_parent_6();
		NullCheck(L_8);
		Il2CppObject* L_9 = (Il2CppObject*)L_8->get_source1_1();
		CombineLatestObserver_1_t3792433964 * L_10 = (CombineLatestObserver_1_t3792433964 *)__this->get_c1_8();
		NullCheck((Il2CppObject*)L_9);
		Il2CppObject * L_11 = InterfaceFuncInvoker1< Il2CppObject *, Il2CppObject* >::Invoke(0 /* System.IDisposable UniRx.IObservable`1<System.Boolean>::Subscribe(UniRx.IObserver`1<T>) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 11), (Il2CppObject*)L_9, (Il2CppObject*)L_10);
		V_0 = (Il2CppObject *)L_11;
		CombineLatestObservable_5_t3418069172 * L_12 = (CombineLatestObservable_5_t3418069172 *)__this->get_parent_6();
		NullCheck(L_12);
		Il2CppObject* L_13 = (Il2CppObject*)L_12->get_source2_2();
		CombineLatestObserver_1_t3792433964 * L_14 = (CombineLatestObserver_1_t3792433964 *)__this->get_c2_9();
		NullCheck((Il2CppObject*)L_13);
		Il2CppObject * L_15 = InterfaceFuncInvoker1< Il2CppObject *, Il2CppObject* >::Invoke(0 /* System.IDisposable UniRx.IObservable`1<System.Boolean>::Subscribe(UniRx.IObserver`1<T>) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 12), (Il2CppObject*)L_13, (Il2CppObject*)L_14);
		V_1 = (Il2CppObject *)L_15;
		CombineLatestObservable_5_t3418069172 * L_16 = (CombineLatestObservable_5_t3418069172 *)__this->get_parent_6();
		NullCheck(L_16);
		Il2CppObject* L_17 = (Il2CppObject*)L_16->get_source3_3();
		CombineLatestObserver_1_t3792433964 * L_18 = (CombineLatestObserver_1_t3792433964 *)__this->get_c3_10();
		NullCheck((Il2CppObject*)L_17);
		Il2CppObject * L_19 = InterfaceFuncInvoker1< Il2CppObject *, Il2CppObject* >::Invoke(0 /* System.IDisposable UniRx.IObservable`1<System.Boolean>::Subscribe(UniRx.IObserver`1<T>) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 13), (Il2CppObject*)L_17, (Il2CppObject*)L_18);
		V_2 = (Il2CppObject *)L_19;
		CombineLatestObservable_5_t3418069172 * L_20 = (CombineLatestObservable_5_t3418069172 *)__this->get_parent_6();
		NullCheck(L_20);
		Il2CppObject* L_21 = (Il2CppObject*)L_20->get_source4_4();
		CombineLatestObserver_1_t3792433964 * L_22 = (CombineLatestObserver_1_t3792433964 *)__this->get_c4_11();
		NullCheck((Il2CppObject*)L_21);
		Il2CppObject * L_23 = InterfaceFuncInvoker1< Il2CppObject *, Il2CppObject* >::Invoke(0 /* System.IDisposable UniRx.IObservable`1<System.Boolean>::Subscribe(UniRx.IObserver`1<T>) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 14), (Il2CppObject*)L_21, (Il2CppObject*)L_22);
		V_3 = (Il2CppObject *)L_23;
		Il2CppObject * L_24 = V_0;
		Il2CppObject * L_25 = V_1;
		Il2CppObject * L_26 = V_2;
		Il2CppObject * L_27 = V_3;
		Il2CppObject * L_28 = StableCompositeDisposable_Create_m232885879(NULL /*static, unused*/, (Il2CppObject *)L_24, (Il2CppObject *)L_25, (Il2CppObject *)L_26, (Il2CppObject *)L_27, /*hidden argument*/NULL);
		return L_28;
	}
}
// TR UniRx.Operators.CombineLatestObservable`5/CombineLatest<System.Boolean,System.Boolean,System.Boolean,System.Boolean,System.Object>::GetResult()
extern "C"  Il2CppObject * CombineLatest_GetResult_m3142059995_gshared (CombineLatest_t2614307975 * __this, const MethodInfo* method)
{
	{
		CombineLatestObservable_5_t3418069172 * L_0 = (CombineLatestObservable_5_t3418069172 *)__this->get_parent_6();
		NullCheck(L_0);
		CombineLatestFunc_5_t2231862077 * L_1 = (CombineLatestFunc_5_t2231862077 *)L_0->get_resultSelector_5();
		CombineLatestObserver_1_t3792433964 * L_2 = (CombineLatestObserver_1_t3792433964 *)__this->get_c1_8();
		NullCheck((CombineLatestObserver_1_t3792433964 *)L_2);
		bool L_3 = ((  bool (*) (CombineLatestObserver_1_t3792433964 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 15)->method)((CombineLatestObserver_1_t3792433964 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 15));
		CombineLatestObserver_1_t3792433964 * L_4 = (CombineLatestObserver_1_t3792433964 *)__this->get_c2_9();
		NullCheck((CombineLatestObserver_1_t3792433964 *)L_4);
		bool L_5 = ((  bool (*) (CombineLatestObserver_1_t3792433964 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 16)->method)((CombineLatestObserver_1_t3792433964 *)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 16));
		CombineLatestObserver_1_t3792433964 * L_6 = (CombineLatestObserver_1_t3792433964 *)__this->get_c3_10();
		NullCheck((CombineLatestObserver_1_t3792433964 *)L_6);
		bool L_7 = ((  bool (*) (CombineLatestObserver_1_t3792433964 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 17)->method)((CombineLatestObserver_1_t3792433964 *)L_6, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 17));
		CombineLatestObserver_1_t3792433964 * L_8 = (CombineLatestObserver_1_t3792433964 *)__this->get_c4_11();
		NullCheck((CombineLatestObserver_1_t3792433964 *)L_8);
		bool L_9 = ((  bool (*) (CombineLatestObserver_1_t3792433964 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 18)->method)((CombineLatestObserver_1_t3792433964 *)L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 18));
		NullCheck((CombineLatestFunc_5_t2231862077 *)L_1);
		Il2CppObject * L_10 = ((  Il2CppObject * (*) (CombineLatestFunc_5_t2231862077 *, bool, bool, bool, bool, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 19)->method)((CombineLatestFunc_5_t2231862077 *)L_1, (bool)L_3, (bool)L_5, (bool)L_7, (bool)L_9, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 19));
		return L_10;
	}
}
// System.Void UniRx.Operators.CombineLatestObservable`5/CombineLatest<System.Boolean,System.Boolean,System.Boolean,System.Boolean,System.Object>::OnNext(TR)
extern "C"  void CombineLatest_OnNext_m220650727_gshared (CombineLatest_t2614307975 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = (Il2CppObject*)((OperatorObserverBase_2_t1187768149 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		Il2CppObject * L_1 = ___value0;
		NullCheck((Il2CppObject*)L_0);
		InterfaceActionInvoker1< Il2CppObject * >::Invoke(2 /* System.Void UniRx.IObserver`1<System.Object>::OnNext(T) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 20), (Il2CppObject*)L_0, (Il2CppObject *)L_1);
		return;
	}
}
// System.Void UniRx.Operators.CombineLatestObservable`5/CombineLatest<System.Boolean,System.Boolean,System.Boolean,System.Boolean,System.Object>::OnError(System.Exception)
extern "C"  void CombineLatest_OnError_m119004264_gshared (CombineLatest_t2614307975 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method)
{
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		Il2CppObject* L_0 = (Il2CppObject*)((OperatorObserverBase_2_t1187768149 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		Exception_t1967233988 * L_1 = ___error0;
		NullCheck((Il2CppObject*)L_0);
		InterfaceActionInvoker1< Exception_t1967233988 * >::Invoke(1 /* System.Void UniRx.IObserver`1<System.Object>::OnError(System.Exception) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 20), (Il2CppObject*)L_0, (Exception_t1967233988 *)L_1);
		IL2CPP_LEAVE(0x1A, FINALLY_0013);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0013;
	}

FINALLY_0013:
	{ // begin finally (depth: 1)
		NullCheck((OperatorObserverBase_2_t1187768149 *)__this);
		VirtActionInvoker0::Invoke(4 /* System.Void UniRx.Operators.OperatorObserverBase`2<System.Object,System.Object>::Dispose() */, (OperatorObserverBase_2_t1187768149 *)__this);
		IL2CPP_END_FINALLY(19)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(19)
	{
		IL2CPP_JUMP_TBL(0x1A, IL_001a)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_001a:
	{
		return;
	}
}
// System.Void UniRx.Operators.CombineLatestObservable`5/CombineLatest<System.Boolean,System.Boolean,System.Boolean,System.Boolean,System.Object>::OnCompleted()
extern "C"  void CombineLatest_OnCompleted_m3493850043_gshared (CombineLatest_t2614307975 * __this, const MethodInfo* method)
{
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		Il2CppObject* L_0 = (Il2CppObject*)((OperatorObserverBase_2_t1187768149 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		NullCheck((Il2CppObject*)L_0);
		InterfaceActionInvoker0::Invoke(0 /* System.Void UniRx.IObserver`1<System.Object>::OnCompleted() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 20), (Il2CppObject*)L_0);
		IL2CPP_LEAVE(0x19, FINALLY_0012);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0012;
	}

FINALLY_0012:
	{ // begin finally (depth: 1)
		NullCheck((OperatorObserverBase_2_t1187768149 *)__this);
		VirtActionInvoker0::Invoke(4 /* System.Void UniRx.Operators.OperatorObserverBase`2<System.Object,System.Object>::Dispose() */, (OperatorObserverBase_2_t1187768149 *)__this);
		IL2CPP_END_FINALLY(18)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(18)
	{
		IL2CPP_JUMP_TBL(0x19, IL_0019)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0019:
	{
		return;
	}
}
// System.Void UniRx.Operators.CombineLatestObservable`5/CombineLatest<System.Object,System.Object,System.Object,System.Object,System.Object>::.ctor(System.Int32,UniRx.Operators.CombineLatestObservable`5<T1,T2,T3,T4,TR>,UniRx.IObserver`1<TR>,System.IDisposable)
extern Il2CppClass* Il2CppObject_il2cpp_TypeInfo_var;
extern const uint32_t CombineLatest__ctor_m1293158819_MetadataUsageId;
extern "C"  void CombineLatest__ctor_m1293158819_gshared (CombineLatest_t928221951 * __this, int32_t ___length0, CombineLatestObservable_5_t1731983148 * ___parent1, Il2CppObject* ___observer2, Il2CppObject * ___cancel3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (CombineLatest__ctor_m1293158819_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject * L_0 = (Il2CppObject *)il2cpp_codegen_object_new(Il2CppObject_il2cpp_TypeInfo_var);
		Object__ctor_m1772956182(L_0, /*hidden argument*/NULL);
		__this->set_gate_7(L_0);
		int32_t L_1 = ___length0;
		Il2CppObject* L_2 = ___observer2;
		Il2CppObject * L_3 = ___cancel3;
		NullCheck((NthCombineLatestObserverBase_1_t2199482330 *)__this);
		((  void (*) (NthCombineLatestObserverBase_1_t2199482330 *, int32_t, Il2CppObject*, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((NthCombineLatestObserverBase_1_t2199482330 *)__this, (int32_t)L_1, (Il2CppObject*)L_2, (Il2CppObject *)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		CombineLatestObservable_5_t1731983148 * L_4 = ___parent1;
		__this->set_parent_6(L_4);
		return;
	}
}
// System.IDisposable UniRx.Operators.CombineLatestObservable`5/CombineLatest<System.Object,System.Object,System.Object,System.Object,System.Object>::Run()
extern "C"  Il2CppObject * CombineLatest_Run_m1833850631_gshared (CombineLatest_t928221951 * __this, const MethodInfo* method)
{
	Il2CppObject * V_0 = NULL;
	Il2CppObject * V_1 = NULL;
	Il2CppObject * V_2 = NULL;
	Il2CppObject * V_3 = NULL;
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_gate_7();
		CombineLatestObserver_1_t123567747 * L_1 = (CombineLatestObserver_1_t123567747 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		((  void (*) (CombineLatestObserver_1_t123567747 *, Il2CppObject *, Il2CppObject *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)(L_1, (Il2CppObject *)L_0, (Il2CppObject *)__this, (int32_t)0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		__this->set_c1_8(L_1);
		Il2CppObject * L_2 = (Il2CppObject *)__this->get_gate_7();
		CombineLatestObserver_1_t123567747 * L_3 = (CombineLatestObserver_1_t123567747 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		((  void (*) (CombineLatestObserver_1_t123567747 *, Il2CppObject *, Il2CppObject *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6)->method)(L_3, (Il2CppObject *)L_2, (Il2CppObject *)__this, (int32_t)1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		__this->set_c2_9(L_3);
		Il2CppObject * L_4 = (Il2CppObject *)__this->get_gate_7();
		CombineLatestObserver_1_t123567747 * L_5 = (CombineLatestObserver_1_t123567747 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7));
		((  void (*) (CombineLatestObserver_1_t123567747 *, Il2CppObject *, Il2CppObject *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8)->method)(L_5, (Il2CppObject *)L_4, (Il2CppObject *)__this, (int32_t)2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8));
		__this->set_c3_10(L_5);
		Il2CppObject * L_6 = (Il2CppObject *)__this->get_gate_7();
		CombineLatestObserver_1_t123567747 * L_7 = (CombineLatestObserver_1_t123567747 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9));
		((  void (*) (CombineLatestObserver_1_t123567747 *, Il2CppObject *, Il2CppObject *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10)->method)(L_7, (Il2CppObject *)L_6, (Il2CppObject *)__this, (int32_t)3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10));
		__this->set_c4_11(L_7);
		CombineLatestObservable_5_t1731983148 * L_8 = (CombineLatestObservable_5_t1731983148 *)__this->get_parent_6();
		NullCheck(L_8);
		Il2CppObject* L_9 = (Il2CppObject*)L_8->get_source1_1();
		CombineLatestObserver_1_t123567747 * L_10 = (CombineLatestObserver_1_t123567747 *)__this->get_c1_8();
		NullCheck((Il2CppObject*)L_9);
		Il2CppObject * L_11 = InterfaceFuncInvoker1< Il2CppObject *, Il2CppObject* >::Invoke(0 /* System.IDisposable UniRx.IObservable`1<System.Object>::Subscribe(UniRx.IObserver`1<T>) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 11), (Il2CppObject*)L_9, (Il2CppObject*)L_10);
		V_0 = (Il2CppObject *)L_11;
		CombineLatestObservable_5_t1731983148 * L_12 = (CombineLatestObservable_5_t1731983148 *)__this->get_parent_6();
		NullCheck(L_12);
		Il2CppObject* L_13 = (Il2CppObject*)L_12->get_source2_2();
		CombineLatestObserver_1_t123567747 * L_14 = (CombineLatestObserver_1_t123567747 *)__this->get_c2_9();
		NullCheck((Il2CppObject*)L_13);
		Il2CppObject * L_15 = InterfaceFuncInvoker1< Il2CppObject *, Il2CppObject* >::Invoke(0 /* System.IDisposable UniRx.IObservable`1<System.Object>::Subscribe(UniRx.IObserver`1<T>) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 12), (Il2CppObject*)L_13, (Il2CppObject*)L_14);
		V_1 = (Il2CppObject *)L_15;
		CombineLatestObservable_5_t1731983148 * L_16 = (CombineLatestObservable_5_t1731983148 *)__this->get_parent_6();
		NullCheck(L_16);
		Il2CppObject* L_17 = (Il2CppObject*)L_16->get_source3_3();
		CombineLatestObserver_1_t123567747 * L_18 = (CombineLatestObserver_1_t123567747 *)__this->get_c3_10();
		NullCheck((Il2CppObject*)L_17);
		Il2CppObject * L_19 = InterfaceFuncInvoker1< Il2CppObject *, Il2CppObject* >::Invoke(0 /* System.IDisposable UniRx.IObservable`1<System.Object>::Subscribe(UniRx.IObserver`1<T>) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 13), (Il2CppObject*)L_17, (Il2CppObject*)L_18);
		V_2 = (Il2CppObject *)L_19;
		CombineLatestObservable_5_t1731983148 * L_20 = (CombineLatestObservable_5_t1731983148 *)__this->get_parent_6();
		NullCheck(L_20);
		Il2CppObject* L_21 = (Il2CppObject*)L_20->get_source4_4();
		CombineLatestObserver_1_t123567747 * L_22 = (CombineLatestObserver_1_t123567747 *)__this->get_c4_11();
		NullCheck((Il2CppObject*)L_21);
		Il2CppObject * L_23 = InterfaceFuncInvoker1< Il2CppObject *, Il2CppObject* >::Invoke(0 /* System.IDisposable UniRx.IObservable`1<System.Object>::Subscribe(UniRx.IObserver`1<T>) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 14), (Il2CppObject*)L_21, (Il2CppObject*)L_22);
		V_3 = (Il2CppObject *)L_23;
		Il2CppObject * L_24 = V_0;
		Il2CppObject * L_25 = V_1;
		Il2CppObject * L_26 = V_2;
		Il2CppObject * L_27 = V_3;
		Il2CppObject * L_28 = StableCompositeDisposable_Create_m232885879(NULL /*static, unused*/, (Il2CppObject *)L_24, (Il2CppObject *)L_25, (Il2CppObject *)L_26, (Il2CppObject *)L_27, /*hidden argument*/NULL);
		return L_28;
	}
}
// TR UniRx.Operators.CombineLatestObservable`5/CombineLatest<System.Object,System.Object,System.Object,System.Object,System.Object>::GetResult()
extern "C"  Il2CppObject * CombineLatest_GetResult_m3403243107_gshared (CombineLatest_t928221951 * __this, const MethodInfo* method)
{
	{
		CombineLatestObservable_5_t1731983148 * L_0 = (CombineLatestObservable_5_t1731983148 *)__this->get_parent_6();
		NullCheck(L_0);
		CombineLatestFunc_5_t545776053 * L_1 = (CombineLatestFunc_5_t545776053 *)L_0->get_resultSelector_5();
		CombineLatestObserver_1_t123567747 * L_2 = (CombineLatestObserver_1_t123567747 *)__this->get_c1_8();
		NullCheck((CombineLatestObserver_1_t123567747 *)L_2);
		Il2CppObject * L_3 = ((  Il2CppObject * (*) (CombineLatestObserver_1_t123567747 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 15)->method)((CombineLatestObserver_1_t123567747 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 15));
		CombineLatestObserver_1_t123567747 * L_4 = (CombineLatestObserver_1_t123567747 *)__this->get_c2_9();
		NullCheck((CombineLatestObserver_1_t123567747 *)L_4);
		Il2CppObject * L_5 = ((  Il2CppObject * (*) (CombineLatestObserver_1_t123567747 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 16)->method)((CombineLatestObserver_1_t123567747 *)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 16));
		CombineLatestObserver_1_t123567747 * L_6 = (CombineLatestObserver_1_t123567747 *)__this->get_c3_10();
		NullCheck((CombineLatestObserver_1_t123567747 *)L_6);
		Il2CppObject * L_7 = ((  Il2CppObject * (*) (CombineLatestObserver_1_t123567747 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 17)->method)((CombineLatestObserver_1_t123567747 *)L_6, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 17));
		CombineLatestObserver_1_t123567747 * L_8 = (CombineLatestObserver_1_t123567747 *)__this->get_c4_11();
		NullCheck((CombineLatestObserver_1_t123567747 *)L_8);
		Il2CppObject * L_9 = ((  Il2CppObject * (*) (CombineLatestObserver_1_t123567747 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 18)->method)((CombineLatestObserver_1_t123567747 *)L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 18));
		NullCheck((CombineLatestFunc_5_t545776053 *)L_1);
		Il2CppObject * L_10 = ((  Il2CppObject * (*) (CombineLatestFunc_5_t545776053 *, Il2CppObject *, Il2CppObject *, Il2CppObject *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 19)->method)((CombineLatestFunc_5_t545776053 *)L_1, (Il2CppObject *)L_3, (Il2CppObject *)L_5, (Il2CppObject *)L_7, (Il2CppObject *)L_9, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 19));
		return L_10;
	}
}
// System.Void UniRx.Operators.CombineLatestObservable`5/CombineLatest<System.Object,System.Object,System.Object,System.Object,System.Object>::OnNext(TR)
extern "C"  void CombineLatest_OnNext_m759908831_gshared (CombineLatest_t928221951 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = (Il2CppObject*)((OperatorObserverBase_2_t1187768149 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		Il2CppObject * L_1 = ___value0;
		NullCheck((Il2CppObject*)L_0);
		InterfaceActionInvoker1< Il2CppObject * >::Invoke(2 /* System.Void UniRx.IObserver`1<System.Object>::OnNext(T) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 20), (Il2CppObject*)L_0, (Il2CppObject *)L_1);
		return;
	}
}
// System.Void UniRx.Operators.CombineLatestObservable`5/CombineLatest<System.Object,System.Object,System.Object,System.Object,System.Object>::OnError(System.Exception)
extern "C"  void CombineLatest_OnError_m693952624_gshared (CombineLatest_t928221951 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method)
{
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		Il2CppObject* L_0 = (Il2CppObject*)((OperatorObserverBase_2_t1187768149 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		Exception_t1967233988 * L_1 = ___error0;
		NullCheck((Il2CppObject*)L_0);
		InterfaceActionInvoker1< Exception_t1967233988 * >::Invoke(1 /* System.Void UniRx.IObserver`1<System.Object>::OnError(System.Exception) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 20), (Il2CppObject*)L_0, (Exception_t1967233988 *)L_1);
		IL2CPP_LEAVE(0x1A, FINALLY_0013);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0013;
	}

FINALLY_0013:
	{ // begin finally (depth: 1)
		NullCheck((OperatorObserverBase_2_t1187768149 *)__this);
		VirtActionInvoker0::Invoke(4 /* System.Void UniRx.Operators.OperatorObserverBase`2<System.Object,System.Object>::Dispose() */, (OperatorObserverBase_2_t1187768149 *)__this);
		IL2CPP_END_FINALLY(19)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(19)
	{
		IL2CPP_JUMP_TBL(0x1A, IL_001a)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_001a:
	{
		return;
	}
}
// System.Void UniRx.Operators.CombineLatestObservable`5/CombineLatest<System.Object,System.Object,System.Object,System.Object,System.Object>::OnCompleted()
extern "C"  void CombineLatest_OnCompleted_m1059371971_gshared (CombineLatest_t928221951 * __this, const MethodInfo* method)
{
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		Il2CppObject* L_0 = (Il2CppObject*)((OperatorObserverBase_2_t1187768149 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		NullCheck((Il2CppObject*)L_0);
		InterfaceActionInvoker0::Invoke(0 /* System.Void UniRx.IObserver`1<System.Object>::OnCompleted() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 20), (Il2CppObject*)L_0);
		IL2CPP_LEAVE(0x19, FINALLY_0012);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0012;
	}

FINALLY_0012:
	{ // begin finally (depth: 1)
		NullCheck((OperatorObserverBase_2_t1187768149 *)__this);
		VirtActionInvoker0::Invoke(4 /* System.Void UniRx.Operators.OperatorObserverBase`2<System.Object,System.Object>::Dispose() */, (OperatorObserverBase_2_t1187768149 *)__this);
		IL2CPP_END_FINALLY(18)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(18)
	{
		IL2CPP_JUMP_TBL(0x19, IL_0019)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0019:
	{
		return;
	}
}
// System.Void UniRx.Operators.CombineLatestObservable`5<System.Boolean,System.Boolean,System.Boolean,System.Boolean,System.Object>::.ctor(UniRx.IObservable`1<T1>,UniRx.IObservable`1<T2>,UniRx.IObservable`1<T3>,UniRx.IObservable`1<T4>,UniRx.Operators.CombineLatestFunc`5<T1,T2,T3,T4,TR>)
extern "C"  void CombineLatestObservable_5__ctor_m1765463864_gshared (CombineLatestObservable_5_t3418069172 * __this, Il2CppObject* ___source10, Il2CppObject* ___source21, Il2CppObject* ___source32, Il2CppObject* ___source43, CombineLatestFunc_5_t2231862077 * ___resultSelector4, const MethodInfo* method)
{
	CombineLatestObservable_5_t3418069172 * G_B5_0 = NULL;
	CombineLatestObservable_5_t3418069172 * G_B1_0 = NULL;
	CombineLatestObservable_5_t3418069172 * G_B2_0 = NULL;
	CombineLatestObservable_5_t3418069172 * G_B3_0 = NULL;
	CombineLatestObservable_5_t3418069172 * G_B4_0 = NULL;
	int32_t G_B6_0 = 0;
	CombineLatestObservable_5_t3418069172 * G_B6_1 = NULL;
	{
		Il2CppObject* L_0 = ___source10;
		bool L_1 = ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)(NULL /*static, unused*/, (Il2CppObject*)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		G_B1_0 = ((CombineLatestObservable_5_t3418069172 *)(__this));
		if (L_1)
		{
			G_B5_0 = ((CombineLatestObservable_5_t3418069172 *)(__this));
			goto IL_0031;
		}
	}
	{
		Il2CppObject* L_2 = ___source21;
		bool L_3 = ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)(NULL /*static, unused*/, (Il2CppObject*)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		G_B2_0 = ((CombineLatestObservable_5_t3418069172 *)(G_B1_0));
		if (L_3)
		{
			G_B5_0 = ((CombineLatestObservable_5_t3418069172 *)(G_B1_0));
			goto IL_0031;
		}
	}
	{
		Il2CppObject* L_4 = ___source32;
		bool L_5 = ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)(NULL /*static, unused*/, (Il2CppObject*)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		G_B3_0 = ((CombineLatestObservable_5_t3418069172 *)(G_B2_0));
		if (L_5)
		{
			G_B5_0 = ((CombineLatestObservable_5_t3418069172 *)(G_B2_0));
			goto IL_0031;
		}
	}
	{
		Il2CppObject* L_6 = ___source43;
		bool L_7 = ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)(NULL /*static, unused*/, (Il2CppObject*)L_6, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		G_B4_0 = ((CombineLatestObservable_5_t3418069172 *)(G_B3_0));
		if (L_7)
		{
			G_B5_0 = ((CombineLatestObservable_5_t3418069172 *)(G_B3_0));
			goto IL_0031;
		}
	}
	{
		G_B6_0 = 0;
		G_B6_1 = ((CombineLatestObservable_5_t3418069172 *)(G_B4_0));
		goto IL_0032;
	}

IL_0031:
	{
		G_B6_0 = 1;
		G_B6_1 = ((CombineLatestObservable_5_t3418069172 *)(G_B5_0));
	}

IL_0032:
	{
		NullCheck((OperatorObservableBase_1_t4196218687 *)G_B6_1);
		((  void (*) (OperatorObservableBase_1_t4196218687 *, bool, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)((OperatorObservableBase_1_t4196218687 *)G_B6_1, (bool)G_B6_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		Il2CppObject* L_8 = ___source10;
		__this->set_source1_1(L_8);
		Il2CppObject* L_9 = ___source21;
		__this->set_source2_2(L_9);
		Il2CppObject* L_10 = ___source32;
		__this->set_source3_3(L_10);
		Il2CppObject* L_11 = ___source43;
		__this->set_source4_4(L_11);
		CombineLatestFunc_5_t2231862077 * L_12 = ___resultSelector4;
		__this->set_resultSelector_5(L_12);
		return;
	}
}
// System.IDisposable UniRx.Operators.CombineLatestObservable`5<System.Boolean,System.Boolean,System.Boolean,System.Boolean,System.Object>::SubscribeCore(UniRx.IObserver`1<TR>,System.IDisposable)
extern "C"  Il2CppObject * CombineLatestObservable_5_SubscribeCore_m4118138892_gshared (CombineLatestObservable_5_t3418069172 * __this, Il2CppObject* ___observer0, Il2CppObject * ___cancel1, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = ___observer0;
		Il2CppObject * L_1 = ___cancel1;
		CombineLatest_t2614307975 * L_2 = (CombineLatest_t2614307975 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		((  void (*) (CombineLatest_t2614307975 *, int32_t, CombineLatestObservable_5_t3418069172 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7)->method)(L_2, (int32_t)4, (CombineLatestObservable_5_t3418069172 *)__this, (Il2CppObject*)L_0, (Il2CppObject *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7));
		NullCheck((CombineLatest_t2614307975 *)L_2);
		Il2CppObject * L_3 = ((  Il2CppObject * (*) (CombineLatest_t2614307975 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8)->method)((CombineLatest_t2614307975 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8));
		return L_3;
	}
}
// System.Void UniRx.Operators.CombineLatestObservable`5<System.Object,System.Object,System.Object,System.Object,System.Object>::.ctor(UniRx.IObservable`1<T1>,UniRx.IObservable`1<T2>,UniRx.IObservable`1<T3>,UniRx.IObservable`1<T4>,UniRx.Operators.CombineLatestFunc`5<T1,T2,T3,T4,TR>)
extern "C"  void CombineLatestObservable_5__ctor_m3985938352_gshared (CombineLatestObservable_5_t1731983148 * __this, Il2CppObject* ___source10, Il2CppObject* ___source21, Il2CppObject* ___source32, Il2CppObject* ___source43, CombineLatestFunc_5_t545776053 * ___resultSelector4, const MethodInfo* method)
{
	CombineLatestObservable_5_t1731983148 * G_B5_0 = NULL;
	CombineLatestObservable_5_t1731983148 * G_B1_0 = NULL;
	CombineLatestObservable_5_t1731983148 * G_B2_0 = NULL;
	CombineLatestObservable_5_t1731983148 * G_B3_0 = NULL;
	CombineLatestObservable_5_t1731983148 * G_B4_0 = NULL;
	int32_t G_B6_0 = 0;
	CombineLatestObservable_5_t1731983148 * G_B6_1 = NULL;
	{
		Il2CppObject* L_0 = ___source10;
		bool L_1 = ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)(NULL /*static, unused*/, (Il2CppObject*)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		G_B1_0 = ((CombineLatestObservable_5_t1731983148 *)(__this));
		if (L_1)
		{
			G_B5_0 = ((CombineLatestObservable_5_t1731983148 *)(__this));
			goto IL_0031;
		}
	}
	{
		Il2CppObject* L_2 = ___source21;
		bool L_3 = ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)(NULL /*static, unused*/, (Il2CppObject*)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		G_B2_0 = ((CombineLatestObservable_5_t1731983148 *)(G_B1_0));
		if (L_3)
		{
			G_B5_0 = ((CombineLatestObservable_5_t1731983148 *)(G_B1_0));
			goto IL_0031;
		}
	}
	{
		Il2CppObject* L_4 = ___source32;
		bool L_5 = ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)(NULL /*static, unused*/, (Il2CppObject*)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		G_B3_0 = ((CombineLatestObservable_5_t1731983148 *)(G_B2_0));
		if (L_5)
		{
			G_B5_0 = ((CombineLatestObservable_5_t1731983148 *)(G_B2_0));
			goto IL_0031;
		}
	}
	{
		Il2CppObject* L_6 = ___source43;
		bool L_7 = ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)(NULL /*static, unused*/, (Il2CppObject*)L_6, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		G_B4_0 = ((CombineLatestObservable_5_t1731983148 *)(G_B3_0));
		if (L_7)
		{
			G_B5_0 = ((CombineLatestObservable_5_t1731983148 *)(G_B3_0));
			goto IL_0031;
		}
	}
	{
		G_B6_0 = 0;
		G_B6_1 = ((CombineLatestObservable_5_t1731983148 *)(G_B4_0));
		goto IL_0032;
	}

IL_0031:
	{
		G_B6_0 = 1;
		G_B6_1 = ((CombineLatestObservable_5_t1731983148 *)(G_B5_0));
	}

IL_0032:
	{
		NullCheck((OperatorObservableBase_1_t4196218687 *)G_B6_1);
		((  void (*) (OperatorObservableBase_1_t4196218687 *, bool, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)((OperatorObservableBase_1_t4196218687 *)G_B6_1, (bool)G_B6_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		Il2CppObject* L_8 = ___source10;
		__this->set_source1_1(L_8);
		Il2CppObject* L_9 = ___source21;
		__this->set_source2_2(L_9);
		Il2CppObject* L_10 = ___source32;
		__this->set_source3_3(L_10);
		Il2CppObject* L_11 = ___source43;
		__this->set_source4_4(L_11);
		CombineLatestFunc_5_t545776053 * L_12 = ___resultSelector4;
		__this->set_resultSelector_5(L_12);
		return;
	}
}
// System.IDisposable UniRx.Operators.CombineLatestObservable`5<System.Object,System.Object,System.Object,System.Object,System.Object>::SubscribeCore(UniRx.IObserver`1<TR>,System.IDisposable)
extern "C"  Il2CppObject * CombineLatestObservable_5_SubscribeCore_m1504255252_gshared (CombineLatestObservable_5_t1731983148 * __this, Il2CppObject* ___observer0, Il2CppObject * ___cancel1, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = ___observer0;
		Il2CppObject * L_1 = ___cancel1;
		CombineLatest_t928221951 * L_2 = (CombineLatest_t928221951 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		((  void (*) (CombineLatest_t928221951 *, int32_t, CombineLatestObservable_5_t1731983148 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7)->method)(L_2, (int32_t)4, (CombineLatestObservable_5_t1731983148 *)__this, (Il2CppObject*)L_0, (Il2CppObject *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7));
		NullCheck((CombineLatest_t928221951 *)L_2);
		Il2CppObject * L_3 = ((  Il2CppObject * (*) (CombineLatest_t928221951 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8)->method)((CombineLatest_t928221951 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8));
		return L_3;
	}
}
// System.Void UniRx.Operators.CombineLatestObservable`6/CombineLatest<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>::.ctor(System.Int32,UniRx.Operators.CombineLatestObservable`6<T1,T2,T3,T4,T5,TR>,UniRx.IObserver`1<TR>,System.IDisposable)
extern Il2CppClass* Il2CppObject_il2cpp_TypeInfo_var;
extern const uint32_t CombineLatest__ctor_m1870520426_MetadataUsageId;
extern "C"  void CombineLatest__ctor_m1870520426_gshared (CombineLatest_t89686297 * __this, int32_t ___length0, CombineLatestObservable_6_t4095141259 * ___parent1, Il2CppObject* ___observer2, Il2CppObject * ___cancel3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (CombineLatest__ctor_m1870520426_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject * L_0 = (Il2CppObject *)il2cpp_codegen_object_new(Il2CppObject_il2cpp_TypeInfo_var);
		Object__ctor_m1772956182(L_0, /*hidden argument*/NULL);
		__this->set_gate_7(L_0);
		int32_t L_1 = ___length0;
		Il2CppObject* L_2 = ___observer2;
		Il2CppObject * L_3 = ___cancel3;
		NullCheck((NthCombineLatestObserverBase_1_t2199482330 *)__this);
		((  void (*) (NthCombineLatestObserverBase_1_t2199482330 *, int32_t, Il2CppObject*, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((NthCombineLatestObserverBase_1_t2199482330 *)__this, (int32_t)L_1, (Il2CppObject*)L_2, (Il2CppObject *)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		CombineLatestObservable_6_t4095141259 * L_4 = ___parent1;
		__this->set_parent_6(L_4);
		return;
	}
}
// System.IDisposable UniRx.Operators.CombineLatestObservable`6/CombineLatest<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>::Run()
extern Il2CppClass* IDisposableU5BU5D_t165183403_il2cpp_TypeInfo_var;
extern const uint32_t CombineLatest_Run_m2997404474_MetadataUsageId;
extern "C"  Il2CppObject * CombineLatest_Run_m2997404474_gshared (CombineLatest_t89686297 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (CombineLatest_Run_m2997404474_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Il2CppObject * V_0 = NULL;
	Il2CppObject * V_1 = NULL;
	Il2CppObject * V_2 = NULL;
	Il2CppObject * V_3 = NULL;
	Il2CppObject * V_4 = NULL;
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_gate_7();
		CombineLatestObserver_1_t123567747 * L_1 = (CombineLatestObserver_1_t123567747 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		((  void (*) (CombineLatestObserver_1_t123567747 *, Il2CppObject *, Il2CppObject *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)(L_1, (Il2CppObject *)L_0, (Il2CppObject *)__this, (int32_t)0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		__this->set_c1_8(L_1);
		Il2CppObject * L_2 = (Il2CppObject *)__this->get_gate_7();
		CombineLatestObserver_1_t123567747 * L_3 = (CombineLatestObserver_1_t123567747 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		((  void (*) (CombineLatestObserver_1_t123567747 *, Il2CppObject *, Il2CppObject *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6)->method)(L_3, (Il2CppObject *)L_2, (Il2CppObject *)__this, (int32_t)1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		__this->set_c2_9(L_3);
		Il2CppObject * L_4 = (Il2CppObject *)__this->get_gate_7();
		CombineLatestObserver_1_t123567747 * L_5 = (CombineLatestObserver_1_t123567747 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7));
		((  void (*) (CombineLatestObserver_1_t123567747 *, Il2CppObject *, Il2CppObject *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8)->method)(L_5, (Il2CppObject *)L_4, (Il2CppObject *)__this, (int32_t)2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8));
		__this->set_c3_10(L_5);
		Il2CppObject * L_6 = (Il2CppObject *)__this->get_gate_7();
		CombineLatestObserver_1_t123567747 * L_7 = (CombineLatestObserver_1_t123567747 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9));
		((  void (*) (CombineLatestObserver_1_t123567747 *, Il2CppObject *, Il2CppObject *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10)->method)(L_7, (Il2CppObject *)L_6, (Il2CppObject *)__this, (int32_t)3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10));
		__this->set_c4_11(L_7);
		Il2CppObject * L_8 = (Il2CppObject *)__this->get_gate_7();
		CombineLatestObserver_1_t123567747 * L_9 = (CombineLatestObserver_1_t123567747 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 11));
		((  void (*) (CombineLatestObserver_1_t123567747 *, Il2CppObject *, Il2CppObject *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 12)->method)(L_9, (Il2CppObject *)L_8, (Il2CppObject *)__this, (int32_t)4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 12));
		__this->set_c5_12(L_9);
		CombineLatestObservable_6_t4095141259 * L_10 = (CombineLatestObservable_6_t4095141259 *)__this->get_parent_6();
		NullCheck(L_10);
		Il2CppObject* L_11 = (Il2CppObject*)L_10->get_source1_1();
		CombineLatestObserver_1_t123567747 * L_12 = (CombineLatestObserver_1_t123567747 *)__this->get_c1_8();
		NullCheck((Il2CppObject*)L_11);
		Il2CppObject * L_13 = InterfaceFuncInvoker1< Il2CppObject *, Il2CppObject* >::Invoke(0 /* System.IDisposable UniRx.IObservable`1<System.Object>::Subscribe(UniRx.IObserver`1<T>) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 13), (Il2CppObject*)L_11, (Il2CppObject*)L_12);
		V_0 = (Il2CppObject *)L_13;
		CombineLatestObservable_6_t4095141259 * L_14 = (CombineLatestObservable_6_t4095141259 *)__this->get_parent_6();
		NullCheck(L_14);
		Il2CppObject* L_15 = (Il2CppObject*)L_14->get_source2_2();
		CombineLatestObserver_1_t123567747 * L_16 = (CombineLatestObserver_1_t123567747 *)__this->get_c2_9();
		NullCheck((Il2CppObject*)L_15);
		Il2CppObject * L_17 = InterfaceFuncInvoker1< Il2CppObject *, Il2CppObject* >::Invoke(0 /* System.IDisposable UniRx.IObservable`1<System.Object>::Subscribe(UniRx.IObserver`1<T>) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 14), (Il2CppObject*)L_15, (Il2CppObject*)L_16);
		V_1 = (Il2CppObject *)L_17;
		CombineLatestObservable_6_t4095141259 * L_18 = (CombineLatestObservable_6_t4095141259 *)__this->get_parent_6();
		NullCheck(L_18);
		Il2CppObject* L_19 = (Il2CppObject*)L_18->get_source3_3();
		CombineLatestObserver_1_t123567747 * L_20 = (CombineLatestObserver_1_t123567747 *)__this->get_c3_10();
		NullCheck((Il2CppObject*)L_19);
		Il2CppObject * L_21 = InterfaceFuncInvoker1< Il2CppObject *, Il2CppObject* >::Invoke(0 /* System.IDisposable UniRx.IObservable`1<System.Object>::Subscribe(UniRx.IObserver`1<T>) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 15), (Il2CppObject*)L_19, (Il2CppObject*)L_20);
		V_2 = (Il2CppObject *)L_21;
		CombineLatestObservable_6_t4095141259 * L_22 = (CombineLatestObservable_6_t4095141259 *)__this->get_parent_6();
		NullCheck(L_22);
		Il2CppObject* L_23 = (Il2CppObject*)L_22->get_source4_4();
		CombineLatestObserver_1_t123567747 * L_24 = (CombineLatestObserver_1_t123567747 *)__this->get_c4_11();
		NullCheck((Il2CppObject*)L_23);
		Il2CppObject * L_25 = InterfaceFuncInvoker1< Il2CppObject *, Il2CppObject* >::Invoke(0 /* System.IDisposable UniRx.IObservable`1<System.Object>::Subscribe(UniRx.IObserver`1<T>) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 16), (Il2CppObject*)L_23, (Il2CppObject*)L_24);
		V_3 = (Il2CppObject *)L_25;
		CombineLatestObservable_6_t4095141259 * L_26 = (CombineLatestObservable_6_t4095141259 *)__this->get_parent_6();
		NullCheck(L_26);
		Il2CppObject* L_27 = (Il2CppObject*)L_26->get_source5_5();
		CombineLatestObserver_1_t123567747 * L_28 = (CombineLatestObserver_1_t123567747 *)__this->get_c5_12();
		NullCheck((Il2CppObject*)L_27);
		Il2CppObject * L_29 = InterfaceFuncInvoker1< Il2CppObject *, Il2CppObject* >::Invoke(0 /* System.IDisposable UniRx.IObservable`1<System.Object>::Subscribe(UniRx.IObserver`1<T>) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 17), (Il2CppObject*)L_27, (Il2CppObject*)L_28);
		V_4 = (Il2CppObject *)L_29;
		IDisposableU5BU5D_t165183403* L_30 = (IDisposableU5BU5D_t165183403*)((IDisposableU5BU5D_t165183403*)SZArrayNew(IDisposableU5BU5D_t165183403_il2cpp_TypeInfo_var, (uint32_t)5));
		Il2CppObject * L_31 = V_0;
		NullCheck(L_30);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_30, 0);
		ArrayElementTypeCheck (L_30, L_31);
		(L_30)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)L_31);
		IDisposableU5BU5D_t165183403* L_32 = (IDisposableU5BU5D_t165183403*)L_30;
		Il2CppObject * L_33 = V_1;
		NullCheck(L_32);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_32, 1);
		ArrayElementTypeCheck (L_32, L_33);
		(L_32)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_33);
		IDisposableU5BU5D_t165183403* L_34 = (IDisposableU5BU5D_t165183403*)L_32;
		Il2CppObject * L_35 = V_2;
		NullCheck(L_34);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_34, 2);
		ArrayElementTypeCheck (L_34, L_35);
		(L_34)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)L_35);
		IDisposableU5BU5D_t165183403* L_36 = (IDisposableU5BU5D_t165183403*)L_34;
		Il2CppObject * L_37 = V_3;
		NullCheck(L_36);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_36, 3);
		ArrayElementTypeCheck (L_36, L_37);
		(L_36)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_37);
		IDisposableU5BU5D_t165183403* L_38 = (IDisposableU5BU5D_t165183403*)L_36;
		Il2CppObject * L_39 = V_4;
		NullCheck(L_38);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_38, 4);
		ArrayElementTypeCheck (L_38, L_39);
		(L_38)->SetAt(static_cast<il2cpp_array_size_t>(4), (Il2CppObject *)L_39);
		Il2CppObject * L_40 = StableCompositeDisposable_Create_m1215138723(NULL /*static, unused*/, (IDisposableU5BU5D_t165183403*)L_38, /*hidden argument*/NULL);
		return L_40;
	}
}
// TR UniRx.Operators.CombineLatestObservable`6/CombineLatest<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>::GetResult()
extern "C"  Il2CppObject * CombineLatest_GetResult_m2586518870_gshared (CombineLatest_t89686297 * __this, const MethodInfo* method)
{
	{
		CombineLatestObservable_6_t4095141259 * L_0 = (CombineLatestObservable_6_t4095141259 *)__this->get_parent_6();
		NullCheck(L_0);
		CombineLatestFunc_6_t2285714110 * L_1 = (CombineLatestFunc_6_t2285714110 *)L_0->get_resultSelector_6();
		CombineLatestObserver_1_t123567747 * L_2 = (CombineLatestObserver_1_t123567747 *)__this->get_c1_8();
		NullCheck((CombineLatestObserver_1_t123567747 *)L_2);
		Il2CppObject * L_3 = ((  Il2CppObject * (*) (CombineLatestObserver_1_t123567747 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 18)->method)((CombineLatestObserver_1_t123567747 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 18));
		CombineLatestObserver_1_t123567747 * L_4 = (CombineLatestObserver_1_t123567747 *)__this->get_c2_9();
		NullCheck((CombineLatestObserver_1_t123567747 *)L_4);
		Il2CppObject * L_5 = ((  Il2CppObject * (*) (CombineLatestObserver_1_t123567747 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 19)->method)((CombineLatestObserver_1_t123567747 *)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 19));
		CombineLatestObserver_1_t123567747 * L_6 = (CombineLatestObserver_1_t123567747 *)__this->get_c3_10();
		NullCheck((CombineLatestObserver_1_t123567747 *)L_6);
		Il2CppObject * L_7 = ((  Il2CppObject * (*) (CombineLatestObserver_1_t123567747 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 20)->method)((CombineLatestObserver_1_t123567747 *)L_6, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 20));
		CombineLatestObserver_1_t123567747 * L_8 = (CombineLatestObserver_1_t123567747 *)__this->get_c4_11();
		NullCheck((CombineLatestObserver_1_t123567747 *)L_8);
		Il2CppObject * L_9 = ((  Il2CppObject * (*) (CombineLatestObserver_1_t123567747 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 21)->method)((CombineLatestObserver_1_t123567747 *)L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 21));
		CombineLatestObserver_1_t123567747 * L_10 = (CombineLatestObserver_1_t123567747 *)__this->get_c5_12();
		NullCheck((CombineLatestObserver_1_t123567747 *)L_10);
		Il2CppObject * L_11 = ((  Il2CppObject * (*) (CombineLatestObserver_1_t123567747 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 22)->method)((CombineLatestObserver_1_t123567747 *)L_10, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 22));
		NullCheck((CombineLatestFunc_6_t2285714110 *)L_1);
		Il2CppObject * L_12 = ((  Il2CppObject * (*) (CombineLatestFunc_6_t2285714110 *, Il2CppObject *, Il2CppObject *, Il2CppObject *, Il2CppObject *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 23)->method)((CombineLatestFunc_6_t2285714110 *)L_1, (Il2CppObject *)L_3, (Il2CppObject *)L_5, (Il2CppObject *)L_7, (Il2CppObject *)L_9, (Il2CppObject *)L_11, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 23));
		return L_12;
	}
}
// System.Void UniRx.Operators.CombineLatestObservable`6/CombineLatest<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>::OnNext(TR)
extern "C"  void CombineLatest_OnNext_m3018602700_gshared (CombineLatest_t89686297 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = (Il2CppObject*)((OperatorObserverBase_2_t1187768149 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		Il2CppObject * L_1 = ___value0;
		NullCheck((Il2CppObject*)L_0);
		InterfaceActionInvoker1< Il2CppObject * >::Invoke(2 /* System.Void UniRx.IObserver`1<System.Object>::OnNext(T) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 24), (Il2CppObject*)L_0, (Il2CppObject *)L_1);
		return;
	}
}
// System.Void UniRx.Operators.CombineLatestObservable`6/CombineLatest<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>::OnError(System.Exception)
extern "C"  void CombineLatest_OnError_m4279015907_gshared (CombineLatest_t89686297 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method)
{
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		Il2CppObject* L_0 = (Il2CppObject*)((OperatorObserverBase_2_t1187768149 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		Exception_t1967233988 * L_1 = ___error0;
		NullCheck((Il2CppObject*)L_0);
		InterfaceActionInvoker1< Exception_t1967233988 * >::Invoke(1 /* System.Void UniRx.IObserver`1<System.Object>::OnError(System.Exception) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 24), (Il2CppObject*)L_0, (Exception_t1967233988 *)L_1);
		IL2CPP_LEAVE(0x1A, FINALLY_0013);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0013;
	}

FINALLY_0013:
	{ // begin finally (depth: 1)
		NullCheck((OperatorObserverBase_2_t1187768149 *)__this);
		VirtActionInvoker0::Invoke(4 /* System.Void UniRx.Operators.OperatorObserverBase`2<System.Object,System.Object>::Dispose() */, (OperatorObserverBase_2_t1187768149 *)__this);
		IL2CPP_END_FINALLY(19)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(19)
	{
		IL2CPP_JUMP_TBL(0x1A, IL_001a)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_001a:
	{
		return;
	}
}
// System.Void UniRx.Operators.CombineLatestObservable`6/CombineLatest<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>::OnCompleted()
extern "C"  void CombineLatest_OnCompleted_m555796918_gshared (CombineLatest_t89686297 * __this, const MethodInfo* method)
{
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		Il2CppObject* L_0 = (Il2CppObject*)((OperatorObserverBase_2_t1187768149 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		NullCheck((Il2CppObject*)L_0);
		InterfaceActionInvoker0::Invoke(0 /* System.Void UniRx.IObserver`1<System.Object>::OnCompleted() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 24), (Il2CppObject*)L_0);
		IL2CPP_LEAVE(0x19, FINALLY_0012);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0012;
	}

FINALLY_0012:
	{ // begin finally (depth: 1)
		NullCheck((OperatorObserverBase_2_t1187768149 *)__this);
		VirtActionInvoker0::Invoke(4 /* System.Void UniRx.Operators.OperatorObserverBase`2<System.Object,System.Object>::Dispose() */, (OperatorObserverBase_2_t1187768149 *)__this);
		IL2CPP_END_FINALLY(18)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(18)
	{
		IL2CPP_JUMP_TBL(0x19, IL_0019)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0019:
	{
		return;
	}
}
// System.Void UniRx.Operators.CombineLatestObservable`6<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>::.ctor(UniRx.IObservable`1<T1>,UniRx.IObservable`1<T2>,UniRx.IObservable`1<T3>,UniRx.IObservable`1<T4>,UniRx.IObservable`1<T5>,UniRx.Operators.CombineLatestFunc`6<T1,T2,T3,T4,T5,TR>)
extern "C"  void CombineLatestObservable_6__ctor_m3123583675_gshared (CombineLatestObservable_6_t4095141259 * __this, Il2CppObject* ___source10, Il2CppObject* ___source21, Il2CppObject* ___source32, Il2CppObject* ___source43, Il2CppObject* ___source54, CombineLatestFunc_6_t2285714110 * ___resultSelector5, const MethodInfo* method)
{
	CombineLatestObservable_6_t4095141259 * G_B6_0 = NULL;
	CombineLatestObservable_6_t4095141259 * G_B1_0 = NULL;
	CombineLatestObservable_6_t4095141259 * G_B2_0 = NULL;
	CombineLatestObservable_6_t4095141259 * G_B3_0 = NULL;
	CombineLatestObservable_6_t4095141259 * G_B4_0 = NULL;
	CombineLatestObservable_6_t4095141259 * G_B5_0 = NULL;
	int32_t G_B7_0 = 0;
	CombineLatestObservable_6_t4095141259 * G_B7_1 = NULL;
	{
		Il2CppObject* L_0 = ___source10;
		bool L_1 = ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)(NULL /*static, unused*/, (Il2CppObject*)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		G_B1_0 = ((CombineLatestObservable_6_t4095141259 *)(__this));
		if (L_1)
		{
			G_B6_0 = ((CombineLatestObservable_6_t4095141259 *)(__this));
			goto IL_003d;
		}
	}
	{
		Il2CppObject* L_2 = ___source21;
		bool L_3 = ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)(NULL /*static, unused*/, (Il2CppObject*)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		G_B2_0 = ((CombineLatestObservable_6_t4095141259 *)(G_B1_0));
		if (L_3)
		{
			G_B6_0 = ((CombineLatestObservable_6_t4095141259 *)(G_B1_0));
			goto IL_003d;
		}
	}
	{
		Il2CppObject* L_4 = ___source32;
		bool L_5 = ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)(NULL /*static, unused*/, (Il2CppObject*)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		G_B3_0 = ((CombineLatestObservable_6_t4095141259 *)(G_B2_0));
		if (L_5)
		{
			G_B6_0 = ((CombineLatestObservable_6_t4095141259 *)(G_B2_0));
			goto IL_003d;
		}
	}
	{
		Il2CppObject* L_6 = ___source43;
		bool L_7 = ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)(NULL /*static, unused*/, (Il2CppObject*)L_6, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		G_B4_0 = ((CombineLatestObservable_6_t4095141259 *)(G_B3_0));
		if (L_7)
		{
			G_B6_0 = ((CombineLatestObservable_6_t4095141259 *)(G_B3_0));
			goto IL_003d;
		}
	}
	{
		Il2CppObject* L_8 = ___source54;
		bool L_9 = ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)(NULL /*static, unused*/, (Il2CppObject*)L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		G_B5_0 = ((CombineLatestObservable_6_t4095141259 *)(G_B4_0));
		if (L_9)
		{
			G_B6_0 = ((CombineLatestObservable_6_t4095141259 *)(G_B4_0));
			goto IL_003d;
		}
	}
	{
		G_B7_0 = 0;
		G_B7_1 = ((CombineLatestObservable_6_t4095141259 *)(G_B5_0));
		goto IL_003e;
	}

IL_003d:
	{
		G_B7_0 = 1;
		G_B7_1 = ((CombineLatestObservable_6_t4095141259 *)(G_B6_0));
	}

IL_003e:
	{
		NullCheck((OperatorObservableBase_1_t4196218687 *)G_B7_1);
		((  void (*) (OperatorObservableBase_1_t4196218687 *, bool, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->method)((OperatorObservableBase_1_t4196218687 *)G_B7_1, (bool)G_B7_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		Il2CppObject* L_10 = ___source10;
		__this->set_source1_1(L_10);
		Il2CppObject* L_11 = ___source21;
		__this->set_source2_2(L_11);
		Il2CppObject* L_12 = ___source32;
		__this->set_source3_3(L_12);
		Il2CppObject* L_13 = ___source43;
		__this->set_source4_4(L_13);
		Il2CppObject* L_14 = ___source54;
		__this->set_source5_5(L_14);
		CombineLatestFunc_6_t2285714110 * L_15 = ___resultSelector5;
		__this->set_resultSelector_6(L_15);
		return;
	}
}
// System.IDisposable UniRx.Operators.CombineLatestObservable`6<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>::SubscribeCore(UniRx.IObserver`1<TR>,System.IDisposable)
extern "C"  Il2CppObject * CombineLatestObservable_6_SubscribeCore_m3263449223_gshared (CombineLatestObservable_6_t4095141259 * __this, Il2CppObject* ___observer0, Il2CppObject * ___cancel1, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = ___observer0;
		Il2CppObject * L_1 = ___cancel1;
		CombineLatest_t89686297 * L_2 = (CombineLatest_t89686297 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7));
		((  void (*) (CombineLatest_t89686297 *, int32_t, CombineLatestObservable_6_t4095141259 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8)->method)(L_2, (int32_t)5, (CombineLatestObservable_6_t4095141259 *)__this, (Il2CppObject*)L_0, (Il2CppObject *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8));
		NullCheck((CombineLatest_t89686297 *)L_2);
		Il2CppObject * L_3 = ((  Il2CppObject * (*) (CombineLatest_t89686297 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9)->method)((CombineLatest_t89686297 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9));
		return L_3;
	}
}
// System.Void UniRx.Operators.CombineLatestObservable`7/CombineLatest<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>::.ctor(System.Int32,UniRx.Operators.CombineLatestObservable`7<T1,T2,T3,T4,T5,T6,TR>,UniRx.IObserver`1<TR>,System.IDisposable)
extern Il2CppClass* Il2CppObject_il2cpp_TypeInfo_var;
extern const uint32_t CombineLatest__ctor_m810014240_MetadataUsageId;
extern "C"  void CombineLatest__ctor_m810014240_gshared (CombineLatest_t2495053143 * __this, int32_t ___length0, CombineLatestObservable_7_t1894017470 * ___parent1, Il2CppObject* ___observer2, Il2CppObject * ___cancel3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (CombineLatest__ctor_m810014240_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject * L_0 = (Il2CppObject *)il2cpp_codegen_object_new(Il2CppObject_il2cpp_TypeInfo_var);
		Object__ctor_m1772956182(L_0, /*hidden argument*/NULL);
		__this->set_gate_7(L_0);
		int32_t L_1 = ___length0;
		Il2CppObject* L_2 = ___observer2;
		Il2CppObject * L_3 = ___cancel3;
		NullCheck((NthCombineLatestObserverBase_1_t2199482330 *)__this);
		((  void (*) (NthCombineLatestObserverBase_1_t2199482330 *, int32_t, Il2CppObject*, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((NthCombineLatestObserverBase_1_t2199482330 *)__this, (int32_t)L_1, (Il2CppObject*)L_2, (Il2CppObject *)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		CombineLatestObservable_7_t1894017470 * L_4 = ___parent1;
		__this->set_parent_6(L_4);
		return;
	}
}
// System.IDisposable UniRx.Operators.CombineLatestObservable`7/CombineLatest<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>::Run()
extern Il2CppClass* IDisposableU5BU5D_t165183403_il2cpp_TypeInfo_var;
extern const uint32_t CombineLatest_Run_m1483535469_MetadataUsageId;
extern "C"  Il2CppObject * CombineLatest_Run_m1483535469_gshared (CombineLatest_t2495053143 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (CombineLatest_Run_m1483535469_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Il2CppObject * V_0 = NULL;
	Il2CppObject * V_1 = NULL;
	Il2CppObject * V_2 = NULL;
	Il2CppObject * V_3 = NULL;
	Il2CppObject * V_4 = NULL;
	Il2CppObject * V_5 = NULL;
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_gate_7();
		CombineLatestObserver_1_t123567747 * L_1 = (CombineLatestObserver_1_t123567747 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		((  void (*) (CombineLatestObserver_1_t123567747 *, Il2CppObject *, Il2CppObject *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)(L_1, (Il2CppObject *)L_0, (Il2CppObject *)__this, (int32_t)0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		__this->set_c1_8(L_1);
		Il2CppObject * L_2 = (Il2CppObject *)__this->get_gate_7();
		CombineLatestObserver_1_t123567747 * L_3 = (CombineLatestObserver_1_t123567747 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		((  void (*) (CombineLatestObserver_1_t123567747 *, Il2CppObject *, Il2CppObject *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6)->method)(L_3, (Il2CppObject *)L_2, (Il2CppObject *)__this, (int32_t)1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		__this->set_c2_9(L_3);
		Il2CppObject * L_4 = (Il2CppObject *)__this->get_gate_7();
		CombineLatestObserver_1_t123567747 * L_5 = (CombineLatestObserver_1_t123567747 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7));
		((  void (*) (CombineLatestObserver_1_t123567747 *, Il2CppObject *, Il2CppObject *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8)->method)(L_5, (Il2CppObject *)L_4, (Il2CppObject *)__this, (int32_t)2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8));
		__this->set_c3_10(L_5);
		Il2CppObject * L_6 = (Il2CppObject *)__this->get_gate_7();
		CombineLatestObserver_1_t123567747 * L_7 = (CombineLatestObserver_1_t123567747 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9));
		((  void (*) (CombineLatestObserver_1_t123567747 *, Il2CppObject *, Il2CppObject *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10)->method)(L_7, (Il2CppObject *)L_6, (Il2CppObject *)__this, (int32_t)3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10));
		__this->set_c4_11(L_7);
		Il2CppObject * L_8 = (Il2CppObject *)__this->get_gate_7();
		CombineLatestObserver_1_t123567747 * L_9 = (CombineLatestObserver_1_t123567747 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 11));
		((  void (*) (CombineLatestObserver_1_t123567747 *, Il2CppObject *, Il2CppObject *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 12)->method)(L_9, (Il2CppObject *)L_8, (Il2CppObject *)__this, (int32_t)4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 12));
		__this->set_c5_12(L_9);
		Il2CppObject * L_10 = (Il2CppObject *)__this->get_gate_7();
		CombineLatestObserver_1_t123567747 * L_11 = (CombineLatestObserver_1_t123567747 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 13));
		((  void (*) (CombineLatestObserver_1_t123567747 *, Il2CppObject *, Il2CppObject *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 14)->method)(L_11, (Il2CppObject *)L_10, (Il2CppObject *)__this, (int32_t)5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 14));
		__this->set_c6_13(L_11);
		CombineLatestObservable_7_t1894017470 * L_12 = (CombineLatestObservable_7_t1894017470 *)__this->get_parent_6();
		NullCheck(L_12);
		Il2CppObject* L_13 = (Il2CppObject*)L_12->get_source1_1();
		CombineLatestObserver_1_t123567747 * L_14 = (CombineLatestObserver_1_t123567747 *)__this->get_c1_8();
		NullCheck((Il2CppObject*)L_13);
		Il2CppObject * L_15 = InterfaceFuncInvoker1< Il2CppObject *, Il2CppObject* >::Invoke(0 /* System.IDisposable UniRx.IObservable`1<System.Object>::Subscribe(UniRx.IObserver`1<T>) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 15), (Il2CppObject*)L_13, (Il2CppObject*)L_14);
		V_0 = (Il2CppObject *)L_15;
		CombineLatestObservable_7_t1894017470 * L_16 = (CombineLatestObservable_7_t1894017470 *)__this->get_parent_6();
		NullCheck(L_16);
		Il2CppObject* L_17 = (Il2CppObject*)L_16->get_source2_2();
		CombineLatestObserver_1_t123567747 * L_18 = (CombineLatestObserver_1_t123567747 *)__this->get_c2_9();
		NullCheck((Il2CppObject*)L_17);
		Il2CppObject * L_19 = InterfaceFuncInvoker1< Il2CppObject *, Il2CppObject* >::Invoke(0 /* System.IDisposable UniRx.IObservable`1<System.Object>::Subscribe(UniRx.IObserver`1<T>) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 16), (Il2CppObject*)L_17, (Il2CppObject*)L_18);
		V_1 = (Il2CppObject *)L_19;
		CombineLatestObservable_7_t1894017470 * L_20 = (CombineLatestObservable_7_t1894017470 *)__this->get_parent_6();
		NullCheck(L_20);
		Il2CppObject* L_21 = (Il2CppObject*)L_20->get_source3_3();
		CombineLatestObserver_1_t123567747 * L_22 = (CombineLatestObserver_1_t123567747 *)__this->get_c3_10();
		NullCheck((Il2CppObject*)L_21);
		Il2CppObject * L_23 = InterfaceFuncInvoker1< Il2CppObject *, Il2CppObject* >::Invoke(0 /* System.IDisposable UniRx.IObservable`1<System.Object>::Subscribe(UniRx.IObserver`1<T>) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 17), (Il2CppObject*)L_21, (Il2CppObject*)L_22);
		V_2 = (Il2CppObject *)L_23;
		CombineLatestObservable_7_t1894017470 * L_24 = (CombineLatestObservable_7_t1894017470 *)__this->get_parent_6();
		NullCheck(L_24);
		Il2CppObject* L_25 = (Il2CppObject*)L_24->get_source4_4();
		CombineLatestObserver_1_t123567747 * L_26 = (CombineLatestObserver_1_t123567747 *)__this->get_c4_11();
		NullCheck((Il2CppObject*)L_25);
		Il2CppObject * L_27 = InterfaceFuncInvoker1< Il2CppObject *, Il2CppObject* >::Invoke(0 /* System.IDisposable UniRx.IObservable`1<System.Object>::Subscribe(UniRx.IObserver`1<T>) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 18), (Il2CppObject*)L_25, (Il2CppObject*)L_26);
		V_3 = (Il2CppObject *)L_27;
		CombineLatestObservable_7_t1894017470 * L_28 = (CombineLatestObservable_7_t1894017470 *)__this->get_parent_6();
		NullCheck(L_28);
		Il2CppObject* L_29 = (Il2CppObject*)L_28->get_source5_5();
		CombineLatestObserver_1_t123567747 * L_30 = (CombineLatestObserver_1_t123567747 *)__this->get_c5_12();
		NullCheck((Il2CppObject*)L_29);
		Il2CppObject * L_31 = InterfaceFuncInvoker1< Il2CppObject *, Il2CppObject* >::Invoke(0 /* System.IDisposable UniRx.IObservable`1<System.Object>::Subscribe(UniRx.IObserver`1<T>) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 19), (Il2CppObject*)L_29, (Il2CppObject*)L_30);
		V_4 = (Il2CppObject *)L_31;
		CombineLatestObservable_7_t1894017470 * L_32 = (CombineLatestObservable_7_t1894017470 *)__this->get_parent_6();
		NullCheck(L_32);
		Il2CppObject* L_33 = (Il2CppObject*)L_32->get_source6_6();
		CombineLatestObserver_1_t123567747 * L_34 = (CombineLatestObserver_1_t123567747 *)__this->get_c6_13();
		NullCheck((Il2CppObject*)L_33);
		Il2CppObject * L_35 = InterfaceFuncInvoker1< Il2CppObject *, Il2CppObject* >::Invoke(0 /* System.IDisposable UniRx.IObservable`1<System.Object>::Subscribe(UniRx.IObserver`1<T>) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 20), (Il2CppObject*)L_33, (Il2CppObject*)L_34);
		V_5 = (Il2CppObject *)L_35;
		IDisposableU5BU5D_t165183403* L_36 = (IDisposableU5BU5D_t165183403*)((IDisposableU5BU5D_t165183403*)SZArrayNew(IDisposableU5BU5D_t165183403_il2cpp_TypeInfo_var, (uint32_t)6));
		Il2CppObject * L_37 = V_0;
		NullCheck(L_36);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_36, 0);
		ArrayElementTypeCheck (L_36, L_37);
		(L_36)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)L_37);
		IDisposableU5BU5D_t165183403* L_38 = (IDisposableU5BU5D_t165183403*)L_36;
		Il2CppObject * L_39 = V_1;
		NullCheck(L_38);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_38, 1);
		ArrayElementTypeCheck (L_38, L_39);
		(L_38)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_39);
		IDisposableU5BU5D_t165183403* L_40 = (IDisposableU5BU5D_t165183403*)L_38;
		Il2CppObject * L_41 = V_2;
		NullCheck(L_40);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_40, 2);
		ArrayElementTypeCheck (L_40, L_41);
		(L_40)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)L_41);
		IDisposableU5BU5D_t165183403* L_42 = (IDisposableU5BU5D_t165183403*)L_40;
		Il2CppObject * L_43 = V_3;
		NullCheck(L_42);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_42, 3);
		ArrayElementTypeCheck (L_42, L_43);
		(L_42)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_43);
		IDisposableU5BU5D_t165183403* L_44 = (IDisposableU5BU5D_t165183403*)L_42;
		Il2CppObject * L_45 = V_4;
		NullCheck(L_44);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_44, 4);
		ArrayElementTypeCheck (L_44, L_45);
		(L_44)->SetAt(static_cast<il2cpp_array_size_t>(4), (Il2CppObject *)L_45);
		IDisposableU5BU5D_t165183403* L_46 = (IDisposableU5BU5D_t165183403*)L_44;
		Il2CppObject * L_47 = V_5;
		NullCheck(L_46);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_46, 5);
		ArrayElementTypeCheck (L_46, L_47);
		(L_46)->SetAt(static_cast<il2cpp_array_size_t>(5), (Il2CppObject *)L_47);
		Il2CppObject * L_48 = StableCompositeDisposable_Create_m1215138723(NULL /*static, unused*/, (IDisposableU5BU5D_t165183403*)L_46, /*hidden argument*/NULL);
		return L_48;
	}
}
// TR UniRx.Operators.CombineLatestObservable`7/CombineLatest<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>::GetResult()
extern "C"  Il2CppObject * CombineLatest_GetResult_m2261004617_gshared (CombineLatest_t2495053143 * __this, const MethodInfo* method)
{
	{
		CombineLatestObservable_7_t1894017470 * L_0 = (CombineLatestObservable_7_t1894017470 *)__this->get_parent_6();
		NullCheck(L_0);
		CombineLatestFunc_7_t929534559 * L_1 = (CombineLatestFunc_7_t929534559 *)L_0->get_resultSelector_7();
		CombineLatestObserver_1_t123567747 * L_2 = (CombineLatestObserver_1_t123567747 *)__this->get_c1_8();
		NullCheck((CombineLatestObserver_1_t123567747 *)L_2);
		Il2CppObject * L_3 = ((  Il2CppObject * (*) (CombineLatestObserver_1_t123567747 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 21)->method)((CombineLatestObserver_1_t123567747 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 21));
		CombineLatestObserver_1_t123567747 * L_4 = (CombineLatestObserver_1_t123567747 *)__this->get_c2_9();
		NullCheck((CombineLatestObserver_1_t123567747 *)L_4);
		Il2CppObject * L_5 = ((  Il2CppObject * (*) (CombineLatestObserver_1_t123567747 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 22)->method)((CombineLatestObserver_1_t123567747 *)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 22));
		CombineLatestObserver_1_t123567747 * L_6 = (CombineLatestObserver_1_t123567747 *)__this->get_c3_10();
		NullCheck((CombineLatestObserver_1_t123567747 *)L_6);
		Il2CppObject * L_7 = ((  Il2CppObject * (*) (CombineLatestObserver_1_t123567747 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 23)->method)((CombineLatestObserver_1_t123567747 *)L_6, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 23));
		CombineLatestObserver_1_t123567747 * L_8 = (CombineLatestObserver_1_t123567747 *)__this->get_c4_11();
		NullCheck((CombineLatestObserver_1_t123567747 *)L_8);
		Il2CppObject * L_9 = ((  Il2CppObject * (*) (CombineLatestObserver_1_t123567747 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 24)->method)((CombineLatestObserver_1_t123567747 *)L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 24));
		CombineLatestObserver_1_t123567747 * L_10 = (CombineLatestObserver_1_t123567747 *)__this->get_c5_12();
		NullCheck((CombineLatestObserver_1_t123567747 *)L_10);
		Il2CppObject * L_11 = ((  Il2CppObject * (*) (CombineLatestObserver_1_t123567747 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 25)->method)((CombineLatestObserver_1_t123567747 *)L_10, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 25));
		CombineLatestObserver_1_t123567747 * L_12 = (CombineLatestObserver_1_t123567747 *)__this->get_c6_13();
		NullCheck((CombineLatestObserver_1_t123567747 *)L_12);
		Il2CppObject * L_13 = ((  Il2CppObject * (*) (CombineLatestObserver_1_t123567747 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 26)->method)((CombineLatestObserver_1_t123567747 *)L_12, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 26));
		NullCheck((CombineLatestFunc_7_t929534559 *)L_1);
		Il2CppObject * L_14 = ((  Il2CppObject * (*) (CombineLatestFunc_7_t929534559 *, Il2CppObject *, Il2CppObject *, Il2CppObject *, Il2CppObject *, Il2CppObject *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 27)->method)((CombineLatestFunc_7_t929534559 *)L_1, (Il2CppObject *)L_3, (Il2CppObject *)L_5, (Il2CppObject *)L_7, (Il2CppObject *)L_9, (Il2CppObject *)L_11, (Il2CppObject *)L_13, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 27));
		return L_14;
	}
}
// System.Void UniRx.Operators.CombineLatestObservable`7/CombineLatest<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>::OnNext(TR)
extern "C"  void CombineLatest_OnNext_m882627257_gshared (CombineLatest_t2495053143 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = (Il2CppObject*)((OperatorObserverBase_2_t1187768149 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		Il2CppObject * L_1 = ___value0;
		NullCheck((Il2CppObject*)L_0);
		InterfaceActionInvoker1< Il2CppObject * >::Invoke(2 /* System.Void UniRx.IObserver`1<System.Object>::OnNext(T) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 28), (Il2CppObject*)L_0, (Il2CppObject *)L_1);
		return;
	}
}
// System.Void UniRx.Operators.CombineLatestObservable`7/CombineLatest<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>::OnError(System.Exception)
extern "C"  void CombineLatest_OnError_m740444246_gshared (CombineLatest_t2495053143 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method)
{
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		Il2CppObject* L_0 = (Il2CppObject*)((OperatorObserverBase_2_t1187768149 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		Exception_t1967233988 * L_1 = ___error0;
		NullCheck((Il2CppObject*)L_0);
		InterfaceActionInvoker1< Exception_t1967233988 * >::Invoke(1 /* System.Void UniRx.IObserver`1<System.Object>::OnError(System.Exception) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 28), (Il2CppObject*)L_0, (Exception_t1967233988 *)L_1);
		IL2CPP_LEAVE(0x1A, FINALLY_0013);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0013;
	}

FINALLY_0013:
	{ // begin finally (depth: 1)
		NullCheck((OperatorObserverBase_2_t1187768149 *)__this);
		VirtActionInvoker0::Invoke(4 /* System.Void UniRx.Operators.OperatorObserverBase`2<System.Object,System.Object>::Dispose() */, (OperatorObserverBase_2_t1187768149 *)__this);
		IL2CPP_END_FINALLY(19)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(19)
	{
		IL2CPP_JUMP_TBL(0x1A, IL_001a)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_001a:
	{
		return;
	}
}
// System.Void UniRx.Operators.CombineLatestObservable`7/CombineLatest<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>::OnCompleted()
extern "C"  void CombineLatest_OnCompleted_m1946832041_gshared (CombineLatest_t2495053143 * __this, const MethodInfo* method)
{
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		Il2CppObject* L_0 = (Il2CppObject*)((OperatorObserverBase_2_t1187768149 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		NullCheck((Il2CppObject*)L_0);
		InterfaceActionInvoker0::Invoke(0 /* System.Void UniRx.IObserver`1<System.Object>::OnCompleted() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 28), (Il2CppObject*)L_0);
		IL2CPP_LEAVE(0x19, FINALLY_0012);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0012;
	}

FINALLY_0012:
	{ // begin finally (depth: 1)
		NullCheck((OperatorObserverBase_2_t1187768149 *)__this);
		VirtActionInvoker0::Invoke(4 /* System.Void UniRx.Operators.OperatorObserverBase`2<System.Object,System.Object>::Dispose() */, (OperatorObserverBase_2_t1187768149 *)__this);
		IL2CPP_END_FINALLY(18)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(18)
	{
		IL2CPP_JUMP_TBL(0x19, IL_0019)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0019:
	{
		return;
	}
}
// System.Void UniRx.Operators.CombineLatestObservable`7<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>::.ctor(UniRx.IObservable`1<T1>,UniRx.IObservable`1<T2>,UniRx.IObservable`1<T3>,UniRx.IObservable`1<T4>,UniRx.IObservable`1<T5>,UniRx.IObservable`1<T6>,UniRx.Operators.CombineLatestFunc`7<T1,T2,T3,T4,T5,T6,TR>)
extern "C"  void CombineLatestObservable_7__ctor_m2008536340_gshared (CombineLatestObservable_7_t1894017470 * __this, Il2CppObject* ___source10, Il2CppObject* ___source21, Il2CppObject* ___source32, Il2CppObject* ___source43, Il2CppObject* ___source54, Il2CppObject* ___source65, CombineLatestFunc_7_t929534559 * ___resultSelector6, const MethodInfo* method)
{
	CombineLatestObservable_7_t1894017470 * G_B7_0 = NULL;
	CombineLatestObservable_7_t1894017470 * G_B1_0 = NULL;
	CombineLatestObservable_7_t1894017470 * G_B2_0 = NULL;
	CombineLatestObservable_7_t1894017470 * G_B3_0 = NULL;
	CombineLatestObservable_7_t1894017470 * G_B4_0 = NULL;
	CombineLatestObservable_7_t1894017470 * G_B5_0 = NULL;
	CombineLatestObservable_7_t1894017470 * G_B6_0 = NULL;
	int32_t G_B8_0 = 0;
	CombineLatestObservable_7_t1894017470 * G_B8_1 = NULL;
	{
		Il2CppObject* L_0 = ___source10;
		bool L_1 = ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)(NULL /*static, unused*/, (Il2CppObject*)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		G_B1_0 = ((CombineLatestObservable_7_t1894017470 *)(__this));
		if (L_1)
		{
			G_B7_0 = ((CombineLatestObservable_7_t1894017470 *)(__this));
			goto IL_0049;
		}
	}
	{
		Il2CppObject* L_2 = ___source21;
		bool L_3 = ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)(NULL /*static, unused*/, (Il2CppObject*)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		G_B2_0 = ((CombineLatestObservable_7_t1894017470 *)(G_B1_0));
		if (L_3)
		{
			G_B7_0 = ((CombineLatestObservable_7_t1894017470 *)(G_B1_0));
			goto IL_0049;
		}
	}
	{
		Il2CppObject* L_4 = ___source32;
		bool L_5 = ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)(NULL /*static, unused*/, (Il2CppObject*)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		G_B3_0 = ((CombineLatestObservable_7_t1894017470 *)(G_B2_0));
		if (L_5)
		{
			G_B7_0 = ((CombineLatestObservable_7_t1894017470 *)(G_B2_0));
			goto IL_0049;
		}
	}
	{
		Il2CppObject* L_6 = ___source43;
		bool L_7 = ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)(NULL /*static, unused*/, (Il2CppObject*)L_6, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		G_B4_0 = ((CombineLatestObservable_7_t1894017470 *)(G_B3_0));
		if (L_7)
		{
			G_B7_0 = ((CombineLatestObservable_7_t1894017470 *)(G_B3_0));
			goto IL_0049;
		}
	}
	{
		Il2CppObject* L_8 = ___source54;
		bool L_9 = ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)(NULL /*static, unused*/, (Il2CppObject*)L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		G_B5_0 = ((CombineLatestObservable_7_t1894017470 *)(G_B4_0));
		if (L_9)
		{
			G_B7_0 = ((CombineLatestObservable_7_t1894017470 *)(G_B4_0));
			goto IL_0049;
		}
	}
	{
		Il2CppObject* L_10 = ___source65;
		bool L_11 = ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->method)(NULL /*static, unused*/, (Il2CppObject*)L_10, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		G_B6_0 = ((CombineLatestObservable_7_t1894017470 *)(G_B5_0));
		if (L_11)
		{
			G_B7_0 = ((CombineLatestObservable_7_t1894017470 *)(G_B5_0));
			goto IL_0049;
		}
	}
	{
		G_B8_0 = 0;
		G_B8_1 = ((CombineLatestObservable_7_t1894017470 *)(G_B6_0));
		goto IL_004a;
	}

IL_0049:
	{
		G_B8_0 = 1;
		G_B8_1 = ((CombineLatestObservable_7_t1894017470 *)(G_B7_0));
	}

IL_004a:
	{
		NullCheck((OperatorObservableBase_1_t4196218687 *)G_B8_1);
		((  void (*) (OperatorObservableBase_1_t4196218687 *, bool, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6)->method)((OperatorObservableBase_1_t4196218687 *)G_B8_1, (bool)G_B8_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		Il2CppObject* L_12 = ___source10;
		__this->set_source1_1(L_12);
		Il2CppObject* L_13 = ___source21;
		__this->set_source2_2(L_13);
		Il2CppObject* L_14 = ___source32;
		__this->set_source3_3(L_14);
		Il2CppObject* L_15 = ___source43;
		__this->set_source4_4(L_15);
		Il2CppObject* L_16 = ___source54;
		__this->set_source5_5(L_16);
		Il2CppObject* L_17 = ___source65;
		__this->set_source6_6(L_17);
		CombineLatestFunc_7_t929534559 * L_18 = ___resultSelector6;
		__this->set_resultSelector_7(L_18);
		return;
	}
}
// System.IDisposable UniRx.Operators.CombineLatestObservable`7<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>::SubscribeCore(UniRx.IObserver`1<TR>,System.IDisposable)
extern "C"  Il2CppObject * CombineLatestObservable_7_SubscribeCore_m3323554042_gshared (CombineLatestObservable_7_t1894017470 * __this, Il2CppObject* ___observer0, Il2CppObject * ___cancel1, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = ___observer0;
		Il2CppObject * L_1 = ___cancel1;
		CombineLatest_t2495053143 * L_2 = (CombineLatest_t2495053143 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8));
		((  void (*) (CombineLatest_t2495053143 *, int32_t, CombineLatestObservable_7_t1894017470 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9)->method)(L_2, (int32_t)6, (CombineLatestObservable_7_t1894017470 *)__this, (Il2CppObject*)L_0, (Il2CppObject *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9));
		NullCheck((CombineLatest_t2495053143 *)L_2);
		Il2CppObject * L_3 = ((  Il2CppObject * (*) (CombineLatest_t2495053143 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10)->method)((CombineLatest_t2495053143 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10));
		return L_3;
	}
}
// System.Void UniRx.Operators.CombineLatestObservable`8/CombineLatest<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>::.ctor(System.Int32,UniRx.Operators.CombineLatestObservable`8<T1,T2,T3,T4,T5,T6,T7,TR>,UniRx.IObserver`1<TR>,System.IDisposable)
extern Il2CppClass* Il2CppObject_il2cpp_TypeInfo_var;
extern const uint32_t CombineLatest__ctor_m4149794767_MetadataUsageId;
extern "C"  void CombineLatest__ctor_m4149794767_gshared (CombineLatest_t1931772513 * __this, int32_t ___length0, CombineLatestObservable_8_t129076473 * ___parent1, Il2CppObject* ___observer2, Il2CppObject * ___cancel3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (CombineLatest__ctor_m4149794767_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject * L_0 = (Il2CppObject *)il2cpp_codegen_object_new(Il2CppObject_il2cpp_TypeInfo_var);
		Object__ctor_m1772956182(L_0, /*hidden argument*/NULL);
		__this->set_gate_7(L_0);
		int32_t L_1 = ___length0;
		Il2CppObject* L_2 = ___observer2;
		Il2CppObject * L_3 = ___cancel3;
		NullCheck((NthCombineLatestObserverBase_1_t2199482330 *)__this);
		((  void (*) (NthCombineLatestObserverBase_1_t2199482330 *, int32_t, Il2CppObject*, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((NthCombineLatestObserverBase_1_t2199482330 *)__this, (int32_t)L_1, (Il2CppObject*)L_2, (Il2CppObject *)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		CombineLatestObservable_8_t129076473 * L_4 = ___parent1;
		__this->set_parent_6(L_4);
		return;
	}
}
// System.IDisposable UniRx.Operators.CombineLatestObservable`8/CombineLatest<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>::Run()
extern Il2CppClass* IDisposableU5BU5D_t165183403_il2cpp_TypeInfo_var;
extern const uint32_t CombineLatest_Run_m2556639904_MetadataUsageId;
extern "C"  Il2CppObject * CombineLatest_Run_m2556639904_gshared (CombineLatest_t1931772513 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (CombineLatest_Run_m2556639904_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Il2CppObject * V_0 = NULL;
	Il2CppObject * V_1 = NULL;
	Il2CppObject * V_2 = NULL;
	Il2CppObject * V_3 = NULL;
	Il2CppObject * V_4 = NULL;
	Il2CppObject * V_5 = NULL;
	Il2CppObject * V_6 = NULL;
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_gate_7();
		CombineLatestObserver_1_t123567747 * L_1 = (CombineLatestObserver_1_t123567747 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		((  void (*) (CombineLatestObserver_1_t123567747 *, Il2CppObject *, Il2CppObject *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)(L_1, (Il2CppObject *)L_0, (Il2CppObject *)__this, (int32_t)0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		__this->set_c1_8(L_1);
		Il2CppObject * L_2 = (Il2CppObject *)__this->get_gate_7();
		CombineLatestObserver_1_t123567747 * L_3 = (CombineLatestObserver_1_t123567747 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		((  void (*) (CombineLatestObserver_1_t123567747 *, Il2CppObject *, Il2CppObject *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6)->method)(L_3, (Il2CppObject *)L_2, (Il2CppObject *)__this, (int32_t)1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		__this->set_c2_9(L_3);
		Il2CppObject * L_4 = (Il2CppObject *)__this->get_gate_7();
		CombineLatestObserver_1_t123567747 * L_5 = (CombineLatestObserver_1_t123567747 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7));
		((  void (*) (CombineLatestObserver_1_t123567747 *, Il2CppObject *, Il2CppObject *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8)->method)(L_5, (Il2CppObject *)L_4, (Il2CppObject *)__this, (int32_t)2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8));
		__this->set_c3_10(L_5);
		Il2CppObject * L_6 = (Il2CppObject *)__this->get_gate_7();
		CombineLatestObserver_1_t123567747 * L_7 = (CombineLatestObserver_1_t123567747 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9));
		((  void (*) (CombineLatestObserver_1_t123567747 *, Il2CppObject *, Il2CppObject *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10)->method)(L_7, (Il2CppObject *)L_6, (Il2CppObject *)__this, (int32_t)3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10));
		__this->set_c4_11(L_7);
		Il2CppObject * L_8 = (Il2CppObject *)__this->get_gate_7();
		CombineLatestObserver_1_t123567747 * L_9 = (CombineLatestObserver_1_t123567747 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 11));
		((  void (*) (CombineLatestObserver_1_t123567747 *, Il2CppObject *, Il2CppObject *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 12)->method)(L_9, (Il2CppObject *)L_8, (Il2CppObject *)__this, (int32_t)4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 12));
		__this->set_c5_12(L_9);
		Il2CppObject * L_10 = (Il2CppObject *)__this->get_gate_7();
		CombineLatestObserver_1_t123567747 * L_11 = (CombineLatestObserver_1_t123567747 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 13));
		((  void (*) (CombineLatestObserver_1_t123567747 *, Il2CppObject *, Il2CppObject *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 14)->method)(L_11, (Il2CppObject *)L_10, (Il2CppObject *)__this, (int32_t)5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 14));
		__this->set_c6_13(L_11);
		Il2CppObject * L_12 = (Il2CppObject *)__this->get_gate_7();
		CombineLatestObserver_1_t123567747 * L_13 = (CombineLatestObserver_1_t123567747 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 15));
		((  void (*) (CombineLatestObserver_1_t123567747 *, Il2CppObject *, Il2CppObject *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 16)->method)(L_13, (Il2CppObject *)L_12, (Il2CppObject *)__this, (int32_t)6, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 16));
		__this->set_c7_14(L_13);
		CombineLatestObservable_8_t129076473 * L_14 = (CombineLatestObservable_8_t129076473 *)__this->get_parent_6();
		NullCheck(L_14);
		Il2CppObject* L_15 = (Il2CppObject*)L_14->get_source1_1();
		CombineLatestObserver_1_t123567747 * L_16 = (CombineLatestObserver_1_t123567747 *)__this->get_c1_8();
		NullCheck((Il2CppObject*)L_15);
		Il2CppObject * L_17 = InterfaceFuncInvoker1< Il2CppObject *, Il2CppObject* >::Invoke(0 /* System.IDisposable UniRx.IObservable`1<System.Object>::Subscribe(UniRx.IObserver`1<T>) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 17), (Il2CppObject*)L_15, (Il2CppObject*)L_16);
		V_0 = (Il2CppObject *)L_17;
		CombineLatestObservable_8_t129076473 * L_18 = (CombineLatestObservable_8_t129076473 *)__this->get_parent_6();
		NullCheck(L_18);
		Il2CppObject* L_19 = (Il2CppObject*)L_18->get_source2_2();
		CombineLatestObserver_1_t123567747 * L_20 = (CombineLatestObserver_1_t123567747 *)__this->get_c2_9();
		NullCheck((Il2CppObject*)L_19);
		Il2CppObject * L_21 = InterfaceFuncInvoker1< Il2CppObject *, Il2CppObject* >::Invoke(0 /* System.IDisposable UniRx.IObservable`1<System.Object>::Subscribe(UniRx.IObserver`1<T>) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 18), (Il2CppObject*)L_19, (Il2CppObject*)L_20);
		V_1 = (Il2CppObject *)L_21;
		CombineLatestObservable_8_t129076473 * L_22 = (CombineLatestObservable_8_t129076473 *)__this->get_parent_6();
		NullCheck(L_22);
		Il2CppObject* L_23 = (Il2CppObject*)L_22->get_source3_3();
		CombineLatestObserver_1_t123567747 * L_24 = (CombineLatestObserver_1_t123567747 *)__this->get_c3_10();
		NullCheck((Il2CppObject*)L_23);
		Il2CppObject * L_25 = InterfaceFuncInvoker1< Il2CppObject *, Il2CppObject* >::Invoke(0 /* System.IDisposable UniRx.IObservable`1<System.Object>::Subscribe(UniRx.IObserver`1<T>) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 19), (Il2CppObject*)L_23, (Il2CppObject*)L_24);
		V_2 = (Il2CppObject *)L_25;
		CombineLatestObservable_8_t129076473 * L_26 = (CombineLatestObservable_8_t129076473 *)__this->get_parent_6();
		NullCheck(L_26);
		Il2CppObject* L_27 = (Il2CppObject*)L_26->get_source4_4();
		CombineLatestObserver_1_t123567747 * L_28 = (CombineLatestObserver_1_t123567747 *)__this->get_c4_11();
		NullCheck((Il2CppObject*)L_27);
		Il2CppObject * L_29 = InterfaceFuncInvoker1< Il2CppObject *, Il2CppObject* >::Invoke(0 /* System.IDisposable UniRx.IObservable`1<System.Object>::Subscribe(UniRx.IObserver`1<T>) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 20), (Il2CppObject*)L_27, (Il2CppObject*)L_28);
		V_3 = (Il2CppObject *)L_29;
		CombineLatestObservable_8_t129076473 * L_30 = (CombineLatestObservable_8_t129076473 *)__this->get_parent_6();
		NullCheck(L_30);
		Il2CppObject* L_31 = (Il2CppObject*)L_30->get_source5_5();
		CombineLatestObserver_1_t123567747 * L_32 = (CombineLatestObserver_1_t123567747 *)__this->get_c5_12();
		NullCheck((Il2CppObject*)L_31);
		Il2CppObject * L_33 = InterfaceFuncInvoker1< Il2CppObject *, Il2CppObject* >::Invoke(0 /* System.IDisposable UniRx.IObservable`1<System.Object>::Subscribe(UniRx.IObserver`1<T>) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 21), (Il2CppObject*)L_31, (Il2CppObject*)L_32);
		V_4 = (Il2CppObject *)L_33;
		CombineLatestObservable_8_t129076473 * L_34 = (CombineLatestObservable_8_t129076473 *)__this->get_parent_6();
		NullCheck(L_34);
		Il2CppObject* L_35 = (Il2CppObject*)L_34->get_source6_6();
		CombineLatestObserver_1_t123567747 * L_36 = (CombineLatestObserver_1_t123567747 *)__this->get_c6_13();
		NullCheck((Il2CppObject*)L_35);
		Il2CppObject * L_37 = InterfaceFuncInvoker1< Il2CppObject *, Il2CppObject* >::Invoke(0 /* System.IDisposable UniRx.IObservable`1<System.Object>::Subscribe(UniRx.IObserver`1<T>) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 22), (Il2CppObject*)L_35, (Il2CppObject*)L_36);
		V_5 = (Il2CppObject *)L_37;
		CombineLatestObservable_8_t129076473 * L_38 = (CombineLatestObservable_8_t129076473 *)__this->get_parent_6();
		NullCheck(L_38);
		Il2CppObject* L_39 = (Il2CppObject*)L_38->get_source7_7();
		CombineLatestObserver_1_t123567747 * L_40 = (CombineLatestObserver_1_t123567747 *)__this->get_c7_14();
		NullCheck((Il2CppObject*)L_39);
		Il2CppObject * L_41 = InterfaceFuncInvoker1< Il2CppObject *, Il2CppObject* >::Invoke(0 /* System.IDisposable UniRx.IObservable`1<System.Object>::Subscribe(UniRx.IObserver`1<T>) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 23), (Il2CppObject*)L_39, (Il2CppObject*)L_40);
		V_6 = (Il2CppObject *)L_41;
		IDisposableU5BU5D_t165183403* L_42 = (IDisposableU5BU5D_t165183403*)((IDisposableU5BU5D_t165183403*)SZArrayNew(IDisposableU5BU5D_t165183403_il2cpp_TypeInfo_var, (uint32_t)7));
		Il2CppObject * L_43 = V_0;
		NullCheck(L_42);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_42, 0);
		ArrayElementTypeCheck (L_42, L_43);
		(L_42)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)L_43);
		IDisposableU5BU5D_t165183403* L_44 = (IDisposableU5BU5D_t165183403*)L_42;
		Il2CppObject * L_45 = V_1;
		NullCheck(L_44);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_44, 1);
		ArrayElementTypeCheck (L_44, L_45);
		(L_44)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_45);
		IDisposableU5BU5D_t165183403* L_46 = (IDisposableU5BU5D_t165183403*)L_44;
		Il2CppObject * L_47 = V_2;
		NullCheck(L_46);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_46, 2);
		ArrayElementTypeCheck (L_46, L_47);
		(L_46)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)L_47);
		IDisposableU5BU5D_t165183403* L_48 = (IDisposableU5BU5D_t165183403*)L_46;
		Il2CppObject * L_49 = V_3;
		NullCheck(L_48);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_48, 3);
		ArrayElementTypeCheck (L_48, L_49);
		(L_48)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_49);
		IDisposableU5BU5D_t165183403* L_50 = (IDisposableU5BU5D_t165183403*)L_48;
		Il2CppObject * L_51 = V_4;
		NullCheck(L_50);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_50, 4);
		ArrayElementTypeCheck (L_50, L_51);
		(L_50)->SetAt(static_cast<il2cpp_array_size_t>(4), (Il2CppObject *)L_51);
		IDisposableU5BU5D_t165183403* L_52 = (IDisposableU5BU5D_t165183403*)L_50;
		Il2CppObject * L_53 = V_5;
		NullCheck(L_52);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_52, 5);
		ArrayElementTypeCheck (L_52, L_53);
		(L_52)->SetAt(static_cast<il2cpp_array_size_t>(5), (Il2CppObject *)L_53);
		IDisposableU5BU5D_t165183403* L_54 = (IDisposableU5BU5D_t165183403*)L_52;
		Il2CppObject * L_55 = V_6;
		NullCheck(L_54);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_54, 6);
		ArrayElementTypeCheck (L_54, L_55);
		(L_54)->SetAt(static_cast<il2cpp_array_size_t>(6), (Il2CppObject *)L_55);
		Il2CppObject * L_56 = StableCompositeDisposable_Create_m1215138723(NULL /*static, unused*/, (IDisposableU5BU5D_t165183403*)L_54, /*hidden argument*/NULL);
		return L_56;
	}
}
// TR UniRx.Operators.CombineLatestObservable`8/CombineLatest<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>::GetResult()
extern "C"  Il2CppObject * CombineLatest_GetResult_m885051964_gshared (CombineLatest_t1931772513 * __this, const MethodInfo* method)
{
	{
		CombineLatestObservable_8_t129076473 * L_0 = (CombineLatestObservable_8_t129076473 *)__this->get_parent_6();
		NullCheck(L_0);
		CombineLatestFunc_8_t584396980 * L_1 = (CombineLatestFunc_8_t584396980 *)L_0->get_resultSelector_8();
		CombineLatestObserver_1_t123567747 * L_2 = (CombineLatestObserver_1_t123567747 *)__this->get_c1_8();
		NullCheck((CombineLatestObserver_1_t123567747 *)L_2);
		Il2CppObject * L_3 = ((  Il2CppObject * (*) (CombineLatestObserver_1_t123567747 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 24)->method)((CombineLatestObserver_1_t123567747 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 24));
		CombineLatestObserver_1_t123567747 * L_4 = (CombineLatestObserver_1_t123567747 *)__this->get_c2_9();
		NullCheck((CombineLatestObserver_1_t123567747 *)L_4);
		Il2CppObject * L_5 = ((  Il2CppObject * (*) (CombineLatestObserver_1_t123567747 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 25)->method)((CombineLatestObserver_1_t123567747 *)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 25));
		CombineLatestObserver_1_t123567747 * L_6 = (CombineLatestObserver_1_t123567747 *)__this->get_c3_10();
		NullCheck((CombineLatestObserver_1_t123567747 *)L_6);
		Il2CppObject * L_7 = ((  Il2CppObject * (*) (CombineLatestObserver_1_t123567747 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 26)->method)((CombineLatestObserver_1_t123567747 *)L_6, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 26));
		CombineLatestObserver_1_t123567747 * L_8 = (CombineLatestObserver_1_t123567747 *)__this->get_c4_11();
		NullCheck((CombineLatestObserver_1_t123567747 *)L_8);
		Il2CppObject * L_9 = ((  Il2CppObject * (*) (CombineLatestObserver_1_t123567747 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 27)->method)((CombineLatestObserver_1_t123567747 *)L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 27));
		CombineLatestObserver_1_t123567747 * L_10 = (CombineLatestObserver_1_t123567747 *)__this->get_c5_12();
		NullCheck((CombineLatestObserver_1_t123567747 *)L_10);
		Il2CppObject * L_11 = ((  Il2CppObject * (*) (CombineLatestObserver_1_t123567747 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 28)->method)((CombineLatestObserver_1_t123567747 *)L_10, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 28));
		CombineLatestObserver_1_t123567747 * L_12 = (CombineLatestObserver_1_t123567747 *)__this->get_c6_13();
		NullCheck((CombineLatestObserver_1_t123567747 *)L_12);
		Il2CppObject * L_13 = ((  Il2CppObject * (*) (CombineLatestObserver_1_t123567747 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 29)->method)((CombineLatestObserver_1_t123567747 *)L_12, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 29));
		CombineLatestObserver_1_t123567747 * L_14 = (CombineLatestObserver_1_t123567747 *)__this->get_c7_14();
		NullCheck((CombineLatestObserver_1_t123567747 *)L_14);
		Il2CppObject * L_15 = ((  Il2CppObject * (*) (CombineLatestObserver_1_t123567747 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 30)->method)((CombineLatestObserver_1_t123567747 *)L_14, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 30));
		NullCheck((CombineLatestFunc_8_t584396980 *)L_1);
		Il2CppObject * L_16 = ((  Il2CppObject * (*) (CombineLatestFunc_8_t584396980 *, Il2CppObject *, Il2CppObject *, Il2CppObject *, Il2CppObject *, Il2CppObject *, Il2CppObject *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 31)->method)((CombineLatestFunc_8_t584396980 *)L_1, (Il2CppObject *)L_3, (Il2CppObject *)L_5, (Il2CppObject *)L_7, (Il2CppObject *)L_9, (Il2CppObject *)L_11, (Il2CppObject *)L_13, (Il2CppObject *)L_15, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 31));
		return L_16;
	}
}
// System.Void UniRx.Operators.CombineLatestObservable`8/CombineLatest<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>::OnNext(TR)
extern "C"  void CombineLatest_OnNext_m1119340454_gshared (CombineLatest_t1931772513 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = (Il2CppObject*)((OperatorObserverBase_2_t1187768149 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		Il2CppObject * L_1 = ___value0;
		NullCheck((Il2CppObject*)L_0);
		InterfaceActionInvoker1< Il2CppObject * >::Invoke(2 /* System.Void UniRx.IObserver`1<System.Object>::OnNext(T) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 32), (Il2CppObject*)L_0, (Il2CppObject *)L_1);
		return;
	}
}
// System.Void UniRx.Operators.CombineLatestObservable`8/CombineLatest<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>::OnError(System.Exception)
extern "C"  void CombineLatest_OnError_m2687122377_gshared (CombineLatest_t1931772513 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method)
{
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		Il2CppObject* L_0 = (Il2CppObject*)((OperatorObserverBase_2_t1187768149 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		Exception_t1967233988 * L_1 = ___error0;
		NullCheck((Il2CppObject*)L_0);
		InterfaceActionInvoker1< Exception_t1967233988 * >::Invoke(1 /* System.Void UniRx.IObserver`1<System.Object>::OnError(System.Exception) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 32), (Il2CppObject*)L_0, (Exception_t1967233988 *)L_1);
		IL2CPP_LEAVE(0x1A, FINALLY_0013);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0013;
	}

FINALLY_0013:
	{ // begin finally (depth: 1)
		NullCheck((OperatorObserverBase_2_t1187768149 *)__this);
		VirtActionInvoker0::Invoke(4 /* System.Void UniRx.Operators.OperatorObserverBase`2<System.Object,System.Object>::Dispose() */, (OperatorObserverBase_2_t1187768149 *)__this);
		IL2CPP_END_FINALLY(19)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(19)
	{
		IL2CPP_JUMP_TBL(0x1A, IL_001a)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_001a:
	{
		return;
	}
}
// System.Void UniRx.Operators.CombineLatestObservable`8/CombineLatest<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>::OnCompleted()
extern "C"  void CombineLatest_OnCompleted_m1533383836_gshared (CombineLatest_t1931772513 * __this, const MethodInfo* method)
{
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		Il2CppObject* L_0 = (Il2CppObject*)((OperatorObserverBase_2_t1187768149 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		NullCheck((Il2CppObject*)L_0);
		InterfaceActionInvoker0::Invoke(0 /* System.Void UniRx.IObserver`1<System.Object>::OnCompleted() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 32), (Il2CppObject*)L_0);
		IL2CPP_LEAVE(0x19, FINALLY_0012);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0012;
	}

FINALLY_0012:
	{ // begin finally (depth: 1)
		NullCheck((OperatorObserverBase_2_t1187768149 *)__this);
		VirtActionInvoker0::Invoke(4 /* System.Void UniRx.Operators.OperatorObserverBase`2<System.Object,System.Object>::Dispose() */, (OperatorObserverBase_2_t1187768149 *)__this);
		IL2CPP_END_FINALLY(18)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(18)
	{
		IL2CPP_JUMP_TBL(0x19, IL_0019)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0019:
	{
		return;
	}
}
// System.Void UniRx.Operators.CombineLatestObservable`8<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>::.ctor(UniRx.IObservable`1<T1>,UniRx.IObservable`1<T2>,UniRx.IObservable`1<T3>,UniRx.IObservable`1<T4>,UniRx.IObservable`1<T5>,UniRx.IObservable`1<T6>,UniRx.IObservable`1<T7>,UniRx.Operators.CombineLatestFunc`8<T1,T2,T3,T4,T5,T6,T7,TR>)
extern "C"  void CombineLatestObservable_8__ctor_m2856080979_gshared (CombineLatestObservable_8_t129076473 * __this, Il2CppObject* ___source10, Il2CppObject* ___source21, Il2CppObject* ___source32, Il2CppObject* ___source43, Il2CppObject* ___source54, Il2CppObject* ___source65, Il2CppObject* ___source76, CombineLatestFunc_8_t584396980 * ___resultSelector7, const MethodInfo* method)
{
	CombineLatestObservable_8_t129076473 * G_B8_0 = NULL;
	CombineLatestObservable_8_t129076473 * G_B1_0 = NULL;
	CombineLatestObservable_8_t129076473 * G_B2_0 = NULL;
	CombineLatestObservable_8_t129076473 * G_B3_0 = NULL;
	CombineLatestObservable_8_t129076473 * G_B4_0 = NULL;
	CombineLatestObservable_8_t129076473 * G_B5_0 = NULL;
	CombineLatestObservable_8_t129076473 * G_B6_0 = NULL;
	CombineLatestObservable_8_t129076473 * G_B7_0 = NULL;
	int32_t G_B9_0 = 0;
	CombineLatestObservable_8_t129076473 * G_B9_1 = NULL;
	{
		Il2CppObject* L_0 = ___source10;
		bool L_1 = ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)(NULL /*static, unused*/, (Il2CppObject*)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		G_B1_0 = ((CombineLatestObservable_8_t129076473 *)(__this));
		if (L_1)
		{
			G_B8_0 = ((CombineLatestObservable_8_t129076473 *)(__this));
			goto IL_0055;
		}
	}
	{
		Il2CppObject* L_2 = ___source21;
		bool L_3 = ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)(NULL /*static, unused*/, (Il2CppObject*)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		G_B2_0 = ((CombineLatestObservable_8_t129076473 *)(G_B1_0));
		if (L_3)
		{
			G_B8_0 = ((CombineLatestObservable_8_t129076473 *)(G_B1_0));
			goto IL_0055;
		}
	}
	{
		Il2CppObject* L_4 = ___source32;
		bool L_5 = ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)(NULL /*static, unused*/, (Il2CppObject*)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		G_B3_0 = ((CombineLatestObservable_8_t129076473 *)(G_B2_0));
		if (L_5)
		{
			G_B8_0 = ((CombineLatestObservable_8_t129076473 *)(G_B2_0));
			goto IL_0055;
		}
	}
	{
		Il2CppObject* L_6 = ___source43;
		bool L_7 = ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)(NULL /*static, unused*/, (Il2CppObject*)L_6, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		G_B4_0 = ((CombineLatestObservable_8_t129076473 *)(G_B3_0));
		if (L_7)
		{
			G_B8_0 = ((CombineLatestObservable_8_t129076473 *)(G_B3_0));
			goto IL_0055;
		}
	}
	{
		Il2CppObject* L_8 = ___source54;
		bool L_9 = ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)(NULL /*static, unused*/, (Il2CppObject*)L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		G_B5_0 = ((CombineLatestObservable_8_t129076473 *)(G_B4_0));
		if (L_9)
		{
			G_B8_0 = ((CombineLatestObservable_8_t129076473 *)(G_B4_0));
			goto IL_0055;
		}
	}
	{
		Il2CppObject* L_10 = ___source65;
		bool L_11 = ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->method)(NULL /*static, unused*/, (Il2CppObject*)L_10, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		G_B6_0 = ((CombineLatestObservable_8_t129076473 *)(G_B5_0));
		if (L_11)
		{
			G_B8_0 = ((CombineLatestObservable_8_t129076473 *)(G_B5_0));
			goto IL_0055;
		}
	}
	{
		Il2CppObject* L_12 = ___source76;
		bool L_13 = ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6)->method)(NULL /*static, unused*/, (Il2CppObject*)L_12, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		G_B7_0 = ((CombineLatestObservable_8_t129076473 *)(G_B6_0));
		if (L_13)
		{
			G_B8_0 = ((CombineLatestObservable_8_t129076473 *)(G_B6_0));
			goto IL_0055;
		}
	}
	{
		G_B9_0 = 0;
		G_B9_1 = ((CombineLatestObservable_8_t129076473 *)(G_B7_0));
		goto IL_0056;
	}

IL_0055:
	{
		G_B9_0 = 1;
		G_B9_1 = ((CombineLatestObservable_8_t129076473 *)(G_B8_0));
	}

IL_0056:
	{
		NullCheck((OperatorObservableBase_1_t4196218687 *)G_B9_1);
		((  void (*) (OperatorObservableBase_1_t4196218687 *, bool, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7)->method)((OperatorObservableBase_1_t4196218687 *)G_B9_1, (bool)G_B9_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7));
		Il2CppObject* L_14 = ___source10;
		__this->set_source1_1(L_14);
		Il2CppObject* L_15 = ___source21;
		__this->set_source2_2(L_15);
		Il2CppObject* L_16 = ___source32;
		__this->set_source3_3(L_16);
		Il2CppObject* L_17 = ___source43;
		__this->set_source4_4(L_17);
		Il2CppObject* L_18 = ___source54;
		__this->set_source5_5(L_18);
		Il2CppObject* L_19 = ___source65;
		__this->set_source6_6(L_19);
		Il2CppObject* L_20 = ___source76;
		__this->set_source7_7(L_20);
		CombineLatestFunc_8_t584396980 * L_21 = ___resultSelector7;
		__this->set_resultSelector_8(L_21);
		return;
	}
}
// System.IDisposable UniRx.Operators.CombineLatestObservable`8<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>::SubscribeCore(UniRx.IObserver`1<TR>,System.IDisposable)
extern "C"  Il2CppObject * CombineLatestObservable_8_SubscribeCore_m3033845357_gshared (CombineLatestObservable_8_t129076473 * __this, Il2CppObject* ___observer0, Il2CppObject * ___cancel1, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = ___observer0;
		Il2CppObject * L_1 = ___cancel1;
		CombineLatest_t1931772513 * L_2 = (CombineLatest_t1931772513 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9));
		((  void (*) (CombineLatest_t1931772513 *, int32_t, CombineLatestObservable_8_t129076473 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10)->method)(L_2, (int32_t)7, (CombineLatestObservable_8_t129076473 *)__this, (Il2CppObject*)L_0, (Il2CppObject *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10));
		NullCheck((CombineLatest_t1931772513 *)L_2);
		Il2CppObject * L_3 = ((  Il2CppObject * (*) (CombineLatest_t1931772513 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 11)->method)((CombineLatest_t1931772513 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 11));
		return L_3;
	}
}
// System.Void UniRx.Operators.CombineLatestObserver`1<System.Boolean>::.ctor(System.Object,UniRx.Operators.ICombineLatestObservable,System.Int32)
extern "C"  void CombineLatestObserver_1__ctor_m4220137716_gshared (CombineLatestObserver_1_t3792433964 * __this, Il2CppObject * ___gate0, Il2CppObject * ___parent1, int32_t ___index2, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		Il2CppObject * L_0 = ___gate0;
		__this->set_gate_0(L_0);
		Il2CppObject * L_1 = ___parent1;
		__this->set_parent_1(L_1);
		int32_t L_2 = ___index2;
		__this->set_index_2(L_2);
		return;
	}
}
// T UniRx.Operators.CombineLatestObserver`1<System.Boolean>::get_Value()
extern "C"  bool CombineLatestObserver_1_get_Value_m2167492077_gshared (CombineLatestObserver_1_t3792433964 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (bool)__this->get_value_3();
		return L_0;
	}
}
// System.Void UniRx.Operators.CombineLatestObserver`1<System.Boolean>::OnNext(T)
extern Il2CppClass* ICombineLatestObservable_t195534285_il2cpp_TypeInfo_var;
extern const uint32_t CombineLatestObserver_1_OnNext_m3731548080_MetadataUsageId;
extern "C"  void CombineLatestObserver_1_OnNext_m3731548080_gshared (CombineLatestObserver_1_t3792433964 * __this, bool ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (CombineLatestObserver_1_OnNext_m3731548080_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Il2CppObject * V_0 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_gate_0();
		V_0 = (Il2CppObject *)L_0;
		Il2CppObject * L_1 = V_0;
		Monitor_Enter_m476686225(NULL /*static, unused*/, (Il2CppObject *)L_1, /*hidden argument*/NULL);
	}

IL_000d:
	try
	{ // begin try (depth: 1)
		bool L_2 = ___value0;
		__this->set_value_3(L_2);
		Il2CppObject * L_3 = (Il2CppObject *)__this->get_parent_1();
		int32_t L_4 = (int32_t)__this->get_index_2();
		NullCheck((Il2CppObject *)L_3);
		InterfaceActionInvoker1< int32_t >::Invoke(0 /* System.Void UniRx.Operators.ICombineLatestObservable::Publish(System.Int32) */, ICombineLatestObservable_t195534285_il2cpp_TypeInfo_var, (Il2CppObject *)L_3, (int32_t)L_4);
		IL2CPP_LEAVE(0x31, FINALLY_002a);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_002a;
	}

FINALLY_002a:
	{ // begin finally (depth: 1)
		Il2CppObject * L_5 = V_0;
		Monitor_Exit_m2088237919(NULL /*static, unused*/, (Il2CppObject *)L_5, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(42)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(42)
	{
		IL2CPP_JUMP_TBL(0x31, IL_0031)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0031:
	{
		return;
	}
}
// System.Void UniRx.Operators.CombineLatestObserver`1<System.Boolean>::OnError(System.Exception)
extern Il2CppClass* ICombineLatestObservable_t195534285_il2cpp_TypeInfo_var;
extern const uint32_t CombineLatestObserver_1_OnError_m1909039807_MetadataUsageId;
extern "C"  void CombineLatestObserver_1_OnError_m1909039807_gshared (CombineLatestObserver_1_t3792433964 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (CombineLatestObserver_1_OnError_m1909039807_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Il2CppObject * V_0 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_gate_0();
		V_0 = (Il2CppObject *)L_0;
		Il2CppObject * L_1 = V_0;
		Monitor_Enter_m476686225(NULL /*static, unused*/, (Il2CppObject *)L_1, /*hidden argument*/NULL);
	}

IL_000d:
	try
	{ // begin try (depth: 1)
		Il2CppObject * L_2 = (Il2CppObject *)__this->get_parent_1();
		Exception_t1967233988 * L_3 = ___error0;
		NullCheck((Il2CppObject *)L_2);
		InterfaceActionInvoker1< Exception_t1967233988 * >::Invoke(1 /* System.Void UniRx.Operators.ICombineLatestObservable::Fail(System.Exception) */, ICombineLatestObservable_t195534285_il2cpp_TypeInfo_var, (Il2CppObject *)L_2, (Exception_t1967233988 *)L_3);
		IL2CPP_LEAVE(0x25, FINALLY_001e);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_001e;
	}

FINALLY_001e:
	{ // begin finally (depth: 1)
		Il2CppObject * L_4 = V_0;
		Monitor_Exit_m2088237919(NULL /*static, unused*/, (Il2CppObject *)L_4, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(30)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(30)
	{
		IL2CPP_JUMP_TBL(0x25, IL_0025)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0025:
	{
		return;
	}
}
// System.Void UniRx.Operators.CombineLatestObserver`1<System.Boolean>::OnCompleted()
extern Il2CppClass* ICombineLatestObservable_t195534285_il2cpp_TypeInfo_var;
extern const uint32_t CombineLatestObserver_1_OnCompleted_m2942254226_MetadataUsageId;
extern "C"  void CombineLatestObserver_1_OnCompleted_m2942254226_gshared (CombineLatestObserver_1_t3792433964 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (CombineLatestObserver_1_OnCompleted_m2942254226_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Il2CppObject * V_0 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_gate_0();
		V_0 = (Il2CppObject *)L_0;
		Il2CppObject * L_1 = V_0;
		Monitor_Enter_m476686225(NULL /*static, unused*/, (Il2CppObject *)L_1, /*hidden argument*/NULL);
	}

IL_000d:
	try
	{ // begin try (depth: 1)
		Il2CppObject * L_2 = (Il2CppObject *)__this->get_parent_1();
		int32_t L_3 = (int32_t)__this->get_index_2();
		NullCheck((Il2CppObject *)L_2);
		InterfaceActionInvoker1< int32_t >::Invoke(2 /* System.Void UniRx.Operators.ICombineLatestObservable::Done(System.Int32) */, ICombineLatestObservable_t195534285_il2cpp_TypeInfo_var, (Il2CppObject *)L_2, (int32_t)L_3);
		IL2CPP_LEAVE(0x2A, FINALLY_0023);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0023;
	}

FINALLY_0023:
	{ // begin finally (depth: 1)
		Il2CppObject * L_4 = V_0;
		Monitor_Exit_m2088237919(NULL /*static, unused*/, (Il2CppObject *)L_4, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(35)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(35)
	{
		IL2CPP_JUMP_TBL(0x2A, IL_002a)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_002a:
	{
		return;
	}
}
// System.Void UniRx.Operators.CombineLatestObserver`1<System.Object>::.ctor(System.Object,UniRx.Operators.ICombineLatestObservable,System.Int32)
extern "C"  void CombineLatestObserver_1__ctor_m3474797587_gshared (CombineLatestObserver_1_t123567747 * __this, Il2CppObject * ___gate0, Il2CppObject * ___parent1, int32_t ___index2, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		Il2CppObject * L_0 = ___gate0;
		__this->set_gate_0(L_0);
		Il2CppObject * L_1 = ___parent1;
		__this->set_parent_1(L_1);
		int32_t L_2 = ___index2;
		__this->set_index_2(L_2);
		return;
	}
}
// T UniRx.Operators.CombineLatestObserver`1<System.Object>::get_Value()
extern "C"  Il2CppObject * CombineLatestObserver_1_get_Value_m1213727952_gshared (CombineLatestObserver_1_t123567747 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_value_3();
		return L_0;
	}
}
// System.Void UniRx.Operators.CombineLatestObserver`1<System.Object>::OnNext(T)
extern Il2CppClass* ICombineLatestObservable_t195534285_il2cpp_TypeInfo_var;
extern const uint32_t CombineLatestObserver_1_OnNext_m1758727345_MetadataUsageId;
extern "C"  void CombineLatestObserver_1_OnNext_m1758727345_gshared (CombineLatestObserver_1_t123567747 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (CombineLatestObserver_1_OnNext_m1758727345_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Il2CppObject * V_0 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_gate_0();
		V_0 = (Il2CppObject *)L_0;
		Il2CppObject * L_1 = V_0;
		Monitor_Enter_m476686225(NULL /*static, unused*/, (Il2CppObject *)L_1, /*hidden argument*/NULL);
	}

IL_000d:
	try
	{ // begin try (depth: 1)
		Il2CppObject * L_2 = ___value0;
		__this->set_value_3(L_2);
		Il2CppObject * L_3 = (Il2CppObject *)__this->get_parent_1();
		int32_t L_4 = (int32_t)__this->get_index_2();
		NullCheck((Il2CppObject *)L_3);
		InterfaceActionInvoker1< int32_t >::Invoke(0 /* System.Void UniRx.Operators.ICombineLatestObservable::Publish(System.Int32) */, ICombineLatestObservable_t195534285_il2cpp_TypeInfo_var, (Il2CppObject *)L_3, (int32_t)L_4);
		IL2CPP_LEAVE(0x31, FINALLY_002a);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_002a;
	}

FINALLY_002a:
	{ // begin finally (depth: 1)
		Il2CppObject * L_5 = V_0;
		Monitor_Exit_m2088237919(NULL /*static, unused*/, (Il2CppObject *)L_5, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(42)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(42)
	{
		IL2CPP_JUMP_TBL(0x31, IL_0031)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0031:
	{
		return;
	}
}
// System.Void UniRx.Operators.CombineLatestObserver`1<System.Object>::OnError(System.Exception)
extern Il2CppClass* ICombineLatestObservable_t195534285_il2cpp_TypeInfo_var;
extern const uint32_t CombineLatestObserver_1_OnError_m3891439040_MetadataUsageId;
extern "C"  void CombineLatestObserver_1_OnError_m3891439040_gshared (CombineLatestObserver_1_t123567747 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (CombineLatestObserver_1_OnError_m3891439040_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Il2CppObject * V_0 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_gate_0();
		V_0 = (Il2CppObject *)L_0;
		Il2CppObject * L_1 = V_0;
		Monitor_Enter_m476686225(NULL /*static, unused*/, (Il2CppObject *)L_1, /*hidden argument*/NULL);
	}

IL_000d:
	try
	{ // begin try (depth: 1)
		Il2CppObject * L_2 = (Il2CppObject *)__this->get_parent_1();
		Exception_t1967233988 * L_3 = ___error0;
		NullCheck((Il2CppObject *)L_2);
		InterfaceActionInvoker1< Exception_t1967233988 * >::Invoke(1 /* System.Void UniRx.Operators.ICombineLatestObservable::Fail(System.Exception) */, ICombineLatestObservable_t195534285_il2cpp_TypeInfo_var, (Il2CppObject *)L_2, (Exception_t1967233988 *)L_3);
		IL2CPP_LEAVE(0x25, FINALLY_001e);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_001e;
	}

FINALLY_001e:
	{ // begin finally (depth: 1)
		Il2CppObject * L_4 = V_0;
		Monitor_Exit_m2088237919(NULL /*static, unused*/, (Il2CppObject *)L_4, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(30)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(30)
	{
		IL2CPP_JUMP_TBL(0x25, IL_0025)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0025:
	{
		return;
	}
}
// System.Void UniRx.Operators.CombineLatestObserver`1<System.Object>::OnCompleted()
extern Il2CppClass* ICombineLatestObservable_t195534285_il2cpp_TypeInfo_var;
extern const uint32_t CombineLatestObserver_1_OnCompleted_m3871078675_MetadataUsageId;
extern "C"  void CombineLatestObserver_1_OnCompleted_m3871078675_gshared (CombineLatestObserver_1_t123567747 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (CombineLatestObserver_1_OnCompleted_m3871078675_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Il2CppObject * V_0 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_gate_0();
		V_0 = (Il2CppObject *)L_0;
		Il2CppObject * L_1 = V_0;
		Monitor_Enter_m476686225(NULL /*static, unused*/, (Il2CppObject *)L_1, /*hidden argument*/NULL);
	}

IL_000d:
	try
	{ // begin try (depth: 1)
		Il2CppObject * L_2 = (Il2CppObject *)__this->get_parent_1();
		int32_t L_3 = (int32_t)__this->get_index_2();
		NullCheck((Il2CppObject *)L_2);
		InterfaceActionInvoker1< int32_t >::Invoke(2 /* System.Void UniRx.Operators.ICombineLatestObservable::Done(System.Int32) */, ICombineLatestObservable_t195534285_il2cpp_TypeInfo_var, (Il2CppObject *)L_2, (int32_t)L_3);
		IL2CPP_LEAVE(0x2A, FINALLY_0023);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0023;
	}

FINALLY_0023:
	{ // begin finally (depth: 1)
		Il2CppObject * L_4 = V_0;
		Monitor_Exit_m2088237919(NULL /*static, unused*/, (Il2CppObject *)L_4, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(35)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(35)
	{
		IL2CPP_JUMP_TBL(0x2A, IL_002a)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_002a:
	{
		return;
	}
}
// System.Void UniRx.Operators.ConcatObservable`1/<CombineSources>c__Iterator18<System.Object>::.ctor()
extern "C"  void U3CCombineSourcesU3Ec__Iterator18__ctor_m845918298_gshared (U3CCombineSourcesU3Ec__Iterator18_t1596040496 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// UniRx.IObservable`1<T> UniRx.Operators.ConcatObservable`1/<CombineSources>c__Iterator18<System.Object>::System.Collections.Generic.IEnumerator<UniRx.IObservable<T>>.get_Current()
extern "C"  Il2CppObject* U3CCombineSourcesU3Ec__Iterator18_System_Collections_Generic_IEnumeratorU3CUniRx_IObservableU3CTU3EU3E_get_Current_m84683126_gshared (U3CCombineSourcesU3Ec__Iterator18_t1596040496 * __this, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = (Il2CppObject*)__this->get_U24current_7();
		return L_0;
	}
}
// System.Object UniRx.Operators.ConcatObservable`1/<CombineSources>c__Iterator18<System.Object>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CCombineSourcesU3Ec__Iterator18_System_Collections_IEnumerator_get_Current_m2440641484_gshared (U3CCombineSourcesU3Ec__Iterator18_t1596040496 * __this, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = (Il2CppObject*)__this->get_U24current_7();
		return L_0;
	}
}
// System.Collections.IEnumerator UniRx.Operators.ConcatObservable`1/<CombineSources>c__Iterator18<System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * U3CCombineSourcesU3Ec__Iterator18_System_Collections_IEnumerable_GetEnumerator_m933254623_gshared (U3CCombineSourcesU3Ec__Iterator18_t1596040496 * __this, const MethodInfo* method)
{
	{
		NullCheck((U3CCombineSourcesU3Ec__Iterator18_t1596040496 *)__this);
		Il2CppObject* L_0 = ((  Il2CppObject* (*) (U3CCombineSourcesU3Ec__Iterator18_t1596040496 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((U3CCombineSourcesU3Ec__Iterator18_t1596040496 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		return L_0;
	}
}
// System.Collections.Generic.IEnumerator`1<UniRx.IObservable`1<T>> UniRx.Operators.ConcatObservable`1/<CombineSources>c__Iterator18<System.Object>::System.Collections.Generic.IEnumerable<UniRx.IObservable<T>>.GetEnumerator()
extern "C"  Il2CppObject* U3CCombineSourcesU3Ec__Iterator18_System_Collections_Generic_IEnumerableU3CUniRx_IObservableU3CTU3EU3E_GetEnumerator_m1598665587_gshared (U3CCombineSourcesU3Ec__Iterator18_t1596040496 * __this, const MethodInfo* method)
{
	U3CCombineSourcesU3Ec__Iterator18_t1596040496 * V_0 = NULL;
	{
		int32_t* L_0 = (int32_t*)__this->get_address_of_U24PC_6();
		int32_t L_1 = Interlocked_CompareExchange_m1859820752(NULL /*static, unused*/, (int32_t*)L_0, (int32_t)0, (int32_t)((int32_t)-2), /*hidden argument*/NULL);
		if ((!(((uint32_t)L_1) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0014;
		}
	}
	{
		return __this;
	}

IL_0014:
	{
		U3CCombineSourcesU3Ec__Iterator18_t1596040496 * L_2 = (U3CCombineSourcesU3Ec__Iterator18_t1596040496 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		((  void (*) (U3CCombineSourcesU3Ec__Iterator18_t1596040496 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)(L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		V_0 = (U3CCombineSourcesU3Ec__Iterator18_t1596040496 *)L_2;
		U3CCombineSourcesU3Ec__Iterator18_t1596040496 * L_3 = V_0;
		Il2CppObject* L_4 = (Il2CppObject*)__this->get_U3CU24U3Efirst_8();
		NullCheck(L_3);
		L_3->set_first_0(L_4);
		U3CCombineSourcesU3Ec__Iterator18_t1596040496 * L_5 = V_0;
		Il2CppObject* L_6 = (Il2CppObject*)__this->get_U3CU24U3Esecond_9();
		NullCheck(L_5);
		L_5->set_second_3(L_6);
		U3CCombineSourcesU3Ec__Iterator18_t1596040496 * L_7 = V_0;
		return L_7;
	}
}
// System.Boolean UniRx.Operators.ConcatObservable`1/<CombineSources>c__Iterator18<System.Object>::MoveNext()
extern Il2CppClass* IEnumerator_t287207039_il2cpp_TypeInfo_var;
extern Il2CppClass* IDisposable_t1628921374_il2cpp_TypeInfo_var;
extern const uint32_t U3CCombineSourcesU3Ec__Iterator18_MoveNext_m4290604914_MetadataUsageId;
extern "C"  bool U3CCombineSourcesU3Ec__Iterator18_MoveNext_m4290604914_gshared (U3CCombineSourcesU3Ec__Iterator18_t1596040496 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CCombineSourcesU3Ec__Iterator18_MoveNext_m4290604914_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	uint32_t V_0 = 0;
	bool V_1 = false;
	bool V_2 = false;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		int32_t L_0 = (int32_t)__this->get_U24PC_6();
		V_0 = (uint32_t)L_0;
		__this->set_U24PC_6((-1));
		V_1 = (bool)0;
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0027;
		}
		if (L_1 == 1)
		{
			goto IL_003b;
		}
		if (L_1 == 2)
		{
			goto IL_00b9;
		}
	}
	{
		goto IL_012a;
	}

IL_0027:
	{
		Il2CppObject* L_2 = (Il2CppObject*)__this->get_first_0();
		NullCheck((Il2CppObject*)L_2);
		Il2CppObject* L_3 = InterfaceFuncInvoker0< Il2CppObject* >::Invoke(0 /* System.Collections.Generic.IEnumerator`1<!0> System.Collections.Generic.IEnumerable`1<UniRx.IObservable`1<System.Object>>::GetEnumerator() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3), (Il2CppObject*)L_2);
		__this->set_U3CU24s_130U3E__0_1(L_3);
		V_0 = (uint32_t)((int32_t)-3);
	}

IL_003b:
	try
	{ // begin try (depth: 1)
		{
			uint32_t L_4 = V_0;
			if (((int32_t)((int32_t)L_4-(int32_t)1)) == 0)
			{
				goto IL_0077;
			}
		}

IL_0047:
		{
			goto IL_0077;
		}

IL_004c:
		{
			Il2CppObject* L_5 = (Il2CppObject*)__this->get_U3CU24s_130U3E__0_1();
			NullCheck((Il2CppObject*)L_5);
			Il2CppObject* L_6 = InterfaceFuncInvoker0< Il2CppObject* >::Invoke(0 /* !0 System.Collections.Generic.IEnumerator`1<UniRx.IObservable`1<System.Object>>::get_Current() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4), (Il2CppObject*)L_5);
			__this->set_U3CitemU3E__1_2(L_6);
			Il2CppObject* L_7 = (Il2CppObject*)__this->get_U3CitemU3E__1_2();
			__this->set_U24current_7(L_7);
			__this->set_U24PC_6(1);
			V_1 = (bool)1;
			IL2CPP_LEAVE(0x12C, FINALLY_008c);
		}

IL_0077:
		{
			Il2CppObject* L_8 = (Il2CppObject*)__this->get_U3CU24s_130U3E__0_1();
			NullCheck((Il2CppObject *)L_8);
			bool L_9 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t287207039_il2cpp_TypeInfo_var, (Il2CppObject *)L_8);
			if (L_9)
			{
				goto IL_004c;
			}
		}

IL_0087:
		{
			IL2CPP_LEAVE(0xA5, FINALLY_008c);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_008c;
	}

FINALLY_008c:
	{ // begin finally (depth: 1)
		{
			bool L_10 = V_1;
			if (!L_10)
			{
				goto IL_0090;
			}
		}

IL_008f:
		{
			IL2CPP_END_FINALLY(140)
		}

IL_0090:
		{
			Il2CppObject* L_11 = (Il2CppObject*)__this->get_U3CU24s_130U3E__0_1();
			if (L_11)
			{
				goto IL_0099;
			}
		}

IL_0098:
		{
			IL2CPP_END_FINALLY(140)
		}

IL_0099:
		{
			Il2CppObject* L_12 = (Il2CppObject*)__this->get_U3CU24s_130U3E__0_1();
			NullCheck((Il2CppObject *)L_12);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, (Il2CppObject *)L_12);
			IL2CPP_END_FINALLY(140)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(140)
	{
		IL2CPP_JUMP_TBL(0x12C, IL_012c)
		IL2CPP_JUMP_TBL(0xA5, IL_00a5)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_00a5:
	{
		Il2CppObject* L_13 = (Il2CppObject*)__this->get_second_3();
		NullCheck((Il2CppObject*)L_13);
		Il2CppObject* L_14 = InterfaceFuncInvoker0< Il2CppObject* >::Invoke(0 /* System.Collections.Generic.IEnumerator`1<!0> System.Collections.Generic.IEnumerable`1<UniRx.IObservable`1<System.Object>>::GetEnumerator() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3), (Il2CppObject*)L_13);
		__this->set_U3CU24s_131U3E__2_4(L_14);
		V_0 = (uint32_t)((int32_t)-3);
	}

IL_00b9:
	try
	{ // begin try (depth: 1)
		{
			uint32_t L_15 = V_0;
			if (((int32_t)((int32_t)L_15-(int32_t)2)) == 0)
			{
				goto IL_00f5;
			}
		}

IL_00c5:
		{
			goto IL_00f5;
		}

IL_00ca:
		{
			Il2CppObject* L_16 = (Il2CppObject*)__this->get_U3CU24s_131U3E__2_4();
			NullCheck((Il2CppObject*)L_16);
			Il2CppObject* L_17 = InterfaceFuncInvoker0< Il2CppObject* >::Invoke(0 /* !0 System.Collections.Generic.IEnumerator`1<UniRx.IObservable`1<System.Object>>::get_Current() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4), (Il2CppObject*)L_16);
			__this->set_U3CitemU3E__3_5(L_17);
			Il2CppObject* L_18 = (Il2CppObject*)__this->get_U3CitemU3E__3_5();
			__this->set_U24current_7(L_18);
			__this->set_U24PC_6(2);
			V_1 = (bool)1;
			IL2CPP_LEAVE(0x12C, FINALLY_010a);
		}

IL_00f5:
		{
			Il2CppObject* L_19 = (Il2CppObject*)__this->get_U3CU24s_131U3E__2_4();
			NullCheck((Il2CppObject *)L_19);
			bool L_20 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t287207039_il2cpp_TypeInfo_var, (Il2CppObject *)L_19);
			if (L_20)
			{
				goto IL_00ca;
			}
		}

IL_0105:
		{
			IL2CPP_LEAVE(0x123, FINALLY_010a);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_010a;
	}

FINALLY_010a:
	{ // begin finally (depth: 1)
		{
			bool L_21 = V_1;
			if (!L_21)
			{
				goto IL_010e;
			}
		}

IL_010d:
		{
			IL2CPP_END_FINALLY(266)
		}

IL_010e:
		{
			Il2CppObject* L_22 = (Il2CppObject*)__this->get_U3CU24s_131U3E__2_4();
			if (L_22)
			{
				goto IL_0117;
			}
		}

IL_0116:
		{
			IL2CPP_END_FINALLY(266)
		}

IL_0117:
		{
			Il2CppObject* L_23 = (Il2CppObject*)__this->get_U3CU24s_131U3E__2_4();
			NullCheck((Il2CppObject *)L_23);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, (Il2CppObject *)L_23);
			IL2CPP_END_FINALLY(266)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(266)
	{
		IL2CPP_JUMP_TBL(0x12C, IL_012c)
		IL2CPP_JUMP_TBL(0x123, IL_0123)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0123:
	{
		__this->set_U24PC_6((-1));
	}

IL_012a:
	{
		return (bool)0;
	}

IL_012c:
	{
		return (bool)1;
	}
	// Dead block : IL_012e: ldloc.2
}
// System.Void UniRx.Operators.ConcatObservable`1/<CombineSources>c__Iterator18<System.Object>::Dispose()
extern Il2CppClass* IDisposable_t1628921374_il2cpp_TypeInfo_var;
extern const uint32_t U3CCombineSourcesU3Ec__Iterator18_Dispose_m1076948503_MetadataUsageId;
extern "C"  void U3CCombineSourcesU3Ec__Iterator18_Dispose_m1076948503_gshared (U3CCombineSourcesU3Ec__Iterator18_t1596040496 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CCombineSourcesU3Ec__Iterator18_Dispose_m1076948503_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	uint32_t V_0 = 0;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		int32_t L_0 = (int32_t)__this->get_U24PC_6();
		V_0 = (uint32_t)L_0;
		__this->set_U24PC_6((-1));
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_005e;
		}
		if (L_1 == 1)
		{
			goto IL_0025;
		}
		if (L_1 == 2)
		{
			goto IL_0044;
		}
	}
	{
		goto IL_005e;
	}

IL_0025:
	try
	{ // begin try (depth: 1)
		IL2CPP_LEAVE(0x3F, FINALLY_002a);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_002a;
	}

FINALLY_002a:
	{ // begin finally (depth: 1)
		{
			Il2CppObject* L_2 = (Il2CppObject*)__this->get_U3CU24s_130U3E__0_1();
			if (L_2)
			{
				goto IL_0033;
			}
		}

IL_0032:
		{
			IL2CPP_END_FINALLY(42)
		}

IL_0033:
		{
			Il2CppObject* L_3 = (Il2CppObject*)__this->get_U3CU24s_130U3E__0_1();
			NullCheck((Il2CppObject *)L_3);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, (Il2CppObject *)L_3);
			IL2CPP_END_FINALLY(42)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(42)
	{
		IL2CPP_JUMP_TBL(0x3F, IL_003f)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_003f:
	{
		goto IL_005e;
	}

IL_0044:
	try
	{ // begin try (depth: 1)
		IL2CPP_LEAVE(0x5E, FINALLY_0049);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0049;
	}

FINALLY_0049:
	{ // begin finally (depth: 1)
		{
			Il2CppObject* L_4 = (Il2CppObject*)__this->get_U3CU24s_131U3E__2_4();
			if (L_4)
			{
				goto IL_0052;
			}
		}

IL_0051:
		{
			IL2CPP_END_FINALLY(73)
		}

IL_0052:
		{
			Il2CppObject* L_5 = (Il2CppObject*)__this->get_U3CU24s_131U3E__2_4();
			NullCheck((Il2CppObject *)L_5);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, (Il2CppObject *)L_5);
			IL2CPP_END_FINALLY(73)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(73)
	{
		IL2CPP_JUMP_TBL(0x5E, IL_005e)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_005e:
	{
		return;
	}
}
// System.Void UniRx.Operators.ConcatObservable`1/<CombineSources>c__Iterator18<System.Object>::Reset()
extern Il2CppClass* NotSupportedException_t1374155497_il2cpp_TypeInfo_var;
extern const uint32_t U3CCombineSourcesU3Ec__Iterator18_Reset_m2787318535_MetadataUsageId;
extern "C"  void U3CCombineSourcesU3Ec__Iterator18_Reset_m2787318535_gshared (U3CCombineSourcesU3Ec__Iterator18_t1596040496 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CCombineSourcesU3Ec__Iterator18_Reset_m2787318535_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1374155497 * L_0 = (NotSupportedException_t1374155497 *)il2cpp_codegen_object_new(NotSupportedException_t1374155497_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m149930845(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void UniRx.Operators.ConcatObservable`1/Concat<System.Object>::.ctor(UniRx.Operators.ConcatObservable`1<T>,UniRx.IObserver`1<T>,System.IDisposable)
extern Il2CppClass* Il2CppObject_il2cpp_TypeInfo_var;
extern const uint32_t Concat__ctor_m2095294552_MetadataUsageId;
extern "C"  void Concat__ctor_m2095294552_gshared (Concat_t120413325 * __this, ConcatObservable_1_t1571046694 * ___parent0, Il2CppObject* ___observer1, Il2CppObject * ___cancel2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Concat__ctor_m2095294552_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject * L_0 = (Il2CppObject *)il2cpp_codegen_object_new(Il2CppObject_il2cpp_TypeInfo_var);
		Object__ctor_m1772956182(L_0, /*hidden argument*/NULL);
		__this->set_gate_3(L_0);
		Il2CppObject* L_1 = ___observer1;
		Il2CppObject * L_2 = ___cancel2;
		NullCheck((OperatorObserverBase_2_t1187768149 *)__this);
		((  void (*) (OperatorObserverBase_2_t1187768149 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((OperatorObserverBase_2_t1187768149 *)__this, (Il2CppObject*)L_1, (Il2CppObject *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		ConcatObservable_1_t1571046694 * L_3 = ___parent0;
		__this->set_parent_2(L_3);
		return;
	}
}
// System.IDisposable UniRx.Operators.ConcatObservable`1/Concat<System.Object>::Run()
extern Il2CppClass* SerialDisposable_t2547852742_il2cpp_TypeInfo_var;
extern Il2CppClass* Action_1_t585976652_il2cpp_TypeInfo_var;
extern Il2CppClass* Scheduler_t103934925_il2cpp_TypeInfo_var;
extern Il2CppClass* Action_t437523947_il2cpp_TypeInfo_var;
extern Il2CppClass* Disposable_t3736388786_il2cpp_TypeInfo_var;
extern const MethodInfo* Action_1__ctor_m3589476370_MethodInfo_var;
extern const uint32_t Concat_Run_m969624197_MetadataUsageId;
extern "C"  Il2CppObject * Concat_Run_m969624197_gshared (Concat_t120413325 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Concat_Run_m969624197_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Il2CppObject * V_0 = NULL;
	{
		__this->set_isDisposed_4((bool)0);
		ConcatObservable_1_t1571046694 * L_0 = (ConcatObservable_1_t1571046694 *)__this->get_parent_2();
		NullCheck(L_0);
		Il2CppObject* L_1 = (Il2CppObject*)L_0->get_sources_1();
		NullCheck((Il2CppObject*)L_1);
		Il2CppObject* L_2 = InterfaceFuncInvoker0< Il2CppObject* >::Invoke(0 /* System.Collections.Generic.IEnumerator`1<!0> System.Collections.Generic.IEnumerable`1<UniRx.IObservable`1<System.Object>>::GetEnumerator() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), (Il2CppObject*)L_1);
		__this->set_e_5(L_2);
		SerialDisposable_t2547852742 * L_3 = (SerialDisposable_t2547852742 *)il2cpp_codegen_object_new(SerialDisposable_t2547852742_il2cpp_TypeInfo_var);
		SerialDisposable__ctor_m2977852291(L_3, /*hidden argument*/NULL);
		__this->set_subscription_6(L_3);
		Il2CppObject * L_4 = DefaultSchedulers_get_TailRecursion_m2924964748(NULL /*static, unused*/, /*hidden argument*/NULL);
		IntPtr_t L_5;
		L_5.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		Action_1_t585976652 * L_6 = (Action_1_t585976652 *)il2cpp_codegen_object_new(Action_1_t585976652_il2cpp_TypeInfo_var);
		Action_1__ctor_m3589476370(L_6, (Il2CppObject *)__this, (IntPtr_t)L_5, /*hidden argument*/Action_1__ctor_m3589476370_MethodInfo_var);
		IL2CPP_RUNTIME_CLASS_INIT(Scheduler_t103934925_il2cpp_TypeInfo_var);
		Il2CppObject * L_7 = Scheduler_Schedule_m2919836901(NULL /*static, unused*/, (Il2CppObject *)L_4, (Action_1_t585976652 *)L_6, /*hidden argument*/NULL);
		V_0 = (Il2CppObject *)L_7;
		Il2CppObject * L_8 = V_0;
		SerialDisposable_t2547852742 * L_9 = (SerialDisposable_t2547852742 *)__this->get_subscription_6();
		IntPtr_t L_10;
		L_10.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		Action_t437523947 * L_11 = (Action_t437523947 *)il2cpp_codegen_object_new(Action_t437523947_il2cpp_TypeInfo_var);
		Action__ctor_m2957240604(L_11, (Il2CppObject *)__this, (IntPtr_t)L_10, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Disposable_t3736388786_il2cpp_TypeInfo_var);
		Il2CppObject * L_12 = Disposable_Create_m2910846009(NULL /*static, unused*/, (Action_t437523947 *)L_11, /*hidden argument*/NULL);
		Il2CppObject * L_13 = StableCompositeDisposable_Create_m2505029765(NULL /*static, unused*/, (Il2CppObject *)L_8, (Il2CppObject *)L_9, (Il2CppObject *)L_12, /*hidden argument*/NULL);
		return L_13;
	}
}
// System.Void UniRx.Operators.ConcatObservable`1/Concat<System.Object>::RecursiveRun(System.Action)
extern Il2CppClass* IEnumerator_t287207039_il2cpp_TypeInfo_var;
extern Il2CppClass* InvalidOperationException_t2420574324_il2cpp_TypeInfo_var;
extern Il2CppClass* IDisposable_t1628921374_il2cpp_TypeInfo_var;
extern Il2CppClass* Exception_t1967233988_il2cpp_TypeInfo_var;
extern Il2CppClass* SingleAssignmentDisposable_t2336378823_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2696517264;
extern const uint32_t Concat_RecursiveRun_m211724631_MetadataUsageId;
extern "C"  void Concat_RecursiveRun_m211724631_gshared (Concat_t120413325 * __this, Action_t437523947 * ___self0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Concat_RecursiveRun_m211724631_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Il2CppObject * V_0 = NULL;
	Il2CppObject* V_1 = NULL;
	bool V_2 = false;
	Exception_t1967233988 * V_3 = NULL;
	Exception_t1967233988 * V_4 = NULL;
	Il2CppObject* V_5 = NULL;
	SingleAssignmentDisposable_t2336378823 * V_6 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_gate_3();
		V_0 = (Il2CppObject *)L_0;
		Il2CppObject * L_1 = V_0;
		Monitor_Enter_m476686225(NULL /*static, unused*/, (Il2CppObject *)L_1, /*hidden argument*/NULL);
	}

IL_000d:
	try
	{ // begin try (depth: 1)
		{
			Action_t437523947 * L_2 = ___self0;
			__this->set_nextSelf_7(L_2);
			bool L_3 = (bool)__this->get_isDisposed_4();
			if (!L_3)
			{
				goto IL_0024;
			}
		}

IL_001f:
		{
			IL2CPP_LEAVE(0xFE, FINALLY_00f7);
		}

IL_0024:
		{
			V_1 = (Il2CppObject*)NULL;
			V_2 = (bool)0;
			V_3 = (Exception_t1967233988 *)NULL;
		}

IL_002a:
		try
		{ // begin try (depth: 2)
			{
				Il2CppObject* L_4 = (Il2CppObject*)__this->get_e_5();
				NullCheck((Il2CppObject *)L_4);
				bool L_5 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t287207039_il2cpp_TypeInfo_var, (Il2CppObject *)L_4);
				V_2 = (bool)L_5;
				bool L_6 = V_2;
				if (!L_6)
				{
					goto IL_005e;
				}
			}

IL_003c:
			{
				Il2CppObject* L_7 = (Il2CppObject*)__this->get_e_5();
				NullCheck((Il2CppObject*)L_7);
				Il2CppObject* L_8 = InterfaceFuncInvoker0< Il2CppObject* >::Invoke(0 /* !0 System.Collections.Generic.IEnumerator`1<UniRx.IObservable`1<System.Object>>::get_Current() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5), (Il2CppObject*)L_7);
				V_1 = (Il2CppObject*)L_8;
				Il2CppObject* L_9 = V_1;
				if (L_9)
				{
					goto IL_0059;
				}
			}

IL_004e:
			{
				InvalidOperationException_t2420574324 * L_10 = (InvalidOperationException_t2420574324 *)il2cpp_codegen_object_new(InvalidOperationException_t2420574324_il2cpp_TypeInfo_var);
				InvalidOperationException__ctor_m1485483280(L_10, (String_t*)_stringLiteral2696517264, /*hidden argument*/NULL);
				IL2CPP_RAISE_MANAGED_EXCEPTION(L_10);
			}

IL_0059:
			{
				goto IL_0069;
			}

IL_005e:
			{
				Il2CppObject* L_11 = (Il2CppObject*)__this->get_e_5();
				NullCheck((Il2CppObject *)L_11);
				InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, (Il2CppObject *)L_11);
			}

IL_0069:
			{
				goto IL_0083;
			}
		} // end try (depth: 2)
		catch(Il2CppExceptionWrapper& e)
		{
			__exception_local = (Exception_t1967233988 *)e.ex;
			if(il2cpp_codegen_class_is_assignable_from (Exception_t1967233988_il2cpp_TypeInfo_var, e.ex->object.klass))
				goto CATCH_006e;
			throw e;
		}

CATCH_006e:
		{ // begin catch(System.Exception)
			V_4 = (Exception_t1967233988 *)((Exception_t1967233988 *)__exception_local);
			Exception_t1967233988 * L_12 = V_4;
			V_3 = (Exception_t1967233988 *)L_12;
			Il2CppObject* L_13 = (Il2CppObject*)__this->get_e_5();
			NullCheck((Il2CppObject *)L_13);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, (Il2CppObject *)L_13);
			goto IL_0083;
		} // end catch (depth: 2)

IL_0083:
		{
			Exception_t1967233988 * L_14 = V_3;
			if (!L_14)
			{
				goto IL_00a8;
			}
		}

IL_0089:
		try
		{ // begin try (depth: 2)
			Il2CppObject* L_15 = (Il2CppObject*)((OperatorObserverBase_2_t1187768149 *)__this)->get_observer_0();
			il2cpp_codegen_memory_barrier();
			Exception_t1967233988 * L_16 = V_3;
			NullCheck((Il2CppObject*)L_15);
			InterfaceActionInvoker1< Exception_t1967233988 * >::Invoke(1 /* System.Void UniRx.IObserver`1<System.Object>::OnError(System.Exception) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6), (Il2CppObject*)L_15, (Exception_t1967233988 *)L_16);
			IL2CPP_LEAVE(0xA3, FINALLY_009c);
		} // end try (depth: 2)
		catch(Il2CppExceptionWrapper& e)
		{
			__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
			goto FINALLY_009c;
		}

FINALLY_009c:
		{ // begin finally (depth: 2)
			NullCheck((OperatorObserverBase_2_t1187768149 *)__this);
			VirtActionInvoker0::Invoke(4 /* System.Void UniRx.Operators.OperatorObserverBase`2<System.Object,System.Object>::Dispose() */, (OperatorObserverBase_2_t1187768149 *)__this);
			IL2CPP_END_FINALLY(156)
		} // end finally (depth: 2)
		IL2CPP_CLEANUP(156)
		{
			IL2CPP_JUMP_TBL(0xA3, IL_00a3)
			IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
		}

IL_00a3:
		{
			IL2CPP_LEAVE(0xFE, FINALLY_00f7);
		}

IL_00a8:
		{
			bool L_17 = V_2;
			if (L_17)
			{
				goto IL_00cc;
			}
		}

IL_00ae:
		try
		{ // begin try (depth: 2)
			Il2CppObject* L_18 = (Il2CppObject*)((OperatorObserverBase_2_t1187768149 *)__this)->get_observer_0();
			il2cpp_codegen_memory_barrier();
			NullCheck((Il2CppObject*)L_18);
			InterfaceActionInvoker0::Invoke(0 /* System.Void UniRx.IObserver`1<System.Object>::OnCompleted() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6), (Il2CppObject*)L_18);
			IL2CPP_LEAVE(0xC7, FINALLY_00c0);
		} // end try (depth: 2)
		catch(Il2CppExceptionWrapper& e)
		{
			__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
			goto FINALLY_00c0;
		}

FINALLY_00c0:
		{ // begin finally (depth: 2)
			NullCheck((OperatorObserverBase_2_t1187768149 *)__this);
			VirtActionInvoker0::Invoke(4 /* System.Void UniRx.Operators.OperatorObserverBase`2<System.Object,System.Object>::Dispose() */, (OperatorObserverBase_2_t1187768149 *)__this);
			IL2CPP_END_FINALLY(192)
		} // end finally (depth: 2)
		IL2CPP_CLEANUP(192)
		{
			IL2CPP_JUMP_TBL(0xC7, IL_00c7)
			IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
		}

IL_00c7:
		{
			IL2CPP_LEAVE(0xFE, FINALLY_00f7);
		}

IL_00cc:
		{
			Il2CppObject* L_19 = V_1;
			V_5 = (Il2CppObject*)L_19;
			SingleAssignmentDisposable_t2336378823 * L_20 = (SingleAssignmentDisposable_t2336378823 *)il2cpp_codegen_object_new(SingleAssignmentDisposable_t2336378823_il2cpp_TypeInfo_var);
			SingleAssignmentDisposable__ctor_m917449122(L_20, /*hidden argument*/NULL);
			V_6 = (SingleAssignmentDisposable_t2336378823 *)L_20;
			SerialDisposable_t2547852742 * L_21 = (SerialDisposable_t2547852742 *)__this->get_subscription_6();
			SingleAssignmentDisposable_t2336378823 * L_22 = V_6;
			NullCheck((SerialDisposable_t2547852742 *)L_21);
			SerialDisposable_set_Disposable_m1438046228((SerialDisposable_t2547852742 *)L_21, (Il2CppObject *)L_22, /*hidden argument*/NULL);
			SingleAssignmentDisposable_t2336378823 * L_23 = V_6;
			Il2CppObject* L_24 = V_5;
			NullCheck((Il2CppObject*)L_24);
			Il2CppObject * L_25 = InterfaceFuncInvoker1< Il2CppObject *, Il2CppObject* >::Invoke(0 /* System.IDisposable UniRx.IObservable`1<System.Object>::Subscribe(UniRx.IObserver`1<T>) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8), (Il2CppObject*)L_24, (Il2CppObject*)__this);
			NullCheck((SingleAssignmentDisposable_t2336378823 *)L_23);
			SingleAssignmentDisposable_set_Disposable_m1599028885((SingleAssignmentDisposable_t2336378823 *)L_23, (Il2CppObject *)L_25, /*hidden argument*/NULL);
			IL2CPP_LEAVE(0xFE, FINALLY_00f7);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_00f7;
	}

FINALLY_00f7:
	{ // begin finally (depth: 1)
		Il2CppObject * L_26 = V_0;
		Monitor_Exit_m2088237919(NULL /*static, unused*/, (Il2CppObject *)L_26, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(247)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(247)
	{
		IL2CPP_JUMP_TBL(0xFE, IL_00fe)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_00fe:
	{
		return;
	}
}
// System.Void UniRx.Operators.ConcatObservable`1/Concat<System.Object>::OnNext(T)
extern "C"  void Concat_OnNext_m1765770271_gshared (Concat_t120413325 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = (Il2CppObject*)((OperatorObserverBase_2_t1187768149 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		Il2CppObject * L_1 = ___value0;
		NullCheck((Il2CppObject*)L_0);
		InterfaceActionInvoker1< Il2CppObject * >::Invoke(2 /* System.Void UniRx.IObserver`1<System.Object>::OnNext(T) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6), (Il2CppObject*)L_0, (Il2CppObject *)L_1);
		return;
	}
}
// System.Void UniRx.Operators.ConcatObservable`1/Concat<System.Object>::OnError(System.Exception)
extern "C"  void Concat_OnError_m1368228142_gshared (Concat_t120413325 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method)
{
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		Il2CppObject* L_0 = (Il2CppObject*)((OperatorObserverBase_2_t1187768149 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		Exception_t1967233988 * L_1 = ___error0;
		NullCheck((Il2CppObject*)L_0);
		InterfaceActionInvoker1< Exception_t1967233988 * >::Invoke(1 /* System.Void UniRx.IObserver`1<System.Object>::OnError(System.Exception) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6), (Il2CppObject*)L_0, (Exception_t1967233988 *)L_1);
		IL2CPP_LEAVE(0x1A, FINALLY_0013);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0013;
	}

FINALLY_0013:
	{ // begin finally (depth: 1)
		NullCheck((OperatorObserverBase_2_t1187768149 *)__this);
		VirtActionInvoker0::Invoke(4 /* System.Void UniRx.Operators.OperatorObserverBase`2<System.Object,System.Object>::Dispose() */, (OperatorObserverBase_2_t1187768149 *)__this);
		IL2CPP_END_FINALLY(19)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(19)
	{
		IL2CPP_JUMP_TBL(0x1A, IL_001a)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_001a:
	{
		return;
	}
}
// System.Void UniRx.Operators.ConcatObservable`1/Concat<System.Object>::OnCompleted()
extern "C"  void Concat_OnCompleted_m1285687681_gshared (Concat_t120413325 * __this, const MethodInfo* method)
{
	{
		Action_t437523947 * L_0 = (Action_t437523947 *)__this->get_nextSelf_7();
		NullCheck((Action_t437523947 *)L_0);
		Action_Invoke_m1445970038((Action_t437523947 *)L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UniRx.Operators.ConcatObservable`1/Concat<System.Object>::<Run>m__78()
extern Il2CppClass* IDisposable_t1628921374_il2cpp_TypeInfo_var;
extern const uint32_t Concat_U3CRunU3Em__78_m3422706412_MetadataUsageId;
extern "C"  void Concat_U3CRunU3Em__78_m3422706412_gshared (Concat_t120413325 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Concat_U3CRunU3Em__78_m3422706412_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Il2CppObject * V_0 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_gate_3();
		V_0 = (Il2CppObject *)L_0;
		Il2CppObject * L_1 = V_0;
		Monitor_Enter_m476686225(NULL /*static, unused*/, (Il2CppObject *)L_1, /*hidden argument*/NULL);
	}

IL_000d:
	try
	{ // begin try (depth: 1)
		__this->set_isDisposed_4((bool)1);
		Il2CppObject* L_2 = (Il2CppObject*)__this->get_e_5();
		NullCheck((Il2CppObject *)L_2);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, (Il2CppObject *)L_2);
		IL2CPP_LEAVE(0x2B, FINALLY_0024);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0024;
	}

FINALLY_0024:
	{ // begin finally (depth: 1)
		Il2CppObject * L_3 = V_0;
		Monitor_Exit_m2088237919(NULL /*static, unused*/, (Il2CppObject *)L_3, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(36)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(36)
	{
		IL2CPP_JUMP_TBL(0x2B, IL_002b)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_002b:
	{
		return;
	}
}
// System.Void UniRx.Operators.ConcatObservable`1<System.Object>::.ctor(System.Collections.Generic.IEnumerable`1<UniRx.IObservable`1<T>>)
extern "C"  void ConcatObservable_1__ctor_m3950127814_gshared (ConcatObservable_1_t1571046694 * __this, Il2CppObject* ___sources0, const MethodInfo* method)
{
	{
		NullCheck((OperatorObservableBase_1_t4196218687 *)__this);
		((  void (*) (OperatorObservableBase_1_t4196218687 *, bool, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((OperatorObservableBase_1_t4196218687 *)__this, (bool)1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		Il2CppObject* L_0 = ___sources0;
		__this->set_sources_1(L_0);
		return;
	}
}
// UniRx.IObservable`1<T> UniRx.Operators.ConcatObservable`1<System.Object>::Combine(System.Collections.Generic.IEnumerable`1<UniRx.IObservable`1<T>>)
extern "C"  Il2CppObject* ConcatObservable_1_Combine_m1191673255_gshared (ConcatObservable_1_t1571046694 * __this, Il2CppObject* ___combineSources0, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = (Il2CppObject*)__this->get_sources_1();
		Il2CppObject* L_1 = ___combineSources0;
		Il2CppObject* L_2 = ((  Il2CppObject* (*) (Il2CppObject * /* static, unused */, Il2CppObject*, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)(NULL /*static, unused*/, (Il2CppObject*)L_0, (Il2CppObject*)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		ConcatObservable_1_t1571046694 * L_3 = (ConcatObservable_1_t1571046694 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		((  void (*) (ConcatObservable_1_t1571046694 *, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->method)(L_3, (Il2CppObject*)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		return L_3;
	}
}
// System.Collections.Generic.IEnumerable`1<UniRx.IObservable`1<T>> UniRx.Operators.ConcatObservable`1<System.Object>::CombineSources(System.Collections.Generic.IEnumerable`1<UniRx.IObservable`1<T>>,System.Collections.Generic.IEnumerable`1<UniRx.IObservable`1<T>>)
extern "C"  Il2CppObject* ConcatObservable_1_CombineSources_m1435020884_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* ___first0, Il2CppObject* ___second1, const MethodInfo* method)
{
	U3CCombineSourcesU3Ec__Iterator18_t1596040496 * V_0 = NULL;
	{
		U3CCombineSourcesU3Ec__Iterator18_t1596040496 * L_0 = (U3CCombineSourcesU3Ec__Iterator18_t1596040496 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		((  void (*) (U3CCombineSourcesU3Ec__Iterator18_t1596040496 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7)->method)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7));
		V_0 = (U3CCombineSourcesU3Ec__Iterator18_t1596040496 *)L_0;
		U3CCombineSourcesU3Ec__Iterator18_t1596040496 * L_1 = V_0;
		Il2CppObject* L_2 = ___first0;
		NullCheck(L_1);
		L_1->set_first_0(L_2);
		U3CCombineSourcesU3Ec__Iterator18_t1596040496 * L_3 = V_0;
		Il2CppObject* L_4 = ___second1;
		NullCheck(L_3);
		L_3->set_second_3(L_4);
		U3CCombineSourcesU3Ec__Iterator18_t1596040496 * L_5 = V_0;
		Il2CppObject* L_6 = ___first0;
		NullCheck(L_5);
		L_5->set_U3CU24U3Efirst_8(L_6);
		U3CCombineSourcesU3Ec__Iterator18_t1596040496 * L_7 = V_0;
		Il2CppObject* L_8 = ___second1;
		NullCheck(L_7);
		L_7->set_U3CU24U3Esecond_9(L_8);
		U3CCombineSourcesU3Ec__Iterator18_t1596040496 * L_9 = V_0;
		U3CCombineSourcesU3Ec__Iterator18_t1596040496 * L_10 = (U3CCombineSourcesU3Ec__Iterator18_t1596040496 *)L_9;
		NullCheck(L_10);
		L_10->set_U24PC_6(((int32_t)-2));
		return L_10;
	}
}
// System.IDisposable UniRx.Operators.ConcatObservable`1<System.Object>::SubscribeCore(UniRx.IObserver`1<T>,System.IDisposable)
extern "C"  Il2CppObject * ConcatObservable_1_SubscribeCore_m2004381934_gshared (ConcatObservable_1_t1571046694 * __this, Il2CppObject* ___observer0, Il2CppObject * ___cancel1, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = ___observer0;
		Il2CppObject * L_1 = ___cancel1;
		Concat_t120413325 * L_2 = (Concat_t120413325 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8));
		((  void (*) (Concat_t120413325 *, ConcatObservable_1_t1571046694 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9)->method)(L_2, (ConcatObservable_1_t1571046694 *)__this, (Il2CppObject*)L_0, (Il2CppObject *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9));
		NullCheck((Concat_t120413325 *)L_2);
		Il2CppObject * L_3 = ((  Il2CppObject * (*) (Concat_t120413325 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10)->method)((Concat_t120413325 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10));
		return L_3;
	}
}
// System.Void UniRx.Operators.ContinueWithObservable`2/ContinueWith<System.Object,System.Object>::.ctor(UniRx.Operators.ContinueWithObservable`2<TSource,TResult>,UniRx.IObserver`1<TResult>,System.IDisposable)
extern Il2CppClass* SerialDisposable_t2547852742_il2cpp_TypeInfo_var;
extern const uint32_t ContinueWith__ctor_m4278276356_MetadataUsageId;
extern "C"  void ContinueWith__ctor_m4278276356_gshared (ContinueWith_t1869588638 * __this, ContinueWithObservable_2_t4007622692 * ___parent0, Il2CppObject* ___observer1, Il2CppObject * ___cancel2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ContinueWith__ctor_m4278276356_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		SerialDisposable_t2547852742 * L_0 = (SerialDisposable_t2547852742 *)il2cpp_codegen_object_new(SerialDisposable_t2547852742_il2cpp_TypeInfo_var);
		SerialDisposable__ctor_m2977852291(L_0, /*hidden argument*/NULL);
		__this->set_serialDisposable_3(L_0);
		Il2CppObject* L_1 = ___observer1;
		Il2CppObject * L_2 = ___cancel2;
		NullCheck((OperatorObserverBase_2_t1187768149 *)__this);
		((  void (*) (OperatorObserverBase_2_t1187768149 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((OperatorObserverBase_2_t1187768149 *)__this, (Il2CppObject*)L_1, (Il2CppObject *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		ContinueWithObservable_2_t4007622692 * L_3 = ___parent0;
		__this->set_parent_2(L_3);
		return;
	}
}
// System.IDisposable UniRx.Operators.ContinueWithObservable`2/ContinueWith<System.Object,System.Object>::Run()
extern Il2CppClass* SingleAssignmentDisposable_t2336378823_il2cpp_TypeInfo_var;
extern const uint32_t ContinueWith_Run_m3777762166_MetadataUsageId;
extern "C"  Il2CppObject * ContinueWith_Run_m3777762166_gshared (ContinueWith_t1869588638 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ContinueWith_Run_m3777762166_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	SingleAssignmentDisposable_t2336378823 * V_0 = NULL;
	{
		SingleAssignmentDisposable_t2336378823 * L_0 = (SingleAssignmentDisposable_t2336378823 *)il2cpp_codegen_object_new(SingleAssignmentDisposable_t2336378823_il2cpp_TypeInfo_var);
		SingleAssignmentDisposable__ctor_m917449122(L_0, /*hidden argument*/NULL);
		V_0 = (SingleAssignmentDisposable_t2336378823 *)L_0;
		SerialDisposable_t2547852742 * L_1 = (SerialDisposable_t2547852742 *)__this->get_serialDisposable_3();
		SingleAssignmentDisposable_t2336378823 * L_2 = V_0;
		NullCheck((SerialDisposable_t2547852742 *)L_1);
		SerialDisposable_set_Disposable_m1438046228((SerialDisposable_t2547852742 *)L_1, (Il2CppObject *)L_2, /*hidden argument*/NULL);
		SingleAssignmentDisposable_t2336378823 * L_3 = V_0;
		ContinueWithObservable_2_t4007622692 * L_4 = (ContinueWithObservable_2_t4007622692 *)__this->get_parent_2();
		NullCheck(L_4);
		Il2CppObject* L_5 = (Il2CppObject*)L_4->get_source_1();
		NullCheck((Il2CppObject*)L_5);
		Il2CppObject * L_6 = InterfaceFuncInvoker1< Il2CppObject *, Il2CppObject* >::Invoke(0 /* System.IDisposable UniRx.IObservable`1<System.Object>::Subscribe(UniRx.IObserver`1<T>) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), (Il2CppObject*)L_5, (Il2CppObject*)__this);
		NullCheck((SingleAssignmentDisposable_t2336378823 *)L_3);
		SingleAssignmentDisposable_set_Disposable_m1599028885((SingleAssignmentDisposable_t2336378823 *)L_3, (Il2CppObject *)L_6, /*hidden argument*/NULL);
		SerialDisposable_t2547852742 * L_7 = (SerialDisposable_t2547852742 *)__this->get_serialDisposable_3();
		return L_7;
	}
}
// System.Void UniRx.Operators.ContinueWithObservable`2/ContinueWith<System.Object,System.Object>::OnNext(TSource)
extern "C"  void ContinueWith_OnNext_m2111425205_gshared (ContinueWith_t1869588638 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	{
		__this->set_seenValue_4((bool)1);
		Il2CppObject * L_0 = ___value0;
		__this->set_lastValue_5(L_0);
		return;
	}
}
// System.Void UniRx.Operators.ContinueWithObservable`2/ContinueWith<System.Object,System.Object>::OnError(System.Exception)
extern "C"  void ContinueWith_OnError_m2584767455_gshared (ContinueWith_t1869588638 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method)
{
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		Il2CppObject* L_0 = (Il2CppObject*)((OperatorObserverBase_2_t1187768149 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		Exception_t1967233988 * L_1 = ___error0;
		NullCheck((Il2CppObject*)L_0);
		InterfaceActionInvoker1< Exception_t1967233988 * >::Invoke(1 /* System.Void UniRx.IObserver`1<System.Object>::OnError(System.Exception) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3), (Il2CppObject*)L_0, (Exception_t1967233988 *)L_1);
		IL2CPP_LEAVE(0x1A, FINALLY_0013);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0013;
	}

FINALLY_0013:
	{ // begin finally (depth: 1)
		NullCheck((OperatorObserverBase_2_t1187768149 *)__this);
		VirtActionInvoker0::Invoke(4 /* System.Void UniRx.Operators.OperatorObserverBase`2<System.Object,System.Object>::Dispose() */, (OperatorObserverBase_2_t1187768149 *)__this);
		IL2CPP_END_FINALLY(19)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(19)
	{
		IL2CPP_JUMP_TBL(0x1A, IL_001a)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_001a:
	{
		return;
	}
}
// System.Void UniRx.Operators.ContinueWithObservable`2/ContinueWith<System.Object,System.Object>::OnCompleted()
extern "C"  void ContinueWith_OnCompleted_m4256347570_gshared (ContinueWith_t1869588638 * __this, const MethodInfo* method)
{
	Il2CppObject* V_0 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		bool L_0 = (bool)__this->get_seenValue_4();
		if (!L_0)
		{
			goto IL_0040;
		}
	}
	{
		ContinueWithObservable_2_t4007622692 * L_1 = (ContinueWithObservable_2_t4007622692 *)__this->get_parent_2();
		NullCheck(L_1);
		Func_2_t1894581716 * L_2 = (Func_2_t1894581716 *)L_1->get_selector_2();
		Il2CppObject * L_3 = (Il2CppObject *)__this->get_lastValue_5();
		NullCheck((Func_2_t1894581716 *)L_2);
		Il2CppObject* L_4 = ((  Il2CppObject* (*) (Func_2_t1894581716 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->method)((Func_2_t1894581716 *)L_2, (Il2CppObject *)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		V_0 = (Il2CppObject*)L_4;
		SerialDisposable_t2547852742 * L_5 = (SerialDisposable_t2547852742 *)__this->get_serialDisposable_3();
		Il2CppObject* L_6 = V_0;
		Il2CppObject* L_7 = (Il2CppObject*)((OperatorObserverBase_2_t1187768149 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		NullCheck((Il2CppObject*)L_6);
		Il2CppObject * L_8 = InterfaceFuncInvoker1< Il2CppObject *, Il2CppObject* >::Invoke(0 /* System.IDisposable UniRx.IObservable`1<System.Object>::Subscribe(UniRx.IObserver`1<T>) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6), (Il2CppObject*)L_6, (Il2CppObject*)L_7);
		NullCheck((SerialDisposable_t2547852742 *)L_5);
		SerialDisposable_set_Disposable_m1438046228((SerialDisposable_t2547852742 *)L_5, (Il2CppObject *)L_8, /*hidden argument*/NULL);
		goto IL_0059;
	}

IL_0040:
	try
	{ // begin try (depth: 1)
		Il2CppObject* L_9 = (Il2CppObject*)((OperatorObserverBase_2_t1187768149 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		NullCheck((Il2CppObject*)L_9);
		InterfaceActionInvoker0::Invoke(0 /* System.Void UniRx.IObserver`1<System.Object>::OnCompleted() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3), (Il2CppObject*)L_9);
		IL2CPP_LEAVE(0x59, FINALLY_0052);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0052;
	}

FINALLY_0052:
	{ // begin finally (depth: 1)
		NullCheck((OperatorObserverBase_2_t1187768149 *)__this);
		VirtActionInvoker0::Invoke(4 /* System.Void UniRx.Operators.OperatorObserverBase`2<System.Object,System.Object>::Dispose() */, (OperatorObserverBase_2_t1187768149 *)__this);
		IL2CPP_END_FINALLY(82)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(82)
	{
		IL2CPP_JUMP_TBL(0x59, IL_0059)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0059:
	{
		return;
	}
}
// System.Void UniRx.Operators.ContinueWithObservable`2<System.Object,System.Object>::.ctor(UniRx.IObservable`1<TSource>,System.Func`2<TSource,UniRx.IObservable`1<TResult>>)
extern "C"  void ContinueWithObservable_2__ctor_m3943250262_gshared (ContinueWithObservable_2_t4007622692 * __this, Il2CppObject* ___source0, Func_2_t1894581716 * ___selector1, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = ___source0;
		bool L_1 = ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)(NULL /*static, unused*/, (Il2CppObject*)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		NullCheck((OperatorObservableBase_1_t4196218687 *)__this);
		((  void (*) (OperatorObservableBase_1_t4196218687 *, bool, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)((OperatorObservableBase_1_t4196218687 *)__this, (bool)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		Il2CppObject* L_2 = ___source0;
		__this->set_source_1(L_2);
		Func_2_t1894581716 * L_3 = ___selector1;
		__this->set_selector_2(L_3);
		return;
	}
}
// System.IDisposable UniRx.Operators.ContinueWithObservable`2<System.Object,System.Object>::SubscribeCore(UniRx.IObserver`1<TResult>,System.IDisposable)
extern "C"  Il2CppObject * ContinueWithObservable_2_SubscribeCore_m2919942245_gshared (ContinueWithObservable_2_t4007622692 * __this, Il2CppObject* ___observer0, Il2CppObject * ___cancel1, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = ___observer0;
		Il2CppObject * L_1 = ___cancel1;
		ContinueWith_t1869588638 * L_2 = (ContinueWith_t1869588638 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		((  void (*) (ContinueWith_t1869588638 *, ContinueWithObservable_2_t4007622692 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)(L_2, (ContinueWithObservable_2_t4007622692 *)__this, (Il2CppObject*)L_0, (Il2CppObject *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		NullCheck((ContinueWith_t1869588638 *)L_2);
		Il2CppObject * L_3 = ((  Il2CppObject * (*) (ContinueWith_t1869588638 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->method)((ContinueWith_t1869588638 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		return L_3;
	}
}
// System.Void UniRx.Operators.CreateObservable`1/Create<System.Boolean>::.ctor(UniRx.IObserver`1<T>,System.IDisposable)
extern "C"  void Create__ctor_m2173222544_gshared (Create_t16300766 * __this, Il2CppObject* ___observer0, Il2CppObject * ___cancel1, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = ___observer0;
		Il2CppObject * L_1 = ___cancel1;
		NullCheck((OperatorObserverBase_2_t60103313 *)__this);
		((  void (*) (OperatorObserverBase_2_t60103313 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((OperatorObserverBase_2_t60103313 *)__this, (Il2CppObject*)L_0, (Il2CppObject *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		return;
	}
}
// System.Void UniRx.Operators.CreateObservable`1/Create<System.Boolean>::OnNext(T)
extern "C"  void Create_OnNext_m1872647938_gshared (Create_t16300766 * __this, bool ___value0, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = (Il2CppObject*)((OperatorObserverBase_2_t60103313 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		bool L_1 = ___value0;
		NullCheck((Il2CppObject*)L_0);
		InterfaceActionInvoker1< bool >::Invoke(2 /* System.Void UniRx.IObserver`1<System.Boolean>::OnNext(T) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), (Il2CppObject*)L_0, (bool)L_1);
		return;
	}
}
// System.Void UniRx.Operators.CreateObservable`1/Create<System.Boolean>::OnError(System.Exception)
extern "C"  void Create_OnError_m3131584017_gshared (Create_t16300766 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method)
{
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		Il2CppObject* L_0 = (Il2CppObject*)((OperatorObserverBase_2_t60103313 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		Exception_t1967233988 * L_1 = ___error0;
		NullCheck((Il2CppObject*)L_0);
		InterfaceActionInvoker1< Exception_t1967233988 * >::Invoke(1 /* System.Void UniRx.IObserver`1<System.Boolean>::OnError(System.Exception) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), (Il2CppObject*)L_0, (Exception_t1967233988 *)L_1);
		IL2CPP_LEAVE(0x1A, FINALLY_0013);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0013;
	}

FINALLY_0013:
	{ // begin finally (depth: 1)
		NullCheck((OperatorObserverBase_2_t60103313 *)__this);
		VirtActionInvoker0::Invoke(4 /* System.Void UniRx.Operators.OperatorObserverBase`2<System.Boolean,System.Boolean>::Dispose() */, (OperatorObserverBase_2_t60103313 *)__this);
		IL2CPP_END_FINALLY(19)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(19)
	{
		IL2CPP_JUMP_TBL(0x1A, IL_001a)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_001a:
	{
		return;
	}
}
// System.Void UniRx.Operators.CreateObservable`1/Create<System.Boolean>::OnCompleted()
extern "C"  void Create_OnCompleted_m2412163812_gshared (Create_t16300766 * __this, const MethodInfo* method)
{
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		Il2CppObject* L_0 = (Il2CppObject*)((OperatorObserverBase_2_t60103313 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		NullCheck((Il2CppObject*)L_0);
		InterfaceActionInvoker0::Invoke(0 /* System.Void UniRx.IObserver`1<System.Boolean>::OnCompleted() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), (Il2CppObject*)L_0);
		IL2CPP_LEAVE(0x19, FINALLY_0012);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0012;
	}

FINALLY_0012:
	{ // begin finally (depth: 1)
		NullCheck((OperatorObserverBase_2_t60103313 *)__this);
		VirtActionInvoker0::Invoke(4 /* System.Void UniRx.Operators.OperatorObserverBase`2<System.Boolean,System.Boolean>::Dispose() */, (OperatorObserverBase_2_t60103313 *)__this);
		IL2CPP_END_FINALLY(18)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(18)
	{
		IL2CPP_JUMP_TBL(0x19, IL_0019)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0019:
	{
		return;
	}
}
// System.Void UniRx.Operators.CreateObservable`1/Create<System.Object>::.ctor(UniRx.IObserver`1<T>,System.IDisposable)
extern "C"  void Create__ctor_m3386042515_gshared (Create_t642401845 * __this, Il2CppObject* ___observer0, Il2CppObject * ___cancel1, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = ___observer0;
		Il2CppObject * L_1 = ___cancel1;
		NullCheck((OperatorObserverBase_2_t1187768149 *)__this);
		((  void (*) (OperatorObserverBase_2_t1187768149 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((OperatorObserverBase_2_t1187768149 *)__this, (Il2CppObject*)L_0, (Il2CppObject *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		return;
	}
}
// System.Void UniRx.Operators.CreateObservable`1/Create<System.Object>::OnNext(T)
extern "C"  void Create_OnNext_m313289503_gshared (Create_t642401845 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = (Il2CppObject*)((OperatorObserverBase_2_t1187768149 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		Il2CppObject * L_1 = ___value0;
		NullCheck((Il2CppObject*)L_0);
		InterfaceActionInvoker1< Il2CppObject * >::Invoke(2 /* System.Void UniRx.IObserver`1<System.Object>::OnNext(T) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), (Il2CppObject*)L_0, (Il2CppObject *)L_1);
		return;
	}
}
// System.Void UniRx.Operators.CreateObservable`1/Create<System.Object>::OnError(System.Exception)
extern "C"  void Create_OnError_m3930875950_gshared (Create_t642401845 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method)
{
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		Il2CppObject* L_0 = (Il2CppObject*)((OperatorObserverBase_2_t1187768149 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		Exception_t1967233988 * L_1 = ___error0;
		NullCheck((Il2CppObject*)L_0);
		InterfaceActionInvoker1< Exception_t1967233988 * >::Invoke(1 /* System.Void UniRx.IObserver`1<System.Object>::OnError(System.Exception) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), (Il2CppObject*)L_0, (Exception_t1967233988 *)L_1);
		IL2CPP_LEAVE(0x1A, FINALLY_0013);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0013;
	}

FINALLY_0013:
	{ // begin finally (depth: 1)
		NullCheck((OperatorObserverBase_2_t1187768149 *)__this);
		VirtActionInvoker0::Invoke(4 /* System.Void UniRx.Operators.OperatorObserverBase`2<System.Object,System.Object>::Dispose() */, (OperatorObserverBase_2_t1187768149 *)__this);
		IL2CPP_END_FINALLY(19)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(19)
	{
		IL2CPP_JUMP_TBL(0x1A, IL_001a)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_001a:
	{
		return;
	}
}
// System.Void UniRx.Operators.CreateObservable`1/Create<System.Object>::OnCompleted()
extern "C"  void Create_OnCompleted_m390295681_gshared (Create_t642401845 * __this, const MethodInfo* method)
{
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		Il2CppObject* L_0 = (Il2CppObject*)((OperatorObserverBase_2_t1187768149 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		NullCheck((Il2CppObject*)L_0);
		InterfaceActionInvoker0::Invoke(0 /* System.Void UniRx.IObserver`1<System.Object>::OnCompleted() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), (Il2CppObject*)L_0);
		IL2CPP_LEAVE(0x19, FINALLY_0012);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0012;
	}

FINALLY_0012:
	{ // begin finally (depth: 1)
		NullCheck((OperatorObserverBase_2_t1187768149 *)__this);
		VirtActionInvoker0::Invoke(4 /* System.Void UniRx.Operators.OperatorObserverBase`2<System.Object,System.Object>::Dispose() */, (OperatorObserverBase_2_t1187768149 *)__this);
		IL2CPP_END_FINALLY(18)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(18)
	{
		IL2CPP_JUMP_TBL(0x19, IL_0019)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0019:
	{
		return;
	}
}
// System.Void UniRx.Operators.CreateObservable`1/Create<System.Single>::.ctor(UniRx.IObserver`1<T>,System.IDisposable)
extern "C"  void Create__ctor_m3983802026_gshared (Create_t763504446 * __this, Il2CppObject* ___observer0, Il2CppObject * ___cancel1, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = ___observer0;
		Il2CppObject * L_1 = ___cancel1;
		NullCheck((OperatorObserverBase_2_t2516778257 *)__this);
		((  void (*) (OperatorObserverBase_2_t2516778257 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((OperatorObserverBase_2_t2516778257 *)__this, (Il2CppObject*)L_0, (Il2CppObject *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		return;
	}
}
// System.Void UniRx.Operators.CreateObservable`1/Create<System.Single>::OnNext(T)
extern "C"  void Create_OnNext_m2327185064_gshared (Create_t763504446 * __this, float ___value0, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = (Il2CppObject*)((OperatorObserverBase_2_t2516778257 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		float L_1 = ___value0;
		NullCheck((Il2CppObject*)L_0);
		InterfaceActionInvoker1< float >::Invoke(2 /* System.Void UniRx.IObserver`1<System.Single>::OnNext(T) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), (Il2CppObject*)L_0, (float)L_1);
		return;
	}
}
// System.Void UniRx.Operators.CreateObservable`1/Create<System.Single>::OnError(System.Exception)
extern "C"  void Create_OnError_m883413431_gshared (Create_t763504446 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method)
{
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		Il2CppObject* L_0 = (Il2CppObject*)((OperatorObserverBase_2_t2516778257 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		Exception_t1967233988 * L_1 = ___error0;
		NullCheck((Il2CppObject*)L_0);
		InterfaceActionInvoker1< Exception_t1967233988 * >::Invoke(1 /* System.Void UniRx.IObserver`1<System.Single>::OnError(System.Exception) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), (Il2CppObject*)L_0, (Exception_t1967233988 *)L_1);
		IL2CPP_LEAVE(0x1A, FINALLY_0013);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0013;
	}

FINALLY_0013:
	{ // begin finally (depth: 1)
		NullCheck((OperatorObserverBase_2_t2516778257 *)__this);
		VirtActionInvoker0::Invoke(4 /* System.Void UniRx.Operators.OperatorObserverBase`2<System.Single,System.Single>::Dispose() */, (OperatorObserverBase_2_t2516778257 *)__this);
		IL2CPP_END_FINALLY(19)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(19)
	{
		IL2CPP_JUMP_TBL(0x1A, IL_001a)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_001a:
	{
		return;
	}
}
// System.Void UniRx.Operators.CreateObservable`1/Create<System.Single>::OnCompleted()
extern "C"  void Create_OnCompleted_m4069662602_gshared (Create_t763504446 * __this, const MethodInfo* method)
{
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		Il2CppObject* L_0 = (Il2CppObject*)((OperatorObserverBase_2_t2516778257 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		NullCheck((Il2CppObject*)L_0);
		InterfaceActionInvoker0::Invoke(0 /* System.Void UniRx.IObserver`1<System.Single>::OnCompleted() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), (Il2CppObject*)L_0);
		IL2CPP_LEAVE(0x19, FINALLY_0012);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0012;
	}

FINALLY_0012:
	{ // begin finally (depth: 1)
		NullCheck((OperatorObserverBase_2_t2516778257 *)__this);
		VirtActionInvoker0::Invoke(4 /* System.Void UniRx.Operators.OperatorObserverBase`2<System.Single,System.Single>::Dispose() */, (OperatorObserverBase_2_t2516778257 *)__this);
		IL2CPP_END_FINALLY(18)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(18)
	{
		IL2CPP_JUMP_TBL(0x19, IL_0019)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0019:
	{
		return;
	}
}
// System.Void UniRx.Operators.CreateObservable`1/Create<UniRx.Unit>::.ctor(UniRx.IObserver`1<T>,System.IDisposable)
extern "C"  void Create__ctor_m3239614205_gshared (Create_t2363581463 * __this, Il2CppObject* ___observer0, Il2CppObject * ___cancel1, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = ___observer0;
		Il2CppObject * L_1 = ___cancel1;
		NullCheck((OperatorObserverBase_2_t4165376397 *)__this);
		((  void (*) (OperatorObserverBase_2_t4165376397 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((OperatorObserverBase_2_t4165376397 *)__this, (Il2CppObject*)L_0, (Il2CppObject *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		return;
	}
}
// System.Void UniRx.Operators.CreateObservable`1/Create<UniRx.Unit>::OnNext(T)
extern "C"  void Create_OnNext_m3447503477_gshared (Create_t2363581463 * __this, Unit_t2558286038  ___value0, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = (Il2CppObject*)((OperatorObserverBase_2_t4165376397 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		Unit_t2558286038  L_1 = ___value0;
		NullCheck((Il2CppObject*)L_0);
		InterfaceActionInvoker1< Unit_t2558286038  >::Invoke(2 /* System.Void UniRx.IObserver`1<UniRx.Unit>::OnNext(T) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), (Il2CppObject*)L_0, (Unit_t2558286038 )L_1);
		return;
	}
}
// System.Void UniRx.Operators.CreateObservable`1/Create<UniRx.Unit>::OnError(System.Exception)
extern "C"  void Create_OnError_m2309966724_gshared (Create_t2363581463 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method)
{
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		Il2CppObject* L_0 = (Il2CppObject*)((OperatorObserverBase_2_t4165376397 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		Exception_t1967233988 * L_1 = ___error0;
		NullCheck((Il2CppObject*)L_0);
		InterfaceActionInvoker1< Exception_t1967233988 * >::Invoke(1 /* System.Void UniRx.IObserver`1<UniRx.Unit>::OnError(System.Exception) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), (Il2CppObject*)L_0, (Exception_t1967233988 *)L_1);
		IL2CPP_LEAVE(0x1A, FINALLY_0013);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0013;
	}

FINALLY_0013:
	{ // begin finally (depth: 1)
		NullCheck((OperatorObserverBase_2_t4165376397 *)__this);
		VirtActionInvoker0::Invoke(4 /* System.Void UniRx.Operators.OperatorObserverBase`2<UniRx.Unit,UniRx.Unit>::Dispose() */, (OperatorObserverBase_2_t4165376397 *)__this);
		IL2CPP_END_FINALLY(19)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(19)
	{
		IL2CPP_JUMP_TBL(0x1A, IL_001a)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_001a:
	{
		return;
	}
}
// System.Void UniRx.Operators.CreateObservable`1/Create<UniRx.Unit>::OnCompleted()
extern "C"  void Create_OnCompleted_m1209017559_gshared (Create_t2363581463 * __this, const MethodInfo* method)
{
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		Il2CppObject* L_0 = (Il2CppObject*)((OperatorObserverBase_2_t4165376397 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		NullCheck((Il2CppObject*)L_0);
		InterfaceActionInvoker0::Invoke(0 /* System.Void UniRx.IObserver`1<UniRx.Unit>::OnCompleted() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), (Il2CppObject*)L_0);
		IL2CPP_LEAVE(0x19, FINALLY_0012);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0012;
	}

FINALLY_0012:
	{ // begin finally (depth: 1)
		NullCheck((OperatorObserverBase_2_t4165376397 *)__this);
		VirtActionInvoker0::Invoke(4 /* System.Void UniRx.Operators.OperatorObserverBase`2<UniRx.Unit,UniRx.Unit>::Dispose() */, (OperatorObserverBase_2_t4165376397 *)__this);
		IL2CPP_END_FINALLY(18)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(18)
	{
		IL2CPP_JUMP_TBL(0x19, IL_0019)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0019:
	{
		return;
	}
}
// System.Void UniRx.Operators.CreateObservable`1/Create<UnityEngine.Vector2>::.ctor(UniRx.IObserver`1<T>,System.IDisposable)
extern "C"  void Create__ctor_m2830912633_gshared (Create_t3330625213 * __this, Il2CppObject* ___observer0, Il2CppObject * ___cancel1, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = ___observer0;
		Il2CppObject * L_1 = ___cancel1;
		NullCheck((OperatorObserverBase_2_t2310884405 *)__this);
		((  void (*) (OperatorObserverBase_2_t2310884405 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((OperatorObserverBase_2_t2310884405 *)__this, (Il2CppObject*)L_0, (Il2CppObject *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		return;
	}
}
// System.Void UniRx.Operators.CreateObservable`1/Create<UnityEngine.Vector2>::OnNext(T)
extern "C"  void Create_OnNext_m98146169_gshared (Create_t3330625213 * __this, Vector2_t3525329788  ___value0, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = (Il2CppObject*)((OperatorObserverBase_2_t2310884405 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		Vector2_t3525329788  L_1 = ___value0;
		NullCheck((Il2CppObject*)L_0);
		InterfaceActionInvoker1< Vector2_t3525329788  >::Invoke(2 /* System.Void UniRx.IObserver`1<UnityEngine.Vector2>::OnNext(T) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), (Il2CppObject*)L_0, (Vector2_t3525329788 )L_1);
		return;
	}
}
// System.Void UniRx.Operators.CreateObservable`1/Create<UnityEngine.Vector2>::OnError(System.Exception)
extern "C"  void Create_OnError_m1901699208_gshared (Create_t3330625213 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method)
{
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		Il2CppObject* L_0 = (Il2CppObject*)((OperatorObserverBase_2_t2310884405 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		Exception_t1967233988 * L_1 = ___error0;
		NullCheck((Il2CppObject*)L_0);
		InterfaceActionInvoker1< Exception_t1967233988 * >::Invoke(1 /* System.Void UniRx.IObserver`1<UnityEngine.Vector2>::OnError(System.Exception) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), (Il2CppObject*)L_0, (Exception_t1967233988 *)L_1);
		IL2CPP_LEAVE(0x1A, FINALLY_0013);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0013;
	}

FINALLY_0013:
	{ // begin finally (depth: 1)
		NullCheck((OperatorObserverBase_2_t2310884405 *)__this);
		VirtActionInvoker0::Invoke(4 /* System.Void UniRx.Operators.OperatorObserverBase`2<UnityEngine.Vector2,UnityEngine.Vector2>::Dispose() */, (OperatorObserverBase_2_t2310884405 *)__this);
		IL2CPP_END_FINALLY(19)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(19)
	{
		IL2CPP_JUMP_TBL(0x1A, IL_001a)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_001a:
	{
		return;
	}
}
// System.Void UniRx.Operators.CreateObservable`1/Create<UnityEngine.Vector2>::OnCompleted()
extern "C"  void Create_OnCompleted_m485416923_gshared (Create_t3330625213 * __this, const MethodInfo* method)
{
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		Il2CppObject* L_0 = (Il2CppObject*)((OperatorObserverBase_2_t2310884405 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		NullCheck((Il2CppObject*)L_0);
		InterfaceActionInvoker0::Invoke(0 /* System.Void UniRx.IObserver`1<UnityEngine.Vector2>::OnCompleted() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), (Il2CppObject*)L_0);
		IL2CPP_LEAVE(0x19, FINALLY_0012);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0012;
	}

FINALLY_0012:
	{ // begin finally (depth: 1)
		NullCheck((OperatorObserverBase_2_t2310884405 *)__this);
		VirtActionInvoker0::Invoke(4 /* System.Void UniRx.Operators.OperatorObserverBase`2<UnityEngine.Vector2,UnityEngine.Vector2>::Dispose() */, (OperatorObserverBase_2_t2310884405 *)__this);
		IL2CPP_END_FINALLY(18)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(18)
	{
		IL2CPP_JUMP_TBL(0x19, IL_0019)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0019:
	{
		return;
	}
}
// System.Void UniRx.Operators.CreateObservable`1<System.Boolean>::.ctor(System.Func`2<UniRx.IObserver`1<T>,System.IDisposable>)
extern "C"  void CreateObservable_1__ctor_m3037764018_gshared (CreateObservable_1_t1414948727 * __this, Func_2_t2118199298 * ___subscribe0, const MethodInfo* method)
{
	{
		NullCheck((OperatorObservableBase_1_t3570117608 *)__this);
		((  void (*) (OperatorObservableBase_1_t3570117608 *, bool, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((OperatorObservableBase_1_t3570117608 *)__this, (bool)1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		Func_2_t2118199298 * L_0 = ___subscribe0;
		__this->set_subscribe_1(L_0);
		return;
	}
}
// System.Void UniRx.Operators.CreateObservable`1<System.Boolean>::.ctor(System.Func`2<UniRx.IObserver`1<T>,System.IDisposable>,System.Boolean)
extern "C"  void CreateObservable_1__ctor_m2570510219_gshared (CreateObservable_1_t1414948727 * __this, Func_2_t2118199298 * ___subscribe0, bool ___isRequiredSubscribeOnCurrentThread1, const MethodInfo* method)
{
	{
		bool L_0 = ___isRequiredSubscribeOnCurrentThread1;
		NullCheck((OperatorObservableBase_1_t3570117608 *)__this);
		((  void (*) (OperatorObservableBase_1_t3570117608 *, bool, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((OperatorObservableBase_1_t3570117608 *)__this, (bool)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		Func_2_t2118199298 * L_1 = ___subscribe0;
		__this->set_subscribe_1(L_1);
		return;
	}
}
// System.IDisposable UniRx.Operators.CreateObservable`1<System.Boolean>::SubscribeCore(UniRx.IObserver`1<T>,System.IDisposable)
extern Il2CppClass* Disposable_t3736388786_il2cpp_TypeInfo_var;
extern const uint32_t CreateObservable_1_SubscribeCore_m1273965007_MetadataUsageId;
extern "C"  Il2CppObject * CreateObservable_1_SubscribeCore_m1273965007_gshared (CreateObservable_1_t1414948727 * __this, Il2CppObject* ___observer0, Il2CppObject * ___cancel1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (CreateObservable_1_SubscribeCore_m1273965007_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Il2CppObject * G_B2_0 = NULL;
	Il2CppObject * G_B1_0 = NULL;
	{
		Il2CppObject* L_0 = ___observer0;
		Il2CppObject * L_1 = ___cancel1;
		Create_t16300766 * L_2 = (Create_t16300766 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		((  void (*) (Create_t16300766 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)(L_2, (Il2CppObject*)L_0, (Il2CppObject *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		___observer0 = (Il2CppObject*)L_2;
		Func_2_t2118199298 * L_3 = (Func_2_t2118199298 *)__this->get_subscribe_1();
		Il2CppObject* L_4 = ___observer0;
		NullCheck((Func_2_t2118199298 *)L_3);
		Il2CppObject * L_5 = ((  Il2CppObject * (*) (Func_2_t2118199298 *, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)((Func_2_t2118199298 *)L_3, (Il2CppObject*)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		Il2CppObject * L_6 = (Il2CppObject *)L_5;
		G_B1_0 = L_6;
		if (L_6)
		{
			G_B2_0 = L_6;
			goto IL_0021;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Disposable_t3736388786_il2cpp_TypeInfo_var);
		Il2CppObject * L_7 = ((Disposable_t3736388786_StaticFields*)Disposable_t3736388786_il2cpp_TypeInfo_var->static_fields)->get_Empty_0();
		G_B2_0 = L_7;
	}

IL_0021:
	{
		return G_B2_0;
	}
}
// System.Void UniRx.Operators.CreateObservable`1<System.Object>::.ctor(System.Func`2<UniRx.IObserver`1<T>,System.IDisposable>)
extern "C"  void CreateObservable_1__ctor_m3114897029_gshared (CreateObservable_1_t2041049806 * __this, Func_2_t2619763055 * ___subscribe0, const MethodInfo* method)
{
	{
		NullCheck((OperatorObservableBase_1_t4196218687 *)__this);
		((  void (*) (OperatorObservableBase_1_t4196218687 *, bool, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((OperatorObservableBase_1_t4196218687 *)__this, (bool)1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		Func_2_t2619763055 * L_0 = ___subscribe0;
		__this->set_subscribe_1(L_0);
		return;
	}
}
// System.Void UniRx.Operators.CreateObservable`1<System.Object>::.ctor(System.Func`2<UniRx.IObserver`1<T>,System.IDisposable>,System.Boolean)
extern "C"  void CreateObservable_1__ctor_m3293661784_gshared (CreateObservable_1_t2041049806 * __this, Func_2_t2619763055 * ___subscribe0, bool ___isRequiredSubscribeOnCurrentThread1, const MethodInfo* method)
{
	{
		bool L_0 = ___isRequiredSubscribeOnCurrentThread1;
		NullCheck((OperatorObservableBase_1_t4196218687 *)__this);
		((  void (*) (OperatorObservableBase_1_t4196218687 *, bool, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((OperatorObservableBase_1_t4196218687 *)__this, (bool)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		Func_2_t2619763055 * L_1 = ___subscribe0;
		__this->set_subscribe_1(L_1);
		return;
	}
}
// System.IDisposable UniRx.Operators.CreateObservable`1<System.Object>::SubscribeCore(UniRx.IObserver`1<T>,System.IDisposable)
extern Il2CppClass* Disposable_t3736388786_il2cpp_TypeInfo_var;
extern const uint32_t CreateObservable_1_SubscribeCore_m2669489670_MetadataUsageId;
extern "C"  Il2CppObject * CreateObservable_1_SubscribeCore_m2669489670_gshared (CreateObservable_1_t2041049806 * __this, Il2CppObject* ___observer0, Il2CppObject * ___cancel1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (CreateObservable_1_SubscribeCore_m2669489670_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Il2CppObject * G_B2_0 = NULL;
	Il2CppObject * G_B1_0 = NULL;
	{
		Il2CppObject* L_0 = ___observer0;
		Il2CppObject * L_1 = ___cancel1;
		Create_t642401845 * L_2 = (Create_t642401845 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		((  void (*) (Create_t642401845 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)(L_2, (Il2CppObject*)L_0, (Il2CppObject *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		___observer0 = (Il2CppObject*)L_2;
		Func_2_t2619763055 * L_3 = (Func_2_t2619763055 *)__this->get_subscribe_1();
		Il2CppObject* L_4 = ___observer0;
		NullCheck((Func_2_t2619763055 *)L_3);
		Il2CppObject * L_5 = ((  Il2CppObject * (*) (Func_2_t2619763055 *, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)((Func_2_t2619763055 *)L_3, (Il2CppObject*)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		Il2CppObject * L_6 = (Il2CppObject *)L_5;
		G_B1_0 = L_6;
		if (L_6)
		{
			G_B2_0 = L_6;
			goto IL_0021;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Disposable_t3736388786_il2cpp_TypeInfo_var);
		Il2CppObject * L_7 = ((Disposable_t3736388786_StaticFields*)Disposable_t3736388786_il2cpp_TypeInfo_var->static_fields)->get_Empty_0();
		G_B2_0 = L_7;
	}

IL_0021:
	{
		return G_B2_0;
	}
}
// System.Void UniRx.Operators.CreateObservable`1<System.Single>::.ctor(System.Func`2<UniRx.IObserver`1<T>,System.IDisposable>)
extern "C"  void CreateObservable_1__ctor_m2366941070_gshared (CreateObservable_1_t2162152407 * __this, Func_2_t3827670562 * ___subscribe0, const MethodInfo* method)
{
	{
		NullCheck((OperatorObservableBase_1_t22353992 *)__this);
		((  void (*) (OperatorObservableBase_1_t22353992 *, bool, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((OperatorObservableBase_1_t22353992 *)__this, (bool)1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		Func_2_t3827670562 * L_0 = ___subscribe0;
		__this->set_subscribe_1(L_0);
		return;
	}
}
// System.Void UniRx.Operators.CreateObservable`1<System.Single>::.ctor(System.Func`2<UniRx.IObserver`1<T>,System.IDisposable>,System.Boolean)
extern "C"  void CreateObservable_1__ctor_m2052185647_gshared (CreateObservable_1_t2162152407 * __this, Func_2_t3827670562 * ___subscribe0, bool ___isRequiredSubscribeOnCurrentThread1, const MethodInfo* method)
{
	{
		bool L_0 = ___isRequiredSubscribeOnCurrentThread1;
		NullCheck((OperatorObservableBase_1_t22353992 *)__this);
		((  void (*) (OperatorObservableBase_1_t22353992 *, bool, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((OperatorObservableBase_1_t22353992 *)__this, (bool)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		Func_2_t3827670562 * L_1 = ___subscribe0;
		__this->set_subscribe_1(L_1);
		return;
	}
}
// System.IDisposable UniRx.Operators.CreateObservable`1<System.Single>::SubscribeCore(UniRx.IObserver`1<T>,System.IDisposable)
extern Il2CppClass* Disposable_t3736388786_il2cpp_TypeInfo_var;
extern const uint32_t CreateObservable_1_SubscribeCore_m3312336157_MetadataUsageId;
extern "C"  Il2CppObject * CreateObservable_1_SubscribeCore_m3312336157_gshared (CreateObservable_1_t2162152407 * __this, Il2CppObject* ___observer0, Il2CppObject * ___cancel1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (CreateObservable_1_SubscribeCore_m3312336157_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Il2CppObject * G_B2_0 = NULL;
	Il2CppObject * G_B1_0 = NULL;
	{
		Il2CppObject* L_0 = ___observer0;
		Il2CppObject * L_1 = ___cancel1;
		Create_t763504446 * L_2 = (Create_t763504446 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		((  void (*) (Create_t763504446 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)(L_2, (Il2CppObject*)L_0, (Il2CppObject *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		___observer0 = (Il2CppObject*)L_2;
		Func_2_t3827670562 * L_3 = (Func_2_t3827670562 *)__this->get_subscribe_1();
		Il2CppObject* L_4 = ___observer0;
		NullCheck((Func_2_t3827670562 *)L_3);
		Il2CppObject * L_5 = ((  Il2CppObject * (*) (Func_2_t3827670562 *, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)((Func_2_t3827670562 *)L_3, (Il2CppObject*)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		Il2CppObject * L_6 = (Il2CppObject *)L_5;
		G_B1_0 = L_6;
		if (L_6)
		{
			G_B2_0 = L_6;
			goto IL_0021;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Disposable_t3736388786_il2cpp_TypeInfo_var);
		Il2CppObject * L_7 = ((Disposable_t3736388786_StaticFields*)Disposable_t3736388786_il2cpp_TypeInfo_var->static_fields)->get_Empty_0();
		G_B2_0 = L_7;
	}

IL_0021:
	{
		return G_B2_0;
	}
}
// System.Void UniRx.Operators.CreateObservable`1<UniRx.Unit>::.ctor(System.Func`2<UniRx.IObserver`1<T>,System.IDisposable>)
extern "C"  void CreateObservable_1__ctor_m1854901285_gshared (CreateObservable_1_t3762229424 * __this, Func_2_t3876191685 * ___subscribe0, const MethodInfo* method)
{
	{
		NullCheck((OperatorObservableBase_1_t1622431009 *)__this);
		((  void (*) (OperatorObservableBase_1_t1622431009 *, bool, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((OperatorObservableBase_1_t1622431009 *)__this, (bool)1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		Func_2_t3876191685 * L_0 = ___subscribe0;
		__this->set_subscribe_1(L_0);
		return;
	}
}
// System.Void UniRx.Operators.CreateObservable`1<UniRx.Unit>::.ctor(System.Func`2<UniRx.IObserver`1<T>,System.IDisposable>,System.Boolean)
extern "C"  void CreateObservable_1__ctor_m742152376_gshared (CreateObservable_1_t3762229424 * __this, Func_2_t3876191685 * ___subscribe0, bool ___isRequiredSubscribeOnCurrentThread1, const MethodInfo* method)
{
	{
		bool L_0 = ___isRequiredSubscribeOnCurrentThread1;
		NullCheck((OperatorObservableBase_1_t1622431009 *)__this);
		((  void (*) (OperatorObservableBase_1_t1622431009 *, bool, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((OperatorObservableBase_1_t1622431009 *)__this, (bool)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		Func_2_t3876191685 * L_1 = ___subscribe0;
		__this->set_subscribe_1(L_1);
		return;
	}
}
// System.IDisposable UniRx.Operators.CreateObservable`1<UniRx.Unit>::SubscribeCore(UniRx.IObserver`1<T>,System.IDisposable)
extern Il2CppClass* Disposable_t3736388786_il2cpp_TypeInfo_var;
extern const uint32_t CreateObservable_1_SubscribeCore_m1750972476_MetadataUsageId;
extern "C"  Il2CppObject * CreateObservable_1_SubscribeCore_m1750972476_gshared (CreateObservable_1_t3762229424 * __this, Il2CppObject* ___observer0, Il2CppObject * ___cancel1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (CreateObservable_1_SubscribeCore_m1750972476_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Il2CppObject * G_B2_0 = NULL;
	Il2CppObject * G_B1_0 = NULL;
	{
		Il2CppObject* L_0 = ___observer0;
		Il2CppObject * L_1 = ___cancel1;
		Create_t2363581463 * L_2 = (Create_t2363581463 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		((  void (*) (Create_t2363581463 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)(L_2, (Il2CppObject*)L_0, (Il2CppObject *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		___observer0 = (Il2CppObject*)L_2;
		Func_2_t3876191685 * L_3 = (Func_2_t3876191685 *)__this->get_subscribe_1();
		Il2CppObject* L_4 = ___observer0;
		NullCheck((Func_2_t3876191685 *)L_3);
		Il2CppObject * L_5 = ((  Il2CppObject * (*) (Func_2_t3876191685 *, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)((Func_2_t3876191685 *)L_3, (Il2CppObject*)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		Il2CppObject * L_6 = (Il2CppObject *)L_5;
		G_B1_0 = L_6;
		if (L_6)
		{
			G_B2_0 = L_6;
			goto IL_0021;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Disposable_t3736388786_il2cpp_TypeInfo_var);
		Il2CppObject * L_7 = ((Disposable_t3736388786_StaticFields*)Disposable_t3736388786_il2cpp_TypeInfo_var->static_fields)->get_Empty_0();
		G_B2_0 = L_7;
	}

IL_0021:
	{
		return G_B2_0;
	}
}
// System.Void UniRx.Operators.CreateObservable`1<UnityEngine.Vector2>::.ctor(System.Func`2<UniRx.IObserver`1<T>,System.IDisposable>)
extern "C"  void CreateObservable_1__ctor_m1286684063_gshared (CreateObservable_1_t434305878 * __this, Func_2_t1054655943 * ___subscribe0, const MethodInfo* method)
{
	{
		NullCheck((OperatorObservableBase_1_t2589474759 *)__this);
		((  void (*) (OperatorObservableBase_1_t2589474759 *, bool, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((OperatorObservableBase_1_t2589474759 *)__this, (bool)1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		Func_2_t1054655943 * L_0 = ___subscribe0;
		__this->set_subscribe_1(L_0);
		return;
	}
}
// System.Void UniRx.Operators.CreateObservable`1<UnityEngine.Vector2>::.ctor(System.Func`2<UniRx.IObserver`1<T>,System.IDisposable>,System.Boolean)
extern "C"  void CreateObservable_1__ctor_m2179351550_gshared (CreateObservable_1_t434305878 * __this, Func_2_t1054655943 * ___subscribe0, bool ___isRequiredSubscribeOnCurrentThread1, const MethodInfo* method)
{
	{
		bool L_0 = ___isRequiredSubscribeOnCurrentThread1;
		NullCheck((OperatorObservableBase_1_t2589474759 *)__this);
		((  void (*) (OperatorObservableBase_1_t2589474759 *, bool, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((OperatorObservableBase_1_t2589474759 *)__this, (bool)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		Func_2_t1054655943 * L_1 = ___subscribe0;
		__this->set_subscribe_1(L_1);
		return;
	}
}
// System.IDisposable UniRx.Operators.CreateObservable`1<UnityEngine.Vector2>::SubscribeCore(UniRx.IObserver`1<T>,System.IDisposable)
extern Il2CppClass* Disposable_t3736388786_il2cpp_TypeInfo_var;
extern const uint32_t CreateObservable_1_SubscribeCore_m21051500_MetadataUsageId;
extern "C"  Il2CppObject * CreateObservable_1_SubscribeCore_m21051500_gshared (CreateObservable_1_t434305878 * __this, Il2CppObject* ___observer0, Il2CppObject * ___cancel1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (CreateObservable_1_SubscribeCore_m21051500_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Il2CppObject * G_B2_0 = NULL;
	Il2CppObject * G_B1_0 = NULL;
	{
		Il2CppObject* L_0 = ___observer0;
		Il2CppObject * L_1 = ___cancel1;
		Create_t3330625213 * L_2 = (Create_t3330625213 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		((  void (*) (Create_t3330625213 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)(L_2, (Il2CppObject*)L_0, (Il2CppObject *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		___observer0 = (Il2CppObject*)L_2;
		Func_2_t1054655943 * L_3 = (Func_2_t1054655943 *)__this->get_subscribe_1();
		Il2CppObject* L_4 = ___observer0;
		NullCheck((Func_2_t1054655943 *)L_3);
		Il2CppObject * L_5 = ((  Il2CppObject * (*) (Func_2_t1054655943 *, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)((Func_2_t1054655943 *)L_3, (Il2CppObject*)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		Il2CppObject * L_6 = (Il2CppObject *)L_5;
		G_B1_0 = L_6;
		if (L_6)
		{
			G_B2_0 = L_6;
			goto IL_0021;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Disposable_t3736388786_il2cpp_TypeInfo_var);
		Il2CppObject * L_7 = ((Disposable_t3736388786_StaticFields*)Disposable_t3736388786_il2cpp_TypeInfo_var->static_fields)->get_Empty_0();
		G_B2_0 = L_7;
	}

IL_0021:
	{
		return G_B2_0;
	}
}
// System.Void UniRx.Operators.CreateSafeObservable`1/CreateSafe<System.Object>::.ctor(UniRx.IObserver`1<T>,System.IDisposable)
extern "C"  void CreateSafe__ctor_m903941043_gshared (CreateSafe_t861772378 * __this, Il2CppObject* ___observer0, Il2CppObject * ___cancel1, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = ___observer0;
		Il2CppObject * L_1 = ___cancel1;
		NullCheck((OperatorObserverBase_2_t1187768149 *)__this);
		((  void (*) (OperatorObserverBase_2_t1187768149 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((OperatorObserverBase_2_t1187768149 *)__this, (Il2CppObject*)L_0, (Il2CppObject *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		return;
	}
}
// System.Void UniRx.Operators.CreateSafeObservable`1/CreateSafe<System.Object>::OnNext(T)
extern Il2CppClass* Il2CppObject_il2cpp_TypeInfo_var;
extern const uint32_t CreateSafe_OnNext_m2277004287_MetadataUsageId;
extern "C"  void CreateSafe_OnNext_m2277004287_gshared (CreateSafe_t861772378 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (CreateSafe_OnNext_m2277004287_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		Il2CppObject* L_0 = (Il2CppObject*)((OperatorObserverBase_2_t1187768149 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		Il2CppObject * L_1 = ___value0;
		NullCheck((Il2CppObject*)L_0);
		InterfaceActionInvoker1< Il2CppObject * >::Invoke(2 /* System.Void UniRx.IObserver`1<System.Object>::OnNext(T) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), (Il2CppObject*)L_0, (Il2CppObject *)L_1);
		goto IL_0021;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1967233988 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Il2CppObject_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_0013;
		throw e;
	}

CATCH_0013:
	{ // begin catch(System.Object)
		{
			NullCheck((OperatorObserverBase_2_t1187768149 *)__this);
			VirtActionInvoker0::Invoke(4 /* System.Void UniRx.Operators.OperatorObserverBase`2<System.Object,System.Object>::Dispose() */, (OperatorObserverBase_2_t1187768149 *)__this);
			IL2CPP_RAISE_MANAGED_EXCEPTION(__exception_local);
		}

IL_001c:
		{
			goto IL_0021;
		}
	} // end catch (depth: 1)

IL_0021:
	{
		return;
	}
}
// System.Void UniRx.Operators.CreateSafeObservable`1/CreateSafe<System.Object>::OnError(System.Exception)
extern "C"  void CreateSafe_OnError_m1865060622_gshared (CreateSafe_t861772378 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method)
{
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		Il2CppObject* L_0 = (Il2CppObject*)((OperatorObserverBase_2_t1187768149 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		Exception_t1967233988 * L_1 = ___error0;
		NullCheck((Il2CppObject*)L_0);
		InterfaceActionInvoker1< Exception_t1967233988 * >::Invoke(1 /* System.Void UniRx.IObserver`1<System.Object>::OnError(System.Exception) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), (Il2CppObject*)L_0, (Exception_t1967233988 *)L_1);
		IL2CPP_LEAVE(0x1A, FINALLY_0013);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0013;
	}

FINALLY_0013:
	{ // begin finally (depth: 1)
		NullCheck((OperatorObserverBase_2_t1187768149 *)__this);
		VirtActionInvoker0::Invoke(4 /* System.Void UniRx.Operators.OperatorObserverBase`2<System.Object,System.Object>::Dispose() */, (OperatorObserverBase_2_t1187768149 *)__this);
		IL2CPP_END_FINALLY(19)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(19)
	{
		IL2CPP_JUMP_TBL(0x1A, IL_001a)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_001a:
	{
		return;
	}
}
// System.Void UniRx.Operators.CreateSafeObservable`1/CreateSafe<System.Object>::OnCompleted()
extern "C"  void CreateSafe_OnCompleted_m3765430625_gshared (CreateSafe_t861772378 * __this, const MethodInfo* method)
{
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		Il2CppObject* L_0 = (Il2CppObject*)((OperatorObserverBase_2_t1187768149 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		NullCheck((Il2CppObject*)L_0);
		InterfaceActionInvoker0::Invoke(0 /* System.Void UniRx.IObserver`1<System.Object>::OnCompleted() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), (Il2CppObject*)L_0);
		IL2CPP_LEAVE(0x19, FINALLY_0012);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0012;
	}

FINALLY_0012:
	{ // begin finally (depth: 1)
		NullCheck((OperatorObserverBase_2_t1187768149 *)__this);
		VirtActionInvoker0::Invoke(4 /* System.Void UniRx.Operators.OperatorObserverBase`2<System.Object,System.Object>::Dispose() */, (OperatorObserverBase_2_t1187768149 *)__this);
		IL2CPP_END_FINALLY(18)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(18)
	{
		IL2CPP_JUMP_TBL(0x19, IL_0019)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0019:
	{
		return;
	}
}
// System.Void UniRx.Operators.CreateSafeObservable`1<System.Object>::.ctor(System.Func`2<UniRx.IObserver`1<T>,System.IDisposable>)
extern "C"  void CreateSafeObservable_1__ctor_m1423138930_gshared (CreateSafeObservable_1_t4182587251 * __this, Func_2_t2619763055 * ___subscribe0, const MethodInfo* method)
{
	{
		NullCheck((OperatorObservableBase_1_t4196218687 *)__this);
		((  void (*) (OperatorObservableBase_1_t4196218687 *, bool, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((OperatorObservableBase_1_t4196218687 *)__this, (bool)1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		Func_2_t2619763055 * L_0 = ___subscribe0;
		__this->set_subscribe_1(L_0);
		return;
	}
}
// System.Void UniRx.Operators.CreateSafeObservable`1<System.Object>::.ctor(System.Func`2<UniRx.IObserver`1<T>,System.IDisposable>,System.Boolean)
extern "C"  void CreateSafeObservable_1__ctor_m1010155723_gshared (CreateSafeObservable_1_t4182587251 * __this, Func_2_t2619763055 * ___subscribe0, bool ___isRequiredSubscribeOnCurrentThread1, const MethodInfo* method)
{
	{
		bool L_0 = ___isRequiredSubscribeOnCurrentThread1;
		NullCheck((OperatorObservableBase_1_t4196218687 *)__this);
		((  void (*) (OperatorObservableBase_1_t4196218687 *, bool, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((OperatorObservableBase_1_t4196218687 *)__this, (bool)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		Func_2_t2619763055 * L_1 = ___subscribe0;
		__this->set_subscribe_1(L_1);
		return;
	}
}
// System.IDisposable UniRx.Operators.CreateSafeObservable`1<System.Object>::SubscribeCore(UniRx.IObserver`1<T>,System.IDisposable)
extern Il2CppClass* Disposable_t3736388786_il2cpp_TypeInfo_var;
extern const uint32_t CreateSafeObservable_1_SubscribeCore_m2639913529_MetadataUsageId;
extern "C"  Il2CppObject * CreateSafeObservable_1_SubscribeCore_m2639913529_gshared (CreateSafeObservable_1_t4182587251 * __this, Il2CppObject* ___observer0, Il2CppObject * ___cancel1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (CreateSafeObservable_1_SubscribeCore_m2639913529_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Il2CppObject * G_B2_0 = NULL;
	Il2CppObject * G_B1_0 = NULL;
	{
		Il2CppObject* L_0 = ___observer0;
		Il2CppObject * L_1 = ___cancel1;
		CreateSafe_t861772378 * L_2 = (CreateSafe_t861772378 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		((  void (*) (CreateSafe_t861772378 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)(L_2, (Il2CppObject*)L_0, (Il2CppObject *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		___observer0 = (Il2CppObject*)L_2;
		Func_2_t2619763055 * L_3 = (Func_2_t2619763055 *)__this->get_subscribe_1();
		Il2CppObject* L_4 = ___observer0;
		NullCheck((Func_2_t2619763055 *)L_3);
		Il2CppObject * L_5 = ((  Il2CppObject * (*) (Func_2_t2619763055 *, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)((Func_2_t2619763055 *)L_3, (Il2CppObject*)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		Il2CppObject * L_6 = (Il2CppObject *)L_5;
		G_B1_0 = L_6;
		if (L_6)
		{
			G_B2_0 = L_6;
			goto IL_0021;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Disposable_t3736388786_il2cpp_TypeInfo_var);
		Il2CppObject * L_7 = ((Disposable_t3736388786_StaticFields*)Disposable_t3736388786_il2cpp_TypeInfo_var->static_fields)->get_Empty_0();
		G_B2_0 = L_7;
	}

IL_0021:
	{
		return G_B2_0;
	}
}
// System.Void UniRx.Operators.DefaultIfEmptyObservable`1/DefaultIfEmpty<System.Object>::.ctor(UniRx.Operators.DefaultIfEmptyObservable`1<T>,UniRx.IObserver`1<T>,System.IDisposable)
extern "C"  void DefaultIfEmpty__ctor_m1237426643_gshared (DefaultIfEmpty_t3912419440 * __this, DefaultIfEmptyObservable_1_t2718954633 * ___parent0, Il2CppObject* ___observer1, Il2CppObject * ___cancel2, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = ___observer1;
		Il2CppObject * L_1 = ___cancel2;
		NullCheck((OperatorObserverBase_2_t1187768149 *)__this);
		((  void (*) (OperatorObserverBase_2_t1187768149 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((OperatorObserverBase_2_t1187768149 *)__this, (Il2CppObject*)L_0, (Il2CppObject *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		DefaultIfEmptyObservable_1_t2718954633 * L_2 = ___parent0;
		__this->set_parent_2(L_2);
		__this->set_hasValue_3((bool)0);
		return;
	}
}
// System.Void UniRx.Operators.DefaultIfEmptyObservable`1/DefaultIfEmpty<System.Object>::OnNext(T)
extern "C"  void DefaultIfEmpty_OnNext_m2296050239_gshared (DefaultIfEmpty_t3912419440 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	{
		__this->set_hasValue_3((bool)1);
		Il2CppObject* L_0 = (Il2CppObject*)((OperatorObserverBase_2_t1187768149 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		Il2CppObject * L_1 = ___value0;
		NullCheck((Il2CppObject*)L_0);
		InterfaceActionInvoker1< Il2CppObject * >::Invoke(2 /* System.Void UniRx.IObserver`1<System.Object>::OnNext(T) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), (Il2CppObject*)L_0, (Il2CppObject *)L_1);
		return;
	}
}
// System.Void UniRx.Operators.DefaultIfEmptyObservable`1/DefaultIfEmpty<System.Object>::OnError(System.Exception)
extern "C"  void DefaultIfEmpty_OnError_m3989352270_gshared (DefaultIfEmpty_t3912419440 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method)
{
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		Il2CppObject* L_0 = (Il2CppObject*)((OperatorObserverBase_2_t1187768149 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		Exception_t1967233988 * L_1 = ___error0;
		NullCheck((Il2CppObject*)L_0);
		InterfaceActionInvoker1< Exception_t1967233988 * >::Invoke(1 /* System.Void UniRx.IObserver`1<System.Object>::OnError(System.Exception) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), (Il2CppObject*)L_0, (Exception_t1967233988 *)L_1);
		IL2CPP_LEAVE(0x1A, FINALLY_0013);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0013;
	}

FINALLY_0013:
	{ // begin finally (depth: 1)
		NullCheck((OperatorObserverBase_2_t1187768149 *)__this);
		VirtActionInvoker0::Invoke(4 /* System.Void UniRx.Operators.OperatorObserverBase`2<System.Object,System.Object>::Dispose() */, (OperatorObserverBase_2_t1187768149 *)__this);
		IL2CPP_END_FINALLY(19)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(19)
	{
		IL2CPP_JUMP_TBL(0x1A, IL_001a)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_001a:
	{
		return;
	}
}
// System.Void UniRx.Operators.DefaultIfEmptyObservable`1/DefaultIfEmpty<System.Object>::OnCompleted()
extern "C"  void DefaultIfEmpty_OnCompleted_m916023201_gshared (DefaultIfEmpty_t3912419440 * __this, const MethodInfo* method)
{
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		bool L_0 = (bool)__this->get_hasValue_3();
		if (L_0)
		{
			goto IL_0023;
		}
	}
	{
		Il2CppObject* L_1 = (Il2CppObject*)((OperatorObserverBase_2_t1187768149 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		DefaultIfEmptyObservable_1_t2718954633 * L_2 = (DefaultIfEmptyObservable_1_t2718954633 *)__this->get_parent_2();
		NullCheck(L_2);
		Il2CppObject * L_3 = (Il2CppObject *)L_2->get_defaultValue_2();
		NullCheck((Il2CppObject*)L_1);
		InterfaceActionInvoker1< Il2CppObject * >::Invoke(2 /* System.Void UniRx.IObserver`1<System.Object>::OnNext(T) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), (Il2CppObject*)L_1, (Il2CppObject *)L_3);
	}

IL_0023:
	try
	{ // begin try (depth: 1)
		Il2CppObject* L_4 = (Il2CppObject*)((OperatorObserverBase_2_t1187768149 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		NullCheck((Il2CppObject*)L_4);
		InterfaceActionInvoker0::Invoke(0 /* System.Void UniRx.IObserver`1<System.Object>::OnCompleted() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), (Il2CppObject*)L_4);
		IL2CPP_LEAVE(0x3C, FINALLY_0035);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0035;
	}

FINALLY_0035:
	{ // begin finally (depth: 1)
		NullCheck((OperatorObserverBase_2_t1187768149 *)__this);
		VirtActionInvoker0::Invoke(4 /* System.Void UniRx.Operators.OperatorObserverBase`2<System.Object,System.Object>::Dispose() */, (OperatorObserverBase_2_t1187768149 *)__this);
		IL2CPP_END_FINALLY(53)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(53)
	{
		IL2CPP_JUMP_TBL(0x3C, IL_003c)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_003c:
	{
		return;
	}
}
// System.Void UniRx.Operators.DefaultIfEmptyObservable`1<System.Object>::.ctor(UniRx.IObservable`1<T>,T)
extern "C"  void DefaultIfEmptyObservable_1__ctor_m3746922412_gshared (DefaultIfEmptyObservable_1_t2718954633 * __this, Il2CppObject* ___source0, Il2CppObject * ___defaultValue1, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = ___source0;
		bool L_1 = ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)(NULL /*static, unused*/, (Il2CppObject*)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		NullCheck((OperatorObservableBase_1_t4196218687 *)__this);
		((  void (*) (OperatorObservableBase_1_t4196218687 *, bool, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)((OperatorObservableBase_1_t4196218687 *)__this, (bool)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		Il2CppObject* L_2 = ___source0;
		__this->set_source_1(L_2);
		Il2CppObject * L_3 = ___defaultValue1;
		__this->set_defaultValue_2(L_3);
		return;
	}
}
// System.IDisposable UniRx.Operators.DefaultIfEmptyObservable`1<System.Object>::SubscribeCore(UniRx.IObserver`1<T>,System.IDisposable)
extern "C"  Il2CppObject * DefaultIfEmptyObservable_1_SubscribeCore_m3634043251_gshared (DefaultIfEmptyObservable_1_t2718954633 * __this, Il2CppObject* ___observer0, Il2CppObject * ___cancel1, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = (Il2CppObject*)__this->get_source_1();
		Il2CppObject* L_1 = ___observer0;
		Il2CppObject * L_2 = ___cancel1;
		DefaultIfEmpty_t3912419440 * L_3 = (DefaultIfEmpty_t3912419440 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		((  void (*) (DefaultIfEmpty_t3912419440 *, DefaultIfEmptyObservable_1_t2718954633 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)(L_3, (DefaultIfEmptyObservable_1_t2718954633 *)__this, (Il2CppObject*)L_1, (Il2CppObject *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		NullCheck((Il2CppObject*)L_0);
		Il2CppObject * L_4 = InterfaceFuncInvoker1< Il2CppObject *, Il2CppObject* >::Invoke(0 /* System.IDisposable UniRx.IObservable`1<System.Object>::Subscribe(UniRx.IObserver`1<T>) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5), (Il2CppObject*)L_0, (Il2CppObject*)L_3);
		return L_4;
	}
}
// System.Void UniRx.Operators.DeferObservable`1/Defer<System.Object>::.ctor(UniRx.IObserver`1<T>,System.IDisposable)
extern "C"  void Defer__ctor_m1777074501_gshared (Defer_t3471526139 * __this, Il2CppObject* ___observer0, Il2CppObject * ___cancel1, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = ___observer0;
		Il2CppObject * L_1 = ___cancel1;
		NullCheck((OperatorObserverBase_2_t1187768149 *)__this);
		((  void (*) (OperatorObserverBase_2_t1187768149 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((OperatorObserverBase_2_t1187768149 *)__this, (Il2CppObject*)L_0, (Il2CppObject *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		return;
	}
}
// System.Void UniRx.Operators.DeferObservable`1/Defer<System.Object>::OnNext(T)
extern Il2CppClass* Il2CppObject_il2cpp_TypeInfo_var;
extern const uint32_t Defer_OnNext_m3829588781_MetadataUsageId;
extern "C"  void Defer_OnNext_m3829588781_gshared (Defer_t3471526139 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Defer_OnNext_m3829588781_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		Il2CppObject* L_0 = (Il2CppObject*)((OperatorObserverBase_2_t1187768149 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		Il2CppObject * L_1 = ___value0;
		NullCheck((Il2CppObject*)L_0);
		InterfaceActionInvoker1< Il2CppObject * >::Invoke(2 /* System.Void UniRx.IObserver`1<System.Object>::OnNext(T) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), (Il2CppObject*)L_0, (Il2CppObject *)L_1);
		goto IL_0021;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1967233988 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Il2CppObject_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_0013;
		throw e;
	}

CATCH_0013:
	{ // begin catch(System.Object)
		{
			NullCheck((OperatorObserverBase_2_t1187768149 *)__this);
			VirtActionInvoker0::Invoke(4 /* System.Void UniRx.Operators.OperatorObserverBase`2<System.Object,System.Object>::Dispose() */, (OperatorObserverBase_2_t1187768149 *)__this);
			IL2CPP_RAISE_MANAGED_EXCEPTION(__exception_local);
		}

IL_001c:
		{
			goto IL_0021;
		}
	} // end catch (depth: 1)

IL_0021:
	{
		return;
	}
}
// System.Void UniRx.Operators.DeferObservable`1/Defer<System.Object>::OnError(System.Exception)
extern "C"  void Defer_OnError_m3644654652_gshared (Defer_t3471526139 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method)
{
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		Il2CppObject* L_0 = (Il2CppObject*)((OperatorObserverBase_2_t1187768149 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		Exception_t1967233988 * L_1 = ___error0;
		NullCheck((Il2CppObject*)L_0);
		InterfaceActionInvoker1< Exception_t1967233988 * >::Invoke(1 /* System.Void UniRx.IObserver`1<System.Object>::OnError(System.Exception) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), (Il2CppObject*)L_0, (Exception_t1967233988 *)L_1);
		IL2CPP_LEAVE(0x1A, FINALLY_0013);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0013;
	}

FINALLY_0013:
	{ // begin finally (depth: 1)
		NullCheck((OperatorObserverBase_2_t1187768149 *)__this);
		VirtActionInvoker0::Invoke(4 /* System.Void UniRx.Operators.OperatorObserverBase`2<System.Object,System.Object>::Dispose() */, (OperatorObserverBase_2_t1187768149 *)__this);
		IL2CPP_END_FINALLY(19)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(19)
	{
		IL2CPP_JUMP_TBL(0x1A, IL_001a)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_001a:
	{
		return;
	}
}
// System.Void UniRx.Operators.DeferObservable`1/Defer<System.Object>::OnCompleted()
extern "C"  void Defer_OnCompleted_m3382915471_gshared (Defer_t3471526139 * __this, const MethodInfo* method)
{
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		Il2CppObject* L_0 = (Il2CppObject*)((OperatorObserverBase_2_t1187768149 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		NullCheck((Il2CppObject*)L_0);
		InterfaceActionInvoker0::Invoke(0 /* System.Void UniRx.IObserver`1<System.Object>::OnCompleted() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), (Il2CppObject*)L_0);
		IL2CPP_LEAVE(0x19, FINALLY_0012);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0012;
	}

FINALLY_0012:
	{ // begin finally (depth: 1)
		NullCheck((OperatorObserverBase_2_t1187768149 *)__this);
		VirtActionInvoker0::Invoke(4 /* System.Void UniRx.Operators.OperatorObserverBase`2<System.Object,System.Object>::Dispose() */, (OperatorObserverBase_2_t1187768149 *)__this);
		IL2CPP_END_FINALLY(18)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(18)
	{
		IL2CPP_JUMP_TBL(0x19, IL_0019)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0019:
	{
		return;
	}
}
// System.Void UniRx.Operators.DeferObservable`1<System.Object>::.ctor(System.Func`1<UniRx.IObservable`1<T>>)
extern "C"  void DeferObservable_1__ctor_m1920396347_gshared (DeferObservable_1_t3089955988 * __this, Func_1_t1738686031 * ___observableFactory0, const MethodInfo* method)
{
	{
		NullCheck((OperatorObservableBase_1_t4196218687 *)__this);
		((  void (*) (OperatorObservableBase_1_t4196218687 *, bool, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((OperatorObservableBase_1_t4196218687 *)__this, (bool)0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		Func_1_t1738686031 * L_0 = ___observableFactory0;
		__this->set_observableFactory_1(L_0);
		return;
	}
}
// System.IDisposable UniRx.Operators.DeferObservable`1<System.Object>::SubscribeCore(UniRx.IObserver`1<T>,System.IDisposable)
extern Il2CppClass* Exception_t1967233988_il2cpp_TypeInfo_var;
extern Il2CppClass* Observable_t258071701_il2cpp_TypeInfo_var;
extern const uint32_t DeferObservable_1_SubscribeCore_m1896247388_MetadataUsageId;
extern "C"  Il2CppObject * DeferObservable_1_SubscribeCore_m1896247388_gshared (DeferObservable_1_t3089955988 * __this, Il2CppObject* ___observer0, Il2CppObject * ___cancel1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DeferObservable_1_SubscribeCore_m1896247388_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Il2CppObject* V_0 = NULL;
	Exception_t1967233988 * V_1 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Il2CppObject* L_0 = ___observer0;
		Il2CppObject * L_1 = ___cancel1;
		Defer_t3471526139 * L_2 = (Defer_t3471526139 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		((  void (*) (Defer_t3471526139 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)(L_2, (Il2CppObject*)L_0, (Il2CppObject *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		___observer0 = (Il2CppObject*)L_2;
	}

IL_0009:
	try
	{ // begin try (depth: 1)
		Func_1_t1738686031 * L_3 = (Func_1_t1738686031 *)__this->get_observableFactory_1();
		NullCheck((Func_1_t1738686031 *)L_3);
		Il2CppObject* L_4 = ((  Il2CppObject* (*) (Func_1_t1738686031 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)((Func_1_t1738686031 *)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		V_0 = (Il2CppObject*)L_4;
		goto IL_0027;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1967233988 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Exception_t1967233988_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_001a;
		throw e;
	}

CATCH_001a:
	{ // begin catch(System.Exception)
		V_1 = (Exception_t1967233988 *)((Exception_t1967233988 *)__exception_local);
		Exception_t1967233988 * L_5 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Observable_t258071701_il2cpp_TypeInfo_var);
		Il2CppObject* L_6 = ((  Il2CppObject* (*) (Il2CppObject * /* static, unused */, Exception_t1967233988 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->method)(NULL /*static, unused*/, (Exception_t1967233988 *)L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		V_0 = (Il2CppObject*)L_6;
		goto IL_0027;
	} // end catch (depth: 1)

IL_0027:
	{
		Il2CppObject* L_7 = V_0;
		Il2CppObject* L_8 = ___observer0;
		NullCheck((Il2CppObject*)L_7);
		Il2CppObject * L_9 = InterfaceFuncInvoker1< Il2CppObject *, Il2CppObject* >::Invoke(0 /* System.IDisposable UniRx.IObservable`1<System.Object>::Subscribe(UniRx.IObserver`1<T>) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6), (Il2CppObject*)L_7, (Il2CppObject*)L_8);
		return L_9;
	}
}
// System.Void UniRx.Operators.DelayFrameObservable`1/DelayFrame/<OnCompletedDelay>c__Iterator27<System.Object>::.ctor()
extern "C"  void U3COnCompletedDelayU3Ec__Iterator27__ctor_m3657059411_gshared (U3COnCompletedDelayU3Ec__Iterator27_t259619244 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Object UniRx.Operators.DelayFrameObservable`1/DelayFrame/<OnCompletedDelay>c__Iterator27<System.Object>::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3COnCompletedDelayU3Ec__Iterator27_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1027862633_gshared (U3COnCompletedDelayU3Ec__Iterator27_t259619244 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_U24current_2();
		return L_0;
	}
}
// System.Object UniRx.Operators.DelayFrameObservable`1/DelayFrame/<OnCompletedDelay>c__Iterator27<System.Object>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3COnCompletedDelayU3Ec__Iterator27_System_Collections_IEnumerator_get_Current_m617473533_gshared (U3COnCompletedDelayU3Ec__Iterator27_t259619244 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_U24current_2();
		return L_0;
	}
}
// System.Boolean UniRx.Operators.DelayFrameObservable`1/DelayFrame/<OnCompletedDelay>c__Iterator27<System.Object>::MoveNext()
extern "C"  bool U3COnCompletedDelayU3Ec__Iterator27_MoveNext_m1159235473_gshared (U3COnCompletedDelayU3Ec__Iterator27_t259619244 * __this, const MethodInfo* method)
{
	uint32_t V_0 = 0;
	int32_t V_1 = 0;
	bool V_2 = false;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		int32_t L_0 = (int32_t)__this->get_U24PC_1();
		V_0 = (uint32_t)L_0;
		__this->set_U24PC_1((-1));
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0021;
		}
		if (L_1 == 1)
		{
			goto IL_0059;
		}
	}
	{
		goto IL_00ce;
	}

IL_0021:
	{
		DelayFrame_t1619463347 * L_2 = (DelayFrame_t1619463347 *)__this->get_U3CU3Ef__this_3();
		NullCheck(L_2);
		DelayFrameObservable_1_t4117294156 * L_3 = (DelayFrameObservable_1_t4117294156 *)L_2->get_parent_2();
		NullCheck(L_3);
		int32_t L_4 = (int32_t)L_3->get_frameCount_2();
		__this->set_U3CframeCountU3E__0_0(L_4);
		goto IL_0059;
	}

IL_003c:
	{
		DelayFrame_t1619463347 * L_5 = (DelayFrame_t1619463347 *)__this->get_U3CU3Ef__this_3();
		NullCheck(L_5);
		YieldInstruction_t3557331758 * L_6 = (YieldInstruction_t3557331758 *)L_5->get_yieldInstruction_3();
		__this->set_U24current_2(L_6);
		__this->set_U24PC_1(1);
		goto IL_00d0;
	}

IL_0059:
	{
		DelayFrame_t1619463347 * L_7 = (DelayFrame_t1619463347 *)__this->get_U3CU3Ef__this_3();
		NullCheck(L_7);
		BooleanDisposable_t3065601722 * L_8 = (BooleanDisposable_t3065601722 *)L_7->get_coroutineKey_4();
		NullCheck((BooleanDisposable_t3065601722 *)L_8);
		bool L_9 = VirtFuncInvoker0< bool >::Invoke(5 /* System.Boolean UniRx.BooleanDisposable::get_IsDisposed() */, (BooleanDisposable_t3065601722 *)L_8);
		if (L_9)
		{
			goto IL_0084;
		}
	}
	{
		int32_t L_10 = (int32_t)__this->get_U3CframeCountU3E__0_0();
		int32_t L_11 = (int32_t)L_10;
		V_1 = (int32_t)L_11;
		__this->set_U3CframeCountU3E__0_0(((int32_t)((int32_t)L_11-(int32_t)1)));
		int32_t L_12 = V_1;
		if (L_12)
		{
			goto IL_003c;
		}
	}

IL_0084:
	{
		DelayFrame_t1619463347 * L_13 = (DelayFrame_t1619463347 *)__this->get_U3CU3Ef__this_3();
		NullCheck(L_13);
		BooleanDisposable_t3065601722 * L_14 = (BooleanDisposable_t3065601722 *)L_13->get_coroutineKey_4();
		NullCheck((BooleanDisposable_t3065601722 *)L_14);
		bool L_15 = VirtFuncInvoker0< bool >::Invoke(5 /* System.Boolean UniRx.BooleanDisposable::get_IsDisposed() */, (BooleanDisposable_t3065601722 *)L_14);
		if (L_15)
		{
			goto IL_00c7;
		}
	}
	{
		DelayFrame_t1619463347 * L_16 = (DelayFrame_t1619463347 *)__this->get_U3CU3Ef__this_3();
		NullCheck(L_16);
		BooleanDisposable_t3065601722 * L_17 = (BooleanDisposable_t3065601722 *)L_16->get_coroutineKey_4();
		NullCheck((BooleanDisposable_t3065601722 *)L_17);
		VirtActionInvoker0::Invoke(4 /* System.Void UniRx.BooleanDisposable::Dispose() */, (BooleanDisposable_t3065601722 *)L_17);
	}

IL_00a9:
	try
	{ // begin try (depth: 1)
		DelayFrame_t1619463347 * L_18 = (DelayFrame_t1619463347 *)__this->get_U3CU3Ef__this_3();
		NullCheck(L_18);
		Il2CppObject* L_19 = (Il2CppObject*)((OperatorObserverBase_2_t1187768149 *)L_18)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		NullCheck((Il2CppObject*)L_19);
		InterfaceActionInvoker0::Invoke(0 /* System.Void UniRx.IObserver`1<System.Object>::OnCompleted() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0), (Il2CppObject*)L_19);
		IL2CPP_LEAVE(0xC7, FINALLY_00c0);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_00c0;
	}

FINALLY_00c0:
	{ // begin finally (depth: 1)
		NullCheck((U3COnCompletedDelayU3Ec__Iterator27_t259619244 *)__this);
		((  void (*) (U3COnCompletedDelayU3Ec__Iterator27_t259619244 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)((U3COnCompletedDelayU3Ec__Iterator27_t259619244 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		IL2CPP_END_FINALLY(192)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(192)
	{
		IL2CPP_JUMP_TBL(0xC7, IL_00c7)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_00c7:
	{
		__this->set_U24PC_1((-1));
	}

IL_00ce:
	{
		return (bool)0;
	}

IL_00d0:
	{
		return (bool)1;
	}
	// Dead block : IL_00d2: ldloc.2
}
// System.Void UniRx.Operators.DelayFrameObservable`1/DelayFrame/<OnCompletedDelay>c__Iterator27<System.Object>::Dispose()
extern "C"  void U3COnCompletedDelayU3Ec__Iterator27_Dispose_m1049128912_gshared (U3COnCompletedDelayU3Ec__Iterator27_t259619244 * __this, const MethodInfo* method)
{
	{
		__this->set_U24PC_1((-1));
		return;
	}
}
// System.Void UniRx.Operators.DelayFrameObservable`1/DelayFrame/<OnCompletedDelay>c__Iterator27<System.Object>::Reset()
extern Il2CppClass* NotSupportedException_t1374155497_il2cpp_TypeInfo_var;
extern const uint32_t U3COnCompletedDelayU3Ec__Iterator27_Reset_m1303492352_MetadataUsageId;
extern "C"  void U3COnCompletedDelayU3Ec__Iterator27_Reset_m1303492352_gshared (U3COnCompletedDelayU3Ec__Iterator27_t259619244 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3COnCompletedDelayU3Ec__Iterator27_Reset_m1303492352_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1374155497 * L_0 = (NotSupportedException_t1374155497 *)il2cpp_codegen_object_new(NotSupportedException_t1374155497_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m149930845(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void UniRx.Operators.DelayFrameObservable`1/DelayFrame/<OnCompletedDelay>c__Iterator27<System.Object>::<>__Finally0()
extern "C"  void U3COnCompletedDelayU3Ec__Iterator27_U3CU3E__Finally0_m3281021632_gshared (U3COnCompletedDelayU3Ec__Iterator27_t259619244 * __this, const MethodInfo* method)
{
	{
		DelayFrame_t1619463347 * L_0 = (DelayFrame_t1619463347 *)__this->get_U3CU3Ef__this_3();
		NullCheck((OperatorObserverBase_2_t1187768149 *)L_0);
		VirtActionInvoker0::Invoke(4 /* System.Void UniRx.Operators.OperatorObserverBase`2<System.Object,System.Object>::Dispose() */, (OperatorObserverBase_2_t1187768149 *)L_0);
		return;
	}
}
// System.Void UniRx.Operators.DelayFrameObservable`1/DelayFrame/<OnNextDelay>c__Iterator26<System.Object>::.ctor()
extern "C"  void U3COnNextDelayU3Ec__Iterator26__ctor_m138004466_gshared (U3COnNextDelayU3Ec__Iterator26_t3660093125 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Object UniRx.Operators.DelayFrameObservable`1/DelayFrame/<OnNextDelay>c__Iterator26<System.Object>::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3COnNextDelayU3Ec__Iterator26_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m2756900640_gshared (U3COnNextDelayU3Ec__Iterator26_t3660093125 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_U24current_3();
		return L_0;
	}
}
// System.Object UniRx.Operators.DelayFrameObservable`1/DelayFrame/<OnNextDelay>c__Iterator26<System.Object>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3COnNextDelayU3Ec__Iterator26_System_Collections_IEnumerator_get_Current_m3617012404_gshared (U3COnNextDelayU3Ec__Iterator26_t3660093125 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_U24current_3();
		return L_0;
	}
}
// System.Boolean UniRx.Operators.DelayFrameObservable`1/DelayFrame/<OnNextDelay>c__Iterator26<System.Object>::MoveNext()
extern "C"  bool U3COnNextDelayU3Ec__Iterator26_MoveNext_m2925189850_gshared (U3COnNextDelayU3Ec__Iterator26_t3660093125 * __this, const MethodInfo* method)
{
	uint32_t V_0 = 0;
	int32_t V_1 = 0;
	bool V_2 = false;
	{
		int32_t L_0 = (int32_t)__this->get_U24PC_2();
		V_0 = (uint32_t)L_0;
		__this->set_U24PC_2((-1));
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0021;
		}
		if (L_1 == 1)
		{
			goto IL_0059;
		}
	}
	{
		goto IL_00b8;
	}

IL_0021:
	{
		DelayFrame_t1619463347 * L_2 = (DelayFrame_t1619463347 *)__this->get_U3CU3Ef__this_5();
		NullCheck(L_2);
		DelayFrameObservable_1_t4117294156 * L_3 = (DelayFrameObservable_1_t4117294156 *)L_2->get_parent_2();
		NullCheck(L_3);
		int32_t L_4 = (int32_t)L_3->get_frameCount_2();
		__this->set_U3CframeCountU3E__0_0(L_4);
		goto IL_0059;
	}

IL_003c:
	{
		DelayFrame_t1619463347 * L_5 = (DelayFrame_t1619463347 *)__this->get_U3CU3Ef__this_5();
		NullCheck(L_5);
		YieldInstruction_t3557331758 * L_6 = (YieldInstruction_t3557331758 *)L_5->get_yieldInstruction_3();
		__this->set_U24current_3(L_6);
		__this->set_U24PC_2(1);
		goto IL_00ba;
	}

IL_0059:
	{
		DelayFrame_t1619463347 * L_7 = (DelayFrame_t1619463347 *)__this->get_U3CU3Ef__this_5();
		NullCheck(L_7);
		BooleanDisposable_t3065601722 * L_8 = (BooleanDisposable_t3065601722 *)L_7->get_coroutineKey_4();
		NullCheck((BooleanDisposable_t3065601722 *)L_8);
		bool L_9 = VirtFuncInvoker0< bool >::Invoke(5 /* System.Boolean UniRx.BooleanDisposable::get_IsDisposed() */, (BooleanDisposable_t3065601722 *)L_8);
		if (L_9)
		{
			goto IL_0084;
		}
	}
	{
		int32_t L_10 = (int32_t)__this->get_U3CframeCountU3E__0_0();
		int32_t L_11 = (int32_t)L_10;
		V_1 = (int32_t)L_11;
		__this->set_U3CframeCountU3E__0_0(((int32_t)((int32_t)L_11-(int32_t)1)));
		int32_t L_12 = V_1;
		if (L_12)
		{
			goto IL_003c;
		}
	}

IL_0084:
	{
		DelayFrame_t1619463347 * L_13 = (DelayFrame_t1619463347 *)__this->get_U3CU3Ef__this_5();
		NullCheck(L_13);
		BooleanDisposable_t3065601722 * L_14 = (BooleanDisposable_t3065601722 *)L_13->get_coroutineKey_4();
		NullCheck((BooleanDisposable_t3065601722 *)L_14);
		bool L_15 = VirtFuncInvoker0< bool >::Invoke(5 /* System.Boolean UniRx.BooleanDisposable::get_IsDisposed() */, (BooleanDisposable_t3065601722 *)L_14);
		if (L_15)
		{
			goto IL_00b1;
		}
	}
	{
		DelayFrame_t1619463347 * L_16 = (DelayFrame_t1619463347 *)__this->get_U3CU3Ef__this_5();
		NullCheck(L_16);
		Il2CppObject* L_17 = (Il2CppObject*)((OperatorObserverBase_2_t1187768149 *)L_16)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		Il2CppObject * L_18 = (Il2CppObject *)__this->get_value_1();
		NullCheck((Il2CppObject*)L_17);
		InterfaceActionInvoker1< Il2CppObject * >::Invoke(2 /* System.Void UniRx.IObserver`1<System.Object>::OnNext(T) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0), (Il2CppObject*)L_17, (Il2CppObject *)L_18);
	}

IL_00b1:
	{
		__this->set_U24PC_2((-1));
	}

IL_00b8:
	{
		return (bool)0;
	}

IL_00ba:
	{
		return (bool)1;
	}
	// Dead block : IL_00bc: ldloc.2
}
// System.Void UniRx.Operators.DelayFrameObservable`1/DelayFrame/<OnNextDelay>c__Iterator26<System.Object>::Dispose()
extern "C"  void U3COnNextDelayU3Ec__Iterator26_Dispose_m3671556015_gshared (U3COnNextDelayU3Ec__Iterator26_t3660093125 * __this, const MethodInfo* method)
{
	{
		__this->set_U24PC_2((-1));
		return;
	}
}
// System.Void UniRx.Operators.DelayFrameObservable`1/DelayFrame/<OnNextDelay>c__Iterator26<System.Object>::Reset()
extern Il2CppClass* NotSupportedException_t1374155497_il2cpp_TypeInfo_var;
extern const uint32_t U3COnNextDelayU3Ec__Iterator26_Reset_m2079404703_MetadataUsageId;
extern "C"  void U3COnNextDelayU3Ec__Iterator26_Reset_m2079404703_gshared (U3COnNextDelayU3Ec__Iterator26_t3660093125 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3COnNextDelayU3Ec__Iterator26_Reset_m2079404703_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1374155497 * L_0 = (NotSupportedException_t1374155497 *)il2cpp_codegen_object_new(NotSupportedException_t1374155497_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m149930845(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void UniRx.Operators.DelayFrameObservable`1/DelayFrame<System.Object>::.ctor(UniRx.Operators.DelayFrameObservable`1<T>,UniRx.IObserver`1<T>,System.IDisposable)
extern "C"  void DelayFrame__ctor_m2525345198_gshared (DelayFrame_t1619463347 * __this, DelayFrameObservable_1_t4117294156 * ___parent0, Il2CppObject* ___observer1, Il2CppObject * ___cancel2, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = ___observer1;
		Il2CppObject * L_1 = ___cancel2;
		NullCheck((OperatorObserverBase_2_t1187768149 *)__this);
		((  void (*) (OperatorObserverBase_2_t1187768149 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((OperatorObserverBase_2_t1187768149 *)__this, (Il2CppObject*)L_0, (Il2CppObject *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		DelayFrameObservable_1_t4117294156 * L_2 = ___parent0;
		__this->set_parent_2(L_2);
		DelayFrameObservable_1_t4117294156 * L_3 = ___parent0;
		NullCheck(L_3);
		int32_t L_4 = (int32_t)L_3->get_frameCountType_3();
		YieldInstruction_t3557331758 * L_5 = FrameCountTypeExtensions_GetYieldInstruction_m1644022139(NULL /*static, unused*/, (int32_t)L_4, /*hidden argument*/NULL);
		__this->set_yieldInstruction_3(L_5);
		return;
	}
}
// System.IDisposable UniRx.Operators.DelayFrameObservable`1/DelayFrame<System.Object>::Run()
extern Il2CppClass* BooleanDisposable_t3065601722_il2cpp_TypeInfo_var;
extern const uint32_t DelayFrame_Run_m876846149_MetadataUsageId;
extern "C"  Il2CppObject * DelayFrame_Run_m876846149_gshared (DelayFrame_t1619463347 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DelayFrame_Run_m876846149_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Il2CppObject * V_0 = NULL;
	{
		BooleanDisposable_t3065601722 * L_0 = (BooleanDisposable_t3065601722 *)il2cpp_codegen_object_new(BooleanDisposable_t3065601722_il2cpp_TypeInfo_var);
		BooleanDisposable__ctor_m2534881639(L_0, /*hidden argument*/NULL);
		__this->set_coroutineKey_4(L_0);
		DelayFrameObservable_1_t4117294156 * L_1 = (DelayFrameObservable_1_t4117294156 *)__this->get_parent_2();
		NullCheck(L_1);
		Il2CppObject* L_2 = (Il2CppObject*)L_1->get_source_1();
		NullCheck((Il2CppObject*)L_2);
		Il2CppObject * L_3 = InterfaceFuncInvoker1< Il2CppObject *, Il2CppObject* >::Invoke(0 /* System.IDisposable UniRx.IObservable`1<System.Object>::Subscribe(UniRx.IObserver`1<T>) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), (Il2CppObject*)L_2, (Il2CppObject*)__this);
		V_0 = (Il2CppObject *)L_3;
		BooleanDisposable_t3065601722 * L_4 = (BooleanDisposable_t3065601722 *)__this->get_coroutineKey_4();
		Il2CppObject * L_5 = V_0;
		Il2CppObject * L_6 = StableCompositeDisposable_Create_m3764788535(NULL /*static, unused*/, (Il2CppObject *)L_4, (Il2CppObject *)L_5, /*hidden argument*/NULL);
		return L_6;
	}
}
// System.Collections.IEnumerator UniRx.Operators.DelayFrameObservable`1/DelayFrame<System.Object>::OnNextDelay(T)
extern "C"  Il2CppObject * DelayFrame_OnNextDelay_m2080628808_gshared (DelayFrame_t1619463347 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	U3COnNextDelayU3Ec__Iterator26_t3660093125 * V_0 = NULL;
	{
		U3COnNextDelayU3Ec__Iterator26_t3660093125 * L_0 = (U3COnNextDelayU3Ec__Iterator26_t3660093125 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		((  void (*) (U3COnNextDelayU3Ec__Iterator26_t3660093125 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		V_0 = (U3COnNextDelayU3Ec__Iterator26_t3660093125 *)L_0;
		U3COnNextDelayU3Ec__Iterator26_t3660093125 * L_1 = V_0;
		Il2CppObject * L_2 = ___value0;
		NullCheck(L_1);
		L_1->set_value_1(L_2);
		U3COnNextDelayU3Ec__Iterator26_t3660093125 * L_3 = V_0;
		Il2CppObject * L_4 = ___value0;
		NullCheck(L_3);
		L_3->set_U3CU24U3Evalue_4(L_4);
		U3COnNextDelayU3Ec__Iterator26_t3660093125 * L_5 = V_0;
		NullCheck(L_5);
		L_5->set_U3CU3Ef__this_5(__this);
		U3COnNextDelayU3Ec__Iterator26_t3660093125 * L_6 = V_0;
		return L_6;
	}
}
// System.Collections.IEnumerator UniRx.Operators.DelayFrameObservable`1/DelayFrame<System.Object>::OnCompletedDelay()
extern "C"  Il2CppObject * DelayFrame_OnCompletedDelay_m158968724_gshared (DelayFrame_t1619463347 * __this, const MethodInfo* method)
{
	U3COnCompletedDelayU3Ec__Iterator27_t259619244 * V_0 = NULL;
	{
		U3COnCompletedDelayU3Ec__Iterator27_t259619244 * L_0 = (U3COnCompletedDelayU3Ec__Iterator27_t259619244 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		((  void (*) (U3COnCompletedDelayU3Ec__Iterator27_t259619244 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6)->method)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		V_0 = (U3COnCompletedDelayU3Ec__Iterator27_t259619244 *)L_0;
		U3COnCompletedDelayU3Ec__Iterator27_t259619244 * L_1 = V_0;
		NullCheck(L_1);
		L_1->set_U3CU3Ef__this_3(__this);
		U3COnCompletedDelayU3Ec__Iterator27_t259619244 * L_2 = V_0;
		return L_2;
	}
}
// System.Void UniRx.Operators.DelayFrameObservable`1/DelayFrame<System.Object>::OnNext(T)
extern Il2CppClass* MainThreadDispatcher_t4027217788_il2cpp_TypeInfo_var;
extern const uint32_t DelayFrame_OnNext_m3257379551_MetadataUsageId;
extern "C"  void DelayFrame_OnNext_m3257379551_gshared (DelayFrame_t1619463347 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DelayFrame_OnNext_m3257379551_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		BooleanDisposable_t3065601722 * L_0 = (BooleanDisposable_t3065601722 *)__this->get_coroutineKey_4();
		NullCheck((BooleanDisposable_t3065601722 *)L_0);
		bool L_1 = VirtFuncInvoker0< bool >::Invoke(5 /* System.Boolean UniRx.BooleanDisposable::get_IsDisposed() */, (BooleanDisposable_t3065601722 *)L_0);
		if (!L_1)
		{
			goto IL_0011;
		}
	}
	{
		return;
	}

IL_0011:
	{
		Il2CppObject * L_2 = ___value0;
		NullCheck((DelayFrame_t1619463347 *)__this);
		Il2CppObject * L_3 = ((  Il2CppObject * (*) (DelayFrame_t1619463347 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7)->method)((DelayFrame_t1619463347 *)__this, (Il2CppObject *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7));
		IL2CPP_RUNTIME_CLASS_INIT(MainThreadDispatcher_t4027217788_il2cpp_TypeInfo_var);
		MainThreadDispatcher_StartCoroutine_m3648496618(NULL /*static, unused*/, (Il2CppObject *)L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UniRx.Operators.DelayFrameObservable`1/DelayFrame<System.Object>::OnError(System.Exception)
extern "C"  void DelayFrame_OnError_m2429560814_gshared (DelayFrame_t1619463347 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method)
{
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		BooleanDisposable_t3065601722 * L_0 = (BooleanDisposable_t3065601722 *)__this->get_coroutineKey_4();
		NullCheck((BooleanDisposable_t3065601722 *)L_0);
		bool L_1 = VirtFuncInvoker0< bool >::Invoke(5 /* System.Boolean UniRx.BooleanDisposable::get_IsDisposed() */, (BooleanDisposable_t3065601722 *)L_0);
		if (!L_1)
		{
			goto IL_0011;
		}
	}
	{
		return;
	}

IL_0011:
	{
		BooleanDisposable_t3065601722 * L_2 = (BooleanDisposable_t3065601722 *)__this->get_coroutineKey_4();
		NullCheck((BooleanDisposable_t3065601722 *)L_2);
		VirtActionInvoker0::Invoke(4 /* System.Void UniRx.BooleanDisposable::Dispose() */, (BooleanDisposable_t3065601722 *)L_2);
	}

IL_001c:
	try
	{ // begin try (depth: 1)
		Il2CppObject* L_3 = (Il2CppObject*)((OperatorObserverBase_2_t1187768149 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		Exception_t1967233988 * L_4 = ___error0;
		NullCheck((Il2CppObject*)L_3);
		InterfaceActionInvoker1< Exception_t1967233988 * >::Invoke(1 /* System.Void UniRx.IObserver`1<System.Object>::OnError(System.Exception) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8), (Il2CppObject*)L_3, (Exception_t1967233988 *)L_4);
		IL2CPP_LEAVE(0x36, FINALLY_002f);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_002f;
	}

FINALLY_002f:
	{ // begin finally (depth: 1)
		NullCheck((OperatorObserverBase_2_t1187768149 *)__this);
		VirtActionInvoker0::Invoke(4 /* System.Void UniRx.Operators.OperatorObserverBase`2<System.Object,System.Object>::Dispose() */, (OperatorObserverBase_2_t1187768149 *)__this);
		IL2CPP_END_FINALLY(47)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(47)
	{
		IL2CPP_JUMP_TBL(0x36, IL_0036)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0036:
	{
		return;
	}
}
// System.Void UniRx.Operators.DelayFrameObservable`1/DelayFrame<System.Object>::OnCompleted()
extern Il2CppClass* MainThreadDispatcher_t4027217788_il2cpp_TypeInfo_var;
extern const uint32_t DelayFrame_OnCompleted_m328781889_MetadataUsageId;
extern "C"  void DelayFrame_OnCompleted_m328781889_gshared (DelayFrame_t1619463347 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DelayFrame_OnCompleted_m328781889_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		BooleanDisposable_t3065601722 * L_0 = (BooleanDisposable_t3065601722 *)__this->get_coroutineKey_4();
		NullCheck((BooleanDisposable_t3065601722 *)L_0);
		bool L_1 = VirtFuncInvoker0< bool >::Invoke(5 /* System.Boolean UniRx.BooleanDisposable::get_IsDisposed() */, (BooleanDisposable_t3065601722 *)L_0);
		if (!L_1)
		{
			goto IL_0011;
		}
	}
	{
		return;
	}

IL_0011:
	{
		NullCheck((DelayFrame_t1619463347 *)__this);
		Il2CppObject * L_2 = ((  Il2CppObject * (*) (DelayFrame_t1619463347 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10)->method)((DelayFrame_t1619463347 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10));
		IL2CPP_RUNTIME_CLASS_INIT(MainThreadDispatcher_t4027217788_il2cpp_TypeInfo_var);
		MainThreadDispatcher_StartCoroutine_m3648496618(NULL /*static, unused*/, (Il2CppObject *)L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UniRx.Operators.DelayFrameObservable`1<System.Object>::.ctor(UniRx.IObservable`1<T>,System.Int32,UniRx.FrameCountType)
extern "C"  void DelayFrameObservable_1__ctor_m846536778_gshared (DelayFrameObservable_1_t4117294156 * __this, Il2CppObject* ___source0, int32_t ___frameCount1, int32_t ___frameCountType2, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = ___source0;
		bool L_1 = ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)(NULL /*static, unused*/, (Il2CppObject*)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		NullCheck((OperatorObservableBase_1_t4196218687 *)__this);
		((  void (*) (OperatorObservableBase_1_t4196218687 *, bool, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)((OperatorObservableBase_1_t4196218687 *)__this, (bool)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		Il2CppObject* L_2 = ___source0;
		__this->set_source_1(L_2);
		int32_t L_3 = ___frameCount1;
		__this->set_frameCount_2(L_3);
		int32_t L_4 = ___frameCountType2;
		__this->set_frameCountType_3(L_4);
		return;
	}
}
// System.IDisposable UniRx.Operators.DelayFrameObservable`1<System.Object>::SubscribeCore(UniRx.IObserver`1<T>,System.IDisposable)
extern "C"  Il2CppObject * DelayFrameObservable_1_SubscribeCore_m120249752_gshared (DelayFrameObservable_1_t4117294156 * __this, Il2CppObject* ___observer0, Il2CppObject * ___cancel1, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = ___observer0;
		Il2CppObject * L_1 = ___cancel1;
		DelayFrame_t1619463347 * L_2 = (DelayFrame_t1619463347 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		((  void (*) (DelayFrame_t1619463347 *, DelayFrameObservable_1_t4117294156 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)(L_2, (DelayFrameObservable_1_t4117294156 *)__this, (Il2CppObject*)L_0, (Il2CppObject *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		NullCheck((DelayFrame_t1619463347 *)L_2);
		Il2CppObject * L_3 = ((  Il2CppObject * (*) (DelayFrame_t1619463347 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->method)((DelayFrame_t1619463347 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		return L_3;
	}
}
// System.Void UniRx.Operators.DelayFrameSubscriptionObservable`1/<SubscribeCore>c__AnonStorey93<System.Object>::.ctor()
extern "C"  void U3CSubscribeCoreU3Ec__AnonStorey93__ctor_m345941600_gshared (U3CSubscribeCoreU3Ec__AnonStorey93_t2770048777 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UniRx.Operators.DelayFrameSubscriptionObservable`1/<SubscribeCore>c__AnonStorey93<System.Object>::<>m__BF(System.Int64)
extern "C"  void U3CSubscribeCoreU3Ec__AnonStorey93_U3CU3Em__BF_m2629830079_gshared (U3CSubscribeCoreU3Ec__AnonStorey93_t2770048777 * __this, int64_t ____0, const MethodInfo* method)
{
	{
		MultipleAssignmentDisposable_t3090232719 * L_0 = (MultipleAssignmentDisposable_t3090232719 *)__this->get_d_1();
		DelayFrameSubscriptionObservable_1_t2645984001 * L_1 = (DelayFrameSubscriptionObservable_1_t2645984001 *)__this->get_U3CU3Ef__this_2();
		NullCheck(L_1);
		Il2CppObject* L_2 = (Il2CppObject*)L_1->get_source_1();
		Il2CppObject* L_3 = (Il2CppObject*)__this->get_observer_0();
		NullCheck((Il2CppObject*)L_2);
		Il2CppObject * L_4 = InterfaceFuncInvoker1< Il2CppObject *, Il2CppObject* >::Invoke(0 /* System.IDisposable UniRx.IObservable`1<System.Object>::Subscribe(UniRx.IObserver`1<T>) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0), (Il2CppObject*)L_2, (Il2CppObject*)L_3);
		NullCheck((MultipleAssignmentDisposable_t3090232719 *)L_0);
		MultipleAssignmentDisposable_set_Disposable_m2693870941((MultipleAssignmentDisposable_t3090232719 *)L_0, (Il2CppObject *)L_4, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UniRx.Operators.DelayFrameSubscriptionObservable`1<System.Object>::.ctor(UniRx.IObservable`1<T>,System.Int32,UniRx.FrameCountType)
extern "C"  void DelayFrameSubscriptionObservable_1__ctor_m3625992327_gshared (DelayFrameSubscriptionObservable_1_t2645984001 * __this, Il2CppObject* ___source0, int32_t ___frameCount1, int32_t ___frameCountType2, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = ___source0;
		bool L_1 = ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)(NULL /*static, unused*/, (Il2CppObject*)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		NullCheck((OperatorObservableBase_1_t4196218687 *)__this);
		((  void (*) (OperatorObservableBase_1_t4196218687 *, bool, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)((OperatorObservableBase_1_t4196218687 *)__this, (bool)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		Il2CppObject* L_2 = ___source0;
		__this->set_source_1(L_2);
		int32_t L_3 = ___frameCount1;
		__this->set_frameCount_2(L_3);
		int32_t L_4 = ___frameCountType2;
		__this->set_frameCountType_3(L_4);
		return;
	}
}
// System.IDisposable UniRx.Operators.DelayFrameSubscriptionObservable`1<System.Object>::SubscribeCore(UniRx.IObserver`1<T>,System.IDisposable)
extern Il2CppClass* MultipleAssignmentDisposable_t3090232719_il2cpp_TypeInfo_var;
extern Il2CppClass* Observable_t258071701_il2cpp_TypeInfo_var;
extern Il2CppClass* Action_1_t2995867587_il2cpp_TypeInfo_var;
extern const MethodInfo* Action_1__ctor_m1589437765_MethodInfo_var;
extern const MethodInfo* ObservableExtensions_Subscribe_TisInt64_t2847414882_m1751770470_MethodInfo_var;
extern const uint32_t DelayFrameSubscriptionObservable_1_SubscribeCore_m442975547_MetadataUsageId;
extern "C"  Il2CppObject * DelayFrameSubscriptionObservable_1_SubscribeCore_m442975547_gshared (DelayFrameSubscriptionObservable_1_t2645984001 * __this, Il2CppObject* ___observer0, Il2CppObject * ___cancel1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DelayFrameSubscriptionObservable_1_SubscribeCore_m442975547_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	U3CSubscribeCoreU3Ec__AnonStorey93_t2770048777 * V_0 = NULL;
	{
		U3CSubscribeCoreU3Ec__AnonStorey93_t2770048777 * L_0 = (U3CSubscribeCoreU3Ec__AnonStorey93_t2770048777 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		((  void (*) (U3CSubscribeCoreU3Ec__AnonStorey93_t2770048777 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		V_0 = (U3CSubscribeCoreU3Ec__AnonStorey93_t2770048777 *)L_0;
		U3CSubscribeCoreU3Ec__AnonStorey93_t2770048777 * L_1 = V_0;
		Il2CppObject* L_2 = ___observer0;
		NullCheck(L_1);
		L_1->set_observer_0(L_2);
		U3CSubscribeCoreU3Ec__AnonStorey93_t2770048777 * L_3 = V_0;
		NullCheck(L_3);
		L_3->set_U3CU3Ef__this_2(__this);
		U3CSubscribeCoreU3Ec__AnonStorey93_t2770048777 * L_4 = V_0;
		MultipleAssignmentDisposable_t3090232719 * L_5 = (MultipleAssignmentDisposable_t3090232719 *)il2cpp_codegen_object_new(MultipleAssignmentDisposable_t3090232719_il2cpp_TypeInfo_var);
		MultipleAssignmentDisposable__ctor_m4117172186(L_5, /*hidden argument*/NULL);
		NullCheck(L_4);
		L_4->set_d_1(L_5);
		U3CSubscribeCoreU3Ec__AnonStorey93_t2770048777 * L_6 = V_0;
		NullCheck(L_6);
		MultipleAssignmentDisposable_t3090232719 * L_7 = (MultipleAssignmentDisposable_t3090232719 *)L_6->get_d_1();
		int32_t L_8 = (int32_t)__this->get_frameCount_2();
		int32_t L_9 = (int32_t)__this->get_frameCountType_3();
		IL2CPP_RUNTIME_CLASS_INIT(Observable_t258071701_il2cpp_TypeInfo_var);
		Il2CppObject* L_10 = Observable_TimerFrame_m594366159(NULL /*static, unused*/, (int32_t)L_8, (int32_t)L_9, /*hidden argument*/NULL);
		U3CSubscribeCoreU3Ec__AnonStorey93_t2770048777 * L_11 = V_0;
		IntPtr_t L_12;
		L_12.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		Action_1_t2995867587 * L_13 = (Action_1_t2995867587 *)il2cpp_codegen_object_new(Action_1_t2995867587_il2cpp_TypeInfo_var);
		Action_1__ctor_m1589437765(L_13, (Il2CppObject *)L_11, (IntPtr_t)L_12, /*hidden argument*/Action_1__ctor_m1589437765_MethodInfo_var);
		Il2CppObject * L_14 = ObservableExtensions_Subscribe_TisInt64_t2847414882_m1751770470(NULL /*static, unused*/, (Il2CppObject*)L_10, (Action_1_t2995867587 *)L_13, /*hidden argument*/ObservableExtensions_Subscribe_TisInt64_t2847414882_m1751770470_MethodInfo_var);
		NullCheck((MultipleAssignmentDisposable_t3090232719 *)L_7);
		MultipleAssignmentDisposable_set_Disposable_m2693870941((MultipleAssignmentDisposable_t3090232719 *)L_7, (Il2CppObject *)L_14, /*hidden argument*/NULL);
		U3CSubscribeCoreU3Ec__AnonStorey93_t2770048777 * L_15 = V_0;
		NullCheck(L_15);
		MultipleAssignmentDisposable_t3090232719 * L_16 = (MultipleAssignmentDisposable_t3090232719 *)L_15->get_d_1();
		return L_16;
	}
}
// System.Void UniRx.Operators.DelayObservable`1/Delay<System.Object>::.ctor(UniRx.Operators.DelayObservable`1<T>,UniRx.IObserver`1<T>,System.IDisposable)
extern Il2CppClass* Il2CppObject_il2cpp_TypeInfo_var;
extern const uint32_t Delay__ctor_m1727646917_MetadataUsageId;
extern "C"  void Delay__ctor_m1727646917_gshared (Delay_t3380693988 * __this, DelayObservable_1_t2635131389 * ___parent0, Il2CppObject* ___observer1, Il2CppObject * ___cancel2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Delay__ctor_m1727646917_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject * L_0 = (Il2CppObject *)il2cpp_codegen_object_new(Il2CppObject_il2cpp_TypeInfo_var);
		Object__ctor_m1772956182(L_0, /*hidden argument*/NULL);
		__this->set_gate_3(L_0);
		Il2CppObject* L_1 = ___observer1;
		Il2CppObject * L_2 = ___cancel2;
		NullCheck((OperatorObserverBase_2_t1187768149 *)__this);
		((  void (*) (OperatorObserverBase_2_t1187768149 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((OperatorObserverBase_2_t1187768149 *)__this, (Il2CppObject*)L_1, (Il2CppObject *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		DelayObservable_1_t2635131389 * L_3 = ___parent0;
		__this->set_parent_2(L_3);
		return;
	}
}
// System.IDisposable UniRx.Operators.DelayObservable`1/Delay<System.Object>::Run()
extern Il2CppClass* SerialDisposable_t2547852742_il2cpp_TypeInfo_var;
extern Il2CppClass* DateTimeOffset_t3712260035_il2cpp_TypeInfo_var;
extern Il2CppClass* Scheduler_t103934925_il2cpp_TypeInfo_var;
extern Il2CppClass* SingleAssignmentDisposable_t2336378823_il2cpp_TypeInfo_var;
extern const uint32_t Delay_Run_m2426087989_MetadataUsageId;
extern "C"  Il2CppObject * Delay_Run_m2426087989_gshared (Delay_t3380693988 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Delay_Run_m2426087989_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	SingleAssignmentDisposable_t2336378823 * V_0 = NULL;
	DateTimeOffset_t3712260035  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		SerialDisposable_t2547852742 * L_0 = (SerialDisposable_t2547852742 *)il2cpp_codegen_object_new(SerialDisposable_t2547852742_il2cpp_TypeInfo_var);
		SerialDisposable__ctor_m2977852291(L_0, /*hidden argument*/NULL);
		__this->set_cancelable_14(L_0);
		__this->set_active_6((bool)0);
		__this->set_running_5((bool)0);
		Queue_1_t4227271813 * L_1 = (Queue_1_t4227271813 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		((  void (*) (Queue_1_t4227271813 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)(L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		__this->set_queue_8(L_1);
		__this->set_onCompleted_9((bool)0);
		Initobj (DateTimeOffset_t3712260035_il2cpp_TypeInfo_var, (&V_1));
		DateTimeOffset_t3712260035  L_2 = V_1;
		__this->set_completeAt_10(L_2);
		__this->set_hasFailed_4((bool)0);
		__this->set_exception_7((Exception_t1967233988 *)NULL);
		__this->set_ready_13((bool)1);
		DelayObservable_1_t2635131389 * L_3 = (DelayObservable_1_t2635131389 *)__this->get_parent_2();
		NullCheck(L_3);
		TimeSpan_t763862892  L_4 = (TimeSpan_t763862892 )L_3->get_dueTime_2();
		IL2CPP_RUNTIME_CLASS_INIT(Scheduler_t103934925_il2cpp_TypeInfo_var);
		TimeSpan_t763862892  L_5 = Scheduler_Normalize_m3379699520(NULL /*static, unused*/, (TimeSpan_t763862892 )L_4, /*hidden argument*/NULL);
		__this->set_delay_12(L_5);
		SingleAssignmentDisposable_t2336378823 * L_6 = (SingleAssignmentDisposable_t2336378823 *)il2cpp_codegen_object_new(SingleAssignmentDisposable_t2336378823_il2cpp_TypeInfo_var);
		SingleAssignmentDisposable__ctor_m917449122(L_6, /*hidden argument*/NULL);
		V_0 = (SingleAssignmentDisposable_t2336378823 *)L_6;
		SingleAssignmentDisposable_t2336378823 * L_7 = V_0;
		__this->set_sourceSubscription_11(L_7);
		SingleAssignmentDisposable_t2336378823 * L_8 = V_0;
		DelayObservable_1_t2635131389 * L_9 = (DelayObservable_1_t2635131389 *)__this->get_parent_2();
		NullCheck(L_9);
		Il2CppObject* L_10 = (Il2CppObject*)L_9->get_source_1();
		NullCheck((Il2CppObject*)L_10);
		Il2CppObject * L_11 = InterfaceFuncInvoker1< Il2CppObject *, Il2CppObject* >::Invoke(0 /* System.IDisposable UniRx.IObservable`1<System.Object>::Subscribe(UniRx.IObserver`1<T>) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4), (Il2CppObject*)L_10, (Il2CppObject*)__this);
		NullCheck((SingleAssignmentDisposable_t2336378823 *)L_8);
		SingleAssignmentDisposable_set_Disposable_m1599028885((SingleAssignmentDisposable_t2336378823 *)L_8, (Il2CppObject *)L_11, /*hidden argument*/NULL);
		Il2CppObject * L_12 = (Il2CppObject *)__this->get_sourceSubscription_11();
		SerialDisposable_t2547852742 * L_13 = (SerialDisposable_t2547852742 *)__this->get_cancelable_14();
		Il2CppObject * L_14 = StableCompositeDisposable_Create_m3764788535(NULL /*static, unused*/, (Il2CppObject *)L_12, (Il2CppObject *)L_13, /*hidden argument*/NULL);
		return L_14;
	}
}
// System.Void UniRx.Operators.DelayObservable`1/Delay<System.Object>::OnNext(T)
extern Il2CppClass* IScheduler_t2938318244_il2cpp_TypeInfo_var;
extern Il2CppClass* Action_1_t1060768302_il2cpp_TypeInfo_var;
extern Il2CppClass* Scheduler_t103934925_il2cpp_TypeInfo_var;
extern const MethodInfo* Action_1__ctor_m3957693621_MethodInfo_var;
extern const uint32_t Delay_OnNext_m3212416655_MetadataUsageId;
extern "C"  void Delay_OnNext_m3212416655_gshared (Delay_t3380693988 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Delay_OnNext_m3212416655_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	DateTimeOffset_t3712260035  V_0;
	memset(&V_0, 0, sizeof(V_0));
	bool V_1 = false;
	Il2CppObject * V_2 = NULL;
	DateTimeOffset_t3712260035  V_3;
	memset(&V_3, 0, sizeof(V_3));
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	int32_t G_B4_0 = 0;
	{
		DelayObservable_1_t2635131389 * L_0 = (DelayObservable_1_t2635131389 *)__this->get_parent_2();
		NullCheck(L_0);
		Il2CppObject * L_1 = (Il2CppObject *)L_0->get_scheduler_3();
		NullCheck((Il2CppObject *)L_1);
		DateTimeOffset_t3712260035  L_2 = InterfaceFuncInvoker0< DateTimeOffset_t3712260035  >::Invoke(0 /* System.DateTimeOffset UniRx.IScheduler::get_Now() */, IScheduler_t2938318244_il2cpp_TypeInfo_var, (Il2CppObject *)L_1);
		V_3 = (DateTimeOffset_t3712260035 )L_2;
		TimeSpan_t763862892  L_3 = (TimeSpan_t763862892 )__this->get_delay_12();
		DateTimeOffset_t3712260035  L_4 = DateTimeOffset_Add_m1161530352((DateTimeOffset_t3712260035 *)(&V_3), (TimeSpan_t763862892 )L_3, /*hidden argument*/NULL);
		V_0 = (DateTimeOffset_t3712260035 )L_4;
		V_1 = (bool)0;
		Il2CppObject * L_5 = (Il2CppObject *)__this->get_gate_3();
		V_2 = (Il2CppObject *)L_5;
		Il2CppObject * L_6 = V_2;
		Monitor_Enter_m476686225(NULL /*static, unused*/, (Il2CppObject *)L_6, /*hidden argument*/NULL);
	}

IL_002e:
	try
	{ // begin try (depth: 1)
		{
			Queue_1_t4227271813 * L_7 = (Queue_1_t4227271813 *)__this->get_queue_8();
			Il2CppObject * L_8 = ___value0;
			DateTimeOffset_t3712260035  L_9 = V_0;
			Timestamped_1_t2519184273  L_10;
			memset(&L_10, 0, sizeof(L_10));
			((  void (*) (Timestamped_1_t2519184273 *, Il2CppObject *, DateTimeOffset_t3712260035 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6)->method)(&L_10, (Il2CppObject *)L_8, (DateTimeOffset_t3712260035 )L_9, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
			NullCheck((Queue_1_t4227271813 *)L_7);
			((  void (*) (Queue_1_t4227271813 *, Timestamped_1_t2519184273 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7)->method)((Queue_1_t4227271813 *)L_7, (Timestamped_1_t2519184273 )L_10, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7));
			bool L_11 = (bool)__this->get_ready_13();
			if (!L_11)
			{
				goto IL_0056;
			}
		}

IL_004b:
		{
			bool L_12 = (bool)__this->get_active_6();
			G_B4_0 = ((((int32_t)L_12) == ((int32_t)0))? 1 : 0);
			goto IL_0057;
		}

IL_0056:
		{
			G_B4_0 = 0;
		}

IL_0057:
		{
			V_1 = (bool)G_B4_0;
			__this->set_active_6((bool)1);
			IL2CPP_LEAVE(0x6B, FINALLY_0064);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0064;
	}

FINALLY_0064:
	{ // begin finally (depth: 1)
		Il2CppObject * L_13 = V_2;
		Monitor_Exit_m2088237919(NULL /*static, unused*/, (Il2CppObject *)L_13, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(100)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(100)
	{
		IL2CPP_JUMP_TBL(0x6B, IL_006b)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_006b:
	{
		bool L_14 = V_1;
		if (!L_14)
		{
			goto IL_009e;
		}
	}
	{
		SerialDisposable_t2547852742 * L_15 = (SerialDisposable_t2547852742 *)__this->get_cancelable_14();
		DelayObservable_1_t2635131389 * L_16 = (DelayObservable_1_t2635131389 *)__this->get_parent_2();
		NullCheck(L_16);
		Il2CppObject * L_17 = (Il2CppObject *)L_16->get_scheduler_3();
		TimeSpan_t763862892  L_18 = (TimeSpan_t763862892 )__this->get_delay_12();
		IntPtr_t L_19;
		L_19.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8));
		Action_1_t1060768302 * L_20 = (Action_1_t1060768302 *)il2cpp_codegen_object_new(Action_1_t1060768302_il2cpp_TypeInfo_var);
		Action_1__ctor_m3957693621(L_20, (Il2CppObject *)__this, (IntPtr_t)L_19, /*hidden argument*/Action_1__ctor_m3957693621_MethodInfo_var);
		IL2CPP_RUNTIME_CLASS_INIT(Scheduler_t103934925_il2cpp_TypeInfo_var);
		Il2CppObject * L_21 = Scheduler_Schedule_m279164218(NULL /*static, unused*/, (Il2CppObject *)L_17, (TimeSpan_t763862892 )L_18, (Action_1_t1060768302 *)L_20, /*hidden argument*/NULL);
		NullCheck((SerialDisposable_t2547852742 *)L_15);
		SerialDisposable_set_Disposable_m1438046228((SerialDisposable_t2547852742 *)L_15, (Il2CppObject *)L_21, /*hidden argument*/NULL);
	}

IL_009e:
	{
		return;
	}
}
// System.Void UniRx.Operators.DelayObservable`1/Delay<System.Object>::OnError(System.Exception)
extern Il2CppClass* IDisposable_t1628921374_il2cpp_TypeInfo_var;
extern const uint32_t Delay_OnError_m800371614_MetadataUsageId;
extern "C"  void Delay_OnError_m800371614_gshared (Delay_t3380693988 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Delay_OnError_m800371614_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	bool V_0 = false;
	Il2CppObject * V_1 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_sourceSubscription_11();
		NullCheck((Il2CppObject *)L_0);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, (Il2CppObject *)L_0);
		V_0 = (bool)0;
		Il2CppObject * L_1 = (Il2CppObject *)__this->get_gate_3();
		V_1 = (Il2CppObject *)L_1;
		Il2CppObject * L_2 = V_1;
		Monitor_Enter_m476686225(NULL /*static, unused*/, (Il2CppObject *)L_2, /*hidden argument*/NULL);
	}

IL_001a:
	try
	{ // begin try (depth: 1)
		Queue_1_t4227271813 * L_3 = (Queue_1_t4227271813 *)__this->get_queue_8();
		NullCheck((Queue_1_t4227271813 *)L_3);
		((  void (*) (Queue_1_t4227271813 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9)->method)((Queue_1_t4227271813 *)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9));
		Exception_t1967233988 * L_4 = ___error0;
		__this->set_exception_7(L_4);
		__this->set_hasFailed_4((bool)1);
		bool L_5 = (bool)__this->get_running_5();
		V_0 = (bool)((((int32_t)L_5) == ((int32_t)0))? 1 : 0);
		IL2CPP_LEAVE(0x49, FINALLY_0042);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0042;
	}

FINALLY_0042:
	{ // begin finally (depth: 1)
		Il2CppObject * L_6 = V_1;
		Monitor_Exit_m2088237919(NULL /*static, unused*/, (Il2CppObject *)L_6, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(66)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(66)
	{
		IL2CPP_JUMP_TBL(0x49, IL_0049)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0049:
	{
		bool L_7 = V_0;
		if (!L_7)
		{
			goto IL_0069;
		}
	}

IL_004f:
	try
	{ // begin try (depth: 1)
		Il2CppObject* L_8 = (Il2CppObject*)((OperatorObserverBase_2_t1187768149 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		Exception_t1967233988 * L_9 = ___error0;
		NullCheck((Il2CppObject*)L_8);
		InterfaceActionInvoker1< Exception_t1967233988 * >::Invoke(1 /* System.Void UniRx.IObserver`1<System.Object>::OnError(System.Exception) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10), (Il2CppObject*)L_8, (Exception_t1967233988 *)L_9);
		IL2CPP_LEAVE(0x69, FINALLY_0062);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0062;
	}

FINALLY_0062:
	{ // begin finally (depth: 1)
		NullCheck((OperatorObserverBase_2_t1187768149 *)__this);
		VirtActionInvoker0::Invoke(4 /* System.Void UniRx.Operators.OperatorObserverBase`2<System.Object,System.Object>::Dispose() */, (OperatorObserverBase_2_t1187768149 *)__this);
		IL2CPP_END_FINALLY(98)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(98)
	{
		IL2CPP_JUMP_TBL(0x69, IL_0069)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0069:
	{
		return;
	}
}
// System.Void UniRx.Operators.DelayObservable`1/Delay<System.Object>::OnCompleted()
extern Il2CppClass* IDisposable_t1628921374_il2cpp_TypeInfo_var;
extern Il2CppClass* IScheduler_t2938318244_il2cpp_TypeInfo_var;
extern Il2CppClass* Action_1_t1060768302_il2cpp_TypeInfo_var;
extern Il2CppClass* Scheduler_t103934925_il2cpp_TypeInfo_var;
extern const MethodInfo* Action_1__ctor_m3957693621_MethodInfo_var;
extern const uint32_t Delay_OnCompleted_m4188890097_MetadataUsageId;
extern "C"  void Delay_OnCompleted_m4188890097_gshared (Delay_t3380693988 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Delay_OnCompleted_m4188890097_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	DateTimeOffset_t3712260035  V_0;
	memset(&V_0, 0, sizeof(V_0));
	bool V_1 = false;
	Il2CppObject * V_2 = NULL;
	DateTimeOffset_t3712260035  V_3;
	memset(&V_3, 0, sizeof(V_3));
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	int32_t G_B4_0 = 0;
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_sourceSubscription_11();
		NullCheck((Il2CppObject *)L_0);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, (Il2CppObject *)L_0);
		DelayObservable_1_t2635131389 * L_1 = (DelayObservable_1_t2635131389 *)__this->get_parent_2();
		NullCheck(L_1);
		Il2CppObject * L_2 = (Il2CppObject *)L_1->get_scheduler_3();
		NullCheck((Il2CppObject *)L_2);
		DateTimeOffset_t3712260035  L_3 = InterfaceFuncInvoker0< DateTimeOffset_t3712260035  >::Invoke(0 /* System.DateTimeOffset UniRx.IScheduler::get_Now() */, IScheduler_t2938318244_il2cpp_TypeInfo_var, (Il2CppObject *)L_2);
		V_3 = (DateTimeOffset_t3712260035 )L_3;
		TimeSpan_t763862892  L_4 = (TimeSpan_t763862892 )__this->get_delay_12();
		DateTimeOffset_t3712260035  L_5 = DateTimeOffset_Add_m1161530352((DateTimeOffset_t3712260035 *)(&V_3), (TimeSpan_t763862892 )L_4, /*hidden argument*/NULL);
		V_0 = (DateTimeOffset_t3712260035 )L_5;
		V_1 = (bool)0;
		Il2CppObject * L_6 = (Il2CppObject *)__this->get_gate_3();
		V_2 = (Il2CppObject *)L_6;
		Il2CppObject * L_7 = V_2;
		Monitor_Enter_m476686225(NULL /*static, unused*/, (Il2CppObject *)L_7, /*hidden argument*/NULL);
	}

IL_0039:
	try
	{ // begin try (depth: 1)
		{
			DateTimeOffset_t3712260035  L_8 = V_0;
			__this->set_completeAt_10(L_8);
			__this->set_onCompleted_9((bool)1);
			bool L_9 = (bool)__this->get_ready_13();
			if (!L_9)
			{
				goto IL_005d;
			}
		}

IL_0052:
		{
			bool L_10 = (bool)__this->get_active_6();
			G_B4_0 = ((((int32_t)L_10) == ((int32_t)0))? 1 : 0);
			goto IL_005e;
		}

IL_005d:
		{
			G_B4_0 = 0;
		}

IL_005e:
		{
			V_1 = (bool)G_B4_0;
			__this->set_active_6((bool)1);
			IL2CPP_LEAVE(0x72, FINALLY_006b);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_006b;
	}

FINALLY_006b:
	{ // begin finally (depth: 1)
		Il2CppObject * L_11 = V_2;
		Monitor_Exit_m2088237919(NULL /*static, unused*/, (Il2CppObject *)L_11, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(107)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(107)
	{
		IL2CPP_JUMP_TBL(0x72, IL_0072)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0072:
	{
		bool L_12 = V_1;
		if (!L_12)
		{
			goto IL_00a5;
		}
	}
	{
		SerialDisposable_t2547852742 * L_13 = (SerialDisposable_t2547852742 *)__this->get_cancelable_14();
		DelayObservable_1_t2635131389 * L_14 = (DelayObservable_1_t2635131389 *)__this->get_parent_2();
		NullCheck(L_14);
		Il2CppObject * L_15 = (Il2CppObject *)L_14->get_scheduler_3();
		TimeSpan_t763862892  L_16 = (TimeSpan_t763862892 )__this->get_delay_12();
		IntPtr_t L_17;
		L_17.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8));
		Action_1_t1060768302 * L_18 = (Action_1_t1060768302 *)il2cpp_codegen_object_new(Action_1_t1060768302_il2cpp_TypeInfo_var);
		Action_1__ctor_m3957693621(L_18, (Il2CppObject *)__this, (IntPtr_t)L_17, /*hidden argument*/Action_1__ctor_m3957693621_MethodInfo_var);
		IL2CPP_RUNTIME_CLASS_INIT(Scheduler_t103934925_il2cpp_TypeInfo_var);
		Il2CppObject * L_19 = Scheduler_Schedule_m279164218(NULL /*static, unused*/, (Il2CppObject *)L_15, (TimeSpan_t763862892 )L_16, (Action_1_t1060768302 *)L_18, /*hidden argument*/NULL);
		NullCheck((SerialDisposable_t2547852742 *)L_13);
		SerialDisposable_set_Disposable_m1438046228((SerialDisposable_t2547852742 *)L_13, (Il2CppObject *)L_19, /*hidden argument*/NULL);
	}

IL_00a5:
	{
		return;
	}
}
// System.Void UniRx.Operators.DelayObservable`1/Delay<System.Object>::DrainQueue(System.Action`1<System.TimeSpan>)
extern Il2CppClass* Il2CppObject_il2cpp_TypeInfo_var;
extern Il2CppClass* TimeSpan_t763862892_il2cpp_TypeInfo_var;
extern Il2CppClass* IScheduler_t2938318244_il2cpp_TypeInfo_var;
extern Il2CppClass* Scheduler_t103934925_il2cpp_TypeInfo_var;
extern const MethodInfo* Action_1_Invoke_m613956924_MethodInfo_var;
extern const uint32_t Delay_DrainQueue_m1278116664_MetadataUsageId;
extern "C"  void Delay_DrainQueue_m1278116664_gshared (Delay_t3380693988 * __this, Action_1_t912315597 * ___recurse0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Delay_DrainQueue_m1278116664_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Il2CppObject * V_0 = NULL;
	bool V_1 = false;
	bool V_2 = false;
	Exception_t1967233988 * V_3 = NULL;
	bool V_4 = false;
	Il2CppObject * V_5 = NULL;
	bool V_6 = false;
	bool V_7 = false;
	TimeSpan_t763862892  V_8;
	memset(&V_8, 0, sizeof(V_8));
	Il2CppObject * V_9 = NULL;
	DateTimeOffset_t3712260035  V_10;
	memset(&V_10, 0, sizeof(V_10));
	Il2CppObject * V_11 = NULL;
	TimeSpan_t763862892  V_12;
	memset(&V_12, 0, sizeof(V_12));
	Timestamped_1_t2519184273  V_13;
	memset(&V_13, 0, sizeof(V_13));
	Timestamped_1_t2519184273  V_14;
	memset(&V_14, 0, sizeof(V_14));
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_gate_3();
		V_0 = (Il2CppObject *)L_0;
		Il2CppObject * L_1 = V_0;
		Monitor_Enter_m476686225(NULL /*static, unused*/, (Il2CppObject *)L_1, /*hidden argument*/NULL);
	}

IL_000d:
	try
	{ // begin try (depth: 1)
		{
			bool L_2 = (bool)__this->get_hasFailed_4();
			if (!L_2)
			{
				goto IL_001d;
			}
		}

IL_0018:
		{
			IL2CPP_LEAVE(0x218, FINALLY_0029);
		}

IL_001d:
		{
			__this->set_running_5((bool)1);
			IL2CPP_LEAVE(0x30, FINALLY_0029);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0029;
	}

FINALLY_0029:
	{ // begin finally (depth: 1)
		Il2CppObject * L_3 = V_0;
		Monitor_Exit_m2088237919(NULL /*static, unused*/, (Il2CppObject *)L_3, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(41)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(41)
	{
		IL2CPP_JUMP_TBL(0x218, IL_0218)
		IL2CPP_JUMP_TBL(0x30, IL_0030)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0030:
	{
		V_1 = (bool)0;
	}

IL_0032:
	{
		V_2 = (bool)0;
		V_3 = (Exception_t1967233988 *)NULL;
		V_4 = (bool)0;
		Initobj (Il2CppObject_il2cpp_TypeInfo_var, (&V_11));
		Il2CppObject * L_4 = V_11;
		V_5 = (Il2CppObject *)L_4;
		V_6 = (bool)0;
		V_7 = (bool)0;
		Initobj (TimeSpan_t763862892_il2cpp_TypeInfo_var, (&V_12));
		TimeSpan_t763862892  L_5 = V_12;
		V_8 = (TimeSpan_t763862892 )L_5;
		Il2CppObject * L_6 = (Il2CppObject *)__this->get_gate_3();
		V_9 = (Il2CppObject *)L_6;
		Il2CppObject * L_7 = V_9;
		Monitor_Enter_m476686225(NULL /*static, unused*/, (Il2CppObject *)L_7, /*hidden argument*/NULL);
	}

IL_0066:
	try
	{ // begin try (depth: 1)
		{
			bool L_8 = V_2;
			if (!L_8)
			{
				goto IL_0081;
			}
		}

IL_006c:
		{
			Exception_t1967233988 * L_9 = (Exception_t1967233988 *)__this->get_exception_7();
			V_3 = (Exception_t1967233988 *)L_9;
			V_2 = (bool)1;
			__this->set_running_5((bool)0);
			goto IL_018f;
		}

IL_0081:
		{
			Queue_1_t4227271813 * L_10 = (Queue_1_t4227271813 *)__this->get_queue_8();
			NullCheck((Queue_1_t4227271813 *)L_10);
			int32_t L_11 = VirtFuncInvoker0< int32_t >::Invoke(6 /* System.Int32 System.Collections.Generic.Queue`1<UniRx.Timestamped`1<System.Object>>::get_Count() */, (Queue_1_t4227271813 *)L_10);
			if ((((int32_t)L_11) <= ((int32_t)0)))
			{
				goto IL_0116;
			}
		}

IL_0092:
		{
			Queue_1_t4227271813 * L_12 = (Queue_1_t4227271813 *)__this->get_queue_8();
			NullCheck((Queue_1_t4227271813 *)L_12);
			Timestamped_1_t2519184273  L_13 = ((  Timestamped_1_t2519184273  (*) (Queue_1_t4227271813 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 13)->method)((Queue_1_t4227271813 *)L_12, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 13));
			V_13 = (Timestamped_1_t2519184273 )L_13;
			DateTimeOffset_t3712260035  L_14 = ((  DateTimeOffset_t3712260035  (*) (Timestamped_1_t2519184273 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 14)->method)((Timestamped_1_t2519184273 *)(&V_13), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 14));
			V_10 = (DateTimeOffset_t3712260035 )L_14;
			DelayObservable_1_t2635131389 * L_15 = (DelayObservable_1_t2635131389 *)__this->get_parent_2();
			NullCheck(L_15);
			Il2CppObject * L_16 = (Il2CppObject *)L_15->get_scheduler_3();
			NullCheck((Il2CppObject *)L_16);
			DateTimeOffset_t3712260035  L_17 = InterfaceFuncInvoker0< DateTimeOffset_t3712260035  >::Invoke(0 /* System.DateTimeOffset UniRx.IScheduler::get_Now() */, IScheduler_t2938318244_il2cpp_TypeInfo_var, (Il2CppObject *)L_16);
			int32_t L_18 = DateTimeOffset_CompareTo_m1122350244((DateTimeOffset_t3712260035 *)(&V_10), (DateTimeOffset_t3712260035 )L_17, /*hidden argument*/NULL);
			if ((((int32_t)L_18) > ((int32_t)0)))
			{
				goto IL_00e9;
			}
		}

IL_00c5:
		{
			bool L_19 = V_1;
			if (L_19)
			{
				goto IL_00e9;
			}
		}

IL_00cb:
		{
			Queue_1_t4227271813 * L_20 = (Queue_1_t4227271813 *)__this->get_queue_8();
			NullCheck((Queue_1_t4227271813 *)L_20);
			Timestamped_1_t2519184273  L_21 = ((  Timestamped_1_t2519184273  (*) (Queue_1_t4227271813 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 15)->method)((Queue_1_t4227271813 *)L_20, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 15));
			V_14 = (Timestamped_1_t2519184273 )L_21;
			Il2CppObject * L_22 = ((  Il2CppObject * (*) (Timestamped_1_t2519184273 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 16)->method)((Timestamped_1_t2519184273 *)(&V_14), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 16));
			V_5 = (Il2CppObject *)L_22;
			V_4 = (bool)1;
			goto IL_0111;
		}

IL_00e9:
		{
			V_7 = (bool)1;
			DelayObservable_1_t2635131389 * L_23 = (DelayObservable_1_t2635131389 *)__this->get_parent_2();
			NullCheck(L_23);
			Il2CppObject * L_24 = (Il2CppObject *)L_23->get_scheduler_3();
			NullCheck((Il2CppObject *)L_24);
			DateTimeOffset_t3712260035  L_25 = InterfaceFuncInvoker0< DateTimeOffset_t3712260035  >::Invoke(0 /* System.DateTimeOffset UniRx.IScheduler::get_Now() */, IScheduler_t2938318244_il2cpp_TypeInfo_var, (Il2CppObject *)L_24);
			TimeSpan_t763862892  L_26 = DateTimeOffset_Subtract_m2754043121((DateTimeOffset_t3712260035 *)(&V_10), (DateTimeOffset_t3712260035 )L_25, /*hidden argument*/NULL);
			IL2CPP_RUNTIME_CLASS_INIT(Scheduler_t103934925_il2cpp_TypeInfo_var);
			TimeSpan_t763862892  L_27 = Scheduler_Normalize_m3379699520(NULL /*static, unused*/, (TimeSpan_t763862892 )L_26, /*hidden argument*/NULL);
			V_8 = (TimeSpan_t763862892 )L_27;
			__this->set_running_5((bool)0);
		}

IL_0111:
		{
			goto IL_018f;
		}

IL_0116:
		{
			bool L_28 = (bool)__this->get_onCompleted_9();
			if (!L_28)
			{
				goto IL_0181;
			}
		}

IL_0121:
		{
			DateTimeOffset_t3712260035 * L_29 = (DateTimeOffset_t3712260035 *)__this->get_address_of_completeAt_10();
			DelayObservable_1_t2635131389 * L_30 = (DelayObservable_1_t2635131389 *)__this->get_parent_2();
			NullCheck(L_30);
			Il2CppObject * L_31 = (Il2CppObject *)L_30->get_scheduler_3();
			NullCheck((Il2CppObject *)L_31);
			DateTimeOffset_t3712260035  L_32 = InterfaceFuncInvoker0< DateTimeOffset_t3712260035  >::Invoke(0 /* System.DateTimeOffset UniRx.IScheduler::get_Now() */, IScheduler_t2938318244_il2cpp_TypeInfo_var, (Il2CppObject *)L_31);
			int32_t L_33 = DateTimeOffset_CompareTo_m1122350244((DateTimeOffset_t3712260035 *)L_29, (DateTimeOffset_t3712260035 )L_32, /*hidden argument*/NULL);
			if ((((int32_t)L_33) > ((int32_t)0)))
			{
				goto IL_0150;
			}
		}

IL_0142:
		{
			bool L_34 = V_1;
			if (L_34)
			{
				goto IL_0150;
			}
		}

IL_0148:
		{
			V_6 = (bool)1;
			goto IL_017c;
		}

IL_0150:
		{
			V_7 = (bool)1;
			DateTimeOffset_t3712260035 * L_35 = (DateTimeOffset_t3712260035 *)__this->get_address_of_completeAt_10();
			DelayObservable_1_t2635131389 * L_36 = (DelayObservable_1_t2635131389 *)__this->get_parent_2();
			NullCheck(L_36);
			Il2CppObject * L_37 = (Il2CppObject *)L_36->get_scheduler_3();
			NullCheck((Il2CppObject *)L_37);
			DateTimeOffset_t3712260035  L_38 = InterfaceFuncInvoker0< DateTimeOffset_t3712260035  >::Invoke(0 /* System.DateTimeOffset UniRx.IScheduler::get_Now() */, IScheduler_t2938318244_il2cpp_TypeInfo_var, (Il2CppObject *)L_37);
			TimeSpan_t763862892  L_39 = DateTimeOffset_Subtract_m2754043121((DateTimeOffset_t3712260035 *)L_35, (DateTimeOffset_t3712260035 )L_38, /*hidden argument*/NULL);
			IL2CPP_RUNTIME_CLASS_INIT(Scheduler_t103934925_il2cpp_TypeInfo_var);
			TimeSpan_t763862892  L_40 = Scheduler_Normalize_m3379699520(NULL /*static, unused*/, (TimeSpan_t763862892 )L_39, /*hidden argument*/NULL);
			V_8 = (TimeSpan_t763862892 )L_40;
			__this->set_running_5((bool)0);
		}

IL_017c:
		{
			goto IL_018f;
		}

IL_0181:
		{
			__this->set_running_5((bool)0);
			__this->set_active_6((bool)0);
		}

IL_018f:
		{
			IL2CPP_LEAVE(0x19C, FINALLY_0194);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0194;
	}

FINALLY_0194:
	{ // begin finally (depth: 1)
		Il2CppObject * L_41 = V_9;
		Monitor_Exit_m2088237919(NULL /*static, unused*/, (Il2CppObject *)L_41, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(404)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(404)
	{
		IL2CPP_JUMP_TBL(0x19C, IL_019c)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_019c:
	{
		bool L_42 = V_4;
		if (!L_42)
		{
			goto IL_01b9;
		}
	}
	{
		Il2CppObject* L_43 = (Il2CppObject*)((OperatorObserverBase_2_t1187768149 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		Il2CppObject * L_44 = V_5;
		NullCheck((Il2CppObject*)L_43);
		InterfaceActionInvoker1< Il2CppObject * >::Invoke(2 /* System.Void UniRx.IObserver`1<System.Object>::OnNext(T) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10), (Il2CppObject*)L_43, (Il2CppObject *)L_44);
		V_1 = (bool)1;
		goto IL_0213;
	}

IL_01b9:
	{
		bool L_45 = V_6;
		if (!L_45)
		{
			goto IL_01de;
		}
	}

IL_01c0:
	try
	{ // begin try (depth: 1)
		Il2CppObject* L_46 = (Il2CppObject*)((OperatorObserverBase_2_t1187768149 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		NullCheck((Il2CppObject*)L_46);
		InterfaceActionInvoker0::Invoke(0 /* System.Void UniRx.IObserver`1<System.Object>::OnCompleted() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10), (Il2CppObject*)L_46);
		IL2CPP_LEAVE(0x1D9, FINALLY_01d2);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_01d2;
	}

FINALLY_01d2:
	{ // begin finally (depth: 1)
		NullCheck((OperatorObserverBase_2_t1187768149 *)__this);
		VirtActionInvoker0::Invoke(4 /* System.Void UniRx.Operators.OperatorObserverBase`2<System.Object,System.Object>::Dispose() */, (OperatorObserverBase_2_t1187768149 *)__this);
		IL2CPP_END_FINALLY(466)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(466)
	{
		IL2CPP_JUMP_TBL(0x1D9, IL_01d9)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_01d9:
	{
		goto IL_0212;
	}

IL_01de:
	{
		bool L_47 = V_2;
		if (!L_47)
		{
			goto IL_0203;
		}
	}

IL_01e4:
	try
	{ // begin try (depth: 1)
		Il2CppObject* L_48 = (Il2CppObject*)((OperatorObserverBase_2_t1187768149 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		Exception_t1967233988 * L_49 = V_3;
		NullCheck((Il2CppObject*)L_48);
		InterfaceActionInvoker1< Exception_t1967233988 * >::Invoke(1 /* System.Void UniRx.IObserver`1<System.Object>::OnError(System.Exception) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10), (Il2CppObject*)L_48, (Exception_t1967233988 *)L_49);
		IL2CPP_LEAVE(0x1FE, FINALLY_01f7);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_01f7;
	}

FINALLY_01f7:
	{ // begin finally (depth: 1)
		NullCheck((OperatorObserverBase_2_t1187768149 *)__this);
		VirtActionInvoker0::Invoke(4 /* System.Void UniRx.Operators.OperatorObserverBase`2<System.Object,System.Object>::Dispose() */, (OperatorObserverBase_2_t1187768149 *)__this);
		IL2CPP_END_FINALLY(503)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(503)
	{
		IL2CPP_JUMP_TBL(0x1FE, IL_01fe)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_01fe:
	{
		goto IL_0212;
	}

IL_0203:
	{
		bool L_50 = V_7;
		if (!L_50)
		{
			goto IL_0212;
		}
	}
	{
		Action_1_t912315597 * L_51 = ___recurse0;
		TimeSpan_t763862892  L_52 = V_8;
		NullCheck((Action_1_t912315597 *)L_51);
		Action_1_Invoke_m613956924((Action_1_t912315597 *)L_51, (TimeSpan_t763862892 )L_52, /*hidden argument*/Action_1_Invoke_m613956924_MethodInfo_var);
	}

IL_0212:
	{
		return;
	}

IL_0213:
	{
		goto IL_0032;
	}

IL_0218:
	{
		return;
	}
}
// System.Void UniRx.Operators.DelayObservable`1<System.Object>::.ctor(UniRx.IObservable`1<T>,System.TimeSpan,UniRx.IScheduler)
extern Il2CppClass* Scheduler_t103934925_il2cpp_TypeInfo_var;
extern const uint32_t DelayObservable_1__ctor_m4083549844_MetadataUsageId;
extern "C"  void DelayObservable_1__ctor_m4083549844_gshared (DelayObservable_1_t2635131389 * __this, Il2CppObject* ___source0, TimeSpan_t763862892  ___dueTime1, Il2CppObject * ___scheduler2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DelayObservable_1__ctor_m4083549844_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	DelayObservable_1_t2635131389 * G_B2_0 = NULL;
	DelayObservable_1_t2635131389 * G_B1_0 = NULL;
	int32_t G_B3_0 = 0;
	DelayObservable_1_t2635131389 * G_B3_1 = NULL;
	{
		Il2CppObject * L_0 = ___scheduler2;
		IL2CPP_RUNTIME_CLASS_INIT(Scheduler_t103934925_il2cpp_TypeInfo_var);
		Il2CppObject * L_1 = ((Scheduler_t103934925_StaticFields*)Scheduler_t103934925_il2cpp_TypeInfo_var->static_fields)->get_CurrentThread_0();
		G_B1_0 = ((DelayObservable_1_t2635131389 *)(__this));
		if ((((Il2CppObject*)(Il2CppObject *)L_0) == ((Il2CppObject*)(Il2CppObject *)L_1)))
		{
			G_B2_0 = ((DelayObservable_1_t2635131389 *)(__this));
			goto IL_0014;
		}
	}
	{
		Il2CppObject* L_2 = ___source0;
		bool L_3 = ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)(NULL /*static, unused*/, (Il2CppObject*)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		G_B3_0 = ((int32_t)(L_3));
		G_B3_1 = ((DelayObservable_1_t2635131389 *)(G_B1_0));
		goto IL_0015;
	}

IL_0014:
	{
		G_B3_0 = 1;
		G_B3_1 = ((DelayObservable_1_t2635131389 *)(G_B2_0));
	}

IL_0015:
	{
		NullCheck((OperatorObservableBase_1_t4196218687 *)G_B3_1);
		((  void (*) (OperatorObservableBase_1_t4196218687 *, bool, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)((OperatorObservableBase_1_t4196218687 *)G_B3_1, (bool)G_B3_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		Il2CppObject* L_4 = ___source0;
		__this->set_source_1(L_4);
		TimeSpan_t763862892  L_5 = ___dueTime1;
		__this->set_dueTime_2(L_5);
		Il2CppObject * L_6 = ___scheduler2;
		__this->set_scheduler_3(L_6);
		return;
	}
}
// System.IDisposable UniRx.Operators.DelayObservable`1<System.Object>::SubscribeCore(UniRx.IObserver`1<T>,System.IDisposable)
extern "C"  Il2CppObject * DelayObservable_1_SubscribeCore_m722984619_gshared (DelayObservable_1_t2635131389 * __this, Il2CppObject* ___observer0, Il2CppObject * ___cancel1, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = ___observer0;
		Il2CppObject * L_1 = ___cancel1;
		Delay_t3380693988 * L_2 = (Delay_t3380693988 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		((  void (*) (Delay_t3380693988 *, DelayObservable_1_t2635131389 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)(L_2, (DelayObservable_1_t2635131389 *)__this, (Il2CppObject*)L_0, (Il2CppObject *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		NullCheck((Delay_t3380693988 *)L_2);
		Il2CppObject * L_3 = ((  Il2CppObject * (*) (Delay_t3380693988 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->method)((Delay_t3380693988 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		return L_3;
	}
}
// System.Void UniRx.Operators.DelaySubscriptionObservable`1/<SubscribeCore>c__AnonStorey5E<System.Object>::.ctor()
extern "C"  void U3CSubscribeCoreU3Ec__AnonStorey5E__ctor_m777474035_gshared (U3CSubscribeCoreU3Ec__AnonStorey5E_t2206845999 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UniRx.Operators.DelaySubscriptionObservable`1/<SubscribeCore>c__AnonStorey5F<System.Object>::.ctor()
extern "C"  void U3CSubscribeCoreU3Ec__AnonStorey5F__ctor_m2995231988_gshared (U3CSubscribeCoreU3Ec__AnonStorey5F_t3873420168 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UniRx.Operators.DelaySubscriptionObservable`1/<SubscribeCore>c__AnonStorey5F<System.Object>::<>m__79()
extern "C"  void U3CSubscribeCoreU3Ec__AnonStorey5F_U3CU3Em__79_m3098079519_gshared (U3CSubscribeCoreU3Ec__AnonStorey5F_t3873420168 * __this, const MethodInfo* method)
{
	{
		MultipleAssignmentDisposable_t3090232719 * L_0 = (MultipleAssignmentDisposable_t3090232719 *)__this->get_d_0();
		DelaySubscriptionObservable_1_t1759674162 * L_1 = (DelaySubscriptionObservable_1_t1759674162 *)__this->get_U3CU3Ef__this_2();
		NullCheck(L_1);
		Il2CppObject* L_2 = (Il2CppObject*)L_1->get_source_1();
		U3CSubscribeCoreU3Ec__AnonStorey5E_t2206845999 * L_3 = (U3CSubscribeCoreU3Ec__AnonStorey5E_t2206845999 *)__this->get_U3CU3Ef__refU2494_1();
		NullCheck(L_3);
		Il2CppObject* L_4 = (Il2CppObject*)L_3->get_observer_0();
		NullCheck((Il2CppObject*)L_2);
		Il2CppObject * L_5 = InterfaceFuncInvoker1< Il2CppObject *, Il2CppObject* >::Invoke(0 /* System.IDisposable UniRx.IObservable`1<System.Object>::Subscribe(UniRx.IObserver`1<T>) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0), (Il2CppObject*)L_2, (Il2CppObject*)L_4);
		NullCheck((MultipleAssignmentDisposable_t3090232719 *)L_0);
		MultipleAssignmentDisposable_set_Disposable_m2693870941((MultipleAssignmentDisposable_t3090232719 *)L_0, (Il2CppObject *)L_5, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UniRx.Operators.DelaySubscriptionObservable`1/<SubscribeCore>c__AnonStorey60<System.Object>::.ctor()
extern "C"  void U3CSubscribeCoreU3Ec__AnonStorey60__ctor_m1480217085_gshared (U3CSubscribeCoreU3Ec__AnonStorey60_t1692718505 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UniRx.Operators.DelaySubscriptionObservable`1/<SubscribeCore>c__AnonStorey60<System.Object>::<>m__7A()
extern "C"  void U3CSubscribeCoreU3Ec__AnonStorey60_U3CU3Em__7A_m3162678768_gshared (U3CSubscribeCoreU3Ec__AnonStorey60_t1692718505 * __this, const MethodInfo* method)
{
	{
		MultipleAssignmentDisposable_t3090232719 * L_0 = (MultipleAssignmentDisposable_t3090232719 *)__this->get_d_0();
		DelaySubscriptionObservable_1_t1759674162 * L_1 = (DelaySubscriptionObservable_1_t1759674162 *)__this->get_U3CU3Ef__this_2();
		NullCheck(L_1);
		Il2CppObject* L_2 = (Il2CppObject*)L_1->get_source_1();
		U3CSubscribeCoreU3Ec__AnonStorey5E_t2206845999 * L_3 = (U3CSubscribeCoreU3Ec__AnonStorey5E_t2206845999 *)__this->get_U3CU3Ef__refU2494_1();
		NullCheck(L_3);
		Il2CppObject* L_4 = (Il2CppObject*)L_3->get_observer_0();
		NullCheck((Il2CppObject*)L_2);
		Il2CppObject * L_5 = InterfaceFuncInvoker1< Il2CppObject *, Il2CppObject* >::Invoke(0 /* System.IDisposable UniRx.IObservable`1<System.Object>::Subscribe(UniRx.IObserver`1<T>) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0), (Il2CppObject*)L_2, (Il2CppObject*)L_4);
		NullCheck((MultipleAssignmentDisposable_t3090232719 *)L_0);
		MultipleAssignmentDisposable_set_Disposable_m2693870941((MultipleAssignmentDisposable_t3090232719 *)L_0, (Il2CppObject *)L_5, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UniRx.Operators.DelaySubscriptionObservable`1<System.Object>::.ctor(UniRx.IObservable`1<T>,System.TimeSpan,UniRx.IScheduler)
extern Il2CppClass* Scheduler_t103934925_il2cpp_TypeInfo_var;
extern const MethodInfo* Nullable_1__ctor_m4008503583_MethodInfo_var;
extern const uint32_t DelaySubscriptionObservable_1__ctor_m3412956983_MetadataUsageId;
extern "C"  void DelaySubscriptionObservable_1__ctor_m3412956983_gshared (DelaySubscriptionObservable_1_t1759674162 * __this, Il2CppObject* ___source0, TimeSpan_t763862892  ___dueTime1, Il2CppObject * ___scheduler2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DelaySubscriptionObservable_1__ctor_m3412956983_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	DelaySubscriptionObservable_1_t1759674162 * G_B2_0 = NULL;
	DelaySubscriptionObservable_1_t1759674162 * G_B1_0 = NULL;
	int32_t G_B3_0 = 0;
	DelaySubscriptionObservable_1_t1759674162 * G_B3_1 = NULL;
	{
		Il2CppObject * L_0 = ___scheduler2;
		IL2CPP_RUNTIME_CLASS_INIT(Scheduler_t103934925_il2cpp_TypeInfo_var);
		Il2CppObject * L_1 = ((Scheduler_t103934925_StaticFields*)Scheduler_t103934925_il2cpp_TypeInfo_var->static_fields)->get_CurrentThread_0();
		G_B1_0 = ((DelaySubscriptionObservable_1_t1759674162 *)(__this));
		if ((((Il2CppObject*)(Il2CppObject *)L_0) == ((Il2CppObject*)(Il2CppObject *)L_1)))
		{
			G_B2_0 = ((DelaySubscriptionObservable_1_t1759674162 *)(__this));
			goto IL_0014;
		}
	}
	{
		Il2CppObject* L_2 = ___source0;
		bool L_3 = ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)(NULL /*static, unused*/, (Il2CppObject*)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		G_B3_0 = ((int32_t)(L_3));
		G_B3_1 = ((DelaySubscriptionObservable_1_t1759674162 *)(G_B1_0));
		goto IL_0015;
	}

IL_0014:
	{
		G_B3_0 = 1;
		G_B3_1 = ((DelaySubscriptionObservable_1_t1759674162 *)(G_B2_0));
	}

IL_0015:
	{
		NullCheck((OperatorObservableBase_1_t4196218687 *)G_B3_1);
		((  void (*) (OperatorObservableBase_1_t4196218687 *, bool, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)((OperatorObservableBase_1_t4196218687 *)G_B3_1, (bool)G_B3_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		Il2CppObject* L_4 = ___source0;
		__this->set_source_1(L_4);
		Il2CppObject * L_5 = ___scheduler2;
		__this->set_scheduler_2(L_5);
		TimeSpan_t763862892  L_6 = ___dueTime1;
		Nullable_1_t3649900800  L_7;
		memset(&L_7, 0, sizeof(L_7));
		Nullable_1__ctor_m4008503583(&L_7, (TimeSpan_t763862892 )L_6, /*hidden argument*/Nullable_1__ctor_m4008503583_MethodInfo_var);
		__this->set_dueTimeT_3(L_7);
		return;
	}
}
// System.Void UniRx.Operators.DelaySubscriptionObservable`1<System.Object>::.ctor(UniRx.IObservable`1<T>,System.DateTimeOffset,UniRx.IScheduler)
extern Il2CppClass* Scheduler_t103934925_il2cpp_TypeInfo_var;
extern const MethodInfo* Nullable_1__ctor_m522950596_MethodInfo_var;
extern const uint32_t DelaySubscriptionObservable_1__ctor_m2101111310_MetadataUsageId;
extern "C"  void DelaySubscriptionObservable_1__ctor_m2101111310_gshared (DelaySubscriptionObservable_1_t1759674162 * __this, Il2CppObject* ___source0, DateTimeOffset_t3712260035  ___dueTime1, Il2CppObject * ___scheduler2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DelaySubscriptionObservable_1__ctor_m2101111310_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	DelaySubscriptionObservable_1_t1759674162 * G_B2_0 = NULL;
	DelaySubscriptionObservable_1_t1759674162 * G_B1_0 = NULL;
	int32_t G_B3_0 = 0;
	DelaySubscriptionObservable_1_t1759674162 * G_B3_1 = NULL;
	{
		Il2CppObject * L_0 = ___scheduler2;
		IL2CPP_RUNTIME_CLASS_INIT(Scheduler_t103934925_il2cpp_TypeInfo_var);
		Il2CppObject * L_1 = ((Scheduler_t103934925_StaticFields*)Scheduler_t103934925_il2cpp_TypeInfo_var->static_fields)->get_CurrentThread_0();
		G_B1_0 = ((DelaySubscriptionObservable_1_t1759674162 *)(__this));
		if ((((Il2CppObject*)(Il2CppObject *)L_0) == ((Il2CppObject*)(Il2CppObject *)L_1)))
		{
			G_B2_0 = ((DelaySubscriptionObservable_1_t1759674162 *)(__this));
			goto IL_0014;
		}
	}
	{
		Il2CppObject* L_2 = ___source0;
		bool L_3 = ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)(NULL /*static, unused*/, (Il2CppObject*)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		G_B3_0 = ((int32_t)(L_3));
		G_B3_1 = ((DelaySubscriptionObservable_1_t1759674162 *)(G_B1_0));
		goto IL_0015;
	}

IL_0014:
	{
		G_B3_0 = 1;
		G_B3_1 = ((DelaySubscriptionObservable_1_t1759674162 *)(G_B2_0));
	}

IL_0015:
	{
		NullCheck((OperatorObservableBase_1_t4196218687 *)G_B3_1);
		((  void (*) (OperatorObservableBase_1_t4196218687 *, bool, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)((OperatorObservableBase_1_t4196218687 *)G_B3_1, (bool)G_B3_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		Il2CppObject* L_4 = ___source0;
		__this->set_source_1(L_4);
		Il2CppObject * L_5 = ___scheduler2;
		__this->set_scheduler_2(L_5);
		DateTimeOffset_t3712260035  L_6 = ___dueTime1;
		Nullable_1_t2303330647  L_7;
		memset(&L_7, 0, sizeof(L_7));
		Nullable_1__ctor_m522950596(&L_7, (DateTimeOffset_t3712260035 )L_6, /*hidden argument*/Nullable_1__ctor_m522950596_MethodInfo_var);
		__this->set_dueTimeD_4(L_7);
		return;
	}
}
// System.IDisposable UniRx.Operators.DelaySubscriptionObservable`1<System.Object>::SubscribeCore(UniRx.IObserver`1<T>,System.IDisposable)
extern Il2CppClass* MultipleAssignmentDisposable_t3090232719_il2cpp_TypeInfo_var;
extern Il2CppClass* Scheduler_t103934925_il2cpp_TypeInfo_var;
extern Il2CppClass* Action_t437523947_il2cpp_TypeInfo_var;
extern Il2CppClass* IScheduler_t2938318244_il2cpp_TypeInfo_var;
extern const MethodInfo* Nullable_1_get_HasValue_m2797118855_MethodInfo_var;
extern const MethodInfo* Nullable_1_get_Value_m3338249190_MethodInfo_var;
extern const MethodInfo* Nullable_1_get_Value_m4125167011_MethodInfo_var;
extern const uint32_t DelaySubscriptionObservable_1_SubscribeCore_m4187242958_MetadataUsageId;
extern "C"  Il2CppObject * DelaySubscriptionObservable_1_SubscribeCore_m4187242958_gshared (DelaySubscriptionObservable_1_t1759674162 * __this, Il2CppObject* ___observer0, Il2CppObject * ___cancel1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DelaySubscriptionObservable_1_SubscribeCore_m4187242958_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	TimeSpan_t763862892  V_0;
	memset(&V_0, 0, sizeof(V_0));
	U3CSubscribeCoreU3Ec__AnonStorey5E_t2206845999 * V_1 = NULL;
	Nullable_1_t3649900800  V_2;
	memset(&V_2, 0, sizeof(V_2));
	U3CSubscribeCoreU3Ec__AnonStorey5F_t3873420168 * V_3 = NULL;
	Nullable_1_t3649900800  V_4;
	memset(&V_4, 0, sizeof(V_4));
	U3CSubscribeCoreU3Ec__AnonStorey60_t1692718505 * V_5 = NULL;
	Nullable_1_t2303330647  V_6;
	memset(&V_6, 0, sizeof(V_6));
	{
		U3CSubscribeCoreU3Ec__AnonStorey5E_t2206845999 * L_0 = (U3CSubscribeCoreU3Ec__AnonStorey5E_t2206845999 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		((  void (*) (U3CSubscribeCoreU3Ec__AnonStorey5E_t2206845999 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		V_1 = (U3CSubscribeCoreU3Ec__AnonStorey5E_t2206845999 *)L_0;
		U3CSubscribeCoreU3Ec__AnonStorey5E_t2206845999 * L_1 = V_1;
		Il2CppObject* L_2 = ___observer0;
		NullCheck(L_1);
		L_1->set_observer_0(L_2);
		U3CSubscribeCoreU3Ec__AnonStorey5E_t2206845999 * L_3 = V_1;
		NullCheck(L_3);
		L_3->set_U3CU3Ef__this_1(__this);
		Nullable_1_t3649900800  L_4 = (Nullable_1_t3649900800 )__this->get_dueTimeT_3();
		V_2 = (Nullable_1_t3649900800 )L_4;
		bool L_5 = Nullable_1_get_HasValue_m2797118855((Nullable_1_t3649900800 *)(&V_2), /*hidden argument*/Nullable_1_get_HasValue_m2797118855_MethodInfo_var);
		if (!L_5)
		{
			goto IL_0085;
		}
	}
	{
		U3CSubscribeCoreU3Ec__AnonStorey5F_t3873420168 * L_6 = (U3CSubscribeCoreU3Ec__AnonStorey5F_t3873420168 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		((  void (*) (U3CSubscribeCoreU3Ec__AnonStorey5F_t3873420168 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6)->method)(L_6, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		V_3 = (U3CSubscribeCoreU3Ec__AnonStorey5F_t3873420168 *)L_6;
		U3CSubscribeCoreU3Ec__AnonStorey5F_t3873420168 * L_7 = V_3;
		U3CSubscribeCoreU3Ec__AnonStorey5E_t2206845999 * L_8 = V_1;
		NullCheck(L_7);
		L_7->set_U3CU3Ef__refU2494_1(L_8);
		U3CSubscribeCoreU3Ec__AnonStorey5F_t3873420168 * L_9 = V_3;
		NullCheck(L_9);
		L_9->set_U3CU3Ef__this_2(__this);
		U3CSubscribeCoreU3Ec__AnonStorey5F_t3873420168 * L_10 = V_3;
		MultipleAssignmentDisposable_t3090232719 * L_11 = (MultipleAssignmentDisposable_t3090232719 *)il2cpp_codegen_object_new(MultipleAssignmentDisposable_t3090232719_il2cpp_TypeInfo_var);
		MultipleAssignmentDisposable__ctor_m4117172186(L_11, /*hidden argument*/NULL);
		NullCheck(L_10);
		L_10->set_d_0(L_11);
		Nullable_1_t3649900800  L_12 = (Nullable_1_t3649900800 )__this->get_dueTimeT_3();
		V_4 = (Nullable_1_t3649900800 )L_12;
		TimeSpan_t763862892  L_13 = Nullable_1_get_Value_m3338249190((Nullable_1_t3649900800 *)(&V_4), /*hidden argument*/Nullable_1_get_Value_m3338249190_MethodInfo_var);
		IL2CPP_RUNTIME_CLASS_INIT(Scheduler_t103934925_il2cpp_TypeInfo_var);
		TimeSpan_t763862892  L_14 = Scheduler_Normalize_m3379699520(NULL /*static, unused*/, (TimeSpan_t763862892 )L_13, /*hidden argument*/NULL);
		V_0 = (TimeSpan_t763862892 )L_14;
		U3CSubscribeCoreU3Ec__AnonStorey5F_t3873420168 * L_15 = V_3;
		NullCheck(L_15);
		MultipleAssignmentDisposable_t3090232719 * L_16 = (MultipleAssignmentDisposable_t3090232719 *)L_15->get_d_0();
		Il2CppObject * L_17 = (Il2CppObject *)__this->get_scheduler_2();
		TimeSpan_t763862892  L_18 = V_0;
		U3CSubscribeCoreU3Ec__AnonStorey5F_t3873420168 * L_19 = V_3;
		IntPtr_t L_20;
		L_20.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7));
		Action_t437523947 * L_21 = (Action_t437523947 *)il2cpp_codegen_object_new(Action_t437523947_il2cpp_TypeInfo_var);
		Action__ctor_m2957240604(L_21, (Il2CppObject *)L_19, (IntPtr_t)L_20, /*hidden argument*/NULL);
		NullCheck((Il2CppObject *)L_17);
		Il2CppObject * L_22 = InterfaceFuncInvoker2< Il2CppObject *, TimeSpan_t763862892 , Action_t437523947 * >::Invoke(2 /* System.IDisposable UniRx.IScheduler::Schedule(System.TimeSpan,System.Action) */, IScheduler_t2938318244_il2cpp_TypeInfo_var, (Il2CppObject *)L_17, (TimeSpan_t763862892 )L_18, (Action_t437523947 *)L_21);
		NullCheck((MultipleAssignmentDisposable_t3090232719 *)L_16);
		MultipleAssignmentDisposable_set_Disposable_m2693870941((MultipleAssignmentDisposable_t3090232719 *)L_16, (Il2CppObject *)L_22, /*hidden argument*/NULL);
		U3CSubscribeCoreU3Ec__AnonStorey5F_t3873420168 * L_23 = V_3;
		NullCheck(L_23);
		MultipleAssignmentDisposable_t3090232719 * L_24 = (MultipleAssignmentDisposable_t3090232719 *)L_23->get_d_0();
		return L_24;
	}

IL_0085:
	{
		U3CSubscribeCoreU3Ec__AnonStorey60_t1692718505 * L_25 = (U3CSubscribeCoreU3Ec__AnonStorey60_t1692718505 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8));
		((  void (*) (U3CSubscribeCoreU3Ec__AnonStorey60_t1692718505 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9)->method)(L_25, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9));
		V_5 = (U3CSubscribeCoreU3Ec__AnonStorey60_t1692718505 *)L_25;
		U3CSubscribeCoreU3Ec__AnonStorey60_t1692718505 * L_26 = V_5;
		U3CSubscribeCoreU3Ec__AnonStorey5E_t2206845999 * L_27 = V_1;
		NullCheck(L_26);
		L_26->set_U3CU3Ef__refU2494_1(L_27);
		U3CSubscribeCoreU3Ec__AnonStorey60_t1692718505 * L_28 = V_5;
		NullCheck(L_28);
		L_28->set_U3CU3Ef__this_2(__this);
		U3CSubscribeCoreU3Ec__AnonStorey60_t1692718505 * L_29 = V_5;
		MultipleAssignmentDisposable_t3090232719 * L_30 = (MultipleAssignmentDisposable_t3090232719 *)il2cpp_codegen_object_new(MultipleAssignmentDisposable_t3090232719_il2cpp_TypeInfo_var);
		MultipleAssignmentDisposable__ctor_m4117172186(L_30, /*hidden argument*/NULL);
		NullCheck(L_29);
		L_29->set_d_0(L_30);
		U3CSubscribeCoreU3Ec__AnonStorey60_t1692718505 * L_31 = V_5;
		NullCheck(L_31);
		MultipleAssignmentDisposable_t3090232719 * L_32 = (MultipleAssignmentDisposable_t3090232719 *)L_31->get_d_0();
		Il2CppObject * L_33 = (Il2CppObject *)__this->get_scheduler_2();
		Nullable_1_t2303330647  L_34 = (Nullable_1_t2303330647 )__this->get_dueTimeD_4();
		V_6 = (Nullable_1_t2303330647 )L_34;
		DateTimeOffset_t3712260035  L_35 = Nullable_1_get_Value_m4125167011((Nullable_1_t2303330647 *)(&V_6), /*hidden argument*/Nullable_1_get_Value_m4125167011_MethodInfo_var);
		U3CSubscribeCoreU3Ec__AnonStorey60_t1692718505 * L_36 = V_5;
		IntPtr_t L_37;
		L_37.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10));
		Action_t437523947 * L_38 = (Action_t437523947 *)il2cpp_codegen_object_new(Action_t437523947_il2cpp_TypeInfo_var);
		Action__ctor_m2957240604(L_38, (Il2CppObject *)L_36, (IntPtr_t)L_37, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Scheduler_t103934925_il2cpp_TypeInfo_var);
		Il2CppObject * L_39 = Scheduler_Schedule_m96532664(NULL /*static, unused*/, (Il2CppObject *)L_33, (DateTimeOffset_t3712260035 )L_35, (Action_t437523947 *)L_38, /*hidden argument*/NULL);
		NullCheck((MultipleAssignmentDisposable_t3090232719 *)L_32);
		MultipleAssignmentDisposable_set_Disposable_m2693870941((MultipleAssignmentDisposable_t3090232719 *)L_32, (Il2CppObject *)L_39, /*hidden argument*/NULL);
		U3CSubscribeCoreU3Ec__AnonStorey60_t1692718505 * L_40 = V_5;
		NullCheck(L_40);
		MultipleAssignmentDisposable_t3090232719 * L_41 = (MultipleAssignmentDisposable_t3090232719 *)L_40->get_d_0();
		return L_41;
	}
}
// System.Void UniRx.Operators.DematerializeObservable`1/Dematerialize<System.Object>::.ctor(UniRx.Operators.DematerializeObservable`1<T>,UniRx.IObserver`1<T>,System.IDisposable)
extern "C"  void Dematerialize__ctor_m3301817088_gshared (Dematerialize_t196388645 * __this, DematerializeObservable_1_t1297640382 * ___parent0, Il2CppObject* ___observer1, Il2CppObject * ___cancel2, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = ___observer1;
		Il2CppObject * L_1 = ___cancel2;
		NullCheck((OperatorObserverBase_2_t306849478 *)__this);
		((  void (*) (OperatorObserverBase_2_t306849478 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((OperatorObserverBase_2_t306849478 *)__this, (Il2CppObject*)L_0, (Il2CppObject *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		DematerializeObservable_1_t1297640382 * L_2 = ___parent0;
		__this->set_parent_2(L_2);
		return;
	}
}
// System.IDisposable UniRx.Operators.DematerializeObservable`1/Dematerialize<System.Object>::Run()
extern "C"  Il2CppObject * Dematerialize_Run_m940466119_gshared (Dematerialize_t196388645 * __this, const MethodInfo* method)
{
	{
		DematerializeObservable_1_t1297640382 * L_0 = (DematerializeObservable_1_t1297640382 *)__this->get_parent_2();
		NullCheck(L_0);
		Il2CppObject* L_1 = (Il2CppObject*)L_0->get_source_1();
		NullCheck((Il2CppObject*)L_1);
		Il2CppObject * L_2 = InterfaceFuncInvoker1< Il2CppObject *, Il2CppObject* >::Invoke(0 /* System.IDisposable UniRx.IObservable`1<UniRx.Notification`1<System.Object>>::Subscribe(UniRx.IObserver`1<T>) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), (Il2CppObject*)L_1, (Il2CppObject*)__this);
		return L_2;
	}
}
// System.Void UniRx.Operators.DematerializeObservable`1/Dematerialize<System.Object>::OnNext(UniRx.Notification`1<T>)
extern "C"  void Dematerialize_OnNext_m646096099_gshared (Dematerialize_t196388645 * __this, Notification_1_t38356375 * ___value0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Notification_1_t38356375 * L_0 = ___value0;
		NullCheck((Notification_1_t38356375 *)L_0);
		int32_t L_1 = VirtFuncInvoker0< int32_t >::Invoke(8 /* UniRx.NotificationKind UniRx.Notification`1<System.Object>::get_Kind() */, (Notification_1_t38356375 *)L_0);
		V_0 = (int32_t)L_1;
		int32_t L_2 = V_0;
		if (L_2 == 0)
		{
			goto IL_001e;
		}
		if (L_2 == 1)
		{
			goto IL_0036;
		}
		if (L_2 == 2)
		{
			goto IL_005a;
		}
	}
	{
		goto IL_0078;
	}

IL_001e:
	{
		Il2CppObject* L_3 = (Il2CppObject*)((OperatorObserverBase_2_t306849478 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		Notification_1_t38356375 * L_4 = ___value0;
		NullCheck((Notification_1_t38356375 *)L_4);
		Il2CppObject * L_5 = VirtFuncInvoker0< Il2CppObject * >::Invoke(5 /* T UniRx.Notification`1<System.Object>::get_Value() */, (Notification_1_t38356375 *)L_4);
		NullCheck((Il2CppObject*)L_3);
		InterfaceActionInvoker1< Il2CppObject * >::Invoke(2 /* System.Void UniRx.IObserver`1<System.Object>::OnNext(T) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5), (Il2CppObject*)L_3, (Il2CppObject *)L_5);
		goto IL_007d;
	}

IL_0036:
	try
	{ // begin try (depth: 1)
		Il2CppObject* L_6 = (Il2CppObject*)((OperatorObserverBase_2_t306849478 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		Notification_1_t38356375 * L_7 = ___value0;
		NullCheck((Notification_1_t38356375 *)L_7);
		Exception_t1967233988 * L_8 = VirtFuncInvoker0< Exception_t1967233988 * >::Invoke(7 /* System.Exception UniRx.Notification`1<System.Object>::get_Exception() */, (Notification_1_t38356375 *)L_7);
		NullCheck((Il2CppObject*)L_6);
		InterfaceActionInvoker1< Exception_t1967233988 * >::Invoke(1 /* System.Void UniRx.IObserver`1<System.Object>::OnError(System.Exception) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5), (Il2CppObject*)L_6, (Exception_t1967233988 *)L_8);
		IL2CPP_LEAVE(0x55, FINALLY_004e);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_004e;
	}

FINALLY_004e:
	{ // begin finally (depth: 1)
		NullCheck((OperatorObserverBase_2_t306849478 *)__this);
		VirtActionInvoker0::Invoke(4 /* System.Void UniRx.Operators.OperatorObserverBase`2<UniRx.Notification`1<System.Object>,System.Object>::Dispose() */, (OperatorObserverBase_2_t306849478 *)__this);
		IL2CPP_END_FINALLY(78)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(78)
	{
		IL2CPP_JUMP_TBL(0x55, IL_0055)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0055:
	{
		goto IL_007d;
	}

IL_005a:
	try
	{ // begin try (depth: 1)
		Il2CppObject* L_9 = (Il2CppObject*)((OperatorObserverBase_2_t306849478 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		NullCheck((Il2CppObject*)L_9);
		InterfaceActionInvoker0::Invoke(0 /* System.Void UniRx.IObserver`1<System.Object>::OnCompleted() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5), (Il2CppObject*)L_9);
		IL2CPP_LEAVE(0x73, FINALLY_006c);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_006c;
	}

FINALLY_006c:
	{ // begin finally (depth: 1)
		NullCheck((OperatorObserverBase_2_t306849478 *)__this);
		VirtActionInvoker0::Invoke(4 /* System.Void UniRx.Operators.OperatorObserverBase`2<UniRx.Notification`1<System.Object>,System.Object>::Dispose() */, (OperatorObserverBase_2_t306849478 *)__this);
		IL2CPP_END_FINALLY(108)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(108)
	{
		IL2CPP_JUMP_TBL(0x73, IL_0073)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0073:
	{
		goto IL_007d;
	}

IL_0078:
	{
		goto IL_007d;
	}

IL_007d:
	{
		return;
	}
}
// System.Void UniRx.Operators.DematerializeObservable`1/Dematerialize<System.Object>::OnError(System.Exception)
extern "C"  void Dematerialize_OnError_m3809367600_gshared (Dematerialize_t196388645 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method)
{
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		Il2CppObject* L_0 = (Il2CppObject*)((OperatorObserverBase_2_t306849478 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		Exception_t1967233988 * L_1 = ___error0;
		NullCheck((Il2CppObject*)L_0);
		InterfaceActionInvoker1< Exception_t1967233988 * >::Invoke(1 /* System.Void UniRx.IObserver`1<System.Object>::OnError(System.Exception) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5), (Il2CppObject*)L_0, (Exception_t1967233988 *)L_1);
		IL2CPP_LEAVE(0x1A, FINALLY_0013);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0013;
	}

FINALLY_0013:
	{ // begin finally (depth: 1)
		NullCheck((OperatorObserverBase_2_t306849478 *)__this);
		VirtActionInvoker0::Invoke(4 /* System.Void UniRx.Operators.OperatorObserverBase`2<UniRx.Notification`1<System.Object>,System.Object>::Dispose() */, (OperatorObserverBase_2_t306849478 *)__this);
		IL2CPP_END_FINALLY(19)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(19)
	{
		IL2CPP_JUMP_TBL(0x1A, IL_001a)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_001a:
	{
		return;
	}
}
// System.Void UniRx.Operators.DematerializeObservable`1/Dematerialize<System.Object>::OnCompleted()
extern "C"  void Dematerialize_OnCompleted_m218681731_gshared (Dematerialize_t196388645 * __this, const MethodInfo* method)
{
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		Il2CppObject* L_0 = (Il2CppObject*)((OperatorObserverBase_2_t306849478 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		NullCheck((Il2CppObject*)L_0);
		InterfaceActionInvoker0::Invoke(0 /* System.Void UniRx.IObserver`1<System.Object>::OnCompleted() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5), (Il2CppObject*)L_0);
		IL2CPP_LEAVE(0x19, FINALLY_0012);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0012;
	}

FINALLY_0012:
	{ // begin finally (depth: 1)
		NullCheck((OperatorObserverBase_2_t306849478 *)__this);
		VirtActionInvoker0::Invoke(4 /* System.Void UniRx.Operators.OperatorObserverBase`2<UniRx.Notification`1<System.Object>,System.Object>::Dispose() */, (OperatorObserverBase_2_t306849478 *)__this);
		IL2CPP_END_FINALLY(18)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(18)
	{
		IL2CPP_JUMP_TBL(0x19, IL_0019)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0019:
	{
		return;
	}
}
// System.Void UniRx.Operators.DematerializeObservable`1<System.Object>::.ctor(UniRx.IObservable`1<UniRx.Notification`1<T>>)
extern "C"  void DematerializeObservable_1__ctor_m2318627865_gshared (DematerializeObservable_1_t1297640382 * __this, Il2CppObject* ___source0, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = ___source0;
		bool L_1 = ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)(NULL /*static, unused*/, (Il2CppObject*)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		NullCheck((OperatorObservableBase_1_t4196218687 *)__this);
		((  void (*) (OperatorObservableBase_1_t4196218687 *, bool, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)((OperatorObservableBase_1_t4196218687 *)__this, (bool)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		Il2CppObject* L_2 = ___source0;
		__this->set_source_1(L_2);
		return;
	}
}
// System.IDisposable UniRx.Operators.DematerializeObservable`1<System.Object>::SubscribeCore(UniRx.IObserver`1<T>,System.IDisposable)
extern "C"  Il2CppObject * DematerializeObservable_1_SubscribeCore_m2578143202_gshared (DematerializeObservable_1_t1297640382 * __this, Il2CppObject* ___observer0, Il2CppObject * ___cancel1, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = ___observer0;
		Il2CppObject * L_1 = ___cancel1;
		Dematerialize_t196388645 * L_2 = (Dematerialize_t196388645 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		((  void (*) (Dematerialize_t196388645 *, DematerializeObservable_1_t1297640382 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)(L_2, (DematerializeObservable_1_t1297640382 *)__this, (Il2CppObject*)L_0, (Il2CppObject *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		NullCheck((Dematerialize_t196388645 *)L_2);
		Il2CppObject * L_3 = ((  Il2CppObject * (*) (Dematerialize_t196388645 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->method)((Dematerialize_t196388645 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		return L_3;
	}
}
// System.Void UniRx.Operators.DistinctObservable`1/Distinct<System.Object>::.ctor(UniRx.Operators.DistinctObservable`1<T>,UniRx.IObserver`1<T>,System.IDisposable)
extern "C"  void Distinct__ctor_m3356823872_gshared (Distinct_t3232980021 * __this, DistinctObservable_1_t18876622 * ___parent0, Il2CppObject* ___observer1, Il2CppObject * ___cancel2, const MethodInfo* method)
{
	Distinct_t3232980021 * G_B2_0 = NULL;
	Distinct_t3232980021 * G_B1_0 = NULL;
	HashSet_1_t3535795091 * G_B3_0 = NULL;
	Distinct_t3232980021 * G_B3_1 = NULL;
	{
		Il2CppObject* L_0 = ___observer1;
		Il2CppObject * L_1 = ___cancel2;
		NullCheck((OperatorObserverBase_2_t1187768149 *)__this);
		((  void (*) (OperatorObserverBase_2_t1187768149 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((OperatorObserverBase_2_t1187768149 *)__this, (Il2CppObject*)L_0, (Il2CppObject *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		DistinctObservable_1_t18876622 * L_2 = ___parent0;
		NullCheck(L_2);
		Il2CppObject* L_3 = (Il2CppObject*)L_2->get_comparer_2();
		G_B1_0 = ((Distinct_t3232980021 *)(__this));
		if (L_3)
		{
			G_B2_0 = ((Distinct_t3232980021 *)(__this));
			goto IL_001e;
		}
	}
	{
		HashSet_1_t3535795091 * L_4 = (HashSet_1_t3535795091 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		((  void (*) (HashSet_1_t3535795091 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)(L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		G_B3_0 = L_4;
		G_B3_1 = ((Distinct_t3232980021 *)(G_B1_0));
		goto IL_0029;
	}

IL_001e:
	{
		DistinctObservable_1_t18876622 * L_5 = ___parent0;
		NullCheck(L_5);
		Il2CppObject* L_6 = (Il2CppObject*)L_5->get_comparer_2();
		HashSet_1_t3535795091 * L_7 = (HashSet_1_t3535795091 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		((  void (*) (HashSet_1_t3535795091 *, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)(L_7, (Il2CppObject*)L_6, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		G_B3_0 = L_7;
		G_B3_1 = ((Distinct_t3232980021 *)(G_B2_0));
	}

IL_0029:
	{
		NullCheck(G_B3_1);
		G_B3_1->set_hashSet_2(G_B3_0);
		return;
	}
}
// System.Void UniRx.Operators.DistinctObservable`1/Distinct<System.Object>::OnNext(T)
extern Il2CppClass* Il2CppObject_il2cpp_TypeInfo_var;
extern Il2CppClass* Exception_t1967233988_il2cpp_TypeInfo_var;
extern const uint32_t Distinct_OnNext_m2032766175_MetadataUsageId;
extern "C"  void Distinct_OnNext_m2032766175_gshared (Distinct_t3232980021 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Distinct_OnNext_m2032766175_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Il2CppObject * V_0 = NULL;
	bool V_1 = false;
	Exception_t1967233988 * V_2 = NULL;
	Il2CppObject * V_3 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Initobj (Il2CppObject_il2cpp_TypeInfo_var, (&V_3));
		Il2CppObject * L_0 = V_3;
		V_0 = (Il2CppObject *)L_0;
		V_1 = (bool)0;
	}

IL_000c:
	try
	{ // begin try (depth: 1)
		Il2CppObject * L_1 = ___value0;
		V_0 = (Il2CppObject *)L_1;
		HashSet_1_t3535795091 * L_2 = (HashSet_1_t3535795091 *)__this->get_hashSet_2();
		Il2CppObject * L_3 = V_0;
		NullCheck((HashSet_1_t3535795091 *)L_2);
		bool L_4 = ((  bool (*) (HashSet_1_t3535795091 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->method)((HashSet_1_t3535795091 *)L_2, (Il2CppObject *)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		V_1 = (bool)L_4;
		goto IL_0045;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1967233988 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Exception_t1967233988_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_0020;
		throw e;
	}

CATCH_0020:
	{ // begin catch(System.Exception)
		{
			V_2 = (Exception_t1967233988 *)((Exception_t1967233988 *)__exception_local);
		}

IL_0021:
		try
		{ // begin try (depth: 2)
			Il2CppObject* L_5 = (Il2CppObject*)((OperatorObserverBase_2_t1187768149 *)__this)->get_observer_0();
			il2cpp_codegen_memory_barrier();
			Exception_t1967233988 * L_6 = V_2;
			NullCheck((Il2CppObject*)L_5);
			InterfaceActionInvoker1< Exception_t1967233988 * >::Invoke(1 /* System.Void UniRx.IObserver`1<System.Object>::OnError(System.Exception) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6), (Il2CppObject*)L_5, (Exception_t1967233988 *)L_6);
			IL2CPP_LEAVE(0x3B, FINALLY_0034);
		} // end try (depth: 2)
		catch(Il2CppExceptionWrapper& e)
		{
			__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
			goto FINALLY_0034;
		}

FINALLY_0034:
		{ // begin finally (depth: 2)
			NullCheck((OperatorObserverBase_2_t1187768149 *)__this);
			VirtActionInvoker0::Invoke(4 /* System.Void UniRx.Operators.OperatorObserverBase`2<System.Object,System.Object>::Dispose() */, (OperatorObserverBase_2_t1187768149 *)__this);
			IL2CPP_END_FINALLY(52)
		} // end finally (depth: 2)
		IL2CPP_CLEANUP(52)
		{
			IL2CPP_JUMP_TBL(0x3B, IL_003b)
			IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
		}

IL_003b:
		{
			goto IL_0059;
		}

IL_0040:
		{
			; // IL_0040: leave IL_0045
		}
	} // end catch (depth: 1)

IL_0045:
	{
		bool L_7 = V_1;
		if (!L_7)
		{
			goto IL_0059;
		}
	}
	{
		Il2CppObject* L_8 = (Il2CppObject*)((OperatorObserverBase_2_t1187768149 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		Il2CppObject * L_9 = ___value0;
		NullCheck((Il2CppObject*)L_8);
		InterfaceActionInvoker1< Il2CppObject * >::Invoke(2 /* System.Void UniRx.IObserver`1<System.Object>::OnNext(T) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6), (Il2CppObject*)L_8, (Il2CppObject *)L_9);
	}

IL_0059:
	{
		return;
	}
}
// System.Void UniRx.Operators.DistinctObservable`1/Distinct<System.Object>::OnError(System.Exception)
extern "C"  void Distinct_OnError_m466487790_gshared (Distinct_t3232980021 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method)
{
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		Il2CppObject* L_0 = (Il2CppObject*)((OperatorObserverBase_2_t1187768149 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		Exception_t1967233988 * L_1 = ___error0;
		NullCheck((Il2CppObject*)L_0);
		InterfaceActionInvoker1< Exception_t1967233988 * >::Invoke(1 /* System.Void UniRx.IObserver`1<System.Object>::OnError(System.Exception) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6), (Il2CppObject*)L_0, (Exception_t1967233988 *)L_1);
		IL2CPP_LEAVE(0x1A, FINALLY_0013);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0013;
	}

FINALLY_0013:
	{ // begin finally (depth: 1)
		NullCheck((OperatorObserverBase_2_t1187768149 *)__this);
		VirtActionInvoker0::Invoke(4 /* System.Void UniRx.Operators.OperatorObserverBase`2<System.Object,System.Object>::Dispose() */, (OperatorObserverBase_2_t1187768149 *)__this);
		IL2CPP_END_FINALLY(19)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(19)
	{
		IL2CPP_JUMP_TBL(0x1A, IL_001a)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_001a:
	{
		return;
	}
}
// System.Void UniRx.Operators.DistinctObservable`1/Distinct<System.Object>::OnCompleted()
extern "C"  void Distinct_OnCompleted_m3537482305_gshared (Distinct_t3232980021 * __this, const MethodInfo* method)
{
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		Il2CppObject* L_0 = (Il2CppObject*)((OperatorObserverBase_2_t1187768149 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		NullCheck((Il2CppObject*)L_0);
		InterfaceActionInvoker0::Invoke(0 /* System.Void UniRx.IObserver`1<System.Object>::OnCompleted() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6), (Il2CppObject*)L_0);
		IL2CPP_LEAVE(0x19, FINALLY_0012);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0012;
	}

FINALLY_0012:
	{ // begin finally (depth: 1)
		NullCheck((OperatorObserverBase_2_t1187768149 *)__this);
		VirtActionInvoker0::Invoke(4 /* System.Void UniRx.Operators.OperatorObserverBase`2<System.Object,System.Object>::Dispose() */, (OperatorObserverBase_2_t1187768149 *)__this);
		IL2CPP_END_FINALLY(18)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(18)
	{
		IL2CPP_JUMP_TBL(0x19, IL_0019)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0019:
	{
		return;
	}
}
// System.Void UniRx.Operators.DistinctObservable`1<System.Object>::.ctor(UniRx.IObservable`1<T>,System.Collections.Generic.IEqualityComparer`1<T>)
extern "C"  void DistinctObservable_1__ctor_m1443669977_gshared (DistinctObservable_1_t18876622 * __this, Il2CppObject* ___source0, Il2CppObject* ___comparer1, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = ___source0;
		bool L_1 = ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)(NULL /*static, unused*/, (Il2CppObject*)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		NullCheck((OperatorObservableBase_1_t4196218687 *)__this);
		((  void (*) (OperatorObservableBase_1_t4196218687 *, bool, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)((OperatorObservableBase_1_t4196218687 *)__this, (bool)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		Il2CppObject* L_2 = ___source0;
		__this->set_source_1(L_2);
		Il2CppObject* L_3 = ___comparer1;
		__this->set_comparer_2(L_3);
		return;
	}
}
// System.IDisposable UniRx.Operators.DistinctObservable`1<System.Object>::SubscribeCore(UniRx.IObserver`1<T>,System.IDisposable)
extern "C"  Il2CppObject * DistinctObservable_1_SubscribeCore_m2138390918_gshared (DistinctObservable_1_t18876622 * __this, Il2CppObject* ___observer0, Il2CppObject * ___cancel1, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = (Il2CppObject*)__this->get_source_1();
		Il2CppObject* L_1 = ___observer0;
		Il2CppObject * L_2 = ___cancel1;
		Distinct_t3232980021 * L_3 = (Distinct_t3232980021 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		((  void (*) (Distinct_t3232980021 *, DistinctObservable_1_t18876622 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)(L_3, (DistinctObservable_1_t18876622 *)__this, (Il2CppObject*)L_1, (Il2CppObject *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		NullCheck((Il2CppObject*)L_0);
		Il2CppObject * L_4 = InterfaceFuncInvoker1< Il2CppObject *, Il2CppObject* >::Invoke(0 /* System.IDisposable UniRx.IObservable`1<System.Object>::Subscribe(UniRx.IObserver`1<T>) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5), (Il2CppObject*)L_0, (Il2CppObject*)L_3);
		return L_4;
	}
}
// System.Void UniRx.Operators.DistinctObservable`2/Distinct<System.Object,System.Object>::.ctor(UniRx.Operators.DistinctObservable`2<T,TKey>,UniRx.IObserver`1<T>,System.IDisposable)
extern "C"  void Distinct__ctor_m630126341_gshared (Distinct_t1668326987 * __this, DistinctObservable_2_t2396543569 * ___parent0, Il2CppObject* ___observer1, Il2CppObject * ___cancel2, const MethodInfo* method)
{
	Distinct_t1668326987 * G_B2_0 = NULL;
	Distinct_t1668326987 * G_B1_0 = NULL;
	HashSet_1_t3535795091 * G_B3_0 = NULL;
	Distinct_t1668326987 * G_B3_1 = NULL;
	{
		Il2CppObject* L_0 = ___observer1;
		Il2CppObject * L_1 = ___cancel2;
		NullCheck((OperatorObserverBase_2_t1187768149 *)__this);
		((  void (*) (OperatorObserverBase_2_t1187768149 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((OperatorObserverBase_2_t1187768149 *)__this, (Il2CppObject*)L_0, (Il2CppObject *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		DistinctObservable_2_t2396543569 * L_2 = ___parent0;
		__this->set_parent_2(L_2);
		DistinctObservable_2_t2396543569 * L_3 = ___parent0;
		NullCheck(L_3);
		Il2CppObject* L_4 = (Il2CppObject*)L_3->get_comparer_2();
		G_B1_0 = ((Distinct_t1668326987 *)(__this));
		if (L_4)
		{
			G_B2_0 = ((Distinct_t1668326987 *)(__this));
			goto IL_0025;
		}
	}
	{
		HashSet_1_t3535795091 * L_5 = (HashSet_1_t3535795091 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		((  void (*) (HashSet_1_t3535795091 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)(L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		G_B3_0 = L_5;
		G_B3_1 = ((Distinct_t1668326987 *)(G_B1_0));
		goto IL_0030;
	}

IL_0025:
	{
		DistinctObservable_2_t2396543569 * L_6 = ___parent0;
		NullCheck(L_6);
		Il2CppObject* L_7 = (Il2CppObject*)L_6->get_comparer_2();
		HashSet_1_t3535795091 * L_8 = (HashSet_1_t3535795091 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		((  void (*) (HashSet_1_t3535795091 *, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)(L_8, (Il2CppObject*)L_7, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		G_B3_0 = L_8;
		G_B3_1 = ((Distinct_t1668326987 *)(G_B2_0));
	}

IL_0030:
	{
		NullCheck(G_B3_1);
		G_B3_1->set_hashSet_3(G_B3_0);
		return;
	}
}
// System.Void UniRx.Operators.DistinctObservable`2/Distinct<System.Object,System.Object>::OnNext(T)
extern Il2CppClass* Il2CppObject_il2cpp_TypeInfo_var;
extern Il2CppClass* Exception_t1967233988_il2cpp_TypeInfo_var;
extern const uint32_t Distinct_OnNext_m586424240_MetadataUsageId;
extern "C"  void Distinct_OnNext_m586424240_gshared (Distinct_t1668326987 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Distinct_OnNext_m586424240_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Il2CppObject * V_0 = NULL;
	bool V_1 = false;
	Exception_t1967233988 * V_2 = NULL;
	Il2CppObject * V_3 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Initobj (Il2CppObject_il2cpp_TypeInfo_var, (&V_3));
		Il2CppObject * L_0 = V_3;
		V_0 = (Il2CppObject *)L_0;
		V_1 = (bool)0;
	}

IL_000c:
	try
	{ // begin try (depth: 1)
		DistinctObservable_2_t2396543569 * L_1 = (DistinctObservable_2_t2396543569 *)__this->get_parent_2();
		NullCheck(L_1);
		Func_2_t2135783352 * L_2 = (Func_2_t2135783352 *)L_1->get_keySelector_3();
		Il2CppObject * L_3 = ___value0;
		NullCheck((Func_2_t2135783352 *)L_2);
		Il2CppObject * L_4 = ((  Il2CppObject * (*) (Func_2_t2135783352 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->method)((Func_2_t2135783352 *)L_2, (Il2CppObject *)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		V_0 = (Il2CppObject *)L_4;
		HashSet_1_t3535795091 * L_5 = (HashSet_1_t3535795091 *)__this->get_hashSet_3();
		Il2CppObject * L_6 = V_0;
		NullCheck((HashSet_1_t3535795091 *)L_5);
		bool L_7 = ((  bool (*) (HashSet_1_t3535795091 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6)->method)((HashSet_1_t3535795091 *)L_5, (Il2CppObject *)L_6, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		V_1 = (bool)L_7;
		goto IL_0055;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1967233988 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Exception_t1967233988_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_0030;
		throw e;
	}

CATCH_0030:
	{ // begin catch(System.Exception)
		{
			V_2 = (Exception_t1967233988 *)((Exception_t1967233988 *)__exception_local);
		}

IL_0031:
		try
		{ // begin try (depth: 2)
			Il2CppObject* L_8 = (Il2CppObject*)((OperatorObserverBase_2_t1187768149 *)__this)->get_observer_0();
			il2cpp_codegen_memory_barrier();
			Exception_t1967233988 * L_9 = V_2;
			NullCheck((Il2CppObject*)L_8);
			InterfaceActionInvoker1< Exception_t1967233988 * >::Invoke(1 /* System.Void UniRx.IObserver`1<System.Object>::OnError(System.Exception) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7), (Il2CppObject*)L_8, (Exception_t1967233988 *)L_9);
			IL2CPP_LEAVE(0x4B, FINALLY_0044);
		} // end try (depth: 2)
		catch(Il2CppExceptionWrapper& e)
		{
			__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
			goto FINALLY_0044;
		}

FINALLY_0044:
		{ // begin finally (depth: 2)
			NullCheck((OperatorObserverBase_2_t1187768149 *)__this);
			VirtActionInvoker0::Invoke(4 /* System.Void UniRx.Operators.OperatorObserverBase`2<System.Object,System.Object>::Dispose() */, (OperatorObserverBase_2_t1187768149 *)__this);
			IL2CPP_END_FINALLY(68)
		} // end finally (depth: 2)
		IL2CPP_CLEANUP(68)
		{
			IL2CPP_JUMP_TBL(0x4B, IL_004b)
			IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
		}

IL_004b:
		{
			goto IL_0069;
		}

IL_0050:
		{
			; // IL_0050: leave IL_0055
		}
	} // end catch (depth: 1)

IL_0055:
	{
		bool L_10 = V_1;
		if (!L_10)
		{
			goto IL_0069;
		}
	}
	{
		Il2CppObject* L_11 = (Il2CppObject*)((OperatorObserverBase_2_t1187768149 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		Il2CppObject * L_12 = ___value0;
		NullCheck((Il2CppObject*)L_11);
		InterfaceActionInvoker1< Il2CppObject * >::Invoke(2 /* System.Void UniRx.IObserver`1<System.Object>::OnNext(T) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7), (Il2CppObject*)L_11, (Il2CppObject *)L_12);
	}

IL_0069:
	{
		return;
	}
}
// System.Void UniRx.Operators.DistinctObservable`2/Distinct<System.Object,System.Object>::OnError(System.Exception)
extern "C"  void Distinct_OnError_m2900548287_gshared (Distinct_t1668326987 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method)
{
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		Il2CppObject* L_0 = (Il2CppObject*)((OperatorObserverBase_2_t1187768149 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		Exception_t1967233988 * L_1 = ___error0;
		NullCheck((Il2CppObject*)L_0);
		InterfaceActionInvoker1< Exception_t1967233988 * >::Invoke(1 /* System.Void UniRx.IObserver`1<System.Object>::OnError(System.Exception) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7), (Il2CppObject*)L_0, (Exception_t1967233988 *)L_1);
		IL2CPP_LEAVE(0x1A, FINALLY_0013);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0013;
	}

FINALLY_0013:
	{ // begin finally (depth: 1)
		NullCheck((OperatorObserverBase_2_t1187768149 *)__this);
		VirtActionInvoker0::Invoke(4 /* System.Void UniRx.Operators.OperatorObserverBase`2<System.Object,System.Object>::Dispose() */, (OperatorObserverBase_2_t1187768149 *)__this);
		IL2CPP_END_FINALLY(19)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(19)
	{
		IL2CPP_JUMP_TBL(0x1A, IL_001a)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_001a:
	{
		return;
	}
}
// System.Void UniRx.Operators.DistinctObservable`2/Distinct<System.Object,System.Object>::OnCompleted()
extern "C"  void Distinct_OnCompleted_m2626450578_gshared (Distinct_t1668326987 * __this, const MethodInfo* method)
{
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		Il2CppObject* L_0 = (Il2CppObject*)((OperatorObserverBase_2_t1187768149 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		NullCheck((Il2CppObject*)L_0);
		InterfaceActionInvoker0::Invoke(0 /* System.Void UniRx.IObserver`1<System.Object>::OnCompleted() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7), (Il2CppObject*)L_0);
		IL2CPP_LEAVE(0x19, FINALLY_0012);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0012;
	}

FINALLY_0012:
	{ // begin finally (depth: 1)
		NullCheck((OperatorObserverBase_2_t1187768149 *)__this);
		VirtActionInvoker0::Invoke(4 /* System.Void UniRx.Operators.OperatorObserverBase`2<System.Object,System.Object>::Dispose() */, (OperatorObserverBase_2_t1187768149 *)__this);
		IL2CPP_END_FINALLY(18)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(18)
	{
		IL2CPP_JUMP_TBL(0x19, IL_0019)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0019:
	{
		return;
	}
}
// System.Void UniRx.Operators.DistinctObservable`2<System.Object,System.Object>::.ctor(UniRx.IObservable`1<T>,System.Func`2<T,TKey>,System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C"  void DistinctObservable_2__ctor_m2459079185_gshared (DistinctObservable_2_t2396543569 * __this, Il2CppObject* ___source0, Func_2_t2135783352 * ___keySelector1, Il2CppObject* ___comparer2, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = ___source0;
		bool L_1 = ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)(NULL /*static, unused*/, (Il2CppObject*)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		NullCheck((OperatorObservableBase_1_t4196218687 *)__this);
		((  void (*) (OperatorObservableBase_1_t4196218687 *, bool, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)((OperatorObservableBase_1_t4196218687 *)__this, (bool)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		Il2CppObject* L_2 = ___source0;
		__this->set_source_1(L_2);
		Il2CppObject* L_3 = ___comparer2;
		__this->set_comparer_2(L_3);
		Func_2_t2135783352 * L_4 = ___keySelector1;
		__this->set_keySelector_3(L_4);
		return;
	}
}
// System.IDisposable UniRx.Operators.DistinctObservable`2<System.Object,System.Object>::SubscribeCore(UniRx.IObserver`1<T>,System.IDisposable)
extern "C"  Il2CppObject * DistinctObservable_2_SubscribeCore_m2477650739_gshared (DistinctObservable_2_t2396543569 * __this, Il2CppObject* ___observer0, Il2CppObject * ___cancel1, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = (Il2CppObject*)__this->get_source_1();
		Il2CppObject* L_1 = ___observer0;
		Il2CppObject * L_2 = ___cancel1;
		Distinct_t1668326987 * L_3 = (Distinct_t1668326987 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		((  void (*) (Distinct_t1668326987 *, DistinctObservable_2_t2396543569 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)(L_3, (DistinctObservable_2_t2396543569 *)__this, (Il2CppObject*)L_1, (Il2CppObject *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		NullCheck((Il2CppObject*)L_0);
		Il2CppObject * L_4 = InterfaceFuncInvoker1< Il2CppObject *, Il2CppObject* >::Invoke(0 /* System.IDisposable UniRx.IObservable`1<System.Object>::Subscribe(UniRx.IObserver`1<T>) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5), (Il2CppObject*)L_0, (Il2CppObject*)L_3);
		return L_4;
	}
}
// System.Void UniRx.Operators.DistinctUntilChangedObservable`1/DistinctUntilChanged<System.Boolean>::.ctor(UniRx.Operators.DistinctUntilChangedObservable`1<T>,UniRx.IObserver`1<T>,System.IDisposable)
extern Il2CppClass* Boolean_t211005341_il2cpp_TypeInfo_var;
extern const uint32_t DistinctUntilChanged__ctor_m3524137875_MetadataUsageId;
extern "C"  void DistinctUntilChanged__ctor_m3524137875_gshared (DistinctUntilChanged_t1676912132 * __this, DistinctUntilChangedObservable_1_t3199977885 * ___parent0, Il2CppObject* ___observer1, Il2CppObject * ___cancel2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DistinctUntilChanged__ctor_m3524137875_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	bool V_0 = false;
	{
		__this->set_isFirst_3((bool)1);
		Initobj (Boolean_t211005341_il2cpp_TypeInfo_var, (&V_0));
		bool L_0 = V_0;
		__this->set_prevKey_4(L_0);
		Il2CppObject* L_1 = ___observer1;
		Il2CppObject * L_2 = ___cancel2;
		NullCheck((OperatorObserverBase_2_t60103313 *)__this);
		((  void (*) (OperatorObserverBase_2_t60103313 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((OperatorObserverBase_2_t60103313 *)__this, (Il2CppObject*)L_1, (Il2CppObject *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		DistinctUntilChangedObservable_1_t3199977885 * L_3 = ___parent0;
		__this->set_parent_2(L_3);
		return;
	}
}
// System.Void UniRx.Operators.DistinctUntilChangedObservable`1/DistinctUntilChanged<System.Boolean>::OnNext(T)
extern Il2CppClass* Exception_t1967233988_il2cpp_TypeInfo_var;
extern const uint32_t DistinctUntilChanged_OnNext_m1542520834_MetadataUsageId;
extern "C"  void DistinctUntilChanged_OnNext_m1542520834_gshared (DistinctUntilChanged_t1676912132 * __this, bool ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DistinctUntilChanged_OnNext_m1542520834_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	bool V_0 = false;
	Exception_t1967233988 * V_1 = NULL;
	bool V_2 = false;
	Exception_t1967233988 * V_3 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		bool L_0 = ___value0;
		V_0 = (bool)L_0;
		goto IL_002c;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1967233988 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Exception_t1967233988_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_0007;
		throw e;
	}

CATCH_0007:
	{ // begin catch(System.Exception)
		{
			V_1 = (Exception_t1967233988 *)((Exception_t1967233988 *)__exception_local);
		}

IL_0008:
		try
		{ // begin try (depth: 2)
			Il2CppObject* L_1 = (Il2CppObject*)((OperatorObserverBase_2_t60103313 *)__this)->get_observer_0();
			il2cpp_codegen_memory_barrier();
			Exception_t1967233988 * L_2 = V_1;
			NullCheck((Il2CppObject*)L_1);
			InterfaceActionInvoker1< Exception_t1967233988 * >::Invoke(1 /* System.Void UniRx.IObserver`1<System.Boolean>::OnError(System.Exception) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), (Il2CppObject*)L_1, (Exception_t1967233988 *)L_2);
			IL2CPP_LEAVE(0x22, FINALLY_001b);
		} // end try (depth: 2)
		catch(Il2CppExceptionWrapper& e)
		{
			__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
			goto FINALLY_001b;
		}

FINALLY_001b:
		{ // begin finally (depth: 2)
			NullCheck((OperatorObserverBase_2_t60103313 *)__this);
			VirtActionInvoker0::Invoke(4 /* System.Void UniRx.Operators.OperatorObserverBase`2<System.Boolean,System.Boolean>::Dispose() */, (OperatorObserverBase_2_t60103313 *)__this);
			IL2CPP_END_FINALLY(27)
		} // end finally (depth: 2)
		IL2CPP_CLEANUP(27)
		{
			IL2CPP_JUMP_TBL(0x22, IL_0022)
			IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
		}

IL_0022:
		{
			goto IL_00a2;
		}

IL_0027:
		{
			; // IL_0027: leave IL_002c
		}
	} // end catch (depth: 1)

IL_002c:
	{
		V_2 = (bool)0;
		bool L_3 = (bool)__this->get_isFirst_3();
		if (!L_3)
		{
			goto IL_0045;
		}
	}
	{
		__this->set_isFirst_3((bool)0);
		goto IL_0087;
	}

IL_0045:
	try
	{ // begin try (depth: 1)
		DistinctUntilChangedObservable_1_t3199977885 * L_4 = (DistinctUntilChangedObservable_1_t3199977885 *)__this->get_parent_2();
		NullCheck(L_4);
		Il2CppObject* L_5 = (Il2CppObject*)L_4->get_comparer_2();
		bool L_6 = V_0;
		bool L_7 = (bool)__this->get_prevKey_4();
		NullCheck((Il2CppObject*)L_5);
		bool L_8 = InterfaceFuncInvoker2< bool, bool, bool >::Invoke(0 /* System.Boolean System.Collections.Generic.IEqualityComparer`1<System.Boolean>::Equals(!0,!0) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4), (Il2CppObject*)L_5, (bool)L_6, (bool)L_7);
		V_2 = (bool)L_8;
		goto IL_0087;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1967233988 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Exception_t1967233988_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_0062;
		throw e;
	}

CATCH_0062:
	{ // begin catch(System.Exception)
		{
			V_3 = (Exception_t1967233988 *)((Exception_t1967233988 *)__exception_local);
		}

IL_0063:
		try
		{ // begin try (depth: 2)
			Il2CppObject* L_9 = (Il2CppObject*)((OperatorObserverBase_2_t60103313 *)__this)->get_observer_0();
			il2cpp_codegen_memory_barrier();
			Exception_t1967233988 * L_10 = V_3;
			NullCheck((Il2CppObject*)L_9);
			InterfaceActionInvoker1< Exception_t1967233988 * >::Invoke(1 /* System.Void UniRx.IObserver`1<System.Boolean>::OnError(System.Exception) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), (Il2CppObject*)L_9, (Exception_t1967233988 *)L_10);
			IL2CPP_LEAVE(0x7D, FINALLY_0076);
		} // end try (depth: 2)
		catch(Il2CppExceptionWrapper& e)
		{
			__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
			goto FINALLY_0076;
		}

FINALLY_0076:
		{ // begin finally (depth: 2)
			NullCheck((OperatorObserverBase_2_t60103313 *)__this);
			VirtActionInvoker0::Invoke(4 /* System.Void UniRx.Operators.OperatorObserverBase`2<System.Boolean,System.Boolean>::Dispose() */, (OperatorObserverBase_2_t60103313 *)__this);
			IL2CPP_END_FINALLY(118)
		} // end finally (depth: 2)
		IL2CPP_CLEANUP(118)
		{
			IL2CPP_JUMP_TBL(0x7D, IL_007d)
			IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
		}

IL_007d:
		{
			goto IL_00a2;
		}

IL_0082:
		{
			; // IL_0082: leave IL_0087
		}
	} // end catch (depth: 1)

IL_0087:
	{
		bool L_11 = V_2;
		if (L_11)
		{
			goto IL_00a2;
		}
	}
	{
		bool L_12 = V_0;
		__this->set_prevKey_4(L_12);
		Il2CppObject* L_13 = (Il2CppObject*)((OperatorObserverBase_2_t60103313 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		bool L_14 = ___value0;
		NullCheck((Il2CppObject*)L_13);
		InterfaceActionInvoker1< bool >::Invoke(2 /* System.Void UniRx.IObserver`1<System.Boolean>::OnNext(T) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), (Il2CppObject*)L_13, (bool)L_14);
	}

IL_00a2:
	{
		return;
	}
}
// System.Void UniRx.Operators.DistinctUntilChangedObservable`1/DistinctUntilChanged<System.Boolean>::OnError(System.Exception)
extern "C"  void DistinctUntilChanged_OnError_m3867727633_gshared (DistinctUntilChanged_t1676912132 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method)
{
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		Il2CppObject* L_0 = (Il2CppObject*)((OperatorObserverBase_2_t60103313 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		Exception_t1967233988 * L_1 = ___error0;
		NullCheck((Il2CppObject*)L_0);
		InterfaceActionInvoker1< Exception_t1967233988 * >::Invoke(1 /* System.Void UniRx.IObserver`1<System.Boolean>::OnError(System.Exception) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), (Il2CppObject*)L_0, (Exception_t1967233988 *)L_1);
		IL2CPP_LEAVE(0x1A, FINALLY_0013);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0013;
	}

FINALLY_0013:
	{ // begin finally (depth: 1)
		NullCheck((OperatorObserverBase_2_t60103313 *)__this);
		VirtActionInvoker0::Invoke(4 /* System.Void UniRx.Operators.OperatorObserverBase`2<System.Boolean,System.Boolean>::Dispose() */, (OperatorObserverBase_2_t60103313 *)__this);
		IL2CPP_END_FINALLY(19)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(19)
	{
		IL2CPP_JUMP_TBL(0x1A, IL_001a)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_001a:
	{
		return;
	}
}
// System.Void UniRx.Operators.DistinctUntilChangedObservable`1/DistinctUntilChanged<System.Boolean>::OnCompleted()
extern "C"  void DistinctUntilChanged_OnCompleted_m1352457188_gshared (DistinctUntilChanged_t1676912132 * __this, const MethodInfo* method)
{
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		Il2CppObject* L_0 = (Il2CppObject*)((OperatorObserverBase_2_t60103313 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		NullCheck((Il2CppObject*)L_0);
		InterfaceActionInvoker0::Invoke(0 /* System.Void UniRx.IObserver`1<System.Boolean>::OnCompleted() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), (Il2CppObject*)L_0);
		IL2CPP_LEAVE(0x19, FINALLY_0012);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0012;
	}

FINALLY_0012:
	{ // begin finally (depth: 1)
		NullCheck((OperatorObserverBase_2_t60103313 *)__this);
		VirtActionInvoker0::Invoke(4 /* System.Void UniRx.Operators.OperatorObserverBase`2<System.Boolean,System.Boolean>::Dispose() */, (OperatorObserverBase_2_t60103313 *)__this);
		IL2CPP_END_FINALLY(18)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(18)
	{
		IL2CPP_JUMP_TBL(0x19, IL_0019)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0019:
	{
		return;
	}
}
// System.Void UniRx.Operators.DistinctUntilChangedObservable`1/DistinctUntilChanged<System.Int32>::.ctor(UniRx.Operators.DistinctUntilChangedObservable`1<T>,UniRx.IObserver`1<T>,System.IDisposable)
extern Il2CppClass* Int32_t2847414787_il2cpp_TypeInfo_var;
extern const uint32_t DistinctUntilChanged__ctor_m647915949_MetadataUsageId;
extern "C"  void DistinctUntilChanged__ctor_m647915949_gshared (DistinctUntilChanged_t18354282 * __this, DistinctUntilChangedObservable_1_t1541420035 * ___parent0, Il2CppObject* ___observer1, Il2CppObject * ___cancel2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DistinctUntilChanged__ctor_m647915949_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	{
		__this->set_isFirst_3((bool)1);
		Initobj (Int32_t2847414787_il2cpp_TypeInfo_var, (&V_0));
		int32_t L_0 = V_0;
		__this->set_prevKey_4(L_0);
		Il2CppObject* L_1 = ___observer1;
		Il2CppObject * L_2 = ___cancel2;
		NullCheck((OperatorObserverBase_2_t701568569 *)__this);
		((  void (*) (OperatorObserverBase_2_t701568569 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((OperatorObserverBase_2_t701568569 *)__this, (Il2CppObject*)L_1, (Il2CppObject *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		DistinctUntilChangedObservable_1_t1541420035 * L_3 = ___parent0;
		__this->set_parent_2(L_3);
		return;
	}
}
// System.Void UniRx.Operators.DistinctUntilChangedObservable`1/DistinctUntilChanged<System.Int32>::OnNext(T)
extern Il2CppClass* Exception_t1967233988_il2cpp_TypeInfo_var;
extern const uint32_t DistinctUntilChanged_OnNext_m1535557800_MetadataUsageId;
extern "C"  void DistinctUntilChanged_OnNext_m1535557800_gshared (DistinctUntilChanged_t18354282 * __this, int32_t ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DistinctUntilChanged_OnNext_m1535557800_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	Exception_t1967233988 * V_1 = NULL;
	bool V_2 = false;
	Exception_t1967233988 * V_3 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		int32_t L_0 = ___value0;
		V_0 = (int32_t)L_0;
		goto IL_002c;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1967233988 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Exception_t1967233988_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_0007;
		throw e;
	}

CATCH_0007:
	{ // begin catch(System.Exception)
		{
			V_1 = (Exception_t1967233988 *)((Exception_t1967233988 *)__exception_local);
		}

IL_0008:
		try
		{ // begin try (depth: 2)
			Il2CppObject* L_1 = (Il2CppObject*)((OperatorObserverBase_2_t701568569 *)__this)->get_observer_0();
			il2cpp_codegen_memory_barrier();
			Exception_t1967233988 * L_2 = V_1;
			NullCheck((Il2CppObject*)L_1);
			InterfaceActionInvoker1< Exception_t1967233988 * >::Invoke(1 /* System.Void UniRx.IObserver`1<System.Int32>::OnError(System.Exception) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), (Il2CppObject*)L_1, (Exception_t1967233988 *)L_2);
			IL2CPP_LEAVE(0x22, FINALLY_001b);
		} // end try (depth: 2)
		catch(Il2CppExceptionWrapper& e)
		{
			__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
			goto FINALLY_001b;
		}

FINALLY_001b:
		{ // begin finally (depth: 2)
			NullCheck((OperatorObserverBase_2_t701568569 *)__this);
			VirtActionInvoker0::Invoke(4 /* System.Void UniRx.Operators.OperatorObserverBase`2<System.Int32,System.Int32>::Dispose() */, (OperatorObserverBase_2_t701568569 *)__this);
			IL2CPP_END_FINALLY(27)
		} // end finally (depth: 2)
		IL2CPP_CLEANUP(27)
		{
			IL2CPP_JUMP_TBL(0x22, IL_0022)
			IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
		}

IL_0022:
		{
			goto IL_00a2;
		}

IL_0027:
		{
			; // IL_0027: leave IL_002c
		}
	} // end catch (depth: 1)

IL_002c:
	{
		V_2 = (bool)0;
		bool L_3 = (bool)__this->get_isFirst_3();
		if (!L_3)
		{
			goto IL_0045;
		}
	}
	{
		__this->set_isFirst_3((bool)0);
		goto IL_0087;
	}

IL_0045:
	try
	{ // begin try (depth: 1)
		DistinctUntilChangedObservable_1_t1541420035 * L_4 = (DistinctUntilChangedObservable_1_t1541420035 *)__this->get_parent_2();
		NullCheck(L_4);
		Il2CppObject* L_5 = (Il2CppObject*)L_4->get_comparer_2();
		int32_t L_6 = V_0;
		int32_t L_7 = (int32_t)__this->get_prevKey_4();
		NullCheck((Il2CppObject*)L_5);
		bool L_8 = InterfaceFuncInvoker2< bool, int32_t, int32_t >::Invoke(0 /* System.Boolean System.Collections.Generic.IEqualityComparer`1<System.Int32>::Equals(!0,!0) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4), (Il2CppObject*)L_5, (int32_t)L_6, (int32_t)L_7);
		V_2 = (bool)L_8;
		goto IL_0087;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1967233988 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Exception_t1967233988_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_0062;
		throw e;
	}

CATCH_0062:
	{ // begin catch(System.Exception)
		{
			V_3 = (Exception_t1967233988 *)((Exception_t1967233988 *)__exception_local);
		}

IL_0063:
		try
		{ // begin try (depth: 2)
			Il2CppObject* L_9 = (Il2CppObject*)((OperatorObserverBase_2_t701568569 *)__this)->get_observer_0();
			il2cpp_codegen_memory_barrier();
			Exception_t1967233988 * L_10 = V_3;
			NullCheck((Il2CppObject*)L_9);
			InterfaceActionInvoker1< Exception_t1967233988 * >::Invoke(1 /* System.Void UniRx.IObserver`1<System.Int32>::OnError(System.Exception) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), (Il2CppObject*)L_9, (Exception_t1967233988 *)L_10);
			IL2CPP_LEAVE(0x7D, FINALLY_0076);
		} // end try (depth: 2)
		catch(Il2CppExceptionWrapper& e)
		{
			__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
			goto FINALLY_0076;
		}

FINALLY_0076:
		{ // begin finally (depth: 2)
			NullCheck((OperatorObserverBase_2_t701568569 *)__this);
			VirtActionInvoker0::Invoke(4 /* System.Void UniRx.Operators.OperatorObserverBase`2<System.Int32,System.Int32>::Dispose() */, (OperatorObserverBase_2_t701568569 *)__this);
			IL2CPP_END_FINALLY(118)
		} // end finally (depth: 2)
		IL2CPP_CLEANUP(118)
		{
			IL2CPP_JUMP_TBL(0x7D, IL_007d)
			IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
		}

IL_007d:
		{
			goto IL_00a2;
		}

IL_0082:
		{
			; // IL_0082: leave IL_0087
		}
	} // end catch (depth: 1)

IL_0087:
	{
		bool L_11 = V_2;
		if (L_11)
		{
			goto IL_00a2;
		}
	}
	{
		int32_t L_12 = V_0;
		__this->set_prevKey_4(L_12);
		Il2CppObject* L_13 = (Il2CppObject*)((OperatorObserverBase_2_t701568569 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		int32_t L_14 = ___value0;
		NullCheck((Il2CppObject*)L_13);
		InterfaceActionInvoker1< int32_t >::Invoke(2 /* System.Void UniRx.IObserver`1<System.Int32>::OnNext(T) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), (Il2CppObject*)L_13, (int32_t)L_14);
	}

IL_00a2:
	{
		return;
	}
}
// System.Void UniRx.Operators.DistinctUntilChangedObservable`1/DistinctUntilChanged<System.Int32>::OnError(System.Exception)
extern "C"  void DistinctUntilChanged_OnError_m3502541751_gshared (DistinctUntilChanged_t18354282 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method)
{
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		Il2CppObject* L_0 = (Il2CppObject*)((OperatorObserverBase_2_t701568569 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		Exception_t1967233988 * L_1 = ___error0;
		NullCheck((Il2CppObject*)L_0);
		InterfaceActionInvoker1< Exception_t1967233988 * >::Invoke(1 /* System.Void UniRx.IObserver`1<System.Int32>::OnError(System.Exception) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), (Il2CppObject*)L_0, (Exception_t1967233988 *)L_1);
		IL2CPP_LEAVE(0x1A, FINALLY_0013);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0013;
	}

FINALLY_0013:
	{ // begin finally (depth: 1)
		NullCheck((OperatorObserverBase_2_t701568569 *)__this);
		VirtActionInvoker0::Invoke(4 /* System.Void UniRx.Operators.OperatorObserverBase`2<System.Int32,System.Int32>::Dispose() */, (OperatorObserverBase_2_t701568569 *)__this);
		IL2CPP_END_FINALLY(19)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(19)
	{
		IL2CPP_JUMP_TBL(0x1A, IL_001a)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_001a:
	{
		return;
	}
}
// System.Void UniRx.Operators.DistinctUntilChangedObservable`1/DistinctUntilChanged<System.Int32>::OnCompleted()
extern "C"  void DistinctUntilChanged_OnCompleted_m410376586_gshared (DistinctUntilChanged_t18354282 * __this, const MethodInfo* method)
{
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		Il2CppObject* L_0 = (Il2CppObject*)((OperatorObserverBase_2_t701568569 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		NullCheck((Il2CppObject*)L_0);
		InterfaceActionInvoker0::Invoke(0 /* System.Void UniRx.IObserver`1<System.Int32>::OnCompleted() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), (Il2CppObject*)L_0);
		IL2CPP_LEAVE(0x19, FINALLY_0012);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0012;
	}

FINALLY_0012:
	{ // begin finally (depth: 1)
		NullCheck((OperatorObserverBase_2_t701568569 *)__this);
		VirtActionInvoker0::Invoke(4 /* System.Void UniRx.Operators.OperatorObserverBase`2<System.Int32,System.Int32>::Dispose() */, (OperatorObserverBase_2_t701568569 *)__this);
		IL2CPP_END_FINALLY(18)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(18)
	{
		IL2CPP_JUMP_TBL(0x19, IL_0019)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0019:
	{
		return;
	}
}
// System.Void UniRx.Operators.DistinctUntilChangedObservable`1/DistinctUntilChanged<System.Object>::.ctor(UniRx.Operators.DistinctUntilChangedObservable`1<T>,UniRx.IObserver`1<T>,System.IDisposable)
extern Il2CppClass* Il2CppObject_il2cpp_TypeInfo_var;
extern const uint32_t DistinctUntilChanged__ctor_m351372310_MetadataUsageId;
extern "C"  void DistinctUntilChanged__ctor_m351372310_gshared (DistinctUntilChanged_t2303013211 * __this, DistinctUntilChangedObservable_1_t3826078964 * ___parent0, Il2CppObject* ___observer1, Il2CppObject * ___cancel2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DistinctUntilChanged__ctor_m351372310_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Il2CppObject * V_0 = NULL;
	{
		__this->set_isFirst_3((bool)1);
		Initobj (Il2CppObject_il2cpp_TypeInfo_var, (&V_0));
		Il2CppObject * L_0 = V_0;
		__this->set_prevKey_4(L_0);
		Il2CppObject* L_1 = ___observer1;
		Il2CppObject * L_2 = ___cancel2;
		NullCheck((OperatorObserverBase_2_t1187768149 *)__this);
		((  void (*) (OperatorObserverBase_2_t1187768149 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((OperatorObserverBase_2_t1187768149 *)__this, (Il2CppObject*)L_1, (Il2CppObject *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		DistinctUntilChangedObservable_1_t3826078964 * L_3 = ___parent0;
		__this->set_parent_2(L_3);
		return;
	}
}
// System.Void UniRx.Operators.DistinctUntilChangedObservable`1/DistinctUntilChanged<System.Object>::OnNext(T)
extern Il2CppClass* Exception_t1967233988_il2cpp_TypeInfo_var;
extern const uint32_t DistinctUntilChanged_OnNext_m1826660895_MetadataUsageId;
extern "C"  void DistinctUntilChanged_OnNext_m1826660895_gshared (DistinctUntilChanged_t2303013211 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DistinctUntilChanged_OnNext_m1826660895_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Il2CppObject * V_0 = NULL;
	Exception_t1967233988 * V_1 = NULL;
	bool V_2 = false;
	Exception_t1967233988 * V_3 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		Il2CppObject * L_0 = ___value0;
		V_0 = (Il2CppObject *)L_0;
		goto IL_002c;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1967233988 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Exception_t1967233988_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_0007;
		throw e;
	}

CATCH_0007:
	{ // begin catch(System.Exception)
		{
			V_1 = (Exception_t1967233988 *)((Exception_t1967233988 *)__exception_local);
		}

IL_0008:
		try
		{ // begin try (depth: 2)
			Il2CppObject* L_1 = (Il2CppObject*)((OperatorObserverBase_2_t1187768149 *)__this)->get_observer_0();
			il2cpp_codegen_memory_barrier();
			Exception_t1967233988 * L_2 = V_1;
			NullCheck((Il2CppObject*)L_1);
			InterfaceActionInvoker1< Exception_t1967233988 * >::Invoke(1 /* System.Void UniRx.IObserver`1<System.Object>::OnError(System.Exception) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), (Il2CppObject*)L_1, (Exception_t1967233988 *)L_2);
			IL2CPP_LEAVE(0x22, FINALLY_001b);
		} // end try (depth: 2)
		catch(Il2CppExceptionWrapper& e)
		{
			__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
			goto FINALLY_001b;
		}

FINALLY_001b:
		{ // begin finally (depth: 2)
			NullCheck((OperatorObserverBase_2_t1187768149 *)__this);
			VirtActionInvoker0::Invoke(4 /* System.Void UniRx.Operators.OperatorObserverBase`2<System.Object,System.Object>::Dispose() */, (OperatorObserverBase_2_t1187768149 *)__this);
			IL2CPP_END_FINALLY(27)
		} // end finally (depth: 2)
		IL2CPP_CLEANUP(27)
		{
			IL2CPP_JUMP_TBL(0x22, IL_0022)
			IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
		}

IL_0022:
		{
			goto IL_00a2;
		}

IL_0027:
		{
			; // IL_0027: leave IL_002c
		}
	} // end catch (depth: 1)

IL_002c:
	{
		V_2 = (bool)0;
		bool L_3 = (bool)__this->get_isFirst_3();
		if (!L_3)
		{
			goto IL_0045;
		}
	}
	{
		__this->set_isFirst_3((bool)0);
		goto IL_0087;
	}

IL_0045:
	try
	{ // begin try (depth: 1)
		DistinctUntilChangedObservable_1_t3826078964 * L_4 = (DistinctUntilChangedObservable_1_t3826078964 *)__this->get_parent_2();
		NullCheck(L_4);
		Il2CppObject* L_5 = (Il2CppObject*)L_4->get_comparer_2();
		Il2CppObject * L_6 = V_0;
		Il2CppObject * L_7 = (Il2CppObject *)__this->get_prevKey_4();
		NullCheck((Il2CppObject*)L_5);
		bool L_8 = InterfaceFuncInvoker2< bool, Il2CppObject *, Il2CppObject * >::Invoke(0 /* System.Boolean System.Collections.Generic.IEqualityComparer`1<System.Object>::Equals(!0,!0) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4), (Il2CppObject*)L_5, (Il2CppObject *)L_6, (Il2CppObject *)L_7);
		V_2 = (bool)L_8;
		goto IL_0087;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1967233988 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Exception_t1967233988_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_0062;
		throw e;
	}

CATCH_0062:
	{ // begin catch(System.Exception)
		{
			V_3 = (Exception_t1967233988 *)((Exception_t1967233988 *)__exception_local);
		}

IL_0063:
		try
		{ // begin try (depth: 2)
			Il2CppObject* L_9 = (Il2CppObject*)((OperatorObserverBase_2_t1187768149 *)__this)->get_observer_0();
			il2cpp_codegen_memory_barrier();
			Exception_t1967233988 * L_10 = V_3;
			NullCheck((Il2CppObject*)L_9);
			InterfaceActionInvoker1< Exception_t1967233988 * >::Invoke(1 /* System.Void UniRx.IObserver`1<System.Object>::OnError(System.Exception) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), (Il2CppObject*)L_9, (Exception_t1967233988 *)L_10);
			IL2CPP_LEAVE(0x7D, FINALLY_0076);
		} // end try (depth: 2)
		catch(Il2CppExceptionWrapper& e)
		{
			__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
			goto FINALLY_0076;
		}

FINALLY_0076:
		{ // begin finally (depth: 2)
			NullCheck((OperatorObserverBase_2_t1187768149 *)__this);
			VirtActionInvoker0::Invoke(4 /* System.Void UniRx.Operators.OperatorObserverBase`2<System.Object,System.Object>::Dispose() */, (OperatorObserverBase_2_t1187768149 *)__this);
			IL2CPP_END_FINALLY(118)
		} // end finally (depth: 2)
		IL2CPP_CLEANUP(118)
		{
			IL2CPP_JUMP_TBL(0x7D, IL_007d)
			IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
		}

IL_007d:
		{
			goto IL_00a2;
		}

IL_0082:
		{
			; // IL_0082: leave IL_0087
		}
	} // end catch (depth: 1)

IL_0087:
	{
		bool L_11 = V_2;
		if (L_11)
		{
			goto IL_00a2;
		}
	}
	{
		Il2CppObject * L_12 = V_0;
		__this->set_prevKey_4(L_12);
		Il2CppObject* L_13 = (Il2CppObject*)((OperatorObserverBase_2_t1187768149 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		Il2CppObject * L_14 = ___value0;
		NullCheck((Il2CppObject*)L_13);
		InterfaceActionInvoker1< Il2CppObject * >::Invoke(2 /* System.Void UniRx.IObserver`1<System.Object>::OnNext(T) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), (Il2CppObject*)L_13, (Il2CppObject *)L_14);
	}

IL_00a2:
	{
		return;
	}
}
// System.Void UniRx.Operators.DistinctUntilChangedObservable`1/DistinctUntilChanged<System.Object>::OnError(System.Exception)
extern "C"  void DistinctUntilChanged_OnError_m3677527854_gshared (DistinctUntilChanged_t2303013211 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method)
{
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		Il2CppObject* L_0 = (Il2CppObject*)((OperatorObserverBase_2_t1187768149 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		Exception_t1967233988 * L_1 = ___error0;
		NullCheck((Il2CppObject*)L_0);
		InterfaceActionInvoker1< Exception_t1967233988 * >::Invoke(1 /* System.Void UniRx.IObserver`1<System.Object>::OnError(System.Exception) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), (Il2CppObject*)L_0, (Exception_t1967233988 *)L_1);
		IL2CPP_LEAVE(0x1A, FINALLY_0013);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0013;
	}

FINALLY_0013:
	{ // begin finally (depth: 1)
		NullCheck((OperatorObserverBase_2_t1187768149 *)__this);
		VirtActionInvoker0::Invoke(4 /* System.Void UniRx.Operators.OperatorObserverBase`2<System.Object,System.Object>::Dispose() */, (OperatorObserverBase_2_t1187768149 *)__this);
		IL2CPP_END_FINALLY(19)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(19)
	{
		IL2CPP_JUMP_TBL(0x1A, IL_001a)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_001a:
	{
		return;
	}
}
// System.Void UniRx.Operators.DistinctUntilChangedObservable`1/DistinctUntilChanged<System.Object>::OnCompleted()
extern "C"  void DistinctUntilChanged_OnCompleted_m1048848257_gshared (DistinctUntilChanged_t2303013211 * __this, const MethodInfo* method)
{
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		Il2CppObject* L_0 = (Il2CppObject*)((OperatorObserverBase_2_t1187768149 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		NullCheck((Il2CppObject*)L_0);
		InterfaceActionInvoker0::Invoke(0 /* System.Void UniRx.IObserver`1<System.Object>::OnCompleted() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), (Il2CppObject*)L_0);
		IL2CPP_LEAVE(0x19, FINALLY_0012);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0012;
	}

FINALLY_0012:
	{ // begin finally (depth: 1)
		NullCheck((OperatorObserverBase_2_t1187768149 *)__this);
		VirtActionInvoker0::Invoke(4 /* System.Void UniRx.Operators.OperatorObserverBase`2<System.Object,System.Object>::Dispose() */, (OperatorObserverBase_2_t1187768149 *)__this);
		IL2CPP_END_FINALLY(18)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(18)
	{
		IL2CPP_JUMP_TBL(0x19, IL_0019)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0019:
	{
		return;
	}
}
// System.Void UniRx.Operators.DistinctUntilChangedObservable`1<System.Boolean>::.ctor(UniRx.IObservable`1<T>,System.Collections.Generic.IEqualityComparer`1<T>)
extern "C"  void DistinctUntilChangedObservable_1__ctor_m1803329520_gshared (DistinctUntilChangedObservable_1_t3199977885 * __this, Il2CppObject* ___source0, Il2CppObject* ___comparer1, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = ___source0;
		bool L_1 = ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)(NULL /*static, unused*/, (Il2CppObject*)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		NullCheck((OperatorObservableBase_1_t3570117608 *)__this);
		((  void (*) (OperatorObservableBase_1_t3570117608 *, bool, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)((OperatorObservableBase_1_t3570117608 *)__this, (bool)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		Il2CppObject* L_2 = ___source0;
		__this->set_source_1(L_2);
		Il2CppObject* L_3 = ___comparer1;
		__this->set_comparer_2(L_3);
		return;
	}
}
// System.IDisposable UniRx.Operators.DistinctUntilChangedObservable`1<System.Boolean>::SubscribeCore(UniRx.IObserver`1<T>,System.IDisposable)
extern "C"  Il2CppObject * DistinctUntilChangedObservable_1_SubscribeCore_m3109991909_gshared (DistinctUntilChangedObservable_1_t3199977885 * __this, Il2CppObject* ___observer0, Il2CppObject * ___cancel1, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = (Il2CppObject*)__this->get_source_1();
		Il2CppObject* L_1 = ___observer0;
		Il2CppObject * L_2 = ___cancel1;
		DistinctUntilChanged_t1676912132 * L_3 = (DistinctUntilChanged_t1676912132 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		((  void (*) (DistinctUntilChanged_t1676912132 *, DistinctUntilChangedObservable_1_t3199977885 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)(L_3, (DistinctUntilChangedObservable_1_t3199977885 *)__this, (Il2CppObject*)L_1, (Il2CppObject *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		NullCheck((Il2CppObject*)L_0);
		Il2CppObject * L_4 = InterfaceFuncInvoker1< Il2CppObject *, Il2CppObject* >::Invoke(0 /* System.IDisposable UniRx.IObservable`1<System.Boolean>::Subscribe(UniRx.IObserver`1<T>) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5), (Il2CppObject*)L_0, (Il2CppObject*)L_3);
		return L_4;
	}
}
// System.Void UniRx.Operators.DistinctUntilChangedObservable`1<System.Int32>::.ctor(UniRx.IObservable`1<T>,System.Collections.Generic.IEqualityComparer`1<T>)
extern "C"  void DistinctUntilChangedObservable_1__ctor_m2656853974_gshared (DistinctUntilChangedObservable_1_t1541420035 * __this, Il2CppObject* ___source0, Il2CppObject* ___comparer1, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = ___source0;
		bool L_1 = ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)(NULL /*static, unused*/, (Il2CppObject*)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		NullCheck((OperatorObservableBase_1_t1911559758 *)__this);
		((  void (*) (OperatorObservableBase_1_t1911559758 *, bool, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)((OperatorObservableBase_1_t1911559758 *)__this, (bool)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		Il2CppObject* L_2 = ___source0;
		__this->set_source_1(L_2);
		Il2CppObject* L_3 = ___comparer1;
		__this->set_comparer_2(L_3);
		return;
	}
}
// System.IDisposable UniRx.Operators.DistinctUntilChangedObservable`1<System.Int32>::SubscribeCore(UniRx.IObserver`1<T>,System.IDisposable)
extern "C"  Il2CppObject * DistinctUntilChangedObservable_1_SubscribeCore_m712286719_gshared (DistinctUntilChangedObservable_1_t1541420035 * __this, Il2CppObject* ___observer0, Il2CppObject * ___cancel1, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = (Il2CppObject*)__this->get_source_1();
		Il2CppObject* L_1 = ___observer0;
		Il2CppObject * L_2 = ___cancel1;
		DistinctUntilChanged_t18354282 * L_3 = (DistinctUntilChanged_t18354282 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		((  void (*) (DistinctUntilChanged_t18354282 *, DistinctUntilChangedObservable_1_t1541420035 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)(L_3, (DistinctUntilChangedObservable_1_t1541420035 *)__this, (Il2CppObject*)L_1, (Il2CppObject *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		NullCheck((Il2CppObject*)L_0);
		Il2CppObject * L_4 = InterfaceFuncInvoker1< Il2CppObject *, Il2CppObject* >::Invoke(0 /* System.IDisposable UniRx.IObservable`1<System.Int32>::Subscribe(UniRx.IObserver`1<T>) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5), (Il2CppObject*)L_0, (Il2CppObject*)L_3);
		return L_4;
	}
}
// System.Void UniRx.Operators.DistinctUntilChangedObservable`1<System.Object>::.ctor(UniRx.IObservable`1<T>,System.Collections.Generic.IEqualityComparer`1<T>)
extern "C"  void DistinctUntilChangedObservable_1__ctor_m3263149551_gshared (DistinctUntilChangedObservable_1_t3826078964 * __this, Il2CppObject* ___source0, Il2CppObject* ___comparer1, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = ___source0;
		bool L_1 = ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)(NULL /*static, unused*/, (Il2CppObject*)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		NullCheck((OperatorObservableBase_1_t4196218687 *)__this);
		((  void (*) (OperatorObservableBase_1_t4196218687 *, bool, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)((OperatorObservableBase_1_t4196218687 *)__this, (bool)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		Il2CppObject* L_2 = ___source0;
		__this->set_source_1(L_2);
		Il2CppObject* L_3 = ___comparer1;
		__this->set_comparer_2(L_3);
		return;
	}
}
// System.IDisposable UniRx.Operators.DistinctUntilChangedObservable`1<System.Object>::SubscribeCore(UniRx.IObserver`1<T>,System.IDisposable)
extern "C"  Il2CppObject * DistinctUntilChangedObservable_1_SubscribeCore_m2451621680_gshared (DistinctUntilChangedObservable_1_t3826078964 * __this, Il2CppObject* ___observer0, Il2CppObject * ___cancel1, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = (Il2CppObject*)__this->get_source_1();
		Il2CppObject* L_1 = ___observer0;
		Il2CppObject * L_2 = ___cancel1;
		DistinctUntilChanged_t2303013211 * L_3 = (DistinctUntilChanged_t2303013211 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		((  void (*) (DistinctUntilChanged_t2303013211 *, DistinctUntilChangedObservable_1_t3826078964 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)(L_3, (DistinctUntilChangedObservable_1_t3826078964 *)__this, (Il2CppObject*)L_1, (Il2CppObject *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		NullCheck((Il2CppObject*)L_0);
		Il2CppObject * L_4 = InterfaceFuncInvoker1< Il2CppObject *, Il2CppObject* >::Invoke(0 /* System.IDisposable UniRx.IObservable`1<System.Object>::Subscribe(UniRx.IObserver`1<T>) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5), (Il2CppObject*)L_0, (Il2CppObject*)L_3);
		return L_4;
	}
}
// System.Void UniRx.Operators.DistinctUntilChangedObservable`2/DistinctUntilChanged<System.Object,System.Object>::.ctor(UniRx.Operators.DistinctUntilChangedObservable`2<T,TKey>,UniRx.IObserver`1<T>,System.IDisposable)
extern Il2CppClass* Il2CppObject_il2cpp_TypeInfo_var;
extern const uint32_t DistinctUntilChanged__ctor_m309756399_MetadataUsageId;
extern "C"  void DistinctUntilChanged__ctor_m309756399_gshared (DistinctUntilChanged_t2419067341 * __this, DistinctUntilChangedObservable_2_t1596230355 * ___parent0, Il2CppObject* ___observer1, Il2CppObject * ___cancel2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DistinctUntilChanged__ctor_m309756399_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Il2CppObject * V_0 = NULL;
	{
		__this->set_isFirst_3((bool)1);
		Initobj (Il2CppObject_il2cpp_TypeInfo_var, (&V_0));
		Il2CppObject * L_0 = V_0;
		__this->set_prevKey_4(L_0);
		Il2CppObject* L_1 = ___observer1;
		Il2CppObject * L_2 = ___cancel2;
		NullCheck((OperatorObserverBase_2_t1187768149 *)__this);
		((  void (*) (OperatorObserverBase_2_t1187768149 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((OperatorObserverBase_2_t1187768149 *)__this, (Il2CppObject*)L_1, (Il2CppObject *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		DistinctUntilChangedObservable_2_t1596230355 * L_3 = ___parent0;
		__this->set_parent_2(L_3);
		return;
	}
}
// System.Void UniRx.Operators.DistinctUntilChangedObservable`2/DistinctUntilChanged<System.Object,System.Object>::OnNext(T)
extern Il2CppClass* Exception_t1967233988_il2cpp_TypeInfo_var;
extern const uint32_t DistinctUntilChanged_OnNext_m3817983600_MetadataUsageId;
extern "C"  void DistinctUntilChanged_OnNext_m3817983600_gshared (DistinctUntilChanged_t2419067341 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DistinctUntilChanged_OnNext_m3817983600_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Il2CppObject * V_0 = NULL;
	Exception_t1967233988 * V_1 = NULL;
	bool V_2 = false;
	Exception_t1967233988 * V_3 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		DistinctUntilChangedObservable_2_t1596230355 * L_0 = (DistinctUntilChangedObservable_2_t1596230355 *)__this->get_parent_2();
		NullCheck(L_0);
		Func_2_t2135783352 * L_1 = (Func_2_t2135783352 *)L_0->get_keySelector_3();
		Il2CppObject * L_2 = ___value0;
		NullCheck((Func_2_t2135783352 *)L_1);
		Il2CppObject * L_3 = ((  Il2CppObject * (*) (Func_2_t2135783352 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)((Func_2_t2135783352 *)L_1, (Il2CppObject *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		V_0 = (Il2CppObject *)L_3;
		goto IL_003c;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1967233988 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Exception_t1967233988_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_0017;
		throw e;
	}

CATCH_0017:
	{ // begin catch(System.Exception)
		{
			V_1 = (Exception_t1967233988 *)((Exception_t1967233988 *)__exception_local);
		}

IL_0018:
		try
		{ // begin try (depth: 2)
			Il2CppObject* L_4 = (Il2CppObject*)((OperatorObserverBase_2_t1187768149 *)__this)->get_observer_0();
			il2cpp_codegen_memory_barrier();
			Exception_t1967233988 * L_5 = V_1;
			NullCheck((Il2CppObject*)L_4);
			InterfaceActionInvoker1< Exception_t1967233988 * >::Invoke(1 /* System.Void UniRx.IObserver`1<System.Object>::OnError(System.Exception) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3), (Il2CppObject*)L_4, (Exception_t1967233988 *)L_5);
			IL2CPP_LEAVE(0x32, FINALLY_002b);
		} // end try (depth: 2)
		catch(Il2CppExceptionWrapper& e)
		{
			__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
			goto FINALLY_002b;
		}

FINALLY_002b:
		{ // begin finally (depth: 2)
			NullCheck((OperatorObserverBase_2_t1187768149 *)__this);
			VirtActionInvoker0::Invoke(4 /* System.Void UniRx.Operators.OperatorObserverBase`2<System.Object,System.Object>::Dispose() */, (OperatorObserverBase_2_t1187768149 *)__this);
			IL2CPP_END_FINALLY(43)
		} // end finally (depth: 2)
		IL2CPP_CLEANUP(43)
		{
			IL2CPP_JUMP_TBL(0x32, IL_0032)
			IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
		}

IL_0032:
		{
			goto IL_00b2;
		}

IL_0037:
		{
			; // IL_0037: leave IL_003c
		}
	} // end catch (depth: 1)

IL_003c:
	{
		V_2 = (bool)0;
		bool L_6 = (bool)__this->get_isFirst_3();
		if (!L_6)
		{
			goto IL_0055;
		}
	}
	{
		__this->set_isFirst_3((bool)0);
		goto IL_0097;
	}

IL_0055:
	try
	{ // begin try (depth: 1)
		DistinctUntilChangedObservable_2_t1596230355 * L_7 = (DistinctUntilChangedObservable_2_t1596230355 *)__this->get_parent_2();
		NullCheck(L_7);
		Il2CppObject* L_8 = (Il2CppObject*)L_7->get_comparer_2();
		Il2CppObject * L_9 = V_0;
		Il2CppObject * L_10 = (Il2CppObject *)__this->get_prevKey_4();
		NullCheck((Il2CppObject*)L_8);
		bool L_11 = InterfaceFuncInvoker2< bool, Il2CppObject *, Il2CppObject * >::Invoke(0 /* System.Boolean System.Collections.Generic.IEqualityComparer`1<System.Object>::Equals(!0,!0) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5), (Il2CppObject*)L_8, (Il2CppObject *)L_9, (Il2CppObject *)L_10);
		V_2 = (bool)L_11;
		goto IL_0097;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1967233988 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Exception_t1967233988_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_0072;
		throw e;
	}

CATCH_0072:
	{ // begin catch(System.Exception)
		{
			V_3 = (Exception_t1967233988 *)((Exception_t1967233988 *)__exception_local);
		}

IL_0073:
		try
		{ // begin try (depth: 2)
			Il2CppObject* L_12 = (Il2CppObject*)((OperatorObserverBase_2_t1187768149 *)__this)->get_observer_0();
			il2cpp_codegen_memory_barrier();
			Exception_t1967233988 * L_13 = V_3;
			NullCheck((Il2CppObject*)L_12);
			InterfaceActionInvoker1< Exception_t1967233988 * >::Invoke(1 /* System.Void UniRx.IObserver`1<System.Object>::OnError(System.Exception) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3), (Il2CppObject*)L_12, (Exception_t1967233988 *)L_13);
			IL2CPP_LEAVE(0x8D, FINALLY_0086);
		} // end try (depth: 2)
		catch(Il2CppExceptionWrapper& e)
		{
			__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
			goto FINALLY_0086;
		}

FINALLY_0086:
		{ // begin finally (depth: 2)
			NullCheck((OperatorObserverBase_2_t1187768149 *)__this);
			VirtActionInvoker0::Invoke(4 /* System.Void UniRx.Operators.OperatorObserverBase`2<System.Object,System.Object>::Dispose() */, (OperatorObserverBase_2_t1187768149 *)__this);
			IL2CPP_END_FINALLY(134)
		} // end finally (depth: 2)
		IL2CPP_CLEANUP(134)
		{
			IL2CPP_JUMP_TBL(0x8D, IL_008d)
			IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
		}

IL_008d:
		{
			goto IL_00b2;
		}

IL_0092:
		{
			; // IL_0092: leave IL_0097
		}
	} // end catch (depth: 1)

IL_0097:
	{
		bool L_14 = V_2;
		if (L_14)
		{
			goto IL_00b2;
		}
	}
	{
		Il2CppObject * L_15 = V_0;
		__this->set_prevKey_4(L_15);
		Il2CppObject* L_16 = (Il2CppObject*)((OperatorObserverBase_2_t1187768149 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		Il2CppObject * L_17 = ___value0;
		NullCheck((Il2CppObject*)L_16);
		InterfaceActionInvoker1< Il2CppObject * >::Invoke(2 /* System.Void UniRx.IObserver`1<System.Object>::OnNext(T) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3), (Il2CppObject*)L_16, (Il2CppObject *)L_17);
	}

IL_00b2:
	{
		return;
	}
}
// System.Void UniRx.Operators.DistinctUntilChangedObservable`2/DistinctUntilChanged<System.Object,System.Object>::OnError(System.Exception)
extern "C"  void DistinctUntilChanged_OnError_m2278230399_gshared (DistinctUntilChanged_t2419067341 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method)
{
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		Il2CppObject* L_0 = (Il2CppObject*)((OperatorObserverBase_2_t1187768149 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		Exception_t1967233988 * L_1 = ___error0;
		NullCheck((Il2CppObject*)L_0);
		InterfaceActionInvoker1< Exception_t1967233988 * >::Invoke(1 /* System.Void UniRx.IObserver`1<System.Object>::OnError(System.Exception) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3), (Il2CppObject*)L_0, (Exception_t1967233988 *)L_1);
		IL2CPP_LEAVE(0x1A, FINALLY_0013);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0013;
	}

FINALLY_0013:
	{ // begin finally (depth: 1)
		NullCheck((OperatorObserverBase_2_t1187768149 *)__this);
		VirtActionInvoker0::Invoke(4 /* System.Void UniRx.Operators.OperatorObserverBase`2<System.Object,System.Object>::Dispose() */, (OperatorObserverBase_2_t1187768149 *)__this);
		IL2CPP_END_FINALLY(19)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(19)
	{
		IL2CPP_JUMP_TBL(0x1A, IL_001a)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_001a:
	{
		return;
	}
}
// System.Void UniRx.Operators.DistinctUntilChangedObservable`2/DistinctUntilChanged<System.Object,System.Object>::OnCompleted()
extern "C"  void DistinctUntilChanged_OnCompleted_m1697956690_gshared (DistinctUntilChanged_t2419067341 * __this, const MethodInfo* method)
{
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		Il2CppObject* L_0 = (Il2CppObject*)((OperatorObserverBase_2_t1187768149 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		NullCheck((Il2CppObject*)L_0);
		InterfaceActionInvoker0::Invoke(0 /* System.Void UniRx.IObserver`1<System.Object>::OnCompleted() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3), (Il2CppObject*)L_0);
		IL2CPP_LEAVE(0x19, FINALLY_0012);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0012;
	}

FINALLY_0012:
	{ // begin finally (depth: 1)
		NullCheck((OperatorObserverBase_2_t1187768149 *)__this);
		VirtActionInvoker0::Invoke(4 /* System.Void UniRx.Operators.OperatorObserverBase`2<System.Object,System.Object>::Dispose() */, (OperatorObserverBase_2_t1187768149 *)__this);
		IL2CPP_END_FINALLY(18)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(18)
	{
		IL2CPP_JUMP_TBL(0x19, IL_0019)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0019:
	{
		return;
	}
}
// System.Void UniRx.Operators.DistinctUntilChangedObservable`2<System.Object,System.Object>::.ctor(UniRx.IObservable`1<T>,System.Func`2<T,TKey>,System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C"  void DistinctUntilChangedObservable_2__ctor_m4202592059_gshared (DistinctUntilChangedObservable_2_t1596230355 * __this, Il2CppObject* ___source0, Func_2_t2135783352 * ___keySelector1, Il2CppObject* ___comparer2, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = ___source0;
		bool L_1 = ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)(NULL /*static, unused*/, (Il2CppObject*)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		NullCheck((OperatorObservableBase_1_t4196218687 *)__this);
		((  void (*) (OperatorObservableBase_1_t4196218687 *, bool, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)((OperatorObservableBase_1_t4196218687 *)__this, (bool)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		Il2CppObject* L_2 = ___source0;
		__this->set_source_1(L_2);
		Il2CppObject* L_3 = ___comparer2;
		__this->set_comparer_2(L_3);
		Func_2_t2135783352 * L_4 = ___keySelector1;
		__this->set_keySelector_3(L_4);
		return;
	}
}
// System.IDisposable UniRx.Operators.DistinctUntilChangedObservable`2<System.Object,System.Object>::SubscribeCore(UniRx.IObserver`1<T>,System.IDisposable)
extern "C"  Il2CppObject * DistinctUntilChangedObservable_2_SubscribeCore_m3372304221_gshared (DistinctUntilChangedObservable_2_t1596230355 * __this, Il2CppObject* ___observer0, Il2CppObject * ___cancel1, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = (Il2CppObject*)__this->get_source_1();
		Il2CppObject* L_1 = ___observer0;
		Il2CppObject * L_2 = ___cancel1;
		DistinctUntilChanged_t2419067341 * L_3 = (DistinctUntilChanged_t2419067341 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		((  void (*) (DistinctUntilChanged_t2419067341 *, DistinctUntilChangedObservable_2_t1596230355 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)(L_3, (DistinctUntilChangedObservable_2_t1596230355 *)__this, (Il2CppObject*)L_1, (Il2CppObject *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		NullCheck((Il2CppObject*)L_0);
		Il2CppObject * L_4 = InterfaceFuncInvoker1< Il2CppObject *, Il2CppObject* >::Invoke(0 /* System.IDisposable UniRx.IObservable`1<System.Object>::Subscribe(UniRx.IObserver`1<T>) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5), (Il2CppObject*)L_0, (Il2CppObject*)L_3);
		return L_4;
	}
}
// System.Void UniRx.Operators.DoObservable`1/Do<System.Object>::.ctor(UniRx.Operators.DoObservable`1<T>,UniRx.IObserver`1<T>,System.IDisposable)
extern "C"  void Do__ctor_m3090528687_gshared (Do_t3185263597 * __this, DoObservable_1_t911152645 * ___parent0, Il2CppObject* ___observer1, Il2CppObject * ___cancel2, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = ___observer1;
		Il2CppObject * L_1 = ___cancel2;
		NullCheck((OperatorObserverBase_2_t1187768149 *)__this);
		((  void (*) (OperatorObserverBase_2_t1187768149 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((OperatorObserverBase_2_t1187768149 *)__this, (Il2CppObject*)L_0, (Il2CppObject *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		DoObservable_1_t911152645 * L_2 = ___parent0;
		__this->set_parent_2(L_2);
		return;
	}
}
// System.IDisposable UniRx.Operators.DoObservable`1/Do<System.Object>::Run()
extern "C"  Il2CppObject * Do_Run_m1114745637_gshared (Do_t3185263597 * __this, const MethodInfo* method)
{
	{
		DoObservable_1_t911152645 * L_0 = (DoObservable_1_t911152645 *)__this->get_parent_2();
		NullCheck(L_0);
		Il2CppObject* L_1 = (Il2CppObject*)L_0->get_source_1();
		NullCheck((Il2CppObject*)L_1);
		Il2CppObject * L_2 = InterfaceFuncInvoker1< Il2CppObject *, Il2CppObject* >::Invoke(0 /* System.IDisposable UniRx.IObservable`1<System.Object>::Subscribe(UniRx.IObserver`1<T>) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), (Il2CppObject*)L_1, (Il2CppObject*)__this);
		return L_2;
	}
}
// System.Void UniRx.Operators.DoObservable`1/Do<System.Object>::OnNext(T)
extern Il2CppClass* Exception_t1967233988_il2cpp_TypeInfo_var;
extern const uint32_t Do_OnNext_m3851476927_MetadataUsageId;
extern "C"  void Do_OnNext_m3851476927_gshared (Do_t3185263597 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Do_OnNext_m3851476927_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Exception_t1967233988 * V_0 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		DoObservable_1_t911152645 * L_0 = (DoObservable_1_t911152645 *)__this->get_parent_2();
		NullCheck(L_0);
		Action_1_t985559125 * L_1 = (Action_1_t985559125 *)L_0->get_onNext_2();
		Il2CppObject * L_2 = ___value0;
		NullCheck((Action_1_t985559125 *)L_1);
		((  void (*) (Action_1_t985559125 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)((Action_1_t985559125 *)L_1, (Il2CppObject *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		goto IL_003b;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1967233988 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Exception_t1967233988_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_0016;
		throw e;
	}

CATCH_0016:
	{ // begin catch(System.Exception)
		{
			V_0 = (Exception_t1967233988 *)((Exception_t1967233988 *)__exception_local);
		}

IL_0017:
		try
		{ // begin try (depth: 2)
			Il2CppObject* L_3 = (Il2CppObject*)((OperatorObserverBase_2_t1187768149 *)__this)->get_observer_0();
			il2cpp_codegen_memory_barrier();
			Exception_t1967233988 * L_4 = V_0;
			NullCheck((Il2CppObject*)L_3);
			InterfaceActionInvoker1< Exception_t1967233988 * >::Invoke(1 /* System.Void UniRx.IObserver`1<System.Object>::OnError(System.Exception) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4), (Il2CppObject*)L_3, (Exception_t1967233988 *)L_4);
			IL2CPP_LEAVE(0x31, FINALLY_002a);
		} // end try (depth: 2)
		catch(Il2CppExceptionWrapper& e)
		{
			__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
			goto FINALLY_002a;
		}

FINALLY_002a:
		{ // begin finally (depth: 2)
			NullCheck((OperatorObserverBase_2_t1187768149 *)__this);
			VirtActionInvoker0::Invoke(4 /* System.Void UniRx.Operators.OperatorObserverBase`2<System.Object,System.Object>::Dispose() */, (OperatorObserverBase_2_t1187768149 *)__this);
			IL2CPP_END_FINALLY(42)
		} // end finally (depth: 2)
		IL2CPP_CLEANUP(42)
		{
			IL2CPP_JUMP_TBL(0x31, IL_0031)
			IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
		}

IL_0031:
		{
			goto IL_0049;
		}

IL_0036:
		{
			; // IL_0036: leave IL_003b
		}
	} // end catch (depth: 1)

IL_003b:
	{
		Il2CppObject* L_5 = (Il2CppObject*)((OperatorObserverBase_2_t1187768149 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		Il2CppObject * L_6 = ___value0;
		NullCheck((Il2CppObject*)L_5);
		InterfaceActionInvoker1< Il2CppObject * >::Invoke(2 /* System.Void UniRx.IObserver`1<System.Object>::OnNext(T) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4), (Il2CppObject*)L_5, (Il2CppObject *)L_6);
	}

IL_0049:
	{
		return;
	}
}
// System.Void UniRx.Operators.DoObservable`1/Do<System.Object>::OnError(System.Exception)
extern Il2CppClass* Exception_t1967233988_il2cpp_TypeInfo_var;
extern const MethodInfo* Action_1_Invoke_m1286428014_MethodInfo_var;
extern const uint32_t Do_OnError_m3623066830_MetadataUsageId;
extern "C"  void Do_OnError_m3623066830_gshared (Do_t3185263597 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Do_OnError_m3623066830_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Exception_t1967233988 * V_0 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		DoObservable_1_t911152645 * L_0 = (DoObservable_1_t911152645 *)__this->get_parent_2();
		NullCheck(L_0);
		Action_1_t2115686693 * L_1 = (Action_1_t2115686693 *)L_0->get_onError_3();
		Exception_t1967233988 * L_2 = ___error0;
		NullCheck((Action_1_t2115686693 *)L_1);
		Action_1_Invoke_m1286428014((Action_1_t2115686693 *)L_1, (Exception_t1967233988 *)L_2, /*hidden argument*/Action_1_Invoke_m1286428014_MethodInfo_var);
		goto IL_003b;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1967233988 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Exception_t1967233988_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_0016;
		throw e;
	}

CATCH_0016:
	{ // begin catch(System.Exception)
		{
			V_0 = (Exception_t1967233988 *)((Exception_t1967233988 *)__exception_local);
		}

IL_0017:
		try
		{ // begin try (depth: 2)
			Il2CppObject* L_3 = (Il2CppObject*)((OperatorObserverBase_2_t1187768149 *)__this)->get_observer_0();
			il2cpp_codegen_memory_barrier();
			Exception_t1967233988 * L_4 = V_0;
			NullCheck((Il2CppObject*)L_3);
			InterfaceActionInvoker1< Exception_t1967233988 * >::Invoke(1 /* System.Void UniRx.IObserver`1<System.Object>::OnError(System.Exception) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4), (Il2CppObject*)L_3, (Exception_t1967233988 *)L_4);
			IL2CPP_LEAVE(0x31, FINALLY_002a);
		} // end try (depth: 2)
		catch(Il2CppExceptionWrapper& e)
		{
			__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
			goto FINALLY_002a;
		}

FINALLY_002a:
		{ // begin finally (depth: 2)
			NullCheck((OperatorObserverBase_2_t1187768149 *)__this);
			VirtActionInvoker0::Invoke(4 /* System.Void UniRx.Operators.OperatorObserverBase`2<System.Object,System.Object>::Dispose() */, (OperatorObserverBase_2_t1187768149 *)__this);
			IL2CPP_END_FINALLY(42)
		} // end finally (depth: 2)
		IL2CPP_CLEANUP(42)
		{
			IL2CPP_JUMP_TBL(0x31, IL_0031)
			IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
		}

IL_0031:
		{
			goto IL_0055;
		}

IL_0036:
		{
			; // IL_0036: leave IL_003b
		}
	} // end catch (depth: 1)

IL_003b:
	try
	{ // begin try (depth: 1)
		Il2CppObject* L_5 = (Il2CppObject*)((OperatorObserverBase_2_t1187768149 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		Exception_t1967233988 * L_6 = ___error0;
		NullCheck((Il2CppObject*)L_5);
		InterfaceActionInvoker1< Exception_t1967233988 * >::Invoke(1 /* System.Void UniRx.IObserver`1<System.Object>::OnError(System.Exception) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4), (Il2CppObject*)L_5, (Exception_t1967233988 *)L_6);
		IL2CPP_LEAVE(0x55, FINALLY_004e);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_004e;
	}

FINALLY_004e:
	{ // begin finally (depth: 1)
		NullCheck((OperatorObserverBase_2_t1187768149 *)__this);
		VirtActionInvoker0::Invoke(4 /* System.Void UniRx.Operators.OperatorObserverBase`2<System.Object,System.Object>::Dispose() */, (OperatorObserverBase_2_t1187768149 *)__this);
		IL2CPP_END_FINALLY(78)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(78)
	{
		IL2CPP_JUMP_TBL(0x55, IL_0055)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0055:
	{
		return;
	}
}
// System.Void UniRx.Operators.DoObservable`1/Do<System.Object>::OnCompleted()
extern Il2CppClass* Exception_t1967233988_il2cpp_TypeInfo_var;
extern const uint32_t Do_OnCompleted_m1134335265_MetadataUsageId;
extern "C"  void Do_OnCompleted_m1134335265_gshared (Do_t3185263597 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Do_OnCompleted_m1134335265_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Exception_t1967233988 * V_0 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		DoObservable_1_t911152645 * L_0 = (DoObservable_1_t911152645 *)__this->get_parent_2();
		NullCheck(L_0);
		Action_t437523947 * L_1 = (Action_t437523947 *)L_0->get_onCompleted_4();
		NullCheck((Action_t437523947 *)L_1);
		Action_Invoke_m1445970038((Action_t437523947 *)L_1, /*hidden argument*/NULL);
		goto IL_0034;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1967233988 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Exception_t1967233988_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_0015;
		throw e;
	}

CATCH_0015:
	{ // begin catch(System.Exception)
		{
			V_0 = (Exception_t1967233988 *)((Exception_t1967233988 *)__exception_local);
			Il2CppObject* L_2 = (Il2CppObject*)((OperatorObserverBase_2_t1187768149 *)__this)->get_observer_0();
			il2cpp_codegen_memory_barrier();
			Exception_t1967233988 * L_3 = V_0;
			NullCheck((Il2CppObject*)L_2);
			InterfaceActionInvoker1< Exception_t1967233988 * >::Invoke(1 /* System.Void UniRx.IObserver`1<System.Object>::OnError(System.Exception) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4), (Il2CppObject*)L_2, (Exception_t1967233988 *)L_3);
			NullCheck((OperatorObserverBase_2_t1187768149 *)__this);
			VirtActionInvoker0::Invoke(4 /* System.Void UniRx.Operators.OperatorObserverBase`2<System.Object,System.Object>::Dispose() */, (OperatorObserverBase_2_t1187768149 *)__this);
			goto IL_004d;
		}

IL_002f:
		{
			; // IL_002f: leave IL_0034
		}
	} // end catch (depth: 1)

IL_0034:
	try
	{ // begin try (depth: 1)
		Il2CppObject* L_4 = (Il2CppObject*)((OperatorObserverBase_2_t1187768149 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		NullCheck((Il2CppObject*)L_4);
		InterfaceActionInvoker0::Invoke(0 /* System.Void UniRx.IObserver`1<System.Object>::OnCompleted() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4), (Il2CppObject*)L_4);
		IL2CPP_LEAVE(0x4D, FINALLY_0046);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0046;
	}

FINALLY_0046:
	{ // begin finally (depth: 1)
		NullCheck((OperatorObserverBase_2_t1187768149 *)__this);
		VirtActionInvoker0::Invoke(4 /* System.Void UniRx.Operators.OperatorObserverBase`2<System.Object,System.Object>::Dispose() */, (OperatorObserverBase_2_t1187768149 *)__this);
		IL2CPP_END_FINALLY(70)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(70)
	{
		IL2CPP_JUMP_TBL(0x4D, IL_004d)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_004d:
	{
		return;
	}
}
// System.Void UniRx.Operators.DoObservable`1<System.Object>::.ctor(UniRx.IObservable`1<T>,System.Action`1<T>,System.Action`1<System.Exception>,System.Action)
extern "C"  void DoObservable_1__ctor_m529972321_gshared (DoObservable_1_t911152645 * __this, Il2CppObject* ___source0, Action_1_t985559125 * ___onNext1, Action_1_t2115686693 * ___onError2, Action_t437523947 * ___onCompleted3, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = ___source0;
		bool L_1 = ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)(NULL /*static, unused*/, (Il2CppObject*)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		NullCheck((OperatorObservableBase_1_t4196218687 *)__this);
		((  void (*) (OperatorObservableBase_1_t4196218687 *, bool, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)((OperatorObservableBase_1_t4196218687 *)__this, (bool)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		Il2CppObject* L_2 = ___source0;
		__this->set_source_1(L_2);
		Action_1_t985559125 * L_3 = ___onNext1;
		__this->set_onNext_2(L_3);
		Action_1_t2115686693 * L_4 = ___onError2;
		__this->set_onError_3(L_4);
		Action_t437523947 * L_5 = ___onCompleted3;
		__this->set_onCompleted_4(L_5);
		return;
	}
}
// System.IDisposable UniRx.Operators.DoObservable`1<System.Object>::SubscribeCore(UniRx.IObserver`1<T>,System.IDisposable)
extern "C"  Il2CppObject * DoObservable_1_SubscribeCore_m1332131607_gshared (DoObservable_1_t911152645 * __this, Il2CppObject* ___observer0, Il2CppObject * ___cancel1, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = ___observer0;
		Il2CppObject * L_1 = ___cancel1;
		Do_t3185263597 * L_2 = (Do_t3185263597 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		((  void (*) (Do_t3185263597 *, DoObservable_1_t911152645 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)(L_2, (DoObservable_1_t911152645 *)__this, (Il2CppObject*)L_0, (Il2CppObject *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		NullCheck((Do_t3185263597 *)L_2);
		Il2CppObject * L_3 = ((  Il2CppObject * (*) (Do_t3185263597 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->method)((Do_t3185263597 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		return L_3;
	}
}
// System.Void UniRx.Operators.DoObserverObservable`1/Do<System.Object>::.ctor(UniRx.Operators.DoObserverObservable`1<T>,UniRx.IObserver`1<T>,System.IDisposable)
extern "C"  void Do__ctor_m1276205467_gshared (Do_t3185263596 * __this, DoObserverObservable_1_t2771775371 * ___parent0, Il2CppObject* ___observer1, Il2CppObject * ___cancel2, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = ___observer1;
		Il2CppObject * L_1 = ___cancel2;
		NullCheck((OperatorObserverBase_2_t1187768149 *)__this);
		((  void (*) (OperatorObserverBase_2_t1187768149 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((OperatorObserverBase_2_t1187768149 *)__this, (Il2CppObject*)L_0, (Il2CppObject *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		DoObserverObservable_1_t2771775371 * L_2 = ___parent0;
		__this->set_parent_2(L_2);
		return;
	}
}
// System.IDisposable UniRx.Operators.DoObserverObservable`1/Do<System.Object>::Run()
extern "C"  Il2CppObject * Do_Run_m1848628975_gshared (Do_t3185263596 * __this, const MethodInfo* method)
{
	{
		DoObserverObservable_1_t2771775371 * L_0 = (DoObserverObservable_1_t2771775371 *)__this->get_parent_2();
		NullCheck(L_0);
		Il2CppObject* L_1 = (Il2CppObject*)L_0->get_source_1();
		NullCheck((Il2CppObject*)L_1);
		Il2CppObject * L_2 = InterfaceFuncInvoker1< Il2CppObject *, Il2CppObject* >::Invoke(0 /* System.IDisposable UniRx.IObservable`1<System.Object>::Subscribe(UniRx.IObserver`1<T>) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), (Il2CppObject*)L_1, (Il2CppObject*)__this);
		return L_2;
	}
}
// System.Void UniRx.Operators.DoObserverObservable`1/Do<System.Object>::OnNext(T)
extern Il2CppClass* Exception_t1967233988_il2cpp_TypeInfo_var;
extern const uint32_t Do_OnNext_m2253638537_MetadataUsageId;
extern "C"  void Do_OnNext_m2253638537_gshared (Do_t3185263596 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Do_OnNext_m2253638537_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Exception_t1967233988 * V_0 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		DoObserverObservable_1_t2771775371 * L_0 = (DoObserverObservable_1_t2771775371 *)__this->get_parent_2();
		NullCheck(L_0);
		Il2CppObject* L_1 = (Il2CppObject*)L_0->get_observer_2();
		Il2CppObject * L_2 = ___value0;
		NullCheck((Il2CppObject*)L_1);
		InterfaceActionInvoker1< Il2CppObject * >::Invoke(2 /* System.Void UniRx.IObserver`1<System.Object>::OnNext(T) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3), (Il2CppObject*)L_1, (Il2CppObject *)L_2);
		goto IL_003b;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1967233988 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Exception_t1967233988_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_0016;
		throw e;
	}

CATCH_0016:
	{ // begin catch(System.Exception)
		{
			V_0 = (Exception_t1967233988 *)((Exception_t1967233988 *)__exception_local);
		}

IL_0017:
		try
		{ // begin try (depth: 2)
			Il2CppObject* L_3 = (Il2CppObject*)((OperatorObserverBase_2_t1187768149 *)__this)->get_observer_0();
			il2cpp_codegen_memory_barrier();
			Exception_t1967233988 * L_4 = V_0;
			NullCheck((Il2CppObject*)L_3);
			InterfaceActionInvoker1< Exception_t1967233988 * >::Invoke(1 /* System.Void UniRx.IObserver`1<System.Object>::OnError(System.Exception) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3), (Il2CppObject*)L_3, (Exception_t1967233988 *)L_4);
			IL2CPP_LEAVE(0x31, FINALLY_002a);
		} // end try (depth: 2)
		catch(Il2CppExceptionWrapper& e)
		{
			__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
			goto FINALLY_002a;
		}

FINALLY_002a:
		{ // begin finally (depth: 2)
			NullCheck((OperatorObserverBase_2_t1187768149 *)__this);
			VirtActionInvoker0::Invoke(4 /* System.Void UniRx.Operators.OperatorObserverBase`2<System.Object,System.Object>::Dispose() */, (OperatorObserverBase_2_t1187768149 *)__this);
			IL2CPP_END_FINALLY(42)
		} // end finally (depth: 2)
		IL2CPP_CLEANUP(42)
		{
			IL2CPP_JUMP_TBL(0x31, IL_0031)
			IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
		}

IL_0031:
		{
			goto IL_0049;
		}

IL_0036:
		{
			; // IL_0036: leave IL_003b
		}
	} // end catch (depth: 1)

IL_003b:
	{
		Il2CppObject* L_5 = (Il2CppObject*)((OperatorObserverBase_2_t1187768149 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		Il2CppObject * L_6 = ___value0;
		NullCheck((Il2CppObject*)L_5);
		InterfaceActionInvoker1< Il2CppObject * >::Invoke(2 /* System.Void UniRx.IObserver`1<System.Object>::OnNext(T) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3), (Il2CppObject*)L_5, (Il2CppObject *)L_6);
	}

IL_0049:
	{
		return;
	}
}
// System.Void UniRx.Operators.DoObserverObservable`1/Do<System.Object>::OnError(System.Exception)
extern Il2CppClass* Exception_t1967233988_il2cpp_TypeInfo_var;
extern const uint32_t Do_OnError_m1233761432_MetadataUsageId;
extern "C"  void Do_OnError_m1233761432_gshared (Do_t3185263596 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Do_OnError_m1233761432_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Exception_t1967233988 * V_0 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		DoObserverObservable_1_t2771775371 * L_0 = (DoObserverObservable_1_t2771775371 *)__this->get_parent_2();
		NullCheck(L_0);
		Il2CppObject* L_1 = (Il2CppObject*)L_0->get_observer_2();
		Exception_t1967233988 * L_2 = ___error0;
		NullCheck((Il2CppObject*)L_1);
		InterfaceActionInvoker1< Exception_t1967233988 * >::Invoke(1 /* System.Void UniRx.IObserver`1<System.Object>::OnError(System.Exception) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3), (Il2CppObject*)L_1, (Exception_t1967233988 *)L_2);
		goto IL_003b;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1967233988 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Exception_t1967233988_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_0016;
		throw e;
	}

CATCH_0016:
	{ // begin catch(System.Exception)
		{
			V_0 = (Exception_t1967233988 *)((Exception_t1967233988 *)__exception_local);
		}

IL_0017:
		try
		{ // begin try (depth: 2)
			Il2CppObject* L_3 = (Il2CppObject*)((OperatorObserverBase_2_t1187768149 *)__this)->get_observer_0();
			il2cpp_codegen_memory_barrier();
			Exception_t1967233988 * L_4 = V_0;
			NullCheck((Il2CppObject*)L_3);
			InterfaceActionInvoker1< Exception_t1967233988 * >::Invoke(1 /* System.Void UniRx.IObserver`1<System.Object>::OnError(System.Exception) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3), (Il2CppObject*)L_3, (Exception_t1967233988 *)L_4);
			IL2CPP_LEAVE(0x31, FINALLY_002a);
		} // end try (depth: 2)
		catch(Il2CppExceptionWrapper& e)
		{
			__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
			goto FINALLY_002a;
		}

FINALLY_002a:
		{ // begin finally (depth: 2)
			NullCheck((OperatorObserverBase_2_t1187768149 *)__this);
			VirtActionInvoker0::Invoke(4 /* System.Void UniRx.Operators.OperatorObserverBase`2<System.Object,System.Object>::Dispose() */, (OperatorObserverBase_2_t1187768149 *)__this);
			IL2CPP_END_FINALLY(42)
		} // end finally (depth: 2)
		IL2CPP_CLEANUP(42)
		{
			IL2CPP_JUMP_TBL(0x31, IL_0031)
			IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
		}

IL_0031:
		{
			goto IL_0055;
		}

IL_0036:
		{
			; // IL_0036: leave IL_003b
		}
	} // end catch (depth: 1)

IL_003b:
	try
	{ // begin try (depth: 1)
		Il2CppObject* L_5 = (Il2CppObject*)((OperatorObserverBase_2_t1187768149 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		Exception_t1967233988 * L_6 = ___error0;
		NullCheck((Il2CppObject*)L_5);
		InterfaceActionInvoker1< Exception_t1967233988 * >::Invoke(1 /* System.Void UniRx.IObserver`1<System.Object>::OnError(System.Exception) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3), (Il2CppObject*)L_5, (Exception_t1967233988 *)L_6);
		IL2CPP_LEAVE(0x55, FINALLY_004e);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_004e;
	}

FINALLY_004e:
	{ // begin finally (depth: 1)
		NullCheck((OperatorObserverBase_2_t1187768149 *)__this);
		VirtActionInvoker0::Invoke(4 /* System.Void UniRx.Operators.OperatorObserverBase`2<System.Object,System.Object>::Dispose() */, (OperatorObserverBase_2_t1187768149 *)__this);
		IL2CPP_END_FINALLY(78)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(78)
	{
		IL2CPP_JUMP_TBL(0x55, IL_0055)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0055:
	{
		return;
	}
}
// System.Void UniRx.Operators.DoObserverObservable`1/Do<System.Object>::OnCompleted()
extern Il2CppClass* Exception_t1967233988_il2cpp_TypeInfo_var;
extern const uint32_t Do_OnCompleted_m2920319979_MetadataUsageId;
extern "C"  void Do_OnCompleted_m2920319979_gshared (Do_t3185263596 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Do_OnCompleted_m2920319979_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Exception_t1967233988 * V_0 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		DoObserverObservable_1_t2771775371 * L_0 = (DoObserverObservable_1_t2771775371 *)__this->get_parent_2();
		NullCheck(L_0);
		Il2CppObject* L_1 = (Il2CppObject*)L_0->get_observer_2();
		NullCheck((Il2CppObject*)L_1);
		InterfaceActionInvoker0::Invoke(0 /* System.Void UniRx.IObserver`1<System.Object>::OnCompleted() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3), (Il2CppObject*)L_1);
		goto IL_003a;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1967233988 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Exception_t1967233988_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_0015;
		throw e;
	}

CATCH_0015:
	{ // begin catch(System.Exception)
		{
			V_0 = (Exception_t1967233988 *)((Exception_t1967233988 *)__exception_local);
		}

IL_0016:
		try
		{ // begin try (depth: 2)
			Il2CppObject* L_2 = (Il2CppObject*)((OperatorObserverBase_2_t1187768149 *)__this)->get_observer_0();
			il2cpp_codegen_memory_barrier();
			Exception_t1967233988 * L_3 = V_0;
			NullCheck((Il2CppObject*)L_2);
			InterfaceActionInvoker1< Exception_t1967233988 * >::Invoke(1 /* System.Void UniRx.IObserver`1<System.Object>::OnError(System.Exception) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3), (Il2CppObject*)L_2, (Exception_t1967233988 *)L_3);
			IL2CPP_LEAVE(0x30, FINALLY_0029);
		} // end try (depth: 2)
		catch(Il2CppExceptionWrapper& e)
		{
			__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
			goto FINALLY_0029;
		}

FINALLY_0029:
		{ // begin finally (depth: 2)
			NullCheck((OperatorObserverBase_2_t1187768149 *)__this);
			VirtActionInvoker0::Invoke(4 /* System.Void UniRx.Operators.OperatorObserverBase`2<System.Object,System.Object>::Dispose() */, (OperatorObserverBase_2_t1187768149 *)__this);
			IL2CPP_END_FINALLY(41)
		} // end finally (depth: 2)
		IL2CPP_CLEANUP(41)
		{
			IL2CPP_JUMP_TBL(0x30, IL_0030)
			IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
		}

IL_0030:
		{
			goto IL_0053;
		}

IL_0035:
		{
			; // IL_0035: leave IL_003a
		}
	} // end catch (depth: 1)

IL_003a:
	try
	{ // begin try (depth: 1)
		Il2CppObject* L_4 = (Il2CppObject*)((OperatorObserverBase_2_t1187768149 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		NullCheck((Il2CppObject*)L_4);
		InterfaceActionInvoker0::Invoke(0 /* System.Void UniRx.IObserver`1<System.Object>::OnCompleted() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3), (Il2CppObject*)L_4);
		IL2CPP_LEAVE(0x53, FINALLY_004c);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_004c;
	}

FINALLY_004c:
	{ // begin finally (depth: 1)
		NullCheck((OperatorObserverBase_2_t1187768149 *)__this);
		VirtActionInvoker0::Invoke(4 /* System.Void UniRx.Operators.OperatorObserverBase`2<System.Object,System.Object>::Dispose() */, (OperatorObserverBase_2_t1187768149 *)__this);
		IL2CPP_END_FINALLY(76)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(76)
	{
		IL2CPP_JUMP_TBL(0x53, IL_0053)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0053:
	{
		return;
	}
}
// System.Void UniRx.Operators.DoObserverObservable`1<System.Object>::.ctor(UniRx.IObservable`1<T>,UniRx.IObserver`1<T>)
extern "C"  void DoObserverObservable_1__ctor_m3723482770_gshared (DoObserverObservable_1_t2771775371 * __this, Il2CppObject* ___source0, Il2CppObject* ___observer1, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = ___source0;
		bool L_1 = ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)(NULL /*static, unused*/, (Il2CppObject*)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		NullCheck((OperatorObservableBase_1_t4196218687 *)__this);
		((  void (*) (OperatorObservableBase_1_t4196218687 *, bool, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)((OperatorObservableBase_1_t4196218687 *)__this, (bool)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		Il2CppObject* L_2 = ___source0;
		__this->set_source_1(L_2);
		Il2CppObject* L_3 = ___observer1;
		__this->set_observer_2(L_3);
		return;
	}
}
// System.IDisposable UniRx.Operators.DoObserverObservable`1<System.Object>::SubscribeCore(UniRx.IObserver`1<T>,System.IDisposable)
extern "C"  Il2CppObject * DoObserverObservable_1_SubscribeCore_m3581137249_gshared (DoObserverObservable_1_t2771775371 * __this, Il2CppObject* ___observer0, Il2CppObject * ___cancel1, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = ___observer0;
		Il2CppObject * L_1 = ___cancel1;
		Do_t3185263596 * L_2 = (Do_t3185263596 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		((  void (*) (Do_t3185263596 *, DoObserverObservable_1_t2771775371 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)(L_2, (DoObserverObservable_1_t2771775371 *)__this, (Il2CppObject*)L_0, (Il2CppObject *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		NullCheck((Do_t3185263596 *)L_2);
		Il2CppObject * L_3 = ((  Il2CppObject * (*) (Do_t3185263596 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->method)((Do_t3185263596 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		return L_3;
	}
}
// System.Void UniRx.Operators.DoOnCancelObservable`1/DoOnCancel<System.Object>::.ctor(UniRx.Operators.DoOnCancelObservable`1<T>,UniRx.IObserver`1<T>,System.IDisposable)
extern "C"  void DoOnCancel__ctor_m207837736_gshared (DoOnCancel_t3043692509 * __this, DoOnCancelObservable_1_t3202318454 * ___parent0, Il2CppObject* ___observer1, Il2CppObject * ___cancel2, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = ___observer1;
		Il2CppObject * L_1 = ___cancel2;
		NullCheck((OperatorObserverBase_2_t1187768149 *)__this);
		((  void (*) (OperatorObserverBase_2_t1187768149 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((OperatorObserverBase_2_t1187768149 *)__this, (Il2CppObject*)L_0, (Il2CppObject *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		DoOnCancelObservable_1_t3202318454 * L_2 = ___parent0;
		__this->set_parent_2(L_2);
		return;
	}
}
// System.IDisposable UniRx.Operators.DoOnCancelObservable`1/DoOnCancel<System.Object>::Run()
extern Il2CppClass* Action_t437523947_il2cpp_TypeInfo_var;
extern Il2CppClass* Disposable_t3736388786_il2cpp_TypeInfo_var;
extern const uint32_t DoOnCancel_Run_m3849669381_MetadataUsageId;
extern "C"  Il2CppObject * DoOnCancel_Run_m3849669381_gshared (DoOnCancel_t3043692509 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DoOnCancel_Run_m3849669381_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		DoOnCancelObservable_1_t3202318454 * L_0 = (DoOnCancelObservable_1_t3202318454 *)__this->get_parent_2();
		NullCheck(L_0);
		Il2CppObject* L_1 = (Il2CppObject*)L_0->get_source_1();
		NullCheck((Il2CppObject*)L_1);
		Il2CppObject * L_2 = InterfaceFuncInvoker1< Il2CppObject *, Il2CppObject* >::Invoke(0 /* System.IDisposable UniRx.IObservable`1<System.Object>::Subscribe(UniRx.IObserver`1<T>) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), (Il2CppObject*)L_1, (Il2CppObject*)__this);
		IntPtr_t L_3;
		L_3.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		Action_t437523947 * L_4 = (Action_t437523947 *)il2cpp_codegen_object_new(Action_t437523947_il2cpp_TypeInfo_var);
		Action__ctor_m2957240604(L_4, (Il2CppObject *)__this, (IntPtr_t)L_3, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Disposable_t3736388786_il2cpp_TypeInfo_var);
		Il2CppObject * L_5 = Disposable_Create_m2910846009(NULL /*static, unused*/, (Action_t437523947 *)L_4, /*hidden argument*/NULL);
		Il2CppObject * L_6 = StableCompositeDisposable_Create_m3764788535(NULL /*static, unused*/, (Il2CppObject *)L_2, (Il2CppObject *)L_5, /*hidden argument*/NULL);
		return L_6;
	}
}
// System.Void UniRx.Operators.DoOnCancelObservable`1/DoOnCancel<System.Object>::OnNext(T)
extern "C"  void DoOnCancel_OnNext_m291764639_gshared (DoOnCancel_t3043692509 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = (Il2CppObject*)((OperatorObserverBase_2_t1187768149 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		Il2CppObject * L_1 = ___value0;
		NullCheck((Il2CppObject*)L_0);
		InterfaceActionInvoker1< Il2CppObject * >::Invoke(2 /* System.Void UniRx.IObserver`1<System.Object>::OnNext(T) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4), (Il2CppObject*)L_0, (Il2CppObject *)L_1);
		return;
	}
}
// System.Void UniRx.Operators.DoOnCancelObservable`1/DoOnCancel<System.Object>::OnError(System.Exception)
extern "C"  void DoOnCancel_OnError_m689370798_gshared (DoOnCancel_t3043692509 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method)
{
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		__this->set_isCompletedCall_3((bool)1);
	}

IL_0007:
	try
	{ // begin try (depth: 1)
		Il2CppObject* L_0 = (Il2CppObject*)((OperatorObserverBase_2_t1187768149 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		Exception_t1967233988 * L_1 = ___error0;
		NullCheck((Il2CppObject*)L_0);
		InterfaceActionInvoker1< Exception_t1967233988 * >::Invoke(1 /* System.Void UniRx.IObserver`1<System.Object>::OnError(System.Exception) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4), (Il2CppObject*)L_0, (Exception_t1967233988 *)L_1);
		IL2CPP_LEAVE(0x21, FINALLY_001a);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_001a;
	}

FINALLY_001a:
	{ // begin finally (depth: 1)
		NullCheck((OperatorObserverBase_2_t1187768149 *)__this);
		VirtActionInvoker0::Invoke(4 /* System.Void UniRx.Operators.OperatorObserverBase`2<System.Object,System.Object>::Dispose() */, (OperatorObserverBase_2_t1187768149 *)__this);
		IL2CPP_END_FINALLY(26)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(26)
	{
		IL2CPP_JUMP_TBL(0x21, IL_0021)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0021:
	{
		return;
	}
}
// System.Void UniRx.Operators.DoOnCancelObservable`1/DoOnCancel<System.Object>::OnCompleted()
extern "C"  void DoOnCancel_OnCompleted_m3129982721_gshared (DoOnCancel_t3043692509 * __this, const MethodInfo* method)
{
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		__this->set_isCompletedCall_3((bool)1);
	}

IL_0007:
	try
	{ // begin try (depth: 1)
		Il2CppObject* L_0 = (Il2CppObject*)((OperatorObserverBase_2_t1187768149 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		NullCheck((Il2CppObject*)L_0);
		InterfaceActionInvoker0::Invoke(0 /* System.Void UniRx.IObserver`1<System.Object>::OnCompleted() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4), (Il2CppObject*)L_0);
		IL2CPP_LEAVE(0x20, FINALLY_0019);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0019;
	}

FINALLY_0019:
	{ // begin finally (depth: 1)
		NullCheck((OperatorObserverBase_2_t1187768149 *)__this);
		VirtActionInvoker0::Invoke(4 /* System.Void UniRx.Operators.OperatorObserverBase`2<System.Object,System.Object>::Dispose() */, (OperatorObserverBase_2_t1187768149 *)__this);
		IL2CPP_END_FINALLY(25)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(25)
	{
		IL2CPP_JUMP_TBL(0x20, IL_0020)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0020:
	{
		return;
	}
}
// System.Void UniRx.Operators.DoOnCancelObservable`1/DoOnCancel<System.Object>::<Run>m__7B()
extern "C"  void DoOnCancel_U3CRunU3Em__7B_m3066567414_gshared (DoOnCancel_t3043692509 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (bool)__this->get_isCompletedCall_3();
		if (L_0)
		{
			goto IL_001b;
		}
	}
	{
		DoOnCancelObservable_1_t3202318454 * L_1 = (DoOnCancelObservable_1_t3202318454 *)__this->get_parent_2();
		NullCheck(L_1);
		Action_t437523947 * L_2 = (Action_t437523947 *)L_1->get_onCancel_2();
		NullCheck((Action_t437523947 *)L_2);
		Action_Invoke_m1445970038((Action_t437523947 *)L_2, /*hidden argument*/NULL);
	}

IL_001b:
	{
		return;
	}
}
// System.Void UniRx.Operators.DoOnCancelObservable`1<System.Object>::.ctor(UniRx.IObservable`1<T>,System.Action)
extern "C"  void DoOnCancelObservable_1__ctor_m1242037632_gshared (DoOnCancelObservable_1_t3202318454 * __this, Il2CppObject* ___source0, Action_t437523947 * ___onCancel1, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = ___source0;
		bool L_1 = ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)(NULL /*static, unused*/, (Il2CppObject*)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		NullCheck((OperatorObservableBase_1_t4196218687 *)__this);
		((  void (*) (OperatorObservableBase_1_t4196218687 *, bool, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)((OperatorObservableBase_1_t4196218687 *)__this, (bool)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		Il2CppObject* L_2 = ___source0;
		__this->set_source_1(L_2);
		Action_t437523947 * L_3 = ___onCancel1;
		__this->set_onCancel_2(L_3);
		return;
	}
}
// System.IDisposable UniRx.Operators.DoOnCancelObservable`1<System.Object>::SubscribeCore(UniRx.IObserver`1<T>,System.IDisposable)
extern "C"  Il2CppObject * DoOnCancelObservable_1_SubscribeCore_m2764856862_gshared (DoOnCancelObservable_1_t3202318454 * __this, Il2CppObject* ___observer0, Il2CppObject * ___cancel1, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = ___observer0;
		Il2CppObject * L_1 = ___cancel1;
		DoOnCancel_t3043692509 * L_2 = (DoOnCancel_t3043692509 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		((  void (*) (DoOnCancel_t3043692509 *, DoOnCancelObservable_1_t3202318454 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)(L_2, (DoOnCancelObservable_1_t3202318454 *)__this, (Il2CppObject*)L_0, (Il2CppObject *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		NullCheck((DoOnCancel_t3043692509 *)L_2);
		Il2CppObject * L_3 = ((  Il2CppObject * (*) (DoOnCancel_t3043692509 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->method)((DoOnCancel_t3043692509 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		return L_3;
	}
}
// System.Void UniRx.Operators.DoOnCompletedObservable`1/DoOnCompleted<System.Object>::.ctor(UniRx.Operators.DoOnCompletedObservable`1<T>,UniRx.IObserver`1<T>,System.IDisposable)
extern "C"  void DoOnCompleted__ctor_m3030253215_gshared (DoOnCompleted_t4100057170 * __this, DoOnCompletedObservable_1_t3425980779 * ___parent0, Il2CppObject* ___observer1, Il2CppObject * ___cancel2, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = ___observer1;
		Il2CppObject * L_1 = ___cancel2;
		NullCheck((OperatorObserverBase_2_t1187768149 *)__this);
		((  void (*) (OperatorObserverBase_2_t1187768149 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((OperatorObserverBase_2_t1187768149 *)__this, (Il2CppObject*)L_0, (Il2CppObject *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		DoOnCompletedObservable_1_t3425980779 * L_2 = ___parent0;
		__this->set_parent_2(L_2);
		return;
	}
}
// System.IDisposable UniRx.Operators.DoOnCompletedObservable`1/DoOnCompleted<System.Object>::Run()
extern "C"  Il2CppObject * DoOnCompleted_Run_m3422597105_gshared (DoOnCompleted_t4100057170 * __this, const MethodInfo* method)
{
	{
		DoOnCompletedObservable_1_t3425980779 * L_0 = (DoOnCompletedObservable_1_t3425980779 *)__this->get_parent_2();
		NullCheck(L_0);
		Il2CppObject* L_1 = (Il2CppObject*)L_0->get_source_1();
		NullCheck((Il2CppObject*)L_1);
		Il2CppObject * L_2 = InterfaceFuncInvoker1< Il2CppObject *, Il2CppObject* >::Invoke(0 /* System.IDisposable UniRx.IObservable`1<System.Object>::Subscribe(UniRx.IObserver`1<T>) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), (Il2CppObject*)L_1, (Il2CppObject*)__this);
		return L_2;
	}
}
// System.Void UniRx.Operators.DoOnCompletedObservable`1/DoOnCompleted<System.Object>::OnNext(T)
extern "C"  void DoOnCompleted_OnNext_m1431881803_gshared (DoOnCompleted_t4100057170 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = (Il2CppObject*)((OperatorObserverBase_2_t1187768149 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		Il2CppObject * L_1 = ___value0;
		NullCheck((Il2CppObject*)L_0);
		InterfaceActionInvoker1< Il2CppObject * >::Invoke(2 /* System.Void UniRx.IObserver`1<System.Object>::OnNext(T) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3), (Il2CppObject*)L_0, (Il2CppObject *)L_1);
		return;
	}
}
// System.Void UniRx.Operators.DoOnCompletedObservable`1/DoOnCompleted<System.Object>::OnError(System.Exception)
extern "C"  void DoOnCompleted_OnError_m3889229146_gshared (DoOnCompleted_t4100057170 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method)
{
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		Il2CppObject* L_0 = (Il2CppObject*)((OperatorObserverBase_2_t1187768149 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		Exception_t1967233988 * L_1 = ___error0;
		NullCheck((Il2CppObject*)L_0);
		InterfaceActionInvoker1< Exception_t1967233988 * >::Invoke(1 /* System.Void UniRx.IObserver`1<System.Object>::OnError(System.Exception) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3), (Il2CppObject*)L_0, (Exception_t1967233988 *)L_1);
		IL2CPP_LEAVE(0x1A, FINALLY_0013);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0013;
	}

FINALLY_0013:
	{ // begin finally (depth: 1)
		NullCheck((OperatorObserverBase_2_t1187768149 *)__this);
		VirtActionInvoker0::Invoke(4 /* System.Void UniRx.Operators.OperatorObserverBase`2<System.Object,System.Object>::Dispose() */, (OperatorObserverBase_2_t1187768149 *)__this);
		IL2CPP_END_FINALLY(19)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(19)
	{
		IL2CPP_JUMP_TBL(0x1A, IL_001a)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_001a:
	{
		return;
	}
}
// System.Void UniRx.Operators.DoOnCompletedObservable`1/DoOnCompleted<System.Object>::OnCompleted()
extern Il2CppClass* Exception_t1967233988_il2cpp_TypeInfo_var;
extern const uint32_t DoOnCompleted_OnCompleted_m1155880877_MetadataUsageId;
extern "C"  void DoOnCompleted_OnCompleted_m1155880877_gshared (DoOnCompleted_t4100057170 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DoOnCompleted_OnCompleted_m1155880877_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Exception_t1967233988 * V_0 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		DoOnCompletedObservable_1_t3425980779 * L_0 = (DoOnCompletedObservable_1_t3425980779 *)__this->get_parent_2();
		NullCheck(L_0);
		Action_t437523947 * L_1 = (Action_t437523947 *)L_0->get_onCompleted_2();
		NullCheck((Action_t437523947 *)L_1);
		Action_Invoke_m1445970038((Action_t437523947 *)L_1, /*hidden argument*/NULL);
		goto IL_0034;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1967233988 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Exception_t1967233988_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_0015;
		throw e;
	}

CATCH_0015:
	{ // begin catch(System.Exception)
		{
			V_0 = (Exception_t1967233988 *)((Exception_t1967233988 *)__exception_local);
			Il2CppObject* L_2 = (Il2CppObject*)((OperatorObserverBase_2_t1187768149 *)__this)->get_observer_0();
			il2cpp_codegen_memory_barrier();
			Exception_t1967233988 * L_3 = V_0;
			NullCheck((Il2CppObject*)L_2);
			InterfaceActionInvoker1< Exception_t1967233988 * >::Invoke(1 /* System.Void UniRx.IObserver`1<System.Object>::OnError(System.Exception) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3), (Il2CppObject*)L_2, (Exception_t1967233988 *)L_3);
			NullCheck((OperatorObserverBase_2_t1187768149 *)__this);
			VirtActionInvoker0::Invoke(4 /* System.Void UniRx.Operators.OperatorObserverBase`2<System.Object,System.Object>::Dispose() */, (OperatorObserverBase_2_t1187768149 *)__this);
			goto IL_004d;
		}

IL_002f:
		{
			; // IL_002f: leave IL_0034
		}
	} // end catch (depth: 1)

IL_0034:
	try
	{ // begin try (depth: 1)
		Il2CppObject* L_4 = (Il2CppObject*)((OperatorObserverBase_2_t1187768149 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		NullCheck((Il2CppObject*)L_4);
		InterfaceActionInvoker0::Invoke(0 /* System.Void UniRx.IObserver`1<System.Object>::OnCompleted() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3), (Il2CppObject*)L_4);
		IL2CPP_LEAVE(0x4D, FINALLY_0046);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0046;
	}

FINALLY_0046:
	{ // begin finally (depth: 1)
		NullCheck((OperatorObserverBase_2_t1187768149 *)__this);
		VirtActionInvoker0::Invoke(4 /* System.Void UniRx.Operators.OperatorObserverBase`2<System.Object,System.Object>::Dispose() */, (OperatorObserverBase_2_t1187768149 *)__this);
		IL2CPP_END_FINALLY(70)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(70)
	{
		IL2CPP_JUMP_TBL(0x4D, IL_004d)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_004d:
	{
		return;
	}
}
// System.Void UniRx.Operators.DoOnCompletedObservable`1<System.Object>::.ctor(UniRx.IObservable`1<T>,System.Action)
extern "C"  void DoOnCompletedObservable_1__ctor_m2098452999_gshared (DoOnCompletedObservable_1_t3425980779 * __this, Il2CppObject* ___source0, Action_t437523947 * ___onCompleted1, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = ___source0;
		bool L_1 = ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)(NULL /*static, unused*/, (Il2CppObject*)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		NullCheck((OperatorObservableBase_1_t4196218687 *)__this);
		((  void (*) (OperatorObservableBase_1_t4196218687 *, bool, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)((OperatorObservableBase_1_t4196218687 *)__this, (bool)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		Il2CppObject* L_2 = ___source0;
		__this->set_source_1(L_2);
		Action_t437523947 * L_3 = ___onCompleted1;
		__this->set_onCompleted_2(L_3);
		return;
	}
}
// System.IDisposable UniRx.Operators.DoOnCompletedObservable`1<System.Object>::SubscribeCore(UniRx.IObserver`1<T>,System.IDisposable)
extern "C"  Il2CppObject * DoOnCompletedObservable_1_SubscribeCore_m2149812173_gshared (DoOnCompletedObservable_1_t3425980779 * __this, Il2CppObject* ___observer0, Il2CppObject * ___cancel1, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = ___observer0;
		Il2CppObject * L_1 = ___cancel1;
		DoOnCompleted_t4100057170 * L_2 = (DoOnCompleted_t4100057170 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		((  void (*) (DoOnCompleted_t4100057170 *, DoOnCompletedObservable_1_t3425980779 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)(L_2, (DoOnCompletedObservable_1_t3425980779 *)__this, (Il2CppObject*)L_0, (Il2CppObject *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		NullCheck((DoOnCompleted_t4100057170 *)L_2);
		Il2CppObject * L_3 = ((  Il2CppObject * (*) (DoOnCompleted_t4100057170 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->method)((DoOnCompleted_t4100057170 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		return L_3;
	}
}
// System.Void UniRx.Operators.DoOnErrorObservable`1/DoOnError<System.Object>::.ctor(UniRx.Operators.DoOnErrorObservable`1<T>,UniRx.IObserver`1<T>,System.IDisposable)
extern "C"  void DoOnError__ctor_m3425299350_gshared (DoOnError_t3716721415 * __this, DoOnErrorObservable_1_t434875552 * ___parent0, Il2CppObject* ___observer1, Il2CppObject * ___cancel2, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = ___observer1;
		Il2CppObject * L_1 = ___cancel2;
		NullCheck((OperatorObserverBase_2_t1187768149 *)__this);
		((  void (*) (OperatorObserverBase_2_t1187768149 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((OperatorObserverBase_2_t1187768149 *)__this, (Il2CppObject*)L_0, (Il2CppObject *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		DoOnErrorObservable_1_t434875552 * L_2 = ___parent0;
		__this->set_parent_2(L_2);
		return;
	}
}
// System.IDisposable UniRx.Operators.DoOnErrorObservable`1/DoOnError<System.Object>::Run()
extern "C"  Il2CppObject * DoOnError_Run_m1586640683_gshared (DoOnError_t3716721415 * __this, const MethodInfo* method)
{
	{
		DoOnErrorObservable_1_t434875552 * L_0 = (DoOnErrorObservable_1_t434875552 *)__this->get_parent_2();
		NullCheck(L_0);
		Il2CppObject* L_1 = (Il2CppObject*)L_0->get_source_1();
		NullCheck((Il2CppObject*)L_1);
		Il2CppObject * L_2 = InterfaceFuncInvoker1< Il2CppObject *, Il2CppObject* >::Invoke(0 /* System.IDisposable UniRx.IObservable`1<System.Object>::Subscribe(UniRx.IObserver`1<T>) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), (Il2CppObject*)L_1, (Il2CppObject*)__this);
		return L_2;
	}
}
// System.Void UniRx.Operators.DoOnErrorObservable`1/DoOnError<System.Object>::OnNext(T)
extern "C"  void DoOnError_OnNext_m288173445_gshared (DoOnError_t3716721415 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = (Il2CppObject*)((OperatorObserverBase_2_t1187768149 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		Il2CppObject * L_1 = ___value0;
		NullCheck((Il2CppObject*)L_0);
		InterfaceActionInvoker1< Il2CppObject * >::Invoke(2 /* System.Void UniRx.IObserver`1<System.Object>::OnNext(T) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3), (Il2CppObject*)L_0, (Il2CppObject *)L_1);
		return;
	}
}
// System.Void UniRx.Operators.DoOnErrorObservable`1/DoOnError<System.Object>::OnError(System.Exception)
extern Il2CppClass* Exception_t1967233988_il2cpp_TypeInfo_var;
extern const MethodInfo* Action_1_Invoke_m1286428014_MethodInfo_var;
extern const uint32_t DoOnError_OnError_m3333922452_MetadataUsageId;
extern "C"  void DoOnError_OnError_m3333922452_gshared (DoOnError_t3716721415 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DoOnError_OnError_m3333922452_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Exception_t1967233988 * V_0 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		DoOnErrorObservable_1_t434875552 * L_0 = (DoOnErrorObservable_1_t434875552 *)__this->get_parent_2();
		NullCheck(L_0);
		Action_1_t2115686693 * L_1 = (Action_1_t2115686693 *)L_0->get_onError_2();
		Exception_t1967233988 * L_2 = ___error0;
		NullCheck((Action_1_t2115686693 *)L_1);
		Action_1_Invoke_m1286428014((Action_1_t2115686693 *)L_1, (Exception_t1967233988 *)L_2, /*hidden argument*/Action_1_Invoke_m1286428014_MethodInfo_var);
		goto IL_003b;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1967233988 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Exception_t1967233988_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_0016;
		throw e;
	}

CATCH_0016:
	{ // begin catch(System.Exception)
		{
			V_0 = (Exception_t1967233988 *)((Exception_t1967233988 *)__exception_local);
		}

IL_0017:
		try
		{ // begin try (depth: 2)
			Il2CppObject* L_3 = (Il2CppObject*)((OperatorObserverBase_2_t1187768149 *)__this)->get_observer_0();
			il2cpp_codegen_memory_barrier();
			Exception_t1967233988 * L_4 = V_0;
			NullCheck((Il2CppObject*)L_3);
			InterfaceActionInvoker1< Exception_t1967233988 * >::Invoke(1 /* System.Void UniRx.IObserver`1<System.Object>::OnError(System.Exception) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3), (Il2CppObject*)L_3, (Exception_t1967233988 *)L_4);
			IL2CPP_LEAVE(0x31, FINALLY_002a);
		} // end try (depth: 2)
		catch(Il2CppExceptionWrapper& e)
		{
			__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
			goto FINALLY_002a;
		}

FINALLY_002a:
		{ // begin finally (depth: 2)
			NullCheck((OperatorObserverBase_2_t1187768149 *)__this);
			VirtActionInvoker0::Invoke(4 /* System.Void UniRx.Operators.OperatorObserverBase`2<System.Object,System.Object>::Dispose() */, (OperatorObserverBase_2_t1187768149 *)__this);
			IL2CPP_END_FINALLY(42)
		} // end finally (depth: 2)
		IL2CPP_CLEANUP(42)
		{
			IL2CPP_JUMP_TBL(0x31, IL_0031)
			IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
		}

IL_0031:
		{
			goto IL_0055;
		}

IL_0036:
		{
			; // IL_0036: leave IL_003b
		}
	} // end catch (depth: 1)

IL_003b:
	try
	{ // begin try (depth: 1)
		Il2CppObject* L_5 = (Il2CppObject*)((OperatorObserverBase_2_t1187768149 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		Exception_t1967233988 * L_6 = ___error0;
		NullCheck((Il2CppObject*)L_5);
		InterfaceActionInvoker1< Exception_t1967233988 * >::Invoke(1 /* System.Void UniRx.IObserver`1<System.Object>::OnError(System.Exception) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3), (Il2CppObject*)L_5, (Exception_t1967233988 *)L_6);
		IL2CPP_LEAVE(0x55, FINALLY_004e);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_004e;
	}

FINALLY_004e:
	{ // begin finally (depth: 1)
		NullCheck((OperatorObserverBase_2_t1187768149 *)__this);
		VirtActionInvoker0::Invoke(4 /* System.Void UniRx.Operators.OperatorObserverBase`2<System.Object,System.Object>::Dispose() */, (OperatorObserverBase_2_t1187768149 *)__this);
		IL2CPP_END_FINALLY(78)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(78)
	{
		IL2CPP_JUMP_TBL(0x55, IL_0055)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0055:
	{
		return;
	}
}
// System.Void UniRx.Operators.DoOnErrorObservable`1/DoOnError<System.Object>::OnCompleted()
extern "C"  void DoOnError_OnCompleted_m2301661159_gshared (DoOnError_t3716721415 * __this, const MethodInfo* method)
{
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		Il2CppObject* L_0 = (Il2CppObject*)((OperatorObserverBase_2_t1187768149 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		NullCheck((Il2CppObject*)L_0);
		InterfaceActionInvoker0::Invoke(0 /* System.Void UniRx.IObserver`1<System.Object>::OnCompleted() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3), (Il2CppObject*)L_0);
		IL2CPP_LEAVE(0x19, FINALLY_0012);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0012;
	}

FINALLY_0012:
	{ // begin finally (depth: 1)
		NullCheck((OperatorObserverBase_2_t1187768149 *)__this);
		VirtActionInvoker0::Invoke(4 /* System.Void UniRx.Operators.OperatorObserverBase`2<System.Object,System.Object>::Dispose() */, (OperatorObserverBase_2_t1187768149 *)__this);
		IL2CPP_END_FINALLY(18)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(18)
	{
		IL2CPP_JUMP_TBL(0x19, IL_0019)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0019:
	{
		return;
	}
}
// System.Void UniRx.Operators.DoOnErrorObservable`1<System.Object>::.ctor(UniRx.IObservable`1<T>,System.Action`1<System.Exception>)
extern "C"  void DoOnErrorObservable_1__ctor_m1696072545_gshared (DoOnErrorObservable_1_t434875552 * __this, Il2CppObject* ___source0, Action_1_t2115686693 * ___onError1, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = ___source0;
		bool L_1 = ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)(NULL /*static, unused*/, (Il2CppObject*)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		NullCheck((OperatorObservableBase_1_t4196218687 *)__this);
		((  void (*) (OperatorObservableBase_1_t4196218687 *, bool, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)((OperatorObservableBase_1_t4196218687 *)__this, (bool)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		Il2CppObject* L_2 = ___source0;
		__this->set_source_1(L_2);
		Action_1_t2115686693 * L_3 = ___onError1;
		__this->set_onError_2(L_3);
		return;
	}
}
// System.IDisposable UniRx.Operators.DoOnErrorObservable`1<System.Object>::SubscribeCore(UniRx.IObserver`1<T>,System.IDisposable)
extern "C"  Il2CppObject * DoOnErrorObservable_1_SubscribeCore_m3106849008_gshared (DoOnErrorObservable_1_t434875552 * __this, Il2CppObject* ___observer0, Il2CppObject * ___cancel1, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = ___observer0;
		Il2CppObject * L_1 = ___cancel1;
		DoOnError_t3716721415 * L_2 = (DoOnError_t3716721415 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		((  void (*) (DoOnError_t3716721415 *, DoOnErrorObservable_1_t434875552 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)(L_2, (DoOnErrorObservable_1_t434875552 *)__this, (Il2CppObject*)L_0, (Il2CppObject *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		NullCheck((DoOnError_t3716721415 *)L_2);
		Il2CppObject * L_3 = ((  Il2CppObject * (*) (DoOnError_t3716721415 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->method)((DoOnError_t3716721415 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		return L_3;
	}
}
// System.Void UniRx.Operators.DoOnSubscribeObservable`1/DoOnSubscribe<System.Object>::.ctor(UniRx.Operators.DoOnSubscribeObservable`1<T>,UniRx.IObserver`1<T>,System.IDisposable)
extern "C"  void DoOnSubscribe__ctor_m346588380_gshared (DoOnSubscribe_t871059449 * __this, DoOnSubscribeObservable_1_t1431598738 * ___parent0, Il2CppObject* ___observer1, Il2CppObject * ___cancel2, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = ___observer1;
		Il2CppObject * L_1 = ___cancel2;
		NullCheck((OperatorObserverBase_2_t1187768149 *)__this);
		((  void (*) (OperatorObserverBase_2_t1187768149 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((OperatorObserverBase_2_t1187768149 *)__this, (Il2CppObject*)L_0, (Il2CppObject *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		DoOnSubscribeObservable_1_t1431598738 * L_2 = ___parent0;
		__this->set_parent_2(L_2);
		return;
	}
}
// System.IDisposable UniRx.Operators.DoOnSubscribeObservable`1/DoOnSubscribe<System.Object>::Run()
extern Il2CppClass* Exception_t1967233988_il2cpp_TypeInfo_var;
extern Il2CppClass* Disposable_t3736388786_il2cpp_TypeInfo_var;
extern const uint32_t DoOnSubscribe_Run_m1872890543_MetadataUsageId;
extern "C"  Il2CppObject * DoOnSubscribe_Run_m1872890543_gshared (DoOnSubscribe_t871059449 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DoOnSubscribe_Run_m1872890543_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Exception_t1967233988 * V_0 = NULL;
	Il2CppObject * V_1 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		DoOnSubscribeObservable_1_t1431598738 * L_0 = (DoOnSubscribeObservable_1_t1431598738 *)__this->get_parent_2();
		NullCheck(L_0);
		Action_t437523947 * L_1 = (Action_t437523947 *)L_0->get_onSubscribe_2();
		NullCheck((Action_t437523947 *)L_1);
		Action_Invoke_m1445970038((Action_t437523947 *)L_1, /*hidden argument*/NULL);
		goto IL_0040;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1967233988 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Exception_t1967233988_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_0015;
		throw e;
	}

CATCH_0015:
	{ // begin catch(System.Exception)
		{
			V_0 = (Exception_t1967233988 *)((Exception_t1967233988 *)__exception_local);
		}

IL_0016:
		try
		{ // begin try (depth: 2)
			Il2CppObject* L_2 = (Il2CppObject*)((OperatorObserverBase_2_t1187768149 *)__this)->get_observer_0();
			il2cpp_codegen_memory_barrier();
			Exception_t1967233988 * L_3 = V_0;
			NullCheck((Il2CppObject*)L_2);
			InterfaceActionInvoker1< Exception_t1967233988 * >::Invoke(1 /* System.Void UniRx.IObserver`1<System.Object>::OnError(System.Exception) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), (Il2CppObject*)L_2, (Exception_t1967233988 *)L_3);
			IL2CPP_LEAVE(0x30, FINALLY_0029);
		} // end try (depth: 2)
		catch(Il2CppExceptionWrapper& e)
		{
			__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
			goto FINALLY_0029;
		}

FINALLY_0029:
		{ // begin finally (depth: 2)
			NullCheck((OperatorObserverBase_2_t1187768149 *)__this);
			VirtActionInvoker0::Invoke(4 /* System.Void UniRx.Operators.OperatorObserverBase`2<System.Object,System.Object>::Dispose() */, (OperatorObserverBase_2_t1187768149 *)__this);
			IL2CPP_END_FINALLY(41)
		} // end finally (depth: 2)
		IL2CPP_CLEANUP(41)
		{
			IL2CPP_JUMP_TBL(0x30, IL_0030)
			IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
		}

IL_0030:
		{
			IL2CPP_RUNTIME_CLASS_INIT(Disposable_t3736388786_il2cpp_TypeInfo_var);
			Il2CppObject * L_4 = ((Disposable_t3736388786_StaticFields*)Disposable_t3736388786_il2cpp_TypeInfo_var->static_fields)->get_Empty_0();
			V_1 = (Il2CppObject *)L_4;
			goto IL_0052;
		}

IL_003b:
		{
			; // IL_003b: leave IL_0040
		}
	} // end catch (depth: 1)

IL_0040:
	{
		DoOnSubscribeObservable_1_t1431598738 * L_5 = (DoOnSubscribeObservable_1_t1431598738 *)__this->get_parent_2();
		NullCheck(L_5);
		Il2CppObject* L_6 = (Il2CppObject*)L_5->get_source_1();
		NullCheck((Il2CppObject*)L_6);
		Il2CppObject * L_7 = InterfaceFuncInvoker1< Il2CppObject *, Il2CppObject* >::Invoke(0 /* System.IDisposable UniRx.IObservable`1<System.Object>::Subscribe(UniRx.IObserver`1<T>) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4), (Il2CppObject*)L_6, (Il2CppObject*)__this);
		return L_7;
	}

IL_0052:
	{
		Il2CppObject * L_8 = V_1;
		return L_8;
	}
}
// System.Void UniRx.Operators.DoOnSubscribeObservable`1/DoOnSubscribe<System.Object>::OnNext(T)
extern "C"  void DoOnSubscribe_OnNext_m1060279305_gshared (DoOnSubscribe_t871059449 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = (Il2CppObject*)((OperatorObserverBase_2_t1187768149 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		Il2CppObject * L_1 = ___value0;
		NullCheck((Il2CppObject*)L_0);
		InterfaceActionInvoker1< Il2CppObject * >::Invoke(2 /* System.Void UniRx.IObserver`1<System.Object>::OnNext(T) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), (Il2CppObject*)L_0, (Il2CppObject *)L_1);
		return;
	}
}
// System.Void UniRx.Operators.DoOnSubscribeObservable`1/DoOnSubscribe<System.Object>::OnError(System.Exception)
extern "C"  void DoOnSubscribe_OnError_m2428992792_gshared (DoOnSubscribe_t871059449 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method)
{
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		Il2CppObject* L_0 = (Il2CppObject*)((OperatorObserverBase_2_t1187768149 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		Exception_t1967233988 * L_1 = ___error0;
		NullCheck((Il2CppObject*)L_0);
		InterfaceActionInvoker1< Exception_t1967233988 * >::Invoke(1 /* System.Void UniRx.IObserver`1<System.Object>::OnError(System.Exception) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), (Il2CppObject*)L_0, (Exception_t1967233988 *)L_1);
		IL2CPP_LEAVE(0x1A, FINALLY_0013);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0013;
	}

FINALLY_0013:
	{ // begin finally (depth: 1)
		NullCheck((OperatorObserverBase_2_t1187768149 *)__this);
		VirtActionInvoker0::Invoke(4 /* System.Void UniRx.Operators.OperatorObserverBase`2<System.Object,System.Object>::Dispose() */, (OperatorObserverBase_2_t1187768149 *)__this);
		IL2CPP_END_FINALLY(19)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(19)
	{
		IL2CPP_JUMP_TBL(0x1A, IL_001a)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_001a:
	{
		return;
	}
}
// System.Void UniRx.Operators.DoOnSubscribeObservable`1/DoOnSubscribe<System.Object>::OnCompleted()
extern "C"  void DoOnSubscribe_OnCompleted_m3512145003_gshared (DoOnSubscribe_t871059449 * __this, const MethodInfo* method)
{
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		Il2CppObject* L_0 = (Il2CppObject*)((OperatorObserverBase_2_t1187768149 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		NullCheck((Il2CppObject*)L_0);
		InterfaceActionInvoker0::Invoke(0 /* System.Void UniRx.IObserver`1<System.Object>::OnCompleted() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), (Il2CppObject*)L_0);
		IL2CPP_LEAVE(0x19, FINALLY_0012);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0012;
	}

FINALLY_0012:
	{ // begin finally (depth: 1)
		NullCheck((OperatorObserverBase_2_t1187768149 *)__this);
		VirtActionInvoker0::Invoke(4 /* System.Void UniRx.Operators.OperatorObserverBase`2<System.Object,System.Object>::Dispose() */, (OperatorObserverBase_2_t1187768149 *)__this);
		IL2CPP_END_FINALLY(18)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(18)
	{
		IL2CPP_JUMP_TBL(0x19, IL_0019)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0019:
	{
		return;
	}
}
// System.Void UniRx.Operators.DoOnSubscribeObservable`1<System.Object>::.ctor(UniRx.IObservable`1<T>,System.Action)
extern "C"  void DoOnSubscribeObservable_1__ctor_m1204279814_gshared (DoOnSubscribeObservable_1_t1431598738 * __this, Il2CppObject* ___source0, Action_t437523947 * ___onSubscribe1, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = ___source0;
		bool L_1 = ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)(NULL /*static, unused*/, (Il2CppObject*)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		NullCheck((OperatorObservableBase_1_t4196218687 *)__this);
		((  void (*) (OperatorObservableBase_1_t4196218687 *, bool, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)((OperatorObservableBase_1_t4196218687 *)__this, (bool)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		Il2CppObject* L_2 = ___source0;
		__this->set_source_1(L_2);
		Action_t437523947 * L_3 = ___onSubscribe1;
		__this->set_onSubscribe_2(L_3);
		return;
	}
}
// System.IDisposable UniRx.Operators.DoOnSubscribeObservable`1<System.Object>::SubscribeCore(UniRx.IObserver`1<T>,System.IDisposable)
extern "C"  Il2CppObject * DoOnSubscribeObservable_1_SubscribeCore_m3040731758_gshared (DoOnSubscribeObservable_1_t1431598738 * __this, Il2CppObject* ___observer0, Il2CppObject * ___cancel1, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = ___observer0;
		Il2CppObject * L_1 = ___cancel1;
		DoOnSubscribe_t871059449 * L_2 = (DoOnSubscribe_t871059449 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		((  void (*) (DoOnSubscribe_t871059449 *, DoOnSubscribeObservable_1_t1431598738 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)(L_2, (DoOnSubscribeObservable_1_t1431598738 *)__this, (Il2CppObject*)L_0, (Il2CppObject *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		NullCheck((DoOnSubscribe_t871059449 *)L_2);
		Il2CppObject * L_3 = ((  Il2CppObject * (*) (DoOnSubscribe_t871059449 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->method)((DoOnSubscribe_t871059449 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		return L_3;
	}
}
// System.Void UniRx.Operators.DoOnTerminateObservable`1/DoOnTerminate<System.Object>::.ctor(UniRx.Operators.DoOnTerminateObservable`1<T>,UniRx.IObserver`1<T>,System.IDisposable)
extern "C"  void DoOnTerminate__ctor_m2580601697_gshared (DoOnTerminate_t2350010872 * __this, DoOnTerminateObservable_1_t3263525393 * ___parent0, Il2CppObject* ___observer1, Il2CppObject * ___cancel2, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = ___observer1;
		Il2CppObject * L_1 = ___cancel2;
		NullCheck((OperatorObserverBase_2_t1187768149 *)__this);
		((  void (*) (OperatorObserverBase_2_t1187768149 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((OperatorObserverBase_2_t1187768149 *)__this, (Il2CppObject*)L_0, (Il2CppObject *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		DoOnTerminateObservable_1_t3263525393 * L_2 = ___parent0;
		__this->set_parent_2(L_2);
		return;
	}
}
// System.IDisposable UniRx.Operators.DoOnTerminateObservable`1/DoOnTerminate<System.Object>::Run()
extern "C"  Il2CppObject * DoOnTerminate_Run_m3579094941_gshared (DoOnTerminate_t2350010872 * __this, const MethodInfo* method)
{
	{
		DoOnTerminateObservable_1_t3263525393 * L_0 = (DoOnTerminateObservable_1_t3263525393 *)__this->get_parent_2();
		NullCheck(L_0);
		Il2CppObject* L_1 = (Il2CppObject*)L_0->get_source_1();
		NullCheck((Il2CppObject*)L_1);
		Il2CppObject * L_2 = InterfaceFuncInvoker1< Il2CppObject *, Il2CppObject* >::Invoke(0 /* System.IDisposable UniRx.IObservable`1<System.Object>::Subscribe(UniRx.IObserver`1<T>) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), (Il2CppObject*)L_1, (Il2CppObject*)__this);
		return L_2;
	}
}
// System.Void UniRx.Operators.DoOnTerminateObservable`1/DoOnTerminate<System.Object>::OnNext(T)
extern "C"  void DoOnTerminate_OnNext_m525404663_gshared (DoOnTerminate_t2350010872 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = (Il2CppObject*)((OperatorObserverBase_2_t1187768149 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		Il2CppObject * L_1 = ___value0;
		NullCheck((Il2CppObject*)L_0);
		InterfaceActionInvoker1< Il2CppObject * >::Invoke(2 /* System.Void UniRx.IObserver`1<System.Object>::OnNext(T) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3), (Il2CppObject*)L_0, (Il2CppObject *)L_1);
		return;
	}
}
// System.Void UniRx.Operators.DoOnTerminateObservable`1/DoOnTerminate<System.Object>::OnError(System.Exception)
extern Il2CppClass* Exception_t1967233988_il2cpp_TypeInfo_var;
extern const uint32_t DoOnTerminate_OnError_m357062406_MetadataUsageId;
extern "C"  void DoOnTerminate_OnError_m357062406_gshared (DoOnTerminate_t2350010872 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DoOnTerminate_OnError_m357062406_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Exception_t1967233988 * V_0 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		DoOnTerminateObservable_1_t3263525393 * L_0 = (DoOnTerminateObservable_1_t3263525393 *)__this->get_parent_2();
		NullCheck(L_0);
		Action_t437523947 * L_1 = (Action_t437523947 *)L_0->get_onTerminate_2();
		NullCheck((Action_t437523947 *)L_1);
		Action_Invoke_m1445970038((Action_t437523947 *)L_1, /*hidden argument*/NULL);
		goto IL_003a;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1967233988 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Exception_t1967233988_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_0015;
		throw e;
	}

CATCH_0015:
	{ // begin catch(System.Exception)
		{
			V_0 = (Exception_t1967233988 *)((Exception_t1967233988 *)__exception_local);
		}

IL_0016:
		try
		{ // begin try (depth: 2)
			Il2CppObject* L_2 = (Il2CppObject*)((OperatorObserverBase_2_t1187768149 *)__this)->get_observer_0();
			il2cpp_codegen_memory_barrier();
			Exception_t1967233988 * L_3 = V_0;
			NullCheck((Il2CppObject*)L_2);
			InterfaceActionInvoker1< Exception_t1967233988 * >::Invoke(1 /* System.Void UniRx.IObserver`1<System.Object>::OnError(System.Exception) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3), (Il2CppObject*)L_2, (Exception_t1967233988 *)L_3);
			IL2CPP_LEAVE(0x30, FINALLY_0029);
		} // end try (depth: 2)
		catch(Il2CppExceptionWrapper& e)
		{
			__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
			goto FINALLY_0029;
		}

FINALLY_0029:
		{ // begin finally (depth: 2)
			NullCheck((OperatorObserverBase_2_t1187768149 *)__this);
			VirtActionInvoker0::Invoke(4 /* System.Void UniRx.Operators.OperatorObserverBase`2<System.Object,System.Object>::Dispose() */, (OperatorObserverBase_2_t1187768149 *)__this);
			IL2CPP_END_FINALLY(41)
		} // end finally (depth: 2)
		IL2CPP_CLEANUP(41)
		{
			IL2CPP_JUMP_TBL(0x30, IL_0030)
			IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
		}

IL_0030:
		{
			goto IL_0054;
		}

IL_0035:
		{
			; // IL_0035: leave IL_003a
		}
	} // end catch (depth: 1)

IL_003a:
	try
	{ // begin try (depth: 1)
		Il2CppObject* L_4 = (Il2CppObject*)((OperatorObserverBase_2_t1187768149 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		Exception_t1967233988 * L_5 = ___error0;
		NullCheck((Il2CppObject*)L_4);
		InterfaceActionInvoker1< Exception_t1967233988 * >::Invoke(1 /* System.Void UniRx.IObserver`1<System.Object>::OnError(System.Exception) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3), (Il2CppObject*)L_4, (Exception_t1967233988 *)L_5);
		IL2CPP_LEAVE(0x54, FINALLY_004d);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_004d;
	}

FINALLY_004d:
	{ // begin finally (depth: 1)
		NullCheck((OperatorObserverBase_2_t1187768149 *)__this);
		VirtActionInvoker0::Invoke(4 /* System.Void UniRx.Operators.OperatorObserverBase`2<System.Object,System.Object>::Dispose() */, (OperatorObserverBase_2_t1187768149 *)__this);
		IL2CPP_END_FINALLY(77)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(77)
	{
		IL2CPP_JUMP_TBL(0x54, IL_0054)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0054:
	{
		return;
	}
}
// System.Void UniRx.Operators.DoOnTerminateObservable`1/DoOnTerminate<System.Object>::OnCompleted()
extern Il2CppClass* Exception_t1967233988_il2cpp_TypeInfo_var;
extern const uint32_t DoOnTerminate_OnCompleted_m4031570777_MetadataUsageId;
extern "C"  void DoOnTerminate_OnCompleted_m4031570777_gshared (DoOnTerminate_t2350010872 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DoOnTerminate_OnCompleted_m4031570777_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Exception_t1967233988 * V_0 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		DoOnTerminateObservable_1_t3263525393 * L_0 = (DoOnTerminateObservable_1_t3263525393 *)__this->get_parent_2();
		NullCheck(L_0);
		Action_t437523947 * L_1 = (Action_t437523947 *)L_0->get_onTerminate_2();
		NullCheck((Action_t437523947 *)L_1);
		Action_Invoke_m1445970038((Action_t437523947 *)L_1, /*hidden argument*/NULL);
		goto IL_0034;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1967233988 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Exception_t1967233988_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_0015;
		throw e;
	}

CATCH_0015:
	{ // begin catch(System.Exception)
		{
			V_0 = (Exception_t1967233988 *)((Exception_t1967233988 *)__exception_local);
			Il2CppObject* L_2 = (Il2CppObject*)((OperatorObserverBase_2_t1187768149 *)__this)->get_observer_0();
			il2cpp_codegen_memory_barrier();
			Exception_t1967233988 * L_3 = V_0;
			NullCheck((Il2CppObject*)L_2);
			InterfaceActionInvoker1< Exception_t1967233988 * >::Invoke(1 /* System.Void UniRx.IObserver`1<System.Object>::OnError(System.Exception) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3), (Il2CppObject*)L_2, (Exception_t1967233988 *)L_3);
			NullCheck((OperatorObserverBase_2_t1187768149 *)__this);
			VirtActionInvoker0::Invoke(4 /* System.Void UniRx.Operators.OperatorObserverBase`2<System.Object,System.Object>::Dispose() */, (OperatorObserverBase_2_t1187768149 *)__this);
			goto IL_004d;
		}

IL_002f:
		{
			; // IL_002f: leave IL_0034
		}
	} // end catch (depth: 1)

IL_0034:
	try
	{ // begin try (depth: 1)
		Il2CppObject* L_4 = (Il2CppObject*)((OperatorObserverBase_2_t1187768149 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		NullCheck((Il2CppObject*)L_4);
		InterfaceActionInvoker0::Invoke(0 /* System.Void UniRx.IObserver`1<System.Object>::OnCompleted() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3), (Il2CppObject*)L_4);
		IL2CPP_LEAVE(0x4D, FINALLY_0046);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0046;
	}

FINALLY_0046:
	{ // begin finally (depth: 1)
		NullCheck((OperatorObserverBase_2_t1187768149 *)__this);
		VirtActionInvoker0::Invoke(4 /* System.Void UniRx.Operators.OperatorObserverBase`2<System.Object,System.Object>::Dispose() */, (OperatorObserverBase_2_t1187768149 *)__this);
		IL2CPP_END_FINALLY(70)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(70)
	{
		IL2CPP_JUMP_TBL(0x4D, IL_004d)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_004d:
	{
		return;
	}
}
// System.Void UniRx.Operators.DoOnTerminateObservable`1<System.Object>::.ctor(UniRx.IObservable`1<T>,System.Action)
extern "C"  void DoOnTerminateObservable_1__ctor_m1619754013_gshared (DoOnTerminateObservable_1_t3263525393 * __this, Il2CppObject* ___source0, Action_t437523947 * ___onTerminate1, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = ___source0;
		bool L_1 = ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)(NULL /*static, unused*/, (Il2CppObject*)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		NullCheck((OperatorObservableBase_1_t4196218687 *)__this);
		((  void (*) (OperatorObservableBase_1_t4196218687 *, bool, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)((OperatorObservableBase_1_t4196218687 *)__this, (bool)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		Il2CppObject* L_2 = ___source0;
		__this->set_source_1(L_2);
		Action_t437523947 * L_3 = ___onTerminate1;
		__this->set_onTerminate_2(L_3);
		return;
	}
}
// System.IDisposable UniRx.Operators.DoOnTerminateObservable`1<System.Object>::SubscribeCore(UniRx.IObserver`1<T>,System.IDisposable)
extern "C"  Il2CppObject * DoOnTerminateObservable_1_SubscribeCore_m2956385271_gshared (DoOnTerminateObservable_1_t3263525393 * __this, Il2CppObject* ___observer0, Il2CppObject * ___cancel1, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = ___observer0;
		Il2CppObject * L_1 = ___cancel1;
		DoOnTerminate_t2350010872 * L_2 = (DoOnTerminate_t2350010872 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		((  void (*) (DoOnTerminate_t2350010872 *, DoOnTerminateObservable_1_t3263525393 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)(L_2, (DoOnTerminateObservable_1_t3263525393 *)__this, (Il2CppObject*)L_0, (Il2CppObject *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		NullCheck((DoOnTerminate_t2350010872 *)L_2);
		Il2CppObject * L_3 = ((  Il2CppObject * (*) (DoOnTerminate_t2350010872 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->method)((DoOnTerminate_t2350010872 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		return L_3;
	}
}
// System.Void UniRx.Operators.EmptyObservable`1/Empty<System.Double>::.ctor(UniRx.IObserver`1<T>,System.IDisposable)
extern "C"  void Empty__ctor_m1156529469_gshared (Empty_t2493334448 * __this, Il2CppObject* ___observer0, Il2CppObject * ___cancel1, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = ___observer0;
		Il2CppObject * L_1 = ___cancel1;
		NullCheck((OperatorObserverBase_2_t1921935565 *)__this);
		((  void (*) (OperatorObserverBase_2_t1921935565 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((OperatorObserverBase_2_t1921935565 *)__this, (Il2CppObject*)L_0, (Il2CppObject *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		return;
	}
}
// System.Void UniRx.Operators.EmptyObservable`1/Empty<System.Double>::OnNext(T)
extern Il2CppClass* Il2CppObject_il2cpp_TypeInfo_var;
extern const uint32_t Empty_OnNext_m123612213_MetadataUsageId;
extern "C"  void Empty_OnNext_m123612213_gshared (Empty_t2493334448 * __this, double ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Empty_OnNext_m123612213_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		Il2CppObject* L_0 = (Il2CppObject*)((OperatorObserverBase_2_t1921935565 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		double L_1 = ___value0;
		NullCheck((Il2CppObject*)L_0);
		InterfaceActionInvoker1< double >::Invoke(2 /* System.Void UniRx.IObserver`1<System.Double>::OnNext(T) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), (Il2CppObject*)L_0, (double)L_1);
		goto IL_0021;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1967233988 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Il2CppObject_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_0013;
		throw e;
	}

CATCH_0013:
	{ // begin catch(System.Object)
		{
			NullCheck((OperatorObserverBase_2_t1921935565 *)__this);
			VirtActionInvoker0::Invoke(4 /* System.Void UniRx.Operators.OperatorObserverBase`2<System.Double,System.Double>::Dispose() */, (OperatorObserverBase_2_t1921935565 *)__this);
			IL2CPP_RAISE_MANAGED_EXCEPTION(__exception_local);
		}

IL_001c:
		{
			goto IL_0021;
		}
	} // end catch (depth: 1)

IL_0021:
	{
		return;
	}
}
// System.Void UniRx.Operators.EmptyObservable`1/Empty<System.Double>::OnError(System.Exception)
extern "C"  void Empty_OnError_m1542798660_gshared (Empty_t2493334448 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method)
{
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		Il2CppObject* L_0 = (Il2CppObject*)((OperatorObserverBase_2_t1921935565 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		Exception_t1967233988 * L_1 = ___error0;
		NullCheck((Il2CppObject*)L_0);
		InterfaceActionInvoker1< Exception_t1967233988 * >::Invoke(1 /* System.Void UniRx.IObserver`1<System.Double>::OnError(System.Exception) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), (Il2CppObject*)L_0, (Exception_t1967233988 *)L_1);
		IL2CPP_LEAVE(0x1A, FINALLY_0013);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0013;
	}

FINALLY_0013:
	{ // begin finally (depth: 1)
		NullCheck((OperatorObserverBase_2_t1921935565 *)__this);
		VirtActionInvoker0::Invoke(4 /* System.Void UniRx.Operators.OperatorObserverBase`2<System.Double,System.Double>::Dispose() */, (OperatorObserverBase_2_t1921935565 *)__this);
		IL2CPP_END_FINALLY(19)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(19)
	{
		IL2CPP_JUMP_TBL(0x1A, IL_001a)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_001a:
	{
		return;
	}
}
// System.Void UniRx.Operators.EmptyObservable`1/Empty<System.Double>::OnCompleted()
extern "C"  void Empty_OnCompleted_m3965892247_gshared (Empty_t2493334448 * __this, const MethodInfo* method)
{
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		Il2CppObject* L_0 = (Il2CppObject*)((OperatorObserverBase_2_t1921935565 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		NullCheck((Il2CppObject*)L_0);
		InterfaceActionInvoker0::Invoke(0 /* System.Void UniRx.IObserver`1<System.Double>::OnCompleted() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), (Il2CppObject*)L_0);
		IL2CPP_LEAVE(0x19, FINALLY_0012);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0012;
	}

FINALLY_0012:
	{ // begin finally (depth: 1)
		NullCheck((OperatorObserverBase_2_t1921935565 *)__this);
		VirtActionInvoker0::Invoke(4 /* System.Void UniRx.Operators.OperatorObserverBase`2<System.Double,System.Double>::Dispose() */, (OperatorObserverBase_2_t1921935565 *)__this);
		IL2CPP_END_FINALLY(18)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(18)
	{
		IL2CPP_JUMP_TBL(0x19, IL_0019)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0019:
	{
		return;
	}
}
// System.Void UniRx.Operators.EmptyObservable`1/Empty<System.Int32>::.ctor(UniRx.IObserver`1<T>,System.IDisposable)
extern "C"  void Empty__ctor_m2893778734_gshared (Empty_t511265325 * __this, Il2CppObject* ___observer0, Il2CppObject * ___cancel1, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = ___observer0;
		Il2CppObject * L_1 = ___cancel1;
		NullCheck((OperatorObserverBase_2_t701568569 *)__this);
		((  void (*) (OperatorObserverBase_2_t701568569 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((OperatorObserverBase_2_t701568569 *)__this, (Il2CppObject*)L_0, (Il2CppObject *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		return;
	}
}
// System.Void UniRx.Operators.EmptyObservable`1/Empty<System.Int32>::OnNext(T)
extern Il2CppClass* Il2CppObject_il2cpp_TypeInfo_var;
extern const uint32_t Empty_OnNext_m1168253860_MetadataUsageId;
extern "C"  void Empty_OnNext_m1168253860_gshared (Empty_t511265325 * __this, int32_t ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Empty_OnNext_m1168253860_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		Il2CppObject* L_0 = (Il2CppObject*)((OperatorObserverBase_2_t701568569 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		int32_t L_1 = ___value0;
		NullCheck((Il2CppObject*)L_0);
		InterfaceActionInvoker1< int32_t >::Invoke(2 /* System.Void UniRx.IObserver`1<System.Int32>::OnNext(T) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), (Il2CppObject*)L_0, (int32_t)L_1);
		goto IL_0021;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1967233988 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Il2CppObject_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_0013;
		throw e;
	}

CATCH_0013:
	{ // begin catch(System.Object)
		{
			NullCheck((OperatorObserverBase_2_t701568569 *)__this);
			VirtActionInvoker0::Invoke(4 /* System.Void UniRx.Operators.OperatorObserverBase`2<System.Int32,System.Int32>::Dispose() */, (OperatorObserverBase_2_t701568569 *)__this);
			IL2CPP_RAISE_MANAGED_EXCEPTION(__exception_local);
		}

IL_001c:
		{
			goto IL_0021;
		}
	} // end catch (depth: 1)

IL_0021:
	{
		return;
	}
}
// System.Void UniRx.Operators.EmptyObservable`1/Empty<System.Int32>::OnError(System.Exception)
extern "C"  void Empty_OnError_m2467460787_gshared (Empty_t511265325 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method)
{
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		Il2CppObject* L_0 = (Il2CppObject*)((OperatorObserverBase_2_t701568569 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		Exception_t1967233988 * L_1 = ___error0;
		NullCheck((Il2CppObject*)L_0);
		InterfaceActionInvoker1< Exception_t1967233988 * >::Invoke(1 /* System.Void UniRx.IObserver`1<System.Int32>::OnError(System.Exception) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), (Il2CppObject*)L_0, (Exception_t1967233988 *)L_1);
		IL2CPP_LEAVE(0x1A, FINALLY_0013);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0013;
	}

FINALLY_0013:
	{ // begin finally (depth: 1)
		NullCheck((OperatorObserverBase_2_t701568569 *)__this);
		VirtActionInvoker0::Invoke(4 /* System.Void UniRx.Operators.OperatorObserverBase`2<System.Int32,System.Int32>::Dispose() */, (OperatorObserverBase_2_t701568569 *)__this);
		IL2CPP_END_FINALLY(19)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(19)
	{
		IL2CPP_JUMP_TBL(0x1A, IL_001a)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_001a:
	{
		return;
	}
}
// System.Void UniRx.Operators.EmptyObservable`1/Empty<System.Int32>::OnCompleted()
extern "C"  void Empty_OnCompleted_m4025441926_gshared (Empty_t511265325 * __this, const MethodInfo* method)
{
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		Il2CppObject* L_0 = (Il2CppObject*)((OperatorObserverBase_2_t701568569 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		NullCheck((Il2CppObject*)L_0);
		InterfaceActionInvoker0::Invoke(0 /* System.Void UniRx.IObserver`1<System.Int32>::OnCompleted() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), (Il2CppObject*)L_0);
		IL2CPP_LEAVE(0x19, FINALLY_0012);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0012;
	}

FINALLY_0012:
	{ // begin finally (depth: 1)
		NullCheck((OperatorObserverBase_2_t701568569 *)__this);
		VirtActionInvoker0::Invoke(4 /* System.Void UniRx.Operators.OperatorObserverBase`2<System.Int32,System.Int32>::Dispose() */, (OperatorObserverBase_2_t701568569 *)__this);
		IL2CPP_END_FINALLY(18)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(18)
	{
		IL2CPP_JUMP_TBL(0x19, IL_0019)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0019:
	{
		return;
	}
}
// System.Void UniRx.Operators.EmptyObservable`1/Empty<System.Int64>::.ctor(UniRx.IObserver`1<T>,System.IDisposable)
extern "C"  void Empty__ctor_m1517302959_gshared (Empty_t511265420 * __this, Il2CppObject* ___observer0, Il2CppObject * ___cancel1, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = ___observer0;
		Il2CppObject * L_1 = ___cancel1;
		NullCheck((OperatorObserverBase_2_t3939730909 *)__this);
		((  void (*) (OperatorObserverBase_2_t3939730909 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((OperatorObserverBase_2_t3939730909 *)__this, (Il2CppObject*)L_0, (Il2CppObject *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		return;
	}
}
// System.Void UniRx.Operators.EmptyObservable`1/Empty<System.Int64>::OnNext(T)
extern Il2CppClass* Il2CppObject_il2cpp_TypeInfo_var;
extern const uint32_t Empty_OnNext_m3359739267_MetadataUsageId;
extern "C"  void Empty_OnNext_m3359739267_gshared (Empty_t511265420 * __this, int64_t ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Empty_OnNext_m3359739267_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		Il2CppObject* L_0 = (Il2CppObject*)((OperatorObserverBase_2_t3939730909 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		int64_t L_1 = ___value0;
		NullCheck((Il2CppObject*)L_0);
		InterfaceActionInvoker1< int64_t >::Invoke(2 /* System.Void UniRx.IObserver`1<System.Int64>::OnNext(T) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), (Il2CppObject*)L_0, (int64_t)L_1);
		goto IL_0021;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1967233988 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Il2CppObject_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_0013;
		throw e;
	}

CATCH_0013:
	{ // begin catch(System.Object)
		{
			NullCheck((OperatorObserverBase_2_t3939730909 *)__this);
			VirtActionInvoker0::Invoke(4 /* System.Void UniRx.Operators.OperatorObserverBase`2<System.Int64,System.Int64>::Dispose() */, (OperatorObserverBase_2_t3939730909 *)__this);
			IL2CPP_RAISE_MANAGED_EXCEPTION(__exception_local);
		}

IL_001c:
		{
			goto IL_0021;
		}
	} // end catch (depth: 1)

IL_0021:
	{
		return;
	}
}
// System.Void UniRx.Operators.EmptyObservable`1/Empty<System.Int64>::OnError(System.Exception)
extern "C"  void Empty_OnError_m3887612562_gshared (Empty_t511265420 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method)
{
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		Il2CppObject* L_0 = (Il2CppObject*)((OperatorObserverBase_2_t3939730909 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		Exception_t1967233988 * L_1 = ___error0;
		NullCheck((Il2CppObject*)L_0);
		InterfaceActionInvoker1< Exception_t1967233988 * >::Invoke(1 /* System.Void UniRx.IObserver`1<System.Int64>::OnError(System.Exception) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), (Il2CppObject*)L_0, (Exception_t1967233988 *)L_1);
		IL2CPP_LEAVE(0x1A, FINALLY_0013);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0013;
	}

FINALLY_0013:
	{ // begin finally (depth: 1)
		NullCheck((OperatorObserverBase_2_t3939730909 *)__this);
		VirtActionInvoker0::Invoke(4 /* System.Void UniRx.Operators.OperatorObserverBase`2<System.Int64,System.Int64>::Dispose() */, (OperatorObserverBase_2_t3939730909 *)__this);
		IL2CPP_END_FINALLY(19)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(19)
	{
		IL2CPP_JUMP_TBL(0x1A, IL_001a)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_001a:
	{
		return;
	}
}
// System.Void UniRx.Operators.EmptyObservable`1/Empty<System.Int64>::OnCompleted()
extern "C"  void Empty_OnCompleted_m3740844261_gshared (Empty_t511265420 * __this, const MethodInfo* method)
{
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		Il2CppObject* L_0 = (Il2CppObject*)((OperatorObserverBase_2_t3939730909 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		NullCheck((Il2CppObject*)L_0);
		InterfaceActionInvoker0::Invoke(0 /* System.Void UniRx.IObserver`1<System.Int64>::OnCompleted() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), (Il2CppObject*)L_0);
		IL2CPP_LEAVE(0x19, FINALLY_0012);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0012;
	}

FINALLY_0012:
	{ // begin finally (depth: 1)
		NullCheck((OperatorObserverBase_2_t3939730909 *)__this);
		VirtActionInvoker0::Invoke(4 /* System.Void UniRx.Operators.OperatorObserverBase`2<System.Int64,System.Int64>::Dispose() */, (OperatorObserverBase_2_t3939730909 *)__this);
		IL2CPP_END_FINALLY(18)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(18)
	{
		IL2CPP_JUMP_TBL(0x19, IL_0019)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0019:
	{
		return;
	}
}
// System.Void UniRx.Operators.EmptyObservable`1/Empty<System.Object>::.ctor(UniRx.IObserver`1<T>,System.IDisposable)
extern "C"  void Empty__ctor_m4181350287_gshared (Empty_t2795924254 * __this, Il2CppObject* ___observer0, Il2CppObject * ___cancel1, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = ___observer0;
		Il2CppObject * L_1 = ___cancel1;
		NullCheck((OperatorObserverBase_2_t1187768149 *)__this);
		((  void (*) (OperatorObserverBase_2_t1187768149 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((OperatorObserverBase_2_t1187768149 *)__this, (Il2CppObject*)L_0, (Il2CppObject *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		return;
	}
}
// System.Void UniRx.Operators.EmptyObservable`1/Empty<System.Object>::OnNext(T)
extern Il2CppClass* Il2CppObject_il2cpp_TypeInfo_var;
extern const uint32_t Empty_OnNext_m3325140643_MetadataUsageId;
extern "C"  void Empty_OnNext_m3325140643_gshared (Empty_t2795924254 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Empty_OnNext_m3325140643_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		Il2CppObject* L_0 = (Il2CppObject*)((OperatorObserverBase_2_t1187768149 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		Il2CppObject * L_1 = ___value0;
		NullCheck((Il2CppObject*)L_0);
		InterfaceActionInvoker1< Il2CppObject * >::Invoke(2 /* System.Void UniRx.IObserver`1<System.Object>::OnNext(T) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), (Il2CppObject*)L_0, (Il2CppObject *)L_1);
		goto IL_0021;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1967233988 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Il2CppObject_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_0013;
		throw e;
	}

CATCH_0013:
	{ // begin catch(System.Object)
		{
			NullCheck((OperatorObserverBase_2_t1187768149 *)__this);
			VirtActionInvoker0::Invoke(4 /* System.Void UniRx.Operators.OperatorObserverBase`2<System.Object,System.Object>::Dispose() */, (OperatorObserverBase_2_t1187768149 *)__this);
			IL2CPP_RAISE_MANAGED_EXCEPTION(__exception_local);
		}

IL_001c:
		{
			goto IL_0021;
		}
	} // end catch (depth: 1)

IL_0021:
	{
		return;
	}
}
// System.Void UniRx.Operators.EmptyObservable`1/Empty<System.Object>::OnError(System.Exception)
extern "C"  void Empty_OnError_m1654789042_gshared (Empty_t2795924254 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method)
{
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		Il2CppObject* L_0 = (Il2CppObject*)((OperatorObserverBase_2_t1187768149 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		Exception_t1967233988 * L_1 = ___error0;
		NullCheck((Il2CppObject*)L_0);
		InterfaceActionInvoker1< Exception_t1967233988 * >::Invoke(1 /* System.Void UniRx.IObserver`1<System.Object>::OnError(System.Exception) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), (Il2CppObject*)L_0, (Exception_t1967233988 *)L_1);
		IL2CPP_LEAVE(0x1A, FINALLY_0013);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0013;
	}

FINALLY_0013:
	{ // begin finally (depth: 1)
		NullCheck((OperatorObserverBase_2_t1187768149 *)__this);
		VirtActionInvoker0::Invoke(4 /* System.Void UniRx.Operators.OperatorObserverBase`2<System.Object,System.Object>::Dispose() */, (OperatorObserverBase_2_t1187768149 *)__this);
		IL2CPP_END_FINALLY(19)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(19)
	{
		IL2CPP_JUMP_TBL(0x1A, IL_001a)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_001a:
	{
		return;
	}
}
// System.Void UniRx.Operators.EmptyObservable`1/Empty<System.Object>::OnCompleted()
extern "C"  void Empty_OnCompleted_m1446724101_gshared (Empty_t2795924254 * __this, const MethodInfo* method)
{
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		Il2CppObject* L_0 = (Il2CppObject*)((OperatorObserverBase_2_t1187768149 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		NullCheck((Il2CppObject*)L_0);
		InterfaceActionInvoker0::Invoke(0 /* System.Void UniRx.IObserver`1<System.Object>::OnCompleted() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), (Il2CppObject*)L_0);
		IL2CPP_LEAVE(0x19, FINALLY_0012);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0012;
	}

FINALLY_0012:
	{ // begin finally (depth: 1)
		NullCheck((OperatorObserverBase_2_t1187768149 *)__this);
		VirtActionInvoker0::Invoke(4 /* System.Void UniRx.Operators.OperatorObserverBase`2<System.Object,System.Object>::Dispose() */, (OperatorObserverBase_2_t1187768149 *)__this);
		IL2CPP_END_FINALLY(18)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(18)
	{
		IL2CPP_JUMP_TBL(0x19, IL_0019)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0019:
	{
		return;
	}
}
// System.Void UniRx.Operators.EmptyObservable`1/Empty<UniRx.CollectionAddEvent`1<System.Object>>::.ctor(UniRx.IObserver`1<T>,System.IDisposable)
extern "C"  void Empty__ctor_m213159153_gshared (Empty_t80372525 * __this, Il2CppObject* ___observer0, Il2CppObject * ___cancel1, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = ___observer0;
		Il2CppObject * L_1 = ___cancel1;
		NullCheck((OperatorObserverBase_2_t3545535033 *)__this);
		((  void (*) (OperatorObserverBase_2_t3545535033 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((OperatorObserverBase_2_t3545535033 *)__this, (Il2CppObject*)L_0, (Il2CppObject *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		return;
	}
}
// System.Void UniRx.Operators.EmptyObservable`1/Empty<UniRx.CollectionAddEvent`1<System.Object>>::OnNext(T)
extern Il2CppClass* Il2CppObject_il2cpp_TypeInfo_var;
extern const uint32_t Empty_OnNext_m2459765761_MetadataUsageId;
extern "C"  void Empty_OnNext_m2459765761_gshared (Empty_t80372525 * __this, CollectionAddEvent_1_t2416521987  ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Empty_OnNext_m2459765761_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		Il2CppObject* L_0 = (Il2CppObject*)((OperatorObserverBase_2_t3545535033 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		CollectionAddEvent_1_t2416521987  L_1 = ___value0;
		NullCheck((Il2CppObject*)L_0);
		InterfaceActionInvoker1< CollectionAddEvent_1_t2416521987  >::Invoke(2 /* System.Void UniRx.IObserver`1<UniRx.CollectionAddEvent`1<System.Object>>::OnNext(T) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), (Il2CppObject*)L_0, (CollectionAddEvent_1_t2416521987 )L_1);
		goto IL_0021;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1967233988 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Il2CppObject_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_0013;
		throw e;
	}

CATCH_0013:
	{ // begin catch(System.Object)
		{
			NullCheck((OperatorObserverBase_2_t3545535033 *)__this);
			VirtActionInvoker0::Invoke(4 /* System.Void UniRx.Operators.OperatorObserverBase`2<UniRx.CollectionAddEvent`1<System.Object>,UniRx.CollectionAddEvent`1<System.Object>>::Dispose() */, (OperatorObserverBase_2_t3545535033 *)__this);
			IL2CPP_RAISE_MANAGED_EXCEPTION(__exception_local);
		}

IL_001c:
		{
			goto IL_0021;
		}
	} // end catch (depth: 1)

IL_0021:
	{
		return;
	}
}
// System.Void UniRx.Operators.EmptyObservable`1/Empty<UniRx.CollectionAddEvent`1<System.Object>>::OnError(System.Exception)
extern "C"  void Empty_OnError_m3941795088_gshared (Empty_t80372525 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method)
{
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		Il2CppObject* L_0 = (Il2CppObject*)((OperatorObserverBase_2_t3545535033 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		Exception_t1967233988 * L_1 = ___error0;
		NullCheck((Il2CppObject*)L_0);
		InterfaceActionInvoker1< Exception_t1967233988 * >::Invoke(1 /* System.Void UniRx.IObserver`1<UniRx.CollectionAddEvent`1<System.Object>>::OnError(System.Exception) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), (Il2CppObject*)L_0, (Exception_t1967233988 *)L_1);
		IL2CPP_LEAVE(0x1A, FINALLY_0013);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0013;
	}

FINALLY_0013:
	{ // begin finally (depth: 1)
		NullCheck((OperatorObserverBase_2_t3545535033 *)__this);
		VirtActionInvoker0::Invoke(4 /* System.Void UniRx.Operators.OperatorObserverBase`2<UniRx.CollectionAddEvent`1<System.Object>,UniRx.CollectionAddEvent`1<System.Object>>::Dispose() */, (OperatorObserverBase_2_t3545535033 *)__this);
		IL2CPP_END_FINALLY(19)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(19)
	{
		IL2CPP_JUMP_TBL(0x1A, IL_001a)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_001a:
	{
		return;
	}
}
// System.Void UniRx.Operators.EmptyObservable`1/Empty<UniRx.CollectionAddEvent`1<System.Object>>::OnCompleted()
extern "C"  void Empty_OnCompleted_m4199862371_gshared (Empty_t80372525 * __this, const MethodInfo* method)
{
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		Il2CppObject* L_0 = (Il2CppObject*)((OperatorObserverBase_2_t3545535033 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		NullCheck((Il2CppObject*)L_0);
		InterfaceActionInvoker0::Invoke(0 /* System.Void UniRx.IObserver`1<UniRx.CollectionAddEvent`1<System.Object>>::OnCompleted() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), (Il2CppObject*)L_0);
		IL2CPP_LEAVE(0x19, FINALLY_0012);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0012;
	}

FINALLY_0012:
	{ // begin finally (depth: 1)
		NullCheck((OperatorObserverBase_2_t3545535033 *)__this);
		VirtActionInvoker0::Invoke(4 /* System.Void UniRx.Operators.OperatorObserverBase`2<UniRx.CollectionAddEvent`1<System.Object>,UniRx.CollectionAddEvent`1<System.Object>>::Dispose() */, (OperatorObserverBase_2_t3545535033 *)__this);
		IL2CPP_END_FINALLY(18)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(18)
	{
		IL2CPP_JUMP_TBL(0x19, IL_0019)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0019:
	{
		return;
	}
}
// System.Void UniRx.Operators.EmptyObservable`1/Empty<UniRx.CollectionAddEvent`1<UniRx.Tuple`2<System.Object,System.Object>>>::.ctor(UniRx.IObserver`1<T>,System.IDisposable)
extern "C"  void Empty__ctor_m2885220451_gshared (Empty_t3907495220 * __this, Il2CppObject* ___observer0, Il2CppObject * ___cancel1, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = ___observer0;
		Il2CppObject * L_1 = ___cancel1;
		NullCheck((OperatorObserverBase_2_t2669133373 *)__this);
		((  void (*) (OperatorObserverBase_2_t2669133373 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((OperatorObserverBase_2_t2669133373 *)__this, (Il2CppObject*)L_0, (Il2CppObject *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		return;
	}
}
// System.Void UniRx.Operators.EmptyObservable`1/Empty<UniRx.CollectionAddEvent`1<UniRx.Tuple`2<System.Object,System.Object>>>::OnNext(T)
extern Il2CppClass* Il2CppObject_il2cpp_TypeInfo_var;
extern const uint32_t Empty_OnNext_m630487375_MetadataUsageId;
extern "C"  void Empty_OnNext_m630487375_gshared (Empty_t3907495220 * __this, CollectionAddEvent_1_t1948677386  ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Empty_OnNext_m630487375_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		Il2CppObject* L_0 = (Il2CppObject*)((OperatorObserverBase_2_t2669133373 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		CollectionAddEvent_1_t1948677386  L_1 = ___value0;
		NullCheck((Il2CppObject*)L_0);
		InterfaceActionInvoker1< CollectionAddEvent_1_t1948677386  >::Invoke(2 /* System.Void UniRx.IObserver`1<UniRx.CollectionAddEvent`1<UniRx.Tuple`2<System.Object,System.Object>>>::OnNext(T) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), (Il2CppObject*)L_0, (CollectionAddEvent_1_t1948677386 )L_1);
		goto IL_0021;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1967233988 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Il2CppObject_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_0013;
		throw e;
	}

CATCH_0013:
	{ // begin catch(System.Object)
		{
			NullCheck((OperatorObserverBase_2_t2669133373 *)__this);
			VirtActionInvoker0::Invoke(4 /* System.Void UniRx.Operators.OperatorObserverBase`2<UniRx.CollectionAddEvent`1<UniRx.Tuple`2<System.Object,System.Object>>,UniRx.CollectionAddEvent`1<UniRx.Tuple`2<System.Object,System.Object>>>::Dispose() */, (OperatorObserverBase_2_t2669133373 *)__this);
			IL2CPP_RAISE_MANAGED_EXCEPTION(__exception_local);
		}

IL_001c:
		{
			goto IL_0021;
		}
	} // end catch (depth: 1)

IL_0021:
	{
		return;
	}
}
// System.Void UniRx.Operators.EmptyObservable`1/Empty<UniRx.CollectionAddEvent`1<UniRx.Tuple`2<System.Object,System.Object>>>::OnError(System.Exception)
extern "C"  void Empty_OnError_m2536642142_gshared (Empty_t3907495220 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method)
{
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		Il2CppObject* L_0 = (Il2CppObject*)((OperatorObserverBase_2_t2669133373 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		Exception_t1967233988 * L_1 = ___error0;
		NullCheck((Il2CppObject*)L_0);
		InterfaceActionInvoker1< Exception_t1967233988 * >::Invoke(1 /* System.Void UniRx.IObserver`1<UniRx.CollectionAddEvent`1<UniRx.Tuple`2<System.Object,System.Object>>>::OnError(System.Exception) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), (Il2CppObject*)L_0, (Exception_t1967233988 *)L_1);
		IL2CPP_LEAVE(0x1A, FINALLY_0013);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0013;
	}

FINALLY_0013:
	{ // begin finally (depth: 1)
		NullCheck((OperatorObserverBase_2_t2669133373 *)__this);
		VirtActionInvoker0::Invoke(4 /* System.Void UniRx.Operators.OperatorObserverBase`2<UniRx.CollectionAddEvent`1<UniRx.Tuple`2<System.Object,System.Object>>,UniRx.CollectionAddEvent`1<UniRx.Tuple`2<System.Object,System.Object>>>::Dispose() */, (OperatorObserverBase_2_t2669133373 *)__this);
		IL2CPP_END_FINALLY(19)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(19)
	{
		IL2CPP_JUMP_TBL(0x1A, IL_001a)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_001a:
	{
		return;
	}
}
// System.Void UniRx.Operators.EmptyObservable`1/Empty<UniRx.CollectionAddEvent`1<UniRx.Tuple`2<System.Object,System.Object>>>::OnCompleted()
extern "C"  void Empty_OnCompleted_m1041819313_gshared (Empty_t3907495220 * __this, const MethodInfo* method)
{
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		Il2CppObject* L_0 = (Il2CppObject*)((OperatorObserverBase_2_t2669133373 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		NullCheck((Il2CppObject*)L_0);
		InterfaceActionInvoker0::Invoke(0 /* System.Void UniRx.IObserver`1<UniRx.CollectionAddEvent`1<UniRx.Tuple`2<System.Object,System.Object>>>::OnCompleted() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), (Il2CppObject*)L_0);
		IL2CPP_LEAVE(0x19, FINALLY_0012);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0012;
	}

FINALLY_0012:
	{ // begin finally (depth: 1)
		NullCheck((OperatorObserverBase_2_t2669133373 *)__this);
		VirtActionInvoker0::Invoke(4 /* System.Void UniRx.Operators.OperatorObserverBase`2<UniRx.CollectionAddEvent`1<UniRx.Tuple`2<System.Object,System.Object>>,UniRx.CollectionAddEvent`1<UniRx.Tuple`2<System.Object,System.Object>>>::Dispose() */, (OperatorObserverBase_2_t2669133373 *)__this);
		IL2CPP_END_FINALLY(18)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(18)
	{
		IL2CPP_JUMP_TBL(0x19, IL_0019)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0019:
	{
		return;
	}
}
// System.Void UniRx.Operators.EmptyObservable`1/Empty<UniRx.CollectionMoveEvent`1<System.Object>>::.ctor(UniRx.IObserver`1<T>,System.IDisposable)
extern "C"  void Empty__ctor_m805200901_gshared (Empty_t580284385 * __this, Il2CppObject* ___observer0, Il2CppObject * ___cancel1, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = ___observer0;
		Il2CppObject * L_1 = ___cancel1;
		NullCheck((OperatorObserverBase_2_t2793497833 *)__this);
		((  void (*) (OperatorObserverBase_2_t2793497833 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((OperatorObserverBase_2_t2793497833 *)__this, (Il2CppObject*)L_0, (Il2CppObject *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		return;
	}
}
// System.Void UniRx.Operators.EmptyObservable`1/Empty<UniRx.CollectionMoveEvent`1<System.Object>>::OnNext(T)
extern Il2CppClass* Il2CppObject_il2cpp_TypeInfo_var;
extern const uint32_t Empty_OnNext_m3327314029_MetadataUsageId;
extern "C"  void Empty_OnNext_m3327314029_gshared (Empty_t580284385 * __this, CollectionMoveEvent_1_t2916433847  ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Empty_OnNext_m3327314029_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		Il2CppObject* L_0 = (Il2CppObject*)((OperatorObserverBase_2_t2793497833 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		CollectionMoveEvent_1_t2916433847  L_1 = ___value0;
		NullCheck((Il2CppObject*)L_0);
		InterfaceActionInvoker1< CollectionMoveEvent_1_t2916433847  >::Invoke(2 /* System.Void UniRx.IObserver`1<UniRx.CollectionMoveEvent`1<System.Object>>::OnNext(T) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), (Il2CppObject*)L_0, (CollectionMoveEvent_1_t2916433847 )L_1);
		goto IL_0021;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1967233988 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Il2CppObject_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_0013;
		throw e;
	}

CATCH_0013:
	{ // begin catch(System.Object)
		{
			NullCheck((OperatorObserverBase_2_t2793497833 *)__this);
			VirtActionInvoker0::Invoke(4 /* System.Void UniRx.Operators.OperatorObserverBase`2<UniRx.CollectionMoveEvent`1<System.Object>,UniRx.CollectionMoveEvent`1<System.Object>>::Dispose() */, (OperatorObserverBase_2_t2793497833 *)__this);
			IL2CPP_RAISE_MANAGED_EXCEPTION(__exception_local);
		}

IL_001c:
		{
			goto IL_0021;
		}
	} // end catch (depth: 1)

IL_0021:
	{
		return;
	}
}
// System.Void UniRx.Operators.EmptyObservable`1/Empty<UniRx.CollectionMoveEvent`1<System.Object>>::OnError(System.Exception)
extern "C"  void Empty_OnError_m3680135548_gshared (Empty_t580284385 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method)
{
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		Il2CppObject* L_0 = (Il2CppObject*)((OperatorObserverBase_2_t2793497833 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		Exception_t1967233988 * L_1 = ___error0;
		NullCheck((Il2CppObject*)L_0);
		InterfaceActionInvoker1< Exception_t1967233988 * >::Invoke(1 /* System.Void UniRx.IObserver`1<UniRx.CollectionMoveEvent`1<System.Object>>::OnError(System.Exception) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), (Il2CppObject*)L_0, (Exception_t1967233988 *)L_1);
		IL2CPP_LEAVE(0x1A, FINALLY_0013);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0013;
	}

FINALLY_0013:
	{ // begin finally (depth: 1)
		NullCheck((OperatorObserverBase_2_t2793497833 *)__this);
		VirtActionInvoker0::Invoke(4 /* System.Void UniRx.Operators.OperatorObserverBase`2<UniRx.CollectionMoveEvent`1<System.Object>,UniRx.CollectionMoveEvent`1<System.Object>>::Dispose() */, (OperatorObserverBase_2_t2793497833 *)__this);
		IL2CPP_END_FINALLY(19)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(19)
	{
		IL2CPP_JUMP_TBL(0x1A, IL_001a)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_001a:
	{
		return;
	}
}
// System.Void UniRx.Operators.EmptyObservable`1/Empty<UniRx.CollectionMoveEvent`1<System.Object>>::OnCompleted()
extern "C"  void Empty_OnCompleted_m2864608975_gshared (Empty_t580284385 * __this, const MethodInfo* method)
{
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		Il2CppObject* L_0 = (Il2CppObject*)((OperatorObserverBase_2_t2793497833 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		NullCheck((Il2CppObject*)L_0);
		InterfaceActionInvoker0::Invoke(0 /* System.Void UniRx.IObserver`1<UniRx.CollectionMoveEvent`1<System.Object>>::OnCompleted() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), (Il2CppObject*)L_0);
		IL2CPP_LEAVE(0x19, FINALLY_0012);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0012;
	}

FINALLY_0012:
	{ // begin finally (depth: 1)
		NullCheck((OperatorObserverBase_2_t2793497833 *)__this);
		VirtActionInvoker0::Invoke(4 /* System.Void UniRx.Operators.OperatorObserverBase`2<UniRx.CollectionMoveEvent`1<System.Object>,UniRx.CollectionMoveEvent`1<System.Object>>::Dispose() */, (OperatorObserverBase_2_t2793497833 *)__this);
		IL2CPP_END_FINALLY(18)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(18)
	{
		IL2CPP_JUMP_TBL(0x19, IL_0019)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0019:
	{
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
