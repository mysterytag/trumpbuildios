﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.String
struct String_t;
// System.Object[]
struct ObjectU5BU5D_t11523773;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String968488902.h"

// System.Void PlayFab.Internal.Util::.cctor()
extern "C"  void Util__cctor_m2662243994 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PlayFab.Internal.Util::get_timeStamp()
extern "C"  String_t* Util_get_timeStamp_m2139631777 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PlayFab.Internal.Util::get_utcTimeStamp()
extern "C"  String_t* Util_get_utcTimeStamp_m899435465 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PlayFab.Internal.Util::Format(System.String,System.Object[])
extern "C"  String_t* Util_Format_m1726450793 (Il2CppObject * __this /* static, unused */, String_t* ___text0, ObjectU5BU5D_t11523773* ___args1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
