﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UniRx.MultipleAssignmentDisposable
struct MultipleAssignmentDisposable_t3090232719;
// UniRx.Operators.DelaySubscriptionObservable`1/<SubscribeCore>c__AnonStorey5E<System.Object>
struct U3CSubscribeCoreU3Ec__AnonStorey5E_t2206845999;
// UniRx.Operators.DelaySubscriptionObservable`1<System.Object>
struct DelaySubscriptionObservable_1_t1759674162;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Operators.DelaySubscriptionObservable`1/<SubscribeCore>c__AnonStorey60<System.Object>
struct  U3CSubscribeCoreU3Ec__AnonStorey60_t1692718505  : public Il2CppObject
{
public:
	// UniRx.MultipleAssignmentDisposable UniRx.Operators.DelaySubscriptionObservable`1/<SubscribeCore>c__AnonStorey60::d
	MultipleAssignmentDisposable_t3090232719 * ___d_0;
	// UniRx.Operators.DelaySubscriptionObservable`1/<SubscribeCore>c__AnonStorey5E<T> UniRx.Operators.DelaySubscriptionObservable`1/<SubscribeCore>c__AnonStorey60::<>f__ref$94
	U3CSubscribeCoreU3Ec__AnonStorey5E_t2206845999 * ___U3CU3Ef__refU2494_1;
	// UniRx.Operators.DelaySubscriptionObservable`1<T> UniRx.Operators.DelaySubscriptionObservable`1/<SubscribeCore>c__AnonStorey60::<>f__this
	DelaySubscriptionObservable_1_t1759674162 * ___U3CU3Ef__this_2;

public:
	inline static int32_t get_offset_of_d_0() { return static_cast<int32_t>(offsetof(U3CSubscribeCoreU3Ec__AnonStorey60_t1692718505, ___d_0)); }
	inline MultipleAssignmentDisposable_t3090232719 * get_d_0() const { return ___d_0; }
	inline MultipleAssignmentDisposable_t3090232719 ** get_address_of_d_0() { return &___d_0; }
	inline void set_d_0(MultipleAssignmentDisposable_t3090232719 * value)
	{
		___d_0 = value;
		Il2CppCodeGenWriteBarrier(&___d_0, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__refU2494_1() { return static_cast<int32_t>(offsetof(U3CSubscribeCoreU3Ec__AnonStorey60_t1692718505, ___U3CU3Ef__refU2494_1)); }
	inline U3CSubscribeCoreU3Ec__AnonStorey5E_t2206845999 * get_U3CU3Ef__refU2494_1() const { return ___U3CU3Ef__refU2494_1; }
	inline U3CSubscribeCoreU3Ec__AnonStorey5E_t2206845999 ** get_address_of_U3CU3Ef__refU2494_1() { return &___U3CU3Ef__refU2494_1; }
	inline void set_U3CU3Ef__refU2494_1(U3CSubscribeCoreU3Ec__AnonStorey5E_t2206845999 * value)
	{
		___U3CU3Ef__refU2494_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__refU2494_1, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__this_2() { return static_cast<int32_t>(offsetof(U3CSubscribeCoreU3Ec__AnonStorey60_t1692718505, ___U3CU3Ef__this_2)); }
	inline DelaySubscriptionObservable_1_t1759674162 * get_U3CU3Ef__this_2() const { return ___U3CU3Ef__this_2; }
	inline DelaySubscriptionObservable_1_t1759674162 ** get_address_of_U3CU3Ef__this_2() { return &___U3CU3Ef__this_2; }
	inline void set_U3CU3Ef__this_2(DelaySubscriptionObservable_1_t1759674162 * value)
	{
		___U3CU3Ef__this_2 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__this_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
