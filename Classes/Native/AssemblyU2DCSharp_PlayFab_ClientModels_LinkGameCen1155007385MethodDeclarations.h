﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.ClientModels.LinkGameCenterAccountResult
struct LinkGameCenterAccountResult_t1155007385;

#include "codegen/il2cpp-codegen.h"

// System.Void PlayFab.ClientModels.LinkGameCenterAccountResult::.ctor()
extern "C"  void LinkGameCenterAccountResult__ctor_m1289404208 (LinkGameCenterAccountResult_t1155007385 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
