﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.InternalUtil.ListObserver`1<UniRx.CollectionReplaceEvent`1<UniRx.Tuple`2<System.Object,System.Object>>>
struct ListObserver_1_t1668523606;
// UniRx.InternalUtil.ImmutableList`1<UniRx.IObserver`1<UniRx.CollectionReplaceEvent`1<UniRx.Tuple`2<System.Object,System.Object>>>>
struct ImmutableList_1_t351142041;
// System.Exception
struct Exception_t1967233988;
// UniRx.IObserver`1<UniRx.CollectionReplaceEvent`1<UniRx.Tuple`2<System.Object,System.Object>>>
struct IObserver_1_t3585904838;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Exception1967233988.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_CollectionRepl1373905935.h"

// System.Void UniRx.InternalUtil.ListObserver`1<UniRx.CollectionReplaceEvent`1<UniRx.Tuple`2<System.Object,System.Object>>>::.ctor(UniRx.InternalUtil.ImmutableList`1<UniRx.IObserver`1<T>>)
extern "C"  void ListObserver_1__ctor_m2014486450_gshared (ListObserver_1_t1668523606 * __this, ImmutableList_1_t351142041 * ___observers0, const MethodInfo* method);
#define ListObserver_1__ctor_m2014486450(__this, ___observers0, method) ((  void (*) (ListObserver_1_t1668523606 *, ImmutableList_1_t351142041 *, const MethodInfo*))ListObserver_1__ctor_m2014486450_gshared)(__this, ___observers0, method)
// System.Void UniRx.InternalUtil.ListObserver`1<UniRx.CollectionReplaceEvent`1<UniRx.Tuple`2<System.Object,System.Object>>>::OnCompleted()
extern "C"  void ListObserver_1_OnCompleted_m798259818_gshared (ListObserver_1_t1668523606 * __this, const MethodInfo* method);
#define ListObserver_1_OnCompleted_m798259818(__this, method) ((  void (*) (ListObserver_1_t1668523606 *, const MethodInfo*))ListObserver_1_OnCompleted_m798259818_gshared)(__this, method)
// System.Void UniRx.InternalUtil.ListObserver`1<UniRx.CollectionReplaceEvent`1<UniRx.Tuple`2<System.Object,System.Object>>>::OnError(System.Exception)
extern "C"  void ListObserver_1_OnError_m3248020631_gshared (ListObserver_1_t1668523606 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method);
#define ListObserver_1_OnError_m3248020631(__this, ___error0, method) ((  void (*) (ListObserver_1_t1668523606 *, Exception_t1967233988 *, const MethodInfo*))ListObserver_1_OnError_m3248020631_gshared)(__this, ___error0, method)
// System.Void UniRx.InternalUtil.ListObserver`1<UniRx.CollectionReplaceEvent`1<UniRx.Tuple`2<System.Object,System.Object>>>::OnNext(T)
extern "C"  void ListObserver_1_OnNext_m4258845064_gshared (ListObserver_1_t1668523606 * __this, CollectionReplaceEvent_1_t1373905935  ___value0, const MethodInfo* method);
#define ListObserver_1_OnNext_m4258845064(__this, ___value0, method) ((  void (*) (ListObserver_1_t1668523606 *, CollectionReplaceEvent_1_t1373905935 , const MethodInfo*))ListObserver_1_OnNext_m4258845064_gshared)(__this, ___value0, method)
// UniRx.IObserver`1<T> UniRx.InternalUtil.ListObserver`1<UniRx.CollectionReplaceEvent`1<UniRx.Tuple`2<System.Object,System.Object>>>::Add(UniRx.IObserver`1<T>)
extern "C"  Il2CppObject* ListObserver_1_Add_m1913525288_gshared (ListObserver_1_t1668523606 * __this, Il2CppObject* ___observer0, const MethodInfo* method);
#define ListObserver_1_Add_m1913525288(__this, ___observer0, method) ((  Il2CppObject* (*) (ListObserver_1_t1668523606 *, Il2CppObject*, const MethodInfo*))ListObserver_1_Add_m1913525288_gshared)(__this, ___observer0, method)
// UniRx.IObserver`1<T> UniRx.InternalUtil.ListObserver`1<UniRx.CollectionReplaceEvent`1<UniRx.Tuple`2<System.Object,System.Object>>>::Remove(UniRx.IObserver`1<T>)
extern "C"  Il2CppObject* ListObserver_1_Remove_m3459348003_gshared (ListObserver_1_t1668523606 * __this, Il2CppObject* ___observer0, const MethodInfo* method);
#define ListObserver_1_Remove_m3459348003(__this, ___observer0, method) ((  Il2CppObject* (*) (ListObserver_1_t1668523606 *, Il2CppObject*, const MethodInfo*))ListObserver_1_Remove_m3459348003_gshared)(__this, ___observer0, method)
