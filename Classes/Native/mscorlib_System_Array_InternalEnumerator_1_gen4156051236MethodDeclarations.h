﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Array
struct Il2CppArray;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen4156051236.h"
#include "mscorlib_System_Array2840145358.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_P4248560733.h"

// System.Void System.Array/InternalEnumerator`1<UniRx.InternalUtil.PriorityQueue`1/IndexedItem<System.Object>>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m1613344613_gshared (InternalEnumerator_1_t4156051236 * __this, Il2CppArray * ___array0, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m1613344613(__this, ___array0, method) ((  void (*) (InternalEnumerator_1_t4156051236 *, Il2CppArray *, const MethodInfo*))InternalEnumerator_1__ctor_m1613344613_gshared)(__this, ___array0, method)
// System.Void System.Array/InternalEnumerator`1<UniRx.InternalUtil.PriorityQueue`1/IndexedItem<System.Object>>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m636074523_gshared (InternalEnumerator_1_t4156051236 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m636074523(__this, method) ((  void (*) (InternalEnumerator_1_t4156051236 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m636074523_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<UniRx.InternalUtil.PriorityQueue`1/IndexedItem<System.Object>>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m4272588689_gshared (InternalEnumerator_1_t4156051236 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m4272588689(__this, method) ((  Il2CppObject * (*) (InternalEnumerator_1_t4156051236 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m4272588689_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<UniRx.InternalUtil.PriorityQueue`1/IndexedItem<System.Object>>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m196297596_gshared (InternalEnumerator_1_t4156051236 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m196297596(__this, method) ((  void (*) (InternalEnumerator_1_t4156051236 *, const MethodInfo*))InternalEnumerator_1_Dispose_m196297596_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<UniRx.InternalUtil.PriorityQueue`1/IndexedItem<System.Object>>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m864721355_gshared (InternalEnumerator_1_t4156051236 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m864721355(__this, method) ((  bool (*) (InternalEnumerator_1_t4156051236 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m864721355_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<UniRx.InternalUtil.PriorityQueue`1/IndexedItem<System.Object>>::get_Current()
extern "C"  IndexedItem_t4248560733  InternalEnumerator_1_get_Current_m448163854_gshared (InternalEnumerator_1_t4156051236 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m448163854(__this, method) ((  IndexedItem_t4248560733  (*) (InternalEnumerator_1_t4156051236 *, const MethodInfo*))InternalEnumerator_1_get_Current_m448163854_gshared)(__this, method)
