﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Observable/<FromCoroutineValue>c__AnonStorey4B`1<System.Object>
struct U3CFromCoroutineValueU3Ec__AnonStorey4B_1_t3800936616;
// System.Collections.IEnumerator
struct IEnumerator_t287207039;
// UniRx.IObserver`1<System.Object>
struct IObserver_1_t3049105323;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_CancellationTo1439151560.h"

// System.Void UniRx.Observable/<FromCoroutineValue>c__AnonStorey4B`1<System.Object>::.ctor()
extern "C"  void U3CFromCoroutineValueU3Ec__AnonStorey4B_1__ctor_m3265911342_gshared (U3CFromCoroutineValueU3Ec__AnonStorey4B_1_t3800936616 * __this, const MethodInfo* method);
#define U3CFromCoroutineValueU3Ec__AnonStorey4B_1__ctor_m3265911342(__this, method) ((  void (*) (U3CFromCoroutineValueU3Ec__AnonStorey4B_1_t3800936616 *, const MethodInfo*))U3CFromCoroutineValueU3Ec__AnonStorey4B_1__ctor_m3265911342_gshared)(__this, method)
// System.Collections.IEnumerator UniRx.Observable/<FromCoroutineValue>c__AnonStorey4B`1<System.Object>::<>m__49(UniRx.IObserver`1<T>,UniRx.CancellationToken)
extern "C"  Il2CppObject * U3CFromCoroutineValueU3Ec__AnonStorey4B_1_U3CU3Em__49_m3253753900_gshared (U3CFromCoroutineValueU3Ec__AnonStorey4B_1_t3800936616 * __this, Il2CppObject* ___observer0, CancellationToken_t1439151560  ___cancellationToken1, const MethodInfo* method);
#define U3CFromCoroutineValueU3Ec__AnonStorey4B_1_U3CU3Em__49_m3253753900(__this, ___observer0, ___cancellationToken1, method) ((  Il2CppObject * (*) (U3CFromCoroutineValueU3Ec__AnonStorey4B_1_t3800936616 *, Il2CppObject*, CancellationToken_t1439151560 , const MethodInfo*))U3CFromCoroutineValueU3Ec__AnonStorey4B_1_U3CU3Em__49_m3253753900_gshared)(__this, ___observer0, ___cancellationToken1, method)
