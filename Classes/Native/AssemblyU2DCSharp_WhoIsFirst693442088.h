﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Action`1<System.Int32>
struct Action_1_t2995867492;
// System.Collections.Generic.IEnumerable`1<IProcess>
struct IEnumerable_1_t2603424474;

#include "AssemblyU2DCSharp_MetaProcessBase2426506875.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WhoIsFirst
struct  WhoIsFirst_t693442088  : public MetaProcessBase_t2426506875
{
public:
	// System.Action`1<System.Int32> WhoIsFirst::callResult
	Action_1_t2995867492 * ___callResult_3;
	// System.Collections.Generic.IEnumerable`1<IProcess> WhoIsFirst::processes
	Il2CppObject* ___processes_4;

public:
	inline static int32_t get_offset_of_callResult_3() { return static_cast<int32_t>(offsetof(WhoIsFirst_t693442088, ___callResult_3)); }
	inline Action_1_t2995867492 * get_callResult_3() const { return ___callResult_3; }
	inline Action_1_t2995867492 ** get_address_of_callResult_3() { return &___callResult_3; }
	inline void set_callResult_3(Action_1_t2995867492 * value)
	{
		___callResult_3 = value;
		Il2CppCodeGenWriteBarrier(&___callResult_3, value);
	}

	inline static int32_t get_offset_of_processes_4() { return static_cast<int32_t>(offsetof(WhoIsFirst_t693442088, ___processes_4)); }
	inline Il2CppObject* get_processes_4() const { return ___processes_4; }
	inline Il2CppObject** get_address_of_processes_4() { return &___processes_4; }
	inline void set_processes_4(Il2CppObject* value)
	{
		___processes_4 = value;
		Il2CppCodeGenWriteBarrier(&___processes_4, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
