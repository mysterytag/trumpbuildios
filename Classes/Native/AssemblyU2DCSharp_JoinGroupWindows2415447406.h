﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// BottomPanel
struct BottomPanel_t1044499065;
// UniRx.Subject`1<UniRx.Unit>
struct Subject_1_t201353362;
// Message
struct Message_t2619578343;
// BarygaVkController
struct BarygaVkController_t3617163921;

#include "AssemblyU2DCSharp_WindowBase3855465217.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// JoinGroupWindows
struct  JoinGroupWindows_t2415447406  : public WindowBase_t3855465217
{
public:
	// BottomPanel JoinGroupWindows::BottomPanel
	BottomPanel_t1044499065 * ___BottomPanel_6;
	// UniRx.Subject`1<UniRx.Unit> JoinGroupWindows::CloseSubject
	Subject_1_t201353362 * ___CloseSubject_7;
	// UniRx.Subject`1<UniRx.Unit> JoinGroupWindows::Do
	Subject_1_t201353362 * ___Do_8;
	// Message JoinGroupWindows::Message
	Message_t2619578343 * ___Message_9;
	// BarygaVkController JoinGroupWindows::BarygaVkController
	BarygaVkController_t3617163921 * ___BarygaVkController_10;

public:
	inline static int32_t get_offset_of_BottomPanel_6() { return static_cast<int32_t>(offsetof(JoinGroupWindows_t2415447406, ___BottomPanel_6)); }
	inline BottomPanel_t1044499065 * get_BottomPanel_6() const { return ___BottomPanel_6; }
	inline BottomPanel_t1044499065 ** get_address_of_BottomPanel_6() { return &___BottomPanel_6; }
	inline void set_BottomPanel_6(BottomPanel_t1044499065 * value)
	{
		___BottomPanel_6 = value;
		Il2CppCodeGenWriteBarrier(&___BottomPanel_6, value);
	}

	inline static int32_t get_offset_of_CloseSubject_7() { return static_cast<int32_t>(offsetof(JoinGroupWindows_t2415447406, ___CloseSubject_7)); }
	inline Subject_1_t201353362 * get_CloseSubject_7() const { return ___CloseSubject_7; }
	inline Subject_1_t201353362 ** get_address_of_CloseSubject_7() { return &___CloseSubject_7; }
	inline void set_CloseSubject_7(Subject_1_t201353362 * value)
	{
		___CloseSubject_7 = value;
		Il2CppCodeGenWriteBarrier(&___CloseSubject_7, value);
	}

	inline static int32_t get_offset_of_Do_8() { return static_cast<int32_t>(offsetof(JoinGroupWindows_t2415447406, ___Do_8)); }
	inline Subject_1_t201353362 * get_Do_8() const { return ___Do_8; }
	inline Subject_1_t201353362 ** get_address_of_Do_8() { return &___Do_8; }
	inline void set_Do_8(Subject_1_t201353362 * value)
	{
		___Do_8 = value;
		Il2CppCodeGenWriteBarrier(&___Do_8, value);
	}

	inline static int32_t get_offset_of_Message_9() { return static_cast<int32_t>(offsetof(JoinGroupWindows_t2415447406, ___Message_9)); }
	inline Message_t2619578343 * get_Message_9() const { return ___Message_9; }
	inline Message_t2619578343 ** get_address_of_Message_9() { return &___Message_9; }
	inline void set_Message_9(Message_t2619578343 * value)
	{
		___Message_9 = value;
		Il2CppCodeGenWriteBarrier(&___Message_9, value);
	}

	inline static int32_t get_offset_of_BarygaVkController_10() { return static_cast<int32_t>(offsetof(JoinGroupWindows_t2415447406, ___BarygaVkController_10)); }
	inline BarygaVkController_t3617163921 * get_BarygaVkController_10() const { return ___BarygaVkController_10; }
	inline BarygaVkController_t3617163921 ** get_address_of_BarygaVkController_10() { return &___BarygaVkController_10; }
	inline void set_BarygaVkController_10(BarygaVkController_t3617163921 * value)
	{
		___BarygaVkController_10 = value;
		Il2CppCodeGenWriteBarrier(&___BarygaVkController_10, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
