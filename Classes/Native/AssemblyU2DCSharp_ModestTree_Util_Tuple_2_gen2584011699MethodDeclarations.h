﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ModestTree.Util.Tuple`2<System.Object,System.Object>
struct Tuple_2_t2584011699;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"

// System.Void ModestTree.Util.Tuple`2<System.Object,System.Object>::.ctor()
extern "C"  void Tuple_2__ctor_m3534991723_gshared (Tuple_2_t2584011699 * __this, const MethodInfo* method);
#define Tuple_2__ctor_m3534991723(__this, method) ((  void (*) (Tuple_2_t2584011699 *, const MethodInfo*))Tuple_2__ctor_m3534991723_gshared)(__this, method)
// System.Void ModestTree.Util.Tuple`2<System.Object,System.Object>::.ctor(T1,T2)
extern "C"  void Tuple_2__ctor_m4054949306_gshared (Tuple_2_t2584011699 * __this, Il2CppObject * ___first0, Il2CppObject * ___second1, const MethodInfo* method);
#define Tuple_2__ctor_m4054949306(__this, ___first0, ___second1, method) ((  void (*) (Tuple_2_t2584011699 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))Tuple_2__ctor_m4054949306_gshared)(__this, ___first0, ___second1, method)
// System.Boolean ModestTree.Util.Tuple`2<System.Object,System.Object>::Equals(System.Object)
extern "C"  bool Tuple_2_Equals_m2602159336_gshared (Tuple_2_t2584011699 * __this, Il2CppObject * ___obj0, const MethodInfo* method);
#define Tuple_2_Equals_m2602159336(__this, ___obj0, method) ((  bool (*) (Tuple_2_t2584011699 *, Il2CppObject *, const MethodInfo*))Tuple_2_Equals_m2602159336_gshared)(__this, ___obj0, method)
// System.Boolean ModestTree.Util.Tuple`2<System.Object,System.Object>::Equals(ModestTree.Util.Tuple`2<T1,T2>)
extern "C"  bool Tuple_2_Equals_m2951541881_gshared (Tuple_2_t2584011699 * __this, Tuple_2_t2584011699 * ___that0, const MethodInfo* method);
#define Tuple_2_Equals_m2951541881(__this, ___that0, method) ((  bool (*) (Tuple_2_t2584011699 *, Tuple_2_t2584011699 *, const MethodInfo*))Tuple_2_Equals_m2951541881_gshared)(__this, ___that0, method)
// System.Int32 ModestTree.Util.Tuple`2<System.Object,System.Object>::GetHashCode()
extern "C"  int32_t Tuple_2_GetHashCode_m16712972_gshared (Tuple_2_t2584011699 * __this, const MethodInfo* method);
#define Tuple_2_GetHashCode_m16712972(__this, method) ((  int32_t (*) (Tuple_2_t2584011699 *, const MethodInfo*))Tuple_2_GetHashCode_m16712972_gshared)(__this, method)
