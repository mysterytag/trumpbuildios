﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.ClientModels.GetTitleNewsResult
struct GetTitleNewsResult_t422977346;
// System.Collections.Generic.List`1<PlayFab.ClientModels.TitleNewsItem>
struct List_1_t3320275943;

#include "codegen/il2cpp-codegen.h"

// System.Void PlayFab.ClientModels.GetTitleNewsResult::.ctor()
extern "C"  void GetTitleNewsResult__ctor_m636885179 (GetTitleNewsResult_t422977346 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<PlayFab.ClientModels.TitleNewsItem> PlayFab.ClientModels.GetTitleNewsResult::get_News()
extern "C"  List_1_t3320275943 * GetTitleNewsResult_get_News_m2836968498 (GetTitleNewsResult_t422977346 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.GetTitleNewsResult::set_News(System.Collections.Generic.List`1<PlayFab.ClientModels.TitleNewsItem>)
extern "C"  void GetTitleNewsResult_set_News_m1842271737 (GetTitleNewsResult_t422977346 * __this, List_1_t3320275943 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
