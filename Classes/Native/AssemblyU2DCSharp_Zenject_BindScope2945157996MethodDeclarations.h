﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Zenject.BindScope
struct BindScope_t2945157996;
// Zenject.DiContainer
struct DiContainer_t2383114449;
// Zenject.SingletonProviderMap
struct SingletonProviderMap_t1557411893;
// Zenject.BinderUntyped
struct BinderUntyped_t2430239676;
// System.Type
struct Type_t;
// System.String
struct String_t;
// Zenject.ProviderBase
struct ProviderBase_t1627494391;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Zenject_DiContainer2383114449.h"
#include "AssemblyU2DCSharp_Zenject_SingletonProviderMap1557411893.h"
#include "mscorlib_System_Type2779229935.h"
#include "mscorlib_System_String968488902.h"
#include "AssemblyU2DCSharp_Zenject_ProviderBase1627494391.h"

// System.Void Zenject.BindScope::.ctor(Zenject.DiContainer,Zenject.SingletonProviderMap)
extern "C"  void BindScope__ctor_m2413122213 (BindScope_t2945157996 * __this, DiContainer_t2383114449 * ___container0, SingletonProviderMap_t1557411893 * ___singletonMap1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Zenject.BinderUntyped Zenject.BindScope::Bind(System.Type)
extern "C"  BinderUntyped_t2430239676 * BindScope_Bind_m3334941526 (BindScope_t2945157996 * __this, Type_t * ___contractType0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Zenject.BinderUntyped Zenject.BindScope::Bind(System.Type,System.String)
extern "C"  BinderUntyped_t2430239676 * BindScope_Bind_m4186883346 (BindScope_t2945157996 * __this, Type_t * ___contractType0, String_t* ___identifier1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Zenject.BindScope::AddProvider(Zenject.ProviderBase)
extern "C"  void BindScope_AddProvider_m1601391874 (BindScope_t2945157996 * __this, ProviderBase_t1627494391 * ___provider0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Zenject.BindScope::Dispose()
extern "C"  void BindScope_Dispose_m1319489328 (BindScope_t2945157996 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
