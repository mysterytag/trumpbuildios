﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.ZipObservable`3/Zip/LeftZipObserver<System.Object,System.Object,System.Object>
struct LeftZipObserver_t922849585;
// UniRx.Operators.ZipObservable`3/Zip<System.Object,System.Object,System.Object>
struct Zip_t2358317490;
// System.Object
struct Il2CppObject;
// System.Exception
struct Exception_t1967233988;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_Exception1967233988.h"

// System.Void UniRx.Operators.ZipObservable`3/Zip/LeftZipObserver<System.Object,System.Object,System.Object>::.ctor(UniRx.Operators.ZipObservable`3/Zip<TLeft,TRight,TResult>)
extern "C"  void LeftZipObserver__ctor_m135947698_gshared (LeftZipObserver_t922849585 * __this, Zip_t2358317490 * ___parent0, const MethodInfo* method);
#define LeftZipObserver__ctor_m135947698(__this, ___parent0, method) ((  void (*) (LeftZipObserver_t922849585 *, Zip_t2358317490 *, const MethodInfo*))LeftZipObserver__ctor_m135947698_gshared)(__this, ___parent0, method)
// System.Void UniRx.Operators.ZipObservable`3/Zip/LeftZipObserver<System.Object,System.Object,System.Object>::OnNext(TLeft)
extern "C"  void LeftZipObserver_OnNext_m803787915_gshared (LeftZipObserver_t922849585 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define LeftZipObserver_OnNext_m803787915(__this, ___value0, method) ((  void (*) (LeftZipObserver_t922849585 *, Il2CppObject *, const MethodInfo*))LeftZipObserver_OnNext_m803787915_gshared)(__this, ___value0, method)
// System.Void UniRx.Operators.ZipObservable`3/Zip/LeftZipObserver<System.Object,System.Object,System.Object>::OnError(System.Exception)
extern "C"  void LeftZipObserver_OnError_m587585377_gshared (LeftZipObserver_t922849585 * __this, Exception_t1967233988 * ___ex0, const MethodInfo* method);
#define LeftZipObserver_OnError_m587585377(__this, ___ex0, method) ((  void (*) (LeftZipObserver_t922849585 *, Exception_t1967233988 *, const MethodInfo*))LeftZipObserver_OnError_m587585377_gshared)(__this, ___ex0, method)
// System.Void UniRx.Operators.ZipObservable`3/Zip/LeftZipObserver<System.Object,System.Object,System.Object>::OnCompleted()
extern "C"  void LeftZipObserver_OnCompleted_m2195444788_gshared (LeftZipObserver_t922849585 * __this, const MethodInfo* method);
#define LeftZipObserver_OnCompleted_m2195444788(__this, method) ((  void (*) (LeftZipObserver_t922849585 *, const MethodInfo*))LeftZipObserver_OnCompleted_m2195444788_gshared)(__this, method)
