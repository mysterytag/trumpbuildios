﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.WhenAllObservable`1/WhenAll<UniRx.Tuple`2<System.Object,System.Object>>
struct WhenAll_t491399631;
// UniRx.IObservable`1<UniRx.Tuple`2<System.Object,System.Object>>[]
struct IObservable_1U5BU5D_t1796868782;
// UniRx.IObserver`1<UniRx.Tuple`2<System.Object,System.Object>[]>
struct IObserver_1_t1814965617;
// System.IDisposable
struct IDisposable_t1628921374;
// UniRx.Tuple`2<System.Object,System.Object>[]
struct Tuple_2U5BU5D_t3897934010;
// System.Exception
struct Exception_t1967233988;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Exception1967233988.h"

// System.Void UniRx.Operators.WhenAllObservable`1/WhenAll<UniRx.Tuple`2<System.Object,System.Object>>::.ctor(UniRx.IObservable`1<T>[],UniRx.IObserver`1<T[]>,System.IDisposable)
extern "C"  void WhenAll__ctor_m4155808608_gshared (WhenAll_t491399631 * __this, IObservable_1U5BU5D_t1796868782* ___sources0, Il2CppObject* ___observer1, Il2CppObject * ___cancel2, const MethodInfo* method);
#define WhenAll__ctor_m4155808608(__this, ___sources0, ___observer1, ___cancel2, method) ((  void (*) (WhenAll_t491399631 *, IObservable_1U5BU5D_t1796868782*, Il2CppObject*, Il2CppObject *, const MethodInfo*))WhenAll__ctor_m4155808608_gshared)(__this, ___sources0, ___observer1, ___cancel2, method)
// System.IDisposable UniRx.Operators.WhenAllObservable`1/WhenAll<UniRx.Tuple`2<System.Object,System.Object>>::Run()
extern "C"  Il2CppObject * WhenAll_Run_m4052510853_gshared (WhenAll_t491399631 * __this, const MethodInfo* method);
#define WhenAll_Run_m4052510853(__this, method) ((  Il2CppObject * (*) (WhenAll_t491399631 *, const MethodInfo*))WhenAll_Run_m4052510853_gshared)(__this, method)
// System.Void UniRx.Operators.WhenAllObservable`1/WhenAll<UniRx.Tuple`2<System.Object,System.Object>>::OnNext(T[])
extern "C"  void WhenAll_OnNext_m2227541351_gshared (WhenAll_t491399631 * __this, Tuple_2U5BU5D_t3897934010* ___value0, const MethodInfo* method);
#define WhenAll_OnNext_m2227541351(__this, ___value0, method) ((  void (*) (WhenAll_t491399631 *, Tuple_2U5BU5D_t3897934010*, const MethodInfo*))WhenAll_OnNext_m2227541351_gshared)(__this, ___value0, method)
// System.Void UniRx.Operators.WhenAllObservable`1/WhenAll<UniRx.Tuple`2<System.Object,System.Object>>::OnError(System.Exception)
extern "C"  void WhenAll_OnError_m4162434456_gshared (WhenAll_t491399631 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method);
#define WhenAll_OnError_m4162434456(__this, ___error0, method) ((  void (*) (WhenAll_t491399631 *, Exception_t1967233988 *, const MethodInfo*))WhenAll_OnError_m4162434456_gshared)(__this, ___error0, method)
// System.Void UniRx.Operators.WhenAllObservable`1/WhenAll<UniRx.Tuple`2<System.Object,System.Object>>::OnCompleted()
extern "C"  void WhenAll_OnCompleted_m3151629547_gshared (WhenAll_t491399631 * __this, const MethodInfo* method);
#define WhenAll_OnCompleted_m3151629547(__this, method) ((  void (*) (WhenAll_t491399631 *, const MethodInfo*))WhenAll_OnCompleted_m3151629547_gshared)(__this, method)
