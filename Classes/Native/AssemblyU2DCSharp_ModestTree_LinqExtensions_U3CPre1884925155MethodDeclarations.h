﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ModestTree.LinqExtensions/<Prepend>c__Iterator37`1<System.Object>
struct U3CPrependU3Ec__Iterator37_1_t1884925155;
// System.Object
struct Il2CppObject;
// System.Collections.IEnumerator
struct IEnumerator_t287207039;
// System.Collections.Generic.IEnumerator`1<System.Object>
struct IEnumerator_1_t2320212868;

#include "codegen/il2cpp-codegen.h"

// System.Void ModestTree.LinqExtensions/<Prepend>c__Iterator37`1<System.Object>::.ctor()
extern "C"  void U3CPrependU3Ec__Iterator37_1__ctor_m1486960792_gshared (U3CPrependU3Ec__Iterator37_1_t1884925155 * __this, const MethodInfo* method);
#define U3CPrependU3Ec__Iterator37_1__ctor_m1486960792(__this, method) ((  void (*) (U3CPrependU3Ec__Iterator37_1_t1884925155 *, const MethodInfo*))U3CPrependU3Ec__Iterator37_1__ctor_m1486960792_gshared)(__this, method)
// T ModestTree.LinqExtensions/<Prepend>c__Iterator37`1<System.Object>::System.Collections.Generic.IEnumerator<T>.get_Current()
extern "C"  Il2CppObject * U3CPrependU3Ec__Iterator37_1_System_Collections_Generic_IEnumeratorU3CTU3E_get_Current_m1462883807_gshared (U3CPrependU3Ec__Iterator37_1_t1884925155 * __this, const MethodInfo* method);
#define U3CPrependU3Ec__Iterator37_1_System_Collections_Generic_IEnumeratorU3CTU3E_get_Current_m1462883807(__this, method) ((  Il2CppObject * (*) (U3CPrependU3Ec__Iterator37_1_t1884925155 *, const MethodInfo*))U3CPrependU3Ec__Iterator37_1_System_Collections_Generic_IEnumeratorU3CTU3E_get_Current_m1462883807_gshared)(__this, method)
// System.Object ModestTree.LinqExtensions/<Prepend>c__Iterator37`1<System.Object>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CPrependU3Ec__Iterator37_1_System_Collections_IEnumerator_get_Current_m930842254_gshared (U3CPrependU3Ec__Iterator37_1_t1884925155 * __this, const MethodInfo* method);
#define U3CPrependU3Ec__Iterator37_1_System_Collections_IEnumerator_get_Current_m930842254(__this, method) ((  Il2CppObject * (*) (U3CPrependU3Ec__Iterator37_1_t1884925155 *, const MethodInfo*))U3CPrependU3Ec__Iterator37_1_System_Collections_IEnumerator_get_Current_m930842254_gshared)(__this, method)
// System.Collections.IEnumerator ModestTree.LinqExtensions/<Prepend>c__Iterator37`1<System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * U3CPrependU3Ec__Iterator37_1_System_Collections_IEnumerable_GetEnumerator_m2374052297_gshared (U3CPrependU3Ec__Iterator37_1_t1884925155 * __this, const MethodInfo* method);
#define U3CPrependU3Ec__Iterator37_1_System_Collections_IEnumerable_GetEnumerator_m2374052297(__this, method) ((  Il2CppObject * (*) (U3CPrependU3Ec__Iterator37_1_t1884925155 *, const MethodInfo*))U3CPrependU3Ec__Iterator37_1_System_Collections_IEnumerable_GetEnumerator_m2374052297_gshared)(__this, method)
// System.Collections.Generic.IEnumerator`1<T> ModestTree.LinqExtensions/<Prepend>c__Iterator37`1<System.Object>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
extern "C"  Il2CppObject* U3CPrependU3Ec__Iterator37_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m3578865058_gshared (U3CPrependU3Ec__Iterator37_1_t1884925155 * __this, const MethodInfo* method);
#define U3CPrependU3Ec__Iterator37_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m3578865058(__this, method) ((  Il2CppObject* (*) (U3CPrependU3Ec__Iterator37_1_t1884925155 *, const MethodInfo*))U3CPrependU3Ec__Iterator37_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m3578865058_gshared)(__this, method)
// System.Boolean ModestTree.LinqExtensions/<Prepend>c__Iterator37`1<System.Object>::MoveNext()
extern "C"  bool U3CPrependU3Ec__Iterator37_1_MoveNext_m2435304284_gshared (U3CPrependU3Ec__Iterator37_1_t1884925155 * __this, const MethodInfo* method);
#define U3CPrependU3Ec__Iterator37_1_MoveNext_m2435304284(__this, method) ((  bool (*) (U3CPrependU3Ec__Iterator37_1_t1884925155 *, const MethodInfo*))U3CPrependU3Ec__Iterator37_1_MoveNext_m2435304284_gshared)(__this, method)
// System.Void ModestTree.LinqExtensions/<Prepend>c__Iterator37`1<System.Object>::Dispose()
extern "C"  void U3CPrependU3Ec__Iterator37_1_Dispose_m2938461909_gshared (U3CPrependU3Ec__Iterator37_1_t1884925155 * __this, const MethodInfo* method);
#define U3CPrependU3Ec__Iterator37_1_Dispose_m2938461909(__this, method) ((  void (*) (U3CPrependU3Ec__Iterator37_1_t1884925155 *, const MethodInfo*))U3CPrependU3Ec__Iterator37_1_Dispose_m2938461909_gshared)(__this, method)
// System.Void ModestTree.LinqExtensions/<Prepend>c__Iterator37`1<System.Object>::Reset()
extern "C"  void U3CPrependU3Ec__Iterator37_1_Reset_m3428361029_gshared (U3CPrependU3Ec__Iterator37_1_t1884925155 * __this, const MethodInfo* method);
#define U3CPrependU3Ec__Iterator37_1_Reset_m3428361029(__this, method) ((  void (*) (U3CPrependU3Ec__Iterator37_1_t1884925155 *, const MethodInfo*))U3CPrependU3Ec__Iterator37_1_Reset_m3428361029_gshared)(__this, method)
