﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Zenject.BindScope/CustomScopeUntypedBinder
struct CustomScopeUntypedBinder_t2439484920;
// Zenject.BindScope
struct BindScope_t2945157996;
// System.Type
struct Type_t;
// System.String
struct String_t;
// Zenject.DiContainer
struct DiContainer_t2383114449;
// Zenject.SingletonProviderMap
struct SingletonProviderMap_t1557411893;
// Zenject.BindingConditionSetter
struct BindingConditionSetter_t259147722;
// Zenject.ProviderBase
struct ProviderBase_t1627494391;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Zenject_BindScope2945157996.h"
#include "mscorlib_System_Type2779229935.h"
#include "mscorlib_System_String968488902.h"
#include "AssemblyU2DCSharp_Zenject_DiContainer2383114449.h"
#include "AssemblyU2DCSharp_Zenject_SingletonProviderMap1557411893.h"
#include "AssemblyU2DCSharp_Zenject_ProviderBase1627494391.h"

// System.Void Zenject.BindScope/CustomScopeUntypedBinder::.ctor(Zenject.BindScope,System.Type,System.String,Zenject.DiContainer,Zenject.SingletonProviderMap)
extern "C"  void CustomScopeUntypedBinder__ctor_m497987451 (CustomScopeUntypedBinder_t2439484920 * __this, BindScope_t2945157996 * ___owner0, Type_t * ___contractType1, String_t* ___identifier2, DiContainer_t2383114449 * ___container3, SingletonProviderMap_t1557411893 * ___singletonMap4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Zenject.BindingConditionSetter Zenject.BindScope/CustomScopeUntypedBinder::ToProvider(Zenject.ProviderBase)
extern "C"  BindingConditionSetter_t259147722 * CustomScopeUntypedBinder_ToProvider_m2994840140 (CustomScopeUntypedBinder_t2439484920 * __this, ProviderBase_t1627494391 * ___provider0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
