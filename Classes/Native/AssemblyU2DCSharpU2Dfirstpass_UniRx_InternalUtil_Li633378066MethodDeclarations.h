﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.InternalUtil.ListObserver`1<UnityEngine.NetworkDisconnection>
struct ListObserver_1_t633378066;
// UniRx.InternalUtil.ImmutableList`1<UniRx.IObserver`1<UnityEngine.NetworkDisconnection>>
struct ImmutableList_1_t3610963797;
// System.Exception
struct Exception_t1967233988;
// UniRx.IObserver`1<UnityEngine.NetworkDisconnection>
struct IObserver_1_t2550759298;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Exception1967233988.h"
#include "UnityEngine_UnityEngine_NetworkDisconnection338760395.h"

// System.Void UniRx.InternalUtil.ListObserver`1<UnityEngine.NetworkDisconnection>::.ctor(UniRx.InternalUtil.ImmutableList`1<UniRx.IObserver`1<T>>)
extern "C"  void ListObserver_1__ctor_m2654633216_gshared (ListObserver_1_t633378066 * __this, ImmutableList_1_t3610963797 * ___observers0, const MethodInfo* method);
#define ListObserver_1__ctor_m2654633216(__this, ___observers0, method) ((  void (*) (ListObserver_1_t633378066 *, ImmutableList_1_t3610963797 *, const MethodInfo*))ListObserver_1__ctor_m2654633216_gshared)(__this, ___observers0, method)
// System.Void UniRx.InternalUtil.ListObserver`1<UnityEngine.NetworkDisconnection>::OnCompleted()
extern "C"  void ListObserver_1_OnCompleted_m4267534136_gshared (ListObserver_1_t633378066 * __this, const MethodInfo* method);
#define ListObserver_1_OnCompleted_m4267534136(__this, method) ((  void (*) (ListObserver_1_t633378066 *, const MethodInfo*))ListObserver_1_OnCompleted_m4267534136_gshared)(__this, method)
// System.Void UniRx.InternalUtil.ListObserver`1<UnityEngine.NetworkDisconnection>::OnError(System.Exception)
extern "C"  void ListObserver_1_OnError_m1540293733_gshared (ListObserver_1_t633378066 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method);
#define ListObserver_1_OnError_m1540293733(__this, ___error0, method) ((  void (*) (ListObserver_1_t633378066 *, Exception_t1967233988 *, const MethodInfo*))ListObserver_1_OnError_m1540293733_gshared)(__this, ___error0, method)
// System.Void UniRx.InternalUtil.ListObserver`1<UnityEngine.NetworkDisconnection>::OnNext(T)
extern "C"  void ListObserver_1_OnNext_m4173616470_gshared (ListObserver_1_t633378066 * __this, int32_t ___value0, const MethodInfo* method);
#define ListObserver_1_OnNext_m4173616470(__this, ___value0, method) ((  void (*) (ListObserver_1_t633378066 *, int32_t, const MethodInfo*))ListObserver_1_OnNext_m4173616470_gshared)(__this, ___value0, method)
// UniRx.IObserver`1<T> UniRx.InternalUtil.ListObserver`1<UnityEngine.NetworkDisconnection>::Add(UniRx.IObserver`1<T>)
extern "C"  Il2CppObject* ListObserver_1_Add_m4160006326_gshared (ListObserver_1_t633378066 * __this, Il2CppObject* ___observer0, const MethodInfo* method);
#define ListObserver_1_Add_m4160006326(__this, ___observer0, method) ((  Il2CppObject* (*) (ListObserver_1_t633378066 *, Il2CppObject*, const MethodInfo*))ListObserver_1_Add_m4160006326_gshared)(__this, ___observer0, method)
// UniRx.IObserver`1<T> UniRx.InternalUtil.ListObserver`1<UnityEngine.NetworkDisconnection>::Remove(UniRx.IObserver`1<T>)
extern "C"  Il2CppObject* ListObserver_1_Remove_m4195544789_gshared (ListObserver_1_t633378066 * __this, Il2CppObject* ___observer0, const MethodInfo* method);
#define ListObserver_1_Remove_m4195544789(__this, ___observer0, method) ((  Il2CppObject* (*) (ListObserver_1_t633378066 *, Il2CppObject*, const MethodInfo*))ListObserver_1_Remove_m4195544789_gshared)(__this, ___observer0, method)
