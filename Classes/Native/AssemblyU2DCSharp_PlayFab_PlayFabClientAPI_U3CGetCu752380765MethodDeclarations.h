﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.PlayFabClientAPI/<GetCurrentGames>c__AnonStoreyB4
struct U3CGetCurrentGamesU3Ec__AnonStoreyB4_t752380765;
// PlayFab.CallRequestContainer
struct CallRequestContainer_t432318609;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharpU2Dfirstpass_PlayFab_CallRequestCo432318609.h"

// System.Void PlayFab.PlayFabClientAPI/<GetCurrentGames>c__AnonStoreyB4::.ctor()
extern "C"  void U3CGetCurrentGamesU3Ec__AnonStoreyB4__ctor_m2764024982 (U3CGetCurrentGamesU3Ec__AnonStoreyB4_t752380765 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/<GetCurrentGames>c__AnonStoreyB4::<>m__A1(PlayFab.CallRequestContainer)
extern "C"  void U3CGetCurrentGamesU3Ec__AnonStoreyB4_U3CU3Em__A1_m1568251140 (U3CGetCurrentGamesU3Ec__AnonStoreyB4_t752380765 * __this, CallRequestContainer_t432318609 * ___requestContainer0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
