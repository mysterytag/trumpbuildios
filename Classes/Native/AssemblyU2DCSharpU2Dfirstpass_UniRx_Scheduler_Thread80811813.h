﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Generic.HashSet`1<System.Threading.Timer>
struct HashSet_1_t1949832359;
// UniRx.SingleAssignmentDisposable
struct SingleAssignmentDisposable_t2336378823;
// System.Action
struct Action_t437523947;
// System.Threading.Timer
struct Timer_t3546110984;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Scheduler/ThreadPoolScheduler/Timer
struct  Timer_t80811814  : public Il2CppObject
{
public:
	// UniRx.SingleAssignmentDisposable UniRx.Scheduler/ThreadPoolScheduler/Timer::_disposable
	SingleAssignmentDisposable_t2336378823 * ____disposable_1;
	// System.Action UniRx.Scheduler/ThreadPoolScheduler/Timer::_action
	Action_t437523947 * ____action_2;
	// System.Threading.Timer UniRx.Scheduler/ThreadPoolScheduler/Timer::_timer
	Timer_t3546110984 * ____timer_3;
	// System.Boolean UniRx.Scheduler/ThreadPoolScheduler/Timer::_hasAdded
	bool ____hasAdded_4;
	// System.Boolean UniRx.Scheduler/ThreadPoolScheduler/Timer::_hasRemoved
	bool ____hasRemoved_5;

public:
	inline static int32_t get_offset_of__disposable_1() { return static_cast<int32_t>(offsetof(Timer_t80811814, ____disposable_1)); }
	inline SingleAssignmentDisposable_t2336378823 * get__disposable_1() const { return ____disposable_1; }
	inline SingleAssignmentDisposable_t2336378823 ** get_address_of__disposable_1() { return &____disposable_1; }
	inline void set__disposable_1(SingleAssignmentDisposable_t2336378823 * value)
	{
		____disposable_1 = value;
		Il2CppCodeGenWriteBarrier(&____disposable_1, value);
	}

	inline static int32_t get_offset_of__action_2() { return static_cast<int32_t>(offsetof(Timer_t80811814, ____action_2)); }
	inline Action_t437523947 * get__action_2() const { return ____action_2; }
	inline Action_t437523947 ** get_address_of__action_2() { return &____action_2; }
	inline void set__action_2(Action_t437523947 * value)
	{
		____action_2 = value;
		Il2CppCodeGenWriteBarrier(&____action_2, value);
	}

	inline static int32_t get_offset_of__timer_3() { return static_cast<int32_t>(offsetof(Timer_t80811814, ____timer_3)); }
	inline Timer_t3546110984 * get__timer_3() const { return ____timer_3; }
	inline Timer_t3546110984 ** get_address_of__timer_3() { return &____timer_3; }
	inline void set__timer_3(Timer_t3546110984 * value)
	{
		____timer_3 = value;
		Il2CppCodeGenWriteBarrier(&____timer_3, value);
	}

	inline static int32_t get_offset_of__hasAdded_4() { return static_cast<int32_t>(offsetof(Timer_t80811814, ____hasAdded_4)); }
	inline bool get__hasAdded_4() const { return ____hasAdded_4; }
	inline bool* get_address_of__hasAdded_4() { return &____hasAdded_4; }
	inline void set__hasAdded_4(bool value)
	{
		____hasAdded_4 = value;
	}

	inline static int32_t get_offset_of__hasRemoved_5() { return static_cast<int32_t>(offsetof(Timer_t80811814, ____hasRemoved_5)); }
	inline bool get__hasRemoved_5() const { return ____hasRemoved_5; }
	inline bool* get_address_of__hasRemoved_5() { return &____hasRemoved_5; }
	inline void set__hasRemoved_5(bool value)
	{
		____hasRemoved_5 = value;
	}
};

struct Timer_t80811814_StaticFields
{
public:
	// System.Collections.Generic.HashSet`1<System.Threading.Timer> UniRx.Scheduler/ThreadPoolScheduler/Timer::s_timers
	HashSet_1_t1949832359 * ___s_timers_0;

public:
	inline static int32_t get_offset_of_s_timers_0() { return static_cast<int32_t>(offsetof(Timer_t80811814_StaticFields, ___s_timers_0)); }
	inline HashSet_1_t1949832359 * get_s_timers_0() const { return ___s_timers_0; }
	inline HashSet_1_t1949832359 ** get_address_of_s_timers_0() { return &___s_timers_0; }
	inline void set_s_timers_0(HashSet_1_t1949832359 * value)
	{
		___s_timers_0 = value;
		Il2CppCodeGenWriteBarrier(&___s_timers_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
