﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.InternalUtil.AsyncLock
struct AsyncLock_t4023816884;
// System.Action
struct Action_t437523947;

#include "codegen/il2cpp-codegen.h"
#include "System_Core_System_Action437523947.h"

// System.Void UniRx.InternalUtil.AsyncLock::.ctor()
extern "C"  void AsyncLock__ctor_m3596353695 (AsyncLock_t4023816884 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.InternalUtil.AsyncLock::Wait(System.Action)
extern "C"  void AsyncLock_Wait_m1580205763 (AsyncLock_t4023816884 * __this, Action_t437523947 * ___action0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.InternalUtil.AsyncLock::Dispose()
extern "C"  void AsyncLock_Dispose_m2840477980 (AsyncLock_t4023816884 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
