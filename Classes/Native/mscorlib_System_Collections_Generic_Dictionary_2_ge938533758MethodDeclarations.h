﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2<GameAnalyticsSDK.GA_ServerFieldTypes/FieldType,System.Object>
struct Dictionary_2_t938533758;
// System.Collections.Generic.IEqualityComparer`1<GameAnalyticsSDK.GA_ServerFieldTypes/FieldType>
struct IEqualityComparer_1_t1298478543;
// System.Collections.Generic.IDictionary`2<GameAnalyticsSDK.GA_ServerFieldTypes/FieldType,System.Object>
struct IDictionary_2_t2114199545;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t2995724695;
// System.Collections.Generic.ICollection`1<GameAnalyticsSDK.GA_ServerFieldTypes/FieldType>
struct ICollection_1_t3735010574;
// System.Collections.Generic.ICollection`1<System.Object>
struct ICollection_1_t1302937806;
// System.Collections.ICollection
struct ICollection_t3761522009;
// System.Object
struct Il2CppObject;
// System.Collections.Generic.KeyValuePair`2<GameAnalyticsSDK.GA_ServerFieldTypes/FieldType,System.Object>[]
struct KeyValuePair_2U5BU5D_t3853666721;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t287207039;
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<GameAnalyticsSDK.GA_ServerFieldTypes/FieldType,System.Object>>
struct IEnumerator_1_t1910171504;
// System.Collections.IDictionaryEnumerator
struct IDictionaryEnumerator_t1541724277;
// System.Collections.Generic.Dictionary`2/KeyCollection<GameAnalyticsSDK.GA_ServerFieldTypes/FieldType,System.Object>
struct KeyCollection_t3261809038;
// System.Collections.Generic.Dictionary`2/ValueCollection<GameAnalyticsSDK.GA_ServerFieldTypes/FieldType,System.Object>
struct ValueCollection_t2860670852;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Runtime_Serialization_Serializatio2995724695.h"
#include "mscorlib_System_Runtime_Serialization_StreamingCont986364934.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_427065056.h"
#include "mscorlib_System_Array2840145358.h"
#include "AssemblyU2DCSharp_GameAnalyticsSDK_GA_ServerFieldT3269179188.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_En705561699.h"
#include "mscorlib_System_Collections_DictionaryEntry130027246.h"

// System.Void System.Collections.Generic.Dictionary`2<GameAnalyticsSDK.GA_ServerFieldTypes/FieldType,System.Object>::.ctor()
extern "C"  void Dictionary_2__ctor_m574749105_gshared (Dictionary_2_t938533758 * __this, const MethodInfo* method);
#define Dictionary_2__ctor_m574749105(__this, method) ((  void (*) (Dictionary_2_t938533758 *, const MethodInfo*))Dictionary_2__ctor_m574749105_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<GameAnalyticsSDK.GA_ServerFieldTypes/FieldType,System.Object>::.ctor(System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C"  void Dictionary_2__ctor_m3308210600_gshared (Dictionary_2_t938533758 * __this, Il2CppObject* ___comparer0, const MethodInfo* method);
#define Dictionary_2__ctor_m3308210600(__this, ___comparer0, method) ((  void (*) (Dictionary_2_t938533758 *, Il2CppObject*, const MethodInfo*))Dictionary_2__ctor_m3308210600_gshared)(__this, ___comparer0, method)
// System.Void System.Collections.Generic.Dictionary`2<GameAnalyticsSDK.GA_ServerFieldTypes/FieldType,System.Object>::.ctor(System.Collections.Generic.IDictionary`2<TKey,TValue>)
extern "C"  void Dictionary_2__ctor_m3786006663_gshared (Dictionary_2_t938533758 * __this, Il2CppObject* ___dictionary0, const MethodInfo* method);
#define Dictionary_2__ctor_m3786006663(__this, ___dictionary0, method) ((  void (*) (Dictionary_2_t938533758 *, Il2CppObject*, const MethodInfo*))Dictionary_2__ctor_m3786006663_gshared)(__this, ___dictionary0, method)
// System.Void System.Collections.Generic.Dictionary`2<GameAnalyticsSDK.GA_ServerFieldTypes/FieldType,System.Object>::.ctor(System.Int32)
extern "C"  void Dictionary_2__ctor_m960081282_gshared (Dictionary_2_t938533758 * __this, int32_t ___capacity0, const MethodInfo* method);
#define Dictionary_2__ctor_m960081282(__this, ___capacity0, method) ((  void (*) (Dictionary_2_t938533758 *, int32_t, const MethodInfo*))Dictionary_2__ctor_m960081282_gshared)(__this, ___capacity0, method)
// System.Void System.Collections.Generic.Dictionary`2<GameAnalyticsSDK.GA_ServerFieldTypes/FieldType,System.Object>::.ctor(System.Collections.Generic.IDictionary`2<TKey,TValue>,System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C"  void Dictionary_2__ctor_m3945653718_gshared (Dictionary_2_t938533758 * __this, Il2CppObject* ___dictionary0, Il2CppObject* ___comparer1, const MethodInfo* method);
#define Dictionary_2__ctor_m3945653718(__this, ___dictionary0, ___comparer1, method) ((  void (*) (Dictionary_2_t938533758 *, Il2CppObject*, Il2CppObject*, const MethodInfo*))Dictionary_2__ctor_m3945653718_gshared)(__this, ___dictionary0, ___comparer1, method)
// System.Void System.Collections.Generic.Dictionary`2<GameAnalyticsSDK.GA_ServerFieldTypes/FieldType,System.Object>::.ctor(System.Int32,System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C"  void Dictionary_2__ctor_m819642299_gshared (Dictionary_2_t938533758 * __this, int32_t ___capacity0, Il2CppObject* ___comparer1, const MethodInfo* method);
#define Dictionary_2__ctor_m819642299(__this, ___capacity0, ___comparer1, method) ((  void (*) (Dictionary_2_t938533758 *, int32_t, Il2CppObject*, const MethodInfo*))Dictionary_2__ctor_m819642299_gshared)(__this, ___capacity0, ___comparer1, method)
// System.Void System.Collections.Generic.Dictionary`2<GameAnalyticsSDK.GA_ServerFieldTypes/FieldType,System.Object>::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C"  void Dictionary_2__ctor_m4248645490_gshared (Dictionary_2_t938533758 * __this, SerializationInfo_t2995724695 * ___info0, StreamingContext_t986364934  ___context1, const MethodInfo* method);
#define Dictionary_2__ctor_m4248645490(__this, ___info0, ___context1, method) ((  void (*) (Dictionary_2_t938533758 *, SerializationInfo_t2995724695 *, StreamingContext_t986364934 , const MethodInfo*))Dictionary_2__ctor_m4248645490_gshared)(__this, ___info0, ___context1, method)
// System.Collections.Generic.ICollection`1<TKey> System.Collections.Generic.Dictionary`2<GameAnalyticsSDK.GA_ServerFieldTypes/FieldType,System.Object>::System.Collections.Generic.IDictionary<TKey,TValue>.get_Keys()
extern "C"  Il2CppObject* Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m1258586695_gshared (Dictionary_2_t938533758 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m1258586695(__this, method) ((  Il2CppObject* (*) (Dictionary_2_t938533758 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m1258586695_gshared)(__this, method)
// System.Collections.Generic.ICollection`1<TValue> System.Collections.Generic.Dictionary`2<GameAnalyticsSDK.GA_ServerFieldTypes/FieldType,System.Object>::System.Collections.Generic.IDictionary<TKey,TValue>.get_Values()
extern "C"  Il2CppObject* Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Values_m3929941347_gshared (Dictionary_2_t938533758 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Values_m3929941347(__this, method) ((  Il2CppObject* (*) (Dictionary_2_t938533758 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Values_m3929941347_gshared)(__this, method)
// System.Collections.ICollection System.Collections.Generic.Dictionary`2<GameAnalyticsSDK.GA_ServerFieldTypes/FieldType,System.Object>::System.Collections.IDictionary.get_Keys()
extern "C"  Il2CppObject * Dictionary_2_System_Collections_IDictionary_get_Keys_m3737011887_gshared (Dictionary_2_t938533758 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_get_Keys_m3737011887(__this, method) ((  Il2CppObject * (*) (Dictionary_2_t938533758 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_Keys_m3737011887_gshared)(__this, method)
// System.Collections.ICollection System.Collections.Generic.Dictionary`2<GameAnalyticsSDK.GA_ServerFieldTypes/FieldType,System.Object>::System.Collections.IDictionary.get_Values()
extern "C"  Il2CppObject * Dictionary_2_System_Collections_IDictionary_get_Values_m3043318749_gshared (Dictionary_2_t938533758 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_get_Values_m3043318749(__this, method) ((  Il2CppObject * (*) (Dictionary_2_t938533758 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_Values_m3043318749_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<GameAnalyticsSDK.GA_ServerFieldTypes/FieldType,System.Object>::System.Collections.IDictionary.get_IsFixedSize()
extern "C"  bool Dictionary_2_System_Collections_IDictionary_get_IsFixedSize_m1837648652_gshared (Dictionary_2_t938533758 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_get_IsFixedSize_m1837648652(__this, method) ((  bool (*) (Dictionary_2_t938533758 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_IsFixedSize_m1837648652_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<GameAnalyticsSDK.GA_ServerFieldTypes/FieldType,System.Object>::System.Collections.IDictionary.get_IsReadOnly()
extern "C"  bool Dictionary_2_System_Collections_IDictionary_get_IsReadOnly_m3699876461_gshared (Dictionary_2_t938533758 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_get_IsReadOnly_m3699876461(__this, method) ((  bool (*) (Dictionary_2_t938533758 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_IsReadOnly_m3699876461_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2<GameAnalyticsSDK.GA_ServerFieldTypes/FieldType,System.Object>::System.Collections.IDictionary.get_Item(System.Object)
extern "C"  Il2CppObject * Dictionary_2_System_Collections_IDictionary_get_Item_m82154861_gshared (Dictionary_2_t938533758 * __this, Il2CppObject * ___key0, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_get_Item_m82154861(__this, ___key0, method) ((  Il2CppObject * (*) (Dictionary_2_t938533758 *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_Item_m82154861_gshared)(__this, ___key0, method)
// System.Void System.Collections.Generic.Dictionary`2<GameAnalyticsSDK.GA_ServerFieldTypes/FieldType,System.Object>::System.Collections.IDictionary.set_Item(System.Object,System.Object)
extern "C"  void Dictionary_2_System_Collections_IDictionary_set_Item_m3756764444_gshared (Dictionary_2_t938533758 * __this, Il2CppObject * ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_set_Item_m3756764444(__this, ___key0, ___value1, method) ((  void (*) (Dictionary_2_t938533758 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_set_Item_m3756764444_gshared)(__this, ___key0, ___value1, method)
// System.Void System.Collections.Generic.Dictionary`2<GameAnalyticsSDK.GA_ServerFieldTypes/FieldType,System.Object>::System.Collections.IDictionary.Add(System.Object,System.Object)
extern "C"  void Dictionary_2_System_Collections_IDictionary_Add_m504483765_gshared (Dictionary_2_t938533758 * __this, Il2CppObject * ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Add_m504483765(__this, ___key0, ___value1, method) ((  void (*) (Dictionary_2_t938533758 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Add_m504483765_gshared)(__this, ___key0, ___value1, method)
// System.Boolean System.Collections.Generic.Dictionary`2<GameAnalyticsSDK.GA_ServerFieldTypes/FieldType,System.Object>::System.Collections.IDictionary.Contains(System.Object)
extern "C"  bool Dictionary_2_System_Collections_IDictionary_Contains_m647147997_gshared (Dictionary_2_t938533758 * __this, Il2CppObject * ___key0, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Contains_m647147997(__this, ___key0, method) ((  bool (*) (Dictionary_2_t938533758 *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Contains_m647147997_gshared)(__this, ___key0, method)
// System.Void System.Collections.Generic.Dictionary`2<GameAnalyticsSDK.GA_ServerFieldTypes/FieldType,System.Object>::System.Collections.IDictionary.Remove(System.Object)
extern "C"  void Dictionary_2_System_Collections_IDictionary_Remove_m661905242_gshared (Dictionary_2_t938533758 * __this, Il2CppObject * ___key0, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Remove_m661905242(__this, ___key0, method) ((  void (*) (Dictionary_2_t938533758 *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Remove_m661905242_gshared)(__this, ___key0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<GameAnalyticsSDK.GA_ServerFieldTypes/FieldType,System.Object>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m1249809047_gshared (Dictionary_2_t938533758 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m1249809047(__this, method) ((  bool (*) (Dictionary_2_t938533758 *, const MethodInfo*))Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m1249809047_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2<GameAnalyticsSDK.GA_ServerFieldTypes/FieldType,System.Object>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * Dictionary_2_System_Collections_ICollection_get_SyncRoot_m399356809_gshared (Dictionary_2_t938533758 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_get_SyncRoot_m399356809(__this, method) ((  Il2CppObject * (*) (Dictionary_2_t938533758 *, const MethodInfo*))Dictionary_2_System_Collections_ICollection_get_SyncRoot_m399356809_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<GameAnalyticsSDK.GA_ServerFieldTypes/FieldType,System.Object>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.get_IsReadOnly()
extern "C"  bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m3789803547_gshared (Dictionary_2_t938533758 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m3789803547(__this, method) ((  bool (*) (Dictionary_2_t938533758 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m3789803547_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<GameAnalyticsSDK.GA_ServerFieldTypes/FieldType,System.Object>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Add(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C"  void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m3938227952_gshared (Dictionary_2_t938533758 * __this, KeyValuePair_2_t427065056  ___keyValuePair0, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m3938227952(__this, ___keyValuePair0, method) ((  void (*) (Dictionary_2_t938533758 *, KeyValuePair_2_t427065056 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m3938227952_gshared)(__this, ___keyValuePair0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<GameAnalyticsSDK.GA_ServerFieldTypes/FieldType,System.Object>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Contains(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C"  bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m4095872662_gshared (Dictionary_2_t938533758 * __this, KeyValuePair_2_t427065056  ___keyValuePair0, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m4095872662(__this, ___keyValuePair0, method) ((  bool (*) (Dictionary_2_t938533758 *, KeyValuePair_2_t427065056 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m4095872662_gshared)(__this, ___keyValuePair0, method)
// System.Void System.Collections.Generic.Dictionary`2<GameAnalyticsSDK.GA_ServerFieldTypes/FieldType,System.Object>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
extern "C"  void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m2782636756_gshared (Dictionary_2_t938533758 * __this, KeyValuePair_2U5BU5D_t3853666721* ___array0, int32_t ___index1, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m2782636756(__this, ___array0, ___index1, method) ((  void (*) (Dictionary_2_t938533758 *, KeyValuePair_2U5BU5D_t3853666721*, int32_t, const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m2782636756_gshared)(__this, ___array0, ___index1, method)
// System.Boolean System.Collections.Generic.Dictionary`2<GameAnalyticsSDK.GA_ServerFieldTypes/FieldType,System.Object>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Remove(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C"  bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m3169772347_gshared (Dictionary_2_t938533758 * __this, KeyValuePair_2_t427065056  ___keyValuePair0, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m3169772347(__this, ___keyValuePair0, method) ((  bool (*) (Dictionary_2_t938533758 *, KeyValuePair_2_t427065056 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m3169772347_gshared)(__this, ___keyValuePair0, method)
// System.Void System.Collections.Generic.Dictionary`2<GameAnalyticsSDK.GA_ServerFieldTypes/FieldType,System.Object>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void Dictionary_2_System_Collections_ICollection_CopyTo_m3549839539_gshared (Dictionary_2_t938533758 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_CopyTo_m3549839539(__this, ___array0, ___index1, method) ((  void (*) (Dictionary_2_t938533758 *, Il2CppArray *, int32_t, const MethodInfo*))Dictionary_2_System_Collections_ICollection_CopyTo_m3549839539_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2<GameAnalyticsSDK.GA_ServerFieldTypes/FieldType,System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m68283650_gshared (Dictionary_2_t938533758 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m68283650(__this, method) ((  Il2CppObject * (*) (Dictionary_2_t938533758 *, const MethodInfo*))Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m68283650_gshared)(__this, method)
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<TKey,TValue>> System.Collections.Generic.Dictionary`2<GameAnalyticsSDK.GA_ServerFieldTypes/FieldType,System.Object>::System.Collections.Generic.IEnumerable<System.Collections.Generic.KeyValuePair<TKey,TValue>>.GetEnumerator()
extern "C"  Il2CppObject* Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m794590713_gshared (Dictionary_2_t938533758 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m794590713(__this, method) ((  Il2CppObject* (*) (Dictionary_2_t938533758 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m794590713_gshared)(__this, method)
// System.Collections.IDictionaryEnumerator System.Collections.Generic.Dictionary`2<GameAnalyticsSDK.GA_ServerFieldTypes/FieldType,System.Object>::System.Collections.IDictionary.GetEnumerator()
extern "C"  Il2CppObject * Dictionary_2_System_Collections_IDictionary_GetEnumerator_m2726530118_gshared (Dictionary_2_t938533758 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_GetEnumerator_m2726530118(__this, method) ((  Il2CppObject * (*) (Dictionary_2_t938533758 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_GetEnumerator_m2726530118_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2<GameAnalyticsSDK.GA_ServerFieldTypes/FieldType,System.Object>::get_Count()
extern "C"  int32_t Dictionary_2_get_Count_m4272651857_gshared (Dictionary_2_t938533758 * __this, const MethodInfo* method);
#define Dictionary_2_get_Count_m4272651857(__this, method) ((  int32_t (*) (Dictionary_2_t938533758 *, const MethodInfo*))Dictionary_2_get_Count_m4272651857_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2<GameAnalyticsSDK.GA_ServerFieldTypes/FieldType,System.Object>::get_Item(TKey)
extern "C"  Il2CppObject * Dictionary_2_get_Item_m1689662998_gshared (Dictionary_2_t938533758 * __this, int32_t ___key0, const MethodInfo* method);
#define Dictionary_2_get_Item_m1689662998(__this, ___key0, method) ((  Il2CppObject * (*) (Dictionary_2_t938533758 *, int32_t, const MethodInfo*))Dictionary_2_get_Item_m1689662998_gshared)(__this, ___key0, method)
// System.Void System.Collections.Generic.Dictionary`2<GameAnalyticsSDK.GA_ServerFieldTypes/FieldType,System.Object>::set_Item(TKey,TValue)
extern "C"  void Dictionary_2_set_Item_m126018993_gshared (Dictionary_2_t938533758 * __this, int32_t ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define Dictionary_2_set_Item_m126018993(__this, ___key0, ___value1, method) ((  void (*) (Dictionary_2_t938533758 *, int32_t, Il2CppObject *, const MethodInfo*))Dictionary_2_set_Item_m126018993_gshared)(__this, ___key0, ___value1, method)
// System.Void System.Collections.Generic.Dictionary`2<GameAnalyticsSDK.GA_ServerFieldTypes/FieldType,System.Object>::Init(System.Int32,System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C"  void Dictionary_2_Init_m4220569897_gshared (Dictionary_2_t938533758 * __this, int32_t ___capacity0, Il2CppObject* ___hcp1, const MethodInfo* method);
#define Dictionary_2_Init_m4220569897(__this, ___capacity0, ___hcp1, method) ((  void (*) (Dictionary_2_t938533758 *, int32_t, Il2CppObject*, const MethodInfo*))Dictionary_2_Init_m4220569897_gshared)(__this, ___capacity0, ___hcp1, method)
// System.Void System.Collections.Generic.Dictionary`2<GameAnalyticsSDK.GA_ServerFieldTypes/FieldType,System.Object>::InitArrays(System.Int32)
extern "C"  void Dictionary_2_InitArrays_m3693517230_gshared (Dictionary_2_t938533758 * __this, int32_t ___size0, const MethodInfo* method);
#define Dictionary_2_InitArrays_m3693517230(__this, ___size0, method) ((  void (*) (Dictionary_2_t938533758 *, int32_t, const MethodInfo*))Dictionary_2_InitArrays_m3693517230_gshared)(__this, ___size0, method)
// System.Void System.Collections.Generic.Dictionary`2<GameAnalyticsSDK.GA_ServerFieldTypes/FieldType,System.Object>::CopyToCheck(System.Array,System.Int32)
extern "C"  void Dictionary_2_CopyToCheck_m2731273514_gshared (Dictionary_2_t938533758 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define Dictionary_2_CopyToCheck_m2731273514(__this, ___array0, ___index1, method) ((  void (*) (Dictionary_2_t938533758 *, Il2CppArray *, int32_t, const MethodInfo*))Dictionary_2_CopyToCheck_m2731273514_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2<GameAnalyticsSDK.GA_ServerFieldTypes/FieldType,System.Object>::make_pair(TKey,TValue)
extern "C"  KeyValuePair_2_t427065056  Dictionary_2_make_pair_m1764014270_gshared (Il2CppObject * __this /* static, unused */, int32_t ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define Dictionary_2_make_pair_m1764014270(__this /* static, unused */, ___key0, ___value1, method) ((  KeyValuePair_2_t427065056  (*) (Il2CppObject * /* static, unused */, int32_t, Il2CppObject *, const MethodInfo*))Dictionary_2_make_pair_m1764014270_gshared)(__this /* static, unused */, ___key0, ___value1, method)
// TKey System.Collections.Generic.Dictionary`2<GameAnalyticsSDK.GA_ServerFieldTypes/FieldType,System.Object>::pick_key(TKey,TValue)
extern "C"  int32_t Dictionary_2_pick_key_m440264000_gshared (Il2CppObject * __this /* static, unused */, int32_t ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define Dictionary_2_pick_key_m440264000(__this /* static, unused */, ___key0, ___value1, method) ((  int32_t (*) (Il2CppObject * /* static, unused */, int32_t, Il2CppObject *, const MethodInfo*))Dictionary_2_pick_key_m440264000_gshared)(__this /* static, unused */, ___key0, ___value1, method)
// TValue System.Collections.Generic.Dictionary`2<GameAnalyticsSDK.GA_ServerFieldTypes/FieldType,System.Object>::pick_value(TKey,TValue)
extern "C"  Il2CppObject * Dictionary_2_pick_value_m470559516_gshared (Il2CppObject * __this /* static, unused */, int32_t ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define Dictionary_2_pick_value_m470559516(__this /* static, unused */, ___key0, ___value1, method) ((  Il2CppObject * (*) (Il2CppObject * /* static, unused */, int32_t, Il2CppObject *, const MethodInfo*))Dictionary_2_pick_value_m470559516_gshared)(__this /* static, unused */, ___key0, ___value1, method)
// System.Void System.Collections.Generic.Dictionary`2<GameAnalyticsSDK.GA_ServerFieldTypes/FieldType,System.Object>::CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
extern "C"  void Dictionary_2_CopyTo_m462867301_gshared (Dictionary_2_t938533758 * __this, KeyValuePair_2U5BU5D_t3853666721* ___array0, int32_t ___index1, const MethodInfo* method);
#define Dictionary_2_CopyTo_m462867301(__this, ___array0, ___index1, method) ((  void (*) (Dictionary_2_t938533758 *, KeyValuePair_2U5BU5D_t3853666721*, int32_t, const MethodInfo*))Dictionary_2_CopyTo_m462867301_gshared)(__this, ___array0, ___index1, method)
// System.Void System.Collections.Generic.Dictionary`2<GameAnalyticsSDK.GA_ServerFieldTypes/FieldType,System.Object>::Resize()
extern "C"  void Dictionary_2_Resize_m695149735_gshared (Dictionary_2_t938533758 * __this, const MethodInfo* method);
#define Dictionary_2_Resize_m695149735(__this, method) ((  void (*) (Dictionary_2_t938533758 *, const MethodInfo*))Dictionary_2_Resize_m695149735_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<GameAnalyticsSDK.GA_ServerFieldTypes/FieldType,System.Object>::Add(TKey,TValue)
extern "C"  void Dictionary_2_Add_m2561970724_gshared (Dictionary_2_t938533758 * __this, int32_t ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define Dictionary_2_Add_m2561970724(__this, ___key0, ___value1, method) ((  void (*) (Dictionary_2_t938533758 *, int32_t, Il2CppObject *, const MethodInfo*))Dictionary_2_Add_m2561970724_gshared)(__this, ___key0, ___value1, method)
// System.Void System.Collections.Generic.Dictionary`2<GameAnalyticsSDK.GA_ServerFieldTypes/FieldType,System.Object>::Clear()
extern "C"  void Dictionary_2_Clear_m2275849692_gshared (Dictionary_2_t938533758 * __this, const MethodInfo* method);
#define Dictionary_2_Clear_m2275849692(__this, method) ((  void (*) (Dictionary_2_t938533758 *, const MethodInfo*))Dictionary_2_Clear_m2275849692_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<GameAnalyticsSDK.GA_ServerFieldTypes/FieldType,System.Object>::ContainsKey(TKey)
extern "C"  bool Dictionary_2_ContainsKey_m433240582_gshared (Dictionary_2_t938533758 * __this, int32_t ___key0, const MethodInfo* method);
#define Dictionary_2_ContainsKey_m433240582(__this, ___key0, method) ((  bool (*) (Dictionary_2_t938533758 *, int32_t, const MethodInfo*))Dictionary_2_ContainsKey_m433240582_gshared)(__this, ___key0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<GameAnalyticsSDK.GA_ServerFieldTypes/FieldType,System.Object>::ContainsValue(TValue)
extern "C"  bool Dictionary_2_ContainsValue_m311794438_gshared (Dictionary_2_t938533758 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Dictionary_2_ContainsValue_m311794438(__this, ___value0, method) ((  bool (*) (Dictionary_2_t938533758 *, Il2CppObject *, const MethodInfo*))Dictionary_2_ContainsValue_m311794438_gshared)(__this, ___value0, method)
// System.Void System.Collections.Generic.Dictionary`2<GameAnalyticsSDK.GA_ServerFieldTypes/FieldType,System.Object>::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C"  void Dictionary_2_GetObjectData_m2270749903_gshared (Dictionary_2_t938533758 * __this, SerializationInfo_t2995724695 * ___info0, StreamingContext_t986364934  ___context1, const MethodInfo* method);
#define Dictionary_2_GetObjectData_m2270749903(__this, ___info0, ___context1, method) ((  void (*) (Dictionary_2_t938533758 *, SerializationInfo_t2995724695 *, StreamingContext_t986364934 , const MethodInfo*))Dictionary_2_GetObjectData_m2270749903_gshared)(__this, ___info0, ___context1, method)
// System.Void System.Collections.Generic.Dictionary`2<GameAnalyticsSDK.GA_ServerFieldTypes/FieldType,System.Object>::OnDeserialization(System.Object)
extern "C"  void Dictionary_2_OnDeserialization_m2113831157_gshared (Dictionary_2_t938533758 * __this, Il2CppObject * ___sender0, const MethodInfo* method);
#define Dictionary_2_OnDeserialization_m2113831157(__this, ___sender0, method) ((  void (*) (Dictionary_2_t938533758 *, Il2CppObject *, const MethodInfo*))Dictionary_2_OnDeserialization_m2113831157_gshared)(__this, ___sender0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<GameAnalyticsSDK.GA_ServerFieldTypes/FieldType,System.Object>::Remove(TKey)
extern "C"  bool Dictionary_2_Remove_m1066849066_gshared (Dictionary_2_t938533758 * __this, int32_t ___key0, const MethodInfo* method);
#define Dictionary_2_Remove_m1066849066(__this, ___key0, method) ((  bool (*) (Dictionary_2_t938533758 *, int32_t, const MethodInfo*))Dictionary_2_Remove_m1066849066_gshared)(__this, ___key0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<GameAnalyticsSDK.GA_ServerFieldTypes/FieldType,System.Object>::TryGetValue(TKey,TValue&)
extern "C"  bool Dictionary_2_TryGetValue_m1772072031_gshared (Dictionary_2_t938533758 * __this, int32_t ___key0, Il2CppObject ** ___value1, const MethodInfo* method);
#define Dictionary_2_TryGetValue_m1772072031(__this, ___key0, ___value1, method) ((  bool (*) (Dictionary_2_t938533758 *, int32_t, Il2CppObject **, const MethodInfo*))Dictionary_2_TryGetValue_m1772072031_gshared)(__this, ___key0, ___value1, method)
// System.Collections.Generic.Dictionary`2/KeyCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<GameAnalyticsSDK.GA_ServerFieldTypes/FieldType,System.Object>::get_Keys()
extern "C"  KeyCollection_t3261809038 * Dictionary_2_get_Keys_m1955631516_gshared (Dictionary_2_t938533758 * __this, const MethodInfo* method);
#define Dictionary_2_get_Keys_m1955631516(__this, method) ((  KeyCollection_t3261809038 * (*) (Dictionary_2_t938533758 *, const MethodInfo*))Dictionary_2_get_Keys_m1955631516_gshared)(__this, method)
// System.Collections.Generic.Dictionary`2/ValueCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<GameAnalyticsSDK.GA_ServerFieldTypes/FieldType,System.Object>::get_Values()
extern "C"  ValueCollection_t2860670852 * Dictionary_2_get_Values_m2533922360_gshared (Dictionary_2_t938533758 * __this, const MethodInfo* method);
#define Dictionary_2_get_Values_m2533922360(__this, method) ((  ValueCollection_t2860670852 * (*) (Dictionary_2_t938533758 *, const MethodInfo*))Dictionary_2_get_Values_m2533922360_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2<GameAnalyticsSDK.GA_ServerFieldTypes/FieldType,System.Object>::ToTKey(System.Object)
extern "C"  int32_t Dictionary_2_ToTKey_m4185090203_gshared (Dictionary_2_t938533758 * __this, Il2CppObject * ___key0, const MethodInfo* method);
#define Dictionary_2_ToTKey_m4185090203(__this, ___key0, method) ((  int32_t (*) (Dictionary_2_t938533758 *, Il2CppObject *, const MethodInfo*))Dictionary_2_ToTKey_m4185090203_gshared)(__this, ___key0, method)
// TValue System.Collections.Generic.Dictionary`2<GameAnalyticsSDK.GA_ServerFieldTypes/FieldType,System.Object>::ToTValue(System.Object)
extern "C"  Il2CppObject * Dictionary_2_ToTValue_m3077840951_gshared (Dictionary_2_t938533758 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Dictionary_2_ToTValue_m3077840951(__this, ___value0, method) ((  Il2CppObject * (*) (Dictionary_2_t938533758 *, Il2CppObject *, const MethodInfo*))Dictionary_2_ToTValue_m3077840951_gshared)(__this, ___value0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<GameAnalyticsSDK.GA_ServerFieldTypes/FieldType,System.Object>::ContainsKeyValuePair(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C"  bool Dictionary_2_ContainsKeyValuePair_m1661579533_gshared (Dictionary_2_t938533758 * __this, KeyValuePair_2_t427065056  ___pair0, const MethodInfo* method);
#define Dictionary_2_ContainsKeyValuePair_m1661579533(__this, ___pair0, method) ((  bool (*) (Dictionary_2_t938533758 *, KeyValuePair_2_t427065056 , const MethodInfo*))Dictionary_2_ContainsKeyValuePair_m1661579533_gshared)(__this, ___pair0, method)
// System.Collections.Generic.Dictionary`2/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2<GameAnalyticsSDK.GA_ServerFieldTypes/FieldType,System.Object>::GetEnumerator()
extern "C"  Enumerator_t705561700  Dictionary_2_GetEnumerator_m2371079420_gshared (Dictionary_2_t938533758 * __this, const MethodInfo* method);
#define Dictionary_2_GetEnumerator_m2371079420(__this, method) ((  Enumerator_t705561700  (*) (Dictionary_2_t938533758 *, const MethodInfo*))Dictionary_2_GetEnumerator_m2371079420_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2<GameAnalyticsSDK.GA_ServerFieldTypes/FieldType,System.Object>::<CopyTo>m__0(TKey,TValue)
extern "C"  DictionaryEntry_t130027246  Dictionary_2_U3CCopyToU3Em__0_m2306743923_gshared (Il2CppObject * __this /* static, unused */, int32_t ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define Dictionary_2_U3CCopyToU3Em__0_m2306743923(__this /* static, unused */, ___key0, ___value1, method) ((  DictionaryEntry_t130027246  (*) (Il2CppObject * /* static, unused */, int32_t, Il2CppObject *, const MethodInfo*))Dictionary_2_U3CCopyToU3Em__0_m2306743923_gshared)(__this /* static, unused */, ___key0, ___value1, method)
