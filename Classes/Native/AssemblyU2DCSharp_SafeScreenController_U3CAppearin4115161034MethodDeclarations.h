﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SafeScreenController/<AppearingCoroutine>c__Iterator1A
struct U3CAppearingCoroutineU3Ec__Iterator1A_t4115161034;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void SafeScreenController/<AppearingCoroutine>c__Iterator1A::.ctor()
extern "C"  void U3CAppearingCoroutineU3Ec__Iterator1A__ctor_m3096059755 (U3CAppearingCoroutineU3Ec__Iterator1A_t4115161034 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object SafeScreenController/<AppearingCoroutine>c__Iterator1A::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CAppearingCoroutineU3Ec__Iterator1A_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1657680273 (U3CAppearingCoroutineU3Ec__Iterator1A_t4115161034 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object SafeScreenController/<AppearingCoroutine>c__Iterator1A::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CAppearingCoroutineU3Ec__Iterator1A_System_Collections_IEnumerator_get_Current_m453998373 (U3CAppearingCoroutineU3Ec__Iterator1A_t4115161034 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean SafeScreenController/<AppearingCoroutine>c__Iterator1A::MoveNext()
extern "C"  bool U3CAppearingCoroutineU3Ec__Iterator1A_MoveNext_m3740814225 (U3CAppearingCoroutineU3Ec__Iterator1A_t4115161034 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SafeScreenController/<AppearingCoroutine>c__Iterator1A::Dispose()
extern "C"  void U3CAppearingCoroutineU3Ec__Iterator1A_Dispose_m3094338792 (U3CAppearingCoroutineU3Ec__Iterator1A_t4115161034 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SafeScreenController/<AppearingCoroutine>c__Iterator1A::Reset()
extern "C"  void U3CAppearingCoroutineU3Ec__Iterator1A_Reset_m742492696 (U3CAppearingCoroutineU3Ec__Iterator1A_t4115161034 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
