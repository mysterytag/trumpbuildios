﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Action
struct Action_t437523947;
// System.Action`1<System.Exception>
struct Action_1_t2115686693;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Stubs
struct  Stubs_t2635868311  : public Il2CppObject
{
public:

public:
};

struct Stubs_t2635868311_StaticFields
{
public:
	// System.Action UniRx.Stubs::Nop
	Action_t437523947 * ___Nop_0;
	// System.Action`1<System.Exception> UniRx.Stubs::Throw
	Action_1_t2115686693 * ___Throw_1;
	// System.Action UniRx.Stubs::<>f__am$cache2
	Action_t437523947 * ___U3CU3Ef__amU24cache2_2;
	// System.Action`1<System.Exception> UniRx.Stubs::<>f__am$cache3
	Action_1_t2115686693 * ___U3CU3Ef__amU24cache3_3;

public:
	inline static int32_t get_offset_of_Nop_0() { return static_cast<int32_t>(offsetof(Stubs_t2635868311_StaticFields, ___Nop_0)); }
	inline Action_t437523947 * get_Nop_0() const { return ___Nop_0; }
	inline Action_t437523947 ** get_address_of_Nop_0() { return &___Nop_0; }
	inline void set_Nop_0(Action_t437523947 * value)
	{
		___Nop_0 = value;
		Il2CppCodeGenWriteBarrier(&___Nop_0, value);
	}

	inline static int32_t get_offset_of_Throw_1() { return static_cast<int32_t>(offsetof(Stubs_t2635868311_StaticFields, ___Throw_1)); }
	inline Action_1_t2115686693 * get_Throw_1() const { return ___Throw_1; }
	inline Action_1_t2115686693 ** get_address_of_Throw_1() { return &___Throw_1; }
	inline void set_Throw_1(Action_1_t2115686693 * value)
	{
		___Throw_1 = value;
		Il2CppCodeGenWriteBarrier(&___Throw_1, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache2_2() { return static_cast<int32_t>(offsetof(Stubs_t2635868311_StaticFields, ___U3CU3Ef__amU24cache2_2)); }
	inline Action_t437523947 * get_U3CU3Ef__amU24cache2_2() const { return ___U3CU3Ef__amU24cache2_2; }
	inline Action_t437523947 ** get_address_of_U3CU3Ef__amU24cache2_2() { return &___U3CU3Ef__amU24cache2_2; }
	inline void set_U3CU3Ef__amU24cache2_2(Action_t437523947 * value)
	{
		___U3CU3Ef__amU24cache2_2 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache2_2, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache3_3() { return static_cast<int32_t>(offsetof(Stubs_t2635868311_StaticFields, ___U3CU3Ef__amU24cache3_3)); }
	inline Action_1_t2115686693 * get_U3CU3Ef__amU24cache3_3() const { return ___U3CU3Ef__amU24cache3_3; }
	inline Action_1_t2115686693 ** get_address_of_U3CU3Ef__amU24cache3_3() { return &___U3CU3Ef__amU24cache3_3; }
	inline void set_U3CU3Ef__amU24cache3_3(Action_1_t2115686693 * value)
	{
		___U3CU3Ef__amU24cache3_3 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache3_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
