﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.OfTypeObservable`2/OfType<System.Object,System.Object>
struct OfType_t384837642;
// UniRx.IObserver`1<System.Object>
struct IObserver_1_t3049105323;
// System.IDisposable
struct IDisposable_t1628921374;
// System.Object
struct Il2CppObject;
// System.Exception
struct Exception_t1967233988;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_Exception1967233988.h"

// System.Void UniRx.Operators.OfTypeObservable`2/OfType<System.Object,System.Object>::.ctor(UniRx.IObserver`1<TResult>,System.IDisposable)
extern "C"  void OfType__ctor_m3081147877_gshared (OfType_t384837642 * __this, Il2CppObject* ___observer0, Il2CppObject * ___cancel1, const MethodInfo* method);
#define OfType__ctor_m3081147877(__this, ___observer0, ___cancel1, method) ((  void (*) (OfType_t384837642 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))OfType__ctor_m3081147877_gshared)(__this, ___observer0, ___cancel1, method)
// System.Void UniRx.Operators.OfTypeObservable`2/OfType<System.Object,System.Object>::OnNext(TSource)
extern "C"  void OfType_OnNext_m2739570037_gshared (OfType_t384837642 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define OfType_OnNext_m2739570037(__this, ___value0, method) ((  void (*) (OfType_t384837642 *, Il2CppObject *, const MethodInfo*))OfType_OnNext_m2739570037_gshared)(__this, ___value0, method)
// System.Void UniRx.Operators.OfTypeObservable`2/OfType<System.Object,System.Object>::OnError(System.Exception)
extern "C"  void OfType_OnError_m2296821407_gshared (OfType_t384837642 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method);
#define OfType_OnError_m2296821407(__this, ___error0, method) ((  void (*) (OfType_t384837642 *, Exception_t1967233988 *, const MethodInfo*))OfType_OnError_m2296821407_gshared)(__this, ___error0, method)
// System.Void UniRx.Operators.OfTypeObservable`2/OfType<System.Object,System.Object>::OnCompleted()
extern "C"  void OfType_OnCompleted_m2576556146_gshared (OfType_t384837642 * __this, const MethodInfo* method);
#define OfType_OnCompleted_m2576556146(__this, method) ((  void (*) (OfType_t384837642 *, const MethodInfo*))OfType_OnCompleted_m2576556146_gshared)(__this, method)
