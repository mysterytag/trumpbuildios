﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Executer
struct Executer_t2107661245;

#include "UnityEngine_UnityEngine_MonoBehaviour3012272455.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TestCoro
struct  TestCoro_t3212325755  : public MonoBehaviour_t3012272455
{
public:
	// Executer TestCoro::testExecuter
	Executer_t2107661245 * ___testExecuter_2;

public:
	inline static int32_t get_offset_of_testExecuter_2() { return static_cast<int32_t>(offsetof(TestCoro_t3212325755, ___testExecuter_2)); }
	inline Executer_t2107661245 * get_testExecuter_2() const { return ___testExecuter_2; }
	inline Executer_t2107661245 ** get_address_of_testExecuter_2() { return &___testExecuter_2; }
	inline void set_testExecuter_2(Executer_t2107661245 * value)
	{
		___testExecuter_2 = value;
		Il2CppCodeGenWriteBarrier(&___testExecuter_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
