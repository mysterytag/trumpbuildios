﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.ClientModels.GetCharacterStatisticsResult
struct GetCharacterStatisticsResult_t2637826531;
// System.Collections.Generic.Dictionary`2<System.String,System.Int32>
struct Dictionary_2_t190145395;

#include "codegen/il2cpp-codegen.h"

// System.Void PlayFab.ClientModels.GetCharacterStatisticsResult::.ctor()
extern "C"  void GetCharacterStatisticsResult__ctor_m2532648634 (GetCharacterStatisticsResult_t2637826531 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.Dictionary`2<System.String,System.Int32> PlayFab.ClientModels.GetCharacterStatisticsResult::get_CharacterStatistics()
extern "C"  Dictionary_2_t190145395 * GetCharacterStatisticsResult_get_CharacterStatistics_m3378762912 (GetCharacterStatisticsResult_t2637826531 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.GetCharacterStatisticsResult::set_CharacterStatistics(System.Collections.Generic.Dictionary`2<System.String,System.Int32>)
extern "C"  void GetCharacterStatisticsResult_set_CharacterStatistics_m1367997661 (GetCharacterStatisticsResult_t2637826531 * __this, Dictionary_2_t190145395 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
