﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "System_Core_System_Func_2_gen2135783352MethodDeclarations.h"

// System.Void System.Func`2<Zenject.InjectContext,Zenject.CompositionRoot>::.ctor(System.Object,System.IntPtr)
#define Func_2__ctor_m3918194172(__this, ___object0, ___method1, method) ((  void (*) (Func_2_t3724067498 *, Il2CppObject *, IntPtr_t, const MethodInfo*))Func_2__ctor_m3944524044_gshared)(__this, ___object0, ___method1, method)
// TResult System.Func`2<Zenject.InjectContext,Zenject.CompositionRoot>::Invoke(T)
#define Func_2_Invoke_m3834346214(__this, ___arg10, method) ((  CompositionRoot_t1939928321 * (*) (Func_2_t3724067498 *, InjectContext_t3456483891 *, const MethodInfo*))Func_2_Invoke_m1924616534_gshared)(__this, ___arg10, method)
// System.IAsyncResult System.Func`2<Zenject.InjectContext,Zenject.CompositionRoot>::BeginInvoke(T,System.AsyncCallback,System.Object)
#define Func_2_BeginInvoke_m1454152085(__this, ___arg10, ___callback1, ___object2, method) ((  Il2CppObject * (*) (Func_2_t3724067498 *, InjectContext_t3456483891 *, AsyncCallback_t1363551830 *, Il2CppObject *, const MethodInfo*))Func_2_BeginInvoke_m4281703301_gshared)(__this, ___arg10, ___callback1, ___object2, method)
// TResult System.Func`2<Zenject.InjectContext,Zenject.CompositionRoot>::EndInvoke(System.IAsyncResult)
#define Func_2_EndInvoke_m3325998254(__this, ___result0, method) ((  CompositionRoot_t1939928321 * (*) (Func_2_t3724067498 *, Il2CppObject *, const MethodInfo*))Func_2_EndInvoke_m4118168638_gshared)(__this, ___result0, method)
