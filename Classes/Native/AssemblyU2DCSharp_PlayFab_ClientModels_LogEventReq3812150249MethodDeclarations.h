﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.ClientModels.LogEventRequest
struct LogEventRequest_t3812150249;
// System.String
struct String_t;
// System.Collections.Generic.Dictionary`2<System.String,System.Object>
struct Dictionary_2_t2474804324;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Nullable_1_gen3225071844.h"
#include "mscorlib_System_String968488902.h"

// System.Void PlayFab.ClientModels.LogEventRequest::.ctor()
extern "C"  void LogEventRequest__ctor_m3950199264 (LogEventRequest_t3812150249 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Nullable`1<System.DateTime> PlayFab.ClientModels.LogEventRequest::get_Timestamp()
extern "C"  Nullable_1_t3225071844  LogEventRequest_get_Timestamp_m1030575069 (LogEventRequest_t3812150249 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.LogEventRequest::set_Timestamp(System.Nullable`1<System.DateTime>)
extern "C"  void LogEventRequest_set_Timestamp_m3131855944 (LogEventRequest_t3812150249 * __this, Nullable_1_t3225071844  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PlayFab.ClientModels.LogEventRequest::get_EventName()
extern "C"  String_t* LogEventRequest_get_EventName_m2885370711 (LogEventRequest_t3812150249 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.LogEventRequest::set_EventName(System.String)
extern "C"  void LogEventRequest_set_EventName_m1245967388 (LogEventRequest_t3812150249 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.Dictionary`2<System.String,System.Object> PlayFab.ClientModels.LogEventRequest::get_Body()
extern "C"  Dictionary_2_t2474804324 * LogEventRequest_get_Body_m1640965547 (LogEventRequest_t3812150249 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.LogEventRequest::set_Body(System.Collections.Generic.Dictionary`2<System.String,System.Object>)
extern "C"  void LogEventRequest_set_Body_m1260650210 (LogEventRequest_t3812150249 * __this, Dictionary_2_t2474804324 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PlayFab.ClientModels.LogEventRequest::get_ProfileSetEvent()
extern "C"  bool LogEventRequest_get_ProfileSetEvent_m3098647618 (LogEventRequest_t3812150249 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.LogEventRequest::set_ProfileSetEvent(System.Boolean)
extern "C"  void LogEventRequest_set_ProfileSetEvent_m811120505 (LogEventRequest_t3812150249 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
