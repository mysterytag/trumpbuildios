﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// TutorialPlayer/<WaitForButton>c__Iterator22/<WaitForButton>c__AnonStoreyED
struct U3CWaitForButtonU3Ec__AnonStoreyED_t1755169010;

#include "codegen/il2cpp-codegen.h"

// System.Void TutorialPlayer/<WaitForButton>c__Iterator22/<WaitForButton>c__AnonStoreyED::.ctor()
extern "C"  void U3CWaitForButtonU3Ec__AnonStoreyED__ctor_m1903061639 (U3CWaitForButtonU3Ec__AnonStoreyED_t1755169010 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TutorialPlayer/<WaitForButton>c__Iterator22/<WaitForButton>c__AnonStoreyED::<>m__151(System.Int64)
extern "C"  void U3CWaitForButtonU3Ec__AnonStoreyED_U3CU3Em__151_m283780593 (U3CWaitForButtonU3Ec__AnonStoreyED_t1755169010 * __this, int64_t ____0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
