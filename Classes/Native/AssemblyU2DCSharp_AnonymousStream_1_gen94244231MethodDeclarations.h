﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// AnonymousStream`1<System.Object>
struct AnonymousStream_1_t94244231;
// System.Func`3<System.Action`1<System.Object>,Priority,System.IDisposable>
struct Func_3_t1585443616;
// System.IDisposable
struct IDisposable_t1628921374;
// System.Action`1<System.Object>
struct Action_1_t985559125;
// System.Action
struct Action_t437523947;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Priority3194150340.h"
#include "System_Core_System_Action437523947.h"

// System.Void AnonymousStream`1<System.Object>::.ctor(System.Func`3<System.Action`1<T>,Priority,System.IDisposable>)
extern "C"  void AnonymousStream_1__ctor_m1486310211_gshared (AnonymousStream_1_t94244231 * __this, Func_3_t1585443616 * ___subscribe0, const MethodInfo* method);
#define AnonymousStream_1__ctor_m1486310211(__this, ___subscribe0, method) ((  void (*) (AnonymousStream_1_t94244231 *, Func_3_t1585443616 *, const MethodInfo*))AnonymousStream_1__ctor_m1486310211_gshared)(__this, ___subscribe0, method)
// System.IDisposable AnonymousStream`1<System.Object>::Listen(System.Action`1<T>,Priority)
extern "C"  Il2CppObject * AnonymousStream_1_Listen_m191877793_gshared (AnonymousStream_1_t94244231 * __this, Action_1_t985559125 * ___observer0, int32_t ___p1, const MethodInfo* method);
#define AnonymousStream_1_Listen_m191877793(__this, ___observer0, ___p1, method) ((  Il2CppObject * (*) (AnonymousStream_1_t94244231 *, Action_1_t985559125 *, int32_t, const MethodInfo*))AnonymousStream_1_Listen_m191877793_gshared)(__this, ___observer0, ___p1, method)
// System.IDisposable AnonymousStream`1<System.Object>::Listen(System.Action,Priority)
extern "C"  Il2CppObject * AnonymousStream_1_Listen_m3450752486_gshared (AnonymousStream_1_t94244231 * __this, Action_t437523947 * ___observer0, int32_t ___p1, const MethodInfo* method);
#define AnonymousStream_1_Listen_m3450752486(__this, ___observer0, ___p1, method) ((  Il2CppObject * (*) (AnonymousStream_1_t94244231 *, Action_t437523947 *, int32_t, const MethodInfo*))AnonymousStream_1_Listen_m3450752486_gshared)(__this, ___observer0, ___p1, method)
