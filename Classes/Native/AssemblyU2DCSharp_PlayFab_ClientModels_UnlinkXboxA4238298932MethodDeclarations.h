﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.ClientModels.UnlinkXboxAccountResult
struct UnlinkXboxAccountResult_t4238298932;

#include "codegen/il2cpp-codegen.h"

// System.Void PlayFab.ClientModels.UnlinkXboxAccountResult::.ctor()
extern "C"  void UnlinkXboxAccountResult__ctor_m949013749 (UnlinkXboxAccountResult_t4238298932 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
