﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1634065389MethodDeclarations.h"

// System.Void System.Collections.Generic.List`1<PlayFab.ClientModels.CatalogItem>::.ctor()
#define List_1__ctor_m1452928290(__this, method) ((  void (*) (List_1_t634580917 *, const MethodInfo*))List_1__ctor_m348833073_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<PlayFab.ClientModels.CatalogItem>::.ctor(System.Collections.Generic.IEnumerable`1<T>)
#define List_1__ctor_m1862262301(__this, ___collection0, method) ((  void (*) (List_1_t634580917 *, Il2CppObject*, const MethodInfo*))List_1__ctor_m1160795371_gshared)(__this, ___collection0, method)
// System.Void System.Collections.Generic.List`1<PlayFab.ClientModels.CatalogItem>::.ctor(System.Int32)
#define List_1__ctor_m1242759027(__this, ___capacity0, method) ((  void (*) (List_1_t634580917 *, int32_t, const MethodInfo*))List_1__ctor_m2892763214_gshared)(__this, ___capacity0, method)
// System.Void System.Collections.Generic.List`1<PlayFab.ClientModels.CatalogItem>::.cctor()
#define List_1__cctor_m1609007819(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))List_1__cctor_m3826137881_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.List`1<PlayFab.ClientModels.CatalogItem>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
#define List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m615360684(__this, method) ((  Il2CppObject* (*) (List_1_t634580917 *, const MethodInfo*))List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m2808422246_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<PlayFab.ClientModels.CatalogItem>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define List_1_System_Collections_ICollection_CopyTo_m1018749538(__this, ___array0, ___arrayIndex1, method) ((  void (*) (List_1_t634580917 *, Il2CppArray *, int32_t, const MethodInfo*))List_1_System_Collections_ICollection_CopyTo_m4034025648_gshared)(__this, ___array0, ___arrayIndex1, method)
// System.Collections.IEnumerator System.Collections.Generic.List`1<PlayFab.ClientModels.CatalogItem>::System.Collections.IEnumerable.GetEnumerator()
#define List_1_System_Collections_IEnumerable_GetEnumerator_m3489093361(__this, method) ((  Il2CppObject * (*) (List_1_t634580917 *, const MethodInfo*))List_1_System_Collections_IEnumerable_GetEnumerator_m1841330603_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<PlayFab.ClientModels.CatalogItem>::System.Collections.IList.Add(System.Object)
#define List_1_System_Collections_IList_Add_m3925990636(__this, ___item0, method) ((  int32_t (*) (List_1_t634580917 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Add_m3794749222_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.List`1<PlayFab.ClientModels.CatalogItem>::System.Collections.IList.Contains(System.Object)
#define List_1_System_Collections_IList_Contains_m1767962068(__this, ___item0, method) ((  bool (*) (List_1_t634580917 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Contains_m2659633254_gshared)(__this, ___item0, method)
// System.Int32 System.Collections.Generic.List`1<PlayFab.ClientModels.CatalogItem>::System.Collections.IList.IndexOf(System.Object)
#define List_1_System_Collections_IList_IndexOf_m3656498500(__this, ___item0, method) ((  int32_t (*) (List_1_t634580917 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_IndexOf_m3431692926_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<PlayFab.ClientModels.CatalogItem>::System.Collections.IList.Insert(System.Int32,System.Object)
#define List_1_System_Collections_IList_Insert_m3996036279(__this, ___index0, ___item1, method) ((  void (*) (List_1_t634580917 *, int32_t, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Insert_m2067529129_gshared)(__this, ___index0, ___item1, method)
// System.Void System.Collections.Generic.List`1<PlayFab.ClientModels.CatalogItem>::System.Collections.IList.Remove(System.Object)
#define List_1_System_Collections_IList_Remove_m978820881(__this, ___item0, method) ((  void (*) (List_1_t634580917 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Remove_m1644145887_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.List`1<PlayFab.ClientModels.CatalogItem>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
#define List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m4253199573(__this, method) ((  bool (*) (List_1_t634580917 *, const MethodInfo*))List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m1299706087_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<PlayFab.ClientModels.CatalogItem>::System.Collections.ICollection.get_IsSynchronized()
#define List_1_System_Collections_ICollection_get_IsSynchronized_m168993032(__this, method) ((  bool (*) (List_1_t634580917 *, const MethodInfo*))List_1_System_Collections_ICollection_get_IsSynchronized_m3867536694_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<PlayFab.ClientModels.CatalogItem>::System.Collections.ICollection.get_SyncRoot()
#define List_1_System_Collections_ICollection_get_SyncRoot_m145266554(__this, method) ((  Il2CppObject * (*) (List_1_t634580917 *, const MethodInfo*))List_1_System_Collections_ICollection_get_SyncRoot_m4244374434_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<PlayFab.ClientModels.CatalogItem>::System.Collections.IList.get_IsFixedSize()
#define List_1_System_Collections_IList_get_IsFixedSize_m3917124675(__this, method) ((  bool (*) (List_1_t634580917 *, const MethodInfo*))List_1_System_Collections_IList_get_IsFixedSize_m432946261_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<PlayFab.ClientModels.CatalogItem>::System.Collections.IList.get_IsReadOnly()
#define List_1_System_Collections_IList_get_IsReadOnly_m857462358(__this, method) ((  bool (*) (List_1_t634580917 *, const MethodInfo*))List_1_System_Collections_IList_get_IsReadOnly_m2961826820_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<PlayFab.ClientModels.CatalogItem>::System.Collections.IList.get_Item(System.Int32)
#define List_1_System_Collections_IList_get_Item_m3227816577(__this, ___index0, method) ((  Il2CppObject * (*) (List_1_t634580917 *, int32_t, const MethodInfo*))List_1_System_Collections_IList_get_Item_m3985478825_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<PlayFab.ClientModels.CatalogItem>::System.Collections.IList.set_Item(System.Int32,System.Object)
#define List_1_System_Collections_IList_set_Item_m1104053966(__this, ___index0, ___value1, method) ((  void (*) (List_1_t634580917 *, int32_t, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_set_Item_m3234554688_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.Generic.List`1<PlayFab.ClientModels.CatalogItem>::Add(T)
#define List_1_Add_m2530862877(__this, ___item0, method) ((  void (*) (List_1_t634580917 *, CatalogItem_t4132589244 *, const MethodInfo*))List_1_Add_m642669291_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<PlayFab.ClientModels.CatalogItem>::GrowIfNeeded(System.Int32)
#define List_1_GrowIfNeeded_m3471271128(__this, ___newCount0, method) ((  void (*) (List_1_t634580917 *, int32_t, const MethodInfo*))List_1_GrowIfNeeded_m4122600870_gshared)(__this, ___newCount0, method)
// System.Void System.Collections.Generic.List`1<PlayFab.ClientModels.CatalogItem>::AddCollection(System.Collections.Generic.ICollection`1<T>)
#define List_1_AddCollection_m323902422(__this, ___collection0, method) ((  void (*) (List_1_t634580917 *, Il2CppObject*, const MethodInfo*))List_1_AddCollection_m2478449828_gshared)(__this, ___collection0, method)
// System.Void System.Collections.Generic.List`1<PlayFab.ClientModels.CatalogItem>::AddEnumerable(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_AddEnumerable_m3879841942(__this, ___enumerable0, method) ((  void (*) (List_1_t634580917 *, Il2CppObject*, const MethodInfo*))List_1_AddEnumerable_m1739422052_gshared)(__this, ___enumerable0, method)
// System.Void System.Collections.Generic.List`1<PlayFab.ClientModels.CatalogItem>::AddRange(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_AddRange_m319600705(__this, ___collection0, method) ((  void (*) (List_1_t634580917 *, Il2CppObject*, const MethodInfo*))List_1_AddRange_m2229151411_gshared)(__this, ___collection0, method)
// System.Collections.ObjectModel.ReadOnlyCollection`1<T> System.Collections.Generic.List`1<PlayFab.ClientModels.CatalogItem>::AsReadOnly()
#define List_1_AsReadOnly_m2609354660(__this, method) ((  ReadOnlyCollection_1_t3000767296 * (*) (List_1_t634580917 *, const MethodInfo*))List_1_AsReadOnly_m769820182_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<PlayFab.ClientModels.CatalogItem>::Clear()
#define List_1_Clear_m3154028877(__this, method) ((  void (*) (List_1_t634580917 *, const MethodInfo*))List_1_Clear_m454602559_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<PlayFab.ClientModels.CatalogItem>::Contains(T)
#define List_1_Contains_m4049412799(__this, ___item0, method) ((  bool (*) (List_1_t634580917 *, CatalogItem_t4132589244 *, const MethodInfo*))List_1_Contains_m4186092781_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<PlayFab.ClientModels.CatalogItem>::CopyTo(T[],System.Int32)
#define List_1_CopyTo_m3393266125(__this, ___array0, ___arrayIndex1, method) ((  void (*) (List_1_t634580917 *, CatalogItemU5BU5D_t1104433877*, int32_t, const MethodInfo*))List_1_CopyTo_m3988356635_gshared)(__this, ___array0, ___arrayIndex1, method)
// T System.Collections.Generic.List`1<PlayFab.ClientModels.CatalogItem>::Find(System.Predicate`1<T>)
#define List_1_Find_m1572008473(__this, ___match0, method) ((  CatalogItem_t4132589244 * (*) (List_1_t634580917 *, Predicate_1_t408585846 *, const MethodInfo*))List_1_Find_m3379773421_gshared)(__this, ___match0, method)
// System.Void System.Collections.Generic.List`1<PlayFab.ClientModels.CatalogItem>::CheckMatch(System.Predicate`1<T>)
#define List_1_CheckMatch_m3011883958(__this /* static, unused */, ___match0, method) ((  void (*) (Il2CppObject * /* static, unused */, Predicate_1_t408585846 *, const MethodInfo*))List_1_CheckMatch_m3390394152_gshared)(__this /* static, unused */, ___match0, method)
// System.Int32 System.Collections.Generic.List`1<PlayFab.ClientModels.CatalogItem>::GetIndex(System.Int32,System.Int32,System.Predicate`1<T>)
#define List_1_GetIndex_m599550355(__this, ___startIndex0, ___count1, ___match2, method) ((  int32_t (*) (List_1_t634580917 *, int32_t, int32_t, Predicate_1_t408585846 *, const MethodInfo*))List_1_GetIndex_m4275988045_gshared)(__this, ___startIndex0, ___count1, ___match2, method)
// System.Void System.Collections.Generic.List`1<PlayFab.ClientModels.CatalogItem>::ForEach(System.Action`1<T>)
#define List_1_ForEach_m3313962922(__this, ___action0, method) ((  void (*) (List_1_t634580917 *, Action_1_t4281041949 *, const MethodInfo*))List_1_ForEach_m2030348444_gshared)(__this, ___action0, method)
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<PlayFab.ClientModels.CatalogItem>::GetEnumerator()
#define List_1_GetEnumerator_m1663134844(__this, method) ((  Enumerator_t3015331205  (*) (List_1_t634580917 *, const MethodInfo*))List_1_GetEnumerator_m1820263692_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<PlayFab.ClientModels.CatalogItem>::IndexOf(T)
#define List_1_IndexOf_m2888239193(__this, ___item0, method) ((  int32_t (*) (List_1_t634580917 *, CatalogItem_t4132589244 *, const MethodInfo*))List_1_IndexOf_m1752303327_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<PlayFab.ClientModels.CatalogItem>::Shift(System.Int32,System.Int32)
#define List_1_Shift_m4071938852(__this, ___start0, ___delta1, method) ((  void (*) (List_1_t634580917 *, int32_t, int32_t, const MethodInfo*))List_1_Shift_m3807054194_gshared)(__this, ___start0, ___delta1, method)
// System.Void System.Collections.Generic.List`1<PlayFab.ClientModels.CatalogItem>::CheckIndex(System.Int32)
#define List_1_CheckIndex_m3139633309(__this, ___index0, method) ((  void (*) (List_1_t634580917 *, int32_t, const MethodInfo*))List_1_CheckIndex_m3734723819_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<PlayFab.ClientModels.CatalogItem>::Insert(System.Int32,T)
#define List_1_Insert_m1835485060(__this, ___index0, ___item1, method) ((  void (*) (List_1_t634580917 *, int32_t, CatalogItem_t4132589244 *, const MethodInfo*))List_1_Insert_m3427163986_gshared)(__this, ___index0, ___item1, method)
// System.Void System.Collections.Generic.List`1<PlayFab.ClientModels.CatalogItem>::CheckCollection(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_CheckCollection_m2559250681(__this, ___collection0, method) ((  void (*) (List_1_t634580917 *, Il2CppObject*, const MethodInfo*))List_1_CheckCollection_m2905071175_gshared)(__this, ___collection0, method)
// System.Boolean System.Collections.Generic.List`1<PlayFab.ClientModels.CatalogItem>::Remove(T)
#define List_1_Remove_m1420396154(__this, ___item0, method) ((  bool (*) (List_1_t634580917 *, CatalogItem_t4132589244 *, const MethodInfo*))List_1_Remove_m2747911208_gshared)(__this, ___item0, method)
// System.Int32 System.Collections.Generic.List`1<PlayFab.ClientModels.CatalogItem>::RemoveAll(System.Predicate`1<T>)
#define List_1_RemoveAll_m2584352604(__this, ___match0, method) ((  int32_t (*) (List_1_t634580917 *, Predicate_1_t408585846 *, const MethodInfo*))List_1_RemoveAll_m2933443938_gshared)(__this, ___match0, method)
// System.Void System.Collections.Generic.List`1<PlayFab.ClientModels.CatalogItem>::RemoveAt(System.Int32)
#define List_1_RemoveAt_m4004305226(__this, ___index0, method) ((  void (*) (List_1_t634580917 *, int32_t, const MethodInfo*))List_1_RemoveAt_m1301016856_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<PlayFab.ClientModels.CatalogItem>::Reverse()
#define List_1_Reverse_m437526754(__this, method) ((  void (*) (List_1_t634580917 *, const MethodInfo*))List_1_Reverse_m449081940_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<PlayFab.ClientModels.CatalogItem>::Sort()
#define List_1_Sort_m3056835072(__this, method) ((  void (*) (List_1_t634580917 *, const MethodInfo*))List_1_Sort_m1168641486_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<PlayFab.ClientModels.CatalogItem>::Sort(System.Collections.Generic.IComparer`1<T>)
#define List_1_Sort_m281471972(__this, ___comparer0, method) ((  void (*) (List_1_t634580917 *, Il2CppObject*, const MethodInfo*))List_1_Sort_m3726677974_gshared)(__this, ___comparer0, method)
// System.Void System.Collections.Generic.List`1<PlayFab.ClientModels.CatalogItem>::Sort(System.Comparison`1<T>)
#define List_1_Sort_m1034561107(__this, ___comparison0, method) ((  void (*) (List_1_t634580917 *, Comparison_1_t2541296824 *, const MethodInfo*))List_1_Sort_m4192185249_gshared)(__this, ___comparison0, method)
// T[] System.Collections.Generic.List`1<PlayFab.ClientModels.CatalogItem>::ToArray()
#define List_1_ToArray_m4206479483(__this, method) ((  CatalogItemU5BU5D_t1104433877* (*) (List_1_t634580917 *, const MethodInfo*))List_1_ToArray_m1046025517_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<PlayFab.ClientModels.CatalogItem>::TrimExcess()
#define List_1_TrimExcess_m1808218521(__this, method) ((  void (*) (List_1_t634580917 *, const MethodInfo*))List_1_TrimExcess_m2451380967_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<PlayFab.ClientModels.CatalogItem>::get_Capacity()
#define List_1_get_Capacity_m2523474953(__this, method) ((  int32_t (*) (List_1_t634580917 *, const MethodInfo*))List_1_get_Capacity_m543520655_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<PlayFab.ClientModels.CatalogItem>::set_Capacity(System.Int32)
#define List_1_set_Capacity_m681459946(__this, ___value0, method) ((  void (*) (List_1_t634580917 *, int32_t, const MethodInfo*))List_1_set_Capacity_m1332789688_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.Generic.List`1<PlayFab.ClientModels.CatalogItem>::get_Count()
#define List_1_get_Count_m3453376578(__this, method) ((  int32_t (*) (List_1_t634580917 *, const MethodInfo*))List_1_get_Count_m2599103100_gshared)(__this, method)
// T System.Collections.Generic.List`1<PlayFab.ClientModels.CatalogItem>::get_Item(System.Int32)
#define List_1_get_Item_m4242332912(__this, ___index0, method) ((  CatalogItem_t4132589244 * (*) (List_1_t634580917 *, int32_t, const MethodInfo*))List_1_get_Item_m2771401372_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<PlayFab.ClientModels.CatalogItem>::set_Item(System.Int32,T)
#define List_1_set_Item_m479180635(__this, ___index0, ___value1, method) ((  void (*) (List_1_t634580917 *, int32_t, CatalogItem_t4132589244 *, const MethodInfo*))List_1_set_Item_m1074271145_gshared)(__this, ___index0, ___value1, method)
