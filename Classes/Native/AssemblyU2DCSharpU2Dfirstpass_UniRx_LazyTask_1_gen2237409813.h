﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UniRx.IObservable`1<System.Object>
struct IObservable_1_t595904784;
// System.Object
struct Il2CppObject;
// System.Exception
struct Exception_t1967233988;

#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_LazyTask1365889643.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.LazyTask`1<System.Object>
struct  LazyTask_1_t2237409813  : public LazyTask_t1365889643
{
public:
	// UniRx.IObservable`1<T> UniRx.LazyTask`1::source
	Il2CppObject* ___source_3;
	// T UniRx.LazyTask`1::result
	Il2CppObject * ___result_4;
	// System.Exception UniRx.LazyTask`1::<Exception>k__BackingField
	Exception_t1967233988 * ___U3CExceptionU3Ek__BackingField_5;

public:
	inline static int32_t get_offset_of_source_3() { return static_cast<int32_t>(offsetof(LazyTask_1_t2237409813, ___source_3)); }
	inline Il2CppObject* get_source_3() const { return ___source_3; }
	inline Il2CppObject** get_address_of_source_3() { return &___source_3; }
	inline void set_source_3(Il2CppObject* value)
	{
		___source_3 = value;
		Il2CppCodeGenWriteBarrier(&___source_3, value);
	}

	inline static int32_t get_offset_of_result_4() { return static_cast<int32_t>(offsetof(LazyTask_1_t2237409813, ___result_4)); }
	inline Il2CppObject * get_result_4() const { return ___result_4; }
	inline Il2CppObject ** get_address_of_result_4() { return &___result_4; }
	inline void set_result_4(Il2CppObject * value)
	{
		___result_4 = value;
		Il2CppCodeGenWriteBarrier(&___result_4, value);
	}

	inline static int32_t get_offset_of_U3CExceptionU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(LazyTask_1_t2237409813, ___U3CExceptionU3Ek__BackingField_5)); }
	inline Exception_t1967233988 * get_U3CExceptionU3Ek__BackingField_5() const { return ___U3CExceptionU3Ek__BackingField_5; }
	inline Exception_t1967233988 ** get_address_of_U3CExceptionU3Ek__BackingField_5() { return &___U3CExceptionU3Ek__BackingField_5; }
	inline void set_U3CExceptionU3Ek__BackingField_5(Exception_t1967233988 * value)
	{
		___U3CExceptionU3Ek__BackingField_5 = value;
		Il2CppCodeGenWriteBarrier(&___U3CExceptionU3Ek__BackingField_5, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
