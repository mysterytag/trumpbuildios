﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UniRx.Operators.MergeObservable`1<UniRx.Unit>
struct MergeObservable_1_t1064204364;
// UniRx.CompositeDisposable
struct CompositeDisposable_t1894629977;
// UniRx.SingleAssignmentDisposable
struct SingleAssignmentDisposable_t2336378823;
// System.Object
struct Il2CppObject;

#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Oper2064311169.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Operators.MergeObservable`1/MergeOuterObserver<UniRx.Unit>
struct  MergeOuterObserver_t3334261548  : public OperatorObserverBase_2_t2064311169
{
public:
	// UniRx.Operators.MergeObservable`1<T> UniRx.Operators.MergeObservable`1/MergeOuterObserver::parent
	MergeObservable_1_t1064204364 * ___parent_2;
	// UniRx.CompositeDisposable UniRx.Operators.MergeObservable`1/MergeOuterObserver::collectionDisposable
	CompositeDisposable_t1894629977 * ___collectionDisposable_3;
	// UniRx.SingleAssignmentDisposable UniRx.Operators.MergeObservable`1/MergeOuterObserver::sourceDisposable
	SingleAssignmentDisposable_t2336378823 * ___sourceDisposable_4;
	// System.Object UniRx.Operators.MergeObservable`1/MergeOuterObserver::gate
	Il2CppObject * ___gate_5;
	// System.Boolean UniRx.Operators.MergeObservable`1/MergeOuterObserver::isStopped
	bool ___isStopped_6;

public:
	inline static int32_t get_offset_of_parent_2() { return static_cast<int32_t>(offsetof(MergeOuterObserver_t3334261548, ___parent_2)); }
	inline MergeObservable_1_t1064204364 * get_parent_2() const { return ___parent_2; }
	inline MergeObservable_1_t1064204364 ** get_address_of_parent_2() { return &___parent_2; }
	inline void set_parent_2(MergeObservable_1_t1064204364 * value)
	{
		___parent_2 = value;
		Il2CppCodeGenWriteBarrier(&___parent_2, value);
	}

	inline static int32_t get_offset_of_collectionDisposable_3() { return static_cast<int32_t>(offsetof(MergeOuterObserver_t3334261548, ___collectionDisposable_3)); }
	inline CompositeDisposable_t1894629977 * get_collectionDisposable_3() const { return ___collectionDisposable_3; }
	inline CompositeDisposable_t1894629977 ** get_address_of_collectionDisposable_3() { return &___collectionDisposable_3; }
	inline void set_collectionDisposable_3(CompositeDisposable_t1894629977 * value)
	{
		___collectionDisposable_3 = value;
		Il2CppCodeGenWriteBarrier(&___collectionDisposable_3, value);
	}

	inline static int32_t get_offset_of_sourceDisposable_4() { return static_cast<int32_t>(offsetof(MergeOuterObserver_t3334261548, ___sourceDisposable_4)); }
	inline SingleAssignmentDisposable_t2336378823 * get_sourceDisposable_4() const { return ___sourceDisposable_4; }
	inline SingleAssignmentDisposable_t2336378823 ** get_address_of_sourceDisposable_4() { return &___sourceDisposable_4; }
	inline void set_sourceDisposable_4(SingleAssignmentDisposable_t2336378823 * value)
	{
		___sourceDisposable_4 = value;
		Il2CppCodeGenWriteBarrier(&___sourceDisposable_4, value);
	}

	inline static int32_t get_offset_of_gate_5() { return static_cast<int32_t>(offsetof(MergeOuterObserver_t3334261548, ___gate_5)); }
	inline Il2CppObject * get_gate_5() const { return ___gate_5; }
	inline Il2CppObject ** get_address_of_gate_5() { return &___gate_5; }
	inline void set_gate_5(Il2CppObject * value)
	{
		___gate_5 = value;
		Il2CppCodeGenWriteBarrier(&___gate_5, value);
	}

	inline static int32_t get_offset_of_isStopped_6() { return static_cast<int32_t>(offsetof(MergeOuterObserver_t3334261548, ___isStopped_6)); }
	inline bool get_isStopped_6() const { return ___isStopped_6; }
	inline bool* get_address_of_isStopped_6() { return &___isStopped_6; }
	inline void set_isStopped_6(bool value)
	{
		___isStopped_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
