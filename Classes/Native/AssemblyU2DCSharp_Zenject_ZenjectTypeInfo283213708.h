﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Generic.List`1<Zenject.PostInjectableInfo>
struct List_1_t3877242631;
// System.Collections.Generic.List`1<Zenject.InjectableInfo>
struct List_1_t1944668743;
// System.Reflection.ConstructorInfo
struct ConstructorInfo_t3542137334;
// System.Type
struct Type_t;
// System.Func`2<Zenject.PostInjectableInfo,System.Collections.Generic.IEnumerable`1<Zenject.InjectableInfo>>
struct Func_2_t2429407812;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.ZenjectTypeInfo
struct  ZenjectTypeInfo_t283213708  : public Il2CppObject
{
public:
	// System.Collections.Generic.List`1<Zenject.PostInjectableInfo> Zenject.ZenjectTypeInfo::_postInjectMethods
	List_1_t3877242631 * ____postInjectMethods_0;
	// System.Collections.Generic.List`1<Zenject.InjectableInfo> Zenject.ZenjectTypeInfo::_constructorInjectables
	List_1_t1944668743 * ____constructorInjectables_1;
	// System.Collections.Generic.List`1<Zenject.InjectableInfo> Zenject.ZenjectTypeInfo::_fieldInjectables
	List_1_t1944668743 * ____fieldInjectables_2;
	// System.Collections.Generic.List`1<Zenject.InjectableInfo> Zenject.ZenjectTypeInfo::_propertyInjectables
	List_1_t1944668743 * ____propertyInjectables_3;
	// System.Reflection.ConstructorInfo Zenject.ZenjectTypeInfo::_injectConstructor
	ConstructorInfo_t3542137334 * ____injectConstructor_4;
	// System.Type Zenject.ZenjectTypeInfo::_typeAnalyzed
	Type_t * ____typeAnalyzed_5;

public:
	inline static int32_t get_offset_of__postInjectMethods_0() { return static_cast<int32_t>(offsetof(ZenjectTypeInfo_t283213708, ____postInjectMethods_0)); }
	inline List_1_t3877242631 * get__postInjectMethods_0() const { return ____postInjectMethods_0; }
	inline List_1_t3877242631 ** get_address_of__postInjectMethods_0() { return &____postInjectMethods_0; }
	inline void set__postInjectMethods_0(List_1_t3877242631 * value)
	{
		____postInjectMethods_0 = value;
		Il2CppCodeGenWriteBarrier(&____postInjectMethods_0, value);
	}

	inline static int32_t get_offset_of__constructorInjectables_1() { return static_cast<int32_t>(offsetof(ZenjectTypeInfo_t283213708, ____constructorInjectables_1)); }
	inline List_1_t1944668743 * get__constructorInjectables_1() const { return ____constructorInjectables_1; }
	inline List_1_t1944668743 ** get_address_of__constructorInjectables_1() { return &____constructorInjectables_1; }
	inline void set__constructorInjectables_1(List_1_t1944668743 * value)
	{
		____constructorInjectables_1 = value;
		Il2CppCodeGenWriteBarrier(&____constructorInjectables_1, value);
	}

	inline static int32_t get_offset_of__fieldInjectables_2() { return static_cast<int32_t>(offsetof(ZenjectTypeInfo_t283213708, ____fieldInjectables_2)); }
	inline List_1_t1944668743 * get__fieldInjectables_2() const { return ____fieldInjectables_2; }
	inline List_1_t1944668743 ** get_address_of__fieldInjectables_2() { return &____fieldInjectables_2; }
	inline void set__fieldInjectables_2(List_1_t1944668743 * value)
	{
		____fieldInjectables_2 = value;
		Il2CppCodeGenWriteBarrier(&____fieldInjectables_2, value);
	}

	inline static int32_t get_offset_of__propertyInjectables_3() { return static_cast<int32_t>(offsetof(ZenjectTypeInfo_t283213708, ____propertyInjectables_3)); }
	inline List_1_t1944668743 * get__propertyInjectables_3() const { return ____propertyInjectables_3; }
	inline List_1_t1944668743 ** get_address_of__propertyInjectables_3() { return &____propertyInjectables_3; }
	inline void set__propertyInjectables_3(List_1_t1944668743 * value)
	{
		____propertyInjectables_3 = value;
		Il2CppCodeGenWriteBarrier(&____propertyInjectables_3, value);
	}

	inline static int32_t get_offset_of__injectConstructor_4() { return static_cast<int32_t>(offsetof(ZenjectTypeInfo_t283213708, ____injectConstructor_4)); }
	inline ConstructorInfo_t3542137334 * get__injectConstructor_4() const { return ____injectConstructor_4; }
	inline ConstructorInfo_t3542137334 ** get_address_of__injectConstructor_4() { return &____injectConstructor_4; }
	inline void set__injectConstructor_4(ConstructorInfo_t3542137334 * value)
	{
		____injectConstructor_4 = value;
		Il2CppCodeGenWriteBarrier(&____injectConstructor_4, value);
	}

	inline static int32_t get_offset_of__typeAnalyzed_5() { return static_cast<int32_t>(offsetof(ZenjectTypeInfo_t283213708, ____typeAnalyzed_5)); }
	inline Type_t * get__typeAnalyzed_5() const { return ____typeAnalyzed_5; }
	inline Type_t ** get_address_of__typeAnalyzed_5() { return &____typeAnalyzed_5; }
	inline void set__typeAnalyzed_5(Type_t * value)
	{
		____typeAnalyzed_5 = value;
		Il2CppCodeGenWriteBarrier(&____typeAnalyzed_5, value);
	}
};

struct ZenjectTypeInfo_t283213708_StaticFields
{
public:
	// System.Func`2<Zenject.PostInjectableInfo,System.Collections.Generic.IEnumerable`1<Zenject.InjectableInfo>> Zenject.ZenjectTypeInfo::<>f__am$cache6
	Func_2_t2429407812 * ___U3CU3Ef__amU24cache6_6;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache6_6() { return static_cast<int32_t>(offsetof(ZenjectTypeInfo_t283213708_StaticFields, ___U3CU3Ef__amU24cache6_6)); }
	inline Func_2_t2429407812 * get_U3CU3Ef__amU24cache6_6() const { return ___U3CU3Ef__amU24cache6_6; }
	inline Func_2_t2429407812 ** get_address_of_U3CU3Ef__amU24cache6_6() { return &___U3CU3Ef__amU24cache6_6; }
	inline void set_U3CU3Ef__amU24cache6_6(Func_2_t2429407812 * value)
	{
		___U3CU3Ef__amU24cache6_6 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache6_6, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
