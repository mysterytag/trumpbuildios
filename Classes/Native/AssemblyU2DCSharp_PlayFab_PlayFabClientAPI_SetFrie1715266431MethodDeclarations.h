﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.PlayFabClientAPI/SetFriendTagsResponseCallback
struct SetFriendTagsResponseCallback_t1715266431;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;
// PlayFab.ClientModels.SetFriendTagsRequest
struct SetFriendTagsRequest_t1987017382;
// PlayFab.ClientModels.SetFriendTagsResult
struct SetFriendTagsResult_t4117117766;
// PlayFab.PlayFabError
struct PlayFabError_t750598646;
// System.IAsyncResult
struct IAsyncResult_t537683269;
// System.AsyncCallback
struct AsyncCallback_t1363551830;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_IntPtr676692020.h"
#include "mscorlib_System_String968488902.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_SetFriendTa1987017382.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_SetFriendTa4117117766.h"
#include "AssemblyU2DCSharpU2Dfirstpass_PlayFab_PlayFabError750598646.h"
#include "mscorlib_System_AsyncCallback1363551830.h"

// System.Void PlayFab.PlayFabClientAPI/SetFriendTagsResponseCallback::.ctor(System.Object,System.IntPtr)
extern "C"  void SetFriendTagsResponseCallback__ctor_m2880786574 (SetFriendTagsResponseCallback_t1715266431 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/SetFriendTagsResponseCallback::Invoke(System.String,System.Int32,PlayFab.ClientModels.SetFriendTagsRequest,PlayFab.ClientModels.SetFriendTagsResult,PlayFab.PlayFabError,System.Object)
extern "C"  void SetFriendTagsResponseCallback_Invoke_m485302507 (SetFriendTagsResponseCallback_t1715266431 * __this, String_t* ___urlPath0, int32_t ___callId1, SetFriendTagsRequest_t1987017382 * ___request2, SetFriendTagsResult_t4117117766 * ___result3, PlayFabError_t750598646 * ___error4, Il2CppObject * ___customData5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
extern "C" void pinvoke_delegate_wrapper_SetFriendTagsResponseCallback_t1715266431(Il2CppObject* delegate, String_t* ___urlPath0, int32_t ___callId1, SetFriendTagsRequest_t1987017382 * ___request2, SetFriendTagsResult_t4117117766 * ___result3, PlayFabError_t750598646 * ___error4, Il2CppObject * ___customData5);
// System.IAsyncResult PlayFab.PlayFabClientAPI/SetFriendTagsResponseCallback::BeginInvoke(System.String,System.Int32,PlayFab.ClientModels.SetFriendTagsRequest,PlayFab.ClientModels.SetFriendTagsResult,PlayFab.PlayFabError,System.Object,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * SetFriendTagsResponseCallback_BeginInvoke_m3871511616 (SetFriendTagsResponseCallback_t1715266431 * __this, String_t* ___urlPath0, int32_t ___callId1, SetFriendTagsRequest_t1987017382 * ___request2, SetFriendTagsResult_t4117117766 * ___result3, PlayFabError_t750598646 * ___error4, Il2CppObject * ___customData5, AsyncCallback_t1363551830 * ___callback6, Il2CppObject * ___object7, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/SetFriendTagsResponseCallback::EndInvoke(System.IAsyncResult)
extern "C"  void SetFriendTagsResponseCallback_EndInvoke_m1064515998 (SetFriendTagsResponseCallback_t1715266431 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
