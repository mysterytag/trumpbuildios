﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.ObjectModel.ReadOnlyCollection`1<PlayFab.Internal.GMFB_327/testRegion>
struct ReadOnlyCollection_1_t553117354;
// System.Collections.Generic.IList`1<PlayFab.Internal.GMFB_327/testRegion>
struct IList_1_t3851431616;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t287207039;
// System.Object
struct Il2CppObject;
// PlayFab.Internal.GMFB_327/testRegion[]
struct testRegionU5BU5D_t485442179;
// System.Collections.Generic.IEnumerator`1<PlayFab.Internal.GMFB_327/testRegion>
struct IEnumerator_1_t3168045750;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_PlayFab_Internal_GMFB_327_testRe1684939302.h"
#include "mscorlib_System_Array2840145358.h"
#include "mscorlib_System_Object837106420.h"

// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<PlayFab.Internal.GMFB_327/testRegion>::.ctor(System.Collections.Generic.IList`1<T>)
extern "C"  void ReadOnlyCollection_1__ctor_m3930575468_gshared (ReadOnlyCollection_1_t553117354 * __this, Il2CppObject* ___list0, const MethodInfo* method);
#define ReadOnlyCollection_1__ctor_m3930575468(__this, ___list0, method) ((  void (*) (ReadOnlyCollection_1_t553117354 *, Il2CppObject*, const MethodInfo*))ReadOnlyCollection_1__ctor_m3930575468_gshared)(__this, ___list0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<PlayFab.Internal.GMFB_327/testRegion>::System.Collections.Generic.ICollection<T>.Add(T)
extern "C"  void ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m4192530006_gshared (ReadOnlyCollection_1_t553117354 * __this, int32_t ___item0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m4192530006(__this, ___item0, method) ((  void (*) (ReadOnlyCollection_1_t553117354 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m4192530006_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<PlayFab.Internal.GMFB_327/testRegion>::System.Collections.Generic.ICollection<T>.Clear()
extern "C"  void ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m3126102324_gshared (ReadOnlyCollection_1_t553117354 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m3126102324(__this, method) ((  void (*) (ReadOnlyCollection_1_t553117354 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m3126102324_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<PlayFab.Internal.GMFB_327/testRegion>::System.Collections.Generic.IList<T>.Insert(System.Int32,T)
extern "C"  void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m828636605_gshared (ReadOnlyCollection_1_t553117354 * __this, int32_t ___index0, int32_t ___item1, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m828636605(__this, ___index0, ___item1, method) ((  void (*) (ReadOnlyCollection_1_t553117354 *, int32_t, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m828636605_gshared)(__this, ___index0, ___item1, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<PlayFab.Internal.GMFB_327/testRegion>::System.Collections.Generic.ICollection<T>.Remove(T)
extern "C"  bool ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m2141845665_gshared (ReadOnlyCollection_1_t553117354 * __this, int32_t ___item0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m2141845665(__this, ___item0, method) ((  bool (*) (ReadOnlyCollection_1_t553117354 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m2141845665_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<PlayFab.Internal.GMFB_327/testRegion>::System.Collections.Generic.IList<T>.RemoveAt(System.Int32)
extern "C"  void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m2997456771_gshared (ReadOnlyCollection_1_t553117354 * __this, int32_t ___index0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m2997456771(__this, ___index0, method) ((  void (*) (ReadOnlyCollection_1_t553117354 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m2997456771_gshared)(__this, ___index0, method)
// T System.Collections.ObjectModel.ReadOnlyCollection`1<PlayFab.Internal.GMFB_327/testRegion>::System.Collections.Generic.IList<T>.get_Item(System.Int32)
extern "C"  int32_t ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m149571561_gshared (ReadOnlyCollection_1_t553117354 * __this, int32_t ___index0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m149571561(__this, ___index0, method) ((  int32_t (*) (ReadOnlyCollection_1_t553117354 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m149571561_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<PlayFab.Internal.GMFB_327/testRegion>::System.Collections.Generic.IList<T>.set_Item(System.Int32,T)
extern "C"  void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m3560424276_gshared (ReadOnlyCollection_1_t553117354 * __this, int32_t ___index0, int32_t ___value1, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m3560424276(__this, ___index0, ___value1, method) ((  void (*) (ReadOnlyCollection_1_t553117354 *, int32_t, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m3560424276_gshared)(__this, ___index0, ___value1, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<PlayFab.Internal.GMFB_327/testRegion>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C"  bool ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m625212878_gshared (ReadOnlyCollection_1_t553117354 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m625212878(__this, method) ((  bool (*) (ReadOnlyCollection_1_t553117354 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m625212878_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<PlayFab.Internal.GMFB_327/testRegion>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m3874764571_gshared (ReadOnlyCollection_1_t553117354 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m3874764571(__this, ___array0, ___index1, method) ((  void (*) (ReadOnlyCollection_1_t553117354 *, Il2CppArray *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m3874764571_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.ObjectModel.ReadOnlyCollection`1<PlayFab.Internal.GMFB_327/testRegion>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m3480558698_gshared (ReadOnlyCollection_1_t553117354 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m3480558698(__this, method) ((  Il2CppObject * (*) (ReadOnlyCollection_1_t553117354 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m3480558698_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<PlayFab.Internal.GMFB_327/testRegion>::System.Collections.IList.Add(System.Object)
extern "C"  int32_t ReadOnlyCollection_1_System_Collections_IList_Add_m3048778131_gshared (ReadOnlyCollection_1_t553117354 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Add_m3048778131(__this, ___value0, method) ((  int32_t (*) (ReadOnlyCollection_1_t553117354 *, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Add_m3048778131_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<PlayFab.Internal.GMFB_327/testRegion>::System.Collections.IList.Clear()
extern "C"  void ReadOnlyCollection_1_System_Collections_IList_Clear_m669807145_gshared (ReadOnlyCollection_1_t553117354 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Clear_m669807145(__this, method) ((  void (*) (ReadOnlyCollection_1_t553117354 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Clear_m669807145_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<PlayFab.Internal.GMFB_327/testRegion>::System.Collections.IList.Contains(System.Object)
extern "C"  bool ReadOnlyCollection_1_System_Collections_IList_Contains_m1787525645_gshared (ReadOnlyCollection_1_t553117354 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Contains_m1787525645(__this, ___value0, method) ((  bool (*) (ReadOnlyCollection_1_t553117354 *, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Contains_m1787525645_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<PlayFab.Internal.GMFB_327/testRegion>::System.Collections.IList.IndexOf(System.Object)
extern "C"  int32_t ReadOnlyCollection_1_System_Collections_IList_IndexOf_m513007211_gshared (ReadOnlyCollection_1_t553117354 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_IndexOf_m513007211(__this, ___value0, method) ((  int32_t (*) (ReadOnlyCollection_1_t553117354 *, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_IndexOf_m513007211_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<PlayFab.Internal.GMFB_327/testRegion>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C"  void ReadOnlyCollection_1_System_Collections_IList_Insert_m1727891550_gshared (ReadOnlyCollection_1_t553117354 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Insert_m1727891550(__this, ___index0, ___value1, method) ((  void (*) (ReadOnlyCollection_1_t553117354 *, int32_t, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Insert_m1727891550_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<PlayFab.Internal.GMFB_327/testRegion>::System.Collections.IList.Remove(System.Object)
extern "C"  void ReadOnlyCollection_1_System_Collections_IList_Remove_m1105846282_gshared (ReadOnlyCollection_1_t553117354 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Remove_m1105846282(__this, ___value0, method) ((  void (*) (ReadOnlyCollection_1_t553117354 *, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Remove_m1105846282_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<PlayFab.Internal.GMFB_327/testRegion>::System.Collections.IList.RemoveAt(System.Int32)
extern "C"  void ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m1506852078_gshared (ReadOnlyCollection_1_t553117354 * __this, int32_t ___index0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m1506852078(__this, ___index0, method) ((  void (*) (ReadOnlyCollection_1_t553117354 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m1506852078_gshared)(__this, ___index0, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<PlayFab.Internal.GMFB_327/testRegion>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m3166930479_gshared (ReadOnlyCollection_1_t553117354 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m3166930479(__this, method) ((  bool (*) (ReadOnlyCollection_1_t553117354 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m3166930479_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.ReadOnlyCollection`1<PlayFab.Internal.GMFB_327/testRegion>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m2663206305_gshared (ReadOnlyCollection_1_t553117354 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m2663206305(__this, method) ((  Il2CppObject * (*) (ReadOnlyCollection_1_t553117354 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m2663206305_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<PlayFab.Internal.GMFB_327/testRegion>::System.Collections.IList.get_IsFixedSize()
extern "C"  bool ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m755431228_gshared (ReadOnlyCollection_1_t553117354 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m755431228(__this, method) ((  bool (*) (ReadOnlyCollection_1_t553117354 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m755431228_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<PlayFab.Internal.GMFB_327/testRegion>::System.Collections.IList.get_IsReadOnly()
extern "C"  bool ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m3110776893_gshared (ReadOnlyCollection_1_t553117354 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m3110776893(__this, method) ((  bool (*) (ReadOnlyCollection_1_t553117354 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m3110776893_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.ReadOnlyCollection`1<PlayFab.Internal.GMFB_327/testRegion>::System.Collections.IList.get_Item(System.Int32)
extern "C"  Il2CppObject * ReadOnlyCollection_1_System_Collections_IList_get_Item_m606362344_gshared (ReadOnlyCollection_1_t553117354 * __this, int32_t ___index0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_get_Item_m606362344(__this, ___index0, method) ((  Il2CppObject * (*) (ReadOnlyCollection_1_t553117354 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_Item_m606362344_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<PlayFab.Internal.GMFB_327/testRegion>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C"  void ReadOnlyCollection_1_System_Collections_IList_set_Item_m3260355765_gshared (ReadOnlyCollection_1_t553117354 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_set_Item_m3260355765(__this, ___index0, ___value1, method) ((  void (*) (ReadOnlyCollection_1_t553117354 *, int32_t, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_set_Item_m3260355765_gshared)(__this, ___index0, ___value1, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<PlayFab.Internal.GMFB_327/testRegion>::Contains(T)
extern "C"  bool ReadOnlyCollection_1_Contains_m1390377702_gshared (ReadOnlyCollection_1_t553117354 * __this, int32_t ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_Contains_m1390377702(__this, ___value0, method) ((  bool (*) (ReadOnlyCollection_1_t553117354 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_Contains_m1390377702_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<PlayFab.Internal.GMFB_327/testRegion>::CopyTo(T[],System.Int32)
extern "C"  void ReadOnlyCollection_1_CopyTo_m3469894534_gshared (ReadOnlyCollection_1_t553117354 * __this, testRegionU5BU5D_t485442179* ___array0, int32_t ___index1, const MethodInfo* method);
#define ReadOnlyCollection_1_CopyTo_m3469894534(__this, ___array0, ___index1, method) ((  void (*) (ReadOnlyCollection_1_t553117354 *, testRegionU5BU5D_t485442179*, int32_t, const MethodInfo*))ReadOnlyCollection_1_CopyTo_m3469894534_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.ObjectModel.ReadOnlyCollection`1<PlayFab.Internal.GMFB_327/testRegion>::GetEnumerator()
extern "C"  Il2CppObject* ReadOnlyCollection_1_GetEnumerator_m1894937277_gshared (ReadOnlyCollection_1_t553117354 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_GetEnumerator_m1894937277(__this, method) ((  Il2CppObject* (*) (ReadOnlyCollection_1_t553117354 *, const MethodInfo*))ReadOnlyCollection_1_GetEnumerator_m1894937277_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<PlayFab.Internal.GMFB_327/testRegion>::IndexOf(T)
extern "C"  int32_t ReadOnlyCollection_1_IndexOf_m2495655634_gshared (ReadOnlyCollection_1_t553117354 * __this, int32_t ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_IndexOf_m2495655634(__this, ___value0, method) ((  int32_t (*) (ReadOnlyCollection_1_t553117354 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_IndexOf_m2495655634_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<PlayFab.Internal.GMFB_327/testRegion>::get_Count()
extern "C"  int32_t ReadOnlyCollection_1_get_Count_m4168188137_gshared (ReadOnlyCollection_1_t553117354 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_get_Count_m4168188137(__this, method) ((  int32_t (*) (ReadOnlyCollection_1_t553117354 *, const MethodInfo*))ReadOnlyCollection_1_get_Count_m4168188137_gshared)(__this, method)
// T System.Collections.ObjectModel.ReadOnlyCollection`1<PlayFab.Internal.GMFB_327/testRegion>::get_Item(System.Int32)
extern "C"  int32_t ReadOnlyCollection_1_get_Item_m1252207145_gshared (ReadOnlyCollection_1_t553117354 * __this, int32_t ___index0, const MethodInfo* method);
#define ReadOnlyCollection_1_get_Item_m1252207145(__this, ___index0, method) ((  int32_t (*) (ReadOnlyCollection_1_t553117354 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_get_Item_m1252207145_gshared)(__this, ___index0, method)
