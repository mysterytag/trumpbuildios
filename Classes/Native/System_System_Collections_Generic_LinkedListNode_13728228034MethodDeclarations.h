﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "System_System_Collections_Generic_LinkedListNode_1_539097040MethodDeclarations.h"

// System.Void System.Collections.Generic.LinkedListNode`1<IProcess>::.ctor(System.Collections.Generic.LinkedList`1<T>,T)
#define LinkedListNode_1__ctor_m3428748446(__this, ___list0, ___value1, method) ((  void (*) (LinkedListNode_1_t3728228034 *, LinkedList_1_t1471399664 *, Il2CppObject *, const MethodInfo*))LinkedListNode_1__ctor_m648136130_gshared)(__this, ___list0, ___value1, method)
// System.Void System.Collections.Generic.LinkedListNode`1<IProcess>::.ctor(System.Collections.Generic.LinkedList`1<T>,T,System.Collections.Generic.LinkedListNode`1<T>,System.Collections.Generic.LinkedListNode`1<T>)
#define LinkedListNode_1__ctor_m562521854(__this, ___list0, ___value1, ___previousNode2, ___nextNode3, method) ((  void (*) (LinkedListNode_1_t3728228034 *, LinkedList_1_t1471399664 *, Il2CppObject *, LinkedListNode_1_t3728228034 *, LinkedListNode_1_t3728228034 *, const MethodInfo*))LinkedListNode_1__ctor_m448391458_gshared)(__this, ___list0, ___value1, ___previousNode2, ___nextNode3, method)
// System.Void System.Collections.Generic.LinkedListNode`1<IProcess>::Detach()
#define LinkedListNode_1_Detach_m3191197186(__this, method) ((  void (*) (LinkedListNode_1_t3728228034 *, const MethodInfo*))LinkedListNode_1_Detach_m3406254942_gshared)(__this, method)
// System.Collections.Generic.LinkedList`1<T> System.Collections.Generic.LinkedListNode`1<IProcess>::get_List()
#define LinkedListNode_1_get_List_m4212222566(__this, method) ((  LinkedList_1_t1471399664 * (*) (LinkedListNode_1_t3728228034 *, const MethodInfo*))LinkedListNode_1_get_List_m3467110818_gshared)(__this, method)
// System.Collections.Generic.LinkedListNode`1<T> System.Collections.Generic.LinkedListNode`1<IProcess>::get_Next()
#define LinkedListNode_1_get_Next_m3092895619(__this, method) ((  LinkedListNode_1_t3728228034 * (*) (LinkedListNode_1_t3728228034 *, const MethodInfo*))LinkedListNode_1_get_Next_m1427618777_gshared)(__this, method)
// System.Collections.Generic.LinkedListNode`1<T> System.Collections.Generic.LinkedListNode`1<IProcess>::get_Previous()
#define LinkedListNode_1_get_Previous_m3449217437(__this, method) ((  LinkedListNode_1_t3728228034 * (*) (LinkedListNode_1_t3728228034 *, const MethodInfo*))LinkedListNode_1_get_Previous_m3755155549_gshared)(__this, method)
// T System.Collections.Generic.LinkedListNode`1<IProcess>::get_Value()
#define LinkedListNode_1_get_Value_m233840874(__this, method) ((  Il2CppObject * (*) (LinkedListNode_1_t3728228034 *, const MethodInfo*))LinkedListNode_1_get_Value_m702633824_gshared)(__this, method)
