﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Func`2<System.Collections.Generic.IList`1<System.Boolean>,System.Boolean>
struct Func_2_t1721034418;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.ReactivePropertyExtensions
struct  ReactivePropertyExtensions_t1053651444  : public Il2CppObject
{
public:

public:
};

struct ReactivePropertyExtensions_t1053651444_StaticFields
{
public:
	// System.Func`2<System.Collections.Generic.IList`1<System.Boolean>,System.Boolean> UniRx.ReactivePropertyExtensions::<>f__am$cache0
	Func_2_t1721034418 * ___U3CU3Ef__amU24cache0_0;
	// System.Func`2<System.Collections.Generic.IList`1<System.Boolean>,System.Boolean> UniRx.ReactivePropertyExtensions::<>f__am$cache1
	Func_2_t1721034418 * ___U3CU3Ef__amU24cache1_1;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_0() { return static_cast<int32_t>(offsetof(ReactivePropertyExtensions_t1053651444_StaticFields, ___U3CU3Ef__amU24cache0_0)); }
	inline Func_2_t1721034418 * get_U3CU3Ef__amU24cache0_0() const { return ___U3CU3Ef__amU24cache0_0; }
	inline Func_2_t1721034418 ** get_address_of_U3CU3Ef__amU24cache0_0() { return &___U3CU3Ef__amU24cache0_0; }
	inline void set_U3CU3Ef__amU24cache0_0(Func_2_t1721034418 * value)
	{
		___U3CU3Ef__amU24cache0_0 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache0_0, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache1_1() { return static_cast<int32_t>(offsetof(ReactivePropertyExtensions_t1053651444_StaticFields, ___U3CU3Ef__amU24cache1_1)); }
	inline Func_2_t1721034418 * get_U3CU3Ef__amU24cache1_1() const { return ___U3CU3Ef__amU24cache1_1; }
	inline Func_2_t1721034418 ** get_address_of_U3CU3Ef__amU24cache1_1() { return &___U3CU3Ef__amU24cache1_1; }
	inline void set_U3CU3Ef__amU24cache1_1(Func_2_t1721034418 * value)
	{
		___U3CU3Ef__amU24cache1_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache1_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
