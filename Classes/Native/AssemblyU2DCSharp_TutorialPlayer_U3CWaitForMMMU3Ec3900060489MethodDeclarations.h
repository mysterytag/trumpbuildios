﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// TutorialPlayer/<WaitForMMM>c__Iterator23
struct U3CWaitForMMMU3Ec__Iterator23_t3900060489;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void TutorialPlayer/<WaitForMMM>c__Iterator23::.ctor()
extern "C"  void U3CWaitForMMMU3Ec__Iterator23__ctor_m4234529154 (U3CWaitForMMMU3Ec__Iterator23_t3900060489 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object TutorialPlayer/<WaitForMMM>c__Iterator23::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CWaitForMMMU3Ec__Iterator23_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3759896730 (U3CWaitForMMMU3Ec__Iterator23_t3900060489 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object TutorialPlayer/<WaitForMMM>c__Iterator23::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CWaitForMMMU3Ec__Iterator23_System_Collections_IEnumerator_get_Current_m74838062 (U3CWaitForMMMU3Ec__Iterator23_t3900060489 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean TutorialPlayer/<WaitForMMM>c__Iterator23::MoveNext()
extern "C"  bool U3CWaitForMMMU3Ec__Iterator23_MoveNext_m2038505754 (U3CWaitForMMMU3Ec__Iterator23_t3900060489 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TutorialPlayer/<WaitForMMM>c__Iterator23::Dispose()
extern "C"  void U3CWaitForMMMU3Ec__Iterator23_Dispose_m1946770751 (U3CWaitForMMMU3Ec__Iterator23_t3900060489 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TutorialPlayer/<WaitForMMM>c__Iterator23::Reset()
extern "C"  void U3CWaitForMMMU3Ec__Iterator23_Reset_m1880962095 (U3CWaitForMMMU3Ec__Iterator23_t3900060489 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
