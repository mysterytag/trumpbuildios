﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Zenject.DiContainer/<GetProviderMatchesInternal>c__AnonStorey187
struct U3CGetProviderMatchesInternalU3Ec__AnonStorey187_t855530210;
// Zenject.ProviderBase
struct ProviderBase_t1627494391;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Zenject_ProviderBase1627494391.h"

// System.Void Zenject.DiContainer/<GetProviderMatchesInternal>c__AnonStorey187::.ctor()
extern "C"  void U3CGetProviderMatchesInternalU3Ec__AnonStorey187__ctor_m2319065927 (U3CGetProviderMatchesInternalU3Ec__AnonStorey187_t855530210 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Zenject.DiContainer/<GetProviderMatchesInternal>c__AnonStorey187::<>m__2C2(Zenject.ProviderBase)
extern "C"  bool U3CGetProviderMatchesInternalU3Ec__AnonStorey187_U3CU3Em__2C2_m774606086 (U3CGetProviderMatchesInternalU3Ec__AnonStorey187_t855530210 * __this, ProviderBase_t1627494391 * ___x0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
