﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Mono.Security.Protocol.Tls.SslStreamBase/AsyncHandshakeDelegate
struct AsyncHandshakeDelegate_t751863712;
// System.Object
struct Il2CppObject;
// Mono.Security.Protocol.Tls.SslStreamBase/InternalAsyncResult
struct InternalAsyncResult_t2689067676;
// System.IAsyncResult
struct IAsyncResult_t537683269;
// System.AsyncCallback
struct AsyncCallback_t1363551830;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_IntPtr676692020.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_SslStream2689067676.h"
#include "mscorlib_System_AsyncCallback1363551830.h"

// System.Void Mono.Security.Protocol.Tls.SslStreamBase/AsyncHandshakeDelegate::.ctor(System.Object,System.IntPtr)
extern "C"  void AsyncHandshakeDelegate__ctor_m2041293580 (AsyncHandshakeDelegate_t751863712 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Security.Protocol.Tls.SslStreamBase/AsyncHandshakeDelegate::Invoke(Mono.Security.Protocol.Tls.SslStreamBase/InternalAsyncResult,System.Boolean)
extern "C"  void AsyncHandshakeDelegate_Invoke_m2395526203 (AsyncHandshakeDelegate_t751863712 * __this, InternalAsyncResult_t2689067676 * ___asyncResult0, bool ___fromWrite1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
extern "C" void pinvoke_delegate_wrapper_AsyncHandshakeDelegate_t751863712(Il2CppObject* delegate, InternalAsyncResult_t2689067676 * ___asyncResult0, bool ___fromWrite1);
// System.IAsyncResult Mono.Security.Protocol.Tls.SslStreamBase/AsyncHandshakeDelegate::BeginInvoke(Mono.Security.Protocol.Tls.SslStreamBase/InternalAsyncResult,System.Boolean,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * AsyncHandshakeDelegate_BeginInvoke_m212673258 (AsyncHandshakeDelegate_t751863712 * __this, InternalAsyncResult_t2689067676 * ___asyncResult0, bool ___fromWrite1, AsyncCallback_t1363551830 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Security.Protocol.Tls.SslStreamBase/AsyncHandshakeDelegate::EndInvoke(System.IAsyncResult)
extern "C"  void AsyncHandshakeDelegate_EndInvoke_m591823644 (AsyncHandshakeDelegate_t751863712 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
