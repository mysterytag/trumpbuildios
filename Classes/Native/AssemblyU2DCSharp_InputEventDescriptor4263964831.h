﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_Object837106420.h"
#include "AssemblyU2DCSharp_InputEventDescriptor_Type2622298.h"
#include "UnityEngine_UnityEngine_Vector23525329788.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// InputEventDescriptor
struct  InputEventDescriptor_t4263964831  : public Il2CppObject
{
public:
	// System.Int32 InputEventDescriptor::FingerId
	int32_t ___FingerId_0;
	// InputEventDescriptor/Type InputEventDescriptor::type
	int32_t ___type_1;
	// UnityEngine.Vector2 InputEventDescriptor::inputPosition
	Vector2_t3525329788  ___inputPosition_2;
	// UnityEngine.Vector2 InputEventDescriptor::inputDelta
	Vector2_t3525329788  ___inputDelta_3;
	// System.Boolean InputEventDescriptor::used_
	bool ___used__4;

public:
	inline static int32_t get_offset_of_FingerId_0() { return static_cast<int32_t>(offsetof(InputEventDescriptor_t4263964831, ___FingerId_0)); }
	inline int32_t get_FingerId_0() const { return ___FingerId_0; }
	inline int32_t* get_address_of_FingerId_0() { return &___FingerId_0; }
	inline void set_FingerId_0(int32_t value)
	{
		___FingerId_0 = value;
	}

	inline static int32_t get_offset_of_type_1() { return static_cast<int32_t>(offsetof(InputEventDescriptor_t4263964831, ___type_1)); }
	inline int32_t get_type_1() const { return ___type_1; }
	inline int32_t* get_address_of_type_1() { return &___type_1; }
	inline void set_type_1(int32_t value)
	{
		___type_1 = value;
	}

	inline static int32_t get_offset_of_inputPosition_2() { return static_cast<int32_t>(offsetof(InputEventDescriptor_t4263964831, ___inputPosition_2)); }
	inline Vector2_t3525329788  get_inputPosition_2() const { return ___inputPosition_2; }
	inline Vector2_t3525329788 * get_address_of_inputPosition_2() { return &___inputPosition_2; }
	inline void set_inputPosition_2(Vector2_t3525329788  value)
	{
		___inputPosition_2 = value;
	}

	inline static int32_t get_offset_of_inputDelta_3() { return static_cast<int32_t>(offsetof(InputEventDescriptor_t4263964831, ___inputDelta_3)); }
	inline Vector2_t3525329788  get_inputDelta_3() const { return ___inputDelta_3; }
	inline Vector2_t3525329788 * get_address_of_inputDelta_3() { return &___inputDelta_3; }
	inline void set_inputDelta_3(Vector2_t3525329788  value)
	{
		___inputDelta_3 = value;
	}

	inline static int32_t get_offset_of_used__4() { return static_cast<int32_t>(offsetof(InputEventDescriptor_t4263964831, ___used__4)); }
	inline bool get_used__4() const { return ___used__4; }
	inline bool* get_address_of_used__4() { return &___used__4; }
	inline void set_used__4(bool value)
	{
		___used__4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
