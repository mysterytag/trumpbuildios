﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// <>__AnonType4`2<System.Single,System.Object>
struct U3CU3E__AnonType4_2_t3892302468;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"

// System.Void <>__AnonType4`2<System.Single,System.Object>::.ctor(<f>__T,<valuta>__T)
extern "C"  void U3CU3E__AnonType4_2__ctor_m2661760497_gshared (U3CU3E__AnonType4_2_t3892302468 * __this, float ___f0, Il2CppObject * ___valuta1, const MethodInfo* method);
#define U3CU3E__AnonType4_2__ctor_m2661760497(__this, ___f0, ___valuta1, method) ((  void (*) (U3CU3E__AnonType4_2_t3892302468 *, float, Il2CppObject *, const MethodInfo*))U3CU3E__AnonType4_2__ctor_m2661760497_gshared)(__this, ___f0, ___valuta1, method)
// <f>__T <>__AnonType4`2<System.Single,System.Object>::get_f()
extern "C"  float U3CU3E__AnonType4_2_get_f_m487471190_gshared (U3CU3E__AnonType4_2_t3892302468 * __this, const MethodInfo* method);
#define U3CU3E__AnonType4_2_get_f_m487471190(__this, method) ((  float (*) (U3CU3E__AnonType4_2_t3892302468 *, const MethodInfo*))U3CU3E__AnonType4_2_get_f_m487471190_gshared)(__this, method)
// <valuta>__T <>__AnonType4`2<System.Single,System.Object>::get_valuta()
extern "C"  Il2CppObject * U3CU3E__AnonType4_2_get_valuta_m2605979094_gshared (U3CU3E__AnonType4_2_t3892302468 * __this, const MethodInfo* method);
#define U3CU3E__AnonType4_2_get_valuta_m2605979094(__this, method) ((  Il2CppObject * (*) (U3CU3E__AnonType4_2_t3892302468 *, const MethodInfo*))U3CU3E__AnonType4_2_get_valuta_m2605979094_gshared)(__this, method)
// System.Boolean <>__AnonType4`2<System.Single,System.Object>::Equals(System.Object)
extern "C"  bool U3CU3E__AnonType4_2_Equals_m1823435029_gshared (U3CU3E__AnonType4_2_t3892302468 * __this, Il2CppObject * ___obj0, const MethodInfo* method);
#define U3CU3E__AnonType4_2_Equals_m1823435029(__this, ___obj0, method) ((  bool (*) (U3CU3E__AnonType4_2_t3892302468 *, Il2CppObject *, const MethodInfo*))U3CU3E__AnonType4_2_Equals_m1823435029_gshared)(__this, ___obj0, method)
// System.Int32 <>__AnonType4`2<System.Single,System.Object>::GetHashCode()
extern "C"  int32_t U3CU3E__AnonType4_2_GetHashCode_m815653945_gshared (U3CU3E__AnonType4_2_t3892302468 * __this, const MethodInfo* method);
#define U3CU3E__AnonType4_2_GetHashCode_m815653945(__this, method) ((  int32_t (*) (U3CU3E__AnonType4_2_t3892302468 *, const MethodInfo*))U3CU3E__AnonType4_2_GetHashCode_m815653945_gshared)(__this, method)
// System.String <>__AnonType4`2<System.Single,System.Object>::ToString()
extern "C"  String_t* U3CU3E__AnonType4_2_ToString_m937938171_gshared (U3CU3E__AnonType4_2_t3892302468 * __this, const MethodInfo* method);
#define U3CU3E__AnonType4_2_ToString_m937938171(__this, method) ((  String_t* (*) (U3CU3E__AnonType4_2_t3892302468 *, const MethodInfo*))U3CU3E__AnonType4_2_ToString_m937938171_gshared)(__this, method)
