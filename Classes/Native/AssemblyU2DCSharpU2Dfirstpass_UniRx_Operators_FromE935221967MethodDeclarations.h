﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.FromEventObservable`2/FromEvent<System.Object,UnityEngine.Vector2>
struct FromEvent_t935221967;
// UniRx.Operators.FromEventObservable`2<System.Object,UnityEngine.Vector2>
struct FromEventObservable_2_t645395669;
// UniRx.IObserver`1<UnityEngine.Vector2>
struct IObserver_1_t1442361395;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Vector23525329788.h"

// System.Void UniRx.Operators.FromEventObservable`2/FromEvent<System.Object,UnityEngine.Vector2>::.ctor(UniRx.Operators.FromEventObservable`2<TDelegate,TEventArgs>,UniRx.IObserver`1<TEventArgs>)
extern "C"  void FromEvent__ctor_m65149535_gshared (FromEvent_t935221967 * __this, FromEventObservable_2_t645395669 * ___parent0, Il2CppObject* ___observer1, const MethodInfo* method);
#define FromEvent__ctor_m65149535(__this, ___parent0, ___observer1, method) ((  void (*) (FromEvent_t935221967 *, FromEventObservable_2_t645395669 *, Il2CppObject*, const MethodInfo*))FromEvent__ctor_m65149535_gshared)(__this, ___parent0, ___observer1, method)
// System.Boolean UniRx.Operators.FromEventObservable`2/FromEvent<System.Object,UnityEngine.Vector2>::Register()
extern "C"  bool FromEvent_Register_m4007355061_gshared (FromEvent_t935221967 * __this, const MethodInfo* method);
#define FromEvent_Register_m4007355061(__this, method) ((  bool (*) (FromEvent_t935221967 *, const MethodInfo*))FromEvent_Register_m4007355061_gshared)(__this, method)
// System.Void UniRx.Operators.FromEventObservable`2/FromEvent<System.Object,UnityEngine.Vector2>::OnNext(TEventArgs)
extern "C"  void FromEvent_OnNext_m2501074789_gshared (FromEvent_t935221967 * __this, Vector2_t3525329788  ___args0, const MethodInfo* method);
#define FromEvent_OnNext_m2501074789(__this, ___args0, method) ((  void (*) (FromEvent_t935221967 *, Vector2_t3525329788 , const MethodInfo*))FromEvent_OnNext_m2501074789_gshared)(__this, ___args0, method)
// System.Void UniRx.Operators.FromEventObservable`2/FromEvent<System.Object,UnityEngine.Vector2>::Dispose()
extern "C"  void FromEvent_Dispose_m4108434827_gshared (FromEvent_t935221967 * __this, const MethodInfo* method);
#define FromEvent_Dispose_m4108434827(__this, method) ((  void (*) (FromEvent_t935221967 *, const MethodInfo*))FromEvent_Dispose_m4108434827_gshared)(__this, method)
