﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.PlayFabClientAPI/UnlinkXboxAccountResponseCallback
struct UnlinkXboxAccountResponseCallback_t3759695341;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;
// PlayFab.ClientModels.UnlinkXboxAccountRequest
struct UnlinkXboxAccountRequest_t1448666232;
// PlayFab.ClientModels.UnlinkXboxAccountResult
struct UnlinkXboxAccountResult_t4238298932;
// PlayFab.PlayFabError
struct PlayFabError_t750598646;
// System.IAsyncResult
struct IAsyncResult_t537683269;
// System.AsyncCallback
struct AsyncCallback_t1363551830;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_IntPtr676692020.h"
#include "mscorlib_System_String968488902.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_UnlinkXboxA1448666232.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_UnlinkXboxA4238298932.h"
#include "AssemblyU2DCSharpU2Dfirstpass_PlayFab_PlayFabError750598646.h"
#include "mscorlib_System_AsyncCallback1363551830.h"

// System.Void PlayFab.PlayFabClientAPI/UnlinkXboxAccountResponseCallback::.ctor(System.Object,System.IntPtr)
extern "C"  void UnlinkXboxAccountResponseCallback__ctor_m2898578428 (UnlinkXboxAccountResponseCallback_t3759695341 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/UnlinkXboxAccountResponseCallback::Invoke(System.String,System.Int32,PlayFab.ClientModels.UnlinkXboxAccountRequest,PlayFab.ClientModels.UnlinkXboxAccountResult,PlayFab.PlayFabError,System.Object)
extern "C"  void UnlinkXboxAccountResponseCallback_Invoke_m804997941 (UnlinkXboxAccountResponseCallback_t3759695341 * __this, String_t* ___urlPath0, int32_t ___callId1, UnlinkXboxAccountRequest_t1448666232 * ___request2, UnlinkXboxAccountResult_t4238298932 * ___result3, PlayFabError_t750598646 * ___error4, Il2CppObject * ___customData5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
extern "C" void pinvoke_delegate_wrapper_UnlinkXboxAccountResponseCallback_t3759695341(Il2CppObject* delegate, String_t* ___urlPath0, int32_t ___callId1, UnlinkXboxAccountRequest_t1448666232 * ___request2, UnlinkXboxAccountResult_t4238298932 * ___result3, PlayFabError_t750598646 * ___error4, Il2CppObject * ___customData5);
// System.IAsyncResult PlayFab.PlayFabClientAPI/UnlinkXboxAccountResponseCallback::BeginInvoke(System.String,System.Int32,PlayFab.ClientModels.UnlinkXboxAccountRequest,PlayFab.ClientModels.UnlinkXboxAccountResult,PlayFab.PlayFabError,System.Object,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * UnlinkXboxAccountResponseCallback_BeginInvoke_m2918005074 (UnlinkXboxAccountResponseCallback_t3759695341 * __this, String_t* ___urlPath0, int32_t ___callId1, UnlinkXboxAccountRequest_t1448666232 * ___request2, UnlinkXboxAccountResult_t4238298932 * ___result3, PlayFabError_t750598646 * ___error4, Il2CppObject * ___customData5, AsyncCallback_t1363551830 * ___callback6, Il2CppObject * ___object7, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/UnlinkXboxAccountResponseCallback::EndInvoke(System.IAsyncResult)
extern "C"  void UnlinkXboxAccountResponseCallback_EndInvoke_m645241868 (UnlinkXboxAccountResponseCallback_t3759695341 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
