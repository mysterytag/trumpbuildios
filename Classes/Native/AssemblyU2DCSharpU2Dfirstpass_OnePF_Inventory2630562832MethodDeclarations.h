﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// OnePF.Inventory
struct Inventory_t2630562832;
// System.String
struct String_t;
// OnePF.SkuDetails
struct SkuDetails_t70130009;
// OnePF.Purchase
struct Purchase_t160195573;
// System.Collections.Generic.List`1<System.String>
struct List_1_t1765447871;
// System.Collections.Generic.List`1<OnePF.Purchase>
struct List_1_t957154542;
// System.Collections.Generic.List`1<OnePF.SkuDetails>
struct List_1_t867088978;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String968488902.h"
#include "AssemblyU2DCSharpU2Dfirstpass_OnePF_SkuDetails70130009.h"
#include "AssemblyU2DCSharpU2Dfirstpass_OnePF_Purchase160195573.h"

// System.Void OnePF.Inventory::.ctor(System.String)
extern "C"  void Inventory__ctor_m529083189 (Inventory_t2630562832 * __this, String_t* ___json0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String OnePF.Inventory::ToString()
extern "C"  String_t* Inventory_ToString_m517228064 (Inventory_t2630562832 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// OnePF.SkuDetails OnePF.Inventory::GetSkuDetails(System.String)
extern "C"  SkuDetails_t70130009 * Inventory_GetSkuDetails_m2242380878 (Inventory_t2630562832 * __this, String_t* ___sku0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// OnePF.Purchase OnePF.Inventory::GetPurchase(System.String)
extern "C"  Purchase_t160195573 * Inventory_GetPurchase_m339254670 (Inventory_t2630562832 * __this, String_t* ___sku0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean OnePF.Inventory::HasPurchase(System.String)
extern "C"  bool Inventory_HasPurchase_m1648840736 (Inventory_t2630562832 * __this, String_t* ___sku0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean OnePF.Inventory::HasDetails(System.String)
extern "C"  bool Inventory_HasDetails_m774176127 (Inventory_t2630562832 * __this, String_t* ___sku0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OnePF.Inventory::ErasePurchase(System.String)
extern "C"  void Inventory_ErasePurchase_m1346425904 (Inventory_t2630562832 * __this, String_t* ___sku0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<System.String> OnePF.Inventory::GetAllOwnedSkus()
extern "C"  List_1_t1765447871 * Inventory_GetAllOwnedSkus_m2770015284 (Inventory_t2630562832 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<System.String> OnePF.Inventory::GetAllOwnedSkus(System.String)
extern "C"  List_1_t1765447871 * Inventory_GetAllOwnedSkus_m3388211022 (Inventory_t2630562832 * __this, String_t* ___itemType0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<OnePF.Purchase> OnePF.Inventory::GetAllPurchases()
extern "C"  List_1_t957154542 * Inventory_GetAllPurchases_m2700192536 (Inventory_t2630562832 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<OnePF.SkuDetails> OnePF.Inventory::GetAllAvailableSkus()
extern "C"  List_1_t867088978 * Inventory_GetAllAvailableSkus_m887039689 (Inventory_t2630562832 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OnePF.Inventory::AddSkuDetails(OnePF.SkuDetails)
extern "C"  void Inventory_AddSkuDetails_m1656086106 (Inventory_t2630562832 * __this, SkuDetails_t70130009 * ___d0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OnePF.Inventory::AddPurchase(OnePF.Purchase)
extern "C"  void Inventory_AddPurchase_m3006154266 (Inventory_t2630562832 * __this, Purchase_t160195573 * ___p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
