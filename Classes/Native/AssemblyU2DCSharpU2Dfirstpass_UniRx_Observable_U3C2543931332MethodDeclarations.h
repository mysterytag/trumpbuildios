﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Observable/<NextFrame>c__AnonStorey52
struct U3CNextFrameU3Ec__AnonStorey52_t2543931332;
// System.Collections.IEnumerator
struct IEnumerator_t287207039;
// UniRx.IObserver`1<UniRx.Unit>
struct IObserver_1_t475317645;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_CancellationTo1439151560.h"

// System.Void UniRx.Observable/<NextFrame>c__AnonStorey52::.ctor()
extern "C"  void U3CNextFrameU3Ec__AnonStorey52__ctor_m2220673023 (U3CNextFrameU3Ec__AnonStorey52_t2543931332 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator UniRx.Observable/<NextFrame>c__AnonStorey52::<>m__55(UniRx.IObserver`1<UniRx.Unit>,UniRx.CancellationToken)
extern "C"  Il2CppObject * U3CNextFrameU3Ec__AnonStorey52_U3CU3Em__55_m4159195242 (U3CNextFrameU3Ec__AnonStorey52_t2543931332 * __this, Il2CppObject* ___observer0, CancellationToken_t1439151560  ___cancellation1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
