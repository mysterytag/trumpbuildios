﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.PlayFabClientAPI/<LoginWithPSN>c__AnonStorey68
struct U3CLoginWithPSNU3Ec__AnonStorey68_t3151494499;
// PlayFab.CallRequestContainer
struct CallRequestContainer_t432318609;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharpU2Dfirstpass_PlayFab_CallRequestCo432318609.h"

// System.Void PlayFab.PlayFabClientAPI/<LoginWithPSN>c__AnonStorey68::.ctor()
extern "C"  void U3CLoginWithPSNU3Ec__AnonStorey68__ctor_m882034096 (U3CLoginWithPSNU3Ec__AnonStorey68_t3151494499 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/<LoginWithPSN>c__AnonStorey68::<>m__55(PlayFab.CallRequestContainer)
extern "C"  void U3CLoginWithPSNU3Ec__AnonStorey68_U3CU3Em__55_m2258595630 (U3CLoginWithPSNU3Ec__AnonStorey68_t3151494499 * __this, CallRequestContainer_t432318609 * ___requestContainer0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
