﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Generic.IList`1<FriendInfo>
struct IList_1_t2402962726;
// System.String
struct String_t;
// System.Action`2<FriendInfo,FriendView>
struct Action_2_t2425171301;
// UnityEngine.GameObject
struct GameObject_t4012695102;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GenericDataSource`2<FriendInfo,FriendView>
struct  GenericDataSource_2_t4294355128  : public Il2CppObject
{
public:
	// System.Collections.Generic.IList`1<T> GenericDataSource`2::data
	Il2CppObject* ___data_0;
	// System.String GenericDataSource`2::rId
	String_t* ___rId_1;
	// System.Action`2<T,TV> GenericDataSource`2::fillFactory
	Action_2_t2425171301 * ___fillFactory_2;
	// UnityEngine.GameObject GenericDataSource`2::_cellPrefab
	GameObject_t4012695102 * ____cellPrefab_3;
	// System.Single GenericDataSource`2::cellHeight
	float ___cellHeight_4;

public:
	inline static int32_t get_offset_of_data_0() { return static_cast<int32_t>(offsetof(GenericDataSource_2_t4294355128, ___data_0)); }
	inline Il2CppObject* get_data_0() const { return ___data_0; }
	inline Il2CppObject** get_address_of_data_0() { return &___data_0; }
	inline void set_data_0(Il2CppObject* value)
	{
		___data_0 = value;
		Il2CppCodeGenWriteBarrier(&___data_0, value);
	}

	inline static int32_t get_offset_of_rId_1() { return static_cast<int32_t>(offsetof(GenericDataSource_2_t4294355128, ___rId_1)); }
	inline String_t* get_rId_1() const { return ___rId_1; }
	inline String_t** get_address_of_rId_1() { return &___rId_1; }
	inline void set_rId_1(String_t* value)
	{
		___rId_1 = value;
		Il2CppCodeGenWriteBarrier(&___rId_1, value);
	}

	inline static int32_t get_offset_of_fillFactory_2() { return static_cast<int32_t>(offsetof(GenericDataSource_2_t4294355128, ___fillFactory_2)); }
	inline Action_2_t2425171301 * get_fillFactory_2() const { return ___fillFactory_2; }
	inline Action_2_t2425171301 ** get_address_of_fillFactory_2() { return &___fillFactory_2; }
	inline void set_fillFactory_2(Action_2_t2425171301 * value)
	{
		___fillFactory_2 = value;
		Il2CppCodeGenWriteBarrier(&___fillFactory_2, value);
	}

	inline static int32_t get_offset_of__cellPrefab_3() { return static_cast<int32_t>(offsetof(GenericDataSource_2_t4294355128, ____cellPrefab_3)); }
	inline GameObject_t4012695102 * get__cellPrefab_3() const { return ____cellPrefab_3; }
	inline GameObject_t4012695102 ** get_address_of__cellPrefab_3() { return &____cellPrefab_3; }
	inline void set__cellPrefab_3(GameObject_t4012695102 * value)
	{
		____cellPrefab_3 = value;
		Il2CppCodeGenWriteBarrier(&____cellPrefab_3, value);
	}

	inline static int32_t get_offset_of_cellHeight_4() { return static_cast<int32_t>(offsetof(GenericDataSource_2_t4294355128, ___cellHeight_4)); }
	inline float get_cellHeight_4() const { return ___cellHeight_4; }
	inline float* get_address_of_cellHeight_4() { return &___cellHeight_4; }
	inline void set_cellHeight_4(float value)
	{
		___cellHeight_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
