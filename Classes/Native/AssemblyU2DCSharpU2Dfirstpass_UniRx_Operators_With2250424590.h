﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UniRx.Operators.WithLatestFromObservable`3/WithLatestFrom<System.Object,System.Object,System.Object>
struct WithLatestFrom_t2869896360;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Operators.WithLatestFromObservable`3/WithLatestFrom/LeftObserver<System.Object,System.Object,System.Object>
struct  LeftObserver_t2250424592  : public Il2CppObject
{
public:
	// UniRx.Operators.WithLatestFromObservable`3/WithLatestFrom<TLeft,TRight,TResult> UniRx.Operators.WithLatestFromObservable`3/WithLatestFrom/LeftObserver::parent
	WithLatestFrom_t2869896360 * ___parent_0;

public:
	inline static int32_t get_offset_of_parent_0() { return static_cast<int32_t>(offsetof(LeftObserver_t2250424592, ___parent_0)); }
	inline WithLatestFrom_t2869896360 * get_parent_0() const { return ___parent_0; }
	inline WithLatestFrom_t2869896360 ** get_address_of_parent_0() { return &___parent_0; }
	inline void set_parent_0(WithLatestFrom_t2869896360 * value)
	{
		___parent_0 = value;
		Il2CppCodeGenWriteBarrier(&___parent_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
