﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// UnityEngine.WWWForm
struct WWWForm_t3999572776;
// System.Collections.Generic.Dictionary`2<System.String,System.String>
struct Dictionary_2_t2606186806;
// UniRx.IProgress`1<System.Single>
struct IProgress_1_t4173802291;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.ObservableWWW/<Post>c__AnonStorey84
struct  U3CPostU3Ec__AnonStorey84_t213725633  : public Il2CppObject
{
public:
	// System.String UniRx.ObservableWWW/<Post>c__AnonStorey84::url
	String_t* ___url_0;
	// UnityEngine.WWWForm UniRx.ObservableWWW/<Post>c__AnonStorey84::content
	WWWForm_t3999572776 * ___content_1;
	// System.Collections.Generic.Dictionary`2<System.String,System.String> UniRx.ObservableWWW/<Post>c__AnonStorey84::contentHeaders
	Dictionary_2_t2606186806 * ___contentHeaders_2;
	// System.Collections.Generic.Dictionary`2<System.String,System.String> UniRx.ObservableWWW/<Post>c__AnonStorey84::headers
	Dictionary_2_t2606186806 * ___headers_3;
	// UniRx.IProgress`1<System.Single> UniRx.ObservableWWW/<Post>c__AnonStorey84::progress
	Il2CppObject* ___progress_4;

public:
	inline static int32_t get_offset_of_url_0() { return static_cast<int32_t>(offsetof(U3CPostU3Ec__AnonStorey84_t213725633, ___url_0)); }
	inline String_t* get_url_0() const { return ___url_0; }
	inline String_t** get_address_of_url_0() { return &___url_0; }
	inline void set_url_0(String_t* value)
	{
		___url_0 = value;
		Il2CppCodeGenWriteBarrier(&___url_0, value);
	}

	inline static int32_t get_offset_of_content_1() { return static_cast<int32_t>(offsetof(U3CPostU3Ec__AnonStorey84_t213725633, ___content_1)); }
	inline WWWForm_t3999572776 * get_content_1() const { return ___content_1; }
	inline WWWForm_t3999572776 ** get_address_of_content_1() { return &___content_1; }
	inline void set_content_1(WWWForm_t3999572776 * value)
	{
		___content_1 = value;
		Il2CppCodeGenWriteBarrier(&___content_1, value);
	}

	inline static int32_t get_offset_of_contentHeaders_2() { return static_cast<int32_t>(offsetof(U3CPostU3Ec__AnonStorey84_t213725633, ___contentHeaders_2)); }
	inline Dictionary_2_t2606186806 * get_contentHeaders_2() const { return ___contentHeaders_2; }
	inline Dictionary_2_t2606186806 ** get_address_of_contentHeaders_2() { return &___contentHeaders_2; }
	inline void set_contentHeaders_2(Dictionary_2_t2606186806 * value)
	{
		___contentHeaders_2 = value;
		Il2CppCodeGenWriteBarrier(&___contentHeaders_2, value);
	}

	inline static int32_t get_offset_of_headers_3() { return static_cast<int32_t>(offsetof(U3CPostU3Ec__AnonStorey84_t213725633, ___headers_3)); }
	inline Dictionary_2_t2606186806 * get_headers_3() const { return ___headers_3; }
	inline Dictionary_2_t2606186806 ** get_address_of_headers_3() { return &___headers_3; }
	inline void set_headers_3(Dictionary_2_t2606186806 * value)
	{
		___headers_3 = value;
		Il2CppCodeGenWriteBarrier(&___headers_3, value);
	}

	inline static int32_t get_offset_of_progress_4() { return static_cast<int32_t>(offsetof(U3CPostU3Ec__AnonStorey84_t213725633, ___progress_4)); }
	inline Il2CppObject* get_progress_4() const { return ___progress_4; }
	inline Il2CppObject** get_address_of_progress_4() { return &___progress_4; }
	inline void set_progress_4(Il2CppObject* value)
	{
		___progress_4 = value;
		Il2CppCodeGenWriteBarrier(&___progress_4, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
