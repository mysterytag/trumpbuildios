﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Zenject.ResourceProvider/<ValidateBinding>c__Iterator4F
struct U3CValidateBindingU3Ec__Iterator4F_t3066593112;
// Zenject.ZenjectResolveException
struct ZenjectResolveException_t1201052999;
// System.Object
struct Il2CppObject;
// System.Collections.IEnumerator
struct IEnumerator_t287207039;
// System.Collections.Generic.IEnumerator`1<Zenject.ZenjectResolveException>
struct IEnumerator_1_t2684159447;

#include "codegen/il2cpp-codegen.h"

// System.Void Zenject.ResourceProvider/<ValidateBinding>c__Iterator4F::.ctor()
extern "C"  void U3CValidateBindingU3Ec__Iterator4F__ctor_m2393165410 (U3CValidateBindingU3Ec__Iterator4F_t3066593112 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Zenject.ZenjectResolveException Zenject.ResourceProvider/<ValidateBinding>c__Iterator4F::System.Collections.Generic.IEnumerator<Zenject.ZenjectResolveException>.get_Current()
extern "C"  ZenjectResolveException_t1201052999 * U3CValidateBindingU3Ec__Iterator4F_System_Collections_Generic_IEnumeratorU3CZenject_ZenjectResolveExceptionU3E_get_Current_m3854998379 (U3CValidateBindingU3Ec__Iterator4F_t3066593112 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Zenject.ResourceProvider/<ValidateBinding>c__Iterator4F::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CValidateBindingU3Ec__Iterator4F_System_Collections_IEnumerator_get_Current_m2292445380 (U3CValidateBindingU3Ec__Iterator4F_t3066593112 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator Zenject.ResourceProvider/<ValidateBinding>c__Iterator4F::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * U3CValidateBindingU3Ec__Iterator4F_System_Collections_IEnumerable_GetEnumerator_m473926335 (U3CValidateBindingU3Ec__Iterator4F_t3066593112 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.IEnumerator`1<Zenject.ZenjectResolveException> Zenject.ResourceProvider/<ValidateBinding>c__Iterator4F::System.Collections.Generic.IEnumerable<Zenject.ZenjectResolveException>.GetEnumerator()
extern "C"  Il2CppObject* U3CValidateBindingU3Ec__Iterator4F_System_Collections_Generic_IEnumerableU3CZenject_ZenjectResolveExceptionU3E_GetEnumerator_m2915701964 (U3CValidateBindingU3Ec__Iterator4F_t3066593112 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Zenject.ResourceProvider/<ValidateBinding>c__Iterator4F::MoveNext()
extern "C"  bool U3CValidateBindingU3Ec__Iterator4F_MoveNext_m4198660178 (U3CValidateBindingU3Ec__Iterator4F_t3066593112 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Zenject.ResourceProvider/<ValidateBinding>c__Iterator4F::Dispose()
extern "C"  void U3CValidateBindingU3Ec__Iterator4F_Dispose_m1922738719 (U3CValidateBindingU3Ec__Iterator4F_t3066593112 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Zenject.ResourceProvider/<ValidateBinding>c__Iterator4F::Reset()
extern "C"  void U3CValidateBindingU3Ec__Iterator4F_Reset_m39598351 (U3CValidateBindingU3Ec__Iterator4F_t3066593112 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
