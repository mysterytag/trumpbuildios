﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Observable/<ToAwaitableEnumerator>c__Iterator17`1<System.Object>
struct U3CToAwaitableEnumeratorU3Ec__Iterator17_1_t1120858664;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void UniRx.Observable/<ToAwaitableEnumerator>c__Iterator17`1<System.Object>::.ctor()
extern "C"  void U3CToAwaitableEnumeratorU3Ec__Iterator17_1__ctor_m3641051694_gshared (U3CToAwaitableEnumeratorU3Ec__Iterator17_1_t1120858664 * __this, const MethodInfo* method);
#define U3CToAwaitableEnumeratorU3Ec__Iterator17_1__ctor_m3641051694(__this, method) ((  void (*) (U3CToAwaitableEnumeratorU3Ec__Iterator17_1_t1120858664 *, const MethodInfo*))U3CToAwaitableEnumeratorU3Ec__Iterator17_1__ctor_m3641051694_gshared)(__this, method)
// System.Object UniRx.Observable/<ToAwaitableEnumerator>c__Iterator17`1<System.Object>::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CToAwaitableEnumeratorU3Ec__Iterator17_1_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3896765870_gshared (U3CToAwaitableEnumeratorU3Ec__Iterator17_1_t1120858664 * __this, const MethodInfo* method);
#define U3CToAwaitableEnumeratorU3Ec__Iterator17_1_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3896765870(__this, method) ((  Il2CppObject * (*) (U3CToAwaitableEnumeratorU3Ec__Iterator17_1_t1120858664 *, const MethodInfo*))U3CToAwaitableEnumeratorU3Ec__Iterator17_1_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3896765870_gshared)(__this, method)
// System.Object UniRx.Observable/<ToAwaitableEnumerator>c__Iterator17`1<System.Object>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CToAwaitableEnumeratorU3Ec__Iterator17_1_System_Collections_IEnumerator_get_Current_m2603814210_gshared (U3CToAwaitableEnumeratorU3Ec__Iterator17_1_t1120858664 * __this, const MethodInfo* method);
#define U3CToAwaitableEnumeratorU3Ec__Iterator17_1_System_Collections_IEnumerator_get_Current_m2603814210(__this, method) ((  Il2CppObject * (*) (U3CToAwaitableEnumeratorU3Ec__Iterator17_1_t1120858664 *, const MethodInfo*))U3CToAwaitableEnumeratorU3Ec__Iterator17_1_System_Collections_IEnumerator_get_Current_m2603814210_gshared)(__this, method)
// System.Boolean UniRx.Observable/<ToAwaitableEnumerator>c__Iterator17`1<System.Object>::MoveNext()
extern "C"  bool U3CToAwaitableEnumeratorU3Ec__Iterator17_1_MoveNext_m3369239254_gshared (U3CToAwaitableEnumeratorU3Ec__Iterator17_1_t1120858664 * __this, const MethodInfo* method);
#define U3CToAwaitableEnumeratorU3Ec__Iterator17_1_MoveNext_m3369239254(__this, method) ((  bool (*) (U3CToAwaitableEnumeratorU3Ec__Iterator17_1_t1120858664 *, const MethodInfo*))U3CToAwaitableEnumeratorU3Ec__Iterator17_1_MoveNext_m3369239254_gshared)(__this, method)
// System.Void UniRx.Observable/<ToAwaitableEnumerator>c__Iterator17`1<System.Object>::Dispose()
extern "C"  void U3CToAwaitableEnumeratorU3Ec__Iterator17_1_Dispose_m2845582059_gshared (U3CToAwaitableEnumeratorU3Ec__Iterator17_1_t1120858664 * __this, const MethodInfo* method);
#define U3CToAwaitableEnumeratorU3Ec__Iterator17_1_Dispose_m2845582059(__this, method) ((  void (*) (U3CToAwaitableEnumeratorU3Ec__Iterator17_1_t1120858664 *, const MethodInfo*))U3CToAwaitableEnumeratorU3Ec__Iterator17_1_Dispose_m2845582059_gshared)(__this, method)
// System.Void UniRx.Observable/<ToAwaitableEnumerator>c__Iterator17`1<System.Object>::Reset()
extern "C"  void U3CToAwaitableEnumeratorU3Ec__Iterator17_1_Reset_m1287484635_gshared (U3CToAwaitableEnumeratorU3Ec__Iterator17_1_t1120858664 * __this, const MethodInfo* method);
#define U3CToAwaitableEnumeratorU3Ec__Iterator17_1_Reset_m1287484635(__this, method) ((  void (*) (U3CToAwaitableEnumeratorU3Ec__Iterator17_1_t1120858664 *, const MethodInfo*))U3CToAwaitableEnumeratorU3Ec__Iterator17_1_Reset_m1287484635_gshared)(__this, method)
