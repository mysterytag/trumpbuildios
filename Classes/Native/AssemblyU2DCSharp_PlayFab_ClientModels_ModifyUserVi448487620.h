﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "AssemblyU2DCSharp_PlayFab_Internal_PlayFabResultCom379512675.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PlayFab.ClientModels.ModifyUserVirtualCurrencyResult
struct  ModifyUserVirtualCurrencyResult_t448487620  : public PlayFabResultCommon_t379512675
{
public:
	// System.String PlayFab.ClientModels.ModifyUserVirtualCurrencyResult::<PlayFabId>k__BackingField
	String_t* ___U3CPlayFabIdU3Ek__BackingField_2;
	// System.String PlayFab.ClientModels.ModifyUserVirtualCurrencyResult::<VirtualCurrency>k__BackingField
	String_t* ___U3CVirtualCurrencyU3Ek__BackingField_3;
	// System.Int32 PlayFab.ClientModels.ModifyUserVirtualCurrencyResult::<BalanceChange>k__BackingField
	int32_t ___U3CBalanceChangeU3Ek__BackingField_4;
	// System.Int32 PlayFab.ClientModels.ModifyUserVirtualCurrencyResult::<Balance>k__BackingField
	int32_t ___U3CBalanceU3Ek__BackingField_5;

public:
	inline static int32_t get_offset_of_U3CPlayFabIdU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(ModifyUserVirtualCurrencyResult_t448487620, ___U3CPlayFabIdU3Ek__BackingField_2)); }
	inline String_t* get_U3CPlayFabIdU3Ek__BackingField_2() const { return ___U3CPlayFabIdU3Ek__BackingField_2; }
	inline String_t** get_address_of_U3CPlayFabIdU3Ek__BackingField_2() { return &___U3CPlayFabIdU3Ek__BackingField_2; }
	inline void set_U3CPlayFabIdU3Ek__BackingField_2(String_t* value)
	{
		___U3CPlayFabIdU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier(&___U3CPlayFabIdU3Ek__BackingField_2, value);
	}

	inline static int32_t get_offset_of_U3CVirtualCurrencyU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(ModifyUserVirtualCurrencyResult_t448487620, ___U3CVirtualCurrencyU3Ek__BackingField_3)); }
	inline String_t* get_U3CVirtualCurrencyU3Ek__BackingField_3() const { return ___U3CVirtualCurrencyU3Ek__BackingField_3; }
	inline String_t** get_address_of_U3CVirtualCurrencyU3Ek__BackingField_3() { return &___U3CVirtualCurrencyU3Ek__BackingField_3; }
	inline void set_U3CVirtualCurrencyU3Ek__BackingField_3(String_t* value)
	{
		___U3CVirtualCurrencyU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier(&___U3CVirtualCurrencyU3Ek__BackingField_3, value);
	}

	inline static int32_t get_offset_of_U3CBalanceChangeU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(ModifyUserVirtualCurrencyResult_t448487620, ___U3CBalanceChangeU3Ek__BackingField_4)); }
	inline int32_t get_U3CBalanceChangeU3Ek__BackingField_4() const { return ___U3CBalanceChangeU3Ek__BackingField_4; }
	inline int32_t* get_address_of_U3CBalanceChangeU3Ek__BackingField_4() { return &___U3CBalanceChangeU3Ek__BackingField_4; }
	inline void set_U3CBalanceChangeU3Ek__BackingField_4(int32_t value)
	{
		___U3CBalanceChangeU3Ek__BackingField_4 = value;
	}

	inline static int32_t get_offset_of_U3CBalanceU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(ModifyUserVirtualCurrencyResult_t448487620, ___U3CBalanceU3Ek__BackingField_5)); }
	inline int32_t get_U3CBalanceU3Ek__BackingField_5() const { return ___U3CBalanceU3Ek__BackingField_5; }
	inline int32_t* get_address_of_U3CBalanceU3Ek__BackingField_5() { return &___U3CBalanceU3Ek__BackingField_5; }
	inline void set_U3CBalanceU3Ek__BackingField_5(int32_t value)
	{
		___U3CBalanceU3Ek__BackingField_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
