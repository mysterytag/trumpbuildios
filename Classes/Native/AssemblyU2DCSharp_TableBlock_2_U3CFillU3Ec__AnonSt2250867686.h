﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// CollectionRemoveEvent`1<System.Object>
struct CollectionRemoveEvent_1_t898259258;
// System.Object
struct Il2CppObject;
// TableBlock`2<System.Object,System.Object>
struct TableBlock_2_t3756399434;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TableBlock`2/<Fill>c__AnonStoreyEB<System.Object,System.Object>
struct  U3CFillU3Ec__AnonStoreyEB_t2250867686  : public Il2CppObject
{
public:
	// CollectionRemoveEvent`1<TData> TableBlock`2/<Fill>c__AnonStoreyEB::ev
	CollectionRemoveEvent_1_t898259258 * ___ev_0;
	// TPrefab TableBlock`2/<Fill>c__AnonStoreyEB::prefab
	Il2CppObject * ___prefab_1;
	// TableBlock`2<TData,TPrefab> TableBlock`2/<Fill>c__AnonStoreyEB::<>f__this
	TableBlock_2_t3756399434 * ___U3CU3Ef__this_2;

public:
	inline static int32_t get_offset_of_ev_0() { return static_cast<int32_t>(offsetof(U3CFillU3Ec__AnonStoreyEB_t2250867686, ___ev_0)); }
	inline CollectionRemoveEvent_1_t898259258 * get_ev_0() const { return ___ev_0; }
	inline CollectionRemoveEvent_1_t898259258 ** get_address_of_ev_0() { return &___ev_0; }
	inline void set_ev_0(CollectionRemoveEvent_1_t898259258 * value)
	{
		___ev_0 = value;
		Il2CppCodeGenWriteBarrier(&___ev_0, value);
	}

	inline static int32_t get_offset_of_prefab_1() { return static_cast<int32_t>(offsetof(U3CFillU3Ec__AnonStoreyEB_t2250867686, ___prefab_1)); }
	inline Il2CppObject * get_prefab_1() const { return ___prefab_1; }
	inline Il2CppObject ** get_address_of_prefab_1() { return &___prefab_1; }
	inline void set_prefab_1(Il2CppObject * value)
	{
		___prefab_1 = value;
		Il2CppCodeGenWriteBarrier(&___prefab_1, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__this_2() { return static_cast<int32_t>(offsetof(U3CFillU3Ec__AnonStoreyEB_t2250867686, ___U3CU3Ef__this_2)); }
	inline TableBlock_2_t3756399434 * get_U3CU3Ef__this_2() const { return ___U3CU3Ef__this_2; }
	inline TableBlock_2_t3756399434 ** get_address_of_U3CU3Ef__this_2() { return &___U3CU3Ef__this_2; }
	inline void set_U3CU3Ef__this_2(TableBlock_2_t3756399434 * value)
	{
		___U3CU3Ef__this_2 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__this_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
