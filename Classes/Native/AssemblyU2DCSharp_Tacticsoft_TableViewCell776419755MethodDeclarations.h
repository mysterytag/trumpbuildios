﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Tacticsoft.TableViewCell
struct TableViewCell_t776419755;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String968488902.h"

// System.Void Tacticsoft.TableViewCell::.ctor()
extern "C"  void TableViewCell__ctor_m3864253618 (TableViewCell_t776419755 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Tacticsoft.TableViewCell::get_reuseIdentifier()
extern "C"  String_t* TableViewCell_get_reuseIdentifier_m3234078081 (TableViewCell_t776419755 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Tacticsoft.TableViewCell::set_reuseIdentifier(System.String)
extern "C"  void TableViewCell_set_reuseIdentifier_m288145650 (TableViewCell_t776419755 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Tacticsoft.TableViewCell::Press()
extern "C"  void TableViewCell_Press_m95344979 (TableViewCell_t776419755 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
