﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.SingleObservable`1<System.Object>
struct SingleObservable_1_t427280954;
// UniRx.IObservable`1<System.Object>
struct IObservable_1_t595904784;
// System.Func`2<System.Object,System.Boolean>
struct Func_2_t1509682273;
// System.IDisposable
struct IDisposable_t1628921374;
// UniRx.IObserver`1<System.Object>
struct IObserver_1_t3049105323;

#include "codegen/il2cpp-codegen.h"

// System.Void UniRx.Operators.SingleObservable`1<System.Object>::.ctor(UniRx.IObservable`1<T>,System.Boolean)
extern "C"  void SingleObservable_1__ctor_m3653257072_gshared (SingleObservable_1_t427280954 * __this, Il2CppObject* ___source0, bool ___useDefault1, const MethodInfo* method);
#define SingleObservable_1__ctor_m3653257072(__this, ___source0, ___useDefault1, method) ((  void (*) (SingleObservable_1_t427280954 *, Il2CppObject*, bool, const MethodInfo*))SingleObservable_1__ctor_m3653257072_gshared)(__this, ___source0, ___useDefault1, method)
// System.Void UniRx.Operators.SingleObservable`1<System.Object>::.ctor(UniRx.IObservable`1<T>,System.Func`2<T,System.Boolean>,System.Boolean)
extern "C"  void SingleObservable_1__ctor_m1677592730_gshared (SingleObservable_1_t427280954 * __this, Il2CppObject* ___source0, Func_2_t1509682273 * ___predicate1, bool ___useDefault2, const MethodInfo* method);
#define SingleObservable_1__ctor_m1677592730(__this, ___source0, ___predicate1, ___useDefault2, method) ((  void (*) (SingleObservable_1_t427280954 *, Il2CppObject*, Func_2_t1509682273 *, bool, const MethodInfo*))SingleObservable_1__ctor_m1677592730_gshared)(__this, ___source0, ___predicate1, ___useDefault2, method)
// System.IDisposable UniRx.Operators.SingleObservable`1<System.Object>::SubscribeCore(UniRx.IObserver`1<T>,System.IDisposable)
extern "C"  Il2CppObject * SingleObservable_1_SubscribeCore_m2139713338_gshared (SingleObservable_1_t427280954 * __this, Il2CppObject* ___observer0, Il2CppObject * ___cancel1, const MethodInfo* method);
#define SingleObservable_1_SubscribeCore_m2139713338(__this, ___observer0, ___cancel1, method) ((  Il2CppObject * (*) (SingleObservable_1_t427280954 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))SingleObservable_1_SubscribeCore_m2139713338_gshared)(__this, ___observer0, ___cancel1, method)
