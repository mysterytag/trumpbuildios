﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// UnityEngine.Sprite
struct Sprite_t4006040370;
// System.Action`1<SettingsCell>
struct Action_1_t846625190;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SettingsButton
struct  SettingsButton_t915256661  : public Il2CppObject
{
public:
	// System.String SettingsButton::Name
	String_t* ___Name_0;
	// System.String SettingsButton::TitleTranslateId
	String_t* ___TitleTranslateId_1;
	// System.String SettingsButton::SoloTextTranslateId
	String_t* ___SoloTextTranslateId_2;
	// System.String SettingsButton::DescriptionTranslateId
	String_t* ___DescriptionTranslateId_3;
	// UnityEngine.Sprite SettingsButton::SpriteIcon
	Sprite_t4006040370 * ___SpriteIcon_4;
	// System.Action`1<SettingsCell> SettingsButton::InitializeCellSet
	Action_1_t846625190 * ___InitializeCellSet_5;

public:
	inline static int32_t get_offset_of_Name_0() { return static_cast<int32_t>(offsetof(SettingsButton_t915256661, ___Name_0)); }
	inline String_t* get_Name_0() const { return ___Name_0; }
	inline String_t** get_address_of_Name_0() { return &___Name_0; }
	inline void set_Name_0(String_t* value)
	{
		___Name_0 = value;
		Il2CppCodeGenWriteBarrier(&___Name_0, value);
	}

	inline static int32_t get_offset_of_TitleTranslateId_1() { return static_cast<int32_t>(offsetof(SettingsButton_t915256661, ___TitleTranslateId_1)); }
	inline String_t* get_TitleTranslateId_1() const { return ___TitleTranslateId_1; }
	inline String_t** get_address_of_TitleTranslateId_1() { return &___TitleTranslateId_1; }
	inline void set_TitleTranslateId_1(String_t* value)
	{
		___TitleTranslateId_1 = value;
		Il2CppCodeGenWriteBarrier(&___TitleTranslateId_1, value);
	}

	inline static int32_t get_offset_of_SoloTextTranslateId_2() { return static_cast<int32_t>(offsetof(SettingsButton_t915256661, ___SoloTextTranslateId_2)); }
	inline String_t* get_SoloTextTranslateId_2() const { return ___SoloTextTranslateId_2; }
	inline String_t** get_address_of_SoloTextTranslateId_2() { return &___SoloTextTranslateId_2; }
	inline void set_SoloTextTranslateId_2(String_t* value)
	{
		___SoloTextTranslateId_2 = value;
		Il2CppCodeGenWriteBarrier(&___SoloTextTranslateId_2, value);
	}

	inline static int32_t get_offset_of_DescriptionTranslateId_3() { return static_cast<int32_t>(offsetof(SettingsButton_t915256661, ___DescriptionTranslateId_3)); }
	inline String_t* get_DescriptionTranslateId_3() const { return ___DescriptionTranslateId_3; }
	inline String_t** get_address_of_DescriptionTranslateId_3() { return &___DescriptionTranslateId_3; }
	inline void set_DescriptionTranslateId_3(String_t* value)
	{
		___DescriptionTranslateId_3 = value;
		Il2CppCodeGenWriteBarrier(&___DescriptionTranslateId_3, value);
	}

	inline static int32_t get_offset_of_SpriteIcon_4() { return static_cast<int32_t>(offsetof(SettingsButton_t915256661, ___SpriteIcon_4)); }
	inline Sprite_t4006040370 * get_SpriteIcon_4() const { return ___SpriteIcon_4; }
	inline Sprite_t4006040370 ** get_address_of_SpriteIcon_4() { return &___SpriteIcon_4; }
	inline void set_SpriteIcon_4(Sprite_t4006040370 * value)
	{
		___SpriteIcon_4 = value;
		Il2CppCodeGenWriteBarrier(&___SpriteIcon_4, value);
	}

	inline static int32_t get_offset_of_InitializeCellSet_5() { return static_cast<int32_t>(offsetof(SettingsButton_t915256661, ___InitializeCellSet_5)); }
	inline Action_1_t846625190 * get_InitializeCellSet_5() const { return ___InitializeCellSet_5; }
	inline Action_1_t846625190 ** get_address_of_InitializeCellSet_5() { return &___InitializeCellSet_5; }
	inline void set_InitializeCellSet_5(Action_1_t846625190 * value)
	{
		___InitializeCellSet_5 = value;
		Il2CppCodeGenWriteBarrier(&___InitializeCellSet_5, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
