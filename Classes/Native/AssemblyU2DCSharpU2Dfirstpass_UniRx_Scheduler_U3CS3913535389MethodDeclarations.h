﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Scheduler/<Schedule>c__AnonStorey77/<Schedule>c__AnonStorey78
struct U3CScheduleU3Ec__AnonStorey78_t3913535389;

#include "codegen/il2cpp-codegen.h"

// System.Void UniRx.Scheduler/<Schedule>c__AnonStorey77/<Schedule>c__AnonStorey78::.ctor()
extern "C"  void U3CScheduleU3Ec__AnonStorey78__ctor_m1699716921 (U3CScheduleU3Ec__AnonStorey78_t3913535389 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.Scheduler/<Schedule>c__AnonStorey77/<Schedule>c__AnonStorey78::<>m__9F()
extern "C"  void U3CScheduleU3Ec__AnonStorey78_U3CU3Em__9F_m3648688047 (U3CScheduleU3Ec__AnonStorey78_t3913535389 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
