﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.PlayFabClientAPI/<UnlinkPSNAccount>c__AnonStorey87
struct U3CUnlinkPSNAccountU3Ec__AnonStorey87_t1880070265;
// PlayFab.CallRequestContainer
struct CallRequestContainer_t432318609;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharpU2Dfirstpass_PlayFab_CallRequestCo432318609.h"

// System.Void PlayFab.PlayFabClientAPI/<UnlinkPSNAccount>c__AnonStorey87::.ctor()
extern "C"  void U3CUnlinkPSNAccountU3Ec__AnonStorey87__ctor_m4274531162 (U3CUnlinkPSNAccountU3Ec__AnonStorey87_t1880070265 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/<UnlinkPSNAccount>c__AnonStorey87::<>m__74(PlayFab.CallRequestContainer)
extern "C"  void U3CUnlinkPSNAccountU3Ec__AnonStorey87_U3CU3Em__74_m4213360341 (U3CUnlinkPSNAccountU3Ec__AnonStorey87_t1880070265 * __this, CallRequestContainer_t432318609 * ___requestContainer0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
