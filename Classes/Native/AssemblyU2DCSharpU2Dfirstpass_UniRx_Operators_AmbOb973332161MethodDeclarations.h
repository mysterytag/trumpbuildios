﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.AmbObservable`1/AmbOuterObserver/AmbDecisionObserver<System.Object>
struct AmbDecisionObserver_t973332161;
// UniRx.Operators.AmbObservable`1/AmbOuterObserver<System.Object>
struct AmbOuterObserver_t2827192636;
// System.IDisposable
struct IDisposable_t1628921374;
// UniRx.Operators.AmbObservable`1/AmbOuterObserver/Amb<System.Object>
struct Amb_t634706591;
// System.Object
struct Il2CppObject;
// System.Exception
struct Exception_t1967233988;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_AmbO2663999068.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_Exception1967233988.h"

// System.Void UniRx.Operators.AmbObservable`1/AmbOuterObserver/AmbDecisionObserver<System.Object>::.ctor(UniRx.Operators.AmbObservable`1/AmbOuterObserver<T>,UniRx.Operators.AmbObservable`1/AmbOuterObserver/AmbState<T>,System.IDisposable,UniRx.Operators.AmbObservable`1/AmbOuterObserver/Amb<T>)
extern "C"  void AmbDecisionObserver__ctor_m347170949_gshared (AmbDecisionObserver_t973332161 * __this, AmbOuterObserver_t2827192636 * ___parent0, int32_t ___me1, Il2CppObject * ___otherSubscription2, Amb_t634706591 * ___self3, const MethodInfo* method);
#define AmbDecisionObserver__ctor_m347170949(__this, ___parent0, ___me1, ___otherSubscription2, ___self3, method) ((  void (*) (AmbDecisionObserver_t973332161 *, AmbOuterObserver_t2827192636 *, int32_t, Il2CppObject *, Amb_t634706591 *, const MethodInfo*))AmbDecisionObserver__ctor_m347170949_gshared)(__this, ___parent0, ___me1, ___otherSubscription2, ___self3, method)
// System.Void UniRx.Operators.AmbObservable`1/AmbOuterObserver/AmbDecisionObserver<System.Object>::OnNext(T)
extern "C"  void AmbDecisionObserver_OnNext_m4246898483_gshared (AmbDecisionObserver_t973332161 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define AmbDecisionObserver_OnNext_m4246898483(__this, ___value0, method) ((  void (*) (AmbDecisionObserver_t973332161 *, Il2CppObject *, const MethodInfo*))AmbDecisionObserver_OnNext_m4246898483_gshared)(__this, ___value0, method)
// System.Void UniRx.Operators.AmbObservable`1/AmbOuterObserver/AmbDecisionObserver<System.Object>::OnError(System.Exception)
extern "C"  void AmbDecisionObserver_OnError_m173005890_gshared (AmbDecisionObserver_t973332161 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method);
#define AmbDecisionObserver_OnError_m173005890(__this, ___error0, method) ((  void (*) (AmbDecisionObserver_t973332161 *, Exception_t1967233988 *, const MethodInfo*))AmbDecisionObserver_OnError_m173005890_gshared)(__this, ___error0, method)
// System.Void UniRx.Operators.AmbObservable`1/AmbOuterObserver/AmbDecisionObserver<System.Object>::OnCompleted()
extern "C"  void AmbDecisionObserver_OnCompleted_m1650811541_gshared (AmbDecisionObserver_t973332161 * __this, const MethodInfo* method);
#define AmbDecisionObserver_OnCompleted_m1650811541(__this, method) ((  void (*) (AmbDecisionObserver_t973332161 *, const MethodInfo*))AmbDecisionObserver_OnCompleted_m1650811541_gshared)(__this, method)
