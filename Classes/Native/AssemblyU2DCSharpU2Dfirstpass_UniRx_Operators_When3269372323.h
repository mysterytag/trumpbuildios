﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UniRx.Operators.WhenAllObservable`1/WhenAll<UniRx.Tuple`2<System.Object,System.Object>>
struct WhenAll_t491399631;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Operators.WhenAllObservable`1/WhenAll/WhenAllCollectionObserver<UniRx.Tuple`2<System.Object,System.Object>>
struct  WhenAllCollectionObserver_t3269372323  : public Il2CppObject
{
public:
	// UniRx.Operators.WhenAllObservable`1/WhenAll<T> UniRx.Operators.WhenAllObservable`1/WhenAll/WhenAllCollectionObserver::parent
	WhenAll_t491399631 * ___parent_0;
	// System.Int32 UniRx.Operators.WhenAllObservable`1/WhenAll/WhenAllCollectionObserver::index
	int32_t ___index_1;
	// System.Boolean UniRx.Operators.WhenAllObservable`1/WhenAll/WhenAllCollectionObserver::isCompleted
	bool ___isCompleted_2;

public:
	inline static int32_t get_offset_of_parent_0() { return static_cast<int32_t>(offsetof(WhenAllCollectionObserver_t3269372323, ___parent_0)); }
	inline WhenAll_t491399631 * get_parent_0() const { return ___parent_0; }
	inline WhenAll_t491399631 ** get_address_of_parent_0() { return &___parent_0; }
	inline void set_parent_0(WhenAll_t491399631 * value)
	{
		___parent_0 = value;
		Il2CppCodeGenWriteBarrier(&___parent_0, value);
	}

	inline static int32_t get_offset_of_index_1() { return static_cast<int32_t>(offsetof(WhenAllCollectionObserver_t3269372323, ___index_1)); }
	inline int32_t get_index_1() const { return ___index_1; }
	inline int32_t* get_address_of_index_1() { return &___index_1; }
	inline void set_index_1(int32_t value)
	{
		___index_1 = value;
	}

	inline static int32_t get_offset_of_isCompleted_2() { return static_cast<int32_t>(offsetof(WhenAllCollectionObserver_t3269372323, ___isCompleted_2)); }
	inline bool get_isCompleted_2() const { return ___isCompleted_2; }
	inline bool* get_address_of_isCompleted_2() { return &___isCompleted_2; }
	inline void set_isCompleted_2(bool value)
	{
		___isCompleted_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
