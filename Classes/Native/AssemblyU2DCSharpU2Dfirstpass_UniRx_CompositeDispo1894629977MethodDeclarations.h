﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.CompositeDisposable
struct CompositeDisposable_t1894629977;
// System.IDisposable[]
struct IDisposableU5BU5D_t165183403;
// System.Collections.Generic.IEnumerable`1<System.IDisposable>
struct IEnumerable_1_t206108434;
// System.Collections.IEnumerator
struct IEnumerator_t287207039;
// System.IDisposable
struct IDisposable_t1628921374;
// System.Collections.Generic.IEnumerator`1<System.IDisposable>
struct IEnumerator_1_t3112027822;

#include "codegen/il2cpp-codegen.h"

// System.Void UniRx.CompositeDisposable::.ctor()
extern "C"  void CompositeDisposable__ctor_m1237994920 (CompositeDisposable_t1894629977 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.CompositeDisposable::.ctor(System.Int32)
extern "C"  void CompositeDisposable__ctor_m4031301369 (CompositeDisposable_t1894629977 * __this, int32_t ___capacity0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.CompositeDisposable::.ctor(System.IDisposable[])
extern "C"  void CompositeDisposable__ctor_m654163804 (CompositeDisposable_t1894629977 * __this, IDisposableU5BU5D_t165183403* ___disposables0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.CompositeDisposable::.ctor(System.Collections.Generic.IEnumerable`1<System.IDisposable>)
extern "C"  void CompositeDisposable__ctor_m2139080381 (CompositeDisposable_t1894629977 * __this, Il2CppObject* ___disposables0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator UniRx.CompositeDisposable::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * CompositeDisposable_System_Collections_IEnumerable_GetEnumerator_m204028113 (CompositeDisposable_t1894629977 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UniRx.CompositeDisposable::get_Count()
extern "C"  int32_t CompositeDisposable_get_Count_m1817945890 (CompositeDisposable_t1894629977 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.CompositeDisposable::Add(System.IDisposable)
extern "C"  void CompositeDisposable_Add_m2171552957 (CompositeDisposable_t1894629977 * __this, Il2CppObject * ___item0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UniRx.CompositeDisposable::Remove(System.IDisposable)
extern "C"  bool CompositeDisposable_Remove_m495331162 (CompositeDisposable_t1894629977 * __this, Il2CppObject * ___item0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.CompositeDisposable::Dispose()
extern "C"  void CompositeDisposable_Dispose_m4200427493 (CompositeDisposable_t1894629977 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.CompositeDisposable::Clear()
extern "C"  void CompositeDisposable_Clear_m2939095507 (CompositeDisposable_t1894629977 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UniRx.CompositeDisposable::Contains(System.IDisposable)
extern "C"  bool CompositeDisposable_Contains_m2190427573 (CompositeDisposable_t1894629977 * __this, Il2CppObject * ___item0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.CompositeDisposable::CopyTo(System.IDisposable[],System.Int32)
extern "C"  void CompositeDisposable_CopyTo_m3492652151 (CompositeDisposable_t1894629977 * __this, IDisposableU5BU5D_t165183403* ___array0, int32_t ___arrayIndex1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UniRx.CompositeDisposable::get_IsReadOnly()
extern "C"  bool CompositeDisposable_get_IsReadOnly_m1220575925 (CompositeDisposable_t1894629977 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.IEnumerator`1<System.IDisposable> UniRx.CompositeDisposable::GetEnumerator()
extern "C"  Il2CppObject* CompositeDisposable_GetEnumerator_m1298705342 (CompositeDisposable_t1894629977 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UniRx.CompositeDisposable::get_IsDisposed()
extern "C"  bool CompositeDisposable_get_IsDisposed_m1028393400 (CompositeDisposable_t1894629977 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
