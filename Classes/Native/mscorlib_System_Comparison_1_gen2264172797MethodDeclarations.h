﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Comparison_1_gen3540781296MethodDeclarations.h"

// System.Void System.Comparison`1<WindowBase>::.ctor(System.Object,System.IntPtr)
#define Comparison_1__ctor_m3437622728(__this, ___object0, ___method1, method) ((  void (*) (Comparison_1_t2264172797 *, Il2CppObject *, IntPtr_t, const MethodInfo*))Comparison_1__ctor_m487232819_gshared)(__this, ___object0, ___method1, method)
// System.Int32 System.Comparison`1<WindowBase>::Invoke(T,T)
#define Comparison_1_Invoke_m3397764656(__this, ___x0, ___y1, method) ((  int32_t (*) (Comparison_1_t2264172797 *, WindowBase_t3855465217 *, WindowBase_t3855465217 *, const MethodInfo*))Comparison_1_Invoke_m1888033133_gshared)(__this, ___x0, ___y1, method)
// System.IAsyncResult System.Comparison`1<WindowBase>::BeginInvoke(T,T,System.AsyncCallback,System.Object)
#define Comparison_1_BeginInvoke_m1869247865(__this, ___x0, ___y1, ___callback2, ___object3, method) ((  Il2CppObject * (*) (Comparison_1_t2264172797 *, WindowBase_t3855465217 *, WindowBase_t3855465217 *, AsyncCallback_t1363551830 *, Il2CppObject *, const MethodInfo*))Comparison_1_BeginInvoke_m3177996774_gshared)(__this, ___x0, ___y1, ___callback2, ___object3, method)
// System.Int32 System.Comparison`1<WindowBase>::EndInvoke(System.IAsyncResult)
#define Comparison_1_EndInvoke_m1604836412(__this, ___result0, method) ((  int32_t (*) (Comparison_1_t2264172797 *, Il2CppObject *, const MethodInfo*))Comparison_1_EndInvoke_m651541983_gshared)(__this, ___result0, method)
