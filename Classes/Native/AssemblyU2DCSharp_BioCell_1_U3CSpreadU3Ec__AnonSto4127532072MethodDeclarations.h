﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// BioCell`1/<Spread>c__AnonStoreyF5<System.Object>
struct U3CSpreadU3Ec__AnonStoreyF5_t4127532072;

#include "codegen/il2cpp-codegen.h"

// System.Void BioCell`1/<Spread>c__AnonStoreyF5<System.Object>::.ctor()
extern "C"  void U3CSpreadU3Ec__AnonStoreyF5__ctor_m1461531314_gshared (U3CSpreadU3Ec__AnonStoreyF5_t4127532072 * __this, const MethodInfo* method);
#define U3CSpreadU3Ec__AnonStoreyF5__ctor_m1461531314(__this, method) ((  void (*) (U3CSpreadU3Ec__AnonStoreyF5_t4127532072 *, const MethodInfo*))U3CSpreadU3Ec__AnonStoreyF5__ctor_m1461531314_gshared)(__this, method)
// System.Void BioCell`1/<Spread>c__AnonStoreyF5<System.Object>::<>m__164()
extern "C"  void U3CSpreadU3Ec__AnonStoreyF5_U3CU3Em__164_m930881334_gshared (U3CSpreadU3Ec__AnonStoreyF5_t4127532072 * __this, const MethodInfo* method);
#define U3CSpreadU3Ec__AnonStoreyF5_U3CU3Em__164_m930881334(__this, method) ((  void (*) (U3CSpreadU3Ec__AnonStoreyF5_t4127532072 *, const MethodInfo*))U3CSpreadU3Ec__AnonStoreyF5_U3CU3Em__164_m930881334_gshared)(__this, method)
