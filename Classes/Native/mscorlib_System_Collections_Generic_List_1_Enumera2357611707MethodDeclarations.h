﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera4014815677MethodDeclarations.h"

// System.Void System.Collections.Generic.List`1/Enumerator<PlayFab.ClientModels.RegionInfo>::.ctor(System.Collections.Generic.List`1<T>)
#define Enumerator__ctor_m779966919(__this, ___l0, method) ((  void (*) (Enumerator_t2357611707 *, List_1_t4271828715 *, const MethodInfo*))Enumerator__ctor_m1029849669_gshared)(__this, ___l0, method)
// System.Void System.Collections.Generic.List`1/Enumerator<PlayFab.ClientModels.RegionInfo>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m1179941419(__this, method) ((  void (*) (Enumerator_t2357611707 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m771996397_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1/Enumerator<PlayFab.ClientModels.RegionInfo>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m540170839(__this, method) ((  Il2CppObject * (*) (Enumerator_t2357611707 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m3561903705_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<PlayFab.ClientModels.RegionInfo>::Dispose()
#define Enumerator_Dispose_m845908332(__this, method) ((  void (*) (Enumerator_t2357611707 *, const MethodInfo*))Enumerator_Dispose_m2904289642_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<PlayFab.ClientModels.RegionInfo>::VerifyState()
#define Enumerator_VerifyState_m4272206501(__this, method) ((  void (*) (Enumerator_t2357611707 *, const MethodInfo*))Enumerator_VerifyState_m1522854819_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<PlayFab.ClientModels.RegionInfo>::MoveNext()
#define Enumerator_MoveNext_m1266087191(__this, method) ((  bool (*) (Enumerator_t2357611707 *, const MethodInfo*))Enumerator_MoveNext_m302222300_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<PlayFab.ClientModels.RegionInfo>::get_Current()
#define Enumerator_get_Current_m759097564(__this, method) ((  RegionInfo_t3474869746 * (*) (Enumerator_t2357611707 *, const MethodInfo*))Enumerator_get_Current_m1113061044_gshared)(__this, method)
