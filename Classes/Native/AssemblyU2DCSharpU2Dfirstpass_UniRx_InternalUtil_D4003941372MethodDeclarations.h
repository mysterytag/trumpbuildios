﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.InternalUtil.DisposedObserver`1<System.Double>
struct DisposedObserver_1_t4003941372;
// System.Exception
struct Exception_t1967233988;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Exception1967233988.h"

// System.Void UniRx.InternalUtil.DisposedObserver`1<System.Double>::.ctor()
extern "C"  void DisposedObserver_1__ctor_m4150059240_gshared (DisposedObserver_1_t4003941372 * __this, const MethodInfo* method);
#define DisposedObserver_1__ctor_m4150059240(__this, method) ((  void (*) (DisposedObserver_1_t4003941372 *, const MethodInfo*))DisposedObserver_1__ctor_m4150059240_gshared)(__this, method)
// System.Void UniRx.InternalUtil.DisposedObserver`1<System.Double>::.cctor()
extern "C"  void DisposedObserver_1__cctor_m3615688645_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define DisposedObserver_1__cctor_m3615688645(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))DisposedObserver_1__cctor_m3615688645_gshared)(__this /* static, unused */, method)
// System.Void UniRx.InternalUtil.DisposedObserver`1<System.Double>::OnCompleted()
extern "C"  void DisposedObserver_1_OnCompleted_m3620062834_gshared (DisposedObserver_1_t4003941372 * __this, const MethodInfo* method);
#define DisposedObserver_1_OnCompleted_m3620062834(__this, method) ((  void (*) (DisposedObserver_1_t4003941372 *, const MethodInfo*))DisposedObserver_1_OnCompleted_m3620062834_gshared)(__this, method)
// System.Void UniRx.InternalUtil.DisposedObserver`1<System.Double>::OnError(System.Exception)
extern "C"  void DisposedObserver_1_OnError_m38821023_gshared (DisposedObserver_1_t4003941372 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method);
#define DisposedObserver_1_OnError_m38821023(__this, ___error0, method) ((  void (*) (DisposedObserver_1_t4003941372 *, Exception_t1967233988 *, const MethodInfo*))DisposedObserver_1_OnError_m38821023_gshared)(__this, ___error0, method)
// System.Void UniRx.InternalUtil.DisposedObserver`1<System.Double>::OnNext(T)
extern "C"  void DisposedObserver_1_OnNext_m1013014928_gshared (DisposedObserver_1_t4003941372 * __this, double ___value0, const MethodInfo* method);
#define DisposedObserver_1_OnNext_m1013014928(__this, ___value0, method) ((  void (*) (DisposedObserver_1_t4003941372 *, double, const MethodInfo*))DisposedObserver_1_OnNext_m1013014928_gshared)(__this, ___value0, method)
