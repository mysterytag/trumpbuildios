﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.ClientModels.LoginWithEmailAddressRequest
struct LoginWithEmailAddressRequest_t3586495128;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String968488902.h"

// System.Void PlayFab.ClientModels.LoginWithEmailAddressRequest::.ctor()
extern "C"  void LoginWithEmailAddressRequest__ctor_m2261752229 (LoginWithEmailAddressRequest_t3586495128 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PlayFab.ClientModels.LoginWithEmailAddressRequest::get_TitleId()
extern "C"  String_t* LoginWithEmailAddressRequest_get_TitleId_m2367744656 (LoginWithEmailAddressRequest_t3586495128 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.LoginWithEmailAddressRequest::set_TitleId(System.String)
extern "C"  void LoginWithEmailAddressRequest_set_TitleId_m3250033961 (LoginWithEmailAddressRequest_t3586495128 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PlayFab.ClientModels.LoginWithEmailAddressRequest::get_Email()
extern "C"  String_t* LoginWithEmailAddressRequest_get_Email_m3358843225 (LoginWithEmailAddressRequest_t3586495128 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.LoginWithEmailAddressRequest::set_Email(System.String)
extern "C"  void LoginWithEmailAddressRequest_set_Email_m2923687616 (LoginWithEmailAddressRequest_t3586495128 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PlayFab.ClientModels.LoginWithEmailAddressRequest::get_Password()
extern "C"  String_t* LoginWithEmailAddressRequest_get_Password_m3472069536 (LoginWithEmailAddressRequest_t3586495128 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.LoginWithEmailAddressRequest::set_Password(System.String)
extern "C"  void LoginWithEmailAddressRequest_set_Password_m4285566859 (LoginWithEmailAddressRequest_t3586495128 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
