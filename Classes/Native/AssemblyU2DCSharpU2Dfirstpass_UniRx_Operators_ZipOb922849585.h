﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UniRx.Operators.ZipObservable`3/Zip<System.Object,System.Object,System.Object>
struct Zip_t2358317490;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Operators.ZipObservable`3/Zip/LeftZipObserver<System.Object,System.Object,System.Object>
struct  LeftZipObserver_t922849585  : public Il2CppObject
{
public:
	// UniRx.Operators.ZipObservable`3/Zip<TLeft,TRight,TResult> UniRx.Operators.ZipObservable`3/Zip/LeftZipObserver::parent
	Zip_t2358317490 * ___parent_0;

public:
	inline static int32_t get_offset_of_parent_0() { return static_cast<int32_t>(offsetof(LeftZipObserver_t922849585, ___parent_0)); }
	inline Zip_t2358317490 * get_parent_0() const { return ___parent_0; }
	inline Zip_t2358317490 ** get_address_of_parent_0() { return &___parent_0; }
	inline void set_parent_0(Zip_t2358317490 * value)
	{
		___parent_0 = value;
		Il2CppCodeGenWriteBarrier(&___parent_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
