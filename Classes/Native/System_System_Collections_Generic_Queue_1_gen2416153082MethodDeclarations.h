﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Queue`1<UniRx.TimeInterval`1<System.Object>>
struct Queue_1_t2416153082;
// System.Array
struct Il2CppArray;
// System.Object
struct Il2CppObject;
// System.Collections.Generic.IEnumerator`1<UniRx.TimeInterval`1<System.Object>>
struct IEnumerator_1_t2191171990;
// System.Collections.IEnumerator
struct IEnumerator_t287207039;
// UniRx.TimeInterval`1<System.Object>[]
struct TimeInterval_1U5BU5D_t2124321827;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array2840145358.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_TimeInterval_1_708065542.h"
#include "System_System_Collections_Generic_Queue_1_Enumerat3885774799.h"

// System.Void System.Collections.Generic.Queue`1<UniRx.TimeInterval`1<System.Object>>::.ctor()
extern "C"  void Queue_1__ctor_m19953764_gshared (Queue_1_t2416153082 * __this, const MethodInfo* method);
#define Queue_1__ctor_m19953764(__this, method) ((  void (*) (Queue_1_t2416153082 *, const MethodInfo*))Queue_1__ctor_m19953764_gshared)(__this, method)
// System.Void System.Collections.Generic.Queue`1<UniRx.TimeInterval`1<System.Object>>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void Queue_1_System_Collections_ICollection_CopyTo_m1871752544_gshared (Queue_1_t2416153082 * __this, Il2CppArray * ___array0, int32_t ___idx1, const MethodInfo* method);
#define Queue_1_System_Collections_ICollection_CopyTo_m1871752544(__this, ___array0, ___idx1, method) ((  void (*) (Queue_1_t2416153082 *, Il2CppArray *, int32_t, const MethodInfo*))Queue_1_System_Collections_ICollection_CopyTo_m1871752544_gshared)(__this, ___array0, ___idx1, method)
// System.Boolean System.Collections.Generic.Queue`1<UniRx.TimeInterval`1<System.Object>>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool Queue_1_System_Collections_ICollection_get_IsSynchronized_m2030547934_gshared (Queue_1_t2416153082 * __this, const MethodInfo* method);
#define Queue_1_System_Collections_ICollection_get_IsSynchronized_m2030547934(__this, method) ((  bool (*) (Queue_1_t2416153082 *, const MethodInfo*))Queue_1_System_Collections_ICollection_get_IsSynchronized_m2030547934_gshared)(__this, method)
// System.Object System.Collections.Generic.Queue`1<UniRx.TimeInterval`1<System.Object>>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * Queue_1_System_Collections_ICollection_get_SyncRoot_m346202428_gshared (Queue_1_t2416153082 * __this, const MethodInfo* method);
#define Queue_1_System_Collections_ICollection_get_SyncRoot_m346202428(__this, method) ((  Il2CppObject * (*) (Queue_1_t2416153082 *, const MethodInfo*))Queue_1_System_Collections_ICollection_get_SyncRoot_m346202428_gshared)(__this, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.Queue`1<UniRx.TimeInterval`1<System.Object>>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
extern "C"  Il2CppObject* Queue_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m2401491310_gshared (Queue_1_t2416153082 * __this, const MethodInfo* method);
#define Queue_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m2401491310(__this, method) ((  Il2CppObject* (*) (Queue_1_t2416153082 *, const MethodInfo*))Queue_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m2401491310_gshared)(__this, method)
// System.Collections.IEnumerator System.Collections.Generic.Queue`1<UniRx.TimeInterval`1<System.Object>>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * Queue_1_System_Collections_IEnumerable_GetEnumerator_m3798209627_gshared (Queue_1_t2416153082 * __this, const MethodInfo* method);
#define Queue_1_System_Collections_IEnumerable_GetEnumerator_m3798209627(__this, method) ((  Il2CppObject * (*) (Queue_1_t2416153082 *, const MethodInfo*))Queue_1_System_Collections_IEnumerable_GetEnumerator_m3798209627_gshared)(__this, method)
// System.Void System.Collections.Generic.Queue`1<UniRx.TimeInterval`1<System.Object>>::Clear()
extern "C"  void Queue_1_Clear_m1721054351_gshared (Queue_1_t2416153082 * __this, const MethodInfo* method);
#define Queue_1_Clear_m1721054351(__this, method) ((  void (*) (Queue_1_t2416153082 *, const MethodInfo*))Queue_1_Clear_m1721054351_gshared)(__this, method)
// System.Void System.Collections.Generic.Queue`1<UniRx.TimeInterval`1<System.Object>>::CopyTo(T[],System.Int32)
extern "C"  void Queue_1_CopyTo_m3251520203_gshared (Queue_1_t2416153082 * __this, TimeInterval_1U5BU5D_t2124321827* ___array0, int32_t ___idx1, const MethodInfo* method);
#define Queue_1_CopyTo_m3251520203(__this, ___array0, ___idx1, method) ((  void (*) (Queue_1_t2416153082 *, TimeInterval_1U5BU5D_t2124321827*, int32_t, const MethodInfo*))Queue_1_CopyTo_m3251520203_gshared)(__this, ___array0, ___idx1, method)
// T System.Collections.Generic.Queue`1<UniRx.TimeInterval`1<System.Object>>::Dequeue()
extern "C"  TimeInterval_1_t708065542  Queue_1_Dequeue_m3739117297_gshared (Queue_1_t2416153082 * __this, const MethodInfo* method);
#define Queue_1_Dequeue_m3739117297(__this, method) ((  TimeInterval_1_t708065542  (*) (Queue_1_t2416153082 *, const MethodInfo*))Queue_1_Dequeue_m3739117297_gshared)(__this, method)
// T System.Collections.Generic.Queue`1<UniRx.TimeInterval`1<System.Object>>::Peek()
extern "C"  TimeInterval_1_t708065542  Queue_1_Peek_m3201764060_gshared (Queue_1_t2416153082 * __this, const MethodInfo* method);
#define Queue_1_Peek_m3201764060(__this, method) ((  TimeInterval_1_t708065542  (*) (Queue_1_t2416153082 *, const MethodInfo*))Queue_1_Peek_m3201764060_gshared)(__this, method)
// System.Void System.Collections.Generic.Queue`1<UniRx.TimeInterval`1<System.Object>>::Enqueue(T)
extern "C"  void Queue_1_Enqueue_m2902442324_gshared (Queue_1_t2416153082 * __this, TimeInterval_1_t708065542  ___item0, const MethodInfo* method);
#define Queue_1_Enqueue_m2902442324(__this, ___item0, method) ((  void (*) (Queue_1_t2416153082 *, TimeInterval_1_t708065542 , const MethodInfo*))Queue_1_Enqueue_m2902442324_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.Queue`1<UniRx.TimeInterval`1<System.Object>>::SetCapacity(System.Int32)
extern "C"  void Queue_1_SetCapacity_m3880367439_gshared (Queue_1_t2416153082 * __this, int32_t ___new_size0, const MethodInfo* method);
#define Queue_1_SetCapacity_m3880367439(__this, ___new_size0, method) ((  void (*) (Queue_1_t2416153082 *, int32_t, const MethodInfo*))Queue_1_SetCapacity_m3880367439_gshared)(__this, ___new_size0, method)
// System.Int32 System.Collections.Generic.Queue`1<UniRx.TimeInterval`1<System.Object>>::get_Count()
extern "C"  int32_t Queue_1_get_Count_m246566552_gshared (Queue_1_t2416153082 * __this, const MethodInfo* method);
#define Queue_1_get_Count_m246566552(__this, method) ((  int32_t (*) (Queue_1_t2416153082 *, const MethodInfo*))Queue_1_get_Count_m246566552_gshared)(__this, method)
// System.Collections.Generic.Queue`1/Enumerator<T> System.Collections.Generic.Queue`1<UniRx.TimeInterval`1<System.Object>>::GetEnumerator()
extern "C"  Enumerator_t3885774799  Queue_1_GetEnumerator_m17847029_gshared (Queue_1_t2416153082 * __this, const MethodInfo* method);
#define Queue_1_GetEnumerator_m17847029(__this, method) ((  Enumerator_t3885774799  (*) (Queue_1_t2416153082 *, const MethodInfo*))Queue_1_GetEnumerator_m17847029_gshared)(__this, method)
