﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Transform
struct Transform_t284553113;
// System.Object
struct Il2CppObject;

#include "mscorlib_System_Object837106420.h"
#include "UnityEngine_UnityEngine_Vector33525329789.h"
#include "UnityEngine_UnityEngine_Quaternion1891715979.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameController/<SendMoneyCoroutine>c__Iterator12
struct  U3CSendMoneyCoroutineU3Ec__Iterator12_t392378224  : public Il2CppObject
{
public:
	// System.Single GameController/<SendMoneyCoroutine>c__Iterator12::<startTime>__0
	float ___U3CstartTimeU3E__0_0;
	// System.Single GameController/<SendMoneyCoroutine>c__Iterator12::<processTime>__1
	float ___U3CprocessTimeU3E__1_1;
	// UnityEngine.Transform GameController/<SendMoneyCoroutine>c__Iterator12::moneyTransform
	Transform_t284553113 * ___moneyTransform_2;
	// UnityEngine.Vector3 GameController/<SendMoneyCoroutine>c__Iterator12::<startMoneyPosition>__2
	Vector3_t3525329789  ___U3CstartMoneyPositionU3E__2_3;
	// UnityEngine.Vector3 GameController/<SendMoneyCoroutine>c__Iterator12::<targetScale>__3
	Vector3_t3525329789  ___U3CtargetScaleU3E__3_4;
	// System.Single GameController/<SendMoneyCoroutine>c__Iterator12::<randomRotation>__4
	float ___U3CrandomRotationU3E__4_5;
	// UnityEngine.Quaternion GameController/<SendMoneyCoroutine>c__Iterator12::<targetRotation>__5
	Quaternion_t1891715979  ___U3CtargetRotationU3E__5_6;
	// UnityEngine.Vector3 GameController/<SendMoneyCoroutine>c__Iterator12::<finalPosition>__6
	Vector3_t3525329789  ___U3CfinalPositionU3E__6_7;
	// System.Single GameController/<SendMoneyCoroutine>c__Iterator12::gripDelta
	float ___gripDelta_8;
	// System.Single GameController/<SendMoneyCoroutine>c__Iterator12::<deltaTime>__7
	float ___U3CdeltaTimeU3E__7_9;
	// System.Single GameController/<SendMoneyCoroutine>c__Iterator12::<ratio>__8
	float ___U3CratioU3E__8_10;
	// System.Int32 GameController/<SendMoneyCoroutine>c__Iterator12::$PC
	int32_t ___U24PC_11;
	// System.Object GameController/<SendMoneyCoroutine>c__Iterator12::$current
	Il2CppObject * ___U24current_12;
	// UnityEngine.Transform GameController/<SendMoneyCoroutine>c__Iterator12::<$>moneyTransform
	Transform_t284553113 * ___U3CU24U3EmoneyTransform_13;
	// System.Single GameController/<SendMoneyCoroutine>c__Iterator12::<$>gripDelta
	float ___U3CU24U3EgripDelta_14;

public:
	inline static int32_t get_offset_of_U3CstartTimeU3E__0_0() { return static_cast<int32_t>(offsetof(U3CSendMoneyCoroutineU3Ec__Iterator12_t392378224, ___U3CstartTimeU3E__0_0)); }
	inline float get_U3CstartTimeU3E__0_0() const { return ___U3CstartTimeU3E__0_0; }
	inline float* get_address_of_U3CstartTimeU3E__0_0() { return &___U3CstartTimeU3E__0_0; }
	inline void set_U3CstartTimeU3E__0_0(float value)
	{
		___U3CstartTimeU3E__0_0 = value;
	}

	inline static int32_t get_offset_of_U3CprocessTimeU3E__1_1() { return static_cast<int32_t>(offsetof(U3CSendMoneyCoroutineU3Ec__Iterator12_t392378224, ___U3CprocessTimeU3E__1_1)); }
	inline float get_U3CprocessTimeU3E__1_1() const { return ___U3CprocessTimeU3E__1_1; }
	inline float* get_address_of_U3CprocessTimeU3E__1_1() { return &___U3CprocessTimeU3E__1_1; }
	inline void set_U3CprocessTimeU3E__1_1(float value)
	{
		___U3CprocessTimeU3E__1_1 = value;
	}

	inline static int32_t get_offset_of_moneyTransform_2() { return static_cast<int32_t>(offsetof(U3CSendMoneyCoroutineU3Ec__Iterator12_t392378224, ___moneyTransform_2)); }
	inline Transform_t284553113 * get_moneyTransform_2() const { return ___moneyTransform_2; }
	inline Transform_t284553113 ** get_address_of_moneyTransform_2() { return &___moneyTransform_2; }
	inline void set_moneyTransform_2(Transform_t284553113 * value)
	{
		___moneyTransform_2 = value;
		Il2CppCodeGenWriteBarrier(&___moneyTransform_2, value);
	}

	inline static int32_t get_offset_of_U3CstartMoneyPositionU3E__2_3() { return static_cast<int32_t>(offsetof(U3CSendMoneyCoroutineU3Ec__Iterator12_t392378224, ___U3CstartMoneyPositionU3E__2_3)); }
	inline Vector3_t3525329789  get_U3CstartMoneyPositionU3E__2_3() const { return ___U3CstartMoneyPositionU3E__2_3; }
	inline Vector3_t3525329789 * get_address_of_U3CstartMoneyPositionU3E__2_3() { return &___U3CstartMoneyPositionU3E__2_3; }
	inline void set_U3CstartMoneyPositionU3E__2_3(Vector3_t3525329789  value)
	{
		___U3CstartMoneyPositionU3E__2_3 = value;
	}

	inline static int32_t get_offset_of_U3CtargetScaleU3E__3_4() { return static_cast<int32_t>(offsetof(U3CSendMoneyCoroutineU3Ec__Iterator12_t392378224, ___U3CtargetScaleU3E__3_4)); }
	inline Vector3_t3525329789  get_U3CtargetScaleU3E__3_4() const { return ___U3CtargetScaleU3E__3_4; }
	inline Vector3_t3525329789 * get_address_of_U3CtargetScaleU3E__3_4() { return &___U3CtargetScaleU3E__3_4; }
	inline void set_U3CtargetScaleU3E__3_4(Vector3_t3525329789  value)
	{
		___U3CtargetScaleU3E__3_4 = value;
	}

	inline static int32_t get_offset_of_U3CrandomRotationU3E__4_5() { return static_cast<int32_t>(offsetof(U3CSendMoneyCoroutineU3Ec__Iterator12_t392378224, ___U3CrandomRotationU3E__4_5)); }
	inline float get_U3CrandomRotationU3E__4_5() const { return ___U3CrandomRotationU3E__4_5; }
	inline float* get_address_of_U3CrandomRotationU3E__4_5() { return &___U3CrandomRotationU3E__4_5; }
	inline void set_U3CrandomRotationU3E__4_5(float value)
	{
		___U3CrandomRotationU3E__4_5 = value;
	}

	inline static int32_t get_offset_of_U3CtargetRotationU3E__5_6() { return static_cast<int32_t>(offsetof(U3CSendMoneyCoroutineU3Ec__Iterator12_t392378224, ___U3CtargetRotationU3E__5_6)); }
	inline Quaternion_t1891715979  get_U3CtargetRotationU3E__5_6() const { return ___U3CtargetRotationU3E__5_6; }
	inline Quaternion_t1891715979 * get_address_of_U3CtargetRotationU3E__5_6() { return &___U3CtargetRotationU3E__5_6; }
	inline void set_U3CtargetRotationU3E__5_6(Quaternion_t1891715979  value)
	{
		___U3CtargetRotationU3E__5_6 = value;
	}

	inline static int32_t get_offset_of_U3CfinalPositionU3E__6_7() { return static_cast<int32_t>(offsetof(U3CSendMoneyCoroutineU3Ec__Iterator12_t392378224, ___U3CfinalPositionU3E__6_7)); }
	inline Vector3_t3525329789  get_U3CfinalPositionU3E__6_7() const { return ___U3CfinalPositionU3E__6_7; }
	inline Vector3_t3525329789 * get_address_of_U3CfinalPositionU3E__6_7() { return &___U3CfinalPositionU3E__6_7; }
	inline void set_U3CfinalPositionU3E__6_7(Vector3_t3525329789  value)
	{
		___U3CfinalPositionU3E__6_7 = value;
	}

	inline static int32_t get_offset_of_gripDelta_8() { return static_cast<int32_t>(offsetof(U3CSendMoneyCoroutineU3Ec__Iterator12_t392378224, ___gripDelta_8)); }
	inline float get_gripDelta_8() const { return ___gripDelta_8; }
	inline float* get_address_of_gripDelta_8() { return &___gripDelta_8; }
	inline void set_gripDelta_8(float value)
	{
		___gripDelta_8 = value;
	}

	inline static int32_t get_offset_of_U3CdeltaTimeU3E__7_9() { return static_cast<int32_t>(offsetof(U3CSendMoneyCoroutineU3Ec__Iterator12_t392378224, ___U3CdeltaTimeU3E__7_9)); }
	inline float get_U3CdeltaTimeU3E__7_9() const { return ___U3CdeltaTimeU3E__7_9; }
	inline float* get_address_of_U3CdeltaTimeU3E__7_9() { return &___U3CdeltaTimeU3E__7_9; }
	inline void set_U3CdeltaTimeU3E__7_9(float value)
	{
		___U3CdeltaTimeU3E__7_9 = value;
	}

	inline static int32_t get_offset_of_U3CratioU3E__8_10() { return static_cast<int32_t>(offsetof(U3CSendMoneyCoroutineU3Ec__Iterator12_t392378224, ___U3CratioU3E__8_10)); }
	inline float get_U3CratioU3E__8_10() const { return ___U3CratioU3E__8_10; }
	inline float* get_address_of_U3CratioU3E__8_10() { return &___U3CratioU3E__8_10; }
	inline void set_U3CratioU3E__8_10(float value)
	{
		___U3CratioU3E__8_10 = value;
	}

	inline static int32_t get_offset_of_U24PC_11() { return static_cast<int32_t>(offsetof(U3CSendMoneyCoroutineU3Ec__Iterator12_t392378224, ___U24PC_11)); }
	inline int32_t get_U24PC_11() const { return ___U24PC_11; }
	inline int32_t* get_address_of_U24PC_11() { return &___U24PC_11; }
	inline void set_U24PC_11(int32_t value)
	{
		___U24PC_11 = value;
	}

	inline static int32_t get_offset_of_U24current_12() { return static_cast<int32_t>(offsetof(U3CSendMoneyCoroutineU3Ec__Iterator12_t392378224, ___U24current_12)); }
	inline Il2CppObject * get_U24current_12() const { return ___U24current_12; }
	inline Il2CppObject ** get_address_of_U24current_12() { return &___U24current_12; }
	inline void set_U24current_12(Il2CppObject * value)
	{
		___U24current_12 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_12, value);
	}

	inline static int32_t get_offset_of_U3CU24U3EmoneyTransform_13() { return static_cast<int32_t>(offsetof(U3CSendMoneyCoroutineU3Ec__Iterator12_t392378224, ___U3CU24U3EmoneyTransform_13)); }
	inline Transform_t284553113 * get_U3CU24U3EmoneyTransform_13() const { return ___U3CU24U3EmoneyTransform_13; }
	inline Transform_t284553113 ** get_address_of_U3CU24U3EmoneyTransform_13() { return &___U3CU24U3EmoneyTransform_13; }
	inline void set_U3CU24U3EmoneyTransform_13(Transform_t284553113 * value)
	{
		___U3CU24U3EmoneyTransform_13 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU24U3EmoneyTransform_13, value);
	}

	inline static int32_t get_offset_of_U3CU24U3EgripDelta_14() { return static_cast<int32_t>(offsetof(U3CSendMoneyCoroutineU3Ec__Iterator12_t392378224, ___U3CU24U3EgripDelta_14)); }
	inline float get_U3CU24U3EgripDelta_14() const { return ___U3CU24U3EgripDelta_14; }
	inline float* get_address_of_U3CU24U3EgripDelta_14() { return &___U3CU24U3EgripDelta_14; }
	inline void set_U3CU24U3EgripDelta_14(float value)
	{
		___U3CU24U3EgripDelta_14 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
