﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SubstanceBase`2<System.Single,System.Single>
struct SubstanceBase_2_t1869245964;
// ICell`1<System.Single>
struct ICell_1_t2509839998;
// System.IDisposable
struct IDisposable_t1628921374;
// System.Action`1<System.Single>
struct Action_1_t1106661726;
// System.Action
struct Action_t437523947;
// System.Object
struct Il2CppObject;
// IStream`1<System.Single>
struct IStream_1_t1510900012;
// SubstanceBase`2/Intrusion<System.Single,System.Single>
struct Intrusion_t78896424;
// ILiving
struct ILiving_t2639664210;
// IEmptyStream
struct IEmptyStream_t3684082468;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Priority3194150340.h"
#include "System_Core_System_Action437523947.h"

// System.Void SubstanceBase`2<System.Single,System.Single>::.ctor()
extern "C"  void SubstanceBase_2__ctor_m4143605078_gshared (SubstanceBase_2_t1869245964 * __this, const MethodInfo* method);
#define SubstanceBase_2__ctor_m4143605078(__this, method) ((  void (*) (SubstanceBase_2_t1869245964 *, const MethodInfo*))SubstanceBase_2__ctor_m4143605078_gshared)(__this, method)
// System.Void SubstanceBase`2<System.Single,System.Single>::.ctor(T)
extern "C"  void SubstanceBase_2__ctor_m3897707208_gshared (SubstanceBase_2_t1869245964 * __this, float ___baseVal0, const MethodInfo* method);
#define SubstanceBase_2__ctor_m3897707208(__this, ___baseVal0, method) ((  void (*) (SubstanceBase_2_t1869245964 *, float, const MethodInfo*))SubstanceBase_2__ctor_m3897707208_gshared)(__this, ___baseVal0, method)
// T SubstanceBase`2<System.Single,System.Single>::get_baseValue()
extern "C"  float SubstanceBase_2_get_baseValue_m4060810796_gshared (SubstanceBase_2_t1869245964 * __this, const MethodInfo* method);
#define SubstanceBase_2_get_baseValue_m4060810796(__this, method) ((  float (*) (SubstanceBase_2_t1869245964 *, const MethodInfo*))SubstanceBase_2_get_baseValue_m4060810796_gshared)(__this, method)
// System.Void SubstanceBase`2<System.Single,System.Single>::set_baseValue(T)
extern "C"  void SubstanceBase_2_set_baseValue_m1547869159_gshared (SubstanceBase_2_t1869245964 * __this, float ___value0, const MethodInfo* method);
#define SubstanceBase_2_set_baseValue_m1547869159(__this, ___value0, method) ((  void (*) (SubstanceBase_2_t1869245964 *, float, const MethodInfo*))SubstanceBase_2_set_baseValue_m1547869159_gshared)(__this, ___value0, method)
// System.Void SubstanceBase`2<System.Single,System.Single>::set_baseValueReactive(ICell`1<T>)
extern "C"  void SubstanceBase_2_set_baseValueReactive_m653709286_gshared (SubstanceBase_2_t1869245964 * __this, Il2CppObject* ___value0, const MethodInfo* method);
#define SubstanceBase_2_set_baseValueReactive_m653709286(__this, ___value0, method) ((  void (*) (SubstanceBase_2_t1869245964 *, Il2CppObject*, const MethodInfo*))SubstanceBase_2_set_baseValueReactive_m653709286_gshared)(__this, ___value0, method)
// System.IDisposable SubstanceBase`2<System.Single,System.Single>::Bind(System.Action`1<T>,Priority)
extern "C"  Il2CppObject * SubstanceBase_2_Bind_m2534636228_gshared (SubstanceBase_2_t1869245964 * __this, Action_1_t1106661726 * ___action0, int32_t ___p1, const MethodInfo* method);
#define SubstanceBase_2_Bind_m2534636228(__this, ___action0, ___p1, method) ((  Il2CppObject * (*) (SubstanceBase_2_t1869245964 *, Action_1_t1106661726 *, int32_t, const MethodInfo*))SubstanceBase_2_Bind_m2534636228_gshared)(__this, ___action0, ___p1, method)
// System.IDisposable SubstanceBase`2<System.Single,System.Single>::OnChanged(System.Action,Priority)
extern "C"  Il2CppObject * SubstanceBase_2_OnChanged_m3103538555_gshared (SubstanceBase_2_t1869245964 * __this, Action_t437523947 * ___action0, int32_t ___p1, const MethodInfo* method);
#define SubstanceBase_2_OnChanged_m3103538555(__this, ___action0, ___p1, method) ((  Il2CppObject * (*) (SubstanceBase_2_t1869245964 *, Action_t437523947 *, int32_t, const MethodInfo*))SubstanceBase_2_OnChanged_m3103538555_gshared)(__this, ___action0, ___p1, method)
// System.Object SubstanceBase`2<System.Single,System.Single>::get_valueObject()
extern "C"  Il2CppObject * SubstanceBase_2_get_valueObject_m1472958054_gshared (SubstanceBase_2_t1869245964 * __this, const MethodInfo* method);
#define SubstanceBase_2_get_valueObject_m1472958054(__this, method) ((  Il2CppObject * (*) (SubstanceBase_2_t1869245964 *, const MethodInfo*))SubstanceBase_2_get_valueObject_m1472958054_gshared)(__this, method)
// System.IDisposable SubstanceBase`2<System.Single,System.Single>::ListenUpdates(System.Action`1<T>,Priority)
extern "C"  Il2CppObject * SubstanceBase_2_ListenUpdates_m3356296638_gshared (SubstanceBase_2_t1869245964 * __this, Action_1_t1106661726 * ___action0, int32_t ___p1, const MethodInfo* method);
#define SubstanceBase_2_ListenUpdates_m3356296638(__this, ___action0, ___p1, method) ((  Il2CppObject * (*) (SubstanceBase_2_t1869245964 *, Action_1_t1106661726 *, int32_t, const MethodInfo*))SubstanceBase_2_ListenUpdates_m3356296638_gshared)(__this, ___action0, ___p1, method)
// IStream`1<T> SubstanceBase`2<System.Single,System.Single>::get_updates()
extern "C"  Il2CppObject* SubstanceBase_2_get_updates_m1233191236_gshared (SubstanceBase_2_t1869245964 * __this, const MethodInfo* method);
#define SubstanceBase_2_get_updates_m1233191236(__this, method) ((  Il2CppObject* (*) (SubstanceBase_2_t1869245964 *, const MethodInfo*))SubstanceBase_2_get_updates_m1233191236_gshared)(__this, method)
// T SubstanceBase`2<System.Single,System.Single>::get_value()
extern "C"  float SubstanceBase_2_get_value_m2710263741_gshared (SubstanceBase_2_t1869245964 * __this, const MethodInfo* method);
#define SubstanceBase_2_get_value_m2710263741(__this, method) ((  float (*) (SubstanceBase_2_t1869245964 *, const MethodInfo*))SubstanceBase_2_get_value_m2710263741_gshared)(__this, method)
// SubstanceBase`2/Intrusion<T,InfT> SubstanceBase`2<System.Single,System.Single>::AffectInner(ILiving,InfT)
extern "C"  Intrusion_t78896424 * SubstanceBase_2_AffectInner_m1370536951_gshared (SubstanceBase_2_t1869245964 * __this, Il2CppObject * ___intruder0, float ___influence1, const MethodInfo* method);
#define SubstanceBase_2_AffectInner_m1370536951(__this, ___intruder0, ___influence1, method) ((  Intrusion_t78896424 * (*) (SubstanceBase_2_t1869245964 *, Il2CppObject *, float, const MethodInfo*))SubstanceBase_2_AffectInner_m1370536951_gshared)(__this, ___intruder0, ___influence1, method)
// System.IDisposable SubstanceBase`2<System.Single,System.Single>::AffectUnsafe(InfT)
extern "C"  Il2CppObject * SubstanceBase_2_AffectUnsafe_m2088159881_gshared (SubstanceBase_2_t1869245964 * __this, float ___influence0, const MethodInfo* method);
#define SubstanceBase_2_AffectUnsafe_m2088159881(__this, ___influence0, method) ((  Il2CppObject * (*) (SubstanceBase_2_t1869245964 *, float, const MethodInfo*))SubstanceBase_2_AffectUnsafe_m2088159881_gshared)(__this, ___influence0, method)
// System.IDisposable SubstanceBase`2<System.Single,System.Single>::AffectUnsafe(ICell`1<InfT>)
extern "C"  Il2CppObject * SubstanceBase_2_AffectUnsafe_m139999659_gshared (SubstanceBase_2_t1869245964 * __this, Il2CppObject* ___influence0, const MethodInfo* method);
#define SubstanceBase_2_AffectUnsafe_m139999659(__this, ___influence0, method) ((  Il2CppObject * (*) (SubstanceBase_2_t1869245964 *, Il2CppObject*, const MethodInfo*))SubstanceBase_2_AffectUnsafe_m139999659_gshared)(__this, ___influence0, method)
// System.IDisposable SubstanceBase`2<System.Single,System.Single>::Affect(ILiving,InfT)
extern "C"  Il2CppObject * SubstanceBase_2_Affect_m1176605225_gshared (SubstanceBase_2_t1869245964 * __this, Il2CppObject * ___intruder0, float ___influence1, const MethodInfo* method);
#define SubstanceBase_2_Affect_m1176605225(__this, ___intruder0, ___influence1, method) ((  Il2CppObject * (*) (SubstanceBase_2_t1869245964 *, Il2CppObject *, float, const MethodInfo*))SubstanceBase_2_Affect_m1176605225_gshared)(__this, ___intruder0, ___influence1, method)
// System.IDisposable SubstanceBase`2<System.Single,System.Single>::Affect(ILiving,ICell`1<InfT>)
extern "C"  Il2CppObject * SubstanceBase_2_Affect_m1120142859_gshared (SubstanceBase_2_t1869245964 * __this, Il2CppObject * ___intruder0, Il2CppObject* ___influence1, const MethodInfo* method);
#define SubstanceBase_2_Affect_m1120142859(__this, ___intruder0, ___influence1, method) ((  Il2CppObject * (*) (SubstanceBase_2_t1869245964 *, Il2CppObject *, Il2CppObject*, const MethodInfo*))SubstanceBase_2_Affect_m1120142859_gshared)(__this, ___intruder0, ___influence1, method)
// System.IDisposable SubstanceBase`2<System.Single,System.Single>::Affect(ILiving,InfT,IEmptyStream)
extern "C"  Il2CppObject * SubstanceBase_2_Affect_m1574442713_gshared (SubstanceBase_2_t1869245964 * __this, Il2CppObject * ___intruder0, float ___influence1, Il2CppObject * ___update2, const MethodInfo* method);
#define SubstanceBase_2_Affect_m1574442713(__this, ___intruder0, ___influence1, ___update2, method) ((  Il2CppObject * (*) (SubstanceBase_2_t1869245964 *, Il2CppObject *, float, Il2CppObject *, const MethodInfo*))SubstanceBase_2_Affect_m1574442713_gshared)(__this, ___intruder0, ___influence1, ___update2, method)
// System.Void SubstanceBase`2<System.Single,System.Single>::OnAdd(SubstanceBase`2/Intrusion<T,InfT>)
extern "C"  void SubstanceBase_2_OnAdd_m775173466_gshared (SubstanceBase_2_t1869245964 * __this, Intrusion_t78896424 * ___intrusion0, const MethodInfo* method);
#define SubstanceBase_2_OnAdd_m775173466(__this, ___intrusion0, method) ((  void (*) (SubstanceBase_2_t1869245964 *, Intrusion_t78896424 *, const MethodInfo*))SubstanceBase_2_OnAdd_m775173466_gshared)(__this, ___intrusion0, method)
// System.Void SubstanceBase`2<System.Single,System.Single>::OnRemove(SubstanceBase`2/Intrusion<T,InfT>)
extern "C"  void SubstanceBase_2_OnRemove_m1451557823_gshared (SubstanceBase_2_t1869245964 * __this, Intrusion_t78896424 * ___intrusion0, const MethodInfo* method);
#define SubstanceBase_2_OnRemove_m1451557823(__this, ___intrusion0, method) ((  void (*) (SubstanceBase_2_t1869245964 *, Intrusion_t78896424 *, const MethodInfo*))SubstanceBase_2_OnRemove_m1451557823_gshared)(__this, ___intrusion0, method)
// System.Void SubstanceBase`2<System.Single,System.Single>::OnUpdate(SubstanceBase`2/Intrusion<T,InfT>)
extern "C"  void SubstanceBase_2_OnUpdate_m802288986_gshared (SubstanceBase_2_t1869245964 * __this, Intrusion_t78896424 * ___intrusion0, const MethodInfo* method);
#define SubstanceBase_2_OnUpdate_m802288986(__this, ___intrusion0, method) ((  void (*) (SubstanceBase_2_t1869245964 *, Intrusion_t78896424 *, const MethodInfo*))SubstanceBase_2_OnUpdate_m802288986_gshared)(__this, ___intrusion0, method)
// System.Void SubstanceBase`2<System.Single,System.Single>::UpdateAll()
extern "C"  void SubstanceBase_2_UpdateAll_m1969183500_gshared (SubstanceBase_2_t1869245964 * __this, const MethodInfo* method);
#define SubstanceBase_2_UpdateAll_m1969183500(__this, method) ((  void (*) (SubstanceBase_2_t1869245964 *, const MethodInfo*))SubstanceBase_2_UpdateAll_m1969183500_gshared)(__this, method)
// System.Void SubstanceBase`2<System.Single,System.Single>::ClearIntrusion(SubstanceBase`2/Intrusion<T,InfT>)
extern "C"  void SubstanceBase_2_ClearIntrusion_m824825096_gshared (SubstanceBase_2_t1869245964 * __this, Intrusion_t78896424 * ___intr0, const MethodInfo* method);
#define SubstanceBase_2_ClearIntrusion_m824825096(__this, ___intr0, method) ((  void (*) (SubstanceBase_2_t1869245964 *, Intrusion_t78896424 *, const MethodInfo*))SubstanceBase_2_ClearIntrusion_m824825096_gshared)(__this, ___intr0, method)
// System.Void SubstanceBase`2<System.Single,System.Single>::<set_baseValueReactive>m__1D7(T)
extern "C"  void SubstanceBase_2_U3Cset_baseValueReactiveU3Em__1D7_m2869877525_gshared (SubstanceBase_2_t1869245964 * __this, float ___val0, const MethodInfo* method);
#define SubstanceBase_2_U3Cset_baseValueReactiveU3Em__1D7_m2869877525(__this, ___val0, method) ((  void (*) (SubstanceBase_2_t1869245964 *, float, const MethodInfo*))SubstanceBase_2_U3Cset_baseValueReactiveU3Em__1D7_m2869877525_gshared)(__this, ___val0, method)
