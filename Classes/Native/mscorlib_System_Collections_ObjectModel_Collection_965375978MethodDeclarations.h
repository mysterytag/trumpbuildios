﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.ObjectModel.Collection`1<GameAnalyticsSDK.Settings/HelpTypes>
struct Collection_1_t965375978;
// System.Collections.Generic.IList`1<GameAnalyticsSDK.Settings/HelpTypes>
struct IList_1_t1162880562;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t287207039;
// System.Object
struct Il2CppObject;
// GameAnalyticsSDK.Settings/HelpTypes[]
struct HelpTypesU5BU5D_t2651985161;
// System.Collections.Generic.IEnumerator`1<GameAnalyticsSDK.Settings/HelpTypes>
struct IEnumerator_1_t479494696;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array2840145358.h"
#include "mscorlib_System_Object837106420.h"
#include "AssemblyU2DCSharp_GameAnalyticsSDK_Settings_HelpTy3291355544.h"

// System.Void System.Collections.ObjectModel.Collection`1<GameAnalyticsSDK.Settings/HelpTypes>::.ctor()
extern "C"  void Collection_1__ctor_m656066935_gshared (Collection_1_t965375978 * __this, const MethodInfo* method);
#define Collection_1__ctor_m656066935(__this, method) ((  void (*) (Collection_1_t965375978 *, const MethodInfo*))Collection_1__ctor_m656066935_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<GameAnalyticsSDK.Settings/HelpTypes>::.ctor(System.Collections.Generic.IList`1<T>)
extern "C"  void Collection_1__ctor_m3051244542_gshared (Collection_1_t965375978 * __this, Il2CppObject* ___list0, const MethodInfo* method);
#define Collection_1__ctor_m3051244542(__this, ___list0, method) ((  void (*) (Collection_1_t965375978 *, Il2CppObject*, const MethodInfo*))Collection_1__ctor_m3051244542_gshared)(__this, ___list0, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<GameAnalyticsSDK.Settings/HelpTypes>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C"  bool Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m125863332_gshared (Collection_1_t965375978 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m125863332(__this, method) ((  bool (*) (Collection_1_t965375978 *, const MethodInfo*))Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m125863332_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<GameAnalyticsSDK.Settings/HelpTypes>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void Collection_1_System_Collections_ICollection_CopyTo_m2261791661_gshared (Collection_1_t965375978 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_CopyTo_m2261791661(__this, ___array0, ___index1, method) ((  void (*) (Collection_1_t965375978 *, Il2CppArray *, int32_t, const MethodInfo*))Collection_1_System_Collections_ICollection_CopyTo_m2261791661_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.ObjectModel.Collection`1<GameAnalyticsSDK.Settings/HelpTypes>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * Collection_1_System_Collections_IEnumerable_GetEnumerator_m2828087784_gshared (Collection_1_t965375978 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IEnumerable_GetEnumerator_m2828087784(__this, method) ((  Il2CppObject * (*) (Collection_1_t965375978 *, const MethodInfo*))Collection_1_System_Collections_IEnumerable_GetEnumerator_m2828087784_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<GameAnalyticsSDK.Settings/HelpTypes>::System.Collections.IList.Add(System.Object)
extern "C"  int32_t Collection_1_System_Collections_IList_Add_m556488713_gshared (Collection_1_t965375978 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Add_m556488713(__this, ___value0, method) ((  int32_t (*) (Collection_1_t965375978 *, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_Add_m556488713_gshared)(__this, ___value0, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<GameAnalyticsSDK.Settings/HelpTypes>::System.Collections.IList.Contains(System.Object)
extern "C"  bool Collection_1_System_Collections_IList_Contains_m1044493667_gshared (Collection_1_t965375978 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Contains_m1044493667(__this, ___value0, method) ((  bool (*) (Collection_1_t965375978 *, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_Contains_m1044493667_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<GameAnalyticsSDK.Settings/HelpTypes>::System.Collections.IList.IndexOf(System.Object)
extern "C"  int32_t Collection_1_System_Collections_IList_IndexOf_m461267425_gshared (Collection_1_t965375978 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Collection_1_System_Collections_IList_IndexOf_m461267425(__this, ___value0, method) ((  int32_t (*) (Collection_1_t965375978 *, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_IndexOf_m461267425_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.Collection`1<GameAnalyticsSDK.Settings/HelpTypes>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C"  void Collection_1_System_Collections_IList_Insert_m3551950988_gshared (Collection_1_t965375978 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Insert_m3551950988(__this, ___index0, ___value1, method) ((  void (*) (Collection_1_t965375978 *, int32_t, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_Insert_m3551950988_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.ObjectModel.Collection`1<GameAnalyticsSDK.Settings/HelpTypes>::System.Collections.IList.Remove(System.Object)
extern "C"  void Collection_1_System_Collections_IList_Remove_m2177383708_gshared (Collection_1_t965375978 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Remove_m2177383708(__this, ___value0, method) ((  void (*) (Collection_1_t965375978 *, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_Remove_m2177383708_gshared)(__this, ___value0, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<GameAnalyticsSDK.Settings/HelpTypes>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool Collection_1_System_Collections_ICollection_get_IsSynchronized_m3762717465_gshared (Collection_1_t965375978 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_get_IsSynchronized_m3762717465(__this, method) ((  bool (*) (Collection_1_t965375978 *, const MethodInfo*))Collection_1_System_Collections_ICollection_get_IsSynchronized_m3762717465_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.Collection`1<GameAnalyticsSDK.Settings/HelpTypes>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * Collection_1_System_Collections_ICollection_get_SyncRoot_m2828241861_gshared (Collection_1_t965375978 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_get_SyncRoot_m2828241861(__this, method) ((  Il2CppObject * (*) (Collection_1_t965375978 *, const MethodInfo*))Collection_1_System_Collections_ICollection_get_SyncRoot_m2828241861_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<GameAnalyticsSDK.Settings/HelpTypes>::System.Collections.IList.get_IsFixedSize()
extern "C"  bool Collection_1_System_Collections_IList_get_IsFixedSize_m2810833682_gshared (Collection_1_t965375978 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_IsFixedSize_m2810833682(__this, method) ((  bool (*) (Collection_1_t965375978 *, const MethodInfo*))Collection_1_System_Collections_IList_get_IsFixedSize_m2810833682_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<GameAnalyticsSDK.Settings/HelpTypes>::System.Collections.IList.get_IsReadOnly()
extern "C"  bool Collection_1_System_Collections_IList_get_IsReadOnly_m4285458855_gshared (Collection_1_t965375978 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_IsReadOnly_m4285458855(__this, method) ((  bool (*) (Collection_1_t965375978 *, const MethodInfo*))Collection_1_System_Collections_IList_get_IsReadOnly_m4285458855_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.Collection`1<GameAnalyticsSDK.Settings/HelpTypes>::System.Collections.IList.get_Item(System.Int32)
extern "C"  Il2CppObject * Collection_1_System_Collections_IList_get_Item_m291741708_gshared (Collection_1_t965375978 * __this, int32_t ___index0, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_Item_m291741708(__this, ___index0, method) ((  Il2CppObject * (*) (Collection_1_t965375978 *, int32_t, const MethodInfo*))Collection_1_System_Collections_IList_get_Item_m291741708_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.Collection`1<GameAnalyticsSDK.Settings/HelpTypes>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C"  void Collection_1_System_Collections_IList_set_Item_m3834818915_gshared (Collection_1_t965375978 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method);
#define Collection_1_System_Collections_IList_set_Item_m3834818915(__this, ___index0, ___value1, method) ((  void (*) (Collection_1_t965375978 *, int32_t, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_set_Item_m3834818915_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.ObjectModel.Collection`1<GameAnalyticsSDK.Settings/HelpTypes>::Add(T)
extern "C"  void Collection_1_Add_m2505157672_gshared (Collection_1_t965375978 * __this, int32_t ___item0, const MethodInfo* method);
#define Collection_1_Add_m2505157672(__this, ___item0, method) ((  void (*) (Collection_1_t965375978 *, int32_t, const MethodInfo*))Collection_1_Add_m2505157672_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<GameAnalyticsSDK.Settings/HelpTypes>::Clear()
extern "C"  void Collection_1_Clear_m2357167522_gshared (Collection_1_t965375978 * __this, const MethodInfo* method);
#define Collection_1_Clear_m2357167522(__this, method) ((  void (*) (Collection_1_t965375978 *, const MethodInfo*))Collection_1_Clear_m2357167522_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<GameAnalyticsSDK.Settings/HelpTypes>::ClearItems()
extern "C"  void Collection_1_ClearItems_m3410514080_gshared (Collection_1_t965375978 * __this, const MethodInfo* method);
#define Collection_1_ClearItems_m3410514080(__this, method) ((  void (*) (Collection_1_t965375978 *, const MethodInfo*))Collection_1_ClearItems_m3410514080_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<GameAnalyticsSDK.Settings/HelpTypes>::Contains(T)
extern "C"  bool Collection_1_Contains_m1136732624_gshared (Collection_1_t965375978 * __this, int32_t ___item0, const MethodInfo* method);
#define Collection_1_Contains_m1136732624(__this, ___item0, method) ((  bool (*) (Collection_1_t965375978 *, int32_t, const MethodInfo*))Collection_1_Contains_m1136732624_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<GameAnalyticsSDK.Settings/HelpTypes>::CopyTo(T[],System.Int32)
extern "C"  void Collection_1_CopyTo_m2894322200_gshared (Collection_1_t965375978 * __this, HelpTypesU5BU5D_t2651985161* ___array0, int32_t ___index1, const MethodInfo* method);
#define Collection_1_CopyTo_m2894322200(__this, ___array0, ___index1, method) ((  void (*) (Collection_1_t965375978 *, HelpTypesU5BU5D_t2651985161*, int32_t, const MethodInfo*))Collection_1_CopyTo_m2894322200_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.ObjectModel.Collection`1<GameAnalyticsSDK.Settings/HelpTypes>::GetEnumerator()
extern "C"  Il2CppObject* Collection_1_GetEnumerator_m2960820275_gshared (Collection_1_t965375978 * __this, const MethodInfo* method);
#define Collection_1_GetEnumerator_m2960820275(__this, method) ((  Il2CppObject* (*) (Collection_1_t965375978 *, const MethodInfo*))Collection_1_GetEnumerator_m2960820275_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<GameAnalyticsSDK.Settings/HelpTypes>::IndexOf(T)
extern "C"  int32_t Collection_1_IndexOf_m2670789020_gshared (Collection_1_t965375978 * __this, int32_t ___item0, const MethodInfo* method);
#define Collection_1_IndexOf_m2670789020(__this, ___item0, method) ((  int32_t (*) (Collection_1_t965375978 *, int32_t, const MethodInfo*))Collection_1_IndexOf_m2670789020_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<GameAnalyticsSDK.Settings/HelpTypes>::Insert(System.Int32,T)
extern "C"  void Collection_1_Insert_m3439433359_gshared (Collection_1_t965375978 * __this, int32_t ___index0, int32_t ___item1, const MethodInfo* method);
#define Collection_1_Insert_m3439433359(__this, ___index0, ___item1, method) ((  void (*) (Collection_1_t965375978 *, int32_t, int32_t, const MethodInfo*))Collection_1_Insert_m3439433359_gshared)(__this, ___index0, ___item1, method)
// System.Void System.Collections.ObjectModel.Collection`1<GameAnalyticsSDK.Settings/HelpTypes>::InsertItem(System.Int32,T)
extern "C"  void Collection_1_InsertItem_m266134850_gshared (Collection_1_t965375978 * __this, int32_t ___index0, int32_t ___item1, const MethodInfo* method);
#define Collection_1_InsertItem_m266134850(__this, ___index0, ___item1, method) ((  void (*) (Collection_1_t965375978 *, int32_t, int32_t, const MethodInfo*))Collection_1_InsertItem_m266134850_gshared)(__this, ___index0, ___item1, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<GameAnalyticsSDK.Settings/HelpTypes>::Remove(T)
extern "C"  bool Collection_1_Remove_m3218480587_gshared (Collection_1_t965375978 * __this, int32_t ___item0, const MethodInfo* method);
#define Collection_1_Remove_m3218480587(__this, ___item0, method) ((  bool (*) (Collection_1_t965375978 *, int32_t, const MethodInfo*))Collection_1_Remove_m3218480587_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<GameAnalyticsSDK.Settings/HelpTypes>::RemoveAt(System.Int32)
extern "C"  void Collection_1_RemoveAt_m1313286229_gshared (Collection_1_t965375978 * __this, int32_t ___index0, const MethodInfo* method);
#define Collection_1_RemoveAt_m1313286229(__this, ___index0, method) ((  void (*) (Collection_1_t965375978 *, int32_t, const MethodInfo*))Collection_1_RemoveAt_m1313286229_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.Collection`1<GameAnalyticsSDK.Settings/HelpTypes>::RemoveItem(System.Int32)
extern "C"  void Collection_1_RemoveItem_m2247306037_gshared (Collection_1_t965375978 * __this, int32_t ___index0, const MethodInfo* method);
#define Collection_1_RemoveItem_m2247306037(__this, ___index0, method) ((  void (*) (Collection_1_t965375978 *, int32_t, const MethodInfo*))Collection_1_RemoveItem_m2247306037_gshared)(__this, ___index0, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<GameAnalyticsSDK.Settings/HelpTypes>::get_Count()
extern "C"  int32_t Collection_1_get_Count_m1007388511_gshared (Collection_1_t965375978 * __this, const MethodInfo* method);
#define Collection_1_get_Count_m1007388511(__this, method) ((  int32_t (*) (Collection_1_t965375978 *, const MethodInfo*))Collection_1_get_Count_m1007388511_gshared)(__this, method)
// T System.Collections.ObjectModel.Collection`1<GameAnalyticsSDK.Settings/HelpTypes>::get_Item(System.Int32)
extern "C"  int32_t Collection_1_get_Item_m3698636249_gshared (Collection_1_t965375978 * __this, int32_t ___index0, const MethodInfo* method);
#define Collection_1_get_Item_m3698636249(__this, ___index0, method) ((  int32_t (*) (Collection_1_t965375978 *, int32_t, const MethodInfo*))Collection_1_get_Item_m3698636249_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.Collection`1<GameAnalyticsSDK.Settings/HelpTypes>::set_Item(System.Int32,T)
extern "C"  void Collection_1_set_Item_m4275204006_gshared (Collection_1_t965375978 * __this, int32_t ___index0, int32_t ___value1, const MethodInfo* method);
#define Collection_1_set_Item_m4275204006(__this, ___index0, ___value1, method) ((  void (*) (Collection_1_t965375978 *, int32_t, int32_t, const MethodInfo*))Collection_1_set_Item_m4275204006_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.ObjectModel.Collection`1<GameAnalyticsSDK.Settings/HelpTypes>::SetItem(System.Int32,T)
extern "C"  void Collection_1_SetItem_m564841491_gshared (Collection_1_t965375978 * __this, int32_t ___index0, int32_t ___item1, const MethodInfo* method);
#define Collection_1_SetItem_m564841491(__this, ___index0, ___item1, method) ((  void (*) (Collection_1_t965375978 *, int32_t, int32_t, const MethodInfo*))Collection_1_SetItem_m564841491_gshared)(__this, ___index0, ___item1, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<GameAnalyticsSDK.Settings/HelpTypes>::IsValidItem(System.Object)
extern "C"  bool Collection_1_IsValidItem_m4167855708_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * ___item0, const MethodInfo* method);
#define Collection_1_IsValidItem_m4167855708(__this /* static, unused */, ___item0, method) ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))Collection_1_IsValidItem_m4167855708_gshared)(__this /* static, unused */, ___item0, method)
// T System.Collections.ObjectModel.Collection`1<GameAnalyticsSDK.Settings/HelpTypes>::ConvertItem(System.Object)
extern "C"  int32_t Collection_1_ConvertItem_m3627929656_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * ___item0, const MethodInfo* method);
#define Collection_1_ConvertItem_m3627929656(__this /* static, unused */, ___item0, method) ((  int32_t (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))Collection_1_ConvertItem_m3627929656_gshared)(__this /* static, unused */, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<GameAnalyticsSDK.Settings/HelpTypes>::CheckWritable(System.Collections.Generic.IList`1<T>)
extern "C"  void Collection_1_CheckWritable_m498225112_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* ___list0, const MethodInfo* method);
#define Collection_1_CheckWritable_m498225112(__this /* static, unused */, ___list0, method) ((  void (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Collection_1_CheckWritable_m498225112_gshared)(__this /* static, unused */, ___list0, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<GameAnalyticsSDK.Settings/HelpTypes>::IsSynchronized(System.Collections.Generic.IList`1<T>)
extern "C"  bool Collection_1_IsSynchronized_m126106344_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* ___list0, const MethodInfo* method);
#define Collection_1_IsSynchronized_m126106344(__this /* static, unused */, ___list0, method) ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Collection_1_IsSynchronized_m126106344_gshared)(__this /* static, unused */, ___list0, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<GameAnalyticsSDK.Settings/HelpTypes>::IsFixedSize(System.Collections.Generic.IList`1<T>)
extern "C"  bool Collection_1_IsFixedSize_m3141940407_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* ___list0, const MethodInfo* method);
#define Collection_1_IsFixedSize_m3141940407(__this /* static, unused */, ___list0, method) ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Collection_1_IsFixedSize_m3141940407_gshared)(__this /* static, unused */, ___list0, method)
