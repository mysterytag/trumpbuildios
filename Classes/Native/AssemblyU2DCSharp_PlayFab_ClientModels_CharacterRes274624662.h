﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "AssemblyU2DCSharp_PlayFab_Internal_PlayFabResultCom379512675.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PlayFab.ClientModels.CharacterResult
struct  CharacterResult_t274624662  : public PlayFabResultCommon_t379512675
{
public:
	// System.String PlayFab.ClientModels.CharacterResult::<CharacterId>k__BackingField
	String_t* ___U3CCharacterIdU3Ek__BackingField_2;
	// System.String PlayFab.ClientModels.CharacterResult::<CharacterName>k__BackingField
	String_t* ___U3CCharacterNameU3Ek__BackingField_3;
	// System.String PlayFab.ClientModels.CharacterResult::<CharacterType>k__BackingField
	String_t* ___U3CCharacterTypeU3Ek__BackingField_4;

public:
	inline static int32_t get_offset_of_U3CCharacterIdU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(CharacterResult_t274624662, ___U3CCharacterIdU3Ek__BackingField_2)); }
	inline String_t* get_U3CCharacterIdU3Ek__BackingField_2() const { return ___U3CCharacterIdU3Ek__BackingField_2; }
	inline String_t** get_address_of_U3CCharacterIdU3Ek__BackingField_2() { return &___U3CCharacterIdU3Ek__BackingField_2; }
	inline void set_U3CCharacterIdU3Ek__BackingField_2(String_t* value)
	{
		___U3CCharacterIdU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier(&___U3CCharacterIdU3Ek__BackingField_2, value);
	}

	inline static int32_t get_offset_of_U3CCharacterNameU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(CharacterResult_t274624662, ___U3CCharacterNameU3Ek__BackingField_3)); }
	inline String_t* get_U3CCharacterNameU3Ek__BackingField_3() const { return ___U3CCharacterNameU3Ek__BackingField_3; }
	inline String_t** get_address_of_U3CCharacterNameU3Ek__BackingField_3() { return &___U3CCharacterNameU3Ek__BackingField_3; }
	inline void set_U3CCharacterNameU3Ek__BackingField_3(String_t* value)
	{
		___U3CCharacterNameU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier(&___U3CCharacterNameU3Ek__BackingField_3, value);
	}

	inline static int32_t get_offset_of_U3CCharacterTypeU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(CharacterResult_t274624662, ___U3CCharacterTypeU3Ek__BackingField_4)); }
	inline String_t* get_U3CCharacterTypeU3Ek__BackingField_4() const { return ___U3CCharacterTypeU3Ek__BackingField_4; }
	inline String_t** get_address_of_U3CCharacterTypeU3Ek__BackingField_4() { return &___U3CCharacterTypeU3Ek__BackingField_4; }
	inline void set_U3CCharacterTypeU3Ek__BackingField_4(String_t* value)
	{
		___U3CCharacterTypeU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier(&___U3CCharacterTypeU3Ek__BackingField_4, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
