﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// EditorNotificationManager
struct EditorNotificationManager_t3413835125;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String968488902.h"

// System.Void EditorNotificationManager::.ctor()
extern "C"  void EditorNotificationManager__ctor_m3566640022 (EditorNotificationManager_t3413835125 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EditorNotificationManager::SetNotification(System.String,System.Int32,System.String)
extern "C"  void EditorNotificationManager_SetNotification_m3282012082 (EditorNotificationManager_t3413835125 * __this, String_t* ___body0, int32_t ___delay1, String_t* ___title2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EditorNotificationManager::CancelAllNotification()
extern "C"  void EditorNotificationManager_CancelAllNotification_m1103503750 (EditorNotificationManager_t3413835125 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
