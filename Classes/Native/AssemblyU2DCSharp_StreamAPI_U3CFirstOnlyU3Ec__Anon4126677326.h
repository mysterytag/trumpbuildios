﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Action`1<System.Object>
struct Action_1_t985559125;
// CellUtils.SingleAssignmentDisposable
struct SingleAssignmentDisposable_t1832432170;
// StreamAPI/<FirstOnly>c__AnonStorey136`1<System.Object>
struct U3CFirstOnlyU3Ec__AnonStorey136_1_t276735029;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// StreamAPI/<FirstOnly>c__AnonStorey136`1/<FirstOnly>c__AnonStorey137`1<System.Object>
struct  U3CFirstOnlyU3Ec__AnonStorey137_1_t4126677326  : public Il2CppObject
{
public:
	// System.Action`1<T> StreamAPI/<FirstOnly>c__AnonStorey136`1/<FirstOnly>c__AnonStorey137`1::reaction
	Action_1_t985559125 * ___reaction_0;
	// CellUtils.SingleAssignmentDisposable StreamAPI/<FirstOnly>c__AnonStorey136`1/<FirstOnly>c__AnonStorey137`1::disp
	SingleAssignmentDisposable_t1832432170 * ___disp_1;
	// StreamAPI/<FirstOnly>c__AnonStorey136`1<T> StreamAPI/<FirstOnly>c__AnonStorey136`1/<FirstOnly>c__AnonStorey137`1::<>f__ref$310
	U3CFirstOnlyU3Ec__AnonStorey136_1_t276735029 * ___U3CU3Ef__refU24310_2;

public:
	inline static int32_t get_offset_of_reaction_0() { return static_cast<int32_t>(offsetof(U3CFirstOnlyU3Ec__AnonStorey137_1_t4126677326, ___reaction_0)); }
	inline Action_1_t985559125 * get_reaction_0() const { return ___reaction_0; }
	inline Action_1_t985559125 ** get_address_of_reaction_0() { return &___reaction_0; }
	inline void set_reaction_0(Action_1_t985559125 * value)
	{
		___reaction_0 = value;
		Il2CppCodeGenWriteBarrier(&___reaction_0, value);
	}

	inline static int32_t get_offset_of_disp_1() { return static_cast<int32_t>(offsetof(U3CFirstOnlyU3Ec__AnonStorey137_1_t4126677326, ___disp_1)); }
	inline SingleAssignmentDisposable_t1832432170 * get_disp_1() const { return ___disp_1; }
	inline SingleAssignmentDisposable_t1832432170 ** get_address_of_disp_1() { return &___disp_1; }
	inline void set_disp_1(SingleAssignmentDisposable_t1832432170 * value)
	{
		___disp_1 = value;
		Il2CppCodeGenWriteBarrier(&___disp_1, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__refU24310_2() { return static_cast<int32_t>(offsetof(U3CFirstOnlyU3Ec__AnonStorey137_1_t4126677326, ___U3CU3Ef__refU24310_2)); }
	inline U3CFirstOnlyU3Ec__AnonStorey136_1_t276735029 * get_U3CU3Ef__refU24310_2() const { return ___U3CU3Ef__refU24310_2; }
	inline U3CFirstOnlyU3Ec__AnonStorey136_1_t276735029 ** get_address_of_U3CU3Ef__refU24310_2() { return &___U3CU3Ef__refU24310_2; }
	inline void set_U3CU3Ef__refU24310_2(U3CFirstOnlyU3Ec__AnonStorey136_1_t276735029 * value)
	{
		___U3CU3Ef__refU24310_2 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__refU24310_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
