﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UniRx.Subject`1<System.Boolean>
struct Subject_1_t2149039961;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.BooleanNotifier
struct  BooleanNotifier_t3248530688  : public Il2CppObject
{
public:
	// UniRx.Subject`1<System.Boolean> UniRx.BooleanNotifier::boolTrigger
	Subject_1_t2149039961 * ___boolTrigger_0;
	// System.Boolean UniRx.BooleanNotifier::boolValue
	bool ___boolValue_1;

public:
	inline static int32_t get_offset_of_boolTrigger_0() { return static_cast<int32_t>(offsetof(BooleanNotifier_t3248530688, ___boolTrigger_0)); }
	inline Subject_1_t2149039961 * get_boolTrigger_0() const { return ___boolTrigger_0; }
	inline Subject_1_t2149039961 ** get_address_of_boolTrigger_0() { return &___boolTrigger_0; }
	inline void set_boolTrigger_0(Subject_1_t2149039961 * value)
	{
		___boolTrigger_0 = value;
		Il2CppCodeGenWriteBarrier(&___boolTrigger_0, value);
	}

	inline static int32_t get_offset_of_boolValue_1() { return static_cast<int32_t>(offsetof(BooleanNotifier_t3248530688, ___boolValue_1)); }
	inline bool get_boolValue_1() const { return ___boolValue_1; }
	inline bool* get_address_of_boolValue_1() { return &___boolValue_1; }
	inline void set_boolValue_1(bool value)
	{
		___boolValue_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
