﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Zenject.IInitializable
struct IInitializable_t617891835;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.InitializableManager/<InitializableManager>c__AnonStorey18F
struct  U3CInitializableManagerU3Ec__AnonStorey18F_t3916719836  : public Il2CppObject
{
public:
	// Zenject.IInitializable Zenject.InitializableManager/<InitializableManager>c__AnonStorey18F::initializable
	Il2CppObject * ___initializable_0;

public:
	inline static int32_t get_offset_of_initializable_0() { return static_cast<int32_t>(offsetof(U3CInitializableManagerU3Ec__AnonStorey18F_t3916719836, ___initializable_0)); }
	inline Il2CppObject * get_initializable_0() const { return ___initializable_0; }
	inline Il2CppObject ** get_address_of_initializable_0() { return &___initializable_0; }
	inline void set_initializable_0(Il2CppObject * value)
	{
		___initializable_0 = value;
		Il2CppCodeGenWriteBarrier(&___initializable_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
