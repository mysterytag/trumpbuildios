﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Observable/<CombineSources>c__IteratorD`1<System.Object>
struct U3CCombineSourcesU3Ec__IteratorD_1_t2571110558;
// UniRx.IObservable`1<System.Object>
struct IObservable_1_t595904784;
// System.Object
struct Il2CppObject;
// System.Collections.IEnumerator
struct IEnumerator_t287207039;
// System.Collections.Generic.IEnumerator`1<UniRx.IObservable`1<System.Object>>
struct IEnumerator_1_t2079011232;

#include "codegen/il2cpp-codegen.h"

// System.Void UniRx.Observable/<CombineSources>c__IteratorD`1<System.Object>::.ctor()
extern "C"  void U3CCombineSourcesU3Ec__IteratorD_1__ctor_m301271444_gshared (U3CCombineSourcesU3Ec__IteratorD_1_t2571110558 * __this, const MethodInfo* method);
#define U3CCombineSourcesU3Ec__IteratorD_1__ctor_m301271444(__this, method) ((  void (*) (U3CCombineSourcesU3Ec__IteratorD_1_t2571110558 *, const MethodInfo*))U3CCombineSourcesU3Ec__IteratorD_1__ctor_m301271444_gshared)(__this, method)
// UniRx.IObservable`1<T> UniRx.Observable/<CombineSources>c__IteratorD`1<System.Object>::System.Collections.Generic.IEnumerator<UniRx.IObservable<T>>.get_Current()
extern "C"  Il2CppObject* U3CCombineSourcesU3Ec__IteratorD_1_System_Collections_Generic_IEnumeratorU3CUniRx_IObservableU3CTU3EU3E_get_Current_m2795496324_gshared (U3CCombineSourcesU3Ec__IteratorD_1_t2571110558 * __this, const MethodInfo* method);
#define U3CCombineSourcesU3Ec__IteratorD_1_System_Collections_Generic_IEnumeratorU3CUniRx_IObservableU3CTU3EU3E_get_Current_m2795496324(__this, method) ((  Il2CppObject* (*) (U3CCombineSourcesU3Ec__IteratorD_1_t2571110558 *, const MethodInfo*))U3CCombineSourcesU3Ec__IteratorD_1_System_Collections_Generic_IEnumeratorU3CUniRx_IObservableU3CTU3EU3E_get_Current_m2795496324_gshared)(__this, method)
// System.Object UniRx.Observable/<CombineSources>c__IteratorD`1<System.Object>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CCombineSourcesU3Ec__IteratorD_1_System_Collections_IEnumerator_get_Current_m844946076_gshared (U3CCombineSourcesU3Ec__IteratorD_1_t2571110558 * __this, const MethodInfo* method);
#define U3CCombineSourcesU3Ec__IteratorD_1_System_Collections_IEnumerator_get_Current_m844946076(__this, method) ((  Il2CppObject * (*) (U3CCombineSourcesU3Ec__IteratorD_1_t2571110558 *, const MethodInfo*))U3CCombineSourcesU3Ec__IteratorD_1_System_Collections_IEnumerator_get_Current_m844946076_gshared)(__this, method)
// System.Collections.IEnumerator UniRx.Observable/<CombineSources>c__IteratorD`1<System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * U3CCombineSourcesU3Ec__IteratorD_1_System_Collections_IEnumerable_GetEnumerator_m3725167557_gshared (U3CCombineSourcesU3Ec__IteratorD_1_t2571110558 * __this, const MethodInfo* method);
#define U3CCombineSourcesU3Ec__IteratorD_1_System_Collections_IEnumerable_GetEnumerator_m3725167557(__this, method) ((  Il2CppObject * (*) (U3CCombineSourcesU3Ec__IteratorD_1_t2571110558 *, const MethodInfo*))U3CCombineSourcesU3Ec__IteratorD_1_System_Collections_IEnumerable_GetEnumerator_m3725167557_gshared)(__this, method)
// System.Collections.Generic.IEnumerator`1<UniRx.IObservable`1<T>> UniRx.Observable/<CombineSources>c__IteratorD`1<System.Object>::System.Collections.Generic.IEnumerable<UniRx.IObservable<T>>.GetEnumerator()
extern "C"  Il2CppObject* U3CCombineSourcesU3Ec__IteratorD_1_System_Collections_Generic_IEnumerableU3CUniRx_IObservableU3CTU3EU3E_GetEnumerator_m3078531331_gshared (U3CCombineSourcesU3Ec__IteratorD_1_t2571110558 * __this, const MethodInfo* method);
#define U3CCombineSourcesU3Ec__IteratorD_1_System_Collections_Generic_IEnumerableU3CUniRx_IObservableU3CTU3EU3E_GetEnumerator_m3078531331(__this, method) ((  Il2CppObject* (*) (U3CCombineSourcesU3Ec__IteratorD_1_t2571110558 *, const MethodInfo*))U3CCombineSourcesU3Ec__IteratorD_1_System_Collections_Generic_IEnumerableU3CUniRx_IObservableU3CTU3EU3E_GetEnumerator_m3078531331_gshared)(__this, method)
// System.Boolean UniRx.Observable/<CombineSources>c__IteratorD`1<System.Object>::MoveNext()
extern "C"  bool U3CCombineSourcesU3Ec__IteratorD_1_MoveNext_m3300122032_gshared (U3CCombineSourcesU3Ec__IteratorD_1_t2571110558 * __this, const MethodInfo* method);
#define U3CCombineSourcesU3Ec__IteratorD_1_MoveNext_m3300122032(__this, method) ((  bool (*) (U3CCombineSourcesU3Ec__IteratorD_1_t2571110558 *, const MethodInfo*))U3CCombineSourcesU3Ec__IteratorD_1_MoveNext_m3300122032_gshared)(__this, method)
// System.Void UniRx.Observable/<CombineSources>c__IteratorD`1<System.Object>::Dispose()
extern "C"  void U3CCombineSourcesU3Ec__IteratorD_1_Dispose_m1657331921_gshared (U3CCombineSourcesU3Ec__IteratorD_1_t2571110558 * __this, const MethodInfo* method);
#define U3CCombineSourcesU3Ec__IteratorD_1_Dispose_m1657331921(__this, method) ((  void (*) (U3CCombineSourcesU3Ec__IteratorD_1_t2571110558 *, const MethodInfo*))U3CCombineSourcesU3Ec__IteratorD_1_Dispose_m1657331921_gshared)(__this, method)
// System.Void UniRx.Observable/<CombineSources>c__IteratorD`1<System.Object>::Reset()
extern "C"  void U3CCombineSourcesU3Ec__IteratorD_1_Reset_m2242671681_gshared (U3CCombineSourcesU3Ec__IteratorD_1_t2571110558 * __this, const MethodInfo* method);
#define U3CCombineSourcesU3Ec__IteratorD_1_Reset_m2242671681(__this, method) ((  void (*) (U3CCombineSourcesU3Ec__IteratorD_1_t2571110558 *, const MethodInfo*))U3CCombineSourcesU3Ec__IteratorD_1_Reset_m2242671681_gshared)(__this, method)
