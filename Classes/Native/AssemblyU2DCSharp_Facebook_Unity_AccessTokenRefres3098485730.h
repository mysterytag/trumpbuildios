﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Facebook.Unity.AccessToken
struct AccessToken_t3144904884;

#include "AssemblyU2DCSharp_Facebook_Unity_ResultBase3940793997.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Facebook.Unity.AccessTokenRefreshResult
struct  AccessTokenRefreshResult_t3098485730  : public ResultBase_t3940793997
{
public:
	// Facebook.Unity.AccessToken Facebook.Unity.AccessTokenRefreshResult::<AccessToken>k__BackingField
	AccessToken_t3144904884 * ___U3CAccessTokenU3Ek__BackingField_9;

public:
	inline static int32_t get_offset_of_U3CAccessTokenU3Ek__BackingField_9() { return static_cast<int32_t>(offsetof(AccessTokenRefreshResult_t3098485730, ___U3CAccessTokenU3Ek__BackingField_9)); }
	inline AccessToken_t3144904884 * get_U3CAccessTokenU3Ek__BackingField_9() const { return ___U3CAccessTokenU3Ek__BackingField_9; }
	inline AccessToken_t3144904884 ** get_address_of_U3CAccessTokenU3Ek__BackingField_9() { return &___U3CAccessTokenU3Ek__BackingField_9; }
	inline void set_U3CAccessTokenU3Ek__BackingField_9(AccessToken_t3144904884 * value)
	{
		___U3CAccessTokenU3Ek__BackingField_9 = value;
		Il2CppCodeGenWriteBarrier(&___U3CAccessTokenU3Ek__BackingField_9, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
