﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_PlayFab_Internal_ResultContainer1789381198MethodDeclarations.h"

// System.Void PlayFab.Internal.ResultContainer`1<PlayFab.ClientModels.RegisterForIOSPushNotificationResult>::.ctor()
#define ResultContainer_1__ctor_m3758722839(__this, method) ((  void (*) (ResultContainer_1_t3437701395 *, const MethodInfo*))ResultContainer_1__ctor_m944826126_gshared)(__this, method)
// System.Void PlayFab.Internal.ResultContainer`1<PlayFab.ClientModels.RegisterForIOSPushNotificationResult>::.cctor()
#define ResultContainer_1__cctor_m74194806(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))ResultContainer_1__cctor_m3037709919_gshared)(__this /* static, unused */, method)
// PlayFab.Internal.ResultContainer`1<TResultType> PlayFab.Internal.ResultContainer`1<PlayFab.ClientModels.RegisterForIOSPushNotificationResult>::KillWarnings()
#define ResultContainer_1_KillWarnings_m3083799551(__this /* static, unused */, method) ((  ResultContainer_1_t3437701395 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))ResultContainer_1_KillWarnings_m2638698408_gshared)(__this /* static, unused */, method)
// TResultType PlayFab.Internal.ResultContainer`1<PlayFab.ClientModels.RegisterForIOSPushNotificationResult>::HandleResults(PlayFab.CallRequestContainer,System.Delegate,PlayFab.ErrorCallback,System.Action`2<TResultType,PlayFab.CallRequestContainer>)
#define ResultContainer_1_HandleResults_m1065364369(__this /* static, unused */, ___callRequest0, ___resultCallback1, ___errorCallback2, ___resultAction3, method) ((  RegisterForIOSPushNotificationResult_t2485426617 * (*) (Il2CppObject * /* static, unused */, CallRequestContainer_t432318609 *, Delegate_t3660574010 *, ErrorCallback_t582108558 *, Action_2_t455377650 *, const MethodInfo*))ResultContainer_1_HandleResults_m887745544_gshared)(__this /* static, unused */, ___callRequest0, ___resultCallback1, ___errorCallback2, ___resultAction3, method)
