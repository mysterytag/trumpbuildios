﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Zenject.BindScope/CustomScopeBinder`1<System.Object>
struct CustomScopeBinder_1_t4252514087;
// Zenject.BindScope
struct BindScope_t2945157996;
// System.String
struct String_t;
// Zenject.DiContainer
struct DiContainer_t2383114449;
// Zenject.SingletonProviderMap
struct SingletonProviderMap_t1557411893;
// Zenject.BindingConditionSetter
struct BindingConditionSetter_t259147722;
// Zenject.ProviderBase
struct ProviderBase_t1627494391;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Zenject_BindScope2945157996.h"
#include "mscorlib_System_String968488902.h"
#include "AssemblyU2DCSharp_Zenject_DiContainer2383114449.h"
#include "AssemblyU2DCSharp_Zenject_SingletonProviderMap1557411893.h"
#include "AssemblyU2DCSharp_Zenject_ProviderBase1627494391.h"

// System.Void Zenject.BindScope/CustomScopeBinder`1<System.Object>::.ctor(Zenject.BindScope,System.String,Zenject.DiContainer,Zenject.SingletonProviderMap)
extern "C"  void CustomScopeBinder_1__ctor_m1386595656_gshared (CustomScopeBinder_1_t4252514087 * __this, BindScope_t2945157996 * ___owner0, String_t* ___identifier1, DiContainer_t2383114449 * ___container2, SingletonProviderMap_t1557411893 * ___singletonMap3, const MethodInfo* method);
#define CustomScopeBinder_1__ctor_m1386595656(__this, ___owner0, ___identifier1, ___container2, ___singletonMap3, method) ((  void (*) (CustomScopeBinder_1_t4252514087 *, BindScope_t2945157996 *, String_t*, DiContainer_t2383114449 *, SingletonProviderMap_t1557411893 *, const MethodInfo*))CustomScopeBinder_1__ctor_m1386595656_gshared)(__this, ___owner0, ___identifier1, ___container2, ___singletonMap3, method)
// Zenject.BindingConditionSetter Zenject.BindScope/CustomScopeBinder`1<System.Object>::ToProvider(Zenject.ProviderBase)
extern "C"  BindingConditionSetter_t259147722 * CustomScopeBinder_1_ToProvider_m1928424210_gshared (CustomScopeBinder_1_t4252514087 * __this, ProviderBase_t1627494391 * ___provider0, const MethodInfo* method);
#define CustomScopeBinder_1_ToProvider_m1928424210(__this, ___provider0, method) ((  BindingConditionSetter_t259147722 * (*) (CustomScopeBinder_1_t4252514087 *, ProviderBase_t1627494391 *, const MethodInfo*))CustomScopeBinder_1_ToProvider_m1928424210_gshared)(__this, ___provider0, method)
