﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// TableViewCellBase
struct TableViewCellBase_t2057844710;

#include "codegen/il2cpp-codegen.h"

// System.Void TableViewCellBase::.ctor()
extern "C"  void TableViewCellBase__ctor_m3681876037 (TableViewCellBase_t2057844710 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TableViewCellBase::.cctor()
extern "C"  void TableViewCellBase__cctor_m1986911240 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TableViewCellBase::Awake()
extern "C"  void TableViewCellBase_Awake_m3919481256 (TableViewCellBase_t2057844710 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TableViewCellBase::Disable(System.Boolean)
extern "C"  void TableViewCellBase_Disable_m2038434818 (TableViewCellBase_t2057844710 * __this, bool ___b0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
