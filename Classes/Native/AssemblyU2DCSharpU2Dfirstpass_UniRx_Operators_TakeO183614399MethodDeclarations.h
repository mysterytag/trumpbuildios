﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.TakeObservable`1/Take_<System.Int64>
struct Take__t183614399;
// UniRx.Operators.TakeObservable`1<System.Int64>
struct TakeObservable_1_t794160655;
// UniRx.IObserver`1<System.Int64>
struct IObserver_1_t764446489;
// System.IDisposable
struct IDisposable_t1628921374;
// System.Exception
struct Exception_t1967233988;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Exception1967233988.h"

// System.Void UniRx.Operators.TakeObservable`1/Take_<System.Int64>::.ctor(UniRx.Operators.TakeObservable`1<T>,UniRx.IObserver`1<T>,System.IDisposable)
extern "C"  void Take___ctor_m3799520386_gshared (Take__t183614399 * __this, TakeObservable_1_t794160655 * ___parent0, Il2CppObject* ___observer1, Il2CppObject * ___cancel2, const MethodInfo* method);
#define Take___ctor_m3799520386(__this, ___parent0, ___observer1, ___cancel2, method) ((  void (*) (Take__t183614399 *, TakeObservable_1_t794160655 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))Take___ctor_m3799520386_gshared)(__this, ___parent0, ___observer1, ___cancel2, method)
// System.IDisposable UniRx.Operators.TakeObservable`1/Take_<System.Int64>::Run()
extern "C"  Il2CppObject * Take__Run_m4022569518_gshared (Take__t183614399 * __this, const MethodInfo* method);
#define Take__Run_m4022569518(__this, method) ((  Il2CppObject * (*) (Take__t183614399 *, const MethodInfo*))Take__Run_m4022569518_gshared)(__this, method)
// System.Void UniRx.Operators.TakeObservable`1/Take_<System.Int64>::Tick()
extern "C"  void Take__Tick_m4004564353_gshared (Take__t183614399 * __this, const MethodInfo* method);
#define Take__Tick_m4004564353(__this, method) ((  void (*) (Take__t183614399 *, const MethodInfo*))Take__Tick_m4004564353_gshared)(__this, method)
// System.Void UniRx.Operators.TakeObservable`1/Take_<System.Int64>::OnNext(T)
extern "C"  void Take__OnNext_m1983155400_gshared (Take__t183614399 * __this, int64_t ___value0, const MethodInfo* method);
#define Take__OnNext_m1983155400(__this, ___value0, method) ((  void (*) (Take__t183614399 *, int64_t, const MethodInfo*))Take__OnNext_m1983155400_gshared)(__this, ___value0, method)
// System.Void UniRx.Operators.TakeObservable`1/Take_<System.Int64>::OnError(System.Exception)
extern "C"  void Take__OnError_m1670780887_gshared (Take__t183614399 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method);
#define Take__OnError_m1670780887(__this, ___error0, method) ((  void (*) (Take__t183614399 *, Exception_t1967233988 *, const MethodInfo*))Take__OnError_m1670780887_gshared)(__this, ___error0, method)
// System.Void UniRx.Operators.TakeObservable`1/Take_<System.Int64>::OnCompleted()
extern "C"  void Take__OnCompleted_m1361089962_gshared (Take__t183614399 * __this, const MethodInfo* method);
#define Take__OnCompleted_m1361089962(__this, method) ((  void (*) (Take__t183614399 *, const MethodInfo*))Take__OnCompleted_m1361089962_gshared)(__this, method)
