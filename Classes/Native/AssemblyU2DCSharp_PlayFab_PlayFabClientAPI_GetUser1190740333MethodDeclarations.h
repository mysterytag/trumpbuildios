﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.PlayFabClientAPI/GetUserPublisherReadOnlyDataResponseCallback
struct GetUserPublisherReadOnlyDataResponseCallback_t1190740333;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;
// PlayFab.ClientModels.GetUserDataRequest
struct GetUserDataRequest_t2084642996;
// PlayFab.ClientModels.GetUserDataResult
struct GetUserDataResult_t518036344;
// PlayFab.PlayFabError
struct PlayFabError_t750598646;
// System.IAsyncResult
struct IAsyncResult_t537683269;
// System.AsyncCallback
struct AsyncCallback_t1363551830;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_IntPtr676692020.h"
#include "mscorlib_System_String968488902.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_GetUserData2084642996.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_GetUserDataR518036344.h"
#include "AssemblyU2DCSharpU2Dfirstpass_PlayFab_PlayFabError750598646.h"
#include "mscorlib_System_AsyncCallback1363551830.h"

// System.Void PlayFab.PlayFabClientAPI/GetUserPublisherReadOnlyDataResponseCallback::.ctor(System.Object,System.IntPtr)
extern "C"  void GetUserPublisherReadOnlyDataResponseCallback__ctor_m1714030556 (GetUserPublisherReadOnlyDataResponseCallback_t1190740333 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/GetUserPublisherReadOnlyDataResponseCallback::Invoke(System.String,System.Int32,PlayFab.ClientModels.GetUserDataRequest,PlayFab.ClientModels.GetUserDataResult,PlayFab.PlayFabError,System.Object)
extern "C"  void GetUserPublisherReadOnlyDataResponseCallback_Invoke_m3968611357 (GetUserPublisherReadOnlyDataResponseCallback_t1190740333 * __this, String_t* ___urlPath0, int32_t ___callId1, GetUserDataRequest_t2084642996 * ___request2, GetUserDataResult_t518036344 * ___result3, PlayFabError_t750598646 * ___error4, Il2CppObject * ___customData5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
extern "C" void pinvoke_delegate_wrapper_GetUserPublisherReadOnlyDataResponseCallback_t1190740333(Il2CppObject* delegate, String_t* ___urlPath0, int32_t ___callId1, GetUserDataRequest_t2084642996 * ___request2, GetUserDataResult_t518036344 * ___result3, PlayFabError_t750598646 * ___error4, Il2CppObject * ___customData5);
// System.IAsyncResult PlayFab.PlayFabClientAPI/GetUserPublisherReadOnlyDataResponseCallback::BeginInvoke(System.String,System.Int32,PlayFab.ClientModels.GetUserDataRequest,PlayFab.ClientModels.GetUserDataResult,PlayFab.PlayFabError,System.Object,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * GetUserPublisherReadOnlyDataResponseCallback_BeginInvoke_m3195953442 (GetUserPublisherReadOnlyDataResponseCallback_t1190740333 * __this, String_t* ___urlPath0, int32_t ___callId1, GetUserDataRequest_t2084642996 * ___request2, GetUserDataResult_t518036344 * ___result3, PlayFabError_t750598646 * ___error4, Il2CppObject * ___customData5, AsyncCallback_t1363551830 * ___callback6, Il2CppObject * ___object7, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/GetUserPublisherReadOnlyDataResponseCallback::EndInvoke(System.IAsyncResult)
extern "C"  void GetUserPublisherReadOnlyDataResponseCallback_EndInvoke_m3029583852 (GetUserPublisherReadOnlyDataResponseCallback_t1190740333 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
