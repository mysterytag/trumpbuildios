﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Facebook.Unity.Canvas.CanvasFacebook/<FormatAuthResponse>c__AnonStorey52
struct U3CFormatAuthResponseU3Ec__AnonStorey52_t1449625442;
// Facebook.Unity.IGraphResult
struct IGraphResult_t2342947041;

#include "codegen/il2cpp-codegen.h"

// System.Void Facebook.Unity.Canvas.CanvasFacebook/<FormatAuthResponse>c__AnonStorey52::.ctor()
extern "C"  void U3CFormatAuthResponseU3Ec__AnonStorey52__ctor_m1497297749 (U3CFormatAuthResponseU3Ec__AnonStorey52_t1449625442 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Facebook.Unity.Canvas.CanvasFacebook/<FormatAuthResponse>c__AnonStorey52::<>m__C(Facebook.Unity.IGraphResult)
extern "C"  void U3CFormatAuthResponseU3Ec__AnonStorey52_U3CU3Em__C_m229062632 (U3CFormatAuthResponseU3Ec__AnonStorey52_t1449625442 * __this, Il2CppObject * ___r0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
