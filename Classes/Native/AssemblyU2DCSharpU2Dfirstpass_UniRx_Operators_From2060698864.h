﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UniRx.Operators.FromEventObservable
struct FromEventObservable_t1591372576;
// UniRx.IObserver`1<UniRx.Unit>
struct IObserver_1_t475317645;
// System.Action
struct Action_t437523947;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Operators.FromEventObservable/FromEvent
struct  FromEvent_t2060698864  : public Il2CppObject
{
public:
	// UniRx.Operators.FromEventObservable UniRx.Operators.FromEventObservable/FromEvent::parent
	FromEventObservable_t1591372576 * ___parent_0;
	// UniRx.IObserver`1<UniRx.Unit> UniRx.Operators.FromEventObservable/FromEvent::observer
	Il2CppObject* ___observer_1;
	// System.Action UniRx.Operators.FromEventObservable/FromEvent::handler
	Action_t437523947 * ___handler_2;

public:
	inline static int32_t get_offset_of_parent_0() { return static_cast<int32_t>(offsetof(FromEvent_t2060698864, ___parent_0)); }
	inline FromEventObservable_t1591372576 * get_parent_0() const { return ___parent_0; }
	inline FromEventObservable_t1591372576 ** get_address_of_parent_0() { return &___parent_0; }
	inline void set_parent_0(FromEventObservable_t1591372576 * value)
	{
		___parent_0 = value;
		Il2CppCodeGenWriteBarrier(&___parent_0, value);
	}

	inline static int32_t get_offset_of_observer_1() { return static_cast<int32_t>(offsetof(FromEvent_t2060698864, ___observer_1)); }
	inline Il2CppObject* get_observer_1() const { return ___observer_1; }
	inline Il2CppObject** get_address_of_observer_1() { return &___observer_1; }
	inline void set_observer_1(Il2CppObject* value)
	{
		___observer_1 = value;
		Il2CppCodeGenWriteBarrier(&___observer_1, value);
	}

	inline static int32_t get_offset_of_handler_2() { return static_cast<int32_t>(offsetof(FromEvent_t2060698864, ___handler_2)); }
	inline Action_t437523947 * get_handler_2() const { return ___handler_2; }
	inline Action_t437523947 ** get_address_of_handler_2() { return &___handler_2; }
	inline void set_handler_2(Action_t437523947 * value)
	{
		___handler_2 = value;
		Il2CppCodeGenWriteBarrier(&___handler_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
