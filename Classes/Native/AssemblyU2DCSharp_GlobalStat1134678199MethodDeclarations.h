﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GlobalStat
struct GlobalStat_t1134678199;
// System.Double[]
struct DoubleU5BU5D_t1048280995;
// System.String
struct String_t;
// System.Collections.IEnumerator
struct IEnumerator_t287207039;
// DateTimeCell
struct DateTimeCell_t773798845;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_GlobalStat_UpgradeType4118271830.h"
#include "UnityEngine_UnityEngine_Color1588175760.h"
#include "AssemblyU2DCSharp_DateTimeCell773798845.h"

// System.Void GlobalStat::.ctor()
extern "C"  void GlobalStat__ctor_m699639236 (GlobalStat_t1134678199 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GlobalStat::.cctor()
extern "C"  void GlobalStat__cctor_m4026850921 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GlobalStat::AddMoney(System.Double)
extern "C"  void GlobalStat_AddMoney_m3841285443 (GlobalStat_t1134678199 * __this, double ___count0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GlobalStat::SpendMoney(System.Double)
extern "C"  void GlobalStat_SpendMoney_m1805431104 (GlobalStat_t1134678199 * __this, double ___count0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Double GlobalStat::get_passiveIncome()
extern "C"  double GlobalStat_get_passiveIncome_m844851078 (GlobalStat_t1134678199 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Double GlobalStat::get_just_passiveIncome()
extern "C"  double GlobalStat_get_just_passiveIncome_m2365866409 (GlobalStat_t1134678199 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Double GlobalStat::get_bucketIncome()
extern "C"  double GlobalStat_get_bucketIncome_m1406208767 (GlobalStat_t1134678199 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GlobalStat::set_bucketIncome(System.Double)
extern "C"  void GlobalStat_set_bucketIncome_m2397274098 (GlobalStat_t1134678199 * __this, double ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Double GlobalStat::get_bucketSize()
extern "C"  double GlobalStat_get_bucketSize_m2022356119 (GlobalStat_t1134678199 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GlobalStat::set_bucketSize(System.Double)
extern "C"  void GlobalStat_set_bucketSize_m1727018266 (GlobalStat_t1134678199 * __this, double ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Double GlobalStat::get_achievSize()
extern "C"  double GlobalStat_get_achievSize_m4286008257 (GlobalStat_t1134678199 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GlobalStat::set_achievSize(System.Double)
extern "C"  void GlobalStat_set_achievSize_m1607733808 (GlobalStat_t1134678199 * __this, double ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Double GlobalStat::get_achivIncom()
extern "C"  double GlobalStat_get_achivIncom_m3333178837 (GlobalStat_t1134678199 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GlobalStat::set_achivIncom(System.Double)
extern "C"  void GlobalStat_set_achivIncom_m693704860 (GlobalStat_t1134678199 * __this, double ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GlobalStat::InitiatePurchase()
extern "C"  void GlobalStat_InitiatePurchase_m820888762 (GlobalStat_t1134678199 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GlobalStat::CheckPurchase()
extern "C"  bool GlobalStat_CheckPurchase_m3126262423 (GlobalStat_t1134678199 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Double GlobalStat::get_currentBillSize()
extern "C"  double GlobalStat_get_currentBillSize_m1591269271 (GlobalStat_t1134678199 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 GlobalStat::get_currentCityLevel()
extern "C"  int32_t GlobalStat_get_currentCityLevel_m3497299543 (GlobalStat_t1134678199 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 GlobalStat::get_currentHolderNumber()
extern "C"  int32_t GlobalStat_get_currentHolderNumber_m850723833 (GlobalStat_t1134678199 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GlobalStat::PostInject()
extern "C"  void GlobalStat_PostInject_m103508433 (GlobalStat_t1134678199 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Double[] GlobalStat::get_billSizes()
extern "C"  DoubleU5BU5D_t1048280995* GlobalStat_get_billSizes_m1862193027 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GlobalStat::SaveStats()
extern "C"  void GlobalStat_SaveStats_m192277252 (GlobalStat_t1134678199 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GlobalStat::ReportSwipeChain()
extern "C"  void GlobalStat_ReportSwipeChain_m3742925243 (GlobalStat_t1134678199 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String GlobalStat::get_savePath()
extern "C"  String_t* GlobalStat_get_savePath_m3680061326 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// GlobalStat GlobalStat::LoadStats()
extern "C"  GlobalStat_t1134678199 * GlobalStat_LoadStats_m555077397 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Double GlobalStat::GetFriendReward()
extern "C"  double GlobalStat_GetFriendReward_m1979181890 (GlobalStat_t1134678199 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Double GlobalStat::GetJoinReward()
extern "C"  double GlobalStat_GetJoinReward_m3443320526 (GlobalStat_t1134678199 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Double GlobalStat::GetLoginReward()
extern "C"  double GlobalStat_GetLoginReward_m3060094565 (GlobalStat_t1134678199 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Double GlobalStat::GetBequestReward()
extern "C"  double GlobalStat_GetBequestReward_m3567060091 (GlobalStat_t1134678199 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Double GlobalStat::GetBequestReward(System.Int32)
extern "C"  double GlobalStat_GetBequestReward_m2360924492 (GlobalStat_t1134678199 * __this, int32_t ___pack0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GlobalStat::StartDoubleEarning()
extern "C"  void GlobalStat_StartDoubleEarning_m4058546615 (GlobalStat_t1134678199 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GlobalStat::ResetProgress()
extern "C"  void GlobalStat_ResetProgress_m1243572094 (GlobalStat_t1134678199 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GlobalStat::ClearProgress()
extern "C"  void GlobalStat_ClearProgress_m3751435004 (GlobalStat_t1134678199 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GlobalStat::LoadDefaults()
extern "C"  void GlobalStat_LoadDefaults_m2016259928 (GlobalStat_t1134678199 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 GlobalStat::GetUpgradeLevel(System.Int32,GlobalStat/UpgradeType)
extern "C"  int32_t GlobalStat_GetUpgradeLevel_m3006657733 (GlobalStat_t1134678199 * __this, int32_t ___number0, int32_t ___type1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Double GlobalStat::GetUpgradePrice(System.Int32,GlobalStat/UpgradeType)
extern "C"  double GlobalStat_GetUpgradePrice_m873091701 (GlobalStat_t1134678199 * __this, int32_t ___number0, int32_t ___type1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Color GlobalStat::GetUpgradeColor(System.Int32)
extern "C"  Color_t1588175760  GlobalStat_GetUpgradeColor_m567515625 (Il2CppObject * __this /* static, unused */, int32_t ___number0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 GlobalStat::GetBillUpgradeNumber()
extern "C"  int32_t GlobalStat_GetBillUpgradeNumber_m824234134 (GlobalStat_t1134678199 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 GlobalStat::GetBillNumber()
extern "C"  int32_t GlobalStat_GetBillNumber_m1138945978 (GlobalStat_t1134678199 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 GlobalStat::GetMaxSafeUpgrade()
extern "C"  int32_t GlobalStat_GetMaxSafeUpgrade_m1147814933 (GlobalStat_t1134678199 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 GlobalStat::GetMaxPassiveUpgrade()
extern "C"  int32_t GlobalStat_GetMaxPassiveUpgrade_m2587708817 (GlobalStat_t1134678199 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Double GlobalStat::GetCurrentBillSize()
extern "C"  double GlobalStat_GetCurrentBillSize_m3224899438 (GlobalStat_t1134678199 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Double GlobalStat::GetUpgradeAmount(System.Int32,GlobalStat/UpgradeType)
extern "C"  double GlobalStat_GetUpgradeAmount_m2501626998 (GlobalStat_t1134678199 * __this, int32_t ___number0, int32_t ___type1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 GlobalStat::GetCurrentBillUpgrade()
extern "C"  int32_t GlobalStat_GetCurrentBillUpgrade_m2582613606 (GlobalStat_t1134678199 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String GlobalStat::GetLastUpgradeName()
extern "C"  String_t* GlobalStat_GetLastUpgradeName_m94752830 (GlobalStat_t1134678199 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GlobalStat::DismissLastUpgrade()
extern "C"  void GlobalStat_DismissLastUpgrade_m1616468860 (GlobalStat_t1134678199 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GlobalStat::DoubleRevenue()
extern "C"  void GlobalStat_DoubleRevenue_m3356468781 (GlobalStat_t1134678199 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GlobalStat::TwentyRevenue(System.Int32)
extern "C"  void GlobalStat_TwentyRevenue_m675636702 (GlobalStat_t1134678199 * __this, int32_t ___sec0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GlobalStat::CheckUpgrade(System.Int32,GlobalStat/UpgradeType,System.Double)
extern "C"  bool GlobalStat_CheckUpgrade_m3204598443 (GlobalStat_t1134678199 * __this, int32_t ___number0, int32_t ___type1, double ___packMoney2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GlobalStat::BuyUpgrade(System.Int32,GlobalStat/UpgradeType)
extern "C"  bool GlobalStat_BuyUpgrade_m2139963437 (GlobalStat_t1134678199 * __this, int32_t ___number0, int32_t ___type1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GlobalStat::AllBuyed(GlobalStat/UpgradeType)
extern "C"  bool GlobalStat_AllBuyed_m3558239034 (GlobalStat_t1134678199 * __this, int32_t ___upgType0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator GlobalStat::DoubleRevenueListCheck()
extern "C"  Il2CppObject * GlobalStat_DoubleRevenueListCheck_m19919879 (GlobalStat_t1134678199 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator GlobalStat::TwentyRevenueListCheck()
extern "C"  Il2CppObject * GlobalStat_TwentyRevenueListCheck_m2416171559 (GlobalStat_t1134678199 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GlobalStat::Init()
extern "C"  void GlobalStat_Init_m3714883600 (GlobalStat_t1134678199 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GlobalStat::<PostInject>m__FE(System.Double)
extern "C"  void GlobalStat_U3CPostInjectU3Em__FE_m1683126437 (GlobalStat_t1134678199 * __this, double ___d0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GlobalStat::<PostInject>m__FF(System.Int64)
extern "C"  bool GlobalStat_U3CPostInjectU3Em__FF_m3048365596 (Il2CppObject * __this /* static, unused */, int64_t ___l0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// DateTimeCell GlobalStat::<PostInject>m__100(System.Int64)
extern "C"  DateTimeCell_t773798845 * GlobalStat_U3CPostInjectU3Em__100_m1649759751 (Il2CppObject * __this /* static, unused */, int64_t ___l0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// DateTimeCell GlobalStat::<PostInject>m__101(System.Int64)
extern "C"  DateTimeCell_t773798845 * GlobalStat_U3CPostInjectU3Em__101_m3157311560 (Il2CppObject * __this /* static, unused */, int64_t ___l0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GlobalStat::<PostInject>m__102(System.Int64)
extern "C"  void GlobalStat_U3CPostInjectU3Em__102_m2066168937 (GlobalStat_t1134678199 * __this, int64_t _____0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GlobalStat::<PostInject>m__103(System.Int64)
extern "C"  bool GlobalStat_U3CPostInjectU3Em__103_m2299549534 (GlobalStat_t1134678199 * __this, int64_t ___l0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 GlobalStat::<PostInject>m__104(System.Int64)
extern "C"  int32_t GlobalStat_U3CPostInjectU3Em__104_m2768295417 (GlobalStat_t1134678199 * __this, int64_t ___l0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GlobalStat::<PostInject>m__105(System.Int64)
extern "C"  bool GlobalStat_U3CPostInjectU3Em__105_m1019685856 (GlobalStat_t1134678199 * __this, int64_t ____0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GlobalStat::<PostInject>m__106(System.Double)
extern "C"  void GlobalStat_U3CPostInjectU3Em__106_m1767926279 (GlobalStat_t1134678199 * __this, double ___d0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GlobalStat::<PostInject>m__107(System.Int64)
extern "C"  void GlobalStat_U3CPostInjectU3Em__107_m1013993390 (GlobalStat_t1134678199 * __this, int64_t ____0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 GlobalStat::<SaveStats>m__108(DateTimeCell)
extern "C"  int64_t GlobalStat_U3CSaveStatsU3Em__108_m570733690 (Il2CppObject * __this /* static, unused */, DateTimeCell_t773798845 * ___timeCell0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 GlobalStat::<SaveStats>m__109(DateTimeCell)
extern "C"  int64_t GlobalStat_U3CSaveStatsU3Em__109_m2078285499 (Il2CppObject * __this /* static, unused */, DateTimeCell_t773798845 * ___timeCell0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GlobalStat::<ReportSwipeChain>m__10A(System.Boolean)
extern "C"  void GlobalStat_U3CReportSwipeChainU3Em__10A_m1939281459 (Il2CppObject * __this /* static, unused */, bool ___success0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
