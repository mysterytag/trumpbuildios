﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Zenject.PrefabSingletonProvider
struct PrefabSingletonProvider_t2854807885;
// Zenject.DiContainer
struct DiContainer_t2383114449;
// System.Type
struct Type_t;
// Zenject.PrefabSingletonLazyCreator
struct PrefabSingletonLazyCreator_t3719004230;
// System.Object
struct Il2CppObject;
// Zenject.InjectContext
struct InjectContext_t3456483891;
// System.Collections.Generic.IEnumerable`1<Zenject.ZenjectResolveException>
struct IEnumerable_1_t4073207355;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Zenject_DiContainer2383114449.h"
#include "mscorlib_System_Type2779229935.h"
#include "AssemblyU2DCSharp_Zenject_PrefabSingletonLazyCreat3719004230.h"
#include "AssemblyU2DCSharp_Zenject_InjectContext3456483891.h"

// System.Void Zenject.PrefabSingletonProvider::.ctor(Zenject.DiContainer,System.Type,Zenject.PrefabSingletonLazyCreator)
extern "C"  void PrefabSingletonProvider__ctor_m4183108064 (PrefabSingletonProvider_t2854807885 * __this, DiContainer_t2383114449 * ___container0, Type_t * ___concreteType1, PrefabSingletonLazyCreator_t3719004230 * ___creator2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Zenject.PrefabSingletonProvider::Dispose()
extern "C"  void PrefabSingletonProvider_Dispose_m2388882031 (PrefabSingletonProvider_t2854807885 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type Zenject.PrefabSingletonProvider::GetInstanceType()
extern "C"  Type_t * PrefabSingletonProvider_GetInstanceType_m519112783 (PrefabSingletonProvider_t2854807885 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Zenject.PrefabSingletonProvider::GetInstance(Zenject.InjectContext)
extern "C"  Il2CppObject * PrefabSingletonProvider_GetInstance_m763755459 (PrefabSingletonProvider_t2854807885 * __this, InjectContext_t3456483891 * ___context0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.IEnumerable`1<Zenject.ZenjectResolveException> Zenject.PrefabSingletonProvider::ValidateBinding(Zenject.InjectContext)
extern "C"  Il2CppObject* PrefabSingletonProvider_ValidateBinding_m837728981 (PrefabSingletonProvider_t2854807885 * __this, InjectContext_t3456483891 * ___context0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
