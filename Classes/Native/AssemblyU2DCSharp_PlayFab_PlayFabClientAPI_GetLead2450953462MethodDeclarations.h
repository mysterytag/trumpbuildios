﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.PlayFabClientAPI/GetLeaderboardAroundCurrentUserResponseCallback
struct GetLeaderboardAroundCurrentUserResponseCallback_t2450953462;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;
// PlayFab.ClientModels.GetLeaderboardAroundCurrentUserRequest
struct GetLeaderboardAroundCurrentUserRequest_t545060687;
// PlayFab.ClientModels.GetLeaderboardAroundCurrentUserResult
struct GetLeaderboardAroundCurrentUserResult_t3100771709;
// PlayFab.PlayFabError
struct PlayFabError_t750598646;
// System.IAsyncResult
struct IAsyncResult_t537683269;
// System.AsyncCallback
struct AsyncCallback_t1363551830;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_IntPtr676692020.h"
#include "mscorlib_System_String968488902.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_GetLeaderboa545060687.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_GetLeaderbo3100771709.h"
#include "AssemblyU2DCSharpU2Dfirstpass_PlayFab_PlayFabError750598646.h"
#include "mscorlib_System_AsyncCallback1363551830.h"

// System.Void PlayFab.PlayFabClientAPI/GetLeaderboardAroundCurrentUserResponseCallback::.ctor(System.Object,System.IntPtr)
extern "C"  void GetLeaderboardAroundCurrentUserResponseCallback__ctor_m1650441861 (GetLeaderboardAroundCurrentUserResponseCallback_t2450953462 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/GetLeaderboardAroundCurrentUserResponseCallback::Invoke(System.String,System.Int32,PlayFab.ClientModels.GetLeaderboardAroundCurrentUserRequest,PlayFab.ClientModels.GetLeaderboardAroundCurrentUserResult,PlayFab.PlayFabError,System.Object)
extern "C"  void GetLeaderboardAroundCurrentUserResponseCallback_Invoke_m3916417552 (GetLeaderboardAroundCurrentUserResponseCallback_t2450953462 * __this, String_t* ___urlPath0, int32_t ___callId1, GetLeaderboardAroundCurrentUserRequest_t545060687 * ___request2, GetLeaderboardAroundCurrentUserResult_t3100771709 * ___result3, PlayFabError_t750598646 * ___error4, Il2CppObject * ___customData5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
extern "C" void pinvoke_delegate_wrapper_GetLeaderboardAroundCurrentUserResponseCallback_t2450953462(Il2CppObject* delegate, String_t* ___urlPath0, int32_t ___callId1, GetLeaderboardAroundCurrentUserRequest_t545060687 * ___request2, GetLeaderboardAroundCurrentUserResult_t3100771709 * ___result3, PlayFabError_t750598646 * ___error4, Il2CppObject * ___customData5);
// System.IAsyncResult PlayFab.PlayFabClientAPI/GetLeaderboardAroundCurrentUserResponseCallback::BeginInvoke(System.String,System.Int32,PlayFab.ClientModels.GetLeaderboardAroundCurrentUserRequest,PlayFab.ClientModels.GetLeaderboardAroundCurrentUserResult,PlayFab.PlayFabError,System.Object,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * GetLeaderboardAroundCurrentUserResponseCallback_BeginInvoke_m3623775817 (GetLeaderboardAroundCurrentUserResponseCallback_t2450953462 * __this, String_t* ___urlPath0, int32_t ___callId1, GetLeaderboardAroundCurrentUserRequest_t545060687 * ___request2, GetLeaderboardAroundCurrentUserResult_t3100771709 * ___result3, PlayFabError_t750598646 * ___error4, Il2CppObject * ___customData5, AsyncCallback_t1363551830 * ___callback6, Il2CppObject * ___object7, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/GetLeaderboardAroundCurrentUserResponseCallback::EndInvoke(System.IAsyncResult)
extern "C"  void GetLeaderboardAroundCurrentUserResponseCallback_EndInvoke_m3564714773 (GetLeaderboardAroundCurrentUserResponseCallback_t2450953462 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
