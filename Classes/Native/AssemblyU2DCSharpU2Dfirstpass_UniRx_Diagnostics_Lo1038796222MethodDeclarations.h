﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.IDisposable
struct IDisposable_t1628921374;
// UniRx.IObservable`1<UniRx.Diagnostics.LogEntry>
struct IObservable_1_t1649954086;

#include "codegen/il2cpp-codegen.h"

// System.IDisposable UniRx.Diagnostics.LogEntryExtensions::LogToUnityDebug(UniRx.IObservable`1<UniRx.Diagnostics.LogEntry>)
extern "C"  Il2CppObject * LogEntryExtensions_LogToUnityDebug_m3186420352 (Il2CppObject * __this /* static, unused */, Il2CppObject* ___source0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
