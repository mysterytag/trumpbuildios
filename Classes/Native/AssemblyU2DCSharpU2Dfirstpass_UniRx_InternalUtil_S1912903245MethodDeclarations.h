﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.InternalUtil.ScheduledItem
struct ScheduledItem_t1912903245;
// System.Action
struct Action_t437523947;
// System.Object
struct Il2CppObject;
// System.IDisposable
struct IDisposable_t1628921374;

#include "codegen/il2cpp-codegen.h"
#include "System_Core_System_Action437523947.h"
#include "mscorlib_System_TimeSpan763862892.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_S1912903245.h"
#include "mscorlib_System_Object837106420.h"

// System.Void UniRx.InternalUtil.ScheduledItem::.ctor(System.Action,System.TimeSpan)
extern "C"  void ScheduledItem__ctor_m3322249709 (ScheduledItem_t1912903245 * __this, Action_t437523947 * ___action0, TimeSpan_t763862892  ___dueTime1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.TimeSpan UniRx.InternalUtil.ScheduledItem::get_DueTime()
extern "C"  TimeSpan_t763862892  ScheduledItem_get_DueTime_m852343711 (ScheduledItem_t1912903245 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.InternalUtil.ScheduledItem::Invoke()
extern "C"  void ScheduledItem_Invoke_m1551717398 (ScheduledItem_t1912903245 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UniRx.InternalUtil.ScheduledItem::CompareTo(UniRx.InternalUtil.ScheduledItem)
extern "C"  int32_t ScheduledItem_CompareTo_m2869093541 (ScheduledItem_t1912903245 * __this, ScheduledItem_t1912903245 * ___other0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UniRx.InternalUtil.ScheduledItem::Equals(System.Object)
extern "C"  bool ScheduledItem_Equals_m1129861627 (ScheduledItem_t1912903245 * __this, Il2CppObject * ___obj0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UniRx.InternalUtil.ScheduledItem::GetHashCode()
extern "C"  int32_t ScheduledItem_GetHashCode_m1775533855 (ScheduledItem_t1912903245 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IDisposable UniRx.InternalUtil.ScheduledItem::get_Cancellation()
extern "C"  Il2CppObject * ScheduledItem_get_Cancellation_m836388197 (ScheduledItem_t1912903245 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UniRx.InternalUtil.ScheduledItem::get_IsCanceled()
extern "C"  bool ScheduledItem_get_IsCanceled_m950977702 (ScheduledItem_t1912903245 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UniRx.InternalUtil.ScheduledItem::op_LessThan(UniRx.InternalUtil.ScheduledItem,UniRx.InternalUtil.ScheduledItem)
extern "C"  bool ScheduledItem_op_LessThan_m3567740998 (Il2CppObject * __this /* static, unused */, ScheduledItem_t1912903245 * ___left0, ScheduledItem_t1912903245 * ___right1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UniRx.InternalUtil.ScheduledItem::op_LessThanOrEqual(UniRx.InternalUtil.ScheduledItem,UniRx.InternalUtil.ScheduledItem)
extern "C"  bool ScheduledItem_op_LessThanOrEqual_m452202771 (Il2CppObject * __this /* static, unused */, ScheduledItem_t1912903245 * ___left0, ScheduledItem_t1912903245 * ___right1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UniRx.InternalUtil.ScheduledItem::op_GreaterThan(UniRx.InternalUtil.ScheduledItem,UniRx.InternalUtil.ScheduledItem)
extern "C"  bool ScheduledItem_op_GreaterThan_m1585086255 (Il2CppObject * __this /* static, unused */, ScheduledItem_t1912903245 * ___left0, ScheduledItem_t1912903245 * ___right1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UniRx.InternalUtil.ScheduledItem::op_GreaterThanOrEqual(UniRx.InternalUtil.ScheduledItem,UniRx.InternalUtil.ScheduledItem)
extern "C"  bool ScheduledItem_op_GreaterThanOrEqual_m1401819658 (Il2CppObject * __this /* static, unused */, ScheduledItem_t1912903245 * ___left0, ScheduledItem_t1912903245 * ___right1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UniRx.InternalUtil.ScheduledItem::op_Equality(UniRx.InternalUtil.ScheduledItem,UniRx.InternalUtil.ScheduledItem)
extern "C"  bool ScheduledItem_op_Equality_m868724262 (Il2CppObject * __this /* static, unused */, ScheduledItem_t1912903245 * ___left0, ScheduledItem_t1912903245 * ___right1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UniRx.InternalUtil.ScheduledItem::op_Inequality(UniRx.InternalUtil.ScheduledItem,UniRx.InternalUtil.ScheduledItem)
extern "C"  bool ScheduledItem_op_Inequality_m373540129 (Il2CppObject * __this /* static, unused */, ScheduledItem_t1912903245 * ___left0, ScheduledItem_t1912903245 * ___right1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
