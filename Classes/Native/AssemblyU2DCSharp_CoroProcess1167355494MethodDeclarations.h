﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CoroProcess
struct CoroProcess_t1167355494;
// System.Collections.IEnumerator
struct IEnumerator_t287207039;
// ILivingState
struct ILivingState_t4195066719;

#include "codegen/il2cpp-codegen.h"

// System.Void CoroProcess::.ctor(System.Collections.IEnumerator,ILivingState)
extern "C"  void CoroProcess__ctor_m3386238327 (CoroProcess_t1167355494 * __this, Il2CppObject * ___iterator0, Il2CppObject * ___inOwner1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean CoroProcess::get_finished()
extern "C"  bool CoroProcess_get_finished_m228366342 (CoroProcess_t1167355494 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CoroProcess::set_finished(System.Boolean)
extern "C"  void CoroProcess_set_finished_m3775854501 (CoroProcess_t1167355494 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CoroProcess::Tick(System.Single)
extern "C"  void CoroProcess_Tick_m401736687 (CoroProcess_t1167355494 * __this, float ___dt0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CoroProcess::Dispose()
extern "C"  void CoroProcess_Dispose_m3421302466 (CoroProcess_t1167355494 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
