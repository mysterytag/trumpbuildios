﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.CreateSafeObservable`1<System.Object>
struct CreateSafeObservable_1_t4182587251;
// System.Func`2<UniRx.IObserver`1<System.Object>,System.IDisposable>
struct Func_2_t2619763055;
// System.IDisposable
struct IDisposable_t1628921374;
// UniRx.IObserver`1<System.Object>
struct IObserver_1_t3049105323;

#include "codegen/il2cpp-codegen.h"

// System.Void UniRx.Operators.CreateSafeObservable`1<System.Object>::.ctor(System.Func`2<UniRx.IObserver`1<T>,System.IDisposable>)
extern "C"  void CreateSafeObservable_1__ctor_m1423138930_gshared (CreateSafeObservable_1_t4182587251 * __this, Func_2_t2619763055 * ___subscribe0, const MethodInfo* method);
#define CreateSafeObservable_1__ctor_m1423138930(__this, ___subscribe0, method) ((  void (*) (CreateSafeObservable_1_t4182587251 *, Func_2_t2619763055 *, const MethodInfo*))CreateSafeObservable_1__ctor_m1423138930_gshared)(__this, ___subscribe0, method)
// System.Void UniRx.Operators.CreateSafeObservable`1<System.Object>::.ctor(System.Func`2<UniRx.IObserver`1<T>,System.IDisposable>,System.Boolean)
extern "C"  void CreateSafeObservable_1__ctor_m1010155723_gshared (CreateSafeObservable_1_t4182587251 * __this, Func_2_t2619763055 * ___subscribe0, bool ___isRequiredSubscribeOnCurrentThread1, const MethodInfo* method);
#define CreateSafeObservable_1__ctor_m1010155723(__this, ___subscribe0, ___isRequiredSubscribeOnCurrentThread1, method) ((  void (*) (CreateSafeObservable_1_t4182587251 *, Func_2_t2619763055 *, bool, const MethodInfo*))CreateSafeObservable_1__ctor_m1010155723_gshared)(__this, ___subscribe0, ___isRequiredSubscribeOnCurrentThread1, method)
// System.IDisposable UniRx.Operators.CreateSafeObservable`1<System.Object>::SubscribeCore(UniRx.IObserver`1<T>,System.IDisposable)
extern "C"  Il2CppObject * CreateSafeObservable_1_SubscribeCore_m2639913529_gshared (CreateSafeObservable_1_t4182587251 * __this, Il2CppObject* ___observer0, Il2CppObject * ___cancel1, const MethodInfo* method);
#define CreateSafeObservable_1_SubscribeCore_m2639913529(__this, ___observer0, ___cancel1, method) ((  Il2CppObject * (*) (CreateSafeObservable_1_t4182587251 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))CreateSafeObservable_1_SubscribeCore_m2639913529_gshared)(__this, ___observer0, ___cancel1, method)
