﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Func`1<System.Boolean>
struct Func_1_t1353786588;
// System.Object
struct Il2CppObject;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TutorialPlayer/<WaitForCorroutine>c__Iterator24
struct  U3CWaitForCorroutineU3Ec__Iterator24_t1668957931  : public Il2CppObject
{
public:
	// System.Func`1<System.Boolean> TutorialPlayer/<WaitForCorroutine>c__Iterator24::predicate
	Func_1_t1353786588 * ___predicate_0;
	// System.Int32 TutorialPlayer/<WaitForCorroutine>c__Iterator24::$PC
	int32_t ___U24PC_1;
	// System.Object TutorialPlayer/<WaitForCorroutine>c__Iterator24::$current
	Il2CppObject * ___U24current_2;
	// System.Func`1<System.Boolean> TutorialPlayer/<WaitForCorroutine>c__Iterator24::<$>predicate
	Func_1_t1353786588 * ___U3CU24U3Epredicate_3;

public:
	inline static int32_t get_offset_of_predicate_0() { return static_cast<int32_t>(offsetof(U3CWaitForCorroutineU3Ec__Iterator24_t1668957931, ___predicate_0)); }
	inline Func_1_t1353786588 * get_predicate_0() const { return ___predicate_0; }
	inline Func_1_t1353786588 ** get_address_of_predicate_0() { return &___predicate_0; }
	inline void set_predicate_0(Func_1_t1353786588 * value)
	{
		___predicate_0 = value;
		Il2CppCodeGenWriteBarrier(&___predicate_0, value);
	}

	inline static int32_t get_offset_of_U24PC_1() { return static_cast<int32_t>(offsetof(U3CWaitForCorroutineU3Ec__Iterator24_t1668957931, ___U24PC_1)); }
	inline int32_t get_U24PC_1() const { return ___U24PC_1; }
	inline int32_t* get_address_of_U24PC_1() { return &___U24PC_1; }
	inline void set_U24PC_1(int32_t value)
	{
		___U24PC_1 = value;
	}

	inline static int32_t get_offset_of_U24current_2() { return static_cast<int32_t>(offsetof(U3CWaitForCorroutineU3Ec__Iterator24_t1668957931, ___U24current_2)); }
	inline Il2CppObject * get_U24current_2() const { return ___U24current_2; }
	inline Il2CppObject ** get_address_of_U24current_2() { return &___U24current_2; }
	inline void set_U24current_2(Il2CppObject * value)
	{
		___U24current_2 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_2, value);
	}

	inline static int32_t get_offset_of_U3CU24U3Epredicate_3() { return static_cast<int32_t>(offsetof(U3CWaitForCorroutineU3Ec__Iterator24_t1668957931, ___U3CU24U3Epredicate_3)); }
	inline Func_1_t1353786588 * get_U3CU24U3Epredicate_3() const { return ___U3CU24U3Epredicate_3; }
	inline Func_1_t1353786588 ** get_address_of_U3CU24U3Epredicate_3() { return &___U3CU24U3Epredicate_3; }
	inline void set_U3CU24U3Epredicate_3(Func_1_t1353786588 * value)
	{
		___U3CU24U3Epredicate_3 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU24U3Epredicate_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
