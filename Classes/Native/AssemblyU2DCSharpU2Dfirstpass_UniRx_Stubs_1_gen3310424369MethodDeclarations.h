﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Stubs_1_gen2180296801MethodDeclarations.h"

// System.Void UniRx.Stubs`1<System.Exception>::.cctor()
#define Stubs_1__cctor_m2201477190(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))Stubs_1__cctor_m4180273948_gshared)(__this /* static, unused */, method)
// System.Void UniRx.Stubs`1<System.Exception>::<Ignore>m__71(T)
#define Stubs_1_U3CIgnoreU3Em__71_m3469479746(__this /* static, unused */, ___t0, method) ((  void (*) (Il2CppObject * /* static, unused */, Exception_t1967233988 *, const MethodInfo*))Stubs_1_U3CIgnoreU3Em__71_m3494437912_gshared)(__this /* static, unused */, ___t0, method)
// T UniRx.Stubs`1<System.Exception>::<Identity>m__72(T)
#define Stubs_1_U3CIdentityU3Em__72_m3639242070(__this /* static, unused */, ___t0, method) ((  Exception_t1967233988 * (*) (Il2CppObject * /* static, unused */, Exception_t1967233988 *, const MethodInfo*))Stubs_1_U3CIdentityU3Em__72_m4025510410_gshared)(__this /* static, unused */, ___t0, method)
