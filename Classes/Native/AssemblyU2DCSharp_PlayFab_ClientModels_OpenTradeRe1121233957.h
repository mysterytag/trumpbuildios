﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Generic.List`1<System.String>
struct List_1_t1765447871;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PlayFab.ClientModels.OpenTradeRequest
struct  OpenTradeRequest_t1121233957  : public Il2CppObject
{
public:
	// System.Collections.Generic.List`1<System.String> PlayFab.ClientModels.OpenTradeRequest::<OfferedInventoryInstanceIds>k__BackingField
	List_1_t1765447871 * ___U3COfferedInventoryInstanceIdsU3Ek__BackingField_0;
	// System.Collections.Generic.List`1<System.String> PlayFab.ClientModels.OpenTradeRequest::<RequestedCatalogItemIds>k__BackingField
	List_1_t1765447871 * ___U3CRequestedCatalogItemIdsU3Ek__BackingField_1;
	// System.Collections.Generic.List`1<System.String> PlayFab.ClientModels.OpenTradeRequest::<AllowedPlayerIds>k__BackingField
	List_1_t1765447871 * ___U3CAllowedPlayerIdsU3Ek__BackingField_2;

public:
	inline static int32_t get_offset_of_U3COfferedInventoryInstanceIdsU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(OpenTradeRequest_t1121233957, ___U3COfferedInventoryInstanceIdsU3Ek__BackingField_0)); }
	inline List_1_t1765447871 * get_U3COfferedInventoryInstanceIdsU3Ek__BackingField_0() const { return ___U3COfferedInventoryInstanceIdsU3Ek__BackingField_0; }
	inline List_1_t1765447871 ** get_address_of_U3COfferedInventoryInstanceIdsU3Ek__BackingField_0() { return &___U3COfferedInventoryInstanceIdsU3Ek__BackingField_0; }
	inline void set_U3COfferedInventoryInstanceIdsU3Ek__BackingField_0(List_1_t1765447871 * value)
	{
		___U3COfferedInventoryInstanceIdsU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier(&___U3COfferedInventoryInstanceIdsU3Ek__BackingField_0, value);
	}

	inline static int32_t get_offset_of_U3CRequestedCatalogItemIdsU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(OpenTradeRequest_t1121233957, ___U3CRequestedCatalogItemIdsU3Ek__BackingField_1)); }
	inline List_1_t1765447871 * get_U3CRequestedCatalogItemIdsU3Ek__BackingField_1() const { return ___U3CRequestedCatalogItemIdsU3Ek__BackingField_1; }
	inline List_1_t1765447871 ** get_address_of_U3CRequestedCatalogItemIdsU3Ek__BackingField_1() { return &___U3CRequestedCatalogItemIdsU3Ek__BackingField_1; }
	inline void set_U3CRequestedCatalogItemIdsU3Ek__BackingField_1(List_1_t1765447871 * value)
	{
		___U3CRequestedCatalogItemIdsU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CRequestedCatalogItemIdsU3Ek__BackingField_1, value);
	}

	inline static int32_t get_offset_of_U3CAllowedPlayerIdsU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(OpenTradeRequest_t1121233957, ___U3CAllowedPlayerIdsU3Ek__BackingField_2)); }
	inline List_1_t1765447871 * get_U3CAllowedPlayerIdsU3Ek__BackingField_2() const { return ___U3CAllowedPlayerIdsU3Ek__BackingField_2; }
	inline List_1_t1765447871 ** get_address_of_U3CAllowedPlayerIdsU3Ek__BackingField_2() { return &___U3CAllowedPlayerIdsU3Ek__BackingField_2; }
	inline void set_U3CAllowedPlayerIdsU3Ek__BackingField_2(List_1_t1765447871 * value)
	{
		___U3CAllowedPlayerIdsU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier(&___U3CAllowedPlayerIdsU3Ek__BackingField_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
