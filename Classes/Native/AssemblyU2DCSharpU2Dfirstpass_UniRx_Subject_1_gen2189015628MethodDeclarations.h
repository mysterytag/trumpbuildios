﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Subject`1<UniRx.DictionaryAddEvent`2<System.Object,System.Object>>
struct Subject_1_t2189015628;
// System.Exception
struct Exception_t1967233988;
// System.IDisposable
struct IDisposable_t1628921374;
// UniRx.IObserver`1<UniRx.DictionaryAddEvent`2<System.Object,System.Object>>
struct IObserver_1_t2462979911;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Exception1967233988.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_DictionaryAddEv250981008.h"

// System.Void UniRx.Subject`1<UniRx.DictionaryAddEvent`2<System.Object,System.Object>>::.ctor()
extern "C"  void Subject_1__ctor_m3195923499_gshared (Subject_1_t2189015628 * __this, const MethodInfo* method);
#define Subject_1__ctor_m3195923499(__this, method) ((  void (*) (Subject_1_t2189015628 *, const MethodInfo*))Subject_1__ctor_m3195923499_gshared)(__this, method)
// System.Boolean UniRx.Subject`1<UniRx.DictionaryAddEvent`2<System.Object,System.Object>>::get_HasObservers()
extern "C"  bool Subject_1_get_HasObservers_m1989649793_gshared (Subject_1_t2189015628 * __this, const MethodInfo* method);
#define Subject_1_get_HasObservers_m1989649793(__this, method) ((  bool (*) (Subject_1_t2189015628 *, const MethodInfo*))Subject_1_get_HasObservers_m1989649793_gshared)(__this, method)
// System.Void UniRx.Subject`1<UniRx.DictionaryAddEvent`2<System.Object,System.Object>>::OnCompleted()
extern "C"  void Subject_1_OnCompleted_m3203666805_gshared (Subject_1_t2189015628 * __this, const MethodInfo* method);
#define Subject_1_OnCompleted_m3203666805(__this, method) ((  void (*) (Subject_1_t2189015628 *, const MethodInfo*))Subject_1_OnCompleted_m3203666805_gshared)(__this, method)
// System.Void UniRx.Subject`1<UniRx.DictionaryAddEvent`2<System.Object,System.Object>>::OnError(System.Exception)
extern "C"  void Subject_1_OnError_m3371973922_gshared (Subject_1_t2189015628 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method);
#define Subject_1_OnError_m3371973922(__this, ___error0, method) ((  void (*) (Subject_1_t2189015628 *, Exception_t1967233988 *, const MethodInfo*))Subject_1_OnError_m3371973922_gshared)(__this, ___error0, method)
// System.Void UniRx.Subject`1<UniRx.DictionaryAddEvent`2<System.Object,System.Object>>::OnNext(T)
extern "C"  void Subject_1_OnNext_m3211569171_gshared (Subject_1_t2189015628 * __this, DictionaryAddEvent_2_t250981008  ___value0, const MethodInfo* method);
#define Subject_1_OnNext_m3211569171(__this, ___value0, method) ((  void (*) (Subject_1_t2189015628 *, DictionaryAddEvent_2_t250981008 , const MethodInfo*))Subject_1_OnNext_m3211569171_gshared)(__this, ___value0, method)
// System.IDisposable UniRx.Subject`1<UniRx.DictionaryAddEvent`2<System.Object,System.Object>>::Subscribe(UniRx.IObserver`1<T>)
extern "C"  Il2CppObject * Subject_1_Subscribe_m2122008298_gshared (Subject_1_t2189015628 * __this, Il2CppObject* ___observer0, const MethodInfo* method);
#define Subject_1_Subscribe_m2122008298(__this, ___observer0, method) ((  Il2CppObject * (*) (Subject_1_t2189015628 *, Il2CppObject*, const MethodInfo*))Subject_1_Subscribe_m2122008298_gshared)(__this, ___observer0, method)
// System.Void UniRx.Subject`1<UniRx.DictionaryAddEvent`2<System.Object,System.Object>>::Dispose()
extern "C"  void Subject_1_Dispose_m279148968_gshared (Subject_1_t2189015628 * __this, const MethodInfo* method);
#define Subject_1_Dispose_m279148968(__this, method) ((  void (*) (Subject_1_t2189015628 *, const MethodInfo*))Subject_1_Dispose_m279148968_gshared)(__this, method)
// System.Void UniRx.Subject`1<UniRx.DictionaryAddEvent`2<System.Object,System.Object>>::ThrowIfDisposed()
extern "C"  void Subject_1_ThrowIfDisposed_m1914764081_gshared (Subject_1_t2189015628 * __this, const MethodInfo* method);
#define Subject_1_ThrowIfDisposed_m1914764081(__this, method) ((  void (*) (Subject_1_t2189015628 *, const MethodInfo*))Subject_1_ThrowIfDisposed_m1914764081_gshared)(__this, method)
// System.Boolean UniRx.Subject`1<UniRx.DictionaryAddEvent`2<System.Object,System.Object>>::IsRequiredSubscribeOnCurrentThread()
extern "C"  bool Subject_1_IsRequiredSubscribeOnCurrentThread_m3886027832_gshared (Subject_1_t2189015628 * __this, const MethodInfo* method);
#define Subject_1_IsRequiredSubscribeOnCurrentThread_m3886027832(__this, method) ((  bool (*) (Subject_1_t2189015628 *, const MethodInfo*))Subject_1_IsRequiredSubscribeOnCurrentThread_m3886027832_gshared)(__this, method)
