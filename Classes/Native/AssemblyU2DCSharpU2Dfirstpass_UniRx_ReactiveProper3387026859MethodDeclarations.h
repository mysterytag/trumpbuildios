﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.ReactiveProperty`1<System.Byte>
struct ReactiveProperty_1_t3387026859;
// UniRx.IObservable`1<System.Byte>
struct IObservable_1_t2537492185;
// System.Collections.Generic.IEqualityComparer`1<System.Byte>
struct IEqualityComparer_1_t807993176;
// System.IDisposable
struct IDisposable_t1628921374;
// UniRx.IObserver`1<System.Byte>
struct IObserver_1_t695725428;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"

// System.Void UniRx.ReactiveProperty`1<System.Byte>::.ctor()
extern "C"  void ReactiveProperty_1__ctor_m1302869371_gshared (ReactiveProperty_1_t3387026859 * __this, const MethodInfo* method);
#define ReactiveProperty_1__ctor_m1302869371(__this, method) ((  void (*) (ReactiveProperty_1_t3387026859 *, const MethodInfo*))ReactiveProperty_1__ctor_m1302869371_gshared)(__this, method)
// System.Void UniRx.ReactiveProperty`1<System.Byte>::.ctor(T)
extern "C"  void ReactiveProperty_1__ctor_m1734246211_gshared (ReactiveProperty_1_t3387026859 * __this, uint8_t ___initialValue0, const MethodInfo* method);
#define ReactiveProperty_1__ctor_m1734246211(__this, ___initialValue0, method) ((  void (*) (ReactiveProperty_1_t3387026859 *, uint8_t, const MethodInfo*))ReactiveProperty_1__ctor_m1734246211_gshared)(__this, ___initialValue0, method)
// System.Void UniRx.ReactiveProperty`1<System.Byte>::.ctor(UniRx.IObservable`1<T>)
extern "C"  void ReactiveProperty_1__ctor_m4204920090_gshared (ReactiveProperty_1_t3387026859 * __this, Il2CppObject* ___source0, const MethodInfo* method);
#define ReactiveProperty_1__ctor_m4204920090(__this, ___source0, method) ((  void (*) (ReactiveProperty_1_t3387026859 *, Il2CppObject*, const MethodInfo*))ReactiveProperty_1__ctor_m4204920090_gshared)(__this, ___source0, method)
// System.Void UniRx.ReactiveProperty`1<System.Byte>::.ctor(UniRx.IObservable`1<T>,T)
extern "C"  void ReactiveProperty_1__ctor_m3658953778_gshared (ReactiveProperty_1_t3387026859 * __this, Il2CppObject* ___source0, uint8_t ___initialValue1, const MethodInfo* method);
#define ReactiveProperty_1__ctor_m3658953778(__this, ___source0, ___initialValue1, method) ((  void (*) (ReactiveProperty_1_t3387026859 *, Il2CppObject*, uint8_t, const MethodInfo*))ReactiveProperty_1__ctor_m3658953778_gshared)(__this, ___source0, ___initialValue1, method)
// System.Void UniRx.ReactiveProperty`1<System.Byte>::.cctor()
extern "C"  void ReactiveProperty_1__cctor_m1252148626_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define ReactiveProperty_1__cctor_m1252148626(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))ReactiveProperty_1__cctor_m1252148626_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.IEqualityComparer`1<T> UniRx.ReactiveProperty`1<System.Byte>::get_EqualityComparer()
extern "C"  Il2CppObject* ReactiveProperty_1_get_EqualityComparer_m332684858_gshared (ReactiveProperty_1_t3387026859 * __this, const MethodInfo* method);
#define ReactiveProperty_1_get_EqualityComparer_m332684858(__this, method) ((  Il2CppObject* (*) (ReactiveProperty_1_t3387026859 *, const MethodInfo*))ReactiveProperty_1_get_EqualityComparer_m332684858_gshared)(__this, method)
// T UniRx.ReactiveProperty`1<System.Byte>::get_Value()
extern "C"  uint8_t ReactiveProperty_1_get_Value_m1384232096_gshared (ReactiveProperty_1_t3387026859 * __this, const MethodInfo* method);
#define ReactiveProperty_1_get_Value_m1384232096(__this, method) ((  uint8_t (*) (ReactiveProperty_1_t3387026859 *, const MethodInfo*))ReactiveProperty_1_get_Value_m1384232096_gshared)(__this, method)
// System.Void UniRx.ReactiveProperty`1<System.Byte>::set_Value(T)
extern "C"  void ReactiveProperty_1_set_Value_m437209617_gshared (ReactiveProperty_1_t3387026859 * __this, uint8_t ___value0, const MethodInfo* method);
#define ReactiveProperty_1_set_Value_m437209617(__this, ___value0, method) ((  void (*) (ReactiveProperty_1_t3387026859 *, uint8_t, const MethodInfo*))ReactiveProperty_1_set_Value_m437209617_gshared)(__this, ___value0, method)
// System.Void UniRx.ReactiveProperty`1<System.Byte>::SetValue(T)
extern "C"  void ReactiveProperty_1_SetValue_m1762358662_gshared (ReactiveProperty_1_t3387026859 * __this, uint8_t ___value0, const MethodInfo* method);
#define ReactiveProperty_1_SetValue_m1762358662(__this, ___value0, method) ((  void (*) (ReactiveProperty_1_t3387026859 *, uint8_t, const MethodInfo*))ReactiveProperty_1_SetValue_m1762358662_gshared)(__this, ___value0, method)
// System.Void UniRx.ReactiveProperty`1<System.Byte>::SetValueAndForceNotify(T)
extern "C"  void ReactiveProperty_1_SetValueAndForceNotify_m2180602057_gshared (ReactiveProperty_1_t3387026859 * __this, uint8_t ___value0, const MethodInfo* method);
#define ReactiveProperty_1_SetValueAndForceNotify_m2180602057(__this, ___value0, method) ((  void (*) (ReactiveProperty_1_t3387026859 *, uint8_t, const MethodInfo*))ReactiveProperty_1_SetValueAndForceNotify_m2180602057_gshared)(__this, ___value0, method)
// System.IDisposable UniRx.ReactiveProperty`1<System.Byte>::Subscribe(UniRx.IObserver`1<T>)
extern "C"  Il2CppObject * ReactiveProperty_1_Subscribe_m764395536_gshared (ReactiveProperty_1_t3387026859 * __this, Il2CppObject* ___observer0, const MethodInfo* method);
#define ReactiveProperty_1_Subscribe_m764395536(__this, ___observer0, method) ((  Il2CppObject * (*) (ReactiveProperty_1_t3387026859 *, Il2CppObject*, const MethodInfo*))ReactiveProperty_1_Subscribe_m764395536_gshared)(__this, ___observer0, method)
// System.Void UniRx.ReactiveProperty`1<System.Byte>::Dispose()
extern "C"  void ReactiveProperty_1_Dispose_m2120265464_gshared (ReactiveProperty_1_t3387026859 * __this, const MethodInfo* method);
#define ReactiveProperty_1_Dispose_m2120265464(__this, method) ((  void (*) (ReactiveProperty_1_t3387026859 *, const MethodInfo*))ReactiveProperty_1_Dispose_m2120265464_gshared)(__this, method)
// System.Void UniRx.ReactiveProperty`1<System.Byte>::Dispose(System.Boolean)
extern "C"  void ReactiveProperty_1_Dispose_m126734959_gshared (ReactiveProperty_1_t3387026859 * __this, bool ___disposing0, const MethodInfo* method);
#define ReactiveProperty_1_Dispose_m126734959(__this, ___disposing0, method) ((  void (*) (ReactiveProperty_1_t3387026859 *, bool, const MethodInfo*))ReactiveProperty_1_Dispose_m126734959_gshared)(__this, ___disposing0, method)
// System.String UniRx.ReactiveProperty`1<System.Byte>::ToString()
extern "C"  String_t* ReactiveProperty_1_ToString_m3932696338_gshared (ReactiveProperty_1_t3387026859 * __this, const MethodInfo* method);
#define ReactiveProperty_1_ToString_m3932696338(__this, method) ((  String_t* (*) (ReactiveProperty_1_t3387026859 *, const MethodInfo*))ReactiveProperty_1_ToString_m3932696338_gshared)(__this, method)
// System.Boolean UniRx.ReactiveProperty`1<System.Byte>::IsRequiredSubscribeOnCurrentThread()
extern "C"  bool ReactiveProperty_1_IsRequiredSubscribeOnCurrentThread_m3230296368_gshared (ReactiveProperty_1_t3387026859 * __this, const MethodInfo* method);
#define ReactiveProperty_1_IsRequiredSubscribeOnCurrentThread_m3230296368(__this, method) ((  bool (*) (ReactiveProperty_1_t3387026859 *, const MethodInfo*))ReactiveProperty_1_IsRequiredSubscribeOnCurrentThread_m3230296368_gshared)(__this, method)
