﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CellReactiveApi/<AsStream>c__AnonStorey105`1<System.Object>
struct U3CAsStreamU3Ec__AnonStorey105_1_t3800865897;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void CellReactiveApi/<AsStream>c__AnonStorey105`1<System.Object>::.ctor()
extern "C"  void U3CAsStreamU3Ec__AnonStorey105_1__ctor_m1617263759_gshared (U3CAsStreamU3Ec__AnonStorey105_1_t3800865897 * __this, const MethodInfo* method);
#define U3CAsStreamU3Ec__AnonStorey105_1__ctor_m1617263759(__this, method) ((  void (*) (U3CAsStreamU3Ec__AnonStorey105_1_t3800865897 *, const MethodInfo*))U3CAsStreamU3Ec__AnonStorey105_1__ctor_m1617263759_gshared)(__this, method)
// T CellReactiveApi/<AsStream>c__AnonStorey105`1<System.Object>::<>m__17A()
extern "C"  Il2CppObject * U3CAsStreamU3Ec__AnonStorey105_1_U3CU3Em__17A_m3302523142_gshared (U3CAsStreamU3Ec__AnonStorey105_1_t3800865897 * __this, const MethodInfo* method);
#define U3CAsStreamU3Ec__AnonStorey105_1_U3CU3Em__17A_m3302523142(__this, method) ((  Il2CppObject * (*) (U3CAsStreamU3Ec__AnonStorey105_1_t3800865897 *, const MethodInfo*))U3CAsStreamU3Ec__AnonStorey105_1_U3CU3Em__17A_m3302523142_gshared)(__this, method)
