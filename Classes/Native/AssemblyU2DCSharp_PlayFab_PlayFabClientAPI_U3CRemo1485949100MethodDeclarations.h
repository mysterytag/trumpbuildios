﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.PlayFabClientAPI/<RemoveSharedGroupMembers>c__AnonStoreyBF
struct U3CRemoveSharedGroupMembersU3Ec__AnonStoreyBF_t1485949100;
// PlayFab.CallRequestContainer
struct CallRequestContainer_t432318609;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharpU2Dfirstpass_PlayFab_CallRequestCo432318609.h"

// System.Void PlayFab.PlayFabClientAPI/<RemoveSharedGroupMembers>c__AnonStoreyBF::.ctor()
extern "C"  void U3CRemoveSharedGroupMembersU3Ec__AnonStoreyBF__ctor_m180311175 (U3CRemoveSharedGroupMembersU3Ec__AnonStoreyBF_t1485949100 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/<RemoveSharedGroupMembers>c__AnonStoreyBF::<>m__AC(PlayFab.CallRequestContainer)
extern "C"  void U3CRemoveSharedGroupMembersU3Ec__AnonStoreyBF_U3CU3Em__AC_m2103287239 (U3CRemoveSharedGroupMembersU3Ec__AnonStoreyBF_t1485949100 * __this, CallRequestContainer_t432318609 * ___requestContainer0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
