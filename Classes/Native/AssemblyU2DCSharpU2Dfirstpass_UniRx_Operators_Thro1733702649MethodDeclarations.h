﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.ThrottleFrameObservable`1/ThrottleFrame/ThrottleFrameTick<System.Object>
struct ThrottleFrameTick_t1733702649;
// UniRx.Operators.ThrottleFrameObservable`1/ThrottleFrame<System.Object>
struct ThrottleFrame_t3629975492;
// System.Exception
struct Exception_t1967233988;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Exception1967233988.h"

// System.Void UniRx.Operators.ThrottleFrameObservable`1/ThrottleFrame/ThrottleFrameTick<System.Object>::.ctor(UniRx.Operators.ThrottleFrameObservable`1/ThrottleFrame<T>,System.UInt64)
extern "C"  void ThrottleFrameTick__ctor_m3445849371_gshared (ThrottleFrameTick_t1733702649 * __this, ThrottleFrame_t3629975492 * ___parent0, uint64_t ___currentid1, const MethodInfo* method);
#define ThrottleFrameTick__ctor_m3445849371(__this, ___parent0, ___currentid1, method) ((  void (*) (ThrottleFrameTick_t1733702649 *, ThrottleFrame_t3629975492 *, uint64_t, const MethodInfo*))ThrottleFrameTick__ctor_m3445849371_gshared)(__this, ___parent0, ___currentid1, method)
// System.Void UniRx.Operators.ThrottleFrameObservable`1/ThrottleFrame/ThrottleFrameTick<System.Object>::OnCompleted()
extern "C"  void ThrottleFrameTick_OnCompleted_m1882604578_gshared (ThrottleFrameTick_t1733702649 * __this, const MethodInfo* method);
#define ThrottleFrameTick_OnCompleted_m1882604578(__this, method) ((  void (*) (ThrottleFrameTick_t1733702649 *, const MethodInfo*))ThrottleFrameTick_OnCompleted_m1882604578_gshared)(__this, method)
// System.Void UniRx.Operators.ThrottleFrameObservable`1/ThrottleFrame/ThrottleFrameTick<System.Object>::OnError(System.Exception)
extern "C"  void ThrottleFrameTick_OnError_m1728172623_gshared (ThrottleFrameTick_t1733702649 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method);
#define ThrottleFrameTick_OnError_m1728172623(__this, ___error0, method) ((  void (*) (ThrottleFrameTick_t1733702649 *, Exception_t1967233988 *, const MethodInfo*))ThrottleFrameTick_OnError_m1728172623_gshared)(__this, ___error0, method)
// System.Void UniRx.Operators.ThrottleFrameObservable`1/ThrottleFrame/ThrottleFrameTick<System.Object>::OnNext(System.Int64)
extern "C"  void ThrottleFrameTick_OnNext_m3270831536_gshared (ThrottleFrameTick_t1733702649 * __this, int64_t ____0, const MethodInfo* method);
#define ThrottleFrameTick_OnNext_m3270831536(__this, ____0, method) ((  void (*) (ThrottleFrameTick_t1733702649 *, int64_t, const MethodInfo*))ThrottleFrameTick_OnNext_m3270831536_gshared)(__this, ____0, method)
