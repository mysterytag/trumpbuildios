﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// PlayFab.UUnit.ObjNumPropTest
struct ObjNumPropTest_t1151851726;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PlayFab.UUnit.ObjNumPropTest
struct  ObjNumPropTest_t1151851726  : public Il2CppObject
{
public:
	// System.SByte PlayFab.UUnit.ObjNumPropTest::<SbyteValue>k__BackingField
	int8_t ___U3CSbyteValueU3Ek__BackingField_3;
	// System.Byte PlayFab.UUnit.ObjNumPropTest::<ByteValue>k__BackingField
	uint8_t ___U3CByteValueU3Ek__BackingField_4;
	// System.Int16 PlayFab.UUnit.ObjNumPropTest::<ShortValue>k__BackingField
	int16_t ___U3CShortValueU3Ek__BackingField_5;
	// System.UInt16 PlayFab.UUnit.ObjNumPropTest::<UshortValue>k__BackingField
	uint16_t ___U3CUshortValueU3Ek__BackingField_6;
	// System.Int32 PlayFab.UUnit.ObjNumPropTest::<IntValue>k__BackingField
	int32_t ___U3CIntValueU3Ek__BackingField_7;
	// System.UInt32 PlayFab.UUnit.ObjNumPropTest::<UintValue>k__BackingField
	uint32_t ___U3CUintValueU3Ek__BackingField_8;
	// System.Int64 PlayFab.UUnit.ObjNumPropTest::<LongValue>k__BackingField
	int64_t ___U3CLongValueU3Ek__BackingField_9;
	// System.UInt64 PlayFab.UUnit.ObjNumPropTest::<UlongValue>k__BackingField
	uint64_t ___U3CUlongValueU3Ek__BackingField_10;
	// System.Single PlayFab.UUnit.ObjNumPropTest::<FloatValue>k__BackingField
	float ___U3CFloatValueU3Ek__BackingField_11;
	// System.Double PlayFab.UUnit.ObjNumPropTest::<DoubleValue>k__BackingField
	double ___U3CDoubleValueU3Ek__BackingField_12;

public:
	inline static int32_t get_offset_of_U3CSbyteValueU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(ObjNumPropTest_t1151851726, ___U3CSbyteValueU3Ek__BackingField_3)); }
	inline int8_t get_U3CSbyteValueU3Ek__BackingField_3() const { return ___U3CSbyteValueU3Ek__BackingField_3; }
	inline int8_t* get_address_of_U3CSbyteValueU3Ek__BackingField_3() { return &___U3CSbyteValueU3Ek__BackingField_3; }
	inline void set_U3CSbyteValueU3Ek__BackingField_3(int8_t value)
	{
		___U3CSbyteValueU3Ek__BackingField_3 = value;
	}

	inline static int32_t get_offset_of_U3CByteValueU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(ObjNumPropTest_t1151851726, ___U3CByteValueU3Ek__BackingField_4)); }
	inline uint8_t get_U3CByteValueU3Ek__BackingField_4() const { return ___U3CByteValueU3Ek__BackingField_4; }
	inline uint8_t* get_address_of_U3CByteValueU3Ek__BackingField_4() { return &___U3CByteValueU3Ek__BackingField_4; }
	inline void set_U3CByteValueU3Ek__BackingField_4(uint8_t value)
	{
		___U3CByteValueU3Ek__BackingField_4 = value;
	}

	inline static int32_t get_offset_of_U3CShortValueU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(ObjNumPropTest_t1151851726, ___U3CShortValueU3Ek__BackingField_5)); }
	inline int16_t get_U3CShortValueU3Ek__BackingField_5() const { return ___U3CShortValueU3Ek__BackingField_5; }
	inline int16_t* get_address_of_U3CShortValueU3Ek__BackingField_5() { return &___U3CShortValueU3Ek__BackingField_5; }
	inline void set_U3CShortValueU3Ek__BackingField_5(int16_t value)
	{
		___U3CShortValueU3Ek__BackingField_5 = value;
	}

	inline static int32_t get_offset_of_U3CUshortValueU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(ObjNumPropTest_t1151851726, ___U3CUshortValueU3Ek__BackingField_6)); }
	inline uint16_t get_U3CUshortValueU3Ek__BackingField_6() const { return ___U3CUshortValueU3Ek__BackingField_6; }
	inline uint16_t* get_address_of_U3CUshortValueU3Ek__BackingField_6() { return &___U3CUshortValueU3Ek__BackingField_6; }
	inline void set_U3CUshortValueU3Ek__BackingField_6(uint16_t value)
	{
		___U3CUshortValueU3Ek__BackingField_6 = value;
	}

	inline static int32_t get_offset_of_U3CIntValueU3Ek__BackingField_7() { return static_cast<int32_t>(offsetof(ObjNumPropTest_t1151851726, ___U3CIntValueU3Ek__BackingField_7)); }
	inline int32_t get_U3CIntValueU3Ek__BackingField_7() const { return ___U3CIntValueU3Ek__BackingField_7; }
	inline int32_t* get_address_of_U3CIntValueU3Ek__BackingField_7() { return &___U3CIntValueU3Ek__BackingField_7; }
	inline void set_U3CIntValueU3Ek__BackingField_7(int32_t value)
	{
		___U3CIntValueU3Ek__BackingField_7 = value;
	}

	inline static int32_t get_offset_of_U3CUintValueU3Ek__BackingField_8() { return static_cast<int32_t>(offsetof(ObjNumPropTest_t1151851726, ___U3CUintValueU3Ek__BackingField_8)); }
	inline uint32_t get_U3CUintValueU3Ek__BackingField_8() const { return ___U3CUintValueU3Ek__BackingField_8; }
	inline uint32_t* get_address_of_U3CUintValueU3Ek__BackingField_8() { return &___U3CUintValueU3Ek__BackingField_8; }
	inline void set_U3CUintValueU3Ek__BackingField_8(uint32_t value)
	{
		___U3CUintValueU3Ek__BackingField_8 = value;
	}

	inline static int32_t get_offset_of_U3CLongValueU3Ek__BackingField_9() { return static_cast<int32_t>(offsetof(ObjNumPropTest_t1151851726, ___U3CLongValueU3Ek__BackingField_9)); }
	inline int64_t get_U3CLongValueU3Ek__BackingField_9() const { return ___U3CLongValueU3Ek__BackingField_9; }
	inline int64_t* get_address_of_U3CLongValueU3Ek__BackingField_9() { return &___U3CLongValueU3Ek__BackingField_9; }
	inline void set_U3CLongValueU3Ek__BackingField_9(int64_t value)
	{
		___U3CLongValueU3Ek__BackingField_9 = value;
	}

	inline static int32_t get_offset_of_U3CUlongValueU3Ek__BackingField_10() { return static_cast<int32_t>(offsetof(ObjNumPropTest_t1151851726, ___U3CUlongValueU3Ek__BackingField_10)); }
	inline uint64_t get_U3CUlongValueU3Ek__BackingField_10() const { return ___U3CUlongValueU3Ek__BackingField_10; }
	inline uint64_t* get_address_of_U3CUlongValueU3Ek__BackingField_10() { return &___U3CUlongValueU3Ek__BackingField_10; }
	inline void set_U3CUlongValueU3Ek__BackingField_10(uint64_t value)
	{
		___U3CUlongValueU3Ek__BackingField_10 = value;
	}

	inline static int32_t get_offset_of_U3CFloatValueU3Ek__BackingField_11() { return static_cast<int32_t>(offsetof(ObjNumPropTest_t1151851726, ___U3CFloatValueU3Ek__BackingField_11)); }
	inline float get_U3CFloatValueU3Ek__BackingField_11() const { return ___U3CFloatValueU3Ek__BackingField_11; }
	inline float* get_address_of_U3CFloatValueU3Ek__BackingField_11() { return &___U3CFloatValueU3Ek__BackingField_11; }
	inline void set_U3CFloatValueU3Ek__BackingField_11(float value)
	{
		___U3CFloatValueU3Ek__BackingField_11 = value;
	}

	inline static int32_t get_offset_of_U3CDoubleValueU3Ek__BackingField_12() { return static_cast<int32_t>(offsetof(ObjNumPropTest_t1151851726, ___U3CDoubleValueU3Ek__BackingField_12)); }
	inline double get_U3CDoubleValueU3Ek__BackingField_12() const { return ___U3CDoubleValueU3Ek__BackingField_12; }
	inline double* get_address_of_U3CDoubleValueU3Ek__BackingField_12() { return &___U3CDoubleValueU3Ek__BackingField_12; }
	inline void set_U3CDoubleValueU3Ek__BackingField_12(double value)
	{
		___U3CDoubleValueU3Ek__BackingField_12 = value;
	}
};

struct ObjNumPropTest_t1151851726_StaticFields
{
public:
	// PlayFab.UUnit.ObjNumPropTest PlayFab.UUnit.ObjNumPropTest::Max
	ObjNumPropTest_t1151851726 * ___Max_0;
	// PlayFab.UUnit.ObjNumPropTest PlayFab.UUnit.ObjNumPropTest::Min
	ObjNumPropTest_t1151851726 * ___Min_1;
	// PlayFab.UUnit.ObjNumPropTest PlayFab.UUnit.ObjNumPropTest::Zero
	ObjNumPropTest_t1151851726 * ___Zero_2;

public:
	inline static int32_t get_offset_of_Max_0() { return static_cast<int32_t>(offsetof(ObjNumPropTest_t1151851726_StaticFields, ___Max_0)); }
	inline ObjNumPropTest_t1151851726 * get_Max_0() const { return ___Max_0; }
	inline ObjNumPropTest_t1151851726 ** get_address_of_Max_0() { return &___Max_0; }
	inline void set_Max_0(ObjNumPropTest_t1151851726 * value)
	{
		___Max_0 = value;
		Il2CppCodeGenWriteBarrier(&___Max_0, value);
	}

	inline static int32_t get_offset_of_Min_1() { return static_cast<int32_t>(offsetof(ObjNumPropTest_t1151851726_StaticFields, ___Min_1)); }
	inline ObjNumPropTest_t1151851726 * get_Min_1() const { return ___Min_1; }
	inline ObjNumPropTest_t1151851726 ** get_address_of_Min_1() { return &___Min_1; }
	inline void set_Min_1(ObjNumPropTest_t1151851726 * value)
	{
		___Min_1 = value;
		Il2CppCodeGenWriteBarrier(&___Min_1, value);
	}

	inline static int32_t get_offset_of_Zero_2() { return static_cast<int32_t>(offsetof(ObjNumPropTest_t1151851726_StaticFields, ___Zero_2)); }
	inline ObjNumPropTest_t1151851726 * get_Zero_2() const { return ___Zero_2; }
	inline ObjNumPropTest_t1151851726 ** get_address_of_Zero_2() { return &___Zero_2; }
	inline void set_Zero_2(ObjNumPropTest_t1151851726 * value)
	{
		___Zero_2 = value;
		Il2CppCodeGenWriteBarrier(&___Zero_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
