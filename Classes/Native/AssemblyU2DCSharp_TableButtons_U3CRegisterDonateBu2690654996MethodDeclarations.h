﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// TableButtons/<RegisterDonateButtons>c__AnonStorey169/<RegisterDonateButtons>c__AnonStorey165
struct U3CRegisterDonateButtonsU3Ec__AnonStorey165_t2690654996;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Unit2558286038.h"

// System.Void TableButtons/<RegisterDonateButtons>c__AnonStorey169/<RegisterDonateButtons>c__AnonStorey165::.ctor()
extern "C"  void U3CRegisterDonateButtonsU3Ec__AnonStorey165__ctor_m1570015930 (U3CRegisterDonateButtonsU3Ec__AnonStorey165_t2690654996 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TableButtons/<RegisterDonateButtons>c__AnonStorey169/<RegisterDonateButtons>c__AnonStorey165::<>m__26A(UniRx.Unit)
extern "C"  void U3CRegisterDonateButtonsU3Ec__AnonStorey165_U3CU3Em__26A_m543366368 (U3CRegisterDonateButtonsU3Ec__AnonStorey165_t2690654996 * __this, Unit_t2558286038  ___unit0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TableButtons/<RegisterDonateButtons>c__AnonStorey169/<RegisterDonateButtons>c__AnonStorey165::<>m__26B(System.Boolean)
extern "C"  void U3CRegisterDonateButtonsU3Ec__AnonStorey165_U3CU3Em__26B_m1320172788 (U3CRegisterDonateButtonsU3Ec__AnonStorey165_t2690654996 * __this, bool ___b0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
