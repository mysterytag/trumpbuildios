﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UniRx.Subject`1<UnityEngine.EventSystems.PointerEventData>
struct Subject_1_t848168958;

#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Triggers_Obser1883237895.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Triggers.ObservableBeginDragTrigger
struct  ObservableBeginDragTrigger_t1382750143  : public ObservableTriggerBase_t1883237895
{
public:
	// UniRx.Subject`1<UnityEngine.EventSystems.PointerEventData> UniRx.Triggers.ObservableBeginDragTrigger::onBeginDrag
	Subject_1_t848168958 * ___onBeginDrag_8;

public:
	inline static int32_t get_offset_of_onBeginDrag_8() { return static_cast<int32_t>(offsetof(ObservableBeginDragTrigger_t1382750143, ___onBeginDrag_8)); }
	inline Subject_1_t848168958 * get_onBeginDrag_8() const { return ___onBeginDrag_8; }
	inline Subject_1_t848168958 ** get_address_of_onBeginDrag_8() { return &___onBeginDrag_8; }
	inline void set_onBeginDrag_8(Subject_1_t848168958 * value)
	{
		___onBeginDrag_8 = value;
		Il2CppCodeGenWriteBarrier(&___onBeginDrag_8, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
