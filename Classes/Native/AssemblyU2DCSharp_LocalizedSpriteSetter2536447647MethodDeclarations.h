﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// LocalizedSpriteSetter
struct LocalizedSpriteSetter_t2536447647;

#include "codegen/il2cpp-codegen.h"

// System.Void LocalizedSpriteSetter::.ctor()
extern "C"  void LocalizedSpriteSetter__ctor_m2784419244 (LocalizedSpriteSetter_t2536447647 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LocalizedSpriteSetter::Awake()
extern "C"  void LocalizedSpriteSetter_Awake_m3022024463 (LocalizedSpriteSetter_t2536447647 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
