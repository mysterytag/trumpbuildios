﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.String
struct String_t;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.Object
struct Il2CppObject;
// System.Delegate
struct Delegate_t3660574010;
// PlayFab.PlayFabError
struct PlayFabError_t750598646;
// System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.HashSet`1<System.Reflection.MethodInfo>>
struct Dictionary_2_t3502640556;
// System.Object[]
struct ObjectU5BU5D_t11523773;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String968488902.h"
#include "mscorlib_System_Reflection_MethodInfo3461221277.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_Delegate3660574010.h"
#include "AssemblyU2DCSharpU2Dfirstpass_PlayFab_PlayFabError750598646.h"

// System.Void PlayFab.PlayFabSettings::.cctor()
extern "C"  void PlayFabSettings__cctor_m2687039325 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PlayFab.PlayFabSettings::GetLogicUrl(System.String)
extern "C"  String_t* PlayFabSettings_GetLogicUrl_m3859886986 (Il2CppObject * __this /* static, unused */, String_t* ___apiCall0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PlayFab.PlayFabSettings::GetFullUrl(System.String)
extern "C"  String_t* PlayFabSettings_GetFullUrl_m3988204231 (Il2CppObject * __this /* static, unused */, String_t* ___apiCall0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabSettings::RegisterForRequests(System.String,System.Reflection.MethodInfo,System.Object)
extern "C"  void PlayFabSettings_RegisterForRequests_m3266018679 (Il2CppObject * __this /* static, unused */, String_t* ___urlPath0, MethodInfo_t * ___methodInfo1, Il2CppObject * ___instance2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabSettings::RegisterForRequests(System.String,System.Delegate)
extern "C"  void PlayFabSettings_RegisterForRequests_m4209543954 (Il2CppObject * __this /* static, unused */, String_t* ___urlPath0, Delegate_t3660574010 * ___callback1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabSettings::RegisterForResponses(System.String,System.Reflection.MethodInfo,System.Object)
extern "C"  void PlayFabSettings_RegisterForResponses_m710811135 (Il2CppObject * __this /* static, unused */, String_t* ___urlPath0, MethodInfo_t * ___methodInfo1, Il2CppObject * ___instance2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabSettings::RegisterForResponses(System.String,System.Delegate)
extern "C"  void PlayFabSettings_RegisterForResponses_m2333925258 (Il2CppObject * __this /* static, unused */, String_t* ___urlPath0, Delegate_t3660574010 * ___callback1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabSettings::UnregisterForRequests(System.String,System.Reflection.MethodInfo,System.Object)
extern "C"  void PlayFabSettings_UnregisterForRequests_m2759491646 (Il2CppObject * __this /* static, unused */, String_t* ___urlPath0, MethodInfo_t * ___methodInfo1, Il2CppObject * ___instance2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabSettings::UnregisterForRequests(System.String,System.Delegate)
extern "C"  void PlayFabSettings_UnregisterForRequests_m4028879019 (Il2CppObject * __this /* static, unused */, String_t* ___urlPath0, Delegate_t3660574010 * ___callback1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabSettings::UnregisterForResponses(System.String,System.Reflection.MethodInfo,System.Object)
extern "C"  void PlayFabSettings_UnregisterForResponses_m2188342296 (Il2CppObject * __this /* static, unused */, String_t* ___urlPath0, MethodInfo_t * ___methodInfo1, Il2CppObject * ___instance2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabSettings::UnregisterForResponses(System.String,System.Delegate)
extern "C"  void PlayFabSettings_UnregisterForResponses_m1028279569 (Il2CppObject * __this /* static, unused */, String_t* ___urlPath0, Delegate_t3660574010 * ___callback1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabSettings::UnregisterInstance(System.Object)
extern "C"  void PlayFabSettings_UnregisterInstance_m592811343 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___instance0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabSettings::ForceUnregisterAll()
extern "C"  void PlayFabSettings_ForceUnregisterAll_m1997377486 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabSettings::CheckMethod(System.String&,System.Reflection.MethodInfo,System.Object)
extern "C"  void PlayFabSettings_CheckMethod_m459755424 (Il2CppObject * __this /* static, unused */, String_t** ___urlPath0, MethodInfo_t * ___methodInfo1, Il2CppObject * ___instance2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PlayFab.PlayFabSettings::CheckInstance(System.Object)
extern "C"  bool PlayFabSettings_CheckInstance_m1272614997 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___instance0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabSettings::InvokeRequest(System.String,System.Int32,System.Object,System.Object)
extern "C"  void PlayFabSettings_InvokeRequest_m4289211030 (Il2CppObject * __this /* static, unused */, String_t* ___urlPath0, int32_t ___callId1, Il2CppObject * ___request2, Il2CppObject * ___customData3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabSettings::InvokeResponse(System.String,System.Int32,System.Object,System.Object,PlayFab.PlayFabError,System.Object)
extern "C"  void PlayFabSettings_InvokeResponse_m3170466598 (Il2CppObject * __this /* static, unused */, String_t* ___urlPath0, int32_t ___callId1, Il2CppObject * ___request2, Il2CppObject * ___result3, PlayFabError_t750598646 * ___error4, Il2CppObject * ___customData5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabSettings::InvokeHelper(System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.HashSet`1<System.Reflection.MethodInfo>>,System.String,System.Object[])
extern "C"  void PlayFabSettings_InvokeHelper_m2271589256 (Il2CppObject * __this /* static, unused */, Dictionary_2_t3502640556 * ___handlers0, String_t* ___urlPath1, ObjectU5BU5D_t11523773* ___prams2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabSettings::SaveInvoke(System.Reflection.MethodInfo,System.Object,System.Object[])
extern "C"  void PlayFabSettings_SaveInvoke_m2029632112 (Il2CppObject * __this /* static, unused */, MethodInfo_t * ___methodInfo0, Il2CppObject * ___instance1, ObjectU5BU5D_t11523773* ___prams2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
