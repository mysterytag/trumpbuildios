﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Generic.List`1<System.IDisposable>
struct List_1_t2425880343;
// OnceEmptyStream
struct OnceEmptyStream_t3081939404;
// IOrganism
struct IOrganism_t2580843579;
// IRoot
struct IRoot_t69970123;
// System.Object
struct Il2CppObject;
// Stream`1<System.Int32>
struct Stream_1_t1538553809;
// Stream`1<UniRx.Unit>
struct Stream_1_t1249425060;
// Stream`1<UniRx.CollectionAddEvent`1<System.Object>>
struct Stream_1_t1107661009;
// Stream`1<CollectionMoveEvent`1<System.Object>>
struct Stream_1_t2263194403;
// Stream`1<CollectionRemoveEvent`1<System.Object>>
struct Stream_1_t3884365576;
// Stream`1<CollectionReplaceEvent`1<System.Object>>
struct Stream_1_t1188511092;
// Stream`1<System.Collections.Generic.ICollection`1<System.Object>>
struct Stream_1_t4289044124;
// System.Action`1<System.Object>
struct Action_1_t985559125;
// System.Action`1<System.IDisposable>
struct Action_1_t1777374079;
// System.Func`2<System.Collections.Generic.ICollection`1<System.Object>,System.Object>
struct Func_2_t3667291446;

#include "mscorlib_System_Collections_ObjectModel_Collection2806094150.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BioCollection`1<System.Object>
struct  BioCollection_1_t694114648  : public Collection_1_t2806094150
{
public:
	// System.Collections.Generic.List`1<System.IDisposable> BioCollection`1::tails
	List_1_t2425880343 * ___tails_2;
	// System.Boolean BioCollection`1::wokeUp
	bool ___wokeUp_3;
	// System.Boolean BioCollection`1::alive
	bool ___alive_4;
	// OnceEmptyStream BioCollection`1::fellAsleep_
	OnceEmptyStream_t3081939404 * ___fellAsleep__5;
	// IOrganism BioCollection`1::carrier_
	Il2CppObject * ___carrier__6;
	// IRoot BioCollection`1::root_
	Il2CppObject * ___root__7;
	// T BioCollection`1::transplantTarget
	Il2CppObject * ___transplantTarget_8;
	// Stream`1<System.Int32> BioCollection`1::countChanged
	Stream_1_t1538553809 * ___countChanged_9;
	// Stream`1<UniRx.Unit> BioCollection`1::collectionReset
	Stream_1_t1249425060 * ___collectionReset_10;
	// Stream`1<UniRx.CollectionAddEvent`1<T>> BioCollection`1::collectionAdd
	Stream_1_t1107661009 * ___collectionAdd_11;
	// Stream`1<CollectionMoveEvent`1<T>> BioCollection`1::collectionMove
	Stream_1_t2263194403 * ___collectionMove_12;
	// Stream`1<CollectionRemoveEvent`1<T>> BioCollection`1::collectionRemove
	Stream_1_t3884365576 * ___collectionRemove_13;
	// Stream`1<CollectionReplaceEvent`1<T>> BioCollection`1::collectionReplace
	Stream_1_t1188511092 * ___collectionReplace_14;
	// Stream`1<System.Collections.Generic.ICollection`1<T>> BioCollection`1::updates
	Stream_1_t4289044124 * ___updates_15;

public:
	inline static int32_t get_offset_of_tails_2() { return static_cast<int32_t>(offsetof(BioCollection_1_t694114648, ___tails_2)); }
	inline List_1_t2425880343 * get_tails_2() const { return ___tails_2; }
	inline List_1_t2425880343 ** get_address_of_tails_2() { return &___tails_2; }
	inline void set_tails_2(List_1_t2425880343 * value)
	{
		___tails_2 = value;
		Il2CppCodeGenWriteBarrier(&___tails_2, value);
	}

	inline static int32_t get_offset_of_wokeUp_3() { return static_cast<int32_t>(offsetof(BioCollection_1_t694114648, ___wokeUp_3)); }
	inline bool get_wokeUp_3() const { return ___wokeUp_3; }
	inline bool* get_address_of_wokeUp_3() { return &___wokeUp_3; }
	inline void set_wokeUp_3(bool value)
	{
		___wokeUp_3 = value;
	}

	inline static int32_t get_offset_of_alive_4() { return static_cast<int32_t>(offsetof(BioCollection_1_t694114648, ___alive_4)); }
	inline bool get_alive_4() const { return ___alive_4; }
	inline bool* get_address_of_alive_4() { return &___alive_4; }
	inline void set_alive_4(bool value)
	{
		___alive_4 = value;
	}

	inline static int32_t get_offset_of_fellAsleep__5() { return static_cast<int32_t>(offsetof(BioCollection_1_t694114648, ___fellAsleep__5)); }
	inline OnceEmptyStream_t3081939404 * get_fellAsleep__5() const { return ___fellAsleep__5; }
	inline OnceEmptyStream_t3081939404 ** get_address_of_fellAsleep__5() { return &___fellAsleep__5; }
	inline void set_fellAsleep__5(OnceEmptyStream_t3081939404 * value)
	{
		___fellAsleep__5 = value;
		Il2CppCodeGenWriteBarrier(&___fellAsleep__5, value);
	}

	inline static int32_t get_offset_of_carrier__6() { return static_cast<int32_t>(offsetof(BioCollection_1_t694114648, ___carrier__6)); }
	inline Il2CppObject * get_carrier__6() const { return ___carrier__6; }
	inline Il2CppObject ** get_address_of_carrier__6() { return &___carrier__6; }
	inline void set_carrier__6(Il2CppObject * value)
	{
		___carrier__6 = value;
		Il2CppCodeGenWriteBarrier(&___carrier__6, value);
	}

	inline static int32_t get_offset_of_root__7() { return static_cast<int32_t>(offsetof(BioCollection_1_t694114648, ___root__7)); }
	inline Il2CppObject * get_root__7() const { return ___root__7; }
	inline Il2CppObject ** get_address_of_root__7() { return &___root__7; }
	inline void set_root__7(Il2CppObject * value)
	{
		___root__7 = value;
		Il2CppCodeGenWriteBarrier(&___root__7, value);
	}

	inline static int32_t get_offset_of_transplantTarget_8() { return static_cast<int32_t>(offsetof(BioCollection_1_t694114648, ___transplantTarget_8)); }
	inline Il2CppObject * get_transplantTarget_8() const { return ___transplantTarget_8; }
	inline Il2CppObject ** get_address_of_transplantTarget_8() { return &___transplantTarget_8; }
	inline void set_transplantTarget_8(Il2CppObject * value)
	{
		___transplantTarget_8 = value;
		Il2CppCodeGenWriteBarrier(&___transplantTarget_8, value);
	}

	inline static int32_t get_offset_of_countChanged_9() { return static_cast<int32_t>(offsetof(BioCollection_1_t694114648, ___countChanged_9)); }
	inline Stream_1_t1538553809 * get_countChanged_9() const { return ___countChanged_9; }
	inline Stream_1_t1538553809 ** get_address_of_countChanged_9() { return &___countChanged_9; }
	inline void set_countChanged_9(Stream_1_t1538553809 * value)
	{
		___countChanged_9 = value;
		Il2CppCodeGenWriteBarrier(&___countChanged_9, value);
	}

	inline static int32_t get_offset_of_collectionReset_10() { return static_cast<int32_t>(offsetof(BioCollection_1_t694114648, ___collectionReset_10)); }
	inline Stream_1_t1249425060 * get_collectionReset_10() const { return ___collectionReset_10; }
	inline Stream_1_t1249425060 ** get_address_of_collectionReset_10() { return &___collectionReset_10; }
	inline void set_collectionReset_10(Stream_1_t1249425060 * value)
	{
		___collectionReset_10 = value;
		Il2CppCodeGenWriteBarrier(&___collectionReset_10, value);
	}

	inline static int32_t get_offset_of_collectionAdd_11() { return static_cast<int32_t>(offsetof(BioCollection_1_t694114648, ___collectionAdd_11)); }
	inline Stream_1_t1107661009 * get_collectionAdd_11() const { return ___collectionAdd_11; }
	inline Stream_1_t1107661009 ** get_address_of_collectionAdd_11() { return &___collectionAdd_11; }
	inline void set_collectionAdd_11(Stream_1_t1107661009 * value)
	{
		___collectionAdd_11 = value;
		Il2CppCodeGenWriteBarrier(&___collectionAdd_11, value);
	}

	inline static int32_t get_offset_of_collectionMove_12() { return static_cast<int32_t>(offsetof(BioCollection_1_t694114648, ___collectionMove_12)); }
	inline Stream_1_t2263194403 * get_collectionMove_12() const { return ___collectionMove_12; }
	inline Stream_1_t2263194403 ** get_address_of_collectionMove_12() { return &___collectionMove_12; }
	inline void set_collectionMove_12(Stream_1_t2263194403 * value)
	{
		___collectionMove_12 = value;
		Il2CppCodeGenWriteBarrier(&___collectionMove_12, value);
	}

	inline static int32_t get_offset_of_collectionRemove_13() { return static_cast<int32_t>(offsetof(BioCollection_1_t694114648, ___collectionRemove_13)); }
	inline Stream_1_t3884365576 * get_collectionRemove_13() const { return ___collectionRemove_13; }
	inline Stream_1_t3884365576 ** get_address_of_collectionRemove_13() { return &___collectionRemove_13; }
	inline void set_collectionRemove_13(Stream_1_t3884365576 * value)
	{
		___collectionRemove_13 = value;
		Il2CppCodeGenWriteBarrier(&___collectionRemove_13, value);
	}

	inline static int32_t get_offset_of_collectionReplace_14() { return static_cast<int32_t>(offsetof(BioCollection_1_t694114648, ___collectionReplace_14)); }
	inline Stream_1_t1188511092 * get_collectionReplace_14() const { return ___collectionReplace_14; }
	inline Stream_1_t1188511092 ** get_address_of_collectionReplace_14() { return &___collectionReplace_14; }
	inline void set_collectionReplace_14(Stream_1_t1188511092 * value)
	{
		___collectionReplace_14 = value;
		Il2CppCodeGenWriteBarrier(&___collectionReplace_14, value);
	}

	inline static int32_t get_offset_of_updates_15() { return static_cast<int32_t>(offsetof(BioCollection_1_t694114648, ___updates_15)); }
	inline Stream_1_t4289044124 * get_updates_15() const { return ___updates_15; }
	inline Stream_1_t4289044124 ** get_address_of_updates_15() { return &___updates_15; }
	inline void set_updates_15(Stream_1_t4289044124 * value)
	{
		___updates_15 = value;
		Il2CppCodeGenWriteBarrier(&___updates_15, value);
	}
};

struct BioCollection_1_t694114648_StaticFields
{
public:
	// System.Action`1<T> BioCollection`1::<>f__am$cacheE
	Action_1_t985559125 * ___U3CU3Ef__amU24cacheE_16;
	// System.Action`1<System.IDisposable> BioCollection`1::<>f__am$cacheF
	Action_1_t1777374079 * ___U3CU3Ef__amU24cacheF_17;
	// System.Action`1<T> BioCollection`1::<>f__am$cache10
	Action_1_t985559125 * ___U3CU3Ef__amU24cache10_18;
	// System.Action`1<T> BioCollection`1::<>f__am$cache11
	Action_1_t985559125 * ___U3CU3Ef__amU24cache11_19;
	// System.Func`2<System.Collections.Generic.ICollection`1<T>,System.Object> BioCollection`1::<>f__am$cache12
	Func_2_t3667291446 * ___U3CU3Ef__amU24cache12_20;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cacheE_16() { return static_cast<int32_t>(offsetof(BioCollection_1_t694114648_StaticFields, ___U3CU3Ef__amU24cacheE_16)); }
	inline Action_1_t985559125 * get_U3CU3Ef__amU24cacheE_16() const { return ___U3CU3Ef__amU24cacheE_16; }
	inline Action_1_t985559125 ** get_address_of_U3CU3Ef__amU24cacheE_16() { return &___U3CU3Ef__amU24cacheE_16; }
	inline void set_U3CU3Ef__amU24cacheE_16(Action_1_t985559125 * value)
	{
		___U3CU3Ef__amU24cacheE_16 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cacheE_16, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cacheF_17() { return static_cast<int32_t>(offsetof(BioCollection_1_t694114648_StaticFields, ___U3CU3Ef__amU24cacheF_17)); }
	inline Action_1_t1777374079 * get_U3CU3Ef__amU24cacheF_17() const { return ___U3CU3Ef__amU24cacheF_17; }
	inline Action_1_t1777374079 ** get_address_of_U3CU3Ef__amU24cacheF_17() { return &___U3CU3Ef__amU24cacheF_17; }
	inline void set_U3CU3Ef__amU24cacheF_17(Action_1_t1777374079 * value)
	{
		___U3CU3Ef__amU24cacheF_17 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cacheF_17, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache10_18() { return static_cast<int32_t>(offsetof(BioCollection_1_t694114648_StaticFields, ___U3CU3Ef__amU24cache10_18)); }
	inline Action_1_t985559125 * get_U3CU3Ef__amU24cache10_18() const { return ___U3CU3Ef__amU24cache10_18; }
	inline Action_1_t985559125 ** get_address_of_U3CU3Ef__amU24cache10_18() { return &___U3CU3Ef__amU24cache10_18; }
	inline void set_U3CU3Ef__amU24cache10_18(Action_1_t985559125 * value)
	{
		___U3CU3Ef__amU24cache10_18 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache10_18, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache11_19() { return static_cast<int32_t>(offsetof(BioCollection_1_t694114648_StaticFields, ___U3CU3Ef__amU24cache11_19)); }
	inline Action_1_t985559125 * get_U3CU3Ef__amU24cache11_19() const { return ___U3CU3Ef__amU24cache11_19; }
	inline Action_1_t985559125 ** get_address_of_U3CU3Ef__amU24cache11_19() { return &___U3CU3Ef__amU24cache11_19; }
	inline void set_U3CU3Ef__amU24cache11_19(Action_1_t985559125 * value)
	{
		___U3CU3Ef__amU24cache11_19 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache11_19, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache12_20() { return static_cast<int32_t>(offsetof(BioCollection_1_t694114648_StaticFields, ___U3CU3Ef__amU24cache12_20)); }
	inline Func_2_t3667291446 * get_U3CU3Ef__amU24cache12_20() const { return ___U3CU3Ef__amU24cache12_20; }
	inline Func_2_t3667291446 ** get_address_of_U3CU3Ef__amU24cache12_20() { return &___U3CU3Ef__amU24cache12_20; }
	inline void set_U3CU3Ef__amU24cache12_20(Func_2_t3667291446 * value)
	{
		___U3CU3Ef__amU24cache12_20 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache12_20, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
