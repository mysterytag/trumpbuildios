﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.WhereObservable`1/Where_<System.Int64>
struct Where__t2908696735;
// UniRx.Operators.WhereObservable`1<System.Int64>
struct WhereObservable_1_t4045974831;
// UniRx.IObserver`1<System.Int64>
struct IObserver_1_t764446489;
// System.IDisposable
struct IDisposable_t1628921374;
// System.Exception
struct Exception_t1967233988;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Exception1967233988.h"

// System.Void UniRx.Operators.WhereObservable`1/Where_<System.Int64>::.ctor(UniRx.Operators.WhereObservable`1<T>,UniRx.IObserver`1<T>,System.IDisposable)
extern "C"  void Where___ctor_m2335664826_gshared (Where__t2908696735 * __this, WhereObservable_1_t4045974831 * ___parent0, Il2CppObject* ___observer1, Il2CppObject * ___cancel2, const MethodInfo* method);
#define Where___ctor_m2335664826(__this, ___parent0, ___observer1, ___cancel2, method) ((  void (*) (Where__t2908696735 *, WhereObservable_1_t4045974831 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))Where___ctor_m2335664826_gshared)(__this, ___parent0, ___observer1, ___cancel2, method)
// System.Void UniRx.Operators.WhereObservable`1/Where_<System.Int64>::OnNext(T)
extern "C"  void Where__OnNext_m3848373856_gshared (Where__t2908696735 * __this, int64_t ___value0, const MethodInfo* method);
#define Where__OnNext_m3848373856(__this, ___value0, method) ((  void (*) (Where__t2908696735 *, int64_t, const MethodInfo*))Where__OnNext_m3848373856_gshared)(__this, ___value0, method)
// System.Void UniRx.Operators.WhereObservable`1/Where_<System.Int64>::OnError(System.Exception)
extern "C"  void Where__OnError_m411885935_gshared (Where__t2908696735 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method);
#define Where__OnError_m411885935(__this, ___error0, method) ((  void (*) (Where__t2908696735 *, Exception_t1967233988 *, const MethodInfo*))Where__OnError_m411885935_gshared)(__this, ___error0, method)
// System.Void UniRx.Operators.WhereObservable`1/Where_<System.Int64>::OnCompleted()
extern "C"  void Where__OnCompleted_m126288706_gshared (Where__t2908696735 * __this, const MethodInfo* method);
#define Where__OnCompleted_m126288706(__this, method) ((  void (*) (Where__t2908696735 *, const MethodInfo*))Where__OnCompleted_m126288706_gshared)(__this, method)
