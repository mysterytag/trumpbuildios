﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.ObserveExtensions/<PublishPocoValueChanged>c__Iterator24`2<System.Object,System.Object>
struct U3CPublishPocoValueChangedU3Ec__Iterator24_2_t3171065807;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void UniRx.ObserveExtensions/<PublishPocoValueChanged>c__Iterator24`2<System.Object,System.Object>::.ctor()
extern "C"  void U3CPublishPocoValueChangedU3Ec__Iterator24_2__ctor_m2887248332_gshared (U3CPublishPocoValueChangedU3Ec__Iterator24_2_t3171065807 * __this, const MethodInfo* method);
#define U3CPublishPocoValueChangedU3Ec__Iterator24_2__ctor_m2887248332(__this, method) ((  void (*) (U3CPublishPocoValueChangedU3Ec__Iterator24_2_t3171065807 *, const MethodInfo*))U3CPublishPocoValueChangedU3Ec__Iterator24_2__ctor_m2887248332_gshared)(__this, method)
// System.Object UniRx.ObserveExtensions/<PublishPocoValueChanged>c__Iterator24`2<System.Object,System.Object>::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CPublishPocoValueChangedU3Ec__Iterator24_2_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m510261958_gshared (U3CPublishPocoValueChangedU3Ec__Iterator24_2_t3171065807 * __this, const MethodInfo* method);
#define U3CPublishPocoValueChangedU3Ec__Iterator24_2_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m510261958(__this, method) ((  Il2CppObject * (*) (U3CPublishPocoValueChangedU3Ec__Iterator24_2_t3171065807 *, const MethodInfo*))U3CPublishPocoValueChangedU3Ec__Iterator24_2_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m510261958_gshared)(__this, method)
// System.Object UniRx.ObserveExtensions/<PublishPocoValueChanged>c__Iterator24`2<System.Object,System.Object>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CPublishPocoValueChangedU3Ec__Iterator24_2_System_Collections_IEnumerator_get_Current_m3448130138_gshared (U3CPublishPocoValueChangedU3Ec__Iterator24_2_t3171065807 * __this, const MethodInfo* method);
#define U3CPublishPocoValueChangedU3Ec__Iterator24_2_System_Collections_IEnumerator_get_Current_m3448130138(__this, method) ((  Il2CppObject * (*) (U3CPublishPocoValueChangedU3Ec__Iterator24_2_t3171065807 *, const MethodInfo*))U3CPublishPocoValueChangedU3Ec__Iterator24_2_System_Collections_IEnumerator_get_Current_m3448130138_gshared)(__this, method)
// System.Boolean UniRx.ObserveExtensions/<PublishPocoValueChanged>c__Iterator24`2<System.Object,System.Object>::MoveNext()
extern "C"  bool U3CPublishPocoValueChangedU3Ec__Iterator24_2_MoveNext_m1249446336_gshared (U3CPublishPocoValueChangedU3Ec__Iterator24_2_t3171065807 * __this, const MethodInfo* method);
#define U3CPublishPocoValueChangedU3Ec__Iterator24_2_MoveNext_m1249446336(__this, method) ((  bool (*) (U3CPublishPocoValueChangedU3Ec__Iterator24_2_t3171065807 *, const MethodInfo*))U3CPublishPocoValueChangedU3Ec__Iterator24_2_MoveNext_m1249446336_gshared)(__this, method)
// System.Void UniRx.ObserveExtensions/<PublishPocoValueChanged>c__Iterator24`2<System.Object,System.Object>::Dispose()
extern "C"  void U3CPublishPocoValueChangedU3Ec__Iterator24_2_Dispose_m4290024201_gshared (U3CPublishPocoValueChangedU3Ec__Iterator24_2_t3171065807 * __this, const MethodInfo* method);
#define U3CPublishPocoValueChangedU3Ec__Iterator24_2_Dispose_m4290024201(__this, method) ((  void (*) (U3CPublishPocoValueChangedU3Ec__Iterator24_2_t3171065807 *, const MethodInfo*))U3CPublishPocoValueChangedU3Ec__Iterator24_2_Dispose_m4290024201_gshared)(__this, method)
// System.Void UniRx.ObserveExtensions/<PublishPocoValueChanged>c__Iterator24`2<System.Object,System.Object>::Reset()
extern "C"  void U3CPublishPocoValueChangedU3Ec__Iterator24_2_Reset_m533681273_gshared (U3CPublishPocoValueChangedU3Ec__Iterator24_2_t3171065807 * __this, const MethodInfo* method);
#define U3CPublishPocoValueChangedU3Ec__Iterator24_2_Reset_m533681273(__this, method) ((  void (*) (U3CPublishPocoValueChangedU3Ec__Iterator24_2_t3171065807 *, const MethodInfo*))U3CPublishPocoValueChangedU3Ec__Iterator24_2_Reset_m533681273_gshared)(__this, method)
// System.Void UniRx.ObserveExtensions/<PublishPocoValueChanged>c__Iterator24`2<System.Object,System.Object>::<>__Finally0()
extern "C"  void U3CPublishPocoValueChangedU3Ec__Iterator24_2_U3CU3E__Finally0_m4173751079_gshared (U3CPublishPocoValueChangedU3Ec__Iterator24_2_t3171065807 * __this, const MethodInfo* method);
#define U3CPublishPocoValueChangedU3Ec__Iterator24_2_U3CU3E__Finally0_m4173751079(__this, method) ((  void (*) (U3CPublishPocoValueChangedU3Ec__Iterator24_2_t3171065807 *, const MethodInfo*))U3CPublishPocoValueChangedU3Ec__Iterator24_2_U3CU3E__Finally0_m4173751079_gshared)(__this, method)
