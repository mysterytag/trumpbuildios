﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.IEnumerator
struct IEnumerator_t287207039;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Observable/<SelectMany>c__AnonStorey4D`1<System.Object>
struct  U3CSelectManyU3Ec__AnonStorey4D_1_t352756178  : public Il2CppObject
{
public:
	// System.Collections.IEnumerator UniRx.Observable/<SelectMany>c__AnonStorey4D`1::coroutine
	Il2CppObject * ___coroutine_0;

public:
	inline static int32_t get_offset_of_coroutine_0() { return static_cast<int32_t>(offsetof(U3CSelectManyU3Ec__AnonStorey4D_1_t352756178, ___coroutine_0)); }
	inline Il2CppObject * get_coroutine_0() const { return ___coroutine_0; }
	inline Il2CppObject ** get_address_of_coroutine_0() { return &___coroutine_0; }
	inline void set_coroutine_0(Il2CppObject * value)
	{
		___coroutine_0 = value;
		Il2CppCodeGenWriteBarrier(&___coroutine_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
