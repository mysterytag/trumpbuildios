﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.IgnoreElementsObservable`1/IgnoreElements<System.Object>
struct IgnoreElements_t1769081498;
// UniRx.IObserver`1<System.Object>
struct IObserver_1_t3049105323;
// System.IDisposable
struct IDisposable_t1628921374;
// System.Object
struct Il2CppObject;
// System.Exception
struct Exception_t1967233988;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_Exception1967233988.h"

// System.Void UniRx.Operators.IgnoreElementsObservable`1/IgnoreElements<System.Object>::.ctor(UniRx.IObserver`1<T>,System.IDisposable)
extern "C"  void IgnoreElements__ctor_m1978479027_gshared (IgnoreElements_t1769081498 * __this, Il2CppObject* ___observer0, Il2CppObject * ___cancel1, const MethodInfo* method);
#define IgnoreElements__ctor_m1978479027(__this, ___observer0, ___cancel1, method) ((  void (*) (IgnoreElements_t1769081498 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))IgnoreElements__ctor_m1978479027_gshared)(__this, ___observer0, ___cancel1, method)
// System.Void UniRx.Operators.IgnoreElementsObservable`1/IgnoreElements<System.Object>::OnNext(T)
extern "C"  void IgnoreElements_OnNext_m353660415_gshared (IgnoreElements_t1769081498 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define IgnoreElements_OnNext_m353660415(__this, ___value0, method) ((  void (*) (IgnoreElements_t1769081498 *, Il2CppObject *, const MethodInfo*))IgnoreElements_OnNext_m353660415_gshared)(__this, ___value0, method)
// System.Void UniRx.Operators.IgnoreElementsObservable`1/IgnoreElements<System.Object>::OnError(System.Exception)
extern "C"  void IgnoreElements_OnError_m85109518_gshared (IgnoreElements_t1769081498 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method);
#define IgnoreElements_OnError_m85109518(__this, ___error0, method) ((  void (*) (IgnoreElements_t1769081498 *, Exception_t1967233988 *, const MethodInfo*))IgnoreElements_OnError_m85109518_gshared)(__this, ___error0, method)
// System.Void UniRx.Operators.IgnoreElementsObservable`1/IgnoreElements<System.Object>::OnCompleted()
extern "C"  void IgnoreElements_OnCompleted_m3459187553_gshared (IgnoreElements_t1769081498 * __this, const MethodInfo* method);
#define IgnoreElements_OnCompleted_m3459187553(__this, method) ((  void (*) (IgnoreElements_t1769081498 *, const MethodInfo*))IgnoreElements_OnCompleted_m3459187553_gshared)(__this, method)
