﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.CombineLatestObservable`3/CombineLatest<System.Boolean,System.Boolean,System.Object>
struct CombineLatest_t4145300795;
// UniRx.Operators.CombineLatestObservable`3<System.Boolean,System.Boolean,System.Object>
struct CombineLatestObservable_3_t243429454;
// UniRx.IObserver`1<System.Object>
struct IObserver_1_t3049105323;
// System.IDisposable
struct IDisposable_t1628921374;
// System.Object
struct Il2CppObject;
// System.Exception
struct Exception_t1967233988;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_Exception1967233988.h"

// System.Void UniRx.Operators.CombineLatestObservable`3/CombineLatest<System.Boolean,System.Boolean,System.Object>::.ctor(UniRx.Operators.CombineLatestObservable`3<TLeft,TRight,TResult>,UniRx.IObserver`1<TResult>,System.IDisposable)
extern "C"  void CombineLatest__ctor_m2104227009_gshared (CombineLatest_t4145300795 * __this, CombineLatestObservable_3_t243429454 * ___parent0, Il2CppObject* ___observer1, Il2CppObject * ___cancel2, const MethodInfo* method);
#define CombineLatest__ctor_m2104227009(__this, ___parent0, ___observer1, ___cancel2, method) ((  void (*) (CombineLatest_t4145300795 *, CombineLatestObservable_3_t243429454 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))CombineLatest__ctor_m2104227009_gshared)(__this, ___parent0, ___observer1, ___cancel2, method)
// System.IDisposable UniRx.Operators.CombineLatestObservable`3/CombineLatest<System.Boolean,System.Boolean,System.Object>::Run()
extern "C"  Il2CppObject * CombineLatest_Run_m781429213_gshared (CombineLatest_t4145300795 * __this, const MethodInfo* method);
#define CombineLatest_Run_m781429213(__this, method) ((  Il2CppObject * (*) (CombineLatest_t4145300795 *, const MethodInfo*))CombineLatest_Run_m781429213_gshared)(__this, method)
// System.Void UniRx.Operators.CombineLatestObservable`3/CombineLatest<System.Boolean,System.Boolean,System.Object>::Publish()
extern "C"  void CombineLatest_Publish_m505661308_gshared (CombineLatest_t4145300795 * __this, const MethodInfo* method);
#define CombineLatest_Publish_m505661308(__this, method) ((  void (*) (CombineLatest_t4145300795 *, const MethodInfo*))CombineLatest_Publish_m505661308_gshared)(__this, method)
// System.Void UniRx.Operators.CombineLatestObservable`3/CombineLatest<System.Boolean,System.Boolean,System.Object>::OnNext(TResult)
extern "C"  void CombineLatest_OnNext_m2474605530_gshared (CombineLatest_t4145300795 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define CombineLatest_OnNext_m2474605530(__this, ___value0, method) ((  void (*) (CombineLatest_t4145300795 *, Il2CppObject *, const MethodInfo*))CombineLatest_OnNext_m2474605530_gshared)(__this, ___value0, method)
// System.Void UniRx.Operators.CombineLatestObservable`3/CombineLatest<System.Boolean,System.Boolean,System.Object>::OnError(System.Exception)
extern "C"  void CombineLatest_OnError_m2727397382_gshared (CombineLatest_t4145300795 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method);
#define CombineLatest_OnError_m2727397382(__this, ___error0, method) ((  void (*) (CombineLatest_t4145300795 *, Exception_t1967233988 *, const MethodInfo*))CombineLatest_OnError_m2727397382_gshared)(__this, ___error0, method)
// System.Void UniRx.Operators.CombineLatestObservable`3/CombineLatest<System.Boolean,System.Boolean,System.Object>::OnCompleted()
extern "C"  void CombineLatest_OnCompleted_m1509348441_gshared (CombineLatest_t4145300795 * __this, const MethodInfo* method);
#define CombineLatest_OnCompleted_m1509348441(__this, method) ((  void (*) (CombineLatest_t4145300795 *, const MethodInfo*))CombineLatest_OnCompleted_m1509348441_gshared)(__this, method)
