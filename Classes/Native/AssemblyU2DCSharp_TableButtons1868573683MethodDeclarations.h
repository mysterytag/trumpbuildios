﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// TableButtons
struct TableButtons_t1868573683;
// UpgradeButton
struct UpgradeButton_t1475467342;
// UpgradeCell
struct UpgradeCell_t4117746046;
// System.Collections.Generic.List`1<Cell`1<System.Int32>>
struct List_1_t3826471388;
// Cell`1<System.DateTime>
struct Cell_1_t521131568;
// Tacticsoft.ITableViewDataSource
struct ITableViewDataSource_t1629295493;
// System.String
struct String_t;
// ZergRush.InorganicCollection`1<UpgradeButton>
struct InorganicCollection_1_t4089338440;
// System.Action`3<UpgradeButton,UpgradeCell,System.Int32>
struct Action_3_t1751200134;
// System.Action`1<DonateCell>
struct Action_1_t2946927090;
// SettingsButton
struct SettingsButton_t915256661;
// SettingsCell
struct SettingsCell_t698172485;
// DonateButton
struct DonateButton_t670753441;
// DonateCell
struct DonateCell_t2798474385;
// <>__AnonType5`2<System.DateTime,System.Int64>
struct U3CU3E__AnonType5_2_t3897323094;
// <>__AnonType6`2<System.Double,System.Int32>
struct U3CU3E__AnonType6_2_t3631555212;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_UpgradeButton1475467342.h"
#include "AssemblyU2DCSharp_UpgradeCell4117746046.h"
#include "AssemblyU2DCSharp_GlobalStat_UpgradeType4118271830.h"
#include "mscorlib_System_String968488902.h"
#include "AssemblyU2DCSharp_SettingsButton915256661.h"
#include "AssemblyU2DCSharp_SettingsCell698172485.h"
#include "AssemblyU2DCSharp_DonateButton670753441.h"
#include "AssemblyU2DCSharp_DonateCell2798474385.h"
#include "mscorlib_System_DateTime339033936.h"

// System.Void TableButtons::.ctor()
extern "C"  void TableButtons__ctor_m2619246600 (TableButtons_t1868573683 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TableButtons::Initialize()
extern "C"  void TableButtons_Initialize_m2613678988 (TableButtons_t1868573683 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TableButtons::SetSafeButtons()
extern "C"  void TableButtons_SetSafeButtons_m1231948654 (TableButtons_t1868573683 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TableButtons::SetSettingsButtons()
extern "C"  void TableButtons_SetSettingsButtons_m1304302360 (TableButtons_t1868573683 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TableButtons::SetDonateButtons()
extern "C"  void TableButtons_SetDonateButtons_m1726772364 (TableButtons_t1868573683 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TableButtons::DisableHandler(UpgradeButton,UpgradeCell,System.Collections.Generic.List`1<Cell`1<System.Int32>>,GlobalStat/UpgradeType)
extern "C"  void TableButtons_DisableHandler_m3467529826 (TableButtons_t1868573683 * __this, UpgradeButton_t1475467342 * ___button0, UpgradeCell_t4117746046 * ___cell1, List_1_t3826471388 * ___levelsContainer2, int32_t ___upgradeType3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TableButtons::ClickHandler(UpgradeButton,GlobalStat/UpgradeType)
extern "C"  void TableButtons_ClickHandler_m915281218 (TableButtons_t1868573683 * __this, UpgradeButton_t1475467342 * ___button0, int32_t ___upgradeType1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TableButtons::ShowSpeedUp(Cell`1<System.DateTime>)
extern "C"  void TableButtons_ShowSpeedUp_m2181920058 (TableButtons_t1868573683 * __this, Cell_1_t521131568 * ___deliveryTime0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TableButtons::SetTable(Tacticsoft.ITableViewDataSource,System.String,System.String,System.String,System.String,System.String)
extern "C"  void TableButtons_SetTable_m3522975035 (TableButtons_t1868573683 * __this, Il2CppObject * ___dataSource0, String_t* ___menuName1, String_t* ___menuDescription2, String_t* ___purchaseTranslateId3, String_t* ___premiumSprite4, String_t* ___premiumSku5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TableButtons::FillMethodBase(UpgradeCell,UpgradeButton,ZergRush.InorganicCollection`1<UpgradeButton>,GlobalStat/UpgradeType,System.Collections.Generic.List`1<Cell`1<System.Int32>>,System.Action`3<UpgradeButton,UpgradeCell,System.Int32>,System.String)
extern "C"  void TableButtons_FillMethodBase_m883010003 (TableButtons_t1868573683 * __this, UpgradeCell_t4117746046 * ___cell0, UpgradeButton_t1475467342 * ___button1, InorganicCollection_1_t4089338440 * ___buttons2, int32_t ___upgradeType3, List_1_t3826471388 * ___levelsList4, Action_3_t1751200134 * ___fillWithlevel5, String_t* ___iconString6, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TableButtons::AddOpenIabButton(System.String,System.String,System.String,System.String,System.Action`1<DonateCell>)
extern "C"  void TableButtons_AddOpenIabButton_m649527518 (TableButtons_t1868573683 * __this, String_t* ___text0, String_t* ___titleTranslateId1, String_t* ___sprite2, String_t* ___soloTranslateId3, Action_1_t2946927090 * ___initCell4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TableButtons::OpenIabRequest(System.String)
extern "C"  void TableButtons_OpenIabRequest_m1009737559 (TableButtons_t1868573683 * __this, String_t* ___s0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TableButtons::RegisterDonateButtons()
extern "C"  void TableButtons_RegisterDonateButtons_m3596490645 (TableButtons_t1868573683 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TableButtons::RegisterButtons()
extern "C"  void TableButtons_RegisterButtons_m1377525444 (TableButtons_t1868573683 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TableButtons::AddPassiveButton(System.String)
extern "C"  void TableButtons_AddPassiveButton_m602524590 (TableButtons_t1868573683 * __this, String_t* ___titleId0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TableButtons::AddBillButton(System.String)
extern "C"  void TableButtons_AddBillButton_m571332546 (TableButtons_t1868573683 * __this, String_t* ___titleId0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TableButtons::AddSafeButton(System.String)
extern "C"  void TableButtons_AddSafeButton_m2657405948 (TableButtons_t1868573683 * __this, String_t* ___titleId0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TableButtons::AddButtonBase(System.String,ZergRush.InorganicCollection`1<UpgradeButton>)
extern "C"  void TableButtons_AddButtonBase_m220227671 (TableButtons_t1868573683 * __this, String_t* ___titleId0, InorganicCollection_1_t4089338440 * ___inorganicCollection1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TableButtons::SetBillButtons()
extern "C"  void TableButtons_SetBillButtons_m3685093428 (TableButtons_t1868573683 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TableButtons::SetPassiveButtons()
extern "C"  void TableButtons_SetPassiveButtons_m695241218 (TableButtons_t1868573683 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TableButtons::<Initialize>m__225(SettingsButton,SettingsCell)
extern "C"  void TableButtons_U3CInitializeU3Em__225_m4103028992 (Il2CppObject * __this /* static, unused */, SettingsButton_t915256661 * ___infoS0, SettingsCell_t698172485 * ___prefabS1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TableButtons::<Initialize>m__226(DonateButton,DonateCell)
extern "C"  void TableButtons_U3CInitializeU3Em__226_m590172255 (Il2CppObject * __this /* static, unused */, DonateButton_t670753441 * ___info0, DonateCell_t2798474385 * ___prefab1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TableButtons::<Initialize>m__227(UpgradeButton,UpgradeCell)
extern "C"  void TableButtons_U3CInitializeU3Em__227_m3698541338 (TableButtons_t1868573683 * __this, UpgradeButton_t1475467342 * ___button0, UpgradeCell_t4117746046 * ___cell1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TableButtons::<Initialize>m__228(UpgradeButton,UpgradeCell)
extern "C"  void TableButtons_U3CInitializeU3Em__228_m3443804793 (TableButtons_t1868573683 * __this, UpgradeButton_t1475467342 * ___button0, UpgradeCell_t4117746046 * ___cell1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TableButtons::<Initialize>m__229(UpgradeButton,UpgradeCell)
extern "C"  void TableButtons_U3CInitializeU3Em__229_m3189068248 (TableButtons_t1868573683 * __this, UpgradeButton_t1475467342 * ___button0, UpgradeCell_t4117746046 * ___cell1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Double TableButtons::<DisableHandler>m__22A(System.Double,System.Int32)
extern "C"  double TableButtons_U3CDisableHandlerU3Em__22A_m2034505928 (Il2CppObject * __this /* static, unused */, double ___m0, int32_t ___l1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// <>__AnonType5`2<System.DateTime,System.Int64> TableButtons::<FillMethodBase>m__233(System.DateTime,System.Int64)
extern "C"  U3CU3E__AnonType5_2_t3897323094 * TableButtons_U3CFillMethodBaseU3Em__233_m1665189344 (Il2CppObject * __this /* static, unused */, DateTime_t339033936  ___time0, int64_t ____1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// <>__AnonType6`2<System.Double,System.Int32> TableButtons::<FillMethodBase>m__236(System.Double,System.Int32)
extern "C"  U3CU3E__AnonType6_2_t3631555212 * TableButtons_U3CFillMethodBaseU3Em__236_m1879044514 (Il2CppObject * __this /* static, unused */, double ___money0, int32_t ___level1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TableButtons::<RegisterDonateButtons>m__24E(DonateCell)
extern "C"  void TableButtons_U3CRegisterDonateButtonsU3Em__24E_m3676087606 (Il2CppObject * __this /* static, unused */, DonateCell_t2798474385 * ___cell0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
