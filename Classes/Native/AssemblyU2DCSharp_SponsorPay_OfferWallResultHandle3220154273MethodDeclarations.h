﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SponsorPay.OfferWallResultHandler
struct OfferWallResultHandler_t3220154273;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;
// System.IAsyncResult
struct IAsyncResult_t537683269;
// System.AsyncCallback
struct AsyncCallback_t1363551830;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_IntPtr676692020.h"
#include "mscorlib_System_String968488902.h"
#include "mscorlib_System_AsyncCallback1363551830.h"

// System.Void SponsorPay.OfferWallResultHandler::.ctor(System.Object,System.IntPtr)
extern "C"  void OfferWallResultHandler__ctor_m3155796702 (OfferWallResultHandler_t3220154273 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SponsorPay.OfferWallResultHandler::Invoke(System.String)
extern "C"  void OfferWallResultHandler_Invoke_m1431986506 (OfferWallResultHandler_t3220154273 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
extern "C" void pinvoke_delegate_wrapper_OfferWallResultHandler_t3220154273(Il2CppObject* delegate, String_t* ___message0);
// System.IAsyncResult SponsorPay.OfferWallResultHandler::BeginInvoke(System.String,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * OfferWallResultHandler_BeginInvoke_m1900612439 (OfferWallResultHandler_t3220154273 * __this, String_t* ___message0, AsyncCallback_t1363551830 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SponsorPay.OfferWallResultHandler::EndInvoke(System.IAsyncResult)
extern "C"  void OfferWallResultHandler_EndInvoke_m1962750958 (OfferWallResultHandler_t3220154273 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
