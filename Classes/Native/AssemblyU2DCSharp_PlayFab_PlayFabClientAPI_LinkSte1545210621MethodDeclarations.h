﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.PlayFabClientAPI/LinkSteamAccountRequestCallback
struct LinkSteamAccountRequestCallback_t1545210621;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;
// PlayFab.ClientModels.LinkSteamAccountRequest
struct LinkSteamAccountRequest_t3142953640;
// System.IAsyncResult
struct IAsyncResult_t537683269;
// System.AsyncCallback
struct AsyncCallback_t1363551830;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_IntPtr676692020.h"
#include "mscorlib_System_String968488902.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_LinkSteamAc3142953640.h"
#include "mscorlib_System_AsyncCallback1363551830.h"

// System.Void PlayFab.PlayFabClientAPI/LinkSteamAccountRequestCallback::.ctor(System.Object,System.IntPtr)
extern "C"  void LinkSteamAccountRequestCallback__ctor_m4271283468 (LinkSteamAccountRequestCallback_t1545210621 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/LinkSteamAccountRequestCallback::Invoke(System.String,System.Int32,PlayFab.ClientModels.LinkSteamAccountRequest,System.Object)
extern "C"  void LinkSteamAccountRequestCallback_Invoke_m2605037407 (LinkSteamAccountRequestCallback_t1545210621 * __this, String_t* ___urlPath0, int32_t ___callId1, LinkSteamAccountRequest_t3142953640 * ___request2, Il2CppObject * ___customData3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
extern "C" void pinvoke_delegate_wrapper_LinkSteamAccountRequestCallback_t1545210621(Il2CppObject* delegate, String_t* ___urlPath0, int32_t ___callId1, LinkSteamAccountRequest_t3142953640 * ___request2, Il2CppObject * ___customData3);
// System.IAsyncResult PlayFab.PlayFabClientAPI/LinkSteamAccountRequestCallback::BeginInvoke(System.String,System.Int32,PlayFab.ClientModels.LinkSteamAccountRequest,System.Object,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * LinkSteamAccountRequestCallback_BeginInvoke_m3871286270 (LinkSteamAccountRequestCallback_t1545210621 * __this, String_t* ___urlPath0, int32_t ___callId1, LinkSteamAccountRequest_t3142953640 * ___request2, Il2CppObject * ___customData3, AsyncCallback_t1363551830 * ___callback4, Il2CppObject * ___object5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/LinkSteamAccountRequestCallback::EndInvoke(System.IAsyncResult)
extern "C"  void LinkSteamAccountRequestCallback_EndInvoke_m2809820444 (LinkSteamAccountRequestCallback_t1545210621 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
