﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.PlayFabClientAPI/<GetPlayFabIDsFromKongregateIDs>c__AnonStorey71
struct U3CGetPlayFabIDsFromKongregateIDsU3Ec__AnonStorey71_t431690775;
// PlayFab.CallRequestContainer
struct CallRequestContainer_t432318609;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharpU2Dfirstpass_PlayFab_CallRequestCo432318609.h"

// System.Void PlayFab.PlayFabClientAPI/<GetPlayFabIDsFromKongregateIDs>c__AnonStorey71::.ctor()
extern "C"  void U3CGetPlayFabIDsFromKongregateIDsU3Ec__AnonStorey71__ctor_m2281540988 (U3CGetPlayFabIDsFromKongregateIDsU3Ec__AnonStorey71_t431690775 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/<GetPlayFabIDsFromKongregateIDs>c__AnonStorey71::<>m__5E(PlayFab.CallRequestContainer)
extern "C"  void U3CGetPlayFabIDsFromKongregateIDsU3Ec__AnonStorey71_U3CU3Em__5E_m1883246090 (U3CGetPlayFabIDsFromKongregateIDsU3Ec__AnonStorey71_t431690775 * __this, CallRequestContainer_t432318609 * ___requestContainer0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
