﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Xml.XPath.ExprFilter
struct ExprFilter_t3320087274;
// System.Xml.XPath.Expression
struct Expression_t4217024437;
// System.String
struct String_t;
// System.Object
struct Il2CppObject;
// System.Xml.XPath.BaseIterator
struct BaseIterator_t3696600956;

#include "codegen/il2cpp-codegen.h"
#include "System_Xml_System_Xml_XPath_Expression4217024437.h"
#include "System_Xml_System_Xml_XPath_BaseIterator3696600956.h"

// System.Void System.Xml.XPath.ExprFilter::.ctor(System.Xml.XPath.Expression,System.Xml.XPath.Expression)
extern "C"  void ExprFilter__ctor_m1987528211 (ExprFilter_t3320087274 * __this, Expression_t4217024437 * ___expr0, Expression_t4217024437 * ___pred1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Xml.XPath.Expression System.Xml.XPath.ExprFilter::Optimize()
extern "C"  Expression_t4217024437 * ExprFilter_Optimize_m4005115936 (ExprFilter_t3320087274 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Xml.XPath.ExprFilter::ToString()
extern "C"  String_t* ExprFilter_ToString_m1575612896 (ExprFilter_t3320087274 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Xml.XPath.ExprFilter::Evaluate(System.Xml.XPath.BaseIterator)
extern "C"  Il2CppObject * ExprFilter_Evaluate_m1209514577 (ExprFilter_t3320087274 * __this, BaseIterator_t3696600956 * ___iter0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Xml.XPath.ExprFilter::get_Peer()
extern "C"  bool ExprFilter_get_Peer_m1513885530 (ExprFilter_t3320087274 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Xml.XPath.ExprFilter::get_Subtree()
extern "C"  bool ExprFilter_get_Subtree_m1459955336 (ExprFilter_t3320087274 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
