﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SponsorPay.NativeExceptionHandler
struct NativeExceptionHandler_t1751116300;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;
// System.IAsyncResult
struct IAsyncResult_t537683269;
// System.AsyncCallback
struct AsyncCallback_t1363551830;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_IntPtr676692020.h"
#include "mscorlib_System_String968488902.h"
#include "mscorlib_System_AsyncCallback1363551830.h"

// System.Void SponsorPay.NativeExceptionHandler::.ctor(System.Object,System.IntPtr)
extern "C"  void NativeExceptionHandler__ctor_m1748388809 (NativeExceptionHandler_t1751116300 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SponsorPay.NativeExceptionHandler::Invoke(System.String)
extern "C"  void NativeExceptionHandler_Invoke_m3932591743 (NativeExceptionHandler_t1751116300 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
extern "C" void pinvoke_delegate_wrapper_NativeExceptionHandler_t1751116300(Il2CppObject* delegate, String_t* ___message0);
// System.IAsyncResult SponsorPay.NativeExceptionHandler::BeginInvoke(System.String,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * NativeExceptionHandler_BeginInvoke_m1348806540 (NativeExceptionHandler_t1751116300 * __this, String_t* ___message0, AsyncCallback_t1363551830 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SponsorPay.NativeExceptionHandler::EndInvoke(System.IAsyncResult)
extern "C"  void NativeExceptionHandler_EndInvoke_m2500921945 (NativeExceptionHandler_t1751116300 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
