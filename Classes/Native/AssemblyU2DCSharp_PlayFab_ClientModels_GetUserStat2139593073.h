﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Generic.Dictionary`2<System.String,System.Int32>
struct Dictionary_2_t190145395;

#include "AssemblyU2DCSharp_PlayFab_Internal_PlayFabResultCom379512675.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PlayFab.ClientModels.GetUserStatisticsResult
struct  GetUserStatisticsResult_t2139593073  : public PlayFabResultCommon_t379512675
{
public:
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> PlayFab.ClientModels.GetUserStatisticsResult::<UserStatistics>k__BackingField
	Dictionary_2_t190145395 * ___U3CUserStatisticsU3Ek__BackingField_2;

public:
	inline static int32_t get_offset_of_U3CUserStatisticsU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(GetUserStatisticsResult_t2139593073, ___U3CUserStatisticsU3Ek__BackingField_2)); }
	inline Dictionary_2_t190145395 * get_U3CUserStatisticsU3Ek__BackingField_2() const { return ___U3CUserStatisticsU3Ek__BackingField_2; }
	inline Dictionary_2_t190145395 ** get_address_of_U3CUserStatisticsU3Ek__BackingField_2() { return &___U3CUserStatisticsU3Ek__BackingField_2; }
	inline void set_U3CUserStatisticsU3Ek__BackingField_2(Dictionary_2_t190145395 * value)
	{
		___U3CUserStatisticsU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier(&___U3CUserStatisticsU3Ek__BackingField_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
