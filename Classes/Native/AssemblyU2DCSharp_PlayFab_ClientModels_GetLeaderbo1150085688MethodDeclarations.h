﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.ClientModels.GetLeaderboardRequest
struct GetLeaderboardRequest_t1150085688;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String968488902.h"
#include "mscorlib_System_Nullable_1_gen1438485399.h"

// System.Void PlayFab.ClientModels.GetLeaderboardRequest::.ctor()
extern "C"  void GetLeaderboardRequest__ctor_m2214077425 (GetLeaderboardRequest_t1150085688 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PlayFab.ClientModels.GetLeaderboardRequest::get_StatisticName()
extern "C"  String_t* GetLeaderboardRequest_get_StatisticName_m403507166 (GetLeaderboardRequest_t1150085688 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.GetLeaderboardRequest::set_StatisticName(System.String)
extern "C"  void GetLeaderboardRequest_set_StatisticName_m907397301 (GetLeaderboardRequest_t1150085688 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 PlayFab.ClientModels.GetLeaderboardRequest::get_StartPosition()
extern "C"  int32_t GetLeaderboardRequest_get_StartPosition_m2087199299 (GetLeaderboardRequest_t1150085688 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.GetLeaderboardRequest::set_StartPosition(System.Int32)
extern "C"  void GetLeaderboardRequest_set_StartPosition_m731829486 (GetLeaderboardRequest_t1150085688 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Nullable`1<System.Int32> PlayFab.ClientModels.GetLeaderboardRequest::get_MaxResultsCount()
extern "C"  Nullable_1_t1438485399  GetLeaderboardRequest_get_MaxResultsCount_m874363860 (GetLeaderboardRequest_t1150085688 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.GetLeaderboardRequest::set_MaxResultsCount(System.Nullable`1<System.Int32>)
extern "C"  void GetLeaderboardRequest_set_MaxResultsCount_m2087649151 (GetLeaderboardRequest_t1150085688 * __this, Nullable_1_t1438485399  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
