﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.ClientModels.PlayerLeaderboardEntry
struct PlayerLeaderboardEntry_t2241891366;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String968488902.h"

// System.Void PlayFab.ClientModels.PlayerLeaderboardEntry::.ctor()
extern "C"  void PlayerLeaderboardEntry__ctor_m3341829975 (PlayerLeaderboardEntry_t2241891366 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PlayFab.ClientModels.PlayerLeaderboardEntry::get_PlayFabId()
extern "C"  String_t* PlayerLeaderboardEntry_get_PlayFabId_m944328925 (PlayerLeaderboardEntry_t2241891366 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.PlayerLeaderboardEntry::set_PlayFabId(System.String)
extern "C"  void PlayerLeaderboardEntry_set_PlayFabId_m3384811644 (PlayerLeaderboardEntry_t2241891366 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PlayFab.ClientModels.PlayerLeaderboardEntry::get_DisplayName()
extern "C"  String_t* PlayerLeaderboardEntry_get_DisplayName_m3000298588 (PlayerLeaderboardEntry_t2241891366 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.PlayerLeaderboardEntry::set_DisplayName(System.String)
extern "C"  void PlayerLeaderboardEntry_set_DisplayName_m915110301 (PlayerLeaderboardEntry_t2241891366 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 PlayFab.ClientModels.PlayerLeaderboardEntry::get_StatValue()
extern "C"  int32_t PlayerLeaderboardEntry_get_StatValue_m3273454167 (PlayerLeaderboardEntry_t2241891366 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.PlayerLeaderboardEntry::set_StatValue(System.Int32)
extern "C"  void PlayerLeaderboardEntry_set_StatValue_m499900966 (PlayerLeaderboardEntry_t2241891366 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 PlayFab.ClientModels.PlayerLeaderboardEntry::get_Position()
extern "C"  int32_t PlayerLeaderboardEntry_get_Position_m1726384049 (PlayerLeaderboardEntry_t2241891366 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.PlayerLeaderboardEntry::set_Position(System.Int32)
extern "C"  void PlayerLeaderboardEntry_set_Position_m2700123844 (PlayerLeaderboardEntry_t2241891366 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
