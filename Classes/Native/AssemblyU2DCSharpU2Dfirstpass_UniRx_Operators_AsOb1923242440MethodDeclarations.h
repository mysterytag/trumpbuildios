﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.AsObservableObservable`1/AsObservable<UniRx.Unit>
struct AsObservable_t1923242440;
// UniRx.IObserver`1<UniRx.Unit>
struct IObserver_1_t475317645;
// System.IDisposable
struct IDisposable_t1628921374;
// System.Exception
struct Exception_t1967233988;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Unit2558286038.h"
#include "mscorlib_System_Exception1967233988.h"

// System.Void UniRx.Operators.AsObservableObservable`1/AsObservable<UniRx.Unit>::.ctor(UniRx.IObserver`1<T>,System.IDisposable)
extern "C"  void AsObservable__ctor_m2789927517_gshared (AsObservable_t1923242440 * __this, Il2CppObject* ___observer0, Il2CppObject * ___cancel1, const MethodInfo* method);
#define AsObservable__ctor_m2789927517(__this, ___observer0, ___cancel1, method) ((  void (*) (AsObservable_t1923242440 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))AsObservable__ctor_m2789927517_gshared)(__this, ___observer0, ___cancel1, method)
// System.Void UniRx.Operators.AsObservableObservable`1/AsObservable<UniRx.Unit>::OnNext(T)
extern "C"  void AsObservable_OnNext_m3383118613_gshared (AsObservable_t1923242440 * __this, Unit_t2558286038  ___value0, const MethodInfo* method);
#define AsObservable_OnNext_m3383118613(__this, ___value0, method) ((  void (*) (AsObservable_t1923242440 *, Unit_t2558286038 , const MethodInfo*))AsObservable_OnNext_m3383118613_gshared)(__this, ___value0, method)
// System.Void UniRx.Operators.AsObservableObservable`1/AsObservable<UniRx.Unit>::OnError(System.Exception)
extern "C"  void AsObservable_OnError_m182688804_gshared (AsObservable_t1923242440 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method);
#define AsObservable_OnError_m182688804(__this, ___error0, method) ((  void (*) (AsObservable_t1923242440 *, Exception_t1967233988 *, const MethodInfo*))AsObservable_OnError_m182688804_gshared)(__this, ___error0, method)
// System.Void UniRx.Operators.AsObservableObservable`1/AsObservable<UniRx.Unit>::OnCompleted()
extern "C"  void AsObservable_OnCompleted_m4257244535_gshared (AsObservable_t1923242440 * __this, const MethodInfo* method);
#define AsObservable_OnCompleted_m4257244535(__this, method) ((  void (*) (AsObservable_t1923242440 *, const MethodInfo*))AsObservable_OnCompleted_m4257244535_gshared)(__this, method)
