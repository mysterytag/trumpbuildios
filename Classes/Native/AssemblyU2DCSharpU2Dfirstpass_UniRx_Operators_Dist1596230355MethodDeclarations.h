﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.DistinctUntilChangedObservable`2<System.Object,System.Object>
struct DistinctUntilChangedObservable_2_t1596230355;
// UniRx.IObservable`1<System.Object>
struct IObservable_1_t595904784;
// System.Func`2<System.Object,System.Object>
struct Func_2_t2135783352;
// System.Collections.Generic.IEqualityComparer`1<System.Object>
struct IEqualityComparer_1_t3161373071;
// System.IDisposable
struct IDisposable_t1628921374;
// UniRx.IObserver`1<System.Object>
struct IObserver_1_t3049105323;

#include "codegen/il2cpp-codegen.h"

// System.Void UniRx.Operators.DistinctUntilChangedObservable`2<System.Object,System.Object>::.ctor(UniRx.IObservable`1<T>,System.Func`2<T,TKey>,System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C"  void DistinctUntilChangedObservable_2__ctor_m4202592059_gshared (DistinctUntilChangedObservable_2_t1596230355 * __this, Il2CppObject* ___source0, Func_2_t2135783352 * ___keySelector1, Il2CppObject* ___comparer2, const MethodInfo* method);
#define DistinctUntilChangedObservable_2__ctor_m4202592059(__this, ___source0, ___keySelector1, ___comparer2, method) ((  void (*) (DistinctUntilChangedObservable_2_t1596230355 *, Il2CppObject*, Func_2_t2135783352 *, Il2CppObject*, const MethodInfo*))DistinctUntilChangedObservable_2__ctor_m4202592059_gshared)(__this, ___source0, ___keySelector1, ___comparer2, method)
// System.IDisposable UniRx.Operators.DistinctUntilChangedObservable`2<System.Object,System.Object>::SubscribeCore(UniRx.IObserver`1<T>,System.IDisposable)
extern "C"  Il2CppObject * DistinctUntilChangedObservable_2_SubscribeCore_m3372304221_gshared (DistinctUntilChangedObservable_2_t1596230355 * __this, Il2CppObject* ___observer0, Il2CppObject * ___cancel1, const MethodInfo* method);
#define DistinctUntilChangedObservable_2_SubscribeCore_m3372304221(__this, ___observer0, ___cancel1, method) ((  Il2CppObject * (*) (DistinctUntilChangedObservable_2_t1596230355 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))DistinctUntilChangedObservable_2_SubscribeCore_m3372304221_gshared)(__this, ___observer0, ___cancel1, method)
