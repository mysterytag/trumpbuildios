﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Subject`1<UniRx.Unit>
struct Subject_1_t201353362;
// System.Exception
struct Exception_t1967233988;
// System.IDisposable
struct IDisposable_t1628921374;
// UniRx.IObserver`1<UniRx.Unit>
struct IObserver_1_t475317645;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Exception1967233988.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Unit2558286038.h"

// System.Void UniRx.Subject`1<UniRx.Unit>::.ctor()
extern "C"  void Subject_1__ctor_m55998028_gshared (Subject_1_t201353362 * __this, const MethodInfo* method);
#define Subject_1__ctor_m55998028(__this, method) ((  void (*) (Subject_1_t201353362 *, const MethodInfo*))Subject_1__ctor_m55998028_gshared)(__this, method)
// System.Boolean UniRx.Subject`1<UniRx.Unit>::get_HasObservers()
extern "C"  bool Subject_1_get_HasObservers_m689559560_gshared (Subject_1_t201353362 * __this, const MethodInfo* method);
#define Subject_1_get_HasObservers_m689559560(__this, method) ((  bool (*) (Subject_1_t201353362 *, const MethodInfo*))Subject_1_get_HasObservers_m689559560_gshared)(__this, method)
// System.Void UniRx.Subject`1<UniRx.Unit>::OnCompleted()
extern "C"  void Subject_1_OnCompleted_m4016750806_gshared (Subject_1_t201353362 * __this, const MethodInfo* method);
#define Subject_1_OnCompleted_m4016750806(__this, method) ((  void (*) (Subject_1_t201353362 *, const MethodInfo*))Subject_1_OnCompleted_m4016750806_gshared)(__this, method)
// System.Void UniRx.Subject`1<UniRx.Unit>::OnError(System.Exception)
extern "C"  void Subject_1_OnError_m3816612099_gshared (Subject_1_t201353362 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method);
#define Subject_1_OnError_m3816612099(__this, ___error0, method) ((  void (*) (Subject_1_t201353362 *, Exception_t1967233988 *, const MethodInfo*))Subject_1_OnError_m3816612099_gshared)(__this, ___error0, method)
// System.Void UniRx.Subject`1<UniRx.Unit>::OnNext(T)
extern "C"  void Subject_1_OnNext_m810233332_gshared (Subject_1_t201353362 * __this, Unit_t2558286038  ___value0, const MethodInfo* method);
#define Subject_1_OnNext_m810233332(__this, ___value0, method) ((  void (*) (Subject_1_t201353362 *, Unit_t2558286038 , const MethodInfo*))Subject_1_OnNext_m810233332_gshared)(__this, ___value0, method)
// System.IDisposable UniRx.Subject`1<UniRx.Unit>::Subscribe(UniRx.IObserver`1<T>)
extern "C"  Il2CppObject * Subject_1_Subscribe_m4005448609_gshared (Subject_1_t201353362 * __this, Il2CppObject* ___observer0, const MethodInfo* method);
#define Subject_1_Subscribe_m4005448609(__this, ___observer0, method) ((  Il2CppObject * (*) (Subject_1_t201353362 *, Il2CppObject*, const MethodInfo*))Subject_1_Subscribe_m4005448609_gshared)(__this, ___observer0, method)
// System.Void UniRx.Subject`1<UniRx.Unit>::Dispose()
extern "C"  void Subject_1_Dispose_m2172780425_gshared (Subject_1_t201353362 * __this, const MethodInfo* method);
#define Subject_1_Dispose_m2172780425(__this, method) ((  void (*) (Subject_1_t201353362 *, const MethodInfo*))Subject_1_Dispose_m2172780425_gshared)(__this, method)
// System.Void UniRx.Subject`1<UniRx.Unit>::ThrowIfDisposed()
extern "C"  void Subject_1_ThrowIfDisposed_m47190034_gshared (Subject_1_t201353362 * __this, const MethodInfo* method);
#define Subject_1_ThrowIfDisposed_m47190034(__this, method) ((  void (*) (Subject_1_t201353362 *, const MethodInfo*))Subject_1_ThrowIfDisposed_m47190034_gshared)(__this, method)
// System.Boolean UniRx.Subject`1<UniRx.Unit>::IsRequiredSubscribeOnCurrentThread()
extern "C"  bool Subject_1_IsRequiredSubscribeOnCurrentThread_m2039698175_gshared (Subject_1_t201353362 * __this, const MethodInfo* method);
#define Subject_1_IsRequiredSubscribeOnCurrentThread_m2039698175(__this, method) ((  bool (*) (Subject_1_t201353362 *, const MethodInfo*))Subject_1_IsRequiredSubscribeOnCurrentThread_m2039698175_gshared)(__this, method)
