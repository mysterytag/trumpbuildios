﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// System.String[]
struct StringU5BU5D_t2956870243;
// System.Int32[]
struct Int32U5BU5D_t1809983122;
// System.Collections.Generic.List`1<GameAnalyticsSDK.Studio>
struct List_1_t2889281553;
// System.Collections.Generic.List`1<System.String>
struct List_1_t1765447871;
// System.Collections.Generic.List`1<GameAnalyticsSDK.Settings/HelpTypes>
struct List_1_t4088314513;
// UnityEngine.Texture2D
struct Texture2D_t2509538522;
// UnityEngine.GUIStyle
struct GUIStyle_t1006925219;
// System.Boolean[]
struct BooleanU5BU5D_t3804927312;

#include "UnityEngine_UnityEngine_ScriptableObject184905905.h"
#include "AssemblyU2DCSharp_GameAnalyticsSDK_GAPlatform3251704975.h"
#include "AssemblyU2DCSharp_GameAnalyticsSDK_Settings_Inspec3262207929.h"
#include "UnityEngine_UnityEngine_Vector23525329788.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameAnalyticsSDK.Settings
struct  Settings_t1104664933  : public ScriptableObject_t184905905
{
public:
	// System.Int32 GameAnalyticsSDK.Settings::TotalMessagesSubmitted
	int32_t ___TotalMessagesSubmitted_4;
	// System.Int32 GameAnalyticsSDK.Settings::TotalMessagesFailed
	int32_t ___TotalMessagesFailed_5;
	// System.Int32 GameAnalyticsSDK.Settings::DesignMessagesSubmitted
	int32_t ___DesignMessagesSubmitted_6;
	// System.Int32 GameAnalyticsSDK.Settings::DesignMessagesFailed
	int32_t ___DesignMessagesFailed_7;
	// System.Int32 GameAnalyticsSDK.Settings::QualityMessagesSubmitted
	int32_t ___QualityMessagesSubmitted_8;
	// System.Int32 GameAnalyticsSDK.Settings::QualityMessagesFailed
	int32_t ___QualityMessagesFailed_9;
	// System.Int32 GameAnalyticsSDK.Settings::ErrorMessagesSubmitted
	int32_t ___ErrorMessagesSubmitted_10;
	// System.Int32 GameAnalyticsSDK.Settings::ErrorMessagesFailed
	int32_t ___ErrorMessagesFailed_11;
	// System.Int32 GameAnalyticsSDK.Settings::BusinessMessagesSubmitted
	int32_t ___BusinessMessagesSubmitted_12;
	// System.Int32 GameAnalyticsSDK.Settings::BusinessMessagesFailed
	int32_t ___BusinessMessagesFailed_13;
	// System.Int32 GameAnalyticsSDK.Settings::UserMessagesSubmitted
	int32_t ___UserMessagesSubmitted_14;
	// System.Int32 GameAnalyticsSDK.Settings::UserMessagesFailed
	int32_t ___UserMessagesFailed_15;
	// System.String GameAnalyticsSDK.Settings::CustomArea
	String_t* ___CustomArea_16;
	// System.String[] GameAnalyticsSDK.Settings::gameKey
	StringU5BU5D_t2956870243* ___gameKey_17;
	// System.String[] GameAnalyticsSDK.Settings::secretKey
	StringU5BU5D_t2956870243* ___secretKey_18;
	// System.String[] GameAnalyticsSDK.Settings::Build
	StringU5BU5D_t2956870243* ___Build_19;
	// System.String[] GameAnalyticsSDK.Settings::SelectedPlatformStudio
	StringU5BU5D_t2956870243* ___SelectedPlatformStudio_20;
	// System.String[] GameAnalyticsSDK.Settings::SelectedPlatformGame
	StringU5BU5D_t2956870243* ___SelectedPlatformGame_21;
	// System.Int32[] GameAnalyticsSDK.Settings::SelectedPlatformGameID
	Int32U5BU5D_t1809983122* ___SelectedPlatformGameID_22;
	// System.Int32[] GameAnalyticsSDK.Settings::SelectedStudio
	Int32U5BU5D_t1809983122* ___SelectedStudio_23;
	// System.Int32[] GameAnalyticsSDK.Settings::SelectedGame
	Int32U5BU5D_t1809983122* ___SelectedGame_24;
	// System.String GameAnalyticsSDK.Settings::NewVersion
	String_t* ___NewVersion_25;
	// System.String GameAnalyticsSDK.Settings::Changes
	String_t* ___Changes_26;
	// System.Boolean GameAnalyticsSDK.Settings::SignUpOpen
	bool ___SignUpOpen_27;
	// System.String GameAnalyticsSDK.Settings::FirstName
	String_t* ___FirstName_28;
	// System.String GameAnalyticsSDK.Settings::LastName
	String_t* ___LastName_29;
	// System.String GameAnalyticsSDK.Settings::StudioName
	String_t* ___StudioName_30;
	// System.String GameAnalyticsSDK.Settings::GameName
	String_t* ___GameName_31;
	// System.String GameAnalyticsSDK.Settings::PasswordConfirm
	String_t* ___PasswordConfirm_32;
	// System.Boolean GameAnalyticsSDK.Settings::EmailOptIn
	bool ___EmailOptIn_33;
	// System.String GameAnalyticsSDK.Settings::EmailGA
	String_t* ___EmailGA_34;
	// System.String GameAnalyticsSDK.Settings::PasswordGA
	String_t* ___PasswordGA_35;
	// System.String GameAnalyticsSDK.Settings::TokenGA
	String_t* ___TokenGA_36;
	// System.String GameAnalyticsSDK.Settings::ExpireTime
	String_t* ___ExpireTime_37;
	// System.String GameAnalyticsSDK.Settings::LoginStatus
	String_t* ___LoginStatus_38;
	// System.Boolean GameAnalyticsSDK.Settings::JustSignedUp
	bool ___JustSignedUp_39;
	// System.Boolean GameAnalyticsSDK.Settings::HideSignupWarning
	bool ___HideSignupWarning_40;
	// System.Boolean GameAnalyticsSDK.Settings::IntroScreen
	bool ___IntroScreen_41;
	// System.Collections.Generic.List`1<GameAnalyticsSDK.Studio> GameAnalyticsSDK.Settings::Studios
	List_1_t2889281553 * ___Studios_42;
	// System.Boolean GameAnalyticsSDK.Settings::InfoLogEditor
	bool ___InfoLogEditor_43;
	// System.Boolean GameAnalyticsSDK.Settings::InfoLogBuild
	bool ___InfoLogBuild_44;
	// System.Boolean GameAnalyticsSDK.Settings::VerboseLogBuild
	bool ___VerboseLogBuild_45;
	// System.Boolean GameAnalyticsSDK.Settings::SendExampleGameDataToMyGame
	bool ___SendExampleGameDataToMyGame_46;
	// System.Boolean GameAnalyticsSDK.Settings::InternetConnectivity
	bool ___InternetConnectivity_47;
	// System.Collections.Generic.List`1<System.String> GameAnalyticsSDK.Settings::CustomDimensions01
	List_1_t1765447871 * ___CustomDimensions01_48;
	// System.Collections.Generic.List`1<System.String> GameAnalyticsSDK.Settings::CustomDimensions02
	List_1_t1765447871 * ___CustomDimensions02_49;
	// System.Collections.Generic.List`1<System.String> GameAnalyticsSDK.Settings::CustomDimensions03
	List_1_t1765447871 * ___CustomDimensions03_50;
	// System.Collections.Generic.List`1<System.String> GameAnalyticsSDK.Settings::ResourceItemTypes
	List_1_t1765447871 * ___ResourceItemTypes_51;
	// System.Collections.Generic.List`1<System.String> GameAnalyticsSDK.Settings::ResourceCurrencies
	List_1_t1765447871 * ___ResourceCurrencies_52;
	// GameAnalyticsSDK.GAPlatform GameAnalyticsSDK.Settings::LastCreatedGamePlatform
	int32_t ___LastCreatedGamePlatform_53;
	// GameAnalyticsSDK.Settings/InspectorStates GameAnalyticsSDK.Settings::CurrentInspectorState
	int32_t ___CurrentInspectorState_54;
	// System.Collections.Generic.List`1<GameAnalyticsSDK.Settings/HelpTypes> GameAnalyticsSDK.Settings::ClosedHints
	List_1_t4088314513 * ___ClosedHints_55;
	// System.Boolean GameAnalyticsSDK.Settings::DisplayHints
	bool ___DisplayHints_56;
	// UnityEngine.Vector2 GameAnalyticsSDK.Settings::DisplayHintsScrollState
	Vector2_t3525329788  ___DisplayHintsScrollState_57;
	// UnityEngine.Texture2D GameAnalyticsSDK.Settings::Logo
	Texture2D_t2509538522 * ___Logo_58;
	// UnityEngine.Texture2D GameAnalyticsSDK.Settings::UpdateIcon
	Texture2D_t2509538522 * ___UpdateIcon_59;
	// UnityEngine.Texture2D GameAnalyticsSDK.Settings::InfoIcon
	Texture2D_t2509538522 * ___InfoIcon_60;
	// UnityEngine.Texture2D GameAnalyticsSDK.Settings::DeleteIcon
	Texture2D_t2509538522 * ___DeleteIcon_61;
	// UnityEngine.Texture2D GameAnalyticsSDK.Settings::GameIcon
	Texture2D_t2509538522 * ___GameIcon_62;
	// UnityEngine.Texture2D GameAnalyticsSDK.Settings::HomeIcon
	Texture2D_t2509538522 * ___HomeIcon_63;
	// UnityEngine.Texture2D GameAnalyticsSDK.Settings::InstrumentIcon
	Texture2D_t2509538522 * ___InstrumentIcon_64;
	// UnityEngine.Texture2D GameAnalyticsSDK.Settings::QuestionIcon
	Texture2D_t2509538522 * ___QuestionIcon_65;
	// UnityEngine.Texture2D GameAnalyticsSDK.Settings::UserIcon
	Texture2D_t2509538522 * ___UserIcon_66;
	// UnityEngine.Texture2D GameAnalyticsSDK.Settings::AmazonIcon
	Texture2D_t2509538522 * ___AmazonIcon_67;
	// UnityEngine.Texture2D GameAnalyticsSDK.Settings::GooglePlayIcon
	Texture2D_t2509538522 * ___GooglePlayIcon_68;
	// UnityEngine.Texture2D GameAnalyticsSDK.Settings::iosIcon
	Texture2D_t2509538522 * ___iosIcon_69;
	// UnityEngine.Texture2D GameAnalyticsSDK.Settings::macIcon
	Texture2D_t2509538522 * ___macIcon_70;
	// UnityEngine.Texture2D GameAnalyticsSDK.Settings::windowsPhoneIcon
	Texture2D_t2509538522 * ___windowsPhoneIcon_71;
	// UnityEngine.GUIStyle GameAnalyticsSDK.Settings::SignupButton
	GUIStyle_t1006925219 * ___SignupButton_72;
	// System.Boolean GameAnalyticsSDK.Settings::SubmitErrors
	bool ___SubmitErrors_73;
	// System.Int32 GameAnalyticsSDK.Settings::MaxErrorCount
	int32_t ___MaxErrorCount_74;
	// System.Boolean GameAnalyticsSDK.Settings::SubmitFpsAverage
	bool ___SubmitFpsAverage_75;
	// System.Boolean GameAnalyticsSDK.Settings::SubmitFpsCritical
	bool ___SubmitFpsCritical_76;
	// System.Int32 GameAnalyticsSDK.Settings::FpsCriticalThreshold
	int32_t ___FpsCriticalThreshold_77;
	// System.Int32 GameAnalyticsSDK.Settings::FpsCirticalSubmitInterval
	int32_t ___FpsCirticalSubmitInterval_78;
	// System.Boolean[] GameAnalyticsSDK.Settings::PlatformFoldOut
	BooleanU5BU5D_t3804927312* ___PlatformFoldOut_79;
	// System.Boolean GameAnalyticsSDK.Settings::CustomDimensions01FoldOut
	bool ___CustomDimensions01FoldOut_80;
	// System.Boolean GameAnalyticsSDK.Settings::CustomDimensions02FoldOut
	bool ___CustomDimensions02FoldOut_81;
	// System.Boolean GameAnalyticsSDK.Settings::CustomDimensions03FoldOut
	bool ___CustomDimensions03FoldOut_82;
	// System.Boolean GameAnalyticsSDK.Settings::ResourceItemTypesFoldOut
	bool ___ResourceItemTypesFoldOut_83;
	// System.Boolean GameAnalyticsSDK.Settings::ResourceCurrenciesFoldOut
	bool ___ResourceCurrenciesFoldOut_84;

public:
	inline static int32_t get_offset_of_TotalMessagesSubmitted_4() { return static_cast<int32_t>(offsetof(Settings_t1104664933, ___TotalMessagesSubmitted_4)); }
	inline int32_t get_TotalMessagesSubmitted_4() const { return ___TotalMessagesSubmitted_4; }
	inline int32_t* get_address_of_TotalMessagesSubmitted_4() { return &___TotalMessagesSubmitted_4; }
	inline void set_TotalMessagesSubmitted_4(int32_t value)
	{
		___TotalMessagesSubmitted_4 = value;
	}

	inline static int32_t get_offset_of_TotalMessagesFailed_5() { return static_cast<int32_t>(offsetof(Settings_t1104664933, ___TotalMessagesFailed_5)); }
	inline int32_t get_TotalMessagesFailed_5() const { return ___TotalMessagesFailed_5; }
	inline int32_t* get_address_of_TotalMessagesFailed_5() { return &___TotalMessagesFailed_5; }
	inline void set_TotalMessagesFailed_5(int32_t value)
	{
		___TotalMessagesFailed_5 = value;
	}

	inline static int32_t get_offset_of_DesignMessagesSubmitted_6() { return static_cast<int32_t>(offsetof(Settings_t1104664933, ___DesignMessagesSubmitted_6)); }
	inline int32_t get_DesignMessagesSubmitted_6() const { return ___DesignMessagesSubmitted_6; }
	inline int32_t* get_address_of_DesignMessagesSubmitted_6() { return &___DesignMessagesSubmitted_6; }
	inline void set_DesignMessagesSubmitted_6(int32_t value)
	{
		___DesignMessagesSubmitted_6 = value;
	}

	inline static int32_t get_offset_of_DesignMessagesFailed_7() { return static_cast<int32_t>(offsetof(Settings_t1104664933, ___DesignMessagesFailed_7)); }
	inline int32_t get_DesignMessagesFailed_7() const { return ___DesignMessagesFailed_7; }
	inline int32_t* get_address_of_DesignMessagesFailed_7() { return &___DesignMessagesFailed_7; }
	inline void set_DesignMessagesFailed_7(int32_t value)
	{
		___DesignMessagesFailed_7 = value;
	}

	inline static int32_t get_offset_of_QualityMessagesSubmitted_8() { return static_cast<int32_t>(offsetof(Settings_t1104664933, ___QualityMessagesSubmitted_8)); }
	inline int32_t get_QualityMessagesSubmitted_8() const { return ___QualityMessagesSubmitted_8; }
	inline int32_t* get_address_of_QualityMessagesSubmitted_8() { return &___QualityMessagesSubmitted_8; }
	inline void set_QualityMessagesSubmitted_8(int32_t value)
	{
		___QualityMessagesSubmitted_8 = value;
	}

	inline static int32_t get_offset_of_QualityMessagesFailed_9() { return static_cast<int32_t>(offsetof(Settings_t1104664933, ___QualityMessagesFailed_9)); }
	inline int32_t get_QualityMessagesFailed_9() const { return ___QualityMessagesFailed_9; }
	inline int32_t* get_address_of_QualityMessagesFailed_9() { return &___QualityMessagesFailed_9; }
	inline void set_QualityMessagesFailed_9(int32_t value)
	{
		___QualityMessagesFailed_9 = value;
	}

	inline static int32_t get_offset_of_ErrorMessagesSubmitted_10() { return static_cast<int32_t>(offsetof(Settings_t1104664933, ___ErrorMessagesSubmitted_10)); }
	inline int32_t get_ErrorMessagesSubmitted_10() const { return ___ErrorMessagesSubmitted_10; }
	inline int32_t* get_address_of_ErrorMessagesSubmitted_10() { return &___ErrorMessagesSubmitted_10; }
	inline void set_ErrorMessagesSubmitted_10(int32_t value)
	{
		___ErrorMessagesSubmitted_10 = value;
	}

	inline static int32_t get_offset_of_ErrorMessagesFailed_11() { return static_cast<int32_t>(offsetof(Settings_t1104664933, ___ErrorMessagesFailed_11)); }
	inline int32_t get_ErrorMessagesFailed_11() const { return ___ErrorMessagesFailed_11; }
	inline int32_t* get_address_of_ErrorMessagesFailed_11() { return &___ErrorMessagesFailed_11; }
	inline void set_ErrorMessagesFailed_11(int32_t value)
	{
		___ErrorMessagesFailed_11 = value;
	}

	inline static int32_t get_offset_of_BusinessMessagesSubmitted_12() { return static_cast<int32_t>(offsetof(Settings_t1104664933, ___BusinessMessagesSubmitted_12)); }
	inline int32_t get_BusinessMessagesSubmitted_12() const { return ___BusinessMessagesSubmitted_12; }
	inline int32_t* get_address_of_BusinessMessagesSubmitted_12() { return &___BusinessMessagesSubmitted_12; }
	inline void set_BusinessMessagesSubmitted_12(int32_t value)
	{
		___BusinessMessagesSubmitted_12 = value;
	}

	inline static int32_t get_offset_of_BusinessMessagesFailed_13() { return static_cast<int32_t>(offsetof(Settings_t1104664933, ___BusinessMessagesFailed_13)); }
	inline int32_t get_BusinessMessagesFailed_13() const { return ___BusinessMessagesFailed_13; }
	inline int32_t* get_address_of_BusinessMessagesFailed_13() { return &___BusinessMessagesFailed_13; }
	inline void set_BusinessMessagesFailed_13(int32_t value)
	{
		___BusinessMessagesFailed_13 = value;
	}

	inline static int32_t get_offset_of_UserMessagesSubmitted_14() { return static_cast<int32_t>(offsetof(Settings_t1104664933, ___UserMessagesSubmitted_14)); }
	inline int32_t get_UserMessagesSubmitted_14() const { return ___UserMessagesSubmitted_14; }
	inline int32_t* get_address_of_UserMessagesSubmitted_14() { return &___UserMessagesSubmitted_14; }
	inline void set_UserMessagesSubmitted_14(int32_t value)
	{
		___UserMessagesSubmitted_14 = value;
	}

	inline static int32_t get_offset_of_UserMessagesFailed_15() { return static_cast<int32_t>(offsetof(Settings_t1104664933, ___UserMessagesFailed_15)); }
	inline int32_t get_UserMessagesFailed_15() const { return ___UserMessagesFailed_15; }
	inline int32_t* get_address_of_UserMessagesFailed_15() { return &___UserMessagesFailed_15; }
	inline void set_UserMessagesFailed_15(int32_t value)
	{
		___UserMessagesFailed_15 = value;
	}

	inline static int32_t get_offset_of_CustomArea_16() { return static_cast<int32_t>(offsetof(Settings_t1104664933, ___CustomArea_16)); }
	inline String_t* get_CustomArea_16() const { return ___CustomArea_16; }
	inline String_t** get_address_of_CustomArea_16() { return &___CustomArea_16; }
	inline void set_CustomArea_16(String_t* value)
	{
		___CustomArea_16 = value;
		Il2CppCodeGenWriteBarrier(&___CustomArea_16, value);
	}

	inline static int32_t get_offset_of_gameKey_17() { return static_cast<int32_t>(offsetof(Settings_t1104664933, ___gameKey_17)); }
	inline StringU5BU5D_t2956870243* get_gameKey_17() const { return ___gameKey_17; }
	inline StringU5BU5D_t2956870243** get_address_of_gameKey_17() { return &___gameKey_17; }
	inline void set_gameKey_17(StringU5BU5D_t2956870243* value)
	{
		___gameKey_17 = value;
		Il2CppCodeGenWriteBarrier(&___gameKey_17, value);
	}

	inline static int32_t get_offset_of_secretKey_18() { return static_cast<int32_t>(offsetof(Settings_t1104664933, ___secretKey_18)); }
	inline StringU5BU5D_t2956870243* get_secretKey_18() const { return ___secretKey_18; }
	inline StringU5BU5D_t2956870243** get_address_of_secretKey_18() { return &___secretKey_18; }
	inline void set_secretKey_18(StringU5BU5D_t2956870243* value)
	{
		___secretKey_18 = value;
		Il2CppCodeGenWriteBarrier(&___secretKey_18, value);
	}

	inline static int32_t get_offset_of_Build_19() { return static_cast<int32_t>(offsetof(Settings_t1104664933, ___Build_19)); }
	inline StringU5BU5D_t2956870243* get_Build_19() const { return ___Build_19; }
	inline StringU5BU5D_t2956870243** get_address_of_Build_19() { return &___Build_19; }
	inline void set_Build_19(StringU5BU5D_t2956870243* value)
	{
		___Build_19 = value;
		Il2CppCodeGenWriteBarrier(&___Build_19, value);
	}

	inline static int32_t get_offset_of_SelectedPlatformStudio_20() { return static_cast<int32_t>(offsetof(Settings_t1104664933, ___SelectedPlatformStudio_20)); }
	inline StringU5BU5D_t2956870243* get_SelectedPlatformStudio_20() const { return ___SelectedPlatformStudio_20; }
	inline StringU5BU5D_t2956870243** get_address_of_SelectedPlatformStudio_20() { return &___SelectedPlatformStudio_20; }
	inline void set_SelectedPlatformStudio_20(StringU5BU5D_t2956870243* value)
	{
		___SelectedPlatformStudio_20 = value;
		Il2CppCodeGenWriteBarrier(&___SelectedPlatformStudio_20, value);
	}

	inline static int32_t get_offset_of_SelectedPlatformGame_21() { return static_cast<int32_t>(offsetof(Settings_t1104664933, ___SelectedPlatformGame_21)); }
	inline StringU5BU5D_t2956870243* get_SelectedPlatformGame_21() const { return ___SelectedPlatformGame_21; }
	inline StringU5BU5D_t2956870243** get_address_of_SelectedPlatformGame_21() { return &___SelectedPlatformGame_21; }
	inline void set_SelectedPlatformGame_21(StringU5BU5D_t2956870243* value)
	{
		___SelectedPlatformGame_21 = value;
		Il2CppCodeGenWriteBarrier(&___SelectedPlatformGame_21, value);
	}

	inline static int32_t get_offset_of_SelectedPlatformGameID_22() { return static_cast<int32_t>(offsetof(Settings_t1104664933, ___SelectedPlatformGameID_22)); }
	inline Int32U5BU5D_t1809983122* get_SelectedPlatformGameID_22() const { return ___SelectedPlatformGameID_22; }
	inline Int32U5BU5D_t1809983122** get_address_of_SelectedPlatformGameID_22() { return &___SelectedPlatformGameID_22; }
	inline void set_SelectedPlatformGameID_22(Int32U5BU5D_t1809983122* value)
	{
		___SelectedPlatformGameID_22 = value;
		Il2CppCodeGenWriteBarrier(&___SelectedPlatformGameID_22, value);
	}

	inline static int32_t get_offset_of_SelectedStudio_23() { return static_cast<int32_t>(offsetof(Settings_t1104664933, ___SelectedStudio_23)); }
	inline Int32U5BU5D_t1809983122* get_SelectedStudio_23() const { return ___SelectedStudio_23; }
	inline Int32U5BU5D_t1809983122** get_address_of_SelectedStudio_23() { return &___SelectedStudio_23; }
	inline void set_SelectedStudio_23(Int32U5BU5D_t1809983122* value)
	{
		___SelectedStudio_23 = value;
		Il2CppCodeGenWriteBarrier(&___SelectedStudio_23, value);
	}

	inline static int32_t get_offset_of_SelectedGame_24() { return static_cast<int32_t>(offsetof(Settings_t1104664933, ___SelectedGame_24)); }
	inline Int32U5BU5D_t1809983122* get_SelectedGame_24() const { return ___SelectedGame_24; }
	inline Int32U5BU5D_t1809983122** get_address_of_SelectedGame_24() { return &___SelectedGame_24; }
	inline void set_SelectedGame_24(Int32U5BU5D_t1809983122* value)
	{
		___SelectedGame_24 = value;
		Il2CppCodeGenWriteBarrier(&___SelectedGame_24, value);
	}

	inline static int32_t get_offset_of_NewVersion_25() { return static_cast<int32_t>(offsetof(Settings_t1104664933, ___NewVersion_25)); }
	inline String_t* get_NewVersion_25() const { return ___NewVersion_25; }
	inline String_t** get_address_of_NewVersion_25() { return &___NewVersion_25; }
	inline void set_NewVersion_25(String_t* value)
	{
		___NewVersion_25 = value;
		Il2CppCodeGenWriteBarrier(&___NewVersion_25, value);
	}

	inline static int32_t get_offset_of_Changes_26() { return static_cast<int32_t>(offsetof(Settings_t1104664933, ___Changes_26)); }
	inline String_t* get_Changes_26() const { return ___Changes_26; }
	inline String_t** get_address_of_Changes_26() { return &___Changes_26; }
	inline void set_Changes_26(String_t* value)
	{
		___Changes_26 = value;
		Il2CppCodeGenWriteBarrier(&___Changes_26, value);
	}

	inline static int32_t get_offset_of_SignUpOpen_27() { return static_cast<int32_t>(offsetof(Settings_t1104664933, ___SignUpOpen_27)); }
	inline bool get_SignUpOpen_27() const { return ___SignUpOpen_27; }
	inline bool* get_address_of_SignUpOpen_27() { return &___SignUpOpen_27; }
	inline void set_SignUpOpen_27(bool value)
	{
		___SignUpOpen_27 = value;
	}

	inline static int32_t get_offset_of_FirstName_28() { return static_cast<int32_t>(offsetof(Settings_t1104664933, ___FirstName_28)); }
	inline String_t* get_FirstName_28() const { return ___FirstName_28; }
	inline String_t** get_address_of_FirstName_28() { return &___FirstName_28; }
	inline void set_FirstName_28(String_t* value)
	{
		___FirstName_28 = value;
		Il2CppCodeGenWriteBarrier(&___FirstName_28, value);
	}

	inline static int32_t get_offset_of_LastName_29() { return static_cast<int32_t>(offsetof(Settings_t1104664933, ___LastName_29)); }
	inline String_t* get_LastName_29() const { return ___LastName_29; }
	inline String_t** get_address_of_LastName_29() { return &___LastName_29; }
	inline void set_LastName_29(String_t* value)
	{
		___LastName_29 = value;
		Il2CppCodeGenWriteBarrier(&___LastName_29, value);
	}

	inline static int32_t get_offset_of_StudioName_30() { return static_cast<int32_t>(offsetof(Settings_t1104664933, ___StudioName_30)); }
	inline String_t* get_StudioName_30() const { return ___StudioName_30; }
	inline String_t** get_address_of_StudioName_30() { return &___StudioName_30; }
	inline void set_StudioName_30(String_t* value)
	{
		___StudioName_30 = value;
		Il2CppCodeGenWriteBarrier(&___StudioName_30, value);
	}

	inline static int32_t get_offset_of_GameName_31() { return static_cast<int32_t>(offsetof(Settings_t1104664933, ___GameName_31)); }
	inline String_t* get_GameName_31() const { return ___GameName_31; }
	inline String_t** get_address_of_GameName_31() { return &___GameName_31; }
	inline void set_GameName_31(String_t* value)
	{
		___GameName_31 = value;
		Il2CppCodeGenWriteBarrier(&___GameName_31, value);
	}

	inline static int32_t get_offset_of_PasswordConfirm_32() { return static_cast<int32_t>(offsetof(Settings_t1104664933, ___PasswordConfirm_32)); }
	inline String_t* get_PasswordConfirm_32() const { return ___PasswordConfirm_32; }
	inline String_t** get_address_of_PasswordConfirm_32() { return &___PasswordConfirm_32; }
	inline void set_PasswordConfirm_32(String_t* value)
	{
		___PasswordConfirm_32 = value;
		Il2CppCodeGenWriteBarrier(&___PasswordConfirm_32, value);
	}

	inline static int32_t get_offset_of_EmailOptIn_33() { return static_cast<int32_t>(offsetof(Settings_t1104664933, ___EmailOptIn_33)); }
	inline bool get_EmailOptIn_33() const { return ___EmailOptIn_33; }
	inline bool* get_address_of_EmailOptIn_33() { return &___EmailOptIn_33; }
	inline void set_EmailOptIn_33(bool value)
	{
		___EmailOptIn_33 = value;
	}

	inline static int32_t get_offset_of_EmailGA_34() { return static_cast<int32_t>(offsetof(Settings_t1104664933, ___EmailGA_34)); }
	inline String_t* get_EmailGA_34() const { return ___EmailGA_34; }
	inline String_t** get_address_of_EmailGA_34() { return &___EmailGA_34; }
	inline void set_EmailGA_34(String_t* value)
	{
		___EmailGA_34 = value;
		Il2CppCodeGenWriteBarrier(&___EmailGA_34, value);
	}

	inline static int32_t get_offset_of_PasswordGA_35() { return static_cast<int32_t>(offsetof(Settings_t1104664933, ___PasswordGA_35)); }
	inline String_t* get_PasswordGA_35() const { return ___PasswordGA_35; }
	inline String_t** get_address_of_PasswordGA_35() { return &___PasswordGA_35; }
	inline void set_PasswordGA_35(String_t* value)
	{
		___PasswordGA_35 = value;
		Il2CppCodeGenWriteBarrier(&___PasswordGA_35, value);
	}

	inline static int32_t get_offset_of_TokenGA_36() { return static_cast<int32_t>(offsetof(Settings_t1104664933, ___TokenGA_36)); }
	inline String_t* get_TokenGA_36() const { return ___TokenGA_36; }
	inline String_t** get_address_of_TokenGA_36() { return &___TokenGA_36; }
	inline void set_TokenGA_36(String_t* value)
	{
		___TokenGA_36 = value;
		Il2CppCodeGenWriteBarrier(&___TokenGA_36, value);
	}

	inline static int32_t get_offset_of_ExpireTime_37() { return static_cast<int32_t>(offsetof(Settings_t1104664933, ___ExpireTime_37)); }
	inline String_t* get_ExpireTime_37() const { return ___ExpireTime_37; }
	inline String_t** get_address_of_ExpireTime_37() { return &___ExpireTime_37; }
	inline void set_ExpireTime_37(String_t* value)
	{
		___ExpireTime_37 = value;
		Il2CppCodeGenWriteBarrier(&___ExpireTime_37, value);
	}

	inline static int32_t get_offset_of_LoginStatus_38() { return static_cast<int32_t>(offsetof(Settings_t1104664933, ___LoginStatus_38)); }
	inline String_t* get_LoginStatus_38() const { return ___LoginStatus_38; }
	inline String_t** get_address_of_LoginStatus_38() { return &___LoginStatus_38; }
	inline void set_LoginStatus_38(String_t* value)
	{
		___LoginStatus_38 = value;
		Il2CppCodeGenWriteBarrier(&___LoginStatus_38, value);
	}

	inline static int32_t get_offset_of_JustSignedUp_39() { return static_cast<int32_t>(offsetof(Settings_t1104664933, ___JustSignedUp_39)); }
	inline bool get_JustSignedUp_39() const { return ___JustSignedUp_39; }
	inline bool* get_address_of_JustSignedUp_39() { return &___JustSignedUp_39; }
	inline void set_JustSignedUp_39(bool value)
	{
		___JustSignedUp_39 = value;
	}

	inline static int32_t get_offset_of_HideSignupWarning_40() { return static_cast<int32_t>(offsetof(Settings_t1104664933, ___HideSignupWarning_40)); }
	inline bool get_HideSignupWarning_40() const { return ___HideSignupWarning_40; }
	inline bool* get_address_of_HideSignupWarning_40() { return &___HideSignupWarning_40; }
	inline void set_HideSignupWarning_40(bool value)
	{
		___HideSignupWarning_40 = value;
	}

	inline static int32_t get_offset_of_IntroScreen_41() { return static_cast<int32_t>(offsetof(Settings_t1104664933, ___IntroScreen_41)); }
	inline bool get_IntroScreen_41() const { return ___IntroScreen_41; }
	inline bool* get_address_of_IntroScreen_41() { return &___IntroScreen_41; }
	inline void set_IntroScreen_41(bool value)
	{
		___IntroScreen_41 = value;
	}

	inline static int32_t get_offset_of_Studios_42() { return static_cast<int32_t>(offsetof(Settings_t1104664933, ___Studios_42)); }
	inline List_1_t2889281553 * get_Studios_42() const { return ___Studios_42; }
	inline List_1_t2889281553 ** get_address_of_Studios_42() { return &___Studios_42; }
	inline void set_Studios_42(List_1_t2889281553 * value)
	{
		___Studios_42 = value;
		Il2CppCodeGenWriteBarrier(&___Studios_42, value);
	}

	inline static int32_t get_offset_of_InfoLogEditor_43() { return static_cast<int32_t>(offsetof(Settings_t1104664933, ___InfoLogEditor_43)); }
	inline bool get_InfoLogEditor_43() const { return ___InfoLogEditor_43; }
	inline bool* get_address_of_InfoLogEditor_43() { return &___InfoLogEditor_43; }
	inline void set_InfoLogEditor_43(bool value)
	{
		___InfoLogEditor_43 = value;
	}

	inline static int32_t get_offset_of_InfoLogBuild_44() { return static_cast<int32_t>(offsetof(Settings_t1104664933, ___InfoLogBuild_44)); }
	inline bool get_InfoLogBuild_44() const { return ___InfoLogBuild_44; }
	inline bool* get_address_of_InfoLogBuild_44() { return &___InfoLogBuild_44; }
	inline void set_InfoLogBuild_44(bool value)
	{
		___InfoLogBuild_44 = value;
	}

	inline static int32_t get_offset_of_VerboseLogBuild_45() { return static_cast<int32_t>(offsetof(Settings_t1104664933, ___VerboseLogBuild_45)); }
	inline bool get_VerboseLogBuild_45() const { return ___VerboseLogBuild_45; }
	inline bool* get_address_of_VerboseLogBuild_45() { return &___VerboseLogBuild_45; }
	inline void set_VerboseLogBuild_45(bool value)
	{
		___VerboseLogBuild_45 = value;
	}

	inline static int32_t get_offset_of_SendExampleGameDataToMyGame_46() { return static_cast<int32_t>(offsetof(Settings_t1104664933, ___SendExampleGameDataToMyGame_46)); }
	inline bool get_SendExampleGameDataToMyGame_46() const { return ___SendExampleGameDataToMyGame_46; }
	inline bool* get_address_of_SendExampleGameDataToMyGame_46() { return &___SendExampleGameDataToMyGame_46; }
	inline void set_SendExampleGameDataToMyGame_46(bool value)
	{
		___SendExampleGameDataToMyGame_46 = value;
	}

	inline static int32_t get_offset_of_InternetConnectivity_47() { return static_cast<int32_t>(offsetof(Settings_t1104664933, ___InternetConnectivity_47)); }
	inline bool get_InternetConnectivity_47() const { return ___InternetConnectivity_47; }
	inline bool* get_address_of_InternetConnectivity_47() { return &___InternetConnectivity_47; }
	inline void set_InternetConnectivity_47(bool value)
	{
		___InternetConnectivity_47 = value;
	}

	inline static int32_t get_offset_of_CustomDimensions01_48() { return static_cast<int32_t>(offsetof(Settings_t1104664933, ___CustomDimensions01_48)); }
	inline List_1_t1765447871 * get_CustomDimensions01_48() const { return ___CustomDimensions01_48; }
	inline List_1_t1765447871 ** get_address_of_CustomDimensions01_48() { return &___CustomDimensions01_48; }
	inline void set_CustomDimensions01_48(List_1_t1765447871 * value)
	{
		___CustomDimensions01_48 = value;
		Il2CppCodeGenWriteBarrier(&___CustomDimensions01_48, value);
	}

	inline static int32_t get_offset_of_CustomDimensions02_49() { return static_cast<int32_t>(offsetof(Settings_t1104664933, ___CustomDimensions02_49)); }
	inline List_1_t1765447871 * get_CustomDimensions02_49() const { return ___CustomDimensions02_49; }
	inline List_1_t1765447871 ** get_address_of_CustomDimensions02_49() { return &___CustomDimensions02_49; }
	inline void set_CustomDimensions02_49(List_1_t1765447871 * value)
	{
		___CustomDimensions02_49 = value;
		Il2CppCodeGenWriteBarrier(&___CustomDimensions02_49, value);
	}

	inline static int32_t get_offset_of_CustomDimensions03_50() { return static_cast<int32_t>(offsetof(Settings_t1104664933, ___CustomDimensions03_50)); }
	inline List_1_t1765447871 * get_CustomDimensions03_50() const { return ___CustomDimensions03_50; }
	inline List_1_t1765447871 ** get_address_of_CustomDimensions03_50() { return &___CustomDimensions03_50; }
	inline void set_CustomDimensions03_50(List_1_t1765447871 * value)
	{
		___CustomDimensions03_50 = value;
		Il2CppCodeGenWriteBarrier(&___CustomDimensions03_50, value);
	}

	inline static int32_t get_offset_of_ResourceItemTypes_51() { return static_cast<int32_t>(offsetof(Settings_t1104664933, ___ResourceItemTypes_51)); }
	inline List_1_t1765447871 * get_ResourceItemTypes_51() const { return ___ResourceItemTypes_51; }
	inline List_1_t1765447871 ** get_address_of_ResourceItemTypes_51() { return &___ResourceItemTypes_51; }
	inline void set_ResourceItemTypes_51(List_1_t1765447871 * value)
	{
		___ResourceItemTypes_51 = value;
		Il2CppCodeGenWriteBarrier(&___ResourceItemTypes_51, value);
	}

	inline static int32_t get_offset_of_ResourceCurrencies_52() { return static_cast<int32_t>(offsetof(Settings_t1104664933, ___ResourceCurrencies_52)); }
	inline List_1_t1765447871 * get_ResourceCurrencies_52() const { return ___ResourceCurrencies_52; }
	inline List_1_t1765447871 ** get_address_of_ResourceCurrencies_52() { return &___ResourceCurrencies_52; }
	inline void set_ResourceCurrencies_52(List_1_t1765447871 * value)
	{
		___ResourceCurrencies_52 = value;
		Il2CppCodeGenWriteBarrier(&___ResourceCurrencies_52, value);
	}

	inline static int32_t get_offset_of_LastCreatedGamePlatform_53() { return static_cast<int32_t>(offsetof(Settings_t1104664933, ___LastCreatedGamePlatform_53)); }
	inline int32_t get_LastCreatedGamePlatform_53() const { return ___LastCreatedGamePlatform_53; }
	inline int32_t* get_address_of_LastCreatedGamePlatform_53() { return &___LastCreatedGamePlatform_53; }
	inline void set_LastCreatedGamePlatform_53(int32_t value)
	{
		___LastCreatedGamePlatform_53 = value;
	}

	inline static int32_t get_offset_of_CurrentInspectorState_54() { return static_cast<int32_t>(offsetof(Settings_t1104664933, ___CurrentInspectorState_54)); }
	inline int32_t get_CurrentInspectorState_54() const { return ___CurrentInspectorState_54; }
	inline int32_t* get_address_of_CurrentInspectorState_54() { return &___CurrentInspectorState_54; }
	inline void set_CurrentInspectorState_54(int32_t value)
	{
		___CurrentInspectorState_54 = value;
	}

	inline static int32_t get_offset_of_ClosedHints_55() { return static_cast<int32_t>(offsetof(Settings_t1104664933, ___ClosedHints_55)); }
	inline List_1_t4088314513 * get_ClosedHints_55() const { return ___ClosedHints_55; }
	inline List_1_t4088314513 ** get_address_of_ClosedHints_55() { return &___ClosedHints_55; }
	inline void set_ClosedHints_55(List_1_t4088314513 * value)
	{
		___ClosedHints_55 = value;
		Il2CppCodeGenWriteBarrier(&___ClosedHints_55, value);
	}

	inline static int32_t get_offset_of_DisplayHints_56() { return static_cast<int32_t>(offsetof(Settings_t1104664933, ___DisplayHints_56)); }
	inline bool get_DisplayHints_56() const { return ___DisplayHints_56; }
	inline bool* get_address_of_DisplayHints_56() { return &___DisplayHints_56; }
	inline void set_DisplayHints_56(bool value)
	{
		___DisplayHints_56 = value;
	}

	inline static int32_t get_offset_of_DisplayHintsScrollState_57() { return static_cast<int32_t>(offsetof(Settings_t1104664933, ___DisplayHintsScrollState_57)); }
	inline Vector2_t3525329788  get_DisplayHintsScrollState_57() const { return ___DisplayHintsScrollState_57; }
	inline Vector2_t3525329788 * get_address_of_DisplayHintsScrollState_57() { return &___DisplayHintsScrollState_57; }
	inline void set_DisplayHintsScrollState_57(Vector2_t3525329788  value)
	{
		___DisplayHintsScrollState_57 = value;
	}

	inline static int32_t get_offset_of_Logo_58() { return static_cast<int32_t>(offsetof(Settings_t1104664933, ___Logo_58)); }
	inline Texture2D_t2509538522 * get_Logo_58() const { return ___Logo_58; }
	inline Texture2D_t2509538522 ** get_address_of_Logo_58() { return &___Logo_58; }
	inline void set_Logo_58(Texture2D_t2509538522 * value)
	{
		___Logo_58 = value;
		Il2CppCodeGenWriteBarrier(&___Logo_58, value);
	}

	inline static int32_t get_offset_of_UpdateIcon_59() { return static_cast<int32_t>(offsetof(Settings_t1104664933, ___UpdateIcon_59)); }
	inline Texture2D_t2509538522 * get_UpdateIcon_59() const { return ___UpdateIcon_59; }
	inline Texture2D_t2509538522 ** get_address_of_UpdateIcon_59() { return &___UpdateIcon_59; }
	inline void set_UpdateIcon_59(Texture2D_t2509538522 * value)
	{
		___UpdateIcon_59 = value;
		Il2CppCodeGenWriteBarrier(&___UpdateIcon_59, value);
	}

	inline static int32_t get_offset_of_InfoIcon_60() { return static_cast<int32_t>(offsetof(Settings_t1104664933, ___InfoIcon_60)); }
	inline Texture2D_t2509538522 * get_InfoIcon_60() const { return ___InfoIcon_60; }
	inline Texture2D_t2509538522 ** get_address_of_InfoIcon_60() { return &___InfoIcon_60; }
	inline void set_InfoIcon_60(Texture2D_t2509538522 * value)
	{
		___InfoIcon_60 = value;
		Il2CppCodeGenWriteBarrier(&___InfoIcon_60, value);
	}

	inline static int32_t get_offset_of_DeleteIcon_61() { return static_cast<int32_t>(offsetof(Settings_t1104664933, ___DeleteIcon_61)); }
	inline Texture2D_t2509538522 * get_DeleteIcon_61() const { return ___DeleteIcon_61; }
	inline Texture2D_t2509538522 ** get_address_of_DeleteIcon_61() { return &___DeleteIcon_61; }
	inline void set_DeleteIcon_61(Texture2D_t2509538522 * value)
	{
		___DeleteIcon_61 = value;
		Il2CppCodeGenWriteBarrier(&___DeleteIcon_61, value);
	}

	inline static int32_t get_offset_of_GameIcon_62() { return static_cast<int32_t>(offsetof(Settings_t1104664933, ___GameIcon_62)); }
	inline Texture2D_t2509538522 * get_GameIcon_62() const { return ___GameIcon_62; }
	inline Texture2D_t2509538522 ** get_address_of_GameIcon_62() { return &___GameIcon_62; }
	inline void set_GameIcon_62(Texture2D_t2509538522 * value)
	{
		___GameIcon_62 = value;
		Il2CppCodeGenWriteBarrier(&___GameIcon_62, value);
	}

	inline static int32_t get_offset_of_HomeIcon_63() { return static_cast<int32_t>(offsetof(Settings_t1104664933, ___HomeIcon_63)); }
	inline Texture2D_t2509538522 * get_HomeIcon_63() const { return ___HomeIcon_63; }
	inline Texture2D_t2509538522 ** get_address_of_HomeIcon_63() { return &___HomeIcon_63; }
	inline void set_HomeIcon_63(Texture2D_t2509538522 * value)
	{
		___HomeIcon_63 = value;
		Il2CppCodeGenWriteBarrier(&___HomeIcon_63, value);
	}

	inline static int32_t get_offset_of_InstrumentIcon_64() { return static_cast<int32_t>(offsetof(Settings_t1104664933, ___InstrumentIcon_64)); }
	inline Texture2D_t2509538522 * get_InstrumentIcon_64() const { return ___InstrumentIcon_64; }
	inline Texture2D_t2509538522 ** get_address_of_InstrumentIcon_64() { return &___InstrumentIcon_64; }
	inline void set_InstrumentIcon_64(Texture2D_t2509538522 * value)
	{
		___InstrumentIcon_64 = value;
		Il2CppCodeGenWriteBarrier(&___InstrumentIcon_64, value);
	}

	inline static int32_t get_offset_of_QuestionIcon_65() { return static_cast<int32_t>(offsetof(Settings_t1104664933, ___QuestionIcon_65)); }
	inline Texture2D_t2509538522 * get_QuestionIcon_65() const { return ___QuestionIcon_65; }
	inline Texture2D_t2509538522 ** get_address_of_QuestionIcon_65() { return &___QuestionIcon_65; }
	inline void set_QuestionIcon_65(Texture2D_t2509538522 * value)
	{
		___QuestionIcon_65 = value;
		Il2CppCodeGenWriteBarrier(&___QuestionIcon_65, value);
	}

	inline static int32_t get_offset_of_UserIcon_66() { return static_cast<int32_t>(offsetof(Settings_t1104664933, ___UserIcon_66)); }
	inline Texture2D_t2509538522 * get_UserIcon_66() const { return ___UserIcon_66; }
	inline Texture2D_t2509538522 ** get_address_of_UserIcon_66() { return &___UserIcon_66; }
	inline void set_UserIcon_66(Texture2D_t2509538522 * value)
	{
		___UserIcon_66 = value;
		Il2CppCodeGenWriteBarrier(&___UserIcon_66, value);
	}

	inline static int32_t get_offset_of_AmazonIcon_67() { return static_cast<int32_t>(offsetof(Settings_t1104664933, ___AmazonIcon_67)); }
	inline Texture2D_t2509538522 * get_AmazonIcon_67() const { return ___AmazonIcon_67; }
	inline Texture2D_t2509538522 ** get_address_of_AmazonIcon_67() { return &___AmazonIcon_67; }
	inline void set_AmazonIcon_67(Texture2D_t2509538522 * value)
	{
		___AmazonIcon_67 = value;
		Il2CppCodeGenWriteBarrier(&___AmazonIcon_67, value);
	}

	inline static int32_t get_offset_of_GooglePlayIcon_68() { return static_cast<int32_t>(offsetof(Settings_t1104664933, ___GooglePlayIcon_68)); }
	inline Texture2D_t2509538522 * get_GooglePlayIcon_68() const { return ___GooglePlayIcon_68; }
	inline Texture2D_t2509538522 ** get_address_of_GooglePlayIcon_68() { return &___GooglePlayIcon_68; }
	inline void set_GooglePlayIcon_68(Texture2D_t2509538522 * value)
	{
		___GooglePlayIcon_68 = value;
		Il2CppCodeGenWriteBarrier(&___GooglePlayIcon_68, value);
	}

	inline static int32_t get_offset_of_iosIcon_69() { return static_cast<int32_t>(offsetof(Settings_t1104664933, ___iosIcon_69)); }
	inline Texture2D_t2509538522 * get_iosIcon_69() const { return ___iosIcon_69; }
	inline Texture2D_t2509538522 ** get_address_of_iosIcon_69() { return &___iosIcon_69; }
	inline void set_iosIcon_69(Texture2D_t2509538522 * value)
	{
		___iosIcon_69 = value;
		Il2CppCodeGenWriteBarrier(&___iosIcon_69, value);
	}

	inline static int32_t get_offset_of_macIcon_70() { return static_cast<int32_t>(offsetof(Settings_t1104664933, ___macIcon_70)); }
	inline Texture2D_t2509538522 * get_macIcon_70() const { return ___macIcon_70; }
	inline Texture2D_t2509538522 ** get_address_of_macIcon_70() { return &___macIcon_70; }
	inline void set_macIcon_70(Texture2D_t2509538522 * value)
	{
		___macIcon_70 = value;
		Il2CppCodeGenWriteBarrier(&___macIcon_70, value);
	}

	inline static int32_t get_offset_of_windowsPhoneIcon_71() { return static_cast<int32_t>(offsetof(Settings_t1104664933, ___windowsPhoneIcon_71)); }
	inline Texture2D_t2509538522 * get_windowsPhoneIcon_71() const { return ___windowsPhoneIcon_71; }
	inline Texture2D_t2509538522 ** get_address_of_windowsPhoneIcon_71() { return &___windowsPhoneIcon_71; }
	inline void set_windowsPhoneIcon_71(Texture2D_t2509538522 * value)
	{
		___windowsPhoneIcon_71 = value;
		Il2CppCodeGenWriteBarrier(&___windowsPhoneIcon_71, value);
	}

	inline static int32_t get_offset_of_SignupButton_72() { return static_cast<int32_t>(offsetof(Settings_t1104664933, ___SignupButton_72)); }
	inline GUIStyle_t1006925219 * get_SignupButton_72() const { return ___SignupButton_72; }
	inline GUIStyle_t1006925219 ** get_address_of_SignupButton_72() { return &___SignupButton_72; }
	inline void set_SignupButton_72(GUIStyle_t1006925219 * value)
	{
		___SignupButton_72 = value;
		Il2CppCodeGenWriteBarrier(&___SignupButton_72, value);
	}

	inline static int32_t get_offset_of_SubmitErrors_73() { return static_cast<int32_t>(offsetof(Settings_t1104664933, ___SubmitErrors_73)); }
	inline bool get_SubmitErrors_73() const { return ___SubmitErrors_73; }
	inline bool* get_address_of_SubmitErrors_73() { return &___SubmitErrors_73; }
	inline void set_SubmitErrors_73(bool value)
	{
		___SubmitErrors_73 = value;
	}

	inline static int32_t get_offset_of_MaxErrorCount_74() { return static_cast<int32_t>(offsetof(Settings_t1104664933, ___MaxErrorCount_74)); }
	inline int32_t get_MaxErrorCount_74() const { return ___MaxErrorCount_74; }
	inline int32_t* get_address_of_MaxErrorCount_74() { return &___MaxErrorCount_74; }
	inline void set_MaxErrorCount_74(int32_t value)
	{
		___MaxErrorCount_74 = value;
	}

	inline static int32_t get_offset_of_SubmitFpsAverage_75() { return static_cast<int32_t>(offsetof(Settings_t1104664933, ___SubmitFpsAverage_75)); }
	inline bool get_SubmitFpsAverage_75() const { return ___SubmitFpsAverage_75; }
	inline bool* get_address_of_SubmitFpsAverage_75() { return &___SubmitFpsAverage_75; }
	inline void set_SubmitFpsAverage_75(bool value)
	{
		___SubmitFpsAverage_75 = value;
	}

	inline static int32_t get_offset_of_SubmitFpsCritical_76() { return static_cast<int32_t>(offsetof(Settings_t1104664933, ___SubmitFpsCritical_76)); }
	inline bool get_SubmitFpsCritical_76() const { return ___SubmitFpsCritical_76; }
	inline bool* get_address_of_SubmitFpsCritical_76() { return &___SubmitFpsCritical_76; }
	inline void set_SubmitFpsCritical_76(bool value)
	{
		___SubmitFpsCritical_76 = value;
	}

	inline static int32_t get_offset_of_FpsCriticalThreshold_77() { return static_cast<int32_t>(offsetof(Settings_t1104664933, ___FpsCriticalThreshold_77)); }
	inline int32_t get_FpsCriticalThreshold_77() const { return ___FpsCriticalThreshold_77; }
	inline int32_t* get_address_of_FpsCriticalThreshold_77() { return &___FpsCriticalThreshold_77; }
	inline void set_FpsCriticalThreshold_77(int32_t value)
	{
		___FpsCriticalThreshold_77 = value;
	}

	inline static int32_t get_offset_of_FpsCirticalSubmitInterval_78() { return static_cast<int32_t>(offsetof(Settings_t1104664933, ___FpsCirticalSubmitInterval_78)); }
	inline int32_t get_FpsCirticalSubmitInterval_78() const { return ___FpsCirticalSubmitInterval_78; }
	inline int32_t* get_address_of_FpsCirticalSubmitInterval_78() { return &___FpsCirticalSubmitInterval_78; }
	inline void set_FpsCirticalSubmitInterval_78(int32_t value)
	{
		___FpsCirticalSubmitInterval_78 = value;
	}

	inline static int32_t get_offset_of_PlatformFoldOut_79() { return static_cast<int32_t>(offsetof(Settings_t1104664933, ___PlatformFoldOut_79)); }
	inline BooleanU5BU5D_t3804927312* get_PlatformFoldOut_79() const { return ___PlatformFoldOut_79; }
	inline BooleanU5BU5D_t3804927312** get_address_of_PlatformFoldOut_79() { return &___PlatformFoldOut_79; }
	inline void set_PlatformFoldOut_79(BooleanU5BU5D_t3804927312* value)
	{
		___PlatformFoldOut_79 = value;
		Il2CppCodeGenWriteBarrier(&___PlatformFoldOut_79, value);
	}

	inline static int32_t get_offset_of_CustomDimensions01FoldOut_80() { return static_cast<int32_t>(offsetof(Settings_t1104664933, ___CustomDimensions01FoldOut_80)); }
	inline bool get_CustomDimensions01FoldOut_80() const { return ___CustomDimensions01FoldOut_80; }
	inline bool* get_address_of_CustomDimensions01FoldOut_80() { return &___CustomDimensions01FoldOut_80; }
	inline void set_CustomDimensions01FoldOut_80(bool value)
	{
		___CustomDimensions01FoldOut_80 = value;
	}

	inline static int32_t get_offset_of_CustomDimensions02FoldOut_81() { return static_cast<int32_t>(offsetof(Settings_t1104664933, ___CustomDimensions02FoldOut_81)); }
	inline bool get_CustomDimensions02FoldOut_81() const { return ___CustomDimensions02FoldOut_81; }
	inline bool* get_address_of_CustomDimensions02FoldOut_81() { return &___CustomDimensions02FoldOut_81; }
	inline void set_CustomDimensions02FoldOut_81(bool value)
	{
		___CustomDimensions02FoldOut_81 = value;
	}

	inline static int32_t get_offset_of_CustomDimensions03FoldOut_82() { return static_cast<int32_t>(offsetof(Settings_t1104664933, ___CustomDimensions03FoldOut_82)); }
	inline bool get_CustomDimensions03FoldOut_82() const { return ___CustomDimensions03FoldOut_82; }
	inline bool* get_address_of_CustomDimensions03FoldOut_82() { return &___CustomDimensions03FoldOut_82; }
	inline void set_CustomDimensions03FoldOut_82(bool value)
	{
		___CustomDimensions03FoldOut_82 = value;
	}

	inline static int32_t get_offset_of_ResourceItemTypesFoldOut_83() { return static_cast<int32_t>(offsetof(Settings_t1104664933, ___ResourceItemTypesFoldOut_83)); }
	inline bool get_ResourceItemTypesFoldOut_83() const { return ___ResourceItemTypesFoldOut_83; }
	inline bool* get_address_of_ResourceItemTypesFoldOut_83() { return &___ResourceItemTypesFoldOut_83; }
	inline void set_ResourceItemTypesFoldOut_83(bool value)
	{
		___ResourceItemTypesFoldOut_83 = value;
	}

	inline static int32_t get_offset_of_ResourceCurrenciesFoldOut_84() { return static_cast<int32_t>(offsetof(Settings_t1104664933, ___ResourceCurrenciesFoldOut_84)); }
	inline bool get_ResourceCurrenciesFoldOut_84() const { return ___ResourceCurrenciesFoldOut_84; }
	inline bool* get_address_of_ResourceCurrenciesFoldOut_84() { return &___ResourceCurrenciesFoldOut_84; }
	inline void set_ResourceCurrenciesFoldOut_84(bool value)
	{
		___ResourceCurrenciesFoldOut_84 = value;
	}
};

struct Settings_t1104664933_StaticFields
{
public:
	// System.String GameAnalyticsSDK.Settings::VERSION
	String_t* ___VERSION_3;

public:
	inline static int32_t get_offset_of_VERSION_3() { return static_cast<int32_t>(offsetof(Settings_t1104664933_StaticFields, ___VERSION_3)); }
	inline String_t* get_VERSION_3() const { return ___VERSION_3; }
	inline String_t** get_address_of_VERSION_3() { return &___VERSION_3; }
	inline void set_VERSION_3(String_t* value)
	{
		___VERSION_3 = value;
		Il2CppCodeGenWriteBarrier(&___VERSION_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
