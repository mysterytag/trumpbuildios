﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// AchievementController/AchievementsInfo
struct AchievementsInfo_t1267089938;

#include "codegen/il2cpp-codegen.h"

// System.Void AchievementController/AchievementsInfo::.ctor()
extern "C"  void AchievementsInfo__ctor_m3604423109 (AchievementsInfo_t1267089938 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
