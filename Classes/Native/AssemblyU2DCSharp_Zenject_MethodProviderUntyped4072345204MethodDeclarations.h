﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Zenject.MethodProviderUntyped
struct MethodProviderUntyped_t4072345204;
// System.Type
struct Type_t;
// System.Func`2<Zenject.InjectContext,System.Object>
struct Func_2_t2621245597;
// System.Object
struct Il2CppObject;
// Zenject.InjectContext
struct InjectContext_t3456483891;
// System.Collections.Generic.IEnumerable`1<Zenject.ZenjectResolveException>
struct IEnumerable_1_t4073207355;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Type2779229935.h"
#include "AssemblyU2DCSharp_Zenject_InjectContext3456483891.h"

// System.Void Zenject.MethodProviderUntyped::.ctor(System.Type,System.Func`2<Zenject.InjectContext,System.Object>)
extern "C"  void MethodProviderUntyped__ctor_m2073753604 (MethodProviderUntyped_t4072345204 * __this, Type_t * ___returnType0, Func_2_t2621245597 * ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type Zenject.MethodProviderUntyped::GetInstanceType()
extern "C"  Type_t * MethodProviderUntyped_GetInstanceType_m3175886856 (MethodProviderUntyped_t4072345204 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Zenject.MethodProviderUntyped::GetInstance(Zenject.InjectContext)
extern "C"  Il2CppObject * MethodProviderUntyped_GetInstance_m356285034 (MethodProviderUntyped_t4072345204 * __this, InjectContext_t3456483891 * ___context0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.IEnumerable`1<Zenject.ZenjectResolveException> Zenject.MethodProviderUntyped::ValidateBinding(Zenject.InjectContext)
extern "C"  Il2CppObject* MethodProviderUntyped_ValidateBinding_m4068907900 (MethodProviderUntyped_t4072345204 * __this, InjectContext_t3456483891 * ___context0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
