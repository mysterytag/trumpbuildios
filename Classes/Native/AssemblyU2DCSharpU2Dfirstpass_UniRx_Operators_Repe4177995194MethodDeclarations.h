﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.RepeatSafeObservable`1<System.Object>
struct RepeatSafeObservable_1_t4177995194;
// System.Collections.Generic.IEnumerable`1<UniRx.IObservable`1<System.Object>>
struct IEnumerable_1_t3468059140;
// System.IDisposable
struct IDisposable_t1628921374;
// UniRx.IObserver`1<System.Object>
struct IObserver_1_t3049105323;

#include "codegen/il2cpp-codegen.h"

// System.Void UniRx.Operators.RepeatSafeObservable`1<System.Object>::.ctor(System.Collections.Generic.IEnumerable`1<UniRx.IObservable`1<T>>,System.Boolean)
extern "C"  void RepeatSafeObservable_1__ctor_m3539667011_gshared (RepeatSafeObservable_1_t4177995194 * __this, Il2CppObject* ___sources0, bool ___isRequiredSubscribeOnCurrentThread1, const MethodInfo* method);
#define RepeatSafeObservable_1__ctor_m3539667011(__this, ___sources0, ___isRequiredSubscribeOnCurrentThread1, method) ((  void (*) (RepeatSafeObservable_1_t4177995194 *, Il2CppObject*, bool, const MethodInfo*))RepeatSafeObservable_1__ctor_m3539667011_gshared)(__this, ___sources0, ___isRequiredSubscribeOnCurrentThread1, method)
// System.IDisposable UniRx.Operators.RepeatSafeObservable`1<System.Object>::SubscribeCore(UniRx.IObserver`1<T>,System.IDisposable)
extern "C"  Il2CppObject * RepeatSafeObservable_1_SubscribeCore_m4173925050_gshared (RepeatSafeObservable_1_t4177995194 * __this, Il2CppObject* ___observer0, Il2CppObject * ___cancel1, const MethodInfo* method);
#define RepeatSafeObservable_1_SubscribeCore_m4173925050(__this, ___observer0, ___cancel1, method) ((  Il2CppObject * (*) (RepeatSafeObservable_1_t4177995194 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))RepeatSafeObservable_1_SubscribeCore_m4173925050_gshared)(__this, ___observer0, ___cancel1, method)
