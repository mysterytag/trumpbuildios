﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UniRx.RefCountDisposable
struct RefCountDisposable_t3288429070;
// System.Object
struct Il2CppObject;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.RefCountDisposable/InnerDisposable
struct  InnerDisposable_t3329838134  : public Il2CppObject
{
public:
	// UniRx.RefCountDisposable UniRx.RefCountDisposable/InnerDisposable::_parent
	RefCountDisposable_t3288429070 * ____parent_0;
	// System.Object UniRx.RefCountDisposable/InnerDisposable::parentLock
	Il2CppObject * ___parentLock_1;

public:
	inline static int32_t get_offset_of__parent_0() { return static_cast<int32_t>(offsetof(InnerDisposable_t3329838134, ____parent_0)); }
	inline RefCountDisposable_t3288429070 * get__parent_0() const { return ____parent_0; }
	inline RefCountDisposable_t3288429070 ** get_address_of__parent_0() { return &____parent_0; }
	inline void set__parent_0(RefCountDisposable_t3288429070 * value)
	{
		____parent_0 = value;
		Il2CppCodeGenWriteBarrier(&____parent_0, value);
	}

	inline static int32_t get_offset_of_parentLock_1() { return static_cast<int32_t>(offsetof(InnerDisposable_t3329838134, ___parentLock_1)); }
	inline Il2CppObject * get_parentLock_1() const { return ___parentLock_1; }
	inline Il2CppObject ** get_address_of_parentLock_1() { return &___parentLock_1; }
	inline void set_parentLock_1(Il2CppObject * value)
	{
		___parentLock_1 = value;
		Il2CppCodeGenWriteBarrier(&___parentLock_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
