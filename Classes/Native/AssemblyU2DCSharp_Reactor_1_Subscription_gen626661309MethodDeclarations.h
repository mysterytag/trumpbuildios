﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Reactor`1/Subscription<System.Int32>
struct Subscription_t626661310;

#include "codegen/il2cpp-codegen.h"

// System.Void Reactor`1/Subscription<System.Int32>::.ctor()
extern "C"  void Subscription__ctor_m2023534479_gshared (Subscription_t626661310 * __this, const MethodInfo* method);
#define Subscription__ctor_m2023534479(__this, method) ((  void (*) (Subscription_t626661310 *, const MethodInfo*))Subscription__ctor_m2023534479_gshared)(__this, method)
// System.Void Reactor`1/Subscription<System.Int32>::Dispose()
extern "C"  void Subscription_Dispose_m3189699596_gshared (Subscription_t626661310 * __this, const MethodInfo* method);
#define Subscription_Dispose_m3189699596(__this, method) ((  void (*) (Subscription_t626661310 *, const MethodInfo*))Subscription_Dispose_m3189699596_gshared)(__this, method)
