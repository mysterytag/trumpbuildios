﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// TableButtons/<RegisterDonateButtons>c__AnonStorey169/<RegisterDonateButtons>c__AnonStorey167
struct U3CRegisterDonateButtonsU3Ec__AnonStorey167_t2690654998;

#include "codegen/il2cpp-codegen.h"

// System.Void TableButtons/<RegisterDonateButtons>c__AnonStorey169/<RegisterDonateButtons>c__AnonStorey167::.ctor()
extern "C"  void U3CRegisterDonateButtonsU3Ec__AnonStorey167__ctor_m1176988920 (U3CRegisterDonateButtonsU3Ec__AnonStorey167_t2690654998 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TableButtons/<RegisterDonateButtons>c__AnonStorey169/<RegisterDonateButtons>c__AnonStorey167::<>m__26F(System.Boolean)
extern "C"  void U3CRegisterDonateButtonsU3Ec__AnonStorey167_U3CU3Em__26F_m66705018 (U3CRegisterDonateButtonsU3Ec__AnonStorey167_t2690654998 * __this, bool ___b0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
