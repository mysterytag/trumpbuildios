﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.List`1<UniRx.CollectionAddEvent`1<System.Object>>
struct List_1_t3213480956;
// System.Collections.Generic.IEnumerable`1<UniRx.CollectionAddEvent`1<System.Object>>
struct IEnumerable_1_t993709047;
// System.Collections.Generic.IEnumerator`1<UniRx.CollectionAddEvent`1<System.Object>>
struct IEnumerator_1_t3899628435;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t287207039;
// System.Object
struct Il2CppObject;
// System.Collections.Generic.ICollection`1<UniRx.CollectionAddEvent`1<System.Object>>
struct ICollection_1_t2882353373;
// System.Collections.ObjectModel.ReadOnlyCollection`1<UniRx.CollectionAddEvent`1<System.Object>>
struct ReadOnlyCollection_1_t1284700039;
// UniRx.CollectionAddEvent`1<System.Object>[]
struct CollectionAddEvent_1U5BU5D_t789875090;
// System.Predicate`1<UniRx.CollectionAddEvent`1<System.Object>>
struct Predicate_1_t2987485885;
// System.Action`1<UniRx.CollectionAddEvent`1<System.Object>>
struct Action_1_t2564974692;
// System.Collections.Generic.IComparer`1<UniRx.CollectionAddEvent`1<System.Object>>
struct IComparer_1_t821262100;
// System.Comparison`1<UniRx.CollectionAddEvent`1<System.Object>>
struct Comparison_1_t825229567;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array2840145358.h"
#include "mscorlib_System_Object837106420.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_CollectionAddE2416521987.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera1299263948.h"

// System.Void System.Collections.Generic.List`1<UniRx.CollectionAddEvent`1<System.Object>>::.ctor()
extern "C"  void List_1__ctor_m4068708466_gshared (List_1_t3213480956 * __this, const MethodInfo* method);
#define List_1__ctor_m4068708466(__this, method) ((  void (*) (List_1_t3213480956 *, const MethodInfo*))List_1__ctor_m4068708466_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UniRx.CollectionAddEvent`1<System.Object>>::.ctor(System.Collections.Generic.IEnumerable`1<T>)
extern "C"  void List_1__ctor_m1282361549_gshared (List_1_t3213480956 * __this, Il2CppObject* ___collection0, const MethodInfo* method);
#define List_1__ctor_m1282361549(__this, ___collection0, method) ((  void (*) (List_1_t3213480956 *, Il2CppObject*, const MethodInfo*))List_1__ctor_m1282361549_gshared)(__this, ___collection0, method)
// System.Void System.Collections.Generic.List`1<UniRx.CollectionAddEvent`1<System.Object>>::.ctor(System.Int32)
extern "C"  void List_1__ctor_m1183984323_gshared (List_1_t3213480956 * __this, int32_t ___capacity0, const MethodInfo* method);
#define List_1__ctor_m1183984323(__this, ___capacity0, method) ((  void (*) (List_1_t3213480956 *, int32_t, const MethodInfo*))List_1__ctor_m1183984323_gshared)(__this, ___capacity0, method)
// System.Void System.Collections.Generic.List`1<UniRx.CollectionAddEvent`1<System.Object>>::.cctor()
extern "C"  void List_1__cctor_m1093814651_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define List_1__cctor_m1093814651(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))List_1__cctor_m1093814651_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.List`1<UniRx.CollectionAddEvent`1<System.Object>>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
extern "C"  Il2CppObject* List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m3358927684_gshared (List_1_t3213480956 * __this, const MethodInfo* method);
#define List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m3358927684(__this, method) ((  Il2CppObject* (*) (List_1_t3213480956 *, const MethodInfo*))List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m3358927684_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UniRx.CollectionAddEvent`1<System.Object>>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void List_1_System_Collections_ICollection_CopyTo_m213899538_gshared (List_1_t3213480956 * __this, Il2CppArray * ___array0, int32_t ___arrayIndex1, const MethodInfo* method);
#define List_1_System_Collections_ICollection_CopyTo_m213899538(__this, ___array0, ___arrayIndex1, method) ((  void (*) (List_1_t3213480956 *, Il2CppArray *, int32_t, const MethodInfo*))List_1_System_Collections_ICollection_CopyTo_m213899538_gshared)(__this, ___array0, ___arrayIndex1, method)
// System.Collections.IEnumerator System.Collections.Generic.List`1<UniRx.CollectionAddEvent`1<System.Object>>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * List_1_System_Collections_IEnumerable_GetEnumerator_m979669389_gshared (List_1_t3213480956 * __this, const MethodInfo* method);
#define List_1_System_Collections_IEnumerable_GetEnumerator_m979669389(__this, method) ((  Il2CppObject * (*) (List_1_t3213480956 *, const MethodInfo*))List_1_System_Collections_IEnumerable_GetEnumerator_m979669389_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<UniRx.CollectionAddEvent`1<System.Object>>::System.Collections.IList.Add(System.Object)
extern "C"  int32_t List_1_System_Collections_IList_Add_m2203176836_gshared (List_1_t3213480956 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define List_1_System_Collections_IList_Add_m2203176836(__this, ___item0, method) ((  int32_t (*) (List_1_t3213480956 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Add_m2203176836_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.List`1<UniRx.CollectionAddEvent`1<System.Object>>::System.Collections.IList.Contains(System.Object)
extern "C"  bool List_1_System_Collections_IList_Contains_m2699449800_gshared (List_1_t3213480956 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define List_1_System_Collections_IList_Contains_m2699449800(__this, ___item0, method) ((  bool (*) (List_1_t3213480956 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Contains_m2699449800_gshared)(__this, ___item0, method)
// System.Int32 System.Collections.Generic.List`1<UniRx.CollectionAddEvent`1<System.Object>>::System.Collections.IList.IndexOf(System.Object)
extern "C"  int32_t List_1_System_Collections_IList_IndexOf_m2388042716_gshared (List_1_t3213480956 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define List_1_System_Collections_IList_IndexOf_m2388042716(__this, ___item0, method) ((  int32_t (*) (List_1_t3213480956 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_IndexOf_m2388042716_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<UniRx.CollectionAddEvent`1<System.Object>>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C"  void List_1_System_Collections_IList_Insert_m418662407_gshared (List_1_t3213480956 * __this, int32_t ___index0, Il2CppObject * ___item1, const MethodInfo* method);
#define List_1_System_Collections_IList_Insert_m418662407(__this, ___index0, ___item1, method) ((  void (*) (List_1_t3213480956 *, int32_t, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Insert_m418662407_gshared)(__this, ___index0, ___item1, method)
// System.Void System.Collections.Generic.List`1<UniRx.CollectionAddEvent`1<System.Object>>::System.Collections.IList.Remove(System.Object)
extern "C"  void List_1_System_Collections_IList_Remove_m3288850881_gshared (List_1_t3213480956 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define List_1_System_Collections_IList_Remove_m3288850881(__this, ___item0, method) ((  void (*) (List_1_t3213480956 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Remove_m3288850881_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.List`1<UniRx.CollectionAddEvent`1<System.Object>>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C"  bool List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m454536137_gshared (List_1_t3213480956 * __this, const MethodInfo* method);
#define List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m454536137(__this, method) ((  bool (*) (List_1_t3213480956 *, const MethodInfo*))List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m454536137_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UniRx.CollectionAddEvent`1<System.Object>>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool List_1_System_Collections_ICollection_get_IsSynchronized_m336317588_gshared (List_1_t3213480956 * __this, const MethodInfo* method);
#define List_1_System_Collections_ICollection_get_IsSynchronized_m336317588(__this, method) ((  bool (*) (List_1_t3213480956 *, const MethodInfo*))List_1_System_Collections_ICollection_get_IsSynchronized_m336317588_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<UniRx.CollectionAddEvent`1<System.Object>>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * List_1_System_Collections_ICollection_get_SyncRoot_m1355049984_gshared (List_1_t3213480956 * __this, const MethodInfo* method);
#define List_1_System_Collections_ICollection_get_SyncRoot_m1355049984(__this, method) ((  Il2CppObject * (*) (List_1_t3213480956 *, const MethodInfo*))List_1_System_Collections_ICollection_get_SyncRoot_m1355049984_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UniRx.CollectionAddEvent`1<System.Object>>::System.Collections.IList.get_IsFixedSize()
extern "C"  bool List_1_System_Collections_IList_get_IsFixedSize_m146012983_gshared (List_1_t3213480956 * __this, const MethodInfo* method);
#define List_1_System_Collections_IList_get_IsFixedSize_m146012983(__this, method) ((  bool (*) (List_1_t3213480956 *, const MethodInfo*))List_1_System_Collections_IList_get_IsFixedSize_m146012983_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UniRx.CollectionAddEvent`1<System.Object>>::System.Collections.IList.get_IsReadOnly()
extern "C"  bool List_1_System_Collections_IList_get_IsReadOnly_m1012908258_gshared (List_1_t3213480956 * __this, const MethodInfo* method);
#define List_1_System_Collections_IList_get_IsReadOnly_m1012908258(__this, method) ((  bool (*) (List_1_t3213480956 *, const MethodInfo*))List_1_System_Collections_IList_get_IsReadOnly_m1012908258_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<UniRx.CollectionAddEvent`1<System.Object>>::System.Collections.IList.get_Item(System.Int32)
extern "C"  Il2CppObject * List_1_System_Collections_IList_get_Item_m1893555591_gshared (List_1_t3213480956 * __this, int32_t ___index0, const MethodInfo* method);
#define List_1_System_Collections_IList_get_Item_m1893555591(__this, ___index0, method) ((  Il2CppObject * (*) (List_1_t3213480956 *, int32_t, const MethodInfo*))List_1_System_Collections_IList_get_Item_m1893555591_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<UniRx.CollectionAddEvent`1<System.Object>>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C"  void List_1_System_Collections_IList_set_Item_m3516567070_gshared (List_1_t3213480956 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method);
#define List_1_System_Collections_IList_set_Item_m3516567070(__this, ___index0, ___value1, method) ((  void (*) (List_1_t3213480956 *, int32_t, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_set_Item_m3516567070_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.Generic.List`1<UniRx.CollectionAddEvent`1<System.Object>>::Add(T)
extern "C"  void List_1_Add_m814127565_gshared (List_1_t3213480956 * __this, CollectionAddEvent_1_t2416521987  ___item0, const MethodInfo* method);
#define List_1_Add_m814127565(__this, ___item0, method) ((  void (*) (List_1_t3213480956 *, CollectionAddEvent_1_t2416521987 , const MethodInfo*))List_1_Add_m814127565_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<UniRx.CollectionAddEvent`1<System.Object>>::GrowIfNeeded(System.Int32)
extern "C"  void List_1_GrowIfNeeded_m3731873672_gshared (List_1_t3213480956 * __this, int32_t ___newCount0, const MethodInfo* method);
#define List_1_GrowIfNeeded_m3731873672(__this, ___newCount0, method) ((  void (*) (List_1_t3213480956 *, int32_t, const MethodInfo*))List_1_GrowIfNeeded_m3731873672_gshared)(__this, ___newCount0, method)
// System.Void System.Collections.Generic.List`1<UniRx.CollectionAddEvent`1<System.Object>>::AddCollection(System.Collections.Generic.ICollection`1<T>)
extern "C"  void List_1_AddCollection_m1455429254_gshared (List_1_t3213480956 * __this, Il2CppObject* ___collection0, const MethodInfo* method);
#define List_1_AddCollection_m1455429254(__this, ___collection0, method) ((  void (*) (List_1_t3213480956 *, Il2CppObject*, const MethodInfo*))List_1_AddCollection_m1455429254_gshared)(__this, ___collection0, method)
// System.Void System.Collections.Generic.List`1<UniRx.CollectionAddEvent`1<System.Object>>::AddEnumerable(System.Collections.Generic.IEnumerable`1<T>)
extern "C"  void List_1_AddEnumerable_m716401478_gshared (List_1_t3213480956 * __this, Il2CppObject* ___enumerable0, const MethodInfo* method);
#define List_1_AddEnumerable_m716401478(__this, ___enumerable0, method) ((  void (*) (List_1_t3213480956 *, Il2CppObject*, const MethodInfo*))List_1_AddEnumerable_m716401478_gshared)(__this, ___enumerable0, method)
// System.Void System.Collections.Generic.List`1<UniRx.CollectionAddEvent`1<System.Object>>::AddRange(System.Collections.Generic.IEnumerable`1<T>)
extern "C"  void List_1_AddRange_m3149729681_gshared (List_1_t3213480956 * __this, Il2CppObject* ___collection0, const MethodInfo* method);
#define List_1_AddRange_m3149729681(__this, ___collection0, method) ((  void (*) (List_1_t3213480956 *, Il2CppObject*, const MethodInfo*))List_1_AddRange_m3149729681_gshared)(__this, ___collection0, method)
// System.Collections.ObjectModel.ReadOnlyCollection`1<T> System.Collections.Generic.List`1<UniRx.CollectionAddEvent`1<System.Object>>::AsReadOnly()
extern "C"  ReadOnlyCollection_1_t1284700039 * List_1_AsReadOnly_m3976402040_gshared (List_1_t3213480956 * __this, const MethodInfo* method);
#define List_1_AsReadOnly_m3976402040(__this, method) ((  ReadOnlyCollection_1_t1284700039 * (*) (List_1_t3213480956 *, const MethodInfo*))List_1_AsReadOnly_m3976402040_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UniRx.CollectionAddEvent`1<System.Object>>::Clear()
extern "C"  void List_1_Clear_m1474841757_gshared (List_1_t3213480956 * __this, const MethodInfo* method);
#define List_1_Clear_m1474841757(__this, method) ((  void (*) (List_1_t3213480956 *, const MethodInfo*))List_1_Clear_m1474841757_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UniRx.CollectionAddEvent`1<System.Object>>::Contains(T)
extern "C"  bool List_1_Contains_m2075249227_gshared (List_1_t3213480956 * __this, CollectionAddEvent_1_t2416521987  ___item0, const MethodInfo* method);
#define List_1_Contains_m2075249227(__this, ___item0, method) ((  bool (*) (List_1_t3213480956 *, CollectionAddEvent_1_t2416521987 , const MethodInfo*))List_1_Contains_m2075249227_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<UniRx.CollectionAddEvent`1<System.Object>>::CopyTo(T[],System.Int32)
extern "C"  void List_1_CopyTo_m2919794813_gshared (List_1_t3213480956 * __this, CollectionAddEvent_1U5BU5D_t789875090* ___array0, int32_t ___arrayIndex1, const MethodInfo* method);
#define List_1_CopyTo_m2919794813(__this, ___array0, ___arrayIndex1, method) ((  void (*) (List_1_t3213480956 *, CollectionAddEvent_1U5BU5D_t789875090*, int32_t, const MethodInfo*))List_1_CopyTo_m2919794813_gshared)(__this, ___array0, ___arrayIndex1, method)
// T System.Collections.Generic.List`1<UniRx.CollectionAddEvent`1<System.Object>>::Find(System.Predicate`1<T>)
extern "C"  CollectionAddEvent_1_t2416521987  List_1_Find_m1183889099_gshared (List_1_t3213480956 * __this, Predicate_1_t2987485885 * ___match0, const MethodInfo* method);
#define List_1_Find_m1183889099(__this, ___match0, method) ((  CollectionAddEvent_1_t2416521987  (*) (List_1_t3213480956 *, Predicate_1_t2987485885 *, const MethodInfo*))List_1_Find_m1183889099_gshared)(__this, ___match0, method)
// System.Void System.Collections.Generic.List`1<UniRx.CollectionAddEvent`1<System.Object>>::CheckMatch(System.Predicate`1<T>)
extern "C"  void List_1_CheckMatch_m1429132038_gshared (Il2CppObject * __this /* static, unused */, Predicate_1_t2987485885 * ___match0, const MethodInfo* method);
#define List_1_CheckMatch_m1429132038(__this /* static, unused */, ___match0, method) ((  void (*) (Il2CppObject * /* static, unused */, Predicate_1_t2987485885 *, const MethodInfo*))List_1_CheckMatch_m1429132038_gshared)(__this /* static, unused */, ___match0, method)
// System.Int32 System.Collections.Generic.List`1<UniRx.CollectionAddEvent`1<System.Object>>::GetIndex(System.Int32,System.Int32,System.Predicate`1<T>)
extern "C"  int32_t List_1_GetIndex_m2340680235_gshared (List_1_t3213480956 * __this, int32_t ___startIndex0, int32_t ___count1, Predicate_1_t2987485885 * ___match2, const MethodInfo* method);
#define List_1_GetIndex_m2340680235(__this, ___startIndex0, ___count1, ___match2, method) ((  int32_t (*) (List_1_t3213480956 *, int32_t, int32_t, Predicate_1_t2987485885 *, const MethodInfo*))List_1_GetIndex_m2340680235_gshared)(__this, ___startIndex0, ___count1, ___match2, method)
// System.Void System.Collections.Generic.List`1<UniRx.CollectionAddEvent`1<System.Object>>::ForEach(System.Action`1<T>)
extern "C"  void List_1_ForEach_m2802707194_gshared (List_1_t3213480956 * __this, Action_1_t2564974692 * ___action0, const MethodInfo* method);
#define List_1_ForEach_m2802707194(__this, ___action0, method) ((  void (*) (List_1_t3213480956 *, Action_1_t2564974692 *, const MethodInfo*))List_1_ForEach_m2802707194_gshared)(__this, ___action0, method)
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<UniRx.CollectionAddEvent`1<System.Object>>::GetEnumerator()
extern "C"  Enumerator_t1299263948  List_1_GetEnumerator_m4189659656_gshared (List_1_t3213480956 * __this, const MethodInfo* method);
#define List_1_GetEnumerator_m4189659656(__this, method) ((  Enumerator_t1299263948  (*) (List_1_t3213480956 *, const MethodInfo*))List_1_GetEnumerator_m4189659656_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<UniRx.CollectionAddEvent`1<System.Object>>::IndexOf(T)
extern "C"  int32_t List_1_IndexOf_m2164654785_gshared (List_1_t3213480956 * __this, CollectionAddEvent_1_t2416521987  ___item0, const MethodInfo* method);
#define List_1_IndexOf_m2164654785(__this, ___item0, method) ((  int32_t (*) (List_1_t3213480956 *, CollectionAddEvent_1_t2416521987 , const MethodInfo*))List_1_IndexOf_m2164654785_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<UniRx.CollectionAddEvent`1<System.Object>>::Shift(System.Int32,System.Int32)
extern "C"  void List_1_Shift_m1388483028_gshared (List_1_t3213480956 * __this, int32_t ___start0, int32_t ___delta1, const MethodInfo* method);
#define List_1_Shift_m1388483028(__this, ___start0, ___delta1, method) ((  void (*) (List_1_t3213480956 *, int32_t, int32_t, const MethodInfo*))List_1_Shift_m1388483028_gshared)(__this, ___start0, ___delta1, method)
// System.Void System.Collections.Generic.List`1<UniRx.CollectionAddEvent`1<System.Object>>::CheckIndex(System.Int32)
extern "C"  void List_1_CheckIndex_m2666161997_gshared (List_1_t3213480956 * __this, int32_t ___index0, const MethodInfo* method);
#define List_1_CheckIndex_m2666161997(__this, ___index0, method) ((  void (*) (List_1_t3213480956 *, int32_t, const MethodInfo*))List_1_CheckIndex_m2666161997_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<UniRx.CollectionAddEvent`1<System.Object>>::Insert(System.Int32,T)
extern "C"  void List_1_Insert_m3224934964_gshared (List_1_t3213480956 * __this, int32_t ___index0, CollectionAddEvent_1_t2416521987  ___item1, const MethodInfo* method);
#define List_1_Insert_m3224934964(__this, ___index0, ___item1, method) ((  void (*) (List_1_t3213480956 *, int32_t, CollectionAddEvent_1_t2416521987 , const MethodInfo*))List_1_Insert_m3224934964_gshared)(__this, ___index0, ___item1, method)
// System.Void System.Collections.Generic.List`1<UniRx.CollectionAddEvent`1<System.Object>>::CheckCollection(System.Collections.Generic.IEnumerable`1<T>)
extern "C"  void List_1_CheckCollection_m3329810345_gshared (List_1_t3213480956 * __this, Il2CppObject* ___collection0, const MethodInfo* method);
#define List_1_CheckCollection_m3329810345(__this, ___collection0, method) ((  void (*) (List_1_t3213480956 *, Il2CppObject*, const MethodInfo*))List_1_CheckCollection_m3329810345_gshared)(__this, ___collection0, method)
// System.Boolean System.Collections.Generic.List`1<UniRx.CollectionAddEvent`1<System.Object>>::Remove(T)
extern "C"  bool List_1_Remove_m2799345926_gshared (List_1_t3213480956 * __this, CollectionAddEvent_1_t2416521987  ___item0, const MethodInfo* method);
#define List_1_Remove_m2799345926(__this, ___item0, method) ((  bool (*) (List_1_t3213480956 *, CollectionAddEvent_1_t2416521987 , const MethodInfo*))List_1_Remove_m2799345926_gshared)(__this, ___item0, method)
// System.Int32 System.Collections.Generic.List`1<UniRx.CollectionAddEvent`1<System.Object>>::RemoveAll(System.Predicate`1<T>)
extern "C"  int32_t List_1_RemoveAll_m3895972804_gshared (List_1_t3213480956 * __this, Predicate_1_t2987485885 * ___match0, const MethodInfo* method);
#define List_1_RemoveAll_m3895972804(__this, ___match0, method) ((  int32_t (*) (List_1_t3213480956 *, Predicate_1_t2987485885 *, const MethodInfo*))List_1_RemoveAll_m3895972804_gshared)(__this, ___match0, method)
// System.Void System.Collections.Generic.List`1<UniRx.CollectionAddEvent`1<System.Object>>::RemoveAt(System.Int32)
extern "C"  void List_1_RemoveAt_m1098787834_gshared (List_1_t3213480956 * __this, int32_t ___index0, const MethodInfo* method);
#define List_1_RemoveAt_m1098787834(__this, ___index0, method) ((  void (*) (List_1_t3213480956 *, int32_t, const MethodInfo*))List_1_RemoveAt_m1098787834_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<UniRx.CollectionAddEvent`1<System.Object>>::Reverse()
extern "C"  void List_1_Reverse_m1646407730_gshared (List_1_t3213480956 * __this, const MethodInfo* method);
#define List_1_Reverse_m1646407730(__this, method) ((  void (*) (List_1_t3213480956 *, const MethodInfo*))List_1_Reverse_m1646407730_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UniRx.CollectionAddEvent`1<System.Object>>::Sort()
extern "C"  void List_1_Sort_m1340099760_gshared (List_1_t3213480956 * __this, const MethodInfo* method);
#define List_1_Sort_m1340099760(__this, method) ((  void (*) (List_1_t3213480956 *, const MethodInfo*))List_1_Sort_m1340099760_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UniRx.CollectionAddEvent`1<System.Object>>::Sort(System.Collections.Generic.IComparer`1<T>)
extern "C"  void List_1_Sort_m3172925236_gshared (List_1_t3213480956 * __this, Il2CppObject* ___comparer0, const MethodInfo* method);
#define List_1_Sort_m3172925236(__this, ___comparer0, method) ((  void (*) (List_1_t3213480956 *, Il2CppObject*, const MethodInfo*))List_1_Sort_m3172925236_gshared)(__this, ___comparer0, method)
// System.Void System.Collections.Generic.List`1<UniRx.CollectionAddEvent`1<System.Object>>::Sort(System.Comparison`1<T>)
extern "C"  void List_1_Sort_m2365502723_gshared (List_1_t3213480956 * __this, Comparison_1_t825229567 * ___comparison0, const MethodInfo* method);
#define List_1_Sort_m2365502723(__this, ___comparison0, method) ((  void (*) (List_1_t3213480956 *, Comparison_1_t825229567 *, const MethodInfo*))List_1_Sort_m2365502723_gshared)(__this, ___comparison0, method)
// T[] System.Collections.Generic.List`1<UniRx.CollectionAddEvent`1<System.Object>>::ToArray()
extern "C"  CollectionAddEvent_1U5BU5D_t789875090* List_1_ToArray_m1279008433_gshared (List_1_t3213480956 * __this, const MethodInfo* method);
#define List_1_ToArray_m1279008433(__this, method) ((  CollectionAddEvent_1U5BU5D_t789875090* (*) (List_1_t3213480956 *, const MethodInfo*))List_1_ToArray_m1279008433_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UniRx.CollectionAddEvent`1<System.Object>>::TrimExcess()
extern "C"  void List_1_TrimExcess_m2280597577_gshared (List_1_t3213480956 * __this, const MethodInfo* method);
#define List_1_TrimExcess_m2280597577(__this, method) ((  void (*) (List_1_t3213480956 *, const MethodInfo*))List_1_TrimExcess_m2280597577_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<UniRx.CollectionAddEvent`1<System.Object>>::get_Capacity()
extern "C"  int32_t List_1_get_Capacity_m2499064433_gshared (List_1_t3213480956 * __this, const MethodInfo* method);
#define List_1_get_Capacity_m2499064433(__this, method) ((  int32_t (*) (List_1_t3213480956 *, const MethodInfo*))List_1_get_Capacity_m2499064433_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UniRx.CollectionAddEvent`1<System.Object>>::set_Capacity(System.Int32)
extern "C"  void List_1_set_Capacity_m942062490_gshared (List_1_t3213480956 * __this, int32_t ___value0, const MethodInfo* method);
#define List_1_set_Capacity_m942062490(__this, ___value0, method) ((  void (*) (List_1_t3213480956 *, int32_t, const MethodInfo*))List_1_set_Capacity_m942062490_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.Generic.List`1<UniRx.CollectionAddEvent`1<System.Object>>::get_Count()
extern "C"  int32_t List_1_get_Count_m2497096410_gshared (List_1_t3213480956 * __this, const MethodInfo* method);
#define List_1_get_Count_m2497096410(__this, method) ((  int32_t (*) (List_1_t3213480956 *, const MethodInfo*))List_1_get_Count_m2497096410_gshared)(__this, method)
// T System.Collections.Generic.List`1<UniRx.CollectionAddEvent`1<System.Object>>::get_Item(System.Int32)
extern "C"  CollectionAddEvent_1_t2416521987  List_1_get_Item_m3663687934_gshared (List_1_t3213480956 * __this, int32_t ___index0, const MethodInfo* method);
#define List_1_get_Item_m3663687934(__this, ___index0, method) ((  CollectionAddEvent_1_t2416521987  (*) (List_1_t3213480956 *, int32_t, const MethodInfo*))List_1_get_Item_m3663687934_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<UniRx.CollectionAddEvent`1<System.Object>>::set_Item(System.Int32,T)
extern "C"  void List_1_set_Item_m5709323_gshared (List_1_t3213480956 * __this, int32_t ___index0, CollectionAddEvent_1_t2416521987  ___value1, const MethodInfo* method);
#define List_1_set_Item_m5709323(__this, ___index0, ___value1, method) ((  void (*) (List_1_t3213480956 *, int32_t, CollectionAddEvent_1_t2416521987 , const MethodInfo*))List_1_set_Item_m5709323_gshared)(__this, ___index0, ___value1, method)
