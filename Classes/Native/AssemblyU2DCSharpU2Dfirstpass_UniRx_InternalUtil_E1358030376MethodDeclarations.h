﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.InternalUtil.EmptyObserver`1<UniRx.CollectionAddEvent`1<UniRx.Tuple`2<System.Object,System.Object>>>
struct EmptyObserver_1_t1358030376;
// System.Exception
struct Exception_t1967233988;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Exception1967233988.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_CollectionAddE1948677386.h"

// System.Void UniRx.InternalUtil.EmptyObserver`1<UniRx.CollectionAddEvent`1<UniRx.Tuple`2<System.Object,System.Object>>>::.ctor()
extern "C"  void EmptyObserver_1__ctor_m3466252522_gshared (EmptyObserver_1_t1358030376 * __this, const MethodInfo* method);
#define EmptyObserver_1__ctor_m3466252522(__this, method) ((  void (*) (EmptyObserver_1_t1358030376 *, const MethodInfo*))EmptyObserver_1__ctor_m3466252522_gshared)(__this, method)
// System.Void UniRx.InternalUtil.EmptyObserver`1<UniRx.CollectionAddEvent`1<UniRx.Tuple`2<System.Object,System.Object>>>::.cctor()
extern "C"  void EmptyObserver_1__cctor_m3892516867_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define EmptyObserver_1__cctor_m3892516867(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))EmptyObserver_1__cctor_m3892516867_gshared)(__this /* static, unused */, method)
// System.Void UniRx.InternalUtil.EmptyObserver`1<UniRx.CollectionAddEvent`1<UniRx.Tuple`2<System.Object,System.Object>>>::OnCompleted()
extern "C"  void EmptyObserver_1_OnCompleted_m3466341620_gshared (EmptyObserver_1_t1358030376 * __this, const MethodInfo* method);
#define EmptyObserver_1_OnCompleted_m3466341620(__this, method) ((  void (*) (EmptyObserver_1_t1358030376 *, const MethodInfo*))EmptyObserver_1_OnCompleted_m3466341620_gshared)(__this, method)
// System.Void UniRx.InternalUtil.EmptyObserver`1<UniRx.CollectionAddEvent`1<UniRx.Tuple`2<System.Object,System.Object>>>::OnError(System.Exception)
extern "C"  void EmptyObserver_1_OnError_m3141996577_gshared (EmptyObserver_1_t1358030376 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method);
#define EmptyObserver_1_OnError_m3141996577(__this, ___error0, method) ((  void (*) (EmptyObserver_1_t1358030376 *, Exception_t1967233988 *, const MethodInfo*))EmptyObserver_1_OnError_m3141996577_gshared)(__this, ___error0, method)
// System.Void UniRx.InternalUtil.EmptyObserver`1<UniRx.CollectionAddEvent`1<UniRx.Tuple`2<System.Object,System.Object>>>::OnNext(T)
extern "C"  void EmptyObserver_1_OnNext_m1004755218_gshared (EmptyObserver_1_t1358030376 * __this, CollectionAddEvent_1_t1948677386  ___value0, const MethodInfo* method);
#define EmptyObserver_1_OnNext_m1004755218(__this, ___value0, method) ((  void (*) (EmptyObserver_1_t1358030376 *, CollectionAddEvent_1_t1948677386 , const MethodInfo*))EmptyObserver_1_OnNext_m1004755218_gshared)(__this, ___value0, method)
