﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Zenject.InitializableManager/<InitializableManager>c__AnonStorey18F
struct U3CInitializableManagerU3Ec__AnonStorey18F_t3916719836;
// ModestTree.Util.Tuple`2<System.Type,System.Int32>
struct Tuple_2_t3719549051;

#include "codegen/il2cpp-codegen.h"

// System.Void Zenject.InitializableManager/<InitializableManager>c__AnonStorey18F::.ctor()
extern "C"  void U3CInitializableManagerU3Ec__AnonStorey18F__ctor_m1147730205 (U3CInitializableManagerU3Ec__AnonStorey18F_t3916719836 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Zenject.InitializableManager/<InitializableManager>c__AnonStorey18F::<>m__2DA(ModestTree.Util.Tuple`2<System.Type,System.Int32>)
extern "C"  bool U3CInitializableManagerU3Ec__AnonStorey18F_U3CU3Em__2DA_m2508986657 (U3CInitializableManagerU3Ec__AnonStorey18F_t3916719836 * __this, Tuple_2_t3719549051 * ___x0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
