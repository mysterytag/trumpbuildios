﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.PlayFabClientAPI/UpdateSharedGroupDataRequestCallback
struct UpdateSharedGroupDataRequestCallback_t295913145;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;
// PlayFab.ClientModels.UpdateSharedGroupDataRequest
struct UpdateSharedGroupDataRequest_t1900307044;
// System.IAsyncResult
struct IAsyncResult_t537683269;
// System.AsyncCallback
struct AsyncCallback_t1363551830;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_IntPtr676692020.h"
#include "mscorlib_System_String968488902.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_UpdateShare1900307044.h"
#include "mscorlib_System_AsyncCallback1363551830.h"

// System.Void PlayFab.PlayFabClientAPI/UpdateSharedGroupDataRequestCallback::.ctor(System.Object,System.IntPtr)
extern "C"  void UpdateSharedGroupDataRequestCallback__ctor_m728925992 (UpdateSharedGroupDataRequestCallback_t295913145 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/UpdateSharedGroupDataRequestCallback::Invoke(System.String,System.Int32,PlayFab.ClientModels.UpdateSharedGroupDataRequest,System.Object)
extern "C"  void UpdateSharedGroupDataRequestCallback_Invoke_m2083528383 (UpdateSharedGroupDataRequestCallback_t295913145 * __this, String_t* ___urlPath0, int32_t ___callId1, UpdateSharedGroupDataRequest_t1900307044 * ___request2, Il2CppObject * ___customData3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
extern "C" void pinvoke_delegate_wrapper_UpdateSharedGroupDataRequestCallback_t295913145(Il2CppObject* delegate, String_t* ___urlPath0, int32_t ___callId1, UpdateSharedGroupDataRequest_t1900307044 * ___request2, Il2CppObject * ___customData3);
// System.IAsyncResult PlayFab.PlayFabClientAPI/UpdateSharedGroupDataRequestCallback::BeginInvoke(System.String,System.Int32,PlayFab.ClientModels.UpdateSharedGroupDataRequest,System.Object,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * UpdateSharedGroupDataRequestCallback_BeginInvoke_m4091591320 (UpdateSharedGroupDataRequestCallback_t295913145 * __this, String_t* ___urlPath0, int32_t ___callId1, UpdateSharedGroupDataRequest_t1900307044 * ___request2, Il2CppObject * ___customData3, AsyncCallback_t1363551830 * ___callback4, Il2CppObject * ___object5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/UpdateSharedGroupDataRequestCallback::EndInvoke(System.IAsyncResult)
extern "C"  void UpdateSharedGroupDataRequestCallback_EndInvoke_m557305144 (UpdateSharedGroupDataRequestCallback_t295913145 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
