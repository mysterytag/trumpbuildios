﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Type
struct Type_t;
// System.Collections.Generic.IDictionary`2<System.String,LitJson.PropertyMetadata>
struct IDictionary_2_t2460870484;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_LitJson_ObjectMetadata2608047315.h"
#include "mscorlib_System_Type2779229935.h"

// System.Type LitJson.ObjectMetadata::get_ElementType()
extern "C"  Type_t * ObjectMetadata_get_ElementType_m2977552111 (ObjectMetadata_t2608047315 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LitJson.ObjectMetadata::set_ElementType(System.Type)
extern "C"  void ObjectMetadata_set_ElementType_m970729348 (ObjectMetadata_t2608047315 * __this, Type_t * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean LitJson.ObjectMetadata::get_IsDictionary()
extern "C"  bool ObjectMetadata_get_IsDictionary_m3905358179 (ObjectMetadata_t2608047315 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LitJson.ObjectMetadata::set_IsDictionary(System.Boolean)
extern "C"  void ObjectMetadata_set_IsDictionary_m2325795162 (ObjectMetadata_t2608047315 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.IDictionary`2<System.String,LitJson.PropertyMetadata> LitJson.ObjectMetadata::get_Properties()
extern "C"  Il2CppObject* ObjectMetadata_get_Properties_m176148938 (ObjectMetadata_t2608047315 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LitJson.ObjectMetadata::set_Properties(System.Collections.Generic.IDictionary`2<System.String,LitJson.PropertyMetadata>)
extern "C"  void ObjectMetadata_set_Properties_m2617118337 (ObjectMetadata_t2608047315 * __this, Il2CppObject* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
