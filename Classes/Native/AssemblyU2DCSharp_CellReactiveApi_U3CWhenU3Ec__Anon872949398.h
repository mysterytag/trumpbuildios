﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// ICell`1<System.Boolean>
struct ICell_1_t1762636318;
// System.Func`2<System.Boolean,System.Boolean>
struct Func_2_t1008118516;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CellReactiveApi/<When>c__AnonStorey10A`1<System.Boolean>
struct  U3CWhenU3Ec__AnonStorey10A_1_t872949398  : public Il2CppObject
{
public:
	// ICell`1<T> CellReactiveApi/<When>c__AnonStorey10A`1::cell
	Il2CppObject* ___cell_0;
	// System.Func`2<T,System.Boolean> CellReactiveApi/<When>c__AnonStorey10A`1::filter
	Func_2_t1008118516 * ___filter_1;

public:
	inline static int32_t get_offset_of_cell_0() { return static_cast<int32_t>(offsetof(U3CWhenU3Ec__AnonStorey10A_1_t872949398, ___cell_0)); }
	inline Il2CppObject* get_cell_0() const { return ___cell_0; }
	inline Il2CppObject** get_address_of_cell_0() { return &___cell_0; }
	inline void set_cell_0(Il2CppObject* value)
	{
		___cell_0 = value;
		Il2CppCodeGenWriteBarrier(&___cell_0, value);
	}

	inline static int32_t get_offset_of_filter_1() { return static_cast<int32_t>(offsetof(U3CWhenU3Ec__AnonStorey10A_1_t872949398, ___filter_1)); }
	inline Func_2_t1008118516 * get_filter_1() const { return ___filter_1; }
	inline Func_2_t1008118516 ** get_address_of_filter_1() { return &___filter_1; }
	inline void set_filter_1(Func_2_t1008118516 * value)
	{
		___filter_1 = value;
		Il2CppCodeGenWriteBarrier(&___filter_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
