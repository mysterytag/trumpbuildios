﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.EqualityComparer`1/DefaultComparer<GameAnalyticsSDK.Settings/HelpTypes>
struct DefaultComparer_t2629309275;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_GameAnalyticsSDK_Settings_HelpTy3291355544.h"

// System.Void System.Collections.Generic.EqualityComparer`1/DefaultComparer<GameAnalyticsSDK.Settings/HelpTypes>::.ctor()
extern "C"  void DefaultComparer__ctor_m3918567794_gshared (DefaultComparer_t2629309275 * __this, const MethodInfo* method);
#define DefaultComparer__ctor_m3918567794(__this, method) ((  void (*) (DefaultComparer_t2629309275 *, const MethodInfo*))DefaultComparer__ctor_m3918567794_gshared)(__this, method)
// System.Int32 System.Collections.Generic.EqualityComparer`1/DefaultComparer<GameAnalyticsSDK.Settings/HelpTypes>::GetHashCode(T)
extern "C"  int32_t DefaultComparer_GetHashCode_m2012083705_gshared (DefaultComparer_t2629309275 * __this, int32_t ___obj0, const MethodInfo* method);
#define DefaultComparer_GetHashCode_m2012083705(__this, ___obj0, method) ((  int32_t (*) (DefaultComparer_t2629309275 *, int32_t, const MethodInfo*))DefaultComparer_GetHashCode_m2012083705_gshared)(__this, ___obj0, method)
// System.Boolean System.Collections.Generic.EqualityComparer`1/DefaultComparer<GameAnalyticsSDK.Settings/HelpTypes>::Equals(T,T)
extern "C"  bool DefaultComparer_Equals_m3256072515_gshared (DefaultComparer_t2629309275 * __this, int32_t ___x0, int32_t ___y1, const MethodInfo* method);
#define DefaultComparer_Equals_m3256072515(__this, ___x0, ___y1, method) ((  bool (*) (DefaultComparer_t2629309275 *, int32_t, int32_t, const MethodInfo*))DefaultComparer_Equals_m3256072515_gshared)(__this, ___x0, ___y1, method)
