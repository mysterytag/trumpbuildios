﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Facebook.Unity.GroupJoinResult
struct GroupJoinResult_t4173281157;
// Facebook.Unity.ResultContainer
struct ResultContainer_t79372963;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Facebook_Unity_ResultContainer79372963.h"

// System.Void Facebook.Unity.GroupJoinResult::.ctor(Facebook.Unity.ResultContainer)
extern "C"  void GroupJoinResult__ctor_m511053491 (GroupJoinResult_t4173281157 * __this, ResultContainer_t79372963 * ___resultContainer0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
