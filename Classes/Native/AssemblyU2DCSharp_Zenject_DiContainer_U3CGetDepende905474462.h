﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Type
struct Type_t;
// System.Collections.Generic.IEnumerator`1<Zenject.InjectableInfo>
struct IEnumerator_1_t2630816222;
// Zenject.InjectableInfo
struct InjectableInfo_t1147709774;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.DiContainer/<GetDependencyContracts>c__Iterator4C
struct  U3CGetDependencyContractsU3Ec__Iterator4C_t905474462  : public Il2CppObject
{
public:
	// System.Type Zenject.DiContainer/<GetDependencyContracts>c__Iterator4C::contract
	Type_t * ___contract_0;
	// System.Collections.Generic.IEnumerator`1<Zenject.InjectableInfo> Zenject.DiContainer/<GetDependencyContracts>c__Iterator4C::<$s_288>__0
	Il2CppObject* ___U3CU24s_288U3E__0_1;
	// Zenject.InjectableInfo Zenject.DiContainer/<GetDependencyContracts>c__Iterator4C::<injectMember>__1
	InjectableInfo_t1147709774 * ___U3CinjectMemberU3E__1_2;
	// System.Int32 Zenject.DiContainer/<GetDependencyContracts>c__Iterator4C::$PC
	int32_t ___U24PC_3;
	// System.Type Zenject.DiContainer/<GetDependencyContracts>c__Iterator4C::$current
	Type_t * ___U24current_4;
	// System.Type Zenject.DiContainer/<GetDependencyContracts>c__Iterator4C::<$>contract
	Type_t * ___U3CU24U3Econtract_5;

public:
	inline static int32_t get_offset_of_contract_0() { return static_cast<int32_t>(offsetof(U3CGetDependencyContractsU3Ec__Iterator4C_t905474462, ___contract_0)); }
	inline Type_t * get_contract_0() const { return ___contract_0; }
	inline Type_t ** get_address_of_contract_0() { return &___contract_0; }
	inline void set_contract_0(Type_t * value)
	{
		___contract_0 = value;
		Il2CppCodeGenWriteBarrier(&___contract_0, value);
	}

	inline static int32_t get_offset_of_U3CU24s_288U3E__0_1() { return static_cast<int32_t>(offsetof(U3CGetDependencyContractsU3Ec__Iterator4C_t905474462, ___U3CU24s_288U3E__0_1)); }
	inline Il2CppObject* get_U3CU24s_288U3E__0_1() const { return ___U3CU24s_288U3E__0_1; }
	inline Il2CppObject** get_address_of_U3CU24s_288U3E__0_1() { return &___U3CU24s_288U3E__0_1; }
	inline void set_U3CU24s_288U3E__0_1(Il2CppObject* value)
	{
		___U3CU24s_288U3E__0_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU24s_288U3E__0_1, value);
	}

	inline static int32_t get_offset_of_U3CinjectMemberU3E__1_2() { return static_cast<int32_t>(offsetof(U3CGetDependencyContractsU3Ec__Iterator4C_t905474462, ___U3CinjectMemberU3E__1_2)); }
	inline InjectableInfo_t1147709774 * get_U3CinjectMemberU3E__1_2() const { return ___U3CinjectMemberU3E__1_2; }
	inline InjectableInfo_t1147709774 ** get_address_of_U3CinjectMemberU3E__1_2() { return &___U3CinjectMemberU3E__1_2; }
	inline void set_U3CinjectMemberU3E__1_2(InjectableInfo_t1147709774 * value)
	{
		___U3CinjectMemberU3E__1_2 = value;
		Il2CppCodeGenWriteBarrier(&___U3CinjectMemberU3E__1_2, value);
	}

	inline static int32_t get_offset_of_U24PC_3() { return static_cast<int32_t>(offsetof(U3CGetDependencyContractsU3Ec__Iterator4C_t905474462, ___U24PC_3)); }
	inline int32_t get_U24PC_3() const { return ___U24PC_3; }
	inline int32_t* get_address_of_U24PC_3() { return &___U24PC_3; }
	inline void set_U24PC_3(int32_t value)
	{
		___U24PC_3 = value;
	}

	inline static int32_t get_offset_of_U24current_4() { return static_cast<int32_t>(offsetof(U3CGetDependencyContractsU3Ec__Iterator4C_t905474462, ___U24current_4)); }
	inline Type_t * get_U24current_4() const { return ___U24current_4; }
	inline Type_t ** get_address_of_U24current_4() { return &___U24current_4; }
	inline void set_U24current_4(Type_t * value)
	{
		___U24current_4 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_4, value);
	}

	inline static int32_t get_offset_of_U3CU24U3Econtract_5() { return static_cast<int32_t>(offsetof(U3CGetDependencyContractsU3Ec__Iterator4C_t905474462, ___U3CU24U3Econtract_5)); }
	inline Type_t * get_U3CU24U3Econtract_5() const { return ___U3CU24U3Econtract_5; }
	inline Type_t ** get_address_of_U3CU24U3Econtract_5() { return &___U3CU24U3Econtract_5; }
	inline void set_U3CU24U3Econtract_5(Type_t * value)
	{
		___U3CU24U3Econtract_5 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU24U3Econtract_5, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
