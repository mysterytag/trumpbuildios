﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UniRx.IObservable`1<System.Object>
struct IObservable_1_t595904784;
// System.Action`1<System.Object>
struct Action_1_t985559125;
// System.Action`2<System.Object,System.Int32>
struct Action_2_t1820800989;

#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Oper1622431009.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Operators.ForEachAsyncObservable`1<System.Object>
struct  ForEachAsyncObservable_1_t3176723348  : public OperatorObservableBase_1_t1622431009
{
public:
	// UniRx.IObservable`1<T> UniRx.Operators.ForEachAsyncObservable`1::source
	Il2CppObject* ___source_1;
	// System.Action`1<T> UniRx.Operators.ForEachAsyncObservable`1::onNext
	Action_1_t985559125 * ___onNext_2;
	// System.Action`2<T,System.Int32> UniRx.Operators.ForEachAsyncObservable`1::onNextWithIndex
	Action_2_t1820800989 * ___onNextWithIndex_3;

public:
	inline static int32_t get_offset_of_source_1() { return static_cast<int32_t>(offsetof(ForEachAsyncObservable_1_t3176723348, ___source_1)); }
	inline Il2CppObject* get_source_1() const { return ___source_1; }
	inline Il2CppObject** get_address_of_source_1() { return &___source_1; }
	inline void set_source_1(Il2CppObject* value)
	{
		___source_1 = value;
		Il2CppCodeGenWriteBarrier(&___source_1, value);
	}

	inline static int32_t get_offset_of_onNext_2() { return static_cast<int32_t>(offsetof(ForEachAsyncObservable_1_t3176723348, ___onNext_2)); }
	inline Action_1_t985559125 * get_onNext_2() const { return ___onNext_2; }
	inline Action_1_t985559125 ** get_address_of_onNext_2() { return &___onNext_2; }
	inline void set_onNext_2(Action_1_t985559125 * value)
	{
		___onNext_2 = value;
		Il2CppCodeGenWriteBarrier(&___onNext_2, value);
	}

	inline static int32_t get_offset_of_onNextWithIndex_3() { return static_cast<int32_t>(offsetof(ForEachAsyncObservable_1_t3176723348, ___onNextWithIndex_3)); }
	inline Action_2_t1820800989 * get_onNextWithIndex_3() const { return ___onNextWithIndex_3; }
	inline Action_2_t1820800989 ** get_address_of_onNextWithIndex_3() { return &___onNextWithIndex_3; }
	inline void set_onNextWithIndex_3(Action_2_t1820800989 * value)
	{
		___onNextWithIndex_3 = value;
		Il2CppCodeGenWriteBarrier(&___onNextWithIndex_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
