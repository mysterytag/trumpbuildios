﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Stream`1<UniRx.CollectionAddEvent`1<System.Object>>
struct Stream_1_t1107661009;
// System.IDisposable
struct IDisposable_t1628921374;
// System.Action`1<UniRx.CollectionAddEvent`1<System.Object>>
struct Action_1_t2564974692;
// System.Action
struct Action_t437523947;
// IStream`1<UniRx.CollectionAddEvent`1<System.Object>>
struct IStream_1_t2969212978;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_CollectionAddE2416521987.h"
#include "AssemblyU2DCSharp_Priority3194150340.h"
#include "System_Core_System_Action437523947.h"

// System.Void Stream`1<UniRx.CollectionAddEvent`1<System.Object>>::.ctor()
extern "C"  void Stream_1__ctor_m3235287902_gshared (Stream_1_t1107661009 * __this, const MethodInfo* method);
#define Stream_1__ctor_m3235287902(__this, method) ((  void (*) (Stream_1_t1107661009 *, const MethodInfo*))Stream_1__ctor_m3235287902_gshared)(__this, method)
// System.Void Stream`1<UniRx.CollectionAddEvent`1<System.Object>>::Send(T)
extern "C"  void Stream_1_Send_m1764505456_gshared (Stream_1_t1107661009 * __this, CollectionAddEvent_1_t2416521987  ___value0, const MethodInfo* method);
#define Stream_1_Send_m1764505456(__this, ___value0, method) ((  void (*) (Stream_1_t1107661009 *, CollectionAddEvent_1_t2416521987 , const MethodInfo*))Stream_1_Send_m1764505456_gshared)(__this, ___value0, method)
// System.Void Stream`1<UniRx.CollectionAddEvent`1<System.Object>>::SendInTransaction(T,System.Int32)
extern "C"  void Stream_1_SendInTransaction_m1512275718_gshared (Stream_1_t1107661009 * __this, CollectionAddEvent_1_t2416521987  ___value0, int32_t ___p1, const MethodInfo* method);
#define Stream_1_SendInTransaction_m1512275718(__this, ___value0, ___p1, method) ((  void (*) (Stream_1_t1107661009 *, CollectionAddEvent_1_t2416521987 , int32_t, const MethodInfo*))Stream_1_SendInTransaction_m1512275718_gshared)(__this, ___value0, ___p1, method)
// System.IDisposable Stream`1<UniRx.CollectionAddEvent`1<System.Object>>::Listen(System.Action`1<T>,Priority)
extern "C"  Il2CppObject * Stream_1_Listen_m1890769608_gshared (Stream_1_t1107661009 * __this, Action_1_t2564974692 * ___action0, int32_t ___priority1, const MethodInfo* method);
#define Stream_1_Listen_m1890769608(__this, ___action0, ___priority1, method) ((  Il2CppObject * (*) (Stream_1_t1107661009 *, Action_1_t2564974692 *, int32_t, const MethodInfo*))Stream_1_Listen_m1890769608_gshared)(__this, ___action0, ___priority1, method)
// System.IDisposable Stream`1<UniRx.CollectionAddEvent`1<System.Object>>::Listen(System.Action,Priority)
extern "C"  Il2CppObject * Stream_1_Listen_m3411183967_gshared (Stream_1_t1107661009 * __this, Action_t437523947 * ___action0, int32_t ___p1, const MethodInfo* method);
#define Stream_1_Listen_m3411183967(__this, ___action0, ___p1, method) ((  Il2CppObject * (*) (Stream_1_t1107661009 *, Action_t437523947 *, int32_t, const MethodInfo*))Stream_1_Listen_m3411183967_gshared)(__this, ___action0, ___p1, method)
// System.Void Stream`1<UniRx.CollectionAddEvent`1<System.Object>>::Dispose()
extern "C"  void Stream_1_Dispose_m3748601883_gshared (Stream_1_t1107661009 * __this, const MethodInfo* method);
#define Stream_1_Dispose_m3748601883(__this, method) ((  void (*) (Stream_1_t1107661009 *, const MethodInfo*))Stream_1_Dispose_m3748601883_gshared)(__this, method)
// System.Void Stream`1<UniRx.CollectionAddEvent`1<System.Object>>::SetInputStream(IStream`1<T>)
extern "C"  void Stream_1_SetInputStream_m453976930_gshared (Stream_1_t1107661009 * __this, Il2CppObject* ___stream0, const MethodInfo* method);
#define Stream_1_SetInputStream_m453976930(__this, ___stream0, method) ((  void (*) (Stream_1_t1107661009 *, Il2CppObject*, const MethodInfo*))Stream_1_SetInputStream_m453976930_gshared)(__this, ___stream0, method)
// System.Void Stream`1<UniRx.CollectionAddEvent`1<System.Object>>::ClearInputStream()
extern "C"  void Stream_1_ClearInputStream_m1610565315_gshared (Stream_1_t1107661009 * __this, const MethodInfo* method);
#define Stream_1_ClearInputStream_m1610565315(__this, method) ((  void (*) (Stream_1_t1107661009 *, const MethodInfo*))Stream_1_ClearInputStream_m1610565315_gshared)(__this, method)
// System.Void Stream`1<UniRx.CollectionAddEvent`1<System.Object>>::TransactionIterationFinished()
extern "C"  void Stream_1_TransactionIterationFinished_m477624855_gshared (Stream_1_t1107661009 * __this, const MethodInfo* method);
#define Stream_1_TransactionIterationFinished_m477624855(__this, method) ((  void (*) (Stream_1_t1107661009 *, const MethodInfo*))Stream_1_TransactionIterationFinished_m477624855_gshared)(__this, method)
// System.Void Stream`1<UniRx.CollectionAddEvent`1<System.Object>>::Unpack(System.Int32)
extern "C"  void Stream_1_Unpack_m1846888937_gshared (Stream_1_t1107661009 * __this, int32_t ___p0, const MethodInfo* method);
#define Stream_1_Unpack_m1846888937(__this, ___p0, method) ((  void (*) (Stream_1_t1107661009 *, int32_t, const MethodInfo*))Stream_1_Unpack_m1846888937_gshared)(__this, ___p0, method)
// System.Void Stream`1<UniRx.CollectionAddEvent`1<System.Object>>::<SetInputStream>m__1BA(T)
extern "C"  void Stream_1_U3CSetInputStreamU3Em__1BA_m4229248635_gshared (Stream_1_t1107661009 * __this, CollectionAddEvent_1_t2416521987  ___val0, const MethodInfo* method);
#define Stream_1_U3CSetInputStreamU3Em__1BA_m4229248635(__this, ___val0, method) ((  void (*) (Stream_1_t1107661009 *, CollectionAddEvent_1_t2416521987 , const MethodInfo*))Stream_1_U3CSetInputStreamU3Em__1BA_m4229248635_gshared)(__this, ___val0, method)
