﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.PlayFabClientAPI/ConsumePSNEntitlementsRequestCallback
struct ConsumePSNEntitlementsRequestCallback_t909350751;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;
// PlayFab.ClientModels.ConsumePSNEntitlementsRequest
struct ConsumePSNEntitlementsRequest_t3123362058;
// System.IAsyncResult
struct IAsyncResult_t537683269;
// System.AsyncCallback
struct AsyncCallback_t1363551830;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_IntPtr676692020.h"
#include "mscorlib_System_String968488902.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_ConsumePSNE3123362058.h"
#include "mscorlib_System_AsyncCallback1363551830.h"

// System.Void PlayFab.PlayFabClientAPI/ConsumePSNEntitlementsRequestCallback::.ctor(System.Object,System.IntPtr)
extern "C"  void ConsumePSNEntitlementsRequestCallback__ctor_m3640378478 (ConsumePSNEntitlementsRequestCallback_t909350751 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/ConsumePSNEntitlementsRequestCallback::Invoke(System.String,System.Int32,PlayFab.ClientModels.ConsumePSNEntitlementsRequest,System.Object)
extern "C"  void ConsumePSNEntitlementsRequestCallback_Invoke_m997547419 (ConsumePSNEntitlementsRequestCallback_t909350751 * __this, String_t* ___urlPath0, int32_t ___callId1, ConsumePSNEntitlementsRequest_t3123362058 * ___request2, Il2CppObject * ___customData3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
extern "C" void pinvoke_delegate_wrapper_ConsumePSNEntitlementsRequestCallback_t909350751(Il2CppObject* delegate, String_t* ___urlPath0, int32_t ___callId1, ConsumePSNEntitlementsRequest_t3123362058 * ___request2, Il2CppObject * ___customData3);
// System.IAsyncResult PlayFab.PlayFabClientAPI/ConsumePSNEntitlementsRequestCallback::BeginInvoke(System.String,System.Int32,PlayFab.ClientModels.ConsumePSNEntitlementsRequest,System.Object,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * ConsumePSNEntitlementsRequestCallback_BeginInvoke_m2951160702 (ConsumePSNEntitlementsRequestCallback_t909350751 * __this, String_t* ___urlPath0, int32_t ___callId1, ConsumePSNEntitlementsRequest_t3123362058 * ___request2, Il2CppObject * ___customData3, AsyncCallback_t1363551830 * ___callback4, Il2CppObject * ___object5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/ConsumePSNEntitlementsRequestCallback::EndInvoke(System.IAsyncResult)
extern "C"  void ConsumePSNEntitlementsRequestCallback_EndInvoke_m315530622 (ConsumePSNEntitlementsRequestCallback_t909350751 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
