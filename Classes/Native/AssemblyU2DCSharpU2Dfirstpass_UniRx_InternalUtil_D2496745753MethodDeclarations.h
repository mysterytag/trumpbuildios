﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.InternalUtil.DisposedObserver`1<UniRx.DictionaryRemoveEvent`2<System.Object,System.Object>>
struct DisposedObserver_1_t2496745753;
// System.Exception
struct Exception_t1967233988;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Exception1967233988.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_DictionaryRemo3322288291.h"

// System.Void UniRx.InternalUtil.DisposedObserver`1<UniRx.DictionaryRemoveEvent`2<System.Object,System.Object>>::.ctor()
extern "C"  void DisposedObserver_1__ctor_m1620369314_gshared (DisposedObserver_1_t2496745753 * __this, const MethodInfo* method);
#define DisposedObserver_1__ctor_m1620369314(__this, method) ((  void (*) (DisposedObserver_1_t2496745753 *, const MethodInfo*))DisposedObserver_1__ctor_m1620369314_gshared)(__this, method)
// System.Void UniRx.InternalUtil.DisposedObserver`1<UniRx.DictionaryRemoveEvent`2<System.Object,System.Object>>::.cctor()
extern "C"  void DisposedObserver_1__cctor_m2504712267_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define DisposedObserver_1__cctor_m2504712267(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))DisposedObserver_1__cctor_m2504712267_gshared)(__this /* static, unused */, method)
// System.Void UniRx.InternalUtil.DisposedObserver`1<UniRx.DictionaryRemoveEvent`2<System.Object,System.Object>>::OnCompleted()
extern "C"  void DisposedObserver_1_OnCompleted_m432951724_gshared (DisposedObserver_1_t2496745753 * __this, const MethodInfo* method);
#define DisposedObserver_1_OnCompleted_m432951724(__this, method) ((  void (*) (DisposedObserver_1_t2496745753 *, const MethodInfo*))DisposedObserver_1_OnCompleted_m432951724_gshared)(__this, method)
// System.Void UniRx.InternalUtil.DisposedObserver`1<UniRx.DictionaryRemoveEvent`2<System.Object,System.Object>>::OnError(System.Exception)
extern "C"  void DisposedObserver_1_OnError_m2794447065_gshared (DisposedObserver_1_t2496745753 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method);
#define DisposedObserver_1_OnError_m2794447065(__this, ___error0, method) ((  void (*) (DisposedObserver_1_t2496745753 *, Exception_t1967233988 *, const MethodInfo*))DisposedObserver_1_OnError_m2794447065_gshared)(__this, ___error0, method)
// System.Void UniRx.InternalUtil.DisposedObserver`1<UniRx.DictionaryRemoveEvent`2<System.Object,System.Object>>::OnNext(T)
extern "C"  void DisposedObserver_1_OnNext_m932485578_gshared (DisposedObserver_1_t2496745753 * __this, DictionaryRemoveEvent_2_t3322288291  ___value0, const MethodInfo* method);
#define DisposedObserver_1_OnNext_m932485578(__this, ___value0, method) ((  void (*) (DisposedObserver_1_t2496745753 *, DictionaryRemoveEvent_2_t3322288291 , const MethodInfo*))DisposedObserver_1_OnNext_m932485578_gshared)(__this, ___value0, method)
