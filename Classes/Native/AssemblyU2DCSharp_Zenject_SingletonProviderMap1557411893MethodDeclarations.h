﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Zenject.SingletonProviderMap
struct SingletonProviderMap_t1557411893;
// Zenject.DiContainer
struct DiContainer_t2383114449;
// System.Collections.Generic.IEnumerable`1<Zenject.SingletonLazyCreator>
struct IEnumerable_1_t1339471254;
// Zenject.SingletonId
struct SingletonId_t1838183899;
// Zenject.SingletonLazyCreator
struct SingletonLazyCreator_t2762284194;
// Zenject.ProviderBase
struct ProviderBase_t1627494391;
// System.String
struct String_t;
// System.Type
struct Type_t;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Zenject_DiContainer2383114449.h"
#include "AssemblyU2DCSharp_Zenject_SingletonId1838183899.h"
#include "mscorlib_System_String968488902.h"
#include "mscorlib_System_Type2779229935.h"
#include "mscorlib_System_Object837106420.h"

// System.Void Zenject.SingletonProviderMap::.ctor(Zenject.DiContainer)
extern "C"  void SingletonProviderMap__ctor_m1613265689 (SingletonProviderMap_t1557411893 * __this, DiContainer_t2383114449 * ___container0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.IEnumerable`1<Zenject.SingletonLazyCreator> Zenject.SingletonProviderMap::get_Creators()
extern "C"  Il2CppObject* SingletonProviderMap_get_Creators_m919684460 (SingletonProviderMap_t1557411893 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Zenject.SingletonProviderMap::RemoveCreator(Zenject.SingletonId)
extern "C"  void SingletonProviderMap_RemoveCreator_m1625230441 (SingletonProviderMap_t1557411893 * __this, SingletonId_t1838183899 * ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Zenject.SingletonLazyCreator Zenject.SingletonProviderMap::AddCreator(Zenject.SingletonId)
extern "C"  SingletonLazyCreator_t2762284194 * SingletonProviderMap_AddCreator_m3456770503 (SingletonProviderMap_t1557411893 * __this, SingletonId_t1838183899 * ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Zenject.ProviderBase Zenject.SingletonProviderMap::CreateProviderFromType(System.String,System.Type)
extern "C"  ProviderBase_t1627494391 * SingletonProviderMap_CreateProviderFromType_m1979550400 (SingletonProviderMap_t1557411893 * __this, String_t* ___identifier0, Type_t * ___concreteType1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Zenject.ProviderBase Zenject.SingletonProviderMap::CreateProviderFromInstance(System.String,System.Type,System.Object)
extern "C"  ProviderBase_t1627494391 * SingletonProviderMap_CreateProviderFromInstance_m1303240595 (SingletonProviderMap_t1557411893 * __this, String_t* ___identifier0, Type_t * ___concreteType1, Il2CppObject * ___instance2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
