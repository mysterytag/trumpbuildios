﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Utils2/<TakeAllButLast>c__Iterator29`1<System.Object>
struct U3CTakeAllButLastU3Ec__Iterator29_1_t3815074525;
// System.Object
struct Il2CppObject;
// System.Collections.IEnumerator
struct IEnumerator_t287207039;
// System.Collections.Generic.IEnumerator`1<System.Object>
struct IEnumerator_1_t2320212868;

#include "codegen/il2cpp-codegen.h"

// System.Void Utils2/<TakeAllButLast>c__Iterator29`1<System.Object>::.ctor()
extern "C"  void U3CTakeAllButLastU3Ec__Iterator29_1__ctor_m1968373061_gshared (U3CTakeAllButLastU3Ec__Iterator29_1_t3815074525 * __this, const MethodInfo* method);
#define U3CTakeAllButLastU3Ec__Iterator29_1__ctor_m1968373061(__this, method) ((  void (*) (U3CTakeAllButLastU3Ec__Iterator29_1_t3815074525 *, const MethodInfo*))U3CTakeAllButLastU3Ec__Iterator29_1__ctor_m1968373061_gshared)(__this, method)
// T Utils2/<TakeAllButLast>c__Iterator29`1<System.Object>::System.Collections.Generic.IEnumerator<T>.get_Current()
extern "C"  Il2CppObject * U3CTakeAllButLastU3Ec__Iterator29_1_System_Collections_Generic_IEnumeratorU3CTU3E_get_Current_m256217868_gshared (U3CTakeAllButLastU3Ec__Iterator29_1_t3815074525 * __this, const MethodInfo* method);
#define U3CTakeAllButLastU3Ec__Iterator29_1_System_Collections_Generic_IEnumeratorU3CTU3E_get_Current_m256217868(__this, method) ((  Il2CppObject * (*) (U3CTakeAllButLastU3Ec__Iterator29_1_t3815074525 *, const MethodInfo*))U3CTakeAllButLastU3Ec__Iterator29_1_System_Collections_Generic_IEnumeratorU3CTU3E_get_Current_m256217868_gshared)(__this, method)
// System.Object Utils2/<TakeAllButLast>c__Iterator29`1<System.Object>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CTakeAllButLastU3Ec__Iterator29_1_System_Collections_IEnumerator_get_Current_m224952961_gshared (U3CTakeAllButLastU3Ec__Iterator29_1_t3815074525 * __this, const MethodInfo* method);
#define U3CTakeAllButLastU3Ec__Iterator29_1_System_Collections_IEnumerator_get_Current_m224952961(__this, method) ((  Il2CppObject * (*) (U3CTakeAllButLastU3Ec__Iterator29_1_t3815074525 *, const MethodInfo*))U3CTakeAllButLastU3Ec__Iterator29_1_System_Collections_IEnumerator_get_Current_m224952961_gshared)(__this, method)
// System.Collections.IEnumerator Utils2/<TakeAllButLast>c__Iterator29`1<System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * U3CTakeAllButLastU3Ec__Iterator29_1_System_Collections_IEnumerable_GetEnumerator_m2039414908_gshared (U3CTakeAllButLastU3Ec__Iterator29_1_t3815074525 * __this, const MethodInfo* method);
#define U3CTakeAllButLastU3Ec__Iterator29_1_System_Collections_IEnumerable_GetEnumerator_m2039414908(__this, method) ((  Il2CppObject * (*) (U3CTakeAllButLastU3Ec__Iterator29_1_t3815074525 *, const MethodInfo*))U3CTakeAllButLastU3Ec__Iterator29_1_System_Collections_IEnumerable_GetEnumerator_m2039414908_gshared)(__this, method)
// System.Collections.Generic.IEnumerator`1<T> Utils2/<TakeAllButLast>c__Iterator29`1<System.Object>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
extern "C"  Il2CppObject* U3CTakeAllButLastU3Ec__Iterator29_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m3024146191_gshared (U3CTakeAllButLastU3Ec__Iterator29_1_t3815074525 * __this, const MethodInfo* method);
#define U3CTakeAllButLastU3Ec__Iterator29_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m3024146191(__this, method) ((  Il2CppObject* (*) (U3CTakeAllButLastU3Ec__Iterator29_1_t3815074525 *, const MethodInfo*))U3CTakeAllButLastU3Ec__Iterator29_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m3024146191_gshared)(__this, method)
// System.Boolean Utils2/<TakeAllButLast>c__Iterator29`1<System.Object>::MoveNext()
extern "C"  bool U3CTakeAllButLastU3Ec__Iterator29_1_MoveNext_m4259806607_gshared (U3CTakeAllButLastU3Ec__Iterator29_1_t3815074525 * __this, const MethodInfo* method);
#define U3CTakeAllButLastU3Ec__Iterator29_1_MoveNext_m4259806607(__this, method) ((  bool (*) (U3CTakeAllButLastU3Ec__Iterator29_1_t3815074525 *, const MethodInfo*))U3CTakeAllButLastU3Ec__Iterator29_1_MoveNext_m4259806607_gshared)(__this, method)
// System.Void Utils2/<TakeAllButLast>c__Iterator29`1<System.Object>::Dispose()
extern "C"  void U3CTakeAllButLastU3Ec__Iterator29_1_Dispose_m1719184450_gshared (U3CTakeAllButLastU3Ec__Iterator29_1_t3815074525 * __this, const MethodInfo* method);
#define U3CTakeAllButLastU3Ec__Iterator29_1_Dispose_m1719184450(__this, method) ((  void (*) (U3CTakeAllButLastU3Ec__Iterator29_1_t3815074525 *, const MethodInfo*))U3CTakeAllButLastU3Ec__Iterator29_1_Dispose_m1719184450_gshared)(__this, method)
// System.Void Utils2/<TakeAllButLast>c__Iterator29`1<System.Object>::Reset()
extern "C"  void U3CTakeAllButLastU3Ec__Iterator29_1_Reset_m3909773298_gshared (U3CTakeAllButLastU3Ec__Iterator29_1_t3815074525 * __this, const MethodInfo* method);
#define U3CTakeAllButLastU3Ec__Iterator29_1_Reset_m3909773298(__this, method) ((  void (*) (U3CTakeAllButLastU3Ec__Iterator29_1_t3815074525 *, const MethodInfo*))U3CTakeAllButLastU3Ec__Iterator29_1_Reset_m3909773298_gshared)(__this, method)
