﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Array
struct Il2CppArray;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen3198846047.h"
#include "mscorlib_System_Array2840145358.h"
#include "AssemblyU2DCSharp_GameAnalyticsSDK_Settings_HelpTy3291355544.h"

// System.Void System.Array/InternalEnumerator`1<GameAnalyticsSDK.Settings/HelpTypes>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m1977182905_gshared (InternalEnumerator_1_t3198846047 * __this, Il2CppArray * ___array0, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m1977182905(__this, ___array0, method) ((  void (*) (InternalEnumerator_1_t3198846047 *, Il2CppArray *, const MethodInfo*))InternalEnumerator_1__ctor_m1977182905_gshared)(__this, ___array0, method)
// System.Void System.Array/InternalEnumerator`1<GameAnalyticsSDK.Settings/HelpTypes>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3029104199_gshared (InternalEnumerator_1_t3198846047 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3029104199(__this, method) ((  void (*) (InternalEnumerator_1_t3198846047 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3029104199_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<GameAnalyticsSDK.Settings/HelpTypes>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1810737917_gshared (InternalEnumerator_1_t3198846047 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1810737917(__this, method) ((  Il2CppObject * (*) (InternalEnumerator_1_t3198846047 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1810737917_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<GameAnalyticsSDK.Settings/HelpTypes>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m3386988496_gshared (InternalEnumerator_1_t3198846047 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m3386988496(__this, method) ((  void (*) (InternalEnumerator_1_t3198846047 *, const MethodInfo*))InternalEnumerator_1_Dispose_m3386988496_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<GameAnalyticsSDK.Settings/HelpTypes>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m420941431_gshared (InternalEnumerator_1_t3198846047 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m420941431(__this, method) ((  bool (*) (InternalEnumerator_1_t3198846047 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m420941431_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<GameAnalyticsSDK.Settings/HelpTypes>::get_Current()
extern "C"  int32_t InternalEnumerator_1_get_Current_m415576482_gshared (InternalEnumerator_1_t3198846047 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m415576482(__this, method) ((  int32_t (*) (InternalEnumerator_1_t3198846047 *, const MethodInfo*))InternalEnumerator_1_get_Current_m415576482_gshared)(__this, method)
