﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Action`1<GooglePlayGames.BasicApi.Nearby.InitializationStatus>
struct Action_1_t805070320;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t537683269;
// System.AsyncCallback
struct AsyncCallback_t1363551830;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_IntPtr676692020.h"
#include "AssemblyU2DCSharp_GooglePlayGames_BasicApi_Nearby_I656617615.h"
#include "mscorlib_System_AsyncCallback1363551830.h"

// System.Void System.Action`1<GooglePlayGames.BasicApi.Nearby.InitializationStatus>::.ctor(System.Object,System.IntPtr)
extern "C"  void Action_1__ctor_m2262397789_gshared (Action_1_t805070320 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method);
#define Action_1__ctor_m2262397789(__this, ___object0, ___method1, method) ((  void (*) (Action_1_t805070320 *, Il2CppObject *, IntPtr_t, const MethodInfo*))Action_1__ctor_m2262397789_gshared)(__this, ___object0, ___method1, method)
// System.Void System.Action`1<GooglePlayGames.BasicApi.Nearby.InitializationStatus>::Invoke(T)
extern "C"  void Action_1_Invoke_m2110401095_gshared (Action_1_t805070320 * __this, int32_t ___obj0, const MethodInfo* method);
#define Action_1_Invoke_m2110401095(__this, ___obj0, method) ((  void (*) (Action_1_t805070320 *, int32_t, const MethodInfo*))Action_1_Invoke_m2110401095_gshared)(__this, ___obj0, method)
// System.IAsyncResult System.Action`1<GooglePlayGames.BasicApi.Nearby.InitializationStatus>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * Action_1_BeginInvoke_m1004290204_gshared (Action_1_t805070320 * __this, int32_t ___obj0, AsyncCallback_t1363551830 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method);
#define Action_1_BeginInvoke_m1004290204(__this, ___obj0, ___callback1, ___object2, method) ((  Il2CppObject * (*) (Action_1_t805070320 *, int32_t, AsyncCallback_t1363551830 *, Il2CppObject *, const MethodInfo*))Action_1_BeginInvoke_m1004290204_gshared)(__this, ___obj0, ___callback1, ___object2, method)
// System.Void System.Action`1<GooglePlayGames.BasicApi.Nearby.InitializationStatus>::EndInvoke(System.IAsyncResult)
extern "C"  void Action_1_EndInvoke_m1660885997_gshared (Action_1_t805070320 * __this, Il2CppObject * ___result0, const MethodInfo* method);
#define Action_1_EndInvoke_m1660885997(__this, ___result0, method) ((  void (*) (Action_1_t805070320 *, Il2CppObject *, const MethodInfo*))Action_1_EndInvoke_m1660885997_gshared)(__this, ___result0, method)
