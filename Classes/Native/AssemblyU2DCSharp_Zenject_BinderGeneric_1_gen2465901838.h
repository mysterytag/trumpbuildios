﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "AssemblyU2DCSharp_Zenject_Binder3872662847.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.BinderGeneric`1<GameController>
struct  BinderGeneric_1_t2465901838  : public Binder_t3872662847
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
