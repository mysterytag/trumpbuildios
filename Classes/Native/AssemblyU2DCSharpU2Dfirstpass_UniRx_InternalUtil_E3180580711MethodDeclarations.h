﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.InternalUtil.EmptyObserver`1<UniRx.CountChangedStatus>
struct EmptyObserver_1_t3180580711;
// System.Exception
struct Exception_t1967233988;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Exception1967233988.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_CountChangedSt3771227721.h"

// System.Void UniRx.InternalUtil.EmptyObserver`1<UniRx.CountChangedStatus>::.ctor()
extern "C"  void EmptyObserver_1__ctor_m14799999_gshared (EmptyObserver_1_t3180580711 * __this, const MethodInfo* method);
#define EmptyObserver_1__ctor_m14799999(__this, method) ((  void (*) (EmptyObserver_1_t3180580711 *, const MethodInfo*))EmptyObserver_1__ctor_m14799999_gshared)(__this, method)
// System.Void UniRx.InternalUtil.EmptyObserver`1<UniRx.CountChangedStatus>::.cctor()
extern "C"  void EmptyObserver_1__cctor_m4271671054_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define EmptyObserver_1__cctor_m4271671054(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))EmptyObserver_1__cctor_m4271671054_gshared)(__this /* static, unused */, method)
// System.Void UniRx.InternalUtil.EmptyObserver`1<UniRx.CountChangedStatus>::OnCompleted()
extern "C"  void EmptyObserver_1_OnCompleted_m1817537737_gshared (EmptyObserver_1_t3180580711 * __this, const MethodInfo* method);
#define EmptyObserver_1_OnCompleted_m1817537737(__this, method) ((  void (*) (EmptyObserver_1_t3180580711 *, const MethodInfo*))EmptyObserver_1_OnCompleted_m1817537737_gshared)(__this, method)
// System.Void UniRx.InternalUtil.EmptyObserver`1<UniRx.CountChangedStatus>::OnError(System.Exception)
extern "C"  void EmptyObserver_1_OnError_m1025390710_gshared (EmptyObserver_1_t3180580711 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method);
#define EmptyObserver_1_OnError_m1025390710(__this, ___error0, method) ((  void (*) (EmptyObserver_1_t3180580711 *, Exception_t1967233988 *, const MethodInfo*))EmptyObserver_1_OnError_m1025390710_gshared)(__this, ___error0, method)
// System.Void UniRx.InternalUtil.EmptyObserver`1<UniRx.CountChangedStatus>::OnNext(T)
extern "C"  void EmptyObserver_1_OnNext_m4168600423_gshared (EmptyObserver_1_t3180580711 * __this, int32_t ___value0, const MethodInfo* method);
#define EmptyObserver_1_OnNext_m4168600423(__this, ___value0, method) ((  void (*) (EmptyObserver_1_t3180580711 *, int32_t, const MethodInfo*))EmptyObserver_1_OnNext_m4168600423_gshared)(__this, ___value0, method)
