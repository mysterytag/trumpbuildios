﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LocalyticsUnity.MiniJSON
struct  MiniJSON_t1258204565  : public Il2CppObject
{
public:

public:
};

struct MiniJSON_t1258204565_StaticFields
{
public:
	// System.Int32 LocalyticsUnity.MiniJSON::lastErrorIndex
	int32_t ___lastErrorIndex_13;
	// System.String LocalyticsUnity.MiniJSON::lastDecode
	String_t* ___lastDecode_14;
	// System.Boolean LocalyticsUnity.MiniJSON::_useGenericContainer
	bool ____useGenericContainer_15;

public:
	inline static int32_t get_offset_of_lastErrorIndex_13() { return static_cast<int32_t>(offsetof(MiniJSON_t1258204565_StaticFields, ___lastErrorIndex_13)); }
	inline int32_t get_lastErrorIndex_13() const { return ___lastErrorIndex_13; }
	inline int32_t* get_address_of_lastErrorIndex_13() { return &___lastErrorIndex_13; }
	inline void set_lastErrorIndex_13(int32_t value)
	{
		___lastErrorIndex_13 = value;
	}

	inline static int32_t get_offset_of_lastDecode_14() { return static_cast<int32_t>(offsetof(MiniJSON_t1258204565_StaticFields, ___lastDecode_14)); }
	inline String_t* get_lastDecode_14() const { return ___lastDecode_14; }
	inline String_t** get_address_of_lastDecode_14() { return &___lastDecode_14; }
	inline void set_lastDecode_14(String_t* value)
	{
		___lastDecode_14 = value;
		Il2CppCodeGenWriteBarrier(&___lastDecode_14, value);
	}

	inline static int32_t get_offset_of__useGenericContainer_15() { return static_cast<int32_t>(offsetof(MiniJSON_t1258204565_StaticFields, ____useGenericContainer_15)); }
	inline bool get__useGenericContainer_15() const { return ____useGenericContainer_15; }
	inline bool* get_address_of__useGenericContainer_15() { return &____useGenericContainer_15; }
	inline void set__useGenericContainer_15(bool value)
	{
		____useGenericContainer_15 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
