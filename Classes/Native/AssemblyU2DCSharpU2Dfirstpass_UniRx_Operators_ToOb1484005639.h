﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UniRx.Operators.ToObservableObservable`1<System.Object>
struct ToObservableObservable_1_t4048954016;

#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Oper1187768149.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Operators.ToObservableObservable`1/ToObservable<System.Object>
struct  ToObservable_t1484005639  : public OperatorObserverBase_2_t1187768149
{
public:
	// UniRx.Operators.ToObservableObservable`1<T> UniRx.Operators.ToObservableObservable`1/ToObservable::parent
	ToObservableObservable_1_t4048954016 * ___parent_2;

public:
	inline static int32_t get_offset_of_parent_2() { return static_cast<int32_t>(offsetof(ToObservable_t1484005639, ___parent_2)); }
	inline ToObservableObservable_1_t4048954016 * get_parent_2() const { return ___parent_2; }
	inline ToObservableObservable_1_t4048954016 ** get_address_of_parent_2() { return &___parent_2; }
	inline void set_parent_2(ToObservableObservable_1_t4048954016 * value)
	{
		___parent_2 = value;
		Il2CppCodeGenWriteBarrier(&___parent_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
