﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <>__AnonType1`2<System.Boolean,System.Boolean>
struct  U3CU3E__AnonType1_2_t2559104484  : public Il2CppObject
{
public:
	// <show>__T <>__AnonType1`2::<show>
	bool ___U3CshowU3E_0;
	// <hidden>__T <>__AnonType1`2::<hidden>
	bool ___U3ChiddenU3E_1;

public:
	inline static int32_t get_offset_of_U3CshowU3E_0() { return static_cast<int32_t>(offsetof(U3CU3E__AnonType1_2_t2559104484, ___U3CshowU3E_0)); }
	inline bool get_U3CshowU3E_0() const { return ___U3CshowU3E_0; }
	inline bool* get_address_of_U3CshowU3E_0() { return &___U3CshowU3E_0; }
	inline void set_U3CshowU3E_0(bool value)
	{
		___U3CshowU3E_0 = value;
	}

	inline static int32_t get_offset_of_U3ChiddenU3E_1() { return static_cast<int32_t>(offsetof(U3CU3E__AnonType1_2_t2559104484, ___U3ChiddenU3E_1)); }
	inline bool get_U3ChiddenU3E_1() const { return ___U3ChiddenU3E_1; }
	inline bool* get_address_of_U3ChiddenU3E_1() { return &___U3ChiddenU3E_1; }
	inline void set_U3ChiddenU3E_1(bool value)
	{
		___U3ChiddenU3E_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
