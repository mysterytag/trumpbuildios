﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Zenject.FactoryUntyped`1<System.Object>
struct FactoryUntyped_1_t2760421534;
// Zenject.DiContainer
struct DiContainer_t2383114449;
// System.Type
struct Type_t;
// System.Object
struct Il2CppObject;
// System.Object[]
struct ObjectU5BU5D_t11523773;
// System.Collections.Generic.IEnumerable`1<Zenject.ZenjectResolveException>
struct IEnumerable_1_t4073207355;
// System.Type[]
struct TypeU5BU5D_t3431720054;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Zenject_DiContainer2383114449.h"
#include "mscorlib_System_Type2779229935.h"

// System.Void Zenject.FactoryUntyped`1<System.Object>::.ctor()
extern "C"  void FactoryUntyped_1__ctor_m3424962022_gshared (FactoryUntyped_1_t2760421534 * __this, const MethodInfo* method);
#define FactoryUntyped_1__ctor_m3424962022(__this, method) ((  void (*) (FactoryUntyped_1_t2760421534 *, const MethodInfo*))FactoryUntyped_1__ctor_m3424962022_gshared)(__this, method)
// Zenject.DiContainer Zenject.FactoryUntyped`1<System.Object>::get_Container()
extern "C"  DiContainer_t2383114449 * FactoryUntyped_1_get_Container_m425604402_gshared (FactoryUntyped_1_t2760421534 * __this, const MethodInfo* method);
#define FactoryUntyped_1_get_Container_m425604402(__this, method) ((  DiContainer_t2383114449 * (*) (FactoryUntyped_1_t2760421534 *, const MethodInfo*))FactoryUntyped_1_get_Container_m425604402_gshared)(__this, method)
// System.Void Zenject.FactoryUntyped`1<System.Object>::set_Container(Zenject.DiContainer)
extern "C"  void FactoryUntyped_1_set_Container_m2834492365_gshared (FactoryUntyped_1_t2760421534 * __this, DiContainer_t2383114449 * ___value0, const MethodInfo* method);
#define FactoryUntyped_1_set_Container_m2834492365(__this, ___value0, method) ((  void (*) (FactoryUntyped_1_t2760421534 *, DiContainer_t2383114449 *, const MethodInfo*))FactoryUntyped_1_set_Container_m2834492365_gshared)(__this, ___value0, method)
// System.Type Zenject.FactoryUntyped`1<System.Object>::get_ConcreteType()
extern "C"  Type_t * FactoryUntyped_1_get_ConcreteType_m3473729388_gshared (FactoryUntyped_1_t2760421534 * __this, const MethodInfo* method);
#define FactoryUntyped_1_get_ConcreteType_m3473729388(__this, method) ((  Type_t * (*) (FactoryUntyped_1_t2760421534 *, const MethodInfo*))FactoryUntyped_1_get_ConcreteType_m3473729388_gshared)(__this, method)
// System.Void Zenject.FactoryUntyped`1<System.Object>::set_ConcreteType(System.Type)
extern "C"  void FactoryUntyped_1_set_ConcreteType_m3693397759_gshared (FactoryUntyped_1_t2760421534 * __this, Type_t * ___value0, const MethodInfo* method);
#define FactoryUntyped_1_set_ConcreteType_m3693397759(__this, ___value0, method) ((  void (*) (FactoryUntyped_1_t2760421534 *, Type_t *, const MethodInfo*))FactoryUntyped_1_set_ConcreteType_m3693397759_gshared)(__this, ___value0, method)
// System.Void Zenject.FactoryUntyped`1<System.Object>::Initialize()
extern "C"  void FactoryUntyped_1_Initialize_m301289582_gshared (FactoryUntyped_1_t2760421534 * __this, const MethodInfo* method);
#define FactoryUntyped_1_Initialize_m301289582(__this, method) ((  void (*) (FactoryUntyped_1_t2760421534 *, const MethodInfo*))FactoryUntyped_1_Initialize_m301289582_gshared)(__this, method)
// TContract Zenject.FactoryUntyped`1<System.Object>::Create(System.Object[])
extern "C"  Il2CppObject * FactoryUntyped_1_Create_m2845155493_gshared (FactoryUntyped_1_t2760421534 * __this, ObjectU5BU5D_t11523773* ___constructorArgs0, const MethodInfo* method);
#define FactoryUntyped_1_Create_m2845155493(__this, ___constructorArgs0, method) ((  Il2CppObject * (*) (FactoryUntyped_1_t2760421534 *, ObjectU5BU5D_t11523773*, const MethodInfo*))FactoryUntyped_1_Create_m2845155493_gshared)(__this, ___constructorArgs0, method)
// System.Collections.Generic.IEnumerable`1<Zenject.ZenjectResolveException> Zenject.FactoryUntyped`1<System.Object>::Validate(System.Type[])
extern "C"  Il2CppObject* FactoryUntyped_1_Validate_m1573356162_gshared (FactoryUntyped_1_t2760421534 * __this, TypeU5BU5D_t3431720054* ___extras0, const MethodInfo* method);
#define FactoryUntyped_1_Validate_m1573356162(__this, ___extras0, method) ((  Il2CppObject* (*) (FactoryUntyped_1_t2760421534 *, TypeU5BU5D_t3431720054*, const MethodInfo*))FactoryUntyped_1_Validate_m1573356162_gshared)(__this, ___extras0, method)
