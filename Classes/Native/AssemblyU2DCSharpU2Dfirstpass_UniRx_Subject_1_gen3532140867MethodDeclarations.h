﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Subject_1_gen1712827743MethodDeclarations.h"

// System.Void UniRx.Subject`1<UniRx.CollectionRemoveEvent`1<UniRx.Tuple`2<System.String,UnityEngine.Sprite>>>::.ctor()
#define Subject_1__ctor_m954709709(__this, method) ((  void (*) (Subject_1_t3532140867 *, const MethodInfo*))Subject_1__ctor_m1370208901_gshared)(__this, method)
// System.Boolean UniRx.Subject`1<UniRx.CollectionRemoveEvent`1<UniRx.Tuple`2<System.String,UnityEngine.Sprite>>>::get_HasObservers()
#define Subject_1_get_HasObservers_m2317728487(__this, method) ((  bool (*) (Subject_1_t3532140867 *, const MethodInfo*))Subject_1_get_HasObservers_m1297353703_gshared)(__this, method)
// System.Void UniRx.Subject`1<UniRx.CollectionRemoveEvent`1<UniRx.Tuple`2<System.String,UnityEngine.Sprite>>>::OnCompleted()
#define Subject_1_OnCompleted_m722459799(__this, method) ((  void (*) (Subject_1_t3532140867 *, const MethodInfo*))Subject_1_OnCompleted_m1493186127_gshared)(__this, method)
// System.Void UniRx.Subject`1<UniRx.CollectionRemoveEvent`1<UniRx.Tuple`2<System.String,UnityEngine.Sprite>>>::OnError(System.Exception)
#define Subject_1_OnError_m1914266436(__this, ___error0, method) ((  void (*) (Subject_1_t3532140867 *, Exception_t1967233988 *, const MethodInfo*))Subject_1_OnError_m863561980_gshared)(__this, ___error0, method)
// System.Void UniRx.Subject`1<UniRx.CollectionRemoveEvent`1<UniRx.Tuple`2<System.String,UnityEngine.Sprite>>>::OnNext(T)
#define Subject_1_OnNext_m1183732277(__this, ___value0, method) ((  void (*) (Subject_1_t3532140867 *, CollectionRemoveEvent_1_t1594106247 , const MethodInfo*))Subject_1_OnNext_m1046497261_gshared)(__this, ___value0, method)
// System.IDisposable UniRx.Subject`1<UniRx.CollectionRemoveEvent`1<UniRx.Tuple`2<System.String,UnityEngine.Sprite>>>::Subscribe(UniRx.IObserver`1<T>)
#define Subject_1_Subscribe_m851061666(__this, ___observer0, method) ((  Il2CppObject * (*) (Subject_1_t3532140867 *, Il2CppObject*, const MethodInfo*))Subject_1_Subscribe_m4152592388_gshared)(__this, ___observer0, method)
// System.Void UniRx.Subject`1<UniRx.CollectionRemoveEvent`1<UniRx.Tuple`2<System.String,UnityEngine.Sprite>>>::Dispose()
#define Subject_1_Dispose_m2546279370(__this, method) ((  void (*) (Subject_1_t3532140867 *, const MethodInfo*))Subject_1_Dispose_m2409044354_gshared)(__this, method)
// System.Void UniRx.Subject`1<UniRx.CollectionRemoveEvent`1<UniRx.Tuple`2<System.String,UnityEngine.Sprite>>>::ThrowIfDisposed()
#define Subject_1_ThrowIfDisposed_m1796170579(__this, method) ((  void (*) (Subject_1_t3532140867 *, const MethodInfo*))Subject_1_ThrowIfDisposed_m290201867_gshared)(__this, method)
// System.Boolean UniRx.Subject`1<UniRx.CollectionRemoveEvent`1<UniRx.Tuple`2<System.String,UnityEngine.Sprite>>>::IsRequiredSubscribeOnCurrentThread()
#define Subject_1_IsRequiredSubscribeOnCurrentThread_m1603656734(__this, method) ((  bool (*) (Subject_1_t3532140867 *, const MethodInfo*))Subject_1_IsRequiredSubscribeOnCurrentThread_m3694259486_gshared)(__this, method)
