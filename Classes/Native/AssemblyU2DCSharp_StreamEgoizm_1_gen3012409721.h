﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Generic.Stack`1<System.Collections.Generic.KeyValuePair`2<System.IDisposable,System.Action`1<System.Object>>>
struct Stack_1_t1890507522;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// StreamEgoizm`1<System.Object>
struct  StreamEgoizm_1_t3012409721  : public Il2CppObject
{
public:
	// System.Collections.Generic.Stack`1<System.Collections.Generic.KeyValuePair`2<System.IDisposable,System.Action`1<T>>> StreamEgoizm`1::_subscribes
	Stack_1_t1890507522 * ____subscribes_0;

public:
	inline static int32_t get_offset_of__subscribes_0() { return static_cast<int32_t>(offsetof(StreamEgoizm_1_t3012409721, ____subscribes_0)); }
	inline Stack_1_t1890507522 * get__subscribes_0() const { return ____subscribes_0; }
	inline Stack_1_t1890507522 ** get_address_of__subscribes_0() { return &____subscribes_0; }
	inline void set__subscribes_0(Stack_1_t1890507522 * value)
	{
		____subscribes_0 = value;
		Il2CppCodeGenWriteBarrier(&____subscribes_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
