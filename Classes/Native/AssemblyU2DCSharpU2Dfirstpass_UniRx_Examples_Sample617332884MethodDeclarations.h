﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Examples.Sample06_ConvertToCoroutine
struct Sample06_ConvertToCoroutine_t617332884;
// System.Collections.IEnumerator
struct IEnumerator_t287207039;

#include "codegen/il2cpp-codegen.h"

// System.Void UniRx.Examples.Sample06_ConvertToCoroutine::.ctor()
extern "C"  void Sample06_ConvertToCoroutine__ctor_m1767416259 (Sample06_ConvertToCoroutine_t617332884 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.Examples.Sample06_ConvertToCoroutine::Start()
extern "C"  void Sample06_ConvertToCoroutine_Start_m714554051 (Sample06_ConvertToCoroutine_t617332884 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator UniRx.Examples.Sample06_ConvertToCoroutine::ComplexCoroutineTest()
extern "C"  Il2CppObject * Sample06_ConvertToCoroutine_ComplexCoroutineTest_m4082061963 (Sample06_ConvertToCoroutine_t617332884 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator UniRx.Examples.Sample06_ConvertToCoroutine::LazyTaskTest()
extern "C"  Il2CppObject * Sample06_ConvertToCoroutine_LazyTaskTest_m122679516 (Sample06_ConvertToCoroutine_t617332884 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator UniRx.Examples.Sample06_ConvertToCoroutine::TestNewCustomYieldInstruction()
extern "C"  Il2CppObject * Sample06_ConvertToCoroutine_TestNewCustomYieldInstruction_m562664785 (Sample06_ConvertToCoroutine_t617332884 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
