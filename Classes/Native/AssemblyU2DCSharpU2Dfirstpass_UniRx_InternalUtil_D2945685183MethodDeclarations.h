﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.InternalUtil.DisposedObserver`1<UniRx.CountChangedStatus>
struct DisposedObserver_1_t2945685183;
// System.Exception
struct Exception_t1967233988;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Exception1967233988.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_CountChangedSt3771227721.h"

// System.Void UniRx.InternalUtil.DisposedObserver`1<UniRx.CountChangedStatus>::.ctor()
extern "C"  void DisposedObserver_1__ctor_m830530497_gshared (DisposedObserver_1_t2945685183 * __this, const MethodInfo* method);
#define DisposedObserver_1__ctor_m830530497(__this, method) ((  void (*) (DisposedObserver_1_t2945685183 *, const MethodInfo*))DisposedObserver_1__ctor_m830530497_gshared)(__this, method)
// System.Void UniRx.InternalUtil.DisposedObserver`1<UniRx.CountChangedStatus>::.cctor()
extern "C"  void DisposedObserver_1__cctor_m3789512716_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define DisposedObserver_1__cctor_m3789512716(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))DisposedObserver_1__cctor_m3789512716_gshared)(__this /* static, unused */, method)
// System.Void UniRx.InternalUtil.DisposedObserver`1<UniRx.CountChangedStatus>::OnCompleted()
extern "C"  void DisposedObserver_1_OnCompleted_m2324202123_gshared (DisposedObserver_1_t2945685183 * __this, const MethodInfo* method);
#define DisposedObserver_1_OnCompleted_m2324202123(__this, method) ((  void (*) (DisposedObserver_1_t2945685183 *, const MethodInfo*))DisposedObserver_1_OnCompleted_m2324202123_gshared)(__this, method)
// System.Void UniRx.InternalUtil.DisposedObserver`1<UniRx.CountChangedStatus>::OnError(System.Exception)
extern "C"  void DisposedObserver_1_OnError_m631552824_gshared (DisposedObserver_1_t2945685183 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method);
#define DisposedObserver_1_OnError_m631552824(__this, ___error0, method) ((  void (*) (DisposedObserver_1_t2945685183 *, Exception_t1967233988 *, const MethodInfo*))DisposedObserver_1_OnError_m631552824_gshared)(__this, ___error0, method)
// System.Void UniRx.InternalUtil.DisposedObserver`1<UniRx.CountChangedStatus>::OnNext(T)
extern "C"  void DisposedObserver_1_OnNext_m2106593833_gshared (DisposedObserver_1_t2945685183 * __this, int32_t ___value0, const MethodInfo* method);
#define DisposedObserver_1_OnNext_m2106593833(__this, ___value0, method) ((  void (*) (DisposedObserver_1_t2945685183 *, int32_t, const MethodInfo*))DisposedObserver_1_OnNext_m2106593833_gshared)(__this, ___value0, method)
