﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.PlayFabClientAPI/GetGameServerRegionsResponseCallback
struct GetGameServerRegionsResponseCallback_t717093786;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;
// PlayFab.ClientModels.GameServerRegionsRequest
struct GameServerRegionsRequest_t2476765941;
// PlayFab.ClientModels.GameServerRegionsResult
struct GameServerRegionsResult_t253590807;
// PlayFab.PlayFabError
struct PlayFabError_t750598646;
// System.IAsyncResult
struct IAsyncResult_t537683269;
// System.AsyncCallback
struct AsyncCallback_t1363551830;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_IntPtr676692020.h"
#include "mscorlib_System_String968488902.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_GameServerR2476765941.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_GameServerRe253590807.h"
#include "AssemblyU2DCSharpU2Dfirstpass_PlayFab_PlayFabError750598646.h"
#include "mscorlib_System_AsyncCallback1363551830.h"

// System.Void PlayFab.PlayFabClientAPI/GetGameServerRegionsResponseCallback::.ctor(System.Object,System.IntPtr)
extern "C"  void GetGameServerRegionsResponseCallback__ctor_m2261000073 (GetGameServerRegionsResponseCallback_t717093786 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/GetGameServerRegionsResponseCallback::Invoke(System.String,System.Int32,PlayFab.ClientModels.GameServerRegionsRequest,PlayFab.ClientModels.GameServerRegionsResult,PlayFab.PlayFabError,System.Object)
extern "C"  void GetGameServerRegionsResponseCallback_Invoke_m1111329160 (GetGameServerRegionsResponseCallback_t717093786 * __this, String_t* ___urlPath0, int32_t ___callId1, GameServerRegionsRequest_t2476765941 * ___request2, GameServerRegionsResult_t253590807 * ___result3, PlayFabError_t750598646 * ___error4, Il2CppObject * ___customData5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
extern "C" void pinvoke_delegate_wrapper_GetGameServerRegionsResponseCallback_t717093786(Il2CppObject* delegate, String_t* ___urlPath0, int32_t ___callId1, GameServerRegionsRequest_t2476765941 * ___request2, GameServerRegionsResult_t253590807 * ___result3, PlayFabError_t750598646 * ___error4, Il2CppObject * ___customData5);
// System.IAsyncResult PlayFab.PlayFabClientAPI/GetGameServerRegionsResponseCallback::BeginInvoke(System.String,System.Int32,PlayFab.ClientModels.GameServerRegionsRequest,PlayFab.ClientModels.GameServerRegionsResult,PlayFab.PlayFabError,System.Object,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * GetGameServerRegionsResponseCallback_BeginInvoke_m2461145169 (GetGameServerRegionsResponseCallback_t717093786 * __this, String_t* ___urlPath0, int32_t ___callId1, GameServerRegionsRequest_t2476765941 * ___request2, GameServerRegionsResult_t253590807 * ___result3, PlayFabError_t750598646 * ___error4, Il2CppObject * ___customData5, AsyncCallback_t1363551830 * ___callback6, Il2CppObject * ___object7, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/GetGameServerRegionsResponseCallback::EndInvoke(System.IAsyncResult)
extern "C"  void GetGameServerRegionsResponseCallback_EndInvoke_m2636037657 (GetGameServerRegionsResponseCallback_t717093786 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
