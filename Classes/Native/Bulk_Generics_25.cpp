﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

// UniRx.Operators.EmptyObservable`1/Empty<UniRx.CollectionMoveEvent`1<UniRx.Tuple`2<System.Object,System.Object>>>
struct Empty_t112439784;
// UniRx.IObserver`1<UniRx.CollectionMoveEvent`1<UniRx.Tuple`2<System.Object,System.Object>>>
struct IObserver_1_t365620853;
// System.IDisposable
struct IDisposable_t1628921374;
// System.Exception
struct Exception_t1967233988;
// UniRx.Operators.EmptyObservable`1/Empty<UniRx.CollectionRemoveEvent`1<System.Object>>
struct Empty_t2201455558;
// UniRx.IObserver`1<UniRx.CollectionRemoveEvent`1<System.Object>>
struct IObserver_1_t2454636627;
// UniRx.Operators.EmptyObservable`1/Empty<UniRx.CollectionRemoveEvent`1<UniRx.Tuple`2<System.Object,System.Object>>>
struct Empty_t1733610957;
// UniRx.IObserver`1<UniRx.CollectionRemoveEvent`1<UniRx.Tuple`2<System.Object,System.Object>>>
struct IObserver_1_t1986792026;
// UniRx.Operators.EmptyObservable`1/Empty<UniRx.CollectionReplaceEvent`1<System.Object>>
struct Empty_t3800568370;
// UniRx.IObserver`1<UniRx.CollectionReplaceEvent`1<System.Object>>
struct IObserver_1_t4053749439;
// UniRx.Operators.EmptyObservable`1/Empty<UniRx.CollectionReplaceEvent`1<UniRx.Tuple`2<System.Object,System.Object>>>
struct Empty_t3332723769;
// UniRx.IObserver`1<UniRx.CollectionReplaceEvent`1<UniRx.Tuple`2<System.Object,System.Object>>>
struct IObserver_1_t3585904838;
// UniRx.Operators.EmptyObservable`1/Empty<UniRx.DictionaryAddEvent`2<System.Object,System.Object>>
struct Empty_t2209798842;
// UniRx.IObserver`1<UniRx.DictionaryAddEvent`2<System.Object,System.Object>>
struct IObserver_1_t2462979911;
// UniRx.Operators.EmptyObservable`1/Empty<UniRx.DictionaryRemoveEvent`2<System.Object,System.Object>>
struct Empty_t986138829;
// UniRx.IObserver`1<UniRx.DictionaryRemoveEvent`2<System.Object,System.Object>>
struct IObserver_1_t1239319898;
// UniRx.Operators.EmptyObservable`1/Empty<UniRx.DictionaryReplaceEvent`2<System.Object,System.Object>>
struct Empty_t1442777601;
// UniRx.IObserver`1<UniRx.DictionaryReplaceEvent`2<System.Object,System.Object>>
struct IObserver_1_t1695958670;
// UniRx.Operators.EmptyObservable`1/Empty<UniRx.Unit>
struct Empty_t222136576;
// UniRx.IObserver`1<UniRx.Unit>
struct IObserver_1_t475317645;
// UniRx.Operators.EmptyObservable`1<System.Double>
struct EmptyObservable_1_t3223706313;
// UniRx.IScheduler
struct IScheduler_t2938318244;
// UniRx.IObserver`1<System.Double>
struct IObserver_1_t2746515517;
// UniRx.Operators.EmptyObservable`1<System.Int32>
struct EmptyObservable_1_t1241637190;
// UniRx.IObserver`1<System.Int32>
struct IObserver_1_t764446394;
// UniRx.Operators.EmptyObservable`1<System.Int64>
struct EmptyObservable_1_t1241637285;
// UniRx.IObserver`1<System.Int64>
struct IObserver_1_t764446489;
// UniRx.Operators.EmptyObservable`1<System.Object>
struct EmptyObservable_1_t3526296119;
// UniRx.IObserver`1<System.Object>
struct IObserver_1_t3049105323;
// UniRx.Operators.EmptyObservable`1<UniRx.CollectionAddEvent`1<System.Object>>
struct EmptyObservable_1_t810744390;
// UniRx.IObserver`1<UniRx.CollectionAddEvent`1<System.Object>>
struct IObserver_1_t333553594;
// UniRx.Operators.EmptyObservable`1<UniRx.CollectionAddEvent`1<UniRx.Tuple`2<System.Object,System.Object>>>
struct EmptyObservable_1_t342899789;
// UniRx.IObserver`1<UniRx.CollectionAddEvent`1<UniRx.Tuple`2<System.Object,System.Object>>>
struct IObserver_1_t4160676289;
// UniRx.Operators.EmptyObservable`1<UniRx.CollectionMoveEvent`1<System.Object>>
struct EmptyObservable_1_t1310656250;
// UniRx.IObserver`1<UniRx.CollectionMoveEvent`1<System.Object>>
struct IObserver_1_t833465454;
// UniRx.Operators.EmptyObservable`1<UniRx.CollectionMoveEvent`1<UniRx.Tuple`2<System.Object,System.Object>>>
struct EmptyObservable_1_t842811649;
// UniRx.Operators.EmptyObservable`1<UniRx.CollectionRemoveEvent`1<System.Object>>
struct EmptyObservable_1_t2931827423;
// UniRx.Operators.EmptyObservable`1<UniRx.CollectionRemoveEvent`1<UniRx.Tuple`2<System.Object,System.Object>>>
struct EmptyObservable_1_t2463982822;
// UniRx.Operators.EmptyObservable`1<UniRx.CollectionReplaceEvent`1<System.Object>>
struct EmptyObservable_1_t235972939;
// UniRx.Operators.EmptyObservable`1<UniRx.CollectionReplaceEvent`1<UniRx.Tuple`2<System.Object,System.Object>>>
struct EmptyObservable_1_t4063095634;
// UniRx.Operators.EmptyObservable`1<UniRx.DictionaryAddEvent`2<System.Object,System.Object>>
struct EmptyObservable_1_t2940170707;
// UniRx.Operators.EmptyObservable`1<UniRx.DictionaryRemoveEvent`2<System.Object,System.Object>>
struct EmptyObservable_1_t1716510694;
// UniRx.Operators.EmptyObservable`1<UniRx.DictionaryReplaceEvent`2<System.Object,System.Object>>
struct EmptyObservable_1_t2173149466;
// UniRx.Operators.EmptyObservable`1<UniRx.Unit>
struct EmptyObservable_1_t952508441;
// UniRx.Operators.FinallyObservable`1/Finally<System.Object>
struct Finally_t1925751780;
// UniRx.Operators.FinallyObservable`1<System.Object>
struct FinallyObservable_1_t2652127741;
// System.Object
struct Il2CppObject;
// UniRx.IObservable`1<System.Object>
struct IObservable_1_t595904784;
// System.Action
struct Action_t437523947;
// UniRx.Operators.FirstObservable`1/First_<System.Boolean>
struct First__t3433238553;
// UniRx.Operators.FirstObservable`1<System.Boolean>
struct FirstObservable_1_t2233559755;
// UniRx.IObserver`1<System.Boolean>
struct IObserver_1_t2423004244;
// UniRx.Operators.FirstObservable`1/First_<System.Int64>
struct First__t1774680798;
// UniRx.Operators.FirstObservable`1<System.Int64>
struct FirstObservable_1_t575002000;
// UniRx.Operators.FirstObservable`1/First_<System.Object>
struct First__t4059339632;
// UniRx.Operators.FirstObservable`1<System.Object>
struct FirstObservable_1_t2859660834;
// UniRx.Operators.FirstObservable`1/First_<UniRx.Tuple`2<System.Object,System.Object>>
struct First__t3591495031;
// UniRx.Operators.FirstObservable`1<UniRx.Tuple`2<System.Object,System.Object>>
struct FirstObservable_1_t2391816233;
// UniRx.IObserver`1<UniRx.Tuple`2<System.Object,System.Object>>
struct IObserver_1_t2581260722;
// UniRx.Operators.FirstObservable`1/First_<UniRx.Unit>
struct First__t1485551954;
// UniRx.Operators.FirstObservable`1<UniRx.Unit>
struct FirstObservable_1_t285873156;
// UniRx.Operators.FirstObservable`1/First<System.Boolean>
struct First_t2588875826;
// UniRx.Operators.FirstObservable`1/First<System.Int64>
struct First_t930318071;
// UniRx.Operators.FirstObservable`1/First<System.Object>
struct First_t3214976905;
// UniRx.Operators.FirstObservable`1/First<UniRx.Tuple`2<System.Object,System.Object>>
struct First_t2747132304;
// UniRx.Operators.FirstObservable`1/First<UniRx.Unit>
struct First_t641189227;
// UniRx.IObservable`1<System.Boolean>
struct IObservable_1_t4264771001;
// System.Func`2<System.Boolean,System.Boolean>
struct Func_2_t1008118516;
// UniRx.IObservable`1<System.Int64>
struct IObservable_1_t2606213246;
// System.Func`2<System.Int64,System.Boolean>
struct Func_2_t2251336571;
// System.Func`2<System.Object,System.Boolean>
struct Func_2_t1509682273;
// UniRx.IObservable`1<UniRx.Tuple`2<System.Object,System.Object>>
struct IObservable_1_t128060183;
// System.Func`2<UniRx.Tuple`2<System.Object,System.Object>,System.Boolean>
struct Func_2_t1101125214;
// UniRx.IObservable`1<UniRx.Unit>
struct IObservable_1_t2317084402;
// System.Func`2<UniRx.Unit,System.Boolean>
struct Func_2_t2766110903;
// UniRx.Operators.ForEachAsyncObservable`1/ForEachAsync_<System.Object>
struct ForEachAsync__t1789301054;
// UniRx.Operators.ForEachAsyncObservable`1<System.Object>
struct ForEachAsyncObservable_1_t3176723348;
// UniRx.Operators.ForEachAsyncObservable`1/ForEachAsync<System.Object>
struct ForEachAsync_t4111581179;
// System.Action`1<System.Object>
struct Action_1_t985559125;
// System.Action`2<System.Object,System.Int32>
struct Action_2_t1820800989;
// UniRx.Operators.FromCoroutine`1/FromCoroutineObserver<System.Double>
struct FromCoroutineObserver_t3712745359;
// UniRx.Operators.FromCoroutine`1/FromCoroutineObserver<System.Int64>
struct FromCoroutineObserver_t1730676331;
// UniRx.Operators.FromCoroutine`1/FromCoroutineObserver<System.Object>
struct FromCoroutineObserver_t4015335165;
// UniRx.Operators.FromCoroutine`1/FromCoroutineObserver<UniRx.Unit>
struct FromCoroutineObserver_t1441547487;
// UniRx.Operators.FromCoroutine`1<System.Double>
struct FromCoroutine_1_t211580279;
// System.Func`3<UniRx.IObserver`1<System.Double>,UniRx.CancellationToken,System.Collections.IEnumerator>
struct Func_3_t1280587413;
// UniRx.Operators.FromCoroutine`1<System.Int64>
struct FromCoroutine_1_t2524478547;
// System.Func`3<UniRx.IObserver`1<System.Int64>,UniRx.CancellationToken,System.Collections.IEnumerator>
struct Func_3_t521084177;
// UniRx.Operators.FromCoroutine`1<System.Object>
struct FromCoroutine_1_t514170085;
// System.Func`3<UniRx.IObserver`1<System.Object>,UniRx.CancellationToken,System.Collections.IEnumerator>
struct Func_3_t1973235155;
// UniRx.Operators.FromCoroutine`1<UniRx.Unit>
struct FromCoroutine_1_t2235349703;
// System.Func`3<UniRx.IObserver`1<UniRx.Unit>,UniRx.CancellationToken,System.Collections.IEnumerator>
struct Func_3_t3458695013;
// UniRx.Operators.FromEventObservable_`1/FromEvent<System.Int32>
struct FromEvent_t706075128;
// UniRx.Operators.FromEventObservable_`1<System.Int32>
struct FromEventObservable__1_t350522882;
// UniRx.Operators.FromEventObservable_`1/FromEvent<System.Object>
struct FromEvent_t2990734058;
// UniRx.Operators.FromEventObservable_`1<System.Object>
struct FromEventObservable__1_t2635181811;
// System.Action`1<System.Action`1<System.Int32>>
struct Action_1_t3144320197;
// System.Action`1<System.Action`1<System.Object>>
struct Action_1_t1134011830;
// UniRx.Operators.FromEventObservable`1/FromEvent<System.Object>
struct FromEvent_t2990734057;
// UniRx.Operators.FromEventObservable`1<System.Object>
struct FromEventObservable_1_t2790472066;
// System.Func`2<System.Action,System.Object>
struct Func_2_t2670270213;
// UniRx.Operators.FromEventObservable`2/FromEvent<System.Object,System.Boolean>
struct FromEvent_t1915864816;
// UniRx.Operators.FromEventObservable`2<System.Object,System.Boolean>
struct FromEventObservable_2_t1626038518;
// UniRx.Operators.FromEventObservable`2/FromEvent<System.Object,System.Object>
struct FromEvent_t2541965895;
// UniRx.Operators.FromEventObservable`2<System.Object,System.Object>
struct FromEventObservable_2_t2252139597;
// UniRx.Operators.FromEventObservable`2/FromEvent<System.Object,System.Single>
struct FromEvent_t2663068496;
// UniRx.Operators.FromEventObservable`2<System.Object,System.Single>
struct FromEventObservable_2_t2373242198;
// UniRx.IObserver`1<System.Single>
struct IObserver_1_t3170207924;
// UniRx.Operators.FromEventObservable`2/FromEvent<System.Object,UniRx.Tuple`2<System.Object,System.Object>>
struct FromEvent_t2074121294;
// UniRx.Operators.FromEventObservable`2<System.Object,UniRx.Tuple`2<System.Object,System.Object>>
struct FromEventObservable_2_t1784294996;
// UniRx.Operators.FromEventObservable`2/FromEvent<System.Object,UniRx.Tuple`3<System.Object,System.Object,System.Object>>
struct FromEvent_t3355758577;
// UniRx.Operators.FromEventObservable`2<System.Object,UniRx.Tuple`3<System.Object,System.Object,System.Object>>
struct FromEventObservable_2_t3065932279;
// UniRx.IObserver`1<UniRx.Tuple`3<System.Object,System.Object,System.Object>>
struct IObserver_1_t3862898005;
// UniRx.Operators.FromEventObservable`2/FromEvent<System.Object,UniRx.Tuple`4<System.Object,System.Object,System.Object,System.Object>>
struct FromEvent_t2732089756;
// UniRx.Operators.FromEventObservable`2<System.Object,UniRx.Tuple`4<System.Object,System.Object,System.Object,System.Object>>
struct FromEventObservable_2_t2442263458;
// UniRx.IObserver`1<UniRx.Tuple`4<System.Object,System.Object,System.Object,System.Object>>
struct IObserver_1_t3239229184;
// UniRx.Operators.FromEventObservable`2/FromEvent<System.Object,UnityEngine.Vector2>
struct FromEvent_t935221967;
// UniRx.Operators.FromEventObservable`2<System.Object,UnityEngine.Vector2>
struct FromEventObservable_2_t645395669;
// UniRx.IObserver`1<UnityEngine.Vector2>
struct IObserver_1_t1442361395;
// System.Func`2<System.Action`1<System.Boolean>,System.Object>
struct Func_2_t599409222;
// System.Func`2<System.Action`1<System.Object>,System.Object>
struct Func_2_t1100972979;
// System.Func`2<System.Action`1<System.Single>,System.Object>
struct Func_2_t2308880486;
// System.Func`2<System.Action`1<UniRx.Tuple`2<System.Object,System.Object>>,System.Object>
struct Func_2_t692415920;
// System.Func`2<System.Action`1<UniRx.Tuple`3<System.Object,System.Object,System.Object>>,System.Object>
struct Func_2_t32479265;
// System.Func`2<System.Action`1<UniRx.Tuple`4<System.Object,System.Object,System.Object,System.Object>>,System.Object>
struct Func_2_t202992490;
// System.Func`2<System.Action`1<UnityEngine.Vector2>,System.Object>
struct Func_2_t3830833163;
// UniRx.Operators.FromEventPatternObservable`2/FromEventPattern<System.Object,System.Object>
struct FromEventPattern_t208508663;
// UniRx.Operators.FromEventPatternObservable`2<System.Object,System.Object>
struct FromEventPatternObservable_2_t3019221245;
// UniRx.IObserver`1<UniRx.EventPattern`1<System.Object>>
struct IObserver_1_t820035585;
// System.Func`2<System.EventHandler`1<System.Object>,System.Object>
struct Func_2_t2937104577;
// UniRx.Operators.GroupByObservable`3/GroupBy<System.Object,System.Object,System.Object>
struct GroupBy_t3470189943;
// UniRx.Operators.GroupByObservable`3<System.Object,System.Object,System.Object>
struct GroupByObservable_3_t657897610;
// UniRx.IObserver`1<UniRx.IGroupedObservable`2<System.Object,System.Object>>
struct IObserver_1_t3518384802;
// System.Func`2<System.Object,System.Object>
struct Func_2_t2135783352;
// System.Collections.Generic.IEqualityComparer`1<System.Object>
struct IEqualityComparer_1_t3161373071;
// UniRx.Operators.GroupedObservable`2<System.Object,System.Object>
struct GroupedObservable_2_t2613254135;
// UniRx.ISubject`1<System.Object>
struct ISubject_1_t353709935;
// UniRx.RefCountDisposable
struct RefCountDisposable_t3288429070;
// UniRx.Operators.IgnoreElementsObservable`1/IgnoreElements<System.Object>
struct IgnoreElements_t1769081498;
// UniRx.Operators.IgnoreElementsObservable`1<System.Object>
struct IgnoreElementsObservable_1_t2767931315;
// UniRx.Operators.LastObservable`1/Last_<System.Object>
struct Last__t3329173242;
// UniRx.Operators.LastObservable`1<System.Object>
struct LastObservable_1_t3225858136;
// UniRx.Operators.LastObservable`1/Last<System.Object>
struct Last_t3745612479;
// UniRx.Operators.MaterializeObservable`1/Materialize<System.Object>
struct Materialize_t1797935262;
// UniRx.Operators.MaterializeObservable`1<System.Object>
struct MaterializeObservable_1_t1619437495;
// UniRx.IObserver`1<UniRx.Notification`1<System.Object>>
struct IObserver_1_t2250355278;
// UniRx.Notification`1<System.Object>
struct Notification_1_t38356375;
// UniRx.Operators.MergeObservable`1/MergeConcurrentObserver/Merge<System.Object>
struct Merge_t1753429969;
// UniRx.Operators.MergeObservable`1/MergeConcurrentObserver<System.Object>
struct MergeConcurrentObserver_t3534593302;
// UniRx.Operators.MergeObservable`1/MergeConcurrentObserver/Merge<UniRx.Unit>
struct Merge_t3474609587;
// UniRx.Operators.MergeObservable`1/MergeConcurrentObserver<UniRx.Unit>
struct MergeConcurrentObserver_t960805624;
// UniRx.Operators.MergeObservable`1<System.Object>
struct MergeObservable_1_t3637992042;
// UniRx.Operators.MergeObservable`1<UniRx.Unit>
struct MergeObservable_1_t1064204364;
// UniRx.Operators.MergeObservable`1/MergeOuterObserver/Merge<System.Object>
struct Merge_t1753429970;
// UniRx.Operators.MergeObservable`1/MergeOuterObserver<System.Object>
struct MergeOuterObserver_t1613081930;
// UniRx.Operators.MergeObservable`1/MergeOuterObserver/Merge<UniRx.Unit>
struct Merge_t3474609588;
// UniRx.Operators.MergeObservable`1/MergeOuterObserver<UniRx.Unit>
struct MergeOuterObserver_t3334261548;
// UniRx.IObservable`1<UniRx.IObservable`1<System.Object>>
struct IObservable_1_t354703148;
// UniRx.IObservable`1<UniRx.IObservable`1<UniRx.Unit>>
struct IObservable_1_t2075882766;

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array2840145358.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Empty112439784.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Empty112439784MethodDeclarations.h"
#include "mscorlib_System_Void2779279689.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Oper1917096173MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_CollectionMove2448589246.h"
#include "mscorlib_System_Object837106420.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Oper1917096173.h"
#include "mscorlib_System_Exception1967233988.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Empt2201455558.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Empt2201455558MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Oper3997058997MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_CollectionRemov242637724.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Oper3997058997.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Empt1733610957.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Empt1733610957MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Oper3120657337MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_CollectionRemo4069760419.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Oper3120657337.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Empt3800568370.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Empt3800568370MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Oper3791266949MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_CollectionRepl1841750536.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Oper3791266949.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Empt3332723769.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Empt3332723769MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Oper2914865289MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_CollectionRepl1373905935.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Oper2914865289.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Empt2209798842.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Empt2209798842MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Oper3051653477MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_DictionaryAddEv250981008.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Oper3051653477.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Empty986138829.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Empty986138829MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Operato3745721MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_DictionaryRemo3322288291.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Operato3745721.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Empt1442777601.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Empt1442777601MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Opera154015849MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_DictionaryRepl3778927063.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Opera154015849.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Empty222136576.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Empty222136576MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Oper4165376397MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Unit2558286038.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Oper4165376397.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Empt3223706313.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Empt3223706313MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Oper3893628881MethodDeclarations.h"
#include "mscorlib_System_Boolean211005341.h"
#include "System_Core_System_Action437523947MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Empt2493334448.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Empt2493334448MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Scheduler103934925.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Scheduler103934925MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Disposable3736388786.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Disposable3736388786MethodDeclarations.h"
#include "System_Core_System_Action437523947.h"
#include "mscorlib_System_IntPtr676692020.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Empt1241637190.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Empt1241637190MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Oper1911559758MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Empty511265325.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Empty511265325MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Empt1241637285.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Empt1241637285MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Oper1911559853MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Empty511265420.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Empty511265420MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Empt3526296119.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Empt3526296119MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Oper4196218687MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Empt2795924254.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Empt2795924254MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Empty810744390.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Empty810744390MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Oper1480666958MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_EmptyO80372525.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_EmptyO80372525MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Empty342899789.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Empty342899789MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Oper1012822357MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Empt3907495220.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Empt3907495220MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Empt1310656250.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Empt1310656250MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Oper1980578818MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Empty580284385.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Empty580284385MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Empty842811649.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Empty842811649MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Oper1512734217MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Empt2931827423.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Empt2931827423MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Oper3601749991MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Empt2463982822.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Empt2463982822MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Oper3133905390MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Empty235972939.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Empty235972939MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Opera905895507MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Empt4063095634.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Empt4063095634MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Opera438050906MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Empt2940170707.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Empt2940170707MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Oper3610093275MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Empt1716510694.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Empt1716510694MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Oper2386433262MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Empt2173149466.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Empt2173149466MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Oper2843072034MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Empty952508441.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Empty952508441MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Oper1622431009MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Fina1925751780.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Fina1925751780MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Fina2652127741.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Oper1187768149MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_StableComposit3433635614MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Oper1187768149.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Fina2652127741MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_OptimizedObser2379048080MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_OptimizedObser2379048080.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Firs3433238553.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Firs3433238553MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Firs2233559755.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Operat60103313MethodDeclarations.h"
#include "System_Core_System_Func_2_gen1008118516.h"
#include "System_Core_System_Func_2_gen1008118516MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Operat60103313.h"
#include "mscorlib_System_InvalidOperationException2420574324MethodDeclarations.h"
#include "mscorlib_System_String968488902.h"
#include "mscorlib_System_InvalidOperationException2420574324.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Firs1774680798.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Firs1774680798MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_First575002000.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Oper3939730909MethodDeclarations.h"
#include "mscorlib_System_Int642847414882.h"
#include "System_Core_System_Func_2_gen2251336571.h"
#include "System_Core_System_Func_2_gen2251336571MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Oper3939730909.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Firs4059339632.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Firs4059339632MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Firs2859660834.h"
#include "System_Core_System_Func_2_gen1509682273.h"
#include "System_Core_System_Func_2_gen1509682273MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Firs3591495031.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Firs3591495031MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Firs2391816233.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Opera311366489MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Tuple_2_gen369261819.h"
#include "System_Core_System_Func_2_gen1101125214.h"
#include "System_Core_System_Func_2_gen1101125214MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Opera311366489.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Firs1485551954.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Firs1485551954MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_First285873156.h"
#include "System_Core_System_Func_2_gen2766110903.h"
#include "System_Core_System_Func_2_gen2766110903MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Firs2588875826.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Firs2588875826MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_First930318071.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_First930318071MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Firs3214976905.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Firs3214976905MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Firs2747132304.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Firs2747132304MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_First641189227.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_First641189227MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Firs2233559755MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Oper3570117608MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_First575002000MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Firs2859660834MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Firs2391816233MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Oper3728374086MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_First285873156MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_ForE1789301054.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_ForE1789301054MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_ForE3176723348.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Oper2908947767MethodDeclarations.h"
#include "mscorlib_System_Int322847414787.h"
#include "System_Core_System_Action_2_gen1820800989.h"
#include "System_Core_System_Action_2_gen1820800989MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Oper2908947767.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Unit2558286038MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_ForE4111581179.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_ForE4111581179MethodDeclarations.h"
#include "mscorlib_System_Action_1_gen985559125.h"
#include "mscorlib_System_Action_1_gen985559125MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_ForE3176723348MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_From3712745359.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_From3712745359MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Oper1921935565MethodDeclarations.h"
#include "mscorlib_System_Double534516614.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Oper1921935565.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_From1730676331.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_From1730676331MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_From4015335165.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_From4015335165MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_From1441547487.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_From1441547487MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_FromC211580279.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_FromC211580279MethodDeclarations.h"
#include "System_Core_System_Func_3_gen1280587413.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_BooleanDisposa3065601722MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_CancellationTo1439151560MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_MainThreadDisp4027217788MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_BooleanDisposa3065601722.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_CancellationTo1439151560.h"
#include "System_Core_System_Func_3_gen1280587413MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_From2524478547.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_From2524478547MethodDeclarations.h"
#include "System_Core_System_Func_3_gen521084177.h"
#include "System_Core_System_Func_3_gen521084177MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_FromC514170085.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_FromC514170085MethodDeclarations.h"
#include "System_Core_System_Func_3_gen1973235155.h"
#include "System_Core_System_Func_3_gen1973235155MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_From2235349703.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_From2235349703MethodDeclarations.h"
#include "System_Core_System_Func_3_gen3458695013.h"
#include "System_Core_System_Func_3_gen3458695013MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_FromE706075128.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_FromE706075128MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_FromE350522882.h"
#include "mscorlib_System_Object837106420MethodDeclarations.h"
#include "mscorlib_System_Action_1_gen2995867492.h"
#include "mscorlib_System_Action_1_gen2995867492MethodDeclarations.h"
#include "mscorlib_System_Action_1_gen3144320197.h"
#include "mscorlib_System_Action_1_gen3144320197MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_From_0.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_From_0MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_From2635181811.h"
#include "mscorlib_System_Action_1_gen1134011830.h"
#include "mscorlib_System_Action_1_gen1134011830MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_FromE350522882MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_From2635181811MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_From2990734057.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_From2990734057MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_From2790472066.h"
#include "System_Core_System_Func_2_gen2670270213.h"
#include "System_Core_System_Func_2_gen2670270213MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_From2790472066MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_From1915864816.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_From1915864816MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_From1626038518.h"
#include "System_Core_System_Func_2_gen599409222.h"
#include "mscorlib_System_Action_1_gen359458046.h"
#include "mscorlib_System_Action_1_gen359458046MethodDeclarations.h"
#include "System_Core_System_Func_2_gen599409222MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_From2541965895.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_From2541965895MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_From2252139597.h"
#include "System_Core_System_Func_2_gen1100972979.h"
#include "System_Core_System_Func_2_gen1100972979MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_From2663068496.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_From2663068496MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_From2373242198.h"
#include "System_Core_System_Func_2_gen2308880486.h"
#include "mscorlib_System_Single958209021.h"
#include "mscorlib_System_Action_1_gen1106661726.h"
#include "mscorlib_System_Action_1_gen1106661726MethodDeclarations.h"
#include "System_Core_System_Func_2_gen2308880486MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_From2074121294.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_From2074121294MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_From1784294996.h"
#include "System_Core_System_Func_2_gen692415920.h"
#include "mscorlib_System_Action_1_gen517714524.h"
#include "mscorlib_System_Action_1_gen517714524MethodDeclarations.h"
#include "System_Core_System_Func_2_gen692415920MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_From3355758577.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_From3355758577MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_From3065932279.h"
#include "System_Core_System_Func_2_gen32479265.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Tuple_3_gen1650899102.h"
#include "mscorlib_System_Action_1_gen1799351807.h"
#include "mscorlib_System_Action_1_gen1799351807MethodDeclarations.h"
#include "System_Core_System_Func_2_gen32479265MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_From2732089756.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_From2732089756MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_From2442263458.h"
#include "System_Core_System_Func_2_gen202992490.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Tuple_4_gen1027230281.h"
#include "mscorlib_System_Action_1_gen1175682986.h"
#include "mscorlib_System_Action_1_gen1175682986MethodDeclarations.h"
#include "System_Core_System_Func_2_gen202992490MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_FromE935221967.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_FromE935221967MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_FromE645395669.h"
#include "System_Core_System_Func_2_gen3830833163.h"
#include "UnityEngine_UnityEngine_Vector23525329788.h"
#include "mscorlib_System_Action_1_gen3673782493.h"
#include "mscorlib_System_Action_1_gen3673782493MethodDeclarations.h"
#include "System_Core_System_Func_2_gen3830833163MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_From1626038518MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_From2252139597MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_From2373242198MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Operat22353992MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_From1784294996MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_From3065932279MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Opera715044073MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_From2442263458MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Operat91375252MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_FromE645395669MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Oper2589474759MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_FromE208508663.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_FromE208508663MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_From3019221245.h"
#include "System_Core_System_Func_2_gen2937104577.h"
#include "mscorlib_System_EventHandler_1_gen1679684063.h"
#include "mscorlib_System_EventHandler_1_gen1679684063MethodDeclarations.h"
#include "System_Core_System_Func_2_gen2937104577MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_EventPattern_12903003978.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_EventPattern_12903003978MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_From3019221245MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Oper1967148949MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Grou3470189943.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Grou3470189943MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Group657897610.h"
#include "mscorlib_System_Nullable_1_gen1438485399MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Oper1657047628MethodDeclarations.h"
#include "mscorlib_System_Nullable_1_gen1438485399.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g3341028665.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g3341028665MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_CompositeDispo1894629977MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_RefCountDispos3288429070MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_CompositeDispo1894629977.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_RefCountDispos3288429070.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Grou2613254135.h"
#include "System_Core_System_Func_2_gen2135783352.h"
#include "System_Core_System_Func_2_gen2135783352MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Subject_1_gen2775141040.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Subject_1_gen2775141040MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Grou2613254135MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Oper1657047628.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_V3108056606.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Va968198463.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Va968198463MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_V3108056606MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Group657897610MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Opera370530870MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Igno1769081498.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Igno1769081498MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Igno2767931315.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Igno2767931315MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Last3329173242.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Last3329173242MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Last3225858136.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Last3745612479.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Last3745612479MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Last3225858136MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Mate1797935262.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Mate1797935262MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Mate1619437495.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Opera389018104MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Opera389018104.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Notification3315198013MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Notification_1_g38356375.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Notification3315198013.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Mate1619437495MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Oper3397468642MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Merg1753429969.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Merg1753429969MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Merg3534593302.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Oper3381670217.h"
#include "mscorlib_System_Threading_Monitor2071304733MethodDeclarations.h"
#include "System_System_Collections_Generic_Queue_1_gen2303992324.h"
#include "System_System_Collections_Generic_Queue_1_gen2303992324MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Merg3534593302MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Merg3474609587.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Merg3474609587MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Merge960805624.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Oper2064311169.h"
#include "System_System_Collections_Generic_Queue_1_gen4025171942.h"
#include "System_System_Collections_Generic_Queue_1_gen4025171942MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Merge960805624MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Merg3637992042.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Oper3381670217MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_SingleAssignme2336378823MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_SingleAssignme2336378823.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Merg1064204364.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Oper2064311169MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Merg_0.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Merg_0MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Merg1613081930.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Merg_1.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Merg_1MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Merg3334261548.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Merg1613081930MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Merg3334261548MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Merg3637992042MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Merg1064204364MethodDeclarations.h"

// System.Boolean UniRx.OptimizedObservableExtensions::IsRequiredSubscribeOnCurrentThread<System.Object>(UniRx.IObservable`1<!!0>)
extern "C"  bool OptimizedObservableExtensions_IsRequiredSubscribeOnCurrentThread_TisIl2CppObject_m1909184259_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* p0, const MethodInfo* method);
#define OptimizedObservableExtensions_IsRequiredSubscribeOnCurrentThread_TisIl2CppObject_m1909184259(__this /* static, unused */, p0, method) ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))OptimizedObservableExtensions_IsRequiredSubscribeOnCurrentThread_TisIl2CppObject_m1909184259_gshared)(__this /* static, unused */, p0, method)
// System.Boolean UniRx.OptimizedObservableExtensions::IsRequiredSubscribeOnCurrentThread<System.Boolean>(UniRx.IObservable`1<!!0>)
extern "C"  bool OptimizedObservableExtensions_IsRequiredSubscribeOnCurrentThread_TisBoolean_t211005341_m545890482_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* p0, const MethodInfo* method);
#define OptimizedObservableExtensions_IsRequiredSubscribeOnCurrentThread_TisBoolean_t211005341_m545890482(__this /* static, unused */, p0, method) ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))OptimizedObservableExtensions_IsRequiredSubscribeOnCurrentThread_TisBoolean_t211005341_m545890482_gshared)(__this /* static, unused */, p0, method)
// System.Boolean UniRx.OptimizedObservableExtensions::IsRequiredSubscribeOnCurrentThread<System.Int64>(UniRx.IObservable`1<!!0>)
extern "C"  bool OptimizedObservableExtensions_IsRequiredSubscribeOnCurrentThread_TisInt64_t2847414882_m2547913869_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* p0, const MethodInfo* method);
#define OptimizedObservableExtensions_IsRequiredSubscribeOnCurrentThread_TisInt64_t2847414882_m2547913869(__this /* static, unused */, p0, method) ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))OptimizedObservableExtensions_IsRequiredSubscribeOnCurrentThread_TisInt64_t2847414882_m2547913869_gshared)(__this /* static, unused */, p0, method)
// System.Boolean UniRx.OptimizedObservableExtensions::IsRequiredSubscribeOnCurrentThread<UniRx.Tuple`2<System.Object,System.Object>>(UniRx.IObservable`1<!!0>)
extern "C"  bool OptimizedObservableExtensions_IsRequiredSubscribeOnCurrentThread_TisTuple_2_t369261819_m2537354515_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* p0, const MethodInfo* method);
#define OptimizedObservableExtensions_IsRequiredSubscribeOnCurrentThread_TisTuple_2_t369261819_m2537354515(__this /* static, unused */, p0, method) ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))OptimizedObservableExtensions_IsRequiredSubscribeOnCurrentThread_TisTuple_2_t369261819_m2537354515_gshared)(__this /* static, unused */, p0, method)
// System.Boolean UniRx.OptimizedObservableExtensions::IsRequiredSubscribeOnCurrentThread<UniRx.Unit>(UniRx.IObservable`1<!!0>)
extern "C"  bool OptimizedObservableExtensions_IsRequiredSubscribeOnCurrentThread_TisUnit_t2558286038_m849542303_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* p0, const MethodInfo* method);
#define OptimizedObservableExtensions_IsRequiredSubscribeOnCurrentThread_TisUnit_t2558286038_m849542303(__this /* static, unused */, p0, method) ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))OptimizedObservableExtensions_IsRequiredSubscribeOnCurrentThread_TisUnit_t2558286038_m849542303_gshared)(__this /* static, unused */, p0, method)
// UniRx.Notification`1<!!0> UniRx.Notification::CreateOnNext<System.Object>(!!0)
extern "C"  Notification_1_t38356375 * Notification_CreateOnNext_TisIl2CppObject_m3363431495_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * p0, const MethodInfo* method);
#define Notification_CreateOnNext_TisIl2CppObject_m3363431495(__this /* static, unused */, p0, method) ((  Notification_1_t38356375 * (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))Notification_CreateOnNext_TisIl2CppObject_m3363431495_gshared)(__this /* static, unused */, p0, method)
// UniRx.Notification`1<!!0> UniRx.Notification::CreateOnError<System.Object>(System.Exception)
extern "C"  Notification_1_t38356375 * Notification_CreateOnError_TisIl2CppObject_m1777500536_gshared (Il2CppObject * __this /* static, unused */, Exception_t1967233988 * p0, const MethodInfo* method);
#define Notification_CreateOnError_TisIl2CppObject_m1777500536(__this /* static, unused */, p0, method) ((  Notification_1_t38356375 * (*) (Il2CppObject * /* static, unused */, Exception_t1967233988 *, const MethodInfo*))Notification_CreateOnError_TisIl2CppObject_m1777500536_gshared)(__this /* static, unused */, p0, method)
// UniRx.Notification`1<!!0> UniRx.Notification::CreateOnCompleted<System.Object>()
extern "C"  Notification_1_t38356375 * Notification_CreateOnCompleted_TisIl2CppObject_m2171509093_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define Notification_CreateOnCompleted_TisIl2CppObject_m2171509093(__this /* static, unused */, method) ((  Notification_1_t38356375 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))Notification_CreateOnCompleted_TisIl2CppObject_m2171509093_gshared)(__this /* static, unused */, method)
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void UniRx.Operators.EmptyObservable`1/Empty<UniRx.CollectionMoveEvent`1<UniRx.Tuple`2<System.Object,System.Object>>>::.ctor(UniRx.IObserver`1<T>,System.IDisposable)
extern "C"  void Empty__ctor_m1794107855_gshared (Empty_t112439784 * __this, Il2CppObject* ___observer0, Il2CppObject * ___cancel1, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = ___observer0;
		Il2CppObject * L_1 = ___cancel1;
		NullCheck((OperatorObserverBase_2_t1917096173 *)__this);
		((  void (*) (OperatorObserverBase_2_t1917096173 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((OperatorObserverBase_2_t1917096173 *)__this, (Il2CppObject*)L_0, (Il2CppObject *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		return;
	}
}
// System.Void UniRx.Operators.EmptyObservable`1/Empty<UniRx.CollectionMoveEvent`1<UniRx.Tuple`2<System.Object,System.Object>>>::OnNext(T)
extern Il2CppClass* Il2CppObject_il2cpp_TypeInfo_var;
extern const uint32_t Empty_OnNext_m1088463971_MetadataUsageId;
extern "C"  void Empty_OnNext_m1088463971_gshared (Empty_t112439784 * __this, CollectionMoveEvent_1_t2448589246  ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Empty_OnNext_m1088463971_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		Il2CppObject* L_0 = (Il2CppObject*)((OperatorObserverBase_2_t1917096173 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		CollectionMoveEvent_1_t2448589246  L_1 = ___value0;
		NullCheck((Il2CppObject*)L_0);
		InterfaceActionInvoker1< CollectionMoveEvent_1_t2448589246  >::Invoke(2 /* System.Void UniRx.IObserver`1<UniRx.CollectionMoveEvent`1<UniRx.Tuple`2<System.Object,System.Object>>>::OnNext(T) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), (Il2CppObject*)L_0, (CollectionMoveEvent_1_t2448589246 )L_1);
		goto IL_0021;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1967233988 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Il2CppObject_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_0013;
		throw e;
	}

CATCH_0013:
	{ // begin catch(System.Object)
		{
			NullCheck((OperatorObserverBase_2_t1917096173 *)__this);
			VirtActionInvoker0::Invoke(4 /* System.Void UniRx.Operators.OperatorObserverBase`2<UniRx.CollectionMoveEvent`1<UniRx.Tuple`2<System.Object,System.Object>>,UniRx.CollectionMoveEvent`1<UniRx.Tuple`2<System.Object,System.Object>>>::Dispose() */, (OperatorObserverBase_2_t1917096173 *)__this);
			IL2CPP_RAISE_MANAGED_EXCEPTION(__exception_local);
		}

IL_001c:
		{
			goto IL_0021;
		}
	} // end catch (depth: 1)

IL_0021:
	{
		return;
	}
}
// System.Void UniRx.Operators.EmptyObservable`1/Empty<UniRx.CollectionMoveEvent`1<UniRx.Tuple`2<System.Object,System.Object>>>::OnError(System.Exception)
extern "C"  void Empty_OnError_m1879939442_gshared (Empty_t112439784 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method)
{
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		Il2CppObject* L_0 = (Il2CppObject*)((OperatorObserverBase_2_t1917096173 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		Exception_t1967233988 * L_1 = ___error0;
		NullCheck((Il2CppObject*)L_0);
		InterfaceActionInvoker1< Exception_t1967233988 * >::Invoke(1 /* System.Void UniRx.IObserver`1<UniRx.CollectionMoveEvent`1<UniRx.Tuple`2<System.Object,System.Object>>>::OnError(System.Exception) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), (Il2CppObject*)L_0, (Exception_t1967233988 *)L_1);
		IL2CPP_LEAVE(0x1A, FINALLY_0013);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0013;
	}

FINALLY_0013:
	{ // begin finally (depth: 1)
		NullCheck((OperatorObserverBase_2_t1917096173 *)__this);
		VirtActionInvoker0::Invoke(4 /* System.Void UniRx.Operators.OperatorObserverBase`2<UniRx.CollectionMoveEvent`1<UniRx.Tuple`2<System.Object,System.Object>>,UniRx.CollectionMoveEvent`1<UniRx.Tuple`2<System.Object,System.Object>>>::Dispose() */, (OperatorObserverBase_2_t1917096173 *)__this);
		IL2CPP_END_FINALLY(19)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(19)
	{
		IL2CPP_JUMP_TBL(0x1A, IL_001a)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_001a:
	{
		return;
	}
}
// System.Void UniRx.Operators.EmptyObservable`1/Empty<UniRx.CollectionMoveEvent`1<UniRx.Tuple`2<System.Object,System.Object>>>::OnCompleted()
extern "C"  void Empty_OnCompleted_m846292933_gshared (Empty_t112439784 * __this, const MethodInfo* method)
{
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		Il2CppObject* L_0 = (Il2CppObject*)((OperatorObserverBase_2_t1917096173 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		NullCheck((Il2CppObject*)L_0);
		InterfaceActionInvoker0::Invoke(0 /* System.Void UniRx.IObserver`1<UniRx.CollectionMoveEvent`1<UniRx.Tuple`2<System.Object,System.Object>>>::OnCompleted() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), (Il2CppObject*)L_0);
		IL2CPP_LEAVE(0x19, FINALLY_0012);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0012;
	}

FINALLY_0012:
	{ // begin finally (depth: 1)
		NullCheck((OperatorObserverBase_2_t1917096173 *)__this);
		VirtActionInvoker0::Invoke(4 /* System.Void UniRx.Operators.OperatorObserverBase`2<UniRx.CollectionMoveEvent`1<UniRx.Tuple`2<System.Object,System.Object>>,UniRx.CollectionMoveEvent`1<UniRx.Tuple`2<System.Object,System.Object>>>::Dispose() */, (OperatorObserverBase_2_t1917096173 *)__this);
		IL2CPP_END_FINALLY(18)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(18)
	{
		IL2CPP_JUMP_TBL(0x19, IL_0019)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0019:
	{
		return;
	}
}
// System.Void UniRx.Operators.EmptyObservable`1/Empty<UniRx.CollectionRemoveEvent`1<System.Object>>::.ctor(UniRx.IObserver`1<T>,System.IDisposable)
extern "C"  void Empty__ctor_m284600178_gshared (Empty_t2201455558 * __this, Il2CppObject* ___observer0, Il2CppObject * ___cancel1, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = ___observer0;
		Il2CppObject * L_1 = ___cancel1;
		NullCheck((OperatorObserverBase_2_t3997058997 *)__this);
		((  void (*) (OperatorObserverBase_2_t3997058997 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((OperatorObserverBase_2_t3997058997 *)__this, (Il2CppObject*)L_0, (Il2CppObject *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		return;
	}
}
// System.Void UniRx.Operators.EmptyObservable`1/Empty<UniRx.CollectionRemoveEvent`1<System.Object>>::OnNext(T)
extern Il2CppClass* Il2CppObject_il2cpp_TypeInfo_var;
extern const uint32_t Empty_OnNext_m2147868896_MetadataUsageId;
extern "C"  void Empty_OnNext_m2147868896_gshared (Empty_t2201455558 * __this, CollectionRemoveEvent_1_t242637724  ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Empty_OnNext_m2147868896_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		Il2CppObject* L_0 = (Il2CppObject*)((OperatorObserverBase_2_t3997058997 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		CollectionRemoveEvent_1_t242637724  L_1 = ___value0;
		NullCheck((Il2CppObject*)L_0);
		InterfaceActionInvoker1< CollectionRemoveEvent_1_t242637724  >::Invoke(2 /* System.Void UniRx.IObserver`1<UniRx.CollectionRemoveEvent`1<System.Object>>::OnNext(T) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), (Il2CppObject*)L_0, (CollectionRemoveEvent_1_t242637724 )L_1);
		goto IL_0021;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1967233988 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Il2CppObject_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_0013;
		throw e;
	}

CATCH_0013:
	{ // begin catch(System.Object)
		{
			NullCheck((OperatorObserverBase_2_t3997058997 *)__this);
			VirtActionInvoker0::Invoke(4 /* System.Void UniRx.Operators.OperatorObserverBase`2<UniRx.CollectionRemoveEvent`1<System.Object>,UniRx.CollectionRemoveEvent`1<System.Object>>::Dispose() */, (OperatorObserverBase_2_t3997058997 *)__this);
			IL2CPP_RAISE_MANAGED_EXCEPTION(__exception_local);
		}

IL_001c:
		{
			goto IL_0021;
		}
	} // end catch (depth: 1)

IL_0021:
	{
		return;
	}
}
// System.Void UniRx.Operators.EmptyObservable`1/Empty<UniRx.CollectionRemoveEvent`1<System.Object>>::OnError(System.Exception)
extern "C"  void Empty_OnError_m4163648495_gshared (Empty_t2201455558 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method)
{
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		Il2CppObject* L_0 = (Il2CppObject*)((OperatorObserverBase_2_t3997058997 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		Exception_t1967233988 * L_1 = ___error0;
		NullCheck((Il2CppObject*)L_0);
		InterfaceActionInvoker1< Exception_t1967233988 * >::Invoke(1 /* System.Void UniRx.IObserver`1<UniRx.CollectionRemoveEvent`1<System.Object>>::OnError(System.Exception) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), (Il2CppObject*)L_0, (Exception_t1967233988 *)L_1);
		IL2CPP_LEAVE(0x1A, FINALLY_0013);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0013;
	}

FINALLY_0013:
	{ // begin finally (depth: 1)
		NullCheck((OperatorObserverBase_2_t3997058997 *)__this);
		VirtActionInvoker0::Invoke(4 /* System.Void UniRx.Operators.OperatorObserverBase`2<UniRx.CollectionRemoveEvent`1<System.Object>,UniRx.CollectionRemoveEvent`1<System.Object>>::Dispose() */, (OperatorObserverBase_2_t3997058997 *)__this);
		IL2CPP_END_FINALLY(19)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(19)
	{
		IL2CPP_JUMP_TBL(0x1A, IL_001a)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_001a:
	{
		return;
	}
}
// System.Void UniRx.Operators.EmptyObservable`1/Empty<UniRx.CollectionRemoveEvent`1<System.Object>>::OnCompleted()
extern "C"  void Empty_OnCompleted_m2876906946_gshared (Empty_t2201455558 * __this, const MethodInfo* method)
{
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		Il2CppObject* L_0 = (Il2CppObject*)((OperatorObserverBase_2_t3997058997 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		NullCheck((Il2CppObject*)L_0);
		InterfaceActionInvoker0::Invoke(0 /* System.Void UniRx.IObserver`1<UniRx.CollectionRemoveEvent`1<System.Object>>::OnCompleted() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), (Il2CppObject*)L_0);
		IL2CPP_LEAVE(0x19, FINALLY_0012);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0012;
	}

FINALLY_0012:
	{ // begin finally (depth: 1)
		NullCheck((OperatorObserverBase_2_t3997058997 *)__this);
		VirtActionInvoker0::Invoke(4 /* System.Void UniRx.Operators.OperatorObserverBase`2<UniRx.CollectionRemoveEvent`1<System.Object>,UniRx.CollectionRemoveEvent`1<System.Object>>::Dispose() */, (OperatorObserverBase_2_t3997058997 *)__this);
		IL2CPP_END_FINALLY(18)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(18)
	{
		IL2CPP_JUMP_TBL(0x19, IL_0019)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0019:
	{
		return;
	}
}
// System.Void UniRx.Operators.EmptyObservable`1/Empty<UniRx.CollectionRemoveEvent`1<UniRx.Tuple`2<System.Object,System.Object>>>::.ctor(UniRx.IObserver`1<T>,System.IDisposable)
extern "C"  void Empty__ctor_m1197971842_gshared (Empty_t1733610957 * __this, Il2CppObject* ___observer0, Il2CppObject * ___cancel1, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = ___observer0;
		Il2CppObject * L_1 = ___cancel1;
		NullCheck((OperatorObserverBase_2_t3120657337 *)__this);
		((  void (*) (OperatorObserverBase_2_t3120657337 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((OperatorObserverBase_2_t3120657337 *)__this, (Il2CppObject*)L_0, (Il2CppObject *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		return;
	}
}
// System.Void UniRx.Operators.EmptyObservable`1/Empty<UniRx.CollectionRemoveEvent`1<UniRx.Tuple`2<System.Object,System.Object>>>::OnNext(T)
extern Il2CppClass* Il2CppObject_il2cpp_TypeInfo_var;
extern const uint32_t Empty_OnNext_m2780144848_MetadataUsageId;
extern "C"  void Empty_OnNext_m2780144848_gshared (Empty_t1733610957 * __this, CollectionRemoveEvent_1_t4069760419  ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Empty_OnNext_m2780144848_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		Il2CppObject* L_0 = (Il2CppObject*)((OperatorObserverBase_2_t3120657337 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		CollectionRemoveEvent_1_t4069760419  L_1 = ___value0;
		NullCheck((Il2CppObject*)L_0);
		InterfaceActionInvoker1< CollectionRemoveEvent_1_t4069760419  >::Invoke(2 /* System.Void UniRx.IObserver`1<UniRx.CollectionRemoveEvent`1<UniRx.Tuple`2<System.Object,System.Object>>>::OnNext(T) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), (Il2CppObject*)L_0, (CollectionRemoveEvent_1_t4069760419 )L_1);
		goto IL_0021;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1967233988 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Il2CppObject_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_0013;
		throw e;
	}

CATCH_0013:
	{ // begin catch(System.Object)
		{
			NullCheck((OperatorObserverBase_2_t3120657337 *)__this);
			VirtActionInvoker0::Invoke(4 /* System.Void UniRx.Operators.OperatorObserverBase`2<UniRx.CollectionRemoveEvent`1<UniRx.Tuple`2<System.Object,System.Object>>,UniRx.CollectionRemoveEvent`1<UniRx.Tuple`2<System.Object,System.Object>>>::Dispose() */, (OperatorObserverBase_2_t3120657337 *)__this);
			IL2CPP_RAISE_MANAGED_EXCEPTION(__exception_local);
		}

IL_001c:
		{
			goto IL_0021;
		}
	} // end catch (depth: 1)

IL_0021:
	{
		return;
	}
}
// System.Void UniRx.Operators.EmptyObservable`1/Empty<UniRx.CollectionRemoveEvent`1<UniRx.Tuple`2<System.Object,System.Object>>>::OnError(System.Exception)
extern "C"  void Empty_OnError_m3811975135_gshared (Empty_t1733610957 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method)
{
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		Il2CppObject* L_0 = (Il2CppObject*)((OperatorObserverBase_2_t3120657337 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		Exception_t1967233988 * L_1 = ___error0;
		NullCheck((Il2CppObject*)L_0);
		InterfaceActionInvoker1< Exception_t1967233988 * >::Invoke(1 /* System.Void UniRx.IObserver`1<UniRx.CollectionRemoveEvent`1<UniRx.Tuple`2<System.Object,System.Object>>>::OnError(System.Exception) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), (Il2CppObject*)L_0, (Exception_t1967233988 *)L_1);
		IL2CPP_LEAVE(0x1A, FINALLY_0013);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0013;
	}

FINALLY_0013:
	{ // begin finally (depth: 1)
		NullCheck((OperatorObserverBase_2_t3120657337 *)__this);
		VirtActionInvoker0::Invoke(4 /* System.Void UniRx.Operators.OperatorObserverBase`2<UniRx.CollectionRemoveEvent`1<UniRx.Tuple`2<System.Object,System.Object>>,UniRx.CollectionRemoveEvent`1<UniRx.Tuple`2<System.Object,System.Object>>>::Dispose() */, (OperatorObserverBase_2_t3120657337 *)__this);
		IL2CPP_END_FINALLY(19)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(19)
	{
		IL2CPP_JUMP_TBL(0x1A, IL_001a)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_001a:
	{
		return;
	}
}
// System.Void UniRx.Operators.EmptyObservable`1/Empty<UniRx.CollectionRemoveEvent`1<UniRx.Tuple`2<System.Object,System.Object>>>::OnCompleted()
extern "C"  void Empty_OnCompleted_m717646258_gshared (Empty_t1733610957 * __this, const MethodInfo* method)
{
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		Il2CppObject* L_0 = (Il2CppObject*)((OperatorObserverBase_2_t3120657337 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		NullCheck((Il2CppObject*)L_0);
		InterfaceActionInvoker0::Invoke(0 /* System.Void UniRx.IObserver`1<UniRx.CollectionRemoveEvent`1<UniRx.Tuple`2<System.Object,System.Object>>>::OnCompleted() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), (Il2CppObject*)L_0);
		IL2CPP_LEAVE(0x19, FINALLY_0012);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0012;
	}

FINALLY_0012:
	{ // begin finally (depth: 1)
		NullCheck((OperatorObserverBase_2_t3120657337 *)__this);
		VirtActionInvoker0::Invoke(4 /* System.Void UniRx.Operators.OperatorObserverBase`2<UniRx.CollectionRemoveEvent`1<UniRx.Tuple`2<System.Object,System.Object>>,UniRx.CollectionRemoveEvent`1<UniRx.Tuple`2<System.Object,System.Object>>>::Dispose() */, (OperatorObserverBase_2_t3120657337 *)__this);
		IL2CPP_END_FINALLY(18)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(18)
	{
		IL2CPP_JUMP_TBL(0x19, IL_0019)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0019:
	{
		return;
	}
}
// System.Void UniRx.Operators.EmptyObservable`1/Empty<UniRx.CollectionReplaceEvent`1<System.Object>>::.ctor(UniRx.IObserver`1<T>,System.IDisposable)
extern "C"  void Empty__ctor_m2550362238_gshared (Empty_t3800568370 * __this, Il2CppObject* ___observer0, Il2CppObject * ___cancel1, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = ___observer0;
		Il2CppObject * L_1 = ___cancel1;
		NullCheck((OperatorObserverBase_2_t3791266949 *)__this);
		((  void (*) (OperatorObserverBase_2_t3791266949 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((OperatorObserverBase_2_t3791266949 *)__this, (Il2CppObject*)L_0, (Il2CppObject *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		return;
	}
}
// System.Void UniRx.Operators.EmptyObservable`1/Empty<UniRx.CollectionReplaceEvent`1<System.Object>>::OnNext(T)
extern Il2CppClass* Il2CppObject_il2cpp_TypeInfo_var;
extern const uint32_t Empty_OnNext_m2739236948_MetadataUsageId;
extern "C"  void Empty_OnNext_m2739236948_gshared (Empty_t3800568370 * __this, CollectionReplaceEvent_1_t1841750536  ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Empty_OnNext_m2739236948_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		Il2CppObject* L_0 = (Il2CppObject*)((OperatorObserverBase_2_t3791266949 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		CollectionReplaceEvent_1_t1841750536  L_1 = ___value0;
		NullCheck((Il2CppObject*)L_0);
		InterfaceActionInvoker1< CollectionReplaceEvent_1_t1841750536  >::Invoke(2 /* System.Void UniRx.IObserver`1<UniRx.CollectionReplaceEvent`1<System.Object>>::OnNext(T) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), (Il2CppObject*)L_0, (CollectionReplaceEvent_1_t1841750536 )L_1);
		goto IL_0021;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1967233988 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Il2CppObject_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_0013;
		throw e;
	}

CATCH_0013:
	{ // begin catch(System.Object)
		{
			NullCheck((OperatorObserverBase_2_t3791266949 *)__this);
			VirtActionInvoker0::Invoke(4 /* System.Void UniRx.Operators.OperatorObserverBase`2<UniRx.CollectionReplaceEvent`1<System.Object>,UniRx.CollectionReplaceEvent`1<System.Object>>::Dispose() */, (OperatorObserverBase_2_t3791266949 *)__this);
			IL2CPP_RAISE_MANAGED_EXCEPTION(__exception_local);
		}

IL_001c:
		{
			goto IL_0021;
		}
	} // end catch (depth: 1)

IL_0021:
	{
		return;
	}
}
// System.Void UniRx.Operators.EmptyObservable`1/Empty<UniRx.CollectionReplaceEvent`1<System.Object>>::OnError(System.Exception)
extern "C"  void Empty_OnError_m761684835_gshared (Empty_t3800568370 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method)
{
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		Il2CppObject* L_0 = (Il2CppObject*)((OperatorObserverBase_2_t3791266949 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		Exception_t1967233988 * L_1 = ___error0;
		NullCheck((Il2CppObject*)L_0);
		InterfaceActionInvoker1< Exception_t1967233988 * >::Invoke(1 /* System.Void UniRx.IObserver`1<UniRx.CollectionReplaceEvent`1<System.Object>>::OnError(System.Exception) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), (Il2CppObject*)L_0, (Exception_t1967233988 *)L_1);
		IL2CPP_LEAVE(0x1A, FINALLY_0013);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0013;
	}

FINALLY_0013:
	{ // begin finally (depth: 1)
		NullCheck((OperatorObserverBase_2_t3791266949 *)__this);
		VirtActionInvoker0::Invoke(4 /* System.Void UniRx.Operators.OperatorObserverBase`2<UniRx.CollectionReplaceEvent`1<System.Object>,UniRx.CollectionReplaceEvent`1<System.Object>>::Dispose() */, (OperatorObserverBase_2_t3791266949 *)__this);
		IL2CPP_END_FINALLY(19)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(19)
	{
		IL2CPP_JUMP_TBL(0x1A, IL_001a)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_001a:
	{
		return;
	}
}
// System.Void UniRx.Operators.EmptyObservable`1/Empty<UniRx.CollectionReplaceEvent`1<System.Object>>::OnCompleted()
extern "C"  void Empty_OnCompleted_m4240233270_gshared (Empty_t3800568370 * __this, const MethodInfo* method)
{
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		Il2CppObject* L_0 = (Il2CppObject*)((OperatorObserverBase_2_t3791266949 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		NullCheck((Il2CppObject*)L_0);
		InterfaceActionInvoker0::Invoke(0 /* System.Void UniRx.IObserver`1<UniRx.CollectionReplaceEvent`1<System.Object>>::OnCompleted() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), (Il2CppObject*)L_0);
		IL2CPP_LEAVE(0x19, FINALLY_0012);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0012;
	}

FINALLY_0012:
	{ // begin finally (depth: 1)
		NullCheck((OperatorObserverBase_2_t3791266949 *)__this);
		VirtActionInvoker0::Invoke(4 /* System.Void UniRx.Operators.OperatorObserverBase`2<UniRx.CollectionReplaceEvent`1<System.Object>,UniRx.CollectionReplaceEvent`1<System.Object>>::Dispose() */, (OperatorObserverBase_2_t3791266949 *)__this);
		IL2CPP_END_FINALLY(18)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(18)
	{
		IL2CPP_JUMP_TBL(0x19, IL_0019)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0019:
	{
		return;
	}
}
// System.Void UniRx.Operators.EmptyObservable`1/Empty<UniRx.CollectionReplaceEvent`1<UniRx.Tuple`2<System.Object,System.Object>>>::.ctor(UniRx.IObserver`1<T>,System.IDisposable)
extern "C"  void Empty__ctor_m3333651446_gshared (Empty_t3332723769 * __this, Il2CppObject* ___observer0, Il2CppObject * ___cancel1, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = ___observer0;
		Il2CppObject * L_1 = ___cancel1;
		NullCheck((OperatorObserverBase_2_t2914865289 *)__this);
		((  void (*) (OperatorObserverBase_2_t2914865289 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((OperatorObserverBase_2_t2914865289 *)__this, (Il2CppObject*)L_0, (Il2CppObject *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		return;
	}
}
// System.Void UniRx.Operators.EmptyObservable`1/Empty<UniRx.CollectionReplaceEvent`1<UniRx.Tuple`2<System.Object,System.Object>>>::OnNext(T)
extern Il2CppClass* Il2CppObject_il2cpp_TypeInfo_var;
extern const uint32_t Empty_OnNext_m1626487260_MetadataUsageId;
extern "C"  void Empty_OnNext_m1626487260_gshared (Empty_t3332723769 * __this, CollectionReplaceEvent_1_t1373905935  ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Empty_OnNext_m1626487260_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		Il2CppObject* L_0 = (Il2CppObject*)((OperatorObserverBase_2_t2914865289 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		CollectionReplaceEvent_1_t1373905935  L_1 = ___value0;
		NullCheck((Il2CppObject*)L_0);
		InterfaceActionInvoker1< CollectionReplaceEvent_1_t1373905935  >::Invoke(2 /* System.Void UniRx.IObserver`1<UniRx.CollectionReplaceEvent`1<UniRx.Tuple`2<System.Object,System.Object>>>::OnNext(T) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), (Il2CppObject*)L_0, (CollectionReplaceEvent_1_t1373905935 )L_1);
		goto IL_0021;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1967233988 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Il2CppObject_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_0013;
		throw e;
	}

CATCH_0013:
	{ // begin catch(System.Object)
		{
			NullCheck((OperatorObserverBase_2_t2914865289 *)__this);
			VirtActionInvoker0::Invoke(4 /* System.Void UniRx.Operators.OperatorObserverBase`2<UniRx.CollectionReplaceEvent`1<UniRx.Tuple`2<System.Object,System.Object>>,UniRx.CollectionReplaceEvent`1<UniRx.Tuple`2<System.Object,System.Object>>>::Dispose() */, (OperatorObserverBase_2_t2914865289 *)__this);
			IL2CPP_RAISE_MANAGED_EXCEPTION(__exception_local);
		}

IL_001c:
		{
			goto IL_0021;
		}
	} // end catch (depth: 1)

IL_0021:
	{
		return;
	}
}
// System.Void UniRx.Operators.EmptyObservable`1/Empty<UniRx.CollectionReplaceEvent`1<UniRx.Tuple`2<System.Object,System.Object>>>::OnError(System.Exception)
extern "C"  void Empty_OnError_m1285496043_gshared (Empty_t3332723769 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method)
{
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		Il2CppObject* L_0 = (Il2CppObject*)((OperatorObserverBase_2_t2914865289 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		Exception_t1967233988 * L_1 = ___error0;
		NullCheck((Il2CppObject*)L_0);
		InterfaceActionInvoker1< Exception_t1967233988 * >::Invoke(1 /* System.Void UniRx.IObserver`1<UniRx.CollectionReplaceEvent`1<UniRx.Tuple`2<System.Object,System.Object>>>::OnError(System.Exception) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), (Il2CppObject*)L_0, (Exception_t1967233988 *)L_1);
		IL2CPP_LEAVE(0x1A, FINALLY_0013);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0013;
	}

FINALLY_0013:
	{ // begin finally (depth: 1)
		NullCheck((OperatorObserverBase_2_t2914865289 *)__this);
		VirtActionInvoker0::Invoke(4 /* System.Void UniRx.Operators.OperatorObserverBase`2<UniRx.CollectionReplaceEvent`1<UniRx.Tuple`2<System.Object,System.Object>>,UniRx.CollectionReplaceEvent`1<UniRx.Tuple`2<System.Object,System.Object>>>::Dispose() */, (OperatorObserverBase_2_t2914865289 *)__this);
		IL2CPP_END_FINALLY(19)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(19)
	{
		IL2CPP_JUMP_TBL(0x1A, IL_001a)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_001a:
	{
		return;
	}
}
// System.Void UniRx.Operators.EmptyObservable`1/Empty<UniRx.CollectionReplaceEvent`1<UniRx.Tuple`2<System.Object,System.Object>>>::OnCompleted()
extern "C"  void Empty_OnCompleted_m475633854_gshared (Empty_t3332723769 * __this, const MethodInfo* method)
{
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		Il2CppObject* L_0 = (Il2CppObject*)((OperatorObserverBase_2_t2914865289 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		NullCheck((Il2CppObject*)L_0);
		InterfaceActionInvoker0::Invoke(0 /* System.Void UniRx.IObserver`1<UniRx.CollectionReplaceEvent`1<UniRx.Tuple`2<System.Object,System.Object>>>::OnCompleted() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), (Il2CppObject*)L_0);
		IL2CPP_LEAVE(0x19, FINALLY_0012);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0012;
	}

FINALLY_0012:
	{ // begin finally (depth: 1)
		NullCheck((OperatorObserverBase_2_t2914865289 *)__this);
		VirtActionInvoker0::Invoke(4 /* System.Void UniRx.Operators.OperatorObserverBase`2<UniRx.CollectionReplaceEvent`1<UniRx.Tuple`2<System.Object,System.Object>>,UniRx.CollectionReplaceEvent`1<UniRx.Tuple`2<System.Object,System.Object>>>::Dispose() */, (OperatorObserverBase_2_t2914865289 *)__this);
		IL2CPP_END_FINALLY(18)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(18)
	{
		IL2CPP_JUMP_TBL(0x19, IL_0019)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0019:
	{
		return;
	}
}
// System.Void UniRx.Operators.EmptyObservable`1/Empty<UniRx.DictionaryAddEvent`2<System.Object,System.Object>>::.ctor(UniRx.IObserver`1<T>,System.IDisposable)
extern "C"  void Empty__ctor_m681291356_gshared (Empty_t2209798842 * __this, Il2CppObject* ___observer0, Il2CppObject * ___cancel1, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = ___observer0;
		Il2CppObject * L_1 = ___cancel1;
		NullCheck((OperatorObserverBase_2_t3051653477 *)__this);
		((  void (*) (OperatorObserverBase_2_t3051653477 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((OperatorObserverBase_2_t3051653477 *)__this, (Il2CppObject*)L_0, (Il2CppObject *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		return;
	}
}
// System.Void UniRx.Operators.EmptyObservable`1/Empty<UniRx.DictionaryAddEvent`2<System.Object,System.Object>>::OnNext(T)
extern Il2CppClass* Il2CppObject_il2cpp_TypeInfo_var;
extern const uint32_t Empty_OnNext_m4036986806_MetadataUsageId;
extern "C"  void Empty_OnNext_m4036986806_gshared (Empty_t2209798842 * __this, DictionaryAddEvent_2_t250981008  ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Empty_OnNext_m4036986806_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		Il2CppObject* L_0 = (Il2CppObject*)((OperatorObserverBase_2_t3051653477 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		DictionaryAddEvent_2_t250981008  L_1 = ___value0;
		NullCheck((Il2CppObject*)L_0);
		InterfaceActionInvoker1< DictionaryAddEvent_2_t250981008  >::Invoke(2 /* System.Void UniRx.IObserver`1<UniRx.DictionaryAddEvent`2<System.Object,System.Object>>::OnNext(T) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), (Il2CppObject*)L_0, (DictionaryAddEvent_2_t250981008 )L_1);
		goto IL_0021;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1967233988 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Il2CppObject_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_0013;
		throw e;
	}

CATCH_0013:
	{ // begin catch(System.Object)
		{
			NullCheck((OperatorObserverBase_2_t3051653477 *)__this);
			VirtActionInvoker0::Invoke(4 /* System.Void UniRx.Operators.OperatorObserverBase`2<UniRx.DictionaryAddEvent`2<System.Object,System.Object>,UniRx.DictionaryAddEvent`2<System.Object,System.Object>>::Dispose() */, (OperatorObserverBase_2_t3051653477 *)__this);
			IL2CPP_RAISE_MANAGED_EXCEPTION(__exception_local);
		}

IL_001c:
		{
			goto IL_0021;
		}
	} // end catch (depth: 1)

IL_0021:
	{
		return;
	}
}
// System.Void UniRx.Operators.EmptyObservable`1/Empty<UniRx.DictionaryAddEvent`2<System.Object,System.Object>>::OnError(System.Exception)
extern "C"  void Empty_OnError_m2864281285_gshared (Empty_t2209798842 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method)
{
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		Il2CppObject* L_0 = (Il2CppObject*)((OperatorObserverBase_2_t3051653477 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		Exception_t1967233988 * L_1 = ___error0;
		NullCheck((Il2CppObject*)L_0);
		InterfaceActionInvoker1< Exception_t1967233988 * >::Invoke(1 /* System.Void UniRx.IObserver`1<UniRx.DictionaryAddEvent`2<System.Object,System.Object>>::OnError(System.Exception) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), (Il2CppObject*)L_0, (Exception_t1967233988 *)L_1);
		IL2CPP_LEAVE(0x1A, FINALLY_0013);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0013;
	}

FINALLY_0013:
	{ // begin finally (depth: 1)
		NullCheck((OperatorObserverBase_2_t3051653477 *)__this);
		VirtActionInvoker0::Invoke(4 /* System.Void UniRx.Operators.OperatorObserverBase`2<UniRx.DictionaryAddEvent`2<System.Object,System.Object>,UniRx.DictionaryAddEvent`2<System.Object,System.Object>>::Dispose() */, (OperatorObserverBase_2_t3051653477 *)__this);
		IL2CPP_END_FINALLY(19)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(19)
	{
		IL2CPP_JUMP_TBL(0x1A, IL_001a)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_001a:
	{
		return;
	}
}
// System.Void UniRx.Operators.EmptyObservable`1/Empty<UniRx.DictionaryAddEvent`2<System.Object,System.Object>>::OnCompleted()
extern "C"  void Empty_OnCompleted_m1452829080_gshared (Empty_t2209798842 * __this, const MethodInfo* method)
{
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		Il2CppObject* L_0 = (Il2CppObject*)((OperatorObserverBase_2_t3051653477 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		NullCheck((Il2CppObject*)L_0);
		InterfaceActionInvoker0::Invoke(0 /* System.Void UniRx.IObserver`1<UniRx.DictionaryAddEvent`2<System.Object,System.Object>>::OnCompleted() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), (Il2CppObject*)L_0);
		IL2CPP_LEAVE(0x19, FINALLY_0012);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0012;
	}

FINALLY_0012:
	{ // begin finally (depth: 1)
		NullCheck((OperatorObserverBase_2_t3051653477 *)__this);
		VirtActionInvoker0::Invoke(4 /* System.Void UniRx.Operators.OperatorObserverBase`2<UniRx.DictionaryAddEvent`2<System.Object,System.Object>,UniRx.DictionaryAddEvent`2<System.Object,System.Object>>::Dispose() */, (OperatorObserverBase_2_t3051653477 *)__this);
		IL2CPP_END_FINALLY(18)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(18)
	{
		IL2CPP_JUMP_TBL(0x19, IL_0019)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0019:
	{
		return;
	}
}
// System.Void UniRx.Operators.EmptyObservable`1/Empty<UniRx.DictionaryRemoveEvent`2<System.Object,System.Object>>::.ctor(UniRx.IObserver`1<T>,System.IDisposable)
extern "C"  void Empty__ctor_m1088404525_gshared (Empty_t986138829 * __this, Il2CppObject* ___observer0, Il2CppObject * ___cancel1, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = ___observer0;
		Il2CppObject * L_1 = ___cancel1;
		NullCheck((OperatorObserverBase_2_t3745721 *)__this);
		((  void (*) (OperatorObserverBase_2_t3745721 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((OperatorObserverBase_2_t3745721 *)__this, (Il2CppObject*)L_0, (Il2CppObject *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		return;
	}
}
// System.Void UniRx.Operators.EmptyObservable`1/Empty<UniRx.DictionaryRemoveEvent`2<System.Object,System.Object>>::OnNext(T)
extern Il2CppClass* Il2CppObject_il2cpp_TypeInfo_var;
extern const uint32_t Empty_OnNext_m4125513541_MetadataUsageId;
extern "C"  void Empty_OnNext_m4125513541_gshared (Empty_t986138829 * __this, DictionaryRemoveEvent_2_t3322288291  ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Empty_OnNext_m4125513541_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		Il2CppObject* L_0 = (Il2CppObject*)((OperatorObserverBase_2_t3745721 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		DictionaryRemoveEvent_2_t3322288291  L_1 = ___value0;
		NullCheck((Il2CppObject*)L_0);
		InterfaceActionInvoker1< DictionaryRemoveEvent_2_t3322288291  >::Invoke(2 /* System.Void UniRx.IObserver`1<UniRx.DictionaryRemoveEvent`2<System.Object,System.Object>>::OnNext(T) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), (Il2CppObject*)L_0, (DictionaryRemoveEvent_2_t3322288291 )L_1);
		goto IL_0021;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1967233988 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Il2CppObject_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_0013;
		throw e;
	}

CATCH_0013:
	{ // begin catch(System.Object)
		{
			NullCheck((OperatorObserverBase_2_t3745721 *)__this);
			VirtActionInvoker0::Invoke(4 /* System.Void UniRx.Operators.OperatorObserverBase`2<UniRx.DictionaryRemoveEvent`2<System.Object,System.Object>,UniRx.DictionaryRemoveEvent`2<System.Object,System.Object>>::Dispose() */, (OperatorObserverBase_2_t3745721 *)__this);
			IL2CPP_RAISE_MANAGED_EXCEPTION(__exception_local);
		}

IL_001c:
		{
			goto IL_0021;
		}
	} // end catch (depth: 1)

IL_0021:
	{
		return;
	}
}
// System.Void UniRx.Operators.EmptyObservable`1/Empty<UniRx.DictionaryRemoveEvent`2<System.Object,System.Object>>::OnError(System.Exception)
extern "C"  void Empty_OnError_m3445966932_gshared (Empty_t986138829 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method)
{
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		Il2CppObject* L_0 = (Il2CppObject*)((OperatorObserverBase_2_t3745721 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		Exception_t1967233988 * L_1 = ___error0;
		NullCheck((Il2CppObject*)L_0);
		InterfaceActionInvoker1< Exception_t1967233988 * >::Invoke(1 /* System.Void UniRx.IObserver`1<UniRx.DictionaryRemoveEvent`2<System.Object,System.Object>>::OnError(System.Exception) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), (Il2CppObject*)L_0, (Exception_t1967233988 *)L_1);
		IL2CPP_LEAVE(0x1A, FINALLY_0013);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0013;
	}

FINALLY_0013:
	{ // begin finally (depth: 1)
		NullCheck((OperatorObserverBase_2_t3745721 *)__this);
		VirtActionInvoker0::Invoke(4 /* System.Void UniRx.Operators.OperatorObserverBase`2<UniRx.DictionaryRemoveEvent`2<System.Object,System.Object>,UniRx.DictionaryRemoveEvent`2<System.Object,System.Object>>::Dispose() */, (OperatorObserverBase_2_t3745721 *)__this);
		IL2CPP_END_FINALLY(19)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(19)
	{
		IL2CPP_JUMP_TBL(0x1A, IL_001a)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_001a:
	{
		return;
	}
}
// System.Void UniRx.Operators.EmptyObservable`1/Empty<UniRx.DictionaryRemoveEvent`2<System.Object,System.Object>>::OnCompleted()
extern "C"  void Empty_OnCompleted_m3049183655_gshared (Empty_t986138829 * __this, const MethodInfo* method)
{
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		Il2CppObject* L_0 = (Il2CppObject*)((OperatorObserverBase_2_t3745721 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		NullCheck((Il2CppObject*)L_0);
		InterfaceActionInvoker0::Invoke(0 /* System.Void UniRx.IObserver`1<UniRx.DictionaryRemoveEvent`2<System.Object,System.Object>>::OnCompleted() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), (Il2CppObject*)L_0);
		IL2CPP_LEAVE(0x19, FINALLY_0012);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0012;
	}

FINALLY_0012:
	{ // begin finally (depth: 1)
		NullCheck((OperatorObserverBase_2_t3745721 *)__this);
		VirtActionInvoker0::Invoke(4 /* System.Void UniRx.Operators.OperatorObserverBase`2<UniRx.DictionaryRemoveEvent`2<System.Object,System.Object>,UniRx.DictionaryRemoveEvent`2<System.Object,System.Object>>::Dispose() */, (OperatorObserverBase_2_t3745721 *)__this);
		IL2CPP_END_FINALLY(18)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(18)
	{
		IL2CPP_JUMP_TBL(0x19, IL_0019)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0019:
	{
		return;
	}
}
// System.Void UniRx.Operators.EmptyObservable`1/Empty<UniRx.DictionaryReplaceEvent`2<System.Object,System.Object>>::.ctor(UniRx.IObserver`1<T>,System.IDisposable)
extern "C"  void Empty__ctor_m2762808105_gshared (Empty_t1442777601 * __this, Il2CppObject* ___observer0, Il2CppObject * ___cancel1, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = ___observer0;
		Il2CppObject * L_1 = ___cancel1;
		NullCheck((OperatorObserverBase_2_t154015849 *)__this);
		((  void (*) (OperatorObserverBase_2_t154015849 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((OperatorObserverBase_2_t154015849 *)__this, (Il2CppObject*)L_0, (Il2CppObject *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		return;
	}
}
// System.Void UniRx.Operators.EmptyObservable`1/Empty<UniRx.DictionaryReplaceEvent`2<System.Object,System.Object>>::OnNext(T)
extern Il2CppClass* Il2CppObject_il2cpp_TypeInfo_var;
extern const uint32_t Empty_OnNext_m4249852617_MetadataUsageId;
extern "C"  void Empty_OnNext_m4249852617_gshared (Empty_t1442777601 * __this, DictionaryReplaceEvent_2_t3778927063  ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Empty_OnNext_m4249852617_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		Il2CppObject* L_0 = (Il2CppObject*)((OperatorObserverBase_2_t154015849 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		DictionaryReplaceEvent_2_t3778927063  L_1 = ___value0;
		NullCheck((Il2CppObject*)L_0);
		InterfaceActionInvoker1< DictionaryReplaceEvent_2_t3778927063  >::Invoke(2 /* System.Void UniRx.IObserver`1<UniRx.DictionaryReplaceEvent`2<System.Object,System.Object>>::OnNext(T) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), (Il2CppObject*)L_0, (DictionaryReplaceEvent_2_t3778927063 )L_1);
		goto IL_0021;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1967233988 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Il2CppObject_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_0013;
		throw e;
	}

CATCH_0013:
	{ // begin catch(System.Object)
		{
			NullCheck((OperatorObserverBase_2_t154015849 *)__this);
			VirtActionInvoker0::Invoke(4 /* System.Void UniRx.Operators.OperatorObserverBase`2<UniRx.DictionaryReplaceEvent`2<System.Object,System.Object>,UniRx.DictionaryReplaceEvent`2<System.Object,System.Object>>::Dispose() */, (OperatorObserverBase_2_t154015849 *)__this);
			IL2CPP_RAISE_MANAGED_EXCEPTION(__exception_local);
		}

IL_001c:
		{
			goto IL_0021;
		}
	} // end catch (depth: 1)

IL_0021:
	{
		return;
	}
}
// System.Void UniRx.Operators.EmptyObservable`1/Empty<UniRx.DictionaryReplaceEvent`2<System.Object,System.Object>>::OnError(System.Exception)
extern "C"  void Empty_OnError_m4018078680_gshared (Empty_t1442777601 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method)
{
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		Il2CppObject* L_0 = (Il2CppObject*)((OperatorObserverBase_2_t154015849 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		Exception_t1967233988 * L_1 = ___error0;
		NullCheck((Il2CppObject*)L_0);
		InterfaceActionInvoker1< Exception_t1967233988 * >::Invoke(1 /* System.Void UniRx.IObserver`1<UniRx.DictionaryReplaceEvent`2<System.Object,System.Object>>::OnError(System.Exception) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), (Il2CppObject*)L_0, (Exception_t1967233988 *)L_1);
		IL2CPP_LEAVE(0x1A, FINALLY_0013);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0013;
	}

FINALLY_0013:
	{ // begin finally (depth: 1)
		NullCheck((OperatorObserverBase_2_t154015849 *)__this);
		VirtActionInvoker0::Invoke(4 /* System.Void UniRx.Operators.OperatorObserverBase`2<UniRx.DictionaryReplaceEvent`2<System.Object,System.Object>,UniRx.DictionaryReplaceEvent`2<System.Object,System.Object>>::Dispose() */, (OperatorObserverBase_2_t154015849 *)__this);
		IL2CPP_END_FINALLY(19)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(19)
	{
		IL2CPP_JUMP_TBL(0x1A, IL_001a)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_001a:
	{
		return;
	}
}
// System.Void UniRx.Operators.EmptyObservable`1/Empty<UniRx.DictionaryReplaceEvent`2<System.Object,System.Object>>::OnCompleted()
extern "C"  void Empty_OnCompleted_m2551364395_gshared (Empty_t1442777601 * __this, const MethodInfo* method)
{
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		Il2CppObject* L_0 = (Il2CppObject*)((OperatorObserverBase_2_t154015849 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		NullCheck((Il2CppObject*)L_0);
		InterfaceActionInvoker0::Invoke(0 /* System.Void UniRx.IObserver`1<UniRx.DictionaryReplaceEvent`2<System.Object,System.Object>>::OnCompleted() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), (Il2CppObject*)L_0);
		IL2CPP_LEAVE(0x19, FINALLY_0012);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0012;
	}

FINALLY_0012:
	{ // begin finally (depth: 1)
		NullCheck((OperatorObserverBase_2_t154015849 *)__this);
		VirtActionInvoker0::Invoke(4 /* System.Void UniRx.Operators.OperatorObserverBase`2<UniRx.DictionaryReplaceEvent`2<System.Object,System.Object>,UniRx.DictionaryReplaceEvent`2<System.Object,System.Object>>::Dispose() */, (OperatorObserverBase_2_t154015849 *)__this);
		IL2CPP_END_FINALLY(18)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(18)
	{
		IL2CPP_JUMP_TBL(0x19, IL_0019)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0019:
	{
		return;
	}
}
// System.Void UniRx.Operators.EmptyObservable`1/Empty<UniRx.Unit>::.ctor(UniRx.IObserver`1<T>,System.IDisposable)
extern "C"  void Empty__ctor_m2303977857_gshared (Empty_t222136576 * __this, Il2CppObject* ___observer0, Il2CppObject * ___cancel1, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = ___observer0;
		Il2CppObject * L_1 = ___cancel1;
		NullCheck((OperatorObserverBase_2_t4165376397 *)__this);
		((  void (*) (OperatorObserverBase_2_t4165376397 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((OperatorObserverBase_2_t4165376397 *)__this, (Il2CppObject*)L_0, (Il2CppObject *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		return;
	}
}
// System.Void UniRx.Operators.EmptyObservable`1/Empty<UniRx.Unit>::OnNext(T)
extern Il2CppClass* Il2CppObject_il2cpp_TypeInfo_var;
extern const uint32_t Empty_OnNext_m1440470385_MetadataUsageId;
extern "C"  void Empty_OnNext_m1440470385_gshared (Empty_t222136576 * __this, Unit_t2558286038  ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Empty_OnNext_m1440470385_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		Il2CppObject* L_0 = (Il2CppObject*)((OperatorObserverBase_2_t4165376397 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		Unit_t2558286038  L_1 = ___value0;
		NullCheck((Il2CppObject*)L_0);
		InterfaceActionInvoker1< Unit_t2558286038  >::Invoke(2 /* System.Void UniRx.IObserver`1<UniRx.Unit>::OnNext(T) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), (Il2CppObject*)L_0, (Unit_t2558286038 )L_1);
		goto IL_0021;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1967233988 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Il2CppObject_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_0013;
		throw e;
	}

CATCH_0013:
	{ // begin catch(System.Object)
		{
			NullCheck((OperatorObserverBase_2_t4165376397 *)__this);
			VirtActionInvoker0::Invoke(4 /* System.Void UniRx.Operators.OperatorObserverBase`2<UniRx.Unit,UniRx.Unit>::Dispose() */, (OperatorObserverBase_2_t4165376397 *)__this);
			IL2CPP_RAISE_MANAGED_EXCEPTION(__exception_local);
		}

IL_001c:
		{
			goto IL_0021;
		}
	} // end catch (depth: 1)

IL_0021:
	{
		return;
	}
}
// System.Void UniRx.Operators.EmptyObservable`1/Empty<UniRx.Unit>::OnError(System.Exception)
extern "C"  void Empty_OnError_m466677376_gshared (Empty_t222136576 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method)
{
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		Il2CppObject* L_0 = (Il2CppObject*)((OperatorObserverBase_2_t4165376397 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		Exception_t1967233988 * L_1 = ___error0;
		NullCheck((Il2CppObject*)L_0);
		InterfaceActionInvoker1< Exception_t1967233988 * >::Invoke(1 /* System.Void UniRx.IObserver`1<UniRx.Unit>::OnError(System.Exception) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), (Il2CppObject*)L_0, (Exception_t1967233988 *)L_1);
		IL2CPP_LEAVE(0x1A, FINALLY_0013);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0013;
	}

FINALLY_0013:
	{ // begin finally (depth: 1)
		NullCheck((OperatorObserverBase_2_t4165376397 *)__this);
		VirtActionInvoker0::Invoke(4 /* System.Void UniRx.Operators.OperatorObserverBase`2<UniRx.Unit,UniRx.Unit>::Dispose() */, (OperatorObserverBase_2_t4165376397 *)__this);
		IL2CPP_END_FINALLY(19)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(19)
	{
		IL2CPP_JUMP_TBL(0x1A, IL_001a)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_001a:
	{
		return;
	}
}
// System.Void UniRx.Operators.EmptyObservable`1/Empty<UniRx.Unit>::OnCompleted()
extern "C"  void Empty_OnCompleted_m87122387_gshared (Empty_t222136576 * __this, const MethodInfo* method)
{
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		Il2CppObject* L_0 = (Il2CppObject*)((OperatorObserverBase_2_t4165376397 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		NullCheck((Il2CppObject*)L_0);
		InterfaceActionInvoker0::Invoke(0 /* System.Void UniRx.IObserver`1<UniRx.Unit>::OnCompleted() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), (Il2CppObject*)L_0);
		IL2CPP_LEAVE(0x19, FINALLY_0012);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0012;
	}

FINALLY_0012:
	{ // begin finally (depth: 1)
		NullCheck((OperatorObserverBase_2_t4165376397 *)__this);
		VirtActionInvoker0::Invoke(4 /* System.Void UniRx.Operators.OperatorObserverBase`2<UniRx.Unit,UniRx.Unit>::Dispose() */, (OperatorObserverBase_2_t4165376397 *)__this);
		IL2CPP_END_FINALLY(18)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(18)
	{
		IL2CPP_JUMP_TBL(0x19, IL_0019)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0019:
	{
		return;
	}
}
// System.Void UniRx.Operators.EmptyObservable`1<System.Double>::.ctor(UniRx.IScheduler)
extern "C"  void EmptyObservable_1__ctor_m1489914373_gshared (EmptyObservable_1_t3223706313 * __this, Il2CppObject * ___scheduler0, const MethodInfo* method)
{
	{
		NullCheck((OperatorObservableBase_1_t3893628881 *)__this);
		((  void (*) (OperatorObservableBase_1_t3893628881 *, bool, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((OperatorObservableBase_1_t3893628881 *)__this, (bool)0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		Il2CppObject * L_0 = ___scheduler0;
		__this->set_scheduler_1(L_0);
		return;
	}
}
// System.IDisposable UniRx.Operators.EmptyObservable`1<System.Double>::SubscribeCore(UniRx.IObserver`1<T>,System.IDisposable)
extern Il2CppClass* Scheduler_t103934925_il2cpp_TypeInfo_var;
extern Il2CppClass* Disposable_t3736388786_il2cpp_TypeInfo_var;
extern Il2CppClass* Action_t437523947_il2cpp_TypeInfo_var;
extern Il2CppClass* IScheduler_t2938318244_il2cpp_TypeInfo_var;
extern const uint32_t EmptyObservable_1_SubscribeCore_m339739215_MetadataUsageId;
extern "C"  Il2CppObject * EmptyObservable_1_SubscribeCore_m339739215_gshared (EmptyObservable_1_t3223706313 * __this, Il2CppObject* ___observer0, Il2CppObject * ___cancel1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (EmptyObservable_1_SubscribeCore_m339739215_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject* L_0 = ___observer0;
		Il2CppObject * L_1 = ___cancel1;
		Empty_t2493334448 * L_2 = (Empty_t2493334448 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		((  void (*) (Empty_t2493334448 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)(L_2, (Il2CppObject*)L_0, (Il2CppObject *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		___observer0 = (Il2CppObject*)L_2;
		Il2CppObject * L_3 = (Il2CppObject *)__this->get_scheduler_1();
		IL2CPP_RUNTIME_CLASS_INIT(Scheduler_t103934925_il2cpp_TypeInfo_var);
		Il2CppObject * L_4 = ((Scheduler_t103934925_StaticFields*)Scheduler_t103934925_il2cpp_TypeInfo_var->static_fields)->get_Immediate_1();
		if ((!(((Il2CppObject*)(Il2CppObject *)L_3) == ((Il2CppObject*)(Il2CppObject *)L_4))))
		{
			goto IL_0025;
		}
	}
	{
		Il2CppObject* L_5 = ___observer0;
		NullCheck((Il2CppObject*)L_5);
		InterfaceActionInvoker0::Invoke(0 /* System.Void UniRx.IObserver`1<System.Double>::OnCompleted() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4), (Il2CppObject*)L_5);
		IL2CPP_RUNTIME_CLASS_INIT(Disposable_t3736388786_il2cpp_TypeInfo_var);
		Il2CppObject * L_6 = ((Disposable_t3736388786_StaticFields*)Disposable_t3736388786_il2cpp_TypeInfo_var->static_fields)->get_Empty_0();
		return L_6;
	}

IL_0025:
	{
		Il2CppObject * L_7 = (Il2CppObject *)__this->get_scheduler_1();
		Il2CppObject* L_8 = ___observer0;
		Il2CppObject* L_9 = (Il2CppObject*)L_8;
		IntPtr_t L_10;
		L_10.set_m_value_0((void*)(void*)GetInterfaceMethodInfo(L_9, 0, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)));
		Action_t437523947 * L_11 = (Action_t437523947 *)il2cpp_codegen_object_new(Action_t437523947_il2cpp_TypeInfo_var);
		Action__ctor_m2957240604(L_11, (Il2CppObject *)L_9, (IntPtr_t)L_10, /*hidden argument*/NULL);
		NullCheck((Il2CppObject *)L_7);
		Il2CppObject * L_12 = InterfaceFuncInvoker1< Il2CppObject *, Action_t437523947 * >::Invoke(1 /* System.IDisposable UniRx.IScheduler::Schedule(System.Action) */, IScheduler_t2938318244_il2cpp_TypeInfo_var, (Il2CppObject *)L_7, (Action_t437523947 *)L_11);
		return L_12;
	}
}
// System.Void UniRx.Operators.EmptyObservable`1<System.Int32>::.ctor(UniRx.IScheduler)
extern "C"  void EmptyObservable_1__ctor_m350375600_gshared (EmptyObservable_1_t1241637190 * __this, Il2CppObject * ___scheduler0, const MethodInfo* method)
{
	{
		NullCheck((OperatorObservableBase_1_t1911559758 *)__this);
		((  void (*) (OperatorObservableBase_1_t1911559758 *, bool, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((OperatorObservableBase_1_t1911559758 *)__this, (bool)0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		Il2CppObject * L_0 = ___scheduler0;
		__this->set_scheduler_1(L_0);
		return;
	}
}
// System.IDisposable UniRx.Operators.EmptyObservable`1<System.Int32>::SubscribeCore(UniRx.IObserver`1<T>,System.IDisposable)
extern Il2CppClass* Scheduler_t103934925_il2cpp_TypeInfo_var;
extern Il2CppClass* Disposable_t3736388786_il2cpp_TypeInfo_var;
extern Il2CppClass* Action_t437523947_il2cpp_TypeInfo_var;
extern Il2CppClass* IScheduler_t2938318244_il2cpp_TypeInfo_var;
extern const uint32_t EmptyObservable_1_SubscribeCore_m2953186414_MetadataUsageId;
extern "C"  Il2CppObject * EmptyObservable_1_SubscribeCore_m2953186414_gshared (EmptyObservable_1_t1241637190 * __this, Il2CppObject* ___observer0, Il2CppObject * ___cancel1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (EmptyObservable_1_SubscribeCore_m2953186414_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject* L_0 = ___observer0;
		Il2CppObject * L_1 = ___cancel1;
		Empty_t511265325 * L_2 = (Empty_t511265325 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		((  void (*) (Empty_t511265325 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)(L_2, (Il2CppObject*)L_0, (Il2CppObject *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		___observer0 = (Il2CppObject*)L_2;
		Il2CppObject * L_3 = (Il2CppObject *)__this->get_scheduler_1();
		IL2CPP_RUNTIME_CLASS_INIT(Scheduler_t103934925_il2cpp_TypeInfo_var);
		Il2CppObject * L_4 = ((Scheduler_t103934925_StaticFields*)Scheduler_t103934925_il2cpp_TypeInfo_var->static_fields)->get_Immediate_1();
		if ((!(((Il2CppObject*)(Il2CppObject *)L_3) == ((Il2CppObject*)(Il2CppObject *)L_4))))
		{
			goto IL_0025;
		}
	}
	{
		Il2CppObject* L_5 = ___observer0;
		NullCheck((Il2CppObject*)L_5);
		InterfaceActionInvoker0::Invoke(0 /* System.Void UniRx.IObserver`1<System.Int32>::OnCompleted() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4), (Il2CppObject*)L_5);
		IL2CPP_RUNTIME_CLASS_INIT(Disposable_t3736388786_il2cpp_TypeInfo_var);
		Il2CppObject * L_6 = ((Disposable_t3736388786_StaticFields*)Disposable_t3736388786_il2cpp_TypeInfo_var->static_fields)->get_Empty_0();
		return L_6;
	}

IL_0025:
	{
		Il2CppObject * L_7 = (Il2CppObject *)__this->get_scheduler_1();
		Il2CppObject* L_8 = ___observer0;
		Il2CppObject* L_9 = (Il2CppObject*)L_8;
		IntPtr_t L_10;
		L_10.set_m_value_0((void*)(void*)GetInterfaceMethodInfo(L_9, 0, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)));
		Action_t437523947 * L_11 = (Action_t437523947 *)il2cpp_codegen_object_new(Action_t437523947_il2cpp_TypeInfo_var);
		Action__ctor_m2957240604(L_11, (Il2CppObject *)L_9, (IntPtr_t)L_10, /*hidden argument*/NULL);
		NullCheck((Il2CppObject *)L_7);
		Il2CppObject * L_12 = InterfaceFuncInvoker1< Il2CppObject *, Action_t437523947 * >::Invoke(1 /* System.IDisposable UniRx.IScheduler::Schedule(System.Action) */, IScheduler_t2938318244_il2cpp_TypeInfo_var, (Il2CppObject *)L_7, (Action_t437523947 *)L_11);
		return L_12;
	}
}
// System.Void UniRx.Operators.EmptyObservable`1<System.Int64>::.ctor(UniRx.IScheduler)
extern "C"  void EmptyObservable_1__ctor_m1509393999_gshared (EmptyObservable_1_t1241637285 * __this, Il2CppObject * ___scheduler0, const MethodInfo* method)
{
	{
		NullCheck((OperatorObservableBase_1_t1911559853 *)__this);
		((  void (*) (OperatorObservableBase_1_t1911559853 *, bool, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((OperatorObservableBase_1_t1911559853 *)__this, (bool)0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		Il2CppObject * L_0 = ___scheduler0;
		__this->set_scheduler_1(L_0);
		return;
	}
}
// System.IDisposable UniRx.Operators.EmptyObservable`1<System.Int64>::SubscribeCore(UniRx.IObserver`1<T>,System.IDisposable)
extern Il2CppClass* Scheduler_t103934925_il2cpp_TypeInfo_var;
extern Il2CppClass* Disposable_t3736388786_il2cpp_TypeInfo_var;
extern Il2CppClass* Action_t437523947_il2cpp_TypeInfo_var;
extern Il2CppClass* IScheduler_t2938318244_il2cpp_TypeInfo_var;
extern const uint32_t EmptyObservable_1_SubscribeCore_m4227965167_MetadataUsageId;
extern "C"  Il2CppObject * EmptyObservable_1_SubscribeCore_m4227965167_gshared (EmptyObservable_1_t1241637285 * __this, Il2CppObject* ___observer0, Il2CppObject * ___cancel1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (EmptyObservable_1_SubscribeCore_m4227965167_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject* L_0 = ___observer0;
		Il2CppObject * L_1 = ___cancel1;
		Empty_t511265420 * L_2 = (Empty_t511265420 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		((  void (*) (Empty_t511265420 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)(L_2, (Il2CppObject*)L_0, (Il2CppObject *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		___observer0 = (Il2CppObject*)L_2;
		Il2CppObject * L_3 = (Il2CppObject *)__this->get_scheduler_1();
		IL2CPP_RUNTIME_CLASS_INIT(Scheduler_t103934925_il2cpp_TypeInfo_var);
		Il2CppObject * L_4 = ((Scheduler_t103934925_StaticFields*)Scheduler_t103934925_il2cpp_TypeInfo_var->static_fields)->get_Immediate_1();
		if ((!(((Il2CppObject*)(Il2CppObject *)L_3) == ((Il2CppObject*)(Il2CppObject *)L_4))))
		{
			goto IL_0025;
		}
	}
	{
		Il2CppObject* L_5 = ___observer0;
		NullCheck((Il2CppObject*)L_5);
		InterfaceActionInvoker0::Invoke(0 /* System.Void UniRx.IObserver`1<System.Int64>::OnCompleted() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4), (Il2CppObject*)L_5);
		IL2CPP_RUNTIME_CLASS_INIT(Disposable_t3736388786_il2cpp_TypeInfo_var);
		Il2CppObject * L_6 = ((Disposable_t3736388786_StaticFields*)Disposable_t3736388786_il2cpp_TypeInfo_var->static_fields)->get_Empty_0();
		return L_6;
	}

IL_0025:
	{
		Il2CppObject * L_7 = (Il2CppObject *)__this->get_scheduler_1();
		Il2CppObject* L_8 = ___observer0;
		Il2CppObject* L_9 = (Il2CppObject*)L_8;
		IntPtr_t L_10;
		L_10.set_m_value_0((void*)(void*)GetInterfaceMethodInfo(L_9, 0, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)));
		Action_t437523947 * L_11 = (Action_t437523947 *)il2cpp_codegen_object_new(Action_t437523947_il2cpp_TypeInfo_var);
		Action__ctor_m2957240604(L_11, (Il2CppObject *)L_9, (IntPtr_t)L_10, /*hidden argument*/NULL);
		NullCheck((Il2CppObject *)L_7);
		Il2CppObject * L_12 = InterfaceFuncInvoker1< Il2CppObject *, Action_t437523947 * >::Invoke(1 /* System.IDisposable UniRx.IScheduler::Schedule(System.Action) */, IScheduler_t2938318244_il2cpp_TypeInfo_var, (Il2CppObject *)L_7, (Action_t437523947 *)L_11);
		return L_12;
	}
}
// System.Void UniRx.Operators.EmptyObservable`1<System.Object>::.ctor(UniRx.IScheduler)
extern "C"  void EmptyObservable_1__ctor_m2079974387_gshared (EmptyObservable_1_t3526296119 * __this, Il2CppObject * ___scheduler0, const MethodInfo* method)
{
	{
		NullCheck((OperatorObservableBase_1_t4196218687 *)__this);
		((  void (*) (OperatorObservableBase_1_t4196218687 *, bool, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((OperatorObservableBase_1_t4196218687 *)__this, (bool)0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		Il2CppObject * L_0 = ___scheduler0;
		__this->set_scheduler_1(L_0);
		return;
	}
}
// System.IDisposable UniRx.Operators.EmptyObservable`1<System.Object>::SubscribeCore(UniRx.IObserver`1<T>,System.IDisposable)
extern Il2CppClass* Scheduler_t103934925_il2cpp_TypeInfo_var;
extern Il2CppClass* Disposable_t3736388786_il2cpp_TypeInfo_var;
extern Il2CppClass* Action_t437523947_il2cpp_TypeInfo_var;
extern Il2CppClass* IScheduler_t2938318244_il2cpp_TypeInfo_var;
extern const uint32_t EmptyObservable_1_SubscribeCore_m3200035489_MetadataUsageId;
extern "C"  Il2CppObject * EmptyObservable_1_SubscribeCore_m3200035489_gshared (EmptyObservable_1_t3526296119 * __this, Il2CppObject* ___observer0, Il2CppObject * ___cancel1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (EmptyObservable_1_SubscribeCore_m3200035489_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject* L_0 = ___observer0;
		Il2CppObject * L_1 = ___cancel1;
		Empty_t2795924254 * L_2 = (Empty_t2795924254 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		((  void (*) (Empty_t2795924254 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)(L_2, (Il2CppObject*)L_0, (Il2CppObject *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		___observer0 = (Il2CppObject*)L_2;
		Il2CppObject * L_3 = (Il2CppObject *)__this->get_scheduler_1();
		IL2CPP_RUNTIME_CLASS_INIT(Scheduler_t103934925_il2cpp_TypeInfo_var);
		Il2CppObject * L_4 = ((Scheduler_t103934925_StaticFields*)Scheduler_t103934925_il2cpp_TypeInfo_var->static_fields)->get_Immediate_1();
		if ((!(((Il2CppObject*)(Il2CppObject *)L_3) == ((Il2CppObject*)(Il2CppObject *)L_4))))
		{
			goto IL_0025;
		}
	}
	{
		Il2CppObject* L_5 = ___observer0;
		NullCheck((Il2CppObject*)L_5);
		InterfaceActionInvoker0::Invoke(0 /* System.Void UniRx.IObserver`1<System.Object>::OnCompleted() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4), (Il2CppObject*)L_5);
		IL2CPP_RUNTIME_CLASS_INIT(Disposable_t3736388786_il2cpp_TypeInfo_var);
		Il2CppObject * L_6 = ((Disposable_t3736388786_StaticFields*)Disposable_t3736388786_il2cpp_TypeInfo_var->static_fields)->get_Empty_0();
		return L_6;
	}

IL_0025:
	{
		Il2CppObject * L_7 = (Il2CppObject *)__this->get_scheduler_1();
		Il2CppObject* L_8 = ___observer0;
		Il2CppObject* L_9 = (Il2CppObject*)L_8;
		IntPtr_t L_10;
		L_10.set_m_value_0((void*)(void*)GetInterfaceMethodInfo(L_9, 0, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)));
		Action_t437523947 * L_11 = (Action_t437523947 *)il2cpp_codegen_object_new(Action_t437523947_il2cpp_TypeInfo_var);
		Action__ctor_m2957240604(L_11, (Il2CppObject *)L_9, (IntPtr_t)L_10, /*hidden argument*/NULL);
		NullCheck((Il2CppObject *)L_7);
		Il2CppObject * L_12 = InterfaceFuncInvoker1< Il2CppObject *, Action_t437523947 * >::Invoke(1 /* System.IDisposable UniRx.IScheduler::Schedule(System.Action) */, IScheduler_t2938318244_il2cpp_TypeInfo_var, (Il2CppObject *)L_7, (Action_t437523947 *)L_11);
		return L_12;
	}
}
// System.Void UniRx.Operators.EmptyObservable`1<UniRx.CollectionAddEvent`1<System.Object>>::.ctor(UniRx.IScheduler)
extern "C"  void EmptyObservable_1__ctor_m3337481681_gshared (EmptyObservable_1_t810744390 * __this, Il2CppObject * ___scheduler0, const MethodInfo* method)
{
	{
		NullCheck((OperatorObservableBase_1_t1480666958 *)__this);
		((  void (*) (OperatorObservableBase_1_t1480666958 *, bool, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((OperatorObservableBase_1_t1480666958 *)__this, (bool)0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		Il2CppObject * L_0 = ___scheduler0;
		__this->set_scheduler_1(L_0);
		return;
	}
}
// System.IDisposable UniRx.Operators.EmptyObservable`1<UniRx.CollectionAddEvent`1<System.Object>>::SubscribeCore(UniRx.IObserver`1<T>,System.IDisposable)
extern Il2CppClass* Scheduler_t103934925_il2cpp_TypeInfo_var;
extern Il2CppClass* Disposable_t3736388786_il2cpp_TypeInfo_var;
extern Il2CppClass* Action_t437523947_il2cpp_TypeInfo_var;
extern Il2CppClass* IScheduler_t2938318244_il2cpp_TypeInfo_var;
extern const uint32_t EmptyObservable_1_SubscribeCore_m1180864131_MetadataUsageId;
extern "C"  Il2CppObject * EmptyObservable_1_SubscribeCore_m1180864131_gshared (EmptyObservable_1_t810744390 * __this, Il2CppObject* ___observer0, Il2CppObject * ___cancel1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (EmptyObservable_1_SubscribeCore_m1180864131_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject* L_0 = ___observer0;
		Il2CppObject * L_1 = ___cancel1;
		Empty_t80372525 * L_2 = (Empty_t80372525 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		((  void (*) (Empty_t80372525 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)(L_2, (Il2CppObject*)L_0, (Il2CppObject *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		___observer0 = (Il2CppObject*)L_2;
		Il2CppObject * L_3 = (Il2CppObject *)__this->get_scheduler_1();
		IL2CPP_RUNTIME_CLASS_INIT(Scheduler_t103934925_il2cpp_TypeInfo_var);
		Il2CppObject * L_4 = ((Scheduler_t103934925_StaticFields*)Scheduler_t103934925_il2cpp_TypeInfo_var->static_fields)->get_Immediate_1();
		if ((!(((Il2CppObject*)(Il2CppObject *)L_3) == ((Il2CppObject*)(Il2CppObject *)L_4))))
		{
			goto IL_0025;
		}
	}
	{
		Il2CppObject* L_5 = ___observer0;
		NullCheck((Il2CppObject*)L_5);
		InterfaceActionInvoker0::Invoke(0 /* System.Void UniRx.IObserver`1<UniRx.CollectionAddEvent`1<System.Object>>::OnCompleted() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4), (Il2CppObject*)L_5);
		IL2CPP_RUNTIME_CLASS_INIT(Disposable_t3736388786_il2cpp_TypeInfo_var);
		Il2CppObject * L_6 = ((Disposable_t3736388786_StaticFields*)Disposable_t3736388786_il2cpp_TypeInfo_var->static_fields)->get_Empty_0();
		return L_6;
	}

IL_0025:
	{
		Il2CppObject * L_7 = (Il2CppObject *)__this->get_scheduler_1();
		Il2CppObject* L_8 = ___observer0;
		Il2CppObject* L_9 = (Il2CppObject*)L_8;
		IntPtr_t L_10;
		L_10.set_m_value_0((void*)(void*)GetInterfaceMethodInfo(L_9, 0, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)));
		Action_t437523947 * L_11 = (Action_t437523947 *)il2cpp_codegen_object_new(Action_t437523947_il2cpp_TypeInfo_var);
		Action__ctor_m2957240604(L_11, (Il2CppObject *)L_9, (IntPtr_t)L_10, /*hidden argument*/NULL);
		NullCheck((Il2CppObject *)L_7);
		Il2CppObject * L_12 = InterfaceFuncInvoker1< Il2CppObject *, Action_t437523947 * >::Invoke(1 /* System.IDisposable UniRx.IScheduler::Schedule(System.Action) */, IScheduler_t2938318244_il2cpp_TypeInfo_var, (Il2CppObject *)L_7, (Action_t437523947 *)L_11);
		return L_12;
	}
}
// System.Void UniRx.Operators.EmptyObservable`1<UniRx.CollectionAddEvent`1<UniRx.Tuple`2<System.Object,System.Object>>>::.ctor(UniRx.IScheduler)
extern "C"  void EmptyObservable_1__ctor_m1128627099_gshared (EmptyObservable_1_t342899789 * __this, Il2CppObject * ___scheduler0, const MethodInfo* method)
{
	{
		NullCheck((OperatorObservableBase_1_t1012822357 *)__this);
		((  void (*) (OperatorObservableBase_1_t1012822357 *, bool, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((OperatorObservableBase_1_t1012822357 *)__this, (bool)0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		Il2CppObject * L_0 = ___scheduler0;
		__this->set_scheduler_1(L_0);
		return;
	}
}
// System.IDisposable UniRx.Operators.EmptyObservable`1<UniRx.CollectionAddEvent`1<UniRx.Tuple`2<System.Object,System.Object>>>::SubscribeCore(UniRx.IObserver`1<T>,System.IDisposable)
extern Il2CppClass* Scheduler_t103934925_il2cpp_TypeInfo_var;
extern Il2CppClass* Disposable_t3736388786_il2cpp_TypeInfo_var;
extern Il2CppClass* Action_t437523947_il2cpp_TypeInfo_var;
extern Il2CppClass* IScheduler_t2938318244_il2cpp_TypeInfo_var;
extern const uint32_t EmptyObservable_1_SubscribeCore_m3984312803_MetadataUsageId;
extern "C"  Il2CppObject * EmptyObservable_1_SubscribeCore_m3984312803_gshared (EmptyObservable_1_t342899789 * __this, Il2CppObject* ___observer0, Il2CppObject * ___cancel1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (EmptyObservable_1_SubscribeCore_m3984312803_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject* L_0 = ___observer0;
		Il2CppObject * L_1 = ___cancel1;
		Empty_t3907495220 * L_2 = (Empty_t3907495220 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		((  void (*) (Empty_t3907495220 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)(L_2, (Il2CppObject*)L_0, (Il2CppObject *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		___observer0 = (Il2CppObject*)L_2;
		Il2CppObject * L_3 = (Il2CppObject *)__this->get_scheduler_1();
		IL2CPP_RUNTIME_CLASS_INIT(Scheduler_t103934925_il2cpp_TypeInfo_var);
		Il2CppObject * L_4 = ((Scheduler_t103934925_StaticFields*)Scheduler_t103934925_il2cpp_TypeInfo_var->static_fields)->get_Immediate_1();
		if ((!(((Il2CppObject*)(Il2CppObject *)L_3) == ((Il2CppObject*)(Il2CppObject *)L_4))))
		{
			goto IL_0025;
		}
	}
	{
		Il2CppObject* L_5 = ___observer0;
		NullCheck((Il2CppObject*)L_5);
		InterfaceActionInvoker0::Invoke(0 /* System.Void UniRx.IObserver`1<UniRx.CollectionAddEvent`1<UniRx.Tuple`2<System.Object,System.Object>>>::OnCompleted() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4), (Il2CppObject*)L_5);
		IL2CPP_RUNTIME_CLASS_INIT(Disposable_t3736388786_il2cpp_TypeInfo_var);
		Il2CppObject * L_6 = ((Disposable_t3736388786_StaticFields*)Disposable_t3736388786_il2cpp_TypeInfo_var->static_fields)->get_Empty_0();
		return L_6;
	}

IL_0025:
	{
		Il2CppObject * L_7 = (Il2CppObject *)__this->get_scheduler_1();
		Il2CppObject* L_8 = ___observer0;
		Il2CppObject* L_9 = (Il2CppObject*)L_8;
		IntPtr_t L_10;
		L_10.set_m_value_0((void*)(void*)GetInterfaceMethodInfo(L_9, 0, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)));
		Action_t437523947 * L_11 = (Action_t437523947 *)il2cpp_codegen_object_new(Action_t437523947_il2cpp_TypeInfo_var);
		Action__ctor_m2957240604(L_11, (Il2CppObject *)L_9, (IntPtr_t)L_10, /*hidden argument*/NULL);
		NullCheck((Il2CppObject *)L_7);
		Il2CppObject * L_12 = InterfaceFuncInvoker1< Il2CppObject *, Action_t437523947 * >::Invoke(1 /* System.IDisposable UniRx.IScheduler::Schedule(System.Action) */, IScheduler_t2938318244_il2cpp_TypeInfo_var, (Il2CppObject *)L_7, (Action_t437523947 *)L_11);
		return L_12;
	}
}
// System.Void UniRx.Operators.EmptyObservable`1<UniRx.CollectionMoveEvent`1<System.Object>>::.ctor(UniRx.IScheduler)
extern "C"  void EmptyObservable_1__ctor_m3323180857_gshared (EmptyObservable_1_t1310656250 * __this, Il2CppObject * ___scheduler0, const MethodInfo* method)
{
	{
		NullCheck((OperatorObservableBase_1_t1980578818 *)__this);
		((  void (*) (OperatorObservableBase_1_t1980578818 *, bool, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((OperatorObservableBase_1_t1980578818 *)__this, (bool)0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		Il2CppObject * L_0 = ___scheduler0;
		__this->set_scheduler_1(L_0);
		return;
	}
}
// System.IDisposable UniRx.Operators.EmptyObservable`1<UniRx.CollectionMoveEvent`1<System.Object>>::SubscribeCore(UniRx.IObserver`1<T>,System.IDisposable)
extern Il2CppClass* Scheduler_t103934925_il2cpp_TypeInfo_var;
extern Il2CppClass* Disposable_t3736388786_il2cpp_TypeInfo_var;
extern Il2CppClass* Action_t437523947_il2cpp_TypeInfo_var;
extern Il2CppClass* IScheduler_t2938318244_il2cpp_TypeInfo_var;
extern const uint32_t EmptyObservable_1_SubscribeCore_m2884555781_MetadataUsageId;
extern "C"  Il2CppObject * EmptyObservable_1_SubscribeCore_m2884555781_gshared (EmptyObservable_1_t1310656250 * __this, Il2CppObject* ___observer0, Il2CppObject * ___cancel1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (EmptyObservable_1_SubscribeCore_m2884555781_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject* L_0 = ___observer0;
		Il2CppObject * L_1 = ___cancel1;
		Empty_t580284385 * L_2 = (Empty_t580284385 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		((  void (*) (Empty_t580284385 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)(L_2, (Il2CppObject*)L_0, (Il2CppObject *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		___observer0 = (Il2CppObject*)L_2;
		Il2CppObject * L_3 = (Il2CppObject *)__this->get_scheduler_1();
		IL2CPP_RUNTIME_CLASS_INIT(Scheduler_t103934925_il2cpp_TypeInfo_var);
		Il2CppObject * L_4 = ((Scheduler_t103934925_StaticFields*)Scheduler_t103934925_il2cpp_TypeInfo_var->static_fields)->get_Immediate_1();
		if ((!(((Il2CppObject*)(Il2CppObject *)L_3) == ((Il2CppObject*)(Il2CppObject *)L_4))))
		{
			goto IL_0025;
		}
	}
	{
		Il2CppObject* L_5 = ___observer0;
		NullCheck((Il2CppObject*)L_5);
		InterfaceActionInvoker0::Invoke(0 /* System.Void UniRx.IObserver`1<UniRx.CollectionMoveEvent`1<System.Object>>::OnCompleted() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4), (Il2CppObject*)L_5);
		IL2CPP_RUNTIME_CLASS_INIT(Disposable_t3736388786_il2cpp_TypeInfo_var);
		Il2CppObject * L_6 = ((Disposable_t3736388786_StaticFields*)Disposable_t3736388786_il2cpp_TypeInfo_var->static_fields)->get_Empty_0();
		return L_6;
	}

IL_0025:
	{
		Il2CppObject * L_7 = (Il2CppObject *)__this->get_scheduler_1();
		Il2CppObject* L_8 = ___observer0;
		Il2CppObject* L_9 = (Il2CppObject*)L_8;
		IntPtr_t L_10;
		L_10.set_m_value_0((void*)(void*)GetInterfaceMethodInfo(L_9, 0, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)));
		Action_t437523947 * L_11 = (Action_t437523947 *)il2cpp_codegen_object_new(Action_t437523947_il2cpp_TypeInfo_var);
		Action__ctor_m2957240604(L_11, (Il2CppObject *)L_9, (IntPtr_t)L_10, /*hidden argument*/NULL);
		NullCheck((Il2CppObject *)L_7);
		Il2CppObject * L_12 = InterfaceFuncInvoker1< Il2CppObject *, Action_t437523947 * >::Invoke(1 /* System.IDisposable UniRx.IScheduler::Schedule(System.Action) */, IScheduler_t2938318244_il2cpp_TypeInfo_var, (Il2CppObject *)L_7, (Action_t437523947 *)L_11);
		return L_12;
	}
}
// System.Void UniRx.Operators.EmptyObservable`1<UniRx.CollectionMoveEvent`1<UniRx.Tuple`2<System.Object,System.Object>>>::.ctor(UniRx.IScheduler)
extern "C"  void EmptyObservable_1__ctor_m1260784435_gshared (EmptyObservable_1_t842811649 * __this, Il2CppObject * ___scheduler0, const MethodInfo* method)
{
	{
		NullCheck((OperatorObservableBase_1_t1512734217 *)__this);
		((  void (*) (OperatorObservableBase_1_t1512734217 *, bool, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((OperatorObservableBase_1_t1512734217 *)__this, (bool)0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		Il2CppObject * L_0 = ___scheduler0;
		__this->set_scheduler_1(L_0);
		return;
	}
}
// System.IDisposable UniRx.Operators.EmptyObservable`1<UniRx.CollectionMoveEvent`1<UniRx.Tuple`2<System.Object,System.Object>>>::SubscribeCore(UniRx.IObserver`1<T>,System.IDisposable)
extern Il2CppClass* Scheduler_t103934925_il2cpp_TypeInfo_var;
extern Il2CppClass* Disposable_t3736388786_il2cpp_TypeInfo_var;
extern Il2CppClass* Action_t437523947_il2cpp_TypeInfo_var;
extern Il2CppClass* IScheduler_t2938318244_il2cpp_TypeInfo_var;
extern const uint32_t EmptyObservable_1_SubscribeCore_m1700842401_MetadataUsageId;
extern "C"  Il2CppObject * EmptyObservable_1_SubscribeCore_m1700842401_gshared (EmptyObservable_1_t842811649 * __this, Il2CppObject* ___observer0, Il2CppObject * ___cancel1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (EmptyObservable_1_SubscribeCore_m1700842401_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject* L_0 = ___observer0;
		Il2CppObject * L_1 = ___cancel1;
		Empty_t112439784 * L_2 = (Empty_t112439784 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		((  void (*) (Empty_t112439784 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)(L_2, (Il2CppObject*)L_0, (Il2CppObject *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		___observer0 = (Il2CppObject*)L_2;
		Il2CppObject * L_3 = (Il2CppObject *)__this->get_scheduler_1();
		IL2CPP_RUNTIME_CLASS_INIT(Scheduler_t103934925_il2cpp_TypeInfo_var);
		Il2CppObject * L_4 = ((Scheduler_t103934925_StaticFields*)Scheduler_t103934925_il2cpp_TypeInfo_var->static_fields)->get_Immediate_1();
		if ((!(((Il2CppObject*)(Il2CppObject *)L_3) == ((Il2CppObject*)(Il2CppObject *)L_4))))
		{
			goto IL_0025;
		}
	}
	{
		Il2CppObject* L_5 = ___observer0;
		NullCheck((Il2CppObject*)L_5);
		InterfaceActionInvoker0::Invoke(0 /* System.Void UniRx.IObserver`1<UniRx.CollectionMoveEvent`1<UniRx.Tuple`2<System.Object,System.Object>>>::OnCompleted() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4), (Il2CppObject*)L_5);
		IL2CPP_RUNTIME_CLASS_INIT(Disposable_t3736388786_il2cpp_TypeInfo_var);
		Il2CppObject * L_6 = ((Disposable_t3736388786_StaticFields*)Disposable_t3736388786_il2cpp_TypeInfo_var->static_fields)->get_Empty_0();
		return L_6;
	}

IL_0025:
	{
		Il2CppObject * L_7 = (Il2CppObject *)__this->get_scheduler_1();
		Il2CppObject* L_8 = ___observer0;
		Il2CppObject* L_9 = (Il2CppObject*)L_8;
		IntPtr_t L_10;
		L_10.set_m_value_0((void*)(void*)GetInterfaceMethodInfo(L_9, 0, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)));
		Action_t437523947 * L_11 = (Action_t437523947 *)il2cpp_codegen_object_new(Action_t437523947_il2cpp_TypeInfo_var);
		Action__ctor_m2957240604(L_11, (Il2CppObject *)L_9, (IntPtr_t)L_10, /*hidden argument*/NULL);
		NullCheck((Il2CppObject *)L_7);
		Il2CppObject * L_12 = InterfaceFuncInvoker1< Il2CppObject *, Action_t437523947 * >::Invoke(1 /* System.IDisposable UniRx.IScheduler::Schedule(System.Action) */, IScheduler_t2938318244_il2cpp_TypeInfo_var, (Il2CppObject *)L_7, (Action_t437523947 *)L_11);
		return L_12;
	}
}
// System.Void UniRx.Operators.EmptyObservable`1<UniRx.CollectionRemoveEvent`1<System.Object>>::.ctor(UniRx.IScheduler)
extern "C"  void EmptyObservable_1__ctor_m669924076_gshared (EmptyObservable_1_t2931827423 * __this, Il2CppObject * ___scheduler0, const MethodInfo* method)
{
	{
		NullCheck((OperatorObservableBase_1_t3601749991 *)__this);
		((  void (*) (OperatorObservableBase_1_t3601749991 *, bool, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((OperatorObservableBase_1_t3601749991 *)__this, (bool)0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		Il2CppObject * L_0 = ___scheduler0;
		__this->set_scheduler_1(L_0);
		return;
	}
}
// System.IDisposable UniRx.Operators.EmptyObservable`1<UniRx.CollectionRemoveEvent`1<System.Object>>::SubscribeCore(UniRx.IObserver`1<T>,System.IDisposable)
extern Il2CppClass* Scheduler_t103934925_il2cpp_TypeInfo_var;
extern Il2CppClass* Disposable_t3736388786_il2cpp_TypeInfo_var;
extern Il2CppClass* Action_t437523947_il2cpp_TypeInfo_var;
extern Il2CppClass* IScheduler_t2938318244_il2cpp_TypeInfo_var;
extern const uint32_t EmptyObservable_1_SubscribeCore_m1275643058_MetadataUsageId;
extern "C"  Il2CppObject * EmptyObservable_1_SubscribeCore_m1275643058_gshared (EmptyObservable_1_t2931827423 * __this, Il2CppObject* ___observer0, Il2CppObject * ___cancel1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (EmptyObservable_1_SubscribeCore_m1275643058_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject* L_0 = ___observer0;
		Il2CppObject * L_1 = ___cancel1;
		Empty_t2201455558 * L_2 = (Empty_t2201455558 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		((  void (*) (Empty_t2201455558 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)(L_2, (Il2CppObject*)L_0, (Il2CppObject *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		___observer0 = (Il2CppObject*)L_2;
		Il2CppObject * L_3 = (Il2CppObject *)__this->get_scheduler_1();
		IL2CPP_RUNTIME_CLASS_INIT(Scheduler_t103934925_il2cpp_TypeInfo_var);
		Il2CppObject * L_4 = ((Scheduler_t103934925_StaticFields*)Scheduler_t103934925_il2cpp_TypeInfo_var->static_fields)->get_Immediate_1();
		if ((!(((Il2CppObject*)(Il2CppObject *)L_3) == ((Il2CppObject*)(Il2CppObject *)L_4))))
		{
			goto IL_0025;
		}
	}
	{
		Il2CppObject* L_5 = ___observer0;
		NullCheck((Il2CppObject*)L_5);
		InterfaceActionInvoker0::Invoke(0 /* System.Void UniRx.IObserver`1<UniRx.CollectionRemoveEvent`1<System.Object>>::OnCompleted() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4), (Il2CppObject*)L_5);
		IL2CPP_RUNTIME_CLASS_INIT(Disposable_t3736388786_il2cpp_TypeInfo_var);
		Il2CppObject * L_6 = ((Disposable_t3736388786_StaticFields*)Disposable_t3736388786_il2cpp_TypeInfo_var->static_fields)->get_Empty_0();
		return L_6;
	}

IL_0025:
	{
		Il2CppObject * L_7 = (Il2CppObject *)__this->get_scheduler_1();
		Il2CppObject* L_8 = ___observer0;
		Il2CppObject* L_9 = (Il2CppObject*)L_8;
		IntPtr_t L_10;
		L_10.set_m_value_0((void*)(void*)GetInterfaceMethodInfo(L_9, 0, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)));
		Action_t437523947 * L_11 = (Action_t437523947 *)il2cpp_codegen_object_new(Action_t437523947_il2cpp_TypeInfo_var);
		Action__ctor_m2957240604(L_11, (Il2CppObject *)L_9, (IntPtr_t)L_10, /*hidden argument*/NULL);
		NullCheck((Il2CppObject *)L_7);
		Il2CppObject * L_12 = InterfaceFuncInvoker1< Il2CppObject *, Action_t437523947 * >::Invoke(1 /* System.IDisposable UniRx.IScheduler::Schedule(System.Action) */, IScheduler_t2938318244_il2cpp_TypeInfo_var, (Il2CppObject *)L_7, (Action_t437523947 *)L_11);
		return L_12;
	}
}
// System.Void UniRx.Operators.EmptyObservable`1<UniRx.CollectionRemoveEvent`1<UniRx.Tuple`2<System.Object,System.Object>>>::.ctor(UniRx.IScheduler)
extern "C"  void EmptyObservable_1__ctor_m2216323168_gshared (EmptyObservable_1_t2463982822 * __this, Il2CppObject * ___scheduler0, const MethodInfo* method)
{
	{
		NullCheck((OperatorObservableBase_1_t3133905390 *)__this);
		((  void (*) (OperatorObservableBase_1_t3133905390 *, bool, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((OperatorObservableBase_1_t3133905390 *)__this, (bool)0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		Il2CppObject * L_0 = ___scheduler0;
		__this->set_scheduler_1(L_0);
		return;
	}
}
// System.IDisposable UniRx.Operators.EmptyObservable`1<UniRx.CollectionRemoveEvent`1<UniRx.Tuple`2<System.Object,System.Object>>>::SubscribeCore(UniRx.IObserver`1<T>,System.IDisposable)
extern Il2CppClass* Scheduler_t103934925_il2cpp_TypeInfo_var;
extern Il2CppClass* Disposable_t3736388786_il2cpp_TypeInfo_var;
extern Il2CppClass* Action_t437523947_il2cpp_TypeInfo_var;
extern Il2CppClass* IScheduler_t2938318244_il2cpp_TypeInfo_var;
extern const uint32_t EmptyObservable_1_SubscribeCore_m3715946004_MetadataUsageId;
extern "C"  Il2CppObject * EmptyObservable_1_SubscribeCore_m3715946004_gshared (EmptyObservable_1_t2463982822 * __this, Il2CppObject* ___observer0, Il2CppObject * ___cancel1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (EmptyObservable_1_SubscribeCore_m3715946004_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject* L_0 = ___observer0;
		Il2CppObject * L_1 = ___cancel1;
		Empty_t1733610957 * L_2 = (Empty_t1733610957 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		((  void (*) (Empty_t1733610957 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)(L_2, (Il2CppObject*)L_0, (Il2CppObject *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		___observer0 = (Il2CppObject*)L_2;
		Il2CppObject * L_3 = (Il2CppObject *)__this->get_scheduler_1();
		IL2CPP_RUNTIME_CLASS_INIT(Scheduler_t103934925_il2cpp_TypeInfo_var);
		Il2CppObject * L_4 = ((Scheduler_t103934925_StaticFields*)Scheduler_t103934925_il2cpp_TypeInfo_var->static_fields)->get_Immediate_1();
		if ((!(((Il2CppObject*)(Il2CppObject *)L_3) == ((Il2CppObject*)(Il2CppObject *)L_4))))
		{
			goto IL_0025;
		}
	}
	{
		Il2CppObject* L_5 = ___observer0;
		NullCheck((Il2CppObject*)L_5);
		InterfaceActionInvoker0::Invoke(0 /* System.Void UniRx.IObserver`1<UniRx.CollectionRemoveEvent`1<UniRx.Tuple`2<System.Object,System.Object>>>::OnCompleted() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4), (Il2CppObject*)L_5);
		IL2CPP_RUNTIME_CLASS_INIT(Disposable_t3736388786_il2cpp_TypeInfo_var);
		Il2CppObject * L_6 = ((Disposable_t3736388786_StaticFields*)Disposable_t3736388786_il2cpp_TypeInfo_var->static_fields)->get_Empty_0();
		return L_6;
	}

IL_0025:
	{
		Il2CppObject * L_7 = (Il2CppObject *)__this->get_scheduler_1();
		Il2CppObject* L_8 = ___observer0;
		Il2CppObject* L_9 = (Il2CppObject*)L_8;
		IntPtr_t L_10;
		L_10.set_m_value_0((void*)(void*)GetInterfaceMethodInfo(L_9, 0, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)));
		Action_t437523947 * L_11 = (Action_t437523947 *)il2cpp_codegen_object_new(Action_t437523947_il2cpp_TypeInfo_var);
		Action__ctor_m2957240604(L_11, (Il2CppObject *)L_9, (IntPtr_t)L_10, /*hidden argument*/NULL);
		NullCheck((Il2CppObject *)L_7);
		Il2CppObject * L_12 = InterfaceFuncInvoker1< Il2CppObject *, Action_t437523947 * >::Invoke(1 /* System.IDisposable UniRx.IScheduler::Schedule(System.Action) */, IScheduler_t2938318244_il2cpp_TypeInfo_var, (Il2CppObject *)L_7, (Action_t437523947 *)L_11);
		return L_12;
	}
}
// System.Void UniRx.Operators.EmptyObservable`1<UniRx.CollectionReplaceEvent`1<System.Object>>::.ctor(UniRx.IScheduler)
extern "C"  void EmptyObservable_1__ctor_m712467428_gshared (EmptyObservable_1_t235972939 * __this, Il2CppObject * ___scheduler0, const MethodInfo* method)
{
	{
		NullCheck((OperatorObservableBase_1_t905895507 *)__this);
		((  void (*) (OperatorObservableBase_1_t905895507 *, bool, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((OperatorObservableBase_1_t905895507 *)__this, (bool)0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		Il2CppObject * L_0 = ___scheduler0;
		__this->set_scheduler_1(L_0);
		return;
	}
}
// System.IDisposable UniRx.Operators.EmptyObservable`1<UniRx.CollectionReplaceEvent`1<System.Object>>::SubscribeCore(UniRx.IObserver`1<T>,System.IDisposable)
extern Il2CppClass* Scheduler_t103934925_il2cpp_TypeInfo_var;
extern Il2CppClass* Disposable_t3736388786_il2cpp_TypeInfo_var;
extern Il2CppClass* Action_t437523947_il2cpp_TypeInfo_var;
extern Il2CppClass* IScheduler_t2938318244_il2cpp_TypeInfo_var;
extern const uint32_t EmptyObservable_1_SubscribeCore_m52674704_MetadataUsageId;
extern "C"  Il2CppObject * EmptyObservable_1_SubscribeCore_m52674704_gshared (EmptyObservable_1_t235972939 * __this, Il2CppObject* ___observer0, Il2CppObject * ___cancel1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (EmptyObservable_1_SubscribeCore_m52674704_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject* L_0 = ___observer0;
		Il2CppObject * L_1 = ___cancel1;
		Empty_t3800568370 * L_2 = (Empty_t3800568370 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		((  void (*) (Empty_t3800568370 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)(L_2, (Il2CppObject*)L_0, (Il2CppObject *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		___observer0 = (Il2CppObject*)L_2;
		Il2CppObject * L_3 = (Il2CppObject *)__this->get_scheduler_1();
		IL2CPP_RUNTIME_CLASS_INIT(Scheduler_t103934925_il2cpp_TypeInfo_var);
		Il2CppObject * L_4 = ((Scheduler_t103934925_StaticFields*)Scheduler_t103934925_il2cpp_TypeInfo_var->static_fields)->get_Immediate_1();
		if ((!(((Il2CppObject*)(Il2CppObject *)L_3) == ((Il2CppObject*)(Il2CppObject *)L_4))))
		{
			goto IL_0025;
		}
	}
	{
		Il2CppObject* L_5 = ___observer0;
		NullCheck((Il2CppObject*)L_5);
		InterfaceActionInvoker0::Invoke(0 /* System.Void UniRx.IObserver`1<UniRx.CollectionReplaceEvent`1<System.Object>>::OnCompleted() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4), (Il2CppObject*)L_5);
		IL2CPP_RUNTIME_CLASS_INIT(Disposable_t3736388786_il2cpp_TypeInfo_var);
		Il2CppObject * L_6 = ((Disposable_t3736388786_StaticFields*)Disposable_t3736388786_il2cpp_TypeInfo_var->static_fields)->get_Empty_0();
		return L_6;
	}

IL_0025:
	{
		Il2CppObject * L_7 = (Il2CppObject *)__this->get_scheduler_1();
		Il2CppObject* L_8 = ___observer0;
		Il2CppObject* L_9 = (Il2CppObject*)L_8;
		IntPtr_t L_10;
		L_10.set_m_value_0((void*)(void*)GetInterfaceMethodInfo(L_9, 0, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)));
		Action_t437523947 * L_11 = (Action_t437523947 *)il2cpp_codegen_object_new(Action_t437523947_il2cpp_TypeInfo_var);
		Action__ctor_m2957240604(L_11, (Il2CppObject *)L_9, (IntPtr_t)L_10, /*hidden argument*/NULL);
		NullCheck((Il2CppObject *)L_7);
		Il2CppObject * L_12 = InterfaceFuncInvoker1< Il2CppObject *, Action_t437523947 * >::Invoke(1 /* System.IDisposable UniRx.IScheduler::Schedule(System.Action) */, IScheduler_t2938318244_il2cpp_TypeInfo_var, (Il2CppObject *)L_7, (Action_t437523947 *)L_11);
		return L_12;
	}
}
// System.Void UniRx.Operators.EmptyObservable`1<UniRx.CollectionReplaceEvent`1<UniRx.Tuple`2<System.Object,System.Object>>>::.ctor(UniRx.IScheduler)
extern "C"  void EmptyObservable_1__ctor_m1011384936_gshared (EmptyObservable_1_t4063095634 * __this, Il2CppObject * ___scheduler0, const MethodInfo* method)
{
	{
		NullCheck((OperatorObservableBase_1_t438050906 *)__this);
		((  void (*) (OperatorObservableBase_1_t438050906 *, bool, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((OperatorObservableBase_1_t438050906 *)__this, (bool)0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		Il2CppObject * L_0 = ___scheduler0;
		__this->set_scheduler_1(L_0);
		return;
	}
}
// System.IDisposable UniRx.Operators.EmptyObservable`1<UniRx.CollectionReplaceEvent`1<UniRx.Tuple`2<System.Object,System.Object>>>::SubscribeCore(UniRx.IObserver`1<T>,System.IDisposable)
extern Il2CppClass* Scheduler_t103934925_il2cpp_TypeInfo_var;
extern Il2CppClass* Disposable_t3736388786_il2cpp_TypeInfo_var;
extern Il2CppClass* Action_t437523947_il2cpp_TypeInfo_var;
extern Il2CppClass* IScheduler_t2938318244_il2cpp_TypeInfo_var;
extern const uint32_t EmptyObservable_1_SubscribeCore_m3601108726_MetadataUsageId;
extern "C"  Il2CppObject * EmptyObservable_1_SubscribeCore_m3601108726_gshared (EmptyObservable_1_t4063095634 * __this, Il2CppObject* ___observer0, Il2CppObject * ___cancel1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (EmptyObservable_1_SubscribeCore_m3601108726_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject* L_0 = ___observer0;
		Il2CppObject * L_1 = ___cancel1;
		Empty_t3332723769 * L_2 = (Empty_t3332723769 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		((  void (*) (Empty_t3332723769 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)(L_2, (Il2CppObject*)L_0, (Il2CppObject *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		___observer0 = (Il2CppObject*)L_2;
		Il2CppObject * L_3 = (Il2CppObject *)__this->get_scheduler_1();
		IL2CPP_RUNTIME_CLASS_INIT(Scheduler_t103934925_il2cpp_TypeInfo_var);
		Il2CppObject * L_4 = ((Scheduler_t103934925_StaticFields*)Scheduler_t103934925_il2cpp_TypeInfo_var->static_fields)->get_Immediate_1();
		if ((!(((Il2CppObject*)(Il2CppObject *)L_3) == ((Il2CppObject*)(Il2CppObject *)L_4))))
		{
			goto IL_0025;
		}
	}
	{
		Il2CppObject* L_5 = ___observer0;
		NullCheck((Il2CppObject*)L_5);
		InterfaceActionInvoker0::Invoke(0 /* System.Void UniRx.IObserver`1<UniRx.CollectionReplaceEvent`1<UniRx.Tuple`2<System.Object,System.Object>>>::OnCompleted() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4), (Il2CppObject*)L_5);
		IL2CPP_RUNTIME_CLASS_INIT(Disposable_t3736388786_il2cpp_TypeInfo_var);
		Il2CppObject * L_6 = ((Disposable_t3736388786_StaticFields*)Disposable_t3736388786_il2cpp_TypeInfo_var->static_fields)->get_Empty_0();
		return L_6;
	}

IL_0025:
	{
		Il2CppObject * L_7 = (Il2CppObject *)__this->get_scheduler_1();
		Il2CppObject* L_8 = ___observer0;
		Il2CppObject* L_9 = (Il2CppObject*)L_8;
		IntPtr_t L_10;
		L_10.set_m_value_0((void*)(void*)GetInterfaceMethodInfo(L_9, 0, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)));
		Action_t437523947 * L_11 = (Action_t437523947 *)il2cpp_codegen_object_new(Action_t437523947_il2cpp_TypeInfo_var);
		Action__ctor_m2957240604(L_11, (Il2CppObject *)L_9, (IntPtr_t)L_10, /*hidden argument*/NULL);
		NullCheck((Il2CppObject *)L_7);
		Il2CppObject * L_12 = InterfaceFuncInvoker1< Il2CppObject *, Action_t437523947 * >::Invoke(1 /* System.IDisposable UniRx.IScheduler::Schedule(System.Action) */, IScheduler_t2938318244_il2cpp_TypeInfo_var, (Il2CppObject *)L_7, (Action_t437523947 *)L_11);
		return L_12;
	}
}
// System.Void UniRx.Operators.EmptyObservable`1<UniRx.DictionaryAddEvent`2<System.Object,System.Object>>::.ctor(UniRx.IScheduler)
extern "C"  void EmptyObservable_1__ctor_m1501734214_gshared (EmptyObservable_1_t2940170707 * __this, Il2CppObject * ___scheduler0, const MethodInfo* method)
{
	{
		NullCheck((OperatorObservableBase_1_t3610093275 *)__this);
		((  void (*) (OperatorObservableBase_1_t3610093275 *, bool, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((OperatorObservableBase_1_t3610093275 *)__this, (bool)0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		Il2CppObject * L_0 = ___scheduler0;
		__this->set_scheduler_1(L_0);
		return;
	}
}
// System.IDisposable UniRx.Operators.EmptyObservable`1<UniRx.DictionaryAddEvent`2<System.Object,System.Object>>::SubscribeCore(UniRx.IObserver`1<T>,System.IDisposable)
extern Il2CppClass* Scheduler_t103934925_il2cpp_TypeInfo_var;
extern Il2CppClass* Disposable_t3736388786_il2cpp_TypeInfo_var;
extern Il2CppClass* Action_t437523947_il2cpp_TypeInfo_var;
extern Il2CppClass* IScheduler_t2938318244_il2cpp_TypeInfo_var;
extern const uint32_t EmptyObservable_1_SubscribeCore_m2438726958_MetadataUsageId;
extern "C"  Il2CppObject * EmptyObservable_1_SubscribeCore_m2438726958_gshared (EmptyObservable_1_t2940170707 * __this, Il2CppObject* ___observer0, Il2CppObject * ___cancel1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (EmptyObservable_1_SubscribeCore_m2438726958_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject* L_0 = ___observer0;
		Il2CppObject * L_1 = ___cancel1;
		Empty_t2209798842 * L_2 = (Empty_t2209798842 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		((  void (*) (Empty_t2209798842 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)(L_2, (Il2CppObject*)L_0, (Il2CppObject *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		___observer0 = (Il2CppObject*)L_2;
		Il2CppObject * L_3 = (Il2CppObject *)__this->get_scheduler_1();
		IL2CPP_RUNTIME_CLASS_INIT(Scheduler_t103934925_il2cpp_TypeInfo_var);
		Il2CppObject * L_4 = ((Scheduler_t103934925_StaticFields*)Scheduler_t103934925_il2cpp_TypeInfo_var->static_fields)->get_Immediate_1();
		if ((!(((Il2CppObject*)(Il2CppObject *)L_3) == ((Il2CppObject*)(Il2CppObject *)L_4))))
		{
			goto IL_0025;
		}
	}
	{
		Il2CppObject* L_5 = ___observer0;
		NullCheck((Il2CppObject*)L_5);
		InterfaceActionInvoker0::Invoke(0 /* System.Void UniRx.IObserver`1<UniRx.DictionaryAddEvent`2<System.Object,System.Object>>::OnCompleted() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4), (Il2CppObject*)L_5);
		IL2CPP_RUNTIME_CLASS_INIT(Disposable_t3736388786_il2cpp_TypeInfo_var);
		Il2CppObject * L_6 = ((Disposable_t3736388786_StaticFields*)Disposable_t3736388786_il2cpp_TypeInfo_var->static_fields)->get_Empty_0();
		return L_6;
	}

IL_0025:
	{
		Il2CppObject * L_7 = (Il2CppObject *)__this->get_scheduler_1();
		Il2CppObject* L_8 = ___observer0;
		Il2CppObject* L_9 = (Il2CppObject*)L_8;
		IntPtr_t L_10;
		L_10.set_m_value_0((void*)(void*)GetInterfaceMethodInfo(L_9, 0, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)));
		Action_t437523947 * L_11 = (Action_t437523947 *)il2cpp_codegen_object_new(Action_t437523947_il2cpp_TypeInfo_var);
		Action__ctor_m2957240604(L_11, (Il2CppObject *)L_9, (IntPtr_t)L_10, /*hidden argument*/NULL);
		NullCheck((Il2CppObject *)L_7);
		Il2CppObject * L_12 = InterfaceFuncInvoker1< Il2CppObject *, Action_t437523947 * >::Invoke(1 /* System.IDisposable UniRx.IScheduler::Schedule(System.Action) */, IScheduler_t2938318244_il2cpp_TypeInfo_var, (Il2CppObject *)L_7, (Action_t437523947 *)L_11);
		return L_12;
	}
}
// System.Void UniRx.Operators.EmptyObservable`1<UniRx.DictionaryRemoveEvent`2<System.Object,System.Object>>::.ctor(UniRx.IScheduler)
extern "C"  void EmptyObservable_1__ctor_m4221726737_gshared (EmptyObservable_1_t1716510694 * __this, Il2CppObject * ___scheduler0, const MethodInfo* method)
{
	{
		NullCheck((OperatorObservableBase_1_t2386433262 *)__this);
		((  void (*) (OperatorObservableBase_1_t2386433262 *, bool, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((OperatorObservableBase_1_t2386433262 *)__this, (bool)0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		Il2CppObject * L_0 = ___scheduler0;
		__this->set_scheduler_1(L_0);
		return;
	}
}
// System.IDisposable UniRx.Operators.EmptyObservable`1<UniRx.DictionaryRemoveEvent`2<System.Object,System.Object>>::SubscribeCore(UniRx.IObserver`1<T>,System.IDisposable)
extern Il2CppClass* Scheduler_t103934925_il2cpp_TypeInfo_var;
extern Il2CppClass* Disposable_t3736388786_il2cpp_TypeInfo_var;
extern Il2CppClass* Action_t437523947_il2cpp_TypeInfo_var;
extern Il2CppClass* IScheduler_t2938318244_il2cpp_TypeInfo_var;
extern const uint32_t EmptyObservable_1_SubscribeCore_m2863207981_MetadataUsageId;
extern "C"  Il2CppObject * EmptyObservable_1_SubscribeCore_m2863207981_gshared (EmptyObservable_1_t1716510694 * __this, Il2CppObject* ___observer0, Il2CppObject * ___cancel1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (EmptyObservable_1_SubscribeCore_m2863207981_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject* L_0 = ___observer0;
		Il2CppObject * L_1 = ___cancel1;
		Empty_t986138829 * L_2 = (Empty_t986138829 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		((  void (*) (Empty_t986138829 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)(L_2, (Il2CppObject*)L_0, (Il2CppObject *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		___observer0 = (Il2CppObject*)L_2;
		Il2CppObject * L_3 = (Il2CppObject *)__this->get_scheduler_1();
		IL2CPP_RUNTIME_CLASS_INIT(Scheduler_t103934925_il2cpp_TypeInfo_var);
		Il2CppObject * L_4 = ((Scheduler_t103934925_StaticFields*)Scheduler_t103934925_il2cpp_TypeInfo_var->static_fields)->get_Immediate_1();
		if ((!(((Il2CppObject*)(Il2CppObject *)L_3) == ((Il2CppObject*)(Il2CppObject *)L_4))))
		{
			goto IL_0025;
		}
	}
	{
		Il2CppObject* L_5 = ___observer0;
		NullCheck((Il2CppObject*)L_5);
		InterfaceActionInvoker0::Invoke(0 /* System.Void UniRx.IObserver`1<UniRx.DictionaryRemoveEvent`2<System.Object,System.Object>>::OnCompleted() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4), (Il2CppObject*)L_5);
		IL2CPP_RUNTIME_CLASS_INIT(Disposable_t3736388786_il2cpp_TypeInfo_var);
		Il2CppObject * L_6 = ((Disposable_t3736388786_StaticFields*)Disposable_t3736388786_il2cpp_TypeInfo_var->static_fields)->get_Empty_0();
		return L_6;
	}

IL_0025:
	{
		Il2CppObject * L_7 = (Il2CppObject *)__this->get_scheduler_1();
		Il2CppObject* L_8 = ___observer0;
		Il2CppObject* L_9 = (Il2CppObject*)L_8;
		IntPtr_t L_10;
		L_10.set_m_value_0((void*)(void*)GetInterfaceMethodInfo(L_9, 0, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)));
		Action_t437523947 * L_11 = (Action_t437523947 *)il2cpp_codegen_object_new(Action_t437523947_il2cpp_TypeInfo_var);
		Action__ctor_m2957240604(L_11, (Il2CppObject *)L_9, (IntPtr_t)L_10, /*hidden argument*/NULL);
		NullCheck((Il2CppObject *)L_7);
		Il2CppObject * L_12 = InterfaceFuncInvoker1< Il2CppObject *, Action_t437523947 * >::Invoke(1 /* System.IDisposable UniRx.IScheduler::Schedule(System.Action) */, IScheduler_t2938318244_il2cpp_TypeInfo_var, (Il2CppObject *)L_7, (Action_t437523947 *)L_11);
		return L_12;
	}
}
// System.Void UniRx.Operators.EmptyObservable`1<UniRx.DictionaryReplaceEvent`2<System.Object,System.Object>>::.ctor(UniRx.IScheduler)
extern "C"  void EmptyObservable_1__ctor_m2509814297_gshared (EmptyObservable_1_t2173149466 * __this, Il2CppObject * ___scheduler0, const MethodInfo* method)
{
	{
		NullCheck((OperatorObservableBase_1_t2843072034 *)__this);
		((  void (*) (OperatorObservableBase_1_t2843072034 *, bool, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((OperatorObservableBase_1_t2843072034 *)__this, (bool)0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		Il2CppObject * L_0 = ___scheduler0;
		__this->set_scheduler_1(L_0);
		return;
	}
}
// System.IDisposable UniRx.Operators.EmptyObservable`1<UniRx.DictionaryReplaceEvent`2<System.Object,System.Object>>::SubscribeCore(UniRx.IObserver`1<T>,System.IDisposable)
extern Il2CppClass* Scheduler_t103934925_il2cpp_TypeInfo_var;
extern Il2CppClass* Disposable_t3736388786_il2cpp_TypeInfo_var;
extern Il2CppClass* Action_t437523947_il2cpp_TypeInfo_var;
extern Il2CppClass* IScheduler_t2938318244_il2cpp_TypeInfo_var;
extern const uint32_t EmptyObservable_1_SubscribeCore_m1928355963_MetadataUsageId;
extern "C"  Il2CppObject * EmptyObservable_1_SubscribeCore_m1928355963_gshared (EmptyObservable_1_t2173149466 * __this, Il2CppObject* ___observer0, Il2CppObject * ___cancel1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (EmptyObservable_1_SubscribeCore_m1928355963_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject* L_0 = ___observer0;
		Il2CppObject * L_1 = ___cancel1;
		Empty_t1442777601 * L_2 = (Empty_t1442777601 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		((  void (*) (Empty_t1442777601 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)(L_2, (Il2CppObject*)L_0, (Il2CppObject *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		___observer0 = (Il2CppObject*)L_2;
		Il2CppObject * L_3 = (Il2CppObject *)__this->get_scheduler_1();
		IL2CPP_RUNTIME_CLASS_INIT(Scheduler_t103934925_il2cpp_TypeInfo_var);
		Il2CppObject * L_4 = ((Scheduler_t103934925_StaticFields*)Scheduler_t103934925_il2cpp_TypeInfo_var->static_fields)->get_Immediate_1();
		if ((!(((Il2CppObject*)(Il2CppObject *)L_3) == ((Il2CppObject*)(Il2CppObject *)L_4))))
		{
			goto IL_0025;
		}
	}
	{
		Il2CppObject* L_5 = ___observer0;
		NullCheck((Il2CppObject*)L_5);
		InterfaceActionInvoker0::Invoke(0 /* System.Void UniRx.IObserver`1<UniRx.DictionaryReplaceEvent`2<System.Object,System.Object>>::OnCompleted() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4), (Il2CppObject*)L_5);
		IL2CPP_RUNTIME_CLASS_INIT(Disposable_t3736388786_il2cpp_TypeInfo_var);
		Il2CppObject * L_6 = ((Disposable_t3736388786_StaticFields*)Disposable_t3736388786_il2cpp_TypeInfo_var->static_fields)->get_Empty_0();
		return L_6;
	}

IL_0025:
	{
		Il2CppObject * L_7 = (Il2CppObject *)__this->get_scheduler_1();
		Il2CppObject* L_8 = ___observer0;
		Il2CppObject* L_9 = (Il2CppObject*)L_8;
		IntPtr_t L_10;
		L_10.set_m_value_0((void*)(void*)GetInterfaceMethodInfo(L_9, 0, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)));
		Action_t437523947 * L_11 = (Action_t437523947 *)il2cpp_codegen_object_new(Action_t437523947_il2cpp_TypeInfo_var);
		Action__ctor_m2957240604(L_11, (Il2CppObject *)L_9, (IntPtr_t)L_10, /*hidden argument*/NULL);
		NullCheck((Il2CppObject *)L_7);
		Il2CppObject * L_12 = InterfaceFuncInvoker1< Il2CppObject *, Action_t437523947 * >::Invoke(1 /* System.IDisposable UniRx.IScheduler::Schedule(System.Action) */, IScheduler_t2938318244_il2cpp_TypeInfo_var, (Il2CppObject *)L_7, (Action_t437523947 *)L_11);
		return L_12;
	}
}
// System.Void UniRx.Operators.EmptyObservable`1<UniRx.Unit>::.ctor(UniRx.IScheduler)
extern "C"  void EmptyObservable_1__ctor_m4257457469_gshared (EmptyObservable_1_t952508441 * __this, Il2CppObject * ___scheduler0, const MethodInfo* method)
{
	{
		NullCheck((OperatorObservableBase_1_t1622431009 *)__this);
		((  void (*) (OperatorObservableBase_1_t1622431009 *, bool, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((OperatorObservableBase_1_t1622431009 *)__this, (bool)0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		Il2CppObject * L_0 = ___scheduler0;
		__this->set_scheduler_1(L_0);
		return;
	}
}
// System.IDisposable UniRx.Operators.EmptyObservable`1<UniRx.Unit>::SubscribeCore(UniRx.IObserver`1<T>,System.IDisposable)
extern Il2CppClass* Scheduler_t103934925_il2cpp_TypeInfo_var;
extern Il2CppClass* Disposable_t3736388786_il2cpp_TypeInfo_var;
extern Il2CppClass* Action_t437523947_il2cpp_TypeInfo_var;
extern Il2CppClass* IScheduler_t2938318244_il2cpp_TypeInfo_var;
extern const uint32_t EmptyObservable_1_SubscribeCore_m3404331393_MetadataUsageId;
extern "C"  Il2CppObject * EmptyObservable_1_SubscribeCore_m3404331393_gshared (EmptyObservable_1_t952508441 * __this, Il2CppObject* ___observer0, Il2CppObject * ___cancel1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (EmptyObservable_1_SubscribeCore_m3404331393_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject* L_0 = ___observer0;
		Il2CppObject * L_1 = ___cancel1;
		Empty_t222136576 * L_2 = (Empty_t222136576 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		((  void (*) (Empty_t222136576 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)(L_2, (Il2CppObject*)L_0, (Il2CppObject *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		___observer0 = (Il2CppObject*)L_2;
		Il2CppObject * L_3 = (Il2CppObject *)__this->get_scheduler_1();
		IL2CPP_RUNTIME_CLASS_INIT(Scheduler_t103934925_il2cpp_TypeInfo_var);
		Il2CppObject * L_4 = ((Scheduler_t103934925_StaticFields*)Scheduler_t103934925_il2cpp_TypeInfo_var->static_fields)->get_Immediate_1();
		if ((!(((Il2CppObject*)(Il2CppObject *)L_3) == ((Il2CppObject*)(Il2CppObject *)L_4))))
		{
			goto IL_0025;
		}
	}
	{
		Il2CppObject* L_5 = ___observer0;
		NullCheck((Il2CppObject*)L_5);
		InterfaceActionInvoker0::Invoke(0 /* System.Void UniRx.IObserver`1<UniRx.Unit>::OnCompleted() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4), (Il2CppObject*)L_5);
		IL2CPP_RUNTIME_CLASS_INIT(Disposable_t3736388786_il2cpp_TypeInfo_var);
		Il2CppObject * L_6 = ((Disposable_t3736388786_StaticFields*)Disposable_t3736388786_il2cpp_TypeInfo_var->static_fields)->get_Empty_0();
		return L_6;
	}

IL_0025:
	{
		Il2CppObject * L_7 = (Il2CppObject *)__this->get_scheduler_1();
		Il2CppObject* L_8 = ___observer0;
		Il2CppObject* L_9 = (Il2CppObject*)L_8;
		IntPtr_t L_10;
		L_10.set_m_value_0((void*)(void*)GetInterfaceMethodInfo(L_9, 0, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)));
		Action_t437523947 * L_11 = (Action_t437523947 *)il2cpp_codegen_object_new(Action_t437523947_il2cpp_TypeInfo_var);
		Action__ctor_m2957240604(L_11, (Il2CppObject *)L_9, (IntPtr_t)L_10, /*hidden argument*/NULL);
		NullCheck((Il2CppObject *)L_7);
		Il2CppObject * L_12 = InterfaceFuncInvoker1< Il2CppObject *, Action_t437523947 * >::Invoke(1 /* System.IDisposable UniRx.IScheduler::Schedule(System.Action) */, IScheduler_t2938318244_il2cpp_TypeInfo_var, (Il2CppObject *)L_7, (Action_t437523947 *)L_11);
		return L_12;
	}
}
// System.Void UniRx.Operators.FinallyObservable`1/Finally<System.Object>::.ctor(UniRx.Operators.FinallyObservable`1<T>,UniRx.IObserver`1<T>,System.IDisposable)
extern "C"  void Finally__ctor_m1268980357_gshared (Finally_t1925751780 * __this, FinallyObservable_1_t2652127741 * ___parent0, Il2CppObject* ___observer1, Il2CppObject * ___cancel2, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = ___observer1;
		Il2CppObject * L_1 = ___cancel2;
		NullCheck((OperatorObserverBase_2_t1187768149 *)__this);
		((  void (*) (OperatorObserverBase_2_t1187768149 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((OperatorObserverBase_2_t1187768149 *)__this, (Il2CppObject*)L_0, (Il2CppObject *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		FinallyObservable_1_t2652127741 * L_2 = ___parent0;
		__this->set_parent_2(L_2);
		return;
	}
}
// System.IDisposable UniRx.Operators.FinallyObservable`1/Finally<System.Object>::Run()
extern Il2CppClass* Il2CppObject_il2cpp_TypeInfo_var;
extern Il2CppClass* Action_t437523947_il2cpp_TypeInfo_var;
extern Il2CppClass* Disposable_t3736388786_il2cpp_TypeInfo_var;
extern const uint32_t Finally_Run_m780958517_MetadataUsageId;
extern "C"  Il2CppObject * Finally_Run_m780958517_gshared (Finally_t1925751780 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Finally_Run_m780958517_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Il2CppObject * V_0 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		FinallyObservable_1_t2652127741 * L_0 = (FinallyObservable_1_t2652127741 *)__this->get_parent_2();
		NullCheck(L_0);
		Il2CppObject* L_1 = (Il2CppObject*)L_0->get_source_1();
		NullCheck((Il2CppObject*)L_1);
		Il2CppObject * L_2 = InterfaceFuncInvoker1< Il2CppObject *, Il2CppObject* >::Invoke(0 /* System.IDisposable UniRx.IObservable`1<System.Object>::Subscribe(UniRx.IObserver`1<T>) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), (Il2CppObject*)L_1, (Il2CppObject*)__this);
		V_0 = (Il2CppObject *)L_2;
		goto IL_002f;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1967233988 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Il2CppObject_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_0017;
		throw e;
	}

CATCH_0017:
	{ // begin catch(System.Object)
		{
			FinallyObservable_1_t2652127741 * L_3 = (FinallyObservable_1_t2652127741 *)__this->get_parent_2();
			NullCheck(L_3);
			Action_t437523947 * L_4 = (Action_t437523947 *)L_3->get_finallyAction_2();
			NullCheck((Action_t437523947 *)L_4);
			Action_Invoke_m1445970038((Action_t437523947 *)L_4, /*hidden argument*/NULL);
			IL2CPP_RAISE_MANAGED_EXCEPTION(__exception_local);
		}

IL_002a:
		{
			goto IL_002f;
		}
	} // end catch (depth: 1)

IL_002f:
	{
		Il2CppObject * L_5 = V_0;
		IntPtr_t L_6;
		L_6.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		Action_t437523947 * L_7 = (Action_t437523947 *)il2cpp_codegen_object_new(Action_t437523947_il2cpp_TypeInfo_var);
		Action__ctor_m2957240604(L_7, (Il2CppObject *)__this, (IntPtr_t)L_6, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Disposable_t3736388786_il2cpp_TypeInfo_var);
		Il2CppObject * L_8 = Disposable_Create_m2910846009(NULL /*static, unused*/, (Action_t437523947 *)L_7, /*hidden argument*/NULL);
		Il2CppObject * L_9 = StableCompositeDisposable_Create_m3764788535(NULL /*static, unused*/, (Il2CppObject *)L_5, (Il2CppObject *)L_8, /*hidden argument*/NULL);
		return L_9;
	}
}
// System.Void UniRx.Operators.FinallyObservable`1/Finally<System.Object>::OnNext(T)
extern "C"  void Finally_OnNext_m3205431823_gshared (Finally_t1925751780 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = (Il2CppObject*)((OperatorObserverBase_2_t1187768149 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		Il2CppObject * L_1 = ___value0;
		NullCheck((Il2CppObject*)L_0);
		InterfaceActionInvoker1< Il2CppObject * >::Invoke(2 /* System.Void UniRx.IObserver`1<System.Object>::OnNext(T) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4), (Il2CppObject*)L_0, (Il2CppObject *)L_1);
		return;
	}
}
// System.Void UniRx.Operators.FinallyObservable`1/Finally<System.Object>::OnError(System.Exception)
extern "C"  void Finally_OnError_m3120766750_gshared (Finally_t1925751780 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method)
{
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		Il2CppObject* L_0 = (Il2CppObject*)((OperatorObserverBase_2_t1187768149 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		Exception_t1967233988 * L_1 = ___error0;
		NullCheck((Il2CppObject*)L_0);
		InterfaceActionInvoker1< Exception_t1967233988 * >::Invoke(1 /* System.Void UniRx.IObserver`1<System.Object>::OnError(System.Exception) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4), (Il2CppObject*)L_0, (Exception_t1967233988 *)L_1);
		IL2CPP_LEAVE(0x1A, FINALLY_0013);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0013;
	}

FINALLY_0013:
	{ // begin finally (depth: 1)
		NullCheck((OperatorObserverBase_2_t1187768149 *)__this);
		VirtActionInvoker0::Invoke(4 /* System.Void UniRx.Operators.OperatorObserverBase`2<System.Object,System.Object>::Dispose() */, (OperatorObserverBase_2_t1187768149 *)__this);
		IL2CPP_END_FINALLY(19)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(19)
	{
		IL2CPP_JUMP_TBL(0x1A, IL_001a)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_001a:
	{
		return;
	}
}
// System.Void UniRx.Operators.FinallyObservable`1/Finally<System.Object>::OnCompleted()
extern "C"  void Finally_OnCompleted_m295767921_gshared (Finally_t1925751780 * __this, const MethodInfo* method)
{
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		Il2CppObject* L_0 = (Il2CppObject*)((OperatorObserverBase_2_t1187768149 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		NullCheck((Il2CppObject*)L_0);
		InterfaceActionInvoker0::Invoke(0 /* System.Void UniRx.IObserver`1<System.Object>::OnCompleted() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4), (Il2CppObject*)L_0);
		IL2CPP_LEAVE(0x19, FINALLY_0012);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0012;
	}

FINALLY_0012:
	{ // begin finally (depth: 1)
		NullCheck((OperatorObserverBase_2_t1187768149 *)__this);
		VirtActionInvoker0::Invoke(4 /* System.Void UniRx.Operators.OperatorObserverBase`2<System.Object,System.Object>::Dispose() */, (OperatorObserverBase_2_t1187768149 *)__this);
		IL2CPP_END_FINALLY(18)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(18)
	{
		IL2CPP_JUMP_TBL(0x19, IL_0019)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0019:
	{
		return;
	}
}
// System.Void UniRx.Operators.FinallyObservable`1/Finally<System.Object>::<Run>m__7C()
extern "C"  void Finally_U3CRunU3Em__7C_m2836594759_gshared (Finally_t1925751780 * __this, const MethodInfo* method)
{
	{
		FinallyObservable_1_t2652127741 * L_0 = (FinallyObservable_1_t2652127741 *)__this->get_parent_2();
		NullCheck(L_0);
		Action_t437523947 * L_1 = (Action_t437523947 *)L_0->get_finallyAction_2();
		NullCheck((Action_t437523947 *)L_1);
		Action_Invoke_m1445970038((Action_t437523947 *)L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UniRx.Operators.FinallyObservable`1<System.Object>::.ctor(UniRx.IObservable`1<T>,System.Action)
extern "C"  void FinallyObservable_1__ctor_m2944988073_gshared (FinallyObservable_1_t2652127741 * __this, Il2CppObject* ___source0, Action_t437523947 * ___finallyAction1, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = ___source0;
		bool L_1 = ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)(NULL /*static, unused*/, (Il2CppObject*)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		NullCheck((OperatorObservableBase_1_t4196218687 *)__this);
		((  void (*) (OperatorObservableBase_1_t4196218687 *, bool, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)((OperatorObservableBase_1_t4196218687 *)__this, (bool)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		Il2CppObject* L_2 = ___source0;
		__this->set_source_1(L_2);
		Action_t437523947 * L_3 = ___finallyAction1;
		__this->set_finallyAction_2(L_3);
		return;
	}
}
// System.IDisposable UniRx.Operators.FinallyObservable`1<System.Object>::SubscribeCore(UniRx.IObserver`1<T>,System.IDisposable)
extern "C"  Il2CppObject * FinallyObservable_1_SubscribeCore_m2576989483_gshared (FinallyObservable_1_t2652127741 * __this, Il2CppObject* ___observer0, Il2CppObject * ___cancel1, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = ___observer0;
		Il2CppObject * L_1 = ___cancel1;
		Finally_t1925751780 * L_2 = (Finally_t1925751780 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		((  void (*) (Finally_t1925751780 *, FinallyObservable_1_t2652127741 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)(L_2, (FinallyObservable_1_t2652127741 *)__this, (Il2CppObject*)L_0, (Il2CppObject *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		NullCheck((Finally_t1925751780 *)L_2);
		Il2CppObject * L_3 = ((  Il2CppObject * (*) (Finally_t1925751780 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->method)((Finally_t1925751780 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		return L_3;
	}
}
// System.Void UniRx.Operators.FirstObservable`1/First_<System.Boolean>::.ctor(UniRx.Operators.FirstObservable`1<T>,UniRx.IObserver`1<T>,System.IDisposable)
extern "C"  void First___ctor_m3460627664_gshared (First__t3433238553 * __this, FirstObservable_1_t2233559755 * ___parent0, Il2CppObject* ___observer1, Il2CppObject * ___cancel2, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = ___observer1;
		Il2CppObject * L_1 = ___cancel2;
		NullCheck((OperatorObserverBase_2_t60103313 *)__this);
		((  void (*) (OperatorObserverBase_2_t60103313 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((OperatorObserverBase_2_t60103313 *)__this, (Il2CppObject*)L_0, (Il2CppObject *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		FirstObservable_1_t2233559755 * L_2 = ___parent0;
		__this->set_parent_2(L_2);
		__this->set_notPublished_3((bool)1);
		return;
	}
}
// System.Void UniRx.Operators.FirstObservable`1/First_<System.Boolean>::OnNext(T)
extern Il2CppClass* Exception_t1967233988_il2cpp_TypeInfo_var;
extern const uint32_t First__OnNext_m3056281069_MetadataUsageId;
extern "C"  void First__OnNext_m3056281069_gshared (First__t3433238553 * __this, bool ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (First__OnNext_m3056281069_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	bool V_0 = false;
	Exception_t1967233988 * V_1 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		bool L_0 = (bool)__this->get_notPublished_3();
		if (!L_0)
		{
			goto IL_007b;
		}
	}

IL_000b:
	try
	{ // begin try (depth: 1)
		FirstObservable_1_t2233559755 * L_1 = (FirstObservable_1_t2233559755 *)__this->get_parent_2();
		NullCheck(L_1);
		Func_2_t1008118516 * L_2 = (Func_2_t1008118516 *)L_1->get_predicate_3();
		bool L_3 = ___value0;
		NullCheck((Func_2_t1008118516 *)L_2);
		bool L_4 = ((  bool (*) (Func_2_t1008118516 *, bool, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)((Func_2_t1008118516 *)L_2, (bool)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		V_0 = (bool)L_4;
		goto IL_0047;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1967233988 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Exception_t1967233988_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_0022;
		throw e;
	}

CATCH_0022:
	{ // begin catch(System.Exception)
		{
			V_1 = (Exception_t1967233988 *)((Exception_t1967233988 *)__exception_local);
		}

IL_0023:
		try
		{ // begin try (depth: 2)
			Il2CppObject* L_5 = (Il2CppObject*)((OperatorObserverBase_2_t60103313 *)__this)->get_observer_0();
			il2cpp_codegen_memory_barrier();
			Exception_t1967233988 * L_6 = V_1;
			NullCheck((Il2CppObject*)L_5);
			InterfaceActionInvoker1< Exception_t1967233988 * >::Invoke(1 /* System.Void UniRx.IObserver`1<System.Boolean>::OnError(System.Exception) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3), (Il2CppObject*)L_5, (Exception_t1967233988 *)L_6);
			IL2CPP_LEAVE(0x3D, FINALLY_0036);
		} // end try (depth: 2)
		catch(Il2CppExceptionWrapper& e)
		{
			__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
			goto FINALLY_0036;
		}

FINALLY_0036:
		{ // begin finally (depth: 2)
			NullCheck((OperatorObserverBase_2_t60103313 *)__this);
			VirtActionInvoker0::Invoke(4 /* System.Void UniRx.Operators.OperatorObserverBase`2<System.Boolean,System.Boolean>::Dispose() */, (OperatorObserverBase_2_t60103313 *)__this);
			IL2CPP_END_FINALLY(54)
		} // end finally (depth: 2)
		IL2CPP_CLEANUP(54)
		{
			IL2CPP_JUMP_TBL(0x3D, IL_003d)
			IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
		}

IL_003d:
		{
			goto IL_007b;
		}

IL_0042:
		{
			; // IL_0042: leave IL_0047
		}
	} // end catch (depth: 1)

IL_0047:
	{
		bool L_7 = V_0;
		if (!L_7)
		{
			goto IL_007b;
		}
	}
	{
		__this->set_notPublished_3((bool)0);
		Il2CppObject* L_8 = (Il2CppObject*)((OperatorObserverBase_2_t60103313 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		bool L_9 = ___value0;
		NullCheck((Il2CppObject*)L_8);
		InterfaceActionInvoker1< bool >::Invoke(2 /* System.Void UniRx.IObserver`1<System.Boolean>::OnNext(T) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3), (Il2CppObject*)L_8, (bool)L_9);
	}

IL_0062:
	try
	{ // begin try (depth: 1)
		Il2CppObject* L_10 = (Il2CppObject*)((OperatorObserverBase_2_t60103313 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		NullCheck((Il2CppObject*)L_10);
		InterfaceActionInvoker0::Invoke(0 /* System.Void UniRx.IObserver`1<System.Boolean>::OnCompleted() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3), (Il2CppObject*)L_10);
		IL2CPP_LEAVE(0x7B, FINALLY_0074);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0074;
	}

FINALLY_0074:
	{ // begin finally (depth: 1)
		NullCheck((OperatorObserverBase_2_t60103313 *)__this);
		VirtActionInvoker0::Invoke(4 /* System.Void UniRx.Operators.OperatorObserverBase`2<System.Boolean,System.Boolean>::Dispose() */, (OperatorObserverBase_2_t60103313 *)__this);
		IL2CPP_END_FINALLY(116)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(116)
	{
		IL2CPP_JUMP_TBL(0x7B, IL_007b)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_007b:
	{
		return;
	}
}
// System.Void UniRx.Operators.FirstObservable`1/First_<System.Boolean>::OnError(System.Exception)
extern "C"  void First__OnError_m633849596_gshared (First__t3433238553 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method)
{
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		Il2CppObject* L_0 = (Il2CppObject*)((OperatorObserverBase_2_t60103313 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		Exception_t1967233988 * L_1 = ___error0;
		NullCheck((Il2CppObject*)L_0);
		InterfaceActionInvoker1< Exception_t1967233988 * >::Invoke(1 /* System.Void UniRx.IObserver`1<System.Boolean>::OnError(System.Exception) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3), (Il2CppObject*)L_0, (Exception_t1967233988 *)L_1);
		IL2CPP_LEAVE(0x1A, FINALLY_0013);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0013;
	}

FINALLY_0013:
	{ // begin finally (depth: 1)
		NullCheck((OperatorObserverBase_2_t60103313 *)__this);
		VirtActionInvoker0::Invoke(4 /* System.Void UniRx.Operators.OperatorObserverBase`2<System.Boolean,System.Boolean>::Dispose() */, (OperatorObserverBase_2_t60103313 *)__this);
		IL2CPP_END_FINALLY(19)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(19)
	{
		IL2CPP_JUMP_TBL(0x1A, IL_001a)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_001a:
	{
		return;
	}
}
// System.Void UniRx.Operators.FirstObservable`1/First_<System.Boolean>::OnCompleted()
extern Il2CppClass* Boolean_t211005341_il2cpp_TypeInfo_var;
extern Il2CppClass* InvalidOperationException_t2420574324_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2687971414;
extern const uint32_t First__OnCompleted_m338433103_MetadataUsageId;
extern "C"  void First__OnCompleted_m338433103_gshared (First__t3433238553 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (First__OnCompleted_m338433103_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	bool V_0 = false;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		FirstObservable_1_t2233559755 * L_0 = (FirstObservable_1_t2233559755 *)__this->get_parent_2();
		NullCheck(L_0);
		bool L_1 = (bool)L_0->get_useDefault_2();
		if (!L_1)
		{
			goto IL_004f;
		}
	}
	{
		bool L_2 = (bool)__this->get_notPublished_3();
		if (!L_2)
		{
			goto IL_0031;
		}
	}
	{
		Il2CppObject* L_3 = (Il2CppObject*)((OperatorObserverBase_2_t60103313 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		Initobj (Boolean_t211005341_il2cpp_TypeInfo_var, (&V_0));
		bool L_4 = V_0;
		NullCheck((Il2CppObject*)L_3);
		InterfaceActionInvoker1< bool >::Invoke(2 /* System.Void UniRx.IObserver`1<System.Boolean>::OnNext(T) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3), (Il2CppObject*)L_3, (bool)L_4);
	}

IL_0031:
	try
	{ // begin try (depth: 1)
		Il2CppObject* L_5 = (Il2CppObject*)((OperatorObserverBase_2_t60103313 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		NullCheck((Il2CppObject*)L_5);
		InterfaceActionInvoker0::Invoke(0 /* System.Void UniRx.IObserver`1<System.Boolean>::OnCompleted() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3), (Il2CppObject*)L_5);
		IL2CPP_LEAVE(0x4A, FINALLY_0043);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0043;
	}

FINALLY_0043:
	{ // begin finally (depth: 1)
		NullCheck((OperatorObserverBase_2_t60103313 *)__this);
		VirtActionInvoker0::Invoke(4 /* System.Void UniRx.Operators.OperatorObserverBase`2<System.Boolean,System.Boolean>::Dispose() */, (OperatorObserverBase_2_t60103313 *)__this);
		IL2CPP_END_FINALLY(67)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(67)
	{
		IL2CPP_JUMP_TBL(0x4A, IL_004a)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_004a:
	{
		goto IL_009b;
	}

IL_004f:
	{
		bool L_6 = (bool)__this->get_notPublished_3();
		if (!L_6)
		{
			goto IL_0082;
		}
	}

IL_005a:
	try
	{ // begin try (depth: 1)
		Il2CppObject* L_7 = (Il2CppObject*)((OperatorObserverBase_2_t60103313 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		InvalidOperationException_t2420574324 * L_8 = (InvalidOperationException_t2420574324 *)il2cpp_codegen_object_new(InvalidOperationException_t2420574324_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_8, (String_t*)_stringLiteral2687971414, /*hidden argument*/NULL);
		NullCheck((Il2CppObject*)L_7);
		InterfaceActionInvoker1< Exception_t1967233988 * >::Invoke(1 /* System.Void UniRx.IObserver`1<System.Boolean>::OnError(System.Exception) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3), (Il2CppObject*)L_7, (Exception_t1967233988 *)L_8);
		IL2CPP_LEAVE(0x7D, FINALLY_0076);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0076;
	}

FINALLY_0076:
	{ // begin finally (depth: 1)
		NullCheck((OperatorObserverBase_2_t60103313 *)__this);
		VirtActionInvoker0::Invoke(4 /* System.Void UniRx.Operators.OperatorObserverBase`2<System.Boolean,System.Boolean>::Dispose() */, (OperatorObserverBase_2_t60103313 *)__this);
		IL2CPP_END_FINALLY(118)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(118)
	{
		IL2CPP_JUMP_TBL(0x7D, IL_007d)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_007d:
	{
		goto IL_009b;
	}

IL_0082:
	try
	{ // begin try (depth: 1)
		Il2CppObject* L_9 = (Il2CppObject*)((OperatorObserverBase_2_t60103313 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		NullCheck((Il2CppObject*)L_9);
		InterfaceActionInvoker0::Invoke(0 /* System.Void UniRx.IObserver`1<System.Boolean>::OnCompleted() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3), (Il2CppObject*)L_9);
		IL2CPP_LEAVE(0x9B, FINALLY_0094);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0094;
	}

FINALLY_0094:
	{ // begin finally (depth: 1)
		NullCheck((OperatorObserverBase_2_t60103313 *)__this);
		VirtActionInvoker0::Invoke(4 /* System.Void UniRx.Operators.OperatorObserverBase`2<System.Boolean,System.Boolean>::Dispose() */, (OperatorObserverBase_2_t60103313 *)__this);
		IL2CPP_END_FINALLY(148)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(148)
	{
		IL2CPP_JUMP_TBL(0x9B, IL_009b)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_009b:
	{
		return;
	}
}
// System.Void UniRx.Operators.FirstObservable`1/First_<System.Int64>::.ctor(UniRx.Operators.FirstObservable`1<T>,UniRx.IObserver`1<T>,System.IDisposable)
extern "C"  void First___ctor_m276491221_gshared (First__t1774680798 * __this, FirstObservable_1_t575002000 * ___parent0, Il2CppObject* ___observer1, Il2CppObject * ___cancel2, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = ___observer1;
		Il2CppObject * L_1 = ___cancel2;
		NullCheck((OperatorObserverBase_2_t3939730909 *)__this);
		((  void (*) (OperatorObserverBase_2_t3939730909 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((OperatorObserverBase_2_t3939730909 *)__this, (Il2CppObject*)L_0, (Il2CppObject *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		FirstObservable_1_t575002000 * L_2 = ___parent0;
		__this->set_parent_2(L_2);
		__this->set_notPublished_3((bool)1);
		return;
	}
}
// System.Void UniRx.Operators.FirstObservable`1/First_<System.Int64>::OnNext(T)
extern Il2CppClass* Exception_t1967233988_il2cpp_TypeInfo_var;
extern const uint32_t First__OnNext_m3925266226_MetadataUsageId;
extern "C"  void First__OnNext_m3925266226_gshared (First__t1774680798 * __this, int64_t ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (First__OnNext_m3925266226_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	bool V_0 = false;
	Exception_t1967233988 * V_1 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		bool L_0 = (bool)__this->get_notPublished_3();
		if (!L_0)
		{
			goto IL_007b;
		}
	}

IL_000b:
	try
	{ // begin try (depth: 1)
		FirstObservable_1_t575002000 * L_1 = (FirstObservable_1_t575002000 *)__this->get_parent_2();
		NullCheck(L_1);
		Func_2_t2251336571 * L_2 = (Func_2_t2251336571 *)L_1->get_predicate_3();
		int64_t L_3 = ___value0;
		NullCheck((Func_2_t2251336571 *)L_2);
		bool L_4 = ((  bool (*) (Func_2_t2251336571 *, int64_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)((Func_2_t2251336571 *)L_2, (int64_t)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		V_0 = (bool)L_4;
		goto IL_0047;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1967233988 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Exception_t1967233988_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_0022;
		throw e;
	}

CATCH_0022:
	{ // begin catch(System.Exception)
		{
			V_1 = (Exception_t1967233988 *)((Exception_t1967233988 *)__exception_local);
		}

IL_0023:
		try
		{ // begin try (depth: 2)
			Il2CppObject* L_5 = (Il2CppObject*)((OperatorObserverBase_2_t3939730909 *)__this)->get_observer_0();
			il2cpp_codegen_memory_barrier();
			Exception_t1967233988 * L_6 = V_1;
			NullCheck((Il2CppObject*)L_5);
			InterfaceActionInvoker1< Exception_t1967233988 * >::Invoke(1 /* System.Void UniRx.IObserver`1<System.Int64>::OnError(System.Exception) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3), (Il2CppObject*)L_5, (Exception_t1967233988 *)L_6);
			IL2CPP_LEAVE(0x3D, FINALLY_0036);
		} // end try (depth: 2)
		catch(Il2CppExceptionWrapper& e)
		{
			__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
			goto FINALLY_0036;
		}

FINALLY_0036:
		{ // begin finally (depth: 2)
			NullCheck((OperatorObserverBase_2_t3939730909 *)__this);
			VirtActionInvoker0::Invoke(4 /* System.Void UniRx.Operators.OperatorObserverBase`2<System.Int64,System.Int64>::Dispose() */, (OperatorObserverBase_2_t3939730909 *)__this);
			IL2CPP_END_FINALLY(54)
		} // end finally (depth: 2)
		IL2CPP_CLEANUP(54)
		{
			IL2CPP_JUMP_TBL(0x3D, IL_003d)
			IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
		}

IL_003d:
		{
			goto IL_007b;
		}

IL_0042:
		{
			; // IL_0042: leave IL_0047
		}
	} // end catch (depth: 1)

IL_0047:
	{
		bool L_7 = V_0;
		if (!L_7)
		{
			goto IL_007b;
		}
	}
	{
		__this->set_notPublished_3((bool)0);
		Il2CppObject* L_8 = (Il2CppObject*)((OperatorObserverBase_2_t3939730909 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		int64_t L_9 = ___value0;
		NullCheck((Il2CppObject*)L_8);
		InterfaceActionInvoker1< int64_t >::Invoke(2 /* System.Void UniRx.IObserver`1<System.Int64>::OnNext(T) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3), (Il2CppObject*)L_8, (int64_t)L_9);
	}

IL_0062:
	try
	{ // begin try (depth: 1)
		Il2CppObject* L_10 = (Il2CppObject*)((OperatorObserverBase_2_t3939730909 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		NullCheck((Il2CppObject*)L_10);
		InterfaceActionInvoker0::Invoke(0 /* System.Void UniRx.IObserver`1<System.Int64>::OnCompleted() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3), (Il2CppObject*)L_10);
		IL2CPP_LEAVE(0x7B, FINALLY_0074);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0074;
	}

FINALLY_0074:
	{ // begin finally (depth: 1)
		NullCheck((OperatorObserverBase_2_t3939730909 *)__this);
		VirtActionInvoker0::Invoke(4 /* System.Void UniRx.Operators.OperatorObserverBase`2<System.Int64,System.Int64>::Dispose() */, (OperatorObserverBase_2_t3939730909 *)__this);
		IL2CPP_END_FINALLY(116)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(116)
	{
		IL2CPP_JUMP_TBL(0x7B, IL_007b)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_007b:
	{
		return;
	}
}
// System.Void UniRx.Operators.FirstObservable`1/First_<System.Int64>::OnError(System.Exception)
extern "C"  void First__OnError_m1156204097_gshared (First__t1774680798 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method)
{
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		Il2CppObject* L_0 = (Il2CppObject*)((OperatorObserverBase_2_t3939730909 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		Exception_t1967233988 * L_1 = ___error0;
		NullCheck((Il2CppObject*)L_0);
		InterfaceActionInvoker1< Exception_t1967233988 * >::Invoke(1 /* System.Void UniRx.IObserver`1<System.Int64>::OnError(System.Exception) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3), (Il2CppObject*)L_0, (Exception_t1967233988 *)L_1);
		IL2CPP_LEAVE(0x1A, FINALLY_0013);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0013;
	}

FINALLY_0013:
	{ // begin finally (depth: 1)
		NullCheck((OperatorObserverBase_2_t3939730909 *)__this);
		VirtActionInvoker0::Invoke(4 /* System.Void UniRx.Operators.OperatorObserverBase`2<System.Int64,System.Int64>::Dispose() */, (OperatorObserverBase_2_t3939730909 *)__this);
		IL2CPP_END_FINALLY(19)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(19)
	{
		IL2CPP_JUMP_TBL(0x1A, IL_001a)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_001a:
	{
		return;
	}
}
// System.Void UniRx.Operators.FirstObservable`1/First_<System.Int64>::OnCompleted()
extern Il2CppClass* Int64_t2847414882_il2cpp_TypeInfo_var;
extern Il2CppClass* InvalidOperationException_t2420574324_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2687971414;
extern const uint32_t First__OnCompleted_m3150418708_MetadataUsageId;
extern "C"  void First__OnCompleted_m3150418708_gshared (First__t1774680798 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (First__OnCompleted_m3150418708_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int64_t V_0 = 0;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		FirstObservable_1_t575002000 * L_0 = (FirstObservable_1_t575002000 *)__this->get_parent_2();
		NullCheck(L_0);
		bool L_1 = (bool)L_0->get_useDefault_2();
		if (!L_1)
		{
			goto IL_004f;
		}
	}
	{
		bool L_2 = (bool)__this->get_notPublished_3();
		if (!L_2)
		{
			goto IL_0031;
		}
	}
	{
		Il2CppObject* L_3 = (Il2CppObject*)((OperatorObserverBase_2_t3939730909 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		Initobj (Int64_t2847414882_il2cpp_TypeInfo_var, (&V_0));
		int64_t L_4 = V_0;
		NullCheck((Il2CppObject*)L_3);
		InterfaceActionInvoker1< int64_t >::Invoke(2 /* System.Void UniRx.IObserver`1<System.Int64>::OnNext(T) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3), (Il2CppObject*)L_3, (int64_t)L_4);
	}

IL_0031:
	try
	{ // begin try (depth: 1)
		Il2CppObject* L_5 = (Il2CppObject*)((OperatorObserverBase_2_t3939730909 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		NullCheck((Il2CppObject*)L_5);
		InterfaceActionInvoker0::Invoke(0 /* System.Void UniRx.IObserver`1<System.Int64>::OnCompleted() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3), (Il2CppObject*)L_5);
		IL2CPP_LEAVE(0x4A, FINALLY_0043);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0043;
	}

FINALLY_0043:
	{ // begin finally (depth: 1)
		NullCheck((OperatorObserverBase_2_t3939730909 *)__this);
		VirtActionInvoker0::Invoke(4 /* System.Void UniRx.Operators.OperatorObserverBase`2<System.Int64,System.Int64>::Dispose() */, (OperatorObserverBase_2_t3939730909 *)__this);
		IL2CPP_END_FINALLY(67)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(67)
	{
		IL2CPP_JUMP_TBL(0x4A, IL_004a)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_004a:
	{
		goto IL_009b;
	}

IL_004f:
	{
		bool L_6 = (bool)__this->get_notPublished_3();
		if (!L_6)
		{
			goto IL_0082;
		}
	}

IL_005a:
	try
	{ // begin try (depth: 1)
		Il2CppObject* L_7 = (Il2CppObject*)((OperatorObserverBase_2_t3939730909 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		InvalidOperationException_t2420574324 * L_8 = (InvalidOperationException_t2420574324 *)il2cpp_codegen_object_new(InvalidOperationException_t2420574324_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_8, (String_t*)_stringLiteral2687971414, /*hidden argument*/NULL);
		NullCheck((Il2CppObject*)L_7);
		InterfaceActionInvoker1< Exception_t1967233988 * >::Invoke(1 /* System.Void UniRx.IObserver`1<System.Int64>::OnError(System.Exception) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3), (Il2CppObject*)L_7, (Exception_t1967233988 *)L_8);
		IL2CPP_LEAVE(0x7D, FINALLY_0076);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0076;
	}

FINALLY_0076:
	{ // begin finally (depth: 1)
		NullCheck((OperatorObserverBase_2_t3939730909 *)__this);
		VirtActionInvoker0::Invoke(4 /* System.Void UniRx.Operators.OperatorObserverBase`2<System.Int64,System.Int64>::Dispose() */, (OperatorObserverBase_2_t3939730909 *)__this);
		IL2CPP_END_FINALLY(118)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(118)
	{
		IL2CPP_JUMP_TBL(0x7D, IL_007d)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_007d:
	{
		goto IL_009b;
	}

IL_0082:
	try
	{ // begin try (depth: 1)
		Il2CppObject* L_9 = (Il2CppObject*)((OperatorObserverBase_2_t3939730909 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		NullCheck((Il2CppObject*)L_9);
		InterfaceActionInvoker0::Invoke(0 /* System.Void UniRx.IObserver`1<System.Int64>::OnCompleted() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3), (Il2CppObject*)L_9);
		IL2CPP_LEAVE(0x9B, FINALLY_0094);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0094;
	}

FINALLY_0094:
	{ // begin finally (depth: 1)
		NullCheck((OperatorObserverBase_2_t3939730909 *)__this);
		VirtActionInvoker0::Invoke(4 /* System.Void UniRx.Operators.OperatorObserverBase`2<System.Int64,System.Int64>::Dispose() */, (OperatorObserverBase_2_t3939730909 *)__this);
		IL2CPP_END_FINALLY(148)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(148)
	{
		IL2CPP_JUMP_TBL(0x9B, IL_009b)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_009b:
	{
		return;
	}
}
// System.Void UniRx.Operators.FirstObservable`1/First_<System.Object>::.ctor(UniRx.Operators.FirstObservable`1<T>,UniRx.IObserver`1<T>,System.IDisposable)
extern "C"  void First___ctor_m2754608375_gshared (First__t4059339632 * __this, FirstObservable_1_t2859660834 * ___parent0, Il2CppObject* ___observer1, Il2CppObject * ___cancel2, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = ___observer1;
		Il2CppObject * L_1 = ___cancel2;
		NullCheck((OperatorObserverBase_2_t1187768149 *)__this);
		((  void (*) (OperatorObserverBase_2_t1187768149 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((OperatorObserverBase_2_t1187768149 *)__this, (Il2CppObject*)L_0, (Il2CppObject *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		FirstObservable_1_t2859660834 * L_2 = ___parent0;
		__this->set_parent_2(L_2);
		__this->set_notPublished_3((bool)1);
		return;
	}
}
// System.Void UniRx.Operators.FirstObservable`1/First_<System.Object>::OnNext(T)
extern Il2CppClass* Exception_t1967233988_il2cpp_TypeInfo_var;
extern const uint32_t First__OnNext_m3676607188_MetadataUsageId;
extern "C"  void First__OnNext_m3676607188_gshared (First__t4059339632 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (First__OnNext_m3676607188_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	bool V_0 = false;
	Exception_t1967233988 * V_1 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		bool L_0 = (bool)__this->get_notPublished_3();
		if (!L_0)
		{
			goto IL_007b;
		}
	}

IL_000b:
	try
	{ // begin try (depth: 1)
		FirstObservable_1_t2859660834 * L_1 = (FirstObservable_1_t2859660834 *)__this->get_parent_2();
		NullCheck(L_1);
		Func_2_t1509682273 * L_2 = (Func_2_t1509682273 *)L_1->get_predicate_3();
		Il2CppObject * L_3 = ___value0;
		NullCheck((Func_2_t1509682273 *)L_2);
		bool L_4 = ((  bool (*) (Func_2_t1509682273 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)((Func_2_t1509682273 *)L_2, (Il2CppObject *)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		V_0 = (bool)L_4;
		goto IL_0047;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1967233988 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Exception_t1967233988_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_0022;
		throw e;
	}

CATCH_0022:
	{ // begin catch(System.Exception)
		{
			V_1 = (Exception_t1967233988 *)((Exception_t1967233988 *)__exception_local);
		}

IL_0023:
		try
		{ // begin try (depth: 2)
			Il2CppObject* L_5 = (Il2CppObject*)((OperatorObserverBase_2_t1187768149 *)__this)->get_observer_0();
			il2cpp_codegen_memory_barrier();
			Exception_t1967233988 * L_6 = V_1;
			NullCheck((Il2CppObject*)L_5);
			InterfaceActionInvoker1< Exception_t1967233988 * >::Invoke(1 /* System.Void UniRx.IObserver`1<System.Object>::OnError(System.Exception) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3), (Il2CppObject*)L_5, (Exception_t1967233988 *)L_6);
			IL2CPP_LEAVE(0x3D, FINALLY_0036);
		} // end try (depth: 2)
		catch(Il2CppExceptionWrapper& e)
		{
			__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
			goto FINALLY_0036;
		}

FINALLY_0036:
		{ // begin finally (depth: 2)
			NullCheck((OperatorObserverBase_2_t1187768149 *)__this);
			VirtActionInvoker0::Invoke(4 /* System.Void UniRx.Operators.OperatorObserverBase`2<System.Object,System.Object>::Dispose() */, (OperatorObserverBase_2_t1187768149 *)__this);
			IL2CPP_END_FINALLY(54)
		} // end finally (depth: 2)
		IL2CPP_CLEANUP(54)
		{
			IL2CPP_JUMP_TBL(0x3D, IL_003d)
			IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
		}

IL_003d:
		{
			goto IL_007b;
		}

IL_0042:
		{
			; // IL_0042: leave IL_0047
		}
	} // end catch (depth: 1)

IL_0047:
	{
		bool L_7 = V_0;
		if (!L_7)
		{
			goto IL_007b;
		}
	}
	{
		__this->set_notPublished_3((bool)0);
		Il2CppObject* L_8 = (Il2CppObject*)((OperatorObserverBase_2_t1187768149 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		Il2CppObject * L_9 = ___value0;
		NullCheck((Il2CppObject*)L_8);
		InterfaceActionInvoker1< Il2CppObject * >::Invoke(2 /* System.Void UniRx.IObserver`1<System.Object>::OnNext(T) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3), (Il2CppObject*)L_8, (Il2CppObject *)L_9);
	}

IL_0062:
	try
	{ // begin try (depth: 1)
		Il2CppObject* L_10 = (Il2CppObject*)((OperatorObserverBase_2_t1187768149 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		NullCheck((Il2CppObject*)L_10);
		InterfaceActionInvoker0::Invoke(0 /* System.Void UniRx.IObserver`1<System.Object>::OnCompleted() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3), (Il2CppObject*)L_10);
		IL2CPP_LEAVE(0x7B, FINALLY_0074);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0074;
	}

FINALLY_0074:
	{ // begin finally (depth: 1)
		NullCheck((OperatorObserverBase_2_t1187768149 *)__this);
		VirtActionInvoker0::Invoke(4 /* System.Void UniRx.Operators.OperatorObserverBase`2<System.Object,System.Object>::Dispose() */, (OperatorObserverBase_2_t1187768149 *)__this);
		IL2CPP_END_FINALLY(116)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(116)
	{
		IL2CPP_JUMP_TBL(0x7B, IL_007b)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_007b:
	{
		return;
	}
}
// System.Void UniRx.Operators.FirstObservable`1/First_<System.Object>::OnError(System.Exception)
extern "C"  void First__OnError_m2880472547_gshared (First__t4059339632 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method)
{
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		Il2CppObject* L_0 = (Il2CppObject*)((OperatorObserverBase_2_t1187768149 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		Exception_t1967233988 * L_1 = ___error0;
		NullCheck((Il2CppObject*)L_0);
		InterfaceActionInvoker1< Exception_t1967233988 * >::Invoke(1 /* System.Void UniRx.IObserver`1<System.Object>::OnError(System.Exception) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3), (Il2CppObject*)L_0, (Exception_t1967233988 *)L_1);
		IL2CPP_LEAVE(0x1A, FINALLY_0013);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0013;
	}

FINALLY_0013:
	{ // begin finally (depth: 1)
		NullCheck((OperatorObserverBase_2_t1187768149 *)__this);
		VirtActionInvoker0::Invoke(4 /* System.Void UniRx.Operators.OperatorObserverBase`2<System.Object,System.Object>::Dispose() */, (OperatorObserverBase_2_t1187768149 *)__this);
		IL2CPP_END_FINALLY(19)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(19)
	{
		IL2CPP_JUMP_TBL(0x1A, IL_001a)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_001a:
	{
		return;
	}
}
// System.Void UniRx.Operators.FirstObservable`1/First_<System.Object>::OnCompleted()
extern Il2CppClass* Il2CppObject_il2cpp_TypeInfo_var;
extern Il2CppClass* InvalidOperationException_t2420574324_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2687971414;
extern const uint32_t First__OnCompleted_m323401142_MetadataUsageId;
extern "C"  void First__OnCompleted_m323401142_gshared (First__t4059339632 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (First__OnCompleted_m323401142_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Il2CppObject * V_0 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		FirstObservable_1_t2859660834 * L_0 = (FirstObservable_1_t2859660834 *)__this->get_parent_2();
		NullCheck(L_0);
		bool L_1 = (bool)L_0->get_useDefault_2();
		if (!L_1)
		{
			goto IL_004f;
		}
	}
	{
		bool L_2 = (bool)__this->get_notPublished_3();
		if (!L_2)
		{
			goto IL_0031;
		}
	}
	{
		Il2CppObject* L_3 = (Il2CppObject*)((OperatorObserverBase_2_t1187768149 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		Initobj (Il2CppObject_il2cpp_TypeInfo_var, (&V_0));
		Il2CppObject * L_4 = V_0;
		NullCheck((Il2CppObject*)L_3);
		InterfaceActionInvoker1< Il2CppObject * >::Invoke(2 /* System.Void UniRx.IObserver`1<System.Object>::OnNext(T) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3), (Il2CppObject*)L_3, (Il2CppObject *)L_4);
	}

IL_0031:
	try
	{ // begin try (depth: 1)
		Il2CppObject* L_5 = (Il2CppObject*)((OperatorObserverBase_2_t1187768149 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		NullCheck((Il2CppObject*)L_5);
		InterfaceActionInvoker0::Invoke(0 /* System.Void UniRx.IObserver`1<System.Object>::OnCompleted() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3), (Il2CppObject*)L_5);
		IL2CPP_LEAVE(0x4A, FINALLY_0043);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0043;
	}

FINALLY_0043:
	{ // begin finally (depth: 1)
		NullCheck((OperatorObserverBase_2_t1187768149 *)__this);
		VirtActionInvoker0::Invoke(4 /* System.Void UniRx.Operators.OperatorObserverBase`2<System.Object,System.Object>::Dispose() */, (OperatorObserverBase_2_t1187768149 *)__this);
		IL2CPP_END_FINALLY(67)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(67)
	{
		IL2CPP_JUMP_TBL(0x4A, IL_004a)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_004a:
	{
		goto IL_009b;
	}

IL_004f:
	{
		bool L_6 = (bool)__this->get_notPublished_3();
		if (!L_6)
		{
			goto IL_0082;
		}
	}

IL_005a:
	try
	{ // begin try (depth: 1)
		Il2CppObject* L_7 = (Il2CppObject*)((OperatorObserverBase_2_t1187768149 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		InvalidOperationException_t2420574324 * L_8 = (InvalidOperationException_t2420574324 *)il2cpp_codegen_object_new(InvalidOperationException_t2420574324_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_8, (String_t*)_stringLiteral2687971414, /*hidden argument*/NULL);
		NullCheck((Il2CppObject*)L_7);
		InterfaceActionInvoker1< Exception_t1967233988 * >::Invoke(1 /* System.Void UniRx.IObserver`1<System.Object>::OnError(System.Exception) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3), (Il2CppObject*)L_7, (Exception_t1967233988 *)L_8);
		IL2CPP_LEAVE(0x7D, FINALLY_0076);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0076;
	}

FINALLY_0076:
	{ // begin finally (depth: 1)
		NullCheck((OperatorObserverBase_2_t1187768149 *)__this);
		VirtActionInvoker0::Invoke(4 /* System.Void UniRx.Operators.OperatorObserverBase`2<System.Object,System.Object>::Dispose() */, (OperatorObserverBase_2_t1187768149 *)__this);
		IL2CPP_END_FINALLY(118)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(118)
	{
		IL2CPP_JUMP_TBL(0x7D, IL_007d)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_007d:
	{
		goto IL_009b;
	}

IL_0082:
	try
	{ // begin try (depth: 1)
		Il2CppObject* L_9 = (Il2CppObject*)((OperatorObserverBase_2_t1187768149 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		NullCheck((Il2CppObject*)L_9);
		InterfaceActionInvoker0::Invoke(0 /* System.Void UniRx.IObserver`1<System.Object>::OnCompleted() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3), (Il2CppObject*)L_9);
		IL2CPP_LEAVE(0x9B, FINALLY_0094);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0094;
	}

FINALLY_0094:
	{ // begin finally (depth: 1)
		NullCheck((OperatorObserverBase_2_t1187768149 *)__this);
		VirtActionInvoker0::Invoke(4 /* System.Void UniRx.Operators.OperatorObserverBase`2<System.Object,System.Object>::Dispose() */, (OperatorObserverBase_2_t1187768149 *)__this);
		IL2CPP_END_FINALLY(148)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(148)
	{
		IL2CPP_JUMP_TBL(0x9B, IL_009b)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_009b:
	{
		return;
	}
}
// System.Void UniRx.Operators.FirstObservable`1/First_<UniRx.Tuple`2<System.Object,System.Object>>::.ctor(UniRx.Operators.FirstObservable`1<T>,UniRx.IObserver`1<T>,System.IDisposable)
extern "C"  void First___ctor_m1997630991_gshared (First__t3591495031 * __this, FirstObservable_1_t2391816233 * ___parent0, Il2CppObject* ___observer1, Il2CppObject * ___cancel2, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = ___observer1;
		Il2CppObject * L_1 = ___cancel2;
		NullCheck((OperatorObserverBase_2_t311366489 *)__this);
		((  void (*) (OperatorObserverBase_2_t311366489 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((OperatorObserverBase_2_t311366489 *)__this, (Il2CppObject*)L_0, (Il2CppObject *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		FirstObservable_1_t2391816233 * L_2 = ___parent0;
		__this->set_parent_2(L_2);
		__this->set_notPublished_3((bool)1);
		return;
	}
}
// System.Void UniRx.Operators.FirstObservable`1/First_<UniRx.Tuple`2<System.Object,System.Object>>::OnNext(T)
extern Il2CppClass* Exception_t1967233988_il2cpp_TypeInfo_var;
extern const uint32_t First__OnNext_m215532012_MetadataUsageId;
extern "C"  void First__OnNext_m215532012_gshared (First__t3591495031 * __this, Tuple_2_t369261819  ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (First__OnNext_m215532012_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	bool V_0 = false;
	Exception_t1967233988 * V_1 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		bool L_0 = (bool)__this->get_notPublished_3();
		if (!L_0)
		{
			goto IL_007b;
		}
	}

IL_000b:
	try
	{ // begin try (depth: 1)
		FirstObservable_1_t2391816233 * L_1 = (FirstObservable_1_t2391816233 *)__this->get_parent_2();
		NullCheck(L_1);
		Func_2_t1101125214 * L_2 = (Func_2_t1101125214 *)L_1->get_predicate_3();
		Tuple_2_t369261819  L_3 = ___value0;
		NullCheck((Func_2_t1101125214 *)L_2);
		bool L_4 = ((  bool (*) (Func_2_t1101125214 *, Tuple_2_t369261819 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)((Func_2_t1101125214 *)L_2, (Tuple_2_t369261819 )L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		V_0 = (bool)L_4;
		goto IL_0047;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1967233988 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Exception_t1967233988_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_0022;
		throw e;
	}

CATCH_0022:
	{ // begin catch(System.Exception)
		{
			V_1 = (Exception_t1967233988 *)((Exception_t1967233988 *)__exception_local);
		}

IL_0023:
		try
		{ // begin try (depth: 2)
			Il2CppObject* L_5 = (Il2CppObject*)((OperatorObserverBase_2_t311366489 *)__this)->get_observer_0();
			il2cpp_codegen_memory_barrier();
			Exception_t1967233988 * L_6 = V_1;
			NullCheck((Il2CppObject*)L_5);
			InterfaceActionInvoker1< Exception_t1967233988 * >::Invoke(1 /* System.Void UniRx.IObserver`1<UniRx.Tuple`2<System.Object,System.Object>>::OnError(System.Exception) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3), (Il2CppObject*)L_5, (Exception_t1967233988 *)L_6);
			IL2CPP_LEAVE(0x3D, FINALLY_0036);
		} // end try (depth: 2)
		catch(Il2CppExceptionWrapper& e)
		{
			__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
			goto FINALLY_0036;
		}

FINALLY_0036:
		{ // begin finally (depth: 2)
			NullCheck((OperatorObserverBase_2_t311366489 *)__this);
			VirtActionInvoker0::Invoke(4 /* System.Void UniRx.Operators.OperatorObserverBase`2<UniRx.Tuple`2<System.Object,System.Object>,UniRx.Tuple`2<System.Object,System.Object>>::Dispose() */, (OperatorObserverBase_2_t311366489 *)__this);
			IL2CPP_END_FINALLY(54)
		} // end finally (depth: 2)
		IL2CPP_CLEANUP(54)
		{
			IL2CPP_JUMP_TBL(0x3D, IL_003d)
			IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
		}

IL_003d:
		{
			goto IL_007b;
		}

IL_0042:
		{
			; // IL_0042: leave IL_0047
		}
	} // end catch (depth: 1)

IL_0047:
	{
		bool L_7 = V_0;
		if (!L_7)
		{
			goto IL_007b;
		}
	}
	{
		__this->set_notPublished_3((bool)0);
		Il2CppObject* L_8 = (Il2CppObject*)((OperatorObserverBase_2_t311366489 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		Tuple_2_t369261819  L_9 = ___value0;
		NullCheck((Il2CppObject*)L_8);
		InterfaceActionInvoker1< Tuple_2_t369261819  >::Invoke(2 /* System.Void UniRx.IObserver`1<UniRx.Tuple`2<System.Object,System.Object>>::OnNext(T) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3), (Il2CppObject*)L_8, (Tuple_2_t369261819 )L_9);
	}

IL_0062:
	try
	{ // begin try (depth: 1)
		Il2CppObject* L_10 = (Il2CppObject*)((OperatorObserverBase_2_t311366489 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		NullCheck((Il2CppObject*)L_10);
		InterfaceActionInvoker0::Invoke(0 /* System.Void UniRx.IObserver`1<UniRx.Tuple`2<System.Object,System.Object>>::OnCompleted() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3), (Il2CppObject*)L_10);
		IL2CPP_LEAVE(0x7B, FINALLY_0074);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0074;
	}

FINALLY_0074:
	{ // begin finally (depth: 1)
		NullCheck((OperatorObserverBase_2_t311366489 *)__this);
		VirtActionInvoker0::Invoke(4 /* System.Void UniRx.Operators.OperatorObserverBase`2<UniRx.Tuple`2<System.Object,System.Object>,UniRx.Tuple`2<System.Object,System.Object>>::Dispose() */, (OperatorObserverBase_2_t311366489 *)__this);
		IL2CPP_END_FINALLY(116)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(116)
	{
		IL2CPP_JUMP_TBL(0x7B, IL_007b)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_007b:
	{
		return;
	}
}
// System.Void UniRx.Operators.FirstObservable`1/First_<UniRx.Tuple`2<System.Object,System.Object>>::OnError(System.Exception)
extern "C"  void First__OnError_m3037955323_gshared (First__t3591495031 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method)
{
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		Il2CppObject* L_0 = (Il2CppObject*)((OperatorObserverBase_2_t311366489 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		Exception_t1967233988 * L_1 = ___error0;
		NullCheck((Il2CppObject*)L_0);
		InterfaceActionInvoker1< Exception_t1967233988 * >::Invoke(1 /* System.Void UniRx.IObserver`1<UniRx.Tuple`2<System.Object,System.Object>>::OnError(System.Exception) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3), (Il2CppObject*)L_0, (Exception_t1967233988 *)L_1);
		IL2CPP_LEAVE(0x1A, FINALLY_0013);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0013;
	}

FINALLY_0013:
	{ // begin finally (depth: 1)
		NullCheck((OperatorObserverBase_2_t311366489 *)__this);
		VirtActionInvoker0::Invoke(4 /* System.Void UniRx.Operators.OperatorObserverBase`2<UniRx.Tuple`2<System.Object,System.Object>,UniRx.Tuple`2<System.Object,System.Object>>::Dispose() */, (OperatorObserverBase_2_t311366489 *)__this);
		IL2CPP_END_FINALLY(19)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(19)
	{
		IL2CPP_JUMP_TBL(0x1A, IL_001a)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_001a:
	{
		return;
	}
}
// System.Void UniRx.Operators.FirstObservable`1/First_<UniRx.Tuple`2<System.Object,System.Object>>::OnCompleted()
extern Il2CppClass* Tuple_2_t369261819_il2cpp_TypeInfo_var;
extern Il2CppClass* InvalidOperationException_t2420574324_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2687971414;
extern const uint32_t First__OnCompleted_m3801979086_MetadataUsageId;
extern "C"  void First__OnCompleted_m3801979086_gshared (First__t3591495031 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (First__OnCompleted_m3801979086_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Tuple_2_t369261819  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		FirstObservable_1_t2391816233 * L_0 = (FirstObservable_1_t2391816233 *)__this->get_parent_2();
		NullCheck(L_0);
		bool L_1 = (bool)L_0->get_useDefault_2();
		if (!L_1)
		{
			goto IL_004f;
		}
	}
	{
		bool L_2 = (bool)__this->get_notPublished_3();
		if (!L_2)
		{
			goto IL_0031;
		}
	}
	{
		Il2CppObject* L_3 = (Il2CppObject*)((OperatorObserverBase_2_t311366489 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		Initobj (Tuple_2_t369261819_il2cpp_TypeInfo_var, (&V_0));
		Tuple_2_t369261819  L_4 = V_0;
		NullCheck((Il2CppObject*)L_3);
		InterfaceActionInvoker1< Tuple_2_t369261819  >::Invoke(2 /* System.Void UniRx.IObserver`1<UniRx.Tuple`2<System.Object,System.Object>>::OnNext(T) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3), (Il2CppObject*)L_3, (Tuple_2_t369261819 )L_4);
	}

IL_0031:
	try
	{ // begin try (depth: 1)
		Il2CppObject* L_5 = (Il2CppObject*)((OperatorObserverBase_2_t311366489 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		NullCheck((Il2CppObject*)L_5);
		InterfaceActionInvoker0::Invoke(0 /* System.Void UniRx.IObserver`1<UniRx.Tuple`2<System.Object,System.Object>>::OnCompleted() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3), (Il2CppObject*)L_5);
		IL2CPP_LEAVE(0x4A, FINALLY_0043);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0043;
	}

FINALLY_0043:
	{ // begin finally (depth: 1)
		NullCheck((OperatorObserverBase_2_t311366489 *)__this);
		VirtActionInvoker0::Invoke(4 /* System.Void UniRx.Operators.OperatorObserverBase`2<UniRx.Tuple`2<System.Object,System.Object>,UniRx.Tuple`2<System.Object,System.Object>>::Dispose() */, (OperatorObserverBase_2_t311366489 *)__this);
		IL2CPP_END_FINALLY(67)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(67)
	{
		IL2CPP_JUMP_TBL(0x4A, IL_004a)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_004a:
	{
		goto IL_009b;
	}

IL_004f:
	{
		bool L_6 = (bool)__this->get_notPublished_3();
		if (!L_6)
		{
			goto IL_0082;
		}
	}

IL_005a:
	try
	{ // begin try (depth: 1)
		Il2CppObject* L_7 = (Il2CppObject*)((OperatorObserverBase_2_t311366489 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		InvalidOperationException_t2420574324 * L_8 = (InvalidOperationException_t2420574324 *)il2cpp_codegen_object_new(InvalidOperationException_t2420574324_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_8, (String_t*)_stringLiteral2687971414, /*hidden argument*/NULL);
		NullCheck((Il2CppObject*)L_7);
		InterfaceActionInvoker1< Exception_t1967233988 * >::Invoke(1 /* System.Void UniRx.IObserver`1<UniRx.Tuple`2<System.Object,System.Object>>::OnError(System.Exception) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3), (Il2CppObject*)L_7, (Exception_t1967233988 *)L_8);
		IL2CPP_LEAVE(0x7D, FINALLY_0076);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0076;
	}

FINALLY_0076:
	{ // begin finally (depth: 1)
		NullCheck((OperatorObserverBase_2_t311366489 *)__this);
		VirtActionInvoker0::Invoke(4 /* System.Void UniRx.Operators.OperatorObserverBase`2<UniRx.Tuple`2<System.Object,System.Object>,UniRx.Tuple`2<System.Object,System.Object>>::Dispose() */, (OperatorObserverBase_2_t311366489 *)__this);
		IL2CPP_END_FINALLY(118)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(118)
	{
		IL2CPP_JUMP_TBL(0x7D, IL_007d)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_007d:
	{
		goto IL_009b;
	}

IL_0082:
	try
	{ // begin try (depth: 1)
		Il2CppObject* L_9 = (Il2CppObject*)((OperatorObserverBase_2_t311366489 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		NullCheck((Il2CppObject*)L_9);
		InterfaceActionInvoker0::Invoke(0 /* System.Void UniRx.IObserver`1<UniRx.Tuple`2<System.Object,System.Object>>::OnCompleted() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3), (Il2CppObject*)L_9);
		IL2CPP_LEAVE(0x9B, FINALLY_0094);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0094;
	}

FINALLY_0094:
	{ // begin finally (depth: 1)
		NullCheck((OperatorObserverBase_2_t311366489 *)__this);
		VirtActionInvoker0::Invoke(4 /* System.Void UniRx.Operators.OperatorObserverBase`2<UniRx.Tuple`2<System.Object,System.Object>,UniRx.Tuple`2<System.Object,System.Object>>::Dispose() */, (OperatorObserverBase_2_t311366489 *)__this);
		IL2CPP_END_FINALLY(148)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(148)
	{
		IL2CPP_JUMP_TBL(0x9B, IL_009b)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_009b:
	{
		return;
	}
}
// System.Void UniRx.Operators.FirstObservable`1/First_<UniRx.Unit>::.ctor(UniRx.Operators.FirstObservable`1<T>,UniRx.IObserver`1<T>,System.IDisposable)
extern "C"  void First___ctor_m1383866883_gshared (First__t1485551954 * __this, FirstObservable_1_t285873156 * ___parent0, Il2CppObject* ___observer1, Il2CppObject * ___cancel2, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = ___observer1;
		Il2CppObject * L_1 = ___cancel2;
		NullCheck((OperatorObserverBase_2_t4165376397 *)__this);
		((  void (*) (OperatorObserverBase_2_t4165376397 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((OperatorObserverBase_2_t4165376397 *)__this, (Il2CppObject*)L_0, (Il2CppObject *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		FirstObservable_1_t285873156 * L_2 = ___parent0;
		__this->set_parent_2(L_2);
		__this->set_notPublished_3((bool)1);
		return;
	}
}
// System.Void UniRx.Operators.FirstObservable`1/First_<UniRx.Unit>::OnNext(T)
extern Il2CppClass* Exception_t1967233988_il2cpp_TypeInfo_var;
extern const uint32_t First__OnNext_m3791894240_MetadataUsageId;
extern "C"  void First__OnNext_m3791894240_gshared (First__t1485551954 * __this, Unit_t2558286038  ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (First__OnNext_m3791894240_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	bool V_0 = false;
	Exception_t1967233988 * V_1 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		bool L_0 = (bool)__this->get_notPublished_3();
		if (!L_0)
		{
			goto IL_007b;
		}
	}

IL_000b:
	try
	{ // begin try (depth: 1)
		FirstObservable_1_t285873156 * L_1 = (FirstObservable_1_t285873156 *)__this->get_parent_2();
		NullCheck(L_1);
		Func_2_t2766110903 * L_2 = (Func_2_t2766110903 *)L_1->get_predicate_3();
		Unit_t2558286038  L_3 = ___value0;
		NullCheck((Func_2_t2766110903 *)L_2);
		bool L_4 = ((  bool (*) (Func_2_t2766110903 *, Unit_t2558286038 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)((Func_2_t2766110903 *)L_2, (Unit_t2558286038 )L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		V_0 = (bool)L_4;
		goto IL_0047;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1967233988 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Exception_t1967233988_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_0022;
		throw e;
	}

CATCH_0022:
	{ // begin catch(System.Exception)
		{
			V_1 = (Exception_t1967233988 *)((Exception_t1967233988 *)__exception_local);
		}

IL_0023:
		try
		{ // begin try (depth: 2)
			Il2CppObject* L_5 = (Il2CppObject*)((OperatorObserverBase_2_t4165376397 *)__this)->get_observer_0();
			il2cpp_codegen_memory_barrier();
			Exception_t1967233988 * L_6 = V_1;
			NullCheck((Il2CppObject*)L_5);
			InterfaceActionInvoker1< Exception_t1967233988 * >::Invoke(1 /* System.Void UniRx.IObserver`1<UniRx.Unit>::OnError(System.Exception) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3), (Il2CppObject*)L_5, (Exception_t1967233988 *)L_6);
			IL2CPP_LEAVE(0x3D, FINALLY_0036);
		} // end try (depth: 2)
		catch(Il2CppExceptionWrapper& e)
		{
			__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
			goto FINALLY_0036;
		}

FINALLY_0036:
		{ // begin finally (depth: 2)
			NullCheck((OperatorObserverBase_2_t4165376397 *)__this);
			VirtActionInvoker0::Invoke(4 /* System.Void UniRx.Operators.OperatorObserverBase`2<UniRx.Unit,UniRx.Unit>::Dispose() */, (OperatorObserverBase_2_t4165376397 *)__this);
			IL2CPP_END_FINALLY(54)
		} // end finally (depth: 2)
		IL2CPP_CLEANUP(54)
		{
			IL2CPP_JUMP_TBL(0x3D, IL_003d)
			IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
		}

IL_003d:
		{
			goto IL_007b;
		}

IL_0042:
		{
			; // IL_0042: leave IL_0047
		}
	} // end catch (depth: 1)

IL_0047:
	{
		bool L_7 = V_0;
		if (!L_7)
		{
			goto IL_007b;
		}
	}
	{
		__this->set_notPublished_3((bool)0);
		Il2CppObject* L_8 = (Il2CppObject*)((OperatorObserverBase_2_t4165376397 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		Unit_t2558286038  L_9 = ___value0;
		NullCheck((Il2CppObject*)L_8);
		InterfaceActionInvoker1< Unit_t2558286038  >::Invoke(2 /* System.Void UniRx.IObserver`1<UniRx.Unit>::OnNext(T) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3), (Il2CppObject*)L_8, (Unit_t2558286038 )L_9);
	}

IL_0062:
	try
	{ // begin try (depth: 1)
		Il2CppObject* L_10 = (Il2CppObject*)((OperatorObserverBase_2_t4165376397 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		NullCheck((Il2CppObject*)L_10);
		InterfaceActionInvoker0::Invoke(0 /* System.Void UniRx.IObserver`1<UniRx.Unit>::OnCompleted() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3), (Il2CppObject*)L_10);
		IL2CPP_LEAVE(0x7B, FINALLY_0074);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0074;
	}

FINALLY_0074:
	{ // begin finally (depth: 1)
		NullCheck((OperatorObserverBase_2_t4165376397 *)__this);
		VirtActionInvoker0::Invoke(4 /* System.Void UniRx.Operators.OperatorObserverBase`2<UniRx.Unit,UniRx.Unit>::Dispose() */, (OperatorObserverBase_2_t4165376397 *)__this);
		IL2CPP_END_FINALLY(116)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(116)
	{
		IL2CPP_JUMP_TBL(0x7B, IL_007b)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_007b:
	{
		return;
	}
}
// System.Void UniRx.Operators.FirstObservable`1/First_<UniRx.Unit>::OnError(System.Exception)
extern "C"  void First__OnError_m3216904687_gshared (First__t1485551954 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method)
{
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		Il2CppObject* L_0 = (Il2CppObject*)((OperatorObserverBase_2_t4165376397 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		Exception_t1967233988 * L_1 = ___error0;
		NullCheck((Il2CppObject*)L_0);
		InterfaceActionInvoker1< Exception_t1967233988 * >::Invoke(1 /* System.Void UniRx.IObserver`1<UniRx.Unit>::OnError(System.Exception) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3), (Il2CppObject*)L_0, (Exception_t1967233988 *)L_1);
		IL2CPP_LEAVE(0x1A, FINALLY_0013);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0013;
	}

FINALLY_0013:
	{ // begin finally (depth: 1)
		NullCheck((OperatorObserverBase_2_t4165376397 *)__this);
		VirtActionInvoker0::Invoke(4 /* System.Void UniRx.Operators.OperatorObserverBase`2<UniRx.Unit,UniRx.Unit>::Dispose() */, (OperatorObserverBase_2_t4165376397 *)__this);
		IL2CPP_END_FINALLY(19)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(19)
	{
		IL2CPP_JUMP_TBL(0x1A, IL_001a)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_001a:
	{
		return;
	}
}
// System.Void UniRx.Operators.FirstObservable`1/First_<UniRx.Unit>::OnCompleted()
extern Il2CppClass* Unit_t2558286038_il2cpp_TypeInfo_var;
extern Il2CppClass* InvalidOperationException_t2420574324_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2687971414;
extern const uint32_t First__OnCompleted_m2392650690_MetadataUsageId;
extern "C"  void First__OnCompleted_m2392650690_gshared (First__t1485551954 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (First__OnCompleted_m2392650690_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Unit_t2558286038  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		FirstObservable_1_t285873156 * L_0 = (FirstObservable_1_t285873156 *)__this->get_parent_2();
		NullCheck(L_0);
		bool L_1 = (bool)L_0->get_useDefault_2();
		if (!L_1)
		{
			goto IL_004f;
		}
	}
	{
		bool L_2 = (bool)__this->get_notPublished_3();
		if (!L_2)
		{
			goto IL_0031;
		}
	}
	{
		Il2CppObject* L_3 = (Il2CppObject*)((OperatorObserverBase_2_t4165376397 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		Initobj (Unit_t2558286038_il2cpp_TypeInfo_var, (&V_0));
		Unit_t2558286038  L_4 = V_0;
		NullCheck((Il2CppObject*)L_3);
		InterfaceActionInvoker1< Unit_t2558286038  >::Invoke(2 /* System.Void UniRx.IObserver`1<UniRx.Unit>::OnNext(T) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3), (Il2CppObject*)L_3, (Unit_t2558286038 )L_4);
	}

IL_0031:
	try
	{ // begin try (depth: 1)
		Il2CppObject* L_5 = (Il2CppObject*)((OperatorObserverBase_2_t4165376397 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		NullCheck((Il2CppObject*)L_5);
		InterfaceActionInvoker0::Invoke(0 /* System.Void UniRx.IObserver`1<UniRx.Unit>::OnCompleted() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3), (Il2CppObject*)L_5);
		IL2CPP_LEAVE(0x4A, FINALLY_0043);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0043;
	}

FINALLY_0043:
	{ // begin finally (depth: 1)
		NullCheck((OperatorObserverBase_2_t4165376397 *)__this);
		VirtActionInvoker0::Invoke(4 /* System.Void UniRx.Operators.OperatorObserverBase`2<UniRx.Unit,UniRx.Unit>::Dispose() */, (OperatorObserverBase_2_t4165376397 *)__this);
		IL2CPP_END_FINALLY(67)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(67)
	{
		IL2CPP_JUMP_TBL(0x4A, IL_004a)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_004a:
	{
		goto IL_009b;
	}

IL_004f:
	{
		bool L_6 = (bool)__this->get_notPublished_3();
		if (!L_6)
		{
			goto IL_0082;
		}
	}

IL_005a:
	try
	{ // begin try (depth: 1)
		Il2CppObject* L_7 = (Il2CppObject*)((OperatorObserverBase_2_t4165376397 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		InvalidOperationException_t2420574324 * L_8 = (InvalidOperationException_t2420574324 *)il2cpp_codegen_object_new(InvalidOperationException_t2420574324_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_8, (String_t*)_stringLiteral2687971414, /*hidden argument*/NULL);
		NullCheck((Il2CppObject*)L_7);
		InterfaceActionInvoker1< Exception_t1967233988 * >::Invoke(1 /* System.Void UniRx.IObserver`1<UniRx.Unit>::OnError(System.Exception) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3), (Il2CppObject*)L_7, (Exception_t1967233988 *)L_8);
		IL2CPP_LEAVE(0x7D, FINALLY_0076);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0076;
	}

FINALLY_0076:
	{ // begin finally (depth: 1)
		NullCheck((OperatorObserverBase_2_t4165376397 *)__this);
		VirtActionInvoker0::Invoke(4 /* System.Void UniRx.Operators.OperatorObserverBase`2<UniRx.Unit,UniRx.Unit>::Dispose() */, (OperatorObserverBase_2_t4165376397 *)__this);
		IL2CPP_END_FINALLY(118)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(118)
	{
		IL2CPP_JUMP_TBL(0x7D, IL_007d)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_007d:
	{
		goto IL_009b;
	}

IL_0082:
	try
	{ // begin try (depth: 1)
		Il2CppObject* L_9 = (Il2CppObject*)((OperatorObserverBase_2_t4165376397 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		NullCheck((Il2CppObject*)L_9);
		InterfaceActionInvoker0::Invoke(0 /* System.Void UniRx.IObserver`1<UniRx.Unit>::OnCompleted() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3), (Il2CppObject*)L_9);
		IL2CPP_LEAVE(0x9B, FINALLY_0094);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0094;
	}

FINALLY_0094:
	{ // begin finally (depth: 1)
		NullCheck((OperatorObserverBase_2_t4165376397 *)__this);
		VirtActionInvoker0::Invoke(4 /* System.Void UniRx.Operators.OperatorObserverBase`2<UniRx.Unit,UniRx.Unit>::Dispose() */, (OperatorObserverBase_2_t4165376397 *)__this);
		IL2CPP_END_FINALLY(148)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(148)
	{
		IL2CPP_JUMP_TBL(0x9B, IL_009b)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_009b:
	{
		return;
	}
}
// System.Void UniRx.Operators.FirstObservable`1/First<System.Boolean>::.ctor(UniRx.Operators.FirstObservable`1<T>,UniRx.IObserver`1<T>,System.IDisposable)
extern "C"  void First__ctor_m355693915_gshared (First_t2588875826 * __this, FirstObservable_1_t2233559755 * ___parent0, Il2CppObject* ___observer1, Il2CppObject * ___cancel2, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = ___observer1;
		Il2CppObject * L_1 = ___cancel2;
		NullCheck((OperatorObserverBase_2_t60103313 *)__this);
		((  void (*) (OperatorObserverBase_2_t60103313 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((OperatorObserverBase_2_t60103313 *)__this, (Il2CppObject*)L_0, (Il2CppObject *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		FirstObservable_1_t2233559755 * L_2 = ___parent0;
		__this->set_parent_2(L_2);
		__this->set_notPublished_3((bool)1);
		return;
	}
}
// System.Void UniRx.Operators.FirstObservable`1/First<System.Boolean>::OnNext(T)
extern "C"  void First_OnNext_m3616118840_gshared (First_t2588875826 * __this, bool ___value0, const MethodInfo* method)
{
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		bool L_0 = (bool)__this->get_notPublished_3();
		if (!L_0)
		{
			goto IL_003a;
		}
	}
	{
		__this->set_notPublished_3((bool)0);
		Il2CppObject* L_1 = (Il2CppObject*)((OperatorObserverBase_2_t60103313 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		bool L_2 = ___value0;
		NullCheck((Il2CppObject*)L_1);
		InterfaceActionInvoker1< bool >::Invoke(2 /* System.Void UniRx.IObserver`1<System.Boolean>::OnNext(T) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), (Il2CppObject*)L_1, (bool)L_2);
	}

IL_0020:
	try
	{ // begin try (depth: 1)
		Il2CppObject* L_3 = (Il2CppObject*)((OperatorObserverBase_2_t60103313 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		NullCheck((Il2CppObject*)L_3);
		InterfaceActionInvoker0::Invoke(0 /* System.Void UniRx.IObserver`1<System.Boolean>::OnCompleted() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), (Il2CppObject*)L_3);
		IL2CPP_LEAVE(0x39, FINALLY_0032);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0032;
	}

FINALLY_0032:
	{ // begin finally (depth: 1)
		NullCheck((OperatorObserverBase_2_t60103313 *)__this);
		VirtActionInvoker0::Invoke(4 /* System.Void UniRx.Operators.OperatorObserverBase`2<System.Boolean,System.Boolean>::Dispose() */, (OperatorObserverBase_2_t60103313 *)__this);
		IL2CPP_END_FINALLY(50)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(50)
	{
		IL2CPP_JUMP_TBL(0x39, IL_0039)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0039:
	{
		return;
	}

IL_003a:
	{
		return;
	}
}
// System.Void UniRx.Operators.FirstObservable`1/First<System.Boolean>::OnError(System.Exception)
extern "C"  void First_OnError_m298206023_gshared (First_t2588875826 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method)
{
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		Il2CppObject* L_0 = (Il2CppObject*)((OperatorObserverBase_2_t60103313 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		Exception_t1967233988 * L_1 = ___error0;
		NullCheck((Il2CppObject*)L_0);
		InterfaceActionInvoker1< Exception_t1967233988 * >::Invoke(1 /* System.Void UniRx.IObserver`1<System.Boolean>::OnError(System.Exception) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), (Il2CppObject*)L_0, (Exception_t1967233988 *)L_1);
		IL2CPP_LEAVE(0x1A, FINALLY_0013);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0013;
	}

FINALLY_0013:
	{ // begin finally (depth: 1)
		NullCheck((OperatorObserverBase_2_t60103313 *)__this);
		VirtActionInvoker0::Invoke(4 /* System.Void UniRx.Operators.OperatorObserverBase`2<System.Boolean,System.Boolean>::Dispose() */, (OperatorObserverBase_2_t60103313 *)__this);
		IL2CPP_END_FINALLY(19)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(19)
	{
		IL2CPP_JUMP_TBL(0x1A, IL_001a)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_001a:
	{
		return;
	}
}
// System.Void UniRx.Operators.FirstObservable`1/First<System.Boolean>::OnCompleted()
extern Il2CppClass* Boolean_t211005341_il2cpp_TypeInfo_var;
extern Il2CppClass* InvalidOperationException_t2420574324_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2687971414;
extern const uint32_t First_OnCompleted_m2703386906_MetadataUsageId;
extern "C"  void First_OnCompleted_m2703386906_gshared (First_t2588875826 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (First_OnCompleted_m2703386906_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	bool V_0 = false;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		FirstObservable_1_t2233559755 * L_0 = (FirstObservable_1_t2233559755 *)__this->get_parent_2();
		NullCheck(L_0);
		bool L_1 = (bool)L_0->get_useDefault_2();
		if (!L_1)
		{
			goto IL_004f;
		}
	}
	{
		bool L_2 = (bool)__this->get_notPublished_3();
		if (!L_2)
		{
			goto IL_0031;
		}
	}
	{
		Il2CppObject* L_3 = (Il2CppObject*)((OperatorObserverBase_2_t60103313 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		Initobj (Boolean_t211005341_il2cpp_TypeInfo_var, (&V_0));
		bool L_4 = V_0;
		NullCheck((Il2CppObject*)L_3);
		InterfaceActionInvoker1< bool >::Invoke(2 /* System.Void UniRx.IObserver`1<System.Boolean>::OnNext(T) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), (Il2CppObject*)L_3, (bool)L_4);
	}

IL_0031:
	try
	{ // begin try (depth: 1)
		Il2CppObject* L_5 = (Il2CppObject*)((OperatorObserverBase_2_t60103313 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		NullCheck((Il2CppObject*)L_5);
		InterfaceActionInvoker0::Invoke(0 /* System.Void UniRx.IObserver`1<System.Boolean>::OnCompleted() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), (Il2CppObject*)L_5);
		IL2CPP_LEAVE(0x4A, FINALLY_0043);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0043;
	}

FINALLY_0043:
	{ // begin finally (depth: 1)
		NullCheck((OperatorObserverBase_2_t60103313 *)__this);
		VirtActionInvoker0::Invoke(4 /* System.Void UniRx.Operators.OperatorObserverBase`2<System.Boolean,System.Boolean>::Dispose() */, (OperatorObserverBase_2_t60103313 *)__this);
		IL2CPP_END_FINALLY(67)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(67)
	{
		IL2CPP_JUMP_TBL(0x4A, IL_004a)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_004a:
	{
		goto IL_009b;
	}

IL_004f:
	{
		bool L_6 = (bool)__this->get_notPublished_3();
		if (!L_6)
		{
			goto IL_0082;
		}
	}

IL_005a:
	try
	{ // begin try (depth: 1)
		Il2CppObject* L_7 = (Il2CppObject*)((OperatorObserverBase_2_t60103313 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		InvalidOperationException_t2420574324 * L_8 = (InvalidOperationException_t2420574324 *)il2cpp_codegen_object_new(InvalidOperationException_t2420574324_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_8, (String_t*)_stringLiteral2687971414, /*hidden argument*/NULL);
		NullCheck((Il2CppObject*)L_7);
		InterfaceActionInvoker1< Exception_t1967233988 * >::Invoke(1 /* System.Void UniRx.IObserver`1<System.Boolean>::OnError(System.Exception) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), (Il2CppObject*)L_7, (Exception_t1967233988 *)L_8);
		IL2CPP_LEAVE(0x7D, FINALLY_0076);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0076;
	}

FINALLY_0076:
	{ // begin finally (depth: 1)
		NullCheck((OperatorObserverBase_2_t60103313 *)__this);
		VirtActionInvoker0::Invoke(4 /* System.Void UniRx.Operators.OperatorObserverBase`2<System.Boolean,System.Boolean>::Dispose() */, (OperatorObserverBase_2_t60103313 *)__this);
		IL2CPP_END_FINALLY(118)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(118)
	{
		IL2CPP_JUMP_TBL(0x7D, IL_007d)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_007d:
	{
		goto IL_009b;
	}

IL_0082:
	try
	{ // begin try (depth: 1)
		Il2CppObject* L_9 = (Il2CppObject*)((OperatorObserverBase_2_t60103313 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		NullCheck((Il2CppObject*)L_9);
		InterfaceActionInvoker0::Invoke(0 /* System.Void UniRx.IObserver`1<System.Boolean>::OnCompleted() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), (Il2CppObject*)L_9);
		IL2CPP_LEAVE(0x9B, FINALLY_0094);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0094;
	}

FINALLY_0094:
	{ // begin finally (depth: 1)
		NullCheck((OperatorObserverBase_2_t60103313 *)__this);
		VirtActionInvoker0::Invoke(4 /* System.Void UniRx.Operators.OperatorObserverBase`2<System.Boolean,System.Boolean>::Dispose() */, (OperatorObserverBase_2_t60103313 *)__this);
		IL2CPP_END_FINALLY(148)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(148)
	{
		IL2CPP_JUMP_TBL(0x9B, IL_009b)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_009b:
	{
		return;
	}
}
// System.Void UniRx.Operators.FirstObservable`1/First<System.Int64>::.ctor(UniRx.Operators.FirstObservable`1<T>,UniRx.IObserver`1<T>,System.IDisposable)
extern "C"  void First__ctor_m2704542496_gshared (First_t930318071 * __this, FirstObservable_1_t575002000 * ___parent0, Il2CppObject* ___observer1, Il2CppObject * ___cancel2, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = ___observer1;
		Il2CppObject * L_1 = ___cancel2;
		NullCheck((OperatorObserverBase_2_t3939730909 *)__this);
		((  void (*) (OperatorObserverBase_2_t3939730909 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((OperatorObserverBase_2_t3939730909 *)__this, (Il2CppObject*)L_0, (Il2CppObject *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		FirstObservable_1_t575002000 * L_2 = ___parent0;
		__this->set_parent_2(L_2);
		__this->set_notPublished_3((bool)1);
		return;
	}
}
// System.Void UniRx.Operators.FirstObservable`1/First<System.Int64>::OnNext(T)
extern "C"  void First_OnNext_m810768445_gshared (First_t930318071 * __this, int64_t ___value0, const MethodInfo* method)
{
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		bool L_0 = (bool)__this->get_notPublished_3();
		if (!L_0)
		{
			goto IL_003a;
		}
	}
	{
		__this->set_notPublished_3((bool)0);
		Il2CppObject* L_1 = (Il2CppObject*)((OperatorObserverBase_2_t3939730909 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		int64_t L_2 = ___value0;
		NullCheck((Il2CppObject*)L_1);
		InterfaceActionInvoker1< int64_t >::Invoke(2 /* System.Void UniRx.IObserver`1<System.Int64>::OnNext(T) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), (Il2CppObject*)L_1, (int64_t)L_2);
	}

IL_0020:
	try
	{ // begin try (depth: 1)
		Il2CppObject* L_3 = (Il2CppObject*)((OperatorObserverBase_2_t3939730909 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		NullCheck((Il2CppObject*)L_3);
		InterfaceActionInvoker0::Invoke(0 /* System.Void UniRx.IObserver`1<System.Int64>::OnCompleted() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), (Il2CppObject*)L_3);
		IL2CPP_LEAVE(0x39, FINALLY_0032);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0032;
	}

FINALLY_0032:
	{ // begin finally (depth: 1)
		NullCheck((OperatorObserverBase_2_t3939730909 *)__this);
		VirtActionInvoker0::Invoke(4 /* System.Void UniRx.Operators.OperatorObserverBase`2<System.Int64,System.Int64>::Dispose() */, (OperatorObserverBase_2_t3939730909 *)__this);
		IL2CPP_END_FINALLY(50)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(50)
	{
		IL2CPP_JUMP_TBL(0x39, IL_0039)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0039:
	{
		return;
	}

IL_003a:
	{
		return;
	}
}
// System.Void UniRx.Operators.FirstObservable`1/First<System.Int64>::OnError(System.Exception)
extern "C"  void First_OnError_m2992724300_gshared (First_t930318071 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method)
{
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		Il2CppObject* L_0 = (Il2CppObject*)((OperatorObserverBase_2_t3939730909 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		Exception_t1967233988 * L_1 = ___error0;
		NullCheck((Il2CppObject*)L_0);
		InterfaceActionInvoker1< Exception_t1967233988 * >::Invoke(1 /* System.Void UniRx.IObserver`1<System.Int64>::OnError(System.Exception) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), (Il2CppObject*)L_0, (Exception_t1967233988 *)L_1);
		IL2CPP_LEAVE(0x1A, FINALLY_0013);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0013;
	}

FINALLY_0013:
	{ // begin finally (depth: 1)
		NullCheck((OperatorObserverBase_2_t3939730909 *)__this);
		VirtActionInvoker0::Invoke(4 /* System.Void UniRx.Operators.OperatorObserverBase`2<System.Int64,System.Int64>::Dispose() */, (OperatorObserverBase_2_t3939730909 *)__this);
		IL2CPP_END_FINALLY(19)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(19)
	{
		IL2CPP_JUMP_TBL(0x1A, IL_001a)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_001a:
	{
		return;
	}
}
// System.Void UniRx.Operators.FirstObservable`1/First<System.Int64>::OnCompleted()
extern Il2CppClass* Int64_t2847414882_il2cpp_TypeInfo_var;
extern Il2CppClass* InvalidOperationException_t2420574324_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2687971414;
extern const uint32_t First_OnCompleted_m4283604639_MetadataUsageId;
extern "C"  void First_OnCompleted_m4283604639_gshared (First_t930318071 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (First_OnCompleted_m4283604639_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int64_t V_0 = 0;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		FirstObservable_1_t575002000 * L_0 = (FirstObservable_1_t575002000 *)__this->get_parent_2();
		NullCheck(L_0);
		bool L_1 = (bool)L_0->get_useDefault_2();
		if (!L_1)
		{
			goto IL_004f;
		}
	}
	{
		bool L_2 = (bool)__this->get_notPublished_3();
		if (!L_2)
		{
			goto IL_0031;
		}
	}
	{
		Il2CppObject* L_3 = (Il2CppObject*)((OperatorObserverBase_2_t3939730909 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		Initobj (Int64_t2847414882_il2cpp_TypeInfo_var, (&V_0));
		int64_t L_4 = V_0;
		NullCheck((Il2CppObject*)L_3);
		InterfaceActionInvoker1< int64_t >::Invoke(2 /* System.Void UniRx.IObserver`1<System.Int64>::OnNext(T) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), (Il2CppObject*)L_3, (int64_t)L_4);
	}

IL_0031:
	try
	{ // begin try (depth: 1)
		Il2CppObject* L_5 = (Il2CppObject*)((OperatorObserverBase_2_t3939730909 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		NullCheck((Il2CppObject*)L_5);
		InterfaceActionInvoker0::Invoke(0 /* System.Void UniRx.IObserver`1<System.Int64>::OnCompleted() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), (Il2CppObject*)L_5);
		IL2CPP_LEAVE(0x4A, FINALLY_0043);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0043;
	}

FINALLY_0043:
	{ // begin finally (depth: 1)
		NullCheck((OperatorObserverBase_2_t3939730909 *)__this);
		VirtActionInvoker0::Invoke(4 /* System.Void UniRx.Operators.OperatorObserverBase`2<System.Int64,System.Int64>::Dispose() */, (OperatorObserverBase_2_t3939730909 *)__this);
		IL2CPP_END_FINALLY(67)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(67)
	{
		IL2CPP_JUMP_TBL(0x4A, IL_004a)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_004a:
	{
		goto IL_009b;
	}

IL_004f:
	{
		bool L_6 = (bool)__this->get_notPublished_3();
		if (!L_6)
		{
			goto IL_0082;
		}
	}

IL_005a:
	try
	{ // begin try (depth: 1)
		Il2CppObject* L_7 = (Il2CppObject*)((OperatorObserverBase_2_t3939730909 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		InvalidOperationException_t2420574324 * L_8 = (InvalidOperationException_t2420574324 *)il2cpp_codegen_object_new(InvalidOperationException_t2420574324_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_8, (String_t*)_stringLiteral2687971414, /*hidden argument*/NULL);
		NullCheck((Il2CppObject*)L_7);
		InterfaceActionInvoker1< Exception_t1967233988 * >::Invoke(1 /* System.Void UniRx.IObserver`1<System.Int64>::OnError(System.Exception) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), (Il2CppObject*)L_7, (Exception_t1967233988 *)L_8);
		IL2CPP_LEAVE(0x7D, FINALLY_0076);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0076;
	}

FINALLY_0076:
	{ // begin finally (depth: 1)
		NullCheck((OperatorObserverBase_2_t3939730909 *)__this);
		VirtActionInvoker0::Invoke(4 /* System.Void UniRx.Operators.OperatorObserverBase`2<System.Int64,System.Int64>::Dispose() */, (OperatorObserverBase_2_t3939730909 *)__this);
		IL2CPP_END_FINALLY(118)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(118)
	{
		IL2CPP_JUMP_TBL(0x7D, IL_007d)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_007d:
	{
		goto IL_009b;
	}

IL_0082:
	try
	{ // begin try (depth: 1)
		Il2CppObject* L_9 = (Il2CppObject*)((OperatorObserverBase_2_t3939730909 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		NullCheck((Il2CppObject*)L_9);
		InterfaceActionInvoker0::Invoke(0 /* System.Void UniRx.IObserver`1<System.Int64>::OnCompleted() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), (Il2CppObject*)L_9);
		IL2CPP_LEAVE(0x9B, FINALLY_0094);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0094;
	}

FINALLY_0094:
	{ // begin finally (depth: 1)
		NullCheck((OperatorObserverBase_2_t3939730909 *)__this);
		VirtActionInvoker0::Invoke(4 /* System.Void UniRx.Operators.OperatorObserverBase`2<System.Int64,System.Int64>::Dispose() */, (OperatorObserverBase_2_t3939730909 *)__this);
		IL2CPP_END_FINALLY(148)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(148)
	{
		IL2CPP_JUMP_TBL(0x9B, IL_009b)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_009b:
	{
		return;
	}
}
// System.Void UniRx.Operators.FirstObservable`1/First<System.Object>::.ctor(UniRx.Operators.FirstObservable`1<T>,UniRx.IObserver`1<T>,System.IDisposable)
extern "C"  void First__ctor_m714786572_gshared (First_t3214976905 * __this, FirstObservable_1_t2859660834 * ___parent0, Il2CppObject* ___observer1, Il2CppObject * ___cancel2, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = ___observer1;
		Il2CppObject * L_1 = ___cancel2;
		NullCheck((OperatorObserverBase_2_t1187768149 *)__this);
		((  void (*) (OperatorObserverBase_2_t1187768149 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((OperatorObserverBase_2_t1187768149 *)__this, (Il2CppObject*)L_0, (Il2CppObject *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		FirstObservable_1_t2859660834 * L_2 = ___parent0;
		__this->set_parent_2(L_2);
		__this->set_notPublished_3((bool)1);
		return;
	}
}
// System.Void UniRx.Operators.FirstObservable`1/First<System.Object>::OnNext(T)
extern "C"  void First_OnNext_m1616456489_gshared (First_t3214976905 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		bool L_0 = (bool)__this->get_notPublished_3();
		if (!L_0)
		{
			goto IL_003a;
		}
	}
	{
		__this->set_notPublished_3((bool)0);
		Il2CppObject* L_1 = (Il2CppObject*)((OperatorObserverBase_2_t1187768149 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		Il2CppObject * L_2 = ___value0;
		NullCheck((Il2CppObject*)L_1);
		InterfaceActionInvoker1< Il2CppObject * >::Invoke(2 /* System.Void UniRx.IObserver`1<System.Object>::OnNext(T) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), (Il2CppObject*)L_1, (Il2CppObject *)L_2);
	}

IL_0020:
	try
	{ // begin try (depth: 1)
		Il2CppObject* L_3 = (Il2CppObject*)((OperatorObserverBase_2_t1187768149 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		NullCheck((Il2CppObject*)L_3);
		InterfaceActionInvoker0::Invoke(0 /* System.Void UniRx.IObserver`1<System.Object>::OnCompleted() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), (Il2CppObject*)L_3);
		IL2CPP_LEAVE(0x39, FINALLY_0032);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0032;
	}

FINALLY_0032:
	{ // begin finally (depth: 1)
		NullCheck((OperatorObserverBase_2_t1187768149 *)__this);
		VirtActionInvoker0::Invoke(4 /* System.Void UniRx.Operators.OperatorObserverBase`2<System.Object,System.Object>::Dispose() */, (OperatorObserverBase_2_t1187768149 *)__this);
		IL2CPP_END_FINALLY(50)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(50)
	{
		IL2CPP_JUMP_TBL(0x39, IL_0039)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0039:
	{
		return;
	}

IL_003a:
	{
		return;
	}
}
// System.Void UniRx.Operators.FirstObservable`1/First<System.Object>::OnError(System.Exception)
extern "C"  void First_OnError_m3978023992_gshared (First_t3214976905 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method)
{
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		Il2CppObject* L_0 = (Il2CppObject*)((OperatorObserverBase_2_t1187768149 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		Exception_t1967233988 * L_1 = ___error0;
		NullCheck((Il2CppObject*)L_0);
		InterfaceActionInvoker1< Exception_t1967233988 * >::Invoke(1 /* System.Void UniRx.IObserver`1<System.Object>::OnError(System.Exception) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), (Il2CppObject*)L_0, (Exception_t1967233988 *)L_1);
		IL2CPP_LEAVE(0x1A, FINALLY_0013);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0013;
	}

FINALLY_0013:
	{ // begin finally (depth: 1)
		NullCheck((OperatorObserverBase_2_t1187768149 *)__this);
		VirtActionInvoker0::Invoke(4 /* System.Void UniRx.Operators.OperatorObserverBase`2<System.Object,System.Object>::Dispose() */, (OperatorObserverBase_2_t1187768149 *)__this);
		IL2CPP_END_FINALLY(19)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(19)
	{
		IL2CPP_JUMP_TBL(0x1A, IL_001a)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_001a:
	{
		return;
	}
}
// System.Void UniRx.Operators.FirstObservable`1/First<System.Object>::OnCompleted()
extern Il2CppClass* Il2CppObject_il2cpp_TypeInfo_var;
extern Il2CppClass* InvalidOperationException_t2420574324_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2687971414;
extern const uint32_t First_OnCompleted_m1092426635_MetadataUsageId;
extern "C"  void First_OnCompleted_m1092426635_gshared (First_t3214976905 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (First_OnCompleted_m1092426635_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Il2CppObject * V_0 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		FirstObservable_1_t2859660834 * L_0 = (FirstObservable_1_t2859660834 *)__this->get_parent_2();
		NullCheck(L_0);
		bool L_1 = (bool)L_0->get_useDefault_2();
		if (!L_1)
		{
			goto IL_004f;
		}
	}
	{
		bool L_2 = (bool)__this->get_notPublished_3();
		if (!L_2)
		{
			goto IL_0031;
		}
	}
	{
		Il2CppObject* L_3 = (Il2CppObject*)((OperatorObserverBase_2_t1187768149 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		Initobj (Il2CppObject_il2cpp_TypeInfo_var, (&V_0));
		Il2CppObject * L_4 = V_0;
		NullCheck((Il2CppObject*)L_3);
		InterfaceActionInvoker1< Il2CppObject * >::Invoke(2 /* System.Void UniRx.IObserver`1<System.Object>::OnNext(T) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), (Il2CppObject*)L_3, (Il2CppObject *)L_4);
	}

IL_0031:
	try
	{ // begin try (depth: 1)
		Il2CppObject* L_5 = (Il2CppObject*)((OperatorObserverBase_2_t1187768149 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		NullCheck((Il2CppObject*)L_5);
		InterfaceActionInvoker0::Invoke(0 /* System.Void UniRx.IObserver`1<System.Object>::OnCompleted() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), (Il2CppObject*)L_5);
		IL2CPP_LEAVE(0x4A, FINALLY_0043);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0043;
	}

FINALLY_0043:
	{ // begin finally (depth: 1)
		NullCheck((OperatorObserverBase_2_t1187768149 *)__this);
		VirtActionInvoker0::Invoke(4 /* System.Void UniRx.Operators.OperatorObserverBase`2<System.Object,System.Object>::Dispose() */, (OperatorObserverBase_2_t1187768149 *)__this);
		IL2CPP_END_FINALLY(67)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(67)
	{
		IL2CPP_JUMP_TBL(0x4A, IL_004a)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_004a:
	{
		goto IL_009b;
	}

IL_004f:
	{
		bool L_6 = (bool)__this->get_notPublished_3();
		if (!L_6)
		{
			goto IL_0082;
		}
	}

IL_005a:
	try
	{ // begin try (depth: 1)
		Il2CppObject* L_7 = (Il2CppObject*)((OperatorObserverBase_2_t1187768149 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		InvalidOperationException_t2420574324 * L_8 = (InvalidOperationException_t2420574324 *)il2cpp_codegen_object_new(InvalidOperationException_t2420574324_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_8, (String_t*)_stringLiteral2687971414, /*hidden argument*/NULL);
		NullCheck((Il2CppObject*)L_7);
		InterfaceActionInvoker1< Exception_t1967233988 * >::Invoke(1 /* System.Void UniRx.IObserver`1<System.Object>::OnError(System.Exception) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), (Il2CppObject*)L_7, (Exception_t1967233988 *)L_8);
		IL2CPP_LEAVE(0x7D, FINALLY_0076);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0076;
	}

FINALLY_0076:
	{ // begin finally (depth: 1)
		NullCheck((OperatorObserverBase_2_t1187768149 *)__this);
		VirtActionInvoker0::Invoke(4 /* System.Void UniRx.Operators.OperatorObserverBase`2<System.Object,System.Object>::Dispose() */, (OperatorObserverBase_2_t1187768149 *)__this);
		IL2CPP_END_FINALLY(118)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(118)
	{
		IL2CPP_JUMP_TBL(0x7D, IL_007d)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_007d:
	{
		goto IL_009b;
	}

IL_0082:
	try
	{ // begin try (depth: 1)
		Il2CppObject* L_9 = (Il2CppObject*)((OperatorObserverBase_2_t1187768149 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		NullCheck((Il2CppObject*)L_9);
		InterfaceActionInvoker0::Invoke(0 /* System.Void UniRx.IObserver`1<System.Object>::OnCompleted() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), (Il2CppObject*)L_9);
		IL2CPP_LEAVE(0x9B, FINALLY_0094);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0094;
	}

FINALLY_0094:
	{ // begin finally (depth: 1)
		NullCheck((OperatorObserverBase_2_t1187768149 *)__this);
		VirtActionInvoker0::Invoke(4 /* System.Void UniRx.Operators.OperatorObserverBase`2<System.Object,System.Object>::Dispose() */, (OperatorObserverBase_2_t1187768149 *)__this);
		IL2CPP_END_FINALLY(148)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(148)
	{
		IL2CPP_JUMP_TBL(0x9B, IL_009b)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_009b:
	{
		return;
	}
}
// System.Void UniRx.Operators.FirstObservable`1/First<UniRx.Tuple`2<System.Object,System.Object>>::.ctor(UniRx.Operators.FirstObservable`1<T>,UniRx.IObserver`1<T>,System.IDisposable)
extern "C"  void First__ctor_m243071002_gshared (First_t2747132304 * __this, FirstObservable_1_t2391816233 * ___parent0, Il2CppObject* ___observer1, Il2CppObject * ___cancel2, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = ___observer1;
		Il2CppObject * L_1 = ___cancel2;
		NullCheck((OperatorObserverBase_2_t311366489 *)__this);
		((  void (*) (OperatorObserverBase_2_t311366489 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((OperatorObserverBase_2_t311366489 *)__this, (Il2CppObject*)L_0, (Il2CppObject *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		FirstObservable_1_t2391816233 * L_2 = ___parent0;
		__this->set_parent_2(L_2);
		__this->set_notPublished_3((bool)1);
		return;
	}
}
// System.Void UniRx.Operators.FirstObservable`1/First<UniRx.Tuple`2<System.Object,System.Object>>::OnNext(T)
extern "C"  void First_OnNext_m1130374583_gshared (First_t2747132304 * __this, Tuple_2_t369261819  ___value0, const MethodInfo* method)
{
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		bool L_0 = (bool)__this->get_notPublished_3();
		if (!L_0)
		{
			goto IL_003a;
		}
	}
	{
		__this->set_notPublished_3((bool)0);
		Il2CppObject* L_1 = (Il2CppObject*)((OperatorObserverBase_2_t311366489 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		Tuple_2_t369261819  L_2 = ___value0;
		NullCheck((Il2CppObject*)L_1);
		InterfaceActionInvoker1< Tuple_2_t369261819  >::Invoke(2 /* System.Void UniRx.IObserver`1<UniRx.Tuple`2<System.Object,System.Object>>::OnNext(T) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), (Il2CppObject*)L_1, (Tuple_2_t369261819 )L_2);
	}

IL_0020:
	try
	{ // begin try (depth: 1)
		Il2CppObject* L_3 = (Il2CppObject*)((OperatorObserverBase_2_t311366489 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		NullCheck((Il2CppObject*)L_3);
		InterfaceActionInvoker0::Invoke(0 /* System.Void UniRx.IObserver`1<UniRx.Tuple`2<System.Object,System.Object>>::OnCompleted() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), (Il2CppObject*)L_3);
		IL2CPP_LEAVE(0x39, FINALLY_0032);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0032;
	}

FINALLY_0032:
	{ // begin finally (depth: 1)
		NullCheck((OperatorObserverBase_2_t311366489 *)__this);
		VirtActionInvoker0::Invoke(4 /* System.Void UniRx.Operators.OperatorObserverBase`2<UniRx.Tuple`2<System.Object,System.Object>,UniRx.Tuple`2<System.Object,System.Object>>::Dispose() */, (OperatorObserverBase_2_t311366489 *)__this);
		IL2CPP_END_FINALLY(50)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(50)
	{
		IL2CPP_JUMP_TBL(0x39, IL_0039)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0039:
	{
		return;
	}

IL_003a:
	{
		return;
	}
}
// System.Void UniRx.Operators.FirstObservable`1/First<UniRx.Tuple`2<System.Object,System.Object>>::OnError(System.Exception)
extern "C"  void First_OnError_m2770858694_gshared (First_t2747132304 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method)
{
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		Il2CppObject* L_0 = (Il2CppObject*)((OperatorObserverBase_2_t311366489 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		Exception_t1967233988 * L_1 = ___error0;
		NullCheck((Il2CppObject*)L_0);
		InterfaceActionInvoker1< Exception_t1967233988 * >::Invoke(1 /* System.Void UniRx.IObserver`1<UniRx.Tuple`2<System.Object,System.Object>>::OnError(System.Exception) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), (Il2CppObject*)L_0, (Exception_t1967233988 *)L_1);
		IL2CPP_LEAVE(0x1A, FINALLY_0013);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0013;
	}

FINALLY_0013:
	{ // begin finally (depth: 1)
		NullCheck((OperatorObserverBase_2_t311366489 *)__this);
		VirtActionInvoker0::Invoke(4 /* System.Void UniRx.Operators.OperatorObserverBase`2<UniRx.Tuple`2<System.Object,System.Object>,UniRx.Tuple`2<System.Object,System.Object>>::Dispose() */, (OperatorObserverBase_2_t311366489 *)__this);
		IL2CPP_END_FINALLY(19)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(19)
	{
		IL2CPP_JUMP_TBL(0x1A, IL_001a)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_001a:
	{
		return;
	}
}
// System.Void UniRx.Operators.FirstObservable`1/First<UniRx.Tuple`2<System.Object,System.Object>>::OnCompleted()
extern Il2CppClass* Tuple_2_t369261819_il2cpp_TypeInfo_var;
extern Il2CppClass* InvalidOperationException_t2420574324_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2687971414;
extern const uint32_t First_OnCompleted_m4226293529_MetadataUsageId;
extern "C"  void First_OnCompleted_m4226293529_gshared (First_t2747132304 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (First_OnCompleted_m4226293529_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Tuple_2_t369261819  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		FirstObservable_1_t2391816233 * L_0 = (FirstObservable_1_t2391816233 *)__this->get_parent_2();
		NullCheck(L_0);
		bool L_1 = (bool)L_0->get_useDefault_2();
		if (!L_1)
		{
			goto IL_004f;
		}
	}
	{
		bool L_2 = (bool)__this->get_notPublished_3();
		if (!L_2)
		{
			goto IL_0031;
		}
	}
	{
		Il2CppObject* L_3 = (Il2CppObject*)((OperatorObserverBase_2_t311366489 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		Initobj (Tuple_2_t369261819_il2cpp_TypeInfo_var, (&V_0));
		Tuple_2_t369261819  L_4 = V_0;
		NullCheck((Il2CppObject*)L_3);
		InterfaceActionInvoker1< Tuple_2_t369261819  >::Invoke(2 /* System.Void UniRx.IObserver`1<UniRx.Tuple`2<System.Object,System.Object>>::OnNext(T) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), (Il2CppObject*)L_3, (Tuple_2_t369261819 )L_4);
	}

IL_0031:
	try
	{ // begin try (depth: 1)
		Il2CppObject* L_5 = (Il2CppObject*)((OperatorObserverBase_2_t311366489 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		NullCheck((Il2CppObject*)L_5);
		InterfaceActionInvoker0::Invoke(0 /* System.Void UniRx.IObserver`1<UniRx.Tuple`2<System.Object,System.Object>>::OnCompleted() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), (Il2CppObject*)L_5);
		IL2CPP_LEAVE(0x4A, FINALLY_0043);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0043;
	}

FINALLY_0043:
	{ // begin finally (depth: 1)
		NullCheck((OperatorObserverBase_2_t311366489 *)__this);
		VirtActionInvoker0::Invoke(4 /* System.Void UniRx.Operators.OperatorObserverBase`2<UniRx.Tuple`2<System.Object,System.Object>,UniRx.Tuple`2<System.Object,System.Object>>::Dispose() */, (OperatorObserverBase_2_t311366489 *)__this);
		IL2CPP_END_FINALLY(67)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(67)
	{
		IL2CPP_JUMP_TBL(0x4A, IL_004a)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_004a:
	{
		goto IL_009b;
	}

IL_004f:
	{
		bool L_6 = (bool)__this->get_notPublished_3();
		if (!L_6)
		{
			goto IL_0082;
		}
	}

IL_005a:
	try
	{ // begin try (depth: 1)
		Il2CppObject* L_7 = (Il2CppObject*)((OperatorObserverBase_2_t311366489 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		InvalidOperationException_t2420574324 * L_8 = (InvalidOperationException_t2420574324 *)il2cpp_codegen_object_new(InvalidOperationException_t2420574324_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_8, (String_t*)_stringLiteral2687971414, /*hidden argument*/NULL);
		NullCheck((Il2CppObject*)L_7);
		InterfaceActionInvoker1< Exception_t1967233988 * >::Invoke(1 /* System.Void UniRx.IObserver`1<UniRx.Tuple`2<System.Object,System.Object>>::OnError(System.Exception) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), (Il2CppObject*)L_7, (Exception_t1967233988 *)L_8);
		IL2CPP_LEAVE(0x7D, FINALLY_0076);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0076;
	}

FINALLY_0076:
	{ // begin finally (depth: 1)
		NullCheck((OperatorObserverBase_2_t311366489 *)__this);
		VirtActionInvoker0::Invoke(4 /* System.Void UniRx.Operators.OperatorObserverBase`2<UniRx.Tuple`2<System.Object,System.Object>,UniRx.Tuple`2<System.Object,System.Object>>::Dispose() */, (OperatorObserverBase_2_t311366489 *)__this);
		IL2CPP_END_FINALLY(118)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(118)
	{
		IL2CPP_JUMP_TBL(0x7D, IL_007d)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_007d:
	{
		goto IL_009b;
	}

IL_0082:
	try
	{ // begin try (depth: 1)
		Il2CppObject* L_9 = (Il2CppObject*)((OperatorObserverBase_2_t311366489 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		NullCheck((Il2CppObject*)L_9);
		InterfaceActionInvoker0::Invoke(0 /* System.Void UniRx.IObserver`1<UniRx.Tuple`2<System.Object,System.Object>>::OnCompleted() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), (Il2CppObject*)L_9);
		IL2CPP_LEAVE(0x9B, FINALLY_0094);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0094;
	}

FINALLY_0094:
	{ // begin finally (depth: 1)
		NullCheck((OperatorObserverBase_2_t311366489 *)__this);
		VirtActionInvoker0::Invoke(4 /* System.Void UniRx.Operators.OperatorObserverBase`2<UniRx.Tuple`2<System.Object,System.Object>,UniRx.Tuple`2<System.Object,System.Object>>::Dispose() */, (OperatorObserverBase_2_t311366489 *)__this);
		IL2CPP_END_FINALLY(148)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(148)
	{
		IL2CPP_JUMP_TBL(0x9B, IL_009b)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_009b:
	{
		return;
	}
}
// System.Void UniRx.Operators.FirstObservable`1/First<UniRx.Unit>::.ctor(UniRx.Operators.FirstObservable`1<T>,UniRx.IObserver`1<T>,System.IDisposable)
extern "C"  void First__ctor_m2298124302_gshared (First_t641189227 * __this, FirstObservable_1_t285873156 * ___parent0, Il2CppObject* ___observer1, Il2CppObject * ___cancel2, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = ___observer1;
		Il2CppObject * L_1 = ___cancel2;
		NullCheck((OperatorObserverBase_2_t4165376397 *)__this);
		((  void (*) (OperatorObserverBase_2_t4165376397 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((OperatorObserverBase_2_t4165376397 *)__this, (Il2CppObject*)L_0, (Il2CppObject *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		FirstObservable_1_t285873156 * L_2 = ___parent0;
		__this->set_parent_2(L_2);
		__this->set_notPublished_3((bool)1);
		return;
	}
}
// System.Void UniRx.Operators.FirstObservable`1/First<UniRx.Unit>::OnNext(T)
extern "C"  void First_OnNext_m3212117675_gshared (First_t641189227 * __this, Unit_t2558286038  ___value0, const MethodInfo* method)
{
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		bool L_0 = (bool)__this->get_notPublished_3();
		if (!L_0)
		{
			goto IL_003a;
		}
	}
	{
		__this->set_notPublished_3((bool)0);
		Il2CppObject* L_1 = (Il2CppObject*)((OperatorObserverBase_2_t4165376397 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		Unit_t2558286038  L_2 = ___value0;
		NullCheck((Il2CppObject*)L_1);
		InterfaceActionInvoker1< Unit_t2558286038  >::Invoke(2 /* System.Void UniRx.IObserver`1<UniRx.Unit>::OnNext(T) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), (Il2CppObject*)L_1, (Unit_t2558286038 )L_2);
	}

IL_0020:
	try
	{ // begin try (depth: 1)
		Il2CppObject* L_3 = (Il2CppObject*)((OperatorObserverBase_2_t4165376397 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		NullCheck((Il2CppObject*)L_3);
		InterfaceActionInvoker0::Invoke(0 /* System.Void UniRx.IObserver`1<UniRx.Unit>::OnCompleted() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), (Il2CppObject*)L_3);
		IL2CPP_LEAVE(0x39, FINALLY_0032);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0032;
	}

FINALLY_0032:
	{ // begin finally (depth: 1)
		NullCheck((OperatorObserverBase_2_t4165376397 *)__this);
		VirtActionInvoker0::Invoke(4 /* System.Void UniRx.Operators.OperatorObserverBase`2<UniRx.Unit,UniRx.Unit>::Dispose() */, (OperatorObserverBase_2_t4165376397 *)__this);
		IL2CPP_END_FINALLY(50)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(50)
	{
		IL2CPP_JUMP_TBL(0x39, IL_0039)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0039:
	{
		return;
	}

IL_003a:
	{
		return;
	}
}
// System.Void UniRx.Operators.FirstObservable`1/First<UniRx.Unit>::OnError(System.Exception)
extern "C"  void First_OnError_m4251216826_gshared (First_t641189227 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method)
{
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		Il2CppObject* L_0 = (Il2CppObject*)((OperatorObserverBase_2_t4165376397 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		Exception_t1967233988 * L_1 = ___error0;
		NullCheck((Il2CppObject*)L_0);
		InterfaceActionInvoker1< Exception_t1967233988 * >::Invoke(1 /* System.Void UniRx.IObserver`1<UniRx.Unit>::OnError(System.Exception) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), (Il2CppObject*)L_0, (Exception_t1967233988 *)L_1);
		IL2CPP_LEAVE(0x1A, FINALLY_0013);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0013;
	}

FINALLY_0013:
	{ // begin finally (depth: 1)
		NullCheck((OperatorObserverBase_2_t4165376397 *)__this);
		VirtActionInvoker0::Invoke(4 /* System.Void UniRx.Operators.OperatorObserverBase`2<UniRx.Unit,UniRx.Unit>::Dispose() */, (OperatorObserverBase_2_t4165376397 *)__this);
		IL2CPP_END_FINALLY(19)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(19)
	{
		IL2CPP_JUMP_TBL(0x1A, IL_001a)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_001a:
	{
		return;
	}
}
// System.Void UniRx.Operators.FirstObservable`1/First<UniRx.Unit>::OnCompleted()
extern Il2CppClass* Unit_t2558286038_il2cpp_TypeInfo_var;
extern Il2CppClass* InvalidOperationException_t2420574324_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2687971414;
extern const uint32_t First_OnCompleted_m2952488461_MetadataUsageId;
extern "C"  void First_OnCompleted_m2952488461_gshared (First_t641189227 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (First_OnCompleted_m2952488461_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Unit_t2558286038  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		FirstObservable_1_t285873156 * L_0 = (FirstObservable_1_t285873156 *)__this->get_parent_2();
		NullCheck(L_0);
		bool L_1 = (bool)L_0->get_useDefault_2();
		if (!L_1)
		{
			goto IL_004f;
		}
	}
	{
		bool L_2 = (bool)__this->get_notPublished_3();
		if (!L_2)
		{
			goto IL_0031;
		}
	}
	{
		Il2CppObject* L_3 = (Il2CppObject*)((OperatorObserverBase_2_t4165376397 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		Initobj (Unit_t2558286038_il2cpp_TypeInfo_var, (&V_0));
		Unit_t2558286038  L_4 = V_0;
		NullCheck((Il2CppObject*)L_3);
		InterfaceActionInvoker1< Unit_t2558286038  >::Invoke(2 /* System.Void UniRx.IObserver`1<UniRx.Unit>::OnNext(T) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), (Il2CppObject*)L_3, (Unit_t2558286038 )L_4);
	}

IL_0031:
	try
	{ // begin try (depth: 1)
		Il2CppObject* L_5 = (Il2CppObject*)((OperatorObserverBase_2_t4165376397 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		NullCheck((Il2CppObject*)L_5);
		InterfaceActionInvoker0::Invoke(0 /* System.Void UniRx.IObserver`1<UniRx.Unit>::OnCompleted() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), (Il2CppObject*)L_5);
		IL2CPP_LEAVE(0x4A, FINALLY_0043);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0043;
	}

FINALLY_0043:
	{ // begin finally (depth: 1)
		NullCheck((OperatorObserverBase_2_t4165376397 *)__this);
		VirtActionInvoker0::Invoke(4 /* System.Void UniRx.Operators.OperatorObserverBase`2<UniRx.Unit,UniRx.Unit>::Dispose() */, (OperatorObserverBase_2_t4165376397 *)__this);
		IL2CPP_END_FINALLY(67)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(67)
	{
		IL2CPP_JUMP_TBL(0x4A, IL_004a)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_004a:
	{
		goto IL_009b;
	}

IL_004f:
	{
		bool L_6 = (bool)__this->get_notPublished_3();
		if (!L_6)
		{
			goto IL_0082;
		}
	}

IL_005a:
	try
	{ // begin try (depth: 1)
		Il2CppObject* L_7 = (Il2CppObject*)((OperatorObserverBase_2_t4165376397 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		InvalidOperationException_t2420574324 * L_8 = (InvalidOperationException_t2420574324 *)il2cpp_codegen_object_new(InvalidOperationException_t2420574324_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_8, (String_t*)_stringLiteral2687971414, /*hidden argument*/NULL);
		NullCheck((Il2CppObject*)L_7);
		InterfaceActionInvoker1< Exception_t1967233988 * >::Invoke(1 /* System.Void UniRx.IObserver`1<UniRx.Unit>::OnError(System.Exception) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), (Il2CppObject*)L_7, (Exception_t1967233988 *)L_8);
		IL2CPP_LEAVE(0x7D, FINALLY_0076);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0076;
	}

FINALLY_0076:
	{ // begin finally (depth: 1)
		NullCheck((OperatorObserverBase_2_t4165376397 *)__this);
		VirtActionInvoker0::Invoke(4 /* System.Void UniRx.Operators.OperatorObserverBase`2<UniRx.Unit,UniRx.Unit>::Dispose() */, (OperatorObserverBase_2_t4165376397 *)__this);
		IL2CPP_END_FINALLY(118)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(118)
	{
		IL2CPP_JUMP_TBL(0x7D, IL_007d)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_007d:
	{
		goto IL_009b;
	}

IL_0082:
	try
	{ // begin try (depth: 1)
		Il2CppObject* L_9 = (Il2CppObject*)((OperatorObserverBase_2_t4165376397 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		NullCheck((Il2CppObject*)L_9);
		InterfaceActionInvoker0::Invoke(0 /* System.Void UniRx.IObserver`1<UniRx.Unit>::OnCompleted() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), (Il2CppObject*)L_9);
		IL2CPP_LEAVE(0x9B, FINALLY_0094);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0094;
	}

FINALLY_0094:
	{ // begin finally (depth: 1)
		NullCheck((OperatorObserverBase_2_t4165376397 *)__this);
		VirtActionInvoker0::Invoke(4 /* System.Void UniRx.Operators.OperatorObserverBase`2<UniRx.Unit,UniRx.Unit>::Dispose() */, (OperatorObserverBase_2_t4165376397 *)__this);
		IL2CPP_END_FINALLY(148)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(148)
	{
		IL2CPP_JUMP_TBL(0x9B, IL_009b)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_009b:
	{
		return;
	}
}
// System.Void UniRx.Operators.FirstObservable`1<System.Boolean>::.ctor(UniRx.IObservable`1<T>,System.Boolean)
extern "C"  void FirstObservable_1__ctor_m2155067117_gshared (FirstObservable_1_t2233559755 * __this, Il2CppObject* ___source0, bool ___useDefault1, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = ___source0;
		bool L_1 = ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)(NULL /*static, unused*/, (Il2CppObject*)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		NullCheck((OperatorObservableBase_1_t3570117608 *)__this);
		((  void (*) (OperatorObservableBase_1_t3570117608 *, bool, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)((OperatorObservableBase_1_t3570117608 *)__this, (bool)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		Il2CppObject* L_2 = ___source0;
		__this->set_source_1(L_2);
		bool L_3 = ___useDefault1;
		__this->set_useDefault_2(L_3);
		return;
	}
}
// System.Void UniRx.Operators.FirstObservable`1<System.Boolean>::.ctor(UniRx.IObservable`1<T>,System.Func`2<T,System.Boolean>,System.Boolean)
extern "C"  void FirstObservable_1__ctor_m2072790039_gshared (FirstObservable_1_t2233559755 * __this, Il2CppObject* ___source0, Func_2_t1008118516 * ___predicate1, bool ___useDefault2, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = ___source0;
		bool L_1 = ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)(NULL /*static, unused*/, (Il2CppObject*)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		NullCheck((OperatorObservableBase_1_t3570117608 *)__this);
		((  void (*) (OperatorObservableBase_1_t3570117608 *, bool, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)((OperatorObservableBase_1_t3570117608 *)__this, (bool)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		Il2CppObject* L_2 = ___source0;
		__this->set_source_1(L_2);
		Func_2_t1008118516 * L_3 = ___predicate1;
		__this->set_predicate_3(L_3);
		bool L_4 = ___useDefault2;
		__this->set_useDefault_2(L_4);
		return;
	}
}
// System.IDisposable UniRx.Operators.FirstObservable`1<System.Boolean>::SubscribeCore(UniRx.IObserver`1<T>,System.IDisposable)
extern "C"  Il2CppObject * FirstObservable_1_SubscribeCore_m2141808247_gshared (FirstObservable_1_t2233559755 * __this, Il2CppObject* ___observer0, Il2CppObject * ___cancel1, const MethodInfo* method)
{
	{
		Func_2_t1008118516 * L_0 = (Func_2_t1008118516 *)__this->get_predicate_3();
		if (L_0)
		{
			goto IL_001f;
		}
	}
	{
		Il2CppObject* L_1 = (Il2CppObject*)__this->get_source_1();
		Il2CppObject* L_2 = ___observer0;
		Il2CppObject * L_3 = ___cancel1;
		First_t2588875826 * L_4 = (First_t2588875826 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		((  void (*) (First_t2588875826 *, FirstObservable_1_t2233559755 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)(L_4, (FirstObservable_1_t2233559755 *)__this, (Il2CppObject*)L_2, (Il2CppObject *)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		NullCheck((Il2CppObject*)L_1);
		Il2CppObject * L_5 = InterfaceFuncInvoker1< Il2CppObject *, Il2CppObject* >::Invoke(0 /* System.IDisposable UniRx.IObservable`1<System.Boolean>::Subscribe(UniRx.IObserver`1<T>) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5), (Il2CppObject*)L_1, (Il2CppObject*)L_4);
		return L_5;
	}

IL_001f:
	{
		Il2CppObject* L_6 = (Il2CppObject*)__this->get_source_1();
		Il2CppObject* L_7 = ___observer0;
		Il2CppObject * L_8 = ___cancel1;
		First__t3433238553 * L_9 = (First__t3433238553 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		((  void (*) (First__t3433238553 *, FirstObservable_1_t2233559755 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7)->method)(L_9, (FirstObservable_1_t2233559755 *)__this, (Il2CppObject*)L_7, (Il2CppObject *)L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7));
		NullCheck((Il2CppObject*)L_6);
		Il2CppObject * L_10 = InterfaceFuncInvoker1< Il2CppObject *, Il2CppObject* >::Invoke(0 /* System.IDisposable UniRx.IObservable`1<System.Boolean>::Subscribe(UniRx.IObserver`1<T>) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5), (Il2CppObject*)L_6, (Il2CppObject*)L_9);
		return L_10;
	}
}
// System.Void UniRx.Operators.FirstObservable`1<System.Int64>::.ctor(UniRx.IObservable`1<T>,System.Boolean)
extern "C"  void FirstObservable_1__ctor_m1910486152_gshared (FirstObservable_1_t575002000 * __this, Il2CppObject* ___source0, bool ___useDefault1, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = ___source0;
		bool L_1 = ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)(NULL /*static, unused*/, (Il2CppObject*)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		NullCheck((OperatorObservableBase_1_t1911559853 *)__this);
		((  void (*) (OperatorObservableBase_1_t1911559853 *, bool, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)((OperatorObservableBase_1_t1911559853 *)__this, (bool)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		Il2CppObject* L_2 = ___source0;
		__this->set_source_1(L_2);
		bool L_3 = ___useDefault1;
		__this->set_useDefault_2(L_3);
		return;
	}
}
// System.Void UniRx.Operators.FirstObservable`1<System.Int64>::.ctor(UniRx.IObservable`1<T>,System.Func`2<T,System.Boolean>,System.Boolean)
extern "C"  void FirstObservable_1__ctor_m1734546866_gshared (FirstObservable_1_t575002000 * __this, Il2CppObject* ___source0, Func_2_t2251336571 * ___predicate1, bool ___useDefault2, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = ___source0;
		bool L_1 = ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)(NULL /*static, unused*/, (Il2CppObject*)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		NullCheck((OperatorObservableBase_1_t1911559853 *)__this);
		((  void (*) (OperatorObservableBase_1_t1911559853 *, bool, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)((OperatorObservableBase_1_t1911559853 *)__this, (bool)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		Il2CppObject* L_2 = ___source0;
		__this->set_source_1(L_2);
		Func_2_t2251336571 * L_3 = ___predicate1;
		__this->set_predicate_3(L_3);
		bool L_4 = ___useDefault2;
		__this->set_useDefault_2(L_4);
		return;
	}
}
// System.IDisposable UniRx.Operators.FirstObservable`1<System.Int64>::SubscribeCore(UniRx.IObserver`1<T>,System.IDisposable)
extern "C"  Il2CppObject * FirstObservable_1_SubscribeCore_m913433490_gshared (FirstObservable_1_t575002000 * __this, Il2CppObject* ___observer0, Il2CppObject * ___cancel1, const MethodInfo* method)
{
	{
		Func_2_t2251336571 * L_0 = (Func_2_t2251336571 *)__this->get_predicate_3();
		if (L_0)
		{
			goto IL_001f;
		}
	}
	{
		Il2CppObject* L_1 = (Il2CppObject*)__this->get_source_1();
		Il2CppObject* L_2 = ___observer0;
		Il2CppObject * L_3 = ___cancel1;
		First_t930318071 * L_4 = (First_t930318071 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		((  void (*) (First_t930318071 *, FirstObservable_1_t575002000 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)(L_4, (FirstObservable_1_t575002000 *)__this, (Il2CppObject*)L_2, (Il2CppObject *)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		NullCheck((Il2CppObject*)L_1);
		Il2CppObject * L_5 = InterfaceFuncInvoker1< Il2CppObject *, Il2CppObject* >::Invoke(0 /* System.IDisposable UniRx.IObservable`1<System.Int64>::Subscribe(UniRx.IObserver`1<T>) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5), (Il2CppObject*)L_1, (Il2CppObject*)L_4);
		return L_5;
	}

IL_001f:
	{
		Il2CppObject* L_6 = (Il2CppObject*)__this->get_source_1();
		Il2CppObject* L_7 = ___observer0;
		Il2CppObject * L_8 = ___cancel1;
		First__t1774680798 * L_9 = (First__t1774680798 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		((  void (*) (First__t1774680798 *, FirstObservable_1_t575002000 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7)->method)(L_9, (FirstObservable_1_t575002000 *)__this, (Il2CppObject*)L_7, (Il2CppObject *)L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7));
		NullCheck((Il2CppObject*)L_6);
		Il2CppObject * L_10 = InterfaceFuncInvoker1< Il2CppObject *, Il2CppObject* >::Invoke(0 /* System.IDisposable UniRx.IObservable`1<System.Int64>::Subscribe(UniRx.IObserver`1<T>) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5), (Il2CppObject*)L_6, (Il2CppObject*)L_9);
		return L_10;
	}
}
// System.Void UniRx.Operators.FirstObservable`1<System.Object>::.ctor(UniRx.IObservable`1<T>,System.Boolean)
extern "C"  void FirstObservable_1__ctor_m1199967486_gshared (FirstObservable_1_t2859660834 * __this, Il2CppObject* ___source0, bool ___useDefault1, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = ___source0;
		bool L_1 = ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)(NULL /*static, unused*/, (Il2CppObject*)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		NullCheck((OperatorObservableBase_1_t4196218687 *)__this);
		((  void (*) (OperatorObservableBase_1_t4196218687 *, bool, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)((OperatorObservableBase_1_t4196218687 *)__this, (bool)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		Il2CppObject* L_2 = ___source0;
		__this->set_source_1(L_2);
		bool L_3 = ___useDefault1;
		__this->set_useDefault_2(L_3);
		return;
	}
}
// System.Void UniRx.Operators.FirstObservable`1<System.Object>::.ctor(UniRx.IObservable`1<T>,System.Func`2<T,System.Boolean>,System.Boolean)
extern "C"  void FirstObservable_1__ctor_m1327109672_gshared (FirstObservable_1_t2859660834 * __this, Il2CppObject* ___source0, Func_2_t1509682273 * ___predicate1, bool ___useDefault2, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = ___source0;
		bool L_1 = ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)(NULL /*static, unused*/, (Il2CppObject*)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		NullCheck((OperatorObservableBase_1_t4196218687 *)__this);
		((  void (*) (OperatorObservableBase_1_t4196218687 *, bool, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)((OperatorObservableBase_1_t4196218687 *)__this, (bool)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		Il2CppObject* L_2 = ___source0;
		__this->set_source_1(L_2);
		Func_2_t1509682273 * L_3 = ___predicate1;
		__this->set_predicate_3(L_3);
		bool L_4 = ___useDefault2;
		__this->set_useDefault_2(L_4);
		return;
	}
}
// System.IDisposable UniRx.Operators.FirstObservable`1<System.Object>::SubscribeCore(UniRx.IObserver`1<T>,System.IDisposable)
extern "C"  Il2CppObject * FirstObservable_1_SubscribeCore_m3528768606_gshared (FirstObservable_1_t2859660834 * __this, Il2CppObject* ___observer0, Il2CppObject * ___cancel1, const MethodInfo* method)
{
	{
		Func_2_t1509682273 * L_0 = (Func_2_t1509682273 *)__this->get_predicate_3();
		if (L_0)
		{
			goto IL_001f;
		}
	}
	{
		Il2CppObject* L_1 = (Il2CppObject*)__this->get_source_1();
		Il2CppObject* L_2 = ___observer0;
		Il2CppObject * L_3 = ___cancel1;
		First_t3214976905 * L_4 = (First_t3214976905 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		((  void (*) (First_t3214976905 *, FirstObservable_1_t2859660834 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)(L_4, (FirstObservable_1_t2859660834 *)__this, (Il2CppObject*)L_2, (Il2CppObject *)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		NullCheck((Il2CppObject*)L_1);
		Il2CppObject * L_5 = InterfaceFuncInvoker1< Il2CppObject *, Il2CppObject* >::Invoke(0 /* System.IDisposable UniRx.IObservable`1<System.Object>::Subscribe(UniRx.IObserver`1<T>) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5), (Il2CppObject*)L_1, (Il2CppObject*)L_4);
		return L_5;
	}

IL_001f:
	{
		Il2CppObject* L_6 = (Il2CppObject*)__this->get_source_1();
		Il2CppObject* L_7 = ___observer0;
		Il2CppObject * L_8 = ___cancel1;
		First__t4059339632 * L_9 = (First__t4059339632 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		((  void (*) (First__t4059339632 *, FirstObservable_1_t2859660834 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7)->method)(L_9, (FirstObservable_1_t2859660834 *)__this, (Il2CppObject*)L_7, (Il2CppObject *)L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7));
		NullCheck((Il2CppObject*)L_6);
		Il2CppObject * L_10 = InterfaceFuncInvoker1< Il2CppObject *, Il2CppObject* >::Invoke(0 /* System.IDisposable UniRx.IObservable`1<System.Object>::Subscribe(UniRx.IObserver`1<T>) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5), (Il2CppObject*)L_6, (Il2CppObject*)L_9);
		return L_10;
	}
}
// System.Void UniRx.Operators.FirstObservable`1<UniRx.Tuple`2<System.Object,System.Object>>::.ctor(UniRx.IObservable`1<T>,System.Boolean)
extern "C"  void FirstObservable_1__ctor_m890207374_gshared (FirstObservable_1_t2391816233 * __this, Il2CppObject* ___source0, bool ___useDefault1, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = ___source0;
		bool L_1 = ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)(NULL /*static, unused*/, (Il2CppObject*)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		NullCheck((OperatorObservableBase_1_t3728374086 *)__this);
		((  void (*) (OperatorObservableBase_1_t3728374086 *, bool, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)((OperatorObservableBase_1_t3728374086 *)__this, (bool)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		Il2CppObject* L_2 = ___source0;
		__this->set_source_1(L_2);
		bool L_3 = ___useDefault1;
		__this->set_useDefault_2(L_3);
		return;
	}
}
// System.Void UniRx.Operators.FirstObservable`1<UniRx.Tuple`2<System.Object,System.Object>>::.ctor(UniRx.IObservable`1<T>,System.Func`2<T,System.Boolean>,System.Boolean)
extern "C"  void FirstObservable_1__ctor_m746669496_gshared (FirstObservable_1_t2391816233 * __this, Il2CppObject* ___source0, Func_2_t1101125214 * ___predicate1, bool ___useDefault2, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = ___source0;
		bool L_1 = ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)(NULL /*static, unused*/, (Il2CppObject*)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		NullCheck((OperatorObservableBase_1_t3728374086 *)__this);
		((  void (*) (OperatorObservableBase_1_t3728374086 *, bool, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)((OperatorObservableBase_1_t3728374086 *)__this, (bool)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		Il2CppObject* L_2 = ___source0;
		__this->set_source_1(L_2);
		Func_2_t1101125214 * L_3 = ___predicate1;
		__this->set_predicate_3(L_3);
		bool L_4 = ___useDefault2;
		__this->set_useDefault_2(L_4);
		return;
	}
}
// System.IDisposable UniRx.Operators.FirstObservable`1<UniRx.Tuple`2<System.Object,System.Object>>::SubscribeCore(UniRx.IObserver`1<T>,System.IDisposable)
extern "C"  Il2CppObject * FirstObservable_1_SubscribeCore_m1264115032_gshared (FirstObservable_1_t2391816233 * __this, Il2CppObject* ___observer0, Il2CppObject * ___cancel1, const MethodInfo* method)
{
	{
		Func_2_t1101125214 * L_0 = (Func_2_t1101125214 *)__this->get_predicate_3();
		if (L_0)
		{
			goto IL_001f;
		}
	}
	{
		Il2CppObject* L_1 = (Il2CppObject*)__this->get_source_1();
		Il2CppObject* L_2 = ___observer0;
		Il2CppObject * L_3 = ___cancel1;
		First_t2747132304 * L_4 = (First_t2747132304 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		((  void (*) (First_t2747132304 *, FirstObservable_1_t2391816233 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)(L_4, (FirstObservable_1_t2391816233 *)__this, (Il2CppObject*)L_2, (Il2CppObject *)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		NullCheck((Il2CppObject*)L_1);
		Il2CppObject * L_5 = InterfaceFuncInvoker1< Il2CppObject *, Il2CppObject* >::Invoke(0 /* System.IDisposable UniRx.IObservable`1<UniRx.Tuple`2<System.Object,System.Object>>::Subscribe(UniRx.IObserver`1<T>) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5), (Il2CppObject*)L_1, (Il2CppObject*)L_4);
		return L_5;
	}

IL_001f:
	{
		Il2CppObject* L_6 = (Il2CppObject*)__this->get_source_1();
		Il2CppObject* L_7 = ___observer0;
		Il2CppObject * L_8 = ___cancel1;
		First__t3591495031 * L_9 = (First__t3591495031 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		((  void (*) (First__t3591495031 *, FirstObservable_1_t2391816233 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7)->method)(L_9, (FirstObservable_1_t2391816233 *)__this, (Il2CppObject*)L_7, (Il2CppObject *)L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7));
		NullCheck((Il2CppObject*)L_6);
		Il2CppObject * L_10 = InterfaceFuncInvoker1< Il2CppObject *, Il2CppObject* >::Invoke(0 /* System.IDisposable UniRx.IObservable`1<UniRx.Tuple`2<System.Object,System.Object>>::Subscribe(UniRx.IObserver`1<T>) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5), (Il2CppObject*)L_6, (Il2CppObject*)L_9);
		return L_10;
	}
}
// System.Void UniRx.Operators.FirstObservable`1<UniRx.Unit>::.ctor(UniRx.IObservable`1<T>,System.Boolean)
extern "C"  void FirstObservable_1__ctor_m629216026_gshared (FirstObservable_1_t285873156 * __this, Il2CppObject* ___source0, bool ___useDefault1, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = ___source0;
		bool L_1 = ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)(NULL /*static, unused*/, (Il2CppObject*)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		NullCheck((OperatorObservableBase_1_t1622431009 *)__this);
		((  void (*) (OperatorObservableBase_1_t1622431009 *, bool, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)((OperatorObservableBase_1_t1622431009 *)__this, (bool)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		Il2CppObject* L_2 = ___source0;
		__this->set_source_1(L_2);
		bool L_3 = ___useDefault1;
		__this->set_useDefault_2(L_3);
		return;
	}
}
// System.Void UniRx.Operators.FirstObservable`1<UniRx.Unit>::.ctor(UniRx.IObservable`1<T>,System.Func`2<T,System.Boolean>,System.Boolean)
extern "C"  void FirstObservable_1__ctor_m1393929284_gshared (FirstObservable_1_t285873156 * __this, Il2CppObject* ___source0, Func_2_t2766110903 * ___predicate1, bool ___useDefault2, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = ___source0;
		bool L_1 = ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)(NULL /*static, unused*/, (Il2CppObject*)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		NullCheck((OperatorObservableBase_1_t1622431009 *)__this);
		((  void (*) (OperatorObservableBase_1_t1622431009 *, bool, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)((OperatorObservableBase_1_t1622431009 *)__this, (bool)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		Il2CppObject* L_2 = ___source0;
		__this->set_source_1(L_2);
		Func_2_t2766110903 * L_3 = ___predicate1;
		__this->set_predicate_3(L_3);
		bool L_4 = ___useDefault2;
		__this->set_useDefault_2(L_4);
		return;
	}
}
// System.IDisposable UniRx.Operators.FirstObservable`1<UniRx.Unit>::SubscribeCore(UniRx.IObserver`1<T>,System.IDisposable)
extern "C"  Il2CppObject * FirstObservable_1_SubscribeCore_m549488868_gshared (FirstObservable_1_t285873156 * __this, Il2CppObject* ___observer0, Il2CppObject * ___cancel1, const MethodInfo* method)
{
	{
		Func_2_t2766110903 * L_0 = (Func_2_t2766110903 *)__this->get_predicate_3();
		if (L_0)
		{
			goto IL_001f;
		}
	}
	{
		Il2CppObject* L_1 = (Il2CppObject*)__this->get_source_1();
		Il2CppObject* L_2 = ___observer0;
		Il2CppObject * L_3 = ___cancel1;
		First_t641189227 * L_4 = (First_t641189227 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		((  void (*) (First_t641189227 *, FirstObservable_1_t285873156 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)(L_4, (FirstObservable_1_t285873156 *)__this, (Il2CppObject*)L_2, (Il2CppObject *)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		NullCheck((Il2CppObject*)L_1);
		Il2CppObject * L_5 = InterfaceFuncInvoker1< Il2CppObject *, Il2CppObject* >::Invoke(0 /* System.IDisposable UniRx.IObservable`1<UniRx.Unit>::Subscribe(UniRx.IObserver`1<T>) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5), (Il2CppObject*)L_1, (Il2CppObject*)L_4);
		return L_5;
	}

IL_001f:
	{
		Il2CppObject* L_6 = (Il2CppObject*)__this->get_source_1();
		Il2CppObject* L_7 = ___observer0;
		Il2CppObject * L_8 = ___cancel1;
		First__t1485551954 * L_9 = (First__t1485551954 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		((  void (*) (First__t1485551954 *, FirstObservable_1_t285873156 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7)->method)(L_9, (FirstObservable_1_t285873156 *)__this, (Il2CppObject*)L_7, (Il2CppObject *)L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7));
		NullCheck((Il2CppObject*)L_6);
		Il2CppObject * L_10 = InterfaceFuncInvoker1< Il2CppObject *, Il2CppObject* >::Invoke(0 /* System.IDisposable UniRx.IObservable`1<UniRx.Unit>::Subscribe(UniRx.IObserver`1<T>) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5), (Il2CppObject*)L_6, (Il2CppObject*)L_9);
		return L_10;
	}
}
// System.Void UniRx.Operators.ForEachAsyncObservable`1/ForEachAsync_<System.Object>::.ctor(UniRx.Operators.ForEachAsyncObservable`1<T>,UniRx.IObserver`1<UniRx.Unit>,System.IDisposable)
extern "C"  void ForEachAsync___ctor_m396767131_gshared (ForEachAsync__t1789301054 * __this, ForEachAsyncObservable_1_t3176723348 * ___parent0, Il2CppObject* ___observer1, Il2CppObject * ___cancel2, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = ___observer1;
		Il2CppObject * L_1 = ___cancel2;
		NullCheck((OperatorObserverBase_2_t2908947767 *)__this);
		((  void (*) (OperatorObserverBase_2_t2908947767 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((OperatorObserverBase_2_t2908947767 *)__this, (Il2CppObject*)L_0, (Il2CppObject *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		ForEachAsyncObservable_1_t3176723348 * L_2 = ___parent0;
		__this->set_parent_2(L_2);
		return;
	}
}
// System.Void UniRx.Operators.ForEachAsyncObservable`1/ForEachAsync_<System.Object>::OnNext(T)
extern Il2CppClass* Exception_t1967233988_il2cpp_TypeInfo_var;
extern Il2CppClass* IObserver_1_t475317645_il2cpp_TypeInfo_var;
extern const uint32_t ForEachAsync__OnNext_m449815454_MetadataUsageId;
extern "C"  void ForEachAsync__OnNext_m449815454_gshared (ForEachAsync__t1789301054 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ForEachAsync__OnNext_m449815454_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Exception_t1967233988 * V_0 = NULL;
	int32_t V_1 = 0;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		ForEachAsyncObservable_1_t3176723348 * L_0 = (ForEachAsyncObservable_1_t3176723348 *)__this->get_parent_2();
		NullCheck(L_0);
		Action_2_t1820800989 * L_1 = (Action_2_t1820800989 *)L_0->get_onNextWithIndex_3();
		Il2CppObject * L_2 = ___value0;
		int32_t L_3 = (int32_t)__this->get_index_3();
		int32_t L_4 = (int32_t)L_3;
		V_1 = (int32_t)L_4;
		__this->set_index_3(((int32_t)((int32_t)L_4+(int32_t)1)));
		int32_t L_5 = V_1;
		NullCheck((Action_2_t1820800989 *)L_1);
		((  void (*) (Action_2_t1820800989 *, Il2CppObject *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)((Action_2_t1820800989 *)L_1, (Il2CppObject *)L_2, (int32_t)L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		goto IL_004c;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1967233988 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Exception_t1967233988_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_0027;
		throw e;
	}

CATCH_0027:
	{ // begin catch(System.Exception)
		{
			V_0 = (Exception_t1967233988 *)((Exception_t1967233988 *)__exception_local);
		}

IL_0028:
		try
		{ // begin try (depth: 2)
			Il2CppObject* L_6 = (Il2CppObject*)((OperatorObserverBase_2_t2908947767 *)__this)->get_observer_0();
			il2cpp_codegen_memory_barrier();
			Exception_t1967233988 * L_7 = V_0;
			NullCheck((Il2CppObject*)L_6);
			InterfaceActionInvoker1< Exception_t1967233988 * >::Invoke(1 /* System.Void UniRx.IObserver`1<UniRx.Unit>::OnError(System.Exception) */, IObserver_1_t475317645_il2cpp_TypeInfo_var, (Il2CppObject*)L_6, (Exception_t1967233988 *)L_7);
			IL2CPP_LEAVE(0x42, FINALLY_003b);
		} // end try (depth: 2)
		catch(Il2CppExceptionWrapper& e)
		{
			__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
			goto FINALLY_003b;
		}

FINALLY_003b:
		{ // begin finally (depth: 2)
			NullCheck((OperatorObserverBase_2_t2908947767 *)__this);
			VirtActionInvoker0::Invoke(4 /* System.Void UniRx.Operators.OperatorObserverBase`2<System.Object,UniRx.Unit>::Dispose() */, (OperatorObserverBase_2_t2908947767 *)__this);
			IL2CPP_END_FINALLY(59)
		} // end finally (depth: 2)
		IL2CPP_CLEANUP(59)
		{
			IL2CPP_JUMP_TBL(0x42, IL_0042)
			IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
		}

IL_0042:
		{
			goto IL_004c;
		}

IL_0047:
		{
			; // IL_0047: leave IL_004c
		}
	} // end catch (depth: 1)

IL_004c:
	{
		return;
	}
}
// System.Void UniRx.Operators.ForEachAsyncObservable`1/ForEachAsync_<System.Object>::OnError(System.Exception)
extern Il2CppClass* IObserver_1_t475317645_il2cpp_TypeInfo_var;
extern const uint32_t ForEachAsync__OnError_m2034549421_MetadataUsageId;
extern "C"  void ForEachAsync__OnError_m2034549421_gshared (ForEachAsync__t1789301054 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ForEachAsync__OnError_m2034549421_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		Il2CppObject* L_0 = (Il2CppObject*)((OperatorObserverBase_2_t2908947767 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		Exception_t1967233988 * L_1 = ___error0;
		NullCheck((Il2CppObject*)L_0);
		InterfaceActionInvoker1< Exception_t1967233988 * >::Invoke(1 /* System.Void UniRx.IObserver`1<UniRx.Unit>::OnError(System.Exception) */, IObserver_1_t475317645_il2cpp_TypeInfo_var, (Il2CppObject*)L_0, (Exception_t1967233988 *)L_1);
		IL2CPP_LEAVE(0x1A, FINALLY_0013);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0013;
	}

FINALLY_0013:
	{ // begin finally (depth: 1)
		NullCheck((OperatorObserverBase_2_t2908947767 *)__this);
		VirtActionInvoker0::Invoke(4 /* System.Void UniRx.Operators.OperatorObserverBase`2<System.Object,UniRx.Unit>::Dispose() */, (OperatorObserverBase_2_t2908947767 *)__this);
		IL2CPP_END_FINALLY(19)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(19)
	{
		IL2CPP_JUMP_TBL(0x1A, IL_001a)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_001a:
	{
		return;
	}
}
// System.Void UniRx.Operators.ForEachAsyncObservable`1/ForEachAsync_<System.Object>::OnCompleted()
extern Il2CppClass* Unit_t2558286038_il2cpp_TypeInfo_var;
extern Il2CppClass* IObserver_1_t475317645_il2cpp_TypeInfo_var;
extern const uint32_t ForEachAsync__OnCompleted_m1913147776_MetadataUsageId;
extern "C"  void ForEachAsync__OnCompleted_m1913147776_gshared (ForEachAsync__t1789301054 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ForEachAsync__OnCompleted_m1913147776_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Il2CppObject* L_0 = (Il2CppObject*)((OperatorObserverBase_2_t2908947767 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		IL2CPP_RUNTIME_CLASS_INIT(Unit_t2558286038_il2cpp_TypeInfo_var);
		Unit_t2558286038  L_1 = Unit_get_Default_m3654553472(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck((Il2CppObject*)L_0);
		InterfaceActionInvoker1< Unit_t2558286038  >::Invoke(2 /* System.Void UniRx.IObserver`1<UniRx.Unit>::OnNext(T) */, IObserver_1_t475317645_il2cpp_TypeInfo_var, (Il2CppObject*)L_0, (Unit_t2558286038 )L_1);
	}

IL_0012:
	try
	{ // begin try (depth: 1)
		Il2CppObject* L_2 = (Il2CppObject*)((OperatorObserverBase_2_t2908947767 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		NullCheck((Il2CppObject*)L_2);
		InterfaceActionInvoker0::Invoke(0 /* System.Void UniRx.IObserver`1<UniRx.Unit>::OnCompleted() */, IObserver_1_t475317645_il2cpp_TypeInfo_var, (Il2CppObject*)L_2);
		IL2CPP_LEAVE(0x2B, FINALLY_0024);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0024;
	}

FINALLY_0024:
	{ // begin finally (depth: 1)
		NullCheck((OperatorObserverBase_2_t2908947767 *)__this);
		VirtActionInvoker0::Invoke(4 /* System.Void UniRx.Operators.OperatorObserverBase`2<System.Object,UniRx.Unit>::Dispose() */, (OperatorObserverBase_2_t2908947767 *)__this);
		IL2CPP_END_FINALLY(36)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(36)
	{
		IL2CPP_JUMP_TBL(0x2B, IL_002b)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_002b:
	{
		return;
	}
}
// System.Void UniRx.Operators.ForEachAsyncObservable`1/ForEachAsync<System.Object>::.ctor(UniRx.Operators.ForEachAsyncObservable`1<T>,UniRx.IObserver`1<UniRx.Unit>,System.IDisposable)
extern "C"  void ForEachAsync__ctor_m2207845852_gshared (ForEachAsync_t4111581179 * __this, ForEachAsyncObservable_1_t3176723348 * ___parent0, Il2CppObject* ___observer1, Il2CppObject * ___cancel2, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = ___observer1;
		Il2CppObject * L_1 = ___cancel2;
		NullCheck((OperatorObserverBase_2_t2908947767 *)__this);
		((  void (*) (OperatorObserverBase_2_t2908947767 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((OperatorObserverBase_2_t2908947767 *)__this, (Il2CppObject*)L_0, (Il2CppObject *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		ForEachAsyncObservable_1_t3176723348 * L_2 = ___parent0;
		__this->set_parent_2(L_2);
		return;
	}
}
// System.Void UniRx.Operators.ForEachAsyncObservable`1/ForEachAsync<System.Object>::OnNext(T)
extern Il2CppClass* Exception_t1967233988_il2cpp_TypeInfo_var;
extern Il2CppClass* IObserver_1_t475317645_il2cpp_TypeInfo_var;
extern const uint32_t ForEachAsync_OnNext_m3590576415_MetadataUsageId;
extern "C"  void ForEachAsync_OnNext_m3590576415_gshared (ForEachAsync_t4111581179 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ForEachAsync_OnNext_m3590576415_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Exception_t1967233988 * V_0 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		ForEachAsyncObservable_1_t3176723348 * L_0 = (ForEachAsyncObservable_1_t3176723348 *)__this->get_parent_2();
		NullCheck(L_0);
		Action_1_t985559125 * L_1 = (Action_1_t985559125 *)L_0->get_onNext_2();
		Il2CppObject * L_2 = ___value0;
		NullCheck((Action_1_t985559125 *)L_1);
		((  void (*) (Action_1_t985559125 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)((Action_1_t985559125 *)L_1, (Il2CppObject *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		goto IL_003b;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1967233988 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Exception_t1967233988_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_0016;
		throw e;
	}

CATCH_0016:
	{ // begin catch(System.Exception)
		{
			V_0 = (Exception_t1967233988 *)((Exception_t1967233988 *)__exception_local);
		}

IL_0017:
		try
		{ // begin try (depth: 2)
			Il2CppObject* L_3 = (Il2CppObject*)((OperatorObserverBase_2_t2908947767 *)__this)->get_observer_0();
			il2cpp_codegen_memory_barrier();
			Exception_t1967233988 * L_4 = V_0;
			NullCheck((Il2CppObject*)L_3);
			InterfaceActionInvoker1< Exception_t1967233988 * >::Invoke(1 /* System.Void UniRx.IObserver`1<UniRx.Unit>::OnError(System.Exception) */, IObserver_1_t475317645_il2cpp_TypeInfo_var, (Il2CppObject*)L_3, (Exception_t1967233988 *)L_4);
			IL2CPP_LEAVE(0x31, FINALLY_002a);
		} // end try (depth: 2)
		catch(Il2CppExceptionWrapper& e)
		{
			__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
			goto FINALLY_002a;
		}

FINALLY_002a:
		{ // begin finally (depth: 2)
			NullCheck((OperatorObserverBase_2_t2908947767 *)__this);
			VirtActionInvoker0::Invoke(4 /* System.Void UniRx.Operators.OperatorObserverBase`2<System.Object,UniRx.Unit>::Dispose() */, (OperatorObserverBase_2_t2908947767 *)__this);
			IL2CPP_END_FINALLY(42)
		} // end finally (depth: 2)
		IL2CPP_CLEANUP(42)
		{
			IL2CPP_JUMP_TBL(0x31, IL_0031)
			IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
		}

IL_0031:
		{
			goto IL_003b;
		}

IL_0036:
		{
			; // IL_0036: leave IL_003b
		}
	} // end catch (depth: 1)

IL_003b:
	{
		return;
	}
}
// System.Void UniRx.Operators.ForEachAsyncObservable`1/ForEachAsync<System.Object>::OnError(System.Exception)
extern Il2CppClass* IObserver_1_t475317645_il2cpp_TypeInfo_var;
extern const uint32_t ForEachAsync_OnError_m487052846_MetadataUsageId;
extern "C"  void ForEachAsync_OnError_m487052846_gshared (ForEachAsync_t4111581179 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ForEachAsync_OnError_m487052846_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		Il2CppObject* L_0 = (Il2CppObject*)((OperatorObserverBase_2_t2908947767 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		Exception_t1967233988 * L_1 = ___error0;
		NullCheck((Il2CppObject*)L_0);
		InterfaceActionInvoker1< Exception_t1967233988 * >::Invoke(1 /* System.Void UniRx.IObserver`1<UniRx.Unit>::OnError(System.Exception) */, IObserver_1_t475317645_il2cpp_TypeInfo_var, (Il2CppObject*)L_0, (Exception_t1967233988 *)L_1);
		IL2CPP_LEAVE(0x1A, FINALLY_0013);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0013;
	}

FINALLY_0013:
	{ // begin finally (depth: 1)
		NullCheck((OperatorObserverBase_2_t2908947767 *)__this);
		VirtActionInvoker0::Invoke(4 /* System.Void UniRx.Operators.OperatorObserverBase`2<System.Object,UniRx.Unit>::Dispose() */, (OperatorObserverBase_2_t2908947767 *)__this);
		IL2CPP_END_FINALLY(19)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(19)
	{
		IL2CPP_JUMP_TBL(0x1A, IL_001a)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_001a:
	{
		return;
	}
}
// System.Void UniRx.Operators.ForEachAsyncObservable`1/ForEachAsync<System.Object>::OnCompleted()
extern Il2CppClass* Unit_t2558286038_il2cpp_TypeInfo_var;
extern Il2CppClass* IObserver_1_t475317645_il2cpp_TypeInfo_var;
extern const uint32_t ForEachAsync_OnCompleted_m1697898113_MetadataUsageId;
extern "C"  void ForEachAsync_OnCompleted_m1697898113_gshared (ForEachAsync_t4111581179 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ForEachAsync_OnCompleted_m1697898113_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Il2CppObject* L_0 = (Il2CppObject*)((OperatorObserverBase_2_t2908947767 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		IL2CPP_RUNTIME_CLASS_INIT(Unit_t2558286038_il2cpp_TypeInfo_var);
		Unit_t2558286038  L_1 = Unit_get_Default_m3654553472(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck((Il2CppObject*)L_0);
		InterfaceActionInvoker1< Unit_t2558286038  >::Invoke(2 /* System.Void UniRx.IObserver`1<UniRx.Unit>::OnNext(T) */, IObserver_1_t475317645_il2cpp_TypeInfo_var, (Il2CppObject*)L_0, (Unit_t2558286038 )L_1);
	}

IL_0012:
	try
	{ // begin try (depth: 1)
		Il2CppObject* L_2 = (Il2CppObject*)((OperatorObserverBase_2_t2908947767 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		NullCheck((Il2CppObject*)L_2);
		InterfaceActionInvoker0::Invoke(0 /* System.Void UniRx.IObserver`1<UniRx.Unit>::OnCompleted() */, IObserver_1_t475317645_il2cpp_TypeInfo_var, (Il2CppObject*)L_2);
		IL2CPP_LEAVE(0x2B, FINALLY_0024);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0024;
	}

FINALLY_0024:
	{ // begin finally (depth: 1)
		NullCheck((OperatorObserverBase_2_t2908947767 *)__this);
		VirtActionInvoker0::Invoke(4 /* System.Void UniRx.Operators.OperatorObserverBase`2<System.Object,UniRx.Unit>::Dispose() */, (OperatorObserverBase_2_t2908947767 *)__this);
		IL2CPP_END_FINALLY(36)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(36)
	{
		IL2CPP_JUMP_TBL(0x2B, IL_002b)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_002b:
	{
		return;
	}
}
// System.Void UniRx.Operators.ForEachAsyncObservable`1<System.Object>::.ctor(UniRx.IObservable`1<T>,System.Action`1<T>)
extern const MethodInfo* OperatorObservableBase_1__ctor_m284357152_MethodInfo_var;
extern const uint32_t ForEachAsyncObservable_1__ctor_m2188122063_MetadataUsageId;
extern "C"  void ForEachAsyncObservable_1__ctor_m2188122063_gshared (ForEachAsyncObservable_1_t3176723348 * __this, Il2CppObject* ___source0, Action_1_t985559125 * ___onNext1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ForEachAsyncObservable_1__ctor_m2188122063_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject* L_0 = ___source0;
		bool L_1 = ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)(NULL /*static, unused*/, (Il2CppObject*)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		NullCheck((OperatorObservableBase_1_t1622431009 *)__this);
		OperatorObservableBase_1__ctor_m284357152((OperatorObservableBase_1_t1622431009 *)__this, (bool)L_1, /*hidden argument*/OperatorObservableBase_1__ctor_m284357152_MethodInfo_var);
		Il2CppObject* L_2 = ___source0;
		__this->set_source_1(L_2);
		Action_1_t985559125 * L_3 = ___onNext1;
		__this->set_onNext_2(L_3);
		return;
	}
}
// System.Void UniRx.Operators.ForEachAsyncObservable`1<System.Object>::.ctor(UniRx.IObservable`1<T>,System.Action`2<T,System.Int32>)
extern const MethodInfo* OperatorObservableBase_1__ctor_m284357152_MethodInfo_var;
extern const uint32_t ForEachAsyncObservable_1__ctor_m2329194913_MetadataUsageId;
extern "C"  void ForEachAsyncObservable_1__ctor_m2329194913_gshared (ForEachAsyncObservable_1_t3176723348 * __this, Il2CppObject* ___source0, Action_2_t1820800989 * ___onNext1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ForEachAsyncObservable_1__ctor_m2329194913_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject* L_0 = ___source0;
		bool L_1 = ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)(NULL /*static, unused*/, (Il2CppObject*)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		NullCheck((OperatorObservableBase_1_t1622431009 *)__this);
		OperatorObservableBase_1__ctor_m284357152((OperatorObservableBase_1_t1622431009 *)__this, (bool)L_1, /*hidden argument*/OperatorObservableBase_1__ctor_m284357152_MethodInfo_var);
		Il2CppObject* L_2 = ___source0;
		__this->set_source_1(L_2);
		Action_2_t1820800989 * L_3 = ___onNext1;
		__this->set_onNextWithIndex_3(L_3);
		return;
	}
}
// System.IDisposable UniRx.Operators.ForEachAsyncObservable`1<System.Object>::SubscribeCore(UniRx.IObserver`1<UniRx.Unit>,System.IDisposable)
extern "C"  Il2CppObject * ForEachAsyncObservable_1_SubscribeCore_m3095022402_gshared (ForEachAsyncObservable_1_t3176723348 * __this, Il2CppObject* ___observer0, Il2CppObject * ___cancel1, const MethodInfo* method)
{
	{
		Action_1_t985559125 * L_0 = (Action_1_t985559125 *)__this->get_onNext_2();
		if (!L_0)
		{
			goto IL_001f;
		}
	}
	{
		Il2CppObject* L_1 = (Il2CppObject*)__this->get_source_1();
		Il2CppObject* L_2 = ___observer0;
		Il2CppObject * L_3 = ___cancel1;
		ForEachAsync_t4111581179 * L_4 = (ForEachAsync_t4111581179 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		((  void (*) (ForEachAsync_t4111581179 *, ForEachAsyncObservable_1_t3176723348 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)(L_4, (ForEachAsyncObservable_1_t3176723348 *)__this, (Il2CppObject*)L_2, (Il2CppObject *)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		NullCheck((Il2CppObject*)L_1);
		Il2CppObject * L_5 = InterfaceFuncInvoker1< Il2CppObject *, Il2CppObject* >::Invoke(0 /* System.IDisposable UniRx.IObservable`1<System.Object>::Subscribe(UniRx.IObserver`1<T>) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3), (Il2CppObject*)L_1, (Il2CppObject*)L_4);
		return L_5;
	}

IL_001f:
	{
		Il2CppObject* L_6 = (Il2CppObject*)__this->get_source_1();
		Il2CppObject* L_7 = ___observer0;
		Il2CppObject * L_8 = ___cancel1;
		ForEachAsync__t1789301054 * L_9 = (ForEachAsync__t1789301054 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		((  void (*) (ForEachAsync__t1789301054 *, ForEachAsyncObservable_1_t3176723348 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->method)(L_9, (ForEachAsyncObservable_1_t3176723348 *)__this, (Il2CppObject*)L_7, (Il2CppObject *)L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		NullCheck((Il2CppObject*)L_6);
		Il2CppObject * L_10 = InterfaceFuncInvoker1< Il2CppObject *, Il2CppObject* >::Invoke(0 /* System.IDisposable UniRx.IObservable`1<System.Object>::Subscribe(UniRx.IObserver`1<T>) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3), (Il2CppObject*)L_6, (Il2CppObject*)L_9);
		return L_10;
	}
}
// System.Void UniRx.Operators.FromCoroutine`1/FromCoroutineObserver<System.Double>::.ctor(UniRx.IObserver`1<T>,System.IDisposable)
extern "C"  void FromCoroutineObserver__ctor_m3431534472_gshared (FromCoroutineObserver_t3712745359 * __this, Il2CppObject* ___observer0, Il2CppObject * ___cancel1, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = ___observer0;
		Il2CppObject * L_1 = ___cancel1;
		NullCheck((OperatorObserverBase_2_t1921935565 *)__this);
		((  void (*) (OperatorObserverBase_2_t1921935565 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((OperatorObserverBase_2_t1921935565 *)__this, (Il2CppObject*)L_0, (Il2CppObject *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		return;
	}
}
// System.Void UniRx.Operators.FromCoroutine`1/FromCoroutineObserver<System.Double>::OnNext(T)
extern Il2CppClass* Il2CppObject_il2cpp_TypeInfo_var;
extern const uint32_t FromCoroutineObserver_OnNext_m2249157386_MetadataUsageId;
extern "C"  void FromCoroutineObserver_OnNext_m2249157386_gshared (FromCoroutineObserver_t3712745359 * __this, double ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (FromCoroutineObserver_OnNext_m2249157386_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		Il2CppObject* L_0 = (Il2CppObject*)((OperatorObserverBase_2_t1921935565 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		double L_1 = ___value0;
		NullCheck((Il2CppObject*)L_0);
		InterfaceActionInvoker1< double >::Invoke(2 /* System.Void UniRx.IObserver`1<System.Double>::OnNext(T) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), (Il2CppObject*)L_0, (double)L_1);
		goto IL_0021;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1967233988 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Il2CppObject_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_0013;
		throw e;
	}

CATCH_0013:
	{ // begin catch(System.Object)
		{
			NullCheck((OperatorObserverBase_2_t1921935565 *)__this);
			VirtActionInvoker0::Invoke(4 /* System.Void UniRx.Operators.OperatorObserverBase`2<System.Double,System.Double>::Dispose() */, (OperatorObserverBase_2_t1921935565 *)__this);
			IL2CPP_RAISE_MANAGED_EXCEPTION(__exception_local);
		}

IL_001c:
		{
			goto IL_0021;
		}
	} // end catch (depth: 1)

IL_0021:
	{
		return;
	}
}
// System.Void UniRx.Operators.FromCoroutine`1/FromCoroutineObserver<System.Double>::OnError(System.Exception)
extern "C"  void FromCoroutineObserver_OnError_m2579509785_gshared (FromCoroutineObserver_t3712745359 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method)
{
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		Il2CppObject* L_0 = (Il2CppObject*)((OperatorObserverBase_2_t1921935565 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		Exception_t1967233988 * L_1 = ___error0;
		NullCheck((Il2CppObject*)L_0);
		InterfaceActionInvoker1< Exception_t1967233988 * >::Invoke(1 /* System.Void UniRx.IObserver`1<System.Double>::OnError(System.Exception) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), (Il2CppObject*)L_0, (Exception_t1967233988 *)L_1);
		IL2CPP_LEAVE(0x1A, FINALLY_0013);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0013;
	}

FINALLY_0013:
	{ // begin finally (depth: 1)
		NullCheck((OperatorObserverBase_2_t1921935565 *)__this);
		VirtActionInvoker0::Invoke(4 /* System.Void UniRx.Operators.OperatorObserverBase`2<System.Double,System.Double>::Dispose() */, (OperatorObserverBase_2_t1921935565 *)__this);
		IL2CPP_END_FINALLY(19)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(19)
	{
		IL2CPP_JUMP_TBL(0x1A, IL_001a)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_001a:
	{
		return;
	}
}
// System.Void UniRx.Operators.FromCoroutine`1/FromCoroutineObserver<System.Double>::OnCompleted()
extern "C"  void FromCoroutineObserver_OnCompleted_m536773356_gshared (FromCoroutineObserver_t3712745359 * __this, const MethodInfo* method)
{
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		Il2CppObject* L_0 = (Il2CppObject*)((OperatorObserverBase_2_t1921935565 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		NullCheck((Il2CppObject*)L_0);
		InterfaceActionInvoker0::Invoke(0 /* System.Void UniRx.IObserver`1<System.Double>::OnCompleted() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), (Il2CppObject*)L_0);
		IL2CPP_LEAVE(0x19, FINALLY_0012);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0012;
	}

FINALLY_0012:
	{ // begin finally (depth: 1)
		NullCheck((OperatorObserverBase_2_t1921935565 *)__this);
		VirtActionInvoker0::Invoke(4 /* System.Void UniRx.Operators.OperatorObserverBase`2<System.Double,System.Double>::Dispose() */, (OperatorObserverBase_2_t1921935565 *)__this);
		IL2CPP_END_FINALLY(18)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(18)
	{
		IL2CPP_JUMP_TBL(0x19, IL_0019)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0019:
	{
		return;
	}
}
// System.Void UniRx.Operators.FromCoroutine`1/FromCoroutineObserver<System.Int64>::.ctor(UniRx.IObserver`1<T>,System.IDisposable)
extern "C"  void FromCoroutineObserver__ctor_m343764228_gshared (FromCoroutineObserver_t1730676331 * __this, Il2CppObject* ___observer0, Il2CppObject * ___cancel1, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = ___observer0;
		Il2CppObject * L_1 = ___cancel1;
		NullCheck((OperatorObserverBase_2_t3939730909 *)__this);
		((  void (*) (OperatorObserverBase_2_t3939730909 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((OperatorObserverBase_2_t3939730909 *)__this, (Il2CppObject*)L_0, (Il2CppObject *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		return;
	}
}
// System.Void UniRx.Operators.FromCoroutine`1/FromCoroutineObserver<System.Int64>::OnNext(T)
extern Il2CppClass* Il2CppObject_il2cpp_TypeInfo_var;
extern const uint32_t FromCoroutineObserver_OnNext_m934453262_MetadataUsageId;
extern "C"  void FromCoroutineObserver_OnNext_m934453262_gshared (FromCoroutineObserver_t1730676331 * __this, int64_t ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (FromCoroutineObserver_OnNext_m934453262_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		Il2CppObject* L_0 = (Il2CppObject*)((OperatorObserverBase_2_t3939730909 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		int64_t L_1 = ___value0;
		NullCheck((Il2CppObject*)L_0);
		InterfaceActionInvoker1< int64_t >::Invoke(2 /* System.Void UniRx.IObserver`1<System.Int64>::OnNext(T) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), (Il2CppObject*)L_0, (int64_t)L_1);
		goto IL_0021;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1967233988 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Il2CppObject_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_0013;
		throw e;
	}

CATCH_0013:
	{ // begin catch(System.Object)
		{
			NullCheck((OperatorObserverBase_2_t3939730909 *)__this);
			VirtActionInvoker0::Invoke(4 /* System.Void UniRx.Operators.OperatorObserverBase`2<System.Int64,System.Int64>::Dispose() */, (OperatorObserverBase_2_t3939730909 *)__this);
			IL2CPP_RAISE_MANAGED_EXCEPTION(__exception_local);
		}

IL_001c:
		{
			goto IL_0021;
		}
	} // end catch (depth: 1)

IL_0021:
	{
		return;
	}
}
// System.Void UniRx.Operators.FromCoroutine`1/FromCoroutineObserver<System.Int64>::OnError(System.Exception)
extern "C"  void FromCoroutineObserver_OnError_m318824221_gshared (FromCoroutineObserver_t1730676331 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method)
{
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		Il2CppObject* L_0 = (Il2CppObject*)((OperatorObserverBase_2_t3939730909 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		Exception_t1967233988 * L_1 = ___error0;
		NullCheck((Il2CppObject*)L_0);
		InterfaceActionInvoker1< Exception_t1967233988 * >::Invoke(1 /* System.Void UniRx.IObserver`1<System.Int64>::OnError(System.Exception) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), (Il2CppObject*)L_0, (Exception_t1967233988 *)L_1);
		IL2CPP_LEAVE(0x1A, FINALLY_0013);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0013;
	}

FINALLY_0013:
	{ // begin finally (depth: 1)
		NullCheck((OperatorObserverBase_2_t3939730909 *)__this);
		VirtActionInvoker0::Invoke(4 /* System.Void UniRx.Operators.OperatorObserverBase`2<System.Int64,System.Int64>::Dispose() */, (OperatorObserverBase_2_t3939730909 *)__this);
		IL2CPP_END_FINALLY(19)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(19)
	{
		IL2CPP_JUMP_TBL(0x1A, IL_001a)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_001a:
	{
		return;
	}
}
// System.Void UniRx.Operators.FromCoroutine`1/FromCoroutineObserver<System.Int64>::OnCompleted()
extern "C"  void FromCoroutineObserver_OnCompleted_m859280880_gshared (FromCoroutineObserver_t1730676331 * __this, const MethodInfo* method)
{
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		Il2CppObject* L_0 = (Il2CppObject*)((OperatorObserverBase_2_t3939730909 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		NullCheck((Il2CppObject*)L_0);
		InterfaceActionInvoker0::Invoke(0 /* System.Void UniRx.IObserver`1<System.Int64>::OnCompleted() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), (Il2CppObject*)L_0);
		IL2CPP_LEAVE(0x19, FINALLY_0012);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0012;
	}

FINALLY_0012:
	{ // begin finally (depth: 1)
		NullCheck((OperatorObserverBase_2_t3939730909 *)__this);
		VirtActionInvoker0::Invoke(4 /* System.Void UniRx.Operators.OperatorObserverBase`2<System.Int64,System.Int64>::Dispose() */, (OperatorObserverBase_2_t3939730909 *)__this);
		IL2CPP_END_FINALLY(18)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(18)
	{
		IL2CPP_JUMP_TBL(0x19, IL_0019)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0019:
	{
		return;
	}
}
// System.Void UniRx.Operators.FromCoroutine`1/FromCoroutineObserver<System.Object>::.ctor(UniRx.IObserver`1<T>,System.IDisposable)
extern "C"  void FromCoroutineObserver__ctor_m2161387994_gshared (FromCoroutineObserver_t4015335165 * __this, Il2CppObject* ___observer0, Il2CppObject * ___cancel1, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = ___observer0;
		Il2CppObject * L_1 = ___cancel1;
		NullCheck((OperatorObserverBase_2_t1187768149 *)__this);
		((  void (*) (OperatorObserverBase_2_t1187768149 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((OperatorObserverBase_2_t1187768149 *)__this, (Il2CppObject*)L_0, (Il2CppObject *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		return;
	}
}
// System.Void UniRx.Operators.FromCoroutine`1/FromCoroutineObserver<System.Object>::OnNext(T)
extern Il2CppClass* Il2CppObject_il2cpp_TypeInfo_var;
extern const uint32_t FromCoroutineObserver_OnNext_m1155718520_MetadataUsageId;
extern "C"  void FromCoroutineObserver_OnNext_m1155718520_gshared (FromCoroutineObserver_t4015335165 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (FromCoroutineObserver_OnNext_m1155718520_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		Il2CppObject* L_0 = (Il2CppObject*)((OperatorObserverBase_2_t1187768149 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		Il2CppObject * L_1 = ___value0;
		NullCheck((Il2CppObject*)L_0);
		InterfaceActionInvoker1< Il2CppObject * >::Invoke(2 /* System.Void UniRx.IObserver`1<System.Object>::OnNext(T) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), (Il2CppObject*)L_0, (Il2CppObject *)L_1);
		goto IL_0021;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1967233988 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Il2CppObject_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_0013;
		throw e;
	}

CATCH_0013:
	{ // begin catch(System.Object)
		{
			NullCheck((OperatorObserverBase_2_t1187768149 *)__this);
			VirtActionInvoker0::Invoke(4 /* System.Void UniRx.Operators.OperatorObserverBase`2<System.Object,System.Object>::Dispose() */, (OperatorObserverBase_2_t1187768149 *)__this);
			IL2CPP_RAISE_MANAGED_EXCEPTION(__exception_local);
		}

IL_001c:
		{
			goto IL_0021;
		}
	} // end catch (depth: 1)

IL_0021:
	{
		return;
	}
}
// System.Void UniRx.Operators.FromCoroutine`1/FromCoroutineObserver<System.Object>::OnError(System.Exception)
extern "C"  void FromCoroutineObserver_OnError_m2691500167_gshared (FromCoroutineObserver_t4015335165 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method)
{
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		Il2CppObject* L_0 = (Il2CppObject*)((OperatorObserverBase_2_t1187768149 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		Exception_t1967233988 * L_1 = ___error0;
		NullCheck((Il2CppObject*)L_0);
		InterfaceActionInvoker1< Exception_t1967233988 * >::Invoke(1 /* System.Void UniRx.IObserver`1<System.Object>::OnError(System.Exception) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), (Il2CppObject*)L_0, (Exception_t1967233988 *)L_1);
		IL2CPP_LEAVE(0x1A, FINALLY_0013);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0013;
	}

FINALLY_0013:
	{ // begin finally (depth: 1)
		NullCheck((OperatorObserverBase_2_t1187768149 *)__this);
		VirtActionInvoker0::Invoke(4 /* System.Void UniRx.Operators.OperatorObserverBase`2<System.Object,System.Object>::Dispose() */, (OperatorObserverBase_2_t1187768149 *)__this);
		IL2CPP_END_FINALLY(19)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(19)
	{
		IL2CPP_JUMP_TBL(0x1A, IL_001a)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_001a:
	{
		return;
	}
}
// System.Void UniRx.Operators.FromCoroutine`1/FromCoroutineObserver<System.Object>::OnCompleted()
extern "C"  void FromCoroutineObserver_OnCompleted_m2312572506_gshared (FromCoroutineObserver_t4015335165 * __this, const MethodInfo* method)
{
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		Il2CppObject* L_0 = (Il2CppObject*)((OperatorObserverBase_2_t1187768149 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		NullCheck((Il2CppObject*)L_0);
		InterfaceActionInvoker0::Invoke(0 /* System.Void UniRx.IObserver`1<System.Object>::OnCompleted() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), (Il2CppObject*)L_0);
		IL2CPP_LEAVE(0x19, FINALLY_0012);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0012;
	}

FINALLY_0012:
	{ // begin finally (depth: 1)
		NullCheck((OperatorObserverBase_2_t1187768149 *)__this);
		VirtActionInvoker0::Invoke(4 /* System.Void UniRx.Operators.OperatorObserverBase`2<System.Object,System.Object>::Dispose() */, (OperatorObserverBase_2_t1187768149 *)__this);
		IL2CPP_END_FINALLY(18)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(18)
	{
		IL2CPP_JUMP_TBL(0x19, IL_0019)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0019:
	{
		return;
	}
}
// System.Void UniRx.Operators.FromCoroutine`1/FromCoroutineObserver<UniRx.Unit>::.ctor(UniRx.IObserver`1<T>,System.IDisposable)
extern "C"  void FromCoroutineObserver__ctor_m747451158_gshared (FromCoroutineObserver_t1441547487 * __this, Il2CppObject* ___observer0, Il2CppObject * ___cancel1, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = ___observer0;
		Il2CppObject * L_1 = ___cancel1;
		NullCheck((OperatorObserverBase_2_t4165376397 *)__this);
		((  void (*) (OperatorObserverBase_2_t4165376397 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((OperatorObserverBase_2_t4165376397 *)__this, (Il2CppObject*)L_0, (Il2CppObject *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		return;
	}
}
// System.Void UniRx.Operators.FromCoroutine`1/FromCoroutineObserver<UniRx.Unit>::OnNext(T)
extern Il2CppClass* Il2CppObject_il2cpp_TypeInfo_var;
extern const uint32_t FromCoroutineObserver_OnNext_m2734034620_MetadataUsageId;
extern "C"  void FromCoroutineObserver_OnNext_m2734034620_gshared (FromCoroutineObserver_t1441547487 * __this, Unit_t2558286038  ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (FromCoroutineObserver_OnNext_m2734034620_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		Il2CppObject* L_0 = (Il2CppObject*)((OperatorObserverBase_2_t4165376397 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		Unit_t2558286038  L_1 = ___value0;
		NullCheck((Il2CppObject*)L_0);
		InterfaceActionInvoker1< Unit_t2558286038  >::Invoke(2 /* System.Void UniRx.IObserver`1<UniRx.Unit>::OnNext(T) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), (Il2CppObject*)L_0, (Unit_t2558286038 )L_1);
		goto IL_0021;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1967233988 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Il2CppObject_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_0013;
		throw e;
	}

CATCH_0013:
	{ // begin catch(System.Object)
		{
			NullCheck((OperatorObserverBase_2_t4165376397 *)__this);
			VirtActionInvoker0::Invoke(4 /* System.Void UniRx.Operators.OperatorObserverBase`2<UniRx.Unit,UniRx.Unit>::Dispose() */, (OperatorObserverBase_2_t4165376397 *)__this);
			IL2CPP_RAISE_MANAGED_EXCEPTION(__exception_local);
		}

IL_001c:
		{
			goto IL_0021;
		}
	} // end catch (depth: 1)

IL_0021:
	{
		return;
	}
}
// System.Void UniRx.Operators.FromCoroutine`1/FromCoroutineObserver<UniRx.Unit>::OnError(System.Exception)
extern "C"  void FromCoroutineObserver_OnError_m1763520971_gshared (FromCoroutineObserver_t1441547487 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method)
{
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		Il2CppObject* L_0 = (Il2CppObject*)((OperatorObserverBase_2_t4165376397 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		Exception_t1967233988 * L_1 = ___error0;
		NullCheck((Il2CppObject*)L_0);
		InterfaceActionInvoker1< Exception_t1967233988 * >::Invoke(1 /* System.Void UniRx.IObserver`1<UniRx.Unit>::OnError(System.Exception) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), (Il2CppObject*)L_0, (Exception_t1967233988 *)L_1);
		IL2CPP_LEAVE(0x1A, FINALLY_0013);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0013;
	}

FINALLY_0013:
	{ // begin finally (depth: 1)
		NullCheck((OperatorObserverBase_2_t4165376397 *)__this);
		VirtActionInvoker0::Invoke(4 /* System.Void UniRx.Operators.OperatorObserverBase`2<UniRx.Unit,UniRx.Unit>::Dispose() */, (OperatorObserverBase_2_t4165376397 *)__this);
		IL2CPP_END_FINALLY(19)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(19)
	{
		IL2CPP_JUMP_TBL(0x1A, IL_001a)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_001a:
	{
		return;
	}
}
// System.Void UniRx.Operators.FromCoroutine`1/FromCoroutineObserver<UniRx.Unit>::OnCompleted()
extern "C"  void FromCoroutineObserver_OnCompleted_m1554513310_gshared (FromCoroutineObserver_t1441547487 * __this, const MethodInfo* method)
{
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		Il2CppObject* L_0 = (Il2CppObject*)((OperatorObserverBase_2_t4165376397 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		NullCheck((Il2CppObject*)L_0);
		InterfaceActionInvoker0::Invoke(0 /* System.Void UniRx.IObserver`1<UniRx.Unit>::OnCompleted() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), (Il2CppObject*)L_0);
		IL2CPP_LEAVE(0x19, FINALLY_0012);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0012;
	}

FINALLY_0012:
	{ // begin finally (depth: 1)
		NullCheck((OperatorObserverBase_2_t4165376397 *)__this);
		VirtActionInvoker0::Invoke(4 /* System.Void UniRx.Operators.OperatorObserverBase`2<UniRx.Unit,UniRx.Unit>::Dispose() */, (OperatorObserverBase_2_t4165376397 *)__this);
		IL2CPP_END_FINALLY(18)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(18)
	{
		IL2CPP_JUMP_TBL(0x19, IL_0019)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0019:
	{
		return;
	}
}
// System.Void UniRx.Operators.FromCoroutine`1<System.Double>::.ctor(System.Func`3<UniRx.IObserver`1<T>,UniRx.CancellationToken,System.Collections.IEnumerator>)
extern "C"  void FromCoroutine_1__ctor_m2896891658_gshared (FromCoroutine_1_t211580279 * __this, Func_3_t1280587413 * ___coroutine0, const MethodInfo* method)
{
	{
		NullCheck((OperatorObservableBase_1_t3893628881 *)__this);
		((  void (*) (OperatorObservableBase_1_t3893628881 *, bool, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((OperatorObservableBase_1_t3893628881 *)__this, (bool)0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		Func_3_t1280587413 * L_0 = ___coroutine0;
		__this->set_coroutine_1(L_0);
		return;
	}
}
// System.IDisposable UniRx.Operators.FromCoroutine`1<System.Double>::SubscribeCore(UniRx.IObserver`1<T>,System.IDisposable)
extern Il2CppClass* BooleanDisposable_t3065601722_il2cpp_TypeInfo_var;
extern Il2CppClass* MainThreadDispatcher_t4027217788_il2cpp_TypeInfo_var;
extern const uint32_t FromCoroutine_1_SubscribeCore_m943320497_MetadataUsageId;
extern "C"  Il2CppObject * FromCoroutine_1_SubscribeCore_m943320497_gshared (FromCoroutine_1_t211580279 * __this, Il2CppObject* ___observer0, Il2CppObject * ___cancel1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (FromCoroutine_1_SubscribeCore_m943320497_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	FromCoroutineObserver_t3712745359 * V_0 = NULL;
	BooleanDisposable_t3065601722 * V_1 = NULL;
	{
		Il2CppObject* L_0 = ___observer0;
		Il2CppObject * L_1 = ___cancel1;
		FromCoroutineObserver_t3712745359 * L_2 = (FromCoroutineObserver_t3712745359 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		((  void (*) (FromCoroutineObserver_t3712745359 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)(L_2, (Il2CppObject*)L_0, (Il2CppObject *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		V_0 = (FromCoroutineObserver_t3712745359 *)L_2;
		BooleanDisposable_t3065601722 * L_3 = (BooleanDisposable_t3065601722 *)il2cpp_codegen_object_new(BooleanDisposable_t3065601722_il2cpp_TypeInfo_var);
		BooleanDisposable__ctor_m2534881639(L_3, /*hidden argument*/NULL);
		V_1 = (BooleanDisposable_t3065601722 *)L_3;
		Func_3_t1280587413 * L_4 = (Func_3_t1280587413 *)__this->get_coroutine_1();
		FromCoroutineObserver_t3712745359 * L_5 = V_0;
		BooleanDisposable_t3065601722 * L_6 = V_1;
		CancellationToken_t1439151560  L_7;
		memset(&L_7, 0, sizeof(L_7));
		CancellationToken__ctor_m1453920148(&L_7, (Il2CppObject *)L_6, /*hidden argument*/NULL);
		NullCheck((Func_3_t1280587413 *)L_4);
		Il2CppObject * L_8 = ((  Il2CppObject * (*) (Func_3_t1280587413 *, Il2CppObject*, CancellationToken_t1439151560 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)((Func_3_t1280587413 *)L_4, (Il2CppObject*)L_5, (CancellationToken_t1439151560 )L_7, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		IL2CPP_RUNTIME_CLASS_INIT(MainThreadDispatcher_t4027217788_il2cpp_TypeInfo_var);
		MainThreadDispatcher_SendStartCoroutine_m2702036384(NULL /*static, unused*/, (Il2CppObject *)L_8, /*hidden argument*/NULL);
		BooleanDisposable_t3065601722 * L_9 = V_1;
		return L_9;
	}
}
// System.Void UniRx.Operators.FromCoroutine`1<System.Int64>::.ctor(System.Func`3<UniRx.IObserver`1<T>,UniRx.CancellationToken,System.Collections.IEnumerator>)
extern "C"  void FromCoroutine_1__ctor_m2654486232_gshared (FromCoroutine_1_t2524478547 * __this, Func_3_t521084177 * ___coroutine0, const MethodInfo* method)
{
	{
		NullCheck((OperatorObservableBase_1_t1911559853 *)__this);
		((  void (*) (OperatorObservableBase_1_t1911559853 *, bool, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((OperatorObservableBase_1_t1911559853 *)__this, (bool)0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		Func_3_t521084177 * L_0 = ___coroutine0;
		__this->set_coroutine_1(L_0);
		return;
	}
}
// System.IDisposable UniRx.Operators.FromCoroutine`1<System.Int64>::SubscribeCore(UniRx.IObserver`1<T>,System.IDisposable)
extern Il2CppClass* BooleanDisposable_t3065601722_il2cpp_TypeInfo_var;
extern Il2CppClass* MainThreadDispatcher_t4027217788_il2cpp_TypeInfo_var;
extern const uint32_t FromCoroutine_1_SubscribeCore_m2169225549_MetadataUsageId;
extern "C"  Il2CppObject * FromCoroutine_1_SubscribeCore_m2169225549_gshared (FromCoroutine_1_t2524478547 * __this, Il2CppObject* ___observer0, Il2CppObject * ___cancel1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (FromCoroutine_1_SubscribeCore_m2169225549_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	FromCoroutineObserver_t1730676331 * V_0 = NULL;
	BooleanDisposable_t3065601722 * V_1 = NULL;
	{
		Il2CppObject* L_0 = ___observer0;
		Il2CppObject * L_1 = ___cancel1;
		FromCoroutineObserver_t1730676331 * L_2 = (FromCoroutineObserver_t1730676331 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		((  void (*) (FromCoroutineObserver_t1730676331 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)(L_2, (Il2CppObject*)L_0, (Il2CppObject *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		V_0 = (FromCoroutineObserver_t1730676331 *)L_2;
		BooleanDisposable_t3065601722 * L_3 = (BooleanDisposable_t3065601722 *)il2cpp_codegen_object_new(BooleanDisposable_t3065601722_il2cpp_TypeInfo_var);
		BooleanDisposable__ctor_m2534881639(L_3, /*hidden argument*/NULL);
		V_1 = (BooleanDisposable_t3065601722 *)L_3;
		Func_3_t521084177 * L_4 = (Func_3_t521084177 *)__this->get_coroutine_1();
		FromCoroutineObserver_t1730676331 * L_5 = V_0;
		BooleanDisposable_t3065601722 * L_6 = V_1;
		CancellationToken_t1439151560  L_7;
		memset(&L_7, 0, sizeof(L_7));
		CancellationToken__ctor_m1453920148(&L_7, (Il2CppObject *)L_6, /*hidden argument*/NULL);
		NullCheck((Func_3_t521084177 *)L_4);
		Il2CppObject * L_8 = ((  Il2CppObject * (*) (Func_3_t521084177 *, Il2CppObject*, CancellationToken_t1439151560 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)((Func_3_t521084177 *)L_4, (Il2CppObject*)L_5, (CancellationToken_t1439151560 )L_7, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		IL2CPP_RUNTIME_CLASS_INIT(MainThreadDispatcher_t4027217788_il2cpp_TypeInfo_var);
		MainThreadDispatcher_SendStartCoroutine_m2702036384(NULL /*static, unused*/, (Il2CppObject *)L_8, /*hidden argument*/NULL);
		BooleanDisposable_t3065601722 * L_9 = V_1;
		return L_9;
	}
}
// System.Void UniRx.Operators.FromCoroutine`1<System.Object>::.ctor(System.Func`3<UniRx.IObserver`1<T>,UniRx.CancellationToken,System.Collections.IEnumerator>)
extern "C"  void FromCoroutine_1__ctor_m1636771704_gshared (FromCoroutine_1_t514170085 * __this, Func_3_t1973235155 * ___coroutine0, const MethodInfo* method)
{
	{
		NullCheck((OperatorObservableBase_1_t4196218687 *)__this);
		((  void (*) (OperatorObservableBase_1_t4196218687 *, bool, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((OperatorObservableBase_1_t4196218687 *)__this, (bool)0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		Func_3_t1973235155 * L_0 = ___coroutine0;
		__this->set_coroutine_1(L_0);
		return;
	}
}
// System.IDisposable UniRx.Operators.FromCoroutine`1<System.Object>::SubscribeCore(UniRx.IObserver`1<T>,System.IDisposable)
extern Il2CppClass* BooleanDisposable_t3065601722_il2cpp_TypeInfo_var;
extern Il2CppClass* MainThreadDispatcher_t4027217788_il2cpp_TypeInfo_var;
extern const uint32_t FromCoroutine_1_SubscribeCore_m3803616771_MetadataUsageId;
extern "C"  Il2CppObject * FromCoroutine_1_SubscribeCore_m3803616771_gshared (FromCoroutine_1_t514170085 * __this, Il2CppObject* ___observer0, Il2CppObject * ___cancel1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (FromCoroutine_1_SubscribeCore_m3803616771_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	FromCoroutineObserver_t4015335165 * V_0 = NULL;
	BooleanDisposable_t3065601722 * V_1 = NULL;
	{
		Il2CppObject* L_0 = ___observer0;
		Il2CppObject * L_1 = ___cancel1;
		FromCoroutineObserver_t4015335165 * L_2 = (FromCoroutineObserver_t4015335165 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		((  void (*) (FromCoroutineObserver_t4015335165 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)(L_2, (Il2CppObject*)L_0, (Il2CppObject *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		V_0 = (FromCoroutineObserver_t4015335165 *)L_2;
		BooleanDisposable_t3065601722 * L_3 = (BooleanDisposable_t3065601722 *)il2cpp_codegen_object_new(BooleanDisposable_t3065601722_il2cpp_TypeInfo_var);
		BooleanDisposable__ctor_m2534881639(L_3, /*hidden argument*/NULL);
		V_1 = (BooleanDisposable_t3065601722 *)L_3;
		Func_3_t1973235155 * L_4 = (Func_3_t1973235155 *)__this->get_coroutine_1();
		FromCoroutineObserver_t4015335165 * L_5 = V_0;
		BooleanDisposable_t3065601722 * L_6 = V_1;
		CancellationToken_t1439151560  L_7;
		memset(&L_7, 0, sizeof(L_7));
		CancellationToken__ctor_m1453920148(&L_7, (Il2CppObject *)L_6, /*hidden argument*/NULL);
		NullCheck((Func_3_t1973235155 *)L_4);
		Il2CppObject * L_8 = ((  Il2CppObject * (*) (Func_3_t1973235155 *, Il2CppObject*, CancellationToken_t1439151560 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)((Func_3_t1973235155 *)L_4, (Il2CppObject*)L_5, (CancellationToken_t1439151560 )L_7, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		IL2CPP_RUNTIME_CLASS_INIT(MainThreadDispatcher_t4027217788_il2cpp_TypeInfo_var);
		MainThreadDispatcher_SendStartCoroutine_m2702036384(NULL /*static, unused*/, (Il2CppObject *)L_8, /*hidden argument*/NULL);
		BooleanDisposable_t3065601722 * L_9 = V_1;
		return L_9;
	}
}
// System.Void UniRx.Operators.FromCoroutine`1<UniRx.Unit>::.ctor(System.Func`3<UniRx.IObserver`1<T>,UniRx.CancellationToken,System.Collections.IEnumerator>)
extern "C"  void FromCoroutine_1__ctor_m4069244102_gshared (FromCoroutine_1_t2235349703 * __this, Func_3_t3458695013 * ___coroutine0, const MethodInfo* method)
{
	{
		NullCheck((OperatorObservableBase_1_t1622431009 *)__this);
		((  void (*) (OperatorObservableBase_1_t1622431009 *, bool, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((OperatorObservableBase_1_t1622431009 *)__this, (bool)0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		Func_3_t3458695013 * L_0 = ___coroutine0;
		__this->set_coroutine_1(L_0);
		return;
	}
}
// System.IDisposable UniRx.Operators.FromCoroutine`1<UniRx.Unit>::SubscribeCore(UniRx.IObserver`1<T>,System.IDisposable)
extern Il2CppClass* BooleanDisposable_t3065601722_il2cpp_TypeInfo_var;
extern Il2CppClass* MainThreadDispatcher_t4027217788_il2cpp_TypeInfo_var;
extern const uint32_t FromCoroutine_1_SubscribeCore_m1650235743_MetadataUsageId;
extern "C"  Il2CppObject * FromCoroutine_1_SubscribeCore_m1650235743_gshared (FromCoroutine_1_t2235349703 * __this, Il2CppObject* ___observer0, Il2CppObject * ___cancel1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (FromCoroutine_1_SubscribeCore_m1650235743_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	FromCoroutineObserver_t1441547487 * V_0 = NULL;
	BooleanDisposable_t3065601722 * V_1 = NULL;
	{
		Il2CppObject* L_0 = ___observer0;
		Il2CppObject * L_1 = ___cancel1;
		FromCoroutineObserver_t1441547487 * L_2 = (FromCoroutineObserver_t1441547487 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		((  void (*) (FromCoroutineObserver_t1441547487 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)(L_2, (Il2CppObject*)L_0, (Il2CppObject *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		V_0 = (FromCoroutineObserver_t1441547487 *)L_2;
		BooleanDisposable_t3065601722 * L_3 = (BooleanDisposable_t3065601722 *)il2cpp_codegen_object_new(BooleanDisposable_t3065601722_il2cpp_TypeInfo_var);
		BooleanDisposable__ctor_m2534881639(L_3, /*hidden argument*/NULL);
		V_1 = (BooleanDisposable_t3065601722 *)L_3;
		Func_3_t3458695013 * L_4 = (Func_3_t3458695013 *)__this->get_coroutine_1();
		FromCoroutineObserver_t1441547487 * L_5 = V_0;
		BooleanDisposable_t3065601722 * L_6 = V_1;
		CancellationToken_t1439151560  L_7;
		memset(&L_7, 0, sizeof(L_7));
		CancellationToken__ctor_m1453920148(&L_7, (Il2CppObject *)L_6, /*hidden argument*/NULL);
		NullCheck((Func_3_t3458695013 *)L_4);
		Il2CppObject * L_8 = ((  Il2CppObject * (*) (Func_3_t3458695013 *, Il2CppObject*, CancellationToken_t1439151560 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)((Func_3_t3458695013 *)L_4, (Il2CppObject*)L_5, (CancellationToken_t1439151560 )L_7, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		IL2CPP_RUNTIME_CLASS_INIT(MainThreadDispatcher_t4027217788_il2cpp_TypeInfo_var);
		MainThreadDispatcher_SendStartCoroutine_m2702036384(NULL /*static, unused*/, (Il2CppObject *)L_8, /*hidden argument*/NULL);
		BooleanDisposable_t3065601722 * L_9 = V_1;
		return L_9;
	}
}
// System.Void UniRx.Operators.FromEventObservable_`1/FromEvent<System.Int32>::.ctor(UniRx.Operators.FromEventObservable_`1<T>,UniRx.IObserver`1<T>)
extern "C"  void FromEvent__ctor_m3298522655_gshared (FromEvent_t706075128 * __this, FromEventObservable__1_t350522882 * ___parent0, Il2CppObject* ___observer1, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		FromEventObservable__1_t350522882 * L_0 = ___parent0;
		__this->set_parent_0(L_0);
		Il2CppObject* L_1 = ___observer1;
		__this->set_observer_1(L_1);
		IntPtr_t L_2;
		L_2.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		Action_1_t2995867492 * L_3 = (Action_1_t2995867492 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		((  void (*) (Action_1_t2995867492 *, Il2CppObject *, IntPtr_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)(L_3, (Il2CppObject *)__this, (IntPtr_t)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		__this->set_handler_2(L_3);
		return;
	}
}
// System.Boolean UniRx.Operators.FromEventObservable_`1/FromEvent<System.Int32>::Register()
extern Il2CppClass* Exception_t1967233988_il2cpp_TypeInfo_var;
extern const uint32_t FromEvent_Register_m2524226652_MetadataUsageId;
extern "C"  bool FromEvent_Register_m2524226652_gshared (FromEvent_t706075128 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (FromEvent_Register_m2524226652_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Exception_t1967233988 * V_0 = NULL;
	bool V_1 = false;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		FromEventObservable__1_t350522882 * L_0 = (FromEventObservable__1_t350522882 *)__this->get_parent_0();
		NullCheck(L_0);
		Action_1_t3144320197 * L_1 = (Action_1_t3144320197 *)L_0->get_addHandler_1();
		Action_1_t2995867492 * L_2 = (Action_1_t2995867492 *)__this->get_handler_2();
		NullCheck((Action_1_t3144320197 *)L_1);
		((  void (*) (Action_1_t3144320197 *, Action_1_t2995867492 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)((Action_1_t3144320197 *)L_1, (Action_1_t2995867492 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		goto IL_0034;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1967233988 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Exception_t1967233988_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_001b;
		throw e;
	}

CATCH_001b:
	{ // begin catch(System.Exception)
		{
			V_0 = (Exception_t1967233988 *)((Exception_t1967233988 *)__exception_local);
			Il2CppObject* L_3 = (Il2CppObject*)__this->get_observer_1();
			Exception_t1967233988 * L_4 = V_0;
			NullCheck((Il2CppObject*)L_3);
			InterfaceActionInvoker1< Exception_t1967233988 * >::Invoke(1 /* System.Void UniRx.IObserver`1<System.Int32>::OnError(System.Exception) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4), (Il2CppObject*)L_3, (Exception_t1967233988 *)L_4);
			V_1 = (bool)0;
			goto IL_0036;
		}

IL_002f:
		{
			; // IL_002f: leave IL_0034
		}
	} // end catch (depth: 1)

IL_0034:
	{
		return (bool)1;
	}

IL_0036:
	{
		bool L_5 = V_1;
		return L_5;
	}
}
// System.Void UniRx.Operators.FromEventObservable_`1/FromEvent<System.Int32>::OnNext(T)
extern "C"  void FromEvent_OnNext_m412581743_gshared (FromEvent_t706075128 * __this, int32_t ___value0, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = (Il2CppObject*)__this->get_observer_1();
		int32_t L_1 = ___value0;
		NullCheck((Il2CppObject*)L_0);
		InterfaceActionInvoker1< int32_t >::Invoke(2 /* System.Void UniRx.IObserver`1<System.Int32>::OnNext(T) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4), (Il2CppObject*)L_0, (int32_t)L_1);
		return;
	}
}
// System.Void UniRx.Operators.FromEventObservable_`1/FromEvent<System.Int32>::Dispose()
extern "C"  void FromEvent_Dispose_m1775128836_gshared (FromEvent_t706075128 * __this, const MethodInfo* method)
{
	{
		Action_1_t2995867492 * L_0 = (Action_1_t2995867492 *)__this->get_handler_2();
		if (!L_0)
		{
			goto IL_0028;
		}
	}
	{
		FromEventObservable__1_t350522882 * L_1 = (FromEventObservable__1_t350522882 *)__this->get_parent_0();
		NullCheck(L_1);
		Action_1_t3144320197 * L_2 = (Action_1_t3144320197 *)L_1->get_removeHandler_2();
		Action_1_t2995867492 * L_3 = (Action_1_t2995867492 *)__this->get_handler_2();
		NullCheck((Action_1_t3144320197 *)L_2);
		((  void (*) (Action_1_t3144320197 *, Action_1_t2995867492 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)((Action_1_t3144320197 *)L_2, (Action_1_t2995867492 *)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		__this->set_handler_2((Action_1_t2995867492 *)NULL);
	}

IL_0028:
	{
		return;
	}
}
// System.Void UniRx.Operators.FromEventObservable_`1/FromEvent<System.Object>::.ctor(UniRx.Operators.FromEventObservable_`1<T>,UniRx.IObserver`1<T>)
extern "C"  void FromEvent__ctor_m2649012072_gshared (FromEvent_t2990734058 * __this, FromEventObservable__1_t2635181811 * ___parent0, Il2CppObject* ___observer1, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		FromEventObservable__1_t2635181811 * L_0 = ___parent0;
		__this->set_parent_0(L_0);
		Il2CppObject* L_1 = ___observer1;
		__this->set_observer_1(L_1);
		IntPtr_t L_2;
		L_2.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		Action_1_t985559125 * L_3 = (Action_1_t985559125 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		((  void (*) (Action_1_t985559125 *, Il2CppObject *, IntPtr_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)(L_3, (Il2CppObject *)__this, (IntPtr_t)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		__this->set_handler_2(L_3);
		return;
	}
}
// System.Boolean UniRx.Operators.FromEventObservable_`1/FromEvent<System.Object>::Register()
extern Il2CppClass* Exception_t1967233988_il2cpp_TypeInfo_var;
extern const uint32_t FromEvent_Register_m736247547_MetadataUsageId;
extern "C"  bool FromEvent_Register_m736247547_gshared (FromEvent_t2990734058 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (FromEvent_Register_m736247547_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Exception_t1967233988 * V_0 = NULL;
	bool V_1 = false;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		FromEventObservable__1_t2635181811 * L_0 = (FromEventObservable__1_t2635181811 *)__this->get_parent_0();
		NullCheck(L_0);
		Action_1_t1134011830 * L_1 = (Action_1_t1134011830 *)L_0->get_addHandler_1();
		Action_1_t985559125 * L_2 = (Action_1_t985559125 *)__this->get_handler_2();
		NullCheck((Action_1_t1134011830 *)L_1);
		((  void (*) (Action_1_t1134011830 *, Action_1_t985559125 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)((Action_1_t1134011830 *)L_1, (Action_1_t985559125 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		goto IL_0034;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1967233988 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Exception_t1967233988_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_001b;
		throw e;
	}

CATCH_001b:
	{ // begin catch(System.Exception)
		{
			V_0 = (Exception_t1967233988 *)((Exception_t1967233988 *)__exception_local);
			Il2CppObject* L_3 = (Il2CppObject*)__this->get_observer_1();
			Exception_t1967233988 * L_4 = V_0;
			NullCheck((Il2CppObject*)L_3);
			InterfaceActionInvoker1< Exception_t1967233988 * >::Invoke(1 /* System.Void UniRx.IObserver`1<System.Object>::OnError(System.Exception) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4), (Il2CppObject*)L_3, (Exception_t1967233988 *)L_4);
			V_1 = (bool)0;
			goto IL_0036;
		}

IL_002f:
		{
			; // IL_002f: leave IL_0034
		}
	} // end catch (depth: 1)

IL_0034:
	{
		return (bool)1;
	}

IL_0036:
	{
		bool L_5 = V_1;
		return L_5;
	}
}
// System.Void UniRx.Operators.FromEventObservable_`1/FromEvent<System.Object>::OnNext(T)
extern "C"  void FromEvent_OnNext_m1374141496_gshared (FromEvent_t2990734058 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = (Il2CppObject*)__this->get_observer_1();
		Il2CppObject * L_1 = ___value0;
		NullCheck((Il2CppObject*)L_0);
		InterfaceActionInvoker1< Il2CppObject * >::Invoke(2 /* System.Void UniRx.IObserver`1<System.Object>::OnNext(T) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4), (Il2CppObject*)L_0, (Il2CppObject *)L_1);
		return;
	}
}
// System.Void UniRx.Operators.FromEventObservable_`1/FromEvent<System.Object>::Dispose()
extern "C"  void FromEvent_Dispose_m2736688589_gshared (FromEvent_t2990734058 * __this, const MethodInfo* method)
{
	{
		Action_1_t985559125 * L_0 = (Action_1_t985559125 *)__this->get_handler_2();
		if (!L_0)
		{
			goto IL_0028;
		}
	}
	{
		FromEventObservable__1_t2635181811 * L_1 = (FromEventObservable__1_t2635181811 *)__this->get_parent_0();
		NullCheck(L_1);
		Action_1_t1134011830 * L_2 = (Action_1_t1134011830 *)L_1->get_removeHandler_2();
		Action_1_t985559125 * L_3 = (Action_1_t985559125 *)__this->get_handler_2();
		NullCheck((Action_1_t1134011830 *)L_2);
		((  void (*) (Action_1_t1134011830 *, Action_1_t985559125 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)((Action_1_t1134011830 *)L_2, (Action_1_t985559125 *)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		__this->set_handler_2((Action_1_t985559125 *)NULL);
	}

IL_0028:
	{
		return;
	}
}
// System.Void UniRx.Operators.FromEventObservable_`1<System.Int32>::.ctor(System.Action`1<System.Action`1<T>>,System.Action`1<System.Action`1<T>>)
extern "C"  void FromEventObservable__1__ctor_m3339292550_gshared (FromEventObservable__1_t350522882 * __this, Action_1_t3144320197 * ___addHandler0, Action_1_t3144320197 * ___removeHandler1, const MethodInfo* method)
{
	{
		NullCheck((OperatorObservableBase_1_t1911559758 *)__this);
		((  void (*) (OperatorObservableBase_1_t1911559758 *, bool, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((OperatorObservableBase_1_t1911559758 *)__this, (bool)0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		Action_1_t3144320197 * L_0 = ___addHandler0;
		__this->set_addHandler_1(L_0);
		Action_1_t3144320197 * L_1 = ___removeHandler1;
		__this->set_removeHandler_2(L_1);
		return;
	}
}
// System.IDisposable UniRx.Operators.FromEventObservable_`1<System.Int32>::SubscribeCore(UniRx.IObserver`1<T>,System.IDisposable)
extern Il2CppClass* Disposable_t3736388786_il2cpp_TypeInfo_var;
extern const uint32_t FromEventObservable__1_SubscribeCore_m1012114774_MetadataUsageId;
extern "C"  Il2CppObject * FromEventObservable__1_SubscribeCore_m1012114774_gshared (FromEventObservable__1_t350522882 * __this, Il2CppObject* ___observer0, Il2CppObject * ___cancel1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (FromEventObservable__1_SubscribeCore_m1012114774_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	FromEvent_t706075128 * V_0 = NULL;
	Il2CppObject * V_1 = NULL;
	Il2CppObject * G_B3_0 = NULL;
	{
		Il2CppObject* L_0 = ___observer0;
		FromEvent_t706075128 * L_1 = (FromEvent_t706075128 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		((  void (*) (FromEvent_t706075128 *, FromEventObservable__1_t350522882 *, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)(L_1, (FromEventObservable__1_t350522882 *)__this, (Il2CppObject*)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		V_0 = (FromEvent_t706075128 *)L_1;
		FromEvent_t706075128 * L_2 = V_0;
		NullCheck((FromEvent_t706075128 *)L_2);
		bool L_3 = ((  bool (*) (FromEvent_t706075128 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)((FromEvent_t706075128 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		if (!L_3)
		{
			goto IL_001b;
		}
	}
	{
		FromEvent_t706075128 * L_4 = V_0;
		V_1 = (Il2CppObject *)L_4;
		Il2CppObject * L_5 = V_1;
		G_B3_0 = L_5;
		goto IL_0020;
	}

IL_001b:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Disposable_t3736388786_il2cpp_TypeInfo_var);
		Il2CppObject * L_6 = ((Disposable_t3736388786_StaticFields*)Disposable_t3736388786_il2cpp_TypeInfo_var->static_fields)->get_Empty_0();
		G_B3_0 = L_6;
	}

IL_0020:
	{
		return G_B3_0;
	}
}
// System.Void UniRx.Operators.FromEventObservable_`1<System.Object>::.ctor(System.Action`1<System.Action`1<T>>,System.Action`1<System.Action`1<T>>)
extern "C"  void FromEventObservable__1__ctor_m1630118911_gshared (FromEventObservable__1_t2635181811 * __this, Action_1_t1134011830 * ___addHandler0, Action_1_t1134011830 * ___removeHandler1, const MethodInfo* method)
{
	{
		NullCheck((OperatorObservableBase_1_t4196218687 *)__this);
		((  void (*) (OperatorObservableBase_1_t4196218687 *, bool, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((OperatorObservableBase_1_t4196218687 *)__this, (bool)0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		Action_1_t1134011830 * L_0 = ___addHandler0;
		__this->set_addHandler_1(L_0);
		Action_1_t1134011830 * L_1 = ___removeHandler1;
		__this->set_removeHandler_2(L_1);
		return;
	}
}
// System.IDisposable UniRx.Operators.FromEventObservable_`1<System.Object>::SubscribeCore(UniRx.IObserver`1<T>,System.IDisposable)
extern Il2CppClass* Disposable_t3736388786_il2cpp_TypeInfo_var;
extern const uint32_t FromEventObservable__1_SubscribeCore_m3156356793_MetadataUsageId;
extern "C"  Il2CppObject * FromEventObservable__1_SubscribeCore_m3156356793_gshared (FromEventObservable__1_t2635181811 * __this, Il2CppObject* ___observer0, Il2CppObject * ___cancel1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (FromEventObservable__1_SubscribeCore_m3156356793_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	FromEvent_t2990734058 * V_0 = NULL;
	Il2CppObject * V_1 = NULL;
	Il2CppObject * G_B3_0 = NULL;
	{
		Il2CppObject* L_0 = ___observer0;
		FromEvent_t2990734058 * L_1 = (FromEvent_t2990734058 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		((  void (*) (FromEvent_t2990734058 *, FromEventObservable__1_t2635181811 *, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)(L_1, (FromEventObservable__1_t2635181811 *)__this, (Il2CppObject*)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		V_0 = (FromEvent_t2990734058 *)L_1;
		FromEvent_t2990734058 * L_2 = V_0;
		NullCheck((FromEvent_t2990734058 *)L_2);
		bool L_3 = ((  bool (*) (FromEvent_t2990734058 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)((FromEvent_t2990734058 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		if (!L_3)
		{
			goto IL_001b;
		}
	}
	{
		FromEvent_t2990734058 * L_4 = V_0;
		V_1 = (Il2CppObject *)L_4;
		Il2CppObject * L_5 = V_1;
		G_B3_0 = L_5;
		goto IL_0020;
	}

IL_001b:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Disposable_t3736388786_il2cpp_TypeInfo_var);
		Il2CppObject * L_6 = ((Disposable_t3736388786_StaticFields*)Disposable_t3736388786_il2cpp_TypeInfo_var->static_fields)->get_Empty_0();
		G_B3_0 = L_6;
	}

IL_0020:
	{
		return G_B3_0;
	}
}
// System.Void UniRx.Operators.FromEventObservable`1/FromEvent<System.Object>::.ctor(UniRx.Operators.FromEventObservable`1<TDelegate>,UniRx.IObserver`1<UniRx.Unit>)
extern "C"  void FromEvent__ctor_m3529457115_gshared (FromEvent_t2990734057 * __this, FromEventObservable_1_t2790472066 * ___parent0, Il2CppObject* ___observer1, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		FromEventObservable_1_t2790472066 * L_0 = ___parent0;
		__this->set_parent_0(L_0);
		Il2CppObject* L_1 = ___observer1;
		__this->set_observer_1(L_1);
		return;
	}
}
// System.Boolean UniRx.Operators.FromEventObservable`1/FromEvent<System.Object>::Register()
extern Il2CppClass* Action_t437523947_il2cpp_TypeInfo_var;
extern Il2CppClass* Exception_t1967233988_il2cpp_TypeInfo_var;
extern Il2CppClass* IObserver_1_t475317645_il2cpp_TypeInfo_var;
extern const uint32_t FromEvent_Register_m4289606306_MetadataUsageId;
extern "C"  bool FromEvent_Register_m4289606306_gshared (FromEvent_t2990734057 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (FromEvent_Register_m4289606306_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Exception_t1967233988 * V_0 = NULL;
	bool V_1 = false;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		FromEventObservable_1_t2790472066 * L_0 = (FromEventObservable_1_t2790472066 *)__this->get_parent_0();
		NullCheck(L_0);
		Func_2_t2670270213 * L_1 = (Func_2_t2670270213 *)L_0->get_conversion_1();
		IntPtr_t L_2;
		L_2.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		Action_t437523947 * L_3 = (Action_t437523947 *)il2cpp_codegen_object_new(Action_t437523947_il2cpp_TypeInfo_var);
		Action__ctor_m2957240604(L_3, (Il2CppObject *)__this, (IntPtr_t)L_2, /*hidden argument*/NULL);
		NullCheck((Func_2_t2670270213 *)L_1);
		Il2CppObject * L_4 = ((  Il2CppObject * (*) (Func_2_t2670270213 *, Action_t437523947 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)((Func_2_t2670270213 *)L_1, (Action_t437523947 *)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		__this->set_handler_2(L_4);
	}

IL_0022:
	try
	{ // begin try (depth: 1)
		FromEventObservable_1_t2790472066 * L_5 = (FromEventObservable_1_t2790472066 *)__this->get_parent_0();
		NullCheck(L_5);
		Action_1_t985559125 * L_6 = (Action_1_t985559125 *)L_5->get_addHandler_2();
		Il2CppObject * L_7 = (Il2CppObject *)__this->get_handler_2();
		NullCheck((Action_1_t985559125 *)L_6);
		((  void (*) (Action_1_t985559125 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)((Action_1_t985559125 *)L_6, (Il2CppObject *)L_7, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		goto IL_0056;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1967233988 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Exception_t1967233988_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_003d;
		throw e;
	}

CATCH_003d:
	{ // begin catch(System.Exception)
		{
			V_0 = (Exception_t1967233988 *)((Exception_t1967233988 *)__exception_local);
			Il2CppObject* L_8 = (Il2CppObject*)__this->get_observer_1();
			Exception_t1967233988 * L_9 = V_0;
			NullCheck((Il2CppObject*)L_8);
			InterfaceActionInvoker1< Exception_t1967233988 * >::Invoke(1 /* System.Void UniRx.IObserver`1<UniRx.Unit>::OnError(System.Exception) */, IObserver_1_t475317645_il2cpp_TypeInfo_var, (Il2CppObject*)L_8, (Exception_t1967233988 *)L_9);
			V_1 = (bool)0;
			goto IL_0058;
		}

IL_0051:
		{
			; // IL_0051: leave IL_0056
		}
	} // end catch (depth: 1)

IL_0056:
	{
		return (bool)1;
	}

IL_0058:
	{
		bool L_10 = V_1;
		return L_10;
	}
}
// System.Void UniRx.Operators.FromEventObservable`1/FromEvent<System.Object>::OnNext()
extern Il2CppClass* Unit_t2558286038_il2cpp_TypeInfo_var;
extern Il2CppClass* IObserver_1_t475317645_il2cpp_TypeInfo_var;
extern const uint32_t FromEvent_OnNext_m2066540437_MetadataUsageId;
extern "C"  void FromEvent_OnNext_m2066540437_gshared (FromEvent_t2990734057 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (FromEvent_OnNext_m2066540437_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject* L_0 = (Il2CppObject*)__this->get_observer_1();
		IL2CPP_RUNTIME_CLASS_INIT(Unit_t2558286038_il2cpp_TypeInfo_var);
		Unit_t2558286038  L_1 = Unit_get_Default_m3654553472(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck((Il2CppObject*)L_0);
		InterfaceActionInvoker1< Unit_t2558286038  >::Invoke(2 /* System.Void UniRx.IObserver`1<UniRx.Unit>::OnNext(T) */, IObserver_1_t475317645_il2cpp_TypeInfo_var, (Il2CppObject*)L_0, (Unit_t2558286038 )L_1);
		return;
	}
}
// System.Void UniRx.Operators.FromEventObservable`1/FromEvent<System.Object>::Dispose()
extern Il2CppClass* Il2CppObject_il2cpp_TypeInfo_var;
extern const uint32_t FromEvent_Dispose_m1000792574_MetadataUsageId;
extern "C"  void FromEvent_Dispose_m1000792574_gshared (FromEvent_t2990734057 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (FromEvent_Dispose_m1000792574_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Il2CppObject * V_0 = NULL;
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_handler_2();
		if (!L_0)
		{
			goto IL_0035;
		}
	}
	{
		FromEventObservable_1_t2790472066 * L_1 = (FromEventObservable_1_t2790472066 *)__this->get_parent_0();
		NullCheck(L_1);
		Action_1_t985559125 * L_2 = (Action_1_t985559125 *)L_1->get_removeHandler_3();
		Il2CppObject * L_3 = (Il2CppObject *)__this->get_handler_2();
		NullCheck((Action_1_t985559125 *)L_2);
		((  void (*) (Action_1_t985559125 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)((Action_1_t985559125 *)L_2, (Il2CppObject *)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		Initobj (Il2CppObject_il2cpp_TypeInfo_var, (&V_0));
		Il2CppObject * L_4 = V_0;
		__this->set_handler_2(L_4);
	}

IL_0035:
	{
		return;
	}
}
// System.Void UniRx.Operators.FromEventObservable`1<System.Object>::.ctor(System.Func`2<System.Action,TDelegate>,System.Action`1<TDelegate>,System.Action`1<TDelegate>)
extern const MethodInfo* OperatorObservableBase_1__ctor_m284357152_MethodInfo_var;
extern const uint32_t FromEventObservable_1__ctor_m1522889437_MetadataUsageId;
extern "C"  void FromEventObservable_1__ctor_m1522889437_gshared (FromEventObservable_1_t2790472066 * __this, Func_2_t2670270213 * ___conversion0, Action_1_t985559125 * ___addHandler1, Action_1_t985559125 * ___removeHandler2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (FromEventObservable_1__ctor_m1522889437_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NullCheck((OperatorObservableBase_1_t1622431009 *)__this);
		OperatorObservableBase_1__ctor_m284357152((OperatorObservableBase_1_t1622431009 *)__this, (bool)0, /*hidden argument*/OperatorObservableBase_1__ctor_m284357152_MethodInfo_var);
		Func_2_t2670270213 * L_0 = ___conversion0;
		__this->set_conversion_1(L_0);
		Action_1_t985559125 * L_1 = ___addHandler1;
		__this->set_addHandler_2(L_1);
		Action_1_t985559125 * L_2 = ___removeHandler2;
		__this->set_removeHandler_3(L_2);
		return;
	}
}
// System.IDisposable UniRx.Operators.FromEventObservable`1<System.Object>::SubscribeCore(UniRx.IObserver`1<UniRx.Unit>,System.IDisposable)
extern Il2CppClass* Disposable_t3736388786_il2cpp_TypeInfo_var;
extern const uint32_t FromEventObservable_1_SubscribeCore_m3939969428_MetadataUsageId;
extern "C"  Il2CppObject * FromEventObservable_1_SubscribeCore_m3939969428_gshared (FromEventObservable_1_t2790472066 * __this, Il2CppObject* ___observer0, Il2CppObject * ___cancel1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (FromEventObservable_1_SubscribeCore_m3939969428_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	FromEvent_t2990734057 * V_0 = NULL;
	Il2CppObject * V_1 = NULL;
	Il2CppObject * G_B3_0 = NULL;
	{
		Il2CppObject* L_0 = ___observer0;
		FromEvent_t2990734057 * L_1 = (FromEvent_t2990734057 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		((  void (*) (FromEvent_t2990734057 *, FromEventObservable_1_t2790472066 *, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)(L_1, (FromEventObservable_1_t2790472066 *)__this, (Il2CppObject*)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		V_0 = (FromEvent_t2990734057 *)L_1;
		FromEvent_t2990734057 * L_2 = V_0;
		NullCheck((FromEvent_t2990734057 *)L_2);
		bool L_3 = ((  bool (*) (FromEvent_t2990734057 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)((FromEvent_t2990734057 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		if (!L_3)
		{
			goto IL_001b;
		}
	}
	{
		FromEvent_t2990734057 * L_4 = V_0;
		V_1 = (Il2CppObject *)L_4;
		Il2CppObject * L_5 = V_1;
		G_B3_0 = L_5;
		goto IL_0020;
	}

IL_001b:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Disposable_t3736388786_il2cpp_TypeInfo_var);
		Il2CppObject * L_6 = ((Disposable_t3736388786_StaticFields*)Disposable_t3736388786_il2cpp_TypeInfo_var->static_fields)->get_Empty_0();
		G_B3_0 = L_6;
	}

IL_0020:
	{
		return G_B3_0;
	}
}
// System.Void UniRx.Operators.FromEventObservable`2/FromEvent<System.Object,System.Boolean>::.ctor(UniRx.Operators.FromEventObservable`2<TDelegate,TEventArgs>,UniRx.IObserver`1<TEventArgs>)
extern "C"  void FromEvent__ctor_m3420522768_gshared (FromEvent_t1915864816 * __this, FromEventObservable_2_t1626038518 * ___parent0, Il2CppObject* ___observer1, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		FromEventObservable_2_t1626038518 * L_0 = ___parent0;
		__this->set_parent_0(L_0);
		Il2CppObject* L_1 = ___observer1;
		__this->set_observer_1(L_1);
		return;
	}
}
// System.Boolean UniRx.Operators.FromEventObservable`2/FromEvent<System.Object,System.Boolean>::Register()
extern Il2CppClass* Exception_t1967233988_il2cpp_TypeInfo_var;
extern const uint32_t FromEvent_Register_m3962354030_MetadataUsageId;
extern "C"  bool FromEvent_Register_m3962354030_gshared (FromEvent_t1915864816 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (FromEvent_Register_m3962354030_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Exception_t1967233988 * V_0 = NULL;
	bool V_1 = false;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		FromEventObservable_2_t1626038518 * L_0 = (FromEventObservable_2_t1626038518 *)__this->get_parent_0();
		NullCheck(L_0);
		Func_2_t599409222 * L_1 = (Func_2_t599409222 *)L_0->get_conversion_1();
		IntPtr_t L_2;
		L_2.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		Action_1_t359458046 * L_3 = (Action_1_t359458046 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		((  void (*) (Action_1_t359458046 *, Il2CppObject *, IntPtr_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)(L_3, (Il2CppObject *)__this, (IntPtr_t)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		NullCheck((Func_2_t599409222 *)L_1);
		Il2CppObject * L_4 = ((  Il2CppObject * (*) (Func_2_t599409222 *, Action_1_t359458046 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)((Func_2_t599409222 *)L_1, (Action_1_t359458046 *)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		__this->set_handler_2(L_4);
	}

IL_0022:
	try
	{ // begin try (depth: 1)
		FromEventObservable_2_t1626038518 * L_5 = (FromEventObservable_2_t1626038518 *)__this->get_parent_0();
		NullCheck(L_5);
		Action_1_t985559125 * L_6 = (Action_1_t985559125 *)L_5->get_addHandler_2();
		Il2CppObject * L_7 = (Il2CppObject *)__this->get_handler_2();
		NullCheck((Action_1_t985559125 *)L_6);
		((  void (*) (Action_1_t985559125 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)((Action_1_t985559125 *)L_6, (Il2CppObject *)L_7, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		goto IL_0056;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1967233988 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Exception_t1967233988_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_003d;
		throw e;
	}

CATCH_003d:
	{ // begin catch(System.Exception)
		{
			V_0 = (Exception_t1967233988 *)((Exception_t1967233988 *)__exception_local);
			Il2CppObject* L_8 = (Il2CppObject*)__this->get_observer_1();
			Exception_t1967233988 * L_9 = V_0;
			NullCheck((Il2CppObject*)L_8);
			InterfaceActionInvoker1< Exception_t1967233988 * >::Invoke(1 /* System.Void UniRx.IObserver`1<System.Boolean>::OnError(System.Exception) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5), (Il2CppObject*)L_8, (Exception_t1967233988 *)L_9);
			V_1 = (bool)0;
			goto IL_0058;
		}

IL_0051:
		{
			; // IL_0051: leave IL_0056
		}
	} // end catch (depth: 1)

IL_0056:
	{
		return (bool)1;
	}

IL_0058:
	{
		bool L_10 = V_1;
		return L_10;
	}
}
// System.Void UniRx.Operators.FromEventObservable`2/FromEvent<System.Object,System.Boolean>::OnNext(TEventArgs)
extern "C"  void FromEvent_OnNext_m2753720278_gshared (FromEvent_t1915864816 * __this, bool ___args0, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = (Il2CppObject*)__this->get_observer_1();
		bool L_1 = ___args0;
		NullCheck((Il2CppObject*)L_0);
		InterfaceActionInvoker1< bool >::Invoke(2 /* System.Void UniRx.IObserver`1<System.Boolean>::OnNext(T) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5), (Il2CppObject*)L_0, (bool)L_1);
		return;
	}
}
// System.Void UniRx.Operators.FromEventObservable`2/FromEvent<System.Object,System.Boolean>::Dispose()
extern Il2CppClass* Il2CppObject_il2cpp_TypeInfo_var;
extern const uint32_t FromEvent_Dispose_m4210440698_MetadataUsageId;
extern "C"  void FromEvent_Dispose_m4210440698_gshared (FromEvent_t1915864816 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (FromEvent_Dispose_m4210440698_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Il2CppObject * V_0 = NULL;
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_handler_2();
		if (!L_0)
		{
			goto IL_0035;
		}
	}
	{
		FromEventObservable_2_t1626038518 * L_1 = (FromEventObservable_2_t1626038518 *)__this->get_parent_0();
		NullCheck(L_1);
		Action_1_t985559125 * L_2 = (Action_1_t985559125 *)L_1->get_removeHandler_3();
		Il2CppObject * L_3 = (Il2CppObject *)__this->get_handler_2();
		NullCheck((Action_1_t985559125 *)L_2);
		((  void (*) (Action_1_t985559125 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)((Action_1_t985559125 *)L_2, (Il2CppObject *)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		Initobj (Il2CppObject_il2cpp_TypeInfo_var, (&V_0));
		Il2CppObject * L_4 = V_0;
		__this->set_handler_2(L_4);
	}

IL_0035:
	{
		return;
	}
}
// System.Void UniRx.Operators.FromEventObservable`2/FromEvent<System.Object,System.Object>::.ctor(UniRx.Operators.FromEventObservable`2<TDelegate,TEventArgs>,UniRx.IObserver`1<TEventArgs>)
extern "C"  void FromEvent__ctor_m1718772281_gshared (FromEvent_t2541965895 * __this, FromEventObservable_2_t2252139597 * ___parent0, Il2CppObject* ___observer1, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		FromEventObservable_2_t2252139597 * L_0 = ___parent0;
		__this->set_parent_0(L_0);
		Il2CppObject* L_1 = ___observer1;
		__this->set_observer_1(L_1);
		return;
	}
}
// System.Boolean UniRx.Operators.FromEventObservable`2/FromEvent<System.Object,System.Object>::Register()
extern Il2CppClass* Exception_t1967233988_il2cpp_TypeInfo_var;
extern const uint32_t FromEvent_Register_m1129938703_MetadataUsageId;
extern "C"  bool FromEvent_Register_m1129938703_gshared (FromEvent_t2541965895 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (FromEvent_Register_m1129938703_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Exception_t1967233988 * V_0 = NULL;
	bool V_1 = false;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		FromEventObservable_2_t2252139597 * L_0 = (FromEventObservable_2_t2252139597 *)__this->get_parent_0();
		NullCheck(L_0);
		Func_2_t1100972979 * L_1 = (Func_2_t1100972979 *)L_0->get_conversion_1();
		IntPtr_t L_2;
		L_2.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		Action_1_t985559125 * L_3 = (Action_1_t985559125 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		((  void (*) (Action_1_t985559125 *, Il2CppObject *, IntPtr_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)(L_3, (Il2CppObject *)__this, (IntPtr_t)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		NullCheck((Func_2_t1100972979 *)L_1);
		Il2CppObject * L_4 = ((  Il2CppObject * (*) (Func_2_t1100972979 *, Action_1_t985559125 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)((Func_2_t1100972979 *)L_1, (Action_1_t985559125 *)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		__this->set_handler_2(L_4);
	}

IL_0022:
	try
	{ // begin try (depth: 1)
		FromEventObservable_2_t2252139597 * L_5 = (FromEventObservable_2_t2252139597 *)__this->get_parent_0();
		NullCheck(L_5);
		Action_1_t985559125 * L_6 = (Action_1_t985559125 *)L_5->get_addHandler_2();
		Il2CppObject * L_7 = (Il2CppObject *)__this->get_handler_2();
		NullCheck((Action_1_t985559125 *)L_6);
		((  void (*) (Action_1_t985559125 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)((Action_1_t985559125 *)L_6, (Il2CppObject *)L_7, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		goto IL_0056;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1967233988 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Exception_t1967233988_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_003d;
		throw e;
	}

CATCH_003d:
	{ // begin catch(System.Exception)
		{
			V_0 = (Exception_t1967233988 *)((Exception_t1967233988 *)__exception_local);
			Il2CppObject* L_8 = (Il2CppObject*)__this->get_observer_1();
			Exception_t1967233988 * L_9 = V_0;
			NullCheck((Il2CppObject*)L_8);
			InterfaceActionInvoker1< Exception_t1967233988 * >::Invoke(1 /* System.Void UniRx.IObserver`1<System.Object>::OnError(System.Exception) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5), (Il2CppObject*)L_8, (Exception_t1967233988 *)L_9);
			V_1 = (bool)0;
			goto IL_0058;
		}

IL_0051:
		{
			; // IL_0051: leave IL_0056
		}
	} // end catch (depth: 1)

IL_0056:
	{
		return (bool)1;
	}

IL_0058:
	{
		bool L_10 = V_1;
		return L_10;
	}
}
// System.Void UniRx.Operators.FromEventObservable`2/FromEvent<System.Object,System.Object>::OnNext(TEventArgs)
extern "C"  void FromEvent_OnNext_m1896537279_gshared (FromEvent_t2541965895 * __this, Il2CppObject * ___args0, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = (Il2CppObject*)__this->get_observer_1();
		Il2CppObject * L_1 = ___args0;
		NullCheck((Il2CppObject*)L_0);
		InterfaceActionInvoker1< Il2CppObject * >::Invoke(2 /* System.Void UniRx.IObserver`1<System.Object>::OnNext(T) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5), (Il2CppObject*)L_0, (Il2CppObject *)L_1);
		return;
	}
}
// System.Void UniRx.Operators.FromEventObservable`2/FromEvent<System.Object,System.Object>::Dispose()
extern Il2CppClass* Il2CppObject_il2cpp_TypeInfo_var;
extern const uint32_t FromEvent_Dispose_m1014559473_MetadataUsageId;
extern "C"  void FromEvent_Dispose_m1014559473_gshared (FromEvent_t2541965895 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (FromEvent_Dispose_m1014559473_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Il2CppObject * V_0 = NULL;
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_handler_2();
		if (!L_0)
		{
			goto IL_0035;
		}
	}
	{
		FromEventObservable_2_t2252139597 * L_1 = (FromEventObservable_2_t2252139597 *)__this->get_parent_0();
		NullCheck(L_1);
		Action_1_t985559125 * L_2 = (Action_1_t985559125 *)L_1->get_removeHandler_3();
		Il2CppObject * L_3 = (Il2CppObject *)__this->get_handler_2();
		NullCheck((Action_1_t985559125 *)L_2);
		((  void (*) (Action_1_t985559125 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)((Action_1_t985559125 *)L_2, (Il2CppObject *)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		Initobj (Il2CppObject_il2cpp_TypeInfo_var, (&V_0));
		Il2CppObject * L_4 = V_0;
		__this->set_handler_2(L_4);
	}

IL_0035:
	{
		return;
	}
}
// System.Void UniRx.Operators.FromEventObservable`2/FromEvent<System.Object,System.Single>::.ctor(UniRx.Operators.FromEventObservable`2<TDelegate,TEventArgs>,UniRx.IObserver`1<TEventArgs>)
extern "C"  void FromEvent__ctor_m2246157456_gshared (FromEvent_t2663068496 * __this, FromEventObservable_2_t2373242198 * ___parent0, Il2CppObject* ___observer1, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		FromEventObservable_2_t2373242198 * L_0 = ___parent0;
		__this->set_parent_0(L_0);
		Il2CppObject* L_1 = ___observer1;
		__this->set_observer_1(L_1);
		return;
	}
}
// System.Boolean UniRx.Operators.FromEventObservable`2/FromEvent<System.Object,System.Single>::Register()
extern Il2CppClass* Exception_t1967233988_il2cpp_TypeInfo_var;
extern const uint32_t FromEvent_Register_m3431158950_MetadataUsageId;
extern "C"  bool FromEvent_Register_m3431158950_gshared (FromEvent_t2663068496 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (FromEvent_Register_m3431158950_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Exception_t1967233988 * V_0 = NULL;
	bool V_1 = false;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		FromEventObservable_2_t2373242198 * L_0 = (FromEventObservable_2_t2373242198 *)__this->get_parent_0();
		NullCheck(L_0);
		Func_2_t2308880486 * L_1 = (Func_2_t2308880486 *)L_0->get_conversion_1();
		IntPtr_t L_2;
		L_2.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		Action_1_t1106661726 * L_3 = (Action_1_t1106661726 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		((  void (*) (Action_1_t1106661726 *, Il2CppObject *, IntPtr_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)(L_3, (Il2CppObject *)__this, (IntPtr_t)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		NullCheck((Func_2_t2308880486 *)L_1);
		Il2CppObject * L_4 = ((  Il2CppObject * (*) (Func_2_t2308880486 *, Action_1_t1106661726 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)((Func_2_t2308880486 *)L_1, (Action_1_t1106661726 *)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		__this->set_handler_2(L_4);
	}

IL_0022:
	try
	{ // begin try (depth: 1)
		FromEventObservable_2_t2373242198 * L_5 = (FromEventObservable_2_t2373242198 *)__this->get_parent_0();
		NullCheck(L_5);
		Action_1_t985559125 * L_6 = (Action_1_t985559125 *)L_5->get_addHandler_2();
		Il2CppObject * L_7 = (Il2CppObject *)__this->get_handler_2();
		NullCheck((Action_1_t985559125 *)L_6);
		((  void (*) (Action_1_t985559125 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)((Action_1_t985559125 *)L_6, (Il2CppObject *)L_7, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		goto IL_0056;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1967233988 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Exception_t1967233988_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_003d;
		throw e;
	}

CATCH_003d:
	{ // begin catch(System.Exception)
		{
			V_0 = (Exception_t1967233988 *)((Exception_t1967233988 *)__exception_local);
			Il2CppObject* L_8 = (Il2CppObject*)__this->get_observer_1();
			Exception_t1967233988 * L_9 = V_0;
			NullCheck((Il2CppObject*)L_8);
			InterfaceActionInvoker1< Exception_t1967233988 * >::Invoke(1 /* System.Void UniRx.IObserver`1<System.Single>::OnError(System.Exception) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5), (Il2CppObject*)L_8, (Exception_t1967233988 *)L_9);
			V_1 = (bool)0;
			goto IL_0058;
		}

IL_0051:
		{
			; // IL_0051: leave IL_0056
		}
	} // end catch (depth: 1)

IL_0056:
	{
		return (bool)1;
	}

IL_0058:
	{
		bool L_10 = V_1;
		return L_10;
	}
}
// System.Void UniRx.Operators.FromEventObservable`2/FromEvent<System.Object,System.Single>::OnNext(TEventArgs)
extern "C"  void FromEvent_OnNext_m636300118_gshared (FromEvent_t2663068496 * __this, float ___args0, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = (Il2CppObject*)__this->get_observer_1();
		float L_1 = ___args0;
		NullCheck((Il2CppObject*)L_0);
		InterfaceActionInvoker1< float >::Invoke(2 /* System.Void UniRx.IObserver`1<System.Single>::OnNext(T) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5), (Il2CppObject*)L_0, (float)L_1);
		return;
	}
}
// System.Void UniRx.Operators.FromEventObservable`2/FromEvent<System.Object,System.Single>::Dispose()
extern Il2CppClass* Il2CppObject_il2cpp_TypeInfo_var;
extern const uint32_t FromEvent_Dispose_m3028455034_MetadataUsageId;
extern "C"  void FromEvent_Dispose_m3028455034_gshared (FromEvent_t2663068496 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (FromEvent_Dispose_m3028455034_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Il2CppObject * V_0 = NULL;
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_handler_2();
		if (!L_0)
		{
			goto IL_0035;
		}
	}
	{
		FromEventObservable_2_t2373242198 * L_1 = (FromEventObservable_2_t2373242198 *)__this->get_parent_0();
		NullCheck(L_1);
		Action_1_t985559125 * L_2 = (Action_1_t985559125 *)L_1->get_removeHandler_3();
		Il2CppObject * L_3 = (Il2CppObject *)__this->get_handler_2();
		NullCheck((Action_1_t985559125 *)L_2);
		((  void (*) (Action_1_t985559125 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)((Action_1_t985559125 *)L_2, (Il2CppObject *)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		Initobj (Il2CppObject_il2cpp_TypeInfo_var, (&V_0));
		Il2CppObject * L_4 = V_0;
		__this->set_handler_2(L_4);
	}

IL_0035:
	{
		return;
	}
}
// System.Void UniRx.Operators.FromEventObservable`2/FromEvent<System.Object,UniRx.Tuple`2<System.Object,System.Object>>::.ctor(UniRx.Operators.FromEventObservable`2<TDelegate,TEventArgs>,UniRx.IObserver`1<TEventArgs>)
extern "C"  void FromEvent__ctor_m3905763889_gshared (FromEvent_t2074121294 * __this, FromEventObservable_2_t1784294996 * ___parent0, Il2CppObject* ___observer1, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		FromEventObservable_2_t1784294996 * L_0 = ___parent0;
		__this->set_parent_0(L_0);
		Il2CppObject* L_1 = ___observer1;
		__this->set_observer_1(L_1);
		return;
	}
}
// System.Boolean UniRx.Operators.FromEventObservable`2/FromEvent<System.Object,UniRx.Tuple`2<System.Object,System.Object>>::Register()
extern Il2CppClass* Exception_t1967233988_il2cpp_TypeInfo_var;
extern const uint32_t FromEvent_Register_m2788819791_MetadataUsageId;
extern "C"  bool FromEvent_Register_m2788819791_gshared (FromEvent_t2074121294 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (FromEvent_Register_m2788819791_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Exception_t1967233988 * V_0 = NULL;
	bool V_1 = false;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		FromEventObservable_2_t1784294996 * L_0 = (FromEventObservable_2_t1784294996 *)__this->get_parent_0();
		NullCheck(L_0);
		Func_2_t692415920 * L_1 = (Func_2_t692415920 *)L_0->get_conversion_1();
		IntPtr_t L_2;
		L_2.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		Action_1_t517714524 * L_3 = (Action_1_t517714524 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		((  void (*) (Action_1_t517714524 *, Il2CppObject *, IntPtr_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)(L_3, (Il2CppObject *)__this, (IntPtr_t)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		NullCheck((Func_2_t692415920 *)L_1);
		Il2CppObject * L_4 = ((  Il2CppObject * (*) (Func_2_t692415920 *, Action_1_t517714524 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)((Func_2_t692415920 *)L_1, (Action_1_t517714524 *)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		__this->set_handler_2(L_4);
	}

IL_0022:
	try
	{ // begin try (depth: 1)
		FromEventObservable_2_t1784294996 * L_5 = (FromEventObservable_2_t1784294996 *)__this->get_parent_0();
		NullCheck(L_5);
		Action_1_t985559125 * L_6 = (Action_1_t985559125 *)L_5->get_addHandler_2();
		Il2CppObject * L_7 = (Il2CppObject *)__this->get_handler_2();
		NullCheck((Action_1_t985559125 *)L_6);
		((  void (*) (Action_1_t985559125 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)((Action_1_t985559125 *)L_6, (Il2CppObject *)L_7, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		goto IL_0056;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1967233988 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Exception_t1967233988_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_003d;
		throw e;
	}

CATCH_003d:
	{ // begin catch(System.Exception)
		{
			V_0 = (Exception_t1967233988 *)((Exception_t1967233988 *)__exception_local);
			Il2CppObject* L_8 = (Il2CppObject*)__this->get_observer_1();
			Exception_t1967233988 * L_9 = V_0;
			NullCheck((Il2CppObject*)L_8);
			InterfaceActionInvoker1< Exception_t1967233988 * >::Invoke(1 /* System.Void UniRx.IObserver`1<UniRx.Tuple`2<System.Object,System.Object>>::OnError(System.Exception) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5), (Il2CppObject*)L_8, (Exception_t1967233988 *)L_9);
			V_1 = (bool)0;
			goto IL_0058;
		}

IL_0051:
		{
			; // IL_0051: leave IL_0056
		}
	} // end catch (depth: 1)

IL_0056:
	{
		return (bool)1;
	}

IL_0058:
	{
		bool L_10 = V_1;
		return L_10;
	}
}
// System.Void UniRx.Operators.FromEventObservable`2/FromEvent<System.Object,UniRx.Tuple`2<System.Object,System.Object>>::OnNext(TEventArgs)
extern "C"  void FromEvent_OnNext_m1816020663_gshared (FromEvent_t2074121294 * __this, Tuple_2_t369261819  ___args0, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = (Il2CppObject*)__this->get_observer_1();
		Tuple_2_t369261819  L_1 = ___args0;
		NullCheck((Il2CppObject*)L_0);
		InterfaceActionInvoker1< Tuple_2_t369261819  >::Invoke(2 /* System.Void UniRx.IObserver`1<UniRx.Tuple`2<System.Object,System.Object>>::OnNext(T) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5), (Il2CppObject*)L_0, (Tuple_2_t369261819 )L_1);
		return;
	}
}
// System.Void UniRx.Operators.FromEventObservable`2/FromEvent<System.Object,UniRx.Tuple`2<System.Object,System.Object>>::Dispose()
extern Il2CppClass* Il2CppObject_il2cpp_TypeInfo_var;
extern const uint32_t FromEvent_Dispose_m3791647225_MetadataUsageId;
extern "C"  void FromEvent_Dispose_m3791647225_gshared (FromEvent_t2074121294 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (FromEvent_Dispose_m3791647225_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Il2CppObject * V_0 = NULL;
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_handler_2();
		if (!L_0)
		{
			goto IL_0035;
		}
	}
	{
		FromEventObservable_2_t1784294996 * L_1 = (FromEventObservable_2_t1784294996 *)__this->get_parent_0();
		NullCheck(L_1);
		Action_1_t985559125 * L_2 = (Action_1_t985559125 *)L_1->get_removeHandler_3();
		Il2CppObject * L_3 = (Il2CppObject *)__this->get_handler_2();
		NullCheck((Action_1_t985559125 *)L_2);
		((  void (*) (Action_1_t985559125 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)((Action_1_t985559125 *)L_2, (Il2CppObject *)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		Initobj (Il2CppObject_il2cpp_TypeInfo_var, (&V_0));
		Il2CppObject * L_4 = V_0;
		__this->set_handler_2(L_4);
	}

IL_0035:
	{
		return;
	}
}
// System.Void UniRx.Operators.FromEventObservable`2/FromEvent<System.Object,UniRx.Tuple`3<System.Object,System.Object,System.Object>>::.ctor(UniRx.Operators.FromEventObservable`2<TDelegate,TEventArgs>,UniRx.IObserver`1<TEventArgs>)
extern "C"  void FromEvent__ctor_m367008356_gshared (FromEvent_t3355758577 * __this, FromEventObservable_2_t3065932279 * ___parent0, Il2CppObject* ___observer1, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		FromEventObservable_2_t3065932279 * L_0 = ___parent0;
		__this->set_parent_0(L_0);
		Il2CppObject* L_1 = ___observer1;
		__this->set_observer_1(L_1);
		return;
	}
}
// System.Boolean UniRx.Operators.FromEventObservable`2/FromEvent<System.Object,UniRx.Tuple`3<System.Object,System.Object,System.Object>>::Register()
extern Il2CppClass* Exception_t1967233988_il2cpp_TypeInfo_var;
extern const uint32_t FromEvent_Register_m164332738_MetadataUsageId;
extern "C"  bool FromEvent_Register_m164332738_gshared (FromEvent_t3355758577 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (FromEvent_Register_m164332738_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Exception_t1967233988 * V_0 = NULL;
	bool V_1 = false;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		FromEventObservable_2_t3065932279 * L_0 = (FromEventObservable_2_t3065932279 *)__this->get_parent_0();
		NullCheck(L_0);
		Func_2_t32479265 * L_1 = (Func_2_t32479265 *)L_0->get_conversion_1();
		IntPtr_t L_2;
		L_2.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		Action_1_t1799351807 * L_3 = (Action_1_t1799351807 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		((  void (*) (Action_1_t1799351807 *, Il2CppObject *, IntPtr_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)(L_3, (Il2CppObject *)__this, (IntPtr_t)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		NullCheck((Func_2_t32479265 *)L_1);
		Il2CppObject * L_4 = ((  Il2CppObject * (*) (Func_2_t32479265 *, Action_1_t1799351807 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)((Func_2_t32479265 *)L_1, (Action_1_t1799351807 *)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		__this->set_handler_2(L_4);
	}

IL_0022:
	try
	{ // begin try (depth: 1)
		FromEventObservable_2_t3065932279 * L_5 = (FromEventObservable_2_t3065932279 *)__this->get_parent_0();
		NullCheck(L_5);
		Action_1_t985559125 * L_6 = (Action_1_t985559125 *)L_5->get_addHandler_2();
		Il2CppObject * L_7 = (Il2CppObject *)__this->get_handler_2();
		NullCheck((Action_1_t985559125 *)L_6);
		((  void (*) (Action_1_t985559125 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)((Action_1_t985559125 *)L_6, (Il2CppObject *)L_7, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		goto IL_0056;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1967233988 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Exception_t1967233988_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_003d;
		throw e;
	}

CATCH_003d:
	{ // begin catch(System.Exception)
		{
			V_0 = (Exception_t1967233988 *)((Exception_t1967233988 *)__exception_local);
			Il2CppObject* L_8 = (Il2CppObject*)__this->get_observer_1();
			Exception_t1967233988 * L_9 = V_0;
			NullCheck((Il2CppObject*)L_8);
			InterfaceActionInvoker1< Exception_t1967233988 * >::Invoke(1 /* System.Void UniRx.IObserver`1<UniRx.Tuple`3<System.Object,System.Object,System.Object>>::OnError(System.Exception) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5), (Il2CppObject*)L_8, (Exception_t1967233988 *)L_9);
			V_1 = (bool)0;
			goto IL_0058;
		}

IL_0051:
		{
			; // IL_0051: leave IL_0056
		}
	} // end catch (depth: 1)

IL_0056:
	{
		return (bool)1;
	}

IL_0058:
	{
		bool L_10 = V_1;
		return L_10;
	}
}
// System.Void UniRx.Operators.FromEventObservable`2/FromEvent<System.Object,UniRx.Tuple`3<System.Object,System.Object,System.Object>>::OnNext(TEventArgs)
extern "C"  void FromEvent_OnNext_m1301955626_gshared (FromEvent_t3355758577 * __this, Tuple_3_t1650899102  ___args0, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = (Il2CppObject*)__this->get_observer_1();
		Tuple_3_t1650899102  L_1 = ___args0;
		NullCheck((Il2CppObject*)L_0);
		InterfaceActionInvoker1< Tuple_3_t1650899102  >::Invoke(2 /* System.Void UniRx.IObserver`1<UniRx.Tuple`3<System.Object,System.Object,System.Object>>::OnNext(T) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5), (Il2CppObject*)L_0, (Tuple_3_t1650899102 )L_1);
		return;
	}
}
// System.Void UniRx.Operators.FromEventObservable`2/FromEvent<System.Object,UniRx.Tuple`3<System.Object,System.Object,System.Object>>::Dispose()
extern Il2CppClass* Il2CppObject_il2cpp_TypeInfo_var;
extern const uint32_t FromEvent_Dispose_m4254234406_MetadataUsageId;
extern "C"  void FromEvent_Dispose_m4254234406_gshared (FromEvent_t3355758577 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (FromEvent_Dispose_m4254234406_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Il2CppObject * V_0 = NULL;
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_handler_2();
		if (!L_0)
		{
			goto IL_0035;
		}
	}
	{
		FromEventObservable_2_t3065932279 * L_1 = (FromEventObservable_2_t3065932279 *)__this->get_parent_0();
		NullCheck(L_1);
		Action_1_t985559125 * L_2 = (Action_1_t985559125 *)L_1->get_removeHandler_3();
		Il2CppObject * L_3 = (Il2CppObject *)__this->get_handler_2();
		NullCheck((Action_1_t985559125 *)L_2);
		((  void (*) (Action_1_t985559125 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)((Action_1_t985559125 *)L_2, (Il2CppObject *)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		Initobj (Il2CppObject_il2cpp_TypeInfo_var, (&V_0));
		Il2CppObject * L_4 = V_0;
		__this->set_handler_2(L_4);
	}

IL_0035:
	{
		return;
	}
}
// System.Void UniRx.Operators.FromEventObservable`2/FromEvent<System.Object,UniRx.Tuple`4<System.Object,System.Object,System.Object,System.Object>>::.ctor(UniRx.Operators.FromEventObservable`2<TDelegate,TEventArgs>,UniRx.IObserver`1<TEventArgs>)
extern "C"  void FromEvent__ctor_m1555144599_gshared (FromEvent_t2732089756 * __this, FromEventObservable_2_t2442263458 * ___parent0, Il2CppObject* ___observer1, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		FromEventObservable_2_t2442263458 * L_0 = ___parent0;
		__this->set_parent_0(L_0);
		Il2CppObject* L_1 = ___observer1;
		__this->set_observer_1(L_1);
		return;
	}
}
// System.Boolean UniRx.Operators.FromEventObservable`2/FromEvent<System.Object,UniRx.Tuple`4<System.Object,System.Object,System.Object,System.Object>>::Register()
extern Il2CppClass* Exception_t1967233988_il2cpp_TypeInfo_var;
extern const uint32_t FromEvent_Register_m122661685_MetadataUsageId;
extern "C"  bool FromEvent_Register_m122661685_gshared (FromEvent_t2732089756 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (FromEvent_Register_m122661685_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Exception_t1967233988 * V_0 = NULL;
	bool V_1 = false;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		FromEventObservable_2_t2442263458 * L_0 = (FromEventObservable_2_t2442263458 *)__this->get_parent_0();
		NullCheck(L_0);
		Func_2_t202992490 * L_1 = (Func_2_t202992490 *)L_0->get_conversion_1();
		IntPtr_t L_2;
		L_2.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		Action_1_t1175682986 * L_3 = (Action_1_t1175682986 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		((  void (*) (Action_1_t1175682986 *, Il2CppObject *, IntPtr_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)(L_3, (Il2CppObject *)__this, (IntPtr_t)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		NullCheck((Func_2_t202992490 *)L_1);
		Il2CppObject * L_4 = ((  Il2CppObject * (*) (Func_2_t202992490 *, Action_1_t1175682986 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)((Func_2_t202992490 *)L_1, (Action_1_t1175682986 *)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		__this->set_handler_2(L_4);
	}

IL_0022:
	try
	{ // begin try (depth: 1)
		FromEventObservable_2_t2442263458 * L_5 = (FromEventObservable_2_t2442263458 *)__this->get_parent_0();
		NullCheck(L_5);
		Action_1_t985559125 * L_6 = (Action_1_t985559125 *)L_5->get_addHandler_2();
		Il2CppObject * L_7 = (Il2CppObject *)__this->get_handler_2();
		NullCheck((Action_1_t985559125 *)L_6);
		((  void (*) (Action_1_t985559125 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)((Action_1_t985559125 *)L_6, (Il2CppObject *)L_7, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		goto IL_0056;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1967233988 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Exception_t1967233988_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_003d;
		throw e;
	}

CATCH_003d:
	{ // begin catch(System.Exception)
		{
			V_0 = (Exception_t1967233988 *)((Exception_t1967233988 *)__exception_local);
			Il2CppObject* L_8 = (Il2CppObject*)__this->get_observer_1();
			Exception_t1967233988 * L_9 = V_0;
			NullCheck((Il2CppObject*)L_8);
			InterfaceActionInvoker1< Exception_t1967233988 * >::Invoke(1 /* System.Void UniRx.IObserver`1<UniRx.Tuple`4<System.Object,System.Object,System.Object,System.Object>>::OnError(System.Exception) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5), (Il2CppObject*)L_8, (Exception_t1967233988 *)L_9);
			V_1 = (bool)0;
			goto IL_0058;
		}

IL_0051:
		{
			; // IL_0051: leave IL_0056
		}
	} // end catch (depth: 1)

IL_0056:
	{
		return (bool)1;
	}

IL_0058:
	{
		bool L_10 = V_1;
		return L_10;
	}
}
// System.Void UniRx.Operators.FromEventObservable`2/FromEvent<System.Object,UniRx.Tuple`4<System.Object,System.Object,System.Object,System.Object>>::OnNext(TEventArgs)
extern "C"  void FromEvent_OnNext_m582067869_gshared (FromEvent_t2732089756 * __this, Tuple_4_t1027230281  ___args0, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = (Il2CppObject*)__this->get_observer_1();
		Tuple_4_t1027230281  L_1 = ___args0;
		NullCheck((Il2CppObject*)L_0);
		InterfaceActionInvoker1< Tuple_4_t1027230281  >::Invoke(2 /* System.Void UniRx.IObserver`1<UniRx.Tuple`4<System.Object,System.Object,System.Object,System.Object>>::OnNext(T) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5), (Il2CppObject*)L_0, (Tuple_4_t1027230281 )L_1);
		return;
	}
}
// System.Void UniRx.Operators.FromEventObservable`2/FromEvent<System.Object,UniRx.Tuple`4<System.Object,System.Object,System.Object,System.Object>>::Dispose()
extern Il2CppClass* Il2CppObject_il2cpp_TypeInfo_var;
extern const uint32_t FromEvent_Dispose_m1930688339_MetadataUsageId;
extern "C"  void FromEvent_Dispose_m1930688339_gshared (FromEvent_t2732089756 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (FromEvent_Dispose_m1930688339_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Il2CppObject * V_0 = NULL;
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_handler_2();
		if (!L_0)
		{
			goto IL_0035;
		}
	}
	{
		FromEventObservable_2_t2442263458 * L_1 = (FromEventObservable_2_t2442263458 *)__this->get_parent_0();
		NullCheck(L_1);
		Action_1_t985559125 * L_2 = (Action_1_t985559125 *)L_1->get_removeHandler_3();
		Il2CppObject * L_3 = (Il2CppObject *)__this->get_handler_2();
		NullCheck((Action_1_t985559125 *)L_2);
		((  void (*) (Action_1_t985559125 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)((Action_1_t985559125 *)L_2, (Il2CppObject *)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		Initobj (Il2CppObject_il2cpp_TypeInfo_var, (&V_0));
		Il2CppObject * L_4 = V_0;
		__this->set_handler_2(L_4);
	}

IL_0035:
	{
		return;
	}
}
// System.Void UniRx.Operators.FromEventObservable`2/FromEvent<System.Object,UnityEngine.Vector2>::.ctor(UniRx.Operators.FromEventObservable`2<TDelegate,TEventArgs>,UniRx.IObserver`1<TEventArgs>)
extern "C"  void FromEvent__ctor_m65149535_gshared (FromEvent_t935221967 * __this, FromEventObservable_2_t645395669 * ___parent0, Il2CppObject* ___observer1, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		FromEventObservable_2_t645395669 * L_0 = ___parent0;
		__this->set_parent_0(L_0);
		Il2CppObject* L_1 = ___observer1;
		__this->set_observer_1(L_1);
		return;
	}
}
// System.Boolean UniRx.Operators.FromEventObservable`2/FromEvent<System.Object,UnityEngine.Vector2>::Register()
extern Il2CppClass* Exception_t1967233988_il2cpp_TypeInfo_var;
extern const uint32_t FromEvent_Register_m4007355061_MetadataUsageId;
extern "C"  bool FromEvent_Register_m4007355061_gshared (FromEvent_t935221967 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (FromEvent_Register_m4007355061_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Exception_t1967233988 * V_0 = NULL;
	bool V_1 = false;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		FromEventObservable_2_t645395669 * L_0 = (FromEventObservable_2_t645395669 *)__this->get_parent_0();
		NullCheck(L_0);
		Func_2_t3830833163 * L_1 = (Func_2_t3830833163 *)L_0->get_conversion_1();
		IntPtr_t L_2;
		L_2.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		Action_1_t3673782493 * L_3 = (Action_1_t3673782493 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		((  void (*) (Action_1_t3673782493 *, Il2CppObject *, IntPtr_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)(L_3, (Il2CppObject *)__this, (IntPtr_t)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		NullCheck((Func_2_t3830833163 *)L_1);
		Il2CppObject * L_4 = ((  Il2CppObject * (*) (Func_2_t3830833163 *, Action_1_t3673782493 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)((Func_2_t3830833163 *)L_1, (Action_1_t3673782493 *)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		__this->set_handler_2(L_4);
	}

IL_0022:
	try
	{ // begin try (depth: 1)
		FromEventObservable_2_t645395669 * L_5 = (FromEventObservable_2_t645395669 *)__this->get_parent_0();
		NullCheck(L_5);
		Action_1_t985559125 * L_6 = (Action_1_t985559125 *)L_5->get_addHandler_2();
		Il2CppObject * L_7 = (Il2CppObject *)__this->get_handler_2();
		NullCheck((Action_1_t985559125 *)L_6);
		((  void (*) (Action_1_t985559125 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)((Action_1_t985559125 *)L_6, (Il2CppObject *)L_7, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		goto IL_0056;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1967233988 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Exception_t1967233988_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_003d;
		throw e;
	}

CATCH_003d:
	{ // begin catch(System.Exception)
		{
			V_0 = (Exception_t1967233988 *)((Exception_t1967233988 *)__exception_local);
			Il2CppObject* L_8 = (Il2CppObject*)__this->get_observer_1();
			Exception_t1967233988 * L_9 = V_0;
			NullCheck((Il2CppObject*)L_8);
			InterfaceActionInvoker1< Exception_t1967233988 * >::Invoke(1 /* System.Void UniRx.IObserver`1<UnityEngine.Vector2>::OnError(System.Exception) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5), (Il2CppObject*)L_8, (Exception_t1967233988 *)L_9);
			V_1 = (bool)0;
			goto IL_0058;
		}

IL_0051:
		{
			; // IL_0051: leave IL_0056
		}
	} // end catch (depth: 1)

IL_0056:
	{
		return (bool)1;
	}

IL_0058:
	{
		bool L_10 = V_1;
		return L_10;
	}
}
// System.Void UniRx.Operators.FromEventObservable`2/FromEvent<System.Object,UnityEngine.Vector2>::OnNext(TEventArgs)
extern "C"  void FromEvent_OnNext_m2501074789_gshared (FromEvent_t935221967 * __this, Vector2_t3525329788  ___args0, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = (Il2CppObject*)__this->get_observer_1();
		Vector2_t3525329788  L_1 = ___args0;
		NullCheck((Il2CppObject*)L_0);
		InterfaceActionInvoker1< Vector2_t3525329788  >::Invoke(2 /* System.Void UniRx.IObserver`1<UnityEngine.Vector2>::OnNext(T) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5), (Il2CppObject*)L_0, (Vector2_t3525329788 )L_1);
		return;
	}
}
// System.Void UniRx.Operators.FromEventObservable`2/FromEvent<System.Object,UnityEngine.Vector2>::Dispose()
extern Il2CppClass* Il2CppObject_il2cpp_TypeInfo_var;
extern const uint32_t FromEvent_Dispose_m4108434827_MetadataUsageId;
extern "C"  void FromEvent_Dispose_m4108434827_gshared (FromEvent_t935221967 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (FromEvent_Dispose_m4108434827_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Il2CppObject * V_0 = NULL;
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_handler_2();
		if (!L_0)
		{
			goto IL_0035;
		}
	}
	{
		FromEventObservable_2_t645395669 * L_1 = (FromEventObservable_2_t645395669 *)__this->get_parent_0();
		NullCheck(L_1);
		Action_1_t985559125 * L_2 = (Action_1_t985559125 *)L_1->get_removeHandler_3();
		Il2CppObject * L_3 = (Il2CppObject *)__this->get_handler_2();
		NullCheck((Action_1_t985559125 *)L_2);
		((  void (*) (Action_1_t985559125 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)((Action_1_t985559125 *)L_2, (Il2CppObject *)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		Initobj (Il2CppObject_il2cpp_TypeInfo_var, (&V_0));
		Il2CppObject * L_4 = V_0;
		__this->set_handler_2(L_4);
	}

IL_0035:
	{
		return;
	}
}
// System.Void UniRx.Operators.FromEventObservable`2<System.Object,System.Boolean>::.ctor(System.Func`2<System.Action`1<TEventArgs>,TDelegate>,System.Action`1<TDelegate>,System.Action`1<TDelegate>)
extern "C"  void FromEventObservable_2__ctor_m851348427_gshared (FromEventObservable_2_t1626038518 * __this, Func_2_t599409222 * ___conversion0, Action_1_t985559125 * ___addHandler1, Action_1_t985559125 * ___removeHandler2, const MethodInfo* method)
{
	{
		NullCheck((OperatorObservableBase_1_t3570117608 *)__this);
		((  void (*) (OperatorObservableBase_1_t3570117608 *, bool, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((OperatorObservableBase_1_t3570117608 *)__this, (bool)0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		Func_2_t599409222 * L_0 = ___conversion0;
		__this->set_conversion_1(L_0);
		Action_1_t985559125 * L_1 = ___addHandler1;
		__this->set_addHandler_2(L_1);
		Action_1_t985559125 * L_2 = ___removeHandler2;
		__this->set_removeHandler_3(L_2);
		return;
	}
}
// System.IDisposable UniRx.Operators.FromEventObservable`2<System.Object,System.Boolean>::SubscribeCore(UniRx.IObserver`1<TEventArgs>,System.IDisposable)
extern Il2CppClass* Disposable_t3736388786_il2cpp_TypeInfo_var;
extern const uint32_t FromEventObservable_2_SubscribeCore_m4127373153_MetadataUsageId;
extern "C"  Il2CppObject * FromEventObservable_2_SubscribeCore_m4127373153_gshared (FromEventObservable_2_t1626038518 * __this, Il2CppObject* ___observer0, Il2CppObject * ___cancel1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (FromEventObservable_2_SubscribeCore_m4127373153_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	FromEvent_t1915864816 * V_0 = NULL;
	Il2CppObject * V_1 = NULL;
	Il2CppObject * G_B3_0 = NULL;
	{
		Il2CppObject* L_0 = ___observer0;
		FromEvent_t1915864816 * L_1 = (FromEvent_t1915864816 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		((  void (*) (FromEvent_t1915864816 *, FromEventObservable_2_t1626038518 *, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)(L_1, (FromEventObservable_2_t1626038518 *)__this, (Il2CppObject*)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		V_0 = (FromEvent_t1915864816 *)L_1;
		FromEvent_t1915864816 * L_2 = V_0;
		NullCheck((FromEvent_t1915864816 *)L_2);
		bool L_3 = ((  bool (*) (FromEvent_t1915864816 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)((FromEvent_t1915864816 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		if (!L_3)
		{
			goto IL_001b;
		}
	}
	{
		FromEvent_t1915864816 * L_4 = V_0;
		V_1 = (Il2CppObject *)L_4;
		Il2CppObject * L_5 = V_1;
		G_B3_0 = L_5;
		goto IL_0020;
	}

IL_001b:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Disposable_t3736388786_il2cpp_TypeInfo_var);
		Il2CppObject * L_6 = ((Disposable_t3736388786_StaticFields*)Disposable_t3736388786_il2cpp_TypeInfo_var->static_fields)->get_Empty_0();
		G_B3_0 = L_6;
	}

IL_0020:
	{
		return G_B3_0;
	}
}
// System.Void UniRx.Operators.FromEventObservable`2<System.Object,System.Object>::.ctor(System.Func`2<System.Action`1<TEventArgs>,TDelegate>,System.Action`1<TDelegate>,System.Action`1<TDelegate>)
extern "C"  void FromEventObservable_2__ctor_m670375840_gshared (FromEventObservable_2_t2252139597 * __this, Func_2_t1100972979 * ___conversion0, Action_1_t985559125 * ___addHandler1, Action_1_t985559125 * ___removeHandler2, const MethodInfo* method)
{
	{
		NullCheck((OperatorObservableBase_1_t4196218687 *)__this);
		((  void (*) (OperatorObservableBase_1_t4196218687 *, bool, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((OperatorObservableBase_1_t4196218687 *)__this, (bool)0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		Func_2_t1100972979 * L_0 = ___conversion0;
		__this->set_conversion_1(L_0);
		Action_1_t985559125 * L_1 = ___addHandler1;
		__this->set_addHandler_2(L_1);
		Action_1_t985559125 * L_2 = ___removeHandler2;
		__this->set_removeHandler_3(L_2);
		return;
	}
}
// System.IDisposable UniRx.Operators.FromEventObservable`2<System.Object,System.Object>::SubscribeCore(UniRx.IObserver`1<TEventArgs>,System.IDisposable)
extern Il2CppClass* Disposable_t3736388786_il2cpp_TypeInfo_var;
extern const uint32_t FromEventObservable_2_SubscribeCore_m1097595808_MetadataUsageId;
extern "C"  Il2CppObject * FromEventObservable_2_SubscribeCore_m1097595808_gshared (FromEventObservable_2_t2252139597 * __this, Il2CppObject* ___observer0, Il2CppObject * ___cancel1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (FromEventObservable_2_SubscribeCore_m1097595808_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	FromEvent_t2541965895 * V_0 = NULL;
	Il2CppObject * V_1 = NULL;
	Il2CppObject * G_B3_0 = NULL;
	{
		Il2CppObject* L_0 = ___observer0;
		FromEvent_t2541965895 * L_1 = (FromEvent_t2541965895 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		((  void (*) (FromEvent_t2541965895 *, FromEventObservable_2_t2252139597 *, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)(L_1, (FromEventObservable_2_t2252139597 *)__this, (Il2CppObject*)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		V_0 = (FromEvent_t2541965895 *)L_1;
		FromEvent_t2541965895 * L_2 = V_0;
		NullCheck((FromEvent_t2541965895 *)L_2);
		bool L_3 = ((  bool (*) (FromEvent_t2541965895 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)((FromEvent_t2541965895 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		if (!L_3)
		{
			goto IL_001b;
		}
	}
	{
		FromEvent_t2541965895 * L_4 = V_0;
		V_1 = (Il2CppObject *)L_4;
		Il2CppObject * L_5 = V_1;
		G_B3_0 = L_5;
		goto IL_0020;
	}

IL_001b:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Disposable_t3736388786_il2cpp_TypeInfo_var);
		Il2CppObject * L_6 = ((Disposable_t3736388786_StaticFields*)Disposable_t3736388786_il2cpp_TypeInfo_var->static_fields)->get_Empty_0();
		G_B3_0 = L_6;
	}

IL_0020:
	{
		return G_B3_0;
	}
}
// System.Void UniRx.Operators.FromEventObservable`2<System.Object,System.Single>::.ctor(System.Func`2<System.Action`1<TEventArgs>,TDelegate>,System.Action`1<TDelegate>,System.Action`1<TDelegate>)
extern "C"  void FromEventObservable_2__ctor_m1581643305_gshared (FromEventObservable_2_t2373242198 * __this, Func_2_t2308880486 * ___conversion0, Action_1_t985559125 * ___addHandler1, Action_1_t985559125 * ___removeHandler2, const MethodInfo* method)
{
	{
		NullCheck((OperatorObservableBase_1_t22353992 *)__this);
		((  void (*) (OperatorObservableBase_1_t22353992 *, bool, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((OperatorObservableBase_1_t22353992 *)__this, (bool)0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		Func_2_t2308880486 * L_0 = ___conversion0;
		__this->set_conversion_1(L_0);
		Action_1_t985559125 * L_1 = ___addHandler1;
		__this->set_addHandler_2(L_1);
		Action_1_t985559125 * L_2 = ___removeHandler2;
		__this->set_removeHandler_3(L_2);
		return;
	}
}
// System.IDisposable UniRx.Operators.FromEventObservable`2<System.Object,System.Single>::SubscribeCore(UniRx.IObserver`1<TEventArgs>,System.IDisposable)
extern Il2CppClass* Disposable_t3736388786_il2cpp_TypeInfo_var;
extern const uint32_t FromEventObservable_2_SubscribeCore_m3866424937_MetadataUsageId;
extern "C"  Il2CppObject * FromEventObservable_2_SubscribeCore_m3866424937_gshared (FromEventObservable_2_t2373242198 * __this, Il2CppObject* ___observer0, Il2CppObject * ___cancel1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (FromEventObservable_2_SubscribeCore_m3866424937_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	FromEvent_t2663068496 * V_0 = NULL;
	Il2CppObject * V_1 = NULL;
	Il2CppObject * G_B3_0 = NULL;
	{
		Il2CppObject* L_0 = ___observer0;
		FromEvent_t2663068496 * L_1 = (FromEvent_t2663068496 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		((  void (*) (FromEvent_t2663068496 *, FromEventObservable_2_t2373242198 *, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)(L_1, (FromEventObservable_2_t2373242198 *)__this, (Il2CppObject*)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		V_0 = (FromEvent_t2663068496 *)L_1;
		FromEvent_t2663068496 * L_2 = V_0;
		NullCheck((FromEvent_t2663068496 *)L_2);
		bool L_3 = ((  bool (*) (FromEvent_t2663068496 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)((FromEvent_t2663068496 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		if (!L_3)
		{
			goto IL_001b;
		}
	}
	{
		FromEvent_t2663068496 * L_4 = V_0;
		V_1 = (Il2CppObject *)L_4;
		Il2CppObject * L_5 = V_1;
		G_B3_0 = L_5;
		goto IL_0020;
	}

IL_001b:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Disposable_t3736388786_il2cpp_TypeInfo_var);
		Il2CppObject * L_6 = ((Disposable_t3736388786_StaticFields*)Disposable_t3736388786_il2cpp_TypeInfo_var->static_fields)->get_Empty_0();
		G_B3_0 = L_6;
	}

IL_0020:
	{
		return G_B3_0;
	}
}
// System.Void UniRx.Operators.FromEventObservable`2<System.Object,UniRx.Tuple`2<System.Object,System.Object>>::.ctor(System.Func`2<System.Action`1<TEventArgs>,TDelegate>,System.Action`1<TDelegate>,System.Action`1<TDelegate>)
extern "C"  void FromEventObservable_2__ctor_m29754698_gshared (FromEventObservable_2_t1784294996 * __this, Func_2_t692415920 * ___conversion0, Action_1_t985559125 * ___addHandler1, Action_1_t985559125 * ___removeHandler2, const MethodInfo* method)
{
	{
		NullCheck((OperatorObservableBase_1_t3728374086 *)__this);
		((  void (*) (OperatorObservableBase_1_t3728374086 *, bool, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((OperatorObservableBase_1_t3728374086 *)__this, (bool)0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		Func_2_t692415920 * L_0 = ___conversion0;
		__this->set_conversion_1(L_0);
		Action_1_t985559125 * L_1 = ___addHandler1;
		__this->set_addHandler_2(L_1);
		Action_1_t985559125 * L_2 = ___removeHandler2;
		__this->set_removeHandler_3(L_2);
		return;
	}
}
// System.IDisposable UniRx.Operators.FromEventObservable`2<System.Object,UniRx.Tuple`2<System.Object,System.Object>>::SubscribeCore(UniRx.IObserver`1<TEventArgs>,System.IDisposable)
extern Il2CppClass* Disposable_t3736388786_il2cpp_TypeInfo_var;
extern const uint32_t FromEventObservable_2_SubscribeCore_m1455687712_MetadataUsageId;
extern "C"  Il2CppObject * FromEventObservable_2_SubscribeCore_m1455687712_gshared (FromEventObservable_2_t1784294996 * __this, Il2CppObject* ___observer0, Il2CppObject * ___cancel1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (FromEventObservable_2_SubscribeCore_m1455687712_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	FromEvent_t2074121294 * V_0 = NULL;
	Il2CppObject * V_1 = NULL;
	Il2CppObject * G_B3_0 = NULL;
	{
		Il2CppObject* L_0 = ___observer0;
		FromEvent_t2074121294 * L_1 = (FromEvent_t2074121294 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		((  void (*) (FromEvent_t2074121294 *, FromEventObservable_2_t1784294996 *, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)(L_1, (FromEventObservable_2_t1784294996 *)__this, (Il2CppObject*)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		V_0 = (FromEvent_t2074121294 *)L_1;
		FromEvent_t2074121294 * L_2 = V_0;
		NullCheck((FromEvent_t2074121294 *)L_2);
		bool L_3 = ((  bool (*) (FromEvent_t2074121294 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)((FromEvent_t2074121294 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		if (!L_3)
		{
			goto IL_001b;
		}
	}
	{
		FromEvent_t2074121294 * L_4 = V_0;
		V_1 = (Il2CppObject *)L_4;
		Il2CppObject * L_5 = V_1;
		G_B3_0 = L_5;
		goto IL_0020;
	}

IL_001b:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Disposable_t3736388786_il2cpp_TypeInfo_var);
		Il2CppObject * L_6 = ((Disposable_t3736388786_StaticFields*)Disposable_t3736388786_il2cpp_TypeInfo_var->static_fields)->get_Empty_0();
		G_B3_0 = L_6;
	}

IL_0020:
	{
		return G_B3_0;
	}
}
// System.Void UniRx.Operators.FromEventObservable`2<System.Object,UniRx.Tuple`3<System.Object,System.Object,System.Object>>::.ctor(System.Func`2<System.Action`1<TEventArgs>,TDelegate>,System.Action`1<TDelegate>,System.Action`1<TDelegate>)
extern "C"  void FromEventObservable_2__ctor_m2308930487_gshared (FromEventObservable_2_t3065932279 * __this, Func_2_t32479265 * ___conversion0, Action_1_t985559125 * ___addHandler1, Action_1_t985559125 * ___removeHandler2, const MethodInfo* method)
{
	{
		NullCheck((OperatorObservableBase_1_t715044073 *)__this);
		((  void (*) (OperatorObservableBase_1_t715044073 *, bool, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((OperatorObservableBase_1_t715044073 *)__this, (bool)0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		Func_2_t32479265 * L_0 = ___conversion0;
		__this->set_conversion_1(L_0);
		Action_1_t985559125 * L_1 = ___addHandler1;
		__this->set_addHandler_2(L_1);
		Action_1_t985559125 * L_2 = ___removeHandler2;
		__this->set_removeHandler_3(L_2);
		return;
	}
}
// System.IDisposable UniRx.Operators.FromEventObservable`2<System.Object,UniRx.Tuple`3<System.Object,System.Object,System.Object>>::SubscribeCore(UniRx.IObserver`1<TEventArgs>,System.IDisposable)
extern Il2CppClass* Disposable_t3736388786_il2cpp_TypeInfo_var;
extern const uint32_t FromEventObservable_2_SubscribeCore_m811098253_MetadataUsageId;
extern "C"  Il2CppObject * FromEventObservable_2_SubscribeCore_m811098253_gshared (FromEventObservable_2_t3065932279 * __this, Il2CppObject* ___observer0, Il2CppObject * ___cancel1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (FromEventObservable_2_SubscribeCore_m811098253_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	FromEvent_t3355758577 * V_0 = NULL;
	Il2CppObject * V_1 = NULL;
	Il2CppObject * G_B3_0 = NULL;
	{
		Il2CppObject* L_0 = ___observer0;
		FromEvent_t3355758577 * L_1 = (FromEvent_t3355758577 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		((  void (*) (FromEvent_t3355758577 *, FromEventObservable_2_t3065932279 *, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)(L_1, (FromEventObservable_2_t3065932279 *)__this, (Il2CppObject*)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		V_0 = (FromEvent_t3355758577 *)L_1;
		FromEvent_t3355758577 * L_2 = V_0;
		NullCheck((FromEvent_t3355758577 *)L_2);
		bool L_3 = ((  bool (*) (FromEvent_t3355758577 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)((FromEvent_t3355758577 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		if (!L_3)
		{
			goto IL_001b;
		}
	}
	{
		FromEvent_t3355758577 * L_4 = V_0;
		V_1 = (Il2CppObject *)L_4;
		Il2CppObject * L_5 = V_1;
		G_B3_0 = L_5;
		goto IL_0020;
	}

IL_001b:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Disposable_t3736388786_il2cpp_TypeInfo_var);
		Il2CppObject * L_6 = ((Disposable_t3736388786_StaticFields*)Disposable_t3736388786_il2cpp_TypeInfo_var->static_fields)->get_Empty_0();
		G_B3_0 = L_6;
	}

IL_0020:
	{
		return G_B3_0;
	}
}
// System.Void UniRx.Operators.FromEventObservable`2<System.Object,UniRx.Tuple`4<System.Object,System.Object,System.Object,System.Object>>::.ctor(System.Func`2<System.Action`1<TEventArgs>,TDelegate>,System.Action`1<TDelegate>,System.Action`1<TDelegate>)
extern "C"  void FromEventObservable_2__ctor_m1170865444_gshared (FromEventObservable_2_t2442263458 * __this, Func_2_t202992490 * ___conversion0, Action_1_t985559125 * ___addHandler1, Action_1_t985559125 * ___removeHandler2, const MethodInfo* method)
{
	{
		NullCheck((OperatorObservableBase_1_t91375252 *)__this);
		((  void (*) (OperatorObservableBase_1_t91375252 *, bool, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((OperatorObservableBase_1_t91375252 *)__this, (bool)0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		Func_2_t202992490 * L_0 = ___conversion0;
		__this->set_conversion_1(L_0);
		Action_1_t985559125 * L_1 = ___addHandler1;
		__this->set_addHandler_2(L_1);
		Action_1_t985559125 * L_2 = ___removeHandler2;
		__this->set_removeHandler_3(L_2);
		return;
	}
}
// System.IDisposable UniRx.Operators.FromEventObservable`2<System.Object,UniRx.Tuple`4<System.Object,System.Object,System.Object,System.Object>>::SubscribeCore(UniRx.IObserver`1<TEventArgs>,System.IDisposable)
extern Il2CppClass* Disposable_t3736388786_il2cpp_TypeInfo_var;
extern const uint32_t FromEventObservable_2_SubscribeCore_m1100821498_MetadataUsageId;
extern "C"  Il2CppObject * FromEventObservable_2_SubscribeCore_m1100821498_gshared (FromEventObservable_2_t2442263458 * __this, Il2CppObject* ___observer0, Il2CppObject * ___cancel1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (FromEventObservable_2_SubscribeCore_m1100821498_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	FromEvent_t2732089756 * V_0 = NULL;
	Il2CppObject * V_1 = NULL;
	Il2CppObject * G_B3_0 = NULL;
	{
		Il2CppObject* L_0 = ___observer0;
		FromEvent_t2732089756 * L_1 = (FromEvent_t2732089756 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		((  void (*) (FromEvent_t2732089756 *, FromEventObservable_2_t2442263458 *, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)(L_1, (FromEventObservable_2_t2442263458 *)__this, (Il2CppObject*)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		V_0 = (FromEvent_t2732089756 *)L_1;
		FromEvent_t2732089756 * L_2 = V_0;
		NullCheck((FromEvent_t2732089756 *)L_2);
		bool L_3 = ((  bool (*) (FromEvent_t2732089756 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)((FromEvent_t2732089756 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		if (!L_3)
		{
			goto IL_001b;
		}
	}
	{
		FromEvent_t2732089756 * L_4 = V_0;
		V_1 = (Il2CppObject *)L_4;
		Il2CppObject * L_5 = V_1;
		G_B3_0 = L_5;
		goto IL_0020;
	}

IL_001b:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Disposable_t3736388786_il2cpp_TypeInfo_var);
		Il2CppObject * L_6 = ((Disposable_t3736388786_StaticFields*)Disposable_t3736388786_il2cpp_TypeInfo_var->static_fields)->get_Empty_0();
		G_B3_0 = L_6;
	}

IL_0020:
	{
		return G_B3_0;
	}
}
// System.Void UniRx.Operators.FromEventObservable`2<System.Object,UnityEngine.Vector2>::.ctor(System.Func`2<System.Action`1<TEventArgs>,TDelegate>,System.Action`1<TDelegate>,System.Action`1<TDelegate>)
extern "C"  void FromEventObservable_2__ctor_m1920963834_gshared (FromEventObservable_2_t645395669 * __this, Func_2_t3830833163 * ___conversion0, Action_1_t985559125 * ___addHandler1, Action_1_t985559125 * ___removeHandler2, const MethodInfo* method)
{
	{
		NullCheck((OperatorObservableBase_1_t2589474759 *)__this);
		((  void (*) (OperatorObservableBase_1_t2589474759 *, bool, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((OperatorObservableBase_1_t2589474759 *)__this, (bool)0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		Func_2_t3830833163 * L_0 = ___conversion0;
		__this->set_conversion_1(L_0);
		Action_1_t985559125 * L_1 = ___addHandler1;
		__this->set_addHandler_2(L_1);
		Action_1_t985559125 * L_2 = ___removeHandler2;
		__this->set_removeHandler_3(L_2);
		return;
	}
}
// System.IDisposable UniRx.Operators.FromEventObservable`2<System.Object,UnityEngine.Vector2>::SubscribeCore(UniRx.IObserver`1<TEventArgs>,System.IDisposable)
extern Il2CppClass* Disposable_t3736388786_il2cpp_TypeInfo_var;
extern const uint32_t FromEventObservable_2_SubscribeCore_m521755578_MetadataUsageId;
extern "C"  Il2CppObject * FromEventObservable_2_SubscribeCore_m521755578_gshared (FromEventObservable_2_t645395669 * __this, Il2CppObject* ___observer0, Il2CppObject * ___cancel1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (FromEventObservable_2_SubscribeCore_m521755578_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	FromEvent_t935221967 * V_0 = NULL;
	Il2CppObject * V_1 = NULL;
	Il2CppObject * G_B3_0 = NULL;
	{
		Il2CppObject* L_0 = ___observer0;
		FromEvent_t935221967 * L_1 = (FromEvent_t935221967 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		((  void (*) (FromEvent_t935221967 *, FromEventObservable_2_t645395669 *, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)(L_1, (FromEventObservable_2_t645395669 *)__this, (Il2CppObject*)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		V_0 = (FromEvent_t935221967 *)L_1;
		FromEvent_t935221967 * L_2 = V_0;
		NullCheck((FromEvent_t935221967 *)L_2);
		bool L_3 = ((  bool (*) (FromEvent_t935221967 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)((FromEvent_t935221967 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		if (!L_3)
		{
			goto IL_001b;
		}
	}
	{
		FromEvent_t935221967 * L_4 = V_0;
		V_1 = (Il2CppObject *)L_4;
		Il2CppObject * L_5 = V_1;
		G_B3_0 = L_5;
		goto IL_0020;
	}

IL_001b:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Disposable_t3736388786_il2cpp_TypeInfo_var);
		Il2CppObject * L_6 = ((Disposable_t3736388786_StaticFields*)Disposable_t3736388786_il2cpp_TypeInfo_var->static_fields)->get_Empty_0();
		G_B3_0 = L_6;
	}

IL_0020:
	{
		return G_B3_0;
	}
}
// System.Void UniRx.Operators.FromEventPatternObservable`2/FromEventPattern<System.Object,System.Object>::.ctor(UniRx.Operators.FromEventPatternObservable`2<TDelegate,TEventArgs>,UniRx.IObserver`1<UniRx.EventPattern`1<TEventArgs>>)
extern "C"  void FromEventPattern__ctor_m1976577664_gshared (FromEventPattern_t208508663 * __this, FromEventPatternObservable_2_t3019221245 * ___parent0, Il2CppObject* ___observer1, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		FromEventPatternObservable_2_t3019221245 * L_0 = ___parent0;
		__this->set_parent_0(L_0);
		Il2CppObject* L_1 = ___observer1;
		__this->set_observer_1(L_1);
		return;
	}
}
// System.Boolean UniRx.Operators.FromEventPatternObservable`2/FromEventPattern<System.Object,System.Object>::Register()
extern Il2CppClass* Exception_t1967233988_il2cpp_TypeInfo_var;
extern const uint32_t FromEventPattern_Register_m522344379_MetadataUsageId;
extern "C"  bool FromEventPattern_Register_m522344379_gshared (FromEventPattern_t208508663 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (FromEventPattern_Register_m522344379_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Exception_t1967233988 * V_0 = NULL;
	bool V_1 = false;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		FromEventPatternObservable_2_t3019221245 * L_0 = (FromEventPatternObservable_2_t3019221245 *)__this->get_parent_0();
		NullCheck(L_0);
		Func_2_t2937104577 * L_1 = (Func_2_t2937104577 *)L_0->get_conversion_1();
		IntPtr_t L_2;
		L_2.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		EventHandler_1_t1679684063 * L_3 = (EventHandler_1_t1679684063 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		((  void (*) (EventHandler_1_t1679684063 *, Il2CppObject *, IntPtr_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)(L_3, (Il2CppObject *)__this, (IntPtr_t)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		NullCheck((Func_2_t2937104577 *)L_1);
		Il2CppObject * L_4 = ((  Il2CppObject * (*) (Func_2_t2937104577 *, EventHandler_1_t1679684063 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)((Func_2_t2937104577 *)L_1, (EventHandler_1_t1679684063 *)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		__this->set_handler_2(L_4);
	}

IL_0022:
	try
	{ // begin try (depth: 1)
		FromEventPatternObservable_2_t3019221245 * L_5 = (FromEventPatternObservable_2_t3019221245 *)__this->get_parent_0();
		NullCheck(L_5);
		Action_1_t985559125 * L_6 = (Action_1_t985559125 *)L_5->get_addHandler_2();
		Il2CppObject * L_7 = (Il2CppObject *)__this->get_handler_2();
		NullCheck((Action_1_t985559125 *)L_6);
		((  void (*) (Action_1_t985559125 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)((Action_1_t985559125 *)L_6, (Il2CppObject *)L_7, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		goto IL_0056;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1967233988 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Exception_t1967233988_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_003d;
		throw e;
	}

CATCH_003d:
	{ // begin catch(System.Exception)
		{
			V_0 = (Exception_t1967233988 *)((Exception_t1967233988 *)__exception_local);
			Il2CppObject* L_8 = (Il2CppObject*)__this->get_observer_1();
			Exception_t1967233988 * L_9 = V_0;
			NullCheck((Il2CppObject*)L_8);
			InterfaceActionInvoker1< Exception_t1967233988 * >::Invoke(1 /* System.Void UniRx.IObserver`1<UniRx.EventPattern`1<System.Object>>::OnError(System.Exception) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5), (Il2CppObject*)L_8, (Exception_t1967233988 *)L_9);
			V_1 = (bool)0;
			goto IL_0058;
		}

IL_0051:
		{
			; // IL_0051: leave IL_0056
		}
	} // end catch (depth: 1)

IL_0056:
	{
		return (bool)1;
	}

IL_0058:
	{
		bool L_10 = V_1;
		return L_10;
	}
}
// System.Void UniRx.Operators.FromEventPatternObservable`2/FromEventPattern<System.Object,System.Object>::OnNext(System.Object,TEventArgs)
extern "C"  void FromEventPattern_OnNext_m3769990365_gshared (FromEventPattern_t208508663 * __this, Il2CppObject * ___sender0, Il2CppObject * ___eventArgs1, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = (Il2CppObject*)__this->get_observer_1();
		Il2CppObject * L_1 = ___sender0;
		Il2CppObject * L_2 = ___eventArgs1;
		EventPattern_1_t2903003978 * L_3 = (EventPattern_1_t2903003978 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		((  void (*) (EventPattern_1_t2903003978 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7)->method)(L_3, (Il2CppObject *)L_1, (Il2CppObject *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7));
		NullCheck((Il2CppObject*)L_0);
		InterfaceActionInvoker1< EventPattern_1_t2903003978 * >::Invoke(2 /* System.Void UniRx.IObserver`1<UniRx.EventPattern`1<System.Object>>::OnNext(T) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5), (Il2CppObject*)L_0, (EventPattern_1_t2903003978 *)L_3);
		return;
	}
}
// System.Void UniRx.Operators.FromEventPatternObservable`2/FromEventPattern<System.Object,System.Object>::Dispose()
extern Il2CppClass* Il2CppObject_il2cpp_TypeInfo_var;
extern const uint32_t FromEventPattern_Dispose_m1389966277_MetadataUsageId;
extern "C"  void FromEventPattern_Dispose_m1389966277_gshared (FromEventPattern_t208508663 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (FromEventPattern_Dispose_m1389966277_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Il2CppObject * V_0 = NULL;
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_handler_2();
		if (!L_0)
		{
			goto IL_0035;
		}
	}
	{
		FromEventPatternObservable_2_t3019221245 * L_1 = (FromEventPatternObservable_2_t3019221245 *)__this->get_parent_0();
		NullCheck(L_1);
		Action_1_t985559125 * L_2 = (Action_1_t985559125 *)L_1->get_removeHandler_3();
		Il2CppObject * L_3 = (Il2CppObject *)__this->get_handler_2();
		NullCheck((Action_1_t985559125 *)L_2);
		((  void (*) (Action_1_t985559125 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)((Action_1_t985559125 *)L_2, (Il2CppObject *)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		Initobj (Il2CppObject_il2cpp_TypeInfo_var, (&V_0));
		Il2CppObject * L_4 = V_0;
		__this->set_handler_2(L_4);
	}

IL_0035:
	{
		return;
	}
}
// System.Void UniRx.Operators.FromEventPatternObservable`2<System.Object,System.Object>::.ctor(System.Func`2<System.EventHandler`1<TEventArgs>,TDelegate>,System.Action`1<TDelegate>,System.Action`1<TDelegate>)
extern "C"  void FromEventPatternObservable_2__ctor_m4058073472_gshared (FromEventPatternObservable_2_t3019221245 * __this, Func_2_t2937104577 * ___conversion0, Action_1_t985559125 * ___addHandler1, Action_1_t985559125 * ___removeHandler2, const MethodInfo* method)
{
	{
		NullCheck((OperatorObservableBase_1_t1967148949 *)__this);
		((  void (*) (OperatorObservableBase_1_t1967148949 *, bool, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((OperatorObservableBase_1_t1967148949 *)__this, (bool)0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		Func_2_t2937104577 * L_0 = ___conversion0;
		__this->set_conversion_1(L_0);
		Action_1_t985559125 * L_1 = ___addHandler1;
		__this->set_addHandler_2(L_1);
		Action_1_t985559125 * L_2 = ___removeHandler2;
		__this->set_removeHandler_3(L_2);
		return;
	}
}
// System.IDisposable UniRx.Operators.FromEventPatternObservable`2<System.Object,System.Object>::SubscribeCore(UniRx.IObserver`1<UniRx.EventPattern`1<TEventArgs>>,System.IDisposable)
extern Il2CppClass* Disposable_t3736388786_il2cpp_TypeInfo_var;
extern const uint32_t FromEventPatternObservable_2_SubscribeCore_m1451785217_MetadataUsageId;
extern "C"  Il2CppObject * FromEventPatternObservable_2_SubscribeCore_m1451785217_gshared (FromEventPatternObservable_2_t3019221245 * __this, Il2CppObject* ___observer0, Il2CppObject * ___cancel1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (FromEventPatternObservable_2_SubscribeCore_m1451785217_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	FromEventPattern_t208508663 * V_0 = NULL;
	Il2CppObject * V_1 = NULL;
	Il2CppObject * G_B3_0 = NULL;
	{
		Il2CppObject* L_0 = ___observer0;
		FromEventPattern_t208508663 * L_1 = (FromEventPattern_t208508663 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		((  void (*) (FromEventPattern_t208508663 *, FromEventPatternObservable_2_t3019221245 *, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)(L_1, (FromEventPatternObservable_2_t3019221245 *)__this, (Il2CppObject*)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		V_0 = (FromEventPattern_t208508663 *)L_1;
		FromEventPattern_t208508663 * L_2 = V_0;
		NullCheck((FromEventPattern_t208508663 *)L_2);
		bool L_3 = ((  bool (*) (FromEventPattern_t208508663 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)((FromEventPattern_t208508663 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		if (!L_3)
		{
			goto IL_001b;
		}
	}
	{
		FromEventPattern_t208508663 * L_4 = V_0;
		V_1 = (Il2CppObject *)L_4;
		Il2CppObject * L_5 = V_1;
		G_B3_0 = L_5;
		goto IL_0020;
	}

IL_001b:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Disposable_t3736388786_il2cpp_TypeInfo_var);
		Il2CppObject * L_6 = ((Disposable_t3736388786_StaticFields*)Disposable_t3736388786_il2cpp_TypeInfo_var->static_fields)->get_Empty_0();
		G_B3_0 = L_6;
	}

IL_0020:
	{
		return G_B3_0;
	}
}
// System.Void UniRx.Operators.GroupByObservable`3/GroupBy<System.Object,System.Object,System.Object>::.ctor(UniRx.Operators.GroupByObservable`3<TSource,TKey,TElement>,UniRx.IObserver`1<UniRx.IGroupedObservable`2<TKey,TElement>>,System.IDisposable)
extern const MethodInfo* Nullable_1_get_HasValue_m1686547625_MethodInfo_var;
extern const MethodInfo* Nullable_1_get_Value_m844974555_MethodInfo_var;
extern const uint32_t GroupBy__ctor_m1099347075_MetadataUsageId;
extern "C"  void GroupBy__ctor_m1099347075_gshared (GroupBy_t3470189943 * __this, GroupByObservable_3_t657897610 * ___parent0, Il2CppObject* ___observer1, Il2CppObject * ___cancel2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GroupBy__ctor_m1099347075_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject* L_0 = ___observer1;
		Il2CppObject * L_1 = ___cancel2;
		NullCheck((OperatorObserverBase_2_t1657047628 *)__this);
		((  void (*) (OperatorObserverBase_2_t1657047628 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((OperatorObserverBase_2_t1657047628 *)__this, (Il2CppObject*)L_0, (Il2CppObject *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		GroupByObservable_3_t657897610 * L_2 = ___parent0;
		__this->set_parent_2(L_2);
		GroupByObservable_3_t657897610 * L_3 = ___parent0;
		NullCheck(L_3);
		Nullable_1_t1438485399 * L_4 = (Nullable_1_t1438485399 *)L_3->get_address_of_capacity_4();
		bool L_5 = Nullable_1_get_HasValue_m1686547625((Nullable_1_t1438485399 *)L_4, /*hidden argument*/Nullable_1_get_HasValue_m1686547625_MethodInfo_var);
		if (!L_5)
		{
			goto IL_0040;
		}
	}
	{
		GroupByObservable_3_t657897610 * L_6 = ___parent0;
		NullCheck(L_6);
		Nullable_1_t1438485399 * L_7 = (Nullable_1_t1438485399 *)L_6->get_address_of_capacity_4();
		int32_t L_8 = Nullable_1_get_Value_m844974555((Nullable_1_t1438485399 *)L_7, /*hidden argument*/Nullable_1_get_Value_m844974555_MethodInfo_var);
		GroupByObservable_3_t657897610 * L_9 = ___parent0;
		NullCheck(L_9);
		Il2CppObject* L_10 = (Il2CppObject*)L_9->get_comparer_5();
		Dictionary_2_t3341028665 * L_11 = (Dictionary_2_t3341028665 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		((  void (*) (Dictionary_2_t3341028665 *, int32_t, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)(L_11, (int32_t)L_8, (Il2CppObject*)L_10, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		__this->set_map_3(L_11);
		goto IL_0051;
	}

IL_0040:
	{
		GroupByObservable_3_t657897610 * L_12 = ___parent0;
		NullCheck(L_12);
		Il2CppObject* L_13 = (Il2CppObject*)L_12->get_comparer_5();
		Dictionary_2_t3341028665 * L_14 = (Dictionary_2_t3341028665 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		((  void (*) (Dictionary_2_t3341028665 *, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)(L_14, (Il2CppObject*)L_13, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		__this->set_map_3(L_14);
	}

IL_0051:
	{
		return;
	}
}
// System.IDisposable UniRx.Operators.GroupByObservable`3/GroupBy<System.Object,System.Object,System.Object>::Run()
extern Il2CppClass* CompositeDisposable_t1894629977_il2cpp_TypeInfo_var;
extern Il2CppClass* RefCountDisposable_t3288429070_il2cpp_TypeInfo_var;
extern const uint32_t GroupBy_Run_m82052481_MetadataUsageId;
extern "C"  Il2CppObject * GroupBy_Run_m82052481_gshared (GroupBy_t3470189943 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GroupBy_Run_m82052481_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		CompositeDisposable_t1894629977 * L_0 = (CompositeDisposable_t1894629977 *)il2cpp_codegen_object_new(CompositeDisposable_t1894629977_il2cpp_TypeInfo_var);
		CompositeDisposable__ctor_m1237994920(L_0, /*hidden argument*/NULL);
		__this->set_groupDisposable_5(L_0);
		CompositeDisposable_t1894629977 * L_1 = (CompositeDisposable_t1894629977 *)__this->get_groupDisposable_5();
		RefCountDisposable_t3288429070 * L_2 = (RefCountDisposable_t3288429070 *)il2cpp_codegen_object_new(RefCountDisposable_t3288429070_il2cpp_TypeInfo_var);
		RefCountDisposable__ctor_m3639770769(L_2, (Il2CppObject *)L_1, /*hidden argument*/NULL);
		__this->set_refCountDisposable_6(L_2);
		CompositeDisposable_t1894629977 * L_3 = (CompositeDisposable_t1894629977 *)__this->get_groupDisposable_5();
		GroupByObservable_3_t657897610 * L_4 = (GroupByObservable_3_t657897610 *)__this->get_parent_2();
		NullCheck(L_4);
		Il2CppObject* L_5 = (Il2CppObject*)L_4->get_source_1();
		NullCheck((Il2CppObject*)L_5);
		Il2CppObject * L_6 = InterfaceFuncInvoker1< Il2CppObject *, Il2CppObject* >::Invoke(0 /* System.IDisposable UniRx.IObservable`1<System.Object>::Subscribe(UniRx.IObserver`1<T>) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5), (Il2CppObject*)L_5, (Il2CppObject*)__this);
		NullCheck((CompositeDisposable_t1894629977 *)L_3);
		CompositeDisposable_Add_m2171552957((CompositeDisposable_t1894629977 *)L_3, (Il2CppObject *)L_6, /*hidden argument*/NULL);
		RefCountDisposable_t3288429070 * L_7 = (RefCountDisposable_t3288429070 *)__this->get_refCountDisposable_6();
		return L_7;
	}
}
// System.Void UniRx.Operators.GroupByObservable`3/GroupBy<System.Object,System.Object,System.Object>::OnNext(TSource)
extern Il2CppClass* Il2CppObject_il2cpp_TypeInfo_var;
extern Il2CppClass* Exception_t1967233988_il2cpp_TypeInfo_var;
extern const uint32_t GroupBy_OnNext_m789374336_MetadataUsageId;
extern "C"  void GroupBy_OnNext_m789374336_gshared (GroupBy_t3470189943 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GroupBy_OnNext_m789374336_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Il2CppObject * V_0 = NULL;
	Exception_t1967233988 * V_1 = NULL;
	bool V_2 = false;
	Il2CppObject* V_3 = NULL;
	Exception_t1967233988 * V_4 = NULL;
	GroupedObservable_2_t2613254135 * V_5 = NULL;
	Il2CppObject * V_6 = NULL;
	Exception_t1967233988 * V_7 = NULL;
	Il2CppObject * V_8 = NULL;
	Il2CppObject * V_9 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Initobj (Il2CppObject_il2cpp_TypeInfo_var, (&V_8));
		Il2CppObject * L_0 = V_8;
		V_0 = (Il2CppObject *)L_0;
	}

IL_000b:
	try
	{ // begin try (depth: 1)
		GroupByObservable_3_t657897610 * L_1 = (GroupByObservable_3_t657897610 *)__this->get_parent_2();
		NullCheck(L_1);
		Func_2_t2135783352 * L_2 = (Func_2_t2135783352 *)L_1->get_keySelector_2();
		Il2CppObject * L_3 = ___value0;
		NullCheck((Func_2_t2135783352 *)L_2);
		Il2CppObject * L_4 = ((  Il2CppObject * (*) (Func_2_t2135783352 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6)->method)((Func_2_t2135783352 *)L_2, (Il2CppObject *)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		V_0 = (Il2CppObject *)L_4;
		goto IL_0034;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1967233988 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Exception_t1967233988_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_0022;
		throw e;
	}

CATCH_0022:
	{ // begin catch(System.Exception)
		{
			V_1 = (Exception_t1967233988 *)((Exception_t1967233988 *)__exception_local);
			Exception_t1967233988 * L_5 = V_1;
			NullCheck((GroupBy_t3470189943 *)__this);
			((  void (*) (GroupBy_t3470189943 *, Exception_t1967233988 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7)->method)((GroupBy_t3470189943 *)__this, (Exception_t1967233988 *)L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7));
			goto IL_010c;
		}

IL_002f:
		{
			; // IL_002f: leave IL_0034
		}
	} // end catch (depth: 1)

IL_0034:
	{
		V_2 = (bool)0;
		V_3 = (Il2CppObject*)NULL;
	}

IL_0038:
	try
	{ // begin try (depth: 1)
		{
			Il2CppObject * L_6 = V_0;
			if (L_6)
			{
				goto IL_0067;
			}
		}

IL_0043:
		{
			Il2CppObject* L_7 = (Il2CppObject*)__this->get_nullKeySubject_4();
			if (L_7)
			{
				goto IL_005b;
			}
		}

IL_004e:
		{
			Subject_1_t2775141040 * L_8 = (Subject_1_t2775141040 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9));
			((  void (*) (Subject_1_t2775141040 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10)->method)(L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10));
			__this->set_nullKeySubject_4(L_8);
			V_2 = (bool)1;
		}

IL_005b:
		{
			Il2CppObject* L_9 = (Il2CppObject*)__this->get_nullKeySubject_4();
			V_3 = (Il2CppObject*)L_9;
			goto IL_008f;
		}

IL_0067:
		{
			Dictionary_2_t3341028665 * L_10 = (Dictionary_2_t3341028665 *)__this->get_map_3();
			Il2CppObject * L_11 = V_0;
			NullCheck((Dictionary_2_t3341028665 *)L_10);
			bool L_12 = VirtFuncInvoker2< bool, Il2CppObject *, Il2CppObject** >::Invoke(21 /* System.Boolean System.Collections.Generic.Dictionary`2<System.Object,UniRx.ISubject`1<System.Object>>::TryGetValue(!0,!1&) */, (Dictionary_2_t3341028665 *)L_10, (Il2CppObject *)L_11, (Il2CppObject**)(&V_3));
			if (L_12)
			{
				goto IL_008f;
			}
		}

IL_007a:
		{
			Subject_1_t2775141040 * L_13 = (Subject_1_t2775141040 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9));
			((  void (*) (Subject_1_t2775141040 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10)->method)(L_13, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10));
			V_3 = (Il2CppObject*)L_13;
			Dictionary_2_t3341028665 * L_14 = (Dictionary_2_t3341028665 *)__this->get_map_3();
			Il2CppObject * L_15 = V_0;
			Il2CppObject* L_16 = V_3;
			NullCheck((Dictionary_2_t3341028665 *)L_14);
			VirtActionInvoker2< Il2CppObject *, Il2CppObject* >::Invoke(18 /* System.Void System.Collections.Generic.Dictionary`2<System.Object,UniRx.ISubject`1<System.Object>>::Add(!0,!1) */, (Dictionary_2_t3341028665 *)L_14, (Il2CppObject *)L_15, (Il2CppObject*)L_16);
			V_2 = (bool)1;
		}

IL_008f:
		{
			goto IL_00a8;
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1967233988 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Exception_t1967233988_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_0094;
		throw e;
	}

CATCH_0094:
	{ // begin catch(System.Exception)
		{
			V_4 = (Exception_t1967233988 *)((Exception_t1967233988 *)__exception_local);
			Exception_t1967233988 * L_17 = V_4;
			NullCheck((GroupBy_t3470189943 *)__this);
			((  void (*) (GroupBy_t3470189943 *, Exception_t1967233988 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7)->method)((GroupBy_t3470189943 *)__this, (Exception_t1967233988 *)L_17, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7));
			goto IL_010c;
		}

IL_00a3:
		{
			; // IL_00a3: leave IL_00a8
		}
	} // end catch (depth: 1)

IL_00a8:
	{
		bool L_18 = V_2;
		if (!L_18)
		{
			goto IL_00cc;
		}
	}
	{
		Il2CppObject * L_19 = V_0;
		Il2CppObject* L_20 = V_3;
		RefCountDisposable_t3288429070 * L_21 = (RefCountDisposable_t3288429070 *)__this->get_refCountDisposable_6();
		GroupedObservable_2_t2613254135 * L_22 = (GroupedObservable_2_t2613254135 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 13));
		((  void (*) (GroupedObservable_2_t2613254135 *, Il2CppObject *, Il2CppObject*, RefCountDisposable_t3288429070 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 14)->method)(L_22, (Il2CppObject *)L_19, (Il2CppObject*)L_20, (RefCountDisposable_t3288429070 *)L_21, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 14));
		V_5 = (GroupedObservable_2_t2613254135 *)L_22;
		Il2CppObject* L_23 = (Il2CppObject*)((OperatorObserverBase_2_t1657047628 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		GroupedObservable_2_t2613254135 * L_24 = V_5;
		NullCheck((Il2CppObject*)L_23);
		InterfaceActionInvoker1< Il2CppObject* >::Invoke(2 /* System.Void UniRx.IObserver`1<UniRx.IGroupedObservable`2<System.Object,System.Object>>::OnNext(T) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 15), (Il2CppObject*)L_23, (Il2CppObject*)L_24);
	}

IL_00cc:
	{
		Initobj (Il2CppObject_il2cpp_TypeInfo_var, (&V_9));
		Il2CppObject * L_25 = V_9;
		V_6 = (Il2CppObject *)L_25;
	}

IL_00d8:
	try
	{ // begin try (depth: 1)
		GroupByObservable_3_t657897610 * L_26 = (GroupByObservable_3_t657897610 *)__this->get_parent_2();
		NullCheck(L_26);
		Func_2_t2135783352 * L_27 = (Func_2_t2135783352 *)L_26->get_elementSelector_3();
		Il2CppObject * L_28 = ___value0;
		NullCheck((Func_2_t2135783352 *)L_27);
		Il2CppObject * L_29 = ((  Il2CppObject * (*) (Func_2_t2135783352 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 16)->method)((Func_2_t2135783352 *)L_27, (Il2CppObject *)L_28, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 16));
		V_6 = (Il2CppObject *)L_29;
		goto IL_0104;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1967233988 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Exception_t1967233988_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_00f0;
		throw e;
	}

CATCH_00f0:
	{ // begin catch(System.Exception)
		{
			V_7 = (Exception_t1967233988 *)((Exception_t1967233988 *)__exception_local);
			Exception_t1967233988 * L_30 = V_7;
			NullCheck((GroupBy_t3470189943 *)__this);
			((  void (*) (GroupBy_t3470189943 *, Exception_t1967233988 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7)->method)((GroupBy_t3470189943 *)__this, (Exception_t1967233988 *)L_30, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7));
			goto IL_010c;
		}

IL_00ff:
		{
			; // IL_00ff: leave IL_0104
		}
	} // end catch (depth: 1)

IL_0104:
	{
		Il2CppObject* L_31 = V_3;
		Il2CppObject * L_32 = V_6;
		NullCheck((Il2CppObject*)L_31);
		InterfaceActionInvoker1< Il2CppObject * >::Invoke(2 /* System.Void UniRx.IObserver`1<System.Object>::OnNext(T) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 17), (Il2CppObject*)L_31, (Il2CppObject *)L_32);
	}

IL_010c:
	{
		return;
	}
}
// System.Void UniRx.Operators.GroupByObservable`3/GroupBy<System.Object,System.Object,System.Object>::OnError(System.Exception)
extern "C"  void GroupBy_OnError_m4077176042_gshared (GroupBy_t3470189943 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method)
{
	{
		Exception_t1967233988 * L_0 = ___error0;
		NullCheck((GroupBy_t3470189943 *)__this);
		((  void (*) (GroupBy_t3470189943 *, Exception_t1967233988 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7)->method)((GroupBy_t3470189943 *)__this, (Exception_t1967233988 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7));
		return;
	}
}
// System.Void UniRx.Operators.GroupByObservable`3/GroupBy<System.Object,System.Object,System.Object>::OnCompleted()
extern Il2CppClass* IDisposable_t1628921374_il2cpp_TypeInfo_var;
extern const uint32_t GroupBy_OnCompleted_m3119777597_MetadataUsageId;
extern "C"  void GroupBy_OnCompleted_m3119777597_gshared (GroupBy_t3470189943 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GroupBy_OnCompleted_m3119777597_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Il2CppObject* V_0 = NULL;
	Enumerator_t3108056607  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		{
			Il2CppObject* L_0 = (Il2CppObject*)__this->get_nullKeySubject_4();
			if (!L_0)
			{
				goto IL_0016;
			}
		}

IL_000b:
		{
			Il2CppObject* L_1 = (Il2CppObject*)__this->get_nullKeySubject_4();
			NullCheck((Il2CppObject*)L_1);
			InterfaceActionInvoker0::Invoke(0 /* System.Void UniRx.IObserver`1<System.Object>::OnCompleted() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 17), (Il2CppObject*)L_1);
		}

IL_0016:
		{
			Dictionary_2_t3341028665 * L_2 = (Dictionary_2_t3341028665 *)__this->get_map_3();
			NullCheck((Dictionary_2_t3341028665 *)L_2);
			ValueCollection_t968198463 * L_3 = ((  ValueCollection_t968198463 * (*) (Dictionary_2_t3341028665 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 18)->method)((Dictionary_2_t3341028665 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 18));
			NullCheck((ValueCollection_t968198463 *)L_3);
			Enumerator_t3108056607  L_4 = ((  Enumerator_t3108056607  (*) (ValueCollection_t968198463 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 19)->method)((ValueCollection_t968198463 *)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 19));
			V_1 = (Enumerator_t3108056607 )L_4;
		}

IL_0027:
		try
		{ // begin try (depth: 2)
			{
				goto IL_003a;
			}

IL_002c:
			{
				Il2CppObject* L_5 = ((  Il2CppObject* (*) (Enumerator_t3108056607 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 20)->method)((Enumerator_t3108056607 *)(&V_1), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 20));
				V_0 = (Il2CppObject*)L_5;
				Il2CppObject* L_6 = V_0;
				NullCheck((Il2CppObject*)L_6);
				InterfaceActionInvoker0::Invoke(0 /* System.Void UniRx.IObserver`1<System.Object>::OnCompleted() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 17), (Il2CppObject*)L_6);
			}

IL_003a:
			{
				bool L_7 = ((  bool (*) (Enumerator_t3108056607 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 21)->method)((Enumerator_t3108056607 *)(&V_1), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 21));
				if (L_7)
				{
					goto IL_002c;
				}
			}

IL_0046:
			{
				IL2CPP_LEAVE(0x57, FINALLY_004b);
			}
		} // end try (depth: 2)
		catch(Il2CppExceptionWrapper& e)
		{
			__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
			goto FINALLY_004b;
		}

FINALLY_004b:
		{ // begin finally (depth: 2)
			Enumerator_t3108056607  L_8 = V_1;
			Enumerator_t3108056607  L_9 = L_8;
			Il2CppObject * L_10 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 22), &L_9);
			NullCheck((Il2CppObject *)L_10);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, (Il2CppObject *)L_10);
			IL2CPP_END_FINALLY(75)
		} // end finally (depth: 2)
		IL2CPP_CLEANUP(75)
		{
			IL2CPP_JUMP_TBL(0x57, IL_0057)
			IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
		}

IL_0057:
		{
			Il2CppObject* L_11 = (Il2CppObject*)((OperatorObserverBase_2_t1657047628 *)__this)->get_observer_0();
			il2cpp_codegen_memory_barrier();
			NullCheck((Il2CppObject*)L_11);
			InterfaceActionInvoker0::Invoke(0 /* System.Void UniRx.IObserver`1<UniRx.IGroupedObservable`2<System.Object,System.Object>>::OnCompleted() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 15), (Il2CppObject*)L_11);
			IL2CPP_LEAVE(0x70, FINALLY_0069);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0069;
	}

FINALLY_0069:
	{ // begin finally (depth: 1)
		NullCheck((OperatorObserverBase_2_t1657047628 *)__this);
		VirtActionInvoker0::Invoke(4 /* System.Void UniRx.Operators.OperatorObserverBase`2<System.Object,UniRx.IGroupedObservable`2<System.Object,System.Object>>::Dispose() */, (OperatorObserverBase_2_t1657047628 *)__this);
		IL2CPP_END_FINALLY(105)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(105)
	{
		IL2CPP_JUMP_TBL(0x70, IL_0070)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0070:
	{
		return;
	}
}
// System.Void UniRx.Operators.GroupByObservable`3/GroupBy<System.Object,System.Object,System.Object>::Error(System.Exception)
extern Il2CppClass* IDisposable_t1628921374_il2cpp_TypeInfo_var;
extern const uint32_t GroupBy_Error_m2267572681_MetadataUsageId;
extern "C"  void GroupBy_Error_m2267572681_gshared (GroupBy_t3470189943 * __this, Exception_t1967233988 * ___exception0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GroupBy_Error_m2267572681_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Il2CppObject* V_0 = NULL;
	Enumerator_t3108056607  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		{
			Il2CppObject* L_0 = (Il2CppObject*)__this->get_nullKeySubject_4();
			if (!L_0)
			{
				goto IL_0017;
			}
		}

IL_000b:
		{
			Il2CppObject* L_1 = (Il2CppObject*)__this->get_nullKeySubject_4();
			Exception_t1967233988 * L_2 = ___exception0;
			NullCheck((Il2CppObject*)L_1);
			InterfaceActionInvoker1< Exception_t1967233988 * >::Invoke(1 /* System.Void UniRx.IObserver`1<System.Object>::OnError(System.Exception) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 17), (Il2CppObject*)L_1, (Exception_t1967233988 *)L_2);
		}

IL_0017:
		{
			Dictionary_2_t3341028665 * L_3 = (Dictionary_2_t3341028665 *)__this->get_map_3();
			NullCheck((Dictionary_2_t3341028665 *)L_3);
			ValueCollection_t968198463 * L_4 = ((  ValueCollection_t968198463 * (*) (Dictionary_2_t3341028665 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 18)->method)((Dictionary_2_t3341028665 *)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 18));
			NullCheck((ValueCollection_t968198463 *)L_4);
			Enumerator_t3108056607  L_5 = ((  Enumerator_t3108056607  (*) (ValueCollection_t968198463 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 19)->method)((ValueCollection_t968198463 *)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 19));
			V_1 = (Enumerator_t3108056607 )L_5;
		}

IL_0028:
		try
		{ // begin try (depth: 2)
			{
				goto IL_003c;
			}

IL_002d:
			{
				Il2CppObject* L_6 = ((  Il2CppObject* (*) (Enumerator_t3108056607 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 20)->method)((Enumerator_t3108056607 *)(&V_1), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 20));
				V_0 = (Il2CppObject*)L_6;
				Il2CppObject* L_7 = V_0;
				Exception_t1967233988 * L_8 = ___exception0;
				NullCheck((Il2CppObject*)L_7);
				InterfaceActionInvoker1< Exception_t1967233988 * >::Invoke(1 /* System.Void UniRx.IObserver`1<System.Object>::OnError(System.Exception) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 17), (Il2CppObject*)L_7, (Exception_t1967233988 *)L_8);
			}

IL_003c:
			{
				bool L_9 = ((  bool (*) (Enumerator_t3108056607 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 21)->method)((Enumerator_t3108056607 *)(&V_1), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 21));
				if (L_9)
				{
					goto IL_002d;
				}
			}

IL_0048:
			{
				IL2CPP_LEAVE(0x59, FINALLY_004d);
			}
		} // end try (depth: 2)
		catch(Il2CppExceptionWrapper& e)
		{
			__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
			goto FINALLY_004d;
		}

FINALLY_004d:
		{ // begin finally (depth: 2)
			Enumerator_t3108056607  L_10 = V_1;
			Enumerator_t3108056607  L_11 = L_10;
			Il2CppObject * L_12 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 22), &L_11);
			NullCheck((Il2CppObject *)L_12);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, (Il2CppObject *)L_12);
			IL2CPP_END_FINALLY(77)
		} // end finally (depth: 2)
		IL2CPP_CLEANUP(77)
		{
			IL2CPP_JUMP_TBL(0x59, IL_0059)
			IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
		}

IL_0059:
		{
			Il2CppObject* L_13 = (Il2CppObject*)((OperatorObserverBase_2_t1657047628 *)__this)->get_observer_0();
			il2cpp_codegen_memory_barrier();
			Exception_t1967233988 * L_14 = ___exception0;
			NullCheck((Il2CppObject*)L_13);
			InterfaceActionInvoker1< Exception_t1967233988 * >::Invoke(1 /* System.Void UniRx.IObserver`1<UniRx.IGroupedObservable`2<System.Object,System.Object>>::OnError(System.Exception) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 15), (Il2CppObject*)L_13, (Exception_t1967233988 *)L_14);
			IL2CPP_LEAVE(0x73, FINALLY_006c);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_006c;
	}

FINALLY_006c:
	{ // begin finally (depth: 1)
		NullCheck((OperatorObserverBase_2_t1657047628 *)__this);
		VirtActionInvoker0::Invoke(4 /* System.Void UniRx.Operators.OperatorObserverBase`2<System.Object,UniRx.IGroupedObservable`2<System.Object,System.Object>>::Dispose() */, (OperatorObserverBase_2_t1657047628 *)__this);
		IL2CPP_END_FINALLY(108)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(108)
	{
		IL2CPP_JUMP_TBL(0x73, IL_0073)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0073:
	{
		return;
	}
}
// System.Void UniRx.Operators.GroupByObservable`3<System.Object,System.Object,System.Object>::.ctor(UniRx.IObservable`1<TSource>,System.Func`2<TSource,TKey>,System.Func`2<TSource,TElement>,System.Nullable`1<System.Int32>,System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C"  void GroupByObservable_3__ctor_m2517188728_gshared (GroupByObservable_3_t657897610 * __this, Il2CppObject* ___source0, Func_2_t2135783352 * ___keySelector1, Func_2_t2135783352 * ___elementSelector2, Nullable_1_t1438485399  ___capacity3, Il2CppObject* ___comparer4, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = ___source0;
		bool L_1 = ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)(NULL /*static, unused*/, (Il2CppObject*)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		NullCheck((OperatorObservableBase_1_t370530870 *)__this);
		((  void (*) (OperatorObservableBase_1_t370530870 *, bool, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)((OperatorObservableBase_1_t370530870 *)__this, (bool)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		Il2CppObject* L_2 = ___source0;
		__this->set_source_1(L_2);
		Func_2_t2135783352 * L_3 = ___keySelector1;
		__this->set_keySelector_2(L_3);
		Func_2_t2135783352 * L_4 = ___elementSelector2;
		__this->set_elementSelector_3(L_4);
		Nullable_1_t1438485399  L_5 = ___capacity3;
		__this->set_capacity_4(L_5);
		Il2CppObject* L_6 = ___comparer4;
		__this->set_comparer_5(L_6);
		return;
	}
}
// System.IDisposable UniRx.Operators.GroupByObservable`3<System.Object,System.Object,System.Object>::SubscribeCore(UniRx.IObserver`1<UniRx.IGroupedObservable`2<TKey,TElement>>,System.IDisposable)
extern "C"  Il2CppObject * GroupByObservable_3_SubscribeCore_m2362185847_gshared (GroupByObservable_3_t657897610 * __this, Il2CppObject* ___observer0, Il2CppObject * ___cancel1, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = ___observer0;
		Il2CppObject * L_1 = ___cancel1;
		GroupBy_t3470189943 * L_2 = (GroupBy_t3470189943 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		((  void (*) (GroupBy_t3470189943 *, GroupByObservable_3_t657897610 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)(L_2, (GroupByObservable_3_t657897610 *)__this, (Il2CppObject*)L_0, (Il2CppObject *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		NullCheck((GroupBy_t3470189943 *)L_2);
		Il2CppObject * L_3 = ((  Il2CppObject * (*) (GroupBy_t3470189943 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->method)((GroupBy_t3470189943 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		return L_3;
	}
}
// System.Void UniRx.Operators.GroupedObservable`2<System.Object,System.Object>::.ctor(TKey,UniRx.ISubject`1<TElement>,UniRx.RefCountDisposable)
extern "C"  void GroupedObservable_2__ctor_m1743351720_gshared (GroupedObservable_2_t2613254135 * __this, Il2CppObject * ___key0, Il2CppObject* ___subject1, RefCountDisposable_t3288429070 * ___refCount2, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		Il2CppObject * L_0 = ___key0;
		__this->set_key_0(L_0);
		Il2CppObject* L_1 = ___subject1;
		__this->set_subject_1(L_1);
		RefCountDisposable_t3288429070 * L_2 = ___refCount2;
		__this->set_refCount_2(L_2);
		return;
	}
}
// TKey UniRx.Operators.GroupedObservable`2<System.Object,System.Object>::get_Key()
extern "C"  Il2CppObject * GroupedObservable_2_get_Key_m665794075_gshared (GroupedObservable_2_t2613254135 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_key_0();
		return L_0;
	}
}
// System.IDisposable UniRx.Operators.GroupedObservable`2<System.Object,System.Object>::Subscribe(UniRx.IObserver`1<TElement>)
extern "C"  Il2CppObject * GroupedObservable_2_Subscribe_m3495507058_gshared (GroupedObservable_2_t2613254135 * __this, Il2CppObject* ___observer0, const MethodInfo* method)
{
	Il2CppObject * V_0 = NULL;
	Il2CppObject * V_1 = NULL;
	{
		RefCountDisposable_t3288429070 * L_0 = (RefCountDisposable_t3288429070 *)__this->get_refCount_2();
		NullCheck((RefCountDisposable_t3288429070 *)L_0);
		Il2CppObject * L_1 = RefCountDisposable_GetDisposable_m4263689044((RefCountDisposable_t3288429070 *)L_0, /*hidden argument*/NULL);
		V_0 = (Il2CppObject *)L_1;
		Il2CppObject* L_2 = (Il2CppObject*)__this->get_subject_1();
		Il2CppObject* L_3 = ___observer0;
		NullCheck((Il2CppObject*)L_2);
		Il2CppObject * L_4 = InterfaceFuncInvoker1< Il2CppObject *, Il2CppObject* >::Invoke(0 /* System.IDisposable UniRx.IObservable`1<System.Object>::Subscribe(UniRx.IObserver`1<T>) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0), (Il2CppObject*)L_2, (Il2CppObject*)L_3);
		V_1 = (Il2CppObject *)L_4;
		Il2CppObject * L_5 = V_0;
		Il2CppObject * L_6 = V_1;
		Il2CppObject * L_7 = StableCompositeDisposable_Create_m3764788535(NULL /*static, unused*/, (Il2CppObject *)L_5, (Il2CppObject *)L_6, /*hidden argument*/NULL);
		return L_7;
	}
}
// System.Void UniRx.Operators.IgnoreElementsObservable`1/IgnoreElements<System.Object>::.ctor(UniRx.IObserver`1<T>,System.IDisposable)
extern "C"  void IgnoreElements__ctor_m1978479027_gshared (IgnoreElements_t1769081498 * __this, Il2CppObject* ___observer0, Il2CppObject * ___cancel1, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = ___observer0;
		Il2CppObject * L_1 = ___cancel1;
		NullCheck((OperatorObserverBase_2_t1187768149 *)__this);
		((  void (*) (OperatorObserverBase_2_t1187768149 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((OperatorObserverBase_2_t1187768149 *)__this, (Il2CppObject*)L_0, (Il2CppObject *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		return;
	}
}
// System.Void UniRx.Operators.IgnoreElementsObservable`1/IgnoreElements<System.Object>::OnNext(T)
extern "C"  void IgnoreElements_OnNext_m353660415_gshared (IgnoreElements_t1769081498 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void UniRx.Operators.IgnoreElementsObservable`1/IgnoreElements<System.Object>::OnError(System.Exception)
extern "C"  void IgnoreElements_OnError_m85109518_gshared (IgnoreElements_t1769081498 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method)
{
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		Il2CppObject* L_0 = (Il2CppObject*)((OperatorObserverBase_2_t1187768149 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		Exception_t1967233988 * L_1 = ___error0;
		NullCheck((Il2CppObject*)L_0);
		InterfaceActionInvoker1< Exception_t1967233988 * >::Invoke(1 /* System.Void UniRx.IObserver`1<System.Object>::OnError(System.Exception) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), (Il2CppObject*)L_0, (Exception_t1967233988 *)L_1);
		IL2CPP_LEAVE(0x1A, FINALLY_0013);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0013;
	}

FINALLY_0013:
	{ // begin finally (depth: 1)
		NullCheck((OperatorObserverBase_2_t1187768149 *)__this);
		VirtActionInvoker0::Invoke(4 /* System.Void UniRx.Operators.OperatorObserverBase`2<System.Object,System.Object>::Dispose() */, (OperatorObserverBase_2_t1187768149 *)__this);
		IL2CPP_END_FINALLY(19)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(19)
	{
		IL2CPP_JUMP_TBL(0x1A, IL_001a)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_001a:
	{
		return;
	}
}
// System.Void UniRx.Operators.IgnoreElementsObservable`1/IgnoreElements<System.Object>::OnCompleted()
extern "C"  void IgnoreElements_OnCompleted_m3459187553_gshared (IgnoreElements_t1769081498 * __this, const MethodInfo* method)
{
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		Il2CppObject* L_0 = (Il2CppObject*)((OperatorObserverBase_2_t1187768149 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		NullCheck((Il2CppObject*)L_0);
		InterfaceActionInvoker0::Invoke(0 /* System.Void UniRx.IObserver`1<System.Object>::OnCompleted() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), (Il2CppObject*)L_0);
		IL2CPP_LEAVE(0x19, FINALLY_0012);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0012;
	}

FINALLY_0012:
	{ // begin finally (depth: 1)
		NullCheck((OperatorObserverBase_2_t1187768149 *)__this);
		VirtActionInvoker0::Invoke(4 /* System.Void UniRx.Operators.OperatorObserverBase`2<System.Object,System.Object>::Dispose() */, (OperatorObserverBase_2_t1187768149 *)__this);
		IL2CPP_END_FINALLY(18)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(18)
	{
		IL2CPP_JUMP_TBL(0x19, IL_0019)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0019:
	{
		return;
	}
}
// System.Void UniRx.Operators.IgnoreElementsObservable`1<System.Object>::.ctor(UniRx.IObservable`1<T>)
extern "C"  void IgnoreElementsObservable_1__ctor_m1260378830_gshared (IgnoreElementsObservable_1_t2767931315 * __this, Il2CppObject* ___source0, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = ___source0;
		bool L_1 = ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)(NULL /*static, unused*/, (Il2CppObject*)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		NullCheck((OperatorObservableBase_1_t4196218687 *)__this);
		((  void (*) (OperatorObservableBase_1_t4196218687 *, bool, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)((OperatorObservableBase_1_t4196218687 *)__this, (bool)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		Il2CppObject* L_2 = ___source0;
		__this->set_source_1(L_2);
		return;
	}
}
// System.IDisposable UniRx.Operators.IgnoreElementsObservable`1<System.Object>::SubscribeCore(UniRx.IObserver`1<T>,System.IDisposable)
extern "C"  Il2CppObject * IgnoreElementsObservable_1_SubscribeCore_m2243093241_gshared (IgnoreElementsObservable_1_t2767931315 * __this, Il2CppObject* ___observer0, Il2CppObject * ___cancel1, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = (Il2CppObject*)__this->get_source_1();
		Il2CppObject* L_1 = ___observer0;
		Il2CppObject * L_2 = ___cancel1;
		IgnoreElements_t1769081498 * L_3 = (IgnoreElements_t1769081498 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		((  void (*) (IgnoreElements_t1769081498 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)(L_3, (Il2CppObject*)L_1, (Il2CppObject *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		NullCheck((Il2CppObject*)L_0);
		Il2CppObject * L_4 = InterfaceFuncInvoker1< Il2CppObject *, Il2CppObject* >::Invoke(0 /* System.IDisposable UniRx.IObservable`1<System.Object>::Subscribe(UniRx.IObserver`1<T>) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5), (Il2CppObject*)L_0, (Il2CppObject*)L_3);
		return L_4;
	}
}
// System.Void UniRx.Operators.LastObservable`1/Last_<System.Object>::.ctor(UniRx.Operators.LastObservable`1<T>,UniRx.IObserver`1<T>,System.IDisposable)
extern "C"  void Last___ctor_m1155955451_gshared (Last__t3329173242 * __this, LastObservable_1_t3225858136 * ___parent0, Il2CppObject* ___observer1, Il2CppObject * ___cancel2, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = ___observer1;
		Il2CppObject * L_1 = ___cancel2;
		NullCheck((OperatorObserverBase_2_t1187768149 *)__this);
		((  void (*) (OperatorObserverBase_2_t1187768149 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((OperatorObserverBase_2_t1187768149 *)__this, (Il2CppObject*)L_0, (Il2CppObject *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		LastObservable_1_t3225858136 * L_2 = ___parent0;
		__this->set_parent_2(L_2);
		__this->set_notPublished_3((bool)1);
		return;
	}
}
// System.Void UniRx.Operators.LastObservable`1/Last_<System.Object>::OnNext(T)
extern Il2CppClass* Exception_t1967233988_il2cpp_TypeInfo_var;
extern const uint32_t Last__OnNext_m2733218846_MetadataUsageId;
extern "C"  void Last__OnNext_m2733218846_gshared (Last__t3329173242 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Last__OnNext_m2733218846_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	bool V_0 = false;
	Exception_t1967233988 * V_1 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		LastObservable_1_t3225858136 * L_0 = (LastObservable_1_t3225858136 *)__this->get_parent_2();
		NullCheck(L_0);
		Func_2_t1509682273 * L_1 = (Func_2_t1509682273 *)L_0->get_predicate_3();
		Il2CppObject * L_2 = ___value0;
		NullCheck((Func_2_t1509682273 *)L_1);
		bool L_3 = ((  bool (*) (Func_2_t1509682273 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)((Func_2_t1509682273 *)L_1, (Il2CppObject *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		V_0 = (bool)L_3;
		goto IL_003c;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1967233988 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Exception_t1967233988_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_0017;
		throw e;
	}

CATCH_0017:
	{ // begin catch(System.Exception)
		{
			V_1 = (Exception_t1967233988 *)((Exception_t1967233988 *)__exception_local);
		}

IL_0018:
		try
		{ // begin try (depth: 2)
			Il2CppObject* L_4 = (Il2CppObject*)((OperatorObserverBase_2_t1187768149 *)__this)->get_observer_0();
			il2cpp_codegen_memory_barrier();
			Exception_t1967233988 * L_5 = V_1;
			NullCheck((Il2CppObject*)L_4);
			InterfaceActionInvoker1< Exception_t1967233988 * >::Invoke(1 /* System.Void UniRx.IObserver`1<System.Object>::OnError(System.Exception) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3), (Il2CppObject*)L_4, (Exception_t1967233988 *)L_5);
			IL2CPP_LEAVE(0x32, FINALLY_002b);
		} // end try (depth: 2)
		catch(Il2CppExceptionWrapper& e)
		{
			__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
			goto FINALLY_002b;
		}

FINALLY_002b:
		{ // begin finally (depth: 2)
			NullCheck((OperatorObserverBase_2_t1187768149 *)__this);
			VirtActionInvoker0::Invoke(4 /* System.Void UniRx.Operators.OperatorObserverBase`2<System.Object,System.Object>::Dispose() */, (OperatorObserverBase_2_t1187768149 *)__this);
			IL2CPP_END_FINALLY(43)
		} // end finally (depth: 2)
		IL2CPP_CLEANUP(43)
		{
			IL2CPP_JUMP_TBL(0x32, IL_0032)
			IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
		}

IL_0032:
		{
			goto IL_0050;
		}

IL_0037:
		{
			; // IL_0037: leave IL_003c
		}
	} // end catch (depth: 1)

IL_003c:
	{
		bool L_6 = V_0;
		if (!L_6)
		{
			goto IL_0050;
		}
	}
	{
		__this->set_notPublished_3((bool)0);
		Il2CppObject * L_7 = ___value0;
		__this->set_lastValue_4(L_7);
	}

IL_0050:
	{
		return;
	}
}
// System.Void UniRx.Operators.LastObservable`1/Last_<System.Object>::OnError(System.Exception)
extern "C"  void Last__OnError_m2472393517_gshared (Last__t3329173242 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method)
{
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		Il2CppObject* L_0 = (Il2CppObject*)((OperatorObserverBase_2_t1187768149 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		Exception_t1967233988 * L_1 = ___error0;
		NullCheck((Il2CppObject*)L_0);
		InterfaceActionInvoker1< Exception_t1967233988 * >::Invoke(1 /* System.Void UniRx.IObserver`1<System.Object>::OnError(System.Exception) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3), (Il2CppObject*)L_0, (Exception_t1967233988 *)L_1);
		IL2CPP_LEAVE(0x1A, FINALLY_0013);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0013;
	}

FINALLY_0013:
	{ // begin finally (depth: 1)
		NullCheck((OperatorObserverBase_2_t1187768149 *)__this);
		VirtActionInvoker0::Invoke(4 /* System.Void UniRx.Operators.OperatorObserverBase`2<System.Object,System.Object>::Dispose() */, (OperatorObserverBase_2_t1187768149 *)__this);
		IL2CPP_END_FINALLY(19)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(19)
	{
		IL2CPP_JUMP_TBL(0x1A, IL_001a)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_001a:
	{
		return;
	}
}
// System.Void UniRx.Operators.LastObservable`1/Last_<System.Object>::OnCompleted()
extern Il2CppClass* Il2CppObject_il2cpp_TypeInfo_var;
extern Il2CppClass* InvalidOperationException_t2420574324_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2687971414;
extern const uint32_t Last__OnCompleted_m4084337152_MetadataUsageId;
extern "C"  void Last__OnCompleted_m4084337152_gshared (Last__t3329173242 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Last__OnCompleted_m4084337152_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Il2CppObject * V_0 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		LastObservable_1_t3225858136 * L_0 = (LastObservable_1_t3225858136 *)__this->get_parent_2();
		NullCheck(L_0);
		bool L_1 = (bool)L_0->get_useDefault_2();
		if (!L_1)
		{
			goto IL_0067;
		}
	}
	{
		bool L_2 = (bool)__this->get_notPublished_3();
		if (!L_2)
		{
			goto IL_0036;
		}
	}
	{
		Il2CppObject* L_3 = (Il2CppObject*)((OperatorObserverBase_2_t1187768149 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		Initobj (Il2CppObject_il2cpp_TypeInfo_var, (&V_0));
		Il2CppObject * L_4 = V_0;
		NullCheck((Il2CppObject*)L_3);
		InterfaceActionInvoker1< Il2CppObject * >::Invoke(2 /* System.Void UniRx.IObserver`1<System.Object>::OnNext(T) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3), (Il2CppObject*)L_3, (Il2CppObject *)L_4);
		goto IL_0049;
	}

IL_0036:
	{
		Il2CppObject* L_5 = (Il2CppObject*)((OperatorObserverBase_2_t1187768149 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		Il2CppObject * L_6 = (Il2CppObject *)__this->get_lastValue_4();
		NullCheck((Il2CppObject*)L_5);
		InterfaceActionInvoker1< Il2CppObject * >::Invoke(2 /* System.Void UniRx.IObserver`1<System.Object>::OnNext(T) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3), (Il2CppObject*)L_5, (Il2CppObject *)L_6);
	}

IL_0049:
	try
	{ // begin try (depth: 1)
		Il2CppObject* L_7 = (Il2CppObject*)((OperatorObserverBase_2_t1187768149 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		NullCheck((Il2CppObject*)L_7);
		InterfaceActionInvoker0::Invoke(0 /* System.Void UniRx.IObserver`1<System.Object>::OnCompleted() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3), (Il2CppObject*)L_7);
		IL2CPP_LEAVE(0x62, FINALLY_005b);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_005b;
	}

FINALLY_005b:
	{ // begin finally (depth: 1)
		NullCheck((OperatorObserverBase_2_t1187768149 *)__this);
		VirtActionInvoker0::Invoke(4 /* System.Void UniRx.Operators.OperatorObserverBase`2<System.Object,System.Object>::Dispose() */, (OperatorObserverBase_2_t1187768149 *)__this);
		IL2CPP_END_FINALLY(91)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(91)
	{
		IL2CPP_JUMP_TBL(0x62, IL_0062)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0062:
	{
		goto IL_00c6;
	}

IL_0067:
	{
		bool L_8 = (bool)__this->get_notPublished_3();
		if (!L_8)
		{
			goto IL_009a;
		}
	}

IL_0072:
	try
	{ // begin try (depth: 1)
		Il2CppObject* L_9 = (Il2CppObject*)((OperatorObserverBase_2_t1187768149 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		InvalidOperationException_t2420574324 * L_10 = (InvalidOperationException_t2420574324 *)il2cpp_codegen_object_new(InvalidOperationException_t2420574324_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_10, (String_t*)_stringLiteral2687971414, /*hidden argument*/NULL);
		NullCheck((Il2CppObject*)L_9);
		InterfaceActionInvoker1< Exception_t1967233988 * >::Invoke(1 /* System.Void UniRx.IObserver`1<System.Object>::OnError(System.Exception) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3), (Il2CppObject*)L_9, (Exception_t1967233988 *)L_10);
		IL2CPP_LEAVE(0x95, FINALLY_008e);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_008e;
	}

FINALLY_008e:
	{ // begin finally (depth: 1)
		NullCheck((OperatorObserverBase_2_t1187768149 *)__this);
		VirtActionInvoker0::Invoke(4 /* System.Void UniRx.Operators.OperatorObserverBase`2<System.Object,System.Object>::Dispose() */, (OperatorObserverBase_2_t1187768149 *)__this);
		IL2CPP_END_FINALLY(142)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(142)
	{
		IL2CPP_JUMP_TBL(0x95, IL_0095)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0095:
	{
		goto IL_00c6;
	}

IL_009a:
	{
		Il2CppObject* L_11 = (Il2CppObject*)((OperatorObserverBase_2_t1187768149 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		Il2CppObject * L_12 = (Il2CppObject *)__this->get_lastValue_4();
		NullCheck((Il2CppObject*)L_11);
		InterfaceActionInvoker1< Il2CppObject * >::Invoke(2 /* System.Void UniRx.IObserver`1<System.Object>::OnNext(T) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3), (Il2CppObject*)L_11, (Il2CppObject *)L_12);
	}

IL_00ad:
	try
	{ // begin try (depth: 1)
		Il2CppObject* L_13 = (Il2CppObject*)((OperatorObserverBase_2_t1187768149 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		NullCheck((Il2CppObject*)L_13);
		InterfaceActionInvoker0::Invoke(0 /* System.Void UniRx.IObserver`1<System.Object>::OnCompleted() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3), (Il2CppObject*)L_13);
		IL2CPP_LEAVE(0xC6, FINALLY_00bf);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_00bf;
	}

FINALLY_00bf:
	{ // begin finally (depth: 1)
		NullCheck((OperatorObserverBase_2_t1187768149 *)__this);
		VirtActionInvoker0::Invoke(4 /* System.Void UniRx.Operators.OperatorObserverBase`2<System.Object,System.Object>::Dispose() */, (OperatorObserverBase_2_t1187768149 *)__this);
		IL2CPP_END_FINALLY(191)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(191)
	{
		IL2CPP_JUMP_TBL(0xC6, IL_00c6)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_00c6:
	{
		return;
	}
}
// System.Void UniRx.Operators.LastObservable`1/Last<System.Object>::.ctor(UniRx.Operators.LastObservable`1<T>,UniRx.IObserver`1<T>,System.IDisposable)
extern "C"  void Last__ctor_m3920580506_gshared (Last_t3745612479 * __this, LastObservable_1_t3225858136 * ___parent0, Il2CppObject* ___observer1, Il2CppObject * ___cancel2, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = ___observer1;
		Il2CppObject * L_1 = ___cancel2;
		NullCheck((OperatorObserverBase_2_t1187768149 *)__this);
		((  void (*) (OperatorObserverBase_2_t1187768149 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((OperatorObserverBase_2_t1187768149 *)__this, (Il2CppObject*)L_0, (Il2CppObject *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		LastObservable_1_t3225858136 * L_2 = ___parent0;
		__this->set_parent_2(L_2);
		__this->set_notPublished_3((bool)1);
		return;
	}
}
// System.Void UniRx.Operators.LastObservable`1/Last<System.Object>::OnNext(T)
extern "C"  void Last_OnNext_m1586024607_gshared (Last_t3745612479 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	{
		__this->set_notPublished_3((bool)0);
		Il2CppObject * L_0 = ___value0;
		__this->set_lastValue_4(L_0);
		return;
	}
}
// System.Void UniRx.Operators.LastObservable`1/Last<System.Object>::OnError(System.Exception)
extern "C"  void Last_OnError_m1471008174_gshared (Last_t3745612479 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method)
{
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		Il2CppObject* L_0 = (Il2CppObject*)((OperatorObserverBase_2_t1187768149 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		Exception_t1967233988 * L_1 = ___error0;
		NullCheck((Il2CppObject*)L_0);
		InterfaceActionInvoker1< Exception_t1967233988 * >::Invoke(1 /* System.Void UniRx.IObserver`1<System.Object>::OnError(System.Exception) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), (Il2CppObject*)L_0, (Exception_t1967233988 *)L_1);
		IL2CPP_LEAVE(0x1A, FINALLY_0013);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0013;
	}

FINALLY_0013:
	{ // begin finally (depth: 1)
		NullCheck((OperatorObserverBase_2_t1187768149 *)__this);
		VirtActionInvoker0::Invoke(4 /* System.Void UniRx.Operators.OperatorObserverBase`2<System.Object,System.Object>::Dispose() */, (OperatorObserverBase_2_t1187768149 *)__this);
		IL2CPP_END_FINALLY(19)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(19)
	{
		IL2CPP_JUMP_TBL(0x1A, IL_001a)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_001a:
	{
		return;
	}
}
// System.Void UniRx.Operators.LastObservable`1/Last<System.Object>::OnCompleted()
extern Il2CppClass* Il2CppObject_il2cpp_TypeInfo_var;
extern Il2CppClass* InvalidOperationException_t2420574324_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2687971414;
extern const uint32_t Last_OnCompleted_m2876315137_MetadataUsageId;
extern "C"  void Last_OnCompleted_m2876315137_gshared (Last_t3745612479 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Last_OnCompleted_m2876315137_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Il2CppObject * V_0 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		LastObservable_1_t3225858136 * L_0 = (LastObservable_1_t3225858136 *)__this->get_parent_2();
		NullCheck(L_0);
		bool L_1 = (bool)L_0->get_useDefault_2();
		if (!L_1)
		{
			goto IL_0067;
		}
	}
	{
		bool L_2 = (bool)__this->get_notPublished_3();
		if (!L_2)
		{
			goto IL_0036;
		}
	}
	{
		Il2CppObject* L_3 = (Il2CppObject*)((OperatorObserverBase_2_t1187768149 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		Initobj (Il2CppObject_il2cpp_TypeInfo_var, (&V_0));
		Il2CppObject * L_4 = V_0;
		NullCheck((Il2CppObject*)L_3);
		InterfaceActionInvoker1< Il2CppObject * >::Invoke(2 /* System.Void UniRx.IObserver`1<System.Object>::OnNext(T) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), (Il2CppObject*)L_3, (Il2CppObject *)L_4);
		goto IL_0049;
	}

IL_0036:
	{
		Il2CppObject* L_5 = (Il2CppObject*)((OperatorObserverBase_2_t1187768149 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		Il2CppObject * L_6 = (Il2CppObject *)__this->get_lastValue_4();
		NullCheck((Il2CppObject*)L_5);
		InterfaceActionInvoker1< Il2CppObject * >::Invoke(2 /* System.Void UniRx.IObserver`1<System.Object>::OnNext(T) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), (Il2CppObject*)L_5, (Il2CppObject *)L_6);
	}

IL_0049:
	try
	{ // begin try (depth: 1)
		Il2CppObject* L_7 = (Il2CppObject*)((OperatorObserverBase_2_t1187768149 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		NullCheck((Il2CppObject*)L_7);
		InterfaceActionInvoker0::Invoke(0 /* System.Void UniRx.IObserver`1<System.Object>::OnCompleted() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), (Il2CppObject*)L_7);
		IL2CPP_LEAVE(0x62, FINALLY_005b);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_005b;
	}

FINALLY_005b:
	{ // begin finally (depth: 1)
		NullCheck((OperatorObserverBase_2_t1187768149 *)__this);
		VirtActionInvoker0::Invoke(4 /* System.Void UniRx.Operators.OperatorObserverBase`2<System.Object,System.Object>::Dispose() */, (OperatorObserverBase_2_t1187768149 *)__this);
		IL2CPP_END_FINALLY(91)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(91)
	{
		IL2CPP_JUMP_TBL(0x62, IL_0062)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0062:
	{
		goto IL_00c6;
	}

IL_0067:
	{
		bool L_8 = (bool)__this->get_notPublished_3();
		if (!L_8)
		{
			goto IL_009a;
		}
	}

IL_0072:
	try
	{ // begin try (depth: 1)
		Il2CppObject* L_9 = (Il2CppObject*)((OperatorObserverBase_2_t1187768149 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		InvalidOperationException_t2420574324 * L_10 = (InvalidOperationException_t2420574324 *)il2cpp_codegen_object_new(InvalidOperationException_t2420574324_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_10, (String_t*)_stringLiteral2687971414, /*hidden argument*/NULL);
		NullCheck((Il2CppObject*)L_9);
		InterfaceActionInvoker1< Exception_t1967233988 * >::Invoke(1 /* System.Void UniRx.IObserver`1<System.Object>::OnError(System.Exception) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), (Il2CppObject*)L_9, (Exception_t1967233988 *)L_10);
		IL2CPP_LEAVE(0x95, FINALLY_008e);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_008e;
	}

FINALLY_008e:
	{ // begin finally (depth: 1)
		NullCheck((OperatorObserverBase_2_t1187768149 *)__this);
		VirtActionInvoker0::Invoke(4 /* System.Void UniRx.Operators.OperatorObserverBase`2<System.Object,System.Object>::Dispose() */, (OperatorObserverBase_2_t1187768149 *)__this);
		IL2CPP_END_FINALLY(142)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(142)
	{
		IL2CPP_JUMP_TBL(0x95, IL_0095)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0095:
	{
		goto IL_00c6;
	}

IL_009a:
	{
		Il2CppObject* L_11 = (Il2CppObject*)((OperatorObserverBase_2_t1187768149 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		Il2CppObject * L_12 = (Il2CppObject *)__this->get_lastValue_4();
		NullCheck((Il2CppObject*)L_11);
		InterfaceActionInvoker1< Il2CppObject * >::Invoke(2 /* System.Void UniRx.IObserver`1<System.Object>::OnNext(T) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), (Il2CppObject*)L_11, (Il2CppObject *)L_12);
	}

IL_00ad:
	try
	{ // begin try (depth: 1)
		Il2CppObject* L_13 = (Il2CppObject*)((OperatorObserverBase_2_t1187768149 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		NullCheck((Il2CppObject*)L_13);
		InterfaceActionInvoker0::Invoke(0 /* System.Void UniRx.IObserver`1<System.Object>::OnCompleted() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), (Il2CppObject*)L_13);
		IL2CPP_LEAVE(0xC6, FINALLY_00bf);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_00bf;
	}

FINALLY_00bf:
	{ // begin finally (depth: 1)
		NullCheck((OperatorObserverBase_2_t1187768149 *)__this);
		VirtActionInvoker0::Invoke(4 /* System.Void UniRx.Operators.OperatorObserverBase`2<System.Object,System.Object>::Dispose() */, (OperatorObserverBase_2_t1187768149 *)__this);
		IL2CPP_END_FINALLY(191)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(191)
	{
		IL2CPP_JUMP_TBL(0xC6, IL_00c6)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_00c6:
	{
		return;
	}
}
// System.Void UniRx.Operators.LastObservable`1<System.Object>::.ctor(UniRx.IObservable`1<T>,System.Boolean)
extern "C"  void LastObservable_1__ctor_m3352767010_gshared (LastObservable_1_t3225858136 * __this, Il2CppObject* ___source0, bool ___useDefault1, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = ___source0;
		bool L_1 = ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)(NULL /*static, unused*/, (Il2CppObject*)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		NullCheck((OperatorObservableBase_1_t4196218687 *)__this);
		((  void (*) (OperatorObservableBase_1_t4196218687 *, bool, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)((OperatorObservableBase_1_t4196218687 *)__this, (bool)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		Il2CppObject* L_2 = ___source0;
		__this->set_source_1(L_2);
		bool L_3 = ___useDefault1;
		__this->set_useDefault_2(L_3);
		return;
	}
}
// System.Void UniRx.Operators.LastObservable`1<System.Object>::.ctor(UniRx.IObservable`1<T>,System.Func`2<T,System.Boolean>,System.Boolean)
extern "C"  void LastObservable_1__ctor_m1846387532_gshared (LastObservable_1_t3225858136 * __this, Il2CppObject* ___source0, Func_2_t1509682273 * ___predicate1, bool ___useDefault2, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = ___source0;
		bool L_1 = ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)(NULL /*static, unused*/, (Il2CppObject*)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		NullCheck((OperatorObservableBase_1_t4196218687 *)__this);
		((  void (*) (OperatorObservableBase_1_t4196218687 *, bool, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)((OperatorObservableBase_1_t4196218687 *)__this, (bool)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		Il2CppObject* L_2 = ___source0;
		__this->set_source_1(L_2);
		Func_2_t1509682273 * L_3 = ___predicate1;
		__this->set_predicate_3(L_3);
		bool L_4 = ___useDefault2;
		__this->set_useDefault_2(L_4);
		return;
	}
}
// System.IDisposable UniRx.Operators.LastObservable`1<System.Object>::SubscribeCore(UniRx.IObserver`1<T>,System.IDisposable)
extern "C"  Il2CppObject * LastObservable_1_SubscribeCore_m3673071788_gshared (LastObservable_1_t3225858136 * __this, Il2CppObject* ___observer0, Il2CppObject * ___cancel1, const MethodInfo* method)
{
	{
		Func_2_t1509682273 * L_0 = (Func_2_t1509682273 *)__this->get_predicate_3();
		if (L_0)
		{
			goto IL_001f;
		}
	}
	{
		Il2CppObject* L_1 = (Il2CppObject*)__this->get_source_1();
		Il2CppObject* L_2 = ___observer0;
		Il2CppObject * L_3 = ___cancel1;
		Last_t3745612479 * L_4 = (Last_t3745612479 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		((  void (*) (Last_t3745612479 *, LastObservable_1_t3225858136 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)(L_4, (LastObservable_1_t3225858136 *)__this, (Il2CppObject*)L_2, (Il2CppObject *)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		NullCheck((Il2CppObject*)L_1);
		Il2CppObject * L_5 = InterfaceFuncInvoker1< Il2CppObject *, Il2CppObject* >::Invoke(0 /* System.IDisposable UniRx.IObservable`1<System.Object>::Subscribe(UniRx.IObserver`1<T>) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5), (Il2CppObject*)L_1, (Il2CppObject*)L_4);
		return L_5;
	}

IL_001f:
	{
		Il2CppObject* L_6 = (Il2CppObject*)__this->get_source_1();
		Il2CppObject* L_7 = ___observer0;
		Il2CppObject * L_8 = ___cancel1;
		Last__t3329173242 * L_9 = (Last__t3329173242 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		((  void (*) (Last__t3329173242 *, LastObservable_1_t3225858136 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7)->method)(L_9, (LastObservable_1_t3225858136 *)__this, (Il2CppObject*)L_7, (Il2CppObject *)L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7));
		NullCheck((Il2CppObject*)L_6);
		Il2CppObject * L_10 = InterfaceFuncInvoker1< Il2CppObject *, Il2CppObject* >::Invoke(0 /* System.IDisposable UniRx.IObservable`1<System.Object>::Subscribe(UniRx.IObserver`1<T>) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5), (Il2CppObject*)L_6, (Il2CppObject*)L_9);
		return L_10;
	}
}
// System.Void UniRx.Operators.MaterializeObservable`1/Materialize<System.Object>::.ctor(UniRx.Operators.MaterializeObservable`1<T>,UniRx.IObserver`1<UniRx.Notification`1<T>>,System.IDisposable)
extern "C"  void Materialize__ctor_m4173294629_gshared (Materialize_t1797935262 * __this, MaterializeObservable_1_t1619437495 * ___parent0, Il2CppObject* ___observer1, Il2CppObject * ___cancel2, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = ___observer1;
		Il2CppObject * L_1 = ___cancel2;
		NullCheck((OperatorObserverBase_2_t389018104 *)__this);
		((  void (*) (OperatorObserverBase_2_t389018104 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((OperatorObserverBase_2_t389018104 *)__this, (Il2CppObject*)L_0, (Il2CppObject *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		MaterializeObservable_1_t1619437495 * L_2 = ___parent0;
		__this->set_parent_2(L_2);
		return;
	}
}
// System.IDisposable UniRx.Operators.MaterializeObservable`1/Materialize<System.Object>::Run()
extern "C"  Il2CppObject * Materialize_Run_m2526528201_gshared (Materialize_t1797935262 * __this, const MethodInfo* method)
{
	{
		MaterializeObservable_1_t1619437495 * L_0 = (MaterializeObservable_1_t1619437495 *)__this->get_parent_2();
		NullCheck(L_0);
		Il2CppObject* L_1 = (Il2CppObject*)L_0->get_source_1();
		NullCheck((Il2CppObject*)L_1);
		Il2CppObject * L_2 = InterfaceFuncInvoker1< Il2CppObject *, Il2CppObject* >::Invoke(0 /* System.IDisposable UniRx.IObservable`1<System.Object>::Subscribe(UniRx.IObserver`1<T>) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), (Il2CppObject*)L_1, (Il2CppObject*)__this);
		return L_2;
	}
}
// System.Void UniRx.Operators.MaterializeObservable`1/Materialize<System.Object>::OnNext(T)
extern "C"  void Materialize_OnNext_m333042851_gshared (Materialize_t1797935262 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = (Il2CppObject*)((OperatorObserverBase_2_t389018104 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		Il2CppObject * L_1 = ___value0;
		Notification_1_t38356375 * L_2 = ((  Notification_1_t38356375 * (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)(NULL /*static, unused*/, (Il2CppObject *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		NullCheck((Il2CppObject*)L_0);
		InterfaceActionInvoker1< Notification_1_t38356375 * >::Invoke(2 /* System.Void UniRx.IObserver`1<UniRx.Notification`1<System.Object>>::OnNext(T) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4), (Il2CppObject*)L_0, (Notification_1_t38356375 *)L_2);
		return;
	}
}
// System.Void UniRx.Operators.MaterializeObservable`1/Materialize<System.Object>::OnError(System.Exception)
extern "C"  void Materialize_OnError_m891177394_gshared (Materialize_t1797935262 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method)
{
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Il2CppObject* L_0 = (Il2CppObject*)((OperatorObserverBase_2_t389018104 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		Exception_t1967233988 * L_1 = ___error0;
		Notification_1_t38356375 * L_2 = ((  Notification_1_t38356375 * (*) (Il2CppObject * /* static, unused */, Exception_t1967233988 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->method)(NULL /*static, unused*/, (Exception_t1967233988 *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		NullCheck((Il2CppObject*)L_0);
		InterfaceActionInvoker1< Notification_1_t38356375 * >::Invoke(2 /* System.Void UniRx.IObserver`1<UniRx.Notification`1<System.Object>>::OnNext(T) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4), (Il2CppObject*)L_0, (Notification_1_t38356375 *)L_2);
	}

IL_0013:
	try
	{ // begin try (depth: 1)
		Il2CppObject* L_3 = (Il2CppObject*)((OperatorObserverBase_2_t389018104 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		NullCheck((Il2CppObject*)L_3);
		InterfaceActionInvoker0::Invoke(0 /* System.Void UniRx.IObserver`1<UniRx.Notification`1<System.Object>>::OnCompleted() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4), (Il2CppObject*)L_3);
		IL2CPP_LEAVE(0x2C, FINALLY_0025);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0025;
	}

FINALLY_0025:
	{ // begin finally (depth: 1)
		NullCheck((OperatorObserverBase_2_t389018104 *)__this);
		VirtActionInvoker0::Invoke(4 /* System.Void UniRx.Operators.OperatorObserverBase`2<System.Object,UniRx.Notification`1<System.Object>>::Dispose() */, (OperatorObserverBase_2_t389018104 *)__this);
		IL2CPP_END_FINALLY(37)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(37)
	{
		IL2CPP_JUMP_TBL(0x2C, IL_002c)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_002c:
	{
		return;
	}
}
// System.Void UniRx.Operators.MaterializeObservable`1/Materialize<System.Object>::OnCompleted()
extern "C"  void Materialize_OnCompleted_m2295887877_gshared (Materialize_t1797935262 * __this, const MethodInfo* method)
{
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Il2CppObject* L_0 = (Il2CppObject*)((OperatorObserverBase_2_t389018104 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		Notification_1_t38356375 * L_1 = ((  Notification_1_t38356375 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7)->method)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7));
		NullCheck((Il2CppObject*)L_0);
		InterfaceActionInvoker1< Notification_1_t38356375 * >::Invoke(2 /* System.Void UniRx.IObserver`1<UniRx.Notification`1<System.Object>>::OnNext(T) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4), (Il2CppObject*)L_0, (Notification_1_t38356375 *)L_1);
	}

IL_0012:
	try
	{ // begin try (depth: 1)
		Il2CppObject* L_2 = (Il2CppObject*)((OperatorObserverBase_2_t389018104 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		NullCheck((Il2CppObject*)L_2);
		InterfaceActionInvoker0::Invoke(0 /* System.Void UniRx.IObserver`1<UniRx.Notification`1<System.Object>>::OnCompleted() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4), (Il2CppObject*)L_2);
		IL2CPP_LEAVE(0x2B, FINALLY_0024);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0024;
	}

FINALLY_0024:
	{ // begin finally (depth: 1)
		NullCheck((OperatorObserverBase_2_t389018104 *)__this);
		VirtActionInvoker0::Invoke(4 /* System.Void UniRx.Operators.OperatorObserverBase`2<System.Object,UniRx.Notification`1<System.Object>>::Dispose() */, (OperatorObserverBase_2_t389018104 *)__this);
		IL2CPP_END_FINALLY(36)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(36)
	{
		IL2CPP_JUMP_TBL(0x2B, IL_002b)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_002b:
	{
		return;
	}
}
// System.Void UniRx.Operators.MaterializeObservable`1<System.Object>::.ctor(UniRx.IObservable`1<T>)
extern "C"  void MaterializeObservable_1__ctor_m2251900604_gshared (MaterializeObservable_1_t1619437495 * __this, Il2CppObject* ___source0, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = ___source0;
		bool L_1 = ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)(NULL /*static, unused*/, (Il2CppObject*)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		NullCheck((OperatorObservableBase_1_t3397468642 *)__this);
		((  void (*) (OperatorObservableBase_1_t3397468642 *, bool, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)((OperatorObservableBase_1_t3397468642 *)__this, (bool)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		Il2CppObject* L_2 = ___source0;
		__this->set_source_1(L_2);
		return;
	}
}
// System.IDisposable UniRx.Operators.MaterializeObservable`1<System.Object>::SubscribeCore(UniRx.IObserver`1<UniRx.Notification`1<T>>,System.IDisposable)
extern "C"  Il2CppObject * MaterializeObservable_1_SubscribeCore_m3597847907_gshared (MaterializeObservable_1_t1619437495 * __this, Il2CppObject* ___observer0, Il2CppObject * ___cancel1, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = ___observer0;
		Il2CppObject * L_1 = ___cancel1;
		Materialize_t1797935262 * L_2 = (Materialize_t1797935262 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		((  void (*) (Materialize_t1797935262 *, MaterializeObservable_1_t1619437495 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)(L_2, (MaterializeObservable_1_t1619437495 *)__this, (Il2CppObject*)L_0, (Il2CppObject *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		NullCheck((Materialize_t1797935262 *)L_2);
		Il2CppObject * L_3 = ((  Il2CppObject * (*) (Materialize_t1797935262 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->method)((Materialize_t1797935262 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		return L_3;
	}
}
// System.Void UniRx.Operators.MergeObservable`1/MergeConcurrentObserver/Merge<System.Object>::.ctor(UniRx.Operators.MergeObservable`1/MergeConcurrentObserver<T>,System.IDisposable)
extern "C"  void Merge__ctor_m508001328_gshared (Merge_t1753429969 * __this, MergeConcurrentObserver_t3534593302 * ___parent0, Il2CppObject * ___cancel1, const MethodInfo* method)
{
	{
		MergeConcurrentObserver_t3534593302 * L_0 = ___parent0;
		NullCheck(L_0);
		Il2CppObject* L_1 = (Il2CppObject*)((OperatorObserverBase_2_t3381670217 *)L_0)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		Il2CppObject * L_2 = ___cancel1;
		NullCheck((OperatorObserverBase_2_t1187768149 *)__this);
		((  void (*) (OperatorObserverBase_2_t1187768149 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((OperatorObserverBase_2_t1187768149 *)__this, (Il2CppObject*)L_1, (Il2CppObject *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		MergeConcurrentObserver_t3534593302 * L_3 = ___parent0;
		__this->set_parent_2(L_3);
		Il2CppObject * L_4 = ___cancel1;
		__this->set_cancel_3(L_4);
		return;
	}
}
// System.Void UniRx.Operators.MergeObservable`1/MergeConcurrentObserver/Merge<System.Object>::OnNext(T)
extern "C"  void Merge_OnNext_m2854594575_gshared (Merge_t1753429969 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	Il2CppObject * V_0 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		MergeConcurrentObserver_t3534593302 * L_0 = (MergeConcurrentObserver_t3534593302 *)__this->get_parent_2();
		NullCheck(L_0);
		Il2CppObject * L_1 = (Il2CppObject *)L_0->get_gate_5();
		V_0 = (Il2CppObject *)L_1;
		Il2CppObject * L_2 = V_0;
		Monitor_Enter_m476686225(NULL /*static, unused*/, (Il2CppObject *)L_2, /*hidden argument*/NULL);
	}

IL_0012:
	try
	{ // begin try (depth: 1)
		Il2CppObject* L_3 = (Il2CppObject*)((OperatorObserverBase_2_t1187768149 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		Il2CppObject * L_4 = ___value0;
		NullCheck((Il2CppObject*)L_3);
		InterfaceActionInvoker1< Il2CppObject * >::Invoke(2 /* System.Void UniRx.IObserver`1<System.Object>::OnNext(T) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), (Il2CppObject*)L_3, (Il2CppObject *)L_4);
		IL2CPP_LEAVE(0x2C, FINALLY_0025);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0025;
	}

FINALLY_0025:
	{ // begin finally (depth: 1)
		Il2CppObject * L_5 = V_0;
		Monitor_Exit_m2088237919(NULL /*static, unused*/, (Il2CppObject *)L_5, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(37)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(37)
	{
		IL2CPP_JUMP_TBL(0x2C, IL_002c)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_002c:
	{
		return;
	}
}
// System.Void UniRx.Operators.MergeObservable`1/MergeConcurrentObserver/Merge<System.Object>::OnError(System.Exception)
extern "C"  void Merge_OnError_m3742221598_gshared (Merge_t1753429969 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method)
{
	Il2CppObject * V_0 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		MergeConcurrentObserver_t3534593302 * L_0 = (MergeConcurrentObserver_t3534593302 *)__this->get_parent_2();
		NullCheck(L_0);
		Il2CppObject * L_1 = (Il2CppObject *)L_0->get_gate_5();
		V_0 = (Il2CppObject *)L_1;
		Il2CppObject * L_2 = V_0;
		Monitor_Enter_m476686225(NULL /*static, unused*/, (Il2CppObject *)L_2, /*hidden argument*/NULL);
	}

IL_0012:
	try
	{ // begin try (depth: 1)
		try
		{ // begin try (depth: 2)
			Il2CppObject* L_3 = (Il2CppObject*)((OperatorObserverBase_2_t1187768149 *)__this)->get_observer_0();
			il2cpp_codegen_memory_barrier();
			Exception_t1967233988 * L_4 = ___error0;
			NullCheck((Il2CppObject*)L_3);
			InterfaceActionInvoker1< Exception_t1967233988 * >::Invoke(1 /* System.Void UniRx.IObserver`1<System.Object>::OnError(System.Exception) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), (Il2CppObject*)L_3, (Exception_t1967233988 *)L_4);
			IL2CPP_LEAVE(0x2C, FINALLY_0025);
		} // end try (depth: 2)
		catch(Il2CppExceptionWrapper& e)
		{
			__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
			goto FINALLY_0025;
		}

FINALLY_0025:
		{ // begin finally (depth: 2)
			NullCheck((OperatorObserverBase_2_t1187768149 *)__this);
			VirtActionInvoker0::Invoke(4 /* System.Void UniRx.Operators.OperatorObserverBase`2<System.Object,System.Object>::Dispose() */, (OperatorObserverBase_2_t1187768149 *)__this);
			IL2CPP_END_FINALLY(37)
		} // end finally (depth: 2)
		IL2CPP_CLEANUP(37)
		{
			IL2CPP_JUMP_TBL(0x2C, IL_002c)
			IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
		}

IL_002c:
		{
			IL2CPP_LEAVE(0x38, FINALLY_0031);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0031;
	}

FINALLY_0031:
	{ // begin finally (depth: 1)
		Il2CppObject * L_5 = V_0;
		Monitor_Exit_m2088237919(NULL /*static, unused*/, (Il2CppObject *)L_5, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(49)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(49)
	{
		IL2CPP_JUMP_TBL(0x38, IL_0038)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0038:
	{
		return;
	}
}
// System.Void UniRx.Operators.MergeObservable`1/MergeConcurrentObserver/Merge<System.Object>::OnCompleted()
extern "C"  void Merge_OnCompleted_m2767500657_gshared (Merge_t1753429969 * __this, const MethodInfo* method)
{
	Il2CppObject * V_0 = NULL;
	Il2CppObject* V_1 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		MergeConcurrentObserver_t3534593302 * L_0 = (MergeConcurrentObserver_t3534593302 *)__this->get_parent_2();
		NullCheck(L_0);
		CompositeDisposable_t1894629977 * L_1 = (CompositeDisposable_t1894629977 *)L_0->get_collectionDisposable_3();
		Il2CppObject * L_2 = (Il2CppObject *)__this->get_cancel_3();
		NullCheck((CompositeDisposable_t1894629977 *)L_1);
		CompositeDisposable_Remove_m495331162((CompositeDisposable_t1894629977 *)L_1, (Il2CppObject *)L_2, /*hidden argument*/NULL);
		MergeConcurrentObserver_t3534593302 * L_3 = (MergeConcurrentObserver_t3534593302 *)__this->get_parent_2();
		NullCheck(L_3);
		Il2CppObject * L_4 = (Il2CppObject *)L_3->get_gate_5();
		V_0 = (Il2CppObject *)L_4;
		Il2CppObject * L_5 = V_0;
		Monitor_Enter_m476686225(NULL /*static, unused*/, (Il2CppObject *)L_5, /*hidden argument*/NULL);
	}

IL_0029:
	try
	{ // begin try (depth: 1)
		{
			MergeConcurrentObserver_t3534593302 * L_6 = (MergeConcurrentObserver_t3534593302 *)__this->get_parent_2();
			NullCheck(L_6);
			Queue_1_t2303992324 * L_7 = (Queue_1_t2303992324 *)L_6->get_q_7();
			NullCheck((Queue_1_t2303992324 *)L_7);
			int32_t L_8 = VirtFuncInvoker0< int32_t >::Invoke(6 /* System.Int32 System.Collections.Generic.Queue`1<UniRx.IObservable`1<System.Object>>::get_Count() */, (Queue_1_t2303992324 *)L_7);
			if ((((int32_t)L_8) <= ((int32_t)0)))
			{
				goto IL_0061;
			}
		}

IL_003f:
		{
			MergeConcurrentObserver_t3534593302 * L_9 = (MergeConcurrentObserver_t3534593302 *)__this->get_parent_2();
			NullCheck(L_9);
			Queue_1_t2303992324 * L_10 = (Queue_1_t2303992324 *)L_9->get_q_7();
			NullCheck((Queue_1_t2303992324 *)L_10);
			Il2CppObject* L_11 = ((  Il2CppObject* (*) (Queue_1_t2303992324 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->method)((Queue_1_t2303992324 *)L_10, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
			V_1 = (Il2CppObject*)L_11;
			MergeConcurrentObserver_t3534593302 * L_12 = (MergeConcurrentObserver_t3534593302 *)__this->get_parent_2();
			Il2CppObject* L_13 = V_1;
			NullCheck((MergeConcurrentObserver_t3534593302 *)L_12);
			((  void (*) (MergeConcurrentObserver_t3534593302 *, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6)->method)((MergeConcurrentObserver_t3534593302 *)L_12, (Il2CppObject*)L_13, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
			goto IL_00ad;
		}

IL_0061:
		{
			MergeConcurrentObserver_t3534593302 * L_14 = (MergeConcurrentObserver_t3534593302 *)__this->get_parent_2();
			MergeConcurrentObserver_t3534593302 * L_15 = (MergeConcurrentObserver_t3534593302 *)L_14;
			NullCheck(L_15);
			int32_t L_16 = (int32_t)L_15->get_activeCount_8();
			NullCheck(L_15);
			L_15->set_activeCount_8(((int32_t)((int32_t)L_16-(int32_t)1)));
			MergeConcurrentObserver_t3534593302 * L_17 = (MergeConcurrentObserver_t3534593302 *)__this->get_parent_2();
			NullCheck(L_17);
			bool L_18 = (bool)L_17->get_isStopped_6();
			if (!L_18)
			{
				goto IL_00ad;
			}
		}

IL_0084:
		{
			MergeConcurrentObserver_t3534593302 * L_19 = (MergeConcurrentObserver_t3534593302 *)__this->get_parent_2();
			NullCheck(L_19);
			int32_t L_20 = (int32_t)L_19->get_activeCount_8();
			if (L_20)
			{
				goto IL_00ad;
			}
		}

IL_0094:
		try
		{ // begin try (depth: 2)
			Il2CppObject* L_21 = (Il2CppObject*)((OperatorObserverBase_2_t1187768149 *)__this)->get_observer_0();
			il2cpp_codegen_memory_barrier();
			NullCheck((Il2CppObject*)L_21);
			InterfaceActionInvoker0::Invoke(0 /* System.Void UniRx.IObserver`1<System.Object>::OnCompleted() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), (Il2CppObject*)L_21);
			IL2CPP_LEAVE(0xAD, FINALLY_00a6);
		} // end try (depth: 2)
		catch(Il2CppExceptionWrapper& e)
		{
			__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
			goto FINALLY_00a6;
		}

FINALLY_00a6:
		{ // begin finally (depth: 2)
			NullCheck((OperatorObserverBase_2_t1187768149 *)__this);
			VirtActionInvoker0::Invoke(4 /* System.Void UniRx.Operators.OperatorObserverBase`2<System.Object,System.Object>::Dispose() */, (OperatorObserverBase_2_t1187768149 *)__this);
			IL2CPP_END_FINALLY(166)
		} // end finally (depth: 2)
		IL2CPP_CLEANUP(166)
		{
			IL2CPP_JUMP_TBL(0xAD, IL_00ad)
			IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
		}

IL_00ad:
		{
			IL2CPP_LEAVE(0xB9, FINALLY_00b2);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_00b2;
	}

FINALLY_00b2:
	{ // begin finally (depth: 1)
		Il2CppObject * L_22 = V_0;
		Monitor_Exit_m2088237919(NULL /*static, unused*/, (Il2CppObject *)L_22, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(178)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(178)
	{
		IL2CPP_JUMP_TBL(0xB9, IL_00b9)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_00b9:
	{
		return;
	}
}
// System.Void UniRx.Operators.MergeObservable`1/MergeConcurrentObserver/Merge<UniRx.Unit>::.ctor(UniRx.Operators.MergeObservable`1/MergeConcurrentObserver<T>,System.IDisposable)
extern "C"  void Merge__ctor_m4162486394_gshared (Merge_t3474609587 * __this, MergeConcurrentObserver_t960805624 * ___parent0, Il2CppObject * ___cancel1, const MethodInfo* method)
{
	{
		MergeConcurrentObserver_t960805624 * L_0 = ___parent0;
		NullCheck(L_0);
		Il2CppObject* L_1 = (Il2CppObject*)((OperatorObserverBase_2_t2064311169 *)L_0)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		Il2CppObject * L_2 = ___cancel1;
		NullCheck((OperatorObserverBase_2_t4165376397 *)__this);
		((  void (*) (OperatorObserverBase_2_t4165376397 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((OperatorObserverBase_2_t4165376397 *)__this, (Il2CppObject*)L_1, (Il2CppObject *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		MergeConcurrentObserver_t960805624 * L_3 = ___parent0;
		__this->set_parent_2(L_3);
		Il2CppObject * L_4 = ___cancel1;
		__this->set_cancel_3(L_4);
		return;
	}
}
// System.Void UniRx.Operators.MergeObservable`1/MergeConcurrentObserver/Merge<UniRx.Unit>::OnNext(T)
extern "C"  void Merge_OnNext_m1495671685_gshared (Merge_t3474609587 * __this, Unit_t2558286038  ___value0, const MethodInfo* method)
{
	Il2CppObject * V_0 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		MergeConcurrentObserver_t960805624 * L_0 = (MergeConcurrentObserver_t960805624 *)__this->get_parent_2();
		NullCheck(L_0);
		Il2CppObject * L_1 = (Il2CppObject *)L_0->get_gate_5();
		V_0 = (Il2CppObject *)L_1;
		Il2CppObject * L_2 = V_0;
		Monitor_Enter_m476686225(NULL /*static, unused*/, (Il2CppObject *)L_2, /*hidden argument*/NULL);
	}

IL_0012:
	try
	{ // begin try (depth: 1)
		Il2CppObject* L_3 = (Il2CppObject*)((OperatorObserverBase_2_t4165376397 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		Unit_t2558286038  L_4 = ___value0;
		NullCheck((Il2CppObject*)L_3);
		InterfaceActionInvoker1< Unit_t2558286038  >::Invoke(2 /* System.Void UniRx.IObserver`1<UniRx.Unit>::OnNext(T) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), (Il2CppObject*)L_3, (Unit_t2558286038 )L_4);
		IL2CPP_LEAVE(0x2C, FINALLY_0025);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0025;
	}

FINALLY_0025:
	{ // begin finally (depth: 1)
		Il2CppObject * L_5 = V_0;
		Monitor_Exit_m2088237919(NULL /*static, unused*/, (Il2CppObject *)L_5, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(37)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(37)
	{
		IL2CPP_JUMP_TBL(0x2C, IL_002c)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_002c:
	{
		return;
	}
}
// System.Void UniRx.Operators.MergeObservable`1/MergeConcurrentObserver/Merge<UniRx.Unit>::OnError(System.Exception)
extern "C"  void Merge_OnError_m1510249620_gshared (Merge_t3474609587 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method)
{
	Il2CppObject * V_0 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		MergeConcurrentObserver_t960805624 * L_0 = (MergeConcurrentObserver_t960805624 *)__this->get_parent_2();
		NullCheck(L_0);
		Il2CppObject * L_1 = (Il2CppObject *)L_0->get_gate_5();
		V_0 = (Il2CppObject *)L_1;
		Il2CppObject * L_2 = V_0;
		Monitor_Enter_m476686225(NULL /*static, unused*/, (Il2CppObject *)L_2, /*hidden argument*/NULL);
	}

IL_0012:
	try
	{ // begin try (depth: 1)
		try
		{ // begin try (depth: 2)
			Il2CppObject* L_3 = (Il2CppObject*)((OperatorObserverBase_2_t4165376397 *)__this)->get_observer_0();
			il2cpp_codegen_memory_barrier();
			Exception_t1967233988 * L_4 = ___error0;
			NullCheck((Il2CppObject*)L_3);
			InterfaceActionInvoker1< Exception_t1967233988 * >::Invoke(1 /* System.Void UniRx.IObserver`1<UniRx.Unit>::OnError(System.Exception) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), (Il2CppObject*)L_3, (Exception_t1967233988 *)L_4);
			IL2CPP_LEAVE(0x2C, FINALLY_0025);
		} // end try (depth: 2)
		catch(Il2CppExceptionWrapper& e)
		{
			__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
			goto FINALLY_0025;
		}

FINALLY_0025:
		{ // begin finally (depth: 2)
			NullCheck((OperatorObserverBase_2_t4165376397 *)__this);
			VirtActionInvoker0::Invoke(4 /* System.Void UniRx.Operators.OperatorObserverBase`2<UniRx.Unit,UniRx.Unit>::Dispose() */, (OperatorObserverBase_2_t4165376397 *)__this);
			IL2CPP_END_FINALLY(37)
		} // end finally (depth: 2)
		IL2CPP_CLEANUP(37)
		{
			IL2CPP_JUMP_TBL(0x2C, IL_002c)
			IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
		}

IL_002c:
		{
			IL2CPP_LEAVE(0x38, FINALLY_0031);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0031;
	}

FINALLY_0031:
	{ // begin finally (depth: 1)
		Il2CppObject * L_5 = V_0;
		Monitor_Exit_m2088237919(NULL /*static, unused*/, (Il2CppObject *)L_5, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(49)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(49)
	{
		IL2CPP_JUMP_TBL(0x38, IL_0038)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0038:
	{
		return;
	}
}
// System.Void UniRx.Operators.MergeObservable`1/MergeConcurrentObserver/Merge<UniRx.Unit>::OnCompleted()
extern "C"  void Merge_OnCompleted_m2680063463_gshared (Merge_t3474609587 * __this, const MethodInfo* method)
{
	Il2CppObject * V_0 = NULL;
	Il2CppObject* V_1 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		MergeConcurrentObserver_t960805624 * L_0 = (MergeConcurrentObserver_t960805624 *)__this->get_parent_2();
		NullCheck(L_0);
		CompositeDisposable_t1894629977 * L_1 = (CompositeDisposable_t1894629977 *)L_0->get_collectionDisposable_3();
		Il2CppObject * L_2 = (Il2CppObject *)__this->get_cancel_3();
		NullCheck((CompositeDisposable_t1894629977 *)L_1);
		CompositeDisposable_Remove_m495331162((CompositeDisposable_t1894629977 *)L_1, (Il2CppObject *)L_2, /*hidden argument*/NULL);
		MergeConcurrentObserver_t960805624 * L_3 = (MergeConcurrentObserver_t960805624 *)__this->get_parent_2();
		NullCheck(L_3);
		Il2CppObject * L_4 = (Il2CppObject *)L_3->get_gate_5();
		V_0 = (Il2CppObject *)L_4;
		Il2CppObject * L_5 = V_0;
		Monitor_Enter_m476686225(NULL /*static, unused*/, (Il2CppObject *)L_5, /*hidden argument*/NULL);
	}

IL_0029:
	try
	{ // begin try (depth: 1)
		{
			MergeConcurrentObserver_t960805624 * L_6 = (MergeConcurrentObserver_t960805624 *)__this->get_parent_2();
			NullCheck(L_6);
			Queue_1_t4025171942 * L_7 = (Queue_1_t4025171942 *)L_6->get_q_7();
			NullCheck((Queue_1_t4025171942 *)L_7);
			int32_t L_8 = VirtFuncInvoker0< int32_t >::Invoke(6 /* System.Int32 System.Collections.Generic.Queue`1<UniRx.IObservable`1<UniRx.Unit>>::get_Count() */, (Queue_1_t4025171942 *)L_7);
			if ((((int32_t)L_8) <= ((int32_t)0)))
			{
				goto IL_0061;
			}
		}

IL_003f:
		{
			MergeConcurrentObserver_t960805624 * L_9 = (MergeConcurrentObserver_t960805624 *)__this->get_parent_2();
			NullCheck(L_9);
			Queue_1_t4025171942 * L_10 = (Queue_1_t4025171942 *)L_9->get_q_7();
			NullCheck((Queue_1_t4025171942 *)L_10);
			Il2CppObject* L_11 = ((  Il2CppObject* (*) (Queue_1_t4025171942 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->method)((Queue_1_t4025171942 *)L_10, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
			V_1 = (Il2CppObject*)L_11;
			MergeConcurrentObserver_t960805624 * L_12 = (MergeConcurrentObserver_t960805624 *)__this->get_parent_2();
			Il2CppObject* L_13 = V_1;
			NullCheck((MergeConcurrentObserver_t960805624 *)L_12);
			((  void (*) (MergeConcurrentObserver_t960805624 *, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6)->method)((MergeConcurrentObserver_t960805624 *)L_12, (Il2CppObject*)L_13, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
			goto IL_00ad;
		}

IL_0061:
		{
			MergeConcurrentObserver_t960805624 * L_14 = (MergeConcurrentObserver_t960805624 *)__this->get_parent_2();
			MergeConcurrentObserver_t960805624 * L_15 = (MergeConcurrentObserver_t960805624 *)L_14;
			NullCheck(L_15);
			int32_t L_16 = (int32_t)L_15->get_activeCount_8();
			NullCheck(L_15);
			L_15->set_activeCount_8(((int32_t)((int32_t)L_16-(int32_t)1)));
			MergeConcurrentObserver_t960805624 * L_17 = (MergeConcurrentObserver_t960805624 *)__this->get_parent_2();
			NullCheck(L_17);
			bool L_18 = (bool)L_17->get_isStopped_6();
			if (!L_18)
			{
				goto IL_00ad;
			}
		}

IL_0084:
		{
			MergeConcurrentObserver_t960805624 * L_19 = (MergeConcurrentObserver_t960805624 *)__this->get_parent_2();
			NullCheck(L_19);
			int32_t L_20 = (int32_t)L_19->get_activeCount_8();
			if (L_20)
			{
				goto IL_00ad;
			}
		}

IL_0094:
		try
		{ // begin try (depth: 2)
			Il2CppObject* L_21 = (Il2CppObject*)((OperatorObserverBase_2_t4165376397 *)__this)->get_observer_0();
			il2cpp_codegen_memory_barrier();
			NullCheck((Il2CppObject*)L_21);
			InterfaceActionInvoker0::Invoke(0 /* System.Void UniRx.IObserver`1<UniRx.Unit>::OnCompleted() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), (Il2CppObject*)L_21);
			IL2CPP_LEAVE(0xAD, FINALLY_00a6);
		} // end try (depth: 2)
		catch(Il2CppExceptionWrapper& e)
		{
			__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
			goto FINALLY_00a6;
		}

FINALLY_00a6:
		{ // begin finally (depth: 2)
			NullCheck((OperatorObserverBase_2_t4165376397 *)__this);
			VirtActionInvoker0::Invoke(4 /* System.Void UniRx.Operators.OperatorObserverBase`2<UniRx.Unit,UniRx.Unit>::Dispose() */, (OperatorObserverBase_2_t4165376397 *)__this);
			IL2CPP_END_FINALLY(166)
		} // end finally (depth: 2)
		IL2CPP_CLEANUP(166)
		{
			IL2CPP_JUMP_TBL(0xAD, IL_00ad)
			IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
		}

IL_00ad:
		{
			IL2CPP_LEAVE(0xB9, FINALLY_00b2);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_00b2;
	}

FINALLY_00b2:
	{ // begin finally (depth: 1)
		Il2CppObject * L_22 = V_0;
		Monitor_Exit_m2088237919(NULL /*static, unused*/, (Il2CppObject *)L_22, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(178)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(178)
	{
		IL2CPP_JUMP_TBL(0xB9, IL_00b9)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_00b9:
	{
		return;
	}
}
// System.Void UniRx.Operators.MergeObservable`1/MergeConcurrentObserver<System.Object>::.ctor(UniRx.Operators.MergeObservable`1<T>,UniRx.IObserver`1<T>,System.IDisposable)
extern Il2CppClass* Il2CppObject_il2cpp_TypeInfo_var;
extern const uint32_t MergeConcurrentObserver__ctor_m1744247409_MetadataUsageId;
extern "C"  void MergeConcurrentObserver__ctor_m1744247409_gshared (MergeConcurrentObserver_t3534593302 * __this, MergeObservable_1_t3637992042 * ___parent0, Il2CppObject* ___observer1, Il2CppObject * ___cancel2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (MergeConcurrentObserver__ctor_m1744247409_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject * L_0 = (Il2CppObject *)il2cpp_codegen_object_new(Il2CppObject_il2cpp_TypeInfo_var);
		Object__ctor_m1772956182(L_0, /*hidden argument*/NULL);
		__this->set_gate_5(L_0);
		Il2CppObject* L_1 = ___observer1;
		Il2CppObject * L_2 = ___cancel2;
		NullCheck((OperatorObserverBase_2_t3381670217 *)__this);
		((  void (*) (OperatorObserverBase_2_t3381670217 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((OperatorObserverBase_2_t3381670217 *)__this, (Il2CppObject*)L_1, (Il2CppObject *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		MergeObservable_1_t3637992042 * L_3 = ___parent0;
		__this->set_parent_2(L_3);
		return;
	}
}
// System.IDisposable UniRx.Operators.MergeObservable`1/MergeConcurrentObserver<System.Object>::Run()
extern Il2CppClass* CompositeDisposable_t1894629977_il2cpp_TypeInfo_var;
extern Il2CppClass* SingleAssignmentDisposable_t2336378823_il2cpp_TypeInfo_var;
extern const uint32_t MergeConcurrentObserver_Run_m2796918060_MetadataUsageId;
extern "C"  Il2CppObject * MergeConcurrentObserver_Run_m2796918060_gshared (MergeConcurrentObserver_t3534593302 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (MergeConcurrentObserver_Run_m2796918060_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Queue_1_t2303992324 * L_0 = (Queue_1_t2303992324 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		((  void (*) (Queue_1_t2303992324 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		__this->set_q_7(L_0);
		__this->set_activeCount_8(0);
		CompositeDisposable_t1894629977 * L_1 = (CompositeDisposable_t1894629977 *)il2cpp_codegen_object_new(CompositeDisposable_t1894629977_il2cpp_TypeInfo_var);
		CompositeDisposable__ctor_m1237994920(L_1, /*hidden argument*/NULL);
		__this->set_collectionDisposable_3(L_1);
		SingleAssignmentDisposable_t2336378823 * L_2 = (SingleAssignmentDisposable_t2336378823 *)il2cpp_codegen_object_new(SingleAssignmentDisposable_t2336378823_il2cpp_TypeInfo_var);
		SingleAssignmentDisposable__ctor_m917449122(L_2, /*hidden argument*/NULL);
		__this->set_sourceDisposable_4(L_2);
		CompositeDisposable_t1894629977 * L_3 = (CompositeDisposable_t1894629977 *)__this->get_collectionDisposable_3();
		SingleAssignmentDisposable_t2336378823 * L_4 = (SingleAssignmentDisposable_t2336378823 *)__this->get_sourceDisposable_4();
		NullCheck((CompositeDisposable_t1894629977 *)L_3);
		CompositeDisposable_Add_m2171552957((CompositeDisposable_t1894629977 *)L_3, (Il2CppObject *)L_4, /*hidden argument*/NULL);
		SingleAssignmentDisposable_t2336378823 * L_5 = (SingleAssignmentDisposable_t2336378823 *)__this->get_sourceDisposable_4();
		MergeObservable_1_t3637992042 * L_6 = (MergeObservable_1_t3637992042 *)__this->get_parent_2();
		NullCheck(L_6);
		Il2CppObject* L_7 = (Il2CppObject*)L_6->get_sources_1();
		NullCheck((Il2CppObject*)L_7);
		Il2CppObject * L_8 = InterfaceFuncInvoker1< Il2CppObject *, Il2CppObject* >::Invoke(0 /* System.IDisposable UniRx.IObservable`1<UniRx.IObservable`1<System.Object>>::Subscribe(UniRx.IObserver`1<T>) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4), (Il2CppObject*)L_7, (Il2CppObject*)__this);
		NullCheck((SingleAssignmentDisposable_t2336378823 *)L_5);
		SingleAssignmentDisposable_set_Disposable_m1599028885((SingleAssignmentDisposable_t2336378823 *)L_5, (Il2CppObject *)L_8, /*hidden argument*/NULL);
		CompositeDisposable_t1894629977 * L_9 = (CompositeDisposable_t1894629977 *)__this->get_collectionDisposable_3();
		return L_9;
	}
}
// System.Void UniRx.Operators.MergeObservable`1/MergeConcurrentObserver<System.Object>::OnNext(UniRx.IObservable`1<T>)
extern "C"  void MergeConcurrentObserver_OnNext_m3276267255_gshared (MergeConcurrentObserver_t3534593302 * __this, Il2CppObject* ___value0, const MethodInfo* method)
{
	Il2CppObject * V_0 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_gate_5();
		V_0 = (Il2CppObject *)L_0;
		Il2CppObject * L_1 = V_0;
		Monitor_Enter_m476686225(NULL /*static, unused*/, (Il2CppObject *)L_1, /*hidden argument*/NULL);
	}

IL_000d:
	try
	{ // begin try (depth: 1)
		{
			int32_t L_2 = (int32_t)__this->get_activeCount_8();
			MergeObservable_1_t3637992042 * L_3 = (MergeObservable_1_t3637992042 *)__this->get_parent_2();
			NullCheck(L_3);
			int32_t L_4 = (int32_t)L_3->get_maxConcurrent_2();
			if ((((int32_t)L_2) >= ((int32_t)L_4)))
			{
				goto IL_003d;
			}
		}

IL_0023:
		{
			int32_t L_5 = (int32_t)__this->get_activeCount_8();
			__this->set_activeCount_8(((int32_t)((int32_t)L_5+(int32_t)1)));
			Il2CppObject* L_6 = ___value0;
			NullCheck((MergeConcurrentObserver_t3534593302 *)__this);
			((  void (*) (MergeConcurrentObserver_t3534593302 *, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->method)((MergeConcurrentObserver_t3534593302 *)__this, (Il2CppObject*)L_6, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
			goto IL_0049;
		}

IL_003d:
		{
			Queue_1_t2303992324 * L_7 = (Queue_1_t2303992324 *)__this->get_q_7();
			Il2CppObject* L_8 = ___value0;
			NullCheck((Queue_1_t2303992324 *)L_7);
			((  void (*) (Queue_1_t2303992324 *, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6)->method)((Queue_1_t2303992324 *)L_7, (Il2CppObject*)L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		}

IL_0049:
		{
			IL2CPP_LEAVE(0x55, FINALLY_004e);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_004e;
	}

FINALLY_004e:
	{ // begin finally (depth: 1)
		Il2CppObject * L_9 = V_0;
		Monitor_Exit_m2088237919(NULL /*static, unused*/, (Il2CppObject *)L_9, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(78)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(78)
	{
		IL2CPP_JUMP_TBL(0x55, IL_0055)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0055:
	{
		return;
	}
}
// System.Void UniRx.Operators.MergeObservable`1/MergeConcurrentObserver<System.Object>::OnError(System.Exception)
extern "C"  void MergeConcurrentObserver_OnError_m58416213_gshared (MergeConcurrentObserver_t3534593302 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method)
{
	Il2CppObject * V_0 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_gate_5();
		V_0 = (Il2CppObject *)L_0;
		Il2CppObject * L_1 = V_0;
		Monitor_Enter_m476686225(NULL /*static, unused*/, (Il2CppObject *)L_1, /*hidden argument*/NULL);
	}

IL_000d:
	try
	{ // begin try (depth: 1)
		try
		{ // begin try (depth: 2)
			Il2CppObject* L_2 = (Il2CppObject*)((OperatorObserverBase_2_t3381670217 *)__this)->get_observer_0();
			il2cpp_codegen_memory_barrier();
			Exception_t1967233988 * L_3 = ___error0;
			NullCheck((Il2CppObject*)L_2);
			InterfaceActionInvoker1< Exception_t1967233988 * >::Invoke(1 /* System.Void UniRx.IObserver`1<System.Object>::OnError(System.Exception) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7), (Il2CppObject*)L_2, (Exception_t1967233988 *)L_3);
			IL2CPP_LEAVE(0x27, FINALLY_0020);
		} // end try (depth: 2)
		catch(Il2CppExceptionWrapper& e)
		{
			__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
			goto FINALLY_0020;
		}

FINALLY_0020:
		{ // begin finally (depth: 2)
			NullCheck((OperatorObserverBase_2_t3381670217 *)__this);
			VirtActionInvoker0::Invoke(4 /* System.Void UniRx.Operators.OperatorObserverBase`2<UniRx.IObservable`1<System.Object>,System.Object>::Dispose() */, (OperatorObserverBase_2_t3381670217 *)__this);
			IL2CPP_END_FINALLY(32)
		} // end finally (depth: 2)
		IL2CPP_CLEANUP(32)
		{
			IL2CPP_JUMP_TBL(0x27, IL_0027)
			IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
		}

IL_0027:
		{
			IL2CPP_LEAVE(0x33, FINALLY_002c);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_002c;
	}

FINALLY_002c:
	{ // begin finally (depth: 1)
		Il2CppObject * L_4 = V_0;
		Monitor_Exit_m2088237919(NULL /*static, unused*/, (Il2CppObject *)L_4, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(44)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(44)
	{
		IL2CPP_JUMP_TBL(0x33, IL_0033)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0033:
	{
		return;
	}
}
// System.Void UniRx.Operators.MergeObservable`1/MergeConcurrentObserver<System.Object>::OnCompleted()
extern "C"  void MergeConcurrentObserver_OnCompleted_m3814336296_gshared (MergeConcurrentObserver_t3534593302 * __this, const MethodInfo* method)
{
	Il2CppObject * V_0 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_gate_5();
		V_0 = (Il2CppObject *)L_0;
		Il2CppObject * L_1 = V_0;
		Monitor_Enter_m476686225(NULL /*static, unused*/, (Il2CppObject *)L_1, /*hidden argument*/NULL);
	}

IL_000d:
	try
	{ // begin try (depth: 1)
		{
			__this->set_isStopped_6((bool)1);
			int32_t L_2 = (int32_t)__this->get_activeCount_8();
			if (L_2)
			{
				goto IL_003d;
			}
		}

IL_001f:
		try
		{ // begin try (depth: 2)
			Il2CppObject* L_3 = (Il2CppObject*)((OperatorObserverBase_2_t3381670217 *)__this)->get_observer_0();
			il2cpp_codegen_memory_barrier();
			NullCheck((Il2CppObject*)L_3);
			InterfaceActionInvoker0::Invoke(0 /* System.Void UniRx.IObserver`1<System.Object>::OnCompleted() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7), (Il2CppObject*)L_3);
			IL2CPP_LEAVE(0x38, FINALLY_0031);
		} // end try (depth: 2)
		catch(Il2CppExceptionWrapper& e)
		{
			__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
			goto FINALLY_0031;
		}

FINALLY_0031:
		{ // begin finally (depth: 2)
			NullCheck((OperatorObserverBase_2_t3381670217 *)__this);
			VirtActionInvoker0::Invoke(4 /* System.Void UniRx.Operators.OperatorObserverBase`2<UniRx.IObservable`1<System.Object>,System.Object>::Dispose() */, (OperatorObserverBase_2_t3381670217 *)__this);
			IL2CPP_END_FINALLY(49)
		} // end finally (depth: 2)
		IL2CPP_CLEANUP(49)
		{
			IL2CPP_JUMP_TBL(0x38, IL_0038)
			IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
		}

IL_0038:
		{
			goto IL_0048;
		}

IL_003d:
		{
			SingleAssignmentDisposable_t2336378823 * L_4 = (SingleAssignmentDisposable_t2336378823 *)__this->get_sourceDisposable_4();
			NullCheck((SingleAssignmentDisposable_t2336378823 *)L_4);
			VirtActionInvoker0::Invoke(4 /* System.Void UniRx.SingleAssignmentDisposable::Dispose() */, (SingleAssignmentDisposable_t2336378823 *)L_4);
		}

IL_0048:
		{
			IL2CPP_LEAVE(0x54, FINALLY_004d);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_004d;
	}

FINALLY_004d:
	{ // begin finally (depth: 1)
		Il2CppObject * L_5 = V_0;
		Monitor_Exit_m2088237919(NULL /*static, unused*/, (Il2CppObject *)L_5, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(77)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(77)
	{
		IL2CPP_JUMP_TBL(0x54, IL_0054)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0054:
	{
		return;
	}
}
// System.Void UniRx.Operators.MergeObservable`1/MergeConcurrentObserver<System.Object>::Subscribe(UniRx.IObservable`1<T>)
extern Il2CppClass* SingleAssignmentDisposable_t2336378823_il2cpp_TypeInfo_var;
extern const uint32_t MergeConcurrentObserver_Subscribe_m3366134245_MetadataUsageId;
extern "C"  void MergeConcurrentObserver_Subscribe_m3366134245_gshared (MergeConcurrentObserver_t3534593302 * __this, Il2CppObject* ___innerSource0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (MergeConcurrentObserver_Subscribe_m3366134245_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	SingleAssignmentDisposable_t2336378823 * V_0 = NULL;
	Merge_t1753429969 * V_1 = NULL;
	{
		SingleAssignmentDisposable_t2336378823 * L_0 = (SingleAssignmentDisposable_t2336378823 *)il2cpp_codegen_object_new(SingleAssignmentDisposable_t2336378823_il2cpp_TypeInfo_var);
		SingleAssignmentDisposable__ctor_m917449122(L_0, /*hidden argument*/NULL);
		V_0 = (SingleAssignmentDisposable_t2336378823 *)L_0;
		CompositeDisposable_t1894629977 * L_1 = (CompositeDisposable_t1894629977 *)__this->get_collectionDisposable_3();
		SingleAssignmentDisposable_t2336378823 * L_2 = V_0;
		NullCheck((CompositeDisposable_t1894629977 *)L_1);
		CompositeDisposable_Add_m2171552957((CompositeDisposable_t1894629977 *)L_1, (Il2CppObject *)L_2, /*hidden argument*/NULL);
		SingleAssignmentDisposable_t2336378823 * L_3 = V_0;
		Merge_t1753429969 * L_4 = (Merge_t1753429969 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9));
		((  void (*) (Merge_t1753429969 *, MergeConcurrentObserver_t3534593302 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10)->method)(L_4, (MergeConcurrentObserver_t3534593302 *)__this, (Il2CppObject *)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10));
		V_1 = (Merge_t1753429969 *)L_4;
		SingleAssignmentDisposable_t2336378823 * L_5 = V_0;
		Il2CppObject* L_6 = ___innerSource0;
		Merge_t1753429969 * L_7 = V_1;
		NullCheck((Il2CppObject*)L_6);
		Il2CppObject * L_8 = InterfaceFuncInvoker1< Il2CppObject *, Il2CppObject* >::Invoke(0 /* System.IDisposable UniRx.IObservable`1<System.Object>::Subscribe(UniRx.IObserver`1<T>) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 11), (Il2CppObject*)L_6, (Il2CppObject*)L_7);
		NullCheck((SingleAssignmentDisposable_t2336378823 *)L_5);
		SingleAssignmentDisposable_set_Disposable_m1599028885((SingleAssignmentDisposable_t2336378823 *)L_5, (Il2CppObject *)L_8, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UniRx.Operators.MergeObservable`1/MergeConcurrentObserver<UniRx.Unit>::.ctor(UniRx.Operators.MergeObservable`1<T>,UniRx.IObserver`1<T>,System.IDisposable)
extern Il2CppClass* Il2CppObject_il2cpp_TypeInfo_var;
extern const uint32_t MergeConcurrentObserver__ctor_m2772962521_MetadataUsageId;
extern "C"  void MergeConcurrentObserver__ctor_m2772962521_gshared (MergeConcurrentObserver_t960805624 * __this, MergeObservable_1_t1064204364 * ___parent0, Il2CppObject* ___observer1, Il2CppObject * ___cancel2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (MergeConcurrentObserver__ctor_m2772962521_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject * L_0 = (Il2CppObject *)il2cpp_codegen_object_new(Il2CppObject_il2cpp_TypeInfo_var);
		Object__ctor_m1772956182(L_0, /*hidden argument*/NULL);
		__this->set_gate_5(L_0);
		Il2CppObject* L_1 = ___observer1;
		Il2CppObject * L_2 = ___cancel2;
		NullCheck((OperatorObserverBase_2_t2064311169 *)__this);
		((  void (*) (OperatorObserverBase_2_t2064311169 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((OperatorObserverBase_2_t2064311169 *)__this, (Il2CppObject*)L_1, (Il2CppObject *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		MergeObservable_1_t1064204364 * L_3 = ___parent0;
		__this->set_parent_2(L_3);
		return;
	}
}
// System.IDisposable UniRx.Operators.MergeObservable`1/MergeConcurrentObserver<UniRx.Unit>::Run()
extern Il2CppClass* CompositeDisposable_t1894629977_il2cpp_TypeInfo_var;
extern Il2CppClass* SingleAssignmentDisposable_t2336378823_il2cpp_TypeInfo_var;
extern const uint32_t MergeConcurrentObserver_Run_m2388209898_MetadataUsageId;
extern "C"  Il2CppObject * MergeConcurrentObserver_Run_m2388209898_gshared (MergeConcurrentObserver_t960805624 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (MergeConcurrentObserver_Run_m2388209898_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Queue_1_t4025171942 * L_0 = (Queue_1_t4025171942 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		((  void (*) (Queue_1_t4025171942 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		__this->set_q_7(L_0);
		__this->set_activeCount_8(0);
		CompositeDisposable_t1894629977 * L_1 = (CompositeDisposable_t1894629977 *)il2cpp_codegen_object_new(CompositeDisposable_t1894629977_il2cpp_TypeInfo_var);
		CompositeDisposable__ctor_m1237994920(L_1, /*hidden argument*/NULL);
		__this->set_collectionDisposable_3(L_1);
		SingleAssignmentDisposable_t2336378823 * L_2 = (SingleAssignmentDisposable_t2336378823 *)il2cpp_codegen_object_new(SingleAssignmentDisposable_t2336378823_il2cpp_TypeInfo_var);
		SingleAssignmentDisposable__ctor_m917449122(L_2, /*hidden argument*/NULL);
		__this->set_sourceDisposable_4(L_2);
		CompositeDisposable_t1894629977 * L_3 = (CompositeDisposable_t1894629977 *)__this->get_collectionDisposable_3();
		SingleAssignmentDisposable_t2336378823 * L_4 = (SingleAssignmentDisposable_t2336378823 *)__this->get_sourceDisposable_4();
		NullCheck((CompositeDisposable_t1894629977 *)L_3);
		CompositeDisposable_Add_m2171552957((CompositeDisposable_t1894629977 *)L_3, (Il2CppObject *)L_4, /*hidden argument*/NULL);
		SingleAssignmentDisposable_t2336378823 * L_5 = (SingleAssignmentDisposable_t2336378823 *)__this->get_sourceDisposable_4();
		MergeObservable_1_t1064204364 * L_6 = (MergeObservable_1_t1064204364 *)__this->get_parent_2();
		NullCheck(L_6);
		Il2CppObject* L_7 = (Il2CppObject*)L_6->get_sources_1();
		NullCheck((Il2CppObject*)L_7);
		Il2CppObject * L_8 = InterfaceFuncInvoker1< Il2CppObject *, Il2CppObject* >::Invoke(0 /* System.IDisposable UniRx.IObservable`1<UniRx.IObservable`1<UniRx.Unit>>::Subscribe(UniRx.IObserver`1<T>) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4), (Il2CppObject*)L_7, (Il2CppObject*)__this);
		NullCheck((SingleAssignmentDisposable_t2336378823 *)L_5);
		SingleAssignmentDisposable_set_Disposable_m1599028885((SingleAssignmentDisposable_t2336378823 *)L_5, (Il2CppObject *)L_8, /*hidden argument*/NULL);
		CompositeDisposable_t1894629977 * L_9 = (CompositeDisposable_t1894629977 *)__this->get_collectionDisposable_3();
		return L_9;
	}
}
// System.Void UniRx.Operators.MergeObservable`1/MergeConcurrentObserver<UniRx.Unit>::OnNext(UniRx.IObservable`1<T>)
extern "C"  void MergeConcurrentObserver_OnNext_m3567754127_gshared (MergeConcurrentObserver_t960805624 * __this, Il2CppObject* ___value0, const MethodInfo* method)
{
	Il2CppObject * V_0 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_gate_5();
		V_0 = (Il2CppObject *)L_0;
		Il2CppObject * L_1 = V_0;
		Monitor_Enter_m476686225(NULL /*static, unused*/, (Il2CppObject *)L_1, /*hidden argument*/NULL);
	}

IL_000d:
	try
	{ // begin try (depth: 1)
		{
			int32_t L_2 = (int32_t)__this->get_activeCount_8();
			MergeObservable_1_t1064204364 * L_3 = (MergeObservable_1_t1064204364 *)__this->get_parent_2();
			NullCheck(L_3);
			int32_t L_4 = (int32_t)L_3->get_maxConcurrent_2();
			if ((((int32_t)L_2) >= ((int32_t)L_4)))
			{
				goto IL_003d;
			}
		}

IL_0023:
		{
			int32_t L_5 = (int32_t)__this->get_activeCount_8();
			__this->set_activeCount_8(((int32_t)((int32_t)L_5+(int32_t)1)));
			Il2CppObject* L_6 = ___value0;
			NullCheck((MergeConcurrentObserver_t960805624 *)__this);
			((  void (*) (MergeConcurrentObserver_t960805624 *, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->method)((MergeConcurrentObserver_t960805624 *)__this, (Il2CppObject*)L_6, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
			goto IL_0049;
		}

IL_003d:
		{
			Queue_1_t4025171942 * L_7 = (Queue_1_t4025171942 *)__this->get_q_7();
			Il2CppObject* L_8 = ___value0;
			NullCheck((Queue_1_t4025171942 *)L_7);
			((  void (*) (Queue_1_t4025171942 *, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6)->method)((Queue_1_t4025171942 *)L_7, (Il2CppObject*)L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		}

IL_0049:
		{
			IL2CPP_LEAVE(0x55, FINALLY_004e);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_004e;
	}

FINALLY_004e:
	{ // begin finally (depth: 1)
		Il2CppObject * L_9 = V_0;
		Monitor_Exit_m2088237919(NULL /*static, unused*/, (Il2CppObject *)L_9, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(78)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(78)
	{
		IL2CPP_JUMP_TBL(0x55, IL_0055)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0055:
	{
		return;
	}
}
// System.Void UniRx.Operators.MergeObservable`1/MergeConcurrentObserver<UniRx.Unit>::OnError(System.Exception)
extern "C"  void MergeConcurrentObserver_OnError_m2293113021_gshared (MergeConcurrentObserver_t960805624 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method)
{
	Il2CppObject * V_0 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_gate_5();
		V_0 = (Il2CppObject *)L_0;
		Il2CppObject * L_1 = V_0;
		Monitor_Enter_m476686225(NULL /*static, unused*/, (Il2CppObject *)L_1, /*hidden argument*/NULL);
	}

IL_000d:
	try
	{ // begin try (depth: 1)
		try
		{ // begin try (depth: 2)
			Il2CppObject* L_2 = (Il2CppObject*)((OperatorObserverBase_2_t2064311169 *)__this)->get_observer_0();
			il2cpp_codegen_memory_barrier();
			Exception_t1967233988 * L_3 = ___error0;
			NullCheck((Il2CppObject*)L_2);
			InterfaceActionInvoker1< Exception_t1967233988 * >::Invoke(1 /* System.Void UniRx.IObserver`1<UniRx.Unit>::OnError(System.Exception) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7), (Il2CppObject*)L_2, (Exception_t1967233988 *)L_3);
			IL2CPP_LEAVE(0x27, FINALLY_0020);
		} // end try (depth: 2)
		catch(Il2CppExceptionWrapper& e)
		{
			__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
			goto FINALLY_0020;
		}

FINALLY_0020:
		{ // begin finally (depth: 2)
			NullCheck((OperatorObserverBase_2_t2064311169 *)__this);
			VirtActionInvoker0::Invoke(4 /* System.Void UniRx.Operators.OperatorObserverBase`2<UniRx.IObservable`1<UniRx.Unit>,UniRx.Unit>::Dispose() */, (OperatorObserverBase_2_t2064311169 *)__this);
			IL2CPP_END_FINALLY(32)
		} // end finally (depth: 2)
		IL2CPP_CLEANUP(32)
		{
			IL2CPP_JUMP_TBL(0x27, IL_0027)
			IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
		}

IL_0027:
		{
			IL2CPP_LEAVE(0x33, FINALLY_002c);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_002c;
	}

FINALLY_002c:
	{ // begin finally (depth: 1)
		Il2CppObject * L_4 = V_0;
		Monitor_Exit_m2088237919(NULL /*static, unused*/, (Il2CppObject *)L_4, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(44)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(44)
	{
		IL2CPP_JUMP_TBL(0x33, IL_0033)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0033:
	{
		return;
	}
}
// System.Void UniRx.Operators.MergeObservable`1/MergeConcurrentObserver<UniRx.Unit>::OnCompleted()
extern "C"  void MergeConcurrentObserver_OnCompleted_m2659338128_gshared (MergeConcurrentObserver_t960805624 * __this, const MethodInfo* method)
{
	Il2CppObject * V_0 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_gate_5();
		V_0 = (Il2CppObject *)L_0;
		Il2CppObject * L_1 = V_0;
		Monitor_Enter_m476686225(NULL /*static, unused*/, (Il2CppObject *)L_1, /*hidden argument*/NULL);
	}

IL_000d:
	try
	{ // begin try (depth: 1)
		{
			__this->set_isStopped_6((bool)1);
			int32_t L_2 = (int32_t)__this->get_activeCount_8();
			if (L_2)
			{
				goto IL_003d;
			}
		}

IL_001f:
		try
		{ // begin try (depth: 2)
			Il2CppObject* L_3 = (Il2CppObject*)((OperatorObserverBase_2_t2064311169 *)__this)->get_observer_0();
			il2cpp_codegen_memory_barrier();
			NullCheck((Il2CppObject*)L_3);
			InterfaceActionInvoker0::Invoke(0 /* System.Void UniRx.IObserver`1<UniRx.Unit>::OnCompleted() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7), (Il2CppObject*)L_3);
			IL2CPP_LEAVE(0x38, FINALLY_0031);
		} // end try (depth: 2)
		catch(Il2CppExceptionWrapper& e)
		{
			__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
			goto FINALLY_0031;
		}

FINALLY_0031:
		{ // begin finally (depth: 2)
			NullCheck((OperatorObserverBase_2_t2064311169 *)__this);
			VirtActionInvoker0::Invoke(4 /* System.Void UniRx.Operators.OperatorObserverBase`2<UniRx.IObservable`1<UniRx.Unit>,UniRx.Unit>::Dispose() */, (OperatorObserverBase_2_t2064311169 *)__this);
			IL2CPP_END_FINALLY(49)
		} // end finally (depth: 2)
		IL2CPP_CLEANUP(49)
		{
			IL2CPP_JUMP_TBL(0x38, IL_0038)
			IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
		}

IL_0038:
		{
			goto IL_0048;
		}

IL_003d:
		{
			SingleAssignmentDisposable_t2336378823 * L_4 = (SingleAssignmentDisposable_t2336378823 *)__this->get_sourceDisposable_4();
			NullCheck((SingleAssignmentDisposable_t2336378823 *)L_4);
			VirtActionInvoker0::Invoke(4 /* System.Void UniRx.SingleAssignmentDisposable::Dispose() */, (SingleAssignmentDisposable_t2336378823 *)L_4);
		}

IL_0048:
		{
			IL2CPP_LEAVE(0x54, FINALLY_004d);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_004d;
	}

FINALLY_004d:
	{ // begin finally (depth: 1)
		Il2CppObject * L_5 = V_0;
		Monitor_Exit_m2088237919(NULL /*static, unused*/, (Il2CppObject *)L_5, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(77)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(77)
	{
		IL2CPP_JUMP_TBL(0x54, IL_0054)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0054:
	{
		return;
	}
}
// System.Void UniRx.Operators.MergeObservable`1/MergeConcurrentObserver<UniRx.Unit>::Subscribe(UniRx.IObservable`1<T>)
extern Il2CppClass* SingleAssignmentDisposable_t2336378823_il2cpp_TypeInfo_var;
extern const uint32_t MergeConcurrentObserver_Subscribe_m2627665485_MetadataUsageId;
extern "C"  void MergeConcurrentObserver_Subscribe_m2627665485_gshared (MergeConcurrentObserver_t960805624 * __this, Il2CppObject* ___innerSource0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (MergeConcurrentObserver_Subscribe_m2627665485_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	SingleAssignmentDisposable_t2336378823 * V_0 = NULL;
	Merge_t3474609587 * V_1 = NULL;
	{
		SingleAssignmentDisposable_t2336378823 * L_0 = (SingleAssignmentDisposable_t2336378823 *)il2cpp_codegen_object_new(SingleAssignmentDisposable_t2336378823_il2cpp_TypeInfo_var);
		SingleAssignmentDisposable__ctor_m917449122(L_0, /*hidden argument*/NULL);
		V_0 = (SingleAssignmentDisposable_t2336378823 *)L_0;
		CompositeDisposable_t1894629977 * L_1 = (CompositeDisposable_t1894629977 *)__this->get_collectionDisposable_3();
		SingleAssignmentDisposable_t2336378823 * L_2 = V_0;
		NullCheck((CompositeDisposable_t1894629977 *)L_1);
		CompositeDisposable_Add_m2171552957((CompositeDisposable_t1894629977 *)L_1, (Il2CppObject *)L_2, /*hidden argument*/NULL);
		SingleAssignmentDisposable_t2336378823 * L_3 = V_0;
		Merge_t3474609587 * L_4 = (Merge_t3474609587 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9));
		((  void (*) (Merge_t3474609587 *, MergeConcurrentObserver_t960805624 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10)->method)(L_4, (MergeConcurrentObserver_t960805624 *)__this, (Il2CppObject *)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10));
		V_1 = (Merge_t3474609587 *)L_4;
		SingleAssignmentDisposable_t2336378823 * L_5 = V_0;
		Il2CppObject* L_6 = ___innerSource0;
		Merge_t3474609587 * L_7 = V_1;
		NullCheck((Il2CppObject*)L_6);
		Il2CppObject * L_8 = InterfaceFuncInvoker1< Il2CppObject *, Il2CppObject* >::Invoke(0 /* System.IDisposable UniRx.IObservable`1<UniRx.Unit>::Subscribe(UniRx.IObserver`1<T>) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 11), (Il2CppObject*)L_6, (Il2CppObject*)L_7);
		NullCheck((SingleAssignmentDisposable_t2336378823 *)L_5);
		SingleAssignmentDisposable_set_Disposable_m1599028885((SingleAssignmentDisposable_t2336378823 *)L_5, (Il2CppObject *)L_8, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UniRx.Operators.MergeObservable`1/MergeOuterObserver/Merge<System.Object>::.ctor(UniRx.Operators.MergeObservable`1/MergeOuterObserver<T>,System.IDisposable)
extern "C"  void Merge__ctor_m4128364116_gshared (Merge_t1753429970 * __this, MergeOuterObserver_t1613081930 * ___parent0, Il2CppObject * ___cancel1, const MethodInfo* method)
{
	{
		MergeOuterObserver_t1613081930 * L_0 = ___parent0;
		NullCheck(L_0);
		Il2CppObject* L_1 = (Il2CppObject*)((OperatorObserverBase_2_t3381670217 *)L_0)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		Il2CppObject * L_2 = ___cancel1;
		NullCheck((OperatorObserverBase_2_t1187768149 *)__this);
		((  void (*) (OperatorObserverBase_2_t1187768149 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((OperatorObserverBase_2_t1187768149 *)__this, (Il2CppObject*)L_1, (Il2CppObject *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		MergeOuterObserver_t1613081930 * L_3 = ___parent0;
		__this->set_parent_2(L_3);
		Il2CppObject * L_4 = ___cancel1;
		__this->set_cancel_3(L_4);
		return;
	}
}
// System.Void UniRx.Operators.MergeObservable`1/MergeOuterObserver/Merge<System.Object>::OnNext(T)
extern "C"  void Merge_OnNext_m3563486975_gshared (Merge_t1753429970 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	Il2CppObject * V_0 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		MergeOuterObserver_t1613081930 * L_0 = (MergeOuterObserver_t1613081930 *)__this->get_parent_2();
		NullCheck(L_0);
		Il2CppObject * L_1 = (Il2CppObject *)L_0->get_gate_5();
		V_0 = (Il2CppObject *)L_1;
		Il2CppObject * L_2 = V_0;
		Monitor_Enter_m476686225(NULL /*static, unused*/, (Il2CppObject *)L_2, /*hidden argument*/NULL);
	}

IL_0012:
	try
	{ // begin try (depth: 1)
		Il2CppObject* L_3 = (Il2CppObject*)((OperatorObserverBase_2_t1187768149 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		Il2CppObject * L_4 = ___value0;
		NullCheck((Il2CppObject*)L_3);
		InterfaceActionInvoker1< Il2CppObject * >::Invoke(2 /* System.Void UniRx.IObserver`1<System.Object>::OnNext(T) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), (Il2CppObject*)L_3, (Il2CppObject *)L_4);
		IL2CPP_LEAVE(0x2C, FINALLY_0025);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0025;
	}

FINALLY_0025:
	{ // begin finally (depth: 1)
		Il2CppObject * L_5 = V_0;
		Monitor_Exit_m2088237919(NULL /*static, unused*/, (Il2CppObject *)L_5, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(37)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(37)
	{
		IL2CPP_JUMP_TBL(0x2C, IL_002c)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_002c:
	{
		return;
	}
}
// System.Void UniRx.Operators.MergeObservable`1/MergeOuterObserver/Merge<System.Object>::OnError(System.Exception)
extern "C"  void Merge_OnError_m673365006_gshared (Merge_t1753429970 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method)
{
	Il2CppObject * V_0 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		MergeOuterObserver_t1613081930 * L_0 = (MergeOuterObserver_t1613081930 *)__this->get_parent_2();
		NullCheck(L_0);
		Il2CppObject * L_1 = (Il2CppObject *)L_0->get_gate_5();
		V_0 = (Il2CppObject *)L_1;
		Il2CppObject * L_2 = V_0;
		Monitor_Enter_m476686225(NULL /*static, unused*/, (Il2CppObject *)L_2, /*hidden argument*/NULL);
	}

IL_0012:
	try
	{ // begin try (depth: 1)
		try
		{ // begin try (depth: 2)
			Il2CppObject* L_3 = (Il2CppObject*)((OperatorObserverBase_2_t1187768149 *)__this)->get_observer_0();
			il2cpp_codegen_memory_barrier();
			Exception_t1967233988 * L_4 = ___error0;
			NullCheck((Il2CppObject*)L_3);
			InterfaceActionInvoker1< Exception_t1967233988 * >::Invoke(1 /* System.Void UniRx.IObserver`1<System.Object>::OnError(System.Exception) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), (Il2CppObject*)L_3, (Exception_t1967233988 *)L_4);
			IL2CPP_LEAVE(0x2C, FINALLY_0025);
		} // end try (depth: 2)
		catch(Il2CppExceptionWrapper& e)
		{
			__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
			goto FINALLY_0025;
		}

FINALLY_0025:
		{ // begin finally (depth: 2)
			NullCheck((OperatorObserverBase_2_t1187768149 *)__this);
			VirtActionInvoker0::Invoke(4 /* System.Void UniRx.Operators.OperatorObserverBase`2<System.Object,System.Object>::Dispose() */, (OperatorObserverBase_2_t1187768149 *)__this);
			IL2CPP_END_FINALLY(37)
		} // end finally (depth: 2)
		IL2CPP_CLEANUP(37)
		{
			IL2CPP_JUMP_TBL(0x2C, IL_002c)
			IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
		}

IL_002c:
		{
			IL2CPP_LEAVE(0x38, FINALLY_0031);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0031;
	}

FINALLY_0031:
	{ // begin finally (depth: 1)
		Il2CppObject * L_5 = V_0;
		Monitor_Exit_m2088237919(NULL /*static, unused*/, (Il2CppObject *)L_5, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(49)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(49)
	{
		IL2CPP_JUMP_TBL(0x38, IL_0038)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0038:
	{
		return;
	}
}
// System.Void UniRx.Operators.MergeObservable`1/MergeOuterObserver/Merge<System.Object>::OnCompleted()
extern "C"  void Merge_OnCompleted_m2215679073_gshared (Merge_t1753429970 * __this, const MethodInfo* method)
{
	Il2CppObject * V_0 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		MergeOuterObserver_t1613081930 * L_0 = (MergeOuterObserver_t1613081930 *)__this->get_parent_2();
		NullCheck(L_0);
		CompositeDisposable_t1894629977 * L_1 = (CompositeDisposable_t1894629977 *)L_0->get_collectionDisposable_3();
		Il2CppObject * L_2 = (Il2CppObject *)__this->get_cancel_3();
		NullCheck((CompositeDisposable_t1894629977 *)L_1);
		CompositeDisposable_Remove_m495331162((CompositeDisposable_t1894629977 *)L_1, (Il2CppObject *)L_2, /*hidden argument*/NULL);
		MergeOuterObserver_t1613081930 * L_3 = (MergeOuterObserver_t1613081930 *)__this->get_parent_2();
		NullCheck(L_3);
		bool L_4 = (bool)L_3->get_isStopped_6();
		if (!L_4)
		{
			goto IL_0074;
		}
	}
	{
		MergeOuterObserver_t1613081930 * L_5 = (MergeOuterObserver_t1613081930 *)__this->get_parent_2();
		NullCheck(L_5);
		CompositeDisposable_t1894629977 * L_6 = (CompositeDisposable_t1894629977 *)L_5->get_collectionDisposable_3();
		NullCheck((CompositeDisposable_t1894629977 *)L_6);
		int32_t L_7 = CompositeDisposable_get_Count_m1817945890((CompositeDisposable_t1894629977 *)L_6, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_7) == ((uint32_t)1))))
		{
			goto IL_0074;
		}
	}
	{
		MergeOuterObserver_t1613081930 * L_8 = (MergeOuterObserver_t1613081930 *)__this->get_parent_2();
		NullCheck(L_8);
		Il2CppObject * L_9 = (Il2CppObject *)L_8->get_gate_5();
		V_0 = (Il2CppObject *)L_9;
		Il2CppObject * L_10 = V_0;
		Monitor_Enter_m476686225(NULL /*static, unused*/, (Il2CppObject *)L_10, /*hidden argument*/NULL);
	}

IL_004f:
	try
	{ // begin try (depth: 1)
		try
		{ // begin try (depth: 2)
			Il2CppObject* L_11 = (Il2CppObject*)((OperatorObserverBase_2_t1187768149 *)__this)->get_observer_0();
			il2cpp_codegen_memory_barrier();
			NullCheck((Il2CppObject*)L_11);
			InterfaceActionInvoker0::Invoke(0 /* System.Void UniRx.IObserver`1<System.Object>::OnCompleted() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), (Il2CppObject*)L_11);
			IL2CPP_LEAVE(0x68, FINALLY_0061);
		} // end try (depth: 2)
		catch(Il2CppExceptionWrapper& e)
		{
			__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
			goto FINALLY_0061;
		}

FINALLY_0061:
		{ // begin finally (depth: 2)
			NullCheck((OperatorObserverBase_2_t1187768149 *)__this);
			VirtActionInvoker0::Invoke(4 /* System.Void UniRx.Operators.OperatorObserverBase`2<System.Object,System.Object>::Dispose() */, (OperatorObserverBase_2_t1187768149 *)__this);
			IL2CPP_END_FINALLY(97)
		} // end finally (depth: 2)
		IL2CPP_CLEANUP(97)
		{
			IL2CPP_JUMP_TBL(0x68, IL_0068)
			IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
		}

IL_0068:
		{
			IL2CPP_LEAVE(0x74, FINALLY_006d);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_006d;
	}

FINALLY_006d:
	{ // begin finally (depth: 1)
		Il2CppObject * L_12 = V_0;
		Monitor_Exit_m2088237919(NULL /*static, unused*/, (Il2CppObject *)L_12, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(109)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(109)
	{
		IL2CPP_JUMP_TBL(0x74, IL_0074)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0074:
	{
		return;
	}
}
// System.Void UniRx.Operators.MergeObservable`1/MergeOuterObserver/Merge<UniRx.Unit>::.ctor(UniRx.Operators.MergeObservable`1/MergeOuterObserver<T>,System.IDisposable)
extern "C"  void Merge__ctor_m3303531498_gshared (Merge_t3474609588 * __this, MergeOuterObserver_t3334261548 * ___parent0, Il2CppObject * ___cancel1, const MethodInfo* method)
{
	{
		MergeOuterObserver_t3334261548 * L_0 = ___parent0;
		NullCheck(L_0);
		Il2CppObject* L_1 = (Il2CppObject*)((OperatorObserverBase_2_t2064311169 *)L_0)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		Il2CppObject * L_2 = ___cancel1;
		NullCheck((OperatorObserverBase_2_t4165376397 *)__this);
		((  void (*) (OperatorObserverBase_2_t4165376397 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((OperatorObserverBase_2_t4165376397 *)__this, (Il2CppObject*)L_1, (Il2CppObject *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		MergeOuterObserver_t3334261548 * L_3 = ___parent0;
		__this->set_parent_2(L_3);
		Il2CppObject * L_4 = ___cancel1;
		__this->set_cancel_3(L_4);
		return;
	}
}
// System.Void UniRx.Operators.MergeObservable`1/MergeOuterObserver/Merge<UniRx.Unit>::OnNext(T)
extern "C"  void Merge_OnNext_m784216725_gshared (Merge_t3474609588 * __this, Unit_t2558286038  ___value0, const MethodInfo* method)
{
	Il2CppObject * V_0 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		MergeOuterObserver_t3334261548 * L_0 = (MergeOuterObserver_t3334261548 *)__this->get_parent_2();
		NullCheck(L_0);
		Il2CppObject * L_1 = (Il2CppObject *)L_0->get_gate_5();
		V_0 = (Il2CppObject *)L_1;
		Il2CppObject * L_2 = V_0;
		Monitor_Enter_m476686225(NULL /*static, unused*/, (Il2CppObject *)L_2, /*hidden argument*/NULL);
	}

IL_0012:
	try
	{ // begin try (depth: 1)
		Il2CppObject* L_3 = (Il2CppObject*)((OperatorObserverBase_2_t4165376397 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		Unit_t2558286038  L_4 = ___value0;
		NullCheck((Il2CppObject*)L_3);
		InterfaceActionInvoker1< Unit_t2558286038  >::Invoke(2 /* System.Void UniRx.IObserver`1<UniRx.Unit>::OnNext(T) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), (Il2CppObject*)L_3, (Unit_t2558286038 )L_4);
		IL2CPP_LEAVE(0x2C, FINALLY_0025);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0025;
	}

FINALLY_0025:
	{ // begin finally (depth: 1)
		Il2CppObject * L_5 = V_0;
		Monitor_Exit_m2088237919(NULL /*static, unused*/, (Il2CppObject *)L_5, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(37)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(37)
	{
		IL2CPP_JUMP_TBL(0x2C, IL_002c)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_002c:
	{
		return;
	}
}
// System.Void UniRx.Operators.MergeObservable`1/MergeOuterObserver/Merge<UniRx.Unit>::OnError(System.Exception)
extern "C"  void Merge_OnError_m4265378724_gshared (Merge_t3474609588 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method)
{
	Il2CppObject * V_0 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		MergeOuterObserver_t3334261548 * L_0 = (MergeOuterObserver_t3334261548 *)__this->get_parent_2();
		NullCheck(L_0);
		Il2CppObject * L_1 = (Il2CppObject *)L_0->get_gate_5();
		V_0 = (Il2CppObject *)L_1;
		Il2CppObject * L_2 = V_0;
		Monitor_Enter_m476686225(NULL /*static, unused*/, (Il2CppObject *)L_2, /*hidden argument*/NULL);
	}

IL_0012:
	try
	{ // begin try (depth: 1)
		try
		{ // begin try (depth: 2)
			Il2CppObject* L_3 = (Il2CppObject*)((OperatorObserverBase_2_t4165376397 *)__this)->get_observer_0();
			il2cpp_codegen_memory_barrier();
			Exception_t1967233988 * L_4 = ___error0;
			NullCheck((Il2CppObject*)L_3);
			InterfaceActionInvoker1< Exception_t1967233988 * >::Invoke(1 /* System.Void UniRx.IObserver`1<UniRx.Unit>::OnError(System.Exception) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), (Il2CppObject*)L_3, (Exception_t1967233988 *)L_4);
			IL2CPP_LEAVE(0x2C, FINALLY_0025);
		} // end try (depth: 2)
		catch(Il2CppExceptionWrapper& e)
		{
			__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
			goto FINALLY_0025;
		}

FINALLY_0025:
		{ // begin finally (depth: 2)
			NullCheck((OperatorObserverBase_2_t4165376397 *)__this);
			VirtActionInvoker0::Invoke(4 /* System.Void UniRx.Operators.OperatorObserverBase`2<UniRx.Unit,UniRx.Unit>::Dispose() */, (OperatorObserverBase_2_t4165376397 *)__this);
			IL2CPP_END_FINALLY(37)
		} // end finally (depth: 2)
		IL2CPP_CLEANUP(37)
		{
			IL2CPP_JUMP_TBL(0x2C, IL_002c)
			IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
		}

IL_002c:
		{
			IL2CPP_LEAVE(0x38, FINALLY_0031);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0031;
	}

FINALLY_0031:
	{ // begin finally (depth: 1)
		Il2CppObject * L_5 = V_0;
		Monitor_Exit_m2088237919(NULL /*static, unused*/, (Il2CppObject *)L_5, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(49)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(49)
	{
		IL2CPP_JUMP_TBL(0x38, IL_0038)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0038:
	{
		return;
	}
}
// System.Void UniRx.Operators.MergeObservable`1/MergeOuterObserver/Merge<UniRx.Unit>::OnCompleted()
extern "C"  void Merge_OnCompleted_m3180891383_gshared (Merge_t3474609588 * __this, const MethodInfo* method)
{
	Il2CppObject * V_0 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		MergeOuterObserver_t3334261548 * L_0 = (MergeOuterObserver_t3334261548 *)__this->get_parent_2();
		NullCheck(L_0);
		CompositeDisposable_t1894629977 * L_1 = (CompositeDisposable_t1894629977 *)L_0->get_collectionDisposable_3();
		Il2CppObject * L_2 = (Il2CppObject *)__this->get_cancel_3();
		NullCheck((CompositeDisposable_t1894629977 *)L_1);
		CompositeDisposable_Remove_m495331162((CompositeDisposable_t1894629977 *)L_1, (Il2CppObject *)L_2, /*hidden argument*/NULL);
		MergeOuterObserver_t3334261548 * L_3 = (MergeOuterObserver_t3334261548 *)__this->get_parent_2();
		NullCheck(L_3);
		bool L_4 = (bool)L_3->get_isStopped_6();
		if (!L_4)
		{
			goto IL_0074;
		}
	}
	{
		MergeOuterObserver_t3334261548 * L_5 = (MergeOuterObserver_t3334261548 *)__this->get_parent_2();
		NullCheck(L_5);
		CompositeDisposable_t1894629977 * L_6 = (CompositeDisposable_t1894629977 *)L_5->get_collectionDisposable_3();
		NullCheck((CompositeDisposable_t1894629977 *)L_6);
		int32_t L_7 = CompositeDisposable_get_Count_m1817945890((CompositeDisposable_t1894629977 *)L_6, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_7) == ((uint32_t)1))))
		{
			goto IL_0074;
		}
	}
	{
		MergeOuterObserver_t3334261548 * L_8 = (MergeOuterObserver_t3334261548 *)__this->get_parent_2();
		NullCheck(L_8);
		Il2CppObject * L_9 = (Il2CppObject *)L_8->get_gate_5();
		V_0 = (Il2CppObject *)L_9;
		Il2CppObject * L_10 = V_0;
		Monitor_Enter_m476686225(NULL /*static, unused*/, (Il2CppObject *)L_10, /*hidden argument*/NULL);
	}

IL_004f:
	try
	{ // begin try (depth: 1)
		try
		{ // begin try (depth: 2)
			Il2CppObject* L_11 = (Il2CppObject*)((OperatorObserverBase_2_t4165376397 *)__this)->get_observer_0();
			il2cpp_codegen_memory_barrier();
			NullCheck((Il2CppObject*)L_11);
			InterfaceActionInvoker0::Invoke(0 /* System.Void UniRx.IObserver`1<UniRx.Unit>::OnCompleted() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), (Il2CppObject*)L_11);
			IL2CPP_LEAVE(0x68, FINALLY_0061);
		} // end try (depth: 2)
		catch(Il2CppExceptionWrapper& e)
		{
			__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
			goto FINALLY_0061;
		}

FINALLY_0061:
		{ // begin finally (depth: 2)
			NullCheck((OperatorObserverBase_2_t4165376397 *)__this);
			VirtActionInvoker0::Invoke(4 /* System.Void UniRx.Operators.OperatorObserverBase`2<UniRx.Unit,UniRx.Unit>::Dispose() */, (OperatorObserverBase_2_t4165376397 *)__this);
			IL2CPP_END_FINALLY(97)
		} // end finally (depth: 2)
		IL2CPP_CLEANUP(97)
		{
			IL2CPP_JUMP_TBL(0x68, IL_0068)
			IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
		}

IL_0068:
		{
			IL2CPP_LEAVE(0x74, FINALLY_006d);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_006d;
	}

FINALLY_006d:
	{ // begin finally (depth: 1)
		Il2CppObject * L_12 = V_0;
		Monitor_Exit_m2088237919(NULL /*static, unused*/, (Il2CppObject *)L_12, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(109)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(109)
	{
		IL2CPP_JUMP_TBL(0x74, IL_0074)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0074:
	{
		return;
	}
}
// System.Void UniRx.Operators.MergeObservable`1/MergeOuterObserver<System.Object>::.ctor(UniRx.Operators.MergeObservable`1<T>,UniRx.IObserver`1<T>,System.IDisposable)
extern Il2CppClass* Il2CppObject_il2cpp_TypeInfo_var;
extern const uint32_t MergeOuterObserver__ctor_m3701700961_MetadataUsageId;
extern "C"  void MergeOuterObserver__ctor_m3701700961_gshared (MergeOuterObserver_t1613081930 * __this, MergeObservable_1_t3637992042 * ___parent0, Il2CppObject* ___observer1, Il2CppObject * ___cancel2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (MergeOuterObserver__ctor_m3701700961_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject * L_0 = (Il2CppObject *)il2cpp_codegen_object_new(Il2CppObject_il2cpp_TypeInfo_var);
		Object__ctor_m1772956182(L_0, /*hidden argument*/NULL);
		__this->set_gate_5(L_0);
		Il2CppObject* L_1 = ___observer1;
		Il2CppObject * L_2 = ___cancel2;
		NullCheck((OperatorObserverBase_2_t3381670217 *)__this);
		((  void (*) (OperatorObserverBase_2_t3381670217 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((OperatorObserverBase_2_t3381670217 *)__this, (Il2CppObject*)L_1, (Il2CppObject *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		MergeObservable_1_t3637992042 * L_3 = ___parent0;
		__this->set_parent_2(L_3);
		return;
	}
}
// System.IDisposable UniRx.Operators.MergeObservable`1/MergeOuterObserver<System.Object>::Run()
extern Il2CppClass* CompositeDisposable_t1894629977_il2cpp_TypeInfo_var;
extern Il2CppClass* SingleAssignmentDisposable_t2336378823_il2cpp_TypeInfo_var;
extern const uint32_t MergeOuterObserver_Run_m1261133362_MetadataUsageId;
extern "C"  Il2CppObject * MergeOuterObserver_Run_m1261133362_gshared (MergeOuterObserver_t1613081930 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (MergeOuterObserver_Run_m1261133362_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		CompositeDisposable_t1894629977 * L_0 = (CompositeDisposable_t1894629977 *)il2cpp_codegen_object_new(CompositeDisposable_t1894629977_il2cpp_TypeInfo_var);
		CompositeDisposable__ctor_m1237994920(L_0, /*hidden argument*/NULL);
		__this->set_collectionDisposable_3(L_0);
		SingleAssignmentDisposable_t2336378823 * L_1 = (SingleAssignmentDisposable_t2336378823 *)il2cpp_codegen_object_new(SingleAssignmentDisposable_t2336378823_il2cpp_TypeInfo_var);
		SingleAssignmentDisposable__ctor_m917449122(L_1, /*hidden argument*/NULL);
		__this->set_sourceDisposable_4(L_1);
		CompositeDisposable_t1894629977 * L_2 = (CompositeDisposable_t1894629977 *)__this->get_collectionDisposable_3();
		SingleAssignmentDisposable_t2336378823 * L_3 = (SingleAssignmentDisposable_t2336378823 *)__this->get_sourceDisposable_4();
		NullCheck((CompositeDisposable_t1894629977 *)L_2);
		CompositeDisposable_Add_m2171552957((CompositeDisposable_t1894629977 *)L_2, (Il2CppObject *)L_3, /*hidden argument*/NULL);
		SingleAssignmentDisposable_t2336378823 * L_4 = (SingleAssignmentDisposable_t2336378823 *)__this->get_sourceDisposable_4();
		MergeObservable_1_t3637992042 * L_5 = (MergeObservable_1_t3637992042 *)__this->get_parent_2();
		NullCheck(L_5);
		Il2CppObject* L_6 = (Il2CppObject*)L_5->get_sources_1();
		NullCheck((Il2CppObject*)L_6);
		Il2CppObject * L_7 = InterfaceFuncInvoker1< Il2CppObject *, Il2CppObject* >::Invoke(0 /* System.IDisposable UniRx.IObservable`1<UniRx.IObservable`1<System.Object>>::Subscribe(UniRx.IObserver`1<T>) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), (Il2CppObject*)L_6, (Il2CppObject*)__this);
		NullCheck((SingleAssignmentDisposable_t2336378823 *)L_4);
		SingleAssignmentDisposable_set_Disposable_m1599028885((SingleAssignmentDisposable_t2336378823 *)L_4, (Il2CppObject *)L_7, /*hidden argument*/NULL);
		CompositeDisposable_t1894629977 * L_8 = (CompositeDisposable_t1894629977 *)__this->get_collectionDisposable_3();
		return L_8;
	}
}
// System.Void UniRx.Operators.MergeObservable`1/MergeOuterObserver<System.Object>::OnNext(UniRx.IObservable`1<T>)
extern Il2CppClass* SingleAssignmentDisposable_t2336378823_il2cpp_TypeInfo_var;
extern const uint32_t MergeOuterObserver_OnNext_m960514567_MetadataUsageId;
extern "C"  void MergeOuterObserver_OnNext_m960514567_gshared (MergeOuterObserver_t1613081930 * __this, Il2CppObject* ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (MergeOuterObserver_OnNext_m960514567_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	SingleAssignmentDisposable_t2336378823 * V_0 = NULL;
	Merge_t1753429970 * V_1 = NULL;
	{
		SingleAssignmentDisposable_t2336378823 * L_0 = (SingleAssignmentDisposable_t2336378823 *)il2cpp_codegen_object_new(SingleAssignmentDisposable_t2336378823_il2cpp_TypeInfo_var);
		SingleAssignmentDisposable__ctor_m917449122(L_0, /*hidden argument*/NULL);
		V_0 = (SingleAssignmentDisposable_t2336378823 *)L_0;
		CompositeDisposable_t1894629977 * L_1 = (CompositeDisposable_t1894629977 *)__this->get_collectionDisposable_3();
		SingleAssignmentDisposable_t2336378823 * L_2 = V_0;
		NullCheck((CompositeDisposable_t1894629977 *)L_1);
		CompositeDisposable_Add_m2171552957((CompositeDisposable_t1894629977 *)L_1, (Il2CppObject *)L_2, /*hidden argument*/NULL);
		SingleAssignmentDisposable_t2336378823 * L_3 = V_0;
		Merge_t1753429970 * L_4 = (Merge_t1753429970 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		((  void (*) (Merge_t1753429970 *, MergeOuterObserver_t1613081930 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)(L_4, (MergeOuterObserver_t1613081930 *)__this, (Il2CppObject *)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		V_1 = (Merge_t1753429970 *)L_4;
		SingleAssignmentDisposable_t2336378823 * L_5 = V_0;
		Il2CppObject* L_6 = ___value0;
		Merge_t1753429970 * L_7 = V_1;
		NullCheck((Il2CppObject*)L_6);
		Il2CppObject * L_8 = InterfaceFuncInvoker1< Il2CppObject *, Il2CppObject* >::Invoke(0 /* System.IDisposable UniRx.IObservable`1<System.Object>::Subscribe(UniRx.IObserver`1<T>) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5), (Il2CppObject*)L_6, (Il2CppObject*)L_7);
		NullCheck((SingleAssignmentDisposable_t2336378823 *)L_5);
		SingleAssignmentDisposable_set_Disposable_m1599028885((SingleAssignmentDisposable_t2336378823 *)L_5, (Il2CppObject *)L_8, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UniRx.Operators.MergeObservable`1/MergeOuterObserver<System.Object>::OnError(System.Exception)
extern "C"  void MergeOuterObserver_OnError_m563103557_gshared (MergeOuterObserver_t1613081930 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method)
{
	Il2CppObject * V_0 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_gate_5();
		V_0 = (Il2CppObject *)L_0;
		Il2CppObject * L_1 = V_0;
		Monitor_Enter_m476686225(NULL /*static, unused*/, (Il2CppObject *)L_1, /*hidden argument*/NULL);
	}

IL_000d:
	try
	{ // begin try (depth: 1)
		try
		{ // begin try (depth: 2)
			Il2CppObject* L_2 = (Il2CppObject*)((OperatorObserverBase_2_t3381670217 *)__this)->get_observer_0();
			il2cpp_codegen_memory_barrier();
			Exception_t1967233988 * L_3 = ___error0;
			NullCheck((Il2CppObject*)L_2);
			InterfaceActionInvoker1< Exception_t1967233988 * >::Invoke(1 /* System.Void UniRx.IObserver`1<System.Object>::OnError(System.Exception) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6), (Il2CppObject*)L_2, (Exception_t1967233988 *)L_3);
			IL2CPP_LEAVE(0x27, FINALLY_0020);
		} // end try (depth: 2)
		catch(Il2CppExceptionWrapper& e)
		{
			__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
			goto FINALLY_0020;
		}

FINALLY_0020:
		{ // begin finally (depth: 2)
			NullCheck((OperatorObserverBase_2_t3381670217 *)__this);
			VirtActionInvoker0::Invoke(4 /* System.Void UniRx.Operators.OperatorObserverBase`2<UniRx.IObservable`1<System.Object>,System.Object>::Dispose() */, (OperatorObserverBase_2_t3381670217 *)__this);
			IL2CPP_END_FINALLY(32)
		} // end finally (depth: 2)
		IL2CPP_CLEANUP(32)
		{
			IL2CPP_JUMP_TBL(0x27, IL_0027)
			IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
		}

IL_0027:
		{
			IL2CPP_LEAVE(0x33, FINALLY_002c);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_002c;
	}

FINALLY_002c:
	{ // begin finally (depth: 1)
		Il2CppObject * L_4 = V_0;
		Monitor_Exit_m2088237919(NULL /*static, unused*/, (Il2CppObject *)L_4, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(44)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(44)
	{
		IL2CPP_JUMP_TBL(0x33, IL_0033)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0033:
	{
		return;
	}
}
// System.Void UniRx.Operators.MergeObservable`1/MergeOuterObserver<System.Object>::OnCompleted()
extern "C"  void MergeOuterObserver_OnCompleted_m3234069016_gshared (MergeOuterObserver_t1613081930 * __this, const MethodInfo* method)
{
	Il2CppObject * V_0 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		__this->set_isStopped_6((bool)1);
		CompositeDisposable_t1894629977 * L_0 = (CompositeDisposable_t1894629977 *)__this->get_collectionDisposable_3();
		NullCheck((CompositeDisposable_t1894629977 *)L_0);
		int32_t L_1 = CompositeDisposable_get_Count_m1817945890((CompositeDisposable_t1894629977 *)L_0, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_1) == ((uint32_t)1))))
		{
			goto IL_004f;
		}
	}
	{
		Il2CppObject * L_2 = (Il2CppObject *)__this->get_gate_5();
		V_0 = (Il2CppObject *)L_2;
		Il2CppObject * L_3 = V_0;
		Monitor_Enter_m476686225(NULL /*static, unused*/, (Il2CppObject *)L_3, /*hidden argument*/NULL);
	}

IL_0025:
	try
	{ // begin try (depth: 1)
		try
		{ // begin try (depth: 2)
			Il2CppObject* L_4 = (Il2CppObject*)((OperatorObserverBase_2_t3381670217 *)__this)->get_observer_0();
			il2cpp_codegen_memory_barrier();
			NullCheck((Il2CppObject*)L_4);
			InterfaceActionInvoker0::Invoke(0 /* System.Void UniRx.IObserver`1<System.Object>::OnCompleted() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6), (Il2CppObject*)L_4);
			IL2CPP_LEAVE(0x3E, FINALLY_0037);
		} // end try (depth: 2)
		catch(Il2CppExceptionWrapper& e)
		{
			__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
			goto FINALLY_0037;
		}

FINALLY_0037:
		{ // begin finally (depth: 2)
			NullCheck((OperatorObserverBase_2_t3381670217 *)__this);
			VirtActionInvoker0::Invoke(4 /* System.Void UniRx.Operators.OperatorObserverBase`2<UniRx.IObservable`1<System.Object>,System.Object>::Dispose() */, (OperatorObserverBase_2_t3381670217 *)__this);
			IL2CPP_END_FINALLY(55)
		} // end finally (depth: 2)
		IL2CPP_CLEANUP(55)
		{
			IL2CPP_JUMP_TBL(0x3E, IL_003e)
			IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
		}

IL_003e:
		{
			IL2CPP_LEAVE(0x4A, FINALLY_0043);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0043;
	}

FINALLY_0043:
	{ // begin finally (depth: 1)
		Il2CppObject * L_5 = V_0;
		Monitor_Exit_m2088237919(NULL /*static, unused*/, (Il2CppObject *)L_5, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(67)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(67)
	{
		IL2CPP_JUMP_TBL(0x4A, IL_004a)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_004a:
	{
		goto IL_005a;
	}

IL_004f:
	{
		SingleAssignmentDisposable_t2336378823 * L_6 = (SingleAssignmentDisposable_t2336378823 *)__this->get_sourceDisposable_4();
		NullCheck((SingleAssignmentDisposable_t2336378823 *)L_6);
		VirtActionInvoker0::Invoke(4 /* System.Void UniRx.SingleAssignmentDisposable::Dispose() */, (SingleAssignmentDisposable_t2336378823 *)L_6);
	}

IL_005a:
	{
		return;
	}
}
// System.Void UniRx.Operators.MergeObservable`1/MergeOuterObserver<UniRx.Unit>::.ctor(UniRx.Operators.MergeObservable`1<T>,UniRx.IObserver`1<T>,System.IDisposable)
extern Il2CppClass* Il2CppObject_il2cpp_TypeInfo_var;
extern const uint32_t MergeOuterObserver__ctor_m190944233_MetadataUsageId;
extern "C"  void MergeOuterObserver__ctor_m190944233_gshared (MergeOuterObserver_t3334261548 * __this, MergeObservable_1_t1064204364 * ___parent0, Il2CppObject* ___observer1, Il2CppObject * ___cancel2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (MergeOuterObserver__ctor_m190944233_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject * L_0 = (Il2CppObject *)il2cpp_codegen_object_new(Il2CppObject_il2cpp_TypeInfo_var);
		Object__ctor_m1772956182(L_0, /*hidden argument*/NULL);
		__this->set_gate_5(L_0);
		Il2CppObject* L_1 = ___observer1;
		Il2CppObject * L_2 = ___cancel2;
		NullCheck((OperatorObserverBase_2_t2064311169 *)__this);
		((  void (*) (OperatorObserverBase_2_t2064311169 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((OperatorObserverBase_2_t2064311169 *)__this, (Il2CppObject*)L_1, (Il2CppObject *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		MergeObservable_1_t1064204364 * L_3 = ___parent0;
		__this->set_parent_2(L_3);
		return;
	}
}
// System.IDisposable UniRx.Operators.MergeObservable`1/MergeOuterObserver<UniRx.Unit>::Run()
extern Il2CppClass* CompositeDisposable_t1894629977_il2cpp_TypeInfo_var;
extern Il2CppClass* SingleAssignmentDisposable_t2336378823_il2cpp_TypeInfo_var;
extern const uint32_t MergeOuterObserver_Run_m3558962596_MetadataUsageId;
extern "C"  Il2CppObject * MergeOuterObserver_Run_m3558962596_gshared (MergeOuterObserver_t3334261548 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (MergeOuterObserver_Run_m3558962596_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		CompositeDisposable_t1894629977 * L_0 = (CompositeDisposable_t1894629977 *)il2cpp_codegen_object_new(CompositeDisposable_t1894629977_il2cpp_TypeInfo_var);
		CompositeDisposable__ctor_m1237994920(L_0, /*hidden argument*/NULL);
		__this->set_collectionDisposable_3(L_0);
		SingleAssignmentDisposable_t2336378823 * L_1 = (SingleAssignmentDisposable_t2336378823 *)il2cpp_codegen_object_new(SingleAssignmentDisposable_t2336378823_il2cpp_TypeInfo_var);
		SingleAssignmentDisposable__ctor_m917449122(L_1, /*hidden argument*/NULL);
		__this->set_sourceDisposable_4(L_1);
		CompositeDisposable_t1894629977 * L_2 = (CompositeDisposable_t1894629977 *)__this->get_collectionDisposable_3();
		SingleAssignmentDisposable_t2336378823 * L_3 = (SingleAssignmentDisposable_t2336378823 *)__this->get_sourceDisposable_4();
		NullCheck((CompositeDisposable_t1894629977 *)L_2);
		CompositeDisposable_Add_m2171552957((CompositeDisposable_t1894629977 *)L_2, (Il2CppObject *)L_3, /*hidden argument*/NULL);
		SingleAssignmentDisposable_t2336378823 * L_4 = (SingleAssignmentDisposable_t2336378823 *)__this->get_sourceDisposable_4();
		MergeObservable_1_t1064204364 * L_5 = (MergeObservable_1_t1064204364 *)__this->get_parent_2();
		NullCheck(L_5);
		Il2CppObject* L_6 = (Il2CppObject*)L_5->get_sources_1();
		NullCheck((Il2CppObject*)L_6);
		Il2CppObject * L_7 = InterfaceFuncInvoker1< Il2CppObject *, Il2CppObject* >::Invoke(0 /* System.IDisposable UniRx.IObservable`1<UniRx.IObservable`1<UniRx.Unit>>::Subscribe(UniRx.IObserver`1<T>) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), (Il2CppObject*)L_6, (Il2CppObject*)__this);
		NullCheck((SingleAssignmentDisposable_t2336378823 *)L_4);
		SingleAssignmentDisposable_set_Disposable_m1599028885((SingleAssignmentDisposable_t2336378823 *)L_4, (Il2CppObject *)L_7, /*hidden argument*/NULL);
		CompositeDisposable_t1894629977 * L_8 = (CompositeDisposable_t1894629977 *)__this->get_collectionDisposable_3();
		return L_8;
	}
}
// System.Void UniRx.Operators.MergeObservable`1/MergeOuterObserver<UniRx.Unit>::OnNext(UniRx.IObservable`1<T>)
extern Il2CppClass* SingleAssignmentDisposable_t2336378823_il2cpp_TypeInfo_var;
extern const uint32_t MergeOuterObserver_OnNext_m3240987263_MetadataUsageId;
extern "C"  void MergeOuterObserver_OnNext_m3240987263_gshared (MergeOuterObserver_t3334261548 * __this, Il2CppObject* ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (MergeOuterObserver_OnNext_m3240987263_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	SingleAssignmentDisposable_t2336378823 * V_0 = NULL;
	Merge_t3474609588 * V_1 = NULL;
	{
		SingleAssignmentDisposable_t2336378823 * L_0 = (SingleAssignmentDisposable_t2336378823 *)il2cpp_codegen_object_new(SingleAssignmentDisposable_t2336378823_il2cpp_TypeInfo_var);
		SingleAssignmentDisposable__ctor_m917449122(L_0, /*hidden argument*/NULL);
		V_0 = (SingleAssignmentDisposable_t2336378823 *)L_0;
		CompositeDisposable_t1894629977 * L_1 = (CompositeDisposable_t1894629977 *)__this->get_collectionDisposable_3();
		SingleAssignmentDisposable_t2336378823 * L_2 = V_0;
		NullCheck((CompositeDisposable_t1894629977 *)L_1);
		CompositeDisposable_Add_m2171552957((CompositeDisposable_t1894629977 *)L_1, (Il2CppObject *)L_2, /*hidden argument*/NULL);
		SingleAssignmentDisposable_t2336378823 * L_3 = V_0;
		Merge_t3474609588 * L_4 = (Merge_t3474609588 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		((  void (*) (Merge_t3474609588 *, MergeOuterObserver_t3334261548 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)(L_4, (MergeOuterObserver_t3334261548 *)__this, (Il2CppObject *)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		V_1 = (Merge_t3474609588 *)L_4;
		SingleAssignmentDisposable_t2336378823 * L_5 = V_0;
		Il2CppObject* L_6 = ___value0;
		Merge_t3474609588 * L_7 = V_1;
		NullCheck((Il2CppObject*)L_6);
		Il2CppObject * L_8 = InterfaceFuncInvoker1< Il2CppObject *, Il2CppObject* >::Invoke(0 /* System.IDisposable UniRx.IObservable`1<UniRx.Unit>::Subscribe(UniRx.IObserver`1<T>) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5), (Il2CppObject*)L_6, (Il2CppObject*)L_7);
		NullCheck((SingleAssignmentDisposable_t2336378823 *)L_5);
		SingleAssignmentDisposable_set_Disposable_m1599028885((SingleAssignmentDisposable_t2336378823 *)L_5, (Il2CppObject *)L_8, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UniRx.Operators.MergeObservable`1/MergeOuterObserver<UniRx.Unit>::OnError(System.Exception)
extern "C"  void MergeOuterObserver_OnError_m4111113165_gshared (MergeOuterObserver_t3334261548 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method)
{
	Il2CppObject * V_0 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_gate_5();
		V_0 = (Il2CppObject *)L_0;
		Il2CppObject * L_1 = V_0;
		Monitor_Enter_m476686225(NULL /*static, unused*/, (Il2CppObject *)L_1, /*hidden argument*/NULL);
	}

IL_000d:
	try
	{ // begin try (depth: 1)
		try
		{ // begin try (depth: 2)
			Il2CppObject* L_2 = (Il2CppObject*)((OperatorObserverBase_2_t2064311169 *)__this)->get_observer_0();
			il2cpp_codegen_memory_barrier();
			Exception_t1967233988 * L_3 = ___error0;
			NullCheck((Il2CppObject*)L_2);
			InterfaceActionInvoker1< Exception_t1967233988 * >::Invoke(1 /* System.Void UniRx.IObserver`1<UniRx.Unit>::OnError(System.Exception) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6), (Il2CppObject*)L_2, (Exception_t1967233988 *)L_3);
			IL2CPP_LEAVE(0x27, FINALLY_0020);
		} // end try (depth: 2)
		catch(Il2CppExceptionWrapper& e)
		{
			__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
			goto FINALLY_0020;
		}

FINALLY_0020:
		{ // begin finally (depth: 2)
			NullCheck((OperatorObserverBase_2_t2064311169 *)__this);
			VirtActionInvoker0::Invoke(4 /* System.Void UniRx.Operators.OperatorObserverBase`2<UniRx.IObservable`1<UniRx.Unit>,UniRx.Unit>::Dispose() */, (OperatorObserverBase_2_t2064311169 *)__this);
			IL2CPP_END_FINALLY(32)
		} // end finally (depth: 2)
		IL2CPP_CLEANUP(32)
		{
			IL2CPP_JUMP_TBL(0x27, IL_0027)
			IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
		}

IL_0027:
		{
			IL2CPP_LEAVE(0x33, FINALLY_002c);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_002c;
	}

FINALLY_002c:
	{ // begin finally (depth: 1)
		Il2CppObject * L_4 = V_0;
		Monitor_Exit_m2088237919(NULL /*static, unused*/, (Il2CppObject *)L_4, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(44)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(44)
	{
		IL2CPP_JUMP_TBL(0x33, IL_0033)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0033:
	{
		return;
	}
}
// System.Void UniRx.Operators.MergeObservable`1/MergeOuterObserver<UniRx.Unit>::OnCompleted()
extern "C"  void MergeOuterObserver_OnCompleted_m2381503136_gshared (MergeOuterObserver_t3334261548 * __this, const MethodInfo* method)
{
	Il2CppObject * V_0 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		__this->set_isStopped_6((bool)1);
		CompositeDisposable_t1894629977 * L_0 = (CompositeDisposable_t1894629977 *)__this->get_collectionDisposable_3();
		NullCheck((CompositeDisposable_t1894629977 *)L_0);
		int32_t L_1 = CompositeDisposable_get_Count_m1817945890((CompositeDisposable_t1894629977 *)L_0, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_1) == ((uint32_t)1))))
		{
			goto IL_004f;
		}
	}
	{
		Il2CppObject * L_2 = (Il2CppObject *)__this->get_gate_5();
		V_0 = (Il2CppObject *)L_2;
		Il2CppObject * L_3 = V_0;
		Monitor_Enter_m476686225(NULL /*static, unused*/, (Il2CppObject *)L_3, /*hidden argument*/NULL);
	}

IL_0025:
	try
	{ // begin try (depth: 1)
		try
		{ // begin try (depth: 2)
			Il2CppObject* L_4 = (Il2CppObject*)((OperatorObserverBase_2_t2064311169 *)__this)->get_observer_0();
			il2cpp_codegen_memory_barrier();
			NullCheck((Il2CppObject*)L_4);
			InterfaceActionInvoker0::Invoke(0 /* System.Void UniRx.IObserver`1<UniRx.Unit>::OnCompleted() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6), (Il2CppObject*)L_4);
			IL2CPP_LEAVE(0x3E, FINALLY_0037);
		} // end try (depth: 2)
		catch(Il2CppExceptionWrapper& e)
		{
			__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
			goto FINALLY_0037;
		}

FINALLY_0037:
		{ // begin finally (depth: 2)
			NullCheck((OperatorObserverBase_2_t2064311169 *)__this);
			VirtActionInvoker0::Invoke(4 /* System.Void UniRx.Operators.OperatorObserverBase`2<UniRx.IObservable`1<UniRx.Unit>,UniRx.Unit>::Dispose() */, (OperatorObserverBase_2_t2064311169 *)__this);
			IL2CPP_END_FINALLY(55)
		} // end finally (depth: 2)
		IL2CPP_CLEANUP(55)
		{
			IL2CPP_JUMP_TBL(0x3E, IL_003e)
			IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
		}

IL_003e:
		{
			IL2CPP_LEAVE(0x4A, FINALLY_0043);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0043;
	}

FINALLY_0043:
	{ // begin finally (depth: 1)
		Il2CppObject * L_5 = V_0;
		Monitor_Exit_m2088237919(NULL /*static, unused*/, (Il2CppObject *)L_5, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(67)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(67)
	{
		IL2CPP_JUMP_TBL(0x4A, IL_004a)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_004a:
	{
		goto IL_005a;
	}

IL_004f:
	{
		SingleAssignmentDisposable_t2336378823 * L_6 = (SingleAssignmentDisposable_t2336378823 *)__this->get_sourceDisposable_4();
		NullCheck((SingleAssignmentDisposable_t2336378823 *)L_6);
		VirtActionInvoker0::Invoke(4 /* System.Void UniRx.SingleAssignmentDisposable::Dispose() */, (SingleAssignmentDisposable_t2336378823 *)L_6);
	}

IL_005a:
	{
		return;
	}
}
// System.Void UniRx.Operators.MergeObservable`1<System.Object>::.ctor(UniRx.IObservable`1<UniRx.IObservable`1<T>>,System.Boolean)
extern "C"  void MergeObservable_1__ctor_m951789753_gshared (MergeObservable_1_t3637992042 * __this, Il2CppObject* ___sources0, bool ___isRequiredSubscribeOnCurrentThread1, const MethodInfo* method)
{
	{
		bool L_0 = ___isRequiredSubscribeOnCurrentThread1;
		NullCheck((OperatorObservableBase_1_t4196218687 *)__this);
		((  void (*) (OperatorObservableBase_1_t4196218687 *, bool, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((OperatorObservableBase_1_t4196218687 *)__this, (bool)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		Il2CppObject* L_1 = ___sources0;
		__this->set_sources_1(L_1);
		return;
	}
}
// System.Void UniRx.Operators.MergeObservable`1<System.Object>::.ctor(UniRx.IObservable`1<UniRx.IObservable`1<T>>,System.Int32,System.Boolean)
extern "C"  void MergeObservable_1__ctor_m2030410250_gshared (MergeObservable_1_t3637992042 * __this, Il2CppObject* ___sources0, int32_t ___maxConcurrent1, bool ___isRequiredSubscribeOnCurrentThread2, const MethodInfo* method)
{
	{
		bool L_0 = ___isRequiredSubscribeOnCurrentThread2;
		NullCheck((OperatorObservableBase_1_t4196218687 *)__this);
		((  void (*) (OperatorObservableBase_1_t4196218687 *, bool, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((OperatorObservableBase_1_t4196218687 *)__this, (bool)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		Il2CppObject* L_1 = ___sources0;
		__this->set_sources_1(L_1);
		int32_t L_2 = ___maxConcurrent1;
		__this->set_maxConcurrent_2(L_2);
		return;
	}
}
// System.IDisposable UniRx.Operators.MergeObservable`1<System.Object>::SubscribeCore(UniRx.IObserver`1<T>,System.IDisposable)
extern "C"  Il2CppObject * MergeObservable_1_SubscribeCore_m4054478294_gshared (MergeObservable_1_t3637992042 * __this, Il2CppObject* ___observer0, Il2CppObject * ___cancel1, const MethodInfo* method)
{
	{
		int32_t L_0 = (int32_t)__this->get_maxConcurrent_2();
		if ((((int32_t)L_0) <= ((int32_t)0)))
		{
			goto IL_001a;
		}
	}
	{
		Il2CppObject* L_1 = ___observer0;
		Il2CppObject * L_2 = ___cancel1;
		MergeConcurrentObserver_t3534593302 * L_3 = (MergeConcurrentObserver_t3534593302 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		((  void (*) (MergeConcurrentObserver_t3534593302 *, MergeObservable_1_t3637992042 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)(L_3, (MergeObservable_1_t3637992042 *)__this, (Il2CppObject*)L_1, (Il2CppObject *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		NullCheck((MergeConcurrentObserver_t3534593302 *)L_3);
		Il2CppObject * L_4 = ((  Il2CppObject * (*) (MergeConcurrentObserver_t3534593302 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)((MergeConcurrentObserver_t3534593302 *)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		return L_4;
	}

IL_001a:
	{
		Il2CppObject* L_5 = ___observer0;
		Il2CppObject * L_6 = ___cancel1;
		MergeOuterObserver_t1613081930 * L_7 = (MergeOuterObserver_t1613081930 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		((  void (*) (MergeOuterObserver_t1613081930 *, MergeObservable_1_t3637992042 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6)->method)(L_7, (MergeObservable_1_t3637992042 *)__this, (Il2CppObject*)L_5, (Il2CppObject *)L_6, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		NullCheck((MergeOuterObserver_t1613081930 *)L_7);
		Il2CppObject * L_8 = ((  Il2CppObject * (*) (MergeOuterObserver_t1613081930 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7)->method)((MergeOuterObserver_t1613081930 *)L_7, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7));
		return L_8;
	}
}
// System.Void UniRx.Operators.MergeObservable`1<UniRx.Unit>::.ctor(UniRx.IObservable`1<UniRx.IObservable`1<T>>,System.Boolean)
extern "C"  void MergeObservable_1__ctor_m1101124109_gshared (MergeObservable_1_t1064204364 * __this, Il2CppObject* ___sources0, bool ___isRequiredSubscribeOnCurrentThread1, const MethodInfo* method)
{
	{
		bool L_0 = ___isRequiredSubscribeOnCurrentThread1;
		NullCheck((OperatorObservableBase_1_t1622431009 *)__this);
		((  void (*) (OperatorObservableBase_1_t1622431009 *, bool, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((OperatorObservableBase_1_t1622431009 *)__this, (bool)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		Il2CppObject* L_1 = ___sources0;
		__this->set_sources_1(L_1);
		return;
	}
}
// System.Void UniRx.Operators.MergeObservable`1<UniRx.Unit>::.ctor(UniRx.IObservable`1<UniRx.IObservable`1<T>>,System.Int32,System.Boolean)
extern "C"  void MergeObservable_1__ctor_m482326838_gshared (MergeObservable_1_t1064204364 * __this, Il2CppObject* ___sources0, int32_t ___maxConcurrent1, bool ___isRequiredSubscribeOnCurrentThread2, const MethodInfo* method)
{
	{
		bool L_0 = ___isRequiredSubscribeOnCurrentThread2;
		NullCheck((OperatorObservableBase_1_t1622431009 *)__this);
		((  void (*) (OperatorObservableBase_1_t1622431009 *, bool, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((OperatorObservableBase_1_t1622431009 *)__this, (bool)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		Il2CppObject* L_1 = ___sources0;
		__this->set_sources_1(L_1);
		int32_t L_2 = ___maxConcurrent1;
		__this->set_maxConcurrent_2(L_2);
		return;
	}
}
// System.IDisposable UniRx.Operators.MergeObservable`1<UniRx.Unit>::SubscribeCore(UniRx.IObserver`1<T>,System.IDisposable)
extern "C"  Il2CppObject * MergeObservable_1_SubscribeCore_m2966227564_gshared (MergeObservable_1_t1064204364 * __this, Il2CppObject* ___observer0, Il2CppObject * ___cancel1, const MethodInfo* method)
{
	{
		int32_t L_0 = (int32_t)__this->get_maxConcurrent_2();
		if ((((int32_t)L_0) <= ((int32_t)0)))
		{
			goto IL_001a;
		}
	}
	{
		Il2CppObject* L_1 = ___observer0;
		Il2CppObject * L_2 = ___cancel1;
		MergeConcurrentObserver_t960805624 * L_3 = (MergeConcurrentObserver_t960805624 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		((  void (*) (MergeConcurrentObserver_t960805624 *, MergeObservable_1_t1064204364 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)(L_3, (MergeObservable_1_t1064204364 *)__this, (Il2CppObject*)L_1, (Il2CppObject *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		NullCheck((MergeConcurrentObserver_t960805624 *)L_3);
		Il2CppObject * L_4 = ((  Il2CppObject * (*) (MergeConcurrentObserver_t960805624 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)((MergeConcurrentObserver_t960805624 *)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		return L_4;
	}

IL_001a:
	{
		Il2CppObject* L_5 = ___observer0;
		Il2CppObject * L_6 = ___cancel1;
		MergeOuterObserver_t3334261548 * L_7 = (MergeOuterObserver_t3334261548 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		((  void (*) (MergeOuterObserver_t3334261548 *, MergeObservable_1_t1064204364 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6)->method)(L_7, (MergeObservable_1_t1064204364 *)__this, (Il2CppObject*)L_5, (Il2CppObject *)L_6, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		NullCheck((MergeOuterObserver_t3334261548 *)L_7);
		Il2CppObject * L_8 = ((  Il2CppObject * (*) (MergeOuterObserver_t3334261548 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7)->method)((MergeOuterObserver_t3334261548 *)L_7, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7));
		return L_8;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
