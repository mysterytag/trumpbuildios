﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// StreamEgoizm`1/<Listen>c__AnonStorey129<System.Object>
struct U3CListenU3Ec__AnonStorey129_t1735040613;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"

// System.Void StreamEgoizm`1/<Listen>c__AnonStorey129<System.Object>::.ctor()
extern "C"  void U3CListenU3Ec__AnonStorey129__ctor_m467396026_gshared (U3CListenU3Ec__AnonStorey129_t1735040613 * __this, const MethodInfo* method);
#define U3CListenU3Ec__AnonStorey129__ctor_m467396026(__this, method) ((  void (*) (U3CListenU3Ec__AnonStorey129_t1735040613 *, const MethodInfo*))U3CListenU3Ec__AnonStorey129__ctor_m467396026_gshared)(__this, method)
// System.Void StreamEgoizm`1/<Listen>c__AnonStorey129<System.Object>::<>m__1BB(T)
extern "C"  void U3CListenU3Ec__AnonStorey129_U3CU3Em__1BB_m3377804078_gshared (U3CListenU3Ec__AnonStorey129_t1735040613 * __this, Il2CppObject * ____0, const MethodInfo* method);
#define U3CListenU3Ec__AnonStorey129_U3CU3Em__1BB_m3377804078(__this, ____0, method) ((  void (*) (U3CListenU3Ec__AnonStorey129_t1735040613 *, Il2CppObject *, const MethodInfo*))U3CListenU3Ec__AnonStorey129_U3CU3Em__1BB_m3377804078_gshared)(__this, ____0, method)
