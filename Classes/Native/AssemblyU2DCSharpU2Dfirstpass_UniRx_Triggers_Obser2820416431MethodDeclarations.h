﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Triggers.ObservableAnimatorTrigger
struct ObservableAnimatorTrigger_t2820416431;
// UniRx.IObservable`1<System.Int32>
struct IObservable_1_t2606213151;
// UniRx.IObservable`1<UniRx.Unit>
struct IObservable_1_t2317084402;

#include "codegen/il2cpp-codegen.h"

// System.Void UniRx.Triggers.ObservableAnimatorTrigger::.ctor()
extern "C"  void ObservableAnimatorTrigger__ctor_m2897388124 (ObservableAnimatorTrigger_t2820416431 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.Triggers.ObservableAnimatorTrigger::OnAnimatorIK(System.Int32)
extern "C"  void ObservableAnimatorTrigger_OnAnimatorIK_m3733879105 (ObservableAnimatorTrigger_t2820416431 * __this, int32_t ___layerIndex0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UniRx.IObservable`1<System.Int32> UniRx.Triggers.ObservableAnimatorTrigger::OnAnimatorIKAsObservable()
extern "C"  Il2CppObject* ObservableAnimatorTrigger_OnAnimatorIKAsObservable_m221413920 (ObservableAnimatorTrigger_t2820416431 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.Triggers.ObservableAnimatorTrigger::OnAnimatorMove()
extern "C"  void ObservableAnimatorTrigger_OnAnimatorMove_m1617623999 (ObservableAnimatorTrigger_t2820416431 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UniRx.IObservable`1<UniRx.Unit> UniRx.Triggers.ObservableAnimatorTrigger::OnAnimatorMoveAsObservable()
extern "C"  Il2CppObject* ObservableAnimatorTrigger_OnAnimatorMoveAsObservable_m1273579132 (ObservableAnimatorTrigger_t2820416431 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.Triggers.ObservableAnimatorTrigger::RaiseOnCompletedOnDestroy()
extern "C"  void ObservableAnimatorTrigger_RaiseOnCompletedOnDestroy_m3491410837 (ObservableAnimatorTrigger_t2820416431 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
