﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Object
struct Il2CppObject;

#include "mscorlib_System_ValueType4014882752.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Pair`1<System.Object>
struct  Pair_1_t3831097582 
{
public:
	// T UniRx.Pair`1::previous
	Il2CppObject * ___previous_0;
	// T UniRx.Pair`1::current
	Il2CppObject * ___current_1;

public:
	inline static int32_t get_offset_of_previous_0() { return static_cast<int32_t>(offsetof(Pair_1_t3831097582, ___previous_0)); }
	inline Il2CppObject * get_previous_0() const { return ___previous_0; }
	inline Il2CppObject ** get_address_of_previous_0() { return &___previous_0; }
	inline void set_previous_0(Il2CppObject * value)
	{
		___previous_0 = value;
		Il2CppCodeGenWriteBarrier(&___previous_0, value);
	}

	inline static int32_t get_offset_of_current_1() { return static_cast<int32_t>(offsetof(Pair_1_t3831097582, ___current_1)); }
	inline Il2CppObject * get_current_1() const { return ___current_1; }
	inline Il2CppObject ** get_address_of_current_1() { return &___current_1; }
	inline void set_current_1(Il2CppObject * value)
	{
		___current_1 = value;
		Il2CppCodeGenWriteBarrier(&___current_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
