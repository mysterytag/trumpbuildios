﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.ConcatObservable`1/Concat<System.Object>
struct Concat_t120413325;
// UniRx.Operators.ConcatObservable`1<System.Object>
struct ConcatObservable_1_t1571046694;
// UniRx.IObserver`1<System.Object>
struct IObserver_1_t3049105323;
// System.IDisposable
struct IDisposable_t1628921374;
// System.Action
struct Action_t437523947;
// System.Object
struct Il2CppObject;
// System.Exception
struct Exception_t1967233988;

#include "codegen/il2cpp-codegen.h"
#include "System_Core_System_Action437523947.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_Exception1967233988.h"

// System.Void UniRx.Operators.ConcatObservable`1/Concat<System.Object>::.ctor(UniRx.Operators.ConcatObservable`1<T>,UniRx.IObserver`1<T>,System.IDisposable)
extern "C"  void Concat__ctor_m2095294552_gshared (Concat_t120413325 * __this, ConcatObservable_1_t1571046694 * ___parent0, Il2CppObject* ___observer1, Il2CppObject * ___cancel2, const MethodInfo* method);
#define Concat__ctor_m2095294552(__this, ___parent0, ___observer1, ___cancel2, method) ((  void (*) (Concat_t120413325 *, ConcatObservable_1_t1571046694 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))Concat__ctor_m2095294552_gshared)(__this, ___parent0, ___observer1, ___cancel2, method)
// System.IDisposable UniRx.Operators.ConcatObservable`1/Concat<System.Object>::Run()
extern "C"  Il2CppObject * Concat_Run_m969624197_gshared (Concat_t120413325 * __this, const MethodInfo* method);
#define Concat_Run_m969624197(__this, method) ((  Il2CppObject * (*) (Concat_t120413325 *, const MethodInfo*))Concat_Run_m969624197_gshared)(__this, method)
// System.Void UniRx.Operators.ConcatObservable`1/Concat<System.Object>::RecursiveRun(System.Action)
extern "C"  void Concat_RecursiveRun_m211724631_gshared (Concat_t120413325 * __this, Action_t437523947 * ___self0, const MethodInfo* method);
#define Concat_RecursiveRun_m211724631(__this, ___self0, method) ((  void (*) (Concat_t120413325 *, Action_t437523947 *, const MethodInfo*))Concat_RecursiveRun_m211724631_gshared)(__this, ___self0, method)
// System.Void UniRx.Operators.ConcatObservable`1/Concat<System.Object>::OnNext(T)
extern "C"  void Concat_OnNext_m1765770271_gshared (Concat_t120413325 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Concat_OnNext_m1765770271(__this, ___value0, method) ((  void (*) (Concat_t120413325 *, Il2CppObject *, const MethodInfo*))Concat_OnNext_m1765770271_gshared)(__this, ___value0, method)
// System.Void UniRx.Operators.ConcatObservable`1/Concat<System.Object>::OnError(System.Exception)
extern "C"  void Concat_OnError_m1368228142_gshared (Concat_t120413325 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method);
#define Concat_OnError_m1368228142(__this, ___error0, method) ((  void (*) (Concat_t120413325 *, Exception_t1967233988 *, const MethodInfo*))Concat_OnError_m1368228142_gshared)(__this, ___error0, method)
// System.Void UniRx.Operators.ConcatObservable`1/Concat<System.Object>::OnCompleted()
extern "C"  void Concat_OnCompleted_m1285687681_gshared (Concat_t120413325 * __this, const MethodInfo* method);
#define Concat_OnCompleted_m1285687681(__this, method) ((  void (*) (Concat_t120413325 *, const MethodInfo*))Concat_OnCompleted_m1285687681_gshared)(__this, method)
// System.Void UniRx.Operators.ConcatObservable`1/Concat<System.Object>::<Run>m__78()
extern "C"  void Concat_U3CRunU3Em__78_m3422706412_gshared (Concat_t120413325 * __this, const MethodInfo* method);
#define Concat_U3CRunU3Em__78_m3422706412(__this, method) ((  void (*) (Concat_t120413325 *, const MethodInfo*))Concat_U3CRunU3Em__78_m3422706412_gshared)(__this, method)
