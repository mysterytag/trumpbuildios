﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UniRx.AsyncSubject`1<UniRx.Unit>
struct AsyncSubject_1_t3257925142;
// UniRx.Observable/<ToAsync>c__AnonStorey3A
struct U3CToAsyncU3Ec__AnonStorey3A_t2315298076;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Observable/<ToAsync>c__AnonStorey3A/<ToAsync>c__AnonStorey3B
struct  U3CToAsyncU3Ec__AnonStorey3B_t2315298077  : public Il2CppObject
{
public:
	// UniRx.AsyncSubject`1<UniRx.Unit> UniRx.Observable/<ToAsync>c__AnonStorey3A/<ToAsync>c__AnonStorey3B::subject
	AsyncSubject_1_t3257925142 * ___subject_0;
	// UniRx.Observable/<ToAsync>c__AnonStorey3A UniRx.Observable/<ToAsync>c__AnonStorey3A/<ToAsync>c__AnonStorey3B::<>f__ref$58
	U3CToAsyncU3Ec__AnonStorey3A_t2315298076 * ___U3CU3Ef__refU2458_1;

public:
	inline static int32_t get_offset_of_subject_0() { return static_cast<int32_t>(offsetof(U3CToAsyncU3Ec__AnonStorey3B_t2315298077, ___subject_0)); }
	inline AsyncSubject_1_t3257925142 * get_subject_0() const { return ___subject_0; }
	inline AsyncSubject_1_t3257925142 ** get_address_of_subject_0() { return &___subject_0; }
	inline void set_subject_0(AsyncSubject_1_t3257925142 * value)
	{
		___subject_0 = value;
		Il2CppCodeGenWriteBarrier(&___subject_0, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__refU2458_1() { return static_cast<int32_t>(offsetof(U3CToAsyncU3Ec__AnonStorey3B_t2315298077, ___U3CU3Ef__refU2458_1)); }
	inline U3CToAsyncU3Ec__AnonStorey3A_t2315298076 * get_U3CU3Ef__refU2458_1() const { return ___U3CU3Ef__refU2458_1; }
	inline U3CToAsyncU3Ec__AnonStorey3A_t2315298076 ** get_address_of_U3CU3Ef__refU2458_1() { return &___U3CU3Ef__refU2458_1; }
	inline void set_U3CU3Ef__refU2458_1(U3CToAsyncU3Ec__AnonStorey3A_t2315298076 * value)
	{
		___U3CU3Ef__refU2458_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__refU2458_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
