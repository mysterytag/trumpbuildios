﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Object
struct Il2CppObject;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CellReactiveApi/<NotNull>c__AnonStorey102`1<System.Object>
struct  U3CNotNullU3Ec__AnonStorey102_1_t291767342  : public Il2CppObject
{
public:
	// T CellReactiveApi/<NotNull>c__AnonStorey102`1::orphan
	Il2CppObject * ___orphan_0;

public:
	inline static int32_t get_offset_of_orphan_0() { return static_cast<int32_t>(offsetof(U3CNotNullU3Ec__AnonStorey102_1_t291767342, ___orphan_0)); }
	inline Il2CppObject * get_orphan_0() const { return ___orphan_0; }
	inline Il2CppObject ** get_address_of_orphan_0() { return &___orphan_0; }
	inline void set_orphan_0(Il2CppObject * value)
	{
		___orphan_0 = value;
		Il2CppCodeGenWriteBarrier(&___orphan_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
