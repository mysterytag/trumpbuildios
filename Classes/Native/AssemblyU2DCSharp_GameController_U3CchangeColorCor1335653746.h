﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Object
struct Il2CppObject;
// GameController
struct GameController_t2782302542;

#include "mscorlib_System_Object837106420.h"
#include "UnityEngine_UnityEngine_Color1588175760.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameController/<changeColorCoroutine>c__IteratorF
struct  U3CchangeColorCoroutineU3Ec__IteratorF_t1335653746  : public Il2CppObject
{
public:
	// System.Single GameController/<changeColorCoroutine>c__IteratorF::<processTime>__0
	float ___U3CprocessTimeU3E__0_0;
	// System.Single GameController/<changeColorCoroutine>c__IteratorF::<startTime>__1
	float ___U3CstartTimeU3E__1_1;
	// UnityEngine.Color GameController/<changeColorCoroutine>c__IteratorF::<startColor>__2
	Color_t1588175760  ___U3CstartColorU3E__2_2;
	// System.Single GameController/<changeColorCoroutine>c__IteratorF::<deltaTime>__3
	float ___U3CdeltaTimeU3E__3_3;
	// System.Single GameController/<changeColorCoroutine>c__IteratorF::<ratio>__4
	float ___U3CratioU3E__4_4;
	// UnityEngine.Color GameController/<changeColorCoroutine>c__IteratorF::color
	Color_t1588175760  ___color_5;
	// System.Int32 GameController/<changeColorCoroutine>c__IteratorF::$PC
	int32_t ___U24PC_6;
	// System.Object GameController/<changeColorCoroutine>c__IteratorF::$current
	Il2CppObject * ___U24current_7;
	// UnityEngine.Color GameController/<changeColorCoroutine>c__IteratorF::<$>color
	Color_t1588175760  ___U3CU24U3Ecolor_8;
	// GameController GameController/<changeColorCoroutine>c__IteratorF::<>f__this
	GameController_t2782302542 * ___U3CU3Ef__this_9;

public:
	inline static int32_t get_offset_of_U3CprocessTimeU3E__0_0() { return static_cast<int32_t>(offsetof(U3CchangeColorCoroutineU3Ec__IteratorF_t1335653746, ___U3CprocessTimeU3E__0_0)); }
	inline float get_U3CprocessTimeU3E__0_0() const { return ___U3CprocessTimeU3E__0_0; }
	inline float* get_address_of_U3CprocessTimeU3E__0_0() { return &___U3CprocessTimeU3E__0_0; }
	inline void set_U3CprocessTimeU3E__0_0(float value)
	{
		___U3CprocessTimeU3E__0_0 = value;
	}

	inline static int32_t get_offset_of_U3CstartTimeU3E__1_1() { return static_cast<int32_t>(offsetof(U3CchangeColorCoroutineU3Ec__IteratorF_t1335653746, ___U3CstartTimeU3E__1_1)); }
	inline float get_U3CstartTimeU3E__1_1() const { return ___U3CstartTimeU3E__1_1; }
	inline float* get_address_of_U3CstartTimeU3E__1_1() { return &___U3CstartTimeU3E__1_1; }
	inline void set_U3CstartTimeU3E__1_1(float value)
	{
		___U3CstartTimeU3E__1_1 = value;
	}

	inline static int32_t get_offset_of_U3CstartColorU3E__2_2() { return static_cast<int32_t>(offsetof(U3CchangeColorCoroutineU3Ec__IteratorF_t1335653746, ___U3CstartColorU3E__2_2)); }
	inline Color_t1588175760  get_U3CstartColorU3E__2_2() const { return ___U3CstartColorU3E__2_2; }
	inline Color_t1588175760 * get_address_of_U3CstartColorU3E__2_2() { return &___U3CstartColorU3E__2_2; }
	inline void set_U3CstartColorU3E__2_2(Color_t1588175760  value)
	{
		___U3CstartColorU3E__2_2 = value;
	}

	inline static int32_t get_offset_of_U3CdeltaTimeU3E__3_3() { return static_cast<int32_t>(offsetof(U3CchangeColorCoroutineU3Ec__IteratorF_t1335653746, ___U3CdeltaTimeU3E__3_3)); }
	inline float get_U3CdeltaTimeU3E__3_3() const { return ___U3CdeltaTimeU3E__3_3; }
	inline float* get_address_of_U3CdeltaTimeU3E__3_3() { return &___U3CdeltaTimeU3E__3_3; }
	inline void set_U3CdeltaTimeU3E__3_3(float value)
	{
		___U3CdeltaTimeU3E__3_3 = value;
	}

	inline static int32_t get_offset_of_U3CratioU3E__4_4() { return static_cast<int32_t>(offsetof(U3CchangeColorCoroutineU3Ec__IteratorF_t1335653746, ___U3CratioU3E__4_4)); }
	inline float get_U3CratioU3E__4_4() const { return ___U3CratioU3E__4_4; }
	inline float* get_address_of_U3CratioU3E__4_4() { return &___U3CratioU3E__4_4; }
	inline void set_U3CratioU3E__4_4(float value)
	{
		___U3CratioU3E__4_4 = value;
	}

	inline static int32_t get_offset_of_color_5() { return static_cast<int32_t>(offsetof(U3CchangeColorCoroutineU3Ec__IteratorF_t1335653746, ___color_5)); }
	inline Color_t1588175760  get_color_5() const { return ___color_5; }
	inline Color_t1588175760 * get_address_of_color_5() { return &___color_5; }
	inline void set_color_5(Color_t1588175760  value)
	{
		___color_5 = value;
	}

	inline static int32_t get_offset_of_U24PC_6() { return static_cast<int32_t>(offsetof(U3CchangeColorCoroutineU3Ec__IteratorF_t1335653746, ___U24PC_6)); }
	inline int32_t get_U24PC_6() const { return ___U24PC_6; }
	inline int32_t* get_address_of_U24PC_6() { return &___U24PC_6; }
	inline void set_U24PC_6(int32_t value)
	{
		___U24PC_6 = value;
	}

	inline static int32_t get_offset_of_U24current_7() { return static_cast<int32_t>(offsetof(U3CchangeColorCoroutineU3Ec__IteratorF_t1335653746, ___U24current_7)); }
	inline Il2CppObject * get_U24current_7() const { return ___U24current_7; }
	inline Il2CppObject ** get_address_of_U24current_7() { return &___U24current_7; }
	inline void set_U24current_7(Il2CppObject * value)
	{
		___U24current_7 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_7, value);
	}

	inline static int32_t get_offset_of_U3CU24U3Ecolor_8() { return static_cast<int32_t>(offsetof(U3CchangeColorCoroutineU3Ec__IteratorF_t1335653746, ___U3CU24U3Ecolor_8)); }
	inline Color_t1588175760  get_U3CU24U3Ecolor_8() const { return ___U3CU24U3Ecolor_8; }
	inline Color_t1588175760 * get_address_of_U3CU24U3Ecolor_8() { return &___U3CU24U3Ecolor_8; }
	inline void set_U3CU24U3Ecolor_8(Color_t1588175760  value)
	{
		___U3CU24U3Ecolor_8 = value;
	}

	inline static int32_t get_offset_of_U3CU3Ef__this_9() { return static_cast<int32_t>(offsetof(U3CchangeColorCoroutineU3Ec__IteratorF_t1335653746, ___U3CU3Ef__this_9)); }
	inline GameController_t2782302542 * get_U3CU3Ef__this_9() const { return ___U3CU3Ef__this_9; }
	inline GameController_t2782302542 ** get_address_of_U3CU3Ef__this_9() { return &___U3CU3Ef__this_9; }
	inline void set_U3CU3Ef__this_9(GameController_t2782302542 * value)
	{
		___U3CU3Ef__this_9 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__this_9, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
