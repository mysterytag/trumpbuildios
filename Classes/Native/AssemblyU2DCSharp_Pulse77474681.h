﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Action`1<System.Single>
struct Action_1_t1106661726;

#include "AssemblyU2DCSharp_FiniteTimeProc2933053874.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Pulse
struct  Pulse_t77474681  : public FiniteTimeProc_t2933053874
{
public:
	// System.Single Pulse::from
	float ___from_2;
	// System.Single Pulse::to
	float ___to_3;
	// System.Single Pulse::wave
	float ___wave_4;
	// System.Action`1<System.Single> Pulse::action
	Action_1_t1106661726 * ___action_5;

public:
	inline static int32_t get_offset_of_from_2() { return static_cast<int32_t>(offsetof(Pulse_t77474681, ___from_2)); }
	inline float get_from_2() const { return ___from_2; }
	inline float* get_address_of_from_2() { return &___from_2; }
	inline void set_from_2(float value)
	{
		___from_2 = value;
	}

	inline static int32_t get_offset_of_to_3() { return static_cast<int32_t>(offsetof(Pulse_t77474681, ___to_3)); }
	inline float get_to_3() const { return ___to_3; }
	inline float* get_address_of_to_3() { return &___to_3; }
	inline void set_to_3(float value)
	{
		___to_3 = value;
	}

	inline static int32_t get_offset_of_wave_4() { return static_cast<int32_t>(offsetof(Pulse_t77474681, ___wave_4)); }
	inline float get_wave_4() const { return ___wave_4; }
	inline float* get_address_of_wave_4() { return &___wave_4; }
	inline void set_wave_4(float value)
	{
		___wave_4 = value;
	}

	inline static int32_t get_offset_of_action_5() { return static_cast<int32_t>(offsetof(Pulse_t77474681, ___action_5)); }
	inline Action_1_t1106661726 * get_action_5() const { return ___action_5; }
	inline Action_1_t1106661726 ** get_address_of_action_5() { return &___action_5; }
	inline void set_action_5(Action_1_t1106661726 * value)
	{
		___action_5 = value;
		Il2CppCodeGenWriteBarrier(&___action_5, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
