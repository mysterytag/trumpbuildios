﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SubstanceBase`2/<Affect>c__AnonStorey143<System.Double,System.Double>
struct U3CAffectU3Ec__AnonStorey143_t2252021777;

#include "codegen/il2cpp-codegen.h"

// System.Void SubstanceBase`2/<Affect>c__AnonStorey143<System.Double,System.Double>::.ctor()
extern "C"  void U3CAffectU3Ec__AnonStorey143__ctor_m1794887191_gshared (U3CAffectU3Ec__AnonStorey143_t2252021777 * __this, const MethodInfo* method);
#define U3CAffectU3Ec__AnonStorey143__ctor_m1794887191(__this, method) ((  void (*) (U3CAffectU3Ec__AnonStorey143_t2252021777 *, const MethodInfo*))U3CAffectU3Ec__AnonStorey143__ctor_m1794887191_gshared)(__this, method)
// System.Void SubstanceBase`2/<Affect>c__AnonStorey143<System.Double,System.Double>::<>m__1D9(InfT)
extern "C"  void U3CAffectU3Ec__AnonStorey143_U3CU3Em__1D9_m1383556053_gshared (U3CAffectU3Ec__AnonStorey143_t2252021777 * __this, double ___val0, const MethodInfo* method);
#define U3CAffectU3Ec__AnonStorey143_U3CU3Em__1D9_m1383556053(__this, ___val0, method) ((  void (*) (U3CAffectU3Ec__AnonStorey143_t2252021777 *, double, const MethodInfo*))U3CAffectU3Ec__AnonStorey143_U3CU3Em__1D9_m1383556053_gshared)(__this, ___val0, method)
