﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.PlayFabClientAPI/GetUserStatisticsRequestCallback
struct GetUserStatisticsRequestCallback_t2864275248;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;
// PlayFab.ClientModels.GetUserStatisticsRequest
struct GetUserStatisticsRequest_t813294043;
// System.IAsyncResult
struct IAsyncResult_t537683269;
// System.AsyncCallback
struct AsyncCallback_t1363551830;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_IntPtr676692020.h"
#include "mscorlib_System_String968488902.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_GetUserStati813294043.h"
#include "mscorlib_System_AsyncCallback1363551830.h"

// System.Void PlayFab.PlayFabClientAPI/GetUserStatisticsRequestCallback::.ctor(System.Object,System.IntPtr)
extern "C"  void GetUserStatisticsRequestCallback__ctor_m10946591 (GetUserStatisticsRequestCallback_t2864275248 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/GetUserStatisticsRequestCallback::Invoke(System.String,System.Int32,PlayFab.ClientModels.GetUserStatisticsRequest,System.Object)
extern "C"  void GetUserStatisticsRequestCallback_Invoke_m3672100063 (GetUserStatisticsRequestCallback_t2864275248 * __this, String_t* ___urlPath0, int32_t ___callId1, GetUserStatisticsRequest_t813294043 * ___request2, Il2CppObject * ___customData3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
extern "C" void pinvoke_delegate_wrapper_GetUserStatisticsRequestCallback_t2864275248(Il2CppObject* delegate, String_t* ___urlPath0, int32_t ___callId1, GetUserStatisticsRequest_t813294043 * ___request2, Il2CppObject * ___customData3);
// System.IAsyncResult PlayFab.PlayFabClientAPI/GetUserStatisticsRequestCallback::BeginInvoke(System.String,System.Int32,PlayFab.ClientModels.GetUserStatisticsRequest,System.Object,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * GetUserStatisticsRequestCallback_BeginInvoke_m1302037062 (GetUserStatisticsRequestCallback_t2864275248 * __this, String_t* ___urlPath0, int32_t ___callId1, GetUserStatisticsRequest_t813294043 * ___request2, Il2CppObject * ___customData3, AsyncCallback_t1363551830 * ___callback4, Il2CppObject * ___object5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/GetUserStatisticsRequestCallback::EndInvoke(System.IAsyncResult)
extern "C"  void GetUserStatisticsRequestCallback_EndInvoke_m4207501743 (GetUserStatisticsRequestCallback_t2864275248 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
