﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.PlayFabClientAPI/<RegisterPlayFabUser>c__AnonStorey6B
struct U3CRegisterPlayFabUserU3Ec__AnonStorey6B_t1963450804;
// PlayFab.CallRequestContainer
struct CallRequestContainer_t432318609;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharpU2Dfirstpass_PlayFab_CallRequestCo432318609.h"

// System.Void PlayFab.PlayFabClientAPI/<RegisterPlayFabUser>c__AnonStorey6B::.ctor()
extern "C"  void U3CRegisterPlayFabUserU3Ec__AnonStorey6B__ctor_m1317175327 (U3CRegisterPlayFabUserU3Ec__AnonStorey6B_t1963450804 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/<RegisterPlayFabUser>c__AnonStorey6B::<>m__58(PlayFab.CallRequestContainer)
extern "C"  void U3CRegisterPlayFabUserU3Ec__AnonStorey6B_U3CU3Em__58_m1165415968 (U3CRegisterPlayFabUserU3Ec__AnonStorey6B_t1963450804 * __this, CallRequestContainer_t432318609 * ___requestContainer0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
