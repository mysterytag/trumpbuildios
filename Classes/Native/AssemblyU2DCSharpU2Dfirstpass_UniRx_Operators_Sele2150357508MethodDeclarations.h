﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.SelectManyObservable`3/SelectManyEnumerableObserver<System.Object,System.Int64,System.Object>
struct SelectManyEnumerableObserver_t2150357508;
// UniRx.Operators.SelectManyObservable`3<System.Object,System.Int64,System.Object>
struct SelectManyObservable_3_t744723289;
// UniRx.IObserver`1<System.Object>
struct IObserver_1_t3049105323;
// System.IDisposable
struct IDisposable_t1628921374;
// System.Object
struct Il2CppObject;
// System.Exception
struct Exception_t1967233988;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_Exception1967233988.h"

// System.Void UniRx.Operators.SelectManyObservable`3/SelectManyEnumerableObserver<System.Object,System.Int64,System.Object>::.ctor(UniRx.Operators.SelectManyObservable`3<TSource,TCollection,TResult>,UniRx.IObserver`1<TResult>,System.IDisposable)
extern "C"  void SelectManyEnumerableObserver__ctor_m3392953278_gshared (SelectManyEnumerableObserver_t2150357508 * __this, SelectManyObservable_3_t744723289 * ___parent0, Il2CppObject* ___observer1, Il2CppObject * ___cancel2, const MethodInfo* method);
#define SelectManyEnumerableObserver__ctor_m3392953278(__this, ___parent0, ___observer1, ___cancel2, method) ((  void (*) (SelectManyEnumerableObserver_t2150357508 *, SelectManyObservable_3_t744723289 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))SelectManyEnumerableObserver__ctor_m3392953278_gshared)(__this, ___parent0, ___observer1, ___cancel2, method)
// System.IDisposable UniRx.Operators.SelectManyObservable`3/SelectManyEnumerableObserver<System.Object,System.Int64,System.Object>::Run()
extern "C"  Il2CppObject * SelectManyEnumerableObserver_Run_m295512647_gshared (SelectManyEnumerableObserver_t2150357508 * __this, const MethodInfo* method);
#define SelectManyEnumerableObserver_Run_m295512647(__this, method) ((  Il2CppObject * (*) (SelectManyEnumerableObserver_t2150357508 *, const MethodInfo*))SelectManyEnumerableObserver_Run_m295512647_gshared)(__this, method)
// System.Void UniRx.Operators.SelectManyObservable`3/SelectManyEnumerableObserver<System.Object,System.Int64,System.Object>::OnNext(TSource)
extern "C"  void SelectManyEnumerableObserver_OnNext_m216259888_gshared (SelectManyEnumerableObserver_t2150357508 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define SelectManyEnumerableObserver_OnNext_m216259888(__this, ___value0, method) ((  void (*) (SelectManyEnumerableObserver_t2150357508 *, Il2CppObject *, const MethodInfo*))SelectManyEnumerableObserver_OnNext_m216259888_gshared)(__this, ___value0, method)
// System.Void UniRx.Operators.SelectManyObservable`3/SelectManyEnumerableObserver<System.Object,System.Int64,System.Object>::OnError(System.Exception)
extern "C"  void SelectManyEnumerableObserver_OnError_m51241114_gshared (SelectManyEnumerableObserver_t2150357508 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method);
#define SelectManyEnumerableObserver_OnError_m51241114(__this, ___error0, method) ((  void (*) (SelectManyEnumerableObserver_t2150357508 *, Exception_t1967233988 *, const MethodInfo*))SelectManyEnumerableObserver_OnError_m51241114_gshared)(__this, ___error0, method)
// System.Void UniRx.Operators.SelectManyObservable`3/SelectManyEnumerableObserver<System.Object,System.Int64,System.Object>::OnCompleted()
extern "C"  void SelectManyEnumerableObserver_OnCompleted_m3078957805_gshared (SelectManyEnumerableObserver_t2150357508 * __this, const MethodInfo* method);
#define SelectManyEnumerableObserver_OnCompleted_m3078957805(__this, method) ((  void (*) (SelectManyEnumerableObserver_t2150357508 *, const MethodInfo*))SelectManyEnumerableObserver_OnCompleted_m3078957805_gshared)(__this, method)
