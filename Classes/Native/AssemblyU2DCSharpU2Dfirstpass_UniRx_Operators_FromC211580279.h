﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Func`3<UniRx.IObserver`1<System.Double>,UniRx.CancellationToken,System.Collections.IEnumerator>
struct Func_3_t1280587413;

#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Oper3893628881.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Operators.FromCoroutine`1<System.Double>
struct  FromCoroutine_1_t211580279  : public OperatorObservableBase_1_t3893628881
{
public:
	// System.Func`3<UniRx.IObserver`1<T>,UniRx.CancellationToken,System.Collections.IEnumerator> UniRx.Operators.FromCoroutine`1::coroutine
	Func_3_t1280587413 * ___coroutine_1;

public:
	inline static int32_t get_offset_of_coroutine_1() { return static_cast<int32_t>(offsetof(FromCoroutine_1_t211580279, ___coroutine_1)); }
	inline Func_3_t1280587413 * get_coroutine_1() const { return ___coroutine_1; }
	inline Func_3_t1280587413 ** get_address_of_coroutine_1() { return &___coroutine_1; }
	inline void set_coroutine_1(Func_3_t1280587413 * value)
	{
		___coroutine_1 = value;
		Il2CppCodeGenWriteBarrier(&___coroutine_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
