﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// AnonymousCell`1/<Listen>c__AnonStoreyF9<System.Double>
struct U3CListenU3Ec__AnonStoreyF9_t2251577170;

#include "codegen/il2cpp-codegen.h"

// System.Void AnonymousCell`1/<Listen>c__AnonStoreyF9<System.Double>::.ctor()
extern "C"  void U3CListenU3Ec__AnonStoreyF9__ctor_m2451335361_gshared (U3CListenU3Ec__AnonStoreyF9_t2251577170 * __this, const MethodInfo* method);
#define U3CListenU3Ec__AnonStoreyF9__ctor_m2451335361(__this, method) ((  void (*) (U3CListenU3Ec__AnonStoreyF9_t2251577170 *, const MethodInfo*))U3CListenU3Ec__AnonStoreyF9__ctor_m2451335361_gshared)(__this, method)
// System.Void AnonymousCell`1/<Listen>c__AnonStoreyF9<System.Double>::<>m__16F(T)
extern "C"  void U3CListenU3Ec__AnonStoreyF9_U3CU3Em__16F_m1431802405_gshared (U3CListenU3Ec__AnonStoreyF9_t2251577170 * __this, double ____0, const MethodInfo* method);
#define U3CListenU3Ec__AnonStoreyF9_U3CU3Em__16F_m1431802405(__this, ____0, method) ((  void (*) (U3CListenU3Ec__AnonStoreyF9_t2251577170 *, double, const MethodInfo*))U3CListenU3Ec__AnonStoreyF9_U3CU3Em__16F_m1431802405_gshared)(__this, ____0, method)
