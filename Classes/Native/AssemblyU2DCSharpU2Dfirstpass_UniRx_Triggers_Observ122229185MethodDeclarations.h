﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Triggers.ObservablePointerExitTrigger
struct ObservablePointerExitTrigger_t122229185;
// UnityEngine.EventSystems.PointerEventData
struct PointerEventData_t3205101634;
// UniRx.IObservable`1<UnityEngine.EventSystems.PointerEventData>
struct IObservable_1_t2963899998;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_PointerEve3205101634.h"

// System.Void UniRx.Triggers.ObservablePointerExitTrigger::.ctor()
extern "C"  void ObservablePointerExitTrigger__ctor_m3299324188 (ObservablePointerExitTrigger_t122229185 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.Triggers.ObservablePointerExitTrigger::UnityEngine.EventSystems.IPointerExitHandler.OnPointerExit(UnityEngine.EventSystems.PointerEventData)
extern "C"  void ObservablePointerExitTrigger_UnityEngine_EventSystems_IPointerExitHandler_OnPointerExit_m3728413533 (ObservablePointerExitTrigger_t122229185 * __this, PointerEventData_t3205101634 * ___eventData0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UniRx.IObservable`1<UnityEngine.EventSystems.PointerEventData> UniRx.Triggers.ObservablePointerExitTrigger::OnPointerExitAsObservable()
extern "C"  Il2CppObject* ObservablePointerExitTrigger_OnPointerExitAsObservable_m2746581807 (ObservablePointerExitTrigger_t122229185 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.Triggers.ObservablePointerExitTrigger::RaiseOnCompletedOnDestroy()
extern "C"  void ObservablePointerExitTrigger_RaiseOnCompletedOnDestroy_m733274709 (ObservablePointerExitTrigger_t122229185 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
