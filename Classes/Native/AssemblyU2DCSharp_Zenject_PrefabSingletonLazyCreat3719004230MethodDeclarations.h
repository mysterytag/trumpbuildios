﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Zenject.PrefabSingletonLazyCreator
struct PrefabSingletonLazyCreator_t3719004230;
// Zenject.DiContainer
struct DiContainer_t2383114449;
// Zenject.PrefabSingletonProviderMap
struct PrefabSingletonProviderMap_t2514131929;
// Zenject.PrefabSingletonId
struct PrefabSingletonId_t3643845047;
// UnityEngine.GameObject
struct GameObject_t4012695102;
// System.Collections.Generic.IEnumerable`1<System.Type>
struct IEnumerable_1_t1356416995;
// System.Type
struct Type_t;
// System.Object
struct Il2CppObject;
// Zenject.InjectContext
struct InjectContext_t3456483891;
// UnityEngine.Component
struct Component_t2126946602;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Zenject_DiContainer2383114449.h"
#include "AssemblyU2DCSharp_Zenject_PrefabSingletonProviderM2514131929.h"
#include "AssemblyU2DCSharp_Zenject_PrefabSingletonId3643845047.h"
#include "mscorlib_System_Type2779229935.h"
#include "AssemblyU2DCSharp_Zenject_InjectContext3456483891.h"
#include "UnityEngine_UnityEngine_Component2126946602.h"

// System.Void Zenject.PrefabSingletonLazyCreator::.ctor(Zenject.DiContainer,Zenject.PrefabSingletonProviderMap,Zenject.PrefabSingletonId)
extern "C"  void PrefabSingletonLazyCreator__ctor_m1351708866 (PrefabSingletonLazyCreator_t3719004230 * __this, DiContainer_t2383114449 * ___container0, PrefabSingletonProviderMap_t2514131929 * ___owner1, PrefabSingletonId_t3643845047 * ___id2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject Zenject.PrefabSingletonLazyCreator::get_Prefab()
extern "C"  GameObject_t4012695102 * PrefabSingletonLazyCreator_get_Prefab_m4164200827 (PrefabSingletonLazyCreator_t3719004230 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject Zenject.PrefabSingletonLazyCreator::get_RootObject()
extern "C"  GameObject_t4012695102 * PrefabSingletonLazyCreator_get_RootObject_m806752376 (PrefabSingletonLazyCreator_t3719004230 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Zenject.PrefabSingletonLazyCreator::IncRefCount()
extern "C"  void PrefabSingletonLazyCreator_IncRefCount_m800916291 (PrefabSingletonLazyCreator_t3719004230 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Zenject.PrefabSingletonLazyCreator::DecRefCount()
extern "C"  void PrefabSingletonLazyCreator_DecRefCount_m1106186855 (PrefabSingletonLazyCreator_t3719004230 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.IEnumerable`1<System.Type> Zenject.PrefabSingletonLazyCreator::GetAllComponentTypes()
extern "C"  Il2CppObject* PrefabSingletonLazyCreator_GetAllComponentTypes_m2672545253 (PrefabSingletonLazyCreator_t3719004230 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Zenject.PrefabSingletonLazyCreator::ContainsComponent(System.Type)
extern "C"  bool PrefabSingletonLazyCreator_ContainsComponent_m822529830 (PrefabSingletonLazyCreator_t3719004230 * __this, Type_t * ___type0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Zenject.PrefabSingletonLazyCreator::GetComponent(System.Type,Zenject.InjectContext)
extern "C"  Il2CppObject * PrefabSingletonLazyCreator_GetComponent_m146062513 (PrefabSingletonLazyCreator_t3719004230 * __this, Type_t * ___componentType0, InjectContext_t3456483891 * ___context1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Zenject.PrefabSingletonLazyCreator::<GetAllComponentTypes>m__305(UnityEngine.Component)
extern "C"  bool PrefabSingletonLazyCreator_U3CGetAllComponentTypesU3Em__305_m33094553 (Il2CppObject * __this /* static, unused */, Component_t2126946602 * ___x0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type Zenject.PrefabSingletonLazyCreator::<GetAllComponentTypes>m__306(UnityEngine.Component)
extern "C"  Type_t * PrefabSingletonLazyCreator_U3CGetAllComponentTypesU3Em__306_m1903566674 (Il2CppObject * __this /* static, unused */, Component_t2126946602 * ___x0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
