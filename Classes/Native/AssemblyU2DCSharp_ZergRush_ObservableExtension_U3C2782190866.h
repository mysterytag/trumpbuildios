﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.RectTransform
struct RectTransform_t3317474837;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ZergRush.ObservableExtension/<EveryTapOutsideRectTransform>c__AnonStorey11B
struct  U3CEveryTapOutsideRectTransformU3Ec__AnonStorey11B_t2782190866  : public Il2CppObject
{
public:
	// UnityEngine.RectTransform ZergRush.ObservableExtension/<EveryTapOutsideRectTransform>c__AnonStorey11B::rectTransform
	RectTransform_t3317474837 * ___rectTransform_0;

public:
	inline static int32_t get_offset_of_rectTransform_0() { return static_cast<int32_t>(offsetof(U3CEveryTapOutsideRectTransformU3Ec__AnonStorey11B_t2782190866, ___rectTransform_0)); }
	inline RectTransform_t3317474837 * get_rectTransform_0() const { return ___rectTransform_0; }
	inline RectTransform_t3317474837 ** get_address_of_rectTransform_0() { return &___rectTransform_0; }
	inline void set_rectTransform_0(RectTransform_t3317474837 * value)
	{
		___rectTransform_0 = value;
		Il2CppCodeGenWriteBarrier(&___rectTransform_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
