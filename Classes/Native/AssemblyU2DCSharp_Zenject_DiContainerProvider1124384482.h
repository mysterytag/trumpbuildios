﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Zenject.DiContainer
struct DiContainer_t2383114449;

#include "AssemblyU2DCSharp_Zenject_ProviderBase1627494391.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.DiContainerProvider
struct  DiContainerProvider_t1124384482  : public ProviderBase_t1627494391
{
public:
	// Zenject.DiContainer Zenject.DiContainerProvider::_container
	DiContainer_t2383114449 * ____container_2;

public:
	inline static int32_t get_offset_of__container_2() { return static_cast<int32_t>(offsetof(DiContainerProvider_t1124384482, ____container_2)); }
	inline DiContainer_t2383114449 * get__container_2() const { return ____container_2; }
	inline DiContainer_t2383114449 ** get_address_of__container_2() { return &____container_2; }
	inline void set__container_2(DiContainer_t2383114449 * value)
	{
		____container_2 = value;
		Il2CppCodeGenWriteBarrier(&____container_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
