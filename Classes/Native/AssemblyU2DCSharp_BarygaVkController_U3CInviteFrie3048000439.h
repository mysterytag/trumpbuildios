﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// FriendInfo
struct FriendInfo_t236470412;
// BarygaVkController
struct BarygaVkController_t3617163921;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BarygaVkController/<InviteFriend>c__AnonStoreyE0
struct  U3CInviteFriendU3Ec__AnonStoreyE0_t3048000439  : public Il2CppObject
{
public:
	// FriendInfo BarygaVkController/<InviteFriend>c__AnonStoreyE0::friend
	FriendInfo_t236470412 * ___friend_0;
	// BarygaVkController BarygaVkController/<InviteFriend>c__AnonStoreyE0::<>f__this
	BarygaVkController_t3617163921 * ___U3CU3Ef__this_1;

public:
	inline static int32_t get_offset_of_friend_0() { return static_cast<int32_t>(offsetof(U3CInviteFriendU3Ec__AnonStoreyE0_t3048000439, ___friend_0)); }
	inline FriendInfo_t236470412 * get_friend_0() const { return ___friend_0; }
	inline FriendInfo_t236470412 ** get_address_of_friend_0() { return &___friend_0; }
	inline void set_friend_0(FriendInfo_t236470412 * value)
	{
		___friend_0 = value;
		Il2CppCodeGenWriteBarrier(&___friend_0, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__this_1() { return static_cast<int32_t>(offsetof(U3CInviteFriendU3Ec__AnonStoreyE0_t3048000439, ___U3CU3Ef__this_1)); }
	inline BarygaVkController_t3617163921 * get_U3CU3Ef__this_1() const { return ___U3CU3Ef__this_1; }
	inline BarygaVkController_t3617163921 ** get_address_of_U3CU3Ef__this_1() { return &___U3CU3Ef__this_1; }
	inline void set_U3CU3Ef__this_1(BarygaVkController_t3617163921 * value)
	{
		___U3CU3Ef__this_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__this_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
