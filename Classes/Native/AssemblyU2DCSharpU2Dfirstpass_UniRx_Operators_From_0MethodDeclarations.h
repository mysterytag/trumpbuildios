﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.FromEventObservable_`1/FromEvent<System.Object>
struct FromEvent_t2990734058;
// UniRx.Operators.FromEventObservable_`1<System.Object>
struct FromEventObservable__1_t2635181811;
// UniRx.IObserver`1<System.Object>
struct IObserver_1_t3049105323;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"

// System.Void UniRx.Operators.FromEventObservable_`1/FromEvent<System.Object>::.ctor(UniRx.Operators.FromEventObservable_`1<T>,UniRx.IObserver`1<T>)
extern "C"  void FromEvent__ctor_m2649012072_gshared (FromEvent_t2990734058 * __this, FromEventObservable__1_t2635181811 * ___parent0, Il2CppObject* ___observer1, const MethodInfo* method);
#define FromEvent__ctor_m2649012072(__this, ___parent0, ___observer1, method) ((  void (*) (FromEvent_t2990734058 *, FromEventObservable__1_t2635181811 *, Il2CppObject*, const MethodInfo*))FromEvent__ctor_m2649012072_gshared)(__this, ___parent0, ___observer1, method)
// System.Boolean UniRx.Operators.FromEventObservable_`1/FromEvent<System.Object>::Register()
extern "C"  bool FromEvent_Register_m736247547_gshared (FromEvent_t2990734058 * __this, const MethodInfo* method);
#define FromEvent_Register_m736247547(__this, method) ((  bool (*) (FromEvent_t2990734058 *, const MethodInfo*))FromEvent_Register_m736247547_gshared)(__this, method)
// System.Void UniRx.Operators.FromEventObservable_`1/FromEvent<System.Object>::OnNext(T)
extern "C"  void FromEvent_OnNext_m1374141496_gshared (FromEvent_t2990734058 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define FromEvent_OnNext_m1374141496(__this, ___value0, method) ((  void (*) (FromEvent_t2990734058 *, Il2CppObject *, const MethodInfo*))FromEvent_OnNext_m1374141496_gshared)(__this, ___value0, method)
// System.Void UniRx.Operators.FromEventObservable_`1/FromEvent<System.Object>::Dispose()
extern "C"  void FromEvent_Dispose_m2736688589_gshared (FromEvent_t2990734058 * __this, const MethodInfo* method);
#define FromEvent_Dispose_m2736688589(__this, method) ((  void (*) (FromEvent_t2990734058 *, const MethodInfo*))FromEvent_Dispose_m2736688589_gshared)(__this, method)
