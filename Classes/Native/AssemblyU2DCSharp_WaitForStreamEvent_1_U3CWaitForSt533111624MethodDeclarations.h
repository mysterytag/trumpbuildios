﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// WaitForStreamEvent`1/<WaitForStreamEvent>c__AnonStorey145<System.Object>
struct U3CWaitForStreamEventU3Ec__AnonStorey145_t533111624;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"

// System.Void WaitForStreamEvent`1/<WaitForStreamEvent>c__AnonStorey145<System.Object>::.ctor()
extern "C"  void U3CWaitForStreamEventU3Ec__AnonStorey145__ctor_m3877960318_gshared (U3CWaitForStreamEventU3Ec__AnonStorey145_t533111624 * __this, const MethodInfo* method);
#define U3CWaitForStreamEventU3Ec__AnonStorey145__ctor_m3877960318(__this, method) ((  void (*) (U3CWaitForStreamEventU3Ec__AnonStorey145_t533111624 *, const MethodInfo*))U3CWaitForStreamEventU3Ec__AnonStorey145__ctor_m3877960318_gshared)(__this, method)
// System.Void WaitForStreamEvent`1/<WaitForStreamEvent>c__AnonStorey145<System.Object>::<>m__1DF(T)
extern "C"  void U3CWaitForStreamEventU3Ec__AnonStorey145_U3CU3Em__1DF_m3973858928_gshared (U3CWaitForStreamEventU3Ec__AnonStorey145_t533111624 * __this, Il2CppObject * ___val0, const MethodInfo* method);
#define U3CWaitForStreamEventU3Ec__AnonStorey145_U3CU3Em__1DF_m3973858928(__this, ___val0, method) ((  void (*) (U3CWaitForStreamEventU3Ec__AnonStorey145_t533111624 *, Il2CppObject *, const MethodInfo*))U3CWaitForStreamEventU3Ec__AnonStorey145_U3CU3Em__1DF_m3973858928_gshared)(__this, ___val0, method)
