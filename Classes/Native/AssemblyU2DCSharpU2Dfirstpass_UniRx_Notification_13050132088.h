﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Notification_1_g38356375.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Notification`1/OnCompletedNotification<System.Object>
struct  OnCompletedNotification_t3050132088  : public Notification_1_t38356375
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
