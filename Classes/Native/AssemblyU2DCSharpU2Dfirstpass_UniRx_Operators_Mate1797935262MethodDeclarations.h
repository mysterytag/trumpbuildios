﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.MaterializeObservable`1/Materialize<System.Object>
struct Materialize_t1797935262;
// UniRx.Operators.MaterializeObservable`1<System.Object>
struct MaterializeObservable_1_t1619437495;
// UniRx.IObserver`1<UniRx.Notification`1<System.Object>>
struct IObserver_1_t2250355278;
// System.IDisposable
struct IDisposable_t1628921374;
// System.Object
struct Il2CppObject;
// System.Exception
struct Exception_t1967233988;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_Exception1967233988.h"

// System.Void UniRx.Operators.MaterializeObservable`1/Materialize<System.Object>::.ctor(UniRx.Operators.MaterializeObservable`1<T>,UniRx.IObserver`1<UniRx.Notification`1<T>>,System.IDisposable)
extern "C"  void Materialize__ctor_m4173294629_gshared (Materialize_t1797935262 * __this, MaterializeObservable_1_t1619437495 * ___parent0, Il2CppObject* ___observer1, Il2CppObject * ___cancel2, const MethodInfo* method);
#define Materialize__ctor_m4173294629(__this, ___parent0, ___observer1, ___cancel2, method) ((  void (*) (Materialize_t1797935262 *, MaterializeObservable_1_t1619437495 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))Materialize__ctor_m4173294629_gshared)(__this, ___parent0, ___observer1, ___cancel2, method)
// System.IDisposable UniRx.Operators.MaterializeObservable`1/Materialize<System.Object>::Run()
extern "C"  Il2CppObject * Materialize_Run_m2526528201_gshared (Materialize_t1797935262 * __this, const MethodInfo* method);
#define Materialize_Run_m2526528201(__this, method) ((  Il2CppObject * (*) (Materialize_t1797935262 *, const MethodInfo*))Materialize_Run_m2526528201_gshared)(__this, method)
// System.Void UniRx.Operators.MaterializeObservable`1/Materialize<System.Object>::OnNext(T)
extern "C"  void Materialize_OnNext_m333042851_gshared (Materialize_t1797935262 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Materialize_OnNext_m333042851(__this, ___value0, method) ((  void (*) (Materialize_t1797935262 *, Il2CppObject *, const MethodInfo*))Materialize_OnNext_m333042851_gshared)(__this, ___value0, method)
// System.Void UniRx.Operators.MaterializeObservable`1/Materialize<System.Object>::OnError(System.Exception)
extern "C"  void Materialize_OnError_m891177394_gshared (Materialize_t1797935262 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method);
#define Materialize_OnError_m891177394(__this, ___error0, method) ((  void (*) (Materialize_t1797935262 *, Exception_t1967233988 *, const MethodInfo*))Materialize_OnError_m891177394_gshared)(__this, ___error0, method)
// System.Void UniRx.Operators.MaterializeObservable`1/Materialize<System.Object>::OnCompleted()
extern "C"  void Materialize_OnCompleted_m2295887877_gshared (Materialize_t1797935262 * __this, const MethodInfo* method);
#define Materialize_OnCompleted_m2295887877(__this, method) ((  void (*) (Materialize_t1797935262 *, const MethodInfo*))Materialize_OnCompleted_m2295887877_gshared)(__this, method)
