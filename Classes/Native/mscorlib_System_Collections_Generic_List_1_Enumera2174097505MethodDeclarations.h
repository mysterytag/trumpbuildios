﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.List`1<GameAnalyticsSDK.Settings/HelpTypes>
struct List_1_t4088314513;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera2174097505.h"
#include "AssemblyU2DCSharp_GameAnalyticsSDK_Settings_HelpTy3291355544.h"

// System.Void System.Collections.Generic.List`1/Enumerator<GameAnalyticsSDK.Settings/HelpTypes>::.ctor(System.Collections.Generic.List`1<T>)
extern "C"  void Enumerator__ctor_m635465243_gshared (Enumerator_t2174097505 * __this, List_1_t4088314513 * ___l0, const MethodInfo* method);
#define Enumerator__ctor_m635465243(__this, ___l0, method) ((  void (*) (Enumerator_t2174097505 *, List_1_t4088314513 *, const MethodInfo*))Enumerator__ctor_m635465243_gshared)(__this, ___l0, method)
// System.Void System.Collections.Generic.List`1/Enumerator<GameAnalyticsSDK.Settings/HelpTypes>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m4110614871_gshared (Enumerator_t2174097505 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_Reset_m4110614871(__this, method) ((  void (*) (Enumerator_t2174097505 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m4110614871_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1/Enumerator<GameAnalyticsSDK.Settings/HelpTypes>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m1033347843_gshared (Enumerator_t2174097505 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m1033347843(__this, method) ((  Il2CppObject * (*) (Enumerator_t2174097505 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m1033347843_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<GameAnalyticsSDK.Settings/HelpTypes>::Dispose()
extern "C"  void Enumerator_Dispose_m1267331776_gshared (Enumerator_t2174097505 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m1267331776(__this, method) ((  void (*) (Enumerator_t2174097505 *, const MethodInfo*))Enumerator_Dispose_m1267331776_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<GameAnalyticsSDK.Settings/HelpTypes>::VerifyState()
extern "C"  void Enumerator_VerifyState_m621171193_gshared (Enumerator_t2174097505 * __this, const MethodInfo* method);
#define Enumerator_VerifyState_m621171193(__this, method) ((  void (*) (Enumerator_t2174097505 *, const MethodInfo*))Enumerator_VerifyState_m621171193_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<GameAnalyticsSDK.Settings/HelpTypes>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m3919308611_gshared (Enumerator_t2174097505 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m3919308611(__this, method) ((  bool (*) (Enumerator_t2174097505 *, const MethodInfo*))Enumerator_MoveNext_m3919308611_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<GameAnalyticsSDK.Settings/HelpTypes>::get_Current()
extern "C"  int32_t Enumerator_get_Current_m3422376112_gshared (Enumerator_t2174097505 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m3422376112(__this, method) ((  int32_t (*) (Enumerator_t2174097505 *, const MethodInfo*))Enumerator_get_Current_m3422376112_gshared)(__this, method)
