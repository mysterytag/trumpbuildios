﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_Runtime_InteropServices_HandleRef3541566221.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GooglePlayGames.Native.PInvoke.BaseReferenceHolder
struct  BaseReferenceHolder_t634391631  : public Il2CppObject
{
public:
	// System.Runtime.InteropServices.HandleRef GooglePlayGames.Native.PInvoke.BaseReferenceHolder::mSelfPointer
	HandleRef_t3541566221  ___mSelfPointer_0;

public:
	inline static int32_t get_offset_of_mSelfPointer_0() { return static_cast<int32_t>(offsetof(BaseReferenceHolder_t634391631, ___mSelfPointer_0)); }
	inline HandleRef_t3541566221  get_mSelfPointer_0() const { return ___mSelfPointer_0; }
	inline HandleRef_t3541566221 * get_address_of_mSelfPointer_0() { return &___mSelfPointer_0; }
	inline void set_mSelfPointer_0(HandleRef_t3541566221  value)
	{
		___mSelfPointer_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
