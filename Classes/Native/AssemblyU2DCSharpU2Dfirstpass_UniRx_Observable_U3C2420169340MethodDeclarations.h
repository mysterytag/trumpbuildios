﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Observable/<SelectMany>c__AnonStorey49`2<System.Object,System.Object>
struct U3CSelectManyU3Ec__AnonStorey49_2_t2420169340;
// UniRx.IObservable`1<System.Object>
struct IObservable_1_t595904784;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"

// System.Void UniRx.Observable/<SelectMany>c__AnonStorey49`2<System.Object,System.Object>::.ctor()
extern "C"  void U3CSelectManyU3Ec__AnonStorey49_2__ctor_m1590108432_gshared (U3CSelectManyU3Ec__AnonStorey49_2_t2420169340 * __this, const MethodInfo* method);
#define U3CSelectManyU3Ec__AnonStorey49_2__ctor_m1590108432(__this, method) ((  void (*) (U3CSelectManyU3Ec__AnonStorey49_2_t2420169340 *, const MethodInfo*))U3CSelectManyU3Ec__AnonStorey49_2__ctor_m1590108432_gshared)(__this, method)
// UniRx.IObservable`1<TR> UniRx.Observable/<SelectMany>c__AnonStorey49`2<System.Object,System.Object>::<>m__47(T)
extern "C"  Il2CppObject* U3CSelectManyU3Ec__AnonStorey49_2_U3CU3Em__47_m2471360920_gshared (U3CSelectManyU3Ec__AnonStorey49_2_t2420169340 * __this, Il2CppObject * ____0, const MethodInfo* method);
#define U3CSelectManyU3Ec__AnonStorey49_2_U3CU3Em__47_m2471360920(__this, ____0, method) ((  Il2CppObject* (*) (U3CSelectManyU3Ec__AnonStorey49_2_t2420169340 *, Il2CppObject *, const MethodInfo*))U3CSelectManyU3Ec__AnonStorey49_2_U3CU3Em__47_m2471360920_gshared)(__this, ____0, method)
