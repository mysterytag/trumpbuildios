﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utils/Consts
struct  Consts_t2024055184  : public Il2CppObject
{
public:

public:
};

struct Consts_t2024055184_StaticFields
{
public:
	// System.Single Utils/Consts::MoneyMoveDistance
	float ___MoneyMoveDistance_0;

public:
	inline static int32_t get_offset_of_MoneyMoveDistance_0() { return static_cast<int32_t>(offsetof(Consts_t2024055184_StaticFields, ___MoneyMoveDistance_0)); }
	inline float get_MoneyMoveDistance_0() const { return ___MoneyMoveDistance_0; }
	inline float* get_address_of_MoneyMoveDistance_0() { return &___MoneyMoveDistance_0; }
	inline void set_MoneyMoveDistance_0(float value)
	{
		___MoneyMoveDistance_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
