﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Sprite
struct Sprite_t4006040370;

#include "UnityEngine_UnityEngine_MonoBehaviour3012272455.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PlatformSpriteSetter
struct  PlatformSpriteSetter_t3879007191  : public MonoBehaviour_t3012272455
{
public:
	// UnityEngine.Sprite PlatformSpriteSetter::androidSprite
	Sprite_t4006040370 * ___androidSprite_2;
	// UnityEngine.Sprite PlatformSpriteSetter::iosSprite
	Sprite_t4006040370 * ___iosSprite_3;

public:
	inline static int32_t get_offset_of_androidSprite_2() { return static_cast<int32_t>(offsetof(PlatformSpriteSetter_t3879007191, ___androidSprite_2)); }
	inline Sprite_t4006040370 * get_androidSprite_2() const { return ___androidSprite_2; }
	inline Sprite_t4006040370 ** get_address_of_androidSprite_2() { return &___androidSprite_2; }
	inline void set_androidSprite_2(Sprite_t4006040370 * value)
	{
		___androidSprite_2 = value;
		Il2CppCodeGenWriteBarrier(&___androidSprite_2, value);
	}

	inline static int32_t get_offset_of_iosSprite_3() { return static_cast<int32_t>(offsetof(PlatformSpriteSetter_t3879007191, ___iosSprite_3)); }
	inline Sprite_t4006040370 * get_iosSprite_3() const { return ___iosSprite_3; }
	inline Sprite_t4006040370 ** get_address_of_iosSprite_3() { return &___iosSprite_3; }
	inline void set_iosSprite_3(Sprite_t4006040370 * value)
	{
		___iosSprite_3 = value;
		Il2CppCodeGenWriteBarrier(&___iosSprite_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
