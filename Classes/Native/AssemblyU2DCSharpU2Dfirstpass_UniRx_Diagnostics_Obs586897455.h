﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UniRx.Subject`1<UniRx.Diagnostics.LogEntry>
struct Subject_1_t3829190342;
// UniRx.Diagnostics.ObservableLogger
struct ObservableLogger_t586897455;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Diagnostics.ObservableLogger
struct  ObservableLogger_t586897455  : public Il2CppObject
{
public:

public:
};

struct ObservableLogger_t586897455_StaticFields
{
public:
	// UniRx.Subject`1<UniRx.Diagnostics.LogEntry> UniRx.Diagnostics.ObservableLogger::logPublisher
	Subject_1_t3829190342 * ___logPublisher_0;
	// UniRx.Diagnostics.ObservableLogger UniRx.Diagnostics.ObservableLogger::Listener
	ObservableLogger_t586897455 * ___Listener_1;

public:
	inline static int32_t get_offset_of_logPublisher_0() { return static_cast<int32_t>(offsetof(ObservableLogger_t586897455_StaticFields, ___logPublisher_0)); }
	inline Subject_1_t3829190342 * get_logPublisher_0() const { return ___logPublisher_0; }
	inline Subject_1_t3829190342 ** get_address_of_logPublisher_0() { return &___logPublisher_0; }
	inline void set_logPublisher_0(Subject_1_t3829190342 * value)
	{
		___logPublisher_0 = value;
		Il2CppCodeGenWriteBarrier(&___logPublisher_0, value);
	}

	inline static int32_t get_offset_of_Listener_1() { return static_cast<int32_t>(offsetof(ObservableLogger_t586897455_StaticFields, ___Listener_1)); }
	inline ObservableLogger_t586897455 * get_Listener_1() const { return ___Listener_1; }
	inline ObservableLogger_t586897455 ** get_address_of_Listener_1() { return &___Listener_1; }
	inline void set_Listener_1(ObservableLogger_t586897455 * value)
	{
		___Listener_1 = value;
		Il2CppCodeGenWriteBarrier(&___Listener_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
