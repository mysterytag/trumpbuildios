﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Object
struct Il2CppObject;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Tuple`8<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>
struct  Tuple_8_t1597838469  : public Il2CppObject
{
public:
	// T1 UniRx.Tuple`8::item1
	Il2CppObject * ___item1_0;
	// T2 UniRx.Tuple`8::item2
	Il2CppObject * ___item2_1;
	// T3 UniRx.Tuple`8::item3
	Il2CppObject * ___item3_2;
	// T4 UniRx.Tuple`8::item4
	Il2CppObject * ___item4_3;
	// T5 UniRx.Tuple`8::item5
	Il2CppObject * ___item5_4;
	// T6 UniRx.Tuple`8::item6
	Il2CppObject * ___item6_5;
	// T7 UniRx.Tuple`8::item7
	Il2CppObject * ___item7_6;
	// TRest UniRx.Tuple`8::rest
	Il2CppObject * ___rest_7;

public:
	inline static int32_t get_offset_of_item1_0() { return static_cast<int32_t>(offsetof(Tuple_8_t1597838469, ___item1_0)); }
	inline Il2CppObject * get_item1_0() const { return ___item1_0; }
	inline Il2CppObject ** get_address_of_item1_0() { return &___item1_0; }
	inline void set_item1_0(Il2CppObject * value)
	{
		___item1_0 = value;
		Il2CppCodeGenWriteBarrier(&___item1_0, value);
	}

	inline static int32_t get_offset_of_item2_1() { return static_cast<int32_t>(offsetof(Tuple_8_t1597838469, ___item2_1)); }
	inline Il2CppObject * get_item2_1() const { return ___item2_1; }
	inline Il2CppObject ** get_address_of_item2_1() { return &___item2_1; }
	inline void set_item2_1(Il2CppObject * value)
	{
		___item2_1 = value;
		Il2CppCodeGenWriteBarrier(&___item2_1, value);
	}

	inline static int32_t get_offset_of_item3_2() { return static_cast<int32_t>(offsetof(Tuple_8_t1597838469, ___item3_2)); }
	inline Il2CppObject * get_item3_2() const { return ___item3_2; }
	inline Il2CppObject ** get_address_of_item3_2() { return &___item3_2; }
	inline void set_item3_2(Il2CppObject * value)
	{
		___item3_2 = value;
		Il2CppCodeGenWriteBarrier(&___item3_2, value);
	}

	inline static int32_t get_offset_of_item4_3() { return static_cast<int32_t>(offsetof(Tuple_8_t1597838469, ___item4_3)); }
	inline Il2CppObject * get_item4_3() const { return ___item4_3; }
	inline Il2CppObject ** get_address_of_item4_3() { return &___item4_3; }
	inline void set_item4_3(Il2CppObject * value)
	{
		___item4_3 = value;
		Il2CppCodeGenWriteBarrier(&___item4_3, value);
	}

	inline static int32_t get_offset_of_item5_4() { return static_cast<int32_t>(offsetof(Tuple_8_t1597838469, ___item5_4)); }
	inline Il2CppObject * get_item5_4() const { return ___item5_4; }
	inline Il2CppObject ** get_address_of_item5_4() { return &___item5_4; }
	inline void set_item5_4(Il2CppObject * value)
	{
		___item5_4 = value;
		Il2CppCodeGenWriteBarrier(&___item5_4, value);
	}

	inline static int32_t get_offset_of_item6_5() { return static_cast<int32_t>(offsetof(Tuple_8_t1597838469, ___item6_5)); }
	inline Il2CppObject * get_item6_5() const { return ___item6_5; }
	inline Il2CppObject ** get_address_of_item6_5() { return &___item6_5; }
	inline void set_item6_5(Il2CppObject * value)
	{
		___item6_5 = value;
		Il2CppCodeGenWriteBarrier(&___item6_5, value);
	}

	inline static int32_t get_offset_of_item7_6() { return static_cast<int32_t>(offsetof(Tuple_8_t1597838469, ___item7_6)); }
	inline Il2CppObject * get_item7_6() const { return ___item7_6; }
	inline Il2CppObject ** get_address_of_item7_6() { return &___item7_6; }
	inline void set_item7_6(Il2CppObject * value)
	{
		___item7_6 = value;
		Il2CppCodeGenWriteBarrier(&___item7_6, value);
	}

	inline static int32_t get_offset_of_rest_7() { return static_cast<int32_t>(offsetof(Tuple_8_t1597838469, ___rest_7)); }
	inline Il2CppObject * get_rest_7() const { return ___rest_7; }
	inline Il2CppObject ** get_address_of_rest_7() { return &___rest_7; }
	inline void set_rest_7(Il2CppObject * value)
	{
		___rest_7 = value;
		Il2CppCodeGenWriteBarrier(&___rest_7, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
