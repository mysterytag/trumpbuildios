﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Subject`1/Subscription<UnityEngine.Vector4>
struct Subscription_t1304576312;
// UniRx.Subject`1<UnityEngine.Vector4>
struct Subject_1_t1168397114;
// UniRx.IObserver`1<UnityEngine.Vector4>
struct IObserver_1_t1442361397;

#include "codegen/il2cpp-codegen.h"

// System.Void UniRx.Subject`1/Subscription<UnityEngine.Vector4>::.ctor(UniRx.Subject`1<T>,UniRx.IObserver`1<T>)
extern "C"  void Subscription__ctor_m2746649339_gshared (Subscription_t1304576312 * __this, Subject_1_t1168397114 * ___parent0, Il2CppObject* ___unsubscribeTarget1, const MethodInfo* method);
#define Subscription__ctor_m2746649339(__this, ___parent0, ___unsubscribeTarget1, method) ((  void (*) (Subscription_t1304576312 *, Subject_1_t1168397114 *, Il2CppObject*, const MethodInfo*))Subscription__ctor_m2746649339_gshared)(__this, ___parent0, ___unsubscribeTarget1, method)
// System.Void UniRx.Subject`1/Subscription<UnityEngine.Vector4>::Dispose()
extern "C"  void Subscription_Dispose_m1649065499_gshared (Subscription_t1304576312 * __this, const MethodInfo* method);
#define Subscription_Dispose_m1649065499(__this, method) ((  void (*) (Subscription_t1304576312 *, const MethodInfo*))Subscription_Dispose_m1649065499_gshared)(__this, method)
