﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.DelayFrameSubscriptionObservable`1/<SubscribeCore>c__AnonStorey93<System.Object>
struct U3CSubscribeCoreU3Ec__AnonStorey93_t2770048777;

#include "codegen/il2cpp-codegen.h"

// System.Void UniRx.Operators.DelayFrameSubscriptionObservable`1/<SubscribeCore>c__AnonStorey93<System.Object>::.ctor()
extern "C"  void U3CSubscribeCoreU3Ec__AnonStorey93__ctor_m345941600_gshared (U3CSubscribeCoreU3Ec__AnonStorey93_t2770048777 * __this, const MethodInfo* method);
#define U3CSubscribeCoreU3Ec__AnonStorey93__ctor_m345941600(__this, method) ((  void (*) (U3CSubscribeCoreU3Ec__AnonStorey93_t2770048777 *, const MethodInfo*))U3CSubscribeCoreU3Ec__AnonStorey93__ctor_m345941600_gshared)(__this, method)
// System.Void UniRx.Operators.DelayFrameSubscriptionObservable`1/<SubscribeCore>c__AnonStorey93<System.Object>::<>m__BF(System.Int64)
extern "C"  void U3CSubscribeCoreU3Ec__AnonStorey93_U3CU3Em__BF_m2629830079_gshared (U3CSubscribeCoreU3Ec__AnonStorey93_t2770048777 * __this, int64_t ____0, const MethodInfo* method);
#define U3CSubscribeCoreU3Ec__AnonStorey93_U3CU3Em__BF_m2629830079(__this, ____0, method) ((  void (*) (U3CSubscribeCoreU3Ec__AnonStorey93_t2770048777 *, int64_t, const MethodInfo*))U3CSubscribeCoreU3Ec__AnonStorey93_U3CU3Em__BF_m2629830079_gshared)(__this, ____0, method)
