﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.String,System.String>>
struct IEnumerator_1_t3577824552;
// System.Collections.Generic.Dictionary`2<System.String,System.String>
struct Dictionary_2_t2606186806;
// UnityEngine.WWW
struct WWW_t1522972100;
// System.Object
struct Il2CppObject;
// Facebook.Unity.AsyncRequestString
struct AsyncRequestString_t3519914083;

#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_22094718104.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Facebook.Unity.AsyncRequestString/<Start>c__Iterator1
struct  U3CStartU3Ec__Iterator1_t4138896568  : public Il2CppObject
{
public:
	// System.String Facebook.Unity.AsyncRequestString/<Start>c__Iterator1::<urlParams>__0
	String_t* ___U3CurlParamsU3E__0_0;
	// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.String,System.String>> Facebook.Unity.AsyncRequestString/<Start>c__Iterator1::<$s_17>__1
	Il2CppObject* ___U3CU24s_17U3E__1_1;
	// System.Collections.Generic.KeyValuePair`2<System.String,System.String> Facebook.Unity.AsyncRequestString/<Start>c__Iterator1::<pair>__2
	KeyValuePair_2_t2094718104  ___U3CpairU3E__2_2;
	// System.Collections.Generic.Dictionary`2<System.String,System.String> Facebook.Unity.AsyncRequestString/<Start>c__Iterator1::<headers>__3
	Dictionary_2_t2606186806 * ___U3CheadersU3E__3_3;
	// UnityEngine.WWW Facebook.Unity.AsyncRequestString/<Start>c__Iterator1::<www>__4
	WWW_t1522972100 * ___U3CwwwU3E__4_4;
	// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.String,System.String>> Facebook.Unity.AsyncRequestString/<Start>c__Iterator1::<$s_18>__5
	Il2CppObject* ___U3CU24s_18U3E__5_5;
	// System.Collections.Generic.KeyValuePair`2<System.String,System.String> Facebook.Unity.AsyncRequestString/<Start>c__Iterator1::<pair>__6
	KeyValuePair_2_t2094718104  ___U3CpairU3E__6_6;
	// System.Int32 Facebook.Unity.AsyncRequestString/<Start>c__Iterator1::$PC
	int32_t ___U24PC_7;
	// System.Object Facebook.Unity.AsyncRequestString/<Start>c__Iterator1::$current
	Il2CppObject * ___U24current_8;
	// Facebook.Unity.AsyncRequestString Facebook.Unity.AsyncRequestString/<Start>c__Iterator1::<>f__this
	AsyncRequestString_t3519914083 * ___U3CU3Ef__this_9;

public:
	inline static int32_t get_offset_of_U3CurlParamsU3E__0_0() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator1_t4138896568, ___U3CurlParamsU3E__0_0)); }
	inline String_t* get_U3CurlParamsU3E__0_0() const { return ___U3CurlParamsU3E__0_0; }
	inline String_t** get_address_of_U3CurlParamsU3E__0_0() { return &___U3CurlParamsU3E__0_0; }
	inline void set_U3CurlParamsU3E__0_0(String_t* value)
	{
		___U3CurlParamsU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier(&___U3CurlParamsU3E__0_0, value);
	}

	inline static int32_t get_offset_of_U3CU24s_17U3E__1_1() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator1_t4138896568, ___U3CU24s_17U3E__1_1)); }
	inline Il2CppObject* get_U3CU24s_17U3E__1_1() const { return ___U3CU24s_17U3E__1_1; }
	inline Il2CppObject** get_address_of_U3CU24s_17U3E__1_1() { return &___U3CU24s_17U3E__1_1; }
	inline void set_U3CU24s_17U3E__1_1(Il2CppObject* value)
	{
		___U3CU24s_17U3E__1_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU24s_17U3E__1_1, value);
	}

	inline static int32_t get_offset_of_U3CpairU3E__2_2() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator1_t4138896568, ___U3CpairU3E__2_2)); }
	inline KeyValuePair_2_t2094718104  get_U3CpairU3E__2_2() const { return ___U3CpairU3E__2_2; }
	inline KeyValuePair_2_t2094718104 * get_address_of_U3CpairU3E__2_2() { return &___U3CpairU3E__2_2; }
	inline void set_U3CpairU3E__2_2(KeyValuePair_2_t2094718104  value)
	{
		___U3CpairU3E__2_2 = value;
	}

	inline static int32_t get_offset_of_U3CheadersU3E__3_3() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator1_t4138896568, ___U3CheadersU3E__3_3)); }
	inline Dictionary_2_t2606186806 * get_U3CheadersU3E__3_3() const { return ___U3CheadersU3E__3_3; }
	inline Dictionary_2_t2606186806 ** get_address_of_U3CheadersU3E__3_3() { return &___U3CheadersU3E__3_3; }
	inline void set_U3CheadersU3E__3_3(Dictionary_2_t2606186806 * value)
	{
		___U3CheadersU3E__3_3 = value;
		Il2CppCodeGenWriteBarrier(&___U3CheadersU3E__3_3, value);
	}

	inline static int32_t get_offset_of_U3CwwwU3E__4_4() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator1_t4138896568, ___U3CwwwU3E__4_4)); }
	inline WWW_t1522972100 * get_U3CwwwU3E__4_4() const { return ___U3CwwwU3E__4_4; }
	inline WWW_t1522972100 ** get_address_of_U3CwwwU3E__4_4() { return &___U3CwwwU3E__4_4; }
	inline void set_U3CwwwU3E__4_4(WWW_t1522972100 * value)
	{
		___U3CwwwU3E__4_4 = value;
		Il2CppCodeGenWriteBarrier(&___U3CwwwU3E__4_4, value);
	}

	inline static int32_t get_offset_of_U3CU24s_18U3E__5_5() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator1_t4138896568, ___U3CU24s_18U3E__5_5)); }
	inline Il2CppObject* get_U3CU24s_18U3E__5_5() const { return ___U3CU24s_18U3E__5_5; }
	inline Il2CppObject** get_address_of_U3CU24s_18U3E__5_5() { return &___U3CU24s_18U3E__5_5; }
	inline void set_U3CU24s_18U3E__5_5(Il2CppObject* value)
	{
		___U3CU24s_18U3E__5_5 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU24s_18U3E__5_5, value);
	}

	inline static int32_t get_offset_of_U3CpairU3E__6_6() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator1_t4138896568, ___U3CpairU3E__6_6)); }
	inline KeyValuePair_2_t2094718104  get_U3CpairU3E__6_6() const { return ___U3CpairU3E__6_6; }
	inline KeyValuePair_2_t2094718104 * get_address_of_U3CpairU3E__6_6() { return &___U3CpairU3E__6_6; }
	inline void set_U3CpairU3E__6_6(KeyValuePair_2_t2094718104  value)
	{
		___U3CpairU3E__6_6 = value;
	}

	inline static int32_t get_offset_of_U24PC_7() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator1_t4138896568, ___U24PC_7)); }
	inline int32_t get_U24PC_7() const { return ___U24PC_7; }
	inline int32_t* get_address_of_U24PC_7() { return &___U24PC_7; }
	inline void set_U24PC_7(int32_t value)
	{
		___U24PC_7 = value;
	}

	inline static int32_t get_offset_of_U24current_8() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator1_t4138896568, ___U24current_8)); }
	inline Il2CppObject * get_U24current_8() const { return ___U24current_8; }
	inline Il2CppObject ** get_address_of_U24current_8() { return &___U24current_8; }
	inline void set_U24current_8(Il2CppObject * value)
	{
		___U24current_8 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_8, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__this_9() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator1_t4138896568, ___U3CU3Ef__this_9)); }
	inline AsyncRequestString_t3519914083 * get_U3CU3Ef__this_9() const { return ___U3CU3Ef__this_9; }
	inline AsyncRequestString_t3519914083 ** get_address_of_U3CU3Ef__this_9() { return &___U3CU3Ef__this_9; }
	inline void set_U3CU3Ef__this_9(AsyncRequestString_t3519914083 * value)
	{
		___U3CU3Ef__this_9 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__this_9, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
