﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Observer/Subscribe`1<System.Object>
struct Subscribe_1_t3945894204;
// System.Action`1<System.Object>
struct Action_1_t985559125;
// System.Action`1<System.Exception>
struct Action_1_t2115686693;
// System.Action
struct Action_t437523947;
// System.Object
struct Il2CppObject;
// System.Exception
struct Exception_t1967233988;

#include "codegen/il2cpp-codegen.h"
#include "System_Core_System_Action437523947.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_Exception1967233988.h"

// System.Void UniRx.Observer/Subscribe`1<System.Object>::.ctor(System.Action`1<T>,System.Action`1<System.Exception>,System.Action)
extern "C"  void Subscribe_1__ctor_m947135104_gshared (Subscribe_1_t3945894204 * __this, Action_1_t985559125 * ___onNext0, Action_1_t2115686693 * ___onError1, Action_t437523947 * ___onCompleted2, const MethodInfo* method);
#define Subscribe_1__ctor_m947135104(__this, ___onNext0, ___onError1, ___onCompleted2, method) ((  void (*) (Subscribe_1_t3945894204 *, Action_1_t985559125 *, Action_1_t2115686693 *, Action_t437523947 *, const MethodInfo*))Subscribe_1__ctor_m947135104_gshared)(__this, ___onNext0, ___onError1, ___onCompleted2, method)
// System.Void UniRx.Observer/Subscribe`1<System.Object>::OnNext(T)
extern "C"  void Subscribe_1_OnNext_m4155591005_gshared (Subscribe_1_t3945894204 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Subscribe_1_OnNext_m4155591005(__this, ___value0, method) ((  void (*) (Subscribe_1_t3945894204 *, Il2CppObject *, const MethodInfo*))Subscribe_1_OnNext_m4155591005_gshared)(__this, ___value0, method)
// System.Void UniRx.Observer/Subscribe`1<System.Object>::OnError(System.Exception)
extern "C"  void Subscribe_1_OnError_m519637612_gshared (Subscribe_1_t3945894204 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method);
#define Subscribe_1_OnError_m519637612(__this, ___error0, method) ((  void (*) (Subscribe_1_t3945894204 *, Exception_t1967233988 *, const MethodInfo*))Subscribe_1_OnError_m519637612_gshared)(__this, ___error0, method)
// System.Void UniRx.Observer/Subscribe`1<System.Object>::OnCompleted()
extern "C"  void Subscribe_1_OnCompleted_m370343871_gshared (Subscribe_1_t3945894204 * __this, const MethodInfo* method);
#define Subscribe_1_OnCompleted_m370343871(__this, method) ((  void (*) (Subscribe_1_t3945894204 *, const MethodInfo*))Subscribe_1_OnCompleted_m370343871_gshared)(__this, method)
