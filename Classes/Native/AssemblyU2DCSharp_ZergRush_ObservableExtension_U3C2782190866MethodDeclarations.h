﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ZergRush.ObservableExtension/<EveryTapOutsideRectTransform>c__AnonStorey11B
struct U3CEveryTapOutsideRectTransformU3Ec__AnonStorey11B_t2782190866;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Vector23525329788.h"

// System.Void ZergRush.ObservableExtension/<EveryTapOutsideRectTransform>c__AnonStorey11B::.ctor()
extern "C"  void U3CEveryTapOutsideRectTransformU3Ec__AnonStorey11B__ctor_m3026690416 (U3CEveryTapOutsideRectTransformU3Ec__AnonStorey11B_t2782190866 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ZergRush.ObservableExtension/<EveryTapOutsideRectTransform>c__AnonStorey11B::<>m__1A0(UnityEngine.Vector2)
extern "C"  bool U3CEveryTapOutsideRectTransformU3Ec__AnonStorey11B_U3CU3Em__1A0_m1024781829 (U3CEveryTapOutsideRectTransformU3Ec__AnonStorey11B_t2782190866 * __this, Vector2_t3525329788  ___position0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
