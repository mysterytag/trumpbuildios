﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// LitJson.JsonMapper/<RegisterExporter>c__AnonStorey153`1<System.Object>
struct U3CRegisterExporterU3Ec__AnonStorey153_1_t909718040;
// System.Object
struct Il2CppObject;
// LitJson.JsonWriter
struct JsonWriter_t3021344960;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "AssemblyU2DCSharp_LitJson_JsonWriter3021344960.h"

// System.Void LitJson.JsonMapper/<RegisterExporter>c__AnonStorey153`1<System.Object>::.ctor()
extern "C"  void U3CRegisterExporterU3Ec__AnonStorey153_1__ctor_m3831635565_gshared (U3CRegisterExporterU3Ec__AnonStorey153_1_t909718040 * __this, const MethodInfo* method);
#define U3CRegisterExporterU3Ec__AnonStorey153_1__ctor_m3831635565(__this, method) ((  void (*) (U3CRegisterExporterU3Ec__AnonStorey153_1_t909718040 *, const MethodInfo*))U3CRegisterExporterU3Ec__AnonStorey153_1__ctor_m3831635565_gshared)(__this, method)
// System.Void LitJson.JsonMapper/<RegisterExporter>c__AnonStorey153`1<System.Object>::<>m__221(System.Object,LitJson.JsonWriter)
extern "C"  void U3CRegisterExporterU3Ec__AnonStorey153_1_U3CU3Em__221_m2139979237_gshared (U3CRegisterExporterU3Ec__AnonStorey153_1_t909718040 * __this, Il2CppObject * ___obj0, JsonWriter_t3021344960 * ___writer1, const MethodInfo* method);
#define U3CRegisterExporterU3Ec__AnonStorey153_1_U3CU3Em__221_m2139979237(__this, ___obj0, ___writer1, method) ((  void (*) (U3CRegisterExporterU3Ec__AnonStorey153_1_t909718040 *, Il2CppObject *, JsonWriter_t3021344960 *, const MethodInfo*))U3CRegisterExporterU3Ec__AnonStorey153_1_U3CU3Em__221_m2139979237_gshared)(__this, ___obj0, ___writer1, method)
