﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// OnePF.JSON/_JSON/Serializer
struct Serializer_t1395478963;
// System.String
struct String_t;
// OnePF.JSON
struct JSON_t2649481148;
// System.Object
struct Il2CppObject;
// System.Collections.IDictionary
struct IDictionary_t1654916945;
// System.Collections.IList
struct IList_t1612618265;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharpU2Dfirstpass_OnePF_JSON2649481148.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_String968488902.h"

// System.Void OnePF.JSON/_JSON/Serializer::.ctor()
extern "C"  void Serializer__ctor_m3624748424 (Serializer_t1395478963 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String OnePF.JSON/_JSON/Serializer::Serialize(OnePF.JSON)
extern "C"  String_t* Serializer_Serialize_m1966622095 (Il2CppObject * __this /* static, unused */, JSON_t2649481148 * ___obj0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OnePF.JSON/_JSON/Serializer::SerializeValue(System.Object)
extern "C"  void Serializer_SerializeValue_m824966023 (Serializer_t1395478963 * __this, Il2CppObject * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OnePF.JSON/_JSON/Serializer::SerializeObject(OnePF.JSON)
extern "C"  void Serializer_SerializeObject_m3219164747 (Serializer_t1395478963 * __this, JSON_t2649481148 * ___obj0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OnePF.JSON/_JSON/Serializer::SerializeDictionary(System.Collections.IDictionary)
extern "C"  void Serializer_SerializeDictionary_m1938765173 (Serializer_t1395478963 * __this, Il2CppObject * ___obj0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OnePF.JSON/_JSON/Serializer::SerializeArray(System.Collections.IList)
extern "C"  void Serializer_SerializeArray_m3482085126 (Serializer_t1395478963 * __this, Il2CppObject * ___anArray0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OnePF.JSON/_JSON/Serializer::SerializeString(System.String)
extern "C"  void Serializer_SerializeString_m207856939 (Serializer_t1395478963 * __this, String_t* ___str0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OnePF.JSON/_JSON/Serializer::SerializeOther(System.Object)
extern "C"  void Serializer_SerializeOther_m1994865672 (Serializer_t1395478963 * __this, Il2CppObject * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
