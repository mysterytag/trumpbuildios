﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.FromCoroutine`1<System.Double>
struct FromCoroutine_1_t211580279;
// System.Func`3<UniRx.IObserver`1<System.Double>,UniRx.CancellationToken,System.Collections.IEnumerator>
struct Func_3_t1280587413;
// System.IDisposable
struct IDisposable_t1628921374;
// UniRx.IObserver`1<System.Double>
struct IObserver_1_t2746515517;

#include "codegen/il2cpp-codegen.h"

// System.Void UniRx.Operators.FromCoroutine`1<System.Double>::.ctor(System.Func`3<UniRx.IObserver`1<T>,UniRx.CancellationToken,System.Collections.IEnumerator>)
extern "C"  void FromCoroutine_1__ctor_m2896891658_gshared (FromCoroutine_1_t211580279 * __this, Func_3_t1280587413 * ___coroutine0, const MethodInfo* method);
#define FromCoroutine_1__ctor_m2896891658(__this, ___coroutine0, method) ((  void (*) (FromCoroutine_1_t211580279 *, Func_3_t1280587413 *, const MethodInfo*))FromCoroutine_1__ctor_m2896891658_gshared)(__this, ___coroutine0, method)
// System.IDisposable UniRx.Operators.FromCoroutine`1<System.Double>::SubscribeCore(UniRx.IObserver`1<T>,System.IDisposable)
extern "C"  Il2CppObject * FromCoroutine_1_SubscribeCore_m943320497_gshared (FromCoroutine_1_t211580279 * __this, Il2CppObject* ___observer0, Il2CppObject * ___cancel1, const MethodInfo* method);
#define FromCoroutine_1_SubscribeCore_m943320497(__this, ___observer0, ___cancel1, method) ((  Il2CppObject * (*) (FromCoroutine_1_t211580279 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))FromCoroutine_1_SubscribeCore_m943320497_gshared)(__this, ___observer0, ___cancel1, method)
