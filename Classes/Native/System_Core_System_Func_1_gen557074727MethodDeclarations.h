﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "System_Core_System_Func_1_gen1979887667MethodDeclarations.h"

// System.Void System.Func`1<System.Collections.Generic.IEnumerable`1<System.Object>>::.ctor(System.Object,System.IntPtr)
#define Func_1__ctor_m2647529748(__this, ___object0, ___method1, method) ((  void (*) (Func_1_t557074727 *, Il2CppObject *, IntPtr_t, const MethodInfo*))Func_1__ctor_m2351420511_gshared)(__this, ___object0, ___method1, method)
// TResult System.Func`1<System.Collections.Generic.IEnumerable`1<System.Object>>::Invoke()
#define Func_1_Invoke_m4151312(__this, method) ((  Il2CppObject* (*) (Func_1_t557074727 *, const MethodInfo*))Func_1_Invoke_m2160158107_gshared)(__this, method)
// System.IAsyncResult System.Func`1<System.Collections.Generic.IEnumerable`1<System.Object>>::BeginInvoke(System.AsyncCallback,System.Object)
#define Func_1_BeginInvoke_m3527475669(__this, ___callback0, ___object1, method) ((  Il2CppObject * (*) (Func_1_t557074727 *, AsyncCallback_t1363551830 *, Il2CppObject *, const MethodInfo*))Func_1_BeginInvoke_m4066620266_gshared)(__this, ___callback0, ___object1, method)
// TResult System.Func`1<System.Collections.Generic.IEnumerable`1<System.Object>>::EndInvoke(System.IAsyncResult)
#define Func_1_EndInvoke_m2807746886(__this, ___result0, method) ((  Il2CppObject* (*) (Func_1_t557074727 *, Il2CppObject *, const MethodInfo*))Func_1_EndInvoke_m3356710033_gshared)(__this, ___result0, method)
