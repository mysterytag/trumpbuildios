﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Observable/<ToAsync>c__AnonStorey3A
struct U3CToAsyncU3Ec__AnonStorey3A_t2315298076;
// UniRx.IObservable`1<UniRx.Unit>
struct IObservable_1_t2317084402;

#include "codegen/il2cpp-codegen.h"

// System.Void UniRx.Observable/<ToAsync>c__AnonStorey3A::.ctor()
extern "C"  void U3CToAsyncU3Ec__AnonStorey3A__ctor_m3205604775 (U3CToAsyncU3Ec__AnonStorey3A_t2315298076 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UniRx.IObservable`1<UniRx.Unit> UniRx.Observable/<ToAsync>c__AnonStorey3A::<>m__3D()
extern "C"  Il2CppObject* U3CToAsyncU3Ec__AnonStorey3A_U3CU3Em__3D_m2211096745 (U3CToAsyncU3Ec__AnonStorey3A_t2315298076 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
