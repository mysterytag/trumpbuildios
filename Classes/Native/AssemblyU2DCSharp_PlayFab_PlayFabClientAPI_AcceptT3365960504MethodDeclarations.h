﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.PlayFabClientAPI/AcceptTradeRequestCallback
struct AcceptTradeRequestCallback_t3365960504;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;
// PlayFab.ClientModels.AcceptTradeRequest
struct AcceptTradeRequest_t1814136291;
// System.IAsyncResult
struct IAsyncResult_t537683269;
// System.AsyncCallback
struct AsyncCallback_t1363551830;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_IntPtr676692020.h"
#include "mscorlib_System_String968488902.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_AcceptTrade1814136291.h"
#include "mscorlib_System_AsyncCallback1363551830.h"

// System.Void PlayFab.PlayFabClientAPI/AcceptTradeRequestCallback::.ctor(System.Object,System.IntPtr)
extern "C"  void AcceptTradeRequestCallback__ctor_m476570663 (AcceptTradeRequestCallback_t3365960504 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/AcceptTradeRequestCallback::Invoke(System.String,System.Int32,PlayFab.ClientModels.AcceptTradeRequest,System.Object)
extern "C"  void AcceptTradeRequestCallback_Invoke_m1989441567 (AcceptTradeRequestCallback_t3365960504 * __this, String_t* ___urlPath0, int32_t ___callId1, AcceptTradeRequest_t1814136291 * ___request2, Il2CppObject * ___customData3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
extern "C" void pinvoke_delegate_wrapper_AcceptTradeRequestCallback_t3365960504(Il2CppObject* delegate, String_t* ___urlPath0, int32_t ___callId1, AcceptTradeRequest_t1814136291 * ___request2, Il2CppObject * ___customData3);
// System.IAsyncResult PlayFab.PlayFabClientAPI/AcceptTradeRequestCallback::BeginInvoke(System.String,System.Int32,PlayFab.ClientModels.AcceptTradeRequest,System.Object,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * AcceptTradeRequestCallback_BeginInvoke_m1534752598 (AcceptTradeRequestCallback_t3365960504 * __this, String_t* ___urlPath0, int32_t ___callId1, AcceptTradeRequest_t1814136291 * ___request2, Il2CppObject * ___customData3, AsyncCallback_t1363551830 * ___callback4, Il2CppObject * ___object5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/AcceptTradeRequestCallback::EndInvoke(System.IAsyncResult)
extern "C"  void AcceptTradeRequestCallback_EndInvoke_m2200535479 (AcceptTradeRequestCallback_t3365960504 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
