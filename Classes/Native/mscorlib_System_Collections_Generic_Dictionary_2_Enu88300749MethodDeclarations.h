﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E3591453091MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.IDisposable>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define Enumerator__ctor_m1193163591(__this, ___dictionary0, method) ((  void (*) (Enumerator_t88300749 *, Dictionary_2_t321272808 *, const MethodInfo*))Enumerator__ctor_m3920831137_gshared)(__this, ___dictionary0, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.IDisposable>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m916072900(__this, method) ((  Il2CppObject * (*) (Enumerator_t88300749 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m3262087712_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.IDisposable>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m770504462(__this, method) ((  void (*) (Enumerator_t88300749 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m2959141748_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.IDisposable>::System.Collections.IDictionaryEnumerator.get_Entry()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m2538028037(__this, method) ((  DictionaryEntry_t130027246  (*) (Enumerator_t88300749 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m2279524093_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.IDisposable>::System.Collections.IDictionaryEnumerator.get_Key()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m2288780192(__this, method) ((  Il2CppObject * (*) (Enumerator_t88300749 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m1201448700_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.IDisposable>::System.Collections.IDictionaryEnumerator.get_Value()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m1542945330(__this, method) ((  Il2CppObject * (*) (Enumerator_t88300749 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m294434446_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.IDisposable>::MoveNext()
#define Enumerator_MoveNext_m2031035454(__this, method) ((  bool (*) (Enumerator_t88300749 *, const MethodInfo*))Enumerator_MoveNext_m217327200_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.IDisposable>::get_Current()
#define Enumerator_get_Current_m3284725566(__this, method) ((  KeyValuePair_2_t4104771402  (*) (Enumerator_t88300749 *, const MethodInfo*))Enumerator_get_Current_m4240003024_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.IDisposable>::get_CurrentKey()
#define Enumerator_get_CurrentKey_m741965255(__this, method) ((  Il2CppObject * (*) (Enumerator_t88300749 *, const MethodInfo*))Enumerator_get_CurrentKey_m3062159917_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.IDisposable>::get_CurrentValue()
#define Enumerator_get_CurrentValue_m2887891911(__this, method) ((  Il2CppObject * (*) (Enumerator_t88300749 *, const MethodInfo*))Enumerator_get_CurrentValue_m592783249_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.IDisposable>::Reset()
#define Enumerator_Reset_m3132283673(__this, method) ((  void (*) (Enumerator_t88300749 *, const MethodInfo*))Enumerator_Reset_m3001375603_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.IDisposable>::VerifyState()
#define Enumerator_VerifyState_m3195122786(__this, method) ((  void (*) (Enumerator_t88300749 *, const MethodInfo*))Enumerator_VerifyState_m4290054460_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.IDisposable>::VerifyCurrent()
#define Enumerator_VerifyCurrent_m2356252490(__this, method) ((  void (*) (Enumerator_t88300749 *, const MethodInfo*))Enumerator_VerifyCurrent_m2318603684_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.IDisposable>::Dispose()
#define Enumerator_Dispose_m1875964329(__this, method) ((  void (*) (Enumerator_t88300749 *, const MethodInfo*))Enumerator_Dispose_m627360643_gshared)(__this, method)
