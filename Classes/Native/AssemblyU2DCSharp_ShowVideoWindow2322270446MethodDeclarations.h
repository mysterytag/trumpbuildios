﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ShowVideoWindow
struct ShowVideoWindow_t2322270446;

#include "codegen/il2cpp-codegen.h"

// System.Void ShowVideoWindow::.ctor()
extern "C"  void ShowVideoWindow__ctor_m938273341 (ShowVideoWindow_t2322270446 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ShowVideoWindow::Awake()
extern "C"  void ShowVideoWindow_Awake_m1175878560 (ShowVideoWindow_t2322270446 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ShowVideoWindow::Click()
extern "C"  void ShowVideoWindow_Click_m2643120867 (ShowVideoWindow_t2322270446 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
