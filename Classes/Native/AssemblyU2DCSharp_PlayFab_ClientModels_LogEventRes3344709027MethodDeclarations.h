﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.ClientModels.LogEventResult
struct LogEventResult_t3344709027;

#include "codegen/il2cpp-codegen.h"

// System.Void PlayFab.ClientModels.LogEventResult::.ctor()
extern "C"  void LogEventResult__ctor_m460637306 (LogEventResult_t3344709027 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
