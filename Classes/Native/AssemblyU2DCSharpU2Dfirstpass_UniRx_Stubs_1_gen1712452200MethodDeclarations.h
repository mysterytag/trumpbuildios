﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Tuple_2_gen369261819.h"

// System.Void UniRx.Stubs`1<UniRx.Tuple`2<System.Object,System.Object>>::.cctor()
extern "C"  void Stubs_1__cctor_m3681425582_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define Stubs_1__cctor_m3681425582(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))Stubs_1__cctor_m3681425582_gshared)(__this /* static, unused */, method)
// System.Void UniRx.Stubs`1<UniRx.Tuple`2<System.Object,System.Object>>::<Ignore>m__71(T)
extern "C"  void Stubs_1_U3CIgnoreU3Em__71_m3306643370_gshared (Il2CppObject * __this /* static, unused */, Tuple_2_t369261819  ___t0, const MethodInfo* method);
#define Stubs_1_U3CIgnoreU3Em__71_m3306643370(__this /* static, unused */, ___t0, method) ((  void (*) (Il2CppObject * /* static, unused */, Tuple_2_t369261819 , const MethodInfo*))Stubs_1_U3CIgnoreU3Em__71_m3306643370_gshared)(__this /* static, unused */, ___t0, method)
// T UniRx.Stubs`1<UniRx.Tuple`2<System.Object,System.Object>>::<Identity>m__72(T)
extern "C"  Tuple_2_t369261819  Stubs_1_U3CIdentityU3Em__72_m684371070_gshared (Il2CppObject * __this /* static, unused */, Tuple_2_t369261819  ___t0, const MethodInfo* method);
#define Stubs_1_U3CIdentityU3Em__72_m684371070(__this /* static, unused */, ___t0, method) ((  Tuple_2_t369261819  (*) (Il2CppObject * /* static, unused */, Tuple_2_t369261819 , const MethodInfo*))Stubs_1_U3CIdentityU3Em__72_m684371070_gshared)(__this /* static, unused */, ___t0, method)
