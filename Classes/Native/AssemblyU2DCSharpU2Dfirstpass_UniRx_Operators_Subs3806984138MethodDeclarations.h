﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.SubscribeOnObservable`1/<SubscribeCore>c__AnonStorey69<System.Object>
struct U3CSubscribeCoreU3Ec__AnonStorey69_t3806984138;

#include "codegen/il2cpp-codegen.h"

// System.Void UniRx.Operators.SubscribeOnObservable`1/<SubscribeCore>c__AnonStorey69<System.Object>::.ctor()
extern "C"  void U3CSubscribeCoreU3Ec__AnonStorey69__ctor_m2641813533_gshared (U3CSubscribeCoreU3Ec__AnonStorey69_t3806984138 * __this, const MethodInfo* method);
#define U3CSubscribeCoreU3Ec__AnonStorey69__ctor_m2641813533(__this, method) ((  void (*) (U3CSubscribeCoreU3Ec__AnonStorey69_t3806984138 *, const MethodInfo*))U3CSubscribeCoreU3Ec__AnonStorey69__ctor_m2641813533_gshared)(__this, method)
// System.Void UniRx.Operators.SubscribeOnObservable`1/<SubscribeCore>c__AnonStorey69<System.Object>::<>m__87()
extern "C"  void U3CSubscribeCoreU3Ec__AnonStorey69_U3CU3Em__87_m2765388517_gshared (U3CSubscribeCoreU3Ec__AnonStorey69_t3806984138 * __this, const MethodInfo* method);
#define U3CSubscribeCoreU3Ec__AnonStorey69_U3CU3Em__87_m2765388517(__this, method) ((  void (*) (U3CSubscribeCoreU3Ec__AnonStorey69_t3806984138 *, const MethodInfo*))U3CSubscribeCoreU3Ec__AnonStorey69_U3CU3Em__87_m2765388517_gshared)(__this, method)
