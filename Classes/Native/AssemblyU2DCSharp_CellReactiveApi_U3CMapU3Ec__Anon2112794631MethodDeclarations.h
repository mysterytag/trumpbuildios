﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CellReactiveApi/<Map>c__AnonStorey10E`2/<Map>c__AnonStorey10F`2<System.Object,System.Object>
struct U3CMapU3Ec__AnonStorey10F_2_t2112794631;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"

// System.Void CellReactiveApi/<Map>c__AnonStorey10E`2/<Map>c__AnonStorey10F`2<System.Object,System.Object>::.ctor()
extern "C"  void U3CMapU3Ec__AnonStorey10F_2__ctor_m2278539761_gshared (U3CMapU3Ec__AnonStorey10F_2_t2112794631 * __this, const MethodInfo* method);
#define U3CMapU3Ec__AnonStorey10F_2__ctor_m2278539761(__this, method) ((  void (*) (U3CMapU3Ec__AnonStorey10F_2_t2112794631 *, const MethodInfo*))U3CMapU3Ec__AnonStorey10F_2__ctor_m2278539761_gshared)(__this, method)
// System.Void CellReactiveApi/<Map>c__AnonStorey10E`2/<Map>c__AnonStorey10F`2<System.Object,System.Object>::<>m__197(T)
extern "C"  void U3CMapU3Ec__AnonStorey10F_2_U3CU3Em__197_m578701383_gshared (U3CMapU3Ec__AnonStorey10F_2_t2112794631 * __this, Il2CppObject * ___val0, const MethodInfo* method);
#define U3CMapU3Ec__AnonStorey10F_2_U3CU3Em__197_m578701383(__this, ___val0, method) ((  void (*) (U3CMapU3Ec__AnonStorey10F_2_t2112794631 *, Il2CppObject *, const MethodInfo*))U3CMapU3Ec__AnonStorey10F_2_U3CU3Em__197_m578701383_gshared)(__this, ___val0, method)
