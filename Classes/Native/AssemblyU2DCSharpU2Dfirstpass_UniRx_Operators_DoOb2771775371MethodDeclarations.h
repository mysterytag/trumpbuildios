﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.DoObserverObservable`1<System.Object>
struct DoObserverObservable_1_t2771775371;
// UniRx.IObservable`1<System.Object>
struct IObservable_1_t595904784;
// UniRx.IObserver`1<System.Object>
struct IObserver_1_t3049105323;
// System.IDisposable
struct IDisposable_t1628921374;

#include "codegen/il2cpp-codegen.h"

// System.Void UniRx.Operators.DoObserverObservable`1<System.Object>::.ctor(UniRx.IObservable`1<T>,UniRx.IObserver`1<T>)
extern "C"  void DoObserverObservable_1__ctor_m3723482770_gshared (DoObserverObservable_1_t2771775371 * __this, Il2CppObject* ___source0, Il2CppObject* ___observer1, const MethodInfo* method);
#define DoObserverObservable_1__ctor_m3723482770(__this, ___source0, ___observer1, method) ((  void (*) (DoObserverObservable_1_t2771775371 *, Il2CppObject*, Il2CppObject*, const MethodInfo*))DoObserverObservable_1__ctor_m3723482770_gshared)(__this, ___source0, ___observer1, method)
// System.IDisposable UniRx.Operators.DoObserverObservable`1<System.Object>::SubscribeCore(UniRx.IObserver`1<T>,System.IDisposable)
extern "C"  Il2CppObject * DoObserverObservable_1_SubscribeCore_m3581137249_gshared (DoObserverObservable_1_t2771775371 * __this, Il2CppObject* ___observer0, Il2CppObject * ___cancel1, const MethodInfo* method);
#define DoObserverObservable_1_SubscribeCore_m3581137249(__this, ___observer0, ___cancel1, method) ((  Il2CppObject * (*) (DoObserverObservable_1_t2771775371 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))DoObserverObservable_1_SubscribeCore_m3581137249_gshared)(__this, ___observer0, ___cancel1, method)
