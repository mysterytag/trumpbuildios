﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UniRx.IObserver`1<UniRx.Unit>
struct IObserver_1_t475317645;
// UnityEngine.Events.UnityAction
struct UnityAction_t909267611;
// UniRx.UnityGraphicExtensions/<DirtyMaterialCallbackAsObservable>c__AnonStoreyA0
struct U3CDirtyMaterialCallbackAsObservableU3Ec__AnonStoreyA0_t1880697263;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.UnityGraphicExtensions/<DirtyMaterialCallbackAsObservable>c__AnonStoreyA0/<DirtyMaterialCallbackAsObservable>c__AnonStorey9F
struct  U3CDirtyMaterialCallbackAsObservableU3Ec__AnonStorey9F_t1880697037  : public Il2CppObject
{
public:
	// UniRx.IObserver`1<UniRx.Unit> UniRx.UnityGraphicExtensions/<DirtyMaterialCallbackAsObservable>c__AnonStoreyA0/<DirtyMaterialCallbackAsObservable>c__AnonStorey9F::observer
	Il2CppObject* ___observer_0;
	// UnityEngine.Events.UnityAction UniRx.UnityGraphicExtensions/<DirtyMaterialCallbackAsObservable>c__AnonStoreyA0/<DirtyMaterialCallbackAsObservable>c__AnonStorey9F::registerHandler
	UnityAction_t909267611 * ___registerHandler_1;
	// UniRx.UnityGraphicExtensions/<DirtyMaterialCallbackAsObservable>c__AnonStoreyA0 UniRx.UnityGraphicExtensions/<DirtyMaterialCallbackAsObservable>c__AnonStoreyA0/<DirtyMaterialCallbackAsObservable>c__AnonStorey9F::<>f__ref$160
	U3CDirtyMaterialCallbackAsObservableU3Ec__AnonStoreyA0_t1880697263 * ___U3CU3Ef__refU24160_2;

public:
	inline static int32_t get_offset_of_observer_0() { return static_cast<int32_t>(offsetof(U3CDirtyMaterialCallbackAsObservableU3Ec__AnonStorey9F_t1880697037, ___observer_0)); }
	inline Il2CppObject* get_observer_0() const { return ___observer_0; }
	inline Il2CppObject** get_address_of_observer_0() { return &___observer_0; }
	inline void set_observer_0(Il2CppObject* value)
	{
		___observer_0 = value;
		Il2CppCodeGenWriteBarrier(&___observer_0, value);
	}

	inline static int32_t get_offset_of_registerHandler_1() { return static_cast<int32_t>(offsetof(U3CDirtyMaterialCallbackAsObservableU3Ec__AnonStorey9F_t1880697037, ___registerHandler_1)); }
	inline UnityAction_t909267611 * get_registerHandler_1() const { return ___registerHandler_1; }
	inline UnityAction_t909267611 ** get_address_of_registerHandler_1() { return &___registerHandler_1; }
	inline void set_registerHandler_1(UnityAction_t909267611 * value)
	{
		___registerHandler_1 = value;
		Il2CppCodeGenWriteBarrier(&___registerHandler_1, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__refU24160_2() { return static_cast<int32_t>(offsetof(U3CDirtyMaterialCallbackAsObservableU3Ec__AnonStorey9F_t1880697037, ___U3CU3Ef__refU24160_2)); }
	inline U3CDirtyMaterialCallbackAsObservableU3Ec__AnonStoreyA0_t1880697263 * get_U3CU3Ef__refU24160_2() const { return ___U3CU3Ef__refU24160_2; }
	inline U3CDirtyMaterialCallbackAsObservableU3Ec__AnonStoreyA0_t1880697263 ** get_address_of_U3CU3Ef__refU24160_2() { return &___U3CU3Ef__refU24160_2; }
	inline void set_U3CU3Ef__refU24160_2(U3CDirtyMaterialCallbackAsObservableU3Ec__AnonStoreyA0_t1880697263 * value)
	{
		___U3CU3Ef__refU24160_2 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__refU24160_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
