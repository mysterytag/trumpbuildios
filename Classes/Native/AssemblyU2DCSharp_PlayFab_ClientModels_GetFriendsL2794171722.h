﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Generic.List`1<PlayFab.ClientModels.FriendInfo>
struct List_1_t210693525;

#include "AssemblyU2DCSharp_PlayFab_Internal_PlayFabResultCom379512675.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PlayFab.ClientModels.GetFriendsListResult
struct  GetFriendsListResult_t2794171722  : public PlayFabResultCommon_t379512675
{
public:
	// System.Collections.Generic.List`1<PlayFab.ClientModels.FriendInfo> PlayFab.ClientModels.GetFriendsListResult::<Friends>k__BackingField
	List_1_t210693525 * ___U3CFriendsU3Ek__BackingField_2;

public:
	inline static int32_t get_offset_of_U3CFriendsU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(GetFriendsListResult_t2794171722, ___U3CFriendsU3Ek__BackingField_2)); }
	inline List_1_t210693525 * get_U3CFriendsU3Ek__BackingField_2() const { return ___U3CFriendsU3Ek__BackingField_2; }
	inline List_1_t210693525 ** get_address_of_U3CFriendsU3Ek__BackingField_2() { return &___U3CFriendsU3Ek__BackingField_2; }
	inline void set_U3CFriendsU3Ek__BackingField_2(List_1_t210693525 * value)
	{
		___U3CFriendsU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier(&___U3CFriendsU3Ek__BackingField_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
