﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Cell`1<System.Double>
struct Cell_1_t716614246;
// ICell`1<System.Double>
struct ICell_1_t2086147591;
// System.IDisposable
struct IDisposable_t1628921374;
// System.Action
struct Action_t437523947;
// System.Object
struct Il2CppObject;
// System.Action`1<System.Double>
struct Action_1_t682969319;
// IStream`1<System.Double>
struct IStream_1_t1087207605;
// UnityEngine.UI.Text
struct Text_t3286458198;

#include "codegen/il2cpp-codegen.h"
#include "System_Core_System_Action437523947.h"
#include "AssemblyU2DCSharp_Priority3194150340.h"
#include "UnityEngine_UI_UnityEngine_UI_Text3286458198.h"

// System.Void Cell`1<System.Double>::.ctor()
extern "C"  void Cell_1__ctor_m3106471796_gshared (Cell_1_t716614246 * __this, const MethodInfo* method);
#define Cell_1__ctor_m3106471796(__this, method) ((  void (*) (Cell_1_t716614246 *, const MethodInfo*))Cell_1__ctor_m3106471796_gshared)(__this, method)
// System.Void Cell`1<System.Double>::.ctor(T)
extern "C"  void Cell_1__ctor_m1811346538_gshared (Cell_1_t716614246 * __this, double ___initial0, const MethodInfo* method);
#define Cell_1__ctor_m1811346538(__this, ___initial0, method) ((  void (*) (Cell_1_t716614246 *, double, const MethodInfo*))Cell_1__ctor_m1811346538_gshared)(__this, ___initial0, method)
// System.Void Cell`1<System.Double>::.ctor(ICell`1<T>)
extern "C"  void Cell_1__ctor_m2187734826_gshared (Cell_1_t716614246 * __this, Il2CppObject* ___other0, const MethodInfo* method);
#define Cell_1__ctor_m2187734826(__this, ___other0, method) ((  void (*) (Cell_1_t716614246 *, Il2CppObject*, const MethodInfo*))Cell_1__ctor_m2187734826_gshared)(__this, ___other0, method)
// System.Void Cell`1<System.Double>::SetInputCell(ICell`1<T>)
extern "C"  void Cell_1_SetInputCell_m2840252432_gshared (Cell_1_t716614246 * __this, Il2CppObject* ___cell0, const MethodInfo* method);
#define Cell_1_SetInputCell_m2840252432(__this, ___cell0, method) ((  void (*) (Cell_1_t716614246 *, Il2CppObject*, const MethodInfo*))Cell_1_SetInputCell_m2840252432_gshared)(__this, ___cell0, method)
// T Cell`1<System.Double>::get_value()
extern "C"  double Cell_1_get_value_m3739324729_gshared (Cell_1_t716614246 * __this, const MethodInfo* method);
#define Cell_1_get_value_m3739324729(__this, method) ((  double (*) (Cell_1_t716614246 *, const MethodInfo*))Cell_1_get_value_m3739324729_gshared)(__this, method)
// System.Void Cell`1<System.Double>::set_value(T)
extern "C"  void Cell_1_set_value_m2175823768_gshared (Cell_1_t716614246 * __this, double ___value0, const MethodInfo* method);
#define Cell_1_set_value_m2175823768(__this, ___value0, method) ((  void (*) (Cell_1_t716614246 *, double, const MethodInfo*))Cell_1_set_value_m2175823768_gshared)(__this, ___value0, method)
// System.Void Cell`1<System.Double>::Set(T)
extern "C"  void Cell_1_Set_m4070759210_gshared (Cell_1_t716614246 * __this, double ___val0, const MethodInfo* method);
#define Cell_1_Set_m4070759210(__this, ___val0, method) ((  void (*) (Cell_1_t716614246 *, double, const MethodInfo*))Cell_1_Set_m4070759210_gshared)(__this, ___val0, method)
// System.IDisposable Cell`1<System.Double>::OnChanged(System.Action,Priority)
extern "C"  Il2CppObject * Cell_1_OnChanged_m1552633375_gshared (Cell_1_t716614246 * __this, Action_t437523947 * ___action0, int32_t ___p1, const MethodInfo* method);
#define Cell_1_OnChanged_m1552633375(__this, ___action0, ___p1, method) ((  Il2CppObject * (*) (Cell_1_t716614246 *, Action_t437523947 *, int32_t, const MethodInfo*))Cell_1_OnChanged_m1552633375_gshared)(__this, ___action0, ___p1, method)
// System.Object Cell`1<System.Double>::get_valueObject()
extern "C"  Il2CppObject * Cell_1_get_valueObject_m897220110_gshared (Cell_1_t716614246 * __this, const MethodInfo* method);
#define Cell_1_get_valueObject_m897220110(__this, method) ((  Il2CppObject * (*) (Cell_1_t716614246 *, const MethodInfo*))Cell_1_get_valueObject_m897220110_gshared)(__this, method)
// System.IDisposable Cell`1<System.Double>::ListenUpdates(System.Action`1<T>,Priority)
extern "C"  Il2CppObject * Cell_1_ListenUpdates_m3413929370_gshared (Cell_1_t716614246 * __this, Action_1_t682969319 * ___reaction0, int32_t ___p1, const MethodInfo* method);
#define Cell_1_ListenUpdates_m3413929370(__this, ___reaction0, ___p1, method) ((  Il2CppObject * (*) (Cell_1_t716614246 *, Action_1_t682969319 *, int32_t, const MethodInfo*))Cell_1_ListenUpdates_m3413929370_gshared)(__this, ___reaction0, ___p1, method)
// IStream`1<T> Cell`1<System.Double>::get_updates()
extern "C"  Il2CppObject* Cell_1_get_updates_m3305586980_gshared (Cell_1_t716614246 * __this, const MethodInfo* method);
#define Cell_1_get_updates_m3305586980(__this, method) ((  Il2CppObject* (*) (Cell_1_t716614246 *, const MethodInfo*))Cell_1_get_updates_m3305586980_gshared)(__this, method)
// System.IDisposable Cell`1<System.Double>::Bind(System.Action`1<T>,Priority)
extern "C"  Il2CppObject * Cell_1_Bind_m983731048_gshared (Cell_1_t716614246 * __this, Action_1_t682969319 * ___action0, int32_t ___p1, const MethodInfo* method);
#define Cell_1_Bind_m983731048(__this, ___action0, ___p1, method) ((  Il2CppObject * (*) (Cell_1_t716614246 *, Action_1_t682969319 *, int32_t, const MethodInfo*))Cell_1_Bind_m983731048_gshared)(__this, ___action0, ___p1, method)
// System.Void Cell`1<System.Double>::SetInputStream(IStream`1<T>)
extern "C"  void Cell_1_SetInputStream_m3828760844_gshared (Cell_1_t716614246 * __this, Il2CppObject* ___stream0, const MethodInfo* method);
#define Cell_1_SetInputStream_m3828760844(__this, ___stream0, method) ((  void (*) (Cell_1_t716614246 *, Il2CppObject*, const MethodInfo*))Cell_1_SetInputStream_m3828760844_gshared)(__this, ___stream0, method)
// System.Void Cell`1<System.Double>::ResetInputStream()
extern "C"  void Cell_1_ResetInputStream_m3430948139_gshared (Cell_1_t716614246 * __this, const MethodInfo* method);
#define Cell_1_ResetInputStream_m3430948139(__this, method) ((  void (*) (Cell_1_t716614246 *, const MethodInfo*))Cell_1_ResetInputStream_m3430948139_gshared)(__this, method)
// System.Void Cell`1<System.Double>::TransactionIterationFinished()
extern "C"  void Cell_1_TransactionIterationFinished_m944657729_gshared (Cell_1_t716614246 * __this, const MethodInfo* method);
#define Cell_1_TransactionIterationFinished_m944657729(__this, method) ((  void (*) (Cell_1_t716614246 *, const MethodInfo*))Cell_1_TransactionIterationFinished_m944657729_gshared)(__this, method)
// System.Void Cell`1<System.Double>::Unpack(System.Int32)
extern "C"  void Cell_1_Unpack_m86094227_gshared (Cell_1_t716614246 * __this, int32_t ___p0, const MethodInfo* method);
#define Cell_1_Unpack_m86094227(__this, ___p0, method) ((  void (*) (Cell_1_t716614246 *, int32_t, const MethodInfo*))Cell_1_Unpack_m86094227_gshared)(__this, ___p0, method)
// System.Boolean Cell`1<System.Double>::op_LessThanOrEqual(Cell`1<T>,UnityEngine.UI.Text)
extern "C"  bool Cell_1_op_LessThanOrEqual_m65828006_gshared (Il2CppObject * __this /* static, unused */, Cell_1_t716614246 * ___cell0, Text_t3286458198 * ___text1, const MethodInfo* method);
#define Cell_1_op_LessThanOrEqual_m65828006(__this /* static, unused */, ___cell0, ___text1, method) ((  bool (*) (Il2CppObject * /* static, unused */, Cell_1_t716614246 *, Text_t3286458198 *, const MethodInfo*))Cell_1_op_LessThanOrEqual_m65828006_gshared)(__this /* static, unused */, ___cell0, ___text1, method)
// System.Boolean Cell`1<System.Double>::op_GreaterThanOrEqual(Cell`1<T>,UnityEngine.UI.Text)
extern "C"  bool Cell_1_op_GreaterThanOrEqual_m2659237921_gshared (Il2CppObject * __this /* static, unused */, Cell_1_t716614246 * ___cell0, Text_t3286458198 * ___text1, const MethodInfo* method);
#define Cell_1_op_GreaterThanOrEqual_m2659237921(__this /* static, unused */, ___cell0, ___text1, method) ((  bool (*) (Il2CppObject * /* static, unused */, Cell_1_t716614246 *, Text_t3286458198 *, const MethodInfo*))Cell_1_op_GreaterThanOrEqual_m2659237921_gshared)(__this /* static, unused */, ___cell0, ___text1, method)
