﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.ClientModels.SteamPlayFabIdPair
struct SteamPlayFabIdPair_t2684316776;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String968488902.h"

// System.Void PlayFab.ClientModels.SteamPlayFabIdPair::.ctor()
extern "C"  void SteamPlayFabIdPair__ctor_m2335848021 (SteamPlayFabIdPair_t2684316776 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt64 PlayFab.ClientModels.SteamPlayFabIdPair::get_SteamId()
extern "C"  uint64_t SteamPlayFabIdPair_get_SteamId_m3335315121 (SteamPlayFabIdPair_t2684316776 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.SteamPlayFabIdPair::set_SteamId(System.UInt64)
extern "C"  void SteamPlayFabIdPair_set_SteamId_m3203979258 (SteamPlayFabIdPair_t2684316776 * __this, uint64_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PlayFab.ClientModels.SteamPlayFabIdPair::get_SteamStringId()
extern "C"  String_t* SteamPlayFabIdPair_get_SteamStringId_m1087141033 (SteamPlayFabIdPair_t2684316776 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.SteamPlayFabIdPair::set_SteamStringId(System.String)
extern "C"  void SteamPlayFabIdPair_set_SteamStringId_m1440820784 (SteamPlayFabIdPair_t2684316776 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PlayFab.ClientModels.SteamPlayFabIdPair::get_PlayFabId()
extern "C"  String_t* SteamPlayFabIdPair_get_PlayFabId_m3456150363 (SteamPlayFabIdPair_t2684316776 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.SteamPlayFabIdPair::set_PlayFabId(System.String)
extern "C"  void SteamPlayFabIdPair_set_PlayFabId_m3783662654 (SteamPlayFabIdPair_t2684316776 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
