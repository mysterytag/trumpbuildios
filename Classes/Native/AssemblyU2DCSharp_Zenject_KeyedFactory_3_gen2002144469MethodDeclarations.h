﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Zenject.KeyedFactory`3<System.Object,System.Object,System.Object>
struct KeyedFactory_3_t2002144469;
// System.Type[]
struct TypeU5BU5D_t3431720054;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"

// System.Void Zenject.KeyedFactory`3<System.Object,System.Object,System.Object>::.ctor()
extern "C"  void KeyedFactory_3__ctor_m2992082481_gshared (KeyedFactory_3_t2002144469 * __this, const MethodInfo* method);
#define KeyedFactory_3__ctor_m2992082481(__this, method) ((  void (*) (KeyedFactory_3_t2002144469 *, const MethodInfo*))KeyedFactory_3__ctor_m2992082481_gshared)(__this, method)
// System.Type[] Zenject.KeyedFactory`3<System.Object,System.Object,System.Object>::get_ProvidedTypes()
extern "C"  TypeU5BU5D_t3431720054* KeyedFactory_3_get_ProvidedTypes_m3041128372_gshared (KeyedFactory_3_t2002144469 * __this, const MethodInfo* method);
#define KeyedFactory_3_get_ProvidedTypes_m3041128372(__this, method) ((  TypeU5BU5D_t3431720054* (*) (KeyedFactory_3_t2002144469 *, const MethodInfo*))KeyedFactory_3_get_ProvidedTypes_m3041128372_gshared)(__this, method)
// TBase Zenject.KeyedFactory`3<System.Object,System.Object,System.Object>::Create(TKey,TParam1)
extern "C"  Il2CppObject * KeyedFactory_3_Create_m478300650_gshared (KeyedFactory_3_t2002144469 * __this, Il2CppObject * ___key0, Il2CppObject * ___param11, const MethodInfo* method);
#define KeyedFactory_3_Create_m478300650(__this, ___key0, ___param11, method) ((  Il2CppObject * (*) (KeyedFactory_3_t2002144469 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))KeyedFactory_3_Create_m478300650_gshared)(__this, ___key0, ___param11, method)
