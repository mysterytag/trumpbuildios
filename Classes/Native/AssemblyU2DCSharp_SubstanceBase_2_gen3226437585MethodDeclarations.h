﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_SubstanceBase_2_gen540235856MethodDeclarations.h"

// System.Void SubstanceBase`2<UniRx.InternalUtil.ImmutableList`1<System.Object>,System.Object>::.ctor()
#define SubstanceBase_2__ctor_m3329686792(__this, method) ((  void (*) (SubstanceBase_2_t3226437585 *, const MethodInfo*))SubstanceBase_2__ctor_m1606804868_gshared)(__this, method)
// System.Void SubstanceBase`2<UniRx.InternalUtil.ImmutableList`1<System.Object>,System.Object>::.ctor(T)
#define SubstanceBase_2__ctor_m141076822(__this, ___baseVal0, method) ((  void (*) (SubstanceBase_2_t3226437585 *, ImmutableList_1_t1897310919 *, const MethodInfo*))SubstanceBase_2__ctor_m2566312026_gshared)(__this, ___baseVal0, method)
// T SubstanceBase`2<UniRx.InternalUtil.ImmutableList`1<System.Object>,System.Object>::get_baseValue()
#define SubstanceBase_2_get_baseValue_m2979837790(__this, method) ((  ImmutableList_1_t1897310919 * (*) (SubstanceBase_2_t3226437585 *, const MethodInfo*))SubstanceBase_2_get_baseValue_m1000505434_gshared)(__this, method)
// System.Void SubstanceBase`2<UniRx.InternalUtil.ImmutableList`1<System.Object>,System.Object>::set_baseValue(T)
#define SubstanceBase_2_set_baseValue_m1695583349(__this, ___value0, method) ((  void (*) (SubstanceBase_2_t3226437585 *, ImmutableList_1_t1897310919 *, const MethodInfo*))SubstanceBase_2_set_baseValue_m1167683449_gshared)(__this, ___value0, method)
// System.Void SubstanceBase`2<UniRx.InternalUtil.ImmutableList`1<System.Object>,System.Object>::set_baseValueReactive(ICell`1<T>)
#define SubstanceBase_2_set_baseValueReactive_m4273098008(__this, ___value0, method) ((  void (*) (SubstanceBase_2_t3226437585 *, Il2CppObject*, const MethodInfo*))SubstanceBase_2_set_baseValueReactive_m2901627540_gshared)(__this, ___value0, method)
// System.IDisposable SubstanceBase`2<UniRx.InternalUtil.ImmutableList`1<System.Object>,System.Object>::Bind(System.Action`1<T>,Priority)
#define SubstanceBase_2_Bind_m3388020086(__this, ___action0, ___p1, method) ((  Il2CppObject * (*) (SubstanceBase_2_t3226437585 *, Action_1_t2045763624 *, int32_t, const MethodInfo*))SubstanceBase_2_Bind_m487587186_gshared)(__this, ___action0, ___p1, method)
// System.IDisposable SubstanceBase`2<UniRx.InternalUtil.ImmutableList`1<System.Object>,System.Object>::OnChanged(System.Action,Priority)
#define SubstanceBase_2_OnChanged_m3956922413(__this, ___action0, ___p1, method) ((  Il2CppObject * (*) (SubstanceBase_2_t3226437585 *, Action_t437523947 *, int32_t, const MethodInfo*))SubstanceBase_2_OnChanged_m1056489513_gshared)(__this, ___action0, ___p1, method)
// System.Object SubstanceBase`2<UniRx.InternalUtil.ImmutableList`1<System.Object>,System.Object>::get_valueObject()
#define SubstanceBase_2_get_valueObject_m3768972824(__this, method) ((  Il2CppObject * (*) (SubstanceBase_2_t3226437585 *, const MethodInfo*))SubstanceBase_2_get_valueObject_m2572102932_gshared)(__this, method)
// System.IDisposable SubstanceBase`2<UniRx.InternalUtil.ImmutableList`1<System.Object>,System.Object>::ListenUpdates(System.Action`1<T>,Priority)
#define SubstanceBase_2_ListenUpdates_m2263700300(__this, ___action0, ___p1, method) ((  Il2CppObject * (*) (SubstanceBase_2_t3226437585 *, Action_1_t2045763624 *, int32_t, const MethodInfo*))SubstanceBase_2_ListenUpdates_m2465048272_gshared)(__this, ___action0, ___p1, method)
// IStream`1<T> SubstanceBase`2<UniRx.InternalUtil.ImmutableList`1<System.Object>,System.Object>::get_updates()
#define SubstanceBase_2_get_updates_m2182205430(__this, method) ((  Il2CppObject* (*) (SubstanceBase_2_t3226437585 *, const MethodInfo*))SubstanceBase_2_get_updates_m1292576498_gshared)(__this, method)
// T SubstanceBase`2<UniRx.InternalUtil.ImmutableList`1<System.Object>,System.Object>::get_value()
#define SubstanceBase_2_get_value_m2243258863(__this, method) ((  ImmutableList_1_t1897310919 * (*) (SubstanceBase_2_t3226437585 *, const MethodInfo*))SubstanceBase_2_get_value_m3139375339_gshared)(__this, method)
// SubstanceBase`2/Intrusion<T,InfT> SubstanceBase`2<UniRx.InternalUtil.ImmutableList`1<System.Object>,System.Object>::AffectInner(ILiving,InfT)
#define SubstanceBase_2_AffectInner_m4050747305(__this, ___intruder0, ___influence1, method) ((  Intrusion_t1436088045 * (*) (SubstanceBase_2_t3226437585 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))SubstanceBase_2_AffectInner_m1664732325_gshared)(__this, ___intruder0, ___influence1, method)
// System.IDisposable SubstanceBase`2<UniRx.InternalUtil.ImmutableList`1<System.Object>,System.Object>::AffectUnsafe(InfT)
#define SubstanceBase_2_AffectUnsafe_m1226924567(__this, ___influence0, method) ((  Il2CppObject * (*) (SubstanceBase_2_t3226437585 *, Il2CppObject *, const MethodInfo*))SubstanceBase_2_AffectUnsafe_m1801912731_gshared)(__this, ___influence0, method)
// System.IDisposable SubstanceBase`2<UniRx.InternalUtil.ImmutableList`1<System.Object>,System.Object>::AffectUnsafe(ICell`1<InfT>)
#define SubstanceBase_2_AffectUnsafe_m2909028829(__this, ___influence0, method) ((  Il2CppObject * (*) (SubstanceBase_2_t3226437585 *, Il2CppObject*, const MethodInfo*))SubstanceBase_2_AffectUnsafe_m3688879833_gshared)(__this, ___influence0, method)
// System.IDisposable SubstanceBase`2<UniRx.InternalUtil.ImmutableList`1<System.Object>,System.Object>::Affect(ILiving,InfT)
#define SubstanceBase_2_Affect_m2458156599(__this, ___intruder0, ___influence1, method) ((  Il2CppObject * (*) (SubstanceBase_2_t3226437585 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))SubstanceBase_2_Affect_m971001019_gshared)(__this, ___intruder0, ___influence1, method)
// System.IDisposable SubstanceBase`2<UniRx.InternalUtil.ImmutableList`1<System.Object>,System.Object>::Affect(ILiving,ICell`1<InfT>)
#define SubstanceBase_2_Affect_m3572419005(__this, ___intruder0, ___influence1, method) ((  Il2CppObject * (*) (SubstanceBase_2_t3226437585 *, Il2CppObject *, Il2CppObject*, const MethodInfo*))SubstanceBase_2_Affect_m1389957049_gshared)(__this, ___intruder0, ___influence1, method)
// System.IDisposable SubstanceBase`2<UniRx.InternalUtil.ImmutableList`1<System.Object>,System.Object>::Affect(ILiving,InfT,IEmptyStream)
#define SubstanceBase_2_Affect_m2427826571(__this, ___intruder0, ___influence1, ___update2, method) ((  Il2CppObject * (*) (SubstanceBase_2_t3226437585 *, Il2CppObject *, Il2CppObject *, Il2CppObject *, const MethodInfo*))SubstanceBase_2_Affect_m3822360967_gshared)(__this, ___intruder0, ___influence1, ___update2, method)
// System.Void SubstanceBase`2<UniRx.InternalUtil.ImmutableList`1<System.Object>,System.Object>::OnAdd(SubstanceBase`2/Intrusion<T,InfT>)
#define SubstanceBase_2_OnAdd_m2063391720(__this, ___intrusion0, method) ((  void (*) (SubstanceBase_2_t3226437585 *, Intrusion_t1436088045 *, const MethodInfo*))SubstanceBase_2_OnAdd_m680391404_gshared)(__this, ___intrusion0, method)
// System.Void SubstanceBase`2<UniRx.InternalUtil.ImmutableList`1<System.Object>,System.Object>::OnRemove(SubstanceBase`2/Intrusion<T,InfT>)
#define SubstanceBase_2_OnRemove_m3228772977(__this, ___intrusion0, method) ((  void (*) (SubstanceBase_2_t3226437585 *, Intrusion_t1436088045 *, const MethodInfo*))SubstanceBase_2_OnRemove_m3887629549_gshared)(__this, ___intrusion0, method)
// System.Void SubstanceBase`2<UniRx.InternalUtil.ImmutableList`1<System.Object>,System.Object>::OnUpdate(SubstanceBase`2/Intrusion<T,InfT>)
#define SubstanceBase_2_OnUpdate_m2579504140(__this, ___intrusion0, method) ((  void (*) (SubstanceBase_2_t3226437585 *, Intrusion_t1436088045 *, const MethodInfo*))SubstanceBase_2_OnUpdate_m3238360712_gshared)(__this, ___intrusion0, method)
// System.Void SubstanceBase`2<UniRx.InternalUtil.ImmutableList`1<System.Object>,System.Object>::UpdateAll()
#define SubstanceBase_2_UpdateAll_m2156186046(__this, method) ((  void (*) (SubstanceBase_2_t3226437585 *, const MethodInfo*))SubstanceBase_2_UpdateAll_m2398295098_gshared)(__this, method)
// System.Void SubstanceBase`2<UniRx.InternalUtil.ImmutableList`1<System.Object>,System.Object>::ClearIntrusion(SubstanceBase`2/Intrusion<T,InfT>)
#define SubstanceBase_2_ClearIntrusion_m3849891386(__this, ___intr0, method) ((  void (*) (SubstanceBase_2_t3226437585 *, Intrusion_t1436088045 *, const MethodInfo*))SubstanceBase_2_ClearIntrusion_m1431780278_gshared)(__this, ___intr0, method)
// System.Void SubstanceBase`2<UniRx.InternalUtil.ImmutableList`1<System.Object>,System.Object>::<set_baseValueReactive>m__1D7(T)
#define SubstanceBase_2_U3Cset_baseValueReactiveU3Em__1D7_m2016800675(__this, ___val0, method) ((  void (*) (SubstanceBase_2_t3226437585 *, ImmutableList_1_t1897310919 *, const MethodInfo*))SubstanceBase_2_U3Cset_baseValueReactiveU3Em__1D7_m725633703_gshared)(__this, ___val0, method)
