﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.InternalUtil.ListObserver`1<UnityEngine.Rect>
struct ListObserver_1_t1820046488;
// UniRx.InternalUtil.ImmutableList`1<UniRx.IObserver`1<UnityEngine.Rect>>
struct ImmutableList_1_t502664923;
// System.Exception
struct Exception_t1967233988;
// UniRx.IObserver`1<UnityEngine.Rect>
struct IObserver_1_t3737427720;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Exception1967233988.h"
#include "UnityEngine_UnityEngine_Rect1525428817.h"

// System.Void UniRx.InternalUtil.ListObserver`1<UnityEngine.Rect>::.ctor(UniRx.InternalUtil.ImmutableList`1<UniRx.IObserver`1<T>>)
extern "C"  void ListObserver_1__ctor_m4048574726_gshared (ListObserver_1_t1820046488 * __this, ImmutableList_1_t502664923 * ___observers0, const MethodInfo* method);
#define ListObserver_1__ctor_m4048574726(__this, ___observers0, method) ((  void (*) (ListObserver_1_t1820046488 *, ImmutableList_1_t502664923 *, const MethodInfo*))ListObserver_1__ctor_m4048574726_gshared)(__this, ___observers0, method)
// System.Void UniRx.InternalUtil.ListObserver`1<UnityEngine.Rect>::OnCompleted()
extern "C"  void ListObserver_1_OnCompleted_m162918590_gshared (ListObserver_1_t1820046488 * __this, const MethodInfo* method);
#define ListObserver_1_OnCompleted_m162918590(__this, method) ((  void (*) (ListObserver_1_t1820046488 *, const MethodInfo*))ListObserver_1_OnCompleted_m162918590_gshared)(__this, method)
// System.Void UniRx.InternalUtil.ListObserver`1<UnityEngine.Rect>::OnError(System.Exception)
extern "C"  void ListObserver_1_OnError_m439842027_gshared (ListObserver_1_t1820046488 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method);
#define ListObserver_1_OnError_m439842027(__this, ___error0, method) ((  void (*) (ListObserver_1_t1820046488 *, Exception_t1967233988 *, const MethodInfo*))ListObserver_1_OnError_m439842027_gshared)(__this, ___error0, method)
// System.Void UniRx.InternalUtil.ListObserver`1<UnityEngine.Rect>::OnNext(T)
extern "C"  void ListObserver_1_OnNext_m1513787868_gshared (ListObserver_1_t1820046488 * __this, Rect_t1525428817  ___value0, const MethodInfo* method);
#define ListObserver_1_OnNext_m1513787868(__this, ___value0, method) ((  void (*) (ListObserver_1_t1820046488 *, Rect_t1525428817 , const MethodInfo*))ListObserver_1_OnNext_m1513787868_gshared)(__this, ___value0, method)
// UniRx.IObserver`1<T> UniRx.InternalUtil.ListObserver`1<UnityEngine.Rect>::Add(UniRx.IObserver`1<T>)
extern "C"  Il2CppObject* ListObserver_1_Add_m3816105788_gshared (ListObserver_1_t1820046488 * __this, Il2CppObject* ___observer0, const MethodInfo* method);
#define ListObserver_1_Add_m3816105788(__this, ___observer0, method) ((  Il2CppObject* (*) (ListObserver_1_t1820046488 *, Il2CppObject*, const MethodInfo*))ListObserver_1_Add_m3816105788_gshared)(__this, ___observer0, method)
// UniRx.IObserver`1<T> UniRx.InternalUtil.ListObserver`1<UnityEngine.Rect>::Remove(UniRx.IObserver`1<T>)
extern "C"  Il2CppObject* ListObserver_1_Remove_m2551618191_gshared (ListObserver_1_t1820046488 * __this, Il2CppObject* ___observer0, const MethodInfo* method);
#define ListObserver_1_Remove_m2551618191(__this, ___observer0, method) ((  Il2CppObject* (*) (ListObserver_1_t1820046488 *, Il2CppObject*, const MethodInfo*))ListObserver_1_Remove_m2551618191_gshared)(__this, ___observer0, method)
