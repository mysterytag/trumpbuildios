﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Zenject.Factory`2<System.Object,System.Object>
struct Factory_2_t2762000298;
// System.Type
struct Type_t;
// System.Type[]
struct TypeU5BU5D_t3431720054;
// Zenject.DiContainer
struct DiContainer_t2383114449;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"

// System.Void Zenject.Factory`2<System.Object,System.Object>::.ctor()
extern "C"  void Factory_2__ctor_m4224559918_gshared (Factory_2_t2762000298 * __this, const MethodInfo* method);
#define Factory_2__ctor_m4224559918(__this, method) ((  void (*) (Factory_2_t2762000298 *, const MethodInfo*))Factory_2__ctor_m4224559918_gshared)(__this, method)
// System.Type Zenject.Factory`2<System.Object,System.Object>::get_ConstructedType()
extern "C"  Type_t * Factory_2_get_ConstructedType_m3202430105_gshared (Factory_2_t2762000298 * __this, const MethodInfo* method);
#define Factory_2_get_ConstructedType_m3202430105(__this, method) ((  Type_t * (*) (Factory_2_t2762000298 *, const MethodInfo*))Factory_2_get_ConstructedType_m3202430105_gshared)(__this, method)
// System.Type[] Zenject.Factory`2<System.Object,System.Object>::get_ProvidedTypes()
extern "C"  TypeU5BU5D_t3431720054* Factory_2_get_ProvidedTypes_m1772732737_gshared (Factory_2_t2762000298 * __this, const MethodInfo* method);
#define Factory_2_get_ProvidedTypes_m1772732737(__this, method) ((  TypeU5BU5D_t3431720054* (*) (Factory_2_t2762000298 *, const MethodInfo*))Factory_2_get_ProvidedTypes_m1772732737_gshared)(__this, method)
// Zenject.DiContainer Zenject.Factory`2<System.Object,System.Object>::get_Container()
extern "C"  DiContainer_t2383114449 * Factory_2_get_Container_m168715534_gshared (Factory_2_t2762000298 * __this, const MethodInfo* method);
#define Factory_2_get_Container_m168715534(__this, method) ((  DiContainer_t2383114449 * (*) (Factory_2_t2762000298 *, const MethodInfo*))Factory_2_get_Container_m168715534_gshared)(__this, method)
// TValue Zenject.Factory`2<System.Object,System.Object>::Create(TParam1)
extern "C"  Il2CppObject * Factory_2_Create_m1691441992_gshared (Factory_2_t2762000298 * __this, Il2CppObject * ___param0, const MethodInfo* method);
#define Factory_2_Create_m1691441992(__this, ___param0, method) ((  Il2CppObject * (*) (Factory_2_t2762000298 *, Il2CppObject *, const MethodInfo*))Factory_2_Create_m1691441992_gshared)(__this, ___param0, method)
