﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Object
struct Il2CppObject;
// GlobalStat
struct GlobalStat_t1134678199;
// System.Predicate`1<System.Int64>
struct Predicate_1_t3418378780;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GlobalStat/<DoubleRevenueListCheck>c__Iterator18
struct  U3CDoubleRevenueListCheckU3Ec__Iterator18_t3755667413  : public Il2CppObject
{
public:
	// System.Int64 GlobalStat/<DoubleRevenueListCheck>c__Iterator18::<s>__0
	int64_t ___U3CsU3E__0_0;
	// System.Int32 GlobalStat/<DoubleRevenueListCheck>c__Iterator18::<cel>__1
	int32_t ___U3CcelU3E__1_1;
	// System.Int32 GlobalStat/<DoubleRevenueListCheck>c__Iterator18::$PC
	int32_t ___U24PC_2;
	// System.Object GlobalStat/<DoubleRevenueListCheck>c__Iterator18::$current
	Il2CppObject * ___U24current_3;
	// GlobalStat GlobalStat/<DoubleRevenueListCheck>c__Iterator18::<>f__this
	GlobalStat_t1134678199 * ___U3CU3Ef__this_4;

public:
	inline static int32_t get_offset_of_U3CsU3E__0_0() { return static_cast<int32_t>(offsetof(U3CDoubleRevenueListCheckU3Ec__Iterator18_t3755667413, ___U3CsU3E__0_0)); }
	inline int64_t get_U3CsU3E__0_0() const { return ___U3CsU3E__0_0; }
	inline int64_t* get_address_of_U3CsU3E__0_0() { return &___U3CsU3E__0_0; }
	inline void set_U3CsU3E__0_0(int64_t value)
	{
		___U3CsU3E__0_0 = value;
	}

	inline static int32_t get_offset_of_U3CcelU3E__1_1() { return static_cast<int32_t>(offsetof(U3CDoubleRevenueListCheckU3Ec__Iterator18_t3755667413, ___U3CcelU3E__1_1)); }
	inline int32_t get_U3CcelU3E__1_1() const { return ___U3CcelU3E__1_1; }
	inline int32_t* get_address_of_U3CcelU3E__1_1() { return &___U3CcelU3E__1_1; }
	inline void set_U3CcelU3E__1_1(int32_t value)
	{
		___U3CcelU3E__1_1 = value;
	}

	inline static int32_t get_offset_of_U24PC_2() { return static_cast<int32_t>(offsetof(U3CDoubleRevenueListCheckU3Ec__Iterator18_t3755667413, ___U24PC_2)); }
	inline int32_t get_U24PC_2() const { return ___U24PC_2; }
	inline int32_t* get_address_of_U24PC_2() { return &___U24PC_2; }
	inline void set_U24PC_2(int32_t value)
	{
		___U24PC_2 = value;
	}

	inline static int32_t get_offset_of_U24current_3() { return static_cast<int32_t>(offsetof(U3CDoubleRevenueListCheckU3Ec__Iterator18_t3755667413, ___U24current_3)); }
	inline Il2CppObject * get_U24current_3() const { return ___U24current_3; }
	inline Il2CppObject ** get_address_of_U24current_3() { return &___U24current_3; }
	inline void set_U24current_3(Il2CppObject * value)
	{
		___U24current_3 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_3, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__this_4() { return static_cast<int32_t>(offsetof(U3CDoubleRevenueListCheckU3Ec__Iterator18_t3755667413, ___U3CU3Ef__this_4)); }
	inline GlobalStat_t1134678199 * get_U3CU3Ef__this_4() const { return ___U3CU3Ef__this_4; }
	inline GlobalStat_t1134678199 ** get_address_of_U3CU3Ef__this_4() { return &___U3CU3Ef__this_4; }
	inline void set_U3CU3Ef__this_4(GlobalStat_t1134678199 * value)
	{
		___U3CU3Ef__this_4 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__this_4, value);
	}
};

struct U3CDoubleRevenueListCheckU3Ec__Iterator18_t3755667413_StaticFields
{
public:
	// System.Predicate`1<System.Int64> GlobalStat/<DoubleRevenueListCheck>c__Iterator18::<>f__am$cache5
	Predicate_1_t3418378780 * ___U3CU3Ef__amU24cache5_5;
	// System.Predicate`1<System.Int64> GlobalStat/<DoubleRevenueListCheck>c__Iterator18::<>f__am$cache6
	Predicate_1_t3418378780 * ___U3CU3Ef__amU24cache6_6;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache5_5() { return static_cast<int32_t>(offsetof(U3CDoubleRevenueListCheckU3Ec__Iterator18_t3755667413_StaticFields, ___U3CU3Ef__amU24cache5_5)); }
	inline Predicate_1_t3418378780 * get_U3CU3Ef__amU24cache5_5() const { return ___U3CU3Ef__amU24cache5_5; }
	inline Predicate_1_t3418378780 ** get_address_of_U3CU3Ef__amU24cache5_5() { return &___U3CU3Ef__amU24cache5_5; }
	inline void set_U3CU3Ef__amU24cache5_5(Predicate_1_t3418378780 * value)
	{
		___U3CU3Ef__amU24cache5_5 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache5_5, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache6_6() { return static_cast<int32_t>(offsetof(U3CDoubleRevenueListCheckU3Ec__Iterator18_t3755667413_StaticFields, ___U3CU3Ef__amU24cache6_6)); }
	inline Predicate_1_t3418378780 * get_U3CU3Ef__amU24cache6_6() const { return ___U3CU3Ef__amU24cache6_6; }
	inline Predicate_1_t3418378780 ** get_address_of_U3CU3Ef__amU24cache6_6() { return &___U3CU3Ef__amU24cache6_6; }
	inline void set_U3CU3Ef__amU24cache6_6(Predicate_1_t3418378780 * value)
	{
		___U3CU3Ef__amU24cache6_6 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache6_6, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
