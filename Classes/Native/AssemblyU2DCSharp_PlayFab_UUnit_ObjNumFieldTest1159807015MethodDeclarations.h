﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.UUnit.ObjNumFieldTest
struct ObjNumFieldTest_t1159807015;

#include "codegen/il2cpp-codegen.h"

// System.Void PlayFab.UUnit.ObjNumFieldTest::.ctor()
extern "C"  void ObjNumFieldTest__ctor_m4230728414 (ObjNumFieldTest_t1159807015 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.UUnit.ObjNumFieldTest::.cctor()
extern "C"  void ObjNumFieldTest__cctor_m1821465743 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
