﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.PlayFabClientAPI/AddSharedGroupMembersRequestCallback
struct AddSharedGroupMembersRequestCallback_t3768324820;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;
// PlayFab.ClientModels.AddSharedGroupMembersRequest
struct AddSharedGroupMembersRequest_t3205265791;
// System.IAsyncResult
struct IAsyncResult_t537683269;
// System.AsyncCallback
struct AsyncCallback_t1363551830;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_IntPtr676692020.h"
#include "mscorlib_System_String968488902.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_AddSharedGr3205265791.h"
#include "mscorlib_System_AsyncCallback1363551830.h"

// System.Void PlayFab.PlayFabClientAPI/AddSharedGroupMembersRequestCallback::.ctor(System.Object,System.IntPtr)
extern "C"  void AddSharedGroupMembersRequestCallback__ctor_m2913509827 (AddSharedGroupMembersRequestCallback_t3768324820 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/AddSharedGroupMembersRequestCallback::Invoke(System.String,System.Int32,PlayFab.ClientModels.AddSharedGroupMembersRequest,System.Object)
extern "C"  void AddSharedGroupMembersRequestCallback_Invoke_m571298015 (AddSharedGroupMembersRequestCallback_t3768324820 * __this, String_t* ___urlPath0, int32_t ___callId1, AddSharedGroupMembersRequest_t3205265791 * ___request2, Il2CppObject * ___customData3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
extern "C" void pinvoke_delegate_wrapper_AddSharedGroupMembersRequestCallback_t3768324820(Il2CppObject* delegate, String_t* ___urlPath0, int32_t ___callId1, AddSharedGroupMembersRequest_t3205265791 * ___request2, Il2CppObject * ___customData3);
// System.IAsyncResult PlayFab.PlayFabClientAPI/AddSharedGroupMembersRequestCallback::BeginInvoke(System.String,System.Int32,PlayFab.ClientModels.AddSharedGroupMembersRequest,System.Object,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * AddSharedGroupMembersRequestCallback_BeginInvoke_m2900689038 (AddSharedGroupMembersRequestCallback_t3768324820 * __this, String_t* ___urlPath0, int32_t ___callId1, AddSharedGroupMembersRequest_t3205265791 * ___request2, Il2CppObject * ___customData3, AsyncCallback_t1363551830 * ___callback4, Il2CppObject * ___object5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/AddSharedGroupMembersRequestCallback::EndInvoke(System.IAsyncResult)
extern "C"  void AddSharedGroupMembersRequestCallback_EndInvoke_m1069641043 (AddSharedGroupMembersRequestCallback_t3768324820 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
