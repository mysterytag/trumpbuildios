﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Zenject.InstantiateUtil/<PopValueWithType>c__AnonStorey180
struct U3CPopValueWithTypeU3Ec__AnonStorey180_t578279556;
// Zenject.TypeValuePair
struct TypeValuePair_t620932390;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Zenject_TypeValuePair620932390.h"

// System.Void Zenject.InstantiateUtil/<PopValueWithType>c__AnonStorey180::.ctor()
extern "C"  void U3CPopValueWithTypeU3Ec__AnonStorey180__ctor_m3754869621 (U3CPopValueWithTypeU3Ec__AnonStorey180_t578279556 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Zenject.InstantiateUtil/<PopValueWithType>c__AnonStorey180::<>m__2AB(Zenject.TypeValuePair)
extern "C"  bool U3CPopValueWithTypeU3Ec__AnonStorey180_U3CU3Em__2AB_m2532045429 (U3CPopValueWithTypeU3Ec__AnonStorey180_t578279556 * __this, TypeValuePair_t620932390 * ___x0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
