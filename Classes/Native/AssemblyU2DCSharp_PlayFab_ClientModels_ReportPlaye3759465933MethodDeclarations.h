﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.ClientModels.ReportPlayerClientResult
struct ReportPlayerClientResult_t3759465933;

#include "codegen/il2cpp-codegen.h"

// System.Void PlayFab.ClientModels.ReportPlayerClientResult::.ctor()
extern "C"  void ReportPlayerClientResult__ctor_m2171598608 (ReportPlayerClientResult_t3759465933 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PlayFab.ClientModels.ReportPlayerClientResult::get_Updated()
extern "C"  bool ReportPlayerClientResult_get_Updated_m2358293364 (ReportPlayerClientResult_t3759465933 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.ReportPlayerClientResult::set_Updated(System.Boolean)
extern "C"  void ReportPlayerClientResult_set_Updated_m2295634243 (ReportPlayerClientResult_t3759465933 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 PlayFab.ClientModels.ReportPlayerClientResult::get_SubmissionsRemaining()
extern "C"  int32_t ReportPlayerClientResult_get_SubmissionsRemaining_m3549486270 (ReportPlayerClientResult_t3759465933 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.ReportPlayerClientResult::set_SubmissionsRemaining(System.Int32)
extern "C"  void ReportPlayerClientResult_set_SubmissionsRemaining_m2672490961 (ReportPlayerClientResult_t3759465933 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
