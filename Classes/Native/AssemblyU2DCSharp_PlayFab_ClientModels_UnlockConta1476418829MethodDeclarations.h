﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.ClientModels.UnlockContainerInstanceRequest
struct UnlockContainerInstanceRequest_t1476418829;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String968488902.h"

// System.Void PlayFab.ClientModels.UnlockContainerInstanceRequest::.ctor()
extern "C"  void UnlockContainerInstanceRequest__ctor_m523485264 (UnlockContainerInstanceRequest_t1476418829 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PlayFab.ClientModels.UnlockContainerInstanceRequest::get_CharacterId()
extern "C"  String_t* UnlockContainerInstanceRequest_get_CharacterId_m2341430540 (UnlockContainerInstanceRequest_t1476418829 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.UnlockContainerInstanceRequest::set_CharacterId(System.String)
extern "C"  void UnlockContainerInstanceRequest_set_CharacterId_m3551914989 (UnlockContainerInstanceRequest_t1476418829 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PlayFab.ClientModels.UnlockContainerInstanceRequest::get_ContainerItemInstanceId()
extern "C"  String_t* UnlockContainerInstanceRequest_get_ContainerItemInstanceId_m2253897548 (UnlockContainerInstanceRequest_t1476418829 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.UnlockContainerInstanceRequest::set_ContainerItemInstanceId(System.String)
extern "C"  void UnlockContainerInstanceRequest_set_ContainerItemInstanceId_m2095922989 (UnlockContainerInstanceRequest_t1476418829 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PlayFab.ClientModels.UnlockContainerInstanceRequest::get_KeyItemInstanceId()
extern "C"  String_t* UnlockContainerInstanceRequest_get_KeyItemInstanceId_m1815916138 (UnlockContainerInstanceRequest_t1476418829 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.UnlockContainerInstanceRequest::set_KeyItemInstanceId(System.String)
extern "C"  void UnlockContainerInstanceRequest_set_KeyItemInstanceId_m4270741007 (UnlockContainerInstanceRequest_t1476418829 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PlayFab.ClientModels.UnlockContainerInstanceRequest::get_CatalogVersion()
extern "C"  String_t* UnlockContainerInstanceRequest_get_CatalogVersion_m3273531257 (UnlockContainerInstanceRequest_t1476418829 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.UnlockContainerInstanceRequest::set_CatalogVersion(System.String)
extern "C"  void UnlockContainerInstanceRequest_set_CatalogVersion_m206136914 (UnlockContainerInstanceRequest_t1476418829 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
