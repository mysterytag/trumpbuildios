﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UIHidder
struct UIHidder_t339629410;
// System.Collections.IEnumerator
struct IEnumerator_t287207039;

#include "codegen/il2cpp-codegen.h"

// System.Void UIHidder::.ctor()
extern "C"  void UIHidder__ctor_m3232716665 (UIHidder_t339629410 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIHidder::.cctor()
extern "C"  void UIHidder__cctor_m947872596 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIHidder::StartMultiplierWatcher()
extern "C"  void UIHidder_StartMultiplierWatcher_m1445865060 (UIHidder_t339629410 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator UIHidder::WatchMultiplier()
extern "C"  Il2CppObject * UIHidder_WatchMultiplier_m1058732479 (UIHidder_t339629410 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIHidder::StartMenuWatcher()
extern "C"  void UIHidder_StartMenuWatcher_m1931144838 (UIHidder_t339629410 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator UIHidder::Move(System.Single)
extern "C"  Il2CppObject * UIHidder_Move_m2294348711 (UIHidder_t339629410 * __this, float ___targetVal0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIHidder::Hide()
extern "C"  void UIHidder_Hide_m3624638317 (UIHidder_t339629410 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIHidder::Show()
extern "C"  void UIHidder_Show_m3938980456 (UIHidder_t339629410 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
