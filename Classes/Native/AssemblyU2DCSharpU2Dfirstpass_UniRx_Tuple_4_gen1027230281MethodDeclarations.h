﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Object
struct Il2CppObject;
// System.Collections.IComparer
struct IComparer_t2207526184;
// System.Collections.IEqualityComparer
struct IEqualityComparer_t1661793090;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Tuple_4_gen1027230281.h"
#include "mscorlib_System_Object837106420.h"

// System.Void UniRx.Tuple`4<System.Object,System.Object,System.Object,System.Object>::.ctor(T1,T2,T3,T4)
extern "C"  void Tuple_4__ctor_m138252759_gshared (Tuple_4_t1027230281 * __this, Il2CppObject * ___item10, Il2CppObject * ___item21, Il2CppObject * ___item32, Il2CppObject * ___item43, const MethodInfo* method);
#define Tuple_4__ctor_m138252759(__this, ___item10, ___item21, ___item32, ___item43, method) ((  void (*) (Tuple_4_t1027230281 *, Il2CppObject *, Il2CppObject *, Il2CppObject *, Il2CppObject *, const MethodInfo*))Tuple_4__ctor_m138252759_gshared)(__this, ___item10, ___item21, ___item32, ___item43, method)
// System.Int32 UniRx.Tuple`4<System.Object,System.Object,System.Object,System.Object>::System.IComparable.CompareTo(System.Object)
extern "C"  int32_t Tuple_4_System_IComparable_CompareTo_m1135267505_gshared (Tuple_4_t1027230281 * __this, Il2CppObject * ___obj0, const MethodInfo* method);
#define Tuple_4_System_IComparable_CompareTo_m1135267505(__this, ___obj0, method) ((  int32_t (*) (Tuple_4_t1027230281 *, Il2CppObject *, const MethodInfo*))Tuple_4_System_IComparable_CompareTo_m1135267505_gshared)(__this, ___obj0, method)
// System.Int32 UniRx.Tuple`4<System.Object,System.Object,System.Object,System.Object>::UniRx.IStructuralComparable.CompareTo(System.Object,System.Collections.IComparer)
extern "C"  int32_t Tuple_4_UniRx_IStructuralComparable_CompareTo_m2416851651_gshared (Tuple_4_t1027230281 * __this, Il2CppObject * ___other0, Il2CppObject * ___comparer1, const MethodInfo* method);
#define Tuple_4_UniRx_IStructuralComparable_CompareTo_m2416851651(__this, ___other0, ___comparer1, method) ((  int32_t (*) (Tuple_4_t1027230281 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))Tuple_4_UniRx_IStructuralComparable_CompareTo_m2416851651_gshared)(__this, ___other0, ___comparer1, method)
// System.Boolean UniRx.Tuple`4<System.Object,System.Object,System.Object,System.Object>::UniRx.IStructuralEquatable.Equals(System.Object,System.Collections.IEqualityComparer)
extern "C"  bool Tuple_4_UniRx_IStructuralEquatable_Equals_m1489759150_gshared (Tuple_4_t1027230281 * __this, Il2CppObject * ___other0, Il2CppObject * ___comparer1, const MethodInfo* method);
#define Tuple_4_UniRx_IStructuralEquatable_Equals_m1489759150(__this, ___other0, ___comparer1, method) ((  bool (*) (Tuple_4_t1027230281 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))Tuple_4_UniRx_IStructuralEquatable_Equals_m1489759150_gshared)(__this, ___other0, ___comparer1, method)
// System.Int32 UniRx.Tuple`4<System.Object,System.Object,System.Object,System.Object>::UniRx.IStructuralEquatable.GetHashCode(System.Collections.IEqualityComparer)
extern "C"  int32_t Tuple_4_UniRx_IStructuralEquatable_GetHashCode_m2546925248_gshared (Tuple_4_t1027230281 * __this, Il2CppObject * ___comparer0, const MethodInfo* method);
#define Tuple_4_UniRx_IStructuralEquatable_GetHashCode_m2546925248(__this, ___comparer0, method) ((  int32_t (*) (Tuple_4_t1027230281 *, Il2CppObject *, const MethodInfo*))Tuple_4_UniRx_IStructuralEquatable_GetHashCode_m2546925248_gshared)(__this, ___comparer0, method)
// System.String UniRx.Tuple`4<System.Object,System.Object,System.Object,System.Object>::UniRx.ITuple.ToString()
extern "C"  String_t* Tuple_4_UniRx_ITuple_ToString_m3290986539_gshared (Tuple_4_t1027230281 * __this, const MethodInfo* method);
#define Tuple_4_UniRx_ITuple_ToString_m3290986539(__this, method) ((  String_t* (*) (Tuple_4_t1027230281 *, const MethodInfo*))Tuple_4_UniRx_ITuple_ToString_m3290986539_gshared)(__this, method)
// T1 UniRx.Tuple`4<System.Object,System.Object,System.Object,System.Object>::get_Item1()
extern "C"  Il2CppObject * Tuple_4_get_Item1_m442592216_gshared (Tuple_4_t1027230281 * __this, const MethodInfo* method);
#define Tuple_4_get_Item1_m442592216(__this, method) ((  Il2CppObject * (*) (Tuple_4_t1027230281 *, const MethodInfo*))Tuple_4_get_Item1_m442592216_gshared)(__this, method)
// T2 UniRx.Tuple`4<System.Object,System.Object,System.Object,System.Object>::get_Item2()
extern "C"  Il2CppObject * Tuple_4_get_Item2_m3528318234_gshared (Tuple_4_t1027230281 * __this, const MethodInfo* method);
#define Tuple_4_get_Item2_m3528318234(__this, method) ((  Il2CppObject * (*) (Tuple_4_t1027230281 *, const MethodInfo*))Tuple_4_get_Item2_m3528318234_gshared)(__this, method)
// T3 UniRx.Tuple`4<System.Object,System.Object,System.Object,System.Object>::get_Item3()
extern "C"  Il2CppObject * Tuple_4_get_Item3_m2319076956_gshared (Tuple_4_t1027230281 * __this, const MethodInfo* method);
#define Tuple_4_get_Item3_m2319076956(__this, method) ((  Il2CppObject * (*) (Tuple_4_t1027230281 *, const MethodInfo*))Tuple_4_get_Item3_m2319076956_gshared)(__this, method)
// T4 UniRx.Tuple`4<System.Object,System.Object,System.Object,System.Object>::get_Item4()
extern "C"  Il2CppObject * Tuple_4_get_Item4_m1109835678_gshared (Tuple_4_t1027230281 * __this, const MethodInfo* method);
#define Tuple_4_get_Item4_m1109835678(__this, method) ((  Il2CppObject * (*) (Tuple_4_t1027230281 *, const MethodInfo*))Tuple_4_get_Item4_m1109835678_gshared)(__this, method)
// System.Boolean UniRx.Tuple`4<System.Object,System.Object,System.Object,System.Object>::Equals(System.Object)
extern "C"  bool Tuple_4_Equals_m3074385506_gshared (Tuple_4_t1027230281 * __this, Il2CppObject * ___obj0, const MethodInfo* method);
#define Tuple_4_Equals_m3074385506(__this, ___obj0, method) ((  bool (*) (Tuple_4_t1027230281 *, Il2CppObject *, const MethodInfo*))Tuple_4_Equals_m3074385506_gshared)(__this, ___obj0, method)
// System.Int32 UniRx.Tuple`4<System.Object,System.Object,System.Object,System.Object>::GetHashCode()
extern "C"  int32_t Tuple_4_GetHashCode_m3381554694_gshared (Tuple_4_t1027230281 * __this, const MethodInfo* method);
#define Tuple_4_GetHashCode_m3381554694(__this, method) ((  int32_t (*) (Tuple_4_t1027230281 *, const MethodInfo*))Tuple_4_GetHashCode_m3381554694_gshared)(__this, method)
// System.String UniRx.Tuple`4<System.Object,System.Object,System.Object,System.Object>::ToString()
extern "C"  String_t* Tuple_4_ToString_m2316663078_gshared (Tuple_4_t1027230281 * __this, const MethodInfo* method);
#define Tuple_4_ToString_m2316663078(__this, method) ((  String_t* (*) (Tuple_4_t1027230281 *, const MethodInfo*))Tuple_4_ToString_m2316663078_gshared)(__this, method)
// System.Boolean UniRx.Tuple`4<System.Object,System.Object,System.Object,System.Object>::Equals(UniRx.Tuple`4<T1,T2,T3,T4>)
extern "C"  bool Tuple_4_Equals_m4043923114_gshared (Tuple_4_t1027230281 * __this, Tuple_4_t1027230281  ___other0, const MethodInfo* method);
#define Tuple_4_Equals_m4043923114(__this, ___other0, method) ((  bool (*) (Tuple_4_t1027230281 *, Tuple_4_t1027230281 , const MethodInfo*))Tuple_4_Equals_m4043923114_gshared)(__this, ___other0, method)
