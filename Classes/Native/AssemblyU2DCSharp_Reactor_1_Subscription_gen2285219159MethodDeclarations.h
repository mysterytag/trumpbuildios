﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Reactor`1/Subscription<System.Boolean>
struct Subscription_t2285219160;

#include "codegen/il2cpp-codegen.h"

// System.Void Reactor`1/Subscription<System.Boolean>::.ctor()
extern "C"  void Subscription__ctor_m1613157801_gshared (Subscription_t2285219160 * __this, const MethodInfo* method);
#define Subscription__ctor_m1613157801(__this, method) ((  void (*) (Subscription_t2285219160 *, const MethodInfo*))Subscription__ctor_m1613157801_gshared)(__this, method)
// System.Void Reactor`1/Subscription<System.Boolean>::Dispose()
extern "C"  void Subscription_Dispose_m3954703270_gshared (Subscription_t2285219160 * __this, const MethodInfo* method);
#define Subscription_Dispose_m3954703270(__this, method) ((  void (*) (Subscription_t2285219160 *, const MethodInfo*))Subscription_Dispose_m3954703270_gshared)(__this, method)
