﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GameDataLoader
struct GameDataLoader_t3216331279;
// GameData
struct GameData_t2589969116;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_GameData2589969116.h"

// System.Void GameDataLoader::.ctor()
extern "C"  void GameDataLoader__ctor_m1485462380 (GameDataLoader_t3216331279 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// GameData GameDataLoader::getGameData()
extern "C"  GameData_t2589969116 * GameDataLoader_getGameData_m3372961755 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameDataLoader::saveGameData(GameData)
extern "C"  void GameDataLoader_saveGameData_m2810009333 (Il2CppObject * __this /* static, unused */, GameData_t2589969116 * ___d0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
