﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// AnonymousCell`1/<Listen>c__AnonStoreyF9<System.Object>
struct U3CListenU3Ec__AnonStoreyF9_t2554166976;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"

// System.Void AnonymousCell`1/<Listen>c__AnonStoreyF9<System.Object>::.ctor()
extern "C"  void U3CListenU3Ec__AnonStoreyF9__ctor_m1046847151_gshared (U3CListenU3Ec__AnonStoreyF9_t2554166976 * __this, const MethodInfo* method);
#define U3CListenU3Ec__AnonStoreyF9__ctor_m1046847151(__this, method) ((  void (*) (U3CListenU3Ec__AnonStoreyF9_t2554166976 *, const MethodInfo*))U3CListenU3Ec__AnonStoreyF9__ctor_m1046847151_gshared)(__this, method)
// System.Void AnonymousCell`1/<Listen>c__AnonStoreyF9<System.Object>::<>m__16F(T)
extern "C"  void U3CListenU3Ec__AnonStoreyF9_U3CU3Em__16F_m2904039699_gshared (U3CListenU3Ec__AnonStoreyF9_t2554166976 * __this, Il2CppObject * ____0, const MethodInfo* method);
#define U3CListenU3Ec__AnonStoreyF9_U3CU3Em__16F_m2904039699(__this, ____0, method) ((  void (*) (U3CListenU3Ec__AnonStoreyF9_t2554166976 *, Il2CppObject *, const MethodInfo*))U3CListenU3Ec__AnonStoreyF9_U3CU3Em__16F_m2904039699_gshared)(__this, ____0, method)
