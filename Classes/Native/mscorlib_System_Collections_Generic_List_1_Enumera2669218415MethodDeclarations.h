﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera4014815677MethodDeclarations.h"

// System.Void System.Collections.Generic.List`1/Enumerator<DG.Tweening.TweenCallback>::.ctor(System.Collections.Generic.List`1<T>)
#define Enumerator__ctor_m1254696555(__this, ___l0, method) ((  void (*) (Enumerator_t2669218415 *, List_1_t288468127 *, const MethodInfo*))Enumerator__ctor_m1029849669_gshared)(__this, ___l0, method)
// System.Void System.Collections.Generic.List`1/Enumerator<DG.Tweening.TweenCallback>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m2080134919(__this, method) ((  void (*) (Enumerator_t2669218415 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m771996397_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1/Enumerator<DG.Tweening.TweenCallback>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m3224669811(__this, method) ((  Il2CppObject * (*) (Enumerator_t2669218415 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m3561903705_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<DG.Tweening.TweenCallback>::Dispose()
#define Enumerator_Dispose_m1537851152(__this, method) ((  void (*) (Enumerator_t2669218415 *, const MethodInfo*))Enumerator_Dispose_m2904289642_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<DG.Tweening.TweenCallback>::VerifyState()
#define Enumerator_VerifyState_m1288140361(__this, method) ((  void (*) (Enumerator_t2669218415 *, const MethodInfo*))Enumerator_VerifyState_m1522854819_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<DG.Tweening.TweenCallback>::MoveNext()
#define Enumerator_MoveNext_m3251714675(__this, method) ((  bool (*) (Enumerator_t2669218415 *, const MethodInfo*))Enumerator_MoveNext_m302222300_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<DG.Tweening.TweenCallback>::get_Current()
#define Enumerator_get_Current_m3111323584(__this, method) ((  TweenCallback_t3786476454 * (*) (Enumerator_t2669218415 *, const MethodInfo*))Enumerator_get_Current_m1113061044_gshared)(__this, method)
