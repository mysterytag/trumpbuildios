﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UniRx.IObserver`1<UniRx.Unit>
struct IObserver_1_t475317645;
// UnityEngine.Events.UnityAction
struct UnityAction_t909267611;
// UniRx.UnityGraphicExtensions/<DirtyVerticesCallbackAsObservable>c__AnonStoreyA2
struct U3CDirtyVerticesCallbackAsObservableU3Ec__AnonStoreyA2_t1456778115;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.UnityGraphicExtensions/<DirtyVerticesCallbackAsObservable>c__AnonStoreyA2/<DirtyVerticesCallbackAsObservable>c__AnonStoreyA1
struct  U3CDirtyVerticesCallbackAsObservableU3Ec__AnonStoreyA1_t1456778114  : public Il2CppObject
{
public:
	// UniRx.IObserver`1<UniRx.Unit> UniRx.UnityGraphicExtensions/<DirtyVerticesCallbackAsObservable>c__AnonStoreyA2/<DirtyVerticesCallbackAsObservable>c__AnonStoreyA1::observer
	Il2CppObject* ___observer_0;
	// UnityEngine.Events.UnityAction UniRx.UnityGraphicExtensions/<DirtyVerticesCallbackAsObservable>c__AnonStoreyA2/<DirtyVerticesCallbackAsObservable>c__AnonStoreyA1::registerHandler
	UnityAction_t909267611 * ___registerHandler_1;
	// UniRx.UnityGraphicExtensions/<DirtyVerticesCallbackAsObservable>c__AnonStoreyA2 UniRx.UnityGraphicExtensions/<DirtyVerticesCallbackAsObservable>c__AnonStoreyA2/<DirtyVerticesCallbackAsObservable>c__AnonStoreyA1::<>f__ref$162
	U3CDirtyVerticesCallbackAsObservableU3Ec__AnonStoreyA2_t1456778115 * ___U3CU3Ef__refU24162_2;

public:
	inline static int32_t get_offset_of_observer_0() { return static_cast<int32_t>(offsetof(U3CDirtyVerticesCallbackAsObservableU3Ec__AnonStoreyA1_t1456778114, ___observer_0)); }
	inline Il2CppObject* get_observer_0() const { return ___observer_0; }
	inline Il2CppObject** get_address_of_observer_0() { return &___observer_0; }
	inline void set_observer_0(Il2CppObject* value)
	{
		___observer_0 = value;
		Il2CppCodeGenWriteBarrier(&___observer_0, value);
	}

	inline static int32_t get_offset_of_registerHandler_1() { return static_cast<int32_t>(offsetof(U3CDirtyVerticesCallbackAsObservableU3Ec__AnonStoreyA1_t1456778114, ___registerHandler_1)); }
	inline UnityAction_t909267611 * get_registerHandler_1() const { return ___registerHandler_1; }
	inline UnityAction_t909267611 ** get_address_of_registerHandler_1() { return &___registerHandler_1; }
	inline void set_registerHandler_1(UnityAction_t909267611 * value)
	{
		___registerHandler_1 = value;
		Il2CppCodeGenWriteBarrier(&___registerHandler_1, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__refU24162_2() { return static_cast<int32_t>(offsetof(U3CDirtyVerticesCallbackAsObservableU3Ec__AnonStoreyA1_t1456778114, ___U3CU3Ef__refU24162_2)); }
	inline U3CDirtyVerticesCallbackAsObservableU3Ec__AnonStoreyA2_t1456778115 * get_U3CU3Ef__refU24162_2() const { return ___U3CU3Ef__refU24162_2; }
	inline U3CDirtyVerticesCallbackAsObservableU3Ec__AnonStoreyA2_t1456778115 ** get_address_of_U3CU3Ef__refU24162_2() { return &___U3CU3Ef__refU24162_2; }
	inline void set_U3CU3Ef__refU24162_2(U3CDirtyVerticesCallbackAsObservableU3Ec__AnonStoreyA2_t1456778115 * value)
	{
		___U3CU3Ef__refU24162_2 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__refU24162_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
