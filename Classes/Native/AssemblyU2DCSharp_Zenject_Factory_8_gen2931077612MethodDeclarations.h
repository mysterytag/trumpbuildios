﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Zenject.Factory`8<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>
struct Factory_8_t2931077612;
// System.Type
struct Type_t;
// System.Type[]
struct TypeU5BU5D_t3431720054;
// Zenject.DiContainer
struct DiContainer_t2383114449;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"

// System.Void Zenject.Factory`8<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>::.ctor()
extern "C"  void Factory_8__ctor_m2899016928_gshared (Factory_8_t2931077612 * __this, const MethodInfo* method);
#define Factory_8__ctor_m2899016928(__this, method) ((  void (*) (Factory_8_t2931077612 *, const MethodInfo*))Factory_8__ctor_m2899016928_gshared)(__this, method)
// System.Type Zenject.Factory`8<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>::get_ConstructedType()
extern "C"  Type_t * Factory_8_get_ConstructedType_m170321867_gshared (Factory_8_t2931077612 * __this, const MethodInfo* method);
#define Factory_8_get_ConstructedType_m170321867(__this, method) ((  Type_t * (*) (Factory_8_t2931077612 *, const MethodInfo*))Factory_8_get_ConstructedType_m170321867_gshared)(__this, method)
// System.Type[] Zenject.Factory`8<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>::get_ProvidedTypes()
extern "C"  TypeU5BU5D_t3431720054* Factory_8_get_ProvidedTypes_m1926120435_gshared (Factory_8_t2931077612 * __this, const MethodInfo* method);
#define Factory_8_get_ProvidedTypes_m1926120435(__this, method) ((  TypeU5BU5D_t3431720054* (*) (Factory_8_t2931077612 *, const MethodInfo*))Factory_8_get_ProvidedTypes_m1926120435_gshared)(__this, method)
// Zenject.DiContainer Zenject.Factory`8<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>::get_Container()
extern "C"  DiContainer_t2383114449 * Factory_8_get_Container_m2274327488_gshared (Factory_8_t2931077612 * __this, const MethodInfo* method);
#define Factory_8_get_Container_m2274327488(__this, method) ((  DiContainer_t2383114449 * (*) (Factory_8_t2931077612 *, const MethodInfo*))Factory_8_get_Container_m2274327488_gshared)(__this, method)
// TValue Zenject.Factory`8<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>::Create(TParam1,TParam2,TParam3,TParam4,TParam5,TParam6,TParam7)
extern "C"  Il2CppObject * Factory_8_Create_m2116143421_gshared (Factory_8_t2931077612 * __this, Il2CppObject * ___param10, Il2CppObject * ___param21, Il2CppObject * ___param32, Il2CppObject * ___param43, Il2CppObject * ___param54, Il2CppObject * ___param65, Il2CppObject * ___param76, const MethodInfo* method);
#define Factory_8_Create_m2116143421(__this, ___param10, ___param21, ___param32, ___param43, ___param54, ___param65, ___param76, method) ((  Il2CppObject * (*) (Factory_8_t2931077612 *, Il2CppObject *, Il2CppObject *, Il2CppObject *, Il2CppObject *, Il2CppObject *, Il2CppObject *, Il2CppObject *, const MethodInfo*))Factory_8_Create_m2116143421_gshared)(__this, ___param10, ___param21, ___param32, ___param43, ___param54, ___param65, ___param76, method)
