﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.InternalUtil.SchedulerQueue
struct SchedulerQueue_t3710535235;
// UniRx.InternalUtil.ScheduledItem
struct ScheduledItem_t1912903245;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_S1912903245.h"

// System.Void UniRx.InternalUtil.SchedulerQueue::.ctor()
extern "C"  void SchedulerQueue__ctor_m2206597130 (SchedulerQueue_t3710535235 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.InternalUtil.SchedulerQueue::.ctor(System.Int32)
extern "C"  void SchedulerQueue__ctor_m3526489691 (SchedulerQueue_t3710535235 * __this, int32_t ___capacity0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UniRx.InternalUtil.SchedulerQueue::get_Count()
extern "C"  int32_t SchedulerQueue_get_Count_m661226116 (SchedulerQueue_t3710535235 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.InternalUtil.SchedulerQueue::Enqueue(UniRx.InternalUtil.ScheduledItem)
extern "C"  void SchedulerQueue_Enqueue_m1094543559 (SchedulerQueue_t3710535235 * __this, ScheduledItem_t1912903245 * ___scheduledItem0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UniRx.InternalUtil.SchedulerQueue::Remove(UniRx.InternalUtil.ScheduledItem)
extern "C"  bool SchedulerQueue_Remove_m4278708089 (SchedulerQueue_t3710535235 * __this, ScheduledItem_t1912903245 * ___scheduledItem0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UniRx.InternalUtil.ScheduledItem UniRx.InternalUtil.SchedulerQueue::Dequeue()
extern "C"  ScheduledItem_t1912903245 * SchedulerQueue_Dequeue_m1988603796 (SchedulerQueue_t3710535235 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UniRx.InternalUtil.ScheduledItem UniRx.InternalUtil.SchedulerQueue::Peek()
extern "C"  ScheduledItem_t1912903245 * SchedulerQueue_Peek_m652492057 (SchedulerQueue_t3710535235 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
