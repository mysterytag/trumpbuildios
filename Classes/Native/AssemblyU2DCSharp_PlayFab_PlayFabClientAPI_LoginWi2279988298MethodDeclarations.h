﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.PlayFabClientAPI/LoginWithPlayFabResponseCallback
struct LoginWithPlayFabResponseCallback_t2279988298;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;
// PlayFab.ClientModels.LoginWithPlayFabRequest
struct LoginWithPlayFabRequest_t402940283;
// PlayFab.ClientModels.LoginResult
struct LoginResult_t2117559510;
// PlayFab.PlayFabError
struct PlayFabError_t750598646;
// System.IAsyncResult
struct IAsyncResult_t537683269;
// System.AsyncCallback
struct AsyncCallback_t1363551830;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_IntPtr676692020.h"
#include "mscorlib_System_String968488902.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_LoginWithPla402940283.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_LoginResult2117559510.h"
#include "AssemblyU2DCSharpU2Dfirstpass_PlayFab_PlayFabError750598646.h"
#include "mscorlib_System_AsyncCallback1363551830.h"

// System.Void PlayFab.PlayFabClientAPI/LoginWithPlayFabResponseCallback::.ctor(System.Object,System.IntPtr)
extern "C"  void LoginWithPlayFabResponseCallback__ctor_m2837741625 (LoginWithPlayFabResponseCallback_t2279988298 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/LoginWithPlayFabResponseCallback::Invoke(System.String,System.Int32,PlayFab.ClientModels.LoginWithPlayFabRequest,PlayFab.ClientModels.LoginResult,PlayFab.PlayFabError,System.Object)
extern "C"  void LoginWithPlayFabResponseCallback_Invoke_m2475515575 (LoginWithPlayFabResponseCallback_t2279988298 * __this, String_t* ___urlPath0, int32_t ___callId1, LoginWithPlayFabRequest_t402940283 * ___request2, LoginResult_t2117559510 * ___result3, PlayFabError_t750598646 * ___error4, Il2CppObject * ___customData5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
extern "C" void pinvoke_delegate_wrapper_LoginWithPlayFabResponseCallback_t2279988298(Il2CppObject* delegate, String_t* ___urlPath0, int32_t ___callId1, LoginWithPlayFabRequest_t402940283 * ___request2, LoginResult_t2117559510 * ___result3, PlayFabError_t750598646 * ___error4, Il2CppObject * ___customData5);
// System.IAsyncResult PlayFab.PlayFabClientAPI/LoginWithPlayFabResponseCallback::BeginInvoke(System.String,System.Int32,PlayFab.ClientModels.LoginWithPlayFabRequest,PlayFab.ClientModels.LoginResult,PlayFab.PlayFabError,System.Object,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * LoginWithPlayFabResponseCallback_BeginInvoke_m2989588404 (LoginWithPlayFabResponseCallback_t2279988298 * __this, String_t* ___urlPath0, int32_t ___callId1, LoginWithPlayFabRequest_t402940283 * ___request2, LoginResult_t2117559510 * ___result3, PlayFabError_t750598646 * ___error4, Il2CppObject * ___customData5, AsyncCallback_t1363551830 * ___callback6, Il2CppObject * ___object7, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/LoginWithPlayFabResponseCallback::EndInvoke(System.IAsyncResult)
extern "C"  void LoginWithPlayFabResponseCallback_EndInvoke_m1185609417 (LoginWithPlayFabResponseCallback_t2279988298 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
