﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.ObservableWWW/<PostWWW>c__AnonStorey8C
struct U3CPostWWWU3Ec__AnonStorey8C_t4245247439;
// System.Collections.IEnumerator
struct IEnumerator_t287207039;
// UniRx.IObserver`1<UnityEngine.WWW>
struct IObserver_1_t3734971003;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_CancellationTo1439151560.h"

// System.Void UniRx.ObservableWWW/<PostWWW>c__AnonStorey8C::.ctor()
extern "C"  void U3CPostWWWU3Ec__AnonStorey8C__ctor_m4015583629 (U3CPostWWWU3Ec__AnonStorey8C_t4245247439 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator UniRx.ObservableWWW/<PostWWW>c__AnonStorey8C::<>m__B8(UniRx.IObserver`1<UnityEngine.WWW>,UniRx.CancellationToken)
extern "C"  Il2CppObject * U3CPostWWWU3Ec__AnonStorey8C_U3CU3Em__B8_m532394056 (U3CPostWWWU3Ec__AnonStorey8C_t4245247439 * __this, Il2CppObject* ___observer0, CancellationToken_t1439151560  ___cancellation1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
