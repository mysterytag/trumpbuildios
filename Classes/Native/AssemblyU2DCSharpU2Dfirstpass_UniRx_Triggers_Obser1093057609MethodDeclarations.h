﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Triggers.ObservableScrollTrigger
struct ObservableScrollTrigger_t1093057609;
// UnityEngine.EventSystems.PointerEventData
struct PointerEventData_t3205101634;
// UniRx.IObservable`1<UnityEngine.EventSystems.PointerEventData>
struct IObservable_1_t2963899998;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_PointerEve3205101634.h"

// System.Void UniRx.Triggers.ObservableScrollTrigger::.ctor()
extern "C"  void ObservableScrollTrigger__ctor_m2904829122 (ObservableScrollTrigger_t1093057609 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.Triggers.ObservableScrollTrigger::UnityEngine.EventSystems.IScrollHandler.OnScroll(UnityEngine.EventSystems.PointerEventData)
extern "C"  void ObservableScrollTrigger_UnityEngine_EventSystems_IScrollHandler_OnScroll_m1789177635 (ObservableScrollTrigger_t1093057609 * __this, PointerEventData_t3205101634 * ___eventData0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UniRx.IObservable`1<UnityEngine.EventSystems.PointerEventData> UniRx.Triggers.ObservableScrollTrigger::OnScrollAsObservable()
extern "C"  Il2CppObject* ObservableScrollTrigger_OnScrollAsObservable_m319298215 (ObservableScrollTrigger_t1093057609 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.Triggers.ObservableScrollTrigger::RaiseOnCompletedOnDestroy()
extern "C"  void ObservableScrollTrigger_RaiseOnCompletedOnDestroy_m2807369467 (ObservableScrollTrigger_t1093057609 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
