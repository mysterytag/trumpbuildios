﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_Range938821841.h"

// System.Int32 Tacticsoft.RangeExtensions::Last(UnityEngine.SocialPlatforms.Range)
extern "C"  int32_t RangeExtensions_Last_m3762836021 (Il2CppObject * __this /* static, unused */, Range_t938821841  ___range0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Tacticsoft.RangeExtensions::Contains(UnityEngine.SocialPlatforms.Range,System.Int32)
extern "C"  bool RangeExtensions_Contains_m2800799345 (Il2CppObject * __this /* static, unused */, Range_t938821841  ___range0, int32_t ___num1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
