﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Reactor`1/<TransactionUnpackReact>c__AnonStorey141<System.Boolean>
struct U3CTransactionUnpackReactU3Ec__AnonStorey141_t2538948652;

#include "codegen/il2cpp-codegen.h"

// System.Void Reactor`1/<TransactionUnpackReact>c__AnonStorey141<System.Boolean>::.ctor()
extern "C"  void U3CTransactionUnpackReactU3Ec__AnonStorey141__ctor_m4065999468_gshared (U3CTransactionUnpackReactU3Ec__AnonStorey141_t2538948652 * __this, const MethodInfo* method);
#define U3CTransactionUnpackReactU3Ec__AnonStorey141__ctor_m4065999468(__this, method) ((  void (*) (U3CTransactionUnpackReactU3Ec__AnonStorey141_t2538948652 *, const MethodInfo*))U3CTransactionUnpackReactU3Ec__AnonStorey141__ctor_m4065999468_gshared)(__this, method)
// System.Void Reactor`1/<TransactionUnpackReact>c__AnonStorey141<System.Boolean>::<>m__1D6()
extern "C"  void U3CTransactionUnpackReactU3Ec__AnonStorey141_U3CU3Em__1D6_m2057873904_gshared (U3CTransactionUnpackReactU3Ec__AnonStorey141_t2538948652 * __this, const MethodInfo* method);
#define U3CTransactionUnpackReactU3Ec__AnonStorey141_U3CU3Em__1D6_m2057873904(__this, method) ((  void (*) (U3CTransactionUnpackReactU3Ec__AnonStorey141_t2538948652 *, const MethodInfo*))U3CTransactionUnpackReactU3Ec__AnonStorey141_U3CU3Em__1D6_m2057873904_gshared)(__this, method)
