﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SponsorPay.BrandEngageRequestResponseReceivedHandler
struct BrandEngageRequestResponseReceivedHandler_t263189403;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t537683269;
// System.AsyncCallback
struct AsyncCallback_t1363551830;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_IntPtr676692020.h"
#include "mscorlib_System_AsyncCallback1363551830.h"

// System.Void SponsorPay.BrandEngageRequestResponseReceivedHandler::.ctor(System.Object,System.IntPtr)
extern "C"  void BrandEngageRequestResponseReceivedHandler__ctor_m517602664 (BrandEngageRequestResponseReceivedHandler_t263189403 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SponsorPay.BrandEngageRequestResponseReceivedHandler::Invoke(System.Boolean)
extern "C"  void BrandEngageRequestResponseReceivedHandler_Invoke_m258119097 (BrandEngageRequestResponseReceivedHandler_t263189403 * __this, bool ___offersAvailable0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
extern "C" void pinvoke_delegate_wrapper_BrandEngageRequestResponseReceivedHandler_t263189403(Il2CppObject* delegate, bool ___offersAvailable0);
// System.IAsyncResult SponsorPay.BrandEngageRequestResponseReceivedHandler::BeginInvoke(System.Boolean,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * BrandEngageRequestResponseReceivedHandler_BeginInvoke_m1029202918 (BrandEngageRequestResponseReceivedHandler_t263189403 * __this, bool ___offersAvailable0, AsyncCallback_t1363551830 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SponsorPay.BrandEngageRequestResponseReceivedHandler::EndInvoke(System.IAsyncResult)
extern "C"  void BrandEngageRequestResponseReceivedHandler_EndInvoke_m1028940664 (BrandEngageRequestResponseReceivedHandler_t263189403 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
