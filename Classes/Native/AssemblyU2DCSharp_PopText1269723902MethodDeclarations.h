﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PopText
struct PopText_t1269723902;
// System.String
struct String_t;
// System.Collections.IEnumerator
struct IEnumerator_t287207039;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String968488902.h"

// System.Void PopText::.ctor()
extern "C"  void PopText__ctor_m3212393005 (PopText_t1269723902 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PopText::Awake()
extern "C"  void PopText_Awake_m3449998224 (PopText_t1269723902 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PopText::Init(System.String,System.Single)
extern "C"  void PopText_Init_m4211410816 (PopText_t1269723902 * __this, String_t* ___message0, float ___scale1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PopText::Init(System.Single)
extern "C"  void PopText_Init_m2065447556 (PopText_t1269723902 * __this, float ___scale0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator PopText::ShowCoroutine()
extern "C"  Il2CppObject * PopText_ShowCoroutine_m1441154350 (PopText_t1269723902 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
