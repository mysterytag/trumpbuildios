﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UniRx.Operators.WithLatestFromObservable`3<System.Object,System.Object,System.Object>
struct WithLatestFromObservable_3_t1427945019;
// System.Object
struct Il2CppObject;

#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Oper1187768149.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Operators.WithLatestFromObservable`3/WithLatestFrom<System.Object,System.Object,System.Object>
struct  WithLatestFrom_t2869896360  : public OperatorObserverBase_2_t1187768149
{
public:
	// UniRx.Operators.WithLatestFromObservable`3<TLeft,TRight,TResult> UniRx.Operators.WithLatestFromObservable`3/WithLatestFrom::parent
	WithLatestFromObservable_3_t1427945019 * ___parent_2;
	// System.Object UniRx.Operators.WithLatestFromObservable`3/WithLatestFrom::gate
	Il2CppObject * ___gate_3;
	// System.Boolean modreq(System.Runtime.CompilerServices.IsVolatile) UniRx.Operators.WithLatestFromObservable`3/WithLatestFrom::hasLatest
	bool ___hasLatest_4;
	// TRight UniRx.Operators.WithLatestFromObservable`3/WithLatestFrom::latestValue
	Il2CppObject * ___latestValue_5;

public:
	inline static int32_t get_offset_of_parent_2() { return static_cast<int32_t>(offsetof(WithLatestFrom_t2869896360, ___parent_2)); }
	inline WithLatestFromObservable_3_t1427945019 * get_parent_2() const { return ___parent_2; }
	inline WithLatestFromObservable_3_t1427945019 ** get_address_of_parent_2() { return &___parent_2; }
	inline void set_parent_2(WithLatestFromObservable_3_t1427945019 * value)
	{
		___parent_2 = value;
		Il2CppCodeGenWriteBarrier(&___parent_2, value);
	}

	inline static int32_t get_offset_of_gate_3() { return static_cast<int32_t>(offsetof(WithLatestFrom_t2869896360, ___gate_3)); }
	inline Il2CppObject * get_gate_3() const { return ___gate_3; }
	inline Il2CppObject ** get_address_of_gate_3() { return &___gate_3; }
	inline void set_gate_3(Il2CppObject * value)
	{
		___gate_3 = value;
		Il2CppCodeGenWriteBarrier(&___gate_3, value);
	}

	inline static int32_t get_offset_of_hasLatest_4() { return static_cast<int32_t>(offsetof(WithLatestFrom_t2869896360, ___hasLatest_4)); }
	inline bool get_hasLatest_4() const { return ___hasLatest_4; }
	inline bool* get_address_of_hasLatest_4() { return &___hasLatest_4; }
	inline void set_hasLatest_4(bool value)
	{
		___hasLatest_4 = value;
	}

	inline static int32_t get_offset_of_latestValue_5() { return static_cast<int32_t>(offsetof(WithLatestFrom_t2869896360, ___latestValue_5)); }
	inline Il2CppObject * get_latestValue_5() const { return ___latestValue_5; }
	inline Il2CppObject ** get_address_of_latestValue_5() { return &___latestValue_5; }
	inline void set_latestValue_5(Il2CppObject * value)
	{
		___latestValue_5 = value;
		Il2CppCodeGenWriteBarrier(&___latestValue_5, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
