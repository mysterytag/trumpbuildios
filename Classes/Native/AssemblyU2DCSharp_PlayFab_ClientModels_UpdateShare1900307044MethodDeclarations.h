﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.ClientModels.UpdateSharedGroupDataRequest
struct UpdateSharedGroupDataRequest_t1900307044;
// System.String
struct String_t;
// System.Collections.Generic.Dictionary`2<System.String,System.String>
struct Dictionary_2_t2606186806;
// System.Collections.Generic.List`1<System.String>
struct List_1_t1765447871;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String968488902.h"
#include "mscorlib_System_Nullable_1_gen2611574664.h"

// System.Void PlayFab.ClientModels.UpdateSharedGroupDataRequest::.ctor()
extern "C"  void UpdateSharedGroupDataRequest__ctor_m101699417 (UpdateSharedGroupDataRequest_t1900307044 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PlayFab.ClientModels.UpdateSharedGroupDataRequest::get_SharedGroupId()
extern "C"  String_t* UpdateSharedGroupDataRequest_get_SharedGroupId_m1597551366 (UpdateSharedGroupDataRequest_t1900307044 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.UpdateSharedGroupDataRequest::set_SharedGroupId(System.String)
extern "C"  void UpdateSharedGroupDataRequest_set_SharedGroupId_m2635306803 (UpdateSharedGroupDataRequest_t1900307044 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.Dictionary`2<System.String,System.String> PlayFab.ClientModels.UpdateSharedGroupDataRequest::get_Data()
extern "C"  Dictionary_2_t2606186806 * UpdateSharedGroupDataRequest_get_Data_m4114026704 (UpdateSharedGroupDataRequest_t1900307044 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.UpdateSharedGroupDataRequest::set_Data(System.Collections.Generic.Dictionary`2<System.String,System.String>)
extern "C"  void UpdateSharedGroupDataRequest_set_Data_m1952247715 (UpdateSharedGroupDataRequest_t1900307044 * __this, Dictionary_2_t2606186806 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<System.String> PlayFab.ClientModels.UpdateSharedGroupDataRequest::get_KeysToRemove()
extern "C"  List_1_t1765447871 * UpdateSharedGroupDataRequest_get_KeysToRemove_m2098198710 (UpdateSharedGroupDataRequest_t1900307044 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.UpdateSharedGroupDataRequest::set_KeysToRemove(System.Collections.Generic.List`1<System.String>)
extern "C"  void UpdateSharedGroupDataRequest_set_KeysToRemove_m3378464687 (UpdateSharedGroupDataRequest_t1900307044 * __this, List_1_t1765447871 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Nullable`1<PlayFab.ClientModels.UserDataPermission> PlayFab.ClientModels.UpdateSharedGroupDataRequest::get_Permission()
extern "C"  Nullable_1_t2611574664  UpdateSharedGroupDataRequest_get_Permission_m1906470125 (UpdateSharedGroupDataRequest_t1900307044 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.UpdateSharedGroupDataRequest::set_Permission(System.Nullable`1<PlayFab.ClientModels.UserDataPermission>)
extern "C"  void UpdateSharedGroupDataRequest_set_Permission_m2636002320 (UpdateSharedGroupDataRequest_t1900307044 * __this, Nullable_1_t2611574664  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
