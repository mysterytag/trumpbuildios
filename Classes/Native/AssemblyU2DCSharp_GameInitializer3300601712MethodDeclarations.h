﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GameInitializer
struct GameInitializer_t3300601712;

#include "codegen/il2cpp-codegen.h"

// System.Void GameInitializer::.ctor()
extern "C"  void GameInitializer__ctor_m2343761403 (GameInitializer_t3300601712 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameInitializer::Initialize()
extern "C"  void GameInitializer_Initialize_m3885688185 (GameInitializer_t3300601712 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
