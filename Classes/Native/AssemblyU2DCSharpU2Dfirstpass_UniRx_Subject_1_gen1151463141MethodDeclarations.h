﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Subject`1<TableButtons/StatkaPoTableViev>
struct Subject_1_t1151463141;
// System.Exception
struct Exception_t1967233988;
// System.IDisposable
struct IDisposable_t1628921374;
// UniRx.IObserver`1<TableButtons/StatkaPoTableViev>
struct IObserver_1_t1425427424;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Exception1967233988.h"
#include "AssemblyU2DCSharp_TableButtons_StatkaPoTableViev3508395817.h"

// System.Void UniRx.Subject`1<TableButtons/StatkaPoTableViev>::.ctor()
extern "C"  void Subject_1__ctor_m2319600701_gshared (Subject_1_t1151463141 * __this, const MethodInfo* method);
#define Subject_1__ctor_m2319600701(__this, method) ((  void (*) (Subject_1_t1151463141 *, const MethodInfo*))Subject_1__ctor_m2319600701_gshared)(__this, method)
// System.Boolean UniRx.Subject`1<TableButtons/StatkaPoTableViev>::get_HasObservers()
extern "C"  bool Subject_1_get_HasObservers_m385824631_gshared (Subject_1_t1151463141 * __this, const MethodInfo* method);
#define Subject_1_get_HasObservers_m385824631(__this, method) ((  bool (*) (Subject_1_t1151463141 *, const MethodInfo*))Subject_1_get_HasObservers_m385824631_gshared)(__this, method)
// System.Void UniRx.Subject`1<TableButtons/StatkaPoTableViev>::OnCompleted()
extern "C"  void Subject_1_OnCompleted_m3055590919_gshared (Subject_1_t1151463141 * __this, const MethodInfo* method);
#define Subject_1_OnCompleted_m3055590919(__this, method) ((  void (*) (Subject_1_t1151463141 *, const MethodInfo*))Subject_1_OnCompleted_m3055590919_gshared)(__this, method)
// System.Void UniRx.Subject`1<TableButtons/StatkaPoTableViev>::OnError(System.Exception)
extern "C"  void Subject_1_OnError_m107183284_gshared (Subject_1_t1151463141 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method);
#define Subject_1_OnError_m107183284(__this, ___error0, method) ((  void (*) (Subject_1_t1151463141 *, Exception_t1967233988 *, const MethodInfo*))Subject_1_OnError_m107183284_gshared)(__this, ___error0, method)
// System.Void UniRx.Subject`1<TableButtons/StatkaPoTableViev>::OnNext(T)
extern "C"  void Subject_1_OnNext_m2878950309_gshared (Subject_1_t1151463141 * __this, int32_t ___value0, const MethodInfo* method);
#define Subject_1_OnNext_m2878950309(__this, ___value0, method) ((  void (*) (Subject_1_t1151463141 *, int32_t, const MethodInfo*))Subject_1_OnNext_m2878950309_gshared)(__this, ___value0, method)
// System.IDisposable UniRx.Subject`1<TableButtons/StatkaPoTableViev>::Subscribe(UniRx.IObserver`1<T>)
extern "C"  Il2CppObject * Subject_1_Subscribe_m3727618834_gshared (Subject_1_t1151463141 * __this, Il2CppObject* ___observer0, const MethodInfo* method);
#define Subject_1_Subscribe_m3727618834(__this, ___observer0, method) ((  Il2CppObject * (*) (Subject_1_t1151463141 *, Il2CppObject*, const MethodInfo*))Subject_1_Subscribe_m3727618834_gshared)(__this, ___observer0, method)
// System.Void UniRx.Subject`1<TableButtons/StatkaPoTableViev>::Dispose()
extern "C"  void Subject_1_Dispose_m4241497402_gshared (Subject_1_t1151463141 * __this, const MethodInfo* method);
#define Subject_1_Dispose_m4241497402(__this, method) ((  void (*) (Subject_1_t1151463141 *, const MethodInfo*))Subject_1_Dispose_m4241497402_gshared)(__this, method)
// System.Void UniRx.Subject`1<TableButtons/StatkaPoTableViev>::ThrowIfDisposed()
extern "C"  void Subject_1_ThrowIfDisposed_m2483154115_gshared (Subject_1_t1151463141 * __this, const MethodInfo* method);
#define Subject_1_ThrowIfDisposed_m2483154115(__this, method) ((  void (*) (Subject_1_t1151463141 *, const MethodInfo*))Subject_1_ThrowIfDisposed_m2483154115_gshared)(__this, method)
// System.Boolean UniRx.Subject`1<TableButtons/StatkaPoTableViev>::IsRequiredSubscribeOnCurrentThread()
extern "C"  bool Subject_1_IsRequiredSubscribeOnCurrentThread_m4284437678_gshared (Subject_1_t1151463141 * __this, const MethodInfo* method);
#define Subject_1_IsRequiredSubscribeOnCurrentThread_m4284437678(__this, method) ((  bool (*) (Subject_1_t1151463141 *, const MethodInfo*))Subject_1_IsRequiredSubscribeOnCurrentThread_m4284437678_gshared)(__this, method)
