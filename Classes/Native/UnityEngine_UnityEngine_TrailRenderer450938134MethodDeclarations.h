﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.TrailRenderer
struct TrailRenderer_t450938134;

#include "codegen/il2cpp-codegen.h"

// System.Single UnityEngine.TrailRenderer::get_time()
extern "C"  float TrailRenderer_get_time_m3874247495 (TrailRenderer_t450938134 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.TrailRenderer::set_time(System.Single)
extern "C"  void TrailRenderer_set_time_m1563781764 (TrailRenderer_t450938134 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.TrailRenderer::get_startWidth()
extern "C"  float TrailRenderer_get_startWidth_m3661833854 (TrailRenderer_t450938134 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.TrailRenderer::set_startWidth(System.Single)
extern "C"  void TrailRenderer_set_startWidth_m682375341 (TrailRenderer_t450938134 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.TrailRenderer::get_endWidth()
extern "C"  float TrailRenderer_get_endWidth_m2723856933 (TrailRenderer_t450938134 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.TrailRenderer::set_endWidth(System.Single)
extern "C"  void TrailRenderer_set_endWidth_m35140710 (TrailRenderer_t450938134 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
