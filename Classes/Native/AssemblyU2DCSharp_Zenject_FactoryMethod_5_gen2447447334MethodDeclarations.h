﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Zenject.FactoryMethod`5<System.Object,System.Object,System.Object,System.Object,System.Object>
struct FactoryMethod_5_t2447447334;
// System.Type
struct Type_t;
// System.Type[]
struct TypeU5BU5D_t3431720054;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"

// System.Void Zenject.FactoryMethod`5<System.Object,System.Object,System.Object,System.Object,System.Object>::.ctor()
extern "C"  void FactoryMethod_5__ctor_m795657736_gshared (FactoryMethod_5_t2447447334 * __this, const MethodInfo* method);
#define FactoryMethod_5__ctor_m795657736(__this, method) ((  void (*) (FactoryMethod_5_t2447447334 *, const MethodInfo*))FactoryMethod_5__ctor_m795657736_gshared)(__this, method)
// System.Type Zenject.FactoryMethod`5<System.Object,System.Object,System.Object,System.Object,System.Object>::get_ConstructedType()
extern "C"  Type_t * FactoryMethod_5_get_ConstructedType_m4204406259_gshared (FactoryMethod_5_t2447447334 * __this, const MethodInfo* method);
#define FactoryMethod_5_get_ConstructedType_m4204406259(__this, method) ((  Type_t * (*) (FactoryMethod_5_t2447447334 *, const MethodInfo*))FactoryMethod_5_get_ConstructedType_m4204406259_gshared)(__this, method)
// System.Type[] Zenject.FactoryMethod`5<System.Object,System.Object,System.Object,System.Object,System.Object>::get_ProvidedTypes()
extern "C"  TypeU5BU5D_t3431720054* FactoryMethod_5_get_ProvidedTypes_m3491837211_gshared (FactoryMethod_5_t2447447334 * __this, const MethodInfo* method);
#define FactoryMethod_5_get_ProvidedTypes_m3491837211(__this, method) ((  TypeU5BU5D_t3431720054* (*) (FactoryMethod_5_t2447447334 *, const MethodInfo*))FactoryMethod_5_get_ProvidedTypes_m3491837211_gshared)(__this, method)
// TValue Zenject.FactoryMethod`5<System.Object,System.Object,System.Object,System.Object,System.Object>::Create(TParam1,TParam2,TParam3,TParam4)
extern "C"  Il2CppObject * FactoryMethod_5_Create_m940508984_gshared (FactoryMethod_5_t2447447334 * __this, Il2CppObject * ___param10, Il2CppObject * ___param21, Il2CppObject * ___param32, Il2CppObject * ___param43, const MethodInfo* method);
#define FactoryMethod_5_Create_m940508984(__this, ___param10, ___param21, ___param32, ___param43, method) ((  Il2CppObject * (*) (FactoryMethod_5_t2447447334 *, Il2CppObject *, Il2CppObject *, Il2CppObject *, Il2CppObject *, const MethodInfo*))FactoryMethod_5_Create_m940508984_gshared)(__this, ___param10, ___param21, ___param32, ___param43, method)
