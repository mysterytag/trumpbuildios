﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.IDisposable
struct IDisposable_t1628921374;
// UniRx.Scheduler/<Schedule>c__AnonStorey73
struct U3CScheduleU3Ec__AnonStorey73_t3913535384;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Scheduler/<Schedule>c__AnonStorey73/<Schedule>c__AnonStorey74
struct  U3CScheduleU3Ec__AnonStorey74_t3913535385  : public Il2CppObject
{
public:
	// System.Boolean UniRx.Scheduler/<Schedule>c__AnonStorey73/<Schedule>c__AnonStorey74::isAdded
	bool ___isAdded_0;
	// System.IDisposable UniRx.Scheduler/<Schedule>c__AnonStorey73/<Schedule>c__AnonStorey74::d
	Il2CppObject * ___d_1;
	// System.Boolean UniRx.Scheduler/<Schedule>c__AnonStorey73/<Schedule>c__AnonStorey74::isDone
	bool ___isDone_2;
	// UniRx.Scheduler/<Schedule>c__AnonStorey73 UniRx.Scheduler/<Schedule>c__AnonStorey73/<Schedule>c__AnonStorey74::<>f__ref$115
	U3CScheduleU3Ec__AnonStorey73_t3913535384 * ___U3CU3Ef__refU24115_3;

public:
	inline static int32_t get_offset_of_isAdded_0() { return static_cast<int32_t>(offsetof(U3CScheduleU3Ec__AnonStorey74_t3913535385, ___isAdded_0)); }
	inline bool get_isAdded_0() const { return ___isAdded_0; }
	inline bool* get_address_of_isAdded_0() { return &___isAdded_0; }
	inline void set_isAdded_0(bool value)
	{
		___isAdded_0 = value;
	}

	inline static int32_t get_offset_of_d_1() { return static_cast<int32_t>(offsetof(U3CScheduleU3Ec__AnonStorey74_t3913535385, ___d_1)); }
	inline Il2CppObject * get_d_1() const { return ___d_1; }
	inline Il2CppObject ** get_address_of_d_1() { return &___d_1; }
	inline void set_d_1(Il2CppObject * value)
	{
		___d_1 = value;
		Il2CppCodeGenWriteBarrier(&___d_1, value);
	}

	inline static int32_t get_offset_of_isDone_2() { return static_cast<int32_t>(offsetof(U3CScheduleU3Ec__AnonStorey74_t3913535385, ___isDone_2)); }
	inline bool get_isDone_2() const { return ___isDone_2; }
	inline bool* get_address_of_isDone_2() { return &___isDone_2; }
	inline void set_isDone_2(bool value)
	{
		___isDone_2 = value;
	}

	inline static int32_t get_offset_of_U3CU3Ef__refU24115_3() { return static_cast<int32_t>(offsetof(U3CScheduleU3Ec__AnonStorey74_t3913535385, ___U3CU3Ef__refU24115_3)); }
	inline U3CScheduleU3Ec__AnonStorey73_t3913535384 * get_U3CU3Ef__refU24115_3() const { return ___U3CU3Ef__refU24115_3; }
	inline U3CScheduleU3Ec__AnonStorey73_t3913535384 ** get_address_of_U3CU3Ef__refU24115_3() { return &___U3CU3Ef__refU24115_3; }
	inline void set_U3CU3Ef__refU24115_3(U3CScheduleU3Ec__AnonStorey73_t3913535384 * value)
	{
		___U3CU3Ef__refU24115_3 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__refU24115_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
