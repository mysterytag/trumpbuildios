﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.TimerObservable/<SubscribeCore>c__AnonStorey6F
struct U3CSubscribeCoreU3Ec__AnonStorey6F_t1995955654;

#include "codegen/il2cpp-codegen.h"

// System.Void UniRx.Operators.TimerObservable/<SubscribeCore>c__AnonStorey6F::.ctor()
extern "C"  void U3CSubscribeCoreU3Ec__AnonStorey6F__ctor_m793666315 (U3CSubscribeCoreU3Ec__AnonStorey6F_t1995955654 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.Operators.TimerObservable/<SubscribeCore>c__AnonStorey6F::<>m__8C()
extern "C"  void U3CSubscribeCoreU3Ec__AnonStorey6F_U3CU3Em__8C_m517416799 (U3CSubscribeCoreU3Ec__AnonStorey6F_t1995955654 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
