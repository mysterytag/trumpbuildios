﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String968488902.h"

// System.Void GameAnalyticsSDK.GA_Business::NewEvent(System.String,System.Int32,System.String,System.String,System.String,System.String,System.Boolean)
extern "C"  void GA_Business_NewEvent_m2728964600 (Il2CppObject * __this /* static, unused */, String_t* ___currency0, int32_t ___amount1, String_t* ___itemType2, String_t* ___itemId3, String_t* ___cartType4, String_t* ___receipt5, bool ___autoFetchReceipt6, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameAnalyticsSDK.GA_Business::NewEvent(System.String,System.Int32,System.String,System.String,System.String)
extern "C"  void GA_Business_NewEvent_m4288192105 (Il2CppObject * __this /* static, unused */, String_t* ___currency0, int32_t ___amount1, String_t* ___itemType2, String_t* ___itemId3, String_t* ___cartType4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
