﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_ObjectModel_Collection2806094150MethodDeclarations.h"

// System.Void System.Collections.ObjectModel.Collection`1<DonateButton>::.ctor()
#define Collection_1__ctor_m2882581096(__this, method) ((  void (*) (Collection_1_t2639741171 *, const MethodInfo*))Collection_1__ctor_m1690372513_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<DonateButton>::.ctor(System.Collections.Generic.IList`1<T>)
#define Collection_1__ctor_m3942569389(__this, ___list0, method) ((  void (*) (Collection_1_t2639741171 *, Il2CppObject*, const MethodInfo*))Collection_1__ctor_m3333275156_gshared)(__this, ___list0, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<DonateButton>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
#define Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m4062720847(__this, method) ((  bool (*) (Collection_1_t2639741171 *, const MethodInfo*))Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m1624016570_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<DonateButton>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define Collection_1_System_Collections_ICollection_CopyTo_m1675759580(__this, ___array0, ___index1, method) ((  void (*) (Collection_1_t2639741171 *, Il2CppArray *, int32_t, const MethodInfo*))Collection_1_System_Collections_ICollection_CopyTo_m1285013187_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.ObjectModel.Collection`1<DonateButton>::System.Collections.IEnumerable.GetEnumerator()
#define Collection_1_System_Collections_IEnumerable_GetEnumerator_m919829355(__this, method) ((  Il2CppObject * (*) (Collection_1_t2639741171 *, const MethodInfo*))Collection_1_System_Collections_IEnumerable_GetEnumerator_m2828471038_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<DonateButton>::System.Collections.IList.Add(System.Object)
#define Collection_1_System_Collections_IList_Add_m2141661746(__this, ___value0, method) ((  int32_t (*) (Collection_1_t2639741171 *, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_Add_m1708617267_gshared)(__this, ___value0, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<DonateButton>::System.Collections.IList.Contains(System.Object)
#define Collection_1_System_Collections_IList_Contains_m1221291214(__this, ___value0, method) ((  bool (*) (Collection_1_t2639741171 *, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_Contains_m504494585_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<DonateButton>::System.Collections.IList.IndexOf(System.Object)
#define Collection_1_System_Collections_IList_IndexOf_m1443035018(__this, ___value0, method) ((  int32_t (*) (Collection_1_t2639741171 *, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_IndexOf_m1652511499_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.Collection`1<DonateButton>::System.Collections.IList.Insert(System.Int32,System.Object)
#define Collection_1_System_Collections_IList_Insert_m4245256957(__this, ___index0, ___value1, method) ((  void (*) (Collection_1_t2639741171 *, int32_t, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_Insert_m2187188150_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.ObjectModel.Collection`1<DonateButton>::System.Collections.IList.Remove(System.Object)
#define Collection_1_System_Collections_IList_Remove_m4042069771(__this, ___value0, method) ((  void (*) (Collection_1_t2639741171 *, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_Remove_m2625864114_gshared)(__this, ___value0, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<DonateButton>::System.Collections.ICollection.get_IsSynchronized()
#define Collection_1_System_Collections_ICollection_get_IsSynchronized_m813567950(__this, method) ((  bool (*) (Collection_1_t2639741171 *, const MethodInfo*))Collection_1_System_Collections_ICollection_get_IsSynchronized_m1302589123_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.Collection`1<DonateButton>::System.Collections.ICollection.get_SyncRoot()
#define Collection_1_System_Collections_ICollection_get_SyncRoot_m2265040512(__this, method) ((  Il2CppObject * (*) (Collection_1_t2639741171 *, const MethodInfo*))Collection_1_System_Collections_ICollection_get_SyncRoot_m1873829551_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<DonateButton>::System.Collections.IList.get_IsFixedSize()
#define Collection_1_System_Collections_IList_get_IsFixedSize_m1809862333(__this, method) ((  bool (*) (Collection_1_t2639741171 *, const MethodInfo*))Collection_1_System_Collections_IList_get_IsFixedSize_m2813777960_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<DonateButton>::System.Collections.IList.get_IsReadOnly()
#define Collection_1_System_Collections_IList_get_IsReadOnly_m3560432796(__this, method) ((  bool (*) (Collection_1_t2639741171 *, const MethodInfo*))Collection_1_System_Collections_IList_get_IsReadOnly_m1376059857_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.Collection`1<DonateButton>::System.Collections.IList.get_Item(System.Int32)
#define Collection_1_System_Collections_IList_get_Item_m221124615(__this, ___index0, method) ((  Il2CppObject * (*) (Collection_1_t2639741171 *, int32_t, const MethodInfo*))Collection_1_System_Collections_IList_get_Item_m2224513142_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.Collection`1<DonateButton>::System.Collections.IList.set_Item(System.Int32,System.Object)
#define Collection_1_System_Collections_IList_set_Item_m86956948(__this, ___index0, ___value1, method) ((  void (*) (Collection_1_t2639741171 *, int32_t, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_set_Item_m2262756877_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.ObjectModel.Collection`1<DonateButton>::Add(T)
#define Collection_1_Add_m4239548695(__this, ___item0, method) ((  void (*) (Collection_1_t2639741171 *, DonateButton_t670753441 *, const MethodInfo*))Collection_1_Add_m321765054_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<DonateButton>::Clear()
#define Collection_1_Clear_m288714387(__this, method) ((  void (*) (Collection_1_t2639741171 *, const MethodInfo*))Collection_1_Clear_m3391473100_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<DonateButton>::ClearItems()
#define Collection_1_ClearItems_m1866683983(__this, method) ((  void (*) (Collection_1_t2639741171 *, const MethodInfo*))Collection_1_ClearItems_m2738199222_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<DonateButton>::Contains(T)
#define Collection_1_Contains_m1786990469(__this, ___item0, method) ((  bool (*) (Collection_1_t2639741171 *, DonateButton_t670753441 *, const MethodInfo*))Collection_1_Contains_m1050871674_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<DonateButton>::CopyTo(T[],System.Int32)
#define Collection_1_CopyTo_m3400585543(__this, ___array0, ___index1, method) ((  void (*) (Collection_1_t2639741171 *, DonateButtonU5BU5D_t3829645756*, int32_t, const MethodInfo*))Collection_1_CopyTo_m1746187054_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.ObjectModel.Collection`1<DonateButton>::GetEnumerator()
#define Collection_1_GetEnumerator_m1533134812(__this, method) ((  Il2CppObject* (*) (Collection_1_t2639741171 *, const MethodInfo*))Collection_1_GetEnumerator_m625631581_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<DonateButton>::IndexOf(T)
#define Collection_1_IndexOf_m3700843091(__this, ___item0, method) ((  int32_t (*) (Collection_1_t2639741171 *, DonateButton_t670753441 *, const MethodInfo*))Collection_1_IndexOf_m3101447730_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<DonateButton>::Insert(System.Int32,T)
#define Collection_1_Insert_m856722814(__this, ___index0, ___item1, method) ((  void (*) (Collection_1_t2639741171 *, int32_t, DonateButton_t670753441 *, const MethodInfo*))Collection_1_Insert_m1208073509_gshared)(__this, ___index0, ___item1, method)
// System.Void System.Collections.ObjectModel.Collection`1<DonateButton>::InsertItem(System.Int32,T)
#define Collection_1_InsertItem_m1453903025(__this, ___index0, ___item1, method) ((  void (*) (Collection_1_t2639741171 *, int32_t, DonateButton_t670753441 *, const MethodInfo*))Collection_1_InsertItem_m714854616_gshared)(__this, ___index0, ___item1, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<DonateButton>::Remove(T)
#define Collection_1_Remove_m14691520(__this, ___item0, method) ((  bool (*) (Collection_1_t2639741171 *, DonateButton_t670753441 *, const MethodInfo*))Collection_1_Remove_m2181520885_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<DonateButton>::RemoveAt(System.Int32)
#define Collection_1_RemoveAt_m3025542980(__this, ___index0, method) ((  void (*) (Collection_1_t2639741171 *, int32_t, const MethodInfo*))Collection_1_RemoveAt_m3376893675_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.Collection`1<DonateButton>::RemoveItem(System.Int32)
#define Collection_1_RemoveItem_m2753569380(__this, ___index0, method) ((  void (*) (Collection_1_t2639741171 *, int32_t, const MethodInfo*))Collection_1_RemoveItem_m1099170891_gshared)(__this, ___index0, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<DonateButton>::get_Count()
#define Collection_1_get_Count_m2874293640(__this, method) ((  int32_t (*) (Collection_1_t2639741171 *, const MethodInfo*))Collection_1_get_Count_m1472906633_gshared)(__this, method)
// T System.Collections.ObjectModel.Collection`1<DonateButton>::get_Item(System.Int32)
#define Collection_1_get_Item_m158390314(__this, ___index0, method) ((  DonateButton_t670753441 * (*) (Collection_1_t2639741171 *, int32_t, const MethodInfo*))Collection_1_get_Item_m2356360623_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.Collection`1<DonateButton>::set_Item(System.Int32,T)
#define Collection_1_set_Item_m486500053(__this, ___index0, ___value1, method) ((  void (*) (Collection_1_t2639741171 *, int32_t, DonateButton_t670753441 *, const MethodInfo*))Collection_1_set_Item_m3127068860_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.ObjectModel.Collection`1<DonateButton>::SetItem(System.Int32,T)
#define Collection_1_SetItem_m2105193220(__this, ___index0, ___item1, method) ((  void (*) (Collection_1_t2639741171 *, int32_t, DonateButton_t670753441 *, const MethodInfo*))Collection_1_SetItem_m112162877_gshared)(__this, ___index0, ___item1, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<DonateButton>::IsValidItem(System.Object)
#define Collection_1_IsValidItem_m3332212743(__this /* static, unused */, ___item0, method) ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))Collection_1_IsValidItem_m1993492338_gshared)(__this /* static, unused */, ___item0, method)
// T System.Collections.ObjectModel.Collection`1<DonateButton>::ConvertItem(System.Object)
#define Collection_1_ConvertItem_m181297673(__this /* static, unused */, ___item0, method) ((  DonateButton_t670753441 * (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))Collection_1_ConvertItem_m1655469326_gshared)(__this /* static, unused */, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<DonateButton>::CheckWritable(System.Collections.Generic.IList`1<T>)
#define Collection_1_CheckWritable_m755088007(__this /* static, unused */, ___list0, method) ((  void (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Collection_1_CheckWritable_m651250670_gshared)(__this /* static, unused */, ___list0, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<DonateButton>::IsSynchronized(System.Collections.Generic.IList`1<T>)
#define Collection_1_IsSynchronized_m671837789(__this /* static, unused */, ___list0, method) ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Collection_1_IsSynchronized_m2469749778_gshared)(__this /* static, unused */, ___list0, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<DonateButton>::IsFixedSize(System.Collections.Generic.IList`1<T>)
#define Collection_1_IsFixedSize_m1245691234(__this /* static, unused */, ___list0, method) ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Collection_1_IsFixedSize_m3893865421_gshared)(__this /* static, unused */, ___list0, method)
