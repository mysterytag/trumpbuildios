﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// LitJson.ExporterFunc`1<System.Object>
struct ExporterFunc_1_t4015841116;
// System.Object
struct Il2CppObject;
// LitJson.JsonWriter
struct JsonWriter_t3021344960;
// System.IAsyncResult
struct IAsyncResult_t537683269;
// System.AsyncCallback
struct AsyncCallback_t1363551830;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_IntPtr676692020.h"
#include "AssemblyU2DCSharp_LitJson_JsonWriter3021344960.h"
#include "mscorlib_System_AsyncCallback1363551830.h"

// System.Void LitJson.ExporterFunc`1<System.Object>::.ctor(System.Object,System.IntPtr)
extern "C"  void ExporterFunc_1__ctor_m1424915150_gshared (ExporterFunc_1_t4015841116 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method);
#define ExporterFunc_1__ctor_m1424915150(__this, ___object0, ___method1, method) ((  void (*) (ExporterFunc_1_t4015841116 *, Il2CppObject *, IntPtr_t, const MethodInfo*))ExporterFunc_1__ctor_m1424915150_gshared)(__this, ___object0, ___method1, method)
// System.Void LitJson.ExporterFunc`1<System.Object>::Invoke(T,LitJson.JsonWriter)
extern "C"  void ExporterFunc_1_Invoke_m3275607590_gshared (ExporterFunc_1_t4015841116 * __this, Il2CppObject * ___obj0, JsonWriter_t3021344960 * ___writer1, const MethodInfo* method);
#define ExporterFunc_1_Invoke_m3275607590(__this, ___obj0, ___writer1, method) ((  void (*) (ExporterFunc_1_t4015841116 *, Il2CppObject *, JsonWriter_t3021344960 *, const MethodInfo*))ExporterFunc_1_Invoke_m3275607590_gshared)(__this, ___obj0, ___writer1, method)
// System.IAsyncResult LitJson.ExporterFunc`1<System.Object>::BeginInvoke(T,LitJson.JsonWriter,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * ExporterFunc_1_BeginInvoke_m3759174045_gshared (ExporterFunc_1_t4015841116 * __this, Il2CppObject * ___obj0, JsonWriter_t3021344960 * ___writer1, AsyncCallback_t1363551830 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method);
#define ExporterFunc_1_BeginInvoke_m3759174045(__this, ___obj0, ___writer1, ___callback2, ___object3, method) ((  Il2CppObject * (*) (ExporterFunc_1_t4015841116 *, Il2CppObject *, JsonWriter_t3021344960 *, AsyncCallback_t1363551830 *, Il2CppObject *, const MethodInfo*))ExporterFunc_1_BeginInvoke_m3759174045_gshared)(__this, ___obj0, ___writer1, ___callback2, ___object3, method)
// System.Void LitJson.ExporterFunc`1<System.Object>::EndInvoke(System.IAsyncResult)
extern "C"  void ExporterFunc_1_EndInvoke_m2254144478_gshared (ExporterFunc_1_t4015841116 * __this, Il2CppObject * ___result0, const MethodInfo* method);
#define ExporterFunc_1_EndInvoke_m2254144478(__this, ___result0, method) ((  void (*) (ExporterFunc_1_t4015841116 *, Il2CppObject *, const MethodInfo*))ExporterFunc_1_EndInvoke_m2254144478_gshared)(__this, ___result0, method)
