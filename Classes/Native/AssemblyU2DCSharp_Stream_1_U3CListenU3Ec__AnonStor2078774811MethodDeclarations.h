﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Stream`1/<Listen>c__AnonStorey128<System.Int32>
struct U3CListenU3Ec__AnonStorey128_t2078774811;

#include "codegen/il2cpp-codegen.h"

// System.Void Stream`1/<Listen>c__AnonStorey128<System.Int32>::.ctor()
extern "C"  void U3CListenU3Ec__AnonStorey128__ctor_m3688431535_gshared (U3CListenU3Ec__AnonStorey128_t2078774811 * __this, const MethodInfo* method);
#define U3CListenU3Ec__AnonStorey128__ctor_m3688431535(__this, method) ((  void (*) (U3CListenU3Ec__AnonStorey128_t2078774811 *, const MethodInfo*))U3CListenU3Ec__AnonStorey128__ctor_m3688431535_gshared)(__this, method)
// System.Void Stream`1/<Listen>c__AnonStorey128<System.Int32>::<>m__1B9(T)
extern "C"  void U3CListenU3Ec__AnonStorey128_U3CU3Em__1B9_m2962633548_gshared (U3CListenU3Ec__AnonStorey128_t2078774811 * __this, int32_t ____0, const MethodInfo* method);
#define U3CListenU3Ec__AnonStorey128_U3CU3Em__1B9_m2962633548(__this, ____0, method) ((  void (*) (U3CListenU3Ec__AnonStorey128_t2078774811 *, int32_t, const MethodInfo*))U3CListenU3Ec__AnonStorey128_U3CU3Em__1B9_m2962633548_gshared)(__this, ____0, method)
