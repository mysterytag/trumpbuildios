﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Func`2<System.Object,ICell`1<System.Boolean>>
struct Func_2_t3061313250;
// ZergRush.InorganicCollection`1<System.Object>
struct InorganicCollection_1_t3450977518;
// System.Collections.Generic.Dictionary`2<System.Object,System.IDisposable>
struct Dictionary_2_t321272808;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ZergRush.ReactiveCollectionExtensions/<Filter>c__AnonStorey11D`1<System.Object>
struct  U3CFilterU3Ec__AnonStorey11D_1_t441028593  : public Il2CppObject
{
public:
	// System.Func`2<T,ICell`1<System.Boolean>> ZergRush.ReactiveCollectionExtensions/<Filter>c__AnonStorey11D`1::predicate
	Func_2_t3061313250 * ___predicate_0;
	// ZergRush.InorganicCollection`1<T> ZergRush.ReactiveCollectionExtensions/<Filter>c__AnonStorey11D`1::inorganicCollection
	InorganicCollection_1_t3450977518 * ___inorganicCollection_1;
	// System.Collections.Generic.Dictionary`2<T,System.IDisposable> ZergRush.ReactiveCollectionExtensions/<Filter>c__AnonStorey11D`1::disposables
	Dictionary_2_t321272808 * ___disposables_2;

public:
	inline static int32_t get_offset_of_predicate_0() { return static_cast<int32_t>(offsetof(U3CFilterU3Ec__AnonStorey11D_1_t441028593, ___predicate_0)); }
	inline Func_2_t3061313250 * get_predicate_0() const { return ___predicate_0; }
	inline Func_2_t3061313250 ** get_address_of_predicate_0() { return &___predicate_0; }
	inline void set_predicate_0(Func_2_t3061313250 * value)
	{
		___predicate_0 = value;
		Il2CppCodeGenWriteBarrier(&___predicate_0, value);
	}

	inline static int32_t get_offset_of_inorganicCollection_1() { return static_cast<int32_t>(offsetof(U3CFilterU3Ec__AnonStorey11D_1_t441028593, ___inorganicCollection_1)); }
	inline InorganicCollection_1_t3450977518 * get_inorganicCollection_1() const { return ___inorganicCollection_1; }
	inline InorganicCollection_1_t3450977518 ** get_address_of_inorganicCollection_1() { return &___inorganicCollection_1; }
	inline void set_inorganicCollection_1(InorganicCollection_1_t3450977518 * value)
	{
		___inorganicCollection_1 = value;
		Il2CppCodeGenWriteBarrier(&___inorganicCollection_1, value);
	}

	inline static int32_t get_offset_of_disposables_2() { return static_cast<int32_t>(offsetof(U3CFilterU3Ec__AnonStorey11D_1_t441028593, ___disposables_2)); }
	inline Dictionary_2_t321272808 * get_disposables_2() const { return ___disposables_2; }
	inline Dictionary_2_t321272808 ** get_address_of_disposables_2() { return &___disposables_2; }
	inline void set_disposables_2(Dictionary_2_t321272808 * value)
	{
		___disposables_2 = value;
		Il2CppCodeGenWriteBarrier(&___disposables_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
