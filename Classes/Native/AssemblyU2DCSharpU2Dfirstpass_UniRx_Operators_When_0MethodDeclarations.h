﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.WhenAllObservable`1/WhenAll_/WhenAllCollectionObserver<System.Object>
struct WhenAllCollectionObserver_t3737216925;
// UniRx.Operators.WhenAllObservable`1/WhenAll_<System.Object>
struct WhenAll__t2851103505;
// System.Object
struct Il2CppObject;
// System.Exception
struct Exception_t1967233988;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_Exception1967233988.h"

// System.Void UniRx.Operators.WhenAllObservable`1/WhenAll_/WhenAllCollectionObserver<System.Object>::.ctor(UniRx.Operators.WhenAllObservable`1/WhenAll_<T>,System.Int32)
extern "C"  void WhenAllCollectionObserver__ctor_m1667456594_gshared (WhenAllCollectionObserver_t3737216925 * __this, WhenAll__t2851103505 * ___parent0, int32_t ___index1, const MethodInfo* method);
#define WhenAllCollectionObserver__ctor_m1667456594(__this, ___parent0, ___index1, method) ((  void (*) (WhenAllCollectionObserver_t3737216925 *, WhenAll__t2851103505 *, int32_t, const MethodInfo*))WhenAllCollectionObserver__ctor_m1667456594_gshared)(__this, ___parent0, ___index1, method)
// System.Void UniRx.Operators.WhenAllObservable`1/WhenAll_/WhenAllCollectionObserver<System.Object>::OnNext(T)
extern "C"  void WhenAllCollectionObserver_OnNext_m4014831986_gshared (WhenAllCollectionObserver_t3737216925 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define WhenAllCollectionObserver_OnNext_m4014831986(__this, ___value0, method) ((  void (*) (WhenAllCollectionObserver_t3737216925 *, Il2CppObject *, const MethodInfo*))WhenAllCollectionObserver_OnNext_m4014831986_gshared)(__this, ___value0, method)
// System.Void UniRx.Operators.WhenAllObservable`1/WhenAll_/WhenAllCollectionObserver<System.Object>::OnError(System.Exception)
extern "C"  void WhenAllCollectionObserver_OnError_m3592712321_gshared (WhenAllCollectionObserver_t3737216925 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method);
#define WhenAllCollectionObserver_OnError_m3592712321(__this, ___error0, method) ((  void (*) (WhenAllCollectionObserver_t3737216925 *, Exception_t1967233988 *, const MethodInfo*))WhenAllCollectionObserver_OnError_m3592712321_gshared)(__this, ___error0, method)
// System.Void UniRx.Operators.WhenAllObservable`1/WhenAll_/WhenAllCollectionObserver<System.Object>::OnCompleted()
extern "C"  void WhenAllCollectionObserver_OnCompleted_m2235506004_gshared (WhenAllCollectionObserver_t3737216925 * __this, const MethodInfo* method);
#define WhenAllCollectionObserver_OnCompleted_m2235506004(__this, method) ((  void (*) (WhenAllCollectionObserver_t3737216925 *, const MethodInfo*))WhenAllCollectionObserver_OnCompleted_m2235506004_gshared)(__this, method)
