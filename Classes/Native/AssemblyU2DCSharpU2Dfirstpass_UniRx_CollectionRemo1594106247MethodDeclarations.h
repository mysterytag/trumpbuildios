﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_CollectionRemo4069760419MethodDeclarations.h"

// System.Void UniRx.CollectionRemoveEvent`1<UniRx.Tuple`2<System.String,UnityEngine.Sprite>>::.ctor(System.Int32,T)
#define CollectionRemoveEvent_1__ctor_m1307048827(__this, ___index0, ___value1, method) ((  void (*) (CollectionRemoveEvent_1_t1594106247 *, int32_t, Tuple_2_t2188574943 , const MethodInfo*))CollectionRemoveEvent_1__ctor_m570772917_gshared)(__this, ___index0, ___value1, method)
// System.Int32 UniRx.CollectionRemoveEvent`1<UniRx.Tuple`2<System.String,UnityEngine.Sprite>>::get_Index()
#define CollectionRemoveEvent_1_get_Index_m1711100579(__this, method) ((  int32_t (*) (CollectionRemoveEvent_1_t1594106247 *, const MethodInfo*))CollectionRemoveEvent_1_get_Index_m3875147401_gshared)(__this, method)
// System.Void UniRx.CollectionRemoveEvent`1<UniRx.Tuple`2<System.String,UnityEngine.Sprite>>::set_Index(System.Int32)
#define CollectionRemoveEvent_1_set_Index_m453615926(__this, ___value0, method) ((  void (*) (CollectionRemoveEvent_1_t1594106247 *, int32_t, const MethodInfo*))CollectionRemoveEvent_1_set_Index_m1562070256_gshared)(__this, ___value0, method)
// T UniRx.CollectionRemoveEvent`1<UniRx.Tuple`2<System.String,UnityEngine.Sprite>>::get_Value()
#define CollectionRemoveEvent_1_get_Value_m2640584025(__this, method) ((  Tuple_2_t2188574943  (*) (CollectionRemoveEvent_1_t1594106247 *, const MethodInfo*))CollectionRemoveEvent_1_get_Value_m3444388657_gshared)(__this, method)
// System.Void UniRx.CollectionRemoveEvent`1<UniRx.Tuple`2<System.String,UnityEngine.Sprite>>::set_Value(T)
#define CollectionRemoveEvent_1_set_Value_m828274074(__this, ___value0, method) ((  void (*) (CollectionRemoveEvent_1_t1594106247 *, Tuple_2_t2188574943 , const MethodInfo*))CollectionRemoveEvent_1_set_Value_m2422397472_gshared)(__this, ___value0, method)
// System.String UniRx.CollectionRemoveEvent`1<UniRx.Tuple`2<System.String,UnityEngine.Sprite>>::ToString()
#define CollectionRemoveEvent_1_ToString_m3895751425(__this, method) ((  String_t* (*) (CollectionRemoveEvent_1_t1594106247 *, const MethodInfo*))CollectionRemoveEvent_1_ToString_m1076386913_gshared)(__this, method)
// System.Int32 UniRx.CollectionRemoveEvent`1<UniRx.Tuple`2<System.String,UnityEngine.Sprite>>::GetHashCode()
#define CollectionRemoveEvent_1_GetHashCode_m2684028235(__this, method) ((  int32_t (*) (CollectionRemoveEvent_1_t1594106247 *, const MethodInfo*))CollectionRemoveEvent_1_GetHashCode_m3568852913_gshared)(__this, method)
// System.Boolean UniRx.CollectionRemoveEvent`1<UniRx.Tuple`2<System.String,UnityEngine.Sprite>>::Equals(UniRx.CollectionRemoveEvent`1<T>)
#define CollectionRemoveEvent_1_Equals_m119620248(__this, ___other0, method) ((  bool (*) (CollectionRemoveEvent_1_t1594106247 *, CollectionRemoveEvent_1_t1594106247 , const MethodInfo*))CollectionRemoveEvent_1_Equals_m2262826342_gshared)(__this, ___other0, method)
