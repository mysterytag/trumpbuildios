﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.ClientModels.SetFriendTagsResult
struct SetFriendTagsResult_t4117117766;

#include "codegen/il2cpp-codegen.h"

// System.Void PlayFab.ClientModels.SetFriendTagsResult::.ctor()
extern "C"  void SetFriendTagsResult__ctor_m2700365859 (SetFriendTagsResult_t4117117766 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
