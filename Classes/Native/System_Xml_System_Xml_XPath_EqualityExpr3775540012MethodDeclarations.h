﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Xml.XPath.EqualityExpr
struct EqualityExpr_t3775540012;
// System.Xml.XPath.Expression
struct Expression_t4217024437;
// System.Xml.XPath.BaseIterator
struct BaseIterator_t3696600956;

#include "codegen/il2cpp-codegen.h"
#include "System_Xml_System_Xml_XPath_Expression4217024437.h"
#include "System_Xml_System_Xml_XPath_BaseIterator3696600956.h"

// System.Void System.Xml.XPath.EqualityExpr::.ctor(System.Xml.XPath.Expression,System.Xml.XPath.Expression,System.Boolean)
extern "C"  void EqualityExpr__ctor_m2210778824 (EqualityExpr_t3775540012 * __this, Expression_t4217024437 * ___left0, Expression_t4217024437 * ___right1, bool ___trueVal2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Xml.XPath.EqualityExpr::get_StaticValueAsBoolean()
extern "C"  bool EqualityExpr_get_StaticValueAsBoolean_m405219309 (EqualityExpr_t3775540012 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Xml.XPath.EqualityExpr::EvaluateBoolean(System.Xml.XPath.BaseIterator)
extern "C"  bool EqualityExpr_EvaluateBoolean_m2691361612 (EqualityExpr_t3775540012 * __this, BaseIterator_t3696600956 * ___iter0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
