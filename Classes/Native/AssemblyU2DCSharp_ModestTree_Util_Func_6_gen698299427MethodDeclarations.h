﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ModestTree.Util.Func`6<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>
struct Func_6_t698299427;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t537683269;
// System.AsyncCallback
struct AsyncCallback_t1363551830;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_IntPtr676692020.h"
#include "mscorlib_System_AsyncCallback1363551830.h"

// System.Void ModestTree.Util.Func`6<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>::.ctor(System.Object,System.IntPtr)
extern "C"  void Func_6__ctor_m1024459519_gshared (Func_6_t698299427 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method);
#define Func_6__ctor_m1024459519(__this, ___object0, ___method1, method) ((  void (*) (Func_6_t698299427 *, Il2CppObject *, IntPtr_t, const MethodInfo*))Func_6__ctor_m1024459519_gshared)(__this, ___object0, ___method1, method)
// TResult ModestTree.Util.Func`6<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>::Invoke(T1,T2,T3,T4,T5)
extern "C"  Il2CppObject * Func_6_Invoke_m272686264_gshared (Func_6_t698299427 * __this, Il2CppObject * ___arg10, Il2CppObject * ___arg21, Il2CppObject * ___arg32, Il2CppObject * ___arg43, Il2CppObject * ___arg54, const MethodInfo* method);
#define Func_6_Invoke_m272686264(__this, ___arg10, ___arg21, ___arg32, ___arg43, ___arg54, method) ((  Il2CppObject * (*) (Func_6_t698299427 *, Il2CppObject *, Il2CppObject *, Il2CppObject *, Il2CppObject *, Il2CppObject *, const MethodInfo*))Func_6_Invoke_m272686264_gshared)(__this, ___arg10, ___arg21, ___arg32, ___arg43, ___arg54, method)
// System.IAsyncResult ModestTree.Util.Func`6<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>::BeginInvoke(T1,T2,T3,T4,T5,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * Func_6_BeginInvoke_m2607504747_gshared (Func_6_t698299427 * __this, Il2CppObject * ___arg10, Il2CppObject * ___arg21, Il2CppObject * ___arg32, Il2CppObject * ___arg43, Il2CppObject * ___arg54, AsyncCallback_t1363551830 * ___callback5, Il2CppObject * ___object6, const MethodInfo* method);
#define Func_6_BeginInvoke_m2607504747(__this, ___arg10, ___arg21, ___arg32, ___arg43, ___arg54, ___callback5, ___object6, method) ((  Il2CppObject * (*) (Func_6_t698299427 *, Il2CppObject *, Il2CppObject *, Il2CppObject *, Il2CppObject *, Il2CppObject *, AsyncCallback_t1363551830 *, Il2CppObject *, const MethodInfo*))Func_6_BeginInvoke_m2607504747_gshared)(__this, ___arg10, ___arg21, ___arg32, ___arg43, ___arg54, ___callback5, ___object6, method)
// TResult ModestTree.Util.Func`6<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>::EndInvoke(System.IAsyncResult)
extern "C"  Il2CppObject * Func_6_EndInvoke_m857310637_gshared (Func_6_t698299427 * __this, Il2CppObject * ___result0, const MethodInfo* method);
#define Func_6_EndInvoke_m857310637(__this, ___result0, method) ((  Il2CppObject * (*) (Func_6_t698299427 *, Il2CppObject *, const MethodInfo*))Func_6_EndInvoke_m857310637_gshared)(__this, ___result0, method)
