﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Xml.XPath.XPathFunctionBoolean
struct XPathFunctionBoolean_t3213667280;
// System.Xml.XPath.FunctionArguments
struct FunctionArguments_t3402201403;
// System.Object
struct Il2CppObject;
// System.Xml.XPath.BaseIterator
struct BaseIterator_t3696600956;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "System_Xml_System_Xml_XPath_FunctionArguments3402201403.h"
#include "System_Xml_System_Xml_XPath_BaseIterator3696600956.h"

// System.Void System.Xml.XPath.XPathFunctionBoolean::.ctor(System.Xml.XPath.FunctionArguments)
extern "C"  void XPathFunctionBoolean__ctor_m2311468944 (XPathFunctionBoolean_t3213667280 * __this, FunctionArguments_t3402201403 * ___args0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Xml.XPath.XPathFunctionBoolean::get_Peer()
extern "C"  bool XPathFunctionBoolean_get_Peer_m4075223552 (XPathFunctionBoolean_t3213667280 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Xml.XPath.XPathFunctionBoolean::Evaluate(System.Xml.XPath.BaseIterator)
extern "C"  Il2CppObject * XPathFunctionBoolean_Evaluate_m2675549483 (XPathFunctionBoolean_t3213667280 * __this, BaseIterator_t3696600956 * ___iter0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Xml.XPath.XPathFunctionBoolean::ToString()
extern "C"  String_t* XPathFunctionBoolean_ToString_m2171607622 (XPathFunctionBoolean_t3213667280 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
