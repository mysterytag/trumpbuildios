﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Action
struct Action_t437523947;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Disposable/AnonymousDisposable
struct  AnonymousDisposable_t1367871885  : public Il2CppObject
{
public:
	// System.Boolean UniRx.Disposable/AnonymousDisposable::isDisposed
	bool ___isDisposed_0;
	// System.Action UniRx.Disposable/AnonymousDisposable::dispose
	Action_t437523947 * ___dispose_1;

public:
	inline static int32_t get_offset_of_isDisposed_0() { return static_cast<int32_t>(offsetof(AnonymousDisposable_t1367871885, ___isDisposed_0)); }
	inline bool get_isDisposed_0() const { return ___isDisposed_0; }
	inline bool* get_address_of_isDisposed_0() { return &___isDisposed_0; }
	inline void set_isDisposed_0(bool value)
	{
		___isDisposed_0 = value;
	}

	inline static int32_t get_offset_of_dispose_1() { return static_cast<int32_t>(offsetof(AnonymousDisposable_t1367871885, ___dispose_1)); }
	inline Action_t437523947 * get_dispose_1() const { return ___dispose_1; }
	inline Action_t437523947 ** get_address_of_dispose_1() { return &___dispose_1; }
	inline void set_dispose_1(Action_t437523947 * value)
	{
		___dispose_1 = value;
		Il2CppCodeGenWriteBarrier(&___dispose_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
