﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.ObservableWWW/<LoadFromCacheOrDownload>c__AnonStorey8E
struct U3CLoadFromCacheOrDownloadU3Ec__AnonStorey8E_t1059502551;
// System.Collections.IEnumerator
struct IEnumerator_t287207039;
// UniRx.IObserver`1<UnityEngine.AssetBundle>
struct IObserver_1_t1876462710;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_CancellationTo1439151560.h"

// System.Void UniRx.ObservableWWW/<LoadFromCacheOrDownload>c__AnonStorey8E::.ctor()
extern "C"  void U3CLoadFromCacheOrDownloadU3Ec__AnonStorey8E__ctor_m2544272517 (U3CLoadFromCacheOrDownloadU3Ec__AnonStorey8E_t1059502551 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator UniRx.ObservableWWW/<LoadFromCacheOrDownload>c__AnonStorey8E::<>m__BA(UniRx.IObserver`1<UnityEngine.AssetBundle>,UniRx.CancellationToken)
extern "C"  Il2CppObject * U3CLoadFromCacheOrDownloadU3Ec__AnonStorey8E_U3CU3Em__BA_m2464018692 (U3CLoadFromCacheOrDownloadU3Ec__AnonStorey8E_t1059502551 * __this, Il2CppObject* ___observer0, CancellationToken_t1439151560  ___cancellation1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
