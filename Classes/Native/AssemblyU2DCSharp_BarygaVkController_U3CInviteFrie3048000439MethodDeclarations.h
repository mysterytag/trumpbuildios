﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// BarygaVkController/<InviteFriend>c__AnonStoreyE0
struct U3CInviteFriendU3Ec__AnonStoreyE0_t3048000439;

#include "codegen/il2cpp-codegen.h"

// System.Void BarygaVkController/<InviteFriend>c__AnonStoreyE0::.ctor()
extern "C"  void U3CInviteFriendU3Ec__AnonStoreyE0__ctor_m484459010 (U3CInviteFriendU3Ec__AnonStoreyE0_t3048000439 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BarygaVkController/<InviteFriend>c__AnonStoreyE0::<>m__F6(System.Boolean)
extern "C"  void U3CInviteFriendU3Ec__AnonStoreyE0_U3CU3Em__F6_m401552178 (U3CInviteFriendU3Ec__AnonStoreyE0_t3048000439 * __this, bool ___ok0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
