﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.InternalUtil.EmptyObserver`1<UnityEngine.NetworkConnectionError>
struct EmptyObserver_1_t433158983;
// System.Exception
struct Exception_t1967233988;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Exception1967233988.h"
#include "UnityEngine_UnityEngine_NetworkConnectionError1023805993.h"

// System.Void UniRx.InternalUtil.EmptyObserver`1<UnityEngine.NetworkConnectionError>::.ctor()
extern "C"  void EmptyObserver_1__ctor_m2403898531_gshared (EmptyObserver_1_t433158983 * __this, const MethodInfo* method);
#define EmptyObserver_1__ctor_m2403898531(__this, method) ((  void (*) (EmptyObserver_1_t433158983 *, const MethodInfo*))EmptyObserver_1__ctor_m2403898531_gshared)(__this, method)
// System.Void UniRx.InternalUtil.EmptyObserver`1<UnityEngine.NetworkConnectionError>::.cctor()
extern "C"  void EmptyObserver_1__cctor_m1024314218_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define EmptyObserver_1__cctor_m1024314218(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))EmptyObserver_1__cctor_m1024314218_gshared)(__this /* static, unused */, method)
// System.Void UniRx.InternalUtil.EmptyObserver`1<UnityEngine.NetworkConnectionError>::OnCompleted()
extern "C"  void EmptyObserver_1_OnCompleted_m856457709_gshared (EmptyObserver_1_t433158983 * __this, const MethodInfo* method);
#define EmptyObserver_1_OnCompleted_m856457709(__this, method) ((  void (*) (EmptyObserver_1_t433158983 *, const MethodInfo*))EmptyObserver_1_OnCompleted_m856457709_gshared)(__this, method)
// System.Void UniRx.InternalUtil.EmptyObserver`1<UnityEngine.NetworkConnectionError>::OnError(System.Exception)
extern "C"  void EmptyObserver_1_OnError_m4081760154_gshared (EmptyObserver_1_t433158983 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method);
#define EmptyObserver_1_OnError_m4081760154(__this, ___error0, method) ((  void (*) (EmptyObserver_1_t433158983 *, Exception_t1967233988 *, const MethodInfo*))EmptyObserver_1_OnError_m4081760154_gshared)(__this, ___error0, method)
// System.Void UniRx.InternalUtil.EmptyObserver`1<UnityEngine.NetworkConnectionError>::OnNext(T)
extern "C"  void EmptyObserver_1_OnNext_m2284786315_gshared (EmptyObserver_1_t433158983 * __this, int32_t ___value0, const MethodInfo* method);
#define EmptyObserver_1_OnNext_m2284786315(__this, ___value0, method) ((  void (*) (EmptyObserver_1_t433158983 *, int32_t, const MethodInfo*))EmptyObserver_1_OnNext_m2284786315_gshared)(__this, ___value0, method)
