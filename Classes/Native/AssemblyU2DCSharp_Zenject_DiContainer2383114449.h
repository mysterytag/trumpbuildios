﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Generic.Dictionary`2<Zenject.BindingId,System.Collections.Generic.List`1<Zenject.ProviderBase>>
struct Dictionary_2_t2602827573;
// Zenject.SingletonProviderMap
struct SingletonProviderMap_t1557411893;
// System.Collections.Generic.List`1<Zenject.IInstaller>
struct List_1_t3252182445;
// System.Collections.Generic.Stack`1<System.Type>
struct Stack_1_t1054668674;
// UnityEngine.Transform
struct Transform_t284553113;
// Zenject.ProviderBase
struct ProviderBase_t1627494391;
// System.Func`2<System.Collections.Generic.KeyValuePair`2<Zenject.BindingId,System.Collections.Generic.List`1<Zenject.ProviderBase>>,System.Collections.Generic.IEnumerable`1<Zenject.ProviderBase>>
struct Func_2_t2274780192;
// System.Func`3<System.Collections.Generic.KeyValuePair`2<Zenject.BindingId,System.Collections.Generic.List`1<Zenject.ProviderBase>>,Zenject.ProviderBase,System.Type>
struct Func_3_t2039943812;
// System.Func`2<System.Type,System.Boolean>
struct Func_2_t634911258;
// System.Func`2<System.Collections.Generic.KeyValuePair`2<Zenject.BindingId,System.Collections.Generic.List`1<Zenject.ProviderBase>>,System.Boolean>
struct Func_2_t2281104082;
// System.Func`2<System.Collections.Generic.KeyValuePair`2<Zenject.BindingId,System.Collections.Generic.List`1<Zenject.ProviderBase>>,Zenject.BindingId>
struct Func_2_t740925706;
// System.Func`2<Zenject.ProviderBase,System.Type>
struct Func_2_t1545429828;
// System.Func`2<Zenject.ProviderBase,System.Boolean>
struct Func_2_t3272172530;
// System.Func`2<Zenject.TypeValuePair,System.String>
struct Func_2_t543852880;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.DiContainer
struct  DiContainer_t2383114449  : public Il2CppObject
{
public:
	// System.Collections.Generic.Dictionary`2<Zenject.BindingId,System.Collections.Generic.List`1<Zenject.ProviderBase>> Zenject.DiContainer::_providers
	Dictionary_2_t2602827573 * ____providers_0;
	// Zenject.SingletonProviderMap Zenject.DiContainer::_singletonMap
	SingletonProviderMap_t1557411893 * ____singletonMap_1;
	// System.Collections.Generic.List`1<Zenject.IInstaller> Zenject.DiContainer::_installedInstallers
	List_1_t3252182445 * ____installedInstallers_2;
	// System.Collections.Generic.Stack`1<System.Type> Zenject.DiContainer::_instantiatesInProgress
	Stack_1_t1054668674 * ____instantiatesInProgress_3;
	// UnityEngine.Transform Zenject.DiContainer::_rootTransform
	Transform_t284553113 * ____rootTransform_4;
	// System.Boolean Zenject.DiContainer::_allowNullBindings
	bool ____allowNullBindings_5;
	// Zenject.ProviderBase Zenject.DiContainer::_fallbackProvider
	ProviderBase_t1627494391 * ____fallbackProvider_6;

public:
	inline static int32_t get_offset_of__providers_0() { return static_cast<int32_t>(offsetof(DiContainer_t2383114449, ____providers_0)); }
	inline Dictionary_2_t2602827573 * get__providers_0() const { return ____providers_0; }
	inline Dictionary_2_t2602827573 ** get_address_of__providers_0() { return &____providers_0; }
	inline void set__providers_0(Dictionary_2_t2602827573 * value)
	{
		____providers_0 = value;
		Il2CppCodeGenWriteBarrier(&____providers_0, value);
	}

	inline static int32_t get_offset_of__singletonMap_1() { return static_cast<int32_t>(offsetof(DiContainer_t2383114449, ____singletonMap_1)); }
	inline SingletonProviderMap_t1557411893 * get__singletonMap_1() const { return ____singletonMap_1; }
	inline SingletonProviderMap_t1557411893 ** get_address_of__singletonMap_1() { return &____singletonMap_1; }
	inline void set__singletonMap_1(SingletonProviderMap_t1557411893 * value)
	{
		____singletonMap_1 = value;
		Il2CppCodeGenWriteBarrier(&____singletonMap_1, value);
	}

	inline static int32_t get_offset_of__installedInstallers_2() { return static_cast<int32_t>(offsetof(DiContainer_t2383114449, ____installedInstallers_2)); }
	inline List_1_t3252182445 * get__installedInstallers_2() const { return ____installedInstallers_2; }
	inline List_1_t3252182445 ** get_address_of__installedInstallers_2() { return &____installedInstallers_2; }
	inline void set__installedInstallers_2(List_1_t3252182445 * value)
	{
		____installedInstallers_2 = value;
		Il2CppCodeGenWriteBarrier(&____installedInstallers_2, value);
	}

	inline static int32_t get_offset_of__instantiatesInProgress_3() { return static_cast<int32_t>(offsetof(DiContainer_t2383114449, ____instantiatesInProgress_3)); }
	inline Stack_1_t1054668674 * get__instantiatesInProgress_3() const { return ____instantiatesInProgress_3; }
	inline Stack_1_t1054668674 ** get_address_of__instantiatesInProgress_3() { return &____instantiatesInProgress_3; }
	inline void set__instantiatesInProgress_3(Stack_1_t1054668674 * value)
	{
		____instantiatesInProgress_3 = value;
		Il2CppCodeGenWriteBarrier(&____instantiatesInProgress_3, value);
	}

	inline static int32_t get_offset_of__rootTransform_4() { return static_cast<int32_t>(offsetof(DiContainer_t2383114449, ____rootTransform_4)); }
	inline Transform_t284553113 * get__rootTransform_4() const { return ____rootTransform_4; }
	inline Transform_t284553113 ** get_address_of__rootTransform_4() { return &____rootTransform_4; }
	inline void set__rootTransform_4(Transform_t284553113 * value)
	{
		____rootTransform_4 = value;
		Il2CppCodeGenWriteBarrier(&____rootTransform_4, value);
	}

	inline static int32_t get_offset_of__allowNullBindings_5() { return static_cast<int32_t>(offsetof(DiContainer_t2383114449, ____allowNullBindings_5)); }
	inline bool get__allowNullBindings_5() const { return ____allowNullBindings_5; }
	inline bool* get_address_of__allowNullBindings_5() { return &____allowNullBindings_5; }
	inline void set__allowNullBindings_5(bool value)
	{
		____allowNullBindings_5 = value;
	}

	inline static int32_t get_offset_of__fallbackProvider_6() { return static_cast<int32_t>(offsetof(DiContainer_t2383114449, ____fallbackProvider_6)); }
	inline ProviderBase_t1627494391 * get__fallbackProvider_6() const { return ____fallbackProvider_6; }
	inline ProviderBase_t1627494391 ** get_address_of__fallbackProvider_6() { return &____fallbackProvider_6; }
	inline void set__fallbackProvider_6(ProviderBase_t1627494391 * value)
	{
		____fallbackProvider_6 = value;
		Il2CppCodeGenWriteBarrier(&____fallbackProvider_6, value);
	}
};

struct DiContainer_t2383114449_StaticFields
{
public:
	// System.Func`2<System.Collections.Generic.KeyValuePair`2<Zenject.BindingId,System.Collections.Generic.List`1<Zenject.ProviderBase>>,System.Collections.Generic.IEnumerable`1<Zenject.ProviderBase>> Zenject.DiContainer::<>f__am$cache7
	Func_2_t2274780192 * ___U3CU3Ef__amU24cache7_7;
	// System.Func`3<System.Collections.Generic.KeyValuePair`2<Zenject.BindingId,System.Collections.Generic.List`1<Zenject.ProviderBase>>,Zenject.ProviderBase,System.Type> Zenject.DiContainer::<>f__am$cache8
	Func_3_t2039943812 * ___U3CU3Ef__amU24cache8_8;
	// System.Func`2<System.Type,System.Boolean> Zenject.DiContainer::<>f__am$cache9
	Func_2_t634911258 * ___U3CU3Ef__amU24cache9_9;
	// System.Func`2<System.Collections.Generic.KeyValuePair`2<Zenject.BindingId,System.Collections.Generic.List`1<Zenject.ProviderBase>>,System.Boolean> Zenject.DiContainer::<>f__am$cacheA
	Func_2_t2281104082 * ___U3CU3Ef__amU24cacheA_10;
	// System.Func`2<System.Collections.Generic.KeyValuePair`2<Zenject.BindingId,System.Collections.Generic.List`1<Zenject.ProviderBase>>,Zenject.BindingId> Zenject.DiContainer::<>f__am$cacheB
	Func_2_t740925706 * ___U3CU3Ef__amU24cacheB_11;
	// System.Func`2<Zenject.ProviderBase,System.Type> Zenject.DiContainer::<>f__am$cacheC
	Func_2_t1545429828 * ___U3CU3Ef__amU24cacheC_12;
	// System.Func`2<System.Type,System.Boolean> Zenject.DiContainer::<>f__am$cacheD
	Func_2_t634911258 * ___U3CU3Ef__amU24cacheD_13;
	// System.Func`2<Zenject.ProviderBase,System.Boolean> Zenject.DiContainer::<>f__am$cacheE
	Func_2_t3272172530 * ___U3CU3Ef__amU24cacheE_14;
	// System.Func`2<Zenject.TypeValuePair,System.String> Zenject.DiContainer::<>f__am$cacheF
	Func_2_t543852880 * ___U3CU3Ef__amU24cacheF_15;
	// System.Func`2<Zenject.TypeValuePair,System.String> Zenject.DiContainer::<>f__am$cache10
	Func_2_t543852880 * ___U3CU3Ef__amU24cache10_16;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache7_7() { return static_cast<int32_t>(offsetof(DiContainer_t2383114449_StaticFields, ___U3CU3Ef__amU24cache7_7)); }
	inline Func_2_t2274780192 * get_U3CU3Ef__amU24cache7_7() const { return ___U3CU3Ef__amU24cache7_7; }
	inline Func_2_t2274780192 ** get_address_of_U3CU3Ef__amU24cache7_7() { return &___U3CU3Ef__amU24cache7_7; }
	inline void set_U3CU3Ef__amU24cache7_7(Func_2_t2274780192 * value)
	{
		___U3CU3Ef__amU24cache7_7 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache7_7, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache8_8() { return static_cast<int32_t>(offsetof(DiContainer_t2383114449_StaticFields, ___U3CU3Ef__amU24cache8_8)); }
	inline Func_3_t2039943812 * get_U3CU3Ef__amU24cache8_8() const { return ___U3CU3Ef__amU24cache8_8; }
	inline Func_3_t2039943812 ** get_address_of_U3CU3Ef__amU24cache8_8() { return &___U3CU3Ef__amU24cache8_8; }
	inline void set_U3CU3Ef__amU24cache8_8(Func_3_t2039943812 * value)
	{
		___U3CU3Ef__amU24cache8_8 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache8_8, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache9_9() { return static_cast<int32_t>(offsetof(DiContainer_t2383114449_StaticFields, ___U3CU3Ef__amU24cache9_9)); }
	inline Func_2_t634911258 * get_U3CU3Ef__amU24cache9_9() const { return ___U3CU3Ef__amU24cache9_9; }
	inline Func_2_t634911258 ** get_address_of_U3CU3Ef__amU24cache9_9() { return &___U3CU3Ef__amU24cache9_9; }
	inline void set_U3CU3Ef__amU24cache9_9(Func_2_t634911258 * value)
	{
		___U3CU3Ef__amU24cache9_9 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache9_9, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cacheA_10() { return static_cast<int32_t>(offsetof(DiContainer_t2383114449_StaticFields, ___U3CU3Ef__amU24cacheA_10)); }
	inline Func_2_t2281104082 * get_U3CU3Ef__amU24cacheA_10() const { return ___U3CU3Ef__amU24cacheA_10; }
	inline Func_2_t2281104082 ** get_address_of_U3CU3Ef__amU24cacheA_10() { return &___U3CU3Ef__amU24cacheA_10; }
	inline void set_U3CU3Ef__amU24cacheA_10(Func_2_t2281104082 * value)
	{
		___U3CU3Ef__amU24cacheA_10 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cacheA_10, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cacheB_11() { return static_cast<int32_t>(offsetof(DiContainer_t2383114449_StaticFields, ___U3CU3Ef__amU24cacheB_11)); }
	inline Func_2_t740925706 * get_U3CU3Ef__amU24cacheB_11() const { return ___U3CU3Ef__amU24cacheB_11; }
	inline Func_2_t740925706 ** get_address_of_U3CU3Ef__amU24cacheB_11() { return &___U3CU3Ef__amU24cacheB_11; }
	inline void set_U3CU3Ef__amU24cacheB_11(Func_2_t740925706 * value)
	{
		___U3CU3Ef__amU24cacheB_11 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cacheB_11, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cacheC_12() { return static_cast<int32_t>(offsetof(DiContainer_t2383114449_StaticFields, ___U3CU3Ef__amU24cacheC_12)); }
	inline Func_2_t1545429828 * get_U3CU3Ef__amU24cacheC_12() const { return ___U3CU3Ef__amU24cacheC_12; }
	inline Func_2_t1545429828 ** get_address_of_U3CU3Ef__amU24cacheC_12() { return &___U3CU3Ef__amU24cacheC_12; }
	inline void set_U3CU3Ef__amU24cacheC_12(Func_2_t1545429828 * value)
	{
		___U3CU3Ef__amU24cacheC_12 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cacheC_12, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cacheD_13() { return static_cast<int32_t>(offsetof(DiContainer_t2383114449_StaticFields, ___U3CU3Ef__amU24cacheD_13)); }
	inline Func_2_t634911258 * get_U3CU3Ef__amU24cacheD_13() const { return ___U3CU3Ef__amU24cacheD_13; }
	inline Func_2_t634911258 ** get_address_of_U3CU3Ef__amU24cacheD_13() { return &___U3CU3Ef__amU24cacheD_13; }
	inline void set_U3CU3Ef__amU24cacheD_13(Func_2_t634911258 * value)
	{
		___U3CU3Ef__amU24cacheD_13 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cacheD_13, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cacheE_14() { return static_cast<int32_t>(offsetof(DiContainer_t2383114449_StaticFields, ___U3CU3Ef__amU24cacheE_14)); }
	inline Func_2_t3272172530 * get_U3CU3Ef__amU24cacheE_14() const { return ___U3CU3Ef__amU24cacheE_14; }
	inline Func_2_t3272172530 ** get_address_of_U3CU3Ef__amU24cacheE_14() { return &___U3CU3Ef__amU24cacheE_14; }
	inline void set_U3CU3Ef__amU24cacheE_14(Func_2_t3272172530 * value)
	{
		___U3CU3Ef__amU24cacheE_14 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cacheE_14, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cacheF_15() { return static_cast<int32_t>(offsetof(DiContainer_t2383114449_StaticFields, ___U3CU3Ef__amU24cacheF_15)); }
	inline Func_2_t543852880 * get_U3CU3Ef__amU24cacheF_15() const { return ___U3CU3Ef__amU24cacheF_15; }
	inline Func_2_t543852880 ** get_address_of_U3CU3Ef__amU24cacheF_15() { return &___U3CU3Ef__amU24cacheF_15; }
	inline void set_U3CU3Ef__amU24cacheF_15(Func_2_t543852880 * value)
	{
		___U3CU3Ef__amU24cacheF_15 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cacheF_15, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache10_16() { return static_cast<int32_t>(offsetof(DiContainer_t2383114449_StaticFields, ___U3CU3Ef__amU24cache10_16)); }
	inline Func_2_t543852880 * get_U3CU3Ef__amU24cache10_16() const { return ___U3CU3Ef__amU24cache10_16; }
	inline Func_2_t543852880 ** get_address_of_U3CU3Ef__amU24cache10_16() { return &___U3CU3Ef__amU24cache10_16; }
	inline void set_U3CU3Ef__amU24cache10_16(Func_2_t543852880 * value)
	{
		___U3CU3Ef__amU24cache10_16 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache10_16, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
