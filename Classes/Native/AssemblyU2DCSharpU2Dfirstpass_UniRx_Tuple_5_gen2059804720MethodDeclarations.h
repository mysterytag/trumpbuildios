﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Object
struct Il2CppObject;
// System.Collections.IComparer
struct IComparer_t2207526184;
// System.Collections.IEqualityComparer
struct IEqualityComparer_t1661793090;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Tuple_5_gen2059804720.h"
#include "mscorlib_System_Object837106420.h"

// System.Void UniRx.Tuple`5<System.Object,System.Object,System.Object,System.Object,System.Object>::.ctor(T1,T2,T3,T4,T5)
extern "C"  void Tuple_5__ctor_m1301458241_gshared (Tuple_5_t2059804720 * __this, Il2CppObject * ___item10, Il2CppObject * ___item21, Il2CppObject * ___item32, Il2CppObject * ___item43, Il2CppObject * ___item54, const MethodInfo* method);
#define Tuple_5__ctor_m1301458241(__this, ___item10, ___item21, ___item32, ___item43, ___item54, method) ((  void (*) (Tuple_5_t2059804720 *, Il2CppObject *, Il2CppObject *, Il2CppObject *, Il2CppObject *, Il2CppObject *, const MethodInfo*))Tuple_5__ctor_m1301458241_gshared)(__this, ___item10, ___item21, ___item32, ___item43, ___item54, method)
// System.Int32 UniRx.Tuple`5<System.Object,System.Object,System.Object,System.Object,System.Object>::System.IComparable.CompareTo(System.Object)
extern "C"  int32_t Tuple_5_System_IComparable_CompareTo_m162996324_gshared (Tuple_5_t2059804720 * __this, Il2CppObject * ___obj0, const MethodInfo* method);
#define Tuple_5_System_IComparable_CompareTo_m162996324(__this, ___obj0, method) ((  int32_t (*) (Tuple_5_t2059804720 *, Il2CppObject *, const MethodInfo*))Tuple_5_System_IComparable_CompareTo_m162996324_gshared)(__this, ___obj0, method)
// System.Int32 UniRx.Tuple`5<System.Object,System.Object,System.Object,System.Object,System.Object>::UniRx.IStructuralComparable.CompareTo(System.Object,System.Collections.IComparer)
extern "C"  int32_t Tuple_5_UniRx_IStructuralComparable_CompareTo_m1191358006_gshared (Tuple_5_t2059804720 * __this, Il2CppObject * ___other0, Il2CppObject * ___comparer1, const MethodInfo* method);
#define Tuple_5_UniRx_IStructuralComparable_CompareTo_m1191358006(__this, ___other0, ___comparer1, method) ((  int32_t (*) (Tuple_5_t2059804720 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))Tuple_5_UniRx_IStructuralComparable_CompareTo_m1191358006_gshared)(__this, ___other0, ___comparer1, method)
// System.Boolean UniRx.Tuple`5<System.Object,System.Object,System.Object,System.Object,System.Object>::UniRx.IStructuralEquatable.Equals(System.Object,System.Collections.IEqualityComparer)
extern "C"  bool Tuple_5_UniRx_IStructuralEquatable_Equals_m1056356385_gshared (Tuple_5_t2059804720 * __this, Il2CppObject * ___other0, Il2CppObject * ___comparer1, const MethodInfo* method);
#define Tuple_5_UniRx_IStructuralEquatable_Equals_m1056356385(__this, ___other0, ___comparer1, method) ((  bool (*) (Tuple_5_t2059804720 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))Tuple_5_UniRx_IStructuralEquatable_Equals_m1056356385_gshared)(__this, ___other0, ___comparer1, method)
// System.Int32 UniRx.Tuple`5<System.Object,System.Object,System.Object,System.Object,System.Object>::UniRx.IStructuralEquatable.GetHashCode(System.Collections.IEqualityComparer)
extern "C"  int32_t Tuple_5_UniRx_IStructuralEquatable_GetHashCode_m2374430573_gshared (Tuple_5_t2059804720 * __this, Il2CppObject * ___comparer0, const MethodInfo* method);
#define Tuple_5_UniRx_IStructuralEquatable_GetHashCode_m2374430573(__this, ___comparer0, method) ((  int32_t (*) (Tuple_5_t2059804720 *, Il2CppObject *, const MethodInfo*))Tuple_5_UniRx_IStructuralEquatable_GetHashCode_m2374430573_gshared)(__this, ___comparer0, method)
// System.String UniRx.Tuple`5<System.Object,System.Object,System.Object,System.Object,System.Object>::UniRx.ITuple.ToString()
extern "C"  String_t* Tuple_5_UniRx_ITuple_ToString_m2424359966_gshared (Tuple_5_t2059804720 * __this, const MethodInfo* method);
#define Tuple_5_UniRx_ITuple_ToString_m2424359966(__this, method) ((  String_t* (*) (Tuple_5_t2059804720 *, const MethodInfo*))Tuple_5_UniRx_ITuple_ToString_m2424359966_gshared)(__this, method)
// T1 UniRx.Tuple`5<System.Object,System.Object,System.Object,System.Object,System.Object>::get_Item1()
extern "C"  Il2CppObject * Tuple_5_get_Item1_m3242957067_gshared (Tuple_5_t2059804720 * __this, const MethodInfo* method);
#define Tuple_5_get_Item1_m3242957067(__this, method) ((  Il2CppObject * (*) (Tuple_5_t2059804720 *, const MethodInfo*))Tuple_5_get_Item1_m3242957067_gshared)(__this, method)
// T2 UniRx.Tuple`5<System.Object,System.Object,System.Object,System.Object,System.Object>::get_Item2()
extern "C"  Il2CppObject * Tuple_5_get_Item2_m3758478477_gshared (Tuple_5_t2059804720 * __this, const MethodInfo* method);
#define Tuple_5_get_Item2_m3758478477(__this, method) ((  Il2CppObject * (*) (Tuple_5_t2059804720 *, const MethodInfo*))Tuple_5_get_Item2_m3758478477_gshared)(__this, method)
// T3 UniRx.Tuple`5<System.Object,System.Object,System.Object,System.Object,System.Object>::get_Item3()
extern "C"  Il2CppObject * Tuple_5_get_Item3_m4273999887_gshared (Tuple_5_t2059804720 * __this, const MethodInfo* method);
#define Tuple_5_get_Item3_m4273999887(__this, method) ((  Il2CppObject * (*) (Tuple_5_t2059804720 *, const MethodInfo*))Tuple_5_get_Item3_m4273999887_gshared)(__this, method)
// T4 UniRx.Tuple`5<System.Object,System.Object,System.Object,System.Object,System.Object>::get_Item4()
extern "C"  Il2CppObject * Tuple_5_get_Item4_m494554001_gshared (Tuple_5_t2059804720 * __this, const MethodInfo* method);
#define Tuple_5_get_Item4_m494554001(__this, method) ((  Il2CppObject * (*) (Tuple_5_t2059804720 *, const MethodInfo*))Tuple_5_get_Item4_m494554001_gshared)(__this, method)
// T5 UniRx.Tuple`5<System.Object,System.Object,System.Object,System.Object,System.Object>::get_Item5()
extern "C"  Il2CppObject * Tuple_5_get_Item5_m1010075411_gshared (Tuple_5_t2059804720 * __this, const MethodInfo* method);
#define Tuple_5_get_Item5_m1010075411(__this, method) ((  Il2CppObject * (*) (Tuple_5_t2059804720 *, const MethodInfo*))Tuple_5_get_Item5_m1010075411_gshared)(__this, method)
// System.Boolean UniRx.Tuple`5<System.Object,System.Object,System.Object,System.Object,System.Object>::Equals(System.Object)
extern "C"  bool Tuple_5_Equals_m3783740117_gshared (Tuple_5_t2059804720 * __this, Il2CppObject * ___obj0, const MethodInfo* method);
#define Tuple_5_Equals_m3783740117(__this, ___obj0, method) ((  bool (*) (Tuple_5_t2059804720 *, Il2CppObject *, const MethodInfo*))Tuple_5_Equals_m3783740117_gshared)(__this, ___obj0, method)
// System.Int32 UniRx.Tuple`5<System.Object,System.Object,System.Object,System.Object,System.Object>::GetHashCode()
extern "C"  int32_t Tuple_5_GetHashCode_m2515905273_gshared (Tuple_5_t2059804720 * __this, const MethodInfo* method);
#define Tuple_5_GetHashCode_m2515905273(__this, method) ((  int32_t (*) (Tuple_5_t2059804720 *, const MethodInfo*))Tuple_5_GetHashCode_m2515905273_gshared)(__this, method)
// System.String UniRx.Tuple`5<System.Object,System.Object,System.Object,System.Object,System.Object>::ToString()
extern "C"  String_t* Tuple_5_ToString_m3139745363_gshared (Tuple_5_t2059804720 * __this, const MethodInfo* method);
#define Tuple_5_ToString_m3139745363(__this, method) ((  String_t* (*) (Tuple_5_t2059804720 *, const MethodInfo*))Tuple_5_ToString_m3139745363_gshared)(__this, method)
// System.Boolean UniRx.Tuple`5<System.Object,System.Object,System.Object,System.Object,System.Object>::Equals(UniRx.Tuple`5<T1,T2,T3,T4,T5>)
extern "C"  bool Tuple_5_Equals_m3075890475_gshared (Tuple_5_t2059804720 * __this, Tuple_5_t2059804720  ___other0, const MethodInfo* method);
#define Tuple_5_Equals_m3075890475(__this, ___other0, method) ((  bool (*) (Tuple_5_t2059804720 *, Tuple_5_t2059804720 , const MethodInfo*))Tuple_5_Equals_m3075890475_gshared)(__this, ___other0, method)
