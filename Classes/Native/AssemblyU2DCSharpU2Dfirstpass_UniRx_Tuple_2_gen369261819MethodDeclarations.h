﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Object
struct Il2CppObject;
// System.Collections.IComparer
struct IComparer_t2207526184;
// System.Collections.IEqualityComparer
struct IEqualityComparer_t1661793090;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Tuple_2_gen369261819.h"
#include "mscorlib_System_Object837106420.h"

// System.Void UniRx.Tuple`2<System.Object,System.Object>::.ctor(T1,T2)
extern "C"  void Tuple_2__ctor_m101027774_gshared (Tuple_2_t369261819 * __this, Il2CppObject * ___item10, Il2CppObject * ___item21, const MethodInfo* method);
#define Tuple_2__ctor_m101027774(__this, ___item10, ___item21, method) ((  void (*) (Tuple_2_t369261819 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))Tuple_2__ctor_m101027774_gshared)(__this, ___item10, ___item21, method)
// System.Int32 UniRx.Tuple`2<System.Object,System.Object>::System.IComparable.CompareTo(System.Object)
extern "C"  int32_t Tuple_2_System_IComparable_CompareTo_m964946507_gshared (Tuple_2_t369261819 * __this, Il2CppObject * ___obj0, const MethodInfo* method);
#define Tuple_2_System_IComparable_CompareTo_m964946507(__this, ___obj0, method) ((  int32_t (*) (Tuple_2_t369261819 *, Il2CppObject *, const MethodInfo*))Tuple_2_System_IComparable_CompareTo_m964946507_gshared)(__this, ___obj0, method)
// System.Int32 UniRx.Tuple`2<System.Object,System.Object>::UniRx.IStructuralComparable.CompareTo(System.Object,System.Collections.IComparer)
extern "C"  int32_t Tuple_2_UniRx_IStructuralComparable_CompareTo_m1607455453_gshared (Tuple_2_t369261819 * __this, Il2CppObject * ___other0, Il2CppObject * ___comparer1, const MethodInfo* method);
#define Tuple_2_UniRx_IStructuralComparable_CompareTo_m1607455453(__this, ___other0, ___comparer1, method) ((  int32_t (*) (Tuple_2_t369261819 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))Tuple_2_UniRx_IStructuralComparable_CompareTo_m1607455453_gshared)(__this, ___other0, ___comparer1, method)
// System.Boolean UniRx.Tuple`2<System.Object,System.Object>::UniRx.IStructuralEquatable.Equals(System.Object,System.Collections.IEqualityComparer)
extern "C"  bool Tuple_2_UniRx_IStructuralEquatable_Equals_m2678862280_gshared (Tuple_2_t369261819 * __this, Il2CppObject * ___other0, Il2CppObject * ___comparer1, const MethodInfo* method);
#define Tuple_2_UniRx_IStructuralEquatable_Equals_m2678862280(__this, ___other0, ___comparer1, method) ((  bool (*) (Tuple_2_t369261819 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))Tuple_2_UniRx_IStructuralEquatable_Equals_m2678862280_gshared)(__this, ___other0, ___comparer1, method)
// System.Int32 UniRx.Tuple`2<System.Object,System.Object>::UniRx.IStructuralEquatable.GetHashCode(System.Collections.IEqualityComparer)
extern "C"  int32_t Tuple_2_UniRx_IStructuralEquatable_GetHashCode_m2682338918_gshared (Tuple_2_t369261819 * __this, Il2CppObject * ___comparer0, const MethodInfo* method);
#define Tuple_2_UniRx_IStructuralEquatable_GetHashCode_m2682338918(__this, ___comparer0, method) ((  int32_t (*) (Tuple_2_t369261819 *, Il2CppObject *, const MethodInfo*))Tuple_2_UniRx_IStructuralEquatable_GetHashCode_m2682338918_gshared)(__this, ___comparer0, method)
// System.String UniRx.Tuple`2<System.Object,System.Object>::UniRx.ITuple.ToString()
extern "C"  String_t* Tuple_2_UniRx_ITuple_ToString_m2079956805_gshared (Tuple_2_t369261819 * __this, const MethodInfo* method);
#define Tuple_2_UniRx_ITuple_ToString_m2079956805(__this, method) ((  String_t* (*) (Tuple_2_t369261819 *, const MethodInfo*))Tuple_2_UniRx_ITuple_ToString_m2079956805_gshared)(__this, method)
// T1 UniRx.Tuple`2<System.Object,System.Object>::get_Item1()
extern "C"  Il2CppObject * Tuple_2_get_Item1_m425160818_gshared (Tuple_2_t369261819 * __this, const MethodInfo* method);
#define Tuple_2_get_Item1_m425160818(__this, method) ((  Il2CppObject * (*) (Tuple_2_t369261819 *, const MethodInfo*))Tuple_2_get_Item1_m425160818_gshared)(__this, method)
// T2 UniRx.Tuple`2<System.Object,System.Object>::get_Item2()
extern "C"  Il2CppObject * Tuple_2_get_Item2_m1055620404_gshared (Tuple_2_t369261819 * __this, const MethodInfo* method);
#define Tuple_2_get_Item2_m1055620404(__this, method) ((  Il2CppObject * (*) (Tuple_2_t369261819 *, const MethodInfo*))Tuple_2_get_Item2_m1055620404_gshared)(__this, method)
// System.Boolean UniRx.Tuple`2<System.Object,System.Object>::Equals(System.Object)
extern "C"  bool Tuple_2_Equals_m2489154684_gshared (Tuple_2_t369261819 * __this, Il2CppObject * ___obj0, const MethodInfo* method);
#define Tuple_2_Equals_m2489154684(__this, ___obj0, method) ((  bool (*) (Tuple_2_t369261819 *, Il2CppObject *, const MethodInfo*))Tuple_2_Equals_m2489154684_gshared)(__this, ___obj0, method)
// System.Int32 UniRx.Tuple`2<System.Object,System.Object>::GetHashCode()
extern "C"  int32_t Tuple_2_GetHashCode_m1023013664_gshared (Tuple_2_t369261819 * __this, const MethodInfo* method);
#define Tuple_2_GetHashCode_m1023013664(__this, method) ((  int32_t (*) (Tuple_2_t369261819 *, const MethodInfo*))Tuple_2_GetHashCode_m1023013664_gshared)(__this, method)
// System.String UniRx.Tuple`2<System.Object,System.Object>::ToString()
extern "C"  String_t* Tuple_2_ToString_m3182481356_gshared (Tuple_2_t369261819 * __this, const MethodInfo* method);
#define Tuple_2_ToString_m3182481356(__this, method) ((  String_t* (*) (Tuple_2_t369261819 *, const MethodInfo*))Tuple_2_ToString_m3182481356_gshared)(__this, method)
// System.Boolean UniRx.Tuple`2<System.Object,System.Object>::Equals(UniRx.Tuple`2<T1,T2>)
extern "C"  bool Tuple_2_Equals_m1605966829_gshared (Tuple_2_t369261819 * __this, Tuple_2_t369261819  ___other0, const MethodInfo* method);
#define Tuple_2_Equals_m1605966829(__this, ___other0, method) ((  bool (*) (Tuple_2_t369261819 *, Tuple_2_t369261819 , const MethodInfo*))Tuple_2_Equals_m1605966829_gshared)(__this, ___other0, method)
