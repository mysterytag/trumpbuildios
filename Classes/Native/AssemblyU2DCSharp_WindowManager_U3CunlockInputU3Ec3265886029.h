﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Object
struct Il2CppObject;
// WindowManager
struct WindowManager_t3316821373;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WindowManager/<unlockInput>c__Iterator2B
struct  U3CunlockInputU3Ec__Iterator2B_t3265886029  : public Il2CppObject
{
public:
	// System.Single WindowManager/<unlockInput>c__Iterator2B::inSeconds
	float ___inSeconds_0;
	// System.Int32 WindowManager/<unlockInput>c__Iterator2B::$PC
	int32_t ___U24PC_1;
	// System.Object WindowManager/<unlockInput>c__Iterator2B::$current
	Il2CppObject * ___U24current_2;
	// System.Single WindowManager/<unlockInput>c__Iterator2B::<$>inSeconds
	float ___U3CU24U3EinSeconds_3;
	// WindowManager WindowManager/<unlockInput>c__Iterator2B::<>f__this
	WindowManager_t3316821373 * ___U3CU3Ef__this_4;

public:
	inline static int32_t get_offset_of_inSeconds_0() { return static_cast<int32_t>(offsetof(U3CunlockInputU3Ec__Iterator2B_t3265886029, ___inSeconds_0)); }
	inline float get_inSeconds_0() const { return ___inSeconds_0; }
	inline float* get_address_of_inSeconds_0() { return &___inSeconds_0; }
	inline void set_inSeconds_0(float value)
	{
		___inSeconds_0 = value;
	}

	inline static int32_t get_offset_of_U24PC_1() { return static_cast<int32_t>(offsetof(U3CunlockInputU3Ec__Iterator2B_t3265886029, ___U24PC_1)); }
	inline int32_t get_U24PC_1() const { return ___U24PC_1; }
	inline int32_t* get_address_of_U24PC_1() { return &___U24PC_1; }
	inline void set_U24PC_1(int32_t value)
	{
		___U24PC_1 = value;
	}

	inline static int32_t get_offset_of_U24current_2() { return static_cast<int32_t>(offsetof(U3CunlockInputU3Ec__Iterator2B_t3265886029, ___U24current_2)); }
	inline Il2CppObject * get_U24current_2() const { return ___U24current_2; }
	inline Il2CppObject ** get_address_of_U24current_2() { return &___U24current_2; }
	inline void set_U24current_2(Il2CppObject * value)
	{
		___U24current_2 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_2, value);
	}

	inline static int32_t get_offset_of_U3CU24U3EinSeconds_3() { return static_cast<int32_t>(offsetof(U3CunlockInputU3Ec__Iterator2B_t3265886029, ___U3CU24U3EinSeconds_3)); }
	inline float get_U3CU24U3EinSeconds_3() const { return ___U3CU24U3EinSeconds_3; }
	inline float* get_address_of_U3CU24U3EinSeconds_3() { return &___U3CU24U3EinSeconds_3; }
	inline void set_U3CU24U3EinSeconds_3(float value)
	{
		___U3CU24U3EinSeconds_3 = value;
	}

	inline static int32_t get_offset_of_U3CU3Ef__this_4() { return static_cast<int32_t>(offsetof(U3CunlockInputU3Ec__Iterator2B_t3265886029, ___U3CU3Ef__this_4)); }
	inline WindowManager_t3316821373 * get_U3CU3Ef__this_4() const { return ___U3CU3Ef__this_4; }
	inline WindowManager_t3316821373 ** get_address_of_U3CU3Ef__this_4() { return &___U3CU3Ef__this_4; }
	inline void set_U3CU3Ef__this_4(WindowManager_t3316821373 * value)
	{
		___U3CU3Ef__this_4 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__this_4, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
