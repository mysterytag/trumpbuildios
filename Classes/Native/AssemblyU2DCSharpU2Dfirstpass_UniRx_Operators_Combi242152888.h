﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UniRx.Operators.CombineLatestObservable`1<System.Boolean>
struct CombineLatestObservable_1_t2883136337;
// System.Object
struct Il2CppObject;
// System.Boolean[]
struct BooleanU5BU5D_t3804927312;

#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Oper2939511529.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Operators.CombineLatestObservable`1/CombineLatest<System.Boolean>
struct  CombineLatest_t242152888  : public OperatorObserverBase_2_t2939511529
{
public:
	// UniRx.Operators.CombineLatestObservable`1<T> UniRx.Operators.CombineLatestObservable`1/CombineLatest::parent
	CombineLatestObservable_1_t2883136337 * ___parent_2;
	// System.Object UniRx.Operators.CombineLatestObservable`1/CombineLatest::gate
	Il2CppObject * ___gate_3;
	// System.Int32 UniRx.Operators.CombineLatestObservable`1/CombineLatest::length
	int32_t ___length_4;
	// T[] UniRx.Operators.CombineLatestObservable`1/CombineLatest::values
	BooleanU5BU5D_t3804927312* ___values_5;
	// System.Boolean[] UniRx.Operators.CombineLatestObservable`1/CombineLatest::isStarted
	BooleanU5BU5D_t3804927312* ___isStarted_6;
	// System.Boolean[] UniRx.Operators.CombineLatestObservable`1/CombineLatest::isCompleted
	BooleanU5BU5D_t3804927312* ___isCompleted_7;
	// System.Boolean UniRx.Operators.CombineLatestObservable`1/CombineLatest::isAllValueStarted
	bool ___isAllValueStarted_8;

public:
	inline static int32_t get_offset_of_parent_2() { return static_cast<int32_t>(offsetof(CombineLatest_t242152888, ___parent_2)); }
	inline CombineLatestObservable_1_t2883136337 * get_parent_2() const { return ___parent_2; }
	inline CombineLatestObservable_1_t2883136337 ** get_address_of_parent_2() { return &___parent_2; }
	inline void set_parent_2(CombineLatestObservable_1_t2883136337 * value)
	{
		___parent_2 = value;
		Il2CppCodeGenWriteBarrier(&___parent_2, value);
	}

	inline static int32_t get_offset_of_gate_3() { return static_cast<int32_t>(offsetof(CombineLatest_t242152888, ___gate_3)); }
	inline Il2CppObject * get_gate_3() const { return ___gate_3; }
	inline Il2CppObject ** get_address_of_gate_3() { return &___gate_3; }
	inline void set_gate_3(Il2CppObject * value)
	{
		___gate_3 = value;
		Il2CppCodeGenWriteBarrier(&___gate_3, value);
	}

	inline static int32_t get_offset_of_length_4() { return static_cast<int32_t>(offsetof(CombineLatest_t242152888, ___length_4)); }
	inline int32_t get_length_4() const { return ___length_4; }
	inline int32_t* get_address_of_length_4() { return &___length_4; }
	inline void set_length_4(int32_t value)
	{
		___length_4 = value;
	}

	inline static int32_t get_offset_of_values_5() { return static_cast<int32_t>(offsetof(CombineLatest_t242152888, ___values_5)); }
	inline BooleanU5BU5D_t3804927312* get_values_5() const { return ___values_5; }
	inline BooleanU5BU5D_t3804927312** get_address_of_values_5() { return &___values_5; }
	inline void set_values_5(BooleanU5BU5D_t3804927312* value)
	{
		___values_5 = value;
		Il2CppCodeGenWriteBarrier(&___values_5, value);
	}

	inline static int32_t get_offset_of_isStarted_6() { return static_cast<int32_t>(offsetof(CombineLatest_t242152888, ___isStarted_6)); }
	inline BooleanU5BU5D_t3804927312* get_isStarted_6() const { return ___isStarted_6; }
	inline BooleanU5BU5D_t3804927312** get_address_of_isStarted_6() { return &___isStarted_6; }
	inline void set_isStarted_6(BooleanU5BU5D_t3804927312* value)
	{
		___isStarted_6 = value;
		Il2CppCodeGenWriteBarrier(&___isStarted_6, value);
	}

	inline static int32_t get_offset_of_isCompleted_7() { return static_cast<int32_t>(offsetof(CombineLatest_t242152888, ___isCompleted_7)); }
	inline BooleanU5BU5D_t3804927312* get_isCompleted_7() const { return ___isCompleted_7; }
	inline BooleanU5BU5D_t3804927312** get_address_of_isCompleted_7() { return &___isCompleted_7; }
	inline void set_isCompleted_7(BooleanU5BU5D_t3804927312* value)
	{
		___isCompleted_7 = value;
		Il2CppCodeGenWriteBarrier(&___isCompleted_7, value);
	}

	inline static int32_t get_offset_of_isAllValueStarted_8() { return static_cast<int32_t>(offsetof(CombineLatest_t242152888, ___isAllValueStarted_8)); }
	inline bool get_isAllValueStarted_8() const { return ___isAllValueStarted_8; }
	inline bool* get_address_of_isAllValueStarted_8() { return &___isAllValueStarted_8; }
	inline void set_isAllValueStarted_8(bool value)
	{
		___isAllValueStarted_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
