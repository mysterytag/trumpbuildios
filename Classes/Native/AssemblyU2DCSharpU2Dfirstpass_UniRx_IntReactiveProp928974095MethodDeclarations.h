﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.IntReactiveProperty
struct IntReactiveProperty_t928974095;

#include "codegen/il2cpp-codegen.h"

// System.Void UniRx.IntReactiveProperty::.ctor()
extern "C"  void IntReactiveProperty__ctor_m3453597618 (IntReactiveProperty_t928974095 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.IntReactiveProperty::.ctor(System.Int32)
extern "C"  void IntReactiveProperty__ctor_m1739026947 (IntReactiveProperty_t928974095 * __this, int32_t ___initialValue0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
