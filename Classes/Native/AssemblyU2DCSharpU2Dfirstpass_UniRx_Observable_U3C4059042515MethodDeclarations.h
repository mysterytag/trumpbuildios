﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Observable/<NextFrameCore>c__Iterator14
struct U3CNextFrameCoreU3Ec__Iterator14_t4059042515;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void UniRx.Observable/<NextFrameCore>c__Iterator14::.ctor()
extern "C"  void U3CNextFrameCoreU3Ec__Iterator14__ctor_m2408404432 (U3CNextFrameCoreU3Ec__Iterator14_t4059042515 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object UniRx.Observable/<NextFrameCore>c__Iterator14::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CNextFrameCoreU3Ec__Iterator14_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m2240490306 (U3CNextFrameCoreU3Ec__Iterator14_t4059042515 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object UniRx.Observable/<NextFrameCore>c__Iterator14::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CNextFrameCoreU3Ec__Iterator14_System_Collections_IEnumerator_get_Current_m447738582 (U3CNextFrameCoreU3Ec__Iterator14_t4059042515 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UniRx.Observable/<NextFrameCore>c__Iterator14::MoveNext()
extern "C"  bool U3CNextFrameCoreU3Ec__Iterator14_MoveNext_m3131492156 (U3CNextFrameCoreU3Ec__Iterator14_t4059042515 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.Observable/<NextFrameCore>c__Iterator14::Dispose()
extern "C"  void U3CNextFrameCoreU3Ec__Iterator14_Dispose_m3682536973 (U3CNextFrameCoreU3Ec__Iterator14_t4059042515 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.Observable/<NextFrameCore>c__Iterator14::Reset()
extern "C"  void U3CNextFrameCoreU3Ec__Iterator14_Reset_m54837373 (U3CNextFrameCoreU3Ec__Iterator14_t4059042515 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
