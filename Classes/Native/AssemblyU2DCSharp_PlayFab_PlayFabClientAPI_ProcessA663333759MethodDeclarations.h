﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_PlayFab_PlayFabClientAPI_Process3425006930MethodDeclarations.h"

// System.Void PlayFab.PlayFabClientAPI/ProcessApiCallback`1<PlayFab.ClientModels.PurchaseItemResult>::.ctor(System.Object,System.IntPtr)
#define ProcessApiCallback_1__ctor_m1953632761(__this, ___object0, ___method1, method) ((  void (*) (ProcessApiCallback_1_t663333759 *, Il2CppObject *, IntPtr_t, const MethodInfo*))ProcessApiCallback_1__ctor_m1266910666_gshared)(__this, ___object0, ___method1, method)
// System.Void PlayFab.PlayFabClientAPI/ProcessApiCallback`1<PlayFab.ClientModels.PurchaseItemResult>::Invoke(TResult)
#define ProcessApiCallback_1_Invoke_m3627351566(__this, ___result0, method) ((  void (*) (ProcessApiCallback_1_t663333759 *, PurchaseItemResult_t2370400545 *, const MethodInfo*))ProcessApiCallback_1_Invoke_m1002635741_gshared)(__this, ___result0, method)
// System.IAsyncResult PlayFab.PlayFabClientAPI/ProcessApiCallback`1<PlayFab.ClientModels.PurchaseItemResult>::BeginInvoke(TResult,System.AsyncCallback,System.Object)
#define ProcessApiCallback_1_BeginInvoke_m2187350901(__this, ___result0, ___callback1, ___object2, method) ((  Il2CppObject * (*) (ProcessApiCallback_1_t663333759 *, PurchaseItemResult_t2370400545 *, AsyncCallback_t1363551830 *, Il2CppObject *, const MethodInfo*))ProcessApiCallback_1_BeginInvoke_m1317674820_gshared)(__this, ___result0, ___callback1, ___object2, method)
// System.Void PlayFab.PlayFabClientAPI/ProcessApiCallback`1<PlayFab.ClientModels.PurchaseItemResult>::EndInvoke(System.IAsyncResult)
#define ProcessApiCallback_1_EndInvoke_m1560766601(__this, ___result0, method) ((  void (*) (ProcessApiCallback_1_t663333759 *, Il2CppObject *, const MethodInfo*))ProcessApiCallback_1_EndInvoke_m3326438618_gshared)(__this, ___result0, method)
