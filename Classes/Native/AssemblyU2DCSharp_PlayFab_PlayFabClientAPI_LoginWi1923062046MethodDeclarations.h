﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.PlayFabClientAPI/LoginWithGameCenterRequestCallback
struct LoginWithGameCenterRequestCallback_t1923062046;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;
// PlayFab.ClientModels.LoginWithGameCenterRequest
struct LoginWithGameCenterRequest_t1605888457;
// System.IAsyncResult
struct IAsyncResult_t537683269;
// System.AsyncCallback
struct AsyncCallback_t1363551830;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_IntPtr676692020.h"
#include "mscorlib_System_String968488902.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_LoginWithGa1605888457.h"
#include "mscorlib_System_AsyncCallback1363551830.h"

// System.Void PlayFab.PlayFabClientAPI/LoginWithGameCenterRequestCallback::.ctor(System.Object,System.IntPtr)
extern "C"  void LoginWithGameCenterRequestCallback__ctor_m4261304077 (LoginWithGameCenterRequestCallback_t1923062046 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/LoginWithGameCenterRequestCallback::Invoke(System.String,System.Int32,PlayFab.ClientModels.LoginWithGameCenterRequest,System.Object)
extern "C"  void LoginWithGameCenterRequestCallback_Invoke_m4138304735 (LoginWithGameCenterRequestCallback_t1923062046 * __this, String_t* ___urlPath0, int32_t ___callId1, LoginWithGameCenterRequest_t1605888457 * ___request2, Il2CppObject * ___customData3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
extern "C" void pinvoke_delegate_wrapper_LoginWithGameCenterRequestCallback_t1923062046(Il2CppObject* delegate, String_t* ___urlPath0, int32_t ___callId1, LoginWithGameCenterRequest_t1605888457 * ___request2, Il2CppObject * ___customData3);
// System.IAsyncResult PlayFab.PlayFabClientAPI/LoginWithGameCenterRequestCallback::BeginInvoke(System.String,System.Int32,PlayFab.ClientModels.LoginWithGameCenterRequest,System.Object,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * LoginWithGameCenterRequestCallback_BeginInvoke_m2452117538 (LoginWithGameCenterRequestCallback_t1923062046 * __this, String_t* ___urlPath0, int32_t ___callId1, LoginWithGameCenterRequest_t1605888457 * ___request2, Il2CppObject * ___customData3, AsyncCallback_t1363551830 * ___callback4, Il2CppObject * ___object5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/LoginWithGameCenterRequestCallback::EndInvoke(System.IAsyncResult)
extern "C"  void LoginWithGameCenterRequestCallback_EndInvoke_m638146461 (LoginWithGameCenterRequestCallback_t1923062046 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
