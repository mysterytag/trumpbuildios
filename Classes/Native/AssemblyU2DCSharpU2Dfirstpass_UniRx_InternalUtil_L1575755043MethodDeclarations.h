﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.InternalUtil.ListObserver`1<UnityEngine.NetworkPlayer>
struct ListObserver_1_t1575755043;
// UniRx.InternalUtil.ImmutableList`1<UniRx.IObserver`1<UnityEngine.NetworkPlayer>>
struct ImmutableList_1_t258373478;
// System.Exception
struct Exception_t1967233988;
// UniRx.IObserver`1<UnityEngine.NetworkPlayer>
struct IObserver_1_t3493136275;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Exception1967233988.h"
#include "UnityEngine_UnityEngine_NetworkPlayer1281137372.h"

// System.Void UniRx.InternalUtil.ListObserver`1<UnityEngine.NetworkPlayer>::.ctor(UniRx.InternalUtil.ImmutableList`1<UniRx.IObserver`1<T>>)
extern "C"  void ListObserver_1__ctor_m2324206299_gshared (ListObserver_1_t1575755043 * __this, ImmutableList_1_t258373478 * ___observers0, const MethodInfo* method);
#define ListObserver_1__ctor_m2324206299(__this, ___observers0, method) ((  void (*) (ListObserver_1_t1575755043 *, ImmutableList_1_t258373478 *, const MethodInfo*))ListObserver_1__ctor_m2324206299_gshared)(__this, ___observers0, method)
// System.Void UniRx.InternalUtil.ListObserver`1<UnityEngine.NetworkPlayer>::OnCompleted()
extern "C"  void ListObserver_1_OnCompleted_m993589715_gshared (ListObserver_1_t1575755043 * __this, const MethodInfo* method);
#define ListObserver_1_OnCompleted_m993589715(__this, method) ((  void (*) (ListObserver_1_t1575755043 *, const MethodInfo*))ListObserver_1_OnCompleted_m993589715_gshared)(__this, method)
// System.Void UniRx.InternalUtil.ListObserver`1<UnityEngine.NetworkPlayer>::OnError(System.Exception)
extern "C"  void ListObserver_1_OnError_m1261733504_gshared (ListObserver_1_t1575755043 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method);
#define ListObserver_1_OnError_m1261733504(__this, ___error0, method) ((  void (*) (ListObserver_1_t1575755043 *, Exception_t1967233988 *, const MethodInfo*))ListObserver_1_OnError_m1261733504_gshared)(__this, ___error0, method)
// System.Void UniRx.InternalUtil.ListObserver`1<UnityEngine.NetworkPlayer>::OnNext(T)
extern "C"  void ListObserver_1_OnNext_m795831665_gshared (ListObserver_1_t1575755043 * __this, NetworkPlayer_t1281137372  ___value0, const MethodInfo* method);
#define ListObserver_1_OnNext_m795831665(__this, ___value0, method) ((  void (*) (ListObserver_1_t1575755043 *, NetworkPlayer_t1281137372 , const MethodInfo*))ListObserver_1_OnNext_m795831665_gshared)(__this, ___value0, method)
// UniRx.IObserver`1<T> UniRx.InternalUtil.ListObserver`1<UnityEngine.NetworkPlayer>::Add(UniRx.IObserver`1<T>)
extern "C"  Il2CppObject* ListObserver_1_Add_m942335875_gshared (ListObserver_1_t1575755043 * __this, Il2CppObject* ___observer0, const MethodInfo* method);
#define ListObserver_1_Add_m942335875(__this, ___observer0, method) ((  Il2CppObject* (*) (ListObserver_1_t1575755043 *, Il2CppObject*, const MethodInfo*))ListObserver_1_Add_m942335875_gshared)(__this, ___observer0, method)
// UniRx.IObserver`1<T> UniRx.InternalUtil.ListObserver`1<UnityEngine.NetworkPlayer>::Remove(UniRx.IObserver`1<T>)
extern "C"  Il2CppObject* ListObserver_1_Remove_m1655251176_gshared (ListObserver_1_t1575755043 * __this, Il2CppObject* ___observer0, const MethodInfo* method);
#define ListObserver_1_Remove_m1655251176(__this, ___observer0, method) ((  Il2CppObject* (*) (ListObserver_1_t1575755043 *, Il2CppObject*, const MethodInfo*))ListObserver_1_Remove_m1655251176_gshared)(__this, ___observer0, method)
