﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// TableViewCellBase
struct TableViewCellBase_t2057844710;
// TutorialPlayer/<WaitForButton>c__Iterator22
struct U3CWaitForButtonU3Ec__Iterator22_t467456669;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TutorialPlayer/<WaitForButton>c__Iterator22/<WaitForButton>c__AnonStoreyED
struct  U3CWaitForButtonU3Ec__AnonStoreyED_t1755169010  : public Il2CppObject
{
public:
	// TableViewCellBase TutorialPlayer/<WaitForButton>c__Iterator22/<WaitForButton>c__AnonStoreyED::cell
	TableViewCellBase_t2057844710 * ___cell_0;
	// TutorialPlayer/<WaitForButton>c__Iterator22 TutorialPlayer/<WaitForButton>c__Iterator22/<WaitForButton>c__AnonStoreyED::<>f__ref$34
	U3CWaitForButtonU3Ec__Iterator22_t467456669 * ___U3CU3Ef__refU2434_1;

public:
	inline static int32_t get_offset_of_cell_0() { return static_cast<int32_t>(offsetof(U3CWaitForButtonU3Ec__AnonStoreyED_t1755169010, ___cell_0)); }
	inline TableViewCellBase_t2057844710 * get_cell_0() const { return ___cell_0; }
	inline TableViewCellBase_t2057844710 ** get_address_of_cell_0() { return &___cell_0; }
	inline void set_cell_0(TableViewCellBase_t2057844710 * value)
	{
		___cell_0 = value;
		Il2CppCodeGenWriteBarrier(&___cell_0, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__refU2434_1() { return static_cast<int32_t>(offsetof(U3CWaitForButtonU3Ec__AnonStoreyED_t1755169010, ___U3CU3Ef__refU2434_1)); }
	inline U3CWaitForButtonU3Ec__Iterator22_t467456669 * get_U3CU3Ef__refU2434_1() const { return ___U3CU3Ef__refU2434_1; }
	inline U3CWaitForButtonU3Ec__Iterator22_t467456669 ** get_address_of_U3CU3Ef__refU2434_1() { return &___U3CU3Ef__refU2434_1; }
	inline void set_U3CU3Ef__refU2434_1(U3CWaitForButtonU3Ec__Iterator22_t467456669 * value)
	{
		___U3CU3Ef__refU2434_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__refU2434_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
