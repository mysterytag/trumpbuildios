﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Triggers.ObservableStateMachineTrigger
struct ObservableStateMachineTrigger_t739780288;
// UnityEngine.Animator
struct Animator_t792326996;
// UniRx.IObservable`1<UniRx.Triggers.ObservableStateMachineTrigger/OnStateInfo>
struct IObservable_1_t3604635580;
// UniRx.IObservable`1<UniRx.Triggers.ObservableStateMachineTrigger/OnStateMachineInfo>
struct IObservable_1_t624668287;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Animator792326996.h"
#include "UnityEngine_UnityEngine_AnimatorStateInfo4162640357.h"

// System.Void UniRx.Triggers.ObservableStateMachineTrigger::.ctor()
extern "C"  void ObservableStateMachineTrigger__ctor_m521381611 (ObservableStateMachineTrigger_t739780288 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.Triggers.ObservableStateMachineTrigger::OnStateExit(UnityEngine.Animator,UnityEngine.AnimatorStateInfo,System.Int32)
extern "C"  void ObservableStateMachineTrigger_OnStateExit_m3342804305 (ObservableStateMachineTrigger_t739780288 * __this, Animator_t792326996 * ___animator0, AnimatorStateInfo_t4162640357  ___stateInfo1, int32_t ___layerIndex2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UniRx.IObservable`1<UniRx.Triggers.ObservableStateMachineTrigger/OnStateInfo> UniRx.Triggers.ObservableStateMachineTrigger::OnStateExitAsObservable()
extern "C"  Il2CppObject* ObservableStateMachineTrigger_OnStateExitAsObservable_m3793652333 (ObservableStateMachineTrigger_t739780288 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.Triggers.ObservableStateMachineTrigger::OnStateEnter(UnityEngine.Animator,UnityEngine.AnimatorStateInfo,System.Int32)
extern "C"  void ObservableStateMachineTrigger_OnStateEnter_m565062987 (ObservableStateMachineTrigger_t739780288 * __this, Animator_t792326996 * ___animator0, AnimatorStateInfo_t4162640357  ___stateInfo1, int32_t ___layerIndex2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UniRx.IObservable`1<UniRx.Triggers.ObservableStateMachineTrigger/OnStateInfo> UniRx.Triggers.ObservableStateMachineTrigger::OnStateEnterAsObservable()
extern "C"  Il2CppObject* ObservableStateMachineTrigger_OnStateEnterAsObservable_m2821142549 (ObservableStateMachineTrigger_t739780288 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.Triggers.ObservableStateMachineTrigger::OnStateIK(UnityEngine.Animator,UnityEngine.AnimatorStateInfo,System.Int32)
extern "C"  void ObservableStateMachineTrigger_OnStateIK_m3200974701 (ObservableStateMachineTrigger_t739780288 * __this, Animator_t792326996 * ___animator0, AnimatorStateInfo_t4162640357  ___stateInfo1, int32_t ___layerIndex2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UniRx.IObservable`1<UniRx.Triggers.ObservableStateMachineTrigger/OnStateInfo> UniRx.Triggers.ObservableStateMachineTrigger::OnStateIKAsObservable()
extern "C"  Il2CppObject* ObservableStateMachineTrigger_OnStateIKAsObservable_m3273531793 (ObservableStateMachineTrigger_t739780288 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.Triggers.ObservableStateMachineTrigger::OnStateUpdate(UnityEngine.Animator,UnityEngine.AnimatorStateInfo,System.Int32)
extern "C"  void ObservableStateMachineTrigger_OnStateUpdate_m447758726 (ObservableStateMachineTrigger_t739780288 * __this, Animator_t792326996 * ___animator0, AnimatorStateInfo_t4162640357  ___stateInfo1, int32_t ___layerIndex2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UniRx.IObservable`1<UniRx.Triggers.ObservableStateMachineTrigger/OnStateInfo> UniRx.Triggers.ObservableStateMachineTrigger::OnStateUpdateAsObservable()
extern "C"  Il2CppObject* ObservableStateMachineTrigger_OnStateUpdateAsObservable_m3453120664 (ObservableStateMachineTrigger_t739780288 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.Triggers.ObservableStateMachineTrigger::OnStateMachineEnter(UnityEngine.Animator,System.Int32)
extern "C"  void ObservableStateMachineTrigger_OnStateMachineEnter_m2572921161 (ObservableStateMachineTrigger_t739780288 * __this, Animator_t792326996 * ___animator0, int32_t ___stateMachinePathHash1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UniRx.IObservable`1<UniRx.Triggers.ObservableStateMachineTrigger/OnStateMachineInfo> UniRx.Triggers.ObservableStateMachineTrigger::OnStateMachineEnterAsObservable()
extern "C"  Il2CppObject* ObservableStateMachineTrigger_OnStateMachineEnterAsObservable_m2091101123 (ObservableStateMachineTrigger_t739780288 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.Triggers.ObservableStateMachineTrigger::OnStateMachineExit(UnityEngine.Animator,System.Int32)
extern "C"  void ObservableStateMachineTrigger_OnStateMachineExit_m419350921 (ObservableStateMachineTrigger_t739780288 * __this, Animator_t792326996 * ___animator0, int32_t ___stateMachinePathHash1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UniRx.IObservable`1<UniRx.Triggers.ObservableStateMachineTrigger/OnStateMachineInfo> UniRx.Triggers.ObservableStateMachineTrigger::OnStateMachineExitAsObservable()
extern "C"  Il2CppObject* ObservableStateMachineTrigger_OnStateMachineExitAsObservable_m999155967 (ObservableStateMachineTrigger_t739780288 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
