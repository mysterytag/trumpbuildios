﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Observable/<SelectMany>c__AnonStorey4F`1<System.Object>
struct U3CSelectManyU3Ec__AnonStorey4F_1_t3757673476;
// UniRx.IObservable`1<UniRx.Unit>
struct IObservable_1_t2317084402;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"

// System.Void UniRx.Observable/<SelectMany>c__AnonStorey4F`1<System.Object>::.ctor()
extern "C"  void U3CSelectManyU3Ec__AnonStorey4F_1__ctor_m786173162_gshared (U3CSelectManyU3Ec__AnonStorey4F_1_t3757673476 * __this, const MethodInfo* method);
#define U3CSelectManyU3Ec__AnonStorey4F_1__ctor_m786173162(__this, method) ((  void (*) (U3CSelectManyU3Ec__AnonStorey4F_1_t3757673476 *, const MethodInfo*))U3CSelectManyU3Ec__AnonStorey4F_1__ctor_m786173162_gshared)(__this, method)
// UniRx.IObservable`1<UniRx.Unit> UniRx.Observable/<SelectMany>c__AnonStorey4F`1<System.Object>::<>m__4D(T)
extern "C"  Il2CppObject* U3CSelectManyU3Ec__AnonStorey4F_1_U3CU3Em__4D_m2236246835_gshared (U3CSelectManyU3Ec__AnonStorey4F_1_t3757673476 * __this, Il2CppObject * ___x0, const MethodInfo* method);
#define U3CSelectManyU3Ec__AnonStorey4F_1_U3CU3Em__4D_m2236246835(__this, ___x0, method) ((  Il2CppObject* (*) (U3CSelectManyU3Ec__AnonStorey4F_1_t3757673476 *, Il2CppObject *, const MethodInfo*))U3CSelectManyU3Ec__AnonStorey4F_1_U3CU3Em__4D_m2236246835_gshared)(__this, ___x0, method)
