﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.TypedMonoBehaviour
struct TypedMonoBehaviour_t4014417954;
// System.Single[]
struct SingleU5BU5D_t1219431280;
// UnityEngine.Collision
struct Collision_t1119538015;
// UnityEngine.Collision2D
struct Collision2D_t452810033;
// UnityEngine.ControllerColliderHit
struct ControllerColliderHit_t2693066224;
// UnityEngine.GameObject
struct GameObject_t4012695102;
// UnityEngine.RenderTexture
struct RenderTexture_t12905170;
// UnityEngine.Collider
struct Collider_t955670625;
// UnityEngine.Collider2D
struct Collider2D_t1890038195;
// UnityEngine.BitStream
struct BitStream_t3159523098;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Collision1119538015.h"
#include "UnityEngine_UnityEngine_Collision2D452810033.h"
#include "UnityEngine_UnityEngine_ControllerColliderHit2693066224.h"
#include "UnityEngine_UnityEngine_GameObject4012695102.h"
#include "UnityEngine_UnityEngine_RenderTexture12905170.h"
#include "UnityEngine_UnityEngine_Collider955670625.h"
#include "UnityEngine_UnityEngine_Collider2D1890038195.h"
#include "UnityEngine_UnityEngine_NetworkDisconnection338760395.h"
#include "UnityEngine_UnityEngine_NetworkConnectionError1023805993.h"
#include "UnityEngine_UnityEngine_MasterServerEvent2277807490.h"
#include "UnityEngine_UnityEngine_NetworkMessageInfo2574344884.h"
#include "UnityEngine_UnityEngine_NetworkPlayer1281137372.h"
#include "UnityEngine_UnityEngine_BitStream3159523098.h"

// System.Void UniRx.TypedMonoBehaviour::.ctor()
extern "C"  void TypedMonoBehaviour__ctor_m895605671 (TypedMonoBehaviour_t4014417954 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.TypedMonoBehaviour::Awake()
extern "C"  void TypedMonoBehaviour_Awake_m1133210890 (TypedMonoBehaviour_t4014417954 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.TypedMonoBehaviour::FixedUpdate()
extern "C"  void TypedMonoBehaviour_FixedUpdate_m898375650 (TypedMonoBehaviour_t4014417954 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.TypedMonoBehaviour::LateUpdate()
extern "C"  void TypedMonoBehaviour_LateUpdate_m1247649548 (TypedMonoBehaviour_t4014417954 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.TypedMonoBehaviour::OnAnimatorIK(System.Int32)
extern "C"  void TypedMonoBehaviour_OnAnimatorIK_m3332664854 (TypedMonoBehaviour_t4014417954 * __this, int32_t ___layerIndex0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.TypedMonoBehaviour::OnAnimatorMove()
extern "C"  void TypedMonoBehaviour_OnAnimatorMove_m407897812 (TypedMonoBehaviour_t4014417954 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.TypedMonoBehaviour::OnApplicationFocus(System.Boolean)
extern "C"  void TypedMonoBehaviour_OnApplicationFocus_m64431579 (TypedMonoBehaviour_t4014417954 * __this, bool ___focus0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.TypedMonoBehaviour::OnApplicationPause(System.Boolean)
extern "C"  void TypedMonoBehaviour_OnApplicationPause_m1518598905 (TypedMonoBehaviour_t4014417954 * __this, bool ___pause0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.TypedMonoBehaviour::OnApplicationQuit()
extern "C"  void TypedMonoBehaviour_OnApplicationQuit_m3337815717 (TypedMonoBehaviour_t4014417954 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.TypedMonoBehaviour::OnAudioFilterRead(System.Single[],System.Int32)
extern "C"  void TypedMonoBehaviour_OnAudioFilterRead_m1083499960 (TypedMonoBehaviour_t4014417954 * __this, SingleU5BU5D_t1219431280* ___data0, int32_t ___channels1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.TypedMonoBehaviour::OnBecameInvisible()
extern "C"  void TypedMonoBehaviour_OnBecameInvisible_m2223881242 (TypedMonoBehaviour_t4014417954 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.TypedMonoBehaviour::OnBecameVisible()
extern "C"  void TypedMonoBehaviour_OnBecameVisible_m3204811423 (TypedMonoBehaviour_t4014417954 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.TypedMonoBehaviour::OnCollisionEnter(UnityEngine.Collision)
extern "C"  void TypedMonoBehaviour_OnCollisionEnter_m3671736693 (TypedMonoBehaviour_t4014417954 * __this, Collision_t1119538015 * ___collision0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.TypedMonoBehaviour::OnCollisionEnter2D(UnityEngine.Collision2D)
extern "C"  void TypedMonoBehaviour_OnCollisionEnter2D_m3211378833 (TypedMonoBehaviour_t4014417954 * __this, Collision2D_t452810033 * ___coll0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.TypedMonoBehaviour::OnCollisionExit(UnityEngine.Collision)
extern "C"  void TypedMonoBehaviour_OnCollisionExit_m224813729 (TypedMonoBehaviour_t4014417954 * __this, Collision_t1119538015 * ___collisionInfo0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.TypedMonoBehaviour::OnCollisionExit2D(UnityEngine.Collision2D)
extern "C"  void TypedMonoBehaviour_OnCollisionExit2D_m2674486205 (TypedMonoBehaviour_t4014417954 * __this, Collision2D_t452810033 * ___coll0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.TypedMonoBehaviour::OnCollisionStay(UnityEngine.Collision)
extern "C"  void TypedMonoBehaviour_OnCollisionStay_m4076167942 (TypedMonoBehaviour_t4014417954 * __this, Collision_t1119538015 * ___collisionInfo0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.TypedMonoBehaviour::OnCollisionStay2D(UnityEngine.Collision2D)
extern "C"  void TypedMonoBehaviour_OnCollisionStay2D_m721924514 (TypedMonoBehaviour_t4014417954 * __this, Collision2D_t452810033 * ___coll0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.TypedMonoBehaviour::OnConnectedToServer()
extern "C"  void TypedMonoBehaviour_OnConnectedToServer_m549665645 (TypedMonoBehaviour_t4014417954 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.TypedMonoBehaviour::OnControllerColliderHit(UnityEngine.ControllerColliderHit)
extern "C"  void TypedMonoBehaviour_OnControllerColliderHit_m2900222205 (TypedMonoBehaviour_t4014417954 * __this, ControllerColliderHit_t2693066224 * ___hit0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.TypedMonoBehaviour::OnDestroy()
extern "C"  void TypedMonoBehaviour_OnDestroy_m1140819232 (TypedMonoBehaviour_t4014417954 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.TypedMonoBehaviour::OnDisable()
extern "C"  void TypedMonoBehaviour_OnDisable_m3258254478 (TypedMonoBehaviour_t4014417954 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.TypedMonoBehaviour::OnDrawGizmos()
extern "C"  void TypedMonoBehaviour_OnDrawGizmos_m2026904473 (TypedMonoBehaviour_t4014417954 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.TypedMonoBehaviour::OnDrawGizmosSelected()
extern "C"  void TypedMonoBehaviour_OnDrawGizmosSelected_m2803996564 (TypedMonoBehaviour_t4014417954 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.TypedMonoBehaviour::OnEnable()
extern "C"  void TypedMonoBehaviour_OnEnable_m2723280511 (TypedMonoBehaviour_t4014417954 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.TypedMonoBehaviour::OnJointBreak(System.Single)
extern "C"  void TypedMonoBehaviour_OnJointBreak_m534441914 (TypedMonoBehaviour_t4014417954 * __this, float ___breakForce0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.TypedMonoBehaviour::OnLevelWasLoaded(System.Int32)
extern "C"  void TypedMonoBehaviour_OnLevelWasLoaded_m4112970167 (TypedMonoBehaviour_t4014417954 * __this, int32_t ___level0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.TypedMonoBehaviour::OnParticleCollision(UnityEngine.GameObject)
extern "C"  void TypedMonoBehaviour_OnParticleCollision_m291388330 (TypedMonoBehaviour_t4014417954 * __this, GameObject_t4012695102 * ___other0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.TypedMonoBehaviour::OnPostRender()
extern "C"  void TypedMonoBehaviour_OnPostRender_m350806290 (TypedMonoBehaviour_t4014417954 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.TypedMonoBehaviour::OnPreCull()
extern "C"  void TypedMonoBehaviour_OnPreCull_m1167590683 (TypedMonoBehaviour_t4014417954 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.TypedMonoBehaviour::OnPreRender()
extern "C"  void TypedMonoBehaviour_OnPreRender_m177135167 (TypedMonoBehaviour_t4014417954 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.TypedMonoBehaviour::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C"  void TypedMonoBehaviour_OnRenderImage_m1348987991 (TypedMonoBehaviour_t4014417954 * __this, RenderTexture_t12905170 * ___src0, RenderTexture_t12905170 * ___dest1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.TypedMonoBehaviour::OnRenderObject()
extern "C"  void TypedMonoBehaviour_OnRenderObject_m785902769 (TypedMonoBehaviour_t4014417954 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.TypedMonoBehaviour::OnServerInitialized()
extern "C"  void TypedMonoBehaviour_OnServerInitialized_m397154487 (TypedMonoBehaviour_t4014417954 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.TypedMonoBehaviour::OnTriggerEnter(UnityEngine.Collider)
extern "C"  void TypedMonoBehaviour_OnTriggerEnter_m1062200625 (TypedMonoBehaviour_t4014417954 * __this, Collider_t955670625 * ___other0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.TypedMonoBehaviour::OnTriggerEnter2D(UnityEngine.Collider2D)
extern "C"  void TypedMonoBehaviour_OnTriggerEnter2D_m1162056881 (TypedMonoBehaviour_t4014417954 * __this, Collider2D_t1890038195 * ___other0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.TypedMonoBehaviour::OnTriggerExit(UnityEngine.Collider)
extern "C"  void TypedMonoBehaviour_OnTriggerExit_m4256249425 (TypedMonoBehaviour_t4014417954 * __this, Collider_t955670625 * ___other0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.TypedMonoBehaviour::OnTriggerExit2D(UnityEngine.Collider2D)
extern "C"  void TypedMonoBehaviour_OnTriggerExit2D_m1649890769 (TypedMonoBehaviour_t4014417954 * __this, Collider2D_t1890038195 * ___other0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.TypedMonoBehaviour::OnTriggerStay(UnityEngine.Collider)
extern "C"  void TypedMonoBehaviour_OnTriggerStay_m2579371340 (TypedMonoBehaviour_t4014417954 * __this, Collider_t955670625 * ___other0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.TypedMonoBehaviour::OnTriggerStay2D(UnityEngine.Collider2D)
extern "C"  void TypedMonoBehaviour_OnTriggerStay2D_m1586904908 (TypedMonoBehaviour_t4014417954 * __this, Collider2D_t1890038195 * ___other0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.TypedMonoBehaviour::OnValidate()
extern "C"  void TypedMonoBehaviour_OnValidate_m3797028498 (TypedMonoBehaviour_t4014417954 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.TypedMonoBehaviour::OnWillRenderObject()
extern "C"  void TypedMonoBehaviour_OnWillRenderObject_m2961611683 (TypedMonoBehaviour_t4014417954 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.TypedMonoBehaviour::Reset()
extern "C"  void TypedMonoBehaviour_Reset_m2837005908 (TypedMonoBehaviour_t4014417954 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.TypedMonoBehaviour::Start()
extern "C"  void TypedMonoBehaviour_Start_m4137710759 (TypedMonoBehaviour_t4014417954 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.TypedMonoBehaviour::Update()
extern "C"  void TypedMonoBehaviour_Update_m3720834118 (TypedMonoBehaviour_t4014417954 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.TypedMonoBehaviour::OnDisconnectedFromServer(UnityEngine.NetworkDisconnection)
extern "C"  void TypedMonoBehaviour_OnDisconnectedFromServer_m4256514671 (TypedMonoBehaviour_t4014417954 * __this, int32_t ___info0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.TypedMonoBehaviour::OnFailedToConnect(UnityEngine.NetworkConnectionError)
extern "C"  void TypedMonoBehaviour_OnFailedToConnect_m703172421 (TypedMonoBehaviour_t4014417954 * __this, int32_t ___error0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.TypedMonoBehaviour::OnFailedToConnectToMasterServer(UnityEngine.NetworkConnectionError)
extern "C"  void TypedMonoBehaviour_OnFailedToConnectToMasterServer_m953089413 (TypedMonoBehaviour_t4014417954 * __this, int32_t ___info0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.TypedMonoBehaviour::OnMasterServerEvent(UnityEngine.MasterServerEvent)
extern "C"  void TypedMonoBehaviour_OnMasterServerEvent_m1783233241 (TypedMonoBehaviour_t4014417954 * __this, int32_t ___msEvent0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.TypedMonoBehaviour::OnNetworkInstantiate(UnityEngine.NetworkMessageInfo)
extern "C"  void TypedMonoBehaviour_OnNetworkInstantiate_m1712604634 (TypedMonoBehaviour_t4014417954 * __this, NetworkMessageInfo_t2574344884  ___info0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.TypedMonoBehaviour::OnPlayerConnected(UnityEngine.NetworkPlayer)
extern "C"  void TypedMonoBehaviour_OnPlayerConnected_m946052684 (TypedMonoBehaviour_t4014417954 * __this, NetworkPlayer_t1281137372  ___player0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.TypedMonoBehaviour::OnPlayerDisconnected(UnityEngine.NetworkPlayer)
extern "C"  void TypedMonoBehaviour_OnPlayerDisconnected_m47253858 (TypedMonoBehaviour_t4014417954 * __this, NetworkPlayer_t1281137372  ___player0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.TypedMonoBehaviour::OnSerializeNetworkView(UnityEngine.BitStream,UnityEngine.NetworkMessageInfo)
extern "C"  void TypedMonoBehaviour_OnSerializeNetworkView_m1833181083 (TypedMonoBehaviour_t4014417954 * __this, BitStream_t3159523098 * ___stream0, NetworkMessageInfo_t2574344884  ___info1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
