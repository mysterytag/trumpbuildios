﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Zenject.BindingValidator/<ValidateContract>c__Iterator49
struct U3CValidateContractU3Ec__Iterator49_t2995849596;
// Zenject.ZenjectResolveException
struct ZenjectResolveException_t1201052999;
// System.Object
struct Il2CppObject;
// System.Collections.IEnumerator
struct IEnumerator_t287207039;
// System.Collections.Generic.IEnumerator`1<Zenject.ZenjectResolveException>
struct IEnumerator_1_t2684159447;

#include "codegen/il2cpp-codegen.h"

// System.Void Zenject.BindingValidator/<ValidateContract>c__Iterator49::.ctor()
extern "C"  void U3CValidateContractU3Ec__Iterator49__ctor_m835500226 (U3CValidateContractU3Ec__Iterator49_t2995849596 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Zenject.ZenjectResolveException Zenject.BindingValidator/<ValidateContract>c__Iterator49::System.Collections.Generic.IEnumerator<Zenject.ZenjectResolveException>.get_Current()
extern "C"  ZenjectResolveException_t1201052999 * U3CValidateContractU3Ec__Iterator49_System_Collections_Generic_IEnumeratorU3CZenject_ZenjectResolveExceptionU3E_get_Current_m3832075915 (U3CValidateContractU3Ec__Iterator49_t2995849596 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Zenject.BindingValidator/<ValidateContract>c__Iterator49::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CValidateContractU3Ec__Iterator49_System_Collections_IEnumerator_get_Current_m3731160814 (U3CValidateContractU3Ec__Iterator49_t2995849596 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator Zenject.BindingValidator/<ValidateContract>c__Iterator49::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * U3CValidateContractU3Ec__Iterator49_System_Collections_IEnumerable_GetEnumerator_m1476348399 (U3CValidateContractU3Ec__Iterator49_t2995849596 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.IEnumerator`1<Zenject.ZenjectResolveException> Zenject.BindingValidator/<ValidateContract>c__Iterator49::System.Collections.Generic.IEnumerable<Zenject.ZenjectResolveException>.GetEnumerator()
extern "C"  Il2CppObject* U3CValidateContractU3Ec__Iterator49_System_Collections_Generic_IEnumerableU3CZenject_ZenjectResolveExceptionU3E_GetEnumerator_m298493910 (U3CValidateContractU3Ec__Iterator49_t2995849596 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Zenject.BindingValidator/<ValidateContract>c__Iterator49::MoveNext()
extern "C"  bool U3CValidateContractU3Ec__Iterator49_MoveNext_m261423578 (U3CValidateContractU3Ec__Iterator49_t2995849596 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Zenject.BindingValidator/<ValidateContract>c__Iterator49::Dispose()
extern "C"  void U3CValidateContractU3Ec__Iterator49_Dispose_m3950083199 (U3CValidateContractU3Ec__Iterator49_t2995849596 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Zenject.BindingValidator/<ValidateContract>c__Iterator49::Reset()
extern "C"  void U3CValidateContractU3Ec__Iterator49_Reset_m2776900463 (U3CValidateContractU3Ec__Iterator49_t2995849596 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
