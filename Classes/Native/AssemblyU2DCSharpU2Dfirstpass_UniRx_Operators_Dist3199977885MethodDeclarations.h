﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.DistinctUntilChangedObservable`1<System.Boolean>
struct DistinctUntilChangedObservable_1_t3199977885;
// UniRx.IObservable`1<System.Boolean>
struct IObservable_1_t4264771001;
// System.Collections.Generic.IEqualityComparer`1<System.Boolean>
struct IEqualityComparer_1_t2535271992;
// System.IDisposable
struct IDisposable_t1628921374;
// UniRx.IObserver`1<System.Boolean>
struct IObserver_1_t2423004244;

#include "codegen/il2cpp-codegen.h"

// System.Void UniRx.Operators.DistinctUntilChangedObservable`1<System.Boolean>::.ctor(UniRx.IObservable`1<T>,System.Collections.Generic.IEqualityComparer`1<T>)
extern "C"  void DistinctUntilChangedObservable_1__ctor_m1803329520_gshared (DistinctUntilChangedObservable_1_t3199977885 * __this, Il2CppObject* ___source0, Il2CppObject* ___comparer1, const MethodInfo* method);
#define DistinctUntilChangedObservable_1__ctor_m1803329520(__this, ___source0, ___comparer1, method) ((  void (*) (DistinctUntilChangedObservable_1_t3199977885 *, Il2CppObject*, Il2CppObject*, const MethodInfo*))DistinctUntilChangedObservable_1__ctor_m1803329520_gshared)(__this, ___source0, ___comparer1, method)
// System.IDisposable UniRx.Operators.DistinctUntilChangedObservable`1<System.Boolean>::SubscribeCore(UniRx.IObserver`1<T>,System.IDisposable)
extern "C"  Il2CppObject * DistinctUntilChangedObservable_1_SubscribeCore_m3109991909_gshared (DistinctUntilChangedObservable_1_t3199977885 * __this, Il2CppObject* ___observer0, Il2CppObject * ___cancel1, const MethodInfo* method);
#define DistinctUntilChangedObservable_1_SubscribeCore_m3109991909(__this, ___observer0, ___cancel1, method) ((  Il2CppObject * (*) (DistinctUntilChangedObservable_1_t3199977885 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))DistinctUntilChangedObservable_1_SubscribeCore_m3109991909_gshared)(__this, ___observer0, ___cancel1, method)
