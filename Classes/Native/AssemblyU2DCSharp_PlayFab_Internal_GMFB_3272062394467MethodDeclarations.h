﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.Internal.GMFB_327
struct GMFB_327_t2062394467;

#include "codegen/il2cpp-codegen.h"

// System.Void PlayFab.Internal.GMFB_327::.ctor()
extern "C"  void GMFB_327__ctor_m3004541594 (GMFB_327_t2062394467 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.Internal.GMFB_327::TimeStampHandlesAllFormats()
extern "C"  void GMFB_327_TimeStampHandlesAllFormats_m3025326202 (GMFB_327_t2062394467 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.Internal.GMFB_327::JsonTimeStampHandlesAllFormats()
extern "C"  void GMFB_327_JsonTimeStampHandlesAllFormats_m3878152450 (GMFB_327_t2062394467 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.Internal.GMFB_327::EnumConversionTest_Serialize()
extern "C"  void GMFB_327_EnumConversionTest_Serialize_m4153322004 (GMFB_327_t2062394467 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.Internal.GMFB_327::EnumConversionTest_Deserialize()
extern "C"  void GMFB_327_EnumConversionTest_Deserialize_m4207850035 (GMFB_327_t2062394467 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.Internal.GMFB_327::EnumConversionTest_OptionalEnum()
extern "C"  void GMFB_327_EnumConversionTest_OptionalEnum_m2027914799 (GMFB_327_t2062394467 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
