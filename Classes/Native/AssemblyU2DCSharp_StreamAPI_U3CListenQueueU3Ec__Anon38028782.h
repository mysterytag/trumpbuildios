﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Generic.ICollection`1<System.Action`1<System.Object>>
struct ICollection_1_t1451390512;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// StreamAPI/<ListenQueue>c__AnonStorey12D`1<System.Object>
struct  U3CListenQueueU3Ec__AnonStorey12D_1_t38028782  : public Il2CppObject
{
public:
	// System.Collections.Generic.ICollection`1<System.Action`1<T>> StreamAPI/<ListenQueue>c__AnonStorey12D`1::collection
	Il2CppObject* ___collection_0;

public:
	inline static int32_t get_offset_of_collection_0() { return static_cast<int32_t>(offsetof(U3CListenQueueU3Ec__AnonStorey12D_1_t38028782, ___collection_0)); }
	inline Il2CppObject* get_collection_0() const { return ___collection_0; }
	inline Il2CppObject** get_address_of_collection_0() { return &___collection_0; }
	inline void set_collection_0(Il2CppObject* value)
	{
		___collection_0 = value;
		Il2CppCodeGenWriteBarrier(&___collection_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
