﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// System.Collections.Generic.List`1<System.String>
struct List_1_t1765447871;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PlayFab.ClientModels.AcceptTradeRequest
struct  AcceptTradeRequest_t1814136291  : public Il2CppObject
{
public:
	// System.String PlayFab.ClientModels.AcceptTradeRequest::<OfferingPlayerId>k__BackingField
	String_t* ___U3COfferingPlayerIdU3Ek__BackingField_0;
	// System.String PlayFab.ClientModels.AcceptTradeRequest::<TradeId>k__BackingField
	String_t* ___U3CTradeIdU3Ek__BackingField_1;
	// System.Collections.Generic.List`1<System.String> PlayFab.ClientModels.AcceptTradeRequest::<AcceptedInventoryInstanceIds>k__BackingField
	List_1_t1765447871 * ___U3CAcceptedInventoryInstanceIdsU3Ek__BackingField_2;

public:
	inline static int32_t get_offset_of_U3COfferingPlayerIdU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(AcceptTradeRequest_t1814136291, ___U3COfferingPlayerIdU3Ek__BackingField_0)); }
	inline String_t* get_U3COfferingPlayerIdU3Ek__BackingField_0() const { return ___U3COfferingPlayerIdU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3COfferingPlayerIdU3Ek__BackingField_0() { return &___U3COfferingPlayerIdU3Ek__BackingField_0; }
	inline void set_U3COfferingPlayerIdU3Ek__BackingField_0(String_t* value)
	{
		___U3COfferingPlayerIdU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier(&___U3COfferingPlayerIdU3Ek__BackingField_0, value);
	}

	inline static int32_t get_offset_of_U3CTradeIdU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(AcceptTradeRequest_t1814136291, ___U3CTradeIdU3Ek__BackingField_1)); }
	inline String_t* get_U3CTradeIdU3Ek__BackingField_1() const { return ___U3CTradeIdU3Ek__BackingField_1; }
	inline String_t** get_address_of_U3CTradeIdU3Ek__BackingField_1() { return &___U3CTradeIdU3Ek__BackingField_1; }
	inline void set_U3CTradeIdU3Ek__BackingField_1(String_t* value)
	{
		___U3CTradeIdU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CTradeIdU3Ek__BackingField_1, value);
	}

	inline static int32_t get_offset_of_U3CAcceptedInventoryInstanceIdsU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(AcceptTradeRequest_t1814136291, ___U3CAcceptedInventoryInstanceIdsU3Ek__BackingField_2)); }
	inline List_1_t1765447871 * get_U3CAcceptedInventoryInstanceIdsU3Ek__BackingField_2() const { return ___U3CAcceptedInventoryInstanceIdsU3Ek__BackingField_2; }
	inline List_1_t1765447871 ** get_address_of_U3CAcceptedInventoryInstanceIdsU3Ek__BackingField_2() { return &___U3CAcceptedInventoryInstanceIdsU3Ek__BackingField_2; }
	inline void set_U3CAcceptedInventoryInstanceIdsU3Ek__BackingField_2(List_1_t1765447871 * value)
	{
		___U3CAcceptedInventoryInstanceIdsU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier(&___U3CAcceptedInventoryInstanceIdsU3Ek__BackingField_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
