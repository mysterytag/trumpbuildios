﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_21270642218MethodDeclarations.h"

// System.Void System.Collections.Generic.KeyValuePair`2<System.String,PlayFab.UUnit.Region>::.ctor(TKey,TValue)
#define KeyValuePair_2__ctor_m1050022383(__this, ___key0, ___value1, method) ((  void (*) (KeyValuePair_2_t4215988688 *, String_t*, int32_t, const MethodInfo*))KeyValuePair_2__ctor_m582974365_gshared)(__this, ___key0, ___value1, method)
// TKey System.Collections.Generic.KeyValuePair`2<System.String,PlayFab.UUnit.Region>::get_Key()
#define KeyValuePair_2_get_Key_m1683591449(__this, method) ((  String_t* (*) (KeyValuePair_2_t4215988688 *, const MethodInfo*))KeyValuePair_2_get_Key_m2845305771_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.String,PlayFab.UUnit.Region>::set_Key(TKey)
#define KeyValuePair_2_set_Key_m2747502298(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t4215988688 *, String_t*, const MethodInfo*))KeyValuePair_2_set_Key_m374231148_gshared)(__this, ___value0, method)
// TValue System.Collections.Generic.KeyValuePair`2<System.String,PlayFab.UUnit.Region>::get_Value()
#define KeyValuePair_2_get_Value_m56019737(__this, method) ((  int32_t (*) (KeyValuePair_2_t4215988688 *, const MethodInfo*))KeyValuePair_2_get_Value_m4066953515_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.String,PlayFab.UUnit.Region>::set_Value(TValue)
#define KeyValuePair_2_set_Value_m3685213658(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t4215988688 *, int32_t, const MethodInfo*))KeyValuePair_2_set_Value_m2700316268_gshared)(__this, ___value0, method)
// System.String System.Collections.Generic.KeyValuePair`2<System.String,PlayFab.UUnit.Region>::ToString()
#define KeyValuePair_2_ToString_m304634568(__this, method) ((  String_t* (*) (KeyValuePair_2_t4215988688 *, const MethodInfo*))KeyValuePair_2_ToString_m1958040182_gshared)(__this, method)
