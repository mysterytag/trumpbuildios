﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.UUnit.UUnitSkipException
struct UUnitSkipException_t2699752577;

#include "codegen/il2cpp-codegen.h"

// System.Void PlayFab.UUnit.UUnitSkipException::.ctor()
extern "C"  void UUnitSkipException__ctor_m1983279316 (UUnitSkipException_t2699752577 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
