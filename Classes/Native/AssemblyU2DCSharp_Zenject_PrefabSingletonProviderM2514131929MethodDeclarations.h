﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Zenject.PrefabSingletonProviderMap
struct PrefabSingletonProviderMap_t2514131929;
// Zenject.DiContainer
struct DiContainer_t2383114449;
// System.Collections.Generic.IEnumerable`1<Zenject.PrefabSingletonLazyCreator>
struct IEnumerable_1_t2296191290;
// Zenject.PrefabSingletonId
struct PrefabSingletonId_t3643845047;
// Zenject.PrefabSingletonLazyCreator
struct PrefabSingletonLazyCreator_t3719004230;
// Zenject.ProviderBase
struct ProviderBase_t1627494391;
// System.String
struct String_t;
// System.Type
struct Type_t;
// UnityEngine.GameObject
struct GameObject_t4012695102;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Zenject_DiContainer2383114449.h"
#include "AssemblyU2DCSharp_Zenject_PrefabSingletonId3643845047.h"
#include "mscorlib_System_String968488902.h"
#include "mscorlib_System_Type2779229935.h"
#include "UnityEngine_UnityEngine_GameObject4012695102.h"

// System.Void Zenject.PrefabSingletonProviderMap::.ctor(Zenject.DiContainer)
extern "C"  void PrefabSingletonProviderMap__ctor_m3844939645 (PrefabSingletonProviderMap_t2514131929 * __this, DiContainer_t2383114449 * ___container0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.IEnumerable`1<Zenject.PrefabSingletonLazyCreator> Zenject.PrefabSingletonProviderMap::get_Creators()
extern "C"  Il2CppObject* PrefabSingletonProviderMap_get_Creators_m1687508148 (PrefabSingletonProviderMap_t2514131929 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Zenject.PrefabSingletonProviderMap::RemoveCreator(Zenject.PrefabSingletonId)
extern "C"  void PrefabSingletonProviderMap_RemoveCreator_m2832790769 (PrefabSingletonProviderMap_t2514131929 * __this, PrefabSingletonId_t3643845047 * ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Zenject.PrefabSingletonLazyCreator Zenject.PrefabSingletonProviderMap::AddCreator(Zenject.PrefabSingletonId)
extern "C"  PrefabSingletonLazyCreator_t3719004230 * PrefabSingletonProviderMap_AddCreator_m3853045611 (PrefabSingletonProviderMap_t2514131929 * __this, PrefabSingletonId_t3643845047 * ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Zenject.ProviderBase Zenject.PrefabSingletonProviderMap::CreateProvider(System.String,System.Type,UnityEngine.GameObject,System.String)
extern "C"  ProviderBase_t1627494391 * PrefabSingletonProviderMap_CreateProvider_m79043514 (PrefabSingletonProviderMap_t2514131929 * __this, String_t* ___identifier0, Type_t * ___concreteType1, GameObject_t4012695102 * ___prefab2, String_t* ___resourcePath3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
