﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UniRx.Subject`1<UniRx.Unit>
struct Subject_1_t201353362;

#include "UnityEngine_UnityEngine_MonoBehaviour3012272455.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BottomButton
struct  BottomButton_t1932555613  : public MonoBehaviour_t3012272455
{
public:
	// UniRx.Subject`1<UniRx.Unit> BottomButton::ClickSubject
	Subject_1_t201353362 * ___ClickSubject_2;

public:
	inline static int32_t get_offset_of_ClickSubject_2() { return static_cast<int32_t>(offsetof(BottomButton_t1932555613, ___ClickSubject_2)); }
	inline Subject_1_t201353362 * get_ClickSubject_2() const { return ___ClickSubject_2; }
	inline Subject_1_t201353362 ** get_address_of_ClickSubject_2() { return &___ClickSubject_2; }
	inline void set_ClickSubject_2(Subject_1_t201353362 * value)
	{
		___ClickSubject_2 = value;
		Il2CppCodeGenWriteBarrier(&___ClickSubject_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
