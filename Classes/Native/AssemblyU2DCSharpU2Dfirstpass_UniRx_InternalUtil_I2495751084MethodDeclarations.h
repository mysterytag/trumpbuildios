﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_I1897310919MethodDeclarations.h"

// System.Void UniRx.InternalUtil.ImmutableList`1<UniRx.IObserver`1<UnityEngine.Bounds>>::.ctor()
#define ImmutableList_1__ctor_m1256472485(__this, method) ((  void (*) (ImmutableList_1_t2495751084 *, const MethodInfo*))ImmutableList_1__ctor_m2467204245_gshared)(__this, method)
// System.Void UniRx.InternalUtil.ImmutableList`1<UniRx.IObserver`1<UnityEngine.Bounds>>::.ctor(T[])
#define ImmutableList_1__ctor_m933187383(__this, ___data0, method) ((  void (*) (ImmutableList_1_t2495751084 *, IObserver_1U5BU5D_t579021988*, const MethodInfo*))ImmutableList_1__ctor_m707697735_gshared)(__this, ___data0, method)
// System.Void UniRx.InternalUtil.ImmutableList`1<UniRx.IObserver`1<UnityEngine.Bounds>>::.cctor()
#define ImmutableList_1__cctor_m4108812456(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))ImmutableList_1__cctor_m2986791352_gshared)(__this /* static, unused */, method)
// T[] UniRx.InternalUtil.ImmutableList`1<UniRx.IObserver`1<UnityEngine.Bounds>>::get_Data()
#define ImmutableList_1_get_Data_m411307445(__this, method) ((  IObserver_1U5BU5D_t579021988* (*) (ImmutableList_1_t2495751084 *, const MethodInfo*))ImmutableList_1_get_Data_m1676800965_gshared)(__this, method)
// UniRx.InternalUtil.ImmutableList`1<T> UniRx.InternalUtil.ImmutableList`1<UniRx.IObserver`1<UnityEngine.Bounds>>::Add(T)
#define ImmutableList_1_Add_m4111549683(__this, ___value0, method) ((  ImmutableList_1_t2495751084 * (*) (ImmutableList_1_t2495751084 *, Il2CppObject*, const MethodInfo*))ImmutableList_1_Add_m948892931_gshared)(__this, ___value0, method)
// UniRx.InternalUtil.ImmutableList`1<T> UniRx.InternalUtil.ImmutableList`1<UniRx.IObserver`1<UnityEngine.Bounds>>::Remove(T)
#define ImmutableList_1_Remove_m1548643682(__this, ___value0, method) ((  ImmutableList_1_t2495751084 * (*) (ImmutableList_1_t2495751084 *, Il2CppObject*, const MethodInfo*))ImmutableList_1_Remove_m1538917202_gshared)(__this, ___value0, method)
// System.Int32 UniRx.InternalUtil.ImmutableList`1<UniRx.IObserver`1<UnityEngine.Bounds>>::IndexOf(T)
#define ImmutableList_1_IndexOf_m1729450588(__this, ___value0, method) ((  int32_t (*) (ImmutableList_1_t2495751084 *, Il2CppObject*, const MethodInfo*))ImmutableList_1_IndexOf_m3149323628_gshared)(__this, ___value0, method)
