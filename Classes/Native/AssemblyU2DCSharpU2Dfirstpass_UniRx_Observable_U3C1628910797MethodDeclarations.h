﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Observable/<SelectMany>c__AnonStorey4E`1<UniRx.Unit>
struct U3CSelectManyU3Ec__AnonStorey4E_1_t1628910797;
// System.Collections.IEnumerator
struct IEnumerator_t287207039;

#include "codegen/il2cpp-codegen.h"

// System.Void UniRx.Observable/<SelectMany>c__AnonStorey4E`1<UniRx.Unit>::.ctor()
extern "C"  void U3CSelectManyU3Ec__AnonStorey4E_1__ctor_m1139591771_gshared (U3CSelectManyU3Ec__AnonStorey4E_1_t1628910797 * __this, const MethodInfo* method);
#define U3CSelectManyU3Ec__AnonStorey4E_1__ctor_m1139591771(__this, method) ((  void (*) (U3CSelectManyU3Ec__AnonStorey4E_1_t1628910797 *, const MethodInfo*))U3CSelectManyU3Ec__AnonStorey4E_1__ctor_m1139591771_gshared)(__this, method)
// System.Collections.IEnumerator UniRx.Observable/<SelectMany>c__AnonStorey4E`1<UniRx.Unit>::<>m__4C()
extern "C"  Il2CppObject * U3CSelectManyU3Ec__AnonStorey4E_1_U3CU3Em__4C_m3577479683_gshared (U3CSelectManyU3Ec__AnonStorey4E_1_t1628910797 * __this, const MethodInfo* method);
#define U3CSelectManyU3Ec__AnonStorey4E_1_U3CU3Em__4C_m3577479683(__this, method) ((  Il2CppObject * (*) (U3CSelectManyU3Ec__AnonStorey4E_1_t1628910797 *, const MethodInfo*))U3CSelectManyU3Ec__AnonStorey4E_1_U3CU3Em__4C_m3577479683_gshared)(__this, method)
