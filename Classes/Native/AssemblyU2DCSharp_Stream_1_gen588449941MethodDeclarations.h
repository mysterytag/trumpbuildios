﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Stream_1_gen3823212738MethodDeclarations.h"

// System.Void Stream`1<UniRx.InternalUtil.ImmutableList`1<System.Object>>::.ctor()
#define Stream_1__ctor_m3308650372(__this, method) ((  void (*) (Stream_1_t588449941 *, const MethodInfo*))Stream_1__ctor_m2034388992_gshared)(__this, method)
// System.Void Stream`1<UniRx.InternalUtil.ImmutableList`1<System.Object>>::Send(T)
#define Stream_1_Send_m1837867926(__this, ___value0, method) ((  void (*) (Stream_1_t588449941 *, ImmutableList_1_t1897310919 *, const MethodInfo*))Stream_1_Send_m563606546_gshared)(__this, ___value0, method)
// System.Void Stream`1<UniRx.InternalUtil.ImmutableList`1<System.Object>>::SendInTransaction(T,System.Int32)
#define Stream_1_SendInTransaction_m1255958444(__this, ___value0, ___p1, method) ((  void (*) (Stream_1_t588449941 *, ImmutableList_1_t1897310919 *, int32_t, const MethodInfo*))Stream_1_SendInTransaction_m3764966696_gshared)(__this, ___value0, ___p1, method)
// System.IDisposable Stream`1<UniRx.InternalUtil.ImmutableList`1<System.Object>>::Listen(System.Action`1<T>,Priority)
#define Stream_1_Listen_m329723630(__this, ___action0, ___priority1, method) ((  Il2CppObject * (*) (Stream_1_t588449941 *, Action_1_t2045763624 *, int32_t, const MethodInfo*))Stream_1_Listen_m889871082_gshared)(__this, ___action0, ___priority1, method)
// System.IDisposable Stream`1<UniRx.InternalUtil.ImmutableList`1<System.Object>>::Listen(System.Action,Priority)
#define Stream_1_Listen_m3172447609(__this, ___action0, ___p1, method) ((  Il2CppObject * (*) (Stream_1_t588449941 *, Action_t437523947 *, int32_t, const MethodInfo*))Stream_1_Listen_m2133851133_gshared)(__this, ___action0, ___p1, method)
// System.Void Stream`1<UniRx.InternalUtil.ImmutableList`1<System.Object>>::Dispose()
#define Stream_1_Dispose_m1235491521(__this, method) ((  void (*) (Stream_1_t588449941 *, const MethodInfo*))Stream_1_Dispose_m735984701_gshared)(__this, method)
// System.Void Stream`1<UniRx.InternalUtil.ImmutableList`1<System.Object>>::SetInputStream(IStream`1<T>)
#define Stream_1_SetInputStream_m983886588(__this, ___stream0, method) ((  void (*) (Stream_1_t588449941 *, Il2CppObject*, const MethodInfo*))Stream_1_SetInputStream_m3106996224_gshared)(__this, ___stream0, method)
// System.Void Stream`1<UniRx.InternalUtil.ImmutableList`1<System.Object>>::ClearInputStream()
#define Stream_1_ClearInputStream_m1348658909(__this, method) ((  void (*) (Stream_1_t588449941 *, const MethodInfo*))Stream_1_ClearInputStream_m445690081_gshared)(__this, method)
// System.Void Stream`1<UniRx.InternalUtil.ImmutableList`1<System.Object>>::TransactionIterationFinished()
#define Stream_1_TransactionIterationFinished_m2914665265(__this, method) ((  void (*) (Stream_1_t588449941 *, const MethodInfo*))Stream_1_TransactionIterationFinished_m3113559861_gshared)(__this, method)
// System.Void Stream`1<UniRx.InternalUtil.ImmutableList`1<System.Object>>::Unpack(System.Int32)
#define Stream_1_Unpack_m3557903235(__this, ___p0, method) ((  void (*) (Stream_1_t588449941 *, int32_t, const MethodInfo*))Stream_1_Unpack_m3388253319_gshared)(__this, ___p0, method)
// System.Void Stream`1<UniRx.InternalUtil.ImmutableList`1<System.Object>>::<SetInputStream>m__1BA(T)
#define Stream_1_U3CSetInputStreamU3Em__1BA_m2939954465(__this, ___val0, method) ((  void (*) (Stream_1_t588449941 *, ImmutableList_1_t1897310919 *, const MethodInfo*))Stream_1_U3CSetInputStreamU3Em__1BA_m3579563677_gshared)(__this, ___val0, method)
