﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// System.Collections.Generic.List`1<System.String>
struct List_1_t1765447871;
// System.Collections.Generic.Dictionary`2<System.String,System.UInt32>
struct Dictionary_2_t2623623230;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PlayFab.ClientModels.CatalogItemContainerInfo
struct  CatalogItemContainerInfo_t284142995  : public Il2CppObject
{
public:
	// System.String PlayFab.ClientModels.CatalogItemContainerInfo::<KeyItemId>k__BackingField
	String_t* ___U3CKeyItemIdU3Ek__BackingField_0;
	// System.Collections.Generic.List`1<System.String> PlayFab.ClientModels.CatalogItemContainerInfo::<ItemContents>k__BackingField
	List_1_t1765447871 * ___U3CItemContentsU3Ek__BackingField_1;
	// System.Collections.Generic.List`1<System.String> PlayFab.ClientModels.CatalogItemContainerInfo::<ResultTableContents>k__BackingField
	List_1_t1765447871 * ___U3CResultTableContentsU3Ek__BackingField_2;
	// System.Collections.Generic.Dictionary`2<System.String,System.UInt32> PlayFab.ClientModels.CatalogItemContainerInfo::<VirtualCurrencyContents>k__BackingField
	Dictionary_2_t2623623230 * ___U3CVirtualCurrencyContentsU3Ek__BackingField_3;

public:
	inline static int32_t get_offset_of_U3CKeyItemIdU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(CatalogItemContainerInfo_t284142995, ___U3CKeyItemIdU3Ek__BackingField_0)); }
	inline String_t* get_U3CKeyItemIdU3Ek__BackingField_0() const { return ___U3CKeyItemIdU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CKeyItemIdU3Ek__BackingField_0() { return &___U3CKeyItemIdU3Ek__BackingField_0; }
	inline void set_U3CKeyItemIdU3Ek__BackingField_0(String_t* value)
	{
		___U3CKeyItemIdU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier(&___U3CKeyItemIdU3Ek__BackingField_0, value);
	}

	inline static int32_t get_offset_of_U3CItemContentsU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(CatalogItemContainerInfo_t284142995, ___U3CItemContentsU3Ek__BackingField_1)); }
	inline List_1_t1765447871 * get_U3CItemContentsU3Ek__BackingField_1() const { return ___U3CItemContentsU3Ek__BackingField_1; }
	inline List_1_t1765447871 ** get_address_of_U3CItemContentsU3Ek__BackingField_1() { return &___U3CItemContentsU3Ek__BackingField_1; }
	inline void set_U3CItemContentsU3Ek__BackingField_1(List_1_t1765447871 * value)
	{
		___U3CItemContentsU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CItemContentsU3Ek__BackingField_1, value);
	}

	inline static int32_t get_offset_of_U3CResultTableContentsU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(CatalogItemContainerInfo_t284142995, ___U3CResultTableContentsU3Ek__BackingField_2)); }
	inline List_1_t1765447871 * get_U3CResultTableContentsU3Ek__BackingField_2() const { return ___U3CResultTableContentsU3Ek__BackingField_2; }
	inline List_1_t1765447871 ** get_address_of_U3CResultTableContentsU3Ek__BackingField_2() { return &___U3CResultTableContentsU3Ek__BackingField_2; }
	inline void set_U3CResultTableContentsU3Ek__BackingField_2(List_1_t1765447871 * value)
	{
		___U3CResultTableContentsU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier(&___U3CResultTableContentsU3Ek__BackingField_2, value);
	}

	inline static int32_t get_offset_of_U3CVirtualCurrencyContentsU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(CatalogItemContainerInfo_t284142995, ___U3CVirtualCurrencyContentsU3Ek__BackingField_3)); }
	inline Dictionary_2_t2623623230 * get_U3CVirtualCurrencyContentsU3Ek__BackingField_3() const { return ___U3CVirtualCurrencyContentsU3Ek__BackingField_3; }
	inline Dictionary_2_t2623623230 ** get_address_of_U3CVirtualCurrencyContentsU3Ek__BackingField_3() { return &___U3CVirtualCurrencyContentsU3Ek__BackingField_3; }
	inline void set_U3CVirtualCurrencyContentsU3Ek__BackingField_3(Dictionary_2_t2623623230 * value)
	{
		___U3CVirtualCurrencyContentsU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier(&___U3CVirtualCurrencyContentsU3Ek__BackingField_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
