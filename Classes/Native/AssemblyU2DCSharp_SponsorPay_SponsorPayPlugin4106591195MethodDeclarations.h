﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SponsorPay.SponsorPayPlugin
struct SponsorPayPlugin_t4106591195;
// UnityEngine.GameObject
struct GameObject_t4012695102;
// SponsorPay.DeltaOfCoinsResponseReceivedHandler
struct DeltaOfCoinsResponseReceivedHandler_t2871420975;
// SponsorPay.SuccessfulCurrencyResponseReceivedHandler
struct SuccessfulCurrencyResponseReceivedHandler_t2285112823;
// SponsorPay.ErrorHandler
struct ErrorHandler_t1258969084;
// SponsorPay.OfferWallResultHandler
struct OfferWallResultHandler_t3220154273;
// SponsorPay.BrandEngageRequestResponseReceivedHandler
struct BrandEngageRequestResponseReceivedHandler_t263189403;
// SponsorPay.BrandEngageRequestErrorReceivedHandler
struct BrandEngageRequestErrorReceivedHandler_t816797986;
// SponsorPay.BrandEngageResultHandler
struct BrandEngageResultHandler_t570975071;
// SponsorPay.InterstitialRequestResponseReceivedHandler
struct InterstitialRequestResponseReceivedHandler_t2506718783;
// SponsorPay.InterstitialRequestErrorReceivedHandler
struct InterstitialRequestErrorReceivedHandler_t2305716478;
// SponsorPay.InterstitialStatusCloseHandler
struct InterstitialStatusCloseHandler_t3952291690;
// SponsorPay.InterstitialStatusErrorHandler
struct InterstitialStatusErrorHandler_t1250724186;
// SponsorPay.NativeExceptionHandler
struct NativeExceptionHandler_t1751116300;
// System.String
struct String_t;
// System.Collections.Generic.Dictionary`2<System.String,System.String>
struct Dictionary_2_t2606186806;
// SponsorPay.SPUser
struct SPUser_t2606281602;
// System.String[]
struct StringU5BU5D_t2956870243;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_GameObject4012695102.h"
#include "AssemblyU2DCSharp_SponsorPay_DeltaOfCoinsResponseR2871420975.h"
#include "AssemblyU2DCSharp_SponsorPay_SuccessfulCurrencyRes2285112823.h"
#include "AssemblyU2DCSharp_SponsorPay_ErrorHandler1258969084.h"
#include "AssemblyU2DCSharp_SponsorPay_OfferWallResultHandle3220154273.h"
#include "AssemblyU2DCSharp_SponsorPay_BrandEngageRequestResp263189403.h"
#include "AssemblyU2DCSharp_SponsorPay_BrandEngageRequestErro816797986.h"
#include "AssemblyU2DCSharp_SponsorPay_BrandEngageResultHandl570975071.h"
#include "AssemblyU2DCSharp_SponsorPay_InterstitialRequestRe2506718783.h"
#include "AssemblyU2DCSharp_SponsorPay_InterstitialRequestEr2305716478.h"
#include "AssemblyU2DCSharp_SponsorPay_InterstitialStatusClo3952291690.h"
#include "AssemblyU2DCSharp_SponsorPay_InterstitialStatusErr1250724186.h"
#include "AssemblyU2DCSharp_SponsorPay_NativeExceptionHandle1751116300.h"
#include "mscorlib_System_String968488902.h"
#include "AssemblyU2DCSharp_SponsorPay_SPLogLevel220722135.h"

// System.Void SponsorPay.SponsorPayPlugin::.ctor(UnityEngine.GameObject)
extern "C"  void SponsorPayPlugin__ctor_m2310569410 (SponsorPayPlugin_t4106591195 * __this, GameObject_t4012695102 * ___gameObject0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SponsorPay.SponsorPayPlugin::add_OnDeltaOfCoinsReceived(SponsorPay.DeltaOfCoinsResponseReceivedHandler)
extern "C"  void SponsorPayPlugin_add_OnDeltaOfCoinsReceived_m2485212406 (SponsorPayPlugin_t4106591195 * __this, DeltaOfCoinsResponseReceivedHandler_t2871420975 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SponsorPay.SponsorPayPlugin::remove_OnDeltaOfCoinsReceived(SponsorPay.DeltaOfCoinsResponseReceivedHandler)
extern "C"  void SponsorPayPlugin_remove_OnDeltaOfCoinsReceived_m449014753 (SponsorPayPlugin_t4106591195 * __this, DeltaOfCoinsResponseReceivedHandler_t2871420975 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SponsorPay.SponsorPayPlugin::add_OnSuccessfulCurrencyRequestReceived(SponsorPay.SuccessfulCurrencyResponseReceivedHandler)
extern "C"  void SponsorPayPlugin_add_OnSuccessfulCurrencyRequestReceived_m1050637363 (SponsorPayPlugin_t4106591195 * __this, SuccessfulCurrencyResponseReceivedHandler_t2285112823 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SponsorPay.SponsorPayPlugin::remove_OnSuccessfulCurrencyRequestReceived(SponsorPay.SuccessfulCurrencyResponseReceivedHandler)
extern "C"  void SponsorPayPlugin_remove_OnSuccessfulCurrencyRequestReceived_m716793704 (SponsorPayPlugin_t4106591195 * __this, SuccessfulCurrencyResponseReceivedHandler_t2285112823 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SponsorPay.SponsorPayPlugin::add_OnDeltaOfCoinsRequestFailed(SponsorPay.ErrorHandler)
extern "C"  void SponsorPayPlugin_add_OnDeltaOfCoinsRequestFailed_m965025036 (SponsorPayPlugin_t4106591195 * __this, ErrorHandler_t1258969084 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SponsorPay.SponsorPayPlugin::remove_OnDeltaOfCoinsRequestFailed(SponsorPay.ErrorHandler)
extern "C"  void SponsorPayPlugin_remove_OnDeltaOfCoinsRequestFailed_m1577363639 (SponsorPayPlugin_t4106591195 * __this, ErrorHandler_t1258969084 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SponsorPay.SponsorPayPlugin::add_OnOfferWallResultReceived(SponsorPay.OfferWallResultHandler)
extern "C"  void SponsorPayPlugin_add_OnOfferWallResultReceived_m3561056572 (SponsorPayPlugin_t4106591195 * __this, OfferWallResultHandler_t3220154273 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SponsorPay.SponsorPayPlugin::remove_OnOfferWallResultReceived(SponsorPay.OfferWallResultHandler)
extern "C"  void SponsorPayPlugin_remove_OnOfferWallResultReceived_m1002183143 (SponsorPayPlugin_t4106591195 * __this, OfferWallResultHandler_t3220154273 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SponsorPay.SponsorPayPlugin::add_OnBrandEngageRequestResponseReceived(SponsorPay.BrandEngageRequestResponseReceivedHandler)
extern "C"  void SponsorPayPlugin_add_OnBrandEngageRequestResponseReceived_m657645503 (SponsorPayPlugin_t4106591195 * __this, BrandEngageRequestResponseReceivedHandler_t263189403 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SponsorPay.SponsorPayPlugin::remove_OnBrandEngageRequestResponseReceived(SponsorPay.BrandEngageRequestResponseReceivedHandler)
extern "C"  void SponsorPayPlugin_remove_OnBrandEngageRequestResponseReceived_m3193393962 (SponsorPayPlugin_t4106591195 * __this, BrandEngageRequestResponseReceivedHandler_t263189403 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SponsorPay.SponsorPayPlugin::add_OnBrandEngageRequestErrorReceived(SponsorPay.BrandEngageRequestErrorReceivedHandler)
extern "C"  void SponsorPayPlugin_add_OnBrandEngageRequestErrorReceived_m1495487101 (SponsorPayPlugin_t4106591195 * __this, BrandEngageRequestErrorReceivedHandler_t816797986 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SponsorPay.SponsorPayPlugin::remove_OnBrandEngageRequestErrorReceived(SponsorPay.BrandEngageRequestErrorReceivedHandler)
extern "C"  void SponsorPayPlugin_remove_OnBrandEngageRequestErrorReceived_m4090589736 (SponsorPayPlugin_t4106591195 * __this, BrandEngageRequestErrorReceivedHandler_t816797986 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SponsorPay.SponsorPayPlugin::add_OnBrandEngageResultReceived(SponsorPay.BrandEngageResultHandler)
extern "C"  void SponsorPayPlugin_add_OnBrandEngageResultReceived_m1711980156 (SponsorPayPlugin_t4106591195 * __this, BrandEngageResultHandler_t570975071 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SponsorPay.SponsorPayPlugin::remove_OnBrandEngageResultReceived(SponsorPay.BrandEngageResultHandler)
extern "C"  void SponsorPayPlugin_remove_OnBrandEngageResultReceived_m974594471 (SponsorPayPlugin_t4106591195 * __this, BrandEngageResultHandler_t570975071 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SponsorPay.SponsorPayPlugin::add_OnInterstitialRequestResponseReceived(SponsorPay.InterstitialRequestResponseReceivedHandler)
extern "C"  void SponsorPayPlugin_add_OnInterstitialRequestResponseReceived_m77348637 (SponsorPayPlugin_t4106591195 * __this, InterstitialRequestResponseReceivedHandler_t2506718783 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SponsorPay.SponsorPayPlugin::remove_OnInterstitialRequestResponseReceived(SponsorPay.InterstitialRequestResponseReceivedHandler)
extern "C"  void SponsorPayPlugin_remove_OnInterstitialRequestResponseReceived_m1685160904 (SponsorPayPlugin_t4106591195 * __this, InterstitialRequestResponseReceivedHandler_t2506718783 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SponsorPay.SponsorPayPlugin::add_OnInterstitialRequestErrorReceived(SponsorPay.InterstitialRequestErrorReceivedHandler)
extern "C"  void SponsorPayPlugin_add_OnInterstitialRequestErrorReceived_m3825771897 (SponsorPayPlugin_t4106591195 * __this, InterstitialRequestErrorReceivedHandler_t2305716478 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SponsorPay.SponsorPayPlugin::remove_OnInterstitialRequestErrorReceived(SponsorPay.InterstitialRequestErrorReceivedHandler)
extern "C"  void SponsorPayPlugin_remove_OnInterstitialRequestErrorReceived_m2343405156 (SponsorPayPlugin_t4106591195 * __this, InterstitialRequestErrorReceivedHandler_t2305716478 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SponsorPay.SponsorPayPlugin::add_OnInterstitialStatusCloseReceived(SponsorPay.InterstitialStatusCloseHandler)
extern "C"  void SponsorPayPlugin_add_OnInterstitialStatusCloseReceived_m2435156188 (SponsorPayPlugin_t4106591195 * __this, InterstitialStatusCloseHandler_t3952291690 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SponsorPay.SponsorPayPlugin::remove_OnInterstitialStatusCloseReceived(SponsorPay.InterstitialStatusCloseHandler)
extern "C"  void SponsorPayPlugin_remove_OnInterstitialStatusCloseReceived_m1857520519 (SponsorPayPlugin_t4106591195 * __this, InterstitialStatusCloseHandler_t3952291690 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SponsorPay.SponsorPayPlugin::add_OnInterstitialStatusErrorReceived(SponsorPay.InterstitialStatusErrorHandler)
extern "C"  void SponsorPayPlugin_add_OnInterstitialStatusErrorReceived_m2032508636 (SponsorPayPlugin_t4106591195 * __this, InterstitialStatusErrorHandler_t1250724186 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SponsorPay.SponsorPayPlugin::remove_OnInterstitialStatusErrorReceived(SponsorPay.InterstitialStatusErrorHandler)
extern "C"  void SponsorPayPlugin_remove_OnInterstitialStatusErrorReceived_m1454872967 (SponsorPayPlugin_t4106591195 * __this, InterstitialStatusErrorHandler_t1250724186 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SponsorPay.SponsorPayPlugin::add_OnNativeExceptionReceived(SponsorPay.NativeExceptionHandler)
extern "C"  void SponsorPayPlugin_add_OnNativeExceptionReceived_m3858111772 (SponsorPayPlugin_t4106591195 * __this, NativeExceptionHandler_t1751116300 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SponsorPay.SponsorPayPlugin::remove_OnNativeExceptionReceived(SponsorPay.NativeExceptionHandler)
extern "C"  void SponsorPayPlugin_remove_OnNativeExceptionReceived_m1299238343 (SponsorPayPlugin_t4106591195 * __this, NativeExceptionHandler_t1751116300 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String SponsorPay.SponsorPayPlugin::Start(System.String,System.String,System.String)
extern "C"  String_t* SponsorPayPlugin_Start_m2570944173 (SponsorPayPlugin_t4106591195 * __this, String_t* ___appId0, String_t* ___userId1, String_t* ___securityToken2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SponsorPay.SponsorPayPlugin::ReportActionCompletion(System.String)
extern "C"  void SponsorPayPlugin_ReportActionCompletion_m1300579970 (SponsorPayPlugin_t4106591195 * __this, String_t* ___actionId0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SponsorPay.SponsorPayPlugin::ReportActionCompletion(System.String,System.String)
extern "C"  void SponsorPayPlugin_ReportActionCompletion_m1907876670 (SponsorPayPlugin_t4106591195 * __this, String_t* ___credentialsToken0, String_t* ___actionId1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SponsorPay.SponsorPayPlugin::LaunchOfferWall(System.String)
extern "C"  void SponsorPayPlugin_LaunchOfferWall_m1246037415 (SponsorPayPlugin_t4106591195 * __this, String_t* ___currencyName0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SponsorPay.SponsorPayPlugin::LaunchOfferWall(System.String,System.String)
extern "C"  void SponsorPayPlugin_LaunchOfferWall_m1697889699 (SponsorPayPlugin_t4106591195 * __this, String_t* ___credentialsToken0, String_t* ___currencyName1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SponsorPay.SponsorPayPlugin::LaunchOfferWall(System.String,System.String,System.String)
extern "C"  void SponsorPayPlugin_LaunchOfferWall_m345164959 (SponsorPayPlugin_t4106591195 * __this, String_t* ___credentialsToken0, String_t* ___currencyName1, String_t* ___placementId2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SponsorPay.SponsorPayPlugin::RequestNewCoins()
extern "C"  void SponsorPayPlugin_RequestNewCoins_m3500965849 (SponsorPayPlugin_t4106591195 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SponsorPay.SponsorPayPlugin::RequestNewCoins(System.String)
extern "C"  void SponsorPayPlugin_RequestNewCoins_m4181419977 (SponsorPayPlugin_t4106591195 * __this, String_t* ___currencyName0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SponsorPay.SponsorPayPlugin::RequestNewCoins(System.String,System.String)
extern "C"  void SponsorPayPlugin_RequestNewCoins_m2608512069 (SponsorPayPlugin_t4106591195 * __this, String_t* ___credentialsToken0, String_t* ___currencyName1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SponsorPay.SponsorPayPlugin::RequestNewCoins(System.String,System.String,System.String)
extern "C"  void SponsorPayPlugin_RequestNewCoins_m1755219905 (SponsorPayPlugin_t4106591195 * __this, String_t* ___credentialsToken0, String_t* ___currencyName1, String_t* ___currencyId2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SponsorPay.SponsorPayPlugin::ShowVCSNotifications(System.Boolean)
extern "C"  void SponsorPayPlugin_ShowVCSNotifications_m3911843440 (SponsorPayPlugin_t4106591195 * __this, bool ___showNotification0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SponsorPay.SponsorPayPlugin::HandleException(System.String)
extern "C"  void SponsorPayPlugin_HandleException_m458838003 (SponsorPayPlugin_t4106591195 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SponsorPay.SponsorPayPlugin::RequestBrandEngageOffers(System.String,System.Boolean)
extern "C"  void SponsorPayPlugin_RequestBrandEngageOffers_m3723586469 (SponsorPayPlugin_t4106591195 * __this, String_t* ___currencyName0, bool ___queryVCS1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SponsorPay.SponsorPayPlugin::RequestBrandEngageOffers(System.String,System.String,System.Boolean)
extern "C"  void SponsorPayPlugin_RequestBrandEngageOffers_m3887193577 (SponsorPayPlugin_t4106591195 * __this, String_t* ___credentialsToken0, String_t* ___currencyName1, bool ___queryVCS2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SponsorPay.SponsorPayPlugin::RequestBrandEngageOffers(System.String,System.String,System.Boolean,System.String,System.String)
extern "C"  void SponsorPayPlugin_RequestBrandEngageOffers_m2808384993 (SponsorPayPlugin_t4106591195 * __this, String_t* ___credentialsToken0, String_t* ___currencyName1, bool ___queryVCS2, String_t* ___currencyId3, String_t* ___placementId4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SponsorPay.SponsorPayPlugin::StartBrandEngage()
extern "C"  void SponsorPayPlugin_StartBrandEngage_m1534146368 (SponsorPayPlugin_t4106591195 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SponsorPay.SponsorPayPlugin::ShowBrandEngageRewardNotification(System.Boolean)
extern "C"  void SponsorPayPlugin_ShowBrandEngageRewardNotification_m3152979844 (SponsorPayPlugin_t4106591195 * __this, bool ___showNotification0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SponsorPay.SponsorPayPlugin::RequestInterstitialAds()
extern "C"  void SponsorPayPlugin_RequestInterstitialAds_m881204239 (SponsorPayPlugin_t4106591195 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SponsorPay.SponsorPayPlugin::RequestInterstitialAds(System.String)
extern "C"  void SponsorPayPlugin_RequestInterstitialAds_m698864467 (SponsorPayPlugin_t4106591195 * __this, String_t* ___credentialsToken0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SponsorPay.SponsorPayPlugin::RequestInterstitialAds(System.String,System.String)
extern "C"  void SponsorPayPlugin_RequestInterstitialAds_m3298242127 (SponsorPayPlugin_t4106591195 * __this, String_t* ___credentialsToken0, String_t* ___placementId1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SponsorPay.SponsorPayPlugin::ShowInterstitialAd()
extern "C"  void SponsorPayPlugin_ShowInterstitialAd_m2943536198 (SponsorPayPlugin_t4106591195 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SponsorPay.SponsorPayPlugin::VideoDownloadPause(System.Boolean)
extern "C"  void SponsorPayPlugin_VideoDownloadPause_m544973316 (SponsorPayPlugin_t4106591195 * __this, bool ___pause0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SponsorPay.SponsorPayPlugin::EnableLogging(System.Boolean)
extern "C"  void SponsorPayPlugin_EnableLogging_m884313019 (SponsorPayPlugin_t4106591195 * __this, bool ___shouldEnableLogging0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SponsorPay.SponsorPayPlugin::SetLogLevel(SponsorPay.SPLogLevel)
extern "C"  void SponsorPayPlugin_SetLogLevel_m2069738891 (SponsorPayPlugin_t4106591195 * __this, int32_t ___logLevel0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SponsorPay.SponsorPayPlugin::AddParameters(System.Collections.Generic.Dictionary`2<System.String,System.String>)
extern "C"  void SponsorPayPlugin_AddParameters_m3497421892 (SponsorPayPlugin_t4106591195 * __this, Dictionary_2_t2606186806 * ___parameters0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SponsorPay.SponsorPayPlugin::RemoveAllParameters()
extern "C"  void SponsorPayPlugin_RemoveAllParameters_m2827804975 (SponsorPayPlugin_t4106591195 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// SponsorPay.SPUser SponsorPay.SponsorPayPlugin::get_SPUser()
extern "C"  SPUser_t2606281602 * SponsorPayPlugin_get_SPUser_m1470753376 (SponsorPayPlugin_t4106591195 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SponsorPay.SponsorPayPlugin::HandleOfferWallResultFromSDK(System.String)
extern "C"  void SponsorPayPlugin_HandleOfferWallResultFromSDK_m262206579 (SponsorPayPlugin_t4106591195 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SponsorPay.SponsorPayPlugin::HandleCurrencyDeltaOfCoinsMessageFromSDK(System.String)
extern "C"  void SponsorPayPlugin_HandleCurrencyDeltaOfCoinsMessageFromSDK_m3311995667 (SponsorPayPlugin_t4106591195 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SponsorPay.SponsorPayPlugin::HandleBrandEngageStatusMessageFromSDK(System.String)
extern "C"  void SponsorPayPlugin_HandleBrandEngageStatusMessageFromSDK_m4133676639 (SponsorPayPlugin_t4106591195 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SponsorPay.SponsorPayPlugin::HandleBrandEngageResultFromSDK(System.String)
extern "C"  void SponsorPayPlugin_HandleBrandEngageResultFromSDK_m3449368885 (SponsorPayPlugin_t4106591195 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SponsorPay.SponsorPayPlugin::HandleInterstitialStatusMessageFromSDK(System.String)
extern "C"  void SponsorPayPlugin_HandleInterstitialStatusMessageFromSDK_m2272424729 (SponsorPayPlugin_t4106591195 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SponsorPay.SponsorPayPlugin::HandleInterstitialResultFromSDK(System.String)
extern "C"  void SponsorPayPlugin_HandleInterstitialResultFromSDK_m161030587 (SponsorPayPlugin_t4106591195 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SponsorPay.SponsorPayPlugin::CheckRequiredParameters(System.String[],System.String[])
extern "C"  void SponsorPayPlugin_CheckRequiredParameters_m1978000865 (SponsorPayPlugin_t4106591195 * __this, StringU5BU5D_t2956870243* ___values0, StringU5BU5D_t2956870243* ___names1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
