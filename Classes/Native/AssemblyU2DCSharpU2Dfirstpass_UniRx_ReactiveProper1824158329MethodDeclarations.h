﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.ReactiveProperty`1/ReactivePropertyObserver<UnityEngine.Color>
struct ReactivePropertyObserver_t1824158329;
// UniRx.ReactiveProperty`1<UnityEngine.Color>
struct ReactiveProperty_1_t2196508798;
// System.Exception
struct Exception_t1967233988;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Color1588175760.h"
#include "mscorlib_System_Exception1967233988.h"

// System.Void UniRx.ReactiveProperty`1/ReactivePropertyObserver<UnityEngine.Color>::.ctor(UniRx.ReactiveProperty`1<T>)
extern "C"  void ReactivePropertyObserver__ctor_m910925542_gshared (ReactivePropertyObserver_t1824158329 * __this, ReactiveProperty_1_t2196508798 * ___parent0, const MethodInfo* method);
#define ReactivePropertyObserver__ctor_m910925542(__this, ___parent0, method) ((  void (*) (ReactivePropertyObserver_t1824158329 *, ReactiveProperty_1_t2196508798 *, const MethodInfo*))ReactivePropertyObserver__ctor_m910925542_gshared)(__this, ___parent0, method)
// System.Void UniRx.ReactiveProperty`1/ReactivePropertyObserver<UnityEngine.Color>::OnNext(T)
extern "C"  void ReactivePropertyObserver_OnNext_m3654968325_gshared (ReactivePropertyObserver_t1824158329 * __this, Color_t1588175760  ___value0, const MethodInfo* method);
#define ReactivePropertyObserver_OnNext_m3654968325(__this, ___value0, method) ((  void (*) (ReactivePropertyObserver_t1824158329 *, Color_t1588175760 , const MethodInfo*))ReactivePropertyObserver_OnNext_m3654968325_gshared)(__this, ___value0, method)
// System.Void UniRx.ReactiveProperty`1/ReactivePropertyObserver<UnityEngine.Color>::OnError(System.Exception)
extern "C"  void ReactivePropertyObserver_OnError_m3207058708_gshared (ReactivePropertyObserver_t1824158329 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method);
#define ReactivePropertyObserver_OnError_m3207058708(__this, ___error0, method) ((  void (*) (ReactivePropertyObserver_t1824158329 *, Exception_t1967233988 *, const MethodInfo*))ReactivePropertyObserver_OnError_m3207058708_gshared)(__this, ___error0, method)
// System.Void UniRx.ReactiveProperty`1/ReactivePropertyObserver<UnityEngine.Color>::OnCompleted()
extern "C"  void ReactivePropertyObserver_OnCompleted_m861832807_gshared (ReactivePropertyObserver_t1824158329 * __this, const MethodInfo* method);
#define ReactivePropertyObserver_OnCompleted_m861832807(__this, method) ((  void (*) (ReactivePropertyObserver_t1824158329 *, const MethodInfo*))ReactivePropertyObserver_OnCompleted_m861832807_gshared)(__this, method)
