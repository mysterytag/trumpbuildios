﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Tuple_2_gen369261819MethodDeclarations.h"

// System.Void UniRx.Tuple`2<UniRx.BooleanDisposable,System.Action>::.ctor(T1,T2)
#define Tuple_2__ctor_m2752048309(__this, ___item10, ___item21, method) ((  void (*) (Tuple_2_t1974156884 *, BooleanDisposable_t3065601722 *, Action_t437523947 *, const MethodInfo*))Tuple_2__ctor_m101027774_gshared)(__this, ___item10, ___item21, method)
// System.Int32 UniRx.Tuple`2<UniRx.BooleanDisposable,System.Action>::System.IComparable.CompareTo(System.Object)
#define Tuple_2_System_IComparable_CompareTo_m2108824052(__this, ___obj0, method) ((  int32_t (*) (Tuple_2_t1974156884 *, Il2CppObject *, const MethodInfo*))Tuple_2_System_IComparable_CompareTo_m964946507_gshared)(__this, ___obj0, method)
// System.Int32 UniRx.Tuple`2<UniRx.BooleanDisposable,System.Action>::UniRx.IStructuralComparable.CompareTo(System.Object,System.Collections.IComparer)
#define Tuple_2_UniRx_IStructuralComparable_CompareTo_m4035312582(__this, ___other0, ___comparer1, method) ((  int32_t (*) (Tuple_2_t1974156884 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))Tuple_2_UniRx_IStructuralComparable_CompareTo_m1607455453_gshared)(__this, ___other0, ___comparer1, method)
// System.Boolean UniRx.Tuple`2<UniRx.BooleanDisposable,System.Action>::UniRx.IStructuralEquatable.Equals(System.Object,System.Collections.IEqualityComparer)
#define Tuple_2_UniRx_IStructuralEquatable_Equals_m1482794417(__this, ___other0, ___comparer1, method) ((  bool (*) (Tuple_2_t1974156884 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))Tuple_2_UniRx_IStructuralEquatable_Equals_m2678862280_gshared)(__this, ___other0, ___comparer1, method)
// System.Int32 UniRx.Tuple`2<UniRx.BooleanDisposable,System.Action>::UniRx.IStructuralEquatable.GetHashCode(System.Collections.IEqualityComparer)
#define Tuple_2_UniRx_IStructuralEquatable_GetHashCode_m1215129565(__this, ___comparer0, method) ((  int32_t (*) (Tuple_2_t1974156884 *, Il2CppObject *, const MethodInfo*))Tuple_2_UniRx_IStructuralEquatable_GetHashCode_m2682338918_gshared)(__this, ___comparer0, method)
// System.String UniRx.Tuple`2<UniRx.BooleanDisposable,System.Action>::UniRx.ITuple.ToString()
#define Tuple_2_UniRx_ITuple_ToString_m3676354990(__this, method) ((  String_t* (*) (Tuple_2_t1974156884 *, const MethodInfo*))Tuple_2_UniRx_ITuple_ToString_m2079956805_gshared)(__this, method)
// T1 UniRx.Tuple`2<UniRx.BooleanDisposable,System.Action>::get_Item1()
#define Tuple_2_get_Item1_m1772784283(__this, method) ((  BooleanDisposable_t3065601722 * (*) (Tuple_2_t1974156884 *, const MethodInfo*))Tuple_2_get_Item1_m425160818_gshared)(__this, method)
// T2 UniRx.Tuple`2<UniRx.BooleanDisposable,System.Action>::get_Item2()
#define Tuple_2_get_Item2_m1164708381(__this, method) ((  Action_t437523947 * (*) (Tuple_2_t1974156884 *, const MethodInfo*))Tuple_2_get_Item2_m1055620404_gshared)(__this, method)
// System.Boolean UniRx.Tuple`2<UniRx.BooleanDisposable,System.Action>::Equals(System.Object)
#define Tuple_2_Equals_m824621157(__this, ___obj0, method) ((  bool (*) (Tuple_2_t1974156884 *, Il2CppObject *, const MethodInfo*))Tuple_2_Equals_m2489154684_gshared)(__this, ___obj0, method)
// System.Int32 UniRx.Tuple`2<UniRx.BooleanDisposable,System.Action>::GetHashCode()
#define Tuple_2_GetHashCode_m793952393(__this, method) ((  int32_t (*) (Tuple_2_t1974156884 *, const MethodInfo*))Tuple_2_GetHashCode_m1023013664_gshared)(__this, method)
// System.String UniRx.Tuple`2<UniRx.BooleanDisposable,System.Action>::ToString()
#define Tuple_2_ToString_m3982622403(__this, method) ((  String_t* (*) (Tuple_2_t1974156884 *, const MethodInfo*))Tuple_2_ToString_m3182481356_gshared)(__this, method)
// System.Boolean UniRx.Tuple`2<UniRx.BooleanDisposable,System.Action>::Equals(UniRx.Tuple`2<T1,T2>)
#define Tuple_2_Equals_m4028192740(__this, ___other0, method) ((  bool (*) (Tuple_2_t1974156884 *, Tuple_2_t1974156884 , const MethodInfo*))Tuple_2_Equals_m1605966829_gshared)(__this, ___other0, method)
