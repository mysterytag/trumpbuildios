﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UniRx.IObservable`1<System.Object>
struct IObservable_1_t595904784;
// UniRx.ObservableYieldInstruction`1<System.Object>
struct ObservableYieldInstruction_1_t2959019304;
// System.Collections.Generic.IEnumerator`1<System.Object>
struct IEnumerator_1_t2320212868;
// System.Action`1<System.Object>
struct Action_1_t985559125;
// System.Action`1<System.Exception>
struct Action_1_t2115686693;
// System.Object
struct Il2CppObject;

#include "mscorlib_System_Object837106420.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_CancellationTo1439151560.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Observable/<ToAwaitableEnumerator>c__Iterator17`1<System.Object>
struct  U3CToAwaitableEnumeratorU3Ec__Iterator17_1_t1120858664  : public Il2CppObject
{
public:
	// UniRx.IObservable`1<T> UniRx.Observable/<ToAwaitableEnumerator>c__Iterator17`1::source
	Il2CppObject* ___source_0;
	// UniRx.ObservableYieldInstruction`1<T> UniRx.Observable/<ToAwaitableEnumerator>c__Iterator17`1::<enumerator>__0
	ObservableYieldInstruction_1_t2959019304 * ___U3CenumeratorU3E__0_1;
	// System.Collections.Generic.IEnumerator`1<T> UniRx.Observable/<ToAwaitableEnumerator>c__Iterator17`1::<e>__1
	Il2CppObject* ___U3CeU3E__1_2;
	// UniRx.CancellationToken UniRx.Observable/<ToAwaitableEnumerator>c__Iterator17`1::cancel
	CancellationToken_t1439151560  ___cancel_3;
	// System.Action`1<T> UniRx.Observable/<ToAwaitableEnumerator>c__Iterator17`1::onResult
	Action_1_t985559125 * ___onResult_4;
	// System.Action`1<System.Exception> UniRx.Observable/<ToAwaitableEnumerator>c__Iterator17`1::onError
	Action_1_t2115686693 * ___onError_5;
	// System.Int32 UniRx.Observable/<ToAwaitableEnumerator>c__Iterator17`1::$PC
	int32_t ___U24PC_6;
	// System.Object UniRx.Observable/<ToAwaitableEnumerator>c__Iterator17`1::$current
	Il2CppObject * ___U24current_7;
	// UniRx.IObservable`1<T> UniRx.Observable/<ToAwaitableEnumerator>c__Iterator17`1::<$>source
	Il2CppObject* ___U3CU24U3Esource_8;
	// UniRx.CancellationToken UniRx.Observable/<ToAwaitableEnumerator>c__Iterator17`1::<$>cancel
	CancellationToken_t1439151560  ___U3CU24U3Ecancel_9;
	// System.Action`1<T> UniRx.Observable/<ToAwaitableEnumerator>c__Iterator17`1::<$>onResult
	Action_1_t985559125 * ___U3CU24U3EonResult_10;
	// System.Action`1<System.Exception> UniRx.Observable/<ToAwaitableEnumerator>c__Iterator17`1::<$>onError
	Action_1_t2115686693 * ___U3CU24U3EonError_11;

public:
	inline static int32_t get_offset_of_source_0() { return static_cast<int32_t>(offsetof(U3CToAwaitableEnumeratorU3Ec__Iterator17_1_t1120858664, ___source_0)); }
	inline Il2CppObject* get_source_0() const { return ___source_0; }
	inline Il2CppObject** get_address_of_source_0() { return &___source_0; }
	inline void set_source_0(Il2CppObject* value)
	{
		___source_0 = value;
		Il2CppCodeGenWriteBarrier(&___source_0, value);
	}

	inline static int32_t get_offset_of_U3CenumeratorU3E__0_1() { return static_cast<int32_t>(offsetof(U3CToAwaitableEnumeratorU3Ec__Iterator17_1_t1120858664, ___U3CenumeratorU3E__0_1)); }
	inline ObservableYieldInstruction_1_t2959019304 * get_U3CenumeratorU3E__0_1() const { return ___U3CenumeratorU3E__0_1; }
	inline ObservableYieldInstruction_1_t2959019304 ** get_address_of_U3CenumeratorU3E__0_1() { return &___U3CenumeratorU3E__0_1; }
	inline void set_U3CenumeratorU3E__0_1(ObservableYieldInstruction_1_t2959019304 * value)
	{
		___U3CenumeratorU3E__0_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CenumeratorU3E__0_1, value);
	}

	inline static int32_t get_offset_of_U3CeU3E__1_2() { return static_cast<int32_t>(offsetof(U3CToAwaitableEnumeratorU3Ec__Iterator17_1_t1120858664, ___U3CeU3E__1_2)); }
	inline Il2CppObject* get_U3CeU3E__1_2() const { return ___U3CeU3E__1_2; }
	inline Il2CppObject** get_address_of_U3CeU3E__1_2() { return &___U3CeU3E__1_2; }
	inline void set_U3CeU3E__1_2(Il2CppObject* value)
	{
		___U3CeU3E__1_2 = value;
		Il2CppCodeGenWriteBarrier(&___U3CeU3E__1_2, value);
	}

	inline static int32_t get_offset_of_cancel_3() { return static_cast<int32_t>(offsetof(U3CToAwaitableEnumeratorU3Ec__Iterator17_1_t1120858664, ___cancel_3)); }
	inline CancellationToken_t1439151560  get_cancel_3() const { return ___cancel_3; }
	inline CancellationToken_t1439151560 * get_address_of_cancel_3() { return &___cancel_3; }
	inline void set_cancel_3(CancellationToken_t1439151560  value)
	{
		___cancel_3 = value;
	}

	inline static int32_t get_offset_of_onResult_4() { return static_cast<int32_t>(offsetof(U3CToAwaitableEnumeratorU3Ec__Iterator17_1_t1120858664, ___onResult_4)); }
	inline Action_1_t985559125 * get_onResult_4() const { return ___onResult_4; }
	inline Action_1_t985559125 ** get_address_of_onResult_4() { return &___onResult_4; }
	inline void set_onResult_4(Action_1_t985559125 * value)
	{
		___onResult_4 = value;
		Il2CppCodeGenWriteBarrier(&___onResult_4, value);
	}

	inline static int32_t get_offset_of_onError_5() { return static_cast<int32_t>(offsetof(U3CToAwaitableEnumeratorU3Ec__Iterator17_1_t1120858664, ___onError_5)); }
	inline Action_1_t2115686693 * get_onError_5() const { return ___onError_5; }
	inline Action_1_t2115686693 ** get_address_of_onError_5() { return &___onError_5; }
	inline void set_onError_5(Action_1_t2115686693 * value)
	{
		___onError_5 = value;
		Il2CppCodeGenWriteBarrier(&___onError_5, value);
	}

	inline static int32_t get_offset_of_U24PC_6() { return static_cast<int32_t>(offsetof(U3CToAwaitableEnumeratorU3Ec__Iterator17_1_t1120858664, ___U24PC_6)); }
	inline int32_t get_U24PC_6() const { return ___U24PC_6; }
	inline int32_t* get_address_of_U24PC_6() { return &___U24PC_6; }
	inline void set_U24PC_6(int32_t value)
	{
		___U24PC_6 = value;
	}

	inline static int32_t get_offset_of_U24current_7() { return static_cast<int32_t>(offsetof(U3CToAwaitableEnumeratorU3Ec__Iterator17_1_t1120858664, ___U24current_7)); }
	inline Il2CppObject * get_U24current_7() const { return ___U24current_7; }
	inline Il2CppObject ** get_address_of_U24current_7() { return &___U24current_7; }
	inline void set_U24current_7(Il2CppObject * value)
	{
		___U24current_7 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_7, value);
	}

	inline static int32_t get_offset_of_U3CU24U3Esource_8() { return static_cast<int32_t>(offsetof(U3CToAwaitableEnumeratorU3Ec__Iterator17_1_t1120858664, ___U3CU24U3Esource_8)); }
	inline Il2CppObject* get_U3CU24U3Esource_8() const { return ___U3CU24U3Esource_8; }
	inline Il2CppObject** get_address_of_U3CU24U3Esource_8() { return &___U3CU24U3Esource_8; }
	inline void set_U3CU24U3Esource_8(Il2CppObject* value)
	{
		___U3CU24U3Esource_8 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU24U3Esource_8, value);
	}

	inline static int32_t get_offset_of_U3CU24U3Ecancel_9() { return static_cast<int32_t>(offsetof(U3CToAwaitableEnumeratorU3Ec__Iterator17_1_t1120858664, ___U3CU24U3Ecancel_9)); }
	inline CancellationToken_t1439151560  get_U3CU24U3Ecancel_9() const { return ___U3CU24U3Ecancel_9; }
	inline CancellationToken_t1439151560 * get_address_of_U3CU24U3Ecancel_9() { return &___U3CU24U3Ecancel_9; }
	inline void set_U3CU24U3Ecancel_9(CancellationToken_t1439151560  value)
	{
		___U3CU24U3Ecancel_9 = value;
	}

	inline static int32_t get_offset_of_U3CU24U3EonResult_10() { return static_cast<int32_t>(offsetof(U3CToAwaitableEnumeratorU3Ec__Iterator17_1_t1120858664, ___U3CU24U3EonResult_10)); }
	inline Action_1_t985559125 * get_U3CU24U3EonResult_10() const { return ___U3CU24U3EonResult_10; }
	inline Action_1_t985559125 ** get_address_of_U3CU24U3EonResult_10() { return &___U3CU24U3EonResult_10; }
	inline void set_U3CU24U3EonResult_10(Action_1_t985559125 * value)
	{
		___U3CU24U3EonResult_10 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU24U3EonResult_10, value);
	}

	inline static int32_t get_offset_of_U3CU24U3EonError_11() { return static_cast<int32_t>(offsetof(U3CToAwaitableEnumeratorU3Ec__Iterator17_1_t1120858664, ___U3CU24U3EonError_11)); }
	inline Action_1_t2115686693 * get_U3CU24U3EonError_11() const { return ___U3CU24U3EonError_11; }
	inline Action_1_t2115686693 ** get_address_of_U3CU24U3EonError_11() { return &___U3CU24U3EonError_11; }
	inline void set_U3CU24U3EonError_11(Action_1_t2115686693 * value)
	{
		___U3CU24U3EonError_11 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU24U3EonError_11, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
