﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.ClientModels.GetUserDataResult
struct GetUserDataResult_t518036344;
// System.Collections.Generic.Dictionary`2<System.String,PlayFab.ClientModels.UserDataRecord>
struct Dictionary_2_t1801537638;

#include "codegen/il2cpp-codegen.h"

// System.Void PlayFab.ClientModels.GetUserDataResult::.ctor()
extern "C"  void GetUserDataResult__ctor_m2836501937 (GetUserDataResult_t518036344 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.Dictionary`2<System.String,PlayFab.ClientModels.UserDataRecord> PlayFab.ClientModels.GetUserDataResult::get_Data()
extern "C"  Dictionary_2_t1801537638 * GetUserDataResult_get_Data_m671783624 (GetUserDataResult_t518036344 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.GetUserDataResult::set_Data(System.Collections.Generic.Dictionary`2<System.String,PlayFab.ClientModels.UserDataRecord>)
extern "C"  void GetUserDataResult_set_Data_m2033452031 (GetUserDataResult_t518036344 * __this, Dictionary_2_t1801537638 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 PlayFab.ClientModels.GetUserDataResult::get_DataVersion()
extern "C"  uint32_t GetUserDataResult_get_DataVersion_m3199741625 (GetUserDataResult_t518036344 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.GetUserDataResult::set_DataVersion(System.UInt32)
extern "C"  void GetUserDataResult_set_DataVersion_m915033722 (GetUserDataResult_t518036344 * __this, uint32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
