﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.IDisposable
struct IDisposable_t1628921374;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WaitForStreamEvent
struct  WaitForStreamEvent_t4156505958  : public Il2CppObject
{
public:
	// System.IDisposable WaitForStreamEvent::connection
	Il2CppObject * ___connection_0;
	// System.Boolean WaitForStreamEvent::finished_
	bool ___finished__1;

public:
	inline static int32_t get_offset_of_connection_0() { return static_cast<int32_t>(offsetof(WaitForStreamEvent_t4156505958, ___connection_0)); }
	inline Il2CppObject * get_connection_0() const { return ___connection_0; }
	inline Il2CppObject ** get_address_of_connection_0() { return &___connection_0; }
	inline void set_connection_0(Il2CppObject * value)
	{
		___connection_0 = value;
		Il2CppCodeGenWriteBarrier(&___connection_0, value);
	}

	inline static int32_t get_offset_of_finished__1() { return static_cast<int32_t>(offsetof(WaitForStreamEvent_t4156505958, ___finished__1)); }
	inline bool get_finished__1() const { return ___finished__1; }
	inline bool* get_address_of_finished__1() { return &___finished__1; }
	inline void set_finished__1(bool value)
	{
		___finished__1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
