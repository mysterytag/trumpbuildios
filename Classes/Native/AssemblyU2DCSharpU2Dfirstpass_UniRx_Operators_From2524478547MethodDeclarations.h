﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.FromCoroutine`1<System.Int64>
struct FromCoroutine_1_t2524478547;
// System.Func`3<UniRx.IObserver`1<System.Int64>,UniRx.CancellationToken,System.Collections.IEnumerator>
struct Func_3_t521084177;
// System.IDisposable
struct IDisposable_t1628921374;
// UniRx.IObserver`1<System.Int64>
struct IObserver_1_t764446489;

#include "codegen/il2cpp-codegen.h"

// System.Void UniRx.Operators.FromCoroutine`1<System.Int64>::.ctor(System.Func`3<UniRx.IObserver`1<T>,UniRx.CancellationToken,System.Collections.IEnumerator>)
extern "C"  void FromCoroutine_1__ctor_m2654486232_gshared (FromCoroutine_1_t2524478547 * __this, Func_3_t521084177 * ___coroutine0, const MethodInfo* method);
#define FromCoroutine_1__ctor_m2654486232(__this, ___coroutine0, method) ((  void (*) (FromCoroutine_1_t2524478547 *, Func_3_t521084177 *, const MethodInfo*))FromCoroutine_1__ctor_m2654486232_gshared)(__this, ___coroutine0, method)
// System.IDisposable UniRx.Operators.FromCoroutine`1<System.Int64>::SubscribeCore(UniRx.IObserver`1<T>,System.IDisposable)
extern "C"  Il2CppObject * FromCoroutine_1_SubscribeCore_m2169225549_gshared (FromCoroutine_1_t2524478547 * __this, Il2CppObject* ___observer0, Il2CppObject * ___cancel1, const MethodInfo* method);
#define FromCoroutine_1_SubscribeCore_m2169225549(__this, ___observer0, ___cancel1, method) ((  Il2CppObject * (*) (FromCoroutine_1_t2524478547 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))FromCoroutine_1_SubscribeCore_m2169225549_gshared)(__this, ___observer0, ___cancel1, method)
