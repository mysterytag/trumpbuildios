﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Func`3<UniRx.Unit,System.Int32,System.Boolean>
struct Func_3_t255060061;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t537683269;
// System.AsyncCallback
struct AsyncCallback_t1363551830;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_IntPtr676692020.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Unit2558286038.h"
#include "mscorlib_System_AsyncCallback1363551830.h"

// System.Void System.Func`3<UniRx.Unit,System.Int32,System.Boolean>::.ctor(System.Object,System.IntPtr)
extern "C"  void Func_3__ctor_m3469161193_gshared (Func_3_t255060061 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method);
#define Func_3__ctor_m3469161193(__this, ___object0, ___method1, method) ((  void (*) (Func_3_t255060061 *, Il2CppObject *, IntPtr_t, const MethodInfo*))Func_3__ctor_m3469161193_gshared)(__this, ___object0, ___method1, method)
// TResult System.Func`3<UniRx.Unit,System.Int32,System.Boolean>::Invoke(T1,T2)
extern "C"  bool Func_3_Invoke_m2505118276_gshared (Func_3_t255060061 * __this, Unit_t2558286038  ___arg10, int32_t ___arg21, const MethodInfo* method);
#define Func_3_Invoke_m2505118276(__this, ___arg10, ___arg21, method) ((  bool (*) (Func_3_t255060061 *, Unit_t2558286038 , int32_t, const MethodInfo*))Func_3_Invoke_m2505118276_gshared)(__this, ___arg10, ___arg21, method)
// System.IAsyncResult System.Func`3<UniRx.Unit,System.Int32,System.Boolean>::BeginInvoke(T1,T2,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * Func_3_BeginInvoke_m561687113_gshared (Func_3_t255060061 * __this, Unit_t2558286038  ___arg10, int32_t ___arg21, AsyncCallback_t1363551830 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method);
#define Func_3_BeginInvoke_m561687113(__this, ___arg10, ___arg21, ___callback2, ___object3, method) ((  Il2CppObject * (*) (Func_3_t255060061 *, Unit_t2558286038 , int32_t, AsyncCallback_t1363551830 *, Il2CppObject *, const MethodInfo*))Func_3_BeginInvoke_m561687113_gshared)(__this, ___arg10, ___arg21, ___callback2, ___object3, method)
// TResult System.Func`3<UniRx.Unit,System.Int32,System.Boolean>::EndInvoke(System.IAsyncResult)
extern "C"  bool Func_3_EndInvoke_m1209193751_gshared (Func_3_t255060061 * __this, Il2CppObject * ___result0, const MethodInfo* method);
#define Func_3_EndInvoke_m1209193751(__this, ___result0, method) ((  bool (*) (Func_3_t255060061 *, Il2CppObject *, const MethodInfo*))Func_3_EndInvoke_m1209193751_gshared)(__this, ___result0, method)
