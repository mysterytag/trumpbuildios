﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,LitJson.PropertyMetadata>
struct ValueCollection_t261995321;
// System.Collections.Generic.Dictionary`2<System.Object,LitJson.PropertyMetadata>
struct Dictionary_2_t2634825523;
// System.Collections.Generic.IEnumerator`1<LitJson.PropertyMetadata>
struct IEnumerator_1_t1130613241;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t287207039;
// System.Object
struct Il2CppObject;
// LitJson.PropertyMetadata[]
struct PropertyMetadataU5BU5D_t4210083540;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_LitJson_PropertyMetadata3942474089.h"
#include "mscorlib_System_Array2840145358.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_V2401853464.h"

// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,LitJson.PropertyMetadata>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void ValueCollection__ctor_m2578243927_gshared (ValueCollection_t261995321 * __this, Dictionary_2_t2634825523 * ___dictionary0, const MethodInfo* method);
#define ValueCollection__ctor_m2578243927(__this, ___dictionary0, method) ((  void (*) (ValueCollection_t261995321 *, Dictionary_2_t2634825523 *, const MethodInfo*))ValueCollection__ctor_m2578243927_gshared)(__this, ___dictionary0, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,LitJson.PropertyMetadata>::System.Collections.Generic.ICollection<TValue>.Add(TValue)
extern "C"  void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m2585023483_gshared (ValueCollection_t261995321 * __this, PropertyMetadata_t3942474089  ___item0, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m2585023483(__this, ___item0, method) ((  void (*) (ValueCollection_t261995321 *, PropertyMetadata_t3942474089 , const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m2585023483_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,LitJson.PropertyMetadata>::System.Collections.Generic.ICollection<TValue>.Clear()
extern "C"  void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m984708676_gshared (ValueCollection_t261995321 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m984708676(__this, method) ((  void (*) (ValueCollection_t261995321 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m984708676_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,LitJson.PropertyMetadata>::System.Collections.Generic.ICollection<TValue>.Contains(TValue)
extern "C"  bool ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m1084661199_gshared (ValueCollection_t261995321 * __this, PropertyMetadata_t3942474089  ___item0, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m1084661199(__this, ___item0, method) ((  bool (*) (ValueCollection_t261995321 *, PropertyMetadata_t3942474089 , const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m1084661199_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,LitJson.PropertyMetadata>::System.Collections.Generic.ICollection<TValue>.Remove(TValue)
extern "C"  bool ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m1155194420_gshared (ValueCollection_t261995321 * __this, PropertyMetadata_t3942474089  ___item0, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m1155194420(__this, ___item0, method) ((  bool (*) (ValueCollection_t261995321 *, PropertyMetadata_t3942474089 , const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m1155194420_gshared)(__this, ___item0, method)
// System.Collections.Generic.IEnumerator`1<TValue> System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,LitJson.PropertyMetadata>::System.Collections.Generic.IEnumerable<TValue>.GetEnumerator()
extern "C"  Il2CppObject* ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m1186872196_gshared (ValueCollection_t261995321 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m1186872196(__this, method) ((  Il2CppObject* (*) (ValueCollection_t261995321 *, const MethodInfo*))ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m1186872196_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,LitJson.PropertyMetadata>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void ValueCollection_System_Collections_ICollection_CopyTo_m2496401992_gshared (ValueCollection_t261995321 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define ValueCollection_System_Collections_ICollection_CopyTo_m2496401992(__this, ___array0, ___index1, method) ((  void (*) (ValueCollection_t261995321 *, Il2CppArray *, int32_t, const MethodInfo*))ValueCollection_System_Collections_ICollection_CopyTo_m2496401992_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,LitJson.PropertyMetadata>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * ValueCollection_System_Collections_IEnumerable_GetEnumerator_m1246237015_gshared (ValueCollection_t261995321 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_IEnumerable_GetEnumerator_m1246237015(__this, method) ((  Il2CppObject * (*) (ValueCollection_t261995321 *, const MethodInfo*))ValueCollection_System_Collections_IEnumerable_GetEnumerator_m1246237015_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,LitJson.PropertyMetadata>::System.Collections.Generic.ICollection<TValue>.get_IsReadOnly()
extern "C"  bool ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m3654804354_gshared (ValueCollection_t261995321 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m3654804354(__this, method) ((  bool (*) (ValueCollection_t261995321 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m3654804354_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,LitJson.PropertyMetadata>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool ValueCollection_System_Collections_ICollection_get_IsSynchronized_m2223365218_gshared (ValueCollection_t261995321 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_ICollection_get_IsSynchronized_m2223365218(__this, method) ((  bool (*) (ValueCollection_t261995321 *, const MethodInfo*))ValueCollection_System_Collections_ICollection_get_IsSynchronized_m2223365218_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,LitJson.PropertyMetadata>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * ValueCollection_System_Collections_ICollection_get_SyncRoot_m2057872340_gshared (ValueCollection_t261995321 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_ICollection_get_SyncRoot_m2057872340(__this, method) ((  Il2CppObject * (*) (ValueCollection_t261995321 *, const MethodInfo*))ValueCollection_System_Collections_ICollection_get_SyncRoot_m2057872340_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,LitJson.PropertyMetadata>::CopyTo(TValue[],System.Int32)
extern "C"  void ValueCollection_CopyTo_m1799669726_gshared (ValueCollection_t261995321 * __this, PropertyMetadataU5BU5D_t4210083540* ___array0, int32_t ___index1, const MethodInfo* method);
#define ValueCollection_CopyTo_m1799669726(__this, ___array0, ___index1, method) ((  void (*) (ValueCollection_t261995321 *, PropertyMetadataU5BU5D_t4210083540*, int32_t, const MethodInfo*))ValueCollection_CopyTo_m1799669726_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,LitJson.PropertyMetadata>::GetEnumerator()
extern "C"  Enumerator_t2401853466  ValueCollection_GetEnumerator_m393801799_gshared (ValueCollection_t261995321 * __this, const MethodInfo* method);
#define ValueCollection_GetEnumerator_m393801799(__this, method) ((  Enumerator_t2401853466  (*) (ValueCollection_t261995321 *, const MethodInfo*))ValueCollection_GetEnumerator_m393801799_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,LitJson.PropertyMetadata>::get_Count()
extern "C"  int32_t ValueCollection_get_Count_m1467558556_gshared (ValueCollection_t261995321 * __this, const MethodInfo* method);
#define ValueCollection_get_Count_m1467558556(__this, method) ((  int32_t (*) (ValueCollection_t261995321 *, const MethodInfo*))ValueCollection_get_Count_m1467558556_gshared)(__this, method)
