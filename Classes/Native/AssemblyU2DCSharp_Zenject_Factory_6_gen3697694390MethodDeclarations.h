﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Zenject.Factory`6<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>
struct Factory_6_t3697694390;
// System.Type
struct Type_t;
// System.Type[]
struct TypeU5BU5D_t3431720054;
// Zenject.DiContainer
struct DiContainer_t2383114449;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"

// System.Void Zenject.Factory`6<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>::.ctor()
extern "C"  void Factory_6__ctor_m2403853050_gshared (Factory_6_t3697694390 * __this, const MethodInfo* method);
#define Factory_6__ctor_m2403853050(__this, method) ((  void (*) (Factory_6_t3697694390 *, const MethodInfo*))Factory_6__ctor_m2403853050_gshared)(__this, method)
// System.Type Zenject.Factory`6<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>::get_ConstructedType()
extern "C"  Type_t * Factory_6_get_ConstructedType_m620899685_gshared (Factory_6_t3697694390 * __this, const MethodInfo* method);
#define Factory_6_get_ConstructedType_m620899685(__this, method) ((  Type_t * (*) (Factory_6_t3697694390 *, const MethodInfo*))Factory_6_get_ConstructedType_m620899685_gshared)(__this, method)
// System.Type[] Zenject.Factory`6<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>::get_ProvidedTypes()
extern "C"  TypeU5BU5D_t3431720054* Factory_6_get_ProvidedTypes_m3267653389_gshared (Factory_6_t3697694390 * __this, const MethodInfo* method);
#define Factory_6_get_ProvidedTypes_m3267653389(__this, method) ((  TypeU5BU5D_t3431720054* (*) (Factory_6_t3697694390 *, const MethodInfo*))Factory_6_get_ProvidedTypes_m3267653389_gshared)(__this, method)
// Zenject.DiContainer Zenject.Factory`6<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>::get_Container()
extern "C"  DiContainer_t2383114449 * Factory_6_get_Container_m2027703002_gshared (Factory_6_t3697694390 * __this, const MethodInfo* method);
#define Factory_6_get_Container_m2027703002(__this, method) ((  DiContainer_t2383114449 * (*) (Factory_6_t3697694390 *, const MethodInfo*))Factory_6_get_Container_m2027703002_gshared)(__this, method)
// TValue Zenject.Factory`6<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>::Create(TParam1,TParam2,TParam3,TParam4,TParam5)
extern "C"  Il2CppObject * Factory_6_Create_m1562312730_gshared (Factory_6_t3697694390 * __this, Il2CppObject * ___param10, Il2CppObject * ___param21, Il2CppObject * ___param32, Il2CppObject * ___param43, Il2CppObject * ___param54, const MethodInfo* method);
#define Factory_6_Create_m1562312730(__this, ___param10, ___param21, ___param32, ___param43, ___param54, method) ((  Il2CppObject * (*) (Factory_6_t3697694390 *, Il2CppObject *, Il2CppObject *, Il2CppObject *, Il2CppObject *, Il2CppObject *, const MethodInfo*))Factory_6_Create_m1562312730_gshared)(__this, ___param10, ___param21, ___param32, ___param43, ___param54, method)
