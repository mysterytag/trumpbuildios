﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Net.DownloadStringCompletedEventArgs
struct DownloadStringCompletedEventArgs_t4050086575;
// System.String
struct String_t;
// System.Exception
struct Exception_t1967233988;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String968488902.h"
#include "mscorlib_System_Exception1967233988.h"
#include "mscorlib_System_Object837106420.h"

// System.Void System.Net.DownloadStringCompletedEventArgs::.ctor(System.String,System.Exception,System.Boolean,System.Object)
extern "C"  void DownloadStringCompletedEventArgs__ctor_m1547950562 (DownloadStringCompletedEventArgs_t4050086575 * __this, String_t* ___result0, Exception_t1967233988 * ___error1, bool ___cancelled2, Il2CppObject * ___userState3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Net.DownloadStringCompletedEventArgs::get_Result()
extern "C"  String_t* DownloadStringCompletedEventArgs_get_Result_m347979826 (DownloadStringCompletedEventArgs_t4050086575 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
