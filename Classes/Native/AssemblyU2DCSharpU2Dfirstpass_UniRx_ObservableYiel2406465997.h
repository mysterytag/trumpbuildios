﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.IDisposable
struct IDisposable_t1628921374;
// UnityEngine.Transform
struct Transform_t284553113;
// System.Exception
struct Exception_t1967233988;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.ObservableYieldInstruction`1<UnityEngine.Transform>
struct  ObservableYieldInstruction_1_t2406465997  : public Il2CppObject
{
public:
	// System.IDisposable UniRx.ObservableYieldInstruction`1::subscription
	Il2CppObject * ___subscription_0;
	// System.Boolean UniRx.ObservableYieldInstruction`1::reThrowOnError
	bool ___reThrowOnError_1;
	// T UniRx.ObservableYieldInstruction`1::current
	Transform_t284553113 * ___current_2;
	// T UniRx.ObservableYieldInstruction`1::result
	Transform_t284553113 * ___result_3;
	// System.Boolean UniRx.ObservableYieldInstruction`1::moveNext
	bool ___moveNext_4;
	// System.Boolean UniRx.ObservableYieldInstruction`1::hasResult
	bool ___hasResult_5;
	// System.Exception UniRx.ObservableYieldInstruction`1::error
	Exception_t1967233988 * ___error_6;

public:
	inline static int32_t get_offset_of_subscription_0() { return static_cast<int32_t>(offsetof(ObservableYieldInstruction_1_t2406465997, ___subscription_0)); }
	inline Il2CppObject * get_subscription_0() const { return ___subscription_0; }
	inline Il2CppObject ** get_address_of_subscription_0() { return &___subscription_0; }
	inline void set_subscription_0(Il2CppObject * value)
	{
		___subscription_0 = value;
		Il2CppCodeGenWriteBarrier(&___subscription_0, value);
	}

	inline static int32_t get_offset_of_reThrowOnError_1() { return static_cast<int32_t>(offsetof(ObservableYieldInstruction_1_t2406465997, ___reThrowOnError_1)); }
	inline bool get_reThrowOnError_1() const { return ___reThrowOnError_1; }
	inline bool* get_address_of_reThrowOnError_1() { return &___reThrowOnError_1; }
	inline void set_reThrowOnError_1(bool value)
	{
		___reThrowOnError_1 = value;
	}

	inline static int32_t get_offset_of_current_2() { return static_cast<int32_t>(offsetof(ObservableYieldInstruction_1_t2406465997, ___current_2)); }
	inline Transform_t284553113 * get_current_2() const { return ___current_2; }
	inline Transform_t284553113 ** get_address_of_current_2() { return &___current_2; }
	inline void set_current_2(Transform_t284553113 * value)
	{
		___current_2 = value;
		Il2CppCodeGenWriteBarrier(&___current_2, value);
	}

	inline static int32_t get_offset_of_result_3() { return static_cast<int32_t>(offsetof(ObservableYieldInstruction_1_t2406465997, ___result_3)); }
	inline Transform_t284553113 * get_result_3() const { return ___result_3; }
	inline Transform_t284553113 ** get_address_of_result_3() { return &___result_3; }
	inline void set_result_3(Transform_t284553113 * value)
	{
		___result_3 = value;
		Il2CppCodeGenWriteBarrier(&___result_3, value);
	}

	inline static int32_t get_offset_of_moveNext_4() { return static_cast<int32_t>(offsetof(ObservableYieldInstruction_1_t2406465997, ___moveNext_4)); }
	inline bool get_moveNext_4() const { return ___moveNext_4; }
	inline bool* get_address_of_moveNext_4() { return &___moveNext_4; }
	inline void set_moveNext_4(bool value)
	{
		___moveNext_4 = value;
	}

	inline static int32_t get_offset_of_hasResult_5() { return static_cast<int32_t>(offsetof(ObservableYieldInstruction_1_t2406465997, ___hasResult_5)); }
	inline bool get_hasResult_5() const { return ___hasResult_5; }
	inline bool* get_address_of_hasResult_5() { return &___hasResult_5; }
	inline void set_hasResult_5(bool value)
	{
		___hasResult_5 = value;
	}

	inline static int32_t get_offset_of_error_6() { return static_cast<int32_t>(offsetof(ObservableYieldInstruction_1_t2406465997, ___error_6)); }
	inline Exception_t1967233988 * get_error_6() const { return ___error_6; }
	inline Exception_t1967233988 ** get_address_of_error_6() { return &___error_6; }
	inline void set_error_6(Exception_t1967233988 * value)
	{
		___error_6 = value;
		Il2CppCodeGenWriteBarrier(&___error_6, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
