﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Zenject.GameObjectFactory`1<System.Object>
struct GameObjectFactory_1_t2417718272;
// System.Object
struct Il2CppObject;
// System.Collections.Generic.IEnumerable`1<Zenject.ZenjectResolveException>
struct IEnumerable_1_t4073207355;

#include "codegen/il2cpp-codegen.h"

// System.Void Zenject.GameObjectFactory`1<System.Object>::.ctor()
extern "C"  void GameObjectFactory_1__ctor_m100447914_gshared (GameObjectFactory_1_t2417718272 * __this, const MethodInfo* method);
#define GameObjectFactory_1__ctor_m100447914(__this, method) ((  void (*) (GameObjectFactory_1_t2417718272 *, const MethodInfo*))GameObjectFactory_1__ctor_m100447914_gshared)(__this, method)
// TValue Zenject.GameObjectFactory`1<System.Object>::Create()
extern "C"  Il2CppObject * GameObjectFactory_1_Create_m1300499766_gshared (GameObjectFactory_1_t2417718272 * __this, const MethodInfo* method);
#define GameObjectFactory_1_Create_m1300499766(__this, method) ((  Il2CppObject * (*) (GameObjectFactory_1_t2417718272 *, const MethodInfo*))GameObjectFactory_1_Create_m1300499766_gshared)(__this, method)
// System.Collections.Generic.IEnumerable`1<Zenject.ZenjectResolveException> Zenject.GameObjectFactory`1<System.Object>::Validate()
extern "C"  Il2CppObject* GameObjectFactory_1_Validate_m335307919_gshared (GameObjectFactory_1_t2417718272 * __this, const MethodInfo* method);
#define GameObjectFactory_1_Validate_m335307919(__this, method) ((  Il2CppObject* (*) (GameObjectFactory_1_t2417718272 *, const MethodInfo*))GameObjectFactory_1_Validate_m335307919_gshared)(__this, method)
