﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Predicate_1_gen1408070318MethodDeclarations.h"

// System.Void System.Predicate`1<SubstanceBase`2/Intrusion<System.Object,System.Func`2<System.Object,System.Object>>>::.ctor(System.Object,System.IntPtr)
#define Predicate_1__ctor_m1332665717(__this, ___object0, ___method1, method) ((  void (*) (Predicate_1_t619527146 *, Il2CppObject *, IntPtr_t, const MethodInfo*))Predicate_1__ctor_m982040097_gshared)(__this, ___object0, ___method1, method)
// System.Boolean System.Predicate`1<SubstanceBase`2/Intrusion<System.Object,System.Func`2<System.Object,System.Object>>>::Invoke(T)
#define Predicate_1_Invoke_m3634044849(__this, ___obj0, method) ((  bool (*) (Predicate_1_t619527146 *, Intrusion_t48563248 *, const MethodInfo*))Predicate_1_Invoke_m4106178309_gshared)(__this, ___obj0, method)
// System.IAsyncResult System.Predicate`1<SubstanceBase`2/Intrusion<System.Object,System.Func`2<System.Object,System.Object>>>::BeginInvoke(T,System.AsyncCallback,System.Object)
#define Predicate_1_BeginInvoke_m1988543108(__this, ___obj0, ___callback1, ___object2, method) ((  Il2CppObject * (*) (Predicate_1_t619527146 *, Intrusion_t48563248 *, AsyncCallback_t1363551830 *, Il2CppObject *, const MethodInfo*))Predicate_1_BeginInvoke_m2038073176_gshared)(__this, ___obj0, ___callback1, ___object2, method)
// System.Boolean System.Predicate`1<SubstanceBase`2/Intrusion<System.Object,System.Func`2<System.Object,System.Object>>>::EndInvoke(System.IAsyncResult)
#define Predicate_1_EndInvoke_m2569571523(__this, ___result0, method) ((  bool (*) (Predicate_1_t619527146 *, Il2CppObject *, const MethodInfo*))Predicate_1_EndInvoke_m3970497007_gshared)(__this, ___result0, method)
