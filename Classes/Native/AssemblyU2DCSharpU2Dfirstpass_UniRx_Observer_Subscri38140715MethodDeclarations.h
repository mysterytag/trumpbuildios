﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Observer/Subscribe_`1<UnityEngine.Color>
struct Subscribe__1_t38140715;
// System.Action`1<System.Exception>
struct Action_1_t2115686693;
// System.Action
struct Action_t437523947;
// System.Exception
struct Exception_t1967233988;

#include "codegen/il2cpp-codegen.h"
#include "System_Core_System_Action437523947.h"
#include "UnityEngine_UnityEngine_Color1588175760.h"
#include "mscorlib_System_Exception1967233988.h"

// System.Void UniRx.Observer/Subscribe_`1<UnityEngine.Color>::.ctor(System.Action`1<System.Exception>,System.Action)
extern "C"  void Subscribe__1__ctor_m3979438697_gshared (Subscribe__1_t38140715 * __this, Action_1_t2115686693 * ___onError0, Action_t437523947 * ___onCompleted1, const MethodInfo* method);
#define Subscribe__1__ctor_m3979438697(__this, ___onError0, ___onCompleted1, method) ((  void (*) (Subscribe__1_t38140715 *, Action_1_t2115686693 *, Action_t437523947 *, const MethodInfo*))Subscribe__1__ctor_m3979438697_gshared)(__this, ___onError0, ___onCompleted1, method)
// System.Void UniRx.Observer/Subscribe_`1<UnityEngine.Color>::OnNext(T)
extern "C"  void Subscribe__1_OnNext_m3233191568_gshared (Subscribe__1_t38140715 * __this, Color_t1588175760  ___value0, const MethodInfo* method);
#define Subscribe__1_OnNext_m3233191568(__this, ___value0, method) ((  void (*) (Subscribe__1_t38140715 *, Color_t1588175760 , const MethodInfo*))Subscribe__1_OnNext_m3233191568_gshared)(__this, ___value0, method)
// System.Void UniRx.Observer/Subscribe_`1<UnityEngine.Color>::OnError(System.Exception)
extern "C"  void Subscribe__1_OnError_m1738248607_gshared (Subscribe__1_t38140715 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method);
#define Subscribe__1_OnError_m1738248607(__this, ___error0, method) ((  void (*) (Subscribe__1_t38140715 *, Exception_t1967233988 *, const MethodInfo*))Subscribe__1_OnError_m1738248607_gshared)(__this, ___error0, method)
// System.Void UniRx.Observer/Subscribe_`1<UnityEngine.Color>::OnCompleted()
extern "C"  void Subscribe__1_OnCompleted_m343440242_gshared (Subscribe__1_t38140715 * __this, const MethodInfo* method);
#define Subscribe__1_OnCompleted_m343440242(__this, method) ((  void (*) (Subscribe__1_t38140715 *, const MethodInfo*))Subscribe__1_OnCompleted_m343440242_gshared)(__this, method)
