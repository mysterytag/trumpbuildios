﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UniRx.IObservable`1<System.Object>
struct IObservable_1_t595904784;
// UniRx.Operators.ZipLatestFunc`6<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>
struct ZipLatestFunc_6_t2313265476;

#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Oper4196218687.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Operators.ZipLatestObservable`6<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>
struct  ZipLatestObservable_6_t2530856849  : public OperatorObservableBase_1_t4196218687
{
public:
	// UniRx.IObservable`1<T1> UniRx.Operators.ZipLatestObservable`6::source1
	Il2CppObject* ___source1_1;
	// UniRx.IObservable`1<T2> UniRx.Operators.ZipLatestObservable`6::source2
	Il2CppObject* ___source2_2;
	// UniRx.IObservable`1<T3> UniRx.Operators.ZipLatestObservable`6::source3
	Il2CppObject* ___source3_3;
	// UniRx.IObservable`1<T4> UniRx.Operators.ZipLatestObservable`6::source4
	Il2CppObject* ___source4_4;
	// UniRx.IObservable`1<T5> UniRx.Operators.ZipLatestObservable`6::source5
	Il2CppObject* ___source5_5;
	// UniRx.Operators.ZipLatestFunc`6<T1,T2,T3,T4,T5,TR> UniRx.Operators.ZipLatestObservable`6::resultSelector
	ZipLatestFunc_6_t2313265476 * ___resultSelector_6;

public:
	inline static int32_t get_offset_of_source1_1() { return static_cast<int32_t>(offsetof(ZipLatestObservable_6_t2530856849, ___source1_1)); }
	inline Il2CppObject* get_source1_1() const { return ___source1_1; }
	inline Il2CppObject** get_address_of_source1_1() { return &___source1_1; }
	inline void set_source1_1(Il2CppObject* value)
	{
		___source1_1 = value;
		Il2CppCodeGenWriteBarrier(&___source1_1, value);
	}

	inline static int32_t get_offset_of_source2_2() { return static_cast<int32_t>(offsetof(ZipLatestObservable_6_t2530856849, ___source2_2)); }
	inline Il2CppObject* get_source2_2() const { return ___source2_2; }
	inline Il2CppObject** get_address_of_source2_2() { return &___source2_2; }
	inline void set_source2_2(Il2CppObject* value)
	{
		___source2_2 = value;
		Il2CppCodeGenWriteBarrier(&___source2_2, value);
	}

	inline static int32_t get_offset_of_source3_3() { return static_cast<int32_t>(offsetof(ZipLatestObservable_6_t2530856849, ___source3_3)); }
	inline Il2CppObject* get_source3_3() const { return ___source3_3; }
	inline Il2CppObject** get_address_of_source3_3() { return &___source3_3; }
	inline void set_source3_3(Il2CppObject* value)
	{
		___source3_3 = value;
		Il2CppCodeGenWriteBarrier(&___source3_3, value);
	}

	inline static int32_t get_offset_of_source4_4() { return static_cast<int32_t>(offsetof(ZipLatestObservable_6_t2530856849, ___source4_4)); }
	inline Il2CppObject* get_source4_4() const { return ___source4_4; }
	inline Il2CppObject** get_address_of_source4_4() { return &___source4_4; }
	inline void set_source4_4(Il2CppObject* value)
	{
		___source4_4 = value;
		Il2CppCodeGenWriteBarrier(&___source4_4, value);
	}

	inline static int32_t get_offset_of_source5_5() { return static_cast<int32_t>(offsetof(ZipLatestObservable_6_t2530856849, ___source5_5)); }
	inline Il2CppObject* get_source5_5() const { return ___source5_5; }
	inline Il2CppObject** get_address_of_source5_5() { return &___source5_5; }
	inline void set_source5_5(Il2CppObject* value)
	{
		___source5_5 = value;
		Il2CppCodeGenWriteBarrier(&___source5_5, value);
	}

	inline static int32_t get_offset_of_resultSelector_6() { return static_cast<int32_t>(offsetof(ZipLatestObservable_6_t2530856849, ___resultSelector_6)); }
	inline ZipLatestFunc_6_t2313265476 * get_resultSelector_6() const { return ___resultSelector_6; }
	inline ZipLatestFunc_6_t2313265476 ** get_address_of_resultSelector_6() { return &___resultSelector_6; }
	inline void set_resultSelector_6(ZipLatestFunc_6_t2313265476 * value)
	{
		___resultSelector_6 = value;
		Il2CppCodeGenWriteBarrier(&___resultSelector_6, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
