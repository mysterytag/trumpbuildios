﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.InternalUtil.ListObserver`1<System.Int64>
struct ListObserver_1_t3142032553;
// UniRx.InternalUtil.ImmutableList`1<UniRx.IObserver`1<System.Int64>>
struct ImmutableList_1_t1824650988;
// System.Exception
struct Exception_t1967233988;
// UniRx.IObserver`1<System.Int64>
struct IObserver_1_t764446489;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Exception1967233988.h"

// System.Void UniRx.InternalUtil.ListObserver`1<System.Int64>::.ctor(UniRx.InternalUtil.ImmutableList`1<UniRx.IObserver`1<T>>)
extern "C"  void ListObserver_1__ctor_m3306140953_gshared (ListObserver_1_t3142032553 * __this, ImmutableList_1_t1824650988 * ___observers0, const MethodInfo* method);
#define ListObserver_1__ctor_m3306140953(__this, ___observers0, method) ((  void (*) (ListObserver_1_t3142032553 *, ImmutableList_1_t1824650988 *, const MethodInfo*))ListObserver_1__ctor_m3306140953_gshared)(__this, ___observers0, method)
// System.Void UniRx.InternalUtil.ListObserver`1<System.Int64>::OnCompleted()
extern "C"  void ListObserver_1_OnCompleted_m965560209_gshared (ListObserver_1_t3142032553 * __this, const MethodInfo* method);
#define ListObserver_1_OnCompleted_m965560209(__this, method) ((  void (*) (ListObserver_1_t3142032553 *, const MethodInfo*))ListObserver_1_OnCompleted_m965560209_gshared)(__this, method)
// System.Void UniRx.InternalUtil.ListObserver`1<System.Int64>::OnError(System.Exception)
extern "C"  void ListObserver_1_OnError_m3652986686_gshared (ListObserver_1_t3142032553 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method);
#define ListObserver_1_OnError_m3652986686(__this, ___error0, method) ((  void (*) (ListObserver_1_t3142032553 *, Exception_t1967233988 *, const MethodInfo*))ListObserver_1_OnError_m3652986686_gshared)(__this, ___error0, method)
// System.Void UniRx.InternalUtil.ListObserver`1<System.Int64>::OnNext(T)
extern "C"  void ListObserver_1_OnNext_m3172747823_gshared (ListObserver_1_t3142032553 * __this, int64_t ___value0, const MethodInfo* method);
#define ListObserver_1_OnNext_m3172747823(__this, ___value0, method) ((  void (*) (ListObserver_1_t3142032553 *, int64_t, const MethodInfo*))ListObserver_1_OnNext_m3172747823_gshared)(__this, ___value0, method)
// UniRx.IObserver`1<T> UniRx.InternalUtil.ListObserver`1<System.Int64>::Add(UniRx.IObserver`1<T>)
extern "C"  Il2CppObject* ListObserver_1_Add_m2629451023_gshared (ListObserver_1_t3142032553 * __this, Il2CppObject* ___observer0, const MethodInfo* method);
#define ListObserver_1_Add_m2629451023(__this, ___observer0, method) ((  Il2CppObject* (*) (ListObserver_1_t3142032553 *, Il2CppObject*, const MethodInfo*))ListObserver_1_Add_m2629451023_gshared)(__this, ___observer0, method)
// UniRx.IObserver`1<T> UniRx.InternalUtil.ListObserver`1<System.Int64>::Remove(UniRx.IObserver`1<T>)
extern "C"  Il2CppObject* ListObserver_1_Remove_m2795327452_gshared (ListObserver_1_t3142032553 * __this, Il2CppObject* ___observer0, const MethodInfo* method);
#define ListObserver_1_Remove_m2795327452(__this, ___observer0, method) ((  Il2CppObject* (*) (ListObserver_1_t3142032553 *, Il2CppObject*, const MethodInfo*))ListObserver_1_Remove_m2795327452_gshared)(__this, ___observer0, method)
