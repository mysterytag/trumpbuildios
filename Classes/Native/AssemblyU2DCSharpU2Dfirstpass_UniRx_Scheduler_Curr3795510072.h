﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UniRx.InternalUtil.SchedulerQueue
struct SchedulerQueue_t3710535235;
// System.Diagnostics.Stopwatch
struct Stopwatch_t2509581612;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Scheduler/CurrentThreadScheduler
struct  CurrentThreadScheduler_t3795510072  : public Il2CppObject
{
public:

public:
};

struct CurrentThreadScheduler_t3795510072_ThreadStaticFields
{
public:
	// UniRx.InternalUtil.SchedulerQueue UniRx.Scheduler/CurrentThreadScheduler::s_threadLocalQueue
	SchedulerQueue_t3710535235 * ___s_threadLocalQueue_0;
	// System.Diagnostics.Stopwatch UniRx.Scheduler/CurrentThreadScheduler::s_clock
	Stopwatch_t2509581612 * ___s_clock_1;

public:
	inline static int32_t get_offset_of_s_threadLocalQueue_0() { return static_cast<int32_t>(offsetof(CurrentThreadScheduler_t3795510072_ThreadStaticFields, ___s_threadLocalQueue_0)); }
	inline SchedulerQueue_t3710535235 * get_s_threadLocalQueue_0() const { return ___s_threadLocalQueue_0; }
	inline SchedulerQueue_t3710535235 ** get_address_of_s_threadLocalQueue_0() { return &___s_threadLocalQueue_0; }
	inline void set_s_threadLocalQueue_0(SchedulerQueue_t3710535235 * value)
	{
		___s_threadLocalQueue_0 = value;
		Il2CppCodeGenWriteBarrier(&___s_threadLocalQueue_0, value);
	}

	inline static int32_t get_offset_of_s_clock_1() { return static_cast<int32_t>(offsetof(CurrentThreadScheduler_t3795510072_ThreadStaticFields, ___s_clock_1)); }
	inline Stopwatch_t2509581612 * get_s_clock_1() const { return ___s_clock_1; }
	inline Stopwatch_t2509581612 ** get_address_of_s_clock_1() { return &___s_clock_1; }
	inline void set_s_clock_1(Stopwatch_t2509581612 * value)
	{
		___s_clock_1 = value;
		Il2CppCodeGenWriteBarrier(&___s_clock_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
