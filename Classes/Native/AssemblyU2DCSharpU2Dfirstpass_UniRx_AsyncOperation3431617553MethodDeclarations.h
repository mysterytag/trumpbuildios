﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.IObservable`1<UnityEngine.AsyncOperation>
struct IObservable_1_t3133193428;
// UnityEngine.AsyncOperation
struct AsyncOperation_t3374395064;
struct AsyncOperation_t3374395064_marshaled_pinvoke;
// UniRx.IProgress`1<System.Single>
struct IProgress_1_t4173802291;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_AsyncOperation3374395064.h"

// UniRx.IObservable`1<UnityEngine.AsyncOperation> UniRx.AsyncOperationExtensions::AsObservable(UnityEngine.AsyncOperation,UniRx.IProgress`1<System.Single>)
extern "C"  Il2CppObject* AsyncOperationExtensions_AsObservable_m962243477 (Il2CppObject * __this /* static, unused */, AsyncOperation_t3374395064 * ___asyncOperation0, Il2CppObject* ___progress1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
