﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Generic.IEnumerable`1<System.Type>
struct IEnumerable_1_t1356416995;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.SingletonInstanceHelper/<GetActiveSingletonTypesDerivingFrom>c__AnonStorey190`1<System.Object>
struct  U3CGetActiveSingletonTypesDerivingFromU3Ec__AnonStorey190_1_t1056901837  : public Il2CppObject
{
public:
	// System.Collections.Generic.IEnumerable`1<System.Type> Zenject.SingletonInstanceHelper/<GetActiveSingletonTypesDerivingFrom>c__AnonStorey190`1::ignoreTypes
	Il2CppObject* ___ignoreTypes_0;

public:
	inline static int32_t get_offset_of_ignoreTypes_0() { return static_cast<int32_t>(offsetof(U3CGetActiveSingletonTypesDerivingFromU3Ec__AnonStorey190_1_t1056901837, ___ignoreTypes_0)); }
	inline Il2CppObject* get_ignoreTypes_0() const { return ___ignoreTypes_0; }
	inline Il2CppObject** get_address_of_ignoreTypes_0() { return &___ignoreTypes_0; }
	inline void set_ignoreTypes_0(Il2CppObject* value)
	{
		___ignoreTypes_0 = value;
		Il2CppCodeGenWriteBarrier(&___ignoreTypes_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
