﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2/Transform`1<SponsorPay.SPLogLevel,System.Int32,System.Collections.Generic.KeyValuePair`2<SponsorPay.SPLogLevel,System.Int32>>
struct Transform_1_t1656395066;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t537683269;
// System.AsyncCallback
struct AsyncCallback_t1363551830;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_IntPtr676692020.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_21779196768.h"
#include "AssemblyU2DCSharp_SponsorPay_SPLogLevel220722135.h"
#include "mscorlib_System_AsyncCallback1363551830.h"

// System.Void System.Collections.Generic.Dictionary`2/Transform`1<SponsorPay.SPLogLevel,System.Int32,System.Collections.Generic.KeyValuePair`2<SponsorPay.SPLogLevel,System.Int32>>::.ctor(System.Object,System.IntPtr)
extern "C"  void Transform_1__ctor_m4239734290_gshared (Transform_1_t1656395066 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method);
#define Transform_1__ctor_m4239734290(__this, ___object0, ___method1, method) ((  void (*) (Transform_1_t1656395066 *, Il2CppObject *, IntPtr_t, const MethodInfo*))Transform_1__ctor_m4239734290_gshared)(__this, ___object0, ___method1, method)
// TRet System.Collections.Generic.Dictionary`2/Transform`1<SponsorPay.SPLogLevel,System.Int32,System.Collections.Generic.KeyValuePair`2<SponsorPay.SPLogLevel,System.Int32>>::Invoke(TKey,TValue)
extern "C"  KeyValuePair_2_t1779196768  Transform_1_Invoke_m15368998_gshared (Transform_1_t1656395066 * __this, int32_t ___key0, int32_t ___value1, const MethodInfo* method);
#define Transform_1_Invoke_m15368998(__this, ___key0, ___value1, method) ((  KeyValuePair_2_t1779196768  (*) (Transform_1_t1656395066 *, int32_t, int32_t, const MethodInfo*))Transform_1_Invoke_m15368998_gshared)(__this, ___key0, ___value1, method)
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<SponsorPay.SPLogLevel,System.Int32,System.Collections.Generic.KeyValuePair`2<SponsorPay.SPLogLevel,System.Int32>>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * Transform_1_BeginInvoke_m252533841_gshared (Transform_1_t1656395066 * __this, int32_t ___key0, int32_t ___value1, AsyncCallback_t1363551830 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method);
#define Transform_1_BeginInvoke_m252533841(__this, ___key0, ___value1, ___callback2, ___object3, method) ((  Il2CppObject * (*) (Transform_1_t1656395066 *, int32_t, int32_t, AsyncCallback_t1363551830 *, Il2CppObject *, const MethodInfo*))Transform_1_BeginInvoke_m252533841_gshared)(__this, ___key0, ___value1, ___callback2, ___object3, method)
// TRet System.Collections.Generic.Dictionary`2/Transform`1<SponsorPay.SPLogLevel,System.Int32,System.Collections.Generic.KeyValuePair`2<SponsorPay.SPLogLevel,System.Int32>>::EndInvoke(System.IAsyncResult)
extern "C"  KeyValuePair_2_t1779196768  Transform_1_EndInvoke_m1065273764_gshared (Transform_1_t1656395066 * __this, Il2CppObject * ___result0, const MethodInfo* method);
#define Transform_1_EndInvoke_m1065273764(__this, ___result0, method) ((  KeyValuePair_2_t1779196768  (*) (Transform_1_t1656395066 *, Il2CppObject *, const MethodInfo*))Transform_1_EndInvoke_m1065273764_gshared)(__this, ___result0, method)
