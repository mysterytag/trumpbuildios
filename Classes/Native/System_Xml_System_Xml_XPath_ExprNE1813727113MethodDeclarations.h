﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Xml.XPath.ExprNE
struct ExprNE_t1813727113;
// System.Xml.XPath.Expression
struct Expression_t4217024437;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "System_Xml_System_Xml_XPath_Expression4217024437.h"

// System.Void System.Xml.XPath.ExprNE::.ctor(System.Xml.XPath.Expression,System.Xml.XPath.Expression)
extern "C"  void ExprNE__ctor_m3803811634 (ExprNE_t1813727113 * __this, Expression_t4217024437 * ___left0, Expression_t4217024437 * ___right1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Xml.XPath.ExprNE::get_Operator()
extern "C"  String_t* ExprNE_get_Operator_m1094391488 (ExprNE_t1813727113 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
