﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PlayFab.ClientModels.CharacterLeaderboardEntry
struct  CharacterLeaderboardEntry_t3117535278  : public Il2CppObject
{
public:
	// System.String PlayFab.ClientModels.CharacterLeaderboardEntry::<PlayFabId>k__BackingField
	String_t* ___U3CPlayFabIdU3Ek__BackingField_0;
	// System.String PlayFab.ClientModels.CharacterLeaderboardEntry::<CharacterId>k__BackingField
	String_t* ___U3CCharacterIdU3Ek__BackingField_1;
	// System.String PlayFab.ClientModels.CharacterLeaderboardEntry::<CharacterName>k__BackingField
	String_t* ___U3CCharacterNameU3Ek__BackingField_2;
	// System.String PlayFab.ClientModels.CharacterLeaderboardEntry::<DisplayName>k__BackingField
	String_t* ___U3CDisplayNameU3Ek__BackingField_3;
	// System.String PlayFab.ClientModels.CharacterLeaderboardEntry::<CharacterType>k__BackingField
	String_t* ___U3CCharacterTypeU3Ek__BackingField_4;
	// System.Int32 PlayFab.ClientModels.CharacterLeaderboardEntry::<StatValue>k__BackingField
	int32_t ___U3CStatValueU3Ek__BackingField_5;
	// System.Int32 PlayFab.ClientModels.CharacterLeaderboardEntry::<Position>k__BackingField
	int32_t ___U3CPositionU3Ek__BackingField_6;

public:
	inline static int32_t get_offset_of_U3CPlayFabIdU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(CharacterLeaderboardEntry_t3117535278, ___U3CPlayFabIdU3Ek__BackingField_0)); }
	inline String_t* get_U3CPlayFabIdU3Ek__BackingField_0() const { return ___U3CPlayFabIdU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CPlayFabIdU3Ek__BackingField_0() { return &___U3CPlayFabIdU3Ek__BackingField_0; }
	inline void set_U3CPlayFabIdU3Ek__BackingField_0(String_t* value)
	{
		___U3CPlayFabIdU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier(&___U3CPlayFabIdU3Ek__BackingField_0, value);
	}

	inline static int32_t get_offset_of_U3CCharacterIdU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(CharacterLeaderboardEntry_t3117535278, ___U3CCharacterIdU3Ek__BackingField_1)); }
	inline String_t* get_U3CCharacterIdU3Ek__BackingField_1() const { return ___U3CCharacterIdU3Ek__BackingField_1; }
	inline String_t** get_address_of_U3CCharacterIdU3Ek__BackingField_1() { return &___U3CCharacterIdU3Ek__BackingField_1; }
	inline void set_U3CCharacterIdU3Ek__BackingField_1(String_t* value)
	{
		___U3CCharacterIdU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CCharacterIdU3Ek__BackingField_1, value);
	}

	inline static int32_t get_offset_of_U3CCharacterNameU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(CharacterLeaderboardEntry_t3117535278, ___U3CCharacterNameU3Ek__BackingField_2)); }
	inline String_t* get_U3CCharacterNameU3Ek__BackingField_2() const { return ___U3CCharacterNameU3Ek__BackingField_2; }
	inline String_t** get_address_of_U3CCharacterNameU3Ek__BackingField_2() { return &___U3CCharacterNameU3Ek__BackingField_2; }
	inline void set_U3CCharacterNameU3Ek__BackingField_2(String_t* value)
	{
		___U3CCharacterNameU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier(&___U3CCharacterNameU3Ek__BackingField_2, value);
	}

	inline static int32_t get_offset_of_U3CDisplayNameU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(CharacterLeaderboardEntry_t3117535278, ___U3CDisplayNameU3Ek__BackingField_3)); }
	inline String_t* get_U3CDisplayNameU3Ek__BackingField_3() const { return ___U3CDisplayNameU3Ek__BackingField_3; }
	inline String_t** get_address_of_U3CDisplayNameU3Ek__BackingField_3() { return &___U3CDisplayNameU3Ek__BackingField_3; }
	inline void set_U3CDisplayNameU3Ek__BackingField_3(String_t* value)
	{
		___U3CDisplayNameU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier(&___U3CDisplayNameU3Ek__BackingField_3, value);
	}

	inline static int32_t get_offset_of_U3CCharacterTypeU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(CharacterLeaderboardEntry_t3117535278, ___U3CCharacterTypeU3Ek__BackingField_4)); }
	inline String_t* get_U3CCharacterTypeU3Ek__BackingField_4() const { return ___U3CCharacterTypeU3Ek__BackingField_4; }
	inline String_t** get_address_of_U3CCharacterTypeU3Ek__BackingField_4() { return &___U3CCharacterTypeU3Ek__BackingField_4; }
	inline void set_U3CCharacterTypeU3Ek__BackingField_4(String_t* value)
	{
		___U3CCharacterTypeU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier(&___U3CCharacterTypeU3Ek__BackingField_4, value);
	}

	inline static int32_t get_offset_of_U3CStatValueU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(CharacterLeaderboardEntry_t3117535278, ___U3CStatValueU3Ek__BackingField_5)); }
	inline int32_t get_U3CStatValueU3Ek__BackingField_5() const { return ___U3CStatValueU3Ek__BackingField_5; }
	inline int32_t* get_address_of_U3CStatValueU3Ek__BackingField_5() { return &___U3CStatValueU3Ek__BackingField_5; }
	inline void set_U3CStatValueU3Ek__BackingField_5(int32_t value)
	{
		___U3CStatValueU3Ek__BackingField_5 = value;
	}

	inline static int32_t get_offset_of_U3CPositionU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(CharacterLeaderboardEntry_t3117535278, ___U3CPositionU3Ek__BackingField_6)); }
	inline int32_t get_U3CPositionU3Ek__BackingField_6() const { return ___U3CPositionU3Ek__BackingField_6; }
	inline int32_t* get_address_of_U3CPositionU3Ek__BackingField_6() { return &___U3CPositionU3Ek__BackingField_6; }
	inline void set_U3CPositionU3Ek__BackingField_6(int32_t value)
	{
		___U3CPositionU3Ek__BackingField_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
