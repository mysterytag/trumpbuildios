﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Zenject.TickableManager/<InitLateTickables>c__AnonStorey194
struct U3CInitLateTickablesU3Ec__AnonStorey194_t4122519257;
// ModestTree.Util.Tuple`2<System.Type,System.Int32>
struct Tuple_2_t3719549051;

#include "codegen/il2cpp-codegen.h"

// System.Void Zenject.TickableManager/<InitLateTickables>c__AnonStorey194::.ctor()
extern "C"  void U3CInitLateTickablesU3Ec__AnonStorey194__ctor_m278671530 (U3CInitLateTickablesU3Ec__AnonStorey194_t4122519257 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Zenject.TickableManager/<InitLateTickables>c__AnonStorey194::<>m__2EF(ModestTree.Util.Tuple`2<System.Type,System.Int32>)
extern "C"  bool U3CInitLateTickablesU3Ec__AnonStorey194_U3CU3Em__2EF_m500120970 (U3CInitLateTickablesU3Ec__AnonStorey194_t4122519257 * __this, Tuple_2_t3719549051 * ___x0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
