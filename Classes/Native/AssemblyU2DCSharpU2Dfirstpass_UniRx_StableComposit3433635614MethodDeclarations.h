﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.StableCompositeDisposable
struct StableCompositeDisposable_t3433635614;
// UniRx.ICancelable
struct ICancelable_t4109686575;
// System.IDisposable
struct IDisposable_t1628921374;
// System.IDisposable[]
struct IDisposableU5BU5D_t165183403;
// System.Collections.Generic.IEnumerable`1<System.IDisposable>
struct IEnumerable_1_t206108434;

#include "codegen/il2cpp-codegen.h"

// System.Void UniRx.StableCompositeDisposable::.ctor()
extern "C"  void StableCompositeDisposable__ctor_m884178307 (StableCompositeDisposable_t3433635614 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UniRx.ICancelable UniRx.StableCompositeDisposable::Create(System.IDisposable,System.IDisposable)
extern "C"  Il2CppObject * StableCompositeDisposable_Create_m3764788535 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___disposable10, Il2CppObject * ___disposable21, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UniRx.ICancelable UniRx.StableCompositeDisposable::Create(System.IDisposable,System.IDisposable,System.IDisposable)
extern "C"  Il2CppObject * StableCompositeDisposable_Create_m2505029765 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___disposable10, Il2CppObject * ___disposable21, Il2CppObject * ___disposable32, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UniRx.ICancelable UniRx.StableCompositeDisposable::Create(System.IDisposable,System.IDisposable,System.IDisposable,System.IDisposable)
extern "C"  Il2CppObject * StableCompositeDisposable_Create_m232885879 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___disposable10, Il2CppObject * ___disposable21, Il2CppObject * ___disposable32, Il2CppObject * ___disposable43, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UniRx.ICancelable UniRx.StableCompositeDisposable::Create(System.IDisposable[])
extern "C"  Il2CppObject * StableCompositeDisposable_Create_m1215138723 (Il2CppObject * __this /* static, unused */, IDisposableU5BU5D_t165183403* ___disposables0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UniRx.ICancelable UniRx.StableCompositeDisposable::CreateUnsafe(System.IDisposable[])
extern "C"  Il2CppObject * StableCompositeDisposable_CreateUnsafe_m1071468137 (Il2CppObject * __this /* static, unused */, IDisposableU5BU5D_t165183403* ___disposables0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UniRx.ICancelable UniRx.StableCompositeDisposable::Create(System.Collections.Generic.IEnumerable`1<System.IDisposable>)
extern "C"  Il2CppObject * StableCompositeDisposable_Create_m3902767108 (Il2CppObject * __this /* static, unused */, Il2CppObject* ___disposables0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
