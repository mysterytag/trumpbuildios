﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.PlayFabClientAPI/<RedeemCoupon>c__AnonStoreyA7
struct U3CRedeemCouponU3Ec__AnonStoreyA7_t1750700829;
// PlayFab.CallRequestContainer
struct CallRequestContainer_t432318609;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharpU2Dfirstpass_PlayFab_CallRequestCo432318609.h"

// System.Void PlayFab.PlayFabClientAPI/<RedeemCoupon>c__AnonStoreyA7::.ctor()
extern "C"  void U3CRedeemCouponU3Ec__AnonStoreyA7__ctor_m1164247350 (U3CRedeemCouponU3Ec__AnonStoreyA7_t1750700829 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/<RedeemCoupon>c__AnonStoreyA7::<>m__94(PlayFab.CallRequestContainer)
extern "C"  void U3CRedeemCouponU3Ec__AnonStoreyA7_U3CU3Em__94_m2143908719 (U3CRedeemCouponU3Ec__AnonStoreyA7_t1750700829 * __this, CallRequestContainer_t432318609 * ___requestContainer0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
