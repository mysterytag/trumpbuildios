﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UpgradeDescriptor
struct UpgradeDescriptor_t2646513291;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String968488902.h"

// System.Void UpgradeDescriptor::.ctor()
extern "C"  void UpgradeDescriptor__ctor_m1006519104 (UpgradeDescriptor_t2646513291 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UpgradeDescriptor::.ctor(System.String)
extern "C"  void UpgradeDescriptor__ctor_m173712322 (UpgradeDescriptor_t2646513291 * __this, String_t* ____name0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UpgradeDescriptor::.ctor(System.String,System.Double,System.Double)
extern "C"  void UpgradeDescriptor__ctor_m2099825850 (UpgradeDescriptor_t2646513291 * __this, String_t* ____name0, double ____price1, double ____amount2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UpgradeDescriptor UpgradeDescriptor::SetStringID(System.String)
extern "C"  UpgradeDescriptor_t2646513291 * UpgradeDescriptor_SetStringID_m1253452494 (UpgradeDescriptor_t2646513291 * __this, String_t* ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UpgradeDescriptor UpgradeDescriptor::AddAmountLevel(System.Double)
extern "C"  UpgradeDescriptor_t2646513291 * UpgradeDescriptor_AddAmountLevel_m2477438875 (UpgradeDescriptor_t2646513291 * __this, double ____amount0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UpgradeDescriptor UpgradeDescriptor::AddPriceLevel(System.Double)
extern "C"  UpgradeDescriptor_t2646513291 * UpgradeDescriptor_AddPriceLevel_m1425649344 (UpgradeDescriptor_t2646513291 * __this, double ____price0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
