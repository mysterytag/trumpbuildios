﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.ClientModels.LoginWithXboxRequest
struct LoginWithXboxRequest_t2356925853;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String968488902.h"
#include "mscorlib_System_Nullable_1_gen3097043249.h"

// System.Void PlayFab.ClientModels.LoginWithXboxRequest::.ctor()
extern "C"  void LoginWithXboxRequest__ctor_m2449094208 (LoginWithXboxRequest_t2356925853 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PlayFab.ClientModels.LoginWithXboxRequest::get_TitleId()
extern "C"  String_t* LoginWithXboxRequest_get_TitleId_m4239800811 (LoginWithXboxRequest_t2356925853 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.LoginWithXboxRequest::set_TitleId(System.String)
extern "C"  void LoginWithXboxRequest_set_TitleId_m3756803246 (LoginWithXboxRequest_t2356925853 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PlayFab.ClientModels.LoginWithXboxRequest::get_XboxToken()
extern "C"  String_t* LoginWithXboxRequest_get_XboxToken_m82160958 (LoginWithXboxRequest_t2356925853 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.LoginWithXboxRequest::set_XboxToken(System.String)
extern "C"  void LoginWithXboxRequest_set_XboxToken_m1211399803 (LoginWithXboxRequest_t2356925853 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Nullable`1<System.Boolean> PlayFab.ClientModels.LoginWithXboxRequest::get_CreateAccount()
extern "C"  Nullable_1_t3097043249  LoginWithXboxRequest_get_CreateAccount_m218779343 (LoginWithXboxRequest_t2356925853 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.LoginWithXboxRequest::set_CreateAccount(System.Nullable`1<System.Boolean>)
extern "C"  void LoginWithXboxRequest_set_CreateAccount_m2849995542 (LoginWithXboxRequest_t2356925853 * __this, Nullable_1_t3097043249  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
