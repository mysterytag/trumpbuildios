﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.UUnit.OtherSpecificDatatypes
struct OtherSpecificDatatypes_t2598791895;
// System.Collections.Generic.Dictionary`2<System.String,System.String>
struct Dictionary_2_t2606186806;
// System.Collections.Generic.Dictionary`2<System.String,System.Int32>
struct Dictionary_2_t190145395;
// System.Collections.Generic.Dictionary`2<System.String,System.UInt32>
struct Dictionary_2_t2623623230;
// System.Collections.Generic.Dictionary`2<System.String,PlayFab.UUnit.Region>
struct Dictionary_2_t432490094;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String968488902.h"

// System.Void PlayFab.UUnit.OtherSpecificDatatypes::.ctor()
extern "C"  void OtherSpecificDatatypes__ctor_m3904370494 (OtherSpecificDatatypes_t2598791895 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.Dictionary`2<System.String,System.String> PlayFab.UUnit.OtherSpecificDatatypes::get_StringDict()
extern "C"  Dictionary_2_t2606186806 * OtherSpecificDatatypes_get_StringDict_m2957822820 (OtherSpecificDatatypes_t2598791895 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.UUnit.OtherSpecificDatatypes::set_StringDict(System.Collections.Generic.Dictionary`2<System.String,System.String>)
extern "C"  void OtherSpecificDatatypes_set_StringDict_m4017983643 (OtherSpecificDatatypes_t2598791895 * __this, Dictionary_2_t2606186806 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.Dictionary`2<System.String,System.Int32> PlayFab.UUnit.OtherSpecificDatatypes::get_IntDict()
extern "C"  Dictionary_2_t190145395 * OtherSpecificDatatypes_get_IntDict_m2055803731 (OtherSpecificDatatypes_t2598791895 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.UUnit.OtherSpecificDatatypes::set_IntDict(System.Collections.Generic.Dictionary`2<System.String,System.Int32>)
extern "C"  void OtherSpecificDatatypes_set_IntDict_m1810926176 (OtherSpecificDatatypes_t2598791895 * __this, Dictionary_2_t190145395 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.Dictionary`2<System.String,System.UInt32> PlayFab.UUnit.OtherSpecificDatatypes::get_UintDict()
extern "C"  Dictionary_2_t2623623230 * OtherSpecificDatatypes_get_UintDict_m2251116533 (OtherSpecificDatatypes_t2598791895 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.UUnit.OtherSpecificDatatypes::set_UintDict(System.Collections.Generic.Dictionary`2<System.String,System.UInt32>)
extern "C"  void OtherSpecificDatatypes_set_UintDict_m3749617196 (OtherSpecificDatatypes_t2598791895 * __this, Dictionary_2_t2623623230 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.Dictionary`2<System.String,PlayFab.UUnit.Region> PlayFab.UUnit.OtherSpecificDatatypes::get_EnumDict()
extern "C"  Dictionary_2_t432490094 * OtherSpecificDatatypes_get_EnumDict_m3019190336 (OtherSpecificDatatypes_t2598791895 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.UUnit.OtherSpecificDatatypes::set_EnumDict(System.Collections.Generic.Dictionary`2<System.String,PlayFab.UUnit.Region>)
extern "C"  void OtherSpecificDatatypes_set_EnumDict_m3843096863 (OtherSpecificDatatypes_t2598791895 * __this, Dictionary_2_t432490094 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PlayFab.UUnit.OtherSpecificDatatypes::get_TestString()
extern "C"  String_t* OtherSpecificDatatypes_get_TestString_m459817653 (OtherSpecificDatatypes_t2598791895 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.UUnit.OtherSpecificDatatypes::set_TestString(System.String)
extern "C"  void OtherSpecificDatatypes_set_TestString_m3708901756 (OtherSpecificDatatypes_t2598791895 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
