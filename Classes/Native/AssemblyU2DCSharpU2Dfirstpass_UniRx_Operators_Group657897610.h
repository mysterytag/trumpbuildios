﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UniRx.IObservable`1<System.Object>
struct IObservable_1_t595904784;
// System.Func`2<System.Object,System.Object>
struct Func_2_t2135783352;
// System.Collections.Generic.IEqualityComparer`1<System.Object>
struct IEqualityComparer_1_t3161373071;

#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Opera370530870.h"
#include "mscorlib_System_Nullable_1_gen1438485399.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Operators.GroupByObservable`3<System.Object,System.Object,System.Object>
struct  GroupByObservable_3_t657897610  : public OperatorObservableBase_1_t370530870
{
public:
	// UniRx.IObservable`1<TSource> UniRx.Operators.GroupByObservable`3::source
	Il2CppObject* ___source_1;
	// System.Func`2<TSource,TKey> UniRx.Operators.GroupByObservable`3::keySelector
	Func_2_t2135783352 * ___keySelector_2;
	// System.Func`2<TSource,TElement> UniRx.Operators.GroupByObservable`3::elementSelector
	Func_2_t2135783352 * ___elementSelector_3;
	// System.Nullable`1<System.Int32> UniRx.Operators.GroupByObservable`3::capacity
	Nullable_1_t1438485399  ___capacity_4;
	// System.Collections.Generic.IEqualityComparer`1<TKey> UniRx.Operators.GroupByObservable`3::comparer
	Il2CppObject* ___comparer_5;

public:
	inline static int32_t get_offset_of_source_1() { return static_cast<int32_t>(offsetof(GroupByObservable_3_t657897610, ___source_1)); }
	inline Il2CppObject* get_source_1() const { return ___source_1; }
	inline Il2CppObject** get_address_of_source_1() { return &___source_1; }
	inline void set_source_1(Il2CppObject* value)
	{
		___source_1 = value;
		Il2CppCodeGenWriteBarrier(&___source_1, value);
	}

	inline static int32_t get_offset_of_keySelector_2() { return static_cast<int32_t>(offsetof(GroupByObservable_3_t657897610, ___keySelector_2)); }
	inline Func_2_t2135783352 * get_keySelector_2() const { return ___keySelector_2; }
	inline Func_2_t2135783352 ** get_address_of_keySelector_2() { return &___keySelector_2; }
	inline void set_keySelector_2(Func_2_t2135783352 * value)
	{
		___keySelector_2 = value;
		Il2CppCodeGenWriteBarrier(&___keySelector_2, value);
	}

	inline static int32_t get_offset_of_elementSelector_3() { return static_cast<int32_t>(offsetof(GroupByObservable_3_t657897610, ___elementSelector_3)); }
	inline Func_2_t2135783352 * get_elementSelector_3() const { return ___elementSelector_3; }
	inline Func_2_t2135783352 ** get_address_of_elementSelector_3() { return &___elementSelector_3; }
	inline void set_elementSelector_3(Func_2_t2135783352 * value)
	{
		___elementSelector_3 = value;
		Il2CppCodeGenWriteBarrier(&___elementSelector_3, value);
	}

	inline static int32_t get_offset_of_capacity_4() { return static_cast<int32_t>(offsetof(GroupByObservable_3_t657897610, ___capacity_4)); }
	inline Nullable_1_t1438485399  get_capacity_4() const { return ___capacity_4; }
	inline Nullable_1_t1438485399 * get_address_of_capacity_4() { return &___capacity_4; }
	inline void set_capacity_4(Nullable_1_t1438485399  value)
	{
		___capacity_4 = value;
	}

	inline static int32_t get_offset_of_comparer_5() { return static_cast<int32_t>(offsetof(GroupByObservable_3_t657897610, ___comparer_5)); }
	inline Il2CppObject* get_comparer_5() const { return ___comparer_5; }
	inline Il2CppObject** get_address_of_comparer_5() { return &___comparer_5; }
	inline void set_comparer_5(Il2CppObject* value)
	{
		___comparer_5 = value;
		Il2CppCodeGenWriteBarrier(&___comparer_5, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
