﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UniRx.Operators.TimeoutFrameObservable`1/TimeoutFrame<System.Object>
struct TimeoutFrame_t2437429061;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Operators.TimeoutFrameObservable`1/TimeoutFrame/TimeoutFrameTick<System.Object>
struct  TimeoutFrameTick_t1345043194  : public Il2CppObject
{
public:
	// UniRx.Operators.TimeoutFrameObservable`1/TimeoutFrame<T> UniRx.Operators.TimeoutFrameObservable`1/TimeoutFrame/TimeoutFrameTick::parent
	TimeoutFrame_t2437429061 * ___parent_0;
	// System.UInt64 UniRx.Operators.TimeoutFrameObservable`1/TimeoutFrame/TimeoutFrameTick::timerId
	uint64_t ___timerId_1;

public:
	inline static int32_t get_offset_of_parent_0() { return static_cast<int32_t>(offsetof(TimeoutFrameTick_t1345043194, ___parent_0)); }
	inline TimeoutFrame_t2437429061 * get_parent_0() const { return ___parent_0; }
	inline TimeoutFrame_t2437429061 ** get_address_of_parent_0() { return &___parent_0; }
	inline void set_parent_0(TimeoutFrame_t2437429061 * value)
	{
		___parent_0 = value;
		Il2CppCodeGenWriteBarrier(&___parent_0, value);
	}

	inline static int32_t get_offset_of_timerId_1() { return static_cast<int32_t>(offsetof(TimeoutFrameTick_t1345043194, ___timerId_1)); }
	inline uint64_t get_timerId_1() const { return ___timerId_1; }
	inline uint64_t* get_address_of_timerId_1() { return &___timerId_1; }
	inline void set_timerId_1(uint64_t value)
	{
		___timerId_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
