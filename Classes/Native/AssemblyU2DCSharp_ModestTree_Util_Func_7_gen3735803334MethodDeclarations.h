﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ModestTree.Util.Func`7<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>
struct Func_7_t3735803334;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t537683269;
// System.AsyncCallback
struct AsyncCallback_t1363551830;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_IntPtr676692020.h"
#include "mscorlib_System_AsyncCallback1363551830.h"

// System.Void ModestTree.Util.Func`7<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>::.ctor(System.Object,System.IntPtr)
extern "C"  void Func_7__ctor_m2659415148_gshared (Func_7_t3735803334 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method);
#define Func_7__ctor_m2659415148(__this, ___object0, ___method1, method) ((  void (*) (Func_7_t3735803334 *, Il2CppObject *, IntPtr_t, const MethodInfo*))Func_7__ctor_m2659415148_gshared)(__this, ___object0, ___method1, method)
// TResult ModestTree.Util.Func`7<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>::Invoke(T1,T2,T3,T4,T5,T6)
extern "C"  Il2CppObject * Func_7_Invoke_m632476639_gshared (Func_7_t3735803334 * __this, Il2CppObject * ___arg10, Il2CppObject * ___arg21, Il2CppObject * ___arg32, Il2CppObject * ___arg43, Il2CppObject * ___arg54, Il2CppObject * ___arg65, const MethodInfo* method);
#define Func_7_Invoke_m632476639(__this, ___arg10, ___arg21, ___arg32, ___arg43, ___arg54, ___arg65, method) ((  Il2CppObject * (*) (Func_7_t3735803334 *, Il2CppObject *, Il2CppObject *, Il2CppObject *, Il2CppObject *, Il2CppObject *, Il2CppObject *, const MethodInfo*))Func_7_Invoke_m632476639_gshared)(__this, ___arg10, ___arg21, ___arg32, ___arg43, ___arg54, ___arg65, method)
// System.IAsyncResult ModestTree.Util.Func`7<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>::BeginInvoke(T1,T2,T3,T4,T5,T6,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * Func_7_BeginInvoke_m2277157288_gshared (Func_7_t3735803334 * __this, Il2CppObject * ___arg10, Il2CppObject * ___arg21, Il2CppObject * ___arg32, Il2CppObject * ___arg43, Il2CppObject * ___arg54, Il2CppObject * ___arg65, AsyncCallback_t1363551830 * ___callback6, Il2CppObject * ___object7, const MethodInfo* method);
#define Func_7_BeginInvoke_m2277157288(__this, ___arg10, ___arg21, ___arg32, ___arg43, ___arg54, ___arg65, ___callback6, ___object7, method) ((  Il2CppObject * (*) (Func_7_t3735803334 *, Il2CppObject *, Il2CppObject *, Il2CppObject *, Il2CppObject *, Il2CppObject *, Il2CppObject *, AsyncCallback_t1363551830 *, Il2CppObject *, const MethodInfo*))Func_7_BeginInvoke_m2277157288_gshared)(__this, ___arg10, ___arg21, ___arg32, ___arg43, ___arg54, ___arg65, ___callback6, ___object7, method)
// TResult ModestTree.Util.Func`7<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>::EndInvoke(System.IAsyncResult)
extern "C"  Il2CppObject * Func_7_EndInvoke_m3853478170_gshared (Func_7_t3735803334 * __this, Il2CppObject * ___result0, const MethodInfo* method);
#define Func_7_EndInvoke_m3853478170(__this, ___result0, method) ((  Il2CppObject * (*) (Func_7_t3735803334 *, Il2CppObject *, const MethodInfo*))Func_7_EndInvoke_m3853478170_gshared)(__this, ___result0, method)
