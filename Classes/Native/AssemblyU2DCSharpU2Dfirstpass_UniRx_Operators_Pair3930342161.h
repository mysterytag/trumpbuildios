﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UniRx.Operators.PairwiseObservable`2<System.Object,System.Object>
struct PairwiseObservable_2_t3262709271;
// System.Object
struct Il2CppObject;

#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Oper1187768149.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Operators.PairwiseObservable`2/Pairwise<System.Object,System.Object>
struct  Pairwise_t3930342161  : public OperatorObserverBase_2_t1187768149
{
public:
	// UniRx.Operators.PairwiseObservable`2<T,TR> UniRx.Operators.PairwiseObservable`2/Pairwise::parent
	PairwiseObservable_2_t3262709271 * ___parent_2;
	// T UniRx.Operators.PairwiseObservable`2/Pairwise::prev
	Il2CppObject * ___prev_3;
	// System.Boolean UniRx.Operators.PairwiseObservable`2/Pairwise::isFirst
	bool ___isFirst_4;

public:
	inline static int32_t get_offset_of_parent_2() { return static_cast<int32_t>(offsetof(Pairwise_t3930342161, ___parent_2)); }
	inline PairwiseObservable_2_t3262709271 * get_parent_2() const { return ___parent_2; }
	inline PairwiseObservable_2_t3262709271 ** get_address_of_parent_2() { return &___parent_2; }
	inline void set_parent_2(PairwiseObservable_2_t3262709271 * value)
	{
		___parent_2 = value;
		Il2CppCodeGenWriteBarrier(&___parent_2, value);
	}

	inline static int32_t get_offset_of_prev_3() { return static_cast<int32_t>(offsetof(Pairwise_t3930342161, ___prev_3)); }
	inline Il2CppObject * get_prev_3() const { return ___prev_3; }
	inline Il2CppObject ** get_address_of_prev_3() { return &___prev_3; }
	inline void set_prev_3(Il2CppObject * value)
	{
		___prev_3 = value;
		Il2CppCodeGenWriteBarrier(&___prev_3, value);
	}

	inline static int32_t get_offset_of_isFirst_4() { return static_cast<int32_t>(offsetof(Pairwise_t3930342161, ___isFirst_4)); }
	inline bool get_isFirst_4() const { return ___isFirst_4; }
	inline bool* get_address_of_isFirst_4() { return &___isFirst_4; }
	inline void set_isFirst_4(bool value)
	{
		___isFirst_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
