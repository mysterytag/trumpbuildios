﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SerializableDictionary`2<System.Object,System.Object>
struct SerializableDictionary_2_t2269426764;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t2995724695;
// System.Xml.Schema.XmlSchema
struct XmlSchema_t1932230565;
// System.Xml.XmlReader
struct XmlReader_t4229084514;
// System.Xml.XmlWriter
struct XmlWriter_t89522450;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Runtime_Serialization_Serializatio2995724695.h"
#include "mscorlib_System_Runtime_Serialization_StreamingCont986364934.h"
#include "System_Xml_System_Xml_XmlReader4229084514.h"
#include "System_Xml_System_Xml_XmlWriter89522450.h"

// System.Void SerializableDictionary`2<System.Object,System.Object>::.ctor()
extern "C"  void SerializableDictionary_2__ctor_m3570888488_gshared (SerializableDictionary_2_t2269426764 * __this, const MethodInfo* method);
#define SerializableDictionary_2__ctor_m3570888488(__this, method) ((  void (*) (SerializableDictionary_2_t2269426764 *, const MethodInfo*))SerializableDictionary_2__ctor_m3570888488_gshared)(__this, method)
// System.Void SerializableDictionary`2<System.Object,System.Object>::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C"  void SerializableDictionary_2__ctor_m2587960425_gshared (SerializableDictionary_2_t2269426764 * __this, SerializationInfo_t2995724695 * ___info0, StreamingContext_t986364934  ___context1, const MethodInfo* method);
#define SerializableDictionary_2__ctor_m2587960425(__this, ___info0, ___context1, method) ((  void (*) (SerializableDictionary_2_t2269426764 *, SerializationInfo_t2995724695 *, StreamingContext_t986364934 , const MethodInfo*))SerializableDictionary_2__ctor_m2587960425_gshared)(__this, ___info0, ___context1, method)
// System.Xml.Schema.XmlSchema SerializableDictionary`2<System.Object,System.Object>::GetSchema()
extern "C"  XmlSchema_t1932230565 * SerializableDictionary_2_GetSchema_m992727535_gshared (SerializableDictionary_2_t2269426764 * __this, const MethodInfo* method);
#define SerializableDictionary_2_GetSchema_m992727535(__this, method) ((  XmlSchema_t1932230565 * (*) (SerializableDictionary_2_t2269426764 *, const MethodInfo*))SerializableDictionary_2_GetSchema_m992727535_gshared)(__this, method)
// System.Void SerializableDictionary`2<System.Object,System.Object>::ReadXml(System.Xml.XmlReader)
extern "C"  void SerializableDictionary_2_ReadXml_m3137948131_gshared (SerializableDictionary_2_t2269426764 * __this, XmlReader_t4229084514 * ___reader0, const MethodInfo* method);
#define SerializableDictionary_2_ReadXml_m3137948131(__this, ___reader0, method) ((  void (*) (SerializableDictionary_2_t2269426764 *, XmlReader_t4229084514 *, const MethodInfo*))SerializableDictionary_2_ReadXml_m3137948131_gshared)(__this, ___reader0, method)
// System.Void SerializableDictionary`2<System.Object,System.Object>::WriteXml(System.Xml.XmlWriter)
extern "C"  void SerializableDictionary_2_WriteXml_m3811106720_gshared (SerializableDictionary_2_t2269426764 * __this, XmlWriter_t89522450 * ___writer0, const MethodInfo* method);
#define SerializableDictionary_2_WriteXml_m3811106720(__this, ___writer0, method) ((  void (*) (SerializableDictionary_2_t2269426764 *, XmlWriter_t89522450 *, const MethodInfo*))SerializableDictionary_2_WriteXml_m3811106720_gshared)(__this, ___writer0, method)
