﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.ErrorCallback
struct ErrorCallback_t582108558;
// System.Object
struct Il2CppObject;
// PlayFab.PlayFabError
struct PlayFabError_t750598646;
// System.IAsyncResult
struct IAsyncResult_t537683269;
// System.AsyncCallback
struct AsyncCallback_t1363551830;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_IntPtr676692020.h"
#include "AssemblyU2DCSharpU2Dfirstpass_PlayFab_PlayFabError750598646.h"
#include "mscorlib_System_AsyncCallback1363551830.h"

// System.Void PlayFab.ErrorCallback::.ctor(System.Object,System.IntPtr)
extern "C"  void ErrorCallback__ctor_m2874342461 (ErrorCallback_t582108558 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ErrorCallback::Invoke(PlayFab.PlayFabError)
extern "C"  void ErrorCallback_Invoke_m2764045351 (ErrorCallback_t582108558 * __this, PlayFabError_t750598646 * ___error0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
extern "C" void pinvoke_delegate_wrapper_ErrorCallback_t582108558(Il2CppObject* delegate, PlayFabError_t750598646 * ___error0);
// System.IAsyncResult PlayFab.ErrorCallback::BeginInvoke(PlayFab.PlayFabError,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * ErrorCallback_BeginInvoke_m2638290106 (ErrorCallback_t582108558 * __this, PlayFabError_t750598646 * ___error0, AsyncCallback_t1363551830 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ErrorCallback::EndInvoke(System.IAsyncResult)
extern "C"  void ErrorCallback_EndInvoke_m2220889293 (ErrorCallback_t582108558 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
