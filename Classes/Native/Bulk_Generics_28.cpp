﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

// UniRx.Operators.SelectObservable`2/Select<System.Int64,System.Object>
struct Select_t133091173;
// UniRx.Operators.SelectObservable`2<System.Int64,System.Object>
struct SelectObservable_2_t1317503851;
// UniRx.IObserver`1<System.Object>
struct IObserver_1_t3049105323;
// System.IDisposable
struct IDisposable_t1628921374;
// System.Exception
struct Exception_t1967233988;
// UniRx.Operators.SelectObservable`2/Select<System.Int64,UnityEngine.Vector2>
struct Select_t2821314541;
// UniRx.Operators.SelectObservable`2<System.Int64,UnityEngine.Vector2>
struct SelectObservable_2_t4005727219;
// UniRx.IObserver`1<UnityEngine.Vector2>
struct IObserver_1_t1442361395;
// UniRx.Operators.SelectObservable`2/Select<System.Object,System.Boolean>
struct Select_t3060303092;
// UniRx.Operators.SelectObservable`2<System.Object,System.Boolean>
struct SelectObservable_2_t4244715770;
// UniRx.IObserver`1<System.Boolean>
struct IObserver_1_t2423004244;
// System.Object
struct Il2CppObject;
// UniRx.Operators.SelectObservable`2/Select<System.Object,System.Object>
struct Select_t3686404171;
// UniRx.Operators.SelectObservable`2<System.Object,System.Object>
struct SelectObservable_2_t575849553;
// UniRx.Operators.SelectObservable`2<System.Int32,UnityEngine.Color>
struct SelectObservable_2_t3125378242;
// UniRx.IObservable`1<System.Int32>
struct IObservable_1_t2606213151;
// System.Func`2<System.Int32,UnityEngine.Color>
struct Func_2_t390344745;
// System.Func`3<System.Int32,System.Int32,UnityEngine.Color>
struct Func_3_t3578830837;
// UniRx.IObserver`1<UnityEngine.Color>
struct IObserver_1_t3800174663;
// UniRx.Operators.SelectObservable`2<System.Int64,System.Boolean>
struct SelectObservable_2_t691402772;
// UniRx.IObservable`1<System.Int64>
struct IObservable_1_t2606213246;
// System.Func`2<System.Int64,System.Boolean>
struct Func_2_t2251336571;
// System.Func`3<System.Int64,System.Int32,System.Boolean>
struct Func_3_t1612416521;
// UniRx.Operators.SelectObservable`2<System.Int64,System.Int32>
struct SelectObservable_2_t3327812218;
// System.Func`2<System.Int64,System.Int32>
struct Func_2_t592778721;
// System.Func`3<System.Int64,System.Int32,System.Int32>
struct Func_3_t4248825967;
// UniRx.IObserver`1<System.Int32>
struct IObserver_1_t764446394;
// System.Func`2<System.Int64,System.Object>
struct Func_2_t2877437650;
// System.Func`3<System.Int64,System.Int32,System.Object>
struct Func_3_t2238517600;
// System.Func`2<System.Int64,UnityEngine.Vector2>
struct Func_2_t1270693722;
// System.Func`3<System.Int64,System.Int32,UnityEngine.Vector2>
struct Func_3_t631773672;
// UniRx.IObservable`1<System.Object>
struct IObservable_1_t595904784;
// System.Func`2<System.Object,System.Boolean>
struct Func_2_t1509682273;
// System.Func`3<System.Object,System.Int32,System.Boolean>
struct Func_3_t3064567499;
// System.Func`2<System.Object,System.Object>
struct Func_2_t2135783352;
// System.Func`3<System.Object,System.Int32,System.Object>
struct Func_3_t3690668578;
// UniRx.Operators.SingleObservable`1/Single_<System.Object>
struct Single__t984311384;
// UniRx.Operators.SingleObservable`1<System.Object>
struct SingleObservable_1_t427280954;
// UniRx.Operators.SingleObservable`1/Single<System.Object>
struct Single_t2700140449;
// UniRx.Operators.SkipObservable`1/Skip_<System.Boolean>
struct Skip__t1751828930;
// UniRx.Operators.SkipObservable`1<System.Boolean>
struct SkipObservable_1_t197279106;
// UniRx.Operators.SkipObservable`1/Skip_<System.Object>
struct Skip__t2377930009;
// UniRx.Operators.SkipObservable`1<System.Object>
struct SkipObservable_1_t823380185;
// UniRx.Operators.SkipObservable`1/Skip<System.Boolean>
struct Skip_t1564805481;
// UniRx.Operators.SkipObservable`1/Skip<System.Object>
struct Skip_t2190906560;
// UniRx.IObservable`1<System.Boolean>
struct IObservable_1_t4264771001;
// UniRx.IScheduler
struct IScheduler_t2938318244;
// UniRx.Operators.SkipUntilObservable`2/SkipUntilOuterObserver/SkipUntil<System.Object,System.Object>
struct SkipUntil_t2756386132;
// UniRx.Operators.SkipUntilObservable`2/SkipUntilOuterObserver<System.Object,System.Object>
struct SkipUntilOuterObserver_t2866202317;
// UniRx.Operators.SkipUntilObservable`2/SkipUntilOuterObserver/SkipUntilOther<System.Object,System.Object>
struct SkipUntilOther_t2413447818;
// UniRx.Operators.SkipUntilObservable`2<System.Object,System.Object>
struct SkipUntilObservable_2_t2740161498;
// UniRx.Operators.SkipWhileObservable`1/SkipWhile_<System.Object>
struct SkipWhile__t3820853950;
// UniRx.Operators.SkipWhileObservable`1<System.Object>
struct SkipWhileObservable_1_t2542074388;
// UniRx.Operators.SkipWhileObservable`1/SkipWhile<System.Object>
struct SkipWhile_t3761473147;
// UniRx.Operators.StartObservable`1/StartObserver<System.Int32>
struct StartObserver_t2832445184;
// UniRx.Operators.StartObservable`1<System.Int32>
struct StartObservable_1_t77870867;
// UniRx.Operators.StartObservable`1/StartObserver<System.Object>
struct StartObserver_t822136817;
// UniRx.Operators.StartObservable`1<System.Object>
struct StartObservable_1_t2362529796;
// UniRx.Operators.StartObservable`1/StartObserver<UniRx.Unit>
struct StartObserver_t2543316435;
// UniRx.Operators.StartObservable`1<UniRx.Unit>
struct StartObservable_1_t4083709414;
// UniRx.IObserver`1<UniRx.Unit>
struct IObserver_1_t475317645;
// System.Func`1<System.Int32>
struct Func_1_t3990196034;
// System.Action
struct Action_t437523947;
// System.Func`1<System.Object>
struct Func_1_t1979887667;
// System.Func`1<UniRx.Unit>
struct Func_1_t3701067285;
// UniRx.Operators.StartWithObservable`1/StartWith<System.Int32>
struct StartWith_t1733935216;
// UniRx.Operators.StartWithObservable`1<System.Int32>
struct StartWithObservable_1_t338430729;
// UniRx.Operators.StartWithObservable`1/StartWith<System.Object>
struct StartWith_t4018594145;
// UniRx.Operators.StartWithObservable`1<System.Object>
struct StartWithObservable_1_t2623089658;
// UniRx.Operators.SubscribeOnMainThreadObservable`1/<SubscribeCore>c__AnonStorey94<System.Object>
struct U3CSubscribeCoreU3Ec__AnonStorey94_t141655650;
// UniRx.Operators.SubscribeOnMainThreadObservable`1<System.Object>
struct SubscribeOnMainThreadObservable_1_t1377860062;
// System.Action`1<System.Int64>
struct Action_1_t2995867587;
// UniRx.Operators.SubscribeOnObservable`1/<SubscribeCore>c__AnonStorey69<System.Object>
struct U3CSubscribeCoreU3Ec__AnonStorey69_t3806984138;
// UniRx.Operators.SubscribeOnObservable`1<System.Object>
struct SubscribeOnObservable_1_t2329329235;
// UniRx.Operators.SwitchObservable`1/SwitchObserver/Switch<System.Object>
struct Switch_t874284973;
// UniRx.Operators.SwitchObservable`1/SwitchObserver<System.Object>
struct SwitchObserver_t321759539;
// UniRx.Operators.SwitchObservable`1<System.Object>
struct SwitchObservable_1_t1854652486;
// UniRx.IObservable`1<UniRx.IObservable`1<System.Object>>
struct IObservable_1_t354703148;
// UniRx.Operators.SynchronizedObserver`1<System.Object>
struct SynchronizedObserver_1_t2847152769;
// UniRx.Operators.SynchronizeObservable`1/Synchronize<System.Object>
struct Synchronize_t2861865257;
// UniRx.Operators.SynchronizeObservable`1<System.Object>
struct SynchronizeObservable_1_t736720834;
// UniRx.Operators.TakeLastObservable`1/TakeLast_<System.Object>
struct TakeLast__t2176345419;
// UniRx.Operators.TakeLastObservable`1<System.Object>
struct TakeLastObservable_1_t3265267047;
// UniRx.Operators.TakeLastObservable`1/TakeLast<System.Object>
struct TakeLast_t798930510;
// UniRx.Operators.TakeObservable`1/Take_<System.Int64>
struct Take__t183614399;
// UniRx.Operators.TakeObservable`1<System.Int64>
struct TakeObservable_1_t794160655;
// UniRx.IObserver`1<System.Int64>
struct IObserver_1_t764446489;
// UniRx.Operators.TakeObservable`1/Take_<System.Object>
struct Take__t2468273233;
// UniRx.Operators.TakeObservable`1<System.Object>
struct TakeObservable_1_t3078819489;
// UniRx.Operators.TakeObservable`1/Take_<UniRx.Unit>
struct Take__t4189452851;
// UniRx.Operators.TakeObservable`1<UniRx.Unit>
struct TakeObservable_1_t505031811;
// UniRx.Operators.TakeObservable`1/Take<System.Int64>
struct Take_t2541561334;
// UniRx.Operators.TakeObservable`1/Take<System.Object>
struct Take_t531252872;
// UniRx.Operators.TakeObservable`1/Take<UniRx.Unit>
struct Take_t2252432490;
// UniRx.IObservable`1<UniRx.Unit>
struct IObservable_1_t2317084402;
// UniRx.Operators.TakeUntilObservable`2/TakeUntil/TakeUntilOther<System.Object,System.Object>
struct TakeUntilOther_t1704800930;
// UniRx.Operators.TakeUntilObservable`2/TakeUntil<System.Object,System.Object>
struct TakeUntil_t2678949436;
// UniRx.Operators.TakeUntilObservable`2/TakeUntil/TakeUntilOther<System.Object,UniRx.Unit>
struct TakeUntilOther_t3425980548;
// UniRx.Operators.TakeUntilObservable`2/TakeUntil<System.Object,UniRx.Unit>
struct TakeUntil_t105161758;
// UniRx.Operators.TakeUntilObservable`2/TakeUntil/TakeUntilOther<UniRx.Unit,UniRx.Unit>
struct TakeUntilOther_t387441882;
// UniRx.Operators.TakeUntilObservable`2/TakeUntil<UniRx.Unit,UniRx.Unit>
struct TakeUntil_t1361590388;
// UniRx.Operators.TakeUntilObservable`2<System.Object,System.Object>
struct TakeUntilObservable_2_t4246198466;
// UniRx.Operators.TakeUntilObservable`2<System.Object,UniRx.Unit>
struct TakeUntilObservable_2_t1672410788;
// UniRx.Operators.TakeUntilObservable`2<UniRx.Unit,UniRx.Unit>
struct TakeUntilObservable_2_t2928839418;
// UniRx.Operators.TakeWhileObservable`1/TakeWhile_<System.Object>
struct TakeWhile__t2842089094;
// UniRx.Operators.TakeWhileObservable`1<System.Object>
struct TakeWhileObservable_1_t3525448012;
// UniRx.Operators.TakeWhileObservable`1/TakeWhile<System.Object>
struct TakeWhile_t3591352755;
// UniRx.Operators.ThrottleFirstFrameObservable`1/ThrottleFirstFrame/ThrottleFirstFrameTick<System.Object>
struct ThrottleFirstFrameTick_t2464530589;
// UniRx.Operators.ThrottleFirstFrameObservable`1/ThrottleFirstFrame<System.Object>
struct ThrottleFirstFrame_t517244520;
// UniRx.Operators.ThrottleFirstFrameObservable`1<System.Object>
struct ThrottleFirstFrameObservable_1_t955022977;
// UniRx.Operators.ThrottleFirstObservable`1/ThrottleFirst<System.Object>
struct ThrottleFirst_t4236367119;
// UniRx.Operators.ThrottleFirstObservable`1<System.Object>
struct ThrottleFirstObservable_1_t2123238056;
// UniRx.Operators.ThrottleFrameObservable`1/ThrottleFrame/ThrottleFrameTick<System.Object>
struct ThrottleFrameTick_t1733702649;
// UniRx.Operators.ThrottleFrameObservable`1/ThrottleFrame<System.Object>
struct ThrottleFrame_t3629975492;
// UniRx.Operators.ThrottleFrameObservable`1<System.Object>
struct ThrottleFrameObservable_1_t2748437981;
// UniRx.Operators.ThrottleObservable`1/Throttle/<OnNext>c__AnonStorey6A<System.Int64>
struct U3COnNextU3Ec__AnonStorey6A_t4267933177;
// UniRx.Operators.ThrottleObservable`1/Throttle/<OnNext>c__AnonStorey6A<System.Object>
struct U3COnNextU3Ec__AnonStorey6A_t2257624715;
// UniRx.Operators.ThrottleObservable`1/Throttle<System.Int64>
struct Throttle_t1826605473;
// UniRx.Operators.ThrottleObservable`1<System.Int64>
struct ThrottleObservable_1_t3227609402;
// UniRx.Operators.ThrottleObservable`1/Throttle<System.Object>
struct Throttle_t4111264307;
// UniRx.Operators.ThrottleObservable`1<System.Object>
struct ThrottleObservable_1_t1217300940;
// UniRx.Operators.ThrowObservable`1/<SubscribeCore>c__AnonStorey6B<System.Object>
struct U3CSubscribeCoreU3Ec__AnonStorey6B_t1626282475;
// UniRx.Operators.ThrowObservable`1/<SubscribeCore>c__AnonStorey6B<UniRx.Unit>
struct U3CSubscribeCoreU3Ec__AnonStorey6B_t3347462093;
// UniRx.Operators.ThrowObservable`1/Throw<System.Object>
struct Throw_t4151912527;
// UniRx.Operators.ThrowObservable`1/Throw<UniRx.Unit>
struct Throw_t1578124849;
// UniRx.Operators.ThrowObservable`1<System.Object>
struct ThrowObservable_1_t3672276456;
// UniRx.Operators.ThrowObservable`1<UniRx.Unit>
struct ThrowObservable_1_t1098488778;
// UniRx.Operators.TimeIntervalObservable`1/TimeInterval<System.Object>
struct TimeInterval_t3041115259;
// UniRx.Operators.TimeIntervalObservable`1<System.Object>
struct TimeIntervalObservable_1_t4159975444;
// UniRx.IObserver`1<UniRx.TimeInterval`1<System.Object>>
struct IObserver_1_t2920064445;
// UniRx.Operators.TimeoutFrameObservable`1/TimeoutFrame/TimeoutFrameTick<System.Object>
struct TimeoutFrameTick_t1345043194;
// UniRx.Operators.TimeoutFrameObservable`1/TimeoutFrame<System.Object>
struct TimeoutFrame_t2437429061;
// UniRx.Operators.TimeoutFrameObservable`1<System.Object>
struct TimeoutFrameObservable_1_t2457127902;
// UniRx.Operators.TimeoutObservable`1/Timeout/<RunTimer>c__AnonStorey6C<System.Object>
struct U3CRunTimerU3Ec__AnonStorey6C_t4277735173;
// UniRx.Operators.TimeoutObservable`1/Timeout_<System.Object>
struct Timeout__t2369373703;
// UniRx.Operators.TimeoutObservable`1<System.Object>
struct TimeoutObservable_1_t1895307051;
// UniRx.Operators.TimeoutObservable`1/Timeout<System.Object>
struct Timeout_t2052083218;

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array2840145358.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Selec133091173.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Selec133091173MethodDeclarations.h"
#include "mscorlib_System_Void2779279689.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Sele1317503851.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Oper1929422447MethodDeclarations.h"
#include "mscorlib_System_Int642847414882.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_Exception1967233988.h"
#include "System_Core_System_Func_2_gen2877437650.h"
#include "System_Core_System_Func_2_gen2877437650MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Oper1929422447.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Sele2821314541.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Sele2821314541MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Sele4005727219.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Opera322678519MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Vector23525329788.h"
#include "System_Core_System_Func_2_gen1270693722.h"
#include "System_Core_System_Func_2_gen1270693722MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Opera322678519.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Sele3060303092.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Sele3060303092MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Sele4244715770.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Opera561667070MethodDeclarations.h"
#include "mscorlib_System_Boolean211005341.h"
#include "System_Core_System_Func_2_gen1509682273.h"
#include "System_Core_System_Func_2_gen1509682273MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Opera561667070.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Sele3686404171.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Sele3686404171MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Selec575849553.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Oper1187768149MethodDeclarations.h"
#include "System_Core_System_Func_2_gen2135783352.h"
#include "System_Core_System_Func_2_gen2135783352MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Oper1187768149.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Sele3125378242.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Sele3125378242MethodDeclarations.h"
#include "System_Core_System_Func_2_gen390344745.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_OptimizedObser2379048080MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_OptimizedObser2379048080.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Opera652320731MethodDeclarations.h"
#include "System_Core_System_Func_3_gen3578830837.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Sele1940965564.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Sele1940965564MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Sele1427971985.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Sele1427971985MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Selec691402772.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Selec691402772MethodDeclarations.h"
#include "System_Core_System_Func_2_gen2251336571.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Oper3570117608MethodDeclarations.h"
#include "System_Core_System_Func_3_gen1612416521.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Sele3801957390.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Sele3801957390MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Sele3288963811.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Sele3288963811MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Sele3327812218.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Sele3327812218MethodDeclarations.h"
#include "System_Core_System_Func_2_gen592778721.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Oper1911559758MethodDeclarations.h"
#include "System_Core_System_Func_3_gen4248825967.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Sele2143399540.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Sele2143399540MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Sele1630405961.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Sele1630405961MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Sele1317503851MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Oper4196218687MethodDeclarations.h"
#include "System_Core_System_Func_3_gen2238517600.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Sele3915064890.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Sele3915064890MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Sele4005727219MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Oper2589474759MethodDeclarations.h"
#include "System_Core_System_Func_3_gen631773672.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Sele2308320962.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Sele2308320962MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Sele4244715770MethodDeclarations.h"
#include "System_Core_System_Func_3_gen3064567499.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Sele2547309513.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Sele2547309513MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Selec575849553MethodDeclarations.h"
#include "System_Core_System_Func_3_gen3690668578.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Sele3173410592.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Sele3173410592MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Singl984311384.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Singl984311384MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Singl427280954.h"
#include "mscorlib_System_InvalidOperationException2420574324MethodDeclarations.h"
#include "mscorlib_System_String968488902.h"
#include "mscorlib_System_InvalidOperationException2420574324.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Sing2700140449.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Sing2700140449MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Singl427280954MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Skip1751828930.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Skip1751828930MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_SkipO197279106.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Operat60103313MethodDeclarations.h"
#include "System_Core_System_Action437523947MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_StableComposit3433635614MethodDeclarations.h"
#include "mscorlib_System_TimeSpan763862892.h"
#include "System_Core_System_Action437523947.h"
#include "mscorlib_System_IntPtr676692020.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Operat60103313.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Skip2377930009.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Skip2377930009MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_SkipO823380185.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Skip1564805481.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Skip1564805481MethodDeclarations.h"
#include "mscorlib_System_Int322847414787.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Skip2190906560.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Skip2190906560MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_SkipO197279106MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Scheduler103934925.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Scheduler103934925MethodDeclarations.h"
#include "mscorlib_System_TimeSpan763862892MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_SkipO823380185MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Skip2756386132.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Skip2756386132MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Skip2866202317.h"
#include "mscorlib_System_Object837106420MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_Em246459410.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_Em246459410MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Skip2413447818.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Skip2413447818MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Skip2866202317MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Skip2740161498.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_SingleAssignme2336378823MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_SingleAssignme2336378823.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Skip2740161498MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Skip3820853950.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Skip3820853950MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Skip2542074388.h"
#include "System_Core_System_Func_3_gen3064567499MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Skip3761473147.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Skip3761473147MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Skip2542074388MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Star2832445184.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Star2832445184MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_StartO77870867.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Opera701568569MethodDeclarations.h"
#include "System_Core_System_Func_1_gen3990196034.h"
#include "System_Core_System_Func_1_gen3990196034MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Opera701568569.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Start822136817.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Start822136817MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Star2362529796.h"
#include "System_Core_System_Func_1_gen1979887667.h"
#include "System_Core_System_Func_1_gen1979887667MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Star2543316435.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Star2543316435MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Star4083709414.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Oper4165376397MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Unit2558286038.h"
#include "System_Core_System_Func_1_gen3701067285.h"
#include "System_Core_System_Func_1_gen3701067285MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Oper4165376397.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_StartO77870867MethodDeclarations.h"
#include "mscorlib_System_Nullable_1_gen3649900800.h"
#include "mscorlib_System_Nullable_1_gen3649900800MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Star2362529796MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Star4083709414MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Oper1622431009MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Star1733935216.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Star1733935216MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Start338430729.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Disposable3736388786.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Disposable3736388786MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Star4018594145.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Star4018594145MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Star2623089658.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Start338430729MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Star2623089658MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Subsc141655650.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Subsc141655650MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_SerialDisposab2547852742MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_SerialDisposab2547852742.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Subs1377860062.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Subs1377860062MethodDeclarations.h"
#include "mscorlib_System_Action_1_gen2995867587MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_ObservableExte3095004425MethodDeclarations.h"
#include "mscorlib_System_Action_1_gen2995867587.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_ObservableExte3095004425.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Subs3806984138.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Subs3806984138MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_ScheduledDispo2058426623MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Subs2329329235.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_ScheduledDispo2058426623.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Subs2329329235MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Switc874284973.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Switc874284973MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Switc321759539.h"
#include "mscorlib_System_UInt64985925421.h"
#include "mscorlib_System_Threading_Monitor2071304733MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Oper3381670217.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Switc321759539MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Swit1854652486.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Oper3381670217MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Swit1854652486MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Sync2847152769.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Sync2847152769MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Sync2861865257.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Sync2861865257MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Synch736720834.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Synch736720834MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Take2176345419.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Take2176345419MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Take3265267047.h"
#include "System_System_Collections_Generic_Queue_1_gen2416153082.h"
#include "System_System_Collections_Generic_Queue_1_gen2416153082MethodDeclarations.h"
#include "mscorlib_System_DateTimeOffset3712260035.h"
#include "mscorlib_System_DateTimeOffset3712260035MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_TimeInterval_1_708065542.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_TimeInterval_1_708065542MethodDeclarations.h"
#include "System_System_Collections_Generic_Queue_1_Enumerat3885774799.h"
#include "System_System_Collections_Generic_Queue_1_Enumerat3885774799MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_TakeL798930510.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_TakeL798930510MethodDeclarations.h"
#include "System_System_Collections_Generic_Queue_1_gen2545193960.h"
#include "System_System_Collections_Generic_Queue_1_gen2545193960MethodDeclarations.h"
#include "System_System_Collections_Generic_Queue_1_Enumerat4014815677.h"
#include "System_System_Collections_Generic_Queue_1_Enumerat4014815677MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Take3265267047MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_TakeO183614399.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_TakeO183614399MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_TakeO794160655.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Oper3939730909MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Oper3939730909.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Take2468273233.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Take2468273233MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Take3078819489.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Take4189452851.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Take4189452851MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_TakeO505031811.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Take2541561334.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Take2541561334MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_TakeO531252872.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_TakeO531252872MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Take2252432490.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Take2252432490MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_TakeO794160655MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Oper1911559853MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Take3078819489MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_TakeO505031811MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Take1704800930.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Take1704800930MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Take2678949436.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Take3425980548.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Take3425980548MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_TakeU105161758.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_TakeU387441882.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_TakeU387441882MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Take1361590388.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Take2678949436MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Take4246198466.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_TakeU105161758MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Take1672410788.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Take1361590388MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Take2928839418.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Take4246198466MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Take1672410788MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Take2928839418MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Take2842089094.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Take2842089094MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Take3525448012.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Take3591352755.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Take3591352755MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Take3525448012MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Thro2464530589.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Thro2464530589MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Throt517244520.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Throt517244520MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Throt955022977.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Observable258071701MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_FrameCountType167759182.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Throt955022977MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Thro4236367119.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Thro4236367119MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Thro2123238056.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Thro2123238056MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Thro1733702649.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Thro1733702649MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Thro3629975492.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Thro3629975492MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Thro2748437981.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Thro2748437981MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Thro4267933177.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Thro4267933177MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Thro1826605473.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Thro1826605473MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Thro2257624715.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Thro2257624715MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Thro4111264307.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Thro4111264307MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Thro3227609402.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Thro1217300940.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Thro3227609402MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Thro1217300940MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Thro1626282475.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Thro1626282475MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Thro3672276456.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Thro3347462093.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Thro3347462093MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Thro1098488778.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Thro4151912527.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Thro4151912527MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Thro1578124849.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Thro1578124849MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Thro3672276456MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Thro1098488778MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Time3041115259.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Time3041115259MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Time4159975444.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Oper1058727271MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Oper1058727271.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Time4159975444MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Oper4067177809MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Time1345043194.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Time1345043194MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Time2437429061.h"
#include "mscorlib_System_TimeoutException2903576483MethodDeclarations.h"
#include "mscorlib_System_TimeoutException2903576483.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Time2437429061MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Time2457127902.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Time2457127902MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Time4277735173.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Time4277735173MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Time2052083218.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Time2369373703.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Time2369373703MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Time1895307051.h"
#include "mscorlib_System_Nullable_1_gen2303330647MethodDeclarations.h"
#include "mscorlib_System_Nullable_1_gen2303330647.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Time2052083218MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Time1895307051MethodDeclarations.h"

// System.Boolean UniRx.OptimizedObservableExtensions::IsRequiredSubscribeOnCurrentThread<System.Int32>(UniRx.IObservable`1<!!0>)
extern "C"  bool OptimizedObservableExtensions_IsRequiredSubscribeOnCurrentThread_TisInt32_t2847414787_m3895999500_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* p0, const MethodInfo* method);
#define OptimizedObservableExtensions_IsRequiredSubscribeOnCurrentThread_TisInt32_t2847414787_m3895999500(__this /* static, unused */, p0, method) ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))OptimizedObservableExtensions_IsRequiredSubscribeOnCurrentThread_TisInt32_t2847414787_m3895999500_gshared)(__this /* static, unused */, p0, method)
// System.Boolean UniRx.OptimizedObservableExtensions::IsRequiredSubscribeOnCurrentThread<System.Int64>(UniRx.IObservable`1<!!0>)
extern "C"  bool OptimizedObservableExtensions_IsRequiredSubscribeOnCurrentThread_TisInt64_t2847414882_m2547913869_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* p0, const MethodInfo* method);
#define OptimizedObservableExtensions_IsRequiredSubscribeOnCurrentThread_TisInt64_t2847414882_m2547913869(__this /* static, unused */, p0, method) ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))OptimizedObservableExtensions_IsRequiredSubscribeOnCurrentThread_TisInt64_t2847414882_m2547913869_gshared)(__this /* static, unused */, p0, method)
// System.Boolean UniRx.OptimizedObservableExtensions::IsRequiredSubscribeOnCurrentThread<System.Object>(UniRx.IObservable`1<!!0>)
extern "C"  bool OptimizedObservableExtensions_IsRequiredSubscribeOnCurrentThread_TisIl2CppObject_m1909184259_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* p0, const MethodInfo* method);
#define OptimizedObservableExtensions_IsRequiredSubscribeOnCurrentThread_TisIl2CppObject_m1909184259(__this /* static, unused */, p0, method) ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))OptimizedObservableExtensions_IsRequiredSubscribeOnCurrentThread_TisIl2CppObject_m1909184259_gshared)(__this /* static, unused */, p0, method)
// System.Boolean UniRx.OptimizedObservableExtensions::IsRequiredSubscribeOnCurrentThread<System.Boolean>(UniRx.IObservable`1<!!0>)
extern "C"  bool OptimizedObservableExtensions_IsRequiredSubscribeOnCurrentThread_TisBoolean_t211005341_m545890482_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* p0, const MethodInfo* method);
#define OptimizedObservableExtensions_IsRequiredSubscribeOnCurrentThread_TisBoolean_t211005341_m545890482(__this /* static, unused */, p0, method) ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))OptimizedObservableExtensions_IsRequiredSubscribeOnCurrentThread_TisBoolean_t211005341_m545890482_gshared)(__this /* static, unused */, p0, method)
// System.IDisposable UniRx.ObservableExtensions::Subscribe<System.Int64>(UniRx.IObservable`1<!!0>,System.Action`1<!!0>)
extern "C"  Il2CppObject * ObservableExtensions_Subscribe_TisInt64_t2847414882_m1751770470_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* p0, Action_1_t2995867587 * p1, const MethodInfo* method);
#define ObservableExtensions_Subscribe_TisInt64_t2847414882_m1751770470(__this /* static, unused */, p0, p1, method) ((  Il2CppObject * (*) (Il2CppObject * /* static, unused */, Il2CppObject*, Action_1_t2995867587 *, const MethodInfo*))ObservableExtensions_Subscribe_TisInt64_t2847414882_m1751770470_gshared)(__this /* static, unused */, p0, p1, method)
// System.Boolean UniRx.OptimizedObservableExtensions::IsRequiredSubscribeOnCurrentThread<UniRx.Unit>(UniRx.IObservable`1<!!0>)
extern "C"  bool OptimizedObservableExtensions_IsRequiredSubscribeOnCurrentThread_TisUnit_t2558286038_m849542303_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* p0, const MethodInfo* method);
#define OptimizedObservableExtensions_IsRequiredSubscribeOnCurrentThread_TisUnit_t2558286038_m849542303(__this /* static, unused */, p0, method) ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))OptimizedObservableExtensions_IsRequiredSubscribeOnCurrentThread_TisUnit_t2558286038_m849542303_gshared)(__this /* static, unused */, p0, method)
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void UniRx.Operators.SelectObservable`2/Select<System.Int64,System.Object>::.ctor(UniRx.Operators.SelectObservable`2<T,TR>,UniRx.IObserver`1<TR>,System.IDisposable)
extern "C"  void Select__ctor_m3681417166_gshared (Select_t133091173 * __this, SelectObservable_2_t1317503851 * ___parent0, Il2CppObject* ___observer1, Il2CppObject * ___cancel2, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = ___observer1;
		Il2CppObject * L_1 = ___cancel2;
		NullCheck((OperatorObserverBase_2_t1929422447 *)__this);
		((  void (*) (OperatorObserverBase_2_t1929422447 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((OperatorObserverBase_2_t1929422447 *)__this, (Il2CppObject*)L_0, (Il2CppObject *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		SelectObservable_2_t1317503851 * L_2 = ___parent0;
		__this->set_parent_2(L_2);
		return;
	}
}
// System.Void UniRx.Operators.SelectObservable`2/Select<System.Int64,System.Object>::OnNext(T)
extern Il2CppClass* Il2CppObject_il2cpp_TypeInfo_var;
extern Il2CppClass* Exception_t1967233988_il2cpp_TypeInfo_var;
extern const uint32_t Select_OnNext_m2827718266_MetadataUsageId;
extern "C"  void Select_OnNext_m2827718266_gshared (Select_t133091173 * __this, int64_t ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Select_OnNext_m2827718266_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Il2CppObject * V_0 = NULL;
	Exception_t1967233988 * V_1 = NULL;
	Il2CppObject * V_2 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Initobj (Il2CppObject_il2cpp_TypeInfo_var, (&V_2));
		Il2CppObject * L_0 = V_2;
		V_0 = (Il2CppObject *)L_0;
	}

IL_000a:
	try
	{ // begin try (depth: 1)
		SelectObservable_2_t1317503851 * L_1 = (SelectObservable_2_t1317503851 *)__this->get_parent_2();
		NullCheck(L_1);
		Func_2_t2877437650 * L_2 = (Func_2_t2877437650 *)L_1->get_selector_2();
		int64_t L_3 = ___value0;
		NullCheck((Func_2_t2877437650 *)L_2);
		Il2CppObject * L_4 = ((  Il2CppObject * (*) (Func_2_t2877437650 *, int64_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)((Func_2_t2877437650 *)L_2, (int64_t)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		V_0 = (Il2CppObject *)L_4;
		goto IL_0046;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1967233988 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Exception_t1967233988_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_0021;
		throw e;
	}

CATCH_0021:
	{ // begin catch(System.Exception)
		{
			V_1 = (Exception_t1967233988 *)((Exception_t1967233988 *)__exception_local);
		}

IL_0022:
		try
		{ // begin try (depth: 2)
			Il2CppObject* L_5 = (Il2CppObject*)((OperatorObserverBase_2_t1929422447 *)__this)->get_observer_0();
			il2cpp_codegen_memory_barrier();
			Exception_t1967233988 * L_6 = V_1;
			NullCheck((Il2CppObject*)L_5);
			InterfaceActionInvoker1< Exception_t1967233988 * >::Invoke(1 /* System.Void UniRx.IObserver`1<System.Object>::OnError(System.Exception) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3), (Il2CppObject*)L_5, (Exception_t1967233988 *)L_6);
			IL2CPP_LEAVE(0x3C, FINALLY_0035);
		} // end try (depth: 2)
		catch(Il2CppExceptionWrapper& e)
		{
			__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
			goto FINALLY_0035;
		}

FINALLY_0035:
		{ // begin finally (depth: 2)
			NullCheck((OperatorObserverBase_2_t1929422447 *)__this);
			VirtActionInvoker0::Invoke(4 /* System.Void UniRx.Operators.OperatorObserverBase`2<System.Int64,System.Object>::Dispose() */, (OperatorObserverBase_2_t1929422447 *)__this);
			IL2CPP_END_FINALLY(53)
		} // end finally (depth: 2)
		IL2CPP_CLEANUP(53)
		{
			IL2CPP_JUMP_TBL(0x3C, IL_003c)
			IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
		}

IL_003c:
		{
			goto IL_0054;
		}

IL_0041:
		{
			; // IL_0041: leave IL_0046
		}
	} // end catch (depth: 1)

IL_0046:
	{
		Il2CppObject* L_7 = (Il2CppObject*)((OperatorObserverBase_2_t1929422447 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		Il2CppObject * L_8 = V_0;
		NullCheck((Il2CppObject*)L_7);
		InterfaceActionInvoker1< Il2CppObject * >::Invoke(2 /* System.Void UniRx.IObserver`1<System.Object>::OnNext(T) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3), (Il2CppObject*)L_7, (Il2CppObject *)L_8);
	}

IL_0054:
	{
		return;
	}
}
// System.Void UniRx.Operators.SelectObservable`2/Select<System.Int64,System.Object>::OnError(System.Exception)
extern "C"  void Select_OnError_m3353834889_gshared (Select_t133091173 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method)
{
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		Il2CppObject* L_0 = (Il2CppObject*)((OperatorObserverBase_2_t1929422447 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		Exception_t1967233988 * L_1 = ___error0;
		NullCheck((Il2CppObject*)L_0);
		InterfaceActionInvoker1< Exception_t1967233988 * >::Invoke(1 /* System.Void UniRx.IObserver`1<System.Object>::OnError(System.Exception) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3), (Il2CppObject*)L_0, (Exception_t1967233988 *)L_1);
		IL2CPP_LEAVE(0x1A, FINALLY_0013);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0013;
	}

FINALLY_0013:
	{ // begin finally (depth: 1)
		NullCheck((OperatorObserverBase_2_t1929422447 *)__this);
		VirtActionInvoker0::Invoke(4 /* System.Void UniRx.Operators.OperatorObserverBase`2<System.Int64,System.Object>::Dispose() */, (OperatorObserverBase_2_t1929422447 *)__this);
		IL2CPP_END_FINALLY(19)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(19)
	{
		IL2CPP_JUMP_TBL(0x1A, IL_001a)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_001a:
	{
		return;
	}
}
// System.Void UniRx.Operators.SelectObservable`2/Select<System.Int64,System.Object>::OnCompleted()
extern "C"  void Select_OnCompleted_m2547740252_gshared (Select_t133091173 * __this, const MethodInfo* method)
{
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		Il2CppObject* L_0 = (Il2CppObject*)((OperatorObserverBase_2_t1929422447 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		NullCheck((Il2CppObject*)L_0);
		InterfaceActionInvoker0::Invoke(0 /* System.Void UniRx.IObserver`1<System.Object>::OnCompleted() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3), (Il2CppObject*)L_0);
		IL2CPP_LEAVE(0x19, FINALLY_0012);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0012;
	}

FINALLY_0012:
	{ // begin finally (depth: 1)
		NullCheck((OperatorObserverBase_2_t1929422447 *)__this);
		VirtActionInvoker0::Invoke(4 /* System.Void UniRx.Operators.OperatorObserverBase`2<System.Int64,System.Object>::Dispose() */, (OperatorObserverBase_2_t1929422447 *)__this);
		IL2CPP_END_FINALLY(18)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(18)
	{
		IL2CPP_JUMP_TBL(0x19, IL_0019)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0019:
	{
		return;
	}
}
// System.Void UniRx.Operators.SelectObservable`2/Select<System.Int64,UnityEngine.Vector2>::.ctor(UniRx.Operators.SelectObservable`2<T,TR>,UniRx.IObserver`1<TR>,System.IDisposable)
extern "C"  void Select__ctor_m4176532596_gshared (Select_t2821314541 * __this, SelectObservable_2_t4005727219 * ___parent0, Il2CppObject* ___observer1, Il2CppObject * ___cancel2, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = ___observer1;
		Il2CppObject * L_1 = ___cancel2;
		NullCheck((OperatorObserverBase_2_t322678519 *)__this);
		((  void (*) (OperatorObserverBase_2_t322678519 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((OperatorObserverBase_2_t322678519 *)__this, (Il2CppObject*)L_0, (Il2CppObject *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		SelectObservable_2_t4005727219 * L_2 = ___parent0;
		__this->set_parent_2(L_2);
		return;
	}
}
// System.Void UniRx.Operators.SelectObservable`2/Select<System.Int64,UnityEngine.Vector2>::OnNext(T)
extern Il2CppClass* Vector2_t3525329788_il2cpp_TypeInfo_var;
extern Il2CppClass* Exception_t1967233988_il2cpp_TypeInfo_var;
extern const uint32_t Select_OnNext_m3228142228_MetadataUsageId;
extern "C"  void Select_OnNext_m3228142228_gshared (Select_t2821314541 * __this, int64_t ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Select_OnNext_m3228142228_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Vector2_t3525329788  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Exception_t1967233988 * V_1 = NULL;
	Vector2_t3525329788  V_2;
	memset(&V_2, 0, sizeof(V_2));
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Initobj (Vector2_t3525329788_il2cpp_TypeInfo_var, (&V_2));
		Vector2_t3525329788  L_0 = V_2;
		V_0 = (Vector2_t3525329788 )L_0;
	}

IL_000a:
	try
	{ // begin try (depth: 1)
		SelectObservable_2_t4005727219 * L_1 = (SelectObservable_2_t4005727219 *)__this->get_parent_2();
		NullCheck(L_1);
		Func_2_t1270693722 * L_2 = (Func_2_t1270693722 *)L_1->get_selector_2();
		int64_t L_3 = ___value0;
		NullCheck((Func_2_t1270693722 *)L_2);
		Vector2_t3525329788  L_4 = ((  Vector2_t3525329788  (*) (Func_2_t1270693722 *, int64_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)((Func_2_t1270693722 *)L_2, (int64_t)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		V_0 = (Vector2_t3525329788 )L_4;
		goto IL_0046;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1967233988 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Exception_t1967233988_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_0021;
		throw e;
	}

CATCH_0021:
	{ // begin catch(System.Exception)
		{
			V_1 = (Exception_t1967233988 *)((Exception_t1967233988 *)__exception_local);
		}

IL_0022:
		try
		{ // begin try (depth: 2)
			Il2CppObject* L_5 = (Il2CppObject*)((OperatorObserverBase_2_t322678519 *)__this)->get_observer_0();
			il2cpp_codegen_memory_barrier();
			Exception_t1967233988 * L_6 = V_1;
			NullCheck((Il2CppObject*)L_5);
			InterfaceActionInvoker1< Exception_t1967233988 * >::Invoke(1 /* System.Void UniRx.IObserver`1<UnityEngine.Vector2>::OnError(System.Exception) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3), (Il2CppObject*)L_5, (Exception_t1967233988 *)L_6);
			IL2CPP_LEAVE(0x3C, FINALLY_0035);
		} // end try (depth: 2)
		catch(Il2CppExceptionWrapper& e)
		{
			__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
			goto FINALLY_0035;
		}

FINALLY_0035:
		{ // begin finally (depth: 2)
			NullCheck((OperatorObserverBase_2_t322678519 *)__this);
			VirtActionInvoker0::Invoke(4 /* System.Void UniRx.Operators.OperatorObserverBase`2<System.Int64,UnityEngine.Vector2>::Dispose() */, (OperatorObserverBase_2_t322678519 *)__this);
			IL2CPP_END_FINALLY(53)
		} // end finally (depth: 2)
		IL2CPP_CLEANUP(53)
		{
			IL2CPP_JUMP_TBL(0x3C, IL_003c)
			IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
		}

IL_003c:
		{
			goto IL_0054;
		}

IL_0041:
		{
			; // IL_0041: leave IL_0046
		}
	} // end catch (depth: 1)

IL_0046:
	{
		Il2CppObject* L_7 = (Il2CppObject*)((OperatorObserverBase_2_t322678519 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		Vector2_t3525329788  L_8 = V_0;
		NullCheck((Il2CppObject*)L_7);
		InterfaceActionInvoker1< Vector2_t3525329788  >::Invoke(2 /* System.Void UniRx.IObserver`1<UnityEngine.Vector2>::OnNext(T) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3), (Il2CppObject*)L_7, (Vector2_t3525329788 )L_8);
	}

IL_0054:
	{
		return;
	}
}
// System.Void UniRx.Operators.SelectObservable`2/Select<System.Int64,UnityEngine.Vector2>::OnError(System.Exception)
extern "C"  void Select_OnError_m3985505699_gshared (Select_t2821314541 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method)
{
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		Il2CppObject* L_0 = (Il2CppObject*)((OperatorObserverBase_2_t322678519 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		Exception_t1967233988 * L_1 = ___error0;
		NullCheck((Il2CppObject*)L_0);
		InterfaceActionInvoker1< Exception_t1967233988 * >::Invoke(1 /* System.Void UniRx.IObserver`1<UnityEngine.Vector2>::OnError(System.Exception) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3), (Il2CppObject*)L_0, (Exception_t1967233988 *)L_1);
		IL2CPP_LEAVE(0x1A, FINALLY_0013);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0013;
	}

FINALLY_0013:
	{ // begin finally (depth: 1)
		NullCheck((OperatorObserverBase_2_t322678519 *)__this);
		VirtActionInvoker0::Invoke(4 /* System.Void UniRx.Operators.OperatorObserverBase`2<System.Int64,UnityEngine.Vector2>::Dispose() */, (OperatorObserverBase_2_t322678519 *)__this);
		IL2CPP_END_FINALLY(19)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(19)
	{
		IL2CPP_JUMP_TBL(0x1A, IL_001a)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_001a:
	{
		return;
	}
}
// System.Void UniRx.Operators.SelectObservable`2/Select<System.Int64,UnityEngine.Vector2>::OnCompleted()
extern "C"  void Select_OnCompleted_m1506397558_gshared (Select_t2821314541 * __this, const MethodInfo* method)
{
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		Il2CppObject* L_0 = (Il2CppObject*)((OperatorObserverBase_2_t322678519 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		NullCheck((Il2CppObject*)L_0);
		InterfaceActionInvoker0::Invoke(0 /* System.Void UniRx.IObserver`1<UnityEngine.Vector2>::OnCompleted() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3), (Il2CppObject*)L_0);
		IL2CPP_LEAVE(0x19, FINALLY_0012);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0012;
	}

FINALLY_0012:
	{ // begin finally (depth: 1)
		NullCheck((OperatorObserverBase_2_t322678519 *)__this);
		VirtActionInvoker0::Invoke(4 /* System.Void UniRx.Operators.OperatorObserverBase`2<System.Int64,UnityEngine.Vector2>::Dispose() */, (OperatorObserverBase_2_t322678519 *)__this);
		IL2CPP_END_FINALLY(18)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(18)
	{
		IL2CPP_JUMP_TBL(0x19, IL_0019)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0019:
	{
		return;
	}
}
// System.Void UniRx.Operators.SelectObservable`2/Select<System.Object,System.Boolean>::.ctor(UniRx.Operators.SelectObservable`2<T,TR>,UniRx.IObserver`1<TR>,System.IDisposable)
extern "C"  void Select__ctor_m2668045015_gshared (Select_t3060303092 * __this, SelectObservable_2_t4244715770 * ___parent0, Il2CppObject* ___observer1, Il2CppObject * ___cancel2, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = ___observer1;
		Il2CppObject * L_1 = ___cancel2;
		NullCheck((OperatorObserverBase_2_t561667070 *)__this);
		((  void (*) (OperatorObserverBase_2_t561667070 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((OperatorObserverBase_2_t561667070 *)__this, (Il2CppObject*)L_0, (Il2CppObject *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		SelectObservable_2_t4244715770 * L_2 = ___parent0;
		__this->set_parent_2(L_2);
		return;
	}
}
// System.Void UniRx.Operators.SelectObservable`2/Select<System.Object,System.Boolean>::OnNext(T)
extern Il2CppClass* Boolean_t211005341_il2cpp_TypeInfo_var;
extern Il2CppClass* Exception_t1967233988_il2cpp_TypeInfo_var;
extern const uint32_t Select_OnNext_m1770588817_MetadataUsageId;
extern "C"  void Select_OnNext_m1770588817_gshared (Select_t3060303092 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Select_OnNext_m1770588817_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	bool V_0 = false;
	Exception_t1967233988 * V_1 = NULL;
	bool V_2 = false;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Initobj (Boolean_t211005341_il2cpp_TypeInfo_var, (&V_2));
		bool L_0 = V_2;
		V_0 = (bool)L_0;
	}

IL_000a:
	try
	{ // begin try (depth: 1)
		SelectObservable_2_t4244715770 * L_1 = (SelectObservable_2_t4244715770 *)__this->get_parent_2();
		NullCheck(L_1);
		Func_2_t1509682273 * L_2 = (Func_2_t1509682273 *)L_1->get_selector_2();
		Il2CppObject * L_3 = ___value0;
		NullCheck((Func_2_t1509682273 *)L_2);
		bool L_4 = ((  bool (*) (Func_2_t1509682273 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)((Func_2_t1509682273 *)L_2, (Il2CppObject *)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		V_0 = (bool)L_4;
		goto IL_0046;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1967233988 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Exception_t1967233988_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_0021;
		throw e;
	}

CATCH_0021:
	{ // begin catch(System.Exception)
		{
			V_1 = (Exception_t1967233988 *)((Exception_t1967233988 *)__exception_local);
		}

IL_0022:
		try
		{ // begin try (depth: 2)
			Il2CppObject* L_5 = (Il2CppObject*)((OperatorObserverBase_2_t561667070 *)__this)->get_observer_0();
			il2cpp_codegen_memory_barrier();
			Exception_t1967233988 * L_6 = V_1;
			NullCheck((Il2CppObject*)L_5);
			InterfaceActionInvoker1< Exception_t1967233988 * >::Invoke(1 /* System.Void UniRx.IObserver`1<System.Boolean>::OnError(System.Exception) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3), (Il2CppObject*)L_5, (Exception_t1967233988 *)L_6);
			IL2CPP_LEAVE(0x3C, FINALLY_0035);
		} // end try (depth: 2)
		catch(Il2CppExceptionWrapper& e)
		{
			__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
			goto FINALLY_0035;
		}

FINALLY_0035:
		{ // begin finally (depth: 2)
			NullCheck((OperatorObserverBase_2_t561667070 *)__this);
			VirtActionInvoker0::Invoke(4 /* System.Void UniRx.Operators.OperatorObserverBase`2<System.Object,System.Boolean>::Dispose() */, (OperatorObserverBase_2_t561667070 *)__this);
			IL2CPP_END_FINALLY(53)
		} // end finally (depth: 2)
		IL2CPP_CLEANUP(53)
		{
			IL2CPP_JUMP_TBL(0x3C, IL_003c)
			IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
		}

IL_003c:
		{
			goto IL_0054;
		}

IL_0041:
		{
			; // IL_0041: leave IL_0046
		}
	} // end catch (depth: 1)

IL_0046:
	{
		Il2CppObject* L_7 = (Il2CppObject*)((OperatorObserverBase_2_t561667070 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		bool L_8 = V_0;
		NullCheck((Il2CppObject*)L_7);
		InterfaceActionInvoker1< bool >::Invoke(2 /* System.Void UniRx.IObserver`1<System.Boolean>::OnNext(T) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3), (Il2CppObject*)L_7, (bool)L_8);
	}

IL_0054:
	{
		return;
	}
}
// System.Void UniRx.Operators.SelectObservable`2/Select<System.Object,System.Boolean>::OnError(System.Exception)
extern "C"  void Select_OnError_m1974461344_gshared (Select_t3060303092 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method)
{
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		Il2CppObject* L_0 = (Il2CppObject*)((OperatorObserverBase_2_t561667070 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		Exception_t1967233988 * L_1 = ___error0;
		NullCheck((Il2CppObject*)L_0);
		InterfaceActionInvoker1< Exception_t1967233988 * >::Invoke(1 /* System.Void UniRx.IObserver`1<System.Boolean>::OnError(System.Exception) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3), (Il2CppObject*)L_0, (Exception_t1967233988 *)L_1);
		IL2CPP_LEAVE(0x1A, FINALLY_0013);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0013;
	}

FINALLY_0013:
	{ // begin finally (depth: 1)
		NullCheck((OperatorObserverBase_2_t561667070 *)__this);
		VirtActionInvoker0::Invoke(4 /* System.Void UniRx.Operators.OperatorObserverBase`2<System.Object,System.Boolean>::Dispose() */, (OperatorObserverBase_2_t561667070 *)__this);
		IL2CPP_END_FINALLY(19)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(19)
	{
		IL2CPP_JUMP_TBL(0x1A, IL_001a)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_001a:
	{
		return;
	}
}
// System.Void UniRx.Operators.SelectObservable`2/Select<System.Object,System.Boolean>::OnCompleted()
extern "C"  void Select_OnCompleted_m1727989491_gshared (Select_t3060303092 * __this, const MethodInfo* method)
{
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		Il2CppObject* L_0 = (Il2CppObject*)((OperatorObserverBase_2_t561667070 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		NullCheck((Il2CppObject*)L_0);
		InterfaceActionInvoker0::Invoke(0 /* System.Void UniRx.IObserver`1<System.Boolean>::OnCompleted() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3), (Il2CppObject*)L_0);
		IL2CPP_LEAVE(0x19, FINALLY_0012);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0012;
	}

FINALLY_0012:
	{ // begin finally (depth: 1)
		NullCheck((OperatorObserverBase_2_t561667070 *)__this);
		VirtActionInvoker0::Invoke(4 /* System.Void UniRx.Operators.OperatorObserverBase`2<System.Object,System.Boolean>::Dispose() */, (OperatorObserverBase_2_t561667070 *)__this);
		IL2CPP_END_FINALLY(18)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(18)
	{
		IL2CPP_JUMP_TBL(0x19, IL_0019)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0019:
	{
		return;
	}
}
// System.Void UniRx.Operators.SelectObservable`2/Select<System.Object,System.Object>::.ctor(UniRx.Operators.SelectObservable`2<T,TR>,UniRx.IObserver`1<TR>,System.IDisposable)
extern "C"  void Select__ctor_m3308132824_gshared (Select_t3686404171 * __this, SelectObservable_2_t575849553 * ___parent0, Il2CppObject* ___observer1, Il2CppObject * ___cancel2, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = ___observer1;
		Il2CppObject * L_1 = ___cancel2;
		NullCheck((OperatorObserverBase_2_t1187768149 *)__this);
		((  void (*) (OperatorObserverBase_2_t1187768149 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((OperatorObserverBase_2_t1187768149 *)__this, (Il2CppObject*)L_0, (Il2CppObject *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		SelectObservable_2_t575849553 * L_2 = ___parent0;
		__this->set_parent_2(L_2);
		return;
	}
}
// System.Void UniRx.Operators.SelectObservable`2/Select<System.Object,System.Object>::OnNext(T)
extern Il2CppClass* Il2CppObject_il2cpp_TypeInfo_var;
extern Il2CppClass* Exception_t1967233988_il2cpp_TypeInfo_var;
extern const uint32_t Select_OnNext_m3219491248_MetadataUsageId;
extern "C"  void Select_OnNext_m3219491248_gshared (Select_t3686404171 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Select_OnNext_m3219491248_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Il2CppObject * V_0 = NULL;
	Exception_t1967233988 * V_1 = NULL;
	Il2CppObject * V_2 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Initobj (Il2CppObject_il2cpp_TypeInfo_var, (&V_2));
		Il2CppObject * L_0 = V_2;
		V_0 = (Il2CppObject *)L_0;
	}

IL_000a:
	try
	{ // begin try (depth: 1)
		SelectObservable_2_t575849553 * L_1 = (SelectObservable_2_t575849553 *)__this->get_parent_2();
		NullCheck(L_1);
		Func_2_t2135783352 * L_2 = (Func_2_t2135783352 *)L_1->get_selector_2();
		Il2CppObject * L_3 = ___value0;
		NullCheck((Func_2_t2135783352 *)L_2);
		Il2CppObject * L_4 = ((  Il2CppObject * (*) (Func_2_t2135783352 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)((Func_2_t2135783352 *)L_2, (Il2CppObject *)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		V_0 = (Il2CppObject *)L_4;
		goto IL_0046;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1967233988 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Exception_t1967233988_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_0021;
		throw e;
	}

CATCH_0021:
	{ // begin catch(System.Exception)
		{
			V_1 = (Exception_t1967233988 *)((Exception_t1967233988 *)__exception_local);
		}

IL_0022:
		try
		{ // begin try (depth: 2)
			Il2CppObject* L_5 = (Il2CppObject*)((OperatorObserverBase_2_t1187768149 *)__this)->get_observer_0();
			il2cpp_codegen_memory_barrier();
			Exception_t1967233988 * L_6 = V_1;
			NullCheck((Il2CppObject*)L_5);
			InterfaceActionInvoker1< Exception_t1967233988 * >::Invoke(1 /* System.Void UniRx.IObserver`1<System.Object>::OnError(System.Exception) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3), (Il2CppObject*)L_5, (Exception_t1967233988 *)L_6);
			IL2CPP_LEAVE(0x3C, FINALLY_0035);
		} // end try (depth: 2)
		catch(Il2CppExceptionWrapper& e)
		{
			__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
			goto FINALLY_0035;
		}

FINALLY_0035:
		{ // begin finally (depth: 2)
			NullCheck((OperatorObserverBase_2_t1187768149 *)__this);
			VirtActionInvoker0::Invoke(4 /* System.Void UniRx.Operators.OperatorObserverBase`2<System.Object,System.Object>::Dispose() */, (OperatorObserverBase_2_t1187768149 *)__this);
			IL2CPP_END_FINALLY(53)
		} // end finally (depth: 2)
		IL2CPP_CLEANUP(53)
		{
			IL2CPP_JUMP_TBL(0x3C, IL_003c)
			IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
		}

IL_003c:
		{
			goto IL_0054;
		}

IL_0041:
		{
			; // IL_0041: leave IL_0046
		}
	} // end catch (depth: 1)

IL_0046:
	{
		Il2CppObject* L_7 = (Il2CppObject*)((OperatorObserverBase_2_t1187768149 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		Il2CppObject * L_8 = V_0;
		NullCheck((Il2CppObject*)L_7);
		InterfaceActionInvoker1< Il2CppObject * >::Invoke(2 /* System.Void UniRx.IObserver`1<System.Object>::OnNext(T) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3), (Il2CppObject*)L_7, (Il2CppObject *)L_8);
	}

IL_0054:
	{
		return;
	}
}
// System.Void UniRx.Operators.SelectObservable`2/Select<System.Object,System.Object>::OnError(System.Exception)
extern "C"  void Select_OnError_m2646623423_gshared (Select_t3686404171 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method)
{
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		Il2CppObject* L_0 = (Il2CppObject*)((OperatorObserverBase_2_t1187768149 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		Exception_t1967233988 * L_1 = ___error0;
		NullCheck((Il2CppObject*)L_0);
		InterfaceActionInvoker1< Exception_t1967233988 * >::Invoke(1 /* System.Void UniRx.IObserver`1<System.Object>::OnError(System.Exception) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3), (Il2CppObject*)L_0, (Exception_t1967233988 *)L_1);
		IL2CPP_LEAVE(0x1A, FINALLY_0013);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0013;
	}

FINALLY_0013:
	{ // begin finally (depth: 1)
		NullCheck((OperatorObserverBase_2_t1187768149 *)__this);
		VirtActionInvoker0::Invoke(4 /* System.Void UniRx.Operators.OperatorObserverBase`2<System.Object,System.Object>::Dispose() */, (OperatorObserverBase_2_t1187768149 *)__this);
		IL2CPP_END_FINALLY(19)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(19)
	{
		IL2CPP_JUMP_TBL(0x1A, IL_001a)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_001a:
	{
		return;
	}
}
// System.Void UniRx.Operators.SelectObservable`2/Select<System.Object,System.Object>::OnCompleted()
extern "C"  void Select_OnCompleted_m783867538_gshared (Select_t3686404171 * __this, const MethodInfo* method)
{
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		Il2CppObject* L_0 = (Il2CppObject*)((OperatorObserverBase_2_t1187768149 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		NullCheck((Il2CppObject*)L_0);
		InterfaceActionInvoker0::Invoke(0 /* System.Void UniRx.IObserver`1<System.Object>::OnCompleted() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3), (Il2CppObject*)L_0);
		IL2CPP_LEAVE(0x19, FINALLY_0012);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0012;
	}

FINALLY_0012:
	{ // begin finally (depth: 1)
		NullCheck((OperatorObserverBase_2_t1187768149 *)__this);
		VirtActionInvoker0::Invoke(4 /* System.Void UniRx.Operators.OperatorObserverBase`2<System.Object,System.Object>::Dispose() */, (OperatorObserverBase_2_t1187768149 *)__this);
		IL2CPP_END_FINALLY(18)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(18)
	{
		IL2CPP_JUMP_TBL(0x19, IL_0019)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0019:
	{
		return;
	}
}
// System.Void UniRx.Operators.SelectObservable`2<System.Int32,UnityEngine.Color>::.ctor(UniRx.IObservable`1<T>,System.Func`2<T,TR>)
extern "C"  void SelectObservable_2__ctor_m3316517214_gshared (SelectObservable_2_t3125378242 * __this, Il2CppObject* ___source0, Func_2_t390344745 * ___selector1, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = ___source0;
		bool L_1 = ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)(NULL /*static, unused*/, (Il2CppObject*)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		NullCheck((OperatorObservableBase_1_t652320731 *)__this);
		((  void (*) (OperatorObservableBase_1_t652320731 *, bool, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)((OperatorObservableBase_1_t652320731 *)__this, (bool)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		Il2CppObject* L_2 = ___source0;
		__this->set_source_1(L_2);
		Func_2_t390344745 * L_3 = ___selector1;
		__this->set_selector_2(L_3);
		return;
	}
}
// System.Void UniRx.Operators.SelectObservable`2<System.Int32,UnityEngine.Color>::.ctor(UniRx.IObservable`1<T>,System.Func`3<T,System.Int32,TR>)
extern "C"  void SelectObservable_2__ctor_m1941539090_gshared (SelectObservable_2_t3125378242 * __this, Il2CppObject* ___source0, Func_3_t3578830837 * ___selector1, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = ___source0;
		bool L_1 = ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)(NULL /*static, unused*/, (Il2CppObject*)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		NullCheck((OperatorObservableBase_1_t652320731 *)__this);
		((  void (*) (OperatorObservableBase_1_t652320731 *, bool, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)((OperatorObservableBase_1_t652320731 *)__this, (bool)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		Il2CppObject* L_2 = ___source0;
		__this->set_source_1(L_2);
		Func_3_t3578830837 * L_3 = ___selector1;
		__this->set_selectorWithIndex_3(L_3);
		return;
	}
}
// System.IDisposable UniRx.Operators.SelectObservable`2<System.Int32,UnityEngine.Color>::SubscribeCore(UniRx.IObserver`1<TR>,System.IDisposable)
extern "C"  Il2CppObject * SelectObservable_2_SubscribeCore_m3166000742_gshared (SelectObservable_2_t3125378242 * __this, Il2CppObject* ___observer0, Il2CppObject * ___cancel1, const MethodInfo* method)
{
	{
		Func_2_t390344745 * L_0 = (Func_2_t390344745 *)__this->get_selector_2();
		if (!L_0)
		{
			goto IL_001f;
		}
	}
	{
		Il2CppObject* L_1 = (Il2CppObject*)__this->get_source_1();
		Il2CppObject* L_2 = ___observer0;
		Il2CppObject * L_3 = ___cancel1;
		Select_t1940965564 * L_4 = (Select_t1940965564 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		((  void (*) (Select_t1940965564 *, SelectObservable_2_t3125378242 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)(L_4, (SelectObservable_2_t3125378242 *)__this, (Il2CppObject*)L_2, (Il2CppObject *)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		NullCheck((Il2CppObject*)L_1);
		Il2CppObject * L_5 = InterfaceFuncInvoker1< Il2CppObject *, Il2CppObject* >::Invoke(0 /* System.IDisposable UniRx.IObservable`1<System.Int32>::Subscribe(UniRx.IObserver`1<T>) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5), (Il2CppObject*)L_1, (Il2CppObject*)L_4);
		return L_5;
	}

IL_001f:
	{
		Il2CppObject* L_6 = (Il2CppObject*)__this->get_source_1();
		Il2CppObject* L_7 = ___observer0;
		Il2CppObject * L_8 = ___cancel1;
		Select__t1427971985 * L_9 = (Select__t1427971985 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		((  void (*) (Select__t1427971985 *, SelectObservable_2_t3125378242 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7)->method)(L_9, (SelectObservable_2_t3125378242 *)__this, (Il2CppObject*)L_7, (Il2CppObject *)L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7));
		NullCheck((Il2CppObject*)L_6);
		Il2CppObject * L_10 = InterfaceFuncInvoker1< Il2CppObject *, Il2CppObject* >::Invoke(0 /* System.IDisposable UniRx.IObservable`1<System.Int32>::Subscribe(UniRx.IObserver`1<T>) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5), (Il2CppObject*)L_6, (Il2CppObject*)L_9);
		return L_10;
	}
}
// System.Void UniRx.Operators.SelectObservable`2<System.Int64,System.Boolean>::.ctor(UniRx.IObservable`1<T>,System.Func`2<T,TR>)
extern "C"  void SelectObservable_2__ctor_m389659222_gshared (SelectObservable_2_t691402772 * __this, Il2CppObject* ___source0, Func_2_t2251336571 * ___selector1, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = ___source0;
		bool L_1 = ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)(NULL /*static, unused*/, (Il2CppObject*)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		NullCheck((OperatorObservableBase_1_t3570117608 *)__this);
		((  void (*) (OperatorObservableBase_1_t3570117608 *, bool, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)((OperatorObservableBase_1_t3570117608 *)__this, (bool)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		Il2CppObject* L_2 = ___source0;
		__this->set_source_1(L_2);
		Func_2_t2251336571 * L_3 = ___selector1;
		__this->set_selector_2(L_3);
		return;
	}
}
// System.Void UniRx.Operators.SelectObservable`2<System.Int64,System.Boolean>::.ctor(UniRx.IObservable`1<T>,System.Func`3<T,System.Int32,TR>)
extern "C"  void SelectObservable_2__ctor_m2240228122_gshared (SelectObservable_2_t691402772 * __this, Il2CppObject* ___source0, Func_3_t1612416521 * ___selector1, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = ___source0;
		bool L_1 = ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)(NULL /*static, unused*/, (Il2CppObject*)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		NullCheck((OperatorObservableBase_1_t3570117608 *)__this);
		((  void (*) (OperatorObservableBase_1_t3570117608 *, bool, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)((OperatorObservableBase_1_t3570117608 *)__this, (bool)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		Il2CppObject* L_2 = ___source0;
		__this->set_source_1(L_2);
		Func_3_t1612416521 * L_3 = ___selector1;
		__this->set_selectorWithIndex_3(L_3);
		return;
	}
}
// System.IDisposable UniRx.Operators.SelectObservable`2<System.Int64,System.Boolean>::SubscribeCore(UniRx.IObserver`1<TR>,System.IDisposable)
extern "C"  Il2CppObject * SelectObservable_2_SubscribeCore_m473421748_gshared (SelectObservable_2_t691402772 * __this, Il2CppObject* ___observer0, Il2CppObject * ___cancel1, const MethodInfo* method)
{
	{
		Func_2_t2251336571 * L_0 = (Func_2_t2251336571 *)__this->get_selector_2();
		if (!L_0)
		{
			goto IL_001f;
		}
	}
	{
		Il2CppObject* L_1 = (Il2CppObject*)__this->get_source_1();
		Il2CppObject* L_2 = ___observer0;
		Il2CppObject * L_3 = ___cancel1;
		Select_t3801957390 * L_4 = (Select_t3801957390 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		((  void (*) (Select_t3801957390 *, SelectObservable_2_t691402772 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)(L_4, (SelectObservable_2_t691402772 *)__this, (Il2CppObject*)L_2, (Il2CppObject *)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		NullCheck((Il2CppObject*)L_1);
		Il2CppObject * L_5 = InterfaceFuncInvoker1< Il2CppObject *, Il2CppObject* >::Invoke(0 /* System.IDisposable UniRx.IObservable`1<System.Int64>::Subscribe(UniRx.IObserver`1<T>) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5), (Il2CppObject*)L_1, (Il2CppObject*)L_4);
		return L_5;
	}

IL_001f:
	{
		Il2CppObject* L_6 = (Il2CppObject*)__this->get_source_1();
		Il2CppObject* L_7 = ___observer0;
		Il2CppObject * L_8 = ___cancel1;
		Select__t3288963811 * L_9 = (Select__t3288963811 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		((  void (*) (Select__t3288963811 *, SelectObservable_2_t691402772 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7)->method)(L_9, (SelectObservable_2_t691402772 *)__this, (Il2CppObject*)L_7, (Il2CppObject *)L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7));
		NullCheck((Il2CppObject*)L_6);
		Il2CppObject * L_10 = InterfaceFuncInvoker1< Il2CppObject *, Il2CppObject* >::Invoke(0 /* System.IDisposable UniRx.IObservable`1<System.Int64>::Subscribe(UniRx.IObserver`1<T>) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5), (Il2CppObject*)L_6, (Il2CppObject*)L_9);
		return L_10;
	}
}
// System.Void UniRx.Operators.SelectObservable`2<System.Int64,System.Int32>::.ctor(UniRx.IObservable`1<T>,System.Func`2<T,TR>)
extern "C"  void SelectObservable_2__ctor_m3445760380_gshared (SelectObservable_2_t3327812218 * __this, Il2CppObject* ___source0, Func_2_t592778721 * ___selector1, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = ___source0;
		bool L_1 = ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)(NULL /*static, unused*/, (Il2CppObject*)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		NullCheck((OperatorObservableBase_1_t1911559758 *)__this);
		((  void (*) (OperatorObservableBase_1_t1911559758 *, bool, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)((OperatorObservableBase_1_t1911559758 *)__this, (bool)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		Il2CppObject* L_2 = ___source0;
		__this->set_source_1(L_2);
		Func_2_t592778721 * L_3 = ___selector1;
		__this->set_selector_2(L_3);
		return;
	}
}
// System.Void UniRx.Operators.SelectObservable`2<System.Int64,System.Int32>::.ctor(UniRx.IObservable`1<T>,System.Func`3<T,System.Int32,TR>)
extern "C"  void SelectObservable_2__ctor_m3546943924_gshared (SelectObservable_2_t3327812218 * __this, Il2CppObject* ___source0, Func_3_t4248825967 * ___selector1, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = ___source0;
		bool L_1 = ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)(NULL /*static, unused*/, (Il2CppObject*)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		NullCheck((OperatorObservableBase_1_t1911559758 *)__this);
		((  void (*) (OperatorObservableBase_1_t1911559758 *, bool, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)((OperatorObservableBase_1_t1911559758 *)__this, (bool)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		Il2CppObject* L_2 = ___source0;
		__this->set_source_1(L_2);
		Func_3_t4248825967 * L_3 = ___selector1;
		__this->set_selectorWithIndex_3(L_3);
		return;
	}
}
// System.IDisposable UniRx.Operators.SelectObservable`2<System.Int64,System.Int32>::SubscribeCore(UniRx.IObserver`1<TR>,System.IDisposable)
extern "C"  Il2CppObject * SelectObservable_2_SubscribeCore_m564403482_gshared (SelectObservable_2_t3327812218 * __this, Il2CppObject* ___observer0, Il2CppObject * ___cancel1, const MethodInfo* method)
{
	{
		Func_2_t592778721 * L_0 = (Func_2_t592778721 *)__this->get_selector_2();
		if (!L_0)
		{
			goto IL_001f;
		}
	}
	{
		Il2CppObject* L_1 = (Il2CppObject*)__this->get_source_1();
		Il2CppObject* L_2 = ___observer0;
		Il2CppObject * L_3 = ___cancel1;
		Select_t2143399540 * L_4 = (Select_t2143399540 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		((  void (*) (Select_t2143399540 *, SelectObservable_2_t3327812218 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)(L_4, (SelectObservable_2_t3327812218 *)__this, (Il2CppObject*)L_2, (Il2CppObject *)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		NullCheck((Il2CppObject*)L_1);
		Il2CppObject * L_5 = InterfaceFuncInvoker1< Il2CppObject *, Il2CppObject* >::Invoke(0 /* System.IDisposable UniRx.IObservable`1<System.Int64>::Subscribe(UniRx.IObserver`1<T>) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5), (Il2CppObject*)L_1, (Il2CppObject*)L_4);
		return L_5;
	}

IL_001f:
	{
		Il2CppObject* L_6 = (Il2CppObject*)__this->get_source_1();
		Il2CppObject* L_7 = ___observer0;
		Il2CppObject * L_8 = ___cancel1;
		Select__t1630405961 * L_9 = (Select__t1630405961 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		((  void (*) (Select__t1630405961 *, SelectObservable_2_t3327812218 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7)->method)(L_9, (SelectObservable_2_t3327812218 *)__this, (Il2CppObject*)L_7, (Il2CppObject *)L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7));
		NullCheck((Il2CppObject*)L_6);
		Il2CppObject * L_10 = InterfaceFuncInvoker1< Il2CppObject *, Il2CppObject* >::Invoke(0 /* System.IDisposable UniRx.IObservable`1<System.Int64>::Subscribe(UniRx.IObserver`1<T>) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5), (Il2CppObject*)L_6, (Il2CppObject*)L_9);
		return L_10;
	}
}
// System.Void UniRx.Operators.SelectObservable`2<System.Int64,System.Object>::.ctor(UniRx.IObservable`1<T>,System.Func`2<T,TR>)
extern "C"  void SelectObservable_2__ctor_m3808381391_gshared (SelectObservable_2_t1317503851 * __this, Il2CppObject* ___source0, Func_2_t2877437650 * ___selector1, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = ___source0;
		bool L_1 = ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)(NULL /*static, unused*/, (Il2CppObject*)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		NullCheck((OperatorObservableBase_1_t4196218687 *)__this);
		((  void (*) (OperatorObservableBase_1_t4196218687 *, bool, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)((OperatorObservableBase_1_t4196218687 *)__this, (bool)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		Il2CppObject* L_2 = ___source0;
		__this->set_source_1(L_2);
		Func_2_t2877437650 * L_3 = ___selector1;
		__this->set_selector_2(L_3);
		return;
	}
}
// System.Void UniRx.Operators.SelectObservable`2<System.Int64,System.Object>::.ctor(UniRx.IObservable`1<T>,System.Func`3<T,System.Int32,TR>)
extern "C"  void SelectObservable_2__ctor_m1733707841_gshared (SelectObservable_2_t1317503851 * __this, Il2CppObject* ___source0, Func_3_t2238517600 * ___selector1, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = ___source0;
		bool L_1 = ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)(NULL /*static, unused*/, (Il2CppObject*)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		NullCheck((OperatorObservableBase_1_t4196218687 *)__this);
		((  void (*) (OperatorObservableBase_1_t4196218687 *, bool, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)((OperatorObservableBase_1_t4196218687 *)__this, (bool)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		Il2CppObject* L_2 = ___source0;
		__this->set_source_1(L_2);
		Func_3_t2238517600 * L_3 = ___selector1;
		__this->set_selectorWithIndex_3(L_3);
		return;
	}
}
// System.IDisposable UniRx.Operators.SelectObservable`2<System.Int64,System.Object>::SubscribeCore(UniRx.IObserver`1<TR>,System.IDisposable)
extern "C"  Il2CppObject * SelectObservable_2_SubscribeCore_m1692301463_gshared (SelectObservable_2_t1317503851 * __this, Il2CppObject* ___observer0, Il2CppObject * ___cancel1, const MethodInfo* method)
{
	{
		Func_2_t2877437650 * L_0 = (Func_2_t2877437650 *)__this->get_selector_2();
		if (!L_0)
		{
			goto IL_001f;
		}
	}
	{
		Il2CppObject* L_1 = (Il2CppObject*)__this->get_source_1();
		Il2CppObject* L_2 = ___observer0;
		Il2CppObject * L_3 = ___cancel1;
		Select_t133091173 * L_4 = (Select_t133091173 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		((  void (*) (Select_t133091173 *, SelectObservable_2_t1317503851 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)(L_4, (SelectObservable_2_t1317503851 *)__this, (Il2CppObject*)L_2, (Il2CppObject *)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		NullCheck((Il2CppObject*)L_1);
		Il2CppObject * L_5 = InterfaceFuncInvoker1< Il2CppObject *, Il2CppObject* >::Invoke(0 /* System.IDisposable UniRx.IObservable`1<System.Int64>::Subscribe(UniRx.IObserver`1<T>) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5), (Il2CppObject*)L_1, (Il2CppObject*)L_4);
		return L_5;
	}

IL_001f:
	{
		Il2CppObject* L_6 = (Il2CppObject*)__this->get_source_1();
		Il2CppObject* L_7 = ___observer0;
		Il2CppObject * L_8 = ___cancel1;
		Select__t3915064890 * L_9 = (Select__t3915064890 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		((  void (*) (Select__t3915064890 *, SelectObservable_2_t1317503851 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7)->method)(L_9, (SelectObservable_2_t1317503851 *)__this, (Il2CppObject*)L_7, (Il2CppObject *)L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7));
		NullCheck((Il2CppObject*)L_6);
		Il2CppObject * L_10 = InterfaceFuncInvoker1< Il2CppObject *, Il2CppObject* >::Invoke(0 /* System.IDisposable UniRx.IObservable`1<System.Int64>::Subscribe(UniRx.IObserver`1<T>) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5), (Il2CppObject*)L_6, (Il2CppObject*)L_9);
		return L_10;
	}
}
// System.Void UniRx.Operators.SelectObservable`2<System.Int64,UnityEngine.Vector2>::.ctor(UniRx.IObservable`1<T>,System.Func`2<T,TR>)
extern "C"  void SelectObservable_2__ctor_m3268121001_gshared (SelectObservable_2_t4005727219 * __this, Il2CppObject* ___source0, Func_2_t1270693722 * ___selector1, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = ___source0;
		bool L_1 = ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)(NULL /*static, unused*/, (Il2CppObject*)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		NullCheck((OperatorObservableBase_1_t2589474759 *)__this);
		((  void (*) (OperatorObservableBase_1_t2589474759 *, bool, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)((OperatorObservableBase_1_t2589474759 *)__this, (bool)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		Il2CppObject* L_2 = ___source0;
		__this->set_source_1(L_2);
		Func_2_t1270693722 * L_3 = ___selector1;
		__this->set_selector_2(L_3);
		return;
	}
}
// System.Void UniRx.Operators.SelectObservable`2<System.Int64,UnityEngine.Vector2>::.ctor(UniRx.IObservable`1<T>,System.Func`3<T,System.Int32,TR>)
extern "C"  void SelectObservable_2__ctor_m1825772199_gshared (SelectObservable_2_t4005727219 * __this, Il2CppObject* ___source0, Func_3_t631773672 * ___selector1, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = ___source0;
		bool L_1 = ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)(NULL /*static, unused*/, (Il2CppObject*)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		NullCheck((OperatorObservableBase_1_t2589474759 *)__this);
		((  void (*) (OperatorObservableBase_1_t2589474759 *, bool, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)((OperatorObservableBase_1_t2589474759 *)__this, (bool)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		Il2CppObject* L_2 = ___source0;
		__this->set_source_1(L_2);
		Func_3_t631773672 * L_3 = ___selector1;
		__this->set_selectorWithIndex_3(L_3);
		return;
	}
}
// System.IDisposable UniRx.Operators.SelectObservable`2<System.Int64,UnityEngine.Vector2>::SubscribeCore(UniRx.IObserver`1<TR>,System.IDisposable)
extern "C"  Il2CppObject * SelectObservable_2_SubscribeCore_m2470106417_gshared (SelectObservable_2_t4005727219 * __this, Il2CppObject* ___observer0, Il2CppObject * ___cancel1, const MethodInfo* method)
{
	{
		Func_2_t1270693722 * L_0 = (Func_2_t1270693722 *)__this->get_selector_2();
		if (!L_0)
		{
			goto IL_001f;
		}
	}
	{
		Il2CppObject* L_1 = (Il2CppObject*)__this->get_source_1();
		Il2CppObject* L_2 = ___observer0;
		Il2CppObject * L_3 = ___cancel1;
		Select_t2821314541 * L_4 = (Select_t2821314541 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		((  void (*) (Select_t2821314541 *, SelectObservable_2_t4005727219 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)(L_4, (SelectObservable_2_t4005727219 *)__this, (Il2CppObject*)L_2, (Il2CppObject *)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		NullCheck((Il2CppObject*)L_1);
		Il2CppObject * L_5 = InterfaceFuncInvoker1< Il2CppObject *, Il2CppObject* >::Invoke(0 /* System.IDisposable UniRx.IObservable`1<System.Int64>::Subscribe(UniRx.IObserver`1<T>) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5), (Il2CppObject*)L_1, (Il2CppObject*)L_4);
		return L_5;
	}

IL_001f:
	{
		Il2CppObject* L_6 = (Il2CppObject*)__this->get_source_1();
		Il2CppObject* L_7 = ___observer0;
		Il2CppObject * L_8 = ___cancel1;
		Select__t2308320962 * L_9 = (Select__t2308320962 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		((  void (*) (Select__t2308320962 *, SelectObservable_2_t4005727219 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7)->method)(L_9, (SelectObservable_2_t4005727219 *)__this, (Il2CppObject*)L_7, (Il2CppObject *)L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7));
		NullCheck((Il2CppObject*)L_6);
		Il2CppObject * L_10 = InterfaceFuncInvoker1< Il2CppObject *, Il2CppObject* >::Invoke(0 /* System.IDisposable UniRx.IObservable`1<System.Int64>::Subscribe(UniRx.IObserver`1<T>) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5), (Il2CppObject*)L_6, (Il2CppObject*)L_9);
		return L_10;
	}
}
// System.Void UniRx.Operators.SelectObservable`2<System.Object,System.Boolean>::.ctor(UniRx.IObservable`1<T>,System.Func`2<T,TR>)
extern "C"  void SelectObservable_2__ctor_m1636483622_gshared (SelectObservable_2_t4244715770 * __this, Il2CppObject* ___source0, Func_2_t1509682273 * ___selector1, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = ___source0;
		bool L_1 = ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)(NULL /*static, unused*/, (Il2CppObject*)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		NullCheck((OperatorObservableBase_1_t3570117608 *)__this);
		((  void (*) (OperatorObservableBase_1_t3570117608 *, bool, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)((OperatorObservableBase_1_t3570117608 *)__this, (bool)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		Il2CppObject* L_2 = ___source0;
		__this->set_source_1(L_2);
		Func_2_t1509682273 * L_3 = ___selector1;
		__this->set_selector_2(L_3);
		return;
	}
}
// System.Void UniRx.Operators.SelectObservable`2<System.Object,System.Boolean>::.ctor(UniRx.IObservable`1<T>,System.Func`3<T,System.Int32,TR>)
extern "C"  void SelectObservable_2__ctor_m3683472714_gshared (SelectObservable_2_t4244715770 * __this, Il2CppObject* ___source0, Func_3_t3064567499 * ___selector1, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = ___source0;
		bool L_1 = ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)(NULL /*static, unused*/, (Il2CppObject*)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		NullCheck((OperatorObservableBase_1_t3570117608 *)__this);
		((  void (*) (OperatorObservableBase_1_t3570117608 *, bool, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)((OperatorObservableBase_1_t3570117608 *)__this, (bool)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		Il2CppObject* L_2 = ___source0;
		__this->set_source_1(L_2);
		Func_3_t3064567499 * L_3 = ___selector1;
		__this->set_selectorWithIndex_3(L_3);
		return;
	}
}
// System.IDisposable UniRx.Operators.SelectObservable`2<System.Object,System.Boolean>::SubscribeCore(UniRx.IObserver`1<TR>,System.IDisposable)
extern "C"  Il2CppObject * SelectObservable_2_SubscribeCore_m1583615598_gshared (SelectObservable_2_t4244715770 * __this, Il2CppObject* ___observer0, Il2CppObject * ___cancel1, const MethodInfo* method)
{
	{
		Func_2_t1509682273 * L_0 = (Func_2_t1509682273 *)__this->get_selector_2();
		if (!L_0)
		{
			goto IL_001f;
		}
	}
	{
		Il2CppObject* L_1 = (Il2CppObject*)__this->get_source_1();
		Il2CppObject* L_2 = ___observer0;
		Il2CppObject * L_3 = ___cancel1;
		Select_t3060303092 * L_4 = (Select_t3060303092 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		((  void (*) (Select_t3060303092 *, SelectObservable_2_t4244715770 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)(L_4, (SelectObservable_2_t4244715770 *)__this, (Il2CppObject*)L_2, (Il2CppObject *)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		NullCheck((Il2CppObject*)L_1);
		Il2CppObject * L_5 = InterfaceFuncInvoker1< Il2CppObject *, Il2CppObject* >::Invoke(0 /* System.IDisposable UniRx.IObservable`1<System.Object>::Subscribe(UniRx.IObserver`1<T>) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5), (Il2CppObject*)L_1, (Il2CppObject*)L_4);
		return L_5;
	}

IL_001f:
	{
		Il2CppObject* L_6 = (Il2CppObject*)__this->get_source_1();
		Il2CppObject* L_7 = ___observer0;
		Il2CppObject * L_8 = ___cancel1;
		Select__t2547309513 * L_9 = (Select__t2547309513 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		((  void (*) (Select__t2547309513 *, SelectObservable_2_t4244715770 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7)->method)(L_9, (SelectObservable_2_t4244715770 *)__this, (Il2CppObject*)L_7, (Il2CppObject *)L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7));
		NullCheck((Il2CppObject*)L_6);
		Il2CppObject * L_10 = InterfaceFuncInvoker1< Il2CppObject *, Il2CppObject* >::Invoke(0 /* System.IDisposable UniRx.IObservable`1<System.Object>::Subscribe(UniRx.IObserver`1<T>) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5), (Il2CppObject*)L_6, (Il2CppObject*)L_9);
		return L_10;
	}
}
// System.Void UniRx.Operators.SelectObservable`2<System.Object,System.Object>::.ctor(UniRx.IObservable`1<T>,System.Func`2<T,TR>)
extern "C"  void SelectObservable_2__ctor_m1770391551_gshared (SelectObservable_2_t575849553 * __this, Il2CppObject* ___source0, Func_2_t2135783352 * ___selector1, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = ___source0;
		bool L_1 = ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)(NULL /*static, unused*/, (Il2CppObject*)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		NullCheck((OperatorObservableBase_1_t4196218687 *)__this);
		((  void (*) (OperatorObservableBase_1_t4196218687 *, bool, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)((OperatorObservableBase_1_t4196218687 *)__this, (bool)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		Il2CppObject* L_2 = ___source0;
		__this->set_source_1(L_2);
		Func_2_t2135783352 * L_3 = ___selector1;
		__this->set_selector_2(L_3);
		return;
	}
}
// System.Void UniRx.Operators.SelectObservable`2<System.Object,System.Object>::.ctor(UniRx.IObservable`1<T>,System.Func`3<T,System.Int32,TR>)
extern "C"  void SelectObservable_2__ctor_m533338129_gshared (SelectObservable_2_t575849553 * __this, Il2CppObject* ___source0, Func_3_t3690668578 * ___selector1, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = ___source0;
		bool L_1 = ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)(NULL /*static, unused*/, (Il2CppObject*)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		NullCheck((OperatorObservableBase_1_t4196218687 *)__this);
		((  void (*) (OperatorObservableBase_1_t4196218687 *, bool, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)((OperatorObservableBase_1_t4196218687 *)__this, (bool)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		Il2CppObject* L_2 = ___source0;
		__this->set_source_1(L_2);
		Func_3_t3690668578 * L_3 = ___selector1;
		__this->set_selectorWithIndex_3(L_3);
		return;
	}
}
// System.IDisposable UniRx.Operators.SelectObservable`2<System.Object,System.Object>::SubscribeCore(UniRx.IObserver`1<TR>,System.IDisposable)
extern "C"  Il2CppObject * SelectObservable_2_SubscribeCore_m2975040157_gshared (SelectObservable_2_t575849553 * __this, Il2CppObject* ___observer0, Il2CppObject * ___cancel1, const MethodInfo* method)
{
	{
		Func_2_t2135783352 * L_0 = (Func_2_t2135783352 *)__this->get_selector_2();
		if (!L_0)
		{
			goto IL_001f;
		}
	}
	{
		Il2CppObject* L_1 = (Il2CppObject*)__this->get_source_1();
		Il2CppObject* L_2 = ___observer0;
		Il2CppObject * L_3 = ___cancel1;
		Select_t3686404171 * L_4 = (Select_t3686404171 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		((  void (*) (Select_t3686404171 *, SelectObservable_2_t575849553 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)(L_4, (SelectObservable_2_t575849553 *)__this, (Il2CppObject*)L_2, (Il2CppObject *)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		NullCheck((Il2CppObject*)L_1);
		Il2CppObject * L_5 = InterfaceFuncInvoker1< Il2CppObject *, Il2CppObject* >::Invoke(0 /* System.IDisposable UniRx.IObservable`1<System.Object>::Subscribe(UniRx.IObserver`1<T>) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5), (Il2CppObject*)L_1, (Il2CppObject*)L_4);
		return L_5;
	}

IL_001f:
	{
		Il2CppObject* L_6 = (Il2CppObject*)__this->get_source_1();
		Il2CppObject* L_7 = ___observer0;
		Il2CppObject * L_8 = ___cancel1;
		Select__t3173410592 * L_9 = (Select__t3173410592 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		((  void (*) (Select__t3173410592 *, SelectObservable_2_t575849553 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7)->method)(L_9, (SelectObservable_2_t575849553 *)__this, (Il2CppObject*)L_7, (Il2CppObject *)L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7));
		NullCheck((Il2CppObject*)L_6);
		Il2CppObject * L_10 = InterfaceFuncInvoker1< Il2CppObject *, Il2CppObject* >::Invoke(0 /* System.IDisposable UniRx.IObservable`1<System.Object>::Subscribe(UniRx.IObserver`1<T>) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5), (Il2CppObject*)L_6, (Il2CppObject*)L_9);
		return L_10;
	}
}
// System.Void UniRx.Operators.SingleObservable`1/Single_<System.Object>::.ctor(UniRx.Operators.SingleObservable`1<T>,UniRx.IObserver`1<T>,System.IDisposable)
extern "C"  void Single___ctor_m1197900589_gshared (Single__t984311384 * __this, SingleObservable_1_t427280954 * ___parent0, Il2CppObject* ___observer1, Il2CppObject * ___cancel2, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = ___observer1;
		Il2CppObject * L_1 = ___cancel2;
		NullCheck((OperatorObserverBase_2_t1187768149 *)__this);
		((  void (*) (OperatorObserverBase_2_t1187768149 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((OperatorObserverBase_2_t1187768149 *)__this, (Il2CppObject*)L_0, (Il2CppObject *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		SingleObservable_1_t427280954 * L_2 = ___parent0;
		__this->set_parent_2(L_2);
		__this->set_seenValue_3((bool)0);
		return;
	}
}
// System.Void UniRx.Operators.SingleObservable`1/Single_<System.Object>::OnNext(T)
extern Il2CppClass* Exception_t1967233988_il2cpp_TypeInfo_var;
extern Il2CppClass* InvalidOperationException_t2420574324_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1683804364;
extern const uint32_t Single__OnNext_m675265822_MetadataUsageId;
extern "C"  void Single__OnNext_m675265822_gshared (Single__t984311384 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Single__OnNext_m675265822_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	bool V_0 = false;
	Exception_t1967233988 * V_1 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		SingleObservable_1_t427280954 * L_0 = (SingleObservable_1_t427280954 *)__this->get_parent_2();
		NullCheck(L_0);
		Func_2_t1509682273 * L_1 = (Func_2_t1509682273 *)L_0->get_predicate_3();
		Il2CppObject * L_2 = ___value0;
		NullCheck((Func_2_t1509682273 *)L_1);
		bool L_3 = ((  bool (*) (Func_2_t1509682273 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)((Func_2_t1509682273 *)L_1, (Il2CppObject *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		V_0 = (bool)L_3;
		goto IL_003c;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1967233988 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Exception_t1967233988_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_0017;
		throw e;
	}

CATCH_0017:
	{ // begin catch(System.Exception)
		{
			V_1 = (Exception_t1967233988 *)((Exception_t1967233988 *)__exception_local);
		}

IL_0018:
		try
		{ // begin try (depth: 2)
			Il2CppObject* L_4 = (Il2CppObject*)((OperatorObserverBase_2_t1187768149 *)__this)->get_observer_0();
			il2cpp_codegen_memory_barrier();
			Exception_t1967233988 * L_5 = V_1;
			NullCheck((Il2CppObject*)L_4);
			InterfaceActionInvoker1< Exception_t1967233988 * >::Invoke(1 /* System.Void UniRx.IObserver`1<System.Object>::OnError(System.Exception) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3), (Il2CppObject*)L_4, (Exception_t1967233988 *)L_5);
			IL2CPP_LEAVE(0x32, FINALLY_002b);
		} // end try (depth: 2)
		catch(Il2CppExceptionWrapper& e)
		{
			__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
			goto FINALLY_002b;
		}

FINALLY_002b:
		{ // begin finally (depth: 2)
			NullCheck((OperatorObserverBase_2_t1187768149 *)__this);
			VirtActionInvoker0::Invoke(4 /* System.Void UniRx.Operators.OperatorObserverBase`2<System.Object,System.Object>::Dispose() */, (OperatorObserverBase_2_t1187768149 *)__this);
			IL2CPP_END_FINALLY(43)
		} // end finally (depth: 2)
		IL2CPP_CLEANUP(43)
		{
			IL2CPP_JUMP_TBL(0x32, IL_0032)
			IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
		}

IL_0032:
		{
			goto IL_007f;
		}

IL_0037:
		{
			; // IL_0037: leave IL_003c
		}
	} // end catch (depth: 1)

IL_003c:
	{
		bool L_6 = V_0;
		if (!L_6)
		{
			goto IL_007f;
		}
	}
	{
		bool L_7 = (bool)__this->get_seenValue_3();
		if (!L_7)
		{
			goto IL_0071;
		}
	}

IL_004d:
	try
	{ // begin try (depth: 1)
		Il2CppObject* L_8 = (Il2CppObject*)((OperatorObserverBase_2_t1187768149 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		InvalidOperationException_t2420574324 * L_9 = (InvalidOperationException_t2420574324 *)il2cpp_codegen_object_new(InvalidOperationException_t2420574324_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_9, (String_t*)_stringLiteral1683804364, /*hidden argument*/NULL);
		NullCheck((Il2CppObject*)L_8);
		InterfaceActionInvoker1< Exception_t1967233988 * >::Invoke(1 /* System.Void UniRx.IObserver`1<System.Object>::OnError(System.Exception) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3), (Il2CppObject*)L_8, (Exception_t1967233988 *)L_9);
		IL2CPP_LEAVE(0x70, FINALLY_0069);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0069;
	}

FINALLY_0069:
	{ // begin finally (depth: 1)
		NullCheck((OperatorObserverBase_2_t1187768149 *)__this);
		VirtActionInvoker0::Invoke(4 /* System.Void UniRx.Operators.OperatorObserverBase`2<System.Object,System.Object>::Dispose() */, (OperatorObserverBase_2_t1187768149 *)__this);
		IL2CPP_END_FINALLY(105)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(105)
	{
		IL2CPP_JUMP_TBL(0x70, IL_0070)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0070:
	{
		return;
	}

IL_0071:
	{
		__this->set_seenValue_3((bool)1);
		Il2CppObject * L_10 = ___value0;
		__this->set_lastValue_4(L_10);
	}

IL_007f:
	{
		return;
	}
}
// System.Void UniRx.Operators.SingleObservable`1/Single_<System.Object>::OnError(System.Exception)
extern "C"  void Single__OnError_m2454838317_gshared (Single__t984311384 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method)
{
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		Il2CppObject* L_0 = (Il2CppObject*)((OperatorObserverBase_2_t1187768149 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		Exception_t1967233988 * L_1 = ___error0;
		NullCheck((Il2CppObject*)L_0);
		InterfaceActionInvoker1< Exception_t1967233988 * >::Invoke(1 /* System.Void UniRx.IObserver`1<System.Object>::OnError(System.Exception) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3), (Il2CppObject*)L_0, (Exception_t1967233988 *)L_1);
		IL2CPP_LEAVE(0x1A, FINALLY_0013);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0013;
	}

FINALLY_0013:
	{ // begin finally (depth: 1)
		NullCheck((OperatorObserverBase_2_t1187768149 *)__this);
		VirtActionInvoker0::Invoke(4 /* System.Void UniRx.Operators.OperatorObserverBase`2<System.Object,System.Object>::Dispose() */, (OperatorObserverBase_2_t1187768149 *)__this);
		IL2CPP_END_FINALLY(19)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(19)
	{
		IL2CPP_JUMP_TBL(0x1A, IL_001a)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_001a:
	{
		return;
	}
}
// System.Void UniRx.Operators.SingleObservable`1/Single_<System.Object>::OnCompleted()
extern Il2CppClass* Il2CppObject_il2cpp_TypeInfo_var;
extern Il2CppClass* InvalidOperationException_t2420574324_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2687971414;
extern const uint32_t Single__OnCompleted_m2932845312_MetadataUsageId;
extern "C"  void Single__OnCompleted_m2932845312_gshared (Single__t984311384 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Single__OnCompleted_m2932845312_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Il2CppObject * V_0 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		SingleObservable_1_t427280954 * L_0 = (SingleObservable_1_t427280954 *)__this->get_parent_2();
		NullCheck(L_0);
		bool L_1 = (bool)L_0->get_useDefault_2();
		if (!L_1)
		{
			goto IL_0067;
		}
	}
	{
		bool L_2 = (bool)__this->get_seenValue_3();
		if (L_2)
		{
			goto IL_0036;
		}
	}
	{
		Il2CppObject* L_3 = (Il2CppObject*)((OperatorObserverBase_2_t1187768149 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		Initobj (Il2CppObject_il2cpp_TypeInfo_var, (&V_0));
		Il2CppObject * L_4 = V_0;
		NullCheck((Il2CppObject*)L_3);
		InterfaceActionInvoker1< Il2CppObject * >::Invoke(2 /* System.Void UniRx.IObserver`1<System.Object>::OnNext(T) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3), (Il2CppObject*)L_3, (Il2CppObject *)L_4);
		goto IL_0049;
	}

IL_0036:
	{
		Il2CppObject* L_5 = (Il2CppObject*)((OperatorObserverBase_2_t1187768149 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		Il2CppObject * L_6 = (Il2CppObject *)__this->get_lastValue_4();
		NullCheck((Il2CppObject*)L_5);
		InterfaceActionInvoker1< Il2CppObject * >::Invoke(2 /* System.Void UniRx.IObserver`1<System.Object>::OnNext(T) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3), (Il2CppObject*)L_5, (Il2CppObject *)L_6);
	}

IL_0049:
	try
	{ // begin try (depth: 1)
		Il2CppObject* L_7 = (Il2CppObject*)((OperatorObserverBase_2_t1187768149 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		NullCheck((Il2CppObject*)L_7);
		InterfaceActionInvoker0::Invoke(0 /* System.Void UniRx.IObserver`1<System.Object>::OnCompleted() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3), (Il2CppObject*)L_7);
		IL2CPP_LEAVE(0x62, FINALLY_005b);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_005b;
	}

FINALLY_005b:
	{ // begin finally (depth: 1)
		NullCheck((OperatorObserverBase_2_t1187768149 *)__this);
		VirtActionInvoker0::Invoke(4 /* System.Void UniRx.Operators.OperatorObserverBase`2<System.Object,System.Object>::Dispose() */, (OperatorObserverBase_2_t1187768149 *)__this);
		IL2CPP_END_FINALLY(91)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(91)
	{
		IL2CPP_JUMP_TBL(0x62, IL_0062)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0062:
	{
		goto IL_00c6;
	}

IL_0067:
	{
		bool L_8 = (bool)__this->get_seenValue_3();
		if (L_8)
		{
			goto IL_009a;
		}
	}

IL_0072:
	try
	{ // begin try (depth: 1)
		Il2CppObject* L_9 = (Il2CppObject*)((OperatorObserverBase_2_t1187768149 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		InvalidOperationException_t2420574324 * L_10 = (InvalidOperationException_t2420574324 *)il2cpp_codegen_object_new(InvalidOperationException_t2420574324_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_10, (String_t*)_stringLiteral2687971414, /*hidden argument*/NULL);
		NullCheck((Il2CppObject*)L_9);
		InterfaceActionInvoker1< Exception_t1967233988 * >::Invoke(1 /* System.Void UniRx.IObserver`1<System.Object>::OnError(System.Exception) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3), (Il2CppObject*)L_9, (Exception_t1967233988 *)L_10);
		IL2CPP_LEAVE(0x95, FINALLY_008e);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_008e;
	}

FINALLY_008e:
	{ // begin finally (depth: 1)
		NullCheck((OperatorObserverBase_2_t1187768149 *)__this);
		VirtActionInvoker0::Invoke(4 /* System.Void UniRx.Operators.OperatorObserverBase`2<System.Object,System.Object>::Dispose() */, (OperatorObserverBase_2_t1187768149 *)__this);
		IL2CPP_END_FINALLY(142)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(142)
	{
		IL2CPP_JUMP_TBL(0x95, IL_0095)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0095:
	{
		goto IL_00c6;
	}

IL_009a:
	{
		Il2CppObject* L_11 = (Il2CppObject*)((OperatorObserverBase_2_t1187768149 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		Il2CppObject * L_12 = (Il2CppObject *)__this->get_lastValue_4();
		NullCheck((Il2CppObject*)L_11);
		InterfaceActionInvoker1< Il2CppObject * >::Invoke(2 /* System.Void UniRx.IObserver`1<System.Object>::OnNext(T) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3), (Il2CppObject*)L_11, (Il2CppObject *)L_12);
	}

IL_00ad:
	try
	{ // begin try (depth: 1)
		Il2CppObject* L_13 = (Il2CppObject*)((OperatorObserverBase_2_t1187768149 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		NullCheck((Il2CppObject*)L_13);
		InterfaceActionInvoker0::Invoke(0 /* System.Void UniRx.IObserver`1<System.Object>::OnCompleted() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3), (Il2CppObject*)L_13);
		IL2CPP_LEAVE(0xC6, FINALLY_00bf);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_00bf;
	}

FINALLY_00bf:
	{ // begin finally (depth: 1)
		NullCheck((OperatorObserverBase_2_t1187768149 *)__this);
		VirtActionInvoker0::Invoke(4 /* System.Void UniRx.Operators.OperatorObserverBase`2<System.Object,System.Object>::Dispose() */, (OperatorObserverBase_2_t1187768149 *)__this);
		IL2CPP_END_FINALLY(191)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(191)
	{
		IL2CPP_JUMP_TBL(0xC6, IL_00c6)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_00c6:
	{
		return;
	}
}
// System.Void UniRx.Operators.SingleObservable`1/Single<System.Object>::.ctor(UniRx.Operators.SingleObservable`1<T>,UniRx.IObserver`1<T>,System.IDisposable)
extern "C"  void Single__ctor_m1558334988_gshared (Single_t2700140449 * __this, SingleObservable_1_t427280954 * ___parent0, Il2CppObject* ___observer1, Il2CppObject * ___cancel2, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = ___observer1;
		Il2CppObject * L_1 = ___cancel2;
		NullCheck((OperatorObserverBase_2_t1187768149 *)__this);
		((  void (*) (OperatorObserverBase_2_t1187768149 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((OperatorObserverBase_2_t1187768149 *)__this, (Il2CppObject*)L_0, (Il2CppObject *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		SingleObservable_1_t427280954 * L_2 = ___parent0;
		__this->set_parent_2(L_2);
		__this->set_seenValue_3((bool)0);
		return;
	}
}
// System.Void UniRx.Operators.SingleObservable`1/Single<System.Object>::OnNext(T)
extern Il2CppClass* InvalidOperationException_t2420574324_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1683804364;
extern const uint32_t Single_OnNext_m3043659679_MetadataUsageId;
extern "C"  void Single_OnNext_m3043659679_gshared (Single_t2700140449 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Single_OnNext_m3043659679_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		bool L_0 = (bool)__this->get_seenValue_3();
		if (!L_0)
		{
			goto IL_0033;
		}
	}

IL_000b:
	try
	{ // begin try (depth: 1)
		Il2CppObject* L_1 = (Il2CppObject*)((OperatorObserverBase_2_t1187768149 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		InvalidOperationException_t2420574324 * L_2 = (InvalidOperationException_t2420574324 *)il2cpp_codegen_object_new(InvalidOperationException_t2420574324_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_2, (String_t*)_stringLiteral1683804364, /*hidden argument*/NULL);
		NullCheck((Il2CppObject*)L_1);
		InterfaceActionInvoker1< Exception_t1967233988 * >::Invoke(1 /* System.Void UniRx.IObserver`1<System.Object>::OnError(System.Exception) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), (Il2CppObject*)L_1, (Exception_t1967233988 *)L_2);
		IL2CPP_LEAVE(0x2E, FINALLY_0027);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0027;
	}

FINALLY_0027:
	{ // begin finally (depth: 1)
		NullCheck((OperatorObserverBase_2_t1187768149 *)__this);
		VirtActionInvoker0::Invoke(4 /* System.Void UniRx.Operators.OperatorObserverBase`2<System.Object,System.Object>::Dispose() */, (OperatorObserverBase_2_t1187768149 *)__this);
		IL2CPP_END_FINALLY(39)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(39)
	{
		IL2CPP_JUMP_TBL(0x2E, IL_002e)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_002e:
	{
		goto IL_0041;
	}

IL_0033:
	{
		__this->set_seenValue_3((bool)1);
		Il2CppObject * L_3 = ___value0;
		__this->set_lastValue_4(L_3);
	}

IL_0041:
	{
		return;
	}
}
// System.Void UniRx.Operators.SingleObservable`1/Single<System.Object>::OnError(System.Exception)
extern "C"  void Single_OnError_m2301725870_gshared (Single_t2700140449 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method)
{
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		Il2CppObject* L_0 = (Il2CppObject*)((OperatorObserverBase_2_t1187768149 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		Exception_t1967233988 * L_1 = ___error0;
		NullCheck((Il2CppObject*)L_0);
		InterfaceActionInvoker1< Exception_t1967233988 * >::Invoke(1 /* System.Void UniRx.IObserver`1<System.Object>::OnError(System.Exception) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), (Il2CppObject*)L_0, (Exception_t1967233988 *)L_1);
		IL2CPP_LEAVE(0x1A, FINALLY_0013);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0013;
	}

FINALLY_0013:
	{ // begin finally (depth: 1)
		NullCheck((OperatorObserverBase_2_t1187768149 *)__this);
		VirtActionInvoker0::Invoke(4 /* System.Void UniRx.Operators.OperatorObserverBase`2<System.Object,System.Object>::Dispose() */, (OperatorObserverBase_2_t1187768149 *)__this);
		IL2CPP_END_FINALLY(19)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(19)
	{
		IL2CPP_JUMP_TBL(0x1A, IL_001a)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_001a:
	{
		return;
	}
}
// System.Void UniRx.Operators.SingleObservable`1/Single<System.Object>::OnCompleted()
extern Il2CppClass* Il2CppObject_il2cpp_TypeInfo_var;
extern Il2CppClass* InvalidOperationException_t2420574324_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2687971414;
extern const uint32_t Single_OnCompleted_m760960257_MetadataUsageId;
extern "C"  void Single_OnCompleted_m760960257_gshared (Single_t2700140449 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Single_OnCompleted_m760960257_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Il2CppObject * V_0 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		SingleObservable_1_t427280954 * L_0 = (SingleObservable_1_t427280954 *)__this->get_parent_2();
		NullCheck(L_0);
		bool L_1 = (bool)L_0->get_useDefault_2();
		if (!L_1)
		{
			goto IL_0067;
		}
	}
	{
		bool L_2 = (bool)__this->get_seenValue_3();
		if (L_2)
		{
			goto IL_0036;
		}
	}
	{
		Il2CppObject* L_3 = (Il2CppObject*)((OperatorObserverBase_2_t1187768149 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		Initobj (Il2CppObject_il2cpp_TypeInfo_var, (&V_0));
		Il2CppObject * L_4 = V_0;
		NullCheck((Il2CppObject*)L_3);
		InterfaceActionInvoker1< Il2CppObject * >::Invoke(2 /* System.Void UniRx.IObserver`1<System.Object>::OnNext(T) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), (Il2CppObject*)L_3, (Il2CppObject *)L_4);
		goto IL_0049;
	}

IL_0036:
	{
		Il2CppObject* L_5 = (Il2CppObject*)((OperatorObserverBase_2_t1187768149 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		Il2CppObject * L_6 = (Il2CppObject *)__this->get_lastValue_4();
		NullCheck((Il2CppObject*)L_5);
		InterfaceActionInvoker1< Il2CppObject * >::Invoke(2 /* System.Void UniRx.IObserver`1<System.Object>::OnNext(T) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), (Il2CppObject*)L_5, (Il2CppObject *)L_6);
	}

IL_0049:
	try
	{ // begin try (depth: 1)
		Il2CppObject* L_7 = (Il2CppObject*)((OperatorObserverBase_2_t1187768149 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		NullCheck((Il2CppObject*)L_7);
		InterfaceActionInvoker0::Invoke(0 /* System.Void UniRx.IObserver`1<System.Object>::OnCompleted() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), (Il2CppObject*)L_7);
		IL2CPP_LEAVE(0x62, FINALLY_005b);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_005b;
	}

FINALLY_005b:
	{ // begin finally (depth: 1)
		NullCheck((OperatorObserverBase_2_t1187768149 *)__this);
		VirtActionInvoker0::Invoke(4 /* System.Void UniRx.Operators.OperatorObserverBase`2<System.Object,System.Object>::Dispose() */, (OperatorObserverBase_2_t1187768149 *)__this);
		IL2CPP_END_FINALLY(91)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(91)
	{
		IL2CPP_JUMP_TBL(0x62, IL_0062)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0062:
	{
		goto IL_00c6;
	}

IL_0067:
	{
		bool L_8 = (bool)__this->get_seenValue_3();
		if (L_8)
		{
			goto IL_009a;
		}
	}

IL_0072:
	try
	{ // begin try (depth: 1)
		Il2CppObject* L_9 = (Il2CppObject*)((OperatorObserverBase_2_t1187768149 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		InvalidOperationException_t2420574324 * L_10 = (InvalidOperationException_t2420574324 *)il2cpp_codegen_object_new(InvalidOperationException_t2420574324_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_10, (String_t*)_stringLiteral2687971414, /*hidden argument*/NULL);
		NullCheck((Il2CppObject*)L_9);
		InterfaceActionInvoker1< Exception_t1967233988 * >::Invoke(1 /* System.Void UniRx.IObserver`1<System.Object>::OnError(System.Exception) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), (Il2CppObject*)L_9, (Exception_t1967233988 *)L_10);
		IL2CPP_LEAVE(0x95, FINALLY_008e);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_008e;
	}

FINALLY_008e:
	{ // begin finally (depth: 1)
		NullCheck((OperatorObserverBase_2_t1187768149 *)__this);
		VirtActionInvoker0::Invoke(4 /* System.Void UniRx.Operators.OperatorObserverBase`2<System.Object,System.Object>::Dispose() */, (OperatorObserverBase_2_t1187768149 *)__this);
		IL2CPP_END_FINALLY(142)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(142)
	{
		IL2CPP_JUMP_TBL(0x95, IL_0095)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0095:
	{
		goto IL_00c6;
	}

IL_009a:
	{
		Il2CppObject* L_11 = (Il2CppObject*)((OperatorObserverBase_2_t1187768149 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		Il2CppObject * L_12 = (Il2CppObject *)__this->get_lastValue_4();
		NullCheck((Il2CppObject*)L_11);
		InterfaceActionInvoker1< Il2CppObject * >::Invoke(2 /* System.Void UniRx.IObserver`1<System.Object>::OnNext(T) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), (Il2CppObject*)L_11, (Il2CppObject *)L_12);
	}

IL_00ad:
	try
	{ // begin try (depth: 1)
		Il2CppObject* L_13 = (Il2CppObject*)((OperatorObserverBase_2_t1187768149 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		NullCheck((Il2CppObject*)L_13);
		InterfaceActionInvoker0::Invoke(0 /* System.Void UniRx.IObserver`1<System.Object>::OnCompleted() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), (Il2CppObject*)L_13);
		IL2CPP_LEAVE(0xC6, FINALLY_00bf);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_00bf;
	}

FINALLY_00bf:
	{ // begin finally (depth: 1)
		NullCheck((OperatorObserverBase_2_t1187768149 *)__this);
		VirtActionInvoker0::Invoke(4 /* System.Void UniRx.Operators.OperatorObserverBase`2<System.Object,System.Object>::Dispose() */, (OperatorObserverBase_2_t1187768149 *)__this);
		IL2CPP_END_FINALLY(191)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(191)
	{
		IL2CPP_JUMP_TBL(0xC6, IL_00c6)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_00c6:
	{
		return;
	}
}
// System.Void UniRx.Operators.SingleObservable`1<System.Object>::.ctor(UniRx.IObservable`1<T>,System.Boolean)
extern "C"  void SingleObservable_1__ctor_m3653257072_gshared (SingleObservable_1_t427280954 * __this, Il2CppObject* ___source0, bool ___useDefault1, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = ___source0;
		bool L_1 = ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)(NULL /*static, unused*/, (Il2CppObject*)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		NullCheck((OperatorObservableBase_1_t4196218687 *)__this);
		((  void (*) (OperatorObservableBase_1_t4196218687 *, bool, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)((OperatorObservableBase_1_t4196218687 *)__this, (bool)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		Il2CppObject* L_2 = ___source0;
		__this->set_source_1(L_2);
		bool L_3 = ___useDefault1;
		__this->set_useDefault_2(L_3);
		return;
	}
}
// System.Void UniRx.Operators.SingleObservable`1<System.Object>::.ctor(UniRx.IObservable`1<T>,System.Func`2<T,System.Boolean>,System.Boolean)
extern "C"  void SingleObservable_1__ctor_m1677592730_gshared (SingleObservable_1_t427280954 * __this, Il2CppObject* ___source0, Func_2_t1509682273 * ___predicate1, bool ___useDefault2, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = ___source0;
		bool L_1 = ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)(NULL /*static, unused*/, (Il2CppObject*)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		NullCheck((OperatorObservableBase_1_t4196218687 *)__this);
		((  void (*) (OperatorObservableBase_1_t4196218687 *, bool, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)((OperatorObservableBase_1_t4196218687 *)__this, (bool)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		Il2CppObject* L_2 = ___source0;
		__this->set_source_1(L_2);
		Func_2_t1509682273 * L_3 = ___predicate1;
		__this->set_predicate_3(L_3);
		bool L_4 = ___useDefault2;
		__this->set_useDefault_2(L_4);
		return;
	}
}
// System.IDisposable UniRx.Operators.SingleObservable`1<System.Object>::SubscribeCore(UniRx.IObserver`1<T>,System.IDisposable)
extern "C"  Il2CppObject * SingleObservable_1_SubscribeCore_m2139713338_gshared (SingleObservable_1_t427280954 * __this, Il2CppObject* ___observer0, Il2CppObject * ___cancel1, const MethodInfo* method)
{
	{
		Func_2_t1509682273 * L_0 = (Func_2_t1509682273 *)__this->get_predicate_3();
		if (L_0)
		{
			goto IL_001f;
		}
	}
	{
		Il2CppObject* L_1 = (Il2CppObject*)__this->get_source_1();
		Il2CppObject* L_2 = ___observer0;
		Il2CppObject * L_3 = ___cancel1;
		Single_t2700140449 * L_4 = (Single_t2700140449 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		((  void (*) (Single_t2700140449 *, SingleObservable_1_t427280954 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)(L_4, (SingleObservable_1_t427280954 *)__this, (Il2CppObject*)L_2, (Il2CppObject *)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		NullCheck((Il2CppObject*)L_1);
		Il2CppObject * L_5 = InterfaceFuncInvoker1< Il2CppObject *, Il2CppObject* >::Invoke(0 /* System.IDisposable UniRx.IObservable`1<System.Object>::Subscribe(UniRx.IObserver`1<T>) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5), (Il2CppObject*)L_1, (Il2CppObject*)L_4);
		return L_5;
	}

IL_001f:
	{
		Il2CppObject* L_6 = (Il2CppObject*)__this->get_source_1();
		Il2CppObject* L_7 = ___observer0;
		Il2CppObject * L_8 = ___cancel1;
		Single__t984311384 * L_9 = (Single__t984311384 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		((  void (*) (Single__t984311384 *, SingleObservable_1_t427280954 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7)->method)(L_9, (SingleObservable_1_t427280954 *)__this, (Il2CppObject*)L_7, (Il2CppObject *)L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7));
		NullCheck((Il2CppObject*)L_6);
		Il2CppObject * L_10 = InterfaceFuncInvoker1< Il2CppObject *, Il2CppObject* >::Invoke(0 /* System.IDisposable UniRx.IObservable`1<System.Object>::Subscribe(UniRx.IObserver`1<T>) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5), (Il2CppObject*)L_6, (Il2CppObject*)L_9);
		return L_10;
	}
}
// System.Void UniRx.Operators.SkipObservable`1/Skip_<System.Boolean>::.ctor(UniRx.Operators.SkipObservable`1<T>,UniRx.IObserver`1<T>,System.IDisposable)
extern "C"  void Skip___ctor_m2548371359_gshared (Skip__t1751828930 * __this, SkipObservable_1_t197279106 * ___parent0, Il2CppObject* ___observer1, Il2CppObject * ___cancel2, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = ___observer1;
		Il2CppObject * L_1 = ___cancel2;
		NullCheck((OperatorObserverBase_2_t60103313 *)__this);
		((  void (*) (OperatorObserverBase_2_t60103313 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((OperatorObserverBase_2_t60103313 *)__this, (Il2CppObject*)L_0, (Il2CppObject *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		SkipObservable_1_t197279106 * L_2 = ___parent0;
		__this->set_parent_2(L_2);
		return;
	}
}
// System.IDisposable UniRx.Operators.SkipObservable`1/Skip_<System.Boolean>::Run()
extern Il2CppClass* Action_t437523947_il2cpp_TypeInfo_var;
extern Il2CppClass* IScheduler_t2938318244_il2cpp_TypeInfo_var;
extern const uint32_t Skip__Run_m948916905_MetadataUsageId;
extern "C"  Il2CppObject * Skip__Run_m948916905_gshared (Skip__t1751828930 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Skip__Run_m948916905_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Il2CppObject * V_0 = NULL;
	Il2CppObject * V_1 = NULL;
	{
		SkipObservable_1_t197279106 * L_0 = (SkipObservable_1_t197279106 *)__this->get_parent_2();
		NullCheck(L_0);
		Il2CppObject * L_1 = (Il2CppObject *)L_0->get_scheduler_4();
		SkipObservable_1_t197279106 * L_2 = (SkipObservable_1_t197279106 *)__this->get_parent_2();
		NullCheck(L_2);
		TimeSpan_t763862892  L_3 = (TimeSpan_t763862892 )L_2->get_duration_3();
		IntPtr_t L_4;
		L_4.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		Action_t437523947 * L_5 = (Action_t437523947 *)il2cpp_codegen_object_new(Action_t437523947_il2cpp_TypeInfo_var);
		Action__ctor_m2957240604(L_5, (Il2CppObject *)__this, (IntPtr_t)L_4, /*hidden argument*/NULL);
		NullCheck((Il2CppObject *)L_1);
		Il2CppObject * L_6 = InterfaceFuncInvoker2< Il2CppObject *, TimeSpan_t763862892 , Action_t437523947 * >::Invoke(2 /* System.IDisposable UniRx.IScheduler::Schedule(System.TimeSpan,System.Action) */, IScheduler_t2938318244_il2cpp_TypeInfo_var, (Il2CppObject *)L_1, (TimeSpan_t763862892 )L_3, (Action_t437523947 *)L_5);
		V_0 = (Il2CppObject *)L_6;
		SkipObservable_1_t197279106 * L_7 = (SkipObservable_1_t197279106 *)__this->get_parent_2();
		NullCheck(L_7);
		Il2CppObject* L_8 = (Il2CppObject*)L_7->get_source_1();
		NullCheck((Il2CppObject*)L_8);
		Il2CppObject * L_9 = InterfaceFuncInvoker1< Il2CppObject *, Il2CppObject* >::Invoke(0 /* System.IDisposable UniRx.IObservable`1<System.Boolean>::Subscribe(UniRx.IObserver`1<T>) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3), (Il2CppObject*)L_8, (Il2CppObject*)__this);
		V_1 = (Il2CppObject *)L_9;
		Il2CppObject * L_10 = V_0;
		Il2CppObject * L_11 = V_1;
		Il2CppObject * L_12 = StableCompositeDisposable_Create_m3764788535(NULL /*static, unused*/, (Il2CppObject *)L_10, (Il2CppObject *)L_11, /*hidden argument*/NULL);
		return L_12;
	}
}
// System.Void UniRx.Operators.SkipObservable`1/Skip_<System.Boolean>::Tick()
extern "C"  void Skip__Tick_m129718054_gshared (Skip__t1751828930 * __this, const MethodInfo* method)
{
	{
		il2cpp_codegen_memory_barrier();
		__this->set_open_3((bool)1);
		return;
	}
}
// System.Void UniRx.Operators.SkipObservable`1/Skip_<System.Boolean>::OnNext(T)
extern "C"  void Skip__OnNext_m2273076483_gshared (Skip__t1751828930 * __this, bool ___value0, const MethodInfo* method)
{
	{
		bool L_0 = (bool)__this->get_open_3();
		il2cpp_codegen_memory_barrier();
		if (!L_0)
		{
			goto IL_001b;
		}
	}
	{
		Il2CppObject* L_1 = (Il2CppObject*)((OperatorObserverBase_2_t60103313 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		bool L_2 = ___value0;
		NullCheck((Il2CppObject*)L_1);
		InterfaceActionInvoker1< bool >::Invoke(2 /* System.Void UniRx.IObserver`1<System.Boolean>::OnNext(T) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4), (Il2CppObject*)L_1, (bool)L_2);
	}

IL_001b:
	{
		return;
	}
}
// System.Void UniRx.Operators.SkipObservable`1/Skip_<System.Boolean>::OnError(System.Exception)
extern "C"  void Skip__OnError_m4048820754_gshared (Skip__t1751828930 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method)
{
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		Il2CppObject* L_0 = (Il2CppObject*)((OperatorObserverBase_2_t60103313 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		Exception_t1967233988 * L_1 = ___error0;
		NullCheck((Il2CppObject*)L_0);
		InterfaceActionInvoker1< Exception_t1967233988 * >::Invoke(1 /* System.Void UniRx.IObserver`1<System.Boolean>::OnError(System.Exception) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4), (Il2CppObject*)L_0, (Exception_t1967233988 *)L_1);
		IL2CPP_LEAVE(0x1A, FINALLY_0013);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0013;
	}

FINALLY_0013:
	{ // begin finally (depth: 1)
		NullCheck((OperatorObserverBase_2_t60103313 *)__this);
		VirtActionInvoker0::Invoke(4 /* System.Void UniRx.Operators.OperatorObserverBase`2<System.Boolean,System.Boolean>::Dispose() */, (OperatorObserverBase_2_t60103313 *)__this);
		IL2CPP_END_FINALLY(19)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(19)
	{
		IL2CPP_JUMP_TBL(0x1A, IL_001a)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_001a:
	{
		return;
	}
}
// System.Void UniRx.Operators.SkipObservable`1/Skip_<System.Boolean>::OnCompleted()
extern "C"  void Skip__OnCompleted_m1308350565_gshared (Skip__t1751828930 * __this, const MethodInfo* method)
{
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		Il2CppObject* L_0 = (Il2CppObject*)((OperatorObserverBase_2_t60103313 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		NullCheck((Il2CppObject*)L_0);
		InterfaceActionInvoker0::Invoke(0 /* System.Void UniRx.IObserver`1<System.Boolean>::OnCompleted() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4), (Il2CppObject*)L_0);
		IL2CPP_LEAVE(0x19, FINALLY_0012);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0012;
	}

FINALLY_0012:
	{ // begin finally (depth: 1)
		NullCheck((OperatorObserverBase_2_t60103313 *)__this);
		VirtActionInvoker0::Invoke(4 /* System.Void UniRx.Operators.OperatorObserverBase`2<System.Boolean,System.Boolean>::Dispose() */, (OperatorObserverBase_2_t60103313 *)__this);
		IL2CPP_END_FINALLY(18)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(18)
	{
		IL2CPP_JUMP_TBL(0x19, IL_0019)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0019:
	{
		return;
	}
}
// System.Void UniRx.Operators.SkipObservable`1/Skip_<System.Object>::.ctor(UniRx.Operators.SkipObservable`1<T>,UniRx.IObserver`1<T>,System.IDisposable)
extern "C"  void Skip___ctor_m2222128644_gshared (Skip__t2377930009 * __this, SkipObservable_1_t823380185 * ___parent0, Il2CppObject* ___observer1, Il2CppObject * ___cancel2, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = ___observer1;
		Il2CppObject * L_1 = ___cancel2;
		NullCheck((OperatorObserverBase_2_t1187768149 *)__this);
		((  void (*) (OperatorObserverBase_2_t1187768149 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((OperatorObserverBase_2_t1187768149 *)__this, (Il2CppObject*)L_0, (Il2CppObject *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		SkipObservable_1_t823380185 * L_2 = ___parent0;
		__this->set_parent_2(L_2);
		return;
	}
}
// System.IDisposable UniRx.Operators.SkipObservable`1/Skip_<System.Object>::Run()
extern Il2CppClass* Action_t437523947_il2cpp_TypeInfo_var;
extern Il2CppClass* IScheduler_t2938318244_il2cpp_TypeInfo_var;
extern const uint32_t Skip__Run_m2515609146_MetadataUsageId;
extern "C"  Il2CppObject * Skip__Run_m2515609146_gshared (Skip__t2377930009 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Skip__Run_m2515609146_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Il2CppObject * V_0 = NULL;
	Il2CppObject * V_1 = NULL;
	{
		SkipObservable_1_t823380185 * L_0 = (SkipObservable_1_t823380185 *)__this->get_parent_2();
		NullCheck(L_0);
		Il2CppObject * L_1 = (Il2CppObject *)L_0->get_scheduler_4();
		SkipObservable_1_t823380185 * L_2 = (SkipObservable_1_t823380185 *)__this->get_parent_2();
		NullCheck(L_2);
		TimeSpan_t763862892  L_3 = (TimeSpan_t763862892 )L_2->get_duration_3();
		IntPtr_t L_4;
		L_4.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		Action_t437523947 * L_5 = (Action_t437523947 *)il2cpp_codegen_object_new(Action_t437523947_il2cpp_TypeInfo_var);
		Action__ctor_m2957240604(L_5, (Il2CppObject *)__this, (IntPtr_t)L_4, /*hidden argument*/NULL);
		NullCheck((Il2CppObject *)L_1);
		Il2CppObject * L_6 = InterfaceFuncInvoker2< Il2CppObject *, TimeSpan_t763862892 , Action_t437523947 * >::Invoke(2 /* System.IDisposable UniRx.IScheduler::Schedule(System.TimeSpan,System.Action) */, IScheduler_t2938318244_il2cpp_TypeInfo_var, (Il2CppObject *)L_1, (TimeSpan_t763862892 )L_3, (Action_t437523947 *)L_5);
		V_0 = (Il2CppObject *)L_6;
		SkipObservable_1_t823380185 * L_7 = (SkipObservable_1_t823380185 *)__this->get_parent_2();
		NullCheck(L_7);
		Il2CppObject* L_8 = (Il2CppObject*)L_7->get_source_1();
		NullCheck((Il2CppObject*)L_8);
		Il2CppObject * L_9 = InterfaceFuncInvoker1< Il2CppObject *, Il2CppObject* >::Invoke(0 /* System.IDisposable UniRx.IObservable`1<System.Object>::Subscribe(UniRx.IObserver`1<T>) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3), (Il2CppObject*)L_8, (Il2CppObject*)__this);
		V_1 = (Il2CppObject *)L_9;
		Il2CppObject * L_10 = V_0;
		Il2CppObject * L_11 = V_1;
		Il2CppObject * L_12 = StableCompositeDisposable_Create_m3764788535(NULL /*static, unused*/, (Il2CppObject *)L_10, (Il2CppObject *)L_11, /*hidden argument*/NULL);
		return L_12;
	}
}
// System.Void UniRx.Operators.SkipObservable`1/Skip_<System.Object>::Tick()
extern "C"  void Skip__Tick_m2960760075_gshared (Skip__t2377930009 * __this, const MethodInfo* method)
{
	{
		il2cpp_codegen_memory_barrier();
		__this->set_open_3((bool)1);
		return;
	}
}
// System.Void UniRx.Operators.SkipObservable`1/Skip_<System.Object>::OnNext(T)
extern "C"  void Skip__OnNext_m1573132542_gshared (Skip__t2377930009 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	{
		bool L_0 = (bool)__this->get_open_3();
		il2cpp_codegen_memory_barrier();
		if (!L_0)
		{
			goto IL_001b;
		}
	}
	{
		Il2CppObject* L_1 = (Il2CppObject*)((OperatorObserverBase_2_t1187768149 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		Il2CppObject * L_2 = ___value0;
		NullCheck((Il2CppObject*)L_1);
		InterfaceActionInvoker1< Il2CppObject * >::Invoke(2 /* System.Void UniRx.IObserver`1<System.Object>::OnNext(T) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4), (Il2CppObject*)L_1, (Il2CppObject *)L_2);
	}

IL_001b:
	{
		return;
	}
}
// System.Void UniRx.Operators.SkipObservable`1/Skip_<System.Object>::OnError(System.Exception)
extern "C"  void Skip__OnError_m912422925_gshared (Skip__t2377930009 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method)
{
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		Il2CppObject* L_0 = (Il2CppObject*)((OperatorObserverBase_2_t1187768149 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		Exception_t1967233988 * L_1 = ___error0;
		NullCheck((Il2CppObject*)L_0);
		InterfaceActionInvoker1< Exception_t1967233988 * >::Invoke(1 /* System.Void UniRx.IObserver`1<System.Object>::OnError(System.Exception) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4), (Il2CppObject*)L_0, (Exception_t1967233988 *)L_1);
		IL2CPP_LEAVE(0x1A, FINALLY_0013);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0013;
	}

FINALLY_0013:
	{ // begin finally (depth: 1)
		NullCheck((OperatorObserverBase_2_t1187768149 *)__this);
		VirtActionInvoker0::Invoke(4 /* System.Void UniRx.Operators.OperatorObserverBase`2<System.Object,System.Object>::Dispose() */, (OperatorObserverBase_2_t1187768149 *)__this);
		IL2CPP_END_FINALLY(19)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(19)
	{
		IL2CPP_JUMP_TBL(0x1A, IL_001a)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_001a:
	{
		return;
	}
}
// System.Void UniRx.Operators.SkipObservable`1/Skip_<System.Object>::OnCompleted()
extern "C"  void Skip__OnCompleted_m2432898784_gshared (Skip__t2377930009 * __this, const MethodInfo* method)
{
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		Il2CppObject* L_0 = (Il2CppObject*)((OperatorObserverBase_2_t1187768149 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		NullCheck((Il2CppObject*)L_0);
		InterfaceActionInvoker0::Invoke(0 /* System.Void UniRx.IObserver`1<System.Object>::OnCompleted() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4), (Il2CppObject*)L_0);
		IL2CPP_LEAVE(0x19, FINALLY_0012);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0012;
	}

FINALLY_0012:
	{ // begin finally (depth: 1)
		NullCheck((OperatorObserverBase_2_t1187768149 *)__this);
		VirtActionInvoker0::Invoke(4 /* System.Void UniRx.Operators.OperatorObserverBase`2<System.Object,System.Object>::Dispose() */, (OperatorObserverBase_2_t1187768149 *)__this);
		IL2CPP_END_FINALLY(18)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(18)
	{
		IL2CPP_JUMP_TBL(0x19, IL_0019)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0019:
	{
		return;
	}
}
// System.Void UniRx.Operators.SkipObservable`1/Skip<System.Boolean>::.ctor(UniRx.Operators.SkipObservable`1<T>,UniRx.IObserver`1<T>,System.IDisposable)
extern "C"  void Skip__ctor_m3668764960_gshared (Skip_t1564805481 * __this, SkipObservable_1_t197279106 * ___parent0, Il2CppObject* ___observer1, Il2CppObject * ___cancel2, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = ___observer1;
		Il2CppObject * L_1 = ___cancel2;
		NullCheck((OperatorObserverBase_2_t60103313 *)__this);
		((  void (*) (OperatorObserverBase_2_t60103313 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((OperatorObserverBase_2_t60103313 *)__this, (Il2CppObject*)L_0, (Il2CppObject *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		SkipObservable_1_t197279106 * L_2 = ___parent0;
		NullCheck(L_2);
		int32_t L_3 = (int32_t)L_2->get_count_2();
		__this->set_remaining_2(L_3);
		return;
	}
}
// System.Void UniRx.Operators.SkipObservable`1/Skip<System.Boolean>::OnNext(T)
extern "C"  void Skip_OnNext_m1512644194_gshared (Skip_t1564805481 * __this, bool ___value0, const MethodInfo* method)
{
	{
		int32_t L_0 = (int32_t)__this->get_remaining_2();
		if ((((int32_t)L_0) > ((int32_t)0)))
		{
			goto IL_001f;
		}
	}
	{
		Il2CppObject* L_1 = (Il2CppObject*)((OperatorObserverBase_2_t60103313 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		bool L_2 = ___value0;
		NullCheck((Il2CppObject*)L_1);
		InterfaceActionInvoker1< bool >::Invoke(2 /* System.Void UniRx.IObserver`1<System.Boolean>::OnNext(T) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), (Il2CppObject*)L_1, (bool)L_2);
		goto IL_002d;
	}

IL_001f:
	{
		int32_t L_3 = (int32_t)__this->get_remaining_2();
		__this->set_remaining_2(((int32_t)((int32_t)L_3-(int32_t)1)));
	}

IL_002d:
	{
		return;
	}
}
// System.Void UniRx.Operators.SkipObservable`1/Skip<System.Boolean>::OnError(System.Exception)
extern "C"  void Skip_OnError_m2625123697_gshared (Skip_t1564805481 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method)
{
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		Il2CppObject* L_0 = (Il2CppObject*)((OperatorObserverBase_2_t60103313 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		Exception_t1967233988 * L_1 = ___error0;
		NullCheck((Il2CppObject*)L_0);
		InterfaceActionInvoker1< Exception_t1967233988 * >::Invoke(1 /* System.Void UniRx.IObserver`1<System.Boolean>::OnError(System.Exception) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), (Il2CppObject*)L_0, (Exception_t1967233988 *)L_1);
		IL2CPP_LEAVE(0x1A, FINALLY_0013);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0013;
	}

FINALLY_0013:
	{ // begin finally (depth: 1)
		NullCheck((OperatorObserverBase_2_t60103313 *)__this);
		VirtActionInvoker0::Invoke(4 /* System.Void UniRx.Operators.OperatorObserverBase`2<System.Boolean,System.Boolean>::Dispose() */, (OperatorObserverBase_2_t60103313 *)__this);
		IL2CPP_END_FINALLY(19)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(19)
	{
		IL2CPP_JUMP_TBL(0x1A, IL_001a)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_001a:
	{
		return;
	}
}
// System.Void UniRx.Operators.SkipObservable`1/Skip<System.Boolean>::OnCompleted()
extern "C"  void Skip_OnCompleted_m517917252_gshared (Skip_t1564805481 * __this, const MethodInfo* method)
{
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		Il2CppObject* L_0 = (Il2CppObject*)((OperatorObserverBase_2_t60103313 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		NullCheck((Il2CppObject*)L_0);
		InterfaceActionInvoker0::Invoke(0 /* System.Void UniRx.IObserver`1<System.Boolean>::OnCompleted() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), (Il2CppObject*)L_0);
		IL2CPP_LEAVE(0x19, FINALLY_0012);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0012;
	}

FINALLY_0012:
	{ // begin finally (depth: 1)
		NullCheck((OperatorObserverBase_2_t60103313 *)__this);
		VirtActionInvoker0::Invoke(4 /* System.Void UniRx.Operators.OperatorObserverBase`2<System.Boolean,System.Boolean>::Dispose() */, (OperatorObserverBase_2_t60103313 *)__this);
		IL2CPP_END_FINALLY(18)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(18)
	{
		IL2CPP_JUMP_TBL(0x19, IL_0019)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0019:
	{
		return;
	}
}
// System.Void UniRx.Operators.SkipObservable`1/Skip<System.Object>::.ctor(UniRx.Operators.SkipObservable`1<T>,UniRx.IObserver`1<T>,System.IDisposable)
extern "C"  void Skip__ctor_m41513059_gshared (Skip_t2190906560 * __this, SkipObservable_1_t823380185 * ___parent0, Il2CppObject* ___observer1, Il2CppObject * ___cancel2, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = ___observer1;
		Il2CppObject * L_1 = ___cancel2;
		NullCheck((OperatorObserverBase_2_t1187768149 *)__this);
		((  void (*) (OperatorObserverBase_2_t1187768149 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((OperatorObserverBase_2_t1187768149 *)__this, (Il2CppObject*)L_0, (Il2CppObject *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		SkipObservable_1_t823380185 * L_2 = ___parent0;
		NullCheck(L_2);
		int32_t L_3 = (int32_t)L_2->get_count_2();
		__this->set_remaining_2(L_3);
		return;
	}
}
// System.Void UniRx.Operators.SkipObservable`1/Skip<System.Object>::OnNext(T)
extern "C"  void Skip_OnNext_m301676479_gshared (Skip_t2190906560 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	{
		int32_t L_0 = (int32_t)__this->get_remaining_2();
		if ((((int32_t)L_0) > ((int32_t)0)))
		{
			goto IL_001f;
		}
	}
	{
		Il2CppObject* L_1 = (Il2CppObject*)((OperatorObserverBase_2_t1187768149 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		Il2CppObject * L_2 = ___value0;
		NullCheck((Il2CppObject*)L_1);
		InterfaceActionInvoker1< Il2CppObject * >::Invoke(2 /* System.Void UniRx.IObserver`1<System.Object>::OnNext(T) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), (Il2CppObject*)L_1, (Il2CppObject *)L_2);
		goto IL_002d;
	}

IL_001f:
	{
		int32_t L_3 = (int32_t)__this->get_remaining_2();
		__this->set_remaining_2(((int32_t)((int32_t)L_3-(int32_t)1)));
	}

IL_002d:
	{
		return;
	}
}
// System.Void UniRx.Operators.SkipObservable`1/Skip<System.Object>::OnError(System.Exception)
extern "C"  void Skip_OnError_m1420686542_gshared (Skip_t2190906560 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method)
{
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		Il2CppObject* L_0 = (Il2CppObject*)((OperatorObserverBase_2_t1187768149 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		Exception_t1967233988 * L_1 = ___error0;
		NullCheck((Il2CppObject*)L_0);
		InterfaceActionInvoker1< Exception_t1967233988 * >::Invoke(1 /* System.Void UniRx.IObserver`1<System.Object>::OnError(System.Exception) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), (Il2CppObject*)L_0, (Exception_t1967233988 *)L_1);
		IL2CPP_LEAVE(0x1A, FINALLY_0013);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0013;
	}

FINALLY_0013:
	{ // begin finally (depth: 1)
		NullCheck((OperatorObserverBase_2_t1187768149 *)__this);
		VirtActionInvoker0::Invoke(4 /* System.Void UniRx.Operators.OperatorObserverBase`2<System.Object,System.Object>::Dispose() */, (OperatorObserverBase_2_t1187768149 *)__this);
		IL2CPP_END_FINALLY(19)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(19)
	{
		IL2CPP_JUMP_TBL(0x1A, IL_001a)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_001a:
	{
		return;
	}
}
// System.Void UniRx.Operators.SkipObservable`1/Skip<System.Object>::OnCompleted()
extern "C"  void Skip_OnCompleted_m52096289_gshared (Skip_t2190906560 * __this, const MethodInfo* method)
{
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		Il2CppObject* L_0 = (Il2CppObject*)((OperatorObserverBase_2_t1187768149 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		NullCheck((Il2CppObject*)L_0);
		InterfaceActionInvoker0::Invoke(0 /* System.Void UniRx.IObserver`1<System.Object>::OnCompleted() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), (Il2CppObject*)L_0);
		IL2CPP_LEAVE(0x19, FINALLY_0012);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0012;
	}

FINALLY_0012:
	{ // begin finally (depth: 1)
		NullCheck((OperatorObserverBase_2_t1187768149 *)__this);
		VirtActionInvoker0::Invoke(4 /* System.Void UniRx.Operators.OperatorObserverBase`2<System.Object,System.Object>::Dispose() */, (OperatorObserverBase_2_t1187768149 *)__this);
		IL2CPP_END_FINALLY(18)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(18)
	{
		IL2CPP_JUMP_TBL(0x19, IL_0019)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0019:
	{
		return;
	}
}
// System.Void UniRx.Operators.SkipObservable`1<System.Boolean>::.ctor(UniRx.IObservable`1<T>,System.Int32)
extern "C"  void SkipObservable_1__ctor_m1792269452_gshared (SkipObservable_1_t197279106 * __this, Il2CppObject* ___source0, int32_t ___count1, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = ___source0;
		bool L_1 = ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)(NULL /*static, unused*/, (Il2CppObject*)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		NullCheck((OperatorObservableBase_1_t3570117608 *)__this);
		((  void (*) (OperatorObservableBase_1_t3570117608 *, bool, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)((OperatorObservableBase_1_t3570117608 *)__this, (bool)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		Il2CppObject* L_2 = ___source0;
		__this->set_source_1(L_2);
		int32_t L_3 = ___count1;
		__this->set_count_2(L_3);
		return;
	}
}
// System.Void UniRx.Operators.SkipObservable`1<System.Boolean>::.ctor(UniRx.IObservable`1<T>,System.TimeSpan,UniRx.IScheduler)
extern Il2CppClass* Scheduler_t103934925_il2cpp_TypeInfo_var;
extern const uint32_t SkipObservable_1__ctor_m3503003963_MetadataUsageId;
extern "C"  void SkipObservable_1__ctor_m3503003963_gshared (SkipObservable_1_t197279106 * __this, Il2CppObject* ___source0, TimeSpan_t763862892  ___duration1, Il2CppObject * ___scheduler2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SkipObservable_1__ctor_m3503003963_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	SkipObservable_1_t197279106 * G_B2_0 = NULL;
	SkipObservable_1_t197279106 * G_B1_0 = NULL;
	int32_t G_B3_0 = 0;
	SkipObservable_1_t197279106 * G_B3_1 = NULL;
	{
		Il2CppObject * L_0 = ___scheduler2;
		IL2CPP_RUNTIME_CLASS_INIT(Scheduler_t103934925_il2cpp_TypeInfo_var);
		Il2CppObject * L_1 = ((Scheduler_t103934925_StaticFields*)Scheduler_t103934925_il2cpp_TypeInfo_var->static_fields)->get_CurrentThread_0();
		G_B1_0 = ((SkipObservable_1_t197279106 *)(__this));
		if ((((Il2CppObject*)(Il2CppObject *)L_0) == ((Il2CppObject*)(Il2CppObject *)L_1)))
		{
			G_B2_0 = ((SkipObservable_1_t197279106 *)(__this));
			goto IL_0014;
		}
	}
	{
		Il2CppObject* L_2 = ___source0;
		bool L_3 = ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)(NULL /*static, unused*/, (Il2CppObject*)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		G_B3_0 = ((int32_t)(L_3));
		G_B3_1 = ((SkipObservable_1_t197279106 *)(G_B1_0));
		goto IL_0015;
	}

IL_0014:
	{
		G_B3_0 = 1;
		G_B3_1 = ((SkipObservable_1_t197279106 *)(G_B2_0));
	}

IL_0015:
	{
		NullCheck((OperatorObservableBase_1_t3570117608 *)G_B3_1);
		((  void (*) (OperatorObservableBase_1_t3570117608 *, bool, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)((OperatorObservableBase_1_t3570117608 *)G_B3_1, (bool)G_B3_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		Il2CppObject* L_4 = ___source0;
		__this->set_source_1(L_4);
		TimeSpan_t763862892  L_5 = ___duration1;
		__this->set_duration_3(L_5);
		Il2CppObject * L_6 = ___scheduler2;
		__this->set_scheduler_4(L_6);
		return;
	}
}
// UniRx.IObservable`1<T> UniRx.Operators.SkipObservable`1<System.Boolean>::Combine(System.Int32)
extern "C"  Il2CppObject* SkipObservable_1_Combine_m400455926_gshared (SkipObservable_1_t197279106 * __this, int32_t ___count0, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = (Il2CppObject*)__this->get_source_1();
		int32_t L_1 = (int32_t)__this->get_count_2();
		int32_t L_2 = ___count0;
		SkipObservable_1_t197279106 * L_3 = (SkipObservable_1_t197279106 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		((  void (*) (SkipObservable_1_t197279106 *, Il2CppObject*, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)(L_3, (Il2CppObject*)L_0, (int32_t)((int32_t)((int32_t)L_1+(int32_t)L_2)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		return L_3;
	}
}
// UniRx.IObservable`1<T> UniRx.Operators.SkipObservable`1<System.Boolean>::Combine(System.TimeSpan)
extern Il2CppClass* TimeSpan_t763862892_il2cpp_TypeInfo_var;
extern const uint32_t SkipObservable_1_Combine_m3873617879_MetadataUsageId;
extern "C"  Il2CppObject* SkipObservable_1_Combine_m3873617879_gshared (SkipObservable_1_t197279106 * __this, TimeSpan_t763862892  ___duration0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SkipObservable_1_Combine_m3873617879_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	SkipObservable_1_t197279106 * G_B3_0 = NULL;
	{
		TimeSpan_t763862892  L_0 = ___duration0;
		TimeSpan_t763862892  L_1 = (TimeSpan_t763862892 )__this->get_duration_3();
		IL2CPP_RUNTIME_CLASS_INIT(TimeSpan_t763862892_il2cpp_TypeInfo_var);
		bool L_2 = TimeSpan_op_LessThanOrEqual_m271837557(NULL /*static, unused*/, (TimeSpan_t763862892 )L_0, (TimeSpan_t763862892 )L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0017;
		}
	}
	{
		G_B3_0 = ((SkipObservable_1_t197279106 *)(__this));
		goto IL_0029;
	}

IL_0017:
	{
		Il2CppObject* L_3 = (Il2CppObject*)__this->get_source_1();
		TimeSpan_t763862892  L_4 = ___duration0;
		Il2CppObject * L_5 = (Il2CppObject *)__this->get_scheduler_4();
		SkipObservable_1_t197279106 * L_6 = (SkipObservable_1_t197279106 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		((  void (*) (SkipObservable_1_t197279106 *, Il2CppObject*, TimeSpan_t763862892 , Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->method)(L_6, (Il2CppObject*)L_3, (TimeSpan_t763862892 )L_4, (Il2CppObject *)L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		G_B3_0 = ((SkipObservable_1_t197279106 *)(L_6));
	}

IL_0029:
	{
		return G_B3_0;
	}
}
// System.IDisposable UniRx.Operators.SkipObservable`1<System.Boolean>::SubscribeCore(UniRx.IObserver`1<T>,System.IDisposable)
extern "C"  Il2CppObject * SkipObservable_1_SubscribeCore_m4094822482_gshared (SkipObservable_1_t197279106 * __this, Il2CppObject* ___observer0, Il2CppObject * ___cancel1, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_scheduler_4();
		if (L_0)
		{
			goto IL_001f;
		}
	}
	{
		Il2CppObject* L_1 = (Il2CppObject*)__this->get_source_1();
		Il2CppObject* L_2 = ___observer0;
		Il2CppObject * L_3 = ___cancel1;
		Skip_t1564805481 * L_4 = (Skip_t1564805481 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		((  void (*) (Skip_t1564805481 *, SkipObservable_1_t197279106 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7)->method)(L_4, (SkipObservable_1_t197279106 *)__this, (Il2CppObject*)L_2, (Il2CppObject *)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7));
		NullCheck((Il2CppObject*)L_1);
		Il2CppObject * L_5 = InterfaceFuncInvoker1< Il2CppObject *, Il2CppObject* >::Invoke(0 /* System.IDisposable UniRx.IObservable`1<System.Boolean>::Subscribe(UniRx.IObserver`1<T>) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8), (Il2CppObject*)L_1, (Il2CppObject*)L_4);
		return L_5;
	}

IL_001f:
	{
		Il2CppObject* L_6 = ___observer0;
		Il2CppObject * L_7 = ___cancel1;
		Skip__t1751828930 * L_8 = (Skip__t1751828930 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9));
		((  void (*) (Skip__t1751828930 *, SkipObservable_1_t197279106 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10)->method)(L_8, (SkipObservable_1_t197279106 *)__this, (Il2CppObject*)L_6, (Il2CppObject *)L_7, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10));
		NullCheck((Skip__t1751828930 *)L_8);
		Il2CppObject * L_9 = ((  Il2CppObject * (*) (Skip__t1751828930 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 11)->method)((Skip__t1751828930 *)L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 11));
		return L_9;
	}
}
// System.Void UniRx.Operators.SkipObservable`1<System.Object>::.ctor(UniRx.IObservable`1<T>,System.Int32)
extern "C"  void SkipObservable_1__ctor_m3269748147_gshared (SkipObservable_1_t823380185 * __this, Il2CppObject* ___source0, int32_t ___count1, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = ___source0;
		bool L_1 = ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)(NULL /*static, unused*/, (Il2CppObject*)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		NullCheck((OperatorObservableBase_1_t4196218687 *)__this);
		((  void (*) (OperatorObservableBase_1_t4196218687 *, bool, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)((OperatorObservableBase_1_t4196218687 *)__this, (bool)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		Il2CppObject* L_2 = ___source0;
		__this->set_source_1(L_2);
		int32_t L_3 = ___count1;
		__this->set_count_2(L_3);
		return;
	}
}
// System.Void UniRx.Operators.SkipObservable`1<System.Object>::.ctor(UniRx.IObservable`1<T>,System.TimeSpan,UniRx.IScheduler)
extern Il2CppClass* Scheduler_t103934925_il2cpp_TypeInfo_var;
extern const uint32_t SkipObservable_1__ctor_m1200294114_MetadataUsageId;
extern "C"  void SkipObservable_1__ctor_m1200294114_gshared (SkipObservable_1_t823380185 * __this, Il2CppObject* ___source0, TimeSpan_t763862892  ___duration1, Il2CppObject * ___scheduler2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SkipObservable_1__ctor_m1200294114_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	SkipObservable_1_t823380185 * G_B2_0 = NULL;
	SkipObservable_1_t823380185 * G_B1_0 = NULL;
	int32_t G_B3_0 = 0;
	SkipObservable_1_t823380185 * G_B3_1 = NULL;
	{
		Il2CppObject * L_0 = ___scheduler2;
		IL2CPP_RUNTIME_CLASS_INIT(Scheduler_t103934925_il2cpp_TypeInfo_var);
		Il2CppObject * L_1 = ((Scheduler_t103934925_StaticFields*)Scheduler_t103934925_il2cpp_TypeInfo_var->static_fields)->get_CurrentThread_0();
		G_B1_0 = ((SkipObservable_1_t823380185 *)(__this));
		if ((((Il2CppObject*)(Il2CppObject *)L_0) == ((Il2CppObject*)(Il2CppObject *)L_1)))
		{
			G_B2_0 = ((SkipObservable_1_t823380185 *)(__this));
			goto IL_0014;
		}
	}
	{
		Il2CppObject* L_2 = ___source0;
		bool L_3 = ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)(NULL /*static, unused*/, (Il2CppObject*)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		G_B3_0 = ((int32_t)(L_3));
		G_B3_1 = ((SkipObservable_1_t823380185 *)(G_B1_0));
		goto IL_0015;
	}

IL_0014:
	{
		G_B3_0 = 1;
		G_B3_1 = ((SkipObservable_1_t823380185 *)(G_B2_0));
	}

IL_0015:
	{
		NullCheck((OperatorObservableBase_1_t4196218687 *)G_B3_1);
		((  void (*) (OperatorObservableBase_1_t4196218687 *, bool, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)((OperatorObservableBase_1_t4196218687 *)G_B3_1, (bool)G_B3_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		Il2CppObject* L_4 = ___source0;
		__this->set_source_1(L_4);
		TimeSpan_t763862892  L_5 = ___duration1;
		__this->set_duration_3(L_5);
		Il2CppObject * L_6 = ___scheduler2;
		__this->set_scheduler_4(L_6);
		return;
	}
}
// UniRx.IObservable`1<T> UniRx.Operators.SkipObservable`1<System.Object>::Combine(System.Int32)
extern "C"  Il2CppObject* SkipObservable_1_Combine_m398784247_gshared (SkipObservable_1_t823380185 * __this, int32_t ___count0, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = (Il2CppObject*)__this->get_source_1();
		int32_t L_1 = (int32_t)__this->get_count_2();
		int32_t L_2 = ___count0;
		SkipObservable_1_t823380185 * L_3 = (SkipObservable_1_t823380185 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		((  void (*) (SkipObservable_1_t823380185 *, Il2CppObject*, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)(L_3, (Il2CppObject*)L_0, (int32_t)((int32_t)((int32_t)L_1+(int32_t)L_2)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		return L_3;
	}
}
// UniRx.IObservable`1<T> UniRx.Operators.SkipObservable`1<System.Object>::Combine(System.TimeSpan)
extern Il2CppClass* TimeSpan_t763862892_il2cpp_TypeInfo_var;
extern const uint32_t SkipObservable_1_Combine_m1317269046_MetadataUsageId;
extern "C"  Il2CppObject* SkipObservable_1_Combine_m1317269046_gshared (SkipObservable_1_t823380185 * __this, TimeSpan_t763862892  ___duration0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SkipObservable_1_Combine_m1317269046_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	SkipObservable_1_t823380185 * G_B3_0 = NULL;
	{
		TimeSpan_t763862892  L_0 = ___duration0;
		TimeSpan_t763862892  L_1 = (TimeSpan_t763862892 )__this->get_duration_3();
		IL2CPP_RUNTIME_CLASS_INIT(TimeSpan_t763862892_il2cpp_TypeInfo_var);
		bool L_2 = TimeSpan_op_LessThanOrEqual_m271837557(NULL /*static, unused*/, (TimeSpan_t763862892 )L_0, (TimeSpan_t763862892 )L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0017;
		}
	}
	{
		G_B3_0 = ((SkipObservable_1_t823380185 *)(__this));
		goto IL_0029;
	}

IL_0017:
	{
		Il2CppObject* L_3 = (Il2CppObject*)__this->get_source_1();
		TimeSpan_t763862892  L_4 = ___duration0;
		Il2CppObject * L_5 = (Il2CppObject *)__this->get_scheduler_4();
		SkipObservable_1_t823380185 * L_6 = (SkipObservable_1_t823380185 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		((  void (*) (SkipObservable_1_t823380185 *, Il2CppObject*, TimeSpan_t763862892 , Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->method)(L_6, (Il2CppObject*)L_3, (TimeSpan_t763862892 )L_4, (Il2CppObject *)L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		G_B3_0 = ((SkipObservable_1_t823380185 *)(L_6));
	}

IL_0029:
	{
		return G_B3_0;
	}
}
// System.IDisposable UniRx.Operators.SkipObservable`1<System.Object>::SubscribeCore(UniRx.IObserver`1<T>,System.IDisposable)
extern "C"  Il2CppObject * SkipObservable_1_SubscribeCore_m1236464419_gshared (SkipObservable_1_t823380185 * __this, Il2CppObject* ___observer0, Il2CppObject * ___cancel1, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_scheduler_4();
		if (L_0)
		{
			goto IL_001f;
		}
	}
	{
		Il2CppObject* L_1 = (Il2CppObject*)__this->get_source_1();
		Il2CppObject* L_2 = ___observer0;
		Il2CppObject * L_3 = ___cancel1;
		Skip_t2190906560 * L_4 = (Skip_t2190906560 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		((  void (*) (Skip_t2190906560 *, SkipObservable_1_t823380185 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7)->method)(L_4, (SkipObservable_1_t823380185 *)__this, (Il2CppObject*)L_2, (Il2CppObject *)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7));
		NullCheck((Il2CppObject*)L_1);
		Il2CppObject * L_5 = InterfaceFuncInvoker1< Il2CppObject *, Il2CppObject* >::Invoke(0 /* System.IDisposable UniRx.IObservable`1<System.Object>::Subscribe(UniRx.IObserver`1<T>) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8), (Il2CppObject*)L_1, (Il2CppObject*)L_4);
		return L_5;
	}

IL_001f:
	{
		Il2CppObject* L_6 = ___observer0;
		Il2CppObject * L_7 = ___cancel1;
		Skip__t2377930009 * L_8 = (Skip__t2377930009 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9));
		((  void (*) (Skip__t2377930009 *, SkipObservable_1_t823380185 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10)->method)(L_8, (SkipObservable_1_t823380185 *)__this, (Il2CppObject*)L_6, (Il2CppObject *)L_7, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10));
		NullCheck((Skip__t2377930009 *)L_8);
		Il2CppObject * L_9 = ((  Il2CppObject * (*) (Skip__t2377930009 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 11)->method)((Skip__t2377930009 *)L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 11));
		return L_9;
	}
}
// System.Void UniRx.Operators.SkipUntilObservable`2/SkipUntilOuterObserver/SkipUntil<System.Object,System.Object>::.ctor(UniRx.Operators.SkipUntilObservable`2/SkipUntilOuterObserver<T,TOther>,System.IDisposable)
extern "C"  void SkipUntil__ctor_m4128809165_gshared (SkipUntil_t2756386132 * __this, SkipUntilOuterObserver_t2866202317 * ___parent0, Il2CppObject * ___subscription1, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		SkipUntilOuterObserver_t2866202317 * L_0 = ___parent0;
		__this->set_parent_1(L_0);
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		EmptyObserver_1_t246459410 * L_1 = ((EmptyObserver_1_t246459410_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->get_Instance_0();
		il2cpp_codegen_memory_barrier();
		__this->set_observer_0(L_1);
		Il2CppObject * L_2 = ___subscription1;
		__this->set_subscription_2(L_2);
		return;
	}
}
// System.Void UniRx.Operators.SkipUntilObservable`2/SkipUntilOuterObserver/SkipUntil<System.Object,System.Object>::OnNext(T)
extern "C"  void SkipUntil_OnNext_m294762985_gshared (SkipUntil_t2756386132 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = (Il2CppObject*)__this->get_observer_0();
		il2cpp_codegen_memory_barrier();
		Il2CppObject * L_1 = ___value0;
		NullCheck((Il2CppObject*)L_0);
		InterfaceActionInvoker1< Il2CppObject * >::Invoke(2 /* System.Void UniRx.IObserver`1<System.Object>::OnNext(T) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), (Il2CppObject*)L_0, (Il2CppObject *)L_1);
		return;
	}
}
// System.Void UniRx.Operators.SkipUntilObservable`2/SkipUntilOuterObserver/SkipUntil<System.Object,System.Object>::OnError(System.Exception)
extern "C"  void SkipUntil_OnError_m3873366776_gshared (SkipUntil_t2756386132 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method)
{
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		Il2CppObject* L_0 = (Il2CppObject*)__this->get_observer_0();
		il2cpp_codegen_memory_barrier();
		Exception_t1967233988 * L_1 = ___error0;
		NullCheck((Il2CppObject*)L_0);
		InterfaceActionInvoker1< Exception_t1967233988 * >::Invoke(1 /* System.Void UniRx.IObserver`1<System.Object>::OnError(System.Exception) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), (Il2CppObject*)L_0, (Exception_t1967233988 *)L_1);
		IL2CPP_LEAVE(0x1F, FINALLY_0013);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0013;
	}

FINALLY_0013:
	{ // begin finally (depth: 1)
		SkipUntilOuterObserver_t2866202317 * L_2 = (SkipUntilOuterObserver_t2866202317 *)__this->get_parent_1();
		NullCheck((OperatorObserverBase_2_t1187768149 *)L_2);
		VirtActionInvoker0::Invoke(4 /* System.Void UniRx.Operators.OperatorObserverBase`2<System.Object,System.Object>::Dispose() */, (OperatorObserverBase_2_t1187768149 *)L_2);
		IL2CPP_END_FINALLY(19)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(19)
	{
		IL2CPP_JUMP_TBL(0x1F, IL_001f)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_001f:
	{
		return;
	}
}
// System.Void UniRx.Operators.SkipUntilObservable`2/SkipUntilOuterObserver/SkipUntil<System.Object,System.Object>::OnCompleted()
extern Il2CppClass* IDisposable_t1628921374_il2cpp_TypeInfo_var;
extern const uint32_t SkipUntil_OnCompleted_m1911573067_MetadataUsageId;
extern "C"  void SkipUntil_OnCompleted_m1911573067_gshared (SkipUntil_t2756386132 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SkipUntil_OnCompleted_m1911573067_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		Il2CppObject* L_0 = (Il2CppObject*)__this->get_observer_0();
		il2cpp_codegen_memory_barrier();
		NullCheck((Il2CppObject*)L_0);
		InterfaceActionInvoker0::Invoke(0 /* System.Void UniRx.IObserver`1<System.Object>::OnCompleted() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), (Il2CppObject*)L_0);
		IL2CPP_LEAVE(0x1E, FINALLY_0012);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0012;
	}

FINALLY_0012:
	{ // begin finally (depth: 1)
		Il2CppObject * L_1 = (Il2CppObject *)__this->get_subscription_2();
		NullCheck((Il2CppObject *)L_1);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, (Il2CppObject *)L_1);
		IL2CPP_END_FINALLY(18)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(18)
	{
		IL2CPP_JUMP_TBL(0x1E, IL_001e)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_001e:
	{
		return;
	}
}
// System.Void UniRx.Operators.SkipUntilObservable`2/SkipUntilOuterObserver/SkipUntilOther<System.Object,System.Object>::.ctor(UniRx.Operators.SkipUntilObservable`2/SkipUntilOuterObserver<T,TOther>,UniRx.Operators.SkipUntilObservable`2/SkipUntilOuterObserver/SkipUntil<T,TOther>,System.IDisposable)
extern "C"  void SkipUntilOther__ctor_m1048082279_gshared (SkipUntilOther_t2413447818 * __this, SkipUntilOuterObserver_t2866202317 * ___parent0, SkipUntil_t2756386132 * ___sourceObserver1, Il2CppObject * ___subscription2, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		SkipUntilOuterObserver_t2866202317 * L_0 = ___parent0;
		__this->set_parent_0(L_0);
		SkipUntil_t2756386132 * L_1 = ___sourceObserver1;
		__this->set_sourceObserver_1(L_1);
		Il2CppObject * L_2 = ___subscription2;
		__this->set_subscription_2(L_2);
		return;
	}
}
// System.Void UniRx.Operators.SkipUntilObservable`2/SkipUntilOuterObserver/SkipUntilOther<System.Object,System.Object>::OnNext(TOther)
extern Il2CppClass* IDisposable_t1628921374_il2cpp_TypeInfo_var;
extern const uint32_t SkipUntilOther_OnNext_m93051097_MetadataUsageId;
extern "C"  void SkipUntilOther_OnNext_m93051097_gshared (SkipUntilOther_t2413447818 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SkipUntilOther_OnNext_m93051097_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		SkipUntil_t2756386132 * L_0 = (SkipUntil_t2756386132 *)__this->get_sourceObserver_1();
		SkipUntilOuterObserver_t2866202317 * L_1 = (SkipUntilOuterObserver_t2866202317 *)__this->get_parent_0();
		NullCheck(L_1);
		Il2CppObject* L_2 = (Il2CppObject*)((OperatorObserverBase_2_t1187768149 *)L_1)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		NullCheck(L_0);
		il2cpp_codegen_memory_barrier();
		L_0->set_observer_0(L_2);
		Il2CppObject * L_3 = (Il2CppObject *)__this->get_subscription_2();
		NullCheck((Il2CppObject *)L_3);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, (Il2CppObject *)L_3);
		return;
	}
}
// System.Void UniRx.Operators.SkipUntilObservable`2/SkipUntilOuterObserver/SkipUntilOther<System.Object,System.Object>::OnError(System.Exception)
extern "C"  void SkipUntilOther_OnError_m3140934552_gshared (SkipUntilOther_t2413447818 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method)
{
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		SkipUntilOuterObserver_t2866202317 * L_0 = (SkipUntilOuterObserver_t2866202317 *)__this->get_parent_0();
		NullCheck(L_0);
		Il2CppObject* L_1 = (Il2CppObject*)((OperatorObserverBase_2_t1187768149 *)L_0)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		Exception_t1967233988 * L_2 = ___error0;
		NullCheck((Il2CppObject*)L_1);
		InterfaceActionInvoker1< Exception_t1967233988 * >::Invoke(1 /* System.Void UniRx.IObserver`1<System.Object>::OnError(System.Exception) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0), (Il2CppObject*)L_1, (Exception_t1967233988 *)L_2);
		IL2CPP_LEAVE(0x24, FINALLY_0018);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0018;
	}

FINALLY_0018:
	{ // begin finally (depth: 1)
		SkipUntilOuterObserver_t2866202317 * L_3 = (SkipUntilOuterObserver_t2866202317 *)__this->get_parent_0();
		NullCheck((OperatorObserverBase_2_t1187768149 *)L_3);
		VirtActionInvoker0::Invoke(4 /* System.Void UniRx.Operators.OperatorObserverBase`2<System.Object,System.Object>::Dispose() */, (OperatorObserverBase_2_t1187768149 *)L_3);
		IL2CPP_END_FINALLY(24)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(24)
	{
		IL2CPP_JUMP_TBL(0x24, IL_0024)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0024:
	{
		return;
	}
}
// System.Void UniRx.Operators.SkipUntilObservable`2/SkipUntilOuterObserver/SkipUntilOther<System.Object,System.Object>::OnCompleted()
extern Il2CppClass* IDisposable_t1628921374_il2cpp_TypeInfo_var;
extern const uint32_t SkipUntilOther_OnCompleted_m2499818219_MetadataUsageId;
extern "C"  void SkipUntilOther_OnCompleted_m2499818219_gshared (SkipUntilOther_t2413447818 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SkipUntilOther_OnCompleted_m2499818219_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_subscription_2();
		NullCheck((Il2CppObject *)L_0);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, (Il2CppObject *)L_0);
		return;
	}
}
// System.Void UniRx.Operators.SkipUntilObservable`2/SkipUntilOuterObserver<System.Object,System.Object>::.ctor(UniRx.Operators.SkipUntilObservable`2<T,TOther>,UniRx.IObserver`1<T>,System.IDisposable)
extern "C"  void SkipUntilOuterObserver__ctor_m925276218_gshared (SkipUntilOuterObserver_t2866202317 * __this, SkipUntilObservable_2_t2740161498 * ___parent0, Il2CppObject* ___observer1, Il2CppObject * ___cancel2, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = ___observer1;
		Il2CppObject * L_1 = ___cancel2;
		NullCheck((OperatorObserverBase_2_t1187768149 *)__this);
		((  void (*) (OperatorObserverBase_2_t1187768149 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((OperatorObserverBase_2_t1187768149 *)__this, (Il2CppObject*)L_0, (Il2CppObject *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		SkipUntilObservable_2_t2740161498 * L_2 = ___parent0;
		__this->set_parent_2(L_2);
		return;
	}
}
// System.IDisposable UniRx.Operators.SkipUntilObservable`2/SkipUntilOuterObserver<System.Object,System.Object>::Run()
extern Il2CppClass* SingleAssignmentDisposable_t2336378823_il2cpp_TypeInfo_var;
extern const uint32_t SkipUntilOuterObserver_Run_m2817850229_MetadataUsageId;
extern "C"  Il2CppObject * SkipUntilOuterObserver_Run_m2817850229_gshared (SkipUntilOuterObserver_t2866202317 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SkipUntilOuterObserver_Run_m2817850229_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	SingleAssignmentDisposable_t2336378823 * V_0 = NULL;
	SkipUntil_t2756386132 * V_1 = NULL;
	SingleAssignmentDisposable_t2336378823 * V_2 = NULL;
	SkipUntilOther_t2413447818 * V_3 = NULL;
	{
		SingleAssignmentDisposable_t2336378823 * L_0 = (SingleAssignmentDisposable_t2336378823 *)il2cpp_codegen_object_new(SingleAssignmentDisposable_t2336378823_il2cpp_TypeInfo_var);
		SingleAssignmentDisposable__ctor_m917449122(L_0, /*hidden argument*/NULL);
		V_0 = (SingleAssignmentDisposable_t2336378823 *)L_0;
		SingleAssignmentDisposable_t2336378823 * L_1 = V_0;
		SkipUntil_t2756386132 * L_2 = (SkipUntil_t2756386132 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		((  void (*) (SkipUntil_t2756386132 *, SkipUntilOuterObserver_t2866202317 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)(L_2, (SkipUntilOuterObserver_t2866202317 *)__this, (Il2CppObject *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		V_1 = (SkipUntil_t2756386132 *)L_2;
		SingleAssignmentDisposable_t2336378823 * L_3 = (SingleAssignmentDisposable_t2336378823 *)il2cpp_codegen_object_new(SingleAssignmentDisposable_t2336378823_il2cpp_TypeInfo_var);
		SingleAssignmentDisposable__ctor_m917449122(L_3, /*hidden argument*/NULL);
		V_2 = (SingleAssignmentDisposable_t2336378823 *)L_3;
		SkipUntil_t2756386132 * L_4 = V_1;
		SingleAssignmentDisposable_t2336378823 * L_5 = V_2;
		SkipUntilOther_t2413447818 * L_6 = (SkipUntilOther_t2413447818 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		((  void (*) (SkipUntilOther_t2413447818 *, SkipUntilOuterObserver_t2866202317 *, SkipUntil_t2756386132 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->method)(L_6, (SkipUntilOuterObserver_t2866202317 *)__this, (SkipUntil_t2756386132 *)L_4, (Il2CppObject *)L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		V_3 = (SkipUntilOther_t2413447818 *)L_6;
		SingleAssignmentDisposable_t2336378823 * L_7 = V_0;
		SkipUntilObservable_2_t2740161498 * L_8 = (SkipUntilObservable_2_t2740161498 *)__this->get_parent_2();
		NullCheck(L_8);
		Il2CppObject* L_9 = (Il2CppObject*)L_8->get_source_1();
		SkipUntil_t2756386132 * L_10 = V_1;
		NullCheck((Il2CppObject*)L_9);
		Il2CppObject * L_11 = InterfaceFuncInvoker1< Il2CppObject *, Il2CppObject* >::Invoke(0 /* System.IDisposable UniRx.IObservable`1<System.Object>::Subscribe(UniRx.IObserver`1<T>) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6), (Il2CppObject*)L_9, (Il2CppObject*)L_10);
		NullCheck((SingleAssignmentDisposable_t2336378823 *)L_7);
		SingleAssignmentDisposable_set_Disposable_m1599028885((SingleAssignmentDisposable_t2336378823 *)L_7, (Il2CppObject *)L_11, /*hidden argument*/NULL);
		SingleAssignmentDisposable_t2336378823 * L_12 = V_2;
		SkipUntilObservable_2_t2740161498 * L_13 = (SkipUntilObservable_2_t2740161498 *)__this->get_parent_2();
		NullCheck(L_13);
		Il2CppObject* L_14 = (Il2CppObject*)L_13->get_other_2();
		SkipUntilOther_t2413447818 * L_15 = V_3;
		NullCheck((Il2CppObject*)L_14);
		Il2CppObject * L_16 = InterfaceFuncInvoker1< Il2CppObject *, Il2CppObject* >::Invoke(0 /* System.IDisposable UniRx.IObservable`1<System.Object>::Subscribe(UniRx.IObserver`1<T>) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7), (Il2CppObject*)L_14, (Il2CppObject*)L_15);
		NullCheck((SingleAssignmentDisposable_t2336378823 *)L_12);
		SingleAssignmentDisposable_set_Disposable_m1599028885((SingleAssignmentDisposable_t2336378823 *)L_12, (Il2CppObject *)L_16, /*hidden argument*/NULL);
		SingleAssignmentDisposable_t2336378823 * L_17 = V_2;
		SingleAssignmentDisposable_t2336378823 * L_18 = V_0;
		Il2CppObject * L_19 = StableCompositeDisposable_Create_m3764788535(NULL /*static, unused*/, (Il2CppObject *)L_17, (Il2CppObject *)L_18, /*hidden argument*/NULL);
		return L_19;
	}
}
// System.Void UniRx.Operators.SkipUntilObservable`2/SkipUntilOuterObserver<System.Object,System.Object>::OnNext(T)
extern "C"  void SkipUntilOuterObserver_OnNext_m2736822841_gshared (SkipUntilOuterObserver_t2866202317 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void UniRx.Operators.SkipUntilObservable`2/SkipUntilOuterObserver<System.Object,System.Object>::OnError(System.Exception)
extern "C"  void SkipUntilOuterObserver_OnError_m1942168392_gshared (SkipUntilOuterObserver_t2866202317 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void UniRx.Operators.SkipUntilObservable`2/SkipUntilOuterObserver<System.Object,System.Object>::OnCompleted()
extern "C"  void SkipUntilOuterObserver_OnCompleted_m3849749147_gshared (SkipUntilOuterObserver_t2866202317 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void UniRx.Operators.SkipUntilObservable`2<System.Object,System.Object>::.ctor(UniRx.IObservable`1<T>,UniRx.IObservable`1<TOther>)
extern "C"  void SkipUntilObservable_2__ctor_m2822742818_gshared (SkipUntilObservable_2_t2740161498 * __this, Il2CppObject* ___source0, Il2CppObject* ___other1, const MethodInfo* method)
{
	SkipUntilObservable_2_t2740161498 * G_B2_0 = NULL;
	SkipUntilObservable_2_t2740161498 * G_B1_0 = NULL;
	int32_t G_B3_0 = 0;
	SkipUntilObservable_2_t2740161498 * G_B3_1 = NULL;
	{
		Il2CppObject* L_0 = ___source0;
		bool L_1 = ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)(NULL /*static, unused*/, (Il2CppObject*)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		G_B1_0 = ((SkipUntilObservable_2_t2740161498 *)(__this));
		if (L_1)
		{
			G_B2_0 = ((SkipUntilObservable_2_t2740161498 *)(__this));
			goto IL_0014;
		}
	}
	{
		Il2CppObject* L_2 = ___other1;
		bool L_3 = ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)(NULL /*static, unused*/, (Il2CppObject*)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		G_B3_0 = ((int32_t)(L_3));
		G_B3_1 = ((SkipUntilObservable_2_t2740161498 *)(G_B1_0));
		goto IL_0015;
	}

IL_0014:
	{
		G_B3_0 = 1;
		G_B3_1 = ((SkipUntilObservable_2_t2740161498 *)(G_B2_0));
	}

IL_0015:
	{
		NullCheck((OperatorObservableBase_1_t4196218687 *)G_B3_1);
		((  void (*) (OperatorObservableBase_1_t4196218687 *, bool, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)((OperatorObservableBase_1_t4196218687 *)G_B3_1, (bool)G_B3_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		Il2CppObject* L_4 = ___source0;
		__this->set_source_1(L_4);
		Il2CppObject* L_5 = ___other1;
		__this->set_other_2(L_5);
		return;
	}
}
// System.IDisposable UniRx.Operators.SkipUntilObservable`2<System.Object,System.Object>::SubscribeCore(UniRx.IObserver`1<T>,System.IDisposable)
extern "C"  Il2CppObject * SkipUntilObservable_2_SubscribeCore_m706503804_gshared (SkipUntilObservable_2_t2740161498 * __this, Il2CppObject* ___observer0, Il2CppObject * ___cancel1, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = ___observer0;
		Il2CppObject * L_1 = ___cancel1;
		SkipUntilOuterObserver_t2866202317 * L_2 = (SkipUntilOuterObserver_t2866202317 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		((  void (*) (SkipUntilOuterObserver_t2866202317 *, SkipUntilObservable_2_t2740161498 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->method)(L_2, (SkipUntilObservable_2_t2740161498 *)__this, (Il2CppObject*)L_0, (Il2CppObject *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		NullCheck((SkipUntilOuterObserver_t2866202317 *)L_2);
		Il2CppObject * L_3 = ((  Il2CppObject * (*) (SkipUntilOuterObserver_t2866202317 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6)->method)((SkipUntilOuterObserver_t2866202317 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		return L_3;
	}
}
// System.Void UniRx.Operators.SkipWhileObservable`1/SkipWhile_<System.Object>::.ctor(UniRx.Operators.SkipWhileObservable`1<T>,UniRx.IObserver`1<T>,System.IDisposable)
extern "C"  void SkipWhile___ctor_m3293816885_gshared (SkipWhile__t3820853950 * __this, SkipWhileObservable_1_t2542074388 * ___parent0, Il2CppObject* ___observer1, Il2CppObject * ___cancel2, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = ___observer1;
		Il2CppObject * L_1 = ___cancel2;
		NullCheck((OperatorObserverBase_2_t1187768149 *)__this);
		((  void (*) (OperatorObserverBase_2_t1187768149 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((OperatorObserverBase_2_t1187768149 *)__this, (Il2CppObject*)L_0, (Il2CppObject *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		SkipWhileObservable_1_t2542074388 * L_2 = ___parent0;
		__this->set_parent_2(L_2);
		return;
	}
}
// System.IDisposable UniRx.Operators.SkipWhileObservable`1/SkipWhile_<System.Object>::Run()
extern "C"  Il2CppObject * SkipWhile__Run_m3489040716_gshared (SkipWhile__t3820853950 * __this, const MethodInfo* method)
{
	{
		SkipWhileObservable_1_t2542074388 * L_0 = (SkipWhileObservable_1_t2542074388 *)__this->get_parent_2();
		NullCheck(L_0);
		Il2CppObject* L_1 = (Il2CppObject*)L_0->get_source_1();
		NullCheck((Il2CppObject*)L_1);
		Il2CppObject * L_2 = InterfaceFuncInvoker1< Il2CppObject *, Il2CppObject* >::Invoke(0 /* System.IDisposable UniRx.IObservable`1<System.Object>::Subscribe(UniRx.IObserver`1<T>) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), (Il2CppObject*)L_1, (Il2CppObject*)__this);
		return L_2;
	}
}
// System.Void UniRx.Operators.SkipWhileObservable`1/SkipWhile_<System.Object>::OnNext(T)
extern Il2CppClass* Exception_t1967233988_il2cpp_TypeInfo_var;
extern const uint32_t SkipWhile__OnNext_m1916940496_MetadataUsageId;
extern "C"  void SkipWhile__OnNext_m1916940496_gshared (SkipWhile__t3820853950 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SkipWhile__OnNext_m1916940496_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Exception_t1967233988 * V_0 = NULL;
	int32_t V_1 = 0;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		bool L_0 = (bool)__this->get_endSkip_3();
		if (L_0)
		{
			goto IL_006c;
		}
	}

IL_000b:
	try
	{ // begin try (depth: 1)
		SkipWhileObservable_1_t2542074388 * L_1 = (SkipWhileObservable_1_t2542074388 *)__this->get_parent_2();
		NullCheck(L_1);
		Func_3_t3064567499 * L_2 = (Func_3_t3064567499 *)L_1->get_predicateWithIndex_3();
		Il2CppObject * L_3 = ___value0;
		int32_t L_4 = (int32_t)__this->get_index_4();
		int32_t L_5 = (int32_t)L_4;
		V_1 = (int32_t)L_5;
		__this->set_index_4(((int32_t)((int32_t)L_5+(int32_t)1)));
		int32_t L_6 = V_1;
		NullCheck((Func_3_t3064567499 *)L_2);
		bool L_7 = ((  bool (*) (Func_3_t3064567499 *, Il2CppObject *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)((Func_3_t3064567499 *)L_2, (Il2CppObject *)L_3, (int32_t)L_6, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		__this->set_endSkip_3((bool)((((int32_t)L_7) == ((int32_t)0))? 1 : 0));
		goto IL_0060;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1967233988 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Exception_t1967233988_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_003b;
		throw e;
	}

CATCH_003b:
	{ // begin catch(System.Exception)
		{
			V_0 = (Exception_t1967233988 *)((Exception_t1967233988 *)__exception_local);
		}

IL_003c:
		try
		{ // begin try (depth: 2)
			Il2CppObject* L_8 = (Il2CppObject*)((OperatorObserverBase_2_t1187768149 *)__this)->get_observer_0();
			il2cpp_codegen_memory_barrier();
			Exception_t1967233988 * L_9 = V_0;
			NullCheck((Il2CppObject*)L_8);
			InterfaceActionInvoker1< Exception_t1967233988 * >::Invoke(1 /* System.Void UniRx.IObserver`1<System.Object>::OnError(System.Exception) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4), (Il2CppObject*)L_8, (Exception_t1967233988 *)L_9);
			IL2CPP_LEAVE(0x56, FINALLY_004f);
		} // end try (depth: 2)
		catch(Il2CppExceptionWrapper& e)
		{
			__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
			goto FINALLY_004f;
		}

FINALLY_004f:
		{ // begin finally (depth: 2)
			NullCheck((OperatorObserverBase_2_t1187768149 *)__this);
			VirtActionInvoker0::Invoke(4 /* System.Void UniRx.Operators.OperatorObserverBase`2<System.Object,System.Object>::Dispose() */, (OperatorObserverBase_2_t1187768149 *)__this);
			IL2CPP_END_FINALLY(79)
		} // end finally (depth: 2)
		IL2CPP_CLEANUP(79)
		{
			IL2CPP_JUMP_TBL(0x56, IL_0056)
			IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
		}

IL_0056:
		{
			goto IL_007a;
		}

IL_005b:
		{
			; // IL_005b: leave IL_0060
		}
	} // end catch (depth: 1)

IL_0060:
	{
		bool L_10 = (bool)__this->get_endSkip_3();
		if (L_10)
		{
			goto IL_006c;
		}
	}
	{
		return;
	}

IL_006c:
	{
		Il2CppObject* L_11 = (Il2CppObject*)((OperatorObserverBase_2_t1187768149 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		Il2CppObject * L_12 = ___value0;
		NullCheck((Il2CppObject*)L_11);
		InterfaceActionInvoker1< Il2CppObject * >::Invoke(2 /* System.Void UniRx.IObserver`1<System.Object>::OnNext(T) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4), (Il2CppObject*)L_11, (Il2CppObject *)L_12);
	}

IL_007a:
	{
		return;
	}
}
// System.Void UniRx.Operators.SkipWhileObservable`1/SkipWhile_<System.Object>::OnError(System.Exception)
extern "C"  void SkipWhile__OnError_m4247432159_gshared (SkipWhile__t3820853950 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method)
{
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		Il2CppObject* L_0 = (Il2CppObject*)((OperatorObserverBase_2_t1187768149 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		Exception_t1967233988 * L_1 = ___error0;
		NullCheck((Il2CppObject*)L_0);
		InterfaceActionInvoker1< Exception_t1967233988 * >::Invoke(1 /* System.Void UniRx.IObserver`1<System.Object>::OnError(System.Exception) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4), (Il2CppObject*)L_0, (Exception_t1967233988 *)L_1);
		IL2CPP_LEAVE(0x1A, FINALLY_0013);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0013;
	}

FINALLY_0013:
	{ // begin finally (depth: 1)
		NullCheck((OperatorObserverBase_2_t1187768149 *)__this);
		VirtActionInvoker0::Invoke(4 /* System.Void UniRx.Operators.OperatorObserverBase`2<System.Object,System.Object>::Dispose() */, (OperatorObserverBase_2_t1187768149 *)__this);
		IL2CPP_END_FINALLY(19)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(19)
	{
		IL2CPP_JUMP_TBL(0x1A, IL_001a)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_001a:
	{
		return;
	}
}
// System.Void UniRx.Operators.SkipWhileObservable`1/SkipWhile_<System.Object>::OnCompleted()
extern "C"  void SkipWhile__OnCompleted_m2251093426_gshared (SkipWhile__t3820853950 * __this, const MethodInfo* method)
{
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		Il2CppObject* L_0 = (Il2CppObject*)((OperatorObserverBase_2_t1187768149 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		NullCheck((Il2CppObject*)L_0);
		InterfaceActionInvoker0::Invoke(0 /* System.Void UniRx.IObserver`1<System.Object>::OnCompleted() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4), (Il2CppObject*)L_0);
		IL2CPP_LEAVE(0x19, FINALLY_0012);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0012;
	}

FINALLY_0012:
	{ // begin finally (depth: 1)
		NullCheck((OperatorObserverBase_2_t1187768149 *)__this);
		VirtActionInvoker0::Invoke(4 /* System.Void UniRx.Operators.OperatorObserverBase`2<System.Object,System.Object>::Dispose() */, (OperatorObserverBase_2_t1187768149 *)__this);
		IL2CPP_END_FINALLY(18)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(18)
	{
		IL2CPP_JUMP_TBL(0x19, IL_0019)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0019:
	{
		return;
	}
}
// System.Void UniRx.Operators.SkipWhileObservable`1/SkipWhile<System.Object>::.ctor(UniRx.Operators.SkipWhileObservable`1<T>,UniRx.IObserver`1<T>,System.IDisposable)
extern "C"  void SkipWhile__ctor_m2188486226_gshared (SkipWhile_t3761473147 * __this, SkipWhileObservable_1_t2542074388 * ___parent0, Il2CppObject* ___observer1, Il2CppObject * ___cancel2, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = ___observer1;
		Il2CppObject * L_1 = ___cancel2;
		NullCheck((OperatorObserverBase_2_t1187768149 *)__this);
		((  void (*) (OperatorObserverBase_2_t1187768149 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((OperatorObserverBase_2_t1187768149 *)__this, (Il2CppObject*)L_0, (Il2CppObject *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		SkipWhileObservable_1_t2542074388 * L_2 = ___parent0;
		__this->set_parent_2(L_2);
		return;
	}
}
// System.IDisposable UniRx.Operators.SkipWhileObservable`1/SkipWhile<System.Object>::Run()
extern "C"  Il2CppObject * SkipWhile_Run_m480590675_gshared (SkipWhile_t3761473147 * __this, const MethodInfo* method)
{
	{
		SkipWhileObservable_1_t2542074388 * L_0 = (SkipWhileObservable_1_t2542074388 *)__this->get_parent_2();
		NullCheck(L_0);
		Il2CppObject* L_1 = (Il2CppObject*)L_0->get_source_1();
		NullCheck((Il2CppObject*)L_1);
		Il2CppObject * L_2 = InterfaceFuncInvoker1< Il2CppObject *, Il2CppObject* >::Invoke(0 /* System.IDisposable UniRx.IObservable`1<System.Object>::Subscribe(UniRx.IObserver`1<T>) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), (Il2CppObject*)L_1, (Il2CppObject*)__this);
		return L_2;
	}
}
// System.Void UniRx.Operators.SkipWhileObservable`1/SkipWhile<System.Object>::OnNext(T)
extern Il2CppClass* Exception_t1967233988_il2cpp_TypeInfo_var;
extern const uint32_t SkipWhile_OnNext_m3360808365_MetadataUsageId;
extern "C"  void SkipWhile_OnNext_m3360808365_gshared (SkipWhile_t3761473147 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SkipWhile_OnNext_m3360808365_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Exception_t1967233988 * V_0 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		bool L_0 = (bool)__this->get_endSkip_3();
		if (L_0)
		{
			goto IL_005b;
		}
	}

IL_000b:
	try
	{ // begin try (depth: 1)
		SkipWhileObservable_1_t2542074388 * L_1 = (SkipWhileObservable_1_t2542074388 *)__this->get_parent_2();
		NullCheck(L_1);
		Func_2_t1509682273 * L_2 = (Func_2_t1509682273 *)L_1->get_predicate_2();
		Il2CppObject * L_3 = ___value0;
		NullCheck((Func_2_t1509682273 *)L_2);
		bool L_4 = ((  bool (*) (Func_2_t1509682273 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)((Func_2_t1509682273 *)L_2, (Il2CppObject *)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		__this->set_endSkip_3((bool)((((int32_t)L_4) == ((int32_t)0))? 1 : 0));
		goto IL_004f;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1967233988 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Exception_t1967233988_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_002a;
		throw e;
	}

CATCH_002a:
	{ // begin catch(System.Exception)
		{
			V_0 = (Exception_t1967233988 *)((Exception_t1967233988 *)__exception_local);
		}

IL_002b:
		try
		{ // begin try (depth: 2)
			Il2CppObject* L_5 = (Il2CppObject*)((OperatorObserverBase_2_t1187768149 *)__this)->get_observer_0();
			il2cpp_codegen_memory_barrier();
			Exception_t1967233988 * L_6 = V_0;
			NullCheck((Il2CppObject*)L_5);
			InterfaceActionInvoker1< Exception_t1967233988 * >::Invoke(1 /* System.Void UniRx.IObserver`1<System.Object>::OnError(System.Exception) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4), (Il2CppObject*)L_5, (Exception_t1967233988 *)L_6);
			IL2CPP_LEAVE(0x45, FINALLY_003e);
		} // end try (depth: 2)
		catch(Il2CppExceptionWrapper& e)
		{
			__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
			goto FINALLY_003e;
		}

FINALLY_003e:
		{ // begin finally (depth: 2)
			NullCheck((OperatorObserverBase_2_t1187768149 *)__this);
			VirtActionInvoker0::Invoke(4 /* System.Void UniRx.Operators.OperatorObserverBase`2<System.Object,System.Object>::Dispose() */, (OperatorObserverBase_2_t1187768149 *)__this);
			IL2CPP_END_FINALLY(62)
		} // end finally (depth: 2)
		IL2CPP_CLEANUP(62)
		{
			IL2CPP_JUMP_TBL(0x45, IL_0045)
			IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
		}

IL_0045:
		{
			goto IL_0069;
		}

IL_004a:
		{
			; // IL_004a: leave IL_004f
		}
	} // end catch (depth: 1)

IL_004f:
	{
		bool L_7 = (bool)__this->get_endSkip_3();
		if (L_7)
		{
			goto IL_005b;
		}
	}
	{
		return;
	}

IL_005b:
	{
		Il2CppObject* L_8 = (Il2CppObject*)((OperatorObserverBase_2_t1187768149 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		Il2CppObject * L_9 = ___value0;
		NullCheck((Il2CppObject*)L_8);
		InterfaceActionInvoker1< Il2CppObject * >::Invoke(2 /* System.Void UniRx.IObserver`1<System.Object>::OnNext(T) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4), (Il2CppObject*)L_8, (Il2CppObject *)L_9);
	}

IL_0069:
	{
		return;
	}
}
// System.Void UniRx.Operators.SkipWhileObservable`1/SkipWhile<System.Object>::OnError(System.Exception)
extern "C"  void SkipWhile_OnError_m419888828_gshared (SkipWhile_t3761473147 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method)
{
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		Il2CppObject* L_0 = (Il2CppObject*)((OperatorObserverBase_2_t1187768149 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		Exception_t1967233988 * L_1 = ___error0;
		NullCheck((Il2CppObject*)L_0);
		InterfaceActionInvoker1< Exception_t1967233988 * >::Invoke(1 /* System.Void UniRx.IObserver`1<System.Object>::OnError(System.Exception) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4), (Il2CppObject*)L_0, (Exception_t1967233988 *)L_1);
		IL2CPP_LEAVE(0x1A, FINALLY_0013);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0013;
	}

FINALLY_0013:
	{ // begin finally (depth: 1)
		NullCheck((OperatorObserverBase_2_t1187768149 *)__this);
		VirtActionInvoker0::Invoke(4 /* System.Void UniRx.Operators.OperatorObserverBase`2<System.Object,System.Object>::Dispose() */, (OperatorObserverBase_2_t1187768149 *)__this);
		IL2CPP_END_FINALLY(19)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(19)
	{
		IL2CPP_JUMP_TBL(0x1A, IL_001a)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_001a:
	{
		return;
	}
}
// System.Void UniRx.Operators.SkipWhileObservable`1/SkipWhile<System.Object>::OnCompleted()
extern "C"  void SkipWhile_OnCompleted_m3232820239_gshared (SkipWhile_t3761473147 * __this, const MethodInfo* method)
{
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		Il2CppObject* L_0 = (Il2CppObject*)((OperatorObserverBase_2_t1187768149 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		NullCheck((Il2CppObject*)L_0);
		InterfaceActionInvoker0::Invoke(0 /* System.Void UniRx.IObserver`1<System.Object>::OnCompleted() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4), (Il2CppObject*)L_0);
		IL2CPP_LEAVE(0x19, FINALLY_0012);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0012;
	}

FINALLY_0012:
	{ // begin finally (depth: 1)
		NullCheck((OperatorObserverBase_2_t1187768149 *)__this);
		VirtActionInvoker0::Invoke(4 /* System.Void UniRx.Operators.OperatorObserverBase`2<System.Object,System.Object>::Dispose() */, (OperatorObserverBase_2_t1187768149 *)__this);
		IL2CPP_END_FINALLY(18)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(18)
	{
		IL2CPP_JUMP_TBL(0x19, IL_0019)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0019:
	{
		return;
	}
}
// System.Void UniRx.Operators.SkipWhileObservable`1<System.Object>::.ctor(UniRx.IObservable`1<T>,System.Func`2<T,System.Boolean>)
extern "C"  void SkipWhileObservable_1__ctor_m3244455415_gshared (SkipWhileObservable_1_t2542074388 * __this, Il2CppObject* ___source0, Func_2_t1509682273 * ___predicate1, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = ___source0;
		bool L_1 = ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)(NULL /*static, unused*/, (Il2CppObject*)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		NullCheck((OperatorObservableBase_1_t4196218687 *)__this);
		((  void (*) (OperatorObservableBase_1_t4196218687 *, bool, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)((OperatorObservableBase_1_t4196218687 *)__this, (bool)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		Il2CppObject* L_2 = ___source0;
		__this->set_source_1(L_2);
		Func_2_t1509682273 * L_3 = ___predicate1;
		__this->set_predicate_2(L_3);
		return;
	}
}
// System.Void UniRx.Operators.SkipWhileObservable`1<System.Object>::.ctor(UniRx.IObservable`1<T>,System.Func`3<T,System.Int32,System.Boolean>)
extern "C"  void SkipWhileObservable_1__ctor_m3831277199_gshared (SkipWhileObservable_1_t2542074388 * __this, Il2CppObject* ___source0, Func_3_t3064567499 * ___predicateWithIndex1, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = ___source0;
		bool L_1 = ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)(NULL /*static, unused*/, (Il2CppObject*)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		NullCheck((OperatorObservableBase_1_t4196218687 *)__this);
		((  void (*) (OperatorObservableBase_1_t4196218687 *, bool, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)((OperatorObservableBase_1_t4196218687 *)__this, (bool)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		Il2CppObject* L_2 = ___source0;
		__this->set_source_1(L_2);
		Func_3_t3064567499 * L_3 = ___predicateWithIndex1;
		__this->set_predicateWithIndex_3(L_3);
		return;
	}
}
// System.IDisposable UniRx.Operators.SkipWhileObservable`1<System.Object>::SubscribeCore(UniRx.IObserver`1<T>,System.IDisposable)
extern "C"  Il2CppObject * SkipWhileObservable_1_SubscribeCore_m1795511260_gshared (SkipWhileObservable_1_t2542074388 * __this, Il2CppObject* ___observer0, Il2CppObject * ___cancel1, const MethodInfo* method)
{
	{
		Func_2_t1509682273 * L_0 = (Func_2_t1509682273 *)__this->get_predicate_2();
		if (!L_0)
		{
			goto IL_0019;
		}
	}
	{
		Il2CppObject* L_1 = ___observer0;
		Il2CppObject * L_2 = ___cancel1;
		SkipWhile_t3761473147 * L_3 = (SkipWhile_t3761473147 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		((  void (*) (SkipWhile_t3761473147 *, SkipWhileObservable_1_t2542074388 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)(L_3, (SkipWhileObservable_1_t2542074388 *)__this, (Il2CppObject*)L_1, (Il2CppObject *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		NullCheck((SkipWhile_t3761473147 *)L_3);
		Il2CppObject * L_4 = ((  Il2CppObject * (*) (SkipWhile_t3761473147 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->method)((SkipWhile_t3761473147 *)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		return L_4;
	}

IL_0019:
	{
		Il2CppObject* L_5 = ___observer0;
		Il2CppObject * L_6 = ___cancel1;
		SkipWhile__t3820853950 * L_7 = (SkipWhile__t3820853950 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		((  void (*) (SkipWhile__t3820853950 *, SkipWhileObservable_1_t2542074388 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7)->method)(L_7, (SkipWhileObservable_1_t2542074388 *)__this, (Il2CppObject*)L_5, (Il2CppObject *)L_6, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7));
		NullCheck((SkipWhile__t3820853950 *)L_7);
		Il2CppObject * L_8 = ((  Il2CppObject * (*) (SkipWhile__t3820853950 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8)->method)((SkipWhile__t3820853950 *)L_7, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8));
		return L_8;
	}
}
// System.Void UniRx.Operators.StartObservable`1/StartObserver<System.Int32>::.ctor(UniRx.Operators.StartObservable`1<T>,UniRx.IObserver`1<T>,System.IDisposable)
extern "C"  void StartObserver__ctor_m2206815641_gshared (StartObserver_t2832445184 * __this, StartObservable_1_t77870867 * ___parent0, Il2CppObject* ___observer1, Il2CppObject * ___cancel2, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = ___observer1;
		Il2CppObject * L_1 = ___cancel2;
		NullCheck((OperatorObserverBase_2_t701568569 *)__this);
		((  void (*) (OperatorObserverBase_2_t701568569 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((OperatorObserverBase_2_t701568569 *)__this, (Il2CppObject*)L_0, (Il2CppObject *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		StartObservable_1_t77870867 * L_2 = ___parent0;
		__this->set_parent_2(L_2);
		return;
	}
}
// System.Void UniRx.Operators.StartObservable`1/StartObserver<System.Int32>::Run()
extern Il2CppClass* Int32_t2847414787_il2cpp_TypeInfo_var;
extern Il2CppClass* Exception_t1967233988_il2cpp_TypeInfo_var;
extern const uint32_t StartObserver_Run_m2636809029_MetadataUsageId;
extern "C"  void StartObserver_Run_m2636809029_gshared (StartObserver_t2832445184 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (StartObserver_Run_m2636809029_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	Exception_t1967233988 * V_1 = NULL;
	int32_t V_2 = 0;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Initobj (Int32_t2847414787_il2cpp_TypeInfo_var, (&V_2));
		int32_t L_0 = V_2;
		V_0 = (int32_t)L_0;
	}

IL_000a:
	try
	{ // begin try (depth: 1)
		{
			StartObservable_1_t77870867 * L_1 = (StartObservable_1_t77870867 *)__this->get_parent_2();
			NullCheck(L_1);
			Func_1_t3990196034 * L_2 = (Func_1_t3990196034 *)L_1->get_function_2();
			if (!L_2)
			{
				goto IL_0030;
			}
		}

IL_001a:
		{
			StartObservable_1_t77870867 * L_3 = (StartObservable_1_t77870867 *)__this->get_parent_2();
			NullCheck(L_3);
			Func_1_t3990196034 * L_4 = (Func_1_t3990196034 *)L_3->get_function_2();
			NullCheck((Func_1_t3990196034 *)L_4);
			int32_t L_5 = ((  int32_t (*) (Func_1_t3990196034 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)((Func_1_t3990196034 *)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
			V_0 = (int32_t)L_5;
			goto IL_0040;
		}

IL_0030:
		{
			StartObservable_1_t77870867 * L_6 = (StartObservable_1_t77870867 *)__this->get_parent_2();
			NullCheck(L_6);
			Action_t437523947 * L_7 = (Action_t437523947 *)L_6->get_action_1();
			NullCheck((Action_t437523947 *)L_7);
			Action_Invoke_m1445970038((Action_t437523947 *)L_7, /*hidden argument*/NULL);
		}

IL_0040:
		{
			goto IL_006a;
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1967233988 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Exception_t1967233988_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_0045;
		throw e;
	}

CATCH_0045:
	{ // begin catch(System.Exception)
		{
			V_1 = (Exception_t1967233988 *)((Exception_t1967233988 *)__exception_local);
		}

IL_0046:
		try
		{ // begin try (depth: 2)
			Il2CppObject* L_8 = (Il2CppObject*)((OperatorObserverBase_2_t701568569 *)__this)->get_observer_0();
			il2cpp_codegen_memory_barrier();
			Exception_t1967233988 * L_9 = V_1;
			NullCheck((Il2CppObject*)L_8);
			InterfaceActionInvoker1< Exception_t1967233988 * >::Invoke(1 /* System.Void UniRx.IObserver`1<System.Int32>::OnError(System.Exception) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3), (Il2CppObject*)L_8, (Exception_t1967233988 *)L_9);
			IL2CPP_LEAVE(0x60, FINALLY_0059);
		} // end try (depth: 2)
		catch(Il2CppExceptionWrapper& e)
		{
			__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
			goto FINALLY_0059;
		}

FINALLY_0059:
		{ // begin finally (depth: 2)
			NullCheck((OperatorObserverBase_2_t701568569 *)__this);
			VirtActionInvoker0::Invoke(4 /* System.Void UniRx.Operators.OperatorObserverBase`2<System.Int32,System.Int32>::Dispose() */, (OperatorObserverBase_2_t701568569 *)__this);
			IL2CPP_END_FINALLY(89)
		} // end finally (depth: 2)
		IL2CPP_CLEANUP(89)
		{
			IL2CPP_JUMP_TBL(0x60, IL_0060)
			IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
		}

IL_0060:
		{
			goto IL_008a;
		}

IL_0065:
		{
			; // IL_0065: leave IL_006a
		}
	} // end catch (depth: 1)

IL_006a:
	{
		int32_t L_10 = V_0;
		NullCheck((StartObserver_t2832445184 *)__this);
		VirtActionInvoker1< int32_t >::Invoke(8 /* System.Void UniRx.Operators.StartObservable`1/StartObserver<System.Int32>::OnNext(T) */, (StartObserver_t2832445184 *)__this, (int32_t)L_10);
	}

IL_0071:
	try
	{ // begin try (depth: 1)
		Il2CppObject* L_11 = (Il2CppObject*)((OperatorObserverBase_2_t701568569 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		NullCheck((Il2CppObject*)L_11);
		InterfaceActionInvoker0::Invoke(0 /* System.Void UniRx.IObserver`1<System.Int32>::OnCompleted() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3), (Il2CppObject*)L_11);
		IL2CPP_LEAVE(0x8A, FINALLY_0083);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0083;
	}

FINALLY_0083:
	{ // begin finally (depth: 1)
		NullCheck((OperatorObserverBase_2_t701568569 *)__this);
		VirtActionInvoker0::Invoke(4 /* System.Void UniRx.Operators.OperatorObserverBase`2<System.Int32,System.Int32>::Dispose() */, (OperatorObserverBase_2_t701568569 *)__this);
		IL2CPP_END_FINALLY(131)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(131)
	{
		IL2CPP_JUMP_TBL(0x8A, IL_008a)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_008a:
	{
		return;
	}
}
// System.Void UniRx.Operators.StartObservable`1/StartObserver<System.Int32>::OnNext(T)
extern Il2CppClass* Il2CppObject_il2cpp_TypeInfo_var;
extern const uint32_t StartObserver_OnNext_m1843831748_MetadataUsageId;
extern "C"  void StartObserver_OnNext_m1843831748_gshared (StartObserver_t2832445184 * __this, int32_t ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (StartObserver_OnNext_m1843831748_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		Il2CppObject* L_0 = (Il2CppObject*)((OperatorObserverBase_2_t701568569 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		int32_t L_1 = ___value0;
		NullCheck((Il2CppObject*)L_0);
		InterfaceActionInvoker1< int32_t >::Invoke(2 /* System.Void UniRx.IObserver`1<System.Int32>::OnNext(T) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3), (Il2CppObject*)L_0, (int32_t)L_1);
		goto IL_0021;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1967233988 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Il2CppObject_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_0013;
		throw e;
	}

CATCH_0013:
	{ // begin catch(System.Object)
		{
			NullCheck((OperatorObserverBase_2_t701568569 *)__this);
			VirtActionInvoker0::Invoke(4 /* System.Void UniRx.Operators.OperatorObserverBase`2<System.Int32,System.Int32>::Dispose() */, (OperatorObserverBase_2_t701568569 *)__this);
			IL2CPP_RAISE_MANAGED_EXCEPTION(__exception_local);
		}

IL_001c:
		{
			goto IL_0021;
		}
	} // end catch (depth: 1)

IL_0021:
	{
		return;
	}
}
// System.Void UniRx.Operators.StartObservable`1/StartObserver<System.Int32>::OnError(System.Exception)
extern "C"  void StartObserver_OnError_m3079845587_gshared (StartObserver_t2832445184 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method)
{
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		Il2CppObject* L_0 = (Il2CppObject*)((OperatorObserverBase_2_t701568569 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		Exception_t1967233988 * L_1 = ___error0;
		NullCheck((Il2CppObject*)L_0);
		InterfaceActionInvoker1< Exception_t1967233988 * >::Invoke(1 /* System.Void UniRx.IObserver`1<System.Int32>::OnError(System.Exception) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3), (Il2CppObject*)L_0, (Exception_t1967233988 *)L_1);
		IL2CPP_LEAVE(0x1A, FINALLY_0013);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0013;
	}

FINALLY_0013:
	{ // begin finally (depth: 1)
		NullCheck((OperatorObserverBase_2_t701568569 *)__this);
		VirtActionInvoker0::Invoke(4 /* System.Void UniRx.Operators.OperatorObserverBase`2<System.Int32,System.Int32>::Dispose() */, (OperatorObserverBase_2_t701568569 *)__this);
		IL2CPP_END_FINALLY(19)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(19)
	{
		IL2CPP_JUMP_TBL(0x1A, IL_001a)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_001a:
	{
		return;
	}
}
// System.Void UniRx.Operators.StartObservable`1/StartObserver<System.Int32>::OnCompleted()
extern "C"  void StartObserver_OnCompleted_m1672924838_gshared (StartObserver_t2832445184 * __this, const MethodInfo* method)
{
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		Il2CppObject* L_0 = (Il2CppObject*)((OperatorObserverBase_2_t701568569 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		NullCheck((Il2CppObject*)L_0);
		InterfaceActionInvoker0::Invoke(0 /* System.Void UniRx.IObserver`1<System.Int32>::OnCompleted() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3), (Il2CppObject*)L_0);
		IL2CPP_LEAVE(0x19, FINALLY_0012);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0012;
	}

FINALLY_0012:
	{ // begin finally (depth: 1)
		NullCheck((OperatorObserverBase_2_t701568569 *)__this);
		VirtActionInvoker0::Invoke(4 /* System.Void UniRx.Operators.OperatorObserverBase`2<System.Int32,System.Int32>::Dispose() */, (OperatorObserverBase_2_t701568569 *)__this);
		IL2CPP_END_FINALLY(18)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(18)
	{
		IL2CPP_JUMP_TBL(0x19, IL_0019)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0019:
	{
		return;
	}
}
// System.Void UniRx.Operators.StartObservable`1/StartObserver<System.Object>::.ctor(UniRx.Operators.StartObservable`1<T>,UniRx.IObserver`1<T>,System.IDisposable)
extern "C"  void StartObserver__ctor_m278673816_gshared (StartObserver_t822136817 * __this, StartObservable_1_t2362529796 * ___parent0, Il2CppObject* ___observer1, Il2CppObject * ___cancel2, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = ___observer1;
		Il2CppObject * L_1 = ___cancel2;
		NullCheck((OperatorObserverBase_2_t1187768149 *)__this);
		((  void (*) (OperatorObserverBase_2_t1187768149 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((OperatorObserverBase_2_t1187768149 *)__this, (Il2CppObject*)L_0, (Il2CppObject *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		StartObservable_1_t2362529796 * L_2 = ___parent0;
		__this->set_parent_2(L_2);
		return;
	}
}
// System.Void UniRx.Operators.StartObservable`1/StartObserver<System.Object>::Run()
extern Il2CppClass* Il2CppObject_il2cpp_TypeInfo_var;
extern Il2CppClass* Exception_t1967233988_il2cpp_TypeInfo_var;
extern const uint32_t StartObserver_Run_m1779710341_MetadataUsageId;
extern "C"  void StartObserver_Run_m1779710341_gshared (StartObserver_t822136817 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (StartObserver_Run_m1779710341_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Il2CppObject * V_0 = NULL;
	Exception_t1967233988 * V_1 = NULL;
	Il2CppObject * V_2 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Initobj (Il2CppObject_il2cpp_TypeInfo_var, (&V_2));
		Il2CppObject * L_0 = V_2;
		V_0 = (Il2CppObject *)L_0;
	}

IL_000a:
	try
	{ // begin try (depth: 1)
		{
			StartObservable_1_t2362529796 * L_1 = (StartObservable_1_t2362529796 *)__this->get_parent_2();
			NullCheck(L_1);
			Func_1_t1979887667 * L_2 = (Func_1_t1979887667 *)L_1->get_function_2();
			if (!L_2)
			{
				goto IL_0030;
			}
		}

IL_001a:
		{
			StartObservable_1_t2362529796 * L_3 = (StartObservable_1_t2362529796 *)__this->get_parent_2();
			NullCheck(L_3);
			Func_1_t1979887667 * L_4 = (Func_1_t1979887667 *)L_3->get_function_2();
			NullCheck((Func_1_t1979887667 *)L_4);
			Il2CppObject * L_5 = ((  Il2CppObject * (*) (Func_1_t1979887667 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)((Func_1_t1979887667 *)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
			V_0 = (Il2CppObject *)L_5;
			goto IL_0040;
		}

IL_0030:
		{
			StartObservable_1_t2362529796 * L_6 = (StartObservable_1_t2362529796 *)__this->get_parent_2();
			NullCheck(L_6);
			Action_t437523947 * L_7 = (Action_t437523947 *)L_6->get_action_1();
			NullCheck((Action_t437523947 *)L_7);
			Action_Invoke_m1445970038((Action_t437523947 *)L_7, /*hidden argument*/NULL);
		}

IL_0040:
		{
			goto IL_006a;
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1967233988 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Exception_t1967233988_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_0045;
		throw e;
	}

CATCH_0045:
	{ // begin catch(System.Exception)
		{
			V_1 = (Exception_t1967233988 *)((Exception_t1967233988 *)__exception_local);
		}

IL_0046:
		try
		{ // begin try (depth: 2)
			Il2CppObject* L_8 = (Il2CppObject*)((OperatorObserverBase_2_t1187768149 *)__this)->get_observer_0();
			il2cpp_codegen_memory_barrier();
			Exception_t1967233988 * L_9 = V_1;
			NullCheck((Il2CppObject*)L_8);
			InterfaceActionInvoker1< Exception_t1967233988 * >::Invoke(1 /* System.Void UniRx.IObserver`1<System.Object>::OnError(System.Exception) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3), (Il2CppObject*)L_8, (Exception_t1967233988 *)L_9);
			IL2CPP_LEAVE(0x60, FINALLY_0059);
		} // end try (depth: 2)
		catch(Il2CppExceptionWrapper& e)
		{
			__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
			goto FINALLY_0059;
		}

FINALLY_0059:
		{ // begin finally (depth: 2)
			NullCheck((OperatorObserverBase_2_t1187768149 *)__this);
			VirtActionInvoker0::Invoke(4 /* System.Void UniRx.Operators.OperatorObserverBase`2<System.Object,System.Object>::Dispose() */, (OperatorObserverBase_2_t1187768149 *)__this);
			IL2CPP_END_FINALLY(89)
		} // end finally (depth: 2)
		IL2CPP_CLEANUP(89)
		{
			IL2CPP_JUMP_TBL(0x60, IL_0060)
			IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
		}

IL_0060:
		{
			goto IL_008a;
		}

IL_0065:
		{
			; // IL_0065: leave IL_006a
		}
	} // end catch (depth: 1)

IL_006a:
	{
		Il2CppObject * L_10 = V_0;
		NullCheck((StartObserver_t822136817 *)__this);
		VirtActionInvoker1< Il2CppObject * >::Invoke(8 /* System.Void UniRx.Operators.StartObservable`1/StartObserver<System.Object>::OnNext(T) */, (StartObserver_t822136817 *)__this, (Il2CppObject *)L_10);
	}

IL_0071:
	try
	{ // begin try (depth: 1)
		Il2CppObject* L_11 = (Il2CppObject*)((OperatorObserverBase_2_t1187768149 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		NullCheck((Il2CppObject*)L_11);
		InterfaceActionInvoker0::Invoke(0 /* System.Void UniRx.IObserver`1<System.Object>::OnCompleted() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3), (Il2CppObject*)L_11);
		IL2CPP_LEAVE(0x8A, FINALLY_0083);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0083;
	}

FINALLY_0083:
	{ // begin finally (depth: 1)
		NullCheck((OperatorObserverBase_2_t1187768149 *)__this);
		VirtActionInvoker0::Invoke(4 /* System.Void UniRx.Operators.OperatorObserverBase`2<System.Object,System.Object>::Dispose() */, (OperatorObserverBase_2_t1187768149 *)__this);
		IL2CPP_END_FINALLY(131)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(131)
	{
		IL2CPP_JUMP_TBL(0x8A, IL_008a)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_008a:
	{
		return;
	}
}
// System.Void UniRx.Operators.StartObservable`1/StartObserver<System.Object>::OnNext(T)
extern Il2CppClass* Il2CppObject_il2cpp_TypeInfo_var;
extern const uint32_t StartObserver_OnNext_m2793218691_MetadataUsageId;
extern "C"  void StartObserver_OnNext_m2793218691_gshared (StartObserver_t822136817 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (StartObserver_OnNext_m2793218691_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		Il2CppObject* L_0 = (Il2CppObject*)((OperatorObserverBase_2_t1187768149 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		Il2CppObject * L_1 = ___value0;
		NullCheck((Il2CppObject*)L_0);
		InterfaceActionInvoker1< Il2CppObject * >::Invoke(2 /* System.Void UniRx.IObserver`1<System.Object>::OnNext(T) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3), (Il2CppObject*)L_0, (Il2CppObject *)L_1);
		goto IL_0021;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1967233988 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Il2CppObject_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_0013;
		throw e;
	}

CATCH_0013:
	{ // begin catch(System.Object)
		{
			NullCheck((OperatorObserverBase_2_t1187768149 *)__this);
			VirtActionInvoker0::Invoke(4 /* System.Void UniRx.Operators.OperatorObserverBase`2<System.Object,System.Object>::Dispose() */, (OperatorObserverBase_2_t1187768149 *)__this);
			IL2CPP_RAISE_MANAGED_EXCEPTION(__exception_local);
		}

IL_001c:
		{
			goto IL_0021;
		}
	} // end catch (depth: 1)

IL_0021:
	{
		return;
	}
}
// System.Void UniRx.Operators.StartObservable`1/StartObserver<System.Object>::OnError(System.Exception)
extern "C"  void StartObserver_OnError_m3458848658_gshared (StartObserver_t822136817 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method)
{
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		Il2CppObject* L_0 = (Il2CppObject*)((OperatorObserverBase_2_t1187768149 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		Exception_t1967233988 * L_1 = ___error0;
		NullCheck((Il2CppObject*)L_0);
		InterfaceActionInvoker1< Exception_t1967233988 * >::Invoke(1 /* System.Void UniRx.IObserver`1<System.Object>::OnError(System.Exception) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3), (Il2CppObject*)L_0, (Exception_t1967233988 *)L_1);
		IL2CPP_LEAVE(0x1A, FINALLY_0013);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0013;
	}

FINALLY_0013:
	{ // begin finally (depth: 1)
		NullCheck((OperatorObserverBase_2_t1187768149 *)__this);
		VirtActionInvoker0::Invoke(4 /* System.Void UniRx.Operators.OperatorObserverBase`2<System.Object,System.Object>::Dispose() */, (OperatorObserverBase_2_t1187768149 *)__this);
		IL2CPP_END_FINALLY(19)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(19)
	{
		IL2CPP_JUMP_TBL(0x1A, IL_001a)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_001a:
	{
		return;
	}
}
// System.Void UniRx.Operators.StartObservable`1/StartObserver<System.Object>::OnCompleted()
extern "C"  void StartObserver_OnCompleted_m1533138405_gshared (StartObserver_t822136817 * __this, const MethodInfo* method)
{
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		Il2CppObject* L_0 = (Il2CppObject*)((OperatorObserverBase_2_t1187768149 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		NullCheck((Il2CppObject*)L_0);
		InterfaceActionInvoker0::Invoke(0 /* System.Void UniRx.IObserver`1<System.Object>::OnCompleted() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3), (Il2CppObject*)L_0);
		IL2CPP_LEAVE(0x19, FINALLY_0012);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0012;
	}

FINALLY_0012:
	{ // begin finally (depth: 1)
		NullCheck((OperatorObserverBase_2_t1187768149 *)__this);
		VirtActionInvoker0::Invoke(4 /* System.Void UniRx.Operators.OperatorObserverBase`2<System.Object,System.Object>::Dispose() */, (OperatorObserverBase_2_t1187768149 *)__this);
		IL2CPP_END_FINALLY(18)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(18)
	{
		IL2CPP_JUMP_TBL(0x19, IL_0019)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0019:
	{
		return;
	}
}
// System.Void UniRx.Operators.StartObservable`1/StartObserver<UniRx.Unit>::.ctor(UniRx.Operators.StartObservable`1<T>,UniRx.IObserver`1<T>,System.IDisposable)
extern "C"  void StartObserver__ctor_m828476198_gshared (StartObserver_t2543316435 * __this, StartObservable_1_t4083709414 * ___parent0, Il2CppObject* ___observer1, Il2CppObject * ___cancel2, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = ___observer1;
		Il2CppObject * L_1 = ___cancel2;
		NullCheck((OperatorObserverBase_2_t4165376397 *)__this);
		((  void (*) (OperatorObserverBase_2_t4165376397 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((OperatorObserverBase_2_t4165376397 *)__this, (Il2CppObject*)L_0, (Il2CppObject *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		StartObservable_1_t4083709414 * L_2 = ___parent0;
		__this->set_parent_2(L_2);
		return;
	}
}
// System.Void UniRx.Operators.StartObservable`1/StartObserver<UniRx.Unit>::Run()
extern Il2CppClass* Unit_t2558286038_il2cpp_TypeInfo_var;
extern Il2CppClass* Exception_t1967233988_il2cpp_TypeInfo_var;
extern const uint32_t StartObserver_Run_m4024216466_MetadataUsageId;
extern "C"  void StartObserver_Run_m4024216466_gshared (StartObserver_t2543316435 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (StartObserver_Run_m4024216466_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Unit_t2558286038  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Exception_t1967233988 * V_1 = NULL;
	Unit_t2558286038  V_2;
	memset(&V_2, 0, sizeof(V_2));
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Initobj (Unit_t2558286038_il2cpp_TypeInfo_var, (&V_2));
		Unit_t2558286038  L_0 = V_2;
		V_0 = (Unit_t2558286038 )L_0;
	}

IL_000a:
	try
	{ // begin try (depth: 1)
		{
			StartObservable_1_t4083709414 * L_1 = (StartObservable_1_t4083709414 *)__this->get_parent_2();
			NullCheck(L_1);
			Func_1_t3701067285 * L_2 = (Func_1_t3701067285 *)L_1->get_function_2();
			if (!L_2)
			{
				goto IL_0030;
			}
		}

IL_001a:
		{
			StartObservable_1_t4083709414 * L_3 = (StartObservable_1_t4083709414 *)__this->get_parent_2();
			NullCheck(L_3);
			Func_1_t3701067285 * L_4 = (Func_1_t3701067285 *)L_3->get_function_2();
			NullCheck((Func_1_t3701067285 *)L_4);
			Unit_t2558286038  L_5 = ((  Unit_t2558286038  (*) (Func_1_t3701067285 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)((Func_1_t3701067285 *)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
			V_0 = (Unit_t2558286038 )L_5;
			goto IL_0040;
		}

IL_0030:
		{
			StartObservable_1_t4083709414 * L_6 = (StartObservable_1_t4083709414 *)__this->get_parent_2();
			NullCheck(L_6);
			Action_t437523947 * L_7 = (Action_t437523947 *)L_6->get_action_1();
			NullCheck((Action_t437523947 *)L_7);
			Action_Invoke_m1445970038((Action_t437523947 *)L_7, /*hidden argument*/NULL);
		}

IL_0040:
		{
			goto IL_006a;
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1967233988 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Exception_t1967233988_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_0045;
		throw e;
	}

CATCH_0045:
	{ // begin catch(System.Exception)
		{
			V_1 = (Exception_t1967233988 *)((Exception_t1967233988 *)__exception_local);
		}

IL_0046:
		try
		{ // begin try (depth: 2)
			Il2CppObject* L_8 = (Il2CppObject*)((OperatorObserverBase_2_t4165376397 *)__this)->get_observer_0();
			il2cpp_codegen_memory_barrier();
			Exception_t1967233988 * L_9 = V_1;
			NullCheck((Il2CppObject*)L_8);
			InterfaceActionInvoker1< Exception_t1967233988 * >::Invoke(1 /* System.Void UniRx.IObserver`1<UniRx.Unit>::OnError(System.Exception) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3), (Il2CppObject*)L_8, (Exception_t1967233988 *)L_9);
			IL2CPP_LEAVE(0x60, FINALLY_0059);
		} // end try (depth: 2)
		catch(Il2CppExceptionWrapper& e)
		{
			__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
			goto FINALLY_0059;
		}

FINALLY_0059:
		{ // begin finally (depth: 2)
			NullCheck((OperatorObserverBase_2_t4165376397 *)__this);
			VirtActionInvoker0::Invoke(4 /* System.Void UniRx.Operators.OperatorObserverBase`2<UniRx.Unit,UniRx.Unit>::Dispose() */, (OperatorObserverBase_2_t4165376397 *)__this);
			IL2CPP_END_FINALLY(89)
		} // end finally (depth: 2)
		IL2CPP_CLEANUP(89)
		{
			IL2CPP_JUMP_TBL(0x60, IL_0060)
			IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
		}

IL_0060:
		{
			goto IL_008a;
		}

IL_0065:
		{
			; // IL_0065: leave IL_006a
		}
	} // end catch (depth: 1)

IL_006a:
	{
		Unit_t2558286038  L_10 = V_0;
		NullCheck((StartObserver_t2543316435 *)__this);
		VirtActionInvoker1< Unit_t2558286038  >::Invoke(8 /* System.Void UniRx.Operators.StartObservable`1/StartObserver<UniRx.Unit>::OnNext(T) */, (StartObserver_t2543316435 *)__this, (Unit_t2558286038 )L_10);
	}

IL_0071:
	try
	{ // begin try (depth: 1)
		Il2CppObject* L_11 = (Il2CppObject*)((OperatorObserverBase_2_t4165376397 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		NullCheck((Il2CppObject*)L_11);
		InterfaceActionInvoker0::Invoke(0 /* System.Void UniRx.IObserver`1<UniRx.Unit>::OnCompleted() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3), (Il2CppObject*)L_11);
		IL2CPP_LEAVE(0x8A, FINALLY_0083);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0083;
	}

FINALLY_0083:
	{ // begin finally (depth: 1)
		NullCheck((OperatorObserverBase_2_t4165376397 *)__this);
		VirtActionInvoker0::Invoke(4 /* System.Void UniRx.Operators.OperatorObserverBase`2<UniRx.Unit,UniRx.Unit>::Dispose() */, (OperatorObserverBase_2_t4165376397 *)__this);
		IL2CPP_END_FINALLY(131)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(131)
	{
		IL2CPP_JUMP_TBL(0x8A, IL_008a)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_008a:
	{
		return;
	}
}
// System.Void UniRx.Operators.StartObservable`1/StartObserver<UniRx.Unit>::OnNext(T)
extern Il2CppClass* Il2CppObject_il2cpp_TypeInfo_var;
extern const uint32_t StartObserver_OnNext_m1333910929_MetadataUsageId;
extern "C"  void StartObserver_OnNext_m1333910929_gshared (StartObserver_t2543316435 * __this, Unit_t2558286038  ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (StartObserver_OnNext_m1333910929_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		Il2CppObject* L_0 = (Il2CppObject*)((OperatorObserverBase_2_t4165376397 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		Unit_t2558286038  L_1 = ___value0;
		NullCheck((Il2CppObject*)L_0);
		InterfaceActionInvoker1< Unit_t2558286038  >::Invoke(2 /* System.Void UniRx.IObserver`1<UniRx.Unit>::OnNext(T) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3), (Il2CppObject*)L_0, (Unit_t2558286038 )L_1);
		goto IL_0021;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1967233988 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Il2CppObject_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_0013;
		throw e;
	}

CATCH_0013:
	{ // begin catch(System.Object)
		{
			NullCheck((OperatorObserverBase_2_t4165376397 *)__this);
			VirtActionInvoker0::Invoke(4 /* System.Void UniRx.Operators.OperatorObserverBase`2<UniRx.Unit,UniRx.Unit>::Dispose() */, (OperatorObserverBase_2_t4165376397 *)__this);
			IL2CPP_RAISE_MANAGED_EXCEPTION(__exception_local);
		}

IL_001c:
		{
			goto IL_0021;
		}
	} // end catch (depth: 1)

IL_0021:
	{
		return;
	}
}
// System.Void UniRx.Operators.StartObservable`1/StartObserver<UniRx.Unit>::OnError(System.Exception)
extern "C"  void StartObserver_OnError_m1839380128_gshared (StartObserver_t2543316435 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method)
{
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		Il2CppObject* L_0 = (Il2CppObject*)((OperatorObserverBase_2_t4165376397 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		Exception_t1967233988 * L_1 = ___error0;
		NullCheck((Il2CppObject*)L_0);
		InterfaceActionInvoker1< Exception_t1967233988 * >::Invoke(1 /* System.Void UniRx.IObserver`1<UniRx.Unit>::OnError(System.Exception) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3), (Il2CppObject*)L_0, (Exception_t1967233988 *)L_1);
		IL2CPP_LEAVE(0x1A, FINALLY_0013);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0013;
	}

FINALLY_0013:
	{ // begin finally (depth: 1)
		NullCheck((OperatorObserverBase_2_t4165376397 *)__this);
		VirtActionInvoker0::Invoke(4 /* System.Void UniRx.Operators.OperatorObserverBase`2<UniRx.Unit,UniRx.Unit>::Dispose() */, (OperatorObserverBase_2_t4165376397 *)__this);
		IL2CPP_END_FINALLY(19)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(19)
	{
		IL2CPP_JUMP_TBL(0x1A, IL_001a)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_001a:
	{
		return;
	}
}
// System.Void UniRx.Operators.StartObservable`1/StartObserver<UniRx.Unit>::OnCompleted()
extern "C"  void StartObserver_OnCompleted_m777411059_gshared (StartObserver_t2543316435 * __this, const MethodInfo* method)
{
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		Il2CppObject* L_0 = (Il2CppObject*)((OperatorObserverBase_2_t4165376397 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		NullCheck((Il2CppObject*)L_0);
		InterfaceActionInvoker0::Invoke(0 /* System.Void UniRx.IObserver`1<UniRx.Unit>::OnCompleted() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3), (Il2CppObject*)L_0);
		IL2CPP_LEAVE(0x19, FINALLY_0012);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0012;
	}

FINALLY_0012:
	{ // begin finally (depth: 1)
		NullCheck((OperatorObserverBase_2_t4165376397 *)__this);
		VirtActionInvoker0::Invoke(4 /* System.Void UniRx.Operators.OperatorObserverBase`2<UniRx.Unit,UniRx.Unit>::Dispose() */, (OperatorObserverBase_2_t4165376397 *)__this);
		IL2CPP_END_FINALLY(18)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(18)
	{
		IL2CPP_JUMP_TBL(0x19, IL_0019)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0019:
	{
		return;
	}
}
// System.Void UniRx.Operators.StartObservable`1<System.Int32>::.ctor(System.Func`1<T>,System.Nullable`1<System.TimeSpan>,UniRx.IScheduler)
extern Il2CppClass* Scheduler_t103934925_il2cpp_TypeInfo_var;
extern const uint32_t StartObservable_1__ctor_m3852172604_MetadataUsageId;
extern "C"  void StartObservable_1__ctor_m3852172604_gshared (StartObservable_1_t77870867 * __this, Func_1_t3990196034 * ___function0, Nullable_1_t3649900800  ___startAfter1, Il2CppObject * ___scheduler2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (StartObservable_1__ctor_m3852172604_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject * L_0 = ___scheduler2;
		IL2CPP_RUNTIME_CLASS_INIT(Scheduler_t103934925_il2cpp_TypeInfo_var);
		Il2CppObject * L_1 = ((Scheduler_t103934925_StaticFields*)Scheduler_t103934925_il2cpp_TypeInfo_var->static_fields)->get_CurrentThread_0();
		NullCheck((OperatorObservableBase_1_t1911559758 *)__this);
		((  void (*) (OperatorObservableBase_1_t1911559758 *, bool, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((OperatorObservableBase_1_t1911559758 *)__this, (bool)((((Il2CppObject*)(Il2CppObject *)L_0) == ((Il2CppObject*)(Il2CppObject *)L_1))? 1 : 0), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		Func_1_t3990196034 * L_2 = ___function0;
		__this->set_function_2(L_2);
		Nullable_1_t3649900800  L_3 = ___startAfter1;
		__this->set_startAfter_4(L_3);
		Il2CppObject * L_4 = ___scheduler2;
		__this->set_scheduler_3(L_4);
		return;
	}
}
// System.Void UniRx.Operators.StartObservable`1<System.Int32>::.ctor(System.Action,System.Nullable`1<System.TimeSpan>,UniRx.IScheduler)
extern Il2CppClass* Scheduler_t103934925_il2cpp_TypeInfo_var;
extern const uint32_t StartObservable_1__ctor_m1752235073_MetadataUsageId;
extern "C"  void StartObservable_1__ctor_m1752235073_gshared (StartObservable_1_t77870867 * __this, Action_t437523947 * ___action0, Nullable_1_t3649900800  ___startAfter1, Il2CppObject * ___scheduler2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (StartObservable_1__ctor_m1752235073_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject * L_0 = ___scheduler2;
		IL2CPP_RUNTIME_CLASS_INIT(Scheduler_t103934925_il2cpp_TypeInfo_var);
		Il2CppObject * L_1 = ((Scheduler_t103934925_StaticFields*)Scheduler_t103934925_il2cpp_TypeInfo_var->static_fields)->get_CurrentThread_0();
		NullCheck((OperatorObservableBase_1_t1911559758 *)__this);
		((  void (*) (OperatorObservableBase_1_t1911559758 *, bool, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((OperatorObservableBase_1_t1911559758 *)__this, (bool)((((Il2CppObject*)(Il2CppObject *)L_0) == ((Il2CppObject*)(Il2CppObject *)L_1))? 1 : 0), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		Action_t437523947 * L_2 = ___action0;
		__this->set_action_1(L_2);
		Nullable_1_t3649900800  L_3 = ___startAfter1;
		__this->set_startAfter_4(L_3);
		Il2CppObject * L_4 = ___scheduler2;
		__this->set_scheduler_3(L_4);
		return;
	}
}
// System.IDisposable UniRx.Operators.StartObservable`1<System.Int32>::SubscribeCore(UniRx.IObserver`1<T>,System.IDisposable)
extern Il2CppClass* Action_t437523947_il2cpp_TypeInfo_var;
extern Il2CppClass* IScheduler_t2938318244_il2cpp_TypeInfo_var;
extern const MethodInfo* Nullable_1_get_HasValue_m2797118855_MethodInfo_var;
extern const MethodInfo* Nullable_1_get_Value_m3338249190_MethodInfo_var;
extern const uint32_t StartObservable_1_SubscribeCore_m109930563_MetadataUsageId;
extern "C"  Il2CppObject * StartObservable_1_SubscribeCore_m109930563_gshared (StartObservable_1_t77870867 * __this, Il2CppObject* ___observer0, Il2CppObject * ___cancel1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (StartObservable_1_SubscribeCore_m109930563_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Nullable_1_t3649900800  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Nullable_1_t3649900800  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		Nullable_1_t3649900800  L_0 = (Nullable_1_t3649900800 )__this->get_startAfter_4();
		V_0 = (Nullable_1_t3649900800 )L_0;
		bool L_1 = Nullable_1_get_HasValue_m2797118855((Nullable_1_t3649900800 *)(&V_0), /*hidden argument*/Nullable_1_get_HasValue_m2797118855_MethodInfo_var);
		if (!L_1)
		{
			goto IL_0040;
		}
	}
	{
		Il2CppObject * L_2 = (Il2CppObject *)__this->get_scheduler_3();
		Nullable_1_t3649900800  L_3 = (Nullable_1_t3649900800 )__this->get_startAfter_4();
		V_1 = (Nullable_1_t3649900800 )L_3;
		TimeSpan_t763862892  L_4 = Nullable_1_get_Value_m3338249190((Nullable_1_t3649900800 *)(&V_1), /*hidden argument*/Nullable_1_get_Value_m3338249190_MethodInfo_var);
		Il2CppObject* L_5 = ___observer0;
		Il2CppObject * L_6 = ___cancel1;
		StartObserver_t2832445184 * L_7 = (StartObserver_t2832445184 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		((  void (*) (StartObserver_t2832445184 *, StartObservable_1_t77870867 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)(L_7, (StartObservable_1_t77870867 *)__this, (Il2CppObject*)L_5, (Il2CppObject *)L_6, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		IntPtr_t L_8;
		L_8.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		Action_t437523947 * L_9 = (Action_t437523947 *)il2cpp_codegen_object_new(Action_t437523947_il2cpp_TypeInfo_var);
		Action__ctor_m2957240604(L_9, (Il2CppObject *)L_7, (IntPtr_t)L_8, /*hidden argument*/NULL);
		NullCheck((Il2CppObject *)L_2);
		Il2CppObject * L_10 = InterfaceFuncInvoker2< Il2CppObject *, TimeSpan_t763862892 , Action_t437523947 * >::Invoke(2 /* System.IDisposable UniRx.IScheduler::Schedule(System.TimeSpan,System.Action) */, IScheduler_t2938318244_il2cpp_TypeInfo_var, (Il2CppObject *)L_2, (TimeSpan_t763862892 )L_4, (Action_t437523947 *)L_9);
		return L_10;
	}

IL_0040:
	{
		Il2CppObject * L_11 = (Il2CppObject *)__this->get_scheduler_3();
		Il2CppObject* L_12 = ___observer0;
		Il2CppObject * L_13 = ___cancel1;
		StartObserver_t2832445184 * L_14 = (StartObserver_t2832445184 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		((  void (*) (StartObserver_t2832445184 *, StartObservable_1_t77870867 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)(L_14, (StartObservable_1_t77870867 *)__this, (Il2CppObject*)L_12, (Il2CppObject *)L_13, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		IntPtr_t L_15;
		L_15.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		Action_t437523947 * L_16 = (Action_t437523947 *)il2cpp_codegen_object_new(Action_t437523947_il2cpp_TypeInfo_var);
		Action__ctor_m2957240604(L_16, (Il2CppObject *)L_14, (IntPtr_t)L_15, /*hidden argument*/NULL);
		NullCheck((Il2CppObject *)L_11);
		Il2CppObject * L_17 = InterfaceFuncInvoker1< Il2CppObject *, Action_t437523947 * >::Invoke(1 /* System.IDisposable UniRx.IScheduler::Schedule(System.Action) */, IScheduler_t2938318244_il2cpp_TypeInfo_var, (Il2CppObject *)L_11, (Action_t437523947 *)L_16);
		return L_17;
	}
}
// System.Void UniRx.Operators.StartObservable`1<System.Object>::.ctor(System.Func`1<T>,System.Nullable`1<System.TimeSpan>,UniRx.IScheduler)
extern Il2CppClass* Scheduler_t103934925_il2cpp_TypeInfo_var;
extern const uint32_t StartObservable_1__ctor_m2557532617_MetadataUsageId;
extern "C"  void StartObservable_1__ctor_m2557532617_gshared (StartObservable_1_t2362529796 * __this, Func_1_t1979887667 * ___function0, Nullable_1_t3649900800  ___startAfter1, Il2CppObject * ___scheduler2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (StartObservable_1__ctor_m2557532617_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject * L_0 = ___scheduler2;
		IL2CPP_RUNTIME_CLASS_INIT(Scheduler_t103934925_il2cpp_TypeInfo_var);
		Il2CppObject * L_1 = ((Scheduler_t103934925_StaticFields*)Scheduler_t103934925_il2cpp_TypeInfo_var->static_fields)->get_CurrentThread_0();
		NullCheck((OperatorObservableBase_1_t4196218687 *)__this);
		((  void (*) (OperatorObservableBase_1_t4196218687 *, bool, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((OperatorObservableBase_1_t4196218687 *)__this, (bool)((((Il2CppObject*)(Il2CppObject *)L_0) == ((Il2CppObject*)(Il2CppObject *)L_1))? 1 : 0), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		Func_1_t1979887667 * L_2 = ___function0;
		__this->set_function_2(L_2);
		Nullable_1_t3649900800  L_3 = ___startAfter1;
		__this->set_startAfter_4(L_3);
		Il2CppObject * L_4 = ___scheduler2;
		__this->set_scheduler_3(L_4);
		return;
	}
}
// System.Void UniRx.Operators.StartObservable`1<System.Object>::.ctor(System.Action,System.Nullable`1<System.TimeSpan>,UniRx.IScheduler)
extern Il2CppClass* Scheduler_t103934925_il2cpp_TypeInfo_var;
extern const uint32_t StartObservable_1__ctor_m4026184404_MetadataUsageId;
extern "C"  void StartObservable_1__ctor_m4026184404_gshared (StartObservable_1_t2362529796 * __this, Action_t437523947 * ___action0, Nullable_1_t3649900800  ___startAfter1, Il2CppObject * ___scheduler2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (StartObservable_1__ctor_m4026184404_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject * L_0 = ___scheduler2;
		IL2CPP_RUNTIME_CLASS_INIT(Scheduler_t103934925_il2cpp_TypeInfo_var);
		Il2CppObject * L_1 = ((Scheduler_t103934925_StaticFields*)Scheduler_t103934925_il2cpp_TypeInfo_var->static_fields)->get_CurrentThread_0();
		NullCheck((OperatorObservableBase_1_t4196218687 *)__this);
		((  void (*) (OperatorObservableBase_1_t4196218687 *, bool, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((OperatorObservableBase_1_t4196218687 *)__this, (bool)((((Il2CppObject*)(Il2CppObject *)L_0) == ((Il2CppObject*)(Il2CppObject *)L_1))? 1 : 0), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		Action_t437523947 * L_2 = ___action0;
		__this->set_action_1(L_2);
		Nullable_1_t3649900800  L_3 = ___startAfter1;
		__this->set_startAfter_4(L_3);
		Il2CppObject * L_4 = ___scheduler2;
		__this->set_scheduler_3(L_4);
		return;
	}
}
// System.IDisposable UniRx.Operators.StartObservable`1<System.Object>::SubscribeCore(UniRx.IObserver`1<T>,System.IDisposable)
extern Il2CppClass* Action_t437523947_il2cpp_TypeInfo_var;
extern Il2CppClass* IScheduler_t2938318244_il2cpp_TypeInfo_var;
extern const MethodInfo* Nullable_1_get_HasValue_m2797118855_MethodInfo_var;
extern const MethodInfo* Nullable_1_get_Value_m3338249190_MethodInfo_var;
extern const uint32_t StartObservable_1_SubscribeCore_m958450028_MetadataUsageId;
extern "C"  Il2CppObject * StartObservable_1_SubscribeCore_m958450028_gshared (StartObservable_1_t2362529796 * __this, Il2CppObject* ___observer0, Il2CppObject * ___cancel1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (StartObservable_1_SubscribeCore_m958450028_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Nullable_1_t3649900800  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Nullable_1_t3649900800  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		Nullable_1_t3649900800  L_0 = (Nullable_1_t3649900800 )__this->get_startAfter_4();
		V_0 = (Nullable_1_t3649900800 )L_0;
		bool L_1 = Nullable_1_get_HasValue_m2797118855((Nullable_1_t3649900800 *)(&V_0), /*hidden argument*/Nullable_1_get_HasValue_m2797118855_MethodInfo_var);
		if (!L_1)
		{
			goto IL_0040;
		}
	}
	{
		Il2CppObject * L_2 = (Il2CppObject *)__this->get_scheduler_3();
		Nullable_1_t3649900800  L_3 = (Nullable_1_t3649900800 )__this->get_startAfter_4();
		V_1 = (Nullable_1_t3649900800 )L_3;
		TimeSpan_t763862892  L_4 = Nullable_1_get_Value_m3338249190((Nullable_1_t3649900800 *)(&V_1), /*hidden argument*/Nullable_1_get_Value_m3338249190_MethodInfo_var);
		Il2CppObject* L_5 = ___observer0;
		Il2CppObject * L_6 = ___cancel1;
		StartObserver_t822136817 * L_7 = (StartObserver_t822136817 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		((  void (*) (StartObserver_t822136817 *, StartObservable_1_t2362529796 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)(L_7, (StartObservable_1_t2362529796 *)__this, (Il2CppObject*)L_5, (Il2CppObject *)L_6, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		IntPtr_t L_8;
		L_8.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		Action_t437523947 * L_9 = (Action_t437523947 *)il2cpp_codegen_object_new(Action_t437523947_il2cpp_TypeInfo_var);
		Action__ctor_m2957240604(L_9, (Il2CppObject *)L_7, (IntPtr_t)L_8, /*hidden argument*/NULL);
		NullCheck((Il2CppObject *)L_2);
		Il2CppObject * L_10 = InterfaceFuncInvoker2< Il2CppObject *, TimeSpan_t763862892 , Action_t437523947 * >::Invoke(2 /* System.IDisposable UniRx.IScheduler::Schedule(System.TimeSpan,System.Action) */, IScheduler_t2938318244_il2cpp_TypeInfo_var, (Il2CppObject *)L_2, (TimeSpan_t763862892 )L_4, (Action_t437523947 *)L_9);
		return L_10;
	}

IL_0040:
	{
		Il2CppObject * L_11 = (Il2CppObject *)__this->get_scheduler_3();
		Il2CppObject* L_12 = ___observer0;
		Il2CppObject * L_13 = ___cancel1;
		StartObserver_t822136817 * L_14 = (StartObserver_t822136817 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		((  void (*) (StartObserver_t822136817 *, StartObservable_1_t2362529796 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)(L_14, (StartObservable_1_t2362529796 *)__this, (Il2CppObject*)L_12, (Il2CppObject *)L_13, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		IntPtr_t L_15;
		L_15.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		Action_t437523947 * L_16 = (Action_t437523947 *)il2cpp_codegen_object_new(Action_t437523947_il2cpp_TypeInfo_var);
		Action__ctor_m2957240604(L_16, (Il2CppObject *)L_14, (IntPtr_t)L_15, /*hidden argument*/NULL);
		NullCheck((Il2CppObject *)L_11);
		Il2CppObject * L_17 = InterfaceFuncInvoker1< Il2CppObject *, Action_t437523947 * >::Invoke(1 /* System.IDisposable UniRx.IScheduler::Schedule(System.Action) */, IScheduler_t2938318244_il2cpp_TypeInfo_var, (Il2CppObject *)L_11, (Action_t437523947 *)L_16);
		return L_17;
	}
}
// System.Void UniRx.Operators.StartObservable`1<UniRx.Unit>::.ctor(System.Func`1<T>,System.Nullable`1<System.TimeSpan>,UniRx.IScheduler)
extern Il2CppClass* Scheduler_t103934925_il2cpp_TypeInfo_var;
extern const uint32_t StartObservable_1__ctor_m1030876681_MetadataUsageId;
extern "C"  void StartObservable_1__ctor_m1030876681_gshared (StartObservable_1_t4083709414 * __this, Func_1_t3701067285 * ___function0, Nullable_1_t3649900800  ___startAfter1, Il2CppObject * ___scheduler2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (StartObservable_1__ctor_m1030876681_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject * L_0 = ___scheduler2;
		IL2CPP_RUNTIME_CLASS_INIT(Scheduler_t103934925_il2cpp_TypeInfo_var);
		Il2CppObject * L_1 = ((Scheduler_t103934925_StaticFields*)Scheduler_t103934925_il2cpp_TypeInfo_var->static_fields)->get_CurrentThread_0();
		NullCheck((OperatorObservableBase_1_t1622431009 *)__this);
		((  void (*) (OperatorObservableBase_1_t1622431009 *, bool, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((OperatorObservableBase_1_t1622431009 *)__this, (bool)((((Il2CppObject*)(Il2CppObject *)L_0) == ((Il2CppObject*)(Il2CppObject *)L_1))? 1 : 0), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		Func_1_t3701067285 * L_2 = ___function0;
		__this->set_function_2(L_2);
		Nullable_1_t3649900800  L_3 = ___startAfter1;
		__this->set_startAfter_4(L_3);
		Il2CppObject * L_4 = ___scheduler2;
		__this->set_scheduler_3(L_4);
		return;
	}
}
// System.Void UniRx.Operators.StartObservable`1<UniRx.Unit>::.ctor(System.Action,System.Nullable`1<System.TimeSpan>,UniRx.IScheduler)
extern Il2CppClass* Scheduler_t103934925_il2cpp_TypeInfo_var;
extern const uint32_t StartObservable_1__ctor_m1869350548_MetadataUsageId;
extern "C"  void StartObservable_1__ctor_m1869350548_gshared (StartObservable_1_t4083709414 * __this, Action_t437523947 * ___action0, Nullable_1_t3649900800  ___startAfter1, Il2CppObject * ___scheduler2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (StartObservable_1__ctor_m1869350548_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject * L_0 = ___scheduler2;
		IL2CPP_RUNTIME_CLASS_INIT(Scheduler_t103934925_il2cpp_TypeInfo_var);
		Il2CppObject * L_1 = ((Scheduler_t103934925_StaticFields*)Scheduler_t103934925_il2cpp_TypeInfo_var->static_fields)->get_CurrentThread_0();
		NullCheck((OperatorObservableBase_1_t1622431009 *)__this);
		((  void (*) (OperatorObservableBase_1_t1622431009 *, bool, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((OperatorObservableBase_1_t1622431009 *)__this, (bool)((((Il2CppObject*)(Il2CppObject *)L_0) == ((Il2CppObject*)(Il2CppObject *)L_1))? 1 : 0), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		Action_t437523947 * L_2 = ___action0;
		__this->set_action_1(L_2);
		Nullable_1_t3649900800  L_3 = ___startAfter1;
		__this->set_startAfter_4(L_3);
		Il2CppObject * L_4 = ___scheduler2;
		__this->set_scheduler_3(L_4);
		return;
	}
}
// System.IDisposable UniRx.Operators.StartObservable`1<UniRx.Unit>::SubscribeCore(UniRx.IObserver`1<T>,System.IDisposable)
extern Il2CppClass* Action_t437523947_il2cpp_TypeInfo_var;
extern Il2CppClass* IScheduler_t2938318244_il2cpp_TypeInfo_var;
extern const MethodInfo* Nullable_1_get_HasValue_m2797118855_MethodInfo_var;
extern const MethodInfo* Nullable_1_get_Value_m3338249190_MethodInfo_var;
extern const uint32_t StartObservable_1_SubscribeCore_m1104168598_MetadataUsageId;
extern "C"  Il2CppObject * StartObservable_1_SubscribeCore_m1104168598_gshared (StartObservable_1_t4083709414 * __this, Il2CppObject* ___observer0, Il2CppObject * ___cancel1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (StartObservable_1_SubscribeCore_m1104168598_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Nullable_1_t3649900800  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Nullable_1_t3649900800  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		Nullable_1_t3649900800  L_0 = (Nullable_1_t3649900800 )__this->get_startAfter_4();
		V_0 = (Nullable_1_t3649900800 )L_0;
		bool L_1 = Nullable_1_get_HasValue_m2797118855((Nullable_1_t3649900800 *)(&V_0), /*hidden argument*/Nullable_1_get_HasValue_m2797118855_MethodInfo_var);
		if (!L_1)
		{
			goto IL_0040;
		}
	}
	{
		Il2CppObject * L_2 = (Il2CppObject *)__this->get_scheduler_3();
		Nullable_1_t3649900800  L_3 = (Nullable_1_t3649900800 )__this->get_startAfter_4();
		V_1 = (Nullable_1_t3649900800 )L_3;
		TimeSpan_t763862892  L_4 = Nullable_1_get_Value_m3338249190((Nullable_1_t3649900800 *)(&V_1), /*hidden argument*/Nullable_1_get_Value_m3338249190_MethodInfo_var);
		Il2CppObject* L_5 = ___observer0;
		Il2CppObject * L_6 = ___cancel1;
		StartObserver_t2543316435 * L_7 = (StartObserver_t2543316435 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		((  void (*) (StartObserver_t2543316435 *, StartObservable_1_t4083709414 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)(L_7, (StartObservable_1_t4083709414 *)__this, (Il2CppObject*)L_5, (Il2CppObject *)L_6, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		IntPtr_t L_8;
		L_8.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		Action_t437523947 * L_9 = (Action_t437523947 *)il2cpp_codegen_object_new(Action_t437523947_il2cpp_TypeInfo_var);
		Action__ctor_m2957240604(L_9, (Il2CppObject *)L_7, (IntPtr_t)L_8, /*hidden argument*/NULL);
		NullCheck((Il2CppObject *)L_2);
		Il2CppObject * L_10 = InterfaceFuncInvoker2< Il2CppObject *, TimeSpan_t763862892 , Action_t437523947 * >::Invoke(2 /* System.IDisposable UniRx.IScheduler::Schedule(System.TimeSpan,System.Action) */, IScheduler_t2938318244_il2cpp_TypeInfo_var, (Il2CppObject *)L_2, (TimeSpan_t763862892 )L_4, (Action_t437523947 *)L_9);
		return L_10;
	}

IL_0040:
	{
		Il2CppObject * L_11 = (Il2CppObject *)__this->get_scheduler_3();
		Il2CppObject* L_12 = ___observer0;
		Il2CppObject * L_13 = ___cancel1;
		StartObserver_t2543316435 * L_14 = (StartObserver_t2543316435 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		((  void (*) (StartObserver_t2543316435 *, StartObservable_1_t4083709414 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)(L_14, (StartObservable_1_t4083709414 *)__this, (Il2CppObject*)L_12, (Il2CppObject *)L_13, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		IntPtr_t L_15;
		L_15.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		Action_t437523947 * L_16 = (Action_t437523947 *)il2cpp_codegen_object_new(Action_t437523947_il2cpp_TypeInfo_var);
		Action__ctor_m2957240604(L_16, (Il2CppObject *)L_14, (IntPtr_t)L_15, /*hidden argument*/NULL);
		NullCheck((Il2CppObject *)L_11);
		Il2CppObject * L_17 = InterfaceFuncInvoker1< Il2CppObject *, Action_t437523947 * >::Invoke(1 /* System.IDisposable UniRx.IScheduler::Schedule(System.Action) */, IScheduler_t2938318244_il2cpp_TypeInfo_var, (Il2CppObject *)L_11, (Action_t437523947 *)L_16);
		return L_17;
	}
}
// System.Void UniRx.Operators.StartWithObservable`1/StartWith<System.Int32>::.ctor(UniRx.Operators.StartWithObservable`1<T>,UniRx.IObserver`1<T>,System.IDisposable)
extern "C"  void StartWith__ctor_m4118200041_gshared (StartWith_t1733935216 * __this, StartWithObservable_1_t338430729 * ___parent0, Il2CppObject* ___observer1, Il2CppObject * ___cancel2, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = ___observer1;
		Il2CppObject * L_1 = ___cancel2;
		NullCheck((OperatorObserverBase_2_t701568569 *)__this);
		((  void (*) (OperatorObserverBase_2_t701568569 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((OperatorObserverBase_2_t701568569 *)__this, (Il2CppObject*)L_0, (Il2CppObject *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		StartWithObservable_1_t338430729 * L_2 = ___parent0;
		__this->set_parent_2(L_2);
		return;
	}
}
// System.IDisposable UniRx.Operators.StartWithObservable`1/StartWith<System.Int32>::Run()
extern Il2CppClass* Exception_t1967233988_il2cpp_TypeInfo_var;
extern Il2CppClass* Disposable_t3736388786_il2cpp_TypeInfo_var;
extern const uint32_t StartWith_Run_m3126324458_MetadataUsageId;
extern "C"  Il2CppObject * StartWith_Run_m3126324458_gshared (StartWith_t1733935216 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (StartWith_Run_m3126324458_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	Exception_t1967233988 * V_1 = NULL;
	Il2CppObject * V_2 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		StartWithObservable_1_t338430729 * L_0 = (StartWithObservable_1_t338430729 *)__this->get_parent_2();
		NullCheck(L_0);
		Func_1_t3990196034 * L_1 = (Func_1_t3990196034 *)L_0->get_valueFactory_3();
		if (L_1)
		{
			goto IL_0021;
		}
	}
	{
		StartWithObservable_1_t338430729 * L_2 = (StartWithObservable_1_t338430729 *)__this->get_parent_2();
		NullCheck(L_2);
		int32_t L_3 = (int32_t)L_2->get_value_2();
		V_0 = (int32_t)L_3;
		goto IL_0062;
	}

IL_0021:
	try
	{ // begin try (depth: 1)
		StartWithObservable_1_t338430729 * L_4 = (StartWithObservable_1_t338430729 *)__this->get_parent_2();
		NullCheck(L_4);
		Func_1_t3990196034 * L_5 = (Func_1_t3990196034 *)L_4->get_valueFactory_3();
		NullCheck((Func_1_t3990196034 *)L_5);
		int32_t L_6 = ((  int32_t (*) (Func_1_t3990196034 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)((Func_1_t3990196034 *)L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		V_0 = (int32_t)L_6;
		goto IL_0062;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1967233988 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Exception_t1967233988_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_0037;
		throw e;
	}

CATCH_0037:
	{ // begin catch(System.Exception)
		{
			V_1 = (Exception_t1967233988 *)((Exception_t1967233988 *)__exception_local);
		}

IL_0038:
		try
		{ // begin try (depth: 2)
			Il2CppObject* L_7 = (Il2CppObject*)((OperatorObserverBase_2_t701568569 *)__this)->get_observer_0();
			il2cpp_codegen_memory_barrier();
			Exception_t1967233988 * L_8 = V_1;
			NullCheck((Il2CppObject*)L_7);
			InterfaceActionInvoker1< Exception_t1967233988 * >::Invoke(1 /* System.Void UniRx.IObserver`1<System.Int32>::OnError(System.Exception) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3), (Il2CppObject*)L_7, (Exception_t1967233988 *)L_8);
			IL2CPP_LEAVE(0x52, FINALLY_004b);
		} // end try (depth: 2)
		catch(Il2CppExceptionWrapper& e)
		{
			__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
			goto FINALLY_004b;
		}

FINALLY_004b:
		{ // begin finally (depth: 2)
			NullCheck((OperatorObserverBase_2_t701568569 *)__this);
			VirtActionInvoker0::Invoke(4 /* System.Void UniRx.Operators.OperatorObserverBase`2<System.Int32,System.Int32>::Dispose() */, (OperatorObserverBase_2_t701568569 *)__this);
			IL2CPP_END_FINALLY(75)
		} // end finally (depth: 2)
		IL2CPP_CLEANUP(75)
		{
			IL2CPP_JUMP_TBL(0x52, IL_0052)
			IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
		}

IL_0052:
		{
			IL2CPP_RUNTIME_CLASS_INIT(Disposable_t3736388786_il2cpp_TypeInfo_var);
			Il2CppObject * L_9 = ((Disposable_t3736388786_StaticFields*)Disposable_t3736388786_il2cpp_TypeInfo_var->static_fields)->get_Empty_0();
			V_2 = (Il2CppObject *)L_9;
			goto IL_0082;
		}

IL_005d:
		{
			; // IL_005d: leave IL_0062
		}
	} // end catch (depth: 1)

IL_0062:
	{
		int32_t L_10 = V_0;
		NullCheck((StartWith_t1733935216 *)__this);
		VirtActionInvoker1< int32_t >::Invoke(8 /* System.Void UniRx.Operators.StartWithObservable`1/StartWith<System.Int32>::OnNext(T) */, (StartWith_t1733935216 *)__this, (int32_t)L_10);
		StartWithObservable_1_t338430729 * L_11 = (StartWithObservable_1_t338430729 *)__this->get_parent_2();
		NullCheck(L_11);
		Il2CppObject* L_12 = (Il2CppObject*)L_11->get_source_1();
		Il2CppObject* L_13 = (Il2CppObject*)((OperatorObserverBase_2_t701568569 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		NullCheck((Il2CppObject*)L_12);
		Il2CppObject * L_14 = InterfaceFuncInvoker1< Il2CppObject *, Il2CppObject* >::Invoke(0 /* System.IDisposable UniRx.IObservable`1<System.Int32>::Subscribe(UniRx.IObserver`1<T>) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6), (Il2CppObject*)L_12, (Il2CppObject*)L_13);
		return L_14;
	}

IL_0082:
	{
		Il2CppObject * L_15 = V_2;
		return L_15;
	}
}
// System.Void UniRx.Operators.StartWithObservable`1/StartWith<System.Int32>::OnNext(T)
extern "C"  void StartWith_OnNext_m1897755310_gshared (StartWith_t1733935216 * __this, int32_t ___value0, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = (Il2CppObject*)((OperatorObserverBase_2_t701568569 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		int32_t L_1 = ___value0;
		NullCheck((Il2CppObject*)L_0);
		InterfaceActionInvoker1< int32_t >::Invoke(2 /* System.Void UniRx.IObserver`1<System.Int32>::OnNext(T) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3), (Il2CppObject*)L_0, (int32_t)L_1);
		return;
	}
}
// System.Void UniRx.Operators.StartWithObservable`1/StartWith<System.Int32>::OnError(System.Exception)
extern "C"  void StartWith_OnError_m4205998525_gshared (StartWith_t1733935216 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method)
{
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		Il2CppObject* L_0 = (Il2CppObject*)((OperatorObserverBase_2_t701568569 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		Exception_t1967233988 * L_1 = ___error0;
		NullCheck((Il2CppObject*)L_0);
		InterfaceActionInvoker1< Exception_t1967233988 * >::Invoke(1 /* System.Void UniRx.IObserver`1<System.Int32>::OnError(System.Exception) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3), (Il2CppObject*)L_0, (Exception_t1967233988 *)L_1);
		IL2CPP_LEAVE(0x1A, FINALLY_0013);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0013;
	}

FINALLY_0013:
	{ // begin finally (depth: 1)
		NullCheck((OperatorObserverBase_2_t701568569 *)__this);
		VirtActionInvoker0::Invoke(4 /* System.Void UniRx.Operators.OperatorObserverBase`2<System.Int32,System.Int32>::Dispose() */, (OperatorObserverBase_2_t701568569 *)__this);
		IL2CPP_END_FINALLY(19)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(19)
	{
		IL2CPP_JUMP_TBL(0x1A, IL_001a)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_001a:
	{
		return;
	}
}
// System.Void UniRx.Operators.StartWithObservable`1/StartWith<System.Int32>::OnCompleted()
extern "C"  void StartWith_OnCompleted_m1069029520_gshared (StartWith_t1733935216 * __this, const MethodInfo* method)
{
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		Il2CppObject* L_0 = (Il2CppObject*)((OperatorObserverBase_2_t701568569 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		NullCheck((Il2CppObject*)L_0);
		InterfaceActionInvoker0::Invoke(0 /* System.Void UniRx.IObserver`1<System.Int32>::OnCompleted() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3), (Il2CppObject*)L_0);
		IL2CPP_LEAVE(0x19, FINALLY_0012);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0012;
	}

FINALLY_0012:
	{ // begin finally (depth: 1)
		NullCheck((OperatorObserverBase_2_t701568569 *)__this);
		VirtActionInvoker0::Invoke(4 /* System.Void UniRx.Operators.OperatorObserverBase`2<System.Int32,System.Int32>::Dispose() */, (OperatorObserverBase_2_t701568569 *)__this);
		IL2CPP_END_FINALLY(18)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(18)
	{
		IL2CPP_JUMP_TBL(0x19, IL_0019)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0019:
	{
		return;
	}
}
// System.Void UniRx.Operators.StartWithObservable`1/StartWith<System.Object>::.ctor(UniRx.Operators.StartWithObservable`1<T>,UniRx.IObserver`1<T>,System.IDisposable)
extern "C"  void StartWith__ctor_m516095444_gshared (StartWith_t4018594145 * __this, StartWithObservable_1_t2623089658 * ___parent0, Il2CppObject* ___observer1, Il2CppObject * ___cancel2, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = ___observer1;
		Il2CppObject * L_1 = ___cancel2;
		NullCheck((OperatorObserverBase_2_t1187768149 *)__this);
		((  void (*) (OperatorObserverBase_2_t1187768149 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((OperatorObserverBase_2_t1187768149 *)__this, (Il2CppObject*)L_0, (Il2CppObject *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		StartWithObservable_1_t2623089658 * L_2 = ___parent0;
		__this->set_parent_2(L_2);
		return;
	}
}
// System.IDisposable UniRx.Operators.StartWithObservable`1/StartWith<System.Object>::Run()
extern Il2CppClass* Exception_t1967233988_il2cpp_TypeInfo_var;
extern Il2CppClass* Disposable_t3736388786_il2cpp_TypeInfo_var;
extern const uint32_t StartWith_Run_m4069786751_MetadataUsageId;
extern "C"  Il2CppObject * StartWith_Run_m4069786751_gshared (StartWith_t4018594145 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (StartWith_Run_m4069786751_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Il2CppObject * V_0 = NULL;
	Exception_t1967233988 * V_1 = NULL;
	Il2CppObject * V_2 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		StartWithObservable_1_t2623089658 * L_0 = (StartWithObservable_1_t2623089658 *)__this->get_parent_2();
		NullCheck(L_0);
		Func_1_t1979887667 * L_1 = (Func_1_t1979887667 *)L_0->get_valueFactory_3();
		if (L_1)
		{
			goto IL_0021;
		}
	}
	{
		StartWithObservable_1_t2623089658 * L_2 = (StartWithObservable_1_t2623089658 *)__this->get_parent_2();
		NullCheck(L_2);
		Il2CppObject * L_3 = (Il2CppObject *)L_2->get_value_2();
		V_0 = (Il2CppObject *)L_3;
		goto IL_0062;
	}

IL_0021:
	try
	{ // begin try (depth: 1)
		StartWithObservable_1_t2623089658 * L_4 = (StartWithObservable_1_t2623089658 *)__this->get_parent_2();
		NullCheck(L_4);
		Func_1_t1979887667 * L_5 = (Func_1_t1979887667 *)L_4->get_valueFactory_3();
		NullCheck((Func_1_t1979887667 *)L_5);
		Il2CppObject * L_6 = ((  Il2CppObject * (*) (Func_1_t1979887667 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)((Func_1_t1979887667 *)L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		V_0 = (Il2CppObject *)L_6;
		goto IL_0062;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1967233988 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Exception_t1967233988_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_0037;
		throw e;
	}

CATCH_0037:
	{ // begin catch(System.Exception)
		{
			V_1 = (Exception_t1967233988 *)((Exception_t1967233988 *)__exception_local);
		}

IL_0038:
		try
		{ // begin try (depth: 2)
			Il2CppObject* L_7 = (Il2CppObject*)((OperatorObserverBase_2_t1187768149 *)__this)->get_observer_0();
			il2cpp_codegen_memory_barrier();
			Exception_t1967233988 * L_8 = V_1;
			NullCheck((Il2CppObject*)L_7);
			InterfaceActionInvoker1< Exception_t1967233988 * >::Invoke(1 /* System.Void UniRx.IObserver`1<System.Object>::OnError(System.Exception) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3), (Il2CppObject*)L_7, (Exception_t1967233988 *)L_8);
			IL2CPP_LEAVE(0x52, FINALLY_004b);
		} // end try (depth: 2)
		catch(Il2CppExceptionWrapper& e)
		{
			__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
			goto FINALLY_004b;
		}

FINALLY_004b:
		{ // begin finally (depth: 2)
			NullCheck((OperatorObserverBase_2_t1187768149 *)__this);
			VirtActionInvoker0::Invoke(4 /* System.Void UniRx.Operators.OperatorObserverBase`2<System.Object,System.Object>::Dispose() */, (OperatorObserverBase_2_t1187768149 *)__this);
			IL2CPP_END_FINALLY(75)
		} // end finally (depth: 2)
		IL2CPP_CLEANUP(75)
		{
			IL2CPP_JUMP_TBL(0x52, IL_0052)
			IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
		}

IL_0052:
		{
			IL2CPP_RUNTIME_CLASS_INIT(Disposable_t3736388786_il2cpp_TypeInfo_var);
			Il2CppObject * L_9 = ((Disposable_t3736388786_StaticFields*)Disposable_t3736388786_il2cpp_TypeInfo_var->static_fields)->get_Empty_0();
			V_2 = (Il2CppObject *)L_9;
			goto IL_0082;
		}

IL_005d:
		{
			; // IL_005d: leave IL_0062
		}
	} // end catch (depth: 1)

IL_0062:
	{
		Il2CppObject * L_10 = V_0;
		NullCheck((StartWith_t4018594145 *)__this);
		VirtActionInvoker1< Il2CppObject * >::Invoke(8 /* System.Void UniRx.Operators.StartWithObservable`1/StartWith<System.Object>::OnNext(T) */, (StartWith_t4018594145 *)__this, (Il2CppObject *)L_10);
		StartWithObservable_1_t2623089658 * L_11 = (StartWithObservable_1_t2623089658 *)__this->get_parent_2();
		NullCheck(L_11);
		Il2CppObject* L_12 = (Il2CppObject*)L_11->get_source_1();
		Il2CppObject* L_13 = (Il2CppObject*)((OperatorObserverBase_2_t1187768149 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		NullCheck((Il2CppObject*)L_12);
		Il2CppObject * L_14 = InterfaceFuncInvoker1< Il2CppObject *, Il2CppObject* >::Invoke(0 /* System.IDisposable UniRx.IObservable`1<System.Object>::Subscribe(UniRx.IObserver`1<T>) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6), (Il2CppObject*)L_12, (Il2CppObject*)L_13);
		return L_14;
	}

IL_0082:
	{
		Il2CppObject * L_15 = V_2;
		return L_15;
	}
}
// System.Void UniRx.Operators.StartWithObservable`1/StartWith<System.Object>::OnNext(T)
extern "C"  void StartWith_OnNext_m169881817_gshared (StartWith_t4018594145 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = (Il2CppObject*)((OperatorObserverBase_2_t1187768149 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		Il2CppObject * L_1 = ___value0;
		NullCheck((Il2CppObject*)L_0);
		InterfaceActionInvoker1< Il2CppObject * >::Invoke(2 /* System.Void UniRx.IObserver`1<System.Object>::OnNext(T) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3), (Il2CppObject*)L_0, (Il2CppObject *)L_1);
		return;
	}
}
// System.Void UniRx.Operators.StartWithObservable`1/StartWith<System.Object>::OnError(System.Exception)
extern "C"  void StartWith_OnError_m4009851368_gshared (StartWith_t4018594145 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method)
{
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		Il2CppObject* L_0 = (Il2CppObject*)((OperatorObserverBase_2_t1187768149 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		Exception_t1967233988 * L_1 = ___error0;
		NullCheck((Il2CppObject*)L_0);
		InterfaceActionInvoker1< Exception_t1967233988 * >::Invoke(1 /* System.Void UniRx.IObserver`1<System.Object>::OnError(System.Exception) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3), (Il2CppObject*)L_0, (Exception_t1967233988 *)L_1);
		IL2CPP_LEAVE(0x1A, FINALLY_0013);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0013;
	}

FINALLY_0013:
	{ // begin finally (depth: 1)
		NullCheck((OperatorObserverBase_2_t1187768149 *)__this);
		VirtActionInvoker0::Invoke(4 /* System.Void UniRx.Operators.OperatorObserverBase`2<System.Object,System.Object>::Dispose() */, (OperatorObserverBase_2_t1187768149 *)__this);
		IL2CPP_END_FINALLY(19)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(19)
	{
		IL2CPP_JUMP_TBL(0x1A, IL_001a)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_001a:
	{
		return;
	}
}
// System.Void UniRx.Operators.StartWithObservable`1/StartWith<System.Object>::OnCompleted()
extern "C"  void StartWith_OnCompleted_m4287220027_gshared (StartWith_t4018594145 * __this, const MethodInfo* method)
{
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		Il2CppObject* L_0 = (Il2CppObject*)((OperatorObserverBase_2_t1187768149 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		NullCheck((Il2CppObject*)L_0);
		InterfaceActionInvoker0::Invoke(0 /* System.Void UniRx.IObserver`1<System.Object>::OnCompleted() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3), (Il2CppObject*)L_0);
		IL2CPP_LEAVE(0x19, FINALLY_0012);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0012;
	}

FINALLY_0012:
	{ // begin finally (depth: 1)
		NullCheck((OperatorObserverBase_2_t1187768149 *)__this);
		VirtActionInvoker0::Invoke(4 /* System.Void UniRx.Operators.OperatorObserverBase`2<System.Object,System.Object>::Dispose() */, (OperatorObserverBase_2_t1187768149 *)__this);
		IL2CPP_END_FINALLY(18)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(18)
	{
		IL2CPP_JUMP_TBL(0x19, IL_0019)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0019:
	{
		return;
	}
}
// System.Void UniRx.Operators.StartWithObservable`1<System.Int32>::.ctor(UniRx.IObservable`1<T>,T)
extern "C"  void StartWithObservable_1__ctor_m3411055606_gshared (StartWithObservable_1_t338430729 * __this, Il2CppObject* ___source0, int32_t ___value1, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = ___source0;
		bool L_1 = ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)(NULL /*static, unused*/, (Il2CppObject*)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		NullCheck((OperatorObservableBase_1_t1911559758 *)__this);
		((  void (*) (OperatorObservableBase_1_t1911559758 *, bool, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)((OperatorObservableBase_1_t1911559758 *)__this, (bool)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		Il2CppObject* L_2 = ___source0;
		__this->set_source_1(L_2);
		int32_t L_3 = ___value1;
		__this->set_value_2(L_3);
		return;
	}
}
// System.Void UniRx.Operators.StartWithObservable`1<System.Int32>::.ctor(UniRx.IObservable`1<T>,System.Func`1<T>)
extern "C"  void StartWithObservable_1__ctor_m24185270_gshared (StartWithObservable_1_t338430729 * __this, Il2CppObject* ___source0, Func_1_t3990196034 * ___valueFactory1, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = ___source0;
		bool L_1 = ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)(NULL /*static, unused*/, (Il2CppObject*)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		NullCheck((OperatorObservableBase_1_t1911559758 *)__this);
		((  void (*) (OperatorObservableBase_1_t1911559758 *, bool, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)((OperatorObservableBase_1_t1911559758 *)__this, (bool)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		Il2CppObject* L_2 = ___source0;
		__this->set_source_1(L_2);
		Func_1_t3990196034 * L_3 = ___valueFactory1;
		__this->set_valueFactory_3(L_3);
		return;
	}
}
// System.IDisposable UniRx.Operators.StartWithObservable`1<System.Int32>::SubscribeCore(UniRx.IObserver`1<T>,System.IDisposable)
extern "C"  Il2CppObject * StartWithObservable_1_SubscribeCore_m3383464361_gshared (StartWithObservable_1_t338430729 * __this, Il2CppObject* ___observer0, Il2CppObject * ___cancel1, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = ___observer0;
		Il2CppObject * L_1 = ___cancel1;
		StartWith_t1733935216 * L_2 = (StartWith_t1733935216 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		((  void (*) (StartWith_t1733935216 *, StartWithObservable_1_t338430729 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)(L_2, (StartWithObservable_1_t338430729 *)__this, (Il2CppObject*)L_0, (Il2CppObject *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		NullCheck((StartWith_t1733935216 *)L_2);
		Il2CppObject * L_3 = ((  Il2CppObject * (*) (StartWith_t1733935216 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->method)((StartWith_t1733935216 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		return L_3;
	}
}
// System.Void UniRx.Operators.StartWithObservable`1<System.Object>::.ctor(UniRx.IObservable`1<T>,T)
extern "C"  void StartWithObservable_1__ctor_m3233996175_gshared (StartWithObservable_1_t2623089658 * __this, Il2CppObject* ___source0, Il2CppObject * ___value1, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = ___source0;
		bool L_1 = ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)(NULL /*static, unused*/, (Il2CppObject*)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		NullCheck((OperatorObservableBase_1_t4196218687 *)__this);
		((  void (*) (OperatorObservableBase_1_t4196218687 *, bool, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)((OperatorObservableBase_1_t4196218687 *)__this, (bool)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		Il2CppObject* L_2 = ___source0;
		__this->set_source_1(L_2);
		Il2CppObject * L_3 = ___value1;
		__this->set_value_2(L_3);
		return;
	}
}
// System.Void UniRx.Operators.StartWithObservable`1<System.Object>::.ctor(UniRx.IObservable`1<T>,System.Func`1<T>)
extern "C"  void StartWithObservable_1__ctor_m2721135357_gshared (StartWithObservable_1_t2623089658 * __this, Il2CppObject* ___source0, Func_1_t1979887667 * ___valueFactory1, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = ___source0;
		bool L_1 = ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)(NULL /*static, unused*/, (Il2CppObject*)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		NullCheck((OperatorObservableBase_1_t4196218687 *)__this);
		((  void (*) (OperatorObservableBase_1_t4196218687 *, bool, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)((OperatorObservableBase_1_t4196218687 *)__this, (bool)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		Il2CppObject* L_2 = ___source0;
		__this->set_source_1(L_2);
		Func_1_t1979887667 * L_3 = ___valueFactory1;
		__this->set_valueFactory_3(L_3);
		return;
	}
}
// System.IDisposable UniRx.Operators.StartWithObservable`1<System.Object>::SubscribeCore(UniRx.IObserver`1<T>,System.IDisposable)
extern "C"  Il2CppObject * StartWithObservable_1_SubscribeCore_m3653749958_gshared (StartWithObservable_1_t2623089658 * __this, Il2CppObject* ___observer0, Il2CppObject * ___cancel1, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = ___observer0;
		Il2CppObject * L_1 = ___cancel1;
		StartWith_t4018594145 * L_2 = (StartWith_t4018594145 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		((  void (*) (StartWith_t4018594145 *, StartWithObservable_1_t2623089658 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)(L_2, (StartWithObservable_1_t2623089658 *)__this, (Il2CppObject*)L_0, (Il2CppObject *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		NullCheck((StartWith_t4018594145 *)L_2);
		Il2CppObject * L_3 = ((  Il2CppObject * (*) (StartWith_t4018594145 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->method)((StartWith_t4018594145 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		return L_3;
	}
}
// System.Void UniRx.Operators.SubscribeOnMainThreadObservable`1/<SubscribeCore>c__AnonStorey94<System.Object>::.ctor()
extern "C"  void U3CSubscribeCoreU3Ec__AnonStorey94__ctor_m3217542610_gshared (U3CSubscribeCoreU3Ec__AnonStorey94_t141655650 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UniRx.Operators.SubscribeOnMainThreadObservable`1/<SubscribeCore>c__AnonStorey94<System.Object>::<>m__C2(System.Int64)
extern "C"  void U3CSubscribeCoreU3Ec__AnonStorey94_U3CU3Em__C2_m1501286012_gshared (U3CSubscribeCoreU3Ec__AnonStorey94_t141655650 * __this, int64_t ____0, const MethodInfo* method)
{
	{
		SerialDisposable_t2547852742 * L_0 = (SerialDisposable_t2547852742 *)__this->get_d_1();
		SubscribeOnMainThreadObservable_1_t1377860062 * L_1 = (SubscribeOnMainThreadObservable_1_t1377860062 *)__this->get_U3CU3Ef__this_2();
		NullCheck(L_1);
		Il2CppObject* L_2 = (Il2CppObject*)L_1->get_source_1();
		Il2CppObject* L_3 = (Il2CppObject*)__this->get_observer_0();
		NullCheck((Il2CppObject*)L_2);
		Il2CppObject * L_4 = InterfaceFuncInvoker1< Il2CppObject *, Il2CppObject* >::Invoke(0 /* System.IDisposable UniRx.IObservable`1<System.Object>::Subscribe(UniRx.IObserver`1<T>) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0), (Il2CppObject*)L_2, (Il2CppObject*)L_3);
		NullCheck((SerialDisposable_t2547852742 *)L_0);
		SerialDisposable_set_Disposable_m1438046228((SerialDisposable_t2547852742 *)L_0, (Il2CppObject *)L_4, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UniRx.Operators.SubscribeOnMainThreadObservable`1<System.Object>::.ctor(UniRx.IObservable`1<T>,UniRx.IObservable`1<System.Int64>)
extern "C"  void SubscribeOnMainThreadObservable_1__ctor_m2208469230_gshared (SubscribeOnMainThreadObservable_1_t1377860062 * __this, Il2CppObject* ___source0, Il2CppObject* ___subscribeTrigger1, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = ___source0;
		bool L_1 = ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)(NULL /*static, unused*/, (Il2CppObject*)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		NullCheck((OperatorObservableBase_1_t4196218687 *)__this);
		((  void (*) (OperatorObservableBase_1_t4196218687 *, bool, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)((OperatorObservableBase_1_t4196218687 *)__this, (bool)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		Il2CppObject* L_2 = ___source0;
		__this->set_source_1(L_2);
		Il2CppObject* L_3 = ___subscribeTrigger1;
		__this->set_subscribeTrigger_2(L_3);
		return;
	}
}
// System.IDisposable UniRx.Operators.SubscribeOnMainThreadObservable`1<System.Object>::SubscribeCore(UniRx.IObserver`1<T>,System.IDisposable)
extern Il2CppClass* SingleAssignmentDisposable_t2336378823_il2cpp_TypeInfo_var;
extern Il2CppClass* SerialDisposable_t2547852742_il2cpp_TypeInfo_var;
extern Il2CppClass* Action_1_t2995867587_il2cpp_TypeInfo_var;
extern const MethodInfo* Action_1__ctor_m1589437765_MethodInfo_var;
extern const MethodInfo* ObservableExtensions_Subscribe_TisInt64_t2847414882_m1751770470_MethodInfo_var;
extern const uint32_t SubscribeOnMainThreadObservable_1_SubscribeCore_m561697474_MetadataUsageId;
extern "C"  Il2CppObject * SubscribeOnMainThreadObservable_1_SubscribeCore_m561697474_gshared (SubscribeOnMainThreadObservable_1_t1377860062 * __this, Il2CppObject* ___observer0, Il2CppObject * ___cancel1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SubscribeOnMainThreadObservable_1_SubscribeCore_m561697474_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	SingleAssignmentDisposable_t2336378823 * V_0 = NULL;
	U3CSubscribeCoreU3Ec__AnonStorey94_t141655650 * V_1 = NULL;
	{
		U3CSubscribeCoreU3Ec__AnonStorey94_t141655650 * L_0 = (U3CSubscribeCoreU3Ec__AnonStorey94_t141655650 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		((  void (*) (U3CSubscribeCoreU3Ec__AnonStorey94_t141655650 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		V_1 = (U3CSubscribeCoreU3Ec__AnonStorey94_t141655650 *)L_0;
		U3CSubscribeCoreU3Ec__AnonStorey94_t141655650 * L_1 = V_1;
		Il2CppObject* L_2 = ___observer0;
		NullCheck(L_1);
		L_1->set_observer_0(L_2);
		U3CSubscribeCoreU3Ec__AnonStorey94_t141655650 * L_3 = V_1;
		NullCheck(L_3);
		L_3->set_U3CU3Ef__this_2(__this);
		SingleAssignmentDisposable_t2336378823 * L_4 = (SingleAssignmentDisposable_t2336378823 *)il2cpp_codegen_object_new(SingleAssignmentDisposable_t2336378823_il2cpp_TypeInfo_var);
		SingleAssignmentDisposable__ctor_m917449122(L_4, /*hidden argument*/NULL);
		V_0 = (SingleAssignmentDisposable_t2336378823 *)L_4;
		U3CSubscribeCoreU3Ec__AnonStorey94_t141655650 * L_5 = V_1;
		SerialDisposable_t2547852742 * L_6 = (SerialDisposable_t2547852742 *)il2cpp_codegen_object_new(SerialDisposable_t2547852742_il2cpp_TypeInfo_var);
		SerialDisposable__ctor_m2977852291(L_6, /*hidden argument*/NULL);
		NullCheck(L_5);
		L_5->set_d_1(L_6);
		U3CSubscribeCoreU3Ec__AnonStorey94_t141655650 * L_7 = V_1;
		NullCheck(L_7);
		SerialDisposable_t2547852742 * L_8 = (SerialDisposable_t2547852742 *)L_7->get_d_1();
		SingleAssignmentDisposable_t2336378823 * L_9 = V_0;
		NullCheck((SerialDisposable_t2547852742 *)L_8);
		SerialDisposable_set_Disposable_m1438046228((SerialDisposable_t2547852742 *)L_8, (Il2CppObject *)L_9, /*hidden argument*/NULL);
		SingleAssignmentDisposable_t2336378823 * L_10 = V_0;
		Il2CppObject* L_11 = (Il2CppObject*)__this->get_subscribeTrigger_2();
		U3CSubscribeCoreU3Ec__AnonStorey94_t141655650 * L_12 = V_1;
		IntPtr_t L_13;
		L_13.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		Action_1_t2995867587 * L_14 = (Action_1_t2995867587 *)il2cpp_codegen_object_new(Action_1_t2995867587_il2cpp_TypeInfo_var);
		Action_1__ctor_m1589437765(L_14, (Il2CppObject *)L_12, (IntPtr_t)L_13, /*hidden argument*/Action_1__ctor_m1589437765_MethodInfo_var);
		Il2CppObject * L_15 = ObservableExtensions_Subscribe_TisInt64_t2847414882_m1751770470(NULL /*static, unused*/, (Il2CppObject*)L_11, (Action_1_t2995867587 *)L_14, /*hidden argument*/ObservableExtensions_Subscribe_TisInt64_t2847414882_m1751770470_MethodInfo_var);
		NullCheck((SingleAssignmentDisposable_t2336378823 *)L_10);
		SingleAssignmentDisposable_set_Disposable_m1599028885((SingleAssignmentDisposable_t2336378823 *)L_10, (Il2CppObject *)L_15, /*hidden argument*/NULL);
		U3CSubscribeCoreU3Ec__AnonStorey94_t141655650 * L_16 = V_1;
		NullCheck(L_16);
		SerialDisposable_t2547852742 * L_17 = (SerialDisposable_t2547852742 *)L_16->get_d_1();
		return L_17;
	}
}
// System.Void UniRx.Operators.SubscribeOnObservable`1/<SubscribeCore>c__AnonStorey69<System.Object>::.ctor()
extern "C"  void U3CSubscribeCoreU3Ec__AnonStorey69__ctor_m2641813533_gshared (U3CSubscribeCoreU3Ec__AnonStorey69_t3806984138 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UniRx.Operators.SubscribeOnObservable`1/<SubscribeCore>c__AnonStorey69<System.Object>::<>m__87()
extern Il2CppClass* ScheduledDisposable_t2058426623_il2cpp_TypeInfo_var;
extern const uint32_t U3CSubscribeCoreU3Ec__AnonStorey69_U3CU3Em__87_m2765388517_MetadataUsageId;
extern "C"  void U3CSubscribeCoreU3Ec__AnonStorey69_U3CU3Em__87_m2765388517_gshared (U3CSubscribeCoreU3Ec__AnonStorey69_t3806984138 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CSubscribeCoreU3Ec__AnonStorey69_U3CU3Em__87_m2765388517_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		SerialDisposable_t2547852742 * L_0 = (SerialDisposable_t2547852742 *)__this->get_d_1();
		SubscribeOnObservable_1_t2329329235 * L_1 = (SubscribeOnObservable_1_t2329329235 *)__this->get_U3CU3Ef__this_2();
		NullCheck(L_1);
		Il2CppObject * L_2 = (Il2CppObject *)L_1->get_scheduler_2();
		SubscribeOnObservable_1_t2329329235 * L_3 = (SubscribeOnObservable_1_t2329329235 *)__this->get_U3CU3Ef__this_2();
		NullCheck(L_3);
		Il2CppObject* L_4 = (Il2CppObject*)L_3->get_source_1();
		Il2CppObject* L_5 = (Il2CppObject*)__this->get_observer_0();
		NullCheck((Il2CppObject*)L_4);
		Il2CppObject * L_6 = InterfaceFuncInvoker1< Il2CppObject *, Il2CppObject* >::Invoke(0 /* System.IDisposable UniRx.IObservable`1<System.Object>::Subscribe(UniRx.IObserver`1<T>) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0), (Il2CppObject*)L_4, (Il2CppObject*)L_5);
		ScheduledDisposable_t2058426623 * L_7 = (ScheduledDisposable_t2058426623 *)il2cpp_codegen_object_new(ScheduledDisposable_t2058426623_il2cpp_TypeInfo_var);
		ScheduledDisposable__ctor_m2368627524(L_7, (Il2CppObject *)L_2, (Il2CppObject *)L_6, /*hidden argument*/NULL);
		NullCheck((SerialDisposable_t2547852742 *)L_0);
		SerialDisposable_set_Disposable_m1438046228((SerialDisposable_t2547852742 *)L_0, (Il2CppObject *)L_7, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UniRx.Operators.SubscribeOnObservable`1<System.Object>::.ctor(UniRx.IObservable`1<T>,UniRx.IScheduler)
extern Il2CppClass* Scheduler_t103934925_il2cpp_TypeInfo_var;
extern const uint32_t SubscribeOnObservable_1__ctor_m2955604196_MetadataUsageId;
extern "C"  void SubscribeOnObservable_1__ctor_m2955604196_gshared (SubscribeOnObservable_1_t2329329235 * __this, Il2CppObject* ___source0, Il2CppObject * ___scheduler1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SubscribeOnObservable_1__ctor_m2955604196_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	SubscribeOnObservable_1_t2329329235 * G_B2_0 = NULL;
	SubscribeOnObservable_1_t2329329235 * G_B1_0 = NULL;
	int32_t G_B3_0 = 0;
	SubscribeOnObservable_1_t2329329235 * G_B3_1 = NULL;
	{
		Il2CppObject * L_0 = ___scheduler1;
		IL2CPP_RUNTIME_CLASS_INIT(Scheduler_t103934925_il2cpp_TypeInfo_var);
		Il2CppObject * L_1 = ((Scheduler_t103934925_StaticFields*)Scheduler_t103934925_il2cpp_TypeInfo_var->static_fields)->get_CurrentThread_0();
		G_B1_0 = ((SubscribeOnObservable_1_t2329329235 *)(__this));
		if ((((Il2CppObject*)(Il2CppObject *)L_0) == ((Il2CppObject*)(Il2CppObject *)L_1)))
		{
			G_B2_0 = ((SubscribeOnObservable_1_t2329329235 *)(__this));
			goto IL_0014;
		}
	}
	{
		Il2CppObject* L_2 = ___source0;
		bool L_3 = ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)(NULL /*static, unused*/, (Il2CppObject*)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		G_B3_0 = ((int32_t)(L_3));
		G_B3_1 = ((SubscribeOnObservable_1_t2329329235 *)(G_B1_0));
		goto IL_0015;
	}

IL_0014:
	{
		G_B3_0 = 1;
		G_B3_1 = ((SubscribeOnObservable_1_t2329329235 *)(G_B2_0));
	}

IL_0015:
	{
		NullCheck((OperatorObservableBase_1_t4196218687 *)G_B3_1);
		((  void (*) (OperatorObservableBase_1_t4196218687 *, bool, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)((OperatorObservableBase_1_t4196218687 *)G_B3_1, (bool)G_B3_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		Il2CppObject* L_4 = ___source0;
		__this->set_source_1(L_4);
		Il2CppObject * L_5 = ___scheduler1;
		__this->set_scheduler_2(L_5);
		return;
	}
}
// System.IDisposable UniRx.Operators.SubscribeOnObservable`1<System.Object>::SubscribeCore(UniRx.IObserver`1<T>,System.IDisposable)
extern Il2CppClass* SingleAssignmentDisposable_t2336378823_il2cpp_TypeInfo_var;
extern Il2CppClass* SerialDisposable_t2547852742_il2cpp_TypeInfo_var;
extern Il2CppClass* Action_t437523947_il2cpp_TypeInfo_var;
extern Il2CppClass* IScheduler_t2938318244_il2cpp_TypeInfo_var;
extern const uint32_t SubscribeOnObservable_1_SubscribeCore_m271267109_MetadataUsageId;
extern "C"  Il2CppObject * SubscribeOnObservable_1_SubscribeCore_m271267109_gshared (SubscribeOnObservable_1_t2329329235 * __this, Il2CppObject* ___observer0, Il2CppObject * ___cancel1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SubscribeOnObservable_1_SubscribeCore_m271267109_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	SingleAssignmentDisposable_t2336378823 * V_0 = NULL;
	U3CSubscribeCoreU3Ec__AnonStorey69_t3806984138 * V_1 = NULL;
	{
		U3CSubscribeCoreU3Ec__AnonStorey69_t3806984138 * L_0 = (U3CSubscribeCoreU3Ec__AnonStorey69_t3806984138 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		((  void (*) (U3CSubscribeCoreU3Ec__AnonStorey69_t3806984138 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		V_1 = (U3CSubscribeCoreU3Ec__AnonStorey69_t3806984138 *)L_0;
		U3CSubscribeCoreU3Ec__AnonStorey69_t3806984138 * L_1 = V_1;
		Il2CppObject* L_2 = ___observer0;
		NullCheck(L_1);
		L_1->set_observer_0(L_2);
		U3CSubscribeCoreU3Ec__AnonStorey69_t3806984138 * L_3 = V_1;
		NullCheck(L_3);
		L_3->set_U3CU3Ef__this_2(__this);
		SingleAssignmentDisposable_t2336378823 * L_4 = (SingleAssignmentDisposable_t2336378823 *)il2cpp_codegen_object_new(SingleAssignmentDisposable_t2336378823_il2cpp_TypeInfo_var);
		SingleAssignmentDisposable__ctor_m917449122(L_4, /*hidden argument*/NULL);
		V_0 = (SingleAssignmentDisposable_t2336378823 *)L_4;
		U3CSubscribeCoreU3Ec__AnonStorey69_t3806984138 * L_5 = V_1;
		SerialDisposable_t2547852742 * L_6 = (SerialDisposable_t2547852742 *)il2cpp_codegen_object_new(SerialDisposable_t2547852742_il2cpp_TypeInfo_var);
		SerialDisposable__ctor_m2977852291(L_6, /*hidden argument*/NULL);
		NullCheck(L_5);
		L_5->set_d_1(L_6);
		U3CSubscribeCoreU3Ec__AnonStorey69_t3806984138 * L_7 = V_1;
		NullCheck(L_7);
		SerialDisposable_t2547852742 * L_8 = (SerialDisposable_t2547852742 *)L_7->get_d_1();
		SingleAssignmentDisposable_t2336378823 * L_9 = V_0;
		NullCheck((SerialDisposable_t2547852742 *)L_8);
		SerialDisposable_set_Disposable_m1438046228((SerialDisposable_t2547852742 *)L_8, (Il2CppObject *)L_9, /*hidden argument*/NULL);
		SingleAssignmentDisposable_t2336378823 * L_10 = V_0;
		Il2CppObject * L_11 = (Il2CppObject *)__this->get_scheduler_2();
		U3CSubscribeCoreU3Ec__AnonStorey69_t3806984138 * L_12 = V_1;
		IntPtr_t L_13;
		L_13.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		Action_t437523947 * L_14 = (Action_t437523947 *)il2cpp_codegen_object_new(Action_t437523947_il2cpp_TypeInfo_var);
		Action__ctor_m2957240604(L_14, (Il2CppObject *)L_12, (IntPtr_t)L_13, /*hidden argument*/NULL);
		NullCheck((Il2CppObject *)L_11);
		Il2CppObject * L_15 = InterfaceFuncInvoker1< Il2CppObject *, Action_t437523947 * >::Invoke(1 /* System.IDisposable UniRx.IScheduler::Schedule(System.Action) */, IScheduler_t2938318244_il2cpp_TypeInfo_var, (Il2CppObject *)L_11, (Action_t437523947 *)L_14);
		NullCheck((SingleAssignmentDisposable_t2336378823 *)L_10);
		SingleAssignmentDisposable_set_Disposable_m1599028885((SingleAssignmentDisposable_t2336378823 *)L_10, (Il2CppObject *)L_15, /*hidden argument*/NULL);
		U3CSubscribeCoreU3Ec__AnonStorey69_t3806984138 * L_16 = V_1;
		NullCheck(L_16);
		SerialDisposable_t2547852742 * L_17 = (SerialDisposable_t2547852742 *)L_16->get_d_1();
		return L_17;
	}
}
// System.Void UniRx.Operators.SwitchObservable`1/SwitchObserver/Switch<System.Object>::.ctor(UniRx.Operators.SwitchObservable`1/SwitchObserver<T>,System.UInt64)
extern "C"  void Switch__ctor_m1572081325_gshared (Switch_t874284973 * __this, SwitchObserver_t321759539 * ___observer0, uint64_t ___id1, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		SwitchObserver_t321759539 * L_0 = ___observer0;
		__this->set_parent_0(L_0);
		uint64_t L_1 = ___id1;
		__this->set_id_1(L_1);
		return;
	}
}
// System.Void UniRx.Operators.SwitchObservable`1/SwitchObserver/Switch<System.Object>::OnNext(T)
extern "C"  void Switch_OnNext_m237795148_gshared (Switch_t874284973 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	Il2CppObject * V_0 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		SwitchObserver_t321759539 * L_0 = (SwitchObserver_t321759539 *)__this->get_parent_0();
		NullCheck(L_0);
		Il2CppObject * L_1 = (Il2CppObject *)L_0->get_gate_3();
		V_0 = (Il2CppObject *)L_1;
		Il2CppObject * L_2 = V_0;
		Monitor_Enter_m476686225(NULL /*static, unused*/, (Il2CppObject *)L_2, /*hidden argument*/NULL);
	}

IL_0012:
	try
	{ // begin try (depth: 1)
		{
			SwitchObserver_t321759539 * L_3 = (SwitchObserver_t321759539 *)__this->get_parent_0();
			NullCheck(L_3);
			uint64_t L_4 = (uint64_t)L_3->get_latest_6();
			uint64_t L_5 = (uint64_t)__this->get_id_1();
			if ((!(((uint64_t)L_4) == ((uint64_t)L_5))))
			{
				goto IL_003b;
			}
		}

IL_0028:
		{
			SwitchObserver_t321759539 * L_6 = (SwitchObserver_t321759539 *)__this->get_parent_0();
			NullCheck(L_6);
			Il2CppObject* L_7 = (Il2CppObject*)((OperatorObserverBase_2_t3381670217 *)L_6)->get_observer_0();
			il2cpp_codegen_memory_barrier();
			Il2CppObject * L_8 = ___value0;
			NullCheck((Il2CppObject*)L_7);
			InterfaceActionInvoker1< Il2CppObject * >::Invoke(2 /* System.Void UniRx.IObserver`1<System.Object>::OnNext(T) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0), (Il2CppObject*)L_7, (Il2CppObject *)L_8);
		}

IL_003b:
		{
			IL2CPP_LEAVE(0x47, FINALLY_0040);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0040;
	}

FINALLY_0040:
	{ // begin finally (depth: 1)
		Il2CppObject * L_9 = V_0;
		Monitor_Exit_m2088237919(NULL /*static, unused*/, (Il2CppObject *)L_9, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(64)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(64)
	{
		IL2CPP_JUMP_TBL(0x47, IL_0047)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0047:
	{
		return;
	}
}
// System.Void UniRx.Operators.SwitchObservable`1/SwitchObserver/Switch<System.Object>::OnError(System.Exception)
extern "C"  void Switch_OnError_m168852059_gshared (Switch_t874284973 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method)
{
	Il2CppObject * V_0 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		SwitchObserver_t321759539 * L_0 = (SwitchObserver_t321759539 *)__this->get_parent_0();
		NullCheck(L_0);
		Il2CppObject * L_1 = (Il2CppObject *)L_0->get_gate_3();
		V_0 = (Il2CppObject *)L_1;
		Il2CppObject * L_2 = V_0;
		Monitor_Enter_m476686225(NULL /*static, unused*/, (Il2CppObject *)L_2, /*hidden argument*/NULL);
	}

IL_0012:
	try
	{ // begin try (depth: 1)
		{
			SwitchObserver_t321759539 * L_3 = (SwitchObserver_t321759539 *)__this->get_parent_0();
			NullCheck(L_3);
			uint64_t L_4 = (uint64_t)L_3->get_latest_6();
			uint64_t L_5 = (uint64_t)__this->get_id_1();
			if ((!(((uint64_t)L_4) == ((uint64_t)L_5))))
			{
				goto IL_003b;
			}
		}

IL_0028:
		{
			SwitchObserver_t321759539 * L_6 = (SwitchObserver_t321759539 *)__this->get_parent_0();
			NullCheck(L_6);
			Il2CppObject* L_7 = (Il2CppObject*)((OperatorObserverBase_2_t3381670217 *)L_6)->get_observer_0();
			il2cpp_codegen_memory_barrier();
			Exception_t1967233988 * L_8 = ___error0;
			NullCheck((Il2CppObject*)L_7);
			InterfaceActionInvoker1< Exception_t1967233988 * >::Invoke(1 /* System.Void UniRx.IObserver`1<System.Object>::OnError(System.Exception) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0), (Il2CppObject*)L_7, (Exception_t1967233988 *)L_8);
		}

IL_003b:
		{
			IL2CPP_LEAVE(0x47, FINALLY_0040);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0040;
	}

FINALLY_0040:
	{ // begin finally (depth: 1)
		Il2CppObject * L_9 = V_0;
		Monitor_Exit_m2088237919(NULL /*static, unused*/, (Il2CppObject *)L_9, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(64)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(64)
	{
		IL2CPP_JUMP_TBL(0x47, IL_0047)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0047:
	{
		return;
	}
}
// System.Void UniRx.Operators.SwitchObservable`1/SwitchObserver/Switch<System.Object>::OnCompleted()
extern "C"  void Switch_OnCompleted_m4267154990_gshared (Switch_t874284973 * __this, const MethodInfo* method)
{
	Il2CppObject * V_0 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		SwitchObserver_t321759539 * L_0 = (SwitchObserver_t321759539 *)__this->get_parent_0();
		NullCheck(L_0);
		Il2CppObject * L_1 = (Il2CppObject *)L_0->get_gate_3();
		V_0 = (Il2CppObject *)L_1;
		Il2CppObject * L_2 = V_0;
		Monitor_Enter_m476686225(NULL /*static, unused*/, (Il2CppObject *)L_2, /*hidden argument*/NULL);
	}

IL_0012:
	try
	{ // begin try (depth: 1)
		{
			SwitchObserver_t321759539 * L_3 = (SwitchObserver_t321759539 *)__this->get_parent_0();
			NullCheck(L_3);
			uint64_t L_4 = (uint64_t)L_3->get_latest_6();
			uint64_t L_5 = (uint64_t)__this->get_id_1();
			if ((!(((uint64_t)L_4) == ((uint64_t)L_5))))
			{
				goto IL_0056;
			}
		}

IL_0028:
		{
			SwitchObserver_t321759539 * L_6 = (SwitchObserver_t321759539 *)__this->get_parent_0();
			NullCheck(L_6);
			L_6->set_hasLatest_7((bool)0);
			SwitchObserver_t321759539 * L_7 = (SwitchObserver_t321759539 *)__this->get_parent_0();
			NullCheck(L_7);
			bool L_8 = (bool)L_7->get_isStopped_5();
			if (!L_8)
			{
				goto IL_0056;
			}
		}

IL_0044:
		{
			SwitchObserver_t321759539 * L_9 = (SwitchObserver_t321759539 *)__this->get_parent_0();
			NullCheck(L_9);
			Il2CppObject* L_10 = (Il2CppObject*)((OperatorObserverBase_2_t3381670217 *)L_9)->get_observer_0();
			il2cpp_codegen_memory_barrier();
			NullCheck((Il2CppObject*)L_10);
			InterfaceActionInvoker0::Invoke(0 /* System.Void UniRx.IObserver`1<System.Object>::OnCompleted() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0), (Il2CppObject*)L_10);
		}

IL_0056:
		{
			IL2CPP_LEAVE(0x62, FINALLY_005b);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_005b;
	}

FINALLY_005b:
	{ // begin finally (depth: 1)
		Il2CppObject * L_11 = V_0;
		Monitor_Exit_m2088237919(NULL /*static, unused*/, (Il2CppObject *)L_11, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(91)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(91)
	{
		IL2CPP_JUMP_TBL(0x62, IL_0062)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0062:
	{
		return;
	}
}
// System.Void UniRx.Operators.SwitchObservable`1/SwitchObserver<System.Object>::.ctor(UniRx.Operators.SwitchObservable`1<T>,UniRx.IObserver`1<T>,System.IDisposable)
extern Il2CppClass* Il2CppObject_il2cpp_TypeInfo_var;
extern Il2CppClass* SerialDisposable_t2547852742_il2cpp_TypeInfo_var;
extern const uint32_t SwitchObserver__ctor_m3135145794_MetadataUsageId;
extern "C"  void SwitchObserver__ctor_m3135145794_gshared (SwitchObserver_t321759539 * __this, SwitchObservable_1_t1854652486 * ___parent0, Il2CppObject* ___observer1, Il2CppObject * ___cancel2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SwitchObserver__ctor_m3135145794_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject * L_0 = (Il2CppObject *)il2cpp_codegen_object_new(Il2CppObject_il2cpp_TypeInfo_var);
		Object__ctor_m1772956182(L_0, /*hidden argument*/NULL);
		__this->set_gate_3(L_0);
		SerialDisposable_t2547852742 * L_1 = (SerialDisposable_t2547852742 *)il2cpp_codegen_object_new(SerialDisposable_t2547852742_il2cpp_TypeInfo_var);
		SerialDisposable__ctor_m2977852291(L_1, /*hidden argument*/NULL);
		__this->set_innerSubscription_4(L_1);
		Il2CppObject* L_2 = ___observer1;
		Il2CppObject * L_3 = ___cancel2;
		NullCheck((OperatorObserverBase_2_t3381670217 *)__this);
		((  void (*) (OperatorObserverBase_2_t3381670217 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((OperatorObserverBase_2_t3381670217 *)__this, (Il2CppObject*)L_2, (Il2CppObject *)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		SwitchObservable_1_t1854652486 * L_4 = ___parent0;
		__this->set_parent_2(L_4);
		return;
	}
}
// System.IDisposable UniRx.Operators.SwitchObservable`1/SwitchObserver<System.Object>::Run()
extern "C"  Il2CppObject * SwitchObserver_Run_m3168455675_gshared (SwitchObserver_t321759539 * __this, const MethodInfo* method)
{
	Il2CppObject * V_0 = NULL;
	{
		SwitchObservable_1_t1854652486 * L_0 = (SwitchObservable_1_t1854652486 *)__this->get_parent_2();
		NullCheck(L_0);
		Il2CppObject* L_1 = (Il2CppObject*)L_0->get_sources_1();
		NullCheck((Il2CppObject*)L_1);
		Il2CppObject * L_2 = InterfaceFuncInvoker1< Il2CppObject *, Il2CppObject* >::Invoke(0 /* System.IDisposable UniRx.IObservable`1<UniRx.IObservable`1<System.Object>>::Subscribe(UniRx.IObserver`1<T>) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), (Il2CppObject*)L_1, (Il2CppObject*)__this);
		V_0 = (Il2CppObject *)L_2;
		Il2CppObject * L_3 = V_0;
		SerialDisposable_t2547852742 * L_4 = (SerialDisposable_t2547852742 *)__this->get_innerSubscription_4();
		Il2CppObject * L_5 = StableCompositeDisposable_Create_m3764788535(NULL /*static, unused*/, (Il2CppObject *)L_3, (Il2CppObject *)L_4, /*hidden argument*/NULL);
		return L_5;
	}
}
// System.Void UniRx.Operators.SwitchObservable`1/SwitchObserver<System.Object>::OnNext(UniRx.IObservable`1<T>)
extern Il2CppClass* SingleAssignmentDisposable_t2336378823_il2cpp_TypeInfo_var;
extern const uint32_t SwitchObserver_OnNext_m2371630088_MetadataUsageId;
extern "C"  void SwitchObserver_OnNext_m2371630088_gshared (SwitchObserver_t321759539 * __this, Il2CppObject* ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SwitchObserver_OnNext_m2371630088_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	uint64_t V_0 = 0;
	Il2CppObject * V_1 = NULL;
	SingleAssignmentDisposable_t2336378823 * V_2 = NULL;
	uint64_t V_3 = 0;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		V_0 = (uint64_t)(((int64_t)((int64_t)0)));
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_gate_3();
		V_1 = (Il2CppObject *)L_0;
		Il2CppObject * L_1 = V_1;
		Monitor_Enter_m476686225(NULL /*static, unused*/, (Il2CppObject *)L_1, /*hidden argument*/NULL);
	}

IL_0010:
	try
	{ // begin try (depth: 1)
		uint64_t L_2 = (uint64_t)__this->get_latest_6();
		int64_t L_3 = (int64_t)((int64_t)((int64_t)L_2+(int64_t)(((int64_t)((int64_t)1)))));
		V_3 = (uint64_t)L_3;
		__this->set_latest_6(L_3);
		uint64_t L_4 = V_3;
		V_0 = (uint64_t)L_4;
		__this->set_hasLatest_7((bool)1);
		IL2CPP_LEAVE(0x36, FINALLY_002f);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_002f;
	}

FINALLY_002f:
	{ // begin finally (depth: 1)
		Il2CppObject * L_5 = V_1;
		Monitor_Exit_m2088237919(NULL /*static, unused*/, (Il2CppObject *)L_5, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(47)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(47)
	{
		IL2CPP_JUMP_TBL(0x36, IL_0036)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0036:
	{
		SingleAssignmentDisposable_t2336378823 * L_6 = (SingleAssignmentDisposable_t2336378823 *)il2cpp_codegen_object_new(SingleAssignmentDisposable_t2336378823_il2cpp_TypeInfo_var);
		SingleAssignmentDisposable__ctor_m917449122(L_6, /*hidden argument*/NULL);
		V_2 = (SingleAssignmentDisposable_t2336378823 *)L_6;
		SerialDisposable_t2547852742 * L_7 = (SerialDisposable_t2547852742 *)__this->get_innerSubscription_4();
		SingleAssignmentDisposable_t2336378823 * L_8 = V_2;
		NullCheck((SerialDisposable_t2547852742 *)L_7);
		SerialDisposable_set_Disposable_m1438046228((SerialDisposable_t2547852742 *)L_7, (Il2CppObject *)L_8, /*hidden argument*/NULL);
		SingleAssignmentDisposable_t2336378823 * L_9 = V_2;
		Il2CppObject* L_10 = ___value0;
		uint64_t L_11 = V_0;
		Switch_t874284973 * L_12 = (Switch_t874284973 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		((  void (*) (Switch_t874284973 *, SwitchObserver_t321759539 *, uint64_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)(L_12, (SwitchObserver_t321759539 *)__this, (uint64_t)L_11, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		NullCheck((Il2CppObject*)L_10);
		Il2CppObject * L_13 = InterfaceFuncInvoker1< Il2CppObject *, Il2CppObject* >::Invoke(0 /* System.IDisposable UniRx.IObservable`1<System.Object>::Subscribe(UniRx.IObserver`1<T>) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5), (Il2CppObject*)L_10, (Il2CppObject*)L_12);
		NullCheck((SingleAssignmentDisposable_t2336378823 *)L_9);
		SingleAssignmentDisposable_set_Disposable_m1599028885((SingleAssignmentDisposable_t2336378823 *)L_9, (Il2CppObject *)L_13, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UniRx.Operators.SwitchObservable`1/SwitchObserver<System.Object>::OnError(System.Exception)
extern "C"  void SwitchObserver_OnError_m4055392420_gshared (SwitchObserver_t321759539 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method)
{
	Il2CppObject * V_0 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_gate_3();
		V_0 = (Il2CppObject *)L_0;
		Il2CppObject * L_1 = V_0;
		Monitor_Enter_m476686225(NULL /*static, unused*/, (Il2CppObject *)L_1, /*hidden argument*/NULL);
	}

IL_000d:
	try
	{ // begin try (depth: 1)
		try
		{ // begin try (depth: 2)
			Il2CppObject* L_2 = (Il2CppObject*)((OperatorObserverBase_2_t3381670217 *)__this)->get_observer_0();
			il2cpp_codegen_memory_barrier();
			Exception_t1967233988 * L_3 = ___error0;
			NullCheck((Il2CppObject*)L_2);
			InterfaceActionInvoker1< Exception_t1967233988 * >::Invoke(1 /* System.Void UniRx.IObserver`1<System.Object>::OnError(System.Exception) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6), (Il2CppObject*)L_2, (Exception_t1967233988 *)L_3);
			IL2CPP_LEAVE(0x27, FINALLY_0020);
		} // end try (depth: 2)
		catch(Il2CppExceptionWrapper& e)
		{
			__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
			goto FINALLY_0020;
		}

FINALLY_0020:
		{ // begin finally (depth: 2)
			NullCheck((OperatorObserverBase_2_t3381670217 *)__this);
			VirtActionInvoker0::Invoke(4 /* System.Void UniRx.Operators.OperatorObserverBase`2<UniRx.IObservable`1<System.Object>,System.Object>::Dispose() */, (OperatorObserverBase_2_t3381670217 *)__this);
			IL2CPP_END_FINALLY(32)
		} // end finally (depth: 2)
		IL2CPP_CLEANUP(32)
		{
			IL2CPP_JUMP_TBL(0x27, IL_0027)
			IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
		}

IL_0027:
		{
			IL2CPP_LEAVE(0x33, FINALLY_002c);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_002c;
	}

FINALLY_002c:
	{ // begin finally (depth: 1)
		Il2CppObject * L_4 = V_0;
		Monitor_Exit_m2088237919(NULL /*static, unused*/, (Il2CppObject *)L_4, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(44)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(44)
	{
		IL2CPP_JUMP_TBL(0x33, IL_0033)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0033:
	{
		return;
	}
}
// System.Void UniRx.Operators.SwitchObservable`1/SwitchObserver<System.Object>::OnCompleted()
extern "C"  void SwitchObserver_OnCompleted_m238545399_gshared (SwitchObserver_t321759539 * __this, const MethodInfo* method)
{
	Il2CppObject * V_0 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_gate_3();
		V_0 = (Il2CppObject *)L_0;
		Il2CppObject * L_1 = V_0;
		Monitor_Enter_m476686225(NULL /*static, unused*/, (Il2CppObject *)L_1, /*hidden argument*/NULL);
	}

IL_000d:
	try
	{ // begin try (depth: 1)
		{
			__this->set_isStopped_5((bool)1);
			bool L_2 = (bool)__this->get_hasLatest_7();
			if (L_2)
			{
				goto IL_0038;
			}
		}

IL_001f:
		try
		{ // begin try (depth: 2)
			Il2CppObject* L_3 = (Il2CppObject*)((OperatorObserverBase_2_t3381670217 *)__this)->get_observer_0();
			il2cpp_codegen_memory_barrier();
			NullCheck((Il2CppObject*)L_3);
			InterfaceActionInvoker0::Invoke(0 /* System.Void UniRx.IObserver`1<System.Object>::OnCompleted() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6), (Il2CppObject*)L_3);
			IL2CPP_LEAVE(0x38, FINALLY_0031);
		} // end try (depth: 2)
		catch(Il2CppExceptionWrapper& e)
		{
			__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
			goto FINALLY_0031;
		}

FINALLY_0031:
		{ // begin finally (depth: 2)
			NullCheck((OperatorObserverBase_2_t3381670217 *)__this);
			VirtActionInvoker0::Invoke(4 /* System.Void UniRx.Operators.OperatorObserverBase`2<UniRx.IObservable`1<System.Object>,System.Object>::Dispose() */, (OperatorObserverBase_2_t3381670217 *)__this);
			IL2CPP_END_FINALLY(49)
		} // end finally (depth: 2)
		IL2CPP_CLEANUP(49)
		{
			IL2CPP_JUMP_TBL(0x38, IL_0038)
			IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
		}

IL_0038:
		{
			IL2CPP_LEAVE(0x44, FINALLY_003d);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_003d;
	}

FINALLY_003d:
	{ // begin finally (depth: 1)
		Il2CppObject * L_4 = V_0;
		Monitor_Exit_m2088237919(NULL /*static, unused*/, (Il2CppObject *)L_4, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(61)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(61)
	{
		IL2CPP_JUMP_TBL(0x44, IL_0044)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0044:
	{
		return;
	}
}
// System.Void UniRx.Operators.SwitchObservable`1<System.Object>::.ctor(UniRx.IObservable`1<UniRx.IObservable`1<T>>)
extern "C"  void SwitchObservable_1__ctor_m2020426066_gshared (SwitchObservable_1_t1854652486 * __this, Il2CppObject* ___sources0, const MethodInfo* method)
{
	{
		NullCheck((OperatorObservableBase_1_t4196218687 *)__this);
		((  void (*) (OperatorObservableBase_1_t4196218687 *, bool, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((OperatorObservableBase_1_t4196218687 *)__this, (bool)1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		Il2CppObject* L_0 = ___sources0;
		__this->set_sources_1(L_0);
		return;
	}
}
// System.IDisposable UniRx.Operators.SwitchObservable`1<System.Object>::SubscribeCore(UniRx.IObserver`1<T>,System.IDisposable)
extern "C"  Il2CppObject * SwitchObservable_1_SubscribeCore_m2713713870_gshared (SwitchObservable_1_t1854652486 * __this, Il2CppObject* ___observer0, Il2CppObject * ___cancel1, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = ___observer0;
		Il2CppObject * L_1 = ___cancel1;
		SwitchObserver_t321759539 * L_2 = (SwitchObserver_t321759539 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		((  void (*) (SwitchObserver_t321759539 *, SwitchObservable_1_t1854652486 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)(L_2, (SwitchObservable_1_t1854652486 *)__this, (Il2CppObject*)L_0, (Il2CppObject *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		NullCheck((SwitchObserver_t321759539 *)L_2);
		Il2CppObject * L_3 = ((  Il2CppObject * (*) (SwitchObserver_t321759539 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)((SwitchObserver_t321759539 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		return L_3;
	}
}
// System.Void UniRx.Operators.SynchronizedObserver`1<System.Object>::.ctor(UniRx.IObserver`1<T>,System.Object)
extern "C"  void SynchronizedObserver_1__ctor_m3475009133_gshared (SynchronizedObserver_1_t2847152769 * __this, Il2CppObject* ___observer0, Il2CppObject * ___gate1, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		Il2CppObject* L_0 = ___observer0;
		__this->set_observer_0(L_0);
		Il2CppObject * L_1 = ___gate1;
		__this->set_gate_1(L_1);
		return;
	}
}
// System.Void UniRx.Operators.SynchronizedObserver`1<System.Object>::OnNext(T)
extern "C"  void SynchronizedObserver_1_OnNext_m3514316437_gshared (SynchronizedObserver_1_t2847152769 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	Il2CppObject * V_0 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_gate_1();
		V_0 = (Il2CppObject *)L_0;
		Il2CppObject * L_1 = V_0;
		Monitor_Enter_m476686225(NULL /*static, unused*/, (Il2CppObject *)L_1, /*hidden argument*/NULL);
	}

IL_000d:
	try
	{ // begin try (depth: 1)
		Il2CppObject* L_2 = (Il2CppObject*)__this->get_observer_0();
		Il2CppObject * L_3 = ___value0;
		NullCheck((Il2CppObject*)L_2);
		InterfaceActionInvoker1< Il2CppObject * >::Invoke(2 /* System.Void UniRx.IObserver`1<System.Object>::OnNext(T) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0), (Il2CppObject*)L_2, (Il2CppObject *)L_3);
		IL2CPP_LEAVE(0x25, FINALLY_001e);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_001e;
	}

FINALLY_001e:
	{ // begin finally (depth: 1)
		Il2CppObject * L_4 = V_0;
		Monitor_Exit_m2088237919(NULL /*static, unused*/, (Il2CppObject *)L_4, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(30)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(30)
	{
		IL2CPP_JUMP_TBL(0x25, IL_0025)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0025:
	{
		return;
	}
}
// System.Void UniRx.Operators.SynchronizedObserver`1<System.Object>::OnError(System.Exception)
extern "C"  void SynchronizedObserver_1_OnError_m2490271652_gshared (SynchronizedObserver_1_t2847152769 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method)
{
	Il2CppObject * V_0 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_gate_1();
		V_0 = (Il2CppObject *)L_0;
		Il2CppObject * L_1 = V_0;
		Monitor_Enter_m476686225(NULL /*static, unused*/, (Il2CppObject *)L_1, /*hidden argument*/NULL);
	}

IL_000d:
	try
	{ // begin try (depth: 1)
		Il2CppObject* L_2 = (Il2CppObject*)__this->get_observer_0();
		Exception_t1967233988 * L_3 = ___error0;
		NullCheck((Il2CppObject*)L_2);
		InterfaceActionInvoker1< Exception_t1967233988 * >::Invoke(1 /* System.Void UniRx.IObserver`1<System.Object>::OnError(System.Exception) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0), (Il2CppObject*)L_2, (Exception_t1967233988 *)L_3);
		IL2CPP_LEAVE(0x25, FINALLY_001e);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_001e;
	}

FINALLY_001e:
	{ // begin finally (depth: 1)
		Il2CppObject * L_4 = V_0;
		Monitor_Exit_m2088237919(NULL /*static, unused*/, (Il2CppObject *)L_4, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(30)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(30)
	{
		IL2CPP_JUMP_TBL(0x25, IL_0025)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0025:
	{
		return;
	}
}
// System.Void UniRx.Operators.SynchronizedObserver`1<System.Object>::OnCompleted()
extern "C"  void SynchronizedObserver_1_OnCompleted_m2880475383_gshared (SynchronizedObserver_1_t2847152769 * __this, const MethodInfo* method)
{
	Il2CppObject * V_0 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_gate_1();
		V_0 = (Il2CppObject *)L_0;
		Il2CppObject * L_1 = V_0;
		Monitor_Enter_m476686225(NULL /*static, unused*/, (Il2CppObject *)L_1, /*hidden argument*/NULL);
	}

IL_000d:
	try
	{ // begin try (depth: 1)
		Il2CppObject* L_2 = (Il2CppObject*)__this->get_observer_0();
		NullCheck((Il2CppObject*)L_2);
		InterfaceActionInvoker0::Invoke(0 /* System.Void UniRx.IObserver`1<System.Object>::OnCompleted() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0), (Il2CppObject*)L_2);
		IL2CPP_LEAVE(0x24, FINALLY_001d);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_001d;
	}

FINALLY_001d:
	{ // begin finally (depth: 1)
		Il2CppObject * L_3 = V_0;
		Monitor_Exit_m2088237919(NULL /*static, unused*/, (Il2CppObject *)L_3, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(29)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(29)
	{
		IL2CPP_JUMP_TBL(0x24, IL_0024)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0024:
	{
		return;
	}
}
// System.Void UniRx.Operators.SynchronizeObservable`1/Synchronize<System.Object>::.ctor(UniRx.Operators.SynchronizeObservable`1<T>,UniRx.IObserver`1<T>,System.IDisposable)
extern "C"  void Synchronize__ctor_m3932415724_gshared (Synchronize_t2861865257 * __this, SynchronizeObservable_1_t736720834 * ___parent0, Il2CppObject* ___observer1, Il2CppObject * ___cancel2, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = ___observer1;
		Il2CppObject * L_1 = ___cancel2;
		NullCheck((OperatorObserverBase_2_t1187768149 *)__this);
		((  void (*) (OperatorObserverBase_2_t1187768149 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((OperatorObserverBase_2_t1187768149 *)__this, (Il2CppObject*)L_0, (Il2CppObject *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		SynchronizeObservable_1_t736720834 * L_2 = ___parent0;
		__this->set_parent_2(L_2);
		return;
	}
}
// System.Void UniRx.Operators.SynchronizeObservable`1/Synchronize<System.Object>::OnNext(T)
extern "C"  void Synchronize_OnNext_m212815145_gshared (Synchronize_t2861865257 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	Il2CppObject * V_0 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		SynchronizeObservable_1_t736720834 * L_0 = (SynchronizeObservable_1_t736720834 *)__this->get_parent_2();
		NullCheck(L_0);
		Il2CppObject * L_1 = (Il2CppObject *)L_0->get_gate_2();
		V_0 = (Il2CppObject *)L_1;
		Il2CppObject * L_2 = V_0;
		Monitor_Enter_m476686225(NULL /*static, unused*/, (Il2CppObject *)L_2, /*hidden argument*/NULL);
	}

IL_0012:
	try
	{ // begin try (depth: 1)
		Il2CppObject* L_3 = (Il2CppObject*)((OperatorObserverBase_2_t1187768149 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		Il2CppObject * L_4 = ___value0;
		NullCheck((Il2CppObject*)L_3);
		InterfaceActionInvoker1< Il2CppObject * >::Invoke(2 /* System.Void UniRx.IObserver`1<System.Object>::OnNext(T) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), (Il2CppObject*)L_3, (Il2CppObject *)L_4);
		IL2CPP_LEAVE(0x2C, FINALLY_0025);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0025;
	}

FINALLY_0025:
	{ // begin finally (depth: 1)
		Il2CppObject * L_5 = V_0;
		Monitor_Exit_m2088237919(NULL /*static, unused*/, (Il2CppObject *)L_5, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(37)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(37)
	{
		IL2CPP_JUMP_TBL(0x2C, IL_002c)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_002c:
	{
		return;
	}
}
// System.Void UniRx.Operators.SynchronizeObservable`1/Synchronize<System.Object>::OnError(System.Exception)
extern "C"  void Synchronize_OnError_m3169711672_gshared (Synchronize_t2861865257 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method)
{
	Il2CppObject * V_0 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		SynchronizeObservable_1_t736720834 * L_0 = (SynchronizeObservable_1_t736720834 *)__this->get_parent_2();
		NullCheck(L_0);
		Il2CppObject * L_1 = (Il2CppObject *)L_0->get_gate_2();
		V_0 = (Il2CppObject *)L_1;
		Il2CppObject * L_2 = V_0;
		Monitor_Enter_m476686225(NULL /*static, unused*/, (Il2CppObject *)L_2, /*hidden argument*/NULL);
	}

IL_0012:
	try
	{ // begin try (depth: 1)
		try
		{ // begin try (depth: 2)
			Il2CppObject* L_3 = (Il2CppObject*)((OperatorObserverBase_2_t1187768149 *)__this)->get_observer_0();
			il2cpp_codegen_memory_barrier();
			Exception_t1967233988 * L_4 = ___error0;
			NullCheck((Il2CppObject*)L_3);
			InterfaceActionInvoker1< Exception_t1967233988 * >::Invoke(1 /* System.Void UniRx.IObserver`1<System.Object>::OnError(System.Exception) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), (Il2CppObject*)L_3, (Exception_t1967233988 *)L_4);
			IL2CPP_LEAVE(0x2C, FINALLY_0025);
		} // end try (depth: 2)
		catch(Il2CppExceptionWrapper& e)
		{
			__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
			goto FINALLY_0025;
		}

FINALLY_0025:
		{ // begin finally (depth: 2)
			NullCheck((OperatorObserverBase_2_t1187768149 *)__this);
			VirtActionInvoker0::Invoke(4 /* System.Void UniRx.Operators.OperatorObserverBase`2<System.Object,System.Object>::Dispose() */, (OperatorObserverBase_2_t1187768149 *)__this);
			IL2CPP_END_FINALLY(37)
		} // end finally (depth: 2)
		IL2CPP_CLEANUP(37)
		{
			IL2CPP_JUMP_TBL(0x2C, IL_002c)
			IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
		}

IL_002c:
		{
			IL2CPP_LEAVE(0x38, FINALLY_0031);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0031;
	}

FINALLY_0031:
	{ // begin finally (depth: 1)
		Il2CppObject * L_5 = V_0;
		Monitor_Exit_m2088237919(NULL /*static, unused*/, (Il2CppObject *)L_5, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(49)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(49)
	{
		IL2CPP_JUMP_TBL(0x38, IL_0038)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0038:
	{
		return;
	}
}
// System.Void UniRx.Operators.SynchronizeObservable`1/Synchronize<System.Object>::OnCompleted()
extern "C"  void Synchronize_OnCompleted_m2979151243_gshared (Synchronize_t2861865257 * __this, const MethodInfo* method)
{
	Il2CppObject * V_0 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		SynchronizeObservable_1_t736720834 * L_0 = (SynchronizeObservable_1_t736720834 *)__this->get_parent_2();
		NullCheck(L_0);
		Il2CppObject * L_1 = (Il2CppObject *)L_0->get_gate_2();
		V_0 = (Il2CppObject *)L_1;
		Il2CppObject * L_2 = V_0;
		Monitor_Enter_m476686225(NULL /*static, unused*/, (Il2CppObject *)L_2, /*hidden argument*/NULL);
	}

IL_0012:
	try
	{ // begin try (depth: 1)
		try
		{ // begin try (depth: 2)
			Il2CppObject* L_3 = (Il2CppObject*)((OperatorObserverBase_2_t1187768149 *)__this)->get_observer_0();
			il2cpp_codegen_memory_barrier();
			NullCheck((Il2CppObject*)L_3);
			InterfaceActionInvoker0::Invoke(0 /* System.Void UniRx.IObserver`1<System.Object>::OnCompleted() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), (Il2CppObject*)L_3);
			IL2CPP_LEAVE(0x2B, FINALLY_0024);
		} // end try (depth: 2)
		catch(Il2CppExceptionWrapper& e)
		{
			__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
			goto FINALLY_0024;
		}

FINALLY_0024:
		{ // begin finally (depth: 2)
			NullCheck((OperatorObserverBase_2_t1187768149 *)__this);
			VirtActionInvoker0::Invoke(4 /* System.Void UniRx.Operators.OperatorObserverBase`2<System.Object,System.Object>::Dispose() */, (OperatorObserverBase_2_t1187768149 *)__this);
			IL2CPP_END_FINALLY(36)
		} // end finally (depth: 2)
		IL2CPP_CLEANUP(36)
		{
			IL2CPP_JUMP_TBL(0x2B, IL_002b)
			IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
		}

IL_002b:
		{
			IL2CPP_LEAVE(0x37, FINALLY_0030);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0030;
	}

FINALLY_0030:
	{ // begin finally (depth: 1)
		Il2CppObject * L_4 = V_0;
		Monitor_Exit_m2088237919(NULL /*static, unused*/, (Il2CppObject *)L_4, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(48)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(48)
	{
		IL2CPP_JUMP_TBL(0x37, IL_0037)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0037:
	{
		return;
	}
}
// System.Void UniRx.Operators.SynchronizeObservable`1<System.Object>::.ctor(UniRx.IObservable`1<T>,System.Object)
extern "C"  void SynchronizeObservable_1__ctor_m1075160077_gshared (SynchronizeObservable_1_t736720834 * __this, Il2CppObject* ___source0, Il2CppObject * ___gate1, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = ___source0;
		bool L_1 = ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)(NULL /*static, unused*/, (Il2CppObject*)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		NullCheck((OperatorObservableBase_1_t4196218687 *)__this);
		((  void (*) (OperatorObservableBase_1_t4196218687 *, bool, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)((OperatorObservableBase_1_t4196218687 *)__this, (bool)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		Il2CppObject* L_2 = ___source0;
		__this->set_source_1(L_2);
		Il2CppObject * L_3 = ___gate1;
		__this->set_gate_2(L_3);
		return;
	}
}
// System.IDisposable UniRx.Operators.SynchronizeObservable`1<System.Object>::SubscribeCore(UniRx.IObserver`1<T>,System.IDisposable)
extern "C"  Il2CppObject * SynchronizeObservable_1_SubscribeCore_m1097568574_gshared (SynchronizeObservable_1_t736720834 * __this, Il2CppObject* ___observer0, Il2CppObject * ___cancel1, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = (Il2CppObject*)__this->get_source_1();
		Il2CppObject* L_1 = ___observer0;
		Il2CppObject * L_2 = ___cancel1;
		Synchronize_t2861865257 * L_3 = (Synchronize_t2861865257 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		((  void (*) (Synchronize_t2861865257 *, SynchronizeObservable_1_t736720834 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)(L_3, (SynchronizeObservable_1_t736720834 *)__this, (Il2CppObject*)L_1, (Il2CppObject *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		NullCheck((Il2CppObject*)L_0);
		Il2CppObject * L_4 = InterfaceFuncInvoker1< Il2CppObject *, Il2CppObject* >::Invoke(0 /* System.IDisposable UniRx.IObservable`1<System.Object>::Subscribe(UniRx.IObserver`1<T>) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5), (Il2CppObject*)L_0, (Il2CppObject*)L_3);
		return L_4;
	}
}
// System.Void UniRx.Operators.TakeLastObservable`1/TakeLast_<System.Object>::.ctor(UniRx.Operators.TakeLastObservable`1<T>,UniRx.IObserver`1<T>,System.IDisposable)
extern "C"  void TakeLast___ctor_m2106102018_gshared (TakeLast__t2176345419 * __this, TakeLastObservable_1_t3265267047 * ___parent0, Il2CppObject* ___observer1, Il2CppObject * ___cancel2, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = ___observer1;
		Il2CppObject * L_1 = ___cancel2;
		NullCheck((OperatorObserverBase_2_t1187768149 *)__this);
		((  void (*) (OperatorObserverBase_2_t1187768149 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((OperatorObserverBase_2_t1187768149 *)__this, (Il2CppObject*)L_0, (Il2CppObject *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		TakeLastObservable_1_t3265267047 * L_2 = ___parent0;
		__this->set_parent_3(L_2);
		Queue_1_t2416153082 * L_3 = (Queue_1_t2416153082 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		((  void (*) (Queue_1_t2416153082 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)(L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		__this->set_q_4(L_3);
		return;
	}
}
// System.IDisposable UniRx.Operators.TakeLastObservable`1/TakeLast_<System.Object>::Run()
extern Il2CppClass* IScheduler_t2938318244_il2cpp_TypeInfo_var;
extern const uint32_t TakeLast__Run_m144593786_MetadataUsageId;
extern "C"  Il2CppObject * TakeLast__Run_m144593786_gshared (TakeLast__t2176345419 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TakeLast__Run_m144593786_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		TakeLastObservable_1_t3265267047 * L_0 = (TakeLastObservable_1_t3265267047 *)__this->get_parent_3();
		NullCheck(L_0);
		Il2CppObject * L_1 = (Il2CppObject *)L_0->get_scheduler_4();
		NullCheck((Il2CppObject *)L_1);
		DateTimeOffset_t3712260035  L_2 = InterfaceFuncInvoker0< DateTimeOffset_t3712260035  >::Invoke(0 /* System.DateTimeOffset UniRx.IScheduler::get_Now() */, IScheduler_t2938318244_il2cpp_TypeInfo_var, (Il2CppObject *)L_1);
		__this->set_startTime_2(L_2);
		TakeLastObservable_1_t3265267047 * L_3 = (TakeLastObservable_1_t3265267047 *)__this->get_parent_3();
		NullCheck(L_3);
		Il2CppObject* L_4 = (Il2CppObject*)L_3->get_source_1();
		NullCheck((Il2CppObject*)L_4);
		Il2CppObject * L_5 = InterfaceFuncInvoker1< Il2CppObject *, Il2CppObject* >::Invoke(0 /* System.IDisposable UniRx.IObservable`1<System.Object>::Subscribe(UniRx.IObserver`1<T>) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4), (Il2CppObject*)L_4, (Il2CppObject*)__this);
		return L_5;
	}
}
// System.Void UniRx.Operators.TakeLastObservable`1/TakeLast_<System.Object>::OnNext(T)
extern Il2CppClass* IScheduler_t2938318244_il2cpp_TypeInfo_var;
extern Il2CppClass* DateTimeOffset_t3712260035_il2cpp_TypeInfo_var;
extern const uint32_t TakeLast__OnNext_m3517845822_MetadataUsageId;
extern "C"  void TakeLast__OnNext_m3517845822_gshared (TakeLast__t2176345419 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TakeLast__OnNext_m3517845822_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	DateTimeOffset_t3712260035  V_0;
	memset(&V_0, 0, sizeof(V_0));
	TimeSpan_t763862892  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		TakeLastObservable_1_t3265267047 * L_0 = (TakeLastObservable_1_t3265267047 *)__this->get_parent_3();
		NullCheck(L_0);
		Il2CppObject * L_1 = (Il2CppObject *)L_0->get_scheduler_4();
		NullCheck((Il2CppObject *)L_1);
		DateTimeOffset_t3712260035  L_2 = InterfaceFuncInvoker0< DateTimeOffset_t3712260035  >::Invoke(0 /* System.DateTimeOffset UniRx.IScheduler::get_Now() */, IScheduler_t2938318244_il2cpp_TypeInfo_var, (Il2CppObject *)L_1);
		V_0 = (DateTimeOffset_t3712260035 )L_2;
		DateTimeOffset_t3712260035  L_3 = V_0;
		DateTimeOffset_t3712260035  L_4 = (DateTimeOffset_t3712260035 )__this->get_startTime_2();
		IL2CPP_RUNTIME_CLASS_INIT(DateTimeOffset_t3712260035_il2cpp_TypeInfo_var);
		TimeSpan_t763862892  L_5 = DateTimeOffset_op_Subtraction_m3815865550(NULL /*static, unused*/, (DateTimeOffset_t3712260035 )L_3, (DateTimeOffset_t3712260035 )L_4, /*hidden argument*/NULL);
		V_1 = (TimeSpan_t763862892 )L_5;
		Queue_1_t2416153082 * L_6 = (Queue_1_t2416153082 *)__this->get_q_4();
		Il2CppObject * L_7 = ___value0;
		TimeSpan_t763862892  L_8 = V_1;
		TimeInterval_1_t708065542  L_9;
		memset(&L_9, 0, sizeof(L_9));
		((  void (*) (TimeInterval_1_t708065542 *, Il2CppObject *, TimeSpan_t763862892 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6)->method)(&L_9, (Il2CppObject *)L_7, (TimeSpan_t763862892 )L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		NullCheck((Queue_1_t2416153082 *)L_6);
		((  void (*) (Queue_1_t2416153082 *, TimeInterval_1_t708065542 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7)->method)((Queue_1_t2416153082 *)L_6, (TimeInterval_1_t708065542 )L_9, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7));
		TimeSpan_t763862892  L_10 = V_1;
		NullCheck((TakeLast__t2176345419 *)__this);
		((  void (*) (TakeLast__t2176345419 *, TimeSpan_t763862892 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8)->method)((TakeLast__t2176345419 *)__this, (TimeSpan_t763862892 )L_10, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8));
		return;
	}
}
// System.Void UniRx.Operators.TakeLastObservable`1/TakeLast_<System.Object>::OnError(System.Exception)
extern "C"  void TakeLast__OnError_m1909715021_gshared (TakeLast__t2176345419 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method)
{
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		Il2CppObject* L_0 = (Il2CppObject*)((OperatorObserverBase_2_t1187768149 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		Exception_t1967233988 * L_1 = ___error0;
		NullCheck((Il2CppObject*)L_0);
		InterfaceActionInvoker1< Exception_t1967233988 * >::Invoke(1 /* System.Void UniRx.IObserver`1<System.Object>::OnError(System.Exception) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9), (Il2CppObject*)L_0, (Exception_t1967233988 *)L_1);
		IL2CPP_LEAVE(0x1A, FINALLY_0013);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0013;
	}

FINALLY_0013:
	{ // begin finally (depth: 1)
		NullCheck((OperatorObserverBase_2_t1187768149 *)__this);
		VirtActionInvoker0::Invoke(4 /* System.Void UniRx.Operators.OperatorObserverBase`2<System.Object,System.Object>::Dispose() */, (OperatorObserverBase_2_t1187768149 *)__this);
		IL2CPP_END_FINALLY(19)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(19)
	{
		IL2CPP_JUMP_TBL(0x1A, IL_001a)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_001a:
	{
		return;
	}
}
// System.Void UniRx.Operators.TakeLastObservable`1/TakeLast_<System.Object>::OnCompleted()
extern Il2CppClass* IScheduler_t2938318244_il2cpp_TypeInfo_var;
extern Il2CppClass* DateTimeOffset_t3712260035_il2cpp_TypeInfo_var;
extern Il2CppClass* IDisposable_t1628921374_il2cpp_TypeInfo_var;
extern const uint32_t TakeLast__OnCompleted_m2461462304_MetadataUsageId;
extern "C"  void TakeLast__OnCompleted_m2461462304_gshared (TakeLast__t2176345419 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TakeLast__OnCompleted_m2461462304_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	DateTimeOffset_t3712260035  V_0;
	memset(&V_0, 0, sizeof(V_0));
	TimeSpan_t763862892  V_1;
	memset(&V_1, 0, sizeof(V_1));
	TimeInterval_1_t708065542  V_2;
	memset(&V_2, 0, sizeof(V_2));
	Enumerator_t3885774799  V_3;
	memset(&V_3, 0, sizeof(V_3));
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		TakeLastObservable_1_t3265267047 * L_0 = (TakeLastObservable_1_t3265267047 *)__this->get_parent_3();
		NullCheck(L_0);
		Il2CppObject * L_1 = (Il2CppObject *)L_0->get_scheduler_4();
		NullCheck((Il2CppObject *)L_1);
		DateTimeOffset_t3712260035  L_2 = InterfaceFuncInvoker0< DateTimeOffset_t3712260035  >::Invoke(0 /* System.DateTimeOffset UniRx.IScheduler::get_Now() */, IScheduler_t2938318244_il2cpp_TypeInfo_var, (Il2CppObject *)L_1);
		V_0 = (DateTimeOffset_t3712260035 )L_2;
		DateTimeOffset_t3712260035  L_3 = V_0;
		DateTimeOffset_t3712260035  L_4 = (DateTimeOffset_t3712260035 )__this->get_startTime_2();
		IL2CPP_RUNTIME_CLASS_INIT(DateTimeOffset_t3712260035_il2cpp_TypeInfo_var);
		TimeSpan_t763862892  L_5 = DateTimeOffset_op_Subtraction_m3815865550(NULL /*static, unused*/, (DateTimeOffset_t3712260035 )L_3, (DateTimeOffset_t3712260035 )L_4, /*hidden argument*/NULL);
		V_1 = (TimeSpan_t763862892 )L_5;
		TimeSpan_t763862892  L_6 = V_1;
		NullCheck((TakeLast__t2176345419 *)__this);
		((  void (*) (TakeLast__t2176345419 *, TimeSpan_t763862892 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8)->method)((TakeLast__t2176345419 *)__this, (TimeSpan_t763862892 )L_6, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8));
		Queue_1_t2416153082 * L_7 = (Queue_1_t2416153082 *)__this->get_q_4();
		NullCheck((Queue_1_t2416153082 *)L_7);
		Enumerator_t3885774799  L_8 = ((  Enumerator_t3885774799  (*) (Queue_1_t2416153082 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 11)->method)((Queue_1_t2416153082 *)L_7, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 11));
		V_3 = (Enumerator_t3885774799 )L_8;
	}

IL_0031:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0052;
		}

IL_0036:
		{
			TimeInterval_1_t708065542  L_9 = ((  TimeInterval_1_t708065542  (*) (Enumerator_t3885774799 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 12)->method)((Enumerator_t3885774799 *)(&V_3), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 12));
			V_2 = (TimeInterval_1_t708065542 )L_9;
			Il2CppObject* L_10 = (Il2CppObject*)((OperatorObserverBase_2_t1187768149 *)__this)->get_observer_0();
			il2cpp_codegen_memory_barrier();
			Il2CppObject * L_11 = ((  Il2CppObject * (*) (TimeInterval_1_t708065542 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 13)->method)((TimeInterval_1_t708065542 *)(&V_2), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 13));
			NullCheck((Il2CppObject*)L_10);
			InterfaceActionInvoker1< Il2CppObject * >::Invoke(2 /* System.Void UniRx.IObserver`1<System.Object>::OnNext(T) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9), (Il2CppObject*)L_10, (Il2CppObject *)L_11);
		}

IL_0052:
		{
			bool L_12 = ((  bool (*) (Enumerator_t3885774799 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 14)->method)((Enumerator_t3885774799 *)(&V_3), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 14));
			if (L_12)
			{
				goto IL_0036;
			}
		}

IL_005e:
		{
			IL2CPP_LEAVE(0x6F, FINALLY_0063);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0063;
	}

FINALLY_0063:
	{ // begin finally (depth: 1)
		Enumerator_t3885774799  L_13 = V_3;
		Enumerator_t3885774799  L_14 = L_13;
		Il2CppObject * L_15 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 15), &L_14);
		NullCheck((Il2CppObject *)L_15);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, (Il2CppObject *)L_15);
		IL2CPP_END_FINALLY(99)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(99)
	{
		IL2CPP_JUMP_TBL(0x6F, IL_006f)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_006f:
	try
	{ // begin try (depth: 1)
		Il2CppObject* L_16 = (Il2CppObject*)((OperatorObserverBase_2_t1187768149 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		NullCheck((Il2CppObject*)L_16);
		InterfaceActionInvoker0::Invoke(0 /* System.Void UniRx.IObserver`1<System.Object>::OnCompleted() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9), (Il2CppObject*)L_16);
		IL2CPP_LEAVE(0x88, FINALLY_0081);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0081;
	}

FINALLY_0081:
	{ // begin finally (depth: 1)
		NullCheck((OperatorObserverBase_2_t1187768149 *)__this);
		VirtActionInvoker0::Invoke(4 /* System.Void UniRx.Operators.OperatorObserverBase`2<System.Object,System.Object>::Dispose() */, (OperatorObserverBase_2_t1187768149 *)__this);
		IL2CPP_END_FINALLY(129)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(129)
	{
		IL2CPP_JUMP_TBL(0x88, IL_0088)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0088:
	{
		return;
	}
}
// System.Void UniRx.Operators.TakeLastObservable`1/TakeLast_<System.Object>::Trim(System.TimeSpan)
extern Il2CppClass* TimeSpan_t763862892_il2cpp_TypeInfo_var;
extern const uint32_t TakeLast__Trim_m323671436_MetadataUsageId;
extern "C"  void TakeLast__Trim_m323671436_gshared (TakeLast__t2176345419 * __this, TimeSpan_t763862892  ___now0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TakeLast__Trim_m323671436_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	TimeInterval_1_t708065542  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		goto IL_0011;
	}

IL_0005:
	{
		Queue_1_t2416153082 * L_0 = (Queue_1_t2416153082 *)__this->get_q_4();
		NullCheck((Queue_1_t2416153082 *)L_0);
		((  TimeInterval_1_t708065542  (*) (Queue_1_t2416153082 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 16)->method)((Queue_1_t2416153082 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 16));
	}

IL_0011:
	{
		Queue_1_t2416153082 * L_1 = (Queue_1_t2416153082 *)__this->get_q_4();
		NullCheck((Queue_1_t2416153082 *)L_1);
		int32_t L_2 = VirtFuncInvoker0< int32_t >::Invoke(6 /* System.Int32 System.Collections.Generic.Queue`1<UniRx.TimeInterval`1<System.Object>>::get_Count() */, (Queue_1_t2416153082 *)L_1);
		if ((((int32_t)L_2) <= ((int32_t)0)))
		{
			goto IL_0050;
		}
	}
	{
		TimeSpan_t763862892  L_3 = ___now0;
		Queue_1_t2416153082 * L_4 = (Queue_1_t2416153082 *)__this->get_q_4();
		NullCheck((Queue_1_t2416153082 *)L_4);
		TimeInterval_1_t708065542  L_5 = ((  TimeInterval_1_t708065542  (*) (Queue_1_t2416153082 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 18)->method)((Queue_1_t2416153082 *)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 18));
		V_0 = (TimeInterval_1_t708065542 )L_5;
		TimeSpan_t763862892  L_6 = ((  TimeSpan_t763862892  (*) (TimeInterval_1_t708065542 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 19)->method)((TimeInterval_1_t708065542 *)(&V_0), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 19));
		IL2CPP_RUNTIME_CLASS_INIT(TimeSpan_t763862892_il2cpp_TypeInfo_var);
		TimeSpan_t763862892  L_7 = TimeSpan_op_Subtraction_m3686790579(NULL /*static, unused*/, (TimeSpan_t763862892 )L_3, (TimeSpan_t763862892 )L_6, /*hidden argument*/NULL);
		TakeLastObservable_1_t3265267047 * L_8 = (TakeLastObservable_1_t3265267047 *)__this->get_parent_3();
		NullCheck(L_8);
		TimeSpan_t763862892  L_9 = (TimeSpan_t763862892 )L_8->get_duration_3();
		bool L_10 = TimeSpan_op_GreaterThanOrEqual_m2152983360(NULL /*static, unused*/, (TimeSpan_t763862892 )L_7, (TimeSpan_t763862892 )L_9, /*hidden argument*/NULL);
		if (L_10)
		{
			goto IL_0005;
		}
	}

IL_0050:
	{
		return;
	}
}
// System.Void UniRx.Operators.TakeLastObservable`1/TakeLast<System.Object>::.ctor(UniRx.Operators.TakeLastObservable`1<T>,UniRx.IObserver`1<T>,System.IDisposable)
extern "C"  void TakeLast__ctor_m3937985633_gshared (TakeLast_t798930510 * __this, TakeLastObservable_1_t3265267047 * ___parent0, Il2CppObject* ___observer1, Il2CppObject * ___cancel2, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = ___observer1;
		Il2CppObject * L_1 = ___cancel2;
		NullCheck((OperatorObserverBase_2_t1187768149 *)__this);
		((  void (*) (OperatorObserverBase_2_t1187768149 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((OperatorObserverBase_2_t1187768149 *)__this, (Il2CppObject*)L_0, (Il2CppObject *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		TakeLastObservable_1_t3265267047 * L_2 = ___parent0;
		__this->set_parent_2(L_2);
		Queue_1_t2545193960 * L_3 = (Queue_1_t2545193960 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		((  void (*) (Queue_1_t2545193960 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)(L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		__this->set_q_3(L_3);
		return;
	}
}
// System.IDisposable UniRx.Operators.TakeLastObservable`1/TakeLast<System.Object>::Run()
extern "C"  Il2CppObject * TakeLast_Run_m3420746597_gshared (TakeLast_t798930510 * __this, const MethodInfo* method)
{
	{
		TakeLastObservable_1_t3265267047 * L_0 = (TakeLastObservable_1_t3265267047 *)__this->get_parent_2();
		NullCheck(L_0);
		Il2CppObject* L_1 = (Il2CppObject*)L_0->get_source_1();
		NullCheck((Il2CppObject*)L_1);
		Il2CppObject * L_2 = InterfaceFuncInvoker1< Il2CppObject *, Il2CppObject* >::Invoke(0 /* System.IDisposable UniRx.IObservable`1<System.Object>::Subscribe(UniRx.IObserver`1<T>) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4), (Il2CppObject*)L_1, (Il2CppObject*)__this);
		return L_2;
	}
}
// System.Void UniRx.Operators.TakeLastObservable`1/TakeLast<System.Object>::OnNext(T)
extern "C"  void TakeLast_OnNext_m2026977151_gshared (TakeLast_t798930510 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	{
		Queue_1_t2545193960 * L_0 = (Queue_1_t2545193960 *)__this->get_q_3();
		Il2CppObject * L_1 = ___value0;
		NullCheck((Queue_1_t2545193960 *)L_0);
		((  void (*) (Queue_1_t2545193960 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->method)((Queue_1_t2545193960 *)L_0, (Il2CppObject *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		Queue_1_t2545193960 * L_2 = (Queue_1_t2545193960 *)__this->get_q_3();
		NullCheck((Queue_1_t2545193960 *)L_2);
		int32_t L_3 = VirtFuncInvoker0< int32_t >::Invoke(6 /* System.Int32 System.Collections.Generic.Queue`1<System.Object>::get_Count() */, (Queue_1_t2545193960 *)L_2);
		TakeLastObservable_1_t3265267047 * L_4 = (TakeLastObservable_1_t3265267047 *)__this->get_parent_2();
		NullCheck(L_4);
		int32_t L_5 = (int32_t)L_4->get_count_2();
		if ((((int32_t)L_3) <= ((int32_t)L_5)))
		{
			goto IL_0033;
		}
	}
	{
		Queue_1_t2545193960 * L_6 = (Queue_1_t2545193960 *)__this->get_q_3();
		NullCheck((Queue_1_t2545193960 *)L_6);
		((  Il2CppObject * (*) (Queue_1_t2545193960 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7)->method)((Queue_1_t2545193960 *)L_6, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7));
	}

IL_0033:
	{
		return;
	}
}
// System.Void UniRx.Operators.TakeLastObservable`1/TakeLast<System.Object>::OnError(System.Exception)
extern "C"  void TakeLast_OnError_m621573262_gshared (TakeLast_t798930510 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method)
{
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		Il2CppObject* L_0 = (Il2CppObject*)((OperatorObserverBase_2_t1187768149 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		Exception_t1967233988 * L_1 = ___error0;
		NullCheck((Il2CppObject*)L_0);
		InterfaceActionInvoker1< Exception_t1967233988 * >::Invoke(1 /* System.Void UniRx.IObserver`1<System.Object>::OnError(System.Exception) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8), (Il2CppObject*)L_0, (Exception_t1967233988 *)L_1);
		IL2CPP_LEAVE(0x1A, FINALLY_0013);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0013;
	}

FINALLY_0013:
	{ // begin finally (depth: 1)
		NullCheck((OperatorObserverBase_2_t1187768149 *)__this);
		VirtActionInvoker0::Invoke(4 /* System.Void UniRx.Operators.OperatorObserverBase`2<System.Object,System.Object>::Dispose() */, (OperatorObserverBase_2_t1187768149 *)__this);
		IL2CPP_END_FINALLY(19)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(19)
	{
		IL2CPP_JUMP_TBL(0x1A, IL_001a)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_001a:
	{
		return;
	}
}
// System.Void UniRx.Operators.TakeLastObservable`1/TakeLast<System.Object>::OnCompleted()
extern Il2CppClass* IDisposable_t1628921374_il2cpp_TypeInfo_var;
extern const uint32_t TakeLast_OnCompleted_m191565025_MetadataUsageId;
extern "C"  void TakeLast_OnCompleted_m191565025_gshared (TakeLast_t798930510 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TakeLast_OnCompleted_m191565025_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Il2CppObject * V_0 = NULL;
	Enumerator_t4014815680  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Queue_1_t2545193960 * L_0 = (Queue_1_t2545193960 *)__this->get_q_3();
		NullCheck((Queue_1_t2545193960 *)L_0);
		Enumerator_t4014815680  L_1 = ((  Enumerator_t4014815680  (*) (Queue_1_t2545193960 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10)->method)((Queue_1_t2545193960 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10));
		V_1 = (Enumerator_t4014815680 )L_1;
	}

IL_000c:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0027;
		}

IL_0011:
		{
			Il2CppObject * L_2 = ((  Il2CppObject * (*) (Enumerator_t4014815680 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 11)->method)((Enumerator_t4014815680 *)(&V_1), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 11));
			V_0 = (Il2CppObject *)L_2;
			Il2CppObject* L_3 = (Il2CppObject*)((OperatorObserverBase_2_t1187768149 *)__this)->get_observer_0();
			il2cpp_codegen_memory_barrier();
			Il2CppObject * L_4 = V_0;
			NullCheck((Il2CppObject*)L_3);
			InterfaceActionInvoker1< Il2CppObject * >::Invoke(2 /* System.Void UniRx.IObserver`1<System.Object>::OnNext(T) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8), (Il2CppObject*)L_3, (Il2CppObject *)L_4);
		}

IL_0027:
		{
			bool L_5 = ((  bool (*) (Enumerator_t4014815680 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 12)->method)((Enumerator_t4014815680 *)(&V_1), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 12));
			if (L_5)
			{
				goto IL_0011;
			}
		}

IL_0033:
		{
			IL2CPP_LEAVE(0x44, FINALLY_0038);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0038;
	}

FINALLY_0038:
	{ // begin finally (depth: 1)
		Enumerator_t4014815680  L_6 = V_1;
		Enumerator_t4014815680  L_7 = L_6;
		Il2CppObject * L_8 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 13), &L_7);
		NullCheck((Il2CppObject *)L_8);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, (Il2CppObject *)L_8);
		IL2CPP_END_FINALLY(56)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(56)
	{
		IL2CPP_JUMP_TBL(0x44, IL_0044)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0044:
	try
	{ // begin try (depth: 1)
		Il2CppObject* L_9 = (Il2CppObject*)((OperatorObserverBase_2_t1187768149 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		NullCheck((Il2CppObject*)L_9);
		InterfaceActionInvoker0::Invoke(0 /* System.Void UniRx.IObserver`1<System.Object>::OnCompleted() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8), (Il2CppObject*)L_9);
		IL2CPP_LEAVE(0x5D, FINALLY_0056);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0056;
	}

FINALLY_0056:
	{ // begin finally (depth: 1)
		NullCheck((OperatorObserverBase_2_t1187768149 *)__this);
		VirtActionInvoker0::Invoke(4 /* System.Void UniRx.Operators.OperatorObserverBase`2<System.Object,System.Object>::Dispose() */, (OperatorObserverBase_2_t1187768149 *)__this);
		IL2CPP_END_FINALLY(86)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(86)
	{
		IL2CPP_JUMP_TBL(0x5D, IL_005d)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_005d:
	{
		return;
	}
}
// System.Void UniRx.Operators.TakeLastObservable`1<System.Object>::.ctor(UniRx.IObservable`1<T>,System.Int32)
extern "C"  void TakeLastObservable_1__ctor_m1732400949_gshared (TakeLastObservable_1_t3265267047 * __this, Il2CppObject* ___source0, int32_t ___count1, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = ___source0;
		bool L_1 = ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)(NULL /*static, unused*/, (Il2CppObject*)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		NullCheck((OperatorObservableBase_1_t4196218687 *)__this);
		((  void (*) (OperatorObservableBase_1_t4196218687 *, bool, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)((OperatorObservableBase_1_t4196218687 *)__this, (bool)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		Il2CppObject* L_2 = ___source0;
		__this->set_source_1(L_2);
		int32_t L_3 = ___count1;
		__this->set_count_2(L_3);
		return;
	}
}
// System.Void UniRx.Operators.TakeLastObservable`1<System.Object>::.ctor(UniRx.IObservable`1<T>,System.TimeSpan,UniRx.IScheduler)
extern Il2CppClass* Scheduler_t103934925_il2cpp_TypeInfo_var;
extern const uint32_t TakeLastObservable_1__ctor_m2150835556_MetadataUsageId;
extern "C"  void TakeLastObservable_1__ctor_m2150835556_gshared (TakeLastObservable_1_t3265267047 * __this, Il2CppObject* ___source0, TimeSpan_t763862892  ___duration1, Il2CppObject * ___scheduler2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TakeLastObservable_1__ctor_m2150835556_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	TakeLastObservable_1_t3265267047 * G_B2_0 = NULL;
	TakeLastObservable_1_t3265267047 * G_B1_0 = NULL;
	int32_t G_B3_0 = 0;
	TakeLastObservable_1_t3265267047 * G_B3_1 = NULL;
	{
		Il2CppObject * L_0 = ___scheduler2;
		IL2CPP_RUNTIME_CLASS_INIT(Scheduler_t103934925_il2cpp_TypeInfo_var);
		Il2CppObject * L_1 = ((Scheduler_t103934925_StaticFields*)Scheduler_t103934925_il2cpp_TypeInfo_var->static_fields)->get_CurrentThread_0();
		G_B1_0 = ((TakeLastObservable_1_t3265267047 *)(__this));
		if ((((Il2CppObject*)(Il2CppObject *)L_0) == ((Il2CppObject*)(Il2CppObject *)L_1)))
		{
			G_B2_0 = ((TakeLastObservable_1_t3265267047 *)(__this));
			goto IL_0014;
		}
	}
	{
		Il2CppObject* L_2 = ___source0;
		bool L_3 = ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)(NULL /*static, unused*/, (Il2CppObject*)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		G_B3_0 = ((int32_t)(L_3));
		G_B3_1 = ((TakeLastObservable_1_t3265267047 *)(G_B1_0));
		goto IL_0015;
	}

IL_0014:
	{
		G_B3_0 = 1;
		G_B3_1 = ((TakeLastObservable_1_t3265267047 *)(G_B2_0));
	}

IL_0015:
	{
		NullCheck((OperatorObservableBase_1_t4196218687 *)G_B3_1);
		((  void (*) (OperatorObservableBase_1_t4196218687 *, bool, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)((OperatorObservableBase_1_t4196218687 *)G_B3_1, (bool)G_B3_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		Il2CppObject* L_4 = ___source0;
		__this->set_source_1(L_4);
		TimeSpan_t763862892  L_5 = ___duration1;
		__this->set_duration_3(L_5);
		Il2CppObject * L_6 = ___scheduler2;
		__this->set_scheduler_4(L_6);
		return;
	}
}
// System.IDisposable UniRx.Operators.TakeLastObservable`1<System.Object>::SubscribeCore(UniRx.IObserver`1<T>,System.IDisposable)
extern "C"  Il2CppObject * TakeLastObservable_1_SubscribeCore_m2269902629_gshared (TakeLastObservable_1_t3265267047 * __this, Il2CppObject* ___observer0, Il2CppObject * ___cancel1, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_scheduler_4();
		if (L_0)
		{
			goto IL_0019;
		}
	}
	{
		Il2CppObject* L_1 = ___observer0;
		Il2CppObject * L_2 = ___cancel1;
		TakeLast_t798930510 * L_3 = (TakeLast_t798930510 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		((  void (*) (TakeLast_t798930510 *, TakeLastObservable_1_t3265267047 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)(L_3, (TakeLastObservable_1_t3265267047 *)__this, (Il2CppObject*)L_1, (Il2CppObject *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		NullCheck((TakeLast_t798930510 *)L_3);
		Il2CppObject * L_4 = ((  Il2CppObject * (*) (TakeLast_t798930510 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->method)((TakeLast_t798930510 *)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		return L_4;
	}

IL_0019:
	{
		Il2CppObject* L_5 = ___observer0;
		Il2CppObject * L_6 = ___cancel1;
		TakeLast__t2176345419 * L_7 = (TakeLast__t2176345419 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		((  void (*) (TakeLast__t2176345419 *, TakeLastObservable_1_t3265267047 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7)->method)(L_7, (TakeLastObservable_1_t3265267047 *)__this, (Il2CppObject*)L_5, (Il2CppObject *)L_6, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7));
		NullCheck((TakeLast__t2176345419 *)L_7);
		Il2CppObject * L_8 = ((  Il2CppObject * (*) (TakeLast__t2176345419 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8)->method)((TakeLast__t2176345419 *)L_7, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8));
		return L_8;
	}
}
// System.Void UniRx.Operators.TakeObservable`1/Take_<System.Int64>::.ctor(UniRx.Operators.TakeObservable`1<T>,UniRx.IObserver`1<T>,System.IDisposable)
extern Il2CppClass* Il2CppObject_il2cpp_TypeInfo_var;
extern const uint32_t Take___ctor_m3799520386_MetadataUsageId;
extern "C"  void Take___ctor_m3799520386_gshared (Take__t183614399 * __this, TakeObservable_1_t794160655 * ___parent0, Il2CppObject* ___observer1, Il2CppObject * ___cancel2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Take___ctor_m3799520386_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject * L_0 = (Il2CppObject *)il2cpp_codegen_object_new(Il2CppObject_il2cpp_TypeInfo_var);
		Object__ctor_m1772956182(L_0, /*hidden argument*/NULL);
		__this->set_gate_3(L_0);
		Il2CppObject* L_1 = ___observer1;
		Il2CppObject * L_2 = ___cancel2;
		NullCheck((OperatorObserverBase_2_t3939730909 *)__this);
		((  void (*) (OperatorObserverBase_2_t3939730909 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((OperatorObserverBase_2_t3939730909 *)__this, (Il2CppObject*)L_1, (Il2CppObject *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		TakeObservable_1_t794160655 * L_3 = ___parent0;
		__this->set_parent_2(L_3);
		return;
	}
}
// System.IDisposable UniRx.Operators.TakeObservable`1/Take_<System.Int64>::Run()
extern Il2CppClass* Action_t437523947_il2cpp_TypeInfo_var;
extern Il2CppClass* IScheduler_t2938318244_il2cpp_TypeInfo_var;
extern const uint32_t Take__Run_m4022569518_MetadataUsageId;
extern "C"  Il2CppObject * Take__Run_m4022569518_gshared (Take__t183614399 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Take__Run_m4022569518_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Il2CppObject * V_0 = NULL;
	Il2CppObject * V_1 = NULL;
	{
		TakeObservable_1_t794160655 * L_0 = (TakeObservable_1_t794160655 *)__this->get_parent_2();
		NullCheck(L_0);
		Il2CppObject * L_1 = (Il2CppObject *)L_0->get_scheduler_4();
		TakeObservable_1_t794160655 * L_2 = (TakeObservable_1_t794160655 *)__this->get_parent_2();
		NullCheck(L_2);
		TimeSpan_t763862892  L_3 = (TimeSpan_t763862892 )L_2->get_duration_3();
		IntPtr_t L_4;
		L_4.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		Action_t437523947 * L_5 = (Action_t437523947 *)il2cpp_codegen_object_new(Action_t437523947_il2cpp_TypeInfo_var);
		Action__ctor_m2957240604(L_5, (Il2CppObject *)__this, (IntPtr_t)L_4, /*hidden argument*/NULL);
		NullCheck((Il2CppObject *)L_1);
		Il2CppObject * L_6 = InterfaceFuncInvoker2< Il2CppObject *, TimeSpan_t763862892 , Action_t437523947 * >::Invoke(2 /* System.IDisposable UniRx.IScheduler::Schedule(System.TimeSpan,System.Action) */, IScheduler_t2938318244_il2cpp_TypeInfo_var, (Il2CppObject *)L_1, (TimeSpan_t763862892 )L_3, (Action_t437523947 *)L_5);
		V_0 = (Il2CppObject *)L_6;
		TakeObservable_1_t794160655 * L_7 = (TakeObservable_1_t794160655 *)__this->get_parent_2();
		NullCheck(L_7);
		Il2CppObject* L_8 = (Il2CppObject*)L_7->get_source_1();
		NullCheck((Il2CppObject*)L_8);
		Il2CppObject * L_9 = InterfaceFuncInvoker1< Il2CppObject *, Il2CppObject* >::Invoke(0 /* System.IDisposable UniRx.IObservable`1<System.Int64>::Subscribe(UniRx.IObserver`1<T>) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3), (Il2CppObject*)L_8, (Il2CppObject*)__this);
		V_1 = (Il2CppObject *)L_9;
		Il2CppObject * L_10 = V_0;
		Il2CppObject * L_11 = V_1;
		Il2CppObject * L_12 = StableCompositeDisposable_Create_m3764788535(NULL /*static, unused*/, (Il2CppObject *)L_10, (Il2CppObject *)L_11, /*hidden argument*/NULL);
		return L_12;
	}
}
// System.Void UniRx.Operators.TakeObservable`1/Take_<System.Int64>::Tick()
extern "C"  void Take__Tick_m4004564353_gshared (Take__t183614399 * __this, const MethodInfo* method)
{
	Il2CppObject * V_0 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_gate_3();
		V_0 = (Il2CppObject *)L_0;
		Il2CppObject * L_1 = V_0;
		Monitor_Enter_m476686225(NULL /*static, unused*/, (Il2CppObject *)L_1, /*hidden argument*/NULL);
	}

IL_000d:
	try
	{ // begin try (depth: 1)
		try
		{ // begin try (depth: 2)
			Il2CppObject* L_2 = (Il2CppObject*)((OperatorObserverBase_2_t3939730909 *)__this)->get_observer_0();
			il2cpp_codegen_memory_barrier();
			NullCheck((Il2CppObject*)L_2);
			InterfaceActionInvoker0::Invoke(0 /* System.Void UniRx.IObserver`1<System.Int64>::OnCompleted() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4), (Il2CppObject*)L_2);
			IL2CPP_LEAVE(0x26, FINALLY_001f);
		} // end try (depth: 2)
		catch(Il2CppExceptionWrapper& e)
		{
			__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
			goto FINALLY_001f;
		}

FINALLY_001f:
		{ // begin finally (depth: 2)
			NullCheck((OperatorObserverBase_2_t3939730909 *)__this);
			VirtActionInvoker0::Invoke(4 /* System.Void UniRx.Operators.OperatorObserverBase`2<System.Int64,System.Int64>::Dispose() */, (OperatorObserverBase_2_t3939730909 *)__this);
			IL2CPP_END_FINALLY(31)
		} // end finally (depth: 2)
		IL2CPP_CLEANUP(31)
		{
			IL2CPP_JUMP_TBL(0x26, IL_0026)
			IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
		}

IL_0026:
		{
			IL2CPP_LEAVE(0x32, FINALLY_002b);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_002b;
	}

FINALLY_002b:
	{ // begin finally (depth: 1)
		Il2CppObject * L_3 = V_0;
		Monitor_Exit_m2088237919(NULL /*static, unused*/, (Il2CppObject *)L_3, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(43)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(43)
	{
		IL2CPP_JUMP_TBL(0x32, IL_0032)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0032:
	{
		return;
	}
}
// System.Void UniRx.Operators.TakeObservable`1/Take_<System.Int64>::OnNext(T)
extern "C"  void Take__OnNext_m1983155400_gshared (Take__t183614399 * __this, int64_t ___value0, const MethodInfo* method)
{
	Il2CppObject * V_0 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_gate_3();
		V_0 = (Il2CppObject *)L_0;
		Il2CppObject * L_1 = V_0;
		Monitor_Enter_m476686225(NULL /*static, unused*/, (Il2CppObject *)L_1, /*hidden argument*/NULL);
	}

IL_000d:
	try
	{ // begin try (depth: 1)
		Il2CppObject* L_2 = (Il2CppObject*)((OperatorObserverBase_2_t3939730909 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		int64_t L_3 = ___value0;
		NullCheck((Il2CppObject*)L_2);
		InterfaceActionInvoker1< int64_t >::Invoke(2 /* System.Void UniRx.IObserver`1<System.Int64>::OnNext(T) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4), (Il2CppObject*)L_2, (int64_t)L_3);
		IL2CPP_LEAVE(0x27, FINALLY_0020);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0020;
	}

FINALLY_0020:
	{ // begin finally (depth: 1)
		Il2CppObject * L_4 = V_0;
		Monitor_Exit_m2088237919(NULL /*static, unused*/, (Il2CppObject *)L_4, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(32)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(32)
	{
		IL2CPP_JUMP_TBL(0x27, IL_0027)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0027:
	{
		return;
	}
}
// System.Void UniRx.Operators.TakeObservable`1/Take_<System.Int64>::OnError(System.Exception)
extern "C"  void Take__OnError_m1670780887_gshared (Take__t183614399 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method)
{
	Il2CppObject * V_0 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_gate_3();
		V_0 = (Il2CppObject *)L_0;
		Il2CppObject * L_1 = V_0;
		Monitor_Enter_m476686225(NULL /*static, unused*/, (Il2CppObject *)L_1, /*hidden argument*/NULL);
	}

IL_000d:
	try
	{ // begin try (depth: 1)
		try
		{ // begin try (depth: 2)
			Il2CppObject* L_2 = (Il2CppObject*)((OperatorObserverBase_2_t3939730909 *)__this)->get_observer_0();
			il2cpp_codegen_memory_barrier();
			Exception_t1967233988 * L_3 = ___error0;
			NullCheck((Il2CppObject*)L_2);
			InterfaceActionInvoker1< Exception_t1967233988 * >::Invoke(1 /* System.Void UniRx.IObserver`1<System.Int64>::OnError(System.Exception) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4), (Il2CppObject*)L_2, (Exception_t1967233988 *)L_3);
			IL2CPP_LEAVE(0x27, FINALLY_0020);
		} // end try (depth: 2)
		catch(Il2CppExceptionWrapper& e)
		{
			__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
			goto FINALLY_0020;
		}

FINALLY_0020:
		{ // begin finally (depth: 2)
			NullCheck((OperatorObserverBase_2_t3939730909 *)__this);
			VirtActionInvoker0::Invoke(4 /* System.Void UniRx.Operators.OperatorObserverBase`2<System.Int64,System.Int64>::Dispose() */, (OperatorObserverBase_2_t3939730909 *)__this);
			IL2CPP_END_FINALLY(32)
		} // end finally (depth: 2)
		IL2CPP_CLEANUP(32)
		{
			IL2CPP_JUMP_TBL(0x27, IL_0027)
			IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
		}

IL_0027:
		{
			IL2CPP_LEAVE(0x33, FINALLY_002c);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_002c;
	}

FINALLY_002c:
	{ // begin finally (depth: 1)
		Il2CppObject * L_4 = V_0;
		Monitor_Exit_m2088237919(NULL /*static, unused*/, (Il2CppObject *)L_4, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(44)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(44)
	{
		IL2CPP_JUMP_TBL(0x33, IL_0033)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0033:
	{
		return;
	}
}
// System.Void UniRx.Operators.TakeObservable`1/Take_<System.Int64>::OnCompleted()
extern "C"  void Take__OnCompleted_m1361089962_gshared (Take__t183614399 * __this, const MethodInfo* method)
{
	Il2CppObject * V_0 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_gate_3();
		V_0 = (Il2CppObject *)L_0;
		Il2CppObject * L_1 = V_0;
		Monitor_Enter_m476686225(NULL /*static, unused*/, (Il2CppObject *)L_1, /*hidden argument*/NULL);
	}

IL_000d:
	try
	{ // begin try (depth: 1)
		try
		{ // begin try (depth: 2)
			Il2CppObject* L_2 = (Il2CppObject*)((OperatorObserverBase_2_t3939730909 *)__this)->get_observer_0();
			il2cpp_codegen_memory_barrier();
			NullCheck((Il2CppObject*)L_2);
			InterfaceActionInvoker0::Invoke(0 /* System.Void UniRx.IObserver`1<System.Int64>::OnCompleted() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4), (Il2CppObject*)L_2);
			IL2CPP_LEAVE(0x26, FINALLY_001f);
		} // end try (depth: 2)
		catch(Il2CppExceptionWrapper& e)
		{
			__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
			goto FINALLY_001f;
		}

FINALLY_001f:
		{ // begin finally (depth: 2)
			NullCheck((OperatorObserverBase_2_t3939730909 *)__this);
			VirtActionInvoker0::Invoke(4 /* System.Void UniRx.Operators.OperatorObserverBase`2<System.Int64,System.Int64>::Dispose() */, (OperatorObserverBase_2_t3939730909 *)__this);
			IL2CPP_END_FINALLY(31)
		} // end finally (depth: 2)
		IL2CPP_CLEANUP(31)
		{
			IL2CPP_JUMP_TBL(0x26, IL_0026)
			IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
		}

IL_0026:
		{
			IL2CPP_LEAVE(0x32, FINALLY_002b);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_002b;
	}

FINALLY_002b:
	{ // begin finally (depth: 1)
		Il2CppObject * L_3 = V_0;
		Monitor_Exit_m2088237919(NULL /*static, unused*/, (Il2CppObject *)L_3, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(43)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(43)
	{
		IL2CPP_JUMP_TBL(0x32, IL_0032)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0032:
	{
		return;
	}
}
// System.Void UniRx.Operators.TakeObservable`1/Take_<System.Object>::.ctor(UniRx.Operators.TakeObservable`1<T>,UniRx.IObserver`1<T>,System.IDisposable)
extern Il2CppClass* Il2CppObject_il2cpp_TypeInfo_var;
extern const uint32_t Take___ctor_m138741260_MetadataUsageId;
extern "C"  void Take___ctor_m138741260_gshared (Take__t2468273233 * __this, TakeObservable_1_t3078819489 * ___parent0, Il2CppObject* ___observer1, Il2CppObject * ___cancel2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Take___ctor_m138741260_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject * L_0 = (Il2CppObject *)il2cpp_codegen_object_new(Il2CppObject_il2cpp_TypeInfo_var);
		Object__ctor_m1772956182(L_0, /*hidden argument*/NULL);
		__this->set_gate_3(L_0);
		Il2CppObject* L_1 = ___observer1;
		Il2CppObject * L_2 = ___cancel2;
		NullCheck((OperatorObserverBase_2_t1187768149 *)__this);
		((  void (*) (OperatorObserverBase_2_t1187768149 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((OperatorObserverBase_2_t1187768149 *)__this, (Il2CppObject*)L_1, (Il2CppObject *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		TakeObservable_1_t3078819489 * L_3 = ___parent0;
		__this->set_parent_2(L_3);
		return;
	}
}
// System.IDisposable UniRx.Operators.TakeObservable`1/Take_<System.Object>::Run()
extern Il2CppClass* Action_t437523947_il2cpp_TypeInfo_var;
extern Il2CppClass* IScheduler_t2938318244_il2cpp_TypeInfo_var;
extern const uint32_t Take__Run_m3277526330_MetadataUsageId;
extern "C"  Il2CppObject * Take__Run_m3277526330_gshared (Take__t2468273233 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Take__Run_m3277526330_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Il2CppObject * V_0 = NULL;
	Il2CppObject * V_1 = NULL;
	{
		TakeObservable_1_t3078819489 * L_0 = (TakeObservable_1_t3078819489 *)__this->get_parent_2();
		NullCheck(L_0);
		Il2CppObject * L_1 = (Il2CppObject *)L_0->get_scheduler_4();
		TakeObservable_1_t3078819489 * L_2 = (TakeObservable_1_t3078819489 *)__this->get_parent_2();
		NullCheck(L_2);
		TimeSpan_t763862892  L_3 = (TimeSpan_t763862892 )L_2->get_duration_3();
		IntPtr_t L_4;
		L_4.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		Action_t437523947 * L_5 = (Action_t437523947 *)il2cpp_codegen_object_new(Action_t437523947_il2cpp_TypeInfo_var);
		Action__ctor_m2957240604(L_5, (Il2CppObject *)__this, (IntPtr_t)L_4, /*hidden argument*/NULL);
		NullCheck((Il2CppObject *)L_1);
		Il2CppObject * L_6 = InterfaceFuncInvoker2< Il2CppObject *, TimeSpan_t763862892 , Action_t437523947 * >::Invoke(2 /* System.IDisposable UniRx.IScheduler::Schedule(System.TimeSpan,System.Action) */, IScheduler_t2938318244_il2cpp_TypeInfo_var, (Il2CppObject *)L_1, (TimeSpan_t763862892 )L_3, (Action_t437523947 *)L_5);
		V_0 = (Il2CppObject *)L_6;
		TakeObservable_1_t3078819489 * L_7 = (TakeObservable_1_t3078819489 *)__this->get_parent_2();
		NullCheck(L_7);
		Il2CppObject* L_8 = (Il2CppObject*)L_7->get_source_1();
		NullCheck((Il2CppObject*)L_8);
		Il2CppObject * L_9 = InterfaceFuncInvoker1< Il2CppObject *, Il2CppObject* >::Invoke(0 /* System.IDisposable UniRx.IObservable`1<System.Object>::Subscribe(UniRx.IObserver`1<T>) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3), (Il2CppObject*)L_8, (Il2CppObject*)__this);
		V_1 = (Il2CppObject *)L_9;
		Il2CppObject * L_10 = V_0;
		Il2CppObject * L_11 = V_1;
		Il2CppObject * L_12 = StableCompositeDisposable_Create_m3764788535(NULL /*static, unused*/, (Il2CppObject *)L_10, (Il2CppObject *)L_11, /*hidden argument*/NULL);
		return L_12;
	}
}
// System.Void UniRx.Operators.TakeObservable`1/Take_<System.Object>::Tick()
extern "C"  void Take__Tick_m810389003_gshared (Take__t2468273233 * __this, const MethodInfo* method)
{
	Il2CppObject * V_0 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_gate_3();
		V_0 = (Il2CppObject *)L_0;
		Il2CppObject * L_1 = V_0;
		Monitor_Enter_m476686225(NULL /*static, unused*/, (Il2CppObject *)L_1, /*hidden argument*/NULL);
	}

IL_000d:
	try
	{ // begin try (depth: 1)
		try
		{ // begin try (depth: 2)
			Il2CppObject* L_2 = (Il2CppObject*)((OperatorObserverBase_2_t1187768149 *)__this)->get_observer_0();
			il2cpp_codegen_memory_barrier();
			NullCheck((Il2CppObject*)L_2);
			InterfaceActionInvoker0::Invoke(0 /* System.Void UniRx.IObserver`1<System.Object>::OnCompleted() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4), (Il2CppObject*)L_2);
			IL2CPP_LEAVE(0x26, FINALLY_001f);
		} // end try (depth: 2)
		catch(Il2CppExceptionWrapper& e)
		{
			__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
			goto FINALLY_001f;
		}

FINALLY_001f:
		{ // begin finally (depth: 2)
			NullCheck((OperatorObserverBase_2_t1187768149 *)__this);
			VirtActionInvoker0::Invoke(4 /* System.Void UniRx.Operators.OperatorObserverBase`2<System.Object,System.Object>::Dispose() */, (OperatorObserverBase_2_t1187768149 *)__this);
			IL2CPP_END_FINALLY(31)
		} // end finally (depth: 2)
		IL2CPP_CLEANUP(31)
		{
			IL2CPP_JUMP_TBL(0x26, IL_0026)
			IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
		}

IL_0026:
		{
			IL2CPP_LEAVE(0x32, FINALLY_002b);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_002b;
	}

FINALLY_002b:
	{ // begin finally (depth: 1)
		Il2CppObject * L_3 = V_0;
		Monitor_Exit_m2088237919(NULL /*static, unused*/, (Il2CppObject *)L_3, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(43)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(43)
	{
		IL2CPP_JUMP_TBL(0x32, IL_0032)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0032:
	{
		return;
	}
}
// System.Void UniRx.Operators.TakeObservable`1/Take_<System.Object>::OnNext(T)
extern "C"  void Take__OnNext_m3600713726_gshared (Take__t2468273233 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	Il2CppObject * V_0 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_gate_3();
		V_0 = (Il2CppObject *)L_0;
		Il2CppObject * L_1 = V_0;
		Monitor_Enter_m476686225(NULL /*static, unused*/, (Il2CppObject *)L_1, /*hidden argument*/NULL);
	}

IL_000d:
	try
	{ // begin try (depth: 1)
		Il2CppObject* L_2 = (Il2CppObject*)((OperatorObserverBase_2_t1187768149 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		Il2CppObject * L_3 = ___value0;
		NullCheck((Il2CppObject*)L_2);
		InterfaceActionInvoker1< Il2CppObject * >::Invoke(2 /* System.Void UniRx.IObserver`1<System.Object>::OnNext(T) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4), (Il2CppObject*)L_2, (Il2CppObject *)L_3);
		IL2CPP_LEAVE(0x27, FINALLY_0020);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0020;
	}

FINALLY_0020:
	{ // begin finally (depth: 1)
		Il2CppObject * L_4 = V_0;
		Monitor_Exit_m2088237919(NULL /*static, unused*/, (Il2CppObject *)L_4, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(32)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(32)
	{
		IL2CPP_JUMP_TBL(0x27, IL_0027)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0027:
	{
		return;
	}
}
// System.Void UniRx.Operators.TakeObservable`1/Take_<System.Object>::OnError(System.Exception)
extern "C"  void Take__OnError_m1652483853_gshared (Take__t2468273233 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method)
{
	Il2CppObject * V_0 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_gate_3();
		V_0 = (Il2CppObject *)L_0;
		Il2CppObject * L_1 = V_0;
		Monitor_Enter_m476686225(NULL /*static, unused*/, (Il2CppObject *)L_1, /*hidden argument*/NULL);
	}

IL_000d:
	try
	{ // begin try (depth: 1)
		try
		{ // begin try (depth: 2)
			Il2CppObject* L_2 = (Il2CppObject*)((OperatorObserverBase_2_t1187768149 *)__this)->get_observer_0();
			il2cpp_codegen_memory_barrier();
			Exception_t1967233988 * L_3 = ___error0;
			NullCheck((Il2CppObject*)L_2);
			InterfaceActionInvoker1< Exception_t1967233988 * >::Invoke(1 /* System.Void UniRx.IObserver`1<System.Object>::OnError(System.Exception) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4), (Il2CppObject*)L_2, (Exception_t1967233988 *)L_3);
			IL2CPP_LEAVE(0x27, FINALLY_0020);
		} // end try (depth: 2)
		catch(Il2CppExceptionWrapper& e)
		{
			__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
			goto FINALLY_0020;
		}

FINALLY_0020:
		{ // begin finally (depth: 2)
			NullCheck((OperatorObserverBase_2_t1187768149 *)__this);
			VirtActionInvoker0::Invoke(4 /* System.Void UniRx.Operators.OperatorObserverBase`2<System.Object,System.Object>::Dispose() */, (OperatorObserverBase_2_t1187768149 *)__this);
			IL2CPP_END_FINALLY(32)
		} // end finally (depth: 2)
		IL2CPP_CLEANUP(32)
		{
			IL2CPP_JUMP_TBL(0x27, IL_0027)
			IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
		}

IL_0027:
		{
			IL2CPP_LEAVE(0x33, FINALLY_002c);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_002c;
	}

FINALLY_002c:
	{ // begin finally (depth: 1)
		Il2CppObject * L_4 = V_0;
		Monitor_Exit_m2088237919(NULL /*static, unused*/, (Il2CppObject *)L_4, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(44)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(44)
	{
		IL2CPP_JUMP_TBL(0x33, IL_0033)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0033:
	{
		return;
	}
}
// System.Void UniRx.Operators.TakeObservable`1/Take_<System.Object>::OnCompleted()
extern "C"  void Take__OnCompleted_m688784864_gshared (Take__t2468273233 * __this, const MethodInfo* method)
{
	Il2CppObject * V_0 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_gate_3();
		V_0 = (Il2CppObject *)L_0;
		Il2CppObject * L_1 = V_0;
		Monitor_Enter_m476686225(NULL /*static, unused*/, (Il2CppObject *)L_1, /*hidden argument*/NULL);
	}

IL_000d:
	try
	{ // begin try (depth: 1)
		try
		{ // begin try (depth: 2)
			Il2CppObject* L_2 = (Il2CppObject*)((OperatorObserverBase_2_t1187768149 *)__this)->get_observer_0();
			il2cpp_codegen_memory_barrier();
			NullCheck((Il2CppObject*)L_2);
			InterfaceActionInvoker0::Invoke(0 /* System.Void UniRx.IObserver`1<System.Object>::OnCompleted() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4), (Il2CppObject*)L_2);
			IL2CPP_LEAVE(0x26, FINALLY_001f);
		} // end try (depth: 2)
		catch(Il2CppExceptionWrapper& e)
		{
			__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
			goto FINALLY_001f;
		}

FINALLY_001f:
		{ // begin finally (depth: 2)
			NullCheck((OperatorObserverBase_2_t1187768149 *)__this);
			VirtActionInvoker0::Invoke(4 /* System.Void UniRx.Operators.OperatorObserverBase`2<System.Object,System.Object>::Dispose() */, (OperatorObserverBase_2_t1187768149 *)__this);
			IL2CPP_END_FINALLY(31)
		} // end finally (depth: 2)
		IL2CPP_CLEANUP(31)
		{
			IL2CPP_JUMP_TBL(0x26, IL_0026)
			IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
		}

IL_0026:
		{
			IL2CPP_LEAVE(0x32, FINALLY_002b);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_002b;
	}

FINALLY_002b:
	{ // begin finally (depth: 1)
		Il2CppObject * L_3 = V_0;
		Monitor_Exit_m2088237919(NULL /*static, unused*/, (Il2CppObject *)L_3, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(43)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(43)
	{
		IL2CPP_JUMP_TBL(0x32, IL_0032)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0032:
	{
		return;
	}
}
// System.Void UniRx.Operators.TakeObservable`1/Take_<UniRx.Unit>::.ctor(UniRx.Operators.TakeObservable`1<T>,UniRx.IObserver`1<T>,System.IDisposable)
extern Il2CppClass* Il2CppObject_il2cpp_TypeInfo_var;
extern const uint32_t Take___ctor_m3614515988_MetadataUsageId;
extern "C"  void Take___ctor_m3614515988_gshared (Take__t4189452851 * __this, TakeObservable_1_t505031811 * ___parent0, Il2CppObject* ___observer1, Il2CppObject * ___cancel2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Take___ctor_m3614515988_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject * L_0 = (Il2CppObject *)il2cpp_codegen_object_new(Il2CppObject_il2cpp_TypeInfo_var);
		Object__ctor_m1772956182(L_0, /*hidden argument*/NULL);
		__this->set_gate_3(L_0);
		Il2CppObject* L_1 = ___observer1;
		Il2CppObject * L_2 = ___cancel2;
		NullCheck((OperatorObserverBase_2_t4165376397 *)__this);
		((  void (*) (OperatorObserverBase_2_t4165376397 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((OperatorObserverBase_2_t4165376397 *)__this, (Il2CppObject*)L_1, (Il2CppObject *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		TakeObservable_1_t505031811 * L_3 = ___parent0;
		__this->set_parent_2(L_3);
		return;
	}
}
// System.IDisposable UniRx.Operators.TakeObservable`1/Take_<UniRx.Unit>::Run()
extern Il2CppClass* Action_t437523947_il2cpp_TypeInfo_var;
extern Il2CppClass* IScheduler_t2938318244_il2cpp_TypeInfo_var;
extern const uint32_t Take__Run_m1446796188_MetadataUsageId;
extern "C"  Il2CppObject * Take__Run_m1446796188_gshared (Take__t4189452851 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Take__Run_m1446796188_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Il2CppObject * V_0 = NULL;
	Il2CppObject * V_1 = NULL;
	{
		TakeObservable_1_t505031811 * L_0 = (TakeObservable_1_t505031811 *)__this->get_parent_2();
		NullCheck(L_0);
		Il2CppObject * L_1 = (Il2CppObject *)L_0->get_scheduler_4();
		TakeObservable_1_t505031811 * L_2 = (TakeObservable_1_t505031811 *)__this->get_parent_2();
		NullCheck(L_2);
		TimeSpan_t763862892  L_3 = (TimeSpan_t763862892 )L_2->get_duration_3();
		IntPtr_t L_4;
		L_4.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		Action_t437523947 * L_5 = (Action_t437523947 *)il2cpp_codegen_object_new(Action_t437523947_il2cpp_TypeInfo_var);
		Action__ctor_m2957240604(L_5, (Il2CppObject *)__this, (IntPtr_t)L_4, /*hidden argument*/NULL);
		NullCheck((Il2CppObject *)L_1);
		Il2CppObject * L_6 = InterfaceFuncInvoker2< Il2CppObject *, TimeSpan_t763862892 , Action_t437523947 * >::Invoke(2 /* System.IDisposable UniRx.IScheduler::Schedule(System.TimeSpan,System.Action) */, IScheduler_t2938318244_il2cpp_TypeInfo_var, (Il2CppObject *)L_1, (TimeSpan_t763862892 )L_3, (Action_t437523947 *)L_5);
		V_0 = (Il2CppObject *)L_6;
		TakeObservable_1_t505031811 * L_7 = (TakeObservable_1_t505031811 *)__this->get_parent_2();
		NullCheck(L_7);
		Il2CppObject* L_8 = (Il2CppObject*)L_7->get_source_1();
		NullCheck((Il2CppObject*)L_8);
		Il2CppObject * L_9 = InterfaceFuncInvoker1< Il2CppObject *, Il2CppObject* >::Invoke(0 /* System.IDisposable UniRx.IObservable`1<UniRx.Unit>::Subscribe(UniRx.IObserver`1<T>) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3), (Il2CppObject*)L_8, (Il2CppObject*)__this);
		V_1 = (Il2CppObject *)L_9;
		Il2CppObject * L_10 = V_0;
		Il2CppObject * L_11 = V_1;
		Il2CppObject * L_12 = StableCompositeDisposable_Create_m3764788535(NULL /*static, unused*/, (Il2CppObject *)L_10, (Il2CppObject *)L_11, /*hidden argument*/NULL);
		return L_12;
	}
}
// System.Void UniRx.Operators.TakeObservable`1/Take_<UniRx.Unit>::Tick()
extern "C"  void Take__Tick_m922957587_gshared (Take__t4189452851 * __this, const MethodInfo* method)
{
	Il2CppObject * V_0 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_gate_3();
		V_0 = (Il2CppObject *)L_0;
		Il2CppObject * L_1 = V_0;
		Monitor_Enter_m476686225(NULL /*static, unused*/, (Il2CppObject *)L_1, /*hidden argument*/NULL);
	}

IL_000d:
	try
	{ // begin try (depth: 1)
		try
		{ // begin try (depth: 2)
			Il2CppObject* L_2 = (Il2CppObject*)((OperatorObserverBase_2_t4165376397 *)__this)->get_observer_0();
			il2cpp_codegen_memory_barrier();
			NullCheck((Il2CppObject*)L_2);
			InterfaceActionInvoker0::Invoke(0 /* System.Void UniRx.IObserver`1<UniRx.Unit>::OnCompleted() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4), (Il2CppObject*)L_2);
			IL2CPP_LEAVE(0x26, FINALLY_001f);
		} // end try (depth: 2)
		catch(Il2CppExceptionWrapper& e)
		{
			__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
			goto FINALLY_001f;
		}

FINALLY_001f:
		{ // begin finally (depth: 2)
			NullCheck((OperatorObserverBase_2_t4165376397 *)__this);
			VirtActionInvoker0::Invoke(4 /* System.Void UniRx.Operators.OperatorObserverBase`2<UniRx.Unit,UniRx.Unit>::Dispose() */, (OperatorObserverBase_2_t4165376397 *)__this);
			IL2CPP_END_FINALLY(31)
		} // end finally (depth: 2)
		IL2CPP_CLEANUP(31)
		{
			IL2CPP_JUMP_TBL(0x26, IL_0026)
			IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
		}

IL_0026:
		{
			IL2CPP_LEAVE(0x32, FINALLY_002b);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_002b;
	}

FINALLY_002b:
	{ // begin finally (depth: 1)
		Il2CppObject * L_3 = V_0;
		Monitor_Exit_m2088237919(NULL /*static, unused*/, (Il2CppObject *)L_3, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(43)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(43)
	{
		IL2CPP_JUMP_TBL(0x32, IL_0032)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0032:
	{
		return;
	}
}
// System.Void UniRx.Operators.TakeObservable`1/Take_<UniRx.Unit>::OnNext(T)
extern "C"  void Take__OnNext_m2761941494_gshared (Take__t4189452851 * __this, Unit_t2558286038  ___value0, const MethodInfo* method)
{
	Il2CppObject * V_0 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_gate_3();
		V_0 = (Il2CppObject *)L_0;
		Il2CppObject * L_1 = V_0;
		Monitor_Enter_m476686225(NULL /*static, unused*/, (Il2CppObject *)L_1, /*hidden argument*/NULL);
	}

IL_000d:
	try
	{ // begin try (depth: 1)
		Il2CppObject* L_2 = (Il2CppObject*)((OperatorObserverBase_2_t4165376397 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		Unit_t2558286038  L_3 = ___value0;
		NullCheck((Il2CppObject*)L_2);
		InterfaceActionInvoker1< Unit_t2558286038  >::Invoke(2 /* System.Void UniRx.IObserver`1<UniRx.Unit>::OnNext(T) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4), (Il2CppObject*)L_2, (Unit_t2558286038 )L_3);
		IL2CPP_LEAVE(0x27, FINALLY_0020);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0020;
	}

FINALLY_0020:
	{ // begin finally (depth: 1)
		Il2CppObject * L_4 = V_0;
		Monitor_Exit_m2088237919(NULL /*static, unused*/, (Il2CppObject *)L_4, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(32)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(32)
	{
		IL2CPP_JUMP_TBL(0x27, IL_0027)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0027:
	{
		return;
	}
}
// System.Void UniRx.Operators.TakeObservable`1/Take_<UniRx.Unit>::OnError(System.Exception)
extern "C"  void Take__OnError_m1094537477_gshared (Take__t4189452851 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method)
{
	Il2CppObject * V_0 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_gate_3();
		V_0 = (Il2CppObject *)L_0;
		Il2CppObject * L_1 = V_0;
		Monitor_Enter_m476686225(NULL /*static, unused*/, (Il2CppObject *)L_1, /*hidden argument*/NULL);
	}

IL_000d:
	try
	{ // begin try (depth: 1)
		try
		{ // begin try (depth: 2)
			Il2CppObject* L_2 = (Il2CppObject*)((OperatorObserverBase_2_t4165376397 *)__this)->get_observer_0();
			il2cpp_codegen_memory_barrier();
			Exception_t1967233988 * L_3 = ___error0;
			NullCheck((Il2CppObject*)L_2);
			InterfaceActionInvoker1< Exception_t1967233988 * >::Invoke(1 /* System.Void UniRx.IObserver`1<UniRx.Unit>::OnError(System.Exception) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4), (Il2CppObject*)L_2, (Exception_t1967233988 *)L_3);
			IL2CPP_LEAVE(0x27, FINALLY_0020);
		} // end try (depth: 2)
		catch(Il2CppExceptionWrapper& e)
		{
			__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
			goto FINALLY_0020;
		}

FINALLY_0020:
		{ // begin finally (depth: 2)
			NullCheck((OperatorObserverBase_2_t4165376397 *)__this);
			VirtActionInvoker0::Invoke(4 /* System.Void UniRx.Operators.OperatorObserverBase`2<UniRx.Unit,UniRx.Unit>::Dispose() */, (OperatorObserverBase_2_t4165376397 *)__this);
			IL2CPP_END_FINALLY(32)
		} // end finally (depth: 2)
		IL2CPP_CLEANUP(32)
		{
			IL2CPP_JUMP_TBL(0x27, IL_0027)
			IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
		}

IL_0027:
		{
			IL2CPP_LEAVE(0x33, FINALLY_002c);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_002c;
	}

FINALLY_002c:
	{ // begin finally (depth: 1)
		Il2CppObject * L_4 = V_0;
		Monitor_Exit_m2088237919(NULL /*static, unused*/, (Il2CppObject *)L_4, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(44)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(44)
	{
		IL2CPP_JUMP_TBL(0x33, IL_0033)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0033:
	{
		return;
	}
}
// System.Void UniRx.Operators.TakeObservable`1/Take_<UniRx.Unit>::OnCompleted()
extern "C"  void Take__OnCompleted_m39953368_gshared (Take__t4189452851 * __this, const MethodInfo* method)
{
	Il2CppObject * V_0 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_gate_3();
		V_0 = (Il2CppObject *)L_0;
		Il2CppObject * L_1 = V_0;
		Monitor_Enter_m476686225(NULL /*static, unused*/, (Il2CppObject *)L_1, /*hidden argument*/NULL);
	}

IL_000d:
	try
	{ // begin try (depth: 1)
		try
		{ // begin try (depth: 2)
			Il2CppObject* L_2 = (Il2CppObject*)((OperatorObserverBase_2_t4165376397 *)__this)->get_observer_0();
			il2cpp_codegen_memory_barrier();
			NullCheck((Il2CppObject*)L_2);
			InterfaceActionInvoker0::Invoke(0 /* System.Void UniRx.IObserver`1<UniRx.Unit>::OnCompleted() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4), (Il2CppObject*)L_2);
			IL2CPP_LEAVE(0x26, FINALLY_001f);
		} // end try (depth: 2)
		catch(Il2CppExceptionWrapper& e)
		{
			__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
			goto FINALLY_001f;
		}

FINALLY_001f:
		{ // begin finally (depth: 2)
			NullCheck((OperatorObserverBase_2_t4165376397 *)__this);
			VirtActionInvoker0::Invoke(4 /* System.Void UniRx.Operators.OperatorObserverBase`2<UniRx.Unit,UniRx.Unit>::Dispose() */, (OperatorObserverBase_2_t4165376397 *)__this);
			IL2CPP_END_FINALLY(31)
		} // end finally (depth: 2)
		IL2CPP_CLEANUP(31)
		{
			IL2CPP_JUMP_TBL(0x26, IL_0026)
			IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
		}

IL_0026:
		{
			IL2CPP_LEAVE(0x32, FINALLY_002b);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_002b;
	}

FINALLY_002b:
	{ // begin finally (depth: 1)
		Il2CppObject * L_3 = V_0;
		Monitor_Exit_m2088237919(NULL /*static, unused*/, (Il2CppObject *)L_3, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(43)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(43)
	{
		IL2CPP_JUMP_TBL(0x32, IL_0032)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0032:
	{
		return;
	}
}
// System.Void UniRx.Operators.TakeObservable`1/Take<System.Int64>::.ctor(UniRx.Operators.TakeObservable`1<T>,UniRx.IObserver`1<T>,System.IDisposable)
extern "C"  void Take__ctor_m1191812163_gshared (Take_t2541561334 * __this, TakeObservable_1_t794160655 * ___parent0, Il2CppObject* ___observer1, Il2CppObject * ___cancel2, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = ___observer1;
		Il2CppObject * L_1 = ___cancel2;
		NullCheck((OperatorObserverBase_2_t3939730909 *)__this);
		((  void (*) (OperatorObserverBase_2_t3939730909 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((OperatorObserverBase_2_t3939730909 *)__this, (Il2CppObject*)L_0, (Il2CppObject *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		TakeObservable_1_t794160655 * L_2 = ___parent0;
		NullCheck(L_2);
		int32_t L_3 = (int32_t)L_2->get_count_2();
		__this->set_rest_2(L_3);
		return;
	}
}
// System.Void UniRx.Operators.TakeObservable`1/Take<System.Int64>::OnNext(T)
extern "C"  void Take_OnNext_m3241971687_gshared (Take_t2541561334 * __this, int64_t ___value0, const MethodInfo* method)
{
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		int32_t L_0 = (int32_t)__this->get_rest_2();
		if ((((int32_t)L_0) <= ((int32_t)0)))
		{
			goto IL_004c;
		}
	}
	{
		int32_t L_1 = (int32_t)__this->get_rest_2();
		__this->set_rest_2(((int32_t)((int32_t)L_1-(int32_t)1)));
		Il2CppObject* L_2 = (Il2CppObject*)((OperatorObserverBase_2_t3939730909 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		int64_t L_3 = ___value0;
		NullCheck((Il2CppObject*)L_2);
		InterfaceActionInvoker1< int64_t >::Invoke(2 /* System.Void UniRx.IObserver`1<System.Int64>::OnNext(T) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), (Il2CppObject*)L_2, (int64_t)L_3);
		int32_t L_4 = (int32_t)__this->get_rest_2();
		if (L_4)
		{
			goto IL_004c;
		}
	}

IL_0033:
	try
	{ // begin try (depth: 1)
		Il2CppObject* L_5 = (Il2CppObject*)((OperatorObserverBase_2_t3939730909 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		NullCheck((Il2CppObject*)L_5);
		InterfaceActionInvoker0::Invoke(0 /* System.Void UniRx.IObserver`1<System.Int64>::OnCompleted() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), (Il2CppObject*)L_5);
		IL2CPP_LEAVE(0x4C, FINALLY_0045);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0045;
	}

FINALLY_0045:
	{ // begin finally (depth: 1)
		NullCheck((OperatorObserverBase_2_t3939730909 *)__this);
		VirtActionInvoker0::Invoke(4 /* System.Void UniRx.Operators.OperatorObserverBase`2<System.Int64,System.Int64>::Dispose() */, (OperatorObserverBase_2_t3939730909 *)__this);
		IL2CPP_END_FINALLY(69)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(69)
	{
		IL2CPP_JUMP_TBL(0x4C, IL_004c)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_004c:
	{
		return;
	}
}
// System.Void UniRx.Operators.TakeObservable`1/Take<System.Int64>::OnError(System.Exception)
extern "C"  void Take_OnError_m1623850230_gshared (Take_t2541561334 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method)
{
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		Il2CppObject* L_0 = (Il2CppObject*)((OperatorObserverBase_2_t3939730909 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		Exception_t1967233988 * L_1 = ___error0;
		NullCheck((Il2CppObject*)L_0);
		InterfaceActionInvoker1< Exception_t1967233988 * >::Invoke(1 /* System.Void UniRx.IObserver`1<System.Int64>::OnError(System.Exception) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), (Il2CppObject*)L_0, (Exception_t1967233988 *)L_1);
		IL2CPP_LEAVE(0x1A, FINALLY_0013);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0013;
	}

FINALLY_0013:
	{ // begin finally (depth: 1)
		NullCheck((OperatorObserverBase_2_t3939730909 *)__this);
		VirtActionInvoker0::Invoke(4 /* System.Void UniRx.Operators.OperatorObserverBase`2<System.Int64,System.Int64>::Dispose() */, (OperatorObserverBase_2_t3939730909 *)__this);
		IL2CPP_END_FINALLY(19)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(19)
	{
		IL2CPP_JUMP_TBL(0x1A, IL_001a)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_001a:
	{
		return;
	}
}
// System.Void UniRx.Operators.TakeObservable`1/Take<System.Int64>::OnCompleted()
extern "C"  void Take_OnCompleted_m69464393_gshared (Take_t2541561334 * __this, const MethodInfo* method)
{
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		Il2CppObject* L_0 = (Il2CppObject*)((OperatorObserverBase_2_t3939730909 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		NullCheck((Il2CppObject*)L_0);
		InterfaceActionInvoker0::Invoke(0 /* System.Void UniRx.IObserver`1<System.Int64>::OnCompleted() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), (Il2CppObject*)L_0);
		IL2CPP_LEAVE(0x19, FINALLY_0012);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0012;
	}

FINALLY_0012:
	{ // begin finally (depth: 1)
		NullCheck((OperatorObserverBase_2_t3939730909 *)__this);
		VirtActionInvoker0::Invoke(4 /* System.Void UniRx.Operators.OperatorObserverBase`2<System.Int64,System.Int64>::Dispose() */, (OperatorObserverBase_2_t3939730909 *)__this);
		IL2CPP_END_FINALLY(18)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(18)
	{
		IL2CPP_JUMP_TBL(0x19, IL_0019)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0019:
	{
		return;
	}
}
// System.Void UniRx.Operators.TakeObservable`1/Take<System.Object>::.ctor(UniRx.Operators.TakeObservable`1<T>,UniRx.IObserver`1<T>,System.IDisposable)
extern "C"  void Take__ctor_m904164971_gshared (Take_t531252872 * __this, TakeObservable_1_t3078819489 * ___parent0, Il2CppObject* ___observer1, Il2CppObject * ___cancel2, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = ___observer1;
		Il2CppObject * L_1 = ___cancel2;
		NullCheck((OperatorObserverBase_2_t1187768149 *)__this);
		((  void (*) (OperatorObserverBase_2_t1187768149 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((OperatorObserverBase_2_t1187768149 *)__this, (Il2CppObject*)L_0, (Il2CppObject *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		TakeObservable_1_t3078819489 * L_2 = ___parent0;
		NullCheck(L_2);
		int32_t L_3 = (int32_t)L_2->get_count_2();
		__this->set_rest_2(L_3);
		return;
	}
}
// System.Void UniRx.Operators.TakeObservable`1/Take<System.Object>::OnNext(T)
extern "C"  void Take_OnNext_m3969312959_gshared (Take_t531252872 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		int32_t L_0 = (int32_t)__this->get_rest_2();
		if ((((int32_t)L_0) <= ((int32_t)0)))
		{
			goto IL_004c;
		}
	}
	{
		int32_t L_1 = (int32_t)__this->get_rest_2();
		__this->set_rest_2(((int32_t)((int32_t)L_1-(int32_t)1)));
		Il2CppObject* L_2 = (Il2CppObject*)((OperatorObserverBase_2_t1187768149 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		Il2CppObject * L_3 = ___value0;
		NullCheck((Il2CppObject*)L_2);
		InterfaceActionInvoker1< Il2CppObject * >::Invoke(2 /* System.Void UniRx.IObserver`1<System.Object>::OnNext(T) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), (Il2CppObject*)L_2, (Il2CppObject *)L_3);
		int32_t L_4 = (int32_t)__this->get_rest_2();
		if (L_4)
		{
			goto IL_004c;
		}
	}

IL_0033:
	try
	{ // begin try (depth: 1)
		Il2CppObject* L_5 = (Il2CppObject*)((OperatorObserverBase_2_t1187768149 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		NullCheck((Il2CppObject*)L_5);
		InterfaceActionInvoker0::Invoke(0 /* System.Void UniRx.IObserver`1<System.Object>::OnCompleted() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), (Il2CppObject*)L_5);
		IL2CPP_LEAVE(0x4C, FINALLY_0045);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0045;
	}

FINALLY_0045:
	{ // begin finally (depth: 1)
		NullCheck((OperatorObserverBase_2_t1187768149 *)__this);
		VirtActionInvoker0::Invoke(4 /* System.Void UniRx.Operators.OperatorObserverBase`2<System.Object,System.Object>::Dispose() */, (OperatorObserverBase_2_t1187768149 *)__this);
		IL2CPP_END_FINALLY(69)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(69)
	{
		IL2CPP_JUMP_TBL(0x4C, IL_004c)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_004c:
	{
		return;
	}
}
// System.Void UniRx.Operators.TakeObservable`1/Take<System.Object>::OnError(System.Exception)
extern "C"  void Take_OnError_m197633486_gshared (Take_t531252872 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method)
{
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		Il2CppObject* L_0 = (Il2CppObject*)((OperatorObserverBase_2_t1187768149 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		Exception_t1967233988 * L_1 = ___error0;
		NullCheck((Il2CppObject*)L_0);
		InterfaceActionInvoker1< Exception_t1967233988 * >::Invoke(1 /* System.Void UniRx.IObserver`1<System.Object>::OnError(System.Exception) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), (Il2CppObject*)L_0, (Exception_t1967233988 *)L_1);
		IL2CPP_LEAVE(0x1A, FINALLY_0013);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0013;
	}

FINALLY_0013:
	{ // begin finally (depth: 1)
		NullCheck((OperatorObserverBase_2_t1187768149 *)__this);
		VirtActionInvoker0::Invoke(4 /* System.Void UniRx.Operators.OperatorObserverBase`2<System.Object,System.Object>::Dispose() */, (OperatorObserverBase_2_t1187768149 *)__this);
		IL2CPP_END_FINALLY(19)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(19)
	{
		IL2CPP_JUMP_TBL(0x1A, IL_001a)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_001a:
	{
		return;
	}
}
// System.Void UniRx.Operators.TakeObservable`1/Take<System.Object>::OnCompleted()
extern "C"  void Take_OnCompleted_m3598065185_gshared (Take_t531252872 * __this, const MethodInfo* method)
{
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		Il2CppObject* L_0 = (Il2CppObject*)((OperatorObserverBase_2_t1187768149 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		NullCheck((Il2CppObject*)L_0);
		InterfaceActionInvoker0::Invoke(0 /* System.Void UniRx.IObserver`1<System.Object>::OnCompleted() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), (Il2CppObject*)L_0);
		IL2CPP_LEAVE(0x19, FINALLY_0012);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0012;
	}

FINALLY_0012:
	{ // begin finally (depth: 1)
		NullCheck((OperatorObserverBase_2_t1187768149 *)__this);
		VirtActionInvoker0::Invoke(4 /* System.Void UniRx.Operators.OperatorObserverBase`2<System.Object,System.Object>::Dispose() */, (OperatorObserverBase_2_t1187768149 *)__this);
		IL2CPP_END_FINALLY(18)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(18)
	{
		IL2CPP_JUMP_TBL(0x19, IL_0019)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0019:
	{
		return;
	}
}
// System.Void UniRx.Operators.TakeObservable`1/Take<UniRx.Unit>::.ctor(UniRx.Operators.TakeObservable`1<T>,UniRx.IObserver`1<T>,System.IDisposable)
extern "C"  void Take__ctor_m2056496917_gshared (Take_t2252432490 * __this, TakeObservable_1_t505031811 * ___parent0, Il2CppObject* ___observer1, Il2CppObject * ___cancel2, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = ___observer1;
		Il2CppObject * L_1 = ___cancel2;
		NullCheck((OperatorObserverBase_2_t4165376397 *)__this);
		((  void (*) (OperatorObserverBase_2_t4165376397 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((OperatorObserverBase_2_t4165376397 *)__this, (Il2CppObject*)L_0, (Il2CppObject *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		TakeObservable_1_t505031811 * L_2 = ___parent0;
		NullCheck(L_2);
		int32_t L_3 = (int32_t)L_2->get_count_2();
		__this->set_rest_2(L_3);
		return;
	}
}
// System.Void UniRx.Operators.TakeObservable`1/Take<UniRx.Unit>::OnNext(T)
extern "C"  void Take_OnNext_m3317440725_gshared (Take_t2252432490 * __this, Unit_t2558286038  ___value0, const MethodInfo* method)
{
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		int32_t L_0 = (int32_t)__this->get_rest_2();
		if ((((int32_t)L_0) <= ((int32_t)0)))
		{
			goto IL_004c;
		}
	}
	{
		int32_t L_1 = (int32_t)__this->get_rest_2();
		__this->set_rest_2(((int32_t)((int32_t)L_1-(int32_t)1)));
		Il2CppObject* L_2 = (Il2CppObject*)((OperatorObserverBase_2_t4165376397 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		Unit_t2558286038  L_3 = ___value0;
		NullCheck((Il2CppObject*)L_2);
		InterfaceActionInvoker1< Unit_t2558286038  >::Invoke(2 /* System.Void UniRx.IObserver`1<UniRx.Unit>::OnNext(T) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), (Il2CppObject*)L_2, (Unit_t2558286038 )L_3);
		int32_t L_4 = (int32_t)__this->get_rest_2();
		if (L_4)
		{
			goto IL_004c;
		}
	}

IL_0033:
	try
	{ // begin try (depth: 1)
		Il2CppObject* L_5 = (Il2CppObject*)((OperatorObserverBase_2_t4165376397 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		NullCheck((Il2CppObject*)L_5);
		InterfaceActionInvoker0::Invoke(0 /* System.Void UniRx.IObserver`1<UniRx.Unit>::OnCompleted() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), (Il2CppObject*)L_5);
		IL2CPP_LEAVE(0x4C, FINALLY_0045);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0045;
	}

FINALLY_0045:
	{ // begin finally (depth: 1)
		NullCheck((OperatorObserverBase_2_t4165376397 *)__this);
		VirtActionInvoker0::Invoke(4 /* System.Void UniRx.Operators.OperatorObserverBase`2<UniRx.Unit,UniRx.Unit>::Dispose() */, (OperatorObserverBase_2_t4165376397 *)__this);
		IL2CPP_END_FINALLY(69)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(69)
	{
		IL2CPP_JUMP_TBL(0x4C, IL_004c)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_004c:
	{
		return;
	}
}
// System.Void UniRx.Operators.TakeObservable`1/Take<UniRx.Unit>::OnError(System.Exception)
extern "C"  void Take_OnError_m303428068_gshared (Take_t2252432490 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method)
{
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		Il2CppObject* L_0 = (Il2CppObject*)((OperatorObserverBase_2_t4165376397 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		Exception_t1967233988 * L_1 = ___error0;
		NullCheck((Il2CppObject*)L_0);
		InterfaceActionInvoker1< Exception_t1967233988 * >::Invoke(1 /* System.Void UniRx.IObserver`1<UniRx.Unit>::OnError(System.Exception) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), (Il2CppObject*)L_0, (Exception_t1967233988 *)L_1);
		IL2CPP_LEAVE(0x1A, FINALLY_0013);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0013;
	}

FINALLY_0013:
	{ // begin finally (depth: 1)
		NullCheck((OperatorObserverBase_2_t4165376397 *)__this);
		VirtActionInvoker0::Invoke(4 /* System.Void UniRx.Operators.OperatorObserverBase`2<UniRx.Unit,UniRx.Unit>::Dispose() */, (OperatorObserverBase_2_t4165376397 *)__this);
		IL2CPP_END_FINALLY(19)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(19)
	{
		IL2CPP_JUMP_TBL(0x1A, IL_001a)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_001a:
	{
		return;
	}
}
// System.Void UniRx.Operators.TakeObservable`1/Take<UniRx.Unit>::OnCompleted()
extern "C"  void Take_OnCompleted_m2876594999_gshared (Take_t2252432490 * __this, const MethodInfo* method)
{
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		Il2CppObject* L_0 = (Il2CppObject*)((OperatorObserverBase_2_t4165376397 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		NullCheck((Il2CppObject*)L_0);
		InterfaceActionInvoker0::Invoke(0 /* System.Void UniRx.IObserver`1<UniRx.Unit>::OnCompleted() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), (Il2CppObject*)L_0);
		IL2CPP_LEAVE(0x19, FINALLY_0012);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0012;
	}

FINALLY_0012:
	{ // begin finally (depth: 1)
		NullCheck((OperatorObserverBase_2_t4165376397 *)__this);
		VirtActionInvoker0::Invoke(4 /* System.Void UniRx.Operators.OperatorObserverBase`2<UniRx.Unit,UniRx.Unit>::Dispose() */, (OperatorObserverBase_2_t4165376397 *)__this);
		IL2CPP_END_FINALLY(18)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(18)
	{
		IL2CPP_JUMP_TBL(0x19, IL_0019)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0019:
	{
		return;
	}
}
// System.Void UniRx.Operators.TakeObservable`1<System.Int64>::.ctor(UniRx.IObservable`1<T>,System.Int32)
extern "C"  void TakeObservable_1__ctor_m3160701743_gshared (TakeObservable_1_t794160655 * __this, Il2CppObject* ___source0, int32_t ___count1, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = ___source0;
		bool L_1 = ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)(NULL /*static, unused*/, (Il2CppObject*)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		NullCheck((OperatorObservableBase_1_t1911559853 *)__this);
		((  void (*) (OperatorObservableBase_1_t1911559853 *, bool, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)((OperatorObservableBase_1_t1911559853 *)__this, (bool)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		Il2CppObject* L_2 = ___source0;
		__this->set_source_1(L_2);
		int32_t L_3 = ___count1;
		__this->set_count_2(L_3);
		return;
	}
}
// System.Void UniRx.Operators.TakeObservable`1<System.Int64>::.ctor(UniRx.IObservable`1<T>,System.TimeSpan,UniRx.IScheduler)
extern Il2CppClass* Scheduler_t103934925_il2cpp_TypeInfo_var;
extern const uint32_t TakeObservable_1__ctor_m658868318_MetadataUsageId;
extern "C"  void TakeObservable_1__ctor_m658868318_gshared (TakeObservable_1_t794160655 * __this, Il2CppObject* ___source0, TimeSpan_t763862892  ___duration1, Il2CppObject * ___scheduler2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TakeObservable_1__ctor_m658868318_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	TakeObservable_1_t794160655 * G_B2_0 = NULL;
	TakeObservable_1_t794160655 * G_B1_0 = NULL;
	int32_t G_B3_0 = 0;
	TakeObservable_1_t794160655 * G_B3_1 = NULL;
	{
		Il2CppObject * L_0 = ___scheduler2;
		IL2CPP_RUNTIME_CLASS_INIT(Scheduler_t103934925_il2cpp_TypeInfo_var);
		Il2CppObject * L_1 = ((Scheduler_t103934925_StaticFields*)Scheduler_t103934925_il2cpp_TypeInfo_var->static_fields)->get_CurrentThread_0();
		G_B1_0 = ((TakeObservable_1_t794160655 *)(__this));
		if ((((Il2CppObject*)(Il2CppObject *)L_0) == ((Il2CppObject*)(Il2CppObject *)L_1)))
		{
			G_B2_0 = ((TakeObservable_1_t794160655 *)(__this));
			goto IL_0014;
		}
	}
	{
		Il2CppObject* L_2 = ___source0;
		bool L_3 = ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)(NULL /*static, unused*/, (Il2CppObject*)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		G_B3_0 = ((int32_t)(L_3));
		G_B3_1 = ((TakeObservable_1_t794160655 *)(G_B1_0));
		goto IL_0015;
	}

IL_0014:
	{
		G_B3_0 = 1;
		G_B3_1 = ((TakeObservable_1_t794160655 *)(G_B2_0));
	}

IL_0015:
	{
		NullCheck((OperatorObservableBase_1_t1911559853 *)G_B3_1);
		((  void (*) (OperatorObservableBase_1_t1911559853 *, bool, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)((OperatorObservableBase_1_t1911559853 *)G_B3_1, (bool)G_B3_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		Il2CppObject* L_4 = ___source0;
		__this->set_source_1(L_4);
		TimeSpan_t763862892  L_5 = ___duration1;
		__this->set_duration_3(L_5);
		Il2CppObject * L_6 = ___scheduler2;
		__this->set_scheduler_4(L_6);
		return;
	}
}
// UniRx.IObservable`1<T> UniRx.Operators.TakeObservable`1<System.Int64>::Combine(System.Int32)
extern "C"  Il2CppObject* TakeObservable_1_Combine_m2626963827_gshared (TakeObservable_1_t794160655 * __this, int32_t ___count0, const MethodInfo* method)
{
	TakeObservable_1_t794160655 * G_B3_0 = NULL;
	{
		int32_t L_0 = (int32_t)__this->get_count_2();
		int32_t L_1 = ___count0;
		if ((((int32_t)L_0) > ((int32_t)L_1)))
		{
			goto IL_0012;
		}
	}
	{
		G_B3_0 = ((TakeObservable_1_t794160655 *)(__this));
		goto IL_001e;
	}

IL_0012:
	{
		Il2CppObject* L_2 = (Il2CppObject*)__this->get_source_1();
		int32_t L_3 = ___count0;
		TakeObservable_1_t794160655 * L_4 = (TakeObservable_1_t794160655 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		((  void (*) (TakeObservable_1_t794160655 *, Il2CppObject*, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)(L_4, (Il2CppObject*)L_2, (int32_t)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		G_B3_0 = ((TakeObservable_1_t794160655 *)(L_4));
	}

IL_001e:
	{
		return G_B3_0;
	}
}
// UniRx.IObservable`1<T> UniRx.Operators.TakeObservable`1<System.Int64>::Combine(System.TimeSpan)
extern Il2CppClass* TimeSpan_t763862892_il2cpp_TypeInfo_var;
extern const uint32_t TakeObservable_1_Combine_m2295577146_MetadataUsageId;
extern "C"  Il2CppObject* TakeObservable_1_Combine_m2295577146_gshared (TakeObservable_1_t794160655 * __this, TimeSpan_t763862892  ___duration0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TakeObservable_1_Combine_m2295577146_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	TakeObservable_1_t794160655 * G_B3_0 = NULL;
	{
		TimeSpan_t763862892  L_0 = (TimeSpan_t763862892 )__this->get_duration_3();
		TimeSpan_t763862892  L_1 = ___duration0;
		IL2CPP_RUNTIME_CLASS_INIT(TimeSpan_t763862892_il2cpp_TypeInfo_var);
		bool L_2 = TimeSpan_op_LessThanOrEqual_m271837557(NULL /*static, unused*/, (TimeSpan_t763862892 )L_0, (TimeSpan_t763862892 )L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0017;
		}
	}
	{
		G_B3_0 = ((TakeObservable_1_t794160655 *)(__this));
		goto IL_0029;
	}

IL_0017:
	{
		Il2CppObject* L_3 = (Il2CppObject*)__this->get_source_1();
		TimeSpan_t763862892  L_4 = ___duration0;
		Il2CppObject * L_5 = (Il2CppObject *)__this->get_scheduler_4();
		TakeObservable_1_t794160655 * L_6 = (TakeObservable_1_t794160655 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		((  void (*) (TakeObservable_1_t794160655 *, Il2CppObject*, TimeSpan_t763862892 , Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->method)(L_6, (Il2CppObject*)L_3, (TimeSpan_t763862892 )L_4, (Il2CppObject *)L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		G_B3_0 = ((TakeObservable_1_t794160655 *)(L_6));
	}

IL_0029:
	{
		return G_B3_0;
	}
}
// System.IDisposable UniRx.Operators.TakeObservable`1<System.Int64>::SubscribeCore(UniRx.IObserver`1<T>,System.IDisposable)
extern "C"  Il2CppObject * TakeObservable_1_SubscribeCore_m2094046517_gshared (TakeObservable_1_t794160655 * __this, Il2CppObject* ___observer0, Il2CppObject * ___cancel1, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_scheduler_4();
		if (L_0)
		{
			goto IL_001f;
		}
	}
	{
		Il2CppObject* L_1 = (Il2CppObject*)__this->get_source_1();
		Il2CppObject* L_2 = ___observer0;
		Il2CppObject * L_3 = ___cancel1;
		Take_t2541561334 * L_4 = (Take_t2541561334 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		((  void (*) (Take_t2541561334 *, TakeObservable_1_t794160655 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7)->method)(L_4, (TakeObservable_1_t794160655 *)__this, (Il2CppObject*)L_2, (Il2CppObject *)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7));
		NullCheck((Il2CppObject*)L_1);
		Il2CppObject * L_5 = InterfaceFuncInvoker1< Il2CppObject *, Il2CppObject* >::Invoke(0 /* System.IDisposable UniRx.IObservable`1<System.Int64>::Subscribe(UniRx.IObserver`1<T>) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8), (Il2CppObject*)L_1, (Il2CppObject*)L_4);
		return L_5;
	}

IL_001f:
	{
		Il2CppObject* L_6 = ___observer0;
		Il2CppObject * L_7 = ___cancel1;
		Take__t183614399 * L_8 = (Take__t183614399 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9));
		((  void (*) (Take__t183614399 *, TakeObservable_1_t794160655 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10)->method)(L_8, (TakeObservable_1_t794160655 *)__this, (Il2CppObject*)L_6, (Il2CppObject *)L_7, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10));
		NullCheck((Take__t183614399 *)L_8);
		Il2CppObject * L_9 = ((  Il2CppObject * (*) (Take__t183614399 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 11)->method)((Take__t183614399 *)L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 11));
		return L_9;
	}
}
// System.Void UniRx.Operators.TakeObservable`1<System.Object>::.ctor(UniRx.IObservable`1<T>,System.Int32)
extern "C"  void TakeObservable_1__ctor_m3809140139_gshared (TakeObservable_1_t3078819489 * __this, Il2CppObject* ___source0, int32_t ___count1, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = ___source0;
		bool L_1 = ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)(NULL /*static, unused*/, (Il2CppObject*)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		NullCheck((OperatorObservableBase_1_t4196218687 *)__this);
		((  void (*) (OperatorObservableBase_1_t4196218687 *, bool, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)((OperatorObservableBase_1_t4196218687 *)__this, (bool)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		Il2CppObject* L_2 = ___source0;
		__this->set_source_1(L_2);
		int32_t L_3 = ___count1;
		__this->set_count_2(L_3);
		return;
	}
}
// System.Void UniRx.Operators.TakeObservable`1<System.Object>::.ctor(UniRx.IObservable`1<T>,System.TimeSpan,UniRx.IScheduler)
extern Il2CppClass* Scheduler_t103934925_il2cpp_TypeInfo_var;
extern const uint32_t TakeObservable_1__ctor_m654169306_MetadataUsageId;
extern "C"  void TakeObservable_1__ctor_m654169306_gshared (TakeObservable_1_t3078819489 * __this, Il2CppObject* ___source0, TimeSpan_t763862892  ___duration1, Il2CppObject * ___scheduler2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TakeObservable_1__ctor_m654169306_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	TakeObservable_1_t3078819489 * G_B2_0 = NULL;
	TakeObservable_1_t3078819489 * G_B1_0 = NULL;
	int32_t G_B3_0 = 0;
	TakeObservable_1_t3078819489 * G_B3_1 = NULL;
	{
		Il2CppObject * L_0 = ___scheduler2;
		IL2CPP_RUNTIME_CLASS_INIT(Scheduler_t103934925_il2cpp_TypeInfo_var);
		Il2CppObject * L_1 = ((Scheduler_t103934925_StaticFields*)Scheduler_t103934925_il2cpp_TypeInfo_var->static_fields)->get_CurrentThread_0();
		G_B1_0 = ((TakeObservable_1_t3078819489 *)(__this));
		if ((((Il2CppObject*)(Il2CppObject *)L_0) == ((Il2CppObject*)(Il2CppObject *)L_1)))
		{
			G_B2_0 = ((TakeObservable_1_t3078819489 *)(__this));
			goto IL_0014;
		}
	}
	{
		Il2CppObject* L_2 = ___source0;
		bool L_3 = ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)(NULL /*static, unused*/, (Il2CppObject*)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		G_B3_0 = ((int32_t)(L_3));
		G_B3_1 = ((TakeObservable_1_t3078819489 *)(G_B1_0));
		goto IL_0015;
	}

IL_0014:
	{
		G_B3_0 = 1;
		G_B3_1 = ((TakeObservable_1_t3078819489 *)(G_B2_0));
	}

IL_0015:
	{
		NullCheck((OperatorObservableBase_1_t4196218687 *)G_B3_1);
		((  void (*) (OperatorObservableBase_1_t4196218687 *, bool, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)((OperatorObservableBase_1_t4196218687 *)G_B3_1, (bool)G_B3_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		Il2CppObject* L_4 = ___source0;
		__this->set_source_1(L_4);
		TimeSpan_t763862892  L_5 = ___duration1;
		__this->set_duration_3(L_5);
		Il2CppObject * L_6 = ___scheduler2;
		__this->set_scheduler_4(L_6);
		return;
	}
}
// UniRx.IObservable`1<T> UniRx.Operators.TakeObservable`1<System.Object>::Combine(System.Int32)
extern "C"  Il2CppObject* TakeObservable_1_Combine_m575435775_gshared (TakeObservable_1_t3078819489 * __this, int32_t ___count0, const MethodInfo* method)
{
	TakeObservable_1_t3078819489 * G_B3_0 = NULL;
	{
		int32_t L_0 = (int32_t)__this->get_count_2();
		int32_t L_1 = ___count0;
		if ((((int32_t)L_0) > ((int32_t)L_1)))
		{
			goto IL_0012;
		}
	}
	{
		G_B3_0 = ((TakeObservable_1_t3078819489 *)(__this));
		goto IL_001e;
	}

IL_0012:
	{
		Il2CppObject* L_2 = (Il2CppObject*)__this->get_source_1();
		int32_t L_3 = ___count0;
		TakeObservable_1_t3078819489 * L_4 = (TakeObservable_1_t3078819489 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		((  void (*) (TakeObservable_1_t3078819489 *, Il2CppObject*, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)(L_4, (Il2CppObject*)L_2, (int32_t)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		G_B3_0 = ((TakeObservable_1_t3078819489 *)(L_4));
	}

IL_001e:
	{
		return G_B3_0;
	}
}
// UniRx.IObservable`1<T> UniRx.Operators.TakeObservable`1<System.Object>::Combine(System.TimeSpan)
extern Il2CppClass* TimeSpan_t763862892_il2cpp_TypeInfo_var;
extern const uint32_t TakeObservable_1_Combine_m2608002094_MetadataUsageId;
extern "C"  Il2CppObject* TakeObservable_1_Combine_m2608002094_gshared (TakeObservable_1_t3078819489 * __this, TimeSpan_t763862892  ___duration0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TakeObservable_1_Combine_m2608002094_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	TakeObservable_1_t3078819489 * G_B3_0 = NULL;
	{
		TimeSpan_t763862892  L_0 = (TimeSpan_t763862892 )__this->get_duration_3();
		TimeSpan_t763862892  L_1 = ___duration0;
		IL2CPP_RUNTIME_CLASS_INIT(TimeSpan_t763862892_il2cpp_TypeInfo_var);
		bool L_2 = TimeSpan_op_LessThanOrEqual_m271837557(NULL /*static, unused*/, (TimeSpan_t763862892 )L_0, (TimeSpan_t763862892 )L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0017;
		}
	}
	{
		G_B3_0 = ((TakeObservable_1_t3078819489 *)(__this));
		goto IL_0029;
	}

IL_0017:
	{
		Il2CppObject* L_3 = (Il2CppObject*)__this->get_source_1();
		TimeSpan_t763862892  L_4 = ___duration0;
		Il2CppObject * L_5 = (Il2CppObject *)__this->get_scheduler_4();
		TakeObservable_1_t3078819489 * L_6 = (TakeObservable_1_t3078819489 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		((  void (*) (TakeObservable_1_t3078819489 *, Il2CppObject*, TimeSpan_t763862892 , Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->method)(L_6, (Il2CppObject*)L_3, (TimeSpan_t763862892 )L_4, (Il2CppObject *)L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		G_B3_0 = ((TakeObservable_1_t3078819489 *)(L_6));
	}

IL_0029:
	{
		return G_B3_0;
	}
}
// System.IDisposable UniRx.Operators.TakeObservable`1<System.Object>::SubscribeCore(UniRx.IObserver`1<T>,System.IDisposable)
extern "C"  Il2CppObject * TakeObservable_1_SubscribeCore_m1473066779_gshared (TakeObservable_1_t3078819489 * __this, Il2CppObject* ___observer0, Il2CppObject * ___cancel1, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_scheduler_4();
		if (L_0)
		{
			goto IL_001f;
		}
	}
	{
		Il2CppObject* L_1 = (Il2CppObject*)__this->get_source_1();
		Il2CppObject* L_2 = ___observer0;
		Il2CppObject * L_3 = ___cancel1;
		Take_t531252872 * L_4 = (Take_t531252872 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		((  void (*) (Take_t531252872 *, TakeObservable_1_t3078819489 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7)->method)(L_4, (TakeObservable_1_t3078819489 *)__this, (Il2CppObject*)L_2, (Il2CppObject *)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7));
		NullCheck((Il2CppObject*)L_1);
		Il2CppObject * L_5 = InterfaceFuncInvoker1< Il2CppObject *, Il2CppObject* >::Invoke(0 /* System.IDisposable UniRx.IObservable`1<System.Object>::Subscribe(UniRx.IObserver`1<T>) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8), (Il2CppObject*)L_1, (Il2CppObject*)L_4);
		return L_5;
	}

IL_001f:
	{
		Il2CppObject* L_6 = ___observer0;
		Il2CppObject * L_7 = ___cancel1;
		Take__t2468273233 * L_8 = (Take__t2468273233 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9));
		((  void (*) (Take__t2468273233 *, TakeObservable_1_t3078819489 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10)->method)(L_8, (TakeObservable_1_t3078819489 *)__this, (Il2CppObject*)L_6, (Il2CppObject *)L_7, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10));
		NullCheck((Take__t2468273233 *)L_8);
		Il2CppObject * L_9 = ((  Il2CppObject * (*) (Take__t2468273233 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 11)->method)((Take__t2468273233 *)L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 11));
		return L_9;
	}
}
// System.Void UniRx.Operators.TakeObservable`1<UniRx.Unit>::.ctor(UniRx.IObservable`1<T>,System.Int32)
extern "C"  void TakeObservable_1__ctor_m622237057_gshared (TakeObservable_1_t505031811 * __this, Il2CppObject* ___source0, int32_t ___count1, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = ___source0;
		bool L_1 = ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)(NULL /*static, unused*/, (Il2CppObject*)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		NullCheck((OperatorObservableBase_1_t1622431009 *)__this);
		((  void (*) (OperatorObservableBase_1_t1622431009 *, bool, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)((OperatorObservableBase_1_t1622431009 *)__this, (bool)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		Il2CppObject* L_2 = ___source0;
		__this->set_source_1(L_2);
		int32_t L_3 = ___count1;
		__this->set_count_2(L_3);
		return;
	}
}
// System.Void UniRx.Operators.TakeObservable`1<UniRx.Unit>::.ctor(UniRx.IObservable`1<T>,System.TimeSpan,UniRx.IScheduler)
extern Il2CppClass* Scheduler_t103934925_il2cpp_TypeInfo_var;
extern const uint32_t TakeObservable_1__ctor_m3571795376_MetadataUsageId;
extern "C"  void TakeObservable_1__ctor_m3571795376_gshared (TakeObservable_1_t505031811 * __this, Il2CppObject* ___source0, TimeSpan_t763862892  ___duration1, Il2CppObject * ___scheduler2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TakeObservable_1__ctor_m3571795376_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	TakeObservable_1_t505031811 * G_B2_0 = NULL;
	TakeObservable_1_t505031811 * G_B1_0 = NULL;
	int32_t G_B3_0 = 0;
	TakeObservable_1_t505031811 * G_B3_1 = NULL;
	{
		Il2CppObject * L_0 = ___scheduler2;
		IL2CPP_RUNTIME_CLASS_INIT(Scheduler_t103934925_il2cpp_TypeInfo_var);
		Il2CppObject * L_1 = ((Scheduler_t103934925_StaticFields*)Scheduler_t103934925_il2cpp_TypeInfo_var->static_fields)->get_CurrentThread_0();
		G_B1_0 = ((TakeObservable_1_t505031811 *)(__this));
		if ((((Il2CppObject*)(Il2CppObject *)L_0) == ((Il2CppObject*)(Il2CppObject *)L_1)))
		{
			G_B2_0 = ((TakeObservable_1_t505031811 *)(__this));
			goto IL_0014;
		}
	}
	{
		Il2CppObject* L_2 = ___source0;
		bool L_3 = ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)(NULL /*static, unused*/, (Il2CppObject*)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		G_B3_0 = ((int32_t)(L_3));
		G_B3_1 = ((TakeObservable_1_t505031811 *)(G_B1_0));
		goto IL_0015;
	}

IL_0014:
	{
		G_B3_0 = 1;
		G_B3_1 = ((TakeObservable_1_t505031811 *)(G_B2_0));
	}

IL_0015:
	{
		NullCheck((OperatorObservableBase_1_t1622431009 *)G_B3_1);
		((  void (*) (OperatorObservableBase_1_t1622431009 *, bool, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)((OperatorObservableBase_1_t1622431009 *)G_B3_1, (bool)G_B3_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		Il2CppObject* L_4 = ___source0;
		__this->set_source_1(L_4);
		TimeSpan_t763862892  L_5 = ___duration1;
		__this->set_duration_3(L_5);
		Il2CppObject * L_6 = ___scheduler2;
		__this->set_scheduler_4(L_6);
		return;
	}
}
// UniRx.IObservable`1<T> UniRx.Operators.TakeObservable`1<UniRx.Unit>::Combine(System.Int32)
extern "C"  Il2CppObject* TakeObservable_1_Combine_m2705690337_gshared (TakeObservable_1_t505031811 * __this, int32_t ___count0, const MethodInfo* method)
{
	TakeObservable_1_t505031811 * G_B3_0 = NULL;
	{
		int32_t L_0 = (int32_t)__this->get_count_2();
		int32_t L_1 = ___count0;
		if ((((int32_t)L_0) > ((int32_t)L_1)))
		{
			goto IL_0012;
		}
	}
	{
		G_B3_0 = ((TakeObservable_1_t505031811 *)(__this));
		goto IL_001e;
	}

IL_0012:
	{
		Il2CppObject* L_2 = (Il2CppObject*)__this->get_source_1();
		int32_t L_3 = ___count0;
		TakeObservable_1_t505031811 * L_4 = (TakeObservable_1_t505031811 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		((  void (*) (TakeObservable_1_t505031811 *, Il2CppObject*, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)(L_4, (Il2CppObject*)L_2, (int32_t)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		G_B3_0 = ((TakeObservable_1_t505031811 *)(L_4));
	}

IL_001e:
	{
		return G_B3_0;
	}
}
// UniRx.IObservable`1<T> UniRx.Operators.TakeObservable`1<UniRx.Unit>::Combine(System.TimeSpan)
extern Il2CppClass* TimeSpan_t763862892_il2cpp_TypeInfo_var;
extern const uint32_t TakeObservable_1_Combine_m2584892940_MetadataUsageId;
extern "C"  Il2CppObject* TakeObservable_1_Combine_m2584892940_gshared (TakeObservable_1_t505031811 * __this, TimeSpan_t763862892  ___duration0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TakeObservable_1_Combine_m2584892940_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	TakeObservable_1_t505031811 * G_B3_0 = NULL;
	{
		TimeSpan_t763862892  L_0 = (TimeSpan_t763862892 )__this->get_duration_3();
		TimeSpan_t763862892  L_1 = ___duration0;
		IL2CPP_RUNTIME_CLASS_INIT(TimeSpan_t763862892_il2cpp_TypeInfo_var);
		bool L_2 = TimeSpan_op_LessThanOrEqual_m271837557(NULL /*static, unused*/, (TimeSpan_t763862892 )L_0, (TimeSpan_t763862892 )L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0017;
		}
	}
	{
		G_B3_0 = ((TakeObservable_1_t505031811 *)(__this));
		goto IL_0029;
	}

IL_0017:
	{
		Il2CppObject* L_3 = (Il2CppObject*)__this->get_source_1();
		TimeSpan_t763862892  L_4 = ___duration0;
		Il2CppObject * L_5 = (Il2CppObject *)__this->get_scheduler_4();
		TakeObservable_1_t505031811 * L_6 = (TakeObservable_1_t505031811 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		((  void (*) (TakeObservable_1_t505031811 *, Il2CppObject*, TimeSpan_t763862892 , Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->method)(L_6, (Il2CppObject*)L_3, (TimeSpan_t763862892 )L_4, (Il2CppObject *)L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		G_B3_0 = ((TakeObservable_1_t505031811 *)(L_6));
	}

IL_0029:
	{
		return G_B3_0;
	}
}
// System.IDisposable UniRx.Operators.TakeObservable`1<UniRx.Unit>::SubscribeCore(UniRx.IObserver`1<T>,System.IDisposable)
extern "C"  Il2CppObject * TakeObservable_1_SubscribeCore_m3384233799_gshared (TakeObservable_1_t505031811 * __this, Il2CppObject* ___observer0, Il2CppObject * ___cancel1, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_scheduler_4();
		if (L_0)
		{
			goto IL_001f;
		}
	}
	{
		Il2CppObject* L_1 = (Il2CppObject*)__this->get_source_1();
		Il2CppObject* L_2 = ___observer0;
		Il2CppObject * L_3 = ___cancel1;
		Take_t2252432490 * L_4 = (Take_t2252432490 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		((  void (*) (Take_t2252432490 *, TakeObservable_1_t505031811 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7)->method)(L_4, (TakeObservable_1_t505031811 *)__this, (Il2CppObject*)L_2, (Il2CppObject *)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7));
		NullCheck((Il2CppObject*)L_1);
		Il2CppObject * L_5 = InterfaceFuncInvoker1< Il2CppObject *, Il2CppObject* >::Invoke(0 /* System.IDisposable UniRx.IObservable`1<UniRx.Unit>::Subscribe(UniRx.IObserver`1<T>) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8), (Il2CppObject*)L_1, (Il2CppObject*)L_4);
		return L_5;
	}

IL_001f:
	{
		Il2CppObject* L_6 = ___observer0;
		Il2CppObject * L_7 = ___cancel1;
		Take__t4189452851 * L_8 = (Take__t4189452851 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9));
		((  void (*) (Take__t4189452851 *, TakeObservable_1_t505031811 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10)->method)(L_8, (TakeObservable_1_t505031811 *)__this, (Il2CppObject*)L_6, (Il2CppObject *)L_7, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10));
		NullCheck((Take__t4189452851 *)L_8);
		Il2CppObject * L_9 = ((  Il2CppObject * (*) (Take__t4189452851 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 11)->method)((Take__t4189452851 *)L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 11));
		return L_9;
	}
}
// System.Void UniRx.Operators.TakeUntilObservable`2/TakeUntil/TakeUntilOther<System.Object,System.Object>::.ctor(UniRx.Operators.TakeUntilObservable`2/TakeUntil<T,TOther>,System.IDisposable)
extern "C"  void TakeUntilOther__ctor_m3699070061_gshared (TakeUntilOther_t1704800930 * __this, TakeUntil_t2678949436 * ___sourceObserver0, Il2CppObject * ___subscription1, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		TakeUntil_t2678949436 * L_0 = ___sourceObserver0;
		__this->set_sourceObserver_0(L_0);
		Il2CppObject * L_1 = ___subscription1;
		__this->set_subscription_1(L_1);
		return;
	}
}
// System.Void UniRx.Operators.TakeUntilObservable`2/TakeUntil/TakeUntilOther<System.Object,System.Object>::OnNext(TOther)
extern "C"  void TakeUntilOther_OnNext_m3987641410_gshared (TakeUntilOther_t1704800930 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	Il2CppObject * V_0 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		TakeUntil_t2678949436 * L_0 = (TakeUntil_t2678949436 *)__this->get_sourceObserver_0();
		NullCheck(L_0);
		Il2CppObject * L_1 = (Il2CppObject *)L_0->get_gate_3();
		V_0 = (Il2CppObject *)L_1;
		Il2CppObject * L_2 = V_0;
		Monitor_Enter_m476686225(NULL /*static, unused*/, (Il2CppObject *)L_2, /*hidden argument*/NULL);
	}

IL_0012:
	try
	{ // begin try (depth: 1)
		try
		{ // begin try (depth: 2)
			TakeUntil_t2678949436 * L_3 = (TakeUntil_t2678949436 *)__this->get_sourceObserver_0();
			NullCheck(L_3);
			Il2CppObject* L_4 = (Il2CppObject*)((OperatorObserverBase_2_t1187768149 *)L_3)->get_observer_0();
			il2cpp_codegen_memory_barrier();
			NullCheck((Il2CppObject*)L_4);
			InterfaceActionInvoker0::Invoke(0 /* System.Void UniRx.IObserver`1<System.Object>::OnCompleted() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0), (Il2CppObject*)L_4);
			IL2CPP_LEAVE(0x35, FINALLY_0029);
		} // end try (depth: 2)
		catch(Il2CppExceptionWrapper& e)
		{
			__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
			goto FINALLY_0029;
		}

FINALLY_0029:
		{ // begin finally (depth: 2)
			TakeUntil_t2678949436 * L_5 = (TakeUntil_t2678949436 *)__this->get_sourceObserver_0();
			NullCheck((OperatorObserverBase_2_t1187768149 *)L_5);
			VirtActionInvoker0::Invoke(4 /* System.Void UniRx.Operators.OperatorObserverBase`2<System.Object,System.Object>::Dispose() */, (OperatorObserverBase_2_t1187768149 *)L_5);
			IL2CPP_END_FINALLY(41)
		} // end finally (depth: 2)
		IL2CPP_CLEANUP(41)
		{
			IL2CPP_JUMP_TBL(0x35, IL_0035)
			IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
		}

IL_0035:
		{
			IL2CPP_LEAVE(0x41, FINALLY_003a);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_003a;
	}

FINALLY_003a:
	{ // begin finally (depth: 1)
		Il2CppObject * L_6 = V_0;
		Monitor_Exit_m2088237919(NULL /*static, unused*/, (Il2CppObject *)L_6, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(58)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(58)
	{
		IL2CPP_JUMP_TBL(0x41, IL_0041)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0041:
	{
		return;
	}
}
// System.Void UniRx.Operators.TakeUntilObservable`2/TakeUntil/TakeUntilOther<System.Object,System.Object>::OnError(System.Exception)
extern "C"  void TakeUntilOther_OnError_m3044938895_gshared (TakeUntilOther_t1704800930 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method)
{
	Il2CppObject * V_0 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		TakeUntil_t2678949436 * L_0 = (TakeUntil_t2678949436 *)__this->get_sourceObserver_0();
		NullCheck(L_0);
		Il2CppObject * L_1 = (Il2CppObject *)L_0->get_gate_3();
		V_0 = (Il2CppObject *)L_1;
		Il2CppObject * L_2 = V_0;
		Monitor_Enter_m476686225(NULL /*static, unused*/, (Il2CppObject *)L_2, /*hidden argument*/NULL);
	}

IL_0012:
	try
	{ // begin try (depth: 1)
		try
		{ // begin try (depth: 2)
			TakeUntil_t2678949436 * L_3 = (TakeUntil_t2678949436 *)__this->get_sourceObserver_0();
			NullCheck(L_3);
			Il2CppObject* L_4 = (Il2CppObject*)((OperatorObserverBase_2_t1187768149 *)L_3)->get_observer_0();
			il2cpp_codegen_memory_barrier();
			Exception_t1967233988 * L_5 = ___error0;
			NullCheck((Il2CppObject*)L_4);
			InterfaceActionInvoker1< Exception_t1967233988 * >::Invoke(1 /* System.Void UniRx.IObserver`1<System.Object>::OnError(System.Exception) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0), (Il2CppObject*)L_4, (Exception_t1967233988 *)L_5);
			IL2CPP_LEAVE(0x36, FINALLY_002a);
		} // end try (depth: 2)
		catch(Il2CppExceptionWrapper& e)
		{
			__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
			goto FINALLY_002a;
		}

FINALLY_002a:
		{ // begin finally (depth: 2)
			TakeUntil_t2678949436 * L_6 = (TakeUntil_t2678949436 *)__this->get_sourceObserver_0();
			NullCheck((OperatorObserverBase_2_t1187768149 *)L_6);
			VirtActionInvoker0::Invoke(4 /* System.Void UniRx.Operators.OperatorObserverBase`2<System.Object,System.Object>::Dispose() */, (OperatorObserverBase_2_t1187768149 *)L_6);
			IL2CPP_END_FINALLY(42)
		} // end finally (depth: 2)
		IL2CPP_CLEANUP(42)
		{
			IL2CPP_JUMP_TBL(0x36, IL_0036)
			IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
		}

IL_0036:
		{
			IL2CPP_LEAVE(0x42, FINALLY_003b);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_003b;
	}

FINALLY_003b:
	{ // begin finally (depth: 1)
		Il2CppObject * L_7 = V_0;
		Monitor_Exit_m2088237919(NULL /*static, unused*/, (Il2CppObject *)L_7, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(59)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(59)
	{
		IL2CPP_JUMP_TBL(0x42, IL_0042)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0042:
	{
		return;
	}
}
// System.Void UniRx.Operators.TakeUntilObservable`2/TakeUntil/TakeUntilOther<System.Object,System.Object>::OnCompleted()
extern Il2CppClass* IDisposable_t1628921374_il2cpp_TypeInfo_var;
extern const uint32_t TakeUntilOther_OnCompleted_m4149470818_MetadataUsageId;
extern "C"  void TakeUntilOther_OnCompleted_m4149470818_gshared (TakeUntilOther_t1704800930 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TakeUntilOther_OnCompleted_m4149470818_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Il2CppObject * V_0 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		TakeUntil_t2678949436 * L_0 = (TakeUntil_t2678949436 *)__this->get_sourceObserver_0();
		NullCheck(L_0);
		Il2CppObject * L_1 = (Il2CppObject *)L_0->get_gate_3();
		V_0 = (Il2CppObject *)L_1;
		Il2CppObject * L_2 = V_0;
		Monitor_Enter_m476686225(NULL /*static, unused*/, (Il2CppObject *)L_2, /*hidden argument*/NULL);
	}

IL_0012:
	try
	{ // begin try (depth: 1)
		TakeUntil_t2678949436 * L_3 = (TakeUntil_t2678949436 *)__this->get_sourceObserver_0();
		NullCheck(L_3);
		L_3->set_open_4((bool)1);
		Il2CppObject * L_4 = (Il2CppObject *)__this->get_subscription_1();
		NullCheck((Il2CppObject *)L_4);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, (Il2CppObject *)L_4);
		IL2CPP_LEAVE(0x35, FINALLY_002e);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_002e;
	}

FINALLY_002e:
	{ // begin finally (depth: 1)
		Il2CppObject * L_5 = V_0;
		Monitor_Exit_m2088237919(NULL /*static, unused*/, (Il2CppObject *)L_5, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(46)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(46)
	{
		IL2CPP_JUMP_TBL(0x35, IL_0035)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0035:
	{
		return;
	}
}
// System.Void UniRx.Operators.TakeUntilObservable`2/TakeUntil/TakeUntilOther<System.Object,UniRx.Unit>::.ctor(UniRx.Operators.TakeUntilObservable`2/TakeUntil<T,TOther>,System.IDisposable)
extern "C"  void TakeUntilOther__ctor_m3750508961_gshared (TakeUntilOther_t3425980548 * __this, TakeUntil_t105161758 * ___sourceObserver0, Il2CppObject * ___subscription1, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		TakeUntil_t105161758 * L_0 = ___sourceObserver0;
		__this->set_sourceObserver_0(L_0);
		Il2CppObject * L_1 = ___subscription1;
		__this->set_subscription_1(L_1);
		return;
	}
}
// System.Void UniRx.Operators.TakeUntilObservable`2/TakeUntil/TakeUntilOther<System.Object,UniRx.Unit>::OnNext(TOther)
extern "C"  void TakeUntilOther_OnNext_m1816174222_gshared (TakeUntilOther_t3425980548 * __this, Unit_t2558286038  ___value0, const MethodInfo* method)
{
	Il2CppObject * V_0 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		TakeUntil_t105161758 * L_0 = (TakeUntil_t105161758 *)__this->get_sourceObserver_0();
		NullCheck(L_0);
		Il2CppObject * L_1 = (Il2CppObject *)L_0->get_gate_3();
		V_0 = (Il2CppObject *)L_1;
		Il2CppObject * L_2 = V_0;
		Monitor_Enter_m476686225(NULL /*static, unused*/, (Il2CppObject *)L_2, /*hidden argument*/NULL);
	}

IL_0012:
	try
	{ // begin try (depth: 1)
		try
		{ // begin try (depth: 2)
			TakeUntil_t105161758 * L_3 = (TakeUntil_t105161758 *)__this->get_sourceObserver_0();
			NullCheck(L_3);
			Il2CppObject* L_4 = (Il2CppObject*)((OperatorObserverBase_2_t1187768149 *)L_3)->get_observer_0();
			il2cpp_codegen_memory_barrier();
			NullCheck((Il2CppObject*)L_4);
			InterfaceActionInvoker0::Invoke(0 /* System.Void UniRx.IObserver`1<System.Object>::OnCompleted() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0), (Il2CppObject*)L_4);
			IL2CPP_LEAVE(0x35, FINALLY_0029);
		} // end try (depth: 2)
		catch(Il2CppExceptionWrapper& e)
		{
			__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
			goto FINALLY_0029;
		}

FINALLY_0029:
		{ // begin finally (depth: 2)
			TakeUntil_t105161758 * L_5 = (TakeUntil_t105161758 *)__this->get_sourceObserver_0();
			NullCheck((OperatorObserverBase_2_t1187768149 *)L_5);
			VirtActionInvoker0::Invoke(4 /* System.Void UniRx.Operators.OperatorObserverBase`2<System.Object,System.Object>::Dispose() */, (OperatorObserverBase_2_t1187768149 *)L_5);
			IL2CPP_END_FINALLY(41)
		} // end finally (depth: 2)
		IL2CPP_CLEANUP(41)
		{
			IL2CPP_JUMP_TBL(0x35, IL_0035)
			IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
		}

IL_0035:
		{
			IL2CPP_LEAVE(0x41, FINALLY_003a);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_003a;
	}

FINALLY_003a:
	{ // begin finally (depth: 1)
		Il2CppObject * L_6 = V_0;
		Monitor_Exit_m2088237919(NULL /*static, unused*/, (Il2CppObject *)L_6, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(58)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(58)
	{
		IL2CPP_JUMP_TBL(0x41, IL_0041)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0041:
	{
		return;
	}
}
// System.Void UniRx.Operators.TakeUntilObservable`2/TakeUntil/TakeUntilOther<System.Object,UniRx.Unit>::OnError(System.Exception)
extern "C"  void TakeUntilOther_OnError_m14607043_gshared (TakeUntilOther_t3425980548 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method)
{
	Il2CppObject * V_0 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		TakeUntil_t105161758 * L_0 = (TakeUntil_t105161758 *)__this->get_sourceObserver_0();
		NullCheck(L_0);
		Il2CppObject * L_1 = (Il2CppObject *)L_0->get_gate_3();
		V_0 = (Il2CppObject *)L_1;
		Il2CppObject * L_2 = V_0;
		Monitor_Enter_m476686225(NULL /*static, unused*/, (Il2CppObject *)L_2, /*hidden argument*/NULL);
	}

IL_0012:
	try
	{ // begin try (depth: 1)
		try
		{ // begin try (depth: 2)
			TakeUntil_t105161758 * L_3 = (TakeUntil_t105161758 *)__this->get_sourceObserver_0();
			NullCheck(L_3);
			Il2CppObject* L_4 = (Il2CppObject*)((OperatorObserverBase_2_t1187768149 *)L_3)->get_observer_0();
			il2cpp_codegen_memory_barrier();
			Exception_t1967233988 * L_5 = ___error0;
			NullCheck((Il2CppObject*)L_4);
			InterfaceActionInvoker1< Exception_t1967233988 * >::Invoke(1 /* System.Void UniRx.IObserver`1<System.Object>::OnError(System.Exception) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0), (Il2CppObject*)L_4, (Exception_t1967233988 *)L_5);
			IL2CPP_LEAVE(0x36, FINALLY_002a);
		} // end try (depth: 2)
		catch(Il2CppExceptionWrapper& e)
		{
			__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
			goto FINALLY_002a;
		}

FINALLY_002a:
		{ // begin finally (depth: 2)
			TakeUntil_t105161758 * L_6 = (TakeUntil_t105161758 *)__this->get_sourceObserver_0();
			NullCheck((OperatorObserverBase_2_t1187768149 *)L_6);
			VirtActionInvoker0::Invoke(4 /* System.Void UniRx.Operators.OperatorObserverBase`2<System.Object,System.Object>::Dispose() */, (OperatorObserverBase_2_t1187768149 *)L_6);
			IL2CPP_END_FINALLY(42)
		} // end finally (depth: 2)
		IL2CPP_CLEANUP(42)
		{
			IL2CPP_JUMP_TBL(0x36, IL_0036)
			IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
		}

IL_0036:
		{
			IL2CPP_LEAVE(0x42, FINALLY_003b);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_003b;
	}

FINALLY_003b:
	{ // begin finally (depth: 1)
		Il2CppObject * L_7 = V_0;
		Monitor_Exit_m2088237919(NULL /*static, unused*/, (Il2CppObject *)L_7, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(59)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(59)
	{
		IL2CPP_JUMP_TBL(0x42, IL_0042)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0042:
	{
		return;
	}
}
// System.Void UniRx.Operators.TakeUntilObservable`2/TakeUntil/TakeUntilOther<System.Object,UniRx.Unit>::OnCompleted()
extern Il2CppClass* IDisposable_t1628921374_il2cpp_TypeInfo_var;
extern const uint32_t TakeUntilOther_OnCompleted_m477192854_MetadataUsageId;
extern "C"  void TakeUntilOther_OnCompleted_m477192854_gshared (TakeUntilOther_t3425980548 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TakeUntilOther_OnCompleted_m477192854_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Il2CppObject * V_0 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		TakeUntil_t105161758 * L_0 = (TakeUntil_t105161758 *)__this->get_sourceObserver_0();
		NullCheck(L_0);
		Il2CppObject * L_1 = (Il2CppObject *)L_0->get_gate_3();
		V_0 = (Il2CppObject *)L_1;
		Il2CppObject * L_2 = V_0;
		Monitor_Enter_m476686225(NULL /*static, unused*/, (Il2CppObject *)L_2, /*hidden argument*/NULL);
	}

IL_0012:
	try
	{ // begin try (depth: 1)
		TakeUntil_t105161758 * L_3 = (TakeUntil_t105161758 *)__this->get_sourceObserver_0();
		NullCheck(L_3);
		L_3->set_open_4((bool)1);
		Il2CppObject * L_4 = (Il2CppObject *)__this->get_subscription_1();
		NullCheck((Il2CppObject *)L_4);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, (Il2CppObject *)L_4);
		IL2CPP_LEAVE(0x35, FINALLY_002e);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_002e;
	}

FINALLY_002e:
	{ // begin finally (depth: 1)
		Il2CppObject * L_5 = V_0;
		Monitor_Exit_m2088237919(NULL /*static, unused*/, (Il2CppObject *)L_5, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(46)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(46)
	{
		IL2CPP_JUMP_TBL(0x35, IL_0035)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0035:
	{
		return;
	}
}
// System.Void UniRx.Operators.TakeUntilObservable`2/TakeUntil/TakeUntilOther<UniRx.Unit,UniRx.Unit>::.ctor(UniRx.Operators.TakeUntilObservable`2/TakeUntil<T,TOther>,System.IDisposable)
extern "C"  void TakeUntilOther__ctor_m1790894793_gshared (TakeUntilOther_t387441882 * __this, TakeUntil_t1361590388 * ___sourceObserver0, Il2CppObject * ___subscription1, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		TakeUntil_t1361590388 * L_0 = ___sourceObserver0;
		__this->set_sourceObserver_0(L_0);
		Il2CppObject * L_1 = ___subscription1;
		__this->set_subscription_1(L_1);
		return;
	}
}
// System.Void UniRx.Operators.TakeUntilObservable`2/TakeUntil/TakeUntilOther<UniRx.Unit,UniRx.Unit>::OnNext(TOther)
extern "C"  void TakeUntilOther_OnNext_m257793126_gshared (TakeUntilOther_t387441882 * __this, Unit_t2558286038  ___value0, const MethodInfo* method)
{
	Il2CppObject * V_0 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		TakeUntil_t1361590388 * L_0 = (TakeUntil_t1361590388 *)__this->get_sourceObserver_0();
		NullCheck(L_0);
		Il2CppObject * L_1 = (Il2CppObject *)L_0->get_gate_3();
		V_0 = (Il2CppObject *)L_1;
		Il2CppObject * L_2 = V_0;
		Monitor_Enter_m476686225(NULL /*static, unused*/, (Il2CppObject *)L_2, /*hidden argument*/NULL);
	}

IL_0012:
	try
	{ // begin try (depth: 1)
		try
		{ // begin try (depth: 2)
			TakeUntil_t1361590388 * L_3 = (TakeUntil_t1361590388 *)__this->get_sourceObserver_0();
			NullCheck(L_3);
			Il2CppObject* L_4 = (Il2CppObject*)((OperatorObserverBase_2_t4165376397 *)L_3)->get_observer_0();
			il2cpp_codegen_memory_barrier();
			NullCheck((Il2CppObject*)L_4);
			InterfaceActionInvoker0::Invoke(0 /* System.Void UniRx.IObserver`1<UniRx.Unit>::OnCompleted() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0), (Il2CppObject*)L_4);
			IL2CPP_LEAVE(0x35, FINALLY_0029);
		} // end try (depth: 2)
		catch(Il2CppExceptionWrapper& e)
		{
			__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
			goto FINALLY_0029;
		}

FINALLY_0029:
		{ // begin finally (depth: 2)
			TakeUntil_t1361590388 * L_5 = (TakeUntil_t1361590388 *)__this->get_sourceObserver_0();
			NullCheck((OperatorObserverBase_2_t4165376397 *)L_5);
			VirtActionInvoker0::Invoke(4 /* System.Void UniRx.Operators.OperatorObserverBase`2<UniRx.Unit,UniRx.Unit>::Dispose() */, (OperatorObserverBase_2_t4165376397 *)L_5);
			IL2CPP_END_FINALLY(41)
		} // end finally (depth: 2)
		IL2CPP_CLEANUP(41)
		{
			IL2CPP_JUMP_TBL(0x35, IL_0035)
			IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
		}

IL_0035:
		{
			IL2CPP_LEAVE(0x41, FINALLY_003a);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_003a;
	}

FINALLY_003a:
	{ // begin finally (depth: 1)
		Il2CppObject * L_6 = V_0;
		Monitor_Exit_m2088237919(NULL /*static, unused*/, (Il2CppObject *)L_6, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(58)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(58)
	{
		IL2CPP_JUMP_TBL(0x41, IL_0041)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0041:
	{
		return;
	}
}
// System.Void UniRx.Operators.TakeUntilObservable`2/TakeUntil/TakeUntilOther<UniRx.Unit,UniRx.Unit>::OnError(System.Exception)
extern "C"  void TakeUntilOther_OnError_m2727693803_gshared (TakeUntilOther_t387441882 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method)
{
	Il2CppObject * V_0 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		TakeUntil_t1361590388 * L_0 = (TakeUntil_t1361590388 *)__this->get_sourceObserver_0();
		NullCheck(L_0);
		Il2CppObject * L_1 = (Il2CppObject *)L_0->get_gate_3();
		V_0 = (Il2CppObject *)L_1;
		Il2CppObject * L_2 = V_0;
		Monitor_Enter_m476686225(NULL /*static, unused*/, (Il2CppObject *)L_2, /*hidden argument*/NULL);
	}

IL_0012:
	try
	{ // begin try (depth: 1)
		try
		{ // begin try (depth: 2)
			TakeUntil_t1361590388 * L_3 = (TakeUntil_t1361590388 *)__this->get_sourceObserver_0();
			NullCheck(L_3);
			Il2CppObject* L_4 = (Il2CppObject*)((OperatorObserverBase_2_t4165376397 *)L_3)->get_observer_0();
			il2cpp_codegen_memory_barrier();
			Exception_t1967233988 * L_5 = ___error0;
			NullCheck((Il2CppObject*)L_4);
			InterfaceActionInvoker1< Exception_t1967233988 * >::Invoke(1 /* System.Void UniRx.IObserver`1<UniRx.Unit>::OnError(System.Exception) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0), (Il2CppObject*)L_4, (Exception_t1967233988 *)L_5);
			IL2CPP_LEAVE(0x36, FINALLY_002a);
		} // end try (depth: 2)
		catch(Il2CppExceptionWrapper& e)
		{
			__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
			goto FINALLY_002a;
		}

FINALLY_002a:
		{ // begin finally (depth: 2)
			TakeUntil_t1361590388 * L_6 = (TakeUntil_t1361590388 *)__this->get_sourceObserver_0();
			NullCheck((OperatorObserverBase_2_t4165376397 *)L_6);
			VirtActionInvoker0::Invoke(4 /* System.Void UniRx.Operators.OperatorObserverBase`2<UniRx.Unit,UniRx.Unit>::Dispose() */, (OperatorObserverBase_2_t4165376397 *)L_6);
			IL2CPP_END_FINALLY(42)
		} // end finally (depth: 2)
		IL2CPP_CLEANUP(42)
		{
			IL2CPP_JUMP_TBL(0x36, IL_0036)
			IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
		}

IL_0036:
		{
			IL2CPP_LEAVE(0x42, FINALLY_003b);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_003b;
	}

FINALLY_003b:
	{ // begin finally (depth: 1)
		Il2CppObject * L_7 = V_0;
		Monitor_Exit_m2088237919(NULL /*static, unused*/, (Il2CppObject *)L_7, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(59)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(59)
	{
		IL2CPP_JUMP_TBL(0x42, IL_0042)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0042:
	{
		return;
	}
}
// System.Void UniRx.Operators.TakeUntilObservable`2/TakeUntil/TakeUntilOther<UniRx.Unit,UniRx.Unit>::OnCompleted()
extern Il2CppClass* IDisposable_t1628921374_il2cpp_TypeInfo_var;
extern const uint32_t TakeUntilOther_OnCompleted_m2505132478_MetadataUsageId;
extern "C"  void TakeUntilOther_OnCompleted_m2505132478_gshared (TakeUntilOther_t387441882 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TakeUntilOther_OnCompleted_m2505132478_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Il2CppObject * V_0 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		TakeUntil_t1361590388 * L_0 = (TakeUntil_t1361590388 *)__this->get_sourceObserver_0();
		NullCheck(L_0);
		Il2CppObject * L_1 = (Il2CppObject *)L_0->get_gate_3();
		V_0 = (Il2CppObject *)L_1;
		Il2CppObject * L_2 = V_0;
		Monitor_Enter_m476686225(NULL /*static, unused*/, (Il2CppObject *)L_2, /*hidden argument*/NULL);
	}

IL_0012:
	try
	{ // begin try (depth: 1)
		TakeUntil_t1361590388 * L_3 = (TakeUntil_t1361590388 *)__this->get_sourceObserver_0();
		NullCheck(L_3);
		L_3->set_open_4((bool)1);
		Il2CppObject * L_4 = (Il2CppObject *)__this->get_subscription_1();
		NullCheck((Il2CppObject *)L_4);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, (Il2CppObject *)L_4);
		IL2CPP_LEAVE(0x35, FINALLY_002e);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_002e;
	}

FINALLY_002e:
	{ // begin finally (depth: 1)
		Il2CppObject * L_5 = V_0;
		Monitor_Exit_m2088237919(NULL /*static, unused*/, (Il2CppObject *)L_5, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(46)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(46)
	{
		IL2CPP_JUMP_TBL(0x35, IL_0035)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0035:
	{
		return;
	}
}
// System.Void UniRx.Operators.TakeUntilObservable`2/TakeUntil<System.Object,System.Object>::.ctor(UniRx.Operators.TakeUntilObservable`2<T,TOther>,UniRx.IObserver`1<T>,System.IDisposable)
extern Il2CppClass* Il2CppObject_il2cpp_TypeInfo_var;
extern const uint32_t TakeUntil__ctor_m3722184177_MetadataUsageId;
extern "C"  void TakeUntil__ctor_m3722184177_gshared (TakeUntil_t2678949436 * __this, TakeUntilObservable_2_t4246198466 * ___parent0, Il2CppObject* ___observer1, Il2CppObject * ___cancel2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TakeUntil__ctor_m3722184177_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject * L_0 = (Il2CppObject *)il2cpp_codegen_object_new(Il2CppObject_il2cpp_TypeInfo_var);
		Object__ctor_m1772956182(L_0, /*hidden argument*/NULL);
		__this->set_gate_3(L_0);
		Il2CppObject* L_1 = ___observer1;
		Il2CppObject * L_2 = ___cancel2;
		NullCheck((OperatorObserverBase_2_t1187768149 *)__this);
		((  void (*) (OperatorObserverBase_2_t1187768149 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((OperatorObserverBase_2_t1187768149 *)__this, (Il2CppObject*)L_1, (Il2CppObject *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		TakeUntilObservable_2_t4246198466 * L_3 = ___parent0;
		__this->set_parent_2(L_3);
		return;
	}
}
// System.IDisposable UniRx.Operators.TakeUntilObservable`2/TakeUntil<System.Object,System.Object>::Run()
extern Il2CppClass* SingleAssignmentDisposable_t2336378823_il2cpp_TypeInfo_var;
extern const uint32_t TakeUntil_Run_m1825255312_MetadataUsageId;
extern "C"  Il2CppObject * TakeUntil_Run_m1825255312_gshared (TakeUntil_t2678949436 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TakeUntil_Run_m1825255312_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	SingleAssignmentDisposable_t2336378823 * V_0 = NULL;
	TakeUntilOther_t1704800930 * V_1 = NULL;
	Il2CppObject * V_2 = NULL;
	{
		SingleAssignmentDisposable_t2336378823 * L_0 = (SingleAssignmentDisposable_t2336378823 *)il2cpp_codegen_object_new(SingleAssignmentDisposable_t2336378823_il2cpp_TypeInfo_var);
		SingleAssignmentDisposable__ctor_m917449122(L_0, /*hidden argument*/NULL);
		V_0 = (SingleAssignmentDisposable_t2336378823 *)L_0;
		SingleAssignmentDisposable_t2336378823 * L_1 = V_0;
		TakeUntilOther_t1704800930 * L_2 = (TakeUntilOther_t1704800930 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		((  void (*) (TakeUntilOther_t1704800930 *, TakeUntil_t2678949436 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)(L_2, (TakeUntil_t2678949436 *)__this, (Il2CppObject *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		V_1 = (TakeUntilOther_t1704800930 *)L_2;
		SingleAssignmentDisposable_t2336378823 * L_3 = V_0;
		TakeUntilObservable_2_t4246198466 * L_4 = (TakeUntilObservable_2_t4246198466 *)__this->get_parent_2();
		NullCheck(L_4);
		Il2CppObject* L_5 = (Il2CppObject*)L_4->get_other_2();
		TakeUntilOther_t1704800930 * L_6 = V_1;
		NullCheck((Il2CppObject*)L_5);
		Il2CppObject * L_7 = InterfaceFuncInvoker1< Il2CppObject *, Il2CppObject* >::Invoke(0 /* System.IDisposable UniRx.IObservable`1<System.Object>::Subscribe(UniRx.IObserver`1<T>) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4), (Il2CppObject*)L_5, (Il2CppObject*)L_6);
		NullCheck((SingleAssignmentDisposable_t2336378823 *)L_3);
		SingleAssignmentDisposable_set_Disposable_m1599028885((SingleAssignmentDisposable_t2336378823 *)L_3, (Il2CppObject *)L_7, /*hidden argument*/NULL);
		TakeUntilObservable_2_t4246198466 * L_8 = (TakeUntilObservable_2_t4246198466 *)__this->get_parent_2();
		NullCheck(L_8);
		Il2CppObject* L_9 = (Il2CppObject*)L_8->get_source_1();
		NullCheck((Il2CppObject*)L_9);
		Il2CppObject * L_10 = InterfaceFuncInvoker1< Il2CppObject *, Il2CppObject* >::Invoke(0 /* System.IDisposable UniRx.IObservable`1<System.Object>::Subscribe(UniRx.IObserver`1<T>) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5), (Il2CppObject*)L_9, (Il2CppObject*)__this);
		V_2 = (Il2CppObject *)L_10;
		SingleAssignmentDisposable_t2336378823 * L_11 = V_0;
		Il2CppObject * L_12 = V_2;
		Il2CppObject * L_13 = StableCompositeDisposable_Create_m3764788535(NULL /*static, unused*/, (Il2CppObject *)L_11, (Il2CppObject *)L_12, /*hidden argument*/NULL);
		return L_13;
	}
}
// System.Void UniRx.Operators.TakeUntilObservable`2/TakeUntil<System.Object,System.Object>::OnNext(T)
extern "C"  void TakeUntil_OnNext_m3312144426_gshared (TakeUntil_t2678949436 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	Il2CppObject * V_0 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		bool L_0 = (bool)__this->get_open_4();
		if (!L_0)
		{
			goto IL_001e;
		}
	}
	{
		Il2CppObject* L_1 = (Il2CppObject*)((OperatorObserverBase_2_t1187768149 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		Il2CppObject * L_2 = ___value0;
		NullCheck((Il2CppObject*)L_1);
		InterfaceActionInvoker1< Il2CppObject * >::Invoke(2 /* System.Void UniRx.IObserver`1<System.Object>::OnNext(T) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6), (Il2CppObject*)L_1, (Il2CppObject *)L_2);
		goto IL_0045;
	}

IL_001e:
	{
		Il2CppObject * L_3 = (Il2CppObject *)__this->get_gate_3();
		V_0 = (Il2CppObject *)L_3;
		Il2CppObject * L_4 = V_0;
		Monitor_Enter_m476686225(NULL /*static, unused*/, (Il2CppObject *)L_4, /*hidden argument*/NULL);
	}

IL_002b:
	try
	{ // begin try (depth: 1)
		Il2CppObject* L_5 = (Il2CppObject*)((OperatorObserverBase_2_t1187768149 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		Il2CppObject * L_6 = ___value0;
		NullCheck((Il2CppObject*)L_5);
		InterfaceActionInvoker1< Il2CppObject * >::Invoke(2 /* System.Void UniRx.IObserver`1<System.Object>::OnNext(T) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6), (Il2CppObject*)L_5, (Il2CppObject *)L_6);
		IL2CPP_LEAVE(0x45, FINALLY_003e);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_003e;
	}

FINALLY_003e:
	{ // begin finally (depth: 1)
		Il2CppObject * L_7 = V_0;
		Monitor_Exit_m2088237919(NULL /*static, unused*/, (Il2CppObject *)L_7, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(62)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(62)
	{
		IL2CPP_JUMP_TBL(0x45, IL_0045)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0045:
	{
		return;
	}
}
// System.Void UniRx.Operators.TakeUntilObservable`2/TakeUntil<System.Object,System.Object>::OnError(System.Exception)
extern "C"  void TakeUntil_OnError_m3421361977_gshared (TakeUntil_t2678949436 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method)
{
	Il2CppObject * V_0 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_gate_3();
		V_0 = (Il2CppObject *)L_0;
		Il2CppObject * L_1 = V_0;
		Monitor_Enter_m476686225(NULL /*static, unused*/, (Il2CppObject *)L_1, /*hidden argument*/NULL);
	}

IL_000d:
	try
	{ // begin try (depth: 1)
		try
		{ // begin try (depth: 2)
			Il2CppObject* L_2 = (Il2CppObject*)((OperatorObserverBase_2_t1187768149 *)__this)->get_observer_0();
			il2cpp_codegen_memory_barrier();
			Exception_t1967233988 * L_3 = ___error0;
			NullCheck((Il2CppObject*)L_2);
			InterfaceActionInvoker1< Exception_t1967233988 * >::Invoke(1 /* System.Void UniRx.IObserver`1<System.Object>::OnError(System.Exception) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6), (Il2CppObject*)L_2, (Exception_t1967233988 *)L_3);
			IL2CPP_LEAVE(0x27, FINALLY_0020);
		} // end try (depth: 2)
		catch(Il2CppExceptionWrapper& e)
		{
			__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
			goto FINALLY_0020;
		}

FINALLY_0020:
		{ // begin finally (depth: 2)
			NullCheck((OperatorObserverBase_2_t1187768149 *)__this);
			VirtActionInvoker0::Invoke(4 /* System.Void UniRx.Operators.OperatorObserverBase`2<System.Object,System.Object>::Dispose() */, (OperatorObserverBase_2_t1187768149 *)__this);
			IL2CPP_END_FINALLY(32)
		} // end finally (depth: 2)
		IL2CPP_CLEANUP(32)
		{
			IL2CPP_JUMP_TBL(0x27, IL_0027)
			IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
		}

IL_0027:
		{
			IL2CPP_LEAVE(0x33, FINALLY_002c);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_002c;
	}

FINALLY_002c:
	{ // begin finally (depth: 1)
		Il2CppObject * L_4 = V_0;
		Monitor_Exit_m2088237919(NULL /*static, unused*/, (Il2CppObject *)L_4, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(44)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(44)
	{
		IL2CPP_JUMP_TBL(0x33, IL_0033)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0033:
	{
		return;
	}
}
// System.Void UniRx.Operators.TakeUntilObservable`2/TakeUntil<System.Object,System.Object>::OnCompleted()
extern "C"  void TakeUntil_OnCompleted_m3600996364_gshared (TakeUntil_t2678949436 * __this, const MethodInfo* method)
{
	Il2CppObject * V_0 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_gate_3();
		V_0 = (Il2CppObject *)L_0;
		Il2CppObject * L_1 = V_0;
		Monitor_Enter_m476686225(NULL /*static, unused*/, (Il2CppObject *)L_1, /*hidden argument*/NULL);
	}

IL_000d:
	try
	{ // begin try (depth: 1)
		try
		{ // begin try (depth: 2)
			Il2CppObject* L_2 = (Il2CppObject*)((OperatorObserverBase_2_t1187768149 *)__this)->get_observer_0();
			il2cpp_codegen_memory_barrier();
			NullCheck((Il2CppObject*)L_2);
			InterfaceActionInvoker0::Invoke(0 /* System.Void UniRx.IObserver`1<System.Object>::OnCompleted() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6), (Il2CppObject*)L_2);
			IL2CPP_LEAVE(0x26, FINALLY_001f);
		} // end try (depth: 2)
		catch(Il2CppExceptionWrapper& e)
		{
			__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
			goto FINALLY_001f;
		}

FINALLY_001f:
		{ // begin finally (depth: 2)
			NullCheck((OperatorObserverBase_2_t1187768149 *)__this);
			VirtActionInvoker0::Invoke(4 /* System.Void UniRx.Operators.OperatorObserverBase`2<System.Object,System.Object>::Dispose() */, (OperatorObserverBase_2_t1187768149 *)__this);
			IL2CPP_END_FINALLY(31)
		} // end finally (depth: 2)
		IL2CPP_CLEANUP(31)
		{
			IL2CPP_JUMP_TBL(0x26, IL_0026)
			IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
		}

IL_0026:
		{
			IL2CPP_LEAVE(0x32, FINALLY_002b);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_002b;
	}

FINALLY_002b:
	{ // begin finally (depth: 1)
		Il2CppObject * L_3 = V_0;
		Monitor_Exit_m2088237919(NULL /*static, unused*/, (Il2CppObject *)L_3, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(43)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(43)
	{
		IL2CPP_JUMP_TBL(0x32, IL_0032)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0032:
	{
		return;
	}
}
// System.Void UniRx.Operators.TakeUntilObservable`2/TakeUntil<System.Object,UniRx.Unit>::.ctor(UniRx.Operators.TakeUntilObservable`2<T,TOther>,UniRx.IObserver`1<T>,System.IDisposable)
extern Il2CppClass* Il2CppObject_il2cpp_TypeInfo_var;
extern const uint32_t TakeUntil__ctor_m966353105_MetadataUsageId;
extern "C"  void TakeUntil__ctor_m966353105_gshared (TakeUntil_t105161758 * __this, TakeUntilObservable_2_t1672410788 * ___parent0, Il2CppObject* ___observer1, Il2CppObject * ___cancel2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TakeUntil__ctor_m966353105_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject * L_0 = (Il2CppObject *)il2cpp_codegen_object_new(Il2CppObject_il2cpp_TypeInfo_var);
		Object__ctor_m1772956182(L_0, /*hidden argument*/NULL);
		__this->set_gate_3(L_0);
		Il2CppObject* L_1 = ___observer1;
		Il2CppObject * L_2 = ___cancel2;
		NullCheck((OperatorObserverBase_2_t1187768149 *)__this);
		((  void (*) (OperatorObserverBase_2_t1187768149 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((OperatorObserverBase_2_t1187768149 *)__this, (Il2CppObject*)L_1, (Il2CppObject *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		TakeUntilObservable_2_t1672410788 * L_3 = ___parent0;
		__this->set_parent_2(L_3);
		return;
	}
}
// System.IDisposable UniRx.Operators.TakeUntilObservable`2/TakeUntil<System.Object,UniRx.Unit>::Run()
extern Il2CppClass* SingleAssignmentDisposable_t2336378823_il2cpp_TypeInfo_var;
extern const uint32_t TakeUntil_Run_m1341935878_MetadataUsageId;
extern "C"  Il2CppObject * TakeUntil_Run_m1341935878_gshared (TakeUntil_t105161758 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TakeUntil_Run_m1341935878_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	SingleAssignmentDisposable_t2336378823 * V_0 = NULL;
	TakeUntilOther_t3425980548 * V_1 = NULL;
	Il2CppObject * V_2 = NULL;
	{
		SingleAssignmentDisposable_t2336378823 * L_0 = (SingleAssignmentDisposable_t2336378823 *)il2cpp_codegen_object_new(SingleAssignmentDisposable_t2336378823_il2cpp_TypeInfo_var);
		SingleAssignmentDisposable__ctor_m917449122(L_0, /*hidden argument*/NULL);
		V_0 = (SingleAssignmentDisposable_t2336378823 *)L_0;
		SingleAssignmentDisposable_t2336378823 * L_1 = V_0;
		TakeUntilOther_t3425980548 * L_2 = (TakeUntilOther_t3425980548 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		((  void (*) (TakeUntilOther_t3425980548 *, TakeUntil_t105161758 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)(L_2, (TakeUntil_t105161758 *)__this, (Il2CppObject *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		V_1 = (TakeUntilOther_t3425980548 *)L_2;
		SingleAssignmentDisposable_t2336378823 * L_3 = V_0;
		TakeUntilObservable_2_t1672410788 * L_4 = (TakeUntilObservable_2_t1672410788 *)__this->get_parent_2();
		NullCheck(L_4);
		Il2CppObject* L_5 = (Il2CppObject*)L_4->get_other_2();
		TakeUntilOther_t3425980548 * L_6 = V_1;
		NullCheck((Il2CppObject*)L_5);
		Il2CppObject * L_7 = InterfaceFuncInvoker1< Il2CppObject *, Il2CppObject* >::Invoke(0 /* System.IDisposable UniRx.IObservable`1<UniRx.Unit>::Subscribe(UniRx.IObserver`1<T>) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4), (Il2CppObject*)L_5, (Il2CppObject*)L_6);
		NullCheck((SingleAssignmentDisposable_t2336378823 *)L_3);
		SingleAssignmentDisposable_set_Disposable_m1599028885((SingleAssignmentDisposable_t2336378823 *)L_3, (Il2CppObject *)L_7, /*hidden argument*/NULL);
		TakeUntilObservable_2_t1672410788 * L_8 = (TakeUntilObservable_2_t1672410788 *)__this->get_parent_2();
		NullCheck(L_8);
		Il2CppObject* L_9 = (Il2CppObject*)L_8->get_source_1();
		NullCheck((Il2CppObject*)L_9);
		Il2CppObject * L_10 = InterfaceFuncInvoker1< Il2CppObject *, Il2CppObject* >::Invoke(0 /* System.IDisposable UniRx.IObservable`1<System.Object>::Subscribe(UniRx.IObserver`1<T>) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5), (Il2CppObject*)L_9, (Il2CppObject*)__this);
		V_2 = (Il2CppObject *)L_10;
		SingleAssignmentDisposable_t2336378823 * L_11 = V_0;
		Il2CppObject * L_12 = V_2;
		Il2CppObject * L_13 = StableCompositeDisposable_Create_m3764788535(NULL /*static, unused*/, (Il2CppObject *)L_11, (Il2CppObject *)L_12, /*hidden argument*/NULL);
		return L_13;
	}
}
// System.Void UniRx.Operators.TakeUntilObservable`2/TakeUntil<System.Object,UniRx.Unit>::OnNext(T)
extern "C"  void TakeUntil_OnNext_m740813130_gshared (TakeUntil_t105161758 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	Il2CppObject * V_0 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		bool L_0 = (bool)__this->get_open_4();
		if (!L_0)
		{
			goto IL_001e;
		}
	}
	{
		Il2CppObject* L_1 = (Il2CppObject*)((OperatorObserverBase_2_t1187768149 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		Il2CppObject * L_2 = ___value0;
		NullCheck((Il2CppObject*)L_1);
		InterfaceActionInvoker1< Il2CppObject * >::Invoke(2 /* System.Void UniRx.IObserver`1<System.Object>::OnNext(T) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6), (Il2CppObject*)L_1, (Il2CppObject *)L_2);
		goto IL_0045;
	}

IL_001e:
	{
		Il2CppObject * L_3 = (Il2CppObject *)__this->get_gate_3();
		V_0 = (Il2CppObject *)L_3;
		Il2CppObject * L_4 = V_0;
		Monitor_Enter_m476686225(NULL /*static, unused*/, (Il2CppObject *)L_4, /*hidden argument*/NULL);
	}

IL_002b:
	try
	{ // begin try (depth: 1)
		Il2CppObject* L_5 = (Il2CppObject*)((OperatorObserverBase_2_t1187768149 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		Il2CppObject * L_6 = ___value0;
		NullCheck((Il2CppObject*)L_5);
		InterfaceActionInvoker1< Il2CppObject * >::Invoke(2 /* System.Void UniRx.IObserver`1<System.Object>::OnNext(T) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6), (Il2CppObject*)L_5, (Il2CppObject *)L_6);
		IL2CPP_LEAVE(0x45, FINALLY_003e);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_003e;
	}

FINALLY_003e:
	{ // begin finally (depth: 1)
		Il2CppObject * L_7 = V_0;
		Monitor_Exit_m2088237919(NULL /*static, unused*/, (Il2CppObject *)L_7, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(62)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(62)
	{
		IL2CPP_JUMP_TBL(0x45, IL_0045)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0045:
	{
		return;
	}
}
// System.Void UniRx.Operators.TakeUntilObservable`2/TakeUntil<System.Object,UniRx.Unit>::OnError(System.Exception)
extern "C"  void TakeUntil_OnError_m3583258713_gshared (TakeUntil_t105161758 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method)
{
	Il2CppObject * V_0 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_gate_3();
		V_0 = (Il2CppObject *)L_0;
		Il2CppObject * L_1 = V_0;
		Monitor_Enter_m476686225(NULL /*static, unused*/, (Il2CppObject *)L_1, /*hidden argument*/NULL);
	}

IL_000d:
	try
	{ // begin try (depth: 1)
		try
		{ // begin try (depth: 2)
			Il2CppObject* L_2 = (Il2CppObject*)((OperatorObserverBase_2_t1187768149 *)__this)->get_observer_0();
			il2cpp_codegen_memory_barrier();
			Exception_t1967233988 * L_3 = ___error0;
			NullCheck((Il2CppObject*)L_2);
			InterfaceActionInvoker1< Exception_t1967233988 * >::Invoke(1 /* System.Void UniRx.IObserver`1<System.Object>::OnError(System.Exception) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6), (Il2CppObject*)L_2, (Exception_t1967233988 *)L_3);
			IL2CPP_LEAVE(0x27, FINALLY_0020);
		} // end try (depth: 2)
		catch(Il2CppExceptionWrapper& e)
		{
			__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
			goto FINALLY_0020;
		}

FINALLY_0020:
		{ // begin finally (depth: 2)
			NullCheck((OperatorObserverBase_2_t1187768149 *)__this);
			VirtActionInvoker0::Invoke(4 /* System.Void UniRx.Operators.OperatorObserverBase`2<System.Object,System.Object>::Dispose() */, (OperatorObserverBase_2_t1187768149 *)__this);
			IL2CPP_END_FINALLY(32)
		} // end finally (depth: 2)
		IL2CPP_CLEANUP(32)
		{
			IL2CPP_JUMP_TBL(0x27, IL_0027)
			IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
		}

IL_0027:
		{
			IL2CPP_LEAVE(0x33, FINALLY_002c);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_002c;
	}

FINALLY_002c:
	{ // begin finally (depth: 1)
		Il2CppObject * L_4 = V_0;
		Monitor_Exit_m2088237919(NULL /*static, unused*/, (Il2CppObject *)L_4, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(44)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(44)
	{
		IL2CPP_JUMP_TBL(0x33, IL_0033)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0033:
	{
		return;
	}
}
// System.Void UniRx.Operators.TakeUntilObservable`2/TakeUntil<System.Object,UniRx.Unit>::OnCompleted()
extern "C"  void TakeUntil_OnCompleted_m3979206956_gshared (TakeUntil_t105161758 * __this, const MethodInfo* method)
{
	Il2CppObject * V_0 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_gate_3();
		V_0 = (Il2CppObject *)L_0;
		Il2CppObject * L_1 = V_0;
		Monitor_Enter_m476686225(NULL /*static, unused*/, (Il2CppObject *)L_1, /*hidden argument*/NULL);
	}

IL_000d:
	try
	{ // begin try (depth: 1)
		try
		{ // begin try (depth: 2)
			Il2CppObject* L_2 = (Il2CppObject*)((OperatorObserverBase_2_t1187768149 *)__this)->get_observer_0();
			il2cpp_codegen_memory_barrier();
			NullCheck((Il2CppObject*)L_2);
			InterfaceActionInvoker0::Invoke(0 /* System.Void UniRx.IObserver`1<System.Object>::OnCompleted() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6), (Il2CppObject*)L_2);
			IL2CPP_LEAVE(0x26, FINALLY_001f);
		} // end try (depth: 2)
		catch(Il2CppExceptionWrapper& e)
		{
			__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
			goto FINALLY_001f;
		}

FINALLY_001f:
		{ // begin finally (depth: 2)
			NullCheck((OperatorObserverBase_2_t1187768149 *)__this);
			VirtActionInvoker0::Invoke(4 /* System.Void UniRx.Operators.OperatorObserverBase`2<System.Object,System.Object>::Dispose() */, (OperatorObserverBase_2_t1187768149 *)__this);
			IL2CPP_END_FINALLY(31)
		} // end finally (depth: 2)
		IL2CPP_CLEANUP(31)
		{
			IL2CPP_JUMP_TBL(0x26, IL_0026)
			IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
		}

IL_0026:
		{
			IL2CPP_LEAVE(0x32, FINALLY_002b);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_002b;
	}

FINALLY_002b:
	{ // begin finally (depth: 1)
		Il2CppObject * L_3 = V_0;
		Monitor_Exit_m2088237919(NULL /*static, unused*/, (Il2CppObject *)L_3, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(43)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(43)
	{
		IL2CPP_JUMP_TBL(0x32, IL_0032)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0032:
	{
		return;
	}
}
// System.Void UniRx.Operators.TakeUntilObservable`2/TakeUntil<UniRx.Unit,UniRx.Unit>::.ctor(UniRx.Operators.TakeUntilObservable`2<T,TOther>,UniRx.IObserver`1<T>,System.IDisposable)
extern Il2CppClass* Il2CppObject_il2cpp_TypeInfo_var;
extern const uint32_t TakeUntil__ctor_m288231061_MetadataUsageId;
extern "C"  void TakeUntil__ctor_m288231061_gshared (TakeUntil_t1361590388 * __this, TakeUntilObservable_2_t2928839418 * ___parent0, Il2CppObject* ___observer1, Il2CppObject * ___cancel2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TakeUntil__ctor_m288231061_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject * L_0 = (Il2CppObject *)il2cpp_codegen_object_new(Il2CppObject_il2cpp_TypeInfo_var);
		Object__ctor_m1772956182(L_0, /*hidden argument*/NULL);
		__this->set_gate_3(L_0);
		Il2CppObject* L_1 = ___observer1;
		Il2CppObject * L_2 = ___cancel2;
		NullCheck((OperatorObserverBase_2_t4165376397 *)__this);
		((  void (*) (OperatorObserverBase_2_t4165376397 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((OperatorObserverBase_2_t4165376397 *)__this, (Il2CppObject*)L_1, (Il2CppObject *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		TakeUntilObservable_2_t2928839418 * L_3 = ___parent0;
		__this->set_parent_2(L_3);
		return;
	}
}
// System.IDisposable UniRx.Operators.TakeUntilObservable`2/TakeUntil<UniRx.Unit,UniRx.Unit>::Run()
extern Il2CppClass* SingleAssignmentDisposable_t2336378823_il2cpp_TypeInfo_var;
extern const uint32_t TakeUntil_Run_m847578668_MetadataUsageId;
extern "C"  Il2CppObject * TakeUntil_Run_m847578668_gshared (TakeUntil_t1361590388 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TakeUntil_Run_m847578668_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	SingleAssignmentDisposable_t2336378823 * V_0 = NULL;
	TakeUntilOther_t387441882 * V_1 = NULL;
	Il2CppObject * V_2 = NULL;
	{
		SingleAssignmentDisposable_t2336378823 * L_0 = (SingleAssignmentDisposable_t2336378823 *)il2cpp_codegen_object_new(SingleAssignmentDisposable_t2336378823_il2cpp_TypeInfo_var);
		SingleAssignmentDisposable__ctor_m917449122(L_0, /*hidden argument*/NULL);
		V_0 = (SingleAssignmentDisposable_t2336378823 *)L_0;
		SingleAssignmentDisposable_t2336378823 * L_1 = V_0;
		TakeUntilOther_t387441882 * L_2 = (TakeUntilOther_t387441882 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		((  void (*) (TakeUntilOther_t387441882 *, TakeUntil_t1361590388 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)(L_2, (TakeUntil_t1361590388 *)__this, (Il2CppObject *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		V_1 = (TakeUntilOther_t387441882 *)L_2;
		SingleAssignmentDisposable_t2336378823 * L_3 = V_0;
		TakeUntilObservable_2_t2928839418 * L_4 = (TakeUntilObservable_2_t2928839418 *)__this->get_parent_2();
		NullCheck(L_4);
		Il2CppObject* L_5 = (Il2CppObject*)L_4->get_other_2();
		TakeUntilOther_t387441882 * L_6 = V_1;
		NullCheck((Il2CppObject*)L_5);
		Il2CppObject * L_7 = InterfaceFuncInvoker1< Il2CppObject *, Il2CppObject* >::Invoke(0 /* System.IDisposable UniRx.IObservable`1<UniRx.Unit>::Subscribe(UniRx.IObserver`1<T>) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4), (Il2CppObject*)L_5, (Il2CppObject*)L_6);
		NullCheck((SingleAssignmentDisposable_t2336378823 *)L_3);
		SingleAssignmentDisposable_set_Disposable_m1599028885((SingleAssignmentDisposable_t2336378823 *)L_3, (Il2CppObject *)L_7, /*hidden argument*/NULL);
		TakeUntilObservable_2_t2928839418 * L_8 = (TakeUntilObservable_2_t2928839418 *)__this->get_parent_2();
		NullCheck(L_8);
		Il2CppObject* L_9 = (Il2CppObject*)L_8->get_source_1();
		NullCheck((Il2CppObject*)L_9);
		Il2CppObject * L_10 = InterfaceFuncInvoker1< Il2CppObject *, Il2CppObject* >::Invoke(0 /* System.IDisposable UniRx.IObservable`1<UniRx.Unit>::Subscribe(UniRx.IObserver`1<T>) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5), (Il2CppObject*)L_9, (Il2CppObject*)__this);
		V_2 = (Il2CppObject *)L_10;
		SingleAssignmentDisposable_t2336378823 * L_11 = V_0;
		Il2CppObject * L_12 = V_2;
		Il2CppObject * L_13 = StableCompositeDisposable_Create_m3764788535(NULL /*static, unused*/, (Il2CppObject *)L_11, (Il2CppObject *)L_12, /*hidden argument*/NULL);
		return L_13;
	}
}
// System.Void UniRx.Operators.TakeUntilObservable`2/TakeUntil<UniRx.Unit,UniRx.Unit>::OnNext(T)
extern "C"  void TakeUntil_OnNext_m2567014662_gshared (TakeUntil_t1361590388 * __this, Unit_t2558286038  ___value0, const MethodInfo* method)
{
	Il2CppObject * V_0 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		bool L_0 = (bool)__this->get_open_4();
		if (!L_0)
		{
			goto IL_001e;
		}
	}
	{
		Il2CppObject* L_1 = (Il2CppObject*)((OperatorObserverBase_2_t4165376397 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		Unit_t2558286038  L_2 = ___value0;
		NullCheck((Il2CppObject*)L_1);
		InterfaceActionInvoker1< Unit_t2558286038  >::Invoke(2 /* System.Void UniRx.IObserver`1<UniRx.Unit>::OnNext(T) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6), (Il2CppObject*)L_1, (Unit_t2558286038 )L_2);
		goto IL_0045;
	}

IL_001e:
	{
		Il2CppObject * L_3 = (Il2CppObject *)__this->get_gate_3();
		V_0 = (Il2CppObject *)L_3;
		Il2CppObject * L_4 = V_0;
		Monitor_Enter_m476686225(NULL /*static, unused*/, (Il2CppObject *)L_4, /*hidden argument*/NULL);
	}

IL_002b:
	try
	{ // begin try (depth: 1)
		Il2CppObject* L_5 = (Il2CppObject*)((OperatorObserverBase_2_t4165376397 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		Unit_t2558286038  L_6 = ___value0;
		NullCheck((Il2CppObject*)L_5);
		InterfaceActionInvoker1< Unit_t2558286038  >::Invoke(2 /* System.Void UniRx.IObserver`1<UniRx.Unit>::OnNext(T) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6), (Il2CppObject*)L_5, (Unit_t2558286038 )L_6);
		IL2CPP_LEAVE(0x45, FINALLY_003e);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_003e;
	}

FINALLY_003e:
	{ // begin finally (depth: 1)
		Il2CppObject * L_7 = V_0;
		Monitor_Exit_m2088237919(NULL /*static, unused*/, (Il2CppObject *)L_7, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(62)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(62)
	{
		IL2CPP_JUMP_TBL(0x45, IL_0045)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0045:
	{
		return;
	}
}
// System.Void UniRx.Operators.TakeUntilObservable`2/TakeUntil<UniRx.Unit,UniRx.Unit>::OnError(System.Exception)
extern "C"  void TakeUntil_OnError_m772724757_gshared (TakeUntil_t1361590388 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method)
{
	Il2CppObject * V_0 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_gate_3();
		V_0 = (Il2CppObject *)L_0;
		Il2CppObject * L_1 = V_0;
		Monitor_Enter_m476686225(NULL /*static, unused*/, (Il2CppObject *)L_1, /*hidden argument*/NULL);
	}

IL_000d:
	try
	{ // begin try (depth: 1)
		try
		{ // begin try (depth: 2)
			Il2CppObject* L_2 = (Il2CppObject*)((OperatorObserverBase_2_t4165376397 *)__this)->get_observer_0();
			il2cpp_codegen_memory_barrier();
			Exception_t1967233988 * L_3 = ___error0;
			NullCheck((Il2CppObject*)L_2);
			InterfaceActionInvoker1< Exception_t1967233988 * >::Invoke(1 /* System.Void UniRx.IObserver`1<UniRx.Unit>::OnError(System.Exception) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6), (Il2CppObject*)L_2, (Exception_t1967233988 *)L_3);
			IL2CPP_LEAVE(0x27, FINALLY_0020);
		} // end try (depth: 2)
		catch(Il2CppExceptionWrapper& e)
		{
			__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
			goto FINALLY_0020;
		}

FINALLY_0020:
		{ // begin finally (depth: 2)
			NullCheck((OperatorObserverBase_2_t4165376397 *)__this);
			VirtActionInvoker0::Invoke(4 /* System.Void UniRx.Operators.OperatorObserverBase`2<UniRx.Unit,UniRx.Unit>::Dispose() */, (OperatorObserverBase_2_t4165376397 *)__this);
			IL2CPP_END_FINALLY(32)
		} // end finally (depth: 2)
		IL2CPP_CLEANUP(32)
		{
			IL2CPP_JUMP_TBL(0x27, IL_0027)
			IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
		}

IL_0027:
		{
			IL2CPP_LEAVE(0x33, FINALLY_002c);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_002c;
	}

FINALLY_002c:
	{ // begin finally (depth: 1)
		Il2CppObject * L_4 = V_0;
		Monitor_Exit_m2088237919(NULL /*static, unused*/, (Il2CppObject *)L_4, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(44)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(44)
	{
		IL2CPP_JUMP_TBL(0x33, IL_0033)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0033:
	{
		return;
	}
}
// System.Void UniRx.Operators.TakeUntilObservable`2/TakeUntil<UniRx.Unit,UniRx.Unit>::OnCompleted()
extern "C"  void TakeUntil_OnCompleted_m276382440_gshared (TakeUntil_t1361590388 * __this, const MethodInfo* method)
{
	Il2CppObject * V_0 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_gate_3();
		V_0 = (Il2CppObject *)L_0;
		Il2CppObject * L_1 = V_0;
		Monitor_Enter_m476686225(NULL /*static, unused*/, (Il2CppObject *)L_1, /*hidden argument*/NULL);
	}

IL_000d:
	try
	{ // begin try (depth: 1)
		try
		{ // begin try (depth: 2)
			Il2CppObject* L_2 = (Il2CppObject*)((OperatorObserverBase_2_t4165376397 *)__this)->get_observer_0();
			il2cpp_codegen_memory_barrier();
			NullCheck((Il2CppObject*)L_2);
			InterfaceActionInvoker0::Invoke(0 /* System.Void UniRx.IObserver`1<UniRx.Unit>::OnCompleted() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6), (Il2CppObject*)L_2);
			IL2CPP_LEAVE(0x26, FINALLY_001f);
		} // end try (depth: 2)
		catch(Il2CppExceptionWrapper& e)
		{
			__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
			goto FINALLY_001f;
		}

FINALLY_001f:
		{ // begin finally (depth: 2)
			NullCheck((OperatorObserverBase_2_t4165376397 *)__this);
			VirtActionInvoker0::Invoke(4 /* System.Void UniRx.Operators.OperatorObserverBase`2<UniRx.Unit,UniRx.Unit>::Dispose() */, (OperatorObserverBase_2_t4165376397 *)__this);
			IL2CPP_END_FINALLY(31)
		} // end finally (depth: 2)
		IL2CPP_CLEANUP(31)
		{
			IL2CPP_JUMP_TBL(0x26, IL_0026)
			IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
		}

IL_0026:
		{
			IL2CPP_LEAVE(0x32, FINALLY_002b);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_002b;
	}

FINALLY_002b:
	{ // begin finally (depth: 1)
		Il2CppObject * L_3 = V_0;
		Monitor_Exit_m2088237919(NULL /*static, unused*/, (Il2CppObject *)L_3, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(43)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(43)
	{
		IL2CPP_JUMP_TBL(0x32, IL_0032)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0032:
	{
		return;
	}
}
// System.Void UniRx.Operators.TakeUntilObservable`2<System.Object,System.Object>::.ctor(UniRx.IObservable`1<T>,UniRx.IObservable`1<TOther>)
extern "C"  void TakeUntilObservable_2__ctor_m2718047514_gshared (TakeUntilObservable_2_t4246198466 * __this, Il2CppObject* ___source0, Il2CppObject* ___other1, const MethodInfo* method)
{
	TakeUntilObservable_2_t4246198466 * G_B2_0 = NULL;
	TakeUntilObservable_2_t4246198466 * G_B1_0 = NULL;
	int32_t G_B3_0 = 0;
	TakeUntilObservable_2_t4246198466 * G_B3_1 = NULL;
	{
		Il2CppObject* L_0 = ___source0;
		bool L_1 = ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)(NULL /*static, unused*/, (Il2CppObject*)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		G_B1_0 = ((TakeUntilObservable_2_t4246198466 *)(__this));
		if (L_1)
		{
			G_B2_0 = ((TakeUntilObservable_2_t4246198466 *)(__this));
			goto IL_0014;
		}
	}
	{
		Il2CppObject* L_2 = ___other1;
		bool L_3 = ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)(NULL /*static, unused*/, (Il2CppObject*)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		G_B3_0 = ((int32_t)(L_3));
		G_B3_1 = ((TakeUntilObservable_2_t4246198466 *)(G_B1_0));
		goto IL_0015;
	}

IL_0014:
	{
		G_B3_0 = 1;
		G_B3_1 = ((TakeUntilObservable_2_t4246198466 *)(G_B2_0));
	}

IL_0015:
	{
		NullCheck((OperatorObservableBase_1_t4196218687 *)G_B3_1);
		((  void (*) (OperatorObservableBase_1_t4196218687 *, bool, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)((OperatorObservableBase_1_t4196218687 *)G_B3_1, (bool)G_B3_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		Il2CppObject* L_4 = ___source0;
		__this->set_source_1(L_4);
		Il2CppObject* L_5 = ___other1;
		__this->set_other_2(L_5);
		return;
	}
}
// System.IDisposable UniRx.Operators.TakeUntilObservable`2<System.Object,System.Object>::SubscribeCore(UniRx.IObserver`1<T>,System.IDisposable)
extern "C"  Il2CppObject * TakeUntilObservable_2_SubscribeCore_m3785682308_gshared (TakeUntilObservable_2_t4246198466 * __this, Il2CppObject* ___observer0, Il2CppObject * ___cancel1, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = ___observer0;
		Il2CppObject * L_1 = ___cancel1;
		TakeUntil_t2678949436 * L_2 = (TakeUntil_t2678949436 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		((  void (*) (TakeUntil_t2678949436 *, TakeUntilObservable_2_t4246198466 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->method)(L_2, (TakeUntilObservable_2_t4246198466 *)__this, (Il2CppObject*)L_0, (Il2CppObject *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		NullCheck((TakeUntil_t2678949436 *)L_2);
		Il2CppObject * L_3 = ((  Il2CppObject * (*) (TakeUntil_t2678949436 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6)->method)((TakeUntil_t2678949436 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		return L_3;
	}
}
// System.Void UniRx.Operators.TakeUntilObservable`2<System.Object,UniRx.Unit>::.ctor(UniRx.IObservable`1<T>,UniRx.IObservable`1<TOther>)
extern "C"  void TakeUntilObservable_2__ctor_m1530415626_gshared (TakeUntilObservable_2_t1672410788 * __this, Il2CppObject* ___source0, Il2CppObject* ___other1, const MethodInfo* method)
{
	TakeUntilObservable_2_t1672410788 * G_B2_0 = NULL;
	TakeUntilObservable_2_t1672410788 * G_B1_0 = NULL;
	int32_t G_B3_0 = 0;
	TakeUntilObservable_2_t1672410788 * G_B3_1 = NULL;
	{
		Il2CppObject* L_0 = ___source0;
		bool L_1 = ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)(NULL /*static, unused*/, (Il2CppObject*)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		G_B1_0 = ((TakeUntilObservable_2_t1672410788 *)(__this));
		if (L_1)
		{
			G_B2_0 = ((TakeUntilObservable_2_t1672410788 *)(__this));
			goto IL_0014;
		}
	}
	{
		Il2CppObject* L_2 = ___other1;
		bool L_3 = ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)(NULL /*static, unused*/, (Il2CppObject*)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		G_B3_0 = ((int32_t)(L_3));
		G_B3_1 = ((TakeUntilObservable_2_t1672410788 *)(G_B1_0));
		goto IL_0015;
	}

IL_0014:
	{
		G_B3_0 = 1;
		G_B3_1 = ((TakeUntilObservable_2_t1672410788 *)(G_B2_0));
	}

IL_0015:
	{
		NullCheck((OperatorObservableBase_1_t4196218687 *)G_B3_1);
		((  void (*) (OperatorObservableBase_1_t4196218687 *, bool, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)((OperatorObservableBase_1_t4196218687 *)G_B3_1, (bool)G_B3_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		Il2CppObject* L_4 = ___source0;
		__this->set_source_1(L_4);
		Il2CppObject* L_5 = ___other1;
		__this->set_other_2(L_5);
		return;
	}
}
// System.IDisposable UniRx.Operators.TakeUntilObservable`2<System.Object,UniRx.Unit>::SubscribeCore(UniRx.IObserver`1<T>,System.IDisposable)
extern "C"  Il2CppObject * TakeUntilObservable_2_SubscribeCore_m2305055102_gshared (TakeUntilObservable_2_t1672410788 * __this, Il2CppObject* ___observer0, Il2CppObject * ___cancel1, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = ___observer0;
		Il2CppObject * L_1 = ___cancel1;
		TakeUntil_t105161758 * L_2 = (TakeUntil_t105161758 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		((  void (*) (TakeUntil_t105161758 *, TakeUntilObservable_2_t1672410788 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->method)(L_2, (TakeUntilObservable_2_t1672410788 *)__this, (Il2CppObject*)L_0, (Il2CppObject *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		NullCheck((TakeUntil_t105161758 *)L_2);
		Il2CppObject * L_3 = ((  Il2CppObject * (*) (TakeUntil_t105161758 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6)->method)((TakeUntil_t105161758 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		return L_3;
	}
}
// System.Void UniRx.Operators.TakeUntilObservable`2<UniRx.Unit,UniRx.Unit>::.ctor(UniRx.IObservable`1<T>,UniRx.IObservable`1<TOther>)
extern "C"  void TakeUntilObservable_2__ctor_m3506290678_gshared (TakeUntilObservable_2_t2928839418 * __this, Il2CppObject* ___source0, Il2CppObject* ___other1, const MethodInfo* method)
{
	TakeUntilObservable_2_t2928839418 * G_B2_0 = NULL;
	TakeUntilObservable_2_t2928839418 * G_B1_0 = NULL;
	int32_t G_B3_0 = 0;
	TakeUntilObservable_2_t2928839418 * G_B3_1 = NULL;
	{
		Il2CppObject* L_0 = ___source0;
		bool L_1 = ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)(NULL /*static, unused*/, (Il2CppObject*)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		G_B1_0 = ((TakeUntilObservable_2_t2928839418 *)(__this));
		if (L_1)
		{
			G_B2_0 = ((TakeUntilObservable_2_t2928839418 *)(__this));
			goto IL_0014;
		}
	}
	{
		Il2CppObject* L_2 = ___other1;
		bool L_3 = ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)(NULL /*static, unused*/, (Il2CppObject*)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		G_B3_0 = ((int32_t)(L_3));
		G_B3_1 = ((TakeUntilObservable_2_t2928839418 *)(G_B1_0));
		goto IL_0015;
	}

IL_0014:
	{
		G_B3_0 = 1;
		G_B3_1 = ((TakeUntilObservable_2_t2928839418 *)(G_B2_0));
	}

IL_0015:
	{
		NullCheck((OperatorObservableBase_1_t1622431009 *)G_B3_1);
		((  void (*) (OperatorObservableBase_1_t1622431009 *, bool, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)((OperatorObservableBase_1_t1622431009 *)G_B3_1, (bool)G_B3_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		Il2CppObject* L_4 = ___source0;
		__this->set_source_1(L_4);
		Il2CppObject* L_5 = ___other1;
		__this->set_other_2(L_5);
		return;
	}
}
// System.IDisposable UniRx.Operators.TakeUntilObservable`2<UniRx.Unit,UniRx.Unit>::SubscribeCore(UniRx.IObserver`1<T>,System.IDisposable)
extern "C"  Il2CppObject * TakeUntilObservable_2_SubscribeCore_m1122537576_gshared (TakeUntilObservable_2_t2928839418 * __this, Il2CppObject* ___observer0, Il2CppObject * ___cancel1, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = ___observer0;
		Il2CppObject * L_1 = ___cancel1;
		TakeUntil_t1361590388 * L_2 = (TakeUntil_t1361590388 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		((  void (*) (TakeUntil_t1361590388 *, TakeUntilObservable_2_t2928839418 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->method)(L_2, (TakeUntilObservable_2_t2928839418 *)__this, (Il2CppObject*)L_0, (Il2CppObject *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		NullCheck((TakeUntil_t1361590388 *)L_2);
		Il2CppObject * L_3 = ((  Il2CppObject * (*) (TakeUntil_t1361590388 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6)->method)((TakeUntil_t1361590388 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		return L_3;
	}
}
// System.Void UniRx.Operators.TakeWhileObservable`1/TakeWhile_<System.Object>::.ctor(UniRx.Operators.TakeWhileObservable`1<T>,UniRx.IObserver`1<T>,System.IDisposable)
extern "C"  void TakeWhile___ctor_m2948940349_gshared (TakeWhile__t2842089094 * __this, TakeWhileObservable_1_t3525448012 * ___parent0, Il2CppObject* ___observer1, Il2CppObject * ___cancel2, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = ___observer1;
		Il2CppObject * L_1 = ___cancel2;
		NullCheck((OperatorObserverBase_2_t1187768149 *)__this);
		((  void (*) (OperatorObserverBase_2_t1187768149 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((OperatorObserverBase_2_t1187768149 *)__this, (Il2CppObject*)L_0, (Il2CppObject *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		TakeWhileObservable_1_t3525448012 * L_2 = ___parent0;
		__this->set_parent_2(L_2);
		return;
	}
}
// System.IDisposable UniRx.Operators.TakeWhileObservable`1/TakeWhile_<System.Object>::Run()
extern "C"  Il2CppObject * TakeWhile__Run_m3431004508_gshared (TakeWhile__t2842089094 * __this, const MethodInfo* method)
{
	{
		TakeWhileObservable_1_t3525448012 * L_0 = (TakeWhileObservable_1_t3525448012 *)__this->get_parent_2();
		NullCheck(L_0);
		Il2CppObject* L_1 = (Il2CppObject*)L_0->get_source_1();
		NullCheck((Il2CppObject*)L_1);
		Il2CppObject * L_2 = InterfaceFuncInvoker1< Il2CppObject *, Il2CppObject* >::Invoke(0 /* System.IDisposable UniRx.IObservable`1<System.Object>::Subscribe(UniRx.IObserver`1<T>) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), (Il2CppObject*)L_1, (Il2CppObject*)__this);
		return L_2;
	}
}
// System.Void UniRx.Operators.TakeWhileObservable`1/TakeWhile_<System.Object>::OnNext(T)
extern Il2CppClass* Exception_t1967233988_il2cpp_TypeInfo_var;
extern const uint32_t TakeWhile__OnNext_m1156978912_MetadataUsageId;
extern "C"  void TakeWhile__OnNext_m1156978912_gshared (TakeWhile__t2842089094 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TakeWhile__OnNext_m1156978912_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	bool V_0 = false;
	Exception_t1967233988 * V_1 = NULL;
	int32_t V_2 = 0;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		TakeWhileObservable_1_t3525448012 * L_0 = (TakeWhileObservable_1_t3525448012 *)__this->get_parent_2();
		NullCheck(L_0);
		Func_3_t3064567499 * L_1 = (Func_3_t3064567499 *)L_0->get_predicateWithIndex_3();
		Il2CppObject * L_2 = ___value0;
		int32_t L_3 = (int32_t)__this->get_index_3();
		int32_t L_4 = (int32_t)L_3;
		V_2 = (int32_t)L_4;
		__this->set_index_3(((int32_t)((int32_t)L_4+(int32_t)1)));
		int32_t L_5 = V_2;
		NullCheck((Func_3_t3064567499 *)L_1);
		bool L_6 = ((  bool (*) (Func_3_t3064567499 *, Il2CppObject *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)((Func_3_t3064567499 *)L_1, (Il2CppObject *)L_2, (int32_t)L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		V_0 = (bool)L_6;
		goto IL_004d;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1967233988 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Exception_t1967233988_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_0028;
		throw e;
	}

CATCH_0028:
	{ // begin catch(System.Exception)
		{
			V_1 = (Exception_t1967233988 *)((Exception_t1967233988 *)__exception_local);
		}

IL_0029:
		try
		{ // begin try (depth: 2)
			Il2CppObject* L_7 = (Il2CppObject*)((OperatorObserverBase_2_t1187768149 *)__this)->get_observer_0();
			il2cpp_codegen_memory_barrier();
			Exception_t1967233988 * L_8 = V_1;
			NullCheck((Il2CppObject*)L_7);
			InterfaceActionInvoker1< Exception_t1967233988 * >::Invoke(1 /* System.Void UniRx.IObserver`1<System.Object>::OnError(System.Exception) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4), (Il2CppObject*)L_7, (Exception_t1967233988 *)L_8);
			IL2CPP_LEAVE(0x43, FINALLY_003c);
		} // end try (depth: 2)
		catch(Il2CppExceptionWrapper& e)
		{
			__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
			goto FINALLY_003c;
		}

FINALLY_003c:
		{ // begin finally (depth: 2)
			NullCheck((OperatorObserverBase_2_t1187768149 *)__this);
			VirtActionInvoker0::Invoke(4 /* System.Void UniRx.Operators.OperatorObserverBase`2<System.Object,System.Object>::Dispose() */, (OperatorObserverBase_2_t1187768149 *)__this);
			IL2CPP_END_FINALLY(60)
		} // end finally (depth: 2)
		IL2CPP_CLEANUP(60)
		{
			IL2CPP_JUMP_TBL(0x43, IL_0043)
			IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
		}

IL_0043:
		{
			goto IL_007f;
		}

IL_0048:
		{
			; // IL_0048: leave IL_004d
		}
	} // end catch (depth: 1)

IL_004d:
	{
		bool L_9 = V_0;
		if (!L_9)
		{
			goto IL_0066;
		}
	}
	{
		Il2CppObject* L_10 = (Il2CppObject*)((OperatorObserverBase_2_t1187768149 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		Il2CppObject * L_11 = ___value0;
		NullCheck((Il2CppObject*)L_10);
		InterfaceActionInvoker1< Il2CppObject * >::Invoke(2 /* System.Void UniRx.IObserver`1<System.Object>::OnNext(T) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4), (Il2CppObject*)L_10, (Il2CppObject *)L_11);
		goto IL_007f;
	}

IL_0066:
	try
	{ // begin try (depth: 1)
		Il2CppObject* L_12 = (Il2CppObject*)((OperatorObserverBase_2_t1187768149 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		NullCheck((Il2CppObject*)L_12);
		InterfaceActionInvoker0::Invoke(0 /* System.Void UniRx.IObserver`1<System.Object>::OnCompleted() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4), (Il2CppObject*)L_12);
		IL2CPP_LEAVE(0x7F, FINALLY_0078);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0078;
	}

FINALLY_0078:
	{ // begin finally (depth: 1)
		NullCheck((OperatorObserverBase_2_t1187768149 *)__this);
		VirtActionInvoker0::Invoke(4 /* System.Void UniRx.Operators.OperatorObserverBase`2<System.Object,System.Object>::Dispose() */, (OperatorObserverBase_2_t1187768149 *)__this);
		IL2CPP_END_FINALLY(120)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(120)
	{
		IL2CPP_JUMP_TBL(0x7F, IL_007f)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_007f:
	{
		return;
	}
}
// System.Void UniRx.Operators.TakeWhileObservable`1/TakeWhile_<System.Object>::OnError(System.Exception)
extern "C"  void TakeWhile__OnError_m95187951_gshared (TakeWhile__t2842089094 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method)
{
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		Il2CppObject* L_0 = (Il2CppObject*)((OperatorObserverBase_2_t1187768149 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		Exception_t1967233988 * L_1 = ___error0;
		NullCheck((Il2CppObject*)L_0);
		InterfaceActionInvoker1< Exception_t1967233988 * >::Invoke(1 /* System.Void UniRx.IObserver`1<System.Object>::OnError(System.Exception) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4), (Il2CppObject*)L_0, (Exception_t1967233988 *)L_1);
		IL2CPP_LEAVE(0x1A, FINALLY_0013);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0013;
	}

FINALLY_0013:
	{ // begin finally (depth: 1)
		NullCheck((OperatorObserverBase_2_t1187768149 *)__this);
		VirtActionInvoker0::Invoke(4 /* System.Void UniRx.Operators.OperatorObserverBase`2<System.Object,System.Object>::Dispose() */, (OperatorObserverBase_2_t1187768149 *)__this);
		IL2CPP_END_FINALLY(19)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(19)
	{
		IL2CPP_JUMP_TBL(0x1A, IL_001a)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_001a:
	{
		return;
	}
}
// System.Void UniRx.Operators.TakeWhileObservable`1/TakeWhile_<System.Object>::OnCompleted()
extern "C"  void TakeWhile__OnCompleted_m2374915522_gshared (TakeWhile__t2842089094 * __this, const MethodInfo* method)
{
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		Il2CppObject* L_0 = (Il2CppObject*)((OperatorObserverBase_2_t1187768149 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		NullCheck((Il2CppObject*)L_0);
		InterfaceActionInvoker0::Invoke(0 /* System.Void UniRx.IObserver`1<System.Object>::OnCompleted() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4), (Il2CppObject*)L_0);
		IL2CPP_LEAVE(0x19, FINALLY_0012);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0012;
	}

FINALLY_0012:
	{ // begin finally (depth: 1)
		NullCheck((OperatorObserverBase_2_t1187768149 *)__this);
		VirtActionInvoker0::Invoke(4 /* System.Void UniRx.Operators.OperatorObserverBase`2<System.Object,System.Object>::Dispose() */, (OperatorObserverBase_2_t1187768149 *)__this);
		IL2CPP_END_FINALLY(18)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(18)
	{
		IL2CPP_JUMP_TBL(0x19, IL_0019)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0019:
	{
		return;
	}
}
// System.Void UniRx.Operators.TakeWhileObservable`1/TakeWhile<System.Object>::.ctor(UniRx.Operators.TakeWhileObservable`1<T>,UniRx.IObserver`1<T>,System.IDisposable)
extern "C"  void TakeWhile__ctor_m2870044218_gshared (TakeWhile_t3591352755 * __this, TakeWhileObservable_1_t3525448012 * ___parent0, Il2CppObject* ___observer1, Il2CppObject * ___cancel2, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = ___observer1;
		Il2CppObject * L_1 = ___cancel2;
		NullCheck((OperatorObserverBase_2_t1187768149 *)__this);
		((  void (*) (OperatorObserverBase_2_t1187768149 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((OperatorObserverBase_2_t1187768149 *)__this, (Il2CppObject*)L_0, (Il2CppObject *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		TakeWhileObservable_1_t3525448012 * L_2 = ___parent0;
		__this->set_parent_2(L_2);
		return;
	}
}
// System.IDisposable UniRx.Operators.TakeWhileObservable`1/TakeWhile<System.Object>::Run()
extern "C"  Il2CppObject * TakeWhile_Run_m201623875_gshared (TakeWhile_t3591352755 * __this, const MethodInfo* method)
{
	{
		TakeWhileObservable_1_t3525448012 * L_0 = (TakeWhileObservable_1_t3525448012 *)__this->get_parent_2();
		NullCheck(L_0);
		Il2CppObject* L_1 = (Il2CppObject*)L_0->get_source_1();
		NullCheck((Il2CppObject*)L_1);
		Il2CppObject * L_2 = InterfaceFuncInvoker1< Il2CppObject *, Il2CppObject* >::Invoke(0 /* System.IDisposable UniRx.IObservable`1<System.Object>::Subscribe(UniRx.IObserver`1<T>) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), (Il2CppObject*)L_1, (Il2CppObject*)__this);
		return L_2;
	}
}
// System.Void UniRx.Operators.TakeWhileObservable`1/TakeWhile<System.Object>::OnNext(T)
extern Il2CppClass* Exception_t1967233988_il2cpp_TypeInfo_var;
extern const uint32_t TakeWhile_OnNext_m980988829_MetadataUsageId;
extern "C"  void TakeWhile_OnNext_m980988829_gshared (TakeWhile_t3591352755 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TakeWhile_OnNext_m980988829_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	bool V_0 = false;
	Exception_t1967233988 * V_1 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		TakeWhileObservable_1_t3525448012 * L_0 = (TakeWhileObservable_1_t3525448012 *)__this->get_parent_2();
		NullCheck(L_0);
		Func_2_t1509682273 * L_1 = (Func_2_t1509682273 *)L_0->get_predicate_2();
		Il2CppObject * L_2 = ___value0;
		NullCheck((Func_2_t1509682273 *)L_1);
		bool L_3 = ((  bool (*) (Func_2_t1509682273 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)((Func_2_t1509682273 *)L_1, (Il2CppObject *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		V_0 = (bool)L_3;
		goto IL_003c;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1967233988 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Exception_t1967233988_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_0017;
		throw e;
	}

CATCH_0017:
	{ // begin catch(System.Exception)
		{
			V_1 = (Exception_t1967233988 *)((Exception_t1967233988 *)__exception_local);
		}

IL_0018:
		try
		{ // begin try (depth: 2)
			Il2CppObject* L_4 = (Il2CppObject*)((OperatorObserverBase_2_t1187768149 *)__this)->get_observer_0();
			il2cpp_codegen_memory_barrier();
			Exception_t1967233988 * L_5 = V_1;
			NullCheck((Il2CppObject*)L_4);
			InterfaceActionInvoker1< Exception_t1967233988 * >::Invoke(1 /* System.Void UniRx.IObserver`1<System.Object>::OnError(System.Exception) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4), (Il2CppObject*)L_4, (Exception_t1967233988 *)L_5);
			IL2CPP_LEAVE(0x32, FINALLY_002b);
		} // end try (depth: 2)
		catch(Il2CppExceptionWrapper& e)
		{
			__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
			goto FINALLY_002b;
		}

FINALLY_002b:
		{ // begin finally (depth: 2)
			NullCheck((OperatorObserverBase_2_t1187768149 *)__this);
			VirtActionInvoker0::Invoke(4 /* System.Void UniRx.Operators.OperatorObserverBase`2<System.Object,System.Object>::Dispose() */, (OperatorObserverBase_2_t1187768149 *)__this);
			IL2CPP_END_FINALLY(43)
		} // end finally (depth: 2)
		IL2CPP_CLEANUP(43)
		{
			IL2CPP_JUMP_TBL(0x32, IL_0032)
			IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
		}

IL_0032:
		{
			goto IL_006e;
		}

IL_0037:
		{
			; // IL_0037: leave IL_003c
		}
	} // end catch (depth: 1)

IL_003c:
	{
		bool L_6 = V_0;
		if (!L_6)
		{
			goto IL_0055;
		}
	}
	{
		Il2CppObject* L_7 = (Il2CppObject*)((OperatorObserverBase_2_t1187768149 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		Il2CppObject * L_8 = ___value0;
		NullCheck((Il2CppObject*)L_7);
		InterfaceActionInvoker1< Il2CppObject * >::Invoke(2 /* System.Void UniRx.IObserver`1<System.Object>::OnNext(T) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4), (Il2CppObject*)L_7, (Il2CppObject *)L_8);
		goto IL_006e;
	}

IL_0055:
	try
	{ // begin try (depth: 1)
		Il2CppObject* L_9 = (Il2CppObject*)((OperatorObserverBase_2_t1187768149 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		NullCheck((Il2CppObject*)L_9);
		InterfaceActionInvoker0::Invoke(0 /* System.Void UniRx.IObserver`1<System.Object>::OnCompleted() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4), (Il2CppObject*)L_9);
		IL2CPP_LEAVE(0x6E, FINALLY_0067);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0067;
	}

FINALLY_0067:
	{ // begin finally (depth: 1)
		NullCheck((OperatorObserverBase_2_t1187768149 *)__this);
		VirtActionInvoker0::Invoke(4 /* System.Void UniRx.Operators.OperatorObserverBase`2<System.Object,System.Object>::Dispose() */, (OperatorObserverBase_2_t1187768149 *)__this);
		IL2CPP_END_FINALLY(103)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(103)
	{
		IL2CPP_JUMP_TBL(0x6E, IL_006e)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_006e:
	{
		return;
	}
}
// System.Void UniRx.Operators.TakeWhileObservable`1/TakeWhile<System.Object>::OnError(System.Exception)
extern "C"  void TakeWhile_OnError_m1948513452_gshared (TakeWhile_t3591352755 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method)
{
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		Il2CppObject* L_0 = (Il2CppObject*)((OperatorObserverBase_2_t1187768149 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		Exception_t1967233988 * L_1 = ___error0;
		NullCheck((Il2CppObject*)L_0);
		InterfaceActionInvoker1< Exception_t1967233988 * >::Invoke(1 /* System.Void UniRx.IObserver`1<System.Object>::OnError(System.Exception) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4), (Il2CppObject*)L_0, (Exception_t1967233988 *)L_1);
		IL2CPP_LEAVE(0x1A, FINALLY_0013);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0013;
	}

FINALLY_0013:
	{ // begin finally (depth: 1)
		NullCheck((OperatorObserverBase_2_t1187768149 *)__this);
		VirtActionInvoker0::Invoke(4 /* System.Void UniRx.Operators.OperatorObserverBase`2<System.Object,System.Object>::Dispose() */, (OperatorObserverBase_2_t1187768149 *)__this);
		IL2CPP_END_FINALLY(19)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(19)
	{
		IL2CPP_JUMP_TBL(0x1A, IL_001a)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_001a:
	{
		return;
	}
}
// System.Void UniRx.Operators.TakeWhileObservable`1/TakeWhile<System.Object>::OnCompleted()
extern "C"  void TakeWhile_OnCompleted_m1989888511_gshared (TakeWhile_t3591352755 * __this, const MethodInfo* method)
{
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		Il2CppObject* L_0 = (Il2CppObject*)((OperatorObserverBase_2_t1187768149 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		NullCheck((Il2CppObject*)L_0);
		InterfaceActionInvoker0::Invoke(0 /* System.Void UniRx.IObserver`1<System.Object>::OnCompleted() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4), (Il2CppObject*)L_0);
		IL2CPP_LEAVE(0x19, FINALLY_0012);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0012;
	}

FINALLY_0012:
	{ // begin finally (depth: 1)
		NullCheck((OperatorObserverBase_2_t1187768149 *)__this);
		VirtActionInvoker0::Invoke(4 /* System.Void UniRx.Operators.OperatorObserverBase`2<System.Object,System.Object>::Dispose() */, (OperatorObserverBase_2_t1187768149 *)__this);
		IL2CPP_END_FINALLY(18)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(18)
	{
		IL2CPP_JUMP_TBL(0x19, IL_0019)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0019:
	{
		return;
	}
}
// System.Void UniRx.Operators.TakeWhileObservable`1<System.Object>::.ctor(UniRx.IObservable`1<T>,System.Func`2<T,System.Boolean>)
extern "C"  void TakeWhileObservable_1__ctor_m3525215727_gshared (TakeWhileObservable_1_t3525448012 * __this, Il2CppObject* ___source0, Func_2_t1509682273 * ___predicate1, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = ___source0;
		bool L_1 = ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)(NULL /*static, unused*/, (Il2CppObject*)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		NullCheck((OperatorObservableBase_1_t4196218687 *)__this);
		((  void (*) (OperatorObservableBase_1_t4196218687 *, bool, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)((OperatorObservableBase_1_t4196218687 *)__this, (bool)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		Il2CppObject* L_2 = ___source0;
		__this->set_source_1(L_2);
		Func_2_t1509682273 * L_3 = ___predicate1;
		__this->set_predicate_2(L_3);
		return;
	}
}
// System.Void UniRx.Operators.TakeWhileObservable`1<System.Object>::.ctor(UniRx.IObservable`1<T>,System.Func`3<T,System.Int32,System.Boolean>)
extern "C"  void TakeWhileObservable_1__ctor_m2999732631_gshared (TakeWhileObservable_1_t3525448012 * __this, Il2CppObject* ___source0, Func_3_t3064567499 * ___predicateWithIndex1, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = ___source0;
		bool L_1 = ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)(NULL /*static, unused*/, (Il2CppObject*)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		NullCheck((OperatorObservableBase_1_t4196218687 *)__this);
		((  void (*) (OperatorObservableBase_1_t4196218687 *, bool, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)((OperatorObservableBase_1_t4196218687 *)__this, (bool)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		Il2CppObject* L_2 = ___source0;
		__this->set_source_1(L_2);
		Func_3_t3064567499 * L_3 = ___predicateWithIndex1;
		__this->set_predicateWithIndex_3(L_3);
		return;
	}
}
// System.IDisposable UniRx.Operators.TakeWhileObservable`1<System.Object>::SubscribeCore(UniRx.IObserver`1<T>,System.IDisposable)
extern "C"  Il2CppObject * TakeWhileObservable_1_SubscribeCore_m420399844_gshared (TakeWhileObservable_1_t3525448012 * __this, Il2CppObject* ___observer0, Il2CppObject * ___cancel1, const MethodInfo* method)
{
	{
		Func_2_t1509682273 * L_0 = (Func_2_t1509682273 *)__this->get_predicate_2();
		if (!L_0)
		{
			goto IL_0019;
		}
	}
	{
		Il2CppObject* L_1 = ___observer0;
		Il2CppObject * L_2 = ___cancel1;
		TakeWhile_t3591352755 * L_3 = (TakeWhile_t3591352755 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		((  void (*) (TakeWhile_t3591352755 *, TakeWhileObservable_1_t3525448012 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)(L_3, (TakeWhileObservable_1_t3525448012 *)__this, (Il2CppObject*)L_1, (Il2CppObject *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		NullCheck((TakeWhile_t3591352755 *)L_3);
		Il2CppObject * L_4 = ((  Il2CppObject * (*) (TakeWhile_t3591352755 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->method)((TakeWhile_t3591352755 *)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		return L_4;
	}

IL_0019:
	{
		Il2CppObject* L_5 = ___observer0;
		Il2CppObject * L_6 = ___cancel1;
		TakeWhile__t2842089094 * L_7 = (TakeWhile__t2842089094 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		((  void (*) (TakeWhile__t2842089094 *, TakeWhileObservable_1_t3525448012 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7)->method)(L_7, (TakeWhileObservable_1_t3525448012 *)__this, (Il2CppObject*)L_5, (Il2CppObject *)L_6, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7));
		NullCheck((TakeWhile__t2842089094 *)L_7);
		Il2CppObject * L_8 = ((  Il2CppObject * (*) (TakeWhile__t2842089094 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8)->method)((TakeWhile__t2842089094 *)L_7, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8));
		return L_8;
	}
}
// System.Void UniRx.Operators.ThrottleFirstFrameObservable`1/ThrottleFirstFrame/ThrottleFirstFrameTick<System.Object>::.ctor(UniRx.Operators.ThrottleFirstFrameObservable`1/ThrottleFirstFrame<T>)
extern "C"  void ThrottleFirstFrameTick__ctor_m2743899176_gshared (ThrottleFirstFrameTick_t2464530589 * __this, ThrottleFirstFrame_t517244520 * ___parent0, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		ThrottleFirstFrame_t517244520 * L_0 = ___parent0;
		__this->set_parent_0(L_0);
		return;
	}
}
// System.Void UniRx.Operators.ThrottleFirstFrameObservable`1/ThrottleFirstFrame/ThrottleFirstFrameTick<System.Object>::OnCompleted()
extern "C"  void ThrottleFirstFrameTick_OnCompleted_m665198516_gshared (ThrottleFirstFrameTick_t2464530589 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void UniRx.Operators.ThrottleFirstFrameObservable`1/ThrottleFirstFrame/ThrottleFirstFrameTick<System.Object>::OnError(System.Exception)
extern "C"  void ThrottleFirstFrameTick_OnError_m2369446625_gshared (ThrottleFirstFrameTick_t2464530589 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void UniRx.Operators.ThrottleFirstFrameObservable`1/ThrottleFirstFrame/ThrottleFirstFrameTick<System.Object>::OnNext(System.Int64)
extern "C"  void ThrottleFirstFrameTick_OnNext_m2155104734_gshared (ThrottleFirstFrameTick_t2464530589 * __this, int64_t ____0, const MethodInfo* method)
{
	Il2CppObject * V_0 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		ThrottleFirstFrame_t517244520 * L_0 = (ThrottleFirstFrame_t517244520 *)__this->get_parent_0();
		NullCheck(L_0);
		Il2CppObject * L_1 = (Il2CppObject *)L_0->get_gate_3();
		V_0 = (Il2CppObject *)L_1;
		Il2CppObject * L_2 = V_0;
		Monitor_Enter_m476686225(NULL /*static, unused*/, (Il2CppObject *)L_2, /*hidden argument*/NULL);
	}

IL_0012:
	try
	{ // begin try (depth: 1)
		ThrottleFirstFrame_t517244520 * L_3 = (ThrottleFirstFrame_t517244520 *)__this->get_parent_0();
		NullCheck(L_3);
		L_3->set_open_4((bool)1);
		IL2CPP_LEAVE(0x2A, FINALLY_0023);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0023;
	}

FINALLY_0023:
	{ // begin finally (depth: 1)
		Il2CppObject * L_4 = V_0;
		Monitor_Exit_m2088237919(NULL /*static, unused*/, (Il2CppObject *)L_4, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(35)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(35)
	{
		IL2CPP_JUMP_TBL(0x2A, IL_002a)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_002a:
	{
		return;
	}
}
// System.Void UniRx.Operators.ThrottleFirstFrameObservable`1/ThrottleFirstFrame<System.Object>::.ctor(UniRx.Operators.ThrottleFirstFrameObservable`1<T>,UniRx.IObserver`1<T>,System.IDisposable)
extern Il2CppClass* Il2CppObject_il2cpp_TypeInfo_var;
extern const uint32_t ThrottleFirstFrame__ctor_m2720474635_MetadataUsageId;
extern "C"  void ThrottleFirstFrame__ctor_m2720474635_gshared (ThrottleFirstFrame_t517244520 * __this, ThrottleFirstFrameObservable_1_t955022977 * ___parent0, Il2CppObject* ___observer1, Il2CppObject * ___cancel2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ThrottleFirstFrame__ctor_m2720474635_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject * L_0 = (Il2CppObject *)il2cpp_codegen_object_new(Il2CppObject_il2cpp_TypeInfo_var);
		Object__ctor_m1772956182(L_0, /*hidden argument*/NULL);
		__this->set_gate_3(L_0);
		__this->set_open_4((bool)1);
		Il2CppObject* L_1 = ___observer1;
		Il2CppObject * L_2 = ___cancel2;
		NullCheck((OperatorObserverBase_2_t1187768149 *)__this);
		((  void (*) (OperatorObserverBase_2_t1187768149 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((OperatorObserverBase_2_t1187768149 *)__this, (Il2CppObject*)L_1, (Il2CppObject *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		ThrottleFirstFrameObservable_1_t955022977 * L_3 = ___parent0;
		__this->set_parent_2(L_3);
		return;
	}
}
// System.IDisposable UniRx.Operators.ThrottleFirstFrameObservable`1/ThrottleFirstFrame<System.Object>::Run()
extern Il2CppClass* SerialDisposable_t2547852742_il2cpp_TypeInfo_var;
extern const uint32_t ThrottleFirstFrame_Run_m1300998053_MetadataUsageId;
extern "C"  Il2CppObject * ThrottleFirstFrame_Run_m1300998053_gshared (ThrottleFirstFrame_t517244520 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ThrottleFirstFrame_Run_m1300998053_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Il2CppObject * V_0 = NULL;
	{
		SerialDisposable_t2547852742 * L_0 = (SerialDisposable_t2547852742 *)il2cpp_codegen_object_new(SerialDisposable_t2547852742_il2cpp_TypeInfo_var);
		SerialDisposable__ctor_m2977852291(L_0, /*hidden argument*/NULL);
		__this->set_cancelable_5(L_0);
		ThrottleFirstFrameObservable_1_t955022977 * L_1 = (ThrottleFirstFrameObservable_1_t955022977 *)__this->get_parent_2();
		NullCheck(L_1);
		Il2CppObject* L_2 = (Il2CppObject*)L_1->get_source_1();
		NullCheck((Il2CppObject*)L_2);
		Il2CppObject * L_3 = InterfaceFuncInvoker1< Il2CppObject *, Il2CppObject* >::Invoke(0 /* System.IDisposable UniRx.IObservable`1<System.Object>::Subscribe(UniRx.IObserver`1<T>) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), (Il2CppObject*)L_2, (Il2CppObject*)__this);
		V_0 = (Il2CppObject *)L_3;
		ThrottleFirstFrameTick_t2464530589 * L_4 = (ThrottleFirstFrameTick_t2464530589 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		((  void (*) (ThrottleFirstFrameTick_t2464530589 *, ThrottleFirstFrame_t517244520 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)(L_4, (ThrottleFirstFrame_t517244520 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		__this->set_tick_6(L_4);
		SerialDisposable_t2547852742 * L_5 = (SerialDisposable_t2547852742 *)__this->get_cancelable_5();
		Il2CppObject * L_6 = V_0;
		Il2CppObject * L_7 = StableCompositeDisposable_Create_m3764788535(NULL /*static, unused*/, (Il2CppObject *)L_5, (Il2CppObject *)L_6, /*hidden argument*/NULL);
		return L_7;
	}
}
// System.Void UniRx.Operators.ThrottleFirstFrameObservable`1/ThrottleFirstFrame<System.Object>::OnNext()
extern "C"  void ThrottleFirstFrame_OnNext_m3030201599_gshared (ThrottleFirstFrame_t517244520 * __this, const MethodInfo* method)
{
	Il2CppObject * V_0 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_gate_3();
		V_0 = (Il2CppObject *)L_0;
		Il2CppObject * L_1 = V_0;
		Monitor_Enter_m476686225(NULL /*static, unused*/, (Il2CppObject *)L_1, /*hidden argument*/NULL);
	}

IL_000d:
	try
	{ // begin try (depth: 1)
		__this->set_open_4((bool)1);
		IL2CPP_LEAVE(0x20, FINALLY_0019);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0019;
	}

FINALLY_0019:
	{ // begin finally (depth: 1)
		Il2CppObject * L_2 = V_0;
		Monitor_Exit_m2088237919(NULL /*static, unused*/, (Il2CppObject *)L_2, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(25)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(25)
	{
		IL2CPP_JUMP_TBL(0x20, IL_0020)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0020:
	{
		return;
	}
}
// System.Void UniRx.Operators.ThrottleFirstFrameObservable`1/ThrottleFirstFrame<System.Object>::OnNext(T)
extern Il2CppClass* SingleAssignmentDisposable_t2336378823_il2cpp_TypeInfo_var;
extern Il2CppClass* Observable_t258071701_il2cpp_TypeInfo_var;
extern Il2CppClass* IObservable_1_t2606213246_il2cpp_TypeInfo_var;
extern const uint32_t ThrottleFirstFrame_OnNext_m3741937727_MetadataUsageId;
extern "C"  void ThrottleFirstFrame_OnNext_m3741937727_gshared (ThrottleFirstFrame_t517244520 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ThrottleFirstFrame_OnNext_m3741937727_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Il2CppObject * V_0 = NULL;
	SingleAssignmentDisposable_t2336378823 * V_1 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_gate_3();
		V_0 = (Il2CppObject *)L_0;
		Il2CppObject * L_1 = V_0;
		Monitor_Enter_m476686225(NULL /*static, unused*/, (Il2CppObject *)L_1, /*hidden argument*/NULL);
	}

IL_000d:
	try
	{ // begin try (depth: 1)
		{
			bool L_2 = (bool)__this->get_open_4();
			if (L_2)
			{
				goto IL_001d;
			}
		}

IL_0018:
		{
			IL2CPP_LEAVE(0x7C, FINALLY_0037);
		}

IL_001d:
		{
			Il2CppObject* L_3 = (Il2CppObject*)((OperatorObserverBase_2_t1187768149 *)__this)->get_observer_0();
			il2cpp_codegen_memory_barrier();
			Il2CppObject * L_4 = ___value0;
			NullCheck((Il2CppObject*)L_3);
			InterfaceActionInvoker1< Il2CppObject * >::Invoke(2 /* System.Void UniRx.IObserver`1<System.Object>::OnNext(T) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5), (Il2CppObject*)L_3, (Il2CppObject *)L_4);
			__this->set_open_4((bool)0);
			IL2CPP_LEAVE(0x3E, FINALLY_0037);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0037;
	}

FINALLY_0037:
	{ // begin finally (depth: 1)
		Il2CppObject * L_5 = V_0;
		Monitor_Exit_m2088237919(NULL /*static, unused*/, (Il2CppObject *)L_5, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(55)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(55)
	{
		IL2CPP_JUMP_TBL(0x7C, IL_007c)
		IL2CPP_JUMP_TBL(0x3E, IL_003e)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_003e:
	{
		SingleAssignmentDisposable_t2336378823 * L_6 = (SingleAssignmentDisposable_t2336378823 *)il2cpp_codegen_object_new(SingleAssignmentDisposable_t2336378823_il2cpp_TypeInfo_var);
		SingleAssignmentDisposable__ctor_m917449122(L_6, /*hidden argument*/NULL);
		V_1 = (SingleAssignmentDisposable_t2336378823 *)L_6;
		SerialDisposable_t2547852742 * L_7 = (SerialDisposable_t2547852742 *)__this->get_cancelable_5();
		SingleAssignmentDisposable_t2336378823 * L_8 = V_1;
		NullCheck((SerialDisposable_t2547852742 *)L_7);
		SerialDisposable_set_Disposable_m1438046228((SerialDisposable_t2547852742 *)L_7, (Il2CppObject *)L_8, /*hidden argument*/NULL);
		SingleAssignmentDisposable_t2336378823 * L_9 = V_1;
		ThrottleFirstFrameObservable_1_t955022977 * L_10 = (ThrottleFirstFrameObservable_1_t955022977 *)__this->get_parent_2();
		NullCheck(L_10);
		int32_t L_11 = (int32_t)L_10->get_frameCount_2();
		ThrottleFirstFrameObservable_1_t955022977 * L_12 = (ThrottleFirstFrameObservable_1_t955022977 *)__this->get_parent_2();
		NullCheck(L_12);
		int32_t L_13 = (int32_t)L_12->get_frameCountType_3();
		IL2CPP_RUNTIME_CLASS_INIT(Observable_t258071701_il2cpp_TypeInfo_var);
		Il2CppObject* L_14 = Observable_TimerFrame_m594366159(NULL /*static, unused*/, (int32_t)L_11, (int32_t)L_13, /*hidden argument*/NULL);
		ThrottleFirstFrameTick_t2464530589 * L_15 = (ThrottleFirstFrameTick_t2464530589 *)__this->get_tick_6();
		NullCheck((Il2CppObject*)L_14);
		Il2CppObject * L_16 = InterfaceFuncInvoker1< Il2CppObject *, Il2CppObject* >::Invoke(0 /* System.IDisposable UniRx.IObservable`1<System.Int64>::Subscribe(UniRx.IObserver`1<T>) */, IObservable_1_t2606213246_il2cpp_TypeInfo_var, (Il2CppObject*)L_14, (Il2CppObject*)L_15);
		NullCheck((SingleAssignmentDisposable_t2336378823 *)L_9);
		SingleAssignmentDisposable_set_Disposable_m1599028885((SingleAssignmentDisposable_t2336378823 *)L_9, (Il2CppObject *)L_16, /*hidden argument*/NULL);
	}

IL_007c:
	{
		return;
	}
}
// System.Void UniRx.Operators.ThrottleFirstFrameObservable`1/ThrottleFirstFrame<System.Object>::OnError(System.Exception)
extern "C"  void ThrottleFirstFrame_OnError_m3275435342_gshared (ThrottleFirstFrame_t517244520 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method)
{
	Il2CppObject * V_0 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		SerialDisposable_t2547852742 * L_0 = (SerialDisposable_t2547852742 *)__this->get_cancelable_5();
		NullCheck((SerialDisposable_t2547852742 *)L_0);
		VirtActionInvoker0::Invoke(4 /* System.Void UniRx.SerialDisposable::Dispose() */, (SerialDisposable_t2547852742 *)L_0);
		Il2CppObject * L_1 = (Il2CppObject *)__this->get_gate_3();
		V_0 = (Il2CppObject *)L_1;
		Il2CppObject * L_2 = V_0;
		Monitor_Enter_m476686225(NULL /*static, unused*/, (Il2CppObject *)L_2, /*hidden argument*/NULL);
	}

IL_0018:
	try
	{ // begin try (depth: 1)
		try
		{ // begin try (depth: 2)
			Il2CppObject* L_3 = (Il2CppObject*)((OperatorObserverBase_2_t1187768149 *)__this)->get_observer_0();
			il2cpp_codegen_memory_barrier();
			Exception_t1967233988 * L_4 = ___error0;
			NullCheck((Il2CppObject*)L_3);
			InterfaceActionInvoker1< Exception_t1967233988 * >::Invoke(1 /* System.Void UniRx.IObserver`1<System.Object>::OnError(System.Exception) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5), (Il2CppObject*)L_3, (Exception_t1967233988 *)L_4);
			IL2CPP_LEAVE(0x32, FINALLY_002b);
		} // end try (depth: 2)
		catch(Il2CppExceptionWrapper& e)
		{
			__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
			goto FINALLY_002b;
		}

FINALLY_002b:
		{ // begin finally (depth: 2)
			NullCheck((OperatorObserverBase_2_t1187768149 *)__this);
			VirtActionInvoker0::Invoke(4 /* System.Void UniRx.Operators.OperatorObserverBase`2<System.Object,System.Object>::Dispose() */, (OperatorObserverBase_2_t1187768149 *)__this);
			IL2CPP_END_FINALLY(43)
		} // end finally (depth: 2)
		IL2CPP_CLEANUP(43)
		{
			IL2CPP_JUMP_TBL(0x32, IL_0032)
			IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
		}

IL_0032:
		{
			IL2CPP_LEAVE(0x3E, FINALLY_0037);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0037;
	}

FINALLY_0037:
	{ // begin finally (depth: 1)
		Il2CppObject * L_5 = V_0;
		Monitor_Exit_m2088237919(NULL /*static, unused*/, (Il2CppObject *)L_5, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(55)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(55)
	{
		IL2CPP_JUMP_TBL(0x3E, IL_003e)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_003e:
	{
		return;
	}
}
// System.Void UniRx.Operators.ThrottleFirstFrameObservable`1/ThrottleFirstFrame<System.Object>::OnCompleted()
extern "C"  void ThrottleFirstFrame_OnCompleted_m3042502049_gshared (ThrottleFirstFrame_t517244520 * __this, const MethodInfo* method)
{
	Il2CppObject * V_0 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		SerialDisposable_t2547852742 * L_0 = (SerialDisposable_t2547852742 *)__this->get_cancelable_5();
		NullCheck((SerialDisposable_t2547852742 *)L_0);
		VirtActionInvoker0::Invoke(4 /* System.Void UniRx.SerialDisposable::Dispose() */, (SerialDisposable_t2547852742 *)L_0);
		Il2CppObject * L_1 = (Il2CppObject *)__this->get_gate_3();
		V_0 = (Il2CppObject *)L_1;
		Il2CppObject * L_2 = V_0;
		Monitor_Enter_m476686225(NULL /*static, unused*/, (Il2CppObject *)L_2, /*hidden argument*/NULL);
	}

IL_0018:
	try
	{ // begin try (depth: 1)
		try
		{ // begin try (depth: 2)
			Il2CppObject* L_3 = (Il2CppObject*)((OperatorObserverBase_2_t1187768149 *)__this)->get_observer_0();
			il2cpp_codegen_memory_barrier();
			NullCheck((Il2CppObject*)L_3);
			InterfaceActionInvoker0::Invoke(0 /* System.Void UniRx.IObserver`1<System.Object>::OnCompleted() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5), (Il2CppObject*)L_3);
			IL2CPP_LEAVE(0x31, FINALLY_002a);
		} // end try (depth: 2)
		catch(Il2CppExceptionWrapper& e)
		{
			__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
			goto FINALLY_002a;
		}

FINALLY_002a:
		{ // begin finally (depth: 2)
			NullCheck((OperatorObserverBase_2_t1187768149 *)__this);
			VirtActionInvoker0::Invoke(4 /* System.Void UniRx.Operators.OperatorObserverBase`2<System.Object,System.Object>::Dispose() */, (OperatorObserverBase_2_t1187768149 *)__this);
			IL2CPP_END_FINALLY(42)
		} // end finally (depth: 2)
		IL2CPP_CLEANUP(42)
		{
			IL2CPP_JUMP_TBL(0x31, IL_0031)
			IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
		}

IL_0031:
		{
			IL2CPP_LEAVE(0x3D, FINALLY_0036);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0036;
	}

FINALLY_0036:
	{ // begin finally (depth: 1)
		Il2CppObject * L_4 = V_0;
		Monitor_Exit_m2088237919(NULL /*static, unused*/, (Il2CppObject *)L_4, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(54)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(54)
	{
		IL2CPP_JUMP_TBL(0x3D, IL_003d)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_003d:
	{
		return;
	}
}
// System.Void UniRx.Operators.ThrottleFirstFrameObservable`1<System.Object>::.ctor(UniRx.IObservable`1<T>,System.Int32,UniRx.FrameCountType)
extern "C"  void ThrottleFirstFrameObservable_1__ctor_m3787531655_gshared (ThrottleFirstFrameObservable_1_t955022977 * __this, Il2CppObject* ___source0, int32_t ___frameCount1, int32_t ___frameCountType2, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = ___source0;
		bool L_1 = ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)(NULL /*static, unused*/, (Il2CppObject*)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		NullCheck((OperatorObservableBase_1_t4196218687 *)__this);
		((  void (*) (OperatorObservableBase_1_t4196218687 *, bool, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)((OperatorObservableBase_1_t4196218687 *)__this, (bool)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		Il2CppObject* L_2 = ___source0;
		__this->set_source_1(L_2);
		int32_t L_3 = ___frameCount1;
		__this->set_frameCount_2(L_3);
		int32_t L_4 = ___frameCountType2;
		__this->set_frameCountType_3(L_4);
		return;
	}
}
// System.IDisposable UniRx.Operators.ThrottleFirstFrameObservable`1<System.Object>::SubscribeCore(UniRx.IObserver`1<T>,System.IDisposable)
extern "C"  Il2CppObject * ThrottleFirstFrameObservable_1_SubscribeCore_m2769623739_gshared (ThrottleFirstFrameObservable_1_t955022977 * __this, Il2CppObject* ___observer0, Il2CppObject * ___cancel1, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = ___observer0;
		Il2CppObject * L_1 = ___cancel1;
		ThrottleFirstFrame_t517244520 * L_2 = (ThrottleFirstFrame_t517244520 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		((  void (*) (ThrottleFirstFrame_t517244520 *, ThrottleFirstFrameObservable_1_t955022977 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)(L_2, (ThrottleFirstFrameObservable_1_t955022977 *)__this, (Il2CppObject*)L_0, (Il2CppObject *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		NullCheck((ThrottleFirstFrame_t517244520 *)L_2);
		Il2CppObject * L_3 = ((  Il2CppObject * (*) (ThrottleFirstFrame_t517244520 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->method)((ThrottleFirstFrame_t517244520 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		return L_3;
	}
}
// System.Void UniRx.Operators.ThrottleFirstObservable`1/ThrottleFirst<System.Object>::.ctor(UniRx.Operators.ThrottleFirstObservable`1<T>,UniRx.IObserver`1<T>,System.IDisposable)
extern Il2CppClass* Il2CppObject_il2cpp_TypeInfo_var;
extern const uint32_t ThrottleFirst__ctor_m818225262_MetadataUsageId;
extern "C"  void ThrottleFirst__ctor_m818225262_gshared (ThrottleFirst_t4236367119 * __this, ThrottleFirstObservable_1_t2123238056 * ___parent0, Il2CppObject* ___observer1, Il2CppObject * ___cancel2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ThrottleFirst__ctor_m818225262_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject * L_0 = (Il2CppObject *)il2cpp_codegen_object_new(Il2CppObject_il2cpp_TypeInfo_var);
		Object__ctor_m1772956182(L_0, /*hidden argument*/NULL);
		__this->set_gate_3(L_0);
		__this->set_open_4((bool)1);
		Il2CppObject* L_1 = ___observer1;
		Il2CppObject * L_2 = ___cancel2;
		NullCheck((OperatorObserverBase_2_t1187768149 *)__this);
		((  void (*) (OperatorObserverBase_2_t1187768149 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((OperatorObserverBase_2_t1187768149 *)__this, (Il2CppObject*)L_1, (Il2CppObject *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		ThrottleFirstObservable_1_t2123238056 * L_3 = ___parent0;
		__this->set_parent_2(L_3);
		return;
	}
}
// System.IDisposable UniRx.Operators.ThrottleFirstObservable`1/ThrottleFirst<System.Object>::Run()
extern Il2CppClass* SerialDisposable_t2547852742_il2cpp_TypeInfo_var;
extern const uint32_t ThrottleFirst_Run_m1603010619_MetadataUsageId;
extern "C"  Il2CppObject * ThrottleFirst_Run_m1603010619_gshared (ThrottleFirst_t4236367119 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ThrottleFirst_Run_m1603010619_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Il2CppObject * V_0 = NULL;
	{
		SerialDisposable_t2547852742 * L_0 = (SerialDisposable_t2547852742 *)il2cpp_codegen_object_new(SerialDisposable_t2547852742_il2cpp_TypeInfo_var);
		SerialDisposable__ctor_m2977852291(L_0, /*hidden argument*/NULL);
		__this->set_cancelable_5(L_0);
		ThrottleFirstObservable_1_t2123238056 * L_1 = (ThrottleFirstObservable_1_t2123238056 *)__this->get_parent_2();
		NullCheck(L_1);
		Il2CppObject* L_2 = (Il2CppObject*)L_1->get_source_1();
		NullCheck((Il2CppObject*)L_2);
		Il2CppObject * L_3 = InterfaceFuncInvoker1< Il2CppObject *, Il2CppObject* >::Invoke(0 /* System.IDisposable UniRx.IObservable`1<System.Object>::Subscribe(UniRx.IObserver`1<T>) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), (Il2CppObject*)L_2, (Il2CppObject*)__this);
		V_0 = (Il2CppObject *)L_3;
		SerialDisposable_t2547852742 * L_4 = (SerialDisposable_t2547852742 *)__this->get_cancelable_5();
		Il2CppObject * L_5 = V_0;
		Il2CppObject * L_6 = StableCompositeDisposable_Create_m3764788535(NULL /*static, unused*/, (Il2CppObject *)L_4, (Il2CppObject *)L_5, /*hidden argument*/NULL);
		return L_6;
	}
}
// System.Void UniRx.Operators.ThrottleFirstObservable`1/ThrottleFirst<System.Object>::OnNext()
extern "C"  void ThrottleFirst_OnNext_m3545300713_gshared (ThrottleFirst_t4236367119 * __this, const MethodInfo* method)
{
	Il2CppObject * V_0 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_gate_3();
		V_0 = (Il2CppObject *)L_0;
		Il2CppObject * L_1 = V_0;
		Monitor_Enter_m476686225(NULL /*static, unused*/, (Il2CppObject *)L_1, /*hidden argument*/NULL);
	}

IL_000d:
	try
	{ // begin try (depth: 1)
		__this->set_open_4((bool)1);
		IL2CPP_LEAVE(0x20, FINALLY_0019);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0019;
	}

FINALLY_0019:
	{ // begin finally (depth: 1)
		Il2CppObject * L_2 = V_0;
		Monitor_Exit_m2088237919(NULL /*static, unused*/, (Il2CppObject *)L_2, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(25)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(25)
	{
		IL2CPP_JUMP_TBL(0x20, IL_0020)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0020:
	{
		return;
	}
}
// System.Void UniRx.Operators.ThrottleFirstObservable`1/ThrottleFirst<System.Object>::OnNext(T)
extern Il2CppClass* SingleAssignmentDisposable_t2336378823_il2cpp_TypeInfo_var;
extern Il2CppClass* Action_t437523947_il2cpp_TypeInfo_var;
extern Il2CppClass* IScheduler_t2938318244_il2cpp_TypeInfo_var;
extern const uint32_t ThrottleFirst_OnNext_m2530141077_MetadataUsageId;
extern "C"  void ThrottleFirst_OnNext_m2530141077_gshared (ThrottleFirst_t4236367119 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ThrottleFirst_OnNext_m2530141077_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Il2CppObject * V_0 = NULL;
	SingleAssignmentDisposable_t2336378823 * V_1 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_gate_3();
		V_0 = (Il2CppObject *)L_0;
		Il2CppObject * L_1 = V_0;
		Monitor_Enter_m476686225(NULL /*static, unused*/, (Il2CppObject *)L_1, /*hidden argument*/NULL);
	}

IL_000d:
	try
	{ // begin try (depth: 1)
		{
			bool L_2 = (bool)__this->get_open_4();
			if (L_2)
			{
				goto IL_001d;
			}
		}

IL_0018:
		{
			IL2CPP_LEAVE(0x7D, FINALLY_0037);
		}

IL_001d:
		{
			Il2CppObject* L_3 = (Il2CppObject*)((OperatorObserverBase_2_t1187768149 *)__this)->get_observer_0();
			il2cpp_codegen_memory_barrier();
			Il2CppObject * L_4 = ___value0;
			NullCheck((Il2CppObject*)L_3);
			InterfaceActionInvoker1< Il2CppObject * >::Invoke(2 /* System.Void UniRx.IObserver`1<System.Object>::OnNext(T) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3), (Il2CppObject*)L_3, (Il2CppObject *)L_4);
			__this->set_open_4((bool)0);
			IL2CPP_LEAVE(0x3E, FINALLY_0037);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0037;
	}

FINALLY_0037:
	{ // begin finally (depth: 1)
		Il2CppObject * L_5 = V_0;
		Monitor_Exit_m2088237919(NULL /*static, unused*/, (Il2CppObject *)L_5, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(55)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(55)
	{
		IL2CPP_JUMP_TBL(0x7D, IL_007d)
		IL2CPP_JUMP_TBL(0x3E, IL_003e)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_003e:
	{
		SingleAssignmentDisposable_t2336378823 * L_6 = (SingleAssignmentDisposable_t2336378823 *)il2cpp_codegen_object_new(SingleAssignmentDisposable_t2336378823_il2cpp_TypeInfo_var);
		SingleAssignmentDisposable__ctor_m917449122(L_6, /*hidden argument*/NULL);
		V_1 = (SingleAssignmentDisposable_t2336378823 *)L_6;
		SerialDisposable_t2547852742 * L_7 = (SerialDisposable_t2547852742 *)__this->get_cancelable_5();
		SingleAssignmentDisposable_t2336378823 * L_8 = V_1;
		NullCheck((SerialDisposable_t2547852742 *)L_7);
		SerialDisposable_set_Disposable_m1438046228((SerialDisposable_t2547852742 *)L_7, (Il2CppObject *)L_8, /*hidden argument*/NULL);
		SingleAssignmentDisposable_t2336378823 * L_9 = V_1;
		ThrottleFirstObservable_1_t2123238056 * L_10 = (ThrottleFirstObservable_1_t2123238056 *)__this->get_parent_2();
		NullCheck(L_10);
		Il2CppObject * L_11 = (Il2CppObject *)L_10->get_scheduler_3();
		ThrottleFirstObservable_1_t2123238056 * L_12 = (ThrottleFirstObservable_1_t2123238056 *)__this->get_parent_2();
		NullCheck(L_12);
		TimeSpan_t763862892  L_13 = (TimeSpan_t763862892 )L_12->get_dueTime_2();
		IntPtr_t L_14;
		L_14.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		Action_t437523947 * L_15 = (Action_t437523947 *)il2cpp_codegen_object_new(Action_t437523947_il2cpp_TypeInfo_var);
		Action__ctor_m2957240604(L_15, (Il2CppObject *)__this, (IntPtr_t)L_14, /*hidden argument*/NULL);
		NullCheck((Il2CppObject *)L_11);
		Il2CppObject * L_16 = InterfaceFuncInvoker2< Il2CppObject *, TimeSpan_t763862892 , Action_t437523947 * >::Invoke(2 /* System.IDisposable UniRx.IScheduler::Schedule(System.TimeSpan,System.Action) */, IScheduler_t2938318244_il2cpp_TypeInfo_var, (Il2CppObject *)L_11, (TimeSpan_t763862892 )L_13, (Action_t437523947 *)L_15);
		NullCheck((SingleAssignmentDisposable_t2336378823 *)L_9);
		SingleAssignmentDisposable_set_Disposable_m1599028885((SingleAssignmentDisposable_t2336378823 *)L_9, (Il2CppObject *)L_16, /*hidden argument*/NULL);
	}

IL_007d:
	{
		return;
	}
}
// System.Void UniRx.Operators.ThrottleFirstObservable`1/ThrottleFirst<System.Object>::OnError(System.Exception)
extern "C"  void ThrottleFirst_OnError_m3100325028_gshared (ThrottleFirst_t4236367119 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method)
{
	Il2CppObject * V_0 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		SerialDisposable_t2547852742 * L_0 = (SerialDisposable_t2547852742 *)__this->get_cancelable_5();
		NullCheck((SerialDisposable_t2547852742 *)L_0);
		VirtActionInvoker0::Invoke(4 /* System.Void UniRx.SerialDisposable::Dispose() */, (SerialDisposable_t2547852742 *)L_0);
		Il2CppObject * L_1 = (Il2CppObject *)__this->get_gate_3();
		V_0 = (Il2CppObject *)L_1;
		Il2CppObject * L_2 = V_0;
		Monitor_Enter_m476686225(NULL /*static, unused*/, (Il2CppObject *)L_2, /*hidden argument*/NULL);
	}

IL_0018:
	try
	{ // begin try (depth: 1)
		try
		{ // begin try (depth: 2)
			Il2CppObject* L_3 = (Il2CppObject*)((OperatorObserverBase_2_t1187768149 *)__this)->get_observer_0();
			il2cpp_codegen_memory_barrier();
			Exception_t1967233988 * L_4 = ___error0;
			NullCheck((Il2CppObject*)L_3);
			InterfaceActionInvoker1< Exception_t1967233988 * >::Invoke(1 /* System.Void UniRx.IObserver`1<System.Object>::OnError(System.Exception) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3), (Il2CppObject*)L_3, (Exception_t1967233988 *)L_4);
			IL2CPP_LEAVE(0x32, FINALLY_002b);
		} // end try (depth: 2)
		catch(Il2CppExceptionWrapper& e)
		{
			__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
			goto FINALLY_002b;
		}

FINALLY_002b:
		{ // begin finally (depth: 2)
			NullCheck((OperatorObserverBase_2_t1187768149 *)__this);
			VirtActionInvoker0::Invoke(4 /* System.Void UniRx.Operators.OperatorObserverBase`2<System.Object,System.Object>::Dispose() */, (OperatorObserverBase_2_t1187768149 *)__this);
			IL2CPP_END_FINALLY(43)
		} // end finally (depth: 2)
		IL2CPP_CLEANUP(43)
		{
			IL2CPP_JUMP_TBL(0x32, IL_0032)
			IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
		}

IL_0032:
		{
			IL2CPP_LEAVE(0x3E, FINALLY_0037);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0037;
	}

FINALLY_0037:
	{ // begin finally (depth: 1)
		Il2CppObject * L_5 = V_0;
		Monitor_Exit_m2088237919(NULL /*static, unused*/, (Il2CppObject *)L_5, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(55)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(55)
	{
		IL2CPP_JUMP_TBL(0x3E, IL_003e)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_003e:
	{
		return;
	}
}
// System.Void UniRx.Operators.ThrottleFirstObservable`1/ThrottleFirst<System.Object>::OnCompleted()
extern "C"  void ThrottleFirst_OnCompleted_m1541979639_gshared (ThrottleFirst_t4236367119 * __this, const MethodInfo* method)
{
	Il2CppObject * V_0 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		SerialDisposable_t2547852742 * L_0 = (SerialDisposable_t2547852742 *)__this->get_cancelable_5();
		NullCheck((SerialDisposable_t2547852742 *)L_0);
		VirtActionInvoker0::Invoke(4 /* System.Void UniRx.SerialDisposable::Dispose() */, (SerialDisposable_t2547852742 *)L_0);
		Il2CppObject * L_1 = (Il2CppObject *)__this->get_gate_3();
		V_0 = (Il2CppObject *)L_1;
		Il2CppObject * L_2 = V_0;
		Monitor_Enter_m476686225(NULL /*static, unused*/, (Il2CppObject *)L_2, /*hidden argument*/NULL);
	}

IL_0018:
	try
	{ // begin try (depth: 1)
		try
		{ // begin try (depth: 2)
			Il2CppObject* L_3 = (Il2CppObject*)((OperatorObserverBase_2_t1187768149 *)__this)->get_observer_0();
			il2cpp_codegen_memory_barrier();
			NullCheck((Il2CppObject*)L_3);
			InterfaceActionInvoker0::Invoke(0 /* System.Void UniRx.IObserver`1<System.Object>::OnCompleted() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3), (Il2CppObject*)L_3);
			IL2CPP_LEAVE(0x31, FINALLY_002a);
		} // end try (depth: 2)
		catch(Il2CppExceptionWrapper& e)
		{
			__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
			goto FINALLY_002a;
		}

FINALLY_002a:
		{ // begin finally (depth: 2)
			NullCheck((OperatorObserverBase_2_t1187768149 *)__this);
			VirtActionInvoker0::Invoke(4 /* System.Void UniRx.Operators.OperatorObserverBase`2<System.Object,System.Object>::Dispose() */, (OperatorObserverBase_2_t1187768149 *)__this);
			IL2CPP_END_FINALLY(42)
		} // end finally (depth: 2)
		IL2CPP_CLEANUP(42)
		{
			IL2CPP_JUMP_TBL(0x31, IL_0031)
			IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
		}

IL_0031:
		{
			IL2CPP_LEAVE(0x3D, FINALLY_0036);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0036;
	}

FINALLY_0036:
	{ // begin finally (depth: 1)
		Il2CppObject * L_4 = V_0;
		Monitor_Exit_m2088237919(NULL /*static, unused*/, (Il2CppObject *)L_4, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(54)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(54)
	{
		IL2CPP_JUMP_TBL(0x3D, IL_003d)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_003d:
	{
		return;
	}
}
// System.Void UniRx.Operators.ThrottleFirstObservable`1<System.Object>::.ctor(UniRx.IObservable`1<T>,System.TimeSpan,UniRx.IScheduler)
extern Il2CppClass* Scheduler_t103934925_il2cpp_TypeInfo_var;
extern const uint32_t ThrottleFirstObservable_1__ctor_m2021260689_MetadataUsageId;
extern "C"  void ThrottleFirstObservable_1__ctor_m2021260689_gshared (ThrottleFirstObservable_1_t2123238056 * __this, Il2CppObject* ___source0, TimeSpan_t763862892  ___dueTime1, Il2CppObject * ___scheduler2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ThrottleFirstObservable_1__ctor_m2021260689_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	ThrottleFirstObservable_1_t2123238056 * G_B2_0 = NULL;
	ThrottleFirstObservable_1_t2123238056 * G_B1_0 = NULL;
	int32_t G_B3_0 = 0;
	ThrottleFirstObservable_1_t2123238056 * G_B3_1 = NULL;
	{
		Il2CppObject * L_0 = ___scheduler2;
		IL2CPP_RUNTIME_CLASS_INIT(Scheduler_t103934925_il2cpp_TypeInfo_var);
		Il2CppObject * L_1 = ((Scheduler_t103934925_StaticFields*)Scheduler_t103934925_il2cpp_TypeInfo_var->static_fields)->get_CurrentThread_0();
		G_B1_0 = ((ThrottleFirstObservable_1_t2123238056 *)(__this));
		if ((((Il2CppObject*)(Il2CppObject *)L_0) == ((Il2CppObject*)(Il2CppObject *)L_1)))
		{
			G_B2_0 = ((ThrottleFirstObservable_1_t2123238056 *)(__this));
			goto IL_0014;
		}
	}
	{
		Il2CppObject* L_2 = ___source0;
		bool L_3 = ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)(NULL /*static, unused*/, (Il2CppObject*)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		G_B3_0 = ((int32_t)(L_3));
		G_B3_1 = ((ThrottleFirstObservable_1_t2123238056 *)(G_B1_0));
		goto IL_0015;
	}

IL_0014:
	{
		G_B3_0 = 1;
		G_B3_1 = ((ThrottleFirstObservable_1_t2123238056 *)(G_B2_0));
	}

IL_0015:
	{
		NullCheck((OperatorObservableBase_1_t4196218687 *)G_B3_1);
		((  void (*) (OperatorObservableBase_1_t4196218687 *, bool, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)((OperatorObservableBase_1_t4196218687 *)G_B3_1, (bool)G_B3_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		Il2CppObject* L_4 = ___source0;
		__this->set_source_1(L_4);
		TimeSpan_t763862892  L_5 = ___dueTime1;
		__this->set_dueTime_2(L_5);
		Il2CppObject * L_6 = ___scheduler2;
		__this->set_scheduler_3(L_6);
		return;
	}
}
// System.IDisposable UniRx.Operators.ThrottleFirstObservable`1<System.Object>::SubscribeCore(UniRx.IObserver`1<T>,System.IDisposable)
extern "C"  Il2CppObject * ThrottleFirstObservable_1_SubscribeCore_m1500428200_gshared (ThrottleFirstObservable_1_t2123238056 * __this, Il2CppObject* ___observer0, Il2CppObject * ___cancel1, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = ___observer0;
		Il2CppObject * L_1 = ___cancel1;
		ThrottleFirst_t4236367119 * L_2 = (ThrottleFirst_t4236367119 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		((  void (*) (ThrottleFirst_t4236367119 *, ThrottleFirstObservable_1_t2123238056 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)(L_2, (ThrottleFirstObservable_1_t2123238056 *)__this, (Il2CppObject*)L_0, (Il2CppObject *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		NullCheck((ThrottleFirst_t4236367119 *)L_2);
		Il2CppObject * L_3 = ((  Il2CppObject * (*) (ThrottleFirst_t4236367119 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->method)((ThrottleFirst_t4236367119 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		return L_3;
	}
}
// System.Void UniRx.Operators.ThrottleFrameObservable`1/ThrottleFrame/ThrottleFrameTick<System.Object>::.ctor(UniRx.Operators.ThrottleFrameObservable`1/ThrottleFrame<T>,System.UInt64)
extern "C"  void ThrottleFrameTick__ctor_m3445849371_gshared (ThrottleFrameTick_t1733702649 * __this, ThrottleFrame_t3629975492 * ___parent0, uint64_t ___currentid1, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		ThrottleFrame_t3629975492 * L_0 = ___parent0;
		__this->set_parent_0(L_0);
		uint64_t L_1 = ___currentid1;
		__this->set_currentid_1(L_1);
		return;
	}
}
// System.Void UniRx.Operators.ThrottleFrameObservable`1/ThrottleFrame/ThrottleFrameTick<System.Object>::OnCompleted()
extern "C"  void ThrottleFrameTick_OnCompleted_m1882604578_gshared (ThrottleFrameTick_t1733702649 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void UniRx.Operators.ThrottleFrameObservable`1/ThrottleFrame/ThrottleFrameTick<System.Object>::OnError(System.Exception)
extern "C"  void ThrottleFrameTick_OnError_m1728172623_gshared (ThrottleFrameTick_t1733702649 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void UniRx.Operators.ThrottleFrameObservable`1/ThrottleFrame/ThrottleFrameTick<System.Object>::OnNext(System.Int64)
extern "C"  void ThrottleFrameTick_OnNext_m3270831536_gshared (ThrottleFrameTick_t1733702649 * __this, int64_t ____0, const MethodInfo* method)
{
	Il2CppObject * V_0 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		ThrottleFrame_t3629975492 * L_0 = (ThrottleFrame_t3629975492 *)__this->get_parent_0();
		NullCheck(L_0);
		Il2CppObject * L_1 = (Il2CppObject *)L_0->get_gate_3();
		V_0 = (Il2CppObject *)L_1;
		Il2CppObject * L_2 = V_0;
		Monitor_Enter_m476686225(NULL /*static, unused*/, (Il2CppObject *)L_2, /*hidden argument*/NULL);
	}

IL_0012:
	try
	{ // begin try (depth: 1)
		{
			ThrottleFrame_t3629975492 * L_3 = (ThrottleFrame_t3629975492 *)__this->get_parent_0();
			NullCheck(L_3);
			bool L_4 = (bool)L_3->get_hasValue_5();
			if (!L_4)
			{
				goto IL_0055;
			}
		}

IL_0022:
		{
			ThrottleFrame_t3629975492 * L_5 = (ThrottleFrame_t3629975492 *)__this->get_parent_0();
			NullCheck(L_5);
			uint64_t L_6 = (uint64_t)L_5->get_id_7();
			uint64_t L_7 = (uint64_t)__this->get_currentid_1();
			if ((!(((uint64_t)L_6) == ((uint64_t)L_7))))
			{
				goto IL_0055;
			}
		}

IL_0038:
		{
			ThrottleFrame_t3629975492 * L_8 = (ThrottleFrame_t3629975492 *)__this->get_parent_0();
			NullCheck(L_8);
			Il2CppObject* L_9 = (Il2CppObject*)((OperatorObserverBase_2_t1187768149 *)L_8)->get_observer_0();
			il2cpp_codegen_memory_barrier();
			ThrottleFrame_t3629975492 * L_10 = (ThrottleFrame_t3629975492 *)__this->get_parent_0();
			NullCheck(L_10);
			Il2CppObject * L_11 = (Il2CppObject *)L_10->get_latestValue_4();
			NullCheck((Il2CppObject*)L_9);
			InterfaceActionInvoker1< Il2CppObject * >::Invoke(2 /* System.Void UniRx.IObserver`1<System.Object>::OnNext(T) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0), (Il2CppObject*)L_9, (Il2CppObject *)L_11);
		}

IL_0055:
		{
			ThrottleFrame_t3629975492 * L_12 = (ThrottleFrame_t3629975492 *)__this->get_parent_0();
			NullCheck(L_12);
			L_12->set_hasValue_5((bool)0);
			IL2CPP_LEAVE(0x6D, FINALLY_0066);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0066;
	}

FINALLY_0066:
	{ // begin finally (depth: 1)
		Il2CppObject * L_13 = V_0;
		Monitor_Exit_m2088237919(NULL /*static, unused*/, (Il2CppObject *)L_13, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(102)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(102)
	{
		IL2CPP_JUMP_TBL(0x6D, IL_006d)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_006d:
	{
		return;
	}
}
// System.Void UniRx.Operators.ThrottleFrameObservable`1/ThrottleFrame<System.Object>::.ctor(UniRx.Operators.ThrottleFrameObservable`1<T>,UniRx.IObserver`1<T>,System.IDisposable)
extern Il2CppClass* Il2CppObject_il2cpp_TypeInfo_var;
extern const uint32_t ThrottleFrame__ctor_m3150178405_MetadataUsageId;
extern "C"  void ThrottleFrame__ctor_m3150178405_gshared (ThrottleFrame_t3629975492 * __this, ThrottleFrameObservable_1_t2748437981 * ___parent0, Il2CppObject* ___observer1, Il2CppObject * ___cancel2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ThrottleFrame__ctor_m3150178405_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Il2CppObject * V_0 = NULL;
	{
		Il2CppObject * L_0 = (Il2CppObject *)il2cpp_codegen_object_new(Il2CppObject_il2cpp_TypeInfo_var);
		Object__ctor_m1772956182(L_0, /*hidden argument*/NULL);
		__this->set_gate_3(L_0);
		Initobj (Il2CppObject_il2cpp_TypeInfo_var, (&V_0));
		Il2CppObject * L_1 = V_0;
		__this->set_latestValue_4(L_1);
		Il2CppObject* L_2 = ___observer1;
		Il2CppObject * L_3 = ___cancel2;
		NullCheck((OperatorObserverBase_2_t1187768149 *)__this);
		((  void (*) (OperatorObserverBase_2_t1187768149 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((OperatorObserverBase_2_t1187768149 *)__this, (Il2CppObject*)L_2, (Il2CppObject *)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		ThrottleFrameObservable_1_t2748437981 * L_4 = ___parent0;
		__this->set_parent_2(L_4);
		return;
	}
}
// System.IDisposable UniRx.Operators.ThrottleFrameObservable`1/ThrottleFrame<System.Object>::Run()
extern Il2CppClass* SerialDisposable_t2547852742_il2cpp_TypeInfo_var;
extern const uint32_t ThrottleFrame_Run_m3848274933_MetadataUsageId;
extern "C"  Il2CppObject * ThrottleFrame_Run_m3848274933_gshared (ThrottleFrame_t3629975492 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ThrottleFrame_Run_m3848274933_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Il2CppObject * V_0 = NULL;
	{
		SerialDisposable_t2547852742 * L_0 = (SerialDisposable_t2547852742 *)il2cpp_codegen_object_new(SerialDisposable_t2547852742_il2cpp_TypeInfo_var);
		SerialDisposable__ctor_m2977852291(L_0, /*hidden argument*/NULL);
		__this->set_cancelable_6(L_0);
		ThrottleFrameObservable_1_t2748437981 * L_1 = (ThrottleFrameObservable_1_t2748437981 *)__this->get_parent_2();
		NullCheck(L_1);
		Il2CppObject* L_2 = (Il2CppObject*)L_1->get_source_1();
		NullCheck((Il2CppObject*)L_2);
		Il2CppObject * L_3 = InterfaceFuncInvoker1< Il2CppObject *, Il2CppObject* >::Invoke(0 /* System.IDisposable UniRx.IObservable`1<System.Object>::Subscribe(UniRx.IObserver`1<T>) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), (Il2CppObject*)L_2, (Il2CppObject*)__this);
		V_0 = (Il2CppObject *)L_3;
		SerialDisposable_t2547852742 * L_4 = (SerialDisposable_t2547852742 *)__this->get_cancelable_6();
		Il2CppObject * L_5 = V_0;
		Il2CppObject * L_6 = StableCompositeDisposable_Create_m3764788535(NULL /*static, unused*/, (Il2CppObject *)L_4, (Il2CppObject *)L_5, /*hidden argument*/NULL);
		return L_6;
	}
}
// System.Void UniRx.Operators.ThrottleFrameObservable`1/ThrottleFrame<System.Object>::OnNext(T)
extern Il2CppClass* SingleAssignmentDisposable_t2336378823_il2cpp_TypeInfo_var;
extern Il2CppClass* Observable_t258071701_il2cpp_TypeInfo_var;
extern Il2CppClass* IObservable_1_t2606213246_il2cpp_TypeInfo_var;
extern const uint32_t ThrottleFrame_OnNext_m1193704015_MetadataUsageId;
extern "C"  void ThrottleFrame_OnNext_m1193704015_gshared (ThrottleFrame_t3629975492 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ThrottleFrame_OnNext_m1193704015_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	uint64_t V_0 = 0;
	Il2CppObject * V_1 = NULL;
	SingleAssignmentDisposable_t2336378823 * V_2 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_gate_3();
		V_1 = (Il2CppObject *)L_0;
		Il2CppObject * L_1 = V_1;
		Monitor_Enter_m476686225(NULL /*static, unused*/, (Il2CppObject *)L_1, /*hidden argument*/NULL);
	}

IL_000d:
	try
	{ // begin try (depth: 1)
		__this->set_hasValue_5((bool)1);
		Il2CppObject * L_2 = ___value0;
		__this->set_latestValue_4(L_2);
		uint64_t L_3 = (uint64_t)__this->get_id_7();
		__this->set_id_7(((int64_t)((int64_t)L_3+(int64_t)(((int64_t)((int64_t)1))))));
		uint64_t L_4 = (uint64_t)__this->get_id_7();
		V_0 = (uint64_t)L_4;
		IL2CPP_LEAVE(0x3D, FINALLY_0036);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0036;
	}

FINALLY_0036:
	{ // begin finally (depth: 1)
		Il2CppObject * L_5 = V_1;
		Monitor_Exit_m2088237919(NULL /*static, unused*/, (Il2CppObject *)L_5, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(54)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(54)
	{
		IL2CPP_JUMP_TBL(0x3D, IL_003d)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_003d:
	{
		SingleAssignmentDisposable_t2336378823 * L_6 = (SingleAssignmentDisposable_t2336378823 *)il2cpp_codegen_object_new(SingleAssignmentDisposable_t2336378823_il2cpp_TypeInfo_var);
		SingleAssignmentDisposable__ctor_m917449122(L_6, /*hidden argument*/NULL);
		V_2 = (SingleAssignmentDisposable_t2336378823 *)L_6;
		SerialDisposable_t2547852742 * L_7 = (SerialDisposable_t2547852742 *)__this->get_cancelable_6();
		SingleAssignmentDisposable_t2336378823 * L_8 = V_2;
		NullCheck((SerialDisposable_t2547852742 *)L_7);
		SerialDisposable_set_Disposable_m1438046228((SerialDisposable_t2547852742 *)L_7, (Il2CppObject *)L_8, /*hidden argument*/NULL);
		SingleAssignmentDisposable_t2336378823 * L_9 = V_2;
		ThrottleFrameObservable_1_t2748437981 * L_10 = (ThrottleFrameObservable_1_t2748437981 *)__this->get_parent_2();
		NullCheck(L_10);
		int32_t L_11 = (int32_t)L_10->get_frameCount_2();
		ThrottleFrameObservable_1_t2748437981 * L_12 = (ThrottleFrameObservable_1_t2748437981 *)__this->get_parent_2();
		NullCheck(L_12);
		int32_t L_13 = (int32_t)L_12->get_frameCountType_3();
		IL2CPP_RUNTIME_CLASS_INIT(Observable_t258071701_il2cpp_TypeInfo_var);
		Il2CppObject* L_14 = Observable_TimerFrame_m594366159(NULL /*static, unused*/, (int32_t)L_11, (int32_t)L_13, /*hidden argument*/NULL);
		uint64_t L_15 = V_0;
		ThrottleFrameTick_t1733702649 * L_16 = (ThrottleFrameTick_t1733702649 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		((  void (*) (ThrottleFrameTick_t1733702649 *, ThrottleFrame_t3629975492 *, uint64_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)(L_16, (ThrottleFrame_t3629975492 *)__this, (uint64_t)L_15, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		NullCheck((Il2CppObject*)L_14);
		Il2CppObject * L_17 = InterfaceFuncInvoker1< Il2CppObject *, Il2CppObject* >::Invoke(0 /* System.IDisposable UniRx.IObservable`1<System.Int64>::Subscribe(UniRx.IObserver`1<T>) */, IObservable_1_t2606213246_il2cpp_TypeInfo_var, (Il2CppObject*)L_14, (Il2CppObject*)L_16);
		NullCheck((SingleAssignmentDisposable_t2336378823 *)L_9);
		SingleAssignmentDisposable_set_Disposable_m1599028885((SingleAssignmentDisposable_t2336378823 *)L_9, (Il2CppObject *)L_17, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UniRx.Operators.ThrottleFrameObservable`1/ThrottleFrame<System.Object>::OnError(System.Exception)
extern "C"  void ThrottleFrame_OnError_m4272035678_gshared (ThrottleFrame_t3629975492 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method)
{
	Il2CppObject * V_0 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		SerialDisposable_t2547852742 * L_0 = (SerialDisposable_t2547852742 *)__this->get_cancelable_6();
		NullCheck((SerialDisposable_t2547852742 *)L_0);
		VirtActionInvoker0::Invoke(4 /* System.Void UniRx.SerialDisposable::Dispose() */, (SerialDisposable_t2547852742 *)L_0);
		Il2CppObject * L_1 = (Il2CppObject *)__this->get_gate_3();
		V_0 = (Il2CppObject *)L_1;
		Il2CppObject * L_2 = V_0;
		Monitor_Enter_m476686225(NULL /*static, unused*/, (Il2CppObject *)L_2, /*hidden argument*/NULL);
	}

IL_0018:
	try
	{ // begin try (depth: 1)
		{
			__this->set_hasValue_5((bool)0);
			uint64_t L_3 = (uint64_t)__this->get_id_7();
			__this->set_id_7(((int64_t)((int64_t)L_3+(int64_t)(((int64_t)((int64_t)1))))));
		}

IL_002e:
		try
		{ // begin try (depth: 2)
			Il2CppObject* L_4 = (Il2CppObject*)((OperatorObserverBase_2_t1187768149 *)__this)->get_observer_0();
			il2cpp_codegen_memory_barrier();
			Exception_t1967233988 * L_5 = ___error0;
			NullCheck((Il2CppObject*)L_4);
			InterfaceActionInvoker1< Exception_t1967233988 * >::Invoke(1 /* System.Void UniRx.IObserver`1<System.Object>::OnError(System.Exception) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5), (Il2CppObject*)L_4, (Exception_t1967233988 *)L_5);
			IL2CPP_LEAVE(0x48, FINALLY_0041);
		} // end try (depth: 2)
		catch(Il2CppExceptionWrapper& e)
		{
			__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
			goto FINALLY_0041;
		}

FINALLY_0041:
		{ // begin finally (depth: 2)
			NullCheck((OperatorObserverBase_2_t1187768149 *)__this);
			VirtActionInvoker0::Invoke(4 /* System.Void UniRx.Operators.OperatorObserverBase`2<System.Object,System.Object>::Dispose() */, (OperatorObserverBase_2_t1187768149 *)__this);
			IL2CPP_END_FINALLY(65)
		} // end finally (depth: 2)
		IL2CPP_CLEANUP(65)
		{
			IL2CPP_JUMP_TBL(0x48, IL_0048)
			IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
		}

IL_0048:
		{
			IL2CPP_LEAVE(0x54, FINALLY_004d);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_004d;
	}

FINALLY_004d:
	{ // begin finally (depth: 1)
		Il2CppObject * L_6 = V_0;
		Monitor_Exit_m2088237919(NULL /*static, unused*/, (Il2CppObject *)L_6, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(77)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(77)
	{
		IL2CPP_JUMP_TBL(0x54, IL_0054)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0054:
	{
		return;
	}
}
// System.Void UniRx.Operators.ThrottleFrameObservable`1/ThrottleFrame<System.Object>::OnCompleted()
extern "C"  void ThrottleFrame_OnCompleted_m1422026673_gshared (ThrottleFrame_t3629975492 * __this, const MethodInfo* method)
{
	Il2CppObject * V_0 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		SerialDisposable_t2547852742 * L_0 = (SerialDisposable_t2547852742 *)__this->get_cancelable_6();
		NullCheck((SerialDisposable_t2547852742 *)L_0);
		VirtActionInvoker0::Invoke(4 /* System.Void UniRx.SerialDisposable::Dispose() */, (SerialDisposable_t2547852742 *)L_0);
		Il2CppObject * L_1 = (Il2CppObject *)__this->get_gate_3();
		V_0 = (Il2CppObject *)L_1;
		Il2CppObject * L_2 = V_0;
		Monitor_Enter_m476686225(NULL /*static, unused*/, (Il2CppObject *)L_2, /*hidden argument*/NULL);
	}

IL_0018:
	try
	{ // begin try (depth: 1)
		{
			bool L_3 = (bool)__this->get_hasValue_5();
			if (!L_3)
			{
				goto IL_0036;
			}
		}

IL_0023:
		{
			Il2CppObject* L_4 = (Il2CppObject*)((OperatorObserverBase_2_t1187768149 *)__this)->get_observer_0();
			il2cpp_codegen_memory_barrier();
			Il2CppObject * L_5 = (Il2CppObject *)__this->get_latestValue_4();
			NullCheck((Il2CppObject*)L_4);
			InterfaceActionInvoker1< Il2CppObject * >::Invoke(2 /* System.Void UniRx.IObserver`1<System.Object>::OnNext(T) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5), (Il2CppObject*)L_4, (Il2CppObject *)L_5);
		}

IL_0036:
		{
			__this->set_hasValue_5((bool)0);
			uint64_t L_6 = (uint64_t)__this->get_id_7();
			__this->set_id_7(((int64_t)((int64_t)L_6+(int64_t)(((int64_t)((int64_t)1))))));
		}

IL_004c:
		try
		{ // begin try (depth: 2)
			Il2CppObject* L_7 = (Il2CppObject*)((OperatorObserverBase_2_t1187768149 *)__this)->get_observer_0();
			il2cpp_codegen_memory_barrier();
			NullCheck((Il2CppObject*)L_7);
			InterfaceActionInvoker0::Invoke(0 /* System.Void UniRx.IObserver`1<System.Object>::OnCompleted() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5), (Il2CppObject*)L_7);
			IL2CPP_LEAVE(0x65, FINALLY_005e);
		} // end try (depth: 2)
		catch(Il2CppExceptionWrapper& e)
		{
			__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
			goto FINALLY_005e;
		}

FINALLY_005e:
		{ // begin finally (depth: 2)
			NullCheck((OperatorObserverBase_2_t1187768149 *)__this);
			VirtActionInvoker0::Invoke(4 /* System.Void UniRx.Operators.OperatorObserverBase`2<System.Object,System.Object>::Dispose() */, (OperatorObserverBase_2_t1187768149 *)__this);
			IL2CPP_END_FINALLY(94)
		} // end finally (depth: 2)
		IL2CPP_CLEANUP(94)
		{
			IL2CPP_JUMP_TBL(0x65, IL_0065)
			IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
		}

IL_0065:
		{
			IL2CPP_LEAVE(0x71, FINALLY_006a);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_006a;
	}

FINALLY_006a:
	{ // begin finally (depth: 1)
		Il2CppObject * L_8 = V_0;
		Monitor_Exit_m2088237919(NULL /*static, unused*/, (Il2CppObject *)L_8, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(106)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(106)
	{
		IL2CPP_JUMP_TBL(0x71, IL_0071)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0071:
	{
		return;
	}
}
// System.Void UniRx.Operators.ThrottleFrameObservable`1<System.Object>::.ctor(UniRx.IObservable`1<T>,System.Int32,UniRx.FrameCountType)
extern "C"  void ThrottleFrameObservable_1__ctor_m237748557_gshared (ThrottleFrameObservable_1_t2748437981 * __this, Il2CppObject* ___source0, int32_t ___frameCount1, int32_t ___frameCountType2, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = ___source0;
		bool L_1 = ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)(NULL /*static, unused*/, (Il2CppObject*)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		NullCheck((OperatorObservableBase_1_t4196218687 *)__this);
		((  void (*) (OperatorObservableBase_1_t4196218687 *, bool, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)((OperatorObservableBase_1_t4196218687 *)__this, (bool)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		Il2CppObject* L_2 = ___source0;
		__this->set_source_1(L_2);
		int32_t L_3 = ___frameCount1;
		__this->set_frameCount_2(L_3);
		int32_t L_4 = ___frameCountType2;
		__this->set_frameCountType_3(L_4);
		return;
	}
}
// System.IDisposable UniRx.Operators.ThrottleFrameObservable`1<System.Object>::SubscribeCore(UniRx.IObserver`1<T>,System.IDisposable)
extern "C"  Il2CppObject * ThrottleFrameObservable_1_SubscribeCore_m351784395_gshared (ThrottleFrameObservable_1_t2748437981 * __this, Il2CppObject* ___observer0, Il2CppObject * ___cancel1, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = ___observer0;
		Il2CppObject * L_1 = ___cancel1;
		ThrottleFrame_t3629975492 * L_2 = (ThrottleFrame_t3629975492 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		((  void (*) (ThrottleFrame_t3629975492 *, ThrottleFrameObservable_1_t2748437981 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)(L_2, (ThrottleFrameObservable_1_t2748437981 *)__this, (Il2CppObject*)L_0, (Il2CppObject *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		NullCheck((ThrottleFrame_t3629975492 *)L_2);
		Il2CppObject * L_3 = ((  Il2CppObject * (*) (ThrottleFrame_t3629975492 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->method)((ThrottleFrame_t3629975492 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		return L_3;
	}
}
// System.Void UniRx.Operators.ThrottleObservable`1/Throttle/<OnNext>c__AnonStorey6A<System.Int64>::.ctor()
extern "C"  void U3COnNextU3Ec__AnonStorey6A__ctor_m155556972_gshared (U3COnNextU3Ec__AnonStorey6A_t4267933177 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UniRx.Operators.ThrottleObservable`1/Throttle/<OnNext>c__AnonStorey6A<System.Int64>::<>m__88()
extern "C"  void U3COnNextU3Ec__AnonStorey6A_U3CU3Em__88_m1474650933_gshared (U3COnNextU3Ec__AnonStorey6A_t4267933177 * __this, const MethodInfo* method)
{
	{
		Throttle_t1826605473 * L_0 = (Throttle_t1826605473 *)__this->get_U3CU3Ef__this_1();
		uint64_t L_1 = (uint64_t)__this->get_currentid_0();
		NullCheck((Throttle_t1826605473 *)L_0);
		((  void (*) (Throttle_t1826605473 *, uint64_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((Throttle_t1826605473 *)L_0, (uint64_t)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		return;
	}
}
// System.Void UniRx.Operators.ThrottleObservable`1/Throttle/<OnNext>c__AnonStorey6A<System.Object>::.ctor()
extern "C"  void U3COnNextU3Ec__AnonStorey6A__ctor_m2602365450_gshared (U3COnNextU3Ec__AnonStorey6A_t2257624715 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UniRx.Operators.ThrottleObservable`1/Throttle/<OnNext>c__AnonStorey6A<System.Object>::<>m__88()
extern "C"  void U3COnNextU3Ec__AnonStorey6A_U3CU3Em__88_m3510487379_gshared (U3COnNextU3Ec__AnonStorey6A_t2257624715 * __this, const MethodInfo* method)
{
	{
		Throttle_t4111264307 * L_0 = (Throttle_t4111264307 *)__this->get_U3CU3Ef__this_1();
		uint64_t L_1 = (uint64_t)__this->get_currentid_0();
		NullCheck((Throttle_t4111264307 *)L_0);
		((  void (*) (Throttle_t4111264307 *, uint64_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((Throttle_t4111264307 *)L_0, (uint64_t)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		return;
	}
}
// System.Void UniRx.Operators.ThrottleObservable`1/Throttle<System.Int64>::.ctor(UniRx.Operators.ThrottleObservable`1<T>,UniRx.IObserver`1<T>,System.IDisposable)
extern Il2CppClass* Il2CppObject_il2cpp_TypeInfo_var;
extern Il2CppClass* Int64_t2847414882_il2cpp_TypeInfo_var;
extern const uint32_t Throttle__ctor_m2529190470_MetadataUsageId;
extern "C"  void Throttle__ctor_m2529190470_gshared (Throttle_t1826605473 * __this, ThrottleObservable_1_t3227609402 * ___parent0, Il2CppObject* ___observer1, Il2CppObject * ___cancel2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Throttle__ctor_m2529190470_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int64_t V_0 = 0;
	{
		Il2CppObject * L_0 = (Il2CppObject *)il2cpp_codegen_object_new(Il2CppObject_il2cpp_TypeInfo_var);
		Object__ctor_m1772956182(L_0, /*hidden argument*/NULL);
		__this->set_gate_3(L_0);
		Initobj (Int64_t2847414882_il2cpp_TypeInfo_var, (&V_0));
		int64_t L_1 = V_0;
		__this->set_latestValue_4(L_1);
		Il2CppObject* L_2 = ___observer1;
		Il2CppObject * L_3 = ___cancel2;
		NullCheck((OperatorObserverBase_2_t3939730909 *)__this);
		((  void (*) (OperatorObserverBase_2_t3939730909 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((OperatorObserverBase_2_t3939730909 *)__this, (Il2CppObject*)L_2, (Il2CppObject *)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		ThrottleObservable_1_t3227609402 * L_4 = ___parent0;
		__this->set_parent_2(L_4);
		return;
	}
}
// System.IDisposable UniRx.Operators.ThrottleObservable`1/Throttle<System.Int64>::Run()
extern Il2CppClass* SerialDisposable_t2547852742_il2cpp_TypeInfo_var;
extern const uint32_t Throttle_Run_m2383935619_MetadataUsageId;
extern "C"  Il2CppObject * Throttle_Run_m2383935619_gshared (Throttle_t1826605473 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Throttle_Run_m2383935619_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Il2CppObject * V_0 = NULL;
	{
		SerialDisposable_t2547852742 * L_0 = (SerialDisposable_t2547852742 *)il2cpp_codegen_object_new(SerialDisposable_t2547852742_il2cpp_TypeInfo_var);
		SerialDisposable__ctor_m2977852291(L_0, /*hidden argument*/NULL);
		__this->set_cancelable_6(L_0);
		ThrottleObservable_1_t3227609402 * L_1 = (ThrottleObservable_1_t3227609402 *)__this->get_parent_2();
		NullCheck(L_1);
		Il2CppObject* L_2 = (Il2CppObject*)L_1->get_source_1();
		NullCheck((Il2CppObject*)L_2);
		Il2CppObject * L_3 = InterfaceFuncInvoker1< Il2CppObject *, Il2CppObject* >::Invoke(0 /* System.IDisposable UniRx.IObservable`1<System.Int64>::Subscribe(UniRx.IObserver`1<T>) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), (Il2CppObject*)L_2, (Il2CppObject*)__this);
		V_0 = (Il2CppObject *)L_3;
		SerialDisposable_t2547852742 * L_4 = (SerialDisposable_t2547852742 *)__this->get_cancelable_6();
		Il2CppObject * L_5 = V_0;
		Il2CppObject * L_6 = StableCompositeDisposable_Create_m3764788535(NULL /*static, unused*/, (Il2CppObject *)L_4, (Il2CppObject *)L_5, /*hidden argument*/NULL);
		return L_6;
	}
}
// System.Void UniRx.Operators.ThrottleObservable`1/Throttle<System.Int64>::OnNext(System.UInt64)
extern "C"  void Throttle_OnNext_m1768585380_gshared (Throttle_t1826605473 * __this, uint64_t ___currentid0, const MethodInfo* method)
{
	Il2CppObject * V_0 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_gate_3();
		V_0 = (Il2CppObject *)L_0;
		Il2CppObject * L_1 = V_0;
		Monitor_Enter_m476686225(NULL /*static, unused*/, (Il2CppObject *)L_1, /*hidden argument*/NULL);
	}

IL_000d:
	try
	{ // begin try (depth: 1)
		{
			bool L_2 = (bool)__this->get_hasValue_5();
			if (!L_2)
			{
				goto IL_0037;
			}
		}

IL_0018:
		{
			uint64_t L_3 = (uint64_t)__this->get_id_7();
			uint64_t L_4 = ___currentid0;
			if ((!(((uint64_t)L_3) == ((uint64_t)L_4))))
			{
				goto IL_0037;
			}
		}

IL_0024:
		{
			Il2CppObject* L_5 = (Il2CppObject*)((OperatorObserverBase_2_t3939730909 *)__this)->get_observer_0();
			il2cpp_codegen_memory_barrier();
			int64_t L_6 = (int64_t)__this->get_latestValue_4();
			NullCheck((Il2CppObject*)L_5);
			InterfaceActionInvoker1< int64_t >::Invoke(2 /* System.Void UniRx.IObserver`1<System.Int64>::OnNext(T) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3), (Il2CppObject*)L_5, (int64_t)L_6);
		}

IL_0037:
		{
			__this->set_hasValue_5((bool)0);
			IL2CPP_LEAVE(0x4A, FINALLY_0043);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0043;
	}

FINALLY_0043:
	{ // begin finally (depth: 1)
		Il2CppObject * L_7 = V_0;
		Monitor_Exit_m2088237919(NULL /*static, unused*/, (Il2CppObject *)L_7, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(67)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(67)
	{
		IL2CPP_JUMP_TBL(0x4A, IL_004a)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_004a:
	{
		return;
	}
}
// System.Void UniRx.Operators.ThrottleObservable`1/Throttle<System.Int64>::OnNext(T)
extern Il2CppClass* SingleAssignmentDisposable_t2336378823_il2cpp_TypeInfo_var;
extern Il2CppClass* Action_t437523947_il2cpp_TypeInfo_var;
extern Il2CppClass* IScheduler_t2938318244_il2cpp_TypeInfo_var;
extern const uint32_t Throttle_OnNext_m781399815_MetadataUsageId;
extern "C"  void Throttle_OnNext_m781399815_gshared (Throttle_t1826605473 * __this, int64_t ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Throttle_OnNext_m781399815_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Il2CppObject * V_0 = NULL;
	SingleAssignmentDisposable_t2336378823 * V_1 = NULL;
	U3COnNextU3Ec__AnonStorey6A_t4267933177 * V_2 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		U3COnNextU3Ec__AnonStorey6A_t4267933177 * L_0 = (U3COnNextU3Ec__AnonStorey6A_t4267933177 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		((  void (*) (U3COnNextU3Ec__AnonStorey6A_t4267933177 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->method)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		V_2 = (U3COnNextU3Ec__AnonStorey6A_t4267933177 *)L_0;
		U3COnNextU3Ec__AnonStorey6A_t4267933177 * L_1 = V_2;
		NullCheck(L_1);
		L_1->set_U3CU3Ef__this_1(__this);
		Il2CppObject * L_2 = (Il2CppObject *)__this->get_gate_3();
		V_0 = (Il2CppObject *)L_2;
		Il2CppObject * L_3 = V_0;
		Monitor_Enter_m476686225(NULL /*static, unused*/, (Il2CppObject *)L_3, /*hidden argument*/NULL);
	}

IL_001a:
	try
	{ // begin try (depth: 1)
		__this->set_hasValue_5((bool)1);
		int64_t L_4 = ___value0;
		__this->set_latestValue_4(L_4);
		uint64_t L_5 = (uint64_t)__this->get_id_7();
		__this->set_id_7(((int64_t)((int64_t)L_5+(int64_t)(((int64_t)((int64_t)1))))));
		U3COnNextU3Ec__AnonStorey6A_t4267933177 * L_6 = V_2;
		uint64_t L_7 = (uint64_t)__this->get_id_7();
		NullCheck(L_6);
		L_6->set_currentid_0(L_7);
		IL2CPP_LEAVE(0x4F, FINALLY_0048);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0048;
	}

FINALLY_0048:
	{ // begin finally (depth: 1)
		Il2CppObject * L_8 = V_0;
		Monitor_Exit_m2088237919(NULL /*static, unused*/, (Il2CppObject *)L_8, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(72)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(72)
	{
		IL2CPP_JUMP_TBL(0x4F, IL_004f)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_004f:
	{
		SingleAssignmentDisposable_t2336378823 * L_9 = (SingleAssignmentDisposable_t2336378823 *)il2cpp_codegen_object_new(SingleAssignmentDisposable_t2336378823_il2cpp_TypeInfo_var);
		SingleAssignmentDisposable__ctor_m917449122(L_9, /*hidden argument*/NULL);
		V_1 = (SingleAssignmentDisposable_t2336378823 *)L_9;
		SerialDisposable_t2547852742 * L_10 = (SerialDisposable_t2547852742 *)__this->get_cancelable_6();
		SingleAssignmentDisposable_t2336378823 * L_11 = V_1;
		NullCheck((SerialDisposable_t2547852742 *)L_10);
		SerialDisposable_set_Disposable_m1438046228((SerialDisposable_t2547852742 *)L_10, (Il2CppObject *)L_11, /*hidden argument*/NULL);
		SingleAssignmentDisposable_t2336378823 * L_12 = V_1;
		ThrottleObservable_1_t3227609402 * L_13 = (ThrottleObservable_1_t3227609402 *)__this->get_parent_2();
		NullCheck(L_13);
		Il2CppObject * L_14 = (Il2CppObject *)L_13->get_scheduler_3();
		ThrottleObservable_1_t3227609402 * L_15 = (ThrottleObservable_1_t3227609402 *)__this->get_parent_2();
		NullCheck(L_15);
		TimeSpan_t763862892  L_16 = (TimeSpan_t763862892 )L_15->get_dueTime_2();
		U3COnNextU3Ec__AnonStorey6A_t4267933177 * L_17 = V_2;
		IntPtr_t L_18;
		L_18.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		Action_t437523947 * L_19 = (Action_t437523947 *)il2cpp_codegen_object_new(Action_t437523947_il2cpp_TypeInfo_var);
		Action__ctor_m2957240604(L_19, (Il2CppObject *)L_17, (IntPtr_t)L_18, /*hidden argument*/NULL);
		NullCheck((Il2CppObject *)L_14);
		Il2CppObject * L_20 = InterfaceFuncInvoker2< Il2CppObject *, TimeSpan_t763862892 , Action_t437523947 * >::Invoke(2 /* System.IDisposable UniRx.IScheduler::Schedule(System.TimeSpan,System.Action) */, IScheduler_t2938318244_il2cpp_TypeInfo_var, (Il2CppObject *)L_14, (TimeSpan_t763862892 )L_16, (Action_t437523947 *)L_19);
		NullCheck((SingleAssignmentDisposable_t2336378823 *)L_12);
		SingleAssignmentDisposable_set_Disposable_m1599028885((SingleAssignmentDisposable_t2336378823 *)L_12, (Il2CppObject *)L_20, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UniRx.Operators.ThrottleObservable`1/Throttle<System.Int64>::OnError(System.Exception)
extern "C"  void Throttle_OnError_m1194222614_gshared (Throttle_t1826605473 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method)
{
	Il2CppObject * V_0 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		SerialDisposable_t2547852742 * L_0 = (SerialDisposable_t2547852742 *)__this->get_cancelable_6();
		NullCheck((SerialDisposable_t2547852742 *)L_0);
		VirtActionInvoker0::Invoke(4 /* System.Void UniRx.SerialDisposable::Dispose() */, (SerialDisposable_t2547852742 *)L_0);
		Il2CppObject * L_1 = (Il2CppObject *)__this->get_gate_3();
		V_0 = (Il2CppObject *)L_1;
		Il2CppObject * L_2 = V_0;
		Monitor_Enter_m476686225(NULL /*static, unused*/, (Il2CppObject *)L_2, /*hidden argument*/NULL);
	}

IL_0018:
	try
	{ // begin try (depth: 1)
		{
			__this->set_hasValue_5((bool)0);
			uint64_t L_3 = (uint64_t)__this->get_id_7();
			__this->set_id_7(((int64_t)((int64_t)L_3+(int64_t)(((int64_t)((int64_t)1))))));
		}

IL_002e:
		try
		{ // begin try (depth: 2)
			Il2CppObject* L_4 = (Il2CppObject*)((OperatorObserverBase_2_t3939730909 *)__this)->get_observer_0();
			il2cpp_codegen_memory_barrier();
			Exception_t1967233988 * L_5 = ___error0;
			NullCheck((Il2CppObject*)L_4);
			InterfaceActionInvoker1< Exception_t1967233988 * >::Invoke(1 /* System.Void UniRx.IObserver`1<System.Int64>::OnError(System.Exception) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3), (Il2CppObject*)L_4, (Exception_t1967233988 *)L_5);
			IL2CPP_LEAVE(0x48, FINALLY_0041);
		} // end try (depth: 2)
		catch(Il2CppExceptionWrapper& e)
		{
			__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
			goto FINALLY_0041;
		}

FINALLY_0041:
		{ // begin finally (depth: 2)
			NullCheck((OperatorObserverBase_2_t3939730909 *)__this);
			VirtActionInvoker0::Invoke(4 /* System.Void UniRx.Operators.OperatorObserverBase`2<System.Int64,System.Int64>::Dispose() */, (OperatorObserverBase_2_t3939730909 *)__this);
			IL2CPP_END_FINALLY(65)
		} // end finally (depth: 2)
		IL2CPP_CLEANUP(65)
		{
			IL2CPP_JUMP_TBL(0x48, IL_0048)
			IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
		}

IL_0048:
		{
			IL2CPP_LEAVE(0x54, FINALLY_004d);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_004d;
	}

FINALLY_004d:
	{ // begin finally (depth: 1)
		Il2CppObject * L_6 = V_0;
		Monitor_Exit_m2088237919(NULL /*static, unused*/, (Il2CppObject *)L_6, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(77)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(77)
	{
		IL2CPP_JUMP_TBL(0x54, IL_0054)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0054:
	{
		return;
	}
}
// System.Void UniRx.Operators.ThrottleObservable`1/Throttle<System.Int64>::OnCompleted()
extern "C"  void Throttle_OnCompleted_m160565353_gshared (Throttle_t1826605473 * __this, const MethodInfo* method)
{
	Il2CppObject * V_0 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		SerialDisposable_t2547852742 * L_0 = (SerialDisposable_t2547852742 *)__this->get_cancelable_6();
		NullCheck((SerialDisposable_t2547852742 *)L_0);
		VirtActionInvoker0::Invoke(4 /* System.Void UniRx.SerialDisposable::Dispose() */, (SerialDisposable_t2547852742 *)L_0);
		Il2CppObject * L_1 = (Il2CppObject *)__this->get_gate_3();
		V_0 = (Il2CppObject *)L_1;
		Il2CppObject * L_2 = V_0;
		Monitor_Enter_m476686225(NULL /*static, unused*/, (Il2CppObject *)L_2, /*hidden argument*/NULL);
	}

IL_0018:
	try
	{ // begin try (depth: 1)
		{
			bool L_3 = (bool)__this->get_hasValue_5();
			if (!L_3)
			{
				goto IL_0036;
			}
		}

IL_0023:
		{
			Il2CppObject* L_4 = (Il2CppObject*)((OperatorObserverBase_2_t3939730909 *)__this)->get_observer_0();
			il2cpp_codegen_memory_barrier();
			int64_t L_5 = (int64_t)__this->get_latestValue_4();
			NullCheck((Il2CppObject*)L_4);
			InterfaceActionInvoker1< int64_t >::Invoke(2 /* System.Void UniRx.IObserver`1<System.Int64>::OnNext(T) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3), (Il2CppObject*)L_4, (int64_t)L_5);
		}

IL_0036:
		{
			__this->set_hasValue_5((bool)0);
			uint64_t L_6 = (uint64_t)__this->get_id_7();
			__this->set_id_7(((int64_t)((int64_t)L_6+(int64_t)(((int64_t)((int64_t)1))))));
		}

IL_004c:
		try
		{ // begin try (depth: 2)
			Il2CppObject* L_7 = (Il2CppObject*)((OperatorObserverBase_2_t3939730909 *)__this)->get_observer_0();
			il2cpp_codegen_memory_barrier();
			NullCheck((Il2CppObject*)L_7);
			InterfaceActionInvoker0::Invoke(0 /* System.Void UniRx.IObserver`1<System.Int64>::OnCompleted() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3), (Il2CppObject*)L_7);
			IL2CPP_LEAVE(0x65, FINALLY_005e);
		} // end try (depth: 2)
		catch(Il2CppExceptionWrapper& e)
		{
			__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
			goto FINALLY_005e;
		}

FINALLY_005e:
		{ // begin finally (depth: 2)
			NullCheck((OperatorObserverBase_2_t3939730909 *)__this);
			VirtActionInvoker0::Invoke(4 /* System.Void UniRx.Operators.OperatorObserverBase`2<System.Int64,System.Int64>::Dispose() */, (OperatorObserverBase_2_t3939730909 *)__this);
			IL2CPP_END_FINALLY(94)
		} // end finally (depth: 2)
		IL2CPP_CLEANUP(94)
		{
			IL2CPP_JUMP_TBL(0x65, IL_0065)
			IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
		}

IL_0065:
		{
			IL2CPP_LEAVE(0x71, FINALLY_006a);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_006a;
	}

FINALLY_006a:
	{ // begin finally (depth: 1)
		Il2CppObject * L_8 = V_0;
		Monitor_Exit_m2088237919(NULL /*static, unused*/, (Il2CppObject *)L_8, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(106)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(106)
	{
		IL2CPP_JUMP_TBL(0x71, IL_0071)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0071:
	{
		return;
	}
}
// System.Void UniRx.Operators.ThrottleObservable`1/Throttle<System.Object>::.ctor(UniRx.Operators.ThrottleObservable`1<T>,UniRx.IObserver`1<T>,System.IDisposable)
extern Il2CppClass* Il2CppObject_il2cpp_TypeInfo_var;
extern const uint32_t Throttle__ctor_m2945034926_MetadataUsageId;
extern "C"  void Throttle__ctor_m2945034926_gshared (Throttle_t4111264307 * __this, ThrottleObservable_1_t1217300940 * ___parent0, Il2CppObject* ___observer1, Il2CppObject * ___cancel2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Throttle__ctor_m2945034926_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Il2CppObject * V_0 = NULL;
	{
		Il2CppObject * L_0 = (Il2CppObject *)il2cpp_codegen_object_new(Il2CppObject_il2cpp_TypeInfo_var);
		Object__ctor_m1772956182(L_0, /*hidden argument*/NULL);
		__this->set_gate_3(L_0);
		Initobj (Il2CppObject_il2cpp_TypeInfo_var, (&V_0));
		Il2CppObject * L_1 = V_0;
		__this->set_latestValue_4(L_1);
		Il2CppObject* L_2 = ___observer1;
		Il2CppObject * L_3 = ___cancel2;
		NullCheck((OperatorObserverBase_2_t1187768149 *)__this);
		((  void (*) (OperatorObserverBase_2_t1187768149 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((OperatorObserverBase_2_t1187768149 *)__this, (Il2CppObject*)L_2, (Il2CppObject *)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		ThrottleObservable_1_t1217300940 * L_4 = ___parent0;
		__this->set_parent_2(L_4);
		return;
	}
}
// System.IDisposable UniRx.Operators.ThrottleObservable`1/Throttle<System.Object>::Run()
extern Il2CppClass* SerialDisposable_t2547852742_il2cpp_TypeInfo_var;
extern const uint32_t Throttle_Run_m4019483013_MetadataUsageId;
extern "C"  Il2CppObject * Throttle_Run_m4019483013_gshared (Throttle_t4111264307 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Throttle_Run_m4019483013_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Il2CppObject * V_0 = NULL;
	{
		SerialDisposable_t2547852742 * L_0 = (SerialDisposable_t2547852742 *)il2cpp_codegen_object_new(SerialDisposable_t2547852742_il2cpp_TypeInfo_var);
		SerialDisposable__ctor_m2977852291(L_0, /*hidden argument*/NULL);
		__this->set_cancelable_6(L_0);
		ThrottleObservable_1_t1217300940 * L_1 = (ThrottleObservable_1_t1217300940 *)__this->get_parent_2();
		NullCheck(L_1);
		Il2CppObject* L_2 = (Il2CppObject*)L_1->get_source_1();
		NullCheck((Il2CppObject*)L_2);
		Il2CppObject * L_3 = InterfaceFuncInvoker1< Il2CppObject *, Il2CppObject* >::Invoke(0 /* System.IDisposable UniRx.IObservable`1<System.Object>::Subscribe(UniRx.IObserver`1<T>) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), (Il2CppObject*)L_2, (Il2CppObject*)__this);
		V_0 = (Il2CppObject *)L_3;
		SerialDisposable_t2547852742 * L_4 = (SerialDisposable_t2547852742 *)__this->get_cancelable_6();
		Il2CppObject * L_5 = V_0;
		Il2CppObject * L_6 = StableCompositeDisposable_Create_m3764788535(NULL /*static, unused*/, (Il2CppObject *)L_4, (Il2CppObject *)L_5, /*hidden argument*/NULL);
		return L_6;
	}
}
// System.Void UniRx.Operators.ThrottleObservable`1/Throttle<System.Object>::OnNext(System.UInt64)
extern "C"  void Throttle_OnNext_m2571322172_gshared (Throttle_t4111264307 * __this, uint64_t ___currentid0, const MethodInfo* method)
{
	Il2CppObject * V_0 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_gate_3();
		V_0 = (Il2CppObject *)L_0;
		Il2CppObject * L_1 = V_0;
		Monitor_Enter_m476686225(NULL /*static, unused*/, (Il2CppObject *)L_1, /*hidden argument*/NULL);
	}

IL_000d:
	try
	{ // begin try (depth: 1)
		{
			bool L_2 = (bool)__this->get_hasValue_5();
			if (!L_2)
			{
				goto IL_0037;
			}
		}

IL_0018:
		{
			uint64_t L_3 = (uint64_t)__this->get_id_7();
			uint64_t L_4 = ___currentid0;
			if ((!(((uint64_t)L_3) == ((uint64_t)L_4))))
			{
				goto IL_0037;
			}
		}

IL_0024:
		{
			Il2CppObject* L_5 = (Il2CppObject*)((OperatorObserverBase_2_t1187768149 *)__this)->get_observer_0();
			il2cpp_codegen_memory_barrier();
			Il2CppObject * L_6 = (Il2CppObject *)__this->get_latestValue_4();
			NullCheck((Il2CppObject*)L_5);
			InterfaceActionInvoker1< Il2CppObject * >::Invoke(2 /* System.Void UniRx.IObserver`1<System.Object>::OnNext(T) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3), (Il2CppObject*)L_5, (Il2CppObject *)L_6);
		}

IL_0037:
		{
			__this->set_hasValue_5((bool)0);
			IL2CPP_LEAVE(0x4A, FINALLY_0043);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0043;
	}

FINALLY_0043:
	{ // begin finally (depth: 1)
		Il2CppObject * L_7 = V_0;
		Monitor_Exit_m2088237919(NULL /*static, unused*/, (Il2CppObject *)L_7, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(67)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(67)
	{
		IL2CPP_JUMP_TBL(0x4A, IL_004a)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_004a:
	{
		return;
	}
}
// System.Void UniRx.Operators.ThrottleObservable`1/Throttle<System.Object>::OnNext(T)
extern Il2CppClass* SingleAssignmentDisposable_t2336378823_il2cpp_TypeInfo_var;
extern Il2CppClass* Action_t437523947_il2cpp_TypeInfo_var;
extern Il2CppClass* IScheduler_t2938318244_il2cpp_TypeInfo_var;
extern const uint32_t Throttle_OnNext_m706028959_MetadataUsageId;
extern "C"  void Throttle_OnNext_m706028959_gshared (Throttle_t4111264307 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Throttle_OnNext_m706028959_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Il2CppObject * V_0 = NULL;
	SingleAssignmentDisposable_t2336378823 * V_1 = NULL;
	U3COnNextU3Ec__AnonStorey6A_t2257624715 * V_2 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		U3COnNextU3Ec__AnonStorey6A_t2257624715 * L_0 = (U3COnNextU3Ec__AnonStorey6A_t2257624715 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		((  void (*) (U3COnNextU3Ec__AnonStorey6A_t2257624715 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->method)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		V_2 = (U3COnNextU3Ec__AnonStorey6A_t2257624715 *)L_0;
		U3COnNextU3Ec__AnonStorey6A_t2257624715 * L_1 = V_2;
		NullCheck(L_1);
		L_1->set_U3CU3Ef__this_1(__this);
		Il2CppObject * L_2 = (Il2CppObject *)__this->get_gate_3();
		V_0 = (Il2CppObject *)L_2;
		Il2CppObject * L_3 = V_0;
		Monitor_Enter_m476686225(NULL /*static, unused*/, (Il2CppObject *)L_3, /*hidden argument*/NULL);
	}

IL_001a:
	try
	{ // begin try (depth: 1)
		__this->set_hasValue_5((bool)1);
		Il2CppObject * L_4 = ___value0;
		__this->set_latestValue_4(L_4);
		uint64_t L_5 = (uint64_t)__this->get_id_7();
		__this->set_id_7(((int64_t)((int64_t)L_5+(int64_t)(((int64_t)((int64_t)1))))));
		U3COnNextU3Ec__AnonStorey6A_t2257624715 * L_6 = V_2;
		uint64_t L_7 = (uint64_t)__this->get_id_7();
		NullCheck(L_6);
		L_6->set_currentid_0(L_7);
		IL2CPP_LEAVE(0x4F, FINALLY_0048);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0048;
	}

FINALLY_0048:
	{ // begin finally (depth: 1)
		Il2CppObject * L_8 = V_0;
		Monitor_Exit_m2088237919(NULL /*static, unused*/, (Il2CppObject *)L_8, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(72)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(72)
	{
		IL2CPP_JUMP_TBL(0x4F, IL_004f)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_004f:
	{
		SingleAssignmentDisposable_t2336378823 * L_9 = (SingleAssignmentDisposable_t2336378823 *)il2cpp_codegen_object_new(SingleAssignmentDisposable_t2336378823_il2cpp_TypeInfo_var);
		SingleAssignmentDisposable__ctor_m917449122(L_9, /*hidden argument*/NULL);
		V_1 = (SingleAssignmentDisposable_t2336378823 *)L_9;
		SerialDisposable_t2547852742 * L_10 = (SerialDisposable_t2547852742 *)__this->get_cancelable_6();
		SingleAssignmentDisposable_t2336378823 * L_11 = V_1;
		NullCheck((SerialDisposable_t2547852742 *)L_10);
		SerialDisposable_set_Disposable_m1438046228((SerialDisposable_t2547852742 *)L_10, (Il2CppObject *)L_11, /*hidden argument*/NULL);
		SingleAssignmentDisposable_t2336378823 * L_12 = V_1;
		ThrottleObservable_1_t1217300940 * L_13 = (ThrottleObservable_1_t1217300940 *)__this->get_parent_2();
		NullCheck(L_13);
		Il2CppObject * L_14 = (Il2CppObject *)L_13->get_scheduler_3();
		ThrottleObservable_1_t1217300940 * L_15 = (ThrottleObservable_1_t1217300940 *)__this->get_parent_2();
		NullCheck(L_15);
		TimeSpan_t763862892  L_16 = (TimeSpan_t763862892 )L_15->get_dueTime_2();
		U3COnNextU3Ec__AnonStorey6A_t2257624715 * L_17 = V_2;
		IntPtr_t L_18;
		L_18.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		Action_t437523947 * L_19 = (Action_t437523947 *)il2cpp_codegen_object_new(Action_t437523947_il2cpp_TypeInfo_var);
		Action__ctor_m2957240604(L_19, (Il2CppObject *)L_17, (IntPtr_t)L_18, /*hidden argument*/NULL);
		NullCheck((Il2CppObject *)L_14);
		Il2CppObject * L_20 = InterfaceFuncInvoker2< Il2CppObject *, TimeSpan_t763862892 , Action_t437523947 * >::Invoke(2 /* System.IDisposable UniRx.IScheduler::Schedule(System.TimeSpan,System.Action) */, IScheduler_t2938318244_il2cpp_TypeInfo_var, (Il2CppObject *)L_14, (TimeSpan_t763862892 )L_16, (Action_t437523947 *)L_19);
		NullCheck((SingleAssignmentDisposable_t2336378823 *)L_12);
		SingleAssignmentDisposable_set_Disposable_m1599028885((SingleAssignmentDisposable_t2336378823 *)L_12, (Il2CppObject *)L_20, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UniRx.Operators.ThrottleObservable`1/Throttle<System.Object>::OnError(System.Exception)
extern "C"  void Throttle_OnError_m4059046574_gshared (Throttle_t4111264307 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method)
{
	Il2CppObject * V_0 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		SerialDisposable_t2547852742 * L_0 = (SerialDisposable_t2547852742 *)__this->get_cancelable_6();
		NullCheck((SerialDisposable_t2547852742 *)L_0);
		VirtActionInvoker0::Invoke(4 /* System.Void UniRx.SerialDisposable::Dispose() */, (SerialDisposable_t2547852742 *)L_0);
		Il2CppObject * L_1 = (Il2CppObject *)__this->get_gate_3();
		V_0 = (Il2CppObject *)L_1;
		Il2CppObject * L_2 = V_0;
		Monitor_Enter_m476686225(NULL /*static, unused*/, (Il2CppObject *)L_2, /*hidden argument*/NULL);
	}

IL_0018:
	try
	{ // begin try (depth: 1)
		{
			__this->set_hasValue_5((bool)0);
			uint64_t L_3 = (uint64_t)__this->get_id_7();
			__this->set_id_7(((int64_t)((int64_t)L_3+(int64_t)(((int64_t)((int64_t)1))))));
		}

IL_002e:
		try
		{ // begin try (depth: 2)
			Il2CppObject* L_4 = (Il2CppObject*)((OperatorObserverBase_2_t1187768149 *)__this)->get_observer_0();
			il2cpp_codegen_memory_barrier();
			Exception_t1967233988 * L_5 = ___error0;
			NullCheck((Il2CppObject*)L_4);
			InterfaceActionInvoker1< Exception_t1967233988 * >::Invoke(1 /* System.Void UniRx.IObserver`1<System.Object>::OnError(System.Exception) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3), (Il2CppObject*)L_4, (Exception_t1967233988 *)L_5);
			IL2CPP_LEAVE(0x48, FINALLY_0041);
		} // end try (depth: 2)
		catch(Il2CppExceptionWrapper& e)
		{
			__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
			goto FINALLY_0041;
		}

FINALLY_0041:
		{ // begin finally (depth: 2)
			NullCheck((OperatorObserverBase_2_t1187768149 *)__this);
			VirtActionInvoker0::Invoke(4 /* System.Void UniRx.Operators.OperatorObserverBase`2<System.Object,System.Object>::Dispose() */, (OperatorObserverBase_2_t1187768149 *)__this);
			IL2CPP_END_FINALLY(65)
		} // end finally (depth: 2)
		IL2CPP_CLEANUP(65)
		{
			IL2CPP_JUMP_TBL(0x48, IL_0048)
			IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
		}

IL_0048:
		{
			IL2CPP_LEAVE(0x54, FINALLY_004d);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_004d;
	}

FINALLY_004d:
	{ // begin finally (depth: 1)
		Il2CppObject * L_6 = V_0;
		Monitor_Exit_m2088237919(NULL /*static, unused*/, (Il2CppObject *)L_6, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(77)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(77)
	{
		IL2CPP_JUMP_TBL(0x54, IL_0054)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0054:
	{
		return;
	}
}
// System.Void UniRx.Operators.ThrottleObservable`1/Throttle<System.Object>::OnCompleted()
extern "C"  void Throttle_OnCompleted_m2127227649_gshared (Throttle_t4111264307 * __this, const MethodInfo* method)
{
	Il2CppObject * V_0 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		SerialDisposable_t2547852742 * L_0 = (SerialDisposable_t2547852742 *)__this->get_cancelable_6();
		NullCheck((SerialDisposable_t2547852742 *)L_0);
		VirtActionInvoker0::Invoke(4 /* System.Void UniRx.SerialDisposable::Dispose() */, (SerialDisposable_t2547852742 *)L_0);
		Il2CppObject * L_1 = (Il2CppObject *)__this->get_gate_3();
		V_0 = (Il2CppObject *)L_1;
		Il2CppObject * L_2 = V_0;
		Monitor_Enter_m476686225(NULL /*static, unused*/, (Il2CppObject *)L_2, /*hidden argument*/NULL);
	}

IL_0018:
	try
	{ // begin try (depth: 1)
		{
			bool L_3 = (bool)__this->get_hasValue_5();
			if (!L_3)
			{
				goto IL_0036;
			}
		}

IL_0023:
		{
			Il2CppObject* L_4 = (Il2CppObject*)((OperatorObserverBase_2_t1187768149 *)__this)->get_observer_0();
			il2cpp_codegen_memory_barrier();
			Il2CppObject * L_5 = (Il2CppObject *)__this->get_latestValue_4();
			NullCheck((Il2CppObject*)L_4);
			InterfaceActionInvoker1< Il2CppObject * >::Invoke(2 /* System.Void UniRx.IObserver`1<System.Object>::OnNext(T) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3), (Il2CppObject*)L_4, (Il2CppObject *)L_5);
		}

IL_0036:
		{
			__this->set_hasValue_5((bool)0);
			uint64_t L_6 = (uint64_t)__this->get_id_7();
			__this->set_id_7(((int64_t)((int64_t)L_6+(int64_t)(((int64_t)((int64_t)1))))));
		}

IL_004c:
		try
		{ // begin try (depth: 2)
			Il2CppObject* L_7 = (Il2CppObject*)((OperatorObserverBase_2_t1187768149 *)__this)->get_observer_0();
			il2cpp_codegen_memory_barrier();
			NullCheck((Il2CppObject*)L_7);
			InterfaceActionInvoker0::Invoke(0 /* System.Void UniRx.IObserver`1<System.Object>::OnCompleted() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3), (Il2CppObject*)L_7);
			IL2CPP_LEAVE(0x65, FINALLY_005e);
		} // end try (depth: 2)
		catch(Il2CppExceptionWrapper& e)
		{
			__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
			goto FINALLY_005e;
		}

FINALLY_005e:
		{ // begin finally (depth: 2)
			NullCheck((OperatorObserverBase_2_t1187768149 *)__this);
			VirtActionInvoker0::Invoke(4 /* System.Void UniRx.Operators.OperatorObserverBase`2<System.Object,System.Object>::Dispose() */, (OperatorObserverBase_2_t1187768149 *)__this);
			IL2CPP_END_FINALLY(94)
		} // end finally (depth: 2)
		IL2CPP_CLEANUP(94)
		{
			IL2CPP_JUMP_TBL(0x65, IL_0065)
			IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
		}

IL_0065:
		{
			IL2CPP_LEAVE(0x71, FINALLY_006a);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_006a;
	}

FINALLY_006a:
	{ // begin finally (depth: 1)
		Il2CppObject * L_8 = V_0;
		Monitor_Exit_m2088237919(NULL /*static, unused*/, (Il2CppObject *)L_8, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(106)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(106)
	{
		IL2CPP_JUMP_TBL(0x71, IL_0071)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0071:
	{
		return;
	}
}
// System.Void UniRx.Operators.ThrottleObservable`1<System.Int64>::.ctor(UniRx.IObservable`1<T>,System.TimeSpan,UniRx.IScheduler)
extern Il2CppClass* Scheduler_t103934925_il2cpp_TypeInfo_var;
extern const uint32_t ThrottleObservable_1__ctor_m2092983233_MetadataUsageId;
extern "C"  void ThrottleObservable_1__ctor_m2092983233_gshared (ThrottleObservable_1_t3227609402 * __this, Il2CppObject* ___source0, TimeSpan_t763862892  ___dueTime1, Il2CppObject * ___scheduler2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ThrottleObservable_1__ctor_m2092983233_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	ThrottleObservable_1_t3227609402 * G_B2_0 = NULL;
	ThrottleObservable_1_t3227609402 * G_B1_0 = NULL;
	int32_t G_B3_0 = 0;
	ThrottleObservable_1_t3227609402 * G_B3_1 = NULL;
	{
		Il2CppObject * L_0 = ___scheduler2;
		IL2CPP_RUNTIME_CLASS_INIT(Scheduler_t103934925_il2cpp_TypeInfo_var);
		Il2CppObject * L_1 = ((Scheduler_t103934925_StaticFields*)Scheduler_t103934925_il2cpp_TypeInfo_var->static_fields)->get_CurrentThread_0();
		G_B1_0 = ((ThrottleObservable_1_t3227609402 *)(__this));
		if ((((Il2CppObject*)(Il2CppObject *)L_0) == ((Il2CppObject*)(Il2CppObject *)L_1)))
		{
			G_B2_0 = ((ThrottleObservable_1_t3227609402 *)(__this));
			goto IL_0014;
		}
	}
	{
		Il2CppObject* L_2 = ___source0;
		bool L_3 = ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)(NULL /*static, unused*/, (Il2CppObject*)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		G_B3_0 = ((int32_t)(L_3));
		G_B3_1 = ((ThrottleObservable_1_t3227609402 *)(G_B1_0));
		goto IL_0015;
	}

IL_0014:
	{
		G_B3_0 = 1;
		G_B3_1 = ((ThrottleObservable_1_t3227609402 *)(G_B2_0));
	}

IL_0015:
	{
		NullCheck((OperatorObservableBase_1_t1911559853 *)G_B3_1);
		((  void (*) (OperatorObservableBase_1_t1911559853 *, bool, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)((OperatorObservableBase_1_t1911559853 *)G_B3_1, (bool)G_B3_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		Il2CppObject* L_4 = ___source0;
		__this->set_source_1(L_4);
		TimeSpan_t763862892  L_5 = ___dueTime1;
		__this->set_dueTime_2(L_5);
		Il2CppObject * L_6 = ___scheduler2;
		__this->set_scheduler_3(L_6);
		return;
	}
}
// System.IDisposable UniRx.Operators.ThrottleObservable`1<System.Int64>::SubscribeCore(UniRx.IObserver`1<T>,System.IDisposable)
extern "C"  Il2CppObject * ThrottleObservable_1_SubscribeCore_m2387645976_gshared (ThrottleObservable_1_t3227609402 * __this, Il2CppObject* ___observer0, Il2CppObject * ___cancel1, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = ___observer0;
		Il2CppObject * L_1 = ___cancel1;
		Throttle_t1826605473 * L_2 = (Throttle_t1826605473 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		((  void (*) (Throttle_t1826605473 *, ThrottleObservable_1_t3227609402 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)(L_2, (ThrottleObservable_1_t3227609402 *)__this, (Il2CppObject*)L_0, (Il2CppObject *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		NullCheck((Throttle_t1826605473 *)L_2);
		Il2CppObject * L_3 = ((  Il2CppObject * (*) (Throttle_t1826605473 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->method)((Throttle_t1826605473 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		return L_3;
	}
}
// System.Void UniRx.Operators.ThrottleObservable`1<System.Object>::.ctor(UniRx.IObservable`1<T>,System.TimeSpan,UniRx.IScheduler)
extern Il2CppClass* Scheduler_t103934925_il2cpp_TypeInfo_var;
extern const uint32_t ThrottleObservable_1__ctor_m2162058711_MetadataUsageId;
extern "C"  void ThrottleObservable_1__ctor_m2162058711_gshared (ThrottleObservable_1_t1217300940 * __this, Il2CppObject* ___source0, TimeSpan_t763862892  ___dueTime1, Il2CppObject * ___scheduler2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ThrottleObservable_1__ctor_m2162058711_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	ThrottleObservable_1_t1217300940 * G_B2_0 = NULL;
	ThrottleObservable_1_t1217300940 * G_B1_0 = NULL;
	int32_t G_B3_0 = 0;
	ThrottleObservable_1_t1217300940 * G_B3_1 = NULL;
	{
		Il2CppObject * L_0 = ___scheduler2;
		IL2CPP_RUNTIME_CLASS_INIT(Scheduler_t103934925_il2cpp_TypeInfo_var);
		Il2CppObject * L_1 = ((Scheduler_t103934925_StaticFields*)Scheduler_t103934925_il2cpp_TypeInfo_var->static_fields)->get_CurrentThread_0();
		G_B1_0 = ((ThrottleObservable_1_t1217300940 *)(__this));
		if ((((Il2CppObject*)(Il2CppObject *)L_0) == ((Il2CppObject*)(Il2CppObject *)L_1)))
		{
			G_B2_0 = ((ThrottleObservable_1_t1217300940 *)(__this));
			goto IL_0014;
		}
	}
	{
		Il2CppObject* L_2 = ___source0;
		bool L_3 = ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)(NULL /*static, unused*/, (Il2CppObject*)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		G_B3_0 = ((int32_t)(L_3));
		G_B3_1 = ((ThrottleObservable_1_t1217300940 *)(G_B1_0));
		goto IL_0015;
	}

IL_0014:
	{
		G_B3_0 = 1;
		G_B3_1 = ((ThrottleObservable_1_t1217300940 *)(G_B2_0));
	}

IL_0015:
	{
		NullCheck((OperatorObservableBase_1_t4196218687 *)G_B3_1);
		((  void (*) (OperatorObservableBase_1_t4196218687 *, bool, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)((OperatorObservableBase_1_t4196218687 *)G_B3_1, (bool)G_B3_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		Il2CppObject* L_4 = ___source0;
		__this->set_source_1(L_4);
		TimeSpan_t763862892  L_5 = ___dueTime1;
		__this->set_dueTime_2(L_5);
		Il2CppObject * L_6 = ___scheduler2;
		__this->set_scheduler_3(L_6);
		return;
	}
}
// System.IDisposable UniRx.Operators.ThrottleObservable`1<System.Object>::SubscribeCore(UniRx.IObserver`1<T>,System.IDisposable)
extern "C"  Il2CppObject * ThrottleObservable_1_SubscribeCore_m1984715416_gshared (ThrottleObservable_1_t1217300940 * __this, Il2CppObject* ___observer0, Il2CppObject * ___cancel1, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = ___observer0;
		Il2CppObject * L_1 = ___cancel1;
		Throttle_t4111264307 * L_2 = (Throttle_t4111264307 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		((  void (*) (Throttle_t4111264307 *, ThrottleObservable_1_t1217300940 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)(L_2, (ThrottleObservable_1_t1217300940 *)__this, (Il2CppObject*)L_0, (Il2CppObject *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		NullCheck((Throttle_t4111264307 *)L_2);
		Il2CppObject * L_3 = ((  Il2CppObject * (*) (Throttle_t4111264307 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->method)((Throttle_t4111264307 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		return L_3;
	}
}
// System.Void UniRx.Operators.ThrowObservable`1/<SubscribeCore>c__AnonStorey6B<System.Object>::.ctor()
extern "C"  void U3CSubscribeCoreU3Ec__AnonStorey6B__ctor_m161005097_gshared (U3CSubscribeCoreU3Ec__AnonStorey6B_t1626282475 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UniRx.Operators.ThrowObservable`1/<SubscribeCore>c__AnonStorey6B<System.Object>::<>m__89()
extern "C"  void U3CSubscribeCoreU3Ec__AnonStorey6B_U3CU3Em__89_m2415332723_gshared (U3CSubscribeCoreU3Ec__AnonStorey6B_t1626282475 * __this, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = (Il2CppObject*)__this->get_observer_0();
		ThrowObservable_1_t3672276456 * L_1 = (ThrowObservable_1_t3672276456 *)__this->get_U3CU3Ef__this_1();
		NullCheck(L_1);
		Exception_t1967233988 * L_2 = (Exception_t1967233988 *)L_1->get_error_1();
		NullCheck((Il2CppObject*)L_0);
		InterfaceActionInvoker1< Exception_t1967233988 * >::Invoke(1 /* System.Void UniRx.IObserver`1<System.Object>::OnError(System.Exception) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0), (Il2CppObject*)L_0, (Exception_t1967233988 *)L_2);
		Il2CppObject* L_3 = (Il2CppObject*)__this->get_observer_0();
		NullCheck((Il2CppObject*)L_3);
		InterfaceActionInvoker0::Invoke(0 /* System.Void UniRx.IObserver`1<System.Object>::OnCompleted() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0), (Il2CppObject*)L_3);
		return;
	}
}
// System.Void UniRx.Operators.ThrowObservable`1/<SubscribeCore>c__AnonStorey6B<UniRx.Unit>::.ctor()
extern "C"  void U3CSubscribeCoreU3Ec__AnonStorey6B__ctor_m3062253659_gshared (U3CSubscribeCoreU3Ec__AnonStorey6B_t3347462093 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UniRx.Operators.ThrowObservable`1/<SubscribeCore>c__AnonStorey6B<UniRx.Unit>::<>m__89()
extern "C"  void U3CSubscribeCoreU3Ec__AnonStorey6B_U3CU3Em__89_m3081425701_gshared (U3CSubscribeCoreU3Ec__AnonStorey6B_t3347462093 * __this, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = (Il2CppObject*)__this->get_observer_0();
		ThrowObservable_1_t1098488778 * L_1 = (ThrowObservable_1_t1098488778 *)__this->get_U3CU3Ef__this_1();
		NullCheck(L_1);
		Exception_t1967233988 * L_2 = (Exception_t1967233988 *)L_1->get_error_1();
		NullCheck((Il2CppObject*)L_0);
		InterfaceActionInvoker1< Exception_t1967233988 * >::Invoke(1 /* System.Void UniRx.IObserver`1<UniRx.Unit>::OnError(System.Exception) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0), (Il2CppObject*)L_0, (Exception_t1967233988 *)L_2);
		Il2CppObject* L_3 = (Il2CppObject*)__this->get_observer_0();
		NullCheck((Il2CppObject*)L_3);
		InterfaceActionInvoker0::Invoke(0 /* System.Void UniRx.IObserver`1<UniRx.Unit>::OnCompleted() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0), (Il2CppObject*)L_3);
		return;
	}
}
// System.Void UniRx.Operators.ThrowObservable`1/Throw<System.Object>::.ctor(UniRx.IObserver`1<T>,System.IDisposable)
extern "C"  void Throw__ctor_m2513031773_gshared (Throw_t4151912527 * __this, Il2CppObject* ___observer0, Il2CppObject * ___cancel1, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = ___observer0;
		Il2CppObject * L_1 = ___cancel1;
		NullCheck((OperatorObserverBase_2_t1187768149 *)__this);
		((  void (*) (OperatorObserverBase_2_t1187768149 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((OperatorObserverBase_2_t1187768149 *)__this, (Il2CppObject*)L_0, (Il2CppObject *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		return;
	}
}
// System.Void UniRx.Operators.ThrowObservable`1/Throw<System.Object>::OnNext(T)
extern Il2CppClass* Il2CppObject_il2cpp_TypeInfo_var;
extern const uint32_t Throw_OnNext_m383148821_MetadataUsageId;
extern "C"  void Throw_OnNext_m383148821_gshared (Throw_t4151912527 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Throw_OnNext_m383148821_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		Il2CppObject* L_0 = (Il2CppObject*)((OperatorObserverBase_2_t1187768149 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		Il2CppObject * L_1 = ___value0;
		NullCheck((Il2CppObject*)L_0);
		InterfaceActionInvoker1< Il2CppObject * >::Invoke(2 /* System.Void UniRx.IObserver`1<System.Object>::OnNext(T) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), (Il2CppObject*)L_0, (Il2CppObject *)L_1);
		goto IL_0021;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1967233988 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Il2CppObject_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_0013;
		throw e;
	}

CATCH_0013:
	{ // begin catch(System.Object)
		{
			NullCheck((OperatorObserverBase_2_t1187768149 *)__this);
			VirtActionInvoker0::Invoke(4 /* System.Void UniRx.Operators.OperatorObserverBase`2<System.Object,System.Object>::Dispose() */, (OperatorObserverBase_2_t1187768149 *)__this);
			IL2CPP_RAISE_MANAGED_EXCEPTION(__exception_local);
		}

IL_001c:
		{
			goto IL_0021;
		}
	} // end catch (depth: 1)

IL_0021:
	{
		return;
	}
}
// System.Void UniRx.Operators.ThrowObservable`1/Throw<System.Object>::OnError(System.Exception)
extern "C"  void Throw_OnError_m3840128036_gshared (Throw_t4151912527 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method)
{
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		Il2CppObject* L_0 = (Il2CppObject*)((OperatorObserverBase_2_t1187768149 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		Exception_t1967233988 * L_1 = ___error0;
		NullCheck((Il2CppObject*)L_0);
		InterfaceActionInvoker1< Exception_t1967233988 * >::Invoke(1 /* System.Void UniRx.IObserver`1<System.Object>::OnError(System.Exception) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), (Il2CppObject*)L_0, (Exception_t1967233988 *)L_1);
		IL2CPP_LEAVE(0x1A, FINALLY_0013);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0013;
	}

FINALLY_0013:
	{ // begin finally (depth: 1)
		NullCheck((OperatorObserverBase_2_t1187768149 *)__this);
		VirtActionInvoker0::Invoke(4 /* System.Void UniRx.Operators.OperatorObserverBase`2<System.Object,System.Object>::Dispose() */, (OperatorObserverBase_2_t1187768149 *)__this);
		IL2CPP_END_FINALLY(19)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(19)
	{
		IL2CPP_JUMP_TBL(0x1A, IL_001a)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_001a:
	{
		return;
	}
}
// System.Void UniRx.Operators.ThrowObservable`1/Throw<System.Object>::OnCompleted()
extern "C"  void Throw_OnCompleted_m2233761143_gshared (Throw_t4151912527 * __this, const MethodInfo* method)
{
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		Il2CppObject* L_0 = (Il2CppObject*)((OperatorObserverBase_2_t1187768149 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		NullCheck((Il2CppObject*)L_0);
		InterfaceActionInvoker0::Invoke(0 /* System.Void UniRx.IObserver`1<System.Object>::OnCompleted() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), (Il2CppObject*)L_0);
		IL2CPP_LEAVE(0x19, FINALLY_0012);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0012;
	}

FINALLY_0012:
	{ // begin finally (depth: 1)
		NullCheck((OperatorObserverBase_2_t1187768149 *)__this);
		VirtActionInvoker0::Invoke(4 /* System.Void UniRx.Operators.OperatorObserverBase`2<System.Object,System.Object>::Dispose() */, (OperatorObserverBase_2_t1187768149 *)__this);
		IL2CPP_END_FINALLY(18)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(18)
	{
		IL2CPP_JUMP_TBL(0x19, IL_0019)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0019:
	{
		return;
	}
}
// System.Void UniRx.Operators.ThrowObservable`1/Throw<UniRx.Unit>::.ctor(UniRx.IObserver`1<T>,System.IDisposable)
extern "C"  void Throw__ctor_m1884098931_gshared (Throw_t1578124849 * __this, Il2CppObject* ___observer0, Il2CppObject * ___cancel1, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = ___observer0;
		Il2CppObject * L_1 = ___cancel1;
		NullCheck((OperatorObserverBase_2_t4165376397 *)__this);
		((  void (*) (OperatorObserverBase_2_t4165376397 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((OperatorObserverBase_2_t4165376397 *)__this, (Il2CppObject*)L_0, (Il2CppObject *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		return;
	}
}
// System.Void UniRx.Operators.ThrowObservable`1/Throw<UniRx.Unit>::OnNext(T)
extern Il2CppClass* Il2CppObject_il2cpp_TypeInfo_var;
extern const uint32_t Throw_OnNext_m2682828351_MetadataUsageId;
extern "C"  void Throw_OnNext_m2682828351_gshared (Throw_t1578124849 * __this, Unit_t2558286038  ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Throw_OnNext_m2682828351_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		Il2CppObject* L_0 = (Il2CppObject*)((OperatorObserverBase_2_t4165376397 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		Unit_t2558286038  L_1 = ___value0;
		NullCheck((Il2CppObject*)L_0);
		InterfaceActionInvoker1< Unit_t2558286038  >::Invoke(2 /* System.Void UniRx.IObserver`1<UniRx.Unit>::OnNext(T) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), (Il2CppObject*)L_0, (Unit_t2558286038 )L_1);
		goto IL_0021;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1967233988 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Il2CppObject_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_0013;
		throw e;
	}

CATCH_0013:
	{ // begin catch(System.Object)
		{
			NullCheck((OperatorObserverBase_2_t4165376397 *)__this);
			VirtActionInvoker0::Invoke(4 /* System.Void UniRx.Operators.OperatorObserverBase`2<UniRx.Unit,UniRx.Unit>::Dispose() */, (OperatorObserverBase_2_t4165376397 *)__this);
			IL2CPP_RAISE_MANAGED_EXCEPTION(__exception_local);
		}

IL_001c:
		{
			goto IL_0021;
		}
	} // end catch (depth: 1)

IL_0021:
	{
		return;
	}
}
// System.Void UniRx.Operators.ThrowObservable`1/Throw<UniRx.Unit>::OnError(System.Exception)
extern "C"  void Throw_OnError_m1489924942_gshared (Throw_t1578124849 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method)
{
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		Il2CppObject* L_0 = (Il2CppObject*)((OperatorObserverBase_2_t4165376397 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		Exception_t1967233988 * L_1 = ___error0;
		NullCheck((Il2CppObject*)L_0);
		InterfaceActionInvoker1< Exception_t1967233988 * >::Invoke(1 /* System.Void UniRx.IObserver`1<UniRx.Unit>::OnError(System.Exception) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), (Il2CppObject*)L_0, (Exception_t1967233988 *)L_1);
		IL2CPP_LEAVE(0x1A, FINALLY_0013);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0013;
	}

FINALLY_0013:
	{ // begin finally (depth: 1)
		NullCheck((OperatorObserverBase_2_t4165376397 *)__this);
		VirtActionInvoker0::Invoke(4 /* System.Void UniRx.Operators.OperatorObserverBase`2<UniRx.Unit,UniRx.Unit>::Dispose() */, (OperatorObserverBase_2_t4165376397 *)__this);
		IL2CPP_END_FINALLY(19)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(19)
	{
		IL2CPP_JUMP_TBL(0x1A, IL_001a)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_001a:
	{
		return;
	}
}
// System.Void UniRx.Operators.ThrowObservable`1/Throw<UniRx.Unit>::OnCompleted()
extern "C"  void Throw_OnCompleted_m3374656417_gshared (Throw_t1578124849 * __this, const MethodInfo* method)
{
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		Il2CppObject* L_0 = (Il2CppObject*)((OperatorObserverBase_2_t4165376397 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		NullCheck((Il2CppObject*)L_0);
		InterfaceActionInvoker0::Invoke(0 /* System.Void UniRx.IObserver`1<UniRx.Unit>::OnCompleted() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), (Il2CppObject*)L_0);
		IL2CPP_LEAVE(0x19, FINALLY_0012);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0012;
	}

FINALLY_0012:
	{ // begin finally (depth: 1)
		NullCheck((OperatorObserverBase_2_t4165376397 *)__this);
		VirtActionInvoker0::Invoke(4 /* System.Void UniRx.Operators.OperatorObserverBase`2<UniRx.Unit,UniRx.Unit>::Dispose() */, (OperatorObserverBase_2_t4165376397 *)__this);
		IL2CPP_END_FINALLY(18)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(18)
	{
		IL2CPP_JUMP_TBL(0x19, IL_0019)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0019:
	{
		return;
	}
}
// System.Void UniRx.Operators.ThrowObservable`1<System.Object>::.ctor(System.Exception,UniRx.IScheduler)
extern Il2CppClass* Scheduler_t103934925_il2cpp_TypeInfo_var;
extern const uint32_t ThrowObservable_1__ctor_m2390513590_MetadataUsageId;
extern "C"  void ThrowObservable_1__ctor_m2390513590_gshared (ThrowObservable_1_t3672276456 * __this, Exception_t1967233988 * ___error0, Il2CppObject * ___scheduler1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ThrowObservable_1__ctor_m2390513590_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject * L_0 = ___scheduler1;
		IL2CPP_RUNTIME_CLASS_INIT(Scheduler_t103934925_il2cpp_TypeInfo_var);
		Il2CppObject * L_1 = ((Scheduler_t103934925_StaticFields*)Scheduler_t103934925_il2cpp_TypeInfo_var->static_fields)->get_CurrentThread_0();
		NullCheck((OperatorObservableBase_1_t4196218687 *)__this);
		((  void (*) (OperatorObservableBase_1_t4196218687 *, bool, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((OperatorObservableBase_1_t4196218687 *)__this, (bool)((((Il2CppObject*)(Il2CppObject *)L_0) == ((Il2CppObject*)(Il2CppObject *)L_1))? 1 : 0), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		Exception_t1967233988 * L_2 = ___error0;
		__this->set_error_1(L_2);
		Il2CppObject * L_3 = ___scheduler1;
		__this->set_scheduler_2(L_3);
		return;
	}
}
// System.IDisposable UniRx.Operators.ThrowObservable`1<System.Object>::SubscribeCore(UniRx.IObserver`1<T>,System.IDisposable)
extern Il2CppClass* Scheduler_t103934925_il2cpp_TypeInfo_var;
extern Il2CppClass* Disposable_t3736388786_il2cpp_TypeInfo_var;
extern Il2CppClass* Action_t437523947_il2cpp_TypeInfo_var;
extern Il2CppClass* IScheduler_t2938318244_il2cpp_TypeInfo_var;
extern const uint32_t ThrowObservable_1_SubscribeCore_m3172290664_MetadataUsageId;
extern "C"  Il2CppObject * ThrowObservable_1_SubscribeCore_m3172290664_gshared (ThrowObservable_1_t3672276456 * __this, Il2CppObject* ___observer0, Il2CppObject * ___cancel1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ThrowObservable_1_SubscribeCore_m3172290664_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	U3CSubscribeCoreU3Ec__AnonStorey6B_t1626282475 * V_0 = NULL;
	{
		U3CSubscribeCoreU3Ec__AnonStorey6B_t1626282475 * L_0 = (U3CSubscribeCoreU3Ec__AnonStorey6B_t1626282475 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		((  void (*) (U3CSubscribeCoreU3Ec__AnonStorey6B_t1626282475 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		V_0 = (U3CSubscribeCoreU3Ec__AnonStorey6B_t1626282475 *)L_0;
		U3CSubscribeCoreU3Ec__AnonStorey6B_t1626282475 * L_1 = V_0;
		Il2CppObject* L_2 = ___observer0;
		NullCheck(L_1);
		L_1->set_observer_0(L_2);
		U3CSubscribeCoreU3Ec__AnonStorey6B_t1626282475 * L_3 = V_0;
		NullCheck(L_3);
		L_3->set_U3CU3Ef__this_1(__this);
		U3CSubscribeCoreU3Ec__AnonStorey6B_t1626282475 * L_4 = V_0;
		U3CSubscribeCoreU3Ec__AnonStorey6B_t1626282475 * L_5 = V_0;
		NullCheck(L_5);
		Il2CppObject* L_6 = (Il2CppObject*)L_5->get_observer_0();
		Il2CppObject * L_7 = ___cancel1;
		Throw_t4151912527 * L_8 = (Throw_t4151912527 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		((  void (*) (Throw_t4151912527 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->method)(L_8, (Il2CppObject*)L_6, (Il2CppObject *)L_7, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		NullCheck(L_4);
		L_4->set_observer_0(L_8);
		Il2CppObject * L_9 = (Il2CppObject *)__this->get_scheduler_2();
		IL2CPP_RUNTIME_CLASS_INIT(Scheduler_t103934925_il2cpp_TypeInfo_var);
		Il2CppObject * L_10 = ((Scheduler_t103934925_StaticFields*)Scheduler_t103934925_il2cpp_TypeInfo_var->static_fields)->get_Immediate_1();
		if ((!(((Il2CppObject*)(Il2CppObject *)L_9) == ((Il2CppObject*)(Il2CppObject *)L_10))))
		{
			goto IL_004d;
		}
	}
	{
		U3CSubscribeCoreU3Ec__AnonStorey6B_t1626282475 * L_11 = V_0;
		NullCheck(L_11);
		Il2CppObject* L_12 = (Il2CppObject*)L_11->get_observer_0();
		Exception_t1967233988 * L_13 = (Exception_t1967233988 *)__this->get_error_1();
		NullCheck((Il2CppObject*)L_12);
		InterfaceActionInvoker1< Exception_t1967233988 * >::Invoke(1 /* System.Void UniRx.IObserver`1<System.Object>::OnError(System.Exception) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6), (Il2CppObject*)L_12, (Exception_t1967233988 *)L_13);
		IL2CPP_RUNTIME_CLASS_INIT(Disposable_t3736388786_il2cpp_TypeInfo_var);
		Il2CppObject * L_14 = ((Disposable_t3736388786_StaticFields*)Disposable_t3736388786_il2cpp_TypeInfo_var->static_fields)->get_Empty_0();
		return L_14;
	}

IL_004d:
	{
		Il2CppObject * L_15 = (Il2CppObject *)__this->get_scheduler_2();
		U3CSubscribeCoreU3Ec__AnonStorey6B_t1626282475 * L_16 = V_0;
		IntPtr_t L_17;
		L_17.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7));
		Action_t437523947 * L_18 = (Action_t437523947 *)il2cpp_codegen_object_new(Action_t437523947_il2cpp_TypeInfo_var);
		Action__ctor_m2957240604(L_18, (Il2CppObject *)L_16, (IntPtr_t)L_17, /*hidden argument*/NULL);
		NullCheck((Il2CppObject *)L_15);
		Il2CppObject * L_19 = InterfaceFuncInvoker1< Il2CppObject *, Action_t437523947 * >::Invoke(1 /* System.IDisposable UniRx.IScheduler::Schedule(System.Action) */, IScheduler_t2938318244_il2cpp_TypeInfo_var, (Il2CppObject *)L_15, (Action_t437523947 *)L_18);
		return L_19;
	}
}
// System.Void UniRx.Operators.ThrowObservable`1<UniRx.Unit>::.ctor(System.Exception,UniRx.IScheduler)
extern Il2CppClass* Scheduler_t103934925_il2cpp_TypeInfo_var;
extern const uint32_t ThrowObservable_1__ctor_m155364606_MetadataUsageId;
extern "C"  void ThrowObservable_1__ctor_m155364606_gshared (ThrowObservable_1_t1098488778 * __this, Exception_t1967233988 * ___error0, Il2CppObject * ___scheduler1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ThrowObservable_1__ctor_m155364606_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject * L_0 = ___scheduler1;
		IL2CPP_RUNTIME_CLASS_INIT(Scheduler_t103934925_il2cpp_TypeInfo_var);
		Il2CppObject * L_1 = ((Scheduler_t103934925_StaticFields*)Scheduler_t103934925_il2cpp_TypeInfo_var->static_fields)->get_CurrentThread_0();
		NullCheck((OperatorObservableBase_1_t1622431009 *)__this);
		((  void (*) (OperatorObservableBase_1_t1622431009 *, bool, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((OperatorObservableBase_1_t1622431009 *)__this, (bool)((((Il2CppObject*)(Il2CppObject *)L_0) == ((Il2CppObject*)(Il2CppObject *)L_1))? 1 : 0), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		Exception_t1967233988 * L_2 = ___error0;
		__this->set_error_1(L_2);
		Il2CppObject * L_3 = ___scheduler1;
		__this->set_scheduler_2(L_3);
		return;
	}
}
// System.IDisposable UniRx.Operators.ThrowObservable`1<UniRx.Unit>::SubscribeCore(UniRx.IObserver`1<T>,System.IDisposable)
extern Il2CppClass* Scheduler_t103934925_il2cpp_TypeInfo_var;
extern Il2CppClass* Disposable_t3736388786_il2cpp_TypeInfo_var;
extern Il2CppClass* Action_t437523947_il2cpp_TypeInfo_var;
extern Il2CppClass* IScheduler_t2938318244_il2cpp_TypeInfo_var;
extern const uint32_t ThrowObservable_1_SubscribeCore_m3461710106_MetadataUsageId;
extern "C"  Il2CppObject * ThrowObservable_1_SubscribeCore_m3461710106_gshared (ThrowObservable_1_t1098488778 * __this, Il2CppObject* ___observer0, Il2CppObject * ___cancel1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ThrowObservable_1_SubscribeCore_m3461710106_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	U3CSubscribeCoreU3Ec__AnonStorey6B_t3347462093 * V_0 = NULL;
	{
		U3CSubscribeCoreU3Ec__AnonStorey6B_t3347462093 * L_0 = (U3CSubscribeCoreU3Ec__AnonStorey6B_t3347462093 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		((  void (*) (U3CSubscribeCoreU3Ec__AnonStorey6B_t3347462093 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		V_0 = (U3CSubscribeCoreU3Ec__AnonStorey6B_t3347462093 *)L_0;
		U3CSubscribeCoreU3Ec__AnonStorey6B_t3347462093 * L_1 = V_0;
		Il2CppObject* L_2 = ___observer0;
		NullCheck(L_1);
		L_1->set_observer_0(L_2);
		U3CSubscribeCoreU3Ec__AnonStorey6B_t3347462093 * L_3 = V_0;
		NullCheck(L_3);
		L_3->set_U3CU3Ef__this_1(__this);
		U3CSubscribeCoreU3Ec__AnonStorey6B_t3347462093 * L_4 = V_0;
		U3CSubscribeCoreU3Ec__AnonStorey6B_t3347462093 * L_5 = V_0;
		NullCheck(L_5);
		Il2CppObject* L_6 = (Il2CppObject*)L_5->get_observer_0();
		Il2CppObject * L_7 = ___cancel1;
		Throw_t1578124849 * L_8 = (Throw_t1578124849 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		((  void (*) (Throw_t1578124849 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->method)(L_8, (Il2CppObject*)L_6, (Il2CppObject *)L_7, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		NullCheck(L_4);
		L_4->set_observer_0(L_8);
		Il2CppObject * L_9 = (Il2CppObject *)__this->get_scheduler_2();
		IL2CPP_RUNTIME_CLASS_INIT(Scheduler_t103934925_il2cpp_TypeInfo_var);
		Il2CppObject * L_10 = ((Scheduler_t103934925_StaticFields*)Scheduler_t103934925_il2cpp_TypeInfo_var->static_fields)->get_Immediate_1();
		if ((!(((Il2CppObject*)(Il2CppObject *)L_9) == ((Il2CppObject*)(Il2CppObject *)L_10))))
		{
			goto IL_004d;
		}
	}
	{
		U3CSubscribeCoreU3Ec__AnonStorey6B_t3347462093 * L_11 = V_0;
		NullCheck(L_11);
		Il2CppObject* L_12 = (Il2CppObject*)L_11->get_observer_0();
		Exception_t1967233988 * L_13 = (Exception_t1967233988 *)__this->get_error_1();
		NullCheck((Il2CppObject*)L_12);
		InterfaceActionInvoker1< Exception_t1967233988 * >::Invoke(1 /* System.Void UniRx.IObserver`1<UniRx.Unit>::OnError(System.Exception) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6), (Il2CppObject*)L_12, (Exception_t1967233988 *)L_13);
		IL2CPP_RUNTIME_CLASS_INIT(Disposable_t3736388786_il2cpp_TypeInfo_var);
		Il2CppObject * L_14 = ((Disposable_t3736388786_StaticFields*)Disposable_t3736388786_il2cpp_TypeInfo_var->static_fields)->get_Empty_0();
		return L_14;
	}

IL_004d:
	{
		Il2CppObject * L_15 = (Il2CppObject *)__this->get_scheduler_2();
		U3CSubscribeCoreU3Ec__AnonStorey6B_t3347462093 * L_16 = V_0;
		IntPtr_t L_17;
		L_17.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7));
		Action_t437523947 * L_18 = (Action_t437523947 *)il2cpp_codegen_object_new(Action_t437523947_il2cpp_TypeInfo_var);
		Action__ctor_m2957240604(L_18, (Il2CppObject *)L_16, (IntPtr_t)L_17, /*hidden argument*/NULL);
		NullCheck((Il2CppObject *)L_15);
		Il2CppObject * L_19 = InterfaceFuncInvoker1< Il2CppObject *, Action_t437523947 * >::Invoke(1 /* System.IDisposable UniRx.IScheduler::Schedule(System.Action) */, IScheduler_t2938318244_il2cpp_TypeInfo_var, (Il2CppObject *)L_15, (Action_t437523947 *)L_18);
		return L_19;
	}
}
// System.Void UniRx.Operators.TimeIntervalObservable`1/TimeInterval<System.Object>::.ctor(UniRx.Operators.TimeIntervalObservable`1<T>,UniRx.IObserver`1<UniRx.TimeInterval`1<T>>,System.IDisposable)
extern Il2CppClass* IScheduler_t2938318244_il2cpp_TypeInfo_var;
extern const uint32_t TimeInterval__ctor_m2128041023_MetadataUsageId;
extern "C"  void TimeInterval__ctor_m2128041023_gshared (TimeInterval_t3041115259 * __this, TimeIntervalObservable_1_t4159975444 * ___parent0, Il2CppObject* ___observer1, Il2CppObject * ___cancel2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TimeInterval__ctor_m2128041023_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject* L_0 = ___observer1;
		Il2CppObject * L_1 = ___cancel2;
		NullCheck((OperatorObserverBase_2_t1058727271 *)__this);
		((  void (*) (OperatorObserverBase_2_t1058727271 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((OperatorObserverBase_2_t1058727271 *)__this, (Il2CppObject*)L_0, (Il2CppObject *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		TimeIntervalObservable_1_t4159975444 * L_2 = ___parent0;
		__this->set_parent_2(L_2);
		TimeIntervalObservable_1_t4159975444 * L_3 = ___parent0;
		NullCheck(L_3);
		Il2CppObject * L_4 = (Il2CppObject *)L_3->get_scheduler_2();
		NullCheck((Il2CppObject *)L_4);
		DateTimeOffset_t3712260035  L_5 = InterfaceFuncInvoker0< DateTimeOffset_t3712260035  >::Invoke(0 /* System.DateTimeOffset UniRx.IScheduler::get_Now() */, IScheduler_t2938318244_il2cpp_TypeInfo_var, (Il2CppObject *)L_4);
		__this->set_lastTime_3(L_5);
		return;
	}
}
// System.Void UniRx.Operators.TimeIntervalObservable`1/TimeInterval<System.Object>::OnNext(T)
extern Il2CppClass* IScheduler_t2938318244_il2cpp_TypeInfo_var;
extern const uint32_t TimeInterval_OnNext_m4003686687_MetadataUsageId;
extern "C"  void TimeInterval_OnNext_m4003686687_gshared (TimeInterval_t3041115259 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TimeInterval_OnNext_m4003686687_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	DateTimeOffset_t3712260035  V_0;
	memset(&V_0, 0, sizeof(V_0));
	TimeSpan_t763862892  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		TimeIntervalObservable_1_t4159975444 * L_0 = (TimeIntervalObservable_1_t4159975444 *)__this->get_parent_2();
		NullCheck(L_0);
		Il2CppObject * L_1 = (Il2CppObject *)L_0->get_scheduler_2();
		NullCheck((Il2CppObject *)L_1);
		DateTimeOffset_t3712260035  L_2 = InterfaceFuncInvoker0< DateTimeOffset_t3712260035  >::Invoke(0 /* System.DateTimeOffset UniRx.IScheduler::get_Now() */, IScheduler_t2938318244_il2cpp_TypeInfo_var, (Il2CppObject *)L_1);
		V_0 = (DateTimeOffset_t3712260035 )L_2;
		DateTimeOffset_t3712260035  L_3 = (DateTimeOffset_t3712260035 )__this->get_lastTime_3();
		TimeSpan_t763862892  L_4 = DateTimeOffset_Subtract_m2754043121((DateTimeOffset_t3712260035 *)(&V_0), (DateTimeOffset_t3712260035 )L_3, /*hidden argument*/NULL);
		V_1 = (TimeSpan_t763862892 )L_4;
		DateTimeOffset_t3712260035  L_5 = V_0;
		__this->set_lastTime_3(L_5);
		Il2CppObject* L_6 = (Il2CppObject*)((OperatorObserverBase_2_t1058727271 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		Il2CppObject * L_7 = ___value0;
		TimeSpan_t763862892  L_8 = V_1;
		TimeInterval_1_t708065542  L_9;
		memset(&L_9, 0, sizeof(L_9));
		((  void (*) (TimeInterval_1_t708065542 *, Il2CppObject *, TimeSpan_t763862892 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)(&L_9, (Il2CppObject *)L_7, (TimeSpan_t763862892 )L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		NullCheck((Il2CppObject*)L_6);
		InterfaceActionInvoker1< TimeInterval_1_t708065542  >::Invoke(2 /* System.Void UniRx.IObserver`1<UniRx.TimeInterval`1<System.Object>>::OnNext(T) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4), (Il2CppObject*)L_6, (TimeInterval_1_t708065542 )L_9);
		return;
	}
}
// System.Void UniRx.Operators.TimeIntervalObservable`1/TimeInterval<System.Object>::OnError(System.Exception)
extern "C"  void TimeInterval_OnError_m2760336942_gshared (TimeInterval_t3041115259 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method)
{
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		Il2CppObject* L_0 = (Il2CppObject*)((OperatorObserverBase_2_t1058727271 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		Exception_t1967233988 * L_1 = ___error0;
		NullCheck((Il2CppObject*)L_0);
		InterfaceActionInvoker1< Exception_t1967233988 * >::Invoke(1 /* System.Void UniRx.IObserver`1<UniRx.TimeInterval`1<System.Object>>::OnError(System.Exception) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4), (Il2CppObject*)L_0, (Exception_t1967233988 *)L_1);
		IL2CPP_LEAVE(0x1A, FINALLY_0013);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0013;
	}

FINALLY_0013:
	{ // begin finally (depth: 1)
		NullCheck((OperatorObserverBase_2_t1058727271 *)__this);
		VirtActionInvoker0::Invoke(4 /* System.Void UniRx.Operators.OperatorObserverBase`2<System.Object,UniRx.TimeInterval`1<System.Object>>::Dispose() */, (OperatorObserverBase_2_t1058727271 *)__this);
		IL2CPP_END_FINALLY(19)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(19)
	{
		IL2CPP_JUMP_TBL(0x1A, IL_001a)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_001a:
	{
		return;
	}
}
// System.Void UniRx.Operators.TimeIntervalObservable`1/TimeInterval<System.Object>::OnCompleted()
extern "C"  void TimeInterval_OnCompleted_m59469441_gshared (TimeInterval_t3041115259 * __this, const MethodInfo* method)
{
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		Il2CppObject* L_0 = (Il2CppObject*)((OperatorObserverBase_2_t1058727271 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		NullCheck((Il2CppObject*)L_0);
		InterfaceActionInvoker0::Invoke(0 /* System.Void UniRx.IObserver`1<UniRx.TimeInterval`1<System.Object>>::OnCompleted() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4), (Il2CppObject*)L_0);
		IL2CPP_LEAVE(0x19, FINALLY_0012);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0012;
	}

FINALLY_0012:
	{ // begin finally (depth: 1)
		NullCheck((OperatorObserverBase_2_t1058727271 *)__this);
		VirtActionInvoker0::Invoke(4 /* System.Void UniRx.Operators.OperatorObserverBase`2<System.Object,UniRx.TimeInterval`1<System.Object>>::Dispose() */, (OperatorObserverBase_2_t1058727271 *)__this);
		IL2CPP_END_FINALLY(18)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(18)
	{
		IL2CPP_JUMP_TBL(0x19, IL_0019)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0019:
	{
		return;
	}
}
// System.Void UniRx.Operators.TimeIntervalObservable`1<System.Object>::.ctor(UniRx.IObservable`1<T>,UniRx.IScheduler)
extern Il2CppClass* Scheduler_t103934925_il2cpp_TypeInfo_var;
extern const uint32_t TimeIntervalObservable_1__ctor_m898044517_MetadataUsageId;
extern "C"  void TimeIntervalObservable_1__ctor_m898044517_gshared (TimeIntervalObservable_1_t4159975444 * __this, Il2CppObject* ___source0, Il2CppObject * ___scheduler1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TimeIntervalObservable_1__ctor_m898044517_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	TimeIntervalObservable_1_t4159975444 * G_B2_0 = NULL;
	TimeIntervalObservable_1_t4159975444 * G_B1_0 = NULL;
	int32_t G_B3_0 = 0;
	TimeIntervalObservable_1_t4159975444 * G_B3_1 = NULL;
	{
		Il2CppObject * L_0 = ___scheduler1;
		IL2CPP_RUNTIME_CLASS_INIT(Scheduler_t103934925_il2cpp_TypeInfo_var);
		Il2CppObject * L_1 = ((Scheduler_t103934925_StaticFields*)Scheduler_t103934925_il2cpp_TypeInfo_var->static_fields)->get_CurrentThread_0();
		G_B1_0 = ((TimeIntervalObservable_1_t4159975444 *)(__this));
		if ((((Il2CppObject*)(Il2CppObject *)L_0) == ((Il2CppObject*)(Il2CppObject *)L_1)))
		{
			G_B2_0 = ((TimeIntervalObservable_1_t4159975444 *)(__this));
			goto IL_0014;
		}
	}
	{
		Il2CppObject* L_2 = ___source0;
		bool L_3 = ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)(NULL /*static, unused*/, (Il2CppObject*)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		G_B3_0 = ((int32_t)(L_3));
		G_B3_1 = ((TimeIntervalObservable_1_t4159975444 *)(G_B1_0));
		goto IL_0015;
	}

IL_0014:
	{
		G_B3_0 = 1;
		G_B3_1 = ((TimeIntervalObservable_1_t4159975444 *)(G_B2_0));
	}

IL_0015:
	{
		NullCheck((OperatorObservableBase_1_t4067177809 *)G_B3_1);
		((  void (*) (OperatorObservableBase_1_t4067177809 *, bool, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)((OperatorObservableBase_1_t4067177809 *)G_B3_1, (bool)G_B3_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		Il2CppObject* L_4 = ___source0;
		__this->set_source_1(L_4);
		Il2CppObject * L_5 = ___scheduler1;
		__this->set_scheduler_2(L_5);
		return;
	}
}
// System.IDisposable UniRx.Operators.TimeIntervalObservable`1<System.Object>::SubscribeCore(UniRx.IObserver`1<UniRx.TimeInterval`1<T>>,System.IDisposable)
extern "C"  Il2CppObject * TimeIntervalObservable_1_SubscribeCore_m3860261785_gshared (TimeIntervalObservable_1_t4159975444 * __this, Il2CppObject* ___observer0, Il2CppObject * ___cancel1, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = (Il2CppObject*)__this->get_source_1();
		Il2CppObject* L_1 = ___observer0;
		Il2CppObject * L_2 = ___cancel1;
		TimeInterval_t3041115259 * L_3 = (TimeInterval_t3041115259 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		((  void (*) (TimeInterval_t3041115259 *, TimeIntervalObservable_1_t4159975444 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)(L_3, (TimeIntervalObservable_1_t4159975444 *)__this, (Il2CppObject*)L_1, (Il2CppObject *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		NullCheck((Il2CppObject*)L_0);
		Il2CppObject * L_4 = InterfaceFuncInvoker1< Il2CppObject *, Il2CppObject* >::Invoke(0 /* System.IDisposable UniRx.IObservable`1<System.Object>::Subscribe(UniRx.IObserver`1<T>) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5), (Il2CppObject*)L_0, (Il2CppObject*)L_3);
		return L_4;
	}
}
// System.Void UniRx.Operators.TimeoutFrameObservable`1/TimeoutFrame/TimeoutFrameTick<System.Object>::.ctor(UniRx.Operators.TimeoutFrameObservable`1/TimeoutFrame<T>,System.UInt64)
extern "C"  void TimeoutFrameTick__ctor_m2057949826_gshared (TimeoutFrameTick_t1345043194 * __this, TimeoutFrame_t2437429061 * ___parent0, uint64_t ___timerId1, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		TimeoutFrame_t2437429061 * L_0 = ___parent0;
		__this->set_parent_0(L_0);
		uint64_t L_1 = ___timerId1;
		__this->set_timerId_1(L_1);
		return;
	}
}
// System.Void UniRx.Operators.TimeoutFrameObservable`1/TimeoutFrame/TimeoutFrameTick<System.Object>::OnCompleted()
extern "C"  void TimeoutFrameTick_OnCompleted_m872066233_gshared (TimeoutFrameTick_t1345043194 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void UniRx.Operators.TimeoutFrameObservable`1/TimeoutFrame/TimeoutFrameTick<System.Object>::OnError(System.Exception)
extern "C"  void TimeoutFrameTick_OnError_m2394099814_gshared (TimeoutFrameTick_t1345043194 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void UniRx.Operators.TimeoutFrameObservable`1/TimeoutFrame/TimeoutFrameTick<System.Object>::OnNext(System.Int64)
extern Il2CppClass* TimeoutException_t2903576483_il2cpp_TypeInfo_var;
extern const uint32_t TimeoutFrameTick_OnNext_m3132947769_MetadataUsageId;
extern "C"  void TimeoutFrameTick_OnNext_m3132947769_gshared (TimeoutFrameTick_t1345043194 * __this, int64_t ____0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TimeoutFrameTick_OnNext_m3132947769_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Il2CppObject * V_0 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		TimeoutFrame_t2437429061 * L_0 = (TimeoutFrame_t2437429061 *)__this->get_parent_0();
		NullCheck(L_0);
		Il2CppObject * L_1 = (Il2CppObject *)L_0->get_gate_3();
		V_0 = (Il2CppObject *)L_1;
		Il2CppObject * L_2 = V_0;
		Monitor_Enter_m476686225(NULL /*static, unused*/, (Il2CppObject *)L_2, /*hidden argument*/NULL);
	}

IL_0012:
	try
	{ // begin try (depth: 1)
		{
			TimeoutFrame_t2437429061 * L_3 = (TimeoutFrame_t2437429061 *)__this->get_parent_0();
			NullCheck(L_3);
			uint64_t L_4 = (uint64_t)L_3->get_objectId_4();
			uint64_t L_5 = (uint64_t)__this->get_timerId_1();
			if ((!(((uint64_t)L_4) == ((uint64_t)L_5))))
			{
				goto IL_0034;
			}
		}

IL_0028:
		{
			TimeoutFrame_t2437429061 * L_6 = (TimeoutFrame_t2437429061 *)__this->get_parent_0();
			NullCheck(L_6);
			L_6->set_isTimeout_5((bool)1);
		}

IL_0034:
		{
			IL2CPP_LEAVE(0x40, FINALLY_0039);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0039;
	}

FINALLY_0039:
	{ // begin finally (depth: 1)
		Il2CppObject * L_7 = V_0;
		Monitor_Exit_m2088237919(NULL /*static, unused*/, (Il2CppObject *)L_7, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(57)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(57)
	{
		IL2CPP_JUMP_TBL(0x40, IL_0040)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0040:
	{
		TimeoutFrame_t2437429061 * L_8 = (TimeoutFrame_t2437429061 *)__this->get_parent_0();
		NullCheck(L_8);
		bool L_9 = (bool)L_8->get_isTimeout_5();
		if (!L_9)
		{
			goto IL_0078;
		}
	}

IL_0050:
	try
	{ // begin try (depth: 1)
		TimeoutFrame_t2437429061 * L_10 = (TimeoutFrame_t2437429061 *)__this->get_parent_0();
		NullCheck(L_10);
		Il2CppObject* L_11 = (Il2CppObject*)((OperatorObserverBase_2_t1187768149 *)L_10)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		TimeoutException_t2903576483 * L_12 = (TimeoutException_t2903576483 *)il2cpp_codegen_object_new(TimeoutException_t2903576483_il2cpp_TypeInfo_var);
		TimeoutException__ctor_m3882171335(L_12, /*hidden argument*/NULL);
		NullCheck((Il2CppObject*)L_11);
		InterfaceActionInvoker1< Exception_t1967233988 * >::Invoke(1 /* System.Void UniRx.IObserver`1<System.Object>::OnError(System.Exception) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0), (Il2CppObject*)L_11, (Exception_t1967233988 *)L_12);
		IL2CPP_LEAVE(0x78, FINALLY_006c);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_006c;
	}

FINALLY_006c:
	{ // begin finally (depth: 1)
		TimeoutFrame_t2437429061 * L_13 = (TimeoutFrame_t2437429061 *)__this->get_parent_0();
		NullCheck((OperatorObserverBase_2_t1187768149 *)L_13);
		VirtActionInvoker0::Invoke(4 /* System.Void UniRx.Operators.OperatorObserverBase`2<System.Object,System.Object>::Dispose() */, (OperatorObserverBase_2_t1187768149 *)L_13);
		IL2CPP_END_FINALLY(108)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(108)
	{
		IL2CPP_JUMP_TBL(0x78, IL_0078)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0078:
	{
		return;
	}
}
// System.Void UniRx.Operators.TimeoutFrameObservable`1/TimeoutFrame<System.Object>::.ctor(UniRx.Operators.TimeoutFrameObservable`1<T>,UniRx.IObserver`1<T>,System.IDisposable)
extern Il2CppClass* Il2CppObject_il2cpp_TypeInfo_var;
extern const uint32_t TimeoutFrame__ctor_m3553775824_MetadataUsageId;
extern "C"  void TimeoutFrame__ctor_m3553775824_gshared (TimeoutFrame_t2437429061 * __this, TimeoutFrameObservable_1_t2457127902 * ___parent0, Il2CppObject* ___observer1, Il2CppObject * ___cancel2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TimeoutFrame__ctor_m3553775824_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject * L_0 = (Il2CppObject *)il2cpp_codegen_object_new(Il2CppObject_il2cpp_TypeInfo_var);
		Object__ctor_m1772956182(L_0, /*hidden argument*/NULL);
		__this->set_gate_3(L_0);
		Il2CppObject* L_1 = ___observer1;
		Il2CppObject * L_2 = ___cancel2;
		NullCheck((OperatorObserverBase_2_t1187768149 *)__this);
		((  void (*) (OperatorObserverBase_2_t1187768149 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((OperatorObserverBase_2_t1187768149 *)__this, (Il2CppObject*)L_1, (Il2CppObject *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		TimeoutFrameObservable_1_t2457127902 * L_3 = ___parent0;
		__this->set_parent_2(L_3);
		return;
	}
}
// System.IDisposable UniRx.Operators.TimeoutFrameObservable`1/TimeoutFrame<System.Object>::Run()
extern Il2CppClass* SingleAssignmentDisposable_t2336378823_il2cpp_TypeInfo_var;
extern Il2CppClass* SerialDisposable_t2547852742_il2cpp_TypeInfo_var;
extern const uint32_t TimeoutFrame_Run_m3224058181_MetadataUsageId;
extern "C"  Il2CppObject * TimeoutFrame_Run_m3224058181_gshared (TimeoutFrame_t2437429061 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TimeoutFrame_Run_m3224058181_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		SingleAssignmentDisposable_t2336378823 * L_0 = (SingleAssignmentDisposable_t2336378823 *)il2cpp_codegen_object_new(SingleAssignmentDisposable_t2336378823_il2cpp_TypeInfo_var);
		SingleAssignmentDisposable__ctor_m917449122(L_0, /*hidden argument*/NULL);
		__this->set_sourceSubscription_6(L_0);
		SerialDisposable_t2547852742 * L_1 = (SerialDisposable_t2547852742 *)il2cpp_codegen_object_new(SerialDisposable_t2547852742_il2cpp_TypeInfo_var);
		SerialDisposable__ctor_m2977852291(L_1, /*hidden argument*/NULL);
		__this->set_timerSubscription_7(L_1);
		SerialDisposable_t2547852742 * L_2 = (SerialDisposable_t2547852742 *)__this->get_timerSubscription_7();
		uint64_t L_3 = (uint64_t)__this->get_objectId_4();
		NullCheck((TimeoutFrame_t2437429061 *)__this);
		Il2CppObject * L_4 = ((  Il2CppObject * (*) (TimeoutFrame_t2437429061 *, uint64_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)((TimeoutFrame_t2437429061 *)__this, (uint64_t)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		NullCheck((SerialDisposable_t2547852742 *)L_2);
		SerialDisposable_set_Disposable_m1438046228((SerialDisposable_t2547852742 *)L_2, (Il2CppObject *)L_4, /*hidden argument*/NULL);
		SingleAssignmentDisposable_t2336378823 * L_5 = (SingleAssignmentDisposable_t2336378823 *)__this->get_sourceSubscription_6();
		TimeoutFrameObservable_1_t2457127902 * L_6 = (TimeoutFrameObservable_1_t2457127902 *)__this->get_parent_2();
		NullCheck(L_6);
		Il2CppObject* L_7 = (Il2CppObject*)L_6->get_source_1();
		NullCheck((Il2CppObject*)L_7);
		Il2CppObject * L_8 = InterfaceFuncInvoker1< Il2CppObject *, Il2CppObject* >::Invoke(0 /* System.IDisposable UniRx.IObservable`1<System.Object>::Subscribe(UniRx.IObserver`1<T>) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3), (Il2CppObject*)L_7, (Il2CppObject*)__this);
		NullCheck((SingleAssignmentDisposable_t2336378823 *)L_5);
		SingleAssignmentDisposable_set_Disposable_m1599028885((SingleAssignmentDisposable_t2336378823 *)L_5, (Il2CppObject *)L_8, /*hidden argument*/NULL);
		SerialDisposable_t2547852742 * L_9 = (SerialDisposable_t2547852742 *)__this->get_timerSubscription_7();
		SingleAssignmentDisposable_t2336378823 * L_10 = (SingleAssignmentDisposable_t2336378823 *)__this->get_sourceSubscription_6();
		Il2CppObject * L_11 = StableCompositeDisposable_Create_m3764788535(NULL /*static, unused*/, (Il2CppObject *)L_9, (Il2CppObject *)L_10, /*hidden argument*/NULL);
		return L_11;
	}
}
// System.IDisposable UniRx.Operators.TimeoutFrameObservable`1/TimeoutFrame<System.Object>::RunTimer(System.UInt64)
extern Il2CppClass* Observable_t258071701_il2cpp_TypeInfo_var;
extern Il2CppClass* IObservable_1_t2606213246_il2cpp_TypeInfo_var;
extern const uint32_t TimeoutFrame_RunTimer_m3926042265_MetadataUsageId;
extern "C"  Il2CppObject * TimeoutFrame_RunTimer_m3926042265_gshared (TimeoutFrame_t2437429061 * __this, uint64_t ___timerId0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TimeoutFrame_RunTimer_m3926042265_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		TimeoutFrameObservable_1_t2457127902 * L_0 = (TimeoutFrameObservable_1_t2457127902 *)__this->get_parent_2();
		NullCheck(L_0);
		int32_t L_1 = (int32_t)L_0->get_frameCount_2();
		TimeoutFrameObservable_1_t2457127902 * L_2 = (TimeoutFrameObservable_1_t2457127902 *)__this->get_parent_2();
		NullCheck(L_2);
		int32_t L_3 = (int32_t)L_2->get_frameCountType_3();
		IL2CPP_RUNTIME_CLASS_INIT(Observable_t258071701_il2cpp_TypeInfo_var);
		Il2CppObject* L_4 = Observable_TimerFrame_m594366159(NULL /*static, unused*/, (int32_t)L_1, (int32_t)L_3, /*hidden argument*/NULL);
		uint64_t L_5 = ___timerId0;
		TimeoutFrameTick_t1345043194 * L_6 = (TimeoutFrameTick_t1345043194 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		((  void (*) (TimeoutFrameTick_t1345043194 *, TimeoutFrame_t2437429061 *, uint64_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->method)(L_6, (TimeoutFrame_t2437429061 *)__this, (uint64_t)L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		NullCheck((Il2CppObject*)L_4);
		Il2CppObject * L_7 = InterfaceFuncInvoker1< Il2CppObject *, Il2CppObject* >::Invoke(0 /* System.IDisposable UniRx.IObservable`1<System.Int64>::Subscribe(UniRx.IObserver`1<T>) */, IObservable_1_t2606213246_il2cpp_TypeInfo_var, (Il2CppObject*)L_4, (Il2CppObject*)L_6);
		return L_7;
	}
}
// System.Void UniRx.Operators.TimeoutFrameObservable`1/TimeoutFrame<System.Object>::OnNext(T)
extern Il2CppClass* Disposable_t3736388786_il2cpp_TypeInfo_var;
extern const uint32_t TimeoutFrame_OnNext_m2827895391_MetadataUsageId;
extern "C"  void TimeoutFrame_OnNext_m2827895391_gshared (TimeoutFrame_t2437429061 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TimeoutFrame_OnNext_m2827895391_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	uint64_t V_0 = 0;
	bool V_1 = false;
	Il2CppObject * V_2 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_gate_3();
		V_2 = (Il2CppObject *)L_0;
		Il2CppObject * L_1 = V_2;
		Monitor_Enter_m476686225(NULL /*static, unused*/, (Il2CppObject *)L_1, /*hidden argument*/NULL);
	}

IL_000d:
	try
	{ // begin try (depth: 1)
		bool L_2 = (bool)__this->get_isTimeout_5();
		V_1 = (bool)L_2;
		uint64_t L_3 = (uint64_t)__this->get_objectId_4();
		__this->set_objectId_4(((int64_t)((int64_t)L_3+(int64_t)(((int64_t)((int64_t)1))))));
		uint64_t L_4 = (uint64_t)__this->get_objectId_4();
		V_0 = (uint64_t)L_4;
		IL2CPP_LEAVE(0x36, FINALLY_002f);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_002f;
	}

FINALLY_002f:
	{ // begin finally (depth: 1)
		Il2CppObject * L_5 = V_2;
		Monitor_Exit_m2088237919(NULL /*static, unused*/, (Il2CppObject *)L_5, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(47)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(47)
	{
		IL2CPP_JUMP_TBL(0x36, IL_0036)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0036:
	{
		bool L_6 = V_1;
		if (!L_6)
		{
			goto IL_003d;
		}
	}
	{
		return;
	}

IL_003d:
	{
		SerialDisposable_t2547852742 * L_7 = (SerialDisposable_t2547852742 *)__this->get_timerSubscription_7();
		IL2CPP_RUNTIME_CLASS_INIT(Disposable_t3736388786_il2cpp_TypeInfo_var);
		Il2CppObject * L_8 = ((Disposable_t3736388786_StaticFields*)Disposable_t3736388786_il2cpp_TypeInfo_var->static_fields)->get_Empty_0();
		NullCheck((SerialDisposable_t2547852742 *)L_7);
		SerialDisposable_set_Disposable_m1438046228((SerialDisposable_t2547852742 *)L_7, (Il2CppObject *)L_8, /*hidden argument*/NULL);
		Il2CppObject* L_9 = (Il2CppObject*)((OperatorObserverBase_2_t1187768149 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		Il2CppObject * L_10 = ___value0;
		NullCheck((Il2CppObject*)L_9);
		InterfaceActionInvoker1< Il2CppObject * >::Invoke(2 /* System.Void UniRx.IObserver`1<System.Object>::OnNext(T) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6), (Il2CppObject*)L_9, (Il2CppObject *)L_10);
		SerialDisposable_t2547852742 * L_11 = (SerialDisposable_t2547852742 *)__this->get_timerSubscription_7();
		uint64_t L_12 = V_0;
		NullCheck((TimeoutFrame_t2437429061 *)__this);
		Il2CppObject * L_13 = ((  Il2CppObject * (*) (TimeoutFrame_t2437429061 *, uint64_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)((TimeoutFrame_t2437429061 *)__this, (uint64_t)L_12, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		NullCheck((SerialDisposable_t2547852742 *)L_11);
		SerialDisposable_set_Disposable_m1438046228((SerialDisposable_t2547852742 *)L_11, (Il2CppObject *)L_13, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UniRx.Operators.TimeoutFrameObservable`1/TimeoutFrame<System.Object>::OnError(System.Exception)
extern "C"  void TimeoutFrame_OnError_m1914158958_gshared (TimeoutFrame_t2437429061 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method)
{
	bool V_0 = false;
	Il2CppObject * V_1 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_gate_3();
		V_1 = (Il2CppObject *)L_0;
		Il2CppObject * L_1 = V_1;
		Monitor_Enter_m476686225(NULL /*static, unused*/, (Il2CppObject *)L_1, /*hidden argument*/NULL);
	}

IL_000d:
	try
	{ // begin try (depth: 1)
		bool L_2 = (bool)__this->get_isTimeout_5();
		V_0 = (bool)L_2;
		uint64_t L_3 = (uint64_t)__this->get_objectId_4();
		__this->set_objectId_4(((int64_t)((int64_t)L_3+(int64_t)(((int64_t)((int64_t)1))))));
		IL2CPP_LEAVE(0x2F, FINALLY_0028);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0028;
	}

FINALLY_0028:
	{ // begin finally (depth: 1)
		Il2CppObject * L_4 = V_1;
		Monitor_Exit_m2088237919(NULL /*static, unused*/, (Il2CppObject *)L_4, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(40)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(40)
	{
		IL2CPP_JUMP_TBL(0x2F, IL_002f)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_002f:
	{
		bool L_5 = V_0;
		if (!L_5)
		{
			goto IL_0036;
		}
	}
	{
		return;
	}

IL_0036:
	{
		SerialDisposable_t2547852742 * L_6 = (SerialDisposable_t2547852742 *)__this->get_timerSubscription_7();
		NullCheck((SerialDisposable_t2547852742 *)L_6);
		VirtActionInvoker0::Invoke(4 /* System.Void UniRx.SerialDisposable::Dispose() */, (SerialDisposable_t2547852742 *)L_6);
	}

IL_0041:
	try
	{ // begin try (depth: 1)
		Il2CppObject* L_7 = (Il2CppObject*)((OperatorObserverBase_2_t1187768149 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		Exception_t1967233988 * L_8 = ___error0;
		NullCheck((Il2CppObject*)L_7);
		InterfaceActionInvoker1< Exception_t1967233988 * >::Invoke(1 /* System.Void UniRx.IObserver`1<System.Object>::OnError(System.Exception) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6), (Il2CppObject*)L_7, (Exception_t1967233988 *)L_8);
		IL2CPP_LEAVE(0x5B, FINALLY_0054);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0054;
	}

FINALLY_0054:
	{ // begin finally (depth: 1)
		NullCheck((OperatorObserverBase_2_t1187768149 *)__this);
		VirtActionInvoker0::Invoke(4 /* System.Void UniRx.Operators.OperatorObserverBase`2<System.Object,System.Object>::Dispose() */, (OperatorObserverBase_2_t1187768149 *)__this);
		IL2CPP_END_FINALLY(84)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(84)
	{
		IL2CPP_JUMP_TBL(0x5B, IL_005b)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_005b:
	{
		return;
	}
}
// System.Void UniRx.Operators.TimeoutFrameObservable`1/TimeoutFrame<System.Object>::OnCompleted()
extern "C"  void TimeoutFrame_OnCompleted_m2917640129_gshared (TimeoutFrame_t2437429061 * __this, const MethodInfo* method)
{
	bool V_0 = false;
	Il2CppObject * V_1 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_gate_3();
		V_1 = (Il2CppObject *)L_0;
		Il2CppObject * L_1 = V_1;
		Monitor_Enter_m476686225(NULL /*static, unused*/, (Il2CppObject *)L_1, /*hidden argument*/NULL);
	}

IL_000d:
	try
	{ // begin try (depth: 1)
		bool L_2 = (bool)__this->get_isTimeout_5();
		V_0 = (bool)L_2;
		uint64_t L_3 = (uint64_t)__this->get_objectId_4();
		__this->set_objectId_4(((int64_t)((int64_t)L_3+(int64_t)(((int64_t)((int64_t)1))))));
		IL2CPP_LEAVE(0x2F, FINALLY_0028);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0028;
	}

FINALLY_0028:
	{ // begin finally (depth: 1)
		Il2CppObject * L_4 = V_1;
		Monitor_Exit_m2088237919(NULL /*static, unused*/, (Il2CppObject *)L_4, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(40)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(40)
	{
		IL2CPP_JUMP_TBL(0x2F, IL_002f)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_002f:
	{
		bool L_5 = V_0;
		if (!L_5)
		{
			goto IL_0036;
		}
	}
	{
		return;
	}

IL_0036:
	{
		SerialDisposable_t2547852742 * L_6 = (SerialDisposable_t2547852742 *)__this->get_timerSubscription_7();
		NullCheck((SerialDisposable_t2547852742 *)L_6);
		VirtActionInvoker0::Invoke(4 /* System.Void UniRx.SerialDisposable::Dispose() */, (SerialDisposable_t2547852742 *)L_6);
	}

IL_0041:
	try
	{ // begin try (depth: 1)
		Il2CppObject* L_7 = (Il2CppObject*)((OperatorObserverBase_2_t1187768149 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		NullCheck((Il2CppObject*)L_7);
		InterfaceActionInvoker0::Invoke(0 /* System.Void UniRx.IObserver`1<System.Object>::OnCompleted() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6), (Il2CppObject*)L_7);
		IL2CPP_LEAVE(0x5A, FINALLY_0053);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0053;
	}

FINALLY_0053:
	{ // begin finally (depth: 1)
		NullCheck((OperatorObserverBase_2_t1187768149 *)__this);
		VirtActionInvoker0::Invoke(4 /* System.Void UniRx.Operators.OperatorObserverBase`2<System.Object,System.Object>::Dispose() */, (OperatorObserverBase_2_t1187768149 *)__this);
		IL2CPP_END_FINALLY(83)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(83)
	{
		IL2CPP_JUMP_TBL(0x5A, IL_005a)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_005a:
	{
		return;
	}
}
// System.Void UniRx.Operators.TimeoutFrameObservable`1<System.Object>::.ctor(UniRx.IObservable`1<T>,System.Int32,UniRx.FrameCountType)
extern "C"  void TimeoutFrameObservable_1__ctor_m705515884_gshared (TimeoutFrameObservable_1_t2457127902 * __this, Il2CppObject* ___source0, int32_t ___frameCount1, int32_t ___frameCountType2, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = ___source0;
		bool L_1 = ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)(NULL /*static, unused*/, (Il2CppObject*)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		NullCheck((OperatorObservableBase_1_t4196218687 *)__this);
		((  void (*) (OperatorObservableBase_1_t4196218687 *, bool, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)((OperatorObservableBase_1_t4196218687 *)__this, (bool)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		Il2CppObject* L_2 = ___source0;
		__this->set_source_1(L_2);
		int32_t L_3 = ___frameCount1;
		__this->set_frameCount_2(L_3);
		int32_t L_4 = ___frameCountType2;
		__this->set_frameCountType_3(L_4);
		return;
	}
}
// System.IDisposable UniRx.Operators.TimeoutFrameObservable`1<System.Object>::SubscribeCore(UniRx.IObserver`1<T>,System.IDisposable)
extern "C"  Il2CppObject * TimeoutFrameObservable_1_SubscribeCore_m925296886_gshared (TimeoutFrameObservable_1_t2457127902 * __this, Il2CppObject* ___observer0, Il2CppObject * ___cancel1, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = ___observer0;
		Il2CppObject * L_1 = ___cancel1;
		TimeoutFrame_t2437429061 * L_2 = (TimeoutFrame_t2437429061 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		((  void (*) (TimeoutFrame_t2437429061 *, TimeoutFrameObservable_1_t2457127902 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)(L_2, (TimeoutFrameObservable_1_t2457127902 *)__this, (Il2CppObject*)L_0, (Il2CppObject *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		NullCheck((TimeoutFrame_t2437429061 *)L_2);
		Il2CppObject * L_3 = ((  Il2CppObject * (*) (TimeoutFrame_t2437429061 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->method)((TimeoutFrame_t2437429061 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		return L_3;
	}
}
// System.Void UniRx.Operators.TimeoutObservable`1/Timeout/<RunTimer>c__AnonStorey6C<System.Object>::.ctor()
extern "C"  void U3CRunTimerU3Ec__AnonStorey6C__ctor_m576555264_gshared (U3CRunTimerU3Ec__AnonStorey6C_t4277735173 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UniRx.Operators.TimeoutObservable`1/Timeout/<RunTimer>c__AnonStorey6C<System.Object>::<>m__8A()
extern Il2CppClass* TimeoutException_t2903576483_il2cpp_TypeInfo_var;
extern const uint32_t U3CRunTimerU3Ec__AnonStorey6C_U3CU3Em__8A_m2327092370_MetadataUsageId;
extern "C"  void U3CRunTimerU3Ec__AnonStorey6C_U3CU3Em__8A_m2327092370_gshared (U3CRunTimerU3Ec__AnonStorey6C_t4277735173 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CRunTimerU3Ec__AnonStorey6C_U3CU3Em__8A_m2327092370_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Il2CppObject * V_0 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Timeout_t2052083218 * L_0 = (Timeout_t2052083218 *)__this->get_U3CU3Ef__this_1();
		NullCheck(L_0);
		Il2CppObject * L_1 = (Il2CppObject *)L_0->get_gate_3();
		V_0 = (Il2CppObject *)L_1;
		Il2CppObject * L_2 = V_0;
		Monitor_Enter_m476686225(NULL /*static, unused*/, (Il2CppObject *)L_2, /*hidden argument*/NULL);
	}

IL_0012:
	try
	{ // begin try (depth: 1)
		{
			Timeout_t2052083218 * L_3 = (Timeout_t2052083218 *)__this->get_U3CU3Ef__this_1();
			NullCheck(L_3);
			uint64_t L_4 = (uint64_t)L_3->get_objectId_4();
			uint64_t L_5 = (uint64_t)__this->get_timerId_0();
			if ((!(((uint64_t)L_4) == ((uint64_t)L_5))))
			{
				goto IL_0034;
			}
		}

IL_0028:
		{
			Timeout_t2052083218 * L_6 = (Timeout_t2052083218 *)__this->get_U3CU3Ef__this_1();
			NullCheck(L_6);
			L_6->set_isTimeout_5((bool)1);
		}

IL_0034:
		{
			IL2CPP_LEAVE(0x40, FINALLY_0039);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0039;
	}

FINALLY_0039:
	{ // begin finally (depth: 1)
		Il2CppObject * L_7 = V_0;
		Monitor_Exit_m2088237919(NULL /*static, unused*/, (Il2CppObject *)L_7, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(57)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(57)
	{
		IL2CPP_JUMP_TBL(0x40, IL_0040)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0040:
	{
		Timeout_t2052083218 * L_8 = (Timeout_t2052083218 *)__this->get_U3CU3Ef__this_1();
		NullCheck(L_8);
		bool L_9 = (bool)L_8->get_isTimeout_5();
		if (!L_9)
		{
			goto IL_0078;
		}
	}

IL_0050:
	try
	{ // begin try (depth: 1)
		Timeout_t2052083218 * L_10 = (Timeout_t2052083218 *)__this->get_U3CU3Ef__this_1();
		NullCheck(L_10);
		Il2CppObject* L_11 = (Il2CppObject*)((OperatorObserverBase_2_t1187768149 *)L_10)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		TimeoutException_t2903576483 * L_12 = (TimeoutException_t2903576483 *)il2cpp_codegen_object_new(TimeoutException_t2903576483_il2cpp_TypeInfo_var);
		TimeoutException__ctor_m3882171335(L_12, /*hidden argument*/NULL);
		NullCheck((Il2CppObject*)L_11);
		InterfaceActionInvoker1< Exception_t1967233988 * >::Invoke(1 /* System.Void UniRx.IObserver`1<System.Object>::OnError(System.Exception) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0), (Il2CppObject*)L_11, (Exception_t1967233988 *)L_12);
		IL2CPP_LEAVE(0x78, FINALLY_006c);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_006c;
	}

FINALLY_006c:
	{ // begin finally (depth: 1)
		Timeout_t2052083218 * L_13 = (Timeout_t2052083218 *)__this->get_U3CU3Ef__this_1();
		NullCheck((OperatorObserverBase_2_t1187768149 *)L_13);
		VirtActionInvoker0::Invoke(4 /* System.Void UniRx.Operators.OperatorObserverBase`2<System.Object,System.Object>::Dispose() */, (OperatorObserverBase_2_t1187768149 *)L_13);
		IL2CPP_END_FINALLY(108)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(108)
	{
		IL2CPP_JUMP_TBL(0x78, IL_0078)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0078:
	{
		return;
	}
}
// System.Void UniRx.Operators.TimeoutObservable`1/Timeout_<System.Object>::.ctor(UniRx.Operators.TimeoutObservable`1<T>,UniRx.IObserver`1<T>,System.IDisposable)
extern Il2CppClass* Il2CppObject_il2cpp_TypeInfo_var;
extern const uint32_t Timeout___ctor_m2997021766_MetadataUsageId;
extern "C"  void Timeout___ctor_m2997021766_gshared (Timeout__t2369373703 * __this, TimeoutObservable_1_t1895307051 * ___parent0, Il2CppObject* ___observer1, Il2CppObject * ___cancel2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Timeout___ctor_m2997021766_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject * L_0 = (Il2CppObject *)il2cpp_codegen_object_new(Il2CppObject_il2cpp_TypeInfo_var);
		Object__ctor_m1772956182(L_0, /*hidden argument*/NULL);
		__this->set_gate_3(L_0);
		Il2CppObject* L_1 = ___observer1;
		Il2CppObject * L_2 = ___cancel2;
		NullCheck((OperatorObserverBase_2_t1187768149 *)__this);
		((  void (*) (OperatorObserverBase_2_t1187768149 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((OperatorObserverBase_2_t1187768149 *)__this, (Il2CppObject*)L_1, (Il2CppObject *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		TimeoutObservable_1_t1895307051 * L_3 = ___parent0;
		__this->set_parent_2(L_3);
		return;
	}
}
// System.IDisposable UniRx.Operators.TimeoutObservable`1/Timeout_<System.Object>::Run()
extern Il2CppClass* SingleAssignmentDisposable_t2336378823_il2cpp_TypeInfo_var;
extern Il2CppClass* Action_t437523947_il2cpp_TypeInfo_var;
extern Il2CppClass* Scheduler_t103934925_il2cpp_TypeInfo_var;
extern const MethodInfo* Nullable_1_get_Value_m4125167011_MethodInfo_var;
extern const uint32_t Timeout__Run_m1491551598_MetadataUsageId;
extern "C"  Il2CppObject * Timeout__Run_m1491551598_gshared (Timeout__t2369373703 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Timeout__Run_m1491551598_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Nullable_1_t2303330647  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		SingleAssignmentDisposable_t2336378823 * L_0 = (SingleAssignmentDisposable_t2336378823 *)il2cpp_codegen_object_new(SingleAssignmentDisposable_t2336378823_il2cpp_TypeInfo_var);
		SingleAssignmentDisposable__ctor_m917449122(L_0, /*hidden argument*/NULL);
		__this->set_sourceSubscription_5(L_0);
		TimeoutObservable_1_t1895307051 * L_1 = (TimeoutObservable_1_t1895307051 *)__this->get_parent_2();
		NullCheck(L_1);
		Il2CppObject * L_2 = (Il2CppObject *)L_1->get_scheduler_4();
		TimeoutObservable_1_t1895307051 * L_3 = (TimeoutObservable_1_t1895307051 *)__this->get_parent_2();
		NullCheck(L_3);
		Nullable_1_t2303330647  L_4 = (Nullable_1_t2303330647 )L_3->get_dueTimeDT_3();
		V_0 = (Nullable_1_t2303330647 )L_4;
		DateTimeOffset_t3712260035  L_5 = Nullable_1_get_Value_m4125167011((Nullable_1_t2303330647 *)(&V_0), /*hidden argument*/Nullable_1_get_Value_m4125167011_MethodInfo_var);
		IntPtr_t L_6;
		L_6.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		Action_t437523947 * L_7 = (Action_t437523947 *)il2cpp_codegen_object_new(Action_t437523947_il2cpp_TypeInfo_var);
		Action__ctor_m2957240604(L_7, (Il2CppObject *)__this, (IntPtr_t)L_6, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Scheduler_t103934925_il2cpp_TypeInfo_var);
		Il2CppObject * L_8 = Scheduler_Schedule_m96532664(NULL /*static, unused*/, (Il2CppObject *)L_2, (DateTimeOffset_t3712260035 )L_5, (Action_t437523947 *)L_7, /*hidden argument*/NULL);
		__this->set_timerSubscription_6(L_8);
		SingleAssignmentDisposable_t2336378823 * L_9 = (SingleAssignmentDisposable_t2336378823 *)__this->get_sourceSubscription_5();
		TimeoutObservable_1_t1895307051 * L_10 = (TimeoutObservable_1_t1895307051 *)__this->get_parent_2();
		NullCheck(L_10);
		Il2CppObject* L_11 = (Il2CppObject*)L_10->get_source_1();
		NullCheck((Il2CppObject*)L_11);
		Il2CppObject * L_12 = InterfaceFuncInvoker1< Il2CppObject *, Il2CppObject* >::Invoke(0 /* System.IDisposable UniRx.IObservable`1<System.Object>::Subscribe(UniRx.IObserver`1<T>) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3), (Il2CppObject*)L_11, (Il2CppObject*)__this);
		NullCheck((SingleAssignmentDisposable_t2336378823 *)L_9);
		SingleAssignmentDisposable_set_Disposable_m1599028885((SingleAssignmentDisposable_t2336378823 *)L_9, (Il2CppObject *)L_12, /*hidden argument*/NULL);
		Il2CppObject * L_13 = (Il2CppObject *)__this->get_timerSubscription_6();
		SingleAssignmentDisposable_t2336378823 * L_14 = (SingleAssignmentDisposable_t2336378823 *)__this->get_sourceSubscription_5();
		Il2CppObject * L_15 = StableCompositeDisposable_Create_m3764788535(NULL /*static, unused*/, (Il2CppObject *)L_13, (Il2CppObject *)L_14, /*hidden argument*/NULL);
		return L_15;
	}
}
// System.Void UniRx.Operators.TimeoutObservable`1/Timeout_<System.Object>::OnNext()
extern Il2CppClass* TimeoutException_t2903576483_il2cpp_TypeInfo_var;
extern const uint32_t Timeout__OnNext_m2661514348_MetadataUsageId;
extern "C"  void Timeout__OnNext_m2661514348_gshared (Timeout__t2369373703 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Timeout__OnNext_m2661514348_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Il2CppObject * V_0 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_gate_3();
		V_0 = (Il2CppObject *)L_0;
		Il2CppObject * L_1 = V_0;
		Monitor_Enter_m476686225(NULL /*static, unused*/, (Il2CppObject *)L_1, /*hidden argument*/NULL);
	}

IL_000d:
	try
	{ // begin try (depth: 1)
		{
			bool L_2 = (bool)__this->get_isFinished_4();
			if (!L_2)
			{
				goto IL_001d;
			}
		}

IL_0018:
		{
			IL2CPP_LEAVE(0x59, FINALLY_0029);
		}

IL_001d:
		{
			__this->set_isFinished_4((bool)1);
			IL2CPP_LEAVE(0x30, FINALLY_0029);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0029;
	}

FINALLY_0029:
	{ // begin finally (depth: 1)
		Il2CppObject * L_3 = V_0;
		Monitor_Exit_m2088237919(NULL /*static, unused*/, (Il2CppObject *)L_3, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(41)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(41)
	{
		IL2CPP_JUMP_TBL(0x59, IL_0059)
		IL2CPP_JUMP_TBL(0x30, IL_0030)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0030:
	{
		SingleAssignmentDisposable_t2336378823 * L_4 = (SingleAssignmentDisposable_t2336378823 *)__this->get_sourceSubscription_5();
		NullCheck((SingleAssignmentDisposable_t2336378823 *)L_4);
		VirtActionInvoker0::Invoke(4 /* System.Void UniRx.SingleAssignmentDisposable::Dispose() */, (SingleAssignmentDisposable_t2336378823 *)L_4);
	}

IL_003b:
	try
	{ // begin try (depth: 1)
		Il2CppObject* L_5 = (Il2CppObject*)((OperatorObserverBase_2_t1187768149 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		TimeoutException_t2903576483 * L_6 = (TimeoutException_t2903576483 *)il2cpp_codegen_object_new(TimeoutException_t2903576483_il2cpp_TypeInfo_var);
		TimeoutException__ctor_m3882171335(L_6, /*hidden argument*/NULL);
		NullCheck((Il2CppObject*)L_5);
		InterfaceActionInvoker1< Exception_t1967233988 * >::Invoke(1 /* System.Void UniRx.IObserver`1<System.Object>::OnError(System.Exception) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4), (Il2CppObject*)L_5, (Exception_t1967233988 *)L_6);
		IL2CPP_LEAVE(0x59, FINALLY_0052);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0052;
	}

FINALLY_0052:
	{ // begin finally (depth: 1)
		NullCheck((OperatorObserverBase_2_t1187768149 *)__this);
		VirtActionInvoker0::Invoke(4 /* System.Void UniRx.Operators.OperatorObserverBase`2<System.Object,System.Object>::Dispose() */, (OperatorObserverBase_2_t1187768149 *)__this);
		IL2CPP_END_FINALLY(82)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(82)
	{
		IL2CPP_JUMP_TBL(0x59, IL_0059)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0059:
	{
		return;
	}
}
// System.Void UniRx.Operators.TimeoutObservable`1/Timeout_<System.Object>::OnNext(T)
extern "C"  void Timeout__OnNext_m902567538_gshared (Timeout__t2369373703 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	Il2CppObject * V_0 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_gate_3();
		V_0 = (Il2CppObject *)L_0;
		Il2CppObject * L_1 = V_0;
		Monitor_Enter_m476686225(NULL /*static, unused*/, (Il2CppObject *)L_1, /*hidden argument*/NULL);
	}

IL_000d:
	try
	{ // begin try (depth: 1)
		{
			bool L_2 = (bool)__this->get_isFinished_4();
			if (L_2)
			{
				goto IL_0026;
			}
		}

IL_0018:
		{
			Il2CppObject* L_3 = (Il2CppObject*)((OperatorObserverBase_2_t1187768149 *)__this)->get_observer_0();
			il2cpp_codegen_memory_barrier();
			Il2CppObject * L_4 = ___value0;
			NullCheck((Il2CppObject*)L_3);
			InterfaceActionInvoker1< Il2CppObject * >::Invoke(2 /* System.Void UniRx.IObserver`1<System.Object>::OnNext(T) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4), (Il2CppObject*)L_3, (Il2CppObject *)L_4);
		}

IL_0026:
		{
			IL2CPP_LEAVE(0x32, FINALLY_002b);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_002b;
	}

FINALLY_002b:
	{ // begin finally (depth: 1)
		Il2CppObject * L_5 = V_0;
		Monitor_Exit_m2088237919(NULL /*static, unused*/, (Il2CppObject *)L_5, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(43)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(43)
	{
		IL2CPP_JUMP_TBL(0x32, IL_0032)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0032:
	{
		return;
	}
}
// System.Void UniRx.Operators.TimeoutObservable`1/Timeout_<System.Object>::OnError(System.Exception)
extern Il2CppClass* IDisposable_t1628921374_il2cpp_TypeInfo_var;
extern const uint32_t Timeout__OnError_m2378763649_MetadataUsageId;
extern "C"  void Timeout__OnError_m2378763649_gshared (Timeout__t2369373703 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Timeout__OnError_m2378763649_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Il2CppObject * V_0 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_gate_3();
		V_0 = (Il2CppObject *)L_0;
		Il2CppObject * L_1 = V_0;
		Monitor_Enter_m476686225(NULL /*static, unused*/, (Il2CppObject *)L_1, /*hidden argument*/NULL);
	}

IL_000d:
	try
	{ // begin try (depth: 1)
		{
			bool L_2 = (bool)__this->get_isFinished_4();
			if (!L_2)
			{
				goto IL_001d;
			}
		}

IL_0018:
		{
			IL2CPP_LEAVE(0x55, FINALLY_0034);
		}

IL_001d:
		{
			__this->set_isFinished_4((bool)1);
			Il2CppObject * L_3 = (Il2CppObject *)__this->get_timerSubscription_6();
			NullCheck((Il2CppObject *)L_3);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, (Il2CppObject *)L_3);
			IL2CPP_LEAVE(0x3B, FINALLY_0034);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0034;
	}

FINALLY_0034:
	{ // begin finally (depth: 1)
		Il2CppObject * L_4 = V_0;
		Monitor_Exit_m2088237919(NULL /*static, unused*/, (Il2CppObject *)L_4, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(52)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(52)
	{
		IL2CPP_JUMP_TBL(0x55, IL_0055)
		IL2CPP_JUMP_TBL(0x3B, IL_003b)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_003b:
	try
	{ // begin try (depth: 1)
		Il2CppObject* L_5 = (Il2CppObject*)((OperatorObserverBase_2_t1187768149 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		Exception_t1967233988 * L_6 = ___error0;
		NullCheck((Il2CppObject*)L_5);
		InterfaceActionInvoker1< Exception_t1967233988 * >::Invoke(1 /* System.Void UniRx.IObserver`1<System.Object>::OnError(System.Exception) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4), (Il2CppObject*)L_5, (Exception_t1967233988 *)L_6);
		IL2CPP_LEAVE(0x55, FINALLY_004e);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_004e;
	}

FINALLY_004e:
	{ // begin finally (depth: 1)
		NullCheck((OperatorObserverBase_2_t1187768149 *)__this);
		VirtActionInvoker0::Invoke(4 /* System.Void UniRx.Operators.OperatorObserverBase`2<System.Object,System.Object>::Dispose() */, (OperatorObserverBase_2_t1187768149 *)__this);
		IL2CPP_END_FINALLY(78)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(78)
	{
		IL2CPP_JUMP_TBL(0x55, IL_0055)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0055:
	{
		return;
	}
}
// System.Void UniRx.Operators.TimeoutObservable`1/Timeout_<System.Object>::OnCompleted()
extern Il2CppClass* IDisposable_t1628921374_il2cpp_TypeInfo_var;
extern const uint32_t Timeout__OnCompleted_m19348052_MetadataUsageId;
extern "C"  void Timeout__OnCompleted_m19348052_gshared (Timeout__t2369373703 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Timeout__OnCompleted_m19348052_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Il2CppObject * V_0 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_gate_3();
		V_0 = (Il2CppObject *)L_0;
		Il2CppObject * L_1 = V_0;
		Monitor_Enter_m476686225(NULL /*static, unused*/, (Il2CppObject *)L_1, /*hidden argument*/NULL);
	}

IL_000d:
	try
	{ // begin try (depth: 1)
		{
			bool L_2 = (bool)__this->get_isFinished_4();
			if (L_2)
			{
				goto IL_002a;
			}
		}

IL_0018:
		{
			__this->set_isFinished_4((bool)1);
			Il2CppObject * L_3 = (Il2CppObject *)__this->get_timerSubscription_6();
			NullCheck((Il2CppObject *)L_3);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, (Il2CppObject *)L_3);
		}

IL_002a:
		try
		{ // begin try (depth: 2)
			Il2CppObject* L_4 = (Il2CppObject*)((OperatorObserverBase_2_t1187768149 *)__this)->get_observer_0();
			il2cpp_codegen_memory_barrier();
			NullCheck((Il2CppObject*)L_4);
			InterfaceActionInvoker0::Invoke(0 /* System.Void UniRx.IObserver`1<System.Object>::OnCompleted() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4), (Il2CppObject*)L_4);
			IL2CPP_LEAVE(0x43, FINALLY_003c);
		} // end try (depth: 2)
		catch(Il2CppExceptionWrapper& e)
		{
			__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
			goto FINALLY_003c;
		}

FINALLY_003c:
		{ // begin finally (depth: 2)
			NullCheck((OperatorObserverBase_2_t1187768149 *)__this);
			VirtActionInvoker0::Invoke(4 /* System.Void UniRx.Operators.OperatorObserverBase`2<System.Object,System.Object>::Dispose() */, (OperatorObserverBase_2_t1187768149 *)__this);
			IL2CPP_END_FINALLY(60)
		} // end finally (depth: 2)
		IL2CPP_CLEANUP(60)
		{
			IL2CPP_JUMP_TBL(0x43, IL_0043)
			IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
		}

IL_0043:
		{
			IL2CPP_LEAVE(0x4F, FINALLY_0048);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0048;
	}

FINALLY_0048:
	{ // begin finally (depth: 1)
		Il2CppObject * L_5 = V_0;
		Monitor_Exit_m2088237919(NULL /*static, unused*/, (Il2CppObject *)L_5, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(72)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(72)
	{
		IL2CPP_JUMP_TBL(0x4F, IL_004f)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_004f:
	{
		return;
	}
}
// System.Void UniRx.Operators.TimeoutObservable`1/Timeout<System.Object>::.ctor(UniRx.Operators.TimeoutObservable`1<T>,UniRx.IObserver`1<T>,System.IDisposable)
extern Il2CppClass* Il2CppObject_il2cpp_TypeInfo_var;
extern const uint32_t Timeout__ctor_m3836546079_MetadataUsageId;
extern "C"  void Timeout__ctor_m3836546079_gshared (Timeout_t2052083218 * __this, TimeoutObservable_1_t1895307051 * ___parent0, Il2CppObject* ___observer1, Il2CppObject * ___cancel2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Timeout__ctor_m3836546079_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject * L_0 = (Il2CppObject *)il2cpp_codegen_object_new(Il2CppObject_il2cpp_TypeInfo_var);
		Object__ctor_m1772956182(L_0, /*hidden argument*/NULL);
		__this->set_gate_3(L_0);
		Il2CppObject* L_1 = ___observer1;
		Il2CppObject * L_2 = ___cancel2;
		NullCheck((OperatorObserverBase_2_t1187768149 *)__this);
		((  void (*) (OperatorObserverBase_2_t1187768149 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((OperatorObserverBase_2_t1187768149 *)__this, (Il2CppObject*)L_1, (Il2CppObject *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		TimeoutObservable_1_t1895307051 * L_3 = ___parent0;
		__this->set_parent_2(L_3);
		return;
	}
}
// System.IDisposable UniRx.Operators.TimeoutObservable`1/Timeout<System.Object>::Run()
extern Il2CppClass* SingleAssignmentDisposable_t2336378823_il2cpp_TypeInfo_var;
extern Il2CppClass* SerialDisposable_t2547852742_il2cpp_TypeInfo_var;
extern const uint32_t Timeout_Run_m3464196849_MetadataUsageId;
extern "C"  Il2CppObject * Timeout_Run_m3464196849_gshared (Timeout_t2052083218 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Timeout_Run_m3464196849_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		SingleAssignmentDisposable_t2336378823 * L_0 = (SingleAssignmentDisposable_t2336378823 *)il2cpp_codegen_object_new(SingleAssignmentDisposable_t2336378823_il2cpp_TypeInfo_var);
		SingleAssignmentDisposable__ctor_m917449122(L_0, /*hidden argument*/NULL);
		__this->set_sourceSubscription_6(L_0);
		SerialDisposable_t2547852742 * L_1 = (SerialDisposable_t2547852742 *)il2cpp_codegen_object_new(SerialDisposable_t2547852742_il2cpp_TypeInfo_var);
		SerialDisposable__ctor_m2977852291(L_1, /*hidden argument*/NULL);
		__this->set_timerSubscription_7(L_1);
		SerialDisposable_t2547852742 * L_2 = (SerialDisposable_t2547852742 *)__this->get_timerSubscription_7();
		uint64_t L_3 = (uint64_t)__this->get_objectId_4();
		NullCheck((Timeout_t2052083218 *)__this);
		Il2CppObject * L_4 = ((  Il2CppObject * (*) (Timeout_t2052083218 *, uint64_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)((Timeout_t2052083218 *)__this, (uint64_t)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		NullCheck((SerialDisposable_t2547852742 *)L_2);
		SerialDisposable_set_Disposable_m1438046228((SerialDisposable_t2547852742 *)L_2, (Il2CppObject *)L_4, /*hidden argument*/NULL);
		SingleAssignmentDisposable_t2336378823 * L_5 = (SingleAssignmentDisposable_t2336378823 *)__this->get_sourceSubscription_6();
		TimeoutObservable_1_t1895307051 * L_6 = (TimeoutObservable_1_t1895307051 *)__this->get_parent_2();
		NullCheck(L_6);
		Il2CppObject* L_7 = (Il2CppObject*)L_6->get_source_1();
		NullCheck((Il2CppObject*)L_7);
		Il2CppObject * L_8 = InterfaceFuncInvoker1< Il2CppObject *, Il2CppObject* >::Invoke(0 /* System.IDisposable UniRx.IObservable`1<System.Object>::Subscribe(UniRx.IObserver`1<T>) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3), (Il2CppObject*)L_7, (Il2CppObject*)__this);
		NullCheck((SingleAssignmentDisposable_t2336378823 *)L_5);
		SingleAssignmentDisposable_set_Disposable_m1599028885((SingleAssignmentDisposable_t2336378823 *)L_5, (Il2CppObject *)L_8, /*hidden argument*/NULL);
		SerialDisposable_t2547852742 * L_9 = (SerialDisposable_t2547852742 *)__this->get_timerSubscription_7();
		SingleAssignmentDisposable_t2336378823 * L_10 = (SingleAssignmentDisposable_t2336378823 *)__this->get_sourceSubscription_6();
		Il2CppObject * L_11 = StableCompositeDisposable_Create_m3764788535(NULL /*static, unused*/, (Il2CppObject *)L_9, (Il2CppObject *)L_10, /*hidden argument*/NULL);
		return L_11;
	}
}
// System.IDisposable UniRx.Operators.TimeoutObservable`1/Timeout<System.Object>::RunTimer(System.UInt64)
extern Il2CppClass* Action_t437523947_il2cpp_TypeInfo_var;
extern Il2CppClass* IScheduler_t2938318244_il2cpp_TypeInfo_var;
extern const MethodInfo* Nullable_1_get_Value_m3338249190_MethodInfo_var;
extern const uint32_t Timeout_RunTimer_m579882309_MetadataUsageId;
extern "C"  Il2CppObject * Timeout_RunTimer_m579882309_gshared (Timeout_t2052083218 * __this, uint64_t ___timerId0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Timeout_RunTimer_m579882309_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	U3CRunTimerU3Ec__AnonStorey6C_t4277735173 * V_0 = NULL;
	Nullable_1_t3649900800  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		U3CRunTimerU3Ec__AnonStorey6C_t4277735173 * L_0 = (U3CRunTimerU3Ec__AnonStorey6C_t4277735173 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		((  void (*) (U3CRunTimerU3Ec__AnonStorey6C_t4277735173 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->method)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		V_0 = (U3CRunTimerU3Ec__AnonStorey6C_t4277735173 *)L_0;
		U3CRunTimerU3Ec__AnonStorey6C_t4277735173 * L_1 = V_0;
		uint64_t L_2 = ___timerId0;
		NullCheck(L_1);
		L_1->set_timerId_0(L_2);
		U3CRunTimerU3Ec__AnonStorey6C_t4277735173 * L_3 = V_0;
		NullCheck(L_3);
		L_3->set_U3CU3Ef__this_1(__this);
		TimeoutObservable_1_t1895307051 * L_4 = (TimeoutObservable_1_t1895307051 *)__this->get_parent_2();
		NullCheck(L_4);
		Il2CppObject * L_5 = (Il2CppObject *)L_4->get_scheduler_4();
		TimeoutObservable_1_t1895307051 * L_6 = (TimeoutObservable_1_t1895307051 *)__this->get_parent_2();
		NullCheck(L_6);
		Nullable_1_t3649900800  L_7 = (Nullable_1_t3649900800 )L_6->get_dueTime_2();
		V_1 = (Nullable_1_t3649900800 )L_7;
		TimeSpan_t763862892  L_8 = Nullable_1_get_Value_m3338249190((Nullable_1_t3649900800 *)(&V_1), /*hidden argument*/Nullable_1_get_Value_m3338249190_MethodInfo_var);
		U3CRunTimerU3Ec__AnonStorey6C_t4277735173 * L_9 = V_0;
		IntPtr_t L_10;
		L_10.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		Action_t437523947 * L_11 = (Action_t437523947 *)il2cpp_codegen_object_new(Action_t437523947_il2cpp_TypeInfo_var);
		Action__ctor_m2957240604(L_11, (Il2CppObject *)L_9, (IntPtr_t)L_10, /*hidden argument*/NULL);
		NullCheck((Il2CppObject *)L_5);
		Il2CppObject * L_12 = InterfaceFuncInvoker2< Il2CppObject *, TimeSpan_t763862892 , Action_t437523947 * >::Invoke(2 /* System.IDisposable UniRx.IScheduler::Schedule(System.TimeSpan,System.Action) */, IScheduler_t2938318244_il2cpp_TypeInfo_var, (Il2CppObject *)L_5, (TimeSpan_t763862892 )L_8, (Action_t437523947 *)L_11);
		return L_12;
	}
}
// System.Void UniRx.Operators.TimeoutObservable`1/Timeout<System.Object>::OnNext(T)
extern Il2CppClass* Disposable_t3736388786_il2cpp_TypeInfo_var;
extern const uint32_t Timeout_OnNext_m1526971339_MetadataUsageId;
extern "C"  void Timeout_OnNext_m1526971339_gshared (Timeout_t2052083218 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Timeout_OnNext_m1526971339_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	uint64_t V_0 = 0;
	bool V_1 = false;
	Il2CppObject * V_2 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_gate_3();
		V_2 = (Il2CppObject *)L_0;
		Il2CppObject * L_1 = V_2;
		Monitor_Enter_m476686225(NULL /*static, unused*/, (Il2CppObject *)L_1, /*hidden argument*/NULL);
	}

IL_000d:
	try
	{ // begin try (depth: 1)
		bool L_2 = (bool)__this->get_isTimeout_5();
		V_1 = (bool)L_2;
		uint64_t L_3 = (uint64_t)__this->get_objectId_4();
		__this->set_objectId_4(((int64_t)((int64_t)L_3+(int64_t)(((int64_t)((int64_t)1))))));
		uint64_t L_4 = (uint64_t)__this->get_objectId_4();
		V_0 = (uint64_t)L_4;
		IL2CPP_LEAVE(0x36, FINALLY_002f);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_002f;
	}

FINALLY_002f:
	{ // begin finally (depth: 1)
		Il2CppObject * L_5 = V_2;
		Monitor_Exit_m2088237919(NULL /*static, unused*/, (Il2CppObject *)L_5, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(47)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(47)
	{
		IL2CPP_JUMP_TBL(0x36, IL_0036)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0036:
	{
		bool L_6 = V_1;
		if (!L_6)
		{
			goto IL_003d;
		}
	}
	{
		return;
	}

IL_003d:
	{
		SerialDisposable_t2547852742 * L_7 = (SerialDisposable_t2547852742 *)__this->get_timerSubscription_7();
		IL2CPP_RUNTIME_CLASS_INIT(Disposable_t3736388786_il2cpp_TypeInfo_var);
		Il2CppObject * L_8 = ((Disposable_t3736388786_StaticFields*)Disposable_t3736388786_il2cpp_TypeInfo_var->static_fields)->get_Empty_0();
		NullCheck((SerialDisposable_t2547852742 *)L_7);
		SerialDisposable_set_Disposable_m1438046228((SerialDisposable_t2547852742 *)L_7, (Il2CppObject *)L_8, /*hidden argument*/NULL);
		Il2CppObject* L_9 = (Il2CppObject*)((OperatorObserverBase_2_t1187768149 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		Il2CppObject * L_10 = ___value0;
		NullCheck((Il2CppObject*)L_9);
		InterfaceActionInvoker1< Il2CppObject * >::Invoke(2 /* System.Void UniRx.IObserver`1<System.Object>::OnNext(T) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7), (Il2CppObject*)L_9, (Il2CppObject *)L_10);
		SerialDisposable_t2547852742 * L_11 = (SerialDisposable_t2547852742 *)__this->get_timerSubscription_7();
		uint64_t L_12 = V_0;
		NullCheck((Timeout_t2052083218 *)__this);
		Il2CppObject * L_13 = ((  Il2CppObject * (*) (Timeout_t2052083218 *, uint64_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)((Timeout_t2052083218 *)__this, (uint64_t)L_12, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		NullCheck((SerialDisposable_t2547852742 *)L_11);
		SerialDisposable_set_Disposable_m1438046228((SerialDisposable_t2547852742 *)L_11, (Il2CppObject *)L_13, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UniRx.Operators.TimeoutObservable`1/Timeout<System.Object>::OnError(System.Exception)
extern "C"  void Timeout_OnError_m3961839834_gshared (Timeout_t2052083218 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method)
{
	bool V_0 = false;
	Il2CppObject * V_1 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_gate_3();
		V_1 = (Il2CppObject *)L_0;
		Il2CppObject * L_1 = V_1;
		Monitor_Enter_m476686225(NULL /*static, unused*/, (Il2CppObject *)L_1, /*hidden argument*/NULL);
	}

IL_000d:
	try
	{ // begin try (depth: 1)
		bool L_2 = (bool)__this->get_isTimeout_5();
		V_0 = (bool)L_2;
		uint64_t L_3 = (uint64_t)__this->get_objectId_4();
		__this->set_objectId_4(((int64_t)((int64_t)L_3+(int64_t)(((int64_t)((int64_t)1))))));
		IL2CPP_LEAVE(0x2F, FINALLY_0028);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0028;
	}

FINALLY_0028:
	{ // begin finally (depth: 1)
		Il2CppObject * L_4 = V_1;
		Monitor_Exit_m2088237919(NULL /*static, unused*/, (Il2CppObject *)L_4, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(40)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(40)
	{
		IL2CPP_JUMP_TBL(0x2F, IL_002f)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_002f:
	{
		bool L_5 = V_0;
		if (!L_5)
		{
			goto IL_0036;
		}
	}
	{
		return;
	}

IL_0036:
	{
		SerialDisposable_t2547852742 * L_6 = (SerialDisposable_t2547852742 *)__this->get_timerSubscription_7();
		NullCheck((SerialDisposable_t2547852742 *)L_6);
		VirtActionInvoker0::Invoke(4 /* System.Void UniRx.SerialDisposable::Dispose() */, (SerialDisposable_t2547852742 *)L_6);
	}

IL_0041:
	try
	{ // begin try (depth: 1)
		Il2CppObject* L_7 = (Il2CppObject*)((OperatorObserverBase_2_t1187768149 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		Exception_t1967233988 * L_8 = ___error0;
		NullCheck((Il2CppObject*)L_7);
		InterfaceActionInvoker1< Exception_t1967233988 * >::Invoke(1 /* System.Void UniRx.IObserver`1<System.Object>::OnError(System.Exception) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7), (Il2CppObject*)L_7, (Exception_t1967233988 *)L_8);
		IL2CPP_LEAVE(0x5B, FINALLY_0054);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0054;
	}

FINALLY_0054:
	{ // begin finally (depth: 1)
		NullCheck((OperatorObserverBase_2_t1187768149 *)__this);
		VirtActionInvoker0::Invoke(4 /* System.Void UniRx.Operators.OperatorObserverBase`2<System.Object,System.Object>::Dispose() */, (OperatorObserverBase_2_t1187768149 *)__this);
		IL2CPP_END_FINALLY(84)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(84)
	{
		IL2CPP_JUMP_TBL(0x5B, IL_005b)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_005b:
	{
		return;
	}
}
// System.Void UniRx.Operators.TimeoutObservable`1/Timeout<System.Object>::OnCompleted()
extern "C"  void Timeout_OnCompleted_m3437923117_gshared (Timeout_t2052083218 * __this, const MethodInfo* method)
{
	bool V_0 = false;
	Il2CppObject * V_1 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_gate_3();
		V_1 = (Il2CppObject *)L_0;
		Il2CppObject * L_1 = V_1;
		Monitor_Enter_m476686225(NULL /*static, unused*/, (Il2CppObject *)L_1, /*hidden argument*/NULL);
	}

IL_000d:
	try
	{ // begin try (depth: 1)
		bool L_2 = (bool)__this->get_isTimeout_5();
		V_0 = (bool)L_2;
		uint64_t L_3 = (uint64_t)__this->get_objectId_4();
		__this->set_objectId_4(((int64_t)((int64_t)L_3+(int64_t)(((int64_t)((int64_t)1))))));
		IL2CPP_LEAVE(0x2F, FINALLY_0028);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0028;
	}

FINALLY_0028:
	{ // begin finally (depth: 1)
		Il2CppObject * L_4 = V_1;
		Monitor_Exit_m2088237919(NULL /*static, unused*/, (Il2CppObject *)L_4, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(40)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(40)
	{
		IL2CPP_JUMP_TBL(0x2F, IL_002f)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_002f:
	{
		bool L_5 = V_0;
		if (!L_5)
		{
			goto IL_0036;
		}
	}
	{
		return;
	}

IL_0036:
	{
		SerialDisposable_t2547852742 * L_6 = (SerialDisposable_t2547852742 *)__this->get_timerSubscription_7();
		NullCheck((SerialDisposable_t2547852742 *)L_6);
		VirtActionInvoker0::Invoke(4 /* System.Void UniRx.SerialDisposable::Dispose() */, (SerialDisposable_t2547852742 *)L_6);
	}

IL_0041:
	try
	{ // begin try (depth: 1)
		Il2CppObject* L_7 = (Il2CppObject*)((OperatorObserverBase_2_t1187768149 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		NullCheck((Il2CppObject*)L_7);
		InterfaceActionInvoker0::Invoke(0 /* System.Void UniRx.IObserver`1<System.Object>::OnCompleted() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7), (Il2CppObject*)L_7);
		IL2CPP_LEAVE(0x5A, FINALLY_0053);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0053;
	}

FINALLY_0053:
	{ // begin finally (depth: 1)
		NullCheck((OperatorObserverBase_2_t1187768149 *)__this);
		VirtActionInvoker0::Invoke(4 /* System.Void UniRx.Operators.OperatorObserverBase`2<System.Object,System.Object>::Dispose() */, (OperatorObserverBase_2_t1187768149 *)__this);
		IL2CPP_END_FINALLY(83)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(83)
	{
		IL2CPP_JUMP_TBL(0x5A, IL_005a)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_005a:
	{
		return;
	}
}
// System.Void UniRx.Operators.TimeoutObservable`1<System.Object>::.ctor(UniRx.IObservable`1<T>,System.TimeSpan,UniRx.IScheduler)
extern Il2CppClass* Scheduler_t103934925_il2cpp_TypeInfo_var;
extern const MethodInfo* Nullable_1__ctor_m4008503583_MethodInfo_var;
extern const uint32_t TimeoutObservable_1__ctor_m3367647542_MetadataUsageId;
extern "C"  void TimeoutObservable_1__ctor_m3367647542_gshared (TimeoutObservable_1_t1895307051 * __this, Il2CppObject* ___source0, TimeSpan_t763862892  ___dueTime1, Il2CppObject * ___scheduler2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TimeoutObservable_1__ctor_m3367647542_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	TimeoutObservable_1_t1895307051 * G_B2_0 = NULL;
	TimeoutObservable_1_t1895307051 * G_B1_0 = NULL;
	int32_t G_B3_0 = 0;
	TimeoutObservable_1_t1895307051 * G_B3_1 = NULL;
	{
		Il2CppObject * L_0 = ___scheduler2;
		IL2CPP_RUNTIME_CLASS_INIT(Scheduler_t103934925_il2cpp_TypeInfo_var);
		Il2CppObject * L_1 = ((Scheduler_t103934925_StaticFields*)Scheduler_t103934925_il2cpp_TypeInfo_var->static_fields)->get_CurrentThread_0();
		G_B1_0 = ((TimeoutObservable_1_t1895307051 *)(__this));
		if ((((Il2CppObject*)(Il2CppObject *)L_0) == ((Il2CppObject*)(Il2CppObject *)L_1)))
		{
			G_B2_0 = ((TimeoutObservable_1_t1895307051 *)(__this));
			goto IL_0014;
		}
	}
	{
		Il2CppObject* L_2 = ___source0;
		bool L_3 = ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)(NULL /*static, unused*/, (Il2CppObject*)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		G_B3_0 = ((int32_t)(L_3));
		G_B3_1 = ((TimeoutObservable_1_t1895307051 *)(G_B1_0));
		goto IL_0015;
	}

IL_0014:
	{
		G_B3_0 = 1;
		G_B3_1 = ((TimeoutObservable_1_t1895307051 *)(G_B2_0));
	}

IL_0015:
	{
		NullCheck((OperatorObservableBase_1_t4196218687 *)G_B3_1);
		((  void (*) (OperatorObservableBase_1_t4196218687 *, bool, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)((OperatorObservableBase_1_t4196218687 *)G_B3_1, (bool)G_B3_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		Il2CppObject* L_4 = ___source0;
		__this->set_source_1(L_4);
		TimeSpan_t763862892  L_5 = ___dueTime1;
		Nullable_1_t3649900800  L_6;
		memset(&L_6, 0, sizeof(L_6));
		Nullable_1__ctor_m4008503583(&L_6, (TimeSpan_t763862892 )L_5, /*hidden argument*/Nullable_1__ctor_m4008503583_MethodInfo_var);
		__this->set_dueTime_2(L_6);
		Il2CppObject * L_7 = ___scheduler2;
		__this->set_scheduler_4(L_7);
		return;
	}
}
// System.Void UniRx.Operators.TimeoutObservable`1<System.Object>::.ctor(UniRx.IObservable`1<T>,System.DateTimeOffset,UniRx.IScheduler)
extern Il2CppClass* Scheduler_t103934925_il2cpp_TypeInfo_var;
extern const MethodInfo* Nullable_1__ctor_m522950596_MethodInfo_var;
extern const uint32_t TimeoutObservable_1__ctor_m3458289869_MetadataUsageId;
extern "C"  void TimeoutObservable_1__ctor_m3458289869_gshared (TimeoutObservable_1_t1895307051 * __this, Il2CppObject* ___source0, DateTimeOffset_t3712260035  ___dueTime1, Il2CppObject * ___scheduler2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TimeoutObservable_1__ctor_m3458289869_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	TimeoutObservable_1_t1895307051 * G_B2_0 = NULL;
	TimeoutObservable_1_t1895307051 * G_B1_0 = NULL;
	int32_t G_B3_0 = 0;
	TimeoutObservable_1_t1895307051 * G_B3_1 = NULL;
	{
		Il2CppObject * L_0 = ___scheduler2;
		IL2CPP_RUNTIME_CLASS_INIT(Scheduler_t103934925_il2cpp_TypeInfo_var);
		Il2CppObject * L_1 = ((Scheduler_t103934925_StaticFields*)Scheduler_t103934925_il2cpp_TypeInfo_var->static_fields)->get_CurrentThread_0();
		G_B1_0 = ((TimeoutObservable_1_t1895307051 *)(__this));
		if ((((Il2CppObject*)(Il2CppObject *)L_0) == ((Il2CppObject*)(Il2CppObject *)L_1)))
		{
			G_B2_0 = ((TimeoutObservable_1_t1895307051 *)(__this));
			goto IL_0014;
		}
	}
	{
		Il2CppObject* L_2 = ___source0;
		bool L_3 = ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)(NULL /*static, unused*/, (Il2CppObject*)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		G_B3_0 = ((int32_t)(L_3));
		G_B3_1 = ((TimeoutObservable_1_t1895307051 *)(G_B1_0));
		goto IL_0015;
	}

IL_0014:
	{
		G_B3_0 = 1;
		G_B3_1 = ((TimeoutObservable_1_t1895307051 *)(G_B2_0));
	}

IL_0015:
	{
		NullCheck((OperatorObservableBase_1_t4196218687 *)G_B3_1);
		((  void (*) (OperatorObservableBase_1_t4196218687 *, bool, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)((OperatorObservableBase_1_t4196218687 *)G_B3_1, (bool)G_B3_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		Il2CppObject* L_4 = ___source0;
		__this->set_source_1(L_4);
		DateTimeOffset_t3712260035  L_5 = ___dueTime1;
		Nullable_1_t2303330647  L_6;
		memset(&L_6, 0, sizeof(L_6));
		Nullable_1__ctor_m522950596(&L_6, (DateTimeOffset_t3712260035 )L_5, /*hidden argument*/Nullable_1__ctor_m522950596_MethodInfo_var);
		__this->set_dueTimeDT_3(L_6);
		Il2CppObject * L_7 = ___scheduler2;
		__this->set_scheduler_4(L_7);
		return;
	}
}
// System.IDisposable UniRx.Operators.TimeoutObservable`1<System.Object>::SubscribeCore(UniRx.IObserver`1<T>,System.IDisposable)
extern const MethodInfo* Nullable_1_get_HasValue_m2797118855_MethodInfo_var;
extern const uint32_t TimeoutObservable_1_SubscribeCore_m3770527885_MetadataUsageId;
extern "C"  Il2CppObject * TimeoutObservable_1_SubscribeCore_m3770527885_gshared (TimeoutObservable_1_t1895307051 * __this, Il2CppObject* ___observer0, Il2CppObject * ___cancel1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TimeoutObservable_1_SubscribeCore_m3770527885_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Nullable_1_t3649900800  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Nullable_1_t3649900800  L_0 = (Nullable_1_t3649900800 )__this->get_dueTime_2();
		V_0 = (Nullable_1_t3649900800 )L_0;
		bool L_1 = Nullable_1_get_HasValue_m2797118855((Nullable_1_t3649900800 *)(&V_0), /*hidden argument*/Nullable_1_get_HasValue_m2797118855_MethodInfo_var);
		if (!L_1)
		{
			goto IL_0021;
		}
	}
	{
		Il2CppObject* L_2 = ___observer0;
		Il2CppObject * L_3 = ___cancel1;
		Timeout_t2052083218 * L_4 = (Timeout_t2052083218 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		((  void (*) (Timeout_t2052083218 *, TimeoutObservable_1_t1895307051 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)(L_4, (TimeoutObservable_1_t1895307051 *)__this, (Il2CppObject*)L_2, (Il2CppObject *)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		NullCheck((Timeout_t2052083218 *)L_4);
		Il2CppObject * L_5 = ((  Il2CppObject * (*) (Timeout_t2052083218 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->method)((Timeout_t2052083218 *)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		return L_5;
	}

IL_0021:
	{
		Il2CppObject* L_6 = ___observer0;
		Il2CppObject * L_7 = ___cancel1;
		Timeout__t2369373703 * L_8 = (Timeout__t2369373703 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		((  void (*) (Timeout__t2369373703 *, TimeoutObservable_1_t1895307051 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7)->method)(L_8, (TimeoutObservable_1_t1895307051 *)__this, (Il2CppObject*)L_6, (Il2CppObject *)L_7, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7));
		NullCheck((Timeout__t2369373703 *)L_8);
		Il2CppObject * L_9 = ((  Il2CppObject * (*) (Timeout__t2369373703 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8)->method)((Timeout__t2369373703 *)L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8));
		return L_9;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
