﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.ClientModels.UpdateUserTitleDisplayNameResult
struct UpdateUserTitleDisplayNameResult_t807487222;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String968488902.h"

// System.Void PlayFab.ClientModels.UpdateUserTitleDisplayNameResult::.ctor()
extern "C"  void UpdateUserTitleDisplayNameResult__ctor_m1134725639 (UpdateUserTitleDisplayNameResult_t807487222 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PlayFab.ClientModels.UpdateUserTitleDisplayNameResult::get_DisplayName()
extern "C"  String_t* UpdateUserTitleDisplayNameResult_get_DisplayName_m1335399244 (UpdateUserTitleDisplayNameResult_t807487222 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.UpdateUserTitleDisplayNameResult::set_DisplayName(System.String)
extern "C"  void UpdateUserTitleDisplayNameResult_set_DisplayName_m2176921837 (UpdateUserTitleDisplayNameResult_t807487222 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
