﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// ZergRush.IReactiveCollection`1<System.Object>
struct IReactiveCollection_1_t4089814762;
// System.Func`2<System.Object,System.Boolean>
struct Func_2_t1509682273;

#include "mscorlib_System_Collections_ObjectModel_Collection2806094150.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ZergRush.ReactiveCollectionExtension/FilterCollection`1<System.Object>
struct  FilterCollection_1_t1215889544  : public Collection_1_t2806094150
{
public:
	// ZergRush.IReactiveCollection`1<T> ZergRush.ReactiveCollectionExtension/FilterCollection`1::parent
	Il2CppObject* ___parent_2;
	// System.Func`2<T,System.Boolean> ZergRush.ReactiveCollectionExtension/FilterCollection`1::predicate
	Func_2_t1509682273 * ___predicate_3;

public:
	inline static int32_t get_offset_of_parent_2() { return static_cast<int32_t>(offsetof(FilterCollection_1_t1215889544, ___parent_2)); }
	inline Il2CppObject* get_parent_2() const { return ___parent_2; }
	inline Il2CppObject** get_address_of_parent_2() { return &___parent_2; }
	inline void set_parent_2(Il2CppObject* value)
	{
		___parent_2 = value;
		Il2CppCodeGenWriteBarrier(&___parent_2, value);
	}

	inline static int32_t get_offset_of_predicate_3() { return static_cast<int32_t>(offsetof(FilterCollection_1_t1215889544, ___predicate_3)); }
	inline Func_2_t1509682273 * get_predicate_3() const { return ___predicate_3; }
	inline Func_2_t1509682273 ** get_address_of_predicate_3() { return &___predicate_3; }
	inline void set_predicate_3(Func_2_t1509682273 * value)
	{
		___predicate_3 = value;
		Il2CppCodeGenWriteBarrier(&___predicate_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
