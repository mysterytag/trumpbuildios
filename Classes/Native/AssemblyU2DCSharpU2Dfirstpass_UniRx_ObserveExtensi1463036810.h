﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Object
struct Object_t3878351788;
struct Object_t3878351788_marshaled_pinvoke;
// System.Func`2<System.Object,System.Object>
struct Func_2_t2135783352;

#include "mscorlib_System_Object837106420.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_FrameCountType167759182.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.ObserveExtensions/<ObserveEveryValueChanged>c__AnonStorey91`2<System.Object,System.Object>
struct  U3CObserveEveryValueChangedU3Ec__AnonStorey91_2_t1463036810  : public Il2CppObject
{
public:
	// UnityEngine.Object UniRx.ObserveExtensions/<ObserveEveryValueChanged>c__AnonStorey91`2::unityObject
	Object_t3878351788 * ___unityObject_0;
	// System.Func`2<TSource,TProperty> UniRx.ObserveExtensions/<ObserveEveryValueChanged>c__AnonStorey91`2::propertySelector
	Func_2_t2135783352 * ___propertySelector_1;
	// UniRx.FrameCountType UniRx.ObserveExtensions/<ObserveEveryValueChanged>c__AnonStorey91`2::frameCountType
	int32_t ___frameCountType_2;

public:
	inline static int32_t get_offset_of_unityObject_0() { return static_cast<int32_t>(offsetof(U3CObserveEveryValueChangedU3Ec__AnonStorey91_2_t1463036810, ___unityObject_0)); }
	inline Object_t3878351788 * get_unityObject_0() const { return ___unityObject_0; }
	inline Object_t3878351788 ** get_address_of_unityObject_0() { return &___unityObject_0; }
	inline void set_unityObject_0(Object_t3878351788 * value)
	{
		___unityObject_0 = value;
		Il2CppCodeGenWriteBarrier(&___unityObject_0, value);
	}

	inline static int32_t get_offset_of_propertySelector_1() { return static_cast<int32_t>(offsetof(U3CObserveEveryValueChangedU3Ec__AnonStorey91_2_t1463036810, ___propertySelector_1)); }
	inline Func_2_t2135783352 * get_propertySelector_1() const { return ___propertySelector_1; }
	inline Func_2_t2135783352 ** get_address_of_propertySelector_1() { return &___propertySelector_1; }
	inline void set_propertySelector_1(Func_2_t2135783352 * value)
	{
		___propertySelector_1 = value;
		Il2CppCodeGenWriteBarrier(&___propertySelector_1, value);
	}

	inline static int32_t get_offset_of_frameCountType_2() { return static_cast<int32_t>(offsetof(U3CObserveEveryValueChangedU3Ec__AnonStorey91_2_t1463036810, ___frameCountType_2)); }
	inline int32_t get_frameCountType_2() const { return ___frameCountType_2; }
	inline int32_t* get_address_of_frameCountType_2() { return &___frameCountType_2; }
	inline void set_frameCountType_2(int32_t value)
	{
		___frameCountType_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
