﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CellReactiveApi/<EachNotNull>c__AnonStorey103`1/<EachNotNull>c__AnonStorey104`1<System.Object>
struct U3CEachNotNullU3Ec__AnonStorey104_1_t1588511641;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"

// System.Void CellReactiveApi/<EachNotNull>c__AnonStorey103`1/<EachNotNull>c__AnonStorey104`1<System.Object>::.ctor()
extern "C"  void U3CEachNotNullU3Ec__AnonStorey104_1__ctor_m3035201695_gshared (U3CEachNotNullU3Ec__AnonStorey104_1_t1588511641 * __this, const MethodInfo* method);
#define U3CEachNotNullU3Ec__AnonStorey104_1__ctor_m3035201695(__this, method) ((  void (*) (U3CEachNotNullU3Ec__AnonStorey104_1_t1588511641 *, const MethodInfo*))U3CEachNotNullU3Ec__AnonStorey104_1__ctor_m3035201695_gshared)(__this, method)
// System.Void CellReactiveApi/<EachNotNull>c__AnonStorey103`1/<EachNotNull>c__AnonStorey104`1<System.Object>::<>m__193(T)
extern "C"  void U3CEachNotNullU3Ec__AnonStorey104_1_U3CU3Em__193_m2585472633_gshared (U3CEachNotNullU3Ec__AnonStorey104_1_t1588511641 * __this, Il2CppObject * ___val0, const MethodInfo* method);
#define U3CEachNotNullU3Ec__AnonStorey104_1_U3CU3Em__193_m2585472633(__this, ___val0, method) ((  void (*) (U3CEachNotNullU3Ec__AnonStorey104_1_t1588511641 *, Il2CppObject *, const MethodInfo*))U3CEachNotNullU3Ec__AnonStorey104_1_U3CU3Em__193_m2585472633_gshared)(__this, ___val0, method)
