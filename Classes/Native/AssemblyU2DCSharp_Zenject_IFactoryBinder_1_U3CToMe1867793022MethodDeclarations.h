﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Zenject.IFactoryBinder`1/<ToMethod>c__AnonStorey174<System.Object>
struct U3CToMethodU3Ec__AnonStorey174_t1867793022;
// Zenject.IFactory`1<System.Object>
struct IFactory_1_t2124284520;
// Zenject.InjectContext
struct InjectContext_t3456483891;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Zenject_InjectContext3456483891.h"

// System.Void Zenject.IFactoryBinder`1/<ToMethod>c__AnonStorey174<System.Object>::.ctor()
extern "C"  void U3CToMethodU3Ec__AnonStorey174__ctor_m1094580512_gshared (U3CToMethodU3Ec__AnonStorey174_t1867793022 * __this, const MethodInfo* method);
#define U3CToMethodU3Ec__AnonStorey174__ctor_m1094580512(__this, method) ((  void (*) (U3CToMethodU3Ec__AnonStorey174_t1867793022 *, const MethodInfo*))U3CToMethodU3Ec__AnonStorey174__ctor_m1094580512_gshared)(__this, method)
// Zenject.IFactory`1<TContract> Zenject.IFactoryBinder`1/<ToMethod>c__AnonStorey174<System.Object>::<>m__294(Zenject.InjectContext)
extern "C"  Il2CppObject* U3CToMethodU3Ec__AnonStorey174_U3CU3Em__294_m226769437_gshared (U3CToMethodU3Ec__AnonStorey174_t1867793022 * __this, InjectContext_t3456483891 * ___ctx0, const MethodInfo* method);
#define U3CToMethodU3Ec__AnonStorey174_U3CU3Em__294_m226769437(__this, ___ctx0, method) ((  Il2CppObject* (*) (U3CToMethodU3Ec__AnonStorey174_t1867793022 *, InjectContext_t3456483891 *, const MethodInfo*))U3CToMethodU3Ec__AnonStorey174_U3CU3Em__294_m226769437_gshared)(__this, ___ctx0, method)
