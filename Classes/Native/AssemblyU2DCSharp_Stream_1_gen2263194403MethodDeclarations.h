﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Stream_1_gen3823212738MethodDeclarations.h"

// System.Void Stream`1<CollectionMoveEvent`1<System.Object>>::.ctor()
#define Stream_1__ctor_m4230156392(__this, method) ((  void (*) (Stream_1_t2263194403 *, const MethodInfo*))Stream_1__ctor_m2034388992_gshared)(__this, method)
// System.Void Stream`1<CollectionMoveEvent`1<System.Object>>::Send(T)
#define Stream_1_Send_m2759373946(__this, ___value0, method) ((  void (*) (Stream_1_t2263194403 *, CollectionMoveEvent_1_t3572055381 *, const MethodInfo*))Stream_1_Send_m563606546_gshared)(__this, ___value0, method)
// System.Void Stream`1<CollectionMoveEvent`1<System.Object>>::SendInTransaction(T,System.Int32)
#define Stream_1_SendInTransaction_m2697487248(__this, ___value0, ___p1, method) ((  void (*) (Stream_1_t2263194403 *, CollectionMoveEvent_1_t3572055381 *, int32_t, const MethodInfo*))Stream_1_SendInTransaction_m3764966696_gshared)(__this, ___value0, ___p1, method)
// System.IDisposable Stream`1<CollectionMoveEvent`1<System.Object>>::Listen(System.Action`1<T>,Priority)
#define Stream_1_Listen_m3869139532(__this, ___action0, ___priority1, method) ((  Il2CppObject * (*) (Stream_1_t2263194403 *, Action_1_t3720508086 *, int32_t, const MethodInfo*))Stream_1_Listen_m889871082_gshared)(__this, ___action0, ___priority1, method)
// System.IDisposable Stream`1<CollectionMoveEvent`1<System.Object>>::Listen(System.Action,Priority)
#define Stream_1_Listen_m3647561563(__this, ___action0, ___p1, method) ((  Il2CppObject * (*) (Stream_1_t2263194403 *, Action_t437523947 *, int32_t, const MethodInfo*))Stream_1_Listen_m2133851133_gshared)(__this, ___action0, ___p1, method)
// System.Void Stream`1<CollectionMoveEvent`1<System.Object>>::Dispose()
#define Stream_1_Dispose_m2039513765(__this, method) ((  void (*) (Stream_1_t2263194403 *, const MethodInfo*))Stream_1_Dispose_m735984701_gshared)(__this, method)
// System.Void Stream`1<CollectionMoveEvent`1<System.Object>>::SetInputStream(IStream`1<T>)
#define Stream_1_SetInputStream_m3030126744(__this, ___stream0, method) ((  void (*) (Stream_1_t2263194403 *, Il2CppObject*, const MethodInfo*))Stream_1_SetInputStream_m3106996224_gshared)(__this, ___stream0, method)
// System.Void Stream`1<CollectionMoveEvent`1<System.Object>>::ClearInputStream()
#define Stream_1_ClearInputStream_m1742937977(__this, method) ((  void (*) (Stream_1_t2263194403 *, const MethodInfo*))Stream_1_ClearInputStream_m445690081_gshared)(__this, method)
// System.Void Stream`1<CollectionMoveEvent`1<System.Object>>::TransactionIterationFinished()
#define Stream_1_TransactionIterationFinished_m2256433613(__this, method) ((  void (*) (Stream_1_t2263194403 *, const MethodInfo*))Stream_1_TransactionIterationFinished_m3113559861_gshared)(__this, method)
// System.Void Stream`1<CollectionMoveEvent`1<System.Object>>::Unpack(System.Int32)
#define Stream_1_Unpack_m207998239(__this, ___p0, method) ((  void (*) (Stream_1_t2263194403 *, int32_t, const MethodInfo*))Stream_1_Unpack_m3388253319_gshared)(__this, ___p0, method)
// System.Void Stream`1<CollectionMoveEvent`1<System.Object>>::<SetInputStream>m__1BA(T)
#define Stream_1_U3CSetInputStreamU3Em__1BA_m3066316037(__this, ___val0, method) ((  void (*) (Stream_1_t2263194403 *, CollectionMoveEvent_1_t3572055381 *, const MethodInfo*))Stream_1_U3CSetInputStreamU3Em__1BA_m3579563677_gshared)(__this, ___val0, method)
