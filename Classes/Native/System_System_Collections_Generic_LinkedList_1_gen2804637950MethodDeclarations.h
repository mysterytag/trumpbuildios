﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "System_System_Collections_Generic_LinkedList_1_gen2577235966MethodDeclarations.h"

// System.Void System.Collections.Generic.LinkedList`1<Zenject.TaskUpdater`1/TaskInfo<System.Object>>::.ctor()
#define LinkedList_1__ctor_m1110362889(__this, method) ((  void (*) (LinkedList_1_t2804637950 *, const MethodInfo*))LinkedList_1__ctor_m2955457271_gshared)(__this, method)
// System.Void System.Collections.Generic.LinkedList`1<Zenject.TaskUpdater`1/TaskInfo<System.Object>>::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
#define LinkedList_1__ctor_m3163966666(__this, ___info0, ___context1, method) ((  void (*) (LinkedList_1_t2804637950 *, SerializationInfo_t2995724695 *, StreamingContext_t986364934 , const MethodInfo*))LinkedList_1__ctor_m3369579448_gshared)(__this, ___info0, ___context1, method)
// System.Void System.Collections.Generic.LinkedList`1<Zenject.TaskUpdater`1/TaskInfo<System.Object>>::System.Collections.Generic.ICollection<T>.Add(T)
#define LinkedList_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m3287923094(__this, ___value0, method) ((  void (*) (LinkedList_1_t2804637950 *, TaskInfo_t1064508404 *, const MethodInfo*))LinkedList_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m3576108392_gshared)(__this, ___value0, method)
// System.Void System.Collections.Generic.LinkedList`1<Zenject.TaskUpdater`1/TaskInfo<System.Object>>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define LinkedList_1_System_Collections_ICollection_CopyTo_m3867837019(__this, ___array0, ___index1, method) ((  void (*) (LinkedList_1_t2804637950 *, Il2CppArray *, int32_t, const MethodInfo*))LinkedList_1_System_Collections_ICollection_CopyTo_m2331638317_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.LinkedList`1<Zenject.TaskUpdater`1/TaskInfo<System.Object>>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
#define LinkedList_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m4066827547(__this, method) ((  Il2CppObject* (*) (LinkedList_1_t2804637950 *, const MethodInfo*))LinkedList_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m2027502985_gshared)(__this, method)
// System.Collections.IEnumerator System.Collections.Generic.LinkedList`1<Zenject.TaskUpdater`1/TaskInfo<System.Object>>::System.Collections.IEnumerable.GetEnumerator()
#define LinkedList_1_System_Collections_IEnumerable_GetEnumerator_m2993479210(__this, method) ((  Il2CppObject * (*) (LinkedList_1_t2804637950 *, const MethodInfo*))LinkedList_1_System_Collections_IEnumerable_GetEnumerator_m51916412_gshared)(__this, method)
// System.Boolean System.Collections.Generic.LinkedList`1<Zenject.TaskUpdater`1/TaskInfo<System.Object>>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
#define LinkedList_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m3306418662(__this, method) ((  bool (*) (LinkedList_1_t2804637950 *, const MethodInfo*))LinkedList_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m2230847288_gshared)(__this, method)
// System.Boolean System.Collections.Generic.LinkedList`1<Zenject.TaskUpdater`1/TaskInfo<System.Object>>::System.Collections.ICollection.get_IsSynchronized()
#define LinkedList_1_System_Collections_ICollection_get_IsSynchronized_m3846405399(__this, method) ((  bool (*) (LinkedList_1_t2804637950 *, const MethodInfo*))LinkedList_1_System_Collections_ICollection_get_IsSynchronized_m683991045_gshared)(__this, method)
// System.Object System.Collections.Generic.LinkedList`1<Zenject.TaskUpdater`1/TaskInfo<System.Object>>::System.Collections.ICollection.get_SyncRoot()
#define LinkedList_1_System_Collections_ICollection_get_SyncRoot_m327139607(__this, method) ((  Il2CppObject * (*) (LinkedList_1_t2804637950 *, const MethodInfo*))LinkedList_1_System_Collections_ICollection_get_SyncRoot_m573420165_gshared)(__this, method)
// System.Void System.Collections.Generic.LinkedList`1<Zenject.TaskUpdater`1/TaskInfo<System.Object>>::VerifyReferencedNode(System.Collections.Generic.LinkedListNode`1<T>)
#define LinkedList_1_VerifyReferencedNode_m2104030146(__this, ___node0, method) ((  void (*) (LinkedList_1_t2804637950 *, LinkedListNode_1_t766499024 *, const MethodInfo*))LinkedList_1_VerifyReferencedNode_m3939775124_gshared)(__this, ___node0, method)
// System.Collections.Generic.LinkedListNode`1<T> System.Collections.Generic.LinkedList`1<Zenject.TaskUpdater`1/TaskInfo<System.Object>>::AddBefore(System.Collections.Generic.LinkedListNode`1<T>,T)
#define LinkedList_1_AddBefore_m4277291808(__this, ___node0, ___value1, method) ((  LinkedListNode_1_t766499024 * (*) (LinkedList_1_t2804637950 *, LinkedListNode_1_t766499024 *, TaskInfo_t1064508404 *, const MethodInfo*))LinkedList_1_AddBefore_m1086189582_gshared)(__this, ___node0, ___value1, method)
// System.Collections.Generic.LinkedListNode`1<T> System.Collections.Generic.LinkedList`1<Zenject.TaskUpdater`1/TaskInfo<System.Object>>::AddLast(T)
#define LinkedList_1_AddLast_m3444652274(__this, ___value0, method) ((  LinkedListNode_1_t766499024 * (*) (LinkedList_1_t2804637950 *, TaskInfo_t1064508404 *, const MethodInfo*))LinkedList_1_AddLast_m4070107716_gshared)(__this, ___value0, method)
// System.Void System.Collections.Generic.LinkedList`1<Zenject.TaskUpdater`1/TaskInfo<System.Object>>::Clear()
#define LinkedList_1_Clear_m2811463476(__this, method) ((  void (*) (LinkedList_1_t2804637950 *, const MethodInfo*))LinkedList_1_Clear_m361590562_gshared)(__this, method)
// System.Boolean System.Collections.Generic.LinkedList`1<Zenject.TaskUpdater`1/TaskInfo<System.Object>>::Contains(T)
#define LinkedList_1_Contains_m3851319758(__this, ___value0, method) ((  bool (*) (LinkedList_1_t2804637950 *, TaskInfo_t1064508404 *, const MethodInfo*))LinkedList_1_Contains_m3484410556_gshared)(__this, ___value0, method)
// System.Void System.Collections.Generic.LinkedList`1<Zenject.TaskUpdater`1/TaskInfo<System.Object>>::CopyTo(T[],System.Int32)
#define LinkedList_1_CopyTo_m1225060038(__this, ___array0, ___index1, method) ((  void (*) (LinkedList_1_t2804637950 *, TaskInfoU5BU5D_t460411325*, int32_t, const MethodInfo*))LinkedList_1_CopyTo_m3470139544_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.LinkedListNode`1<T> System.Collections.Generic.LinkedList`1<Zenject.TaskUpdater`1/TaskInfo<System.Object>>::Find(T)
#define LinkedList_1_Find_m2647695608(__this, ___value0, method) ((  LinkedListNode_1_t766499024 * (*) (LinkedList_1_t2804637950 *, TaskInfo_t1064508404 *, const MethodInfo*))LinkedList_1_Find_m2643247334_gshared)(__this, ___value0, method)
// System.Collections.Generic.LinkedList`1/Enumerator<T> System.Collections.Generic.LinkedList`1<Zenject.TaskUpdater`1/TaskInfo<System.Object>>::GetEnumerator()
#define LinkedList_1_GetEnumerator_m3103288856(__this, method) ((  Enumerator_t4242217662  (*) (LinkedList_1_t2804637950 *, const MethodInfo*))LinkedList_1_GetEnumerator_m3713737734_gshared)(__this, method)
// System.Void System.Collections.Generic.LinkedList`1<Zenject.TaskUpdater`1/TaskInfo<System.Object>>::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
#define LinkedList_1_GetObjectData_m2513119783(__this, ___info0, ___context1, method) ((  void (*) (LinkedList_1_t2804637950 *, SerializationInfo_t2995724695 *, StreamingContext_t986364934 , const MethodInfo*))LinkedList_1_GetObjectData_m3974480661_gshared)(__this, ___info0, ___context1, method)
// System.Void System.Collections.Generic.LinkedList`1<Zenject.TaskUpdater`1/TaskInfo<System.Object>>::OnDeserialization(System.Object)
#define LinkedList_1_OnDeserialization_m4012073629(__this, ___sender0, method) ((  void (*) (LinkedList_1_t2804637950 *, Il2CppObject *, const MethodInfo*))LinkedList_1_OnDeserialization_m3445006959_gshared)(__this, ___sender0, method)
// System.Boolean System.Collections.Generic.LinkedList`1<Zenject.TaskUpdater`1/TaskInfo<System.Object>>::Remove(T)
#define LinkedList_1_Remove_m2081641801(__this, ___value0, method) ((  bool (*) (LinkedList_1_t2804637950 *, TaskInfo_t1064508404 *, const MethodInfo*))LinkedList_1_Remove_m3283493303_gshared)(__this, ___value0, method)
// System.Void System.Collections.Generic.LinkedList`1<Zenject.TaskUpdater`1/TaskInfo<System.Object>>::Remove(System.Collections.Generic.LinkedListNode`1<T>)
#define LinkedList_1_Remove_m4179905746(__this, ___node0, method) ((  void (*) (LinkedList_1_t2804637950 *, LinkedListNode_1_t766499024 *, const MethodInfo*))LinkedList_1_Remove_m4034790180_gshared)(__this, ___node0, method)
// System.Void System.Collections.Generic.LinkedList`1<Zenject.TaskUpdater`1/TaskInfo<System.Object>>::RemoveFirst()
#define LinkedList_1_RemoveFirst_m633955635(__this, method) ((  void (*) (LinkedList_1_t2804637950 *, const MethodInfo*))LinkedList_1_RemoveFirst_m1652892321_gshared)(__this, method)
// System.Void System.Collections.Generic.LinkedList`1<Zenject.TaskUpdater`1/TaskInfo<System.Object>>::RemoveLast()
#define LinkedList_1_RemoveLast_m1847433301(__this, method) ((  void (*) (LinkedList_1_t2804637950 *, const MethodInfo*))LinkedList_1_RemoveLast_m2573038887_gshared)(__this, method)
// System.Int32 System.Collections.Generic.LinkedList`1<Zenject.TaskUpdater`1/TaskInfo<System.Object>>::get_Count()
#define LinkedList_1_get_Count_m1693753437(__this, method) ((  int32_t (*) (LinkedList_1_t2804637950 *, const MethodInfo*))LinkedList_1_get_Count_m1368924491_gshared)(__this, method)
// System.Collections.Generic.LinkedListNode`1<T> System.Collections.Generic.LinkedList`1<Zenject.TaskUpdater`1/TaskInfo<System.Object>>::get_First()
#define LinkedList_1_get_First_m1069338268(__this, method) ((  LinkedListNode_1_t766499024 * (*) (LinkedList_1_t2804637950 *, const MethodInfo*))LinkedList_1_get_First_m3278587786_gshared)(__this, method)
// System.Collections.Generic.LinkedListNode`1<T> System.Collections.Generic.LinkedList`1<Zenject.TaskUpdater`1/TaskInfo<System.Object>>::get_Last()
#define LinkedList_1_get_Last_m3939687884(__this, method) ((  LinkedListNode_1_t766499024 * (*) (LinkedList_1_t2804637950 *, const MethodInfo*))LinkedList_1_get_Last_m270176030_gshared)(__this, method)
