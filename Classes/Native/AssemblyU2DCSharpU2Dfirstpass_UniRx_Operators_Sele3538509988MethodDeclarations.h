﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.SelectManyObservable`2/SelectManyEnumerableObserver<System.Object,UniRx.Unit>
struct SelectManyEnumerableObserver_t3538509988;
// UniRx.Operators.SelectManyObservable`2<System.Object,UniRx.Unit>
struct SelectManyObservable_2_t2284173584;
// UniRx.IObserver`1<UniRx.Unit>
struct IObserver_1_t475317645;
// System.IDisposable
struct IDisposable_t1628921374;
// System.Object
struct Il2CppObject;
// System.Exception
struct Exception_t1967233988;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_Exception1967233988.h"

// System.Void UniRx.Operators.SelectManyObservable`2/SelectManyEnumerableObserver<System.Object,UniRx.Unit>::.ctor(UniRx.Operators.SelectManyObservable`2<TSource,TResult>,UniRx.IObserver`1<TResult>,System.IDisposable)
extern "C"  void SelectManyEnumerableObserver__ctor_m3171998844_gshared (SelectManyEnumerableObserver_t3538509988 * __this, SelectManyObservable_2_t2284173584 * ___parent0, Il2CppObject* ___observer1, Il2CppObject * ___cancel2, const MethodInfo* method);
#define SelectManyEnumerableObserver__ctor_m3171998844(__this, ___parent0, ___observer1, ___cancel2, method) ((  void (*) (SelectManyEnumerableObserver_t3538509988 *, SelectManyObservable_2_t2284173584 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))SelectManyEnumerableObserver__ctor_m3171998844_gshared)(__this, ___parent0, ___observer1, ___cancel2, method)
// System.IDisposable UniRx.Operators.SelectManyObservable`2/SelectManyEnumerableObserver<System.Object,UniRx.Unit>::Run()
extern "C"  Il2CppObject * SelectManyEnumerableObserver_Run_m2699538498_gshared (SelectManyEnumerableObserver_t3538509988 * __this, const MethodInfo* method);
#define SelectManyEnumerableObserver_Run_m2699538498(__this, method) ((  Il2CppObject * (*) (SelectManyEnumerableObserver_t3538509988 *, const MethodInfo*))SelectManyEnumerableObserver_Run_m2699538498_gshared)(__this, method)
// System.Void UniRx.Operators.SelectManyObservable`2/SelectManyEnumerableObserver<System.Object,UniRx.Unit>::OnNext(TSource)
extern "C"  void SelectManyEnumerableObserver_OnNext_m2709393259_gshared (SelectManyEnumerableObserver_t3538509988 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define SelectManyEnumerableObserver_OnNext_m2709393259(__this, ___value0, method) ((  void (*) (SelectManyEnumerableObserver_t3538509988 *, Il2CppObject *, const MethodInfo*))SelectManyEnumerableObserver_OnNext_m2709393259_gshared)(__this, ___value0, method)
// System.Void UniRx.Operators.SelectManyObservable`2/SelectManyEnumerableObserver<System.Object,UniRx.Unit>::OnError(System.Exception)
extern "C"  void SelectManyEnumerableObserver_OnError_m449234197_gshared (SelectManyEnumerableObserver_t3538509988 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method);
#define SelectManyEnumerableObserver_OnError_m449234197(__this, ___error0, method) ((  void (*) (SelectManyEnumerableObserver_t3538509988 *, Exception_t1967233988 *, const MethodInfo*))SelectManyEnumerableObserver_OnError_m449234197_gshared)(__this, ___error0, method)
// System.Void UniRx.Operators.SelectManyObservable`2/SelectManyEnumerableObserver<System.Object,UniRx.Unit>::OnCompleted()
extern "C"  void SelectManyEnumerableObserver_OnCompleted_m3354177512_gshared (SelectManyEnumerableObserver_t3538509988 * __this, const MethodInfo* method);
#define SelectManyEnumerableObserver_OnCompleted_m3354177512(__this, method) ((  void (*) (SelectManyEnumerableObserver_t3538509988 *, const MethodInfo*))SelectManyEnumerableObserver_OnCompleted_m3354177512_gshared)(__this, method)
