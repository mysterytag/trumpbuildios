﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.PlayFabClientAPI/StartPurchaseResponseCallback
struct StartPurchaseResponseCallback_t2513748265;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;
// PlayFab.ClientModels.StartPurchaseRequest
struct StartPurchaseRequest_t2105128380;
// PlayFab.ClientModels.StartPurchaseResult
struct StartPurchaseResult_t1765623152;
// PlayFab.PlayFabError
struct PlayFabError_t750598646;
// System.IAsyncResult
struct IAsyncResult_t537683269;
// System.AsyncCallback
struct AsyncCallback_t1363551830;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_IntPtr676692020.h"
#include "mscorlib_System_String968488902.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_StartPurcha2105128380.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_StartPurcha1765623152.h"
#include "AssemblyU2DCSharpU2Dfirstpass_PlayFab_PlayFabError750598646.h"
#include "mscorlib_System_AsyncCallback1363551830.h"

// System.Void PlayFab.PlayFabClientAPI/StartPurchaseResponseCallback::.ctor(System.Object,System.IntPtr)
extern "C"  void StartPurchaseResponseCallback__ctor_m181739832 (StartPurchaseResponseCallback_t2513748265 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/StartPurchaseResponseCallback::Invoke(System.String,System.Int32,PlayFab.ClientModels.StartPurchaseRequest,PlayFab.ClientModels.StartPurchaseResult,PlayFab.PlayFabError,System.Object)
extern "C"  void StartPurchaseResponseCallback_Invoke_m114861033 (StartPurchaseResponseCallback_t2513748265 * __this, String_t* ___urlPath0, int32_t ___callId1, StartPurchaseRequest_t2105128380 * ___request2, StartPurchaseResult_t1765623152 * ___result3, PlayFabError_t750598646 * ___error4, Il2CppObject * ___customData5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
extern "C" void pinvoke_delegate_wrapper_StartPurchaseResponseCallback_t2513748265(Il2CppObject* delegate, String_t* ___urlPath0, int32_t ___callId1, StartPurchaseRequest_t2105128380 * ___request2, StartPurchaseResult_t1765623152 * ___result3, PlayFabError_t750598646 * ___error4, Il2CppObject * ___customData5);
// System.IAsyncResult PlayFab.PlayFabClientAPI/StartPurchaseResponseCallback::BeginInvoke(System.String,System.Int32,PlayFab.ClientModels.StartPurchaseRequest,PlayFab.ClientModels.StartPurchaseResult,PlayFab.PlayFabError,System.Object,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * StartPurchaseResponseCallback_BeginInvoke_m4274120982 (StartPurchaseResponseCallback_t2513748265 * __this, String_t* ___urlPath0, int32_t ___callId1, StartPurchaseRequest_t2105128380 * ___request2, StartPurchaseResult_t1765623152 * ___result3, PlayFabError_t750598646 * ___error4, Il2CppObject * ___customData5, AsyncCallback_t1363551830 * ___callback6, Il2CppObject * ___object7, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/StartPurchaseResponseCallback::EndInvoke(System.IAsyncResult)
extern "C"  void StartPurchaseResponseCallback_EndInvoke_m563736392 (StartPurchaseResponseCallback_t2513748265 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
