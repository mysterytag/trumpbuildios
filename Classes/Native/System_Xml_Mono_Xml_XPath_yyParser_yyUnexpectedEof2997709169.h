﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "System_Xml_Mono_Xml_XPath_yyParser_yyException1136059093.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.XPath.yyParser.yyUnexpectedEof
struct  yyUnexpectedEof_t2997709169  : public yyException_t1136059093
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
