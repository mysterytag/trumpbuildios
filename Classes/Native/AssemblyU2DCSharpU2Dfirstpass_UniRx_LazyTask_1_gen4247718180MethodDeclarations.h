﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.LazyTask`1<System.Int32>
struct LazyTask_1_t4247718180;
// UniRx.IObservable`1<System.Int32>
struct IObservable_1_t2606213151;
// System.Exception
struct Exception_t1967233988;
// UnityEngine.Coroutine
struct Coroutine_t2246592261;
struct Coroutine_t2246592261_marshaled_pinvoke;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Exception1967233988.h"

// System.Void UniRx.LazyTask`1<System.Int32>::.ctor(UniRx.IObservable`1<T>)
extern "C"  void LazyTask_1__ctor_m152437241_gshared (LazyTask_1_t4247718180 * __this, Il2CppObject* ___source0, const MethodInfo* method);
#define LazyTask_1__ctor_m152437241(__this, ___source0, method) ((  void (*) (LazyTask_1_t4247718180 *, Il2CppObject*, const MethodInfo*))LazyTask_1__ctor_m152437241_gshared)(__this, ___source0, method)
// T UniRx.LazyTask`1<System.Int32>::get_Result()
extern "C"  int32_t LazyTask_1_get_Result_m2773840719_gshared (LazyTask_1_t4247718180 * __this, const MethodInfo* method);
#define LazyTask_1_get_Result_m2773840719(__this, method) ((  int32_t (*) (LazyTask_1_t4247718180 *, const MethodInfo*))LazyTask_1_get_Result_m2773840719_gshared)(__this, method)
// System.Exception UniRx.LazyTask`1<System.Int32>::get_Exception()
extern "C"  Exception_t1967233988 * LazyTask_1_get_Exception_m2829354473_gshared (LazyTask_1_t4247718180 * __this, const MethodInfo* method);
#define LazyTask_1_get_Exception_m2829354473(__this, method) ((  Exception_t1967233988 * (*) (LazyTask_1_t4247718180 *, const MethodInfo*))LazyTask_1_get_Exception_m2829354473_gshared)(__this, method)
// System.Void UniRx.LazyTask`1<System.Int32>::set_Exception(System.Exception)
extern "C"  void LazyTask_1_set_Exception_m4039632538_gshared (LazyTask_1_t4247718180 * __this, Exception_t1967233988 * ___value0, const MethodInfo* method);
#define LazyTask_1_set_Exception_m4039632538(__this, ___value0, method) ((  void (*) (LazyTask_1_t4247718180 *, Exception_t1967233988 *, const MethodInfo*))LazyTask_1_set_Exception_m4039632538_gshared)(__this, ___value0, method)
// UnityEngine.Coroutine UniRx.LazyTask`1<System.Int32>::Start()
extern "C"  Coroutine_t2246592261 * LazyTask_1_Start_m990669032_gshared (LazyTask_1_t4247718180 * __this, const MethodInfo* method);
#define LazyTask_1_Start_m990669032(__this, method) ((  Coroutine_t2246592261 * (*) (LazyTask_1_t4247718180 *, const MethodInfo*))LazyTask_1_Start_m990669032_gshared)(__this, method)
// System.String UniRx.LazyTask`1<System.Int32>::ToString()
extern "C"  String_t* LazyTask_1_ToString_m1613548729_gshared (LazyTask_1_t4247718180 * __this, const MethodInfo* method);
#define LazyTask_1_ToString_m1613548729(__this, method) ((  String_t* (*) (LazyTask_1_t4247718180 *, const MethodInfo*))LazyTask_1_ToString_m1613548729_gshared)(__this, method)
// UniRx.LazyTask`1<T> UniRx.LazyTask`1<System.Int32>::FromResult(T)
extern "C"  LazyTask_1_t4247718180 * LazyTask_1_FromResult_m1512477278_gshared (Il2CppObject * __this /* static, unused */, int32_t ___value0, const MethodInfo* method);
#define LazyTask_1_FromResult_m1512477278(__this /* static, unused */, ___value0, method) ((  LazyTask_1_t4247718180 * (*) (Il2CppObject * /* static, unused */, int32_t, const MethodInfo*))LazyTask_1_FromResult_m1512477278_gshared)(__this /* static, unused */, ___value0, method)
// System.Void UniRx.LazyTask`1<System.Int32>::<Start>m__A6(T)
extern "C"  void LazyTask_1_U3CStartU3Em__A6_m3356978090_gshared (LazyTask_1_t4247718180 * __this, int32_t ___x0, const MethodInfo* method);
#define LazyTask_1_U3CStartU3Em__A6_m3356978090(__this, ___x0, method) ((  void (*) (LazyTask_1_t4247718180 *, int32_t, const MethodInfo*))LazyTask_1_U3CStartU3Em__A6_m3356978090_gshared)(__this, ___x0, method)
// System.Void UniRx.LazyTask`1<System.Int32>::<Start>m__A7(System.Exception)
extern "C"  void LazyTask_1_U3CStartU3Em__A7_m3828460197_gshared (LazyTask_1_t4247718180 * __this, Exception_t1967233988 * ___ex0, const MethodInfo* method);
#define LazyTask_1_U3CStartU3Em__A7_m3828460197(__this, ___ex0, method) ((  void (*) (LazyTask_1_t4247718180 *, Exception_t1967233988 *, const MethodInfo*))LazyTask_1_U3CStartU3Em__A7_m3828460197_gshared)(__this, ___ex0, method)
