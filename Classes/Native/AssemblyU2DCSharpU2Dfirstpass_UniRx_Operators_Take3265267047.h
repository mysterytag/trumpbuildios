﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UniRx.IObservable`1<System.Object>
struct IObservable_1_t595904784;
// UniRx.IScheduler
struct IScheduler_t2938318244;

#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Oper4196218687.h"
#include "mscorlib_System_TimeSpan763862892.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Operators.TakeLastObservable`1<System.Object>
struct  TakeLastObservable_1_t3265267047  : public OperatorObservableBase_1_t4196218687
{
public:
	// UniRx.IObservable`1<T> UniRx.Operators.TakeLastObservable`1::source
	Il2CppObject* ___source_1;
	// System.Int32 UniRx.Operators.TakeLastObservable`1::count
	int32_t ___count_2;
	// System.TimeSpan UniRx.Operators.TakeLastObservable`1::duration
	TimeSpan_t763862892  ___duration_3;
	// UniRx.IScheduler UniRx.Operators.TakeLastObservable`1::scheduler
	Il2CppObject * ___scheduler_4;

public:
	inline static int32_t get_offset_of_source_1() { return static_cast<int32_t>(offsetof(TakeLastObservable_1_t3265267047, ___source_1)); }
	inline Il2CppObject* get_source_1() const { return ___source_1; }
	inline Il2CppObject** get_address_of_source_1() { return &___source_1; }
	inline void set_source_1(Il2CppObject* value)
	{
		___source_1 = value;
		Il2CppCodeGenWriteBarrier(&___source_1, value);
	}

	inline static int32_t get_offset_of_count_2() { return static_cast<int32_t>(offsetof(TakeLastObservable_1_t3265267047, ___count_2)); }
	inline int32_t get_count_2() const { return ___count_2; }
	inline int32_t* get_address_of_count_2() { return &___count_2; }
	inline void set_count_2(int32_t value)
	{
		___count_2 = value;
	}

	inline static int32_t get_offset_of_duration_3() { return static_cast<int32_t>(offsetof(TakeLastObservable_1_t3265267047, ___duration_3)); }
	inline TimeSpan_t763862892  get_duration_3() const { return ___duration_3; }
	inline TimeSpan_t763862892 * get_address_of_duration_3() { return &___duration_3; }
	inline void set_duration_3(TimeSpan_t763862892  value)
	{
		___duration_3 = value;
	}

	inline static int32_t get_offset_of_scheduler_4() { return static_cast<int32_t>(offsetof(TakeLastObservable_1_t3265267047, ___scheduler_4)); }
	inline Il2CppObject * get_scheduler_4() const { return ___scheduler_4; }
	inline Il2CppObject ** get_address_of_scheduler_4() { return &___scheduler_4; }
	inline void set_scheduler_4(Il2CppObject * value)
	{
		___scheduler_4 = value;
		Il2CppCodeGenWriteBarrier(&___scheduler_4, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
