﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.UI.Image
struct Image_t3354615620;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ReactiveUI/<SetFill>c__AnonStorey127
struct  U3CSetFillU3Ec__AnonStorey127_t1032993156  : public Il2CppObject
{
public:
	// UnityEngine.UI.Image ReactiveUI/<SetFill>c__AnonStorey127::obj
	Image_t3354615620 * ___obj_0;

public:
	inline static int32_t get_offset_of_obj_0() { return static_cast<int32_t>(offsetof(U3CSetFillU3Ec__AnonStorey127_t1032993156, ___obj_0)); }
	inline Image_t3354615620 * get_obj_0() const { return ___obj_0; }
	inline Image_t3354615620 ** get_address_of_obj_0() { return &___obj_0; }
	inline void set_obj_0(Image_t3354615620 * value)
	{
		___obj_0 = value;
		Il2CppCodeGenWriteBarrier(&___obj_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
