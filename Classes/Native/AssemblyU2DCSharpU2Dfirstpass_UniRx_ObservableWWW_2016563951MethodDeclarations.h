﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.ObservableWWW/<Fetch>c__Iterator20
struct U3CFetchU3Ec__Iterator20_t2016563951;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void UniRx.ObservableWWW/<Fetch>c__Iterator20::.ctor()
extern "C"  void U3CFetchU3Ec__Iterator20__ctor_m1884724461 (U3CFetchU3Ec__Iterator20_t2016563951 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object UniRx.ObservableWWW/<Fetch>c__Iterator20::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CFetchU3Ec__Iterator20_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1972252431 (U3CFetchU3Ec__Iterator20_t2016563951 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object UniRx.ObservableWWW/<Fetch>c__Iterator20::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CFetchU3Ec__Iterator20_System_Collections_IEnumerator_get_Current_m3838193827 (U3CFetchU3Ec__Iterator20_t2016563951 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UniRx.ObservableWWW/<Fetch>c__Iterator20::MoveNext()
extern "C"  bool U3CFetchU3Ec__Iterator20_MoveNext_m3171663031 (U3CFetchU3Ec__Iterator20_t2016563951 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.ObservableWWW/<Fetch>c__Iterator20::Dispose()
extern "C"  void U3CFetchU3Ec__Iterator20_Dispose_m2937258474 (U3CFetchU3Ec__Iterator20_t2016563951 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.ObservableWWW/<Fetch>c__Iterator20::Reset()
extern "C"  void U3CFetchU3Ec__Iterator20_Reset_m3826124698 (U3CFetchU3Ec__Iterator20_t2016563951 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
