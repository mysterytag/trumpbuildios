﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PlayFab.Json.JsonLegacyTest/JsonTest
struct  JsonTest_t2384680378  : public Il2CppObject
{
public:
	// System.Boolean PlayFab.Json.JsonLegacyTest/JsonTest::TestBool
	bool ___TestBool_0;

public:
	inline static int32_t get_offset_of_TestBool_0() { return static_cast<int32_t>(offsetof(JsonTest_t2384680378, ___TestBool_0)); }
	inline bool get_TestBool_0() const { return ___TestBool_0; }
	inline bool* get_address_of_TestBool_0() { return &___TestBool_0; }
	inline void set_TestBool_0(bool value)
	{
		___TestBool_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
