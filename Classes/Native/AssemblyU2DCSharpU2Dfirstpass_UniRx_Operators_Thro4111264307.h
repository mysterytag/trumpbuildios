﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UniRx.Operators.ThrottleObservable`1<System.Object>
struct ThrottleObservable_1_t1217300940;
// System.Object
struct Il2CppObject;
// UniRx.SerialDisposable
struct SerialDisposable_t2547852742;

#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Oper1187768149.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Operators.ThrottleObservable`1/Throttle<System.Object>
struct  Throttle_t4111264307  : public OperatorObserverBase_2_t1187768149
{
public:
	// UniRx.Operators.ThrottleObservable`1<T> UniRx.Operators.ThrottleObservable`1/Throttle::parent
	ThrottleObservable_1_t1217300940 * ___parent_2;
	// System.Object UniRx.Operators.ThrottleObservable`1/Throttle::gate
	Il2CppObject * ___gate_3;
	// T UniRx.Operators.ThrottleObservable`1/Throttle::latestValue
	Il2CppObject * ___latestValue_4;
	// System.Boolean UniRx.Operators.ThrottleObservable`1/Throttle::hasValue
	bool ___hasValue_5;
	// UniRx.SerialDisposable UniRx.Operators.ThrottleObservable`1/Throttle::cancelable
	SerialDisposable_t2547852742 * ___cancelable_6;
	// System.UInt64 UniRx.Operators.ThrottleObservable`1/Throttle::id
	uint64_t ___id_7;

public:
	inline static int32_t get_offset_of_parent_2() { return static_cast<int32_t>(offsetof(Throttle_t4111264307, ___parent_2)); }
	inline ThrottleObservable_1_t1217300940 * get_parent_2() const { return ___parent_2; }
	inline ThrottleObservable_1_t1217300940 ** get_address_of_parent_2() { return &___parent_2; }
	inline void set_parent_2(ThrottleObservable_1_t1217300940 * value)
	{
		___parent_2 = value;
		Il2CppCodeGenWriteBarrier(&___parent_2, value);
	}

	inline static int32_t get_offset_of_gate_3() { return static_cast<int32_t>(offsetof(Throttle_t4111264307, ___gate_3)); }
	inline Il2CppObject * get_gate_3() const { return ___gate_3; }
	inline Il2CppObject ** get_address_of_gate_3() { return &___gate_3; }
	inline void set_gate_3(Il2CppObject * value)
	{
		___gate_3 = value;
		Il2CppCodeGenWriteBarrier(&___gate_3, value);
	}

	inline static int32_t get_offset_of_latestValue_4() { return static_cast<int32_t>(offsetof(Throttle_t4111264307, ___latestValue_4)); }
	inline Il2CppObject * get_latestValue_4() const { return ___latestValue_4; }
	inline Il2CppObject ** get_address_of_latestValue_4() { return &___latestValue_4; }
	inline void set_latestValue_4(Il2CppObject * value)
	{
		___latestValue_4 = value;
		Il2CppCodeGenWriteBarrier(&___latestValue_4, value);
	}

	inline static int32_t get_offset_of_hasValue_5() { return static_cast<int32_t>(offsetof(Throttle_t4111264307, ___hasValue_5)); }
	inline bool get_hasValue_5() const { return ___hasValue_5; }
	inline bool* get_address_of_hasValue_5() { return &___hasValue_5; }
	inline void set_hasValue_5(bool value)
	{
		___hasValue_5 = value;
	}

	inline static int32_t get_offset_of_cancelable_6() { return static_cast<int32_t>(offsetof(Throttle_t4111264307, ___cancelable_6)); }
	inline SerialDisposable_t2547852742 * get_cancelable_6() const { return ___cancelable_6; }
	inline SerialDisposable_t2547852742 ** get_address_of_cancelable_6() { return &___cancelable_6; }
	inline void set_cancelable_6(SerialDisposable_t2547852742 * value)
	{
		___cancelable_6 = value;
		Il2CppCodeGenWriteBarrier(&___cancelable_6, value);
	}

	inline static int32_t get_offset_of_id_7() { return static_cast<int32_t>(offsetof(Throttle_t4111264307, ___id_7)); }
	inline uint64_t get_id_7() const { return ___id_7; }
	inline uint64_t* get_address_of_id_7() { return &___id_7; }
	inline void set_id_7(uint64_t value)
	{
		___id_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
