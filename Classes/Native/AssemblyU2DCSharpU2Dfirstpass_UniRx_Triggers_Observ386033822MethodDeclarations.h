﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Triggers.ObservableSubmitTrigger
struct ObservableSubmitTrigger_t386033822;
// UnityEngine.EventSystems.BaseEventData
struct BaseEventData_t3547103726;
// UniRx.IObservable`1<UnityEngine.EventSystems.BaseEventData>
struct IObservable_1_t3305902090;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_BaseEventD3547103726.h"

// System.Void UniRx.Triggers.ObservableSubmitTrigger::.ctor()
extern "C"  void ObservableSubmitTrigger__ctor_m2987167757 (ObservableSubmitTrigger_t386033822 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.Triggers.ObservableSubmitTrigger::UnityEngine.EventSystems.ISubmitHandler.OnSubmit(UnityEngine.EventSystems.BaseEventData)
extern "C"  void ObservableSubmitTrigger_UnityEngine_EventSystems_ISubmitHandler_OnSubmit_m1368882560 (ObservableSubmitTrigger_t386033822 * __this, BaseEventData_t3547103726 * ___eventData0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UniRx.IObservable`1<UnityEngine.EventSystems.BaseEventData> UniRx.Triggers.ObservableSubmitTrigger::OnSubmitAsObservable()
extern "C"  Il2CppObject* ObservableSubmitTrigger_OnSubmitAsObservable_m3991003317 (ObservableSubmitTrigger_t386033822 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.Triggers.ObservableSubmitTrigger::RaiseOnCompletedOnDestroy()
extern "C"  void ObservableSubmitTrigger_RaiseOnCompletedOnDestroy_m4294827718 (ObservableSubmitTrigger_t386033822 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
