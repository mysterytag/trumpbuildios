﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String968488902.h"

// System.Void Params::.cctor()
extern "C"  void Params__cctor_m3345647768 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Params::ResolveForPlatform(System.String)
extern "C"  String_t* Params_ResolveForPlatform_m1465200480 (Il2CppObject * __this /* static, unused */, String_t* ___key0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Params::Resolve(System.String)
extern "C"  String_t* Params_Resolve_m1418989318 (Il2CppObject * __this /* static, unused */, String_t* ___key0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Params::Initialize()
extern "C"  void Params_Initialize_m4223968767 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Params::<Initialize>m__11B(System.String)
extern "C"  String_t* Params_U3CInitializeU3Em__11B_m1409712169 (Il2CppObject * __this /* static, unused */, String_t* ___s0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Params::<Initialize>m__11C(System.String)
extern "C"  bool Params_U3CInitializeU3Em__11C_m1650049175 (Il2CppObject * __this /* static, unused */, String_t* ___s0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Params::<Initialize>m__11D(System.String)
extern "C"  bool Params_U3CInitializeU3Em__11D_m1139514998 (Il2CppObject * __this /* static, unused */, String_t* ___s0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
