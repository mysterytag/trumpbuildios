﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "System_Core_System_Func_2_gen1101125214MethodDeclarations.h"

// System.Void System.Func`2<UniRx.Tuple`2<System.String,System.String>,System.Boolean>::.ctor(System.Object,System.IntPtr)
#define Func_2__ctor_m3915115931(__this, ___object0, ___method1, method) ((  void (*) (Func_2_t2285987078 *, Il2CppObject *, IntPtr_t, const MethodInfo*))Func_2__ctor_m1978381903_gshared)(__this, ___object0, ___method1, method)
// TResult System.Func`2<UniRx.Tuple`2<System.String,System.String>,System.Boolean>::Invoke(T)
#define Func_2_Invoke_m2450272855(__this, ___arg10, method) ((  bool (*) (Func_2_t2285987078 *, Tuple_2_t3445990771 , const MethodInfo*))Func_2_Invoke_m621375027_gshared)(__this, ___arg10, method)
// System.IAsyncResult System.Func`2<UniRx.Tuple`2<System.String,System.String>,System.Boolean>::BeginInvoke(T,System.AsyncCallback,System.Object)
#define Func_2_BeginInvoke_m3264311046(__this, ___arg10, ___callback1, ___object2, method) ((  Il2CppObject * (*) (Func_2_t2285987078 *, Tuple_2_t3445990771 , AsyncCallback_t1363551830 *, Il2CppObject *, const MethodInfo*))Func_2_BeginInvoke_m2552012514_gshared)(__this, ___arg10, ___callback1, ___object2, method)
// TResult System.Func`2<UniRx.Tuple`2<System.String,System.String>,System.Boolean>::EndInvoke(System.IAsyncResult)
#define Func_2_EndInvoke_m2184035805(__this, ___result0, method) ((  bool (*) (Func_2_t2285987078 *, Il2CppObject *, const MethodInfo*))Func_2_EndInvoke_m205505409_gshared)(__this, ___result0, method)
