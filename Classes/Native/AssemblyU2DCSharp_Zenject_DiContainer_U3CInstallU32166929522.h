﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Zenject.IInstaller
struct IInstaller_t2455223476;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.DiContainer/<Install>c__AnonStorey18A
struct  U3CInstallU3Ec__AnonStorey18A_t2166929522  : public Il2CppObject
{
public:
	// Zenject.IInstaller Zenject.DiContainer/<Install>c__AnonStorey18A::installer
	Il2CppObject * ___installer_0;

public:
	inline static int32_t get_offset_of_installer_0() { return static_cast<int32_t>(offsetof(U3CInstallU3Ec__AnonStorey18A_t2166929522, ___installer_0)); }
	inline Il2CppObject * get_installer_0() const { return ___installer_0; }
	inline Il2CppObject ** get_address_of_installer_0() { return &___installer_0; }
	inline void set_installer_0(Il2CppObject * value)
	{
		___installer_0 = value;
		Il2CppCodeGenWriteBarrier(&___installer_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
