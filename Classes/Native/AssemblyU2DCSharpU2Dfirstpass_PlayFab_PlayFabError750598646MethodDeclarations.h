﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.PlayFabError
struct PlayFabError_t750598646;

#include "codegen/il2cpp-codegen.h"

// System.Void PlayFab.PlayFabError::.ctor()
extern "C"  void PlayFabError__ctor_m1421089695 (PlayFabError_t750598646 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
