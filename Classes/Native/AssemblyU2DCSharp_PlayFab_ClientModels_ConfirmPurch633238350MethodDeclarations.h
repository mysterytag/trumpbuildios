﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.ClientModels.ConfirmPurchaseResult
struct ConfirmPurchaseResult_t633238350;
// System.String
struct String_t;
// System.Collections.Generic.List`1<PlayFab.ClientModels.ItemInstance>
struct List_1_t1322103281;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String968488902.h"
#include "mscorlib_System_DateTime339033936.h"

// System.Void PlayFab.ClientModels.ConfirmPurchaseResult::.ctor()
extern "C"  void ConfirmPurchaseResult__ctor_m3049288603 (ConfirmPurchaseResult_t633238350 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PlayFab.ClientModels.ConfirmPurchaseResult::get_OrderId()
extern "C"  String_t* ConfirmPurchaseResult_get_OrderId_m407665430 (ConfirmPurchaseResult_t633238350 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.ConfirmPurchaseResult::set_OrderId(System.String)
extern "C"  void ConfirmPurchaseResult_set_OrderId_m1130922301 (ConfirmPurchaseResult_t633238350 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.DateTime PlayFab.ClientModels.ConfirmPurchaseResult::get_PurchaseDate()
extern "C"  DateTime_t339033936  ConfirmPurchaseResult_get_PurchaseDate_m1385405946 (ConfirmPurchaseResult_t633238350 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.ConfirmPurchaseResult::set_PurchaseDate(System.DateTime)
extern "C"  void ConfirmPurchaseResult_set_PurchaseDate_m78305603 (ConfirmPurchaseResult_t633238350 * __this, DateTime_t339033936  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<PlayFab.ClientModels.ItemInstance> PlayFab.ClientModels.ConfirmPurchaseResult::get_Items()
extern "C"  List_1_t1322103281 * ConfirmPurchaseResult_get_Items_m2678418825 (ConfirmPurchaseResult_t633238350 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.ConfirmPurchaseResult::set_Items(System.Collections.Generic.List`1<PlayFab.ClientModels.ItemInstance>)
extern "C"  void ConfirmPurchaseResult_set_Items_m101409798 (ConfirmPurchaseResult_t633238350 * __this, List_1_t1322103281 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
