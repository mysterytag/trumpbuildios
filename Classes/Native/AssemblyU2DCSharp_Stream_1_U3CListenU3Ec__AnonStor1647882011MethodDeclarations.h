﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Stream`1/<Listen>c__AnonStorey128<UniRx.CollectionAddEvent`1<System.Object>>
struct U3CListenU3Ec__AnonStorey128_t1647882011;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_CollectionAddE2416521987.h"

// System.Void Stream`1/<Listen>c__AnonStorey128<UniRx.CollectionAddEvent`1<System.Object>>::.ctor()
extern "C"  void U3CListenU3Ec__AnonStorey128__ctor_m1700916422_gshared (U3CListenU3Ec__AnonStorey128_t1647882011 * __this, const MethodInfo* method);
#define U3CListenU3Ec__AnonStorey128__ctor_m1700916422(__this, method) ((  void (*) (U3CListenU3Ec__AnonStorey128_t1647882011 *, const MethodInfo*))U3CListenU3Ec__AnonStorey128__ctor_m1700916422_gshared)(__this, method)
// System.Void Stream`1/<Listen>c__AnonStorey128<UniRx.CollectionAddEvent`1<System.Object>>::<>m__1B9(T)
extern "C"  void U3CListenU3Ec__AnonStorey128_U3CU3Em__1B9_m1126481123_gshared (U3CListenU3Ec__AnonStorey128_t1647882011 * __this, CollectionAddEvent_1_t2416521987  ____0, const MethodInfo* method);
#define U3CListenU3Ec__AnonStorey128_U3CU3Em__1B9_m1126481123(__this, ____0, method) ((  void (*) (U3CListenU3Ec__AnonStorey128_t1647882011 *, CollectionAddEvent_1_t2416521987 , const MethodInfo*))U3CListenU3Ec__AnonStorey128_U3CU3Em__1B9_m1126481123_gshared)(__this, ____0, method)
