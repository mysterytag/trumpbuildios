﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Zenject.IFactoryUntypedBinder`1/<ToMethod>c__AnonStorey17F<System.Object>
struct U3CToMethodU3Ec__AnonStorey17F_t1801356992;
// Zenject.IFactoryUntyped`1<System.Object>
struct IFactoryUntyped_1_t1534379775;
// Zenject.InjectContext
struct InjectContext_t3456483891;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Zenject_InjectContext3456483891.h"

// System.Void Zenject.IFactoryUntypedBinder`1/<ToMethod>c__AnonStorey17F<System.Object>::.ctor()
extern "C"  void U3CToMethodU3Ec__AnonStorey17F__ctor_m3752727377_gshared (U3CToMethodU3Ec__AnonStorey17F_t1801356992 * __this, const MethodInfo* method);
#define U3CToMethodU3Ec__AnonStorey17F__ctor_m3752727377(__this, method) ((  void (*) (U3CToMethodU3Ec__AnonStorey17F_t1801356992 *, const MethodInfo*))U3CToMethodU3Ec__AnonStorey17F__ctor_m3752727377_gshared)(__this, method)
// Zenject.IFactoryUntyped`1<TContract> Zenject.IFactoryUntypedBinder`1/<ToMethod>c__AnonStorey17F<System.Object>::<>m__2A9(Zenject.InjectContext)
extern "C"  Il2CppObject* U3CToMethodU3Ec__AnonStorey17F_U3CU3Em__2A9_m1647927756_gshared (U3CToMethodU3Ec__AnonStorey17F_t1801356992 * __this, InjectContext_t3456483891 * ___c0, const MethodInfo* method);
#define U3CToMethodU3Ec__AnonStorey17F_U3CU3Em__2A9_m1647927756(__this, ___c0, method) ((  Il2CppObject* (*) (U3CToMethodU3Ec__AnonStorey17F_t1801356992 *, InjectContext_t3456483891 *, const MethodInfo*))U3CToMethodU3Ec__AnonStorey17F_U3CU3Em__2A9_m1647927756_gshared)(__this, ___c0, method)
