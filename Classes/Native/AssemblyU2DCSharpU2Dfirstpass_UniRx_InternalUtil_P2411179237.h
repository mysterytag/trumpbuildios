﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UniRx.InternalUtil.PriorityQueue`1/IndexedItem<UniRx.InternalUtil.ScheduledItem>[]
struct IndexedItemU5BU5D_t1528605875;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.InternalUtil.PriorityQueue`1<UniRx.InternalUtil.ScheduledItem>
struct  PriorityQueue_1_t2411179237  : public Il2CppObject
{
public:
	// UniRx.InternalUtil.PriorityQueue`1/IndexedItem<T>[] UniRx.InternalUtil.PriorityQueue`1::_items
	IndexedItemU5BU5D_t1528605875* ____items_1;
	// System.Int32 UniRx.InternalUtil.PriorityQueue`1::_size
	int32_t ____size_2;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(PriorityQueue_1_t2411179237, ____items_1)); }
	inline IndexedItemU5BU5D_t1528605875* get__items_1() const { return ____items_1; }
	inline IndexedItemU5BU5D_t1528605875** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(IndexedItemU5BU5D_t1528605875* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier(&____items_1, value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(PriorityQueue_1_t2411179237, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}
};

struct PriorityQueue_1_t2411179237_StaticFields
{
public:
	// System.Int64 UniRx.InternalUtil.PriorityQueue`1::_count
	int64_t ____count_0;

public:
	inline static int32_t get_offset_of__count_0() { return static_cast<int32_t>(offsetof(PriorityQueue_1_t2411179237_StaticFields, ____count_0)); }
	inline int64_t get__count_0() const { return ____count_0; }
	inline int64_t* get_address_of__count_0() { return &____count_0; }
	inline void set__count_0(int64_t value)
	{
		____count_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
