﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.ComponentModel.DoWorkEventArgs
struct DoWorkEventArgs_t282241604;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"

// System.Void System.ComponentModel.DoWorkEventArgs::.ctor(System.Object)
extern "C"  void DoWorkEventArgs__ctor_m1862664002 (DoWorkEventArgs_t282241604 * __this, Il2CppObject * ___argument0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.ComponentModel.DoWorkEventArgs::get_Argument()
extern "C"  Il2CppObject * DoWorkEventArgs_get_Argument_m919636131 (DoWorkEventArgs_t282241604 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.ComponentModel.DoWorkEventArgs::get_Result()
extern "C"  Il2CppObject * DoWorkEventArgs_get_Result_m1035853635 (DoWorkEventArgs_t282241604 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.ComponentModel.DoWorkEventArgs::set_Result(System.Object)
extern "C"  void DoWorkEventArgs_set_Result_m784017992 (DoWorkEventArgs_t282241604 * __this, Il2CppObject * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.ComponentModel.DoWorkEventArgs::get_Cancel()
extern "C"  bool DoWorkEventArgs_get_Cancel_m2900005151 (DoWorkEventArgs_t282241604 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.ComponentModel.DoWorkEventArgs::set_Cancel(System.Boolean)
extern "C"  void DoWorkEventArgs_set_Cancel_m623734688 (DoWorkEventArgs_t282241604 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
