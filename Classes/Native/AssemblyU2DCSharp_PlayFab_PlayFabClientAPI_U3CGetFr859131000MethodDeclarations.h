﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.PlayFabClientAPI/<GetFriendLeaderboard>c__AnonStorey8B
struct U3CGetFriendLeaderboardU3Ec__AnonStorey8B_t859131000;
// PlayFab.CallRequestContainer
struct CallRequestContainer_t432318609;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharpU2Dfirstpass_PlayFab_CallRequestCo432318609.h"

// System.Void PlayFab.PlayFabClientAPI/<GetFriendLeaderboard>c__AnonStorey8B::.ctor()
extern "C"  void U3CGetFriendLeaderboardU3Ec__AnonStorey8B__ctor_m361655867 (U3CGetFriendLeaderboardU3Ec__AnonStorey8B_t859131000 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/<GetFriendLeaderboard>c__AnonStorey8B::<>m__78(PlayFab.CallRequestContainer)
extern "C"  void U3CGetFriendLeaderboardU3Ec__AnonStorey8B_U3CU3Em__78_m725920506 (U3CGetFriendLeaderboardU3Ec__AnonStorey8B_t859131000 * __this, CallRequestContainer_t432318609 * ___requestContainer0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
