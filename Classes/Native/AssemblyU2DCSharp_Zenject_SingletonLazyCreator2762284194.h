﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Zenject.DiContainer
struct DiContainer_t2383114449;
// Zenject.SingletonProviderMap
struct SingletonProviderMap_t1557411893;
// System.Func`2<Zenject.InjectContext,System.Object>
struct Func_2_t2621245597;
// Zenject.SingletonId
struct SingletonId_t1838183899;
// System.Object
struct Il2CppObject;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.SingletonLazyCreator
struct  SingletonLazyCreator_t2762284194  : public Il2CppObject
{
public:
	// Zenject.DiContainer Zenject.SingletonLazyCreator::_container
	DiContainer_t2383114449 * ____container_0;
	// Zenject.SingletonProviderMap Zenject.SingletonLazyCreator::_owner
	SingletonProviderMap_t1557411893 * ____owner_1;
	// System.Func`2<Zenject.InjectContext,System.Object> Zenject.SingletonLazyCreator::_createMethod
	Func_2_t2621245597 * ____createMethod_2;
	// Zenject.SingletonId Zenject.SingletonLazyCreator::_id
	SingletonId_t1838183899 * ____id_3;
	// System.Int32 Zenject.SingletonLazyCreator::_referenceCount
	int32_t ____referenceCount_4;
	// System.Object Zenject.SingletonLazyCreator::_instance
	Il2CppObject * ____instance_5;
	// System.Boolean Zenject.SingletonLazyCreator::_hasInstance
	bool ____hasInstance_6;

public:
	inline static int32_t get_offset_of__container_0() { return static_cast<int32_t>(offsetof(SingletonLazyCreator_t2762284194, ____container_0)); }
	inline DiContainer_t2383114449 * get__container_0() const { return ____container_0; }
	inline DiContainer_t2383114449 ** get_address_of__container_0() { return &____container_0; }
	inline void set__container_0(DiContainer_t2383114449 * value)
	{
		____container_0 = value;
		Il2CppCodeGenWriteBarrier(&____container_0, value);
	}

	inline static int32_t get_offset_of__owner_1() { return static_cast<int32_t>(offsetof(SingletonLazyCreator_t2762284194, ____owner_1)); }
	inline SingletonProviderMap_t1557411893 * get__owner_1() const { return ____owner_1; }
	inline SingletonProviderMap_t1557411893 ** get_address_of__owner_1() { return &____owner_1; }
	inline void set__owner_1(SingletonProviderMap_t1557411893 * value)
	{
		____owner_1 = value;
		Il2CppCodeGenWriteBarrier(&____owner_1, value);
	}

	inline static int32_t get_offset_of__createMethod_2() { return static_cast<int32_t>(offsetof(SingletonLazyCreator_t2762284194, ____createMethod_2)); }
	inline Func_2_t2621245597 * get__createMethod_2() const { return ____createMethod_2; }
	inline Func_2_t2621245597 ** get_address_of__createMethod_2() { return &____createMethod_2; }
	inline void set__createMethod_2(Func_2_t2621245597 * value)
	{
		____createMethod_2 = value;
		Il2CppCodeGenWriteBarrier(&____createMethod_2, value);
	}

	inline static int32_t get_offset_of__id_3() { return static_cast<int32_t>(offsetof(SingletonLazyCreator_t2762284194, ____id_3)); }
	inline SingletonId_t1838183899 * get__id_3() const { return ____id_3; }
	inline SingletonId_t1838183899 ** get_address_of__id_3() { return &____id_3; }
	inline void set__id_3(SingletonId_t1838183899 * value)
	{
		____id_3 = value;
		Il2CppCodeGenWriteBarrier(&____id_3, value);
	}

	inline static int32_t get_offset_of__referenceCount_4() { return static_cast<int32_t>(offsetof(SingletonLazyCreator_t2762284194, ____referenceCount_4)); }
	inline int32_t get__referenceCount_4() const { return ____referenceCount_4; }
	inline int32_t* get_address_of__referenceCount_4() { return &____referenceCount_4; }
	inline void set__referenceCount_4(int32_t value)
	{
		____referenceCount_4 = value;
	}

	inline static int32_t get_offset_of__instance_5() { return static_cast<int32_t>(offsetof(SingletonLazyCreator_t2762284194, ____instance_5)); }
	inline Il2CppObject * get__instance_5() const { return ____instance_5; }
	inline Il2CppObject ** get_address_of__instance_5() { return &____instance_5; }
	inline void set__instance_5(Il2CppObject * value)
	{
		____instance_5 = value;
		Il2CppCodeGenWriteBarrier(&____instance_5, value);
	}

	inline static int32_t get_offset_of__hasInstance_6() { return static_cast<int32_t>(offsetof(SingletonLazyCreator_t2762284194, ____hasInstance_6)); }
	inline bool get__hasInstance_6() const { return ____hasInstance_6; }
	inline bool* get_address_of__hasInstance_6() { return &____hasInstance_6; }
	inline void set__hasInstance_6(bool value)
	{
		____hasInstance_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
