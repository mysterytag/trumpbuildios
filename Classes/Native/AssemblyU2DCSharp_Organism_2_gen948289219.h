﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// ConnectionCollector
struct ConnectionCollector_t444796719;
// System.Object
struct Il2CppObject;
// OnceEmptyStream
struct OnceEmptyStream_t3081939404;
// System.Collections.Generic.List`1<IOrganism>
struct List_1_t3377802548;
// System.Action`1<IOrganism>
struct Action_1_t2729296284;
// System.Action`1<System.IDisposable>
struct Action_1_t1777374079;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Organism`2<System.Object,System.Object>
struct  Organism_2_t948289219  : public Il2CppObject
{
public:
	// ConnectionCollector Organism`2::tails
	ConnectionCollector_t444796719 * ___tails_0;
	// TCarrier Organism`2::carrier_
	Il2CppObject * ___carrier__1;
	// TRoot Organism`2::root_
	Il2CppObject * ___root__2;
	// System.Boolean Organism`2::notACarrier
	bool ___notACarrier_3;
	// System.Boolean Organism`2::alive
	bool ___alive_4;
	// OnceEmptyStream Organism`2::fellAsleep_
	OnceEmptyStream_t3081939404 * ___fellAsleep__5;
	// System.Collections.Generic.List`1<IOrganism> Organism`2::children
	List_1_t3377802548 * ___children_6;

public:
	inline static int32_t get_offset_of_tails_0() { return static_cast<int32_t>(offsetof(Organism_2_t948289219, ___tails_0)); }
	inline ConnectionCollector_t444796719 * get_tails_0() const { return ___tails_0; }
	inline ConnectionCollector_t444796719 ** get_address_of_tails_0() { return &___tails_0; }
	inline void set_tails_0(ConnectionCollector_t444796719 * value)
	{
		___tails_0 = value;
		Il2CppCodeGenWriteBarrier(&___tails_0, value);
	}

	inline static int32_t get_offset_of_carrier__1() { return static_cast<int32_t>(offsetof(Organism_2_t948289219, ___carrier__1)); }
	inline Il2CppObject * get_carrier__1() const { return ___carrier__1; }
	inline Il2CppObject ** get_address_of_carrier__1() { return &___carrier__1; }
	inline void set_carrier__1(Il2CppObject * value)
	{
		___carrier__1 = value;
		Il2CppCodeGenWriteBarrier(&___carrier__1, value);
	}

	inline static int32_t get_offset_of_root__2() { return static_cast<int32_t>(offsetof(Organism_2_t948289219, ___root__2)); }
	inline Il2CppObject * get_root__2() const { return ___root__2; }
	inline Il2CppObject ** get_address_of_root__2() { return &___root__2; }
	inline void set_root__2(Il2CppObject * value)
	{
		___root__2 = value;
		Il2CppCodeGenWriteBarrier(&___root__2, value);
	}

	inline static int32_t get_offset_of_notACarrier_3() { return static_cast<int32_t>(offsetof(Organism_2_t948289219, ___notACarrier_3)); }
	inline bool get_notACarrier_3() const { return ___notACarrier_3; }
	inline bool* get_address_of_notACarrier_3() { return &___notACarrier_3; }
	inline void set_notACarrier_3(bool value)
	{
		___notACarrier_3 = value;
	}

	inline static int32_t get_offset_of_alive_4() { return static_cast<int32_t>(offsetof(Organism_2_t948289219, ___alive_4)); }
	inline bool get_alive_4() const { return ___alive_4; }
	inline bool* get_address_of_alive_4() { return &___alive_4; }
	inline void set_alive_4(bool value)
	{
		___alive_4 = value;
	}

	inline static int32_t get_offset_of_fellAsleep__5() { return static_cast<int32_t>(offsetof(Organism_2_t948289219, ___fellAsleep__5)); }
	inline OnceEmptyStream_t3081939404 * get_fellAsleep__5() const { return ___fellAsleep__5; }
	inline OnceEmptyStream_t3081939404 ** get_address_of_fellAsleep__5() { return &___fellAsleep__5; }
	inline void set_fellAsleep__5(OnceEmptyStream_t3081939404 * value)
	{
		___fellAsleep__5 = value;
		Il2CppCodeGenWriteBarrier(&___fellAsleep__5, value);
	}

	inline static int32_t get_offset_of_children_6() { return static_cast<int32_t>(offsetof(Organism_2_t948289219, ___children_6)); }
	inline List_1_t3377802548 * get_children_6() const { return ___children_6; }
	inline List_1_t3377802548 ** get_address_of_children_6() { return &___children_6; }
	inline void set_children_6(List_1_t3377802548 * value)
	{
		___children_6 = value;
		Il2CppCodeGenWriteBarrier(&___children_6, value);
	}
};

struct Organism_2_t948289219_StaticFields
{
public:
	// System.Action`1<IOrganism> Organism`2::<>f__am$cache7
	Action_1_t2729296284 * ___U3CU3Ef__amU24cache7_7;
	// System.Action`1<IOrganism> Organism`2::<>f__am$cache8
	Action_1_t2729296284 * ___U3CU3Ef__amU24cache8_8;
	// System.Action`1<IOrganism> Organism`2::<>f__am$cache9
	Action_1_t2729296284 * ___U3CU3Ef__amU24cache9_9;
	// System.Action`1<System.IDisposable> Organism`2::<>f__am$cacheA
	Action_1_t1777374079 * ___U3CU3Ef__amU24cacheA_10;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache7_7() { return static_cast<int32_t>(offsetof(Organism_2_t948289219_StaticFields, ___U3CU3Ef__amU24cache7_7)); }
	inline Action_1_t2729296284 * get_U3CU3Ef__amU24cache7_7() const { return ___U3CU3Ef__amU24cache7_7; }
	inline Action_1_t2729296284 ** get_address_of_U3CU3Ef__amU24cache7_7() { return &___U3CU3Ef__amU24cache7_7; }
	inline void set_U3CU3Ef__amU24cache7_7(Action_1_t2729296284 * value)
	{
		___U3CU3Ef__amU24cache7_7 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache7_7, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache8_8() { return static_cast<int32_t>(offsetof(Organism_2_t948289219_StaticFields, ___U3CU3Ef__amU24cache8_8)); }
	inline Action_1_t2729296284 * get_U3CU3Ef__amU24cache8_8() const { return ___U3CU3Ef__amU24cache8_8; }
	inline Action_1_t2729296284 ** get_address_of_U3CU3Ef__amU24cache8_8() { return &___U3CU3Ef__amU24cache8_8; }
	inline void set_U3CU3Ef__amU24cache8_8(Action_1_t2729296284 * value)
	{
		___U3CU3Ef__amU24cache8_8 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache8_8, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache9_9() { return static_cast<int32_t>(offsetof(Organism_2_t948289219_StaticFields, ___U3CU3Ef__amU24cache9_9)); }
	inline Action_1_t2729296284 * get_U3CU3Ef__amU24cache9_9() const { return ___U3CU3Ef__amU24cache9_9; }
	inline Action_1_t2729296284 ** get_address_of_U3CU3Ef__amU24cache9_9() { return &___U3CU3Ef__amU24cache9_9; }
	inline void set_U3CU3Ef__amU24cache9_9(Action_1_t2729296284 * value)
	{
		___U3CU3Ef__amU24cache9_9 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache9_9, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cacheA_10() { return static_cast<int32_t>(offsetof(Organism_2_t948289219_StaticFields, ___U3CU3Ef__amU24cacheA_10)); }
	inline Action_1_t1777374079 * get_U3CU3Ef__amU24cacheA_10() const { return ___U3CU3Ef__amU24cacheA_10; }
	inline Action_1_t1777374079 ** get_address_of_U3CU3Ef__amU24cacheA_10() { return &___U3CU3Ef__amU24cacheA_10; }
	inline void set_U3CU3Ef__amU24cacheA_10(Action_1_t1777374079 * value)
	{
		___U3CU3Ef__amU24cacheA_10 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cacheA_10, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
