﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// System.Collections.Generic.Dictionary`2<System.String,System.String>
struct Dictionary_2_t2606186806;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// OnePF.OpenIAB_iOS
struct  OpenIAB_iOS_t446655906  : public Il2CppObject
{
public:

public:
};

struct OpenIAB_iOS_t446655906_StaticFields
{
public:
	// System.String OnePF.OpenIAB_iOS::STORE
	String_t* ___STORE_0;
	// System.Collections.Generic.Dictionary`2<System.String,System.String> OnePF.OpenIAB_iOS::_sku2storeSkuMappings
	Dictionary_2_t2606186806 * ____sku2storeSkuMappings_1;
	// System.Collections.Generic.Dictionary`2<System.String,System.String> OnePF.OpenIAB_iOS::_storeSku2skuMappings
	Dictionary_2_t2606186806 * ____storeSku2skuMappings_2;

public:
	inline static int32_t get_offset_of_STORE_0() { return static_cast<int32_t>(offsetof(OpenIAB_iOS_t446655906_StaticFields, ___STORE_0)); }
	inline String_t* get_STORE_0() const { return ___STORE_0; }
	inline String_t** get_address_of_STORE_0() { return &___STORE_0; }
	inline void set_STORE_0(String_t* value)
	{
		___STORE_0 = value;
		Il2CppCodeGenWriteBarrier(&___STORE_0, value);
	}

	inline static int32_t get_offset_of__sku2storeSkuMappings_1() { return static_cast<int32_t>(offsetof(OpenIAB_iOS_t446655906_StaticFields, ____sku2storeSkuMappings_1)); }
	inline Dictionary_2_t2606186806 * get__sku2storeSkuMappings_1() const { return ____sku2storeSkuMappings_1; }
	inline Dictionary_2_t2606186806 ** get_address_of__sku2storeSkuMappings_1() { return &____sku2storeSkuMappings_1; }
	inline void set__sku2storeSkuMappings_1(Dictionary_2_t2606186806 * value)
	{
		____sku2storeSkuMappings_1 = value;
		Il2CppCodeGenWriteBarrier(&____sku2storeSkuMappings_1, value);
	}

	inline static int32_t get_offset_of__storeSku2skuMappings_2() { return static_cast<int32_t>(offsetof(OpenIAB_iOS_t446655906_StaticFields, ____storeSku2skuMappings_2)); }
	inline Dictionary_2_t2606186806 * get__storeSku2skuMappings_2() const { return ____storeSku2skuMappings_2; }
	inline Dictionary_2_t2606186806 ** get_address_of__storeSku2skuMappings_2() { return &____storeSku2skuMappings_2; }
	inline void set__storeSku2skuMappings_2(Dictionary_2_t2606186806 * value)
	{
		____storeSku2skuMappings_2 = value;
		Il2CppCodeGenWriteBarrier(&____storeSku2skuMappings_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
