﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.PlayFabClientAPI/LinkAndroidDeviceIDResponseCallback
struct LinkAndroidDeviceIDResponseCallback_t1895662028;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;
// PlayFab.ClientModels.LinkAndroidDeviceIDRequest
struct LinkAndroidDeviceIDRequest_t2075009465;
// PlayFab.ClientModels.LinkAndroidDeviceIDResult
struct LinkAndroidDeviceIDResult_t2734482899;
// PlayFab.PlayFabError
struct PlayFabError_t750598646;
// System.IAsyncResult
struct IAsyncResult_t537683269;
// System.AsyncCallback
struct AsyncCallback_t1363551830;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_IntPtr676692020.h"
#include "mscorlib_System_String968488902.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_LinkAndroid2075009465.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_LinkAndroid2734482899.h"
#include "AssemblyU2DCSharpU2Dfirstpass_PlayFab_PlayFabError750598646.h"
#include "mscorlib_System_AsyncCallback1363551830.h"

// System.Void PlayFab.PlayFabClientAPI/LinkAndroidDeviceIDResponseCallback::.ctor(System.Object,System.IntPtr)
extern "C"  void LinkAndroidDeviceIDResponseCallback__ctor_m3216322651 (LinkAndroidDeviceIDResponseCallback_t1895662028 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/LinkAndroidDeviceIDResponseCallback::Invoke(System.String,System.Int32,PlayFab.ClientModels.LinkAndroidDeviceIDRequest,PlayFab.ClientModels.LinkAndroidDeviceIDResult,PlayFab.PlayFabError,System.Object)
extern "C"  void LinkAndroidDeviceIDResponseCallback_Invoke_m1244819090 (LinkAndroidDeviceIDResponseCallback_t1895662028 * __this, String_t* ___urlPath0, int32_t ___callId1, LinkAndroidDeviceIDRequest_t2075009465 * ___request2, LinkAndroidDeviceIDResult_t2734482899 * ___result3, PlayFabError_t750598646 * ___error4, Il2CppObject * ___customData5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
extern "C" void pinvoke_delegate_wrapper_LinkAndroidDeviceIDResponseCallback_t1895662028(Il2CppObject* delegate, String_t* ___urlPath0, int32_t ___callId1, LinkAndroidDeviceIDRequest_t2075009465 * ___request2, LinkAndroidDeviceIDResult_t2734482899 * ___result3, PlayFabError_t750598646 * ___error4, Il2CppObject * ___customData5);
// System.IAsyncResult PlayFab.PlayFabClientAPI/LinkAndroidDeviceIDResponseCallback::BeginInvoke(System.String,System.Int32,PlayFab.ClientModels.LinkAndroidDeviceIDRequest,PlayFab.ClientModels.LinkAndroidDeviceIDResult,PlayFab.PlayFabError,System.Object,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * LinkAndroidDeviceIDResponseCallback_BeginInvoke_m2166964211 (LinkAndroidDeviceIDResponseCallback_t1895662028 * __this, String_t* ___urlPath0, int32_t ___callId1, LinkAndroidDeviceIDRequest_t2075009465 * ___request2, LinkAndroidDeviceIDResult_t2734482899 * ___result3, PlayFabError_t750598646 * ___error4, Il2CppObject * ___customData5, AsyncCallback_t1363551830 * ___callback6, Il2CppObject * ___object7, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/LinkAndroidDeviceIDResponseCallback::EndInvoke(System.IAsyncResult)
extern "C"  void LinkAndroidDeviceIDResponseCallback_EndInvoke_m1288505323 (LinkAndroidDeviceIDResponseCallback_t1895662028 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
