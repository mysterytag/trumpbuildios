﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UniRx.Subject`1<UniRx.Triggers.ObservableStateMachineTrigger/OnStateInfo>
struct Subject_1_t1488904540;
// UniRx.Subject`1<UniRx.Triggers.ObservableStateMachineTrigger/OnStateMachineInfo>
struct Subject_1_t2803904543;

#include "UnityEngine_UnityEngine_StateMachineBehaviour1035456276.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Triggers.ObservableStateMachineTrigger
struct  ObservableStateMachineTrigger_t739780288  : public StateMachineBehaviour_t1035456276
{
public:
	// UniRx.Subject`1<UniRx.Triggers.ObservableStateMachineTrigger/OnStateInfo> UniRx.Triggers.ObservableStateMachineTrigger::onStateExit
	Subject_1_t1488904540 * ___onStateExit_2;
	// UniRx.Subject`1<UniRx.Triggers.ObservableStateMachineTrigger/OnStateInfo> UniRx.Triggers.ObservableStateMachineTrigger::onStateEnter
	Subject_1_t1488904540 * ___onStateEnter_3;
	// UniRx.Subject`1<UniRx.Triggers.ObservableStateMachineTrigger/OnStateInfo> UniRx.Triggers.ObservableStateMachineTrigger::onStateIK
	Subject_1_t1488904540 * ___onStateIK_4;
	// UniRx.Subject`1<UniRx.Triggers.ObservableStateMachineTrigger/OnStateInfo> UniRx.Triggers.ObservableStateMachineTrigger::onStateUpdate
	Subject_1_t1488904540 * ___onStateUpdate_5;
	// UniRx.Subject`1<UniRx.Triggers.ObservableStateMachineTrigger/OnStateMachineInfo> UniRx.Triggers.ObservableStateMachineTrigger::onStateMachineEnter
	Subject_1_t2803904543 * ___onStateMachineEnter_6;
	// UniRx.Subject`1<UniRx.Triggers.ObservableStateMachineTrigger/OnStateMachineInfo> UniRx.Triggers.ObservableStateMachineTrigger::onStateMachineExit
	Subject_1_t2803904543 * ___onStateMachineExit_7;

public:
	inline static int32_t get_offset_of_onStateExit_2() { return static_cast<int32_t>(offsetof(ObservableStateMachineTrigger_t739780288, ___onStateExit_2)); }
	inline Subject_1_t1488904540 * get_onStateExit_2() const { return ___onStateExit_2; }
	inline Subject_1_t1488904540 ** get_address_of_onStateExit_2() { return &___onStateExit_2; }
	inline void set_onStateExit_2(Subject_1_t1488904540 * value)
	{
		___onStateExit_2 = value;
		Il2CppCodeGenWriteBarrier(&___onStateExit_2, value);
	}

	inline static int32_t get_offset_of_onStateEnter_3() { return static_cast<int32_t>(offsetof(ObservableStateMachineTrigger_t739780288, ___onStateEnter_3)); }
	inline Subject_1_t1488904540 * get_onStateEnter_3() const { return ___onStateEnter_3; }
	inline Subject_1_t1488904540 ** get_address_of_onStateEnter_3() { return &___onStateEnter_3; }
	inline void set_onStateEnter_3(Subject_1_t1488904540 * value)
	{
		___onStateEnter_3 = value;
		Il2CppCodeGenWriteBarrier(&___onStateEnter_3, value);
	}

	inline static int32_t get_offset_of_onStateIK_4() { return static_cast<int32_t>(offsetof(ObservableStateMachineTrigger_t739780288, ___onStateIK_4)); }
	inline Subject_1_t1488904540 * get_onStateIK_4() const { return ___onStateIK_4; }
	inline Subject_1_t1488904540 ** get_address_of_onStateIK_4() { return &___onStateIK_4; }
	inline void set_onStateIK_4(Subject_1_t1488904540 * value)
	{
		___onStateIK_4 = value;
		Il2CppCodeGenWriteBarrier(&___onStateIK_4, value);
	}

	inline static int32_t get_offset_of_onStateUpdate_5() { return static_cast<int32_t>(offsetof(ObservableStateMachineTrigger_t739780288, ___onStateUpdate_5)); }
	inline Subject_1_t1488904540 * get_onStateUpdate_5() const { return ___onStateUpdate_5; }
	inline Subject_1_t1488904540 ** get_address_of_onStateUpdate_5() { return &___onStateUpdate_5; }
	inline void set_onStateUpdate_5(Subject_1_t1488904540 * value)
	{
		___onStateUpdate_5 = value;
		Il2CppCodeGenWriteBarrier(&___onStateUpdate_5, value);
	}

	inline static int32_t get_offset_of_onStateMachineEnter_6() { return static_cast<int32_t>(offsetof(ObservableStateMachineTrigger_t739780288, ___onStateMachineEnter_6)); }
	inline Subject_1_t2803904543 * get_onStateMachineEnter_6() const { return ___onStateMachineEnter_6; }
	inline Subject_1_t2803904543 ** get_address_of_onStateMachineEnter_6() { return &___onStateMachineEnter_6; }
	inline void set_onStateMachineEnter_6(Subject_1_t2803904543 * value)
	{
		___onStateMachineEnter_6 = value;
		Il2CppCodeGenWriteBarrier(&___onStateMachineEnter_6, value);
	}

	inline static int32_t get_offset_of_onStateMachineExit_7() { return static_cast<int32_t>(offsetof(ObservableStateMachineTrigger_t739780288, ___onStateMachineExit_7)); }
	inline Subject_1_t2803904543 * get_onStateMachineExit_7() const { return ___onStateMachineExit_7; }
	inline Subject_1_t2803904543 ** get_address_of_onStateMachineExit_7() { return &___onStateMachineExit_7; }
	inline void set_onStateMachineExit_7(Subject_1_t2803904543 * value)
	{
		___onStateMachineExit_7 = value;
		Il2CppCodeGenWriteBarrier(&___onStateMachineExit_7, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
