﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.ReplaySubject`1/Subscription<System.Object>
struct Subscription_t2911320241;
// UniRx.ReplaySubject`1<System.Object>
struct ReplaySubject_1_t1910820897;
// UniRx.IObserver`1<System.Object>
struct IObserver_1_t3049105323;

#include "codegen/il2cpp-codegen.h"

// System.Void UniRx.ReplaySubject`1/Subscription<System.Object>::.ctor(UniRx.ReplaySubject`1<T>,UniRx.IObserver`1<T>)
extern "C"  void Subscription__ctor_m2334232119_gshared (Subscription_t2911320241 * __this, ReplaySubject_1_t1910820897 * ___parent0, Il2CppObject* ___unsubscribeTarget1, const MethodInfo* method);
#define Subscription__ctor_m2334232119(__this, ___parent0, ___unsubscribeTarget1, method) ((  void (*) (Subscription_t2911320241 *, ReplaySubject_1_t1910820897 *, Il2CppObject*, const MethodInfo*))Subscription__ctor_m2334232119_gshared)(__this, ___parent0, ___unsubscribeTarget1, method)
// System.Void UniRx.ReplaySubject`1/Subscription<System.Object>::Dispose()
extern "C"  void Subscription_Dispose_m2149913094_gshared (Subscription_t2911320241 * __this, const MethodInfo* method);
#define Subscription_Dispose_m2149913094(__this, method) ((  void (*) (Subscription_t2911320241 *, const MethodInfo*))Subscription_Dispose_m2149913094_gshared)(__this, method)
