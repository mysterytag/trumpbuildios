﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// UnityEngine.Object
struct Object_t3878351788;
struct Object_t3878351788_marshaled_pinvoke;
// System.Exception
struct Exception_t1967233988;
// System.Object
struct Il2CppObject;

#include "mscorlib_System_Object837106420.h"
#include "UnityEngine_UnityEngine_LogType3529269451.h"
#include "mscorlib_System_DateTime339033936.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Diagnostics.LogEntry
struct  LogEntry_t1891155722  : public Il2CppObject
{
public:
	// System.String UniRx.Diagnostics.LogEntry::<LoggerName>k__BackingField
	String_t* ___U3CLoggerNameU3Ek__BackingField_0;
	// UnityEngine.LogType UniRx.Diagnostics.LogEntry::<LogType>k__BackingField
	int32_t ___U3CLogTypeU3Ek__BackingField_1;
	// System.String UniRx.Diagnostics.LogEntry::<Message>k__BackingField
	String_t* ___U3CMessageU3Ek__BackingField_2;
	// System.DateTime UniRx.Diagnostics.LogEntry::<Timestamp>k__BackingField
	DateTime_t339033936  ___U3CTimestampU3Ek__BackingField_3;
	// UnityEngine.Object UniRx.Diagnostics.LogEntry::<Context>k__BackingField
	Object_t3878351788 * ___U3CContextU3Ek__BackingField_4;
	// System.Exception UniRx.Diagnostics.LogEntry::<Exception>k__BackingField
	Exception_t1967233988 * ___U3CExceptionU3Ek__BackingField_5;
	// System.String UniRx.Diagnostics.LogEntry::<StackTrace>k__BackingField
	String_t* ___U3CStackTraceU3Ek__BackingField_6;
	// System.Object UniRx.Diagnostics.LogEntry::<State>k__BackingField
	Il2CppObject * ___U3CStateU3Ek__BackingField_7;

public:
	inline static int32_t get_offset_of_U3CLoggerNameU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(LogEntry_t1891155722, ___U3CLoggerNameU3Ek__BackingField_0)); }
	inline String_t* get_U3CLoggerNameU3Ek__BackingField_0() const { return ___U3CLoggerNameU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CLoggerNameU3Ek__BackingField_0() { return &___U3CLoggerNameU3Ek__BackingField_0; }
	inline void set_U3CLoggerNameU3Ek__BackingField_0(String_t* value)
	{
		___U3CLoggerNameU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier(&___U3CLoggerNameU3Ek__BackingField_0, value);
	}

	inline static int32_t get_offset_of_U3CLogTypeU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(LogEntry_t1891155722, ___U3CLogTypeU3Ek__BackingField_1)); }
	inline int32_t get_U3CLogTypeU3Ek__BackingField_1() const { return ___U3CLogTypeU3Ek__BackingField_1; }
	inline int32_t* get_address_of_U3CLogTypeU3Ek__BackingField_1() { return &___U3CLogTypeU3Ek__BackingField_1; }
	inline void set_U3CLogTypeU3Ek__BackingField_1(int32_t value)
	{
		___U3CLogTypeU3Ek__BackingField_1 = value;
	}

	inline static int32_t get_offset_of_U3CMessageU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(LogEntry_t1891155722, ___U3CMessageU3Ek__BackingField_2)); }
	inline String_t* get_U3CMessageU3Ek__BackingField_2() const { return ___U3CMessageU3Ek__BackingField_2; }
	inline String_t** get_address_of_U3CMessageU3Ek__BackingField_2() { return &___U3CMessageU3Ek__BackingField_2; }
	inline void set_U3CMessageU3Ek__BackingField_2(String_t* value)
	{
		___U3CMessageU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier(&___U3CMessageU3Ek__BackingField_2, value);
	}

	inline static int32_t get_offset_of_U3CTimestampU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(LogEntry_t1891155722, ___U3CTimestampU3Ek__BackingField_3)); }
	inline DateTime_t339033936  get_U3CTimestampU3Ek__BackingField_3() const { return ___U3CTimestampU3Ek__BackingField_3; }
	inline DateTime_t339033936 * get_address_of_U3CTimestampU3Ek__BackingField_3() { return &___U3CTimestampU3Ek__BackingField_3; }
	inline void set_U3CTimestampU3Ek__BackingField_3(DateTime_t339033936  value)
	{
		___U3CTimestampU3Ek__BackingField_3 = value;
	}

	inline static int32_t get_offset_of_U3CContextU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(LogEntry_t1891155722, ___U3CContextU3Ek__BackingField_4)); }
	inline Object_t3878351788 * get_U3CContextU3Ek__BackingField_4() const { return ___U3CContextU3Ek__BackingField_4; }
	inline Object_t3878351788 ** get_address_of_U3CContextU3Ek__BackingField_4() { return &___U3CContextU3Ek__BackingField_4; }
	inline void set_U3CContextU3Ek__BackingField_4(Object_t3878351788 * value)
	{
		___U3CContextU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier(&___U3CContextU3Ek__BackingField_4, value);
	}

	inline static int32_t get_offset_of_U3CExceptionU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(LogEntry_t1891155722, ___U3CExceptionU3Ek__BackingField_5)); }
	inline Exception_t1967233988 * get_U3CExceptionU3Ek__BackingField_5() const { return ___U3CExceptionU3Ek__BackingField_5; }
	inline Exception_t1967233988 ** get_address_of_U3CExceptionU3Ek__BackingField_5() { return &___U3CExceptionU3Ek__BackingField_5; }
	inline void set_U3CExceptionU3Ek__BackingField_5(Exception_t1967233988 * value)
	{
		___U3CExceptionU3Ek__BackingField_5 = value;
		Il2CppCodeGenWriteBarrier(&___U3CExceptionU3Ek__BackingField_5, value);
	}

	inline static int32_t get_offset_of_U3CStackTraceU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(LogEntry_t1891155722, ___U3CStackTraceU3Ek__BackingField_6)); }
	inline String_t* get_U3CStackTraceU3Ek__BackingField_6() const { return ___U3CStackTraceU3Ek__BackingField_6; }
	inline String_t** get_address_of_U3CStackTraceU3Ek__BackingField_6() { return &___U3CStackTraceU3Ek__BackingField_6; }
	inline void set_U3CStackTraceU3Ek__BackingField_6(String_t* value)
	{
		___U3CStackTraceU3Ek__BackingField_6 = value;
		Il2CppCodeGenWriteBarrier(&___U3CStackTraceU3Ek__BackingField_6, value);
	}

	inline static int32_t get_offset_of_U3CStateU3Ek__BackingField_7() { return static_cast<int32_t>(offsetof(LogEntry_t1891155722, ___U3CStateU3Ek__BackingField_7)); }
	inline Il2CppObject * get_U3CStateU3Ek__BackingField_7() const { return ___U3CStateU3Ek__BackingField_7; }
	inline Il2CppObject ** get_address_of_U3CStateU3Ek__BackingField_7() { return &___U3CStateU3Ek__BackingField_7; }
	inline void set_U3CStateU3Ek__BackingField_7(Il2CppObject * value)
	{
		___U3CStateU3Ek__BackingField_7 = value;
		Il2CppCodeGenWriteBarrier(&___U3CStateU3Ek__BackingField_7, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
