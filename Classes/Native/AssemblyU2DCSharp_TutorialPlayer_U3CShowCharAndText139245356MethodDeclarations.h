﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// TutorialPlayer/<ShowCharAndText>c__Iterator26
struct U3CShowCharAndTextU3Ec__Iterator26_t139245356;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void TutorialPlayer/<ShowCharAndText>c__Iterator26::.ctor()
extern "C"  void U3CShowCharAndTextU3Ec__Iterator26__ctor_m110717359 (U3CShowCharAndTextU3Ec__Iterator26_t139245356 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object TutorialPlayer/<ShowCharAndText>c__Iterator26::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CShowCharAndTextU3Ec__Iterator26_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1183965635 (U3CShowCharAndTextU3Ec__Iterator26_t139245356 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object TutorialPlayer/<ShowCharAndText>c__Iterator26::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CShowCharAndTextU3Ec__Iterator26_System_Collections_IEnumerator_get_Current_m3167603543 (U3CShowCharAndTextU3Ec__Iterator26_t139245356 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean TutorialPlayer/<ShowCharAndText>c__Iterator26::MoveNext()
extern "C"  bool U3CShowCharAndTextU3Ec__Iterator26_MoveNext_m1466191077 (U3CShowCharAndTextU3Ec__Iterator26_t139245356 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TutorialPlayer/<ShowCharAndText>c__Iterator26::Dispose()
extern "C"  void U3CShowCharAndTextU3Ec__Iterator26_Dispose_m3218449964 (U3CShowCharAndTextU3Ec__Iterator26_t139245356 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TutorialPlayer/<ShowCharAndText>c__Iterator26::Reset()
extern "C"  void U3CShowCharAndTextU3Ec__Iterator26_Reset_m2052117596 (U3CShowCharAndTextU3Ec__Iterator26_t139245356 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
