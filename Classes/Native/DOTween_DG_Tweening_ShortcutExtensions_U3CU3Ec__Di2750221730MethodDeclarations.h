﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// DG.Tweening.ShortcutExtensions/<>c__DisplayClass60_0
struct U3CU3Ec__DisplayClass60_0_t2750221730;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Vector33525329789.h"

// System.Void DG.Tweening.ShortcutExtensions/<>c__DisplayClass60_0::.ctor()
extern "C"  void U3CU3Ec__DisplayClass60_0__ctor_m2984271028 (U3CU3Ec__DisplayClass60_0_t2750221730 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 DG.Tweening.ShortcutExtensions/<>c__DisplayClass60_0::<DOShakeRotation>b__0()
extern "C"  Vector3_t3525329789  U3CU3Ec__DisplayClass60_0_U3CDOShakeRotationU3Eb__0_m2475376871 (U3CU3Ec__DisplayClass60_0_t2750221730 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DG.Tweening.ShortcutExtensions/<>c__DisplayClass60_0::<DOShakeRotation>b__1(UnityEngine.Vector3)
extern "C"  void U3CU3Ec__DisplayClass60_0_U3CDOShakeRotationU3Eb__1_m815655607 (U3CU3Ec__DisplayClass60_0_t2750221730 * __this, Vector3_t3525329789  ___x0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
