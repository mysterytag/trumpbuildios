﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Zenject.TypeAnalyzer/<GetPostInjectMethods>c__AnonStorey182
struct U3CGetPostInjectMethodsU3Ec__AnonStorey182_t1421092187;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// Zenject.InjectableInfo
struct InjectableInfo_t1147709774;
// System.Reflection.ParameterInfo
struct ParameterInfo_t2610273829;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Reflection_MethodInfo3461221277.h"
#include "mscorlib_System_Reflection_ParameterInfo2610273829.h"

// System.Void Zenject.TypeAnalyzer/<GetPostInjectMethods>c__AnonStorey182::.ctor()
extern "C"  void U3CGetPostInjectMethodsU3Ec__AnonStorey182__ctor_m1647722528 (U3CGetPostInjectMethodsU3Ec__AnonStorey182_t1421092187 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Zenject.TypeAnalyzer/<GetPostInjectMethods>c__AnonStorey182::<>m__2B2(System.Reflection.MethodInfo)
extern "C"  int32_t U3CGetPostInjectMethodsU3Ec__AnonStorey182_U3CU3Em__2B2_m3523916186 (U3CGetPostInjectMethodsU3Ec__AnonStorey182_t1421092187 * __this, MethodInfo_t * ___x0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Zenject.InjectableInfo Zenject.TypeAnalyzer/<GetPostInjectMethods>c__AnonStorey182::<>m__2B3(System.Reflection.ParameterInfo)
extern "C"  InjectableInfo_t1147709774 * U3CGetPostInjectMethodsU3Ec__AnonStorey182_U3CU3Em__2B3_m461157480 (U3CGetPostInjectMethodsU3Ec__AnonStorey182_t1421092187 * __this, ParameterInfo_t2610273829 * ___paramInfo0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
