﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_ModestTree_Util_Tuple_2_gen2584011699MethodDeclarations.h"

// System.Void ModestTree.Util.Tuple`2<System.Object,System.Type>::.ctor()
#define Tuple_2__ctor_m3964972390(__this, method) ((  void (*) (Tuple_2_t231167918 *, const MethodInfo*))Tuple_2__ctor_m3534991723_gshared)(__this, method)
// System.Void ModestTree.Util.Tuple`2<System.Object,System.Type>::.ctor(T1,T2)
#define Tuple_2__ctor_m3636848287(__this, ___first0, ___second1, method) ((  void (*) (Tuple_2_t231167918 *, Il2CppObject *, Type_t *, const MethodInfo*))Tuple_2__ctor_m4054949306_gshared)(__this, ___first0, ___second1, method)
// System.Boolean ModestTree.Util.Tuple`2<System.Object,System.Type>::Equals(System.Object)
#define Tuple_2_Equals_m1381529763(__this, ___obj0, method) ((  bool (*) (Tuple_2_t231167918 *, Il2CppObject *, const MethodInfo*))Tuple_2_Equals_m2602159336_gshared)(__this, ___obj0, method)
// System.Boolean ModestTree.Util.Tuple`2<System.Object,System.Type>::Equals(ModestTree.Util.Tuple`2<T1,T2>)
#define Tuple_2_Equals_m2768885278(__this, ___that0, method) ((  bool (*) (Tuple_2_t231167918 *, Tuple_2_t231167918 *, const MethodInfo*))Tuple_2_Equals_m2951541881_gshared)(__this, ___that0, method)
// System.Int32 ModestTree.Util.Tuple`2<System.Object,System.Type>::GetHashCode()
#define Tuple_2_GetHashCode_m2497248583(__this, method) ((  int32_t (*) (Tuple_2_t231167918 *, const MethodInfo*))Tuple_2_GetHashCode_m16712972_gshared)(__this, method)
