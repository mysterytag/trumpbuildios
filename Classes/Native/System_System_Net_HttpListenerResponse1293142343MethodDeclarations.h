﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Net.HttpListenerResponse
struct HttpListenerResponse_t1293142343;
// System.Net.HttpListenerContext
struct HttpListenerContext_t260537341;
// System.Text.Encoding
struct Encoding_t180559927;
// System.String
struct String_t;
// System.Net.CookieCollection
struct CookieCollection_t2006989740;
// System.Net.WebHeaderCollection
struct WebHeaderCollection_t1099177929;
// System.IO.Stream
struct Stream_t219029575;
// System.Version
struct Version_t497901645;
// System.Net.Cookie
struct Cookie_t1541932526;
// System.Byte[]
struct ByteU5BU5D_t58506160;
// System.IO.MemoryStream
struct MemoryStream_t2881531048;

#include "codegen/il2cpp-codegen.h"
#include "System_System_Net_HttpListenerContext260537341.h"
#include "mscorlib_System_Text_Encoding180559927.h"
#include "mscorlib_System_String968488902.h"
#include "System_System_Net_CookieCollection2006989740.h"
#include "System_System_Net_WebHeaderCollection1099177929.h"
#include "mscorlib_System_Version497901645.h"
#include "System_System_Net_Cookie1541932526.h"
#include "System_System_Net_HttpListenerResponse1293142343.h"
#include "mscorlib_System_IO_MemoryStream2881531048.h"

// System.Void System.Net.HttpListenerResponse::.ctor(System.Net.HttpListenerContext)
extern "C"  void HttpListenerResponse__ctor_m3672574682 (HttpListenerResponse_t1293142343 * __this, HttpListenerContext_t260537341 * ___context0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.HttpListenerResponse::System.IDisposable.Dispose()
extern "C"  void HttpListenerResponse_System_IDisposable_Dispose_m117353026 (HttpListenerResponse_t1293142343 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Net.HttpListenerResponse::get_ForceCloseChunked()
extern "C"  bool HttpListenerResponse_get_ForceCloseChunked_m3197888583 (HttpListenerResponse_t1293142343 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Text.Encoding System.Net.HttpListenerResponse::get_ContentEncoding()
extern "C"  Encoding_t180559927 * HttpListenerResponse_get_ContentEncoding_m3814425672 (HttpListenerResponse_t1293142343 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.HttpListenerResponse::set_ContentEncoding(System.Text.Encoding)
extern "C"  void HttpListenerResponse_set_ContentEncoding_m873700597 (HttpListenerResponse_t1293142343 * __this, Encoding_t180559927 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 System.Net.HttpListenerResponse::get_ContentLength64()
extern "C"  int64_t HttpListenerResponse_get_ContentLength64_m3474505184 (HttpListenerResponse_t1293142343 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.HttpListenerResponse::set_ContentLength64(System.Int64)
extern "C"  void HttpListenerResponse_set_ContentLength64_m3619769165 (HttpListenerResponse_t1293142343 * __this, int64_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Net.HttpListenerResponse::get_ContentType()
extern "C"  String_t* HttpListenerResponse_get_ContentType_m2484805416 (HttpListenerResponse_t1293142343 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.HttpListenerResponse::set_ContentType(System.String)
extern "C"  void HttpListenerResponse_set_ContentType_m941632273 (HttpListenerResponse_t1293142343 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Net.CookieCollection System.Net.HttpListenerResponse::get_Cookies()
extern "C"  CookieCollection_t2006989740 * HttpListenerResponse_get_Cookies_m2190399874 (HttpListenerResponse_t1293142343 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.HttpListenerResponse::set_Cookies(System.Net.CookieCollection)
extern "C"  void HttpListenerResponse_set_Cookies_m3604023027 (HttpListenerResponse_t1293142343 * __this, CookieCollection_t2006989740 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Net.WebHeaderCollection System.Net.HttpListenerResponse::get_Headers()
extern "C"  WebHeaderCollection_t1099177929 * HttpListenerResponse_get_Headers_m744743016 (HttpListenerResponse_t1293142343 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.HttpListenerResponse::set_Headers(System.Net.WebHeaderCollection)
extern "C"  void HttpListenerResponse_set_Headers_m2837012053 (HttpListenerResponse_t1293142343 * __this, WebHeaderCollection_t1099177929 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Net.HttpListenerResponse::get_KeepAlive()
extern "C"  bool HttpListenerResponse_get_KeepAlive_m3204593680 (HttpListenerResponse_t1293142343 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.HttpListenerResponse::set_KeepAlive(System.Boolean)
extern "C"  void HttpListenerResponse_set_KeepAlive_m2184700285 (HttpListenerResponse_t1293142343 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IO.Stream System.Net.HttpListenerResponse::get_OutputStream()
extern "C"  Stream_t219029575 * HttpListenerResponse_get_OutputStream_m3472117019 (HttpListenerResponse_t1293142343 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Version System.Net.HttpListenerResponse::get_ProtocolVersion()
extern "C"  Version_t497901645 * HttpListenerResponse_get_ProtocolVersion_m1091001592 (HttpListenerResponse_t1293142343 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.HttpListenerResponse::set_ProtocolVersion(System.Version)
extern "C"  void HttpListenerResponse_set_ProtocolVersion_m238135269 (HttpListenerResponse_t1293142343 * __this, Version_t497901645 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Net.HttpListenerResponse::get_RedirectLocation()
extern "C"  String_t* HttpListenerResponse_get_RedirectLocation_m3368585566 (HttpListenerResponse_t1293142343 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.HttpListenerResponse::set_RedirectLocation(System.String)
extern "C"  void HttpListenerResponse_set_RedirectLocation_m2419113613 (HttpListenerResponse_t1293142343 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Net.HttpListenerResponse::get_SendChunked()
extern "C"  bool HttpListenerResponse_get_SendChunked_m983497260 (HttpListenerResponse_t1293142343 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.HttpListenerResponse::set_SendChunked(System.Boolean)
extern "C"  void HttpListenerResponse_set_SendChunked_m704945433 (HttpListenerResponse_t1293142343 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Net.HttpListenerResponse::get_StatusCode()
extern "C"  int32_t HttpListenerResponse_get_StatusCode_m378086911 (HttpListenerResponse_t1293142343 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.HttpListenerResponse::set_StatusCode(System.Int32)
extern "C"  void HttpListenerResponse_set_StatusCode_m3923089268 (HttpListenerResponse_t1293142343 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Net.HttpListenerResponse::GetStatusDescription(System.Int32)
extern "C"  String_t* HttpListenerResponse_GetStatusDescription_m2443051689 (Il2CppObject * __this /* static, unused */, int32_t ___code0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Net.HttpListenerResponse::get_StatusDescription()
extern "C"  String_t* HttpListenerResponse_get_StatusDescription_m3716031423 (HttpListenerResponse_t1293142343 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.HttpListenerResponse::set_StatusDescription(System.String)
extern "C"  void HttpListenerResponse_set_StatusDescription_m2005716634 (HttpListenerResponse_t1293142343 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.HttpListenerResponse::Abort()
extern "C"  void HttpListenerResponse_Abort_m1951719883 (HttpListenerResponse_t1293142343 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.HttpListenerResponse::AddHeader(System.String,System.String)
extern "C"  void HttpListenerResponse_AddHeader_m3127258485 (HttpListenerResponse_t1293142343 * __this, String_t* ___name0, String_t* ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.HttpListenerResponse::AppendCookie(System.Net.Cookie)
extern "C"  void HttpListenerResponse_AppendCookie_m4236646521 (HttpListenerResponse_t1293142343 * __this, Cookie_t1541932526 * ___cookie0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.HttpListenerResponse::AppendHeader(System.String,System.String)
extern "C"  void HttpListenerResponse_AppendHeader_m2079364176 (HttpListenerResponse_t1293142343 * __this, String_t* ___name0, String_t* ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.HttpListenerResponse::Close(System.Boolean)
extern "C"  void HttpListenerResponse_Close_m2638037706 (HttpListenerResponse_t1293142343 * __this, bool ___force0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.HttpListenerResponse::Close()
extern "C"  void HttpListenerResponse_Close_m4013034131 (HttpListenerResponse_t1293142343 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.HttpListenerResponse::Close(System.Byte[],System.Boolean)
extern "C"  void HttpListenerResponse_Close_m4126075047 (HttpListenerResponse_t1293142343 * __this, ByteU5BU5D_t58506160* ___responseEntity0, bool ___willBlock1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.HttpListenerResponse::CopyFrom(System.Net.HttpListenerResponse)
extern "C"  void HttpListenerResponse_CopyFrom_m1677288383 (HttpListenerResponse_t1293142343 * __this, HttpListenerResponse_t1293142343 * ___templateResponse0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.HttpListenerResponse::Redirect(System.String)
extern "C"  void HttpListenerResponse_Redirect_m636594655 (HttpListenerResponse_t1293142343 * __this, String_t* ___url0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Net.HttpListenerResponse::FindCookie(System.Net.Cookie)
extern "C"  bool HttpListenerResponse_FindCookie_m2391136080 (HttpListenerResponse_t1293142343 * __this, Cookie_t1541932526 * ___cookie0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.HttpListenerResponse::SendHeaders(System.Boolean,System.IO.MemoryStream)
extern "C"  void HttpListenerResponse_SendHeaders_m1695644076 (HttpListenerResponse_t1293142343 * __this, bool ___closing0, MemoryStream_t2881531048 * ___ms1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.HttpListenerResponse::SetCookie(System.Net.Cookie)
extern "C"  void HttpListenerResponse_SetCookie_m176534333 (HttpListenerResponse_t1293142343 * __this, Cookie_t1541932526 * ___cookie0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
