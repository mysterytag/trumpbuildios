﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// CellReactiveApi/DisposableContainer`1<System.Boolean>
struct DisposableContainer_1_t2879821764;
// System.Action
struct Action_t437523947;
// CellReactiveApi/<WhenSatisfy>c__AnonStorey10C`1<System.Object>
struct U3CWhenSatisfyU3Ec__AnonStorey10C_1_t4279471744;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CellReactiveApi/<WhenSatisfy>c__AnonStorey10C`1/<WhenSatisfy>c__AnonStorey10D`1<System.Object>
struct  U3CWhenSatisfyU3Ec__AnonStorey10D_1_t3834446745  : public Il2CppObject
{
public:
	// CellReactiveApi/DisposableContainer`1<System.Boolean> CellReactiveApi/<WhenSatisfy>c__AnonStorey10C`1/<WhenSatisfy>c__AnonStorey10D`1::disp
	DisposableContainer_1_t2879821764 * ___disp_0;
	// System.Action CellReactiveApi/<WhenSatisfy>c__AnonStorey10C`1/<WhenSatisfy>c__AnonStorey10D`1::reaction
	Action_t437523947 * ___reaction_1;
	// CellReactiveApi/<WhenSatisfy>c__AnonStorey10C`1<T> CellReactiveApi/<WhenSatisfy>c__AnonStorey10C`1/<WhenSatisfy>c__AnonStorey10D`1::<>f__ref$268
	U3CWhenSatisfyU3Ec__AnonStorey10C_1_t4279471744 * ___U3CU3Ef__refU24268_2;

public:
	inline static int32_t get_offset_of_disp_0() { return static_cast<int32_t>(offsetof(U3CWhenSatisfyU3Ec__AnonStorey10D_1_t3834446745, ___disp_0)); }
	inline DisposableContainer_1_t2879821764 * get_disp_0() const { return ___disp_0; }
	inline DisposableContainer_1_t2879821764 ** get_address_of_disp_0() { return &___disp_0; }
	inline void set_disp_0(DisposableContainer_1_t2879821764 * value)
	{
		___disp_0 = value;
		Il2CppCodeGenWriteBarrier(&___disp_0, value);
	}

	inline static int32_t get_offset_of_reaction_1() { return static_cast<int32_t>(offsetof(U3CWhenSatisfyU3Ec__AnonStorey10D_1_t3834446745, ___reaction_1)); }
	inline Action_t437523947 * get_reaction_1() const { return ___reaction_1; }
	inline Action_t437523947 ** get_address_of_reaction_1() { return &___reaction_1; }
	inline void set_reaction_1(Action_t437523947 * value)
	{
		___reaction_1 = value;
		Il2CppCodeGenWriteBarrier(&___reaction_1, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__refU24268_2() { return static_cast<int32_t>(offsetof(U3CWhenSatisfyU3Ec__AnonStorey10D_1_t3834446745, ___U3CU3Ef__refU24268_2)); }
	inline U3CWhenSatisfyU3Ec__AnonStorey10C_1_t4279471744 * get_U3CU3Ef__refU24268_2() const { return ___U3CU3Ef__refU24268_2; }
	inline U3CWhenSatisfyU3Ec__AnonStorey10C_1_t4279471744 ** get_address_of_U3CU3Ef__refU24268_2() { return &___U3CU3Ef__refU24268_2; }
	inline void set_U3CU3Ef__refU24268_2(U3CWhenSatisfyU3Ec__AnonStorey10C_1_t4279471744 * value)
	{
		___U3CU3Ef__refU24268_2 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__refU24268_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
