﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UniRx.Operators.ScanObservable`2<System.Object,System.Object>
struct ScanObservable_2_t1879023476;
// System.Object
struct Il2CppObject;

#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Oper1187768149.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Operators.ScanObservable`2/Scan<System.Object,System.Object>
struct  Scan_t2626963438  : public OperatorObserverBase_2_t1187768149
{
public:
	// UniRx.Operators.ScanObservable`2<TSource,TAccumulate> UniRx.Operators.ScanObservable`2/Scan::parent
	ScanObservable_2_t1879023476 * ___parent_2;
	// TAccumulate UniRx.Operators.ScanObservable`2/Scan::accumulation
	Il2CppObject * ___accumulation_3;
	// System.Boolean UniRx.Operators.ScanObservable`2/Scan::isFirst
	bool ___isFirst_4;

public:
	inline static int32_t get_offset_of_parent_2() { return static_cast<int32_t>(offsetof(Scan_t2626963438, ___parent_2)); }
	inline ScanObservable_2_t1879023476 * get_parent_2() const { return ___parent_2; }
	inline ScanObservable_2_t1879023476 ** get_address_of_parent_2() { return &___parent_2; }
	inline void set_parent_2(ScanObservable_2_t1879023476 * value)
	{
		___parent_2 = value;
		Il2CppCodeGenWriteBarrier(&___parent_2, value);
	}

	inline static int32_t get_offset_of_accumulation_3() { return static_cast<int32_t>(offsetof(Scan_t2626963438, ___accumulation_3)); }
	inline Il2CppObject * get_accumulation_3() const { return ___accumulation_3; }
	inline Il2CppObject ** get_address_of_accumulation_3() { return &___accumulation_3; }
	inline void set_accumulation_3(Il2CppObject * value)
	{
		___accumulation_3 = value;
		Il2CppCodeGenWriteBarrier(&___accumulation_3, value);
	}

	inline static int32_t get_offset_of_isFirst_4() { return static_cast<int32_t>(offsetof(Scan_t2626963438, ___isFirst_4)); }
	inline bool get_isFirst_4() const { return ___isFirst_4; }
	inline bool* get_address_of_isFirst_4() { return &___isFirst_4; }
	inline void set_isFirst_4(bool value)
	{
		___isFirst_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
