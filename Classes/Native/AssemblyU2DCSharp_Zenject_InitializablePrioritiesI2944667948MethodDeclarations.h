﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Zenject.InitializablePrioritiesInstaller
struct InitializablePrioritiesInstaller_t2944667948;
// System.Collections.Generic.List`1<System.Type>
struct List_1_t3576188904;
// Zenject.DiContainer
struct DiContainer_t2383114449;
// System.Type
struct Type_t;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Zenject_DiContainer2383114449.h"
#include "mscorlib_System_Type2779229935.h"

// System.Void Zenject.InitializablePrioritiesInstaller::.ctor(System.Collections.Generic.List`1<System.Type>)
extern "C"  void InitializablePrioritiesInstaller__ctor_m427573730 (InitializablePrioritiesInstaller_t2944667948 * __this, List_1_t3576188904 * ___initializables0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Zenject.InitializablePrioritiesInstaller::InstallBindings()
extern "C"  void InitializablePrioritiesInstaller_InstallBindings_m1320137004 (InitializablePrioritiesInstaller_t2944667948 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Zenject.InitializablePrioritiesInstaller::BindPriority(Zenject.DiContainer,System.Type,System.Int32)
extern "C"  void InitializablePrioritiesInstaller_BindPriority_m3618734415 (Il2CppObject * __this /* static, unused */, DiContainer_t2383114449 * ___container0, Type_t * ___initializableType1, int32_t ___priorityCount2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
