﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.InternalUtil.ListObserver`1<UniRx.Tuple`2<System.Object,System.Int32>>
struct ListObserver_1_t2674187857;
// UniRx.InternalUtil.ImmutableList`1<UniRx.IObserver`1<UniRx.Tuple`2<System.Object,System.Int32>>>
struct ImmutableList_1_t1356806292;
// System.Exception
struct Exception_t1967233988;
// UniRx.IObserver`1<UniRx.Tuple`2<System.Object,System.Int32>>
struct IObserver_1_t296601793;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Exception1967233988.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Tuple_2_gen2379570186.h"

// System.Void UniRx.InternalUtil.ListObserver`1<UniRx.Tuple`2<System.Object,System.Int32>>::.ctor(UniRx.InternalUtil.ImmutableList`1<UniRx.IObserver`1<T>>)
extern "C"  void ListObserver_1__ctor_m2897832202_gshared (ListObserver_1_t2674187857 * __this, ImmutableList_1_t1356806292 * ___observers0, const MethodInfo* method);
#define ListObserver_1__ctor_m2897832202(__this, ___observers0, method) ((  void (*) (ListObserver_1_t2674187857 *, ImmutableList_1_t1356806292 *, const MethodInfo*))ListObserver_1__ctor_m2897832202_gshared)(__this, ___observers0, method)
// System.Void UniRx.InternalUtil.ListObserver`1<UniRx.Tuple`2<System.Object,System.Int32>>::OnCompleted()
extern "C"  void ListObserver_1_OnCompleted_m3552233922_gshared (ListObserver_1_t2674187857 * __this, const MethodInfo* method);
#define ListObserver_1_OnCompleted_m3552233922(__this, method) ((  void (*) (ListObserver_1_t2674187857 *, const MethodInfo*))ListObserver_1_OnCompleted_m3552233922_gshared)(__this, method)
// System.Void UniRx.InternalUtil.ListObserver`1<UniRx.Tuple`2<System.Object,System.Int32>>::OnError(System.Exception)
extern "C"  void ListObserver_1_OnError_m3368740847_gshared (ListObserver_1_t2674187857 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method);
#define ListObserver_1_OnError_m3368740847(__this, ___error0, method) ((  void (*) (ListObserver_1_t2674187857 *, Exception_t1967233988 *, const MethodInfo*))ListObserver_1_OnError_m3368740847_gshared)(__this, ___error0, method)
// System.Void UniRx.InternalUtil.ListObserver`1<UniRx.Tuple`2<System.Object,System.Int32>>::OnNext(T)
extern "C"  void ListObserver_1_OnNext_m3682372832_gshared (ListObserver_1_t2674187857 * __this, Tuple_2_t2379570186  ___value0, const MethodInfo* method);
#define ListObserver_1_OnNext_m3682372832(__this, ___value0, method) ((  void (*) (ListObserver_1_t2674187857 *, Tuple_2_t2379570186 , const MethodInfo*))ListObserver_1_OnNext_m3682372832_gshared)(__this, ___value0, method)
// UniRx.IObserver`1<T> UniRx.InternalUtil.ListObserver`1<UniRx.Tuple`2<System.Object,System.Int32>>::Add(UniRx.IObserver`1<T>)
extern "C"  Il2CppObject* ListObserver_1_Add_m2745800434_gshared (ListObserver_1_t2674187857 * __this, Il2CppObject* ___observer0, const MethodInfo* method);
#define ListObserver_1_Add_m2745800434(__this, ___observer0, method) ((  Il2CppObject* (*) (ListObserver_1_t2674187857 *, Il2CppObject*, const MethodInfo*))ListObserver_1_Add_m2745800434_gshared)(__this, ___observer0, method)
// UniRx.IObserver`1<T> UniRx.InternalUtil.ListObserver`1<UniRx.Tuple`2<System.Object,System.Int32>>::Remove(UniRx.IObserver`1<T>)
extern "C"  Il2CppObject* ListObserver_1_Remove_m2922022681_gshared (ListObserver_1_t2674187857 * __this, Il2CppObject* ___observer0, const MethodInfo* method);
#define ListObserver_1_Remove_m2922022681(__this, ___observer0, method) ((  Il2CppObject* (*) (ListObserver_1_t2674187857 *, Il2CppObject*, const MethodInfo*))ListObserver_1_Remove_m2922022681_gshared)(__this, ___observer0, method)
