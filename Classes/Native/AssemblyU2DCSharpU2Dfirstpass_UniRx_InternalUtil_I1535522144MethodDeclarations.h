﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_I1897310919MethodDeclarations.h"

// System.Void UniRx.InternalUtil.ImmutableList`1<UniRx.IObserver`1<UniRx.Unit>>::.ctor()
#define ImmutableList_1__ctor_m3964855541(__this, method) ((  void (*) (ImmutableList_1_t1535522144 *, const MethodInfo*))ImmutableList_1__ctor_m2467204245_gshared)(__this, method)
// System.Void UniRx.InternalUtil.ImmutableList`1<UniRx.IObserver`1<UniRx.Unit>>::.ctor(T[])
#define ImmutableList_1__ctor_m1117186023(__this, ___data0, method) ((  void (*) (ImmutableList_1_t1535522144 *, IObserver_1U5BU5D_t960117152*, const MethodInfo*))ImmutableList_1__ctor_m707697735_gshared)(__this, ___data0, method)
// System.Void UniRx.InternalUtil.ImmutableList`1<UniRx.IObserver`1<UniRx.Unit>>::.cctor()
#define ImmutableList_1__cctor_m2169341272(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))ImmutableList_1__cctor_m2986791352_gshared)(__this /* static, unused */, method)
// T[] UniRx.InternalUtil.ImmutableList`1<UniRx.IObserver`1<UniRx.Unit>>::get_Data()
#define ImmutableList_1_get_Data_m1927469413(__this, method) ((  IObserver_1U5BU5D_t960117152* (*) (ImmutableList_1_t1535522144 *, const MethodInfo*))ImmutableList_1_get_Data_m1676800965_gshared)(__this, method)
// UniRx.InternalUtil.ImmutableList`1<T> UniRx.InternalUtil.ImmutableList`1<UniRx.IObserver`1<UniRx.Unit>>::Add(T)
#define ImmutableList_1_Add_m4248927907(__this, ___value0, method) ((  ImmutableList_1_t1535522144 * (*) (ImmutableList_1_t1535522144 *, Il2CppObject*, const MethodInfo*))ImmutableList_1_Add_m948892931_gshared)(__this, ___value0, method)
// UniRx.InternalUtil.ImmutableList`1<T> UniRx.InternalUtil.ImmutableList`1<UniRx.IObserver`1<UniRx.Unit>>::Remove(T)
#define ImmutableList_1_Remove_m1079481778(__this, ___value0, method) ((  ImmutableList_1_t1535522144 * (*) (ImmutableList_1_t1535522144 *, Il2CppObject*, const MethodInfo*))ImmutableList_1_Remove_m1538917202_gshared)(__this, ___value0, method)
// System.Int32 UniRx.InternalUtil.ImmutableList`1<UniRx.IObserver`1<UniRx.Unit>>::IndexOf(T)
#define ImmutableList_1_IndexOf_m2053002508(__this, ___value0, method) ((  int32_t (*) (ImmutableList_1_t1535522144 *, Il2CppObject*, const MethodInfo*))ImmutableList_1_IndexOf_m3149323628_gshared)(__this, ___value0, method)
