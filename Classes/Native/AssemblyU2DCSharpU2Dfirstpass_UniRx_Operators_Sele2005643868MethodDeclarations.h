﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.SelectManyObservable`3/SelectManyObserverWithIndex/SelectManyObserver<System.Object,System.Int64,System.Object>
struct SelectManyObserver_t2005643868;
// UniRx.Operators.SelectManyObservable`3/SelectManyObserverWithIndex<System.Object,System.Int64,System.Object>
struct SelectManyObserverWithIndex_t1585155910;
// System.Object
struct Il2CppObject;
// System.IDisposable
struct IDisposable_t1628921374;
// System.Exception
struct Exception_t1967233988;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_Exception1967233988.h"

// System.Void UniRx.Operators.SelectManyObservable`3/SelectManyObserverWithIndex/SelectManyObserver<System.Object,System.Int64,System.Object>::.ctor(UniRx.Operators.SelectManyObservable`3/SelectManyObserverWithIndex<TSource,TCollection,TResult>,TSource,System.Int32,System.IDisposable)
extern "C"  void SelectManyObserver__ctor_m1925785969_gshared (SelectManyObserver_t2005643868 * __this, SelectManyObserverWithIndex_t1585155910 * ___parent0, Il2CppObject * ___value1, int32_t ___index2, Il2CppObject * ___cancel3, const MethodInfo* method);
#define SelectManyObserver__ctor_m1925785969(__this, ___parent0, ___value1, ___index2, ___cancel3, method) ((  void (*) (SelectManyObserver_t2005643868 *, SelectManyObserverWithIndex_t1585155910 *, Il2CppObject *, int32_t, Il2CppObject *, const MethodInfo*))SelectManyObserver__ctor_m1925785969_gshared)(__this, ___parent0, ___value1, ___index2, ___cancel3, method)
// System.Void UniRx.Operators.SelectManyObservable`3/SelectManyObserverWithIndex/SelectManyObserver<System.Object,System.Int64,System.Object>::OnNext(TCollection)
extern "C"  void SelectManyObserver_OnNext_m2871645185_gshared (SelectManyObserver_t2005643868 * __this, int64_t ___value0, const MethodInfo* method);
#define SelectManyObserver_OnNext_m2871645185(__this, ___value0, method) ((  void (*) (SelectManyObserver_t2005643868 *, int64_t, const MethodInfo*))SelectManyObserver_OnNext_m2871645185_gshared)(__this, ___value0, method)
// System.Void UniRx.Operators.SelectManyObservable`3/SelectManyObserverWithIndex/SelectManyObserver<System.Object,System.Int64,System.Object>::OnError(System.Exception)
extern "C"  void SelectManyObserver_OnError_m1569830798_gshared (SelectManyObserver_t2005643868 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method);
#define SelectManyObserver_OnError_m1569830798(__this, ___error0, method) ((  void (*) (SelectManyObserver_t2005643868 *, Exception_t1967233988 *, const MethodInfo*))SelectManyObserver_OnError_m1569830798_gshared)(__this, ___error0, method)
// System.Void UniRx.Operators.SelectManyObservable`3/SelectManyObserverWithIndex/SelectManyObserver<System.Object,System.Int64,System.Object>::OnCompleted()
extern "C"  void SelectManyObserver_OnCompleted_m125620193_gshared (SelectManyObserver_t2005643868 * __this, const MethodInfo* method);
#define SelectManyObserver_OnCompleted_m125620193(__this, method) ((  void (*) (SelectManyObserver_t2005643868 *, const MethodInfo*))SelectManyObserver_OnCompleted_m125620193_gshared)(__this, method)
