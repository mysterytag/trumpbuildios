﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Subject_1_gen2775141040MethodDeclarations.h"

// System.Void UniRx.Subject`1<TableViewCellBase>::.ctor()
#define Subject_1__ctor_m3052935276(__this, method) ((  void (*) (Subject_1_t3995879330 *, const MethodInfo*))Subject_1__ctor_m3752525464_gshared)(__this, method)
// System.Boolean UniRx.Subject`1<TableViewCellBase>::get_HasObservers()
#define Subject_1_get_HasObservers_m1689473388(__this, method) ((  bool (*) (Subject_1_t3995879330 *, const MethodInfo*))Subject_1_get_HasObservers_m2673131508_gshared)(__this, method)
// System.Void UniRx.Subject`1<TableViewCellBase>::OnCompleted()
#define Subject_1_OnCompleted_m1865405866(__this, method) ((  void (*) (Subject_1_t3995879330 *, const MethodInfo*))Subject_1_OnCompleted_m1083367458_gshared)(__this, method)
// System.Void UniRx.Subject`1<TableViewCellBase>::OnError(System.Exception)
#define Subject_1_OnError_m2764396503(__this, ___error0, method) ((  void (*) (Subject_1_t3995879330 *, Exception_t1967233988 *, const MethodInfo*))Subject_1_OnError_m1700032079_gshared)(__this, ___error0, method)
// System.Void UniRx.Subject`1<TableViewCellBase>::OnNext(T)
#define Subject_1_OnNext_m1619753979(__this, ___value0, method) ((  void (*) (Subject_1_t3995879330 *, TableViewCellBase_t2057844710 *, const MethodInfo*))Subject_1_OnNext_m1235145536_gshared)(__this, ___value0, method)
// System.IDisposable UniRx.Subject`1<TableViewCellBase>::Subscribe(UniRx.IObserver`1<T>)
#define Subject_1_Subscribe_m3025672607(__this, ___observer0, method) ((  Il2CppObject * (*) (Subject_1_t3995879330 *, Il2CppObject*, const MethodInfo*))Subject_1_Subscribe_m2428743831_gshared)(__this, ___observer0, method)
// System.Void UniRx.Subject`1<TableViewCellBase>::Dispose()
#define Subject_1_Dispose_m3474628189(__this, method) ((  void (*) (Subject_1_t3995879330 *, const MethodInfo*))Subject_1_Dispose_m2597692629_gshared)(__this, method)
// System.Void UniRx.Subject`1<TableViewCellBase>::ThrowIfDisposed()
#define Subject_1_ThrowIfDisposed_m1033280230(__this, method) ((  void (*) (Subject_1_t3995879330 *, const MethodInfo*))Subject_1_ThrowIfDisposed_m956279134_gshared)(__this, method)
// System.Boolean UniRx.Subject`1<TableViewCellBase>::IsRequiredSubscribeOnCurrentThread()
#define Subject_1_IsRequiredSubscribeOnCurrentThread_m1068415843(__this, method) ((  bool (*) (Subject_1_t3995879330 *, const MethodInfo*))Subject_1_IsRequiredSubscribeOnCurrentThread_m3969161195_gshared)(__this, method)
