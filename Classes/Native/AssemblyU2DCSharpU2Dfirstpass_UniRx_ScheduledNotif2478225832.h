﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UniRx.IScheduler
struct IScheduler_t2938318244;
// UniRx.Subject`1<System.Single>
struct Subject_1_t2896243641;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.ScheduledNotifier`1<System.Single>
struct  ScheduledNotifier_1_t2478225832  : public Il2CppObject
{
public:
	// UniRx.IScheduler UniRx.ScheduledNotifier`1::scheduler
	Il2CppObject * ___scheduler_0;
	// UniRx.Subject`1<T> UniRx.ScheduledNotifier`1::trigger
	Subject_1_t2896243641 * ___trigger_1;

public:
	inline static int32_t get_offset_of_scheduler_0() { return static_cast<int32_t>(offsetof(ScheduledNotifier_1_t2478225832, ___scheduler_0)); }
	inline Il2CppObject * get_scheduler_0() const { return ___scheduler_0; }
	inline Il2CppObject ** get_address_of_scheduler_0() { return &___scheduler_0; }
	inline void set_scheduler_0(Il2CppObject * value)
	{
		___scheduler_0 = value;
		Il2CppCodeGenWriteBarrier(&___scheduler_0, value);
	}

	inline static int32_t get_offset_of_trigger_1() { return static_cast<int32_t>(offsetof(ScheduledNotifier_1_t2478225832, ___trigger_1)); }
	inline Subject_1_t2896243641 * get_trigger_1() const { return ___trigger_1; }
	inline Subject_1_t2896243641 ** get_address_of_trigger_1() { return &___trigger_1; }
	inline void set_trigger_1(Subject_1_t2896243641 * value)
	{
		___trigger_1 = value;
		Il2CppCodeGenWriteBarrier(&___trigger_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
