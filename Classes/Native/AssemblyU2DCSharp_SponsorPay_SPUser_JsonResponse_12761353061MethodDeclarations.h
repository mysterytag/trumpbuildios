﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SponsorPay.SPUser/JsonResponse`1<System.Nullable`1<System.Int64>>
struct JsonResponse_1_t2761353061;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String968488902.h"
#include "mscorlib_System_Nullable_1_gen1438485494.h"

// System.Void SponsorPay.SPUser/JsonResponse`1<System.Nullable`1<System.Int64>>::.ctor()
extern "C"  void JsonResponse_1__ctor_m2573699927_gshared (JsonResponse_1_t2761353061 * __this, const MethodInfo* method);
#define JsonResponse_1__ctor_m2573699927(__this, method) ((  void (*) (JsonResponse_1_t2761353061 *, const MethodInfo*))JsonResponse_1__ctor_m2573699927_gshared)(__this, method)
// System.String SponsorPay.SPUser/JsonResponse`1<System.Nullable`1<System.Int64>>::get_key()
extern "C"  String_t* JsonResponse_1_get_key_m2878858670_gshared (JsonResponse_1_t2761353061 * __this, const MethodInfo* method);
#define JsonResponse_1_get_key_m2878858670(__this, method) ((  String_t* (*) (JsonResponse_1_t2761353061 *, const MethodInfo*))JsonResponse_1_get_key_m2878858670_gshared)(__this, method)
// System.Void SponsorPay.SPUser/JsonResponse`1<System.Nullable`1<System.Int64>>::set_key(System.String)
extern "C"  void JsonResponse_1_set_key_m1268525131_gshared (JsonResponse_1_t2761353061 * __this, String_t* ___value0, const MethodInfo* method);
#define JsonResponse_1_set_key_m1268525131(__this, ___value0, method) ((  void (*) (JsonResponse_1_t2761353061 *, String_t*, const MethodInfo*))JsonResponse_1_set_key_m1268525131_gshared)(__this, ___value0, method)
// T SponsorPay.SPUser/JsonResponse`1<System.Nullable`1<System.Int64>>::get_value()
extern "C"  Nullable_1_t1438485494  JsonResponse_1_get_value_m3815641372_gshared (JsonResponse_1_t2761353061 * __this, const MethodInfo* method);
#define JsonResponse_1_get_value_m3815641372(__this, method) ((  Nullable_1_t1438485494  (*) (JsonResponse_1_t2761353061 *, const MethodInfo*))JsonResponse_1_get_value_m3815641372_gshared)(__this, method)
// System.Void SponsorPay.SPUser/JsonResponse`1<System.Nullable`1<System.Int64>>::set_value(T)
extern "C"  void JsonResponse_1_set_value_m3442268565_gshared (JsonResponse_1_t2761353061 * __this, Nullable_1_t1438485494  ___value0, const MethodInfo* method);
#define JsonResponse_1_set_value_m3442268565(__this, ___value0, method) ((  void (*) (JsonResponse_1_t2761353061 *, Nullable_1_t1438485494 , const MethodInfo*))JsonResponse_1_set_value_m3442268565_gshared)(__this, ___value0, method)
// System.String SponsorPay.SPUser/JsonResponse`1<System.Nullable`1<System.Int64>>::get_error()
extern "C"  String_t* JsonResponse_1_get_error_m4257056663_gshared (JsonResponse_1_t2761353061 * __this, const MethodInfo* method);
#define JsonResponse_1_get_error_m4257056663(__this, method) ((  String_t* (*) (JsonResponse_1_t2761353061 *, const MethodInfo*))JsonResponse_1_get_error_m4257056663_gshared)(__this, method)
// System.Void SponsorPay.SPUser/JsonResponse`1<System.Nullable`1<System.Int64>>::set_error(System.String)
extern "C"  void JsonResponse_1_set_error_m1937798978_gshared (JsonResponse_1_t2761353061 * __this, String_t* ___value0, const MethodInfo* method);
#define JsonResponse_1_set_error_m1937798978(__this, ___value0, method) ((  void (*) (JsonResponse_1_t2761353061 *, String_t*, const MethodInfo*))JsonResponse_1_set_error_m1937798978_gshared)(__this, ___value0, method)
