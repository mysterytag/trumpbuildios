﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.PlayFabClientAPI/GetPlayFabIDsFromKongregateIDsResponseCallback
struct GetPlayFabIDsFromKongregateIDsResponseCallback_t3964210686;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;
// PlayFab.ClientModels.GetPlayFabIDsFromKongregateIDsRequest
struct GetPlayFabIDsFromKongregateIDsRequest_t4157912391;
// PlayFab.ClientModels.GetPlayFabIDsFromKongregateIDsResult
struct GetPlayFabIDsFromKongregateIDsResult_t3910051973;
// PlayFab.PlayFabError
struct PlayFabError_t750598646;
// System.IAsyncResult
struct IAsyncResult_t537683269;
// System.AsyncCallback
struct AsyncCallback_t1363551830;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_IntPtr676692020.h"
#include "mscorlib_System_String968488902.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_GetPlayFabI4157912391.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_GetPlayFabI3910051973.h"
#include "AssemblyU2DCSharpU2Dfirstpass_PlayFab_PlayFabError750598646.h"
#include "mscorlib_System_AsyncCallback1363551830.h"

// System.Void PlayFab.PlayFabClientAPI/GetPlayFabIDsFromKongregateIDsResponseCallback::.ctor(System.Object,System.IntPtr)
extern "C"  void GetPlayFabIDsFromKongregateIDsResponseCallback__ctor_m2987889645 (GetPlayFabIDsFromKongregateIDsResponseCallback_t3964210686 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/GetPlayFabIDsFromKongregateIDsResponseCallback::Invoke(System.String,System.Int32,PlayFab.ClientModels.GetPlayFabIDsFromKongregateIDsRequest,PlayFab.ClientModels.GetPlayFabIDsFromKongregateIDsResult,PlayFab.PlayFabError,System.Object)
extern "C"  void GetPlayFabIDsFromKongregateIDsResponseCallback_Invoke_m3269719354 (GetPlayFabIDsFromKongregateIDsResponseCallback_t3964210686 * __this, String_t* ___urlPath0, int32_t ___callId1, GetPlayFabIDsFromKongregateIDsRequest_t4157912391 * ___request2, GetPlayFabIDsFromKongregateIDsResult_t3910051973 * ___result3, PlayFabError_t750598646 * ___error4, Il2CppObject * ___customData5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
extern "C" void pinvoke_delegate_wrapper_GetPlayFabIDsFromKongregateIDsResponseCallback_t3964210686(Il2CppObject* delegate, String_t* ___urlPath0, int32_t ___callId1, GetPlayFabIDsFromKongregateIDsRequest_t4157912391 * ___request2, GetPlayFabIDsFromKongregateIDsResult_t3910051973 * ___result3, PlayFabError_t750598646 * ___error4, Il2CppObject * ___customData5);
// System.IAsyncResult PlayFab.PlayFabClientAPI/GetPlayFabIDsFromKongregateIDsResponseCallback::BeginInvoke(System.String,System.Int32,PlayFab.ClientModels.GetPlayFabIDsFromKongregateIDsRequest,PlayFab.ClientModels.GetPlayFabIDsFromKongregateIDsResult,PlayFab.PlayFabError,System.Object,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * GetPlayFabIDsFromKongregateIDsResponseCallback_BeginInvoke_m520870119 (GetPlayFabIDsFromKongregateIDsResponseCallback_t3964210686 * __this, String_t* ___urlPath0, int32_t ___callId1, GetPlayFabIDsFromKongregateIDsRequest_t4157912391 * ___request2, GetPlayFabIDsFromKongregateIDsResult_t3910051973 * ___result3, PlayFabError_t750598646 * ___error4, Il2CppObject * ___customData5, AsyncCallback_t1363551830 * ___callback6, Il2CppObject * ___object7, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/GetPlayFabIDsFromKongregateIDsResponseCallback::EndInvoke(System.IAsyncResult)
extern "C"  void GetPlayFabIDsFromKongregateIDsResponseCallback_EndInvoke_m1661207677 (GetPlayFabIDsFromKongregateIDsResponseCallback_t3964210686 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
