﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.ObserveOnObservable`1/ObserveOn/<OnNext>c__AnonStorey61<UniRx.Unit>
struct U3COnNextU3Ec__AnonStorey61_t3083421405;

#include "codegen/il2cpp-codegen.h"

// System.Void UniRx.Operators.ObserveOnObservable`1/ObserveOn/<OnNext>c__AnonStorey61<UniRx.Unit>::.ctor()
extern "C"  void U3COnNextU3Ec__AnonStorey61__ctor_m1853688586_gshared (U3COnNextU3Ec__AnonStorey61_t3083421405 * __this, const MethodInfo* method);
#define U3COnNextU3Ec__AnonStorey61__ctor_m1853688586(__this, method) ((  void (*) (U3COnNextU3Ec__AnonStorey61_t3083421405 *, const MethodInfo*))U3COnNextU3Ec__AnonStorey61__ctor_m1853688586_gshared)(__this, method)
// System.Void UniRx.Operators.ObserveOnObservable`1/ObserveOn/<OnNext>c__AnonStorey61<UniRx.Unit>::<>m__7E()
extern "C"  void U3COnNextU3Ec__AnonStorey61_U3CU3Em__7E_m1291542209_gshared (U3COnNextU3Ec__AnonStorey61_t3083421405 * __this, const MethodInfo* method);
#define U3COnNextU3Ec__AnonStorey61_U3CU3Em__7E_m1291542209(__this, method) ((  void (*) (U3COnNextU3Ec__AnonStorey61_t3083421405 *, const MethodInfo*))U3COnNextU3Ec__AnonStorey61_U3CU3Em__7E_m1291542209_gshared)(__this, method)
