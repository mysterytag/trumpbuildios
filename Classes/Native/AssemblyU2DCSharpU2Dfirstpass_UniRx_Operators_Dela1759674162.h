﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UniRx.IObservable`1<System.Object>
struct IObservable_1_t595904784;
// UniRx.IScheduler
struct IScheduler_t2938318244;

#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Oper4196218687.h"
#include "mscorlib_System_Nullable_1_gen3649900800.h"
#include "mscorlib_System_Nullable_1_gen2303330647.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Operators.DelaySubscriptionObservable`1<System.Object>
struct  DelaySubscriptionObservable_1_t1759674162  : public OperatorObservableBase_1_t4196218687
{
public:
	// UniRx.IObservable`1<T> UniRx.Operators.DelaySubscriptionObservable`1::source
	Il2CppObject* ___source_1;
	// UniRx.IScheduler UniRx.Operators.DelaySubscriptionObservable`1::scheduler
	Il2CppObject * ___scheduler_2;
	// System.Nullable`1<System.TimeSpan> UniRx.Operators.DelaySubscriptionObservable`1::dueTimeT
	Nullable_1_t3649900800  ___dueTimeT_3;
	// System.Nullable`1<System.DateTimeOffset> UniRx.Operators.DelaySubscriptionObservable`1::dueTimeD
	Nullable_1_t2303330647  ___dueTimeD_4;

public:
	inline static int32_t get_offset_of_source_1() { return static_cast<int32_t>(offsetof(DelaySubscriptionObservable_1_t1759674162, ___source_1)); }
	inline Il2CppObject* get_source_1() const { return ___source_1; }
	inline Il2CppObject** get_address_of_source_1() { return &___source_1; }
	inline void set_source_1(Il2CppObject* value)
	{
		___source_1 = value;
		Il2CppCodeGenWriteBarrier(&___source_1, value);
	}

	inline static int32_t get_offset_of_scheduler_2() { return static_cast<int32_t>(offsetof(DelaySubscriptionObservable_1_t1759674162, ___scheduler_2)); }
	inline Il2CppObject * get_scheduler_2() const { return ___scheduler_2; }
	inline Il2CppObject ** get_address_of_scheduler_2() { return &___scheduler_2; }
	inline void set_scheduler_2(Il2CppObject * value)
	{
		___scheduler_2 = value;
		Il2CppCodeGenWriteBarrier(&___scheduler_2, value);
	}

	inline static int32_t get_offset_of_dueTimeT_3() { return static_cast<int32_t>(offsetof(DelaySubscriptionObservable_1_t1759674162, ___dueTimeT_3)); }
	inline Nullable_1_t3649900800  get_dueTimeT_3() const { return ___dueTimeT_3; }
	inline Nullable_1_t3649900800 * get_address_of_dueTimeT_3() { return &___dueTimeT_3; }
	inline void set_dueTimeT_3(Nullable_1_t3649900800  value)
	{
		___dueTimeT_3 = value;
	}

	inline static int32_t get_offset_of_dueTimeD_4() { return static_cast<int32_t>(offsetof(DelaySubscriptionObservable_1_t1759674162, ___dueTimeD_4)); }
	inline Nullable_1_t2303330647  get_dueTimeD_4() const { return ___dueTimeD_4; }
	inline Nullable_1_t2303330647 * get_address_of_dueTimeD_4() { return &___dueTimeD_4; }
	inline void set_dueTimeD_4(Nullable_1_t2303330647  value)
	{
		___dueTimeD_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
