﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UniRx.Observable/ConnectableObservable`1<System.Object>
struct ConnectableObservable_1_t608944977;
// System.IDisposable
struct IDisposable_t1628921374;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Observable/ConnectableObservable`1/Connection<System.Object>
struct  Connection_t217434055  : public Il2CppObject
{
public:
	// UniRx.Observable/ConnectableObservable`1<T> UniRx.Observable/ConnectableObservable`1/Connection::parent
	ConnectableObservable_1_t608944977 * ___parent_0;
	// System.IDisposable UniRx.Observable/ConnectableObservable`1/Connection::subscription
	Il2CppObject * ___subscription_1;

public:
	inline static int32_t get_offset_of_parent_0() { return static_cast<int32_t>(offsetof(Connection_t217434055, ___parent_0)); }
	inline ConnectableObservable_1_t608944977 * get_parent_0() const { return ___parent_0; }
	inline ConnectableObservable_1_t608944977 ** get_address_of_parent_0() { return &___parent_0; }
	inline void set_parent_0(ConnectableObservable_1_t608944977 * value)
	{
		___parent_0 = value;
		Il2CppCodeGenWriteBarrier(&___parent_0, value);
	}

	inline static int32_t get_offset_of_subscription_1() { return static_cast<int32_t>(offsetof(Connection_t217434055, ___subscription_1)); }
	inline Il2CppObject * get_subscription_1() const { return ___subscription_1; }
	inline Il2CppObject ** get_address_of_subscription_1() { return &___subscription_1; }
	inline void set_subscription_1(Il2CppObject * value)
	{
		___subscription_1 = value;
		Il2CppCodeGenWriteBarrier(&___subscription_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
