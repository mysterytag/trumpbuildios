﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Triggers.ObservablePointerUpTrigger
struct ObservablePointerUpTrigger_t2862374692;
// UnityEngine.EventSystems.PointerEventData
struct PointerEventData_t3205101634;
// UniRx.IObservable`1<UnityEngine.EventSystems.PointerEventData>
struct IObservable_1_t2963899998;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_PointerEve3205101634.h"

// System.Void UniRx.Triggers.ObservablePointerUpTrigger::.ctor()
extern "C"  void ObservablePointerUpTrigger__ctor_m3124876633 (ObservablePointerUpTrigger_t2862374692 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.Triggers.ObservablePointerUpTrigger::UnityEngine.EventSystems.IPointerUpHandler.OnPointerUp(UnityEngine.EventSystems.PointerEventData)
extern "C"  void ObservablePointerUpTrigger_UnityEngine_EventSystems_IPointerUpHandler_OnPointerUp_m897839738 (ObservablePointerUpTrigger_t2862374692 * __this, PointerEventData_t3205101634 * ___eventData0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UniRx.IObservable`1<UnityEngine.EventSystems.PointerEventData> UniRx.Triggers.ObservablePointerUpTrigger::OnPointerUpAsObservable()
extern "C"  Il2CppObject* ObservablePointerUpTrigger_OnPointerUpAsObservable_m2358099113 (ObservablePointerUpTrigger_t2862374692 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.Triggers.ObservablePointerUpTrigger::RaiseOnCompletedOnDestroy()
extern "C"  void ObservablePointerUpTrigger_RaiseOnCompletedOnDestroy_m3544679954 (ObservablePointerUpTrigger_t2862374692 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
