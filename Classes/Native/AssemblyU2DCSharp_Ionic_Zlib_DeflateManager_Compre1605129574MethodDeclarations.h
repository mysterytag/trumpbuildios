﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Ionic.Zlib.DeflateManager/CompressFunc
struct CompressFunc_t1605129574;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t537683269;
// System.AsyncCallback
struct AsyncCallback_t1363551830;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_IntPtr676692020.h"
#include "AssemblyU2DCSharp_Ionic_Zlib_BlockState1531979153.h"
#include "AssemblyU2DCSharp_Ionic_Zlib_FlushType3984958891.h"
#include "mscorlib_System_AsyncCallback1363551830.h"

// System.Void Ionic.Zlib.DeflateManager/CompressFunc::.ctor(System.Object,System.IntPtr)
extern "C"  void CompressFunc__ctor_m1402710979 (CompressFunc_t1605129574 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Ionic.Zlib.BlockState Ionic.Zlib.DeflateManager/CompressFunc::Invoke(Ionic.Zlib.FlushType)
extern "C"  int32_t CompressFunc_Invoke_m2420346990 (CompressFunc_t1605129574 * __this, int32_t ___flush0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
extern "C" int32_t pinvoke_delegate_wrapper_CompressFunc_t1605129574(Il2CppObject* delegate, int32_t ___flush0);
// System.IAsyncResult Ionic.Zlib.DeflateManager/CompressFunc::BeginInvoke(Ionic.Zlib.FlushType,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * CompressFunc_BeginInvoke_m161631103 (CompressFunc_t1605129574 * __this, int32_t ___flush0, AsyncCallback_t1363551830 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Ionic.Zlib.BlockState Ionic.Zlib.DeflateManager/CompressFunc::EndInvoke(System.IAsyncResult)
extern "C"  int32_t CompressFunc_EndInvoke_m2884141931 (CompressFunc_t1605129574 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
