﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CellUtils.CellJoinDisposable`1<System.Object>
struct CellJoinDisposable_1_t3736493403;

#include "codegen/il2cpp-codegen.h"

// System.Void CellUtils.CellJoinDisposable`1<System.Object>::.ctor()
extern "C"  void CellJoinDisposable_1__ctor_m2402304747_gshared (CellJoinDisposable_1_t3736493403 * __this, const MethodInfo* method);
#define CellJoinDisposable_1__ctor_m2402304747(__this, method) ((  void (*) (CellJoinDisposable_1_t3736493403 *, const MethodInfo*))CellJoinDisposable_1__ctor_m2402304747_gshared)(__this, method)
