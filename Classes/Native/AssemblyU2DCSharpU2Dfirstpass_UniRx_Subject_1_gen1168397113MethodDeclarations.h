﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Subject`1<UnityEngine.Vector3>
struct Subject_1_t1168397113;
// System.Exception
struct Exception_t1967233988;
// System.IDisposable
struct IDisposable_t1628921374;
// UniRx.IObserver`1<UnityEngine.Vector3>
struct IObserver_1_t1442361396;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Exception1967233988.h"
#include "UnityEngine_UnityEngine_Vector33525329789.h"

// System.Void UniRx.Subject`1<UnityEngine.Vector3>::.ctor()
extern "C"  void Subject_1__ctor_m1883217779_gshared (Subject_1_t1168397113 * __this, const MethodInfo* method);
#define Subject_1__ctor_m1883217779(__this, method) ((  void (*) (Subject_1_t1168397113 *, const MethodInfo*))Subject_1__ctor_m1883217779_gshared)(__this, method)
// System.Boolean UniRx.Subject`1<UnityEngine.Vector3>::get_HasObservers()
extern "C"  bool Subject_1_get_HasObservers_m359805753_gshared (Subject_1_t1168397113 * __this, const MethodInfo* method);
#define Subject_1_get_HasObservers_m359805753(__this, method) ((  bool (*) (Subject_1_t1168397113 *, const MethodInfo*))Subject_1_get_HasObservers_m359805753_gshared)(__this, method)
// System.Void UniRx.Subject`1<UnityEngine.Vector3>::OnCompleted()
extern "C"  void Subject_1_OnCompleted_m941895869_gshared (Subject_1_t1168397113 * __this, const MethodInfo* method);
#define Subject_1_OnCompleted_m941895869(__this, method) ((  void (*) (Subject_1_t1168397113 *, const MethodInfo*))Subject_1_OnCompleted_m941895869_gshared)(__this, method)
// System.Void UniRx.Subject`1<UnityEngine.Vector3>::OnError(System.Exception)
extern "C"  void Subject_1_OnError_m3871518314_gshared (Subject_1_t1168397113 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method);
#define Subject_1_OnError_m3871518314(__this, ___error0, method) ((  void (*) (Subject_1_t1168397113 *, Exception_t1967233988 *, const MethodInfo*))Subject_1_OnError_m3871518314_gshared)(__this, ___error0, method)
// System.Void UniRx.Subject`1<UnityEngine.Vector3>::OnNext(T)
extern "C"  void Subject_1_OnNext_m126789979_gshared (Subject_1_t1168397113 * __this, Vector3_t3525329789  ___value0, const MethodInfo* method);
#define Subject_1_OnNext_m126789979(__this, ___value0, method) ((  void (*) (Subject_1_t1168397113 *, Vector3_t3525329789 , const MethodInfo*))Subject_1_OnNext_m126789979_gshared)(__this, ___value0, method)
// System.IDisposable UniRx.Subject`1<UnityEngine.Vector3>::Subscribe(UniRx.IObserver`1<T>)
extern "C"  Il2CppObject * Subject_1_Subscribe_m2168115378_gshared (Subject_1_t1168397113 * __this, Il2CppObject* ___observer0, const MethodInfo* method);
#define Subject_1_Subscribe_m2168115378(__this, ___observer0, method) ((  Il2CppObject * (*) (Subject_1_t1168397113 *, Il2CppObject*, const MethodInfo*))Subject_1_Subscribe_m2168115378_gshared)(__this, ___observer0, method)
// System.Void UniRx.Subject`1<UnityEngine.Vector3>::Dispose()
extern "C"  void Subject_1_Dispose_m1489337072_gshared (Subject_1_t1168397113 * __this, const MethodInfo* method);
#define Subject_1_Dispose_m1489337072(__this, method) ((  void (*) (Subject_1_t1168397113 *, const MethodInfo*))Subject_1_Dispose_m1489337072_gshared)(__this, method)
// System.Void UniRx.Subject`1<UnityEngine.Vector3>::ThrowIfDisposed()
extern "C"  void Subject_1_ThrowIfDisposed_m1878078585_gshared (Subject_1_t1168397113 * __this, const MethodInfo* method);
#define Subject_1_ThrowIfDisposed_m1878078585(__this, method) ((  void (*) (Subject_1_t1168397113 *, const MethodInfo*))Subject_1_ThrowIfDisposed_m1878078585_gshared)(__this, method)
// System.Boolean UniRx.Subject`1<UnityEngine.Vector3>::IsRequiredSubscribeOnCurrentThread()
extern "C"  bool Subject_1_IsRequiredSubscribeOnCurrentThread_m3997672432_gshared (Subject_1_t1168397113 * __this, const MethodInfo* method);
#define Subject_1_IsRequiredSubscribeOnCurrentThread_m3997672432(__this, method) ((  bool (*) (Subject_1_t1168397113 *, const MethodInfo*))Subject_1_IsRequiredSubscribeOnCurrentThread_m3997672432_gshared)(__this, method)
