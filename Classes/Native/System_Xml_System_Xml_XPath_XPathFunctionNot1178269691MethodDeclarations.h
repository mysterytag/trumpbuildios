﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Xml.XPath.XPathFunctionNot
struct XPathFunctionNot_t1178269691;
// System.Xml.XPath.FunctionArguments
struct FunctionArguments_t3402201403;
// System.Object
struct Il2CppObject;
// System.Xml.XPath.BaseIterator
struct BaseIterator_t3696600956;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "System_Xml_System_Xml_XPath_FunctionArguments3402201403.h"
#include "System_Xml_System_Xml_XPath_BaseIterator3696600956.h"

// System.Void System.Xml.XPath.XPathFunctionNot::.ctor(System.Xml.XPath.FunctionArguments)
extern "C"  void XPathFunctionNot__ctor_m1396109829 (XPathFunctionNot_t1178269691 * __this, FunctionArguments_t3402201403 * ___args0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Xml.XPath.XPathFunctionNot::get_Peer()
extern "C"  bool XPathFunctionNot_get_Peer_m2256221739 (XPathFunctionNot_t1178269691 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Xml.XPath.XPathFunctionNot::Evaluate(System.Xml.XPath.BaseIterator)
extern "C"  Il2CppObject * XPathFunctionNot_Evaluate_m2620128096 (XPathFunctionNot_t1178269691 * __this, BaseIterator_t3696600956 * ___iter0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Xml.XPath.XPathFunctionNot::ToString()
extern "C"  String_t* XPathFunctionNot_ToString_m1390051569 (XPathFunctionNot_t1178269691 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
