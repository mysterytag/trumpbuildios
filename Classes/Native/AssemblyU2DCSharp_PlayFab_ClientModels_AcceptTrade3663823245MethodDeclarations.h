﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.ClientModels.AcceptTradeResponse
struct AcceptTradeResponse_t3663823245;
// PlayFab.ClientModels.TradeInfo
struct TradeInfo_t1933812770;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_TradeInfo1933812770.h"

// System.Void PlayFab.ClientModels.AcceptTradeResponse::.ctor()
extern "C"  void AcceptTradeResponse__ctor_m4221970876 (AcceptTradeResponse_t3663823245 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PlayFab.ClientModels.TradeInfo PlayFab.ClientModels.AcceptTradeResponse::get_Trade()
extern "C"  TradeInfo_t1933812770 * AcceptTradeResponse_get_Trade_m2983538124 (AcceptTradeResponse_t3663823245 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.AcceptTradeResponse::set_Trade(PlayFab.ClientModels.TradeInfo)
extern "C"  void AcceptTradeResponse_set_Trade_m2763527053 (AcceptTradeResponse_t3663823245 * __this, TradeInfo_t1933812770 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
