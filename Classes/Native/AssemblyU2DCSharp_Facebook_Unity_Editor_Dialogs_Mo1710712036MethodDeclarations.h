﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Facebook.Unity.Editor.Dialogs.MockLoginDialog/<SendSuccessResult>c__AnonStorey54
struct U3CSendSuccessResultU3Ec__AnonStorey54_t1710712036;
// Facebook.Unity.IGraphResult
struct IGraphResult_t2342947041;

#include "codegen/il2cpp-codegen.h"

// System.Void Facebook.Unity.Editor.Dialogs.MockLoginDialog/<SendSuccessResult>c__AnonStorey54::.ctor()
extern "C"  void U3CSendSuccessResultU3Ec__AnonStorey54__ctor_m2021501228 (U3CSendSuccessResultU3Ec__AnonStorey54_t1710712036 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Facebook.Unity.Editor.Dialogs.MockLoginDialog/<SendSuccessResult>c__AnonStorey54::<>m__12(Facebook.Unity.IGraphResult)
extern "C"  void U3CSendSuccessResultU3Ec__AnonStorey54_U3CU3Em__12_m2643414201 (U3CSendSuccessResultU3Ec__AnonStorey54_t1710712036 * __this, Il2CppObject * ___permResult0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
