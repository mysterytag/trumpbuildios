﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GameController
struct GameController_t2782302542;
// System.Collections.IEnumerator
struct IEnumerator_t287207039;
// InputEventDescriptor
struct InputEventDescriptor_t4263964831;
// UnityEngine.SpriteRenderer
struct SpriteRenderer_t2223784725;
// UnityEngine.GameObject
struct GameObject_t4012695102;
// System.String
struct String_t;
// UnityEngine.Transform
struct Transform_t284553113;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_InputEventDescriptor4263964831.h"
#include "UnityEngine_UnityEngine_SpriteRenderer2223784725.h"
#include "UnityEngine_UnityEngine_Color1588175760.h"
#include "UnityEngine_UnityEngine_GameObject4012695102.h"
#include "UnityEngine_UnityEngine_Vector23525329788.h"
#include "mscorlib_System_String968488902.h"
#include "UnityEngine_UnityEngine_Transform284553113.h"

// System.Void GameController::.ctor()
extern "C"  void GameController__ctor_m4168274701 (GameController_t2782302542 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameController::.cctor()
extern "C"  void GameController__cctor_m4180367936 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GameController::CheckInternetMain()
extern "C"  bool GameController_CheckInternetMain_m2598239033 (GameController_t2782302542 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator GameController::InternetCor()
extern "C"  Il2CppObject * GameController_InternetCor_m15341736 (GameController_t2782302542 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameController::isNetwork()
extern "C"  void GameController_isNetwork_m2257302511 (GameController_t2782302542 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single GameController::get_dropSpawnTime()
extern "C"  float GameController_get_dropSpawnTime_m495506063 (GameController_t2782302542 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameController::Initialize()
extern "C"  void GameController_Initialize_m3623117735 (GameController_t2782302542 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameController::CancelNotification()
extern "C"  void GameController_CancelNotification_m1215094108 (GameController_t2782302542 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameController::InitNotifications()
extern "C"  void GameController_InitNotifications_m4294857955 (GameController_t2782302542 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameController::UpdateVisuals()
extern "C"  void GameController_UpdateVisuals_m3313240021 (GameController_t2782302542 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameController::InputHandler(InputEventDescriptor)
extern "C"  void GameController_InputHandler_m1154135800 (GameController_t2782302542 * __this, InputEventDescriptor_t4263964831 * ___inputEvent0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameController::UpdateHolder()
extern "C"  void GameController_UpdateHolder_m1183032620 (GameController_t2782302542 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameController::UpdateBill()
extern "C"  void GameController_UpdateBill_m2585928839 (GameController_t2782302542 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameController::SetGoldBill()
extern "C"  void GameController_SetGoldBill_m2902432116 (GameController_t2782302542 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameController::LoadCity()
extern "C"  void GameController_LoadCity_m2247614664 (GameController_t2782302542 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameController::UpdadeCity()
extern "C"  void GameController_UpdadeCity_m499647227 (GameController_t2782302542 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator GameController::UpdateCityLevel(UnityEngine.SpriteRenderer)
extern "C"  Il2CppObject * GameController_UpdateCityLevel_m2770830388 (GameController_t2782302542 * __this, SpriteRenderer_t2223784725 * ___cityRenderer0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameController::OpenRateScreen()
extern "C"  void GameController_OpenRateScreen_m4257366765 (GameController_t2782302542 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator GameController::changeColorCoroutine(UnityEngine.Color)
extern "C"  Il2CppObject * GameController_changeColorCoroutine_m404008098 (GameController_t2782302542 * __this, Color_t1588175760  ___color0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameController::Update()
extern "C"  void GameController_Update_m2094358944 (GameController_t2782302542 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameController::SpawnRandomDrop()
extern "C"  void GameController_SpawnRandomDrop_m706139288 (GameController_t2782302542 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameController::SpawnMoneyDrop(UnityEngine.GameObject,UnityEngine.GameObject)
extern "C"  void GameController_SpawnMoneyDrop_m1685976123 (GameController_t2782302542 * __this, GameObject_t4012695102 * ___drop00, GameObject_t4012695102 * ___drop11, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameController::DestroyMoneyDrop(UnityEngine.GameObject)
extern "C"  void GameController_DestroyMoneyDrop_m210671044 (GameController_t2782302542 * __this, GameObject_t4012695102 * ___drop0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameController::StartGrip(UnityEngine.Vector2)
extern "C"  void GameController_StartGrip_m1826015323 (GameController_t2782302542 * __this, Vector2_t3525329788  ____gripPosition0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameController::StopGrip()
extern "C"  void GameController_StopGrip_m1236824843 (GameController_t2782302542 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameController::ReleaseMoney()
extern "C"  void GameController_ReleaseMoney_m2957863248 (GameController_t2782302542 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator GameController::ReleaseMoneyCoroutine()
extern "C"  Il2CppObject * GameController_ReleaseMoneyCoroutine_m3499073282 (GameController_t2782302542 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameController::SendMoney(System.Single)
extern "C"  void GameController_SendMoney_m936665928 (GameController_t2782302542 * __this, float ___gripDelta0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator GameController::SetCitySpeed()
extern "C"  Il2CppObject * GameController_SetCitySpeed_m1093632953 (GameController_t2782302542 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameController::AddBillMoney()
extern "C"  void GameController_AddBillMoney_m2496863727 (GameController_t2782302542 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameController::ShowPopText(System.String,System.Single)
extern "C"  void GameController_ShowPopText_m2743529755 (GameController_t2782302542 * __this, String_t* ___message0, float ___scale1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameController::ShowMultiplierText(System.Int32)
extern "C"  void GameController_ShowMultiplierText_m2214965395 (GameController_t2782302542 * __this, int32_t ___multiplier0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator GameController::SendMoneyCoroutine(UnityEngine.Transform,System.Single)
extern "C"  Il2CppObject * GameController_SendMoneyCoroutine_m4165071619 (GameController_t2782302542 * __this, Transform_t284553113 * ___moneyTransform0, float ___gripDelta1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameController::OnApplicationPause(System.Boolean)
extern "C"  void GameController_OnApplicationPause_m3470365651 (GameController_t2782302542 * __this, bool ___pauseStatus0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator GameController::ShowMultiplierAnimation(UnityEngine.GameObject)
extern "C"  Il2CppObject * GameController_ShowMultiplierAnimation_m2789140001 (GameController_t2782302542 * __this, GameObject_t4012695102 * ___mult0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator GameController::GoldBillMMM(System.Int32)
extern "C"  Il2CppObject * GameController_GoldBillMMM_m3211640442 (GameController_t2782302542 * __this, int32_t ___sec0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator GameController::WaitForMMM()
extern "C"  Il2CppObject * GameController_WaitForMMM_m1651895096 (GameController_t2782302542 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator GameController::AchievementReporter()
extern "C"  Il2CppObject * GameController_AchievementReporter_m3652149907 (GameController_t2782302542 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator GameController::MMMCounter(System.Single)
extern "C"  Il2CppObject * GameController_MMMCounter_m1752928093 (GameController_t2782302542 * __this, float ___time0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameController::OnApplicationQuit()
extern "C"  void GameController_OnApplicationQuit_m3850245387 (GameController_t2782302542 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameController::<Initialize>m__FB(System.Int64)
extern "C"  void GameController_U3CInitializeU3Em__FB_m3754815060 (Il2CppObject * __this /* static, unused */, int64_t ___l0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameController::<Initialize>m__FC(System.Double)
extern "C"  void GameController_U3CInitializeU3Em__FC_m4107951071 (GameController_t2782302542 * __this, double ___d0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameController::<Update>m__FD()
extern "C"  void GameController_U3CUpdateU3Em__FD_m2403979069 (GameController_t2782302542 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
