﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.ClientModels.LinkPSNAccountResult
struct LinkPSNAccountResult_t2251115497;

#include "codegen/il2cpp-codegen.h"

// System.Void PlayFab.ClientModels.LinkPSNAccountResult::.ctor()
extern "C"  void LinkPSNAccountResult__ctor_m1331369332 (LinkPSNAccountResult_t2251115497 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
