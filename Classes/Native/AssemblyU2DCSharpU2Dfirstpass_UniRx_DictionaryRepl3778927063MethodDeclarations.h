﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Object
struct Il2CppObject;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_DictionaryRepl3778927063.h"
#include "mscorlib_System_Object837106420.h"

// System.Void UniRx.DictionaryReplaceEvent`2<System.Object,System.Object>::.ctor(TKey,TValue,TValue)
extern "C"  void DictionaryReplaceEvent_2__ctor_m2258921096_gshared (DictionaryReplaceEvent_2_t3778927063 * __this, Il2CppObject * ___key0, Il2CppObject * ___oldValue1, Il2CppObject * ___newValue2, const MethodInfo* method);
#define DictionaryReplaceEvent_2__ctor_m2258921096(__this, ___key0, ___oldValue1, ___newValue2, method) ((  void (*) (DictionaryReplaceEvent_2_t3778927063 *, Il2CppObject *, Il2CppObject *, Il2CppObject *, const MethodInfo*))DictionaryReplaceEvent_2__ctor_m2258921096_gshared)(__this, ___key0, ___oldValue1, ___newValue2, method)
// TKey UniRx.DictionaryReplaceEvent`2<System.Object,System.Object>::get_Key()
extern "C"  Il2CppObject * DictionaryReplaceEvent_2_get_Key_m555528929_gshared (DictionaryReplaceEvent_2_t3778927063 * __this, const MethodInfo* method);
#define DictionaryReplaceEvent_2_get_Key_m555528929(__this, method) ((  Il2CppObject * (*) (DictionaryReplaceEvent_2_t3778927063 *, const MethodInfo*))DictionaryReplaceEvent_2_get_Key_m555528929_gshared)(__this, method)
// System.Void UniRx.DictionaryReplaceEvent`2<System.Object,System.Object>::set_Key(TKey)
extern "C"  void DictionaryReplaceEvent_2_set_Key_m2934339144_gshared (DictionaryReplaceEvent_2_t3778927063 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define DictionaryReplaceEvent_2_set_Key_m2934339144(__this, ___value0, method) ((  void (*) (DictionaryReplaceEvent_2_t3778927063 *, Il2CppObject *, const MethodInfo*))DictionaryReplaceEvent_2_set_Key_m2934339144_gshared)(__this, ___value0, method)
// TValue UniRx.DictionaryReplaceEvent`2<System.Object,System.Object>::get_OldValue()
extern "C"  Il2CppObject * DictionaryReplaceEvent_2_get_OldValue_m2992843452_gshared (DictionaryReplaceEvent_2_t3778927063 * __this, const MethodInfo* method);
#define DictionaryReplaceEvent_2_get_OldValue_m2992843452(__this, method) ((  Il2CppObject * (*) (DictionaryReplaceEvent_2_t3778927063 *, const MethodInfo*))DictionaryReplaceEvent_2_get_OldValue_m2992843452_gshared)(__this, method)
// System.Void UniRx.DictionaryReplaceEvent`2<System.Object,System.Object>::set_OldValue(TValue)
extern "C"  void DictionaryReplaceEvent_2_set_OldValue_m78777627_gshared (DictionaryReplaceEvent_2_t3778927063 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define DictionaryReplaceEvent_2_set_OldValue_m78777627(__this, ___value0, method) ((  void (*) (DictionaryReplaceEvent_2_t3778927063 *, Il2CppObject *, const MethodInfo*))DictionaryReplaceEvent_2_set_OldValue_m78777627_gshared)(__this, ___value0, method)
// TValue UniRx.DictionaryReplaceEvent`2<System.Object,System.Object>::get_NewValue()
extern "C"  Il2CppObject * DictionaryReplaceEvent_2_get_NewValue_m1710294307_gshared (DictionaryReplaceEvent_2_t3778927063 * __this, const MethodInfo* method);
#define DictionaryReplaceEvent_2_get_NewValue_m1710294307(__this, method) ((  Il2CppObject * (*) (DictionaryReplaceEvent_2_t3778927063 *, const MethodInfo*))DictionaryReplaceEvent_2_get_NewValue_m1710294307_gshared)(__this, method)
// System.Void UniRx.DictionaryReplaceEvent`2<System.Object,System.Object>::set_NewValue(TValue)
extern "C"  void DictionaryReplaceEvent_2_set_NewValue_m1905671746_gshared (DictionaryReplaceEvent_2_t3778927063 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define DictionaryReplaceEvent_2_set_NewValue_m1905671746(__this, ___value0, method) ((  void (*) (DictionaryReplaceEvent_2_t3778927063 *, Il2CppObject *, const MethodInfo*))DictionaryReplaceEvent_2_set_NewValue_m1905671746_gshared)(__this, ___value0, method)
// System.String UniRx.DictionaryReplaceEvent`2<System.Object,System.Object>::ToString()
extern "C"  String_t* DictionaryReplaceEvent_2_ToString_m3153611802_gshared (DictionaryReplaceEvent_2_t3778927063 * __this, const MethodInfo* method);
#define DictionaryReplaceEvent_2_ToString_m3153611802(__this, method) ((  String_t* (*) (DictionaryReplaceEvent_2_t3778927063 *, const MethodInfo*))DictionaryReplaceEvent_2_ToString_m3153611802_gshared)(__this, method)
// System.Int32 UniRx.DictionaryReplaceEvent`2<System.Object,System.Object>::GetHashCode()
extern "C"  int32_t DictionaryReplaceEvent_2_GetHashCode_m3666249368_gshared (DictionaryReplaceEvent_2_t3778927063 * __this, const MethodInfo* method);
#define DictionaryReplaceEvent_2_GetHashCode_m3666249368(__this, method) ((  int32_t (*) (DictionaryReplaceEvent_2_t3778927063 *, const MethodInfo*))DictionaryReplaceEvent_2_GetHashCode_m3666249368_gshared)(__this, method)
// System.Boolean UniRx.DictionaryReplaceEvent`2<System.Object,System.Object>::Equals(UniRx.DictionaryReplaceEvent`2<TKey,TValue>)
extern "C"  bool DictionaryReplaceEvent_2_Equals_m4080823976_gshared (DictionaryReplaceEvent_2_t3778927063 * __this, DictionaryReplaceEvent_2_t3778927063  ___other0, const MethodInfo* method);
#define DictionaryReplaceEvent_2_Equals_m4080823976(__this, ___other0, method) ((  bool (*) (DictionaryReplaceEvent_2_t3778927063 *, DictionaryReplaceEvent_2_t3778927063 , const MethodInfo*))DictionaryReplaceEvent_2_Equals_m4080823976_gshared)(__this, ___other0, method)
