﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.CreateObservable`1<System.Boolean>
struct CreateObservable_1_t1414948727;
// System.Func`2<UniRx.IObserver`1<System.Boolean>,System.IDisposable>
struct Func_2_t2118199298;
// System.IDisposable
struct IDisposable_t1628921374;
// UniRx.IObserver`1<System.Boolean>
struct IObserver_1_t2423004244;

#include "codegen/il2cpp-codegen.h"

// System.Void UniRx.Operators.CreateObservable`1<System.Boolean>::.ctor(System.Func`2<UniRx.IObserver`1<T>,System.IDisposable>)
extern "C"  void CreateObservable_1__ctor_m3037764018_gshared (CreateObservable_1_t1414948727 * __this, Func_2_t2118199298 * ___subscribe0, const MethodInfo* method);
#define CreateObservable_1__ctor_m3037764018(__this, ___subscribe0, method) ((  void (*) (CreateObservable_1_t1414948727 *, Func_2_t2118199298 *, const MethodInfo*))CreateObservable_1__ctor_m3037764018_gshared)(__this, ___subscribe0, method)
// System.Void UniRx.Operators.CreateObservable`1<System.Boolean>::.ctor(System.Func`2<UniRx.IObserver`1<T>,System.IDisposable>,System.Boolean)
extern "C"  void CreateObservable_1__ctor_m2570510219_gshared (CreateObservable_1_t1414948727 * __this, Func_2_t2118199298 * ___subscribe0, bool ___isRequiredSubscribeOnCurrentThread1, const MethodInfo* method);
#define CreateObservable_1__ctor_m2570510219(__this, ___subscribe0, ___isRequiredSubscribeOnCurrentThread1, method) ((  void (*) (CreateObservable_1_t1414948727 *, Func_2_t2118199298 *, bool, const MethodInfo*))CreateObservable_1__ctor_m2570510219_gshared)(__this, ___subscribe0, ___isRequiredSubscribeOnCurrentThread1, method)
// System.IDisposable UniRx.Operators.CreateObservable`1<System.Boolean>::SubscribeCore(UniRx.IObserver`1<T>,System.IDisposable)
extern "C"  Il2CppObject * CreateObservable_1_SubscribeCore_m1273965007_gshared (CreateObservable_1_t1414948727 * __this, Il2CppObject* ___observer0, Il2CppObject * ___cancel1, const MethodInfo* method);
#define CreateObservable_1_SubscribeCore_m1273965007(__this, ___observer0, ___cancel1, method) ((  Il2CppObject * (*) (CreateObservable_1_t1414948727 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))CreateObservable_1_SubscribeCore_m1273965007_gshared)(__this, ___observer0, ___cancel1, method)
