﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_ValueType4014882752.h"
#include "AssemblyU2DCSharp_GameAnalyticsSDK_Settings_Messag3163203794.h"
#include "AssemblyU2DCSharp_GameAnalyticsSDK_Settings_HelpTy3291355544.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameAnalyticsSDK.Settings/HelpInfo
struct  HelpInfo_t3569517487 
{
public:
	// System.String GameAnalyticsSDK.Settings/HelpInfo::Message
	String_t* ___Message_0;
	// GameAnalyticsSDK.Settings/MessageTypes GameAnalyticsSDK.Settings/HelpInfo::MsgType
	int32_t ___MsgType_1;
	// GameAnalyticsSDK.Settings/HelpTypes GameAnalyticsSDK.Settings/HelpInfo::HelpType
	int32_t ___HelpType_2;

public:
	inline static int32_t get_offset_of_Message_0() { return static_cast<int32_t>(offsetof(HelpInfo_t3569517487, ___Message_0)); }
	inline String_t* get_Message_0() const { return ___Message_0; }
	inline String_t** get_address_of_Message_0() { return &___Message_0; }
	inline void set_Message_0(String_t* value)
	{
		___Message_0 = value;
		Il2CppCodeGenWriteBarrier(&___Message_0, value);
	}

	inline static int32_t get_offset_of_MsgType_1() { return static_cast<int32_t>(offsetof(HelpInfo_t3569517487, ___MsgType_1)); }
	inline int32_t get_MsgType_1() const { return ___MsgType_1; }
	inline int32_t* get_address_of_MsgType_1() { return &___MsgType_1; }
	inline void set_MsgType_1(int32_t value)
	{
		___MsgType_1 = value;
	}

	inline static int32_t get_offset_of_HelpType_2() { return static_cast<int32_t>(offsetof(HelpInfo_t3569517487, ___HelpType_2)); }
	inline int32_t get_HelpType_2() const { return ___HelpType_2; }
	inline int32_t* get_address_of_HelpType_2() { return &___HelpType_2; }
	inline void set_HelpType_2(int32_t value)
	{
		___HelpType_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for marshalling of: GameAnalyticsSDK.Settings/HelpInfo
struct HelpInfo_t3569517487_marshaled_pinvoke
{
	char* ___Message_0;
	int32_t ___MsgType_1;
	int32_t ___HelpType_2;
};
// Native definition for marshalling of: GameAnalyticsSDK.Settings/HelpInfo
struct HelpInfo_t3569517487_marshaled_com
{
	uint16_t* ___Message_0;
	int32_t ___MsgType_1;
	int32_t ___HelpType_2;
};
