﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "System_System_Collections_Generic_Stack_1_Enumerat2195698409MethodDeclarations.h"

// System.Void System.Collections.Generic.Stack`1/Enumerator<System.Collections.Generic.KeyValuePair`2<System.IDisposable,System.Action`1<System.Object>>>::.ctor(System.Collections.Generic.Stack`1<T>)
#define Enumerator__ctor_m868109442(__this, ___t0, method) ((  void (*) (Enumerator_t2497810744 *, Stack_1_t1890507522 *, const MethodInfo*))Enumerator__ctor_m3970752666_gshared)(__this, ___t0, method)
// System.Void System.Collections.Generic.Stack`1/Enumerator<System.Collections.Generic.KeyValuePair`2<System.IDisposable,System.Action`1<System.Object>>>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m227669574(__this, method) ((  void (*) (Enumerator_t2497810744 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m1906801758_gshared)(__this, method)
// System.Object System.Collections.Generic.Stack`1/Enumerator<System.Collections.Generic.KeyValuePair`2<System.IDisposable,System.Action`1<System.Object>>>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m2738924146(__this, method) ((  Il2CppObject * (*) (Enumerator_t2497810744 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m3933016266_gshared)(__this, method)
// System.Void System.Collections.Generic.Stack`1/Enumerator<System.Collections.Generic.KeyValuePair`2<System.IDisposable,System.Action`1<System.Object>>>::Dispose()
#define Enumerator_Dispose_m456796017(__this, method) ((  void (*) (Enumerator_t2497810744 *, const MethodInfo*))Enumerator_Dispose_m2852088409_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Stack`1/Enumerator<System.Collections.Generic.KeyValuePair`2<System.IDisposable,System.Action`1<System.Object>>>::MoveNext()
#define Enumerator_MoveNext_m319491870(__this, method) ((  bool (*) (Enumerator_t2497810744 *, const MethodInfo*))Enumerator_MoveNext_m3622732982_gshared)(__this, method)
// T System.Collections.Generic.Stack`1/Enumerator<System.Collections.Generic.KeyValuePair`2<System.IDisposable,System.Action`1<System.Object>>>::get_Current()
#define Enumerator_get_Current_m3015818145(__this, method) ((  KeyValuePair_2_t3615068783  (*) (Enumerator_t2497810744 *, const MethodInfo*))Enumerator_get_Current_m2377937865_gshared)(__this, method)
