﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Cell`1<System.Single>
struct Cell_1_t1140306653;
// WindowManager
struct WindowManager_t3316821373;
// ConnectionCollector
struct ConnectionCollector_t444796719;
// BottomPanel
struct BottomPanel_t1044499065;
// UniRx.Subject`1<UniRx.Unit>
struct Subject_1_t201353362;
// Message
struct Message_t2619578343;
// IVkConnector
struct IVkConnector_t2786758927;
// GlobalStat
struct GlobalStat_t1134678199;

#include "AssemblyU2DCSharp_WindowBase3855465217.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WallPostWindow
struct  WallPostWindow_t251424314  : public WindowBase_t3855465217
{
public:
	// Cell`1<System.Single> WallPostWindow::Cost
	Cell_1_t1140306653 * ___Cost_6;
	// WindowManager WallPostWindow::WindowManager
	WindowManager_t3316821373 * ___WindowManager_7;
	// ConnectionCollector WallPostWindow::ConnectionCollector
	ConnectionCollector_t444796719 * ___ConnectionCollector_8;
	// BottomPanel WallPostWindow::BottomPanel
	BottomPanel_t1044499065 * ___BottomPanel_9;
	// UniRx.Subject`1<UniRx.Unit> WallPostWindow::CloseSubject
	Subject_1_t201353362 * ___CloseSubject_10;
	// UniRx.Subject`1<UniRx.Unit> WallPostWindow::Do
	Subject_1_t201353362 * ___Do_11;
	// Message WallPostWindow::Message
	Message_t2619578343 * ___Message_12;
	// IVkConnector WallPostWindow::vkConnector
	Il2CppObject * ___vkConnector_13;
	// GlobalStat WallPostWindow::GlobalStat
	GlobalStat_t1134678199 * ___GlobalStat_14;

public:
	inline static int32_t get_offset_of_Cost_6() { return static_cast<int32_t>(offsetof(WallPostWindow_t251424314, ___Cost_6)); }
	inline Cell_1_t1140306653 * get_Cost_6() const { return ___Cost_6; }
	inline Cell_1_t1140306653 ** get_address_of_Cost_6() { return &___Cost_6; }
	inline void set_Cost_6(Cell_1_t1140306653 * value)
	{
		___Cost_6 = value;
		Il2CppCodeGenWriteBarrier(&___Cost_6, value);
	}

	inline static int32_t get_offset_of_WindowManager_7() { return static_cast<int32_t>(offsetof(WallPostWindow_t251424314, ___WindowManager_7)); }
	inline WindowManager_t3316821373 * get_WindowManager_7() const { return ___WindowManager_7; }
	inline WindowManager_t3316821373 ** get_address_of_WindowManager_7() { return &___WindowManager_7; }
	inline void set_WindowManager_7(WindowManager_t3316821373 * value)
	{
		___WindowManager_7 = value;
		Il2CppCodeGenWriteBarrier(&___WindowManager_7, value);
	}

	inline static int32_t get_offset_of_ConnectionCollector_8() { return static_cast<int32_t>(offsetof(WallPostWindow_t251424314, ___ConnectionCollector_8)); }
	inline ConnectionCollector_t444796719 * get_ConnectionCollector_8() const { return ___ConnectionCollector_8; }
	inline ConnectionCollector_t444796719 ** get_address_of_ConnectionCollector_8() { return &___ConnectionCollector_8; }
	inline void set_ConnectionCollector_8(ConnectionCollector_t444796719 * value)
	{
		___ConnectionCollector_8 = value;
		Il2CppCodeGenWriteBarrier(&___ConnectionCollector_8, value);
	}

	inline static int32_t get_offset_of_BottomPanel_9() { return static_cast<int32_t>(offsetof(WallPostWindow_t251424314, ___BottomPanel_9)); }
	inline BottomPanel_t1044499065 * get_BottomPanel_9() const { return ___BottomPanel_9; }
	inline BottomPanel_t1044499065 ** get_address_of_BottomPanel_9() { return &___BottomPanel_9; }
	inline void set_BottomPanel_9(BottomPanel_t1044499065 * value)
	{
		___BottomPanel_9 = value;
		Il2CppCodeGenWriteBarrier(&___BottomPanel_9, value);
	}

	inline static int32_t get_offset_of_CloseSubject_10() { return static_cast<int32_t>(offsetof(WallPostWindow_t251424314, ___CloseSubject_10)); }
	inline Subject_1_t201353362 * get_CloseSubject_10() const { return ___CloseSubject_10; }
	inline Subject_1_t201353362 ** get_address_of_CloseSubject_10() { return &___CloseSubject_10; }
	inline void set_CloseSubject_10(Subject_1_t201353362 * value)
	{
		___CloseSubject_10 = value;
		Il2CppCodeGenWriteBarrier(&___CloseSubject_10, value);
	}

	inline static int32_t get_offset_of_Do_11() { return static_cast<int32_t>(offsetof(WallPostWindow_t251424314, ___Do_11)); }
	inline Subject_1_t201353362 * get_Do_11() const { return ___Do_11; }
	inline Subject_1_t201353362 ** get_address_of_Do_11() { return &___Do_11; }
	inline void set_Do_11(Subject_1_t201353362 * value)
	{
		___Do_11 = value;
		Il2CppCodeGenWriteBarrier(&___Do_11, value);
	}

	inline static int32_t get_offset_of_Message_12() { return static_cast<int32_t>(offsetof(WallPostWindow_t251424314, ___Message_12)); }
	inline Message_t2619578343 * get_Message_12() const { return ___Message_12; }
	inline Message_t2619578343 ** get_address_of_Message_12() { return &___Message_12; }
	inline void set_Message_12(Message_t2619578343 * value)
	{
		___Message_12 = value;
		Il2CppCodeGenWriteBarrier(&___Message_12, value);
	}

	inline static int32_t get_offset_of_vkConnector_13() { return static_cast<int32_t>(offsetof(WallPostWindow_t251424314, ___vkConnector_13)); }
	inline Il2CppObject * get_vkConnector_13() const { return ___vkConnector_13; }
	inline Il2CppObject ** get_address_of_vkConnector_13() { return &___vkConnector_13; }
	inline void set_vkConnector_13(Il2CppObject * value)
	{
		___vkConnector_13 = value;
		Il2CppCodeGenWriteBarrier(&___vkConnector_13, value);
	}

	inline static int32_t get_offset_of_GlobalStat_14() { return static_cast<int32_t>(offsetof(WallPostWindow_t251424314, ___GlobalStat_14)); }
	inline GlobalStat_t1134678199 * get_GlobalStat_14() const { return ___GlobalStat_14; }
	inline GlobalStat_t1134678199 ** get_address_of_GlobalStat_14() { return &___GlobalStat_14; }
	inline void set_GlobalStat_14(GlobalStat_t1134678199 * value)
	{
		___GlobalStat_14 = value;
		Il2CppCodeGenWriteBarrier(&___GlobalStat_14, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
