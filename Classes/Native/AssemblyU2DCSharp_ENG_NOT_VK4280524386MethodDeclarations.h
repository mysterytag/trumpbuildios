﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ENG_NOT_VK
struct ENG_NOT_VK_t4280524386;

#include "codegen/il2cpp-codegen.h"

// System.Void ENG_NOT_VK::.ctor()
extern "C"  void ENG_NOT_VK__ctor_m3421964409 (ENG_NOT_VK_t4280524386 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ENG_NOT_VK::Start()
extern "C"  void ENG_NOT_VK_Start_m2369102201 (ENG_NOT_VK_t4280524386 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ENG_NOT_VK::Update()
extern "C"  void ENG_NOT_VK_Update_m433576372 (ENG_NOT_VK_t4280524386 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ENG_NOT_VK::Awake()
extern "C"  void ENG_NOT_VK_Awake_m3659569628 (ENG_NOT_VK_t4280524386 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
