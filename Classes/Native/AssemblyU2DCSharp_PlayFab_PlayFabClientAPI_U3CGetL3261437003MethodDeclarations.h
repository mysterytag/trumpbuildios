﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.PlayFabClientAPI/<GetLeaderboardAroundCurrentUser>c__AnonStorey8F
struct U3CGetLeaderboardAroundCurrentUserU3Ec__AnonStorey8F_t3261437003;
// PlayFab.CallRequestContainer
struct CallRequestContainer_t432318609;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharpU2Dfirstpass_PlayFab_CallRequestCo432318609.h"

// System.Void PlayFab.PlayFabClientAPI/<GetLeaderboardAroundCurrentUser>c__AnonStorey8F::.ctor()
extern "C"  void U3CGetLeaderboardAroundCurrentUserU3Ec__AnonStorey8F__ctor_m1759448936 (U3CGetLeaderboardAroundCurrentUserU3Ec__AnonStorey8F_t3261437003 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/<GetLeaderboardAroundCurrentUser>c__AnonStorey8F::<>m__7C(PlayFab.CallRequestContainer)
extern "C"  void U3CGetLeaderboardAroundCurrentUserU3Ec__AnonStorey8F_U3CU3Em__7C_m3413203506 (U3CGetLeaderboardAroundCurrentUserU3Ec__AnonStorey8F_t3261437003 * __this, CallRequestContainer_t432318609 * ___requestContainer0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
