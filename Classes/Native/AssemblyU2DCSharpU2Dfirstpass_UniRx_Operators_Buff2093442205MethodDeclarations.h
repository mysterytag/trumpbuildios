﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.BufferObservable`2<System.Object,System.Object>
struct BufferObservable_2_t2093442205;
// UniRx.IObservable`1<System.Object>
struct IObservable_1_t595904784;
// System.IDisposable
struct IDisposable_t1628921374;
// UniRx.IObserver`1<System.Collections.Generic.IList`1<System.Object>>
struct IObserver_1_t920630341;

#include "codegen/il2cpp-codegen.h"

// System.Void UniRx.Operators.BufferObservable`2<System.Object,System.Object>::.ctor(UniRx.IObservable`1<TSource>,UniRx.IObservable`1<TWindowBoundary>)
extern "C"  void BufferObservable_2__ctor_m673927692_gshared (BufferObservable_2_t2093442205 * __this, Il2CppObject* ___source0, Il2CppObject* ___windowBoundaries1, const MethodInfo* method);
#define BufferObservable_2__ctor_m673927692(__this, ___source0, ___windowBoundaries1, method) ((  void (*) (BufferObservable_2_t2093442205 *, Il2CppObject*, Il2CppObject*, const MethodInfo*))BufferObservable_2__ctor_m673927692_gshared)(__this, ___source0, ___windowBoundaries1, method)
// System.IDisposable UniRx.Operators.BufferObservable`2<System.Object,System.Object>::SubscribeCore(UniRx.IObserver`1<System.Collections.Generic.IList`1<TSource>>,System.IDisposable)
extern "C"  Il2CppObject * BufferObservable_2_SubscribeCore_m2188808289_gshared (BufferObservable_2_t2093442205 * __this, Il2CppObject* ___observer0, Il2CppObject * ___cancel1, const MethodInfo* method);
#define BufferObservable_2_SubscribeCore_m2188808289(__this, ___observer0, ___cancel1, method) ((  Il2CppObject * (*) (BufferObservable_2_t2093442205 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))BufferObservable_2_SubscribeCore_m2188808289_gshared)(__this, ___observer0, ___cancel1, method)
