﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CellReactiveApi/<AsObservable>c__AnonStorey106`1<System.Object>
struct U3CAsObservableU3Ec__AnonStorey106_1_t706635991;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void CellReactiveApi/<AsObservable>c__AnonStorey106`1<System.Object>::.ctor()
extern "C"  void U3CAsObservableU3Ec__AnonStorey106_1__ctor_m591083181_gshared (U3CAsObservableU3Ec__AnonStorey106_1_t706635991 * __this, const MethodInfo* method);
#define U3CAsObservableU3Ec__AnonStorey106_1__ctor_m591083181(__this, method) ((  void (*) (U3CAsObservableU3Ec__AnonStorey106_1_t706635991 *, const MethodInfo*))U3CAsObservableU3Ec__AnonStorey106_1__ctor_m591083181_gshared)(__this, method)
// T CellReactiveApi/<AsObservable>c__AnonStorey106`1<System.Object>::<>m__17B()
extern "C"  Il2CppObject * U3CAsObservableU3Ec__AnonStorey106_1_U3CU3Em__17B_m28013929_gshared (U3CAsObservableU3Ec__AnonStorey106_1_t706635991 * __this, const MethodInfo* method);
#define U3CAsObservableU3Ec__AnonStorey106_1_U3CU3Em__17B_m28013929(__this, method) ((  Il2CppObject * (*) (U3CAsObservableU3Ec__AnonStorey106_1_t706635991 *, const MethodInfo*))U3CAsObservableU3Ec__AnonStorey106_1_U3CU3Em__17B_m28013929_gshared)(__this, method)
