﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Generic.HashSet`1<System.String>
struct HashSet_1_t3667177573;

#include "AssemblyU2DCSharp_PlayFab_UUnit_UUnitTestCase14187365.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PlayFab.Internal.EventTest
struct  EventTest_t3226111604  : public UUnitTestCase_t14187365
{
public:

public:
};

struct EventTest_t3226111604_StaticFields
{
public:
	// System.Collections.Generic.HashSet`1<System.String> PlayFab.Internal.EventTest::callbacks
	HashSet_1_t3667177573 * ___callbacks_4;

public:
	inline static int32_t get_offset_of_callbacks_4() { return static_cast<int32_t>(offsetof(EventTest_t3226111604_StaticFields, ___callbacks_4)); }
	inline HashSet_1_t3667177573 * get_callbacks_4() const { return ___callbacks_4; }
	inline HashSet_1_t3667177573 ** get_address_of_callbacks_4() { return &___callbacks_4; }
	inline void set_callbacks_4(HashSet_1_t3667177573 * value)
	{
		___callbacks_4 = value;
		Il2CppCodeGenWriteBarrier(&___callbacks_4, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
