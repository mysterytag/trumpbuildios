﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// InstantiationPool`1<System.Object>
struct InstantiationPool_1_t1380750307;
// InstantiatableInPool
struct InstantiatableInPool_t1882737686;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;
// UnityEngine.Transform
struct Transform_t284553113;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_InstantiatableInPool1882737686.h"
#include "mscorlib_System_String968488902.h"
#include "UnityEngine_UnityEngine_Transform284553113.h"
#include "mscorlib_System_Object837106420.h"

// System.Void InstantiationPool`1<System.Object>::.ctor()
extern "C"  void InstantiationPool_1__ctor_m3774412601_gshared (InstantiationPool_1_t1380750307 * __this, const MethodInfo* method);
#define InstantiationPool_1__ctor_m3774412601(__this, method) ((  void (*) (InstantiationPool_1_t1380750307 *, const MethodInfo*))InstantiationPool_1__ctor_m3774412601_gshared)(__this, method)
// System.Void InstantiationPool`1<System.Object>::KillAll()
extern "C"  void InstantiationPool_1_KillAll_m314356602_gshared (InstantiationPool_1_t1380750307 * __this, const MethodInfo* method);
#define InstantiationPool_1_KillAll_m314356602(__this, method) ((  void (*) (InstantiationPool_1_t1380750307 *, const MethodInfo*))InstantiationPool_1_KillAll_m314356602_gshared)(__this, method)
// System.Void InstantiationPool`1<System.Object>::RecycleAll()
extern "C"  void InstantiationPool_1_RecycleAll_m167619065_gshared (InstantiationPool_1_t1380750307 * __this, const MethodInfo* method);
#define InstantiationPool_1_RecycleAll_m167619065(__this, method) ((  void (*) (InstantiationPool_1_t1380750307 *, const MethodInfo*))InstantiationPool_1_RecycleAll_m167619065_gshared)(__this, method)
// System.Void InstantiationPool`1<System.Object>::Recycle(InstantiatableInPool)
extern "C"  void InstantiationPool_1_Recycle_m3703956692_gshared (InstantiationPool_1_t1380750307 * __this, InstantiatableInPool_t1882737686 * ___inst0, const MethodInfo* method);
#define InstantiationPool_1_Recycle_m3703956692(__this, ___inst0, method) ((  void (*) (InstantiationPool_1_t1380750307 *, InstantiatableInPool_t1882737686 *, const MethodInfo*))InstantiationPool_1_Recycle_m3703956692_gshared)(__this, ___inst0, method)
// T InstantiationPool`1<System.Object>::Create(System.String,UnityEngine.Transform)
extern "C"  Il2CppObject * InstantiationPool_1_Create_m1507201747_gshared (InstantiationPool_1_t1380750307 * __this, String_t* ___prefabName0, Transform_t284553113 * ___parent1, const MethodInfo* method);
#define InstantiationPool_1_Create_m1507201747(__this, ___prefabName0, ___parent1, method) ((  Il2CppObject * (*) (InstantiationPool_1_t1380750307 *, String_t*, Transform_t284553113 *, const MethodInfo*))InstantiationPool_1_Create_m1507201747_gshared)(__this, ___prefabName0, ___parent1, method)
// System.Void InstantiationPool`1<System.Object>::<KillAll>m__12F(T)
extern "C"  void InstantiationPool_1_U3CKillAllU3Em__12F_m2237180024_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * ___t0, const MethodInfo* method);
#define InstantiationPool_1_U3CKillAllU3Em__12F_m2237180024(__this /* static, unused */, ___t0, method) ((  void (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))InstantiationPool_1_U3CKillAllU3Em__12F_m2237180024_gshared)(__this /* static, unused */, ___t0, method)
// System.Void InstantiationPool`1<System.Object>::<RecycleAll>m__130(T)
extern "C"  void InstantiationPool_1_U3CRecycleAllU3Em__130_m3480519710_gshared (InstantiationPool_1_t1380750307 * __this, Il2CppObject * ___inst0, const MethodInfo* method);
#define InstantiationPool_1_U3CRecycleAllU3Em__130_m3480519710(__this, ___inst0, method) ((  void (*) (InstantiationPool_1_t1380750307 *, Il2CppObject *, const MethodInfo*))InstantiationPool_1_U3CRecycleAllU3Em__130_m3480519710_gshared)(__this, ___inst0, method)
