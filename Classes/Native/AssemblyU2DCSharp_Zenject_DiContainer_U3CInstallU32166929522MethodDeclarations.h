﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Zenject.DiContainer/<Install>c__AnonStorey18A
struct U3CInstallU3Ec__AnonStorey18A_t2166929522;
// Zenject.IInstaller
struct IInstaller_t2455223476;

#include "codegen/il2cpp-codegen.h"

// System.Void Zenject.DiContainer/<Install>c__AnonStorey18A::.ctor()
extern "C"  void U3CInstallU3Ec__AnonStorey18A__ctor_m2391144107 (U3CInstallU3Ec__AnonStorey18A_t2166929522 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Zenject.DiContainer/<Install>c__AnonStorey18A::<>m__2C7(Zenject.IInstaller)
extern "C"  bool U3CInstallU3Ec__AnonStorey18A_U3CU3Em__2C7_m501749602 (U3CInstallU3Ec__AnonStorey18A_t2166929522 * __this, Il2CppObject * ___x0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
