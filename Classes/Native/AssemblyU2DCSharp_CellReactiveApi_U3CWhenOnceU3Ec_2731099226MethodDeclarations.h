﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CellReactiveApi/<WhenOnce>c__AnonStorey107`1<System.Object>
struct U3CWhenOnceU3Ec__AnonStorey107_1_t2731099226;
// System.IDisposable
struct IDisposable_t1628921374;
// System.Action
struct Action_t437523947;

#include "codegen/il2cpp-codegen.h"
#include "System_Core_System_Action437523947.h"
#include "AssemblyU2DCSharp_Priority3194150340.h"

// System.Void CellReactiveApi/<WhenOnce>c__AnonStorey107`1<System.Object>::.ctor()
extern "C"  void U3CWhenOnceU3Ec__AnonStorey107_1__ctor_m1862142952_gshared (U3CWhenOnceU3Ec__AnonStorey107_1_t2731099226 * __this, const MethodInfo* method);
#define U3CWhenOnceU3Ec__AnonStorey107_1__ctor_m1862142952(__this, method) ((  void (*) (U3CWhenOnceU3Ec__AnonStorey107_1_t2731099226 *, const MethodInfo*))U3CWhenOnceU3Ec__AnonStorey107_1__ctor_m1862142952_gshared)(__this, method)
// System.IDisposable CellReactiveApi/<WhenOnce>c__AnonStorey107`1<System.Object>::<>m__17C(System.Action,Priority)
extern "C"  Il2CppObject * U3CWhenOnceU3Ec__AnonStorey107_1_U3CU3Em__17C_m2009263200_gshared (U3CWhenOnceU3Ec__AnonStorey107_1_t2731099226 * __this, Action_t437523947 * ___reaction0, int32_t ___p1, const MethodInfo* method);
#define U3CWhenOnceU3Ec__AnonStorey107_1_U3CU3Em__17C_m2009263200(__this, ___reaction0, ___p1, method) ((  Il2CppObject * (*) (U3CWhenOnceU3Ec__AnonStorey107_1_t2731099226 *, Action_t437523947 *, int32_t, const MethodInfo*))U3CWhenOnceU3Ec__AnonStorey107_1_U3CU3Em__17C_m2009263200_gshared)(__this, ___reaction0, ___p1, method)
