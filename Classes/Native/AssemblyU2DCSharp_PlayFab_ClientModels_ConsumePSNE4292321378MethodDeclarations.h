﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.ClientModels.ConsumePSNEntitlementsResult
struct ConsumePSNEntitlementsResult_t4292321378;
// System.Collections.Generic.List`1<PlayFab.ClientModels.ItemInstance>
struct List_1_t1322103281;

#include "codegen/il2cpp-codegen.h"

// System.Void PlayFab.ClientModels.ConsumePSNEntitlementsResult::.ctor()
extern "C"  void ConsumePSNEntitlementsResult__ctor_m229601051 (ConsumePSNEntitlementsResult_t4292321378 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<PlayFab.ClientModels.ItemInstance> PlayFab.ClientModels.ConsumePSNEntitlementsResult::get_ItemsGranted()
extern "C"  List_1_t1322103281 * ConsumePSNEntitlementsResult_get_ItemsGranted_m1125965862 (ConsumePSNEntitlementsResult_t4292321378 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.ConsumePSNEntitlementsResult::set_ItemsGranted(System.Collections.Generic.List`1<PlayFab.ClientModels.ItemInstance>)
extern "C"  void ConsumePSNEntitlementsResult_set_ItemsGranted_m2137990219 (ConsumePSNEntitlementsResult_t4292321378 * __this, List_1_t1322103281 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
