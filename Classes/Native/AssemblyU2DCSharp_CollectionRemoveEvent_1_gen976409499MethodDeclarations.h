﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_CollectionRemoveEvent_1_gen898259258MethodDeclarations.h"

// System.Void CollectionRemoveEvent`1<SettingsButton>::.ctor(System.Int32,T)
#define CollectionRemoveEvent_1__ctor_m1440759198(__this, ___index0, ___value1, method) ((  void (*) (CollectionRemoveEvent_1_t976409499 *, int32_t, SettingsButton_t915256661 *, const MethodInfo*))CollectionRemoveEvent_1__ctor_m1174003473_gshared)(__this, ___index0, ___value1, method)
// System.Int32 CollectionRemoveEvent`1<SettingsButton>::get_Index()
#define CollectionRemoveEvent_1_get_Index_m1141461834(__this, method) ((  int32_t (*) (CollectionRemoveEvent_1_t976409499 *, const MethodInfo*))CollectionRemoveEvent_1_get_Index_m2777665697_gshared)(__this, method)
// System.Void CollectionRemoveEvent`1<SettingsButton>::set_Index(System.Int32)
#define CollectionRemoveEvent_1_set_Index_m100263577(__this, ___value0, method) ((  void (*) (CollectionRemoveEvent_1_t976409499 *, int32_t, const MethodInfo*))CollectionRemoveEvent_1_set_Index_m1446049612_gshared)(__this, ___value0, method)
// T CollectionRemoveEvent`1<SettingsButton>::get_Value()
#define CollectionRemoveEvent_1_get_Value_m1684468506(__this, method) ((  SettingsButton_t915256661 * (*) (CollectionRemoveEvent_1_t976409499 *, const MethodInfo*))CollectionRemoveEvent_1_get_Value_m445974639_gshared)(__this, method)
// System.Void CollectionRemoveEvent`1<SettingsButton>::set_Value(T)
#define CollectionRemoveEvent_1_set_Value_m243511319(__this, ___value0, method) ((  void (*) (CollectionRemoveEvent_1_t976409499 *, SettingsButton_t915256661 *, const MethodInfo*))CollectionRemoveEvent_1_set_Value_m347083076_gshared)(__this, ___value0, method)
// System.String CollectionRemoveEvent`1<SettingsButton>::ToString()
#define CollectionRemoveEvent_1_ToString_m688136280(__this, method) ((  String_t* (*) (CollectionRemoveEvent_1_t976409499 *, const MethodInfo*))CollectionRemoveEvent_1_ToString_m2292716331_gshared)(__this, method)
