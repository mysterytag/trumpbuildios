﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.OperatorObservableBase`1<UniRx.DictionaryReplaceEvent`2<System.Object,System.Object>>
struct OperatorObservableBase_1_t2843072034;
// System.IDisposable
struct IDisposable_t1628921374;
// UniRx.IObserver`1<UniRx.DictionaryReplaceEvent`2<System.Object,System.Object>>
struct IObserver_1_t1695958670;

#include "codegen/il2cpp-codegen.h"

// System.Void UniRx.Operators.OperatorObservableBase`1<UniRx.DictionaryReplaceEvent`2<System.Object,System.Object>>::.ctor(System.Boolean)
extern "C"  void OperatorObservableBase_1__ctor_m918933688_gshared (OperatorObservableBase_1_t2843072034 * __this, bool ___isRequiredSubscribeOnCurrentThread0, const MethodInfo* method);
#define OperatorObservableBase_1__ctor_m918933688(__this, ___isRequiredSubscribeOnCurrentThread0, method) ((  void (*) (OperatorObservableBase_1_t2843072034 *, bool, const MethodInfo*))OperatorObservableBase_1__ctor_m918933688_gshared)(__this, ___isRequiredSubscribeOnCurrentThread0, method)
// System.Boolean UniRx.Operators.OperatorObservableBase`1<UniRx.DictionaryReplaceEvent`2<System.Object,System.Object>>::IsRequiredSubscribeOnCurrentThread()
extern "C"  bool OperatorObservableBase_1_IsRequiredSubscribeOnCurrentThread_m1093387114_gshared (OperatorObservableBase_1_t2843072034 * __this, const MethodInfo* method);
#define OperatorObservableBase_1_IsRequiredSubscribeOnCurrentThread_m1093387114(__this, method) ((  bool (*) (OperatorObservableBase_1_t2843072034 *, const MethodInfo*))OperatorObservableBase_1_IsRequiredSubscribeOnCurrentThread_m1093387114_gshared)(__this, method)
// System.IDisposable UniRx.Operators.OperatorObservableBase`1<UniRx.DictionaryReplaceEvent`2<System.Object,System.Object>>::Subscribe(UniRx.IObserver`1<T>)
extern "C"  Il2CppObject * OperatorObservableBase_1_Subscribe_m3161911702_gshared (OperatorObservableBase_1_t2843072034 * __this, Il2CppObject* ___observer0, const MethodInfo* method);
#define OperatorObservableBase_1_Subscribe_m3161911702(__this, ___observer0, method) ((  Il2CppObject * (*) (OperatorObservableBase_1_t2843072034 *, Il2CppObject*, const MethodInfo*))OperatorObservableBase_1_Subscribe_m3161911702_gshared)(__this, ___observer0, method)
