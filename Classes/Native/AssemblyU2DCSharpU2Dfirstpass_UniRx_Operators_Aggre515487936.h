﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UniRx.Operators.AggregateObservable`1<System.Object>
struct AggregateObservable_1_t497085657;
// System.Object
struct Il2CppObject;

#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Oper1187768149.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Operators.AggregateObservable`1/Aggregate<System.Object>
struct  Aggregate_t515487936  : public OperatorObserverBase_2_t1187768149
{
public:
	// UniRx.Operators.AggregateObservable`1<TSource> UniRx.Operators.AggregateObservable`1/Aggregate::parent
	AggregateObservable_1_t497085657 * ___parent_2;
	// TSource UniRx.Operators.AggregateObservable`1/Aggregate::accumulation
	Il2CppObject * ___accumulation_3;
	// System.Boolean UniRx.Operators.AggregateObservable`1/Aggregate::seenValue
	bool ___seenValue_4;

public:
	inline static int32_t get_offset_of_parent_2() { return static_cast<int32_t>(offsetof(Aggregate_t515487936, ___parent_2)); }
	inline AggregateObservable_1_t497085657 * get_parent_2() const { return ___parent_2; }
	inline AggregateObservable_1_t497085657 ** get_address_of_parent_2() { return &___parent_2; }
	inline void set_parent_2(AggregateObservable_1_t497085657 * value)
	{
		___parent_2 = value;
		Il2CppCodeGenWriteBarrier(&___parent_2, value);
	}

	inline static int32_t get_offset_of_accumulation_3() { return static_cast<int32_t>(offsetof(Aggregate_t515487936, ___accumulation_3)); }
	inline Il2CppObject * get_accumulation_3() const { return ___accumulation_3; }
	inline Il2CppObject ** get_address_of_accumulation_3() { return &___accumulation_3; }
	inline void set_accumulation_3(Il2CppObject * value)
	{
		___accumulation_3 = value;
		Il2CppCodeGenWriteBarrier(&___accumulation_3, value);
	}

	inline static int32_t get_offset_of_seenValue_4() { return static_cast<int32_t>(offsetof(Aggregate_t515487936, ___seenValue_4)); }
	inline bool get_seenValue_4() const { return ___seenValue_4; }
	inline bool* get_address_of_seenValue_4() { return &___seenValue_4; }
	inline void set_seenValue_4(bool value)
	{
		___seenValue_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
