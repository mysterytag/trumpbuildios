﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// CellReactiveApi/MapDisposable
struct MapDisposable_t304581564;
// System.Action`1<System.Boolean>
struct Action_1_t359458046;
// CellReactiveApi/<Map>c__AnonStorey10E`2<System.Boolean,System.Boolean>
struct U3CMapU3Ec__AnonStorey10E_2_t4182566112;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CellReactiveApi/<Map>c__AnonStorey10E`2/<Map>c__AnonStorey10F`2<System.Boolean,System.Boolean>
struct  U3CMapU3Ec__AnonStorey10F_2_t985129795  : public Il2CppObject
{
public:
	// CellReactiveApi/MapDisposable CellReactiveApi/<Map>c__AnonStorey10E`2/<Map>c__AnonStorey10F`2::disp
	MapDisposable_t304581564 * ___disp_0;
	// System.Action`1<T2> CellReactiveApi/<Map>c__AnonStorey10E`2/<Map>c__AnonStorey10F`2::reaction
	Action_1_t359458046 * ___reaction_1;
	// CellReactiveApi/<Map>c__AnonStorey10E`2<T,T2> CellReactiveApi/<Map>c__AnonStorey10E`2/<Map>c__AnonStorey10F`2::<>f__ref$270
	U3CMapU3Ec__AnonStorey10E_2_t4182566112 * ___U3CU3Ef__refU24270_2;

public:
	inline static int32_t get_offset_of_disp_0() { return static_cast<int32_t>(offsetof(U3CMapU3Ec__AnonStorey10F_2_t985129795, ___disp_0)); }
	inline MapDisposable_t304581564 * get_disp_0() const { return ___disp_0; }
	inline MapDisposable_t304581564 ** get_address_of_disp_0() { return &___disp_0; }
	inline void set_disp_0(MapDisposable_t304581564 * value)
	{
		___disp_0 = value;
		Il2CppCodeGenWriteBarrier(&___disp_0, value);
	}

	inline static int32_t get_offset_of_reaction_1() { return static_cast<int32_t>(offsetof(U3CMapU3Ec__AnonStorey10F_2_t985129795, ___reaction_1)); }
	inline Action_1_t359458046 * get_reaction_1() const { return ___reaction_1; }
	inline Action_1_t359458046 ** get_address_of_reaction_1() { return &___reaction_1; }
	inline void set_reaction_1(Action_1_t359458046 * value)
	{
		___reaction_1 = value;
		Il2CppCodeGenWriteBarrier(&___reaction_1, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__refU24270_2() { return static_cast<int32_t>(offsetof(U3CMapU3Ec__AnonStorey10F_2_t985129795, ___U3CU3Ef__refU24270_2)); }
	inline U3CMapU3Ec__AnonStorey10E_2_t4182566112 * get_U3CU3Ef__refU24270_2() const { return ___U3CU3Ef__refU24270_2; }
	inline U3CMapU3Ec__AnonStorey10E_2_t4182566112 ** get_address_of_U3CU3Ef__refU24270_2() { return &___U3CU3Ef__refU24270_2; }
	inline void set_U3CU3Ef__refU24270_2(U3CMapU3Ec__AnonStorey10E_2_t4182566112 * value)
	{
		___U3CU3Ef__refU24270_2 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__refU24270_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
