﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.FirstObservable`1/First_<UniRx.Tuple`2<System.Object,System.Object>>
struct First__t3591495031;
// UniRx.Operators.FirstObservable`1<UniRx.Tuple`2<System.Object,System.Object>>
struct FirstObservable_1_t2391816233;
// UniRx.IObserver`1<UniRx.Tuple`2<System.Object,System.Object>>
struct IObserver_1_t2581260722;
// System.IDisposable
struct IDisposable_t1628921374;
// System.Exception
struct Exception_t1967233988;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Tuple_2_gen369261819.h"
#include "mscorlib_System_Exception1967233988.h"

// System.Void UniRx.Operators.FirstObservable`1/First_<UniRx.Tuple`2<System.Object,System.Object>>::.ctor(UniRx.Operators.FirstObservable`1<T>,UniRx.IObserver`1<T>,System.IDisposable)
extern "C"  void First___ctor_m1997630991_gshared (First__t3591495031 * __this, FirstObservable_1_t2391816233 * ___parent0, Il2CppObject* ___observer1, Il2CppObject * ___cancel2, const MethodInfo* method);
#define First___ctor_m1997630991(__this, ___parent0, ___observer1, ___cancel2, method) ((  void (*) (First__t3591495031 *, FirstObservable_1_t2391816233 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))First___ctor_m1997630991_gshared)(__this, ___parent0, ___observer1, ___cancel2, method)
// System.Void UniRx.Operators.FirstObservable`1/First_<UniRx.Tuple`2<System.Object,System.Object>>::OnNext(T)
extern "C"  void First__OnNext_m215532012_gshared (First__t3591495031 * __this, Tuple_2_t369261819  ___value0, const MethodInfo* method);
#define First__OnNext_m215532012(__this, ___value0, method) ((  void (*) (First__t3591495031 *, Tuple_2_t369261819 , const MethodInfo*))First__OnNext_m215532012_gshared)(__this, ___value0, method)
// System.Void UniRx.Operators.FirstObservable`1/First_<UniRx.Tuple`2<System.Object,System.Object>>::OnError(System.Exception)
extern "C"  void First__OnError_m3037955323_gshared (First__t3591495031 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method);
#define First__OnError_m3037955323(__this, ___error0, method) ((  void (*) (First__t3591495031 *, Exception_t1967233988 *, const MethodInfo*))First__OnError_m3037955323_gshared)(__this, ___error0, method)
// System.Void UniRx.Operators.FirstObservable`1/First_<UniRx.Tuple`2<System.Object,System.Object>>::OnCompleted()
extern "C"  void First__OnCompleted_m3801979086_gshared (First__t3591495031 * __this, const MethodInfo* method);
#define First__OnCompleted_m3801979086(__this, method) ((  void (*) (First__t3591495031 *, const MethodInfo*))First__OnCompleted_m3801979086_gshared)(__this, method)
