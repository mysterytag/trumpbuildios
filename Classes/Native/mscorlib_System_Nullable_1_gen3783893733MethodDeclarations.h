﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Object
struct Il2CppObject;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Nullable_1_gen3783893733.h"
#include "AssemblyU2DCSharp_SponsorPay_SPUserEthnicity897855825.h"
#include "mscorlib_System_Object837106420.h"

// System.Void System.Nullable`1<SponsorPay.SPUserEthnicity>::.ctor(T)
extern "C"  void Nullable_1__ctor_m3737378726_gshared (Nullable_1_t3783893733 * __this, int32_t ___value0, const MethodInfo* method);
#define Nullable_1__ctor_m3737378726(__this, ___value0, method) ((  void (*) (Nullable_1_t3783893733 *, int32_t, const MethodInfo*))Nullable_1__ctor_m3737378726_gshared)(__this, ___value0, method)
// System.Boolean System.Nullable`1<SponsorPay.SPUserEthnicity>::get_HasValue()
extern "C"  bool Nullable_1_get_HasValue_m2087215242_gshared (Nullable_1_t3783893733 * __this, const MethodInfo* method);
#define Nullable_1_get_HasValue_m2087215242(__this, method) ((  bool (*) (Nullable_1_t3783893733 *, const MethodInfo*))Nullable_1_get_HasValue_m2087215242_gshared)(__this, method)
// T System.Nullable`1<SponsorPay.SPUserEthnicity>::get_Value()
extern "C"  int32_t Nullable_1_get_Value_m3786124957_gshared (Nullable_1_t3783893733 * __this, const MethodInfo* method);
#define Nullable_1_get_Value_m3786124957(__this, method) ((  int32_t (*) (Nullable_1_t3783893733 *, const MethodInfo*))Nullable_1_get_Value_m3786124957_gshared)(__this, method)
// System.Boolean System.Nullable`1<SponsorPay.SPUserEthnicity>::Equals(System.Object)
extern "C"  bool Nullable_1_Equals_m1804096171_gshared (Nullable_1_t3783893733 * __this, Il2CppObject * ___other0, const MethodInfo* method);
#define Nullable_1_Equals_m1804096171(__this, ___other0, method) ((  bool (*) (Nullable_1_t3783893733 *, Il2CppObject *, const MethodInfo*))Nullable_1_Equals_m1804096171_gshared)(__this, ___other0, method)
// System.Boolean System.Nullable`1<SponsorPay.SPUserEthnicity>::Equals(System.Nullable`1<T>)
extern "C"  bool Nullable_1_Equals_m3804363316_gshared (Nullable_1_t3783893733 * __this, Nullable_1_t3783893733  ___other0, const MethodInfo* method);
#define Nullable_1_Equals_m3804363316(__this, ___other0, method) ((  bool (*) (Nullable_1_t3783893733 *, Nullable_1_t3783893733 , const MethodInfo*))Nullable_1_Equals_m3804363316_gshared)(__this, ___other0, method)
// System.Int32 System.Nullable`1<SponsorPay.SPUserEthnicity>::GetHashCode()
extern "C"  int32_t Nullable_1_GetHashCode_m1703764355_gshared (Nullable_1_t3783893733 * __this, const MethodInfo* method);
#define Nullable_1_GetHashCode_m1703764355(__this, method) ((  int32_t (*) (Nullable_1_t3783893733 *, const MethodInfo*))Nullable_1_GetHashCode_m1703764355_gshared)(__this, method)
// T System.Nullable`1<SponsorPay.SPUserEthnicity>::GetValueOrDefault()
extern "C"  int32_t Nullable_1_GetValueOrDefault_m2108021240_gshared (Nullable_1_t3783893733 * __this, const MethodInfo* method);
#define Nullable_1_GetValueOrDefault_m2108021240(__this, method) ((  int32_t (*) (Nullable_1_t3783893733 *, const MethodInfo*))Nullable_1_GetValueOrDefault_m2108021240_gshared)(__this, method)
// System.String System.Nullable`1<SponsorPay.SPUserEthnicity>::ToString()
extern "C"  String_t* Nullable_1_ToString_m3672279285_gshared (Nullable_1_t3783893733 * __this, const MethodInfo* method);
#define Nullable_1_ToString_m3672279285(__this, method) ((  String_t* (*) (Nullable_1_t3783893733 *, const MethodInfo*))Nullable_1_ToString_m3672279285_gshared)(__this, method)
// System.Nullable`1<T> System.Nullable`1<SponsorPay.SPUserEthnicity>::op_Implicit(T)
extern "C"  Nullable_1_t3783893733  Nullable_1_op_Implicit_m3264975663_gshared (Il2CppObject * __this /* static, unused */, int32_t ___value0, const MethodInfo* method);
#define Nullable_1_op_Implicit_m3264975663(__this /* static, unused */, ___value0, method) ((  Nullable_1_t3783893733  (*) (Il2CppObject * /* static, unused */, int32_t, const MethodInfo*))Nullable_1_op_Implicit_m3264975663_gshared)(__this /* static, unused */, ___value0, method)
