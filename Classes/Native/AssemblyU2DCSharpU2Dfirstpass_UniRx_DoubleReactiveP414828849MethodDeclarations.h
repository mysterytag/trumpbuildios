﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.DoubleReactiveProperty
struct DoubleReactiveProperty_t414828849;

#include "codegen/il2cpp-codegen.h"

// System.Void UniRx.DoubleReactiveProperty::.ctor()
extern "C"  void DoubleReactiveProperty__ctor_m3598373240 (DoubleReactiveProperty_t414828849 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.DoubleReactiveProperty::.ctor(System.Double)
extern "C"  void DoubleReactiveProperty__ctor_m2782547146 (DoubleReactiveProperty_t414828849 * __this, double ___initialValue0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
