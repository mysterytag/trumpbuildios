﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SubstanceBase`2/Intrusion<System.Single,System.Object>
struct Intrusion_t4252761119;

#include "codegen/il2cpp-codegen.h"

// System.Void SubstanceBase`2/Intrusion<System.Single,System.Object>::.ctor()
extern "C"  void Intrusion__ctor_m1448835205_gshared (Intrusion_t4252761119 * __this, const MethodInfo* method);
#define Intrusion__ctor_m1448835205(__this, method) ((  void (*) (Intrusion_t4252761119 *, const MethodInfo*))Intrusion__ctor_m1448835205_gshared)(__this, method)
