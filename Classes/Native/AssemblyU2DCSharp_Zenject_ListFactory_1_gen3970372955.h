﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Zenject.DiContainer
struct DiContainer_t2383114449;
// System.Collections.Generic.List`1<System.Type>
struct List_1_t3576188904;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.ListFactory`1<System.Object>
struct  ListFactory_1_t3970372955  : public Il2CppObject
{
public:
	// Zenject.DiContainer Zenject.ListFactory`1::_container
	DiContainer_t2383114449 * ____container_0;
	// System.Collections.Generic.List`1<System.Type> Zenject.ListFactory`1::_implTypes
	List_1_t3576188904 * ____implTypes_1;

public:
	inline static int32_t get_offset_of__container_0() { return static_cast<int32_t>(offsetof(ListFactory_1_t3970372955, ____container_0)); }
	inline DiContainer_t2383114449 * get__container_0() const { return ____container_0; }
	inline DiContainer_t2383114449 ** get_address_of__container_0() { return &____container_0; }
	inline void set__container_0(DiContainer_t2383114449 * value)
	{
		____container_0 = value;
		Il2CppCodeGenWriteBarrier(&____container_0, value);
	}

	inline static int32_t get_offset_of__implTypes_1() { return static_cast<int32_t>(offsetof(ListFactory_1_t3970372955, ____implTypes_1)); }
	inline List_1_t3576188904 * get__implTypes_1() const { return ____implTypes_1; }
	inline List_1_t3576188904 ** get_address_of__implTypes_1() { return &____implTypes_1; }
	inline void set__implTypes_1(List_1_t3576188904 * value)
	{
		____implTypes_1 = value;
		Il2CppCodeGenWriteBarrier(&____implTypes_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
