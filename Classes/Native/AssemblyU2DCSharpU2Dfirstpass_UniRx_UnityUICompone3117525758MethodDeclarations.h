﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.UnityUIComponentExtensions/<OnValueChangedAsObservable>c__AnonStoreyAC
struct U3COnValueChangedAsObservableU3Ec__AnonStoreyAC_t3117525758;
// System.IDisposable
struct IDisposable_t1628921374;
// UniRx.IObserver`1<System.String>
struct IObserver_1_t3180487805;

#include "codegen/il2cpp-codegen.h"

// System.Void UniRx.UnityUIComponentExtensions/<OnValueChangedAsObservable>c__AnonStoreyAC::.ctor()
extern "C"  void U3COnValueChangedAsObservableU3Ec__AnonStoreyAC__ctor_m23670144 (U3COnValueChangedAsObservableU3Ec__AnonStoreyAC_t3117525758 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IDisposable UniRx.UnityUIComponentExtensions/<OnValueChangedAsObservable>c__AnonStoreyAC::<>m__EA(UniRx.IObserver`1<System.String>)
extern "C"  Il2CppObject * U3COnValueChangedAsObservableU3Ec__AnonStoreyAC_U3CU3Em__EA_m3352563800 (U3COnValueChangedAsObservableU3Ec__AnonStoreyAC_t3117525758 * __this, Il2CppObject* ___observer0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
