﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.InternalUtil.DisposedObserver`1<System.Int32>
struct DisposedObserver_1_t2021872249;
// System.Exception
struct Exception_t1967233988;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Exception1967233988.h"

// System.Void UniRx.InternalUtil.DisposedObserver`1<System.Int32>::.ctor()
extern "C"  void DisposedObserver_1__ctor_m318789761_gshared (DisposedObserver_1_t2021872249 * __this, const MethodInfo* method);
#define DisposedObserver_1__ctor_m318789761(__this, method) ((  void (*) (DisposedObserver_1_t2021872249 *, const MethodInfo*))DisposedObserver_1__ctor_m318789761_gshared)(__this, method)
// System.Void UniRx.InternalUtil.DisposedObserver`1<System.Int32>::.cctor()
extern "C"  void DisposedObserver_1__cctor_m810451788_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define DisposedObserver_1__cctor_m810451788(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))DisposedObserver_1__cctor_m810451788_gshared)(__this /* static, unused */, method)
// System.Void UniRx.InternalUtil.DisposedObserver`1<System.Int32>::OnCompleted()
extern "C"  void DisposedObserver_1_OnCompleted_m273508171_gshared (DisposedObserver_1_t2021872249 * __this, const MethodInfo* method);
#define DisposedObserver_1_OnCompleted_m273508171(__this, method) ((  void (*) (DisposedObserver_1_t2021872249 *, const MethodInfo*))DisposedObserver_1_OnCompleted_m273508171_gshared)(__this, method)
// System.Void UniRx.InternalUtil.DisposedObserver`1<System.Int32>::OnError(System.Exception)
extern "C"  void DisposedObserver_1_OnError_m3111682040_gshared (DisposedObserver_1_t2021872249 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method);
#define DisposedObserver_1_OnError_m3111682040(__this, ___error0, method) ((  void (*) (DisposedObserver_1_t2021872249 *, Exception_t1967233988 *, const MethodInfo*))DisposedObserver_1_OnError_m3111682040_gshared)(__this, ___error0, method)
// System.Void UniRx.InternalUtil.DisposedObserver`1<System.Int32>::OnNext(T)
extern "C"  void DisposedObserver_1_OnNext_m4244985577_gshared (DisposedObserver_1_t2021872249 * __this, int32_t ___value0, const MethodInfo* method);
#define DisposedObserver_1_OnNext_m4244985577(__this, ___value0, method) ((  void (*) (DisposedObserver_1_t2021872249 *, int32_t, const MethodInfo*))DisposedObserver_1_OnNext_m4244985577_gshared)(__this, ___value0, method)
