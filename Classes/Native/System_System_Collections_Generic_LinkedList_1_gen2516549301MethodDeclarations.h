﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "System_System_Collections_Generic_LinkedList_1_gen2577235966MethodDeclarations.h"

// System.Void System.Collections.Generic.LinkedList`1<Tacticsoft.TableViewCell>::.ctor()
#define LinkedList_1__ctor_m813556635(__this, method) ((  void (*) (LinkedList_1_t2516549301 *, const MethodInfo*))LinkedList_1__ctor_m2955457271_gshared)(__this, method)
// System.Void System.Collections.Generic.LinkedList`1<Tacticsoft.TableViewCell>::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
#define LinkedList_1__ctor_m1337895419(__this, ___info0, ___context1, method) ((  void (*) (LinkedList_1_t2516549301 *, SerializationInfo_t2995724695 *, StreamingContext_t986364934 , const MethodInfo*))LinkedList_1__ctor_m3369579448_gshared)(__this, ___info0, ___context1, method)
// System.Void System.Collections.Generic.LinkedList`1<Tacticsoft.TableViewCell>::System.Collections.Generic.ICollection<T>.Add(T)
#define LinkedList_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m2098138117(__this, ___value0, method) ((  void (*) (LinkedList_1_t2516549301 *, TableViewCell_t776419755 *, const MethodInfo*))LinkedList_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m3576108392_gshared)(__this, ___value0, method)
// System.Void System.Collections.Generic.LinkedList`1<Tacticsoft.TableViewCell>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define LinkedList_1_System_Collections_ICollection_CopyTo_m946017994(__this, ___array0, ___index1, method) ((  void (*) (LinkedList_1_t2516549301 *, Il2CppArray *, int32_t, const MethodInfo*))LinkedList_1_System_Collections_ICollection_CopyTo_m2331638317_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.LinkedList`1<Tacticsoft.TableViewCell>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
#define LinkedList_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m3598981188(__this, method) ((  Il2CppObject* (*) (LinkedList_1_t2516549301 *, const MethodInfo*))LinkedList_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m2027502985_gshared)(__this, method)
// System.Collections.IEnumerator System.Collections.Generic.LinkedList`1<Tacticsoft.TableViewCell>::System.Collections.IEnumerable.GetEnumerator()
#define LinkedList_1_System_Collections_IEnumerable_GetEnumerator_m2486045893(__this, method) ((  Il2CppObject * (*) (LinkedList_1_t2516549301 *, const MethodInfo*))LinkedList_1_System_Collections_IEnumerable_GetEnumerator_m51916412_gshared)(__this, method)
// System.Boolean System.Collections.Generic.LinkedList`1<Tacticsoft.TableViewCell>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
#define LinkedList_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m922396073(__this, method) ((  bool (*) (LinkedList_1_t2516549301 *, const MethodInfo*))LinkedList_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m2230847288_gshared)(__this, method)
// System.Boolean System.Collections.Generic.LinkedList`1<Tacticsoft.TableViewCell>::System.Collections.ICollection.get_IsSynchronized()
#define LinkedList_1_System_Collections_ICollection_get_IsSynchronized_m696004276(__this, method) ((  bool (*) (LinkedList_1_t2516549301 *, const MethodInfo*))LinkedList_1_System_Collections_ICollection_get_IsSynchronized_m683991045_gshared)(__this, method)
// System.Object System.Collections.Generic.LinkedList`1<Tacticsoft.TableViewCell>::System.Collections.ICollection.get_SyncRoot()
#define LinkedList_1_System_Collections_ICollection_get_SyncRoot_m123598802(__this, method) ((  Il2CppObject * (*) (LinkedList_1_t2516549301 *, const MethodInfo*))LinkedList_1_System_Collections_ICollection_get_SyncRoot_m573420165_gshared)(__this, method)
// System.Void System.Collections.Generic.LinkedList`1<Tacticsoft.TableViewCell>::VerifyReferencedNode(System.Collections.Generic.LinkedListNode`1<T>)
#define LinkedList_1_VerifyReferencedNode_m1324595377(__this, ___node0, method) ((  void (*) (LinkedList_1_t2516549301 *, LinkedListNode_1_t478410375 *, const MethodInfo*))LinkedList_1_VerifyReferencedNode_m3939775124_gshared)(__this, ___node0, method)
// System.Collections.Generic.LinkedListNode`1<T> System.Collections.Generic.LinkedList`1<Tacticsoft.TableViewCell>::AddBefore(System.Collections.Generic.LinkedListNode`1<T>,T)
#define LinkedList_1_AddBefore_m351006517(__this, ___node0, ___value1, method) ((  LinkedListNode_1_t478410375 * (*) (LinkedList_1_t2516549301 *, LinkedListNode_1_t478410375 *, TableViewCell_t776419755 *, const MethodInfo*))LinkedList_1_AddBefore_m1086189582_gshared)(__this, ___node0, ___value1, method)
// System.Collections.Generic.LinkedListNode`1<T> System.Collections.Generic.LinkedList`1<Tacticsoft.TableViewCell>::AddLast(T)
#define LinkedList_1_AddLast_m3701025480(__this, ___value0, method) ((  LinkedListNode_1_t478410375 * (*) (LinkedList_1_t2516549301 *, TableViewCell_t776419755 *, const MethodInfo*))LinkedList_1_AddLast_m4070107716_gshared)(__this, ___value0, method)
// System.Void System.Collections.Generic.LinkedList`1<Tacticsoft.TableViewCell>::Clear()
#define LinkedList_1_Clear_m153939941(__this, method) ((  void (*) (LinkedList_1_t2516549301 *, const MethodInfo*))LinkedList_1_Clear_m361590562_gshared)(__this, method)
// System.Boolean System.Collections.Generic.LinkedList`1<Tacticsoft.TableViewCell>::Contains(T)
#define LinkedList_1_Contains_m902220907(__this, ___value0, method) ((  bool (*) (LinkedList_1_t2516549301 *, TableViewCell_t776419755 *, const MethodInfo*))LinkedList_1_Contains_m3484410556_gshared)(__this, ___value0, method)
// System.Void System.Collections.Generic.LinkedList`1<Tacticsoft.TableViewCell>::CopyTo(T[],System.Int32)
#define LinkedList_1_CopyTo_m1497751093(__this, ___array0, ___index1, method) ((  void (*) (LinkedList_1_t2516549301 *, TableViewCellU5BU5D_t2926218058*, int32_t, const MethodInfo*))LinkedList_1_CopyTo_m3470139544_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.LinkedListNode`1<T> System.Collections.Generic.LinkedList`1<Tacticsoft.TableViewCell>::Find(T)
#define LinkedList_1_Find_m2204300685(__this, ___value0, method) ((  LinkedListNode_1_t478410375 * (*) (LinkedList_1_t2516549301 *, TableViewCell_t776419755 *, const MethodInfo*))LinkedList_1_Find_m2643247334_gshared)(__this, ___value0, method)
// System.Collections.Generic.LinkedList`1/Enumerator<T> System.Collections.Generic.LinkedList`1<Tacticsoft.TableViewCell>::GetEnumerator()
#define LinkedList_1_GetEnumerator_m538639355(__this, method) ((  Enumerator_t3954129012  (*) (LinkedList_1_t2516549301 *, const MethodInfo*))LinkedList_1_GetEnumerator_m3713737734_gshared)(__this, method)
// System.Void System.Collections.Generic.LinkedList`1<Tacticsoft.TableViewCell>::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
#define LinkedList_1_GetObjectData_m3193947736(__this, ___info0, ___context1, method) ((  void (*) (LinkedList_1_t2516549301 *, SerializationInfo_t2995724695 *, StreamingContext_t986364934 , const MethodInfo*))LinkedList_1_GetObjectData_m3974480661_gshared)(__this, ___info0, ___context1, method)
// System.Void System.Collections.Generic.LinkedList`1<Tacticsoft.TableViewCell>::OnDeserialization(System.Object)
#define LinkedList_1_OnDeserialization_m3971052300(__this, ___sender0, method) ((  void (*) (LinkedList_1_t2516549301 *, Il2CppObject *, const MethodInfo*))LinkedList_1_OnDeserialization_m3445006959_gshared)(__this, ___sender0, method)
// System.Boolean System.Collections.Generic.LinkedList`1<Tacticsoft.TableViewCell>::Remove(T)
#define LinkedList_1_Remove_m277457702(__this, ___value0, method) ((  bool (*) (LinkedList_1_t2516549301 *, TableViewCell_t776419755 *, const MethodInfo*))LinkedList_1_Remove_m3283493303_gshared)(__this, ___value0, method)
// System.Void System.Collections.Generic.LinkedList`1<Tacticsoft.TableViewCell>::Remove(System.Collections.Generic.LinkedListNode`1<T>)
#define LinkedList_1_Remove_m2101858305(__this, ___node0, method) ((  void (*) (LinkedList_1_t2516549301 *, LinkedListNode_1_t478410375 *, const MethodInfo*))LinkedList_1_Remove_m4034790180_gshared)(__this, ___node0, method)
// System.Void System.Collections.Generic.LinkedList`1<Tacticsoft.TableViewCell>::RemoveFirst()
#define LinkedList_1_RemoveFirst_m2645837893(__this, method) ((  void (*) (LinkedList_1_t2516549301 *, const MethodInfo*))LinkedList_1_RemoveFirst_m1652892321_gshared)(__this, method)
// System.Void System.Collections.Generic.LinkedList`1<Tacticsoft.TableViewCell>::RemoveLast()
#define LinkedList_1_RemoveLast_m3392075332(__this, method) ((  void (*) (LinkedList_1_t2516549301 *, const MethodInfo*))LinkedList_1_RemoveLast_m2573038887_gshared)(__this, method)
// System.Int32 System.Collections.Generic.LinkedList`1<Tacticsoft.TableViewCell>::get_Count()
#define LinkedList_1_get_Count_m648565486(__this, method) ((  int32_t (*) (LinkedList_1_t2516549301 *, const MethodInfo*))LinkedList_1_get_Count_m1368924491_gshared)(__this, method)
// System.Collections.Generic.LinkedListNode`1<T> System.Collections.Generic.LinkedList`1<Tacticsoft.TableViewCell>::get_First()
#define LinkedList_1_get_First_m1065407111(__this, method) ((  LinkedListNode_1_t478410375 * (*) (LinkedList_1_t2516549301 *, const MethodInfo*))LinkedList_1_get_First_m3278587786_gshared)(__this, method)
// System.Collections.Generic.LinkedListNode`1<T> System.Collections.Generic.LinkedList`1<Tacticsoft.TableViewCell>::get_Last()
#define LinkedList_1_get_Last_m1785971991(__this, method) ((  LinkedListNode_1_t478410375 * (*) (LinkedList_1_t2516549301 *, const MethodInfo*))LinkedList_1_get_Last_m270176030_gshared)(__this, method)
