﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.FirstObservable`1<UniRx.Tuple`2<System.Object,System.Object>>
struct FirstObservable_1_t2391816233;
// UniRx.IObservable`1<UniRx.Tuple`2<System.Object,System.Object>>
struct IObservable_1_t128060183;
// System.Func`2<UniRx.Tuple`2<System.Object,System.Object>,System.Boolean>
struct Func_2_t1101125214;
// System.IDisposable
struct IDisposable_t1628921374;
// UniRx.IObserver`1<UniRx.Tuple`2<System.Object,System.Object>>
struct IObserver_1_t2581260722;

#include "codegen/il2cpp-codegen.h"

// System.Void UniRx.Operators.FirstObservable`1<UniRx.Tuple`2<System.Object,System.Object>>::.ctor(UniRx.IObservable`1<T>,System.Boolean)
extern "C"  void FirstObservable_1__ctor_m890207374_gshared (FirstObservable_1_t2391816233 * __this, Il2CppObject* ___source0, bool ___useDefault1, const MethodInfo* method);
#define FirstObservable_1__ctor_m890207374(__this, ___source0, ___useDefault1, method) ((  void (*) (FirstObservable_1_t2391816233 *, Il2CppObject*, bool, const MethodInfo*))FirstObservable_1__ctor_m890207374_gshared)(__this, ___source0, ___useDefault1, method)
// System.Void UniRx.Operators.FirstObservable`1<UniRx.Tuple`2<System.Object,System.Object>>::.ctor(UniRx.IObservable`1<T>,System.Func`2<T,System.Boolean>,System.Boolean)
extern "C"  void FirstObservable_1__ctor_m746669496_gshared (FirstObservable_1_t2391816233 * __this, Il2CppObject* ___source0, Func_2_t1101125214 * ___predicate1, bool ___useDefault2, const MethodInfo* method);
#define FirstObservable_1__ctor_m746669496(__this, ___source0, ___predicate1, ___useDefault2, method) ((  void (*) (FirstObservable_1_t2391816233 *, Il2CppObject*, Func_2_t1101125214 *, bool, const MethodInfo*))FirstObservable_1__ctor_m746669496_gshared)(__this, ___source0, ___predicate1, ___useDefault2, method)
// System.IDisposable UniRx.Operators.FirstObservable`1<UniRx.Tuple`2<System.Object,System.Object>>::SubscribeCore(UniRx.IObserver`1<T>,System.IDisposable)
extern "C"  Il2CppObject * FirstObservable_1_SubscribeCore_m1264115032_gshared (FirstObservable_1_t2391816233 * __this, Il2CppObject* ___observer0, Il2CppObject * ___cancel1, const MethodInfo* method);
#define FirstObservable_1_SubscribeCore_m1264115032(__this, ___observer0, ___cancel1, method) ((  Il2CppObject * (*) (FirstObservable_1_t2391816233 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))FirstObservable_1_SubscribeCore_m1264115032_gshared)(__this, ___observer0, ___cancel1, method)
