﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// TableButtons/<Initialize>c__AnonStorey157
struct U3CInitializeU3Ec__AnonStorey157_t1973587070;
// UpgradeButton
struct UpgradeButton_t1475467342;
// UpgradeCell
struct UpgradeCell_t4117746046;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_UpgradeButton1475467342.h"
#include "AssemblyU2DCSharp_UpgradeCell4117746046.h"

// System.Void TableButtons/<Initialize>c__AnonStorey157::.ctor()
extern "C"  void U3CInitializeU3Ec__AnonStorey157__ctor_m2768991633 (U3CInitializeU3Ec__AnonStorey157_t1973587070 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TableButtons/<Initialize>c__AnonStorey157::<>m__257(UpgradeButton,UpgradeCell,System.Int32)
extern "C"  void U3CInitializeU3Ec__AnonStorey157_U3CU3Em__257_m425290849 (U3CInitializeU3Ec__AnonStorey157_t1973587070 * __this, UpgradeButton_t1475467342 * ___upgradeButton0, UpgradeCell_t4117746046 * ___upgradeCell1, int32_t ___arg32, const MethodInfo* method) IL2CPP_METHOD_ATTR;
