﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "System_Core_System_Func_2_gen1509682273MethodDeclarations.h"

// System.Void System.Func`2<IProcess,System.Boolean>::.ctor(System.Object,System.IntPtr)
#define Func_2__ctor_m3957380477(__this, ___object0, ___method1, method) ((  void (*) (Func_2_t3494101351 *, Il2CppObject *, IntPtr_t, const MethodInfo*))Func_2__ctor_m563515303_gshared)(__this, ___object0, ___method1, method)
// TResult System.Func`2<IProcess,System.Boolean>::Invoke(T)
#define Func_2_Invoke_m2023973813(__this, ___arg10, method) ((  bool (*) (Func_2_t3494101351 *, Il2CppObject *, const MethodInfo*))Func_2_Invoke_m1882130143_gshared)(__this, ___arg10, method)
// System.IAsyncResult System.Func`2<IProcess,System.Boolean>::BeginInvoke(T,System.AsyncCallback,System.Object)
#define Func_2_BeginInvoke_m1133445604(__this, ___arg10, ___callback1, ___object2, method) ((  Il2CppObject * (*) (Func_2_t3494101351 *, Il2CppObject *, AsyncCallback_t1363551830 *, Il2CppObject *, const MethodInfo*))Func_2_BeginInvoke_m1852288274_gshared)(__this, ___arg10, ___callback1, ___object2, method)
// TResult System.Func`2<IProcess,System.Boolean>::EndInvoke(System.IAsyncResult)
#define Func_2_EndInvoke_m1841935167(__this, ___result0, method) ((  bool (*) (Func_2_t3494101351 *, Il2CppObject *, const MethodInfo*))Func_2_EndInvoke_m1659014741_gshared)(__this, ___result0, method)
