﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera2195698409MethodDeclarations.h"

// System.Void System.Collections.Generic.List`1/Enumerator<System.Collections.Generic.KeyValuePair`2<Zenject.BindingId,System.Collections.Generic.List`1<Zenject.ProviderBase>>>::.ctor(System.Collections.Generic.List`1<T>)
#define Enumerator__ctor_m2504037772(__this, ___l0, method) ((  void (*) (Enumerator_t974100832 *, List_1_t2888317840 *, const MethodInfo*))Enumerator__ctor_m2249551728_gshared)(__this, ___l0, method)
// System.Void System.Collections.Generic.List`1/Enumerator<System.Collections.Generic.KeyValuePair`2<Zenject.BindingId,System.Collections.Generic.List`1<Zenject.ProviderBase>>>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m1672623942(__this, method) ((  void (*) (Enumerator_t974100832 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m1882046178_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1/Enumerator<System.Collections.Generic.KeyValuePair`2<Zenject.BindingId,System.Collections.Generic.List`1<Zenject.ProviderBase>>>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m2695788348(__this, method) ((  Il2CppObject * (*) (Enumerator_t974100832 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m602451608_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<System.Collections.Generic.KeyValuePair`2<Zenject.BindingId,System.Collections.Generic.List`1<Zenject.ProviderBase>>>::Dispose()
#define Enumerator_Dispose_m786581105(__this, method) ((  void (*) (Enumerator_t974100832 *, const MethodInfo*))Enumerator_Dispose_m3838892373_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<System.Collections.Generic.KeyValuePair`2<Zenject.BindingId,System.Collections.Generic.List`1<Zenject.ProviderBase>>>::VerifyState()
#define Enumerator_VerifyState_m935028010(__this, method) ((  void (*) (Enumerator_t974100832 *, const MethodInfo*))Enumerator_VerifyState_m1553851918_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<System.Collections.Generic.KeyValuePair`2<Zenject.BindingId,System.Collections.Generic.List`1<Zenject.ProviderBase>>>::MoveNext()
#define Enumerator_MoveNext_m4115796319(__this, method) ((  bool (*) (Enumerator_t974100832 *, const MethodInfo*))Enumerator_MoveNext_m1524607506_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<System.Collections.Generic.KeyValuePair`2<Zenject.BindingId,System.Collections.Generic.List`1<Zenject.ProviderBase>>>::get_Current()
#define Enumerator_get_Current_m1260612893(__this, method) ((  KeyValuePair_2_t2091358871  (*) (Enumerator_t974100832 *, const MethodInfo*))Enumerator_get_Current_m3227842279_gshared)(__this, method)
