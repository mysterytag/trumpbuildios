﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CellReactiveApi/<NotNull>c__AnonStorey102`1<System.Object>
struct U3CNotNullU3Ec__AnonStorey102_1_t291767342;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"

// System.Void CellReactiveApi/<NotNull>c__AnonStorey102`1<System.Object>::.ctor()
extern "C"  void U3CNotNullU3Ec__AnonStorey102_1__ctor_m2607898092_gshared (U3CNotNullU3Ec__AnonStorey102_1_t291767342 * __this, const MethodInfo* method);
#define U3CNotNullU3Ec__AnonStorey102_1__ctor_m2607898092(__this, method) ((  void (*) (U3CNotNullU3Ec__AnonStorey102_1_t291767342 *, const MethodInfo*))U3CNotNullU3Ec__AnonStorey102_1__ctor_m2607898092_gshared)(__this, method)
// T CellReactiveApi/<NotNull>c__AnonStorey102`1<System.Object>::<>m__178(T)
extern "C"  Il2CppObject * U3CNotNullU3Ec__AnonStorey102_1_U3CU3Em__178_m1839195072_gshared (U3CNotNullU3Ec__AnonStorey102_1_t291767342 * __this, Il2CppObject * ___val0, const MethodInfo* method);
#define U3CNotNullU3Ec__AnonStorey102_1_U3CU3Em__178_m1839195072(__this, ___val0, method) ((  Il2CppObject * (*) (U3CNotNullU3Ec__AnonStorey102_1_t291767342 *, Il2CppObject *, const MethodInfo*))U3CNotNullU3Ec__AnonStorey102_1_U3CU3Em__178_m1839195072_gshared)(__this, ___val0, method)
