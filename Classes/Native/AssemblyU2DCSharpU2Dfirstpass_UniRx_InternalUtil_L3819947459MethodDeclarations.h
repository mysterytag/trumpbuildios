﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.InternalUtil.ListObserver`1<UnityEngine.Vector2>
struct ListObserver_1_t3819947459;
// UniRx.InternalUtil.ImmutableList`1<UniRx.IObserver`1<UnityEngine.Vector2>>
struct ImmutableList_1_t2502565894;
// System.Exception
struct Exception_t1967233988;
// UniRx.IObserver`1<UnityEngine.Vector2>
struct IObserver_1_t1442361395;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Exception1967233988.h"
#include "UnityEngine_UnityEngine_Vector23525329788.h"

// System.Void UniRx.InternalUtil.ListObserver`1<UnityEngine.Vector2>::.ctor(UniRx.InternalUtil.ImmutableList`1<UniRx.IObserver`1<T>>)
extern "C"  void ListObserver_1__ctor_m2231967547_gshared (ListObserver_1_t3819947459 * __this, ImmutableList_1_t2502565894 * ___observers0, const MethodInfo* method);
#define ListObserver_1__ctor_m2231967547(__this, ___observers0, method) ((  void (*) (ListObserver_1_t3819947459 *, ImmutableList_1_t2502565894 *, const MethodInfo*))ListObserver_1__ctor_m2231967547_gshared)(__this, ___observers0, method)
// System.Void UniRx.InternalUtil.ListObserver`1<UnityEngine.Vector2>::OnCompleted()
extern "C"  void ListObserver_1_OnCompleted_m1131372083_gshared (ListObserver_1_t3819947459 * __this, const MethodInfo* method);
#define ListObserver_1_OnCompleted_m1131372083(__this, method) ((  void (*) (ListObserver_1_t3819947459 *, const MethodInfo*))ListObserver_1_OnCompleted_m1131372083_gshared)(__this, method)
// System.Void UniRx.InternalUtil.ListObserver`1<UnityEngine.Vector2>::OnError(System.Exception)
extern "C"  void ListObserver_1_OnError_m1377262304_gshared (ListObserver_1_t3819947459 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method);
#define ListObserver_1_OnError_m1377262304(__this, ___error0, method) ((  void (*) (ListObserver_1_t3819947459 *, Exception_t1967233988 *, const MethodInfo*))ListObserver_1_OnError_m1377262304_gshared)(__this, ___error0, method)
// System.Void UniRx.InternalUtil.ListObserver`1<UnityEngine.Vector2>::OnNext(T)
extern "C"  void ListObserver_1_OnNext_m1346830801_gshared (ListObserver_1_t3819947459 * __this, Vector2_t3525329788  ___value0, const MethodInfo* method);
#define ListObserver_1_OnNext_m1346830801(__this, ___value0, method) ((  void (*) (ListObserver_1_t3819947459 *, Vector2_t3525329788 , const MethodInfo*))ListObserver_1_OnNext_m1346830801_gshared)(__this, ___value0, method)
// UniRx.IObserver`1<T> UniRx.InternalUtil.ListObserver`1<UnityEngine.Vector2>::Add(UniRx.IObserver`1<T>)
extern "C"  Il2CppObject* ListObserver_1_Add_m2098521763_gshared (ListObserver_1_t3819947459 * __this, Il2CppObject* ___observer0, const MethodInfo* method);
#define ListObserver_1_Add_m2098521763(__this, ___observer0, method) ((  Il2CppObject* (*) (ListObserver_1_t3819947459 *, Il2CppObject*, const MethodInfo*))ListObserver_1_Add_m2098521763_gshared)(__this, ___observer0, method)
// UniRx.IObserver`1<T> UniRx.InternalUtil.ListObserver`1<UnityEngine.Vector2>::Remove(UniRx.IObserver`1<T>)
extern "C"  Il2CppObject* ListObserver_1_Remove_m4246293960_gshared (ListObserver_1_t3819947459 * __this, Il2CppObject* ___observer0, const MethodInfo* method);
#define ListObserver_1_Remove_m4246293960(__this, ___observer0, method) ((  Il2CppObject* (*) (ListObserver_1_t3819947459 *, Il2CppObject*, const MethodInfo*))ListObserver_1_Remove_m4246293960_gshared)(__this, ___observer0, method)
