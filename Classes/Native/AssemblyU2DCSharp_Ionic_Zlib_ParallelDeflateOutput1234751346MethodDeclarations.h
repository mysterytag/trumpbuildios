﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Ionic.Zlib.ParallelDeflateOutputStream
struct ParallelDeflateOutputStream_t1234751346;
// System.IO.Stream
struct Stream_t219029575;
// System.Byte[]
struct ByteU5BU5D_t58506160;
// System.Object
struct Il2CppObject;
// Ionic.Zlib.WorkItem
struct WorkItem_t2657194833;
// System.String
struct String_t;
// System.Object[]
struct ObjectU5BU5D_t11523773;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_IO_Stream219029575.h"
#include "AssemblyU2DCSharp_Ionic_Zlib_CompressionLevel3940478795.h"
#include "AssemblyU2DCSharp_Ionic_Zlib_CompressionStrategy2328611910.h"
#include "mscorlib_System_Object837106420.h"
#include "AssemblyU2DCSharp_Ionic_Zlib_WorkItem2657194833.h"
#include "AssemblyU2DCSharp_Ionic_Zlib_ParallelDeflateOutput2727706571.h"
#include "mscorlib_System_String968488902.h"
#include "mscorlib_System_IO_SeekOrigin3506139269.h"

// System.Void Ionic.Zlib.ParallelDeflateOutputStream::.ctor(System.IO.Stream)
extern "C"  void ParallelDeflateOutputStream__ctor_m2619341060 (ParallelDeflateOutputStream_t1234751346 * __this, Stream_t219029575 * ___stream0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Ionic.Zlib.ParallelDeflateOutputStream::.ctor(System.IO.Stream,Ionic.Zlib.CompressionLevel)
extern "C"  void ParallelDeflateOutputStream__ctor_m357338043 (ParallelDeflateOutputStream_t1234751346 * __this, Stream_t219029575 * ___stream0, int32_t ___level1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Ionic.Zlib.ParallelDeflateOutputStream::.ctor(System.IO.Stream,System.Boolean)
extern "C"  void ParallelDeflateOutputStream__ctor_m878096889 (ParallelDeflateOutputStream_t1234751346 * __this, Stream_t219029575 * ___stream0, bool ___leaveOpen1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Ionic.Zlib.ParallelDeflateOutputStream::.ctor(System.IO.Stream,Ionic.Zlib.CompressionLevel,System.Boolean)
extern "C"  void ParallelDeflateOutputStream__ctor_m762935906 (ParallelDeflateOutputStream_t1234751346 * __this, Stream_t219029575 * ___stream0, int32_t ___level1, bool ___leaveOpen2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Ionic.Zlib.ParallelDeflateOutputStream::.ctor(System.IO.Stream,Ionic.Zlib.CompressionLevel,Ionic.Zlib.CompressionStrategy,System.Boolean)
extern "C"  void ParallelDeflateOutputStream__ctor_m1295130932 (ParallelDeflateOutputStream_t1234751346 * __this, Stream_t219029575 * ___stream0, int32_t ___level1, int32_t ___strategy2, bool ___leaveOpen3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Ionic.Zlib.ParallelDeflateOutputStream::.cctor()
extern "C"  void ParallelDeflateOutputStream__cctor_m2600807648 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Ionic.Zlib.CompressionStrategy Ionic.Zlib.ParallelDeflateOutputStream::get_Strategy()
extern "C"  int32_t ParallelDeflateOutputStream_get_Strategy_m2512183214 (ParallelDeflateOutputStream_t1234751346 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Ionic.Zlib.ParallelDeflateOutputStream::set_Strategy(Ionic.Zlib.CompressionStrategy)
extern "C"  void ParallelDeflateOutputStream_set_Strategy_m1036415461 (ParallelDeflateOutputStream_t1234751346 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Ionic.Zlib.ParallelDeflateOutputStream::get_MaxBufferPairs()
extern "C"  int32_t ParallelDeflateOutputStream_get_MaxBufferPairs_m958503619 (ParallelDeflateOutputStream_t1234751346 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Ionic.Zlib.ParallelDeflateOutputStream::set_MaxBufferPairs(System.Int32)
extern "C"  void ParallelDeflateOutputStream_set_MaxBufferPairs_m4224810490 (ParallelDeflateOutputStream_t1234751346 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Ionic.Zlib.ParallelDeflateOutputStream::get_BufferSize()
extern "C"  int32_t ParallelDeflateOutputStream_get_BufferSize_m3556757039 (ParallelDeflateOutputStream_t1234751346 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Ionic.Zlib.ParallelDeflateOutputStream::set_BufferSize(System.Int32)
extern "C"  void ParallelDeflateOutputStream_set_BufferSize_m1360728678 (ParallelDeflateOutputStream_t1234751346 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Ionic.Zlib.ParallelDeflateOutputStream::get_Crc32()
extern "C"  int32_t ParallelDeflateOutputStream_get_Crc32_m3808108103 (ParallelDeflateOutputStream_t1234751346 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 Ionic.Zlib.ParallelDeflateOutputStream::get_BytesProcessed()
extern "C"  int64_t ParallelDeflateOutputStream_get_BytesProcessed_m3473894898 (ParallelDeflateOutputStream_t1234751346 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Ionic.Zlib.ParallelDeflateOutputStream::_InitializePoolOfWorkItems()
extern "C"  void ParallelDeflateOutputStream__InitializePoolOfWorkItems_m3415543652 (ParallelDeflateOutputStream_t1234751346 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Ionic.Zlib.ParallelDeflateOutputStream::Write(System.Byte[],System.Int32,System.Int32)
extern "C"  void ParallelDeflateOutputStream_Write_m544154367 (ParallelDeflateOutputStream_t1234751346 * __this, ByteU5BU5D_t58506160* ___buffer0, int32_t ___offset1, int32_t ___count2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Ionic.Zlib.ParallelDeflateOutputStream::_FlushFinish()
extern "C"  void ParallelDeflateOutputStream__FlushFinish_m2646961647 (ParallelDeflateOutputStream_t1234751346 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Ionic.Zlib.ParallelDeflateOutputStream::_Flush(System.Boolean)
extern "C"  void ParallelDeflateOutputStream__Flush_m4147153587 (ParallelDeflateOutputStream_t1234751346 * __this, bool ___lastInput0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Ionic.Zlib.ParallelDeflateOutputStream::Flush()
extern "C"  void ParallelDeflateOutputStream_Flush_m1707416463 (ParallelDeflateOutputStream_t1234751346 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Ionic.Zlib.ParallelDeflateOutputStream::Close()
extern "C"  void ParallelDeflateOutputStream_Close_m3334328707 (ParallelDeflateOutputStream_t1234751346 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Ionic.Zlib.ParallelDeflateOutputStream::Dispose()
extern "C"  void ParallelDeflateOutputStream_Dispose_m979022186 (ParallelDeflateOutputStream_t1234751346 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Ionic.Zlib.ParallelDeflateOutputStream::Dispose(System.Boolean)
extern "C"  void ParallelDeflateOutputStream_Dispose_m9545057 (ParallelDeflateOutputStream_t1234751346 * __this, bool ___disposing0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Ionic.Zlib.ParallelDeflateOutputStream::Reset(System.IO.Stream)
extern "C"  void ParallelDeflateOutputStream_Reset_m2593024433 (ParallelDeflateOutputStream_t1234751346 * __this, Stream_t219029575 * ___stream0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Ionic.Zlib.ParallelDeflateOutputStream::EmitPendingBuffers(System.Boolean,System.Boolean)
extern "C"  void ParallelDeflateOutputStream_EmitPendingBuffers_m2780935328 (ParallelDeflateOutputStream_t1234751346 * __this, bool ___doAll0, bool ___mustWait1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Ionic.Zlib.ParallelDeflateOutputStream::_DeflateOne(System.Object)
extern "C"  void ParallelDeflateOutputStream__DeflateOne_m1022390799 (ParallelDeflateOutputStream_t1234751346 * __this, Il2CppObject * ___wi0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Ionic.Zlib.ParallelDeflateOutputStream::DeflateOneSegment(Ionic.Zlib.WorkItem)
extern "C"  bool ParallelDeflateOutputStream_DeflateOneSegment_m2935786888 (ParallelDeflateOutputStream_t1234751346 * __this, WorkItem_t2657194833 * ___workitem0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Ionic.Zlib.ParallelDeflateOutputStream::TraceOutput(Ionic.Zlib.ParallelDeflateOutputStream/TraceBits,System.String,System.Object[])
extern "C"  void ParallelDeflateOutputStream_TraceOutput_m1790891503 (ParallelDeflateOutputStream_t1234751346 * __this, uint32_t ___bits0, String_t* ___format1, ObjectU5BU5D_t11523773* ___varParams2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Ionic.Zlib.ParallelDeflateOutputStream::get_CanSeek()
extern "C"  bool ParallelDeflateOutputStream_get_CanSeek_m1173389974 (ParallelDeflateOutputStream_t1234751346 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Ionic.Zlib.ParallelDeflateOutputStream::get_CanRead()
extern "C"  bool ParallelDeflateOutputStream_get_CanRead_m1144634932 (ParallelDeflateOutputStream_t1234751346 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Ionic.Zlib.ParallelDeflateOutputStream::get_CanWrite()
extern "C"  bool ParallelDeflateOutputStream_get_CanWrite_m1646598051 (ParallelDeflateOutputStream_t1234751346 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 Ionic.Zlib.ParallelDeflateOutputStream::get_Length()
extern "C"  int64_t ParallelDeflateOutputStream_get_Length_m966213365 (ParallelDeflateOutputStream_t1234751346 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 Ionic.Zlib.ParallelDeflateOutputStream::get_Position()
extern "C"  int64_t ParallelDeflateOutputStream_get_Position_m1039609080 (ParallelDeflateOutputStream_t1234751346 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Ionic.Zlib.ParallelDeflateOutputStream::set_Position(System.Int64)
extern "C"  void ParallelDeflateOutputStream_set_Position_m757666159 (ParallelDeflateOutputStream_t1234751346 * __this, int64_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Ionic.Zlib.ParallelDeflateOutputStream::Read(System.Byte[],System.Int32,System.Int32)
extern "C"  int32_t ParallelDeflateOutputStream_Read_m3642689198 (ParallelDeflateOutputStream_t1234751346 * __this, ByteU5BU5D_t58506160* ___buffer0, int32_t ___offset1, int32_t ___count2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 Ionic.Zlib.ParallelDeflateOutputStream::Seek(System.Int64,System.IO.SeekOrigin)
extern "C"  int64_t ParallelDeflateOutputStream_Seek_m2328189903 (ParallelDeflateOutputStream_t1234751346 * __this, int64_t ___offset0, int32_t ___origin1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Ionic.Zlib.ParallelDeflateOutputStream::SetLength(System.Int64)
extern "C"  void ParallelDeflateOutputStream_SetLength_m555983045 (ParallelDeflateOutputStream_t1234751346 * __this, int64_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
