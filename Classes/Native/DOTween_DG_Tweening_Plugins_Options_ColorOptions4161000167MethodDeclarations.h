﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"


// Methods for marshaling
struct ColorOptions_t4161000167;
struct ColorOptions_t4161000167_marshaled_pinvoke;

extern "C" void ColorOptions_t4161000167_marshal_pinvoke(const ColorOptions_t4161000167& unmarshaled, ColorOptions_t4161000167_marshaled_pinvoke& marshaled);
extern "C" void ColorOptions_t4161000167_marshal_pinvoke_back(const ColorOptions_t4161000167_marshaled_pinvoke& marshaled, ColorOptions_t4161000167& unmarshaled);
extern "C" void ColorOptions_t4161000167_marshal_pinvoke_cleanup(ColorOptions_t4161000167_marshaled_pinvoke& marshaled);

// Methods for marshaling
struct ColorOptions_t4161000167;
struct ColorOptions_t4161000167_marshaled_com;

extern "C" void ColorOptions_t4161000167_marshal_com(const ColorOptions_t4161000167& unmarshaled, ColorOptions_t4161000167_marshaled_com& marshaled);
extern "C" void ColorOptions_t4161000167_marshal_com_back(const ColorOptions_t4161000167_marshaled_com& marshaled, ColorOptions_t4161000167& unmarshaled);
extern "C" void ColorOptions_t4161000167_marshal_com_cleanup(ColorOptions_t4161000167_marshaled_com& marshaled);
