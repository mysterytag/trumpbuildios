﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SimpleJSON.JSONArray/<>c__Iterator2
struct U3CU3Ec__Iterator2_t2516598211;
// SimpleJSON.JSONNode
struct JSONNode_t580622632;
// System.Object
struct Il2CppObject;
// System.Collections.IEnumerator
struct IEnumerator_t287207039;
// System.Collections.Generic.IEnumerator`1<SimpleJSON.JSONNode>
struct IEnumerator_1_t2063729080;

#include "codegen/il2cpp-codegen.h"

// System.Void SimpleJSON.JSONArray/<>c__Iterator2::.ctor()
extern "C"  void U3CU3Ec__Iterator2__ctor_m3872687330 (U3CU3Ec__Iterator2_t2516598211 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// SimpleJSON.JSONNode SimpleJSON.JSONArray/<>c__Iterator2::System.Collections.Generic.IEnumerator<SimpleJSON.JSONNode>.get_Current()
extern "C"  JSONNode_t580622632 * U3CU3Ec__Iterator2_System_Collections_Generic_IEnumeratorU3CSimpleJSON_JSONNodeU3E_get_Current_m1959704405 (U3CU3Ec__Iterator2_t2516598211 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object SimpleJSON.JSONArray/<>c__Iterator2::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CU3Ec__Iterator2_System_Collections_IEnumerator_get_Current_m2408747204 (U3CU3Ec__Iterator2_t2516598211 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator SimpleJSON.JSONArray/<>c__Iterator2::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * U3CU3Ec__Iterator2_System_Collections_IEnumerable_GetEnumerator_m856485975 (U3CU3Ec__Iterator2_t2516598211 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.IEnumerator`1<SimpleJSON.JSONNode> SimpleJSON.JSONArray/<>c__Iterator2::System.Collections.Generic.IEnumerable<SimpleJSON.JSONNode>.GetEnumerator()
extern "C"  Il2CppObject* U3CU3Ec__Iterator2_System_Collections_Generic_IEnumerableU3CSimpleJSON_JSONNodeU3E_GetEnumerator_m1772464492 (U3CU3Ec__Iterator2_t2516598211 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean SimpleJSON.JSONArray/<>c__Iterator2::MoveNext()
extern "C"  bool U3CU3Ec__Iterator2_MoveNext_m3692409834 (U3CU3Ec__Iterator2_t2516598211 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SimpleJSON.JSONArray/<>c__Iterator2::Dispose()
extern "C"  void U3CU3Ec__Iterator2_Dispose_m2109128863 (U3CU3Ec__Iterator2_t2516598211 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SimpleJSON.JSONArray/<>c__Iterator2::Reset()
extern "C"  void U3CU3Ec__Iterator2_Reset_m1519120271 (U3CU3Ec__Iterator2_t2516598211 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
