﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2<System.Object,PlayFab.UUnit.Region>
struct Dictionary_2_t1782110920;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_V1549138861.h"
#include "AssemblyU2DCSharp_PlayFab_UUnit_Region3089759486.h"

// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,PlayFab.UUnit.Region>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void Enumerator__ctor_m4184496641_gshared (Enumerator_t1549138863 * __this, Dictionary_2_t1782110920 * ___host0, const MethodInfo* method);
#define Enumerator__ctor_m4184496641(__this, ___host0, method) ((  void (*) (Enumerator_t1549138863 *, Dictionary_2_t1782110920 *, const MethodInfo*))Enumerator__ctor_m4184496641_gshared)(__this, ___host0, method)
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,PlayFab.UUnit.Region>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m3986598666_gshared (Enumerator_t1549138863 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m3986598666(__this, method) ((  Il2CppObject * (*) (Enumerator_t1549138863 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m3986598666_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,PlayFab.UUnit.Region>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m2638862356_gshared (Enumerator_t1549138863 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_Reset_m2638862356(__this, method) ((  void (*) (Enumerator_t1549138863 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m2638862356_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,PlayFab.UUnit.Region>::Dispose()
extern "C"  void Enumerator_Dispose_m757240547_gshared (Enumerator_t1549138863 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m757240547(__this, method) ((  void (*) (Enumerator_t1549138863 *, const MethodInfo*))Enumerator_Dispose_m757240547_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,PlayFab.UUnit.Region>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m4156730820_gshared (Enumerator_t1549138863 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m4156730820(__this, method) ((  bool (*) (Enumerator_t1549138863 *, const MethodInfo*))Enumerator_MoveNext_m4156730820_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,PlayFab.UUnit.Region>::get_Current()
extern "C"  int32_t Enumerator_get_Current_m3857499622_gshared (Enumerator_t1549138863 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m3857499622(__this, method) ((  int32_t (*) (Enumerator_t1549138863 *, const MethodInfo*))Enumerator_get_Current_m3857499622_gshared)(__this, method)
