﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// StreamAPI/<Filter>c__AnonStorey134`1<System.Object>
struct U3CFilterU3Ec__AnonStorey134_1_t1444715119;
// System.IDisposable
struct IDisposable_t1628921374;
// System.Action`1<System.Object>
struct Action_1_t985559125;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Priority3194150340.h"

// System.Void StreamAPI/<Filter>c__AnonStorey134`1<System.Object>::.ctor()
extern "C"  void U3CFilterU3Ec__AnonStorey134_1__ctor_m3863718218_gshared (U3CFilterU3Ec__AnonStorey134_1_t1444715119 * __this, const MethodInfo* method);
#define U3CFilterU3Ec__AnonStorey134_1__ctor_m3863718218(__this, method) ((  void (*) (U3CFilterU3Ec__AnonStorey134_1_t1444715119 *, const MethodInfo*))U3CFilterU3Ec__AnonStorey134_1__ctor_m3863718218_gshared)(__this, method)
// System.IDisposable StreamAPI/<Filter>c__AnonStorey134`1<System.Object>::<>m__1C5(System.Action`1<T>,Priority)
extern "C"  Il2CppObject * U3CFilterU3Ec__AnonStorey134_1_U3CU3Em__1C5_m262601123_gshared (U3CFilterU3Ec__AnonStorey134_1_t1444715119 * __this, Action_1_t985559125 * ___reaction0, int32_t ___p1, const MethodInfo* method);
#define U3CFilterU3Ec__AnonStorey134_1_U3CU3Em__1C5_m262601123(__this, ___reaction0, ___p1, method) ((  Il2CppObject * (*) (U3CFilterU3Ec__AnonStorey134_1_t1444715119 *, Action_1_t985559125 *, int32_t, const MethodInfo*))U3CFilterU3Ec__AnonStorey134_1_U3CU3Em__1C5_m262601123_gshared)(__this, ___reaction0, ___p1, method)
