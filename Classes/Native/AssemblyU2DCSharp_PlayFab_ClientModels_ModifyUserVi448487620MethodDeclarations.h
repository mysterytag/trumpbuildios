﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.ClientModels.ModifyUserVirtualCurrencyResult
struct ModifyUserVirtualCurrencyResult_t448487620;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String968488902.h"

// System.Void PlayFab.ClientModels.ModifyUserVirtualCurrencyResult::.ctor()
extern "C"  void ModifyUserVirtualCurrencyResult__ctor_m1492780901 (ModifyUserVirtualCurrencyResult_t448487620 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PlayFab.ClientModels.ModifyUserVirtualCurrencyResult::get_PlayFabId()
extern "C"  String_t* ModifyUserVirtualCurrencyResult_get_PlayFabId_m3476370181 (ModifyUserVirtualCurrencyResult_t448487620 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.ModifyUserVirtualCurrencyResult::set_PlayFabId(System.String)
extern "C"  void ModifyUserVirtualCurrencyResult_set_PlayFabId_m115250478 (ModifyUserVirtualCurrencyResult_t448487620 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PlayFab.ClientModels.ModifyUserVirtualCurrencyResult::get_VirtualCurrency()
extern "C"  String_t* ModifyUserVirtualCurrencyResult_get_VirtualCurrency_m275313555 (ModifyUserVirtualCurrencyResult_t448487620 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.ModifyUserVirtualCurrencyResult::set_VirtualCurrency(System.String)
extern "C"  void ModifyUserVirtualCurrencyResult_set_VirtualCurrency_m690739872 (ModifyUserVirtualCurrencyResult_t448487620 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 PlayFab.ClientModels.ModifyUserVirtualCurrencyResult::get_BalanceChange()
extern "C"  int32_t ModifyUserVirtualCurrencyResult_get_BalanceChange_m2905505528 (ModifyUserVirtualCurrencyResult_t448487620 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.ModifyUserVirtualCurrencyResult::set_BalanceChange(System.Int32)
extern "C"  void ModifyUserVirtualCurrencyResult_set_BalanceChange_m1413423267 (ModifyUserVirtualCurrencyResult_t448487620 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 PlayFab.ClientModels.ModifyUserVirtualCurrencyResult::get_Balance()
extern "C"  int32_t ModifyUserVirtualCurrencyResult_get_Balance_m3705576552 (ModifyUserVirtualCurrencyResult_t448487620 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.ModifyUserVirtualCurrencyResult::set_Balance(System.Int32)
extern "C"  void ModifyUserVirtualCurrencyResult_set_Balance_m1030513043 (ModifyUserVirtualCurrencyResult_t448487620 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
