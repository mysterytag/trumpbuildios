﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UniRx.Operators.SelectManyObservable`2<UniRx.Unit,UniRx.Unit>
struct SelectManyObservable_2_t3540602214;
// UniRx.CompositeDisposable
struct CompositeDisposable_t1894629977;
// System.Object
struct Il2CppObject;
// UniRx.SingleAssignmentDisposable
struct SingleAssignmentDisposable_t2336378823;

#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Oper4165376397.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Operators.SelectManyObservable`2/SelectManyObserverWithIndex<UniRx.Unit,UniRx.Unit>
struct  SelectManyObserverWithIndex_t795413344  : public OperatorObserverBase_2_t4165376397
{
public:
	// UniRx.Operators.SelectManyObservable`2<TSource,TResult> UniRx.Operators.SelectManyObservable`2/SelectManyObserverWithIndex::parent
	SelectManyObservable_2_t3540602214 * ___parent_2;
	// UniRx.CompositeDisposable UniRx.Operators.SelectManyObservable`2/SelectManyObserverWithIndex::collectionDisposable
	CompositeDisposable_t1894629977 * ___collectionDisposable_3;
	// System.Int32 UniRx.Operators.SelectManyObservable`2/SelectManyObserverWithIndex::index
	int32_t ___index_4;
	// System.Object UniRx.Operators.SelectManyObservable`2/SelectManyObserverWithIndex::gate
	Il2CppObject * ___gate_5;
	// System.Boolean UniRx.Operators.SelectManyObservable`2/SelectManyObserverWithIndex::isStopped
	bool ___isStopped_6;
	// UniRx.SingleAssignmentDisposable UniRx.Operators.SelectManyObservable`2/SelectManyObserverWithIndex::sourceDisposable
	SingleAssignmentDisposable_t2336378823 * ___sourceDisposable_7;

public:
	inline static int32_t get_offset_of_parent_2() { return static_cast<int32_t>(offsetof(SelectManyObserverWithIndex_t795413344, ___parent_2)); }
	inline SelectManyObservable_2_t3540602214 * get_parent_2() const { return ___parent_2; }
	inline SelectManyObservable_2_t3540602214 ** get_address_of_parent_2() { return &___parent_2; }
	inline void set_parent_2(SelectManyObservable_2_t3540602214 * value)
	{
		___parent_2 = value;
		Il2CppCodeGenWriteBarrier(&___parent_2, value);
	}

	inline static int32_t get_offset_of_collectionDisposable_3() { return static_cast<int32_t>(offsetof(SelectManyObserverWithIndex_t795413344, ___collectionDisposable_3)); }
	inline CompositeDisposable_t1894629977 * get_collectionDisposable_3() const { return ___collectionDisposable_3; }
	inline CompositeDisposable_t1894629977 ** get_address_of_collectionDisposable_3() { return &___collectionDisposable_3; }
	inline void set_collectionDisposable_3(CompositeDisposable_t1894629977 * value)
	{
		___collectionDisposable_3 = value;
		Il2CppCodeGenWriteBarrier(&___collectionDisposable_3, value);
	}

	inline static int32_t get_offset_of_index_4() { return static_cast<int32_t>(offsetof(SelectManyObserverWithIndex_t795413344, ___index_4)); }
	inline int32_t get_index_4() const { return ___index_4; }
	inline int32_t* get_address_of_index_4() { return &___index_4; }
	inline void set_index_4(int32_t value)
	{
		___index_4 = value;
	}

	inline static int32_t get_offset_of_gate_5() { return static_cast<int32_t>(offsetof(SelectManyObserverWithIndex_t795413344, ___gate_5)); }
	inline Il2CppObject * get_gate_5() const { return ___gate_5; }
	inline Il2CppObject ** get_address_of_gate_5() { return &___gate_5; }
	inline void set_gate_5(Il2CppObject * value)
	{
		___gate_5 = value;
		Il2CppCodeGenWriteBarrier(&___gate_5, value);
	}

	inline static int32_t get_offset_of_isStopped_6() { return static_cast<int32_t>(offsetof(SelectManyObserverWithIndex_t795413344, ___isStopped_6)); }
	inline bool get_isStopped_6() const { return ___isStopped_6; }
	inline bool* get_address_of_isStopped_6() { return &___isStopped_6; }
	inline void set_isStopped_6(bool value)
	{
		___isStopped_6 = value;
	}

	inline static int32_t get_offset_of_sourceDisposable_7() { return static_cast<int32_t>(offsetof(SelectManyObserverWithIndex_t795413344, ___sourceDisposable_7)); }
	inline SingleAssignmentDisposable_t2336378823 * get_sourceDisposable_7() const { return ___sourceDisposable_7; }
	inline SingleAssignmentDisposable_t2336378823 ** get_address_of_sourceDisposable_7() { return &___sourceDisposable_7; }
	inline void set_sourceDisposable_7(SingleAssignmentDisposable_t2336378823 * value)
	{
		___sourceDisposable_7 = value;
		Il2CppCodeGenWriteBarrier(&___sourceDisposable_7, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
