﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UniRx.Subject`1<UnityEngine.EventSystems.PointerEventData>
struct Subject_1_t848168958;

#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Triggers_Obser1883237895.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Triggers.ObservablePointerClickTrigger
struct  ObservablePointerClickTrigger_t3710995019  : public ObservableTriggerBase_t1883237895
{
public:
	// UniRx.Subject`1<UnityEngine.EventSystems.PointerEventData> UniRx.Triggers.ObservablePointerClickTrigger::onPointerClick
	Subject_1_t848168958 * ___onPointerClick_8;

public:
	inline static int32_t get_offset_of_onPointerClick_8() { return static_cast<int32_t>(offsetof(ObservablePointerClickTrigger_t3710995019, ___onPointerClick_8)); }
	inline Subject_1_t848168958 * get_onPointerClick_8() const { return ___onPointerClick_8; }
	inline Subject_1_t848168958 ** get_address_of_onPointerClick_8() { return &___onPointerClick_8; }
	inline void set_onPointerClick_8(Subject_1_t848168958 * value)
	{
		___onPointerClick_8 = value;
		Il2CppCodeGenWriteBarrier(&___onPointerClick_8, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
