﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.SelectManyObservable`3/SelectManyOuterObserver/SelectMany<System.Object,UniRx.Unit,System.Object>
struct SelectMany_t3705584898;
// UniRx.Operators.SelectManyObservable`3/SelectManyOuterObserver<System.Object,UniRx.Unit,System.Object>
struct SelectManyOuterObserver_t543945037;
// System.Object
struct Il2CppObject;
// System.IDisposable
struct IDisposable_t1628921374;
// System.Exception
struct Exception_t1967233988;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Unit2558286038.h"
#include "mscorlib_System_Exception1967233988.h"

// System.Void UniRx.Operators.SelectManyObservable`3/SelectManyOuterObserver/SelectMany<System.Object,UniRx.Unit,System.Object>::.ctor(UniRx.Operators.SelectManyObservable`3/SelectManyOuterObserver<TSource,TCollection,TResult>,TSource,System.IDisposable)
extern "C"  void SelectMany__ctor_m3036632148_gshared (SelectMany_t3705584898 * __this, SelectManyOuterObserver_t543945037 * ___parent0, Il2CppObject * ___value1, Il2CppObject * ___cancel2, const MethodInfo* method);
#define SelectMany__ctor_m3036632148(__this, ___parent0, ___value1, ___cancel2, method) ((  void (*) (SelectMany_t3705584898 *, SelectManyOuterObserver_t543945037 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))SelectMany__ctor_m3036632148_gshared)(__this, ___parent0, ___value1, ___cancel2, method)
// System.Void UniRx.Operators.SelectManyObservable`3/SelectManyOuterObserver/SelectMany<System.Object,UniRx.Unit,System.Object>::OnNext(TCollection)
extern "C"  void SelectMany_OnNext_m582151840_gshared (SelectMany_t3705584898 * __this, Unit_t2558286038  ___value0, const MethodInfo* method);
#define SelectMany_OnNext_m582151840(__this, ___value0, method) ((  void (*) (SelectMany_t3705584898 *, Unit_t2558286038 , const MethodInfo*))SelectMany_OnNext_m582151840_gshared)(__this, ___value0, method)
// System.Void UniRx.Operators.SelectManyObservable`3/SelectManyOuterObserver/SelectMany<System.Object,UniRx.Unit,System.Object>::OnError(System.Exception)
extern "C"  void SelectMany_OnError_m2271092461_gshared (SelectMany_t3705584898 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method);
#define SelectMany_OnError_m2271092461(__this, ___error0, method) ((  void (*) (SelectMany_t3705584898 *, Exception_t1967233988 *, const MethodInfo*))SelectMany_OnError_m2271092461_gshared)(__this, ___error0, method)
// System.Void UniRx.Operators.SelectManyObservable`3/SelectManyOuterObserver/SelectMany<System.Object,UniRx.Unit,System.Object>::OnCompleted()
extern "C"  void SelectMany_OnCompleted_m941231552_gshared (SelectMany_t3705584898 * __this, const MethodInfo* method);
#define SelectMany_OnCompleted_m941231552(__this, method) ((  void (*) (SelectMany_t3705584898 *, const MethodInfo*))SelectMany_OnCompleted_m941231552_gshared)(__this, method)
