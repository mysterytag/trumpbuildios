﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Predicate_1_gen1408070318MethodDeclarations.h"

// System.Void System.Predicate`1<Zenject.ProviderBase>::.ctor(System.Object,System.IntPtr)
#define Predicate_1__ctor_m1073716977(__this, ___object0, ___method1, method) ((  void (*) (Predicate_1_t2198458289 *, Il2CppObject *, IntPtr_t, const MethodInfo*))Predicate_1__ctor_m982040097_gshared)(__this, ___object0, ___method1, method)
// System.Boolean System.Predicate`1<Zenject.ProviderBase>::Invoke(T)
#define Predicate_1_Invoke_m660109256(__this, ___obj0, method) ((  bool (*) (Predicate_1_t2198458289 *, ProviderBase_t1627494391 *, const MethodInfo*))Predicate_1_Invoke_m4106178309_gshared)(__this, ___obj0, method)
// System.IAsyncResult System.Predicate`1<Zenject.ProviderBase>::BeginInvoke(T,System.AsyncCallback,System.Object)
#define Predicate_1_BeginInvoke_m949949911(__this, ___obj0, ___callback1, ___object2, method) ((  Il2CppObject * (*) (Predicate_1_t2198458289 *, ProviderBase_t1627494391 *, AsyncCallback_t1363551830 *, Il2CppObject *, const MethodInfo*))Predicate_1_BeginInvoke_m2038073176_gshared)(__this, ___obj0, ___callback1, ___object2, method)
// System.Boolean System.Predicate`1<Zenject.ProviderBase>::EndInvoke(System.IAsyncResult)
#define Predicate_1_EndInvoke_m3770022156(__this, ___result0, method) ((  bool (*) (Predicate_1_t2198458289 *, Il2CppObject *, const MethodInfo*))Predicate_1_EndInvoke_m3970497007_gshared)(__this, ___result0, method)
