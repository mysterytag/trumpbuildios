﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Zenject.IFactoryBinder`1<System.Object>
struct IFactoryBinder_1_t1064970850;
// Zenject.DiContainer
struct DiContainer_t2383114449;
// System.String
struct String_t;
// Zenject.BindingConditionSetter
struct BindingConditionSetter_t259147722;
// System.Object
struct Il2CppObject;
// System.Func`2<Zenject.DiContainer,System.Object>
struct Func_2_t1206216135;
// UnityEngine.GameObject
struct GameObject_t4012695102;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Zenject_DiContainer2383114449.h"
#include "mscorlib_System_String968488902.h"
#include "mscorlib_System_Object837106420.h"
#include "UnityEngine_UnityEngine_GameObject4012695102.h"

// System.Void Zenject.IFactoryBinder`1<System.Object>::.ctor(Zenject.DiContainer,System.String)
extern "C"  void IFactoryBinder_1__ctor_m1944281127_gshared (IFactoryBinder_1_t1064970850 * __this, DiContainer_t2383114449 * ___container0, String_t* ___identifier1, const MethodInfo* method);
#define IFactoryBinder_1__ctor_m1944281127(__this, ___container0, ___identifier1, method) ((  void (*) (IFactoryBinder_1_t1064970850 *, DiContainer_t2383114449 *, String_t*, const MethodInfo*))IFactoryBinder_1__ctor_m1944281127_gshared)(__this, ___container0, ___identifier1, method)
// Zenject.BindingConditionSetter Zenject.IFactoryBinder`1<System.Object>::ToInstance(TContract)
extern "C"  BindingConditionSetter_t259147722 * IFactoryBinder_1_ToInstance_m20710155_gshared (IFactoryBinder_1_t1064970850 * __this, Il2CppObject * ___instance0, const MethodInfo* method);
#define IFactoryBinder_1_ToInstance_m20710155(__this, ___instance0, method) ((  BindingConditionSetter_t259147722 * (*) (IFactoryBinder_1_t1064970850 *, Il2CppObject *, const MethodInfo*))IFactoryBinder_1_ToInstance_m20710155_gshared)(__this, ___instance0, method)
// Zenject.BindingConditionSetter Zenject.IFactoryBinder`1<System.Object>::ToMethod(System.Func`2<Zenject.DiContainer,TContract>)
extern "C"  BindingConditionSetter_t259147722 * IFactoryBinder_1_ToMethod_m1972193855_gshared (IFactoryBinder_1_t1064970850 * __this, Func_2_t1206216135 * ___method0, const MethodInfo* method);
#define IFactoryBinder_1_ToMethod_m1972193855(__this, ___method0, method) ((  BindingConditionSetter_t259147722 * (*) (IFactoryBinder_1_t1064970850 *, Func_2_t1206216135 *, const MethodInfo*))IFactoryBinder_1_ToMethod_m1972193855_gshared)(__this, ___method0, method)
// Zenject.BindingConditionSetter Zenject.IFactoryBinder`1<System.Object>::ToFactory()
extern "C"  BindingConditionSetter_t259147722 * IFactoryBinder_1_ToFactory_m555729664_gshared (IFactoryBinder_1_t1064970850 * __this, const MethodInfo* method);
#define IFactoryBinder_1_ToFactory_m555729664(__this, method) ((  BindingConditionSetter_t259147722 * (*) (IFactoryBinder_1_t1064970850 *, const MethodInfo*))IFactoryBinder_1_ToFactory_m555729664_gshared)(__this, method)
// Zenject.BindingConditionSetter Zenject.IFactoryBinder`1<System.Object>::ToPrefab(UnityEngine.GameObject)
extern "C"  BindingConditionSetter_t259147722 * IFactoryBinder_1_ToPrefab_m3306141416_gshared (IFactoryBinder_1_t1064970850 * __this, GameObject_t4012695102 * ___prefab0, const MethodInfo* method);
#define IFactoryBinder_1_ToPrefab_m3306141416(__this, ___prefab0, method) ((  BindingConditionSetter_t259147722 * (*) (IFactoryBinder_1_t1064970850 *, GameObject_t4012695102 *, const MethodInfo*))IFactoryBinder_1_ToPrefab_m3306141416_gshared)(__this, ___prefab0, method)
