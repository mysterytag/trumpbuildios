﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_SubstanceBase_2_Intrusion_gen4252761119MethodDeclarations.h"

// System.Void SubstanceBase`2/Intrusion<System.Single,System.Func`1<System.Single>>::.ctor()
#define Intrusion__ctor_m1871648162(__this, method) ((  void (*) (Intrusion_t1221677671 *, const MethodInfo*))Intrusion__ctor_m1448835205_gshared)(__this, method)
