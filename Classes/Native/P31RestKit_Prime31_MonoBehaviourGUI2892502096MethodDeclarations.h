﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Prime31.MonoBehaviourGUI
struct MonoBehaviourGUI_t2892502096;
// UnityEngine.Texture2D
struct Texture2D_t2509538522;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String968488902.h"
#include "UnityEngine_UnityEngine_LogType3529269451.h"

// System.Void Prime31.MonoBehaviourGUI::.ctor()
extern "C"  void MonoBehaviourGUI__ctor_m3698201455 (MonoBehaviourGUI_t2892502096 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Texture2D Prime31.MonoBehaviourGUI::get_normalBackground()
extern "C"  Texture2D_t2509538522 * MonoBehaviourGUI_get_normalBackground_m1355396176 (MonoBehaviourGUI_t2892502096 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Texture2D Prime31.MonoBehaviourGUI::get_bottomButtonBackground()
extern "C"  Texture2D_t2509538522 * MonoBehaviourGUI_get_bottomButtonBackground_m261950502 (MonoBehaviourGUI_t2892502096 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Texture2D Prime31.MonoBehaviourGUI::get_activeBackground()
extern "C"  Texture2D_t2509538522 * MonoBehaviourGUI_get_activeBackground_m3938271215 (MonoBehaviourGUI_t2892502096 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Texture2D Prime31.MonoBehaviourGUI::get_toggleButtonBackground()
extern "C"  Texture2D_t2509538522 * MonoBehaviourGUI_get_toggleButtonBackground_m720475407 (MonoBehaviourGUI_t2892502096 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Prime31.MonoBehaviourGUI::isRetinaOrLargeScreen()
extern "C"  bool MonoBehaviourGUI_isRetinaOrLargeScreen_m14176428 (MonoBehaviourGUI_t2892502096 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Prime31.MonoBehaviourGUI::isRetinaIpad()
extern "C"  bool MonoBehaviourGUI_isRetinaIpad_m1920871204 (MonoBehaviourGUI_t2892502096 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Prime31.MonoBehaviourGUI::buttonHeight()
extern "C"  int32_t MonoBehaviourGUI_buttonHeight_m3177878504 (MonoBehaviourGUI_t2892502096 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Prime31.MonoBehaviourGUI::buttonFontSize()
extern "C"  int32_t MonoBehaviourGUI_buttonFontSize_m2180707697 (MonoBehaviourGUI_t2892502096 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Prime31.MonoBehaviourGUI::paintWindow(System.Int32)
extern "C"  void MonoBehaviourGUI_paintWindow_m1070764780 (MonoBehaviourGUI_t2892502096 * __this, int32_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Prime31.MonoBehaviourGUI::handleLog(System.String,System.String,UnityEngine.LogType)
extern "C"  void MonoBehaviourGUI_handleLog_m986101754 (MonoBehaviourGUI_t2892502096 * __this, String_t* ___logString0, String_t* ___stackTrace1, int32_t ___type2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Prime31.MonoBehaviourGUI::OnDestroy()
extern "C"  void MonoBehaviourGUI_OnDestroy_m2535102696 (MonoBehaviourGUI_t2892502096 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Prime31.MonoBehaviourGUI::Update()
extern "C"  void MonoBehaviourGUI_Update_m406990206 (MonoBehaviourGUI_t2892502096 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Prime31.MonoBehaviourGUI::beginColumn()
extern "C"  void MonoBehaviourGUI_beginColumn_m1784932684 (MonoBehaviourGUI_t2892502096 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Prime31.MonoBehaviourGUI::endColumn()
extern "C"  void MonoBehaviourGUI_endColumn_m1045521022 (MonoBehaviourGUI_t2892502096 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Prime31.MonoBehaviourGUI::endColumn(System.Boolean)
extern "C"  void MonoBehaviourGUI_endColumn_m2544034677 (MonoBehaviourGUI_t2892502096 * __this, bool ___hasSecondColumn0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Prime31.MonoBehaviourGUI::beginRightColumn()
extern "C"  void MonoBehaviourGUI_beginRightColumn_m4052146110 (MonoBehaviourGUI_t2892502096 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Prime31.MonoBehaviourGUI::button(System.String)
extern "C"  bool MonoBehaviourGUI_button_m3362767963 (MonoBehaviourGUI_t2892502096 * __this, String_t* ___text0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Prime31.MonoBehaviourGUI::bottomRightButton(System.String,System.Single)
extern "C"  bool MonoBehaviourGUI_bottomRightButton_m939646391 (MonoBehaviourGUI_t2892502096 * __this, String_t* ___text0, float ___width1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Prime31.MonoBehaviourGUI::bottomLeftButton(System.String,System.Single)
extern "C"  bool MonoBehaviourGUI_bottomLeftButton_m3846189774 (MonoBehaviourGUI_t2892502096 * __this, String_t* ___text0, float ___width1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Prime31.MonoBehaviourGUI::bottomCenterButton(System.String,System.Single)
extern "C"  bool MonoBehaviourGUI_bottomCenterButton_m4278278016 (MonoBehaviourGUI_t2892502096 * __this, String_t* ___text0, float ___width1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Prime31.MonoBehaviourGUI::toggleButton(System.String,System.String)
extern "C"  bool MonoBehaviourGUI_toggleButton_m2496466019 (MonoBehaviourGUI_t2892502096 * __this, String_t* ___defaultText0, String_t* ___selectedText1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Prime31.MonoBehaviourGUI::toggleButtonState(System.String)
extern "C"  bool MonoBehaviourGUI_toggleButtonState_m2862615050 (MonoBehaviourGUI_t2892502096 * __this, String_t* ___defaultText0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
