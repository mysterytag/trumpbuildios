﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SubstanceBase`2/DisposableAffect<System.Single,System.Object>
struct DisposableAffect_t266153449;

#include "codegen/il2cpp-codegen.h"

// System.Void SubstanceBase`2/DisposableAffect<System.Single,System.Object>::.ctor()
extern "C"  void DisposableAffect__ctor_m1237754747_gshared (DisposableAffect_t266153449 * __this, const MethodInfo* method);
#define DisposableAffect__ctor_m1237754747(__this, method) ((  void (*) (DisposableAffect_t266153449 *, const MethodInfo*))DisposableAffect__ctor_m1237754747_gshared)(__this, method)
// System.Void SubstanceBase`2/DisposableAffect<System.Single,System.Object>::Dispose()
extern "C"  void DisposableAffect_Dispose_m3969621240_gshared (DisposableAffect_t266153449 * __this, const MethodInfo* method);
#define DisposableAffect_Dispose_m3969621240(__this, method) ((  void (*) (DisposableAffect_t266153449 *, const MethodInfo*))DisposableAffect_Dispose_m3969621240_gshared)(__this, method)
