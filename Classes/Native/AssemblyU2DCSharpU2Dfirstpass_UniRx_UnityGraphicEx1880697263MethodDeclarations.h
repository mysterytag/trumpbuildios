﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.UnityGraphicExtensions/<DirtyMaterialCallbackAsObservable>c__AnonStoreyA0
struct U3CDirtyMaterialCallbackAsObservableU3Ec__AnonStoreyA0_t1880697263;
// System.IDisposable
struct IDisposable_t1628921374;
// UniRx.IObserver`1<UniRx.Unit>
struct IObserver_1_t475317645;

#include "codegen/il2cpp-codegen.h"

// System.Void UniRx.UnityGraphicExtensions/<DirtyMaterialCallbackAsObservable>c__AnonStoreyA0::.ctor()
extern "C"  void U3CDirtyMaterialCallbackAsObservableU3Ec__AnonStoreyA0__ctor_m2952219480 (U3CDirtyMaterialCallbackAsObservableU3Ec__AnonStoreyA0_t1880697263 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IDisposable UniRx.UnityGraphicExtensions/<DirtyMaterialCallbackAsObservable>c__AnonStoreyA0::<>m__D9(UniRx.IObserver`1<UniRx.Unit>)
extern "C"  Il2CppObject * U3CDirtyMaterialCallbackAsObservableU3Ec__AnonStoreyA0_U3CU3Em__D9_m3768827395 (U3CDirtyMaterialCallbackAsObservableU3Ec__AnonStoreyA0_t1880697263 * __this, Il2CppObject* ___observer0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
