﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// StartSceneInstaller
struct StartSceneInstaller_t3492553022;
// UnityEngine.Canvas
struct Canvas_t3534013893;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Canvas3534013893.h"

// System.Void StartSceneInstaller::.ctor()
extern "C"  void StartSceneInstaller__ctor_m3838867437 (StartSceneInstaller_t3492553022 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void StartSceneInstaller::.cctor()
extern "C"  void StartSceneInstaller__cctor_m2558677344 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void StartSceneInstaller::InstallBindings()
extern "C"  void StartSceneInstaller_InstallBindings_m2277346196 (StartSceneInstaller_t3492553022 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean StartSceneInstaller::<InstallBindings>m__223(UnityEngine.Canvas)
extern "C"  bool StartSceneInstaller_U3CInstallBindingsU3Em__223_m1442034023 (Il2CppObject * __this /* static, unused */, Canvas_t3534013893 * ___canvas0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
