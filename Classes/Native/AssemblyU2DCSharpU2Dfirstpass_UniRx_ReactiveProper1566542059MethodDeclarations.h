﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.ReactiveProperty`1<System.Single>
struct ReactiveProperty_1_t1566542059;
// UniRx.IObservable`1<System.Single>
struct IObservable_1_t717007385;
// System.Collections.Generic.IEqualityComparer`1<System.Single>
struct IEqualityComparer_1_t3282475672;
// System.IDisposable
struct IDisposable_t1628921374;
// UniRx.IObserver`1<System.Single>
struct IObserver_1_t3170207924;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"

// System.Void UniRx.ReactiveProperty`1<System.Single>::.ctor()
extern "C"  void ReactiveProperty_1__ctor_m1840047419_gshared (ReactiveProperty_1_t1566542059 * __this, const MethodInfo* method);
#define ReactiveProperty_1__ctor_m1840047419(__this, method) ((  void (*) (ReactiveProperty_1_t1566542059 *, const MethodInfo*))ReactiveProperty_1__ctor_m1840047419_gshared)(__this, method)
// System.Void UniRx.ReactiveProperty`1<System.Single>::.ctor(T)
extern "C"  void ReactiveProperty_1__ctor_m1206896515_gshared (ReactiveProperty_1_t1566542059 * __this, float ___initialValue0, const MethodInfo* method);
#define ReactiveProperty_1__ctor_m1206896515(__this, ___initialValue0, method) ((  void (*) (ReactiveProperty_1_t1566542059 *, float, const MethodInfo*))ReactiveProperty_1__ctor_m1206896515_gshared)(__this, ___initialValue0, method)
// System.Void UniRx.ReactiveProperty`1<System.Single>::.ctor(UniRx.IObservable`1<T>)
extern "C"  void ReactiveProperty_1__ctor_m211573978_gshared (ReactiveProperty_1_t1566542059 * __this, Il2CppObject* ___source0, const MethodInfo* method);
#define ReactiveProperty_1__ctor_m211573978(__this, ___source0, method) ((  void (*) (ReactiveProperty_1_t1566542059 *, Il2CppObject*, const MethodInfo*))ReactiveProperty_1__ctor_m211573978_gshared)(__this, ___source0, method)
// System.Void UniRx.ReactiveProperty`1<System.Single>::.ctor(UniRx.IObservable`1<T>,T)
extern "C"  void ReactiveProperty_1__ctor_m1459135474_gshared (ReactiveProperty_1_t1566542059 * __this, Il2CppObject* ___source0, float ___initialValue1, const MethodInfo* method);
#define ReactiveProperty_1__ctor_m1459135474(__this, ___source0, ___initialValue1, method) ((  void (*) (ReactiveProperty_1_t1566542059 *, Il2CppObject*, float, const MethodInfo*))ReactiveProperty_1__ctor_m1459135474_gshared)(__this, ___source0, ___initialValue1, method)
// System.Void UniRx.ReactiveProperty`1<System.Single>::.cctor()
extern "C"  void ReactiveProperty_1__cctor_m724798930_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define ReactiveProperty_1__cctor_m724798930(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))ReactiveProperty_1__cctor_m724798930_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.IEqualityComparer`1<T> UniRx.ReactiveProperty`1<System.Single>::get_EqualityComparer()
extern "C"  Il2CppObject* ReactiveProperty_1_get_EqualityComparer_m2339768890_gshared (ReactiveProperty_1_t1566542059 * __this, const MethodInfo* method);
#define ReactiveProperty_1_get_EqualityComparer_m2339768890(__this, method) ((  Il2CppObject* (*) (ReactiveProperty_1_t1566542059 *, const MethodInfo*))ReactiveProperty_1_get_EqualityComparer_m2339768890_gshared)(__this, method)
// T UniRx.ReactiveProperty`1<System.Single>::get_Value()
extern "C"  float ReactiveProperty_1_get_Value_m1116491936_gshared (ReactiveProperty_1_t1566542059 * __this, const MethodInfo* method);
#define ReactiveProperty_1_get_Value_m1116491936(__this, method) ((  float (*) (ReactiveProperty_1_t1566542059 *, const MethodInfo*))ReactiveProperty_1_get_Value_m1116491936_gshared)(__this, method)
// System.Void UniRx.ReactiveProperty`1<System.Single>::set_Value(T)
extern "C"  void ReactiveProperty_1_set_Value_m1145205329_gshared (ReactiveProperty_1_t1566542059 * __this, float ___value0, const MethodInfo* method);
#define ReactiveProperty_1_set_Value_m1145205329(__this, ___value0, method) ((  void (*) (ReactiveProperty_1_t1566542059 *, float, const MethodInfo*))ReactiveProperty_1_set_Value_m1145205329_gshared)(__this, ___value0, method)
// System.Void UniRx.ReactiveProperty`1<System.Single>::SetValue(T)
extern "C"  void ReactiveProperty_1_SetValue_m2477933894_gshared (ReactiveProperty_1_t1566542059 * __this, float ___value0, const MethodInfo* method);
#define ReactiveProperty_1_SetValue_m2477933894(__this, ___value0, method) ((  void (*) (ReactiveProperty_1_t1566542059 *, float, const MethodInfo*))ReactiveProperty_1_SetValue_m2477933894_gshared)(__this, ___value0, method)
// System.Void UniRx.ReactiveProperty`1<System.Single>::SetValueAndForceNotify(T)
extern "C"  void ReactiveProperty_1_SetValueAndForceNotify_m3784939657_gshared (ReactiveProperty_1_t1566542059 * __this, float ___value0, const MethodInfo* method);
#define ReactiveProperty_1_SetValueAndForceNotify_m3784939657(__this, ___value0, method) ((  void (*) (ReactiveProperty_1_t1566542059 *, float, const MethodInfo*))ReactiveProperty_1_SetValueAndForceNotify_m3784939657_gshared)(__this, ___value0, method)
// System.IDisposable UniRx.ReactiveProperty`1<System.Single>::Subscribe(UniRx.IObserver`1<T>)
extern "C"  Il2CppObject * ReactiveProperty_1_Subscribe_m3501743376_gshared (ReactiveProperty_1_t1566542059 * __this, Il2CppObject* ___observer0, const MethodInfo* method);
#define ReactiveProperty_1_Subscribe_m3501743376(__this, ___observer0, method) ((  Il2CppObject * (*) (ReactiveProperty_1_t1566542059 *, Il2CppObject*, const MethodInfo*))ReactiveProperty_1_Subscribe_m3501743376_gshared)(__this, ___observer0, method)
// System.Void UniRx.ReactiveProperty`1<System.Single>::Dispose()
extern "C"  void ReactiveProperty_1_Dispose_m2952294072_gshared (ReactiveProperty_1_t1566542059 * __this, const MethodInfo* method);
#define ReactiveProperty_1_Dispose_m2952294072(__this, method) ((  void (*) (ReactiveProperty_1_t1566542059 *, const MethodInfo*))ReactiveProperty_1_Dispose_m2952294072_gshared)(__this, method)
// System.Void UniRx.ReactiveProperty`1<System.Single>::Dispose(System.Boolean)
extern "C"  void ReactiveProperty_1_Dispose_m1035665967_gshared (ReactiveProperty_1_t1566542059 * __this, bool ___disposing0, const MethodInfo* method);
#define ReactiveProperty_1_Dispose_m1035665967(__this, ___disposing0, method) ((  void (*) (ReactiveProperty_1_t1566542059 *, bool, const MethodInfo*))ReactiveProperty_1_Dispose_m1035665967_gshared)(__this, ___disposing0, method)
// System.String UniRx.ReactiveProperty`1<System.Single>::ToString()
extern "C"  String_t* ReactiveProperty_1_ToString_m728599570_gshared (ReactiveProperty_1_t1566542059 * __this, const MethodInfo* method);
#define ReactiveProperty_1_ToString_m728599570(__this, method) ((  String_t* (*) (ReactiveProperty_1_t1566542059 *, const MethodInfo*))ReactiveProperty_1_ToString_m728599570_gshared)(__this, method)
// System.Boolean UniRx.ReactiveProperty`1<System.Single>::IsRequiredSubscribeOnCurrentThread()
extern "C"  bool ReactiveProperty_1_IsRequiredSubscribeOnCurrentThread_m470427760_gshared (ReactiveProperty_1_t1566542059 * __this, const MethodInfo* method);
#define ReactiveProperty_1_IsRequiredSubscribeOnCurrentThread_m470427760(__this, method) ((  bool (*) (ReactiveProperty_1_t1566542059 *, const MethodInfo*))ReactiveProperty_1_IsRequiredSubscribeOnCurrentThread_m470427760_gshared)(__this, method)
