﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Triggers.ObservableTrigger2DTrigger
struct ObservableTrigger2DTrigger_t885825170;
// UnityEngine.Collider2D
struct Collider2D_t1890038195;
// UniRx.IObservable`1<UnityEngine.Collider2D>
struct IObservable_1_t1648836559;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Collider2D1890038195.h"

// System.Void UniRx.Triggers.ObservableTrigger2DTrigger::.ctor()
extern "C"  void ObservableTrigger2DTrigger__ctor_m2960593323 (ObservableTrigger2DTrigger_t885825170 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.Triggers.ObservableTrigger2DTrigger::OnTriggerEnter2D(UnityEngine.Collider2D)
extern "C"  void ObservableTrigger2DTrigger_OnTriggerEnter2D_m3903598381 (ObservableTrigger2DTrigger_t885825170 * __this, Collider2D_t1890038195 * ___other0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UniRx.IObservable`1<UnityEngine.Collider2D> UniRx.Triggers.ObservableTrigger2DTrigger::OnTriggerEnter2DAsObservable()
extern "C"  Il2CppObject* ObservableTrigger2DTrigger_OnTriggerEnter2DAsObservable_m3895651254 (ObservableTrigger2DTrigger_t885825170 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.Triggers.ObservableTrigger2DTrigger::OnTriggerExit2D(UnityEngine.Collider2D)
extern "C"  void ObservableTrigger2DTrigger_OnTriggerExit2D_m3262348245 (ObservableTrigger2DTrigger_t885825170 * __this, Collider2D_t1890038195 * ___other0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UniRx.IObservable`1<UnityEngine.Collider2D> UniRx.Triggers.ObservableTrigger2DTrigger::OnTriggerExit2DAsObservable()
extern "C"  Il2CppObject* ObservableTrigger2DTrigger_OnTriggerExit2DAsObservable_m4124793104 (ObservableTrigger2DTrigger_t885825170 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.Triggers.ObservableTrigger2DTrigger::OnTriggerStay2D(UnityEngine.Collider2D)
extern "C"  void ObservableTrigger2DTrigger_OnTriggerStay2D_m3199362384 (ObservableTrigger2DTrigger_t885825170 * __this, Collider2D_t1890038195 * ___other0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UniRx.IObservable`1<UnityEngine.Collider2D> UniRx.Triggers.ObservableTrigger2DTrigger::OnTriggerStay2DAsObservable()
extern "C"  Il2CppObject* ObservableTrigger2DTrigger_OnTriggerStay2DAsObservable_m3637366859 (ObservableTrigger2DTrigger_t885825170 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.Triggers.ObservableTrigger2DTrigger::RaiseOnCompletedOnDestroy()
extern "C"  void ObservableTrigger2DTrigger_RaiseOnCompletedOnDestroy_m2139448676 (ObservableTrigger2DTrigger_t885825170 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
