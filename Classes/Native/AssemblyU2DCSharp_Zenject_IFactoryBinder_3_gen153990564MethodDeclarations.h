﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Zenject.IFactoryBinder`3<System.Object,System.Object,System.Object>
struct IFactoryBinder_3_t153990564;
// Zenject.DiContainer
struct DiContainer_t2383114449;
// System.String
struct String_t;
// Zenject.BindingConditionSetter
struct BindingConditionSetter_t259147722;
// System.Func`4<Zenject.DiContainer,System.Object,System.Object,System.Object>
struct Func_4_t2229882165;
// UnityEngine.GameObject
struct GameObject_t4012695102;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Zenject_DiContainer2383114449.h"
#include "mscorlib_System_String968488902.h"
#include "UnityEngine_UnityEngine_GameObject4012695102.h"

// System.Void Zenject.IFactoryBinder`3<System.Object,System.Object,System.Object>::.ctor(Zenject.DiContainer,System.String)
extern "C"  void IFactoryBinder_3__ctor_m2916354433_gshared (IFactoryBinder_3_t153990564 * __this, DiContainer_t2383114449 * ___container0, String_t* ___identifier1, const MethodInfo* method);
#define IFactoryBinder_3__ctor_m2916354433(__this, ___container0, ___identifier1, method) ((  void (*) (IFactoryBinder_3_t153990564 *, DiContainer_t2383114449 *, String_t*, const MethodInfo*))IFactoryBinder_3__ctor_m2916354433_gshared)(__this, ___container0, ___identifier1, method)
// Zenject.BindingConditionSetter Zenject.IFactoryBinder`3<System.Object,System.Object,System.Object>::ToMethod(System.Func`4<Zenject.DiContainer,TParam1,TParam2,TContract>)
extern "C"  BindingConditionSetter_t259147722 * IFactoryBinder_3_ToMethod_m1504760372_gshared (IFactoryBinder_3_t153990564 * __this, Func_4_t2229882165 * ___method0, const MethodInfo* method);
#define IFactoryBinder_3_ToMethod_m1504760372(__this, ___method0, method) ((  BindingConditionSetter_t259147722 * (*) (IFactoryBinder_3_t153990564 *, Func_4_t2229882165 *, const MethodInfo*))IFactoryBinder_3_ToMethod_m1504760372_gshared)(__this, ___method0, method)
// Zenject.BindingConditionSetter Zenject.IFactoryBinder`3<System.Object,System.Object,System.Object>::ToFactory()
extern "C"  BindingConditionSetter_t259147722 * IFactoryBinder_3_ToFactory_m208757734_gshared (IFactoryBinder_3_t153990564 * __this, const MethodInfo* method);
#define IFactoryBinder_3_ToFactory_m208757734(__this, method) ((  BindingConditionSetter_t259147722 * (*) (IFactoryBinder_3_t153990564 *, const MethodInfo*))IFactoryBinder_3_ToFactory_m208757734_gshared)(__this, method)
// Zenject.BindingConditionSetter Zenject.IFactoryBinder`3<System.Object,System.Object,System.Object>::ToPrefab(UnityEngine.GameObject)
extern "C"  BindingConditionSetter_t259147722 * IFactoryBinder_3_ToPrefab_m925352386_gshared (IFactoryBinder_3_t153990564 * __this, GameObject_t4012695102 * ___prefab0, const MethodInfo* method);
#define IFactoryBinder_3_ToPrefab_m925352386(__this, ___prefab0, method) ((  BindingConditionSetter_t259147722 * (*) (IFactoryBinder_3_t153990564 *, GameObject_t4012695102 *, const MethodInfo*))IFactoryBinder_3_ToPrefab_m925352386_gshared)(__this, ___prefab0, method)
