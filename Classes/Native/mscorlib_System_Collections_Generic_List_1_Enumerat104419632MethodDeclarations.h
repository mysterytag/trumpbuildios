﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera4014815677MethodDeclarations.h"

// System.Void System.Collections.Generic.List`1/Enumerator<SubstanceBase`2/Intrusion<System.Single,System.Func`1<System.Single>>>::.ctor(System.Collections.Generic.List`1<T>)
#define Enumerator__ctor_m1996970768(__this, ___l0, method) ((  void (*) (Enumerator_t104419632 *, List_1_t2018636640 *, const MethodInfo*))Enumerator__ctor_m1029849669_gshared)(__this, ___l0, method)
// System.Void System.Collections.Generic.List`1/Enumerator<SubstanceBase`2/Intrusion<System.Single,System.Func`1<System.Single>>>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m1029640002(__this, method) ((  void (*) (Enumerator_t104419632 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m771996397_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1/Enumerator<SubstanceBase`2/Intrusion<System.Single,System.Func`1<System.Single>>>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m2646792686(__this, method) ((  Il2CppObject * (*) (Enumerator_t104419632 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m3561903705_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<SubstanceBase`2/Intrusion<System.Single,System.Func`1<System.Single>>>::Dispose()
#define Enumerator_Dispose_m599764213(__this, method) ((  void (*) (Enumerator_t104419632 *, const MethodInfo*))Enumerator_Dispose_m2904289642_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<SubstanceBase`2/Intrusion<System.Single,System.Func`1<System.Single>>>::VerifyState()
#define Enumerator_VerifyState_m448391598(__this, method) ((  void (*) (Enumerator_t104419632 *, const MethodInfo*))Enumerator_VerifyState_m1522854819_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<SubstanceBase`2/Intrusion<System.Single,System.Func`1<System.Single>>>::MoveNext()
#define Enumerator_MoveNext_m2958898917(__this, method) ((  bool (*) (Enumerator_t104419632 *, const MethodInfo*))Enumerator_MoveNext_m302222300_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<SubstanceBase`2/Intrusion<System.Single,System.Func`1<System.Single>>>::get_Current()
#define Enumerator_get_Current_m616750859(__this, method) ((  Intrusion_t1221677671 * (*) (Enumerator_t104419632 *, const MethodInfo*))Enumerator_get_Current_m1113061044_gshared)(__this, method)
