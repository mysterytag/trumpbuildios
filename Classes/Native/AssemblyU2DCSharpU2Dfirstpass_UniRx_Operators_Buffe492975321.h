﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UniRx.Operators.BufferObservable`1<System.Object>
struct BufferObservable_1_t574294898;
// System.Collections.Generic.List`1<System.Object>
struct List_1_t1634065389;

#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Oper3354260463.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Operators.BufferObservable`1/Buffer<System.Object>
struct  Buffer_t492975321  : public OperatorObserverBase_2_t3354260463
{
public:
	// UniRx.Operators.BufferObservable`1<T> UniRx.Operators.BufferObservable`1/Buffer::parent
	BufferObservable_1_t574294898 * ___parent_2;
	// System.Collections.Generic.List`1<T> UniRx.Operators.BufferObservable`1/Buffer::list
	List_1_t1634065389 * ___list_3;

public:
	inline static int32_t get_offset_of_parent_2() { return static_cast<int32_t>(offsetof(Buffer_t492975321, ___parent_2)); }
	inline BufferObservable_1_t574294898 * get_parent_2() const { return ___parent_2; }
	inline BufferObservable_1_t574294898 ** get_address_of_parent_2() { return &___parent_2; }
	inline void set_parent_2(BufferObservable_1_t574294898 * value)
	{
		___parent_2 = value;
		Il2CppCodeGenWriteBarrier(&___parent_2, value);
	}

	inline static int32_t get_offset_of_list_3() { return static_cast<int32_t>(offsetof(Buffer_t492975321, ___list_3)); }
	inline List_1_t1634065389 * get_list_3() const { return ___list_3; }
	inline List_1_t1634065389 ** get_address_of_list_3() { return &___list_3; }
	inline void set_list_3(List_1_t1634065389 * value)
	{
		___list_3 = value;
		Il2CppCodeGenWriteBarrier(&___list_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
