﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.TakeLastObservable`1/TakeLast<System.Object>
struct TakeLast_t798930510;
// UniRx.Operators.TakeLastObservable`1<System.Object>
struct TakeLastObservable_1_t3265267047;
// UniRx.IObserver`1<System.Object>
struct IObserver_1_t3049105323;
// System.IDisposable
struct IDisposable_t1628921374;
// System.Object
struct Il2CppObject;
// System.Exception
struct Exception_t1967233988;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_Exception1967233988.h"

// System.Void UniRx.Operators.TakeLastObservable`1/TakeLast<System.Object>::.ctor(UniRx.Operators.TakeLastObservable`1<T>,UniRx.IObserver`1<T>,System.IDisposable)
extern "C"  void TakeLast__ctor_m3937985633_gshared (TakeLast_t798930510 * __this, TakeLastObservable_1_t3265267047 * ___parent0, Il2CppObject* ___observer1, Il2CppObject * ___cancel2, const MethodInfo* method);
#define TakeLast__ctor_m3937985633(__this, ___parent0, ___observer1, ___cancel2, method) ((  void (*) (TakeLast_t798930510 *, TakeLastObservable_1_t3265267047 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))TakeLast__ctor_m3937985633_gshared)(__this, ___parent0, ___observer1, ___cancel2, method)
// System.IDisposable UniRx.Operators.TakeLastObservable`1/TakeLast<System.Object>::Run()
extern "C"  Il2CppObject * TakeLast_Run_m3420746597_gshared (TakeLast_t798930510 * __this, const MethodInfo* method);
#define TakeLast_Run_m3420746597(__this, method) ((  Il2CppObject * (*) (TakeLast_t798930510 *, const MethodInfo*))TakeLast_Run_m3420746597_gshared)(__this, method)
// System.Void UniRx.Operators.TakeLastObservable`1/TakeLast<System.Object>::OnNext(T)
extern "C"  void TakeLast_OnNext_m2026977151_gshared (TakeLast_t798930510 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define TakeLast_OnNext_m2026977151(__this, ___value0, method) ((  void (*) (TakeLast_t798930510 *, Il2CppObject *, const MethodInfo*))TakeLast_OnNext_m2026977151_gshared)(__this, ___value0, method)
// System.Void UniRx.Operators.TakeLastObservable`1/TakeLast<System.Object>::OnError(System.Exception)
extern "C"  void TakeLast_OnError_m621573262_gshared (TakeLast_t798930510 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method);
#define TakeLast_OnError_m621573262(__this, ___error0, method) ((  void (*) (TakeLast_t798930510 *, Exception_t1967233988 *, const MethodInfo*))TakeLast_OnError_m621573262_gshared)(__this, ___error0, method)
// System.Void UniRx.Operators.TakeLastObservable`1/TakeLast<System.Object>::OnCompleted()
extern "C"  void TakeLast_OnCompleted_m191565025_gshared (TakeLast_t798930510 * __this, const MethodInfo* method);
#define TakeLast_OnCompleted_m191565025(__this, method) ((  void (*) (TakeLast_t798930510 *, const MethodInfo*))TakeLast_OnCompleted_m191565025_gshared)(__this, method)
