﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.DematerializeObservable`1<System.Object>
struct DematerializeObservable_1_t1297640382;
// UniRx.IObservable`1<UniRx.Notification`1<System.Object>>
struct IObservable_1_t4092122035;
// System.IDisposable
struct IDisposable_t1628921374;
// UniRx.IObserver`1<System.Object>
struct IObserver_1_t3049105323;

#include "codegen/il2cpp-codegen.h"

// System.Void UniRx.Operators.DematerializeObservable`1<System.Object>::.ctor(UniRx.IObservable`1<UniRx.Notification`1<T>>)
extern "C"  void DematerializeObservable_1__ctor_m2318627865_gshared (DematerializeObservable_1_t1297640382 * __this, Il2CppObject* ___source0, const MethodInfo* method);
#define DematerializeObservable_1__ctor_m2318627865(__this, ___source0, method) ((  void (*) (DematerializeObservable_1_t1297640382 *, Il2CppObject*, const MethodInfo*))DematerializeObservable_1__ctor_m2318627865_gshared)(__this, ___source0, method)
// System.IDisposable UniRx.Operators.DematerializeObservable`1<System.Object>::SubscribeCore(UniRx.IObserver`1<T>,System.IDisposable)
extern "C"  Il2CppObject * DematerializeObservable_1_SubscribeCore_m2578143202_gshared (DematerializeObservable_1_t1297640382 * __this, Il2CppObject* ___observer0, Il2CppObject * ___cancel1, const MethodInfo* method);
#define DematerializeObservable_1_SubscribeCore_m2578143202(__this, ___observer0, ___cancel1, method) ((  Il2CppObject * (*) (DematerializeObservable_1_t1297640382 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))DematerializeObservable_1_SubscribeCore_m2578143202_gshared)(__this, ___observer0, ___cancel1, method)
