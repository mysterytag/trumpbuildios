﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.PlayFabClientAPI/GetLeaderboardAroundCharacterResponseCallback
struct GetLeaderboardAroundCharacterResponseCallback_t2122453051;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;
// PlayFab.ClientModels.GetLeaderboardAroundCharacterRequest
struct GetLeaderboardAroundCharacterRequest_t1108200042;
// PlayFab.ClientModels.GetLeaderboardAroundCharacterResult
struct GetLeaderboardAroundCharacterResult_t2426200834;
// PlayFab.PlayFabError
struct PlayFabError_t750598646;
// System.IAsyncResult
struct IAsyncResult_t537683269;
// System.AsyncCallback
struct AsyncCallback_t1363551830;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_IntPtr676692020.h"
#include "mscorlib_System_String968488902.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_GetLeaderbo1108200042.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_GetLeaderbo2426200834.h"
#include "AssemblyU2DCSharpU2Dfirstpass_PlayFab_PlayFabError750598646.h"
#include "mscorlib_System_AsyncCallback1363551830.h"

// System.Void PlayFab.PlayFabClientAPI/GetLeaderboardAroundCharacterResponseCallback::.ctor(System.Object,System.IntPtr)
extern "C"  void GetLeaderboardAroundCharacterResponseCallback__ctor_m923545418 (GetLeaderboardAroundCharacterResponseCallback_t2122453051 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/GetLeaderboardAroundCharacterResponseCallback::Invoke(System.String,System.Int32,PlayFab.ClientModels.GetLeaderboardAroundCharacterRequest,PlayFab.ClientModels.GetLeaderboardAroundCharacterResult,PlayFab.PlayFabError,System.Object)
extern "C"  void GetLeaderboardAroundCharacterResponseCallback_Invoke_m1204724511 (GetLeaderboardAroundCharacterResponseCallback_t2122453051 * __this, String_t* ___urlPath0, int32_t ___callId1, GetLeaderboardAroundCharacterRequest_t1108200042 * ___request2, GetLeaderboardAroundCharacterResult_t2426200834 * ___result3, PlayFabError_t750598646 * ___error4, Il2CppObject * ___customData5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
extern "C" void pinvoke_delegate_wrapper_GetLeaderboardAroundCharacterResponseCallback_t2122453051(Il2CppObject* delegate, String_t* ___urlPath0, int32_t ___callId1, GetLeaderboardAroundCharacterRequest_t1108200042 * ___request2, GetLeaderboardAroundCharacterResult_t2426200834 * ___result3, PlayFabError_t750598646 * ___error4, Il2CppObject * ___customData5);
// System.IAsyncResult PlayFab.PlayFabClientAPI/GetLeaderboardAroundCharacterResponseCallback::BeginInvoke(System.String,System.Int32,PlayFab.ClientModels.GetLeaderboardAroundCharacterRequest,PlayFab.ClientModels.GetLeaderboardAroundCharacterResult,PlayFab.PlayFabError,System.Object,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * GetLeaderboardAroundCharacterResponseCallback_BeginInvoke_m613563012 (GetLeaderboardAroundCharacterResponseCallback_t2122453051 * __this, String_t* ___urlPath0, int32_t ___callId1, GetLeaderboardAroundCharacterRequest_t1108200042 * ___request2, GetLeaderboardAroundCharacterResult_t2426200834 * ___result3, PlayFabError_t750598646 * ___error4, Il2CppObject * ___customData5, AsyncCallback_t1363551830 * ___callback6, Il2CppObject * ___object7, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/GetLeaderboardAroundCharacterResponseCallback::EndInvoke(System.IAsyncResult)
extern "C"  void GetLeaderboardAroundCharacterResponseCallback_EndInvoke_m933156442 (GetLeaderboardAroundCharacterResponseCallback_t2122453051 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
