﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// ZergRush.ImmutableList`1<System.Action`1<System.Double>>
struct ImmutableList_1_t186353501;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Reactor`1/<TransactionUnpackReact>c__AnonStorey141<System.Double>
struct  U3CTransactionUnpackReactU3Ec__AnonStorey141_t2862459925  : public Il2CppObject
{
public:
	// ZergRush.ImmutableList`1<System.Action`1<T>> Reactor`1/<TransactionUnpackReact>c__AnonStorey141::list
	ImmutableList_1_t186353501 * ___list_0;
	// T Reactor`1/<TransactionUnpackReact>c__AnonStorey141::t
	double ___t_1;

public:
	inline static int32_t get_offset_of_list_0() { return static_cast<int32_t>(offsetof(U3CTransactionUnpackReactU3Ec__AnonStorey141_t2862459925, ___list_0)); }
	inline ImmutableList_1_t186353501 * get_list_0() const { return ___list_0; }
	inline ImmutableList_1_t186353501 ** get_address_of_list_0() { return &___list_0; }
	inline void set_list_0(ImmutableList_1_t186353501 * value)
	{
		___list_0 = value;
		Il2CppCodeGenWriteBarrier(&___list_0, value);
	}

	inline static int32_t get_offset_of_t_1() { return static_cast<int32_t>(offsetof(U3CTransactionUnpackReactU3Ec__AnonStorey141_t2862459925, ___t_1)); }
	inline double get_t_1() const { return ___t_1; }
	inline double* get_address_of_t_1() { return &___t_1; }
	inline void set_t_1(double value)
	{
		___t_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
