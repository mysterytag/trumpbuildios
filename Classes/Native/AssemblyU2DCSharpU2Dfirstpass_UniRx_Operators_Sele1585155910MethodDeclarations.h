﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.SelectManyObservable`3/SelectManyObserverWithIndex<System.Object,System.Int64,System.Object>
struct SelectManyObserverWithIndex_t1585155910;
// UniRx.Operators.SelectManyObservable`3<System.Object,System.Int64,System.Object>
struct SelectManyObservable_3_t744723289;
// UniRx.IObserver`1<System.Object>
struct IObserver_1_t3049105323;
// System.IDisposable
struct IDisposable_t1628921374;
// System.Object
struct Il2CppObject;
// System.Exception
struct Exception_t1967233988;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_Exception1967233988.h"

// System.Void UniRx.Operators.SelectManyObservable`3/SelectManyObserverWithIndex<System.Object,System.Int64,System.Object>::.ctor(UniRx.Operators.SelectManyObservable`3<TSource,TCollection,TResult>,UniRx.IObserver`1<TResult>,System.IDisposable)
extern "C"  void SelectManyObserverWithIndex__ctor_m2689777562_gshared (SelectManyObserverWithIndex_t1585155910 * __this, SelectManyObservable_3_t744723289 * ___parent0, Il2CppObject* ___observer1, Il2CppObject * ___cancel2, const MethodInfo* method);
#define SelectManyObserverWithIndex__ctor_m2689777562(__this, ___parent0, ___observer1, ___cancel2, method) ((  void (*) (SelectManyObserverWithIndex_t1585155910 *, SelectManyObservable_3_t744723289 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))SelectManyObserverWithIndex__ctor_m2689777562_gshared)(__this, ___parent0, ___observer1, ___cancel2, method)
// System.IDisposable UniRx.Operators.SelectManyObservable`3/SelectManyObserverWithIndex<System.Object,System.Int64,System.Object>::Run()
extern "C"  Il2CppObject * SelectManyObserverWithIndex_Run_m1736450325_gshared (SelectManyObserverWithIndex_t1585155910 * __this, const MethodInfo* method);
#define SelectManyObserverWithIndex_Run_m1736450325(__this, method) ((  Il2CppObject * (*) (SelectManyObserverWithIndex_t1585155910 *, const MethodInfo*))SelectManyObserverWithIndex_Run_m1736450325_gshared)(__this, method)
// System.Void UniRx.Operators.SelectManyObservable`3/SelectManyObserverWithIndex<System.Object,System.Int64,System.Object>::OnNext(TSource)
extern "C"  void SelectManyObserverWithIndex_OnNext_m999498196_gshared (SelectManyObserverWithIndex_t1585155910 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define SelectManyObserverWithIndex_OnNext_m999498196(__this, ___value0, method) ((  void (*) (SelectManyObserverWithIndex_t1585155910 *, Il2CppObject *, const MethodInfo*))SelectManyObserverWithIndex_OnNext_m999498196_gshared)(__this, ___value0, method)
// System.Void UniRx.Operators.SelectManyObservable`3/SelectManyObserverWithIndex<System.Object,System.Int64,System.Object>::OnError(System.Exception)
extern "C"  void SelectManyObserverWithIndex_OnError_m83535934_gshared (SelectManyObserverWithIndex_t1585155910 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method);
#define SelectManyObserverWithIndex_OnError_m83535934(__this, ___error0, method) ((  void (*) (SelectManyObserverWithIndex_t1585155910 *, Exception_t1967233988 *, const MethodInfo*))SelectManyObserverWithIndex_OnError_m83535934_gshared)(__this, ___error0, method)
// System.Void UniRx.Operators.SelectManyObservable`3/SelectManyObserverWithIndex<System.Object,System.Int64,System.Object>::OnCompleted()
extern "C"  void SelectManyObserverWithIndex_OnCompleted_m1975863441_gshared (SelectManyObserverWithIndex_t1585155910 * __this, const MethodInfo* method);
#define SelectManyObserverWithIndex_OnCompleted_m1975863441(__this, method) ((  void (*) (SelectManyObserverWithIndex_t1585155910 *, const MethodInfo*))SelectManyObserverWithIndex_OnCompleted_m1975863441_gshared)(__this, method)
