﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// FriendInfo
struct FriendInfo_t236470412;

#include "codegen/il2cpp-codegen.h"

// System.Void FriendInfo::.ctor()
extern "C"  void FriendInfo__ctor_m3710203279 (FriendInfo_t236470412 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
