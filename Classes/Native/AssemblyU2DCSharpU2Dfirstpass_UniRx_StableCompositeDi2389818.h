﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Generic.List`1<System.IDisposable> modreq(System.Runtime.CompilerServices.IsVolatile)
struct List_1_t2425880343;

#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_StableComposit3433635614.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.StableCompositeDisposable/NAry
struct  NAry_t2389818  : public StableCompositeDisposable_t3433635614
{
public:
	// System.Int32 UniRx.StableCompositeDisposable/NAry::disposedCallCount
	int32_t ___disposedCallCount_0;
	// System.Collections.Generic.List`1<System.IDisposable> modreq(System.Runtime.CompilerServices.IsVolatile) UniRx.StableCompositeDisposable/NAry::_disposables
	List_1_t2425880343 * ____disposables_1;

public:
	inline static int32_t get_offset_of_disposedCallCount_0() { return static_cast<int32_t>(offsetof(NAry_t2389818, ___disposedCallCount_0)); }
	inline int32_t get_disposedCallCount_0() const { return ___disposedCallCount_0; }
	inline int32_t* get_address_of_disposedCallCount_0() { return &___disposedCallCount_0; }
	inline void set_disposedCallCount_0(int32_t value)
	{
		___disposedCallCount_0 = value;
	}

	inline static int32_t get_offset_of__disposables_1() { return static_cast<int32_t>(offsetof(NAry_t2389818, ____disposables_1)); }
	inline List_1_t2425880343 * get__disposables_1() const { return ____disposables_1; }
	inline List_1_t2425880343 ** get_address_of__disposables_1() { return &____disposables_1; }
	inline void set__disposables_1(List_1_t2425880343 * value)
	{
		____disposables_1 = value;
		Il2CppCodeGenWriteBarrier(&____disposables_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
