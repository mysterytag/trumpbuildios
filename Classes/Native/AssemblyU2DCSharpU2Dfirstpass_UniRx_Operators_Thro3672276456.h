﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Exception
struct Exception_t1967233988;
// UniRx.IScheduler
struct IScheduler_t2938318244;

#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Oper4196218687.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Operators.ThrowObservable`1<System.Object>
struct  ThrowObservable_1_t3672276456  : public OperatorObservableBase_1_t4196218687
{
public:
	// System.Exception UniRx.Operators.ThrowObservable`1::error
	Exception_t1967233988 * ___error_1;
	// UniRx.IScheduler UniRx.Operators.ThrowObservable`1::scheduler
	Il2CppObject * ___scheduler_2;

public:
	inline static int32_t get_offset_of_error_1() { return static_cast<int32_t>(offsetof(ThrowObservable_1_t3672276456, ___error_1)); }
	inline Exception_t1967233988 * get_error_1() const { return ___error_1; }
	inline Exception_t1967233988 ** get_address_of_error_1() { return &___error_1; }
	inline void set_error_1(Exception_t1967233988 * value)
	{
		___error_1 = value;
		Il2CppCodeGenWriteBarrier(&___error_1, value);
	}

	inline static int32_t get_offset_of_scheduler_2() { return static_cast<int32_t>(offsetof(ThrowObservable_1_t3672276456, ___scheduler_2)); }
	inline Il2CppObject * get_scheduler_2() const { return ___scheduler_2; }
	inline Il2CppObject ** get_address_of_scheduler_2() { return &___scheduler_2; }
	inline void set_scheduler_2(Il2CppObject * value)
	{
		___scheduler_2 = value;
		Il2CppCodeGenWriteBarrier(&___scheduler_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
