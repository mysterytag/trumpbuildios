﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Generic.LinkedList`1<Zenject.TaskUpdater`1/TaskInfo<Zenject.ILateTickable>>
struct LinkedList_1_t1138375077;
// System.Collections.Generic.List`1<Zenject.TaskUpdater`1/TaskInfo<Zenject.ILateTickable>>
struct List_1_t195204500;
// System.Action`1<Zenject.ILateTickable>
struct Action_1_t3614263548;
// System.Func`2<Zenject.TaskUpdater`1/TaskInfo<Zenject.ILateTickable>,Zenject.ILateTickable>
struct Func_2_t3162558524;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.TaskUpdater`1<Zenject.ILateTickable>
struct  TaskUpdater_1_t39732794  : public Il2CppObject
{
public:
	// System.Collections.Generic.LinkedList`1<Zenject.TaskUpdater`1/TaskInfo<TTask>> Zenject.TaskUpdater`1::_tasks
	LinkedList_1_t1138375077 * ____tasks_0;
	// System.Collections.Generic.List`1<Zenject.TaskUpdater`1/TaskInfo<TTask>> Zenject.TaskUpdater`1::_queuedTasks
	List_1_t195204500 * ____queuedTasks_1;
	// System.Action`1<TTask> Zenject.TaskUpdater`1::_updateFunc
	Action_1_t3614263548 * ____updateFunc_2;

public:
	inline static int32_t get_offset_of__tasks_0() { return static_cast<int32_t>(offsetof(TaskUpdater_1_t39732794, ____tasks_0)); }
	inline LinkedList_1_t1138375077 * get__tasks_0() const { return ____tasks_0; }
	inline LinkedList_1_t1138375077 ** get_address_of__tasks_0() { return &____tasks_0; }
	inline void set__tasks_0(LinkedList_1_t1138375077 * value)
	{
		____tasks_0 = value;
		Il2CppCodeGenWriteBarrier(&____tasks_0, value);
	}

	inline static int32_t get_offset_of__queuedTasks_1() { return static_cast<int32_t>(offsetof(TaskUpdater_1_t39732794, ____queuedTasks_1)); }
	inline List_1_t195204500 * get__queuedTasks_1() const { return ____queuedTasks_1; }
	inline List_1_t195204500 ** get_address_of__queuedTasks_1() { return &____queuedTasks_1; }
	inline void set__queuedTasks_1(List_1_t195204500 * value)
	{
		____queuedTasks_1 = value;
		Il2CppCodeGenWriteBarrier(&____queuedTasks_1, value);
	}

	inline static int32_t get_offset_of__updateFunc_2() { return static_cast<int32_t>(offsetof(TaskUpdater_1_t39732794, ____updateFunc_2)); }
	inline Action_1_t3614263548 * get__updateFunc_2() const { return ____updateFunc_2; }
	inline Action_1_t3614263548 ** get_address_of__updateFunc_2() { return &____updateFunc_2; }
	inline void set__updateFunc_2(Action_1_t3614263548 * value)
	{
		____updateFunc_2 = value;
		Il2CppCodeGenWriteBarrier(&____updateFunc_2, value);
	}
};

struct TaskUpdater_1_t39732794_StaticFields
{
public:
	// System.Func`2<Zenject.TaskUpdater`1/TaskInfo<TTask>,TTask> Zenject.TaskUpdater`1::<>f__am$cache3
	Func_2_t3162558524 * ___U3CU3Ef__amU24cache3_3;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache3_3() { return static_cast<int32_t>(offsetof(TaskUpdater_1_t39732794_StaticFields, ___U3CU3Ef__amU24cache3_3)); }
	inline Func_2_t3162558524 * get_U3CU3Ef__amU24cache3_3() const { return ___U3CU3Ef__amU24cache3_3; }
	inline Func_2_t3162558524 ** get_address_of_U3CU3Ef__amU24cache3_3() { return &___U3CU3Ef__amU24cache3_3; }
	inline void set_U3CU3Ef__amU24cache3_3(Func_2_t3162558524 * value)
	{
		___U3CU3Ef__amU24cache3_3 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache3_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
