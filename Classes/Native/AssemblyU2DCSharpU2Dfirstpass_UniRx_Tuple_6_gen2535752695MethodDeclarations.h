﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Object
struct Il2CppObject;
// System.Collections.IComparer
struct IComparer_t2207526184;
// System.Collections.IEqualityComparer
struct IEqualityComparer_t1661793090;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Tuple_6_gen2535752695.h"
#include "mscorlib_System_Object837106420.h"

// System.Void UniRx.Tuple`6<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>::.ctor(T1,T2,T3,T4,T5,T6)
extern "C"  void Tuple_6__ctor_m1485614320_gshared (Tuple_6_t2535752695 * __this, Il2CppObject * ___item10, Il2CppObject * ___item21, Il2CppObject * ___item32, Il2CppObject * ___item43, Il2CppObject * ___item54, Il2CppObject * ___item65, const MethodInfo* method);
#define Tuple_6__ctor_m1485614320(__this, ___item10, ___item21, ___item32, ___item43, ___item54, ___item65, method) ((  void (*) (Tuple_6_t2535752695 *, Il2CppObject *, Il2CppObject *, Il2CppObject *, Il2CppObject *, Il2CppObject *, Il2CppObject *, const MethodInfo*))Tuple_6__ctor_m1485614320_gshared)(__this, ___item10, ___item21, ___item32, ___item43, ___item54, ___item65, method)
// System.Int32 UniRx.Tuple`6<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>::System.IComparable.CompareTo(System.Object)
extern "C"  int32_t Tuple_6_System_IComparable_CompareTo_m786548503_gshared (Tuple_6_t2535752695 * __this, Il2CppObject * ___obj0, const MethodInfo* method);
#define Tuple_6_System_IComparable_CompareTo_m786548503(__this, ___obj0, method) ((  int32_t (*) (Tuple_6_t2535752695 *, Il2CppObject *, const MethodInfo*))Tuple_6_System_IComparable_CompareTo_m786548503_gshared)(__this, ___obj0, method)
// System.Int32 UniRx.Tuple`6<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>::UniRx.IStructuralComparable.CompareTo(System.Object,System.Collections.IComparer)
extern "C"  int32_t Tuple_6_UniRx_IStructuralComparable_CompareTo_m1756214953_gshared (Tuple_6_t2535752695 * __this, Il2CppObject * ___other0, Il2CppObject * ___comparer1, const MethodInfo* method);
#define Tuple_6_UniRx_IStructuralComparable_CompareTo_m1756214953(__this, ___other0, ___comparer1, method) ((  int32_t (*) (Tuple_6_t2535752695 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))Tuple_6_UniRx_IStructuralComparable_CompareTo_m1756214953_gshared)(__this, ___other0, ___comparer1, method)
// System.Boolean UniRx.Tuple`6<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>::UniRx.IStructuralEquatable.Equals(System.Object,System.Collections.IEqualityComparer)
extern "C"  bool Tuple_6_UniRx_IStructuralEquatable_Equals_m3316726164_gshared (Tuple_6_t2535752695 * __this, Il2CppObject * ___other0, Il2CppObject * ___comparer1, const MethodInfo* method);
#define Tuple_6_UniRx_IStructuralEquatable_Equals_m3316726164(__this, ___other0, ___comparer1, method) ((  bool (*) (Tuple_6_t2535752695 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))Tuple_6_UniRx_IStructuralEquatable_Equals_m3316726164_gshared)(__this, ___other0, ___comparer1, method)
// System.Int32 UniRx.Tuple`6<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>::UniRx.IStructuralEquatable.GetHashCode(System.Collections.IEqualityComparer)
extern "C"  int32_t Tuple_6_UniRx_IStructuralEquatable_GetHashCode_m2348755738_gshared (Tuple_6_t2535752695 * __this, Il2CppObject * ___comparer0, const MethodInfo* method);
#define Tuple_6_UniRx_IStructuralEquatable_GetHashCode_m2348755738(__this, ___comparer0, method) ((  int32_t (*) (Tuple_6_t2535752695 *, Il2CppObject *, const MethodInfo*))Tuple_6_UniRx_IStructuralEquatable_GetHashCode_m2348755738_gshared)(__this, ___comparer0, method)
// System.String UniRx.Tuple`6<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>::UniRx.ITuple.ToString()
extern "C"  String_t* Tuple_6_UniRx_ITuple_ToString_m4039255313_gshared (Tuple_6_t2535752695 * __this, const MethodInfo* method);
#define Tuple_6_UniRx_ITuple_ToString_m4039255313(__this, method) ((  String_t* (*) (Tuple_6_t2535752695 *, const MethodInfo*))Tuple_6_UniRx_ITuple_ToString_m4039255313_gshared)(__this, method)
// T1 UniRx.Tuple`6<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>::get_Item1()
extern "C"  Il2CppObject * Tuple_6_get_Item1_m3857120062_gshared (Tuple_6_t2535752695 * __this, const MethodInfo* method);
#define Tuple_6_get_Item1_m3857120062(__this, method) ((  Il2CppObject * (*) (Tuple_6_t2535752695 *, const MethodInfo*))Tuple_6_get_Item1_m3857120062_gshared)(__this, method)
// T2 UniRx.Tuple`6<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>::get_Item2()
extern "C"  Il2CppObject * Tuple_6_get_Item2_m2573209856_gshared (Tuple_6_t2535752695 * __this, const MethodInfo* method);
#define Tuple_6_get_Item2_m2573209856(__this, method) ((  Il2CppObject * (*) (Tuple_6_t2535752695 *, const MethodInfo*))Tuple_6_get_Item2_m2573209856_gshared)(__this, method)
// T3 UniRx.Tuple`6<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>::get_Item3()
extern "C"  Il2CppObject * Tuple_6_get_Item3_m1289299650_gshared (Tuple_6_t2535752695 * __this, const MethodInfo* method);
#define Tuple_6_get_Item3_m1289299650(__this, method) ((  Il2CppObject * (*) (Tuple_6_t2535752695 *, const MethodInfo*))Tuple_6_get_Item3_m1289299650_gshared)(__this, method)
// T4 UniRx.Tuple`6<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>::get_Item4()
extern "C"  Il2CppObject * Tuple_6_get_Item4_m5389444_gshared (Tuple_6_t2535752695 * __this, const MethodInfo* method);
#define Tuple_6_get_Item4_m5389444(__this, method) ((  Il2CppObject * (*) (Tuple_6_t2535752695 *, const MethodInfo*))Tuple_6_get_Item4_m5389444_gshared)(__this, method)
// T5 UniRx.Tuple`6<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>::get_Item5()
extern "C"  Il2CppObject * Tuple_6_get_Item5_m3016446534_gshared (Tuple_6_t2535752695 * __this, const MethodInfo* method);
#define Tuple_6_get_Item5_m3016446534(__this, method) ((  Il2CppObject * (*) (Tuple_6_t2535752695 *, const MethodInfo*))Tuple_6_get_Item5_m3016446534_gshared)(__this, method)
// T6 UniRx.Tuple`6<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>::get_Item6()
extern "C"  Il2CppObject * Tuple_6_get_Item6_m1732536328_gshared (Tuple_6_t2535752695 * __this, const MethodInfo* method);
#define Tuple_6_get_Item6_m1732536328(__this, method) ((  Il2CppObject * (*) (Tuple_6_t2535752695 *, const MethodInfo*))Tuple_6_get_Item6_m1732536328_gshared)(__this, method)
// System.Boolean UniRx.Tuple`6<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>::Equals(System.Object)
extern "C"  bool Tuple_6_Equals_m2223432776_gshared (Tuple_6_t2535752695 * __this, Il2CppObject * ___obj0, const MethodInfo* method);
#define Tuple_6_Equals_m2223432776(__this, ___obj0, method) ((  bool (*) (Tuple_6_t2535752695 *, Il2CppObject *, const MethodInfo*))Tuple_6_Equals_m2223432776_gshared)(__this, ___obj0, method)
// System.Int32 UniRx.Tuple`6<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>::GetHashCode()
extern "C"  int32_t Tuple_6_GetHashCode_m3301833964_gshared (Tuple_6_t2535752695 * __this, const MethodInfo* method);
#define Tuple_6_GetHashCode_m3301833964(__this, method) ((  int32_t (*) (Tuple_6_t2535752695 *, const MethodInfo*))Tuple_6_GetHashCode_m3301833964_gshared)(__this, method)
// System.String UniRx.Tuple`6<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>::ToString()
extern "C"  String_t* Tuple_6_ToString_m4177972864_gshared (Tuple_6_t2535752695 * __this, const MethodInfo* method);
#define Tuple_6_ToString_m4177972864(__this, method) ((  String_t* (*) (Tuple_6_t2535752695 *, const MethodInfo*))Tuple_6_ToString_m4177972864_gshared)(__this, method)
// System.Boolean UniRx.Tuple`6<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>::Equals(UniRx.Tuple`6<T1,T2,T3,T4,T5,T6>)
extern "C"  bool Tuple_6_Equals_m2758201191_gshared (Tuple_6_t2535752695 * __this, Tuple_6_t2535752695  ___other0, const MethodInfo* method);
#define Tuple_6_Equals_m2758201191(__this, ___other0, method) ((  bool (*) (Tuple_6_t2535752695 *, Tuple_6_t2535752695 , const MethodInfo*))Tuple_6_Equals_m2758201191_gshared)(__this, ___other0, method)
