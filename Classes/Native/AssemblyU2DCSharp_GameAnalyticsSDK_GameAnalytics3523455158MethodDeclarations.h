﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GameAnalyticsSDK.GameAnalytics
struct GameAnalytics_t3523455158;
// GameAnalyticsSDK.Settings
struct Settings_t1104664933;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_GameAnalyticsSDK_Settings1104664933.h"
#include "mscorlib_System_String968488902.h"
#include "AssemblyU2DCSharp_GameAnalyticsSDK_GA_Progression_2058507475.h"
#include "AssemblyU2DCSharp_GameAnalyticsSDK_GA_Resource_GAR3683864752.h"
#include "AssemblyU2DCSharp_GameAnalyticsSDK_GA_Error_GAErro2545392235.h"
#include "AssemblyU2DCSharp_GameAnalyticsSDK_GA_Setup_GAGend3167968315.h"
#include "AssemblyU2DCSharp_GameAnalyticsSDK_GAPlatform3251704975.h"

// System.Void GameAnalyticsSDK.GameAnalytics::.ctor()
extern "C"  void GameAnalytics__ctor_m3124141679 (GameAnalytics_t3523455158 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// GameAnalyticsSDK.Settings GameAnalyticsSDK.GameAnalytics::get_SettingsGA()
extern "C"  Settings_t1104664933 * GameAnalytics_get_SettingsGA_m1755024323 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameAnalyticsSDK.GameAnalytics::set_SettingsGA(GameAnalyticsSDK.Settings)
extern "C"  void GameAnalytics_set_SettingsGA_m1179814296 (Il2CppObject * __this /* static, unused */, Settings_t1104664933 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameAnalyticsSDK.GameAnalytics::Awake()
extern "C"  void GameAnalytics_Awake_m3361746898 (GameAnalytics_t3523455158 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameAnalyticsSDK.GameAnalytics::OnDestroy()
extern "C"  void GameAnalytics_OnDestroy_m4154827752 (GameAnalytics_t3523455158 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameAnalyticsSDK.GameAnalytics::OnApplicationPause(System.Boolean)
extern "C"  void GameAnalytics_OnApplicationPause_m4229151537 (GameAnalytics_t3523455158 * __this, bool ___pauseStatus0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameAnalyticsSDK.GameAnalytics::OnApplicationQuit()
extern "C"  void GameAnalytics_OnApplicationQuit_m1326570861 (GameAnalytics_t3523455158 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameAnalyticsSDK.GameAnalytics::InitAPI()
extern "C"  void GameAnalytics_InitAPI_m3773906167 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameAnalyticsSDK.GameAnalytics::Initialize()
extern "C"  void GameAnalytics_Initialize_m3339871621 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameAnalyticsSDK.GameAnalytics::NewBusinessEvent(System.String,System.Int32,System.String,System.String,System.String)
extern "C"  void GameAnalytics_NewBusinessEvent_m3953005176 (Il2CppObject * __this /* static, unused */, String_t* ___currency0, int32_t ___amount1, String_t* ___itemType2, String_t* ___itemId3, String_t* ___cartType4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameAnalyticsSDK.GameAnalytics::NewBusinessEventIOS(System.String,System.Int32,System.String,System.String,System.String,System.String)
extern "C"  void GameAnalytics_NewBusinessEventIOS_m2895205253 (Il2CppObject * __this /* static, unused */, String_t* ___currency0, int32_t ___amount1, String_t* ___itemType2, String_t* ___itemId3, String_t* ___cartType4, String_t* ___receipt5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameAnalyticsSDK.GameAnalytics::NewBusinessEventIOSAutoFetchReceipt(System.String,System.Int32,System.String,System.String,System.String)
extern "C"  void GameAnalytics_NewBusinessEventIOSAutoFetchReceipt_m2446135510 (Il2CppObject * __this /* static, unused */, String_t* ___currency0, int32_t ___amount1, String_t* ___itemType2, String_t* ___itemId3, String_t* ___cartType4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameAnalyticsSDK.GameAnalytics::NewDesignEvent(System.String)
extern "C"  void GameAnalytics_NewDesignEvent_m2798174193 (Il2CppObject * __this /* static, unused */, String_t* ___eventName0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameAnalyticsSDK.GameAnalytics::NewDesignEvent(System.String,System.Single)
extern "C"  void GameAnalytics_NewDesignEvent_m1914179862 (Il2CppObject * __this /* static, unused */, String_t* ___eventName0, float ___eventValue1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameAnalyticsSDK.GameAnalytics::NewProgressionEvent(GameAnalyticsSDK.GA_Progression/GAProgressionStatus,System.String)
extern "C"  void GameAnalytics_NewProgressionEvent_m217347296 (Il2CppObject * __this /* static, unused */, int32_t ___progressionStatus0, String_t* ___progression011, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameAnalyticsSDK.GameAnalytics::NewProgressionEvent(GameAnalyticsSDK.GA_Progression/GAProgressionStatus,System.String,System.String)
extern "C"  void GameAnalytics_NewProgressionEvent_m1854903580 (Il2CppObject * __this /* static, unused */, int32_t ___progressionStatus0, String_t* ___progression011, String_t* ___progression022, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameAnalyticsSDK.GameAnalytics::NewProgressionEvent(GameAnalyticsSDK.GA_Progression/GAProgressionStatus,System.String,System.String,System.String)
extern "C"  void GameAnalytics_NewProgressionEvent_m3785333848 (Il2CppObject * __this /* static, unused */, int32_t ___progressionStatus0, String_t* ___progression011, String_t* ___progression022, String_t* ___progression033, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameAnalyticsSDK.GameAnalytics::NewProgressionEvent(GameAnalyticsSDK.GA_Progression/GAProgressionStatus,System.String,System.Int32)
extern "C"  void GameAnalytics_NewProgressionEvent_m2954597367 (Il2CppObject * __this /* static, unused */, int32_t ___progressionStatus0, String_t* ___progression011, int32_t ___score2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameAnalyticsSDK.GameAnalytics::NewProgressionEvent(GameAnalyticsSDK.GA_Progression/GAProgressionStatus,System.String,System.String,System.Int32)
extern "C"  void GameAnalytics_NewProgressionEvent_m2878321979 (Il2CppObject * __this /* static, unused */, int32_t ___progressionStatus0, String_t* ___progression011, String_t* ___progression022, int32_t ___score3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameAnalyticsSDK.GameAnalytics::NewProgressionEvent(GameAnalyticsSDK.GA_Progression/GAProgressionStatus,System.String,System.String,System.String,System.Int32)
extern "C"  void GameAnalytics_NewProgressionEvent_m3003010943 (Il2CppObject * __this /* static, unused */, int32_t ___progressionStatus0, String_t* ___progression011, String_t* ___progression022, String_t* ___progression033, int32_t ___score4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameAnalyticsSDK.GameAnalytics::NewResourceEvent(GameAnalyticsSDK.GA_Resource/GAResourceFlowType,System.String,System.Single,System.String,System.String)
extern "C"  void GameAnalytics_NewResourceEvent_m4251734102 (Il2CppObject * __this /* static, unused */, int32_t ___flowType0, String_t* ___currency1, float ___amount2, String_t* ___itemType3, String_t* ___itemId4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameAnalyticsSDK.GameAnalytics::NewErrorEvent(GameAnalyticsSDK.GA_Error/GAErrorSeverity,System.String)
extern "C"  void GameAnalytics_NewErrorEvent_m3139109736 (Il2CppObject * __this /* static, unused */, int32_t ___severity0, String_t* ___message1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameAnalyticsSDK.GameAnalytics::SetFacebookId(System.String)
extern "C"  void GameAnalytics_SetFacebookId_m2091503026 (Il2CppObject * __this /* static, unused */, String_t* ___facebookId0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameAnalyticsSDK.GameAnalytics::SetGender(GameAnalyticsSDK.GA_Setup/GAGender)
extern "C"  void GameAnalytics_SetGender_m500554822 (Il2CppObject * __this /* static, unused */, int32_t ___gender0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameAnalyticsSDK.GameAnalytics::SetBirthYear(System.Int32)
extern "C"  void GameAnalytics_SetBirthYear_m1587741856 (Il2CppObject * __this /* static, unused */, int32_t ___birthYear0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameAnalyticsSDK.GameAnalytics::SetCustomDimension01(System.String)
extern "C"  void GameAnalytics_SetCustomDimension01_m803168313 (Il2CppObject * __this /* static, unused */, String_t* ___customDimension0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameAnalyticsSDK.GameAnalytics::SetCustomDimension02(System.String)
extern "C"  void GameAnalytics_SetCustomDimension02_m292634136 (Il2CppObject * __this /* static, unused */, String_t* ___customDimension0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameAnalyticsSDK.GameAnalytics::SetCustomDimension03(System.String)
extern "C"  void GameAnalytics_SetCustomDimension03_m4077067255 (Il2CppObject * __this /* static, unused */, String_t* ___customDimension0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String GameAnalyticsSDK.GameAnalytics::GetUnityVersion()
extern "C"  String_t* GameAnalytics_GetUnityVersion_m1047457955 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// GameAnalyticsSDK.GAPlatform GameAnalyticsSDK.GameAnalytics::GetPlatform()
extern "C"  int32_t GameAnalytics_GetPlatform_m1319083288 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
