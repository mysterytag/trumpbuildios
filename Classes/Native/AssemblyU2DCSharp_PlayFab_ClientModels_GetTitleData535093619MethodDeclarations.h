﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.ClientModels.GetTitleDataRequest
struct GetTitleDataRequest_t535093619;
// System.Collections.Generic.List`1<System.String>
struct List_1_t1765447871;

#include "codegen/il2cpp-codegen.h"

// System.Void PlayFab.ClientModels.GetTitleDataRequest::.ctor()
extern "C"  void GetTitleDataRequest__ctor_m4101590166 (GetTitleDataRequest_t535093619 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<System.String> PlayFab.ClientModels.GetTitleDataRequest::get_Keys()
extern "C"  List_1_t1765447871 * GetTitleDataRequest_get_Keys_m1158299868 (GetTitleDataRequest_t535093619 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.GetTitleDataRequest::set_Keys(System.Collections.Generic.List`1<System.String>)
extern "C"  void GetTitleDataRequest_set_Keys_m2587258131 (GetTitleDataRequest_t535093619 * __this, List_1_t1765447871 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
