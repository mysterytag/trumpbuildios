﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.ClientModels.LoginWithPSNRequest
struct LoginWithPSNRequest_t4060832355;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String968488902.h"
#include "mscorlib_System_Nullable_1_gen1438485399.h"
#include "mscorlib_System_Nullable_1_gen3097043249.h"

// System.Void PlayFab.ClientModels.LoginWithPSNRequest::.ctor()
extern "C"  void LoginWithPSNRequest__ctor_m3934050726 (LoginWithPSNRequest_t4060832355 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PlayFab.ClientModels.LoginWithPSNRequest::get_TitleId()
extern "C"  String_t* LoginWithPSNRequest_get_TitleId_m4223639083 (LoginWithPSNRequest_t4060832355 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.LoginWithPSNRequest::set_TitleId(System.String)
extern "C"  void LoginWithPSNRequest_set_TitleId_m3122932616 (LoginWithPSNRequest_t4060832355 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PlayFab.ClientModels.LoginWithPSNRequest::get_AuthCode()
extern "C"  String_t* LoginWithPSNRequest_get_AuthCode_m1114678783 (LoginWithPSNRequest_t4060832355 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.LoginWithPSNRequest::set_AuthCode(System.String)
extern "C"  void LoginWithPSNRequest_set_AuthCode_m141687986 (LoginWithPSNRequest_t4060832355 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PlayFab.ClientModels.LoginWithPSNRequest::get_RedirectUri()
extern "C"  String_t* LoginWithPSNRequest_get_RedirectUri_m3316677064 (LoginWithPSNRequest_t4060832355 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.LoginWithPSNRequest::set_RedirectUri(System.String)
extern "C"  void LoginWithPSNRequest_set_RedirectUri_m3742303883 (LoginWithPSNRequest_t4060832355 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Nullable`1<System.Int32> PlayFab.ClientModels.LoginWithPSNRequest::get_IssuerId()
extern "C"  Nullable_1_t1438485399  LoginWithPSNRequest_get_IssuerId_m2689213194 (LoginWithPSNRequest_t4060832355 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.LoginWithPSNRequest::set_IssuerId(System.Nullable`1<System.Int32>)
extern "C"  void LoginWithPSNRequest_set_IssuerId_m2169427423 (LoginWithPSNRequest_t4060832355 * __this, Nullable_1_t1438485399  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Nullable`1<System.Boolean> PlayFab.ClientModels.LoginWithPSNRequest::get_CreateAccount()
extern "C"  Nullable_1_t3097043249  LoginWithPSNRequest_get_CreateAccount_m3964506883 (LoginWithPSNRequest_t4060832355 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.LoginWithPSNRequest::set_CreateAccount(System.Nullable`1<System.Boolean>)
extern "C"  void LoginWithPSNRequest_set_CreateAccount_m1097606512 (LoginWithPSNRequest_t4060832355 * __this, Nullable_1_t3097043249  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
