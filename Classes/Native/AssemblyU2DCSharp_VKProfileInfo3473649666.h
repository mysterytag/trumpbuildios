﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object837106420.h"
#include "AssemblyU2DCSharp_VKProfileInfo_Gender2129321697.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// VKProfileInfo
struct  VKProfileInfo_t3473649666  : public Il2CppObject
{
public:
	// System.String VKProfileInfo::firstName
	String_t* ___firstName_0;
	// System.String VKProfileInfo::lastName
	String_t* ___lastName_1;
	// System.Int32 VKProfileInfo::age
	int32_t ___age_2;
	// VKProfileInfo/Gender VKProfileInfo::gender
	int32_t ___gender_3;
	// System.String VKProfileInfo::country
	String_t* ___country_4;
	// System.String VKProfileInfo::town
	String_t* ___town_5;
	// System.String VKProfileInfo::vkId
	String_t* ___vkId_6;

public:
	inline static int32_t get_offset_of_firstName_0() { return static_cast<int32_t>(offsetof(VKProfileInfo_t3473649666, ___firstName_0)); }
	inline String_t* get_firstName_0() const { return ___firstName_0; }
	inline String_t** get_address_of_firstName_0() { return &___firstName_0; }
	inline void set_firstName_0(String_t* value)
	{
		___firstName_0 = value;
		Il2CppCodeGenWriteBarrier(&___firstName_0, value);
	}

	inline static int32_t get_offset_of_lastName_1() { return static_cast<int32_t>(offsetof(VKProfileInfo_t3473649666, ___lastName_1)); }
	inline String_t* get_lastName_1() const { return ___lastName_1; }
	inline String_t** get_address_of_lastName_1() { return &___lastName_1; }
	inline void set_lastName_1(String_t* value)
	{
		___lastName_1 = value;
		Il2CppCodeGenWriteBarrier(&___lastName_1, value);
	}

	inline static int32_t get_offset_of_age_2() { return static_cast<int32_t>(offsetof(VKProfileInfo_t3473649666, ___age_2)); }
	inline int32_t get_age_2() const { return ___age_2; }
	inline int32_t* get_address_of_age_2() { return &___age_2; }
	inline void set_age_2(int32_t value)
	{
		___age_2 = value;
	}

	inline static int32_t get_offset_of_gender_3() { return static_cast<int32_t>(offsetof(VKProfileInfo_t3473649666, ___gender_3)); }
	inline int32_t get_gender_3() const { return ___gender_3; }
	inline int32_t* get_address_of_gender_3() { return &___gender_3; }
	inline void set_gender_3(int32_t value)
	{
		___gender_3 = value;
	}

	inline static int32_t get_offset_of_country_4() { return static_cast<int32_t>(offsetof(VKProfileInfo_t3473649666, ___country_4)); }
	inline String_t* get_country_4() const { return ___country_4; }
	inline String_t** get_address_of_country_4() { return &___country_4; }
	inline void set_country_4(String_t* value)
	{
		___country_4 = value;
		Il2CppCodeGenWriteBarrier(&___country_4, value);
	}

	inline static int32_t get_offset_of_town_5() { return static_cast<int32_t>(offsetof(VKProfileInfo_t3473649666, ___town_5)); }
	inline String_t* get_town_5() const { return ___town_5; }
	inline String_t** get_address_of_town_5() { return &___town_5; }
	inline void set_town_5(String_t* value)
	{
		___town_5 = value;
		Il2CppCodeGenWriteBarrier(&___town_5, value);
	}

	inline static int32_t get_offset_of_vkId_6() { return static_cast<int32_t>(offsetof(VKProfileInfo_t3473649666, ___vkId_6)); }
	inline String_t* get_vkId_6() const { return ___vkId_6; }
	inline String_t** get_address_of_vkId_6() { return &___vkId_6; }
	inline void set_vkId_6(String_t* value)
	{
		___vkId_6 = value;
		Il2CppCodeGenWriteBarrier(&___vkId_6, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
