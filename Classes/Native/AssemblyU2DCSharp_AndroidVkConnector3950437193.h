﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// JavaClassWrapper
struct JavaClassWrapper_t657619805;

#include "AssemblyU2DCSharp_VkConnectorBase2999232489.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AndroidVkConnector
struct  AndroidVkConnector_t3950437193  : public VkConnectorBase_t2999232489
{
public:
	// JavaClassWrapper AndroidVkConnector::javaClass
	JavaClassWrapper_t657619805 * ___javaClass_13;

public:
	inline static int32_t get_offset_of_javaClass_13() { return static_cast<int32_t>(offsetof(AndroidVkConnector_t3950437193, ___javaClass_13)); }
	inline JavaClassWrapper_t657619805 * get_javaClass_13() const { return ___javaClass_13; }
	inline JavaClassWrapper_t657619805 ** get_address_of_javaClass_13() { return &___javaClass_13; }
	inline void set_javaClass_13(JavaClassWrapper_t657619805 * value)
	{
		___javaClass_13 = value;
		Il2CppCodeGenWriteBarrier(&___javaClass_13, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
