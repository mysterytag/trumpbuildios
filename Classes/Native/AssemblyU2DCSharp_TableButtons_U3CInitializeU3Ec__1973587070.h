﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UpgradeButton
struct UpgradeButton_t1475467342;
// UpgradeCell
struct UpgradeCell_t4117746046;
// TableButtons
struct TableButtons_t1868573683;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TableButtons/<Initialize>c__AnonStorey157
struct  U3CInitializeU3Ec__AnonStorey157_t1973587070  : public Il2CppObject
{
public:
	// UpgradeButton TableButtons/<Initialize>c__AnonStorey157::button
	UpgradeButton_t1475467342 * ___button_0;
	// UpgradeCell TableButtons/<Initialize>c__AnonStorey157::cell
	UpgradeCell_t4117746046 * ___cell_1;
	// TableButtons TableButtons/<Initialize>c__AnonStorey157::<>f__this
	TableButtons_t1868573683 * ___U3CU3Ef__this_2;

public:
	inline static int32_t get_offset_of_button_0() { return static_cast<int32_t>(offsetof(U3CInitializeU3Ec__AnonStorey157_t1973587070, ___button_0)); }
	inline UpgradeButton_t1475467342 * get_button_0() const { return ___button_0; }
	inline UpgradeButton_t1475467342 ** get_address_of_button_0() { return &___button_0; }
	inline void set_button_0(UpgradeButton_t1475467342 * value)
	{
		___button_0 = value;
		Il2CppCodeGenWriteBarrier(&___button_0, value);
	}

	inline static int32_t get_offset_of_cell_1() { return static_cast<int32_t>(offsetof(U3CInitializeU3Ec__AnonStorey157_t1973587070, ___cell_1)); }
	inline UpgradeCell_t4117746046 * get_cell_1() const { return ___cell_1; }
	inline UpgradeCell_t4117746046 ** get_address_of_cell_1() { return &___cell_1; }
	inline void set_cell_1(UpgradeCell_t4117746046 * value)
	{
		___cell_1 = value;
		Il2CppCodeGenWriteBarrier(&___cell_1, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__this_2() { return static_cast<int32_t>(offsetof(U3CInitializeU3Ec__AnonStorey157_t1973587070, ___U3CU3Ef__this_2)); }
	inline TableButtons_t1868573683 * get_U3CU3Ef__this_2() const { return ___U3CU3Ef__this_2; }
	inline TableButtons_t1868573683 ** get_address_of_U3CU3Ef__this_2() { return &___U3CU3Ef__this_2; }
	inline void set_U3CU3Ef__this_2(TableButtons_t1868573683 * value)
	{
		___U3CU3Ef__this_2 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__this_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
