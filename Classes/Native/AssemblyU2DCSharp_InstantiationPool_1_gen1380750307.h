﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Action`1<System.Object>
struct Action_1_t985559125;
// System.String
struct String_t;
// System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.List`1<System.Object>>
struct Dictionary_2_t3271763293;
// System.Collections.Generic.Dictionary`2<System.String,UnityEngine.GameObject>
struct Dictionary_2_t1355425710;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// InstantiationPool`1<System.Object>
struct  InstantiationPool_1_t1380750307  : public Il2CppObject
{
public:
	// System.Action`1<T> InstantiationPool`1::onRecycle
	Action_1_t985559125 * ___onRecycle_0;
	// System.Action`1<T> InstantiationPool`1::onReuse
	Action_1_t985559125 * ___onReuse_1;
	// System.Action`1<T> InstantiationPool`1::onCreated
	Action_1_t985559125 * ___onCreated_2;
	// System.String InstantiationPool`1::searchFolder
	String_t* ___searchFolder_3;
	// System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.List`1<T>> InstantiationPool`1::usedObjects
	Dictionary_2_t3271763293 * ___usedObjects_4;
	// System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.List`1<T>> InstantiationPool`1::recycledObjects
	Dictionary_2_t3271763293 * ___recycledObjects_5;
	// System.Collections.Generic.Dictionary`2<System.String,UnityEngine.GameObject> InstantiationPool`1::loadedPrefabs
	Dictionary_2_t1355425710 * ___loadedPrefabs_6;

public:
	inline static int32_t get_offset_of_onRecycle_0() { return static_cast<int32_t>(offsetof(InstantiationPool_1_t1380750307, ___onRecycle_0)); }
	inline Action_1_t985559125 * get_onRecycle_0() const { return ___onRecycle_0; }
	inline Action_1_t985559125 ** get_address_of_onRecycle_0() { return &___onRecycle_0; }
	inline void set_onRecycle_0(Action_1_t985559125 * value)
	{
		___onRecycle_0 = value;
		Il2CppCodeGenWriteBarrier(&___onRecycle_0, value);
	}

	inline static int32_t get_offset_of_onReuse_1() { return static_cast<int32_t>(offsetof(InstantiationPool_1_t1380750307, ___onReuse_1)); }
	inline Action_1_t985559125 * get_onReuse_1() const { return ___onReuse_1; }
	inline Action_1_t985559125 ** get_address_of_onReuse_1() { return &___onReuse_1; }
	inline void set_onReuse_1(Action_1_t985559125 * value)
	{
		___onReuse_1 = value;
		Il2CppCodeGenWriteBarrier(&___onReuse_1, value);
	}

	inline static int32_t get_offset_of_onCreated_2() { return static_cast<int32_t>(offsetof(InstantiationPool_1_t1380750307, ___onCreated_2)); }
	inline Action_1_t985559125 * get_onCreated_2() const { return ___onCreated_2; }
	inline Action_1_t985559125 ** get_address_of_onCreated_2() { return &___onCreated_2; }
	inline void set_onCreated_2(Action_1_t985559125 * value)
	{
		___onCreated_2 = value;
		Il2CppCodeGenWriteBarrier(&___onCreated_2, value);
	}

	inline static int32_t get_offset_of_searchFolder_3() { return static_cast<int32_t>(offsetof(InstantiationPool_1_t1380750307, ___searchFolder_3)); }
	inline String_t* get_searchFolder_3() const { return ___searchFolder_3; }
	inline String_t** get_address_of_searchFolder_3() { return &___searchFolder_3; }
	inline void set_searchFolder_3(String_t* value)
	{
		___searchFolder_3 = value;
		Il2CppCodeGenWriteBarrier(&___searchFolder_3, value);
	}

	inline static int32_t get_offset_of_usedObjects_4() { return static_cast<int32_t>(offsetof(InstantiationPool_1_t1380750307, ___usedObjects_4)); }
	inline Dictionary_2_t3271763293 * get_usedObjects_4() const { return ___usedObjects_4; }
	inline Dictionary_2_t3271763293 ** get_address_of_usedObjects_4() { return &___usedObjects_4; }
	inline void set_usedObjects_4(Dictionary_2_t3271763293 * value)
	{
		___usedObjects_4 = value;
		Il2CppCodeGenWriteBarrier(&___usedObjects_4, value);
	}

	inline static int32_t get_offset_of_recycledObjects_5() { return static_cast<int32_t>(offsetof(InstantiationPool_1_t1380750307, ___recycledObjects_5)); }
	inline Dictionary_2_t3271763293 * get_recycledObjects_5() const { return ___recycledObjects_5; }
	inline Dictionary_2_t3271763293 ** get_address_of_recycledObjects_5() { return &___recycledObjects_5; }
	inline void set_recycledObjects_5(Dictionary_2_t3271763293 * value)
	{
		___recycledObjects_5 = value;
		Il2CppCodeGenWriteBarrier(&___recycledObjects_5, value);
	}

	inline static int32_t get_offset_of_loadedPrefabs_6() { return static_cast<int32_t>(offsetof(InstantiationPool_1_t1380750307, ___loadedPrefabs_6)); }
	inline Dictionary_2_t1355425710 * get_loadedPrefabs_6() const { return ___loadedPrefabs_6; }
	inline Dictionary_2_t1355425710 ** get_address_of_loadedPrefabs_6() { return &___loadedPrefabs_6; }
	inline void set_loadedPrefabs_6(Dictionary_2_t1355425710 * value)
	{
		___loadedPrefabs_6 = value;
		Il2CppCodeGenWriteBarrier(&___loadedPrefabs_6, value);
	}
};

struct InstantiationPool_1_t1380750307_StaticFields
{
public:
	// System.Action`1<T> InstantiationPool`1::<>f__am$cache7
	Action_1_t985559125 * ___U3CU3Ef__amU24cache7_7;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache7_7() { return static_cast<int32_t>(offsetof(InstantiationPool_1_t1380750307_StaticFields, ___U3CU3Ef__amU24cache7_7)); }
	inline Action_1_t985559125 * get_U3CU3Ef__amU24cache7_7() const { return ___U3CU3Ef__amU24cache7_7; }
	inline Action_1_t985559125 ** get_address_of_U3CU3Ef__amU24cache7_7() { return &___U3CU3Ef__amU24cache7_7; }
	inline void set_U3CU3Ef__amU24cache7_7(Action_1_t985559125 * value)
	{
		___U3CU3Ef__amU24cache7_7 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache7_7, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
