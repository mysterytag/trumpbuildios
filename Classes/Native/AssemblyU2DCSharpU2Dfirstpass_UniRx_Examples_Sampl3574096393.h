﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Func`2<UniRx.Examples.Sample04_ConvertFromUnityCallback/LogCallback,System.Boolean>
struct Func_2_t2102879704;
// System.Action`1<UniRx.Examples.Sample04_ConvertFromUnityCallback/LogCallback>
struct Action_1_t3384115434;

#include "UnityEngine_UnityEngine_MonoBehaviour3012272455.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Examples.Sample04_ConvertFromUnityCallback
struct  Sample04_ConvertFromUnityCallback_t3574096393  : public MonoBehaviour_t3012272455
{
public:

public:
};

struct Sample04_ConvertFromUnityCallback_t3574096393_StaticFields
{
public:
	// System.Func`2<UniRx.Examples.Sample04_ConvertFromUnityCallback/LogCallback,System.Boolean> UniRx.Examples.Sample04_ConvertFromUnityCallback::<>f__am$cache0
	Func_2_t2102879704 * ___U3CU3Ef__amU24cache0_2;
	// System.Action`1<UniRx.Examples.Sample04_ConvertFromUnityCallback/LogCallback> UniRx.Examples.Sample04_ConvertFromUnityCallback::<>f__am$cache1
	Action_1_t3384115434 * ___U3CU3Ef__amU24cache1_3;
	// System.Func`2<UniRx.Examples.Sample04_ConvertFromUnityCallback/LogCallback,System.Boolean> UniRx.Examples.Sample04_ConvertFromUnityCallback::<>f__am$cache2
	Func_2_t2102879704 * ___U3CU3Ef__amU24cache2_4;
	// System.Action`1<UniRx.Examples.Sample04_ConvertFromUnityCallback/LogCallback> UniRx.Examples.Sample04_ConvertFromUnityCallback::<>f__am$cache3
	Action_1_t3384115434 * ___U3CU3Ef__amU24cache3_5;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_2() { return static_cast<int32_t>(offsetof(Sample04_ConvertFromUnityCallback_t3574096393_StaticFields, ___U3CU3Ef__amU24cache0_2)); }
	inline Func_2_t2102879704 * get_U3CU3Ef__amU24cache0_2() const { return ___U3CU3Ef__amU24cache0_2; }
	inline Func_2_t2102879704 ** get_address_of_U3CU3Ef__amU24cache0_2() { return &___U3CU3Ef__amU24cache0_2; }
	inline void set_U3CU3Ef__amU24cache0_2(Func_2_t2102879704 * value)
	{
		___U3CU3Ef__amU24cache0_2 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache0_2, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache1_3() { return static_cast<int32_t>(offsetof(Sample04_ConvertFromUnityCallback_t3574096393_StaticFields, ___U3CU3Ef__amU24cache1_3)); }
	inline Action_1_t3384115434 * get_U3CU3Ef__amU24cache1_3() const { return ___U3CU3Ef__amU24cache1_3; }
	inline Action_1_t3384115434 ** get_address_of_U3CU3Ef__amU24cache1_3() { return &___U3CU3Ef__amU24cache1_3; }
	inline void set_U3CU3Ef__amU24cache1_3(Action_1_t3384115434 * value)
	{
		___U3CU3Ef__amU24cache1_3 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache1_3, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache2_4() { return static_cast<int32_t>(offsetof(Sample04_ConvertFromUnityCallback_t3574096393_StaticFields, ___U3CU3Ef__amU24cache2_4)); }
	inline Func_2_t2102879704 * get_U3CU3Ef__amU24cache2_4() const { return ___U3CU3Ef__amU24cache2_4; }
	inline Func_2_t2102879704 ** get_address_of_U3CU3Ef__amU24cache2_4() { return &___U3CU3Ef__amU24cache2_4; }
	inline void set_U3CU3Ef__amU24cache2_4(Func_2_t2102879704 * value)
	{
		___U3CU3Ef__amU24cache2_4 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache2_4, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache3_5() { return static_cast<int32_t>(offsetof(Sample04_ConvertFromUnityCallback_t3574096393_StaticFields, ___U3CU3Ef__amU24cache3_5)); }
	inline Action_1_t3384115434 * get_U3CU3Ef__amU24cache3_5() const { return ___U3CU3Ef__amU24cache3_5; }
	inline Action_1_t3384115434 ** get_address_of_U3CU3Ef__amU24cache3_5() { return &___U3CU3Ef__amU24cache3_5; }
	inline void set_U3CU3Ef__amU24cache3_5(Action_1_t3384115434 * value)
	{
		___U3CU3Ef__amU24cache3_5 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache3_5, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
