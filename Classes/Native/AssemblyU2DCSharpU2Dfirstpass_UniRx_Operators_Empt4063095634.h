﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UniRx.IScheduler
struct IScheduler_t2938318244;

#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Opera438050906.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Operators.EmptyObservable`1<UniRx.CollectionReplaceEvent`1<UniRx.Tuple`2<System.Object,System.Object>>>
struct  EmptyObservable_1_t4063095634  : public OperatorObservableBase_1_t438050906
{
public:
	// UniRx.IScheduler UniRx.Operators.EmptyObservable`1::scheduler
	Il2CppObject * ___scheduler_1;

public:
	inline static int32_t get_offset_of_scheduler_1() { return static_cast<int32_t>(offsetof(EmptyObservable_1_t4063095634, ___scheduler_1)); }
	inline Il2CppObject * get_scheduler_1() const { return ___scheduler_1; }
	inline Il2CppObject ** get_address_of_scheduler_1() { return &___scheduler_1; }
	inline void set_scheduler_1(Il2CppObject * value)
	{
		___scheduler_1 = value;
		Il2CppCodeGenWriteBarrier(&___scheduler_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
