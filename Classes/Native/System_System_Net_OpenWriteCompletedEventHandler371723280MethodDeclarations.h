﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Net.OpenWriteCompletedEventHandler
struct OpenWriteCompletedEventHandler_t371723280;
// System.Object
struct Il2CppObject;
// System.Net.OpenWriteCompletedEventArgs
struct OpenWriteCompletedEventArgs_t2142017643;
// System.IAsyncResult
struct IAsyncResult_t537683269;
// System.AsyncCallback
struct AsyncCallback_t1363551830;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_IntPtr676692020.h"
#include "System_System_Net_OpenWriteCompletedEventArgs2142017643.h"
#include "mscorlib_System_AsyncCallback1363551830.h"

// System.Void System.Net.OpenWriteCompletedEventHandler::.ctor(System.Object,System.IntPtr)
extern "C"  void OpenWriteCompletedEventHandler__ctor_m2704479118 (OpenWriteCompletedEventHandler_t371723280 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.OpenWriteCompletedEventHandler::Invoke(System.Object,System.Net.OpenWriteCompletedEventArgs)
extern "C"  void OpenWriteCompletedEventHandler_Invoke_m451376585 (OpenWriteCompletedEventHandler_t371723280 * __this, Il2CppObject * ___sender0, OpenWriteCompletedEventArgs_t2142017643 * ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
extern "C" void pinvoke_delegate_wrapper_OpenWriteCompletedEventHandler_t371723280(Il2CppObject* delegate, Il2CppObject * ___sender0, OpenWriteCompletedEventArgs_t2142017643 * ___e1);
// System.IAsyncResult System.Net.OpenWriteCompletedEventHandler::BeginInvoke(System.Object,System.Net.OpenWriteCompletedEventArgs,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * OpenWriteCompletedEventHandler_BeginInvoke_m1419109786 (OpenWriteCompletedEventHandler_t371723280 * __this, Il2CppObject * ___sender0, OpenWriteCompletedEventArgs_t2142017643 * ___e1, AsyncCallback_t1363551830 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.OpenWriteCompletedEventHandler::EndInvoke(System.IAsyncResult)
extern "C"  void OpenWriteCompletedEventHandler_EndInvoke_m3360324766 (OpenWriteCompletedEventHandler_t371723280 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
