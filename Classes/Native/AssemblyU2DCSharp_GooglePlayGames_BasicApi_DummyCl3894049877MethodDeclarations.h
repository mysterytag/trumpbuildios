﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GooglePlayGames.BasicApi.DummyClient
struct DummyClient_t3894049877;
// System.Action`1<System.Boolean>
struct Action_1_t359458046;
// System.String
struct String_t;
// System.Collections.Generic.List`1<GooglePlayGames.BasicApi.Achievement>
struct List_1_t3683771882;
// GooglePlayGames.BasicApi.Achievement
struct Achievement_t2886812913;
// System.Action`1<GooglePlayGames.BasicApi.UIStatus>
struct Action_1_t284412649;
// GooglePlayGames.BasicApi.OnStateLoadedListener
struct OnStateLoadedListener_t1907554349;
// System.Byte[]
struct ByteU5BU5D_t58506160;
// GooglePlayGames.BasicApi.Multiplayer.IRealTimeMultiplayerClient
struct IRealTimeMultiplayerClient_t2806993687;
// GooglePlayGames.BasicApi.Multiplayer.ITurnBasedMultiplayerClient
struct ITurnBasedMultiplayerClient_t590906942;
// GooglePlayGames.BasicApi.SavedGame.ISavedGameClient
struct ISavedGameClient_t4011729670;
// GooglePlayGames.BasicApi.Events.IEventsClient
struct IEventsClient_t2655150760;
// GooglePlayGames.BasicApi.Quests.IQuestsClient
struct IQuestsClient_t959561416;
// GooglePlayGames.BasicApi.InvitationReceivedDelegate
struct InvitationReceivedDelegate_t716623425;
// GooglePlayGames.BasicApi.Multiplayer.Invitation
struct Invitation_t3047929759;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String968488902.h"
#include "AssemblyU2DCSharp_GooglePlayGames_BasicApi_Invitati716623425.h"

// System.Void GooglePlayGames.BasicApi.DummyClient::.ctor()
extern "C"  void DummyClient__ctor_m320356400 (DummyClient_t3894049877 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.BasicApi.DummyClient::Authenticate(System.Action`1<System.Boolean>,System.Boolean)
extern "C"  void DummyClient_Authenticate_m2819517267 (DummyClient_t3894049877 * __this, Action_1_t359458046 * ___callback0, bool ___silent1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GooglePlayGames.BasicApi.DummyClient::IsAuthenticated()
extern "C"  bool DummyClient_IsAuthenticated_m2058474943 (DummyClient_t3894049877 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.BasicApi.DummyClient::SignOut()
extern "C"  void DummyClient_SignOut_m3624213023 (DummyClient_t3894049877 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String GooglePlayGames.BasicApi.DummyClient::GetAccessToken()
extern "C"  String_t* DummyClient_GetAccessToken_m3227382582 (DummyClient_t3894049877 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String GooglePlayGames.BasicApi.DummyClient::GetIdToken()
extern "C"  String_t* DummyClient_GetIdToken_m1962086303 (DummyClient_t3894049877 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String GooglePlayGames.BasicApi.DummyClient::GetUserId()
extern "C"  String_t* DummyClient_GetUserId_m1249568071 (DummyClient_t3894049877 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String GooglePlayGames.BasicApi.DummyClient::GetUserEmail()
extern "C"  String_t* DummyClient_GetUserEmail_m2458116050 (DummyClient_t3894049877 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String GooglePlayGames.BasicApi.DummyClient::GetUserDisplayName()
extern "C"  String_t* DummyClient_GetUserDisplayName_m2739125923 (DummyClient_t3894049877 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String GooglePlayGames.BasicApi.DummyClient::GetUserImageUrl()
extern "C"  String_t* DummyClient_GetUserImageUrl_m2120188864 (DummyClient_t3894049877 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<GooglePlayGames.BasicApi.Achievement> GooglePlayGames.BasicApi.DummyClient::GetAchievements()
extern "C"  List_1_t3683771882 * DummyClient_GetAchievements_m2874064110 (DummyClient_t3894049877 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// GooglePlayGames.BasicApi.Achievement GooglePlayGames.BasicApi.DummyClient::GetAchievement(System.String)
extern "C"  Achievement_t2886812913 * DummyClient_GetAchievement_m1406883999 (DummyClient_t3894049877 * __this, String_t* ___achId0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.BasicApi.DummyClient::UnlockAchievement(System.String,System.Action`1<System.Boolean>)
extern "C"  void DummyClient_UnlockAchievement_m1285816246 (DummyClient_t3894049877 * __this, String_t* ___achId0, Action_1_t359458046 * ___callback1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.BasicApi.DummyClient::RevealAchievement(System.String,System.Action`1<System.Boolean>)
extern "C"  void DummyClient_RevealAchievement_m3355155199 (DummyClient_t3894049877 * __this, String_t* ___achId0, Action_1_t359458046 * ___callback1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.BasicApi.DummyClient::IncrementAchievement(System.String,System.Int32,System.Action`1<System.Boolean>)
extern "C"  void DummyClient_IncrementAchievement_m2206294486 (DummyClient_t3894049877 * __this, String_t* ___achId0, int32_t ___steps1, Action_1_t359458046 * ___callback2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.BasicApi.DummyClient::ShowAchievementsUI(System.Action`1<GooglePlayGames.BasicApi.UIStatus>)
extern "C"  void DummyClient_ShowAchievementsUI_m461724835 (DummyClient_t3894049877 * __this, Action_1_t284412649 * ___callback0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.BasicApi.DummyClient::ShowLeaderboardUI(System.String,System.Action`1<GooglePlayGames.BasicApi.UIStatus>)
extern "C"  void DummyClient_ShowLeaderboardUI_m1247533984 (DummyClient_t3894049877 * __this, String_t* ___lbId0, Action_1_t284412649 * ___callback1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.BasicApi.DummyClient::SubmitScore(System.String,System.Int64,System.Action`1<System.Boolean>)
extern "C"  void DummyClient_SubmitScore_m2922134891 (DummyClient_t3894049877 * __this, String_t* ___lbId0, int64_t ___score1, Action_1_t359458046 * ___callback2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.BasicApi.DummyClient::LoadState(System.Int32,GooglePlayGames.BasicApi.OnStateLoadedListener)
extern "C"  void DummyClient_LoadState_m1062020377 (DummyClient_t3894049877 * __this, int32_t ___slot0, Il2CppObject * ___listener1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.BasicApi.DummyClient::UpdateState(System.Int32,System.Byte[],GooglePlayGames.BasicApi.OnStateLoadedListener)
extern "C"  void DummyClient_UpdateState_m2645504089 (DummyClient_t3894049877 * __this, int32_t ___slot0, ByteU5BU5D_t58506160* ___data1, Il2CppObject * ___listener2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// GooglePlayGames.BasicApi.Multiplayer.IRealTimeMultiplayerClient GooglePlayGames.BasicApi.DummyClient::GetRtmpClient()
extern "C"  Il2CppObject * DummyClient_GetRtmpClient_m596573550 (DummyClient_t3894049877 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// GooglePlayGames.BasicApi.Multiplayer.ITurnBasedMultiplayerClient GooglePlayGames.BasicApi.DummyClient::GetTbmpClient()
extern "C"  Il2CppObject * DummyClient_GetTbmpClient_m4198461663 (DummyClient_t3894049877 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// GooglePlayGames.BasicApi.SavedGame.ISavedGameClient GooglePlayGames.BasicApi.DummyClient::GetSavedGameClient()
extern "C"  Il2CppObject * DummyClient_GetSavedGameClient_m73236605 (DummyClient_t3894049877 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// GooglePlayGames.BasicApi.Events.IEventsClient GooglePlayGames.BasicApi.DummyClient::GetEventsClient()
extern "C"  Il2CppObject * DummyClient_GetEventsClient_m4207568117 (DummyClient_t3894049877 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// GooglePlayGames.BasicApi.Quests.IQuestsClient GooglePlayGames.BasicApi.DummyClient::GetQuestsClient()
extern "C"  Il2CppObject * DummyClient_GetQuestsClient_m1374386461 (DummyClient_t3894049877 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.BasicApi.DummyClient::RegisterInvitationDelegate(GooglePlayGames.BasicApi.InvitationReceivedDelegate)
extern "C"  void DummyClient_RegisterInvitationDelegate_m2515718614 (DummyClient_t3894049877 * __this, InvitationReceivedDelegate_t716623425 * ___deleg0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// GooglePlayGames.BasicApi.Multiplayer.Invitation GooglePlayGames.BasicApi.DummyClient::GetInvitationFromNotification()
extern "C"  Invitation_t3047929759 * DummyClient_GetInvitationFromNotification_m2852733012 (DummyClient_t3894049877 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GooglePlayGames.BasicApi.DummyClient::HasInvitationFromNotification()
extern "C"  bool DummyClient_HasInvitationFromNotification_m3113596866 (DummyClient_t3894049877 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.BasicApi.DummyClient::LogUsage()
extern "C"  void DummyClient_LogUsage_m101351185 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
