﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_P1335382412MethodDeclarations.h"

// System.Void UniRx.InternalUtil.PriorityQueue`1<UniRx.InternalUtil.ScheduledItem>::.ctor()
#define PriorityQueue_1__ctor_m4230080943(__this, method) ((  void (*) (PriorityQueue_1_t2411179237 *, const MethodInfo*))PriorityQueue_1__ctor_m698167010_gshared)(__this, method)
// System.Void UniRx.InternalUtil.PriorityQueue`1<UniRx.InternalUtil.ScheduledItem>::.ctor(System.Int32)
#define PriorityQueue_1__ctor_m900436096(__this, ___capacity0, method) ((  void (*) (PriorityQueue_1_t2411179237 *, int32_t, const MethodInfo*))PriorityQueue_1__ctor_m3615596851_gshared)(__this, ___capacity0, method)
// System.Void UniRx.InternalUtil.PriorityQueue`1<UniRx.InternalUtil.ScheduledItem>::.cctor()
#define PriorityQueue_1__cctor_m1801394142(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))PriorityQueue_1__cctor_m3981211915_gshared)(__this /* static, unused */, method)
// System.Boolean UniRx.InternalUtil.PriorityQueue`1<UniRx.InternalUtil.ScheduledItem>::IsHigherPriority(System.Int32,System.Int32)
#define PriorityQueue_1_IsHigherPriority_m1956319352(__this, ___left0, ___right1, method) ((  bool (*) (PriorityQueue_1_t2411179237 *, int32_t, int32_t, const MethodInfo*))PriorityQueue_1_IsHigherPriority_m999759587_gshared)(__this, ___left0, ___right1, method)
// System.Void UniRx.InternalUtil.PriorityQueue`1<UniRx.InternalUtil.ScheduledItem>::Percolate(System.Int32)
#define PriorityQueue_1_Percolate_m4243371213(__this, ___index0, method) ((  void (*) (PriorityQueue_1_t2411179237 *, int32_t, const MethodInfo*))PriorityQueue_1_Percolate_m2937402368_gshared)(__this, ___index0, method)
// System.Void UniRx.InternalUtil.PriorityQueue`1<UniRx.InternalUtil.ScheduledItem>::Heapify()
#define PriorityQueue_1_Heapify_m1849743709(__this, method) ((  void (*) (PriorityQueue_1_t2411179237 *, const MethodInfo*))PriorityQueue_1_Heapify_m704617936_gshared)(__this, method)
// System.Void UniRx.InternalUtil.PriorityQueue`1<UniRx.InternalUtil.ScheduledItem>::Heapify(System.Int32)
#define PriorityQueue_1_Heapify_m3672795950(__this, ___index0, method) ((  void (*) (PriorityQueue_1_t2411179237 *, int32_t, const MethodInfo*))PriorityQueue_1_Heapify_m1602165537_gshared)(__this, ___index0, method)
// System.Int32 UniRx.InternalUtil.PriorityQueue`1<UniRx.InternalUtil.ScheduledItem>::get_Count()
#define PriorityQueue_1_get_Count_m1827540733(__this, method) ((  int32_t (*) (PriorityQueue_1_t2411179237 *, const MethodInfo*))PriorityQueue_1_get_Count_m1055643996_gshared)(__this, method)
// T UniRx.InternalUtil.PriorityQueue`1<UniRx.InternalUtil.ScheduledItem>::Peek()
#define PriorityQueue_1_Peek_m279483471(__this, method) ((  ScheduledItem_t1912903245 * (*) (PriorityQueue_1_t2411179237 *, const MethodInfo*))PriorityQueue_1_Peek_m2099666718_gshared)(__this, method)
// System.Void UniRx.InternalUtil.PriorityQueue`1<UniRx.InternalUtil.ScheduledItem>::RemoveAt(System.Int32)
#define PriorityQueue_1_RemoveAt_m2114228509(__this, ___index0, method) ((  void (*) (PriorityQueue_1_t2411179237 *, int32_t, const MethodInfo*))PriorityQueue_1_RemoveAt_m2349195146_gshared)(__this, ___index0, method)
// T UniRx.InternalUtil.PriorityQueue`1<UniRx.InternalUtil.ScheduledItem>::Dequeue()
#define PriorityQueue_1_Dequeue_m770213022(__this, method) ((  ScheduledItem_t1912903245 * (*) (PriorityQueue_1_t2411179237 *, const MethodInfo*))PriorityQueue_1_Dequeue_m1887212399_gshared)(__this, method)
// System.Void UniRx.InternalUtil.PriorityQueue`1<UniRx.InternalUtil.ScheduledItem>::Enqueue(T)
#define PriorityQueue_1_Enqueue_m871286825(__this, ___item0, method) ((  void (*) (PriorityQueue_1_t2411179237 *, ScheduledItem_t1912903245 *, const MethodInfo*))PriorityQueue_1_Enqueue_m4027093526_gshared)(__this, ___item0, method)
// System.Boolean UniRx.InternalUtil.PriorityQueue`1<UniRx.InternalUtil.ScheduledItem>::Remove(T)
#define PriorityQueue_1_Remove_m441804585(__this, ___item0, method) ((  bool (*) (PriorityQueue_1_t2411179237 *, ScheduledItem_t1912903245 *, const MethodInfo*))PriorityQueue_1_Remove_m112164948_gshared)(__this, ___item0, method)
