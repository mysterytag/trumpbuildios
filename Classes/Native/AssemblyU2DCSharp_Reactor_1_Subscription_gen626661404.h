﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Reactor`1<System.Int64>
struct Reactor_1_t2107219778;
// System.Action`1<System.Int64>
struct Action_1_t2995867587;

#include "mscorlib_System_Object837106420.h"
#include "AssemblyU2DCSharp_Priority3194150340.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Reactor`1/Subscription<System.Int64>
struct  Subscription_t626661405  : public Il2CppObject
{
public:
	// Reactor`1<T> Reactor`1/Subscription::parent
	Reactor_1_t2107219778 * ___parent_0;
	// System.Action`1<T> Reactor`1/Subscription::unsubscribeTarget
	Action_1_t2995867587 * ___unsubscribeTarget_1;
	// Priority Reactor`1/Subscription::priority
	int32_t ___priority_2;

public:
	inline static int32_t get_offset_of_parent_0() { return static_cast<int32_t>(offsetof(Subscription_t626661405, ___parent_0)); }
	inline Reactor_1_t2107219778 * get_parent_0() const { return ___parent_0; }
	inline Reactor_1_t2107219778 ** get_address_of_parent_0() { return &___parent_0; }
	inline void set_parent_0(Reactor_1_t2107219778 * value)
	{
		___parent_0 = value;
		Il2CppCodeGenWriteBarrier(&___parent_0, value);
	}

	inline static int32_t get_offset_of_unsubscribeTarget_1() { return static_cast<int32_t>(offsetof(Subscription_t626661405, ___unsubscribeTarget_1)); }
	inline Action_1_t2995867587 * get_unsubscribeTarget_1() const { return ___unsubscribeTarget_1; }
	inline Action_1_t2995867587 ** get_address_of_unsubscribeTarget_1() { return &___unsubscribeTarget_1; }
	inline void set_unsubscribeTarget_1(Action_1_t2995867587 * value)
	{
		___unsubscribeTarget_1 = value;
		Il2CppCodeGenWriteBarrier(&___unsubscribeTarget_1, value);
	}

	inline static int32_t get_offset_of_priority_2() { return static_cast<int32_t>(offsetof(Subscription_t626661405, ___priority_2)); }
	inline int32_t get_priority_2() const { return ___priority_2; }
	inline int32_t* get_address_of_priority_2() { return &___priority_2; }
	inline void set_priority_2(int32_t value)
	{
		___priority_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
