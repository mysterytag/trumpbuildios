﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.ClientModels.UserGameCenterInfo
struct UserGameCenterInfo_t386684176;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String968488902.h"

// System.Void PlayFab.ClientModels.UserGameCenterInfo::.ctor()
extern "C"  void UserGameCenterInfo__ctor_m543460013 (UserGameCenterInfo_t386684176 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PlayFab.ClientModels.UserGameCenterInfo::get_GameCenterId()
extern "C"  String_t* UserGameCenterInfo_get_GameCenterId_m3776865759 (UserGameCenterInfo_t386684176 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.UserGameCenterInfo::set_GameCenterId(System.String)
extern "C"  void UserGameCenterInfo_set_GameCenterId_m811536236 (UserGameCenterInfo_t386684176 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
