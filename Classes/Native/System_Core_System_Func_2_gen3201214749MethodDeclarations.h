﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "System_Core_System_Func_2_gen3934242701MethodDeclarations.h"

// System.Void System.Func`2<System.Int32,ICell`1<System.Int32>>::.ctor(System.Object,System.IntPtr)
#define Func_2__ctor_m2095067900(__this, ___object0, ___method1, method) ((  void (*) (Func_2_t3201214749 *, Il2CppObject *, IntPtr_t, const MethodInfo*))Func_2__ctor_m109266461_gshared)(__this, ___object0, ___method1, method)
// TResult System.Func`2<System.Int32,ICell`1<System.Int32>>::Invoke(T)
#define Func_2_Invoke_m1965967738(__this, ___arg10, method) ((  Il2CppObject* (*) (Func_2_t3201214749 *, int32_t, const MethodInfo*))Func_2_Invoke_m3930687529_gshared)(__this, ___arg10, method)
// System.IAsyncResult System.Func`2<System.Int32,ICell`1<System.Int32>>::BeginInvoke(T,System.AsyncCallback,System.Object)
#define Func_2_BeginInvoke_m3263178541(__this, ___arg10, ___callback1, ___object2, method) ((  Il2CppObject * (*) (Func_2_t3201214749 *, int32_t, AsyncCallback_t1363551830 *, Il2CppObject *, const MethodInfo*))Func_2_BeginInvoke_m1814744284_gshared)(__this, ___arg10, ___callback1, ___object2, method)
// TResult System.Func`2<System.Int32,ICell`1<System.Int32>>::EndInvoke(System.IAsyncResult)
#define Func_2_EndInvoke_m2958592666(__this, ___result0, method) ((  Il2CppObject* (*) (Func_2_t3201214749 *, Il2CppObject *, const MethodInfo*))Func_2_EndInvoke_m1656397643_gshared)(__this, ___result0, method)
