﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Ionic.Zlib.InfTree
struct InfTree_t1872341100;
// System.Int32[]
struct Int32U5BU5D_t1809983122;
// Ionic.Zlib.ZlibCodec
struct ZlibCodec_t3910383704;
// System.Int32[][]
struct Int32U5BU5DU5BU5D_t158556903;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Ionic_Zlib_ZlibCodec3910383704.h"

// System.Void Ionic.Zlib.InfTree::.ctor()
extern "C"  void InfTree__ctor_m4294101555 (InfTree_t1872341100 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Ionic.Zlib.InfTree::.cctor()
extern "C"  void InfTree__cctor_m3786033114 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Ionic.Zlib.InfTree::huft_build(System.Int32[],System.Int32,System.Int32,System.Int32,System.Int32[],System.Int32[],System.Int32[],System.Int32[],System.Int32[],System.Int32[],System.Int32[])
extern "C"  int32_t InfTree_huft_build_m685052630 (InfTree_t1872341100 * __this, Int32U5BU5D_t1809983122* ___b0, int32_t ___bindex1, int32_t ___n2, int32_t ___s3, Int32U5BU5D_t1809983122* ___d4, Int32U5BU5D_t1809983122* ___e5, Int32U5BU5D_t1809983122* ___t6, Int32U5BU5D_t1809983122* ___m7, Int32U5BU5D_t1809983122* ___hp8, Int32U5BU5D_t1809983122* ___hn9, Int32U5BU5D_t1809983122* ___v10, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Ionic.Zlib.InfTree::inflate_trees_bits(System.Int32[],System.Int32[],System.Int32[],System.Int32[],Ionic.Zlib.ZlibCodec)
extern "C"  int32_t InfTree_inflate_trees_bits_m3109313139 (InfTree_t1872341100 * __this, Int32U5BU5D_t1809983122* ___c0, Int32U5BU5D_t1809983122* ___bb1, Int32U5BU5D_t1809983122* ___tb2, Int32U5BU5D_t1809983122* ___hp3, ZlibCodec_t3910383704 * ___z4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Ionic.Zlib.InfTree::inflate_trees_dynamic(System.Int32,System.Int32,System.Int32[],System.Int32[],System.Int32[],System.Int32[],System.Int32[],System.Int32[],Ionic.Zlib.ZlibCodec)
extern "C"  int32_t InfTree_inflate_trees_dynamic_m4132442924 (InfTree_t1872341100 * __this, int32_t ___nl0, int32_t ___nd1, Int32U5BU5D_t1809983122* ___c2, Int32U5BU5D_t1809983122* ___bl3, Int32U5BU5D_t1809983122* ___bd4, Int32U5BU5D_t1809983122* ___tl5, Int32U5BU5D_t1809983122* ___td6, Int32U5BU5D_t1809983122* ___hp7, ZlibCodec_t3910383704 * ___z8, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Ionic.Zlib.InfTree::inflate_trees_fixed(System.Int32[],System.Int32[],System.Int32[][],System.Int32[][],Ionic.Zlib.ZlibCodec)
extern "C"  int32_t InfTree_inflate_trees_fixed_m3175301473 (Il2CppObject * __this /* static, unused */, Int32U5BU5D_t1809983122* ___bl0, Int32U5BU5D_t1809983122* ___bd1, Int32U5BU5DU5BU5D_t158556903* ___tl2, Int32U5BU5DU5BU5D_t158556903* ___td3, ZlibCodec_t3910383704 * ___z4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Ionic.Zlib.InfTree::initWorkArea(System.Int32)
extern "C"  void InfTree_initWorkArea_m2659300432 (InfTree_t1872341100 * __this, int32_t ___vsize0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
