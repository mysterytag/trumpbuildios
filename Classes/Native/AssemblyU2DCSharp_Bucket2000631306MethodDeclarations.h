﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Bucket
struct Bucket_t2000631306;

#include "codegen/il2cpp-codegen.h"

// System.Void Bucket::.ctor()
extern "C"  void Bucket__ctor_m2493733329 (Bucket_t2000631306 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Bucket::PostInject()
extern "C"  void Bucket_PostInject_m1461401060 (Bucket_t2000631306 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
