﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Subject`1<UniRx.CountChangedStatus>
struct Subject_1_t1414295045;
// System.Exception
struct Exception_t1967233988;
// System.IDisposable
struct IDisposable_t1628921374;
// UniRx.IObserver`1<UniRx.CountChangedStatus>
struct IObserver_1_t1688259328;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Exception1967233988.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_CountChangedSt3771227721.h"

// System.Void UniRx.Subject`1<UniRx.CountChangedStatus>::.ctor()
extern "C"  void Subject_1__ctor_m735375039_gshared (Subject_1_t1414295045 * __this, const MethodInfo* method);
#define Subject_1__ctor_m735375039(__this, method) ((  void (*) (Subject_1_t1414295045 *, const MethodInfo*))Subject_1__ctor_m735375039_gshared)(__this, method)
// System.Boolean UniRx.Subject`1<UniRx.CountChangedStatus>::get_HasObservers()
extern "C"  bool Subject_1_get_HasObservers_m3034958517_gshared (Subject_1_t1414295045 * __this, const MethodInfo* method);
#define Subject_1_get_HasObservers_m3034958517(__this, method) ((  bool (*) (Subject_1_t1414295045 *, const MethodInfo*))Subject_1_get_HasObservers_m3034958517_gshared)(__this, method)
// System.Void UniRx.Subject`1<UniRx.CountChangedStatus>::OnCompleted()
extern "C"  void Subject_1_OnCompleted_m4036744969_gshared (Subject_1_t1414295045 * __this, const MethodInfo* method);
#define Subject_1_OnCompleted_m4036744969(__this, method) ((  void (*) (Subject_1_t1414295045 *, const MethodInfo*))Subject_1_OnCompleted_m4036744969_gshared)(__this, method)
// System.Void UniRx.Subject`1<UniRx.CountChangedStatus>::OnError(System.Exception)
extern "C"  void Subject_1_OnError_m1775714998_gshared (Subject_1_t1414295045 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method);
#define Subject_1_OnError_m1775714998(__this, ___error0, method) ((  void (*) (Subject_1_t1414295045 *, Exception_t1967233988 *, const MethodInfo*))Subject_1_OnError_m1775714998_gshared)(__this, ___error0, method)
// System.Void UniRx.Subject`1<UniRx.CountChangedStatus>::OnNext(T)
extern "C"  void Subject_1_OnNext_m856511911_gshared (Subject_1_t1414295045 * __this, int32_t ___value0, const MethodInfo* method);
#define Subject_1_OnNext_m856511911(__this, ___value0, method) ((  void (*) (Subject_1_t1414295045 *, int32_t, const MethodInfo*))Subject_1_OnNext_m856511911_gshared)(__this, ___value0, method)
// System.IDisposable UniRx.Subject`1<UniRx.CountChangedStatus>::Subscribe(UniRx.IObserver`1<T>)
extern "C"  Il2CppObject * Subject_1_Subscribe_m598603732_gshared (Subject_1_t1414295045 * __this, Il2CppObject* ___observer0, const MethodInfo* method);
#define Subject_1_Subscribe_m598603732(__this, ___observer0, method) ((  Il2CppObject * (*) (Subject_1_t1414295045 *, Il2CppObject*, const MethodInfo*))Subject_1_Subscribe_m598603732_gshared)(__this, ___observer0, method)
// System.Void UniRx.Subject`1<UniRx.CountChangedStatus>::Dispose()
extern "C"  void Subject_1_Dispose_m2219059004_gshared (Subject_1_t1414295045 * __this, const MethodInfo* method);
#define Subject_1_Dispose_m2219059004(__this, method) ((  void (*) (Subject_1_t1414295045 *, const MethodInfo*))Subject_1_Dispose_m2219059004_gshared)(__this, method)
// System.Void UniRx.Subject`1<UniRx.CountChangedStatus>::ThrowIfDisposed()
extern "C"  void Subject_1_ThrowIfDisposed_m1012192453_gshared (Subject_1_t1414295045 * __this, const MethodInfo* method);
#define Subject_1_ThrowIfDisposed_m1012192453(__this, method) ((  void (*) (Subject_1_t1414295045 *, const MethodInfo*))Subject_1_ThrowIfDisposed_m1012192453_gshared)(__this, method)
// System.Boolean UniRx.Subject`1<UniRx.CountChangedStatus>::IsRequiredSubscribeOnCurrentThread()
extern "C"  bool Subject_1_IsRequiredSubscribeOnCurrentThread_m3576353900_gshared (Subject_1_t1414295045 * __this, const MethodInfo* method);
#define Subject_1_IsRequiredSubscribeOnCurrentThread_m3576353900(__this, method) ((  bool (*) (Subject_1_t1414295045 *, const MethodInfo*))Subject_1_IsRequiredSubscribeOnCurrentThread_m3576353900_gshared)(__this, method)
