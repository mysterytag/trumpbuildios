﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.UUnit.UUnitTestAttribute
struct UUnitTestAttribute_t2882581787;

#include "codegen/il2cpp-codegen.h"

// System.Void PlayFab.UUnit.UUnitTestAttribute::.ctor()
extern "C"  void UUnitTestAttribute__ctor_m3858152826 (UUnitTestAttribute_t2882581787 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
