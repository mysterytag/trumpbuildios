﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.AsyncSubject`1/Subscription<UniRx.Unit>
struct Subscription_t337532560;
// UniRx.AsyncSubject`1<UniRx.Unit>
struct AsyncSubject_1_t3257925142;
// UniRx.IObserver`1<UniRx.Unit>
struct IObserver_1_t475317645;

#include "codegen/il2cpp-codegen.h"

// System.Void UniRx.AsyncSubject`1/Subscription<UniRx.Unit>::.ctor(UniRx.AsyncSubject`1<T>,UniRx.IObserver`1<T>)
extern "C"  void Subscription__ctor_m1842391409_gshared (Subscription_t337532560 * __this, AsyncSubject_1_t3257925142 * ___parent0, Il2CppObject* ___unsubscribeTarget1, const MethodInfo* method);
#define Subscription__ctor_m1842391409(__this, ___parent0, ___unsubscribeTarget1, method) ((  void (*) (Subscription_t337532560 *, AsyncSubject_1_t3257925142 *, Il2CppObject*, const MethodInfo*))Subscription__ctor_m1842391409_gshared)(__this, ___parent0, ___unsubscribeTarget1, method)
// System.Void UniRx.AsyncSubject`1/Subscription<UniRx.Unit>::Dispose()
extern "C"  void Subscription_Dispose_m244388123_gshared (Subscription_t337532560 * __this, const MethodInfo* method);
#define Subscription_Dispose_m244388123(__this, method) ((  void (*) (Subscription_t337532560 *, const MethodInfo*))Subscription_Dispose_m244388123_gshared)(__this, method)
