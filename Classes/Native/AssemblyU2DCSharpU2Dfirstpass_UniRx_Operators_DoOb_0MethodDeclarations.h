﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.DoObservable`1/Do<System.Object>
struct Do_t3185263597;
// UniRx.Operators.DoObservable`1<System.Object>
struct DoObservable_1_t911152645;
// UniRx.IObserver`1<System.Object>
struct IObserver_1_t3049105323;
// System.IDisposable
struct IDisposable_t1628921374;
// System.Object
struct Il2CppObject;
// System.Exception
struct Exception_t1967233988;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_Exception1967233988.h"

// System.Void UniRx.Operators.DoObservable`1/Do<System.Object>::.ctor(UniRx.Operators.DoObservable`1<T>,UniRx.IObserver`1<T>,System.IDisposable)
extern "C"  void Do__ctor_m3090528687_gshared (Do_t3185263597 * __this, DoObservable_1_t911152645 * ___parent0, Il2CppObject* ___observer1, Il2CppObject * ___cancel2, const MethodInfo* method);
#define Do__ctor_m3090528687(__this, ___parent0, ___observer1, ___cancel2, method) ((  void (*) (Do_t3185263597 *, DoObservable_1_t911152645 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))Do__ctor_m3090528687_gshared)(__this, ___parent0, ___observer1, ___cancel2, method)
// System.IDisposable UniRx.Operators.DoObservable`1/Do<System.Object>::Run()
extern "C"  Il2CppObject * Do_Run_m1114745637_gshared (Do_t3185263597 * __this, const MethodInfo* method);
#define Do_Run_m1114745637(__this, method) ((  Il2CppObject * (*) (Do_t3185263597 *, const MethodInfo*))Do_Run_m1114745637_gshared)(__this, method)
// System.Void UniRx.Operators.DoObservable`1/Do<System.Object>::OnNext(T)
extern "C"  void Do_OnNext_m3851476927_gshared (Do_t3185263597 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Do_OnNext_m3851476927(__this, ___value0, method) ((  void (*) (Do_t3185263597 *, Il2CppObject *, const MethodInfo*))Do_OnNext_m3851476927_gshared)(__this, ___value0, method)
// System.Void UniRx.Operators.DoObservable`1/Do<System.Object>::OnError(System.Exception)
extern "C"  void Do_OnError_m3623066830_gshared (Do_t3185263597 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method);
#define Do_OnError_m3623066830(__this, ___error0, method) ((  void (*) (Do_t3185263597 *, Exception_t1967233988 *, const MethodInfo*))Do_OnError_m3623066830_gshared)(__this, ___error0, method)
// System.Void UniRx.Operators.DoObservable`1/Do<System.Object>::OnCompleted()
extern "C"  void Do_OnCompleted_m1134335265_gshared (Do_t3185263597 * __this, const MethodInfo* method);
#define Do_OnCompleted_m1134335265(__this, method) ((  void (*) (Do_t3185263597 *, const MethodInfo*))Do_OnCompleted_m1134335265_gshared)(__this, method)
