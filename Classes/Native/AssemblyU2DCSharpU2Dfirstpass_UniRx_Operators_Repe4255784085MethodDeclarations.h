﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.RepeatObservable`1<System.Object>
struct RepeatObservable_1_t4255784085;
// System.Object
struct Il2CppObject;
// UniRx.IScheduler
struct IScheduler_t2938318244;
// System.IDisposable
struct IDisposable_t1628921374;
// UniRx.IObserver`1<System.Object>
struct IObserver_1_t3049105323;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_Nullable_1_gen1438485399.h"

// System.Void UniRx.Operators.RepeatObservable`1<System.Object>::.ctor(T,System.Nullable`1<System.Int32>,UniRx.IScheduler)
extern "C"  void RepeatObservable_1__ctor_m2410854551_gshared (RepeatObservable_1_t4255784085 * __this, Il2CppObject * ___value0, Nullable_1_t1438485399  ___repeatCount1, Il2CppObject * ___scheduler2, const MethodInfo* method);
#define RepeatObservable_1__ctor_m2410854551(__this, ___value0, ___repeatCount1, ___scheduler2, method) ((  void (*) (RepeatObservable_1_t4255784085 *, Il2CppObject *, Nullable_1_t1438485399 , Il2CppObject *, const MethodInfo*))RepeatObservable_1__ctor_m2410854551_gshared)(__this, ___value0, ___repeatCount1, ___scheduler2, method)
// System.IDisposable UniRx.Operators.RepeatObservable`1<System.Object>::SubscribeCore(UniRx.IObserver`1<T>,System.IDisposable)
extern "C"  Il2CppObject * RepeatObservable_1_SubscribeCore_m2947464967_gshared (RepeatObservable_1_t4255784085 * __this, Il2CppObject* ___observer0, Il2CppObject * ___cancel1, const MethodInfo* method);
#define RepeatObservable_1_SubscribeCore_m2947464967(__this, ___observer0, ___cancel1, method) ((  Il2CppObject * (*) (RepeatObservable_1_t4255784085 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))RepeatObservable_1_SubscribeCore_m2947464967_gshared)(__this, ___observer0, ___cancel1, method)
