﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.InternalUtil.EmptyObserver`1<UniRx.DictionaryAddEvent`2<System.Object,System.Object>>
struct EmptyObserver_1_t3955301294;
// System.Exception
struct Exception_t1967233988;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Exception1967233988.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_DictionaryAddEv250981008.h"

// System.Void UniRx.InternalUtil.EmptyObserver`1<UniRx.DictionaryAddEvent`2<System.Object,System.Object>>::.ctor()
extern "C"  void EmptyObserver_1__ctor_m2504281707_gshared (EmptyObserver_1_t3955301294 * __this, const MethodInfo* method);
#define EmptyObserver_1__ctor_m2504281707(__this, method) ((  void (*) (EmptyObserver_1_t3955301294 *, const MethodInfo*))EmptyObserver_1__ctor_m2504281707_gshared)(__this, method)
// System.Void UniRx.InternalUtil.EmptyObserver`1<UniRx.DictionaryAddEvent`2<System.Object,System.Object>>::.cctor()
extern "C"  void EmptyObserver_1__cctor_m4136192674_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define EmptyObserver_1__cctor_m4136192674(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))EmptyObserver_1__cctor_m4136192674_gshared)(__this /* static, unused */, method)
// System.Void UniRx.InternalUtil.EmptyObserver`1<UniRx.DictionaryAddEvent`2<System.Object,System.Object>>::OnCompleted()
extern "C"  void EmptyObserver_1_OnCompleted_m1165477301_gshared (EmptyObserver_1_t3955301294 * __this, const MethodInfo* method);
#define EmptyObserver_1_OnCompleted_m1165477301(__this, method) ((  void (*) (EmptyObserver_1_t3955301294 *, const MethodInfo*))EmptyObserver_1_OnCompleted_m1165477301_gshared)(__this, method)
// System.Void UniRx.InternalUtil.EmptyObserver`1<UniRx.DictionaryAddEvent`2<System.Object,System.Object>>::OnError(System.Exception)
extern "C"  void EmptyObserver_1_OnError_m1581682530_gshared (EmptyObserver_1_t3955301294 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method);
#define EmptyObserver_1_OnError_m1581682530(__this, ___error0, method) ((  void (*) (EmptyObserver_1_t3955301294 *, Exception_t1967233988 *, const MethodInfo*))EmptyObserver_1_OnError_m1581682530_gshared)(__this, ___error0, method)
// System.Void UniRx.InternalUtil.EmptyObserver`1<UniRx.DictionaryAddEvent`2<System.Object,System.Object>>::OnNext(T)
extern "C"  void EmptyObserver_1_OnNext_m4263737939_gshared (EmptyObserver_1_t3955301294 * __this, DictionaryAddEvent_2_t250981008  ___value0, const MethodInfo* method);
#define EmptyObserver_1_OnNext_m4263737939(__this, ___value0, method) ((  void (*) (EmptyObserver_1_t3955301294 *, DictionaryAddEvent_2_t250981008 , const MethodInfo*))EmptyObserver_1_OnNext_m4263737939_gshared)(__this, ___value0, method)
