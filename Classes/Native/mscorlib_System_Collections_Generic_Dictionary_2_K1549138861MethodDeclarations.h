﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2<System.Object,PlayFab.UUnit.Region>
struct Dictionary_2_t1782110920;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_K1549138861.h"

// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,PlayFab.UUnit.Region>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void Enumerator__ctor_m1337481775_gshared (Enumerator_t1549138861 * __this, Dictionary_2_t1782110920 * ___host0, const MethodInfo* method);
#define Enumerator__ctor_m1337481775(__this, ___host0, method) ((  void (*) (Enumerator_t1549138861 *, Dictionary_2_t1782110920 *, const MethodInfo*))Enumerator__ctor_m1337481775_gshared)(__this, ___host0, method)
// System.Object System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,PlayFab.UUnit.Region>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m561253852_gshared (Enumerator_t1549138861 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m561253852(__this, method) ((  Il2CppObject * (*) (Enumerator_t1549138861 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m561253852_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,PlayFab.UUnit.Region>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m2763544358_gshared (Enumerator_t1549138861 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_Reset_m2763544358(__this, method) ((  void (*) (Enumerator_t1549138861 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m2763544358_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,PlayFab.UUnit.Region>::Dispose()
extern "C"  void Enumerator_Dispose_m2951879313_gshared (Enumerator_t1549138861 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m2951879313(__this, method) ((  void (*) (Enumerator_t1549138861 *, const MethodInfo*))Enumerator_Dispose_m2951879313_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,PlayFab.UUnit.Region>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m1845023830_gshared (Enumerator_t1549138861 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m1845023830(__this, method) ((  bool (*) (Enumerator_t1549138861 *, const MethodInfo*))Enumerator_MoveNext_m1845023830_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,PlayFab.UUnit.Region>::get_Current()
extern "C"  Il2CppObject * Enumerator_get_Current_m3435270402_gshared (Enumerator_t1549138861 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m3435270402(__this, method) ((  Il2CppObject * (*) (Enumerator_t1549138861 *, const MethodInfo*))Enumerator_get_Current_m3435270402_gshared)(__this, method)
