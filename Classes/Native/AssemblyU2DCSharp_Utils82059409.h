﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.AudioClip
struct AudioClip_t3714538611;

#include "mscorlib_System_Object837106420.h"
#include "UnityEngine_UnityEngine_Vector23525329788.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utils
struct  Utils_t82059409  : public Il2CppObject
{
public:

public:
};

struct Utils_t82059409_StaticFields
{
public:
	// UnityEngine.Vector2 Utils::defaultScreenSize
	Vector2_t3525329788  ___defaultScreenSize_0;
	// System.Single Utils::screenRatio_
	float ___screenRatio__1;
	// UnityEngine.AudioClip Utils::buttonSound_
	AudioClip_t3714538611 * ___buttonSound__2;

public:
	inline static int32_t get_offset_of_defaultScreenSize_0() { return static_cast<int32_t>(offsetof(Utils_t82059409_StaticFields, ___defaultScreenSize_0)); }
	inline Vector2_t3525329788  get_defaultScreenSize_0() const { return ___defaultScreenSize_0; }
	inline Vector2_t3525329788 * get_address_of_defaultScreenSize_0() { return &___defaultScreenSize_0; }
	inline void set_defaultScreenSize_0(Vector2_t3525329788  value)
	{
		___defaultScreenSize_0 = value;
	}

	inline static int32_t get_offset_of_screenRatio__1() { return static_cast<int32_t>(offsetof(Utils_t82059409_StaticFields, ___screenRatio__1)); }
	inline float get_screenRatio__1() const { return ___screenRatio__1; }
	inline float* get_address_of_screenRatio__1() { return &___screenRatio__1; }
	inline void set_screenRatio__1(float value)
	{
		___screenRatio__1 = value;
	}

	inline static int32_t get_offset_of_buttonSound__2() { return static_cast<int32_t>(offsetof(Utils_t82059409_StaticFields, ___buttonSound__2)); }
	inline AudioClip_t3714538611 * get_buttonSound__2() const { return ___buttonSound__2; }
	inline AudioClip_t3714538611 ** get_address_of_buttonSound__2() { return &___buttonSound__2; }
	inline void set_buttonSound__2(AudioClip_t3714538611 * value)
	{
		___buttonSound__2 = value;
		Il2CppCodeGenWriteBarrier(&___buttonSound__2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
