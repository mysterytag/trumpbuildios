﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UniRx.Operators.CombineLatestObservable`3/CombineLatest<System.Boolean,System.Boolean,System.Object>
struct CombineLatest_t4145300795;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Operators.CombineLatestObservable`3/CombineLatest/RightObserver<System.Boolean,System.Boolean,System.Object>
struct  RightObserver_t2610190823  : public Il2CppObject
{
public:
	// UniRx.Operators.CombineLatestObservable`3/CombineLatest<TLeft,TRight,TResult> UniRx.Operators.CombineLatestObservable`3/CombineLatest/RightObserver::parent
	CombineLatest_t4145300795 * ___parent_0;

public:
	inline static int32_t get_offset_of_parent_0() { return static_cast<int32_t>(offsetof(RightObserver_t2610190823, ___parent_0)); }
	inline CombineLatest_t4145300795 * get_parent_0() const { return ___parent_0; }
	inline CombineLatest_t4145300795 ** get_address_of_parent_0() { return &___parent_0; }
	inline void set_parent_0(CombineLatest_t4145300795 * value)
	{
		___parent_0 = value;
		Il2CppCodeGenWriteBarrier(&___parent_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
