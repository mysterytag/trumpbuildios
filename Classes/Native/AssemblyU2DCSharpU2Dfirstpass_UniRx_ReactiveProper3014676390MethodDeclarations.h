﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.ReactiveProperty`1/ReactivePropertyObserver<System.Byte>
struct ReactivePropertyObserver_t3014676390;
// UniRx.ReactiveProperty`1<System.Byte>
struct ReactiveProperty_1_t3387026859;
// System.Exception
struct Exception_t1967233988;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Exception1967233988.h"

// System.Void UniRx.ReactiveProperty`1/ReactivePropertyObserver<System.Byte>::.ctor(UniRx.ReactiveProperty`1<T>)
extern "C"  void ReactivePropertyObserver__ctor_m2253343211_gshared (ReactivePropertyObserver_t3014676390 * __this, ReactiveProperty_1_t3387026859 * ___parent0, const MethodInfo* method);
#define ReactivePropertyObserver__ctor_m2253343211(__this, ___parent0, method) ((  void (*) (ReactivePropertyObserver_t3014676390 *, ReactiveProperty_1_t3387026859 *, const MethodInfo*))ReactivePropertyObserver__ctor_m2253343211_gshared)(__this, ___parent0, method)
// System.Void UniRx.ReactiveProperty`1/ReactivePropertyObserver<System.Byte>::OnNext(T)
extern "C"  void ReactivePropertyObserver_OnNext_m761947488_gshared (ReactivePropertyObserver_t3014676390 * __this, uint8_t ___value0, const MethodInfo* method);
#define ReactivePropertyObserver_OnNext_m761947488(__this, ___value0, method) ((  void (*) (ReactivePropertyObserver_t3014676390 *, uint8_t, const MethodInfo*))ReactivePropertyObserver_OnNext_m761947488_gshared)(__this, ___value0, method)
// System.Void UniRx.ReactiveProperty`1/ReactivePropertyObserver<System.Byte>::OnError(System.Exception)
extern "C"  void ReactivePropertyObserver_OnError_m1224196207_gshared (ReactivePropertyObserver_t3014676390 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method);
#define ReactivePropertyObserver_OnError_m1224196207(__this, ___error0, method) ((  void (*) (ReactivePropertyObserver_t3014676390 *, Exception_t1967233988 *, const MethodInfo*))ReactivePropertyObserver_OnError_m1224196207_gshared)(__this, ___error0, method)
// System.Void UniRx.ReactiveProperty`1/ReactivePropertyObserver<System.Byte>::OnCompleted()
extern "C"  void ReactivePropertyObserver_OnCompleted_m1376281154_gshared (ReactivePropertyObserver_t3014676390 * __this, const MethodInfo* method);
#define ReactivePropertyObserver_OnCompleted_m1376281154(__this, method) ((  void (*) (ReactivePropertyObserver_t3014676390 *, const MethodInfo*))ReactivePropertyObserver_OnCompleted_m1376281154_gshared)(__this, method)
