﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Scheduler/ThreadPoolScheduler
struct ThreadPoolScheduler_t1931424245;
// System.IDisposable
struct IDisposable_t1628921374;
// System.Action
struct Action_t437523947;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_DateTimeOffset3712260035.h"
#include "System_Core_System_Action437523947.h"
#include "mscorlib_System_TimeSpan763862892.h"

// System.Void UniRx.Scheduler/ThreadPoolScheduler::.ctor()
extern "C"  void ThreadPoolScheduler__ctor_m1229947022 (ThreadPoolScheduler_t1931424245 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.DateTimeOffset UniRx.Scheduler/ThreadPoolScheduler::get_Now()
extern "C"  DateTimeOffset_t3712260035  ThreadPoolScheduler_get_Now_m1913365087 (ThreadPoolScheduler_t1931424245 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IDisposable UniRx.Scheduler/ThreadPoolScheduler::Schedule(System.Action)
extern "C"  Il2CppObject * ThreadPoolScheduler_Schedule_m3734626827 (ThreadPoolScheduler_t1931424245 * __this, Action_t437523947 * ___action0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IDisposable UniRx.Scheduler/ThreadPoolScheduler::Schedule(System.DateTimeOffset,System.Action)
extern "C"  Il2CppObject * ThreadPoolScheduler_Schedule_m2193382570 (ThreadPoolScheduler_t1931424245 * __this, DateTimeOffset_t3712260035  ___dueTime0, Action_t437523947 * ___action1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IDisposable UniRx.Scheduler/ThreadPoolScheduler::Schedule(System.TimeSpan,System.Action)
extern "C"  Il2CppObject * ThreadPoolScheduler_Schedule_m3889883105 (ThreadPoolScheduler_t1931424245 * __this, TimeSpan_t763862892  ___dueTime0, Action_t437523947 * ___action1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IDisposable UniRx.Scheduler/ThreadPoolScheduler::SchedulePeriodic(System.TimeSpan,System.Action)
extern "C"  Il2CppObject * ThreadPoolScheduler_SchedulePeriodic_m13465286 (ThreadPoolScheduler_t1931424245 * __this, TimeSpan_t763862892  ___period0, Action_t437523947 * ___action1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
