﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.SampleFrameObservable`1/SampleFrame<UniRx.Unit>
struct SampleFrame_t3302090534;
// UniRx.Operators.SampleFrameObservable`1<UniRx.Unit>
struct SampleFrameObservable_1_t3056629055;
// UniRx.IObserver`1<UniRx.Unit>
struct IObserver_1_t475317645;
// System.IDisposable
struct IDisposable_t1628921374;
// System.Exception
struct Exception_t1967233988;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Unit2558286038.h"
#include "mscorlib_System_Exception1967233988.h"

// System.Void UniRx.Operators.SampleFrameObservable`1/SampleFrame<UniRx.Unit>::.ctor(UniRx.Operators.SampleFrameObservable`1<T>,UniRx.IObserver`1<T>,System.IDisposable)
extern "C"  void SampleFrame__ctor_m2803382043_gshared (SampleFrame_t3302090534 * __this, SampleFrameObservable_1_t3056629055 * ___parent0, Il2CppObject* ___observer1, Il2CppObject * ___cancel2, const MethodInfo* method);
#define SampleFrame__ctor_m2803382043(__this, ___parent0, ___observer1, ___cancel2, method) ((  void (*) (SampleFrame_t3302090534 *, SampleFrameObservable_1_t3056629055 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))SampleFrame__ctor_m2803382043_gshared)(__this, ___parent0, ___observer1, ___cancel2, method)
// System.IDisposable UniRx.Operators.SampleFrameObservable`1/SampleFrame<UniRx.Unit>::Run()
extern "C"  Il2CppObject * SampleFrame_Run_m3276606657_gshared (SampleFrame_t3302090534 * __this, const MethodInfo* method);
#define SampleFrame_Run_m3276606657(__this, method) ((  Il2CppObject * (*) (SampleFrame_t3302090534 *, const MethodInfo*))SampleFrame_Run_m3276606657_gshared)(__this, method)
// System.Void UniRx.Operators.SampleFrameObservable`1/SampleFrame<UniRx.Unit>::OnNextTick(System.Int64)
extern "C"  void SampleFrame_OnNextTick_m2770647304_gshared (SampleFrame_t3302090534 * __this, int64_t ____0, const MethodInfo* method);
#define SampleFrame_OnNextTick_m2770647304(__this, ____0, method) ((  void (*) (SampleFrame_t3302090534 *, int64_t, const MethodInfo*))SampleFrame_OnNextTick_m2770647304_gshared)(__this, ____0, method)
// System.Void UniRx.Operators.SampleFrameObservable`1/SampleFrame<UniRx.Unit>::OnNext(T)
extern "C"  void SampleFrame_OnNext_m118485445_gshared (SampleFrame_t3302090534 * __this, Unit_t2558286038  ___value0, const MethodInfo* method);
#define SampleFrame_OnNext_m118485445(__this, ___value0, method) ((  void (*) (SampleFrame_t3302090534 *, Unit_t2558286038 , const MethodInfo*))SampleFrame_OnNext_m118485445_gshared)(__this, ___value0, method)
// System.Void UniRx.Operators.SampleFrameObservable`1/SampleFrame<UniRx.Unit>::OnError(System.Exception)
extern "C"  void SampleFrame_OnError_m4043956948_gshared (SampleFrame_t3302090534 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method);
#define SampleFrame_OnError_m4043956948(__this, ___error0, method) ((  void (*) (SampleFrame_t3302090534 *, Exception_t1967233988 *, const MethodInfo*))SampleFrame_OnError_m4043956948_gshared)(__this, ___error0, method)
// System.Void UniRx.Operators.SampleFrameObservable`1/SampleFrame<UniRx.Unit>::OnCompleted()
extern "C"  void SampleFrame_OnCompleted_m2341942311_gshared (SampleFrame_t3302090534 * __this, const MethodInfo* method);
#define SampleFrame_OnCompleted_m2341942311(__this, method) ((  void (*) (SampleFrame_t3302090534 *, const MethodInfo*))SampleFrame_OnCompleted_m2341942311_gshared)(__this, method)
