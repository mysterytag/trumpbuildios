﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.ClientModels.PurchaseItemResult
struct PurchaseItemResult_t2370400545;
// System.Collections.Generic.List`1<PlayFab.ClientModels.ItemInstance>
struct List_1_t1322103281;

#include "codegen/il2cpp-codegen.h"

// System.Void PlayFab.ClientModels.PurchaseItemResult::.ctor()
extern "C"  void PurchaseItemResult__ctor_m2684842428 (PurchaseItemResult_t2370400545 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<PlayFab.ClientModels.ItemInstance> PlayFab.ClientModels.PurchaseItemResult::get_Items()
extern "C"  List_1_t1322103281 * PurchaseItemResult_get_Items_m3967626200 (PurchaseItemResult_t2370400545 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.PurchaseItemResult::set_Items(System.Collections.Generic.List`1<PlayFab.ClientModels.ItemInstance>)
extern "C"  void PurchaseItemResult_set_Items_m2450336039 (PurchaseItemResult_t2370400545 * __this, List_1_t1322103281 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
