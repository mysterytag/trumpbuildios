﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UniRx.IObserver`1<UniRx.DictionaryReplaceEvent`2<System.Object,System.Object>>
struct IObserver_1_t1695958670;
// System.IDisposable
struct IDisposable_t1628921374;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Operators.OperatorObserverBase`2<UniRx.DictionaryReplaceEvent`2<System.Object,System.Object>,UniRx.DictionaryReplaceEvent`2<System.Object,System.Object>>
struct  OperatorObserverBase_2_t154015849  : public Il2CppObject
{
public:
	// UniRx.IObserver`1<TResult> modreq(System.Runtime.CompilerServices.IsVolatile) UniRx.Operators.OperatorObserverBase`2::observer
	Il2CppObject* ___observer_0;
	// System.IDisposable UniRx.Operators.OperatorObserverBase`2::cancel
	Il2CppObject * ___cancel_1;

public:
	inline static int32_t get_offset_of_observer_0() { return static_cast<int32_t>(offsetof(OperatorObserverBase_2_t154015849, ___observer_0)); }
	inline Il2CppObject* get_observer_0() const { return ___observer_0; }
	inline Il2CppObject** get_address_of_observer_0() { return &___observer_0; }
	inline void set_observer_0(Il2CppObject* value)
	{
		___observer_0 = value;
		Il2CppCodeGenWriteBarrier(&___observer_0, value);
	}

	inline static int32_t get_offset_of_cancel_1() { return static_cast<int32_t>(offsetof(OperatorObserverBase_2_t154015849, ___cancel_1)); }
	inline Il2CppObject * get_cancel_1() const { return ___cancel_1; }
	inline Il2CppObject ** get_address_of_cancel_1() { return &___cancel_1; }
	inline void set_cancel_1(Il2CppObject * value)
	{
		___cancel_1 = value;
		Il2CppCodeGenWriteBarrier(&___cancel_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
