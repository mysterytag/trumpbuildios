﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.BufferObservable`1/Buffer<System.Object>
struct Buffer_t492975321;
// UniRx.Operators.BufferObservable`1<System.Object>
struct BufferObservable_1_t574294898;
// UniRx.IObserver`1<System.Collections.Generic.IList`1<System.Object>>
struct IObserver_1_t920630341;
// System.IDisposable
struct IDisposable_t1628921374;
// System.Object
struct Il2CppObject;
// System.Exception
struct Exception_t1967233988;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_Exception1967233988.h"

// System.Void UniRx.Operators.BufferObservable`1/Buffer<System.Object>::.ctor(UniRx.Operators.BufferObservable`1<T>,UniRx.IObserver`1<System.Collections.Generic.IList`1<T>>,System.IDisposable)
extern "C"  void Buffer__ctor_m594510395_gshared (Buffer_t492975321 * __this, BufferObservable_1_t574294898 * ___parent0, Il2CppObject* ___observer1, Il2CppObject * ___cancel2, const MethodInfo* method);
#define Buffer__ctor_m594510395(__this, ___parent0, ___observer1, ___cancel2, method) ((  void (*) (Buffer_t492975321 *, BufferObservable_1_t574294898 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))Buffer__ctor_m594510395_gshared)(__this, ___parent0, ___observer1, ___cancel2, method)
// System.IDisposable UniRx.Operators.BufferObservable`1/Buffer<System.Object>::Run()
extern "C"  Il2CppObject * Buffer_Run_m1469555461_gshared (Buffer_t492975321 * __this, const MethodInfo* method);
#define Buffer_Run_m1469555461(__this, method) ((  Il2CppObject * (*) (Buffer_t492975321 *, const MethodInfo*))Buffer_Run_m1469555461_gshared)(__this, method)
// System.Void UniRx.Operators.BufferObservable`1/Buffer<System.Object>::OnNext(T)
extern "C"  void Buffer_OnNext_m2687212703_gshared (Buffer_t492975321 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Buffer_OnNext_m2687212703(__this, ___value0, method) ((  void (*) (Buffer_t492975321 *, Il2CppObject *, const MethodInfo*))Buffer_OnNext_m2687212703_gshared)(__this, ___value0, method)
// System.Void UniRx.Operators.BufferObservable`1/Buffer<System.Object>::OnError(System.Exception)
extern "C"  void Buffer_OnError_m1916311982_gshared (Buffer_t492975321 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method);
#define Buffer_OnError_m1916311982(__this, ___error0, method) ((  void (*) (Buffer_t492975321 *, Exception_t1967233988 *, const MethodInfo*))Buffer_OnError_m1916311982_gshared)(__this, ___error0, method)
// System.Void UniRx.Operators.BufferObservable`1/Buffer<System.Object>::OnCompleted()
extern "C"  void Buffer_OnCompleted_m2261639681_gshared (Buffer_t492975321 * __this, const MethodInfo* method);
#define Buffer_OnCompleted_m2261639681(__this, method) ((  void (*) (Buffer_t492975321 *, const MethodInfo*))Buffer_OnCompleted_m2261639681_gshared)(__this, method)
