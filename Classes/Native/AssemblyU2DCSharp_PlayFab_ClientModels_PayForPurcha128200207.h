﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// System.Collections.Generic.Dictionary`2<System.String,System.Int32>
struct Dictionary_2_t190145395;

#include "AssemblyU2DCSharp_PlayFab_Internal_PlayFabResultCom379512675.h"
#include "mscorlib_System_Nullable_1_gen4258606580.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PlayFab.ClientModels.PayForPurchaseResult
struct  PayForPurchaseResult_t128200207  : public PlayFabResultCommon_t379512675
{
public:
	// System.String PlayFab.ClientModels.PayForPurchaseResult::<OrderId>k__BackingField
	String_t* ___U3COrderIdU3Ek__BackingField_2;
	// System.Nullable`1<PlayFab.ClientModels.TransactionStatus> PlayFab.ClientModels.PayForPurchaseResult::<Status>k__BackingField
	Nullable_1_t4258606580  ___U3CStatusU3Ek__BackingField_3;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> PlayFab.ClientModels.PayForPurchaseResult::<VCAmount>k__BackingField
	Dictionary_2_t190145395 * ___U3CVCAmountU3Ek__BackingField_4;
	// System.String PlayFab.ClientModels.PayForPurchaseResult::<PurchaseCurrency>k__BackingField
	String_t* ___U3CPurchaseCurrencyU3Ek__BackingField_5;
	// System.UInt32 PlayFab.ClientModels.PayForPurchaseResult::<PurchasePrice>k__BackingField
	uint32_t ___U3CPurchasePriceU3Ek__BackingField_6;
	// System.UInt32 PlayFab.ClientModels.PayForPurchaseResult::<CreditApplied>k__BackingField
	uint32_t ___U3CCreditAppliedU3Ek__BackingField_7;
	// System.String PlayFab.ClientModels.PayForPurchaseResult::<ProviderData>k__BackingField
	String_t* ___U3CProviderDataU3Ek__BackingField_8;
	// System.String PlayFab.ClientModels.PayForPurchaseResult::<PurchaseConfirmationPageURL>k__BackingField
	String_t* ___U3CPurchaseConfirmationPageURLU3Ek__BackingField_9;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> PlayFab.ClientModels.PayForPurchaseResult::<VirtualCurrency>k__BackingField
	Dictionary_2_t190145395 * ___U3CVirtualCurrencyU3Ek__BackingField_10;

public:
	inline static int32_t get_offset_of_U3COrderIdU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(PayForPurchaseResult_t128200207, ___U3COrderIdU3Ek__BackingField_2)); }
	inline String_t* get_U3COrderIdU3Ek__BackingField_2() const { return ___U3COrderIdU3Ek__BackingField_2; }
	inline String_t** get_address_of_U3COrderIdU3Ek__BackingField_2() { return &___U3COrderIdU3Ek__BackingField_2; }
	inline void set_U3COrderIdU3Ek__BackingField_2(String_t* value)
	{
		___U3COrderIdU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier(&___U3COrderIdU3Ek__BackingField_2, value);
	}

	inline static int32_t get_offset_of_U3CStatusU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(PayForPurchaseResult_t128200207, ___U3CStatusU3Ek__BackingField_3)); }
	inline Nullable_1_t4258606580  get_U3CStatusU3Ek__BackingField_3() const { return ___U3CStatusU3Ek__BackingField_3; }
	inline Nullable_1_t4258606580 * get_address_of_U3CStatusU3Ek__BackingField_3() { return &___U3CStatusU3Ek__BackingField_3; }
	inline void set_U3CStatusU3Ek__BackingField_3(Nullable_1_t4258606580  value)
	{
		___U3CStatusU3Ek__BackingField_3 = value;
	}

	inline static int32_t get_offset_of_U3CVCAmountU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(PayForPurchaseResult_t128200207, ___U3CVCAmountU3Ek__BackingField_4)); }
	inline Dictionary_2_t190145395 * get_U3CVCAmountU3Ek__BackingField_4() const { return ___U3CVCAmountU3Ek__BackingField_4; }
	inline Dictionary_2_t190145395 ** get_address_of_U3CVCAmountU3Ek__BackingField_4() { return &___U3CVCAmountU3Ek__BackingField_4; }
	inline void set_U3CVCAmountU3Ek__BackingField_4(Dictionary_2_t190145395 * value)
	{
		___U3CVCAmountU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier(&___U3CVCAmountU3Ek__BackingField_4, value);
	}

	inline static int32_t get_offset_of_U3CPurchaseCurrencyU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(PayForPurchaseResult_t128200207, ___U3CPurchaseCurrencyU3Ek__BackingField_5)); }
	inline String_t* get_U3CPurchaseCurrencyU3Ek__BackingField_5() const { return ___U3CPurchaseCurrencyU3Ek__BackingField_5; }
	inline String_t** get_address_of_U3CPurchaseCurrencyU3Ek__BackingField_5() { return &___U3CPurchaseCurrencyU3Ek__BackingField_5; }
	inline void set_U3CPurchaseCurrencyU3Ek__BackingField_5(String_t* value)
	{
		___U3CPurchaseCurrencyU3Ek__BackingField_5 = value;
		Il2CppCodeGenWriteBarrier(&___U3CPurchaseCurrencyU3Ek__BackingField_5, value);
	}

	inline static int32_t get_offset_of_U3CPurchasePriceU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(PayForPurchaseResult_t128200207, ___U3CPurchasePriceU3Ek__BackingField_6)); }
	inline uint32_t get_U3CPurchasePriceU3Ek__BackingField_6() const { return ___U3CPurchasePriceU3Ek__BackingField_6; }
	inline uint32_t* get_address_of_U3CPurchasePriceU3Ek__BackingField_6() { return &___U3CPurchasePriceU3Ek__BackingField_6; }
	inline void set_U3CPurchasePriceU3Ek__BackingField_6(uint32_t value)
	{
		___U3CPurchasePriceU3Ek__BackingField_6 = value;
	}

	inline static int32_t get_offset_of_U3CCreditAppliedU3Ek__BackingField_7() { return static_cast<int32_t>(offsetof(PayForPurchaseResult_t128200207, ___U3CCreditAppliedU3Ek__BackingField_7)); }
	inline uint32_t get_U3CCreditAppliedU3Ek__BackingField_7() const { return ___U3CCreditAppliedU3Ek__BackingField_7; }
	inline uint32_t* get_address_of_U3CCreditAppliedU3Ek__BackingField_7() { return &___U3CCreditAppliedU3Ek__BackingField_7; }
	inline void set_U3CCreditAppliedU3Ek__BackingField_7(uint32_t value)
	{
		___U3CCreditAppliedU3Ek__BackingField_7 = value;
	}

	inline static int32_t get_offset_of_U3CProviderDataU3Ek__BackingField_8() { return static_cast<int32_t>(offsetof(PayForPurchaseResult_t128200207, ___U3CProviderDataU3Ek__BackingField_8)); }
	inline String_t* get_U3CProviderDataU3Ek__BackingField_8() const { return ___U3CProviderDataU3Ek__BackingField_8; }
	inline String_t** get_address_of_U3CProviderDataU3Ek__BackingField_8() { return &___U3CProviderDataU3Ek__BackingField_8; }
	inline void set_U3CProviderDataU3Ek__BackingField_8(String_t* value)
	{
		___U3CProviderDataU3Ek__BackingField_8 = value;
		Il2CppCodeGenWriteBarrier(&___U3CProviderDataU3Ek__BackingField_8, value);
	}

	inline static int32_t get_offset_of_U3CPurchaseConfirmationPageURLU3Ek__BackingField_9() { return static_cast<int32_t>(offsetof(PayForPurchaseResult_t128200207, ___U3CPurchaseConfirmationPageURLU3Ek__BackingField_9)); }
	inline String_t* get_U3CPurchaseConfirmationPageURLU3Ek__BackingField_9() const { return ___U3CPurchaseConfirmationPageURLU3Ek__BackingField_9; }
	inline String_t** get_address_of_U3CPurchaseConfirmationPageURLU3Ek__BackingField_9() { return &___U3CPurchaseConfirmationPageURLU3Ek__BackingField_9; }
	inline void set_U3CPurchaseConfirmationPageURLU3Ek__BackingField_9(String_t* value)
	{
		___U3CPurchaseConfirmationPageURLU3Ek__BackingField_9 = value;
		Il2CppCodeGenWriteBarrier(&___U3CPurchaseConfirmationPageURLU3Ek__BackingField_9, value);
	}

	inline static int32_t get_offset_of_U3CVirtualCurrencyU3Ek__BackingField_10() { return static_cast<int32_t>(offsetof(PayForPurchaseResult_t128200207, ___U3CVirtualCurrencyU3Ek__BackingField_10)); }
	inline Dictionary_2_t190145395 * get_U3CVirtualCurrencyU3Ek__BackingField_10() const { return ___U3CVirtualCurrencyU3Ek__BackingField_10; }
	inline Dictionary_2_t190145395 ** get_address_of_U3CVirtualCurrencyU3Ek__BackingField_10() { return &___U3CVirtualCurrencyU3Ek__BackingField_10; }
	inline void set_U3CVirtualCurrencyU3Ek__BackingField_10(Dictionary_2_t190145395 * value)
	{
		___U3CVirtualCurrencyU3Ek__BackingField_10 = value;
		Il2CppCodeGenWriteBarrier(&___U3CVirtualCurrencyU3Ek__BackingField_10, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
