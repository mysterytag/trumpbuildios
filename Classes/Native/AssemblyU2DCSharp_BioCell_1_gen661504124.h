﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// IOrganism
struct IOrganism_t2580843579;
// IRoot
struct IRoot_t69970123;
// OnceEmptyStream
struct OnceEmptyStream_t3081939404;
// System.IDisposable
struct IDisposable_t1628921374;

#include "AssemblyU2DCSharp_Cell_1_gen1019204052.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BioCell`1<System.Object>
struct  BioCell_1_t661504124  : public Cell_1_t1019204052
{
public:
	// System.Boolean BioCell`1::wokeUp
	bool ___wokeUp_5;
	// IOrganism BioCell`1::carrier_
	Il2CppObject * ___carrier__6;
	// IRoot BioCell`1::root_
	Il2CppObject * ___root__7;
	// System.Boolean BioCell`1::alive
	bool ___alive_8;
	// OnceEmptyStream BioCell`1::fellAsleep_
	OnceEmptyStream_t3081939404 * ___fellAsleep__9;
	// System.IDisposable BioCell`1::prevToSleepConnection_
	Il2CppObject * ___prevToSleepConnection__10;

public:
	inline static int32_t get_offset_of_wokeUp_5() { return static_cast<int32_t>(offsetof(BioCell_1_t661504124, ___wokeUp_5)); }
	inline bool get_wokeUp_5() const { return ___wokeUp_5; }
	inline bool* get_address_of_wokeUp_5() { return &___wokeUp_5; }
	inline void set_wokeUp_5(bool value)
	{
		___wokeUp_5 = value;
	}

	inline static int32_t get_offset_of_carrier__6() { return static_cast<int32_t>(offsetof(BioCell_1_t661504124, ___carrier__6)); }
	inline Il2CppObject * get_carrier__6() const { return ___carrier__6; }
	inline Il2CppObject ** get_address_of_carrier__6() { return &___carrier__6; }
	inline void set_carrier__6(Il2CppObject * value)
	{
		___carrier__6 = value;
		Il2CppCodeGenWriteBarrier(&___carrier__6, value);
	}

	inline static int32_t get_offset_of_root__7() { return static_cast<int32_t>(offsetof(BioCell_1_t661504124, ___root__7)); }
	inline Il2CppObject * get_root__7() const { return ___root__7; }
	inline Il2CppObject ** get_address_of_root__7() { return &___root__7; }
	inline void set_root__7(Il2CppObject * value)
	{
		___root__7 = value;
		Il2CppCodeGenWriteBarrier(&___root__7, value);
	}

	inline static int32_t get_offset_of_alive_8() { return static_cast<int32_t>(offsetof(BioCell_1_t661504124, ___alive_8)); }
	inline bool get_alive_8() const { return ___alive_8; }
	inline bool* get_address_of_alive_8() { return &___alive_8; }
	inline void set_alive_8(bool value)
	{
		___alive_8 = value;
	}

	inline static int32_t get_offset_of_fellAsleep__9() { return static_cast<int32_t>(offsetof(BioCell_1_t661504124, ___fellAsleep__9)); }
	inline OnceEmptyStream_t3081939404 * get_fellAsleep__9() const { return ___fellAsleep__9; }
	inline OnceEmptyStream_t3081939404 ** get_address_of_fellAsleep__9() { return &___fellAsleep__9; }
	inline void set_fellAsleep__9(OnceEmptyStream_t3081939404 * value)
	{
		___fellAsleep__9 = value;
		Il2CppCodeGenWriteBarrier(&___fellAsleep__9, value);
	}

	inline static int32_t get_offset_of_prevToSleepConnection__10() { return static_cast<int32_t>(offsetof(BioCell_1_t661504124, ___prevToSleepConnection__10)); }
	inline Il2CppObject * get_prevToSleepConnection__10() const { return ___prevToSleepConnection__10; }
	inline Il2CppObject ** get_address_of_prevToSleepConnection__10() { return &___prevToSleepConnection__10; }
	inline void set_prevToSleepConnection__10(Il2CppObject * value)
	{
		___prevToSleepConnection__10 = value;
		Il2CppCodeGenWriteBarrier(&___prevToSleepConnection__10, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
