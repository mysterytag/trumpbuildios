﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Zenject.KeyedFactoryBase`2/<Validate>c__Iterator46<System.Object,System.Object>
struct U3CValidateU3Ec__Iterator46_t3243744428;
// Zenject.ZenjectResolveException
struct ZenjectResolveException_t1201052999;
// System.Object
struct Il2CppObject;
// System.Collections.IEnumerator
struct IEnumerator_t287207039;
// System.Collections.Generic.IEnumerator`1<Zenject.ZenjectResolveException>
struct IEnumerator_1_t2684159447;

#include "codegen/il2cpp-codegen.h"

// System.Void Zenject.KeyedFactoryBase`2/<Validate>c__Iterator46<System.Object,System.Object>::.ctor()
extern "C"  void U3CValidateU3Ec__Iterator46__ctor_m1860271335_gshared (U3CValidateU3Ec__Iterator46_t3243744428 * __this, const MethodInfo* method);
#define U3CValidateU3Ec__Iterator46__ctor_m1860271335(__this, method) ((  void (*) (U3CValidateU3Ec__Iterator46_t3243744428 *, const MethodInfo*))U3CValidateU3Ec__Iterator46__ctor_m1860271335_gshared)(__this, method)
// Zenject.ZenjectResolveException Zenject.KeyedFactoryBase`2/<Validate>c__Iterator46<System.Object,System.Object>::System.Collections.Generic.IEnumerator<Zenject.ZenjectResolveException>.get_Current()
extern "C"  ZenjectResolveException_t1201052999 * U3CValidateU3Ec__Iterator46_System_Collections_Generic_IEnumeratorU3CZenject_ZenjectResolveExceptionU3E_get_Current_m187790640_gshared (U3CValidateU3Ec__Iterator46_t3243744428 * __this, const MethodInfo* method);
#define U3CValidateU3Ec__Iterator46_System_Collections_Generic_IEnumeratorU3CZenject_ZenjectResolveExceptionU3E_get_Current_m187790640(__this, method) ((  ZenjectResolveException_t1201052999 * (*) (U3CValidateU3Ec__Iterator46_t3243744428 *, const MethodInfo*))U3CValidateU3Ec__Iterator46_System_Collections_Generic_IEnumeratorU3CZenject_ZenjectResolveExceptionU3E_get_Current_m187790640_gshared)(__this, method)
// System.Object Zenject.KeyedFactoryBase`2/<Validate>c__Iterator46<System.Object,System.Object>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CValidateU3Ec__Iterator46_System_Collections_IEnumerator_get_Current_m996118623_gshared (U3CValidateU3Ec__Iterator46_t3243744428 * __this, const MethodInfo* method);
#define U3CValidateU3Ec__Iterator46_System_Collections_IEnumerator_get_Current_m996118623(__this, method) ((  Il2CppObject * (*) (U3CValidateU3Ec__Iterator46_t3243744428 *, const MethodInfo*))U3CValidateU3Ec__Iterator46_System_Collections_IEnumerator_get_Current_m996118623_gshared)(__this, method)
// System.Collections.IEnumerator Zenject.KeyedFactoryBase`2/<Validate>c__Iterator46<System.Object,System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * U3CValidateU3Ec__Iterator46_System_Collections_IEnumerable_GetEnumerator_m3628112026_gshared (U3CValidateU3Ec__Iterator46_t3243744428 * __this, const MethodInfo* method);
#define U3CValidateU3Ec__Iterator46_System_Collections_IEnumerable_GetEnumerator_m3628112026(__this, method) ((  Il2CppObject * (*) (U3CValidateU3Ec__Iterator46_t3243744428 *, const MethodInfo*))U3CValidateU3Ec__Iterator46_System_Collections_IEnumerable_GetEnumerator_m3628112026_gshared)(__this, method)
// System.Collections.Generic.IEnumerator`1<Zenject.ZenjectResolveException> Zenject.KeyedFactoryBase`2/<Validate>c__Iterator46<System.Object,System.Object>::System.Collections.Generic.IEnumerable<Zenject.ZenjectResolveException>.GetEnumerator()
extern "C"  Il2CppObject* U3CValidateU3Ec__Iterator46_System_Collections_Generic_IEnumerableU3CZenject_ZenjectResolveExceptionU3E_GetEnumerator_m291295313_gshared (U3CValidateU3Ec__Iterator46_t3243744428 * __this, const MethodInfo* method);
#define U3CValidateU3Ec__Iterator46_System_Collections_Generic_IEnumerableU3CZenject_ZenjectResolveExceptionU3E_GetEnumerator_m291295313(__this, method) ((  Il2CppObject* (*) (U3CValidateU3Ec__Iterator46_t3243744428 *, const MethodInfo*))U3CValidateU3Ec__Iterator46_System_Collections_Generic_IEnumerableU3CZenject_ZenjectResolveExceptionU3E_GetEnumerator_m291295313_gshared)(__this, method)
// System.Boolean Zenject.KeyedFactoryBase`2/<Validate>c__Iterator46<System.Object,System.Object>::MoveNext()
extern "C"  bool U3CValidateU3Ec__Iterator46_MoveNext_m1431100333_gshared (U3CValidateU3Ec__Iterator46_t3243744428 * __this, const MethodInfo* method);
#define U3CValidateU3Ec__Iterator46_MoveNext_m1431100333(__this, method) ((  bool (*) (U3CValidateU3Ec__Iterator46_t3243744428 *, const MethodInfo*))U3CValidateU3Ec__Iterator46_MoveNext_m1431100333_gshared)(__this, method)
// System.Void Zenject.KeyedFactoryBase`2/<Validate>c__Iterator46<System.Object,System.Object>::Dispose()
extern "C"  void U3CValidateU3Ec__Iterator46_Dispose_m912640868_gshared (U3CValidateU3Ec__Iterator46_t3243744428 * __this, const MethodInfo* method);
#define U3CValidateU3Ec__Iterator46_Dispose_m912640868(__this, method) ((  void (*) (U3CValidateU3Ec__Iterator46_t3243744428 *, const MethodInfo*))U3CValidateU3Ec__Iterator46_Dispose_m912640868_gshared)(__this, method)
// System.Void Zenject.KeyedFactoryBase`2/<Validate>c__Iterator46<System.Object,System.Object>::Reset()
extern "C"  void U3CValidateU3Ec__Iterator46_Reset_m3801671572_gshared (U3CValidateU3Ec__Iterator46_t3243744428 * __this, const MethodInfo* method);
#define U3CValidateU3Ec__Iterator46_Reset_m3801671572(__this, method) ((  void (*) (U3CValidateU3Ec__Iterator46_t3243744428 *, const MethodInfo*))U3CValidateU3Ec__Iterator46_Reset_m3801671572_gshared)(__this, method)
