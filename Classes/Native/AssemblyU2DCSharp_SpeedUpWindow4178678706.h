﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Cell`1<System.Single>
struct Cell_1_t1140306653;
// ConnectionCollector
struct ConnectionCollector_t444796719;
// BottomPanel
struct BottomPanel_t1044499065;
// UniRx.Subject`1<UniRx.Unit>
struct Subject_1_t201353362;
// UnityEngine.UI.Text
struct Text_t3286458198;
// Message
struct Message_t2619578343;
// GlobalStat
struct GlobalStat_t1134678199;
// System.Func`3<System.DateTime,System.Int64,<>__AnonType5`2<System.DateTime,System.Int64>>
struct Func_3_t1001475269;

#include "AssemblyU2DCSharp_WindowBase3855465217.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SpeedUpWindow
struct  SpeedUpWindow_t4178678706  : public WindowBase_t3855465217
{
public:
	// Cell`1<System.Single> SpeedUpWindow::Cost
	Cell_1_t1140306653 * ___Cost_6;
	// ConnectionCollector SpeedUpWindow::ConnectionCollector
	ConnectionCollector_t444796719 * ___ConnectionCollector_7;
	// BottomPanel SpeedUpWindow::BottomPanel
	BottomPanel_t1044499065 * ___BottomPanel_8;
	// UniRx.Subject`1<UniRx.Unit> SpeedUpWindow::CloseSubject
	Subject_1_t201353362 * ___CloseSubject_9;
	// UniRx.Subject`1<UniRx.Unit> SpeedUpWindow::Do
	Subject_1_t201353362 * ___Do_10;
	// UnityEngine.UI.Text SpeedUpWindow::SpeedUpCost
	Text_t3286458198 * ___SpeedUpCost_11;
	// Message SpeedUpWindow::Message
	Message_t2619578343 * ___Message_12;
	// GlobalStat SpeedUpWindow::GlobalStat
	GlobalStat_t1134678199 * ___GlobalStat_13;

public:
	inline static int32_t get_offset_of_Cost_6() { return static_cast<int32_t>(offsetof(SpeedUpWindow_t4178678706, ___Cost_6)); }
	inline Cell_1_t1140306653 * get_Cost_6() const { return ___Cost_6; }
	inline Cell_1_t1140306653 ** get_address_of_Cost_6() { return &___Cost_6; }
	inline void set_Cost_6(Cell_1_t1140306653 * value)
	{
		___Cost_6 = value;
		Il2CppCodeGenWriteBarrier(&___Cost_6, value);
	}

	inline static int32_t get_offset_of_ConnectionCollector_7() { return static_cast<int32_t>(offsetof(SpeedUpWindow_t4178678706, ___ConnectionCollector_7)); }
	inline ConnectionCollector_t444796719 * get_ConnectionCollector_7() const { return ___ConnectionCollector_7; }
	inline ConnectionCollector_t444796719 ** get_address_of_ConnectionCollector_7() { return &___ConnectionCollector_7; }
	inline void set_ConnectionCollector_7(ConnectionCollector_t444796719 * value)
	{
		___ConnectionCollector_7 = value;
		Il2CppCodeGenWriteBarrier(&___ConnectionCollector_7, value);
	}

	inline static int32_t get_offset_of_BottomPanel_8() { return static_cast<int32_t>(offsetof(SpeedUpWindow_t4178678706, ___BottomPanel_8)); }
	inline BottomPanel_t1044499065 * get_BottomPanel_8() const { return ___BottomPanel_8; }
	inline BottomPanel_t1044499065 ** get_address_of_BottomPanel_8() { return &___BottomPanel_8; }
	inline void set_BottomPanel_8(BottomPanel_t1044499065 * value)
	{
		___BottomPanel_8 = value;
		Il2CppCodeGenWriteBarrier(&___BottomPanel_8, value);
	}

	inline static int32_t get_offset_of_CloseSubject_9() { return static_cast<int32_t>(offsetof(SpeedUpWindow_t4178678706, ___CloseSubject_9)); }
	inline Subject_1_t201353362 * get_CloseSubject_9() const { return ___CloseSubject_9; }
	inline Subject_1_t201353362 ** get_address_of_CloseSubject_9() { return &___CloseSubject_9; }
	inline void set_CloseSubject_9(Subject_1_t201353362 * value)
	{
		___CloseSubject_9 = value;
		Il2CppCodeGenWriteBarrier(&___CloseSubject_9, value);
	}

	inline static int32_t get_offset_of_Do_10() { return static_cast<int32_t>(offsetof(SpeedUpWindow_t4178678706, ___Do_10)); }
	inline Subject_1_t201353362 * get_Do_10() const { return ___Do_10; }
	inline Subject_1_t201353362 ** get_address_of_Do_10() { return &___Do_10; }
	inline void set_Do_10(Subject_1_t201353362 * value)
	{
		___Do_10 = value;
		Il2CppCodeGenWriteBarrier(&___Do_10, value);
	}

	inline static int32_t get_offset_of_SpeedUpCost_11() { return static_cast<int32_t>(offsetof(SpeedUpWindow_t4178678706, ___SpeedUpCost_11)); }
	inline Text_t3286458198 * get_SpeedUpCost_11() const { return ___SpeedUpCost_11; }
	inline Text_t3286458198 ** get_address_of_SpeedUpCost_11() { return &___SpeedUpCost_11; }
	inline void set_SpeedUpCost_11(Text_t3286458198 * value)
	{
		___SpeedUpCost_11 = value;
		Il2CppCodeGenWriteBarrier(&___SpeedUpCost_11, value);
	}

	inline static int32_t get_offset_of_Message_12() { return static_cast<int32_t>(offsetof(SpeedUpWindow_t4178678706, ___Message_12)); }
	inline Message_t2619578343 * get_Message_12() const { return ___Message_12; }
	inline Message_t2619578343 ** get_address_of_Message_12() { return &___Message_12; }
	inline void set_Message_12(Message_t2619578343 * value)
	{
		___Message_12 = value;
		Il2CppCodeGenWriteBarrier(&___Message_12, value);
	}

	inline static int32_t get_offset_of_GlobalStat_13() { return static_cast<int32_t>(offsetof(SpeedUpWindow_t4178678706, ___GlobalStat_13)); }
	inline GlobalStat_t1134678199 * get_GlobalStat_13() const { return ___GlobalStat_13; }
	inline GlobalStat_t1134678199 ** get_address_of_GlobalStat_13() { return &___GlobalStat_13; }
	inline void set_GlobalStat_13(GlobalStat_t1134678199 * value)
	{
		___GlobalStat_13 = value;
		Il2CppCodeGenWriteBarrier(&___GlobalStat_13, value);
	}
};

struct SpeedUpWindow_t4178678706_StaticFields
{
public:
	// System.Func`3<System.DateTime,System.Int64,<>__AnonType5`2<System.DateTime,System.Int64>> SpeedUpWindow::<>f__am$cache8
	Func_3_t1001475269 * ___U3CU3Ef__amU24cache8_14;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache8_14() { return static_cast<int32_t>(offsetof(SpeedUpWindow_t4178678706_StaticFields, ___U3CU3Ef__amU24cache8_14)); }
	inline Func_3_t1001475269 * get_U3CU3Ef__amU24cache8_14() const { return ___U3CU3Ef__amU24cache8_14; }
	inline Func_3_t1001475269 ** get_address_of_U3CU3Ef__amU24cache8_14() { return &___U3CU3Ef__amU24cache8_14; }
	inline void set_U3CU3Ef__amU24cache8_14(Func_3_t1001475269 * value)
	{
		___U3CU3Ef__amU24cache8_14 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache8_14, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
