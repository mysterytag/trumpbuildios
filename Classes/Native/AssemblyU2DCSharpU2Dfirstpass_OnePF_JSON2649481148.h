﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Generic.Dictionary`2<System.String,System.Object>
struct Dictionary_2_t2474804324;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// OnePF.JSON
struct  JSON_t2649481148  : public Il2CppObject
{
public:
	// System.Collections.Generic.Dictionary`2<System.String,System.Object> OnePF.JSON::fields
	Dictionary_2_t2474804324 * ___fields_0;

public:
	inline static int32_t get_offset_of_fields_0() { return static_cast<int32_t>(offsetof(JSON_t2649481148, ___fields_0)); }
	inline Dictionary_2_t2474804324 * get_fields_0() const { return ___fields_0; }
	inline Dictionary_2_t2474804324 ** get_address_of_fields_0() { return &___fields_0; }
	inline void set_fields_0(Dictionary_2_t2474804324 * value)
	{
		___fields_0 = value;
		Il2CppCodeGenWriteBarrier(&___fields_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
