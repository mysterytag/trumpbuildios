﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.ObserveOnObservable`1<UniRx.Unit>
struct ObserveOnObservable_1_t3243387575;
// UniRx.IObservable`1<UniRx.Unit>
struct IObservable_1_t2317084402;
// UniRx.IScheduler
struct IScheduler_t2938318244;
// System.IDisposable
struct IDisposable_t1628921374;
// UniRx.IObserver`1<UniRx.Unit>
struct IObserver_1_t475317645;

#include "codegen/il2cpp-codegen.h"

// System.Void UniRx.Operators.ObserveOnObservable`1<UniRx.Unit>::.ctor(UniRx.IObservable`1<T>,UniRx.IScheduler)
extern "C"  void ObserveOnObservable_1__ctor_m410358756_gshared (ObserveOnObservable_1_t3243387575 * __this, Il2CppObject* ___source0, Il2CppObject * ___scheduler1, const MethodInfo* method);
#define ObserveOnObservable_1__ctor_m410358756(__this, ___source0, ___scheduler1, method) ((  void (*) (ObserveOnObservable_1_t3243387575 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))ObserveOnObservable_1__ctor_m410358756_gshared)(__this, ___source0, ___scheduler1, method)
// System.IDisposable UniRx.Operators.ObserveOnObservable`1<UniRx.Unit>::SubscribeCore(UniRx.IObserver`1<T>,System.IDisposable)
extern "C"  Il2CppObject * ObserveOnObservable_1_SubscribeCore_m2620755791_gshared (ObserveOnObservable_1_t3243387575 * __this, Il2CppObject* ___observer0, Il2CppObject * ___cancel1, const MethodInfo* method);
#define ObserveOnObservable_1_SubscribeCore_m2620755791(__this, ___observer0, ___cancel1, method) ((  Il2CppObject * (*) (ObserveOnObservable_1_t3243387575 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))ObserveOnObservable_1_SubscribeCore_m2620755791_gshared)(__this, ___observer0, ___cancel1, method)
