﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Func`3<System.Action`1<System.Int32>,Priority,System.IDisposable>
struct Func_3_t722536535;
// System.Func`1<System.Int32>
struct Func_1_t3990196034;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AnonymousCell`1<System.Int32>
struct  AnonymousCell_1_t2113901288  : public Il2CppObject
{
public:
	// System.Func`3<System.Action`1<T>,Priority,System.IDisposable> AnonymousCell`1::listen
	Func_3_t722536535 * ___listen_0;
	// System.Func`1<T> AnonymousCell`1::current
	Func_1_t3990196034 * ___current_1;

public:
	inline static int32_t get_offset_of_listen_0() { return static_cast<int32_t>(offsetof(AnonymousCell_1_t2113901288, ___listen_0)); }
	inline Func_3_t722536535 * get_listen_0() const { return ___listen_0; }
	inline Func_3_t722536535 ** get_address_of_listen_0() { return &___listen_0; }
	inline void set_listen_0(Func_3_t722536535 * value)
	{
		___listen_0 = value;
		Il2CppCodeGenWriteBarrier(&___listen_0, value);
	}

	inline static int32_t get_offset_of_current_1() { return static_cast<int32_t>(offsetof(AnonymousCell_1_t2113901288, ___current_1)); }
	inline Func_1_t3990196034 * get_current_1() const { return ___current_1; }
	inline Func_1_t3990196034 ** get_address_of_current_1() { return &___current_1; }
	inline void set_current_1(Func_1_t3990196034 * value)
	{
		___current_1 = value;
		Il2CppCodeGenWriteBarrier(&___current_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
