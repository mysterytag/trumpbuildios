﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UniRx.IObservable`1<UniRx.Unit>
struct IObservable_1_t2317084402;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Observable/<SelectMany>c__AnonStorey49`2<System.Object,UniRx.Unit>
struct  U3CSelectManyU3Ec__AnonStorey49_2_t4141348958  : public Il2CppObject
{
public:
	// UniRx.IObservable`1<TR> UniRx.Observable/<SelectMany>c__AnonStorey49`2::other
	Il2CppObject* ___other_0;

public:
	inline static int32_t get_offset_of_other_0() { return static_cast<int32_t>(offsetof(U3CSelectManyU3Ec__AnonStorey49_2_t4141348958, ___other_0)); }
	inline Il2CppObject* get_other_0() const { return ___other_0; }
	inline Il2CppObject** get_address_of_other_0() { return &___other_0; }
	inline void set_other_0(Il2CppObject* value)
	{
		___other_0 = value;
		Il2CppCodeGenWriteBarrier(&___other_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
