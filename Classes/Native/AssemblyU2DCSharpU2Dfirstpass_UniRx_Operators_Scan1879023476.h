﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UniRx.IObservable`1<System.Object>
struct IObservable_1_t595904784;
// System.Object
struct Il2CppObject;
// System.Func`3<System.Object,System.Object,System.Object>
struct Func_3_t1892209229;

#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Oper4196218687.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Operators.ScanObservable`2<System.Object,System.Object>
struct  ScanObservable_2_t1879023476  : public OperatorObservableBase_1_t4196218687
{
public:
	// UniRx.IObservable`1<TSource> UniRx.Operators.ScanObservable`2::source
	Il2CppObject* ___source_1;
	// TAccumulate UniRx.Operators.ScanObservable`2::seed
	Il2CppObject * ___seed_2;
	// System.Func`3<TAccumulate,TSource,TAccumulate> UniRx.Operators.ScanObservable`2::accumulator
	Func_3_t1892209229 * ___accumulator_3;

public:
	inline static int32_t get_offset_of_source_1() { return static_cast<int32_t>(offsetof(ScanObservable_2_t1879023476, ___source_1)); }
	inline Il2CppObject* get_source_1() const { return ___source_1; }
	inline Il2CppObject** get_address_of_source_1() { return &___source_1; }
	inline void set_source_1(Il2CppObject* value)
	{
		___source_1 = value;
		Il2CppCodeGenWriteBarrier(&___source_1, value);
	}

	inline static int32_t get_offset_of_seed_2() { return static_cast<int32_t>(offsetof(ScanObservable_2_t1879023476, ___seed_2)); }
	inline Il2CppObject * get_seed_2() const { return ___seed_2; }
	inline Il2CppObject ** get_address_of_seed_2() { return &___seed_2; }
	inline void set_seed_2(Il2CppObject * value)
	{
		___seed_2 = value;
		Il2CppCodeGenWriteBarrier(&___seed_2, value);
	}

	inline static int32_t get_offset_of_accumulator_3() { return static_cast<int32_t>(offsetof(ScanObservable_2_t1879023476, ___accumulator_3)); }
	inline Func_3_t1892209229 * get_accumulator_3() const { return ___accumulator_3; }
	inline Func_3_t1892209229 ** get_address_of_accumulator_3() { return &___accumulator_3; }
	inline void set_accumulator_3(Func_3_t1892209229 * value)
	{
		___accumulator_3 = value;
		Il2CppCodeGenWriteBarrier(&___accumulator_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
