﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UniRx.Operators.AmbObservable`1<System.Object>
struct AmbObservable_1_t4231346232;
// System.Object
struct Il2CppObject;
// UniRx.SingleAssignmentDisposable
struct SingleAssignmentDisposable_t2336378823;

#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Oper1187768149.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_AmbO2663999068.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Operators.AmbObservable`1/AmbOuterObserver<System.Object>
struct  AmbOuterObserver_t2827192636  : public OperatorObserverBase_2_t1187768149
{
public:
	// UniRx.Operators.AmbObservable`1<T> UniRx.Operators.AmbObservable`1/AmbOuterObserver::parent
	AmbObservable_1_t4231346232 * ___parent_2;
	// System.Object UniRx.Operators.AmbObservable`1/AmbOuterObserver::gate
	Il2CppObject * ___gate_3;
	// UniRx.SingleAssignmentDisposable UniRx.Operators.AmbObservable`1/AmbOuterObserver::leftSubscription
	SingleAssignmentDisposable_t2336378823 * ___leftSubscription_4;
	// UniRx.SingleAssignmentDisposable UniRx.Operators.AmbObservable`1/AmbOuterObserver::rightSubscription
	SingleAssignmentDisposable_t2336378823 * ___rightSubscription_5;
	// UniRx.Operators.AmbObservable`1/AmbOuterObserver/AmbState<T> UniRx.Operators.AmbObservable`1/AmbOuterObserver::choice
	int32_t ___choice_6;

public:
	inline static int32_t get_offset_of_parent_2() { return static_cast<int32_t>(offsetof(AmbOuterObserver_t2827192636, ___parent_2)); }
	inline AmbObservable_1_t4231346232 * get_parent_2() const { return ___parent_2; }
	inline AmbObservable_1_t4231346232 ** get_address_of_parent_2() { return &___parent_2; }
	inline void set_parent_2(AmbObservable_1_t4231346232 * value)
	{
		___parent_2 = value;
		Il2CppCodeGenWriteBarrier(&___parent_2, value);
	}

	inline static int32_t get_offset_of_gate_3() { return static_cast<int32_t>(offsetof(AmbOuterObserver_t2827192636, ___gate_3)); }
	inline Il2CppObject * get_gate_3() const { return ___gate_3; }
	inline Il2CppObject ** get_address_of_gate_3() { return &___gate_3; }
	inline void set_gate_3(Il2CppObject * value)
	{
		___gate_3 = value;
		Il2CppCodeGenWriteBarrier(&___gate_3, value);
	}

	inline static int32_t get_offset_of_leftSubscription_4() { return static_cast<int32_t>(offsetof(AmbOuterObserver_t2827192636, ___leftSubscription_4)); }
	inline SingleAssignmentDisposable_t2336378823 * get_leftSubscription_4() const { return ___leftSubscription_4; }
	inline SingleAssignmentDisposable_t2336378823 ** get_address_of_leftSubscription_4() { return &___leftSubscription_4; }
	inline void set_leftSubscription_4(SingleAssignmentDisposable_t2336378823 * value)
	{
		___leftSubscription_4 = value;
		Il2CppCodeGenWriteBarrier(&___leftSubscription_4, value);
	}

	inline static int32_t get_offset_of_rightSubscription_5() { return static_cast<int32_t>(offsetof(AmbOuterObserver_t2827192636, ___rightSubscription_5)); }
	inline SingleAssignmentDisposable_t2336378823 * get_rightSubscription_5() const { return ___rightSubscription_5; }
	inline SingleAssignmentDisposable_t2336378823 ** get_address_of_rightSubscription_5() { return &___rightSubscription_5; }
	inline void set_rightSubscription_5(SingleAssignmentDisposable_t2336378823 * value)
	{
		___rightSubscription_5 = value;
		Il2CppCodeGenWriteBarrier(&___rightSubscription_5, value);
	}

	inline static int32_t get_offset_of_choice_6() { return static_cast<int32_t>(offsetof(AmbOuterObserver_t2827192636, ___choice_6)); }
	inline int32_t get_choice_6() const { return ___choice_6; }
	inline int32_t* get_address_of_choice_6() { return &___choice_6; }
	inline void set_choice_6(int32_t value)
	{
		___choice_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
