﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Cell`1<System.Int32>
struct Cell_1_t3029512419;
// ICell`1<System.Int32>
struct ICell_1_t104078468;
// System.IDisposable
struct IDisposable_t1628921374;
// System.Action
struct Action_t437523947;
// System.Object
struct Il2CppObject;
// System.Action`1<System.Int32>
struct Action_1_t2995867492;
// IStream`1<System.Int32>
struct IStream_1_t3400105778;
// UnityEngine.UI.Text
struct Text_t3286458198;

#include "codegen/il2cpp-codegen.h"
#include "System_Core_System_Action437523947.h"
#include "AssemblyU2DCSharp_Priority3194150340.h"
#include "UnityEngine_UI_UnityEngine_UI_Text3286458198.h"

// System.Void Cell`1<System.Int32>::.ctor()
extern "C"  void Cell_1__ctor_m3610261621_gshared (Cell_1_t3029512419 * __this, const MethodInfo* method);
#define Cell_1__ctor_m3610261621(__this, method) ((  void (*) (Cell_1_t3029512419 *, const MethodInfo*))Cell_1__ctor_m3610261621_gshared)(__this, method)
// System.Void Cell`1<System.Int32>::.ctor(T)
extern "C"  void Cell_1__ctor_m248961929_gshared (Cell_1_t3029512419 * __this, int32_t ___initial0, const MethodInfo* method);
#define Cell_1__ctor_m248961929(__this, ___initial0, method) ((  void (*) (Cell_1_t3029512419 *, int32_t, const MethodInfo*))Cell_1__ctor_m248961929_gshared)(__this, ___initial0, method)
// System.Void Cell`1<System.Int32>::.ctor(ICell`1<T>)
extern "C"  void Cell_1__ctor_m2220808939_gshared (Cell_1_t3029512419 * __this, Il2CppObject* ___other0, const MethodInfo* method);
#define Cell_1__ctor_m2220808939(__this, ___other0, method) ((  void (*) (Cell_1_t3029512419 *, Il2CppObject*, const MethodInfo*))Cell_1__ctor_m2220808939_gshared)(__this, ___other0, method)
// System.Void Cell`1<System.Int32>::SetInputCell(ICell`1<T>)
extern "C"  void Cell_1_SetInputCell_m3121518895_gshared (Cell_1_t3029512419 * __this, Il2CppObject* ___cell0, const MethodInfo* method);
#define Cell_1_SetInputCell_m3121518895(__this, ___cell0, method) ((  void (*) (Cell_1_t3029512419 *, Il2CppObject*, const MethodInfo*))Cell_1_SetInputCell_m3121518895_gshared)(__this, ___cell0, method)
// T Cell`1<System.Int32>::get_value()
extern "C"  int32_t Cell_1_get_value_m3615970908_gshared (Cell_1_t3029512419 * __this, const MethodInfo* method);
#define Cell_1_get_value_m3615970908(__this, method) ((  int32_t (*) (Cell_1_t3029512419 *, const MethodInfo*))Cell_1_get_value_m3615970908_gshared)(__this, method)
// System.Void Cell`1<System.Int32>::set_value(T)
extern "C"  void Cell_1_set_value_m1442426679_gshared (Cell_1_t3029512419 * __this, int32_t ___value0, const MethodInfo* method);
#define Cell_1_set_value_m1442426679(__this, ___value0, method) ((  void (*) (Cell_1_t3029512419 *, int32_t, const MethodInfo*))Cell_1_set_value_m1442426679_gshared)(__this, ___value0, method)
// System.Void Cell`1<System.Int32>::Set(T)
extern "C"  void Cell_1_Set_m2285895177_gshared (Cell_1_t3029512419 * __this, int32_t ___val0, const MethodInfo* method);
#define Cell_1_Set_m2285895177(__this, ___val0, method) ((  void (*) (Cell_1_t3029512419 *, int32_t, const MethodInfo*))Cell_1_Set_m2285895177_gshared)(__this, ___val0, method)
// System.IDisposable Cell`1<System.Int32>::OnChanged(System.Action,Priority)
extern "C"  Il2CppObject * Cell_1_OnChanged_m1809460954_gshared (Cell_1_t3029512419 * __this, Action_t437523947 * ___action0, int32_t ___p1, const MethodInfo* method);
#define Cell_1_OnChanged_m1809460954(__this, ___action0, ___p1, method) ((  Il2CppObject * (*) (Cell_1_t3029512419 *, Action_t437523947 *, int32_t, const MethodInfo*))Cell_1_OnChanged_m1809460954_gshared)(__this, ___action0, ___p1, method)
// System.Object Cell`1<System.Int32>::get_valueObject()
extern "C"  Il2CppObject * Cell_1_get_valueObject_m67557317_gshared (Cell_1_t3029512419 * __this, const MethodInfo* method);
#define Cell_1_get_valueObject_m67557317(__this, method) ((  Il2CppObject * (*) (Cell_1_t3029512419 *, const MethodInfo*))Cell_1_get_valueObject_m67557317_gshared)(__this, method)
// System.IDisposable Cell`1<System.Int32>::ListenUpdates(System.Action`1<T>,Priority)
extern "C"  Il2CppObject * Cell_1_ListenUpdates_m3364826943_gshared (Cell_1_t3029512419 * __this, Action_1_t2995867492 * ___reaction0, int32_t ___p1, const MethodInfo* method);
#define Cell_1_ListenUpdates_m3364826943(__this, ___reaction0, ___p1, method) ((  Il2CppObject * (*) (Cell_1_t3029512419 *, Action_1_t2995867492 *, int32_t, const MethodInfo*))Cell_1_ListenUpdates_m3364826943_gshared)(__this, ___reaction0, ___p1, method)
// IStream`1<T> Cell`1<System.Int32>::get_updates()
extern "C"  Il2CppObject* Cell_1_get_updates_m1128332835_gshared (Cell_1_t3029512419 * __this, const MethodInfo* method);
#define Cell_1_get_updates_m1128332835(__this, method) ((  Il2CppObject* (*) (Cell_1_t3029512419 *, const MethodInfo*))Cell_1_get_updates_m1128332835_gshared)(__this, method)
// System.IDisposable Cell`1<System.Int32>::Bind(System.Action`1<T>,Priority)
extern "C"  Il2CppObject * Cell_1_Bind_m1240558627_gshared (Cell_1_t3029512419 * __this, Action_1_t2995867492 * ___action0, int32_t ___p1, const MethodInfo* method);
#define Cell_1_Bind_m1240558627(__this, ___action0, ___p1, method) ((  Il2CppObject * (*) (Cell_1_t3029512419 *, Action_1_t2995867492 *, int32_t, const MethodInfo*))Cell_1_Bind_m1240558627_gshared)(__this, ___action0, ___p1, method)
// System.Void Cell`1<System.Int32>::SetInputStream(IStream`1<T>)
extern "C"  void Cell_1_SetInputStream_m3986842283_gshared (Cell_1_t3029512419 * __this, Il2CppObject* ___stream0, const MethodInfo* method);
#define Cell_1_SetInputStream_m3986842283(__this, ___stream0, method) ((  void (*) (Cell_1_t3029512419 *, Il2CppObject*, const MethodInfo*))Cell_1_SetInputStream_m3986842283_gshared)(__this, ___stream0, method)
// System.Void Cell`1<System.Int32>::ResetInputStream()
extern "C"  void Cell_1_ResetInputStream_m161278346_gshared (Cell_1_t3029512419 * __this, const MethodInfo* method);
#define Cell_1_ResetInputStream_m161278346(__this, method) ((  void (*) (Cell_1_t3029512419 *, const MethodInfo*))Cell_1_ResetInputStream_m161278346_gshared)(__this, method)
// System.Void Cell`1<System.Int32>::TransactionIterationFinished()
extern "C"  void Cell_1_TransactionIterationFinished_m2537065248_gshared (Cell_1_t3029512419 * __this, const MethodInfo* method);
#define Cell_1_TransactionIterationFinished_m2537065248(__this, method) ((  void (*) (Cell_1_t3029512419 *, const MethodInfo*))Cell_1_TransactionIterationFinished_m2537065248_gshared)(__this, method)
// System.Void Cell`1<System.Int32>::Unpack(System.Int32)
extern "C"  void Cell_1_Unpack_m1849483826_gshared (Cell_1_t3029512419 * __this, int32_t ___p0, const MethodInfo* method);
#define Cell_1_Unpack_m1849483826(__this, ___p0, method) ((  void (*) (Cell_1_t3029512419 *, int32_t, const MethodInfo*))Cell_1_Unpack_m1849483826_gshared)(__this, ___p0, method)
// System.Boolean Cell`1<System.Int32>::op_LessThanOrEqual(Cell`1<T>,UnityEngine.UI.Text)
extern "C"  bool Cell_1_op_LessThanOrEqual_m204013695_gshared (Il2CppObject * __this /* static, unused */, Cell_1_t3029512419 * ___cell0, Text_t3286458198 * ___text1, const MethodInfo* method);
#define Cell_1_op_LessThanOrEqual_m204013695(__this /* static, unused */, ___cell0, ___text1, method) ((  bool (*) (Il2CppObject * /* static, unused */, Cell_1_t3029512419 *, Text_t3286458198 *, const MethodInfo*))Cell_1_op_LessThanOrEqual_m204013695_gshared)(__this /* static, unused */, ___cell0, ___text1, method)
// System.Boolean Cell`1<System.Int32>::op_GreaterThanOrEqual(Cell`1<T>,UnityEngine.UI.Text)
extern "C"  bool Cell_1_op_GreaterThanOrEqual_m475462056_gshared (Il2CppObject * __this /* static, unused */, Cell_1_t3029512419 * ___cell0, Text_t3286458198 * ___text1, const MethodInfo* method);
#define Cell_1_op_GreaterThanOrEqual_m475462056(__this /* static, unused */, ___cell0, ___text1, method) ((  bool (*) (Il2CppObject * /* static, unused */, Cell_1_t3029512419 *, Text_t3286458198 *, const MethodInfo*))Cell_1_op_GreaterThanOrEqual_m475462056_gshared)(__this /* static, unused */, ___cell0, ___text1, method)
