﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.ClientModels.CharacterLeaderboardEntry
struct CharacterLeaderboardEntry_t3117535278;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String968488902.h"

// System.Void PlayFab.ClientModels.CharacterLeaderboardEntry::.ctor()
extern "C"  void CharacterLeaderboardEntry__ctor_m2362660795 (CharacterLeaderboardEntry_t3117535278 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PlayFab.ClientModels.CharacterLeaderboardEntry::get_PlayFabId()
extern "C"  String_t* CharacterLeaderboardEntry_get_PlayFabId_m1462673179 (CharacterLeaderboardEntry_t3117535278 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.CharacterLeaderboardEntry::set_PlayFabId(System.String)
extern "C"  void CharacterLeaderboardEntry_set_PlayFabId_m3756258968 (CharacterLeaderboardEntry_t3117535278 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PlayFab.ClientModels.CharacterLeaderboardEntry::get_CharacterId()
extern "C"  String_t* CharacterLeaderboardEntry_get_CharacterId_m3092928785 (CharacterLeaderboardEntry_t3117535278 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.CharacterLeaderboardEntry::set_CharacterId(System.String)
extern "C"  void CharacterLeaderboardEntry_set_CharacterId_m3273862178 (CharacterLeaderboardEntry_t3117535278 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PlayFab.ClientModels.CharacterLeaderboardEntry::get_CharacterName()
extern "C"  String_t* CharacterLeaderboardEntry_get_CharacterName_m329683265 (CharacterLeaderboardEntry_t3117535278 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.CharacterLeaderboardEntry::set_CharacterName(System.String)
extern "C"  void CharacterLeaderboardEntry_set_CharacterName_m576283058 (CharacterLeaderboardEntry_t3117535278 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PlayFab.ClientModels.CharacterLeaderboardEntry::get_DisplayName()
extern "C"  String_t* CharacterLeaderboardEntry_get_DisplayName_m2912920346 (CharacterLeaderboardEntry_t3117535278 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.CharacterLeaderboardEntry::set_DisplayName(System.String)
extern "C"  void CharacterLeaderboardEntry_set_DisplayName_m1393703097 (CharacterLeaderboardEntry_t3117535278 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PlayFab.ClientModels.CharacterLeaderboardEntry::get_CharacterType()
extern "C"  String_t* CharacterLeaderboardEntry_get_CharacterType_m523712048 (CharacterLeaderboardEntry_t3117535278 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.CharacterLeaderboardEntry::set_CharacterType(System.String)
extern "C"  void CharacterLeaderboardEntry_set_CharacterType_m1409448227 (CharacterLeaderboardEntry_t3117535278 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 PlayFab.ClientModels.CharacterLeaderboardEntry::get_StatValue()
extern "C"  int32_t CharacterLeaderboardEntry_get_StatValue_m3415689695 (CharacterLeaderboardEntry_t3117535278 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.CharacterLeaderboardEntry::set_StatValue(System.Int32)
extern "C"  void CharacterLeaderboardEntry_set_StatValue_m788977802 (CharacterLeaderboardEntry_t3117535278 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 PlayFab.ClientModels.CharacterLeaderboardEntry::get_Position()
extern "C"  int32_t CharacterLeaderboardEntry_get_Position_m2977898281 (CharacterLeaderboardEntry_t3117535278 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.CharacterLeaderboardEntry::set_Position(System.Int32)
extern "C"  void CharacterLeaderboardEntry_set_Position_m3540732896 (CharacterLeaderboardEntry_t3117535278 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
