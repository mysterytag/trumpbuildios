﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.ClientModels.GetTradeStatusResponse
struct GetTradeStatusResponse_t295324113;
// PlayFab.ClientModels.TradeInfo
struct TradeInfo_t1933812770;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_TradeInfo1933812770.h"

// System.Void PlayFab.ClientModels.GetTradeStatusResponse::.ctor()
extern "C"  void GetTradeStatusResponse__ctor_m149535244 (GetTradeStatusResponse_t295324113 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PlayFab.ClientModels.TradeInfo PlayFab.ClientModels.GetTradeStatusResponse::get_Trade()
extern "C"  TradeInfo_t1933812770 * GetTradeStatusResponse_get_Trade_m3820808398 (GetTradeStatusResponse_t295324113 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.GetTradeStatusResponse::set_Trade(PlayFab.ClientModels.TradeInfo)
extern "C"  void GetTradeStatusResponse_set_Trade_m2966654941 (GetTradeStatusResponse_t295324113 * __this, TradeInfo_t1933812770 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
