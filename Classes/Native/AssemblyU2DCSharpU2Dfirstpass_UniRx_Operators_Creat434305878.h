﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Func`2<UniRx.IObserver`1<UnityEngine.Vector2>,System.IDisposable>
struct Func_2_t1054655943;

#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Oper2589474759.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Operators.CreateObservable`1<UnityEngine.Vector2>
struct  CreateObservable_1_t434305878  : public OperatorObservableBase_1_t2589474759
{
public:
	// System.Func`2<UniRx.IObserver`1<T>,System.IDisposable> UniRx.Operators.CreateObservable`1::subscribe
	Func_2_t1054655943 * ___subscribe_1;

public:
	inline static int32_t get_offset_of_subscribe_1() { return static_cast<int32_t>(offsetof(CreateObservable_1_t434305878, ___subscribe_1)); }
	inline Func_2_t1054655943 * get_subscribe_1() const { return ___subscribe_1; }
	inline Func_2_t1054655943 ** get_address_of_subscribe_1() { return &___subscribe_1; }
	inline void set_subscribe_1(Func_2_t1054655943 * value)
	{
		___subscribe_1 = value;
		Il2CppCodeGenWriteBarrier(&___subscribe_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
