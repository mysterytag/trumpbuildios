﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Object
struct Il2CppObject;
// System.String
struct String_t;

#include "mscorlib_System_Exception1967233988.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PlayFab.UUnit.UUnitAssertException
struct  UUnitAssertException_t1600770266  : public Exception_t1967233988
{
public:
	// System.Object PlayFab.UUnit.UUnitAssertException::expected
	Il2CppObject * ___expected_11;
	// System.Object PlayFab.UUnit.UUnitAssertException::received
	Il2CppObject * ___received_12;
	// System.String PlayFab.UUnit.UUnitAssertException::message
	String_t* ___message_13;

public:
	inline static int32_t get_offset_of_expected_11() { return static_cast<int32_t>(offsetof(UUnitAssertException_t1600770266, ___expected_11)); }
	inline Il2CppObject * get_expected_11() const { return ___expected_11; }
	inline Il2CppObject ** get_address_of_expected_11() { return &___expected_11; }
	inline void set_expected_11(Il2CppObject * value)
	{
		___expected_11 = value;
		Il2CppCodeGenWriteBarrier(&___expected_11, value);
	}

	inline static int32_t get_offset_of_received_12() { return static_cast<int32_t>(offsetof(UUnitAssertException_t1600770266, ___received_12)); }
	inline Il2CppObject * get_received_12() const { return ___received_12; }
	inline Il2CppObject ** get_address_of_received_12() { return &___received_12; }
	inline void set_received_12(Il2CppObject * value)
	{
		___received_12 = value;
		Il2CppCodeGenWriteBarrier(&___received_12, value);
	}

	inline static int32_t get_offset_of_message_13() { return static_cast<int32_t>(offsetof(UUnitAssertException_t1600770266, ___message_13)); }
	inline String_t* get_message_13() const { return ___message_13; }
	inline String_t** get_address_of_message_13() { return &___message_13; }
	inline void set_message_13(String_t* value)
	{
		___message_13 = value;
		Il2CppCodeGenWriteBarrier(&___message_13, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
