﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.StableCompositeDisposable/Binary
struct Binary_t1989867553;
// System.IDisposable
struct IDisposable_t1628921374;

#include "codegen/il2cpp-codegen.h"

// System.Void UniRx.StableCompositeDisposable/Binary::.ctor(System.IDisposable,System.IDisposable)
extern "C"  void Binary__ctor_m2566039219 (Binary_t1989867553 * __this, Il2CppObject * ___disposable10, Il2CppObject * ___disposable21, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UniRx.StableCompositeDisposable/Binary::get_IsDisposed()
extern "C"  bool Binary_get_IsDisposed_m1232560133 (Binary_t1989867553 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.StableCompositeDisposable/Binary::Dispose()
extern "C"  void Binary_Dispose_m2749229040 (Binary_t1989867553 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
