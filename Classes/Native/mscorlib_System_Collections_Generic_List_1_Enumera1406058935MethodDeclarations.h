﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera4014815677MethodDeclarations.h"

// System.Void System.Collections.Generic.List`1/Enumerator<PlayFab.ClientModels.TitleNewsItem>::.ctor(System.Collections.Generic.List`1<T>)
#define Enumerator__ctor_m208269747(__this, ___l0, method) ((  void (*) (Enumerator_t1406058935 *, List_1_t3320275943 *, const MethodInfo*))Enumerator__ctor_m1029849669_gshared)(__this, ___l0, method)
// System.Void System.Collections.Generic.List`1/Enumerator<PlayFab.ClientModels.TitleNewsItem>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m3855326911(__this, method) ((  void (*) (Enumerator_t1406058935 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m771996397_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1/Enumerator<PlayFab.ClientModels.TitleNewsItem>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m2997054261(__this, method) ((  Il2CppObject * (*) (Enumerator_t1406058935 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m3561903705_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<PlayFab.ClientModels.TitleNewsItem>::Dispose()
#define Enumerator_Dispose_m4280015960(__this, method) ((  void (*) (Enumerator_t1406058935 *, const MethodInfo*))Enumerator_Dispose_m2904289642_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<PlayFab.ClientModels.TitleNewsItem>::VerifyState()
#define Enumerator_VerifyState_m2212081553(__this, method) ((  void (*) (Enumerator_t1406058935 *, const MethodInfo*))Enumerator_VerifyState_m1522854819_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<PlayFab.ClientModels.TitleNewsItem>::MoveNext()
#define Enumerator_MoveNext_m2349949039(__this, method) ((  bool (*) (Enumerator_t1406058935 *, const MethodInfo*))Enumerator_MoveNext_m302222300_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<PlayFab.ClientModels.TitleNewsItem>::get_Current()
#define Enumerator_get_Current_m2961972458(__this, method) ((  TitleNewsItem_t2523316974 * (*) (Enumerator_t1406058935 *, const MethodInfo*))Enumerator_get_Current_m1113061044_gshared)(__this, method)
