﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.FromCoroutine`1<UniRx.Unit>
struct FromCoroutine_1_t2235349703;
// System.Func`3<UniRx.IObserver`1<UniRx.Unit>,UniRx.CancellationToken,System.Collections.IEnumerator>
struct Func_3_t3458695013;
// System.IDisposable
struct IDisposable_t1628921374;
// UniRx.IObserver`1<UniRx.Unit>
struct IObserver_1_t475317645;

#include "codegen/il2cpp-codegen.h"

// System.Void UniRx.Operators.FromCoroutine`1<UniRx.Unit>::.ctor(System.Func`3<UniRx.IObserver`1<T>,UniRx.CancellationToken,System.Collections.IEnumerator>)
extern "C"  void FromCoroutine_1__ctor_m4069244102_gshared (FromCoroutine_1_t2235349703 * __this, Func_3_t3458695013 * ___coroutine0, const MethodInfo* method);
#define FromCoroutine_1__ctor_m4069244102(__this, ___coroutine0, method) ((  void (*) (FromCoroutine_1_t2235349703 *, Func_3_t3458695013 *, const MethodInfo*))FromCoroutine_1__ctor_m4069244102_gshared)(__this, ___coroutine0, method)
// System.IDisposable UniRx.Operators.FromCoroutine`1<UniRx.Unit>::SubscribeCore(UniRx.IObserver`1<T>,System.IDisposable)
extern "C"  Il2CppObject * FromCoroutine_1_SubscribeCore_m1650235743_gshared (FromCoroutine_1_t2235349703 * __this, Il2CppObject* ___observer0, Il2CppObject * ___cancel1, const MethodInfo* method);
#define FromCoroutine_1_SubscribeCore_m1650235743(__this, ___observer0, ___cancel1, method) ((  Il2CppObject * (*) (FromCoroutine_1_t2235349703 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))FromCoroutine_1_SubscribeCore_m1650235743_gshared)(__this, ___observer0, ___cancel1, method)
