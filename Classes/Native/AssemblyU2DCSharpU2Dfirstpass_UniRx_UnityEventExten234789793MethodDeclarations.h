﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.UnityEventExtensions/<AsObservable>c__AnonStorey96`1<System.Object>
struct U3CAsObservableU3Ec__AnonStorey96_1_t234789793;
// UnityEngine.Events.UnityAction`1<System.Object>
struct UnityAction_1_t817568325;

#include "codegen/il2cpp-codegen.h"

// System.Void UniRx.UnityEventExtensions/<AsObservable>c__AnonStorey96`1<System.Object>::.ctor()
extern "C"  void U3CAsObservableU3Ec__AnonStorey96_1__ctor_m1757339557_gshared (U3CAsObservableU3Ec__AnonStorey96_1_t234789793 * __this, const MethodInfo* method);
#define U3CAsObservableU3Ec__AnonStorey96_1__ctor_m1757339557(__this, method) ((  void (*) (U3CAsObservableU3Ec__AnonStorey96_1_t234789793 *, const MethodInfo*))U3CAsObservableU3Ec__AnonStorey96_1__ctor_m1757339557_gshared)(__this, method)
// System.Void UniRx.UnityEventExtensions/<AsObservable>c__AnonStorey96`1<System.Object>::<>m__CA(UnityEngine.Events.UnityAction`1<T>)
extern "C"  void U3CAsObservableU3Ec__AnonStorey96_1_U3CU3Em__CA_m225544222_gshared (U3CAsObservableU3Ec__AnonStorey96_1_t234789793 * __this, UnityAction_1_t817568325 * ___h0, const MethodInfo* method);
#define U3CAsObservableU3Ec__AnonStorey96_1_U3CU3Em__CA_m225544222(__this, ___h0, method) ((  void (*) (U3CAsObservableU3Ec__AnonStorey96_1_t234789793 *, UnityAction_1_t817568325 *, const MethodInfo*))U3CAsObservableU3Ec__AnonStorey96_1_U3CU3Em__CA_m225544222_gshared)(__this, ___h0, method)
// System.Void UniRx.UnityEventExtensions/<AsObservable>c__AnonStorey96`1<System.Object>::<>m__CB(UnityEngine.Events.UnityAction`1<T>)
extern "C"  void U3CAsObservableU3Ec__AnonStorey96_1_U3CU3Em__CB_m1858348221_gshared (U3CAsObservableU3Ec__AnonStorey96_1_t234789793 * __this, UnityAction_1_t817568325 * ___h0, const MethodInfo* method);
#define U3CAsObservableU3Ec__AnonStorey96_1_U3CU3Em__CB_m1858348221(__this, ___h0, method) ((  void (*) (U3CAsObservableU3Ec__AnonStorey96_1_t234789793 *, UnityAction_1_t817568325 *, const MethodInfo*))U3CAsObservableU3Ec__AnonStorey96_1_U3CU3Em__CB_m1858348221_gshared)(__this, ___h0, method)
