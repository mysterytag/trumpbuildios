﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

// UniRx.InternalUtil.ListObserver`1<System.Object>
struct ListObserver_1_t1131724091;
// UniRx.InternalUtil.ImmutableList`1<UniRx.IObserver`1<System.Object>>
struct ImmutableList_1_t4109309822;
// System.Exception
struct Exception_t1967233988;
// System.Object
struct Il2CppObject;
// UniRx.IObserver`1<System.Object>
struct IObserver_1_t3049105323;
// UniRx.IObserver`1<System.Object>[]
struct IObserver_1U5BU5D_t3998655818;
// System.Object[]
struct ObjectU5BU5D_t11523773;
// UniRx.InternalUtil.ListObserver`1<System.Single>
struct ListObserver_1_t1252826692;
// UniRx.InternalUtil.ImmutableList`1<UniRx.IObserver`1<System.Single>>
struct ImmutableList_1_t4230412423;
// UniRx.IObserver`1<System.Single>
struct IObserver_1_t3170207924;
// UniRx.IObserver`1<System.Single>[]
struct IObserver_1U5BU5D_t911596029;
// UniRx.InternalUtil.ListObserver`1<TableButtons/StatkaPoTableViev>
struct ListObserver_1_t3803013488;
// UniRx.InternalUtil.ImmutableList`1<UniRx.IObserver`1<TableButtons/StatkaPoTableViev>>
struct ImmutableList_1_t2485631923;
// UniRx.IObserver`1<TableButtons/StatkaPoTableViev>
struct IObserver_1_t1425427424;
// UniRx.IObserver`1<TableButtons/StatkaPoTableViev>[]
struct IObserver_1U5BU5D_t594978977;
// UniRx.InternalUtil.ListObserver`1<UniRx.CollectionAddEvent`1<System.Object>>
struct ListObserver_1_t2711139658;
// UniRx.InternalUtil.ImmutableList`1<UniRx.IObserver`1<UniRx.CollectionAddEvent`1<System.Object>>>
struct ImmutableList_1_t1393758093;
// UniRx.IObserver`1<UniRx.CollectionAddEvent`1<System.Object>>
struct IObserver_1_t333553594;
// UniRx.IObserver`1<UniRx.CollectionAddEvent`1<System.Object>>[]
struct IObserver_1U5BU5D_t482039839;
// UniRx.InternalUtil.ListObserver`1<UniRx.CollectionAddEvent`1<UniRx.Tuple`2<System.Object,System.Object>>>
struct ListObserver_1_t2243295057;
// UniRx.InternalUtil.ImmutableList`1<UniRx.IObserver`1<UniRx.CollectionAddEvent`1<UniRx.Tuple`2<System.Object,System.Object>>>>
struct ImmutableList_1_t925913492;
// UniRx.IObserver`1<UniRx.CollectionAddEvent`1<UniRx.Tuple`2<System.Object,System.Object>>>
struct IObserver_1_t4160676289;
// UniRx.IObserver`1<UniRx.CollectionAddEvent`1<UniRx.Tuple`2<System.Object,System.Object>>>[]
struct IObserver_1U5BU5D_t73482780;
// UniRx.InternalUtil.ListObserver`1<UniRx.CollectionMoveEvent`1<System.Object>>
struct ListObserver_1_t3211051518;
// UniRx.InternalUtil.ImmutableList`1<UniRx.IObserver`1<UniRx.CollectionMoveEvent`1<System.Object>>>
struct ImmutableList_1_t1893669953;
// UniRx.IObserver`1<UniRx.CollectionMoveEvent`1<System.Object>>
struct IObserver_1_t833465454;
// UniRx.IObserver`1<UniRx.CollectionMoveEvent`1<System.Object>>[]
struct IObserver_1U5BU5D_t3525058075;
// UniRx.InternalUtil.ListObserver`1<UniRx.CollectionMoveEvent`1<UniRx.Tuple`2<System.Object,System.Object>>>
struct ListObserver_1_t2743206917;
// UniRx.InternalUtil.ImmutableList`1<UniRx.IObserver`1<UniRx.CollectionMoveEvent`1<UniRx.Tuple`2<System.Object,System.Object>>>>
struct ImmutableList_1_t1425825352;
// UniRx.IObserver`1<UniRx.CollectionMoveEvent`1<UniRx.Tuple`2<System.Object,System.Object>>>
struct IObserver_1_t365620853;
// UniRx.IObserver`1<UniRx.CollectionMoveEvent`1<UniRx.Tuple`2<System.Object,System.Object>>>[]
struct IObserver_1U5BU5D_t3116501016;
// UniRx.InternalUtil.ListObserver`1<UniRx.CollectionRemoveEvent`1<System.Object>>
struct ListObserver_1_t537255395;
// UniRx.InternalUtil.ImmutableList`1<UniRx.IObserver`1<UniRx.CollectionRemoveEvent`1<System.Object>>>
struct ImmutableList_1_t3514841126;
// UniRx.IObserver`1<UniRx.CollectionRemoveEvent`1<System.Object>>
struct IObserver_1_t2454636627;
// UniRx.IObserver`1<UniRx.CollectionRemoveEvent`1<System.Object>>[]
struct IObserver_1U5BU5D_t3107448066;
// UniRx.InternalUtil.ListObserver`1<UniRx.CollectionRemoveEvent`1<UniRx.Tuple`2<System.Object,System.Object>>>
struct ListObserver_1_t69410794;
// UniRx.InternalUtil.ImmutableList`1<UniRx.IObserver`1<UniRx.CollectionRemoveEvent`1<UniRx.Tuple`2<System.Object,System.Object>>>>
struct ImmutableList_1_t3046996525;
// UniRx.IObserver`1<UniRx.CollectionRemoveEvent`1<UniRx.Tuple`2<System.Object,System.Object>>>
struct IObserver_1_t1986792026;
// UniRx.IObserver`1<UniRx.CollectionRemoveEvent`1<UniRx.Tuple`2<System.Object,System.Object>>>[]
struct IObserver_1U5BU5D_t2698891007;
// UniRx.InternalUtil.ListObserver`1<UniRx.CollectionReplaceEvent`1<System.Object>>
struct ListObserver_1_t2136368207;
// UniRx.InternalUtil.ImmutableList`1<UniRx.IObserver`1<UniRx.CollectionReplaceEvent`1<System.Object>>>
struct ImmutableList_1_t818986642;
// UniRx.IObserver`1<UniRx.CollectionReplaceEvent`1<System.Object>>
struct IObserver_1_t4053749439;
// UniRx.IObserver`1<UniRx.CollectionReplaceEvent`1<System.Object>>[]
struct IObserver_1U5BU5D_t1302543206;
// UniRx.InternalUtil.ListObserver`1<UniRx.CollectionReplaceEvent`1<UniRx.Tuple`2<System.Object,System.Object>>>
struct ListObserver_1_t1668523606;
// UniRx.InternalUtil.ImmutableList`1<UniRx.IObserver`1<UniRx.CollectionReplaceEvent`1<UniRx.Tuple`2<System.Object,System.Object>>>>
struct ImmutableList_1_t351142041;
// UniRx.IObserver`1<UniRx.CollectionReplaceEvent`1<UniRx.Tuple`2<System.Object,System.Object>>>
struct IObserver_1_t3585904838;
// UniRx.IObserver`1<UniRx.CollectionReplaceEvent`1<UniRx.Tuple`2<System.Object,System.Object>>>[]
struct IObserver_1U5BU5D_t893986147;
// UniRx.InternalUtil.ListObserver`1<UniRx.CountChangedStatus>
struct ListObserver_1_t4065845392;
// UniRx.InternalUtil.ImmutableList`1<UniRx.IObserver`1<UniRx.CountChangedStatus>>
struct ImmutableList_1_t2748463827;
// UniRx.IObserver`1<UniRx.CountChangedStatus>
struct IObserver_1_t1688259328;
// UniRx.IObserver`1<UniRx.CountChangedStatus>[]
struct IObserver_1U5BU5D_t335784193;
// UniRx.InternalUtil.ListObserver`1<UniRx.DictionaryAddEvent`2<System.Object,System.Object>>
struct ListObserver_1_t545598679;
// UniRx.InternalUtil.ImmutableList`1<UniRx.IObserver`1<UniRx.DictionaryAddEvent`2<System.Object,System.Object>>>
struct ImmutableList_1_t3523184410;
// UniRx.IObserver`1<UniRx.DictionaryAddEvent`2<System.Object,System.Object>>
struct IObserver_1_t2462979911;
// UniRx.IObserver`1<UniRx.DictionaryAddEvent`2<System.Object,System.Object>>[]
struct IObserver_1U5BU5D_t2153699262;
// UniRx.InternalUtil.ListObserver`1<UniRx.DictionaryRemoveEvent`2<System.Object,System.Object>>
struct ListObserver_1_t3616905962;
// UniRx.InternalUtil.ImmutableList`1<UniRx.IObserver`1<UniRx.DictionaryRemoveEvent`2<System.Object,System.Object>>>
struct ImmutableList_1_t2299524397;
// UniRx.IObserver`1<UniRx.DictionaryRemoveEvent`2<System.Object,System.Object>>
struct IObserver_1_t1239319898;
// UniRx.IObserver`1<UniRx.DictionaryRemoveEvent`2<System.Object,System.Object>>[]
struct IObserver_1U5BU5D_t329451519;
// UniRx.InternalUtil.ListObserver`1<UniRx.DictionaryReplaceEvent`2<System.Object,System.Object>>
struct ListObserver_1_t4073544734;
// UniRx.InternalUtil.ImmutableList`1<UniRx.IObserver`1<UniRx.DictionaryReplaceEvent`2<System.Object,System.Object>>>
struct ImmutableList_1_t2756163169;
// UniRx.IObserver`1<UniRx.DictionaryReplaceEvent`2<System.Object,System.Object>>
struct IObserver_1_t1695958670;
// UniRx.IObserver`1<UniRx.DictionaryReplaceEvent`2<System.Object,System.Object>>[]
struct IObserver_1U5BU5D_t23082875;
// UniRx.InternalUtil.ListObserver`1<UniRx.Tuple`2<System.Object,System.Int32>>
struct ListObserver_1_t2674187857;
// UniRx.InternalUtil.ImmutableList`1<UniRx.IObserver`1<UniRx.Tuple`2<System.Object,System.Int32>>>
struct ImmutableList_1_t1356806292;
// UniRx.IObserver`1<UniRx.Tuple`2<System.Object,System.Int32>>
struct IObserver_1_t296601793;
// UniRx.IObserver`1<UniRx.Tuple`2<System.Object,System.Int32>>[]
struct IObserver_1U5BU5D_t1093590812;
// UniRx.InternalUtil.ListObserver`1<UniRx.Tuple`2<System.Object,System.Object>>
struct ListObserver_1_t663879490;
// UniRx.InternalUtil.ImmutableList`1<UniRx.IObserver`1<UniRx.Tuple`2<System.Object,System.Object>>>
struct ImmutableList_1_t3641465221;
// UniRx.IObserver`1<UniRx.Tuple`2<System.Object,System.Object>>
struct IObserver_1_t2581260722;
// UniRx.IObserver`1<UniRx.Tuple`2<System.Object,System.Object>>[]
struct IObserver_1U5BU5D_t3590098759;
// UniRx.InternalUtil.ListObserver`1<UniRx.Tuple`2<System.Object,UnityEngine.NetworkMessageInfo>>
struct ListObserver_1_t2401117954;
// UniRx.InternalUtil.ImmutableList`1<UniRx.IObserver`1<UniRx.Tuple`2<System.Object,UnityEngine.NetworkMessageInfo>>>
struct ImmutableList_1_t1083736389;
// UniRx.IObserver`1<UniRx.Tuple`2<System.Object,UnityEngine.NetworkMessageInfo>>
struct IObserver_1_t23531890;
// UniRx.IObserver`1<UniRx.Tuple`2<System.Object,UnityEngine.NetworkMessageInfo>>[]
struct IObserver_1U5BU5D_t3985248391;
// UniRx.InternalUtil.ListObserver`1<UniRx.Unit>
struct ListObserver_1_t2852903709;
// UniRx.InternalUtil.ImmutableList`1<UniRx.IObserver`1<UniRx.Unit>>
struct ImmutableList_1_t1535522144;
// UniRx.IObserver`1<UniRx.Unit>
struct IObserver_1_t475317645;
// UniRx.IObserver`1<UniRx.Unit>[]
struct IObserver_1U5BU5D_t960117152;
// UniRx.InternalUtil.ListObserver`1<UnityEngine.Bounds>
struct ListObserver_1_t3813132649;
// UniRx.InternalUtil.ImmutableList`1<UniRx.IObserver`1<UnityEngine.Bounds>>
struct ImmutableList_1_t2495751084;
// UniRx.IObserver`1<UnityEngine.Bounds>
struct IObserver_1_t1435546585;
// UniRx.IObserver`1<UnityEngine.Bounds>[]
struct IObserver_1U5BU5D_t579021988;
// UniRx.InternalUtil.ListObserver`1<UnityEngine.Color>
struct ListObserver_1_t1882793431;
// UniRx.InternalUtil.ImmutableList`1<UniRx.IObserver`1<UnityEngine.Color>>
struct ImmutableList_1_t565411866;
// UniRx.IObserver`1<UnityEngine.Color>
struct IObserver_1_t3800174663;
// UniRx.IObserver`1<UnityEngine.Color>[]
struct IObserver_1U5BU5D_t3169245886;
// UniRx.InternalUtil.ListObserver`1<UnityEngine.MasterServerEvent>
struct ListObserver_1_t2572425161;
// UniRx.InternalUtil.ImmutableList`1<UniRx.IObserver`1<UnityEngine.MasterServerEvent>>
struct ImmutableList_1_t1255043596;
// UniRx.IObserver`1<UnityEngine.MasterServerEvent>
struct IObserver_1_t194839097;
// UniRx.IObserver`1<UnityEngine.MasterServerEvent>[]
struct IObserver_1U5BU5D_t2992830660;
// UniRx.InternalUtil.ListObserver`1<UnityEngine.NetworkConnectionError>
struct ListObserver_1_t1318423664;
// UniRx.InternalUtil.ImmutableList`1<UniRx.IObserver`1<UnityEngine.NetworkConnectionError>>
struct ImmutableList_1_t1042099;
// UniRx.IObserver`1<UnityEngine.NetworkConnectionError>
struct IObserver_1_t3235804896;
// UniRx.IObserver`1<UnityEngine.NetworkConnectionError>[]
struct IObserver_1U5BU5D_t1548317089;
// UniRx.InternalUtil.ListObserver`1<UnityEngine.NetworkDisconnection>
struct ListObserver_1_t633378066;
// UniRx.InternalUtil.ImmutableList`1<UniRx.IObserver`1<UnityEngine.NetworkDisconnection>>
struct ImmutableList_1_t3610963797;
// UniRx.IObserver`1<UnityEngine.NetworkDisconnection>
struct IObserver_1_t2550759298;
// UniRx.IObserver`1<UnityEngine.NetworkDisconnection>[]
struct IObserver_1U5BU5D_t3485751351;
// UniRx.InternalUtil.ListObserver`1<UnityEngine.NetworkMessageInfo>
struct ListObserver_1_t2868962555;
// UniRx.InternalUtil.ImmutableList`1<UniRx.IObserver`1<UnityEngine.NetworkMessageInfo>>
struct ImmutableList_1_t1551580990;
// UniRx.IObserver`1<UnityEngine.NetworkMessageInfo>
struct IObserver_1_t491376491;
// UniRx.IObserver`1<UnityEngine.NetworkMessageInfo>[]
struct IObserver_1U5BU5D_t98838154;
// UniRx.InternalUtil.ListObserver`1<UnityEngine.NetworkPlayer>
struct ListObserver_1_t1575755043;
// UniRx.InternalUtil.ImmutableList`1<UniRx.IObserver`1<UnityEngine.NetworkPlayer>>
struct ImmutableList_1_t258373478;
// UniRx.IObserver`1<UnityEngine.NetworkPlayer>
struct IObserver_1_t3493136275;
// UniRx.IObserver`1<UnityEngine.NetworkPlayer>[]
struct IObserver_1U5BU5D_t632717506;
// UniRx.InternalUtil.ListObserver`1<UnityEngine.Quaternion>
struct ListObserver_1_t2186333650;
// UniRx.InternalUtil.ImmutableList`1<UniRx.IObserver`1<UnityEngine.Quaternion>>
struct ImmutableList_1_t868952085;
// UniRx.IObserver`1<UnityEngine.Quaternion>
struct IObserver_1_t4103714882;
// UniRx.IObserver`1<UnityEngine.Quaternion>[]
struct IObserver_1U5BU5D_t2928567415;
// UniRx.InternalUtil.ListObserver`1<UnityEngine.Rect>
struct ListObserver_1_t1820046488;
// UniRx.InternalUtil.ImmutableList`1<UniRx.IObserver`1<UnityEngine.Rect>>
struct ImmutableList_1_t502664923;
// UniRx.IObserver`1<UnityEngine.Rect>
struct IObserver_1_t3737427720;
// UniRx.IObserver`1<UnityEngine.Rect>[]
struct IObserver_1U5BU5D_t749149145;
// UniRx.InternalUtil.ListObserver`1<UnityEngine.Vector2>
struct ListObserver_1_t3819947459;
// UniRx.InternalUtil.ImmutableList`1<UniRx.IObserver`1<UnityEngine.Vector2>>
struct ImmutableList_1_t2502565894;
// UniRx.IObserver`1<UnityEngine.Vector2>
struct IObserver_1_t1442361395;
// UniRx.IObserver`1<UnityEngine.Vector2>[]
struct IObserver_1U5BU5D_t2433548706;
// UniRx.InternalUtil.ListObserver`1<UnityEngine.Vector3>
struct ListObserver_1_t3819947460;
// UniRx.InternalUtil.ImmutableList`1<UniRx.IObserver`1<UnityEngine.Vector3>>
struct ImmutableList_1_t2502565895;
// UniRx.IObserver`1<UnityEngine.Vector3>
struct IObserver_1_t1442361396;
// UniRx.IObserver`1<UnityEngine.Vector3>[]
struct IObserver_1U5BU5D_t2919736445;
// UniRx.InternalUtil.ListObserver`1<UnityEngine.Vector4>
struct ListObserver_1_t3819947461;
// UniRx.InternalUtil.ImmutableList`1<UniRx.IObserver`1<UnityEngine.Vector4>>
struct ImmutableList_1_t2502565896;
// UniRx.IObserver`1<UnityEngine.Vector4>
struct IObserver_1_t1442361397;
// UniRx.IObserver`1<UnityEngine.Vector4>[]
struct IObserver_1U5BU5D_t3405924184;
// UniRx.InternalUtil.PriorityQueue`1<System.Object>
struct PriorityQueue_1_t1335382412;
// UniRx.InternalUtil.ThrowObserver`1<System.Int32>
struct ThrowObserver_1_t3499376402;
// UniRx.InternalUtil.ThrowObserver`1<System.Object>
struct ThrowObserver_1_t1489068035;
// UniRx.InternalUtil.ThrowObserver`1<UniRx.Unit>
struct ThrowObserver_1_t3210247653;

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array2840145358.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_L1131724091.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_L1131724091MethodDeclarations.h"
#include "mscorlib_System_Void2779279689.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_I4109309822.h"
#include "mscorlib_System_Object837106420MethodDeclarations.h"
#include "Assembly-CSharp-firstpass_ArrayTypes.h"
#include "mscorlib_System_Int322847414787.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_I4109309822MethodDeclarations.h"
#include "mscorlib_System_Exception1967233988.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_Array2840145358MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_L1252826692.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_L1252826692MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_I4230412423.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_I4230412423MethodDeclarations.h"
#include "mscorlib_System_Single958209021.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_L3803013488.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_L3803013488MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_I2485631923.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_I2485631923MethodDeclarations.h"
#include "AssemblyU2DCSharp_TableButtons_StatkaPoTableViev3508395817.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_L2711139658.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_L2711139658MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_I1393758093.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_I1393758093MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_CollectionAddE2416521987.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_L2243295057.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_L2243295057MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_Im925913492.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_Im925913492MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_CollectionAddE1948677386.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_L3211051518.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_L3211051518MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_I1893669953.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_I1893669953MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_CollectionMove2916433847.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_L2743206917.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_L2743206917MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_I1425825352.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_I1425825352MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_CollectionMove2448589246.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_Li537255395.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_Li537255395MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_I3514841126.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_I3514841126MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_CollectionRemov242637724.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_Lis69410794.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_Lis69410794MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_I3046996525.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_I3046996525MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_CollectionRemo4069760419.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_L2136368207.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_L2136368207MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_Im818986642.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_Im818986642MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_CollectionRepl1841750536.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_L1668523606.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_L1668523606MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_Im351142041.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_Im351142041MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_CollectionRepl1373905935.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_L4065845392.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_L4065845392MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_I2748463827.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_I2748463827MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_CountChangedSt3771227721.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_Li545598679.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_Li545598679MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_I3523184410.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_I3523184410MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_DictionaryAddEv250981008.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_L3616905962.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_L3616905962MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_I2299524397.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_I2299524397MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_DictionaryRemo3322288291.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_L4073544734.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_L4073544734MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_I2756163169.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_I2756163169MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_DictionaryRepl3778927063.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_L2674187857.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_L2674187857MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_I1356806292.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_I1356806292MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Tuple_2_gen2379570186.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_Li663879490.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_Li663879490MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_I3641465221.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_I3641465221MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Tuple_2_gen369261819.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_L2401117954.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_L2401117954MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_I1083736389.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_I1083736389MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Tuple_2_gen2106500283.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_L2852903709.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_L2852903709MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_I1535522144.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_I1535522144MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Unit2558286038.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_L3813132649.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_L3813132649MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_I2495751084.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_I2495751084MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Bounds3518514978.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_L1882793431.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_L1882793431MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_Im565411866.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_Im565411866MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Color1588175760.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_L2572425161.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_L2572425161MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_I1255043596.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_I1255043596MethodDeclarations.h"
#include "UnityEngine_UnityEngine_MasterServerEvent2277807490.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_L1318423664.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_L1318423664MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_Immu1042099.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_Immu1042099MethodDeclarations.h"
#include "UnityEngine_UnityEngine_NetworkConnectionError1023805993.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_Li633378066.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_Li633378066MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_I3610963797.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_I3610963797MethodDeclarations.h"
#include "UnityEngine_UnityEngine_NetworkDisconnection338760395.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_L2868962555.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_L2868962555MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_I1551580990.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_I1551580990MethodDeclarations.h"
#include "UnityEngine_UnityEngine_NetworkMessageInfo2574344884.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_L1575755043.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_L1575755043MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_Im258373478.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_Im258373478MethodDeclarations.h"
#include "UnityEngine_UnityEngine_NetworkPlayer1281137372.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_L2186333650.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_L2186333650MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_Im868952085.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_Im868952085MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Quaternion1891715979.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_L1820046488.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_L1820046488MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_Im502664923.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_Im502664923MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Rect1525428817.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_L3819947459.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_L3819947459MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_I2502565894.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_I2502565894MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Vector23525329788.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_L3819947460.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_L3819947460MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_I2502565895.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_I2502565895MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Vector33525329789.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_L3819947461.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_L3819947461MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_I2502565896.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_I2502565896MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Vector43525329790.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_P4248560733.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_P4248560733MethodDeclarations.h"
#include "mscorlib_System_Int642847414882MethodDeclarations.h"
#include "mscorlib_System_Int642847414882.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_P1335382412.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_P1335382412MethodDeclarations.h"
#include "mscorlib_System_Boolean211005341.h"
#include "mscorlib_System_InvalidOperationException2420574324MethodDeclarations.h"
#include "mscorlib_System_String968488902.h"
#include "mscorlib_System_InvalidOperationException2420574324.h"
#include "mscorlib_System_Threading_Interlocked3007803305MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_EqualityCompare271497070MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_EqualityCompare271497070.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_T3499376402.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_T3499376402MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_T1489068035.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_T1489068035MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_T3210247653.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_T3210247653MethodDeclarations.h"

// System.Int32 System.Array::IndexOf<System.Object>(!!0[],!!0)
extern "C"  int32_t Array_IndexOf_TisIl2CppObject_m2661005505_gshared (Il2CppObject * __this /* static, unused */, ObjectU5BU5D_t11523773* p0, Il2CppObject * p1, const MethodInfo* method);
#define Array_IndexOf_TisIl2CppObject_m2661005505(__this /* static, unused */, p0, p1, method) ((  int32_t (*) (Il2CppObject * /* static, unused */, ObjectU5BU5D_t11523773*, Il2CppObject *, const MethodInfo*))Array_IndexOf_TisIl2CppObject_m2661005505_gshared)(__this /* static, unused */, p0, p1, method)
// System.Int32 System.Array::IndexOf<UniRx.IObserver`1<System.Object>>(!!0[],!!0)
#define Array_IndexOf_TisIObserver_1_t3049105323_m1536710520(__this /* static, unused */, p0, p1, method) ((  int32_t (*) (Il2CppObject * /* static, unused */, IObserver_1U5BU5D_t3998655818*, Il2CppObject*, const MethodInfo*))Array_IndexOf_TisIl2CppObject_m2661005505_gshared)(__this /* static, unused */, p0, p1, method)
// System.Int32 System.Array::IndexOf<UniRx.IObserver`1<System.Single>>(!!0[],!!0)
#define Array_IndexOf_TisIObserver_1_t3170207924_m3837930767(__this /* static, unused */, p0, p1, method) ((  int32_t (*) (Il2CppObject * /* static, unused */, IObserver_1U5BU5D_t911596029*, Il2CppObject*, const MethodInfo*))Array_IndexOf_TisIl2CppObject_m2661005505_gshared)(__this /* static, unused */, p0, p1, method)
// System.Int32 System.Array::IndexOf<UniRx.IObserver`1<TableButtons/StatkaPoTableViev>>(!!0[],!!0)
#define Array_IndexOf_TisIObserver_1_t1425427424_m3468575955(__this /* static, unused */, p0, p1, method) ((  int32_t (*) (Il2CppObject * /* static, unused */, IObserver_1U5BU5D_t594978977*, Il2CppObject*, const MethodInfo*))Array_IndexOf_TisIl2CppObject_m2661005505_gshared)(__this /* static, unused */, p0, p1, method)
// System.Int32 System.Array::IndexOf<UniRx.IObserver`1<UniRx.CollectionAddEvent`1<System.Object>>>(!!0[],!!0)
#define Array_IndexOf_TisIObserver_1_t333553594_m2598812506(__this /* static, unused */, p0, p1, method) ((  int32_t (*) (Il2CppObject * /* static, unused */, IObserver_1U5BU5D_t482039839*, Il2CppObject*, const MethodInfo*))Array_IndexOf_TisIl2CppObject_m2661005505_gshared)(__this /* static, unused */, p0, p1, method)
// System.Int32 System.Array::IndexOf<UniRx.IObserver`1<UniRx.CollectionAddEvent`1<UniRx.Tuple`2<System.Object,System.Object>>>>(!!0[],!!0)
#define Array_IndexOf_TisIObserver_1_t4160676289_m3007627974(__this /* static, unused */, p0, p1, method) ((  int32_t (*) (Il2CppObject * /* static, unused */, IObserver_1U5BU5D_t73482780*, Il2CppObject*, const MethodInfo*))Array_IndexOf_TisIl2CppObject_m2661005505_gshared)(__this /* static, unused */, p0, p1, method)
// System.Int32 System.Array::IndexOf<UniRx.IObserver`1<UniRx.CollectionMoveEvent`1<System.Object>>>(!!0[],!!0)
#define Array_IndexOf_TisIObserver_1_t833465454_m2851638504(__this /* static, unused */, p0, p1, method) ((  int32_t (*) (Il2CppObject * /* static, unused */, IObserver_1U5BU5D_t3525058075*, Il2CppObject*, const MethodInfo*))Array_IndexOf_TisIl2CppObject_m2661005505_gshared)(__this /* static, unused */, p0, p1, method)
// System.Int32 System.Array::IndexOf<UniRx.IObserver`1<UniRx.CollectionMoveEvent`1<UniRx.Tuple`2<System.Object,System.Object>>>>(!!0[],!!0)
#define Array_IndexOf_TisIObserver_1_t365620853_m3250045944(__this /* static, unused */, p0, p1, method) ((  int32_t (*) (Il2CppObject * /* static, unused */, IObserver_1U5BU5D_t3116501016*, Il2CppObject*, const MethodInfo*))Array_IndexOf_TisIl2CppObject_m2661005505_gshared)(__this /* static, unused */, p0, p1, method)
// System.Int32 System.Array::IndexOf<UniRx.IObserver`1<UniRx.CollectionRemoveEvent`1<System.Object>>>(!!0[],!!0)
#define Array_IndexOf_TisIObserver_1_t2454636627_m3836369813(__this /* static, unused */, p0, p1, method) ((  int32_t (*) (Il2CppObject * /* static, unused */, IObserver_1U5BU5D_t3107448066*, Il2CppObject*, const MethodInfo*))Array_IndexOf_TisIl2CppObject_m2661005505_gshared)(__this /* static, unused */, p0, p1, method)
// System.Int32 System.Array::IndexOf<UniRx.IObserver`1<UniRx.CollectionRemoveEvent`1<UniRx.Tuple`2<System.Object,System.Object>>>>(!!0[],!!0)
#define Array_IndexOf_TisIObserver_1_t1986792026_m3614486635(__this /* static, unused */, p0, p1, method) ((  int32_t (*) (Il2CppObject * /* static, unused */, IObserver_1U5BU5D_t2698891007*, Il2CppObject*, const MethodInfo*))Array_IndexOf_TisIl2CppObject_m2661005505_gshared)(__this /* static, unused */, p0, p1, method)
// System.Int32 System.Array::IndexOf<UniRx.IObserver`1<UniRx.CollectionReplaceEvent`1<System.Object>>>(!!0[],!!0)
#define Array_IndexOf_TisIObserver_1_t4053749439_m595865575(__this /* static, unused */, p0, p1, method) ((  int32_t (*) (Il2CppObject * /* static, unused */, IObserver_1U5BU5D_t1302543206*, Il2CppObject*, const MethodInfo*))Array_IndexOf_TisIl2CppObject_m2661005505_gshared)(__this /* static, unused */, p0, p1, method)
// System.Int32 System.Array::IndexOf<UniRx.IObserver`1<UniRx.CollectionReplaceEvent`1<UniRx.Tuple`2<System.Object,System.Object>>>>(!!0[],!!0)
#define Array_IndexOf_TisIObserver_1_t3585904838_m145118553(__this /* static, unused */, p0, p1, method) ((  int32_t (*) (Il2CppObject * /* static, unused */, IObserver_1U5BU5D_t893986147*, Il2CppObject*, const MethodInfo*))Array_IndexOf_TisIl2CppObject_m2661005505_gshared)(__this /* static, unused */, p0, p1, method)
// System.Int32 System.Array::IndexOf<UniRx.IObserver`1<UniRx.CountChangedStatus>>(!!0[],!!0)
#define Array_IndexOf_TisIObserver_1_t1688259328_m365520657(__this /* static, unused */, p0, p1, method) ((  int32_t (*) (Il2CppObject * /* static, unused */, IObserver_1U5BU5D_t335784193*, Il2CppObject*, const MethodInfo*))Array_IndexOf_TisIl2CppObject_m2661005505_gshared)(__this /* static, unused */, p0, p1, method)
// System.Int32 System.Array::IndexOf<UniRx.IObserver`1<UniRx.DictionaryAddEvent`2<System.Object,System.Object>>>(!!0[],!!0)
#define Array_IndexOf_TisIObserver_1_t2462979911_m2596344069(__this /* static, unused */, p0, p1, method) ((  int32_t (*) (Il2CppObject * /* static, unused */, IObserver_1U5BU5D_t2153699262*, Il2CppObject*, const MethodInfo*))Array_IndexOf_TisIl2CppObject_m2661005505_gshared)(__this /* static, unused */, p0, p1, method)
// System.Int32 System.Array::IndexOf<UniRx.IObserver`1<UniRx.DictionaryRemoveEvent`2<System.Object,System.Object>>>(!!0[],!!0)
#define Array_IndexOf_TisIObserver_1_t1239319898_m2943115024(__this /* static, unused */, p0, p1, method) ((  int32_t (*) (Il2CppObject * /* static, unused */, IObserver_1U5BU5D_t329451519*, Il2CppObject*, const MethodInfo*))Array_IndexOf_TisIl2CppObject_m2661005505_gshared)(__this /* static, unused */, p0, p1, method)
// System.Int32 System.Array::IndexOf<UniRx.IObserver`1<UniRx.DictionaryReplaceEvent`2<System.Object,System.Object>>>(!!0[],!!0)
#define Array_IndexOf_TisIObserver_1_t1695958670_m413224658(__this /* static, unused */, p0, p1, method) ((  int32_t (*) (Il2CppObject * /* static, unused */, IObserver_1U5BU5D_t23082875*, Il2CppObject*, const MethodInfo*))Array_IndexOf_TisIl2CppObject_m2661005505_gshared)(__this /* static, unused */, p0, p1, method)
// System.Int32 System.Array::IndexOf<UniRx.IObserver`1<UniRx.Tuple`2<System.Object,System.Int32>>>(!!0[],!!0)
#define Array_IndexOf_TisIObserver_1_t296601793_m4079423151(__this /* static, unused */, p0, p1, method) ((  int32_t (*) (Il2CppObject * /* static, unused */, IObserver_1U5BU5D_t1093590812*, Il2CppObject*, const MethodInfo*))Array_IndexOf_TisIl2CppObject_m2661005505_gshared)(__this /* static, unused */, p0, p1, method)
// System.Int32 System.Array::IndexOf<UniRx.IObserver`1<UniRx.Tuple`2<System.Object,System.Object>>>(!!0[],!!0)
#define Array_IndexOf_TisIObserver_1_t2581260722_m4082334424(__this /* static, unused */, p0, p1, method) ((  int32_t (*) (Il2CppObject * /* static, unused */, IObserver_1U5BU5D_t3590098759*, Il2CppObject*, const MethodInfo*))Array_IndexOf_TisIl2CppObject_m2661005505_gshared)(__this /* static, unused */, p0, p1, method)
// System.Int32 System.Array::IndexOf<UniRx.IObserver`1<UniRx.Tuple`2<System.Object,UnityEngine.NetworkMessageInfo>>>(!!0[],!!0)
#define Array_IndexOf_TisIObserver_1_t23531890_m1372874590(__this /* static, unused */, p0, p1, method) ((  int32_t (*) (Il2CppObject * /* static, unused */, IObserver_1U5BU5D_t3985248391*, Il2CppObject*, const MethodInfo*))Array_IndexOf_TisIl2CppObject_m2661005505_gshared)(__this /* static, unused */, p0, p1, method)
// System.Int32 System.Array::IndexOf<UniRx.IObserver`1<UniRx.Unit>>(!!0[],!!0)
#define Array_IndexOf_TisIObserver_1_t475317645_m2265060452(__this /* static, unused */, p0, p1, method) ((  int32_t (*) (Il2CppObject * /* static, unused */, IObserver_1U5BU5D_t960117152*, Il2CppObject*, const MethodInfo*))Array_IndexOf_TisIl2CppObject_m2661005505_gshared)(__this /* static, unused */, p0, p1, method)
// System.Int32 System.Array::IndexOf<UniRx.IObserver`1<UnityEngine.Bounds>>(!!0[],!!0)
#define Array_IndexOf_TisIObserver_1_t1435546585_m1991859220(__this /* static, unused */, p0, p1, method) ((  int32_t (*) (Il2CppObject * /* static, unused */, IObserver_1U5BU5D_t579021988*, Il2CppObject*, const MethodInfo*))Array_IndexOf_TisIl2CppObject_m2661005505_gshared)(__this /* static, unused */, p0, p1, method)
// System.Int32 System.Array::IndexOf<UniRx.IObserver`1<UnityEngine.Color>>(!!0[],!!0)
#define Array_IndexOf_TisIObserver_1_t3800174663_m1933147146(__this /* static, unused */, p0, p1, method) ((  int32_t (*) (Il2CppObject * /* static, unused */, IObserver_1U5BU5D_t3169245886*, Il2CppObject*, const MethodInfo*))Array_IndexOf_TisIl2CppObject_m2661005505_gshared)(__this /* static, unused */, p0, p1, method)
// System.Int32 System.Array::IndexOf<UniRx.IObserver`1<UnityEngine.MasterServerEvent>>(!!0[],!!0)
#define Array_IndexOf_TisIObserver_1_t194839097_m4184500312(__this /* static, unused */, p0, p1, method) ((  int32_t (*) (Il2CppObject * /* static, unused */, IObserver_1U5BU5D_t2992830660*, Il2CppObject*, const MethodInfo*))Array_IndexOf_TisIl2CppObject_m2661005505_gshared)(__this /* static, unused */, p0, p1, method)
// System.Int32 System.Array::IndexOf<UniRx.IObserver`1<UnityEngine.NetworkConnectionError>>(!!0[],!!0)
#define Array_IndexOf_TisIObserver_1_t3235804896_m2509152365(__this /* static, unused */, p0, p1, method) ((  int32_t (*) (Il2CppObject * /* static, unused */, IObserver_1U5BU5D_t1548317089*, Il2CppObject*, const MethodInfo*))Array_IndexOf_TisIl2CppObject_m2661005505_gshared)(__this /* static, unused */, p0, p1, method)
// System.Int32 System.Array::IndexOf<UniRx.IObserver`1<UnityEngine.NetworkDisconnection>>(!!0[],!!0)
#define Array_IndexOf_TisIObserver_1_t2550759298_m3449776779(__this /* static, unused */, p0, p1, method) ((  int32_t (*) (Il2CppObject * /* static, unused */, IObserver_1U5BU5D_t3485751351*, Il2CppObject*, const MethodInfo*))Array_IndexOf_TisIl2CppObject_m2661005505_gshared)(__this /* static, unused */, p0, p1, method)
// System.Int32 System.Array::IndexOf<UniRx.IObserver`1<UnityEngine.NetworkMessageInfo>>(!!0[],!!0)
#define Array_IndexOf_TisIObserver_1_t491376491_m2218924226(__this /* static, unused */, p0, p1, method) ((  int32_t (*) (Il2CppObject * /* static, unused */, IObserver_1U5BU5D_t98838154*, Il2CppObject*, const MethodInfo*))Array_IndexOf_TisIl2CppObject_m2661005505_gshared)(__this /* static, unused */, p0, p1, method)
// System.Int32 System.Array::IndexOf<UniRx.IObserver`1<UnityEngine.NetworkPlayer>>(!!0[],!!0)
#define Array_IndexOf_TisIObserver_1_t3493136275_m3611143742(__this /* static, unused */, p0, p1, method) ((  int32_t (*) (Il2CppObject * /* static, unused */, IObserver_1U5BU5D_t632717506*, Il2CppObject*, const MethodInfo*))Array_IndexOf_TisIl2CppObject_m2661005505_gshared)(__this /* static, unused */, p0, p1, method)
// System.Int32 System.Array::IndexOf<UniRx.IObserver`1<UnityEngine.Quaternion>>(!!0[],!!0)
#define Array_IndexOf_TisIObserver_1_t4103714882_m3823153995(__this /* static, unused */, p0, p1, method) ((  int32_t (*) (Il2CppObject * /* static, unused */, IObserver_1U5BU5D_t2928567415*, Il2CppObject*, const MethodInfo*))Array_IndexOf_TisIl2CppObject_m2661005505_gshared)(__this /* static, unused */, p0, p1, method)
// System.Int32 System.Array::IndexOf<UniRx.IObserver`1<UnityEngine.Rect>>(!!0[],!!0)
#define Array_IndexOf_TisIObserver_1_t3737427720_m4191129797(__this /* static, unused */, p0, p1, method) ((  int32_t (*) (Il2CppObject * /* static, unused */, IObserver_1U5BU5D_t749149145*, Il2CppObject*, const MethodInfo*))Array_IndexOf_TisIl2CppObject_m2661005505_gshared)(__this /* static, unused */, p0, p1, method)
// System.Int32 System.Array::IndexOf<UniRx.IObserver`1<UnityEngine.Vector2>>(!!0[],!!0)
#define Array_IndexOf_TisIObserver_1_t1442361395_m3326600990(__this /* static, unused */, p0, p1, method) ((  int32_t (*) (Il2CppObject * /* static, unused */, IObserver_1U5BU5D_t2433548706*, Il2CppObject*, const MethodInfo*))Array_IndexOf_TisIl2CppObject_m2661005505_gshared)(__this /* static, unused */, p0, p1, method)
// System.Int32 System.Array::IndexOf<UniRx.IObserver`1<UnityEngine.Vector3>>(!!0[],!!0)
#define Array_IndexOf_TisIObserver_1_t1442361396_m2821042365(__this /* static, unused */, p0, p1, method) ((  int32_t (*) (Il2CppObject * /* static, unused */, IObserver_1U5BU5D_t2919736445*, Il2CppObject*, const MethodInfo*))Array_IndexOf_TisIl2CppObject_m2661005505_gshared)(__this /* static, unused */, p0, p1, method)
// System.Int32 System.Array::IndexOf<UniRx.IObserver`1<UnityEngine.Vector4>>(!!0[],!!0)
#define Array_IndexOf_TisIObserver_1_t1442361397_m2315483740(__this /* static, unused */, p0, p1, method) ((  int32_t (*) (Il2CppObject * /* static, unused */, IObserver_1U5BU5D_t3405924184*, Il2CppObject*, const MethodInfo*))Array_IndexOf_TisIl2CppObject_m2661005505_gshared)(__this /* static, unused */, p0, p1, method)
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void UniRx.InternalUtil.ListObserver`1<System.Object>::.ctor(UniRx.InternalUtil.ImmutableList`1<UniRx.IObserver`1<T>>)
extern "C"  void ListObserver_1__ctor_m1171659873_gshared (ListObserver_1_t1131724091 * __this, ImmutableList_1_t4109309822 * ___observers0, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		ImmutableList_1_t4109309822 * L_0 = ___observers0;
		__this->set__observers_0(L_0);
		return;
	}
}
// System.Void UniRx.InternalUtil.ListObserver`1<System.Object>::OnCompleted()
extern "C"  void ListObserver_1_OnCompleted_m1312264409_gshared (ListObserver_1_t1131724091 * __this, const MethodInfo* method)
{
	IObserver_1U5BU5D_t3998655818* V_0 = NULL;
	int32_t V_1 = 0;
	{
		ImmutableList_1_t4109309822 * L_0 = (ImmutableList_1_t4109309822 *)__this->get__observers_0();
		NullCheck((ImmutableList_1_t4109309822 *)L_0);
		IObserver_1U5BU5D_t3998655818* L_1 = ((  IObserver_1U5BU5D_t3998655818* (*) (ImmutableList_1_t4109309822 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((ImmutableList_1_t4109309822 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		V_0 = (IObserver_1U5BU5D_t3998655818*)L_1;
		V_1 = (int32_t)0;
		goto IL_001f;
	}

IL_0013:
	{
		IObserver_1U5BU5D_t3998655818* L_2 = V_0;
		int32_t L_3 = V_1;
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, L_3);
		int32_t L_4 = L_3;
		NullCheck((Il2CppObject*)((L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_4))));
		InterfaceActionInvoker0::Invoke(0 /* System.Void UniRx.IObserver`1<System.Object>::OnCompleted() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), (Il2CppObject*)((L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_4))));
		int32_t L_5 = V_1;
		V_1 = (int32_t)((int32_t)((int32_t)L_5+(int32_t)1));
	}

IL_001f:
	{
		int32_t L_6 = V_1;
		IObserver_1U5BU5D_t3998655818* L_7 = V_0;
		NullCheck(L_7);
		if ((((int32_t)L_6) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_7)->max_length)))))))
		{
			goto IL_0013;
		}
	}
	{
		return;
	}
}
// System.Void UniRx.InternalUtil.ListObserver`1<System.Object>::OnError(System.Exception)
extern "C"  void ListObserver_1_OnError_m2971321478_gshared (ListObserver_1_t1131724091 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method)
{
	IObserver_1U5BU5D_t3998655818* V_0 = NULL;
	int32_t V_1 = 0;
	{
		ImmutableList_1_t4109309822 * L_0 = (ImmutableList_1_t4109309822 *)__this->get__observers_0();
		NullCheck((ImmutableList_1_t4109309822 *)L_0);
		IObserver_1U5BU5D_t3998655818* L_1 = ((  IObserver_1U5BU5D_t3998655818* (*) (ImmutableList_1_t4109309822 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((ImmutableList_1_t4109309822 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		V_0 = (IObserver_1U5BU5D_t3998655818*)L_1;
		V_1 = (int32_t)0;
		goto IL_0020;
	}

IL_0013:
	{
		IObserver_1U5BU5D_t3998655818* L_2 = V_0;
		int32_t L_3 = V_1;
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, L_3);
		int32_t L_4 = L_3;
		Exception_t1967233988 * L_5 = ___error0;
		NullCheck((Il2CppObject*)((L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_4))));
		InterfaceActionInvoker1< Exception_t1967233988 * >::Invoke(1 /* System.Void UniRx.IObserver`1<System.Object>::OnError(System.Exception) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), (Il2CppObject*)((L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_4))), (Exception_t1967233988 *)L_5);
		int32_t L_6 = V_1;
		V_1 = (int32_t)((int32_t)((int32_t)L_6+(int32_t)1));
	}

IL_0020:
	{
		int32_t L_7 = V_1;
		IObserver_1U5BU5D_t3998655818* L_8 = V_0;
		NullCheck(L_8);
		if ((((int32_t)L_7) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_8)->max_length)))))))
		{
			goto IL_0013;
		}
	}
	{
		return;
	}
}
// System.Void UniRx.InternalUtil.ListObserver`1<System.Object>::OnNext(T)
extern "C"  void ListObserver_1_OnNext_m1823373175_gshared (ListObserver_1_t1131724091 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	IObserver_1U5BU5D_t3998655818* V_0 = NULL;
	int32_t V_1 = 0;
	{
		ImmutableList_1_t4109309822 * L_0 = (ImmutableList_1_t4109309822 *)__this->get__observers_0();
		NullCheck((ImmutableList_1_t4109309822 *)L_0);
		IObserver_1U5BU5D_t3998655818* L_1 = ((  IObserver_1U5BU5D_t3998655818* (*) (ImmutableList_1_t4109309822 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((ImmutableList_1_t4109309822 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		V_0 = (IObserver_1U5BU5D_t3998655818*)L_1;
		V_1 = (int32_t)0;
		goto IL_0020;
	}

IL_0013:
	{
		IObserver_1U5BU5D_t3998655818* L_2 = V_0;
		int32_t L_3 = V_1;
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, L_3);
		int32_t L_4 = L_3;
		Il2CppObject * L_5 = ___value0;
		NullCheck((Il2CppObject*)((L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_4))));
		InterfaceActionInvoker1< Il2CppObject * >::Invoke(2 /* System.Void UniRx.IObserver`1<System.Object>::OnNext(T) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), (Il2CppObject*)((L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_4))), (Il2CppObject *)L_5);
		int32_t L_6 = V_1;
		V_1 = (int32_t)((int32_t)((int32_t)L_6+(int32_t)1));
	}

IL_0020:
	{
		int32_t L_7 = V_1;
		IObserver_1U5BU5D_t3998655818* L_8 = V_0;
		NullCheck(L_8);
		if ((((int32_t)L_7) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_8)->max_length)))))))
		{
			goto IL_0013;
		}
	}
	{
		return;
	}
}
// UniRx.IObserver`1<T> UniRx.InternalUtil.ListObserver`1<System.Object>::Add(UniRx.IObserver`1<T>)
extern "C"  Il2CppObject* ListObserver_1_Add_m1317440265_gshared (ListObserver_1_t1131724091 * __this, Il2CppObject* ___observer0, const MethodInfo* method)
{
	{
		ImmutableList_1_t4109309822 * L_0 = (ImmutableList_1_t4109309822 *)__this->get__observers_0();
		Il2CppObject* L_1 = ___observer0;
		NullCheck((ImmutableList_1_t4109309822 *)L_0);
		ImmutableList_1_t4109309822 * L_2 = ((  ImmutableList_1_t4109309822 * (*) (ImmutableList_1_t4109309822 *, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)((ImmutableList_1_t4109309822 *)L_0, (Il2CppObject*)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		ListObserver_1_t1131724091 * L_3 = (ListObserver_1_t1131724091 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		((  void (*) (ListObserver_1_t1131724091 *, ImmutableList_1_t4109309822 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)(L_3, (ImmutableList_1_t4109309822 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		return L_3;
	}
}
// UniRx.IObserver`1<T> UniRx.InternalUtil.ListObserver`1<System.Object>::Remove(UniRx.IObserver`1<T>)
extern "C"  Il2CppObject* ListObserver_1_Remove_m885229474_gshared (ListObserver_1_t1131724091 * __this, Il2CppObject* ___observer0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		ImmutableList_1_t4109309822 * L_0 = (ImmutableList_1_t4109309822 *)__this->get__observers_0();
		NullCheck((ImmutableList_1_t4109309822 *)L_0);
		IObserver_1U5BU5D_t3998655818* L_1 = ((  IObserver_1U5BU5D_t3998655818* (*) (ImmutableList_1_t4109309822 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((ImmutableList_1_t4109309822 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		Il2CppObject* L_2 = ___observer0;
		int32_t L_3 = ((  int32_t (*) (Il2CppObject * /* static, unused */, IObserver_1U5BU5D_t3998655818*, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->method)(NULL /*static, unused*/, (IObserver_1U5BU5D_t3998655818*)L_1, (Il2CppObject*)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		V_0 = (int32_t)L_3;
		int32_t L_4 = V_0;
		if ((((int32_t)L_4) >= ((int32_t)0)))
		{
			goto IL_001b;
		}
	}
	{
		return __this;
	}

IL_001b:
	{
		ImmutableList_1_t4109309822 * L_5 = (ImmutableList_1_t4109309822 *)__this->get__observers_0();
		NullCheck((ImmutableList_1_t4109309822 *)L_5);
		IObserver_1U5BU5D_t3998655818* L_6 = ((  IObserver_1U5BU5D_t3998655818* (*) (ImmutableList_1_t4109309822 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((ImmutableList_1_t4109309822 *)L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		NullCheck(L_6);
		if ((!(((uint32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_6)->max_length))))) == ((uint32_t)2))))
		{
			goto IL_003e;
		}
	}
	{
		ImmutableList_1_t4109309822 * L_7 = (ImmutableList_1_t4109309822 *)__this->get__observers_0();
		NullCheck((ImmutableList_1_t4109309822 *)L_7);
		IObserver_1U5BU5D_t3998655818* L_8 = ((  IObserver_1U5BU5D_t3998655818* (*) (ImmutableList_1_t4109309822 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((ImmutableList_1_t4109309822 *)L_7, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_9 = V_0;
		NullCheck(L_8);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_8, ((int32_t)((int32_t)1-(int32_t)L_9)));
		int32_t L_10 = ((int32_t)((int32_t)1-(int32_t)L_9));
		return ((L_8)->GetAt(static_cast<il2cpp_array_size_t>(L_10)));
	}

IL_003e:
	{
		ImmutableList_1_t4109309822 * L_11 = (ImmutableList_1_t4109309822 *)__this->get__observers_0();
		Il2CppObject* L_12 = ___observer0;
		NullCheck((ImmutableList_1_t4109309822 *)L_11);
		ImmutableList_1_t4109309822 * L_13 = ((  ImmutableList_1_t4109309822 * (*) (ImmutableList_1_t4109309822 *, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6)->method)((ImmutableList_1_t4109309822 *)L_11, (Il2CppObject*)L_12, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		ListObserver_1_t1131724091 * L_14 = (ListObserver_1_t1131724091 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		((  void (*) (ListObserver_1_t1131724091 *, ImmutableList_1_t4109309822 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)(L_14, (ImmutableList_1_t4109309822 *)L_13, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		return L_14;
	}
}
// System.Void UniRx.InternalUtil.ListObserver`1<System.Single>::.ctor(UniRx.InternalUtil.ImmutableList`1<UniRx.IObserver`1<T>>)
extern "C"  void ListObserver_1__ctor_m3940489002_gshared (ListObserver_1_t1252826692 * __this, ImmutableList_1_t4230412423 * ___observers0, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		ImmutableList_1_t4230412423 * L_0 = ___observers0;
		__this->set__observers_0(L_0);
		return;
	}
}
// System.Void UniRx.InternalUtil.ListObserver`1<System.Single>::OnCompleted()
extern "C"  void ListObserver_1_OnCompleted_m696664034_gshared (ListObserver_1_t1252826692 * __this, const MethodInfo* method)
{
	IObserver_1U5BU5D_t911596029* V_0 = NULL;
	int32_t V_1 = 0;
	{
		ImmutableList_1_t4230412423 * L_0 = (ImmutableList_1_t4230412423 *)__this->get__observers_0();
		NullCheck((ImmutableList_1_t4230412423 *)L_0);
		IObserver_1U5BU5D_t911596029* L_1 = ((  IObserver_1U5BU5D_t911596029* (*) (ImmutableList_1_t4230412423 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((ImmutableList_1_t4230412423 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		V_0 = (IObserver_1U5BU5D_t911596029*)L_1;
		V_1 = (int32_t)0;
		goto IL_001f;
	}

IL_0013:
	{
		IObserver_1U5BU5D_t911596029* L_2 = V_0;
		int32_t L_3 = V_1;
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, L_3);
		int32_t L_4 = L_3;
		NullCheck((Il2CppObject*)((L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_4))));
		InterfaceActionInvoker0::Invoke(0 /* System.Void UniRx.IObserver`1<System.Single>::OnCompleted() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), (Il2CppObject*)((L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_4))));
		int32_t L_5 = V_1;
		V_1 = (int32_t)((int32_t)((int32_t)L_5+(int32_t)1));
	}

IL_001f:
	{
		int32_t L_6 = V_1;
		IObserver_1U5BU5D_t911596029* L_7 = V_0;
		NullCheck(L_7);
		if ((((int32_t)L_6) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_7)->max_length)))))))
		{
			goto IL_0013;
		}
	}
	{
		return;
	}
}
// System.Void UniRx.InternalUtil.ListObserver`1<System.Single>::OnError(System.Exception)
extern "C"  void ListObserver_1_OnError_m4218826255_gshared (ListObserver_1_t1252826692 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method)
{
	IObserver_1U5BU5D_t911596029* V_0 = NULL;
	int32_t V_1 = 0;
	{
		ImmutableList_1_t4230412423 * L_0 = (ImmutableList_1_t4230412423 *)__this->get__observers_0();
		NullCheck((ImmutableList_1_t4230412423 *)L_0);
		IObserver_1U5BU5D_t911596029* L_1 = ((  IObserver_1U5BU5D_t911596029* (*) (ImmutableList_1_t4230412423 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((ImmutableList_1_t4230412423 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		V_0 = (IObserver_1U5BU5D_t911596029*)L_1;
		V_1 = (int32_t)0;
		goto IL_0020;
	}

IL_0013:
	{
		IObserver_1U5BU5D_t911596029* L_2 = V_0;
		int32_t L_3 = V_1;
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, L_3);
		int32_t L_4 = L_3;
		Exception_t1967233988 * L_5 = ___error0;
		NullCheck((Il2CppObject*)((L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_4))));
		InterfaceActionInvoker1< Exception_t1967233988 * >::Invoke(1 /* System.Void UniRx.IObserver`1<System.Single>::OnError(System.Exception) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), (Il2CppObject*)((L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_4))), (Exception_t1967233988 *)L_5);
		int32_t L_6 = V_1;
		V_1 = (int32_t)((int32_t)((int32_t)L_6+(int32_t)1));
	}

IL_0020:
	{
		int32_t L_7 = V_1;
		IObserver_1U5BU5D_t911596029* L_8 = V_0;
		NullCheck(L_8);
		if ((((int32_t)L_7) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_8)->max_length)))))))
		{
			goto IL_0013;
		}
	}
	{
		return;
	}
}
// System.Void UniRx.InternalUtil.ListObserver`1<System.Single>::OnNext(T)
extern "C"  void ListObserver_1_OnNext_m3837268736_gshared (ListObserver_1_t1252826692 * __this, float ___value0, const MethodInfo* method)
{
	IObserver_1U5BU5D_t911596029* V_0 = NULL;
	int32_t V_1 = 0;
	{
		ImmutableList_1_t4230412423 * L_0 = (ImmutableList_1_t4230412423 *)__this->get__observers_0();
		NullCheck((ImmutableList_1_t4230412423 *)L_0);
		IObserver_1U5BU5D_t911596029* L_1 = ((  IObserver_1U5BU5D_t911596029* (*) (ImmutableList_1_t4230412423 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((ImmutableList_1_t4230412423 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		V_0 = (IObserver_1U5BU5D_t911596029*)L_1;
		V_1 = (int32_t)0;
		goto IL_0020;
	}

IL_0013:
	{
		IObserver_1U5BU5D_t911596029* L_2 = V_0;
		int32_t L_3 = V_1;
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, L_3);
		int32_t L_4 = L_3;
		float L_5 = ___value0;
		NullCheck((Il2CppObject*)((L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_4))));
		InterfaceActionInvoker1< float >::Invoke(2 /* System.Void UniRx.IObserver`1<System.Single>::OnNext(T) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), (Il2CppObject*)((L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_4))), (float)L_5);
		int32_t L_6 = V_1;
		V_1 = (int32_t)((int32_t)((int32_t)L_6+(int32_t)1));
	}

IL_0020:
	{
		int32_t L_7 = V_1;
		IObserver_1U5BU5D_t911596029* L_8 = V_0;
		NullCheck(L_8);
		if ((((int32_t)L_7) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_8)->max_length)))))))
		{
			goto IL_0013;
		}
	}
	{
		return;
	}
}
// UniRx.IObserver`1<T> UniRx.InternalUtil.ListObserver`1<System.Single>::Add(UniRx.IObserver`1<T>)
extern "C"  Il2CppObject* ListObserver_1_Add_m2564945042_gshared (ListObserver_1_t1252826692 * __this, Il2CppObject* ___observer0, const MethodInfo* method)
{
	{
		ImmutableList_1_t4230412423 * L_0 = (ImmutableList_1_t4230412423 *)__this->get__observers_0();
		Il2CppObject* L_1 = ___observer0;
		NullCheck((ImmutableList_1_t4230412423 *)L_0);
		ImmutableList_1_t4230412423 * L_2 = ((  ImmutableList_1_t4230412423 * (*) (ImmutableList_1_t4230412423 *, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)((ImmutableList_1_t4230412423 *)L_0, (Il2CppObject*)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		ListObserver_1_t1252826692 * L_3 = (ListObserver_1_t1252826692 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		((  void (*) (ListObserver_1_t1252826692 *, ImmutableList_1_t4230412423 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)(L_3, (ImmutableList_1_t4230412423 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		return L_3;
	}
}
// UniRx.IObserver`1<T> UniRx.InternalUtil.ListObserver`1<System.Single>::Remove(UniRx.IObserver`1<T>)
extern "C"  Il2CppObject* ListObserver_1_Remove_m948028793_gshared (ListObserver_1_t1252826692 * __this, Il2CppObject* ___observer0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		ImmutableList_1_t4230412423 * L_0 = (ImmutableList_1_t4230412423 *)__this->get__observers_0();
		NullCheck((ImmutableList_1_t4230412423 *)L_0);
		IObserver_1U5BU5D_t911596029* L_1 = ((  IObserver_1U5BU5D_t911596029* (*) (ImmutableList_1_t4230412423 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((ImmutableList_1_t4230412423 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		Il2CppObject* L_2 = ___observer0;
		int32_t L_3 = ((  int32_t (*) (Il2CppObject * /* static, unused */, IObserver_1U5BU5D_t911596029*, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->method)(NULL /*static, unused*/, (IObserver_1U5BU5D_t911596029*)L_1, (Il2CppObject*)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		V_0 = (int32_t)L_3;
		int32_t L_4 = V_0;
		if ((((int32_t)L_4) >= ((int32_t)0)))
		{
			goto IL_001b;
		}
	}
	{
		return __this;
	}

IL_001b:
	{
		ImmutableList_1_t4230412423 * L_5 = (ImmutableList_1_t4230412423 *)__this->get__observers_0();
		NullCheck((ImmutableList_1_t4230412423 *)L_5);
		IObserver_1U5BU5D_t911596029* L_6 = ((  IObserver_1U5BU5D_t911596029* (*) (ImmutableList_1_t4230412423 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((ImmutableList_1_t4230412423 *)L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		NullCheck(L_6);
		if ((!(((uint32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_6)->max_length))))) == ((uint32_t)2))))
		{
			goto IL_003e;
		}
	}
	{
		ImmutableList_1_t4230412423 * L_7 = (ImmutableList_1_t4230412423 *)__this->get__observers_0();
		NullCheck((ImmutableList_1_t4230412423 *)L_7);
		IObserver_1U5BU5D_t911596029* L_8 = ((  IObserver_1U5BU5D_t911596029* (*) (ImmutableList_1_t4230412423 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((ImmutableList_1_t4230412423 *)L_7, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_9 = V_0;
		NullCheck(L_8);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_8, ((int32_t)((int32_t)1-(int32_t)L_9)));
		int32_t L_10 = ((int32_t)((int32_t)1-(int32_t)L_9));
		return ((L_8)->GetAt(static_cast<il2cpp_array_size_t>(L_10)));
	}

IL_003e:
	{
		ImmutableList_1_t4230412423 * L_11 = (ImmutableList_1_t4230412423 *)__this->get__observers_0();
		Il2CppObject* L_12 = ___observer0;
		NullCheck((ImmutableList_1_t4230412423 *)L_11);
		ImmutableList_1_t4230412423 * L_13 = ((  ImmutableList_1_t4230412423 * (*) (ImmutableList_1_t4230412423 *, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6)->method)((ImmutableList_1_t4230412423 *)L_11, (Il2CppObject*)L_12, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		ListObserver_1_t1252826692 * L_14 = (ListObserver_1_t1252826692 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		((  void (*) (ListObserver_1_t1252826692 *, ImmutableList_1_t4230412423 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)(L_14, (ImmutableList_1_t4230412423 *)L_13, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		return L_14;
	}
}
// System.Void UniRx.InternalUtil.ListObserver`1<TableButtons/StatkaPoTableViev>::.ctor(UniRx.InternalUtil.ImmutableList`1<UniRx.IObserver`1<T>>)
extern "C"  void ListObserver_1__ctor_m620624376_gshared (ListObserver_1_t3803013488 * __this, ImmutableList_1_t2485631923 * ___observers0, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		ImmutableList_1_t2485631923 * L_0 = ___observers0;
		__this->set__observers_0(L_0);
		return;
	}
}
// System.Void UniRx.InternalUtil.ListObserver`1<TableButtons/StatkaPoTableViev>::OnCompleted()
extern "C"  void ListObserver_1_OnCompleted_m4105867824_gshared (ListObserver_1_t3803013488 * __this, const MethodInfo* method)
{
	IObserver_1U5BU5D_t594978977* V_0 = NULL;
	int32_t V_1 = 0;
	{
		ImmutableList_1_t2485631923 * L_0 = (ImmutableList_1_t2485631923 *)__this->get__observers_0();
		NullCheck((ImmutableList_1_t2485631923 *)L_0);
		IObserver_1U5BU5D_t594978977* L_1 = ((  IObserver_1U5BU5D_t594978977* (*) (ImmutableList_1_t2485631923 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((ImmutableList_1_t2485631923 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		V_0 = (IObserver_1U5BU5D_t594978977*)L_1;
		V_1 = (int32_t)0;
		goto IL_001f;
	}

IL_0013:
	{
		IObserver_1U5BU5D_t594978977* L_2 = V_0;
		int32_t L_3 = V_1;
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, L_3);
		int32_t L_4 = L_3;
		NullCheck((Il2CppObject*)((L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_4))));
		InterfaceActionInvoker0::Invoke(0 /* System.Void UniRx.IObserver`1<TableButtons/StatkaPoTableViev>::OnCompleted() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), (Il2CppObject*)((L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_4))));
		int32_t L_5 = V_1;
		V_1 = (int32_t)((int32_t)((int32_t)L_5+(int32_t)1));
	}

IL_001f:
	{
		int32_t L_6 = V_1;
		IObserver_1U5BU5D_t594978977* L_7 = V_0;
		NullCheck(L_7);
		if ((((int32_t)L_6) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_7)->max_length)))))))
		{
			goto IL_0013;
		}
	}
	{
		return;
	}
}
// System.Void UniRx.InternalUtil.ListObserver`1<TableButtons/StatkaPoTableViev>::OnError(System.Exception)
extern "C"  void ListObserver_1_OnError_m1017871197_gshared (ListObserver_1_t3803013488 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method)
{
	IObserver_1U5BU5D_t594978977* V_0 = NULL;
	int32_t V_1 = 0;
	{
		ImmutableList_1_t2485631923 * L_0 = (ImmutableList_1_t2485631923 *)__this->get__observers_0();
		NullCheck((ImmutableList_1_t2485631923 *)L_0);
		IObserver_1U5BU5D_t594978977* L_1 = ((  IObserver_1U5BU5D_t594978977* (*) (ImmutableList_1_t2485631923 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((ImmutableList_1_t2485631923 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		V_0 = (IObserver_1U5BU5D_t594978977*)L_1;
		V_1 = (int32_t)0;
		goto IL_0020;
	}

IL_0013:
	{
		IObserver_1U5BU5D_t594978977* L_2 = V_0;
		int32_t L_3 = V_1;
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, L_3);
		int32_t L_4 = L_3;
		Exception_t1967233988 * L_5 = ___error0;
		NullCheck((Il2CppObject*)((L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_4))));
		InterfaceActionInvoker1< Exception_t1967233988 * >::Invoke(1 /* System.Void UniRx.IObserver`1<TableButtons/StatkaPoTableViev>::OnError(System.Exception) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), (Il2CppObject*)((L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_4))), (Exception_t1967233988 *)L_5);
		int32_t L_6 = V_1;
		V_1 = (int32_t)((int32_t)((int32_t)L_6+(int32_t)1));
	}

IL_0020:
	{
		int32_t L_7 = V_1;
		IObserver_1U5BU5D_t594978977* L_8 = V_0;
		NullCheck(L_8);
		if ((((int32_t)L_7) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_8)->max_length)))))))
		{
			goto IL_0013;
		}
	}
	{
		return;
	}
}
// System.Void UniRx.InternalUtil.ListObserver`1<TableButtons/StatkaPoTableViev>::OnNext(T)
extern "C"  void ListObserver_1_OnNext_m3634216014_gshared (ListObserver_1_t3803013488 * __this, int32_t ___value0, const MethodInfo* method)
{
	IObserver_1U5BU5D_t594978977* V_0 = NULL;
	int32_t V_1 = 0;
	{
		ImmutableList_1_t2485631923 * L_0 = (ImmutableList_1_t2485631923 *)__this->get__observers_0();
		NullCheck((ImmutableList_1_t2485631923 *)L_0);
		IObserver_1U5BU5D_t594978977* L_1 = ((  IObserver_1U5BU5D_t594978977* (*) (ImmutableList_1_t2485631923 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((ImmutableList_1_t2485631923 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		V_0 = (IObserver_1U5BU5D_t594978977*)L_1;
		V_1 = (int32_t)0;
		goto IL_0020;
	}

IL_0013:
	{
		IObserver_1U5BU5D_t594978977* L_2 = V_0;
		int32_t L_3 = V_1;
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, L_3);
		int32_t L_4 = L_3;
		int32_t L_5 = ___value0;
		NullCheck((Il2CppObject*)((L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_4))));
		InterfaceActionInvoker1< int32_t >::Invoke(2 /* System.Void UniRx.IObserver`1<TableButtons/StatkaPoTableViev>::OnNext(T) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), (Il2CppObject*)((L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_4))), (int32_t)L_5);
		int32_t L_6 = V_1;
		V_1 = (int32_t)((int32_t)((int32_t)L_6+(int32_t)1));
	}

IL_0020:
	{
		int32_t L_7 = V_1;
		IObserver_1U5BU5D_t594978977* L_8 = V_0;
		NullCheck(L_8);
		if ((((int32_t)L_7) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_8)->max_length)))))))
		{
			goto IL_0013;
		}
	}
	{
		return;
	}
}
// UniRx.IObserver`1<T> UniRx.InternalUtil.ListObserver`1<TableButtons/StatkaPoTableViev>::Add(UniRx.IObserver`1<T>)
extern "C"  Il2CppObject* ListObserver_1_Add_m1033640302_gshared (ListObserver_1_t3803013488 * __this, Il2CppObject* ___observer0, const MethodInfo* method)
{
	{
		ImmutableList_1_t2485631923 * L_0 = (ImmutableList_1_t2485631923 *)__this->get__observers_0();
		Il2CppObject* L_1 = ___observer0;
		NullCheck((ImmutableList_1_t2485631923 *)L_0);
		ImmutableList_1_t2485631923 * L_2 = ((  ImmutableList_1_t2485631923 * (*) (ImmutableList_1_t2485631923 *, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)((ImmutableList_1_t2485631923 *)L_0, (Il2CppObject*)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		ListObserver_1_t3803013488 * L_3 = (ListObserver_1_t3803013488 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		((  void (*) (ListObserver_1_t3803013488 *, ImmutableList_1_t2485631923 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)(L_3, (ImmutableList_1_t2485631923 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		return L_3;
	}
}
// UniRx.IObserver`1<T> UniRx.InternalUtil.ListObserver`1<TableButtons/StatkaPoTableViev>::Remove(UniRx.IObserver`1<T>)
extern "C"  Il2CppObject* ListObserver_1_Remove_m2991137565_gshared (ListObserver_1_t3803013488 * __this, Il2CppObject* ___observer0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		ImmutableList_1_t2485631923 * L_0 = (ImmutableList_1_t2485631923 *)__this->get__observers_0();
		NullCheck((ImmutableList_1_t2485631923 *)L_0);
		IObserver_1U5BU5D_t594978977* L_1 = ((  IObserver_1U5BU5D_t594978977* (*) (ImmutableList_1_t2485631923 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((ImmutableList_1_t2485631923 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		Il2CppObject* L_2 = ___observer0;
		int32_t L_3 = ((  int32_t (*) (Il2CppObject * /* static, unused */, IObserver_1U5BU5D_t594978977*, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->method)(NULL /*static, unused*/, (IObserver_1U5BU5D_t594978977*)L_1, (Il2CppObject*)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		V_0 = (int32_t)L_3;
		int32_t L_4 = V_0;
		if ((((int32_t)L_4) >= ((int32_t)0)))
		{
			goto IL_001b;
		}
	}
	{
		return __this;
	}

IL_001b:
	{
		ImmutableList_1_t2485631923 * L_5 = (ImmutableList_1_t2485631923 *)__this->get__observers_0();
		NullCheck((ImmutableList_1_t2485631923 *)L_5);
		IObserver_1U5BU5D_t594978977* L_6 = ((  IObserver_1U5BU5D_t594978977* (*) (ImmutableList_1_t2485631923 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((ImmutableList_1_t2485631923 *)L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		NullCheck(L_6);
		if ((!(((uint32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_6)->max_length))))) == ((uint32_t)2))))
		{
			goto IL_003e;
		}
	}
	{
		ImmutableList_1_t2485631923 * L_7 = (ImmutableList_1_t2485631923 *)__this->get__observers_0();
		NullCheck((ImmutableList_1_t2485631923 *)L_7);
		IObserver_1U5BU5D_t594978977* L_8 = ((  IObserver_1U5BU5D_t594978977* (*) (ImmutableList_1_t2485631923 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((ImmutableList_1_t2485631923 *)L_7, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_9 = V_0;
		NullCheck(L_8);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_8, ((int32_t)((int32_t)1-(int32_t)L_9)));
		int32_t L_10 = ((int32_t)((int32_t)1-(int32_t)L_9));
		return ((L_8)->GetAt(static_cast<il2cpp_array_size_t>(L_10)));
	}

IL_003e:
	{
		ImmutableList_1_t2485631923 * L_11 = (ImmutableList_1_t2485631923 *)__this->get__observers_0();
		Il2CppObject* L_12 = ___observer0;
		NullCheck((ImmutableList_1_t2485631923 *)L_11);
		ImmutableList_1_t2485631923 * L_13 = ((  ImmutableList_1_t2485631923 * (*) (ImmutableList_1_t2485631923 *, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6)->method)((ImmutableList_1_t2485631923 *)L_11, (Il2CppObject*)L_12, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		ListObserver_1_t3803013488 * L_14 = (ListObserver_1_t3803013488 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		((  void (*) (ListObserver_1_t3803013488 *, ImmutableList_1_t2485631923 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)(L_14, (ImmutableList_1_t2485631923 *)L_13, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		return L_14;
	}
}
// System.Void UniRx.InternalUtil.ListObserver`1<UniRx.CollectionAddEvent`1<System.Object>>::.ctor(UniRx.InternalUtil.ImmutableList`1<UniRx.IObserver`1<T>>)
extern "C"  void ListObserver_1__ctor_m3351957823_gshared (ListObserver_1_t2711139658 * __this, ImmutableList_1_t1393758093 * ___observers0, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		ImmutableList_1_t1393758093 * L_0 = ___observers0;
		__this->set__observers_0(L_0);
		return;
	}
}
// System.Void UniRx.InternalUtil.ListObserver`1<UniRx.CollectionAddEvent`1<System.Object>>::OnCompleted()
extern "C"  void ListObserver_1_OnCompleted_m3994638647_gshared (ListObserver_1_t2711139658 * __this, const MethodInfo* method)
{
	IObserver_1U5BU5D_t482039839* V_0 = NULL;
	int32_t V_1 = 0;
	{
		ImmutableList_1_t1393758093 * L_0 = (ImmutableList_1_t1393758093 *)__this->get__observers_0();
		NullCheck((ImmutableList_1_t1393758093 *)L_0);
		IObserver_1U5BU5D_t482039839* L_1 = ((  IObserver_1U5BU5D_t482039839* (*) (ImmutableList_1_t1393758093 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((ImmutableList_1_t1393758093 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		V_0 = (IObserver_1U5BU5D_t482039839*)L_1;
		V_1 = (int32_t)0;
		goto IL_001f;
	}

IL_0013:
	{
		IObserver_1U5BU5D_t482039839* L_2 = V_0;
		int32_t L_3 = V_1;
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, L_3);
		int32_t L_4 = L_3;
		NullCheck((Il2CppObject*)((L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_4))));
		InterfaceActionInvoker0::Invoke(0 /* System.Void UniRx.IObserver`1<UniRx.CollectionAddEvent`1<System.Object>>::OnCompleted() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), (Il2CppObject*)((L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_4))));
		int32_t L_5 = V_1;
		V_1 = (int32_t)((int32_t)((int32_t)L_5+(int32_t)1));
	}

IL_001f:
	{
		int32_t L_6 = V_1;
		IObserver_1U5BU5D_t482039839* L_7 = V_0;
		NullCheck(L_7);
		if ((((int32_t)L_6) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_7)->max_length)))))))
		{
			goto IL_0013;
		}
	}
	{
		return;
	}
}
// System.Void UniRx.InternalUtil.ListObserver`1<UniRx.CollectionAddEvent`1<System.Object>>::OnError(System.Exception)
extern "C"  void ListObserver_1_OnError_m2024599524_gshared (ListObserver_1_t2711139658 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method)
{
	IObserver_1U5BU5D_t482039839* V_0 = NULL;
	int32_t V_1 = 0;
	{
		ImmutableList_1_t1393758093 * L_0 = (ImmutableList_1_t1393758093 *)__this->get__observers_0();
		NullCheck((ImmutableList_1_t1393758093 *)L_0);
		IObserver_1U5BU5D_t482039839* L_1 = ((  IObserver_1U5BU5D_t482039839* (*) (ImmutableList_1_t1393758093 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((ImmutableList_1_t1393758093 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		V_0 = (IObserver_1U5BU5D_t482039839*)L_1;
		V_1 = (int32_t)0;
		goto IL_0020;
	}

IL_0013:
	{
		IObserver_1U5BU5D_t482039839* L_2 = V_0;
		int32_t L_3 = V_1;
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, L_3);
		int32_t L_4 = L_3;
		Exception_t1967233988 * L_5 = ___error0;
		NullCheck((Il2CppObject*)((L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_4))));
		InterfaceActionInvoker1< Exception_t1967233988 * >::Invoke(1 /* System.Void UniRx.IObserver`1<UniRx.CollectionAddEvent`1<System.Object>>::OnError(System.Exception) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), (Il2CppObject*)((L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_4))), (Exception_t1967233988 *)L_5);
		int32_t L_6 = V_1;
		V_1 = (int32_t)((int32_t)((int32_t)L_6+(int32_t)1));
	}

IL_0020:
	{
		int32_t L_7 = V_1;
		IObserver_1U5BU5D_t482039839* L_8 = V_0;
		NullCheck(L_8);
		if ((((int32_t)L_7) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_8)->max_length)))))))
		{
			goto IL_0013;
		}
	}
	{
		return;
	}
}
// System.Void UniRx.InternalUtil.ListObserver`1<UniRx.CollectionAddEvent`1<System.Object>>::OnNext(T)
extern "C"  void ListObserver_1_OnNext_m2249137877_gshared (ListObserver_1_t2711139658 * __this, CollectionAddEvent_1_t2416521987  ___value0, const MethodInfo* method)
{
	IObserver_1U5BU5D_t482039839* V_0 = NULL;
	int32_t V_1 = 0;
	{
		ImmutableList_1_t1393758093 * L_0 = (ImmutableList_1_t1393758093 *)__this->get__observers_0();
		NullCheck((ImmutableList_1_t1393758093 *)L_0);
		IObserver_1U5BU5D_t482039839* L_1 = ((  IObserver_1U5BU5D_t482039839* (*) (ImmutableList_1_t1393758093 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((ImmutableList_1_t1393758093 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		V_0 = (IObserver_1U5BU5D_t482039839*)L_1;
		V_1 = (int32_t)0;
		goto IL_0020;
	}

IL_0013:
	{
		IObserver_1U5BU5D_t482039839* L_2 = V_0;
		int32_t L_3 = V_1;
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, L_3);
		int32_t L_4 = L_3;
		CollectionAddEvent_1_t2416521987  L_5 = ___value0;
		NullCheck((Il2CppObject*)((L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_4))));
		InterfaceActionInvoker1< CollectionAddEvent_1_t2416521987  >::Invoke(2 /* System.Void UniRx.IObserver`1<UniRx.CollectionAddEvent`1<System.Object>>::OnNext(T) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), (Il2CppObject*)((L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_4))), (CollectionAddEvent_1_t2416521987 )L_5);
		int32_t L_6 = V_1;
		V_1 = (int32_t)((int32_t)((int32_t)L_6+(int32_t)1));
	}

IL_0020:
	{
		int32_t L_7 = V_1;
		IObserver_1U5BU5D_t482039839* L_8 = V_0;
		NullCheck(L_8);
		if ((((int32_t)L_7) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_8)->max_length)))))))
		{
			goto IL_0013;
		}
	}
	{
		return;
	}
}
// UniRx.IObserver`1<T> UniRx.InternalUtil.ListObserver`1<UniRx.CollectionAddEvent`1<System.Object>>::Add(UniRx.IObserver`1<T>)
extern "C"  Il2CppObject* ListObserver_1_Add_m1401659111_gshared (ListObserver_1_t2711139658 * __this, Il2CppObject* ___observer0, const MethodInfo* method)
{
	{
		ImmutableList_1_t1393758093 * L_0 = (ImmutableList_1_t1393758093 *)__this->get__observers_0();
		Il2CppObject* L_1 = ___observer0;
		NullCheck((ImmutableList_1_t1393758093 *)L_0);
		ImmutableList_1_t1393758093 * L_2 = ((  ImmutableList_1_t1393758093 * (*) (ImmutableList_1_t1393758093 *, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)((ImmutableList_1_t1393758093 *)L_0, (Il2CppObject*)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		ListObserver_1_t2711139658 * L_3 = (ListObserver_1_t2711139658 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		((  void (*) (ListObserver_1_t2711139658 *, ImmutableList_1_t1393758093 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)(L_3, (ImmutableList_1_t1393758093 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		return L_3;
	}
}
// UniRx.IObserver`1<T> UniRx.InternalUtil.ListObserver`1<UniRx.CollectionAddEvent`1<System.Object>>::Remove(UniRx.IObserver`1<T>)
extern "C"  Il2CppObject* ListObserver_1_Remove_m1587969796_gshared (ListObserver_1_t2711139658 * __this, Il2CppObject* ___observer0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		ImmutableList_1_t1393758093 * L_0 = (ImmutableList_1_t1393758093 *)__this->get__observers_0();
		NullCheck((ImmutableList_1_t1393758093 *)L_0);
		IObserver_1U5BU5D_t482039839* L_1 = ((  IObserver_1U5BU5D_t482039839* (*) (ImmutableList_1_t1393758093 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((ImmutableList_1_t1393758093 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		Il2CppObject* L_2 = ___observer0;
		int32_t L_3 = ((  int32_t (*) (Il2CppObject * /* static, unused */, IObserver_1U5BU5D_t482039839*, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->method)(NULL /*static, unused*/, (IObserver_1U5BU5D_t482039839*)L_1, (Il2CppObject*)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		V_0 = (int32_t)L_3;
		int32_t L_4 = V_0;
		if ((((int32_t)L_4) >= ((int32_t)0)))
		{
			goto IL_001b;
		}
	}
	{
		return __this;
	}

IL_001b:
	{
		ImmutableList_1_t1393758093 * L_5 = (ImmutableList_1_t1393758093 *)__this->get__observers_0();
		NullCheck((ImmutableList_1_t1393758093 *)L_5);
		IObserver_1U5BU5D_t482039839* L_6 = ((  IObserver_1U5BU5D_t482039839* (*) (ImmutableList_1_t1393758093 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((ImmutableList_1_t1393758093 *)L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		NullCheck(L_6);
		if ((!(((uint32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_6)->max_length))))) == ((uint32_t)2))))
		{
			goto IL_003e;
		}
	}
	{
		ImmutableList_1_t1393758093 * L_7 = (ImmutableList_1_t1393758093 *)__this->get__observers_0();
		NullCheck((ImmutableList_1_t1393758093 *)L_7);
		IObserver_1U5BU5D_t482039839* L_8 = ((  IObserver_1U5BU5D_t482039839* (*) (ImmutableList_1_t1393758093 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((ImmutableList_1_t1393758093 *)L_7, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_9 = V_0;
		NullCheck(L_8);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_8, ((int32_t)((int32_t)1-(int32_t)L_9)));
		int32_t L_10 = ((int32_t)((int32_t)1-(int32_t)L_9));
		return ((L_8)->GetAt(static_cast<il2cpp_array_size_t>(L_10)));
	}

IL_003e:
	{
		ImmutableList_1_t1393758093 * L_11 = (ImmutableList_1_t1393758093 *)__this->get__observers_0();
		Il2CppObject* L_12 = ___observer0;
		NullCheck((ImmutableList_1_t1393758093 *)L_11);
		ImmutableList_1_t1393758093 * L_13 = ((  ImmutableList_1_t1393758093 * (*) (ImmutableList_1_t1393758093 *, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6)->method)((ImmutableList_1_t1393758093 *)L_11, (Il2CppObject*)L_12, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		ListObserver_1_t2711139658 * L_14 = (ListObserver_1_t2711139658 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		((  void (*) (ListObserver_1_t2711139658 *, ImmutableList_1_t1393758093 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)(L_14, (ImmutableList_1_t1393758093 *)L_13, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		return L_14;
	}
}
// System.Void UniRx.InternalUtil.ListObserver`1<UniRx.CollectionAddEvent`1<UniRx.Tuple`2<System.Object,System.Object>>>::.ctor(UniRx.InternalUtil.ImmutableList`1<UniRx.IObserver`1<T>>)
extern "C"  void ListObserver_1__ctor_m2435064037_gshared (ListObserver_1_t2243295057 * __this, ImmutableList_1_t925913492 * ___observers0, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		ImmutableList_1_t925913492 * L_0 = ___observers0;
		__this->set__observers_0(L_0);
		return;
	}
}
// System.Void UniRx.InternalUtil.ListObserver`1<UniRx.CollectionAddEvent`1<UniRx.Tuple`2<System.Object,System.Object>>>::OnCompleted()
extern "C"  void ListObserver_1_OnCompleted_m3674177117_gshared (ListObserver_1_t2243295057 * __this, const MethodInfo* method)
{
	IObserver_1U5BU5D_t73482780* V_0 = NULL;
	int32_t V_1 = 0;
	{
		ImmutableList_1_t925913492 * L_0 = (ImmutableList_1_t925913492 *)__this->get__observers_0();
		NullCheck((ImmutableList_1_t925913492 *)L_0);
		IObserver_1U5BU5D_t73482780* L_1 = ((  IObserver_1U5BU5D_t73482780* (*) (ImmutableList_1_t925913492 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((ImmutableList_1_t925913492 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		V_0 = (IObserver_1U5BU5D_t73482780*)L_1;
		V_1 = (int32_t)0;
		goto IL_001f;
	}

IL_0013:
	{
		IObserver_1U5BU5D_t73482780* L_2 = V_0;
		int32_t L_3 = V_1;
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, L_3);
		int32_t L_4 = L_3;
		NullCheck((Il2CppObject*)((L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_4))));
		InterfaceActionInvoker0::Invoke(0 /* System.Void UniRx.IObserver`1<UniRx.CollectionAddEvent`1<UniRx.Tuple`2<System.Object,System.Object>>>::OnCompleted() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), (Il2CppObject*)((L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_4))));
		int32_t L_5 = V_1;
		V_1 = (int32_t)((int32_t)((int32_t)L_5+(int32_t)1));
	}

IL_001f:
	{
		int32_t L_6 = V_1;
		IObserver_1U5BU5D_t73482780* L_7 = V_0;
		NullCheck(L_7);
		if ((((int32_t)L_6) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_7)->max_length)))))))
		{
			goto IL_0013;
		}
	}
	{
		return;
	}
}
// System.Void UniRx.InternalUtil.ListObserver`1<UniRx.CollectionAddEvent`1<UniRx.Tuple`2<System.Object,System.Object>>>::OnError(System.Exception)
extern "C"  void ListObserver_1_OnError_m2482408458_gshared (ListObserver_1_t2243295057 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method)
{
	IObserver_1U5BU5D_t73482780* V_0 = NULL;
	int32_t V_1 = 0;
	{
		ImmutableList_1_t925913492 * L_0 = (ImmutableList_1_t925913492 *)__this->get__observers_0();
		NullCheck((ImmutableList_1_t925913492 *)L_0);
		IObserver_1U5BU5D_t73482780* L_1 = ((  IObserver_1U5BU5D_t73482780* (*) (ImmutableList_1_t925913492 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((ImmutableList_1_t925913492 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		V_0 = (IObserver_1U5BU5D_t73482780*)L_1;
		V_1 = (int32_t)0;
		goto IL_0020;
	}

IL_0013:
	{
		IObserver_1U5BU5D_t73482780* L_2 = V_0;
		int32_t L_3 = V_1;
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, L_3);
		int32_t L_4 = L_3;
		Exception_t1967233988 * L_5 = ___error0;
		NullCheck((Il2CppObject*)((L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_4))));
		InterfaceActionInvoker1< Exception_t1967233988 * >::Invoke(1 /* System.Void UniRx.IObserver`1<UniRx.CollectionAddEvent`1<UniRx.Tuple`2<System.Object,System.Object>>>::OnError(System.Exception) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), (Il2CppObject*)((L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_4))), (Exception_t1967233988 *)L_5);
		int32_t L_6 = V_1;
		V_1 = (int32_t)((int32_t)((int32_t)L_6+(int32_t)1));
	}

IL_0020:
	{
		int32_t L_7 = V_1;
		IObserver_1U5BU5D_t73482780* L_8 = V_0;
		NullCheck(L_8);
		if ((((int32_t)L_7) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_8)->max_length)))))))
		{
			goto IL_0013;
		}
	}
	{
		return;
	}
}
// System.Void UniRx.InternalUtil.ListObserver`1<UniRx.CollectionAddEvent`1<UniRx.Tuple`2<System.Object,System.Object>>>::OnNext(T)
extern "C"  void ListObserver_1_OnNext_m846317307_gshared (ListObserver_1_t2243295057 * __this, CollectionAddEvent_1_t1948677386  ___value0, const MethodInfo* method)
{
	IObserver_1U5BU5D_t73482780* V_0 = NULL;
	int32_t V_1 = 0;
	{
		ImmutableList_1_t925913492 * L_0 = (ImmutableList_1_t925913492 *)__this->get__observers_0();
		NullCheck((ImmutableList_1_t925913492 *)L_0);
		IObserver_1U5BU5D_t73482780* L_1 = ((  IObserver_1U5BU5D_t73482780* (*) (ImmutableList_1_t925913492 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((ImmutableList_1_t925913492 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		V_0 = (IObserver_1U5BU5D_t73482780*)L_1;
		V_1 = (int32_t)0;
		goto IL_0020;
	}

IL_0013:
	{
		IObserver_1U5BU5D_t73482780* L_2 = V_0;
		int32_t L_3 = V_1;
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, L_3);
		int32_t L_4 = L_3;
		CollectionAddEvent_1_t1948677386  L_5 = ___value0;
		NullCheck((Il2CppObject*)((L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_4))));
		InterfaceActionInvoker1< CollectionAddEvent_1_t1948677386  >::Invoke(2 /* System.Void UniRx.IObserver`1<UniRx.CollectionAddEvent`1<UniRx.Tuple`2<System.Object,System.Object>>>::OnNext(T) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), (Il2CppObject*)((L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_4))), (CollectionAddEvent_1_t1948677386 )L_5);
		int32_t L_6 = V_1;
		V_1 = (int32_t)((int32_t)((int32_t)L_6+(int32_t)1));
	}

IL_0020:
	{
		int32_t L_7 = V_1;
		IObserver_1U5BU5D_t73482780* L_8 = V_0;
		NullCheck(L_8);
		if ((((int32_t)L_7) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_8)->max_length)))))))
		{
			goto IL_0013;
		}
	}
	{
		return;
	}
}
// UniRx.IObserver`1<T> UniRx.InternalUtil.ListObserver`1<UniRx.CollectionAddEvent`1<UniRx.Tuple`2<System.Object,System.Object>>>::Add(UniRx.IObserver`1<T>)
extern "C"  Il2CppObject* ListObserver_1_Add_m926992155_gshared (ListObserver_1_t2243295057 * __this, Il2CppObject* ___observer0, const MethodInfo* method)
{
	{
		ImmutableList_1_t925913492 * L_0 = (ImmutableList_1_t925913492 *)__this->get__observers_0();
		Il2CppObject* L_1 = ___observer0;
		NullCheck((ImmutableList_1_t925913492 *)L_0);
		ImmutableList_1_t925913492 * L_2 = ((  ImmutableList_1_t925913492 * (*) (ImmutableList_1_t925913492 *, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)((ImmutableList_1_t925913492 *)L_0, (Il2CppObject*)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		ListObserver_1_t2243295057 * L_3 = (ListObserver_1_t2243295057 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		((  void (*) (ListObserver_1_t2243295057 *, ImmutableList_1_t925913492 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)(L_3, (ImmutableList_1_t925913492 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		return L_3;
	}
}
// UniRx.IObserver`1<T> UniRx.InternalUtil.ListObserver`1<UniRx.CollectionAddEvent`1<UniRx.Tuple`2<System.Object,System.Object>>>::Remove(UniRx.IObserver`1<T>)
extern "C"  Il2CppObject* ListObserver_1_Remove_m4111989328_gshared (ListObserver_1_t2243295057 * __this, Il2CppObject* ___observer0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		ImmutableList_1_t925913492 * L_0 = (ImmutableList_1_t925913492 *)__this->get__observers_0();
		NullCheck((ImmutableList_1_t925913492 *)L_0);
		IObserver_1U5BU5D_t73482780* L_1 = ((  IObserver_1U5BU5D_t73482780* (*) (ImmutableList_1_t925913492 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((ImmutableList_1_t925913492 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		Il2CppObject* L_2 = ___observer0;
		int32_t L_3 = ((  int32_t (*) (Il2CppObject * /* static, unused */, IObserver_1U5BU5D_t73482780*, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->method)(NULL /*static, unused*/, (IObserver_1U5BU5D_t73482780*)L_1, (Il2CppObject*)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		V_0 = (int32_t)L_3;
		int32_t L_4 = V_0;
		if ((((int32_t)L_4) >= ((int32_t)0)))
		{
			goto IL_001b;
		}
	}
	{
		return __this;
	}

IL_001b:
	{
		ImmutableList_1_t925913492 * L_5 = (ImmutableList_1_t925913492 *)__this->get__observers_0();
		NullCheck((ImmutableList_1_t925913492 *)L_5);
		IObserver_1U5BU5D_t73482780* L_6 = ((  IObserver_1U5BU5D_t73482780* (*) (ImmutableList_1_t925913492 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((ImmutableList_1_t925913492 *)L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		NullCheck(L_6);
		if ((!(((uint32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_6)->max_length))))) == ((uint32_t)2))))
		{
			goto IL_003e;
		}
	}
	{
		ImmutableList_1_t925913492 * L_7 = (ImmutableList_1_t925913492 *)__this->get__observers_0();
		NullCheck((ImmutableList_1_t925913492 *)L_7);
		IObserver_1U5BU5D_t73482780* L_8 = ((  IObserver_1U5BU5D_t73482780* (*) (ImmutableList_1_t925913492 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((ImmutableList_1_t925913492 *)L_7, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_9 = V_0;
		NullCheck(L_8);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_8, ((int32_t)((int32_t)1-(int32_t)L_9)));
		int32_t L_10 = ((int32_t)((int32_t)1-(int32_t)L_9));
		return ((L_8)->GetAt(static_cast<il2cpp_array_size_t>(L_10)));
	}

IL_003e:
	{
		ImmutableList_1_t925913492 * L_11 = (ImmutableList_1_t925913492 *)__this->get__observers_0();
		Il2CppObject* L_12 = ___observer0;
		NullCheck((ImmutableList_1_t925913492 *)L_11);
		ImmutableList_1_t925913492 * L_13 = ((  ImmutableList_1_t925913492 * (*) (ImmutableList_1_t925913492 *, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6)->method)((ImmutableList_1_t925913492 *)L_11, (Il2CppObject*)L_12, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		ListObserver_1_t2243295057 * L_14 = (ListObserver_1_t2243295057 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		((  void (*) (ListObserver_1_t2243295057 *, ImmutableList_1_t925913492 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)(L_14, (ImmutableList_1_t925913492 *)L_13, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		return L_14;
	}
}
// System.Void UniRx.InternalUtil.ListObserver`1<UniRx.CollectionMoveEvent`1<System.Object>>::.ctor(UniRx.InternalUtil.ImmutableList`1<UniRx.IObserver`1<T>>)
extern "C"  void ListObserver_1__ctor_m4115048323_gshared (ListObserver_1_t3211051518 * __this, ImmutableList_1_t1893669953 * ___observers0, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		ImmutableList_1_t1893669953 * L_0 = ___observers0;
		__this->set__observers_0(L_0);
		return;
	}
}
// System.Void UniRx.InternalUtil.ListObserver`1<UniRx.CollectionMoveEvent`1<System.Object>>::OnCompleted()
extern "C"  void ListObserver_1_OnCompleted_m797640827_gshared (ListObserver_1_t3211051518 * __this, const MethodInfo* method)
{
	IObserver_1U5BU5D_t3525058075* V_0 = NULL;
	int32_t V_1 = 0;
	{
		ImmutableList_1_t1893669953 * L_0 = (ImmutableList_1_t1893669953 *)__this->get__observers_0();
		NullCheck((ImmutableList_1_t1893669953 *)L_0);
		IObserver_1U5BU5D_t3525058075* L_1 = ((  IObserver_1U5BU5D_t3525058075* (*) (ImmutableList_1_t1893669953 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((ImmutableList_1_t1893669953 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		V_0 = (IObserver_1U5BU5D_t3525058075*)L_1;
		V_1 = (int32_t)0;
		goto IL_001f;
	}

IL_0013:
	{
		IObserver_1U5BU5D_t3525058075* L_2 = V_0;
		int32_t L_3 = V_1;
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, L_3);
		int32_t L_4 = L_3;
		NullCheck((Il2CppObject*)((L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_4))));
		InterfaceActionInvoker0::Invoke(0 /* System.Void UniRx.IObserver`1<UniRx.CollectionMoveEvent`1<System.Object>>::OnCompleted() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), (Il2CppObject*)((L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_4))));
		int32_t L_5 = V_1;
		V_1 = (int32_t)((int32_t)((int32_t)L_5+(int32_t)1));
	}

IL_001f:
	{
		int32_t L_6 = V_1;
		IObserver_1U5BU5D_t3525058075* L_7 = V_0;
		NullCheck(L_7);
		if ((((int32_t)L_6) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_7)->max_length)))))))
		{
			goto IL_0013;
		}
	}
	{
		return;
	}
}
// System.Void UniRx.InternalUtil.ListObserver`1<UniRx.CollectionMoveEvent`1<System.Object>>::OnError(System.Exception)
extern "C"  void ListObserver_1_OnError_m81647912_gshared (ListObserver_1_t3211051518 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method)
{
	IObserver_1U5BU5D_t3525058075* V_0 = NULL;
	int32_t V_1 = 0;
	{
		ImmutableList_1_t1893669953 * L_0 = (ImmutableList_1_t1893669953 *)__this->get__observers_0();
		NullCheck((ImmutableList_1_t1893669953 *)L_0);
		IObserver_1U5BU5D_t3525058075* L_1 = ((  IObserver_1U5BU5D_t3525058075* (*) (ImmutableList_1_t1893669953 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((ImmutableList_1_t1893669953 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		V_0 = (IObserver_1U5BU5D_t3525058075*)L_1;
		V_1 = (int32_t)0;
		goto IL_0020;
	}

IL_0013:
	{
		IObserver_1U5BU5D_t3525058075* L_2 = V_0;
		int32_t L_3 = V_1;
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, L_3);
		int32_t L_4 = L_3;
		Exception_t1967233988 * L_5 = ___error0;
		NullCheck((Il2CppObject*)((L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_4))));
		InterfaceActionInvoker1< Exception_t1967233988 * >::Invoke(1 /* System.Void UniRx.IObserver`1<UniRx.CollectionMoveEvent`1<System.Object>>::OnError(System.Exception) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), (Il2CppObject*)((L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_4))), (Exception_t1967233988 *)L_5);
		int32_t L_6 = V_1;
		V_1 = (int32_t)((int32_t)((int32_t)L_6+(int32_t)1));
	}

IL_0020:
	{
		int32_t L_7 = V_1;
		IObserver_1U5BU5D_t3525058075* L_8 = V_0;
		NullCheck(L_8);
		if ((((int32_t)L_7) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_8)->max_length)))))))
		{
			goto IL_0013;
		}
	}
	{
		return;
	}
}
// System.Void UniRx.InternalUtil.ListObserver`1<UniRx.CollectionMoveEvent`1<System.Object>>::OnNext(T)
extern "C"  void ListObserver_1_OnNext_m1092816921_gshared (ListObserver_1_t3211051518 * __this, CollectionMoveEvent_1_t2916433847  ___value0, const MethodInfo* method)
{
	IObserver_1U5BU5D_t3525058075* V_0 = NULL;
	int32_t V_1 = 0;
	{
		ImmutableList_1_t1893669953 * L_0 = (ImmutableList_1_t1893669953 *)__this->get__observers_0();
		NullCheck((ImmutableList_1_t1893669953 *)L_0);
		IObserver_1U5BU5D_t3525058075* L_1 = ((  IObserver_1U5BU5D_t3525058075* (*) (ImmutableList_1_t1893669953 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((ImmutableList_1_t1893669953 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		V_0 = (IObserver_1U5BU5D_t3525058075*)L_1;
		V_1 = (int32_t)0;
		goto IL_0020;
	}

IL_0013:
	{
		IObserver_1U5BU5D_t3525058075* L_2 = V_0;
		int32_t L_3 = V_1;
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, L_3);
		int32_t L_4 = L_3;
		CollectionMoveEvent_1_t2916433847  L_5 = ___value0;
		NullCheck((Il2CppObject*)((L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_4))));
		InterfaceActionInvoker1< CollectionMoveEvent_1_t2916433847  >::Invoke(2 /* System.Void UniRx.IObserver`1<UniRx.CollectionMoveEvent`1<System.Object>>::OnNext(T) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), (Il2CppObject*)((L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_4))), (CollectionMoveEvent_1_t2916433847 )L_5);
		int32_t L_6 = V_1;
		V_1 = (int32_t)((int32_t)((int32_t)L_6+(int32_t)1));
	}

IL_0020:
	{
		int32_t L_7 = V_1;
		IObserver_1U5BU5D_t3525058075* L_8 = V_0;
		NullCheck(L_8);
		if ((((int32_t)L_7) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_8)->max_length)))))))
		{
			goto IL_0013;
		}
	}
	{
		return;
	}
}
// UniRx.IObserver`1<T> UniRx.InternalUtil.ListObserver`1<UniRx.CollectionMoveEvent`1<System.Object>>::Add(UniRx.IObserver`1<T>)
extern "C"  Il2CppObject* ListObserver_1_Add_m2256284857_gshared (ListObserver_1_t3211051518 * __this, Il2CppObject* ___observer0, const MethodInfo* method)
{
	{
		ImmutableList_1_t1893669953 * L_0 = (ImmutableList_1_t1893669953 *)__this->get__observers_0();
		Il2CppObject* L_1 = ___observer0;
		NullCheck((ImmutableList_1_t1893669953 *)L_0);
		ImmutableList_1_t1893669953 * L_2 = ((  ImmutableList_1_t1893669953 * (*) (ImmutableList_1_t1893669953 *, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)((ImmutableList_1_t1893669953 *)L_0, (Il2CppObject*)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		ListObserver_1_t3211051518 * L_3 = (ListObserver_1_t3211051518 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		((  void (*) (ListObserver_1_t3211051518 *, ImmutableList_1_t1893669953 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)(L_3, (ImmutableList_1_t1893669953 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		return L_3;
	}
}
// UniRx.IObserver`1<T> UniRx.InternalUtil.ListObserver`1<UniRx.CollectionMoveEvent`1<System.Object>>::Remove(UniRx.IObserver`1<T>)
extern "C"  Il2CppObject* ListObserver_1_Remove_m1177438194_gshared (ListObserver_1_t3211051518 * __this, Il2CppObject* ___observer0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		ImmutableList_1_t1893669953 * L_0 = (ImmutableList_1_t1893669953 *)__this->get__observers_0();
		NullCheck((ImmutableList_1_t1893669953 *)L_0);
		IObserver_1U5BU5D_t3525058075* L_1 = ((  IObserver_1U5BU5D_t3525058075* (*) (ImmutableList_1_t1893669953 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((ImmutableList_1_t1893669953 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		Il2CppObject* L_2 = ___observer0;
		int32_t L_3 = ((  int32_t (*) (Il2CppObject * /* static, unused */, IObserver_1U5BU5D_t3525058075*, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->method)(NULL /*static, unused*/, (IObserver_1U5BU5D_t3525058075*)L_1, (Il2CppObject*)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		V_0 = (int32_t)L_3;
		int32_t L_4 = V_0;
		if ((((int32_t)L_4) >= ((int32_t)0)))
		{
			goto IL_001b;
		}
	}
	{
		return __this;
	}

IL_001b:
	{
		ImmutableList_1_t1893669953 * L_5 = (ImmutableList_1_t1893669953 *)__this->get__observers_0();
		NullCheck((ImmutableList_1_t1893669953 *)L_5);
		IObserver_1U5BU5D_t3525058075* L_6 = ((  IObserver_1U5BU5D_t3525058075* (*) (ImmutableList_1_t1893669953 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((ImmutableList_1_t1893669953 *)L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		NullCheck(L_6);
		if ((!(((uint32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_6)->max_length))))) == ((uint32_t)2))))
		{
			goto IL_003e;
		}
	}
	{
		ImmutableList_1_t1893669953 * L_7 = (ImmutableList_1_t1893669953 *)__this->get__observers_0();
		NullCheck((ImmutableList_1_t1893669953 *)L_7);
		IObserver_1U5BU5D_t3525058075* L_8 = ((  IObserver_1U5BU5D_t3525058075* (*) (ImmutableList_1_t1893669953 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((ImmutableList_1_t1893669953 *)L_7, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_9 = V_0;
		NullCheck(L_8);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_8, ((int32_t)((int32_t)1-(int32_t)L_9)));
		int32_t L_10 = ((int32_t)((int32_t)1-(int32_t)L_9));
		return ((L_8)->GetAt(static_cast<il2cpp_array_size_t>(L_10)));
	}

IL_003e:
	{
		ImmutableList_1_t1893669953 * L_11 = (ImmutableList_1_t1893669953 *)__this->get__observers_0();
		Il2CppObject* L_12 = ___observer0;
		NullCheck((ImmutableList_1_t1893669953 *)L_11);
		ImmutableList_1_t1893669953 * L_13 = ((  ImmutableList_1_t1893669953 * (*) (ImmutableList_1_t1893669953 *, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6)->method)((ImmutableList_1_t1893669953 *)L_11, (Il2CppObject*)L_12, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		ListObserver_1_t3211051518 * L_14 = (ListObserver_1_t3211051518 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		((  void (*) (ListObserver_1_t3211051518 *, ImmutableList_1_t1893669953 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)(L_14, (ImmutableList_1_t1893669953 *)L_13, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		return L_14;
	}
}
// System.Void UniRx.InternalUtil.ListObserver`1<UniRx.CollectionMoveEvent`1<UniRx.Tuple`2<System.Object,System.Object>>>::.ctor(UniRx.InternalUtil.ImmutableList`1<UniRx.IObserver`1<T>>)
extern "C"  void ListObserver_1__ctor_m2128453921_gshared (ListObserver_1_t2743206917 * __this, ImmutableList_1_t1425825352 * ___observers0, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		ImmutableList_1_t1425825352 * L_0 = ___observers0;
		__this->set__observers_0(L_0);
		return;
	}
}
// System.Void UniRx.InternalUtil.ListObserver`1<UniRx.CollectionMoveEvent`1<UniRx.Tuple`2<System.Object,System.Object>>>::OnCompleted()
extern "C"  void ListObserver_1_OnCompleted_m845006233_gshared (ListObserver_1_t2743206917 * __this, const MethodInfo* method)
{
	IObserver_1U5BU5D_t3116501016* V_0 = NULL;
	int32_t V_1 = 0;
	{
		ImmutableList_1_t1425825352 * L_0 = (ImmutableList_1_t1425825352 *)__this->get__observers_0();
		NullCheck((ImmutableList_1_t1425825352 *)L_0);
		IObserver_1U5BU5D_t3116501016* L_1 = ((  IObserver_1U5BU5D_t3116501016* (*) (ImmutableList_1_t1425825352 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((ImmutableList_1_t1425825352 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		V_0 = (IObserver_1U5BU5D_t3116501016*)L_1;
		V_1 = (int32_t)0;
		goto IL_001f;
	}

IL_0013:
	{
		IObserver_1U5BU5D_t3116501016* L_2 = V_0;
		int32_t L_3 = V_1;
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, L_3);
		int32_t L_4 = L_3;
		NullCheck((Il2CppObject*)((L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_4))));
		InterfaceActionInvoker0::Invoke(0 /* System.Void UniRx.IObserver`1<UniRx.CollectionMoveEvent`1<UniRx.Tuple`2<System.Object,System.Object>>>::OnCompleted() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), (Il2CppObject*)((L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_4))));
		int32_t L_5 = V_1;
		V_1 = (int32_t)((int32_t)((int32_t)L_5+(int32_t)1));
	}

IL_001f:
	{
		int32_t L_6 = V_1;
		IObserver_1U5BU5D_t3116501016* L_7 = V_0;
		NullCheck(L_7);
		if ((((int32_t)L_6) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_7)->max_length)))))))
		{
			goto IL_0013;
		}
	}
	{
		return;
	}
}
// System.Void UniRx.InternalUtil.ListObserver`1<UniRx.CollectionMoveEvent`1<UniRx.Tuple`2<System.Object,System.Object>>>::OnError(System.Exception)
extern "C"  void ListObserver_1_OnError_m198695238_gshared (ListObserver_1_t2743206917 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method)
{
	IObserver_1U5BU5D_t3116501016* V_0 = NULL;
	int32_t V_1 = 0;
	{
		ImmutableList_1_t1425825352 * L_0 = (ImmutableList_1_t1425825352 *)__this->get__observers_0();
		NullCheck((ImmutableList_1_t1425825352 *)L_0);
		IObserver_1U5BU5D_t3116501016* L_1 = ((  IObserver_1U5BU5D_t3116501016* (*) (ImmutableList_1_t1425825352 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((ImmutableList_1_t1425825352 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		V_0 = (IObserver_1U5BU5D_t3116501016*)L_1;
		V_1 = (int32_t)0;
		goto IL_0020;
	}

IL_0013:
	{
		IObserver_1U5BU5D_t3116501016* L_2 = V_0;
		int32_t L_3 = V_1;
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, L_3);
		int32_t L_4 = L_3;
		Exception_t1967233988 * L_5 = ___error0;
		NullCheck((Il2CppObject*)((L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_4))));
		InterfaceActionInvoker1< Exception_t1967233988 * >::Invoke(1 /* System.Void UniRx.IObserver`1<UniRx.CollectionMoveEvent`1<UniRx.Tuple`2<System.Object,System.Object>>>::OnError(System.Exception) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), (Il2CppObject*)((L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_4))), (Exception_t1967233988 *)L_5);
		int32_t L_6 = V_1;
		V_1 = (int32_t)((int32_t)((int32_t)L_6+(int32_t)1));
	}

IL_0020:
	{
		int32_t L_7 = V_1;
		IObserver_1U5BU5D_t3116501016* L_8 = V_0;
		NullCheck(L_8);
		if ((((int32_t)L_7) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_8)->max_length)))))))
		{
			goto IL_0013;
		}
	}
	{
		return;
	}
}
// System.Void UniRx.InternalUtil.ListObserver`1<UniRx.CollectionMoveEvent`1<UniRx.Tuple`2<System.Object,System.Object>>>::OnNext(T)
extern "C"  void ListObserver_1_OnNext_m3484224567_gshared (ListObserver_1_t2743206917 * __this, CollectionMoveEvent_1_t2448589246  ___value0, const MethodInfo* method)
{
	IObserver_1U5BU5D_t3116501016* V_0 = NULL;
	int32_t V_1 = 0;
	{
		ImmutableList_1_t1425825352 * L_0 = (ImmutableList_1_t1425825352 *)__this->get__observers_0();
		NullCheck((ImmutableList_1_t1425825352 *)L_0);
		IObserver_1U5BU5D_t3116501016* L_1 = ((  IObserver_1U5BU5D_t3116501016* (*) (ImmutableList_1_t1425825352 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((ImmutableList_1_t1425825352 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		V_0 = (IObserver_1U5BU5D_t3116501016*)L_1;
		V_1 = (int32_t)0;
		goto IL_0020;
	}

IL_0013:
	{
		IObserver_1U5BU5D_t3116501016* L_2 = V_0;
		int32_t L_3 = V_1;
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, L_3);
		int32_t L_4 = L_3;
		CollectionMoveEvent_1_t2448589246  L_5 = ___value0;
		NullCheck((Il2CppObject*)((L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_4))));
		InterfaceActionInvoker1< CollectionMoveEvent_1_t2448589246  >::Invoke(2 /* System.Void UniRx.IObserver`1<UniRx.CollectionMoveEvent`1<UniRx.Tuple`2<System.Object,System.Object>>>::OnNext(T) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), (Il2CppObject*)((L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_4))), (CollectionMoveEvent_1_t2448589246 )L_5);
		int32_t L_6 = V_1;
		V_1 = (int32_t)((int32_t)((int32_t)L_6+(int32_t)1));
	}

IL_0020:
	{
		int32_t L_7 = V_1;
		IObserver_1U5BU5D_t3116501016* L_8 = V_0;
		NullCheck(L_8);
		if ((((int32_t)L_7) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_8)->max_length)))))))
		{
			goto IL_0013;
		}
	}
	{
		return;
	}
}
// UniRx.IObserver`1<T> UniRx.InternalUtil.ListObserver`1<UniRx.CollectionMoveEvent`1<UniRx.Tuple`2<System.Object,System.Object>>>::Add(UniRx.IObserver`1<T>)
extern "C"  Il2CppObject* ListObserver_1_Add_m3531350665_gshared (ListObserver_1_t2743206917 * __this, Il2CppObject* ___observer0, const MethodInfo* method)
{
	{
		ImmutableList_1_t1425825352 * L_0 = (ImmutableList_1_t1425825352 *)__this->get__observers_0();
		Il2CppObject* L_1 = ___observer0;
		NullCheck((ImmutableList_1_t1425825352 *)L_0);
		ImmutableList_1_t1425825352 * L_2 = ((  ImmutableList_1_t1425825352 * (*) (ImmutableList_1_t1425825352 *, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)((ImmutableList_1_t1425825352 *)L_0, (Il2CppObject*)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		ListObserver_1_t2743206917 * L_3 = (ListObserver_1_t2743206917 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		((  void (*) (ListObserver_1_t2743206917 *, ImmutableList_1_t1425825352 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)(L_3, (ImmutableList_1_t1425825352 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		return L_3;
	}
}
// UniRx.IObserver`1<T> UniRx.InternalUtil.ListObserver`1<UniRx.CollectionMoveEvent`1<UniRx.Tuple`2<System.Object,System.Object>>>::Remove(UniRx.IObserver`1<T>)
extern "C"  Il2CppObject* ListObserver_1_Remove_m1972158498_gshared (ListObserver_1_t2743206917 * __this, Il2CppObject* ___observer0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		ImmutableList_1_t1425825352 * L_0 = (ImmutableList_1_t1425825352 *)__this->get__observers_0();
		NullCheck((ImmutableList_1_t1425825352 *)L_0);
		IObserver_1U5BU5D_t3116501016* L_1 = ((  IObserver_1U5BU5D_t3116501016* (*) (ImmutableList_1_t1425825352 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((ImmutableList_1_t1425825352 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		Il2CppObject* L_2 = ___observer0;
		int32_t L_3 = ((  int32_t (*) (Il2CppObject * /* static, unused */, IObserver_1U5BU5D_t3116501016*, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->method)(NULL /*static, unused*/, (IObserver_1U5BU5D_t3116501016*)L_1, (Il2CppObject*)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		V_0 = (int32_t)L_3;
		int32_t L_4 = V_0;
		if ((((int32_t)L_4) >= ((int32_t)0)))
		{
			goto IL_001b;
		}
	}
	{
		return __this;
	}

IL_001b:
	{
		ImmutableList_1_t1425825352 * L_5 = (ImmutableList_1_t1425825352 *)__this->get__observers_0();
		NullCheck((ImmutableList_1_t1425825352 *)L_5);
		IObserver_1U5BU5D_t3116501016* L_6 = ((  IObserver_1U5BU5D_t3116501016* (*) (ImmutableList_1_t1425825352 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((ImmutableList_1_t1425825352 *)L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		NullCheck(L_6);
		if ((!(((uint32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_6)->max_length))))) == ((uint32_t)2))))
		{
			goto IL_003e;
		}
	}
	{
		ImmutableList_1_t1425825352 * L_7 = (ImmutableList_1_t1425825352 *)__this->get__observers_0();
		NullCheck((ImmutableList_1_t1425825352 *)L_7);
		IObserver_1U5BU5D_t3116501016* L_8 = ((  IObserver_1U5BU5D_t3116501016* (*) (ImmutableList_1_t1425825352 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((ImmutableList_1_t1425825352 *)L_7, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_9 = V_0;
		NullCheck(L_8);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_8, ((int32_t)((int32_t)1-(int32_t)L_9)));
		int32_t L_10 = ((int32_t)((int32_t)1-(int32_t)L_9));
		return ((L_8)->GetAt(static_cast<il2cpp_array_size_t>(L_10)));
	}

IL_003e:
	{
		ImmutableList_1_t1425825352 * L_11 = (ImmutableList_1_t1425825352 *)__this->get__observers_0();
		Il2CppObject* L_12 = ___observer0;
		NullCheck((ImmutableList_1_t1425825352 *)L_11);
		ImmutableList_1_t1425825352 * L_13 = ((  ImmutableList_1_t1425825352 * (*) (ImmutableList_1_t1425825352 *, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6)->method)((ImmutableList_1_t1425825352 *)L_11, (Il2CppObject*)L_12, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		ListObserver_1_t2743206917 * L_14 = (ListObserver_1_t2743206917 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		((  void (*) (ListObserver_1_t2743206917 *, ImmutableList_1_t1425825352 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)(L_14, (ImmutableList_1_t1425825352 *)L_13, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		return L_14;
	}
}
// System.Void UniRx.InternalUtil.ListObserver`1<UniRx.CollectionRemoveEvent`1<System.Object>>::.ctor(UniRx.InternalUtil.ImmutableList`1<UniRx.IObserver`1<T>>)
extern "C"  void ListObserver_1__ctor_m3040940214_gshared (ListObserver_1_t537255395 * __this, ImmutableList_1_t3514841126 * ___observers0, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		ImmutableList_1_t3514841126 * L_0 = ___observers0;
		__this->set__observers_0(L_0);
		return;
	}
}
// System.Void UniRx.InternalUtil.ListObserver`1<UniRx.CollectionRemoveEvent`1<System.Object>>::OnCompleted()
extern "C"  void ListObserver_1_OnCompleted_m795407470_gshared (ListObserver_1_t537255395 * __this, const MethodInfo* method)
{
	IObserver_1U5BU5D_t3107448066* V_0 = NULL;
	int32_t V_1 = 0;
	{
		ImmutableList_1_t3514841126 * L_0 = (ImmutableList_1_t3514841126 *)__this->get__observers_0();
		NullCheck((ImmutableList_1_t3514841126 *)L_0);
		IObserver_1U5BU5D_t3107448066* L_1 = ((  IObserver_1U5BU5D_t3107448066* (*) (ImmutableList_1_t3514841126 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((ImmutableList_1_t3514841126 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		V_0 = (IObserver_1U5BU5D_t3107448066*)L_1;
		V_1 = (int32_t)0;
		goto IL_001f;
	}

IL_0013:
	{
		IObserver_1U5BU5D_t3107448066* L_2 = V_0;
		int32_t L_3 = V_1;
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, L_3);
		int32_t L_4 = L_3;
		NullCheck((Il2CppObject*)((L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_4))));
		InterfaceActionInvoker0::Invoke(0 /* System.Void UniRx.IObserver`1<UniRx.CollectionRemoveEvent`1<System.Object>>::OnCompleted() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), (Il2CppObject*)((L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_4))));
		int32_t L_5 = V_1;
		V_1 = (int32_t)((int32_t)((int32_t)L_5+(int32_t)1));
	}

IL_001f:
	{
		int32_t L_6 = V_1;
		IObserver_1U5BU5D_t3107448066* L_7 = V_0;
		NullCheck(L_7);
		if ((((int32_t)L_6) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_7)->max_length)))))))
		{
			goto IL_0013;
		}
	}
	{
		return;
	}
}
// System.Void UniRx.InternalUtil.ListObserver`1<UniRx.CollectionRemoveEvent`1<System.Object>>::OnError(System.Exception)
extern "C"  void ListObserver_1_OnError_m3465703579_gshared (ListObserver_1_t537255395 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method)
{
	IObserver_1U5BU5D_t3107448066* V_0 = NULL;
	int32_t V_1 = 0;
	{
		ImmutableList_1_t3514841126 * L_0 = (ImmutableList_1_t3514841126 *)__this->get__observers_0();
		NullCheck((ImmutableList_1_t3514841126 *)L_0);
		IObserver_1U5BU5D_t3107448066* L_1 = ((  IObserver_1U5BU5D_t3107448066* (*) (ImmutableList_1_t3514841126 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((ImmutableList_1_t3514841126 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		V_0 = (IObserver_1U5BU5D_t3107448066*)L_1;
		V_1 = (int32_t)0;
		goto IL_0020;
	}

IL_0013:
	{
		IObserver_1U5BU5D_t3107448066* L_2 = V_0;
		int32_t L_3 = V_1;
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, L_3);
		int32_t L_4 = L_3;
		Exception_t1967233988 * L_5 = ___error0;
		NullCheck((Il2CppObject*)((L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_4))));
		InterfaceActionInvoker1< Exception_t1967233988 * >::Invoke(1 /* System.Void UniRx.IObserver`1<UniRx.CollectionRemoveEvent`1<System.Object>>::OnError(System.Exception) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), (Il2CppObject*)((L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_4))), (Exception_t1967233988 *)L_5);
		int32_t L_6 = V_1;
		V_1 = (int32_t)((int32_t)((int32_t)L_6+(int32_t)1));
	}

IL_0020:
	{
		int32_t L_7 = V_1;
		IObserver_1U5BU5D_t3107448066* L_8 = V_0;
		NullCheck(L_8);
		if ((((int32_t)L_7) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_8)->max_length)))))))
		{
			goto IL_0013;
		}
	}
	{
		return;
	}
}
// System.Void UniRx.InternalUtil.ListObserver`1<UniRx.CollectionRemoveEvent`1<System.Object>>::OnNext(T)
extern "C"  void ListObserver_1_OnNext_m2279796108_gshared (ListObserver_1_t537255395 * __this, CollectionRemoveEvent_1_t242637724  ___value0, const MethodInfo* method)
{
	IObserver_1U5BU5D_t3107448066* V_0 = NULL;
	int32_t V_1 = 0;
	{
		ImmutableList_1_t3514841126 * L_0 = (ImmutableList_1_t3514841126 *)__this->get__observers_0();
		NullCheck((ImmutableList_1_t3514841126 *)L_0);
		IObserver_1U5BU5D_t3107448066* L_1 = ((  IObserver_1U5BU5D_t3107448066* (*) (ImmutableList_1_t3514841126 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((ImmutableList_1_t3514841126 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		V_0 = (IObserver_1U5BU5D_t3107448066*)L_1;
		V_1 = (int32_t)0;
		goto IL_0020;
	}

IL_0013:
	{
		IObserver_1U5BU5D_t3107448066* L_2 = V_0;
		int32_t L_3 = V_1;
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, L_3);
		int32_t L_4 = L_3;
		CollectionRemoveEvent_1_t242637724  L_5 = ___value0;
		NullCheck((Il2CppObject*)((L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_4))));
		InterfaceActionInvoker1< CollectionRemoveEvent_1_t242637724  >::Invoke(2 /* System.Void UniRx.IObserver`1<UniRx.CollectionRemoveEvent`1<System.Object>>::OnNext(T) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), (Il2CppObject*)((L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_4))), (CollectionRemoveEvent_1_t242637724 )L_5);
		int32_t L_6 = V_1;
		V_1 = (int32_t)((int32_t)((int32_t)L_6+(int32_t)1));
	}

IL_0020:
	{
		int32_t L_7 = V_1;
		IObserver_1U5BU5D_t3107448066* L_8 = V_0;
		NullCheck(L_8);
		if ((((int32_t)L_7) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_8)->max_length)))))))
		{
			goto IL_0013;
		}
	}
	{
		return;
	}
}
// UniRx.IObserver`1<T> UniRx.InternalUtil.ListObserver`1<UniRx.CollectionRemoveEvent`1<System.Object>>::Add(UniRx.IObserver`1<T>)
extern "C"  Il2CppObject* ListObserver_1_Add_m1993239148_gshared (ListObserver_1_t537255395 * __this, Il2CppObject* ___observer0, const MethodInfo* method)
{
	{
		ImmutableList_1_t3514841126 * L_0 = (ImmutableList_1_t3514841126 *)__this->get__observers_0();
		Il2CppObject* L_1 = ___observer0;
		NullCheck((ImmutableList_1_t3514841126 *)L_0);
		ImmutableList_1_t3514841126 * L_2 = ((  ImmutableList_1_t3514841126 * (*) (ImmutableList_1_t3514841126 *, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)((ImmutableList_1_t3514841126 *)L_0, (Il2CppObject*)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		ListObserver_1_t537255395 * L_3 = (ListObserver_1_t537255395 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		((  void (*) (ListObserver_1_t537255395 *, ImmutableList_1_t3514841126 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)(L_3, (ImmutableList_1_t3514841126 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		return L_3;
	}
}
// UniRx.IObserver`1<T> UniRx.InternalUtil.ListObserver`1<UniRx.CollectionRemoveEvent`1<System.Object>>::Remove(UniRx.IObserver`1<T>)
extern "C"  Il2CppObject* ListObserver_1_Remove_m3098036575_gshared (ListObserver_1_t537255395 * __this, Il2CppObject* ___observer0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		ImmutableList_1_t3514841126 * L_0 = (ImmutableList_1_t3514841126 *)__this->get__observers_0();
		NullCheck((ImmutableList_1_t3514841126 *)L_0);
		IObserver_1U5BU5D_t3107448066* L_1 = ((  IObserver_1U5BU5D_t3107448066* (*) (ImmutableList_1_t3514841126 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((ImmutableList_1_t3514841126 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		Il2CppObject* L_2 = ___observer0;
		int32_t L_3 = ((  int32_t (*) (Il2CppObject * /* static, unused */, IObserver_1U5BU5D_t3107448066*, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->method)(NULL /*static, unused*/, (IObserver_1U5BU5D_t3107448066*)L_1, (Il2CppObject*)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		V_0 = (int32_t)L_3;
		int32_t L_4 = V_0;
		if ((((int32_t)L_4) >= ((int32_t)0)))
		{
			goto IL_001b;
		}
	}
	{
		return __this;
	}

IL_001b:
	{
		ImmutableList_1_t3514841126 * L_5 = (ImmutableList_1_t3514841126 *)__this->get__observers_0();
		NullCheck((ImmutableList_1_t3514841126 *)L_5);
		IObserver_1U5BU5D_t3107448066* L_6 = ((  IObserver_1U5BU5D_t3107448066* (*) (ImmutableList_1_t3514841126 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((ImmutableList_1_t3514841126 *)L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		NullCheck(L_6);
		if ((!(((uint32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_6)->max_length))))) == ((uint32_t)2))))
		{
			goto IL_003e;
		}
	}
	{
		ImmutableList_1_t3514841126 * L_7 = (ImmutableList_1_t3514841126 *)__this->get__observers_0();
		NullCheck((ImmutableList_1_t3514841126 *)L_7);
		IObserver_1U5BU5D_t3107448066* L_8 = ((  IObserver_1U5BU5D_t3107448066* (*) (ImmutableList_1_t3514841126 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((ImmutableList_1_t3514841126 *)L_7, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_9 = V_0;
		NullCheck(L_8);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_8, ((int32_t)((int32_t)1-(int32_t)L_9)));
		int32_t L_10 = ((int32_t)((int32_t)1-(int32_t)L_9));
		return ((L_8)->GetAt(static_cast<il2cpp_array_size_t>(L_10)));
	}

IL_003e:
	{
		ImmutableList_1_t3514841126 * L_11 = (ImmutableList_1_t3514841126 *)__this->get__observers_0();
		Il2CppObject* L_12 = ___observer0;
		NullCheck((ImmutableList_1_t3514841126 *)L_11);
		ImmutableList_1_t3514841126 * L_13 = ((  ImmutableList_1_t3514841126 * (*) (ImmutableList_1_t3514841126 *, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6)->method)((ImmutableList_1_t3514841126 *)L_11, (Il2CppObject*)L_12, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		ListObserver_1_t537255395 * L_14 = (ListObserver_1_t537255395 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		((  void (*) (ListObserver_1_t537255395 *, ImmutableList_1_t3514841126 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)(L_14, (ImmutableList_1_t3514841126 *)L_13, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		return L_14;
	}
}
// System.Void UniRx.InternalUtil.ListObserver`1<UniRx.CollectionRemoveEvent`1<UniRx.Tuple`2<System.Object,System.Object>>>::.ctor(UniRx.InternalUtil.ImmutableList`1<UniRx.IObserver`1<T>>)
extern "C"  void ListObserver_1__ctor_m2918058190_gshared (ListObserver_1_t69410794 * __this, ImmutableList_1_t3046996525 * ___observers0, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		ImmutableList_1_t3046996525 * L_0 = ___observers0;
		__this->set__observers_0(L_0);
		return;
	}
}
// System.Void UniRx.InternalUtil.ListObserver`1<UniRx.CollectionRemoveEvent`1<UniRx.Tuple`2<System.Object,System.Object>>>::OnCompleted()
extern "C"  void ListObserver_1_OnCompleted_m3776094854_gshared (ListObserver_1_t69410794 * __this, const MethodInfo* method)
{
	IObserver_1U5BU5D_t2698891007* V_0 = NULL;
	int32_t V_1 = 0;
	{
		ImmutableList_1_t3046996525 * L_0 = (ImmutableList_1_t3046996525 *)__this->get__observers_0();
		NullCheck((ImmutableList_1_t3046996525 *)L_0);
		IObserver_1U5BU5D_t2698891007* L_1 = ((  IObserver_1U5BU5D_t2698891007* (*) (ImmutableList_1_t3046996525 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((ImmutableList_1_t3046996525 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		V_0 = (IObserver_1U5BU5D_t2698891007*)L_1;
		V_1 = (int32_t)0;
		goto IL_001f;
	}

IL_0013:
	{
		IObserver_1U5BU5D_t2698891007* L_2 = V_0;
		int32_t L_3 = V_1;
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, L_3);
		int32_t L_4 = L_3;
		NullCheck((Il2CppObject*)((L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_4))));
		InterfaceActionInvoker0::Invoke(0 /* System.Void UniRx.IObserver`1<UniRx.CollectionRemoveEvent`1<UniRx.Tuple`2<System.Object,System.Object>>>::OnCompleted() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), (Il2CppObject*)((L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_4))));
		int32_t L_5 = V_1;
		V_1 = (int32_t)((int32_t)((int32_t)L_5+(int32_t)1));
	}

IL_001f:
	{
		int32_t L_6 = V_1;
		IObserver_1U5BU5D_t2698891007* L_7 = V_0;
		NullCheck(L_7);
		if ((((int32_t)L_6) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_7)->max_length)))))))
		{
			goto IL_0013;
		}
	}
	{
		return;
	}
}
// System.Void UniRx.InternalUtil.ListObserver`1<UniRx.CollectionRemoveEvent`1<UniRx.Tuple`2<System.Object,System.Object>>>::OnError(System.Exception)
extern "C"  void ListObserver_1_OnError_m3043998387_gshared (ListObserver_1_t69410794 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method)
{
	IObserver_1U5BU5D_t2698891007* V_0 = NULL;
	int32_t V_1 = 0;
	{
		ImmutableList_1_t3046996525 * L_0 = (ImmutableList_1_t3046996525 *)__this->get__observers_0();
		NullCheck((ImmutableList_1_t3046996525 *)L_0);
		IObserver_1U5BU5D_t2698891007* L_1 = ((  IObserver_1U5BU5D_t2698891007* (*) (ImmutableList_1_t3046996525 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((ImmutableList_1_t3046996525 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		V_0 = (IObserver_1U5BU5D_t2698891007*)L_1;
		V_1 = (int32_t)0;
		goto IL_0020;
	}

IL_0013:
	{
		IObserver_1U5BU5D_t2698891007* L_2 = V_0;
		int32_t L_3 = V_1;
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, L_3);
		int32_t L_4 = L_3;
		Exception_t1967233988 * L_5 = ___error0;
		NullCheck((Il2CppObject*)((L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_4))));
		InterfaceActionInvoker1< Exception_t1967233988 * >::Invoke(1 /* System.Void UniRx.IObserver`1<UniRx.CollectionRemoveEvent`1<UniRx.Tuple`2<System.Object,System.Object>>>::OnError(System.Exception) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), (Il2CppObject*)((L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_4))), (Exception_t1967233988 *)L_5);
		int32_t L_6 = V_1;
		V_1 = (int32_t)((int32_t)((int32_t)L_6+(int32_t)1));
	}

IL_0020:
	{
		int32_t L_7 = V_1;
		IObserver_1U5BU5D_t2698891007* L_8 = V_0;
		NullCheck(L_8);
		if ((((int32_t)L_7) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_8)->max_length)))))))
		{
			goto IL_0013;
		}
	}
	{
		return;
	}
}
// System.Void UniRx.InternalUtil.ListObserver`1<UniRx.CollectionRemoveEvent`1<UniRx.Tuple`2<System.Object,System.Object>>>::OnNext(T)
extern "C"  void ListObserver_1_OnNext_m3003606948_gshared (ListObserver_1_t69410794 * __this, CollectionRemoveEvent_1_t4069760419  ___value0, const MethodInfo* method)
{
	IObserver_1U5BU5D_t2698891007* V_0 = NULL;
	int32_t V_1 = 0;
	{
		ImmutableList_1_t3046996525 * L_0 = (ImmutableList_1_t3046996525 *)__this->get__observers_0();
		NullCheck((ImmutableList_1_t3046996525 *)L_0);
		IObserver_1U5BU5D_t2698891007* L_1 = ((  IObserver_1U5BU5D_t2698891007* (*) (ImmutableList_1_t3046996525 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((ImmutableList_1_t3046996525 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		V_0 = (IObserver_1U5BU5D_t2698891007*)L_1;
		V_1 = (int32_t)0;
		goto IL_0020;
	}

IL_0013:
	{
		IObserver_1U5BU5D_t2698891007* L_2 = V_0;
		int32_t L_3 = V_1;
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, L_3);
		int32_t L_4 = L_3;
		CollectionRemoveEvent_1_t4069760419  L_5 = ___value0;
		NullCheck((Il2CppObject*)((L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_4))));
		InterfaceActionInvoker1< CollectionRemoveEvent_1_t4069760419  >::Invoke(2 /* System.Void UniRx.IObserver`1<UniRx.CollectionRemoveEvent`1<UniRx.Tuple`2<System.Object,System.Object>>>::OnNext(T) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), (Il2CppObject*)((L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_4))), (CollectionRemoveEvent_1_t4069760419 )L_5);
		int32_t L_6 = V_1;
		V_1 = (int32_t)((int32_t)((int32_t)L_6+(int32_t)1));
	}

IL_0020:
	{
		int32_t L_7 = V_1;
		IObserver_1U5BU5D_t2698891007* L_8 = V_0;
		NullCheck(L_8);
		if ((((int32_t)L_7) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_8)->max_length)))))))
		{
			goto IL_0013;
		}
	}
	{
		return;
	}
}
// UniRx.IObserver`1<T> UniRx.InternalUtil.ListObserver`1<UniRx.CollectionRemoveEvent`1<UniRx.Tuple`2<System.Object,System.Object>>>::Add(UniRx.IObserver`1<T>)
extern "C"  Il2CppObject* ListObserver_1_Add_m2030765494_gshared (ListObserver_1_t69410794 * __this, Il2CppObject* ___observer0, const MethodInfo* method)
{
	{
		ImmutableList_1_t3046996525 * L_0 = (ImmutableList_1_t3046996525 *)__this->get__observers_0();
		Il2CppObject* L_1 = ___observer0;
		NullCheck((ImmutableList_1_t3046996525 *)L_0);
		ImmutableList_1_t3046996525 * L_2 = ((  ImmutableList_1_t3046996525 * (*) (ImmutableList_1_t3046996525 *, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)((ImmutableList_1_t3046996525 *)L_0, (Il2CppObject*)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		ListObserver_1_t69410794 * L_3 = (ListObserver_1_t69410794 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		((  void (*) (ListObserver_1_t69410794 *, ImmutableList_1_t3046996525 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)(L_3, (ImmutableList_1_t3046996525 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		return L_3;
	}
}
// UniRx.IObserver`1<T> UniRx.InternalUtil.ListObserver`1<UniRx.CollectionRemoveEvent`1<UniRx.Tuple`2<System.Object,System.Object>>>::Remove(UniRx.IObserver`1<T>)
extern "C"  Il2CppObject* ListObserver_1_Remove_m58946005_gshared (ListObserver_1_t69410794 * __this, Il2CppObject* ___observer0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		ImmutableList_1_t3046996525 * L_0 = (ImmutableList_1_t3046996525 *)__this->get__observers_0();
		NullCheck((ImmutableList_1_t3046996525 *)L_0);
		IObserver_1U5BU5D_t2698891007* L_1 = ((  IObserver_1U5BU5D_t2698891007* (*) (ImmutableList_1_t3046996525 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((ImmutableList_1_t3046996525 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		Il2CppObject* L_2 = ___observer0;
		int32_t L_3 = ((  int32_t (*) (Il2CppObject * /* static, unused */, IObserver_1U5BU5D_t2698891007*, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->method)(NULL /*static, unused*/, (IObserver_1U5BU5D_t2698891007*)L_1, (Il2CppObject*)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		V_0 = (int32_t)L_3;
		int32_t L_4 = V_0;
		if ((((int32_t)L_4) >= ((int32_t)0)))
		{
			goto IL_001b;
		}
	}
	{
		return __this;
	}

IL_001b:
	{
		ImmutableList_1_t3046996525 * L_5 = (ImmutableList_1_t3046996525 *)__this->get__observers_0();
		NullCheck((ImmutableList_1_t3046996525 *)L_5);
		IObserver_1U5BU5D_t2698891007* L_6 = ((  IObserver_1U5BU5D_t2698891007* (*) (ImmutableList_1_t3046996525 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((ImmutableList_1_t3046996525 *)L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		NullCheck(L_6);
		if ((!(((uint32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_6)->max_length))))) == ((uint32_t)2))))
		{
			goto IL_003e;
		}
	}
	{
		ImmutableList_1_t3046996525 * L_7 = (ImmutableList_1_t3046996525 *)__this->get__observers_0();
		NullCheck((ImmutableList_1_t3046996525 *)L_7);
		IObserver_1U5BU5D_t2698891007* L_8 = ((  IObserver_1U5BU5D_t2698891007* (*) (ImmutableList_1_t3046996525 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((ImmutableList_1_t3046996525 *)L_7, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_9 = V_0;
		NullCheck(L_8);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_8, ((int32_t)((int32_t)1-(int32_t)L_9)));
		int32_t L_10 = ((int32_t)((int32_t)1-(int32_t)L_9));
		return ((L_8)->GetAt(static_cast<il2cpp_array_size_t>(L_10)));
	}

IL_003e:
	{
		ImmutableList_1_t3046996525 * L_11 = (ImmutableList_1_t3046996525 *)__this->get__observers_0();
		Il2CppObject* L_12 = ___observer0;
		NullCheck((ImmutableList_1_t3046996525 *)L_11);
		ImmutableList_1_t3046996525 * L_13 = ((  ImmutableList_1_t3046996525 * (*) (ImmutableList_1_t3046996525 *, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6)->method)((ImmutableList_1_t3046996525 *)L_11, (Il2CppObject*)L_12, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		ListObserver_1_t69410794 * L_14 = (ListObserver_1_t69410794 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		((  void (*) (ListObserver_1_t69410794 *, ImmutableList_1_t3046996525 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)(L_14, (ImmutableList_1_t3046996525 *)L_13, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		return L_14;
	}
}
// System.Void UniRx.InternalUtil.ListObserver`1<UniRx.CollectionReplaceEvent`1<System.Object>>::.ctor(UniRx.InternalUtil.ImmutableList`1<UniRx.IObserver`1<T>>)
extern "C"  void ListObserver_1__ctor_m1345703762_gshared (ListObserver_1_t2136368207 * __this, ImmutableList_1_t818986642 * ___observers0, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		ImmutableList_1_t818986642 * L_0 = ___observers0;
		__this->set__observers_0(L_0);
		return;
	}
}
// System.Void UniRx.InternalUtil.ListObserver`1<UniRx.CollectionReplaceEvent`1<System.Object>>::OnCompleted()
extern "C"  void ListObserver_1_OnCompleted_m4138258954_gshared (ListObserver_1_t2136368207 * __this, const MethodInfo* method)
{
	IObserver_1U5BU5D_t1302543206* V_0 = NULL;
	int32_t V_1 = 0;
	{
		ImmutableList_1_t818986642 * L_0 = (ImmutableList_1_t818986642 *)__this->get__observers_0();
		NullCheck((ImmutableList_1_t818986642 *)L_0);
		IObserver_1U5BU5D_t1302543206* L_1 = ((  IObserver_1U5BU5D_t1302543206* (*) (ImmutableList_1_t818986642 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((ImmutableList_1_t818986642 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		V_0 = (IObserver_1U5BU5D_t1302543206*)L_1;
		V_1 = (int32_t)0;
		goto IL_001f;
	}

IL_0013:
	{
		IObserver_1U5BU5D_t1302543206* L_2 = V_0;
		int32_t L_3 = V_1;
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, L_3);
		int32_t L_4 = L_3;
		NullCheck((Il2CppObject*)((L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_4))));
		InterfaceActionInvoker0::Invoke(0 /* System.Void UniRx.IObserver`1<UniRx.CollectionReplaceEvent`1<System.Object>>::OnCompleted() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), (Il2CppObject*)((L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_4))));
		int32_t L_5 = V_1;
		V_1 = (int32_t)((int32_t)((int32_t)L_5+(int32_t)1));
	}

IL_001f:
	{
		int32_t L_6 = V_1;
		IObserver_1U5BU5D_t1302543206* L_7 = V_0;
		NullCheck(L_7);
		if ((((int32_t)L_6) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_7)->max_length)))))))
		{
			goto IL_0013;
		}
	}
	{
		return;
	}
}
// System.Void UniRx.InternalUtil.ListObserver`1<UniRx.CollectionReplaceEvent`1<System.Object>>::OnError(System.Exception)
extern "C"  void ListObserver_1_OnError_m600228919_gshared (ListObserver_1_t2136368207 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method)
{
	IObserver_1U5BU5D_t1302543206* V_0 = NULL;
	int32_t V_1 = 0;
	{
		ImmutableList_1_t818986642 * L_0 = (ImmutableList_1_t818986642 *)__this->get__observers_0();
		NullCheck((ImmutableList_1_t818986642 *)L_0);
		IObserver_1U5BU5D_t1302543206* L_1 = ((  IObserver_1U5BU5D_t1302543206* (*) (ImmutableList_1_t818986642 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((ImmutableList_1_t818986642 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		V_0 = (IObserver_1U5BU5D_t1302543206*)L_1;
		V_1 = (int32_t)0;
		goto IL_0020;
	}

IL_0013:
	{
		IObserver_1U5BU5D_t1302543206* L_2 = V_0;
		int32_t L_3 = V_1;
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, L_3);
		int32_t L_4 = L_3;
		Exception_t1967233988 * L_5 = ___error0;
		NullCheck((Il2CppObject*)((L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_4))));
		InterfaceActionInvoker1< Exception_t1967233988 * >::Invoke(1 /* System.Void UniRx.IObserver`1<UniRx.CollectionReplaceEvent`1<System.Object>>::OnError(System.Exception) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), (Il2CppObject*)((L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_4))), (Exception_t1967233988 *)L_5);
		int32_t L_6 = V_1;
		V_1 = (int32_t)((int32_t)((int32_t)L_6+(int32_t)1));
	}

IL_0020:
	{
		int32_t L_7 = V_1;
		IObserver_1U5BU5D_t1302543206* L_8 = V_0;
		NullCheck(L_8);
		if ((((int32_t)L_7) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_8)->max_length)))))))
		{
			goto IL_0013;
		}
	}
	{
		return;
	}
}
// System.Void UniRx.InternalUtil.ListObserver`1<UniRx.CollectionReplaceEvent`1<System.Object>>::OnNext(T)
extern "C"  void ListObserver_1_OnNext_m2534013224_gshared (ListObserver_1_t2136368207 * __this, CollectionReplaceEvent_1_t1841750536  ___value0, const MethodInfo* method)
{
	IObserver_1U5BU5D_t1302543206* V_0 = NULL;
	int32_t V_1 = 0;
	{
		ImmutableList_1_t818986642 * L_0 = (ImmutableList_1_t818986642 *)__this->get__observers_0();
		NullCheck((ImmutableList_1_t818986642 *)L_0);
		IObserver_1U5BU5D_t1302543206* L_1 = ((  IObserver_1U5BU5D_t1302543206* (*) (ImmutableList_1_t818986642 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((ImmutableList_1_t818986642 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		V_0 = (IObserver_1U5BU5D_t1302543206*)L_1;
		V_1 = (int32_t)0;
		goto IL_0020;
	}

IL_0013:
	{
		IObserver_1U5BU5D_t1302543206* L_2 = V_0;
		int32_t L_3 = V_1;
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, L_3);
		int32_t L_4 = L_3;
		CollectionReplaceEvent_1_t1841750536  L_5 = ___value0;
		NullCheck((Il2CppObject*)((L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_4))));
		InterfaceActionInvoker1< CollectionReplaceEvent_1_t1841750536  >::Invoke(2 /* System.Void UniRx.IObserver`1<UniRx.CollectionReplaceEvent`1<System.Object>>::OnNext(T) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), (Il2CppObject*)((L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_4))), (CollectionReplaceEvent_1_t1841750536 )L_5);
		int32_t L_6 = V_1;
		V_1 = (int32_t)((int32_t)((int32_t)L_6+(int32_t)1));
	}

IL_0020:
	{
		int32_t L_7 = V_1;
		IObserver_1U5BU5D_t1302543206* L_8 = V_0;
		NullCheck(L_8);
		if ((((int32_t)L_7) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_8)->max_length)))))))
		{
			goto IL_0013;
		}
	}
	{
		return;
	}
}
// UniRx.IObserver`1<T> UniRx.InternalUtil.ListObserver`1<UniRx.CollectionReplaceEvent`1<System.Object>>::Add(UniRx.IObserver`1<T>)
extern "C"  Il2CppObject* ListObserver_1_Add_m2209425082_gshared (ListObserver_1_t2136368207 * __this, Il2CppObject* ___observer0, const MethodInfo* method)
{
	{
		ImmutableList_1_t818986642 * L_0 = (ImmutableList_1_t818986642 *)__this->get__observers_0();
		Il2CppObject* L_1 = ___observer0;
		NullCheck((ImmutableList_1_t818986642 *)L_0);
		ImmutableList_1_t818986642 * L_2 = ((  ImmutableList_1_t818986642 * (*) (ImmutableList_1_t818986642 *, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)((ImmutableList_1_t818986642 *)L_0, (Il2CppObject*)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		ListObserver_1_t2136368207 * L_3 = (ListObserver_1_t2136368207 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		((  void (*) (ListObserver_1_t2136368207 *, ImmutableList_1_t818986642 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)(L_3, (ImmutableList_1_t818986642 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		return L_3;
	}
}
// UniRx.IObserver`1<T> UniRx.InternalUtil.ListObserver`1<UniRx.CollectionReplaceEvent`1<System.Object>>::Remove(UniRx.IObserver`1<T>)
extern "C"  Il2CppObject* ListObserver_1_Remove_m1042252369_gshared (ListObserver_1_t2136368207 * __this, Il2CppObject* ___observer0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		ImmutableList_1_t818986642 * L_0 = (ImmutableList_1_t818986642 *)__this->get__observers_0();
		NullCheck((ImmutableList_1_t818986642 *)L_0);
		IObserver_1U5BU5D_t1302543206* L_1 = ((  IObserver_1U5BU5D_t1302543206* (*) (ImmutableList_1_t818986642 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((ImmutableList_1_t818986642 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		Il2CppObject* L_2 = ___observer0;
		int32_t L_3 = ((  int32_t (*) (Il2CppObject * /* static, unused */, IObserver_1U5BU5D_t1302543206*, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->method)(NULL /*static, unused*/, (IObserver_1U5BU5D_t1302543206*)L_1, (Il2CppObject*)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		V_0 = (int32_t)L_3;
		int32_t L_4 = V_0;
		if ((((int32_t)L_4) >= ((int32_t)0)))
		{
			goto IL_001b;
		}
	}
	{
		return __this;
	}

IL_001b:
	{
		ImmutableList_1_t818986642 * L_5 = (ImmutableList_1_t818986642 *)__this->get__observers_0();
		NullCheck((ImmutableList_1_t818986642 *)L_5);
		IObserver_1U5BU5D_t1302543206* L_6 = ((  IObserver_1U5BU5D_t1302543206* (*) (ImmutableList_1_t818986642 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((ImmutableList_1_t818986642 *)L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		NullCheck(L_6);
		if ((!(((uint32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_6)->max_length))))) == ((uint32_t)2))))
		{
			goto IL_003e;
		}
	}
	{
		ImmutableList_1_t818986642 * L_7 = (ImmutableList_1_t818986642 *)__this->get__observers_0();
		NullCheck((ImmutableList_1_t818986642 *)L_7);
		IObserver_1U5BU5D_t1302543206* L_8 = ((  IObserver_1U5BU5D_t1302543206* (*) (ImmutableList_1_t818986642 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((ImmutableList_1_t818986642 *)L_7, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_9 = V_0;
		NullCheck(L_8);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_8, ((int32_t)((int32_t)1-(int32_t)L_9)));
		int32_t L_10 = ((int32_t)((int32_t)1-(int32_t)L_9));
		return ((L_8)->GetAt(static_cast<il2cpp_array_size_t>(L_10)));
	}

IL_003e:
	{
		ImmutableList_1_t818986642 * L_11 = (ImmutableList_1_t818986642 *)__this->get__observers_0();
		Il2CppObject* L_12 = ___observer0;
		NullCheck((ImmutableList_1_t818986642 *)L_11);
		ImmutableList_1_t818986642 * L_13 = ((  ImmutableList_1_t818986642 * (*) (ImmutableList_1_t818986642 *, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6)->method)((ImmutableList_1_t818986642 *)L_11, (Il2CppObject*)L_12, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		ListObserver_1_t2136368207 * L_14 = (ListObserver_1_t2136368207 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		((  void (*) (ListObserver_1_t2136368207 *, ImmutableList_1_t818986642 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)(L_14, (ImmutableList_1_t818986642 *)L_13, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		return L_14;
	}
}
// System.Void UniRx.InternalUtil.ListObserver`1<UniRx.CollectionReplaceEvent`1<UniRx.Tuple`2<System.Object,System.Object>>>::.ctor(UniRx.InternalUtil.ImmutableList`1<UniRx.IObserver`1<T>>)
extern "C"  void ListObserver_1__ctor_m2014486450_gshared (ListObserver_1_t1668523606 * __this, ImmutableList_1_t351142041 * ___observers0, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		ImmutableList_1_t351142041 * L_0 = ___observers0;
		__this->set__observers_0(L_0);
		return;
	}
}
// System.Void UniRx.InternalUtil.ListObserver`1<UniRx.CollectionReplaceEvent`1<UniRx.Tuple`2<System.Object,System.Object>>>::OnCompleted()
extern "C"  void ListObserver_1_OnCompleted_m798259818_gshared (ListObserver_1_t1668523606 * __this, const MethodInfo* method)
{
	IObserver_1U5BU5D_t893986147* V_0 = NULL;
	int32_t V_1 = 0;
	{
		ImmutableList_1_t351142041 * L_0 = (ImmutableList_1_t351142041 *)__this->get__observers_0();
		NullCheck((ImmutableList_1_t351142041 *)L_0);
		IObserver_1U5BU5D_t893986147* L_1 = ((  IObserver_1U5BU5D_t893986147* (*) (ImmutableList_1_t351142041 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((ImmutableList_1_t351142041 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		V_0 = (IObserver_1U5BU5D_t893986147*)L_1;
		V_1 = (int32_t)0;
		goto IL_001f;
	}

IL_0013:
	{
		IObserver_1U5BU5D_t893986147* L_2 = V_0;
		int32_t L_3 = V_1;
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, L_3);
		int32_t L_4 = L_3;
		NullCheck((Il2CppObject*)((L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_4))));
		InterfaceActionInvoker0::Invoke(0 /* System.Void UniRx.IObserver`1<UniRx.CollectionReplaceEvent`1<UniRx.Tuple`2<System.Object,System.Object>>>::OnCompleted() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), (Il2CppObject*)((L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_4))));
		int32_t L_5 = V_1;
		V_1 = (int32_t)((int32_t)((int32_t)L_5+(int32_t)1));
	}

IL_001f:
	{
		int32_t L_6 = V_1;
		IObserver_1U5BU5D_t893986147* L_7 = V_0;
		NullCheck(L_7);
		if ((((int32_t)L_6) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_7)->max_length)))))))
		{
			goto IL_0013;
		}
	}
	{
		return;
	}
}
// System.Void UniRx.InternalUtil.ListObserver`1<UniRx.CollectionReplaceEvent`1<UniRx.Tuple`2<System.Object,System.Object>>>::OnError(System.Exception)
extern "C"  void ListObserver_1_OnError_m3248020631_gshared (ListObserver_1_t1668523606 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method)
{
	IObserver_1U5BU5D_t893986147* V_0 = NULL;
	int32_t V_1 = 0;
	{
		ImmutableList_1_t351142041 * L_0 = (ImmutableList_1_t351142041 *)__this->get__observers_0();
		NullCheck((ImmutableList_1_t351142041 *)L_0);
		IObserver_1U5BU5D_t893986147* L_1 = ((  IObserver_1U5BU5D_t893986147* (*) (ImmutableList_1_t351142041 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((ImmutableList_1_t351142041 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		V_0 = (IObserver_1U5BU5D_t893986147*)L_1;
		V_1 = (int32_t)0;
		goto IL_0020;
	}

IL_0013:
	{
		IObserver_1U5BU5D_t893986147* L_2 = V_0;
		int32_t L_3 = V_1;
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, L_3);
		int32_t L_4 = L_3;
		Exception_t1967233988 * L_5 = ___error0;
		NullCheck((Il2CppObject*)((L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_4))));
		InterfaceActionInvoker1< Exception_t1967233988 * >::Invoke(1 /* System.Void UniRx.IObserver`1<UniRx.CollectionReplaceEvent`1<UniRx.Tuple`2<System.Object,System.Object>>>::OnError(System.Exception) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), (Il2CppObject*)((L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_4))), (Exception_t1967233988 *)L_5);
		int32_t L_6 = V_1;
		V_1 = (int32_t)((int32_t)((int32_t)L_6+(int32_t)1));
	}

IL_0020:
	{
		int32_t L_7 = V_1;
		IObserver_1U5BU5D_t893986147* L_8 = V_0;
		NullCheck(L_8);
		if ((((int32_t)L_7) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_8)->max_length)))))))
		{
			goto IL_0013;
		}
	}
	{
		return;
	}
}
// System.Void UniRx.InternalUtil.ListObserver`1<UniRx.CollectionReplaceEvent`1<UniRx.Tuple`2<System.Object,System.Object>>>::OnNext(T)
extern "C"  void ListObserver_1_OnNext_m4258845064_gshared (ListObserver_1_t1668523606 * __this, CollectionReplaceEvent_1_t1373905935  ___value0, const MethodInfo* method)
{
	IObserver_1U5BU5D_t893986147* V_0 = NULL;
	int32_t V_1 = 0;
	{
		ImmutableList_1_t351142041 * L_0 = (ImmutableList_1_t351142041 *)__this->get__observers_0();
		NullCheck((ImmutableList_1_t351142041 *)L_0);
		IObserver_1U5BU5D_t893986147* L_1 = ((  IObserver_1U5BU5D_t893986147* (*) (ImmutableList_1_t351142041 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((ImmutableList_1_t351142041 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		V_0 = (IObserver_1U5BU5D_t893986147*)L_1;
		V_1 = (int32_t)0;
		goto IL_0020;
	}

IL_0013:
	{
		IObserver_1U5BU5D_t893986147* L_2 = V_0;
		int32_t L_3 = V_1;
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, L_3);
		int32_t L_4 = L_3;
		CollectionReplaceEvent_1_t1373905935  L_5 = ___value0;
		NullCheck((Il2CppObject*)((L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_4))));
		InterfaceActionInvoker1< CollectionReplaceEvent_1_t1373905935  >::Invoke(2 /* System.Void UniRx.IObserver`1<UniRx.CollectionReplaceEvent`1<UniRx.Tuple`2<System.Object,System.Object>>>::OnNext(T) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), (Il2CppObject*)((L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_4))), (CollectionReplaceEvent_1_t1373905935 )L_5);
		int32_t L_6 = V_1;
		V_1 = (int32_t)((int32_t)((int32_t)L_6+(int32_t)1));
	}

IL_0020:
	{
		int32_t L_7 = V_1;
		IObserver_1U5BU5D_t893986147* L_8 = V_0;
		NullCheck(L_8);
		if ((((int32_t)L_7) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_8)->max_length)))))))
		{
			goto IL_0013;
		}
	}
	{
		return;
	}
}
// UniRx.IObserver`1<T> UniRx.InternalUtil.ListObserver`1<UniRx.CollectionReplaceEvent`1<UniRx.Tuple`2<System.Object,System.Object>>>::Add(UniRx.IObserver`1<T>)
extern "C"  Il2CppObject* ListObserver_1_Add_m1913525288_gshared (ListObserver_1_t1668523606 * __this, Il2CppObject* ___observer0, const MethodInfo* method)
{
	{
		ImmutableList_1_t351142041 * L_0 = (ImmutableList_1_t351142041 *)__this->get__observers_0();
		Il2CppObject* L_1 = ___observer0;
		NullCheck((ImmutableList_1_t351142041 *)L_0);
		ImmutableList_1_t351142041 * L_2 = ((  ImmutableList_1_t351142041 * (*) (ImmutableList_1_t351142041 *, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)((ImmutableList_1_t351142041 *)L_0, (Il2CppObject*)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		ListObserver_1_t1668523606 * L_3 = (ListObserver_1_t1668523606 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		((  void (*) (ListObserver_1_t1668523606 *, ImmutableList_1_t351142041 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)(L_3, (ImmutableList_1_t351142041 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		return L_3;
	}
}
// UniRx.IObserver`1<T> UniRx.InternalUtil.ListObserver`1<UniRx.CollectionReplaceEvent`1<UniRx.Tuple`2<System.Object,System.Object>>>::Remove(UniRx.IObserver`1<T>)
extern "C"  Il2CppObject* ListObserver_1_Remove_m3459348003_gshared (ListObserver_1_t1668523606 * __this, Il2CppObject* ___observer0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		ImmutableList_1_t351142041 * L_0 = (ImmutableList_1_t351142041 *)__this->get__observers_0();
		NullCheck((ImmutableList_1_t351142041 *)L_0);
		IObserver_1U5BU5D_t893986147* L_1 = ((  IObserver_1U5BU5D_t893986147* (*) (ImmutableList_1_t351142041 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((ImmutableList_1_t351142041 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		Il2CppObject* L_2 = ___observer0;
		int32_t L_3 = ((  int32_t (*) (Il2CppObject * /* static, unused */, IObserver_1U5BU5D_t893986147*, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->method)(NULL /*static, unused*/, (IObserver_1U5BU5D_t893986147*)L_1, (Il2CppObject*)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		V_0 = (int32_t)L_3;
		int32_t L_4 = V_0;
		if ((((int32_t)L_4) >= ((int32_t)0)))
		{
			goto IL_001b;
		}
	}
	{
		return __this;
	}

IL_001b:
	{
		ImmutableList_1_t351142041 * L_5 = (ImmutableList_1_t351142041 *)__this->get__observers_0();
		NullCheck((ImmutableList_1_t351142041 *)L_5);
		IObserver_1U5BU5D_t893986147* L_6 = ((  IObserver_1U5BU5D_t893986147* (*) (ImmutableList_1_t351142041 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((ImmutableList_1_t351142041 *)L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		NullCheck(L_6);
		if ((!(((uint32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_6)->max_length))))) == ((uint32_t)2))))
		{
			goto IL_003e;
		}
	}
	{
		ImmutableList_1_t351142041 * L_7 = (ImmutableList_1_t351142041 *)__this->get__observers_0();
		NullCheck((ImmutableList_1_t351142041 *)L_7);
		IObserver_1U5BU5D_t893986147* L_8 = ((  IObserver_1U5BU5D_t893986147* (*) (ImmutableList_1_t351142041 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((ImmutableList_1_t351142041 *)L_7, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_9 = V_0;
		NullCheck(L_8);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_8, ((int32_t)((int32_t)1-(int32_t)L_9)));
		int32_t L_10 = ((int32_t)((int32_t)1-(int32_t)L_9));
		return ((L_8)->GetAt(static_cast<il2cpp_array_size_t>(L_10)));
	}

IL_003e:
	{
		ImmutableList_1_t351142041 * L_11 = (ImmutableList_1_t351142041 *)__this->get__observers_0();
		Il2CppObject* L_12 = ___observer0;
		NullCheck((ImmutableList_1_t351142041 *)L_11);
		ImmutableList_1_t351142041 * L_13 = ((  ImmutableList_1_t351142041 * (*) (ImmutableList_1_t351142041 *, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6)->method)((ImmutableList_1_t351142041 *)L_11, (Il2CppObject*)L_12, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		ListObserver_1_t1668523606 * L_14 = (ListObserver_1_t1668523606 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		((  void (*) (ListObserver_1_t1668523606 *, ImmutableList_1_t351142041 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)(L_14, (ImmutableList_1_t351142041 *)L_13, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		return L_14;
	}
}
// System.Void UniRx.InternalUtil.ListObserver`1<UniRx.CountChangedStatus>::.ctor(UniRx.InternalUtil.ImmutableList`1<UniRx.IObserver`1<T>>)
extern "C"  void ListObserver_1__ctor_m900567354_gshared (ListObserver_1_t4065845392 * __this, ImmutableList_1_t2748463827 * ___observers0, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		ImmutableList_1_t2748463827 * L_0 = ___observers0;
		__this->set__observers_0(L_0);
		return;
	}
}
// System.Void UniRx.InternalUtil.ListObserver`1<UniRx.CountChangedStatus>::OnCompleted()
extern "C"  void ListObserver_1_OnCompleted_m1445354994_gshared (ListObserver_1_t4065845392 * __this, const MethodInfo* method)
{
	IObserver_1U5BU5D_t335784193* V_0 = NULL;
	int32_t V_1 = 0;
	{
		ImmutableList_1_t2748463827 * L_0 = (ImmutableList_1_t2748463827 *)__this->get__observers_0();
		NullCheck((ImmutableList_1_t2748463827 *)L_0);
		IObserver_1U5BU5D_t335784193* L_1 = ((  IObserver_1U5BU5D_t335784193* (*) (ImmutableList_1_t2748463827 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((ImmutableList_1_t2748463827 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		V_0 = (IObserver_1U5BU5D_t335784193*)L_1;
		V_1 = (int32_t)0;
		goto IL_001f;
	}

IL_0013:
	{
		IObserver_1U5BU5D_t335784193* L_2 = V_0;
		int32_t L_3 = V_1;
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, L_3);
		int32_t L_4 = L_3;
		NullCheck((Il2CppObject*)((L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_4))));
		InterfaceActionInvoker0::Invoke(0 /* System.Void UniRx.IObserver`1<UniRx.CountChangedStatus>::OnCompleted() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), (Il2CppObject*)((L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_4))));
		int32_t L_5 = V_1;
		V_1 = (int32_t)((int32_t)((int32_t)L_5+(int32_t)1));
	}

IL_001f:
	{
		int32_t L_6 = V_1;
		IObserver_1U5BU5D_t335784193* L_7 = V_0;
		NullCheck(L_7);
		if ((((int32_t)L_6) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_7)->max_length)))))))
		{
			goto IL_0013;
		}
	}
	{
		return;
	}
}
// System.Void UniRx.InternalUtil.ListObserver`1<UniRx.CountChangedStatus>::OnError(System.Exception)
extern "C"  void ListObserver_1_OnError_m2064233503_gshared (ListObserver_1_t4065845392 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method)
{
	IObserver_1U5BU5D_t335784193* V_0 = NULL;
	int32_t V_1 = 0;
	{
		ImmutableList_1_t2748463827 * L_0 = (ImmutableList_1_t2748463827 *)__this->get__observers_0();
		NullCheck((ImmutableList_1_t2748463827 *)L_0);
		IObserver_1U5BU5D_t335784193* L_1 = ((  IObserver_1U5BU5D_t335784193* (*) (ImmutableList_1_t2748463827 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((ImmutableList_1_t2748463827 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		V_0 = (IObserver_1U5BU5D_t335784193*)L_1;
		V_1 = (int32_t)0;
		goto IL_0020;
	}

IL_0013:
	{
		IObserver_1U5BU5D_t335784193* L_2 = V_0;
		int32_t L_3 = V_1;
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, L_3);
		int32_t L_4 = L_3;
		Exception_t1967233988 * L_5 = ___error0;
		NullCheck((Il2CppObject*)((L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_4))));
		InterfaceActionInvoker1< Exception_t1967233988 * >::Invoke(1 /* System.Void UniRx.IObserver`1<UniRx.CountChangedStatus>::OnError(System.Exception) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), (Il2CppObject*)((L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_4))), (Exception_t1967233988 *)L_5);
		int32_t L_6 = V_1;
		V_1 = (int32_t)((int32_t)((int32_t)L_6+(int32_t)1));
	}

IL_0020:
	{
		int32_t L_7 = V_1;
		IObserver_1U5BU5D_t335784193* L_8 = V_0;
		NullCheck(L_8);
		if ((((int32_t)L_7) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_8)->max_length)))))))
		{
			goto IL_0013;
		}
	}
	{
		return;
	}
}
// System.Void UniRx.InternalUtil.ListObserver`1<UniRx.CountChangedStatus>::OnNext(T)
extern "C"  void ListObserver_1_OnNext_m1438234896_gshared (ListObserver_1_t4065845392 * __this, int32_t ___value0, const MethodInfo* method)
{
	IObserver_1U5BU5D_t335784193* V_0 = NULL;
	int32_t V_1 = 0;
	{
		ImmutableList_1_t2748463827 * L_0 = (ImmutableList_1_t2748463827 *)__this->get__observers_0();
		NullCheck((ImmutableList_1_t2748463827 *)L_0);
		IObserver_1U5BU5D_t335784193* L_1 = ((  IObserver_1U5BU5D_t335784193* (*) (ImmutableList_1_t2748463827 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((ImmutableList_1_t2748463827 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		V_0 = (IObserver_1U5BU5D_t335784193*)L_1;
		V_1 = (int32_t)0;
		goto IL_0020;
	}

IL_0013:
	{
		IObserver_1U5BU5D_t335784193* L_2 = V_0;
		int32_t L_3 = V_1;
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, L_3);
		int32_t L_4 = L_3;
		int32_t L_5 = ___value0;
		NullCheck((Il2CppObject*)((L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_4))));
		InterfaceActionInvoker1< int32_t >::Invoke(2 /* System.Void UniRx.IObserver`1<UniRx.CountChangedStatus>::OnNext(T) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), (Il2CppObject*)((L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_4))), (int32_t)L_5);
		int32_t L_6 = V_1;
		V_1 = (int32_t)((int32_t)((int32_t)L_6+(int32_t)1));
	}

IL_0020:
	{
		int32_t L_7 = V_1;
		IObserver_1U5BU5D_t335784193* L_8 = V_0;
		NullCheck(L_8);
		if ((((int32_t)L_7) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_8)->max_length)))))))
		{
			goto IL_0013;
		}
	}
	{
		return;
	}
}
// UniRx.IObserver`1<T> UniRx.InternalUtil.ListObserver`1<UniRx.CountChangedStatus>::Add(UniRx.IObserver`1<T>)
extern "C"  Il2CppObject* ListObserver_1_Add_m3716145008_gshared (ListObserver_1_t4065845392 * __this, Il2CppObject* ___observer0, const MethodInfo* method)
{
	{
		ImmutableList_1_t2748463827 * L_0 = (ImmutableList_1_t2748463827 *)__this->get__observers_0();
		Il2CppObject* L_1 = ___observer0;
		NullCheck((ImmutableList_1_t2748463827 *)L_0);
		ImmutableList_1_t2748463827 * L_2 = ((  ImmutableList_1_t2748463827 * (*) (ImmutableList_1_t2748463827 *, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)((ImmutableList_1_t2748463827 *)L_0, (Il2CppObject*)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		ListObserver_1_t4065845392 * L_3 = (ListObserver_1_t4065845392 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		((  void (*) (ListObserver_1_t4065845392 *, ImmutableList_1_t2748463827 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)(L_3, (ImmutableList_1_t2748463827 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		return L_3;
	}
}
// UniRx.IObserver`1<T> UniRx.InternalUtil.ListObserver`1<UniRx.CountChangedStatus>::Remove(UniRx.IObserver`1<T>)
extern "C"  Il2CppObject* ListObserver_1_Remove_m1032357339_gshared (ListObserver_1_t4065845392 * __this, Il2CppObject* ___observer0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		ImmutableList_1_t2748463827 * L_0 = (ImmutableList_1_t2748463827 *)__this->get__observers_0();
		NullCheck((ImmutableList_1_t2748463827 *)L_0);
		IObserver_1U5BU5D_t335784193* L_1 = ((  IObserver_1U5BU5D_t335784193* (*) (ImmutableList_1_t2748463827 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((ImmutableList_1_t2748463827 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		Il2CppObject* L_2 = ___observer0;
		int32_t L_3 = ((  int32_t (*) (Il2CppObject * /* static, unused */, IObserver_1U5BU5D_t335784193*, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->method)(NULL /*static, unused*/, (IObserver_1U5BU5D_t335784193*)L_1, (Il2CppObject*)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		V_0 = (int32_t)L_3;
		int32_t L_4 = V_0;
		if ((((int32_t)L_4) >= ((int32_t)0)))
		{
			goto IL_001b;
		}
	}
	{
		return __this;
	}

IL_001b:
	{
		ImmutableList_1_t2748463827 * L_5 = (ImmutableList_1_t2748463827 *)__this->get__observers_0();
		NullCheck((ImmutableList_1_t2748463827 *)L_5);
		IObserver_1U5BU5D_t335784193* L_6 = ((  IObserver_1U5BU5D_t335784193* (*) (ImmutableList_1_t2748463827 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((ImmutableList_1_t2748463827 *)L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		NullCheck(L_6);
		if ((!(((uint32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_6)->max_length))))) == ((uint32_t)2))))
		{
			goto IL_003e;
		}
	}
	{
		ImmutableList_1_t2748463827 * L_7 = (ImmutableList_1_t2748463827 *)__this->get__observers_0();
		NullCheck((ImmutableList_1_t2748463827 *)L_7);
		IObserver_1U5BU5D_t335784193* L_8 = ((  IObserver_1U5BU5D_t335784193* (*) (ImmutableList_1_t2748463827 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((ImmutableList_1_t2748463827 *)L_7, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_9 = V_0;
		NullCheck(L_8);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_8, ((int32_t)((int32_t)1-(int32_t)L_9)));
		int32_t L_10 = ((int32_t)((int32_t)1-(int32_t)L_9));
		return ((L_8)->GetAt(static_cast<il2cpp_array_size_t>(L_10)));
	}

IL_003e:
	{
		ImmutableList_1_t2748463827 * L_11 = (ImmutableList_1_t2748463827 *)__this->get__observers_0();
		Il2CppObject* L_12 = ___observer0;
		NullCheck((ImmutableList_1_t2748463827 *)L_11);
		ImmutableList_1_t2748463827 * L_13 = ((  ImmutableList_1_t2748463827 * (*) (ImmutableList_1_t2748463827 *, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6)->method)((ImmutableList_1_t2748463827 *)L_11, (Il2CppObject*)L_12, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		ListObserver_1_t4065845392 * L_14 = (ListObserver_1_t4065845392 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		((  void (*) (ListObserver_1_t4065845392 *, ImmutableList_1_t2748463827 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)(L_14, (ImmutableList_1_t2748463827 *)L_13, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		return L_14;
	}
}
// System.Void UniRx.InternalUtil.ListObserver`1<UniRx.DictionaryAddEvent`2<System.Object,System.Object>>::.ctor(UniRx.InternalUtil.ImmutableList`1<UniRx.IObserver`1<T>>)
extern "C"  void ListObserver_1__ctor_m707792948_gshared (ListObserver_1_t545598679 * __this, ImmutableList_1_t3523184410 * ___observers0, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		ImmutableList_1_t3523184410 * L_0 = ___observers0;
		__this->set__observers_0(L_0);
		return;
	}
}
// System.Void UniRx.InternalUtil.ListObserver`1<UniRx.DictionaryAddEvent`2<System.Object,System.Object>>::OnCompleted()
extern "C"  void ListObserver_1_OnCompleted_m1568862060_gshared (ListObserver_1_t545598679 * __this, const MethodInfo* method)
{
	IObserver_1U5BU5D_t2153699262* V_0 = NULL;
	int32_t V_1 = 0;
	{
		ImmutableList_1_t3523184410 * L_0 = (ImmutableList_1_t3523184410 *)__this->get__observers_0();
		NullCheck((ImmutableList_1_t3523184410 *)L_0);
		IObserver_1U5BU5D_t2153699262* L_1 = ((  IObserver_1U5BU5D_t2153699262* (*) (ImmutableList_1_t3523184410 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((ImmutableList_1_t3523184410 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		V_0 = (IObserver_1U5BU5D_t2153699262*)L_1;
		V_1 = (int32_t)0;
		goto IL_001f;
	}

IL_0013:
	{
		IObserver_1U5BU5D_t2153699262* L_2 = V_0;
		int32_t L_3 = V_1;
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, L_3);
		int32_t L_4 = L_3;
		NullCheck((Il2CppObject*)((L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_4))));
		InterfaceActionInvoker0::Invoke(0 /* System.Void UniRx.IObserver`1<UniRx.DictionaryAddEvent`2<System.Object,System.Object>>::OnCompleted() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), (Il2CppObject*)((L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_4))));
		int32_t L_5 = V_1;
		V_1 = (int32_t)((int32_t)((int32_t)L_5+(int32_t)1));
	}

IL_001f:
	{
		int32_t L_6 = V_1;
		IObserver_1U5BU5D_t2153699262* L_7 = V_0;
		NullCheck(L_7);
		if ((((int32_t)L_6) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_7)->max_length)))))))
		{
			goto IL_0013;
		}
	}
	{
		return;
	}
}
// System.Void UniRx.InternalUtil.ListObserver`1<UniRx.DictionaryAddEvent`2<System.Object,System.Object>>::OnError(System.Exception)
extern "C"  void ListObserver_1_OnError_m965074585_gshared (ListObserver_1_t545598679 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method)
{
	IObserver_1U5BU5D_t2153699262* V_0 = NULL;
	int32_t V_1 = 0;
	{
		ImmutableList_1_t3523184410 * L_0 = (ImmutableList_1_t3523184410 *)__this->get__observers_0();
		NullCheck((ImmutableList_1_t3523184410 *)L_0);
		IObserver_1U5BU5D_t2153699262* L_1 = ((  IObserver_1U5BU5D_t2153699262* (*) (ImmutableList_1_t3523184410 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((ImmutableList_1_t3523184410 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		V_0 = (IObserver_1U5BU5D_t2153699262*)L_1;
		V_1 = (int32_t)0;
		goto IL_0020;
	}

IL_0013:
	{
		IObserver_1U5BU5D_t2153699262* L_2 = V_0;
		int32_t L_3 = V_1;
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, L_3);
		int32_t L_4 = L_3;
		Exception_t1967233988 * L_5 = ___error0;
		NullCheck((Il2CppObject*)((L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_4))));
		InterfaceActionInvoker1< Exception_t1967233988 * >::Invoke(1 /* System.Void UniRx.IObserver`1<UniRx.DictionaryAddEvent`2<System.Object,System.Object>>::OnError(System.Exception) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), (Il2CppObject*)((L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_4))), (Exception_t1967233988 *)L_5);
		int32_t L_6 = V_1;
		V_1 = (int32_t)((int32_t)((int32_t)L_6+(int32_t)1));
	}

IL_0020:
	{
		int32_t L_7 = V_1;
		IObserver_1U5BU5D_t2153699262* L_8 = V_0;
		NullCheck(L_8);
		if ((((int32_t)L_7) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_8)->max_length)))))))
		{
			goto IL_0013;
		}
	}
	{
		return;
	}
}
// System.Void UniRx.InternalUtil.ListObserver`1<UniRx.DictionaryAddEvent`2<System.Object,System.Object>>::OnNext(T)
extern "C"  void ListObserver_1_OnNext_m3418232714_gshared (ListObserver_1_t545598679 * __this, DictionaryAddEvent_2_t250981008  ___value0, const MethodInfo* method)
{
	IObserver_1U5BU5D_t2153699262* V_0 = NULL;
	int32_t V_1 = 0;
	{
		ImmutableList_1_t3523184410 * L_0 = (ImmutableList_1_t3523184410 *)__this->get__observers_0();
		NullCheck((ImmutableList_1_t3523184410 *)L_0);
		IObserver_1U5BU5D_t2153699262* L_1 = ((  IObserver_1U5BU5D_t2153699262* (*) (ImmutableList_1_t3523184410 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((ImmutableList_1_t3523184410 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		V_0 = (IObserver_1U5BU5D_t2153699262*)L_1;
		V_1 = (int32_t)0;
		goto IL_0020;
	}

IL_0013:
	{
		IObserver_1U5BU5D_t2153699262* L_2 = V_0;
		int32_t L_3 = V_1;
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, L_3);
		int32_t L_4 = L_3;
		DictionaryAddEvent_2_t250981008  L_5 = ___value0;
		NullCheck((Il2CppObject*)((L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_4))));
		InterfaceActionInvoker1< DictionaryAddEvent_2_t250981008  >::Invoke(2 /* System.Void UniRx.IObserver`1<UniRx.DictionaryAddEvent`2<System.Object,System.Object>>::OnNext(T) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), (Il2CppObject*)((L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_4))), (DictionaryAddEvent_2_t250981008 )L_5);
		int32_t L_6 = V_1;
		V_1 = (int32_t)((int32_t)((int32_t)L_6+(int32_t)1));
	}

IL_0020:
	{
		int32_t L_7 = V_1;
		IObserver_1U5BU5D_t2153699262* L_8 = V_0;
		NullCheck(L_8);
		if ((((int32_t)L_7) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_8)->max_length)))))))
		{
			goto IL_0013;
		}
	}
	{
		return;
	}
}
// UniRx.IObserver`1<T> UniRx.InternalUtil.ListObserver`1<UniRx.DictionaryAddEvent`2<System.Object,System.Object>>::Add(UniRx.IObserver`1<T>)
extern "C"  Il2CppObject* ListObserver_1_Add_m1799248348_gshared (ListObserver_1_t545598679 * __this, Il2CppObject* ___observer0, const MethodInfo* method)
{
	{
		ImmutableList_1_t3523184410 * L_0 = (ImmutableList_1_t3523184410 *)__this->get__observers_0();
		Il2CppObject* L_1 = ___observer0;
		NullCheck((ImmutableList_1_t3523184410 *)L_0);
		ImmutableList_1_t3523184410 * L_2 = ((  ImmutableList_1_t3523184410 * (*) (ImmutableList_1_t3523184410 *, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)((ImmutableList_1_t3523184410 *)L_0, (Il2CppObject*)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		ListObserver_1_t545598679 * L_3 = (ListObserver_1_t545598679 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		((  void (*) (ListObserver_1_t545598679 *, ImmutableList_1_t3523184410 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)(L_3, (ImmutableList_1_t3523184410 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		return L_3;
	}
}
// UniRx.IObserver`1<T> UniRx.InternalUtil.ListObserver`1<UniRx.DictionaryAddEvent`2<System.Object,System.Object>>::Remove(UniRx.IObserver`1<T>)
extern "C"  Il2CppObject* ListObserver_1_Remove_m649126895_gshared (ListObserver_1_t545598679 * __this, Il2CppObject* ___observer0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		ImmutableList_1_t3523184410 * L_0 = (ImmutableList_1_t3523184410 *)__this->get__observers_0();
		NullCheck((ImmutableList_1_t3523184410 *)L_0);
		IObserver_1U5BU5D_t2153699262* L_1 = ((  IObserver_1U5BU5D_t2153699262* (*) (ImmutableList_1_t3523184410 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((ImmutableList_1_t3523184410 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		Il2CppObject* L_2 = ___observer0;
		int32_t L_3 = ((  int32_t (*) (Il2CppObject * /* static, unused */, IObserver_1U5BU5D_t2153699262*, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->method)(NULL /*static, unused*/, (IObserver_1U5BU5D_t2153699262*)L_1, (Il2CppObject*)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		V_0 = (int32_t)L_3;
		int32_t L_4 = V_0;
		if ((((int32_t)L_4) >= ((int32_t)0)))
		{
			goto IL_001b;
		}
	}
	{
		return __this;
	}

IL_001b:
	{
		ImmutableList_1_t3523184410 * L_5 = (ImmutableList_1_t3523184410 *)__this->get__observers_0();
		NullCheck((ImmutableList_1_t3523184410 *)L_5);
		IObserver_1U5BU5D_t2153699262* L_6 = ((  IObserver_1U5BU5D_t2153699262* (*) (ImmutableList_1_t3523184410 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((ImmutableList_1_t3523184410 *)L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		NullCheck(L_6);
		if ((!(((uint32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_6)->max_length))))) == ((uint32_t)2))))
		{
			goto IL_003e;
		}
	}
	{
		ImmutableList_1_t3523184410 * L_7 = (ImmutableList_1_t3523184410 *)__this->get__observers_0();
		NullCheck((ImmutableList_1_t3523184410 *)L_7);
		IObserver_1U5BU5D_t2153699262* L_8 = ((  IObserver_1U5BU5D_t2153699262* (*) (ImmutableList_1_t3523184410 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((ImmutableList_1_t3523184410 *)L_7, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_9 = V_0;
		NullCheck(L_8);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_8, ((int32_t)((int32_t)1-(int32_t)L_9)));
		int32_t L_10 = ((int32_t)((int32_t)1-(int32_t)L_9));
		return ((L_8)->GetAt(static_cast<il2cpp_array_size_t>(L_10)));
	}

IL_003e:
	{
		ImmutableList_1_t3523184410 * L_11 = (ImmutableList_1_t3523184410 *)__this->get__observers_0();
		Il2CppObject* L_12 = ___observer0;
		NullCheck((ImmutableList_1_t3523184410 *)L_11);
		ImmutableList_1_t3523184410 * L_13 = ((  ImmutableList_1_t3523184410 * (*) (ImmutableList_1_t3523184410 *, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6)->method)((ImmutableList_1_t3523184410 *)L_11, (Il2CppObject*)L_12, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		ListObserver_1_t545598679 * L_14 = (ListObserver_1_t545598679 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		((  void (*) (ListObserver_1_t545598679 *, ImmutableList_1_t3523184410 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)(L_14, (ImmutableList_1_t3523184410 *)L_13, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		return L_14;
	}
}
// System.Void UniRx.InternalUtil.ListObserver`1<UniRx.DictionaryRemoveEvent`2<System.Object,System.Object>>::.ctor(UniRx.InternalUtil.ImmutableList`1<UniRx.IObserver`1<T>>)
extern "C"  void ListObserver_1__ctor_m1993395291_gshared (ListObserver_1_t3616905962 * __this, ImmutableList_1_t2299524397 * ___observers0, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		ImmutableList_1_t2299524397 * L_0 = ___observers0;
		__this->set__observers_0(L_0);
		return;
	}
}
// System.Void UniRx.InternalUtil.ListObserver`1<UniRx.DictionaryRemoveEvent`2<System.Object,System.Object>>::OnCompleted()
extern "C"  void ListObserver_1_OnCompleted_m2339017555_gshared (ListObserver_1_t3616905962 * __this, const MethodInfo* method)
{
	IObserver_1U5BU5D_t329451519* V_0 = NULL;
	int32_t V_1 = 0;
	{
		ImmutableList_1_t2299524397 * L_0 = (ImmutableList_1_t2299524397 *)__this->get__observers_0();
		NullCheck((ImmutableList_1_t2299524397 *)L_0);
		IObserver_1U5BU5D_t329451519* L_1 = ((  IObserver_1U5BU5D_t329451519* (*) (ImmutableList_1_t2299524397 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((ImmutableList_1_t2299524397 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		V_0 = (IObserver_1U5BU5D_t329451519*)L_1;
		V_1 = (int32_t)0;
		goto IL_001f;
	}

IL_0013:
	{
		IObserver_1U5BU5D_t329451519* L_2 = V_0;
		int32_t L_3 = V_1;
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, L_3);
		int32_t L_4 = L_3;
		NullCheck((Il2CppObject*)((L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_4))));
		InterfaceActionInvoker0::Invoke(0 /* System.Void UniRx.IObserver`1<UniRx.DictionaryRemoveEvent`2<System.Object,System.Object>>::OnCompleted() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), (Il2CppObject*)((L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_4))));
		int32_t L_5 = V_1;
		V_1 = (int32_t)((int32_t)((int32_t)L_5+(int32_t)1));
	}

IL_001f:
	{
		int32_t L_6 = V_1;
		IObserver_1U5BU5D_t329451519* L_7 = V_0;
		NullCheck(L_7);
		if ((((int32_t)L_6) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_7)->max_length)))))))
		{
			goto IL_0013;
		}
	}
	{
		return;
	}
}
// System.Void UniRx.InternalUtil.ListObserver`1<UniRx.DictionaryRemoveEvent`2<System.Object,System.Object>>::OnError(System.Exception)
extern "C"  void ListObserver_1_OnError_m1783357440_gshared (ListObserver_1_t3616905962 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method)
{
	IObserver_1U5BU5D_t329451519* V_0 = NULL;
	int32_t V_1 = 0;
	{
		ImmutableList_1_t2299524397 * L_0 = (ImmutableList_1_t2299524397 *)__this->get__observers_0();
		NullCheck((ImmutableList_1_t2299524397 *)L_0);
		IObserver_1U5BU5D_t329451519* L_1 = ((  IObserver_1U5BU5D_t329451519* (*) (ImmutableList_1_t2299524397 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((ImmutableList_1_t2299524397 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		V_0 = (IObserver_1U5BU5D_t329451519*)L_1;
		V_1 = (int32_t)0;
		goto IL_0020;
	}

IL_0013:
	{
		IObserver_1U5BU5D_t329451519* L_2 = V_0;
		int32_t L_3 = V_1;
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, L_3);
		int32_t L_4 = L_3;
		Exception_t1967233988 * L_5 = ___error0;
		NullCheck((Il2CppObject*)((L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_4))));
		InterfaceActionInvoker1< Exception_t1967233988 * >::Invoke(1 /* System.Void UniRx.IObserver`1<UniRx.DictionaryRemoveEvent`2<System.Object,System.Object>>::OnError(System.Exception) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), (Il2CppObject*)((L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_4))), (Exception_t1967233988 *)L_5);
		int32_t L_6 = V_1;
		V_1 = (int32_t)((int32_t)((int32_t)L_6+(int32_t)1));
	}

IL_0020:
	{
		int32_t L_7 = V_1;
		IObserver_1U5BU5D_t329451519* L_8 = V_0;
		NullCheck(L_8);
		if ((((int32_t)L_7) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_8)->max_length)))))))
		{
			goto IL_0013;
		}
	}
	{
		return;
	}
}
// System.Void UniRx.InternalUtil.ListObserver`1<UniRx.DictionaryRemoveEvent`2<System.Object,System.Object>>::OnNext(T)
extern "C"  void ListObserver_1_OnNext_m527025905_gshared (ListObserver_1_t3616905962 * __this, DictionaryRemoveEvent_2_t3322288291  ___value0, const MethodInfo* method)
{
	IObserver_1U5BU5D_t329451519* V_0 = NULL;
	int32_t V_1 = 0;
	{
		ImmutableList_1_t2299524397 * L_0 = (ImmutableList_1_t2299524397 *)__this->get__observers_0();
		NullCheck((ImmutableList_1_t2299524397 *)L_0);
		IObserver_1U5BU5D_t329451519* L_1 = ((  IObserver_1U5BU5D_t329451519* (*) (ImmutableList_1_t2299524397 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((ImmutableList_1_t2299524397 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		V_0 = (IObserver_1U5BU5D_t329451519*)L_1;
		V_1 = (int32_t)0;
		goto IL_0020;
	}

IL_0013:
	{
		IObserver_1U5BU5D_t329451519* L_2 = V_0;
		int32_t L_3 = V_1;
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, L_3);
		int32_t L_4 = L_3;
		DictionaryRemoveEvent_2_t3322288291  L_5 = ___value0;
		NullCheck((Il2CppObject*)((L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_4))));
		InterfaceActionInvoker1< DictionaryRemoveEvent_2_t3322288291  >::Invoke(2 /* System.Void UniRx.IObserver`1<UniRx.DictionaryRemoveEvent`2<System.Object,System.Object>>::OnNext(T) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), (Il2CppObject*)((L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_4))), (DictionaryRemoveEvent_2_t3322288291 )L_5);
		int32_t L_6 = V_1;
		V_1 = (int32_t)((int32_t)((int32_t)L_6+(int32_t)1));
	}

IL_0020:
	{
		int32_t L_7 = V_1;
		IObserver_1U5BU5D_t329451519* L_8 = V_0;
		NullCheck(L_8);
		if ((((int32_t)L_7) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_8)->max_length)))))))
		{
			goto IL_0013;
		}
	}
	{
		return;
	}
}
// UniRx.IObserver`1<T> UniRx.InternalUtil.ListObserver`1<UniRx.DictionaryRemoveEvent`2<System.Object,System.Object>>::Add(UniRx.IObserver`1<T>)
extern "C"  Il2CppObject* ListObserver_1_Add_m4259816849_gshared (ListObserver_1_t3616905962 * __this, Il2CppObject* ___observer0, const MethodInfo* method)
{
	{
		ImmutableList_1_t2299524397 * L_0 = (ImmutableList_1_t2299524397 *)__this->get__observers_0();
		Il2CppObject* L_1 = ___observer0;
		NullCheck((ImmutableList_1_t2299524397 *)L_0);
		ImmutableList_1_t2299524397 * L_2 = ((  ImmutableList_1_t2299524397 * (*) (ImmutableList_1_t2299524397 *, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)((ImmutableList_1_t2299524397 *)L_0, (Il2CppObject*)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		ListObserver_1_t3616905962 * L_3 = (ListObserver_1_t3616905962 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		((  void (*) (ListObserver_1_t3616905962 *, ImmutableList_1_t2299524397 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)(L_3, (ImmutableList_1_t2299524397 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		return L_3;
	}
}
// UniRx.IObserver`1<T> UniRx.InternalUtil.ListObserver`1<UniRx.DictionaryRemoveEvent`2<System.Object,System.Object>>::Remove(UniRx.IObserver`1<T>)
extern "C"  Il2CppObject* ListObserver_1_Remove_m1238499354_gshared (ListObserver_1_t3616905962 * __this, Il2CppObject* ___observer0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		ImmutableList_1_t2299524397 * L_0 = (ImmutableList_1_t2299524397 *)__this->get__observers_0();
		NullCheck((ImmutableList_1_t2299524397 *)L_0);
		IObserver_1U5BU5D_t329451519* L_1 = ((  IObserver_1U5BU5D_t329451519* (*) (ImmutableList_1_t2299524397 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((ImmutableList_1_t2299524397 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		Il2CppObject* L_2 = ___observer0;
		int32_t L_3 = ((  int32_t (*) (Il2CppObject * /* static, unused */, IObserver_1U5BU5D_t329451519*, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->method)(NULL /*static, unused*/, (IObserver_1U5BU5D_t329451519*)L_1, (Il2CppObject*)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		V_0 = (int32_t)L_3;
		int32_t L_4 = V_0;
		if ((((int32_t)L_4) >= ((int32_t)0)))
		{
			goto IL_001b;
		}
	}
	{
		return __this;
	}

IL_001b:
	{
		ImmutableList_1_t2299524397 * L_5 = (ImmutableList_1_t2299524397 *)__this->get__observers_0();
		NullCheck((ImmutableList_1_t2299524397 *)L_5);
		IObserver_1U5BU5D_t329451519* L_6 = ((  IObserver_1U5BU5D_t329451519* (*) (ImmutableList_1_t2299524397 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((ImmutableList_1_t2299524397 *)L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		NullCheck(L_6);
		if ((!(((uint32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_6)->max_length))))) == ((uint32_t)2))))
		{
			goto IL_003e;
		}
	}
	{
		ImmutableList_1_t2299524397 * L_7 = (ImmutableList_1_t2299524397 *)__this->get__observers_0();
		NullCheck((ImmutableList_1_t2299524397 *)L_7);
		IObserver_1U5BU5D_t329451519* L_8 = ((  IObserver_1U5BU5D_t329451519* (*) (ImmutableList_1_t2299524397 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((ImmutableList_1_t2299524397 *)L_7, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_9 = V_0;
		NullCheck(L_8);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_8, ((int32_t)((int32_t)1-(int32_t)L_9)));
		int32_t L_10 = ((int32_t)((int32_t)1-(int32_t)L_9));
		return ((L_8)->GetAt(static_cast<il2cpp_array_size_t>(L_10)));
	}

IL_003e:
	{
		ImmutableList_1_t2299524397 * L_11 = (ImmutableList_1_t2299524397 *)__this->get__observers_0();
		Il2CppObject* L_12 = ___observer0;
		NullCheck((ImmutableList_1_t2299524397 *)L_11);
		ImmutableList_1_t2299524397 * L_13 = ((  ImmutableList_1_t2299524397 * (*) (ImmutableList_1_t2299524397 *, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6)->method)((ImmutableList_1_t2299524397 *)L_11, (Il2CppObject*)L_12, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		ListObserver_1_t3616905962 * L_14 = (ListObserver_1_t3616905962 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		((  void (*) (ListObserver_1_t3616905962 *, ImmutableList_1_t2299524397 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)(L_14, (ImmutableList_1_t2299524397 *)L_13, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		return L_14;
	}
}
// System.Void UniRx.InternalUtil.ListObserver`1<UniRx.DictionaryReplaceEvent`2<System.Object,System.Object>>::.ctor(UniRx.InternalUtil.ImmutableList`1<UniRx.IObserver`1<T>>)
extern "C"  void ListObserver_1__ctor_m3052252423_gshared (ListObserver_1_t4073544734 * __this, ImmutableList_1_t2756163169 * ___observers0, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		ImmutableList_1_t2756163169 * L_0 = ___observers0;
		__this->set__observers_0(L_0);
		return;
	}
}
// System.Void UniRx.InternalUtil.ListObserver`1<UniRx.DictionaryReplaceEvent`2<System.Object,System.Object>>::OnCompleted()
extern "C"  void ListObserver_1_OnCompleted_m2011051775_gshared (ListObserver_1_t4073544734 * __this, const MethodInfo* method)
{
	IObserver_1U5BU5D_t23082875* V_0 = NULL;
	int32_t V_1 = 0;
	{
		ImmutableList_1_t2756163169 * L_0 = (ImmutableList_1_t2756163169 *)__this->get__observers_0();
		NullCheck((ImmutableList_1_t2756163169 *)L_0);
		IObserver_1U5BU5D_t23082875* L_1 = ((  IObserver_1U5BU5D_t23082875* (*) (ImmutableList_1_t2756163169 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((ImmutableList_1_t2756163169 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		V_0 = (IObserver_1U5BU5D_t23082875*)L_1;
		V_1 = (int32_t)0;
		goto IL_001f;
	}

IL_0013:
	{
		IObserver_1U5BU5D_t23082875* L_2 = V_0;
		int32_t L_3 = V_1;
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, L_3);
		int32_t L_4 = L_3;
		NullCheck((Il2CppObject*)((L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_4))));
		InterfaceActionInvoker0::Invoke(0 /* System.Void UniRx.IObserver`1<UniRx.DictionaryReplaceEvent`2<System.Object,System.Object>>::OnCompleted() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), (Il2CppObject*)((L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_4))));
		int32_t L_5 = V_1;
		V_1 = (int32_t)((int32_t)((int32_t)L_5+(int32_t)1));
	}

IL_001f:
	{
		int32_t L_6 = V_1;
		IObserver_1U5BU5D_t23082875* L_7 = V_0;
		NullCheck(L_7);
		if ((((int32_t)L_6) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_7)->max_length)))))))
		{
			goto IL_0013;
		}
	}
	{
		return;
	}
}
// System.Void UniRx.InternalUtil.ListObserver`1<UniRx.DictionaryReplaceEvent`2<System.Object,System.Object>>::OnError(System.Exception)
extern "C"  void ListObserver_1_OnError_m4016791980_gshared (ListObserver_1_t4073544734 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method)
{
	IObserver_1U5BU5D_t23082875* V_0 = NULL;
	int32_t V_1 = 0;
	{
		ImmutableList_1_t2756163169 * L_0 = (ImmutableList_1_t2756163169 *)__this->get__observers_0();
		NullCheck((ImmutableList_1_t2756163169 *)L_0);
		IObserver_1U5BU5D_t23082875* L_1 = ((  IObserver_1U5BU5D_t23082875* (*) (ImmutableList_1_t2756163169 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((ImmutableList_1_t2756163169 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		V_0 = (IObserver_1U5BU5D_t23082875*)L_1;
		V_1 = (int32_t)0;
		goto IL_0020;
	}

IL_0013:
	{
		IObserver_1U5BU5D_t23082875* L_2 = V_0;
		int32_t L_3 = V_1;
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, L_3);
		int32_t L_4 = L_3;
		Exception_t1967233988 * L_5 = ___error0;
		NullCheck((Il2CppObject*)((L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_4))));
		InterfaceActionInvoker1< Exception_t1967233988 * >::Invoke(1 /* System.Void UniRx.IObserver`1<UniRx.DictionaryReplaceEvent`2<System.Object,System.Object>>::OnError(System.Exception) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), (Il2CppObject*)((L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_4))), (Exception_t1967233988 *)L_5);
		int32_t L_6 = V_1;
		V_1 = (int32_t)((int32_t)((int32_t)L_6+(int32_t)1));
	}

IL_0020:
	{
		int32_t L_7 = V_1;
		IObserver_1U5BU5D_t23082875* L_8 = V_0;
		NullCheck(L_8);
		if ((((int32_t)L_7) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_8)->max_length)))))))
		{
			goto IL_0013;
		}
	}
	{
		return;
	}
}
// System.Void UniRx.InternalUtil.ListObserver`1<UniRx.DictionaryReplaceEvent`2<System.Object,System.Object>>::OnNext(T)
extern "C"  void ListObserver_1_OnNext_m70918301_gshared (ListObserver_1_t4073544734 * __this, DictionaryReplaceEvent_2_t3778927063  ___value0, const MethodInfo* method)
{
	IObserver_1U5BU5D_t23082875* V_0 = NULL;
	int32_t V_1 = 0;
	{
		ImmutableList_1_t2756163169 * L_0 = (ImmutableList_1_t2756163169 *)__this->get__observers_0();
		NullCheck((ImmutableList_1_t2756163169 *)L_0);
		IObserver_1U5BU5D_t23082875* L_1 = ((  IObserver_1U5BU5D_t23082875* (*) (ImmutableList_1_t2756163169 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((ImmutableList_1_t2756163169 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		V_0 = (IObserver_1U5BU5D_t23082875*)L_1;
		V_1 = (int32_t)0;
		goto IL_0020;
	}

IL_0013:
	{
		IObserver_1U5BU5D_t23082875* L_2 = V_0;
		int32_t L_3 = V_1;
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, L_3);
		int32_t L_4 = L_3;
		DictionaryReplaceEvent_2_t3778927063  L_5 = ___value0;
		NullCheck((Il2CppObject*)((L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_4))));
		InterfaceActionInvoker1< DictionaryReplaceEvent_2_t3778927063  >::Invoke(2 /* System.Void UniRx.IObserver`1<UniRx.DictionaryReplaceEvent`2<System.Object,System.Object>>::OnNext(T) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), (Il2CppObject*)((L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_4))), (DictionaryReplaceEvent_2_t3778927063 )L_5);
		int32_t L_6 = V_1;
		V_1 = (int32_t)((int32_t)((int32_t)L_6+(int32_t)1));
	}

IL_0020:
	{
		int32_t L_7 = V_1;
		IObserver_1U5BU5D_t23082875* L_8 = V_0;
		NullCheck(L_8);
		if ((((int32_t)L_7) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_8)->max_length)))))))
		{
			goto IL_0013;
		}
	}
	{
		return;
	}
}
// UniRx.IObserver`1<T> UniRx.InternalUtil.ListObserver`1<UniRx.DictionaryReplaceEvent`2<System.Object,System.Object>>::Add(UniRx.IObserver`1<T>)
extern "C"  Il2CppObject* ListObserver_1_Add_m3488575599_gshared (ListObserver_1_t4073544734 * __this, Il2CppObject* ___observer0, const MethodInfo* method)
{
	{
		ImmutableList_1_t2756163169 * L_0 = (ImmutableList_1_t2756163169 *)__this->get__observers_0();
		Il2CppObject* L_1 = ___observer0;
		NullCheck((ImmutableList_1_t2756163169 *)L_0);
		ImmutableList_1_t2756163169 * L_2 = ((  ImmutableList_1_t2756163169 * (*) (ImmutableList_1_t2756163169 *, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)((ImmutableList_1_t2756163169 *)L_0, (Il2CppObject*)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		ListObserver_1_t4073544734 * L_3 = (ListObserver_1_t4073544734 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		((  void (*) (ListObserver_1_t4073544734 *, ImmutableList_1_t2756163169 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)(L_3, (ImmutableList_1_t2756163169 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		return L_3;
	}
}
// UniRx.IObserver`1<T> UniRx.InternalUtil.ListObserver`1<UniRx.DictionaryReplaceEvent`2<System.Object,System.Object>>::Remove(UniRx.IObserver`1<T>)
extern "C"  Il2CppObject* ListObserver_1_Remove_m3265454204_gshared (ListObserver_1_t4073544734 * __this, Il2CppObject* ___observer0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		ImmutableList_1_t2756163169 * L_0 = (ImmutableList_1_t2756163169 *)__this->get__observers_0();
		NullCheck((ImmutableList_1_t2756163169 *)L_0);
		IObserver_1U5BU5D_t23082875* L_1 = ((  IObserver_1U5BU5D_t23082875* (*) (ImmutableList_1_t2756163169 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((ImmutableList_1_t2756163169 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		Il2CppObject* L_2 = ___observer0;
		int32_t L_3 = ((  int32_t (*) (Il2CppObject * /* static, unused */, IObserver_1U5BU5D_t23082875*, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->method)(NULL /*static, unused*/, (IObserver_1U5BU5D_t23082875*)L_1, (Il2CppObject*)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		V_0 = (int32_t)L_3;
		int32_t L_4 = V_0;
		if ((((int32_t)L_4) >= ((int32_t)0)))
		{
			goto IL_001b;
		}
	}
	{
		return __this;
	}

IL_001b:
	{
		ImmutableList_1_t2756163169 * L_5 = (ImmutableList_1_t2756163169 *)__this->get__observers_0();
		NullCheck((ImmutableList_1_t2756163169 *)L_5);
		IObserver_1U5BU5D_t23082875* L_6 = ((  IObserver_1U5BU5D_t23082875* (*) (ImmutableList_1_t2756163169 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((ImmutableList_1_t2756163169 *)L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		NullCheck(L_6);
		if ((!(((uint32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_6)->max_length))))) == ((uint32_t)2))))
		{
			goto IL_003e;
		}
	}
	{
		ImmutableList_1_t2756163169 * L_7 = (ImmutableList_1_t2756163169 *)__this->get__observers_0();
		NullCheck((ImmutableList_1_t2756163169 *)L_7);
		IObserver_1U5BU5D_t23082875* L_8 = ((  IObserver_1U5BU5D_t23082875* (*) (ImmutableList_1_t2756163169 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((ImmutableList_1_t2756163169 *)L_7, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_9 = V_0;
		NullCheck(L_8);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_8, ((int32_t)((int32_t)1-(int32_t)L_9)));
		int32_t L_10 = ((int32_t)((int32_t)1-(int32_t)L_9));
		return ((L_8)->GetAt(static_cast<il2cpp_array_size_t>(L_10)));
	}

IL_003e:
	{
		ImmutableList_1_t2756163169 * L_11 = (ImmutableList_1_t2756163169 *)__this->get__observers_0();
		Il2CppObject* L_12 = ___observer0;
		NullCheck((ImmutableList_1_t2756163169 *)L_11);
		ImmutableList_1_t2756163169 * L_13 = ((  ImmutableList_1_t2756163169 * (*) (ImmutableList_1_t2756163169 *, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6)->method)((ImmutableList_1_t2756163169 *)L_11, (Il2CppObject*)L_12, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		ListObserver_1_t4073544734 * L_14 = (ListObserver_1_t4073544734 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		((  void (*) (ListObserver_1_t4073544734 *, ImmutableList_1_t2756163169 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)(L_14, (ImmutableList_1_t2756163169 *)L_13, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		return L_14;
	}
}
// System.Void UniRx.InternalUtil.ListObserver`1<UniRx.Tuple`2<System.Object,System.Int32>>::.ctor(UniRx.InternalUtil.ImmutableList`1<UniRx.IObserver`1<T>>)
extern "C"  void ListObserver_1__ctor_m2897832202_gshared (ListObserver_1_t2674187857 * __this, ImmutableList_1_t1356806292 * ___observers0, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		ImmutableList_1_t1356806292 * L_0 = ___observers0;
		__this->set__observers_0(L_0);
		return;
	}
}
// System.Void UniRx.InternalUtil.ListObserver`1<UniRx.Tuple`2<System.Object,System.Int32>>::OnCompleted()
extern "C"  void ListObserver_1_OnCompleted_m3552233922_gshared (ListObserver_1_t2674187857 * __this, const MethodInfo* method)
{
	IObserver_1U5BU5D_t1093590812* V_0 = NULL;
	int32_t V_1 = 0;
	{
		ImmutableList_1_t1356806292 * L_0 = (ImmutableList_1_t1356806292 *)__this->get__observers_0();
		NullCheck((ImmutableList_1_t1356806292 *)L_0);
		IObserver_1U5BU5D_t1093590812* L_1 = ((  IObserver_1U5BU5D_t1093590812* (*) (ImmutableList_1_t1356806292 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((ImmutableList_1_t1356806292 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		V_0 = (IObserver_1U5BU5D_t1093590812*)L_1;
		V_1 = (int32_t)0;
		goto IL_001f;
	}

IL_0013:
	{
		IObserver_1U5BU5D_t1093590812* L_2 = V_0;
		int32_t L_3 = V_1;
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, L_3);
		int32_t L_4 = L_3;
		NullCheck((Il2CppObject*)((L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_4))));
		InterfaceActionInvoker0::Invoke(0 /* System.Void UniRx.IObserver`1<UniRx.Tuple`2<System.Object,System.Int32>>::OnCompleted() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), (Il2CppObject*)((L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_4))));
		int32_t L_5 = V_1;
		V_1 = (int32_t)((int32_t)((int32_t)L_5+(int32_t)1));
	}

IL_001f:
	{
		int32_t L_6 = V_1;
		IObserver_1U5BU5D_t1093590812* L_7 = V_0;
		NullCheck(L_7);
		if ((((int32_t)L_6) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_7)->max_length)))))))
		{
			goto IL_0013;
		}
	}
	{
		return;
	}
}
// System.Void UniRx.InternalUtil.ListObserver`1<UniRx.Tuple`2<System.Object,System.Int32>>::OnError(System.Exception)
extern "C"  void ListObserver_1_OnError_m3368740847_gshared (ListObserver_1_t2674187857 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method)
{
	IObserver_1U5BU5D_t1093590812* V_0 = NULL;
	int32_t V_1 = 0;
	{
		ImmutableList_1_t1356806292 * L_0 = (ImmutableList_1_t1356806292 *)__this->get__observers_0();
		NullCheck((ImmutableList_1_t1356806292 *)L_0);
		IObserver_1U5BU5D_t1093590812* L_1 = ((  IObserver_1U5BU5D_t1093590812* (*) (ImmutableList_1_t1356806292 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((ImmutableList_1_t1356806292 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		V_0 = (IObserver_1U5BU5D_t1093590812*)L_1;
		V_1 = (int32_t)0;
		goto IL_0020;
	}

IL_0013:
	{
		IObserver_1U5BU5D_t1093590812* L_2 = V_0;
		int32_t L_3 = V_1;
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, L_3);
		int32_t L_4 = L_3;
		Exception_t1967233988 * L_5 = ___error0;
		NullCheck((Il2CppObject*)((L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_4))));
		InterfaceActionInvoker1< Exception_t1967233988 * >::Invoke(1 /* System.Void UniRx.IObserver`1<UniRx.Tuple`2<System.Object,System.Int32>>::OnError(System.Exception) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), (Il2CppObject*)((L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_4))), (Exception_t1967233988 *)L_5);
		int32_t L_6 = V_1;
		V_1 = (int32_t)((int32_t)((int32_t)L_6+(int32_t)1));
	}

IL_0020:
	{
		int32_t L_7 = V_1;
		IObserver_1U5BU5D_t1093590812* L_8 = V_0;
		NullCheck(L_8);
		if ((((int32_t)L_7) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_8)->max_length)))))))
		{
			goto IL_0013;
		}
	}
	{
		return;
	}
}
// System.Void UniRx.InternalUtil.ListObserver`1<UniRx.Tuple`2<System.Object,System.Int32>>::OnNext(T)
extern "C"  void ListObserver_1_OnNext_m3682372832_gshared (ListObserver_1_t2674187857 * __this, Tuple_2_t2379570186  ___value0, const MethodInfo* method)
{
	IObserver_1U5BU5D_t1093590812* V_0 = NULL;
	int32_t V_1 = 0;
	{
		ImmutableList_1_t1356806292 * L_0 = (ImmutableList_1_t1356806292 *)__this->get__observers_0();
		NullCheck((ImmutableList_1_t1356806292 *)L_0);
		IObserver_1U5BU5D_t1093590812* L_1 = ((  IObserver_1U5BU5D_t1093590812* (*) (ImmutableList_1_t1356806292 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((ImmutableList_1_t1356806292 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		V_0 = (IObserver_1U5BU5D_t1093590812*)L_1;
		V_1 = (int32_t)0;
		goto IL_0020;
	}

IL_0013:
	{
		IObserver_1U5BU5D_t1093590812* L_2 = V_0;
		int32_t L_3 = V_1;
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, L_3);
		int32_t L_4 = L_3;
		Tuple_2_t2379570186  L_5 = ___value0;
		NullCheck((Il2CppObject*)((L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_4))));
		InterfaceActionInvoker1< Tuple_2_t2379570186  >::Invoke(2 /* System.Void UniRx.IObserver`1<UniRx.Tuple`2<System.Object,System.Int32>>::OnNext(T) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), (Il2CppObject*)((L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_4))), (Tuple_2_t2379570186 )L_5);
		int32_t L_6 = V_1;
		V_1 = (int32_t)((int32_t)((int32_t)L_6+(int32_t)1));
	}

IL_0020:
	{
		int32_t L_7 = V_1;
		IObserver_1U5BU5D_t1093590812* L_8 = V_0;
		NullCheck(L_8);
		if ((((int32_t)L_7) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_8)->max_length)))))))
		{
			goto IL_0013;
		}
	}
	{
		return;
	}
}
// UniRx.IObserver`1<T> UniRx.InternalUtil.ListObserver`1<UniRx.Tuple`2<System.Object,System.Int32>>::Add(UniRx.IObserver`1<T>)
extern "C"  Il2CppObject* ListObserver_1_Add_m2745800434_gshared (ListObserver_1_t2674187857 * __this, Il2CppObject* ___observer0, const MethodInfo* method)
{
	{
		ImmutableList_1_t1356806292 * L_0 = (ImmutableList_1_t1356806292 *)__this->get__observers_0();
		Il2CppObject* L_1 = ___observer0;
		NullCheck((ImmutableList_1_t1356806292 *)L_0);
		ImmutableList_1_t1356806292 * L_2 = ((  ImmutableList_1_t1356806292 * (*) (ImmutableList_1_t1356806292 *, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)((ImmutableList_1_t1356806292 *)L_0, (Il2CppObject*)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		ListObserver_1_t2674187857 * L_3 = (ListObserver_1_t2674187857 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		((  void (*) (ListObserver_1_t2674187857 *, ImmutableList_1_t1356806292 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)(L_3, (ImmutableList_1_t1356806292 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		return L_3;
	}
}
// UniRx.IObserver`1<T> UniRx.InternalUtil.ListObserver`1<UniRx.Tuple`2<System.Object,System.Int32>>::Remove(UniRx.IObserver`1<T>)
extern "C"  Il2CppObject* ListObserver_1_Remove_m2922022681_gshared (ListObserver_1_t2674187857 * __this, Il2CppObject* ___observer0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		ImmutableList_1_t1356806292 * L_0 = (ImmutableList_1_t1356806292 *)__this->get__observers_0();
		NullCheck((ImmutableList_1_t1356806292 *)L_0);
		IObserver_1U5BU5D_t1093590812* L_1 = ((  IObserver_1U5BU5D_t1093590812* (*) (ImmutableList_1_t1356806292 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((ImmutableList_1_t1356806292 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		Il2CppObject* L_2 = ___observer0;
		int32_t L_3 = ((  int32_t (*) (Il2CppObject * /* static, unused */, IObserver_1U5BU5D_t1093590812*, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->method)(NULL /*static, unused*/, (IObserver_1U5BU5D_t1093590812*)L_1, (Il2CppObject*)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		V_0 = (int32_t)L_3;
		int32_t L_4 = V_0;
		if ((((int32_t)L_4) >= ((int32_t)0)))
		{
			goto IL_001b;
		}
	}
	{
		return __this;
	}

IL_001b:
	{
		ImmutableList_1_t1356806292 * L_5 = (ImmutableList_1_t1356806292 *)__this->get__observers_0();
		NullCheck((ImmutableList_1_t1356806292 *)L_5);
		IObserver_1U5BU5D_t1093590812* L_6 = ((  IObserver_1U5BU5D_t1093590812* (*) (ImmutableList_1_t1356806292 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((ImmutableList_1_t1356806292 *)L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		NullCheck(L_6);
		if ((!(((uint32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_6)->max_length))))) == ((uint32_t)2))))
		{
			goto IL_003e;
		}
	}
	{
		ImmutableList_1_t1356806292 * L_7 = (ImmutableList_1_t1356806292 *)__this->get__observers_0();
		NullCheck((ImmutableList_1_t1356806292 *)L_7);
		IObserver_1U5BU5D_t1093590812* L_8 = ((  IObserver_1U5BU5D_t1093590812* (*) (ImmutableList_1_t1356806292 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((ImmutableList_1_t1356806292 *)L_7, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_9 = V_0;
		NullCheck(L_8);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_8, ((int32_t)((int32_t)1-(int32_t)L_9)));
		int32_t L_10 = ((int32_t)((int32_t)1-(int32_t)L_9));
		return ((L_8)->GetAt(static_cast<il2cpp_array_size_t>(L_10)));
	}

IL_003e:
	{
		ImmutableList_1_t1356806292 * L_11 = (ImmutableList_1_t1356806292 *)__this->get__observers_0();
		Il2CppObject* L_12 = ___observer0;
		NullCheck((ImmutableList_1_t1356806292 *)L_11);
		ImmutableList_1_t1356806292 * L_13 = ((  ImmutableList_1_t1356806292 * (*) (ImmutableList_1_t1356806292 *, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6)->method)((ImmutableList_1_t1356806292 *)L_11, (Il2CppObject*)L_12, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		ListObserver_1_t2674187857 * L_14 = (ListObserver_1_t2674187857 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		((  void (*) (ListObserver_1_t2674187857 *, ImmutableList_1_t1356806292 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)(L_14, (ImmutableList_1_t1356806292 *)L_13, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		return L_14;
	}
}
// System.Void UniRx.InternalUtil.ListObserver`1<UniRx.Tuple`2<System.Object,System.Object>>::.ctor(UniRx.InternalUtil.ImmutableList`1<UniRx.IObserver`1<T>>)
extern "C"  void ListObserver_1__ctor_m3190591379_gshared (ListObserver_1_t663879490 * __this, ImmutableList_1_t3641465221 * ___observers0, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		ImmutableList_1_t3641465221 * L_0 = ___observers0;
		__this->set__observers_0(L_0);
		return;
	}
}
// System.Void UniRx.InternalUtil.ListObserver`1<UniRx.Tuple`2<System.Object,System.Object>>::OnCompleted()
extern "C"  void ListObserver_1_OnCompleted_m2618954891_gshared (ListObserver_1_t663879490 * __this, const MethodInfo* method)
{
	IObserver_1U5BU5D_t3590098759* V_0 = NULL;
	int32_t V_1 = 0;
	{
		ImmutableList_1_t3641465221 * L_0 = (ImmutableList_1_t3641465221 *)__this->get__observers_0();
		NullCheck((ImmutableList_1_t3641465221 *)L_0);
		IObserver_1U5BU5D_t3590098759* L_1 = ((  IObserver_1U5BU5D_t3590098759* (*) (ImmutableList_1_t3641465221 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((ImmutableList_1_t3641465221 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		V_0 = (IObserver_1U5BU5D_t3590098759*)L_1;
		V_1 = (int32_t)0;
		goto IL_001f;
	}

IL_0013:
	{
		IObserver_1U5BU5D_t3590098759* L_2 = V_0;
		int32_t L_3 = V_1;
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, L_3);
		int32_t L_4 = L_3;
		NullCheck((Il2CppObject*)((L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_4))));
		InterfaceActionInvoker0::Invoke(0 /* System.Void UniRx.IObserver`1<UniRx.Tuple`2<System.Object,System.Object>>::OnCompleted() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), (Il2CppObject*)((L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_4))));
		int32_t L_5 = V_1;
		V_1 = (int32_t)((int32_t)((int32_t)L_5+(int32_t)1));
	}

IL_001f:
	{
		int32_t L_6 = V_1;
		IObserver_1U5BU5D_t3590098759* L_7 = V_0;
		NullCheck(L_7);
		if ((((int32_t)L_6) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_7)->max_length)))))))
		{
			goto IL_0013;
		}
	}
	{
		return;
	}
}
// System.Void UniRx.InternalUtil.ListObserver`1<UniRx.Tuple`2<System.Object,System.Object>>::OnError(System.Exception)
extern "C"  void ListObserver_1_OnError_m527125816_gshared (ListObserver_1_t663879490 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method)
{
	IObserver_1U5BU5D_t3590098759* V_0 = NULL;
	int32_t V_1 = 0;
	{
		ImmutableList_1_t3641465221 * L_0 = (ImmutableList_1_t3641465221 *)__this->get__observers_0();
		NullCheck((ImmutableList_1_t3641465221 *)L_0);
		IObserver_1U5BU5D_t3590098759* L_1 = ((  IObserver_1U5BU5D_t3590098759* (*) (ImmutableList_1_t3641465221 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((ImmutableList_1_t3641465221 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		V_0 = (IObserver_1U5BU5D_t3590098759*)L_1;
		V_1 = (int32_t)0;
		goto IL_0020;
	}

IL_0013:
	{
		IObserver_1U5BU5D_t3590098759* L_2 = V_0;
		int32_t L_3 = V_1;
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, L_3);
		int32_t L_4 = L_3;
		Exception_t1967233988 * L_5 = ___error0;
		NullCheck((Il2CppObject*)((L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_4))));
		InterfaceActionInvoker1< Exception_t1967233988 * >::Invoke(1 /* System.Void UniRx.IObserver`1<UniRx.Tuple`2<System.Object,System.Object>>::OnError(System.Exception) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), (Il2CppObject*)((L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_4))), (Exception_t1967233988 *)L_5);
		int32_t L_6 = V_1;
		V_1 = (int32_t)((int32_t)((int32_t)L_6+(int32_t)1));
	}

IL_0020:
	{
		int32_t L_7 = V_1;
		IObserver_1U5BU5D_t3590098759* L_8 = V_0;
		NullCheck(L_8);
		if ((((int32_t)L_7) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_8)->max_length)))))))
		{
			goto IL_0013;
		}
	}
	{
		return;
	}
}
// System.Void UniRx.InternalUtil.ListObserver`1<UniRx.Tuple`2<System.Object,System.Object>>::OnNext(T)
extern "C"  void ListObserver_1_OnNext_m716874793_gshared (ListObserver_1_t663879490 * __this, Tuple_2_t369261819  ___value0, const MethodInfo* method)
{
	IObserver_1U5BU5D_t3590098759* V_0 = NULL;
	int32_t V_1 = 0;
	{
		ImmutableList_1_t3641465221 * L_0 = (ImmutableList_1_t3641465221 *)__this->get__observers_0();
		NullCheck((ImmutableList_1_t3641465221 *)L_0);
		IObserver_1U5BU5D_t3590098759* L_1 = ((  IObserver_1U5BU5D_t3590098759* (*) (ImmutableList_1_t3641465221 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((ImmutableList_1_t3641465221 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		V_0 = (IObserver_1U5BU5D_t3590098759*)L_1;
		V_1 = (int32_t)0;
		goto IL_0020;
	}

IL_0013:
	{
		IObserver_1U5BU5D_t3590098759* L_2 = V_0;
		int32_t L_3 = V_1;
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, L_3);
		int32_t L_4 = L_3;
		Tuple_2_t369261819  L_5 = ___value0;
		NullCheck((Il2CppObject*)((L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_4))));
		InterfaceActionInvoker1< Tuple_2_t369261819  >::Invoke(2 /* System.Void UniRx.IObserver`1<UniRx.Tuple`2<System.Object,System.Object>>::OnNext(T) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), (Il2CppObject*)((L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_4))), (Tuple_2_t369261819 )L_5);
		int32_t L_6 = V_1;
		V_1 = (int32_t)((int32_t)((int32_t)L_6+(int32_t)1));
	}

IL_0020:
	{
		int32_t L_7 = V_1;
		IObserver_1U5BU5D_t3590098759* L_8 = V_0;
		NullCheck(L_8);
		if ((((int32_t)L_7) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_8)->max_length)))))))
		{
			goto IL_0013;
		}
	}
	{
		return;
	}
}
// UniRx.IObserver`1<T> UniRx.InternalUtil.ListObserver`1<UniRx.Tuple`2<System.Object,System.Object>>::Add(UniRx.IObserver`1<T>)
extern "C"  Il2CppObject* ListObserver_1_Add_m2701762761_gshared (ListObserver_1_t663879490 * __this, Il2CppObject* ___observer0, const MethodInfo* method)
{
	{
		ImmutableList_1_t3641465221 * L_0 = (ImmutableList_1_t3641465221 *)__this->get__observers_0();
		Il2CppObject* L_1 = ___observer0;
		NullCheck((ImmutableList_1_t3641465221 *)L_0);
		ImmutableList_1_t3641465221 * L_2 = ((  ImmutableList_1_t3641465221 * (*) (ImmutableList_1_t3641465221 *, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)((ImmutableList_1_t3641465221 *)L_0, (Il2CppObject*)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		ListObserver_1_t663879490 * L_3 = (ListObserver_1_t663879490 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		((  void (*) (ListObserver_1_t663879490 *, ImmutableList_1_t3641465221 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)(L_3, (ImmutableList_1_t3641465221 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		return L_3;
	}
}
// UniRx.IObserver`1<T> UniRx.InternalUtil.ListObserver`1<UniRx.Tuple`2<System.Object,System.Object>>::Remove(UniRx.IObserver`1<T>)
extern "C"  Il2CppObject* ListObserver_1_Remove_m960731618_gshared (ListObserver_1_t663879490 * __this, Il2CppObject* ___observer0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		ImmutableList_1_t3641465221 * L_0 = (ImmutableList_1_t3641465221 *)__this->get__observers_0();
		NullCheck((ImmutableList_1_t3641465221 *)L_0);
		IObserver_1U5BU5D_t3590098759* L_1 = ((  IObserver_1U5BU5D_t3590098759* (*) (ImmutableList_1_t3641465221 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((ImmutableList_1_t3641465221 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		Il2CppObject* L_2 = ___observer0;
		int32_t L_3 = ((  int32_t (*) (Il2CppObject * /* static, unused */, IObserver_1U5BU5D_t3590098759*, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->method)(NULL /*static, unused*/, (IObserver_1U5BU5D_t3590098759*)L_1, (Il2CppObject*)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		V_0 = (int32_t)L_3;
		int32_t L_4 = V_0;
		if ((((int32_t)L_4) >= ((int32_t)0)))
		{
			goto IL_001b;
		}
	}
	{
		return __this;
	}

IL_001b:
	{
		ImmutableList_1_t3641465221 * L_5 = (ImmutableList_1_t3641465221 *)__this->get__observers_0();
		NullCheck((ImmutableList_1_t3641465221 *)L_5);
		IObserver_1U5BU5D_t3590098759* L_6 = ((  IObserver_1U5BU5D_t3590098759* (*) (ImmutableList_1_t3641465221 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((ImmutableList_1_t3641465221 *)L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		NullCheck(L_6);
		if ((!(((uint32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_6)->max_length))))) == ((uint32_t)2))))
		{
			goto IL_003e;
		}
	}
	{
		ImmutableList_1_t3641465221 * L_7 = (ImmutableList_1_t3641465221 *)__this->get__observers_0();
		NullCheck((ImmutableList_1_t3641465221 *)L_7);
		IObserver_1U5BU5D_t3590098759* L_8 = ((  IObserver_1U5BU5D_t3590098759* (*) (ImmutableList_1_t3641465221 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((ImmutableList_1_t3641465221 *)L_7, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_9 = V_0;
		NullCheck(L_8);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_8, ((int32_t)((int32_t)1-(int32_t)L_9)));
		int32_t L_10 = ((int32_t)((int32_t)1-(int32_t)L_9));
		return ((L_8)->GetAt(static_cast<il2cpp_array_size_t>(L_10)));
	}

IL_003e:
	{
		ImmutableList_1_t3641465221 * L_11 = (ImmutableList_1_t3641465221 *)__this->get__observers_0();
		Il2CppObject* L_12 = ___observer0;
		NullCheck((ImmutableList_1_t3641465221 *)L_11);
		ImmutableList_1_t3641465221 * L_13 = ((  ImmutableList_1_t3641465221 * (*) (ImmutableList_1_t3641465221 *, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6)->method)((ImmutableList_1_t3641465221 *)L_11, (Il2CppObject*)L_12, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		ListObserver_1_t663879490 * L_14 = (ListObserver_1_t663879490 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		((  void (*) (ListObserver_1_t663879490 *, ImmutableList_1_t3641465221 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)(L_14, (ImmutableList_1_t3641465221 *)L_13, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		return L_14;
	}
}
// System.Void UniRx.InternalUtil.ListObserver`1<UniRx.Tuple`2<System.Object,UnityEngine.NetworkMessageInfo>>::.ctor(UniRx.InternalUtil.ImmutableList`1<UniRx.IObserver`1<T>>)
extern "C"  void ListObserver_1__ctor_m830657531_gshared (ListObserver_1_t2401117954 * __this, ImmutableList_1_t1083736389 * ___observers0, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		ImmutableList_1_t1083736389 * L_0 = ___observers0;
		__this->set__observers_0(L_0);
		return;
	}
}
// System.Void UniRx.InternalUtil.ListObserver`1<UniRx.Tuple`2<System.Object,UnityEngine.NetworkMessageInfo>>::OnCompleted()
extern "C"  void ListObserver_1_OnCompleted_m3639853811_gshared (ListObserver_1_t2401117954 * __this, const MethodInfo* method)
{
	IObserver_1U5BU5D_t3985248391* V_0 = NULL;
	int32_t V_1 = 0;
	{
		ImmutableList_1_t1083736389 * L_0 = (ImmutableList_1_t1083736389 *)__this->get__observers_0();
		NullCheck((ImmutableList_1_t1083736389 *)L_0);
		IObserver_1U5BU5D_t3985248391* L_1 = ((  IObserver_1U5BU5D_t3985248391* (*) (ImmutableList_1_t1083736389 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((ImmutableList_1_t1083736389 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		V_0 = (IObserver_1U5BU5D_t3985248391*)L_1;
		V_1 = (int32_t)0;
		goto IL_001f;
	}

IL_0013:
	{
		IObserver_1U5BU5D_t3985248391* L_2 = V_0;
		int32_t L_3 = V_1;
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, L_3);
		int32_t L_4 = L_3;
		NullCheck((Il2CppObject*)((L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_4))));
		InterfaceActionInvoker0::Invoke(0 /* System.Void UniRx.IObserver`1<UniRx.Tuple`2<System.Object,UnityEngine.NetworkMessageInfo>>::OnCompleted() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), (Il2CppObject*)((L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_4))));
		int32_t L_5 = V_1;
		V_1 = (int32_t)((int32_t)((int32_t)L_5+(int32_t)1));
	}

IL_001f:
	{
		int32_t L_6 = V_1;
		IObserver_1U5BU5D_t3985248391* L_7 = V_0;
		NullCheck(L_7);
		if ((((int32_t)L_6) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_7)->max_length)))))))
		{
			goto IL_0013;
		}
	}
	{
		return;
	}
}
// System.Void UniRx.InternalUtil.ListObserver`1<UniRx.Tuple`2<System.Object,UnityEngine.NetworkMessageInfo>>::OnError(System.Exception)
extern "C"  void ListObserver_1_OnError_m3530203040_gshared (ListObserver_1_t2401117954 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method)
{
	IObserver_1U5BU5D_t3985248391* V_0 = NULL;
	int32_t V_1 = 0;
	{
		ImmutableList_1_t1083736389 * L_0 = (ImmutableList_1_t1083736389 *)__this->get__observers_0();
		NullCheck((ImmutableList_1_t1083736389 *)L_0);
		IObserver_1U5BU5D_t3985248391* L_1 = ((  IObserver_1U5BU5D_t3985248391* (*) (ImmutableList_1_t1083736389 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((ImmutableList_1_t1083736389 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		V_0 = (IObserver_1U5BU5D_t3985248391*)L_1;
		V_1 = (int32_t)0;
		goto IL_0020;
	}

IL_0013:
	{
		IObserver_1U5BU5D_t3985248391* L_2 = V_0;
		int32_t L_3 = V_1;
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, L_3);
		int32_t L_4 = L_3;
		Exception_t1967233988 * L_5 = ___error0;
		NullCheck((Il2CppObject*)((L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_4))));
		InterfaceActionInvoker1< Exception_t1967233988 * >::Invoke(1 /* System.Void UniRx.IObserver`1<UniRx.Tuple`2<System.Object,UnityEngine.NetworkMessageInfo>>::OnError(System.Exception) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), (Il2CppObject*)((L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_4))), (Exception_t1967233988 *)L_5);
		int32_t L_6 = V_1;
		V_1 = (int32_t)((int32_t)((int32_t)L_6+(int32_t)1));
	}

IL_0020:
	{
		int32_t L_7 = V_1;
		IObserver_1U5BU5D_t3985248391* L_8 = V_0;
		NullCheck(L_8);
		if ((((int32_t)L_7) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_8)->max_length)))))))
		{
			goto IL_0013;
		}
	}
	{
		return;
	}
}
// System.Void UniRx.InternalUtil.ListObserver`1<UniRx.Tuple`2<System.Object,UnityEngine.NetworkMessageInfo>>::OnNext(T)
extern "C"  void ListObserver_1_OnNext_m2872821393_gshared (ListObserver_1_t2401117954 * __this, Tuple_2_t2106500283  ___value0, const MethodInfo* method)
{
	IObserver_1U5BU5D_t3985248391* V_0 = NULL;
	int32_t V_1 = 0;
	{
		ImmutableList_1_t1083736389 * L_0 = (ImmutableList_1_t1083736389 *)__this->get__observers_0();
		NullCheck((ImmutableList_1_t1083736389 *)L_0);
		IObserver_1U5BU5D_t3985248391* L_1 = ((  IObserver_1U5BU5D_t3985248391* (*) (ImmutableList_1_t1083736389 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((ImmutableList_1_t1083736389 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		V_0 = (IObserver_1U5BU5D_t3985248391*)L_1;
		V_1 = (int32_t)0;
		goto IL_0020;
	}

IL_0013:
	{
		IObserver_1U5BU5D_t3985248391* L_2 = V_0;
		int32_t L_3 = V_1;
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, L_3);
		int32_t L_4 = L_3;
		Tuple_2_t2106500283  L_5 = ___value0;
		NullCheck((Il2CppObject*)((L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_4))));
		InterfaceActionInvoker1< Tuple_2_t2106500283  >::Invoke(2 /* System.Void UniRx.IObserver`1<UniRx.Tuple`2<System.Object,UnityEngine.NetworkMessageInfo>>::OnNext(T) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), (Il2CppObject*)((L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_4))), (Tuple_2_t2106500283 )L_5);
		int32_t L_6 = V_1;
		V_1 = (int32_t)((int32_t)((int32_t)L_6+(int32_t)1));
	}

IL_0020:
	{
		int32_t L_7 = V_1;
		IObserver_1U5BU5D_t3985248391* L_8 = V_0;
		NullCheck(L_8);
		if ((((int32_t)L_7) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_8)->max_length)))))))
		{
			goto IL_0013;
		}
	}
	{
		return;
	}
}
// UniRx.IObserver`1<T> UniRx.InternalUtil.ListObserver`1<UniRx.Tuple`2<System.Object,UnityEngine.NetworkMessageInfo>>::Add(UniRx.IObserver`1<T>)
extern "C"  Il2CppObject* ListObserver_1_Add_m3001986659_gshared (ListObserver_1_t2401117954 * __this, Il2CppObject* ___observer0, const MethodInfo* method)
{
	{
		ImmutableList_1_t1083736389 * L_0 = (ImmutableList_1_t1083736389 *)__this->get__observers_0();
		Il2CppObject* L_1 = ___observer0;
		NullCheck((ImmutableList_1_t1083736389 *)L_0);
		ImmutableList_1_t1083736389 * L_2 = ((  ImmutableList_1_t1083736389 * (*) (ImmutableList_1_t1083736389 *, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)((ImmutableList_1_t1083736389 *)L_0, (Il2CppObject*)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		ListObserver_1_t2401117954 * L_3 = (ListObserver_1_t2401117954 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		((  void (*) (ListObserver_1_t2401117954 *, ImmutableList_1_t1083736389 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)(L_3, (ImmutableList_1_t1083736389 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		return L_3;
	}
}
// UniRx.IObserver`1<T> UniRx.InternalUtil.ListObserver`1<UniRx.Tuple`2<System.Object,UnityEngine.NetworkMessageInfo>>::Remove(UniRx.IObserver`1<T>)
extern "C"  Il2CppObject* ListObserver_1_Remove_m2808966664_gshared (ListObserver_1_t2401117954 * __this, Il2CppObject* ___observer0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		ImmutableList_1_t1083736389 * L_0 = (ImmutableList_1_t1083736389 *)__this->get__observers_0();
		NullCheck((ImmutableList_1_t1083736389 *)L_0);
		IObserver_1U5BU5D_t3985248391* L_1 = ((  IObserver_1U5BU5D_t3985248391* (*) (ImmutableList_1_t1083736389 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((ImmutableList_1_t1083736389 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		Il2CppObject* L_2 = ___observer0;
		int32_t L_3 = ((  int32_t (*) (Il2CppObject * /* static, unused */, IObserver_1U5BU5D_t3985248391*, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->method)(NULL /*static, unused*/, (IObserver_1U5BU5D_t3985248391*)L_1, (Il2CppObject*)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		V_0 = (int32_t)L_3;
		int32_t L_4 = V_0;
		if ((((int32_t)L_4) >= ((int32_t)0)))
		{
			goto IL_001b;
		}
	}
	{
		return __this;
	}

IL_001b:
	{
		ImmutableList_1_t1083736389 * L_5 = (ImmutableList_1_t1083736389 *)__this->get__observers_0();
		NullCheck((ImmutableList_1_t1083736389 *)L_5);
		IObserver_1U5BU5D_t3985248391* L_6 = ((  IObserver_1U5BU5D_t3985248391* (*) (ImmutableList_1_t1083736389 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((ImmutableList_1_t1083736389 *)L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		NullCheck(L_6);
		if ((!(((uint32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_6)->max_length))))) == ((uint32_t)2))))
		{
			goto IL_003e;
		}
	}
	{
		ImmutableList_1_t1083736389 * L_7 = (ImmutableList_1_t1083736389 *)__this->get__observers_0();
		NullCheck((ImmutableList_1_t1083736389 *)L_7);
		IObserver_1U5BU5D_t3985248391* L_8 = ((  IObserver_1U5BU5D_t3985248391* (*) (ImmutableList_1_t1083736389 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((ImmutableList_1_t1083736389 *)L_7, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_9 = V_0;
		NullCheck(L_8);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_8, ((int32_t)((int32_t)1-(int32_t)L_9)));
		int32_t L_10 = ((int32_t)((int32_t)1-(int32_t)L_9));
		return ((L_8)->GetAt(static_cast<il2cpp_array_size_t>(L_10)));
	}

IL_003e:
	{
		ImmutableList_1_t1083736389 * L_11 = (ImmutableList_1_t1083736389 *)__this->get__observers_0();
		Il2CppObject* L_12 = ___observer0;
		NullCheck((ImmutableList_1_t1083736389 *)L_11);
		ImmutableList_1_t1083736389 * L_13 = ((  ImmutableList_1_t1083736389 * (*) (ImmutableList_1_t1083736389 *, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6)->method)((ImmutableList_1_t1083736389 *)L_11, (Il2CppObject*)L_12, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		ListObserver_1_t2401117954 * L_14 = (ListObserver_1_t2401117954 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		((  void (*) (ListObserver_1_t2401117954 *, ImmutableList_1_t1083736389 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)(L_14, (ImmutableList_1_t1083736389 *)L_13, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		return L_14;
	}
}
// System.Void UniRx.InternalUtil.ListObserver`1<UniRx.Unit>::.ctor(UniRx.InternalUtil.ImmutableList`1<UniRx.IObserver`1<T>>)
extern "C"  void ListObserver_1__ctor_m2218457479_gshared (ListObserver_1_t2852903709 * __this, ImmutableList_1_t1535522144 * ___observers0, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		ImmutableList_1_t1535522144 * L_0 = ___observers0;
		__this->set__observers_0(L_0);
		return;
	}
}
// System.Void UniRx.InternalUtil.ListObserver`1<UniRx.Unit>::OnCompleted()
extern "C"  void ListObserver_1_OnCompleted_m776971135_gshared (ListObserver_1_t2852903709 * __this, const MethodInfo* method)
{
	IObserver_1U5BU5D_t960117152* V_0 = NULL;
	int32_t V_1 = 0;
	{
		ImmutableList_1_t1535522144 * L_0 = (ImmutableList_1_t1535522144 *)__this->get__observers_0();
		NullCheck((ImmutableList_1_t1535522144 *)L_0);
		IObserver_1U5BU5D_t960117152* L_1 = ((  IObserver_1U5BU5D_t960117152* (*) (ImmutableList_1_t1535522144 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((ImmutableList_1_t1535522144 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		V_0 = (IObserver_1U5BU5D_t960117152*)L_1;
		V_1 = (int32_t)0;
		goto IL_001f;
	}

IL_0013:
	{
		IObserver_1U5BU5D_t960117152* L_2 = V_0;
		int32_t L_3 = V_1;
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, L_3);
		int32_t L_4 = L_3;
		NullCheck((Il2CppObject*)((L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_4))));
		InterfaceActionInvoker0::Invoke(0 /* System.Void UniRx.IObserver`1<UniRx.Unit>::OnCompleted() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), (Il2CppObject*)((L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_4))));
		int32_t L_5 = V_1;
		V_1 = (int32_t)((int32_t)((int32_t)L_5+(int32_t)1));
	}

IL_001f:
	{
		int32_t L_6 = V_1;
		IObserver_1U5BU5D_t960117152* L_7 = V_0;
		NullCheck(L_7);
		if ((((int32_t)L_6) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_7)->max_length)))))))
		{
			goto IL_0013;
		}
	}
	{
		return;
	}
}
// System.Void UniRx.InternalUtil.ListObserver`1<UniRx.Unit>::OnError(System.Exception)
extern "C"  void ListObserver_1_OnError_m211684908_gshared (ListObserver_1_t2852903709 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method)
{
	IObserver_1U5BU5D_t960117152* V_0 = NULL;
	int32_t V_1 = 0;
	{
		ImmutableList_1_t1535522144 * L_0 = (ImmutableList_1_t1535522144 *)__this->get__observers_0();
		NullCheck((ImmutableList_1_t1535522144 *)L_0);
		IObserver_1U5BU5D_t960117152* L_1 = ((  IObserver_1U5BU5D_t960117152* (*) (ImmutableList_1_t1535522144 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((ImmutableList_1_t1535522144 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		V_0 = (IObserver_1U5BU5D_t960117152*)L_1;
		V_1 = (int32_t)0;
		goto IL_0020;
	}

IL_0013:
	{
		IObserver_1U5BU5D_t960117152* L_2 = V_0;
		int32_t L_3 = V_1;
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, L_3);
		int32_t L_4 = L_3;
		Exception_t1967233988 * L_5 = ___error0;
		NullCheck((Il2CppObject*)((L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_4))));
		InterfaceActionInvoker1< Exception_t1967233988 * >::Invoke(1 /* System.Void UniRx.IObserver`1<UniRx.Unit>::OnError(System.Exception) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), (Il2CppObject*)((L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_4))), (Exception_t1967233988 *)L_5);
		int32_t L_6 = V_1;
		V_1 = (int32_t)((int32_t)((int32_t)L_6+(int32_t)1));
	}

IL_0020:
	{
		int32_t L_7 = V_1;
		IObserver_1U5BU5D_t960117152* L_8 = V_0;
		NullCheck(L_8);
		if ((((int32_t)L_7) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_8)->max_length)))))))
		{
			goto IL_0013;
		}
	}
	{
		return;
	}
}
// System.Void UniRx.InternalUtil.ListObserver`1<UniRx.Unit>::OnNext(T)
extern "C"  void ListObserver_1_OnNext_m1096142109_gshared (ListObserver_1_t2852903709 * __this, Unit_t2558286038  ___value0, const MethodInfo* method)
{
	IObserver_1U5BU5D_t960117152* V_0 = NULL;
	int32_t V_1 = 0;
	{
		ImmutableList_1_t1535522144 * L_0 = (ImmutableList_1_t1535522144 *)__this->get__observers_0();
		NullCheck((ImmutableList_1_t1535522144 *)L_0);
		IObserver_1U5BU5D_t960117152* L_1 = ((  IObserver_1U5BU5D_t960117152* (*) (ImmutableList_1_t1535522144 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((ImmutableList_1_t1535522144 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		V_0 = (IObserver_1U5BU5D_t960117152*)L_1;
		V_1 = (int32_t)0;
		goto IL_0020;
	}

IL_0013:
	{
		IObserver_1U5BU5D_t960117152* L_2 = V_0;
		int32_t L_3 = V_1;
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, L_3);
		int32_t L_4 = L_3;
		Unit_t2558286038  L_5 = ___value0;
		NullCheck((Il2CppObject*)((L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_4))));
		InterfaceActionInvoker1< Unit_t2558286038  >::Invoke(2 /* System.Void UniRx.IObserver`1<UniRx.Unit>::OnNext(T) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), (Il2CppObject*)((L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_4))), (Unit_t2558286038 )L_5);
		int32_t L_6 = V_1;
		V_1 = (int32_t)((int32_t)((int32_t)L_6+(int32_t)1));
	}

IL_0020:
	{
		int32_t L_7 = V_1;
		IObserver_1U5BU5D_t960117152* L_8 = V_0;
		NullCheck(L_8);
		if ((((int32_t)L_7) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_8)->max_length)))))))
		{
			goto IL_0013;
		}
	}
	{
		return;
	}
}
// UniRx.IObserver`1<T> UniRx.InternalUtil.ListObserver`1<UniRx.Unit>::Add(UniRx.IObserver`1<T>)
extern "C"  Il2CppObject* ListObserver_1_Add_m1792376253_gshared (ListObserver_1_t2852903709 * __this, Il2CppObject* ___observer0, const MethodInfo* method)
{
	{
		ImmutableList_1_t1535522144 * L_0 = (ImmutableList_1_t1535522144 *)__this->get__observers_0();
		Il2CppObject* L_1 = ___observer0;
		NullCheck((ImmutableList_1_t1535522144 *)L_0);
		ImmutableList_1_t1535522144 * L_2 = ((  ImmutableList_1_t1535522144 * (*) (ImmutableList_1_t1535522144 *, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)((ImmutableList_1_t1535522144 *)L_0, (Il2CppObject*)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		ListObserver_1_t2852903709 * L_3 = (ListObserver_1_t2852903709 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		((  void (*) (ListObserver_1_t2852903709 *, ImmutableList_1_t1535522144 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)(L_3, (ImmutableList_1_t1535522144 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		return L_3;
	}
}
// UniRx.IObserver`1<T> UniRx.InternalUtil.ListObserver`1<UniRx.Unit>::Remove(UniRx.IObserver`1<T>)
extern "C"  Il2CppObject* ListObserver_1_Remove_m2080974958_gshared (ListObserver_1_t2852903709 * __this, Il2CppObject* ___observer0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		ImmutableList_1_t1535522144 * L_0 = (ImmutableList_1_t1535522144 *)__this->get__observers_0();
		NullCheck((ImmutableList_1_t1535522144 *)L_0);
		IObserver_1U5BU5D_t960117152* L_1 = ((  IObserver_1U5BU5D_t960117152* (*) (ImmutableList_1_t1535522144 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((ImmutableList_1_t1535522144 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		Il2CppObject* L_2 = ___observer0;
		int32_t L_3 = ((  int32_t (*) (Il2CppObject * /* static, unused */, IObserver_1U5BU5D_t960117152*, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->method)(NULL /*static, unused*/, (IObserver_1U5BU5D_t960117152*)L_1, (Il2CppObject*)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		V_0 = (int32_t)L_3;
		int32_t L_4 = V_0;
		if ((((int32_t)L_4) >= ((int32_t)0)))
		{
			goto IL_001b;
		}
	}
	{
		return __this;
	}

IL_001b:
	{
		ImmutableList_1_t1535522144 * L_5 = (ImmutableList_1_t1535522144 *)__this->get__observers_0();
		NullCheck((ImmutableList_1_t1535522144 *)L_5);
		IObserver_1U5BU5D_t960117152* L_6 = ((  IObserver_1U5BU5D_t960117152* (*) (ImmutableList_1_t1535522144 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((ImmutableList_1_t1535522144 *)L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		NullCheck(L_6);
		if ((!(((uint32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_6)->max_length))))) == ((uint32_t)2))))
		{
			goto IL_003e;
		}
	}
	{
		ImmutableList_1_t1535522144 * L_7 = (ImmutableList_1_t1535522144 *)__this->get__observers_0();
		NullCheck((ImmutableList_1_t1535522144 *)L_7);
		IObserver_1U5BU5D_t960117152* L_8 = ((  IObserver_1U5BU5D_t960117152* (*) (ImmutableList_1_t1535522144 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((ImmutableList_1_t1535522144 *)L_7, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_9 = V_0;
		NullCheck(L_8);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_8, ((int32_t)((int32_t)1-(int32_t)L_9)));
		int32_t L_10 = ((int32_t)((int32_t)1-(int32_t)L_9));
		return ((L_8)->GetAt(static_cast<il2cpp_array_size_t>(L_10)));
	}

IL_003e:
	{
		ImmutableList_1_t1535522144 * L_11 = (ImmutableList_1_t1535522144 *)__this->get__observers_0();
		Il2CppObject* L_12 = ___observer0;
		NullCheck((ImmutableList_1_t1535522144 *)L_11);
		ImmutableList_1_t1535522144 * L_13 = ((  ImmutableList_1_t1535522144 * (*) (ImmutableList_1_t1535522144 *, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6)->method)((ImmutableList_1_t1535522144 *)L_11, (Il2CppObject*)L_12, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		ListObserver_1_t2852903709 * L_14 = (ListObserver_1_t2852903709 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		((  void (*) (ListObserver_1_t2852903709 *, ImmutableList_1_t1535522144 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)(L_14, (ImmutableList_1_t1535522144 *)L_13, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		return L_14;
	}
}
// System.Void UniRx.InternalUtil.ListObserver`1<UnityEngine.Bounds>::.ctor(UniRx.InternalUtil.ImmutableList`1<UniRx.IObserver`1<T>>)
extern "C"  void ListObserver_1__ctor_m3379697367_gshared (ListObserver_1_t3813132649 * __this, ImmutableList_1_t2495751084 * ___observers0, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		ImmutableList_1_t2495751084 * L_0 = ___observers0;
		__this->set__observers_0(L_0);
		return;
	}
}
// System.Void UniRx.InternalUtil.ListObserver`1<UnityEngine.Bounds>::OnCompleted()
extern "C"  void ListObserver_1_OnCompleted_m2484319439_gshared (ListObserver_1_t3813132649 * __this, const MethodInfo* method)
{
	IObserver_1U5BU5D_t579021988* V_0 = NULL;
	int32_t V_1 = 0;
	{
		ImmutableList_1_t2495751084 * L_0 = (ImmutableList_1_t2495751084 *)__this->get__observers_0();
		NullCheck((ImmutableList_1_t2495751084 *)L_0);
		IObserver_1U5BU5D_t579021988* L_1 = ((  IObserver_1U5BU5D_t579021988* (*) (ImmutableList_1_t2495751084 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((ImmutableList_1_t2495751084 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		V_0 = (IObserver_1U5BU5D_t579021988*)L_1;
		V_1 = (int32_t)0;
		goto IL_001f;
	}

IL_0013:
	{
		IObserver_1U5BU5D_t579021988* L_2 = V_0;
		int32_t L_3 = V_1;
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, L_3);
		int32_t L_4 = L_3;
		NullCheck((Il2CppObject*)((L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_4))));
		InterfaceActionInvoker0::Invoke(0 /* System.Void UniRx.IObserver`1<UnityEngine.Bounds>::OnCompleted() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), (Il2CppObject*)((L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_4))));
		int32_t L_5 = V_1;
		V_1 = (int32_t)((int32_t)((int32_t)L_5+(int32_t)1));
	}

IL_001f:
	{
		int32_t L_6 = V_1;
		IObserver_1U5BU5D_t579021988* L_7 = V_0;
		NullCheck(L_7);
		if ((((int32_t)L_6) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_7)->max_length)))))))
		{
			goto IL_0013;
		}
	}
	{
		return;
	}
}
// System.Void UniRx.InternalUtil.ListObserver`1<UnityEngine.Bounds>::OnError(System.Exception)
extern "C"  void ListObserver_1_OnError_m683845500_gshared (ListObserver_1_t3813132649 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method)
{
	IObserver_1U5BU5D_t579021988* V_0 = NULL;
	int32_t V_1 = 0;
	{
		ImmutableList_1_t2495751084 * L_0 = (ImmutableList_1_t2495751084 *)__this->get__observers_0();
		NullCheck((ImmutableList_1_t2495751084 *)L_0);
		IObserver_1U5BU5D_t579021988* L_1 = ((  IObserver_1U5BU5D_t579021988* (*) (ImmutableList_1_t2495751084 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((ImmutableList_1_t2495751084 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		V_0 = (IObserver_1U5BU5D_t579021988*)L_1;
		V_1 = (int32_t)0;
		goto IL_0020;
	}

IL_0013:
	{
		IObserver_1U5BU5D_t579021988* L_2 = V_0;
		int32_t L_3 = V_1;
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, L_3);
		int32_t L_4 = L_3;
		Exception_t1967233988 * L_5 = ___error0;
		NullCheck((Il2CppObject*)((L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_4))));
		InterfaceActionInvoker1< Exception_t1967233988 * >::Invoke(1 /* System.Void UniRx.IObserver`1<UnityEngine.Bounds>::OnError(System.Exception) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), (Il2CppObject*)((L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_4))), (Exception_t1967233988 *)L_5);
		int32_t L_6 = V_1;
		V_1 = (int32_t)((int32_t)((int32_t)L_6+(int32_t)1));
	}

IL_0020:
	{
		int32_t L_7 = V_1;
		IObserver_1U5BU5D_t579021988* L_8 = V_0;
		NullCheck(L_8);
		if ((((int32_t)L_7) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_8)->max_length)))))))
		{
			goto IL_0013;
		}
	}
	{
		return;
	}
}
// System.Void UniRx.InternalUtil.ListObserver`1<UnityEngine.Bounds>::OnNext(T)
extern "C"  void ListObserver_1_OnNext_m3086419565_gshared (ListObserver_1_t3813132649 * __this, Bounds_t3518514978  ___value0, const MethodInfo* method)
{
	IObserver_1U5BU5D_t579021988* V_0 = NULL;
	int32_t V_1 = 0;
	{
		ImmutableList_1_t2495751084 * L_0 = (ImmutableList_1_t2495751084 *)__this->get__observers_0();
		NullCheck((ImmutableList_1_t2495751084 *)L_0);
		IObserver_1U5BU5D_t579021988* L_1 = ((  IObserver_1U5BU5D_t579021988* (*) (ImmutableList_1_t2495751084 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((ImmutableList_1_t2495751084 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		V_0 = (IObserver_1U5BU5D_t579021988*)L_1;
		V_1 = (int32_t)0;
		goto IL_0020;
	}

IL_0013:
	{
		IObserver_1U5BU5D_t579021988* L_2 = V_0;
		int32_t L_3 = V_1;
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, L_3);
		int32_t L_4 = L_3;
		Bounds_t3518514978  L_5 = ___value0;
		NullCheck((Il2CppObject*)((L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_4))));
		InterfaceActionInvoker1< Bounds_t3518514978  >::Invoke(2 /* System.Void UniRx.IObserver`1<UnityEngine.Bounds>::OnNext(T) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), (Il2CppObject*)((L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_4))), (Bounds_t3518514978 )L_5);
		int32_t L_6 = V_1;
		V_1 = (int32_t)((int32_t)((int32_t)L_6+(int32_t)1));
	}

IL_0020:
	{
		int32_t L_7 = V_1;
		IObserver_1U5BU5D_t579021988* L_8 = V_0;
		NullCheck(L_8);
		if ((((int32_t)L_7) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_8)->max_length)))))))
		{
			goto IL_0013;
		}
	}
	{
		return;
	}
}
// UniRx.IObserver`1<T> UniRx.InternalUtil.ListObserver`1<UnityEngine.Bounds>::Add(UniRx.IObserver`1<T>)
extern "C"  Il2CppObject* ListObserver_1_Add_m2923515917_gshared (ListObserver_1_t3813132649 * __this, Il2CppObject* ___observer0, const MethodInfo* method)
{
	{
		ImmutableList_1_t2495751084 * L_0 = (ImmutableList_1_t2495751084 *)__this->get__observers_0();
		Il2CppObject* L_1 = ___observer0;
		NullCheck((ImmutableList_1_t2495751084 *)L_0);
		ImmutableList_1_t2495751084 * L_2 = ((  ImmutableList_1_t2495751084 * (*) (ImmutableList_1_t2495751084 *, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)((ImmutableList_1_t2495751084 *)L_0, (Il2CppObject*)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		ListObserver_1_t3813132649 * L_3 = (ListObserver_1_t3813132649 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		((  void (*) (ListObserver_1_t3813132649 *, ImmutableList_1_t2495751084 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)(L_3, (ImmutableList_1_t2495751084 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		return L_3;
	}
}
// UniRx.IObserver`1<T> UniRx.InternalUtil.ListObserver`1<UnityEngine.Bounds>::Remove(UniRx.IObserver`1<T>)
extern "C"  Il2CppObject* ListObserver_1_Remove_m1549300766_gshared (ListObserver_1_t3813132649 * __this, Il2CppObject* ___observer0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		ImmutableList_1_t2495751084 * L_0 = (ImmutableList_1_t2495751084 *)__this->get__observers_0();
		NullCheck((ImmutableList_1_t2495751084 *)L_0);
		IObserver_1U5BU5D_t579021988* L_1 = ((  IObserver_1U5BU5D_t579021988* (*) (ImmutableList_1_t2495751084 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((ImmutableList_1_t2495751084 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		Il2CppObject* L_2 = ___observer0;
		int32_t L_3 = ((  int32_t (*) (Il2CppObject * /* static, unused */, IObserver_1U5BU5D_t579021988*, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->method)(NULL /*static, unused*/, (IObserver_1U5BU5D_t579021988*)L_1, (Il2CppObject*)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		V_0 = (int32_t)L_3;
		int32_t L_4 = V_0;
		if ((((int32_t)L_4) >= ((int32_t)0)))
		{
			goto IL_001b;
		}
	}
	{
		return __this;
	}

IL_001b:
	{
		ImmutableList_1_t2495751084 * L_5 = (ImmutableList_1_t2495751084 *)__this->get__observers_0();
		NullCheck((ImmutableList_1_t2495751084 *)L_5);
		IObserver_1U5BU5D_t579021988* L_6 = ((  IObserver_1U5BU5D_t579021988* (*) (ImmutableList_1_t2495751084 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((ImmutableList_1_t2495751084 *)L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		NullCheck(L_6);
		if ((!(((uint32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_6)->max_length))))) == ((uint32_t)2))))
		{
			goto IL_003e;
		}
	}
	{
		ImmutableList_1_t2495751084 * L_7 = (ImmutableList_1_t2495751084 *)__this->get__observers_0();
		NullCheck((ImmutableList_1_t2495751084 *)L_7);
		IObserver_1U5BU5D_t579021988* L_8 = ((  IObserver_1U5BU5D_t579021988* (*) (ImmutableList_1_t2495751084 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((ImmutableList_1_t2495751084 *)L_7, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_9 = V_0;
		NullCheck(L_8);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_8, ((int32_t)((int32_t)1-(int32_t)L_9)));
		int32_t L_10 = ((int32_t)((int32_t)1-(int32_t)L_9));
		return ((L_8)->GetAt(static_cast<il2cpp_array_size_t>(L_10)));
	}

IL_003e:
	{
		ImmutableList_1_t2495751084 * L_11 = (ImmutableList_1_t2495751084 *)__this->get__observers_0();
		Il2CppObject* L_12 = ___observer0;
		NullCheck((ImmutableList_1_t2495751084 *)L_11);
		ImmutableList_1_t2495751084 * L_13 = ((  ImmutableList_1_t2495751084 * (*) (ImmutableList_1_t2495751084 *, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6)->method)((ImmutableList_1_t2495751084 *)L_11, (Il2CppObject*)L_12, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		ListObserver_1_t3813132649 * L_14 = (ListObserver_1_t3813132649 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		((  void (*) (ListObserver_1_t3813132649 *, ImmutableList_1_t2495751084 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)(L_14, (ImmutableList_1_t2495751084 *)L_13, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		return L_14;
	}
}
// System.Void UniRx.InternalUtil.ListObserver`1<UnityEngine.Color>::.ctor(UniRx.InternalUtil.ImmutableList`1<UniRx.IObserver`1<T>>)
extern "C"  void ListObserver_1__ctor_m200844175_gshared (ListObserver_1_t1882793431 * __this, ImmutableList_1_t565411866 * ___observers0, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		ImmutableList_1_t565411866 * L_0 = ___observers0;
		__this->set__observers_0(L_0);
		return;
	}
}
// System.Void UniRx.InternalUtil.ListObserver`1<UnityEngine.Color>::OnCompleted()
extern "C"  void ListObserver_1_OnCompleted_m949706631_gshared (ListObserver_1_t1882793431 * __this, const MethodInfo* method)
{
	IObserver_1U5BU5D_t3169245886* V_0 = NULL;
	int32_t V_1 = 0;
	{
		ImmutableList_1_t565411866 * L_0 = (ImmutableList_1_t565411866 *)__this->get__observers_0();
		NullCheck((ImmutableList_1_t565411866 *)L_0);
		IObserver_1U5BU5D_t3169245886* L_1 = ((  IObserver_1U5BU5D_t3169245886* (*) (ImmutableList_1_t565411866 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((ImmutableList_1_t565411866 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		V_0 = (IObserver_1U5BU5D_t3169245886*)L_1;
		V_1 = (int32_t)0;
		goto IL_001f;
	}

IL_0013:
	{
		IObserver_1U5BU5D_t3169245886* L_2 = V_0;
		int32_t L_3 = V_1;
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, L_3);
		int32_t L_4 = L_3;
		NullCheck((Il2CppObject*)((L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_4))));
		InterfaceActionInvoker0::Invoke(0 /* System.Void UniRx.IObserver`1<UnityEngine.Color>::OnCompleted() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), (Il2CppObject*)((L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_4))));
		int32_t L_5 = V_1;
		V_1 = (int32_t)((int32_t)((int32_t)L_5+(int32_t)1));
	}

IL_001f:
	{
		int32_t L_6 = V_1;
		IObserver_1U5BU5D_t3169245886* L_7 = V_0;
		NullCheck(L_7);
		if ((((int32_t)L_6) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_7)->max_length)))))))
		{
			goto IL_0013;
		}
	}
	{
		return;
	}
}
// System.Void UniRx.InternalUtil.ListObserver`1<UnityEngine.Color>::OnError(System.Exception)
extern "C"  void ListObserver_1_OnError_m2873622068_gshared (ListObserver_1_t1882793431 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method)
{
	IObserver_1U5BU5D_t3169245886* V_0 = NULL;
	int32_t V_1 = 0;
	{
		ImmutableList_1_t565411866 * L_0 = (ImmutableList_1_t565411866 *)__this->get__observers_0();
		NullCheck((ImmutableList_1_t565411866 *)L_0);
		IObserver_1U5BU5D_t3169245886* L_1 = ((  IObserver_1U5BU5D_t3169245886* (*) (ImmutableList_1_t565411866 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((ImmutableList_1_t565411866 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		V_0 = (IObserver_1U5BU5D_t3169245886*)L_1;
		V_1 = (int32_t)0;
		goto IL_0020;
	}

IL_0013:
	{
		IObserver_1U5BU5D_t3169245886* L_2 = V_0;
		int32_t L_3 = V_1;
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, L_3);
		int32_t L_4 = L_3;
		Exception_t1967233988 * L_5 = ___error0;
		NullCheck((Il2CppObject*)((L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_4))));
		InterfaceActionInvoker1< Exception_t1967233988 * >::Invoke(1 /* System.Void UniRx.IObserver`1<UnityEngine.Color>::OnError(System.Exception) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), (Il2CppObject*)((L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_4))), (Exception_t1967233988 *)L_5);
		int32_t L_6 = V_1;
		V_1 = (int32_t)((int32_t)((int32_t)L_6+(int32_t)1));
	}

IL_0020:
	{
		int32_t L_7 = V_1;
		IObserver_1U5BU5D_t3169245886* L_8 = V_0;
		NullCheck(L_8);
		if ((((int32_t)L_7) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_8)->max_length)))))))
		{
			goto IL_0013;
		}
	}
	{
		return;
	}
}
// System.Void UniRx.InternalUtil.ListObserver`1<UnityEngine.Color>::OnNext(T)
extern "C"  void ListObserver_1_OnNext_m1890635045_gshared (ListObserver_1_t1882793431 * __this, Color_t1588175760  ___value0, const MethodInfo* method)
{
	IObserver_1U5BU5D_t3169245886* V_0 = NULL;
	int32_t V_1 = 0;
	{
		ImmutableList_1_t565411866 * L_0 = (ImmutableList_1_t565411866 *)__this->get__observers_0();
		NullCheck((ImmutableList_1_t565411866 *)L_0);
		IObserver_1U5BU5D_t3169245886* L_1 = ((  IObserver_1U5BU5D_t3169245886* (*) (ImmutableList_1_t565411866 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((ImmutableList_1_t565411866 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		V_0 = (IObserver_1U5BU5D_t3169245886*)L_1;
		V_1 = (int32_t)0;
		goto IL_0020;
	}

IL_0013:
	{
		IObserver_1U5BU5D_t3169245886* L_2 = V_0;
		int32_t L_3 = V_1;
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, L_3);
		int32_t L_4 = L_3;
		Color_t1588175760  L_5 = ___value0;
		NullCheck((Il2CppObject*)((L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_4))));
		InterfaceActionInvoker1< Color_t1588175760  >::Invoke(2 /* System.Void UniRx.IObserver`1<UnityEngine.Color>::OnNext(T) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), (Il2CppObject*)((L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_4))), (Color_t1588175760 )L_5);
		int32_t L_6 = V_1;
		V_1 = (int32_t)((int32_t)((int32_t)L_6+(int32_t)1));
	}

IL_0020:
	{
		int32_t L_7 = V_1;
		IObserver_1U5BU5D_t3169245886* L_8 = V_0;
		NullCheck(L_8);
		if ((((int32_t)L_7) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_8)->max_length)))))))
		{
			goto IL_0013;
		}
	}
	{
		return;
	}
}
// UniRx.IObserver`1<T> UniRx.InternalUtil.ListObserver`1<UnityEngine.Color>::Add(UniRx.IObserver`1<T>)
extern "C"  Il2CppObject* ListObserver_1_Add_m174569527_gshared (ListObserver_1_t1882793431 * __this, Il2CppObject* ___observer0, const MethodInfo* method)
{
	{
		ImmutableList_1_t565411866 * L_0 = (ImmutableList_1_t565411866 *)__this->get__observers_0();
		Il2CppObject* L_1 = ___observer0;
		NullCheck((ImmutableList_1_t565411866 *)L_0);
		ImmutableList_1_t565411866 * L_2 = ((  ImmutableList_1_t565411866 * (*) (ImmutableList_1_t565411866 *, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)((ImmutableList_1_t565411866 *)L_0, (Il2CppObject*)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		ListObserver_1_t1882793431 * L_3 = (ListObserver_1_t1882793431 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		((  void (*) (ListObserver_1_t1882793431 *, ImmutableList_1_t565411866 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)(L_3, (ImmutableList_1_t565411866 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		return L_3;
	}
}
// UniRx.IObserver`1<T> UniRx.InternalUtil.ListObserver`1<UnityEngine.Color>::Remove(UniRx.IObserver`1<T>)
extern "C"  Il2CppObject* ListObserver_1_Remove_m4123796404_gshared (ListObserver_1_t1882793431 * __this, Il2CppObject* ___observer0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		ImmutableList_1_t565411866 * L_0 = (ImmutableList_1_t565411866 *)__this->get__observers_0();
		NullCheck((ImmutableList_1_t565411866 *)L_0);
		IObserver_1U5BU5D_t3169245886* L_1 = ((  IObserver_1U5BU5D_t3169245886* (*) (ImmutableList_1_t565411866 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((ImmutableList_1_t565411866 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		Il2CppObject* L_2 = ___observer0;
		int32_t L_3 = ((  int32_t (*) (Il2CppObject * /* static, unused */, IObserver_1U5BU5D_t3169245886*, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->method)(NULL /*static, unused*/, (IObserver_1U5BU5D_t3169245886*)L_1, (Il2CppObject*)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		V_0 = (int32_t)L_3;
		int32_t L_4 = V_0;
		if ((((int32_t)L_4) >= ((int32_t)0)))
		{
			goto IL_001b;
		}
	}
	{
		return __this;
	}

IL_001b:
	{
		ImmutableList_1_t565411866 * L_5 = (ImmutableList_1_t565411866 *)__this->get__observers_0();
		NullCheck((ImmutableList_1_t565411866 *)L_5);
		IObserver_1U5BU5D_t3169245886* L_6 = ((  IObserver_1U5BU5D_t3169245886* (*) (ImmutableList_1_t565411866 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((ImmutableList_1_t565411866 *)L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		NullCheck(L_6);
		if ((!(((uint32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_6)->max_length))))) == ((uint32_t)2))))
		{
			goto IL_003e;
		}
	}
	{
		ImmutableList_1_t565411866 * L_7 = (ImmutableList_1_t565411866 *)__this->get__observers_0();
		NullCheck((ImmutableList_1_t565411866 *)L_7);
		IObserver_1U5BU5D_t3169245886* L_8 = ((  IObserver_1U5BU5D_t3169245886* (*) (ImmutableList_1_t565411866 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((ImmutableList_1_t565411866 *)L_7, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_9 = V_0;
		NullCheck(L_8);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_8, ((int32_t)((int32_t)1-(int32_t)L_9)));
		int32_t L_10 = ((int32_t)((int32_t)1-(int32_t)L_9));
		return ((L_8)->GetAt(static_cast<il2cpp_array_size_t>(L_10)));
	}

IL_003e:
	{
		ImmutableList_1_t565411866 * L_11 = (ImmutableList_1_t565411866 *)__this->get__observers_0();
		Il2CppObject* L_12 = ___observer0;
		NullCheck((ImmutableList_1_t565411866 *)L_11);
		ImmutableList_1_t565411866 * L_13 = ((  ImmutableList_1_t565411866 * (*) (ImmutableList_1_t565411866 *, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6)->method)((ImmutableList_1_t565411866 *)L_11, (Il2CppObject*)L_12, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		ListObserver_1_t1882793431 * L_14 = (ListObserver_1_t1882793431 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		((  void (*) (ListObserver_1_t1882793431 *, ImmutableList_1_t565411866 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)(L_14, (ImmutableList_1_t565411866 *)L_13, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		return L_14;
	}
}
// System.Void UniRx.InternalUtil.ListObserver`1<UnityEngine.MasterServerEvent>::.ctor(UniRx.InternalUtil.ImmutableList`1<UniRx.IObserver`1<T>>)
extern "C"  void ListObserver_1__ctor_m16984449_gshared (ListObserver_1_t2572425161 * __this, ImmutableList_1_t1255043596 * ___observers0, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		ImmutableList_1_t1255043596 * L_0 = ___observers0;
		__this->set__observers_0(L_0);
		return;
	}
}
// System.Void UniRx.InternalUtil.ListObserver`1<UnityEngine.MasterServerEvent>::OnCompleted()
extern "C"  void ListObserver_1_OnCompleted_m1840260601_gshared (ListObserver_1_t2572425161 * __this, const MethodInfo* method)
{
	IObserver_1U5BU5D_t2992830660* V_0 = NULL;
	int32_t V_1 = 0;
	{
		ImmutableList_1_t1255043596 * L_0 = (ImmutableList_1_t1255043596 *)__this->get__observers_0();
		NullCheck((ImmutableList_1_t1255043596 *)L_0);
		IObserver_1U5BU5D_t2992830660* L_1 = ((  IObserver_1U5BU5D_t2992830660* (*) (ImmutableList_1_t1255043596 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((ImmutableList_1_t1255043596 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		V_0 = (IObserver_1U5BU5D_t2992830660*)L_1;
		V_1 = (int32_t)0;
		goto IL_001f;
	}

IL_0013:
	{
		IObserver_1U5BU5D_t2992830660* L_2 = V_0;
		int32_t L_3 = V_1;
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, L_3);
		int32_t L_4 = L_3;
		NullCheck((Il2CppObject*)((L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_4))));
		InterfaceActionInvoker0::Invoke(0 /* System.Void UniRx.IObserver`1<UnityEngine.MasterServerEvent>::OnCompleted() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), (Il2CppObject*)((L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_4))));
		int32_t L_5 = V_1;
		V_1 = (int32_t)((int32_t)((int32_t)L_5+(int32_t)1));
	}

IL_001f:
	{
		int32_t L_6 = V_1;
		IObserver_1U5BU5D_t2992830660* L_7 = V_0;
		NullCheck(L_7);
		if ((((int32_t)L_6) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_7)->max_length)))))))
		{
			goto IL_0013;
		}
	}
	{
		return;
	}
}
// System.Void UniRx.InternalUtil.ListObserver`1<UnityEngine.MasterServerEvent>::OnError(System.Exception)
extern "C"  void ListObserver_1_OnError_m1874635174_gshared (ListObserver_1_t2572425161 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method)
{
	IObserver_1U5BU5D_t2992830660* V_0 = NULL;
	int32_t V_1 = 0;
	{
		ImmutableList_1_t1255043596 * L_0 = (ImmutableList_1_t1255043596 *)__this->get__observers_0();
		NullCheck((ImmutableList_1_t1255043596 *)L_0);
		IObserver_1U5BU5D_t2992830660* L_1 = ((  IObserver_1U5BU5D_t2992830660* (*) (ImmutableList_1_t1255043596 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((ImmutableList_1_t1255043596 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		V_0 = (IObserver_1U5BU5D_t2992830660*)L_1;
		V_1 = (int32_t)0;
		goto IL_0020;
	}

IL_0013:
	{
		IObserver_1U5BU5D_t2992830660* L_2 = V_0;
		int32_t L_3 = V_1;
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, L_3);
		int32_t L_4 = L_3;
		Exception_t1967233988 * L_5 = ___error0;
		NullCheck((Il2CppObject*)((L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_4))));
		InterfaceActionInvoker1< Exception_t1967233988 * >::Invoke(1 /* System.Void UniRx.IObserver`1<UnityEngine.MasterServerEvent>::OnError(System.Exception) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), (Il2CppObject*)((L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_4))), (Exception_t1967233988 *)L_5);
		int32_t L_6 = V_1;
		V_1 = (int32_t)((int32_t)((int32_t)L_6+(int32_t)1));
	}

IL_0020:
	{
		int32_t L_7 = V_1;
		IObserver_1U5BU5D_t2992830660* L_8 = V_0;
		NullCheck(L_8);
		if ((((int32_t)L_7) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_8)->max_length)))))))
		{
			goto IL_0013;
		}
	}
	{
		return;
	}
}
// System.Void UniRx.InternalUtil.ListObserver`1<UnityEngine.MasterServerEvent>::OnNext(T)
extern "C"  void ListObserver_1_OnNext_m687774871_gshared (ListObserver_1_t2572425161 * __this, int32_t ___value0, const MethodInfo* method)
{
	IObserver_1U5BU5D_t2992830660* V_0 = NULL;
	int32_t V_1 = 0;
	{
		ImmutableList_1_t1255043596 * L_0 = (ImmutableList_1_t1255043596 *)__this->get__observers_0();
		NullCheck((ImmutableList_1_t1255043596 *)L_0);
		IObserver_1U5BU5D_t2992830660* L_1 = ((  IObserver_1U5BU5D_t2992830660* (*) (ImmutableList_1_t1255043596 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((ImmutableList_1_t1255043596 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		V_0 = (IObserver_1U5BU5D_t2992830660*)L_1;
		V_1 = (int32_t)0;
		goto IL_0020;
	}

IL_0013:
	{
		IObserver_1U5BU5D_t2992830660* L_2 = V_0;
		int32_t L_3 = V_1;
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, L_3);
		int32_t L_4 = L_3;
		int32_t L_5 = ___value0;
		NullCheck((Il2CppObject*)((L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_4))));
		InterfaceActionInvoker1< int32_t >::Invoke(2 /* System.Void UniRx.IObserver`1<UnityEngine.MasterServerEvent>::OnNext(T) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), (Il2CppObject*)((L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_4))), (int32_t)L_5);
		int32_t L_6 = V_1;
		V_1 = (int32_t)((int32_t)((int32_t)L_6+(int32_t)1));
	}

IL_0020:
	{
		int32_t L_7 = V_1;
		IObserver_1U5BU5D_t2992830660* L_8 = V_0;
		NullCheck(L_8);
		if ((((int32_t)L_7) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_8)->max_length)))))))
		{
			goto IL_0013;
		}
	}
	{
		return;
	}
}
// UniRx.IObserver`1<T> UniRx.InternalUtil.ListObserver`1<UnityEngine.MasterServerEvent>::Add(UniRx.IObserver`1<T>)
extern "C"  Il2CppObject* ListObserver_1_Add_m3398811177_gshared (ListObserver_1_t2572425161 * __this, Il2CppObject* ___observer0, const MethodInfo* method)
{
	{
		ImmutableList_1_t1255043596 * L_0 = (ImmutableList_1_t1255043596 *)__this->get__observers_0();
		Il2CppObject* L_1 = ___observer0;
		NullCheck((ImmutableList_1_t1255043596 *)L_0);
		ImmutableList_1_t1255043596 * L_2 = ((  ImmutableList_1_t1255043596 * (*) (ImmutableList_1_t1255043596 *, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)((ImmutableList_1_t1255043596 *)L_0, (Il2CppObject*)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		ListObserver_1_t2572425161 * L_3 = (ListObserver_1_t2572425161 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		((  void (*) (ListObserver_1_t2572425161 *, ImmutableList_1_t1255043596 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)(L_3, (ImmutableList_1_t1255043596 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		return L_3;
	}
}
// UniRx.IObserver`1<T> UniRx.InternalUtil.ListObserver`1<UnityEngine.MasterServerEvent>::Remove(UniRx.IObserver`1<T>)
extern "C"  Il2CppObject* ListObserver_1_Remove_m563216514_gshared (ListObserver_1_t2572425161 * __this, Il2CppObject* ___observer0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		ImmutableList_1_t1255043596 * L_0 = (ImmutableList_1_t1255043596 *)__this->get__observers_0();
		NullCheck((ImmutableList_1_t1255043596 *)L_0);
		IObserver_1U5BU5D_t2992830660* L_1 = ((  IObserver_1U5BU5D_t2992830660* (*) (ImmutableList_1_t1255043596 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((ImmutableList_1_t1255043596 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		Il2CppObject* L_2 = ___observer0;
		int32_t L_3 = ((  int32_t (*) (Il2CppObject * /* static, unused */, IObserver_1U5BU5D_t2992830660*, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->method)(NULL /*static, unused*/, (IObserver_1U5BU5D_t2992830660*)L_1, (Il2CppObject*)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		V_0 = (int32_t)L_3;
		int32_t L_4 = V_0;
		if ((((int32_t)L_4) >= ((int32_t)0)))
		{
			goto IL_001b;
		}
	}
	{
		return __this;
	}

IL_001b:
	{
		ImmutableList_1_t1255043596 * L_5 = (ImmutableList_1_t1255043596 *)__this->get__observers_0();
		NullCheck((ImmutableList_1_t1255043596 *)L_5);
		IObserver_1U5BU5D_t2992830660* L_6 = ((  IObserver_1U5BU5D_t2992830660* (*) (ImmutableList_1_t1255043596 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((ImmutableList_1_t1255043596 *)L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		NullCheck(L_6);
		if ((!(((uint32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_6)->max_length))))) == ((uint32_t)2))))
		{
			goto IL_003e;
		}
	}
	{
		ImmutableList_1_t1255043596 * L_7 = (ImmutableList_1_t1255043596 *)__this->get__observers_0();
		NullCheck((ImmutableList_1_t1255043596 *)L_7);
		IObserver_1U5BU5D_t2992830660* L_8 = ((  IObserver_1U5BU5D_t2992830660* (*) (ImmutableList_1_t1255043596 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((ImmutableList_1_t1255043596 *)L_7, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_9 = V_0;
		NullCheck(L_8);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_8, ((int32_t)((int32_t)1-(int32_t)L_9)));
		int32_t L_10 = ((int32_t)((int32_t)1-(int32_t)L_9));
		return ((L_8)->GetAt(static_cast<il2cpp_array_size_t>(L_10)));
	}

IL_003e:
	{
		ImmutableList_1_t1255043596 * L_11 = (ImmutableList_1_t1255043596 *)__this->get__observers_0();
		Il2CppObject* L_12 = ___observer0;
		NullCheck((ImmutableList_1_t1255043596 *)L_11);
		ImmutableList_1_t1255043596 * L_13 = ((  ImmutableList_1_t1255043596 * (*) (ImmutableList_1_t1255043596 *, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6)->method)((ImmutableList_1_t1255043596 *)L_11, (Il2CppObject*)L_12, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		ListObserver_1_t2572425161 * L_14 = (ListObserver_1_t2572425161 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		((  void (*) (ListObserver_1_t2572425161 *, ImmutableList_1_t1255043596 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)(L_14, (ImmutableList_1_t1255043596 *)L_13, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		return L_14;
	}
}
// System.Void UniRx.InternalUtil.ListObserver`1<UnityEngine.NetworkConnectionError>::.ctor(UniRx.InternalUtil.ImmutableList`1<UniRx.IObserver`1<T>>)
extern "C"  void ListObserver_1__ctor_m1984644638_gshared (ListObserver_1_t1318423664 * __this, ImmutableList_1_t1042099 * ___observers0, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		ImmutableList_1_t1042099 * L_0 = ___observers0;
		__this->set__observers_0(L_0);
		return;
	}
}
// System.Void UniRx.InternalUtil.ListObserver`1<UnityEngine.NetworkConnectionError>::OnCompleted()
extern "C"  void ListObserver_1_OnCompleted_m1036309462_gshared (ListObserver_1_t1318423664 * __this, const MethodInfo* method)
{
	IObserver_1U5BU5D_t1548317089* V_0 = NULL;
	int32_t V_1 = 0;
	{
		ImmutableList_1_t1042099 * L_0 = (ImmutableList_1_t1042099 *)__this->get__observers_0();
		NullCheck((ImmutableList_1_t1042099 *)L_0);
		IObserver_1U5BU5D_t1548317089* L_1 = ((  IObserver_1U5BU5D_t1548317089* (*) (ImmutableList_1_t1042099 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((ImmutableList_1_t1042099 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		V_0 = (IObserver_1U5BU5D_t1548317089*)L_1;
		V_1 = (int32_t)0;
		goto IL_001f;
	}

IL_0013:
	{
		IObserver_1U5BU5D_t1548317089* L_2 = V_0;
		int32_t L_3 = V_1;
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, L_3);
		int32_t L_4 = L_3;
		NullCheck((Il2CppObject*)((L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_4))));
		InterfaceActionInvoker0::Invoke(0 /* System.Void UniRx.IObserver`1<UnityEngine.NetworkConnectionError>::OnCompleted() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), (Il2CppObject*)((L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_4))));
		int32_t L_5 = V_1;
		V_1 = (int32_t)((int32_t)((int32_t)L_5+(int32_t)1));
	}

IL_001f:
	{
		int32_t L_6 = V_1;
		IObserver_1U5BU5D_t1548317089* L_7 = V_0;
		NullCheck(L_7);
		if ((((int32_t)L_6) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_7)->max_length)))))))
		{
			goto IL_0013;
		}
	}
	{
		return;
	}
}
// System.Void UniRx.InternalUtil.ListObserver`1<UnityEngine.NetworkConnectionError>::OnError(System.Exception)
extern "C"  void ListObserver_1_OnError_m877818883_gshared (ListObserver_1_t1318423664 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method)
{
	IObserver_1U5BU5D_t1548317089* V_0 = NULL;
	int32_t V_1 = 0;
	{
		ImmutableList_1_t1042099 * L_0 = (ImmutableList_1_t1042099 *)__this->get__observers_0();
		NullCheck((ImmutableList_1_t1042099 *)L_0);
		IObserver_1U5BU5D_t1548317089* L_1 = ((  IObserver_1U5BU5D_t1548317089* (*) (ImmutableList_1_t1042099 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((ImmutableList_1_t1042099 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		V_0 = (IObserver_1U5BU5D_t1548317089*)L_1;
		V_1 = (int32_t)0;
		goto IL_0020;
	}

IL_0013:
	{
		IObserver_1U5BU5D_t1548317089* L_2 = V_0;
		int32_t L_3 = V_1;
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, L_3);
		int32_t L_4 = L_3;
		Exception_t1967233988 * L_5 = ___error0;
		NullCheck((Il2CppObject*)((L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_4))));
		InterfaceActionInvoker1< Exception_t1967233988 * >::Invoke(1 /* System.Void UniRx.IObserver`1<UnityEngine.NetworkConnectionError>::OnError(System.Exception) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), (Il2CppObject*)((L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_4))), (Exception_t1967233988 *)L_5);
		int32_t L_6 = V_1;
		V_1 = (int32_t)((int32_t)((int32_t)L_6+(int32_t)1));
	}

IL_0020:
	{
		int32_t L_7 = V_1;
		IObserver_1U5BU5D_t1548317089* L_8 = V_0;
		NullCheck(L_8);
		if ((((int32_t)L_7) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_8)->max_length)))))))
		{
			goto IL_0013;
		}
	}
	{
		return;
	}
}
// System.Void UniRx.InternalUtil.ListObserver`1<UnityEngine.NetworkConnectionError>::OnNext(T)
extern "C"  void ListObserver_1_OnNext_m212254964_gshared (ListObserver_1_t1318423664 * __this, int32_t ___value0, const MethodInfo* method)
{
	IObserver_1U5BU5D_t1548317089* V_0 = NULL;
	int32_t V_1 = 0;
	{
		ImmutableList_1_t1042099 * L_0 = (ImmutableList_1_t1042099 *)__this->get__observers_0();
		NullCheck((ImmutableList_1_t1042099 *)L_0);
		IObserver_1U5BU5D_t1548317089* L_1 = ((  IObserver_1U5BU5D_t1548317089* (*) (ImmutableList_1_t1042099 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((ImmutableList_1_t1042099 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		V_0 = (IObserver_1U5BU5D_t1548317089*)L_1;
		V_1 = (int32_t)0;
		goto IL_0020;
	}

IL_0013:
	{
		IObserver_1U5BU5D_t1548317089* L_2 = V_0;
		int32_t L_3 = V_1;
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, L_3);
		int32_t L_4 = L_3;
		int32_t L_5 = ___value0;
		NullCheck((Il2CppObject*)((L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_4))));
		InterfaceActionInvoker1< int32_t >::Invoke(2 /* System.Void UniRx.IObserver`1<UnityEngine.NetworkConnectionError>::OnNext(T) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), (Il2CppObject*)((L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_4))), (int32_t)L_5);
		int32_t L_6 = V_1;
		V_1 = (int32_t)((int32_t)((int32_t)L_6+(int32_t)1));
	}

IL_0020:
	{
		int32_t L_7 = V_1;
		IObserver_1U5BU5D_t1548317089* L_8 = V_0;
		NullCheck(L_8);
		if ((((int32_t)L_7) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_8)->max_length)))))))
		{
			goto IL_0013;
		}
	}
	{
		return;
	}
}
// UniRx.IObserver`1<T> UniRx.InternalUtil.ListObserver`1<UnityEngine.NetworkConnectionError>::Add(UniRx.IObserver`1<T>)
extern "C"  Il2CppObject* ListObserver_1_Add_m1921289876_gshared (ListObserver_1_t1318423664 * __this, Il2CppObject* ___observer0, const MethodInfo* method)
{
	{
		ImmutableList_1_t1042099 * L_0 = (ImmutableList_1_t1042099 *)__this->get__observers_0();
		Il2CppObject* L_1 = ___observer0;
		NullCheck((ImmutableList_1_t1042099 *)L_0);
		ImmutableList_1_t1042099 * L_2 = ((  ImmutableList_1_t1042099 * (*) (ImmutableList_1_t1042099 *, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)((ImmutableList_1_t1042099 *)L_0, (Il2CppObject*)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		ListObserver_1_t1318423664 * L_3 = (ListObserver_1_t1318423664 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		((  void (*) (ListObserver_1_t1318423664 *, ImmutableList_1_t1042099 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)(L_3, (ImmutableList_1_t1042099 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		return L_3;
	}
}
// UniRx.IObserver`1<T> UniRx.InternalUtil.ListObserver`1<UnityEngine.NetworkConnectionError>::Remove(UniRx.IObserver`1<T>)
extern "C"  Il2CppObject* ListObserver_1_Remove_m2845955127_gshared (ListObserver_1_t1318423664 * __this, Il2CppObject* ___observer0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		ImmutableList_1_t1042099 * L_0 = (ImmutableList_1_t1042099 *)__this->get__observers_0();
		NullCheck((ImmutableList_1_t1042099 *)L_0);
		IObserver_1U5BU5D_t1548317089* L_1 = ((  IObserver_1U5BU5D_t1548317089* (*) (ImmutableList_1_t1042099 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((ImmutableList_1_t1042099 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		Il2CppObject* L_2 = ___observer0;
		int32_t L_3 = ((  int32_t (*) (Il2CppObject * /* static, unused */, IObserver_1U5BU5D_t1548317089*, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->method)(NULL /*static, unused*/, (IObserver_1U5BU5D_t1548317089*)L_1, (Il2CppObject*)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		V_0 = (int32_t)L_3;
		int32_t L_4 = V_0;
		if ((((int32_t)L_4) >= ((int32_t)0)))
		{
			goto IL_001b;
		}
	}
	{
		return __this;
	}

IL_001b:
	{
		ImmutableList_1_t1042099 * L_5 = (ImmutableList_1_t1042099 *)__this->get__observers_0();
		NullCheck((ImmutableList_1_t1042099 *)L_5);
		IObserver_1U5BU5D_t1548317089* L_6 = ((  IObserver_1U5BU5D_t1548317089* (*) (ImmutableList_1_t1042099 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((ImmutableList_1_t1042099 *)L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		NullCheck(L_6);
		if ((!(((uint32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_6)->max_length))))) == ((uint32_t)2))))
		{
			goto IL_003e;
		}
	}
	{
		ImmutableList_1_t1042099 * L_7 = (ImmutableList_1_t1042099 *)__this->get__observers_0();
		NullCheck((ImmutableList_1_t1042099 *)L_7);
		IObserver_1U5BU5D_t1548317089* L_8 = ((  IObserver_1U5BU5D_t1548317089* (*) (ImmutableList_1_t1042099 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((ImmutableList_1_t1042099 *)L_7, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_9 = V_0;
		NullCheck(L_8);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_8, ((int32_t)((int32_t)1-(int32_t)L_9)));
		int32_t L_10 = ((int32_t)((int32_t)1-(int32_t)L_9));
		return ((L_8)->GetAt(static_cast<il2cpp_array_size_t>(L_10)));
	}

IL_003e:
	{
		ImmutableList_1_t1042099 * L_11 = (ImmutableList_1_t1042099 *)__this->get__observers_0();
		Il2CppObject* L_12 = ___observer0;
		NullCheck((ImmutableList_1_t1042099 *)L_11);
		ImmutableList_1_t1042099 * L_13 = ((  ImmutableList_1_t1042099 * (*) (ImmutableList_1_t1042099 *, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6)->method)((ImmutableList_1_t1042099 *)L_11, (Il2CppObject*)L_12, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		ListObserver_1_t1318423664 * L_14 = (ListObserver_1_t1318423664 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		((  void (*) (ListObserver_1_t1318423664 *, ImmutableList_1_t1042099 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)(L_14, (ImmutableList_1_t1042099 *)L_13, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		return L_14;
	}
}
// System.Void UniRx.InternalUtil.ListObserver`1<UnityEngine.NetworkDisconnection>::.ctor(UniRx.InternalUtil.ImmutableList`1<UniRx.IObserver`1<T>>)
extern "C"  void ListObserver_1__ctor_m2654633216_gshared (ListObserver_1_t633378066 * __this, ImmutableList_1_t3610963797 * ___observers0, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		ImmutableList_1_t3610963797 * L_0 = ___observers0;
		__this->set__observers_0(L_0);
		return;
	}
}
// System.Void UniRx.InternalUtil.ListObserver`1<UnityEngine.NetworkDisconnection>::OnCompleted()
extern "C"  void ListObserver_1_OnCompleted_m4267534136_gshared (ListObserver_1_t633378066 * __this, const MethodInfo* method)
{
	IObserver_1U5BU5D_t3485751351* V_0 = NULL;
	int32_t V_1 = 0;
	{
		ImmutableList_1_t3610963797 * L_0 = (ImmutableList_1_t3610963797 *)__this->get__observers_0();
		NullCheck((ImmutableList_1_t3610963797 *)L_0);
		IObserver_1U5BU5D_t3485751351* L_1 = ((  IObserver_1U5BU5D_t3485751351* (*) (ImmutableList_1_t3610963797 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((ImmutableList_1_t3610963797 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		V_0 = (IObserver_1U5BU5D_t3485751351*)L_1;
		V_1 = (int32_t)0;
		goto IL_001f;
	}

IL_0013:
	{
		IObserver_1U5BU5D_t3485751351* L_2 = V_0;
		int32_t L_3 = V_1;
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, L_3);
		int32_t L_4 = L_3;
		NullCheck((Il2CppObject*)((L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_4))));
		InterfaceActionInvoker0::Invoke(0 /* System.Void UniRx.IObserver`1<UnityEngine.NetworkDisconnection>::OnCompleted() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), (Il2CppObject*)((L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_4))));
		int32_t L_5 = V_1;
		V_1 = (int32_t)((int32_t)((int32_t)L_5+(int32_t)1));
	}

IL_001f:
	{
		int32_t L_6 = V_1;
		IObserver_1U5BU5D_t3485751351* L_7 = V_0;
		NullCheck(L_7);
		if ((((int32_t)L_6) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_7)->max_length)))))))
		{
			goto IL_0013;
		}
	}
	{
		return;
	}
}
// System.Void UniRx.InternalUtil.ListObserver`1<UnityEngine.NetworkDisconnection>::OnError(System.Exception)
extern "C"  void ListObserver_1_OnError_m1540293733_gshared (ListObserver_1_t633378066 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method)
{
	IObserver_1U5BU5D_t3485751351* V_0 = NULL;
	int32_t V_1 = 0;
	{
		ImmutableList_1_t3610963797 * L_0 = (ImmutableList_1_t3610963797 *)__this->get__observers_0();
		NullCheck((ImmutableList_1_t3610963797 *)L_0);
		IObserver_1U5BU5D_t3485751351* L_1 = ((  IObserver_1U5BU5D_t3485751351* (*) (ImmutableList_1_t3610963797 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((ImmutableList_1_t3610963797 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		V_0 = (IObserver_1U5BU5D_t3485751351*)L_1;
		V_1 = (int32_t)0;
		goto IL_0020;
	}

IL_0013:
	{
		IObserver_1U5BU5D_t3485751351* L_2 = V_0;
		int32_t L_3 = V_1;
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, L_3);
		int32_t L_4 = L_3;
		Exception_t1967233988 * L_5 = ___error0;
		NullCheck((Il2CppObject*)((L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_4))));
		InterfaceActionInvoker1< Exception_t1967233988 * >::Invoke(1 /* System.Void UniRx.IObserver`1<UnityEngine.NetworkDisconnection>::OnError(System.Exception) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), (Il2CppObject*)((L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_4))), (Exception_t1967233988 *)L_5);
		int32_t L_6 = V_1;
		V_1 = (int32_t)((int32_t)((int32_t)L_6+(int32_t)1));
	}

IL_0020:
	{
		int32_t L_7 = V_1;
		IObserver_1U5BU5D_t3485751351* L_8 = V_0;
		NullCheck(L_8);
		if ((((int32_t)L_7) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_8)->max_length)))))))
		{
			goto IL_0013;
		}
	}
	{
		return;
	}
}
// System.Void UniRx.InternalUtil.ListObserver`1<UnityEngine.NetworkDisconnection>::OnNext(T)
extern "C"  void ListObserver_1_OnNext_m4173616470_gshared (ListObserver_1_t633378066 * __this, int32_t ___value0, const MethodInfo* method)
{
	IObserver_1U5BU5D_t3485751351* V_0 = NULL;
	int32_t V_1 = 0;
	{
		ImmutableList_1_t3610963797 * L_0 = (ImmutableList_1_t3610963797 *)__this->get__observers_0();
		NullCheck((ImmutableList_1_t3610963797 *)L_0);
		IObserver_1U5BU5D_t3485751351* L_1 = ((  IObserver_1U5BU5D_t3485751351* (*) (ImmutableList_1_t3610963797 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((ImmutableList_1_t3610963797 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		V_0 = (IObserver_1U5BU5D_t3485751351*)L_1;
		V_1 = (int32_t)0;
		goto IL_0020;
	}

IL_0013:
	{
		IObserver_1U5BU5D_t3485751351* L_2 = V_0;
		int32_t L_3 = V_1;
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, L_3);
		int32_t L_4 = L_3;
		int32_t L_5 = ___value0;
		NullCheck((Il2CppObject*)((L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_4))));
		InterfaceActionInvoker1< int32_t >::Invoke(2 /* System.Void UniRx.IObserver`1<UnityEngine.NetworkDisconnection>::OnNext(T) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), (Il2CppObject*)((L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_4))), (int32_t)L_5);
		int32_t L_6 = V_1;
		V_1 = (int32_t)((int32_t)((int32_t)L_6+(int32_t)1));
	}

IL_0020:
	{
		int32_t L_7 = V_1;
		IObserver_1U5BU5D_t3485751351* L_8 = V_0;
		NullCheck(L_8);
		if ((((int32_t)L_7) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_8)->max_length)))))))
		{
			goto IL_0013;
		}
	}
	{
		return;
	}
}
// UniRx.IObserver`1<T> UniRx.InternalUtil.ListObserver`1<UnityEngine.NetworkDisconnection>::Add(UniRx.IObserver`1<T>)
extern "C"  Il2CppObject* ListObserver_1_Add_m4160006326_gshared (ListObserver_1_t633378066 * __this, Il2CppObject* ___observer0, const MethodInfo* method)
{
	{
		ImmutableList_1_t3610963797 * L_0 = (ImmutableList_1_t3610963797 *)__this->get__observers_0();
		Il2CppObject* L_1 = ___observer0;
		NullCheck((ImmutableList_1_t3610963797 *)L_0);
		ImmutableList_1_t3610963797 * L_2 = ((  ImmutableList_1_t3610963797 * (*) (ImmutableList_1_t3610963797 *, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)((ImmutableList_1_t3610963797 *)L_0, (Il2CppObject*)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		ListObserver_1_t633378066 * L_3 = (ListObserver_1_t633378066 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		((  void (*) (ListObserver_1_t633378066 *, ImmutableList_1_t3610963797 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)(L_3, (ImmutableList_1_t3610963797 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		return L_3;
	}
}
// UniRx.IObserver`1<T> UniRx.InternalUtil.ListObserver`1<UnityEngine.NetworkDisconnection>::Remove(UniRx.IObserver`1<T>)
extern "C"  Il2CppObject* ListObserver_1_Remove_m4195544789_gshared (ListObserver_1_t633378066 * __this, Il2CppObject* ___observer0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		ImmutableList_1_t3610963797 * L_0 = (ImmutableList_1_t3610963797 *)__this->get__observers_0();
		NullCheck((ImmutableList_1_t3610963797 *)L_0);
		IObserver_1U5BU5D_t3485751351* L_1 = ((  IObserver_1U5BU5D_t3485751351* (*) (ImmutableList_1_t3610963797 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((ImmutableList_1_t3610963797 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		Il2CppObject* L_2 = ___observer0;
		int32_t L_3 = ((  int32_t (*) (Il2CppObject * /* static, unused */, IObserver_1U5BU5D_t3485751351*, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->method)(NULL /*static, unused*/, (IObserver_1U5BU5D_t3485751351*)L_1, (Il2CppObject*)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		V_0 = (int32_t)L_3;
		int32_t L_4 = V_0;
		if ((((int32_t)L_4) >= ((int32_t)0)))
		{
			goto IL_001b;
		}
	}
	{
		return __this;
	}

IL_001b:
	{
		ImmutableList_1_t3610963797 * L_5 = (ImmutableList_1_t3610963797 *)__this->get__observers_0();
		NullCheck((ImmutableList_1_t3610963797 *)L_5);
		IObserver_1U5BU5D_t3485751351* L_6 = ((  IObserver_1U5BU5D_t3485751351* (*) (ImmutableList_1_t3610963797 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((ImmutableList_1_t3610963797 *)L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		NullCheck(L_6);
		if ((!(((uint32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_6)->max_length))))) == ((uint32_t)2))))
		{
			goto IL_003e;
		}
	}
	{
		ImmutableList_1_t3610963797 * L_7 = (ImmutableList_1_t3610963797 *)__this->get__observers_0();
		NullCheck((ImmutableList_1_t3610963797 *)L_7);
		IObserver_1U5BU5D_t3485751351* L_8 = ((  IObserver_1U5BU5D_t3485751351* (*) (ImmutableList_1_t3610963797 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((ImmutableList_1_t3610963797 *)L_7, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_9 = V_0;
		NullCheck(L_8);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_8, ((int32_t)((int32_t)1-(int32_t)L_9)));
		int32_t L_10 = ((int32_t)((int32_t)1-(int32_t)L_9));
		return ((L_8)->GetAt(static_cast<il2cpp_array_size_t>(L_10)));
	}

IL_003e:
	{
		ImmutableList_1_t3610963797 * L_11 = (ImmutableList_1_t3610963797 *)__this->get__observers_0();
		Il2CppObject* L_12 = ___observer0;
		NullCheck((ImmutableList_1_t3610963797 *)L_11);
		ImmutableList_1_t3610963797 * L_13 = ((  ImmutableList_1_t3610963797 * (*) (ImmutableList_1_t3610963797 *, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6)->method)((ImmutableList_1_t3610963797 *)L_11, (Il2CppObject*)L_12, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		ListObserver_1_t633378066 * L_14 = (ListObserver_1_t633378066 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		((  void (*) (ListObserver_1_t633378066 *, ImmutableList_1_t3610963797 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)(L_14, (ImmutableList_1_t3610963797 *)L_13, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		return L_14;
	}
}
// System.Void UniRx.InternalUtil.ListObserver`1<UnityEngine.NetworkMessageInfo>::.ctor(UniRx.InternalUtil.ImmutableList`1<UniRx.IObserver`1<T>>)
extern "C"  void ListObserver_1__ctor_m1684225385_gshared (ListObserver_1_t2868962555 * __this, ImmutableList_1_t1551580990 * ___observers0, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		ImmutableList_1_t1551580990 * L_0 = ___observers0;
		__this->set__observers_0(L_0);
		return;
	}
}
// System.Void UniRx.InternalUtil.ListObserver`1<UnityEngine.NetworkMessageInfo>::OnCompleted()
extern "C"  void ListObserver_1_OnCompleted_m212763617_gshared (ListObserver_1_t2868962555 * __this, const MethodInfo* method)
{
	IObserver_1U5BU5D_t98838154* V_0 = NULL;
	int32_t V_1 = 0;
	{
		ImmutableList_1_t1551580990 * L_0 = (ImmutableList_1_t1551580990 *)__this->get__observers_0();
		NullCheck((ImmutableList_1_t1551580990 *)L_0);
		IObserver_1U5BU5D_t98838154* L_1 = ((  IObserver_1U5BU5D_t98838154* (*) (ImmutableList_1_t1551580990 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((ImmutableList_1_t1551580990 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		V_0 = (IObserver_1U5BU5D_t98838154*)L_1;
		V_1 = (int32_t)0;
		goto IL_001f;
	}

IL_0013:
	{
		IObserver_1U5BU5D_t98838154* L_2 = V_0;
		int32_t L_3 = V_1;
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, L_3);
		int32_t L_4 = L_3;
		NullCheck((Il2CppObject*)((L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_4))));
		InterfaceActionInvoker0::Invoke(0 /* System.Void UniRx.IObserver`1<UnityEngine.NetworkMessageInfo>::OnCompleted() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), (Il2CppObject*)((L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_4))));
		int32_t L_5 = V_1;
		V_1 = (int32_t)((int32_t)((int32_t)L_5+(int32_t)1));
	}

IL_001f:
	{
		int32_t L_6 = V_1;
		IObserver_1U5BU5D_t98838154* L_7 = V_0;
		NullCheck(L_7);
		if ((((int32_t)L_6) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_7)->max_length)))))))
		{
			goto IL_0013;
		}
	}
	{
		return;
	}
}
// System.Void UniRx.InternalUtil.ListObserver`1<UnityEngine.NetworkMessageInfo>::OnError(System.Exception)
extern "C"  void ListObserver_1_OnError_m1104505742_gshared (ListObserver_1_t2868962555 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method)
{
	IObserver_1U5BU5D_t98838154* V_0 = NULL;
	int32_t V_1 = 0;
	{
		ImmutableList_1_t1551580990 * L_0 = (ImmutableList_1_t1551580990 *)__this->get__observers_0();
		NullCheck((ImmutableList_1_t1551580990 *)L_0);
		IObserver_1U5BU5D_t98838154* L_1 = ((  IObserver_1U5BU5D_t98838154* (*) (ImmutableList_1_t1551580990 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((ImmutableList_1_t1551580990 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		V_0 = (IObserver_1U5BU5D_t98838154*)L_1;
		V_1 = (int32_t)0;
		goto IL_0020;
	}

IL_0013:
	{
		IObserver_1U5BU5D_t98838154* L_2 = V_0;
		int32_t L_3 = V_1;
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, L_3);
		int32_t L_4 = L_3;
		Exception_t1967233988 * L_5 = ___error0;
		NullCheck((Il2CppObject*)((L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_4))));
		InterfaceActionInvoker1< Exception_t1967233988 * >::Invoke(1 /* System.Void UniRx.IObserver`1<UnityEngine.NetworkMessageInfo>::OnError(System.Exception) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), (Il2CppObject*)((L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_4))), (Exception_t1967233988 *)L_5);
		int32_t L_6 = V_1;
		V_1 = (int32_t)((int32_t)((int32_t)L_6+(int32_t)1));
	}

IL_0020:
	{
		int32_t L_7 = V_1;
		IObserver_1U5BU5D_t98838154* L_8 = V_0;
		NullCheck(L_8);
		if ((((int32_t)L_7) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_8)->max_length)))))))
		{
			goto IL_0013;
		}
	}
	{
		return;
	}
}
// System.Void UniRx.InternalUtil.ListObserver`1<UnityEngine.NetworkMessageInfo>::OnNext(T)
extern "C"  void ListObserver_1_OnNext_m2624073343_gshared (ListObserver_1_t2868962555 * __this, NetworkMessageInfo_t2574344884  ___value0, const MethodInfo* method)
{
	IObserver_1U5BU5D_t98838154* V_0 = NULL;
	int32_t V_1 = 0;
	{
		ImmutableList_1_t1551580990 * L_0 = (ImmutableList_1_t1551580990 *)__this->get__observers_0();
		NullCheck((ImmutableList_1_t1551580990 *)L_0);
		IObserver_1U5BU5D_t98838154* L_1 = ((  IObserver_1U5BU5D_t98838154* (*) (ImmutableList_1_t1551580990 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((ImmutableList_1_t1551580990 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		V_0 = (IObserver_1U5BU5D_t98838154*)L_1;
		V_1 = (int32_t)0;
		goto IL_0020;
	}

IL_0013:
	{
		IObserver_1U5BU5D_t98838154* L_2 = V_0;
		int32_t L_3 = V_1;
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, L_3);
		int32_t L_4 = L_3;
		NetworkMessageInfo_t2574344884  L_5 = ___value0;
		NullCheck((Il2CppObject*)((L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_4))));
		InterfaceActionInvoker1< NetworkMessageInfo_t2574344884  >::Invoke(2 /* System.Void UniRx.IObserver`1<UnityEngine.NetworkMessageInfo>::OnNext(T) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), (Il2CppObject*)((L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_4))), (NetworkMessageInfo_t2574344884 )L_5);
		int32_t L_6 = V_1;
		V_1 = (int32_t)((int32_t)((int32_t)L_6+(int32_t)1));
	}

IL_0020:
	{
		int32_t L_7 = V_1;
		IObserver_1U5BU5D_t98838154* L_8 = V_0;
		NullCheck(L_8);
		if ((((int32_t)L_7) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_8)->max_length)))))))
		{
			goto IL_0013;
		}
	}
	{
		return;
	}
}
// UniRx.IObserver`1<T> UniRx.InternalUtil.ListObserver`1<UnityEngine.NetworkMessageInfo>::Add(UniRx.IObserver`1<T>)
extern "C"  Il2CppObject* ListObserver_1_Add_m1120274847_gshared (ListObserver_1_t2868962555 * __this, Il2CppObject* ___observer0, const MethodInfo* method)
{
	{
		ImmutableList_1_t1551580990 * L_0 = (ImmutableList_1_t1551580990 *)__this->get__observers_0();
		Il2CppObject* L_1 = ___observer0;
		NullCheck((ImmutableList_1_t1551580990 *)L_0);
		ImmutableList_1_t1551580990 * L_2 = ((  ImmutableList_1_t1551580990 * (*) (ImmutableList_1_t1551580990 *, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)((ImmutableList_1_t1551580990 *)L_0, (Il2CppObject*)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		ListObserver_1_t2868962555 * L_3 = (ListObserver_1_t2868962555 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		((  void (*) (ListObserver_1_t2868962555 *, ImmutableList_1_t1551580990 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)(L_3, (ImmutableList_1_t1551580990 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		return L_3;
	}
}
// UniRx.IObserver`1<T> UniRx.InternalUtil.ListObserver`1<UnityEngine.NetworkMessageInfo>::Remove(UniRx.IObserver`1<T>)
extern "C"  Il2CppObject* ListObserver_1_Remove_m2645522764_gshared (ListObserver_1_t2868962555 * __this, Il2CppObject* ___observer0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		ImmutableList_1_t1551580990 * L_0 = (ImmutableList_1_t1551580990 *)__this->get__observers_0();
		NullCheck((ImmutableList_1_t1551580990 *)L_0);
		IObserver_1U5BU5D_t98838154* L_1 = ((  IObserver_1U5BU5D_t98838154* (*) (ImmutableList_1_t1551580990 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((ImmutableList_1_t1551580990 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		Il2CppObject* L_2 = ___observer0;
		int32_t L_3 = ((  int32_t (*) (Il2CppObject * /* static, unused */, IObserver_1U5BU5D_t98838154*, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->method)(NULL /*static, unused*/, (IObserver_1U5BU5D_t98838154*)L_1, (Il2CppObject*)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		V_0 = (int32_t)L_3;
		int32_t L_4 = V_0;
		if ((((int32_t)L_4) >= ((int32_t)0)))
		{
			goto IL_001b;
		}
	}
	{
		return __this;
	}

IL_001b:
	{
		ImmutableList_1_t1551580990 * L_5 = (ImmutableList_1_t1551580990 *)__this->get__observers_0();
		NullCheck((ImmutableList_1_t1551580990 *)L_5);
		IObserver_1U5BU5D_t98838154* L_6 = ((  IObserver_1U5BU5D_t98838154* (*) (ImmutableList_1_t1551580990 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((ImmutableList_1_t1551580990 *)L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		NullCheck(L_6);
		if ((!(((uint32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_6)->max_length))))) == ((uint32_t)2))))
		{
			goto IL_003e;
		}
	}
	{
		ImmutableList_1_t1551580990 * L_7 = (ImmutableList_1_t1551580990 *)__this->get__observers_0();
		NullCheck((ImmutableList_1_t1551580990 *)L_7);
		IObserver_1U5BU5D_t98838154* L_8 = ((  IObserver_1U5BU5D_t98838154* (*) (ImmutableList_1_t1551580990 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((ImmutableList_1_t1551580990 *)L_7, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_9 = V_0;
		NullCheck(L_8);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_8, ((int32_t)((int32_t)1-(int32_t)L_9)));
		int32_t L_10 = ((int32_t)((int32_t)1-(int32_t)L_9));
		return ((L_8)->GetAt(static_cast<il2cpp_array_size_t>(L_10)));
	}

IL_003e:
	{
		ImmutableList_1_t1551580990 * L_11 = (ImmutableList_1_t1551580990 *)__this->get__observers_0();
		Il2CppObject* L_12 = ___observer0;
		NullCheck((ImmutableList_1_t1551580990 *)L_11);
		ImmutableList_1_t1551580990 * L_13 = ((  ImmutableList_1_t1551580990 * (*) (ImmutableList_1_t1551580990 *, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6)->method)((ImmutableList_1_t1551580990 *)L_11, (Il2CppObject*)L_12, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		ListObserver_1_t2868962555 * L_14 = (ListObserver_1_t2868962555 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		((  void (*) (ListObserver_1_t2868962555 *, ImmutableList_1_t1551580990 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)(L_14, (ImmutableList_1_t1551580990 *)L_13, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		return L_14;
	}
}
// System.Void UniRx.InternalUtil.ListObserver`1<UnityEngine.NetworkPlayer>::.ctor(UniRx.InternalUtil.ImmutableList`1<UniRx.IObserver`1<T>>)
extern "C"  void ListObserver_1__ctor_m2324206299_gshared (ListObserver_1_t1575755043 * __this, ImmutableList_1_t258373478 * ___observers0, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		ImmutableList_1_t258373478 * L_0 = ___observers0;
		__this->set__observers_0(L_0);
		return;
	}
}
// System.Void UniRx.InternalUtil.ListObserver`1<UnityEngine.NetworkPlayer>::OnCompleted()
extern "C"  void ListObserver_1_OnCompleted_m993589715_gshared (ListObserver_1_t1575755043 * __this, const MethodInfo* method)
{
	IObserver_1U5BU5D_t632717506* V_0 = NULL;
	int32_t V_1 = 0;
	{
		ImmutableList_1_t258373478 * L_0 = (ImmutableList_1_t258373478 *)__this->get__observers_0();
		NullCheck((ImmutableList_1_t258373478 *)L_0);
		IObserver_1U5BU5D_t632717506* L_1 = ((  IObserver_1U5BU5D_t632717506* (*) (ImmutableList_1_t258373478 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((ImmutableList_1_t258373478 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		V_0 = (IObserver_1U5BU5D_t632717506*)L_1;
		V_1 = (int32_t)0;
		goto IL_001f;
	}

IL_0013:
	{
		IObserver_1U5BU5D_t632717506* L_2 = V_0;
		int32_t L_3 = V_1;
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, L_3);
		int32_t L_4 = L_3;
		NullCheck((Il2CppObject*)((L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_4))));
		InterfaceActionInvoker0::Invoke(0 /* System.Void UniRx.IObserver`1<UnityEngine.NetworkPlayer>::OnCompleted() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), (Il2CppObject*)((L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_4))));
		int32_t L_5 = V_1;
		V_1 = (int32_t)((int32_t)((int32_t)L_5+(int32_t)1));
	}

IL_001f:
	{
		int32_t L_6 = V_1;
		IObserver_1U5BU5D_t632717506* L_7 = V_0;
		NullCheck(L_7);
		if ((((int32_t)L_6) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_7)->max_length)))))))
		{
			goto IL_0013;
		}
	}
	{
		return;
	}
}
// System.Void UniRx.InternalUtil.ListObserver`1<UnityEngine.NetworkPlayer>::OnError(System.Exception)
extern "C"  void ListObserver_1_OnError_m1261733504_gshared (ListObserver_1_t1575755043 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method)
{
	IObserver_1U5BU5D_t632717506* V_0 = NULL;
	int32_t V_1 = 0;
	{
		ImmutableList_1_t258373478 * L_0 = (ImmutableList_1_t258373478 *)__this->get__observers_0();
		NullCheck((ImmutableList_1_t258373478 *)L_0);
		IObserver_1U5BU5D_t632717506* L_1 = ((  IObserver_1U5BU5D_t632717506* (*) (ImmutableList_1_t258373478 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((ImmutableList_1_t258373478 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		V_0 = (IObserver_1U5BU5D_t632717506*)L_1;
		V_1 = (int32_t)0;
		goto IL_0020;
	}

IL_0013:
	{
		IObserver_1U5BU5D_t632717506* L_2 = V_0;
		int32_t L_3 = V_1;
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, L_3);
		int32_t L_4 = L_3;
		Exception_t1967233988 * L_5 = ___error0;
		NullCheck((Il2CppObject*)((L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_4))));
		InterfaceActionInvoker1< Exception_t1967233988 * >::Invoke(1 /* System.Void UniRx.IObserver`1<UnityEngine.NetworkPlayer>::OnError(System.Exception) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), (Il2CppObject*)((L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_4))), (Exception_t1967233988 *)L_5);
		int32_t L_6 = V_1;
		V_1 = (int32_t)((int32_t)((int32_t)L_6+(int32_t)1));
	}

IL_0020:
	{
		int32_t L_7 = V_1;
		IObserver_1U5BU5D_t632717506* L_8 = V_0;
		NullCheck(L_8);
		if ((((int32_t)L_7) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_8)->max_length)))))))
		{
			goto IL_0013;
		}
	}
	{
		return;
	}
}
// System.Void UniRx.InternalUtil.ListObserver`1<UnityEngine.NetworkPlayer>::OnNext(T)
extern "C"  void ListObserver_1_OnNext_m795831665_gshared (ListObserver_1_t1575755043 * __this, NetworkPlayer_t1281137372  ___value0, const MethodInfo* method)
{
	IObserver_1U5BU5D_t632717506* V_0 = NULL;
	int32_t V_1 = 0;
	{
		ImmutableList_1_t258373478 * L_0 = (ImmutableList_1_t258373478 *)__this->get__observers_0();
		NullCheck((ImmutableList_1_t258373478 *)L_0);
		IObserver_1U5BU5D_t632717506* L_1 = ((  IObserver_1U5BU5D_t632717506* (*) (ImmutableList_1_t258373478 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((ImmutableList_1_t258373478 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		V_0 = (IObserver_1U5BU5D_t632717506*)L_1;
		V_1 = (int32_t)0;
		goto IL_0020;
	}

IL_0013:
	{
		IObserver_1U5BU5D_t632717506* L_2 = V_0;
		int32_t L_3 = V_1;
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, L_3);
		int32_t L_4 = L_3;
		NetworkPlayer_t1281137372  L_5 = ___value0;
		NullCheck((Il2CppObject*)((L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_4))));
		InterfaceActionInvoker1< NetworkPlayer_t1281137372  >::Invoke(2 /* System.Void UniRx.IObserver`1<UnityEngine.NetworkPlayer>::OnNext(T) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), (Il2CppObject*)((L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_4))), (NetworkPlayer_t1281137372 )L_5);
		int32_t L_6 = V_1;
		V_1 = (int32_t)((int32_t)((int32_t)L_6+(int32_t)1));
	}

IL_0020:
	{
		int32_t L_7 = V_1;
		IObserver_1U5BU5D_t632717506* L_8 = V_0;
		NullCheck(L_8);
		if ((((int32_t)L_7) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_8)->max_length)))))))
		{
			goto IL_0013;
		}
	}
	{
		return;
	}
}
// UniRx.IObserver`1<T> UniRx.InternalUtil.ListObserver`1<UnityEngine.NetworkPlayer>::Add(UniRx.IObserver`1<T>)
extern "C"  Il2CppObject* ListObserver_1_Add_m942335875_gshared (ListObserver_1_t1575755043 * __this, Il2CppObject* ___observer0, const MethodInfo* method)
{
	{
		ImmutableList_1_t258373478 * L_0 = (ImmutableList_1_t258373478 *)__this->get__observers_0();
		Il2CppObject* L_1 = ___observer0;
		NullCheck((ImmutableList_1_t258373478 *)L_0);
		ImmutableList_1_t258373478 * L_2 = ((  ImmutableList_1_t258373478 * (*) (ImmutableList_1_t258373478 *, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)((ImmutableList_1_t258373478 *)L_0, (Il2CppObject*)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		ListObserver_1_t1575755043 * L_3 = (ListObserver_1_t1575755043 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		((  void (*) (ListObserver_1_t1575755043 *, ImmutableList_1_t258373478 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)(L_3, (ImmutableList_1_t258373478 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		return L_3;
	}
}
// UniRx.IObserver`1<T> UniRx.InternalUtil.ListObserver`1<UnityEngine.NetworkPlayer>::Remove(UniRx.IObserver`1<T>)
extern "C"  Il2CppObject* ListObserver_1_Remove_m1655251176_gshared (ListObserver_1_t1575755043 * __this, Il2CppObject* ___observer0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		ImmutableList_1_t258373478 * L_0 = (ImmutableList_1_t258373478 *)__this->get__observers_0();
		NullCheck((ImmutableList_1_t258373478 *)L_0);
		IObserver_1U5BU5D_t632717506* L_1 = ((  IObserver_1U5BU5D_t632717506* (*) (ImmutableList_1_t258373478 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((ImmutableList_1_t258373478 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		Il2CppObject* L_2 = ___observer0;
		int32_t L_3 = ((  int32_t (*) (Il2CppObject * /* static, unused */, IObserver_1U5BU5D_t632717506*, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->method)(NULL /*static, unused*/, (IObserver_1U5BU5D_t632717506*)L_1, (Il2CppObject*)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		V_0 = (int32_t)L_3;
		int32_t L_4 = V_0;
		if ((((int32_t)L_4) >= ((int32_t)0)))
		{
			goto IL_001b;
		}
	}
	{
		return __this;
	}

IL_001b:
	{
		ImmutableList_1_t258373478 * L_5 = (ImmutableList_1_t258373478 *)__this->get__observers_0();
		NullCheck((ImmutableList_1_t258373478 *)L_5);
		IObserver_1U5BU5D_t632717506* L_6 = ((  IObserver_1U5BU5D_t632717506* (*) (ImmutableList_1_t258373478 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((ImmutableList_1_t258373478 *)L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		NullCheck(L_6);
		if ((!(((uint32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_6)->max_length))))) == ((uint32_t)2))))
		{
			goto IL_003e;
		}
	}
	{
		ImmutableList_1_t258373478 * L_7 = (ImmutableList_1_t258373478 *)__this->get__observers_0();
		NullCheck((ImmutableList_1_t258373478 *)L_7);
		IObserver_1U5BU5D_t632717506* L_8 = ((  IObserver_1U5BU5D_t632717506* (*) (ImmutableList_1_t258373478 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((ImmutableList_1_t258373478 *)L_7, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_9 = V_0;
		NullCheck(L_8);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_8, ((int32_t)((int32_t)1-(int32_t)L_9)));
		int32_t L_10 = ((int32_t)((int32_t)1-(int32_t)L_9));
		return ((L_8)->GetAt(static_cast<il2cpp_array_size_t>(L_10)));
	}

IL_003e:
	{
		ImmutableList_1_t258373478 * L_11 = (ImmutableList_1_t258373478 *)__this->get__observers_0();
		Il2CppObject* L_12 = ___observer0;
		NullCheck((ImmutableList_1_t258373478 *)L_11);
		ImmutableList_1_t258373478 * L_13 = ((  ImmutableList_1_t258373478 * (*) (ImmutableList_1_t258373478 *, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6)->method)((ImmutableList_1_t258373478 *)L_11, (Il2CppObject*)L_12, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		ListObserver_1_t1575755043 * L_14 = (ListObserver_1_t1575755043 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		((  void (*) (ListObserver_1_t1575755043 *, ImmutableList_1_t258373478 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)(L_14, (ImmutableList_1_t258373478 *)L_13, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		return L_14;
	}
}
// System.Void UniRx.InternalUtil.ListObserver`1<UnityEngine.Quaternion>::.ctor(UniRx.InternalUtil.ImmutableList`1<UniRx.IObserver`1<T>>)
extern "C"  void ListObserver_1__ctor_m4053348224_gshared (ListObserver_1_t2186333650 * __this, ImmutableList_1_t868952085 * ___observers0, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		ImmutableList_1_t868952085 * L_0 = ___observers0;
		__this->set__observers_0(L_0);
		return;
	}
}
// System.Void UniRx.InternalUtil.ListObserver`1<UnityEngine.Quaternion>::OnCompleted()
extern "C"  void ListObserver_1_OnCompleted_m2661661112_gshared (ListObserver_1_t2186333650 * __this, const MethodInfo* method)
{
	IObserver_1U5BU5D_t2928567415* V_0 = NULL;
	int32_t V_1 = 0;
	{
		ImmutableList_1_t868952085 * L_0 = (ImmutableList_1_t868952085 *)__this->get__observers_0();
		NullCheck((ImmutableList_1_t868952085 *)L_0);
		IObserver_1U5BU5D_t2928567415* L_1 = ((  IObserver_1U5BU5D_t2928567415* (*) (ImmutableList_1_t868952085 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((ImmutableList_1_t868952085 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		V_0 = (IObserver_1U5BU5D_t2928567415*)L_1;
		V_1 = (int32_t)0;
		goto IL_001f;
	}

IL_0013:
	{
		IObserver_1U5BU5D_t2928567415* L_2 = V_0;
		int32_t L_3 = V_1;
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, L_3);
		int32_t L_4 = L_3;
		NullCheck((Il2CppObject*)((L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_4))));
		InterfaceActionInvoker0::Invoke(0 /* System.Void UniRx.IObserver`1<UnityEngine.Quaternion>::OnCompleted() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), (Il2CppObject*)((L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_4))));
		int32_t L_5 = V_1;
		V_1 = (int32_t)((int32_t)((int32_t)L_5+(int32_t)1));
	}

IL_001f:
	{
		int32_t L_6 = V_1;
		IObserver_1U5BU5D_t2928567415* L_7 = V_0;
		NullCheck(L_7);
		if ((((int32_t)L_6) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_7)->max_length)))))))
		{
			goto IL_0013;
		}
	}
	{
		return;
	}
}
// System.Void UniRx.InternalUtil.ListObserver`1<UnityEngine.Quaternion>::OnError(System.Exception)
extern "C"  void ListObserver_1_OnError_m162502373_gshared (ListObserver_1_t2186333650 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method)
{
	IObserver_1U5BU5D_t2928567415* V_0 = NULL;
	int32_t V_1 = 0;
	{
		ImmutableList_1_t868952085 * L_0 = (ImmutableList_1_t868952085 *)__this->get__observers_0();
		NullCheck((ImmutableList_1_t868952085 *)L_0);
		IObserver_1U5BU5D_t2928567415* L_1 = ((  IObserver_1U5BU5D_t2928567415* (*) (ImmutableList_1_t868952085 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((ImmutableList_1_t868952085 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		V_0 = (IObserver_1U5BU5D_t2928567415*)L_1;
		V_1 = (int32_t)0;
		goto IL_0020;
	}

IL_0013:
	{
		IObserver_1U5BU5D_t2928567415* L_2 = V_0;
		int32_t L_3 = V_1;
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, L_3);
		int32_t L_4 = L_3;
		Exception_t1967233988 * L_5 = ___error0;
		NullCheck((Il2CppObject*)((L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_4))));
		InterfaceActionInvoker1< Exception_t1967233988 * >::Invoke(1 /* System.Void UniRx.IObserver`1<UnityEngine.Quaternion>::OnError(System.Exception) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), (Il2CppObject*)((L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_4))), (Exception_t1967233988 *)L_5);
		int32_t L_6 = V_1;
		V_1 = (int32_t)((int32_t)((int32_t)L_6+(int32_t)1));
	}

IL_0020:
	{
		int32_t L_7 = V_1;
		IObserver_1U5BU5D_t2928567415* L_8 = V_0;
		NullCheck(L_8);
		if ((((int32_t)L_7) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_8)->max_length)))))))
		{
			goto IL_0013;
		}
	}
	{
		return;
	}
}
// System.Void UniRx.InternalUtil.ListObserver`1<UnityEngine.Quaternion>::OnNext(T)
extern "C"  void ListObserver_1_OnNext_m3139037142_gshared (ListObserver_1_t2186333650 * __this, Quaternion_t1891715979  ___value0, const MethodInfo* method)
{
	IObserver_1U5BU5D_t2928567415* V_0 = NULL;
	int32_t V_1 = 0;
	{
		ImmutableList_1_t868952085 * L_0 = (ImmutableList_1_t868952085 *)__this->get__observers_0();
		NullCheck((ImmutableList_1_t868952085 *)L_0);
		IObserver_1U5BU5D_t2928567415* L_1 = ((  IObserver_1U5BU5D_t2928567415* (*) (ImmutableList_1_t868952085 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((ImmutableList_1_t868952085 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		V_0 = (IObserver_1U5BU5D_t2928567415*)L_1;
		V_1 = (int32_t)0;
		goto IL_0020;
	}

IL_0013:
	{
		IObserver_1U5BU5D_t2928567415* L_2 = V_0;
		int32_t L_3 = V_1;
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, L_3);
		int32_t L_4 = L_3;
		Quaternion_t1891715979  L_5 = ___value0;
		NullCheck((Il2CppObject*)((L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_4))));
		InterfaceActionInvoker1< Quaternion_t1891715979  >::Invoke(2 /* System.Void UniRx.IObserver`1<UnityEngine.Quaternion>::OnNext(T) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), (Il2CppObject*)((L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_4))), (Quaternion_t1891715979 )L_5);
		int32_t L_6 = V_1;
		V_1 = (int32_t)((int32_t)((int32_t)L_6+(int32_t)1));
	}

IL_0020:
	{
		int32_t L_7 = V_1;
		IObserver_1U5BU5D_t2928567415* L_8 = V_0;
		NullCheck(L_8);
		if ((((int32_t)L_7) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_8)->max_length)))))))
		{
			goto IL_0013;
		}
	}
	{
		return;
	}
}
// UniRx.IObserver`1<T> UniRx.InternalUtil.ListObserver`1<UnityEngine.Quaternion>::Add(UniRx.IObserver`1<T>)
extern "C"  Il2CppObject* ListObserver_1_Add_m1768324086_gshared (ListObserver_1_t2186333650 * __this, Il2CppObject* ___observer0, const MethodInfo* method)
{
	{
		ImmutableList_1_t868952085 * L_0 = (ImmutableList_1_t868952085 *)__this->get__observers_0();
		Il2CppObject* L_1 = ___observer0;
		NullCheck((ImmutableList_1_t868952085 *)L_0);
		ImmutableList_1_t868952085 * L_2 = ((  ImmutableList_1_t868952085 * (*) (ImmutableList_1_t868952085 *, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)((ImmutableList_1_t868952085 *)L_0, (Il2CppObject*)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		ListObserver_1_t2186333650 * L_3 = (ListObserver_1_t2186333650 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		((  void (*) (ListObserver_1_t2186333650 *, ImmutableList_1_t868952085 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)(L_3, (ImmutableList_1_t868952085 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		return L_3;
	}
}
// UniRx.IObserver`1<T> UniRx.InternalUtil.ListObserver`1<UnityEngine.Quaternion>::Remove(UniRx.IObserver`1<T>)
extern "C"  Il2CppObject* ListObserver_1_Remove_m2802406293_gshared (ListObserver_1_t2186333650 * __this, Il2CppObject* ___observer0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		ImmutableList_1_t868952085 * L_0 = (ImmutableList_1_t868952085 *)__this->get__observers_0();
		NullCheck((ImmutableList_1_t868952085 *)L_0);
		IObserver_1U5BU5D_t2928567415* L_1 = ((  IObserver_1U5BU5D_t2928567415* (*) (ImmutableList_1_t868952085 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((ImmutableList_1_t868952085 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		Il2CppObject* L_2 = ___observer0;
		int32_t L_3 = ((  int32_t (*) (Il2CppObject * /* static, unused */, IObserver_1U5BU5D_t2928567415*, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->method)(NULL /*static, unused*/, (IObserver_1U5BU5D_t2928567415*)L_1, (Il2CppObject*)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		V_0 = (int32_t)L_3;
		int32_t L_4 = V_0;
		if ((((int32_t)L_4) >= ((int32_t)0)))
		{
			goto IL_001b;
		}
	}
	{
		return __this;
	}

IL_001b:
	{
		ImmutableList_1_t868952085 * L_5 = (ImmutableList_1_t868952085 *)__this->get__observers_0();
		NullCheck((ImmutableList_1_t868952085 *)L_5);
		IObserver_1U5BU5D_t2928567415* L_6 = ((  IObserver_1U5BU5D_t2928567415* (*) (ImmutableList_1_t868952085 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((ImmutableList_1_t868952085 *)L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		NullCheck(L_6);
		if ((!(((uint32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_6)->max_length))))) == ((uint32_t)2))))
		{
			goto IL_003e;
		}
	}
	{
		ImmutableList_1_t868952085 * L_7 = (ImmutableList_1_t868952085 *)__this->get__observers_0();
		NullCheck((ImmutableList_1_t868952085 *)L_7);
		IObserver_1U5BU5D_t2928567415* L_8 = ((  IObserver_1U5BU5D_t2928567415* (*) (ImmutableList_1_t868952085 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((ImmutableList_1_t868952085 *)L_7, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_9 = V_0;
		NullCheck(L_8);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_8, ((int32_t)((int32_t)1-(int32_t)L_9)));
		int32_t L_10 = ((int32_t)((int32_t)1-(int32_t)L_9));
		return ((L_8)->GetAt(static_cast<il2cpp_array_size_t>(L_10)));
	}

IL_003e:
	{
		ImmutableList_1_t868952085 * L_11 = (ImmutableList_1_t868952085 *)__this->get__observers_0();
		Il2CppObject* L_12 = ___observer0;
		NullCheck((ImmutableList_1_t868952085 *)L_11);
		ImmutableList_1_t868952085 * L_13 = ((  ImmutableList_1_t868952085 * (*) (ImmutableList_1_t868952085 *, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6)->method)((ImmutableList_1_t868952085 *)L_11, (Il2CppObject*)L_12, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		ListObserver_1_t2186333650 * L_14 = (ListObserver_1_t2186333650 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		((  void (*) (ListObserver_1_t2186333650 *, ImmutableList_1_t868952085 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)(L_14, (ImmutableList_1_t868952085 *)L_13, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		return L_14;
	}
}
// System.Void UniRx.InternalUtil.ListObserver`1<UnityEngine.Rect>::.ctor(UniRx.InternalUtil.ImmutableList`1<UniRx.IObserver`1<T>>)
extern "C"  void ListObserver_1__ctor_m4048574726_gshared (ListObserver_1_t1820046488 * __this, ImmutableList_1_t502664923 * ___observers0, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		ImmutableList_1_t502664923 * L_0 = ___observers0;
		__this->set__observers_0(L_0);
		return;
	}
}
// System.Void UniRx.InternalUtil.ListObserver`1<UnityEngine.Rect>::OnCompleted()
extern "C"  void ListObserver_1_OnCompleted_m162918590_gshared (ListObserver_1_t1820046488 * __this, const MethodInfo* method)
{
	IObserver_1U5BU5D_t749149145* V_0 = NULL;
	int32_t V_1 = 0;
	{
		ImmutableList_1_t502664923 * L_0 = (ImmutableList_1_t502664923 *)__this->get__observers_0();
		NullCheck((ImmutableList_1_t502664923 *)L_0);
		IObserver_1U5BU5D_t749149145* L_1 = ((  IObserver_1U5BU5D_t749149145* (*) (ImmutableList_1_t502664923 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((ImmutableList_1_t502664923 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		V_0 = (IObserver_1U5BU5D_t749149145*)L_1;
		V_1 = (int32_t)0;
		goto IL_001f;
	}

IL_0013:
	{
		IObserver_1U5BU5D_t749149145* L_2 = V_0;
		int32_t L_3 = V_1;
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, L_3);
		int32_t L_4 = L_3;
		NullCheck((Il2CppObject*)((L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_4))));
		InterfaceActionInvoker0::Invoke(0 /* System.Void UniRx.IObserver`1<UnityEngine.Rect>::OnCompleted() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), (Il2CppObject*)((L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_4))));
		int32_t L_5 = V_1;
		V_1 = (int32_t)((int32_t)((int32_t)L_5+(int32_t)1));
	}

IL_001f:
	{
		int32_t L_6 = V_1;
		IObserver_1U5BU5D_t749149145* L_7 = V_0;
		NullCheck(L_7);
		if ((((int32_t)L_6) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_7)->max_length)))))))
		{
			goto IL_0013;
		}
	}
	{
		return;
	}
}
// System.Void UniRx.InternalUtil.ListObserver`1<UnityEngine.Rect>::OnError(System.Exception)
extern "C"  void ListObserver_1_OnError_m439842027_gshared (ListObserver_1_t1820046488 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method)
{
	IObserver_1U5BU5D_t749149145* V_0 = NULL;
	int32_t V_1 = 0;
	{
		ImmutableList_1_t502664923 * L_0 = (ImmutableList_1_t502664923 *)__this->get__observers_0();
		NullCheck((ImmutableList_1_t502664923 *)L_0);
		IObserver_1U5BU5D_t749149145* L_1 = ((  IObserver_1U5BU5D_t749149145* (*) (ImmutableList_1_t502664923 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((ImmutableList_1_t502664923 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		V_0 = (IObserver_1U5BU5D_t749149145*)L_1;
		V_1 = (int32_t)0;
		goto IL_0020;
	}

IL_0013:
	{
		IObserver_1U5BU5D_t749149145* L_2 = V_0;
		int32_t L_3 = V_1;
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, L_3);
		int32_t L_4 = L_3;
		Exception_t1967233988 * L_5 = ___error0;
		NullCheck((Il2CppObject*)((L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_4))));
		InterfaceActionInvoker1< Exception_t1967233988 * >::Invoke(1 /* System.Void UniRx.IObserver`1<UnityEngine.Rect>::OnError(System.Exception) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), (Il2CppObject*)((L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_4))), (Exception_t1967233988 *)L_5);
		int32_t L_6 = V_1;
		V_1 = (int32_t)((int32_t)((int32_t)L_6+(int32_t)1));
	}

IL_0020:
	{
		int32_t L_7 = V_1;
		IObserver_1U5BU5D_t749149145* L_8 = V_0;
		NullCheck(L_8);
		if ((((int32_t)L_7) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_8)->max_length)))))))
		{
			goto IL_0013;
		}
	}
	{
		return;
	}
}
// System.Void UniRx.InternalUtil.ListObserver`1<UnityEngine.Rect>::OnNext(T)
extern "C"  void ListObserver_1_OnNext_m1513787868_gshared (ListObserver_1_t1820046488 * __this, Rect_t1525428817  ___value0, const MethodInfo* method)
{
	IObserver_1U5BU5D_t749149145* V_0 = NULL;
	int32_t V_1 = 0;
	{
		ImmutableList_1_t502664923 * L_0 = (ImmutableList_1_t502664923 *)__this->get__observers_0();
		NullCheck((ImmutableList_1_t502664923 *)L_0);
		IObserver_1U5BU5D_t749149145* L_1 = ((  IObserver_1U5BU5D_t749149145* (*) (ImmutableList_1_t502664923 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((ImmutableList_1_t502664923 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		V_0 = (IObserver_1U5BU5D_t749149145*)L_1;
		V_1 = (int32_t)0;
		goto IL_0020;
	}

IL_0013:
	{
		IObserver_1U5BU5D_t749149145* L_2 = V_0;
		int32_t L_3 = V_1;
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, L_3);
		int32_t L_4 = L_3;
		Rect_t1525428817  L_5 = ___value0;
		NullCheck((Il2CppObject*)((L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_4))));
		InterfaceActionInvoker1< Rect_t1525428817  >::Invoke(2 /* System.Void UniRx.IObserver`1<UnityEngine.Rect>::OnNext(T) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), (Il2CppObject*)((L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_4))), (Rect_t1525428817 )L_5);
		int32_t L_6 = V_1;
		V_1 = (int32_t)((int32_t)((int32_t)L_6+(int32_t)1));
	}

IL_0020:
	{
		int32_t L_7 = V_1;
		IObserver_1U5BU5D_t749149145* L_8 = V_0;
		NullCheck(L_8);
		if ((((int32_t)L_7) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_8)->max_length)))))))
		{
			goto IL_0013;
		}
	}
	{
		return;
	}
}
// UniRx.IObserver`1<T> UniRx.InternalUtil.ListObserver`1<UnityEngine.Rect>::Add(UniRx.IObserver`1<T>)
extern "C"  Il2CppObject* ListObserver_1_Add_m3816105788_gshared (ListObserver_1_t1820046488 * __this, Il2CppObject* ___observer0, const MethodInfo* method)
{
	{
		ImmutableList_1_t502664923 * L_0 = (ImmutableList_1_t502664923 *)__this->get__observers_0();
		Il2CppObject* L_1 = ___observer0;
		NullCheck((ImmutableList_1_t502664923 *)L_0);
		ImmutableList_1_t502664923 * L_2 = ((  ImmutableList_1_t502664923 * (*) (ImmutableList_1_t502664923 *, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)((ImmutableList_1_t502664923 *)L_0, (Il2CppObject*)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		ListObserver_1_t1820046488 * L_3 = (ListObserver_1_t1820046488 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		((  void (*) (ListObserver_1_t1820046488 *, ImmutableList_1_t502664923 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)(L_3, (ImmutableList_1_t502664923 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		return L_3;
	}
}
// UniRx.IObserver`1<T> UniRx.InternalUtil.ListObserver`1<UnityEngine.Rect>::Remove(UniRx.IObserver`1<T>)
extern "C"  Il2CppObject* ListObserver_1_Remove_m2551618191_gshared (ListObserver_1_t1820046488 * __this, Il2CppObject* ___observer0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		ImmutableList_1_t502664923 * L_0 = (ImmutableList_1_t502664923 *)__this->get__observers_0();
		NullCheck((ImmutableList_1_t502664923 *)L_0);
		IObserver_1U5BU5D_t749149145* L_1 = ((  IObserver_1U5BU5D_t749149145* (*) (ImmutableList_1_t502664923 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((ImmutableList_1_t502664923 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		Il2CppObject* L_2 = ___observer0;
		int32_t L_3 = ((  int32_t (*) (Il2CppObject * /* static, unused */, IObserver_1U5BU5D_t749149145*, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->method)(NULL /*static, unused*/, (IObserver_1U5BU5D_t749149145*)L_1, (Il2CppObject*)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		V_0 = (int32_t)L_3;
		int32_t L_4 = V_0;
		if ((((int32_t)L_4) >= ((int32_t)0)))
		{
			goto IL_001b;
		}
	}
	{
		return __this;
	}

IL_001b:
	{
		ImmutableList_1_t502664923 * L_5 = (ImmutableList_1_t502664923 *)__this->get__observers_0();
		NullCheck((ImmutableList_1_t502664923 *)L_5);
		IObserver_1U5BU5D_t749149145* L_6 = ((  IObserver_1U5BU5D_t749149145* (*) (ImmutableList_1_t502664923 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((ImmutableList_1_t502664923 *)L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		NullCheck(L_6);
		if ((!(((uint32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_6)->max_length))))) == ((uint32_t)2))))
		{
			goto IL_003e;
		}
	}
	{
		ImmutableList_1_t502664923 * L_7 = (ImmutableList_1_t502664923 *)__this->get__observers_0();
		NullCheck((ImmutableList_1_t502664923 *)L_7);
		IObserver_1U5BU5D_t749149145* L_8 = ((  IObserver_1U5BU5D_t749149145* (*) (ImmutableList_1_t502664923 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((ImmutableList_1_t502664923 *)L_7, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_9 = V_0;
		NullCheck(L_8);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_8, ((int32_t)((int32_t)1-(int32_t)L_9)));
		int32_t L_10 = ((int32_t)((int32_t)1-(int32_t)L_9));
		return ((L_8)->GetAt(static_cast<il2cpp_array_size_t>(L_10)));
	}

IL_003e:
	{
		ImmutableList_1_t502664923 * L_11 = (ImmutableList_1_t502664923 *)__this->get__observers_0();
		Il2CppObject* L_12 = ___observer0;
		NullCheck((ImmutableList_1_t502664923 *)L_11);
		ImmutableList_1_t502664923 * L_13 = ((  ImmutableList_1_t502664923 * (*) (ImmutableList_1_t502664923 *, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6)->method)((ImmutableList_1_t502664923 *)L_11, (Il2CppObject*)L_12, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		ListObserver_1_t1820046488 * L_14 = (ListObserver_1_t1820046488 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		((  void (*) (ListObserver_1_t1820046488 *, ImmutableList_1_t502664923 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)(L_14, (ImmutableList_1_t502664923 *)L_13, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		return L_14;
	}
}
// System.Void UniRx.InternalUtil.ListObserver`1<UnityEngine.Vector2>::.ctor(UniRx.InternalUtil.ImmutableList`1<UniRx.IObserver`1<T>>)
extern "C"  void ListObserver_1__ctor_m2231967547_gshared (ListObserver_1_t3819947459 * __this, ImmutableList_1_t2502565894 * ___observers0, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		ImmutableList_1_t2502565894 * L_0 = ___observers0;
		__this->set__observers_0(L_0);
		return;
	}
}
// System.Void UniRx.InternalUtil.ListObserver`1<UnityEngine.Vector2>::OnCompleted()
extern "C"  void ListObserver_1_OnCompleted_m1131372083_gshared (ListObserver_1_t3819947459 * __this, const MethodInfo* method)
{
	IObserver_1U5BU5D_t2433548706* V_0 = NULL;
	int32_t V_1 = 0;
	{
		ImmutableList_1_t2502565894 * L_0 = (ImmutableList_1_t2502565894 *)__this->get__observers_0();
		NullCheck((ImmutableList_1_t2502565894 *)L_0);
		IObserver_1U5BU5D_t2433548706* L_1 = ((  IObserver_1U5BU5D_t2433548706* (*) (ImmutableList_1_t2502565894 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((ImmutableList_1_t2502565894 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		V_0 = (IObserver_1U5BU5D_t2433548706*)L_1;
		V_1 = (int32_t)0;
		goto IL_001f;
	}

IL_0013:
	{
		IObserver_1U5BU5D_t2433548706* L_2 = V_0;
		int32_t L_3 = V_1;
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, L_3);
		int32_t L_4 = L_3;
		NullCheck((Il2CppObject*)((L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_4))));
		InterfaceActionInvoker0::Invoke(0 /* System.Void UniRx.IObserver`1<UnityEngine.Vector2>::OnCompleted() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), (Il2CppObject*)((L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_4))));
		int32_t L_5 = V_1;
		V_1 = (int32_t)((int32_t)((int32_t)L_5+(int32_t)1));
	}

IL_001f:
	{
		int32_t L_6 = V_1;
		IObserver_1U5BU5D_t2433548706* L_7 = V_0;
		NullCheck(L_7);
		if ((((int32_t)L_6) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_7)->max_length)))))))
		{
			goto IL_0013;
		}
	}
	{
		return;
	}
}
// System.Void UniRx.InternalUtil.ListObserver`1<UnityEngine.Vector2>::OnError(System.Exception)
extern "C"  void ListObserver_1_OnError_m1377262304_gshared (ListObserver_1_t3819947459 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method)
{
	IObserver_1U5BU5D_t2433548706* V_0 = NULL;
	int32_t V_1 = 0;
	{
		ImmutableList_1_t2502565894 * L_0 = (ImmutableList_1_t2502565894 *)__this->get__observers_0();
		NullCheck((ImmutableList_1_t2502565894 *)L_0);
		IObserver_1U5BU5D_t2433548706* L_1 = ((  IObserver_1U5BU5D_t2433548706* (*) (ImmutableList_1_t2502565894 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((ImmutableList_1_t2502565894 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		V_0 = (IObserver_1U5BU5D_t2433548706*)L_1;
		V_1 = (int32_t)0;
		goto IL_0020;
	}

IL_0013:
	{
		IObserver_1U5BU5D_t2433548706* L_2 = V_0;
		int32_t L_3 = V_1;
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, L_3);
		int32_t L_4 = L_3;
		Exception_t1967233988 * L_5 = ___error0;
		NullCheck((Il2CppObject*)((L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_4))));
		InterfaceActionInvoker1< Exception_t1967233988 * >::Invoke(1 /* System.Void UniRx.IObserver`1<UnityEngine.Vector2>::OnError(System.Exception) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), (Il2CppObject*)((L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_4))), (Exception_t1967233988 *)L_5);
		int32_t L_6 = V_1;
		V_1 = (int32_t)((int32_t)((int32_t)L_6+(int32_t)1));
	}

IL_0020:
	{
		int32_t L_7 = V_1;
		IObserver_1U5BU5D_t2433548706* L_8 = V_0;
		NullCheck(L_8);
		if ((((int32_t)L_7) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_8)->max_length)))))))
		{
			goto IL_0013;
		}
	}
	{
		return;
	}
}
// System.Void UniRx.InternalUtil.ListObserver`1<UnityEngine.Vector2>::OnNext(T)
extern "C"  void ListObserver_1_OnNext_m1346830801_gshared (ListObserver_1_t3819947459 * __this, Vector2_t3525329788  ___value0, const MethodInfo* method)
{
	IObserver_1U5BU5D_t2433548706* V_0 = NULL;
	int32_t V_1 = 0;
	{
		ImmutableList_1_t2502565894 * L_0 = (ImmutableList_1_t2502565894 *)__this->get__observers_0();
		NullCheck((ImmutableList_1_t2502565894 *)L_0);
		IObserver_1U5BU5D_t2433548706* L_1 = ((  IObserver_1U5BU5D_t2433548706* (*) (ImmutableList_1_t2502565894 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((ImmutableList_1_t2502565894 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		V_0 = (IObserver_1U5BU5D_t2433548706*)L_1;
		V_1 = (int32_t)0;
		goto IL_0020;
	}

IL_0013:
	{
		IObserver_1U5BU5D_t2433548706* L_2 = V_0;
		int32_t L_3 = V_1;
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, L_3);
		int32_t L_4 = L_3;
		Vector2_t3525329788  L_5 = ___value0;
		NullCheck((Il2CppObject*)((L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_4))));
		InterfaceActionInvoker1< Vector2_t3525329788  >::Invoke(2 /* System.Void UniRx.IObserver`1<UnityEngine.Vector2>::OnNext(T) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), (Il2CppObject*)((L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_4))), (Vector2_t3525329788 )L_5);
		int32_t L_6 = V_1;
		V_1 = (int32_t)((int32_t)((int32_t)L_6+(int32_t)1));
	}

IL_0020:
	{
		int32_t L_7 = V_1;
		IObserver_1U5BU5D_t2433548706* L_8 = V_0;
		NullCheck(L_8);
		if ((((int32_t)L_7) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_8)->max_length)))))))
		{
			goto IL_0013;
		}
	}
	{
		return;
	}
}
// UniRx.IObserver`1<T> UniRx.InternalUtil.ListObserver`1<UnityEngine.Vector2>::Add(UniRx.IObserver`1<T>)
extern "C"  Il2CppObject* ListObserver_1_Add_m2098521763_gshared (ListObserver_1_t3819947459 * __this, Il2CppObject* ___observer0, const MethodInfo* method)
{
	{
		ImmutableList_1_t2502565894 * L_0 = (ImmutableList_1_t2502565894 *)__this->get__observers_0();
		Il2CppObject* L_1 = ___observer0;
		NullCheck((ImmutableList_1_t2502565894 *)L_0);
		ImmutableList_1_t2502565894 * L_2 = ((  ImmutableList_1_t2502565894 * (*) (ImmutableList_1_t2502565894 *, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)((ImmutableList_1_t2502565894 *)L_0, (Il2CppObject*)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		ListObserver_1_t3819947459 * L_3 = (ListObserver_1_t3819947459 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		((  void (*) (ListObserver_1_t3819947459 *, ImmutableList_1_t2502565894 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)(L_3, (ImmutableList_1_t2502565894 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		return L_3;
	}
}
// UniRx.IObserver`1<T> UniRx.InternalUtil.ListObserver`1<UnityEngine.Vector2>::Remove(UniRx.IObserver`1<T>)
extern "C"  Il2CppObject* ListObserver_1_Remove_m4246293960_gshared (ListObserver_1_t3819947459 * __this, Il2CppObject* ___observer0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		ImmutableList_1_t2502565894 * L_0 = (ImmutableList_1_t2502565894 *)__this->get__observers_0();
		NullCheck((ImmutableList_1_t2502565894 *)L_0);
		IObserver_1U5BU5D_t2433548706* L_1 = ((  IObserver_1U5BU5D_t2433548706* (*) (ImmutableList_1_t2502565894 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((ImmutableList_1_t2502565894 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		Il2CppObject* L_2 = ___observer0;
		int32_t L_3 = ((  int32_t (*) (Il2CppObject * /* static, unused */, IObserver_1U5BU5D_t2433548706*, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->method)(NULL /*static, unused*/, (IObserver_1U5BU5D_t2433548706*)L_1, (Il2CppObject*)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		V_0 = (int32_t)L_3;
		int32_t L_4 = V_0;
		if ((((int32_t)L_4) >= ((int32_t)0)))
		{
			goto IL_001b;
		}
	}
	{
		return __this;
	}

IL_001b:
	{
		ImmutableList_1_t2502565894 * L_5 = (ImmutableList_1_t2502565894 *)__this->get__observers_0();
		NullCheck((ImmutableList_1_t2502565894 *)L_5);
		IObserver_1U5BU5D_t2433548706* L_6 = ((  IObserver_1U5BU5D_t2433548706* (*) (ImmutableList_1_t2502565894 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((ImmutableList_1_t2502565894 *)L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		NullCheck(L_6);
		if ((!(((uint32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_6)->max_length))))) == ((uint32_t)2))))
		{
			goto IL_003e;
		}
	}
	{
		ImmutableList_1_t2502565894 * L_7 = (ImmutableList_1_t2502565894 *)__this->get__observers_0();
		NullCheck((ImmutableList_1_t2502565894 *)L_7);
		IObserver_1U5BU5D_t2433548706* L_8 = ((  IObserver_1U5BU5D_t2433548706* (*) (ImmutableList_1_t2502565894 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((ImmutableList_1_t2502565894 *)L_7, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_9 = V_0;
		NullCheck(L_8);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_8, ((int32_t)((int32_t)1-(int32_t)L_9)));
		int32_t L_10 = ((int32_t)((int32_t)1-(int32_t)L_9));
		return ((L_8)->GetAt(static_cast<il2cpp_array_size_t>(L_10)));
	}

IL_003e:
	{
		ImmutableList_1_t2502565894 * L_11 = (ImmutableList_1_t2502565894 *)__this->get__observers_0();
		Il2CppObject* L_12 = ___observer0;
		NullCheck((ImmutableList_1_t2502565894 *)L_11);
		ImmutableList_1_t2502565894 * L_13 = ((  ImmutableList_1_t2502565894 * (*) (ImmutableList_1_t2502565894 *, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6)->method)((ImmutableList_1_t2502565894 *)L_11, (Il2CppObject*)L_12, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		ListObserver_1_t3819947459 * L_14 = (ListObserver_1_t3819947459 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		((  void (*) (ListObserver_1_t3819947459 *, ImmutableList_1_t2502565894 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)(L_14, (ImmutableList_1_t2502565894 *)L_13, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		return L_14;
	}
}
// System.Void UniRx.InternalUtil.ListObserver`1<UnityEngine.Vector3>::.ctor(UniRx.InternalUtil.ImmutableList`1<UniRx.IObserver`1<T>>)
extern "C"  void ListObserver_1__ctor_m1623890684_gshared (ListObserver_1_t3819947460 * __this, ImmutableList_1_t2502565895 * ___observers0, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		ImmutableList_1_t2502565895 * L_0 = ___observers0;
		__this->set__observers_0(L_0);
		return;
	}
}
// System.Void UniRx.InternalUtil.ListObserver`1<UnityEngine.Vector3>::OnCompleted()
extern "C"  void ListObserver_1_OnCompleted_m2484681780_gshared (ListObserver_1_t3819947460 * __this, const MethodInfo* method)
{
	IObserver_1U5BU5D_t2919736445* V_0 = NULL;
	int32_t V_1 = 0;
	{
		ImmutableList_1_t2502565895 * L_0 = (ImmutableList_1_t2502565895 *)__this->get__observers_0();
		NullCheck((ImmutableList_1_t2502565895 *)L_0);
		IObserver_1U5BU5D_t2919736445* L_1 = ((  IObserver_1U5BU5D_t2919736445* (*) (ImmutableList_1_t2502565895 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((ImmutableList_1_t2502565895 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		V_0 = (IObserver_1U5BU5D_t2919736445*)L_1;
		V_1 = (int32_t)0;
		goto IL_001f;
	}

IL_0013:
	{
		IObserver_1U5BU5D_t2919736445* L_2 = V_0;
		int32_t L_3 = V_1;
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, L_3);
		int32_t L_4 = L_3;
		NullCheck((Il2CppObject*)((L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_4))));
		InterfaceActionInvoker0::Invoke(0 /* System.Void UniRx.IObserver`1<UnityEngine.Vector3>::OnCompleted() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), (Il2CppObject*)((L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_4))));
		int32_t L_5 = V_1;
		V_1 = (int32_t)((int32_t)((int32_t)L_5+(int32_t)1));
	}

IL_001f:
	{
		int32_t L_6 = V_1;
		IObserver_1U5BU5D_t2919736445* L_7 = V_0;
		NullCheck(L_7);
		if ((((int32_t)L_6) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_7)->max_length)))))))
		{
			goto IL_0013;
		}
	}
	{
		return;
	}
}
// System.Void UniRx.InternalUtil.ListObserver`1<UnityEngine.Vector3>::OnError(System.Exception)
extern "C"  void ListObserver_1_OnError_m2070364001_gshared (ListObserver_1_t3819947460 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method)
{
	IObserver_1U5BU5D_t2919736445* V_0 = NULL;
	int32_t V_1 = 0;
	{
		ImmutableList_1_t2502565895 * L_0 = (ImmutableList_1_t2502565895 *)__this->get__observers_0();
		NullCheck((ImmutableList_1_t2502565895 *)L_0);
		IObserver_1U5BU5D_t2919736445* L_1 = ((  IObserver_1U5BU5D_t2919736445* (*) (ImmutableList_1_t2502565895 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((ImmutableList_1_t2502565895 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		V_0 = (IObserver_1U5BU5D_t2919736445*)L_1;
		V_1 = (int32_t)0;
		goto IL_0020;
	}

IL_0013:
	{
		IObserver_1U5BU5D_t2919736445* L_2 = V_0;
		int32_t L_3 = V_1;
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, L_3);
		int32_t L_4 = L_3;
		Exception_t1967233988 * L_5 = ___error0;
		NullCheck((Il2CppObject*)((L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_4))));
		InterfaceActionInvoker1< Exception_t1967233988 * >::Invoke(1 /* System.Void UniRx.IObserver`1<UnityEngine.Vector3>::OnError(System.Exception) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), (Il2CppObject*)((L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_4))), (Exception_t1967233988 *)L_5);
		int32_t L_6 = V_1;
		V_1 = (int32_t)((int32_t)((int32_t)L_6+(int32_t)1));
	}

IL_0020:
	{
		int32_t L_7 = V_1;
		IObserver_1U5BU5D_t2919736445* L_8 = V_0;
		NullCheck(L_8);
		if ((((int32_t)L_7) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_8)->max_length)))))))
		{
			goto IL_0013;
		}
	}
	{
		return;
	}
}
// System.Void UniRx.InternalUtil.ListObserver`1<UnityEngine.Vector3>::OnNext(T)
extern "C"  void ListObserver_1_OnNext_m1053427794_gshared (ListObserver_1_t3819947460 * __this, Vector3_t3525329789  ___value0, const MethodInfo* method)
{
	IObserver_1U5BU5D_t2919736445* V_0 = NULL;
	int32_t V_1 = 0;
	{
		ImmutableList_1_t2502565895 * L_0 = (ImmutableList_1_t2502565895 *)__this->get__observers_0();
		NullCheck((ImmutableList_1_t2502565895 *)L_0);
		IObserver_1U5BU5D_t2919736445* L_1 = ((  IObserver_1U5BU5D_t2919736445* (*) (ImmutableList_1_t2502565895 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((ImmutableList_1_t2502565895 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		V_0 = (IObserver_1U5BU5D_t2919736445*)L_1;
		V_1 = (int32_t)0;
		goto IL_0020;
	}

IL_0013:
	{
		IObserver_1U5BU5D_t2919736445* L_2 = V_0;
		int32_t L_3 = V_1;
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, L_3);
		int32_t L_4 = L_3;
		Vector3_t3525329789  L_5 = ___value0;
		NullCheck((Il2CppObject*)((L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_4))));
		InterfaceActionInvoker1< Vector3_t3525329789  >::Invoke(2 /* System.Void UniRx.IObserver`1<UnityEngine.Vector3>::OnNext(T) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), (Il2CppObject*)((L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_4))), (Vector3_t3525329789 )L_5);
		int32_t L_6 = V_1;
		V_1 = (int32_t)((int32_t)((int32_t)L_6+(int32_t)1));
	}

IL_0020:
	{
		int32_t L_7 = V_1;
		IObserver_1U5BU5D_t2919736445* L_8 = V_0;
		NullCheck(L_8);
		if ((((int32_t)L_7) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_8)->max_length)))))))
		{
			goto IL_0013;
		}
	}
	{
		return;
	}
}
// UniRx.IObserver`1<T> UniRx.InternalUtil.ListObserver`1<UnityEngine.Vector3>::Add(UniRx.IObserver`1<T>)
extern "C"  Il2CppObject* ListObserver_1_Add_m2791623460_gshared (ListObserver_1_t3819947460 * __this, Il2CppObject* ___observer0, const MethodInfo* method)
{
	{
		ImmutableList_1_t2502565895 * L_0 = (ImmutableList_1_t2502565895 *)__this->get__observers_0();
		Il2CppObject* L_1 = ___observer0;
		NullCheck((ImmutableList_1_t2502565895 *)L_0);
		ImmutableList_1_t2502565895 * L_2 = ((  ImmutableList_1_t2502565895 * (*) (ImmutableList_1_t2502565895 *, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)((ImmutableList_1_t2502565895 *)L_0, (Il2CppObject*)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		ListObserver_1_t3819947460 * L_3 = (ListObserver_1_t3819947460 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		((  void (*) (ListObserver_1_t3819947460 *, ImmutableList_1_t2502565895 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)(L_3, (ImmutableList_1_t2502565895 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		return L_3;
	}
}
// UniRx.IObserver`1<T> UniRx.InternalUtil.ListObserver`1<UnityEngine.Vector3>::Remove(UniRx.IObserver`1<T>)
extern "C"  Il2CppObject* ListObserver_1_Remove_m2236190119_gshared (ListObserver_1_t3819947460 * __this, Il2CppObject* ___observer0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		ImmutableList_1_t2502565895 * L_0 = (ImmutableList_1_t2502565895 *)__this->get__observers_0();
		NullCheck((ImmutableList_1_t2502565895 *)L_0);
		IObserver_1U5BU5D_t2919736445* L_1 = ((  IObserver_1U5BU5D_t2919736445* (*) (ImmutableList_1_t2502565895 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((ImmutableList_1_t2502565895 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		Il2CppObject* L_2 = ___observer0;
		int32_t L_3 = ((  int32_t (*) (Il2CppObject * /* static, unused */, IObserver_1U5BU5D_t2919736445*, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->method)(NULL /*static, unused*/, (IObserver_1U5BU5D_t2919736445*)L_1, (Il2CppObject*)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		V_0 = (int32_t)L_3;
		int32_t L_4 = V_0;
		if ((((int32_t)L_4) >= ((int32_t)0)))
		{
			goto IL_001b;
		}
	}
	{
		return __this;
	}

IL_001b:
	{
		ImmutableList_1_t2502565895 * L_5 = (ImmutableList_1_t2502565895 *)__this->get__observers_0();
		NullCheck((ImmutableList_1_t2502565895 *)L_5);
		IObserver_1U5BU5D_t2919736445* L_6 = ((  IObserver_1U5BU5D_t2919736445* (*) (ImmutableList_1_t2502565895 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((ImmutableList_1_t2502565895 *)L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		NullCheck(L_6);
		if ((!(((uint32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_6)->max_length))))) == ((uint32_t)2))))
		{
			goto IL_003e;
		}
	}
	{
		ImmutableList_1_t2502565895 * L_7 = (ImmutableList_1_t2502565895 *)__this->get__observers_0();
		NullCheck((ImmutableList_1_t2502565895 *)L_7);
		IObserver_1U5BU5D_t2919736445* L_8 = ((  IObserver_1U5BU5D_t2919736445* (*) (ImmutableList_1_t2502565895 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((ImmutableList_1_t2502565895 *)L_7, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_9 = V_0;
		NullCheck(L_8);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_8, ((int32_t)((int32_t)1-(int32_t)L_9)));
		int32_t L_10 = ((int32_t)((int32_t)1-(int32_t)L_9));
		return ((L_8)->GetAt(static_cast<il2cpp_array_size_t>(L_10)));
	}

IL_003e:
	{
		ImmutableList_1_t2502565895 * L_11 = (ImmutableList_1_t2502565895 *)__this->get__observers_0();
		Il2CppObject* L_12 = ___observer0;
		NullCheck((ImmutableList_1_t2502565895 *)L_11);
		ImmutableList_1_t2502565895 * L_13 = ((  ImmutableList_1_t2502565895 * (*) (ImmutableList_1_t2502565895 *, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6)->method)((ImmutableList_1_t2502565895 *)L_11, (Il2CppObject*)L_12, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		ListObserver_1_t3819947460 * L_14 = (ListObserver_1_t3819947460 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		((  void (*) (ListObserver_1_t3819947460 *, ImmutableList_1_t2502565895 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)(L_14, (ImmutableList_1_t2502565895 *)L_13, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		return L_14;
	}
}
// System.Void UniRx.InternalUtil.ListObserver`1<UnityEngine.Vector4>::.ctor(UniRx.InternalUtil.ImmutableList`1<UniRx.IObserver`1<T>>)
extern "C"  void ListObserver_1__ctor_m1015813821_gshared (ListObserver_1_t3819947461 * __this, ImmutableList_1_t2502565896 * ___observers0, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		ImmutableList_1_t2502565896 * L_0 = ___observers0;
		__this->set__observers_0(L_0);
		return;
	}
}
// System.Void UniRx.InternalUtil.ListObserver`1<UnityEngine.Vector4>::OnCompleted()
extern "C"  void ListObserver_1_OnCompleted_m3837991477_gshared (ListObserver_1_t3819947461 * __this, const MethodInfo* method)
{
	IObserver_1U5BU5D_t3405924184* V_0 = NULL;
	int32_t V_1 = 0;
	{
		ImmutableList_1_t2502565896 * L_0 = (ImmutableList_1_t2502565896 *)__this->get__observers_0();
		NullCheck((ImmutableList_1_t2502565896 *)L_0);
		IObserver_1U5BU5D_t3405924184* L_1 = ((  IObserver_1U5BU5D_t3405924184* (*) (ImmutableList_1_t2502565896 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((ImmutableList_1_t2502565896 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		V_0 = (IObserver_1U5BU5D_t3405924184*)L_1;
		V_1 = (int32_t)0;
		goto IL_001f;
	}

IL_0013:
	{
		IObserver_1U5BU5D_t3405924184* L_2 = V_0;
		int32_t L_3 = V_1;
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, L_3);
		int32_t L_4 = L_3;
		NullCheck((Il2CppObject*)((L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_4))));
		InterfaceActionInvoker0::Invoke(0 /* System.Void UniRx.IObserver`1<UnityEngine.Vector4>::OnCompleted() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), (Il2CppObject*)((L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_4))));
		int32_t L_5 = V_1;
		V_1 = (int32_t)((int32_t)((int32_t)L_5+(int32_t)1));
	}

IL_001f:
	{
		int32_t L_6 = V_1;
		IObserver_1U5BU5D_t3405924184* L_7 = V_0;
		NullCheck(L_7);
		if ((((int32_t)L_6) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_7)->max_length)))))))
		{
			goto IL_0013;
		}
	}
	{
		return;
	}
}
// System.Void UniRx.InternalUtil.ListObserver`1<UnityEngine.Vector4>::OnError(System.Exception)
extern "C"  void ListObserver_1_OnError_m2763465698_gshared (ListObserver_1_t3819947461 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method)
{
	IObserver_1U5BU5D_t3405924184* V_0 = NULL;
	int32_t V_1 = 0;
	{
		ImmutableList_1_t2502565896 * L_0 = (ImmutableList_1_t2502565896 *)__this->get__observers_0();
		NullCheck((ImmutableList_1_t2502565896 *)L_0);
		IObserver_1U5BU5D_t3405924184* L_1 = ((  IObserver_1U5BU5D_t3405924184* (*) (ImmutableList_1_t2502565896 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((ImmutableList_1_t2502565896 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		V_0 = (IObserver_1U5BU5D_t3405924184*)L_1;
		V_1 = (int32_t)0;
		goto IL_0020;
	}

IL_0013:
	{
		IObserver_1U5BU5D_t3405924184* L_2 = V_0;
		int32_t L_3 = V_1;
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, L_3);
		int32_t L_4 = L_3;
		Exception_t1967233988 * L_5 = ___error0;
		NullCheck((Il2CppObject*)((L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_4))));
		InterfaceActionInvoker1< Exception_t1967233988 * >::Invoke(1 /* System.Void UniRx.IObserver`1<UnityEngine.Vector4>::OnError(System.Exception) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), (Il2CppObject*)((L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_4))), (Exception_t1967233988 *)L_5);
		int32_t L_6 = V_1;
		V_1 = (int32_t)((int32_t)((int32_t)L_6+(int32_t)1));
	}

IL_0020:
	{
		int32_t L_7 = V_1;
		IObserver_1U5BU5D_t3405924184* L_8 = V_0;
		NullCheck(L_8);
		if ((((int32_t)L_7) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_8)->max_length)))))))
		{
			goto IL_0013;
		}
	}
	{
		return;
	}
}
// System.Void UniRx.InternalUtil.ListObserver`1<UnityEngine.Vector4>::OnNext(T)
extern "C"  void ListObserver_1_OnNext_m760024787_gshared (ListObserver_1_t3819947461 * __this, Vector4_t3525329790  ___value0, const MethodInfo* method)
{
	IObserver_1U5BU5D_t3405924184* V_0 = NULL;
	int32_t V_1 = 0;
	{
		ImmutableList_1_t2502565896 * L_0 = (ImmutableList_1_t2502565896 *)__this->get__observers_0();
		NullCheck((ImmutableList_1_t2502565896 *)L_0);
		IObserver_1U5BU5D_t3405924184* L_1 = ((  IObserver_1U5BU5D_t3405924184* (*) (ImmutableList_1_t2502565896 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((ImmutableList_1_t2502565896 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		V_0 = (IObserver_1U5BU5D_t3405924184*)L_1;
		V_1 = (int32_t)0;
		goto IL_0020;
	}

IL_0013:
	{
		IObserver_1U5BU5D_t3405924184* L_2 = V_0;
		int32_t L_3 = V_1;
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, L_3);
		int32_t L_4 = L_3;
		Vector4_t3525329790  L_5 = ___value0;
		NullCheck((Il2CppObject*)((L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_4))));
		InterfaceActionInvoker1< Vector4_t3525329790  >::Invoke(2 /* System.Void UniRx.IObserver`1<UnityEngine.Vector4>::OnNext(T) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), (Il2CppObject*)((L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_4))), (Vector4_t3525329790 )L_5);
		int32_t L_6 = V_1;
		V_1 = (int32_t)((int32_t)((int32_t)L_6+(int32_t)1));
	}

IL_0020:
	{
		int32_t L_7 = V_1;
		IObserver_1U5BU5D_t3405924184* L_8 = V_0;
		NullCheck(L_8);
		if ((((int32_t)L_7) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_8)->max_length)))))))
		{
			goto IL_0013;
		}
	}
	{
		return;
	}
}
// UniRx.IObserver`1<T> UniRx.InternalUtil.ListObserver`1<UnityEngine.Vector4>::Add(UniRx.IObserver`1<T>)
extern "C"  Il2CppObject* ListObserver_1_Add_m3484725157_gshared (ListObserver_1_t3819947461 * __this, Il2CppObject* ___observer0, const MethodInfo* method)
{
	{
		ImmutableList_1_t2502565896 * L_0 = (ImmutableList_1_t2502565896 *)__this->get__observers_0();
		Il2CppObject* L_1 = ___observer0;
		NullCheck((ImmutableList_1_t2502565896 *)L_0);
		ImmutableList_1_t2502565896 * L_2 = ((  ImmutableList_1_t2502565896 * (*) (ImmutableList_1_t2502565896 *, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)((ImmutableList_1_t2502565896 *)L_0, (Il2CppObject*)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		ListObserver_1_t3819947461 * L_3 = (ListObserver_1_t3819947461 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		((  void (*) (ListObserver_1_t3819947461 *, ImmutableList_1_t2502565896 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)(L_3, (ImmutableList_1_t2502565896 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		return L_3;
	}
}
// UniRx.IObserver`1<T> UniRx.InternalUtil.ListObserver`1<UnityEngine.Vector4>::Remove(UniRx.IObserver`1<T>)
extern "C"  Il2CppObject* ListObserver_1_Remove_m226086278_gshared (ListObserver_1_t3819947461 * __this, Il2CppObject* ___observer0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		ImmutableList_1_t2502565896 * L_0 = (ImmutableList_1_t2502565896 *)__this->get__observers_0();
		NullCheck((ImmutableList_1_t2502565896 *)L_0);
		IObserver_1U5BU5D_t3405924184* L_1 = ((  IObserver_1U5BU5D_t3405924184* (*) (ImmutableList_1_t2502565896 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((ImmutableList_1_t2502565896 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		Il2CppObject* L_2 = ___observer0;
		int32_t L_3 = ((  int32_t (*) (Il2CppObject * /* static, unused */, IObserver_1U5BU5D_t3405924184*, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->method)(NULL /*static, unused*/, (IObserver_1U5BU5D_t3405924184*)L_1, (Il2CppObject*)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		V_0 = (int32_t)L_3;
		int32_t L_4 = V_0;
		if ((((int32_t)L_4) >= ((int32_t)0)))
		{
			goto IL_001b;
		}
	}
	{
		return __this;
	}

IL_001b:
	{
		ImmutableList_1_t2502565896 * L_5 = (ImmutableList_1_t2502565896 *)__this->get__observers_0();
		NullCheck((ImmutableList_1_t2502565896 *)L_5);
		IObserver_1U5BU5D_t3405924184* L_6 = ((  IObserver_1U5BU5D_t3405924184* (*) (ImmutableList_1_t2502565896 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((ImmutableList_1_t2502565896 *)L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		NullCheck(L_6);
		if ((!(((uint32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_6)->max_length))))) == ((uint32_t)2))))
		{
			goto IL_003e;
		}
	}
	{
		ImmutableList_1_t2502565896 * L_7 = (ImmutableList_1_t2502565896 *)__this->get__observers_0();
		NullCheck((ImmutableList_1_t2502565896 *)L_7);
		IObserver_1U5BU5D_t3405924184* L_8 = ((  IObserver_1U5BU5D_t3405924184* (*) (ImmutableList_1_t2502565896 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((ImmutableList_1_t2502565896 *)L_7, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_9 = V_0;
		NullCheck(L_8);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_8, ((int32_t)((int32_t)1-(int32_t)L_9)));
		int32_t L_10 = ((int32_t)((int32_t)1-(int32_t)L_9));
		return ((L_8)->GetAt(static_cast<il2cpp_array_size_t>(L_10)));
	}

IL_003e:
	{
		ImmutableList_1_t2502565896 * L_11 = (ImmutableList_1_t2502565896 *)__this->get__observers_0();
		Il2CppObject* L_12 = ___observer0;
		NullCheck((ImmutableList_1_t2502565896 *)L_11);
		ImmutableList_1_t2502565896 * L_13 = ((  ImmutableList_1_t2502565896 * (*) (ImmutableList_1_t2502565896 *, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6)->method)((ImmutableList_1_t2502565896 *)L_11, (Il2CppObject*)L_12, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		ListObserver_1_t3819947461 * L_14 = (ListObserver_1_t3819947461 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		((  void (*) (ListObserver_1_t3819947461 *, ImmutableList_1_t2502565896 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)(L_14, (ImmutableList_1_t2502565896 *)L_13, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		return L_14;
	}
}
// System.Int32 UniRx.InternalUtil.PriorityQueue`1/IndexedItem<System.Object>::CompareTo(UniRx.InternalUtil.PriorityQueue`1/IndexedItem<T>)
extern "C"  int32_t IndexedItem_CompareTo_m3674265725_gshared (IndexedItem_t4248560733 * __this, IndexedItem_t4248560733  ___other0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		Il2CppObject ** L_0 = (Il2CppObject **)__this->get_address_of_Value_0();
		Il2CppObject * L_1 = (Il2CppObject *)(&___other0)->get_Value_0();
		NullCheck((Il2CppObject*)(*L_0));
		int32_t L_2 = InterfaceFuncInvoker1< int32_t, Il2CppObject * >::Invoke(0 /* System.Int32 System.IComparable`1<System.Object>::CompareTo(!0) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), (Il2CppObject*)(*L_0), (Il2CppObject *)L_1);
		V_0 = (int32_t)L_2;
		int32_t L_3 = V_0;
		if (L_3)
		{
			goto IL_0032;
		}
	}
	{
		int64_t* L_4 = (int64_t*)__this->get_address_of_Id_1();
		int64_t L_5 = (int64_t)(&___other0)->get_Id_1();
		int32_t L_6 = Int64_CompareTo_m1210454552((int64_t*)L_4, (int64_t)L_5, /*hidden argument*/NULL);
		V_0 = (int32_t)L_6;
	}

IL_0032:
	{
		int32_t L_7 = V_0;
		return L_7;
	}
}
// System.Void UniRx.InternalUtil.PriorityQueue`1<System.Object>::.ctor()
extern "C"  void PriorityQueue_1__ctor_m698167010_gshared (PriorityQueue_1_t1335382412 * __this, const MethodInfo* method)
{
	{
		NullCheck((PriorityQueue_1_t1335382412 *)__this);
		((  void (*) (PriorityQueue_1_t1335382412 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((PriorityQueue_1_t1335382412 *)__this, (int32_t)((int32_t)16), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		return;
	}
}
// System.Void UniRx.InternalUtil.PriorityQueue`1<System.Object>::.ctor(System.Int32)
extern "C"  void PriorityQueue_1__ctor_m3615596851_gshared (PriorityQueue_1_t1335382412 * __this, int32_t ___capacity0, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		int32_t L_0 = ___capacity0;
		__this->set__items_1(((IndexedItemU5BU5D_t3079746960*)SZArrayNew(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), (uint32_t)L_0)));
		__this->set__size_2(0);
		return;
	}
}
// System.Void UniRx.InternalUtil.PriorityQueue`1<System.Object>::.cctor()
extern "C"  void PriorityQueue_1__cctor_m3981211915_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		((PriorityQueue_1_t1335382412_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->static_fields)->set__count_0(((int64_t)std::numeric_limits<int64_t>::min()));
		return;
	}
}
// System.Boolean UniRx.InternalUtil.PriorityQueue`1<System.Object>::IsHigherPriority(System.Int32,System.Int32)
extern "C"  bool PriorityQueue_1_IsHigherPriority_m999759587_gshared (PriorityQueue_1_t1335382412 * __this, int32_t ___left0, int32_t ___right1, const MethodInfo* method)
{
	{
		IndexedItemU5BU5D_t3079746960* L_0 = (IndexedItemU5BU5D_t3079746960*)__this->get__items_1();
		int32_t L_1 = ___left0;
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, L_1);
		IndexedItemU5BU5D_t3079746960* L_2 = (IndexedItemU5BU5D_t3079746960*)__this->get__items_1();
		int32_t L_3 = ___right1;
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, L_3);
		int32_t L_4 = ((  int32_t (*) (IndexedItem_t4248560733 *, IndexedItem_t4248560733 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)((IndexedItem_t4248560733 *)((L_0)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_1))), (IndexedItem_t4248560733 )(*(IndexedItem_t4248560733 *)((L_2)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_3)))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		return (bool)((((int32_t)L_4) < ((int32_t)0))? 1 : 0);
	}
}
// System.Void UniRx.InternalUtil.PriorityQueue`1<System.Object>::Percolate(System.Int32)
extern "C"  void PriorityQueue_1_Percolate_m2937402368_gshared (PriorityQueue_1_t1335382412 * __this, int32_t ___index0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	IndexedItem_t4248560733  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		int32_t L_0 = ___index0;
		int32_t L_1 = (int32_t)__this->get__size_2();
		if ((((int32_t)L_0) >= ((int32_t)L_1)))
		{
			goto IL_0013;
		}
	}
	{
		int32_t L_2 = ___index0;
		if ((((int32_t)L_2) >= ((int32_t)0)))
		{
			goto IL_0014;
		}
	}

IL_0013:
	{
		return;
	}

IL_0014:
	{
		int32_t L_3 = ___index0;
		V_0 = (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_3-(int32_t)1))/(int32_t)2));
		int32_t L_4 = V_0;
		if ((((int32_t)L_4) < ((int32_t)0)))
		{
			goto IL_0028;
		}
	}
	{
		int32_t L_5 = V_0;
		int32_t L_6 = ___index0;
		if ((!(((uint32_t)L_5) == ((uint32_t)L_6))))
		{
			goto IL_0029;
		}
	}

IL_0028:
	{
		return;
	}

IL_0029:
	{
		int32_t L_7 = ___index0;
		int32_t L_8 = V_0;
		NullCheck((PriorityQueue_1_t1335382412 *)__this);
		bool L_9 = ((  bool (*) (PriorityQueue_1_t1335382412 *, int32_t, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)((PriorityQueue_1_t1335382412 *)__this, (int32_t)L_7, (int32_t)L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		if (!L_9)
		{
			goto IL_0083;
		}
	}
	{
		IndexedItemU5BU5D_t3079746960* L_10 = (IndexedItemU5BU5D_t3079746960*)__this->get__items_1();
		int32_t L_11 = ___index0;
		NullCheck(L_10);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_10, L_11);
		V_1 = (IndexedItem_t4248560733 )(*(IndexedItem_t4248560733 *)((L_10)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_11))));
		IndexedItemU5BU5D_t3079746960* L_12 = (IndexedItemU5BU5D_t3079746960*)__this->get__items_1();
		int32_t L_13 = ___index0;
		NullCheck(L_12);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_12, L_13);
		IndexedItemU5BU5D_t3079746960* L_14 = (IndexedItemU5BU5D_t3079746960*)__this->get__items_1();
		int32_t L_15 = V_0;
		NullCheck(L_14);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_14, L_15);
		(*(IndexedItem_t4248560733 *)((L_12)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_13)))) = (*(IndexedItem_t4248560733 *)((L_14)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_15))));
		IndexedItemU5BU5D_t3079746960* L_16 = (IndexedItemU5BU5D_t3079746960*)__this->get__items_1();
		int32_t L_17 = V_0;
		NullCheck(L_16);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_16, L_17);
		IndexedItem_t4248560733  L_18 = V_1;
		(*(IndexedItem_t4248560733 *)((L_16)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_17)))) = L_18;
		int32_t L_19 = V_0;
		NullCheck((PriorityQueue_1_t1335382412 *)__this);
		((  void (*) (PriorityQueue_1_t1335382412 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->method)((PriorityQueue_1_t1335382412 *)__this, (int32_t)L_19, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
	}

IL_0083:
	{
		return;
	}
}
// System.Void UniRx.InternalUtil.PriorityQueue`1<System.Object>::Heapify()
extern "C"  void PriorityQueue_1_Heapify_m704617936_gshared (PriorityQueue_1_t1335382412 * __this, const MethodInfo* method)
{
	{
		NullCheck((PriorityQueue_1_t1335382412 *)__this);
		((  void (*) (PriorityQueue_1_t1335382412 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6)->method)((PriorityQueue_1_t1335382412 *)__this, (int32_t)0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		return;
	}
}
// System.Void UniRx.InternalUtil.PriorityQueue`1<System.Object>::Heapify(System.Int32)
extern "C"  void PriorityQueue_1_Heapify_m1602165537_gshared (PriorityQueue_1_t1335382412 * __this, int32_t ___index0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	IndexedItem_t4248560733  V_3;
	memset(&V_3, 0, sizeof(V_3));
	{
		int32_t L_0 = ___index0;
		int32_t L_1 = (int32_t)__this->get__size_2();
		if ((((int32_t)L_0) >= ((int32_t)L_1)))
		{
			goto IL_0013;
		}
	}
	{
		int32_t L_2 = ___index0;
		if ((((int32_t)L_2) >= ((int32_t)0)))
		{
			goto IL_0014;
		}
	}

IL_0013:
	{
		return;
	}

IL_0014:
	{
		int32_t L_3 = ___index0;
		V_0 = (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)2*(int32_t)L_3))+(int32_t)1));
		int32_t L_4 = ___index0;
		V_1 = (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)2*(int32_t)L_4))+(int32_t)2));
		int32_t L_5 = ___index0;
		V_2 = (int32_t)L_5;
		int32_t L_6 = V_0;
		int32_t L_7 = (int32_t)__this->get__size_2();
		if ((((int32_t)L_6) >= ((int32_t)L_7)))
		{
			goto IL_003d;
		}
	}
	{
		int32_t L_8 = V_0;
		int32_t L_9 = V_2;
		NullCheck((PriorityQueue_1_t1335382412 *)__this);
		bool L_10 = ((  bool (*) (PriorityQueue_1_t1335382412 *, int32_t, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)((PriorityQueue_1_t1335382412 *)__this, (int32_t)L_8, (int32_t)L_9, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		if (!L_10)
		{
			goto IL_003d;
		}
	}
	{
		int32_t L_11 = V_0;
		V_2 = (int32_t)L_11;
	}

IL_003d:
	{
		int32_t L_12 = V_1;
		int32_t L_13 = (int32_t)__this->get__size_2();
		if ((((int32_t)L_12) >= ((int32_t)L_13)))
		{
			goto IL_0058;
		}
	}
	{
		int32_t L_14 = V_1;
		int32_t L_15 = V_2;
		NullCheck((PriorityQueue_1_t1335382412 *)__this);
		bool L_16 = ((  bool (*) (PriorityQueue_1_t1335382412 *, int32_t, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)((PriorityQueue_1_t1335382412 *)__this, (int32_t)L_14, (int32_t)L_15, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		if (!L_16)
		{
			goto IL_0058;
		}
	}
	{
		int32_t L_17 = V_1;
		V_2 = (int32_t)L_17;
	}

IL_0058:
	{
		int32_t L_18 = V_2;
		int32_t L_19 = ___index0;
		if ((((int32_t)L_18) == ((int32_t)L_19)))
		{
			goto IL_00ac;
		}
	}
	{
		IndexedItemU5BU5D_t3079746960* L_20 = (IndexedItemU5BU5D_t3079746960*)__this->get__items_1();
		int32_t L_21 = ___index0;
		NullCheck(L_20);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_20, L_21);
		V_3 = (IndexedItem_t4248560733 )(*(IndexedItem_t4248560733 *)((L_20)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_21))));
		IndexedItemU5BU5D_t3079746960* L_22 = (IndexedItemU5BU5D_t3079746960*)__this->get__items_1();
		int32_t L_23 = ___index0;
		NullCheck(L_22);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_22, L_23);
		IndexedItemU5BU5D_t3079746960* L_24 = (IndexedItemU5BU5D_t3079746960*)__this->get__items_1();
		int32_t L_25 = V_2;
		NullCheck(L_24);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_24, L_25);
		(*(IndexedItem_t4248560733 *)((L_22)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_23)))) = (*(IndexedItem_t4248560733 *)((L_24)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_25))));
		IndexedItemU5BU5D_t3079746960* L_26 = (IndexedItemU5BU5D_t3079746960*)__this->get__items_1();
		int32_t L_27 = V_2;
		NullCheck(L_26);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_26, L_27);
		IndexedItem_t4248560733  L_28 = V_3;
		(*(IndexedItem_t4248560733 *)((L_26)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_27)))) = L_28;
		int32_t L_29 = V_2;
		NullCheck((PriorityQueue_1_t1335382412 *)__this);
		((  void (*) (PriorityQueue_1_t1335382412 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6)->method)((PriorityQueue_1_t1335382412 *)__this, (int32_t)L_29, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
	}

IL_00ac:
	{
		return;
	}
}
// System.Int32 UniRx.InternalUtil.PriorityQueue`1<System.Object>::get_Count()
extern "C"  int32_t PriorityQueue_1_get_Count_m1055643996_gshared (PriorityQueue_1_t1335382412 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (int32_t)__this->get__size_2();
		return L_0;
	}
}
// T UniRx.InternalUtil.PriorityQueue`1<System.Object>::Peek()
extern Il2CppClass* InvalidOperationException_t2420574324_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1857073771;
extern const uint32_t PriorityQueue_1_Peek_m2099666718_MetadataUsageId;
extern "C"  Il2CppObject * PriorityQueue_1_Peek_m2099666718_gshared (PriorityQueue_1_t1335382412 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (PriorityQueue_1_Peek_m2099666718_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get__size_2();
		if (L_0)
		{
			goto IL_0016;
		}
	}
	{
		InvalidOperationException_t2420574324 * L_1 = (InvalidOperationException_t2420574324 *)il2cpp_codegen_object_new(InvalidOperationException_t2420574324_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_1, (String_t*)_stringLiteral1857073771, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0016:
	{
		IndexedItemU5BU5D_t3079746960* L_2 = (IndexedItemU5BU5D_t3079746960*)__this->get__items_1();
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, 0);
		Il2CppObject * L_3 = (Il2CppObject *)((L_2)->GetAddressAt(static_cast<il2cpp_array_size_t>(0)))->get_Value_0();
		return L_3;
	}
}
// System.Void UniRx.InternalUtil.PriorityQueue`1<System.Object>::RemoveAt(System.Int32)
extern Il2CppClass* IndexedItem_t4248560733_il2cpp_TypeInfo_var;
extern const uint32_t PriorityQueue_1_RemoveAt_m2349195146_MetadataUsageId;
extern "C"  void PriorityQueue_1_RemoveAt_m2349195146_gshared (PriorityQueue_1_t1335382412 * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (PriorityQueue_1_RemoveAt_m2349195146_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	IndexedItemU5BU5D_t3079746960* V_0 = NULL;
	int32_t V_1 = 0;
	IndexedItem_t4248560733  V_2;
	memset(&V_2, 0, sizeof(V_2));
	{
		IndexedItemU5BU5D_t3079746960* L_0 = (IndexedItemU5BU5D_t3079746960*)__this->get__items_1();
		int32_t L_1 = ___index0;
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, L_1);
		IndexedItemU5BU5D_t3079746960* L_2 = (IndexedItemU5BU5D_t3079746960*)__this->get__items_1();
		int32_t L_3 = (int32_t)__this->get__size_2();
		int32_t L_4 = (int32_t)((int32_t)((int32_t)L_3-(int32_t)1));
		V_1 = (int32_t)L_4;
		__this->set__size_2(L_4);
		int32_t L_5 = V_1;
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, L_5);
		(*(IndexedItem_t4248560733 *)((L_0)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_1)))) = (*(IndexedItem_t4248560733 *)((L_2)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_5))));
		IndexedItemU5BU5D_t3079746960* L_6 = (IndexedItemU5BU5D_t3079746960*)__this->get__items_1();
		int32_t L_7 = (int32_t)__this->get__size_2();
		NullCheck(L_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_6, L_7);
		Initobj (IndexedItem_t4248560733_il2cpp_TypeInfo_var, (&V_2));
		IndexedItem_t4248560733  L_8 = V_2;
		(*(IndexedItem_t4248560733 *)((L_6)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_7)))) = L_8;
		NullCheck((PriorityQueue_1_t1335382412 *)__this);
		((  void (*) (PriorityQueue_1_t1335382412 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7)->method)((PriorityQueue_1_t1335382412 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7));
		int32_t L_9 = (int32_t)__this->get__size_2();
		IndexedItemU5BU5D_t3079746960* L_10 = (IndexedItemU5BU5D_t3079746960*)__this->get__items_1();
		NullCheck(L_10);
		if ((((int32_t)L_9) >= ((int32_t)((int32_t)((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_10)->max_length))))/(int32_t)4)))))
		{
			goto IL_009c;
		}
	}
	{
		IndexedItemU5BU5D_t3079746960* L_11 = (IndexedItemU5BU5D_t3079746960*)__this->get__items_1();
		V_0 = (IndexedItemU5BU5D_t3079746960*)L_11;
		IndexedItemU5BU5D_t3079746960* L_12 = (IndexedItemU5BU5D_t3079746960*)__this->get__items_1();
		NullCheck(L_12);
		__this->set__items_1(((IndexedItemU5BU5D_t3079746960*)SZArrayNew(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), (uint32_t)((int32_t)((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_12)->max_length))))/(int32_t)2)))));
		IndexedItemU5BU5D_t3079746960* L_13 = V_0;
		IndexedItemU5BU5D_t3079746960* L_14 = (IndexedItemU5BU5D_t3079746960*)__this->get__items_1();
		int32_t L_15 = (int32_t)__this->get__size_2();
		Array_Copy_m2598616668(NULL /*static, unused*/, (Il2CppArray *)(Il2CppArray *)L_13, (int32_t)0, (Il2CppArray *)(Il2CppArray *)L_14, (int32_t)0, (int32_t)L_15, /*hidden argument*/NULL);
	}

IL_009c:
	{
		return;
	}
}
// T UniRx.InternalUtil.PriorityQueue`1<System.Object>::Dequeue()
extern "C"  Il2CppObject * PriorityQueue_1_Dequeue_m1887212399_gshared (PriorityQueue_1_t1335382412 * __this, const MethodInfo* method)
{
	Il2CppObject * V_0 = NULL;
	{
		NullCheck((PriorityQueue_1_t1335382412 *)__this);
		Il2CppObject * L_0 = ((  Il2CppObject * (*) (PriorityQueue_1_t1335382412 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8)->method)((PriorityQueue_1_t1335382412 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8));
		V_0 = (Il2CppObject *)L_0;
		NullCheck((PriorityQueue_1_t1335382412 *)__this);
		((  void (*) (PriorityQueue_1_t1335382412 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9)->method)((PriorityQueue_1_t1335382412 *)__this, (int32_t)0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9));
		Il2CppObject * L_1 = V_0;
		return L_1;
	}
}
// System.Void UniRx.InternalUtil.PriorityQueue`1<System.Object>::Enqueue(T)
extern Il2CppClass* IndexedItem_t4248560733_il2cpp_TypeInfo_var;
extern const uint32_t PriorityQueue_1_Enqueue_m4027093526_MetadataUsageId;
extern "C"  void PriorityQueue_1_Enqueue_m4027093526_gshared (PriorityQueue_1_t1335382412 * __this, Il2CppObject * ___item0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (PriorityQueue_1_Enqueue_m4027093526_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	IndexedItemU5BU5D_t3079746960* V_0 = NULL;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	IndexedItem_t4248560733  V_3;
	memset(&V_3, 0, sizeof(V_3));
	{
		int32_t L_0 = (int32_t)__this->get__size_2();
		IndexedItemU5BU5D_t3079746960* L_1 = (IndexedItemU5BU5D_t3079746960*)__this->get__items_1();
		NullCheck(L_1);
		if ((((int32_t)L_0) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_1)->max_length)))))))
		{
			goto IL_003e;
		}
	}
	{
		IndexedItemU5BU5D_t3079746960* L_2 = (IndexedItemU5BU5D_t3079746960*)__this->get__items_1();
		V_0 = (IndexedItemU5BU5D_t3079746960*)L_2;
		IndexedItemU5BU5D_t3079746960* L_3 = (IndexedItemU5BU5D_t3079746960*)__this->get__items_1();
		NullCheck(L_3);
		__this->set__items_1(((IndexedItemU5BU5D_t3079746960*)SZArrayNew(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), (uint32_t)((int32_t)((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_3)->max_length))))*(int32_t)2)))));
		IndexedItemU5BU5D_t3079746960* L_4 = V_0;
		IndexedItemU5BU5D_t3079746960* L_5 = (IndexedItemU5BU5D_t3079746960*)__this->get__items_1();
		IndexedItemU5BU5D_t3079746960* L_6 = V_0;
		NullCheck(L_6);
		Array_Copy_m3799309042(NULL /*static, unused*/, (Il2CppArray *)(Il2CppArray *)L_4, (Il2CppArray *)(Il2CppArray *)L_5, (int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_6)->max_length)))), /*hidden argument*/NULL);
	}

IL_003e:
	{
		int32_t L_7 = (int32_t)__this->get__size_2();
		int32_t L_8 = (int32_t)L_7;
		V_2 = (int32_t)L_8;
		__this->set__size_2(((int32_t)((int32_t)L_8+(int32_t)1)));
		int32_t L_9 = V_2;
		V_1 = (int32_t)L_9;
		IndexedItemU5BU5D_t3079746960* L_10 = (IndexedItemU5BU5D_t3079746960*)__this->get__items_1();
		int32_t L_11 = V_1;
		NullCheck(L_10);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_10, L_11);
		Initobj (IndexedItem_t4248560733_il2cpp_TypeInfo_var, (&V_3));
		Il2CppObject * L_12 = ___item0;
		(&V_3)->set_Value_0(L_12);
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		int64_t L_13 = Interlocked_Increment_m466587433(NULL /*static, unused*/, (int64_t*)(((PriorityQueue_1_t1335382412_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->static_fields)->get_address_of__count_0()), /*hidden argument*/NULL);
		(&V_3)->set_Id_1(L_13);
		IndexedItem_t4248560733  L_14 = V_3;
		(*(IndexedItem_t4248560733 *)((L_10)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_11)))) = L_14;
		int32_t L_15 = V_1;
		NullCheck((PriorityQueue_1_t1335382412 *)__this);
		((  void (*) (PriorityQueue_1_t1335382412 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->method)((PriorityQueue_1_t1335382412 *)__this, (int32_t)L_15, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		return;
	}
}
// System.Boolean UniRx.InternalUtil.PriorityQueue`1<System.Object>::Remove(T)
extern "C"  bool PriorityQueue_1_Remove_m112164948_gshared (PriorityQueue_1_t1335382412 * __this, Il2CppObject * ___item0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		V_0 = (int32_t)0;
		goto IL_0035;
	}

IL_0007:
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 11));
		EqualityComparer_1_t271497070 * L_0 = ((  EqualityComparer_1_t271497070 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10)->method)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10));
		IndexedItemU5BU5D_t3079746960* L_1 = (IndexedItemU5BU5D_t3079746960*)__this->get__items_1();
		int32_t L_2 = V_0;
		NullCheck(L_1);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_1, L_2);
		Il2CppObject * L_3 = (Il2CppObject *)((L_1)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_2)))->get_Value_0();
		Il2CppObject * L_4 = ___item0;
		NullCheck((EqualityComparer_1_t271497070 *)L_0);
		bool L_5 = VirtFuncInvoker2< bool, Il2CppObject *, Il2CppObject * >::Invoke(9 /* System.Boolean System.Collections.Generic.EqualityComparer`1<System.Object>::Equals(!0,!0) */, (EqualityComparer_1_t271497070 *)L_0, (Il2CppObject *)L_3, (Il2CppObject *)L_4);
		if (!L_5)
		{
			goto IL_0031;
		}
	}
	{
		int32_t L_6 = V_0;
		NullCheck((PriorityQueue_1_t1335382412 *)__this);
		((  void (*) (PriorityQueue_1_t1335382412 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9)->method)((PriorityQueue_1_t1335382412 *)__this, (int32_t)L_6, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9));
		return (bool)1;
	}

IL_0031:
	{
		int32_t L_7 = V_0;
		V_0 = (int32_t)((int32_t)((int32_t)L_7+(int32_t)1));
	}

IL_0035:
	{
		int32_t L_8 = V_0;
		int32_t L_9 = (int32_t)__this->get__size_2();
		if ((((int32_t)L_8) < ((int32_t)L_9)))
		{
			goto IL_0007;
		}
	}
	{
		return (bool)0;
	}
}
// System.Void UniRx.InternalUtil.ThrowObserver`1<System.Int32>::.ctor()
extern "C"  void ThrowObserver_1__ctor_m255404998_gshared (ThrowObserver_1_t3499376402 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UniRx.InternalUtil.ThrowObserver`1<System.Int32>::.cctor()
extern "C"  void ThrowObserver_1__cctor_m3140491431_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		ThrowObserver_1_t3499376402 * L_0 = (ThrowObserver_1_t3499376402 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		((  void (*) (ThrowObserver_1_t3499376402 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		((ThrowObserver_1_t3499376402_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->static_fields)->set_Instance_0(L_0);
		return;
	}
}
// System.Void UniRx.InternalUtil.ThrowObserver`1<System.Int32>::OnCompleted()
extern "C"  void ThrowObserver_1_OnCompleted_m124383952_gshared (ThrowObserver_1_t3499376402 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void UniRx.InternalUtil.ThrowObserver`1<System.Int32>::OnError(System.Exception)
extern "C"  void ThrowObserver_1_OnError_m4113492989_gshared (ThrowObserver_1_t3499376402 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method)
{
	{
		Exception_t1967233988 * L_0 = ___error0;
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void UniRx.InternalUtil.ThrowObserver`1<System.Int32>::OnNext(T)
extern "C"  void ThrowObserver_1_OnNext_m3461770478_gshared (ThrowObserver_1_t3499376402 * __this, int32_t ___value0, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void UniRx.InternalUtil.ThrowObserver`1<System.Object>::.ctor()
extern "C"  void ThrowObserver_1__ctor_m780643377_gshared (ThrowObserver_1_t1489068035 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UniRx.InternalUtil.ThrowObserver`1<System.Object>::.cctor()
extern "C"  void ThrowObserver_1__cctor_m2243011996_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		ThrowObserver_1_t1489068035 * L_0 = (ThrowObserver_1_t1489068035 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		((  void (*) (ThrowObserver_1_t1489068035 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		((ThrowObserver_1_t1489068035_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->static_fields)->set_Instance_0(L_0);
		return;
	}
}
// System.Void UniRx.InternalUtil.ThrowObserver`1<System.Object>::OnCompleted()
extern "C"  void ThrowObserver_1_OnCompleted_m773011195_gshared (ThrowObserver_1_t1489068035 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void UniRx.InternalUtil.ThrowObserver`1<System.Object>::OnError(System.Exception)
extern "C"  void ThrowObserver_1_OnError_m1142179752_gshared (ThrowObserver_1_t1489068035 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method)
{
	{
		Exception_t1967233988 * L_0 = ___error0;
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void UniRx.InternalUtil.ThrowObserver`1<System.Object>::OnNext(T)
extern "C"  void ThrowObserver_1_OnNext_m1409711769_gshared (ThrowObserver_1_t1489068035 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void UniRx.InternalUtil.ThrowObserver`1<UniRx.Unit>::.ctor()
extern "C"  void ThrowObserver_1__ctor_m872765267_gshared (ThrowObserver_1_t3210247653 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UniRx.InternalUtil.ThrowObserver`1<UniRx.Unit>::.cctor()
extern "C"  void ThrowObserver_1__cctor_m803823290_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		ThrowObserver_1_t3210247653 * L_0 = (ThrowObserver_1_t3210247653 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		((  void (*) (ThrowObserver_1_t3210247653 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		((ThrowObserver_1_t3210247653_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->static_fields)->set_Instance_0(L_0);
		return;
	}
}
// System.Void UniRx.InternalUtil.ThrowObserver`1<UniRx.Unit>::OnCompleted()
extern "C"  void ThrowObserver_1_OnCompleted_m838369437_gshared (ThrowObserver_1_t3210247653 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void UniRx.InternalUtil.ThrowObserver`1<UniRx.Unit>::OnError(System.Exception)
extern "C"  void ThrowObserver_1_OnError_m3829280330_gshared (ThrowObserver_1_t3210247653 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method)
{
	{
		Exception_t1967233988 * L_0 = ___error0;
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void UniRx.InternalUtil.ThrowObserver`1<UniRx.Unit>::OnNext(T)
extern "C"  void ThrowObserver_1_OnNext_m4039502139_gshared (ThrowObserver_1_t3210247653 * __this, Unit_t2558286038  ___value0, const MethodInfo* method)
{
	{
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
