﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.EqualityComparer`1<UniRx.Tuple`2<System.Object,System.Object>>
struct EqualityComparer_1_t4098619765;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"

// System.Void System.Collections.Generic.EqualityComparer`1<UniRx.Tuple`2<System.Object,System.Object>>::.ctor()
extern "C"  void EqualityComparer_1__ctor_m1973020051_gshared (EqualityComparer_1_t4098619765 * __this, const MethodInfo* method);
#define EqualityComparer_1__ctor_m1973020051(__this, method) ((  void (*) (EqualityComparer_1_t4098619765 *, const MethodInfo*))EqualityComparer_1__ctor_m1973020051_gshared)(__this, method)
// System.Void System.Collections.Generic.EqualityComparer`1<UniRx.Tuple`2<System.Object,System.Object>>::.cctor()
extern "C"  void EqualityComparer_1__cctor_m551983226_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define EqualityComparer_1__cctor_m551983226(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))EqualityComparer_1__cctor_m551983226_gshared)(__this /* static, unused */, method)
// System.Int32 System.Collections.Generic.EqualityComparer`1<UniRx.Tuple`2<System.Object,System.Object>>::System.Collections.IEqualityComparer.GetHashCode(System.Object)
extern "C"  int32_t EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m532347172_gshared (EqualityComparer_1_t4098619765 * __this, Il2CppObject * ___obj0, const MethodInfo* method);
#define EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m532347172(__this, ___obj0, method) ((  int32_t (*) (EqualityComparer_1_t4098619765 *, Il2CppObject *, const MethodInfo*))EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m532347172_gshared)(__this, ___obj0, method)
// System.Boolean System.Collections.Generic.EqualityComparer`1<UniRx.Tuple`2<System.Object,System.Object>>::System.Collections.IEqualityComparer.Equals(System.Object,System.Object)
extern "C"  bool EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m55931226_gshared (EqualityComparer_1_t4098619765 * __this, Il2CppObject * ___x0, Il2CppObject * ___y1, const MethodInfo* method);
#define EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m55931226(__this, ___x0, ___y1, method) ((  bool (*) (EqualityComparer_1_t4098619765 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m55931226_gshared)(__this, ___x0, ___y1, method)
// System.Collections.Generic.EqualityComparer`1<T> System.Collections.Generic.EqualityComparer`1<UniRx.Tuple`2<System.Object,System.Object>>::get_Default()
extern "C"  EqualityComparer_1_t4098619765 * EqualityComparer_1_get_Default_m2478698133_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define EqualityComparer_1_get_Default_m2478698133(__this /* static, unused */, method) ((  EqualityComparer_1_t4098619765 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))EqualityComparer_1_get_Default_m2478698133_gshared)(__this /* static, unused */, method)
