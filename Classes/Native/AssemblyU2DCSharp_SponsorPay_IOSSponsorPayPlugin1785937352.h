﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Generic.Dictionary`2<SponsorPay.SPLogLevel,System.Int32>
struct Dictionary_2_t2290665470;
// System.Action`1<System.Int32>
struct Action_1_t2995867492;

#include "UnityEngine_UnityEngine_MonoBehaviour3012272455.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SponsorPay.IOSSponsorPayPlugin
struct  IOSSponsorPayPlugin_t1785937352  : public MonoBehaviour_t3012272455
{
public:

public:
};

struct IOSSponsorPayPlugin_t1785937352_StaticFields
{
public:
	// System.Boolean SponsorPay.IOSSponsorPayPlugin::didSetCallbackName
	bool ___didSetCallbackName_2;
	// System.Collections.Generic.Dictionary`2<SponsorPay.SPLogLevel,System.Int32> SponsorPay.IOSSponsorPayPlugin::logLevelMapping
	Dictionary_2_t2290665470 * ___logLevelMapping_3;
	// System.Action`1<System.Int32> SponsorPay.IOSSponsorPayPlugin::<>f__am$cache2
	Action_1_t2995867492 * ___U3CU3Ef__amU24cache2_4;
	// System.Action`1<System.Int32> SponsorPay.IOSSponsorPayPlugin::<>f__am$cache3
	Action_1_t2995867492 * ___U3CU3Ef__amU24cache3_5;

public:
	inline static int32_t get_offset_of_didSetCallbackName_2() { return static_cast<int32_t>(offsetof(IOSSponsorPayPlugin_t1785937352_StaticFields, ___didSetCallbackName_2)); }
	inline bool get_didSetCallbackName_2() const { return ___didSetCallbackName_2; }
	inline bool* get_address_of_didSetCallbackName_2() { return &___didSetCallbackName_2; }
	inline void set_didSetCallbackName_2(bool value)
	{
		___didSetCallbackName_2 = value;
	}

	inline static int32_t get_offset_of_logLevelMapping_3() { return static_cast<int32_t>(offsetof(IOSSponsorPayPlugin_t1785937352_StaticFields, ___logLevelMapping_3)); }
	inline Dictionary_2_t2290665470 * get_logLevelMapping_3() const { return ___logLevelMapping_3; }
	inline Dictionary_2_t2290665470 ** get_address_of_logLevelMapping_3() { return &___logLevelMapping_3; }
	inline void set_logLevelMapping_3(Dictionary_2_t2290665470 * value)
	{
		___logLevelMapping_3 = value;
		Il2CppCodeGenWriteBarrier(&___logLevelMapping_3, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache2_4() { return static_cast<int32_t>(offsetof(IOSSponsorPayPlugin_t1785937352_StaticFields, ___U3CU3Ef__amU24cache2_4)); }
	inline Action_1_t2995867492 * get_U3CU3Ef__amU24cache2_4() const { return ___U3CU3Ef__amU24cache2_4; }
	inline Action_1_t2995867492 ** get_address_of_U3CU3Ef__amU24cache2_4() { return &___U3CU3Ef__amU24cache2_4; }
	inline void set_U3CU3Ef__amU24cache2_4(Action_1_t2995867492 * value)
	{
		___U3CU3Ef__amU24cache2_4 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache2_4, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache3_5() { return static_cast<int32_t>(offsetof(IOSSponsorPayPlugin_t1785937352_StaticFields, ___U3CU3Ef__amU24cache3_5)); }
	inline Action_1_t2995867492 * get_U3CU3Ef__amU24cache3_5() const { return ___U3CU3Ef__amU24cache3_5; }
	inline Action_1_t2995867492 ** get_address_of_U3CU3Ef__amU24cache3_5() { return &___U3CU3Ef__amU24cache3_5; }
	inline void set_U3CU3Ef__amU24cache3_5(Action_1_t2995867492 * value)
	{
		___U3CU3Ef__amU24cache3_5 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache3_5, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
