﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.ClientModels.GetCloudScriptUrlRequest
struct GetCloudScriptUrlRequest_t841424922;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Nullable_1_gen1438485399.h"
#include "mscorlib_System_Nullable_1_gen3097043249.h"

// System.Void PlayFab.ClientModels.GetCloudScriptUrlRequest::.ctor()
extern "C"  void GetCloudScriptUrlRequest__ctor_m2169897827 (GetCloudScriptUrlRequest_t841424922 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Nullable`1<System.Int32> PlayFab.ClientModels.GetCloudScriptUrlRequest::get_Version()
extern "C"  Nullable_1_t1438485399  GetCloudScriptUrlRequest_get_Version_m1028552223 (GetCloudScriptUrlRequest_t841424922 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.GetCloudScriptUrlRequest::set_Version(System.Nullable`1<System.Int32>)
extern "C"  void GetCloudScriptUrlRequest_set_Version_m2112336658 (GetCloudScriptUrlRequest_t841424922 * __this, Nullable_1_t1438485399  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Nullable`1<System.Boolean> PlayFab.ClientModels.GetCloudScriptUrlRequest::get_Testing()
extern "C"  Nullable_1_t3097043249  GetCloudScriptUrlRequest_get_Testing_m2762042289 (GetCloudScriptUrlRequest_t841424922 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.GetCloudScriptUrlRequest::set_Testing(System.Nullable`1<System.Boolean>)
extern "C"  void GetCloudScriptUrlRequest_set_Testing_m677392308 (GetCloudScriptUrlRequest_t841424922 * __this, Nullable_1_t3097043249  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
