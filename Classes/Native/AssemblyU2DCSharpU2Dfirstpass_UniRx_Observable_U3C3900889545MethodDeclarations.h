﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Observable/<WrapEnumerator>c__IteratorF
struct U3CWrapEnumeratorU3Ec__IteratorF_t3900889545;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void UniRx.Observable/<WrapEnumerator>c__IteratorF::.ctor()
extern "C"  void U3CWrapEnumeratorU3Ec__IteratorF__ctor_m1766589338 (U3CWrapEnumeratorU3Ec__IteratorF_t3900889545 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object UniRx.Observable/<WrapEnumerator>c__IteratorF::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CWrapEnumeratorU3Ec__IteratorF_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1959823288 (U3CWrapEnumeratorU3Ec__IteratorF_t3900889545 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object UniRx.Observable/<WrapEnumerator>c__IteratorF::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CWrapEnumeratorU3Ec__IteratorF_System_Collections_IEnumerator_get_Current_m3199793996 (U3CWrapEnumeratorU3Ec__IteratorF_t3900889545 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UniRx.Observable/<WrapEnumerator>c__IteratorF::MoveNext()
extern "C"  bool U3CWrapEnumeratorU3Ec__IteratorF_MoveNext_m4012428594 (U3CWrapEnumeratorU3Ec__IteratorF_t3900889545 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.Observable/<WrapEnumerator>c__IteratorF::Dispose()
extern "C"  void U3CWrapEnumeratorU3Ec__IteratorF_Dispose_m1078554967 (U3CWrapEnumeratorU3Ec__IteratorF_t3900889545 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.Observable/<WrapEnumerator>c__IteratorF::Reset()
extern "C"  void U3CWrapEnumeratorU3Ec__IteratorF_Reset_m3707989575 (U3CWrapEnumeratorU3Ec__IteratorF_t3900889545 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.Observable/<WrapEnumerator>c__IteratorF::<>__Finally0()
extern "C"  void U3CWrapEnumeratorU3Ec__IteratorF_U3CU3E__Finally0_m1302742425 (U3CWrapEnumeratorU3Ec__IteratorF_t3900889545 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.Observable/<WrapEnumerator>c__IteratorF::<>__Finally1()
extern "C"  void U3CWrapEnumeratorU3Ec__IteratorF_U3CU3E__Finally1_m1302743386 (U3CWrapEnumeratorU3Ec__IteratorF_t3900889545 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
