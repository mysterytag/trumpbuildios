﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.ClientModels.StartPurchaseRequest
struct StartPurchaseRequest_t2105128380;
// System.String
struct String_t;
// System.Collections.Generic.List`1<PlayFab.ClientModels.ItemPurchaseRequest>
struct List_1_t1528421284;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String968488902.h"

// System.Void PlayFab.ClientModels.StartPurchaseRequest::.ctor()
extern "C"  void StartPurchaseRequest__ctor_m1165822209 (StartPurchaseRequest_t2105128380 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PlayFab.ClientModels.StartPurchaseRequest::get_CatalogVersion()
extern "C"  String_t* StartPurchaseRequest_get_CatalogVersion_m702756520 (StartPurchaseRequest_t2105128380 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.StartPurchaseRequest::set_CatalogVersion(System.String)
extern "C"  void StartPurchaseRequest_set_CatalogVersion_m3876930499 (StartPurchaseRequest_t2105128380 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PlayFab.ClientModels.StartPurchaseRequest::get_StoreId()
extern "C"  String_t* StartPurchaseRequest_get_StoreId_m688693749 (StartPurchaseRequest_t2105128380 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.StartPurchaseRequest::set_StoreId(System.String)
extern "C"  void StartPurchaseRequest_set_StoreId_m3689463012 (StartPurchaseRequest_t2105128380 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<PlayFab.ClientModels.ItemPurchaseRequest> PlayFab.ClientModels.StartPurchaseRequest::get_Items()
extern "C"  List_1_t1528421284 * StartPurchaseRequest_get_Items_m4058324070 (StartPurchaseRequest_t2105128380 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.StartPurchaseRequest::set_Items(System.Collections.Generic.List`1<PlayFab.ClientModels.ItemPurchaseRequest>)
extern "C"  void StartPurchaseRequest_set_Items_m2441049229 (StartPurchaseRequest_t2105128380 * __this, List_1_t1528421284 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
