﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// TableButtons/<RegisterDonateButtons>c__AnonStorey169
struct U3CRegisterDonateButtonsU3Ec__AnonStorey169_t2690655000;
// SettingsCell
struct SettingsCell_t698172485;
// DonateCell
struct DonateCell_t2798474385;
// <>__AnonType7`2<System.Boolean,System.Boolean>
struct U3CU3E__AnonType7_2_t554355766;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_SettingsCell698172485.h"
#include "AssemblyU2DCSharp_DonateCell2798474385.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Unit2558286038.h"

// System.Void TableButtons/<RegisterDonateButtons>c__AnonStorey169::.ctor()
extern "C"  void U3CRegisterDonateButtonsU3Ec__AnonStorey169__ctor_m3999726879 (U3CRegisterDonateButtonsU3Ec__AnonStorey169_t2690655000 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TableButtons/<RegisterDonateButtons>c__AnonStorey169::<>m__23B()
extern "C"  void U3CRegisterDonateButtonsU3Ec__AnonStorey169_U3CU3Em__23B_m3416559771 (U3CRegisterDonateButtonsU3Ec__AnonStorey169_t2690655000 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TableButtons/<RegisterDonateButtons>c__AnonStorey169::<>m__23C()
extern "C"  void U3CRegisterDonateButtonsU3Ec__AnonStorey169_U3CU3Em__23C_m3416560732 (U3CRegisterDonateButtonsU3Ec__AnonStorey169_t2690655000 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TableButtons/<RegisterDonateButtons>c__AnonStorey169::<>m__23D()
extern "C"  void U3CRegisterDonateButtonsU3Ec__AnonStorey169_U3CU3Em__23D_m3416561693 (U3CRegisterDonateButtonsU3Ec__AnonStorey169_t2690655000 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TableButtons/<RegisterDonateButtons>c__AnonStorey169::<>m__23E()
extern "C"  void U3CRegisterDonateButtonsU3Ec__AnonStorey169_U3CU3Em__23E_m3416562654 (U3CRegisterDonateButtonsU3Ec__AnonStorey169_t2690655000 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TableButtons/<RegisterDonateButtons>c__AnonStorey169::<>m__23F()
extern "C"  void U3CRegisterDonateButtonsU3Ec__AnonStorey169_U3CU3Em__23F_m3416563615 (U3CRegisterDonateButtonsU3Ec__AnonStorey169_t2690655000 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TableButtons/<RegisterDonateButtons>c__AnonStorey169::<>m__240(SettingsCell)
extern "C"  void U3CRegisterDonateButtonsU3Ec__AnonStorey169_U3CU3Em__240_m4191493955 (U3CRegisterDonateButtonsU3Ec__AnonStorey169_t2690655000 * __this, SettingsCell_t698172485 * ___cell0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TableButtons/<RegisterDonateButtons>c__AnonStorey169::<>m__241(SettingsCell)
extern "C"  void U3CRegisterDonateButtonsU3Ec__AnonStorey169_U3CU3Em__241_m1404078468 (U3CRegisterDonateButtonsU3Ec__AnonStorey169_t2690655000 * __this, SettingsCell_t698172485 * ___cell0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TableButtons/<RegisterDonateButtons>c__AnonStorey169::<>m__242(SettingsCell)
extern "C"  void U3CRegisterDonateButtonsU3Ec__AnonStorey169_U3CU3Em__242_m2911630277 (U3CRegisterDonateButtonsU3Ec__AnonStorey169_t2690655000 * __this, SettingsCell_t698172485 * ___cell0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TableButtons/<RegisterDonateButtons>c__AnonStorey169::<>m__243(SettingsCell)
extern "C"  void U3CRegisterDonateButtonsU3Ec__AnonStorey169_U3CU3Em__243_m124214790 (U3CRegisterDonateButtonsU3Ec__AnonStorey169_t2690655000 * __this, SettingsCell_t698172485 * ___cell0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TableButtons/<RegisterDonateButtons>c__AnonStorey169::<>m__244(SettingsCell)
extern "C"  void U3CRegisterDonateButtonsU3Ec__AnonStorey169_U3CU3Em__244_m1631766599 (U3CRegisterDonateButtonsU3Ec__AnonStorey169_t2690655000 * __this, SettingsCell_t698172485 * ___cell0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TableButtons/<RegisterDonateButtons>c__AnonStorey169::<>m__245(SettingsCell)
extern "C"  void U3CRegisterDonateButtonsU3Ec__AnonStorey169_U3CU3Em__245_m3139318408 (U3CRegisterDonateButtonsU3Ec__AnonStorey169_t2690655000 * __this, SettingsCell_t698172485 * ___cell0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TableButtons/<RegisterDonateButtons>c__AnonStorey169::<>m__246(SettingsCell)
extern "C"  void U3CRegisterDonateButtonsU3Ec__AnonStorey169_U3CU3Em__246_m351902921 (U3CRegisterDonateButtonsU3Ec__AnonStorey169_t2690655000 * __this, SettingsCell_t698172485 * ___cell0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TableButtons/<RegisterDonateButtons>c__AnonStorey169::<>m__247(SettingsCell)
extern "C"  void U3CRegisterDonateButtonsU3Ec__AnonStorey169_U3CU3Em__247_m1859454730 (U3CRegisterDonateButtonsU3Ec__AnonStorey169_t2690655000 * __this, SettingsCell_t698172485 * ___cell0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TableButtons/<RegisterDonateButtons>c__AnonStorey169::<>m__248(DonateCell)
extern "C"  void U3CRegisterDonateButtonsU3Ec__AnonStorey169_U3CU3Em__248_m1151660095 (U3CRegisterDonateButtonsU3Ec__AnonStorey169_t2690655000 * __this, DonateCell_t2798474385 * ___cell0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TableButtons/<RegisterDonateButtons>c__AnonStorey169::<>m__249(DonateCell)
extern "C"  void U3CRegisterDonateButtonsU3Ec__AnonStorey169_U3CU3Em__249_m858257088 (U3CRegisterDonateButtonsU3Ec__AnonStorey169_t2690655000 * __this, DonateCell_t2798474385 * ___cell0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TableButtons/<RegisterDonateButtons>c__AnonStorey169::<>m__24A(DonateCell)
extern "C"  void U3CRegisterDonateButtonsU3Ec__AnonStorey169_U3CU3Em__24A_m2806000328 (U3CRegisterDonateButtonsU3Ec__AnonStorey169_t2690655000 * __this, DonateCell_t2798474385 * ___cell0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TableButtons/<RegisterDonateButtons>c__AnonStorey169::<>m__24B(DonateCell)
extern "C"  void U3CRegisterDonateButtonsU3Ec__AnonStorey169_U3CU3Em__24B_m2512597321 (U3CRegisterDonateButtonsU3Ec__AnonStorey169_t2690655000 * __this, DonateCell_t2798474385 * ___cell0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TableButtons/<RegisterDonateButtons>c__AnonStorey169::<>m__24C(DonateCell)
extern "C"  void U3CRegisterDonateButtonsU3Ec__AnonStorey169_U3CU3Em__24C_m2219194314 (U3CRegisterDonateButtonsU3Ec__AnonStorey169_t2690655000 * __this, DonateCell_t2798474385 * ___cell0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TableButtons/<RegisterDonateButtons>c__AnonStorey169::<>m__24F(SettingsCell)
extern "C"  void U3CRegisterDonateButtonsU3Ec__AnonStorey169_U3CU3Em__24F_m2997895385 (U3CRegisterDonateButtonsU3Ec__AnonStorey169_t2690655000 * __this, SettingsCell_t698172485 * ___cell0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TableButtons/<RegisterDonateButtons>c__AnonStorey169::<>m__250(SettingsCell)
extern "C"  void U3CRegisterDonateButtonsU3Ec__AnonStorey169_U3CU3Em__250_m3680959778 (U3CRegisterDonateButtonsU3Ec__AnonStorey169_t2690655000 * __this, SettingsCell_t698172485 * ___cell0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TableButtons/<RegisterDonateButtons>c__AnonStorey169::<>m__251(SettingsCell)
extern "C"  void U3CRegisterDonateButtonsU3Ec__AnonStorey169_U3CU3Em__251_m893544291 (U3CRegisterDonateButtonsU3Ec__AnonStorey169_t2690655000 * __this, SettingsCell_t698172485 * ___cell0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TableButtons/<RegisterDonateButtons>c__AnonStorey169::<>m__252(SettingsCell)
extern "C"  void U3CRegisterDonateButtonsU3Ec__AnonStorey169_U3CU3Em__252_m2401096100 (U3CRegisterDonateButtonsU3Ec__AnonStorey169_t2690655000 * __this, SettingsCell_t698172485 * ___cell0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// <>__AnonType7`2<System.Boolean,System.Boolean> TableButtons/<RegisterDonateButtons>c__AnonStorey169::<>m__268(System.Boolean,System.Boolean)
extern "C"  U3CU3E__AnonType7_2_t554355766 * U3CRegisterDonateButtonsU3Ec__AnonStorey169_U3CU3Em__268_m213269448 (Il2CppObject * __this /* static, unused */, bool ___reward0, bool ___loggedIn1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// <>__AnonType7`2<System.Boolean,System.Boolean> TableButtons/<RegisterDonateButtons>c__AnonStorey169::<>m__26D(System.Boolean,System.Boolean)
extern "C"  U3CU3E__AnonType7_2_t554355766 * U3CRegisterDonateButtonsU3Ec__AnonStorey169_U3CU3Em__26D_m1861827132 (Il2CppObject * __this /* static, unused */, bool ___reward0, bool ___loggedIn1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TableButtons/<RegisterDonateButtons>c__AnonStorey169::<>m__270(UniRx.Unit)
extern "C"  void U3CRegisterDonateButtonsU3Ec__AnonStorey169_U3CU3Em__270_m3762606505 (Il2CppObject * __this /* static, unused */, Unit_t2558286038  ___unit0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TableButtons/<RegisterDonateButtons>c__AnonStorey169::<>m__271(UniRx.Unit)
extern "C"  void U3CRegisterDonateButtonsU3Ec__AnonStorey169_U3CU3Em__271_m3469203498 (U3CRegisterDonateButtonsU3Ec__AnonStorey169_t2690655000 * __this, Unit_t2558286038  ___unit0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TableButtons/<RegisterDonateButtons>c__AnonStorey169::<>m__272(UniRx.Unit)
extern "C"  void U3CRegisterDonateButtonsU3Ec__AnonStorey169_U3CU3Em__272_m3175800491 (U3CRegisterDonateButtonsU3Ec__AnonStorey169_t2690655000 * __this, Unit_t2558286038  ____0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TableButtons/<RegisterDonateButtons>c__AnonStorey169::<>m__273(UniRx.Unit)
extern "C"  void U3CRegisterDonateButtonsU3Ec__AnonStorey169_U3CU3Em__273_m2882397484 (U3CRegisterDonateButtonsU3Ec__AnonStorey169_t2690655000 * __this, Unit_t2558286038  ____0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TableButtons/<RegisterDonateButtons>c__AnonStorey169::<>m__274(UniRx.Unit)
extern "C"  void U3CRegisterDonateButtonsU3Ec__AnonStorey169_U3CU3Em__274_m2588994477 (U3CRegisterDonateButtonsU3Ec__AnonStorey169_t2690655000 * __this, Unit_t2558286038  ___unit0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
