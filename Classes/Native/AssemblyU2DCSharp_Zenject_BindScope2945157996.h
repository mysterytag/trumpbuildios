﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Zenject.DiContainer
struct DiContainer_t2383114449;
// System.Collections.Generic.List`1<Zenject.ProviderBase>
struct List_1_t2424453360;
// Zenject.SingletonProviderMap
struct SingletonProviderMap_t1557411893;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.BindScope
struct  BindScope_t2945157996  : public Il2CppObject
{
public:
	// Zenject.DiContainer Zenject.BindScope::_container
	DiContainer_t2383114449 * ____container_0;
	// System.Collections.Generic.List`1<Zenject.ProviderBase> Zenject.BindScope::_scopedProviders
	List_1_t2424453360 * ____scopedProviders_1;
	// Zenject.SingletonProviderMap Zenject.BindScope::_singletonMap
	SingletonProviderMap_t1557411893 * ____singletonMap_2;

public:
	inline static int32_t get_offset_of__container_0() { return static_cast<int32_t>(offsetof(BindScope_t2945157996, ____container_0)); }
	inline DiContainer_t2383114449 * get__container_0() const { return ____container_0; }
	inline DiContainer_t2383114449 ** get_address_of__container_0() { return &____container_0; }
	inline void set__container_0(DiContainer_t2383114449 * value)
	{
		____container_0 = value;
		Il2CppCodeGenWriteBarrier(&____container_0, value);
	}

	inline static int32_t get_offset_of__scopedProviders_1() { return static_cast<int32_t>(offsetof(BindScope_t2945157996, ____scopedProviders_1)); }
	inline List_1_t2424453360 * get__scopedProviders_1() const { return ____scopedProviders_1; }
	inline List_1_t2424453360 ** get_address_of__scopedProviders_1() { return &____scopedProviders_1; }
	inline void set__scopedProviders_1(List_1_t2424453360 * value)
	{
		____scopedProviders_1 = value;
		Il2CppCodeGenWriteBarrier(&____scopedProviders_1, value);
	}

	inline static int32_t get_offset_of__singletonMap_2() { return static_cast<int32_t>(offsetof(BindScope_t2945157996, ____singletonMap_2)); }
	inline SingletonProviderMap_t1557411893 * get__singletonMap_2() const { return ____singletonMap_2; }
	inline SingletonProviderMap_t1557411893 ** get_address_of__singletonMap_2() { return &____singletonMap_2; }
	inline void set__singletonMap_2(SingletonProviderMap_t1557411893 * value)
	{
		____singletonMap_2 = value;
		Il2CppCodeGenWriteBarrier(&____singletonMap_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
