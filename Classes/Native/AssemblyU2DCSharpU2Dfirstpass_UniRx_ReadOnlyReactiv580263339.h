﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UniRx.ReadOnlyReactiveProperty`1<UnityEngine.Color>
struct ReadOnlyReactiveProperty_1_t82610480;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.ReadOnlyReactiveProperty`1/ReadOnlyReactivePropertyObserver<UnityEngine.Color>
struct  ReadOnlyReactivePropertyObserver_t580263339  : public Il2CppObject
{
public:
	// UniRx.ReadOnlyReactiveProperty`1<T> UniRx.ReadOnlyReactiveProperty`1/ReadOnlyReactivePropertyObserver::parent
	ReadOnlyReactiveProperty_1_t82610480 * ___parent_0;
	// System.Int32 UniRx.ReadOnlyReactiveProperty`1/ReadOnlyReactivePropertyObserver::isStopped
	int32_t ___isStopped_1;

public:
	inline static int32_t get_offset_of_parent_0() { return static_cast<int32_t>(offsetof(ReadOnlyReactivePropertyObserver_t580263339, ___parent_0)); }
	inline ReadOnlyReactiveProperty_1_t82610480 * get_parent_0() const { return ___parent_0; }
	inline ReadOnlyReactiveProperty_1_t82610480 ** get_address_of_parent_0() { return &___parent_0; }
	inline void set_parent_0(ReadOnlyReactiveProperty_1_t82610480 * value)
	{
		___parent_0 = value;
		Il2CppCodeGenWriteBarrier(&___parent_0, value);
	}

	inline static int32_t get_offset_of_isStopped_1() { return static_cast<int32_t>(offsetof(ReadOnlyReactivePropertyObserver_t580263339, ___isStopped_1)); }
	inline int32_t get_isStopped_1() const { return ___isStopped_1; }
	inline int32_t* get_address_of_isStopped_1() { return &___isStopped_1; }
	inline void set_isStopped_1(int32_t value)
	{
		___isStopped_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
