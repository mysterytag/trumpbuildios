﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// FiniteTimeProc
struct FiniteTimeProc_t2933053874;

#include "AssemblyU2DCSharp_FiniteTimeProc2933053874.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TimeScalerBase
struct  TimeScalerBase_t3755656742  : public FiniteTimeProc_t2933053874
{
public:
	// FiniteTimeProc TimeScalerBase::proc
	FiniteTimeProc_t2933053874 * ___proc_2;

public:
	inline static int32_t get_offset_of_proc_2() { return static_cast<int32_t>(offsetof(TimeScalerBase_t3755656742, ___proc_2)); }
	inline FiniteTimeProc_t2933053874 * get_proc_2() const { return ___proc_2; }
	inline FiniteTimeProc_t2933053874 ** get_address_of_proc_2() { return &___proc_2; }
	inline void set_proc_2(FiniteTimeProc_t2933053874 * value)
	{
		___proc_2 = value;
		Il2CppCodeGenWriteBarrier(&___proc_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
