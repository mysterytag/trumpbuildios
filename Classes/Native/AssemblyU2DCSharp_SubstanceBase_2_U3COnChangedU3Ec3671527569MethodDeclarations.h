﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SubstanceBase`2/<OnChanged>c__AnonStorey142<System.Single,System.Object>
struct U3COnChangedU3Ec__AnonStorey142_t3671527569;

#include "codegen/il2cpp-codegen.h"

// System.Void SubstanceBase`2/<OnChanged>c__AnonStorey142<System.Single,System.Object>::.ctor()
extern "C"  void U3COnChangedU3Ec__AnonStorey142__ctor_m408260331_gshared (U3COnChangedU3Ec__AnonStorey142_t3671527569 * __this, const MethodInfo* method);
#define U3COnChangedU3Ec__AnonStorey142__ctor_m408260331(__this, method) ((  void (*) (U3COnChangedU3Ec__AnonStorey142_t3671527569 *, const MethodInfo*))U3COnChangedU3Ec__AnonStorey142__ctor_m408260331_gshared)(__this, method)
// System.Void SubstanceBase`2/<OnChanged>c__AnonStorey142<System.Single,System.Object>::<>m__1D8(T)
extern "C"  void U3COnChangedU3Ec__AnonStorey142_U3CU3Em__1D8_m832339755_gshared (U3COnChangedU3Ec__AnonStorey142_t3671527569 * __this, float ____0, const MethodInfo* method);
#define U3COnChangedU3Ec__AnonStorey142_U3CU3Em__1D8_m832339755(__this, ____0, method) ((  void (*) (U3COnChangedU3Ec__AnonStorey142_t3671527569 *, float, const MethodInfo*))U3COnChangedU3Ec__AnonStorey142_U3CU3Em__1D8_m832339755_gshared)(__this, ____0, method)
