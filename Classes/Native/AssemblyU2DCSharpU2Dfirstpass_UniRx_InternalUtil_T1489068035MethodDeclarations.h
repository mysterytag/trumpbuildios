﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.InternalUtil.ThrowObserver`1<System.Object>
struct ThrowObserver_1_t1489068035;
// System.Exception
struct Exception_t1967233988;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Exception1967233988.h"
#include "mscorlib_System_Object837106420.h"

// System.Void UniRx.InternalUtil.ThrowObserver`1<System.Object>::.ctor()
extern "C"  void ThrowObserver_1__ctor_m780643377_gshared (ThrowObserver_1_t1489068035 * __this, const MethodInfo* method);
#define ThrowObserver_1__ctor_m780643377(__this, method) ((  void (*) (ThrowObserver_1_t1489068035 *, const MethodInfo*))ThrowObserver_1__ctor_m780643377_gshared)(__this, method)
// System.Void UniRx.InternalUtil.ThrowObserver`1<System.Object>::.cctor()
extern "C"  void ThrowObserver_1__cctor_m2243011996_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define ThrowObserver_1__cctor_m2243011996(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))ThrowObserver_1__cctor_m2243011996_gshared)(__this /* static, unused */, method)
// System.Void UniRx.InternalUtil.ThrowObserver`1<System.Object>::OnCompleted()
extern "C"  void ThrowObserver_1_OnCompleted_m773011195_gshared (ThrowObserver_1_t1489068035 * __this, const MethodInfo* method);
#define ThrowObserver_1_OnCompleted_m773011195(__this, method) ((  void (*) (ThrowObserver_1_t1489068035 *, const MethodInfo*))ThrowObserver_1_OnCompleted_m773011195_gshared)(__this, method)
// System.Void UniRx.InternalUtil.ThrowObserver`1<System.Object>::OnError(System.Exception)
extern "C"  void ThrowObserver_1_OnError_m1142179752_gshared (ThrowObserver_1_t1489068035 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method);
#define ThrowObserver_1_OnError_m1142179752(__this, ___error0, method) ((  void (*) (ThrowObserver_1_t1489068035 *, Exception_t1967233988 *, const MethodInfo*))ThrowObserver_1_OnError_m1142179752_gshared)(__this, ___error0, method)
// System.Void UniRx.InternalUtil.ThrowObserver`1<System.Object>::OnNext(T)
extern "C"  void ThrowObserver_1_OnNext_m1409711769_gshared (ThrowObserver_1_t1489068035 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define ThrowObserver_1_OnNext_m1409711769(__this, ___value0, method) ((  void (*) (ThrowObserver_1_t1489068035 *, Il2CppObject *, const MethodInfo*))ThrowObserver_1_OnNext_m1409711769_gshared)(__this, ___value0, method)
