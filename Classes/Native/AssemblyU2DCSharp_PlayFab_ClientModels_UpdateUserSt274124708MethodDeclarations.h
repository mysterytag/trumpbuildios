﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.ClientModels.UpdateUserStatisticsResult
struct UpdateUserStatisticsResult_t274124708;

#include "codegen/il2cpp-codegen.h"

// System.Void PlayFab.ClientModels.UpdateUserStatisticsResult::.ctor()
extern "C"  void UpdateUserStatisticsResult__ctor_m412609689 (UpdateUserStatisticsResult_t274124708 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
