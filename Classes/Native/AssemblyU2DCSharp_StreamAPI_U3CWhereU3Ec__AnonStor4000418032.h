﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// IEmptyStream
struct IEmptyStream_t3684082468;
// System.Func`1<System.Boolean>
struct Func_1_t1353786588;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// StreamAPI/<Where>c__AnonStorey12E
struct  U3CWhereU3Ec__AnonStorey12E_t4000418032  : public Il2CppObject
{
public:
	// IEmptyStream StreamAPI/<Where>c__AnonStorey12E::stream
	Il2CppObject * ___stream_0;
	// System.Func`1<System.Boolean> StreamAPI/<Where>c__AnonStorey12E::o
	Func_1_t1353786588 * ___o_1;

public:
	inline static int32_t get_offset_of_stream_0() { return static_cast<int32_t>(offsetof(U3CWhereU3Ec__AnonStorey12E_t4000418032, ___stream_0)); }
	inline Il2CppObject * get_stream_0() const { return ___stream_0; }
	inline Il2CppObject ** get_address_of_stream_0() { return &___stream_0; }
	inline void set_stream_0(Il2CppObject * value)
	{
		___stream_0 = value;
		Il2CppCodeGenWriteBarrier(&___stream_0, value);
	}

	inline static int32_t get_offset_of_o_1() { return static_cast<int32_t>(offsetof(U3CWhereU3Ec__AnonStorey12E_t4000418032, ___o_1)); }
	inline Func_1_t1353786588 * get_o_1() const { return ___o_1; }
	inline Func_1_t1353786588 ** get_address_of_o_1() { return &___o_1; }
	inline void set_o_1(Func_1_t1353786588 * value)
	{
		___o_1 = value;
		Il2CppCodeGenWriteBarrier(&___o_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
