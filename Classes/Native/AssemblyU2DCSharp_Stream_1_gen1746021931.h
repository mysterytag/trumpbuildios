﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Reactor`1<UniRx.CollectionAddEvent`1<UpgradeButton>>
struct Reactor_1_t2314687805;
// System.IDisposable
struct IDisposable_t1628921374;
// System.Collections.Generic.List`1<UniRx.CollectionAddEvent`1<UpgradeButton>>
struct List_1_t3851841878;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Stream`1<UniRx.CollectionAddEvent`1<UpgradeButton>>
struct  Stream_1_t1746021931  : public Il2CppObject
{
public:
	// Reactor`1<T> Stream`1::observer
	Reactor_1_t2314687805 * ___observer_0;
	// System.IDisposable Stream`1::inputStreamConnection
	Il2CppObject * ___inputStreamConnection_1;
	// System.Collections.Generic.List`1<T> Stream`1::holdedValues
	List_1_t3851841878 * ___holdedValues_2;
	// System.Boolean Stream`1::sendOnceInTransaction
	bool ___sendOnceInTransaction_3;
	// System.Boolean Stream`1::autoDisconnectAfterEvent
	bool ___autoDisconnectAfterEvent_4;

public:
	inline static int32_t get_offset_of_observer_0() { return static_cast<int32_t>(offsetof(Stream_1_t1746021931, ___observer_0)); }
	inline Reactor_1_t2314687805 * get_observer_0() const { return ___observer_0; }
	inline Reactor_1_t2314687805 ** get_address_of_observer_0() { return &___observer_0; }
	inline void set_observer_0(Reactor_1_t2314687805 * value)
	{
		___observer_0 = value;
		Il2CppCodeGenWriteBarrier(&___observer_0, value);
	}

	inline static int32_t get_offset_of_inputStreamConnection_1() { return static_cast<int32_t>(offsetof(Stream_1_t1746021931, ___inputStreamConnection_1)); }
	inline Il2CppObject * get_inputStreamConnection_1() const { return ___inputStreamConnection_1; }
	inline Il2CppObject ** get_address_of_inputStreamConnection_1() { return &___inputStreamConnection_1; }
	inline void set_inputStreamConnection_1(Il2CppObject * value)
	{
		___inputStreamConnection_1 = value;
		Il2CppCodeGenWriteBarrier(&___inputStreamConnection_1, value);
	}

	inline static int32_t get_offset_of_holdedValues_2() { return static_cast<int32_t>(offsetof(Stream_1_t1746021931, ___holdedValues_2)); }
	inline List_1_t3851841878 * get_holdedValues_2() const { return ___holdedValues_2; }
	inline List_1_t3851841878 ** get_address_of_holdedValues_2() { return &___holdedValues_2; }
	inline void set_holdedValues_2(List_1_t3851841878 * value)
	{
		___holdedValues_2 = value;
		Il2CppCodeGenWriteBarrier(&___holdedValues_2, value);
	}

	inline static int32_t get_offset_of_sendOnceInTransaction_3() { return static_cast<int32_t>(offsetof(Stream_1_t1746021931, ___sendOnceInTransaction_3)); }
	inline bool get_sendOnceInTransaction_3() const { return ___sendOnceInTransaction_3; }
	inline bool* get_address_of_sendOnceInTransaction_3() { return &___sendOnceInTransaction_3; }
	inline void set_sendOnceInTransaction_3(bool value)
	{
		___sendOnceInTransaction_3 = value;
	}

	inline static int32_t get_offset_of_autoDisconnectAfterEvent_4() { return static_cast<int32_t>(offsetof(Stream_1_t1746021931, ___autoDisconnectAfterEvent_4)); }
	inline bool get_autoDisconnectAfterEvent_4() const { return ___autoDisconnectAfterEvent_4; }
	inline bool* get_address_of_autoDisconnectAfterEvent_4() { return &___autoDisconnectAfterEvent_4; }
	inline void set_autoDisconnectAfterEvent_4(bool value)
	{
		___autoDisconnectAfterEvent_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
