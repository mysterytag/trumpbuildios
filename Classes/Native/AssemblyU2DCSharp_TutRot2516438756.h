﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// OligarchParameters
struct OligarchParameters_t821144443;
// UnityEngine.GameObject
struct GameObject_t4012695102;

#include "UnityEngine_UnityEngine_MonoBehaviour3012272455.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TutRot
struct  TutRot_t2516438756  : public MonoBehaviour_t3012272455
{
public:
	// OligarchParameters TutRot::OligarchParameters
	OligarchParameters_t821144443 * ___OligarchParameters_2;
	// UnityEngine.GameObject TutRot::hand
	GameObject_t4012695102 * ___hand_3;
	// UnityEngine.GameObject TutRot::back
	GameObject_t4012695102 * ___back_4;
	// UnityEngine.GameObject TutRot::text
	GameObject_t4012695102 * ___text_5;

public:
	inline static int32_t get_offset_of_OligarchParameters_2() { return static_cast<int32_t>(offsetof(TutRot_t2516438756, ___OligarchParameters_2)); }
	inline OligarchParameters_t821144443 * get_OligarchParameters_2() const { return ___OligarchParameters_2; }
	inline OligarchParameters_t821144443 ** get_address_of_OligarchParameters_2() { return &___OligarchParameters_2; }
	inline void set_OligarchParameters_2(OligarchParameters_t821144443 * value)
	{
		___OligarchParameters_2 = value;
		Il2CppCodeGenWriteBarrier(&___OligarchParameters_2, value);
	}

	inline static int32_t get_offset_of_hand_3() { return static_cast<int32_t>(offsetof(TutRot_t2516438756, ___hand_3)); }
	inline GameObject_t4012695102 * get_hand_3() const { return ___hand_3; }
	inline GameObject_t4012695102 ** get_address_of_hand_3() { return &___hand_3; }
	inline void set_hand_3(GameObject_t4012695102 * value)
	{
		___hand_3 = value;
		Il2CppCodeGenWriteBarrier(&___hand_3, value);
	}

	inline static int32_t get_offset_of_back_4() { return static_cast<int32_t>(offsetof(TutRot_t2516438756, ___back_4)); }
	inline GameObject_t4012695102 * get_back_4() const { return ___back_4; }
	inline GameObject_t4012695102 ** get_address_of_back_4() { return &___back_4; }
	inline void set_back_4(GameObject_t4012695102 * value)
	{
		___back_4 = value;
		Il2CppCodeGenWriteBarrier(&___back_4, value);
	}

	inline static int32_t get_offset_of_text_5() { return static_cast<int32_t>(offsetof(TutRot_t2516438756, ___text_5)); }
	inline GameObject_t4012695102 * get_text_5() const { return ___text_5; }
	inline GameObject_t4012695102 ** get_address_of_text_5() { return &___text_5; }
	inline void set_text_5(GameObject_t4012695102 * value)
	{
		___text_5 = value;
		Il2CppCodeGenWriteBarrier(&___text_5, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
