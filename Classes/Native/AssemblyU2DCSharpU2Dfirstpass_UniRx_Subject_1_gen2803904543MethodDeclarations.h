﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Subject_1_gen2775141040MethodDeclarations.h"

// System.Void UniRx.Subject`1<UniRx.Triggers.ObservableStateMachineTrigger/OnStateMachineInfo>::.ctor()
#define Subject_1__ctor_m2321925864(__this, method) ((  void (*) (Subject_1_t2803904543 *, const MethodInfo*))Subject_1__ctor_m3752525464_gshared)(__this, method)
// System.Boolean UniRx.Subject`1<UniRx.Triggers.ObservableStateMachineTrigger/OnStateMachineInfo>::get_HasObservers()
#define Subject_1_get_HasObservers_m3761456804(__this, method) ((  bool (*) (Subject_1_t2803904543 *, const MethodInfo*))Subject_1_get_HasObservers_m2673131508_gshared)(__this, method)
// System.Void UniRx.Subject`1<UniRx.Triggers.ObservableStateMachineTrigger/OnStateMachineInfo>::OnCompleted()
#define Subject_1_OnCompleted_m3725208690(__this, method) ((  void (*) (Subject_1_t2803904543 *, const MethodInfo*))Subject_1_OnCompleted_m1083367458_gshared)(__this, method)
// System.Void UniRx.Subject`1<UniRx.Triggers.ObservableStateMachineTrigger/OnStateMachineInfo>::OnError(System.Exception)
#define Subject_1_OnError_m4146840223(__this, ___error0, method) ((  void (*) (Subject_1_t2803904543 *, Exception_t1967233988 *, const MethodInfo*))Subject_1_OnError_m1700032079_gshared)(__this, ___error0, method)
// System.Void UniRx.Subject`1<UniRx.Triggers.ObservableStateMachineTrigger/OnStateMachineInfo>::OnNext(T)
#define Subject_1_OnNext_m818464656(__this, ___value0, method) ((  void (*) (Subject_1_t2803904543 *, OnStateMachineInfo_t865869923 *, const MethodInfo*))Subject_1_OnNext_m1235145536_gshared)(__this, ___value0, method)
// System.IDisposable UniRx.Subject`1<UniRx.Triggers.ObservableStateMachineTrigger/OnStateMachineInfo>::Subscribe(UniRx.IObserver`1<T>)
#define Subject_1_Subscribe_m937362855(__this, ___observer0, method) ((  Il2CppObject * (*) (Subject_1_t2803904543 *, Il2CppObject*, const MethodInfo*))Subject_1_Subscribe_m2428743831_gshared)(__this, ___observer0, method)
// System.Void UniRx.Subject`1<UniRx.Triggers.ObservableStateMachineTrigger/OnStateMachineInfo>::Dispose()
#define Subject_1_Dispose_m2181011749(__this, method) ((  void (*) (Subject_1_t2803904543 *, const MethodInfo*))Subject_1_Dispose_m2597692629_gshared)(__this, method)
// System.Void UniRx.Subject`1<UniRx.Triggers.ObservableStateMachineTrigger/OnStateMachineInfo>::ThrowIfDisposed()
#define Subject_1_ThrowIfDisposed_m1985498542(__this, method) ((  void (*) (Subject_1_t2803904543 *, const MethodInfo*))Subject_1_ThrowIfDisposed_m956279134_gshared)(__this, method)
// System.Boolean UniRx.Subject`1<UniRx.Triggers.ObservableStateMachineTrigger/OnStateMachineInfo>::IsRequiredSubscribeOnCurrentThread()
#define Subject_1_IsRequiredSubscribeOnCurrentThread_m2252395163(__this, method) ((  bool (*) (Subject_1_t2803904543 *, const MethodInfo*))Subject_1_IsRequiredSubscribeOnCurrentThread_m3969161195_gshared)(__this, method)
