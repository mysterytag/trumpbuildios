﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ZergRush.InorganicCollection`1<System.Object>
struct InorganicCollection_1_t3450977518;
// System.Collections.Generic.IEnumerable`1<System.Object>
struct IEnumerable_1_t3709260776;
// System.Collections.Generic.List`1<System.Object>
struct List_1_t1634065389;
// System.Object
struct Il2CppObject;
// System.Func`2<System.Object,System.Boolean>
struct Func_2_t1509682273;
// IStream`1<System.Int32>
struct IStream_1_t3400105778;
// IStream`1<UniRx.Unit>
struct IStream_1_t3110977029;
// IStream`1<UniRx.CollectionAddEvent`1<System.Object>>
struct IStream_1_t2969212978;
// IStream`1<CollectionMoveEvent`1<System.Object>>
struct IStream_1_t4124746372;
// IStream`1<CollectionRemoveEvent`1<System.Object>>
struct IStream_1_t1450950249;
// IStream`1<CollectionReplaceEvent`1<System.Object>>
struct IStream_1_t3050063061;
// System.IDisposable
struct IDisposable_t1628921374;
// System.Action
struct Action_t437523947;
// System.Action`1<System.Collections.Generic.ICollection`1<System.Object>>
struct Action_1_t1451390511;
// System.Collections.Generic.ICollection`1<System.Object>
struct ICollection_1_t1302937806;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "System_Core_System_Action437523947.h"
#include "AssemblyU2DCSharp_Priority3194150340.h"

// System.Void ZergRush.InorganicCollection`1<System.Object>::.ctor()
extern "C"  void InorganicCollection_1__ctor_m2562722286_gshared (InorganicCollection_1_t3450977518 * __this, const MethodInfo* method);
#define InorganicCollection_1__ctor_m2562722286(__this, method) ((  void (*) (InorganicCollection_1_t3450977518 *, const MethodInfo*))InorganicCollection_1__ctor_m2562722286_gshared)(__this, method)
// System.Void ZergRush.InorganicCollection`1<System.Object>::.ctor(System.Collections.Generic.IEnumerable`1<T>)
extern "C"  void InorganicCollection_1__ctor_m3238279633_gshared (InorganicCollection_1_t3450977518 * __this, Il2CppObject* ___collection0, const MethodInfo* method);
#define InorganicCollection_1__ctor_m3238279633(__this, ___collection0, method) ((  void (*) (InorganicCollection_1_t3450977518 *, Il2CppObject*, const MethodInfo*))InorganicCollection_1__ctor_m3238279633_gshared)(__this, ___collection0, method)
// System.Void ZergRush.InorganicCollection`1<System.Object>::.ctor(System.Collections.Generic.List`1<T>)
extern "C"  void InorganicCollection_1__ctor_m1788080966_gshared (InorganicCollection_1_t3450977518 * __this, List_1_t1634065389 * ___list0, const MethodInfo* method);
#define InorganicCollection_1__ctor_m1788080966(__this, ___list0, method) ((  void (*) (InorganicCollection_1_t3450977518 *, List_1_t1634065389 *, const MethodInfo*))InorganicCollection_1__ctor_m1788080966_gshared)(__this, ___list0, method)
// System.Void ZergRush.InorganicCollection`1<System.Object>::ClearItems()
extern "C"  void InorganicCollection_1_ClearItems_m2583413385_gshared (InorganicCollection_1_t3450977518 * __this, const MethodInfo* method);
#define InorganicCollection_1_ClearItems_m2583413385(__this, method) ((  void (*) (InorganicCollection_1_t3450977518 *, const MethodInfo*))InorganicCollection_1_ClearItems_m2583413385_gshared)(__this, method)
// System.Void ZergRush.InorganicCollection`1<System.Object>::InsertItem(System.Int32,T)
extern "C"  void InorganicCollection_1_InsertItem_m1273645931_gshared (InorganicCollection_1_t3450977518 * __this, int32_t ___index0, Il2CppObject * ___item1, const MethodInfo* method);
#define InorganicCollection_1_InsertItem_m1273645931(__this, ___index0, ___item1, method) ((  void (*) (InorganicCollection_1_t3450977518 *, int32_t, Il2CppObject *, const MethodInfo*))InorganicCollection_1_InsertItem_m1273645931_gshared)(__this, ___index0, ___item1, method)
// System.Void ZergRush.InorganicCollection`1<System.Object>::Move(System.Int32,System.Int32)
extern "C"  void InorganicCollection_1_Move_m2620974047_gshared (InorganicCollection_1_t3450977518 * __this, int32_t ___oldIndex0, int32_t ___newIndex1, const MethodInfo* method);
#define InorganicCollection_1_Move_m2620974047(__this, ___oldIndex0, ___newIndex1, method) ((  void (*) (InorganicCollection_1_t3450977518 *, int32_t, int32_t, const MethodInfo*))InorganicCollection_1_Move_m2620974047_gshared)(__this, ___oldIndex0, ___newIndex1, method)
// System.Void ZergRush.InorganicCollection`1<System.Object>::MoveItem(System.Int32,System.Int32)
extern "C"  void InorganicCollection_1_MoveItem_m1781936076_gshared (InorganicCollection_1_t3450977518 * __this, int32_t ___oldIndex0, int32_t ___newIndex1, const MethodInfo* method);
#define InorganicCollection_1_MoveItem_m1781936076(__this, ___oldIndex0, ___newIndex1, method) ((  void (*) (InorganicCollection_1_t3450977518 *, int32_t, int32_t, const MethodInfo*))InorganicCollection_1_MoveItem_m1781936076_gshared)(__this, ___oldIndex0, ___newIndex1, method)
// System.Void ZergRush.InorganicCollection`1<System.Object>::RemoveItem(System.Int32)
extern "C"  void InorganicCollection_1_RemoveItem_m3410364318_gshared (InorganicCollection_1_t3450977518 * __this, int32_t ___index0, const MethodInfo* method);
#define InorganicCollection_1_RemoveItem_m3410364318(__this, ___index0, method) ((  void (*) (InorganicCollection_1_t3450977518 *, int32_t, const MethodInfo*))InorganicCollection_1_RemoveItem_m3410364318_gshared)(__this, ___index0, method)
// System.Void ZergRush.InorganicCollection`1<System.Object>::RemoveItem(System.Func`2<T,System.Boolean>)
extern "C"  void InorganicCollection_1_RemoveItem_m2366946799_gshared (InorganicCollection_1_t3450977518 * __this, Func_2_t1509682273 * ___filter0, const MethodInfo* method);
#define InorganicCollection_1_RemoveItem_m2366946799(__this, ___filter0, method) ((  void (*) (InorganicCollection_1_t3450977518 *, Func_2_t1509682273 *, const MethodInfo*))InorganicCollection_1_RemoveItem_m2366946799_gshared)(__this, ___filter0, method)
// System.Void ZergRush.InorganicCollection`1<System.Object>::SetItem(System.Int32,T)
extern "C"  void InorganicCollection_1_SetItem_m2680569482_gshared (InorganicCollection_1_t3450977518 * __this, int32_t ___index0, Il2CppObject * ___item1, const MethodInfo* method);
#define InorganicCollection_1_SetItem_m2680569482(__this, ___index0, ___item1, method) ((  void (*) (InorganicCollection_1_t3450977518 *, int32_t, Il2CppObject *, const MethodInfo*))InorganicCollection_1_SetItem_m2680569482_gshared)(__this, ___index0, ___item1, method)
// IStream`1<System.Int32> ZergRush.InorganicCollection`1<System.Object>::ObserveCountChanged()
extern "C"  Il2CppObject* InorganicCollection_1_ObserveCountChanged_m3361259667_gshared (InorganicCollection_1_t3450977518 * __this, const MethodInfo* method);
#define InorganicCollection_1_ObserveCountChanged_m3361259667(__this, method) ((  Il2CppObject* (*) (InorganicCollection_1_t3450977518 *, const MethodInfo*))InorganicCollection_1_ObserveCountChanged_m3361259667_gshared)(__this, method)
// IStream`1<UniRx.Unit> ZergRush.InorganicCollection`1<System.Object>::ObserveReset()
extern "C"  Il2CppObject* InorganicCollection_1_ObserveReset_m884902102_gshared (InorganicCollection_1_t3450977518 * __this, const MethodInfo* method);
#define InorganicCollection_1_ObserveReset_m884902102(__this, method) ((  Il2CppObject* (*) (InorganicCollection_1_t3450977518 *, const MethodInfo*))InorganicCollection_1_ObserveReset_m884902102_gshared)(__this, method)
// IStream`1<UniRx.CollectionAddEvent`1<T>> ZergRush.InorganicCollection`1<System.Object>::ObserveAdd()
extern "C"  Il2CppObject* InorganicCollection_1_ObserveAdd_m500474968_gshared (InorganicCollection_1_t3450977518 * __this, const MethodInfo* method);
#define InorganicCollection_1_ObserveAdd_m500474968(__this, method) ((  Il2CppObject* (*) (InorganicCollection_1_t3450977518 *, const MethodInfo*))InorganicCollection_1_ObserveAdd_m500474968_gshared)(__this, method)
// IStream`1<CollectionMoveEvent`1<T>> ZergRush.InorganicCollection`1<System.Object>::ObserveMove()
extern "C"  Il2CppObject* InorganicCollection_1_ObserveMove_m2797955546_gshared (InorganicCollection_1_t3450977518 * __this, const MethodInfo* method);
#define InorganicCollection_1_ObserveMove_m2797955546(__this, method) ((  Il2CppObject* (*) (InorganicCollection_1_t3450977518 *, const MethodInfo*))InorganicCollection_1_ObserveMove_m2797955546_gshared)(__this, method)
// IStream`1<CollectionRemoveEvent`1<T>> ZergRush.InorganicCollection`1<System.Object>::ObserveRemove()
extern "C"  Il2CppObject* InorganicCollection_1_ObserveRemove_m3795636736_gshared (InorganicCollection_1_t3450977518 * __this, const MethodInfo* method);
#define InorganicCollection_1_ObserveRemove_m3795636736(__this, method) ((  Il2CppObject* (*) (InorganicCollection_1_t3450977518 *, const MethodInfo*))InorganicCollection_1_ObserveRemove_m3795636736_gshared)(__this, method)
// IStream`1<CollectionReplaceEvent`1<T>> ZergRush.InorganicCollection`1<System.Object>::ObserveReplace()
extern "C"  Il2CppObject* InorganicCollection_1_ObserveReplace_m3125593920_gshared (InorganicCollection_1_t3450977518 * __this, const MethodInfo* method);
#define InorganicCollection_1_ObserveReplace_m3125593920(__this, method) ((  Il2CppObject* (*) (InorganicCollection_1_t3450977518 *, const MethodInfo*))InorganicCollection_1_ObserveReplace_m3125593920_gshared)(__this, method)
// System.IDisposable ZergRush.InorganicCollection`1<System.Object>::OnChanged(System.Action,Priority)
extern "C"  Il2CppObject * InorganicCollection_1_OnChanged_m1222232857_gshared (InorganicCollection_1_t3450977518 * __this, Action_t437523947 * ___action0, int32_t ___p1, const MethodInfo* method);
#define InorganicCollection_1_OnChanged_m1222232857(__this, ___action0, ___p1, method) ((  Il2CppObject * (*) (InorganicCollection_1_t3450977518 *, Action_t437523947 *, int32_t, const MethodInfo*))InorganicCollection_1_OnChanged_m1222232857_gshared)(__this, ___action0, ___p1, method)
// System.IDisposable ZergRush.InorganicCollection`1<System.Object>::Bind(System.Action`1<System.Collections.Generic.ICollection`1<T>>,Priority)
extern "C"  Il2CppObject * InorganicCollection_1_Bind_m1502836121_gshared (InorganicCollection_1_t3450977518 * __this, Action_1_t1451390511 * ___action0, int32_t ___p1, const MethodInfo* method);
#define InorganicCollection_1_Bind_m1502836121(__this, ___action0, ___p1, method) ((  Il2CppObject * (*) (InorganicCollection_1_t3450977518 *, Action_1_t1451390511 *, int32_t, const MethodInfo*))InorganicCollection_1_Bind_m1502836121_gshared)(__this, ___action0, ___p1, method)
// System.IDisposable ZergRush.InorganicCollection`1<System.Object>::ListenUpdates(System.Action`1<System.Collections.Generic.ICollection`1<T>>,Priority)
extern "C"  Il2CppObject * InorganicCollection_1_ListenUpdates_m1688181143_gshared (InorganicCollection_1_t3450977518 * __this, Action_1_t1451390511 * ___reaction0, int32_t ___p1, const MethodInfo* method);
#define InorganicCollection_1_ListenUpdates_m1688181143(__this, ___reaction0, ___p1, method) ((  Il2CppObject * (*) (InorganicCollection_1_t3450977518 *, Action_1_t1451390511 *, int32_t, const MethodInfo*))InorganicCollection_1_ListenUpdates_m1688181143_gshared)(__this, ___reaction0, ___p1, method)
// System.Collections.Generic.ICollection`1<T> ZergRush.InorganicCollection`1<System.Object>::get_value()
extern "C"  Il2CppObject* InorganicCollection_1_get_value_m3124552170_gshared (InorganicCollection_1_t3450977518 * __this, const MethodInfo* method);
#define InorganicCollection_1_get_value_m3124552170(__this, method) ((  Il2CppObject* (*) (InorganicCollection_1_t3450977518 *, const MethodInfo*))InorganicCollection_1_get_value_m3124552170_gshared)(__this, method)
// System.Object ZergRush.InorganicCollection`1<System.Object>::get_valueObject()
extern "C"  Il2CppObject * InorganicCollection_1_get_valueObject_m2207754504_gshared (InorganicCollection_1_t3450977518 * __this, const MethodInfo* method);
#define InorganicCollection_1_get_valueObject_m2207754504(__this, method) ((  Il2CppObject * (*) (InorganicCollection_1_t3450977518 *, const MethodInfo*))InorganicCollection_1_get_valueObject_m2207754504_gshared)(__this, method)
