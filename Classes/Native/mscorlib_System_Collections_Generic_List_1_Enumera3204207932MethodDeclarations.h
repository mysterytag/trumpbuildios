﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera4014815677MethodDeclarations.h"

// System.Void System.Collections.Generic.List`1/Enumerator<UIElemHideInfo>::.ctor(System.Collections.Generic.List`1<T>)
#define Enumerator__ctor_m3461164870(__this, ___l0, method) ((  void (*) (Enumerator_t3204207932 *, List_1_t823457644 *, const MethodInfo*))Enumerator__ctor_m1029849669_gshared)(__this, ___l0, method)
// System.Void System.Collections.Generic.List`1/Enumerator<UIElemHideInfo>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m1724313932(__this, method) ((  void (*) (Enumerator_t3204207932 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m771996397_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1/Enumerator<UIElemHideInfo>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m3796831362(__this, method) ((  Il2CppObject * (*) (Enumerator_t3204207932 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m3561903705_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<UIElemHideInfo>::Dispose()
#define Enumerator_Dispose_m731737771(__this, method) ((  void (*) (Enumerator_t3204207932 *, const MethodInfo*))Enumerator_Dispose_m2904289642_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<UIElemHideInfo>::VerifyState()
#define Enumerator_VerifyState_m2513690724(__this, method) ((  void (*) (Enumerator_t3204207932 *, const MethodInfo*))Enumerator_VerifyState_m1522854819_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<UIElemHideInfo>::MoveNext()
#define Enumerator_MoveNext_m4293438844(__this, method) ((  bool (*) (Enumerator_t3204207932 *, const MethodInfo*))Enumerator_MoveNext_m302222300_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<UIElemHideInfo>::get_Current()
#define Enumerator_get_Current_m603981885(__this, method) ((  UIElemHideInfo_t26498675 * (*) (Enumerator_t3204207932 *, const MethodInfo*))Enumerator_get_Current_m1113061044_gshared)(__this, method)
