﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Oper2908947767.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Operators.AsUnitObservableObservable`1/AsUnitObservable<System.Object>
struct  AsUnitObservable_t2008788970  : public OperatorObserverBase_2_t2908947767
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
