﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// FriendView
struct FriendView_t236852867;
// System.String
struct String_t;
// System.Collections.IEnumerator
struct IEnumerator_t287207039;
// FriendInfo
struct FriendInfo_t236470412;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String968488902.h"
#include "AssemblyU2DCSharp_FriendInfo236470412.h"

// System.Void FriendView::.ctor()
extern "C"  void FriendView__ctor_m3770361208 (FriendView_t236852867 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String FriendView::get_reuseIdentifier()
extern "C"  String_t* FriendView_get_reuseIdentifier_m446286471 (FriendView_t236852867 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FriendView::Invite()
extern "C"  void FriendView_Invite_m1952760533 (FriendView_t236852867 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator FriendView::LoadImage(System.String)
extern "C"  Il2CppObject * FriendView_LoadImage_m839898175 (FriendView_t236852867 * __this, String_t* ___url0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FriendView::LoadWrap()
extern "C"  void FriendView_LoadWrap_m2696801692 (FriendView_t236852867 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FriendView::Fill(FriendInfo)
extern "C"  void FriendView_Fill_m1608278787 (FriendView_t236852867 * __this, FriendInfo_t236470412 * ___friend0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
