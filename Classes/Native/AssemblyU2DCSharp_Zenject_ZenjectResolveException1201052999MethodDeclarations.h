﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Zenject.ZenjectResolveException
struct ZenjectResolveException_t1201052999;
// System.String
struct String_t;
// System.Exception
struct Exception_t1967233988;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String968488902.h"
#include "mscorlib_System_Exception1967233988.h"

// System.Void Zenject.ZenjectResolveException::.ctor(System.String)
extern "C"  void ZenjectResolveException__ctor_m3382034954 (ZenjectResolveException_t1201052999 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Zenject.ZenjectResolveException::.ctor(System.String,System.Exception)
extern "C"  void ZenjectResolveException__ctor_m1087182956 (ZenjectResolveException_t1201052999 * __this, String_t* ___message0, Exception_t1967233988 * ___innerException1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
