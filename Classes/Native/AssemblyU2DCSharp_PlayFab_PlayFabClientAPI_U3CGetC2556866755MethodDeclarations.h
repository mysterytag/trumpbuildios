﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.PlayFabClientAPI/<GetContentDownloadUrl>c__AnonStoreyC5
struct U3CGetContentDownloadUrlU3Ec__AnonStoreyC5_t2556866755;
// PlayFab.CallRequestContainer
struct CallRequestContainer_t432318609;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharpU2Dfirstpass_PlayFab_CallRequestCo432318609.h"

// System.Void PlayFab.PlayFabClientAPI/<GetContentDownloadUrl>c__AnonStoreyC5::.ctor()
extern "C"  void U3CGetContentDownloadUrlU3Ec__AnonStoreyC5__ctor_m4170825200 (U3CGetContentDownloadUrlU3Ec__AnonStoreyC5_t2556866755 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/<GetContentDownloadUrl>c__AnonStoreyC5::<>m__B2(PlayFab.CallRequestContainer)
extern "C"  void U3CGetContentDownloadUrlU3Ec__AnonStoreyC5_U3CU3Em__B2_m1678728446 (U3CGetContentDownloadUrlU3Ec__AnonStoreyC5_t2556866755 * __this, CallRequestContainer_t432318609 * ___requestContainer0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
