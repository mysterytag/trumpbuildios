﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.ClientModels.GetFriendLeaderboardAroundPlayerRequest
struct GetFriendLeaderboardAroundPlayerRequest_t16887688;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String968488902.h"
#include "mscorlib_System_Nullable_1_gen1438485399.h"
#include "mscorlib_System_Nullable_1_gen3097043249.h"

// System.Void PlayFab.ClientModels.GetFriendLeaderboardAroundPlayerRequest::.ctor()
extern "C"  void GetFriendLeaderboardAroundPlayerRequest__ctor_m4046364449 (GetFriendLeaderboardAroundPlayerRequest_t16887688 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PlayFab.ClientModels.GetFriendLeaderboardAroundPlayerRequest::get_StatisticName()
extern "C"  String_t* GetFriendLeaderboardAroundPlayerRequest_get_StatisticName_m3917264334 (GetFriendLeaderboardAroundPlayerRequest_t16887688 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.GetFriendLeaderboardAroundPlayerRequest::set_StatisticName(System.String)
extern "C"  void GetFriendLeaderboardAroundPlayerRequest_set_StatisticName_m3041870725 (GetFriendLeaderboardAroundPlayerRequest_t16887688 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Nullable`1<System.Int32> PlayFab.ClientModels.GetFriendLeaderboardAroundPlayerRequest::get_MaxResultsCount()
extern "C"  Nullable_1_t1438485399  GetFriendLeaderboardAroundPlayerRequest_get_MaxResultsCount_m4251969732 (GetFriendLeaderboardAroundPlayerRequest_t16887688 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.GetFriendLeaderboardAroundPlayerRequest::set_MaxResultsCount(System.Nullable`1<System.Int32>)
extern "C"  void GetFriendLeaderboardAroundPlayerRequest_set_MaxResultsCount_m1181676111 (GetFriendLeaderboardAroundPlayerRequest_t16887688 * __this, Nullable_1_t1438485399  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PlayFab.ClientModels.GetFriendLeaderboardAroundPlayerRequest::get_PlayFabId()
extern "C"  String_t* GetFriendLeaderboardAroundPlayerRequest_get_PlayFabId_m1270825409 (GetFriendLeaderboardAroundPlayerRequest_t16887688 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.GetFriendLeaderboardAroundPlayerRequest::set_PlayFabId(System.String)
extern "C"  void GetFriendLeaderboardAroundPlayerRequest_set_PlayFabId_m41118450 (GetFriendLeaderboardAroundPlayerRequest_t16887688 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Nullable`1<System.Boolean> PlayFab.ClientModels.GetFriendLeaderboardAroundPlayerRequest::get_IncludeSteamFriends()
extern "C"  Nullable_1_t3097043249  GetFriendLeaderboardAroundPlayerRequest_get_IncludeSteamFriends_m954459098 (GetFriendLeaderboardAroundPlayerRequest_t16887688 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.GetFriendLeaderboardAroundPlayerRequest::set_IncludeSteamFriends(System.Nullable`1<System.Boolean>)
extern "C"  void GetFriendLeaderboardAroundPlayerRequest_set_IncludeSteamFriends_m2632215993 (GetFriendLeaderboardAroundPlayerRequest_t16887688 * __this, Nullable_1_t3097043249  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Nullable`1<System.Boolean> PlayFab.ClientModels.GetFriendLeaderboardAroundPlayerRequest::get_IncludeFacebookFriends()
extern "C"  Nullable_1_t3097043249  GetFriendLeaderboardAroundPlayerRequest_get_IncludeFacebookFriends_m581004988 (GetFriendLeaderboardAroundPlayerRequest_t16887688 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.GetFriendLeaderboardAroundPlayerRequest::set_IncludeFacebookFriends(System.Nullable`1<System.Boolean>)
extern "C"  void GetFriendLeaderboardAroundPlayerRequest_set_IncludeFacebookFriends_m3472051937 (GetFriendLeaderboardAroundPlayerRequest_t16887688 * __this, Nullable_1_t3097043249  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
