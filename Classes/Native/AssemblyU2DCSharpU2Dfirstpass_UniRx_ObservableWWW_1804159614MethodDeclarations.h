﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.ObservableWWW/<GetWWW>c__AnonStorey80
struct U3CGetWWWU3Ec__AnonStorey80_t1804159614;
// System.Collections.IEnumerator
struct IEnumerator_t287207039;
// UniRx.IObserver`1<UnityEngine.WWW>
struct IObserver_1_t3734971003;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_CancellationTo1439151560.h"

// System.Void UniRx.ObservableWWW/<GetWWW>c__AnonStorey80::.ctor()
extern "C"  void U3CGetWWWU3Ec__AnonStorey80__ctor_m1478189740 (U3CGetWWWU3Ec__AnonStorey80_t1804159614 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator UniRx.ObservableWWW/<GetWWW>c__AnonStorey80::<>m__AC(UniRx.IObserver`1<UnityEngine.WWW>,UniRx.CancellationToken)
extern "C"  Il2CppObject * U3CGetWWWU3Ec__AnonStorey80_U3CU3Em__AC_m4003425075 (U3CGetWWWU3Ec__AnonStorey80_t1804159614 * __this, Il2CppObject* ___observer0, CancellationToken_t1439151560  ___cancellation1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
