﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VkConnectorBase/<InviteFriend>c__AnonStoreyDC
struct U3CInviteFriendU3Ec__AnonStoreyDC_t3048000427;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Tuple_2_gen3445990771.h"

// System.Void VkConnectorBase/<InviteFriend>c__AnonStoreyDC::.ctor()
extern "C"  void U3CInviteFriendU3Ec__AnonStoreyDC__ctor_m2227044102 (U3CInviteFriendU3Ec__AnonStoreyDC_t3048000427 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VkConnectorBase/<InviteFriend>c__AnonStoreyDC::<>m__E7(UniRx.Tuple`2<System.String,System.String>)
extern "C"  void U3CInviteFriendU3Ec__AnonStoreyDC_U3CU3Em__E7_m2594639389 (U3CInviteFriendU3Ec__AnonStoreyDC_t3048000427 * __this, Tuple_2_t3445990771  ___t0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
