﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.PlayFabClientAPI/UnlinkIOSDeviceIDResponseCallback
struct UnlinkIOSDeviceIDResponseCallback_t854490801;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;
// PlayFab.ClientModels.UnlinkIOSDeviceIDRequest
struct UnlinkIOSDeviceIDRequest_t1716723508;
// PlayFab.ClientModels.UnlinkIOSDeviceIDResult
struct UnlinkIOSDeviceIDResult_t3277114616;
// PlayFab.PlayFabError
struct PlayFabError_t750598646;
// System.IAsyncResult
struct IAsyncResult_t537683269;
// System.AsyncCallback
struct AsyncCallback_t1363551830;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_IntPtr676692020.h"
#include "mscorlib_System_String968488902.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_UnlinkIOSDe1716723508.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_UnlinkIOSDe3277114616.h"
#include "AssemblyU2DCSharpU2Dfirstpass_PlayFab_PlayFabError750598646.h"
#include "mscorlib_System_AsyncCallback1363551830.h"

// System.Void PlayFab.PlayFabClientAPI/UnlinkIOSDeviceIDResponseCallback::.ctor(System.Object,System.IntPtr)
extern "C"  void UnlinkIOSDeviceIDResponseCallback__ctor_m2498548416 (UnlinkIOSDeviceIDResponseCallback_t854490801 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/UnlinkIOSDeviceIDResponseCallback::Invoke(System.String,System.Int32,PlayFab.ClientModels.UnlinkIOSDeviceIDRequest,PlayFab.ClientModels.UnlinkIOSDeviceIDResult,PlayFab.PlayFabError,System.Object)
extern "C"  void UnlinkIOSDeviceIDResponseCallback_Invoke_m613536641 (UnlinkIOSDeviceIDResponseCallback_t854490801 * __this, String_t* ___urlPath0, int32_t ___callId1, UnlinkIOSDeviceIDRequest_t1716723508 * ___request2, UnlinkIOSDeviceIDResult_t3277114616 * ___result3, PlayFabError_t750598646 * ___error4, Il2CppObject * ___customData5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
extern "C" void pinvoke_delegate_wrapper_UnlinkIOSDeviceIDResponseCallback_t854490801(Il2CppObject* delegate, String_t* ___urlPath0, int32_t ___callId1, UnlinkIOSDeviceIDRequest_t1716723508 * ___request2, UnlinkIOSDeviceIDResult_t3277114616 * ___result3, PlayFabError_t750598646 * ___error4, Il2CppObject * ___customData5);
// System.IAsyncResult PlayFab.PlayFabClientAPI/UnlinkIOSDeviceIDResponseCallback::BeginInvoke(System.String,System.Int32,PlayFab.ClientModels.UnlinkIOSDeviceIDRequest,PlayFab.ClientModels.UnlinkIOSDeviceIDResult,PlayFab.PlayFabError,System.Object,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * UnlinkIOSDeviceIDResponseCallback_BeginInvoke_m1410588558 (UnlinkIOSDeviceIDResponseCallback_t854490801 * __this, String_t* ___urlPath0, int32_t ___callId1, UnlinkIOSDeviceIDRequest_t1716723508 * ___request2, UnlinkIOSDeviceIDResult_t3277114616 * ___result3, PlayFabError_t750598646 * ___error4, Il2CppObject * ___customData5, AsyncCallback_t1363551830 * ___callback6, Il2CppObject * ___object7, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/UnlinkIOSDeviceIDResponseCallback::EndInvoke(System.IAsyncResult)
extern "C"  void UnlinkIOSDeviceIDResponseCallback_EndInvoke_m873964752 (UnlinkIOSDeviceIDResponseCallback_t854490801 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
