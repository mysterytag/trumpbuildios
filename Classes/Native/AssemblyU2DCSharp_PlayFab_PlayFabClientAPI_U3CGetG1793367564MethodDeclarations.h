﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.PlayFabClientAPI/<GetGameServerRegions>c__AnonStoreyB5
struct U3CGetGameServerRegionsU3Ec__AnonStoreyB5_t1793367564;
// PlayFab.CallRequestContainer
struct CallRequestContainer_t432318609;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharpU2Dfirstpass_PlayFab_CallRequestCo432318609.h"

// System.Void PlayFab.PlayFabClientAPI/<GetGameServerRegions>c__AnonStoreyB5::.ctor()
extern "C"  void U3CGetGameServerRegionsU3Ec__AnonStoreyB5__ctor_m2315232039 (U3CGetGameServerRegionsU3Ec__AnonStoreyB5_t1793367564 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/<GetGameServerRegions>c__AnonStoreyB5::<>m__A2(PlayFab.CallRequestContainer)
extern "C"  void U3CGetGameServerRegionsU3Ec__AnonStoreyB5_U3CU3Em__A2_m1739147286 (U3CGetGameServerRegionsU3Ec__AnonStoreyB5_t1793367564 * __this, CallRequestContainer_t432318609 * ___requestContainer0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
