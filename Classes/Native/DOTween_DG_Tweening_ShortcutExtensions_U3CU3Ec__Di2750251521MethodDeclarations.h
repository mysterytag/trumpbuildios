﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// DG.Tweening.ShortcutExtensions/<>c__DisplayClass70_0
struct U3CU3Ec__DisplayClass70_0_t2750251521;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Color1588175760.h"

// System.Void DG.Tweening.ShortcutExtensions/<>c__DisplayClass70_0::.ctor()
extern "C"  void U3CU3Ec__DisplayClass70_0__ctor_m2690868021 (U3CU3Ec__DisplayClass70_0_t2750251521 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Color DG.Tweening.ShortcutExtensions/<>c__DisplayClass70_0::<DOBlendableColor>b__0()
extern "C"  Color_t1588175760  U3CU3Ec__DisplayClass70_0_U3CDOBlendableColorU3Eb__0_m3413144771 (U3CU3Ec__DisplayClass70_0_t2750251521 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DG.Tweening.ShortcutExtensions/<>c__DisplayClass70_0::<DOBlendableColor>b__1(UnityEngine.Color)
extern "C"  void U3CU3Ec__DisplayClass70_0_U3CDOBlendableColorU3Eb__1_m2047147209 (U3CU3Ec__DisplayClass70_0_t2750251521 * __this, Color_t1588175760  ___x0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
