﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.ClientModels.UserSettings
struct UserSettings_t8286526;

#include "codegen/il2cpp-codegen.h"

// System.Void PlayFab.ClientModels.UserSettings::.ctor()
extern "C"  void UserSettings__ctor_m1759930815 (UserSettings_t8286526 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PlayFab.ClientModels.UserSettings::get_NeedsAttribution()
extern "C"  bool UserSettings_get_NeedsAttribution_m1881489436 (UserSettings_t8286526 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.UserSettings::set_NeedsAttribution(System.Boolean)
extern "C"  void UserSettings_set_NeedsAttribution_m3144989243 (UserSettings_t8286526 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
