﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// AchievementController/<Report>c__Iterator9
struct U3CReportU3Ec__Iterator9_t3537486326;
// System.Object
struct Il2CppObject;
// System.Action
struct Action_t437523947;

#include "codegen/il2cpp-codegen.h"
#include "System_Core_System_Action437523947.h"

// System.Void AchievementController/<Report>c__Iterator9::.ctor()
extern "C"  void U3CReportU3Ec__Iterator9__ctor_m4040792161 (U3CReportU3Ec__Iterator9_t3537486326 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object AchievementController/<Report>c__Iterator9::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CReportU3Ec__Iterator9_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1859276251 (U3CReportU3Ec__Iterator9_t3537486326 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object AchievementController/<Report>c__Iterator9::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CReportU3Ec__Iterator9_System_Collections_IEnumerator_get_Current_m1205430127 (U3CReportU3Ec__Iterator9_t3537486326 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean AchievementController/<Report>c__Iterator9::MoveNext()
extern "C"  bool U3CReportU3Ec__Iterator9_MoveNext_m3124466907 (U3CReportU3Ec__Iterator9_t3537486326 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AchievementController/<Report>c__Iterator9::Dispose()
extern "C"  void U3CReportU3Ec__Iterator9_Dispose_m449114206 (U3CReportU3Ec__Iterator9_t3537486326 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AchievementController/<Report>c__Iterator9::Reset()
extern "C"  void U3CReportU3Ec__Iterator9_Reset_m1687225102 (U3CReportU3Ec__Iterator9_t3537486326 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AchievementController/<Report>c__Iterator9::<>m__C9(System.Boolean)
extern "C"  void U3CReportU3Ec__Iterator9_U3CU3Em__C9_m2110644023 (U3CReportU3Ec__Iterator9_t3537486326 * __this, bool ___ok0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AchievementController/<Report>c__Iterator9::<>m__CA(System.Action)
extern "C"  void U3CReportU3Ec__Iterator9_U3CU3Em__CA_m3624325813 (U3CReportU3Ec__Iterator9_t3537486326 * __this, Action_t437523947 * ___call0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
