﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// BioCollection`1/<Implant>c__AnonStoreyF8<System.Object>
struct U3CImplantU3Ec__AnonStoreyF8_t1491113711;

#include "codegen/il2cpp-codegen.h"

// System.Void BioCollection`1/<Implant>c__AnonStoreyF8<System.Object>::.ctor()
extern "C"  void U3CImplantU3Ec__AnonStoreyF8__ctor_m2349960973_gshared (U3CImplantU3Ec__AnonStoreyF8_t1491113711 * __this, const MethodInfo* method);
#define U3CImplantU3Ec__AnonStoreyF8__ctor_m2349960973(__this, method) ((  void (*) (U3CImplantU3Ec__AnonStoreyF8_t1491113711 *, const MethodInfo*))U3CImplantU3Ec__AnonStoreyF8__ctor_m2349960973_gshared)(__this, method)
// System.Void BioCollection`1/<Implant>c__AnonStoreyF8<System.Object>::<>m__16C()
extern "C"  void U3CImplantU3Ec__AnonStoreyF8_U3CU3Em__16C_m2550389066_gshared (U3CImplantU3Ec__AnonStoreyF8_t1491113711 * __this, const MethodInfo* method);
#define U3CImplantU3Ec__AnonStoreyF8_U3CU3Em__16C_m2550389066(__this, method) ((  void (*) (U3CImplantU3Ec__AnonStoreyF8_t1491113711 *, const MethodInfo*))U3CImplantU3Ec__AnonStoreyF8_U3CU3Em__16C_m2550389066_gshared)(__this, method)
