﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// I18nText
struct I18nText_t621566091;

#include "codegen/il2cpp-codegen.h"

// System.Void I18nText::.ctor()
extern "C"  void I18nText__ctor_m3015168624 (I18nText_t621566091 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void I18nText::Start()
extern "C"  void I18nText_Start_m1962306416 (I18nText_t621566091 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
