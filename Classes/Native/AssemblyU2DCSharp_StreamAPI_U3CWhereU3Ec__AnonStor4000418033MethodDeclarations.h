﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// StreamAPI/<Where>c__AnonStorey12E/<Where>c__AnonStorey12F
struct U3CWhereU3Ec__AnonStorey12F_t4000418033;

#include "codegen/il2cpp-codegen.h"

// System.Void StreamAPI/<Where>c__AnonStorey12E/<Where>c__AnonStorey12F::.ctor()
extern "C"  void U3CWhereU3Ec__AnonStorey12F__ctor_m142630638 (U3CWhereU3Ec__AnonStorey12F_t4000418033 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void StreamAPI/<Where>c__AnonStorey12E/<Where>c__AnonStorey12F::<>m__1CA()
extern "C"  void U3CWhereU3Ec__AnonStorey12F_U3CU3Em__1CA_m4217033498 (U3CWhereU3Ec__AnonStorey12F_t4000418033 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
