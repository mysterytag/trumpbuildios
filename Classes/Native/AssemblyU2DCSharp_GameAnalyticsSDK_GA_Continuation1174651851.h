﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.IEnumerator
struct IEnumerator_t287207039;
// System.Func`1<System.Boolean>
struct Func_1_t1353786588;
// System.Action
struct Action_t437523947;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameAnalyticsSDK.GA_ContinuationManager/EditorCoroutine
struct  EditorCoroutine_t1174651851  : public Il2CppObject
{
public:
	// System.Collections.IEnumerator GameAnalyticsSDK.GA_ContinuationManager/EditorCoroutine::<Routine>k__BackingField
	Il2CppObject * ___U3CRoutineU3Ek__BackingField_0;
	// System.Func`1<System.Boolean> GameAnalyticsSDK.GA_ContinuationManager/EditorCoroutine::<Done>k__BackingField
	Func_1_t1353786588 * ___U3CDoneU3Ek__BackingField_1;
	// System.Action GameAnalyticsSDK.GA_ContinuationManager/EditorCoroutine::<ContinueWith>k__BackingField
	Action_t437523947 * ___U3CContinueWithU3Ek__BackingField_2;

public:
	inline static int32_t get_offset_of_U3CRoutineU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(EditorCoroutine_t1174651851, ___U3CRoutineU3Ek__BackingField_0)); }
	inline Il2CppObject * get_U3CRoutineU3Ek__BackingField_0() const { return ___U3CRoutineU3Ek__BackingField_0; }
	inline Il2CppObject ** get_address_of_U3CRoutineU3Ek__BackingField_0() { return &___U3CRoutineU3Ek__BackingField_0; }
	inline void set_U3CRoutineU3Ek__BackingField_0(Il2CppObject * value)
	{
		___U3CRoutineU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier(&___U3CRoutineU3Ek__BackingField_0, value);
	}

	inline static int32_t get_offset_of_U3CDoneU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(EditorCoroutine_t1174651851, ___U3CDoneU3Ek__BackingField_1)); }
	inline Func_1_t1353786588 * get_U3CDoneU3Ek__BackingField_1() const { return ___U3CDoneU3Ek__BackingField_1; }
	inline Func_1_t1353786588 ** get_address_of_U3CDoneU3Ek__BackingField_1() { return &___U3CDoneU3Ek__BackingField_1; }
	inline void set_U3CDoneU3Ek__BackingField_1(Func_1_t1353786588 * value)
	{
		___U3CDoneU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CDoneU3Ek__BackingField_1, value);
	}

	inline static int32_t get_offset_of_U3CContinueWithU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(EditorCoroutine_t1174651851, ___U3CContinueWithU3Ek__BackingField_2)); }
	inline Action_t437523947 * get_U3CContinueWithU3Ek__BackingField_2() const { return ___U3CContinueWithU3Ek__BackingField_2; }
	inline Action_t437523947 ** get_address_of_U3CContinueWithU3Ek__BackingField_2() { return &___U3CContinueWithU3Ek__BackingField_2; }
	inline void set_U3CContinueWithU3Ek__BackingField_2(Action_t437523947 * value)
	{
		___U3CContinueWithU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier(&___U3CContinueWithU3Ek__BackingField_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
