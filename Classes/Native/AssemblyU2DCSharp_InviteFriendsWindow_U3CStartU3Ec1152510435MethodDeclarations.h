﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// InviteFriendsWindow/<Start>c__AnonStoreyE9
struct U3CStartU3Ec__AnonStoreyE9_t1152510435;

#include "codegen/il2cpp-codegen.h"

// System.Void InviteFriendsWindow/<Start>c__AnonStoreyE9::.ctor()
extern "C"  void U3CStartU3Ec__AnonStoreyE9__ctor_m293836101 (U3CStartU3Ec__AnonStoreyE9_t1152510435 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void InviteFriendsWindow/<Start>c__AnonStoreyE9::<>m__11A()
extern "C"  void U3CStartU3Ec__AnonStoreyE9_U3CU3Em__11A_m3357751989 (U3CStartU3Ec__AnonStoreyE9_t1152510435 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
