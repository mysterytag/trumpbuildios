﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Object
struct Il2CppObject;
// PopText
struct PopText_t1269723902;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PopText/<ShowCoroutine>c__Iterator34
struct  U3CShowCoroutineU3Ec__Iterator34_t2818117363  : public Il2CppObject
{
public:
	// System.Single PopText/<ShowCoroutine>c__Iterator34::<startTime>__0
	float ___U3CstartTimeU3E__0_0;
	// System.Single PopText/<ShowCoroutine>c__Iterator34::<processTime>__1
	float ___U3CprocessTimeU3E__1_1;
	// System.Single PopText/<ShowCoroutine>c__Iterator34::<deltaTime>__2
	float ___U3CdeltaTimeU3E__2_2;
	// System.Single PopText/<ShowCoroutine>c__Iterator34::<speed>__3
	float ___U3CspeedU3E__3_3;
	// System.Single PopText/<ShowCoroutine>c__Iterator34::<ratio>__4
	float ___U3CratioU3E__4_4;
	// System.Single PopText/<ShowCoroutine>c__Iterator34::<ratio>__5
	float ___U3CratioU3E__5_5;
	// System.Int32 PopText/<ShowCoroutine>c__Iterator34::$PC
	int32_t ___U24PC_6;
	// System.Object PopText/<ShowCoroutine>c__Iterator34::$current
	Il2CppObject * ___U24current_7;
	// PopText PopText/<ShowCoroutine>c__Iterator34::<>f__this
	PopText_t1269723902 * ___U3CU3Ef__this_8;

public:
	inline static int32_t get_offset_of_U3CstartTimeU3E__0_0() { return static_cast<int32_t>(offsetof(U3CShowCoroutineU3Ec__Iterator34_t2818117363, ___U3CstartTimeU3E__0_0)); }
	inline float get_U3CstartTimeU3E__0_0() const { return ___U3CstartTimeU3E__0_0; }
	inline float* get_address_of_U3CstartTimeU3E__0_0() { return &___U3CstartTimeU3E__0_0; }
	inline void set_U3CstartTimeU3E__0_0(float value)
	{
		___U3CstartTimeU3E__0_0 = value;
	}

	inline static int32_t get_offset_of_U3CprocessTimeU3E__1_1() { return static_cast<int32_t>(offsetof(U3CShowCoroutineU3Ec__Iterator34_t2818117363, ___U3CprocessTimeU3E__1_1)); }
	inline float get_U3CprocessTimeU3E__1_1() const { return ___U3CprocessTimeU3E__1_1; }
	inline float* get_address_of_U3CprocessTimeU3E__1_1() { return &___U3CprocessTimeU3E__1_1; }
	inline void set_U3CprocessTimeU3E__1_1(float value)
	{
		___U3CprocessTimeU3E__1_1 = value;
	}

	inline static int32_t get_offset_of_U3CdeltaTimeU3E__2_2() { return static_cast<int32_t>(offsetof(U3CShowCoroutineU3Ec__Iterator34_t2818117363, ___U3CdeltaTimeU3E__2_2)); }
	inline float get_U3CdeltaTimeU3E__2_2() const { return ___U3CdeltaTimeU3E__2_2; }
	inline float* get_address_of_U3CdeltaTimeU3E__2_2() { return &___U3CdeltaTimeU3E__2_2; }
	inline void set_U3CdeltaTimeU3E__2_2(float value)
	{
		___U3CdeltaTimeU3E__2_2 = value;
	}

	inline static int32_t get_offset_of_U3CspeedU3E__3_3() { return static_cast<int32_t>(offsetof(U3CShowCoroutineU3Ec__Iterator34_t2818117363, ___U3CspeedU3E__3_3)); }
	inline float get_U3CspeedU3E__3_3() const { return ___U3CspeedU3E__3_3; }
	inline float* get_address_of_U3CspeedU3E__3_3() { return &___U3CspeedU3E__3_3; }
	inline void set_U3CspeedU3E__3_3(float value)
	{
		___U3CspeedU3E__3_3 = value;
	}

	inline static int32_t get_offset_of_U3CratioU3E__4_4() { return static_cast<int32_t>(offsetof(U3CShowCoroutineU3Ec__Iterator34_t2818117363, ___U3CratioU3E__4_4)); }
	inline float get_U3CratioU3E__4_4() const { return ___U3CratioU3E__4_4; }
	inline float* get_address_of_U3CratioU3E__4_4() { return &___U3CratioU3E__4_4; }
	inline void set_U3CratioU3E__4_4(float value)
	{
		___U3CratioU3E__4_4 = value;
	}

	inline static int32_t get_offset_of_U3CratioU3E__5_5() { return static_cast<int32_t>(offsetof(U3CShowCoroutineU3Ec__Iterator34_t2818117363, ___U3CratioU3E__5_5)); }
	inline float get_U3CratioU3E__5_5() const { return ___U3CratioU3E__5_5; }
	inline float* get_address_of_U3CratioU3E__5_5() { return &___U3CratioU3E__5_5; }
	inline void set_U3CratioU3E__5_5(float value)
	{
		___U3CratioU3E__5_5 = value;
	}

	inline static int32_t get_offset_of_U24PC_6() { return static_cast<int32_t>(offsetof(U3CShowCoroutineU3Ec__Iterator34_t2818117363, ___U24PC_6)); }
	inline int32_t get_U24PC_6() const { return ___U24PC_6; }
	inline int32_t* get_address_of_U24PC_6() { return &___U24PC_6; }
	inline void set_U24PC_6(int32_t value)
	{
		___U24PC_6 = value;
	}

	inline static int32_t get_offset_of_U24current_7() { return static_cast<int32_t>(offsetof(U3CShowCoroutineU3Ec__Iterator34_t2818117363, ___U24current_7)); }
	inline Il2CppObject * get_U24current_7() const { return ___U24current_7; }
	inline Il2CppObject ** get_address_of_U24current_7() { return &___U24current_7; }
	inline void set_U24current_7(Il2CppObject * value)
	{
		___U24current_7 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_7, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__this_8() { return static_cast<int32_t>(offsetof(U3CShowCoroutineU3Ec__Iterator34_t2818117363, ___U3CU3Ef__this_8)); }
	inline PopText_t1269723902 * get_U3CU3Ef__this_8() const { return ___U3CU3Ef__this_8; }
	inline PopText_t1269723902 ** get_address_of_U3CU3Ef__this_8() { return &___U3CU3Ef__this_8; }
	inline void set_U3CU3Ef__this_8(PopText_t1269723902 * value)
	{
		___U3CU3Ef__this_8 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__this_8, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
