﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.TimestampObservable`1/Timestamp<System.Object>
struct Timestamp_t3118418687;
// UniRx.Operators.TimestampObservable`1<System.Object>
struct TimestampObservable_1_t2503612568;
// UniRx.IObserver`1<UniRx.Timestamped`1<System.Object>>
struct IObserver_1_t436215880;
// System.IDisposable
struct IDisposable_t1628921374;
// System.Object
struct Il2CppObject;
// System.Exception
struct Exception_t1967233988;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_Exception1967233988.h"

// System.Void UniRx.Operators.TimestampObservable`1/Timestamp<System.Object>::.ctor(UniRx.Operators.TimestampObservable`1<T>,UniRx.IObserver`1<UniRx.Timestamped`1<T>>,System.IDisposable)
extern "C"  void Timestamp__ctor_m2054723128_gshared (Timestamp_t3118418687 * __this, TimestampObservable_1_t2503612568 * ___parent0, Il2CppObject* ___observer1, Il2CppObject * ___cancel2, const MethodInfo* method);
#define Timestamp__ctor_m2054723128(__this, ___parent0, ___observer1, ___cancel2, method) ((  void (*) (Timestamp_t3118418687 *, TimestampObservable_1_t2503612568 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))Timestamp__ctor_m2054723128_gshared)(__this, ___parent0, ___observer1, ___cancel2, method)
// System.Void UniRx.Operators.TimestampObservable`1/Timestamp<System.Object>::OnNext(T)
extern "C"  void Timestamp_OnNext_m1386879221_gshared (Timestamp_t3118418687 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Timestamp_OnNext_m1386879221(__this, ___value0, method) ((  void (*) (Timestamp_t3118418687 *, Il2CppObject *, const MethodInfo*))Timestamp_OnNext_m1386879221_gshared)(__this, ___value0, method)
// System.Void UniRx.Operators.TimestampObservable`1/Timestamp<System.Object>::OnError(System.Exception)
extern "C"  void Timestamp_OnError_m3377441284_gshared (Timestamp_t3118418687 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method);
#define Timestamp_OnError_m3377441284(__this, ___error0, method) ((  void (*) (Timestamp_t3118418687 *, Exception_t1967233988 *, const MethodInfo*))Timestamp_OnError_m3377441284_gshared)(__this, ___error0, method)
// System.Void UniRx.Operators.TimestampObservable`1/Timestamp<System.Object>::OnCompleted()
extern "C"  void Timestamp_OnCompleted_m2724873047_gshared (Timestamp_t3118418687 * __this, const MethodInfo* method);
#define Timestamp_OnCompleted_m2724873047(__this, method) ((  void (*) (Timestamp_t3118418687 *, const MethodInfo*))Timestamp_OnCompleted_m2724873047_gshared)(__this, method)
