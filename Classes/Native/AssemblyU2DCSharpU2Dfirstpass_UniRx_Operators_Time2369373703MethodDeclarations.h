﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.TimeoutObservable`1/Timeout_<System.Object>
struct Timeout__t2369373703;
// UniRx.Operators.TimeoutObservable`1<System.Object>
struct TimeoutObservable_1_t1895307051;
// UniRx.IObserver`1<System.Object>
struct IObserver_1_t3049105323;
// System.IDisposable
struct IDisposable_t1628921374;
// System.Object
struct Il2CppObject;
// System.Exception
struct Exception_t1967233988;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_Exception1967233988.h"

// System.Void UniRx.Operators.TimeoutObservable`1/Timeout_<System.Object>::.ctor(UniRx.Operators.TimeoutObservable`1<T>,UniRx.IObserver`1<T>,System.IDisposable)
extern "C"  void Timeout___ctor_m2997021766_gshared (Timeout__t2369373703 * __this, TimeoutObservable_1_t1895307051 * ___parent0, Il2CppObject* ___observer1, Il2CppObject * ___cancel2, const MethodInfo* method);
#define Timeout___ctor_m2997021766(__this, ___parent0, ___observer1, ___cancel2, method) ((  void (*) (Timeout__t2369373703 *, TimeoutObservable_1_t1895307051 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))Timeout___ctor_m2997021766_gshared)(__this, ___parent0, ___observer1, ___cancel2, method)
// System.IDisposable UniRx.Operators.TimeoutObservable`1/Timeout_<System.Object>::Run()
extern "C"  Il2CppObject * Timeout__Run_m1491551598_gshared (Timeout__t2369373703 * __this, const MethodInfo* method);
#define Timeout__Run_m1491551598(__this, method) ((  Il2CppObject * (*) (Timeout__t2369373703 *, const MethodInfo*))Timeout__Run_m1491551598_gshared)(__this, method)
// System.Void UniRx.Operators.TimeoutObservable`1/Timeout_<System.Object>::OnNext()
extern "C"  void Timeout__OnNext_m2661514348_gshared (Timeout__t2369373703 * __this, const MethodInfo* method);
#define Timeout__OnNext_m2661514348(__this, method) ((  void (*) (Timeout__t2369373703 *, const MethodInfo*))Timeout__OnNext_m2661514348_gshared)(__this, method)
// System.Void UniRx.Operators.TimeoutObservable`1/Timeout_<System.Object>::OnNext(T)
extern "C"  void Timeout__OnNext_m902567538_gshared (Timeout__t2369373703 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Timeout__OnNext_m902567538(__this, ___value0, method) ((  void (*) (Timeout__t2369373703 *, Il2CppObject *, const MethodInfo*))Timeout__OnNext_m902567538_gshared)(__this, ___value0, method)
// System.Void UniRx.Operators.TimeoutObservable`1/Timeout_<System.Object>::OnError(System.Exception)
extern "C"  void Timeout__OnError_m2378763649_gshared (Timeout__t2369373703 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method);
#define Timeout__OnError_m2378763649(__this, ___error0, method) ((  void (*) (Timeout__t2369373703 *, Exception_t1967233988 *, const MethodInfo*))Timeout__OnError_m2378763649_gshared)(__this, ___error0, method)
// System.Void UniRx.Operators.TimeoutObservable`1/Timeout_<System.Object>::OnCompleted()
extern "C"  void Timeout__OnCompleted_m19348052_gshared (Timeout__t2369373703 * __this, const MethodInfo* method);
#define Timeout__OnCompleted_m19348052(__this, method) ((  void (*) (Timeout__t2369373703 *, const MethodInfo*))Timeout__OnCompleted_m19348052_gshared)(__this, method)
