﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Ionic.Zlib.ZlibException
struct ZlibException_t388433425;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String968488902.h"

// System.Void Ionic.Zlib.ZlibException::.ctor()
extern "C"  void ZlibException__ctor_m1267539694 (ZlibException_t388433425 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Ionic.Zlib.ZlibException::.ctor(System.String)
extern "C"  void ZlibException__ctor_m1908847316 (ZlibException_t388433425 * __this, String_t* ___s0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
