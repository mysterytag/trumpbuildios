﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Observable/EveryAfterUpdateInvoker
struct EveryAfterUpdateInvoker_t3949333456;
// UniRx.IObserver`1<System.Int64>
struct IObserver_1_t764446489;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_CancellationTo1439151560.h"

// System.Void UniRx.Observable/EveryAfterUpdateInvoker::.ctor(UniRx.IObserver`1<System.Int64>,UniRx.CancellationToken)
extern "C"  void EveryAfterUpdateInvoker__ctor_m2456117825 (EveryAfterUpdateInvoker_t3949333456 * __this, Il2CppObject* ___observer0, CancellationToken_t1439151560  ___cancellationToken1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UniRx.Observable/EveryAfterUpdateInvoker::MoveNext()
extern "C"  bool EveryAfterUpdateInvoker_MoveNext_m3416459249 (EveryAfterUpdateInvoker_t3949333456 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object UniRx.Observable/EveryAfterUpdateInvoker::get_Current()
extern "C"  Il2CppObject * EveryAfterUpdateInvoker_get_Current_m1905716556 (EveryAfterUpdateInvoker_t3949333456 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.Observable/EveryAfterUpdateInvoker::Reset()
extern "C"  void EveryAfterUpdateInvoker_Reset_m2871727776 (EveryAfterUpdateInvoker_t3949333456 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
