﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Runtime.CompilerServices.DateTimeConstantAttribute
struct DateTimeConstantAttribute_t238863971;

#include "codegen/il2cpp-codegen.h"

// System.Int64 System.Runtime.CompilerServices.DateTimeConstantAttribute::get_Ticks()
extern "C"  int64_t DateTimeConstantAttribute_get_Ticks_m3417375099 (DateTimeConstantAttribute_t238863971 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
