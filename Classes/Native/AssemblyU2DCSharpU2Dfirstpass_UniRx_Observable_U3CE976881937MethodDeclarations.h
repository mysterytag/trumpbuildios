﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Observable/<EveryFixedUpdateCore>c__Iterator12
struct U3CEveryFixedUpdateCoreU3Ec__Iterator12_t976881937;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void UniRx.Observable/<EveryFixedUpdateCore>c__Iterator12::.ctor()
extern "C"  void U3CEveryFixedUpdateCoreU3Ec__Iterator12__ctor_m3456378834 (U3CEveryFixedUpdateCoreU3Ec__Iterator12_t976881937 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object UniRx.Observable/<EveryFixedUpdateCore>c__Iterator12::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CEveryFixedUpdateCoreU3Ec__Iterator12_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3689341386 (U3CEveryFixedUpdateCoreU3Ec__Iterator12_t976881937 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object UniRx.Observable/<EveryFixedUpdateCore>c__Iterator12::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CEveryFixedUpdateCoreU3Ec__Iterator12_System_Collections_IEnumerator_get_Current_m655997278 (U3CEveryFixedUpdateCoreU3Ec__Iterator12_t976881937 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UniRx.Observable/<EveryFixedUpdateCore>c__Iterator12::MoveNext()
extern "C"  bool U3CEveryFixedUpdateCoreU3Ec__Iterator12_MoveNext_m3260514738 (U3CEveryFixedUpdateCoreU3Ec__Iterator12_t976881937 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.Observable/<EveryFixedUpdateCore>c__Iterator12::Dispose()
extern "C"  void U3CEveryFixedUpdateCoreU3Ec__Iterator12_Dispose_m1468622735 (U3CEveryFixedUpdateCoreU3Ec__Iterator12_t976881937 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.Observable/<EveryFixedUpdateCore>c__Iterator12::Reset()
extern "C"  void U3CEveryFixedUpdateCoreU3Ec__Iterator12_Reset_m1102811775 (U3CEveryFixedUpdateCoreU3Ec__Iterator12_t976881937 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
