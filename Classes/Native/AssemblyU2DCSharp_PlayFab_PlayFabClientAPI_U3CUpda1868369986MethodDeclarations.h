﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.PlayFabClientAPI/<UpdateUserData>c__AnonStorey98
struct U3CUpdateUserDataU3Ec__AnonStorey98_t1868369986;
// PlayFab.CallRequestContainer
struct CallRequestContainer_t432318609;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharpU2Dfirstpass_PlayFab_CallRequestCo432318609.h"

// System.Void PlayFab.PlayFabClientAPI/<UpdateUserData>c__AnonStorey98::.ctor()
extern "C"  void U3CUpdateUserDataU3Ec__AnonStorey98__ctor_m1423012273 (U3CUpdateUserDataU3Ec__AnonStorey98_t1868369986 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/<UpdateUserData>c__AnonStorey98::<>m__85(PlayFab.CallRequestContainer)
extern "C"  void U3CUpdateUserDataU3Ec__AnonStorey98_U3CU3Em__85_m979542796 (U3CUpdateUserDataU3Ec__AnonStorey98_t1868369986 * __this, CallRequestContainer_t432318609 * ___requestContainer0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
