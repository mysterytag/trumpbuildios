﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "System_System_Collections_Generic_Queue_1_gen2545193960MethodDeclarations.h"

// System.Void System.Collections.Generic.Queue`1<System.Collections.Generic.List`1<System.Object>>::.ctor()
#define Queue_1__ctor_m2322214309(__this, method) ((  void (*) (Queue_1_t3342152929 *, const MethodInfo*))Queue_1__ctor_m3042804833_gshared)(__this, method)
// System.Void System.Collections.Generic.Queue`1<System.Collections.Generic.List`1<System.Object>>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define Queue_1_System_Collections_ICollection_CopyTo_m2516809535(__this, ___array0, ___idx1, method) ((  void (*) (Queue_1_t3342152929 *, Il2CppArray *, int32_t, const MethodInfo*))Queue_1_System_Collections_ICollection_CopyTo_m3260144643_gshared)(__this, ___array0, ___idx1, method)
// System.Boolean System.Collections.Generic.Queue`1<System.Collections.Generic.List`1<System.Object>>::System.Collections.ICollection.get_IsSynchronized()
#define Queue_1_System_Collections_ICollection_get_IsSynchronized_m3654928435(__this, method) ((  bool (*) (Queue_1_t3342152929 *, const MethodInfo*))Queue_1_System_Collections_ICollection_get_IsSynchronized_m63917275_gshared)(__this, method)
// System.Object System.Collections.Generic.Queue`1<System.Collections.Generic.List`1<System.Object>>::System.Collections.ICollection.get_SyncRoot()
#define Queue_1_System_Collections_ICollection_get_SyncRoot_m1350589811(__this, method) ((  Il2CppObject * (*) (Queue_1_t3342152929 *, const MethodInfo*))Queue_1_System_Collections_ICollection_get_SyncRoot_m2093948217_gshared)(__this, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.Queue`1<System.Collections.Generic.List`1<System.Object>>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
#define Queue_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m4197558711(__this, method) ((  Il2CppObject* (*) (Queue_1_t3342152929 *, const MethodInfo*))Queue_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m472615211_gshared)(__this, method)
// System.Collections.IEnumerator System.Collections.Generic.Queue`1<System.Collections.Generic.List`1<System.Object>>::System.Collections.IEnumerable.GetEnumerator()
#define Queue_1_System_Collections_IEnumerable_GetEnumerator_m562407054(__this, method) ((  Il2CppObject * (*) (Queue_1_t3342152929 *, const MethodInfo*))Queue_1_System_Collections_IEnumerable_GetEnumerator_m3688614462_gshared)(__this, method)
// System.Void System.Collections.Generic.Queue`1<System.Collections.Generic.List`1<System.Object>>::Clear()
#define Queue_1_Clear_m4023314896(__this, method) ((  void (*) (Queue_1_t3342152929 *, const MethodInfo*))Queue_1_Clear_m448938124_gshared)(__this, method)
// System.Void System.Collections.Generic.Queue`1<System.Collections.Generic.List`1<System.Object>>::CopyTo(T[],System.Int32)
#define Queue_1_CopyTo_m3214907306(__this, ___array0, ___idx1, method) ((  void (*) (Queue_1_t3342152929 *, List_1U5BU5D_t2025921984*, int32_t, const MethodInfo*))Queue_1_CopyTo_m3592753262_gshared)(__this, ___array0, ___idx1, method)
// T System.Collections.Generic.Queue`1<System.Collections.Generic.List`1<System.Object>>::Dequeue()
#define Queue_1_Dequeue_m888233300(__this, method) ((  List_1_t1634065389 * (*) (Queue_1_t3342152929 *, const MethodInfo*))Queue_1_Dequeue_m102813934_gshared)(__this, method)
// T System.Collections.Generic.Queue`1<System.Collections.Generic.List`1<System.Object>>::Peek()
#define Queue_1_Peek_m1338992473(__this, method) ((  List_1_t1634065389 * (*) (Queue_1_t3342152929 *, const MethodInfo*))Queue_1_Peek_m3013356031_gshared)(__this, method)
// System.Void System.Collections.Generic.Queue`1<System.Collections.Generic.List`1<System.Object>>::Enqueue(T)
#define Queue_1_Enqueue_m3213588595(__this, ___item0, method) ((  void (*) (Queue_1_t3342152929 *, List_1_t1634065389 *, const MethodInfo*))Queue_1_Enqueue_m4079343671_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.Queue`1<System.Collections.Generic.List`1<System.Object>>::SetCapacity(System.Int32)
#define Queue_1_SetCapacity_m2745367632(__this, ___new_size0, method) ((  void (*) (Queue_1_t3342152929 *, int32_t, const MethodInfo*))Queue_1_SetCapacity_m1573690380_gshared)(__this, ___new_size0, method)
// System.Int32 System.Collections.Generic.Queue`1<System.Collections.Generic.List`1<System.Object>>::get_Count()
#define Queue_1_get_Count_m659130617(__this, method) ((  int32_t (*) (Queue_1_t3342152929 *, const MethodInfo*))Queue_1_get_Count_m1429559317_gshared)(__this, method)
// System.Collections.Generic.Queue`1/Enumerator<T> System.Collections.Generic.Queue`1<System.Collections.Generic.List`1<System.Object>>::GetEnumerator()
#define Queue_1_GetEnumerator_m2372944228(__this, method) ((  Enumerator_t516807350  (*) (Queue_1_t3342152929 *, const MethodInfo*))Queue_1_GetEnumerator_m3965043378_gshared)(__this, method)
