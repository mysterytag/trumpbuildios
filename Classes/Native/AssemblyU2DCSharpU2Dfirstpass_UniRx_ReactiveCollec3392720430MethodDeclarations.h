﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_ReactiveCollec1573407306MethodDeclarations.h"

// System.Void UniRx.ReactiveCollection`1<UniRx.Tuple`2<System.String,UnityEngine.Sprite>>::.ctor()
#define ReactiveCollection_1__ctor_m405678365(__this, method) ((  void (*) (ReactiveCollection_1_t3392720430 *, const MethodInfo*))ReactiveCollection_1__ctor_m717548565_gshared)(__this, method)
// System.Void UniRx.ReactiveCollection`1<UniRx.Tuple`2<System.String,UnityEngine.Sprite>>::.ctor(System.Collections.Generic.IEnumerable`1<T>)
#define ReactiveCollection_1__ctor_m3801297654(__this, ___collection0, method) ((  void (*) (ReactiveCollection_1_t3392720430 *, Il2CppObject*, const MethodInfo*))ReactiveCollection_1__ctor_m4220134730_gshared)(__this, ___collection0, method)
// System.Void UniRx.ReactiveCollection`1<UniRx.Tuple`2<System.String,UnityEngine.Sprite>>::.ctor(System.Collections.Generic.List`1<T>)
#define ReactiveCollection_1__ctor_m3979921857(__this, ___list0, method) ((  void (*) (ReactiveCollection_1_t3392720430 *, List_1_t2985533912 *, const MethodInfo*))ReactiveCollection_1__ctor_m1910574573_gshared)(__this, ___list0, method)
// System.Void UniRx.ReactiveCollection`1<UniRx.Tuple`2<System.String,UnityEngine.Sprite>>::ClearItems()
#define ReactiveCollection_1_ClearItems_m405180270(__this, method) ((  void (*) (ReactiveCollection_1_t3392720430 *, const MethodInfo*))ReactiveCollection_1_ClearItems_m2487296194_gshared)(__this, method)
// System.Void UniRx.ReactiveCollection`1<UniRx.Tuple`2<System.String,UnityEngine.Sprite>>::InsertItem(System.Int32,T)
#define ReactiveCollection_1_InsertItem_m4068877200(__this, ___index0, ___item1, method) ((  void (*) (ReactiveCollection_1_t3392720430 *, int32_t, Tuple_2_t2188574943 , const MethodInfo*))ReactiveCollection_1_InsertItem_m4232958436_gshared)(__this, ___index0, ___item1, method)
// System.Void UniRx.ReactiveCollection`1<UniRx.Tuple`2<System.String,UnityEngine.Sprite>>::Move(System.Int32,System.Int32)
#define ReactiveCollection_1_Move_m3026672346(__this, ___oldIndex0, ___newIndex1, method) ((  void (*) (ReactiveCollection_1_t3392720430 *, int32_t, int32_t, const MethodInfo*))ReactiveCollection_1_Move_m697532678_gshared)(__this, ___oldIndex0, ___newIndex1, method)
// System.Void UniRx.ReactiveCollection`1<UniRx.Tuple`2<System.String,UnityEngine.Sprite>>::MoveItem(System.Int32,System.Int32)
#define ReactiveCollection_1_MoveItem_m1208660295(__this, ___oldIndex0, ___newIndex1, method) ((  void (*) (ReactiveCollection_1_t3392720430 *, int32_t, int32_t, const MethodInfo*))ReactiveCollection_1_MoveItem_m1629479283_gshared)(__this, ___oldIndex0, ___newIndex1, method)
// System.Void UniRx.ReactiveCollection`1<UniRx.Tuple`2<System.String,UnityEngine.Sprite>>::RemoveItem(System.Int32)
#define ReactiveCollection_1_RemoveItem_m517186819(__this, ___index0, method) ((  void (*) (ReactiveCollection_1_t3392720430 *, int32_t, const MethodInfo*))ReactiveCollection_1_RemoveItem_m3181041751_gshared)(__this, ___index0, method)
// System.Void UniRx.ReactiveCollection`1<UniRx.Tuple`2<System.String,UnityEngine.Sprite>>::SetItem(System.Int32,T)
#define ReactiveCollection_1_SetItem_m4111261829(__this, ___index0, ___item1, method) ((  void (*) (ReactiveCollection_1_t3392720430 *, int32_t, Tuple_2_t2188574943 , const MethodInfo*))ReactiveCollection_1_SetItem_m3920097969_gshared)(__this, ___index0, ___item1, method)
// UniRx.IObservable`1<System.Int32> UniRx.ReactiveCollection`1<UniRx.Tuple`2<System.String,UnityEngine.Sprite>>::ObserveCountChanged(System.Boolean)
#define ReactiveCollection_1_ObserveCountChanged_m1352334298(__this, ___notifyCurrentCount0, method) ((  Il2CppObject* (*) (ReactiveCollection_1_t3392720430 *, bool, const MethodInfo*))ReactiveCollection_1_ObserveCountChanged_m4252337840_gshared)(__this, ___notifyCurrentCount0, method)
// UniRx.IObservable`1<UniRx.Unit> UniRx.ReactiveCollection`1<UniRx.Tuple`2<System.String,UnityEngine.Sprite>>::ObserveReset()
#define ReactiveCollection_1_ObserveReset_m3273825350(__this, method) ((  Il2CppObject* (*) (ReactiveCollection_1_t3392720430 *, const MethodInfo*))ReactiveCollection_1_ObserveReset_m1369659178_gshared)(__this, method)
// UniRx.IObservable`1<UniRx.CollectionAddEvent`1<T>> UniRx.ReactiveCollection`1<UniRx.Tuple`2<System.String,UnityEngine.Sprite>>::ObserveAdd()
#define ReactiveCollection_1_ObserveAdd_m2345721194(__this, method) ((  Il2CppObject* (*) (ReactiveCollection_1_t3392720430 *, const MethodInfo*))ReactiveCollection_1_ObserveAdd_m3081882730_gshared)(__this, method)
// UniRx.IObservable`1<UniRx.CollectionMoveEvent`1<T>> UniRx.ReactiveCollection`1<UniRx.Tuple`2<System.String,UnityEngine.Sprite>>::ObserveMove()
#define ReactiveCollection_1_ObserveMove_m2576715618(__this, method) ((  Il2CppObject* (*) (ReactiveCollection_1_t3392720430 *, const MethodInfo*))ReactiveCollection_1_ObserveMove_m3790976434_gshared)(__this, method)
// UniRx.IObservable`1<UniRx.CollectionRemoveEvent`1<T>> UniRx.ReactiveCollection`1<UniRx.Tuple`2<System.String,UnityEngine.Sprite>>::ObserveRemove()
#define ReactiveCollection_1_ObserveRemove_m837728264(__this, method) ((  Il2CppObject* (*) (ReactiveCollection_1_t3392720430 *, const MethodInfo*))ReactiveCollection_1_ObserveRemove_m3547014418_gshared)(__this, method)
// UniRx.IObservable`1<UniRx.CollectionReplaceEvent`1<T>> UniRx.ReactiveCollection`1<UniRx.Tuple`2<System.String,UnityEngine.Sprite>>::ObserveReplace()
#define ReactiveCollection_1_ObserveReplace_m4060938250(__this, method) ((  Il2CppObject* (*) (ReactiveCollection_1_t3392720430 *, const MethodInfo*))ReactiveCollection_1_ObserveReplace_m504154192_gshared)(__this, method)
// System.Void UniRx.ReactiveCollection`1<UniRx.Tuple`2<System.String,UnityEngine.Sprite>>::Dispose()
#define ReactiveCollection_1_Dispose_m3896448998(__this, method) ((  void (*) (ReactiveCollection_1_t3392720430 *, const MethodInfo*))ReactiveCollection_1_Dispose_m2267686674_gshared)(__this, method)
// System.Int32 UniRx.ReactiveCollection`1<UniRx.Tuple`2<System.String,UnityEngine.Sprite>>::<ObserveCountChanged>m__C3()
#define ReactiveCollection_1_U3CObserveCountChangedU3Em__C3_m3403406665(__this, method) ((  int32_t (*) (ReactiveCollection_1_t3392720430 *, const MethodInfo*))ReactiveCollection_1_U3CObserveCountChangedU3Em__C3_m2530644681_gshared)(__this, method)
