﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_K4105386200MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.String,PlayFab.UUnit.Region>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define KeyCollection__ctor_m215373288(__this, ___dictionary0, method) ((  void (*) (KeyCollection_t2755765374 *, Dictionary_2_t432490094 *, const MethodInfo*))KeyCollection__ctor_m3026266106_gshared)(__this, ___dictionary0, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.String,PlayFab.UUnit.Region>::System.Collections.Generic.ICollection<TKey>.Add(TKey)
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m1536881262(__this, ___item0, method) ((  void (*) (KeyCollection_t2755765374 *, String_t*, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m1360370972_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.String,PlayFab.UUnit.Region>::System.Collections.Generic.ICollection<TKey>.Clear()
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m3442924389(__this, method) ((  void (*) (KeyCollection_t2755765374 *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m890788243_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.String,PlayFab.UUnit.Region>::System.Collections.Generic.ICollection<TKey>.Contains(TKey)
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m1911776800(__this, ___item0, method) ((  bool (*) (KeyCollection_t2755765374 *, String_t*, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m427702322_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.String,PlayFab.UUnit.Region>::System.Collections.Generic.ICollection<TKey>.Remove(TKey)
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m270720901(__this, ___item0, method) ((  bool (*) (KeyCollection_t2755765374 *, String_t*, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m3187609111_gshared)(__this, ___item0, method)
// System.Collections.Generic.IEnumerator`1<TKey> System.Collections.Generic.Dictionary`2/KeyCollection<System.String,PlayFab.UUnit.Region>::System.Collections.Generic.IEnumerable<TKey>.GetEnumerator()
#define KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m2938154167(__this, method) ((  Il2CppObject* (*) (KeyCollection_t2755765374 *, const MethodInfo*))KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m4176485605_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.String,PlayFab.UUnit.Region>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define KeyCollection_System_Collections_ICollection_CopyTo_m2631520343(__this, ___array0, ___index1, method) ((  void (*) (KeyCollection_t2755765374 *, Il2CppArray *, int32_t, const MethodInfo*))KeyCollection_System_Collections_ICollection_CopyTo_m2482600325_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2/KeyCollection<System.String,PlayFab.UUnit.Region>::System.Collections.IEnumerable.GetEnumerator()
#define KeyCollection_System_Collections_IEnumerable_GetEnumerator_m3132607654(__this, method) ((  Il2CppObject * (*) (KeyCollection_t2755765374 *, const MethodInfo*))KeyCollection_System_Collections_IEnumerable_GetEnumerator_m275187028_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.String,PlayFab.UUnit.Region>::System.Collections.Generic.ICollection<TKey>.get_IsReadOnly()
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m1852560769(__this, method) ((  bool (*) (KeyCollection_t2755765374 *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m1586129683_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.String,PlayFab.UUnit.Region>::System.Collections.ICollection.get_IsSynchronized()
#define KeyCollection_System_Collections_ICollection_get_IsSynchronized_m1175559027(__this, method) ((  bool (*) (KeyCollection_t2755765374 *, const MethodInfo*))KeyCollection_System_Collections_ICollection_get_IsSynchronized_m3725631365_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/KeyCollection<System.String,PlayFab.UUnit.Region>::System.Collections.ICollection.get_SyncRoot()
#define KeyCollection_System_Collections_ICollection_get_SyncRoot_m1820698085(__this, method) ((  Il2CppObject * (*) (KeyCollection_t2755765374 *, const MethodInfo*))KeyCollection_System_Collections_ICollection_get_SyncRoot_m2698354551_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.String,PlayFab.UUnit.Region>::CopyTo(TKey[],System.Int32)
#define KeyCollection_CopyTo_m1280662685(__this, ___array0, ___index1, method) ((  void (*) (KeyCollection_t2755765374 *, StringU5BU5D_t2956870243*, int32_t, const MethodInfo*))KeyCollection_CopyTo_m798815919_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/KeyCollection<System.String,PlayFab.UUnit.Region>::GetEnumerator()
#define KeyCollection_GetEnumerator_m2165475690(__this, method) ((  Enumerator_t199518036  (*) (KeyCollection_t2755765374 *, const MethodInfo*))KeyCollection_GetEnumerator_m2079534716_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2/KeyCollection<System.String,PlayFab.UUnit.Region>::get_Count()
#define KeyCollection_get_Count_m1976642093(__this, method) ((  int32_t (*) (KeyCollection_t2755765374 *, const MethodInfo*))KeyCollection_get_Count_m1692608575_gshared)(__this, method)
