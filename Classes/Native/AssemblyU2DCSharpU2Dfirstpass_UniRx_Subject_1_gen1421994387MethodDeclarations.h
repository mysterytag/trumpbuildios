﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Subject`1<UniRx.DictionaryReplaceEvent`2<System.Object,System.Object>>
struct Subject_1_t1421994387;
// System.Exception
struct Exception_t1967233988;
// System.IDisposable
struct IDisposable_t1628921374;
// UniRx.IObserver`1<UniRx.DictionaryReplaceEvent`2<System.Object,System.Object>>
struct IObserver_1_t1695958670;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Exception1967233988.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_DictionaryRepl3778927063.h"

// System.Void UniRx.Subject`1<UniRx.DictionaryReplaceEvent`2<System.Object,System.Object>>::.ctor()
extern "C"  void Subject_1__ctor_m785420670_gshared (Subject_1_t1421994387 * __this, const MethodInfo* method);
#define Subject_1__ctor_m785420670(__this, method) ((  void (*) (Subject_1_t1421994387 *, const MethodInfo*))Subject_1__ctor_m785420670_gshared)(__this, method)
// System.Boolean UniRx.Subject`1<UniRx.DictionaryReplaceEvent`2<System.Object,System.Object>>::get_HasObservers()
extern "C"  bool Subject_1_get_HasObservers_m1560629326_gshared (Subject_1_t1421994387 * __this, const MethodInfo* method);
#define Subject_1_get_HasObservers_m1560629326(__this, method) ((  bool (*) (Subject_1_t1421994387 *, const MethodInfo*))Subject_1_get_HasObservers_m1560629326_gshared)(__this, method)
// System.Void UniRx.Subject`1<UniRx.DictionaryReplaceEvent`2<System.Object,System.Object>>::OnCompleted()
extern "C"  void Subject_1_OnCompleted_m3030134408_gshared (Subject_1_t1421994387 * __this, const MethodInfo* method);
#define Subject_1_OnCompleted_m3030134408(__this, method) ((  void (*) (Subject_1_t1421994387 *, const MethodInfo*))Subject_1_OnCompleted_m3030134408_gshared)(__this, method)
// System.Void UniRx.Subject`1<UniRx.DictionaryReplaceEvent`2<System.Object,System.Object>>::OnError(System.Exception)
extern "C"  void Subject_1_OnError_m135091125_gshared (Subject_1_t1421994387 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method);
#define Subject_1_OnError_m135091125(__this, ___error0, method) ((  void (*) (Subject_1_t1421994387 *, Exception_t1967233988 *, const MethodInfo*))Subject_1_OnError_m135091125_gshared)(__this, ___error0, method)
// System.Void UniRx.Subject`1<UniRx.DictionaryReplaceEvent`2<System.Object,System.Object>>::OnNext(T)
extern "C"  void Subject_1_OnNext_m1705723046_gshared (Subject_1_t1421994387 * __this, DictionaryReplaceEvent_2_t3778927063  ___value0, const MethodInfo* method);
#define Subject_1_OnNext_m1705723046(__this, ___value0, method) ((  void (*) (Subject_1_t1421994387 *, DictionaryReplaceEvent_2_t3778927063 , const MethodInfo*))Subject_1_OnNext_m1705723046_gshared)(__this, ___value0, method)
// System.IDisposable UniRx.Subject`1<UniRx.DictionaryReplaceEvent`2<System.Object,System.Object>>::Subscribe(UniRx.IObserver`1<T>)
extern "C"  Il2CppObject * Subject_1_Subscribe_m1668270269_gshared (Subject_1_t1421994387 * __this, Il2CppObject* ___observer0, const MethodInfo* method);
#define Subject_1_Subscribe_m1668270269(__this, ___observer0, method) ((  Il2CppObject * (*) (Subject_1_t1421994387 *, Il2CppObject*, const MethodInfo*))Subject_1_Subscribe_m1668270269_gshared)(__this, ___observer0, method)
// System.Void UniRx.Subject`1<UniRx.DictionaryReplaceEvent`2<System.Object,System.Object>>::Dispose()
extern "C"  void Subject_1_Dispose_m3068270139_gshared (Subject_1_t1421994387 * __this, const MethodInfo* method);
#define Subject_1_Dispose_m3068270139(__this, method) ((  void (*) (Subject_1_t1421994387 *, const MethodInfo*))Subject_1_Dispose_m3068270139_gshared)(__this, method)
// System.Void UniRx.Subject`1<UniRx.DictionaryReplaceEvent`2<System.Object,System.Object>>::ThrowIfDisposed()
extern "C"  void Subject_1_ThrowIfDisposed_m3511637188_gshared (Subject_1_t1421994387 * __this, const MethodInfo* method);
#define Subject_1_ThrowIfDisposed_m3511637188(__this, method) ((  void (*) (Subject_1_t1421994387 *, const MethodInfo*))Subject_1_ThrowIfDisposed_m3511637188_gshared)(__this, method)
// System.Boolean UniRx.Subject`1<UniRx.DictionaryReplaceEvent`2<System.Object,System.Object>>::IsRequiredSubscribeOnCurrentThread()
extern "C"  bool Subject_1_IsRequiredSubscribeOnCurrentThread_m3274147269_gshared (Subject_1_t1421994387 * __this, const MethodInfo* method);
#define Subject_1_IsRequiredSubscribeOnCurrentThread_m3274147269(__this, method) ((  bool (*) (Subject_1_t1421994387 *, const MethodInfo*))Subject_1_IsRequiredSubscribeOnCurrentThread_m3274147269_gshared)(__this, method)
