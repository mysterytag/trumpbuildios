﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UniRx.Operators.ThrottleObservable`1/Throttle<System.Int64>
struct Throttle_t1826605473;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Operators.ThrottleObservable`1/Throttle/<OnNext>c__AnonStorey6A<System.Int64>
struct  U3COnNextU3Ec__AnonStorey6A_t4267933177  : public Il2CppObject
{
public:
	// System.UInt64 UniRx.Operators.ThrottleObservable`1/Throttle/<OnNext>c__AnonStorey6A::currentid
	uint64_t ___currentid_0;
	// UniRx.Operators.ThrottleObservable`1/Throttle<T> UniRx.Operators.ThrottleObservable`1/Throttle/<OnNext>c__AnonStorey6A::<>f__this
	Throttle_t1826605473 * ___U3CU3Ef__this_1;

public:
	inline static int32_t get_offset_of_currentid_0() { return static_cast<int32_t>(offsetof(U3COnNextU3Ec__AnonStorey6A_t4267933177, ___currentid_0)); }
	inline uint64_t get_currentid_0() const { return ___currentid_0; }
	inline uint64_t* get_address_of_currentid_0() { return &___currentid_0; }
	inline void set_currentid_0(uint64_t value)
	{
		___currentid_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3Ef__this_1() { return static_cast<int32_t>(offsetof(U3COnNextU3Ec__AnonStorey6A_t4267933177, ___U3CU3Ef__this_1)); }
	inline Throttle_t1826605473 * get_U3CU3Ef__this_1() const { return ___U3CU3Ef__this_1; }
	inline Throttle_t1826605473 ** get_address_of_U3CU3Ef__this_1() { return &___U3CU3Ef__this_1; }
	inline void set_U3CU3Ef__this_1(Throttle_t1826605473 * value)
	{
		___U3CU3Ef__this_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__this_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
