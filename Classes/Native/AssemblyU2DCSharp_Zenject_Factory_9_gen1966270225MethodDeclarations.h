﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Zenject.Factory`9<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>
struct Factory_9_t1966270225;
// System.Type
struct Type_t;
// System.Type[]
struct TypeU5BU5D_t3431720054;
// Zenject.DiContainer
struct DiContainer_t2383114449;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"

// System.Void Zenject.Factory`9<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>::.ctor()
extern "C"  void Factory_9__ctor_m664076627_gshared (Factory_9_t1966270225 * __this, const MethodInfo* method);
#define Factory_9__ctor_m664076627(__this, method) ((  void (*) (Factory_9_t1966270225 *, const MethodInfo*))Factory_9__ctor_m664076627_gshared)(__this, method)
// System.Type Zenject.Factory`9<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>::get_ConstructedType()
extern "C"  Type_t * Factory_9_get_ConstructedType_m1274965118_gshared (Factory_9_t1966270225 * __this, const MethodInfo* method);
#define Factory_9_get_ConstructedType_m1274965118(__this, method) ((  Type_t * (*) (Factory_9_t1966270225 *, const MethodInfo*))Factory_9_get_ConstructedType_m1274965118_gshared)(__this, method)
// System.Type[] Zenject.Factory`9<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>::get_ProvidedTypes()
extern "C"  TypeU5BU5D_t3431720054* Factory_9_get_ProvidedTypes_m4250166758_gshared (Factory_9_t1966270225 * __this, const MethodInfo* method);
#define Factory_9_get_ProvidedTypes_m4250166758(__this, method) ((  TypeU5BU5D_t3431720054* (*) (Factory_9_t1966270225 *, const MethodInfo*))Factory_9_get_ProvidedTypes_m4250166758_gshared)(__this, method)
// Zenject.DiContainer Zenject.Factory`9<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>::get_Container()
extern "C"  DiContainer_t2383114449 * Factory_9_get_Container_m468073395_gshared (Factory_9_t1966270225 * __this, const MethodInfo* method);
#define Factory_9_get_Container_m468073395(__this, method) ((  DiContainer_t2383114449 * (*) (Factory_9_t1966270225 *, const MethodInfo*))Factory_9_get_Container_m468073395_gshared)(__this, method)
// TValue Zenject.Factory`9<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>::Create(TParam1,TParam2,TParam3,TParam4,TParam5,TParam6,TParam7,TParam8)
extern "C"  Il2CppObject * Factory_9_Create_m584920829_gshared (Factory_9_t1966270225 * __this, Il2CppObject * ___param10, Il2CppObject * ___param21, Il2CppObject * ___param32, Il2CppObject * ___param43, Il2CppObject * ___param54, Il2CppObject * ___param65, Il2CppObject * ___param76, Il2CppObject * ___param87, const MethodInfo* method);
#define Factory_9_Create_m584920829(__this, ___param10, ___param21, ___param32, ___param43, ___param54, ___param65, ___param76, ___param87, method) ((  Il2CppObject * (*) (Factory_9_t1966270225 *, Il2CppObject *, Il2CppObject *, Il2CppObject *, Il2CppObject *, Il2CppObject *, Il2CppObject *, Il2CppObject *, Il2CppObject *, const MethodInfo*))Factory_9_Create_m584920829_gshared)(__this, ___param10, ___param21, ___param32, ___param43, ___param54, ___param65, ___param76, ___param87, method)
