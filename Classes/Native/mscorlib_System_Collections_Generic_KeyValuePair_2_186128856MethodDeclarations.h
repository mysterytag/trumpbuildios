﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_g54746374MethodDeclarations.h"

// System.Void System.Collections.Generic.KeyValuePair`2<System.Double,System.String>::.ctor(TKey,TValue)
#define KeyValuePair_2__ctor_m768006107(__this, ___key0, ___value1, method) ((  void (*) (KeyValuePair_2_t186128856 *, double, String_t*, const MethodInfo*))KeyValuePair_2__ctor_m3126525293_gshared)(__this, ___key0, ___value1, method)
// TKey System.Collections.Generic.KeyValuePair`2<System.Double,System.String>::get_Key()
#define KeyValuePair_2_get_Key_m732204392(__this, method) ((  double (*) (KeyValuePair_2_t186128856 *, const MethodInfo*))KeyValuePair_2_get_Key_m2666415963_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Double,System.String>::set_Key(TKey)
#define KeyValuePair_2_set_Key_m126993774(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t186128856 *, double, const MethodInfo*))KeyValuePair_2_set_Key_m1029500060_gshared)(__this, ___value0, method)
// TValue System.Collections.Generic.KeyValuePair`2<System.Double,System.String>::get_Value()
#define KeyValuePair_2_get_Value_m3407945595(__this, method) ((  String_t* (*) (KeyValuePair_2_t186128856 *, const MethodInfo*))KeyValuePair_2_get_Value_m3787089215_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Double,System.String>::set_Value(TValue)
#define KeyValuePair_2_set_Value_m2844832366(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t186128856 *, String_t*, const MethodInfo*))KeyValuePair_2_set_Value_m704156316_gshared)(__this, ___value0, method)
// System.String System.Collections.Generic.KeyValuePair`2<System.Double,System.String>::ToString()
#define KeyValuePair_2_ToString_m2226416730(__this, method) ((  String_t* (*) (KeyValuePair_2_t186128856 *, const MethodInfo*))KeyValuePair_2_ToString_m201868268_gshared)(__this, method)
