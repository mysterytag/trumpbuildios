﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// AnonymousStream`1/<Listen>c__AnonStorey12C<System.Object>
struct U3CListenU3Ec__AnonStorey12C_t1220913119;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"

// System.Void AnonymousStream`1/<Listen>c__AnonStorey12C<System.Object>::.ctor()
extern "C"  void U3CListenU3Ec__AnonStorey12C__ctor_m4165821334_gshared (U3CListenU3Ec__AnonStorey12C_t1220913119 * __this, const MethodInfo* method);
#define U3CListenU3Ec__AnonStorey12C__ctor_m4165821334(__this, method) ((  void (*) (U3CListenU3Ec__AnonStorey12C_t1220913119 *, const MethodInfo*))U3CListenU3Ec__AnonStorey12C__ctor_m4165821334_gshared)(__this, method)
// System.Void AnonymousStream`1/<Listen>c__AnonStorey12C<System.Object>::<>m__1C0(T)
extern "C"  void U3CListenU3Ec__AnonStorey12C_U3CU3Em__1C0_m4074916829_gshared (U3CListenU3Ec__AnonStorey12C_t1220913119 * __this, Il2CppObject * ____0, const MethodInfo* method);
#define U3CListenU3Ec__AnonStorey12C_U3CU3Em__1C0_m4074916829(__this, ____0, method) ((  void (*) (U3CListenU3Ec__AnonStorey12C_t1220913119 *, Il2CppObject *, const MethodInfo*))U3CListenU3Ec__AnonStorey12C_U3CU3Em__1C0_m4074916829_gshared)(__this, ____0, method)
