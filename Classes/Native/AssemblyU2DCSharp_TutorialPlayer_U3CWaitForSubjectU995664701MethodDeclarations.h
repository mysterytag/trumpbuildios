﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// TutorialPlayer/<WaitForSubject>c__AnonStoreyEE
struct U3CWaitForSubjectU3Ec__AnonStoreyEE_t995664701;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String968488902.h"

// System.Void TutorialPlayer/<WaitForSubject>c__AnonStoreyEE::.ctor()
extern "C"  void U3CWaitForSubjectU3Ec__AnonStoreyEE__ctor_m367250958 (U3CWaitForSubjectU3Ec__AnonStoreyEE_t995664701 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TutorialPlayer/<WaitForSubject>c__AnonStoreyEE::<>m__14A(System.String)
extern "C"  void U3CWaitForSubjectU3Ec__AnonStoreyEE_U3CU3Em__14A_m1156141881 (U3CWaitForSubjectU3Ec__AnonStoreyEE_t995664701 * __this, String_t* ____0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean TutorialPlayer/<WaitForSubject>c__AnonStoreyEE::<>m__14B()
extern "C"  bool U3CWaitForSubjectU3Ec__AnonStoreyEE_U3CU3Em__14B_m573908958 (U3CWaitForSubjectU3Ec__AnonStoreyEE_t995664701 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
