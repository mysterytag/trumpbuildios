﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String[]
struct StringU5BU5D_t2956870243;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utils2
struct  Utils2_t2543841729  : public Il2CppObject
{
public:

public:
};

struct Utils2_t2543841729_StaticFields
{
public:
	// System.String[] Utils2::numberNames
	StringU5BU5D_t2956870243* ___numberNames_0;
	// System.String[] Utils2::formats
	StringU5BU5D_t2956870243* ___formats_1;

public:
	inline static int32_t get_offset_of_numberNames_0() { return static_cast<int32_t>(offsetof(Utils2_t2543841729_StaticFields, ___numberNames_0)); }
	inline StringU5BU5D_t2956870243* get_numberNames_0() const { return ___numberNames_0; }
	inline StringU5BU5D_t2956870243** get_address_of_numberNames_0() { return &___numberNames_0; }
	inline void set_numberNames_0(StringU5BU5D_t2956870243* value)
	{
		___numberNames_0 = value;
		Il2CppCodeGenWriteBarrier(&___numberNames_0, value);
	}

	inline static int32_t get_offset_of_formats_1() { return static_cast<int32_t>(offsetof(Utils2_t2543841729_StaticFields, ___formats_1)); }
	inline StringU5BU5D_t2956870243* get_formats_1() const { return ___formats_1; }
	inline StringU5BU5D_t2956870243** get_address_of_formats_1() { return &___formats_1; }
	inline void set_formats_1(StringU5BU5D_t2956870243* value)
	{
		___formats_1 = value;
		Il2CppCodeGenWriteBarrier(&___formats_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
