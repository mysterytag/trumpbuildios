﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_I1897310919MethodDeclarations.h"

// System.Void UniRx.InternalUtil.ImmutableList`1<UniRx.IObserver`1<System.Int64>>::.ctor()
#define ImmutableList_1__ctor_m3124366627(__this, method) ((  void (*) (ImmutableList_1_t1824650988 *, const MethodInfo*))ImmutableList_1__ctor_m2467204245_gshared)(__this, method)
// System.Void UniRx.InternalUtil.ImmutableList`1<UniRx.IObserver`1<System.Int64>>::.ctor(T[])
#define ImmutableList_1__ctor_m1771284729(__this, ___data0, method) ((  void (*) (ImmutableList_1_t1824650988 *, IObserver_1U5BU5D_t445342820*, const MethodInfo*))ImmutableList_1__ctor_m707697735_gshared)(__this, ___data0, method)
// System.Void UniRx.InternalUtil.ImmutableList`1<UniRx.IObserver`1<System.Int64>>::.cctor()
#define ImmutableList_1__cctor_m1883988714(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))ImmutableList_1__cctor_m2986791352_gshared)(__this /* static, unused */, method)
// T[] UniRx.InternalUtil.ImmutableList`1<UniRx.IObserver`1<System.Int64>>::get_Data()
#define ImmutableList_1_get_Data_m3123086775(__this, method) ((  IObserver_1U5BU5D_t445342820* (*) (ImmutableList_1_t1824650988 *, const MethodInfo*))ImmutableList_1_get_Data_m1676800965_gshared)(__this, method)
// UniRx.InternalUtil.ImmutableList`1<T> UniRx.InternalUtil.ImmutableList`1<UniRx.IObserver`1<System.Int64>>::Add(T)
#define ImmutableList_1_Add_m908004981(__this, ___value0, method) ((  ImmutableList_1_t1824650988 * (*) (ImmutableList_1_t1824650988 *, Il2CppObject*, const MethodInfo*))ImmutableList_1_Add_m948892931_gshared)(__this, ___value0, method)
// UniRx.InternalUtil.ImmutableList`1<T> UniRx.InternalUtil.ImmutableList`1<UniRx.IObserver`1<System.Int64>>::Remove(T)
#define ImmutableList_1_Remove_m3216710816(__this, ___value0, method) ((  ImmutableList_1_t1824650988 * (*) (ImmutableList_1_t1824650988 *, Il2CppObject*, const MethodInfo*))ImmutableList_1_Remove_m1538917202_gshared)(__this, ___value0, method)
// System.Int32 UniRx.InternalUtil.ImmutableList`1<UniRx.IObserver`1<System.Int64>>::IndexOf(T)
#define ImmutableList_1_IndexOf_m2897333150(__this, ___value0, method) ((  int32_t (*) (ImmutableList_1_t1824650988 *, Il2CppObject*, const MethodInfo*))ImmutableList_1_IndexOf_m3149323628_gshared)(__this, ___value0, method)
