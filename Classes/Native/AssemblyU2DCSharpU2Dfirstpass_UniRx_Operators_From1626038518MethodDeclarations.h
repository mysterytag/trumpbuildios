﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.FromEventObservable`2<System.Object,System.Boolean>
struct FromEventObservable_2_t1626038518;
// System.Func`2<System.Action`1<System.Boolean>,System.Object>
struct Func_2_t599409222;
// System.Action`1<System.Object>
struct Action_1_t985559125;
// System.IDisposable
struct IDisposable_t1628921374;
// UniRx.IObserver`1<System.Boolean>
struct IObserver_1_t2423004244;

#include "codegen/il2cpp-codegen.h"

// System.Void UniRx.Operators.FromEventObservable`2<System.Object,System.Boolean>::.ctor(System.Func`2<System.Action`1<TEventArgs>,TDelegate>,System.Action`1<TDelegate>,System.Action`1<TDelegate>)
extern "C"  void FromEventObservable_2__ctor_m851348427_gshared (FromEventObservable_2_t1626038518 * __this, Func_2_t599409222 * ___conversion0, Action_1_t985559125 * ___addHandler1, Action_1_t985559125 * ___removeHandler2, const MethodInfo* method);
#define FromEventObservable_2__ctor_m851348427(__this, ___conversion0, ___addHandler1, ___removeHandler2, method) ((  void (*) (FromEventObservable_2_t1626038518 *, Func_2_t599409222 *, Action_1_t985559125 *, Action_1_t985559125 *, const MethodInfo*))FromEventObservable_2__ctor_m851348427_gshared)(__this, ___conversion0, ___addHandler1, ___removeHandler2, method)
// System.IDisposable UniRx.Operators.FromEventObservable`2<System.Object,System.Boolean>::SubscribeCore(UniRx.IObserver`1<TEventArgs>,System.IDisposable)
extern "C"  Il2CppObject * FromEventObservable_2_SubscribeCore_m4127373153_gshared (FromEventObservable_2_t1626038518 * __this, Il2CppObject* ___observer0, Il2CppObject * ___cancel1, const MethodInfo* method);
#define FromEventObservable_2_SubscribeCore_m4127373153(__this, ___observer0, ___cancel1, method) ((  Il2CppObject * (*) (FromEventObservable_2_t1626038518 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))FromEventObservable_2_SubscribeCore_m4127373153_gshared)(__this, ___observer0, ___cancel1, method)
