﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.List`1<Zenject.TypeValuePair>
struct List_1_t1417891359;
// System.Collections.Generic.IEnumerable`1<System.Object>
struct IEnumerable_1_t3709260776;
// System.Type
struct Type_t;
// System.Object
struct Il2CppObject;
// Zenject.TypeValuePair
struct TypeValuePair_t620932390;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Type2779229935.h"
#include "mscorlib_System_Object837106420.h"

// System.Collections.Generic.List`1<Zenject.TypeValuePair> Zenject.InstantiateUtil::CreateTypeValueList(System.Collections.Generic.IEnumerable`1<System.Object>)
extern "C"  List_1_t1417891359 * InstantiateUtil_CreateTypeValueList_m1033639521 (Il2CppObject * __this /* static, unused */, Il2CppObject* ___args0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Zenject.InstantiateUtil::PopValueWithType(System.Collections.Generic.List`1<Zenject.TypeValuePair>,System.Type,System.Object&)
extern "C"  bool InstantiateUtil_PopValueWithType_m1065011875 (Il2CppObject * __this /* static, unused */, List_1_t1417891359 * ___extraArgMap0, Type_t * ___injectedFieldType1, Il2CppObject ** ___value2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Zenject.TypeValuePair Zenject.InstantiateUtil::<CreateTypeValueList>m__2AA(System.Object)
extern "C"  TypeValuePair_t620932390 * InstantiateUtil_U3CCreateTypeValueListU3Em__2AA_m572446929 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___x0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
