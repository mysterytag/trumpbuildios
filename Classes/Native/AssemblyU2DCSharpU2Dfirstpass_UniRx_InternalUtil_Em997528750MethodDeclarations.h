﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.InternalUtil.EmptyObserver`1<UnityEngine.Color>
struct EmptyObserver_1_t997528750;
// System.Exception
struct Exception_t1967233988;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Exception1967233988.h"
#include "UnityEngine_UnityEngine_Color1588175760.h"

// System.Void UniRx.InternalUtil.EmptyObserver`1<UnityEngine.Color>::.ctor()
extern "C"  void EmptyObserver_1__ctor_m2117782918_gshared (EmptyObserver_1_t997528750 * __this, const MethodInfo* method);
#define EmptyObserver_1__ctor_m2117782918(__this, method) ((  void (*) (EmptyObserver_1_t997528750 *, const MethodInfo*))EmptyObserver_1__ctor_m2117782918_gshared)(__this, method)
// System.Void UniRx.InternalUtil.EmptyObserver`1<UnityEngine.Color>::.cctor()
extern "C"  void EmptyObserver_1__cctor_m744664807_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define EmptyObserver_1__cctor_m744664807(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))EmptyObserver_1__cctor_m744664807_gshared)(__this /* static, unused */, method)
// System.Void UniRx.InternalUtil.EmptyObserver`1<UnityEngine.Color>::OnCompleted()
extern "C"  void EmptyObserver_1_OnCompleted_m3919266960_gshared (EmptyObserver_1_t997528750 * __this, const MethodInfo* method);
#define EmptyObserver_1_OnCompleted_m3919266960(__this, method) ((  void (*) (EmptyObserver_1_t997528750 *, const MethodInfo*))EmptyObserver_1_OnCompleted_m3919266960_gshared)(__this, method)
// System.Void UniRx.InternalUtil.EmptyObserver`1<UnityEngine.Color>::OnError(System.Exception)
extern "C"  void EmptyObserver_1_OnError_m1832099261_gshared (EmptyObserver_1_t997528750 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method);
#define EmptyObserver_1_OnError_m1832099261(__this, ___error0, method) ((  void (*) (EmptyObserver_1_t997528750 *, Exception_t1967233988 *, const MethodInfo*))EmptyObserver_1_OnError_m1832099261_gshared)(__this, ___error0, method)
// System.Void UniRx.InternalUtil.EmptyObserver`1<UnityEngine.Color>::OnNext(T)
extern "C"  void EmptyObserver_1_OnNext_m2205589166_gshared (EmptyObserver_1_t997528750 * __this, Color_t1588175760  ___value0, const MethodInfo* method);
#define EmptyObserver_1_OnNext_m2205589166(__this, ___value0, method) ((  void (*) (EmptyObserver_1_t997528750 *, Color_t1588175760 , const MethodInfo*))EmptyObserver_1_OnNext_m2205589166_gshared)(__this, ___value0, method)
