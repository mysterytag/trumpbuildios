﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.OperatorObserverBase`2<System.Int64,System.Int32>
struct OperatorObserverBase_2_t3939730814;
// UniRx.IObserver`1<System.Int32>
struct IObserver_1_t764446394;
// System.IDisposable
struct IDisposable_t1628921374;

#include "codegen/il2cpp-codegen.h"

// System.Void UniRx.Operators.OperatorObserverBase`2<System.Int64,System.Int32>::.ctor(UniRx.IObserver`1<TResult>,System.IDisposable)
extern "C"  void OperatorObserverBase_2__ctor_m2053408303_gshared (OperatorObserverBase_2_t3939730814 * __this, Il2CppObject* ___observer0, Il2CppObject * ___cancel1, const MethodInfo* method);
#define OperatorObserverBase_2__ctor_m2053408303(__this, ___observer0, ___cancel1, method) ((  void (*) (OperatorObserverBase_2_t3939730814 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))OperatorObserverBase_2__ctor_m2053408303_gshared)(__this, ___observer0, ___cancel1, method)
// System.Void UniRx.Operators.OperatorObserverBase`2<System.Int64,System.Int32>::Dispose()
extern "C"  void OperatorObserverBase_2_Dispose_m1865236251_gshared (OperatorObserverBase_2_t3939730814 * __this, const MethodInfo* method);
#define OperatorObserverBase_2_Dispose_m1865236251(__this, method) ((  void (*) (OperatorObserverBase_2_t3939730814 *, const MethodInfo*))OperatorObserverBase_2_Dispose_m1865236251_gshared)(__this, method)
