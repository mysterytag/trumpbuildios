﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.IEnumerator
struct IEnumerator_t287207039;
// IProcess
struct IProcess_t4026237414;
// ILivingState
struct ILivingState_t4195066719;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CoroProcess
struct  CoroProcess_t1167355494  : public Il2CppObject
{
public:
	// System.Collections.IEnumerator CoroProcess::generator
	Il2CppObject * ___generator_0;
	// IProcess CoroProcess::current
	Il2CppObject * ___current_1;
	// ILivingState CoroProcess::owner
	Il2CppObject * ___owner_2;
	// System.Boolean CoroProcess::<finished>k__BackingField
	bool ___U3CfinishedU3Ek__BackingField_3;

public:
	inline static int32_t get_offset_of_generator_0() { return static_cast<int32_t>(offsetof(CoroProcess_t1167355494, ___generator_0)); }
	inline Il2CppObject * get_generator_0() const { return ___generator_0; }
	inline Il2CppObject ** get_address_of_generator_0() { return &___generator_0; }
	inline void set_generator_0(Il2CppObject * value)
	{
		___generator_0 = value;
		Il2CppCodeGenWriteBarrier(&___generator_0, value);
	}

	inline static int32_t get_offset_of_current_1() { return static_cast<int32_t>(offsetof(CoroProcess_t1167355494, ___current_1)); }
	inline Il2CppObject * get_current_1() const { return ___current_1; }
	inline Il2CppObject ** get_address_of_current_1() { return &___current_1; }
	inline void set_current_1(Il2CppObject * value)
	{
		___current_1 = value;
		Il2CppCodeGenWriteBarrier(&___current_1, value);
	}

	inline static int32_t get_offset_of_owner_2() { return static_cast<int32_t>(offsetof(CoroProcess_t1167355494, ___owner_2)); }
	inline Il2CppObject * get_owner_2() const { return ___owner_2; }
	inline Il2CppObject ** get_address_of_owner_2() { return &___owner_2; }
	inline void set_owner_2(Il2CppObject * value)
	{
		___owner_2 = value;
		Il2CppCodeGenWriteBarrier(&___owner_2, value);
	}

	inline static int32_t get_offset_of_U3CfinishedU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(CoroProcess_t1167355494, ___U3CfinishedU3Ek__BackingField_3)); }
	inline bool get_U3CfinishedU3Ek__BackingField_3() const { return ___U3CfinishedU3Ek__BackingField_3; }
	inline bool* get_address_of_U3CfinishedU3Ek__BackingField_3() { return &___U3CfinishedU3Ek__BackingField_3; }
	inline void set_U3CfinishedU3Ek__BackingField_3(bool value)
	{
		___U3CfinishedU3Ek__BackingField_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
