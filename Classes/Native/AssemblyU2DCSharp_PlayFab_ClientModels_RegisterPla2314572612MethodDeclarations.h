﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.ClientModels.RegisterPlayFabUserRequest
struct RegisterPlayFabUserRequest_t2314572612;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String968488902.h"
#include "mscorlib_System_Nullable_1_gen3097043249.h"

// System.Void PlayFab.ClientModels.RegisterPlayFabUserRequest::.ctor()
extern "C"  void RegisterPlayFabUserRequest__ctor_m801848569 (RegisterPlayFabUserRequest_t2314572612 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PlayFab.ClientModels.RegisterPlayFabUserRequest::get_TitleId()
extern "C"  String_t* RegisterPlayFabUserRequest_get_TitleId_m2260331428 (RegisterPlayFabUserRequest_t2314572612 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.RegisterPlayFabUserRequest::set_TitleId(System.String)
extern "C"  void RegisterPlayFabUserRequest_set_TitleId_m1433758037 (RegisterPlayFabUserRequest_t2314572612 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PlayFab.ClientModels.RegisterPlayFabUserRequest::get_Username()
extern "C"  String_t* RegisterPlayFabUserRequest_get_Username_m1197465735 (RegisterPlayFabUserRequest_t2314572612 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.RegisterPlayFabUserRequest::set_Username(System.String)
extern "C"  void RegisterPlayFabUserRequest_set_Username_m2266355012 (RegisterPlayFabUserRequest_t2314572612 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PlayFab.ClientModels.RegisterPlayFabUserRequest::get_Email()
extern "C"  String_t* RegisterPlayFabUserRequest_get_Email_m2625771373 (RegisterPlayFabUserRequest_t2314572612 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.RegisterPlayFabUserRequest::set_Email(System.String)
extern "C"  void RegisterPlayFabUserRequest_set_Email_m494984684 (RegisterPlayFabUserRequest_t2314572612 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PlayFab.ClientModels.RegisterPlayFabUserRequest::get_Password()
extern "C"  String_t* RegisterPlayFabUserRequest_get_Password_m142259468 (RegisterPlayFabUserRequest_t2314572612 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.RegisterPlayFabUserRequest::set_Password(System.String)
extern "C"  void RegisterPlayFabUserRequest_set_Password_m3815588063 (RegisterPlayFabUserRequest_t2314572612 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Nullable`1<System.Boolean> PlayFab.ClientModels.RegisterPlayFabUserRequest::get_RequireBothUsernameAndEmail()
extern "C"  Nullable_1_t3097043249  RegisterPlayFabUserRequest_get_RequireBothUsernameAndEmail_m1377696472 (RegisterPlayFabUserRequest_t2314572612 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.RegisterPlayFabUserRequest::set_RequireBothUsernameAndEmail(System.Nullable`1<System.Boolean>)
extern "C"  void RegisterPlayFabUserRequest_set_RequireBothUsernameAndEmail_m2608564909 (RegisterPlayFabUserRequest_t2314572612 * __this, Nullable_1_t3097043249  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PlayFab.ClientModels.RegisterPlayFabUserRequest::get_DisplayName()
extern "C"  String_t* RegisterPlayFabUserRequest_get_DisplayName_m887264766 (RegisterPlayFabUserRequest_t2314572612 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.RegisterPlayFabUserRequest::set_DisplayName(System.String)
extern "C"  void RegisterPlayFabUserRequest_set_DisplayName_m3077131707 (RegisterPlayFabUserRequest_t2314572612 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PlayFab.ClientModels.RegisterPlayFabUserRequest::get_Origination()
extern "C"  String_t* RegisterPlayFabUserRequest_get_Origination_m664081056 (RegisterPlayFabUserRequest_t2314572612 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.RegisterPlayFabUserRequest::set_Origination(System.String)
extern "C"  void RegisterPlayFabUserRequest_set_Origination_m2449074265 (RegisterPlayFabUserRequest_t2314572612 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
