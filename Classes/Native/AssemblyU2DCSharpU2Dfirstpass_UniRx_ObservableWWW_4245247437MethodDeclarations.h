﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.ObservableWWW/<PostWWW>c__AnonStorey8A
struct U3CPostWWWU3Ec__AnonStorey8A_t4245247437;
// System.Collections.IEnumerator
struct IEnumerator_t287207039;
// UniRx.IObserver`1<UnityEngine.WWW>
struct IObserver_1_t3734971003;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_CancellationTo1439151560.h"

// System.Void UniRx.ObservableWWW/<PostWWW>c__AnonStorey8A::.ctor()
extern "C"  void U3CPostWWWU3Ec__AnonStorey8A__ctor_m113643343 (U3CPostWWWU3Ec__AnonStorey8A_t4245247437 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator UniRx.ObservableWWW/<PostWWW>c__AnonStorey8A::<>m__B6(UniRx.IObserver`1<UnityEngine.WWW>,UniRx.CancellationToken)
extern "C"  Il2CppObject * U3CPostWWWU3Ec__AnonStorey8A_U3CU3Em__B6_m957328904 (U3CPostWWWU3Ec__AnonStorey8A_t4245247437 * __this, Il2CppObject* ___observer0, CancellationToken_t1439151560  ___cancellation1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
