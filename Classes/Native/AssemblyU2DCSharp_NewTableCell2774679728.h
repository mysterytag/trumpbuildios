﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.RectTransform
struct RectTransform_t3317474837;

#include "AssemblyU2DCSharp_InstantiatableInPool1882737686.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// NewTableCell
struct  NewTableCell_t2774679728  : public InstantiatableInPool_t1882737686
{
public:
	// UnityEngine.RectTransform NewTableCell::_rectTransform
	RectTransform_t3317474837 * ____rectTransform_5;
	// System.Boolean NewTableCell::deleted
	bool ___deleted_6;
	// System.Int32 NewTableCell::index
	int32_t ___index_7;

public:
	inline static int32_t get_offset_of__rectTransform_5() { return static_cast<int32_t>(offsetof(NewTableCell_t2774679728, ____rectTransform_5)); }
	inline RectTransform_t3317474837 * get__rectTransform_5() const { return ____rectTransform_5; }
	inline RectTransform_t3317474837 ** get_address_of__rectTransform_5() { return &____rectTransform_5; }
	inline void set__rectTransform_5(RectTransform_t3317474837 * value)
	{
		____rectTransform_5 = value;
		Il2CppCodeGenWriteBarrier(&____rectTransform_5, value);
	}

	inline static int32_t get_offset_of_deleted_6() { return static_cast<int32_t>(offsetof(NewTableCell_t2774679728, ___deleted_6)); }
	inline bool get_deleted_6() const { return ___deleted_6; }
	inline bool* get_address_of_deleted_6() { return &___deleted_6; }
	inline void set_deleted_6(bool value)
	{
		___deleted_6 = value;
	}

	inline static int32_t get_offset_of_index_7() { return static_cast<int32_t>(offsetof(NewTableCell_t2774679728, ___index_7)); }
	inline int32_t get_index_7() const { return ___index_7; }
	inline int32_t* get_address_of_index_7() { return &___index_7; }
	inline void set_index_7(int32_t value)
	{
		___index_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
