﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UniRx.Operators.WithLatestFromObservable`3/WithLatestFrom<System.Object,System.Object,System.Object>
struct WithLatestFrom_t2869896360;
// System.IDisposable
struct IDisposable_t1628921374;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Operators.WithLatestFromObservable`3/WithLatestFrom/RightObserver<System.Object,System.Object,System.Object>
struct  RightObserver_t812748181  : public Il2CppObject
{
public:
	// UniRx.Operators.WithLatestFromObservable`3/WithLatestFrom<TLeft,TRight,TResult> UniRx.Operators.WithLatestFromObservable`3/WithLatestFrom/RightObserver::parent
	WithLatestFrom_t2869896360 * ___parent_0;
	// System.IDisposable UniRx.Operators.WithLatestFromObservable`3/WithLatestFrom/RightObserver::selfSubscription
	Il2CppObject * ___selfSubscription_1;

public:
	inline static int32_t get_offset_of_parent_0() { return static_cast<int32_t>(offsetof(RightObserver_t812748181, ___parent_0)); }
	inline WithLatestFrom_t2869896360 * get_parent_0() const { return ___parent_0; }
	inline WithLatestFrom_t2869896360 ** get_address_of_parent_0() { return &___parent_0; }
	inline void set_parent_0(WithLatestFrom_t2869896360 * value)
	{
		___parent_0 = value;
		Il2CppCodeGenWriteBarrier(&___parent_0, value);
	}

	inline static int32_t get_offset_of_selfSubscription_1() { return static_cast<int32_t>(offsetof(RightObserver_t812748181, ___selfSubscription_1)); }
	inline Il2CppObject * get_selfSubscription_1() const { return ___selfSubscription_1; }
	inline Il2CppObject ** get_address_of_selfSubscription_1() { return &___selfSubscription_1; }
	inline void set_selfSubscription_1(Il2CppObject * value)
	{
		___selfSubscription_1 = value;
		Il2CppCodeGenWriteBarrier(&___selfSubscription_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
