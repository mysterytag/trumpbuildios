﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.CombineLatestFunc`7<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>
struct CombineLatestFunc_7_t929534559;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t537683269;
// System.AsyncCallback
struct AsyncCallback_t1363551830;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_IntPtr676692020.h"
#include "mscorlib_System_AsyncCallback1363551830.h"

// System.Void UniRx.Operators.CombineLatestFunc`7<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>::.ctor(System.Object,System.IntPtr)
extern "C"  void CombineLatestFunc_7__ctor_m3847113881_gshared (CombineLatestFunc_7_t929534559 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method);
#define CombineLatestFunc_7__ctor_m3847113881(__this, ___object0, ___method1, method) ((  void (*) (CombineLatestFunc_7_t929534559 *, Il2CppObject *, IntPtr_t, const MethodInfo*))CombineLatestFunc_7__ctor_m3847113881_gshared)(__this, ___object0, ___method1, method)
// TR UniRx.Operators.CombineLatestFunc`7<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>::Invoke(T1,T2,T3,T4,T5,T6)
extern "C"  Il2CppObject * CombineLatestFunc_7_Invoke_m2111121065_gshared (CombineLatestFunc_7_t929534559 * __this, Il2CppObject * ___arg10, Il2CppObject * ___arg21, Il2CppObject * ___arg32, Il2CppObject * ___arg43, Il2CppObject * ___arg54, Il2CppObject * ___arg65, const MethodInfo* method);
#define CombineLatestFunc_7_Invoke_m2111121065(__this, ___arg10, ___arg21, ___arg32, ___arg43, ___arg54, ___arg65, method) ((  Il2CppObject * (*) (CombineLatestFunc_7_t929534559 *, Il2CppObject *, Il2CppObject *, Il2CppObject *, Il2CppObject *, Il2CppObject *, Il2CppObject *, const MethodInfo*))CombineLatestFunc_7_Invoke_m2111121065_gshared)(__this, ___arg10, ___arg21, ___arg32, ___arg43, ___arg54, ___arg65, method)
// System.IAsyncResult UniRx.Operators.CombineLatestFunc`7<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>::BeginInvoke(T1,T2,T3,T4,T5,T6,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * CombineLatestFunc_7_BeginInvoke_m1651379027_gshared (CombineLatestFunc_7_t929534559 * __this, Il2CppObject * ___arg10, Il2CppObject * ___arg21, Il2CppObject * ___arg32, Il2CppObject * ___arg43, Il2CppObject * ___arg54, Il2CppObject * ___arg65, AsyncCallback_t1363551830 * ___callback6, Il2CppObject * ___object7, const MethodInfo* method);
#define CombineLatestFunc_7_BeginInvoke_m1651379027(__this, ___arg10, ___arg21, ___arg32, ___arg43, ___arg54, ___arg65, ___callback6, ___object7, method) ((  Il2CppObject * (*) (CombineLatestFunc_7_t929534559 *, Il2CppObject *, Il2CppObject *, Il2CppObject *, Il2CppObject *, Il2CppObject *, Il2CppObject *, AsyncCallback_t1363551830 *, Il2CppObject *, const MethodInfo*))CombineLatestFunc_7_BeginInvoke_m1651379027_gshared)(__this, ___arg10, ___arg21, ___arg32, ___arg43, ___arg54, ___arg65, ___callback6, ___object7, method)
// TR UniRx.Operators.CombineLatestFunc`7<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>::EndInvoke(System.IAsyncResult)
extern "C"  Il2CppObject * CombineLatestFunc_7_EndInvoke_m2565031056_gshared (CombineLatestFunc_7_t929534559 * __this, Il2CppObject * ___result0, const MethodInfo* method);
#define CombineLatestFunc_7_EndInvoke_m2565031056(__this, ___result0, method) ((  Il2CppObject * (*) (CombineLatestFunc_7_t929534559 *, Il2CppObject *, const MethodInfo*))CombineLatestFunc_7_EndInvoke_m2565031056_gshared)(__this, ___result0, method)
