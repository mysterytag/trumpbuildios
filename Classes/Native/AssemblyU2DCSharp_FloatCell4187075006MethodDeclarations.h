﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// FloatCell
struct FloatCell_t4187075006;
// IStream`1<System.Single>
struct IStream_1_t1510900012;

#include "codegen/il2cpp-codegen.h"

// System.Void FloatCell::.ctor(System.Single)
extern "C"  void FloatCell__ctor_m2784756062 (FloatCell_t4187075006 * __this, float ___val0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FloatCell::.ctor()
extern "C"  void FloatCell__ctor_m3429005677 (FloatCell_t4187075006 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// IStream`1<System.Single> FloatCell::get_diff()
extern "C"  Il2CppObject* FloatCell_get_diff_m1909129473 (FloatCell_t4187075006 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single FloatCell::get_value()
extern "C"  float FloatCell_get_value_m613291711 (FloatCell_t4187075006 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FloatCell::set_value(System.Single)
extern "C"  void FloatCell_set_value_m1394637836 (FloatCell_t4187075006 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
