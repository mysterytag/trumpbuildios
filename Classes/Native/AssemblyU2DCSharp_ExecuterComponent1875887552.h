﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Executer
struct Executer_t2107661245;

#include "UnityEngine_UnityEngine_MonoBehaviour3012272455.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ExecuterComponent
struct  ExecuterComponent_t1875887552  : public MonoBehaviour_t3012272455
{
public:
	// Executer ExecuterComponent::exec
	Executer_t2107661245 * ___exec_2;

public:
	inline static int32_t get_offset_of_exec_2() { return static_cast<int32_t>(offsetof(ExecuterComponent_t1875887552, ___exec_2)); }
	inline Executer_t2107661245 * get_exec_2() const { return ___exec_2; }
	inline Executer_t2107661245 ** get_address_of_exec_2() { return &___exec_2; }
	inline void set_exec_2(Executer_t2107661245 * value)
	{
		___exec_2 = value;
		Il2CppCodeGenWriteBarrier(&___exec_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
