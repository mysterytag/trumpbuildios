﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SponsorPay.SPUtils
struct SPUtils_t3174287374;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"

// System.Void SponsorPay.SPUtils::.ctor()
extern "C"  void SPUtils__ctor_m3495005543 (SPUtils_t3174287374 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SponsorPay.SPUtils::printWarningMessage()
extern "C"  void SPUtils_printWarningMessage_m4158390909 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String SponsorPay.SPUtils::GetMethodName()
extern "C"  String_t* SPUtils_GetMethodName_m1752190980 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
