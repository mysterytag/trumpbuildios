﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Subject`1/Subscription<UnityEngine.MasterServerEvent>
struct Subscription_t57054012;
// UniRx.Subject`1<UnityEngine.MasterServerEvent>
struct Subject_1_t4215842110;
// UniRx.IObserver`1<UnityEngine.MasterServerEvent>
struct IObserver_1_t194839097;

#include "codegen/il2cpp-codegen.h"

// System.Void UniRx.Subject`1/Subscription<UnityEngine.MasterServerEvent>::.ctor(UniRx.Subject`1<T>,UniRx.IObserver`1<T>)
extern "C"  void Subscription__ctor_m2652697463_gshared (Subscription_t57054012 * __this, Subject_1_t4215842110 * ___parent0, Il2CppObject* ___unsubscribeTarget1, const MethodInfo* method);
#define Subscription__ctor_m2652697463(__this, ___parent0, ___unsubscribeTarget1, method) ((  void (*) (Subscription_t57054012 *, Subject_1_t4215842110 *, Il2CppObject*, const MethodInfo*))Subscription__ctor_m2652697463_gshared)(__this, ___parent0, ___unsubscribeTarget1, method)
// System.Void UniRx.Subject`1/Subscription<UnityEngine.MasterServerEvent>::Dispose()
extern "C"  void Subscription_Dispose_m1756694559_gshared (Subscription_t57054012 * __this, const MethodInfo* method);
#define Subscription_Dispose_m1756694559(__this, method) ((  void (*) (Subscription_t57054012 *, const MethodInfo*))Subscription_Dispose_m1756694559_gshared)(__this, method)
