﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Tuple`8<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>
struct Tuple_8_t1597838469;
// System.Object
struct Il2CppObject;
// System.Collections.IComparer
struct IComparer_t2207526184;
// System.Collections.IEqualityComparer
struct IEqualityComparer_t1661793090;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"

// System.Void UniRx.Tuple`8<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>::.ctor(T1,T2,T3,T4,T5,T6,T7,TRest)
extern "C"  void Tuple_8__ctor_m4056784285_gshared (Tuple_8_t1597838469 * __this, Il2CppObject * ___item10, Il2CppObject * ___item21, Il2CppObject * ___item32, Il2CppObject * ___item43, Il2CppObject * ___item54, Il2CppObject * ___item65, Il2CppObject * ___item76, Il2CppObject * ___rest7, const MethodInfo* method);
#define Tuple_8__ctor_m4056784285(__this, ___item10, ___item21, ___item32, ___item43, ___item54, ___item65, ___item76, ___rest7, method) ((  void (*) (Tuple_8_t1597838469 *, Il2CppObject *, Il2CppObject *, Il2CppObject *, Il2CppObject *, Il2CppObject *, Il2CppObject *, Il2CppObject *, Il2CppObject *, const MethodInfo*))Tuple_8__ctor_m4056784285_gshared)(__this, ___item10, ___item21, ___item32, ___item43, ___item54, ___item65, ___item76, ___rest7, method)
// System.Int32 UniRx.Tuple`8<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>::System.IComparable.CompareTo(System.Object)
extern "C"  int32_t Tuple_8_System_IComparable_CompareTo_m4035662717_gshared (Tuple_8_t1597838469 * __this, Il2CppObject * ___obj0, const MethodInfo* method);
#define Tuple_8_System_IComparable_CompareTo_m4035662717(__this, ___obj0, method) ((  int32_t (*) (Tuple_8_t1597838469 *, Il2CppObject *, const MethodInfo*))Tuple_8_System_IComparable_CompareTo_m4035662717_gshared)(__this, ___obj0, method)
// System.Int32 UniRx.Tuple`8<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>::UniRx.IStructuralComparable.CompareTo(System.Object,System.Collections.IComparer)
extern "C"  int32_t Tuple_8_UniRx_IStructuralComparable_CompareTo_m1093715599_gshared (Tuple_8_t1597838469 * __this, Il2CppObject * ___other0, Il2CppObject * ___comparer1, const MethodInfo* method);
#define Tuple_8_UniRx_IStructuralComparable_CompareTo_m1093715599(__this, ___other0, ___comparer1, method) ((  int32_t (*) (Tuple_8_t1597838469 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))Tuple_8_UniRx_IStructuralComparable_CompareTo_m1093715599_gshared)(__this, ___other0, ___comparer1, method)
// System.Boolean UniRx.Tuple`8<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>::UniRx.IStructuralEquatable.Equals(System.Object,System.Collections.IEqualityComparer)
extern "C"  bool Tuple_8_UniRx_IStructuralEquatable_Equals_m702454650_gshared (Tuple_8_t1597838469 * __this, Il2CppObject * ___other0, Il2CppObject * ___comparer1, const MethodInfo* method);
#define Tuple_8_UniRx_IStructuralEquatable_Equals_m702454650(__this, ___other0, ___comparer1, method) ((  bool (*) (Tuple_8_t1597838469 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))Tuple_8_UniRx_IStructuralEquatable_Equals_m702454650_gshared)(__this, ___other0, ___comparer1, method)
// System.Int32 UniRx.Tuple`8<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>::UniRx.IStructuralEquatable.GetHashCode(System.Collections.IEqualityComparer)
extern "C"  int32_t Tuple_8_UniRx_IStructuralEquatable_GetHashCode_m492782452_gshared (Tuple_8_t1597838469 * __this, Il2CppObject * ___comparer0, const MethodInfo* method);
#define Tuple_8_UniRx_IStructuralEquatable_GetHashCode_m492782452(__this, ___comparer0, method) ((  int32_t (*) (Tuple_8_t1597838469 *, Il2CppObject *, const MethodInfo*))Tuple_8_UniRx_IStructuralEquatable_GetHashCode_m492782452_gshared)(__this, ___comparer0, method)
// System.String UniRx.Tuple`8<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>::UniRx.ITuple.ToString()
extern "C"  String_t* Tuple_8_UniRx_ITuple_ToString_m1892230647_gshared (Tuple_8_t1597838469 * __this, const MethodInfo* method);
#define Tuple_8_UniRx_ITuple_ToString_m1892230647(__this, method) ((  String_t* (*) (Tuple_8_t1597838469 *, const MethodInfo*))Tuple_8_UniRx_ITuple_ToString_m1892230647_gshared)(__this, method)
// T1 UniRx.Tuple`8<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>::get_Item1()
extern "C"  Il2CppObject * Tuple_8_get_Item1_m2802491044_gshared (Tuple_8_t1597838469 * __this, const MethodInfo* method);
#define Tuple_8_get_Item1_m2802491044(__this, method) ((  Il2CppObject * (*) (Tuple_8_t1597838469 *, const MethodInfo*))Tuple_8_get_Item1_m2802491044_gshared)(__this, method)
// T2 UniRx.Tuple`8<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>::get_Item2()
extern "C"  Il2CppObject * Tuple_8_get_Item2_m2724501734_gshared (Tuple_8_t1597838469 * __this, const MethodInfo* method);
#define Tuple_8_get_Item2_m2724501734(__this, method) ((  Il2CppObject * (*) (Tuple_8_t1597838469 *, const MethodInfo*))Tuple_8_get_Item2_m2724501734_gshared)(__this, method)
// T3 UniRx.Tuple`8<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>::get_Item3()
extern "C"  Il2CppObject * Tuple_8_get_Item3_m2646512424_gshared (Tuple_8_t1597838469 * __this, const MethodInfo* method);
#define Tuple_8_get_Item3_m2646512424(__this, method) ((  Il2CppObject * (*) (Tuple_8_t1597838469 *, const MethodInfo*))Tuple_8_get_Item3_m2646512424_gshared)(__this, method)
// T4 UniRx.Tuple`8<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>::get_Item4()
extern "C"  Il2CppObject * Tuple_8_get_Item4_m2568523114_gshared (Tuple_8_t1597838469 * __this, const MethodInfo* method);
#define Tuple_8_get_Item4_m2568523114(__this, method) ((  Il2CppObject * (*) (Tuple_8_t1597838469 *, const MethodInfo*))Tuple_8_get_Item4_m2568523114_gshared)(__this, method)
// T5 UniRx.Tuple`8<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>::get_Item5()
extern "C"  Il2CppObject * Tuple_8_get_Item5_m2490533804_gshared (Tuple_8_t1597838469 * __this, const MethodInfo* method);
#define Tuple_8_get_Item5_m2490533804(__this, method) ((  Il2CppObject * (*) (Tuple_8_t1597838469 *, const MethodInfo*))Tuple_8_get_Item5_m2490533804_gshared)(__this, method)
// T6 UniRx.Tuple`8<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>::get_Item6()
extern "C"  Il2CppObject * Tuple_8_get_Item6_m2412544494_gshared (Tuple_8_t1597838469 * __this, const MethodInfo* method);
#define Tuple_8_get_Item6_m2412544494(__this, method) ((  Il2CppObject * (*) (Tuple_8_t1597838469 *, const MethodInfo*))Tuple_8_get_Item6_m2412544494_gshared)(__this, method)
// T7 UniRx.Tuple`8<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>::get_Item7()
extern "C"  Il2CppObject * Tuple_8_get_Item7_m2334555184_gshared (Tuple_8_t1597838469 * __this, const MethodInfo* method);
#define Tuple_8_get_Item7_m2334555184(__this, method) ((  Il2CppObject * (*) (Tuple_8_t1597838469 *, const MethodInfo*))Tuple_8_get_Item7_m2334555184_gshared)(__this, method)
// TRest UniRx.Tuple`8<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>::get_Rest()
extern "C"  Il2CppObject * Tuple_8_get_Rest_m1584864947_gshared (Tuple_8_t1597838469 * __this, const MethodInfo* method);
#define Tuple_8_get_Rest_m1584864947(__this, method) ((  Il2CppObject * (*) (Tuple_8_t1597838469 *, const MethodInfo*))Tuple_8_get_Rest_m1584864947_gshared)(__this, method)
// System.Boolean UniRx.Tuple`8<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>::Equals(System.Object)
extern "C"  bool Tuple_8_Equals_m2478208558_gshared (Tuple_8_t1597838469 * __this, Il2CppObject * ___obj0, const MethodInfo* method);
#define Tuple_8_Equals_m2478208558(__this, ___obj0, method) ((  bool (*) (Tuple_8_t1597838469 *, Il2CppObject *, const MethodInfo*))Tuple_8_Equals_m2478208558_gshared)(__this, ___obj0, method)
// System.Int32 UniRx.Tuple`8<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>::GetHashCode()
extern "C"  int32_t Tuple_8_GetHashCode_m4286259154_gshared (Tuple_8_t1597838469 * __this, const MethodInfo* method);
#define Tuple_8_GetHashCode_m4286259154(__this, method) ((  int32_t (*) (Tuple_8_t1597838469 *, const MethodInfo*))Tuple_8_GetHashCode_m4286259154_gshared)(__this, method)
// System.String UniRx.Tuple`8<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>::ToString()
extern "C"  String_t* Tuple_8_ToString_m3916582874_gshared (Tuple_8_t1597838469 * __this, const MethodInfo* method);
#define Tuple_8_ToString_m3916582874(__this, method) ((  String_t* (*) (Tuple_8_t1597838469 *, const MethodInfo*))Tuple_8_ToString_m3916582874_gshared)(__this, method)
// System.Boolean UniRx.Tuple`8<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>::Equals(UniRx.Tuple`8<T1,T2,T3,T4,T5,T6,T7,TRest>)
extern "C"  bool Tuple_8_Equals_m2944557758_gshared (Tuple_8_t1597838469 * __this, Tuple_8_t1597838469 * ___other0, const MethodInfo* method);
#define Tuple_8_Equals_m2944557758(__this, ___other0, method) ((  bool (*) (Tuple_8_t1597838469 *, Tuple_8_t1597838469 *, const MethodInfo*))Tuple_8_Equals_m2944557758_gshared)(__this, ___other0, method)
