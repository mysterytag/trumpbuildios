﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Net.WebRequest
struct WebRequest_t3488810021;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t2995724695;
// System.String
struct String_t;
// System.Exception
struct Exception_t1967233988;
// System.Net.Cache.RequestCachePolicy
struct RequestCachePolicy_t3921445643;
// System.Net.ICredentials
struct ICredentials_t2307785309;
// System.Net.WebHeaderCollection
struct WebHeaderCollection_t1099177929;
// System.Net.IWebProxy
struct IWebProxy_t49946189;
// System.Uri
struct Uri_t2776692961;
// System.IAsyncResult
struct IAsyncResult_t537683269;
// System.AsyncCallback
struct AsyncCallback_t1363551830;
// System.Object
struct Il2CppObject;
// System.IO.Stream
struct Stream_t219029575;
// System.Net.WebResponse
struct WebResponse_t2411292415;
// System.Net.IWebRequestCreate
struct IWebRequestCreate_t3734251338;
// System.Type
struct Type_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Runtime_Serialization_Serializatio2995724695.h"
#include "mscorlib_System_Runtime_Serialization_StreamingCont986364934.h"
#include "mscorlib_System_String968488902.h"
#include "System_System_Net_Security_AuthenticationLevel2597042204.h"
#include "System_System_Net_Cache_RequestCachePolicy3921445643.h"
#include "System_System_Net_WebHeaderCollection1099177929.h"
#include "mscorlib_System_Security_Principal_TokenImpersonat3749688742.h"
#include "mscorlib_System_AsyncCallback1363551830.h"
#include "mscorlib_System_Object837106420.h"
#include "System_System_Uri2776692961.h"
#include "mscorlib_System_Type2779229935.h"

// System.Void System.Net.WebRequest::.ctor()
extern "C"  void WebRequest__ctor_m3908406559 (WebRequest_t3488810021 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.WebRequest::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C"  void WebRequest__ctor_m2268070368 (WebRequest_t3488810021 * __this, SerializationInfo_t2995724695 * ___serializationInfo0, StreamingContext_t986364934  ___streamingContext1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.WebRequest::.cctor()
extern "C"  void WebRequest__cctor_m419422830 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.WebRequest::System.Runtime.Serialization.ISerializable.GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C"  void WebRequest_System_Runtime_Serialization_ISerializable_GetObjectData_m3499262348 (WebRequest_t3488810021 * __this, SerializationInfo_t2995724695 * ___serializationInfo0, StreamingContext_t986364934  ___streamingContext1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.WebRequest::AddDynamicPrefix(System.String,System.String)
extern "C"  void WebRequest_AddDynamicPrefix_m4259958729 (Il2CppObject * __this /* static, unused */, String_t* ___protocol0, String_t* ___implementor1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Exception System.Net.WebRequest::GetMustImplement()
extern "C"  Exception_t1967233988 * WebRequest_GetMustImplement_m2345556858 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Net.Security.AuthenticationLevel System.Net.WebRequest::get_AuthenticationLevel()
extern "C"  int32_t WebRequest_get_AuthenticationLevel_m3322656133 (WebRequest_t3488810021 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.WebRequest::set_AuthenticationLevel(System.Net.Security.AuthenticationLevel)
extern "C"  void WebRequest_set_AuthenticationLevel_m622546392 (WebRequest_t3488810021 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Net.Cache.RequestCachePolicy System.Net.WebRequest::get_CachePolicy()
extern "C"  RequestCachePolicy_t3921445643 * WebRequest_get_CachePolicy_m3487961626 (WebRequest_t3488810021 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.WebRequest::set_CachePolicy(System.Net.Cache.RequestCachePolicy)
extern "C"  void WebRequest_set_CachePolicy_m3572597277 (WebRequest_t3488810021 * __this, RequestCachePolicy_t3921445643 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Net.WebRequest::get_ConnectionGroupName()
extern "C"  String_t* WebRequest_get_ConnectionGroupName_m1892303075 (WebRequest_t3488810021 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.WebRequest::set_ConnectionGroupName(System.String)
extern "C"  void WebRequest_set_ConnectionGroupName_m4209971702 (WebRequest_t3488810021 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 System.Net.WebRequest::get_ContentLength()
extern "C"  int64_t WebRequest_get_ContentLength_m282902724 (WebRequest_t3488810021 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.WebRequest::set_ContentLength(System.Int64)
extern "C"  void WebRequest_set_ContentLength_m2204879281 (WebRequest_t3488810021 * __this, int64_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Net.WebRequest::get_ContentType()
extern "C"  String_t* WebRequest_get_ContentType_m1446396938 (WebRequest_t3488810021 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.WebRequest::set_ContentType(System.String)
extern "C"  void WebRequest_set_ContentType_m696487215 (WebRequest_t3488810021 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Net.ICredentials System.Net.WebRequest::get_Credentials()
extern "C"  Il2CppObject * WebRequest_get_Credentials_m632231296 (WebRequest_t3488810021 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.WebRequest::set_Credentials(System.Net.ICredentials)
extern "C"  void WebRequest_set_Credentials_m1948289971 (WebRequest_t3488810021 * __this, Il2CppObject * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Net.Cache.RequestCachePolicy System.Net.WebRequest::get_DefaultCachePolicy()
extern "C"  RequestCachePolicy_t3921445643 * WebRequest_get_DefaultCachePolicy_m3178672175 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.WebRequest::set_DefaultCachePolicy(System.Net.Cache.RequestCachePolicy)
extern "C"  void WebRequest_set_DefaultCachePolicy_m4070843996 (Il2CppObject * __this /* static, unused */, RequestCachePolicy_t3921445643 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Net.WebHeaderCollection System.Net.WebRequest::get_Headers()
extern "C"  WebHeaderCollection_t1099177929 * WebRequest_get_Headers_m2569908874 (WebRequest_t3488810021 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.WebRequest::set_Headers(System.Net.WebHeaderCollection)
extern "C"  void WebRequest_set_Headers_m2948783351 (WebRequest_t3488810021 * __this, WebHeaderCollection_t1099177929 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.Principal.TokenImpersonationLevel System.Net.WebRequest::get_ImpersonationLevel()
extern "C"  int32_t WebRequest_get_ImpersonationLevel_m1488798581 (WebRequest_t3488810021 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.WebRequest::set_ImpersonationLevel(System.Security.Principal.TokenImpersonationLevel)
extern "C"  void WebRequest_set_ImpersonationLevel_m2060242518 (WebRequest_t3488810021 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Net.WebRequest::get_Method()
extern "C"  String_t* WebRequest_get_Method_m1855994732 (WebRequest_t3488810021 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.WebRequest::set_Method(System.String)
extern "C"  void WebRequest_set_Method_m89055935 (WebRequest_t3488810021 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Net.WebRequest::get_PreAuthenticate()
extern "C"  bool WebRequest_get_PreAuthenticate_m266330946 (WebRequest_t3488810021 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.WebRequest::set_PreAuthenticate(System.Boolean)
extern "C"  void WebRequest_set_PreAuthenticate_m3161512239 (WebRequest_t3488810021 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Net.IWebProxy System.Net.WebRequest::get_Proxy()
extern "C"  Il2CppObject * WebRequest_get_Proxy_m1263414958 (WebRequest_t3488810021 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.WebRequest::set_Proxy(System.Net.IWebProxy)
extern "C"  void WebRequest_set_Proxy_m534868955 (WebRequest_t3488810021 * __this, Il2CppObject * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Uri System.Net.WebRequest::get_RequestUri()
extern "C"  Uri_t2776692961 * WebRequest_get_RequestUri_m3887362073 (WebRequest_t3488810021 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Net.WebRequest::get_Timeout()
extern "C"  int32_t WebRequest_get_Timeout_m4029371621 (WebRequest_t3488810021 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.WebRequest::set_Timeout(System.Int32)
extern "C"  void WebRequest_set_Timeout_m1204043410 (WebRequest_t3488810021 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Net.WebRequest::get_UseDefaultCredentials()
extern "C"  bool WebRequest_get_UseDefaultCredentials_m934757580 (WebRequest_t3488810021 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.WebRequest::set_UseDefaultCredentials(System.Boolean)
extern "C"  void WebRequest_set_UseDefaultCredentials_m2408476857 (WebRequest_t3488810021 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Net.IWebProxy System.Net.WebRequest::get_DefaultWebProxy()
extern "C"  Il2CppObject * WebRequest_get_DefaultWebProxy_m645383963 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.WebRequest::set_DefaultWebProxy(System.Net.IWebProxy)
extern "C"  void WebRequest_set_DefaultWebProxy_m242944968 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Net.IWebProxy System.Net.WebRequest::GetDefaultWebProxy()
extern "C"  Il2CppObject * WebRequest_GetDefaultWebProxy_m3231963326 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.WebRequest::Abort()
extern "C"  void WebRequest_Abort_m3557951853 (WebRequest_t3488810021 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult System.Net.WebRequest::BeginGetRequestStream(System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * WebRequest_BeginGetRequestStream_m886113327 (WebRequest_t3488810021 * __this, AsyncCallback_t1363551830 * ___callback0, Il2CppObject * ___state1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult System.Net.WebRequest::BeginGetResponse(System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * WebRequest_BeginGetResponse_m1262614523 (WebRequest_t3488810021 * __this, AsyncCallback_t1363551830 * ___callback0, Il2CppObject * ___state1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Net.WebRequest System.Net.WebRequest::Create(System.String)
extern "C"  WebRequest_t3488810021 * WebRequest_Create_m2936239241 (Il2CppObject * __this /* static, unused */, String_t* ___requestUriString0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Net.WebRequest System.Net.WebRequest::Create(System.Uri)
extern "C"  WebRequest_t3488810021 * WebRequest_Create_m1795071884 (Il2CppObject * __this /* static, unused */, Uri_t2776692961 * ___requestUri0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Net.WebRequest System.Net.WebRequest::CreateDefault(System.Uri)
extern "C"  WebRequest_t3488810021 * WebRequest_CreateDefault_m889661693 (Il2CppObject * __this /* static, unused */, Uri_t2776692961 * ___requestUri0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IO.Stream System.Net.WebRequest::EndGetRequestStream(System.IAsyncResult)
extern "C"  Stream_t219029575 * WebRequest_EndGetRequestStream_m653456156 (WebRequest_t3488810021 * __this, Il2CppObject * ___asyncResult0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Net.WebResponse System.Net.WebRequest::EndGetResponse(System.IAsyncResult)
extern "C"  WebResponse_t2411292415 * WebRequest_EndGetResponse_m1707925532 (WebRequest_t3488810021 * __this, Il2CppObject * ___asyncResult0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IO.Stream System.Net.WebRequest::GetRequestStream()
extern "C"  Stream_t219029575 * WebRequest_GetRequestStream_m2000848392 (WebRequest_t3488810021 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Net.WebResponse System.Net.WebRequest::GetResponse()
extern "C"  WebResponse_t2411292415 * WebRequest_GetResponse_m461741742 (WebRequest_t3488810021 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Net.IWebProxy System.Net.WebRequest::GetSystemWebProxy()
extern "C"  Il2CppObject * WebRequest_GetSystemWebProxy_m4157785000 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.WebRequest::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C"  void WebRequest_GetObjectData_m3619776829 (WebRequest_t3488810021 * __this, SerializationInfo_t2995724695 * ___serializationInfo0, StreamingContext_t986364934  ___streamingContext1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Net.WebRequest::RegisterPrefix(System.String,System.Net.IWebRequestCreate)
extern "C"  bool WebRequest_RegisterPrefix_m463471800 (Il2CppObject * __this /* static, unused */, String_t* ___prefix0, Il2CppObject * ___creator1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Net.IWebRequestCreate System.Net.WebRequest::GetCreator(System.String)
extern "C"  Il2CppObject * WebRequest_GetCreator_m1370740790 (Il2CppObject * __this /* static, unused */, String_t* ___prefix0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.WebRequest::ClearPrefixes()
extern "C"  void WebRequest_ClearPrefixes_m4031807274 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.WebRequest::RemovePrefix(System.String)
extern "C"  void WebRequest_RemovePrefix_m3046155239 (Il2CppObject * __this /* static, unused */, String_t* ___prefix0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.WebRequest::AddPrefix(System.String,System.String)
extern "C"  void WebRequest_AddPrefix_m3692960686 (Il2CppObject * __this /* static, unused */, String_t* ___prefix0, String_t* ___typeName1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.WebRequest::AddPrefix(System.String,System.Type)
extern "C"  void WebRequest_AddPrefix_m2923345701 (Il2CppObject * __this /* static, unused */, String_t* ___prefix0, Type_t * ___type1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
