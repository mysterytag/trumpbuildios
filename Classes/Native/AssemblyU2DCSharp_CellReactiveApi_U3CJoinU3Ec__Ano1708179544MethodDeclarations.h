﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CellReactiveApi/<Join>c__AnonStorey114`1/<Join>c__AnonStorey115`1<System.Object>
struct U3CJoinU3Ec__AnonStorey115_1_t1708179544;
// ICell`1<System.Object>
struct ICell_1_t2388737397;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"

// System.Void CellReactiveApi/<Join>c__AnonStorey114`1/<Join>c__AnonStorey115`1<System.Object>::.ctor()
extern "C"  void U3CJoinU3Ec__AnonStorey115_1__ctor_m4216441311_gshared (U3CJoinU3Ec__AnonStorey115_1_t1708179544 * __this, const MethodInfo* method);
#define U3CJoinU3Ec__AnonStorey115_1__ctor_m4216441311(__this, method) ((  void (*) (U3CJoinU3Ec__AnonStorey115_1_t1708179544 *, const MethodInfo*))U3CJoinU3Ec__AnonStorey115_1__ctor_m4216441311_gshared)(__this, method)
// System.Void CellReactiveApi/<Join>c__AnonStorey114`1/<Join>c__AnonStorey115`1<System.Object>::<>m__19C(ICell`1<T>)
extern "C"  void U3CJoinU3Ec__AnonStorey115_1_U3CU3Em__19C_m4050255051_gshared (U3CJoinU3Ec__AnonStorey115_1_t1708179544 * __this, Il2CppObject* ___innerCell0, const MethodInfo* method);
#define U3CJoinU3Ec__AnonStorey115_1_U3CU3Em__19C_m4050255051(__this, ___innerCell0, method) ((  void (*) (U3CJoinU3Ec__AnonStorey115_1_t1708179544 *, Il2CppObject*, const MethodInfo*))U3CJoinU3Ec__AnonStorey115_1_U3CU3Em__19C_m4050255051_gshared)(__this, ___innerCell0, method)
// System.Void CellReactiveApi/<Join>c__AnonStorey114`1/<Join>c__AnonStorey115`1<System.Object>::<>m__19D(T)
extern "C"  void U3CJoinU3Ec__AnonStorey115_1_U3CU3Em__19D_m1959039496_gshared (U3CJoinU3Ec__AnonStorey115_1_t1708179544 * __this, Il2CppObject * ___val0, const MethodInfo* method);
#define U3CJoinU3Ec__AnonStorey115_1_U3CU3Em__19D_m1959039496(__this, ___val0, method) ((  void (*) (U3CJoinU3Ec__AnonStorey115_1_t1708179544 *, Il2CppObject *, const MethodInfo*))U3CJoinU3Ec__AnonStorey115_1_U3CU3Em__19D_m1959039496_gshared)(__this, ___val0, method)
