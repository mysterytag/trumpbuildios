﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.IDisposable
struct IDisposable_t1628921374;
// UniRx.Operators.RefCountObservable`1/RefCount<System.Object>
struct RefCount_t184354677;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Operators.RefCountObservable`1/RefCount/<Run>c__AnonStorey65<System.Object>
struct  U3CRunU3Ec__AnonStorey65_t1610859672  : public Il2CppObject
{
public:
	// System.IDisposable UniRx.Operators.RefCountObservable`1/RefCount/<Run>c__AnonStorey65::subcription
	Il2CppObject * ___subcription_0;
	// UniRx.Operators.RefCountObservable`1/RefCount<T> UniRx.Operators.RefCountObservable`1/RefCount/<Run>c__AnonStorey65::<>f__this
	RefCount_t184354677 * ___U3CU3Ef__this_1;

public:
	inline static int32_t get_offset_of_subcription_0() { return static_cast<int32_t>(offsetof(U3CRunU3Ec__AnonStorey65_t1610859672, ___subcription_0)); }
	inline Il2CppObject * get_subcription_0() const { return ___subcription_0; }
	inline Il2CppObject ** get_address_of_subcription_0() { return &___subcription_0; }
	inline void set_subcription_0(Il2CppObject * value)
	{
		___subcription_0 = value;
		Il2CppCodeGenWriteBarrier(&___subcription_0, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__this_1() { return static_cast<int32_t>(offsetof(U3CRunU3Ec__AnonStorey65_t1610859672, ___U3CU3Ef__this_1)); }
	inline RefCount_t184354677 * get_U3CU3Ef__this_1() const { return ___U3CU3Ef__this_1; }
	inline RefCount_t184354677 ** get_address_of_U3CU3Ef__this_1() { return &___U3CU3Ef__this_1; }
	inline void set_U3CU3Ef__this_1(RefCount_t184354677 * value)
	{
		___U3CU3Ef__this_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__this_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
