﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Func`2<System.Object,System.Object>
struct Func_2_t2135783352;
// System.String
struct String_t;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.Binder/<ToGetterBase>c__AnonStorey16F`2<System.Object,System.Object>
struct  U3CToGetterBaseU3Ec__AnonStorey16F_2_t3259896156  : public Il2CppObject
{
public:
	// System.Func`2<TObj,TResult> Zenject.Binder/<ToGetterBase>c__AnonStorey16F`2::method
	Func_2_t2135783352 * ___method_0;
	// System.String Zenject.Binder/<ToGetterBase>c__AnonStorey16F`2::identifier
	String_t* ___identifier_1;

public:
	inline static int32_t get_offset_of_method_0() { return static_cast<int32_t>(offsetof(U3CToGetterBaseU3Ec__AnonStorey16F_2_t3259896156, ___method_0)); }
	inline Func_2_t2135783352 * get_method_0() const { return ___method_0; }
	inline Func_2_t2135783352 ** get_address_of_method_0() { return &___method_0; }
	inline void set_method_0(Func_2_t2135783352 * value)
	{
		___method_0 = value;
		Il2CppCodeGenWriteBarrier(&___method_0, value);
	}

	inline static int32_t get_offset_of_identifier_1() { return static_cast<int32_t>(offsetof(U3CToGetterBaseU3Ec__AnonStorey16F_2_t3259896156, ___identifier_1)); }
	inline String_t* get_identifier_1() const { return ___identifier_1; }
	inline String_t** get_address_of_identifier_1() { return &___identifier_1; }
	inline void set_identifier_1(String_t* value)
	{
		___identifier_1 = value;
		Il2CppCodeGenWriteBarrier(&___identifier_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
