﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.PlayFabClientAPI/LoginWithFacebookRequestCallback
struct LoginWithFacebookRequestCallback_t3296410527;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;
// PlayFab.ClientModels.LoginWithFacebookRequest
struct LoginWithFacebookRequest_t58367306;
// System.IAsyncResult
struct IAsyncResult_t537683269;
// System.AsyncCallback
struct AsyncCallback_t1363551830;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_IntPtr676692020.h"
#include "mscorlib_System_String968488902.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_LoginWithFace58367306.h"
#include "mscorlib_System_AsyncCallback1363551830.h"

// System.Void PlayFab.PlayFabClientAPI/LoginWithFacebookRequestCallback::.ctor(System.Object,System.IntPtr)
extern "C"  void LoginWithFacebookRequestCallback__ctor_m3446642958 (LoginWithFacebookRequestCallback_t3296410527 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/LoginWithFacebookRequestCallback::Invoke(System.String,System.Int32,PlayFab.ClientModels.LoginWithFacebookRequest,System.Object)
extern "C"  void LoginWithFacebookRequestCallback_Invoke_m1913802239 (LoginWithFacebookRequestCallback_t3296410527 * __this, String_t* ___urlPath0, int32_t ___callId1, LoginWithFacebookRequest_t58367306 * ___request2, Il2CppObject * ___customData3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
extern "C" void pinvoke_delegate_wrapper_LoginWithFacebookRequestCallback_t3296410527(Il2CppObject* delegate, String_t* ___urlPath0, int32_t ___callId1, LoginWithFacebookRequest_t58367306 * ___request2, Il2CppObject * ___customData3);
// System.IAsyncResult PlayFab.PlayFabClientAPI/LoginWithFacebookRequestCallback::BeginInvoke(System.String,System.Int32,PlayFab.ClientModels.LoginWithFacebookRequest,System.Object,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * LoginWithFacebookRequestCallback_BeginInvoke_m2373284196 (LoginWithFacebookRequestCallback_t3296410527 * __this, String_t* ___urlPath0, int32_t ___callId1, LoginWithFacebookRequest_t58367306 * ___request2, Il2CppObject * ___customData3, AsyncCallback_t1363551830 * ___callback4, Il2CppObject * ___object5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/LoginWithFacebookRequestCallback::EndInvoke(System.IAsyncResult)
extern "C"  void LoginWithFacebookRequestCallback_EndInvoke_m3332751902 (LoginWithFacebookRequestCallback_t3296410527 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
