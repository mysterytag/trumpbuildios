﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// DG.Tweening.Core.DOGetter`1<UnityEngine.Quaternion>
struct DOGetter_1_t585876960;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t537683269;
// System.AsyncCallback
struct AsyncCallback_t1363551830;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_IntPtr676692020.h"
#include "UnityEngine_UnityEngine_Quaternion1891715979.h"
#include "mscorlib_System_AsyncCallback1363551830.h"

// System.Void DG.Tweening.Core.DOGetter`1<UnityEngine.Quaternion>::.ctor(System.Object,System.IntPtr)
extern "C"  void DOGetter_1__ctor_m3151867296_gshared (DOGetter_1_t585876960 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method);
#define DOGetter_1__ctor_m3151867296(__this, ___object0, ___method1, method) ((  void (*) (DOGetter_1_t585876960 *, Il2CppObject *, IntPtr_t, const MethodInfo*))DOGetter_1__ctor_m3151867296_gshared)(__this, ___object0, ___method1, method)
// T DG.Tweening.Core.DOGetter`1<UnityEngine.Quaternion>::Invoke()
extern "C"  Quaternion_t1891715979  DOGetter_1_Invoke_m1361134939_gshared (DOGetter_1_t585876960 * __this, const MethodInfo* method);
#define DOGetter_1_Invoke_m1361134939(__this, method) ((  Quaternion_t1891715979  (*) (DOGetter_1_t585876960 *, const MethodInfo*))DOGetter_1_Invoke_m1361134939_gshared)(__this, method)
// System.IAsyncResult DG.Tweening.Core.DOGetter`1<UnityEngine.Quaternion>::BeginInvoke(System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * DOGetter_1_BeginInvoke_m4132676689_gshared (DOGetter_1_t585876960 * __this, AsyncCallback_t1363551830 * ___callback0, Il2CppObject * ___object1, const MethodInfo* method);
#define DOGetter_1_BeginInvoke_m4132676689(__this, ___callback0, ___object1, method) ((  Il2CppObject * (*) (DOGetter_1_t585876960 *, AsyncCallback_t1363551830 *, Il2CppObject *, const MethodInfo*))DOGetter_1_BeginInvoke_m4132676689_gshared)(__this, ___callback0, ___object1, method)
// T DG.Tweening.Core.DOGetter`1<UnityEngine.Quaternion>::EndInvoke(System.IAsyncResult)
extern "C"  Quaternion_t1891715979  DOGetter_1_EndInvoke_m1357177937_gshared (DOGetter_1_t585876960 * __this, Il2CppObject * ___result0, const MethodInfo* method);
#define DOGetter_1_EndInvoke_m1357177937(__this, ___result0, method) ((  Quaternion_t1891715979  (*) (DOGetter_1_t585876960 *, Il2CppObject *, const MethodInfo*))DOGetter_1_EndInvoke_m1357177937_gshared)(__this, ___result0, method)
