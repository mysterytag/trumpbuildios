﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.ReactiveProperty`1<UnityEngine.Vector3>
struct ReactiveProperty_1_t4133662827;
// UniRx.IObservable`1<UnityEngine.Vector3>
struct IObservable_1_t3284128153;
// System.Collections.Generic.IEqualityComparer`1<UnityEngine.Vector3>
struct IEqualityComparer_1_t1554629144;
// System.IDisposable
struct IDisposable_t1628921374;
// UniRx.IObserver`1<UnityEngine.Vector3>
struct IObserver_1_t1442361396;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Vector33525329789.h"

// System.Void UniRx.ReactiveProperty`1<UnityEngine.Vector3>::.ctor()
extern "C"  void ReactiveProperty_1__ctor_m1711673549_gshared (ReactiveProperty_1_t4133662827 * __this, const MethodInfo* method);
#define ReactiveProperty_1__ctor_m1711673549(__this, method) ((  void (*) (ReactiveProperty_1_t4133662827 *, const MethodInfo*))ReactiveProperty_1__ctor_m1711673549_gshared)(__this, method)
// System.Void UniRx.ReactiveProperty`1<UnityEngine.Vector3>::.ctor(T)
extern "C"  void ReactiveProperty_1__ctor_m1522273841_gshared (ReactiveProperty_1_t4133662827 * __this, Vector3_t3525329789  ___initialValue0, const MethodInfo* method);
#define ReactiveProperty_1__ctor_m1522273841(__this, ___initialValue0, method) ((  void (*) (ReactiveProperty_1_t4133662827 *, Vector3_t3525329789 , const MethodInfo*))ReactiveProperty_1__ctor_m1522273841_gshared)(__this, ___initialValue0, method)
// System.Void UniRx.ReactiveProperty`1<UnityEngine.Vector3>::.ctor(UniRx.IObservable`1<T>)
extern "C"  void ReactiveProperty_1__ctor_m4047285996_gshared (ReactiveProperty_1_t4133662827 * __this, Il2CppObject* ___source0, const MethodInfo* method);
#define ReactiveProperty_1__ctor_m4047285996(__this, ___source0, method) ((  void (*) (ReactiveProperty_1_t4133662827 *, Il2CppObject*, const MethodInfo*))ReactiveProperty_1__ctor_m4047285996_gshared)(__this, ___source0, method)
// System.Void UniRx.ReactiveProperty`1<UnityEngine.Vector3>::.ctor(UniRx.IObservable`1<T>,T)
extern "C"  void ReactiveProperty_1__ctor_m2496444804_gshared (ReactiveProperty_1_t4133662827 * __this, Il2CppObject* ___source0, Vector3_t3525329789  ___initialValue1, const MethodInfo* method);
#define ReactiveProperty_1__ctor_m2496444804(__this, ___source0, ___initialValue1, method) ((  void (*) (ReactiveProperty_1_t4133662827 *, Il2CppObject*, Vector3_t3525329789 , const MethodInfo*))ReactiveProperty_1__ctor_m2496444804_gshared)(__this, ___source0, ___initialValue1, method)
// System.Void UniRx.ReactiveProperty`1<UnityEngine.Vector3>::.cctor()
extern "C"  void ReactiveProperty_1__cctor_m1040176256_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define ReactiveProperty_1__cctor_m1040176256(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))ReactiveProperty_1__cctor_m1040176256_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.IEqualityComparer`1<T> UniRx.ReactiveProperty`1<UnityEngine.Vector3>::get_EqualityComparer()
extern "C"  Il2CppObject* ReactiveProperty_1_get_EqualityComparer_m364503976_gshared (ReactiveProperty_1_t4133662827 * __this, const MethodInfo* method);
#define ReactiveProperty_1_get_EqualityComparer_m364503976(__this, method) ((  Il2CppObject* (*) (ReactiveProperty_1_t4133662827 *, const MethodInfo*))ReactiveProperty_1_get_EqualityComparer_m364503976_gshared)(__this, method)
// T UniRx.ReactiveProperty`1<UnityEngine.Vector3>::get_Value()
extern "C"  Vector3_t3525329789  ReactiveProperty_1_get_Value_m34629618_gshared (ReactiveProperty_1_t4133662827 * __this, const MethodInfo* method);
#define ReactiveProperty_1_get_Value_m34629618(__this, method) ((  Vector3_t3525329789  (*) (ReactiveProperty_1_t4133662827 *, const MethodInfo*))ReactiveProperty_1_get_Value_m34629618_gshared)(__this, method)
// System.Void UniRx.ReactiveProperty`1<UnityEngine.Vector3>::set_Value(T)
extern "C"  void ReactiveProperty_1_set_Value_m4111446527_gshared (ReactiveProperty_1_t4133662827 * __this, Vector3_t3525329789  ___value0, const MethodInfo* method);
#define ReactiveProperty_1_set_Value_m4111446527(__this, ___value0, method) ((  void (*) (ReactiveProperty_1_t4133662827 *, Vector3_t3525329789 , const MethodInfo*))ReactiveProperty_1_set_Value_m4111446527_gshared)(__this, ___value0, method)
// System.Void UniRx.ReactiveProperty`1<UnityEngine.Vector3>::SetValue(T)
extern "C"  void ReactiveProperty_1_SetValue_m495409112_gshared (ReactiveProperty_1_t4133662827 * __this, Vector3_t3525329789  ___value0, const MethodInfo* method);
#define ReactiveProperty_1_SetValue_m495409112(__this, ___value0, method) ((  void (*) (ReactiveProperty_1_t4133662827 *, Vector3_t3525329789 , const MethodInfo*))ReactiveProperty_1_SetValue_m495409112_gshared)(__this, ___value0, method)
// System.Void UniRx.ReactiveProperty`1<UnityEngine.Vector3>::SetValueAndForceNotify(T)
extern "C"  void ReactiveProperty_1_SetValueAndForceNotify_m3209282459_gshared (ReactiveProperty_1_t4133662827 * __this, Vector3_t3525329789  ___value0, const MethodInfo* method);
#define ReactiveProperty_1_SetValueAndForceNotify_m3209282459(__this, ___value0, method) ((  void (*) (ReactiveProperty_1_t4133662827 *, Vector3_t3525329789 , const MethodInfo*))ReactiveProperty_1_SetValueAndForceNotify_m3209282459_gshared)(__this, ___value0, method)
// System.IDisposable UniRx.ReactiveProperty`1<UnityEngine.Vector3>::Subscribe(UniRx.IObserver`1<T>)
extern "C"  Il2CppObject * ReactiveProperty_1_Subscribe_m2938903138_gshared (ReactiveProperty_1_t4133662827 * __this, Il2CppObject* ___observer0, const MethodInfo* method);
#define ReactiveProperty_1_Subscribe_m2938903138(__this, ___observer0, method) ((  Il2CppObject * (*) (ReactiveProperty_1_t4133662827 *, Il2CppObject*, const MethodInfo*))ReactiveProperty_1_Subscribe_m2938903138_gshared)(__this, ___observer0, method)
// System.Void UniRx.ReactiveProperty`1<UnityEngine.Vector3>::Dispose()
extern "C"  void ReactiveProperty_1_Dispose_m4139056586_gshared (ReactiveProperty_1_t4133662827 * __this, const MethodInfo* method);
#define ReactiveProperty_1_Dispose_m4139056586(__this, method) ((  void (*) (ReactiveProperty_1_t4133662827 *, const MethodInfo*))ReactiveProperty_1_Dispose_m4139056586_gshared)(__this, method)
// System.Void UniRx.ReactiveProperty`1<UnityEngine.Vector3>::Dispose(System.Boolean)
extern "C"  void ReactiveProperty_1_Dispose_m3980315073_gshared (ReactiveProperty_1_t4133662827 * __this, bool ___disposing0, const MethodInfo* method);
#define ReactiveProperty_1_Dispose_m3980315073(__this, ___disposing0, method) ((  void (*) (ReactiveProperty_1_t4133662827 *, bool, const MethodInfo*))ReactiveProperty_1_Dispose_m3980315073_gshared)(__this, ___disposing0, method)
// System.String UniRx.ReactiveProperty`1<UnityEngine.Vector3>::ToString()
extern "C"  String_t* ReactiveProperty_1_ToString_m3474795904_gshared (ReactiveProperty_1_t4133662827 * __this, const MethodInfo* method);
#define ReactiveProperty_1_ToString_m3474795904(__this, method) ((  String_t* (*) (ReactiveProperty_1_t4133662827 *, const MethodInfo*))ReactiveProperty_1_ToString_m3474795904_gshared)(__this, method)
// System.Boolean UniRx.ReactiveProperty`1<UnityEngine.Vector3>::IsRequiredSubscribeOnCurrentThread()
extern "C"  bool ReactiveProperty_1_IsRequiredSubscribeOnCurrentThread_m2417509150_gshared (ReactiveProperty_1_t4133662827 * __this, const MethodInfo* method);
#define ReactiveProperty_1_IsRequiredSubscribeOnCurrentThread_m2417509150(__this, method) ((  bool (*) (ReactiveProperty_1_t4133662827 *, const MethodInfo*))ReactiveProperty_1_IsRequiredSubscribeOnCurrentThread_m2417509150_gshared)(__this, method)
