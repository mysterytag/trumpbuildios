﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Generic.IEqualityComparer`1<UnityEngine.Vector2>
struct IEqualityComparer_1_t1554629143;
// System.Collections.Generic.IEqualityComparer`1<UnityEngine.Vector3>
struct IEqualityComparer_1_t1554629144;
// System.Collections.Generic.IEqualityComparer`1<UnityEngine.Vector4>
struct IEqualityComparer_1_t1554629145;
// System.Collections.Generic.IEqualityComparer`1<UnityEngine.Color>
struct IEqualityComparer_1_t3912442411;
// System.Collections.Generic.IEqualityComparer`1<UnityEngine.Rect>
struct IEqualityComparer_1_t3849695468;
// System.Collections.Generic.IEqualityComparer`1<UnityEngine.Bounds>
struct IEqualityComparer_1_t1547814333;
// System.Collections.Generic.IEqualityComparer`1<UnityEngine.Quaternion>
struct IEqualityComparer_1_t4215982630;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.UnityEqualityComparer
struct  UnityEqualityComparer_t3801418734  : public Il2CppObject
{
public:

public:
};

struct UnityEqualityComparer_t3801418734_StaticFields
{
public:
	// System.Collections.Generic.IEqualityComparer`1<UnityEngine.Vector2> UniRx.UnityEqualityComparer::Vector2
	Il2CppObject* ___Vector2_0;
	// System.Collections.Generic.IEqualityComparer`1<UnityEngine.Vector3> UniRx.UnityEqualityComparer::Vector3
	Il2CppObject* ___Vector3_1;
	// System.Collections.Generic.IEqualityComparer`1<UnityEngine.Vector4> UniRx.UnityEqualityComparer::Vector4
	Il2CppObject* ___Vector4_2;
	// System.Collections.Generic.IEqualityComparer`1<UnityEngine.Color> UniRx.UnityEqualityComparer::Color
	Il2CppObject* ___Color_3;
	// System.Collections.Generic.IEqualityComparer`1<UnityEngine.Rect> UniRx.UnityEqualityComparer::Rect
	Il2CppObject* ___Rect_4;
	// System.Collections.Generic.IEqualityComparer`1<UnityEngine.Bounds> UniRx.UnityEqualityComparer::Bounds
	Il2CppObject* ___Bounds_5;
	// System.Collections.Generic.IEqualityComparer`1<UnityEngine.Quaternion> UniRx.UnityEqualityComparer::Quaternion
	Il2CppObject* ___Quaternion_6;

public:
	inline static int32_t get_offset_of_Vector2_0() { return static_cast<int32_t>(offsetof(UnityEqualityComparer_t3801418734_StaticFields, ___Vector2_0)); }
	inline Il2CppObject* get_Vector2_0() const { return ___Vector2_0; }
	inline Il2CppObject** get_address_of_Vector2_0() { return &___Vector2_0; }
	inline void set_Vector2_0(Il2CppObject* value)
	{
		___Vector2_0 = value;
		Il2CppCodeGenWriteBarrier(&___Vector2_0, value);
	}

	inline static int32_t get_offset_of_Vector3_1() { return static_cast<int32_t>(offsetof(UnityEqualityComparer_t3801418734_StaticFields, ___Vector3_1)); }
	inline Il2CppObject* get_Vector3_1() const { return ___Vector3_1; }
	inline Il2CppObject** get_address_of_Vector3_1() { return &___Vector3_1; }
	inline void set_Vector3_1(Il2CppObject* value)
	{
		___Vector3_1 = value;
		Il2CppCodeGenWriteBarrier(&___Vector3_1, value);
	}

	inline static int32_t get_offset_of_Vector4_2() { return static_cast<int32_t>(offsetof(UnityEqualityComparer_t3801418734_StaticFields, ___Vector4_2)); }
	inline Il2CppObject* get_Vector4_2() const { return ___Vector4_2; }
	inline Il2CppObject** get_address_of_Vector4_2() { return &___Vector4_2; }
	inline void set_Vector4_2(Il2CppObject* value)
	{
		___Vector4_2 = value;
		Il2CppCodeGenWriteBarrier(&___Vector4_2, value);
	}

	inline static int32_t get_offset_of_Color_3() { return static_cast<int32_t>(offsetof(UnityEqualityComparer_t3801418734_StaticFields, ___Color_3)); }
	inline Il2CppObject* get_Color_3() const { return ___Color_3; }
	inline Il2CppObject** get_address_of_Color_3() { return &___Color_3; }
	inline void set_Color_3(Il2CppObject* value)
	{
		___Color_3 = value;
		Il2CppCodeGenWriteBarrier(&___Color_3, value);
	}

	inline static int32_t get_offset_of_Rect_4() { return static_cast<int32_t>(offsetof(UnityEqualityComparer_t3801418734_StaticFields, ___Rect_4)); }
	inline Il2CppObject* get_Rect_4() const { return ___Rect_4; }
	inline Il2CppObject** get_address_of_Rect_4() { return &___Rect_4; }
	inline void set_Rect_4(Il2CppObject* value)
	{
		___Rect_4 = value;
		Il2CppCodeGenWriteBarrier(&___Rect_4, value);
	}

	inline static int32_t get_offset_of_Bounds_5() { return static_cast<int32_t>(offsetof(UnityEqualityComparer_t3801418734_StaticFields, ___Bounds_5)); }
	inline Il2CppObject* get_Bounds_5() const { return ___Bounds_5; }
	inline Il2CppObject** get_address_of_Bounds_5() { return &___Bounds_5; }
	inline void set_Bounds_5(Il2CppObject* value)
	{
		___Bounds_5 = value;
		Il2CppCodeGenWriteBarrier(&___Bounds_5, value);
	}

	inline static int32_t get_offset_of_Quaternion_6() { return static_cast<int32_t>(offsetof(UnityEqualityComparer_t3801418734_StaticFields, ___Quaternion_6)); }
	inline Il2CppObject* get_Quaternion_6() const { return ___Quaternion_6; }
	inline Il2CppObject** get_address_of_Quaternion_6() { return &___Quaternion_6; }
	inline void set_Quaternion_6(Il2CppObject* value)
	{
		___Quaternion_6 = value;
		Il2CppCodeGenWriteBarrier(&___Quaternion_6, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
