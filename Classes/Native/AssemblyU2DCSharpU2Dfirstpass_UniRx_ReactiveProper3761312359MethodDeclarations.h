﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.ReactiveProperty`1/ReactivePropertyObserver<UnityEngine.Vector4>
struct ReactivePropertyObserver_t3761312359;
// UniRx.ReactiveProperty`1<UnityEngine.Vector4>
struct ReactiveProperty_1_t4133662828;
// System.Exception
struct Exception_t1967233988;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Vector43525329790.h"
#include "mscorlib_System_Exception1967233988.h"

// System.Void UniRx.ReactiveProperty`1/ReactivePropertyObserver<UnityEngine.Vector4>::.ctor(UniRx.ReactiveProperty`1<T>)
extern "C"  void ReactivePropertyObserver__ctor_m1805399288_gshared (ReactivePropertyObserver_t3761312359 * __this, ReactiveProperty_1_t4133662828 * ___parent0, const MethodInfo* method);
#define ReactivePropertyObserver__ctor_m1805399288(__this, ___parent0, method) ((  void (*) (ReactivePropertyObserver_t3761312359 *, ReactiveProperty_1_t4133662828 *, const MethodInfo*))ReactivePropertyObserver__ctor_m1805399288_gshared)(__this, ___parent0, method)
// System.Void UniRx.ReactiveProperty`1/ReactivePropertyObserver<UnityEngine.Vector4>::OnNext(T)
extern "C"  void ReactivePropertyObserver_OnNext_m4067192243_gshared (ReactivePropertyObserver_t3761312359 * __this, Vector4_t3525329790  ___value0, const MethodInfo* method);
#define ReactivePropertyObserver_OnNext_m4067192243(__this, ___value0, method) ((  void (*) (ReactivePropertyObserver_t3761312359 *, Vector4_t3525329790 , const MethodInfo*))ReactivePropertyObserver_OnNext_m4067192243_gshared)(__this, ___value0, method)
// System.Void UniRx.ReactiveProperty`1/ReactivePropertyObserver<UnityEngine.Vector4>::OnError(System.Exception)
extern "C"  void ReactivePropertyObserver_OnError_m1073529538_gshared (ReactivePropertyObserver_t3761312359 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method);
#define ReactivePropertyObserver_OnError_m1073529538(__this, ___error0, method) ((  void (*) (ReactivePropertyObserver_t3761312359 *, Exception_t1967233988 *, const MethodInfo*))ReactivePropertyObserver_OnError_m1073529538_gshared)(__this, ___error0, method)
// System.Void UniRx.ReactiveProperty`1/ReactivePropertyObserver<UnityEngine.Vector4>::OnCompleted()
extern "C"  void ReactivePropertyObserver_OnCompleted_m995625237_gshared (ReactivePropertyObserver_t3761312359 * __this, const MethodInfo* method);
#define ReactivePropertyObserver_OnCompleted_m995625237(__this, method) ((  void (*) (ReactivePropertyObserver_t3761312359 *, const MethodInfo*))ReactivePropertyObserver_OnCompleted_m995625237_gshared)(__this, method)
