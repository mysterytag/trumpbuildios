﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Generic.IList`1<System.Object>
struct IList_1_t3003598734;
// UnityEngine.Texture2D
struct Texture2D_t2509538522;

#include "AssemblyU2DCSharp_Facebook_Unity_ResultBase3940793997.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Facebook.Unity.GraphResult
struct  GraphResult_t1509843146  : public ResultBase_t3940793997
{
public:
	// System.Collections.Generic.IList`1<System.Object> Facebook.Unity.GraphResult::<ResultList>k__BackingField
	Il2CppObject* ___U3CResultListU3Ek__BackingField_9;
	// UnityEngine.Texture2D Facebook.Unity.GraphResult::<Texture>k__BackingField
	Texture2D_t2509538522 * ___U3CTextureU3Ek__BackingField_10;

public:
	inline static int32_t get_offset_of_U3CResultListU3Ek__BackingField_9() { return static_cast<int32_t>(offsetof(GraphResult_t1509843146, ___U3CResultListU3Ek__BackingField_9)); }
	inline Il2CppObject* get_U3CResultListU3Ek__BackingField_9() const { return ___U3CResultListU3Ek__BackingField_9; }
	inline Il2CppObject** get_address_of_U3CResultListU3Ek__BackingField_9() { return &___U3CResultListU3Ek__BackingField_9; }
	inline void set_U3CResultListU3Ek__BackingField_9(Il2CppObject* value)
	{
		___U3CResultListU3Ek__BackingField_9 = value;
		Il2CppCodeGenWriteBarrier(&___U3CResultListU3Ek__BackingField_9, value);
	}

	inline static int32_t get_offset_of_U3CTextureU3Ek__BackingField_10() { return static_cast<int32_t>(offsetof(GraphResult_t1509843146, ___U3CTextureU3Ek__BackingField_10)); }
	inline Texture2D_t2509538522 * get_U3CTextureU3Ek__BackingField_10() const { return ___U3CTextureU3Ek__BackingField_10; }
	inline Texture2D_t2509538522 ** get_address_of_U3CTextureU3Ek__BackingField_10() { return &___U3CTextureU3Ek__BackingField_10; }
	inline void set_U3CTextureU3Ek__BackingField_10(Texture2D_t2509538522 * value)
	{
		___U3CTextureU3Ek__BackingField_10 = value;
		Il2CppCodeGenWriteBarrier(&___U3CTextureU3Ek__BackingField_10, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
