﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.ClientModels.GetLeaderboardAroundPlayerResult
struct GetLeaderboardAroundPlayerResult_t460441506;
// System.Collections.Generic.List`1<PlayFab.ClientModels.PlayerLeaderboardEntry>
struct List_1_t3038850335;

#include "codegen/il2cpp-codegen.h"

// System.Void PlayFab.ClientModels.GetLeaderboardAroundPlayerResult::.ctor()
extern "C"  void GetLeaderboardAroundPlayerResult__ctor_m3963226843 (GetLeaderboardAroundPlayerResult_t460441506 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<PlayFab.ClientModels.PlayerLeaderboardEntry> PlayFab.ClientModels.GetLeaderboardAroundPlayerResult::get_Leaderboard()
extern "C"  List_1_t3038850335 * GetLeaderboardAroundPlayerResult_get_Leaderboard_m901597346 (GetLeaderboardAroundPlayerResult_t460441506 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.GetLeaderboardAroundPlayerResult::set_Leaderboard(System.Collections.Generic.List`1<PlayFab.ClientModels.PlayerLeaderboardEntry>)
extern "C"  void GetLeaderboardAroundPlayerResult_set_Leaderboard_m2608843953 (GetLeaderboardAroundPlayerResult_t460441506 * __this, List_1_t3038850335 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
