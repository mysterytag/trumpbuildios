﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.ObjectModel.Collection`1<UniRx.Tuple`2<System.Object,System.Object>>
struct Collection_1_t2338249549;
// System.Collections.Generic.IList`1<UniRx.Tuple`2<System.Object,System.Object>>
struct IList_1_t2535754133;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t287207039;
// System.Object
struct Il2CppObject;
// UniRx.Tuple`2<System.Object,System.Object>[]
struct Tuple_2U5BU5D_t3897934010;
// System.Collections.Generic.IEnumerator`1<UniRx.Tuple`2<System.Object,System.Object>>
struct IEnumerator_1_t1852368267;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array2840145358.h"
#include "mscorlib_System_Object837106420.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Tuple_2_gen369261819.h"

// System.Void System.Collections.ObjectModel.Collection`1<UniRx.Tuple`2<System.Object,System.Object>>::.ctor()
extern "C"  void Collection_1__ctor_m2802934255_gshared (Collection_1_t2338249549 * __this, const MethodInfo* method);
#define Collection_1__ctor_m2802934255(__this, method) ((  void (*) (Collection_1_t2338249549 *, const MethodInfo*))Collection_1__ctor_m2802934255_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<UniRx.Tuple`2<System.Object,System.Object>>::.ctor(System.Collections.Generic.IList`1<T>)
extern "C"  void Collection_1__ctor_m3277856390_gshared (Collection_1_t2338249549 * __this, Il2CppObject* ___list0, const MethodInfo* method);
#define Collection_1__ctor_m3277856390(__this, ___list0, method) ((  void (*) (Collection_1_t2338249549 *, Il2CppObject*, const MethodInfo*))Collection_1__ctor_m3277856390_gshared)(__this, ___list0, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UniRx.Tuple`2<System.Object,System.Object>>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C"  bool Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m1891404776_gshared (Collection_1_t2338249549 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m1891404776(__this, method) ((  bool (*) (Collection_1_t2338249549 *, const MethodInfo*))Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m1891404776_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<UniRx.Tuple`2<System.Object,System.Object>>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void Collection_1_System_Collections_ICollection_CopyTo_m4101468725_gshared (Collection_1_t2338249549 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_CopyTo_m4101468725(__this, ___array0, ___index1, method) ((  void (*) (Collection_1_t2338249549 *, Il2CppArray *, int32_t, const MethodInfo*))Collection_1_System_Collections_ICollection_CopyTo_m4101468725_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.ObjectModel.Collection`1<UniRx.Tuple`2<System.Object,System.Object>>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * Collection_1_System_Collections_IEnumerable_GetEnumerator_m1037225092_gshared (Collection_1_t2338249549 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IEnumerable_GetEnumerator_m1037225092(__this, method) ((  Il2CppObject * (*) (Collection_1_t2338249549 *, const MethodInfo*))Collection_1_System_Collections_IEnumerable_GetEnumerator_m1037225092_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<UniRx.Tuple`2<System.Object,System.Object>>::System.Collections.IList.Add(System.Object)
extern "C"  int32_t Collection_1_System_Collections_IList_Add_m3051129145_gshared (Collection_1_t2338249549 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Add_m3051129145(__this, ___value0, method) ((  int32_t (*) (Collection_1_t2338249549 *, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_Add_m3051129145_gshared)(__this, ___value0, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UniRx.Tuple`2<System.Object,System.Object>>::System.Collections.IList.Contains(System.Object)
extern "C"  bool Collection_1_System_Collections_IList_Contains_m132120743_gshared (Collection_1_t2338249549 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Contains_m132120743(__this, ___value0, method) ((  bool (*) (Collection_1_t2338249549 *, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_Contains_m132120743_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<UniRx.Tuple`2<System.Object,System.Object>>::System.Collections.IList.IndexOf(System.Object)
extern "C"  int32_t Collection_1_System_Collections_IList_IndexOf_m2765323025_gshared (Collection_1_t2338249549 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Collection_1_System_Collections_IList_IndexOf_m2765323025(__this, ___value0, method) ((  int32_t (*) (Collection_1_t2338249549 *, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_IndexOf_m2765323025_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.Collection`1<UniRx.Tuple`2<System.Object,System.Object>>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C"  void Collection_1_System_Collections_IList_Insert_m1905542404_gshared (Collection_1_t2338249549 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Insert_m1905542404(__this, ___index0, ___value1, method) ((  void (*) (Collection_1_t2338249549 *, int32_t, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_Insert_m1905542404_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.ObjectModel.Collection`1<UniRx.Tuple`2<System.Object,System.Object>>::System.Collections.IList.Remove(System.Object)
extern "C"  void Collection_1_System_Collections_IList_Remove_m908037540_gshared (Collection_1_t2338249549 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Remove_m908037540(__this, ___value0, method) ((  void (*) (Collection_1_t2338249549 *, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_Remove_m908037540_gshared)(__this, ___value0, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UniRx.Tuple`2<System.Object,System.Object>>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool Collection_1_System_Collections_ICollection_get_IsSynchronized_m1813987669_gshared (Collection_1_t2338249549 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_get_IsSynchronized_m1813987669(__this, method) ((  bool (*) (Collection_1_t2338249549 *, const MethodInfo*))Collection_1_System_Collections_ICollection_get_IsSynchronized_m1813987669_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.Collection`1<UniRx.Tuple`2<System.Object,System.Object>>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * Collection_1_System_Collections_ICollection_get_SyncRoot_m708599303_gshared (Collection_1_t2338249549 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_get_SyncRoot_m708599303(__this, method) ((  Il2CppObject * (*) (Collection_1_t2338249549 *, const MethodInfo*))Collection_1_System_Collections_ICollection_get_SyncRoot_m708599303_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UniRx.Tuple`2<System.Object,System.Object>>::System.Collections.IList.get_IsFixedSize()
extern "C"  bool Collection_1_System_Collections_IList_get_IsFixedSize_m1803745622_gshared (Collection_1_t2338249549 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_IsFixedSize_m1803745622(__this, method) ((  bool (*) (Collection_1_t2338249549 *, const MethodInfo*))Collection_1_System_Collections_IList_get_IsFixedSize_m1803745622_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UniRx.Tuple`2<System.Object,System.Object>>::System.Collections.IList.get_IsReadOnly()
extern "C"  bool Collection_1_System_Collections_IList_get_IsReadOnly_m3837330147_gshared (Collection_1_t2338249549 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_IsReadOnly_m3837330147(__this, method) ((  bool (*) (Collection_1_t2338249549 *, const MethodInfo*))Collection_1_System_Collections_IList_get_IsReadOnly_m3837330147_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.Collection`1<UniRx.Tuple`2<System.Object,System.Object>>::System.Collections.IList.get_Item(System.Int32)
extern "C"  Il2CppObject * Collection_1_System_Collections_IList_get_Item_m3424709070_gshared (Collection_1_t2338249549 * __this, int32_t ___index0, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_Item_m3424709070(__this, ___index0, method) ((  Il2CppObject * (*) (Collection_1_t2338249549 *, int32_t, const MethodInfo*))Collection_1_System_Collections_IList_get_Item_m3424709070_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.Collection`1<UniRx.Tuple`2<System.Object,System.Object>>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C"  void Collection_1_System_Collections_IList_set_Item_m2184134619_gshared (Collection_1_t2338249549 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method);
#define Collection_1_System_Collections_IList_set_Item_m2184134619(__this, ___index0, ___value1, method) ((  void (*) (Collection_1_t2338249549 *, int32_t, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_set_Item_m2184134619_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.ObjectModel.Collection`1<UniRx.Tuple`2<System.Object,System.Object>>::Add(T)
extern "C"  void Collection_1_Add_m2020222128_gshared (Collection_1_t2338249549 * __this, Tuple_2_t369261819  ___item0, const MethodInfo* method);
#define Collection_1_Add_m2020222128(__this, ___item0, method) ((  void (*) (Collection_1_t2338249549 *, Tuple_2_t369261819 , const MethodInfo*))Collection_1_Add_m2020222128_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<UniRx.Tuple`2<System.Object,System.Object>>::Clear()
extern "C"  void Collection_1_Clear_m209067546_gshared (Collection_1_t2338249549 * __this, const MethodInfo* method);
#define Collection_1_Clear_m209067546(__this, method) ((  void (*) (Collection_1_t2338249549 *, const MethodInfo*))Collection_1_Clear_m209067546_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<UniRx.Tuple`2<System.Object,System.Object>>::ClearItems()
extern "C"  void Collection_1_ClearItems_m41304872_gshared (Collection_1_t2338249549 * __this, const MethodInfo* method);
#define Collection_1_ClearItems_m41304872(__this, method) ((  void (*) (Collection_1_t2338249549 *, const MethodInfo*))Collection_1_ClearItems_m41304872_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UniRx.Tuple`2<System.Object,System.Object>>::Contains(T)
extern "C"  bool Collection_1_Contains_m1031918604_gshared (Collection_1_t2338249549 * __this, Tuple_2_t369261819  ___item0, const MethodInfo* method);
#define Collection_1_Contains_m1031918604(__this, ___item0, method) ((  bool (*) (Collection_1_t2338249549 *, Tuple_2_t369261819 , const MethodInfo*))Collection_1_Contains_m1031918604_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<UniRx.Tuple`2<System.Object,System.Object>>::CopyTo(T[],System.Int32)
extern "C"  void Collection_1_CopyTo_m435306656_gshared (Collection_1_t2338249549 * __this, Tuple_2U5BU5D_t3897934010* ___array0, int32_t ___index1, const MethodInfo* method);
#define Collection_1_CopyTo_m435306656(__this, ___array0, ___index1, method) ((  void (*) (Collection_1_t2338249549 *, Tuple_2U5BU5D_t3897934010*, int32_t, const MethodInfo*))Collection_1_CopyTo_m435306656_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.ObjectModel.Collection`1<UniRx.Tuple`2<System.Object,System.Object>>::GetEnumerator()
extern "C"  Il2CppObject* Collection_1_GetEnumerator_m3801016163_gshared (Collection_1_t2338249549 * __this, const MethodInfo* method);
#define Collection_1_GetEnumerator_m3801016163(__this, method) ((  Il2CppObject* (*) (Collection_1_t2338249549 *, const MethodInfo*))Collection_1_GetEnumerator_m3801016163_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<UniRx.Tuple`2<System.Object,System.Object>>::IndexOf(T)
extern "C"  int32_t Collection_1_IndexOf_m4216597100_gshared (Collection_1_t2338249549 * __this, Tuple_2_t369261819  ___item0, const MethodInfo* method);
#define Collection_1_IndexOf_m4216597100(__this, ___item0, method) ((  int32_t (*) (Collection_1_t2338249549 *, Tuple_2_t369261819 , const MethodInfo*))Collection_1_IndexOf_m4216597100_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<UniRx.Tuple`2<System.Object,System.Object>>::Insert(System.Int32,T)
extern "C"  void Collection_1_Insert_m4201119511_gshared (Collection_1_t2338249549 * __this, int32_t ___index0, Tuple_2_t369261819  ___item1, const MethodInfo* method);
#define Collection_1_Insert_m4201119511(__this, ___index0, ___item1, method) ((  void (*) (Collection_1_t2338249549 *, int32_t, Tuple_2_t369261819 , const MethodInfo*))Collection_1_Insert_m4201119511_gshared)(__this, ___index0, ___item1, method)
// System.Void System.Collections.ObjectModel.Collection`1<UniRx.Tuple`2<System.Object,System.Object>>::InsertItem(System.Int32,T)
extern "C"  void Collection_1_InsertItem_m3679177162_gshared (Collection_1_t2338249549 * __this, int32_t ___index0, Tuple_2_t369261819  ___item1, const MethodInfo* method);
#define Collection_1_InsertItem_m3679177162(__this, ___index0, ___item1, method) ((  void (*) (Collection_1_t2338249549 *, int32_t, Tuple_2_t369261819 , const MethodInfo*))Collection_1_InsertItem_m3679177162_gshared)(__this, ___index0, ___item1, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UniRx.Tuple`2<System.Object,System.Object>>::Remove(T)
extern "C"  bool Collection_1_Remove_m2297702151_gshared (Collection_1_t2338249549 * __this, Tuple_2_t369261819  ___item0, const MethodInfo* method);
#define Collection_1_Remove_m2297702151(__this, ___item0, method) ((  bool (*) (Collection_1_t2338249549 *, Tuple_2_t369261819 , const MethodInfo*))Collection_1_Remove_m2297702151_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<UniRx.Tuple`2<System.Object,System.Object>>::RemoveAt(System.Int32)
extern "C"  void Collection_1_RemoveAt_m2074972381_gshared (Collection_1_t2338249549 * __this, int32_t ___index0, const MethodInfo* method);
#define Collection_1_RemoveAt_m2074972381(__this, ___index0, method) ((  void (*) (Collection_1_t2338249549 *, int32_t, const MethodInfo*))Collection_1_RemoveAt_m2074972381_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.Collection`1<UniRx.Tuple`2<System.Object,System.Object>>::RemoveItem(System.Int32)
extern "C"  void Collection_1_RemoveItem_m4083257789_gshared (Collection_1_t2338249549 * __this, int32_t ___index0, const MethodInfo* method);
#define Collection_1_RemoveItem_m4083257789(__this, ___index0, method) ((  void (*) (Collection_1_t2338249549 *, int32_t, const MethodInfo*))Collection_1_RemoveItem_m4083257789_gshared)(__this, ___index0, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<UniRx.Tuple`2<System.Object,System.Object>>::get_Count()
extern "C"  int32_t Collection_1_get_Count_m1682798735_gshared (Collection_1_t2338249549 * __this, const MethodInfo* method);
#define Collection_1_get_Count_m1682798735(__this, method) ((  int32_t (*) (Collection_1_t2338249549 *, const MethodInfo*))Collection_1_get_Count_m1682798735_gshared)(__this, method)
// T System.Collections.ObjectModel.Collection`1<UniRx.Tuple`2<System.Object,System.Object>>::get_Item(System.Int32)
extern "C"  Tuple_2_t369261819  Collection_1_get_Item_m2049497603_gshared (Collection_1_t2338249549 * __this, int32_t ___index0, const MethodInfo* method);
#define Collection_1_get_Item_m2049497603(__this, ___index0, method) ((  Tuple_2_t369261819  (*) (Collection_1_t2338249549 *, int32_t, const MethodInfo*))Collection_1_get_Item_m2049497603_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.Collection`1<UniRx.Tuple`2<System.Object,System.Object>>::set_Item(System.Int32,T)
extern "C"  void Collection_1_set_Item_m1816188462_gshared (Collection_1_t2338249549 * __this, int32_t ___index0, Tuple_2_t369261819  ___value1, const MethodInfo* method);
#define Collection_1_set_Item_m1816188462(__this, ___index0, ___value1, method) ((  void (*) (Collection_1_t2338249549 *, int32_t, Tuple_2_t369261819 , const MethodInfo*))Collection_1_set_Item_m1816188462_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.ObjectModel.Collection`1<UniRx.Tuple`2<System.Object,System.Object>>::SetItem(System.Int32,T)
extern "C"  void Collection_1_SetItem_m2702275723_gshared (Collection_1_t2338249549 * __this, int32_t ___index0, Tuple_2_t369261819  ___item1, const MethodInfo* method);
#define Collection_1_SetItem_m2702275723(__this, ___index0, ___item1, method) ((  void (*) (Collection_1_t2338249549 *, int32_t, Tuple_2_t369261819 , const MethodInfo*))Collection_1_SetItem_m2702275723_gshared)(__this, ___index0, ___item1, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UniRx.Tuple`2<System.Object,System.Object>>::IsValidItem(System.Object)
extern "C"  bool Collection_1_IsValidItem_m36865184_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * ___item0, const MethodInfo* method);
#define Collection_1_IsValidItem_m36865184(__this /* static, unused */, ___item0, method) ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))Collection_1_IsValidItem_m36865184_gshared)(__this /* static, unused */, ___item0, method)
// T System.Collections.ObjectModel.Collection`1<UniRx.Tuple`2<System.Object,System.Object>>::ConvertItem(System.Object)
extern "C"  Tuple_2_t369261819  Collection_1_ConvertItem_m2039467874_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * ___item0, const MethodInfo* method);
#define Collection_1_ConvertItem_m2039467874(__this /* static, unused */, ___item0, method) ((  Tuple_2_t369261819  (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))Collection_1_ConvertItem_m2039467874_gshared)(__this /* static, unused */, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<UniRx.Tuple`2<System.Object,System.Object>>::CheckWritable(System.Collections.Generic.IList`1<T>)
extern "C"  void Collection_1_CheckWritable_m27073120_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* ___list0, const MethodInfo* method);
#define Collection_1_CheckWritable_m27073120(__this /* static, unused */, ___list0, method) ((  void (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Collection_1_CheckWritable_m27073120_gshared)(__this /* static, unused */, ___list0, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UniRx.Tuple`2<System.Object,System.Object>>::IsSynchronized(System.Collections.Generic.IList`1<T>)
extern "C"  bool Collection_1_IsSynchronized_m2513444_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* ___list0, const MethodInfo* method);
#define Collection_1_IsSynchronized_m2513444(__this /* static, unused */, ___list0, method) ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Collection_1_IsSynchronized_m2513444_gshared)(__this /* static, unused */, ___list0, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UniRx.Tuple`2<System.Object,System.Object>>::IsFixedSize(System.Collections.Generic.IList`1<T>)
extern "C"  bool Collection_1_IsFixedSize_m2524888827_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* ___list0, const MethodInfo* method);
#define Collection_1_IsFixedSize_m2524888827(__this /* static, unused */, ___list0, method) ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Collection_1_IsFixedSize_m2524888827_gshared)(__this /* static, unused */, ___list0, method)
