﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Object
struct Il2CppObject;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Nullable_1_gen4258606580.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_Transaction1372568672.h"
#include "mscorlib_System_Object837106420.h"

// System.Void System.Nullable`1<PlayFab.ClientModels.TransactionStatus>::.ctor(T)
extern "C"  void Nullable_1__ctor_m4007637675_gshared (Nullable_1_t4258606580 * __this, int32_t ___value0, const MethodInfo* method);
#define Nullable_1__ctor_m4007637675(__this, ___value0, method) ((  void (*) (Nullable_1_t4258606580 *, int32_t, const MethodInfo*))Nullable_1__ctor_m4007637675_gshared)(__this, ___value0, method)
// System.Boolean System.Nullable`1<PlayFab.ClientModels.TransactionStatus>::get_HasValue()
extern "C"  bool Nullable_1_get_HasValue_m3137646287_gshared (Nullable_1_t4258606580 * __this, const MethodInfo* method);
#define Nullable_1_get_HasValue_m3137646287(__this, method) ((  bool (*) (Nullable_1_t4258606580 *, const MethodInfo*))Nullable_1_get_HasValue_m3137646287_gshared)(__this, method)
// T System.Nullable`1<PlayFab.ClientModels.TransactionStatus>::get_Value()
extern "C"  int32_t Nullable_1_get_Value_m2900966136_gshared (Nullable_1_t4258606580 * __this, const MethodInfo* method);
#define Nullable_1_get_Value_m2900966136(__this, method) ((  int32_t (*) (Nullable_1_t4258606580 *, const MethodInfo*))Nullable_1_get_Value_m2900966136_gshared)(__this, method)
// System.Boolean System.Nullable`1<PlayFab.ClientModels.TransactionStatus>::Equals(System.Object)
extern "C"  bool Nullable_1_Equals_m1623809222_gshared (Nullable_1_t4258606580 * __this, Il2CppObject * ___other0, const MethodInfo* method);
#define Nullable_1_Equals_m1623809222(__this, ___other0, method) ((  bool (*) (Nullable_1_t4258606580 *, Il2CppObject *, const MethodInfo*))Nullable_1_Equals_m1623809222_gshared)(__this, ___other0, method)
// System.Boolean System.Nullable`1<PlayFab.ClientModels.TransactionStatus>::Equals(System.Nullable`1<T>)
extern "C"  bool Nullable_1_Equals_m1927375801_gshared (Nullable_1_t4258606580 * __this, Nullable_1_t4258606580  ___other0, const MethodInfo* method);
#define Nullable_1_Equals_m1927375801(__this, ___other0, method) ((  bool (*) (Nullable_1_t4258606580 *, Nullable_1_t4258606580 , const MethodInfo*))Nullable_1_Equals_m1927375801_gshared)(__this, ___other0, method)
// System.Int32 System.Nullable`1<PlayFab.ClientModels.TransactionStatus>::GetHashCode()
extern "C"  int32_t Nullable_1_GetHashCode_m2127257502_gshared (Nullable_1_t4258606580 * __this, const MethodInfo* method);
#define Nullable_1_GetHashCode_m2127257502(__this, method) ((  int32_t (*) (Nullable_1_t4258606580 *, const MethodInfo*))Nullable_1_GetHashCode_m2127257502_gshared)(__this, method)
// T System.Nullable`1<PlayFab.ClientModels.TransactionStatus>::GetValueOrDefault()
extern "C"  int32_t Nullable_1_GetValueOrDefault_m3706379091_gshared (Nullable_1_t4258606580 * __this, const MethodInfo* method);
#define Nullable_1_GetValueOrDefault_m3706379091(__this, method) ((  int32_t (*) (Nullable_1_t4258606580 *, const MethodInfo*))Nullable_1_GetValueOrDefault_m3706379091_gshared)(__this, method)
// System.String System.Nullable`1<PlayFab.ClientModels.TransactionStatus>::ToString()
extern "C"  String_t* Nullable_1_ToString_m4213201722_gshared (Nullable_1_t4258606580 * __this, const MethodInfo* method);
#define Nullable_1_ToString_m4213201722(__this, method) ((  String_t* (*) (Nullable_1_t4258606580 *, const MethodInfo*))Nullable_1_ToString_m4213201722_gshared)(__this, method)
// System.Nullable`1<T> System.Nullable`1<PlayFab.ClientModels.TransactionStatus>::op_Implicit(T)
extern "C"  Nullable_1_t4258606580  Nullable_1_op_Implicit_m2888462196_gshared (Il2CppObject * __this /* static, unused */, int32_t ___value0, const MethodInfo* method);
#define Nullable_1_op_Implicit_m2888462196(__this /* static, unused */, ___value0, method) ((  Nullable_1_t4258606580  (*) (Il2CppObject * /* static, unused */, int32_t, const MethodInfo*))Nullable_1_op_Implicit_m2888462196_gshared)(__this /* static, unused */, ___value0, method)
