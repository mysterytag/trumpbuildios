﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.CombineLatestObservable`3/CombineLatest/RightObserver<System.Object,System.Object,System.Object>
struct RightObserver_t812748179;
// UniRx.Operators.CombineLatestObservable`3/CombineLatest<System.Object,System.Object,System.Object>
struct CombineLatest_t2347858151;
// System.Object
struct Il2CppObject;
// System.Exception
struct Exception_t1967233988;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_Exception1967233988.h"

// System.Void UniRx.Operators.CombineLatestObservable`3/CombineLatest/RightObserver<System.Object,System.Object,System.Object>::.ctor(UniRx.Operators.CombineLatestObservable`3/CombineLatest<TLeft,TRight,TResult>)
extern "C"  void RightObserver__ctor_m187976476_gshared (RightObserver_t812748179 * __this, CombineLatest_t2347858151 * ___parent0, const MethodInfo* method);
#define RightObserver__ctor_m187976476(__this, ___parent0, method) ((  void (*) (RightObserver_t812748179 *, CombineLatest_t2347858151 *, const MethodInfo*))RightObserver__ctor_m187976476_gshared)(__this, ___parent0, method)
// System.Void UniRx.Operators.CombineLatestObservable`3/CombineLatest/RightObserver<System.Object,System.Object,System.Object>::OnNext(TRight)
extern "C"  void RightObserver_OnNext_m1369781400_gshared (RightObserver_t812748179 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define RightObserver_OnNext_m1369781400(__this, ___value0, method) ((  void (*) (RightObserver_t812748179 *, Il2CppObject *, const MethodInfo*))RightObserver_OnNext_m1369781400_gshared)(__this, ___value0, method)
// System.Void UniRx.Operators.CombineLatestObservable`3/CombineLatest/RightObserver<System.Object,System.Object,System.Object>::OnError(System.Exception)
extern "C"  void RightObserver_OnError_m2404643181_gshared (RightObserver_t812748179 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method);
#define RightObserver_OnError_m2404643181(__this, ___error0, method) ((  void (*) (RightObserver_t812748179 *, Exception_t1967233988 *, const MethodInfo*))RightObserver_OnError_m2404643181_gshared)(__this, ___error0, method)
// System.Void UniRx.Operators.CombineLatestObservable`3/CombineLatest/RightObserver<System.Object,System.Object,System.Object>::OnCompleted()
extern "C"  void RightObserver_OnCompleted_m3092750400_gshared (RightObserver_t812748179 * __this, const MethodInfo* method);
#define RightObserver_OnCompleted_m3092750400(__this, method) ((  void (*) (RightObserver_t812748179 *, const MethodInfo*))RightObserver_OnCompleted_m3092750400_gshared)(__this, method)
