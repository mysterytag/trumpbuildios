﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// AdsManager
struct AdsManager_t814343421;
// GlobalStat
struct GlobalStat_t1134678199;
// GameController
struct GameController_t2782302542;
// Message
struct Message_t2619578343;
// TutorialPlayer
struct TutorialPlayer_t4281139199;
// System.Action`2<System.String,System.String>
struct Action_2_t2887221574;
// System.Action`3<System.String,System.String,System.String>
struct Action_3_t2150340505;

#include "UnityEngine_UnityEngine_MonoBehaviour3012272455.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AdsManager
struct  AdsManager_t814343421  : public MonoBehaviour_t3012272455
{
public:
	// System.Boolean AdsManager::AdsStat
	bool ___AdsStat_3;
	// GlobalStat AdsManager::GlobalStat
	GlobalStat_t1134678199 * ___GlobalStat_4;
	// GameController AdsManager::GameController
	GameController_t2782302542 * ___GameController_5;
	// Message AdsManager::Message
	Message_t2619578343 * ___Message_6;
	// System.Single AdsManager::lastInterstitialShowTime
	float ___lastInterstitialShowTime_7;
	// System.Boolean AdsManager::_videoPlaying
	bool ____videoPlaying_8;
	// System.Single AdsManager::interval
	float ___interval_9;
	// TutorialPlayer AdsManager::TutorialPlayer
	TutorialPlayer_t4281139199 * ___TutorialPlayer_10;

public:
	inline static int32_t get_offset_of_AdsStat_3() { return static_cast<int32_t>(offsetof(AdsManager_t814343421, ___AdsStat_3)); }
	inline bool get_AdsStat_3() const { return ___AdsStat_3; }
	inline bool* get_address_of_AdsStat_3() { return &___AdsStat_3; }
	inline void set_AdsStat_3(bool value)
	{
		___AdsStat_3 = value;
	}

	inline static int32_t get_offset_of_GlobalStat_4() { return static_cast<int32_t>(offsetof(AdsManager_t814343421, ___GlobalStat_4)); }
	inline GlobalStat_t1134678199 * get_GlobalStat_4() const { return ___GlobalStat_4; }
	inline GlobalStat_t1134678199 ** get_address_of_GlobalStat_4() { return &___GlobalStat_4; }
	inline void set_GlobalStat_4(GlobalStat_t1134678199 * value)
	{
		___GlobalStat_4 = value;
		Il2CppCodeGenWriteBarrier(&___GlobalStat_4, value);
	}

	inline static int32_t get_offset_of_GameController_5() { return static_cast<int32_t>(offsetof(AdsManager_t814343421, ___GameController_5)); }
	inline GameController_t2782302542 * get_GameController_5() const { return ___GameController_5; }
	inline GameController_t2782302542 ** get_address_of_GameController_5() { return &___GameController_5; }
	inline void set_GameController_5(GameController_t2782302542 * value)
	{
		___GameController_5 = value;
		Il2CppCodeGenWriteBarrier(&___GameController_5, value);
	}

	inline static int32_t get_offset_of_Message_6() { return static_cast<int32_t>(offsetof(AdsManager_t814343421, ___Message_6)); }
	inline Message_t2619578343 * get_Message_6() const { return ___Message_6; }
	inline Message_t2619578343 ** get_address_of_Message_6() { return &___Message_6; }
	inline void set_Message_6(Message_t2619578343 * value)
	{
		___Message_6 = value;
		Il2CppCodeGenWriteBarrier(&___Message_6, value);
	}

	inline static int32_t get_offset_of_lastInterstitialShowTime_7() { return static_cast<int32_t>(offsetof(AdsManager_t814343421, ___lastInterstitialShowTime_7)); }
	inline float get_lastInterstitialShowTime_7() const { return ___lastInterstitialShowTime_7; }
	inline float* get_address_of_lastInterstitialShowTime_7() { return &___lastInterstitialShowTime_7; }
	inline void set_lastInterstitialShowTime_7(float value)
	{
		___lastInterstitialShowTime_7 = value;
	}

	inline static int32_t get_offset_of__videoPlaying_8() { return static_cast<int32_t>(offsetof(AdsManager_t814343421, ____videoPlaying_8)); }
	inline bool get__videoPlaying_8() const { return ____videoPlaying_8; }
	inline bool* get_address_of__videoPlaying_8() { return &____videoPlaying_8; }
	inline void set__videoPlaying_8(bool value)
	{
		____videoPlaying_8 = value;
	}

	inline static int32_t get_offset_of_interval_9() { return static_cast<int32_t>(offsetof(AdsManager_t814343421, ___interval_9)); }
	inline float get_interval_9() const { return ___interval_9; }
	inline float* get_address_of_interval_9() { return &___interval_9; }
	inline void set_interval_9(float value)
	{
		___interval_9 = value;
	}

	inline static int32_t get_offset_of_TutorialPlayer_10() { return static_cast<int32_t>(offsetof(AdsManager_t814343421, ___TutorialPlayer_10)); }
	inline TutorialPlayer_t4281139199 * get_TutorialPlayer_10() const { return ___TutorialPlayer_10; }
	inline TutorialPlayer_t4281139199 ** get_address_of_TutorialPlayer_10() { return &___TutorialPlayer_10; }
	inline void set_TutorialPlayer_10(TutorialPlayer_t4281139199 * value)
	{
		___TutorialPlayer_10 = value;
		Il2CppCodeGenWriteBarrier(&___TutorialPlayer_10, value);
	}
};

struct AdsManager_t814343421_StaticFields
{
public:
	// AdsManager AdsManager::instance
	AdsManager_t814343421 * ___instance_2;
	// System.Single AdsManager::TimeWait
	float ___TimeWait_11;
	// System.Action`2<System.String,System.String> AdsManager::<>f__am$cacheA
	Action_2_t2887221574 * ___U3CU3Ef__amU24cacheA_12;
	// System.Action`2<System.String,System.String> AdsManager::<>f__am$cacheB
	Action_2_t2887221574 * ___U3CU3Ef__amU24cacheB_13;
	// System.Action`2<System.String,System.String> AdsManager::<>f__am$cacheC
	Action_2_t2887221574 * ___U3CU3Ef__amU24cacheC_14;
	// System.Action`2<System.String,System.String> AdsManager::<>f__am$cacheD
	Action_2_t2887221574 * ___U3CU3Ef__amU24cacheD_15;
	// System.Action`2<System.String,System.String> AdsManager::<>f__am$cacheE
	Action_2_t2887221574 * ___U3CU3Ef__amU24cacheE_16;
	// System.Action`3<System.String,System.String,System.String> AdsManager::<>f__am$cacheF
	Action_3_t2150340505 * ___U3CU3Ef__amU24cacheF_17;

public:
	inline static int32_t get_offset_of_instance_2() { return static_cast<int32_t>(offsetof(AdsManager_t814343421_StaticFields, ___instance_2)); }
	inline AdsManager_t814343421 * get_instance_2() const { return ___instance_2; }
	inline AdsManager_t814343421 ** get_address_of_instance_2() { return &___instance_2; }
	inline void set_instance_2(AdsManager_t814343421 * value)
	{
		___instance_2 = value;
		Il2CppCodeGenWriteBarrier(&___instance_2, value);
	}

	inline static int32_t get_offset_of_TimeWait_11() { return static_cast<int32_t>(offsetof(AdsManager_t814343421_StaticFields, ___TimeWait_11)); }
	inline float get_TimeWait_11() const { return ___TimeWait_11; }
	inline float* get_address_of_TimeWait_11() { return &___TimeWait_11; }
	inline void set_TimeWait_11(float value)
	{
		___TimeWait_11 = value;
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cacheA_12() { return static_cast<int32_t>(offsetof(AdsManager_t814343421_StaticFields, ___U3CU3Ef__amU24cacheA_12)); }
	inline Action_2_t2887221574 * get_U3CU3Ef__amU24cacheA_12() const { return ___U3CU3Ef__amU24cacheA_12; }
	inline Action_2_t2887221574 ** get_address_of_U3CU3Ef__amU24cacheA_12() { return &___U3CU3Ef__amU24cacheA_12; }
	inline void set_U3CU3Ef__amU24cacheA_12(Action_2_t2887221574 * value)
	{
		___U3CU3Ef__amU24cacheA_12 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cacheA_12, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cacheB_13() { return static_cast<int32_t>(offsetof(AdsManager_t814343421_StaticFields, ___U3CU3Ef__amU24cacheB_13)); }
	inline Action_2_t2887221574 * get_U3CU3Ef__amU24cacheB_13() const { return ___U3CU3Ef__amU24cacheB_13; }
	inline Action_2_t2887221574 ** get_address_of_U3CU3Ef__amU24cacheB_13() { return &___U3CU3Ef__amU24cacheB_13; }
	inline void set_U3CU3Ef__amU24cacheB_13(Action_2_t2887221574 * value)
	{
		___U3CU3Ef__amU24cacheB_13 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cacheB_13, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cacheC_14() { return static_cast<int32_t>(offsetof(AdsManager_t814343421_StaticFields, ___U3CU3Ef__amU24cacheC_14)); }
	inline Action_2_t2887221574 * get_U3CU3Ef__amU24cacheC_14() const { return ___U3CU3Ef__amU24cacheC_14; }
	inline Action_2_t2887221574 ** get_address_of_U3CU3Ef__amU24cacheC_14() { return &___U3CU3Ef__amU24cacheC_14; }
	inline void set_U3CU3Ef__amU24cacheC_14(Action_2_t2887221574 * value)
	{
		___U3CU3Ef__amU24cacheC_14 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cacheC_14, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cacheD_15() { return static_cast<int32_t>(offsetof(AdsManager_t814343421_StaticFields, ___U3CU3Ef__amU24cacheD_15)); }
	inline Action_2_t2887221574 * get_U3CU3Ef__amU24cacheD_15() const { return ___U3CU3Ef__amU24cacheD_15; }
	inline Action_2_t2887221574 ** get_address_of_U3CU3Ef__amU24cacheD_15() { return &___U3CU3Ef__amU24cacheD_15; }
	inline void set_U3CU3Ef__amU24cacheD_15(Action_2_t2887221574 * value)
	{
		___U3CU3Ef__amU24cacheD_15 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cacheD_15, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cacheE_16() { return static_cast<int32_t>(offsetof(AdsManager_t814343421_StaticFields, ___U3CU3Ef__amU24cacheE_16)); }
	inline Action_2_t2887221574 * get_U3CU3Ef__amU24cacheE_16() const { return ___U3CU3Ef__amU24cacheE_16; }
	inline Action_2_t2887221574 ** get_address_of_U3CU3Ef__amU24cacheE_16() { return &___U3CU3Ef__amU24cacheE_16; }
	inline void set_U3CU3Ef__amU24cacheE_16(Action_2_t2887221574 * value)
	{
		___U3CU3Ef__amU24cacheE_16 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cacheE_16, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cacheF_17() { return static_cast<int32_t>(offsetof(AdsManager_t814343421_StaticFields, ___U3CU3Ef__amU24cacheF_17)); }
	inline Action_3_t2150340505 * get_U3CU3Ef__amU24cacheF_17() const { return ___U3CU3Ef__amU24cacheF_17; }
	inline Action_3_t2150340505 ** get_address_of_U3CU3Ef__amU24cacheF_17() { return &___U3CU3Ef__amU24cacheF_17; }
	inline void set_U3CU3Ef__amU24cacheF_17(Action_3_t2150340505 * value)
	{
		___U3CU3Ef__amU24cacheF_17 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cacheF_17, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
