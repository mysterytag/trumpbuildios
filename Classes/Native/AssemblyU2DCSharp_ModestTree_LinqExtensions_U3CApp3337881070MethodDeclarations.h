﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ModestTree.LinqExtensions/<Append>c__Iterator36`1<System.Object>
struct U3CAppendU3Ec__Iterator36_1_t3337881070;
// System.Object
struct Il2CppObject;
// System.Collections.IEnumerator
struct IEnumerator_t287207039;
// System.Collections.Generic.IEnumerator`1<System.Object>
struct IEnumerator_1_t2320212868;

#include "codegen/il2cpp-codegen.h"

// System.Void ModestTree.LinqExtensions/<Append>c__Iterator36`1<System.Object>::.ctor()
extern "C"  void U3CAppendU3Ec__Iterator36_1__ctor_m4053237453_gshared (U3CAppendU3Ec__Iterator36_1_t3337881070 * __this, const MethodInfo* method);
#define U3CAppendU3Ec__Iterator36_1__ctor_m4053237453(__this, method) ((  void (*) (U3CAppendU3Ec__Iterator36_1_t3337881070 *, const MethodInfo*))U3CAppendU3Ec__Iterator36_1__ctor_m4053237453_gshared)(__this, method)
// T ModestTree.LinqExtensions/<Append>c__Iterator36`1<System.Object>::System.Collections.Generic.IEnumerator<T>.get_Current()
extern "C"  Il2CppObject * U3CAppendU3Ec__Iterator36_1_System_Collections_Generic_IEnumeratorU3CTU3E_get_Current_m2302975158_gshared (U3CAppendU3Ec__Iterator36_1_t3337881070 * __this, const MethodInfo* method);
#define U3CAppendU3Ec__Iterator36_1_System_Collections_Generic_IEnumeratorU3CTU3E_get_Current_m2302975158(__this, method) ((  Il2CppObject * (*) (U3CAppendU3Ec__Iterator36_1_t3337881070 *, const MethodInfo*))U3CAppendU3Ec__Iterator36_1_System_Collections_Generic_IEnumeratorU3CTU3E_get_Current_m2302975158_gshared)(__this, method)
// System.Object ModestTree.LinqExtensions/<Append>c__Iterator36`1<System.Object>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CAppendU3Ec__Iterator36_1_System_Collections_IEnumerator_get_Current_m491056835_gshared (U3CAppendU3Ec__Iterator36_1_t3337881070 * __this, const MethodInfo* method);
#define U3CAppendU3Ec__Iterator36_1_System_Collections_IEnumerator_get_Current_m491056835(__this, method) ((  Il2CppObject * (*) (U3CAppendU3Ec__Iterator36_1_t3337881070 *, const MethodInfo*))U3CAppendU3Ec__Iterator36_1_System_Collections_IEnumerator_get_Current_m491056835_gshared)(__this, method)
// System.Collections.IEnumerator ModestTree.LinqExtensions/<Append>c__Iterator36`1<System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * U3CAppendU3Ec__Iterator36_1_System_Collections_IEnumerable_GetEnumerator_m873747844_gshared (U3CAppendU3Ec__Iterator36_1_t3337881070 * __this, const MethodInfo* method);
#define U3CAppendU3Ec__Iterator36_1_System_Collections_IEnumerable_GetEnumerator_m873747844(__this, method) ((  Il2CppObject * (*) (U3CAppendU3Ec__Iterator36_1_t3337881070 *, const MethodInfo*))U3CAppendU3Ec__Iterator36_1_System_Collections_IEnumerable_GetEnumerator_m873747844_gshared)(__this, method)
// System.Collections.Generic.IEnumerator`1<T> ModestTree.LinqExtensions/<Append>c__Iterator36`1<System.Object>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
extern "C"  Il2CppObject* U3CAppendU3Ec__Iterator36_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m3911073503_gshared (U3CAppendU3Ec__Iterator36_1_t3337881070 * __this, const MethodInfo* method);
#define U3CAppendU3Ec__Iterator36_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m3911073503(__this, method) ((  Il2CppObject* (*) (U3CAppendU3Ec__Iterator36_1_t3337881070 *, const MethodInfo*))U3CAppendU3Ec__Iterator36_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m3911073503_gshared)(__this, method)
// System.Boolean ModestTree.LinqExtensions/<Append>c__Iterator36`1<System.Object>::MoveNext()
extern "C"  bool U3CAppendU3Ec__Iterator36_1_MoveNext_m1721468911_gshared (U3CAppendU3Ec__Iterator36_1_t3337881070 * __this, const MethodInfo* method);
#define U3CAppendU3Ec__Iterator36_1_MoveNext_m1721468911(__this, method) ((  bool (*) (U3CAppendU3Ec__Iterator36_1_t3337881070 *, const MethodInfo*))U3CAppendU3Ec__Iterator36_1_MoveNext_m1721468911_gshared)(__this, method)
// System.Void ModestTree.LinqExtensions/<Append>c__Iterator36`1<System.Object>::Dispose()
extern "C"  void U3CAppendU3Ec__Iterator36_1_Dispose_m3819105226_gshared (U3CAppendU3Ec__Iterator36_1_t3337881070 * __this, const MethodInfo* method);
#define U3CAppendU3Ec__Iterator36_1_Dispose_m3819105226(__this, method) ((  void (*) (U3CAppendU3Ec__Iterator36_1_t3337881070 *, const MethodInfo*))U3CAppendU3Ec__Iterator36_1_Dispose_m3819105226_gshared)(__this, method)
// System.Void ModestTree.LinqExtensions/<Append>c__Iterator36`1<System.Object>::Reset()
extern "C"  void U3CAppendU3Ec__Iterator36_1_Reset_m1699670394_gshared (U3CAppendU3Ec__Iterator36_1_t3337881070 * __this, const MethodInfo* method);
#define U3CAppendU3Ec__Iterator36_1_Reset_m1699670394(__this, method) ((  void (*) (U3CAppendU3Ec__Iterator36_1_t3337881070 *, const MethodInfo*))U3CAppendU3Ec__Iterator36_1_Reset_m1699670394_gshared)(__this, method)
