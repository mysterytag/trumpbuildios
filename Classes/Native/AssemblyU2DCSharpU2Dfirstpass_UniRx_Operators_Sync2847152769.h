﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UniRx.IObserver`1<System.Object>
struct IObserver_1_t3049105323;
// System.Object
struct Il2CppObject;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Operators.SynchronizedObserver`1<System.Object>
struct  SynchronizedObserver_1_t2847152769  : public Il2CppObject
{
public:
	// UniRx.IObserver`1<T> UniRx.Operators.SynchronizedObserver`1::observer
	Il2CppObject* ___observer_0;
	// System.Object UniRx.Operators.SynchronizedObserver`1::gate
	Il2CppObject * ___gate_1;

public:
	inline static int32_t get_offset_of_observer_0() { return static_cast<int32_t>(offsetof(SynchronizedObserver_1_t2847152769, ___observer_0)); }
	inline Il2CppObject* get_observer_0() const { return ___observer_0; }
	inline Il2CppObject** get_address_of_observer_0() { return &___observer_0; }
	inline void set_observer_0(Il2CppObject* value)
	{
		___observer_0 = value;
		Il2CppCodeGenWriteBarrier(&___observer_0, value);
	}

	inline static int32_t get_offset_of_gate_1() { return static_cast<int32_t>(offsetof(SynchronizedObserver_1_t2847152769, ___gate_1)); }
	inline Il2CppObject * get_gate_1() const { return ___gate_1; }
	inline Il2CppObject ** get_address_of_gate_1() { return &___gate_1; }
	inline void set_gate_1(Il2CppObject * value)
	{
		___gate_1 = value;
		Il2CppCodeGenWriteBarrier(&___gate_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
