﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.Events.InvokableCall`2<System.Int32,System.Boolean>
struct InvokableCall_2_t1033537386;
// System.Object
struct Il2CppObject;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// UnityEngine.Events.UnityAction`2<System.Int32,System.Boolean>
struct UnityAction_2_t3248774556;
// System.Object[]
struct ObjectU5BU5D_t11523773;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_Reflection_MethodInfo3461221277.h"

// System.Void UnityEngine.Events.InvokableCall`2<System.Int32,System.Boolean>::.ctor(System.Object,System.Reflection.MethodInfo)
extern "C"  void InvokableCall_2__ctor_m2332408294_gshared (InvokableCall_2_t1033537386 * __this, Il2CppObject * ___target0, MethodInfo_t * ___theFunction1, const MethodInfo* method);
#define InvokableCall_2__ctor_m2332408294(__this, ___target0, ___theFunction1, method) ((  void (*) (InvokableCall_2_t1033537386 *, Il2CppObject *, MethodInfo_t *, const MethodInfo*))InvokableCall_2__ctor_m2332408294_gshared)(__this, ___target0, ___theFunction1, method)
// System.Void UnityEngine.Events.InvokableCall`2<System.Int32,System.Boolean>::.ctor(UnityEngine.Events.UnityAction`2<T1,T2>)
extern "C"  void InvokableCall_2__ctor_m3160317949_gshared (InvokableCall_2_t1033537386 * __this, UnityAction_2_t3248774556 * ___action0, const MethodInfo* method);
#define InvokableCall_2__ctor_m3160317949(__this, ___action0, method) ((  void (*) (InvokableCall_2_t1033537386 *, UnityAction_2_t3248774556 *, const MethodInfo*))InvokableCall_2__ctor_m3160317949_gshared)(__this, ___action0, method)
// System.Void UnityEngine.Events.InvokableCall`2<System.Int32,System.Boolean>::Invoke(System.Object[])
extern "C"  void InvokableCall_2_Invoke_m607953405_gshared (InvokableCall_2_t1033537386 * __this, ObjectU5BU5D_t11523773* ___args0, const MethodInfo* method);
#define InvokableCall_2_Invoke_m607953405(__this, ___args0, method) ((  void (*) (InvokableCall_2_t1033537386 *, ObjectU5BU5D_t11523773*, const MethodInfo*))InvokableCall_2_Invoke_m607953405_gshared)(__this, ___args0, method)
// System.Boolean UnityEngine.Events.InvokableCall`2<System.Int32,System.Boolean>::Find(System.Object,System.Reflection.MethodInfo)
extern "C"  bool InvokableCall_2_Find_m2745761615_gshared (InvokableCall_2_t1033537386 * __this, Il2CppObject * ___targetObj0, MethodInfo_t * ___method1, const MethodInfo* method);
#define InvokableCall_2_Find_m2745761615(__this, ___targetObj0, ___method1, method) ((  bool (*) (InvokableCall_2_t1033537386 *, Il2CppObject *, MethodInfo_t *, const MethodInfo*))InvokableCall_2_Find_m2745761615_gshared)(__this, ___targetObj0, ___method1, method)
