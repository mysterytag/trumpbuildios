﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.CatchObservable`2/Catch<System.Object,System.Object>
struct Catch_t841129384;
// UniRx.Operators.CatchObservable`2<System.Object,System.Object>
struct CatchObservable_2_t3974704686;
// UniRx.IObserver`1<System.Object>
struct IObserver_1_t3049105323;
// System.IDisposable
struct IDisposable_t1628921374;
// System.Object
struct Il2CppObject;
// System.Exception
struct Exception_t1967233988;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_Exception1967233988.h"

// System.Void UniRx.Operators.CatchObservable`2/Catch<System.Object,System.Object>::.ctor(UniRx.Operators.CatchObservable`2<T,TException>,UniRx.IObserver`1<T>,System.IDisposable)
extern "C"  void Catch__ctor_m3596504964_gshared (Catch_t841129384 * __this, CatchObservable_2_t3974704686 * ___parent0, Il2CppObject* ___observer1, Il2CppObject * ___cancel2, const MethodInfo* method);
#define Catch__ctor_m3596504964(__this, ___parent0, ___observer1, ___cancel2, method) ((  void (*) (Catch_t841129384 *, CatchObservable_2_t3974704686 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))Catch__ctor_m3596504964_gshared)(__this, ___parent0, ___observer1, ___cancel2, method)
// System.IDisposable UniRx.Operators.CatchObservable`2/Catch<System.Object,System.Object>::Run()
extern "C"  Il2CppObject * Catch_Run_m3280276056_gshared (Catch_t841129384 * __this, const MethodInfo* method);
#define Catch_Run_m3280276056(__this, method) ((  Il2CppObject * (*) (Catch_t841129384 *, const MethodInfo*))Catch_Run_m3280276056_gshared)(__this, method)
// System.Void UniRx.Operators.CatchObservable`2/Catch<System.Object,System.Object>::OnNext(T)
extern "C"  void Catch_OnNext_m3651359218_gshared (Catch_t841129384 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Catch_OnNext_m3651359218(__this, ___value0, method) ((  void (*) (Catch_t841129384 *, Il2CppObject *, const MethodInfo*))Catch_OnNext_m3651359218_gshared)(__this, ___value0, method)
// System.Void UniRx.Operators.CatchObservable`2/Catch<System.Object,System.Object>::OnError(System.Exception)
extern "C"  void Catch_OnError_m515459329_gshared (Catch_t841129384 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method);
#define Catch_OnError_m515459329(__this, ___error0, method) ((  void (*) (Catch_t841129384 *, Exception_t1967233988 *, const MethodInfo*))Catch_OnError_m515459329_gshared)(__this, ___error0, method)
// System.Void UniRx.Operators.CatchObservable`2/Catch<System.Object,System.Object>::OnCompleted()
extern "C"  void Catch_OnCompleted_m670348756_gshared (Catch_t841129384 * __this, const MethodInfo* method);
#define Catch_OnCompleted_m670348756(__this, method) ((  void (*) (Catch_t841129384 *, const MethodInfo*))Catch_OnCompleted_m670348756_gshared)(__this, method)
