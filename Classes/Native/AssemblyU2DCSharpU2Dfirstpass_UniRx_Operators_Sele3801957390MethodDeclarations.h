﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.SelectObservable`2/Select<System.Int64,System.Boolean>
struct Select_t3801957390;
// UniRx.Operators.SelectObservable`2<System.Int64,System.Boolean>
struct SelectObservable_2_t691402772;
// UniRx.IObserver`1<System.Boolean>
struct IObserver_1_t2423004244;
// System.IDisposable
struct IDisposable_t1628921374;
// System.Exception
struct Exception_t1967233988;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Exception1967233988.h"

// System.Void UniRx.Operators.SelectObservable`2/Select<System.Int64,System.Boolean>::.ctor(UniRx.Operators.SelectObservable`2<T,TR>,UniRx.IObserver`1<TR>,System.IDisposable)
extern "C"  void Select__ctor_m1354957729_gshared (Select_t3801957390 * __this, SelectObservable_2_t691402772 * ___parent0, Il2CppObject* ___observer1, Il2CppObject * ___cancel2, const MethodInfo* method);
#define Select__ctor_m1354957729(__this, ___parent0, ___observer1, ___cancel2, method) ((  void (*) (Select_t3801957390 *, SelectObservable_2_t691402772 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))Select__ctor_m1354957729_gshared)(__this, ___parent0, ___observer1, ___cancel2, method)
// System.Void UniRx.Operators.SelectObservable`2/Select<System.Int64,System.Boolean>::OnNext(T)
extern "C"  void Select_OnNext_m2510528263_gshared (Select_t3801957390 * __this, int64_t ___value0, const MethodInfo* method);
#define Select_OnNext_m2510528263(__this, ___value0, method) ((  void (*) (Select_t3801957390 *, int64_t, const MethodInfo*))Select_OnNext_m2510528263_gshared)(__this, ___value0, method)
// System.Void UniRx.Operators.SelectObservable`2/Select<System.Int64,System.Boolean>::OnError(System.Exception)
extern "C"  void Select_OnError_m2423180310_gshared (Select_t3801957390 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method);
#define Select_OnError_m2423180310(__this, ___error0, method) ((  void (*) (Select_t3801957390 *, Exception_t1967233988 *, const MethodInfo*))Select_OnError_m2423180310_gshared)(__this, ___error0, method)
// System.Void UniRx.Operators.SelectObservable`2/Select<System.Int64,System.Boolean>::OnCompleted()
extern "C"  void Select_OnCompleted_m573468777_gshared (Select_t3801957390 * __this, const MethodInfo* method);
#define Select_OnCompleted_m573468777(__this, method) ((  void (*) (Select_t3801957390 *, const MethodInfo*))Select_OnCompleted_m573468777_gshared)(__this, method)
