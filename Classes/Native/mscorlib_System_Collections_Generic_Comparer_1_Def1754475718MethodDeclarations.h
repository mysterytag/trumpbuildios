﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Comparer`1/DefaultComparer<UniRx.CollectionAddEvent`1<System.Object>>
struct DefaultComparer_t1754475719;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_CollectionAddE2416521987.h"

// System.Void System.Collections.Generic.Comparer`1/DefaultComparer<UniRx.CollectionAddEvent`1<System.Object>>::.ctor()
extern "C"  void DefaultComparer__ctor_m1073959840_gshared (DefaultComparer_t1754475719 * __this, const MethodInfo* method);
#define DefaultComparer__ctor_m1073959840(__this, method) ((  void (*) (DefaultComparer_t1754475719 *, const MethodInfo*))DefaultComparer__ctor_m1073959840_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Comparer`1/DefaultComparer<UniRx.CollectionAddEvent`1<System.Object>>::Compare(T,T)
extern "C"  int32_t DefaultComparer_Compare_m1770297999_gshared (DefaultComparer_t1754475719 * __this, CollectionAddEvent_1_t2416521987  ___x0, CollectionAddEvent_1_t2416521987  ___y1, const MethodInfo* method);
#define DefaultComparer_Compare_m1770297999(__this, ___x0, ___y1, method) ((  int32_t (*) (DefaultComparer_t1754475719 *, CollectionAddEvent_1_t2416521987 , CollectionAddEvent_1_t2416521987 , const MethodInfo*))DefaultComparer_Compare_m1770297999_gshared)(__this, ___x0, ___y1, method)
