﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.ThrottleFirstObservable`1/ThrottleFirst<System.Object>
struct ThrottleFirst_t4236367119;
// UniRx.Operators.ThrottleFirstObservable`1<System.Object>
struct ThrottleFirstObservable_1_t2123238056;
// UniRx.IObserver`1<System.Object>
struct IObserver_1_t3049105323;
// System.IDisposable
struct IDisposable_t1628921374;
// System.Object
struct Il2CppObject;
// System.Exception
struct Exception_t1967233988;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_Exception1967233988.h"

// System.Void UniRx.Operators.ThrottleFirstObservable`1/ThrottleFirst<System.Object>::.ctor(UniRx.Operators.ThrottleFirstObservable`1<T>,UniRx.IObserver`1<T>,System.IDisposable)
extern "C"  void ThrottleFirst__ctor_m818225262_gshared (ThrottleFirst_t4236367119 * __this, ThrottleFirstObservable_1_t2123238056 * ___parent0, Il2CppObject* ___observer1, Il2CppObject * ___cancel2, const MethodInfo* method);
#define ThrottleFirst__ctor_m818225262(__this, ___parent0, ___observer1, ___cancel2, method) ((  void (*) (ThrottleFirst_t4236367119 *, ThrottleFirstObservable_1_t2123238056 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))ThrottleFirst__ctor_m818225262_gshared)(__this, ___parent0, ___observer1, ___cancel2, method)
// System.IDisposable UniRx.Operators.ThrottleFirstObservable`1/ThrottleFirst<System.Object>::Run()
extern "C"  Il2CppObject * ThrottleFirst_Run_m1603010619_gshared (ThrottleFirst_t4236367119 * __this, const MethodInfo* method);
#define ThrottleFirst_Run_m1603010619(__this, method) ((  Il2CppObject * (*) (ThrottleFirst_t4236367119 *, const MethodInfo*))ThrottleFirst_Run_m1603010619_gshared)(__this, method)
// System.Void UniRx.Operators.ThrottleFirstObservable`1/ThrottleFirst<System.Object>::OnNext()
extern "C"  void ThrottleFirst_OnNext_m3545300713_gshared (ThrottleFirst_t4236367119 * __this, const MethodInfo* method);
#define ThrottleFirst_OnNext_m3545300713(__this, method) ((  void (*) (ThrottleFirst_t4236367119 *, const MethodInfo*))ThrottleFirst_OnNext_m3545300713_gshared)(__this, method)
// System.Void UniRx.Operators.ThrottleFirstObservable`1/ThrottleFirst<System.Object>::OnNext(T)
extern "C"  void ThrottleFirst_OnNext_m2530141077_gshared (ThrottleFirst_t4236367119 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define ThrottleFirst_OnNext_m2530141077(__this, ___value0, method) ((  void (*) (ThrottleFirst_t4236367119 *, Il2CppObject *, const MethodInfo*))ThrottleFirst_OnNext_m2530141077_gshared)(__this, ___value0, method)
// System.Void UniRx.Operators.ThrottleFirstObservable`1/ThrottleFirst<System.Object>::OnError(System.Exception)
extern "C"  void ThrottleFirst_OnError_m3100325028_gshared (ThrottleFirst_t4236367119 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method);
#define ThrottleFirst_OnError_m3100325028(__this, ___error0, method) ((  void (*) (ThrottleFirst_t4236367119 *, Exception_t1967233988 *, const MethodInfo*))ThrottleFirst_OnError_m3100325028_gshared)(__this, ___error0, method)
// System.Void UniRx.Operators.ThrottleFirstObservable`1/ThrottleFirst<System.Object>::OnCompleted()
extern "C"  void ThrottleFirst_OnCompleted_m1541979639_gshared (ThrottleFirst_t4236367119 * __this, const MethodInfo* method);
#define ThrottleFirst_OnCompleted_m1541979639(__this, method) ((  void (*) (ThrottleFirst_t4236367119 *, const MethodInfo*))ThrottleFirst_OnCompleted_m1541979639_gshared)(__this, method)
