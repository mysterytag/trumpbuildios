﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UniRx.ReactiveProperty`1<UnityEngine.Bounds>
struct ReactiveProperty_1_t4126848016;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.ReactiveProperty`1/ReactivePropertyObserver<UnityEngine.Bounds>
struct  ReactivePropertyObserver_t3754497547  : public Il2CppObject
{
public:
	// UniRx.ReactiveProperty`1<T> UniRx.ReactiveProperty`1/ReactivePropertyObserver::parent
	ReactiveProperty_1_t4126848016 * ___parent_0;
	// System.Int32 UniRx.ReactiveProperty`1/ReactivePropertyObserver::isStopped
	int32_t ___isStopped_1;

public:
	inline static int32_t get_offset_of_parent_0() { return static_cast<int32_t>(offsetof(ReactivePropertyObserver_t3754497547, ___parent_0)); }
	inline ReactiveProperty_1_t4126848016 * get_parent_0() const { return ___parent_0; }
	inline ReactiveProperty_1_t4126848016 ** get_address_of_parent_0() { return &___parent_0; }
	inline void set_parent_0(ReactiveProperty_1_t4126848016 * value)
	{
		___parent_0 = value;
		Il2CppCodeGenWriteBarrier(&___parent_0, value);
	}

	inline static int32_t get_offset_of_isStopped_1() { return static_cast<int32_t>(offsetof(ReactivePropertyObserver_t3754497547, ___isStopped_1)); }
	inline int32_t get_isStopped_1() const { return ___isStopped_1; }
	inline int32_t* get_address_of_isStopped_1() { return &___isStopped_1; }
	inline void set_isStopped_1(int32_t value)
	{
		___isStopped_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
