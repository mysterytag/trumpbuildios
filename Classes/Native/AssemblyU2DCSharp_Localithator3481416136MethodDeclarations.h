﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Localithator
struct Localithator_t3481416136;

#include "codegen/il2cpp-codegen.h"

// System.Void Localithator::.ctor()
extern "C"  void Localithator__ctor_m4069343699 (Localithator_t3481416136 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Localithator::Start()
extern "C"  void Localithator_Start_m3016481491 (Localithator_t3481416136 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Localithator::Update()
extern "C"  void Localithator_Update_m3322465178 (Localithator_t3481416136 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
