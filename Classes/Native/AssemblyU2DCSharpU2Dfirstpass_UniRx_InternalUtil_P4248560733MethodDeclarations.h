﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_P4248560733.h"

// System.Int32 UniRx.InternalUtil.PriorityQueue`1/IndexedItem<System.Object>::CompareTo(UniRx.InternalUtil.PriorityQueue`1/IndexedItem<T>)
extern "C"  int32_t IndexedItem_CompareTo_m3674265725_gshared (IndexedItem_t4248560733 * __this, IndexedItem_t4248560733  ___other0, const MethodInfo* method);
#define IndexedItem_CompareTo_m3674265725(__this, ___other0, method) ((  int32_t (*) (IndexedItem_t4248560733 *, IndexedItem_t4248560733 , const MethodInfo*))IndexedItem_CompareTo_m3674265725_gshared)(__this, ___other0, method)
