﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Xml.XPath.XPathFunctionStartsWith
struct XPathFunctionStartsWith_t2744556329;
// System.Xml.XPath.FunctionArguments
struct FunctionArguments_t3402201403;
// System.Object
struct Il2CppObject;
// System.Xml.XPath.BaseIterator
struct BaseIterator_t3696600956;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "System_Xml_System_Xml_XPath_FunctionArguments3402201403.h"
#include "System_Xml_System_Xml_XPath_XPathResultType370286193.h"
#include "System_Xml_System_Xml_XPath_BaseIterator3696600956.h"

// System.Void System.Xml.XPath.XPathFunctionStartsWith::.ctor(System.Xml.XPath.FunctionArguments)
extern "C"  void XPathFunctionStartsWith__ctor_m3911729737 (XPathFunctionStartsWith_t2744556329 * __this, FunctionArguments_t3402201403 * ___args0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Xml.XPath.XPathResultType System.Xml.XPath.XPathFunctionStartsWith::get_ReturnType()
extern "C"  int32_t XPathFunctionStartsWith_get_ReturnType_m116298299 (XPathFunctionStartsWith_t2744556329 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Xml.XPath.XPathFunctionStartsWith::get_Peer()
extern "C"  bool XPathFunctionStartsWith_get_Peer_m267456887 (XPathFunctionStartsWith_t2744556329 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Xml.XPath.XPathFunctionStartsWith::Evaluate(System.Xml.XPath.BaseIterator)
extern "C"  Il2CppObject * XPathFunctionStartsWith_Evaluate_m846929178 (XPathFunctionStartsWith_t2744556329 * __this, BaseIterator_t3696600956 * ___iter0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Xml.XPath.XPathFunctionStartsWith::ToString()
extern "C"  String_t* XPathFunctionStartsWith_ToString_m1645832723 (XPathFunctionStartsWith_t2744556329 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
