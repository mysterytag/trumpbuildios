﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.OperatorObserverBase`2<System.Int64,System.Int64>
struct OperatorObserverBase_2_t3939730909;
// UniRx.IObserver`1<System.Int64>
struct IObserver_1_t764446489;
// System.IDisposable
struct IDisposable_t1628921374;

#include "codegen/il2cpp-codegen.h"

// System.Void UniRx.Operators.OperatorObserverBase`2<System.Int64,System.Int64>::.ctor(UniRx.IObserver`1<TResult>,System.IDisposable)
extern "C"  void OperatorObserverBase_2__ctor_m3985458928_gshared (OperatorObserverBase_2_t3939730909 * __this, Il2CppObject* ___observer0, Il2CppObject * ___cancel1, const MethodInfo* method);
#define OperatorObserverBase_2__ctor_m3985458928(__this, ___observer0, ___cancel1, method) ((  void (*) (OperatorObserverBase_2_t3939730909 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))OperatorObserverBase_2__ctor_m3985458928_gshared)(__this, ___observer0, ___cancel1, method)
// System.Void UniRx.Operators.OperatorObserverBase`2<System.Int64,System.Int64>::Dispose()
extern "C"  void OperatorObserverBase_2_Dispose_m4056721658_gshared (OperatorObserverBase_2_t3939730909 * __this, const MethodInfo* method);
#define OperatorObserverBase_2_Dispose_m4056721658(__this, method) ((  void (*) (OperatorObserverBase_2_t3939730909 *, const MethodInfo*))OperatorObserverBase_2_Dispose_m4056721658_gshared)(__this, method)
