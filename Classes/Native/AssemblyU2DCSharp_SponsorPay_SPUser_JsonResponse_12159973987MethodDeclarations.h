﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SponsorPay.SPUser/JsonResponse`1<System.Object>
struct JsonResponse_1_t2159973987;
// System.String
struct String_t;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String968488902.h"
#include "mscorlib_System_Object837106420.h"

// System.Void SponsorPay.SPUser/JsonResponse`1<System.Object>::.ctor()
extern "C"  void JsonResponse_1__ctor_m4253934096_gshared (JsonResponse_1_t2159973987 * __this, const MethodInfo* method);
#define JsonResponse_1__ctor_m4253934096(__this, method) ((  void (*) (JsonResponse_1_t2159973987 *, const MethodInfo*))JsonResponse_1__ctor_m4253934096_gshared)(__this, method)
// System.String SponsorPay.SPUser/JsonResponse`1<System.Object>::get_key()
extern "C"  String_t* JsonResponse_1_get_key_m910009575_gshared (JsonResponse_1_t2159973987 * __this, const MethodInfo* method);
#define JsonResponse_1_get_key_m910009575(__this, method) ((  String_t* (*) (JsonResponse_1_t2159973987 *, const MethodInfo*))JsonResponse_1_get_key_m910009575_gshared)(__this, method)
// System.Void SponsorPay.SPUser/JsonResponse`1<System.Object>::set_key(System.String)
extern "C"  void JsonResponse_1_set_key_m1504209010_gshared (JsonResponse_1_t2159973987 * __this, String_t* ___value0, const MethodInfo* method);
#define JsonResponse_1_set_key_m1504209010(__this, ___value0, method) ((  void (*) (JsonResponse_1_t2159973987 *, String_t*, const MethodInfo*))JsonResponse_1_set_key_m1504209010_gshared)(__this, ___value0, method)
// T SponsorPay.SPUser/JsonResponse`1<System.Object>::get_value()
extern "C"  Il2CppObject * JsonResponse_1_get_value_m804226325_gshared (JsonResponse_1_t2159973987 * __this, const MethodInfo* method);
#define JsonResponse_1_get_value_m804226325(__this, method) ((  Il2CppObject * (*) (JsonResponse_1_t2159973987 *, const MethodInfo*))JsonResponse_1_get_value_m804226325_gshared)(__this, method)
// System.Void SponsorPay.SPUser/JsonResponse`1<System.Object>::set_value(T)
extern "C"  void JsonResponse_1_set_value_m222088828_gshared (JsonResponse_1_t2159973987 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define JsonResponse_1_set_value_m222088828(__this, ___value0, method) ((  void (*) (JsonResponse_1_t2159973987 *, Il2CppObject *, const MethodInfo*))JsonResponse_1_set_value_m222088828_gshared)(__this, ___value0, method)
// System.String SponsorPay.SPUser/JsonResponse`1<System.Object>::get_error()
extern "C"  String_t* JsonResponse_1_get_error_m1978686608_gshared (JsonResponse_1_t2159973987 * __this, const MethodInfo* method);
#define JsonResponse_1_get_error_m1978686608(__this, method) ((  String_t* (*) (JsonResponse_1_t2159973987 *, const MethodInfo*))JsonResponse_1_get_error_m1978686608_gshared)(__this, method)
// System.Void SponsorPay.SPUser/JsonResponse`1<System.Object>::set_error(System.String)
extern "C"  void JsonResponse_1_set_error_m796740009_gshared (JsonResponse_1_t2159973987 * __this, String_t* ___value0, const MethodInfo* method);
#define JsonResponse_1_set_error_m796740009(__this, ___value0, method) ((  void (*) (JsonResponse_1_t2159973987 *, String_t*, const MethodInfo*))JsonResponse_1_set_error_m796740009_gshared)(__this, ___value0, method)
