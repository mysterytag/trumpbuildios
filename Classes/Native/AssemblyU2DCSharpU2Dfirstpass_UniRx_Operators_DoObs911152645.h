﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UniRx.IObservable`1<System.Object>
struct IObservable_1_t595904784;
// System.Action`1<System.Object>
struct Action_1_t985559125;
// System.Action`1<System.Exception>
struct Action_1_t2115686693;
// System.Action
struct Action_t437523947;

#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Oper4196218687.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Operators.DoObservable`1<System.Object>
struct  DoObservable_1_t911152645  : public OperatorObservableBase_1_t4196218687
{
public:
	// UniRx.IObservable`1<T> UniRx.Operators.DoObservable`1::source
	Il2CppObject* ___source_1;
	// System.Action`1<T> UniRx.Operators.DoObservable`1::onNext
	Action_1_t985559125 * ___onNext_2;
	// System.Action`1<System.Exception> UniRx.Operators.DoObservable`1::onError
	Action_1_t2115686693 * ___onError_3;
	// System.Action UniRx.Operators.DoObservable`1::onCompleted
	Action_t437523947 * ___onCompleted_4;

public:
	inline static int32_t get_offset_of_source_1() { return static_cast<int32_t>(offsetof(DoObservable_1_t911152645, ___source_1)); }
	inline Il2CppObject* get_source_1() const { return ___source_1; }
	inline Il2CppObject** get_address_of_source_1() { return &___source_1; }
	inline void set_source_1(Il2CppObject* value)
	{
		___source_1 = value;
		Il2CppCodeGenWriteBarrier(&___source_1, value);
	}

	inline static int32_t get_offset_of_onNext_2() { return static_cast<int32_t>(offsetof(DoObservable_1_t911152645, ___onNext_2)); }
	inline Action_1_t985559125 * get_onNext_2() const { return ___onNext_2; }
	inline Action_1_t985559125 ** get_address_of_onNext_2() { return &___onNext_2; }
	inline void set_onNext_2(Action_1_t985559125 * value)
	{
		___onNext_2 = value;
		Il2CppCodeGenWriteBarrier(&___onNext_2, value);
	}

	inline static int32_t get_offset_of_onError_3() { return static_cast<int32_t>(offsetof(DoObservable_1_t911152645, ___onError_3)); }
	inline Action_1_t2115686693 * get_onError_3() const { return ___onError_3; }
	inline Action_1_t2115686693 ** get_address_of_onError_3() { return &___onError_3; }
	inline void set_onError_3(Action_1_t2115686693 * value)
	{
		___onError_3 = value;
		Il2CppCodeGenWriteBarrier(&___onError_3, value);
	}

	inline static int32_t get_offset_of_onCompleted_4() { return static_cast<int32_t>(offsetof(DoObservable_1_t911152645, ___onCompleted_4)); }
	inline Action_t437523947 * get_onCompleted_4() const { return ___onCompleted_4; }
	inline Action_t437523947 ** get_address_of_onCompleted_4() { return &___onCompleted_4; }
	inline void set_onCompleted_4(Action_t437523947 * value)
	{
		___onCompleted_4 = value;
		Il2CppCodeGenWriteBarrier(&___onCompleted_4, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
