﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.ReactiveProperty`1/ReactivePropertyObserver<UnityEngine.Vector2>
struct ReactivePropertyObserver_t3761312357;
// UniRx.ReactiveProperty`1<UnityEngine.Vector2>
struct ReactiveProperty_1_t4133662826;
// System.Exception
struct Exception_t1967233988;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Vector23525329788.h"
#include "mscorlib_System_Exception1967233988.h"

// System.Void UniRx.ReactiveProperty`1/ReactivePropertyObserver<UnityEngine.Vector2>::.ctor(UniRx.ReactiveProperty`1<T>)
extern "C"  void ReactivePropertyObserver__ctor_m2834758586_gshared (ReactivePropertyObserver_t3761312357 * __this, ReactiveProperty_1_t4133662826 * ___parent0, const MethodInfo* method);
#define ReactivePropertyObserver__ctor_m2834758586(__this, ___parent0, method) ((  void (*) (ReactivePropertyObserver_t3761312357 *, ReactiveProperty_1_t4133662826 *, const MethodInfo*))ReactivePropertyObserver__ctor_m2834758586_gshared)(__this, ___parent0, method)
// System.Void UniRx.ReactiveProperty`1/ReactivePropertyObserver<UnityEngine.Vector2>::OnNext(T)
extern "C"  void ReactivePropertyObserver_OnNext_m359030961_gshared (ReactivePropertyObserver_t3761312357 * __this, Vector2_t3525329788  ___value0, const MethodInfo* method);
#define ReactivePropertyObserver_OnNext_m359030961(__this, ___value0, method) ((  void (*) (ReactivePropertyObserver_t3761312357 *, Vector2_t3525329788 , const MethodInfo*))ReactivePropertyObserver_OnNext_m359030961_gshared)(__this, ___value0, method)
// System.Void UniRx.ReactiveProperty`1/ReactivePropertyObserver<UnityEngine.Vector2>::OnError(System.Exception)
extern "C"  void ReactivePropertyObserver_OnError_m3982293440_gshared (ReactivePropertyObserver_t3761312357 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method);
#define ReactivePropertyObserver_OnError_m3982293440(__this, ___error0, method) ((  void (*) (ReactivePropertyObserver_t3761312357 *, Exception_t1967233988 *, const MethodInfo*))ReactivePropertyObserver_OnError_m3982293440_gshared)(__this, ___error0, method)
// System.Void UniRx.ReactiveProperty`1/ReactivePropertyObserver<UnityEngine.Vector2>::OnCompleted()
extern "C"  void ReactivePropertyObserver_OnCompleted_m2583973139_gshared (ReactivePropertyObserver_t3761312357 * __this, const MethodInfo* method);
#define ReactivePropertyObserver_OnCompleted_m2583973139(__this, method) ((  void (*) (ReactivePropertyObserver_t3761312357 *, const MethodInfo*))ReactivePropertyObserver_OnCompleted_m2583973139_gshared)(__this, method)
