﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.TimerObservable
struct TimerObservable_t1697791765;
// UniRx.IScheduler
struct IScheduler_t2938318244;
// System.IDisposable
struct IDisposable_t1628921374;
// UniRx.IObserver`1<System.Int64>
struct IObserver_1_t764446489;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_DateTimeOffset3712260035.h"
#include "mscorlib_System_Nullable_1_gen3649900800.h"
#include "mscorlib_System_TimeSpan763862892.h"

// System.Void UniRx.Operators.TimerObservable::.ctor(System.DateTimeOffset,System.Nullable`1<System.TimeSpan>,UniRx.IScheduler)
extern "C"  void TimerObservable__ctor_m2474548840 (TimerObservable_t1697791765 * __this, DateTimeOffset_t3712260035  ___dueTime0, Nullable_1_t3649900800  ___period1, Il2CppObject * ___scheduler2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.Operators.TimerObservable::.ctor(System.TimeSpan,System.Nullable`1<System.TimeSpan>,UniRx.IScheduler)
extern "C"  void TimerObservable__ctor_m1563113823 (TimerObservable_t1697791765 * __this, TimeSpan_t763862892  ___dueTime0, Nullable_1_t3649900800  ___period1, Il2CppObject * ___scheduler2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IDisposable UniRx.Operators.TimerObservable::SubscribeCore(UniRx.IObserver`1<System.Int64>,System.IDisposable)
extern "C"  Il2CppObject * TimerObservable_SubscribeCore_m32394110 (TimerObservable_t1697791765 * __this, Il2CppObject* ___observer0, Il2CppObject * ___cancel1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
