﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Zenject.TickableManager
struct TickableManager_t278367883;
// Zenject.InitializableManager
struct InitializableManager_t1859035635;
// Zenject.DisposableManager
struct DisposableManager_t2094948738;
// System.Collections.Generic.List`1<System.Object>
struct List_1_t1634065389;

#include "UnityEngine_UnityEngine_MonoBehaviour3012272455.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.UnityDependencyRoot
struct  UnityDependencyRoot_t1917880567  : public MonoBehaviour_t3012272455
{
public:
	// Zenject.TickableManager Zenject.UnityDependencyRoot::_tickableManager
	TickableManager_t278367883 * ____tickableManager_2;
	// Zenject.InitializableManager Zenject.UnityDependencyRoot::_initializableManager
	InitializableManager_t1859035635 * ____initializableManager_3;
	// Zenject.DisposableManager Zenject.UnityDependencyRoot::_disposablesManager
	DisposableManager_t2094948738 * ____disposablesManager_4;
	// System.Boolean Zenject.UnityDependencyRoot::_disposed
	bool ____disposed_5;
	// System.Collections.Generic.List`1<System.Object> Zenject.UnityDependencyRoot::_initialObjects
	List_1_t1634065389 * ____initialObjects_6;

public:
	inline static int32_t get_offset_of__tickableManager_2() { return static_cast<int32_t>(offsetof(UnityDependencyRoot_t1917880567, ____tickableManager_2)); }
	inline TickableManager_t278367883 * get__tickableManager_2() const { return ____tickableManager_2; }
	inline TickableManager_t278367883 ** get_address_of__tickableManager_2() { return &____tickableManager_2; }
	inline void set__tickableManager_2(TickableManager_t278367883 * value)
	{
		____tickableManager_2 = value;
		Il2CppCodeGenWriteBarrier(&____tickableManager_2, value);
	}

	inline static int32_t get_offset_of__initializableManager_3() { return static_cast<int32_t>(offsetof(UnityDependencyRoot_t1917880567, ____initializableManager_3)); }
	inline InitializableManager_t1859035635 * get__initializableManager_3() const { return ____initializableManager_3; }
	inline InitializableManager_t1859035635 ** get_address_of__initializableManager_3() { return &____initializableManager_3; }
	inline void set__initializableManager_3(InitializableManager_t1859035635 * value)
	{
		____initializableManager_3 = value;
		Il2CppCodeGenWriteBarrier(&____initializableManager_3, value);
	}

	inline static int32_t get_offset_of__disposablesManager_4() { return static_cast<int32_t>(offsetof(UnityDependencyRoot_t1917880567, ____disposablesManager_4)); }
	inline DisposableManager_t2094948738 * get__disposablesManager_4() const { return ____disposablesManager_4; }
	inline DisposableManager_t2094948738 ** get_address_of__disposablesManager_4() { return &____disposablesManager_4; }
	inline void set__disposablesManager_4(DisposableManager_t2094948738 * value)
	{
		____disposablesManager_4 = value;
		Il2CppCodeGenWriteBarrier(&____disposablesManager_4, value);
	}

	inline static int32_t get_offset_of__disposed_5() { return static_cast<int32_t>(offsetof(UnityDependencyRoot_t1917880567, ____disposed_5)); }
	inline bool get__disposed_5() const { return ____disposed_5; }
	inline bool* get_address_of__disposed_5() { return &____disposed_5; }
	inline void set__disposed_5(bool value)
	{
		____disposed_5 = value;
	}

	inline static int32_t get_offset_of__initialObjects_6() { return static_cast<int32_t>(offsetof(UnityDependencyRoot_t1917880567, ____initialObjects_6)); }
	inline List_1_t1634065389 * get__initialObjects_6() const { return ____initialObjects_6; }
	inline List_1_t1634065389 ** get_address_of__initialObjects_6() { return &____initialObjects_6; }
	inline void set__initialObjects_6(List_1_t1634065389 * value)
	{
		____initialObjects_6 = value;
		Il2CppCodeGenWriteBarrier(&____initialObjects_6, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
