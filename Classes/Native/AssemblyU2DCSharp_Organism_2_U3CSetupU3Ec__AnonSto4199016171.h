﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// IOrganism
struct IOrganism_t2580843579;
// Organism`2<System.Object,System.Object>
struct Organism_2_t948289219;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Organism`2/<Setup>c__AnonStorey11F<System.Object,System.Object>
struct  U3CSetupU3Ec__AnonStorey11F_t4199016171  : public Il2CppObject
{
public:
	// IOrganism Organism`2/<Setup>c__AnonStorey11F::c
	Il2CppObject * ___c_0;
	// Organism`2<TCarrier,TRoot> Organism`2/<Setup>c__AnonStorey11F::<>f__this
	Organism_2_t948289219 * ___U3CU3Ef__this_1;

public:
	inline static int32_t get_offset_of_c_0() { return static_cast<int32_t>(offsetof(U3CSetupU3Ec__AnonStorey11F_t4199016171, ___c_0)); }
	inline Il2CppObject * get_c_0() const { return ___c_0; }
	inline Il2CppObject ** get_address_of_c_0() { return &___c_0; }
	inline void set_c_0(Il2CppObject * value)
	{
		___c_0 = value;
		Il2CppCodeGenWriteBarrier(&___c_0, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__this_1() { return static_cast<int32_t>(offsetof(U3CSetupU3Ec__AnonStorey11F_t4199016171, ___U3CU3Ef__this_1)); }
	inline Organism_2_t948289219 * get_U3CU3Ef__this_1() const { return ___U3CU3Ef__this_1; }
	inline Organism_2_t948289219 ** get_address_of_U3CU3Ef__this_1() { return &___U3CU3Ef__this_1; }
	inline void set_U3CU3Ef__this_1(Organism_2_t948289219 * value)
	{
		___U3CU3Ef__this_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__this_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
