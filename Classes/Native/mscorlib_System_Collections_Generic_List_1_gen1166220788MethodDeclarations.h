﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.List`1<UniRx.Tuple`2<System.Object,System.Object>>
struct List_1_t1166220788;
// System.Collections.Generic.IEnumerable`1<UniRx.Tuple`2<System.Object,System.Object>>
struct IEnumerable_1_t3241416175;
// System.Collections.Generic.IEnumerator`1<UniRx.Tuple`2<System.Object,System.Object>>
struct IEnumerator_1_t1852368267;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t287207039;
// System.Object
struct Il2CppObject;
// System.Collections.Generic.ICollection`1<UniRx.Tuple`2<System.Object,System.Object>>
struct ICollection_1_t835093205;
// System.Collections.ObjectModel.ReadOnlyCollection`1<UniRx.Tuple`2<System.Object,System.Object>>
struct ReadOnlyCollection_1_t3532407167;
// UniRx.Tuple`2<System.Object,System.Object>[]
struct Tuple_2U5BU5D_t3897934010;
// System.Predicate`1<UniRx.Tuple`2<System.Object,System.Object>>
struct Predicate_1_t940225717;
// System.Action`1<UniRx.Tuple`2<System.Object,System.Object>>
struct Action_1_t517714524;
// System.Collections.Generic.IComparer`1<UniRx.Tuple`2<System.Object,System.Object>>
struct IComparer_1_t3068969228;
// System.Comparison`1<UniRx.Tuple`2<System.Object,System.Object>>
struct Comparison_1_t3072936695;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array2840145358.h"
#include "mscorlib_System_Object837106420.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Tuple_2_gen369261819.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera3546971076.h"

// System.Void System.Collections.Generic.List`1<UniRx.Tuple`2<System.Object,System.Object>>::.ctor()
extern "C"  void List_1__ctor_m153366876_gshared (List_1_t1166220788 * __this, const MethodInfo* method);
#define List_1__ctor_m153366876(__this, method) ((  void (*) (List_1_t1166220788 *, const MethodInfo*))List_1__ctor_m153366876_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UniRx.Tuple`2<System.Object,System.Object>>::.ctor(System.Collections.Generic.IEnumerable`1<T>)
extern "C"  void List_1__ctor_m1069742499_gshared (List_1_t1166220788 * __this, Il2CppObject* ___collection0, const MethodInfo* method);
#define List_1__ctor_m1069742499(__this, ___collection0, method) ((  void (*) (List_1_t1166220788 *, Il2CppObject*, const MethodInfo*))List_1__ctor_m1069742499_gshared)(__this, ___collection0, method)
// System.Void System.Collections.Generic.List`1<UniRx.Tuple`2<System.Object,System.Object>>::.ctor(System.Int32)
extern "C"  void List_1__ctor_m1426504877_gshared (List_1_t1166220788 * __this, int32_t ___capacity0, const MethodInfo* method);
#define List_1__ctor_m1426504877(__this, ___capacity0, method) ((  void (*) (List_1_t1166220788 *, int32_t, const MethodInfo*))List_1__ctor_m1426504877_gshared)(__this, ___capacity0, method)
// System.Void System.Collections.Generic.List`1<UniRx.Tuple`2<System.Object,System.Object>>::.cctor()
extern "C"  void List_1__cctor_m4272276945_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define List_1__cctor_m4272276945(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))List_1__cctor_m4272276945_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.List`1<UniRx.Tuple`2<System.Object,System.Object>>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
extern "C"  Il2CppObject* List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m3791632230_gshared (List_1_t1166220788 * __this, const MethodInfo* method);
#define List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m3791632230(__this, method) ((  Il2CppObject* (*) (List_1_t1166220788 *, const MethodInfo*))List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m3791632230_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UniRx.Tuple`2<System.Object,System.Object>>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void List_1_System_Collections_ICollection_CopyTo_m692738920_gshared (List_1_t1166220788 * __this, Il2CppArray * ___array0, int32_t ___arrayIndex1, const MethodInfo* method);
#define List_1_System_Collections_ICollection_CopyTo_m692738920(__this, ___array0, ___arrayIndex1, method) ((  void (*) (List_1_t1166220788 *, Il2CppArray *, int32_t, const MethodInfo*))List_1_System_Collections_ICollection_CopyTo_m692738920_gshared)(__this, ___array0, ___arrayIndex1, method)
// System.Collections.IEnumerator System.Collections.Generic.List`1<UniRx.Tuple`2<System.Object,System.Object>>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * List_1_System_Collections_IEnumerable_GetEnumerator_m3149404407_gshared (List_1_t1166220788 * __this, const MethodInfo* method);
#define List_1_System_Collections_IEnumerable_GetEnumerator_m3149404407(__this, method) ((  Il2CppObject * (*) (List_1_t1166220788 *, const MethodInfo*))List_1_System_Collections_IEnumerable_GetEnumerator_m3149404407_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<UniRx.Tuple`2<System.Object,System.Object>>::System.Collections.IList.Add(System.Object)
extern "C"  int32_t List_1_System_Collections_IList_Add_m1637521702_gshared (List_1_t1166220788 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define List_1_System_Collections_IList_Add_m1637521702(__this, ___item0, method) ((  int32_t (*) (List_1_t1166220788 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Add_m1637521702_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.List`1<UniRx.Tuple`2<System.Object,System.Object>>::System.Collections.IList.Contains(System.Object)
extern "C"  bool List_1_System_Collections_IList_Contains_m2274035034_gshared (List_1_t1166220788 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define List_1_System_Collections_IList_Contains_m2274035034(__this, ___item0, method) ((  bool (*) (List_1_t1166220788 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Contains_m2274035034_gshared)(__this, ___item0, method)
// System.Int32 System.Collections.Generic.List`1<UniRx.Tuple`2<System.Object,System.Object>>::System.Collections.IList.IndexOf(System.Object)
extern "C"  int32_t List_1_System_Collections_IList_IndexOf_m570281086_gshared (List_1_t1166220788 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define List_1_System_Collections_IList_IndexOf_m570281086(__this, ___item0, method) ((  int32_t (*) (List_1_t1166220788 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_IndexOf_m570281086_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<UniRx.Tuple`2<System.Object,System.Object>>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C"  void List_1_System_Collections_IList_Insert_m3861587953_gshared (List_1_t1166220788 * __this, int32_t ___index0, Il2CppObject * ___item1, const MethodInfo* method);
#define List_1_System_Collections_IList_Insert_m3861587953(__this, ___index0, ___item1, method) ((  void (*) (List_1_t1166220788 *, int32_t, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Insert_m3861587953_gshared)(__this, ___index0, ___item1, method)
// System.Void System.Collections.Generic.List`1<UniRx.Tuple`2<System.Object,System.Object>>::System.Collections.IList.Remove(System.Object)
extern "C"  void List_1_System_Collections_IList_Remove_m3511774615_gshared (List_1_t1166220788 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define List_1_System_Collections_IList_Remove_m3511774615(__this, ___item0, method) ((  void (*) (List_1_t1166220788 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Remove_m3511774615_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.List`1<UniRx.Tuple`2<System.Object,System.Object>>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C"  bool List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m1163627739_gshared (List_1_t1166220788 * __this, const MethodInfo* method);
#define List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m1163627739(__this, method) ((  bool (*) (List_1_t1166220788 *, const MethodInfo*))List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m1163627739_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UniRx.Tuple`2<System.Object,System.Object>>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool List_1_System_Collections_ICollection_get_IsSynchronized_m1253514178_gshared (List_1_t1166220788 * __this, const MethodInfo* method);
#define List_1_System_Collections_ICollection_get_IsSynchronized_m1253514178(__this, method) ((  bool (*) (List_1_t1166220788 *, const MethodInfo*))List_1_System_Collections_ICollection_get_IsSynchronized_m1253514178_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<UniRx.Tuple`2<System.Object,System.Object>>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * List_1_System_Collections_ICollection_get_SyncRoot_m1185554932_gshared (List_1_t1166220788 * __this, const MethodInfo* method);
#define List_1_System_Collections_ICollection_get_SyncRoot_m1185554932(__this, method) ((  Il2CppObject * (*) (List_1_t1166220788 *, const MethodInfo*))List_1_System_Collections_ICollection_get_SyncRoot_m1185554932_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UniRx.Tuple`2<System.Object,System.Object>>::System.Collections.IList.get_IsFixedSize()
extern "C"  bool List_1_System_Collections_IList_get_IsFixedSize_m909131849_gshared (List_1_t1166220788 * __this, const MethodInfo* method);
#define List_1_System_Collections_IList_get_IsFixedSize_m909131849(__this, method) ((  bool (*) (List_1_t1166220788 *, const MethodInfo*))List_1_System_Collections_IList_get_IsFixedSize_m909131849_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UniRx.Tuple`2<System.Object,System.Object>>::System.Collections.IList.get_IsReadOnly()
extern "C"  bool List_1_System_Collections_IList_get_IsReadOnly_m1453166992_gshared (List_1_t1166220788 * __this, const MethodInfo* method);
#define List_1_System_Collections_IList_get_IsReadOnly_m1453166992(__this, method) ((  bool (*) (List_1_t1166220788 *, const MethodInfo*))List_1_System_Collections_IList_get_IsReadOnly_m1453166992_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<UniRx.Tuple`2<System.Object,System.Object>>::System.Collections.IList.get_Item(System.Int32)
extern "C"  Il2CppObject * List_1_System_Collections_IList_get_Item_m2217567867_gshared (List_1_t1166220788 * __this, int32_t ___index0, const MethodInfo* method);
#define List_1_System_Collections_IList_get_Item_m2217567867(__this, ___index0, method) ((  Il2CppObject * (*) (List_1_t1166220788 *, int32_t, const MethodInfo*))List_1_System_Collections_IList_get_Item_m2217567867_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<UniRx.Tuple`2<System.Object,System.Object>>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C"  void List_1_System_Collections_IList_set_Item_m748231560_gshared (List_1_t1166220788 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method);
#define List_1_System_Collections_IList_set_Item_m748231560(__this, ___index0, ___value1, method) ((  void (*) (List_1_t1166220788 *, int32_t, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_set_Item_m748231560_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.Generic.List`1<UniRx.Tuple`2<System.Object,System.Object>>::Add(T)
extern "C"  void List_1_Add_m272184227_gshared (List_1_t1166220788 * __this, Tuple_2_t369261819  ___item0, const MethodInfo* method);
#define List_1_Add_m272184227(__this, ___item0, method) ((  void (*) (List_1_t1166220788 *, Tuple_2_t369261819 , const MethodInfo*))List_1_Add_m272184227_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<UniRx.Tuple`2<System.Object,System.Object>>::GrowIfNeeded(System.Int32)
extern "C"  void List_1_GrowIfNeeded_m3820565086_gshared (List_1_t1166220788 * __this, int32_t ___newCount0, const MethodInfo* method);
#define List_1_GrowIfNeeded_m3820565086(__this, ___newCount0, method) ((  void (*) (List_1_t1166220788 *, int32_t, const MethodInfo*))List_1_GrowIfNeeded_m3820565086_gshared)(__this, ___newCount0, method)
// System.Void System.Collections.Generic.List`1<UniRx.Tuple`2<System.Object,System.Object>>::AddCollection(System.Collections.Generic.ICollection`1<T>)
extern "C"  void List_1_AddCollection_m1150849372_gshared (List_1_t1166220788 * __this, Il2CppObject* ___collection0, const MethodInfo* method);
#define List_1_AddCollection_m1150849372(__this, ___collection0, method) ((  void (*) (List_1_t1166220788 *, Il2CppObject*, const MethodInfo*))List_1_AddCollection_m1150849372_gshared)(__this, ___collection0, method)
// System.Void System.Collections.Generic.List`1<UniRx.Tuple`2<System.Object,System.Object>>::AddEnumerable(System.Collections.Generic.IEnumerable`1<T>)
extern "C"  void List_1_AddEnumerable_m411821596_gshared (List_1_t1166220788 * __this, Il2CppObject* ___enumerable0, const MethodInfo* method);
#define List_1_AddEnumerable_m411821596(__this, ___enumerable0, method) ((  void (*) (List_1_t1166220788 *, Il2CppObject*, const MethodInfo*))List_1_AddEnumerable_m411821596_gshared)(__this, ___enumerable0, method)
// System.Void System.Collections.Generic.List`1<UniRx.Tuple`2<System.Object,System.Object>>::AddRange(System.Collections.Generic.IEnumerable`1<T>)
extern "C"  void List_1_AddRange_m4092372731_gshared (List_1_t1166220788 * __this, Il2CppObject* ___collection0, const MethodInfo* method);
#define List_1_AddRange_m4092372731(__this, ___collection0, method) ((  void (*) (List_1_t1166220788 *, Il2CppObject*, const MethodInfo*))List_1_AddRange_m4092372731_gshared)(__this, ___collection0, method)
// System.Collections.ObjectModel.ReadOnlyCollection`1<T> System.Collections.Generic.List`1<UniRx.Tuple`2<System.Object,System.Object>>::AsReadOnly()
extern "C"  ReadOnlyCollection_1_t3532407167 * List_1_AsReadOnly_m2556280362_gshared (List_1_t1166220788 * __this, const MethodInfo* method);
#define List_1_AsReadOnly_m2556280362(__this, method) ((  ReadOnlyCollection_1_t3532407167 * (*) (List_1_t1166220788 *, const MethodInfo*))List_1_AsReadOnly_m2556280362_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UniRx.Tuple`2<System.Object,System.Object>>::Clear()
extern "C"  void List_1_Clear_m1854467463_gshared (List_1_t1166220788 * __this, const MethodInfo* method);
#define List_1_Clear_m1854467463(__this, method) ((  void (*) (List_1_t1166220788 *, const MethodInfo*))List_1_Clear_m1854467463_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UniRx.Tuple`2<System.Object,System.Object>>::Contains(T)
extern "C"  bool List_1_Contains_m1083072377_gshared (List_1_t1166220788 * __this, Tuple_2_t369261819  ___item0, const MethodInfo* method);
#define List_1_Contains_m1083072377(__this, ___item0, method) ((  bool (*) (List_1_t1166220788 *, Tuple_2_t369261819 , const MethodInfo*))List_1_Contains_m1083072377_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<UniRx.Tuple`2<System.Object,System.Object>>::CopyTo(T[],System.Int32)
extern "C"  void List_1_CopyTo_m2249496787_gshared (List_1_t1166220788 * __this, Tuple_2U5BU5D_t3897934010* ___array0, int32_t ___arrayIndex1, const MethodInfo* method);
#define List_1_CopyTo_m2249496787(__this, ___array0, ___arrayIndex1, method) ((  void (*) (List_1_t1166220788 *, Tuple_2U5BU5D_t3897934010*, int32_t, const MethodInfo*))List_1_CopyTo_m2249496787_gshared)(__this, ___array0, ___arrayIndex1, method)
// T System.Collections.Generic.List`1<UniRx.Tuple`2<System.Object,System.Object>>::Find(System.Predicate`1<T>)
extern "C"  Tuple_2_t369261819  List_1_Find_m2498671251_gshared (List_1_t1166220788 * __this, Predicate_1_t940225717 * ___match0, const MethodInfo* method);
#define List_1_Find_m2498671251(__this, ___match0, method) ((  Tuple_2_t369261819  (*) (List_1_t1166220788 *, Predicate_1_t940225717 *, const MethodInfo*))List_1_Find_m2498671251_gshared)(__this, ___match0, method)
// System.Void System.Collections.Generic.List`1<UniRx.Tuple`2<System.Object,System.Object>>::CheckMatch(System.Predicate`1<T>)
extern "C"  void List_1_CheckMatch_m1918944112_gshared (Il2CppObject * __this /* static, unused */, Predicate_1_t940225717 * ___match0, const MethodInfo* method);
#define List_1_CheckMatch_m1918944112(__this /* static, unused */, ___match0, method) ((  void (*) (Il2CppObject * /* static, unused */, Predicate_1_t940225717 *, const MethodInfo*))List_1_CheckMatch_m1918944112_gshared)(__this /* static, unused */, ___match0, method)
// System.Int32 System.Collections.Generic.List`1<UniRx.Tuple`2<System.Object,System.Object>>::GetIndex(System.Int32,System.Int32,System.Predicate`1<T>)
extern "C"  int32_t List_1_GetIndex_m4010948685_gshared (List_1_t1166220788 * __this, int32_t ___startIndex0, int32_t ___count1, Predicate_1_t940225717 * ___match2, const MethodInfo* method);
#define List_1_GetIndex_m4010948685(__this, ___startIndex0, ___count1, ___match2, method) ((  int32_t (*) (List_1_t1166220788 *, int32_t, int32_t, Predicate_1_t940225717 *, const MethodInfo*))List_1_GetIndex_m4010948685_gshared)(__this, ___startIndex0, ___count1, ___match2, method)
// System.Void System.Collections.Generic.List`1<UniRx.Tuple`2<System.Object,System.Object>>::ForEach(System.Action`1<T>)
extern "C"  void List_1_ForEach_m1257173732_gshared (List_1_t1166220788 * __this, Action_1_t517714524 * ___action0, const MethodInfo* method);
#define List_1_ForEach_m1257173732(__this, ___action0, method) ((  void (*) (List_1_t1166220788 *, Action_1_t517714524 *, const MethodInfo*))List_1_ForEach_m1257173732_gshared)(__this, ___action0, method)
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<UniRx.Tuple`2<System.Object,System.Object>>::GetEnumerator()
extern "C"  Enumerator_t3546971076  List_1_GetEnumerator_m1101151286_gshared (List_1_t1166220788 * __this, const MethodInfo* method);
#define List_1_GetEnumerator_m1101151286(__this, method) ((  Enumerator_t3546971076  (*) (List_1_t1166220788 *, const MethodInfo*))List_1_GetEnumerator_m1101151286_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<UniRx.Tuple`2<System.Object,System.Object>>::IndexOf(T)
extern "C"  int32_t List_1_IndexOf_m3129767135_gshared (List_1_t1166220788 * __this, Tuple_2_t369261819  ___item0, const MethodInfo* method);
#define List_1_IndexOf_m3129767135(__this, ___item0, method) ((  int32_t (*) (List_1_t1166220788 *, Tuple_2_t369261819 , const MethodInfo*))List_1_IndexOf_m3129767135_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<UniRx.Tuple`2<System.Object,System.Object>>::Shift(System.Int32,System.Int32)
extern "C"  void List_1_Shift_m1958472746_gshared (List_1_t1166220788 * __this, int32_t ___start0, int32_t ___delta1, const MethodInfo* method);
#define List_1_Shift_m1958472746(__this, ___start0, ___delta1, method) ((  void (*) (List_1_t1166220788 *, int32_t, int32_t, const MethodInfo*))List_1_Shift_m1958472746_gshared)(__this, ___start0, ___delta1, method)
// System.Void System.Collections.Generic.List`1<UniRx.Tuple`2<System.Object,System.Object>>::CheckIndex(System.Int32)
extern "C"  void List_1_CheckIndex_m1995863971_gshared (List_1_t1166220788 * __this, int32_t ___index0, const MethodInfo* method);
#define List_1_CheckIndex_m1995863971(__this, ___index0, method) ((  void (*) (List_1_t1166220788 *, int32_t, const MethodInfo*))List_1_CheckIndex_m1995863971_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<UniRx.Tuple`2<System.Object,System.Object>>::Insert(System.Int32,T)
extern "C"  void List_1_Insert_m4019767306_gshared (List_1_t1166220788 * __this, int32_t ___index0, Tuple_2_t369261819  ___item1, const MethodInfo* method);
#define List_1_Insert_m4019767306(__this, ___index0, ___item1, method) ((  void (*) (List_1_t1166220788 *, int32_t, Tuple_2_t369261819 , const MethodInfo*))List_1_Insert_m4019767306_gshared)(__this, ___index0, ___item1, method)
// System.Void System.Collections.Generic.List`1<UniRx.Tuple`2<System.Object,System.Object>>::CheckCollection(System.Collections.Generic.IEnumerable`1<T>)
extern "C"  void List_1_CheckCollection_m2686319871_gshared (List_1_t1166220788 * __this, Il2CppObject* ___collection0, const MethodInfo* method);
#define List_1_CheckCollection_m2686319871(__this, ___collection0, method) ((  void (*) (List_1_t1166220788 *, Il2CppObject*, const MethodInfo*))List_1_CheckCollection_m2686319871_gshared)(__this, ___collection0, method)
// System.Boolean System.Collections.Generic.List`1<UniRx.Tuple`2<System.Object,System.Object>>::Remove(T)
extern "C"  bool List_1_Remove_m1788258740_gshared (List_1_t1166220788 * __this, Tuple_2_t369261819  ___item0, const MethodInfo* method);
#define List_1_Remove_m1788258740(__this, ___item0, method) ((  bool (*) (List_1_t1166220788 *, Tuple_2_t369261819 , const MethodInfo*))List_1_Remove_m1788258740_gshared)(__this, ___item0, method)
// System.Int32 System.Collections.Generic.List`1<UniRx.Tuple`2<System.Object,System.Object>>::RemoveAll(System.Predicate`1<T>)
extern "C"  int32_t List_1_RemoveAll_m1632418658_gshared (List_1_t1166220788 * __this, Predicate_1_t940225717 * ___match0, const MethodInfo* method);
#define List_1_RemoveAll_m1632418658(__this, ___match0, method) ((  int32_t (*) (List_1_t1166220788 *, Predicate_1_t940225717 *, const MethodInfo*))List_1_RemoveAll_m1632418658_gshared)(__this, ___match0, method)
// System.Void System.Collections.Generic.List`1<UniRx.Tuple`2<System.Object,System.Object>>::RemoveAt(System.Int32)
extern "C"  void List_1_RemoveAt_m1893620176_gshared (List_1_t1166220788 * __this, int32_t ___index0, const MethodInfo* method);
#define List_1_RemoveAt_m1893620176(__this, ___index0, method) ((  void (*) (List_1_t1166220788 *, int32_t, const MethodInfo*))List_1_RemoveAt_m1893620176_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<UniRx.Tuple`2<System.Object,System.Object>>::Reverse()
extern "C"  void List_1_Reverse_m1394491036_gshared (List_1_t1166220788 * __this, const MethodInfo* method);
#define List_1_Reverse_m1394491036(__this, method) ((  void (*) (List_1_t1166220788 *, const MethodInfo*))List_1_Reverse_m1394491036_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UniRx.Tuple`2<System.Object,System.Object>>::Sort()
extern "C"  void List_1_Sort_m798156422_gshared (List_1_t1166220788 * __this, const MethodInfo* method);
#define List_1_Sort_m798156422(__this, method) ((  void (*) (List_1_t1166220788 *, const MethodInfo*))List_1_Sort_m798156422_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UniRx.Tuple`2<System.Object,System.Object>>::Sort(System.Collections.Generic.IComparer`1<T>)
extern "C"  void List_1_Sort_m1493626398_gshared (List_1_t1166220788 * __this, Il2CppObject* ___comparer0, const MethodInfo* method);
#define List_1_Sort_m1493626398(__this, ___comparer0, method) ((  void (*) (List_1_t1166220788 *, Il2CppObject*, const MethodInfo*))List_1_Sort_m1493626398_gshared)(__this, ___comparer0, method)
// System.Void System.Collections.Generic.List`1<UniRx.Tuple`2<System.Object,System.Object>>::Sort(System.Comparison`1<T>)
extern "C"  void List_1_Sort_m1698605657_gshared (List_1_t1166220788 * __this, Comparison_1_t3072936695 * ___comparison0, const MethodInfo* method);
#define List_1_Sort_m1698605657(__this, ___comparison0, method) ((  void (*) (List_1_t1166220788 *, Comparison_1_t3072936695 *, const MethodInfo*))List_1_Sort_m1698605657_gshared)(__this, ___comparison0, method)
// T[] System.Collections.Generic.List`1<UniRx.Tuple`2<System.Object,System.Object>>::ToArray()
extern "C"  Tuple_2U5BU5D_t3897934010* List_1_ToArray_m632698357_gshared (List_1_t1166220788 * __this, const MethodInfo* method);
#define List_1_ToArray_m632698357(__this, method) ((  Tuple_2U5BU5D_t3897934010* (*) (List_1_t1166220788 *, const MethodInfo*))List_1_ToArray_m632698357_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UniRx.Tuple`2<System.Object,System.Object>>::TrimExcess()
extern "C"  void List_1_TrimExcess_m738232735_gshared (List_1_t1166220788 * __this, const MethodInfo* method);
#define List_1_TrimExcess_m738232735(__this, method) ((  void (*) (List_1_t1166220788 *, const MethodInfo*))List_1_TrimExcess_m738232735_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<UniRx.Tuple`2<System.Object,System.Object>>::get_Capacity()
extern "C"  int32_t List_1_get_Capacity_m3818448271_gshared (List_1_t1166220788 * __this, const MethodInfo* method);
#define List_1_get_Capacity_m3818448271(__this, method) ((  int32_t (*) (List_1_t1166220788 *, const MethodInfo*))List_1_get_Capacity_m3818448271_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UniRx.Tuple`2<System.Object,System.Object>>::set_Capacity(System.Int32)
extern "C"  void List_1_set_Capacity_m1030753904_gshared (List_1_t1166220788 * __this, int32_t ___value0, const MethodInfo* method);
#define List_1_set_Capacity_m1030753904(__this, ___value0, method) ((  void (*) (List_1_t1166220788 *, int32_t, const MethodInfo*))List_1_set_Capacity_m1030753904_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.Generic.List`1<UniRx.Tuple`2<System.Object,System.Object>>::get_Count()
extern "C"  int32_t List_1_get_Count_m2350808188_gshared (List_1_t1166220788 * __this, const MethodInfo* method);
#define List_1_get_Count_m2350808188(__this, method) ((  int32_t (*) (List_1_t1166220788 *, const MethodInfo*))List_1_get_Count_m2350808188_gshared)(__this, method)
// T System.Collections.Generic.List`1<UniRx.Tuple`2<System.Object,System.Object>>::get_Item(System.Int32)
extern "C"  Tuple_2_t369261819  List_1_get_Item_m1762998326_gshared (List_1_t1166220788 * __this, int32_t ___index0, const MethodInfo* method);
#define List_1_get_Item_m1762998326(__this, ___index0, method) ((  Tuple_2_t369261819  (*) (List_1_t1166220788 *, int32_t, const MethodInfo*))List_1_get_Item_m1762998326_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<UniRx.Tuple`2<System.Object,System.Object>>::set_Item(System.Int32,T)
extern "C"  void List_1_set_Item_m3630378593_gshared (List_1_t1166220788 * __this, int32_t ___index0, Tuple_2_t369261819  ___value1, const MethodInfo* method);
#define List_1_set_Item_m3630378593(__this, ___index0, ___value1, method) ((  void (*) (List_1_t1166220788 *, int32_t, Tuple_2_t369261819 , const MethodInfo*))List_1_set_Item_m3630378593_gshared)(__this, ___index0, ___value1, method)
