﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Object
struct Il2CppObject;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.DisposableExtensions/<AddTo>c__AnonStorey36`1<System.Object>
struct  U3CAddToU3Ec__AnonStorey36_1_t595347342  : public Il2CppObject
{
public:
	// T UniRx.DisposableExtensions/<AddTo>c__AnonStorey36`1::disposable
	Il2CppObject * ___disposable_0;

public:
	inline static int32_t get_offset_of_disposable_0() { return static_cast<int32_t>(offsetof(U3CAddToU3Ec__AnonStorey36_1_t595347342, ___disposable_0)); }
	inline Il2CppObject * get_disposable_0() const { return ___disposable_0; }
	inline Il2CppObject ** get_address_of_disposable_0() { return &___disposable_0; }
	inline void set_disposable_0(Il2CppObject * value)
	{
		___disposable_0 = value;
		Il2CppCodeGenWriteBarrier(&___disposable_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
