﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.CombineLatestObservable`1/CombineLatest<System.Object>
struct CombineLatest_t868253967;
// UniRx.Operators.CombineLatestObservable`1<System.Object>
struct CombineLatestObservable_1_t3509237416;
// UniRx.IObserver`1<System.Collections.Generic.IList`1<System.Object>>
struct IObserver_1_t920630341;
// System.IDisposable
struct IDisposable_t1628921374;
// System.Collections.Generic.IList`1<System.Object>
struct IList_1_t3003598734;
// System.Exception
struct Exception_t1967233988;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Exception1967233988.h"

// System.Void UniRx.Operators.CombineLatestObservable`1/CombineLatest<System.Object>::.ctor(UniRx.Operators.CombineLatestObservable`1<T>,UniRx.IObserver`1<System.Collections.Generic.IList`1<T>>,System.IDisposable)
extern "C"  void CombineLatest__ctor_m279933093_gshared (CombineLatest_t868253967 * __this, CombineLatestObservable_1_t3509237416 * ___parent0, Il2CppObject* ___observer1, Il2CppObject * ___cancel2, const MethodInfo* method);
#define CombineLatest__ctor_m279933093(__this, ___parent0, ___observer1, ___cancel2, method) ((  void (*) (CombineLatest_t868253967 *, CombineLatestObservable_1_t3509237416 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))CombineLatest__ctor_m279933093_gshared)(__this, ___parent0, ___observer1, ___cancel2, method)
// System.IDisposable UniRx.Operators.CombineLatestObservable`1/CombineLatest<System.Object>::Run()
extern "C"  Il2CppObject * CombineLatest_Run_m447223867_gshared (CombineLatest_t868253967 * __this, const MethodInfo* method);
#define CombineLatest_Run_m447223867(__this, method) ((  Il2CppObject * (*) (CombineLatest_t868253967 *, const MethodInfo*))CombineLatest_Run_m447223867_gshared)(__this, method)
// System.Void UniRx.Operators.CombineLatestObservable`1/CombineLatest<System.Object>::Publish(System.Int32)
extern "C"  void CombineLatest_Publish_m3050808939_gshared (CombineLatest_t868253967 * __this, int32_t ___index0, const MethodInfo* method);
#define CombineLatest_Publish_m3050808939(__this, ___index0, method) ((  void (*) (CombineLatest_t868253967 *, int32_t, const MethodInfo*))CombineLatest_Publish_m3050808939_gshared)(__this, ___index0, method)
// System.Void UniRx.Operators.CombineLatestObservable`1/CombineLatest<System.Object>::OnNext(System.Collections.Generic.IList`1<T>)
extern "C"  void CombineLatest_OnNext_m2009271756_gshared (CombineLatest_t868253967 * __this, Il2CppObject* ___value0, const MethodInfo* method);
#define CombineLatest_OnNext_m2009271756(__this, ___value0, method) ((  void (*) (CombineLatest_t868253967 *, Il2CppObject*, const MethodInfo*))CombineLatest_OnNext_m2009271756_gshared)(__this, ___value0, method)
// System.Void UniRx.Operators.CombineLatestObservable`1/CombineLatest<System.Object>::OnError(System.Exception)
extern "C"  void CombineLatest_OnError_m619400356_gshared (CombineLatest_t868253967 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method);
#define CombineLatest_OnError_m619400356(__this, ___error0, method) ((  void (*) (CombineLatest_t868253967 *, Exception_t1967233988 *, const MethodInfo*))CombineLatest_OnError_m619400356_gshared)(__this, ___error0, method)
// System.Void UniRx.Operators.CombineLatestObservable`1/CombineLatest<System.Object>::OnCompleted()
extern "C"  void CombineLatest_OnCompleted_m2775373303_gshared (CombineLatest_t868253967 * __this, const MethodInfo* method);
#define CombineLatest_OnCompleted_m2775373303(__this, method) ((  void (*) (CombineLatest_t868253967 *, const MethodInfo*))CombineLatest_OnCompleted_m2775373303_gshared)(__this, method)
