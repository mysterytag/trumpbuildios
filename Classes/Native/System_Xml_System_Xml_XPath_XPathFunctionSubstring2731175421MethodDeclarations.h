﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Xml.XPath.XPathFunctionSubstringAfter
struct XPathFunctionSubstringAfter_t2731175421;
// System.Xml.XPath.FunctionArguments
struct FunctionArguments_t3402201403;
// System.Object
struct Il2CppObject;
// System.Xml.XPath.BaseIterator
struct BaseIterator_t3696600956;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "System_Xml_System_Xml_XPath_FunctionArguments3402201403.h"
#include "System_Xml_System_Xml_XPath_XPathResultType370286193.h"
#include "System_Xml_System_Xml_XPath_BaseIterator3696600956.h"

// System.Void System.Xml.XPath.XPathFunctionSubstringAfter::.ctor(System.Xml.XPath.FunctionArguments)
extern "C"  void XPathFunctionSubstringAfter__ctor_m4187633269 (XPathFunctionSubstringAfter_t2731175421 * __this, FunctionArguments_t3402201403 * ___args0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Xml.XPath.XPathResultType System.Xml.XPath.XPathFunctionSubstringAfter::get_ReturnType()
extern "C"  int32_t XPathFunctionSubstringAfter_get_ReturnType_m4232721039 (XPathFunctionSubstringAfter_t2731175421 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Xml.XPath.XPathFunctionSubstringAfter::get_Peer()
extern "C"  bool XPathFunctionSubstringAfter_get_Peer_m94219979 (XPathFunctionSubstringAfter_t2731175421 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Xml.XPath.XPathFunctionSubstringAfter::Evaluate(System.Xml.XPath.BaseIterator)
extern "C"  Il2CppObject * XPathFunctionSubstringAfter_Evaluate_m3905912518 (XPathFunctionSubstringAfter_t2731175421 * __this, BaseIterator_t3696600956 * ___iter0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Xml.XPath.XPathFunctionSubstringAfter::ToString()
extern "C"  String_t* XPathFunctionSubstringAfter_ToString_m3496542695 (XPathFunctionSubstringAfter_t2731175421 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
