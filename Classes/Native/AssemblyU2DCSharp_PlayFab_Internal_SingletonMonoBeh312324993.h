﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Object
struct Il2CppObject;

#include "UnityEngine_UnityEngine_MonoBehaviour3012272455.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PlayFab.Internal.SingletonMonoBehaviour`1<System.Object>
struct  SingletonMonoBehaviour_1_t312324993  : public MonoBehaviour_t3012272455
{
public:
	// System.Boolean PlayFab.Internal.SingletonMonoBehaviour`1::<initialized>k__BackingField
	bool ___U3CinitializedU3Ek__BackingField_3;

public:
	inline static int32_t get_offset_of_U3CinitializedU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(SingletonMonoBehaviour_1_t312324993, ___U3CinitializedU3Ek__BackingField_3)); }
	inline bool get_U3CinitializedU3Ek__BackingField_3() const { return ___U3CinitializedU3Ek__BackingField_3; }
	inline bool* get_address_of_U3CinitializedU3Ek__BackingField_3() { return &___U3CinitializedU3Ek__BackingField_3; }
	inline void set_U3CinitializedU3Ek__BackingField_3(bool value)
	{
		___U3CinitializedU3Ek__BackingField_3 = value;
	}
};

struct SingletonMonoBehaviour_1_t312324993_StaticFields
{
public:
	// T PlayFab.Internal.SingletonMonoBehaviour`1::m_instance
	Il2CppObject * ___m_instance_2;

public:
	inline static int32_t get_offset_of_m_instance_2() { return static_cast<int32_t>(offsetof(SingletonMonoBehaviour_1_t312324993_StaticFields, ___m_instance_2)); }
	inline Il2CppObject * get_m_instance_2() const { return ___m_instance_2; }
	inline Il2CppObject ** get_address_of_m_instance_2() { return &___m_instance_2; }
	inline void set_m_instance_2(Il2CppObject * value)
	{
		___m_instance_2 = value;
		Il2CppCodeGenWriteBarrier(&___m_instance_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
