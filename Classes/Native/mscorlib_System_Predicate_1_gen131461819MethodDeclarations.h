﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Predicate_1_gen1408070318MethodDeclarations.h"

// System.Void System.Predicate`1<WindowBase>::.ctor(System.Object,System.IntPtr)
#define Predicate_1__ctor_m2491163546(__this, ___object0, ___method1, method) ((  void (*) (Predicate_1_t131461819 *, Il2CppObject *, IntPtr_t, const MethodInfo*))Predicate_1__ctor_m982040097_gshared)(__this, ___object0, ___method1, method)
// System.Boolean System.Predicate`1<WindowBase>::Invoke(T)
#define Predicate_1_Invoke_m3644547752(__this, ___obj0, method) ((  bool (*) (Predicate_1_t131461819 *, WindowBase_t3855465217 *, const MethodInfo*))Predicate_1_Invoke_m4106178309_gshared)(__this, ___obj0, method)
// System.IAsyncResult System.Predicate`1<WindowBase>::BeginInvoke(T,System.AsyncCallback,System.Object)
#define Predicate_1_BeginInvoke_m3215980087(__this, ___obj0, ___callback1, ___object2, method) ((  Il2CppObject * (*) (Predicate_1_t131461819 *, WindowBase_t3855465217 *, AsyncCallback_t1363551830 *, Il2CppObject *, const MethodInfo*))Predicate_1_BeginInvoke_m2038073176_gshared)(__this, ___obj0, ___callback1, ___object2, method)
// System.Boolean System.Predicate`1<WindowBase>::EndInvoke(System.IAsyncResult)
#define Predicate_1_EndInvoke_m2175572524(__this, ___result0, method) ((  bool (*) (Predicate_1_t131461819 *, Il2CppObject *, const MethodInfo*))Predicate_1_EndInvoke_m3970497007_gshared)(__this, ___result0, method)
