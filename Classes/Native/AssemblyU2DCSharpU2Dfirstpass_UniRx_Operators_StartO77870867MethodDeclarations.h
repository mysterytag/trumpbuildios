﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.StartObservable`1<System.Int32>
struct StartObservable_1_t77870867;
// System.Func`1<System.Int32>
struct Func_1_t3990196034;
// UniRx.IScheduler
struct IScheduler_t2938318244;
// System.Action
struct Action_t437523947;
// System.IDisposable
struct IDisposable_t1628921374;
// UniRx.IObserver`1<System.Int32>
struct IObserver_1_t764446394;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Nullable_1_gen3649900800.h"
#include "System_Core_System_Action437523947.h"

// System.Void UniRx.Operators.StartObservable`1<System.Int32>::.ctor(System.Func`1<T>,System.Nullable`1<System.TimeSpan>,UniRx.IScheduler)
extern "C"  void StartObservable_1__ctor_m3852172604_gshared (StartObservable_1_t77870867 * __this, Func_1_t3990196034 * ___function0, Nullable_1_t3649900800  ___startAfter1, Il2CppObject * ___scheduler2, const MethodInfo* method);
#define StartObservable_1__ctor_m3852172604(__this, ___function0, ___startAfter1, ___scheduler2, method) ((  void (*) (StartObservable_1_t77870867 *, Func_1_t3990196034 *, Nullable_1_t3649900800 , Il2CppObject *, const MethodInfo*))StartObservable_1__ctor_m3852172604_gshared)(__this, ___function0, ___startAfter1, ___scheduler2, method)
// System.Void UniRx.Operators.StartObservable`1<System.Int32>::.ctor(System.Action,System.Nullable`1<System.TimeSpan>,UniRx.IScheduler)
extern "C"  void StartObservable_1__ctor_m1752235073_gshared (StartObservable_1_t77870867 * __this, Action_t437523947 * ___action0, Nullable_1_t3649900800  ___startAfter1, Il2CppObject * ___scheduler2, const MethodInfo* method);
#define StartObservable_1__ctor_m1752235073(__this, ___action0, ___startAfter1, ___scheduler2, method) ((  void (*) (StartObservable_1_t77870867 *, Action_t437523947 *, Nullable_1_t3649900800 , Il2CppObject *, const MethodInfo*))StartObservable_1__ctor_m1752235073_gshared)(__this, ___action0, ___startAfter1, ___scheduler2, method)
// System.IDisposable UniRx.Operators.StartObservable`1<System.Int32>::SubscribeCore(UniRx.IObserver`1<T>,System.IDisposable)
extern "C"  Il2CppObject * StartObservable_1_SubscribeCore_m109930563_gshared (StartObservable_1_t77870867 * __this, Il2CppObject* ___observer0, Il2CppObject * ___cancel1, const MethodInfo* method);
#define StartObservable_1_SubscribeCore_m109930563(__this, ___observer0, ___cancel1, method) ((  Il2CppObject * (*) (StartObservable_1_t77870867 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))StartObservable_1_SubscribeCore_m109930563_gshared)(__this, ___observer0, ___cancel1, method)
