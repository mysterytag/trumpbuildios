﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera4014815677MethodDeclarations.h"

// System.Void System.Collections.Generic.List`1/Enumerator<Zenject.InjectableInfo>::.ctor(System.Collections.Generic.List`1<T>)
#define Enumerator__ctor_m2403309515(__this, ___l0, method) ((  void (*) (Enumerator_t30451735 *, List_1_t1944668743 *, const MethodInfo*))Enumerator__ctor_m1029849669_gshared)(__this, ___l0, method)
// System.Void System.Collections.Generic.List`1/Enumerator<Zenject.InjectableInfo>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m1725729191(__this, method) ((  void (*) (Enumerator_t30451735 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m771996397_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1/Enumerator<Zenject.InjectableInfo>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m1652600733(__this, method) ((  Il2CppObject * (*) (Enumerator_t30451735 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m3561903705_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<Zenject.InjectableInfo>::Dispose()
#define Enumerator_Dispose_m139102320(__this, method) ((  void (*) (Enumerator_t30451735 *, const MethodInfo*))Enumerator_Dispose_m2904289642_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<Zenject.InjectableInfo>::VerifyState()
#define Enumerator_VerifyState_m3206844329(__this, method) ((  void (*) (Enumerator_t30451735 *, const MethodInfo*))Enumerator_VerifyState_m1522854819_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<Zenject.InjectableInfo>::MoveNext()
#define Enumerator_MoveNext_m602581591(__this, method) ((  bool (*) (Enumerator_t30451735 *, const MethodInfo*))Enumerator_MoveNext_m302222300_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<Zenject.InjectableInfo>::get_Current()
#define Enumerator_get_Current_m3628025474(__this, method) ((  InjectableInfo_t1147709774 * (*) (Enumerator_t30451735 *, const MethodInfo*))Enumerator_get_Current_m1113061044_gshared)(__this, method)
