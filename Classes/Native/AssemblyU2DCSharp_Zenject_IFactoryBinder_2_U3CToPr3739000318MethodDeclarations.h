﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Zenject.IFactoryBinder`2/<ToPrefab>c__AnonStorey177<System.Object,System.Object>
struct U3CToPrefabU3Ec__AnonStorey177_t3739000318;
// Zenject.IFactory`2<System.Object,System.Object>
struct IFactory_2_t972313871;
// Zenject.InjectContext
struct InjectContext_t3456483891;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Zenject_InjectContext3456483891.h"

// System.Void Zenject.IFactoryBinder`2/<ToPrefab>c__AnonStorey177<System.Object,System.Object>::.ctor()
extern "C"  void U3CToPrefabU3Ec__AnonStorey177__ctor_m3867410545_gshared (U3CToPrefabU3Ec__AnonStorey177_t3739000318 * __this, const MethodInfo* method);
#define U3CToPrefabU3Ec__AnonStorey177__ctor_m3867410545(__this, method) ((  void (*) (U3CToPrefabU3Ec__AnonStorey177_t3739000318 *, const MethodInfo*))U3CToPrefabU3Ec__AnonStorey177__ctor_m3867410545_gshared)(__this, method)
// Zenject.IFactory`2<TParam1,TContract> Zenject.IFactoryBinder`2/<ToPrefab>c__AnonStorey177<System.Object,System.Object>::<>m__29B(Zenject.InjectContext)
extern "C"  Il2CppObject* U3CToPrefabU3Ec__AnonStorey177_U3CU3Em__29B_m1569127859_gshared (U3CToPrefabU3Ec__AnonStorey177_t3739000318 * __this, InjectContext_t3456483891 * ___ctx0, const MethodInfo* method);
#define U3CToPrefabU3Ec__AnonStorey177_U3CU3Em__29B_m1569127859(__this, ___ctx0, method) ((  Il2CppObject* (*) (U3CToPrefabU3Ec__AnonStorey177_t3739000318 *, InjectContext_t3456483891 *, const MethodInfo*))U3CToPrefabU3Ec__AnonStorey177_U3CU3Em__29B_m1569127859_gshared)(__this, ___ctx0, method)
