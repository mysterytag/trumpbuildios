﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Reactor_1_gen96911316MethodDeclarations.h"

// System.Void Reactor`1<CollectionReplaceEvent`1<UpgradeButton>>::.ctor()
#define Reactor_1__ctor_m1358959627(__this, method) ((  void (*) (Reactor_1_t2395537888 *, const MethodInfo*))Reactor_1__ctor_m1161914514_gshared)(__this, method)
// System.Void Reactor`1<CollectionReplaceEvent`1<UpgradeButton>>::React(T)
#define Reactor_1_React_m3009737590(__this, ___t0, method) ((  void (*) (Reactor_1_t2395537888 *, CollectionReplaceEvent_1_t3135732992 *, const MethodInfo*))Reactor_1_React_m1196306383_gshared)(__this, ___t0, method)
// System.Void Reactor`1<CollectionReplaceEvent`1<UpgradeButton>>::TransactionUnpackReact(T,System.Int32)
#define Reactor_1_TransactionUnpackReact_m3519869697(__this, ___t0, ___i1, method) ((  void (*) (Reactor_1_t2395537888 *, CollectionReplaceEvent_1_t3135732992 *, int32_t, const MethodInfo*))Reactor_1_TransactionUnpackReact_m1003670938_gshared)(__this, ___t0, ___i1, method)
// System.Boolean Reactor`1<CollectionReplaceEvent`1<UpgradeButton>>::Empty()
#define Reactor_1_Empty_m368455682(__this, method) ((  bool (*) (Reactor_1_t2395537888 *, const MethodInfo*))Reactor_1_Empty_m132443017_gshared)(__this, method)
// System.IDisposable Reactor`1<CollectionReplaceEvent`1<UpgradeButton>>::AddReaction(System.Action`1<T>,Priority)
#define Reactor_1_AddReaction_m4251216866(__this, ___reaction0, ___priority1, method) ((  Il2CppObject * (*) (Reactor_1_t2395537888 *, Action_1_t3284185697 *, int32_t, const MethodInfo*))Reactor_1_AddReaction_m1495770619_gshared)(__this, ___reaction0, ___priority1, method)
// System.Void Reactor`1<CollectionReplaceEvent`1<UpgradeButton>>::UpgradeToMultyPriority()
#define Reactor_1_UpgradeToMultyPriority_m3110770159(__this, method) ((  void (*) (Reactor_1_t2395537888 *, const MethodInfo*))Reactor_1_UpgradeToMultyPriority_m912916552_gshared)(__this, method)
// System.Void Reactor`1<CollectionReplaceEvent`1<UpgradeButton>>::AddToMultipriority(System.Action`1<T>,Priority)
#define Reactor_1_AddToMultipriority_m867583080(__this, ___reaction0, ___p1, method) ((  void (*) (Reactor_1_t2395537888 *, Action_1_t3284185697 *, int32_t, const MethodInfo*))Reactor_1_AddToMultipriority_m3381282287_gshared)(__this, ___reaction0, ___p1, method)
// System.Void Reactor`1<CollectionReplaceEvent`1<UpgradeButton>>::RemoveReaction(System.Action`1<T>,Priority)
#define Reactor_1_RemoveReaction_m3847082332(__this, ___reaction0, ___p1, method) ((  void (*) (Reactor_1_t2395537888 *, Action_1_t3284185697 *, int32_t, const MethodInfo*))Reactor_1_RemoveReaction_m4227182179_gshared)(__this, ___reaction0, ___p1, method)
// System.Void Reactor`1<CollectionReplaceEvent`1<UpgradeButton>>::Clear()
#define Reactor_1_Clear_m3060060214(__this, method) ((  void (*) (Reactor_1_t2395537888 *, const MethodInfo*))Reactor_1_Clear_m2863015101_gshared)(__this, method)
