﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.FromEventObservable`2/FromEvent<System.Object,System.Single>
struct FromEvent_t2663068496;
// UniRx.Operators.FromEventObservable`2<System.Object,System.Single>
struct FromEventObservable_2_t2373242198;
// UniRx.IObserver`1<System.Single>
struct IObserver_1_t3170207924;

#include "codegen/il2cpp-codegen.h"

// System.Void UniRx.Operators.FromEventObservable`2/FromEvent<System.Object,System.Single>::.ctor(UniRx.Operators.FromEventObservable`2<TDelegate,TEventArgs>,UniRx.IObserver`1<TEventArgs>)
extern "C"  void FromEvent__ctor_m2246157456_gshared (FromEvent_t2663068496 * __this, FromEventObservable_2_t2373242198 * ___parent0, Il2CppObject* ___observer1, const MethodInfo* method);
#define FromEvent__ctor_m2246157456(__this, ___parent0, ___observer1, method) ((  void (*) (FromEvent_t2663068496 *, FromEventObservable_2_t2373242198 *, Il2CppObject*, const MethodInfo*))FromEvent__ctor_m2246157456_gshared)(__this, ___parent0, ___observer1, method)
// System.Boolean UniRx.Operators.FromEventObservable`2/FromEvent<System.Object,System.Single>::Register()
extern "C"  bool FromEvent_Register_m3431158950_gshared (FromEvent_t2663068496 * __this, const MethodInfo* method);
#define FromEvent_Register_m3431158950(__this, method) ((  bool (*) (FromEvent_t2663068496 *, const MethodInfo*))FromEvent_Register_m3431158950_gshared)(__this, method)
// System.Void UniRx.Operators.FromEventObservable`2/FromEvent<System.Object,System.Single>::OnNext(TEventArgs)
extern "C"  void FromEvent_OnNext_m636300118_gshared (FromEvent_t2663068496 * __this, float ___args0, const MethodInfo* method);
#define FromEvent_OnNext_m636300118(__this, ___args0, method) ((  void (*) (FromEvent_t2663068496 *, float, const MethodInfo*))FromEvent_OnNext_m636300118_gshared)(__this, ___args0, method)
// System.Void UniRx.Operators.FromEventObservable`2/FromEvent<System.Object,System.Single>::Dispose()
extern "C"  void FromEvent_Dispose_m3028455034_gshared (FromEvent_t2663068496 * __this, const MethodInfo* method);
#define FromEvent_Dispose_m3028455034(__this, method) ((  void (*) (FromEvent_t2663068496 *, const MethodInfo*))FromEvent_Dispose_m3028455034_gshared)(__this, method)
