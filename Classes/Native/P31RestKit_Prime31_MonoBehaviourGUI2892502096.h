﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Generic.Dictionary`2<System.String,System.Boolean>
struct Dictionary_2_t1848703245;
// System.Text.StringBuilder
struct StringBuilder_t3822575854;
// UnityEngine.Texture2D
struct Texture2D_t2509538522;

#include "UnityEngine_UnityEngine_MonoBehaviour3012272455.h"
#include "UnityEngine_UnityEngine_Vector23525329788.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Prime31.MonoBehaviourGUI
struct  MonoBehaviourGUI_t2892502096  : public MonoBehaviour_t3012272455
{
public:
	// System.Single Prime31.MonoBehaviourGUI::_width
	float ____width_2;
	// System.Single Prime31.MonoBehaviourGUI::_buttonHeight
	float ____buttonHeight_3;
	// System.Collections.Generic.Dictionary`2<System.String,System.Boolean> Prime31.MonoBehaviourGUI::_toggleButtons
	Dictionary_2_t1848703245 * ____toggleButtons_4;
	// System.Text.StringBuilder Prime31.MonoBehaviourGUI::_logBuilder
	StringBuilder_t3822575854 * ____logBuilder_5;
	// System.Boolean Prime31.MonoBehaviourGUI::_logRegistered
	bool ____logRegistered_6;
	// UnityEngine.Vector2 Prime31.MonoBehaviourGUI::_logScrollPosition
	Vector2_t3525329788  ____logScrollPosition_7;
	// System.Boolean Prime31.MonoBehaviourGUI::_isShowingLogConsole
	bool ____isShowingLogConsole_8;
	// System.Single Prime31.MonoBehaviourGUI::_doubleClickDelay
	float ____doubleClickDelay_9;
	// System.Single Prime31.MonoBehaviourGUI::_previousClickTime
	float ____previousClickTime_10;
	// System.Single Prime31.MonoBehaviourGUI::_lastTwoFingerTouchTime
	float ____lastTwoFingerTouchTime_11;
	// System.Boolean Prime31.MonoBehaviourGUI::_isWindowsPhone
	bool ____isWindowsPhone_12;
	// UnityEngine.Texture2D Prime31.MonoBehaviourGUI::_normalBackground
	Texture2D_t2509538522 * ____normalBackground_13;
	// UnityEngine.Texture2D Prime31.MonoBehaviourGUI::_bottomButtonBackground
	Texture2D_t2509538522 * ____bottomButtonBackground_14;
	// UnityEngine.Texture2D Prime31.MonoBehaviourGUI::_activeBackground
	Texture2D_t2509538522 * ____activeBackground_15;
	// UnityEngine.Texture2D Prime31.MonoBehaviourGUI::_toggleButtonBackground
	Texture2D_t2509538522 * ____toggleButtonBackground_16;
	// System.Boolean Prime31.MonoBehaviourGUI::_didRetinaIpadCheck
	bool ____didRetinaIpadCheck_17;
	// System.Boolean Prime31.MonoBehaviourGUI::_isRetinaIpad
	bool ____isRetinaIpad_18;

public:
	inline static int32_t get_offset_of__width_2() { return static_cast<int32_t>(offsetof(MonoBehaviourGUI_t2892502096, ____width_2)); }
	inline float get__width_2() const { return ____width_2; }
	inline float* get_address_of__width_2() { return &____width_2; }
	inline void set__width_2(float value)
	{
		____width_2 = value;
	}

	inline static int32_t get_offset_of__buttonHeight_3() { return static_cast<int32_t>(offsetof(MonoBehaviourGUI_t2892502096, ____buttonHeight_3)); }
	inline float get__buttonHeight_3() const { return ____buttonHeight_3; }
	inline float* get_address_of__buttonHeight_3() { return &____buttonHeight_3; }
	inline void set__buttonHeight_3(float value)
	{
		____buttonHeight_3 = value;
	}

	inline static int32_t get_offset_of__toggleButtons_4() { return static_cast<int32_t>(offsetof(MonoBehaviourGUI_t2892502096, ____toggleButtons_4)); }
	inline Dictionary_2_t1848703245 * get__toggleButtons_4() const { return ____toggleButtons_4; }
	inline Dictionary_2_t1848703245 ** get_address_of__toggleButtons_4() { return &____toggleButtons_4; }
	inline void set__toggleButtons_4(Dictionary_2_t1848703245 * value)
	{
		____toggleButtons_4 = value;
		Il2CppCodeGenWriteBarrier(&____toggleButtons_4, value);
	}

	inline static int32_t get_offset_of__logBuilder_5() { return static_cast<int32_t>(offsetof(MonoBehaviourGUI_t2892502096, ____logBuilder_5)); }
	inline StringBuilder_t3822575854 * get__logBuilder_5() const { return ____logBuilder_5; }
	inline StringBuilder_t3822575854 ** get_address_of__logBuilder_5() { return &____logBuilder_5; }
	inline void set__logBuilder_5(StringBuilder_t3822575854 * value)
	{
		____logBuilder_5 = value;
		Il2CppCodeGenWriteBarrier(&____logBuilder_5, value);
	}

	inline static int32_t get_offset_of__logRegistered_6() { return static_cast<int32_t>(offsetof(MonoBehaviourGUI_t2892502096, ____logRegistered_6)); }
	inline bool get__logRegistered_6() const { return ____logRegistered_6; }
	inline bool* get_address_of__logRegistered_6() { return &____logRegistered_6; }
	inline void set__logRegistered_6(bool value)
	{
		____logRegistered_6 = value;
	}

	inline static int32_t get_offset_of__logScrollPosition_7() { return static_cast<int32_t>(offsetof(MonoBehaviourGUI_t2892502096, ____logScrollPosition_7)); }
	inline Vector2_t3525329788  get__logScrollPosition_7() const { return ____logScrollPosition_7; }
	inline Vector2_t3525329788 * get_address_of__logScrollPosition_7() { return &____logScrollPosition_7; }
	inline void set__logScrollPosition_7(Vector2_t3525329788  value)
	{
		____logScrollPosition_7 = value;
	}

	inline static int32_t get_offset_of__isShowingLogConsole_8() { return static_cast<int32_t>(offsetof(MonoBehaviourGUI_t2892502096, ____isShowingLogConsole_8)); }
	inline bool get__isShowingLogConsole_8() const { return ____isShowingLogConsole_8; }
	inline bool* get_address_of__isShowingLogConsole_8() { return &____isShowingLogConsole_8; }
	inline void set__isShowingLogConsole_8(bool value)
	{
		____isShowingLogConsole_8 = value;
	}

	inline static int32_t get_offset_of__doubleClickDelay_9() { return static_cast<int32_t>(offsetof(MonoBehaviourGUI_t2892502096, ____doubleClickDelay_9)); }
	inline float get__doubleClickDelay_9() const { return ____doubleClickDelay_9; }
	inline float* get_address_of__doubleClickDelay_9() { return &____doubleClickDelay_9; }
	inline void set__doubleClickDelay_9(float value)
	{
		____doubleClickDelay_9 = value;
	}

	inline static int32_t get_offset_of__previousClickTime_10() { return static_cast<int32_t>(offsetof(MonoBehaviourGUI_t2892502096, ____previousClickTime_10)); }
	inline float get__previousClickTime_10() const { return ____previousClickTime_10; }
	inline float* get_address_of__previousClickTime_10() { return &____previousClickTime_10; }
	inline void set__previousClickTime_10(float value)
	{
		____previousClickTime_10 = value;
	}

	inline static int32_t get_offset_of__lastTwoFingerTouchTime_11() { return static_cast<int32_t>(offsetof(MonoBehaviourGUI_t2892502096, ____lastTwoFingerTouchTime_11)); }
	inline float get__lastTwoFingerTouchTime_11() const { return ____lastTwoFingerTouchTime_11; }
	inline float* get_address_of__lastTwoFingerTouchTime_11() { return &____lastTwoFingerTouchTime_11; }
	inline void set__lastTwoFingerTouchTime_11(float value)
	{
		____lastTwoFingerTouchTime_11 = value;
	}

	inline static int32_t get_offset_of__isWindowsPhone_12() { return static_cast<int32_t>(offsetof(MonoBehaviourGUI_t2892502096, ____isWindowsPhone_12)); }
	inline bool get__isWindowsPhone_12() const { return ____isWindowsPhone_12; }
	inline bool* get_address_of__isWindowsPhone_12() { return &____isWindowsPhone_12; }
	inline void set__isWindowsPhone_12(bool value)
	{
		____isWindowsPhone_12 = value;
	}

	inline static int32_t get_offset_of__normalBackground_13() { return static_cast<int32_t>(offsetof(MonoBehaviourGUI_t2892502096, ____normalBackground_13)); }
	inline Texture2D_t2509538522 * get__normalBackground_13() const { return ____normalBackground_13; }
	inline Texture2D_t2509538522 ** get_address_of__normalBackground_13() { return &____normalBackground_13; }
	inline void set__normalBackground_13(Texture2D_t2509538522 * value)
	{
		____normalBackground_13 = value;
		Il2CppCodeGenWriteBarrier(&____normalBackground_13, value);
	}

	inline static int32_t get_offset_of__bottomButtonBackground_14() { return static_cast<int32_t>(offsetof(MonoBehaviourGUI_t2892502096, ____bottomButtonBackground_14)); }
	inline Texture2D_t2509538522 * get__bottomButtonBackground_14() const { return ____bottomButtonBackground_14; }
	inline Texture2D_t2509538522 ** get_address_of__bottomButtonBackground_14() { return &____bottomButtonBackground_14; }
	inline void set__bottomButtonBackground_14(Texture2D_t2509538522 * value)
	{
		____bottomButtonBackground_14 = value;
		Il2CppCodeGenWriteBarrier(&____bottomButtonBackground_14, value);
	}

	inline static int32_t get_offset_of__activeBackground_15() { return static_cast<int32_t>(offsetof(MonoBehaviourGUI_t2892502096, ____activeBackground_15)); }
	inline Texture2D_t2509538522 * get__activeBackground_15() const { return ____activeBackground_15; }
	inline Texture2D_t2509538522 ** get_address_of__activeBackground_15() { return &____activeBackground_15; }
	inline void set__activeBackground_15(Texture2D_t2509538522 * value)
	{
		____activeBackground_15 = value;
		Il2CppCodeGenWriteBarrier(&____activeBackground_15, value);
	}

	inline static int32_t get_offset_of__toggleButtonBackground_16() { return static_cast<int32_t>(offsetof(MonoBehaviourGUI_t2892502096, ____toggleButtonBackground_16)); }
	inline Texture2D_t2509538522 * get__toggleButtonBackground_16() const { return ____toggleButtonBackground_16; }
	inline Texture2D_t2509538522 ** get_address_of__toggleButtonBackground_16() { return &____toggleButtonBackground_16; }
	inline void set__toggleButtonBackground_16(Texture2D_t2509538522 * value)
	{
		____toggleButtonBackground_16 = value;
		Il2CppCodeGenWriteBarrier(&____toggleButtonBackground_16, value);
	}

	inline static int32_t get_offset_of__didRetinaIpadCheck_17() { return static_cast<int32_t>(offsetof(MonoBehaviourGUI_t2892502096, ____didRetinaIpadCheck_17)); }
	inline bool get__didRetinaIpadCheck_17() const { return ____didRetinaIpadCheck_17; }
	inline bool* get_address_of__didRetinaIpadCheck_17() { return &____didRetinaIpadCheck_17; }
	inline void set__didRetinaIpadCheck_17(bool value)
	{
		____didRetinaIpadCheck_17 = value;
	}

	inline static int32_t get_offset_of__isRetinaIpad_18() { return static_cast<int32_t>(offsetof(MonoBehaviourGUI_t2892502096, ____isRetinaIpad_18)); }
	inline bool get__isRetinaIpad_18() const { return ____isRetinaIpad_18; }
	inline bool* get_address_of__isRetinaIpad_18() { return &____isRetinaIpad_18; }
	inline void set__isRetinaIpad_18(bool value)
	{
		____isRetinaIpad_18 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
