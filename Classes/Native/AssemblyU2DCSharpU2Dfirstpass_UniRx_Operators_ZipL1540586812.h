﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UniRx.IObservable`1<System.Object>
struct IObservable_1_t595904784;
// System.Func`3<System.Object,System.Object,System.Object>
struct Func_3_t1892209229;

#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Oper4196218687.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Operators.ZipLatestObservable`3<System.Object,System.Object,System.Object>
struct  ZipLatestObservable_3_t1540586812  : public OperatorObservableBase_1_t4196218687
{
public:
	// UniRx.IObservable`1<TLeft> UniRx.Operators.ZipLatestObservable`3::left
	Il2CppObject* ___left_1;
	// UniRx.IObservable`1<TRight> UniRx.Operators.ZipLatestObservable`3::right
	Il2CppObject* ___right_2;
	// System.Func`3<TLeft,TRight,TResult> UniRx.Operators.ZipLatestObservable`3::selector
	Func_3_t1892209229 * ___selector_3;

public:
	inline static int32_t get_offset_of_left_1() { return static_cast<int32_t>(offsetof(ZipLatestObservable_3_t1540586812, ___left_1)); }
	inline Il2CppObject* get_left_1() const { return ___left_1; }
	inline Il2CppObject** get_address_of_left_1() { return &___left_1; }
	inline void set_left_1(Il2CppObject* value)
	{
		___left_1 = value;
		Il2CppCodeGenWriteBarrier(&___left_1, value);
	}

	inline static int32_t get_offset_of_right_2() { return static_cast<int32_t>(offsetof(ZipLatestObservable_3_t1540586812, ___right_2)); }
	inline Il2CppObject* get_right_2() const { return ___right_2; }
	inline Il2CppObject** get_address_of_right_2() { return &___right_2; }
	inline void set_right_2(Il2CppObject* value)
	{
		___right_2 = value;
		Il2CppCodeGenWriteBarrier(&___right_2, value);
	}

	inline static int32_t get_offset_of_selector_3() { return static_cast<int32_t>(offsetof(ZipLatestObservable_3_t1540586812, ___selector_3)); }
	inline Func_3_t1892209229 * get_selector_3() const { return ___selector_3; }
	inline Func_3_t1892209229 ** get_address_of_selector_3() { return &___selector_3; }
	inline void set_selector_3(Func_3_t1892209229 * value)
	{
		___selector_3 = value;
		Il2CppCodeGenWriteBarrier(&___selector_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
