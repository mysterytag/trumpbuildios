﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"


// Methods for marshaling
struct U24ArrayTypeU246144_t336100496;
struct U24ArrayTypeU246144_t336100496_marshaled_pinvoke;

extern "C" void U24ArrayTypeU246144_t336100496_marshal_pinvoke(const U24ArrayTypeU246144_t336100496& unmarshaled, U24ArrayTypeU246144_t336100496_marshaled_pinvoke& marshaled);
extern "C" void U24ArrayTypeU246144_t336100496_marshal_pinvoke_back(const U24ArrayTypeU246144_t336100496_marshaled_pinvoke& marshaled, U24ArrayTypeU246144_t336100496& unmarshaled);
extern "C" void U24ArrayTypeU246144_t336100496_marshal_pinvoke_cleanup(U24ArrayTypeU246144_t336100496_marshaled_pinvoke& marshaled);

// Methods for marshaling
struct U24ArrayTypeU246144_t336100496;
struct U24ArrayTypeU246144_t336100496_marshaled_com;

extern "C" void U24ArrayTypeU246144_t336100496_marshal_com(const U24ArrayTypeU246144_t336100496& unmarshaled, U24ArrayTypeU246144_t336100496_marshaled_com& marshaled);
extern "C" void U24ArrayTypeU246144_t336100496_marshal_com_back(const U24ArrayTypeU246144_t336100496_marshaled_com& marshaled, U24ArrayTypeU246144_t336100496& unmarshaled);
extern "C" void U24ArrayTypeU246144_t336100496_marshal_com_cleanup(U24ArrayTypeU246144_t336100496_marshaled_com& marshaled);
