﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GameAnalyticsSDK.GA_MiniJSON
struct GA_MiniJSON_t4072382662;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;
// System.Collections.Hashtable
struct Hashtable_t3875263730;
// System.Char[]
struct CharU5BU5D_t3416858730;
// System.Collections.ArrayList
struct ArrayList_t2121638921;
// System.Text.StringBuilder
struct StringBuilder_t3822575854;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String968488902.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_Text_StringBuilder3822575854.h"
#include "mscorlib_System_Collections_Hashtable3875263730.h"
#include "mscorlib_System_Collections_ArrayList2121638921.h"

// System.Void GameAnalyticsSDK.GA_MiniJSON::.ctor()
extern "C"  void GA_MiniJSON__ctor_m3241179231 (GA_MiniJSON_t4072382662 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameAnalyticsSDK.GA_MiniJSON::.cctor()
extern "C"  void GA_MiniJSON__cctor_m1210212142 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object GameAnalyticsSDK.GA_MiniJSON::JsonDecode(System.String)
extern "C"  Il2CppObject * GA_MiniJSON_JsonDecode_m3497298194 (Il2CppObject * __this /* static, unused */, String_t* ___json0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String GameAnalyticsSDK.GA_MiniJSON::JsonEncode(System.Object)
extern "C"  String_t* GA_MiniJSON_JsonEncode_m349915470 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___json0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GameAnalyticsSDK.GA_MiniJSON::LastDecodeSuccessful()
extern "C"  bool GA_MiniJSON_LastDecodeSuccessful_m1739674647 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 GameAnalyticsSDK.GA_MiniJSON::GetLastErrorIndex()
extern "C"  int32_t GA_MiniJSON_GetLastErrorIndex_m563969413 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String GameAnalyticsSDK.GA_MiniJSON::GetLastErrorSnippet()
extern "C"  String_t* GA_MiniJSON_GetLastErrorSnippet_m2771135579 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Hashtable GameAnalyticsSDK.GA_MiniJSON::ParseObject(System.Char[],System.Int32&)
extern "C"  Hashtable_t3875263730 * GA_MiniJSON_ParseObject_m2953559510 (GA_MiniJSON_t4072382662 * __this, CharU5BU5D_t3416858730* ___json0, int32_t* ___index1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.ArrayList GameAnalyticsSDK.GA_MiniJSON::ParseArray(System.Char[],System.Int32&)
extern "C"  ArrayList_t2121638921 * GA_MiniJSON_ParseArray_m3369870343 (GA_MiniJSON_t4072382662 * __this, CharU5BU5D_t3416858730* ___json0, int32_t* ___index1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object GameAnalyticsSDK.GA_MiniJSON::ParseValue(System.Char[],System.Int32&,System.Boolean&)
extern "C"  Il2CppObject * GA_MiniJSON_ParseValue_m3502293703 (GA_MiniJSON_t4072382662 * __this, CharU5BU5D_t3416858730* ___json0, int32_t* ___index1, bool* ___success2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String GameAnalyticsSDK.GA_MiniJSON::ParseString(System.Char[],System.Int32&)
extern "C"  String_t* GA_MiniJSON_ParseString_m1972076658 (GA_MiniJSON_t4072382662 * __this, CharU5BU5D_t3416858730* ___json0, int32_t* ___index1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single GameAnalyticsSDK.GA_MiniJSON::ParseNumber(System.Char[],System.Int32&)
extern "C"  float GA_MiniJSON_ParseNumber_m1608296483 (GA_MiniJSON_t4072382662 * __this, CharU5BU5D_t3416858730* ___json0, int32_t* ___index1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 GameAnalyticsSDK.GA_MiniJSON::GetLastIndexOfNumber(System.Char[],System.Int32)
extern "C"  int32_t GA_MiniJSON_GetLastIndexOfNumber_m1957122709 (GA_MiniJSON_t4072382662 * __this, CharU5BU5D_t3416858730* ___json0, int32_t ___index1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameAnalyticsSDK.GA_MiniJSON::EatWhitespace(System.Char[],System.Int32&)
extern "C"  void GA_MiniJSON_EatWhitespace_m1474079198 (GA_MiniJSON_t4072382662 * __this, CharU5BU5D_t3416858730* ___json0, int32_t* ___index1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 GameAnalyticsSDK.GA_MiniJSON::LookAhead(System.Char[],System.Int32)
extern "C"  int32_t GA_MiniJSON_LookAhead_m834174413 (GA_MiniJSON_t4072382662 * __this, CharU5BU5D_t3416858730* ___json0, int32_t ___index1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 GameAnalyticsSDK.GA_MiniJSON::NextToken(System.Char[],System.Int32&)
extern "C"  int32_t GA_MiniJSON_NextToken_m1024127419 (GA_MiniJSON_t4072382662 * __this, CharU5BU5D_t3416858730* ___json0, int32_t* ___index1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GameAnalyticsSDK.GA_MiniJSON::SerializeObjectOrArray(System.Object,System.Text.StringBuilder)
extern "C"  bool GA_MiniJSON_SerializeObjectOrArray_m3376437192 (GA_MiniJSON_t4072382662 * __this, Il2CppObject * ___objectOrArray0, StringBuilder_t3822575854 * ___builder1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GameAnalyticsSDK.GA_MiniJSON::SerializeObject(System.Collections.Hashtable,System.Text.StringBuilder)
extern "C"  bool GA_MiniJSON_SerializeObject_m2476376740 (GA_MiniJSON_t4072382662 * __this, Hashtable_t3875263730 * ___anObject0, StringBuilder_t3822575854 * ___builder1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GameAnalyticsSDK.GA_MiniJSON::SerializeArray(System.Collections.ArrayList,System.Text.StringBuilder)
extern "C"  bool GA_MiniJSON_SerializeArray_m2966192823 (GA_MiniJSON_t4072382662 * __this, ArrayList_t2121638921 * ___anArray0, StringBuilder_t3822575854 * ___builder1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GameAnalyticsSDK.GA_MiniJSON::SerializeValue(System.Object,System.Text.StringBuilder)
extern "C"  bool GA_MiniJSON_SerializeValue_m578488718 (GA_MiniJSON_t4072382662 * __this, Il2CppObject * ___value0, StringBuilder_t3822575854 * ___builder1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameAnalyticsSDK.GA_MiniJSON::SerializeString(System.String,System.Text.StringBuilder)
extern "C"  void GA_MiniJSON_SerializeString_m1578143128 (GA_MiniJSON_t4072382662 * __this, String_t* ___aString0, StringBuilder_t3822575854 * ___builder1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameAnalyticsSDK.GA_MiniJSON::SerializeNumber(System.Single,System.Text.StringBuilder)
extern "C"  void GA_MiniJSON_SerializeNumber_m1363542729 (GA_MiniJSON_t4072382662 * __this, float ___number0, StringBuilder_t3822575854 * ___builder1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
