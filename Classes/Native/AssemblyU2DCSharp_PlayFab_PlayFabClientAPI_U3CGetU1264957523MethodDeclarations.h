﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.PlayFabClientAPI/<GetUserInventory>c__AnonStoreyA4
struct U3CGetUserInventoryU3Ec__AnonStoreyA4_t1264957523;
// PlayFab.CallRequestContainer
struct CallRequestContainer_t432318609;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharpU2Dfirstpass_PlayFab_CallRequestCo432318609.h"

// System.Void PlayFab.PlayFabClientAPI/<GetUserInventory>c__AnonStoreyA4::.ctor()
extern "C"  void U3CGetUserInventoryU3Ec__AnonStoreyA4__ctor_m1869562048 (U3CGetUserInventoryU3Ec__AnonStoreyA4_t1264957523 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/<GetUserInventory>c__AnonStoreyA4::<>m__91(PlayFab.CallRequestContainer)
extern "C"  void U3CGetUserInventoryU3Ec__AnonStoreyA4_U3CU3Em__91_m1281166518 (U3CGetUserInventoryU3Ec__AnonStoreyA4_t1264957523 * __this, CallRequestContainer_t432318609 * ___requestContainer0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
