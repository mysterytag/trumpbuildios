﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// AudioController
struct AudioController_t2316845042;

#include "codegen/il2cpp-codegen.h"

// System.Void AudioController::.ctor()
extern "C"  void AudioController__ctor_m592664505 (AudioController_t2316845042 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AudioController::Update()
extern "C"  void AudioController_Update_m2919592564 (AudioController_t2316845042 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AudioController::PlayWooshSound()
extern "C"  void AudioController_PlayWooshSound_m352406978 (AudioController_t2316845042 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AudioController::PlayMultiplierSound()
extern "C"  void AudioController_PlayMultiplierSound_m4279656913 (AudioController_t2316845042 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AudioController::PlayMmmSound()
extern "C"  void AudioController_PlayMmmSound_m1819134081 (AudioController_t2316845042 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AudioController::PlayMmm5sCountdownSound()
extern "C"  void AudioController_PlayMmm5sCountdownSound_m3005372140 (AudioController_t2316845042 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AudioController::PlayMmm60sCountdownSound()
extern "C"  void AudioController_PlayMmm60sCountdownSound_m2909100745 (AudioController_t2316845042 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AudioController::PlayPurchaseSound()
extern "C"  void AudioController_PlayPurchaseSound_m993487633 (AudioController_t2316845042 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AudioController::SetBoostMusicVolume(System.Single)
extern "C"  void AudioController_SetBoostMusicVolume_m3580014198 (AudioController_t2316845042 * __this, float ___boost0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
