﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.ClientModels.UnlinkFacebookAccountRequest
struct UnlinkFacebookAccountRequest_t3175165483;

#include "codegen/il2cpp-codegen.h"

// System.Void PlayFab.ClientModels.UnlinkFacebookAccountRequest::.ctor()
extern "C"  void UnlinkFacebookAccountRequest__ctor_m2539541362 (UnlinkFacebookAccountRequest_t3175165483 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
