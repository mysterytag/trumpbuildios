﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Examples.Sample02_ObservableTriggers
struct Sample02_ObservableTriggers_t2916496460;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Unit2558286038.h"

// System.Void UniRx.Examples.Sample02_ObservableTriggers::.ctor()
extern "C"  void Sample02_ObservableTriggers__ctor_m4264090891 (Sample02_ObservableTriggers_t2916496460 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.Examples.Sample02_ObservableTriggers::Start()
extern "C"  void Sample02_ObservableTriggers_Start_m3211228683 (Sample02_ObservableTriggers_t2916496460 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.Examples.Sample02_ObservableTriggers::<Start>m__11(UniRx.Unit)
extern "C"  void Sample02_ObservableTriggers_U3CStartU3Em__11_m899926226 (Il2CppObject * __this /* static, unused */, Unit_t2558286038  ___x0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.Examples.Sample02_ObservableTriggers::<Start>m__12()
extern "C"  void Sample02_ObservableTriggers_U3CStartU3Em__12_m602884271 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
