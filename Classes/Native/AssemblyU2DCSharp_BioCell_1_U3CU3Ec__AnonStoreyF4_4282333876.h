﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Object
struct Il2CppObject;
// BioCell`1<System.Object>
struct BioCell_1_t661504124;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BioCell`1/<>c__AnonStoreyF4<System.Object>
struct  U3CU3Ec__AnonStoreyF4_t4282333876  : public Il2CppObject
{
public:
	// T BioCell`1/<>c__AnonStoreyF4::temp
	Il2CppObject * ___temp_0;
	// BioCell`1<T> BioCell`1/<>c__AnonStoreyF4::<>f__this
	BioCell_1_t661504124 * ___U3CU3Ef__this_1;

public:
	inline static int32_t get_offset_of_temp_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__AnonStoreyF4_t4282333876, ___temp_0)); }
	inline Il2CppObject * get_temp_0() const { return ___temp_0; }
	inline Il2CppObject ** get_address_of_temp_0() { return &___temp_0; }
	inline void set_temp_0(Il2CppObject * value)
	{
		___temp_0 = value;
		Il2CppCodeGenWriteBarrier(&___temp_0, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__this_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__AnonStoreyF4_t4282333876, ___U3CU3Ef__this_1)); }
	inline BioCell_1_t661504124 * get_U3CU3Ef__this_1() const { return ___U3CU3Ef__this_1; }
	inline BioCell_1_t661504124 ** get_address_of_U3CU3Ef__this_1() { return &___U3CU3Ef__this_1; }
	inline void set_U3CU3Ef__this_1(BioCell_1_t661504124 * value)
	{
		___U3CU3Ef__this_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__this_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
