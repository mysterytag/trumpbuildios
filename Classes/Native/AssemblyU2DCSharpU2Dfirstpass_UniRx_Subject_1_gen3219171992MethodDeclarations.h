﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Subject`1<UnityEngine.NetworkPlayer>
struct Subject_1_t3219171992;
// System.Exception
struct Exception_t1967233988;
// System.IDisposable
struct IDisposable_t1628921374;
// UniRx.IObserver`1<UnityEngine.NetworkPlayer>
struct IObserver_1_t3493136275;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Exception1967233988.h"
#include "UnityEngine_UnityEngine_NetworkPlayer1281137372.h"

// System.Void UniRx.Subject`1<UnityEngine.NetworkPlayer>::.ctor()
extern "C"  void Subject_1__ctor_m345597330_gshared (Subject_1_t3219171992 * __this, const MethodInfo* method);
#define Subject_1__ctor_m345597330(__this, method) ((  void (*) (Subject_1_t3219171992 *, const MethodInfo*))Subject_1__ctor_m345597330_gshared)(__this, method)
// System.Boolean UniRx.Subject`1<UnityEngine.NetworkPlayer>::get_HasObservers()
extern "C"  bool Subject_1_get_HasObservers_m2665549498_gshared (Subject_1_t3219171992 * __this, const MethodInfo* method);
#define Subject_1_get_HasObservers_m2665549498(__this, method) ((  bool (*) (Subject_1_t3219171992 *, const MethodInfo*))Subject_1_get_HasObservers_m2665549498_gshared)(__this, method)
// System.Void UniRx.Subject`1<UnityEngine.NetworkPlayer>::OnCompleted()
extern "C"  void Subject_1_OnCompleted_m4017267612_gshared (Subject_1_t3219171992 * __this, const MethodInfo* method);
#define Subject_1_OnCompleted_m4017267612(__this, method) ((  void (*) (Subject_1_t3219171992 *, const MethodInfo*))Subject_1_OnCompleted_m4017267612_gshared)(__this, method)
// System.Void UniRx.Subject`1<UnityEngine.NetworkPlayer>::OnError(System.Exception)
extern "C"  void Subject_1_OnError_m907594441_gshared (Subject_1_t3219171992 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method);
#define Subject_1_OnError_m907594441(__this, ___error0, method) ((  void (*) (Subject_1_t3219171992 *, Exception_t1967233988 *, const MethodInfo*))Subject_1_OnError_m907594441_gshared)(__this, ___error0, method)
// System.Void UniRx.Subject`1<UnityEngine.NetworkPlayer>::OnNext(T)
extern "C"  void Subject_1_OnNext_m4237255610_gshared (Subject_1_t3219171992 * __this, NetworkPlayer_t1281137372  ___value0, const MethodInfo* method);
#define Subject_1_OnNext_m4237255610(__this, ___value0, method) ((  void (*) (Subject_1_t3219171992 *, NetworkPlayer_t1281137372 , const MethodInfo*))Subject_1_OnNext_m4237255610_gshared)(__this, ___value0, method)
// System.IDisposable UniRx.Subject`1<UnityEngine.NetworkPlayer>::Subscribe(UniRx.IObserver`1<T>)
extern "C"  Il2CppObject * Subject_1_Subscribe_m1761827345_gshared (Subject_1_t3219171992 * __this, Il2CppObject* ___observer0, const MethodInfo* method);
#define Subject_1_Subscribe_m1761827345(__this, ___observer0, method) ((  Il2CppObject * (*) (Subject_1_t3219171992 *, Il2CppObject*, const MethodInfo*))Subject_1_Subscribe_m1761827345_gshared)(__this, ___observer0, method)
// System.Void UniRx.Subject`1<UnityEngine.NetworkPlayer>::Dispose()
extern "C"  void Subject_1_Dispose_m1304835407_gshared (Subject_1_t3219171992 * __this, const MethodInfo* method);
#define Subject_1_Dispose_m1304835407(__this, method) ((  void (*) (Subject_1_t3219171992 *, const MethodInfo*))Subject_1_Dispose_m1304835407_gshared)(__this, method)
// System.Void UniRx.Subject`1<UnityEngine.NetworkPlayer>::ThrowIfDisposed()
extern "C"  void Subject_1_ThrowIfDisposed_m587014104_gshared (Subject_1_t3219171992 * __this, const MethodInfo* method);
#define Subject_1_ThrowIfDisposed_m587014104(__this, method) ((  void (*) (Subject_1_t3219171992 *, const MethodInfo*))Subject_1_ThrowIfDisposed_m587014104_gshared)(__this, method)
// System.Boolean UniRx.Subject`1<UnityEngine.NetworkPlayer>::IsRequiredSubscribeOnCurrentThread()
extern "C"  bool Subject_1_IsRequiredSubscribeOnCurrentThread_m2628845873_gshared (Subject_1_t3219171992 * __this, const MethodInfo* method);
#define Subject_1_IsRequiredSubscribeOnCurrentThread_m2628845873(__this, method) ((  bool (*) (Subject_1_t3219171992 *, const MethodInfo*))Subject_1_IsRequiredSubscribeOnCurrentThread_m2628845873_gshared)(__this, method)
