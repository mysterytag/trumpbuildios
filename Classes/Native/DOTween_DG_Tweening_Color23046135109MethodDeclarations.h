﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "DOTween_DG_Tweening_Color23046135109.h"
#include "UnityEngine_UnityEngine_Color1588175760.h"

// System.Void DG.Tweening.Color2::.ctor(UnityEngine.Color,UnityEngine.Color)
extern "C"  void Color2__ctor_m2299536948 (Color2_t3046135109 * __this, Color_t1588175760  ___ca0, Color_t1588175760  ___cb1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// DG.Tweening.Color2 DG.Tweening.Color2::op_Addition(DG.Tweening.Color2,DG.Tweening.Color2)
extern "C"  Color2_t3046135109  Color2_op_Addition_m3115293858 (Il2CppObject * __this /* static, unused */, Color2_t3046135109  ___c10, Color2_t3046135109  ___c21, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// DG.Tweening.Color2 DG.Tweening.Color2::op_Subtraction(DG.Tweening.Color2,DG.Tweening.Color2)
extern "C"  Color2_t3046135109  Color2_op_Subtraction_m5333080 (Il2CppObject * __this /* static, unused */, Color2_t3046135109  ___c10, Color2_t3046135109  ___c21, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// DG.Tweening.Color2 DG.Tweening.Color2::op_Multiply(DG.Tweening.Color2,System.Single)
extern "C"  Color2_t3046135109  Color2_op_Multiply_m1321322934 (Il2CppObject * __this /* static, unused */, Color2_t3046135109  ___c10, float ___f1, const MethodInfo* method) IL2CPP_METHOD_ATTR;

// Methods for marshaling
struct Color2_t3046135109;
struct Color2_t3046135109_marshaled_pinvoke;

extern "C" void Color2_t3046135109_marshal_pinvoke(const Color2_t3046135109& unmarshaled, Color2_t3046135109_marshaled_pinvoke& marshaled);
extern "C" void Color2_t3046135109_marshal_pinvoke_back(const Color2_t3046135109_marshaled_pinvoke& marshaled, Color2_t3046135109& unmarshaled);
extern "C" void Color2_t3046135109_marshal_pinvoke_cleanup(Color2_t3046135109_marshaled_pinvoke& marshaled);

// Methods for marshaling
struct Color2_t3046135109;
struct Color2_t3046135109_marshaled_com;

extern "C" void Color2_t3046135109_marshal_com(const Color2_t3046135109& unmarshaled, Color2_t3046135109_marshaled_com& marshaled);
extern "C" void Color2_t3046135109_marshal_com_back(const Color2_t3046135109_marshaled_com& marshaled, Color2_t3046135109& unmarshaled);
extern "C" void Color2_t3046135109_marshal_com_cleanup(Color2_t3046135109_marshaled_com& marshaled);
