﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SubstanceBase`2/Intrusion<System.Boolean,System.Boolean>
struct Intrusion_t1917188776;

#include "codegen/il2cpp-codegen.h"

// System.Void SubstanceBase`2/Intrusion<System.Boolean,System.Boolean>::.ctor()
extern "C"  void Intrusion__ctor_m2367869752_gshared (Intrusion_t1917188776 * __this, const MethodInfo* method);
#define Intrusion__ctor_m2367869752(__this, method) ((  void (*) (Intrusion_t1917188776 *, const MethodInfo*))Intrusion__ctor_m2367869752_gshared)(__this, method)
