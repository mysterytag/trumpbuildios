﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.OperatorObserverBase`2<UniRx.CollectionReplaceEvent`1<System.Object>,UniRx.CollectionReplaceEvent`1<System.Object>>
struct OperatorObserverBase_2_t3791266949;
// UniRx.IObserver`1<UniRx.CollectionReplaceEvent`1<System.Object>>
struct IObserver_1_t4053749439;
// System.IDisposable
struct IDisposable_t1628921374;

#include "codegen/il2cpp-codegen.h"

// System.Void UniRx.Operators.OperatorObserverBase`2<UniRx.CollectionReplaceEvent`1<System.Object>,UniRx.CollectionReplaceEvent`1<System.Object>>::.ctor(UniRx.IObserver`1<TResult>,System.IDisposable)
extern "C"  void OperatorObserverBase_2__ctor_m1289283818_gshared (OperatorObserverBase_2_t3791266949 * __this, Il2CppObject* ___observer0, Il2CppObject * ___cancel1, const MethodInfo* method);
#define OperatorObserverBase_2__ctor_m1289283818(__this, ___observer0, ___cancel1, method) ((  void (*) (OperatorObserverBase_2_t3791266949 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))OperatorObserverBase_2__ctor_m1289283818_gshared)(__this, ___observer0, ___cancel1, method)
// System.Void UniRx.Operators.OperatorObserverBase`2<UniRx.CollectionReplaceEvent`1<System.Object>,UniRx.CollectionReplaceEvent`1<System.Object>>::Dispose()
extern "C"  void OperatorObserverBase_2_Dispose_m2423068480_gshared (OperatorObserverBase_2_t3791266949 * __this, const MethodInfo* method);
#define OperatorObserverBase_2_Dispose_m2423068480(__this, method) ((  void (*) (OperatorObserverBase_2_t3791266949 *, const MethodInfo*))OperatorObserverBase_2_Dispose_m2423068480_gshared)(__this, method)
