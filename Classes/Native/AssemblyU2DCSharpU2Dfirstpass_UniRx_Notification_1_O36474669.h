﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Exception
struct Exception_t1967233988;

#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Notification_1_g38356375.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Notification`1/OnErrorNotification<System.Object>
struct  OnErrorNotification_t36474669  : public Notification_1_t38356375
{
public:
	// System.Exception UniRx.Notification`1/OnErrorNotification::exception
	Exception_t1967233988 * ___exception_0;

public:
	inline static int32_t get_offset_of_exception_0() { return static_cast<int32_t>(offsetof(OnErrorNotification_t36474669, ___exception_0)); }
	inline Exception_t1967233988 * get_exception_0() const { return ___exception_0; }
	inline Exception_t1967233988 ** get_address_of_exception_0() { return &___exception_0; }
	inline void set_exception_0(Exception_t1967233988 * value)
	{
		___exception_0 = value;
		Il2CppCodeGenWriteBarrier(&___exception_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
