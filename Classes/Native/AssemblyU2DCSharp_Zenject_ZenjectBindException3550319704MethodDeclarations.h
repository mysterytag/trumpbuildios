﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Zenject.ZenjectBindException
struct ZenjectBindException_t3550319704;
// System.String
struct String_t;
// System.Exception
struct Exception_t1967233988;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String968488902.h"
#include "mscorlib_System_Exception1967233988.h"

// System.Void Zenject.ZenjectBindException::.ctor(System.String)
extern "C"  void ZenjectBindException__ctor_m3054227529 (ZenjectBindException_t3550319704 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Zenject.ZenjectBindException::.ctor(System.String,System.Exception)
extern "C"  void ZenjectBindException__ctor_m1767600141 (ZenjectBindException_t3550319704 * __this, String_t* ___message0, Exception_t1967233988 * ___innerException1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
