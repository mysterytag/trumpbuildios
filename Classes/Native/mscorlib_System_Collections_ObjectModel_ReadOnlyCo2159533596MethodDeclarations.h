﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.ObjectModel.ReadOnlyCollection`1<GameAnalyticsSDK.Settings/HelpTypes>
struct ReadOnlyCollection_1_t2159533596;
// System.Collections.Generic.IList`1<GameAnalyticsSDK.Settings/HelpTypes>
struct IList_1_t1162880562;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t287207039;
// System.Object
struct Il2CppObject;
// GameAnalyticsSDK.Settings/HelpTypes[]
struct HelpTypesU5BU5D_t2651985161;
// System.Collections.Generic.IEnumerator`1<GameAnalyticsSDK.Settings/HelpTypes>
struct IEnumerator_1_t479494696;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_GameAnalyticsSDK_Settings_HelpTy3291355544.h"
#include "mscorlib_System_Array2840145358.h"
#include "mscorlib_System_Object837106420.h"

// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<GameAnalyticsSDK.Settings/HelpTypes>::.ctor(System.Collections.Generic.IList`1<T>)
extern "C"  void ReadOnlyCollection_1__ctor_m2179297340_gshared (ReadOnlyCollection_1_t2159533596 * __this, Il2CppObject* ___list0, const MethodInfo* method);
#define ReadOnlyCollection_1__ctor_m2179297340(__this, ___list0, method) ((  void (*) (ReadOnlyCollection_1_t2159533596 *, Il2CppObject*, const MethodInfo*))ReadOnlyCollection_1__ctor_m2179297340_gshared)(__this, ___list0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<GameAnalyticsSDK.Settings/HelpTypes>::System.Collections.Generic.ICollection<T>.Add(T)
extern "C"  void ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m719266854_gshared (ReadOnlyCollection_1_t2159533596 * __this, int32_t ___item0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m719266854(__this, ___item0, method) ((  void (*) (ReadOnlyCollection_1_t2159533596 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m719266854_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<GameAnalyticsSDK.Settings/HelpTypes>::System.Collections.Generic.ICollection<T>.Clear()
extern "C"  void ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m2829127012_gshared (ReadOnlyCollection_1_t2159533596 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m2829127012(__this, method) ((  void (*) (ReadOnlyCollection_1_t2159533596 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m2829127012_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<GameAnalyticsSDK.Settings/HelpTypes>::System.Collections.Generic.IList<T>.Insert(System.Int32,T)
extern "C"  void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m1152462733_gshared (ReadOnlyCollection_1_t2159533596 * __this, int32_t ___index0, int32_t ___item1, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m1152462733(__this, ___index0, ___item1, method) ((  void (*) (ReadOnlyCollection_1_t2159533596 *, int32_t, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m1152462733_gshared)(__this, ___index0, ___item1, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<GameAnalyticsSDK.Settings/HelpTypes>::System.Collections.Generic.ICollection<T>.Remove(T)
extern "C"  bool ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m3384742285_gshared (ReadOnlyCollection_1_t2159533596 * __this, int32_t ___item0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m3384742285(__this, ___item0, method) ((  bool (*) (ReadOnlyCollection_1_t2159533596 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m3384742285_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<GameAnalyticsSDK.Settings/HelpTypes>::System.Collections.Generic.IList<T>.RemoveAt(System.Int32)
extern "C"  void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m3321282899_gshared (ReadOnlyCollection_1_t2159533596 * __this, int32_t ___index0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m3321282899(__this, ___index0, method) ((  void (*) (ReadOnlyCollection_1_t2159533596 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m3321282899_gshared)(__this, ___index0, method)
// T System.Collections.ObjectModel.ReadOnlyCollection`1<GameAnalyticsSDK.Settings/HelpTypes>::System.Collections.Generic.IList<T>.get_Item(System.Int32)
extern "C"  int32_t ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m2334846039_gshared (ReadOnlyCollection_1_t2159533596 * __this, int32_t ___index0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m2334846039(__this, ___index0, method) ((  int32_t (*) (ReadOnlyCollection_1_t2159533596 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m2334846039_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<GameAnalyticsSDK.Settings/HelpTypes>::System.Collections.Generic.IList<T>.set_Item(System.Int32,T)
extern "C"  void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m1224720676_gshared (ReadOnlyCollection_1_t2159533596 * __this, int32_t ___index0, int32_t ___value1, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m1224720676(__this, ___index0, ___value1, method) ((  void (*) (ReadOnlyCollection_1_t2159533596 *, int32_t, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m1224720676_gshared)(__this, ___index0, ___value1, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<GameAnalyticsSDK.Settings/HelpTypes>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C"  bool ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m693715810_gshared (ReadOnlyCollection_1_t2159533596 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m693715810(__this, method) ((  bool (*) (ReadOnlyCollection_1_t2159533596 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m693715810_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<GameAnalyticsSDK.Settings/HelpTypes>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m2565393643_gshared (ReadOnlyCollection_1_t2159533596 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m2565393643(__this, ___array0, ___index1, method) ((  void (*) (ReadOnlyCollection_1_t2159533596 *, Il2CppArray *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m2565393643_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.ObjectModel.ReadOnlyCollection`1<GameAnalyticsSDK.Settings/HelpTypes>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m3967371430_gshared (ReadOnlyCollection_1_t2159533596 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m3967371430(__this, method) ((  Il2CppObject * (*) (ReadOnlyCollection_1_t2159533596 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m3967371430_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<GameAnalyticsSDK.Settings/HelpTypes>::System.Collections.IList.Add(System.Object)
extern "C"  int32_t ReadOnlyCollection_1_System_Collections_IList_Add_m3512864779_gshared (ReadOnlyCollection_1_t2159533596 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Add_m3512864779(__this, ___value0, method) ((  int32_t (*) (ReadOnlyCollection_1_t2159533596 *, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Add_m3512864779_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<GameAnalyticsSDK.Settings/HelpTypes>::System.Collections.IList.Clear()
extern "C"  void ReadOnlyCollection_1_System_Collections_IList_Clear_m3610003449_gshared (ReadOnlyCollection_1_t2159533596 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Clear_m3610003449(__this, method) ((  void (*) (ReadOnlyCollection_1_t2159533596 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Clear_m3610003449_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<GameAnalyticsSDK.Settings/HelpTypes>::System.Collections.IList.Contains(System.Object)
extern "C"  bool ReadOnlyCollection_1_System_Collections_IList_Contains_m3428509857_gshared (ReadOnlyCollection_1_t2159533596 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Contains_m3428509857(__this, ___value0, method) ((  bool (*) (ReadOnlyCollection_1_t2159533596 *, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Contains_m3428509857_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<GameAnalyticsSDK.Settings/HelpTypes>::System.Collections.IList.IndexOf(System.Object)
extern "C"  int32_t ReadOnlyCollection_1_System_Collections_IList_IndexOf_m3786754275_gshared (ReadOnlyCollection_1_t2159533596 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_IndexOf_m3786754275(__this, ___value0, method) ((  int32_t (*) (ReadOnlyCollection_1_t2159533596 *, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_IndexOf_m3786754275_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<GameAnalyticsSDK.Settings/HelpTypes>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C"  void ReadOnlyCollection_1_System_Collections_IList_Insert_m3176566926_gshared (ReadOnlyCollection_1_t2159533596 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Insert_m3176566926(__this, ___index0, ___value1, method) ((  void (*) (ReadOnlyCollection_1_t2159533596 *, int32_t, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Insert_m3176566926_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<GameAnalyticsSDK.Settings/HelpTypes>::System.Collections.IList.Remove(System.Object)
extern "C"  void ReadOnlyCollection_1_System_Collections_IList_Remove_m1754745306_gshared (ReadOnlyCollection_1_t2159533596 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Remove_m1754745306(__this, ___value0, method) ((  void (*) (ReadOnlyCollection_1_t2159533596 *, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Remove_m1754745306_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<GameAnalyticsSDK.Settings/HelpTypes>::System.Collections.IList.RemoveAt(System.Int32)
extern "C"  void ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m147885342_gshared (ReadOnlyCollection_1_t2159533596 * __this, int32_t ___index0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m147885342(__this, ___index0, method) ((  void (*) (ReadOnlyCollection_1_t2159533596 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m147885342_gshared)(__this, ___index0, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<GameAnalyticsSDK.Settings/HelpTypes>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m114859803_gshared (ReadOnlyCollection_1_t2159533596 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m114859803(__this, method) ((  bool (*) (ReadOnlyCollection_1_t2159533596 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m114859803_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.ReadOnlyCollection`1<GameAnalyticsSDK.Settings/HelpTypes>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m2589250631_gshared (ReadOnlyCollection_1_t2159533596 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m2589250631(__this, method) ((  Il2CppObject * (*) (ReadOnlyCollection_1_t2159533596 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m2589250631_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<GameAnalyticsSDK.Settings/HelpTypes>::System.Collections.IList.get_IsFixedSize()
extern "C"  bool ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m1080157392_gshared (ReadOnlyCollection_1_t2159533596 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m1080157392(__this, method) ((  bool (*) (ReadOnlyCollection_1_t2159533596 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m1080157392_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<GameAnalyticsSDK.Settings/HelpTypes>::System.Collections.IList.get_IsReadOnly()
extern "C"  bool ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m1458683945_gshared (ReadOnlyCollection_1_t2159533596 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m1458683945(__this, method) ((  bool (*) (ReadOnlyCollection_1_t2159533596 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m1458683945_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.ReadOnlyCollection`1<GameAnalyticsSDK.Settings/HelpTypes>::System.Collections.IList.get_Item(System.Int32)
extern "C"  Il2CppObject * ReadOnlyCollection_1_System_Collections_IList_get_Item_m2549403662_gshared (ReadOnlyCollection_1_t2159533596 * __this, int32_t ___index0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_get_Item_m2549403662(__this, ___index0, method) ((  Il2CppObject * (*) (ReadOnlyCollection_1_t2159533596 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_Item_m2549403662_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<GameAnalyticsSDK.Settings/HelpTypes>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C"  void ReadOnlyCollection_1_System_Collections_IList_set_Item_m3867988197_gshared (ReadOnlyCollection_1_t2159533596 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_set_Item_m3867988197(__this, ___index0, ___value1, method) ((  void (*) (ReadOnlyCollection_1_t2159533596 *, int32_t, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_set_Item_m3867988197_gshared)(__this, ___index0, ___value1, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<GameAnalyticsSDK.Settings/HelpTypes>::Contains(T)
extern "C"  bool ReadOnlyCollection_1_Contains_m3939561426_gshared (ReadOnlyCollection_1_t2159533596 * __this, int32_t ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_Contains_m3939561426(__this, ___value0, method) ((  bool (*) (ReadOnlyCollection_1_t2159533596 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_Contains_m3939561426_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<GameAnalyticsSDK.Settings/HelpTypes>::CopyTo(T[],System.Int32)
extern "C"  void ReadOnlyCollection_1_CopyTo_m2766997846_gshared (ReadOnlyCollection_1_t2159533596 * __this, HelpTypesU5BU5D_t2651985161* ___array0, int32_t ___index1, const MethodInfo* method);
#define ReadOnlyCollection_1_CopyTo_m2766997846(__this, ___array0, ___index1, method) ((  void (*) (ReadOnlyCollection_1_t2159533596 *, HelpTypesU5BU5D_t2651985161*, int32_t, const MethodInfo*))ReadOnlyCollection_1_CopyTo_m2766997846_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.ObjectModel.ReadOnlyCollection`1<GameAnalyticsSDK.Settings/HelpTypes>::GetEnumerator()
extern "C"  Il2CppObject* ReadOnlyCollection_1_GetEnumerator_m2579560245_gshared (ReadOnlyCollection_1_t2159533596 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_GetEnumerator_m2579560245(__this, method) ((  Il2CppObject* (*) (ReadOnlyCollection_1_t2159533596 *, const MethodInfo*))ReadOnlyCollection_1_GetEnumerator_m2579560245_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<GameAnalyticsSDK.Settings/HelpTypes>::IndexOf(T)
extern "C"  int32_t ReadOnlyCollection_1_IndexOf_m2342566234_gshared (ReadOnlyCollection_1_t2159533596 * __this, int32_t ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_IndexOf_m2342566234(__this, ___value0, method) ((  int32_t (*) (ReadOnlyCollection_1_t2159533596 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_IndexOf_m2342566234_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<GameAnalyticsSDK.Settings/HelpTypes>::get_Count()
extern "C"  int32_t ReadOnlyCollection_1_get_Count_m3717384033_gshared (ReadOnlyCollection_1_t2159533596 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_get_Count_m3717384033(__this, method) ((  int32_t (*) (ReadOnlyCollection_1_t2159533596 *, const MethodInfo*))ReadOnlyCollection_1_get_Count_m3717384033_gshared)(__this, method)
// T System.Collections.ObjectModel.ReadOnlyCollection`1<GameAnalyticsSDK.Settings/HelpTypes>::get_Item(System.Int32)
extern "C"  int32_t ReadOnlyCollection_1_get_Item_m518811031_gshared (ReadOnlyCollection_1_t2159533596 * __this, int32_t ___index0, const MethodInfo* method);
#define ReadOnlyCollection_1_get_Item_m518811031(__this, ___index0, method) ((  int32_t (*) (ReadOnlyCollection_1_t2159533596 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_get_Item_m518811031_gshared)(__this, ___index0, method)
