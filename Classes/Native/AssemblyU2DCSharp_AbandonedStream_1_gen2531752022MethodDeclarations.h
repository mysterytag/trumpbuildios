﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// AbandonedStream`1<System.Object>
struct AbandonedStream_1_t2531752022;
// System.IDisposable
struct IDisposable_t1628921374;
// System.Action`1<System.Object>
struct Action_1_t985559125;
// System.Action
struct Action_t437523947;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Priority3194150340.h"
#include "System_Core_System_Action437523947.h"

// System.Void AbandonedStream`1<System.Object>::.ctor()
extern "C"  void AbandonedStream_1__ctor_m2472560868_gshared (AbandonedStream_1_t2531752022 * __this, const MethodInfo* method);
#define AbandonedStream_1__ctor_m2472560868(__this, method) ((  void (*) (AbandonedStream_1_t2531752022 *, const MethodInfo*))AbandonedStream_1__ctor_m2472560868_gshared)(__this, method)
// System.IDisposable AbandonedStream`1<System.Object>::Listen(System.Action`1<T>,Priority)
extern "C"  Il2CppObject * AbandonedStream_1_Listen_m694518664_gshared (AbandonedStream_1_t2531752022 * __this, Action_1_t985559125 * ___action0, int32_t ___p1, const MethodInfo* method);
#define AbandonedStream_1_Listen_m694518664(__this, ___action0, ___p1, method) ((  Il2CppObject * (*) (AbandonedStream_1_t2531752022 *, Action_1_t985559125 *, int32_t, const MethodInfo*))AbandonedStream_1_Listen_m694518664_gshared)(__this, ___action0, ___p1, method)
// System.IDisposable AbandonedStream`1<System.Object>::Listen(System.Action,Priority)
extern "C"  Il2CppObject * AbandonedStream_1_Listen_m4064897183_gshared (AbandonedStream_1_t2531752022 * __this, Action_t437523947 * ___action0, int32_t ___priority1, const MethodInfo* method);
#define AbandonedStream_1_Listen_m4064897183(__this, ___action0, ___priority1, method) ((  Il2CppObject * (*) (AbandonedStream_1_t2531752022 *, Action_t437523947 *, int32_t, const MethodInfo*))AbandonedStream_1_Listen_m4064897183_gshared)(__this, ___action0, ___priority1, method)
