﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.ClientModels.AttributeInstallResult
struct AttributeInstallResult_t1834505356;

#include "codegen/il2cpp-codegen.h"

// System.Void PlayFab.ClientModels.AttributeInstallResult::.ctor()
extern "C"  void AttributeInstallResult__ctor_m1392284081 (AttributeInstallResult_t1834505356 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
