﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "System_Core_System_Func_4_gen3628925182MethodDeclarations.h"

// System.Void System.Func`4<System.Object,System.AsyncCallback,System.Object,System.IAsyncResult>::.ctor(System.Object,System.IntPtr)
#define Func_4__ctor_m1351636987(__this, ___object0, ___method1, method) ((  void (*) (Func_4_t823488097 *, Il2CppObject *, IntPtr_t, const MethodInfo*))Func_4__ctor_m283050854_gshared)(__this, ___object0, ___method1, method)
// TResult System.Func`4<System.Object,System.AsyncCallback,System.Object,System.IAsyncResult>::Invoke(T1,T2,T3)
#define Func_4_Invoke_m1354453781(__this, ___arg10, ___arg21, ___arg32, method) ((  Il2CppObject * (*) (Func_4_t823488097 *, Il2CppObject *, AsyncCallback_t1363551830 *, Il2CppObject *, const MethodInfo*))Func_4_Invoke_m4260158276_gshared)(__this, ___arg10, ___arg21, ___arg32, method)
// System.IAsyncResult System.Func`4<System.Object,System.AsyncCallback,System.Object,System.IAsyncResult>::BeginInvoke(T1,T2,T3,System.AsyncCallback,System.Object)
#define Func_4_BeginInvoke_m1164832294(__this, ___arg10, ___arg21, ___arg32, ___callback3, ___object4, method) ((  Il2CppObject * (*) (Func_4_t823488097 *, Il2CppObject *, AsyncCallback_t1363551830 *, Il2CppObject *, AsyncCallback_t1363551830 *, Il2CppObject *, const MethodInfo*))Func_4_BeginInvoke_m1091833817_gshared)(__this, ___arg10, ___arg21, ___arg32, ___callback3, ___object4, method)
// TResult System.Func`4<System.Object,System.AsyncCallback,System.Object,System.IAsyncResult>::EndInvoke(System.IAsyncResult)
#define Func_4_EndInvoke_m2397416105(__this, ___result0, method) ((  Il2CppObject * (*) (Func_4_t823488097 *, Il2CppObject *, const MethodInfo*))Func_4_EndInvoke_m3792811160_gshared)(__this, ___result0, method)
