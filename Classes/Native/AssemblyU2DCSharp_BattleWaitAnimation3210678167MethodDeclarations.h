﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// BattleWaitAnimation
struct BattleWaitAnimation_t3210678167;
// UnityEngine.Animator
struct Animator_t792326996;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Animator792326996.h"
#include "mscorlib_System_String968488902.h"

// System.Void BattleWaitAnimation::.ctor(UnityEngine.Animator,System.String)
extern "C"  void BattleWaitAnimation__ctor_m2183002770 (BattleWaitAnimation_t3210678167 * __this, Animator_t792326996 * ___animator0, String_t* ___stateId1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BattleWaitAnimation::.ctor(UnityEngine.Animator,System.Int32)
extern "C"  void BattleWaitAnimation__ctor_m4212107201 (BattleWaitAnimation_t3210678167 * __this, Animator_t792326996 * ___animator0, int32_t ___stateHash1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean BattleWaitAnimation::get_finished()
extern "C"  bool BattleWaitAnimation_get_finished_m3288568631 (BattleWaitAnimation_t3210678167 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BattleWaitAnimation::Tick(System.Single)
extern "C"  void BattleWaitAnimation_Tick_m2586661726 (BattleWaitAnimation_t3210678167 * __this, float ___dt0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BattleWaitAnimation::Dispose()
extern "C"  void BattleWaitAnimation_Dispose_m3197168881 (BattleWaitAnimation_t3210678167 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
