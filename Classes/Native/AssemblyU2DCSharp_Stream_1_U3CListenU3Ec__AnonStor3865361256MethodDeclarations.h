﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Stream`1/<Listen>c__AnonStorey128<System.DateTime>
struct U3CListenU3Ec__AnonStorey128_t3865361256;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_DateTime339033936.h"

// System.Void Stream`1/<Listen>c__AnonStorey128<System.DateTime>::.ctor()
extern "C"  void U3CListenU3Ec__AnonStorey128__ctor_m2582115780_gshared (U3CListenU3Ec__AnonStorey128_t3865361256 * __this, const MethodInfo* method);
#define U3CListenU3Ec__AnonStorey128__ctor_m2582115780(__this, method) ((  void (*) (U3CListenU3Ec__AnonStorey128_t3865361256 *, const MethodInfo*))U3CListenU3Ec__AnonStorey128__ctor_m2582115780_gshared)(__this, method)
// System.Void Stream`1/<Listen>c__AnonStorey128<System.DateTime>::<>m__1B9(T)
extern "C"  void U3CListenU3Ec__AnonStorey128_U3CU3Em__1B9_m1130501857_gshared (U3CListenU3Ec__AnonStorey128_t3865361256 * __this, DateTime_t339033936  ____0, const MethodInfo* method);
#define U3CListenU3Ec__AnonStorey128_U3CU3Em__1B9_m1130501857(__this, ____0, method) ((  void (*) (U3CListenU3Ec__AnonStorey128_t3865361256 *, DateTime_t339033936 , const MethodInfo*))U3CListenU3Ec__AnonStorey128_U3CU3Em__1B9_m1130501857_gshared)(__this, ____0, method)
