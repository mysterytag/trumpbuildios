﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_ValueType4014882752.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>/$ArrayType$6144
#pragma pack(push, tp, 1)
struct  U24ArrayTypeU246144_t336100496 
{
public:
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t U24ArrayTypeU246144_t336100496__padding[6144];
	};

public:
};
#pragma pack(pop, tp)

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for marshalling of: <PrivateImplementationDetails>/$ArrayType$6144
#pragma pack(push, tp, 1)
struct U24ArrayTypeU246144_t336100496_marshaled_pinvoke
{
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t U24ArrayTypeU246144_t336100496__padding[6144];
	};
};
#pragma pack(pop, tp)
// Native definition for marshalling of: <PrivateImplementationDetails>/$ArrayType$6144
#pragma pack(push, tp, 1)
struct U24ArrayTypeU246144_t336100496_marshaled_com
{
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t U24ArrayTypeU246144_t336100496__padding[6144];
	};
};
#pragma pack(pop, tp)
