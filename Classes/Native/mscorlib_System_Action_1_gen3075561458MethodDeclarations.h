﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Action`1<ModestTree.Util.MouseWheelScrollDirections>
struct Action_1_t3075561458;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t537683269;
// System.AsyncCallback
struct AsyncCallback_t1363551830;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_IntPtr676692020.h"
#include "AssemblyU2DCSharp_ModestTree_Util_MouseWheelScroll2927108753.h"
#include "mscorlib_System_AsyncCallback1363551830.h"

// System.Void System.Action`1<ModestTree.Util.MouseWheelScrollDirections>::.ctor(System.Object,System.IntPtr)
extern "C"  void Action_1__ctor_m1139518760_gshared (Action_1_t3075561458 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method);
#define Action_1__ctor_m1139518760(__this, ___object0, ___method1, method) ((  void (*) (Action_1_t3075561458 *, Il2CppObject *, IntPtr_t, const MethodInfo*))Action_1__ctor_m1139518760_gshared)(__this, ___object0, ___method1, method)
// System.Void System.Action`1<ModestTree.Util.MouseWheelScrollDirections>::Invoke(T)
extern "C"  void Action_1_Invoke_m2996669171_gshared (Action_1_t3075561458 * __this, int32_t ___obj0, const MethodInfo* method);
#define Action_1_Invoke_m2996669171(__this, ___obj0, method) ((  void (*) (Action_1_t3075561458 *, int32_t, const MethodInfo*))Action_1_Invoke_m2996669171_gshared)(__this, ___obj0, method)
// System.IAsyncResult System.Action`1<ModestTree.Util.MouseWheelScrollDirections>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * Action_1_BeginInvoke_m133552186_gshared (Action_1_t3075561458 * __this, int32_t ___obj0, AsyncCallback_t1363551830 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method);
#define Action_1_BeginInvoke_m133552186(__this, ___obj0, ___callback1, ___object2, method) ((  Il2CppObject * (*) (Action_1_t3075561458 *, int32_t, AsyncCallback_t1363551830 *, Il2CppObject *, const MethodInfo*))Action_1_BeginInvoke_m133552186_gshared)(__this, ___obj0, ___callback1, ___object2, method)
// System.Void System.Action`1<ModestTree.Util.MouseWheelScrollDirections>::EndInvoke(System.IAsyncResult)
extern "C"  void Action_1_EndInvoke_m3520516623_gshared (Action_1_t3075561458 * __this, Il2CppObject * ___result0, const MethodInfo* method);
#define Action_1_EndInvoke_m3520516623(__this, ___result0, method) ((  void (*) (Action_1_t3075561458 *, Il2CppObject *, const MethodInfo*))Action_1_EndInvoke_m3520516623_gshared)(__this, ___result0, method)
