﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FiniteTimeProc
struct  FiniteTimeProc_t2933053874  : public Il2CppObject
{
public:
	// System.Single FiniteTimeProc::timeTotal
	float ___timeTotal_0;
	// System.Single FiniteTimeProc::timeLeft
	float ___timeLeft_1;

public:
	inline static int32_t get_offset_of_timeTotal_0() { return static_cast<int32_t>(offsetof(FiniteTimeProc_t2933053874, ___timeTotal_0)); }
	inline float get_timeTotal_0() const { return ___timeTotal_0; }
	inline float* get_address_of_timeTotal_0() { return &___timeTotal_0; }
	inline void set_timeTotal_0(float value)
	{
		___timeTotal_0 = value;
	}

	inline static int32_t get_offset_of_timeLeft_1() { return static_cast<int32_t>(offsetof(FiniteTimeProc_t2933053874, ___timeLeft_1)); }
	inline float get_timeLeft_1() const { return ___timeLeft_1; }
	inline float* get_address_of_timeLeft_1() { return &___timeLeft_1; }
	inline void set_timeLeft_1(float value)
	{
		___timeLeft_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
