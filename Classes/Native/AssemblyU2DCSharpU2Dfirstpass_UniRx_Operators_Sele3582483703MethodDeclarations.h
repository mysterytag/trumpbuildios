﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.SelectManyObservable`3/SelectManyOuterObserver<System.Object,System.Object,System.Object>
struct SelectManyOuterObserver_t3582483703;
// UniRx.Operators.SelectManyObservable`3<System.Object,System.Object,System.Object>
struct SelectManyObservable_3_t3068991;
// UniRx.IObserver`1<System.Object>
struct IObserver_1_t3049105323;
// System.IDisposable
struct IDisposable_t1628921374;
// System.Object
struct Il2CppObject;
// System.Exception
struct Exception_t1967233988;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_Exception1967233988.h"

// System.Void UniRx.Operators.SelectManyObservable`3/SelectManyOuterObserver<System.Object,System.Object,System.Object>::.ctor(UniRx.Operators.SelectManyObservable`3<TSource,TCollection,TResult>,UniRx.IObserver`1<TResult>,System.IDisposable)
extern "C"  void SelectManyOuterObserver__ctor_m1732890259_gshared (SelectManyOuterObserver_t3582483703 * __this, SelectManyObservable_3_t3068991 * ___parent0, Il2CppObject* ___observer1, Il2CppObject * ___cancel2, const MethodInfo* method);
#define SelectManyOuterObserver__ctor_m1732890259(__this, ___parent0, ___observer1, ___cancel2, method) ((  void (*) (SelectManyOuterObserver_t3582483703 *, SelectManyObservable_3_t3068991 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))SelectManyOuterObserver__ctor_m1732890259_gshared)(__this, ___parent0, ___observer1, ___cancel2, method)
// System.IDisposable UniRx.Operators.SelectManyObservable`3/SelectManyOuterObserver<System.Object,System.Object,System.Object>::Run()
extern "C"  Il2CppObject * SelectManyOuterObserver_Run_m3699277586_gshared (SelectManyOuterObserver_t3582483703 * __this, const MethodInfo* method);
#define SelectManyOuterObserver_Run_m3699277586(__this, method) ((  Il2CppObject * (*) (SelectManyOuterObserver_t3582483703 *, const MethodInfo*))SelectManyOuterObserver_Run_m3699277586_gshared)(__this, method)
// System.Void UniRx.Operators.SelectManyObservable`3/SelectManyOuterObserver<System.Object,System.Object,System.Object>::OnNext(TSource)
extern "C"  void SelectManyOuterObserver_OnNext_m38124219_gshared (SelectManyOuterObserver_t3582483703 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define SelectManyOuterObserver_OnNext_m38124219(__this, ___value0, method) ((  void (*) (SelectManyOuterObserver_t3582483703 *, Il2CppObject *, const MethodInfo*))SelectManyOuterObserver_OnNext_m38124219_gshared)(__this, ___value0, method)
// System.Void UniRx.Operators.SelectManyObservable`3/SelectManyOuterObserver<System.Object,System.Object,System.Object>::OnError(System.Exception)
extern "C"  void SelectManyOuterObserver_OnError_m3115093093_gshared (SelectManyOuterObserver_t3582483703 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method);
#define SelectManyOuterObserver_OnError_m3115093093(__this, ___error0, method) ((  void (*) (SelectManyOuterObserver_t3582483703 *, Exception_t1967233988 *, const MethodInfo*))SelectManyOuterObserver_OnError_m3115093093_gshared)(__this, ___error0, method)
// System.Void UniRx.Operators.SelectManyObservable`3/SelectManyOuterObserver<System.Object,System.Object,System.Object>::OnCompleted()
extern "C"  void SelectManyOuterObserver_OnCompleted_m1447489336_gshared (SelectManyOuterObserver_t3582483703 * __this, const MethodInfo* method);
#define SelectManyOuterObserver_OnCompleted_m1447489336(__this, method) ((  void (*) (SelectManyOuterObserver_t3582483703 *, const MethodInfo*))SelectManyOuterObserver_OnCompleted_m1447489336_gshared)(__this, method)
