﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Reactor_1_gen96911316MethodDeclarations.h"

// System.Void Reactor`1<CollectionMoveEvent`1<DonateButton>>::.ctor()
#define Reactor_1__ctor_m3940844529(__this, method) ((  void (*) (Reactor_1_t2665507298 *, const MethodInfo*))Reactor_1__ctor_m1161914514_gshared)(__this, method)
// System.Void Reactor`1<CollectionMoveEvent`1<DonateButton>>::React(T)
#define Reactor_1_React_m1443790928(__this, ___t0, method) ((  void (*) (Reactor_1_t2665507298 *, CollectionMoveEvent_1_t3405702402 *, const MethodInfo*))Reactor_1_React_m1196306383_gshared)(__this, ___t0, method)
// System.Void Reactor`1<CollectionMoveEvent`1<DonateButton>>::TransactionUnpackReact(T,System.Int32)
#define Reactor_1_TransactionUnpackReact_m1114900059(__this, ___t0, ___i1, method) ((  void (*) (Reactor_1_t2665507298 *, CollectionMoveEvent_1_t3405702402 *, int32_t, const MethodInfo*))Reactor_1_TransactionUnpackReact_m1003670938_gshared)(__this, ___t0, ___i1, method)
// System.Boolean Reactor`1<CollectionMoveEvent`1<DonateButton>>::Empty()
#define Reactor_1_Empty_m2027611624(__this, method) ((  bool (*) (Reactor_1_t2665507298 *, const MethodInfo*))Reactor_1_Empty_m132443017_gshared)(__this, method)
// System.IDisposable Reactor`1<CollectionMoveEvent`1<DonateButton>>::AddReaction(System.Action`1<T>,Priority)
#define Reactor_1_AddReaction_m1280373308(__this, ___reaction0, ___priority1, method) ((  Il2CppObject * (*) (Reactor_1_t2665507298 *, Action_1_t3554155107 *, int32_t, const MethodInfo*))Reactor_1_AddReaction_m1495770619_gshared)(__this, ___reaction0, ___priority1, method)
// System.Void Reactor`1<CollectionMoveEvent`1<DonateButton>>::UpgradeToMultyPriority()
#define Reactor_1_UpgradeToMultyPriority_m2149789385(__this, method) ((  void (*) (Reactor_1_t2665507298 *, const MethodInfo*))Reactor_1_UpgradeToMultyPriority_m912916552_gshared)(__this, method)
// System.Void Reactor`1<CollectionMoveEvent`1<DonateButton>>::AddToMultipriority(System.Action`1<T>,Priority)
#define Reactor_1_AddToMultipriority_m3827717710(__this, ___reaction0, ___p1, method) ((  void (*) (Reactor_1_t2665507298 *, Action_1_t3554155107 *, int32_t, const MethodInfo*))Reactor_1_AddToMultipriority_m3381282287_gshared)(__this, ___reaction0, ___p1, method)
// System.Void Reactor`1<CollectionMoveEvent`1<DonateButton>>::RemoveReaction(System.Action`1<T>,Priority)
#define Reactor_1_RemoveReaction_m3400451650(__this, ___reaction0, ___p1, method) ((  void (*) (Reactor_1_t2665507298 *, Action_1_t3554155107 *, int32_t, const MethodInfo*))Reactor_1_RemoveReaction_m4227182179_gshared)(__this, ___reaction0, ___p1, method)
// System.Void Reactor`1<CollectionMoveEvent`1<DonateButton>>::Clear()
#define Reactor_1_Clear_m1346977820(__this, method) ((  void (*) (Reactor_1_t2665507298 *, const MethodInfo*))Reactor_1_Clear_m2863015101_gshared)(__this, method)
