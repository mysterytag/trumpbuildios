﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.PlayFabClientAPI/LoginWithAndroidDeviceIDResponseCallback
struct LoginWithAndroidDeviceIDResponseCallback_t1716984727;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;
// PlayFab.ClientModels.LoginWithAndroidDeviceIDRequest
struct LoginWithAndroidDeviceIDRequest_t522798990;
// PlayFab.ClientModels.LoginResult
struct LoginResult_t2117559510;
// PlayFab.PlayFabError
struct PlayFabError_t750598646;
// System.IAsyncResult
struct IAsyncResult_t537683269;
// System.AsyncCallback
struct AsyncCallback_t1363551830;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_IntPtr676692020.h"
#include "mscorlib_System_String968488902.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_LoginWithAnd522798990.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_LoginResult2117559510.h"
#include "AssemblyU2DCSharpU2Dfirstpass_PlayFab_PlayFabError750598646.h"
#include "mscorlib_System_AsyncCallback1363551830.h"

// System.Void PlayFab.PlayFabClientAPI/LoginWithAndroidDeviceIDResponseCallback::.ctor(System.Object,System.IntPtr)
extern "C"  void LoginWithAndroidDeviceIDResponseCallback__ctor_m3797593350 (LoginWithAndroidDeviceIDResponseCallback_t1716984727 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/LoginWithAndroidDeviceIDResponseCallback::Invoke(System.String,System.Int32,PlayFab.ClientModels.LoginWithAndroidDeviceIDRequest,PlayFab.ClientModels.LoginResult,PlayFab.PlayFabError,System.Object)
extern "C"  void LoginWithAndroidDeviceIDResponseCallback_Invoke_m3490070871 (LoginWithAndroidDeviceIDResponseCallback_t1716984727 * __this, String_t* ___urlPath0, int32_t ___callId1, LoginWithAndroidDeviceIDRequest_t522798990 * ___request2, LoginResult_t2117559510 * ___result3, PlayFabError_t750598646 * ___error4, Il2CppObject * ___customData5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
extern "C" void pinvoke_delegate_wrapper_LoginWithAndroidDeviceIDResponseCallback_t1716984727(Il2CppObject* delegate, String_t* ___urlPath0, int32_t ___callId1, LoginWithAndroidDeviceIDRequest_t522798990 * ___request2, LoginResult_t2117559510 * ___result3, PlayFabError_t750598646 * ___error4, Il2CppObject * ___customData5);
// System.IAsyncResult PlayFab.PlayFabClientAPI/LoginWithAndroidDeviceIDResponseCallback::BeginInvoke(System.String,System.Int32,PlayFab.ClientModels.LoginWithAndroidDeviceIDRequest,PlayFab.ClientModels.LoginResult,PlayFab.PlayFabError,System.Object,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * LoginWithAndroidDeviceIDResponseCallback_BeginInvoke_m685540762 (LoginWithAndroidDeviceIDResponseCallback_t1716984727 * __this, String_t* ___urlPath0, int32_t ___callId1, LoginWithAndroidDeviceIDRequest_t522798990 * ___request2, LoginResult_t2117559510 * ___result3, PlayFabError_t750598646 * ___error4, Il2CppObject * ___customData5, AsyncCallback_t1363551830 * ___callback6, Il2CppObject * ___object7, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/LoginWithAndroidDeviceIDResponseCallback::EndInvoke(System.IAsyncResult)
extern "C"  void LoginWithAndroidDeviceIDResponseCallback_EndInvoke_m40473110 (LoginWithAndroidDeviceIDResponseCallback_t1716984727 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
