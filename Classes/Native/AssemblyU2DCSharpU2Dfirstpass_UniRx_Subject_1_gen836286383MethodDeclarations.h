﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Subject_1_gen3311940555MethodDeclarations.h"

// System.Void UniRx.Subject`1<UniRx.CollectionReplaceEvent`1<UniRx.Tuple`2<System.String,UnityEngine.Sprite>>>::.ctor()
#define Subject_1__ctor_m1558877979(__this, method) ((  void (*) (Subject_1_t836286383 *, const MethodInfo*))Subject_1__ctor_m2360697207_gshared)(__this, method)
// System.Boolean UniRx.Subject`1<UniRx.CollectionReplaceEvent`1<UniRx.Tuple`2<System.String,UnityEngine.Sprite>>>::get_HasObservers()
#define Subject_1_get_HasObservers_m823505041(__this, method) ((  bool (*) (Subject_1_t836286383 *, const MethodInfo*))Subject_1_get_HasObservers_m2818028541_gshared)(__this, method)
// System.Void UniRx.Subject`1<UniRx.CollectionReplaceEvent`1<UniRx.Tuple`2<System.String,UnityEngine.Sprite>>>::OnCompleted()
#define Subject_1_OnCompleted_m1469106789(__this, method) ((  void (*) (Subject_1_t836286383 *, const MethodInfo*))Subject_1_OnCompleted_m3042533313_gshared)(__this, method)
// System.Void UniRx.Subject`1<UniRx.CollectionReplaceEvent`1<UniRx.Tuple`2<System.String,UnityEngine.Sprite>>>::OnError(System.Exception)
#define Subject_1_OnError_m420350994(__this, ___error0, method) ((  void (*) (Subject_1_t836286383 *, Exception_t1967233988 *, const MethodInfo*))Subject_1_OnError_m79001454_gshared)(__this, ___error0, method)
// System.Void UniRx.Subject`1<UniRx.CollectionReplaceEvent`1<UniRx.Tuple`2<System.String,UnityEngine.Sprite>>>::OnNext(T)
#define Subject_1_OnNext_m1968854787(__this, ___value0, method) ((  void (*) (Subject_1_t836286383 *, CollectionReplaceEvent_1_t3193219059 , const MethodInfo*))Subject_1_OnNext_m3717986911_gshared)(__this, ___value0, method)
// System.IDisposable UniRx.Subject`1<UniRx.CollectionReplaceEvent`1<UniRx.Tuple`2<System.String,UnityEngine.Sprite>>>::Subscribe(UniRx.IObserver`1<T>)
#define Subject_1_Subscribe_m2405712602(__this, ___observer0, method) ((  Il2CppObject * (*) (Subject_1_t836286383 *, Il2CppObject*, const MethodInfo*))Subject_1_Subscribe_m2277887436_gshared)(__this, ___observer0, method)
// System.Void UniRx.Subject`1<UniRx.CollectionReplaceEvent`1<UniRx.Tuple`2<System.String,UnityEngine.Sprite>>>::Dispose()
#define Subject_1_Dispose_m3331401880(__this, method) ((  void (*) (Subject_1_t836286383 *, const MethodInfo*))Subject_1_Dispose_m785566708_gshared)(__this, method)
// System.Void UniRx.Subject`1<UniRx.CollectionReplaceEvent`1<UniRx.Tuple`2<System.String,UnityEngine.Sprite>>>::ThrowIfDisposed()
#define Subject_1_ThrowIfDisposed_m1856551457(__this, method) ((  void (*) (Subject_1_t836286383 *, const MethodInfo*))Subject_1_ThrowIfDisposed_m3777970557_gshared)(__this, method)
// System.Boolean UniRx.Subject`1<UniRx.CollectionReplaceEvent`1<UniRx.Tuple`2<System.String,UnityEngine.Sprite>>>::IsRequiredSubscribeOnCurrentThread()
#define Subject_1_IsRequiredSubscribeOnCurrentThread_m1802992456(__this, method) ((  bool (*) (Subject_1_t836286383 *, const MethodInfo*))Subject_1_IsRequiredSubscribeOnCurrentThread_m1893727156_gshared)(__this, method)
