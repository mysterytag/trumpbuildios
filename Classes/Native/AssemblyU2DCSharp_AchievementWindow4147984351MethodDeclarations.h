﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// AchievementWindow
struct AchievementWindow_t4147984351;
// AchievementDescription
struct AchievementDescription_t2323334509;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_AchievementDescription2323334509.h"

// System.Void AchievementWindow::.ctor()
extern "C"  void AchievementWindow__ctor_m142467180 (AchievementWindow_t4147984351 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AchievementWindow::Awake()
extern "C"  void AchievementWindow_Awake_m380072399 (AchievementWindow_t4147984351 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AchievementWindow::Init(AchievementDescription)
extern "C"  void AchievementWindow_Init_m30052187 (AchievementWindow_t4147984351 * __this, AchievementDescription_t2323334509 * ___ad0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AchievementWindow::ClickVK()
extern "C"  void AchievementWindow_ClickVK_m1449343559 (AchievementWindow_t4147984351 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AchievementWindow::ClickFB()
extern "C"  void AchievementWindow_ClickFB_m1448858254 (AchievementWindow_t4147984351 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
