﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Object
struct Il2CppObject;

#include "UnityEngine_UnityEngine_MonoBehaviour3012272455.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FlurryAnalytics.MonoSingleton`1<System.Object>
struct  MonoSingleton_1_t858865150  : public MonoBehaviour_t3012272455
{
public:

public:
};

struct MonoSingleton_1_t858865150_StaticFields
{
public:
	// T FlurryAnalytics.MonoSingleton`1::s_Instance
	Il2CppObject * ___s_Instance_2;
	// System.Boolean FlurryAnalytics.MonoSingleton`1::s_IsDestroyed
	bool ___s_IsDestroyed_3;

public:
	inline static int32_t get_offset_of_s_Instance_2() { return static_cast<int32_t>(offsetof(MonoSingleton_1_t858865150_StaticFields, ___s_Instance_2)); }
	inline Il2CppObject * get_s_Instance_2() const { return ___s_Instance_2; }
	inline Il2CppObject ** get_address_of_s_Instance_2() { return &___s_Instance_2; }
	inline void set_s_Instance_2(Il2CppObject * value)
	{
		___s_Instance_2 = value;
		Il2CppCodeGenWriteBarrier(&___s_Instance_2, value);
	}

	inline static int32_t get_offset_of_s_IsDestroyed_3() { return static_cast<int32_t>(offsetof(MonoSingleton_1_t858865150_StaticFields, ___s_IsDestroyed_3)); }
	inline bool get_s_IsDestroyed_3() const { return ___s_IsDestroyed_3; }
	inline bool* get_address_of_s_IsDestroyed_3() { return &___s_IsDestroyed_3; }
	inline void set_s_IsDestroyed_3(bool value)
	{
		___s_IsDestroyed_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
