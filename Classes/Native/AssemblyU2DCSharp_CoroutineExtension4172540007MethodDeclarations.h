﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.IEnumerator
struct IEnumerator_t287207039;
// UnityEngine.Component
struct Component_t2126946602;
// System.Action`1<System.Action>
struct Action_1_t585976652;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Component2126946602.h"

// System.Collections.IEnumerator CoroutineExtension::DoAndWaitCallback(UnityEngine.Component,System.Action`1<System.Action>)
extern "C"  Il2CppObject * CoroutineExtension_DoAndWaitCallback_m1754732651 (Il2CppObject * __this /* static, unused */, Component_t2126946602 * ___obj0, Action_1_t585976652 * ___action1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
