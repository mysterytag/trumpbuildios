﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.ComponentModel.LicenseProvider
struct LicenseProvider_t2468156379;

#include "codegen/il2cpp-codegen.h"

// System.Void System.ComponentModel.LicenseProvider::.ctor()
extern "C"  void LicenseProvider__ctor_m2553122779 (LicenseProvider_t2468156379 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
