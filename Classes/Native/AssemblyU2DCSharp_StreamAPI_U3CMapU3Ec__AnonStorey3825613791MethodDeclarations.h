﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// StreamAPI/<Map>c__AnonStorey130`2/<Map>c__AnonStorey131`2<System.Object,System.Object>
struct U3CMapU3Ec__AnonStorey131_2_t3825613791;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"

// System.Void StreamAPI/<Map>c__AnonStorey130`2/<Map>c__AnonStorey131`2<System.Object,System.Object>::.ctor()
extern "C"  void U3CMapU3Ec__AnonStorey131_2__ctor_m2740834076_gshared (U3CMapU3Ec__AnonStorey131_2_t3825613791 * __this, const MethodInfo* method);
#define U3CMapU3Ec__AnonStorey131_2__ctor_m2740834076(__this, method) ((  void (*) (U3CMapU3Ec__AnonStorey131_2_t3825613791 *, const MethodInfo*))U3CMapU3Ec__AnonStorey131_2__ctor_m2740834076_gshared)(__this, method)
// System.Void StreamAPI/<Map>c__AnonStorey130`2/<Map>c__AnonStorey131`2<System.Object,System.Object>::<>m__1CB(T)
extern "C"  void U3CMapU3Ec__AnonStorey131_2_U3CU3Em__1CB_m2167255825_gshared (U3CMapU3Ec__AnonStorey131_2_t3825613791 * __this, Il2CppObject * ___val0, const MethodInfo* method);
#define U3CMapU3Ec__AnonStorey131_2_U3CU3Em__1CB_m2167255825(__this, ___val0, method) ((  void (*) (U3CMapU3Ec__AnonStorey131_2_t3825613791 *, Il2CppObject *, const MethodInfo*))U3CMapU3Ec__AnonStorey131_2_U3CU3Em__1CB_m2167255825_gshared)(__this, ___val0, method)
