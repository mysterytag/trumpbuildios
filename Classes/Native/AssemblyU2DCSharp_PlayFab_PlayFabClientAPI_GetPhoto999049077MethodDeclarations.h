﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.PlayFabClientAPI/GetPhotonAuthenticationTokenResponseCallback
struct GetPhotonAuthenticationTokenResponseCallback_t999049077;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;
// PlayFab.ClientModels.GetPhotonAuthenticationTokenRequest
struct GetPhotonAuthenticationTokenRequest_t2686429680;
// PlayFab.ClientModels.GetPhotonAuthenticationTokenResult
struct GetPhotonAuthenticationTokenResult_t3031300796;
// PlayFab.PlayFabError
struct PlayFabError_t750598646;
// System.IAsyncResult
struct IAsyncResult_t537683269;
// System.AsyncCallback
struct AsyncCallback_t1363551830;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_IntPtr676692020.h"
#include "mscorlib_System_String968488902.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_GetPhotonAu2686429680.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_GetPhotonAu3031300796.h"
#include "AssemblyU2DCSharpU2Dfirstpass_PlayFab_PlayFabError750598646.h"
#include "mscorlib_System_AsyncCallback1363551830.h"

// System.Void PlayFab.PlayFabClientAPI/GetPhotonAuthenticationTokenResponseCallback::.ctor(System.Object,System.IntPtr)
extern "C"  void GetPhotonAuthenticationTokenResponseCallback__ctor_m1500555748 (GetPhotonAuthenticationTokenResponseCallback_t999049077 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/GetPhotonAuthenticationTokenResponseCallback::Invoke(System.String,System.Int32,PlayFab.ClientModels.GetPhotonAuthenticationTokenRequest,PlayFab.ClientModels.GetPhotonAuthenticationTokenResult,PlayFab.PlayFabError,System.Object)
extern "C"  void GetPhotonAuthenticationTokenResponseCallback_Invoke_m2054666321 (GetPhotonAuthenticationTokenResponseCallback_t999049077 * __this, String_t* ___urlPath0, int32_t ___callId1, GetPhotonAuthenticationTokenRequest_t2686429680 * ___request2, GetPhotonAuthenticationTokenResult_t3031300796 * ___result3, PlayFabError_t750598646 * ___error4, Il2CppObject * ___customData5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
extern "C" void pinvoke_delegate_wrapper_GetPhotonAuthenticationTokenResponseCallback_t999049077(Il2CppObject* delegate, String_t* ___urlPath0, int32_t ___callId1, GetPhotonAuthenticationTokenRequest_t2686429680 * ___request2, GetPhotonAuthenticationTokenResult_t3031300796 * ___result3, PlayFabError_t750598646 * ___error4, Il2CppObject * ___customData5);
// System.IAsyncResult PlayFab.PlayFabClientAPI/GetPhotonAuthenticationTokenResponseCallback::BeginInvoke(System.String,System.Int32,PlayFab.ClientModels.GetPhotonAuthenticationTokenRequest,PlayFab.ClientModels.GetPhotonAuthenticationTokenResult,PlayFab.PlayFabError,System.Object,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * GetPhotonAuthenticationTokenResponseCallback_BeginInvoke_m3785987646 (GetPhotonAuthenticationTokenResponseCallback_t999049077 * __this, String_t* ___urlPath0, int32_t ___callId1, GetPhotonAuthenticationTokenRequest_t2686429680 * ___request2, GetPhotonAuthenticationTokenResult_t3031300796 * ___result3, PlayFabError_t750598646 * ___error4, Il2CppObject * ___customData5, AsyncCallback_t1363551830 * ___callback6, Il2CppObject * ___object7, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/GetPhotonAuthenticationTokenResponseCallback::EndInvoke(System.IAsyncResult)
extern "C"  void GetPhotonAuthenticationTokenResponseCallback_EndInvoke_m2588716532 (GetPhotonAuthenticationTokenResponseCallback_t999049077 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
