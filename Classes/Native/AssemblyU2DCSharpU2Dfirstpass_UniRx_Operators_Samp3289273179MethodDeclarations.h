﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.SampleFrameObservable`1/SampleFrame/SampleFrameTick<UniRx.Unit>
struct SampleFrameTick_t3289273179;
// UniRx.Operators.SampleFrameObservable`1/SampleFrame<UniRx.Unit>
struct SampleFrame_t3302090534;
// System.Exception
struct Exception_t1967233988;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Exception1967233988.h"

// System.Void UniRx.Operators.SampleFrameObservable`1/SampleFrame/SampleFrameTick<UniRx.Unit>::.ctor(UniRx.Operators.SampleFrameObservable`1/SampleFrame<T>)
extern "C"  void SampleFrameTick__ctor_m516406874_gshared (SampleFrameTick_t3289273179 * __this, SampleFrame_t3302090534 * ___parent0, const MethodInfo* method);
#define SampleFrameTick__ctor_m516406874(__this, ___parent0, method) ((  void (*) (SampleFrameTick_t3289273179 *, SampleFrame_t3302090534 *, const MethodInfo*))SampleFrameTick__ctor_m516406874_gshared)(__this, ___parent0, method)
// System.Void UniRx.Operators.SampleFrameObservable`1/SampleFrame/SampleFrameTick<UniRx.Unit>::OnCompleted()
extern "C"  void SampleFrameTick_OnCompleted_m1679013782_gshared (SampleFrameTick_t3289273179 * __this, const MethodInfo* method);
#define SampleFrameTick_OnCompleted_m1679013782(__this, method) ((  void (*) (SampleFrameTick_t3289273179 *, const MethodInfo*))SampleFrameTick_OnCompleted_m1679013782_gshared)(__this, method)
// System.Void UniRx.Operators.SampleFrameObservable`1/SampleFrame/SampleFrameTick<UniRx.Unit>::OnError(System.Exception)
extern "C"  void SampleFrameTick_OnError_m1722398659_gshared (SampleFrameTick_t3289273179 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method);
#define SampleFrameTick_OnError_m1722398659(__this, ___error0, method) ((  void (*) (SampleFrameTick_t3289273179 *, Exception_t1967233988 *, const MethodInfo*))SampleFrameTick_OnError_m1722398659_gshared)(__this, ___error0, method)
// System.Void UniRx.Operators.SampleFrameObservable`1/SampleFrame/SampleFrameTick<UniRx.Unit>::OnNext(System.Int64)
extern "C"  void SampleFrameTick_OnNext_m1705537468_gshared (SampleFrameTick_t3289273179 * __this, int64_t ____0, const MethodInfo* method);
#define SampleFrameTick_OnNext_m1705537468(__this, ____0, method) ((  void (*) (SampleFrameTick_t3289273179 *, int64_t, const MethodInfo*))SampleFrameTick_OnNext_m1705537468_gshared)(__this, ____0, method)
