﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.ThrottleFrameObservable`1/ThrottleFrame<System.Object>
struct ThrottleFrame_t3629975492;
// UniRx.Operators.ThrottleFrameObservable`1<System.Object>
struct ThrottleFrameObservable_1_t2748437981;
// UniRx.IObserver`1<System.Object>
struct IObserver_1_t3049105323;
// System.IDisposable
struct IDisposable_t1628921374;
// System.Object
struct Il2CppObject;
// System.Exception
struct Exception_t1967233988;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_Exception1967233988.h"

// System.Void UniRx.Operators.ThrottleFrameObservable`1/ThrottleFrame<System.Object>::.ctor(UniRx.Operators.ThrottleFrameObservable`1<T>,UniRx.IObserver`1<T>,System.IDisposable)
extern "C"  void ThrottleFrame__ctor_m3150178405_gshared (ThrottleFrame_t3629975492 * __this, ThrottleFrameObservable_1_t2748437981 * ___parent0, Il2CppObject* ___observer1, Il2CppObject * ___cancel2, const MethodInfo* method);
#define ThrottleFrame__ctor_m3150178405(__this, ___parent0, ___observer1, ___cancel2, method) ((  void (*) (ThrottleFrame_t3629975492 *, ThrottleFrameObservable_1_t2748437981 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))ThrottleFrame__ctor_m3150178405_gshared)(__this, ___parent0, ___observer1, ___cancel2, method)
// System.IDisposable UniRx.Operators.ThrottleFrameObservable`1/ThrottleFrame<System.Object>::Run()
extern "C"  Il2CppObject * ThrottleFrame_Run_m3848274933_gshared (ThrottleFrame_t3629975492 * __this, const MethodInfo* method);
#define ThrottleFrame_Run_m3848274933(__this, method) ((  Il2CppObject * (*) (ThrottleFrame_t3629975492 *, const MethodInfo*))ThrottleFrame_Run_m3848274933_gshared)(__this, method)
// System.Void UniRx.Operators.ThrottleFrameObservable`1/ThrottleFrame<System.Object>::OnNext(T)
extern "C"  void ThrottleFrame_OnNext_m1193704015_gshared (ThrottleFrame_t3629975492 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define ThrottleFrame_OnNext_m1193704015(__this, ___value0, method) ((  void (*) (ThrottleFrame_t3629975492 *, Il2CppObject *, const MethodInfo*))ThrottleFrame_OnNext_m1193704015_gshared)(__this, ___value0, method)
// System.Void UniRx.Operators.ThrottleFrameObservable`1/ThrottleFrame<System.Object>::OnError(System.Exception)
extern "C"  void ThrottleFrame_OnError_m4272035678_gshared (ThrottleFrame_t3629975492 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method);
#define ThrottleFrame_OnError_m4272035678(__this, ___error0, method) ((  void (*) (ThrottleFrame_t3629975492 *, Exception_t1967233988 *, const MethodInfo*))ThrottleFrame_OnError_m4272035678_gshared)(__this, ___error0, method)
// System.Void UniRx.Operators.ThrottleFrameObservable`1/ThrottleFrame<System.Object>::OnCompleted()
extern "C"  void ThrottleFrame_OnCompleted_m1422026673_gshared (ThrottleFrame_t3629975492 * __this, const MethodInfo* method);
#define ThrottleFrame_OnCompleted_m1422026673(__this, method) ((  void (*) (ThrottleFrame_t3629975492 *, const MethodInfo*))ThrottleFrame_OnCompleted_m1422026673_gshared)(__this, method)
