﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.PlayFabClientAPI/UnlockContainerInstanceRequestCallback
struct UnlockContainerInstanceRequestCallback_t3270674530;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;
// PlayFab.ClientModels.UnlockContainerInstanceRequest
struct UnlockContainerInstanceRequest_t1476418829;
// System.IAsyncResult
struct IAsyncResult_t537683269;
// System.AsyncCallback
struct AsyncCallback_t1363551830;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_IntPtr676692020.h"
#include "mscorlib_System_String968488902.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_UnlockConta1476418829.h"
#include "mscorlib_System_AsyncCallback1363551830.h"

// System.Void PlayFab.PlayFabClientAPI/UnlockContainerInstanceRequestCallback::.ctor(System.Object,System.IntPtr)
extern "C"  void UnlockContainerInstanceRequestCallback__ctor_m964222033 (UnlockContainerInstanceRequestCallback_t3270674530 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/UnlockContainerInstanceRequestCallback::Invoke(System.String,System.Int32,PlayFab.ClientModels.UnlockContainerInstanceRequest,System.Object)
extern "C"  void UnlockContainerInstanceRequestCallback_Invoke_m2709626079 (UnlockContainerInstanceRequestCallback_t3270674530 * __this, String_t* ___urlPath0, int32_t ___callId1, UnlockContainerInstanceRequest_t1476418829 * ___request2, Il2CppObject * ___customData3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
extern "C" void pinvoke_delegate_wrapper_UnlockContainerInstanceRequestCallback_t3270674530(Il2CppObject* delegate, String_t* ___urlPath0, int32_t ___callId1, UnlockContainerInstanceRequest_t1476418829 * ___request2, Il2CppObject * ___customData3);
// System.IAsyncResult PlayFab.PlayFabClientAPI/UnlockContainerInstanceRequestCallback::BeginInvoke(System.String,System.Int32,PlayFab.ClientModels.UnlockContainerInstanceRequest,System.Object,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * UnlockContainerInstanceRequestCallback_BeginInvoke_m1601220010 (UnlockContainerInstanceRequestCallback_t3270674530 * __this, String_t* ___urlPath0, int32_t ___callId1, UnlockContainerInstanceRequest_t1476418829 * ___request2, Il2CppObject * ___customData3, AsyncCallback_t1363551830 * ___callback4, Il2CppObject * ___object5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/UnlockContainerInstanceRequestCallback::EndInvoke(System.IAsyncResult)
extern "C"  void UnlockContainerInstanceRequestCallback_EndInvoke_m3170967265 (UnlockContainerInstanceRequestCallback_t3270674530 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
