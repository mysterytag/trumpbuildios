﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ScaleAdjuster
struct ScaleAdjuster_t209014726;

#include "codegen/il2cpp-codegen.h"

// System.Void ScaleAdjuster::.ctor()
extern "C"  void ScaleAdjuster__ctor_m2828759141 (ScaleAdjuster_t209014726 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ScaleAdjuster::Awake()
extern "C"  void ScaleAdjuster_Awake_m3066364360 (ScaleAdjuster_t209014726 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
