﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Triggers.ObservablePointerEnterTrigger
struct ObservablePointerEnterTrigger_t2503405307;
// UnityEngine.EventSystems.PointerEventData
struct PointerEventData_t3205101634;
// UniRx.IObservable`1<UnityEngine.EventSystems.PointerEventData>
struct IObservable_1_t2963899998;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_PointerEve3205101634.h"

// System.Void UniRx.Triggers.ObservablePointerEnterTrigger::.ctor()
extern "C"  void ObservablePointerEnterTrigger__ctor_m3305304336 (ObservablePointerEnterTrigger_t2503405307 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.Triggers.ObservablePointerEnterTrigger::UnityEngine.EventSystems.IPointerEnterHandler.OnPointerEnter(UnityEngine.EventSystems.PointerEventData)
extern "C"  void ObservablePointerEnterTrigger_UnityEngine_EventSystems_IPointerEnterHandler_OnPointerEnter_m1601943637 (ObservablePointerEnterTrigger_t2503405307 * __this, PointerEventData_t3205101634 * ___eventData0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UniRx.IObservable`1<UnityEngine.EventSystems.PointerEventData> UniRx.Triggers.ObservablePointerEnterTrigger::OnPointerEnterAsObservable()
extern "C"  Il2CppObject* ObservablePointerEnterTrigger_OnPointerEnterAsObservable_m4102962279 (ObservablePointerEnterTrigger_t2503405307 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.Triggers.ObservablePointerEnterTrigger::RaiseOnCompletedOnDestroy()
extern "C"  void ObservablePointerEnterTrigger_RaiseOnCompletedOnDestroy_m381984841 (ObservablePointerEnterTrigger_t2503405307 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
