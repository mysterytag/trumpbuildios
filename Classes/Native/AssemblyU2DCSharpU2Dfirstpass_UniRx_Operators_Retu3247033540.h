﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UniRx.IScheduler
struct IScheduler_t2938318244;

#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Oper1622431009.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Unit2558286038.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Operators.ReturnObservable`1<UniRx.Unit>
struct  ReturnObservable_1_t3247033540  : public OperatorObservableBase_1_t1622431009
{
public:
	// T UniRx.Operators.ReturnObservable`1::value
	Unit_t2558286038  ___value_1;
	// UniRx.IScheduler UniRx.Operators.ReturnObservable`1::scheduler
	Il2CppObject * ___scheduler_2;

public:
	inline static int32_t get_offset_of_value_1() { return static_cast<int32_t>(offsetof(ReturnObservable_1_t3247033540, ___value_1)); }
	inline Unit_t2558286038  get_value_1() const { return ___value_1; }
	inline Unit_t2558286038 * get_address_of_value_1() { return &___value_1; }
	inline void set_value_1(Unit_t2558286038  value)
	{
		___value_1 = value;
	}

	inline static int32_t get_offset_of_scheduler_2() { return static_cast<int32_t>(offsetof(ReturnObservable_1_t3247033540, ___scheduler_2)); }
	inline Il2CppObject * get_scheduler_2() const { return ___scheduler_2; }
	inline Il2CppObject ** get_address_of_scheduler_2() { return &___scheduler_2; }
	inline void set_scheduler_2(Il2CppObject * value)
	{
		___scheduler_2 = value;
		Il2CppCodeGenWriteBarrier(&___scheduler_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
