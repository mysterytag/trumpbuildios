﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.PlayFabClientAPI/UnlinkGameCenterAccountRequestCallback
struct UnlinkGameCenterAccountRequestCallback_t3190542369;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;
// PlayFab.ClientModels.UnlinkGameCenterAccountRequest
struct UnlinkGameCenterAccountRequest_t687920076;
// System.IAsyncResult
struct IAsyncResult_t537683269;
// System.AsyncCallback
struct AsyncCallback_t1363551830;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_IntPtr676692020.h"
#include "mscorlib_System_String968488902.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_UnlinkGameCe687920076.h"
#include "mscorlib_System_AsyncCallback1363551830.h"

// System.Void PlayFab.PlayFabClientAPI/UnlinkGameCenterAccountRequestCallback::.ctor(System.Object,System.IntPtr)
extern "C"  void UnlinkGameCenterAccountRequestCallback__ctor_m34697360 (UnlinkGameCenterAccountRequestCallback_t3190542369 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/UnlinkGameCenterAccountRequestCallback::Invoke(System.String,System.Int32,PlayFab.ClientModels.UnlinkGameCenterAccountRequest,System.Object)
extern "C"  void UnlinkGameCenterAccountRequestCallback_Invoke_m1801189567 (UnlinkGameCenterAccountRequestCallback_t3190542369 * __this, String_t* ___urlPath0, int32_t ___callId1, UnlinkGameCenterAccountRequest_t687920076 * ___request2, Il2CppObject * ___customData3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
extern "C" void pinvoke_delegate_wrapper_UnlinkGameCenterAccountRequestCallback_t3190542369(Il2CppObject* delegate, String_t* ___urlPath0, int32_t ___callId1, UnlinkGameCenterAccountRequest_t687920076 * ___request2, Il2CppObject * ___customData3);
// System.IAsyncResult PlayFab.PlayFabClientAPI/UnlinkGameCenterAccountRequestCallback::BeginInvoke(System.String,System.Int32,PlayFab.ClientModels.UnlinkGameCenterAccountRequest,System.Object,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * UnlinkGameCenterAccountRequestCallback_BeginInvoke_m3615568936 (UnlinkGameCenterAccountRequestCallback_t3190542369 * __this, String_t* ___urlPath0, int32_t ___callId1, UnlinkGameCenterAccountRequest_t687920076 * ___request2, Il2CppObject * ___customData3, AsyncCallback_t1363551830 * ___callback4, Il2CppObject * ___object5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/UnlinkGameCenterAccountRequestCallback::EndInvoke(System.IAsyncResult)
extern "C"  void UnlinkGameCenterAccountRequestCallback_EndInvoke_m1297328800 (UnlinkGameCenterAccountRequestCallback_t3190542369 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
