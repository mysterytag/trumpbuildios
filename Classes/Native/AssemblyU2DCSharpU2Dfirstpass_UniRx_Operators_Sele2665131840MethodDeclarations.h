﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.SelectManyObservable`3/SelectManyEnumerableObserver<System.Object,UniRx.Unit,System.Object>
struct SelectManyEnumerableObserver_t2665131840;
// UniRx.Operators.SelectManyObservable`3<System.Object,UniRx.Unit,System.Object>
struct SelectManyObservable_3_t1259497621;
// UniRx.IObserver`1<System.Object>
struct IObserver_1_t3049105323;
// System.IDisposable
struct IDisposable_t1628921374;
// System.Object
struct Il2CppObject;
// System.Exception
struct Exception_t1967233988;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_Exception1967233988.h"

// System.Void UniRx.Operators.SelectManyObservable`3/SelectManyEnumerableObserver<System.Object,UniRx.Unit,System.Object>::.ctor(UniRx.Operators.SelectManyObservable`3<TSource,TCollection,TResult>,UniRx.IObserver`1<TResult>,System.IDisposable)
extern "C"  void SelectManyEnumerableObserver__ctor_m169532688_gshared (SelectManyEnumerableObserver_t2665131840 * __this, SelectManyObservable_3_t1259497621 * ___parent0, Il2CppObject* ___observer1, Il2CppObject * ___cancel2, const MethodInfo* method);
#define SelectManyEnumerableObserver__ctor_m169532688(__this, ___parent0, ___observer1, ___cancel2, method) ((  void (*) (SelectManyEnumerableObserver_t2665131840 *, SelectManyObservable_3_t1259497621 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))SelectManyEnumerableObserver__ctor_m169532688_gshared)(__this, ___parent0, ___observer1, ___cancel2, method)
// System.IDisposable UniRx.Operators.SelectManyObservable`3/SelectManyEnumerableObserver<System.Object,UniRx.Unit,System.Object>::Run()
extern "C"  Il2CppObject * SelectManyEnumerableObserver_Run_m3836178677_gshared (SelectManyEnumerableObserver_t2665131840 * __this, const MethodInfo* method);
#define SelectManyEnumerableObserver_Run_m3836178677(__this, method) ((  Il2CppObject * (*) (SelectManyEnumerableObserver_t2665131840 *, const MethodInfo*))SelectManyEnumerableObserver_Run_m3836178677_gshared)(__this, method)
// System.Void UniRx.Operators.SelectManyObservable`3/SelectManyEnumerableObserver<System.Object,UniRx.Unit,System.Object>::OnNext(TSource)
extern "C"  void SelectManyEnumerableObserver_OnNext_m1255727006_gshared (SelectManyEnumerableObserver_t2665131840 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define SelectManyEnumerableObserver_OnNext_m1255727006(__this, ___value0, method) ((  void (*) (SelectManyEnumerableObserver_t2665131840 *, Il2CppObject *, const MethodInfo*))SelectManyEnumerableObserver_OnNext_m1255727006_gshared)(__this, ___value0, method)
// System.Void UniRx.Operators.SelectManyObservable`3/SelectManyEnumerableObserver<System.Object,UniRx.Unit,System.Object>::OnError(System.Exception)
extern "C"  void SelectManyEnumerableObserver_OnError_m3597480328_gshared (SelectManyEnumerableObserver_t2665131840 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method);
#define SelectManyEnumerableObserver_OnError_m3597480328(__this, ___error0, method) ((  void (*) (SelectManyEnumerableObserver_t2665131840 *, Exception_t1967233988 *, const MethodInfo*))SelectManyEnumerableObserver_OnError_m3597480328_gshared)(__this, ___error0, method)
// System.Void UniRx.Operators.SelectManyObservable`3/SelectManyEnumerableObserver<System.Object,UniRx.Unit,System.Object>::OnCompleted()
extern "C"  void SelectManyEnumerableObserver_OnCompleted_m1578365147_gshared (SelectManyEnumerableObserver_t2665131840 * __this, const MethodInfo* method);
#define SelectManyEnumerableObserver_OnCompleted_m1578365147(__this, method) ((  void (*) (SelectManyEnumerableObserver_t2665131840 *, const MethodInfo*))SelectManyEnumerableObserver_OnCompleted_m1578365147_gshared)(__this, method)
