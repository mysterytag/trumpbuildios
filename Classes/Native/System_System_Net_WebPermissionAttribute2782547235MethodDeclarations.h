﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Net.WebPermissionAttribute
struct WebPermissionAttribute_t2782547235;
// System.String
struct String_t;
// System.Security.IPermission
struct IPermission_t2562055037;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Security_Permissions_SecurityActio3968659629.h"
#include "mscorlib_System_String968488902.h"

// System.Void System.Net.WebPermissionAttribute::.ctor(System.Security.Permissions.SecurityAction)
extern "C"  void WebPermissionAttribute__ctor_m1384645522 (WebPermissionAttribute_t2782547235 * __this, int32_t ___action0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Net.WebPermissionAttribute::get_Accept()
extern "C"  String_t* WebPermissionAttribute_get_Accept_m2237816145 (WebPermissionAttribute_t2782547235 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.WebPermissionAttribute::set_Accept(System.String)
extern "C"  void WebPermissionAttribute_set_Accept_m1639332218 (WebPermissionAttribute_t2782547235 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Net.WebPermissionAttribute::get_AcceptPattern()
extern "C"  String_t* WebPermissionAttribute_get_AcceptPattern_m1541872161 (WebPermissionAttribute_t2782547235 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.WebPermissionAttribute::set_AcceptPattern(System.String)
extern "C"  void WebPermissionAttribute_set_AcceptPattern_m1420518968 (WebPermissionAttribute_t2782547235 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Net.WebPermissionAttribute::get_Connect()
extern "C"  String_t* WebPermissionAttribute_get_Connect_m2191445795 (WebPermissionAttribute_t2782547235 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.WebPermissionAttribute::set_Connect(System.String)
extern "C"  void WebPermissionAttribute_set_Connect_m2234792374 (WebPermissionAttribute_t2782547235 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Net.WebPermissionAttribute::get_ConnectPattern()
extern "C"  String_t* WebPermissionAttribute_get_ConnectPattern_m3736098831 (WebPermissionAttribute_t2782547235 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.WebPermissionAttribute::set_ConnectPattern(System.String)
extern "C"  void WebPermissionAttribute_set_ConnectPattern_m1077144188 (WebPermissionAttribute_t2782547235 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.IPermission System.Net.WebPermissionAttribute::CreatePermission()
extern "C"  Il2CppObject * WebPermissionAttribute_CreatePermission_m2836911396 (WebPermissionAttribute_t2782547235 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.WebPermissionAttribute::AlreadySet(System.String,System.String)
extern "C"  void WebPermissionAttribute_AlreadySet_m1367521521 (WebPermissionAttribute_t2782547235 * __this, String_t* ___parameter0, String_t* ___property1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
