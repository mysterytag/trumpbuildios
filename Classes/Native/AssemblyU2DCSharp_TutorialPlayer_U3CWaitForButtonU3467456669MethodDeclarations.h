﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// TutorialPlayer/<WaitForButton>c__Iterator22
struct U3CWaitForButtonU3Ec__Iterator22_t467456669;
// System.Object
struct Il2CppObject;
// TableViewCellBase
struct TableViewCellBase_t2057844710;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_TableViewCellBase2057844710.h"
#include "mscorlib_System_String968488902.h"

// System.Void TutorialPlayer/<WaitForButton>c__Iterator22::.ctor()
extern "C"  void U3CWaitForButtonU3Ec__Iterator22__ctor_m611742558 (U3CWaitForButtonU3Ec__Iterator22_t467456669 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object TutorialPlayer/<WaitForButton>c__Iterator22::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CWaitForButtonU3Ec__Iterator22_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1967068724 (U3CWaitForButtonU3Ec__Iterator22_t467456669 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object TutorialPlayer/<WaitForButton>c__Iterator22::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CWaitForButtonU3Ec__Iterator22_System_Collections_IEnumerator_get_Current_m4155245000 (U3CWaitForButtonU3Ec__Iterator22_t467456669 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean TutorialPlayer/<WaitForButton>c__Iterator22::MoveNext()
extern "C"  bool U3CWaitForButtonU3Ec__Iterator22_MoveNext_m4254643158 (U3CWaitForButtonU3Ec__Iterator22_t467456669 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TutorialPlayer/<WaitForButton>c__Iterator22::Dispose()
extern "C"  void U3CWaitForButtonU3Ec__Iterator22_Dispose_m3667329051 (U3CWaitForButtonU3Ec__Iterator22_t467456669 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TutorialPlayer/<WaitForButton>c__Iterator22::Reset()
extern "C"  void U3CWaitForButtonU3Ec__Iterator22_Reset_m2553142795 (U3CWaitForButtonU3Ec__Iterator22_t467456669 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean TutorialPlayer/<WaitForButton>c__Iterator22::<>m__14E(TableViewCellBase)
extern "C"  bool U3CWaitForButtonU3Ec__Iterator22_U3CU3Em__14E_m2609569859 (U3CWaitForButtonU3Ec__Iterator22_t467456669 * __this, TableViewCellBase_t2057844710 * ___cell0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TutorialPlayer/<WaitForButton>c__Iterator22::<>m__14F(TableViewCellBase)
extern "C"  void U3CWaitForButtonU3Ec__Iterator22_U3CU3Em__14F_m995777518 (U3CWaitForButtonU3Ec__Iterator22_t467456669 * __this, TableViewCellBase_t2057844710 * ___cell0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean TutorialPlayer/<WaitForButton>c__Iterator22::<>m__150(System.String)
extern "C"  bool U3CWaitForButtonU3Ec__Iterator22_U3CU3Em__150_m3130347375 (U3CWaitForButtonU3Ec__Iterator22_t467456669 * __this, String_t* ___s0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
