﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Scheduler/ThreadPoolScheduler/<Schedule>c__AnonStorey79
struct U3CScheduleU3Ec__AnonStorey79_t3913535390;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"

// System.Void UniRx.Scheduler/ThreadPoolScheduler/<Schedule>c__AnonStorey79::.ctor()
extern "C"  void U3CScheduleU3Ec__AnonStorey79__ctor_m1320767327 (U3CScheduleU3Ec__AnonStorey79_t3913535390 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.Scheduler/ThreadPoolScheduler/<Schedule>c__AnonStorey79::<>m__A0(System.Object)
extern "C"  void U3CScheduleU3Ec__AnonStorey79_U3CU3Em__A0_m3910042685 (U3CScheduleU3Ec__AnonStorey79_t3913535390 * __this, Il2CppObject * ____0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
