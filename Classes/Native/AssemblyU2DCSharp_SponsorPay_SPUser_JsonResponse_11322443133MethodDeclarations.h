﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SponsorPay.SPUser/JsonResponse`1<System.Nullable`1<SponsorPay.SPUserEducation>>
struct JsonResponse_1_t1322443133;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String968488902.h"
#include "mscorlib_System_Nullable_1_gen4294542862.h"

// System.Void SponsorPay.SPUser/JsonResponse`1<System.Nullable`1<SponsorPay.SPUserEducation>>::.ctor()
extern "C"  void JsonResponse_1__ctor_m1872028901_gshared (JsonResponse_1_t1322443133 * __this, const MethodInfo* method);
#define JsonResponse_1__ctor_m1872028901(__this, method) ((  void (*) (JsonResponse_1_t1322443133 *, const MethodInfo*))JsonResponse_1__ctor_m1872028901_gshared)(__this, method)
// System.String SponsorPay.SPUser/JsonResponse`1<System.Nullable`1<SponsorPay.SPUserEducation>>::get_key()
extern "C"  String_t* JsonResponse_1_get_key_m1310521980_gshared (JsonResponse_1_t1322443133 * __this, const MethodInfo* method);
#define JsonResponse_1_get_key_m1310521980(__this, method) ((  String_t* (*) (JsonResponse_1_t1322443133 *, const MethodInfo*))JsonResponse_1_get_key_m1310521980_gshared)(__this, method)
// System.Void SponsorPay.SPUser/JsonResponse`1<System.Nullable`1<SponsorPay.SPUserEducation>>::set_key(System.String)
extern "C"  void JsonResponse_1_set_key_m2329592061_gshared (JsonResponse_1_t1322443133 * __this, String_t* ___value0, const MethodInfo* method);
#define JsonResponse_1_set_key_m2329592061(__this, ___value0, method) ((  void (*) (JsonResponse_1_t1322443133 *, String_t*, const MethodInfo*))JsonResponse_1_set_key_m2329592061_gshared)(__this, ___value0, method)
// T SponsorPay.SPUser/JsonResponse`1<System.Nullable`1<SponsorPay.SPUserEducation>>::get_value()
extern "C"  Nullable_1_t4294542862  JsonResponse_1_get_value_m2694535530_gshared (JsonResponse_1_t1322443133 * __this, const MethodInfo* method);
#define JsonResponse_1_get_value_m2694535530(__this, method) ((  Nullable_1_t4294542862  (*) (JsonResponse_1_t1322443133 *, const MethodInfo*))JsonResponse_1_get_value_m2694535530_gshared)(__this, method)
// System.Void SponsorPay.SPUser/JsonResponse`1<System.Nullable`1<SponsorPay.SPUserEducation>>::set_value(T)
extern "C"  void JsonResponse_1_set_value_m2629781703_gshared (JsonResponse_1_t1322443133 * __this, Nullable_1_t4294542862  ___value0, const MethodInfo* method);
#define JsonResponse_1_set_value_m2629781703(__this, ___value0, method) ((  void (*) (JsonResponse_1_t1322443133 *, Nullable_1_t4294542862 , const MethodInfo*))JsonResponse_1_set_value_m2629781703_gshared)(__this, ___value0, method)
// System.String SponsorPay.SPUser/JsonResponse`1<System.Nullable`1<SponsorPay.SPUserEducation>>::get_error()
extern "C"  String_t* JsonResponse_1_get_error_m324051173_gshared (JsonResponse_1_t1322443133 * __this, const MethodInfo* method);
#define JsonResponse_1_get_error_m324051173(__this, method) ((  String_t* (*) (JsonResponse_1_t1322443133 *, const MethodInfo*))JsonResponse_1_get_error_m324051173_gshared)(__this, method)
// System.Void SponsorPay.SPUser/JsonResponse`1<System.Nullable`1<SponsorPay.SPUserEducation>>::set_error(System.String)
extern "C"  void JsonResponse_1_set_error_m3715869556_gshared (JsonResponse_1_t1322443133 * __this, String_t* ___value0, const MethodInfo* method);
#define JsonResponse_1_set_error_m3715869556(__this, ___value0, method) ((  void (*) (JsonResponse_1_t1322443133 *, String_t*, const MethodInfo*))JsonResponse_1_set_error_m3715869556_gshared)(__this, ___value0, method)
