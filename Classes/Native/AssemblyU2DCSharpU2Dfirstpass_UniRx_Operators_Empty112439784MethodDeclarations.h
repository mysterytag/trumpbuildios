﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.EmptyObservable`1/Empty<UniRx.CollectionMoveEvent`1<UniRx.Tuple`2<System.Object,System.Object>>>
struct Empty_t112439784;
// UniRx.IObserver`1<UniRx.CollectionMoveEvent`1<UniRx.Tuple`2<System.Object,System.Object>>>
struct IObserver_1_t365620853;
// System.IDisposable
struct IDisposable_t1628921374;
// System.Exception
struct Exception_t1967233988;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_CollectionMove2448589246.h"
#include "mscorlib_System_Exception1967233988.h"

// System.Void UniRx.Operators.EmptyObservable`1/Empty<UniRx.CollectionMoveEvent`1<UniRx.Tuple`2<System.Object,System.Object>>>::.ctor(UniRx.IObserver`1<T>,System.IDisposable)
extern "C"  void Empty__ctor_m1794107855_gshared (Empty_t112439784 * __this, Il2CppObject* ___observer0, Il2CppObject * ___cancel1, const MethodInfo* method);
#define Empty__ctor_m1794107855(__this, ___observer0, ___cancel1, method) ((  void (*) (Empty_t112439784 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))Empty__ctor_m1794107855_gshared)(__this, ___observer0, ___cancel1, method)
// System.Void UniRx.Operators.EmptyObservable`1/Empty<UniRx.CollectionMoveEvent`1<UniRx.Tuple`2<System.Object,System.Object>>>::OnNext(T)
extern "C"  void Empty_OnNext_m1088463971_gshared (Empty_t112439784 * __this, CollectionMoveEvent_1_t2448589246  ___value0, const MethodInfo* method);
#define Empty_OnNext_m1088463971(__this, ___value0, method) ((  void (*) (Empty_t112439784 *, CollectionMoveEvent_1_t2448589246 , const MethodInfo*))Empty_OnNext_m1088463971_gshared)(__this, ___value0, method)
// System.Void UniRx.Operators.EmptyObservable`1/Empty<UniRx.CollectionMoveEvent`1<UniRx.Tuple`2<System.Object,System.Object>>>::OnError(System.Exception)
extern "C"  void Empty_OnError_m1879939442_gshared (Empty_t112439784 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method);
#define Empty_OnError_m1879939442(__this, ___error0, method) ((  void (*) (Empty_t112439784 *, Exception_t1967233988 *, const MethodInfo*))Empty_OnError_m1879939442_gshared)(__this, ___error0, method)
// System.Void UniRx.Operators.EmptyObservable`1/Empty<UniRx.CollectionMoveEvent`1<UniRx.Tuple`2<System.Object,System.Object>>>::OnCompleted()
extern "C"  void Empty_OnCompleted_m846292933_gshared (Empty_t112439784 * __this, const MethodInfo* method);
#define Empty_OnCompleted_m846292933(__this, method) ((  void (*) (Empty_t112439784 *, const MethodInfo*))Empty_OnCompleted_m846292933_gshared)(__this, method)
