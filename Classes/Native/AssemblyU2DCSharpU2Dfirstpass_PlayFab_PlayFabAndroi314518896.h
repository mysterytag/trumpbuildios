﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PlayFab.PlayFabAndroidPlugin
struct  PlayFabAndroidPlugin_t314518896  : public Il2CppObject
{
public:

public:
};

struct PlayFabAndroidPlugin_t314518896_StaticFields
{
public:
	// System.Boolean PlayFab.PlayFabAndroidPlugin::Initted
	bool ___Initted_0;

public:
	inline static int32_t get_offset_of_Initted_0() { return static_cast<int32_t>(offsetof(PlayFabAndroidPlugin_t314518896_StaticFields, ___Initted_0)); }
	inline bool get_Initted_0() const { return ___Initted_0; }
	inline bool* get_address_of_Initted_0() { return &___Initted_0; }
	inline void set_Initted_0(bool value)
	{
		___Initted_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
