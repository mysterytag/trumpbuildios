﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CellReactiveApi/<Merge>c__AnonStorey112`3<System.Double,System.Int32,System.Double>
struct U3CMergeU3Ec__AnonStorey112_3_t3705143050;
// System.IDisposable
struct IDisposable_t1628921374;
// System.Action`1<System.Double>
struct Action_1_t682969319;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Priority3194150340.h"

// System.Void CellReactiveApi/<Merge>c__AnonStorey112`3<System.Double,System.Int32,System.Double>::.ctor()
extern "C"  void U3CMergeU3Ec__AnonStorey112_3__ctor_m3734451752_gshared (U3CMergeU3Ec__AnonStorey112_3_t3705143050 * __this, const MethodInfo* method);
#define U3CMergeU3Ec__AnonStorey112_3__ctor_m3734451752(__this, method) ((  void (*) (U3CMergeU3Ec__AnonStorey112_3_t3705143050 *, const MethodInfo*))U3CMergeU3Ec__AnonStorey112_3__ctor_m3734451752_gshared)(__this, method)
// T3 CellReactiveApi/<Merge>c__AnonStorey112`3<System.Double,System.Int32,System.Double>::<>m__186()
extern "C"  double U3CMergeU3Ec__AnonStorey112_3_U3CU3Em__186_m2791000706_gshared (U3CMergeU3Ec__AnonStorey112_3_t3705143050 * __this, const MethodInfo* method);
#define U3CMergeU3Ec__AnonStorey112_3_U3CU3Em__186_m2791000706(__this, method) ((  double (*) (U3CMergeU3Ec__AnonStorey112_3_t3705143050 *, const MethodInfo*))U3CMergeU3Ec__AnonStorey112_3_U3CU3Em__186_m2791000706_gshared)(__this, method)
// System.IDisposable CellReactiveApi/<Merge>c__AnonStorey112`3<System.Double,System.Int32,System.Double>::<>m__187(System.Action`1<T3>,Priority)
extern "C"  Il2CppObject * U3CMergeU3Ec__AnonStorey112_3_U3CU3Em__187_m2303144199_gshared (U3CMergeU3Ec__AnonStorey112_3_t3705143050 * __this, Action_1_t682969319 * ___reaction0, int32_t ___p1, const MethodInfo* method);
#define U3CMergeU3Ec__AnonStorey112_3_U3CU3Em__187_m2303144199(__this, ___reaction0, ___p1, method) ((  Il2CppObject * (*) (U3CMergeU3Ec__AnonStorey112_3_t3705143050 *, Action_1_t682969319 *, int32_t, const MethodInfo*))U3CMergeU3Ec__AnonStorey112_3_U3CU3Em__187_m2303144199_gshared)(__this, ___reaction0, ___p1, method)
