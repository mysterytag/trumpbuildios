﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.InternalUtil.DisposedObserver`1<UnityEngine.NetworkMessageInfo>
struct DisposedObserver_1_t1748802346;
// System.Exception
struct Exception_t1967233988;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Exception1967233988.h"
#include "UnityEngine_UnityEngine_NetworkMessageInfo2574344884.h"

// System.Void UniRx.InternalUtil.DisposedObserver`1<UnityEngine.NetworkMessageInfo>::.ctor()
extern "C"  void DisposedObserver_1__ctor_m4154606128_gshared (DisposedObserver_1_t1748802346 * __this, const MethodInfo* method);
#define DisposedObserver_1__ctor_m4154606128(__this, method) ((  void (*) (DisposedObserver_1_t1748802346 *, const MethodInfo*))DisposedObserver_1__ctor_m4154606128_gshared)(__this, method)
// System.Void UniRx.InternalUtil.DisposedObserver`1<UnityEngine.NetworkMessageInfo>::.cctor()
extern "C"  void DisposedObserver_1__cctor_m3756642173_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define DisposedObserver_1__cctor_m3756642173(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))DisposedObserver_1__cctor_m3756642173_gshared)(__this /* static, unused */, method)
// System.Void UniRx.InternalUtil.DisposedObserver`1<UnityEngine.NetworkMessageInfo>::OnCompleted()
extern "C"  void DisposedObserver_1_OnCompleted_m3984527802_gshared (DisposedObserver_1_t1748802346 * __this, const MethodInfo* method);
#define DisposedObserver_1_OnCompleted_m3984527802(__this, method) ((  void (*) (DisposedObserver_1_t1748802346 *, const MethodInfo*))DisposedObserver_1_OnCompleted_m3984527802_gshared)(__this, method)
// System.Void UniRx.InternalUtil.DisposedObserver`1<UnityEngine.NetworkMessageInfo>::OnError(System.Exception)
extern "C"  void DisposedObserver_1_OnError_m3259694055_gshared (DisposedObserver_1_t1748802346 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method);
#define DisposedObserver_1_OnError_m3259694055(__this, ___error0, method) ((  void (*) (DisposedObserver_1_t1748802346 *, Exception_t1967233988 *, const MethodInfo*))DisposedObserver_1_OnError_m3259694055_gshared)(__this, ___error0, method)
// System.Void UniRx.InternalUtil.DisposedObserver`1<UnityEngine.NetworkMessageInfo>::OnNext(T)
extern "C"  void DisposedObserver_1_OnNext_m1087607000_gshared (DisposedObserver_1_t1748802346 * __this, NetworkMessageInfo_t2574344884  ___value0, const MethodInfo* method);
#define DisposedObserver_1_OnNext_m1087607000(__this, ___value0, method) ((  void (*) (DisposedObserver_1_t1748802346 *, NetworkMessageInfo_t2574344884 , const MethodInfo*))DisposedObserver_1_OnNext_m1087607000_gshared)(__this, ___value0, method)
