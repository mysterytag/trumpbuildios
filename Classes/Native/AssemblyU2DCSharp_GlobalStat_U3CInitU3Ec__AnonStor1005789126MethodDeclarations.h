﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GlobalStat/<Init>c__AnonStoreyE6
struct U3CInitU3Ec__AnonStoreyE6_t1005789126;

#include "codegen/il2cpp-codegen.h"

// System.Void GlobalStat/<Init>c__AnonStoreyE6::.ctor()
extern "C"  void U3CInitU3Ec__AnonStoreyE6__ctor_m2207371757 (U3CInitU3Ec__AnonStoreyE6_t1005789126 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GlobalStat/<Init>c__AnonStoreyE6::<>m__10C(System.Int32)
extern "C"  void U3CInitU3Ec__AnonStoreyE6_U3CU3Em__10C_m2388548993 (U3CInitU3Ec__AnonStoreyE6_t1005789126 * __this, int32_t ___i0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
