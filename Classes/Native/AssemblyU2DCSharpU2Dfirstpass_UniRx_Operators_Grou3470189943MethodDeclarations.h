﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.GroupByObservable`3/GroupBy<System.Object,System.Object,System.Object>
struct GroupBy_t3470189943;
// UniRx.Operators.GroupByObservable`3<System.Object,System.Object,System.Object>
struct GroupByObservable_3_t657897610;
// UniRx.IObserver`1<UniRx.IGroupedObservable`2<System.Object,System.Object>>
struct IObserver_1_t3518384802;
// System.IDisposable
struct IDisposable_t1628921374;
// System.Object
struct Il2CppObject;
// System.Exception
struct Exception_t1967233988;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_Exception1967233988.h"

// System.Void UniRx.Operators.GroupByObservable`3/GroupBy<System.Object,System.Object,System.Object>::.ctor(UniRx.Operators.GroupByObservable`3<TSource,TKey,TElement>,UniRx.IObserver`1<UniRx.IGroupedObservable`2<TKey,TElement>>,System.IDisposable)
extern "C"  void GroupBy__ctor_m1099347075_gshared (GroupBy_t3470189943 * __this, GroupByObservable_3_t657897610 * ___parent0, Il2CppObject* ___observer1, Il2CppObject * ___cancel2, const MethodInfo* method);
#define GroupBy__ctor_m1099347075(__this, ___parent0, ___observer1, ___cancel2, method) ((  void (*) (GroupBy_t3470189943 *, GroupByObservable_3_t657897610 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))GroupBy__ctor_m1099347075_gshared)(__this, ___parent0, ___observer1, ___cancel2, method)
// System.IDisposable UniRx.Operators.GroupByObservable`3/GroupBy<System.Object,System.Object,System.Object>::Run()
extern "C"  Il2CppObject * GroupBy_Run_m82052481_gshared (GroupBy_t3470189943 * __this, const MethodInfo* method);
#define GroupBy_Run_m82052481(__this, method) ((  Il2CppObject * (*) (GroupBy_t3470189943 *, const MethodInfo*))GroupBy_Run_m82052481_gshared)(__this, method)
// System.Void UniRx.Operators.GroupByObservable`3/GroupBy<System.Object,System.Object,System.Object>::OnNext(TSource)
extern "C"  void GroupBy_OnNext_m789374336_gshared (GroupBy_t3470189943 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define GroupBy_OnNext_m789374336(__this, ___value0, method) ((  void (*) (GroupBy_t3470189943 *, Il2CppObject *, const MethodInfo*))GroupBy_OnNext_m789374336_gshared)(__this, ___value0, method)
// System.Void UniRx.Operators.GroupByObservable`3/GroupBy<System.Object,System.Object,System.Object>::OnError(System.Exception)
extern "C"  void GroupBy_OnError_m4077176042_gshared (GroupBy_t3470189943 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method);
#define GroupBy_OnError_m4077176042(__this, ___error0, method) ((  void (*) (GroupBy_t3470189943 *, Exception_t1967233988 *, const MethodInfo*))GroupBy_OnError_m4077176042_gshared)(__this, ___error0, method)
// System.Void UniRx.Operators.GroupByObservable`3/GroupBy<System.Object,System.Object,System.Object>::OnCompleted()
extern "C"  void GroupBy_OnCompleted_m3119777597_gshared (GroupBy_t3470189943 * __this, const MethodInfo* method);
#define GroupBy_OnCompleted_m3119777597(__this, method) ((  void (*) (GroupBy_t3470189943 *, const MethodInfo*))GroupBy_OnCompleted_m3119777597_gshared)(__this, method)
// System.Void UniRx.Operators.GroupByObservable`3/GroupBy<System.Object,System.Object,System.Object>::Error(System.Exception)
extern "C"  void GroupBy_Error_m2267572681_gshared (GroupBy_t3470189943 * __this, Exception_t1967233988 * ___exception0, const MethodInfo* method);
#define GroupBy_Error_m2267572681(__this, ___exception0, method) ((  void (*) (GroupBy_t3470189943 *, Exception_t1967233988 *, const MethodInfo*))GroupBy_Error_m2267572681_gshared)(__this, ___exception0, method)
