﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.CombineLatestObservable`3<System.Boolean,System.Int32,System.Object>
struct CombineLatestObservable_3_t2543452560;
// UniRx.IObservable`1<System.Boolean>
struct IObservable_1_t4264771001;
// UniRx.IObservable`1<System.Int32>
struct IObservable_1_t2606213151;
// System.Func`3<System.Boolean,System.Int32,System.Object>
struct Func_3_t1694707683;
// System.IDisposable
struct IDisposable_t1628921374;
// UniRx.IObserver`1<System.Object>
struct IObserver_1_t3049105323;

#include "codegen/il2cpp-codegen.h"

// System.Void UniRx.Operators.CombineLatestObservable`3<System.Boolean,System.Int32,System.Object>::.ctor(UniRx.IObservable`1<TLeft>,UniRx.IObservable`1<TRight>,System.Func`3<TLeft,TRight,TResult>)
extern "C"  void CombineLatestObservable_3__ctor_m247565635_gshared (CombineLatestObservable_3_t2543452560 * __this, Il2CppObject* ___left0, Il2CppObject* ___right1, Func_3_t1694707683 * ___selector2, const MethodInfo* method);
#define CombineLatestObservable_3__ctor_m247565635(__this, ___left0, ___right1, ___selector2, method) ((  void (*) (CombineLatestObservable_3_t2543452560 *, Il2CppObject*, Il2CppObject*, Func_3_t1694707683 *, const MethodInfo*))CombineLatestObservable_3__ctor_m247565635_gshared)(__this, ___left0, ___right1, ___selector2, method)
// System.IDisposable UniRx.Operators.CombineLatestObservable`3<System.Boolean,System.Int32,System.Object>::SubscribeCore(UniRx.IObserver`1<TResult>,System.IDisposable)
extern "C"  Il2CppObject * CombineLatestObservable_3_SubscribeCore_m1573197955_gshared (CombineLatestObservable_3_t2543452560 * __this, Il2CppObject* ___observer0, Il2CppObject * ___cancel1, const MethodInfo* method);
#define CombineLatestObservable_3_SubscribeCore_m1573197955(__this, ___observer0, ___cancel1, method) ((  Il2CppObject * (*) (CombineLatestObservable_3_t2543452560 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))CombineLatestObservable_3_SubscribeCore_m1573197955_gshared)(__this, ___observer0, ___cancel1, method)
