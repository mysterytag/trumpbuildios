﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

// UniRx.LazyTask`1<System.Int32>
struct LazyTask_1_t4247718180;
// UniRx.IObservable`1<System.Int32>
struct IObservable_1_t2606213151;
// System.Exception
struct Exception_t1967233988;
// UnityEngine.Coroutine
struct Coroutine_t2246592261;
struct Coroutine_t2246592261_marshaled_pinvoke;
// System.Action`1<System.Int32>
struct Action_1_t2995867492;
// System.Action`1<System.Exception>
struct Action_1_t2115686693;
// System.String
struct String_t;
// UniRx.LazyTask`1<System.Object>
struct LazyTask_1_t2237409813;
// UniRx.IObservable`1<System.Object>
struct IObservable_1_t595904784;
// System.Object
struct Il2CppObject;
// System.Action`1<System.Object>
struct Action_1_t985559125;
// UniRx.Notification`1/<ToObservable>c__AnonStorey55/<ToObservable>c__AnonStorey56<System.Object>
struct U3CToObservableU3Ec__AnonStorey56_t3651252413;
// UniRx.Notification`1/<ToObservable>c__AnonStorey55<System.Object>
struct U3CToObservableU3Ec__AnonStorey55_t1984678244;
// System.IDisposable
struct IDisposable_t1628921374;
// UniRx.IObserver`1<System.Object>
struct IObserver_1_t3049105323;
// UniRx.Notification`1/OnCompletedNotification<System.Object>
struct OnCompletedNotification_t3050132088;
// UniRx.Notification`1<System.Object>
struct Notification_1_t38356375;
// System.Action
struct Action_t437523947;
// UniRx.Notification`1/OnErrorNotification<System.Object>
struct OnErrorNotification_t36474669;
// UniRx.Notification`1/OnNextNotification<System.Object>
struct OnNextNotification_t3645104142;
// UniRx.IScheduler
struct IScheduler_t2938318244;
// System.Func`2<UniRx.IObserver`1<System.Object>,System.IDisposable>
struct Func_2_t2619763055;
// UniRx.Observable/<AddRef>c__AnonStorey37`1<System.Object>
struct U3CAddRefU3Ec__AnonStorey37_1_t3332811877;
// UniRx.Observable/<CatchIgnore>c__AnonStorey3C`2<System.Object,System.Object>
struct U3CCatchIgnoreU3Ec__AnonStorey3C_2_t339163467;
// UniRx.Observable/<CombineSources>c__IteratorD`1<System.Object>
struct U3CCombineSourcesU3Ec__IteratorD_1_t2571110558;
// System.Collections.IEnumerator
struct IEnumerator_t287207039;
// System.Collections.Generic.IEnumerator`1<UniRx.IObservable`1<System.Object>>
struct IEnumerator_1_t2079011232;
// UniRx.Observable/<CombineSources>c__IteratorD`1<UniRx.Unit>
struct U3CCombineSourcesU3Ec__IteratorD_1_t4292290176;
// UniRx.IObservable`1<UniRx.Unit>
struct IObservable_1_t2317084402;
// System.Collections.Generic.IEnumerator`1<UniRx.IObservable`1<UniRx.Unit>>
struct IEnumerator_1_t3800190850;
// UniRx.Observable/<ContinueWith>c__AnonStorey48`2<System.Object,System.Object>
struct U3CContinueWithU3Ec__AnonStorey48_2_t215346063;
// UniRx.Observable/<FromAsyncPattern>c__AnonStorey3F`1/<FromAsyncPattern>c__AnonStorey40`1<System.Object>
struct U3CFromAsyncPatternU3Ec__AnonStorey40_1_t1660482281;
// System.IAsyncResult
struct IAsyncResult_t537683269;
// UniRx.Observable/<FromAsyncPattern>c__AnonStorey3F`1/<FromAsyncPattern>c__AnonStorey40`1<UniRx.Unit>
struct U3CFromAsyncPatternU3Ec__AnonStorey40_1_t3381661899;
// UniRx.Observable/<FromAsyncPattern>c__AnonStorey3F`1<System.Object>
struct U3CFromAsyncPatternU3Ec__AnonStorey3F_1_t1370739976;
// UniRx.Observable/<FromAsyncPattern>c__AnonStorey3F`1<UniRx.Unit>
struct U3CFromAsyncPatternU3Ec__AnonStorey3F_1_t3091919594;
// UniRx.Observable/<FromAsyncPattern>c__AnonStorey41`2/<FromAsyncPattern>c__AnonStorey42`2<System.Object,System.Object>
struct U3CFromAsyncPatternU3Ec__AnonStorey42_2_t2629939760;
// UniRx.Observable/<FromAsyncPattern>c__AnonStorey41`2/<FromAsyncPattern>c__AnonStorey42`2<System.Object,UniRx.Unit>
struct U3CFromAsyncPatternU3Ec__AnonStorey42_2_t56152082;
// UniRx.Observable/<FromAsyncPattern>c__AnonStorey41`2<System.Object,System.Object>
struct U3CFromAsyncPatternU3Ec__AnonStorey41_2_t1532408781;
// UniRx.Observable/<FromAsyncPattern>c__AnonStorey41`2<System.Object,UniRx.Unit>
struct U3CFromAsyncPatternU3Ec__AnonStorey41_2_t3253588399;
// UniRx.Observable/<FromAsyncPattern>c__AnonStorey43`3/<FromAsyncPattern>c__AnonStorey44`3<System.Object,System.Object,System.Object>
struct U3CFromAsyncPatternU3Ec__AnonStorey44_3_t1103581271;
// UniRx.Observable/<FromAsyncPattern>c__AnonStorey43`3/<FromAsyncPattern>c__AnonStorey44`3<System.Object,System.Object,UniRx.Unit>
struct U3CFromAsyncPatternU3Ec__AnonStorey44_3_t2824760889;
// UniRx.Observable/<FromAsyncPattern>c__AnonStorey43`3<System.Object,System.Object,System.Object>
struct U3CFromAsyncPatternU3Ec__AnonStorey43_3_t1435962278;
// UniRx.Observable/<FromAsyncPattern>c__AnonStorey43`3<System.Object,System.Object,UniRx.Unit>
struct U3CFromAsyncPatternU3Ec__AnonStorey43_3_t3157141896;
// UniRx.Observable/<FromAsyncPattern>c__AnonStorey46`1<System.Object>
struct U3CFromAsyncPatternU3Ec__AnonStorey46_1_t3285299583;
// UniRx.Observable/<FromAsyncPattern>c__AnonStorey47`2<System.Object,System.Object>
struct U3CFromAsyncPatternU3Ec__AnonStorey47_2_t3822627359;
// UniRx.Observable/<FromCoroutine>c__AnonStorey4C`1<System.Object>
struct U3CFromCoroutineU3Ec__AnonStorey4C_1_t2050741468;
// UniRx.Observable/<FromCoroutineValue>c__AnonStorey4B`1<System.Object>
struct U3CFromCoroutineValueU3Ec__AnonStorey4B_1_t3800936616;
// UniRx.Observable/<OnErrorRetry>c__AnonStorey3D`2/<OnErrorRetry>c__AnonStorey3E`2<System.Object,System.Object>
struct U3COnErrorRetryU3Ec__AnonStorey3E_2_t1069553519;

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array2840145358.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_LazyTask_1_gen4247718180.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_LazyTask_1_gen4247718180MethodDeclarations.h"
#include "mscorlib_System_Void2779279689.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_LazyTask1365889643MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_LazyTask_TaskS2963002871.h"
#include "mscorlib_System_Int322847414787.h"
#include "mscorlib_System_InvalidOperationException2420574324MethodDeclarations.h"
#include "mscorlib_System_String968488902.h"
#include "mscorlib_System_InvalidOperationException2420574324.h"
#include "mscorlib_System_Exception1967233988.h"
#include "UnityEngine_UnityEngine_Coroutine2246592261.h"
#include "mscorlib_System_Action_1_gen2115686693MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_CancellationTo1439151560MethodDeclarations.h"
#include "mscorlib_System_Action_1_gen2995867492.h"
#include "mscorlib_System_Action_1_gen2995867492MethodDeclarations.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_IntPtr676692020.h"
#include "mscorlib_System_Action_1_gen2115686693.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_LazyTask1365889643.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_BooleanDisposa3065601722.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_CancellationTo1439151560.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Observable258071701MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Observable258071701.h"
#include "mscorlib_System_Int322847414787MethodDeclarations.h"
#include "mscorlib_System_String968488902MethodDeclarations.h"
#include "mscorlib_System_Object837106420MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_LazyTask_1_gen2237409813.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_LazyTask_1_gen2237409813MethodDeclarations.h"
#include "mscorlib_System_Action_1_gen985559125.h"
#include "mscorlib_System_Action_1_gen985559125MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Notification_13651252413.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Notification_13651252413MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Notification_11984678244.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Notification_1_g38356375.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Notification_1_g38356375MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_NotificationKi3324123761.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Notification_11984678244MethodDeclarations.h"
#include "System_Core_System_Action437523947MethodDeclarations.h"
#include "System_Core_System_Action437523947.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Notification_13050132088.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Notification_13050132088MethodDeclarations.h"
#include "mscorlib_System_Boolean211005341.h"
#include "mscorlib_System_Type2779229935MethodDeclarations.h"
#include "mscorlib_System_Type2779229935.h"
#include "mscorlib_System_RuntimeTypeHandle1864875887.h"
#include "mscorlib_System_ArgumentNullException3214793280MethodDeclarations.h"
#include "mscorlib_System_ArgumentNullException3214793280.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Notification_1_O36474669.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Notification_1_O36474669MethodDeclarations.h"
#include "mscorlib_System_Globalization_CultureInfo3603717042MethodDeclarations.h"
#include "mscorlib_System_Globalization_CultureInfo3603717042.h"
#include "mscorlib_ArrayTypes.h"
#include "mscorlib_System_Exception1967233988MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Notification_13645104142.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Notification_13645104142MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_EqualityCompare271497070MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_EqualityCompare271497070.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Scheduler103934925.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Scheduler103934925MethodDeclarations.h"
#include "System_Core_System_Func_2_gen2619763055.h"
#include "System_Core_System_Func_2_gen2619763055MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Observable_U3C3332811877.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Observable_U3C3332811877MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_RefCountDispos3288429070MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_CompositeDispo1894629977MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_RefCountDispos3288429070.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_CompositeDispo1894629977.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Observable_U3CC339163467.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Observable_U3CC339163467MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Observable_U3C2571110558.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Observable_U3C2571110558MethodDeclarations.h"
#include "mscorlib_System_Threading_Interlocked3007803305MethodDeclarations.h"
#include "Assembly-CSharp-firstpass_ArrayTypes.h"
#include "mscorlib_System_UInt32985925326.h"
#include "mscorlib_System_NotSupportedException1374155497MethodDeclarations.h"
#include "mscorlib_System_NotSupportedException1374155497.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Observable_U3C4292290176.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Observable_U3C4292290176MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Observable_U3CC215346063.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Observable_U3CC215346063MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Observable_U3C1660482281.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Observable_U3C1660482281MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Observable_U3C1370739976.h"
#include "System_Core_System_Func_2_gen850390275.h"
#include "System_Core_System_Func_2_gen850390275MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_AsyncSubject_11536745524.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_AsyncSubject_11536745524MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Observable_U3C3381661899.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Observable_U3C3381661899MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Unit2558286038.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Observable_U3C3091919594.h"
#include "System_Core_System_Func_2_gen2571569893.h"
#include "System_Core_System_Func_2_gen2571569893MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_AsyncSubject_13257925142.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_AsyncSubject_13257925142MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Observable_U3C1370739976MethodDeclarations.h"
#include "mscorlib_System_AsyncCallback1363551830MethodDeclarations.h"
#include "System_Core_System_Func_3_gen3381739440MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Scheduler_Defa3564383257MethodDeclarations.h"
#include "System_Core_System_Func_3_gen3381739440.h"
#include "mscorlib_System_AsyncCallback1363551830.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Observable_U3C3091919594MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Observable_U3C2629939760.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Observable_U3C2629939760MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Observable_U3C1532408781.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Observable_U3CFr56152082.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Observable_U3CFr56152082MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Observable_U3C3253588399.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Observable_U3C1532408781MethodDeclarations.h"
#include "System_Core_System_Func_4_gen823488097.h"
#include "System_Core_System_Func_4_gen823488097MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Observable_U3C3253588399MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Observable_U3C1103581271.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Observable_U3C1103581271MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Observable_U3C1435962278.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Observable_U3C2824760889.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Observable_U3C2824760889MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Observable_U3C3157141896.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Observable_U3C1435962278MethodDeclarations.h"
#include "System_Core_System_Func_5_gen2364630122.h"
#include "System_Core_System_Func_5_gen2364630122MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Observable_U3C3157141896MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Observable_U3C3285299583.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Observable_U3C3285299583MethodDeclarations.h"
#include "mscorlib_System_Action_1_gen686135974MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Unit2558286038MethodDeclarations.h"
#include "mscorlib_System_Action_1_gen686135974.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Observable_U3C3822627359.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Observable_U3C3822627359MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Observable_U3C2050741468.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Observable_U3C2050741468MethodDeclarations.h"
#include "System_Core_System_Func_2_gen1278048720.h"
#include "System_Core_System_Func_2_gen1278048720MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Observable_U3C3800936616.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Observable_U3C3800936616MethodDeclarations.h"
#include "System_Core_System_Func_1_gen1429988286MethodDeclarations.h"
#include "System_Core_System_Func_1_gen1429988286.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Observable_U3C1069553519.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Observable_U3C1069553519MethodDeclarations.h"
#include "mscorlib_System_TimeSpan763862892MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Observable_U3C4266989836.h"
#include "mscorlib_System_TimeSpan763862892.h"

// UnityEngine.Coroutine UniRx.Observable::StartAsCoroutine<System.Int32>(UniRx.IObservable`1<!!0>,System.Action`1<!!0>,System.Action`1<System.Exception>,UniRx.CancellationToken)
extern "C"  Coroutine_t2246592261 * Observable_StartAsCoroutine_TisInt32_t2847414787_m1566605456_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* p0, Action_1_t2995867492 * p1, Action_1_t2115686693 * p2, CancellationToken_t1439151560  p3, const MethodInfo* method);
#define Observable_StartAsCoroutine_TisInt32_t2847414787_m1566605456(__this /* static, unused */, p0, p1, p2, p3, method) ((  Coroutine_t2246592261 * (*) (Il2CppObject * /* static, unused */, Il2CppObject*, Action_1_t2995867492 *, Action_1_t2115686693 *, CancellationToken_t1439151560 , const MethodInfo*))Observable_StartAsCoroutine_TisInt32_t2847414787_m1566605456_gshared)(__this /* static, unused */, p0, p1, p2, p3, method)
// UnityEngine.Coroutine UniRx.Observable::StartAsCoroutine<System.Object>(UniRx.IObservable`1<!!0>,System.Action`1<!!0>,System.Action`1<System.Exception>,UniRx.CancellationToken)
extern "C"  Coroutine_t2246592261 * Observable_StartAsCoroutine_TisIl2CppObject_m3784688785_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* p0, Action_1_t985559125 * p1, Action_1_t2115686693 * p2, CancellationToken_t1439151560  p3, const MethodInfo* method);
#define Observable_StartAsCoroutine_TisIl2CppObject_m3784688785(__this /* static, unused */, p0, p1, p2, p3, method) ((  Coroutine_t2246592261 * (*) (Il2CppObject * /* static, unused */, Il2CppObject*, Action_1_t985559125 *, Action_1_t2115686693 *, CancellationToken_t1439151560 , const MethodInfo*))Observable_StartAsCoroutine_TisIl2CppObject_m3784688785_gshared)(__this /* static, unused */, p0, p1, p2, p3, method)
// UniRx.IObservable`1<!!0> UniRx.Observable::Create<System.Object>(System.Func`2<UniRx.IObserver`1<!!0>,System.IDisposable>)
extern "C"  Il2CppObject* Observable_Create_TisIl2CppObject_m3787426897_gshared (Il2CppObject * __this /* static, unused */, Func_2_t2619763055 * p0, const MethodInfo* method);
#define Observable_Create_TisIl2CppObject_m3787426897(__this /* static, unused */, p0, method) ((  Il2CppObject* (*) (Il2CppObject * /* static, unused */, Func_2_t2619763055 *, const MethodInfo*))Observable_Create_TisIl2CppObject_m3787426897_gshared)(__this /* static, unused */, p0, method)
// UniRx.IObservable`1<!!0> UniRx.Observable::Empty<System.Object>()
extern "C"  Il2CppObject* Observable_Empty_TisIl2CppObject_m3547328287_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define Observable_Empty_TisIl2CppObject_m3547328287(__this /* static, unused */, method) ((  Il2CppObject* (*) (Il2CppObject * /* static, unused */, const MethodInfo*))Observable_Empty_TisIl2CppObject_m3547328287_gshared)(__this /* static, unused */, method)
// UniRx.IObservable`1<!!0> UniRx.Observable::Throw<System.Object>(System.Exception,UniRx.IScheduler)
extern "C"  Il2CppObject* Observable_Throw_TisIl2CppObject_m1340112834_gshared (Il2CppObject * __this /* static, unused */, Exception_t1967233988 * p0, Il2CppObject * p1, const MethodInfo* method);
#define Observable_Throw_TisIl2CppObject_m1340112834(__this /* static, unused */, p0, p1, method) ((  Il2CppObject* (*) (Il2CppObject * /* static, unused */, Exception_t1967233988 *, Il2CppObject *, const MethodInfo*))Observable_Throw_TisIl2CppObject_m1340112834_gshared)(__this /* static, unused */, p0, p1, method)
// UniRx.IObservable`1<!!0> UniRx.Observable::AsObservable<System.Object>(UniRx.IObservable`1<!!0>)
extern "C"  Il2CppObject* Observable_AsObservable_TisIl2CppObject_m961297524_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* p0, const MethodInfo* method);
#define Observable_AsObservable_TisIl2CppObject_m961297524(__this /* static, unused */, p0, method) ((  Il2CppObject* (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Observable_AsObservable_TisIl2CppObject_m961297524_gshared)(__this /* static, unused */, p0, method)
// UniRx.IObservable`1<!!0> UniRx.Observable::Throw<UniRx.Unit>(System.Exception,UniRx.IScheduler)
extern "C"  Il2CppObject* Observable_Throw_TisUnit_t2558286038_m1369433586_gshared (Il2CppObject * __this /* static, unused */, Exception_t1967233988 * p0, Il2CppObject * p1, const MethodInfo* method);
#define Observable_Throw_TisUnit_t2558286038_m1369433586(__this /* static, unused */, p0, p1, method) ((  Il2CppObject* (*) (Il2CppObject * /* static, unused */, Exception_t1967233988 *, Il2CppObject *, const MethodInfo*))Observable_Throw_TisUnit_t2558286038_m1369433586_gshared)(__this /* static, unused */, p0, p1, method)
// UniRx.IObservable`1<!!0> UniRx.Observable::AsObservable<UniRx.Unit>(UniRx.IObservable`1<!!0>)
extern "C"  Il2CppObject* Observable_AsObservable_TisUnit_t2558286038_m1062057990_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* p0, const MethodInfo* method);
#define Observable_AsObservable_TisUnit_t2558286038_m1062057990(__this /* static, unused */, p0, method) ((  Il2CppObject* (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Observable_AsObservable_TisUnit_t2558286038_m1062057990_gshared)(__this /* static, unused */, p0, method)
// System.Collections.IEnumerator UniRx.Observable::WrapEnumeratorYieldValue<System.Object>(System.Collections.IEnumerator,UniRx.IObserver`1<!!0>,UniRx.CancellationToken,System.Boolean)
extern "C"  Il2CppObject * Observable_WrapEnumeratorYieldValue_TisIl2CppObject_m2703616302_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * p0, Il2CppObject* p1, CancellationToken_t1439151560  p2, bool p3, const MethodInfo* method);
#define Observable_WrapEnumeratorYieldValue_TisIl2CppObject_m2703616302(__this /* static, unused */, p0, p1, p2, p3, method) ((  Il2CppObject * (*) (Il2CppObject * /* static, unused */, Il2CppObject *, Il2CppObject*, CancellationToken_t1439151560 , bool, const MethodInfo*))Observable_WrapEnumeratorYieldValue_TisIl2CppObject_m2703616302_gshared)(__this /* static, unused */, p0, p1, p2, p3, method)
// UniRx.IObservable`1<!!0> UniRx.Observable::SubscribeOn<System.Object>(UniRx.IObservable`1<!!0>,UniRx.IScheduler)
extern "C"  Il2CppObject* Observable_SubscribeOn_TisIl2CppObject_m4016496918_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* p0, Il2CppObject * p1, const MethodInfo* method);
#define Observable_SubscribeOn_TisIl2CppObject_m4016496918(__this /* static, unused */, p0, p1, method) ((  Il2CppObject* (*) (Il2CppObject * /* static, unused */, Il2CppObject*, Il2CppObject *, const MethodInfo*))Observable_SubscribeOn_TisIl2CppObject_m4016496918_gshared)(__this /* static, unused */, p0, p1, method)
// UniRx.IObservable`1<!!0> UniRx.Observable::DelaySubscription<System.Object>(UniRx.IObservable`1<!!0>,System.TimeSpan,UniRx.IScheduler)
extern "C"  Il2CppObject* Observable_DelaySubscription_TisIl2CppObject_m846123063_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* p0, TimeSpan_t763862892  p1, Il2CppObject * p2, const MethodInfo* method);
#define Observable_DelaySubscription_TisIl2CppObject_m846123063(__this /* static, unused */, p0, p1, p2, method) ((  Il2CppObject* (*) (Il2CppObject * /* static, unused */, Il2CppObject*, TimeSpan_t763862892 , Il2CppObject *, const MethodInfo*))Observable_DelaySubscription_TisIl2CppObject_m846123063_gshared)(__this /* static, unused */, p0, p1, p2, method)
// UniRx.IObservable`1<!!0> UniRx.Observable::Throw<System.Object>(System.Exception)
extern "C"  Il2CppObject* Observable_Throw_TisIl2CppObject_m3997803290_gshared (Il2CppObject * __this /* static, unused */, Exception_t1967233988 * p0, const MethodInfo* method);
#define Observable_Throw_TisIl2CppObject_m3997803290(__this /* static, unused */, p0, method) ((  Il2CppObject* (*) (Il2CppObject * /* static, unused */, Exception_t1967233988 *, const MethodInfo*))Observable_Throw_TisIl2CppObject_m3997803290_gshared)(__this /* static, unused */, p0, method)
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void UniRx.LazyTask`1<System.Int32>::.ctor(UniRx.IObservable`1<T>)
extern "C"  void LazyTask_1__ctor_m152437241_gshared (LazyTask_1_t4247718180 * __this, Il2CppObject* ___source0, const MethodInfo* method)
{
	{
		NullCheck((LazyTask_t1365889643 *)__this);
		LazyTask__ctor_m1095466110((LazyTask_t1365889643 *)__this, /*hidden argument*/NULL);
		Il2CppObject* L_0 = ___source0;
		__this->set_source_3(L_0);
		NullCheck((LazyTask_t1365889643 *)__this);
		LazyTask_set_Status_m280253608((LazyTask_t1365889643 *)__this, (int32_t)0, /*hidden argument*/NULL);
		return;
	}
}
// T UniRx.LazyTask`1<System.Int32>::get_Result()
extern Il2CppClass* InvalidOperationException_t2420574324_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral635665891;
extern const uint32_t LazyTask_1_get_Result_m2773840719_MetadataUsageId;
extern "C"  int32_t LazyTask_1_get_Result_m2773840719_gshared (LazyTask_1_t4247718180 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (LazyTask_1_get_Result_m2773840719_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NullCheck((LazyTask_t1365889643 *)__this);
		int32_t L_0 = LazyTask_get_Status_m3296471199((LazyTask_t1365889643 *)__this, /*hidden argument*/NULL);
		if ((((int32_t)L_0) == ((int32_t)2)))
		{
			goto IL_0017;
		}
	}
	{
		InvalidOperationException_t2420574324 * L_1 = (InvalidOperationException_t2420574324 *)il2cpp_codegen_object_new(InvalidOperationException_t2420574324_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_1, (String_t*)_stringLiteral635665891, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0017:
	{
		int32_t L_2 = (int32_t)__this->get_result_4();
		return L_2;
	}
}
// System.Exception UniRx.LazyTask`1<System.Int32>::get_Exception()
extern "C"  Exception_t1967233988 * LazyTask_1_get_Exception_m2829354473_gshared (LazyTask_1_t4247718180 * __this, const MethodInfo* method)
{
	{
		Exception_t1967233988 * L_0 = (Exception_t1967233988 *)__this->get_U3CExceptionU3Ek__BackingField_5();
		return L_0;
	}
}
// System.Void UniRx.LazyTask`1<System.Int32>::set_Exception(System.Exception)
extern "C"  void LazyTask_1_set_Exception_m4039632538_gshared (LazyTask_1_t4247718180 * __this, Exception_t1967233988 * ___value0, const MethodInfo* method)
{
	{
		Exception_t1967233988 * L_0 = ___value0;
		__this->set_U3CExceptionU3Ek__BackingField_5(L_0);
		return;
	}
}
// UnityEngine.Coroutine UniRx.LazyTask`1<System.Int32>::Start()
extern Il2CppClass* InvalidOperationException_t2420574324_il2cpp_TypeInfo_var;
extern Il2CppClass* Action_1_t2115686693_il2cpp_TypeInfo_var;
extern Il2CppClass* Observable_t258071701_il2cpp_TypeInfo_var;
extern const MethodInfo* Action_1__ctor_m15890083_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral827469886;
extern const uint32_t LazyTask_1_Start_m990669032_MetadataUsageId;
extern "C"  Coroutine_t2246592261 * LazyTask_1_Start_m990669032_gshared (LazyTask_1_t4247718180 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (LazyTask_1_Start_m990669032_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Coroutine_t2246592261 * V_0 = NULL;
	{
		NullCheck((LazyTask_t1365889643 *)__this);
		int32_t L_0 = LazyTask_get_Status_m3296471199((LazyTask_t1365889643 *)__this, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0016;
		}
	}
	{
		InvalidOperationException_t2420574324 * L_1 = (InvalidOperationException_t2420574324 *)il2cpp_codegen_object_new(InvalidOperationException_t2420574324_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_1, (String_t*)_stringLiteral827469886, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0016:
	{
		NullCheck((LazyTask_t1365889643 *)__this);
		LazyTask_set_Status_m280253608((LazyTask_t1365889643 *)__this, (int32_t)1, /*hidden argument*/NULL);
		Il2CppObject* L_2 = (Il2CppObject*)__this->get_source_3();
		IntPtr_t L_3;
		L_3.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		Action_1_t2995867492 * L_4 = (Action_1_t2995867492 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		((  void (*) (Action_1_t2995867492 *, Il2CppObject *, IntPtr_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)(L_4, (Il2CppObject *)__this, (IntPtr_t)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		IntPtr_t L_5;
		L_5.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		Action_1_t2115686693 * L_6 = (Action_1_t2115686693 *)il2cpp_codegen_object_new(Action_1_t2115686693_il2cpp_TypeInfo_var);
		Action_1__ctor_m15890083(L_6, (Il2CppObject *)__this, (IntPtr_t)L_5, /*hidden argument*/Action_1__ctor_m15890083_MethodInfo_var);
		BooleanDisposable_t3065601722 * L_7 = (BooleanDisposable_t3065601722 *)((LazyTask_t1365889643 *)__this)->get_cancellation_0();
		CancellationToken_t1439151560  L_8;
		memset(&L_8, 0, sizeof(L_8));
		CancellationToken__ctor_m1453920148(&L_8, (Il2CppObject *)L_7, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Observable_t258071701_il2cpp_TypeInfo_var);
		Coroutine_t2246592261 * L_9 = ((  Coroutine_t2246592261 * (*) (Il2CppObject * /* static, unused */, Il2CppObject*, Action_1_t2995867492 *, Action_1_t2115686693 *, CancellationToken_t1439151560 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)(NULL /*static, unused*/, (Il2CppObject*)L_2, (Action_1_t2995867492 *)L_4, (Action_1_t2115686693 *)L_6, (CancellationToken_t1439151560 )L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		V_0 = (Coroutine_t2246592261 *)L_9;
		Coroutine_t2246592261 * L_10 = V_0;
		return L_10;
	}
}
// System.String UniRx.LazyTask`1<System.Int32>::ToString()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2356051531;
extern Il2CppCodeGenString* _stringLiteral959603255;
extern Il2CppCodeGenString* _stringLiteral1948199494;
extern Il2CppCodeGenString* _stringLiteral3025061665;
extern Il2CppCodeGenString* _stringLiteral2826629392;
extern const uint32_t LazyTask_1_ToString_m1613548729_MetadataUsageId;
extern "C"  String_t* LazyTask_1_ToString_m1613548729_gshared (LazyTask_1_t4247718180 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (LazyTask_1_ToString_m1613548729_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	{
		NullCheck((LazyTask_t1365889643 *)__this);
		int32_t L_0 = LazyTask_get_Status_m3296471199((LazyTask_t1365889643 *)__this, /*hidden argument*/NULL);
		V_0 = (int32_t)L_0;
		int32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0026;
		}
		if (L_1 == 1)
		{
			goto IL_002c;
		}
		if (L_1 == 2)
		{
			goto IL_0032;
		}
		if (L_1 == 3)
		{
			goto IL_0051;
		}
		if (L_1 == 4)
		{
			goto IL_0057;
		}
	}
	{
		goto IL_0076;
	}

IL_0026:
	{
		return _stringLiteral2356051531;
	}

IL_002c:
	{
		return _stringLiteral959603255;
	}

IL_0032:
	{
		NullCheck((LazyTask_1_t4247718180 *)__this);
		int32_t L_2 = ((  int32_t (*) (LazyTask_1_t4247718180 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->method)((LazyTask_1_t4247718180 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		V_1 = (int32_t)L_2;
		String_t* L_3 = Int32_ToString_m1286526384((int32_t*)(&V_1), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_4 = String_Concat_m138640077(NULL /*static, unused*/, (String_t*)_stringLiteral1948199494, (String_t*)L_3, /*hidden argument*/NULL);
		return L_4;
	}

IL_0051:
	{
		return _stringLiteral3025061665;
	}

IL_0057:
	{
		NullCheck((LazyTask_1_t4247718180 *)__this);
		int32_t L_5 = ((  int32_t (*) (LazyTask_1_t4247718180 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->method)((LazyTask_1_t4247718180 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		V_2 = (int32_t)L_5;
		String_t* L_6 = Int32_ToString_m1286526384((int32_t*)(&V_2), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_7 = String_Concat_m138640077(NULL /*static, unused*/, (String_t*)_stringLiteral2826629392, (String_t*)L_6, /*hidden argument*/NULL);
		return L_7;
	}

IL_0076:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_8 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		return L_8;
	}
}
// UniRx.LazyTask`1<T> UniRx.LazyTask`1<System.Int32>::FromResult(T)
extern "C"  LazyTask_1_t4247718180 * LazyTask_1_FromResult_m1512477278_gshared (Il2CppObject * __this /* static, unused */, int32_t ___value0, const MethodInfo* method)
{
	LazyTask_1_t4247718180 * V_0 = NULL;
	{
		LazyTask_1_t4247718180 * L_0 = (LazyTask_1_t4247718180 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7));
		((  void (*) (LazyTask_1_t4247718180 *, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8)->method)(L_0, (Il2CppObject*)NULL, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8));
		V_0 = (LazyTask_1_t4247718180 *)L_0;
		LazyTask_1_t4247718180 * L_1 = V_0;
		int32_t L_2 = ___value0;
		NullCheck(L_1);
		L_1->set_result_4(L_2);
		LazyTask_1_t4247718180 * L_3 = V_0;
		NullCheck((LazyTask_t1365889643 *)L_3);
		LazyTask_set_Status_m280253608((LazyTask_t1365889643 *)L_3, (int32_t)2, /*hidden argument*/NULL);
		LazyTask_1_t4247718180 * L_4 = V_0;
		return L_4;
	}
}
// System.Void UniRx.LazyTask`1<System.Int32>::<Start>m__A6(T)
extern "C"  void LazyTask_1_U3CStartU3Em__A6_m3356978090_gshared (LazyTask_1_t4247718180 * __this, int32_t ___x0, const MethodInfo* method)
{
	{
		int32_t L_0 = ___x0;
		__this->set_result_4(L_0);
		NullCheck((LazyTask_t1365889643 *)__this);
		LazyTask_set_Status_m280253608((LazyTask_t1365889643 *)__this, (int32_t)2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UniRx.LazyTask`1<System.Int32>::<Start>m__A7(System.Exception)
extern "C"  void LazyTask_1_U3CStartU3Em__A7_m3828460197_gshared (LazyTask_1_t4247718180 * __this, Exception_t1967233988 * ___ex0, const MethodInfo* method)
{
	{
		Exception_t1967233988 * L_0 = ___ex0;
		NullCheck((LazyTask_1_t4247718180 *)__this);
		((  void (*) (LazyTask_1_t4247718180 *, Exception_t1967233988 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9)->method)((LazyTask_1_t4247718180 *)__this, (Exception_t1967233988 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9));
		NullCheck((LazyTask_t1365889643 *)__this);
		LazyTask_set_Status_m280253608((LazyTask_t1365889643 *)__this, (int32_t)4, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UniRx.LazyTask`1<System.Object>::.ctor(UniRx.IObservable`1<T>)
extern "C"  void LazyTask_1__ctor_m3859110140_gshared (LazyTask_1_t2237409813 * __this, Il2CppObject* ___source0, const MethodInfo* method)
{
	{
		NullCheck((LazyTask_t1365889643 *)__this);
		LazyTask__ctor_m1095466110((LazyTask_t1365889643 *)__this, /*hidden argument*/NULL);
		Il2CppObject* L_0 = ___source0;
		__this->set_source_3(L_0);
		NullCheck((LazyTask_t1365889643 *)__this);
		LazyTask_set_Status_m280253608((LazyTask_t1365889643 *)__this, (int32_t)0, /*hidden argument*/NULL);
		return;
	}
}
// T UniRx.LazyTask`1<System.Object>::get_Result()
extern Il2CppClass* InvalidOperationException_t2420574324_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral635665891;
extern const uint32_t LazyTask_1_get_Result_m2602473934_MetadataUsageId;
extern "C"  Il2CppObject * LazyTask_1_get_Result_m2602473934_gshared (LazyTask_1_t2237409813 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (LazyTask_1_get_Result_m2602473934_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NullCheck((LazyTask_t1365889643 *)__this);
		int32_t L_0 = LazyTask_get_Status_m3296471199((LazyTask_t1365889643 *)__this, /*hidden argument*/NULL);
		if ((((int32_t)L_0) == ((int32_t)2)))
		{
			goto IL_0017;
		}
	}
	{
		InvalidOperationException_t2420574324 * L_1 = (InvalidOperationException_t2420574324 *)il2cpp_codegen_object_new(InvalidOperationException_t2420574324_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_1, (String_t*)_stringLiteral635665891, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0017:
	{
		Il2CppObject * L_2 = (Il2CppObject *)__this->get_result_4();
		return L_2;
	}
}
// System.Exception UniRx.LazyTask`1<System.Object>::get_Exception()
extern "C"  Exception_t1967233988 * LazyTask_1_get_Exception_m41140598_gshared (LazyTask_1_t2237409813 * __this, const MethodInfo* method)
{
	{
		Exception_t1967233988 * L_0 = (Exception_t1967233988 *)__this->get_U3CExceptionU3Ek__BackingField_5();
		return L_0;
	}
}
// System.Void UniRx.LazyTask`1<System.Object>::set_Exception(System.Exception)
extern "C"  void LazyTask_1_set_Exception_m1329432797_gshared (LazyTask_1_t2237409813 * __this, Exception_t1967233988 * ___value0, const MethodInfo* method)
{
	{
		Exception_t1967233988 * L_0 = ___value0;
		__this->set_U3CExceptionU3Ek__BackingField_5(L_0);
		return;
	}
}
// UnityEngine.Coroutine UniRx.LazyTask`1<System.Object>::Start()
extern Il2CppClass* InvalidOperationException_t2420574324_il2cpp_TypeInfo_var;
extern Il2CppClass* Action_1_t2115686693_il2cpp_TypeInfo_var;
extern Il2CppClass* Observable_t258071701_il2cpp_TypeInfo_var;
extern const MethodInfo* Action_1__ctor_m15890083_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral827469886;
extern const uint32_t LazyTask_1_Start_m3620087119_MetadataUsageId;
extern "C"  Coroutine_t2246592261 * LazyTask_1_Start_m3620087119_gshared (LazyTask_1_t2237409813 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (LazyTask_1_Start_m3620087119_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Coroutine_t2246592261 * V_0 = NULL;
	{
		NullCheck((LazyTask_t1365889643 *)__this);
		int32_t L_0 = LazyTask_get_Status_m3296471199((LazyTask_t1365889643 *)__this, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0016;
		}
	}
	{
		InvalidOperationException_t2420574324 * L_1 = (InvalidOperationException_t2420574324 *)il2cpp_codegen_object_new(InvalidOperationException_t2420574324_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_1, (String_t*)_stringLiteral827469886, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0016:
	{
		NullCheck((LazyTask_t1365889643 *)__this);
		LazyTask_set_Status_m280253608((LazyTask_t1365889643 *)__this, (int32_t)1, /*hidden argument*/NULL);
		Il2CppObject* L_2 = (Il2CppObject*)__this->get_source_3();
		IntPtr_t L_3;
		L_3.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		Action_1_t985559125 * L_4 = (Action_1_t985559125 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		((  void (*) (Action_1_t985559125 *, Il2CppObject *, IntPtr_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)(L_4, (Il2CppObject *)__this, (IntPtr_t)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		IntPtr_t L_5;
		L_5.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		Action_1_t2115686693 * L_6 = (Action_1_t2115686693 *)il2cpp_codegen_object_new(Action_1_t2115686693_il2cpp_TypeInfo_var);
		Action_1__ctor_m15890083(L_6, (Il2CppObject *)__this, (IntPtr_t)L_5, /*hidden argument*/Action_1__ctor_m15890083_MethodInfo_var);
		BooleanDisposable_t3065601722 * L_7 = (BooleanDisposable_t3065601722 *)((LazyTask_t1365889643 *)__this)->get_cancellation_0();
		CancellationToken_t1439151560  L_8;
		memset(&L_8, 0, sizeof(L_8));
		CancellationToken__ctor_m1453920148(&L_8, (Il2CppObject *)L_7, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Observable_t258071701_il2cpp_TypeInfo_var);
		Coroutine_t2246592261 * L_9 = ((  Coroutine_t2246592261 * (*) (Il2CppObject * /* static, unused */, Il2CppObject*, Action_1_t985559125 *, Action_1_t2115686693 *, CancellationToken_t1439151560 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)(NULL /*static, unused*/, (Il2CppObject*)L_2, (Action_1_t985559125 *)L_4, (Action_1_t2115686693 *)L_6, (CancellationToken_t1439151560 )L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		V_0 = (Coroutine_t2246592261 *)L_9;
		Coroutine_t2246592261 * L_10 = V_0;
		return L_10;
	}
}
// System.String UniRx.LazyTask`1<System.Object>::ToString()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2356051531;
extern Il2CppCodeGenString* _stringLiteral959603255;
extern Il2CppCodeGenString* _stringLiteral1948199494;
extern Il2CppCodeGenString* _stringLiteral3025061665;
extern Il2CppCodeGenString* _stringLiteral2826629392;
extern const uint32_t LazyTask_1_ToString_m598023216_MetadataUsageId;
extern "C"  String_t* LazyTask_1_ToString_m598023216_gshared (LazyTask_1_t2237409813 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (LazyTask_1_ToString_m598023216_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	Il2CppObject * V_1 = NULL;
	Il2CppObject * V_2 = NULL;
	{
		NullCheck((LazyTask_t1365889643 *)__this);
		int32_t L_0 = LazyTask_get_Status_m3296471199((LazyTask_t1365889643 *)__this, /*hidden argument*/NULL);
		V_0 = (int32_t)L_0;
		int32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0026;
		}
		if (L_1 == 1)
		{
			goto IL_002c;
		}
		if (L_1 == 2)
		{
			goto IL_0032;
		}
		if (L_1 == 3)
		{
			goto IL_0051;
		}
		if (L_1 == 4)
		{
			goto IL_0057;
		}
	}
	{
		goto IL_0076;
	}

IL_0026:
	{
		return _stringLiteral2356051531;
	}

IL_002c:
	{
		return _stringLiteral959603255;
	}

IL_0032:
	{
		NullCheck((LazyTask_1_t2237409813 *)__this);
		Il2CppObject * L_2 = ((  Il2CppObject * (*) (LazyTask_1_t2237409813 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->method)((LazyTask_1_t2237409813 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		V_1 = (Il2CppObject *)L_2;
		NullCheck((Il2CppObject *)(*(&V_1)));
		String_t* L_3 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, (Il2CppObject *)(*(&V_1)));
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_4 = String_Concat_m138640077(NULL /*static, unused*/, (String_t*)_stringLiteral1948199494, (String_t*)L_3, /*hidden argument*/NULL);
		return L_4;
	}

IL_0051:
	{
		return _stringLiteral3025061665;
	}

IL_0057:
	{
		NullCheck((LazyTask_1_t2237409813 *)__this);
		Il2CppObject * L_5 = ((  Il2CppObject * (*) (LazyTask_1_t2237409813 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->method)((LazyTask_1_t2237409813 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		V_2 = (Il2CppObject *)L_5;
		NullCheck((Il2CppObject *)(*(&V_2)));
		String_t* L_6 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, (Il2CppObject *)(*(&V_2)));
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_7 = String_Concat_m138640077(NULL /*static, unused*/, (String_t*)_stringLiteral2826629392, (String_t*)L_6, /*hidden argument*/NULL);
		return L_7;
	}

IL_0076:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_8 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		return L_8;
	}
}
// UniRx.LazyTask`1<T> UniRx.LazyTask`1<System.Object>::FromResult(T)
extern "C"  LazyTask_1_t2237409813 * LazyTask_1_FromResult_m690895647_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * ___value0, const MethodInfo* method)
{
	LazyTask_1_t2237409813 * V_0 = NULL;
	{
		LazyTask_1_t2237409813 * L_0 = (LazyTask_1_t2237409813 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7));
		((  void (*) (LazyTask_1_t2237409813 *, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8)->method)(L_0, (Il2CppObject*)NULL, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8));
		V_0 = (LazyTask_1_t2237409813 *)L_0;
		LazyTask_1_t2237409813 * L_1 = V_0;
		Il2CppObject * L_2 = ___value0;
		NullCheck(L_1);
		L_1->set_result_4(L_2);
		LazyTask_1_t2237409813 * L_3 = V_0;
		NullCheck((LazyTask_t1365889643 *)L_3);
		LazyTask_set_Status_m280253608((LazyTask_t1365889643 *)L_3, (int32_t)2, /*hidden argument*/NULL);
		LazyTask_1_t2237409813 * L_4 = V_0;
		return L_4;
	}
}
// System.Void UniRx.LazyTask`1<System.Object>::<Start>m__A6(T)
extern "C"  void LazyTask_1_U3CStartU3Em__A6_m3962041325_gshared (LazyTask_1_t2237409813 * __this, Il2CppObject * ___x0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___x0;
		__this->set_result_4(L_0);
		NullCheck((LazyTask_t1365889643 *)__this);
		LazyTask_set_Status_m280253608((LazyTask_t1365889643 *)__this, (int32_t)2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UniRx.LazyTask`1<System.Object>::<Start>m__A7(System.Exception)
extern "C"  void LazyTask_1_U3CStartU3Em__A7_m2771203074_gshared (LazyTask_1_t2237409813 * __this, Exception_t1967233988 * ___ex0, const MethodInfo* method)
{
	{
		Exception_t1967233988 * L_0 = ___ex0;
		NullCheck((LazyTask_1_t2237409813 *)__this);
		((  void (*) (LazyTask_1_t2237409813 *, Exception_t1967233988 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9)->method)((LazyTask_1_t2237409813 *)__this, (Exception_t1967233988 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9));
		NullCheck((LazyTask_t1365889643 *)__this);
		LazyTask_set_Status_m280253608((LazyTask_t1365889643 *)__this, (int32_t)4, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UniRx.Notification`1/<ToObservable>c__AnonStorey55/<ToObservable>c__AnonStorey56<System.Object>::.ctor()
extern "C"  void U3CToObservableU3Ec__AnonStorey56__ctor_m2315758680_gshared (U3CToObservableU3Ec__AnonStorey56_t3651252413 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UniRx.Notification`1/<ToObservable>c__AnonStorey55/<ToObservable>c__AnonStorey56<System.Object>::<>m__6A()
extern "C"  void U3CToObservableU3Ec__AnonStorey56_U3CU3Em__6A_m2959237420_gshared (U3CToObservableU3Ec__AnonStorey56_t3651252413 * __this, const MethodInfo* method)
{
	{
		U3CToObservableU3Ec__AnonStorey55_t1984678244 * L_0 = (U3CToObservableU3Ec__AnonStorey55_t1984678244 *)__this->get_U3CU3Ef__refU2485_1();
		NullCheck(L_0);
		Notification_1_t38356375 * L_1 = (Notification_1_t38356375 *)L_0->get_U3CU3Ef__this_1();
		Il2CppObject* L_2 = (Il2CppObject*)__this->get_observer_0();
		NullCheck((Notification_1_t38356375 *)L_1);
		VirtActionInvoker1< Il2CppObject* >::Invoke(10 /* System.Void UniRx.Notification`1<System.Object>::Accept(UniRx.IObserver`1<T>) */, (Notification_1_t38356375 *)L_1, (Il2CppObject*)L_2);
		U3CToObservableU3Ec__AnonStorey55_t1984678244 * L_3 = (U3CToObservableU3Ec__AnonStorey55_t1984678244 *)__this->get_U3CU3Ef__refU2485_1();
		NullCheck(L_3);
		Notification_1_t38356375 * L_4 = (Notification_1_t38356375 *)L_3->get_U3CU3Ef__this_1();
		NullCheck((Notification_1_t38356375 *)L_4);
		int32_t L_5 = VirtFuncInvoker0< int32_t >::Invoke(8 /* UniRx.NotificationKind UniRx.Notification`1<System.Object>::get_Kind() */, (Notification_1_t38356375 *)L_4);
		if (L_5)
		{
			goto IL_0036;
		}
	}
	{
		Il2CppObject* L_6 = (Il2CppObject*)__this->get_observer_0();
		NullCheck((Il2CppObject*)L_6);
		InterfaceActionInvoker0::Invoke(0 /* System.Void UniRx.IObserver`1<System.Object>::OnCompleted() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), (Il2CppObject*)L_6);
	}

IL_0036:
	{
		return;
	}
}
// System.Void UniRx.Notification`1/<ToObservable>c__AnonStorey55<System.Object>::.ctor()
extern "C"  void U3CToObservableU3Ec__AnonStorey55__ctor_m1172590403_gshared (U3CToObservableU3Ec__AnonStorey55_t1984678244 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.IDisposable UniRx.Notification`1/<ToObservable>c__AnonStorey55<System.Object>::<>m__69(UniRx.IObserver`1<T>)
extern Il2CppClass* Action_t437523947_il2cpp_TypeInfo_var;
extern Il2CppClass* IScheduler_t2938318244_il2cpp_TypeInfo_var;
extern const uint32_t U3CToObservableU3Ec__AnonStorey55_U3CU3Em__69_m3771438140_MetadataUsageId;
extern "C"  Il2CppObject * U3CToObservableU3Ec__AnonStorey55_U3CU3Em__69_m3771438140_gshared (U3CToObservableU3Ec__AnonStorey55_t1984678244 * __this, Il2CppObject* ___observer0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CToObservableU3Ec__AnonStorey55_U3CU3Em__69_m3771438140_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	U3CToObservableU3Ec__AnonStorey56_t3651252413 * V_0 = NULL;
	{
		U3CToObservableU3Ec__AnonStorey56_t3651252413 * L_0 = (U3CToObservableU3Ec__AnonStorey56_t3651252413 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		((  void (*) (U3CToObservableU3Ec__AnonStorey56_t3651252413 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		V_0 = (U3CToObservableU3Ec__AnonStorey56_t3651252413 *)L_0;
		U3CToObservableU3Ec__AnonStorey56_t3651252413 * L_1 = V_0;
		NullCheck(L_1);
		L_1->set_U3CU3Ef__refU2485_1(__this);
		U3CToObservableU3Ec__AnonStorey56_t3651252413 * L_2 = V_0;
		Il2CppObject* L_3 = ___observer0;
		NullCheck(L_2);
		L_2->set_observer_0(L_3);
		Il2CppObject * L_4 = (Il2CppObject *)__this->get_scheduler_0();
		U3CToObservableU3Ec__AnonStorey56_t3651252413 * L_5 = V_0;
		IntPtr_t L_6;
		L_6.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		Action_t437523947 * L_7 = (Action_t437523947 *)il2cpp_codegen_object_new(Action_t437523947_il2cpp_TypeInfo_var);
		Action__ctor_m2957240604(L_7, (Il2CppObject *)L_5, (IntPtr_t)L_6, /*hidden argument*/NULL);
		NullCheck((Il2CppObject *)L_4);
		Il2CppObject * L_8 = InterfaceFuncInvoker1< Il2CppObject *, Action_t437523947 * >::Invoke(1 /* System.IDisposable UniRx.IScheduler::Schedule(System.Action) */, IScheduler_t2938318244_il2cpp_TypeInfo_var, (Il2CppObject *)L_4, (Action_t437523947 *)L_7);
		return L_8;
	}
}
// System.Void UniRx.Notification`1/OnCompletedNotification<System.Object>::.ctor()
extern "C"  void OnCompletedNotification__ctor_m846116151_gshared (OnCompletedNotification_t3050132088 * __this, const MethodInfo* method)
{
	{
		NullCheck((Notification_1_t38356375 *)__this);
		((  void (*) (Notification_1_t38356375 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((Notification_1_t38356375 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		return;
	}
}
// T UniRx.Notification`1/OnCompletedNotification<System.Object>::get_Value()
extern Il2CppClass* InvalidOperationException_t2420574324_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3522433618;
extern const uint32_t OnCompletedNotification_get_Value_m4172696860_MetadataUsageId;
extern "C"  Il2CppObject * OnCompletedNotification_get_Value_m4172696860_gshared (OnCompletedNotification_t3050132088 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (OnCompletedNotification_get_Value_m4172696860_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		InvalidOperationException_t2420574324 * L_0 = (InvalidOperationException_t2420574324 *)il2cpp_codegen_object_new(InvalidOperationException_t2420574324_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_0, (String_t*)_stringLiteral3522433618, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Exception UniRx.Notification`1/OnCompletedNotification<System.Object>::get_Exception()
extern "C"  Exception_t1967233988 * OnCompletedNotification_get_Exception_m195933264_gshared (OnCompletedNotification_t3050132088 * __this, const MethodInfo* method)
{
	{
		return (Exception_t1967233988 *)NULL;
	}
}
// System.Boolean UniRx.Notification`1/OnCompletedNotification<System.Object>::get_HasValue()
extern "C"  bool OnCompletedNotification_get_HasValue_m1183486929_gshared (OnCompletedNotification_t3050132088 * __this, const MethodInfo* method)
{
	{
		return (bool)0;
	}
}
// UniRx.NotificationKind UniRx.Notification`1/OnCompletedNotification<System.Object>::get_Kind()
extern "C"  int32_t OnCompletedNotification_get_Kind_m3114003516_gshared (OnCompletedNotification_t3050132088 * __this, const MethodInfo* method)
{
	{
		return (int32_t)(2);
	}
}
// System.Int32 UniRx.Notification`1/OnCompletedNotification<System.Object>::GetHashCode()
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern const uint32_t OnCompletedNotification_GetHashCode_m2285451036_MetadataUsageId;
extern "C"  int32_t OnCompletedNotification_GetHashCode_m2285451036_gshared (OnCompletedNotification_t3050132088 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (OnCompletedNotification_GetHashCode_m2285451036_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t1864875887 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)), /*hidden argument*/NULL);
		NullCheck((Type_t *)L_0);
		int32_t L_1 = VirtFuncInvoker0< int32_t >::Invoke(2 /* System.Int32 System.Type::GetHashCode() */, (Type_t *)L_0);
		return ((int32_t)((int32_t)L_1^(int32_t)((int32_t)8510)));
	}
}
// System.Boolean UniRx.Notification`1/OnCompletedNotification<System.Object>::Equals(UniRx.Notification`1<T>)
extern "C"  bool OnCompletedNotification_Equals_m2016138576_gshared (OnCompletedNotification_t3050132088 * __this, Notification_1_t38356375 * ___other0, const MethodInfo* method)
{
	{
		Notification_1_t38356375 * L_0 = ___other0;
		bool L_1 = Object_ReferenceEquals_m3695130242(NULL /*static, unused*/, (Il2CppObject *)__this, (Il2CppObject *)L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_000e;
		}
	}
	{
		return (bool)1;
	}

IL_000e:
	{
		Notification_1_t38356375 * L_2 = ___other0;
		bool L_3 = Object_ReferenceEquals_m3695130242(NULL /*static, unused*/, (Il2CppObject *)L_2, (Il2CppObject *)NULL, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_001c;
		}
	}
	{
		return (bool)0;
	}

IL_001c:
	{
		Notification_1_t38356375 * L_4 = ___other0;
		NullCheck((Notification_1_t38356375 *)L_4);
		int32_t L_5 = VirtFuncInvoker0< int32_t >::Invoke(8 /* UniRx.NotificationKind UniRx.Notification`1<System.Object>::get_Kind() */, (Notification_1_t38356375 *)L_4);
		return (bool)((((int32_t)L_5) == ((int32_t)2))? 1 : 0);
	}
}
// System.String UniRx.Notification`1/OnCompletedNotification<System.Object>::ToString()
extern Il2CppCodeGenString* _stringLiteral1680493837;
extern const uint32_t OnCompletedNotification_ToString_m2489518614_MetadataUsageId;
extern "C"  String_t* OnCompletedNotification_ToString_m2489518614_gshared (OnCompletedNotification_t3050132088 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (OnCompletedNotification_ToString_m2489518614_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		return _stringLiteral1680493837;
	}
}
// System.Void UniRx.Notification`1/OnCompletedNotification<System.Object>::Accept(UniRx.IObserver`1<T>)
extern Il2CppClass* ArgumentNullException_t3214793280_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral348607190;
extern const uint32_t OnCompletedNotification_Accept_m1436023783_MetadataUsageId;
extern "C"  void OnCompletedNotification_Accept_m1436023783_gshared (OnCompletedNotification_t3050132088 * __this, Il2CppObject* ___observer0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (OnCompletedNotification_Accept_m1436023783_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject* L_0 = ___observer0;
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		ArgumentNullException_t3214793280 * L_1 = (ArgumentNullException_t3214793280 *)il2cpp_codegen_object_new(ArgumentNullException_t3214793280_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m135444188(L_1, (String_t*)_stringLiteral348607190, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0011:
	{
		Il2CppObject* L_2 = ___observer0;
		NullCheck((Il2CppObject*)L_2);
		InterfaceActionInvoker0::Invoke(0 /* System.Void UniRx.IObserver`1<System.Object>::OnCompleted() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4), (Il2CppObject*)L_2);
		return;
	}
}
// System.Void UniRx.Notification`1/OnCompletedNotification<System.Object>::Accept(System.Action`1<T>,System.Action`1<System.Exception>,System.Action)
extern Il2CppClass* ArgumentNullException_t3214793280_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3281847666;
extern Il2CppCodeGenString* _stringLiteral2945099625;
extern Il2CppCodeGenString* _stringLiteral2861249324;
extern const uint32_t OnCompletedNotification_Accept_m3744114624_MetadataUsageId;
extern "C"  void OnCompletedNotification_Accept_m3744114624_gshared (OnCompletedNotification_t3050132088 * __this, Action_1_t985559125 * ___onNext0, Action_1_t2115686693 * ___onError1, Action_t437523947 * ___onCompleted2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (OnCompletedNotification_Accept_m3744114624_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Action_1_t985559125 * L_0 = ___onNext0;
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		ArgumentNullException_t3214793280 * L_1 = (ArgumentNullException_t3214793280 *)il2cpp_codegen_object_new(ArgumentNullException_t3214793280_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m135444188(L_1, (String_t*)_stringLiteral3281847666, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0011:
	{
		Action_1_t2115686693 * L_2 = ___onError1;
		if (L_2)
		{
			goto IL_0022;
		}
	}
	{
		ArgumentNullException_t3214793280 * L_3 = (ArgumentNullException_t3214793280 *)il2cpp_codegen_object_new(ArgumentNullException_t3214793280_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m135444188(L_3, (String_t*)_stringLiteral2945099625, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_0022:
	{
		Action_t437523947 * L_4 = ___onCompleted2;
		if (L_4)
		{
			goto IL_0033;
		}
	}
	{
		ArgumentNullException_t3214793280 * L_5 = (ArgumentNullException_t3214793280 *)il2cpp_codegen_object_new(ArgumentNullException_t3214793280_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m135444188(L_5, (String_t*)_stringLiteral2861249324, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_5);
	}

IL_0033:
	{
		Action_t437523947 * L_6 = ___onCompleted2;
		NullCheck((Action_t437523947 *)L_6);
		Action_Invoke_m1445970038((Action_t437523947 *)L_6, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UniRx.Notification`1/OnErrorNotification<System.Object>::.ctor(System.Exception)
extern "C"  void OnErrorNotification__ctor_m1734167588_gshared (OnErrorNotification_t36474669 * __this, Exception_t1967233988 * ___exception0, const MethodInfo* method)
{
	{
		NullCheck((Notification_1_t38356375 *)__this);
		((  void (*) (Notification_1_t38356375 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((Notification_1_t38356375 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		Exception_t1967233988 * L_0 = ___exception0;
		__this->set_exception_0(L_0);
		return;
	}
}
// T UniRx.Notification`1/OnErrorNotification<System.Object>::get_Value()
extern "C"  Il2CppObject * OnErrorNotification_get_Value_m3306744921_gshared (OnErrorNotification_t36474669 * __this, const MethodInfo* method)
{
	{
		Exception_t1967233988 * L_0 = (Exception_t1967233988 *)__this->get_exception_0();
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Exception UniRx.Notification`1/OnErrorNotification<System.Object>::get_Exception()
extern "C"  Exception_t1967233988 * OnErrorNotification_get_Exception_m3413657613_gshared (OnErrorNotification_t36474669 * __this, const MethodInfo* method)
{
	{
		Exception_t1967233988 * L_0 = (Exception_t1967233988 *)__this->get_exception_0();
		return L_0;
	}
}
// System.Boolean UniRx.Notification`1/OnErrorNotification<System.Object>::get_HasValue()
extern "C"  bool OnErrorNotification_get_HasValue_m3730319604_gshared (OnErrorNotification_t36474669 * __this, const MethodInfo* method)
{
	{
		return (bool)0;
	}
}
// UniRx.NotificationKind UniRx.Notification`1/OnErrorNotification<System.Object>::get_Kind()
extern "C"  int32_t OnErrorNotification_get_Kind_m3558801887_gshared (OnErrorNotification_t36474669 * __this, const MethodInfo* method)
{
	{
		return (int32_t)(1);
	}
}
// System.Int32 UniRx.Notification`1/OnErrorNotification<System.Object>::GetHashCode()
extern "C"  int32_t OnErrorNotification_GetHashCode_m734850713_gshared (OnErrorNotification_t36474669 * __this, const MethodInfo* method)
{
	{
		NullCheck((OnErrorNotification_t36474669 *)__this);
		Exception_t1967233988 * L_0 = ((  Exception_t1967233988 * (*) (OnErrorNotification_t36474669 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)((OnErrorNotification_t36474669 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		NullCheck((Il2CppObject *)L_0);
		int32_t L_1 = VirtFuncInvoker0< int32_t >::Invoke(2 /* System.Int32 System.Object::GetHashCode() */, (Il2CppObject *)L_0);
		return L_1;
	}
}
// System.Boolean UniRx.Notification`1/OnErrorNotification<System.Object>::Equals(UniRx.Notification`1<T>)
extern "C"  bool OnErrorNotification_Equals_m2626472589_gshared (OnErrorNotification_t36474669 * __this, Notification_1_t38356375 * ___other0, const MethodInfo* method)
{
	{
		Notification_1_t38356375 * L_0 = ___other0;
		bool L_1 = Object_ReferenceEquals_m3695130242(NULL /*static, unused*/, (Il2CppObject *)__this, (Il2CppObject *)L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_000e;
		}
	}
	{
		return (bool)1;
	}

IL_000e:
	{
		Notification_1_t38356375 * L_2 = ___other0;
		bool L_3 = Object_ReferenceEquals_m3695130242(NULL /*static, unused*/, (Il2CppObject *)L_2, (Il2CppObject *)NULL, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_001c;
		}
	}
	{
		return (bool)0;
	}

IL_001c:
	{
		Notification_1_t38356375 * L_4 = ___other0;
		NullCheck((Notification_1_t38356375 *)L_4);
		int32_t L_5 = VirtFuncInvoker0< int32_t >::Invoke(8 /* UniRx.NotificationKind UniRx.Notification`1<System.Object>::get_Kind() */, (Notification_1_t38356375 *)L_4);
		if ((((int32_t)L_5) == ((int32_t)1)))
		{
			goto IL_002a;
		}
	}
	{
		return (bool)0;
	}

IL_002a:
	{
		NullCheck((OnErrorNotification_t36474669 *)__this);
		Exception_t1967233988 * L_6 = ((  Exception_t1967233988 * (*) (OnErrorNotification_t36474669 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)((OnErrorNotification_t36474669 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		Notification_1_t38356375 * L_7 = ___other0;
		NullCheck((Notification_1_t38356375 *)L_7);
		Exception_t1967233988 * L_8 = VirtFuncInvoker0< Exception_t1967233988 * >::Invoke(7 /* System.Exception UniRx.Notification`1<System.Object>::get_Exception() */, (Notification_1_t38356375 *)L_7);
		bool L_9 = Object_Equals_m3175838359(NULL /*static, unused*/, (Il2CppObject *)L_6, (Il2CppObject *)L_8, /*hidden argument*/NULL);
		return L_9;
	}
}
// System.String UniRx.Notification`1/OnErrorNotification<System.Object>::ToString()
extern Il2CppClass* CultureInfo_t3603717042_il2cpp_TypeInfo_var;
extern Il2CppClass* ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral795492256;
extern const uint32_t OnErrorNotification_ToString_m2686246201_MetadataUsageId;
extern "C"  String_t* OnErrorNotification_ToString_m2686246201_gshared (OnErrorNotification_t36474669 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (OnErrorNotification_ToString_m2686246201_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(CultureInfo_t3603717042_il2cpp_TypeInfo_var);
		CultureInfo_t3603717042 * L_0 = CultureInfo_get_CurrentCulture_m2905498779(NULL /*static, unused*/, /*hidden argument*/NULL);
		ObjectU5BU5D_t11523773* L_1 = (ObjectU5BU5D_t11523773*)((ObjectU5BU5D_t11523773*)SZArrayNew(ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var, (uint32_t)1));
		NullCheck((OnErrorNotification_t36474669 *)__this);
		Exception_t1967233988 * L_2 = ((  Exception_t1967233988 * (*) (OnErrorNotification_t36474669 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)((OnErrorNotification_t36474669 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		NullCheck((Exception_t1967233988 *)L_2);
		Type_t * L_3 = VirtFuncInvoker0< Type_t * >::Invoke(10 /* System.Type System.Exception::GetType() */, (Exception_t1967233988 *)L_2);
		NullCheck((Type_t *)L_3);
		String_t* L_4 = VirtFuncInvoker0< String_t* >::Invoke(19 /* System.String System.Type::get_FullName() */, (Type_t *)L_3);
		NullCheck(L_1);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_1, 0);
		ArrayElementTypeCheck (L_1, L_4);
		(L_1)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)L_4);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_5 = String_Format_m3351777162(NULL /*static, unused*/, (Il2CppObject *)L_0, (String_t*)_stringLiteral795492256, (ObjectU5BU5D_t11523773*)L_1, /*hidden argument*/NULL);
		return L_5;
	}
}
// System.Void UniRx.Notification`1/OnErrorNotification<System.Object>::Accept(UniRx.IObserver`1<T>)
extern Il2CppClass* ArgumentNullException_t3214793280_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral348607190;
extern const uint32_t OnErrorNotification_Accept_m2048116170_MetadataUsageId;
extern "C"  void OnErrorNotification_Accept_m2048116170_gshared (OnErrorNotification_t36474669 * __this, Il2CppObject* ___observer0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (OnErrorNotification_Accept_m2048116170_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject* L_0 = ___observer0;
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		ArgumentNullException_t3214793280 * L_1 = (ArgumentNullException_t3214793280 *)il2cpp_codegen_object_new(ArgumentNullException_t3214793280_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m135444188(L_1, (String_t*)_stringLiteral348607190, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0011:
	{
		Il2CppObject* L_2 = ___observer0;
		NullCheck((OnErrorNotification_t36474669 *)__this);
		Exception_t1967233988 * L_3 = ((  Exception_t1967233988 * (*) (OnErrorNotification_t36474669 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)((OnErrorNotification_t36474669 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		NullCheck((Il2CppObject*)L_2);
		InterfaceActionInvoker1< Exception_t1967233988 * >::Invoke(1 /* System.Void UniRx.IObserver`1<System.Object>::OnError(System.Exception) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5), (Il2CppObject*)L_2, (Exception_t1967233988 *)L_3);
		return;
	}
}
// System.Void UniRx.Notification`1/OnErrorNotification<System.Object>::Accept(System.Action`1<T>,System.Action`1<System.Exception>,System.Action)
extern Il2CppClass* ArgumentNullException_t3214793280_il2cpp_TypeInfo_var;
extern const MethodInfo* Action_1_Invoke_m1286428014_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral3281847666;
extern Il2CppCodeGenString* _stringLiteral2945099625;
extern Il2CppCodeGenString* _stringLiteral2861249324;
extern const uint32_t OnErrorNotification_Accept_m2008980579_MetadataUsageId;
extern "C"  void OnErrorNotification_Accept_m2008980579_gshared (OnErrorNotification_t36474669 * __this, Action_1_t985559125 * ___onNext0, Action_1_t2115686693 * ___onError1, Action_t437523947 * ___onCompleted2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (OnErrorNotification_Accept_m2008980579_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Action_1_t985559125 * L_0 = ___onNext0;
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		ArgumentNullException_t3214793280 * L_1 = (ArgumentNullException_t3214793280 *)il2cpp_codegen_object_new(ArgumentNullException_t3214793280_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m135444188(L_1, (String_t*)_stringLiteral3281847666, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0011:
	{
		Action_1_t2115686693 * L_2 = ___onError1;
		if (L_2)
		{
			goto IL_0022;
		}
	}
	{
		ArgumentNullException_t3214793280 * L_3 = (ArgumentNullException_t3214793280 *)il2cpp_codegen_object_new(ArgumentNullException_t3214793280_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m135444188(L_3, (String_t*)_stringLiteral2945099625, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_0022:
	{
		Action_t437523947 * L_4 = ___onCompleted2;
		if (L_4)
		{
			goto IL_0033;
		}
	}
	{
		ArgumentNullException_t3214793280 * L_5 = (ArgumentNullException_t3214793280 *)il2cpp_codegen_object_new(ArgumentNullException_t3214793280_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m135444188(L_5, (String_t*)_stringLiteral2861249324, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_5);
	}

IL_0033:
	{
		Action_1_t2115686693 * L_6 = ___onError1;
		NullCheck((OnErrorNotification_t36474669 *)__this);
		Exception_t1967233988 * L_7 = ((  Exception_t1967233988 * (*) (OnErrorNotification_t36474669 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)((OnErrorNotification_t36474669 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		NullCheck((Action_1_t2115686693 *)L_6);
		Action_1_Invoke_m1286428014((Action_1_t2115686693 *)L_6, (Exception_t1967233988 *)L_7, /*hidden argument*/Action_1_Invoke_m1286428014_MethodInfo_var);
		return;
	}
}
// System.Void UniRx.Notification`1/OnNextNotification<System.Object>::.ctor(T)
extern "C"  void OnNextNotification__ctor_m832151699_gshared (OnNextNotification_t3645104142 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	{
		NullCheck((Notification_1_t38356375 *)__this);
		((  void (*) (Notification_1_t38356375 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((Notification_1_t38356375 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		Il2CppObject * L_0 = ___value0;
		__this->set_value_0(L_0);
		return;
	}
}
// T UniRx.Notification`1/OnNextNotification<System.Object>::get_Value()
extern "C"  Il2CppObject * OnNextNotification_get_Value_m629542322_gshared (OnNextNotification_t3645104142 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_value_0();
		return L_0;
	}
}
// System.Exception UniRx.Notification`1/OnNextNotification<System.Object>::get_Exception()
extern "C"  Exception_t1967233988 * OnNextNotification_get_Exception_m2844537466_gshared (OnNextNotification_t3645104142 * __this, const MethodInfo* method)
{
	{
		return (Exception_t1967233988 *)NULL;
	}
}
// System.Boolean UniRx.Notification`1/OnNextNotification<System.Object>::get_HasValue()
extern "C"  bool OnNextNotification_get_HasValue_m3054588949_gshared (OnNextNotification_t3645104142 * __this, const MethodInfo* method)
{
	{
		return (bool)1;
	}
}
// UniRx.NotificationKind UniRx.Notification`1/OnNextNotification<System.Object>::get_Kind()
extern "C"  int32_t OnNextNotification_get_Kind_m30742692_gshared (OnNextNotification_t3645104142 * __this, const MethodInfo* method)
{
	{
		return (int32_t)(0);
	}
}
// System.Int32 UniRx.Notification`1/OnNextNotification<System.Object>::GetHashCode()
extern "C"  int32_t OnNextNotification_GetHashCode_m3412995684_gshared (OnNextNotification_t3645104142 * __this, const MethodInfo* method)
{
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		EqualityComparer_1_t271497070 * L_0 = ((  EqualityComparer_1_t271497070 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		NullCheck((OnNextNotification_t3645104142 *)__this);
		Il2CppObject * L_1 = ((  Il2CppObject * (*) (OnNextNotification_t3645104142 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)((OnNextNotification_t3645104142 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		NullCheck((EqualityComparer_1_t271497070 *)L_0);
		int32_t L_2 = VirtFuncInvoker1< int32_t, Il2CppObject * >::Invoke(8 /* System.Int32 System.Collections.Generic.EqualityComparer`1<System.Object>::GetHashCode(!0) */, (EqualityComparer_1_t271497070 *)L_0, (Il2CppObject *)L_1);
		return L_2;
	}
}
// System.Boolean UniRx.Notification`1/OnNextNotification<System.Object>::Equals(UniRx.Notification`1<T>)
extern "C"  bool OnNextNotification_Equals_m3854378380_gshared (OnNextNotification_t3645104142 * __this, Notification_1_t38356375 * ___other0, const MethodInfo* method)
{
	{
		Notification_1_t38356375 * L_0 = ___other0;
		bool L_1 = Object_ReferenceEquals_m3695130242(NULL /*static, unused*/, (Il2CppObject *)__this, (Il2CppObject *)L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_000e;
		}
	}
	{
		return (bool)1;
	}

IL_000e:
	{
		Notification_1_t38356375 * L_2 = ___other0;
		bool L_3 = Object_ReferenceEquals_m3695130242(NULL /*static, unused*/, (Il2CppObject *)L_2, (Il2CppObject *)NULL, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_001c;
		}
	}
	{
		return (bool)0;
	}

IL_001c:
	{
		Notification_1_t38356375 * L_4 = ___other0;
		NullCheck((Notification_1_t38356375 *)L_4);
		int32_t L_5 = VirtFuncInvoker0< int32_t >::Invoke(8 /* UniRx.NotificationKind UniRx.Notification`1<System.Object>::get_Kind() */, (Notification_1_t38356375 *)L_4);
		if (!L_5)
		{
			goto IL_0029;
		}
	}
	{
		return (bool)0;
	}

IL_0029:
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		EqualityComparer_1_t271497070 * L_6 = ((  EqualityComparer_1_t271497070 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		NullCheck((OnNextNotification_t3645104142 *)__this);
		Il2CppObject * L_7 = ((  Il2CppObject * (*) (OnNextNotification_t3645104142 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)((OnNextNotification_t3645104142 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		Notification_1_t38356375 * L_8 = ___other0;
		NullCheck((Notification_1_t38356375 *)L_8);
		Il2CppObject * L_9 = VirtFuncInvoker0< Il2CppObject * >::Invoke(5 /* T UniRx.Notification`1<System.Object>::get_Value() */, (Notification_1_t38356375 *)L_8);
		NullCheck((EqualityComparer_1_t271497070 *)L_6);
		bool L_10 = VirtFuncInvoker2< bool, Il2CppObject *, Il2CppObject * >::Invoke(9 /* System.Boolean System.Collections.Generic.EqualityComparer`1<System.Object>::Equals(!0,!0) */, (EqualityComparer_1_t271497070 *)L_6, (Il2CppObject *)L_7, (Il2CppObject *)L_9);
		return L_10;
	}
}
// System.String UniRx.Notification`1/OnNextNotification<System.Object>::ToString()
extern Il2CppClass* CultureInfo_t3603717042_il2cpp_TypeInfo_var;
extern Il2CppClass* ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2803667703;
extern const uint32_t OnNextNotification_ToString_m1677606792_MetadataUsageId;
extern "C"  String_t* OnNextNotification_ToString_m1677606792_gshared (OnNextNotification_t3645104142 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (OnNextNotification_ToString_m1677606792_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(CultureInfo_t3603717042_il2cpp_TypeInfo_var);
		CultureInfo_t3603717042 * L_0 = CultureInfo_get_CurrentCulture_m2905498779(NULL /*static, unused*/, /*hidden argument*/NULL);
		ObjectU5BU5D_t11523773* L_1 = (ObjectU5BU5D_t11523773*)((ObjectU5BU5D_t11523773*)SZArrayNew(ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var, (uint32_t)1));
		NullCheck((OnNextNotification_t3645104142 *)__this);
		Il2CppObject * L_2 = ((  Il2CppObject * (*) (OnNextNotification_t3645104142 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)((OnNextNotification_t3645104142 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		NullCheck(L_1);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_1, 0);
		ArrayElementTypeCheck (L_1, L_2);
		(L_1)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)L_2);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_3 = String_Format_m3351777162(NULL /*static, unused*/, (Il2CppObject *)L_0, (String_t*)_stringLiteral2803667703, (ObjectU5BU5D_t11523773*)L_1, /*hidden argument*/NULL);
		return L_3;
	}
}
// System.Void UniRx.Notification`1/OnNextNotification<System.Object>::Accept(UniRx.IObserver`1<T>)
extern Il2CppClass* ArgumentNullException_t3214793280_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral348607190;
extern const uint32_t OnNextNotification_Accept_m1191274355_MetadataUsageId;
extern "C"  void OnNextNotification_Accept_m1191274355_gshared (OnNextNotification_t3645104142 * __this, Il2CppObject* ___observer0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (OnNextNotification_Accept_m1191274355_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject* L_0 = ___observer0;
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		ArgumentNullException_t3214793280 * L_1 = (ArgumentNullException_t3214793280 *)il2cpp_codegen_object_new(ArgumentNullException_t3214793280_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m135444188(L_1, (String_t*)_stringLiteral348607190, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0011:
	{
		Il2CppObject* L_2 = ___observer0;
		NullCheck((OnNextNotification_t3645104142 *)__this);
		Il2CppObject * L_3 = ((  Il2CppObject * (*) (OnNextNotification_t3645104142 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)((OnNextNotification_t3645104142 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		NullCheck((Il2CppObject*)L_2);
		InterfaceActionInvoker1< Il2CppObject * >::Invoke(2 /* System.Void UniRx.IObserver`1<System.Object>::OnNext(T) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10), (Il2CppObject*)L_2, (Il2CppObject *)L_3);
		return;
	}
}
// System.Void UniRx.Notification`1/OnNextNotification<System.Object>::Accept(System.Action`1<T>,System.Action`1<System.Exception>,System.Action)
extern Il2CppClass* ArgumentNullException_t3214793280_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3281847666;
extern Il2CppCodeGenString* _stringLiteral2945099625;
extern Il2CppCodeGenString* _stringLiteral2861249324;
extern const uint32_t OnNextNotification_Accept_m228441676_MetadataUsageId;
extern "C"  void OnNextNotification_Accept_m228441676_gshared (OnNextNotification_t3645104142 * __this, Action_1_t985559125 * ___onNext0, Action_1_t2115686693 * ___onError1, Action_t437523947 * ___onCompleted2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (OnNextNotification_Accept_m228441676_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Action_1_t985559125 * L_0 = ___onNext0;
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		ArgumentNullException_t3214793280 * L_1 = (ArgumentNullException_t3214793280 *)il2cpp_codegen_object_new(ArgumentNullException_t3214793280_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m135444188(L_1, (String_t*)_stringLiteral3281847666, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0011:
	{
		Action_1_t2115686693 * L_2 = ___onError1;
		if (L_2)
		{
			goto IL_0022;
		}
	}
	{
		ArgumentNullException_t3214793280 * L_3 = (ArgumentNullException_t3214793280 *)il2cpp_codegen_object_new(ArgumentNullException_t3214793280_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m135444188(L_3, (String_t*)_stringLiteral2945099625, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_0022:
	{
		Action_t437523947 * L_4 = ___onCompleted2;
		if (L_4)
		{
			goto IL_0033;
		}
	}
	{
		ArgumentNullException_t3214793280 * L_5 = (ArgumentNullException_t3214793280 *)il2cpp_codegen_object_new(ArgumentNullException_t3214793280_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m135444188(L_5, (String_t*)_stringLiteral2861249324, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_5);
	}

IL_0033:
	{
		Action_1_t985559125 * L_6 = ___onNext0;
		NullCheck((OnNextNotification_t3645104142 *)__this);
		Il2CppObject * L_7 = ((  Il2CppObject * (*) (OnNextNotification_t3645104142 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)((OnNextNotification_t3645104142 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		NullCheck((Action_1_t985559125 *)L_6);
		((  void (*) (Action_1_t985559125 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 11)->method)((Action_1_t985559125 *)L_6, (Il2CppObject *)L_7, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 11));
		return;
	}
}
// System.Void UniRx.Notification`1<System.Object>::.ctor()
extern "C"  void Notification_1__ctor_m2783159343_gshared (Notification_1_t38356375 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean UniRx.Notification`1<System.Object>::Equals(System.Object)
extern "C"  bool Notification_1_Equals_m1024238204_gshared (Notification_1_t38356375 * __this, Il2CppObject * ___obj0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___obj0;
		NullCheck((Notification_1_t38356375 *)__this);
		bool L_1 = VirtFuncInvoker1< bool, Notification_1_t38356375 * >::Invoke(9 /* System.Boolean UniRx.Notification`1<System.Object>::Equals(UniRx.Notification`1<T>) */, (Notification_1_t38356375 *)__this, (Notification_1_t38356375 *)((Notification_1_t38356375 *)IsInst(L_0, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0))));
		return L_1;
	}
}
// UniRx.IObservable`1<T> UniRx.Notification`1<System.Object>::ToObservable()
extern Il2CppClass* Scheduler_t103934925_il2cpp_TypeInfo_var;
extern const uint32_t Notification_1_ToObservable_m2814509903_MetadataUsageId;
extern "C"  Il2CppObject* Notification_1_ToObservable_m2814509903_gshared (Notification_1_t38356375 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Notification_1_ToObservable_m2814509903_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Scheduler_t103934925_il2cpp_TypeInfo_var);
		Il2CppObject * L_0 = ((Scheduler_t103934925_StaticFields*)Scheduler_t103934925_il2cpp_TypeInfo_var->static_fields)->get_Immediate_1();
		NullCheck((Notification_1_t38356375 *)__this);
		Il2CppObject* L_1 = ((  Il2CppObject* (*) (Notification_1_t38356375 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)((Notification_1_t38356375 *)__this, (Il2CppObject *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_1;
	}
}
// UniRx.IObservable`1<T> UniRx.Notification`1<System.Object>::ToObservable(UniRx.IScheduler)
extern Il2CppClass* ArgumentNullException_t3214793280_il2cpp_TypeInfo_var;
extern Il2CppClass* Observable_t258071701_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral4134256827;
extern const uint32_t Notification_1_ToObservable_m3055295589_MetadataUsageId;
extern "C"  Il2CppObject* Notification_1_ToObservable_m3055295589_gshared (Notification_1_t38356375 * __this, Il2CppObject * ___scheduler0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Notification_1_ToObservable_m3055295589_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	U3CToObservableU3Ec__AnonStorey55_t1984678244 * V_0 = NULL;
	{
		U3CToObservableU3Ec__AnonStorey55_t1984678244 * L_0 = (U3CToObservableU3Ec__AnonStorey55_t1984678244 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		((  void (*) (U3CToObservableU3Ec__AnonStorey55_t1984678244 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		V_0 = (U3CToObservableU3Ec__AnonStorey55_t1984678244 *)L_0;
		U3CToObservableU3Ec__AnonStorey55_t1984678244 * L_1 = V_0;
		Il2CppObject * L_2 = ___scheduler0;
		NullCheck(L_1);
		L_1->set_scheduler_0(L_2);
		U3CToObservableU3Ec__AnonStorey55_t1984678244 * L_3 = V_0;
		NullCheck(L_3);
		L_3->set_U3CU3Ef__this_1(__this);
		U3CToObservableU3Ec__AnonStorey55_t1984678244 * L_4 = V_0;
		NullCheck(L_4);
		Il2CppObject * L_5 = (Il2CppObject *)L_4->get_scheduler_0();
		if (L_5)
		{
			goto IL_002a;
		}
	}
	{
		ArgumentNullException_t3214793280 * L_6 = (ArgumentNullException_t3214793280 *)il2cpp_codegen_object_new(ArgumentNullException_t3214793280_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m135444188(L_6, (String_t*)_stringLiteral4134256827, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_6);
	}

IL_002a:
	{
		U3CToObservableU3Ec__AnonStorey55_t1984678244 * L_7 = V_0;
		IntPtr_t L_8;
		L_8.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		Func_2_t2619763055 * L_9 = (Func_2_t2619763055 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		((  void (*) (Func_2_t2619763055 *, Il2CppObject *, IntPtr_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7)->method)(L_9, (Il2CppObject *)L_7, (IntPtr_t)L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7));
		IL2CPP_RUNTIME_CLASS_INIT(Observable_t258071701_il2cpp_TypeInfo_var);
		Il2CppObject* L_10 = ((  Il2CppObject* (*) (Il2CppObject * /* static, unused */, Func_2_t2619763055 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8)->method)(NULL /*static, unused*/, (Func_2_t2619763055 *)L_9, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8));
		return L_10;
	}
}
// System.Boolean UniRx.Notification`1<System.Object>::op_Equality(UniRx.Notification`1<T>,UniRx.Notification`1<T>)
extern "C"  bool Notification_1_op_Equality_m2804091545_gshared (Il2CppObject * __this /* static, unused */, Notification_1_t38356375 * ___left0, Notification_1_t38356375 * ___right1, const MethodInfo* method)
{
	{
		Notification_1_t38356375 * L_0 = ___left0;
		Notification_1_t38356375 * L_1 = ___right1;
		bool L_2 = Object_ReferenceEquals_m3695130242(NULL /*static, unused*/, (Il2CppObject *)L_0, (Il2CppObject *)L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_000e;
		}
	}
	{
		return (bool)1;
	}

IL_000e:
	{
		Notification_1_t38356375 * L_3 = ___left0;
		if (!L_3)
		{
			goto IL_001a;
		}
	}
	{
		Notification_1_t38356375 * L_4 = ___right1;
		if (L_4)
		{
			goto IL_001c;
		}
	}

IL_001a:
	{
		return (bool)0;
	}

IL_001c:
	{
		Notification_1_t38356375 * L_5 = ___left0;
		Notification_1_t38356375 * L_6 = ___right1;
		NullCheck((Notification_1_t38356375 *)L_5);
		bool L_7 = VirtFuncInvoker1< bool, Notification_1_t38356375 * >::Invoke(9 /* System.Boolean UniRx.Notification`1<System.Object>::Equals(UniRx.Notification`1<T>) */, (Notification_1_t38356375 *)L_5, (Notification_1_t38356375 *)L_6);
		return L_7;
	}
}
// System.Boolean UniRx.Notification`1<System.Object>::op_Inequality(UniRx.Notification`1<T>,UniRx.Notification`1<T>)
extern "C"  bool Notification_1_op_Inequality_m3842301588_gshared (Il2CppObject * __this /* static, unused */, Notification_1_t38356375 * ___left0, Notification_1_t38356375 * ___right1, const MethodInfo* method)
{
	{
		Notification_1_t38356375 * L_0 = ___left0;
		Notification_1_t38356375 * L_1 = ___right1;
		bool L_2 = ((  bool (*) (Il2CppObject * /* static, unused */, Notification_1_t38356375 *, Notification_1_t38356375 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9)->method)(NULL /*static, unused*/, (Notification_1_t38356375 *)L_0, (Notification_1_t38356375 *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9));
		return (bool)((((int32_t)L_2) == ((int32_t)0))? 1 : 0);
	}
}
// System.Void UniRx.Observable/<AddRef>c__AnonStorey37`1<System.Object>::.ctor()
extern "C"  void U3CAddRefU3Ec__AnonStorey37_1__ctor_m3687565875_gshared (U3CAddRefU3Ec__AnonStorey37_1_t3332811877 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.IDisposable UniRx.Observable/<AddRef>c__AnonStorey37`1<System.Object>::<>m__3B(UniRx.IObserver`1<T>)
extern Il2CppClass* IDisposableU5BU5D_t165183403_il2cpp_TypeInfo_var;
extern Il2CppClass* CompositeDisposable_t1894629977_il2cpp_TypeInfo_var;
extern const uint32_t U3CAddRefU3Ec__AnonStorey37_1_U3CU3Em__3B_m4227423448_MetadataUsageId;
extern "C"  Il2CppObject * U3CAddRefU3Ec__AnonStorey37_1_U3CU3Em__3B_m4227423448_gshared (U3CAddRefU3Ec__AnonStorey37_1_t3332811877 * __this, Il2CppObject* ___observer0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CAddRefU3Ec__AnonStorey37_1_U3CU3Em__3B_m4227423448_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IDisposableU5BU5D_t165183403* L_0 = (IDisposableU5BU5D_t165183403*)((IDisposableU5BU5D_t165183403*)SZArrayNew(IDisposableU5BU5D_t165183403_il2cpp_TypeInfo_var, (uint32_t)2));
		RefCountDisposable_t3288429070 * L_1 = (RefCountDisposable_t3288429070 *)__this->get_r_0();
		NullCheck((RefCountDisposable_t3288429070 *)L_1);
		Il2CppObject * L_2 = RefCountDisposable_GetDisposable_m4263689044((RefCountDisposable_t3288429070 *)L_1, /*hidden argument*/NULL);
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, L_2);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)L_2);
		IDisposableU5BU5D_t165183403* L_3 = (IDisposableU5BU5D_t165183403*)L_0;
		Il2CppObject* L_4 = (Il2CppObject*)__this->get_xs_1();
		Il2CppObject* L_5 = ___observer0;
		NullCheck((Il2CppObject*)L_4);
		Il2CppObject * L_6 = InterfaceFuncInvoker1< Il2CppObject *, Il2CppObject* >::Invoke(0 /* System.IDisposable UniRx.IObservable`1<System.Object>::Subscribe(UniRx.IObserver`1<T>) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0), (Il2CppObject*)L_4, (Il2CppObject*)L_5);
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, 1);
		ArrayElementTypeCheck (L_3, L_6);
		(L_3)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_6);
		CompositeDisposable_t1894629977 * L_7 = (CompositeDisposable_t1894629977 *)il2cpp_codegen_object_new(CompositeDisposable_t1894629977_il2cpp_TypeInfo_var);
		CompositeDisposable__ctor_m654163804(L_7, (IDisposableU5BU5D_t165183403*)L_3, /*hidden argument*/NULL);
		return L_7;
	}
}
// System.Void UniRx.Observable/<CatchIgnore>c__AnonStorey3C`2<System.Object,System.Object>::.ctor()
extern "C"  void U3CCatchIgnoreU3Ec__AnonStorey3C_2__ctor_m380469_gshared (U3CCatchIgnoreU3Ec__AnonStorey3C_2_t339163467 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// UniRx.IObservable`1<TSource> UniRx.Observable/<CatchIgnore>c__AnonStorey3C`2<System.Object,System.Object>::<>m__3E(TException)
extern Il2CppClass* Observable_t258071701_il2cpp_TypeInfo_var;
extern const uint32_t U3CCatchIgnoreU3Ec__AnonStorey3C_2_U3CU3Em__3E_m644321494_MetadataUsageId;
extern "C"  Il2CppObject* U3CCatchIgnoreU3Ec__AnonStorey3C_2_U3CU3Em__3E_m644321494_gshared (U3CCatchIgnoreU3Ec__AnonStorey3C_2_t339163467 * __this, Il2CppObject * ___ex0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CCatchIgnoreU3Ec__AnonStorey3C_2_U3CU3Em__3E_m644321494_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Action_1_t985559125 * L_0 = (Action_1_t985559125 *)__this->get_errorAction_0();
		Il2CppObject * L_1 = ___ex0;
		NullCheck((Action_1_t985559125 *)L_0);
		((  void (*) (Action_1_t985559125 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((Action_1_t985559125 *)L_0, (Il2CppObject *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		IL2CPP_RUNTIME_CLASS_INIT(Observable_t258071701_il2cpp_TypeInfo_var);
		Il2CppObject* L_2 = ((  Il2CppObject* (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		return L_2;
	}
}
// System.Void UniRx.Observable/<CombineSources>c__IteratorD`1<System.Object>::.ctor()
extern "C"  void U3CCombineSourcesU3Ec__IteratorD_1__ctor_m301271444_gshared (U3CCombineSourcesU3Ec__IteratorD_1_t2571110558 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// UniRx.IObservable`1<T> UniRx.Observable/<CombineSources>c__IteratorD`1<System.Object>::System.Collections.Generic.IEnumerator<UniRx.IObservable<T>>.get_Current()
extern "C"  Il2CppObject* U3CCombineSourcesU3Ec__IteratorD_1_System_Collections_Generic_IEnumeratorU3CUniRx_IObservableU3CTU3EU3E_get_Current_m2795496324_gshared (U3CCombineSourcesU3Ec__IteratorD_1_t2571110558 * __this, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = (Il2CppObject*)__this->get_U24current_4();
		return L_0;
	}
}
// System.Object UniRx.Observable/<CombineSources>c__IteratorD`1<System.Object>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CCombineSourcesU3Ec__IteratorD_1_System_Collections_IEnumerator_get_Current_m844946076_gshared (U3CCombineSourcesU3Ec__IteratorD_1_t2571110558 * __this, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = (Il2CppObject*)__this->get_U24current_4();
		return L_0;
	}
}
// System.Collections.IEnumerator UniRx.Observable/<CombineSources>c__IteratorD`1<System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * U3CCombineSourcesU3Ec__IteratorD_1_System_Collections_IEnumerable_GetEnumerator_m3725167557_gshared (U3CCombineSourcesU3Ec__IteratorD_1_t2571110558 * __this, const MethodInfo* method)
{
	{
		NullCheck((U3CCombineSourcesU3Ec__IteratorD_1_t2571110558 *)__this);
		Il2CppObject* L_0 = ((  Il2CppObject* (*) (U3CCombineSourcesU3Ec__IteratorD_1_t2571110558 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((U3CCombineSourcesU3Ec__IteratorD_1_t2571110558 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		return L_0;
	}
}
// System.Collections.Generic.IEnumerator`1<UniRx.IObservable`1<T>> UniRx.Observable/<CombineSources>c__IteratorD`1<System.Object>::System.Collections.Generic.IEnumerable<UniRx.IObservable<T>>.GetEnumerator()
extern "C"  Il2CppObject* U3CCombineSourcesU3Ec__IteratorD_1_System_Collections_Generic_IEnumerableU3CUniRx_IObservableU3CTU3EU3E_GetEnumerator_m3078531331_gshared (U3CCombineSourcesU3Ec__IteratorD_1_t2571110558 * __this, const MethodInfo* method)
{
	U3CCombineSourcesU3Ec__IteratorD_1_t2571110558 * V_0 = NULL;
	{
		int32_t* L_0 = (int32_t*)__this->get_address_of_U24PC_3();
		int32_t L_1 = Interlocked_CompareExchange_m1859820752(NULL /*static, unused*/, (int32_t*)L_0, (int32_t)0, (int32_t)((int32_t)-2), /*hidden argument*/NULL);
		if ((!(((uint32_t)L_1) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0014;
		}
	}
	{
		return __this;
	}

IL_0014:
	{
		U3CCombineSourcesU3Ec__IteratorD_1_t2571110558 * L_2 = (U3CCombineSourcesU3Ec__IteratorD_1_t2571110558 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		((  void (*) (U3CCombineSourcesU3Ec__IteratorD_1_t2571110558 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)(L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		V_0 = (U3CCombineSourcesU3Ec__IteratorD_1_t2571110558 *)L_2;
		U3CCombineSourcesU3Ec__IteratorD_1_t2571110558 * L_3 = V_0;
		Il2CppObject* L_4 = (Il2CppObject*)__this->get_U3CU24U3Efirst_5();
		NullCheck(L_3);
		L_3->set_first_0(L_4);
		U3CCombineSourcesU3Ec__IteratorD_1_t2571110558 * L_5 = V_0;
		IObservable_1U5BU5D_t2205425841* L_6 = (IObservable_1U5BU5D_t2205425841*)__this->get_U3CU24U3Eseconds_6();
		NullCheck(L_5);
		L_5->set_seconds_2(L_6);
		U3CCombineSourcesU3Ec__IteratorD_1_t2571110558 * L_7 = V_0;
		return L_7;
	}
}
// System.Boolean UniRx.Observable/<CombineSources>c__IteratorD`1<System.Object>::MoveNext()
extern "C"  bool U3CCombineSourcesU3Ec__IteratorD_1_MoveNext_m3300122032_gshared (U3CCombineSourcesU3Ec__IteratorD_1_t2571110558 * __this, const MethodInfo* method)
{
	uint32_t V_0 = 0;
	bool V_1 = false;
	{
		int32_t L_0 = (int32_t)__this->get_U24PC_3();
		V_0 = (uint32_t)L_0;
		__this->set_U24PC_3((-1));
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0025;
		}
		if (L_1 == 1)
		{
			goto IL_003d;
		}
		if (L_1 == 2)
		{
			goto IL_0068;
		}
	}
	{
		goto IL_0090;
	}

IL_0025:
	{
		Il2CppObject* L_2 = (Il2CppObject*)__this->get_first_0();
		__this->set_U24current_4(L_2);
		__this->set_U24PC_3(1);
		goto IL_0092;
	}

IL_003d:
	{
		__this->set_U3CiU3E__0_1(0);
		goto IL_0076;
	}

IL_0049:
	{
		IObservable_1U5BU5D_t2205425841* L_3 = (IObservable_1U5BU5D_t2205425841*)__this->get_seconds_2();
		int32_t L_4 = (int32_t)__this->get_U3CiU3E__0_1();
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, L_4);
		int32_t L_5 = L_4;
		__this->set_U24current_4(((L_3)->GetAt(static_cast<il2cpp_array_size_t>(L_5))));
		__this->set_U24PC_3(2);
		goto IL_0092;
	}

IL_0068:
	{
		int32_t L_6 = (int32_t)__this->get_U3CiU3E__0_1();
		__this->set_U3CiU3E__0_1(((int32_t)((int32_t)L_6+(int32_t)1)));
	}

IL_0076:
	{
		int32_t L_7 = (int32_t)__this->get_U3CiU3E__0_1();
		IObservable_1U5BU5D_t2205425841* L_8 = (IObservable_1U5BU5D_t2205425841*)__this->get_seconds_2();
		NullCheck(L_8);
		if ((((int32_t)L_7) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_8)->max_length)))))))
		{
			goto IL_0049;
		}
	}
	{
		__this->set_U24PC_3((-1));
	}

IL_0090:
	{
		return (bool)0;
	}

IL_0092:
	{
		return (bool)1;
	}
	// Dead block : IL_0094: ldloc.1
}
// System.Void UniRx.Observable/<CombineSources>c__IteratorD`1<System.Object>::Dispose()
extern "C"  void U3CCombineSourcesU3Ec__IteratorD_1_Dispose_m1657331921_gshared (U3CCombineSourcesU3Ec__IteratorD_1_t2571110558 * __this, const MethodInfo* method)
{
	{
		__this->set_U24PC_3((-1));
		return;
	}
}
// System.Void UniRx.Observable/<CombineSources>c__IteratorD`1<System.Object>::Reset()
extern Il2CppClass* NotSupportedException_t1374155497_il2cpp_TypeInfo_var;
extern const uint32_t U3CCombineSourcesU3Ec__IteratorD_1_Reset_m2242671681_MetadataUsageId;
extern "C"  void U3CCombineSourcesU3Ec__IteratorD_1_Reset_m2242671681_gshared (U3CCombineSourcesU3Ec__IteratorD_1_t2571110558 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CCombineSourcesU3Ec__IteratorD_1_Reset_m2242671681_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1374155497 * L_0 = (NotSupportedException_t1374155497 *)il2cpp_codegen_object_new(NotSupportedException_t1374155497_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m149930845(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void UniRx.Observable/<CombineSources>c__IteratorD`1<UniRx.Unit>::.ctor()
extern "C"  void U3CCombineSourcesU3Ec__IteratorD_1__ctor_m689220816_gshared (U3CCombineSourcesU3Ec__IteratorD_1_t4292290176 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// UniRx.IObservable`1<T> UniRx.Observable/<CombineSources>c__IteratorD`1<UniRx.Unit>::System.Collections.Generic.IEnumerator<UniRx.IObservable<T>>.get_Current()
extern "C"  Il2CppObject* U3CCombineSourcesU3Ec__IteratorD_1_System_Collections_Generic_IEnumeratorU3CUniRx_IObservableU3CTU3EU3E_get_Current_m972461888_gshared (U3CCombineSourcesU3Ec__IteratorD_1_t4292290176 * __this, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = (Il2CppObject*)__this->get_U24current_4();
		return L_0;
	}
}
// System.Object UniRx.Observable/<CombineSources>c__IteratorD`1<UniRx.Unit>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CCombineSourcesU3Ec__IteratorD_1_System_Collections_IEnumerator_get_Current_m3628668054_gshared (U3CCombineSourcesU3Ec__IteratorD_1_t4292290176 * __this, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = (Il2CppObject*)__this->get_U24current_4();
		return L_0;
	}
}
// System.Collections.IEnumerator UniRx.Observable/<CombineSources>c__IteratorD`1<UniRx.Unit>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * U3CCombineSourcesU3Ec__IteratorD_1_System_Collections_IEnumerable_GetEnumerator_m1741645993_gshared (U3CCombineSourcesU3Ec__IteratorD_1_t4292290176 * __this, const MethodInfo* method)
{
	{
		NullCheck((U3CCombineSourcesU3Ec__IteratorD_1_t4292290176 *)__this);
		Il2CppObject* L_0 = ((  Il2CppObject* (*) (U3CCombineSourcesU3Ec__IteratorD_1_t4292290176 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((U3CCombineSourcesU3Ec__IteratorD_1_t4292290176 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		return L_0;
	}
}
// System.Collections.Generic.IEnumerator`1<UniRx.IObservable`1<T>> UniRx.Observable/<CombineSources>c__IteratorD`1<UniRx.Unit>::System.Collections.Generic.IEnumerable<UniRx.IObservable<T>>.GetEnumerator()
extern "C"  Il2CppObject* U3CCombineSourcesU3Ec__IteratorD_1_System_Collections_Generic_IEnumerableU3CUniRx_IObservableU3CTU3EU3E_GetEnumerator_m3455808573_gshared (U3CCombineSourcesU3Ec__IteratorD_1_t4292290176 * __this, const MethodInfo* method)
{
	U3CCombineSourcesU3Ec__IteratorD_1_t4292290176 * V_0 = NULL;
	{
		int32_t* L_0 = (int32_t*)__this->get_address_of_U24PC_3();
		int32_t L_1 = Interlocked_CompareExchange_m1859820752(NULL /*static, unused*/, (int32_t*)L_0, (int32_t)0, (int32_t)((int32_t)-2), /*hidden argument*/NULL);
		if ((!(((uint32_t)L_1) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0014;
		}
	}
	{
		return __this;
	}

IL_0014:
	{
		U3CCombineSourcesU3Ec__IteratorD_1_t4292290176 * L_2 = (U3CCombineSourcesU3Ec__IteratorD_1_t4292290176 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		((  void (*) (U3CCombineSourcesU3Ec__IteratorD_1_t4292290176 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)(L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		V_0 = (U3CCombineSourcesU3Ec__IteratorD_1_t4292290176 *)L_2;
		U3CCombineSourcesU3Ec__IteratorD_1_t4292290176 * L_3 = V_0;
		Il2CppObject* L_4 = (Il2CppObject*)__this->get_U3CU24U3Efirst_5();
		NullCheck(L_3);
		L_3->set_first_0(L_4);
		U3CCombineSourcesU3Ec__IteratorD_1_t4292290176 * L_5 = V_0;
		IObservable_1U5BU5D_t3461854471* L_6 = (IObservable_1U5BU5D_t3461854471*)__this->get_U3CU24U3Eseconds_6();
		NullCheck(L_5);
		L_5->set_seconds_2(L_6);
		U3CCombineSourcesU3Ec__IteratorD_1_t4292290176 * L_7 = V_0;
		return L_7;
	}
}
// System.Boolean UniRx.Observable/<CombineSources>c__IteratorD`1<UniRx.Unit>::MoveNext()
extern "C"  bool U3CCombineSourcesU3Ec__IteratorD_1_MoveNext_m2433675580_gshared (U3CCombineSourcesU3Ec__IteratorD_1_t4292290176 * __this, const MethodInfo* method)
{
	uint32_t V_0 = 0;
	bool V_1 = false;
	{
		int32_t L_0 = (int32_t)__this->get_U24PC_3();
		V_0 = (uint32_t)L_0;
		__this->set_U24PC_3((-1));
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0025;
		}
		if (L_1 == 1)
		{
			goto IL_003d;
		}
		if (L_1 == 2)
		{
			goto IL_0068;
		}
	}
	{
		goto IL_0090;
	}

IL_0025:
	{
		Il2CppObject* L_2 = (Il2CppObject*)__this->get_first_0();
		__this->set_U24current_4(L_2);
		__this->set_U24PC_3(1);
		goto IL_0092;
	}

IL_003d:
	{
		__this->set_U3CiU3E__0_1(0);
		goto IL_0076;
	}

IL_0049:
	{
		IObservable_1U5BU5D_t3461854471* L_3 = (IObservable_1U5BU5D_t3461854471*)__this->get_seconds_2();
		int32_t L_4 = (int32_t)__this->get_U3CiU3E__0_1();
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, L_4);
		int32_t L_5 = L_4;
		__this->set_U24current_4(((L_3)->GetAt(static_cast<il2cpp_array_size_t>(L_5))));
		__this->set_U24PC_3(2);
		goto IL_0092;
	}

IL_0068:
	{
		int32_t L_6 = (int32_t)__this->get_U3CiU3E__0_1();
		__this->set_U3CiU3E__0_1(((int32_t)((int32_t)L_6+(int32_t)1)));
	}

IL_0076:
	{
		int32_t L_7 = (int32_t)__this->get_U3CiU3E__0_1();
		IObservable_1U5BU5D_t3461854471* L_8 = (IObservable_1U5BU5D_t3461854471*)__this->get_seconds_2();
		NullCheck(L_8);
		if ((((int32_t)L_7) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_8)->max_length)))))))
		{
			goto IL_0049;
		}
	}
	{
		__this->set_U24PC_3((-1));
	}

IL_0090:
	{
		return (bool)0;
	}

IL_0092:
	{
		return (bool)1;
	}
	// Dead block : IL_0094: ldloc.1
}
// System.Void UniRx.Observable/<CombineSources>c__IteratorD`1<UniRx.Unit>::Dispose()
extern "C"  void U3CCombineSourcesU3Ec__IteratorD_1_Dispose_m814523661_gshared (U3CCombineSourcesU3Ec__IteratorD_1_t4292290176 * __this, const MethodInfo* method)
{
	{
		__this->set_U24PC_3((-1));
		return;
	}
}
// System.Void UniRx.Observable/<CombineSources>c__IteratorD`1<UniRx.Unit>::Reset()
extern Il2CppClass* NotSupportedException_t1374155497_il2cpp_TypeInfo_var;
extern const uint32_t U3CCombineSourcesU3Ec__IteratorD_1_Reset_m2630621053_MetadataUsageId;
extern "C"  void U3CCombineSourcesU3Ec__IteratorD_1_Reset_m2630621053_gshared (U3CCombineSourcesU3Ec__IteratorD_1_t4292290176 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CCombineSourcesU3Ec__IteratorD_1_Reset_m2630621053_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1374155497 * L_0 = (NotSupportedException_t1374155497 *)il2cpp_codegen_object_new(NotSupportedException_t1374155497_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m149930845(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void UniRx.Observable/<ContinueWith>c__AnonStorey48`2<System.Object,System.Object>::.ctor()
extern "C"  void U3CContinueWithU3Ec__AnonStorey48_2__ctor_m1053906337_gshared (U3CContinueWithU3Ec__AnonStorey48_2_t215346063 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// UniRx.IObservable`1<TR> UniRx.Observable/<ContinueWith>c__AnonStorey48`2<System.Object,System.Object>::<>m__46(T)
extern "C"  Il2CppObject* U3CContinueWithU3Ec__AnonStorey48_2_U3CU3Em__46_m3330506120_gshared (U3CContinueWithU3Ec__AnonStorey48_2_t215346063 * __this, Il2CppObject * ____0, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = (Il2CppObject*)__this->get_other_0();
		return L_0;
	}
}
// System.Void UniRx.Observable/<FromAsyncPattern>c__AnonStorey3F`1/<FromAsyncPattern>c__AnonStorey40`1<System.Object>::.ctor()
extern "C"  void U3CFromAsyncPatternU3Ec__AnonStorey40_1__ctor_m3080020335_gshared (U3CFromAsyncPatternU3Ec__AnonStorey40_1_t1660482281 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UniRx.Observable/<FromAsyncPattern>c__AnonStorey3F`1/<FromAsyncPattern>c__AnonStorey40`1<System.Object>::<>m__65(System.IAsyncResult)
extern Il2CppClass* Exception_t1967233988_il2cpp_TypeInfo_var;
extern const uint32_t U3CFromAsyncPatternU3Ec__AnonStorey40_1_U3CU3Em__65_m1377915148_MetadataUsageId;
extern "C"  void U3CFromAsyncPatternU3Ec__AnonStorey40_1_U3CU3Em__65_m1377915148_gshared (U3CFromAsyncPatternU3Ec__AnonStorey40_1_t1660482281 * __this, Il2CppObject * ___iar0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CFromAsyncPatternU3Ec__AnonStorey40_1_U3CU3Em__65_m1377915148_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Il2CppObject * V_0 = NULL;
	Exception_t1967233988 * V_1 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		U3CFromAsyncPatternU3Ec__AnonStorey3F_1_t1370739976 * L_0 = (U3CFromAsyncPatternU3Ec__AnonStorey3F_1_t1370739976 *)__this->get_U3CU3Ef__refU2463_1();
		NullCheck(L_0);
		Func_2_t850390275 * L_1 = (Func_2_t850390275 *)L_0->get_end_1();
		Il2CppObject * L_2 = ___iar0;
		NullCheck((Func_2_t850390275 *)L_1);
		Il2CppObject * L_3 = ((  Il2CppObject * (*) (Func_2_t850390275 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((Func_2_t850390275 *)L_1, (Il2CppObject *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		V_0 = (Il2CppObject *)L_3;
		goto IL_002e;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1967233988 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Exception_t1967233988_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_0017;
		throw e;
	}

CATCH_0017:
	{ // begin catch(System.Exception)
		{
			V_1 = (Exception_t1967233988 *)((Exception_t1967233988 *)__exception_local);
			AsyncSubject_1_t1536745524 * L_4 = (AsyncSubject_1_t1536745524 *)__this->get_subject_0();
			Exception_t1967233988 * L_5 = V_1;
			NullCheck((AsyncSubject_1_t1536745524 *)L_4);
			((  void (*) (AsyncSubject_1_t1536745524 *, Exception_t1967233988 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)((AsyncSubject_1_t1536745524 *)L_4, (Exception_t1967233988 *)L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
			goto IL_0045;
		}

IL_0029:
		{
			; // IL_0029: leave IL_002e
		}
	} // end catch (depth: 1)

IL_002e:
	{
		AsyncSubject_1_t1536745524 * L_6 = (AsyncSubject_1_t1536745524 *)__this->get_subject_0();
		Il2CppObject * L_7 = V_0;
		NullCheck((AsyncSubject_1_t1536745524 *)L_6);
		((  void (*) (AsyncSubject_1_t1536745524 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)((AsyncSubject_1_t1536745524 *)L_6, (Il2CppObject *)L_7, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		AsyncSubject_1_t1536745524 * L_8 = (AsyncSubject_1_t1536745524 *)__this->get_subject_0();
		NullCheck((AsyncSubject_1_t1536745524 *)L_8);
		((  void (*) (AsyncSubject_1_t1536745524 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)((AsyncSubject_1_t1536745524 *)L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
	}

IL_0045:
	{
		return;
	}
}
// System.Void UniRx.Observable/<FromAsyncPattern>c__AnonStorey3F`1/<FromAsyncPattern>c__AnonStorey40`1<UniRx.Unit>::.ctor()
extern "C"  void U3CFromAsyncPatternU3Ec__AnonStorey40_1__ctor_m1551882965_gshared (U3CFromAsyncPatternU3Ec__AnonStorey40_1_t3381661899 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UniRx.Observable/<FromAsyncPattern>c__AnonStorey3F`1/<FromAsyncPattern>c__AnonStorey40`1<UniRx.Unit>::<>m__65(System.IAsyncResult)
extern Il2CppClass* Exception_t1967233988_il2cpp_TypeInfo_var;
extern const uint32_t U3CFromAsyncPatternU3Ec__AnonStorey40_1_U3CU3Em__65_m2448741222_MetadataUsageId;
extern "C"  void U3CFromAsyncPatternU3Ec__AnonStorey40_1_U3CU3Em__65_m2448741222_gshared (U3CFromAsyncPatternU3Ec__AnonStorey40_1_t3381661899 * __this, Il2CppObject * ___iar0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CFromAsyncPatternU3Ec__AnonStorey40_1_U3CU3Em__65_m2448741222_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Unit_t2558286038  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Exception_t1967233988 * V_1 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		U3CFromAsyncPatternU3Ec__AnonStorey3F_1_t3091919594 * L_0 = (U3CFromAsyncPatternU3Ec__AnonStorey3F_1_t3091919594 *)__this->get_U3CU3Ef__refU2463_1();
		NullCheck(L_0);
		Func_2_t2571569893 * L_1 = (Func_2_t2571569893 *)L_0->get_end_1();
		Il2CppObject * L_2 = ___iar0;
		NullCheck((Func_2_t2571569893 *)L_1);
		Unit_t2558286038  L_3 = ((  Unit_t2558286038  (*) (Func_2_t2571569893 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((Func_2_t2571569893 *)L_1, (Il2CppObject *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		V_0 = (Unit_t2558286038 )L_3;
		goto IL_002e;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1967233988 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Exception_t1967233988_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_0017;
		throw e;
	}

CATCH_0017:
	{ // begin catch(System.Exception)
		{
			V_1 = (Exception_t1967233988 *)((Exception_t1967233988 *)__exception_local);
			AsyncSubject_1_t3257925142 * L_4 = (AsyncSubject_1_t3257925142 *)__this->get_subject_0();
			Exception_t1967233988 * L_5 = V_1;
			NullCheck((AsyncSubject_1_t3257925142 *)L_4);
			((  void (*) (AsyncSubject_1_t3257925142 *, Exception_t1967233988 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)((AsyncSubject_1_t3257925142 *)L_4, (Exception_t1967233988 *)L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
			goto IL_0045;
		}

IL_0029:
		{
			; // IL_0029: leave IL_002e
		}
	} // end catch (depth: 1)

IL_002e:
	{
		AsyncSubject_1_t3257925142 * L_6 = (AsyncSubject_1_t3257925142 *)__this->get_subject_0();
		Unit_t2558286038  L_7 = V_0;
		NullCheck((AsyncSubject_1_t3257925142 *)L_6);
		((  void (*) (AsyncSubject_1_t3257925142 *, Unit_t2558286038 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)((AsyncSubject_1_t3257925142 *)L_6, (Unit_t2558286038 )L_7, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		AsyncSubject_1_t3257925142 * L_8 = (AsyncSubject_1_t3257925142 *)__this->get_subject_0();
		NullCheck((AsyncSubject_1_t3257925142 *)L_8);
		((  void (*) (AsyncSubject_1_t3257925142 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)((AsyncSubject_1_t3257925142 *)L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
	}

IL_0045:
	{
		return;
	}
}
// System.Void UniRx.Observable/<FromAsyncPattern>c__AnonStorey3F`1<System.Object>::.ctor()
extern "C"  void U3CFromAsyncPatternU3Ec__AnonStorey3F_1__ctor_m994084238_gshared (U3CFromAsyncPatternU3Ec__AnonStorey3F_1_t1370739976 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// UniRx.IObservable`1<TResult> UniRx.Observable/<FromAsyncPattern>c__AnonStorey3F`1<System.Object>::<>m__40()
extern Il2CppClass* AsyncCallback_t1363551830_il2cpp_TypeInfo_var;
extern Il2CppClass* Exception_t1967233988_il2cpp_TypeInfo_var;
extern Il2CppClass* Observable_t258071701_il2cpp_TypeInfo_var;
extern const MethodInfo* Func_3_Invoke_m601836101_MethodInfo_var;
extern const uint32_t U3CFromAsyncPatternU3Ec__AnonStorey3F_1_U3CU3Em__40_m2354625300_MetadataUsageId;
extern "C"  Il2CppObject* U3CFromAsyncPatternU3Ec__AnonStorey3F_1_U3CU3Em__40_m2354625300_gshared (U3CFromAsyncPatternU3Ec__AnonStorey3F_1_t1370739976 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CFromAsyncPatternU3Ec__AnonStorey3F_1_U3CU3Em__40_m2354625300_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Exception_t1967233988 * V_0 = NULL;
	U3CFromAsyncPatternU3Ec__AnonStorey40_1_t1660482281 * V_1 = NULL;
	Il2CppObject* V_2 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		U3CFromAsyncPatternU3Ec__AnonStorey40_1_t1660482281 * L_0 = (U3CFromAsyncPatternU3Ec__AnonStorey40_1_t1660482281 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		((  void (*) (U3CFromAsyncPatternU3Ec__AnonStorey40_1_t1660482281 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		V_1 = (U3CFromAsyncPatternU3Ec__AnonStorey40_1_t1660482281 *)L_0;
		U3CFromAsyncPatternU3Ec__AnonStorey40_1_t1660482281 * L_1 = V_1;
		NullCheck(L_1);
		L_1->set_U3CU3Ef__refU2463_1(__this);
		U3CFromAsyncPatternU3Ec__AnonStorey40_1_t1660482281 * L_2 = V_1;
		AsyncSubject_1_t1536745524 * L_3 = (AsyncSubject_1_t1536745524 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		((  void (*) (AsyncSubject_1_t1536745524 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)(L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		NullCheck(L_2);
		L_2->set_subject_0(L_3);
	}

IL_0018:
	try
	{ // begin try (depth: 1)
		Func_3_t3381739440 * L_4 = (Func_3_t3381739440 *)__this->get_begin_0();
		U3CFromAsyncPatternU3Ec__AnonStorey40_1_t1660482281 * L_5 = V_1;
		IntPtr_t L_6;
		L_6.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		AsyncCallback_t1363551830 * L_7 = (AsyncCallback_t1363551830 *)il2cpp_codegen_object_new(AsyncCallback_t1363551830_il2cpp_TypeInfo_var);
		AsyncCallback__ctor_m1986959762(L_7, (Il2CppObject *)L_5, (IntPtr_t)L_6, /*hidden argument*/NULL);
		NullCheck((Func_3_t3381739440 *)L_4);
		Func_3_Invoke_m601836101((Func_3_t3381739440 *)L_4, (AsyncCallback_t1363551830 *)L_7, (Il2CppObject *)NULL, /*hidden argument*/Func_3_Invoke_m601836101_MethodInfo_var);
		goto IL_004d;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1967233988 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Exception_t1967233988_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_0036;
		throw e;
	}

CATCH_0036:
	{ // begin catch(System.Exception)
		{
			V_0 = (Exception_t1967233988 *)((Exception_t1967233988 *)__exception_local);
			Exception_t1967233988 * L_8 = V_0;
			Il2CppObject * L_9 = DefaultSchedulers_get_AsyncConversions_m4176597513(NULL /*static, unused*/, /*hidden argument*/NULL);
			IL2CPP_RUNTIME_CLASS_INIT(Observable_t258071701_il2cpp_TypeInfo_var);
			Il2CppObject* L_10 = ((  Il2CppObject* (*) (Il2CppObject * /* static, unused */, Exception_t1967233988 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->method)(NULL /*static, unused*/, (Exception_t1967233988 *)L_8, (Il2CppObject *)L_9, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
			V_2 = (Il2CppObject*)L_10;
			goto IL_0059;
		}

IL_0048:
		{
			; // IL_0048: leave IL_004d
		}
	} // end catch (depth: 1)

IL_004d:
	{
		U3CFromAsyncPatternU3Ec__AnonStorey40_1_t1660482281 * L_11 = V_1;
		NullCheck(L_11);
		AsyncSubject_1_t1536745524 * L_12 = (AsyncSubject_1_t1536745524 *)L_11->get_subject_0();
		IL2CPP_RUNTIME_CLASS_INIT(Observable_t258071701_il2cpp_TypeInfo_var);
		Il2CppObject* L_13 = ((  Il2CppObject* (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6)->method)(NULL /*static, unused*/, (Il2CppObject*)L_12, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		return L_13;
	}

IL_0059:
	{
		Il2CppObject* L_14 = V_2;
		return L_14;
	}
}
// System.Void UniRx.Observable/<FromAsyncPattern>c__AnonStorey3F`1<UniRx.Unit>::.ctor()
extern "C"  void U3CFromAsyncPatternU3Ec__AnonStorey3F_1__ctor_m3315155734_gshared (U3CFromAsyncPatternU3Ec__AnonStorey3F_1_t3091919594 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// UniRx.IObservable`1<TResult> UniRx.Observable/<FromAsyncPattern>c__AnonStorey3F`1<UniRx.Unit>::<>m__40()
extern Il2CppClass* AsyncCallback_t1363551830_il2cpp_TypeInfo_var;
extern Il2CppClass* Exception_t1967233988_il2cpp_TypeInfo_var;
extern Il2CppClass* Observable_t258071701_il2cpp_TypeInfo_var;
extern const MethodInfo* Func_3_Invoke_m601836101_MethodInfo_var;
extern const uint32_t U3CFromAsyncPatternU3Ec__AnonStorey3F_1_U3CU3Em__40_m112548026_MetadataUsageId;
extern "C"  Il2CppObject* U3CFromAsyncPatternU3Ec__AnonStorey3F_1_U3CU3Em__40_m112548026_gshared (U3CFromAsyncPatternU3Ec__AnonStorey3F_1_t3091919594 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CFromAsyncPatternU3Ec__AnonStorey3F_1_U3CU3Em__40_m112548026_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Exception_t1967233988 * V_0 = NULL;
	U3CFromAsyncPatternU3Ec__AnonStorey40_1_t3381661899 * V_1 = NULL;
	Il2CppObject* V_2 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		U3CFromAsyncPatternU3Ec__AnonStorey40_1_t3381661899 * L_0 = (U3CFromAsyncPatternU3Ec__AnonStorey40_1_t3381661899 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		((  void (*) (U3CFromAsyncPatternU3Ec__AnonStorey40_1_t3381661899 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		V_1 = (U3CFromAsyncPatternU3Ec__AnonStorey40_1_t3381661899 *)L_0;
		U3CFromAsyncPatternU3Ec__AnonStorey40_1_t3381661899 * L_1 = V_1;
		NullCheck(L_1);
		L_1->set_U3CU3Ef__refU2463_1(__this);
		U3CFromAsyncPatternU3Ec__AnonStorey40_1_t3381661899 * L_2 = V_1;
		AsyncSubject_1_t3257925142 * L_3 = (AsyncSubject_1_t3257925142 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		((  void (*) (AsyncSubject_1_t3257925142 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)(L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		NullCheck(L_2);
		L_2->set_subject_0(L_3);
	}

IL_0018:
	try
	{ // begin try (depth: 1)
		Func_3_t3381739440 * L_4 = (Func_3_t3381739440 *)__this->get_begin_0();
		U3CFromAsyncPatternU3Ec__AnonStorey40_1_t3381661899 * L_5 = V_1;
		IntPtr_t L_6;
		L_6.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		AsyncCallback_t1363551830 * L_7 = (AsyncCallback_t1363551830 *)il2cpp_codegen_object_new(AsyncCallback_t1363551830_il2cpp_TypeInfo_var);
		AsyncCallback__ctor_m1986959762(L_7, (Il2CppObject *)L_5, (IntPtr_t)L_6, /*hidden argument*/NULL);
		NullCheck((Func_3_t3381739440 *)L_4);
		Func_3_Invoke_m601836101((Func_3_t3381739440 *)L_4, (AsyncCallback_t1363551830 *)L_7, (Il2CppObject *)NULL, /*hidden argument*/Func_3_Invoke_m601836101_MethodInfo_var);
		goto IL_004d;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1967233988 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Exception_t1967233988_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_0036;
		throw e;
	}

CATCH_0036:
	{ // begin catch(System.Exception)
		{
			V_0 = (Exception_t1967233988 *)((Exception_t1967233988 *)__exception_local);
			Exception_t1967233988 * L_8 = V_0;
			Il2CppObject * L_9 = DefaultSchedulers_get_AsyncConversions_m4176597513(NULL /*static, unused*/, /*hidden argument*/NULL);
			IL2CPP_RUNTIME_CLASS_INIT(Observable_t258071701_il2cpp_TypeInfo_var);
			Il2CppObject* L_10 = ((  Il2CppObject* (*) (Il2CppObject * /* static, unused */, Exception_t1967233988 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->method)(NULL /*static, unused*/, (Exception_t1967233988 *)L_8, (Il2CppObject *)L_9, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
			V_2 = (Il2CppObject*)L_10;
			goto IL_0059;
		}

IL_0048:
		{
			; // IL_0048: leave IL_004d
		}
	} // end catch (depth: 1)

IL_004d:
	{
		U3CFromAsyncPatternU3Ec__AnonStorey40_1_t3381661899 * L_11 = V_1;
		NullCheck(L_11);
		AsyncSubject_1_t3257925142 * L_12 = (AsyncSubject_1_t3257925142 *)L_11->get_subject_0();
		IL2CPP_RUNTIME_CLASS_INIT(Observable_t258071701_il2cpp_TypeInfo_var);
		Il2CppObject* L_13 = ((  Il2CppObject* (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6)->method)(NULL /*static, unused*/, (Il2CppObject*)L_12, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		return L_13;
	}

IL_0059:
	{
		Il2CppObject* L_14 = V_2;
		return L_14;
	}
}
// System.Void UniRx.Observable/<FromAsyncPattern>c__AnonStorey41`2/<FromAsyncPattern>c__AnonStorey42`2<System.Object,System.Object>::.ctor()
extern "C"  void U3CFromAsyncPatternU3Ec__AnonStorey42_2__ctor_m3602729391_gshared (U3CFromAsyncPatternU3Ec__AnonStorey42_2_t2629939760 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UniRx.Observable/<FromAsyncPattern>c__AnonStorey41`2/<FromAsyncPattern>c__AnonStorey42`2<System.Object,System.Object>::<>m__66(System.IAsyncResult)
extern Il2CppClass* Exception_t1967233988_il2cpp_TypeInfo_var;
extern const uint32_t U3CFromAsyncPatternU3Ec__AnonStorey42_2_U3CU3Em__66_m1517210475_MetadataUsageId;
extern "C"  void U3CFromAsyncPatternU3Ec__AnonStorey42_2_U3CU3Em__66_m1517210475_gshared (U3CFromAsyncPatternU3Ec__AnonStorey42_2_t2629939760 * __this, Il2CppObject * ___iar0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CFromAsyncPatternU3Ec__AnonStorey42_2_U3CU3Em__66_m1517210475_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Il2CppObject * V_0 = NULL;
	Exception_t1967233988 * V_1 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		U3CFromAsyncPatternU3Ec__AnonStorey41_2_t1532408781 * L_0 = (U3CFromAsyncPatternU3Ec__AnonStorey41_2_t1532408781 *)__this->get_U3CU3Ef__refU2465_1();
		NullCheck(L_0);
		Func_2_t850390275 * L_1 = (Func_2_t850390275 *)L_0->get_end_1();
		Il2CppObject * L_2 = ___iar0;
		NullCheck((Func_2_t850390275 *)L_1);
		Il2CppObject * L_3 = ((  Il2CppObject * (*) (Func_2_t850390275 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((Func_2_t850390275 *)L_1, (Il2CppObject *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		V_0 = (Il2CppObject *)L_3;
		goto IL_002e;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1967233988 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Exception_t1967233988_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_0017;
		throw e;
	}

CATCH_0017:
	{ // begin catch(System.Exception)
		{
			V_1 = (Exception_t1967233988 *)((Exception_t1967233988 *)__exception_local);
			AsyncSubject_1_t1536745524 * L_4 = (AsyncSubject_1_t1536745524 *)__this->get_subject_0();
			Exception_t1967233988 * L_5 = V_1;
			NullCheck((AsyncSubject_1_t1536745524 *)L_4);
			((  void (*) (AsyncSubject_1_t1536745524 *, Exception_t1967233988 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)((AsyncSubject_1_t1536745524 *)L_4, (Exception_t1967233988 *)L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
			goto IL_0045;
		}

IL_0029:
		{
			; // IL_0029: leave IL_002e
		}
	} // end catch (depth: 1)

IL_002e:
	{
		AsyncSubject_1_t1536745524 * L_6 = (AsyncSubject_1_t1536745524 *)__this->get_subject_0();
		Il2CppObject * L_7 = V_0;
		NullCheck((AsyncSubject_1_t1536745524 *)L_6);
		((  void (*) (AsyncSubject_1_t1536745524 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)((AsyncSubject_1_t1536745524 *)L_6, (Il2CppObject *)L_7, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		AsyncSubject_1_t1536745524 * L_8 = (AsyncSubject_1_t1536745524 *)__this->get_subject_0();
		NullCheck((AsyncSubject_1_t1536745524 *)L_8);
		((  void (*) (AsyncSubject_1_t1536745524 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)((AsyncSubject_1_t1536745524 *)L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
	}

IL_0045:
	{
		return;
	}
}
// System.Void UniRx.Observable/<FromAsyncPattern>c__AnonStorey41`2/<FromAsyncPattern>c__AnonStorey42`2<System.Object,UniRx.Unit>::.ctor()
extern "C"  void U3CFromAsyncPatternU3Ec__AnonStorey42_2__ctor_m190503573_gshared (U3CFromAsyncPatternU3Ec__AnonStorey42_2_t56152082 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UniRx.Observable/<FromAsyncPattern>c__AnonStorey41`2/<FromAsyncPattern>c__AnonStorey42`2<System.Object,UniRx.Unit>::<>m__66(System.IAsyncResult)
extern Il2CppClass* Exception_t1967233988_il2cpp_TypeInfo_var;
extern const uint32_t U3CFromAsyncPatternU3Ec__AnonStorey42_2_U3CU3Em__66_m789636677_MetadataUsageId;
extern "C"  void U3CFromAsyncPatternU3Ec__AnonStorey42_2_U3CU3Em__66_m789636677_gshared (U3CFromAsyncPatternU3Ec__AnonStorey42_2_t56152082 * __this, Il2CppObject * ___iar0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CFromAsyncPatternU3Ec__AnonStorey42_2_U3CU3Em__66_m789636677_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Unit_t2558286038  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Exception_t1967233988 * V_1 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		U3CFromAsyncPatternU3Ec__AnonStorey41_2_t3253588399 * L_0 = (U3CFromAsyncPatternU3Ec__AnonStorey41_2_t3253588399 *)__this->get_U3CU3Ef__refU2465_1();
		NullCheck(L_0);
		Func_2_t2571569893 * L_1 = (Func_2_t2571569893 *)L_0->get_end_1();
		Il2CppObject * L_2 = ___iar0;
		NullCheck((Func_2_t2571569893 *)L_1);
		Unit_t2558286038  L_3 = ((  Unit_t2558286038  (*) (Func_2_t2571569893 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((Func_2_t2571569893 *)L_1, (Il2CppObject *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		V_0 = (Unit_t2558286038 )L_3;
		goto IL_002e;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1967233988 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Exception_t1967233988_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_0017;
		throw e;
	}

CATCH_0017:
	{ // begin catch(System.Exception)
		{
			V_1 = (Exception_t1967233988 *)((Exception_t1967233988 *)__exception_local);
			AsyncSubject_1_t3257925142 * L_4 = (AsyncSubject_1_t3257925142 *)__this->get_subject_0();
			Exception_t1967233988 * L_5 = V_1;
			NullCheck((AsyncSubject_1_t3257925142 *)L_4);
			((  void (*) (AsyncSubject_1_t3257925142 *, Exception_t1967233988 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)((AsyncSubject_1_t3257925142 *)L_4, (Exception_t1967233988 *)L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
			goto IL_0045;
		}

IL_0029:
		{
			; // IL_0029: leave IL_002e
		}
	} // end catch (depth: 1)

IL_002e:
	{
		AsyncSubject_1_t3257925142 * L_6 = (AsyncSubject_1_t3257925142 *)__this->get_subject_0();
		Unit_t2558286038  L_7 = V_0;
		NullCheck((AsyncSubject_1_t3257925142 *)L_6);
		((  void (*) (AsyncSubject_1_t3257925142 *, Unit_t2558286038 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)((AsyncSubject_1_t3257925142 *)L_6, (Unit_t2558286038 )L_7, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		AsyncSubject_1_t3257925142 * L_8 = (AsyncSubject_1_t3257925142 *)__this->get_subject_0();
		NullCheck((AsyncSubject_1_t3257925142 *)L_8);
		((  void (*) (AsyncSubject_1_t3257925142 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)((AsyncSubject_1_t3257925142 *)L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
	}

IL_0045:
	{
		return;
	}
}
// System.Void UniRx.Observable/<FromAsyncPattern>c__AnonStorey41`2<System.Object,System.Object>::.ctor()
extern "C"  void U3CFromAsyncPatternU3Ec__AnonStorey41_2__ctor_m4176316363_gshared (U3CFromAsyncPatternU3Ec__AnonStorey41_2_t1532408781 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// UniRx.IObservable`1<TResult> UniRx.Observable/<FromAsyncPattern>c__AnonStorey41`2<System.Object,System.Object>::<>m__41(T1)
extern Il2CppClass* AsyncCallback_t1363551830_il2cpp_TypeInfo_var;
extern Il2CppClass* Exception_t1967233988_il2cpp_TypeInfo_var;
extern Il2CppClass* Observable_t258071701_il2cpp_TypeInfo_var;
extern const uint32_t U3CFromAsyncPatternU3Ec__AnonStorey41_2_U3CU3Em__41_m3548228373_MetadataUsageId;
extern "C"  Il2CppObject* U3CFromAsyncPatternU3Ec__AnonStorey41_2_U3CU3Em__41_m3548228373_gshared (U3CFromAsyncPatternU3Ec__AnonStorey41_2_t1532408781 * __this, Il2CppObject * ___x0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CFromAsyncPatternU3Ec__AnonStorey41_2_U3CU3Em__41_m3548228373_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Exception_t1967233988 * V_0 = NULL;
	U3CFromAsyncPatternU3Ec__AnonStorey42_2_t2629939760 * V_1 = NULL;
	Il2CppObject* V_2 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		U3CFromAsyncPatternU3Ec__AnonStorey42_2_t2629939760 * L_0 = (U3CFromAsyncPatternU3Ec__AnonStorey42_2_t2629939760 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		((  void (*) (U3CFromAsyncPatternU3Ec__AnonStorey42_2_t2629939760 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		V_1 = (U3CFromAsyncPatternU3Ec__AnonStorey42_2_t2629939760 *)L_0;
		U3CFromAsyncPatternU3Ec__AnonStorey42_2_t2629939760 * L_1 = V_1;
		NullCheck(L_1);
		L_1->set_U3CU3Ef__refU2465_1(__this);
		U3CFromAsyncPatternU3Ec__AnonStorey42_2_t2629939760 * L_2 = V_1;
		AsyncSubject_1_t1536745524 * L_3 = (AsyncSubject_1_t1536745524 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		((  void (*) (AsyncSubject_1_t1536745524 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)(L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		NullCheck(L_2);
		L_2->set_subject_0(L_3);
	}

IL_0018:
	try
	{ // begin try (depth: 1)
		Func_4_t823488097 * L_4 = (Func_4_t823488097 *)__this->get_begin_0();
		Il2CppObject * L_5 = ___x0;
		U3CFromAsyncPatternU3Ec__AnonStorey42_2_t2629939760 * L_6 = V_1;
		IntPtr_t L_7;
		L_7.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		AsyncCallback_t1363551830 * L_8 = (AsyncCallback_t1363551830 *)il2cpp_codegen_object_new(AsyncCallback_t1363551830_il2cpp_TypeInfo_var);
		AsyncCallback__ctor_m1986959762(L_8, (Il2CppObject *)L_6, (IntPtr_t)L_7, /*hidden argument*/NULL);
		NullCheck((Func_4_t823488097 *)L_4);
		((  Il2CppObject * (*) (Func_4_t823488097 *, Il2CppObject *, AsyncCallback_t1363551830 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->method)((Func_4_t823488097 *)L_4, (Il2CppObject *)L_5, (AsyncCallback_t1363551830 *)L_8, (Il2CppObject *)NULL, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		goto IL_004e;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1967233988 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Exception_t1967233988_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_0037;
		throw e;
	}

CATCH_0037:
	{ // begin catch(System.Exception)
		{
			V_0 = (Exception_t1967233988 *)((Exception_t1967233988 *)__exception_local);
			Exception_t1967233988 * L_9 = V_0;
			Il2CppObject * L_10 = DefaultSchedulers_get_AsyncConversions_m4176597513(NULL /*static, unused*/, /*hidden argument*/NULL);
			IL2CPP_RUNTIME_CLASS_INIT(Observable_t258071701_il2cpp_TypeInfo_var);
			Il2CppObject* L_11 = ((  Il2CppObject* (*) (Il2CppObject * /* static, unused */, Exception_t1967233988 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6)->method)(NULL /*static, unused*/, (Exception_t1967233988 *)L_9, (Il2CppObject *)L_10, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
			V_2 = (Il2CppObject*)L_11;
			goto IL_005a;
		}

IL_0049:
		{
			; // IL_0049: leave IL_004e
		}
	} // end catch (depth: 1)

IL_004e:
	{
		U3CFromAsyncPatternU3Ec__AnonStorey42_2_t2629939760 * L_12 = V_1;
		NullCheck(L_12);
		AsyncSubject_1_t1536745524 * L_13 = (AsyncSubject_1_t1536745524 *)L_12->get_subject_0();
		IL2CPP_RUNTIME_CLASS_INIT(Observable_t258071701_il2cpp_TypeInfo_var);
		Il2CppObject* L_14 = ((  Il2CppObject* (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7)->method)(NULL /*static, unused*/, (Il2CppObject*)L_13, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7));
		return L_14;
	}

IL_005a:
	{
		Il2CppObject* L_15 = V_2;
		return L_15;
	}
}
// System.Void UniRx.Observable/<FromAsyncPattern>c__AnonStorey41`2<System.Object,UniRx.Unit>::.ctor()
extern "C"  void U3CFromAsyncPatternU3Ec__AnonStorey41_2__ctor_m2289349113_gshared (U3CFromAsyncPatternU3Ec__AnonStorey41_2_t3253588399 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// UniRx.IObservable`1<TResult> UniRx.Observable/<FromAsyncPattern>c__AnonStorey41`2<System.Object,UniRx.Unit>::<>m__41(T1)
extern Il2CppClass* AsyncCallback_t1363551830_il2cpp_TypeInfo_var;
extern Il2CppClass* Exception_t1967233988_il2cpp_TypeInfo_var;
extern Il2CppClass* Observable_t258071701_il2cpp_TypeInfo_var;
extern const uint32_t U3CFromAsyncPatternU3Ec__AnonStorey41_2_U3CU3Em__41_m2794009697_MetadataUsageId;
extern "C"  Il2CppObject* U3CFromAsyncPatternU3Ec__AnonStorey41_2_U3CU3Em__41_m2794009697_gshared (U3CFromAsyncPatternU3Ec__AnonStorey41_2_t3253588399 * __this, Il2CppObject * ___x0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CFromAsyncPatternU3Ec__AnonStorey41_2_U3CU3Em__41_m2794009697_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Exception_t1967233988 * V_0 = NULL;
	U3CFromAsyncPatternU3Ec__AnonStorey42_2_t56152082 * V_1 = NULL;
	Il2CppObject* V_2 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		U3CFromAsyncPatternU3Ec__AnonStorey42_2_t56152082 * L_0 = (U3CFromAsyncPatternU3Ec__AnonStorey42_2_t56152082 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		((  void (*) (U3CFromAsyncPatternU3Ec__AnonStorey42_2_t56152082 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		V_1 = (U3CFromAsyncPatternU3Ec__AnonStorey42_2_t56152082 *)L_0;
		U3CFromAsyncPatternU3Ec__AnonStorey42_2_t56152082 * L_1 = V_1;
		NullCheck(L_1);
		L_1->set_U3CU3Ef__refU2465_1(__this);
		U3CFromAsyncPatternU3Ec__AnonStorey42_2_t56152082 * L_2 = V_1;
		AsyncSubject_1_t3257925142 * L_3 = (AsyncSubject_1_t3257925142 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		((  void (*) (AsyncSubject_1_t3257925142 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)(L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		NullCheck(L_2);
		L_2->set_subject_0(L_3);
	}

IL_0018:
	try
	{ // begin try (depth: 1)
		Func_4_t823488097 * L_4 = (Func_4_t823488097 *)__this->get_begin_0();
		Il2CppObject * L_5 = ___x0;
		U3CFromAsyncPatternU3Ec__AnonStorey42_2_t56152082 * L_6 = V_1;
		IntPtr_t L_7;
		L_7.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		AsyncCallback_t1363551830 * L_8 = (AsyncCallback_t1363551830 *)il2cpp_codegen_object_new(AsyncCallback_t1363551830_il2cpp_TypeInfo_var);
		AsyncCallback__ctor_m1986959762(L_8, (Il2CppObject *)L_6, (IntPtr_t)L_7, /*hidden argument*/NULL);
		NullCheck((Func_4_t823488097 *)L_4);
		((  Il2CppObject * (*) (Func_4_t823488097 *, Il2CppObject *, AsyncCallback_t1363551830 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->method)((Func_4_t823488097 *)L_4, (Il2CppObject *)L_5, (AsyncCallback_t1363551830 *)L_8, (Il2CppObject *)NULL, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		goto IL_004e;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1967233988 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Exception_t1967233988_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_0037;
		throw e;
	}

CATCH_0037:
	{ // begin catch(System.Exception)
		{
			V_0 = (Exception_t1967233988 *)((Exception_t1967233988 *)__exception_local);
			Exception_t1967233988 * L_9 = V_0;
			Il2CppObject * L_10 = DefaultSchedulers_get_AsyncConversions_m4176597513(NULL /*static, unused*/, /*hidden argument*/NULL);
			IL2CPP_RUNTIME_CLASS_INIT(Observable_t258071701_il2cpp_TypeInfo_var);
			Il2CppObject* L_11 = ((  Il2CppObject* (*) (Il2CppObject * /* static, unused */, Exception_t1967233988 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6)->method)(NULL /*static, unused*/, (Exception_t1967233988 *)L_9, (Il2CppObject *)L_10, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
			V_2 = (Il2CppObject*)L_11;
			goto IL_005a;
		}

IL_0049:
		{
			; // IL_0049: leave IL_004e
		}
	} // end catch (depth: 1)

IL_004e:
	{
		U3CFromAsyncPatternU3Ec__AnonStorey42_2_t56152082 * L_12 = V_1;
		NullCheck(L_12);
		AsyncSubject_1_t3257925142 * L_13 = (AsyncSubject_1_t3257925142 *)L_12->get_subject_0();
		IL2CPP_RUNTIME_CLASS_INIT(Observable_t258071701_il2cpp_TypeInfo_var);
		Il2CppObject* L_14 = ((  Il2CppObject* (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7)->method)(NULL /*static, unused*/, (Il2CppObject*)L_13, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7));
		return L_14;
	}

IL_005a:
	{
		Il2CppObject* L_15 = V_2;
		return L_15;
	}
}
// System.Void UniRx.Observable/<FromAsyncPattern>c__AnonStorey43`3/<FromAsyncPattern>c__AnonStorey44`3<System.Object,System.Object,System.Object>::.ctor()
extern "C"  void U3CFromAsyncPatternU3Ec__AnonStorey44_3__ctor_m1812564839_gshared (U3CFromAsyncPatternU3Ec__AnonStorey44_3_t1103581271 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UniRx.Observable/<FromAsyncPattern>c__AnonStorey43`3/<FromAsyncPattern>c__AnonStorey44`3<System.Object,System.Object,System.Object>::<>m__67(System.IAsyncResult)
extern Il2CppClass* Exception_t1967233988_il2cpp_TypeInfo_var;
extern const uint32_t U3CFromAsyncPatternU3Ec__AnonStorey44_3_U3CU3Em__67_m2007566162_MetadataUsageId;
extern "C"  void U3CFromAsyncPatternU3Ec__AnonStorey44_3_U3CU3Em__67_m2007566162_gshared (U3CFromAsyncPatternU3Ec__AnonStorey44_3_t1103581271 * __this, Il2CppObject * ___iar0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CFromAsyncPatternU3Ec__AnonStorey44_3_U3CU3Em__67_m2007566162_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Il2CppObject * V_0 = NULL;
	Exception_t1967233988 * V_1 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		U3CFromAsyncPatternU3Ec__AnonStorey43_3_t1435962278 * L_0 = (U3CFromAsyncPatternU3Ec__AnonStorey43_3_t1435962278 *)__this->get_U3CU3Ef__refU2467_1();
		NullCheck(L_0);
		Func_2_t850390275 * L_1 = (Func_2_t850390275 *)L_0->get_end_1();
		Il2CppObject * L_2 = ___iar0;
		NullCheck((Func_2_t850390275 *)L_1);
		Il2CppObject * L_3 = ((  Il2CppObject * (*) (Func_2_t850390275 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((Func_2_t850390275 *)L_1, (Il2CppObject *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		V_0 = (Il2CppObject *)L_3;
		goto IL_002e;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1967233988 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Exception_t1967233988_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_0017;
		throw e;
	}

CATCH_0017:
	{ // begin catch(System.Exception)
		{
			V_1 = (Exception_t1967233988 *)((Exception_t1967233988 *)__exception_local);
			AsyncSubject_1_t1536745524 * L_4 = (AsyncSubject_1_t1536745524 *)__this->get_subject_0();
			Exception_t1967233988 * L_5 = V_1;
			NullCheck((AsyncSubject_1_t1536745524 *)L_4);
			((  void (*) (AsyncSubject_1_t1536745524 *, Exception_t1967233988 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)((AsyncSubject_1_t1536745524 *)L_4, (Exception_t1967233988 *)L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
			goto IL_0045;
		}

IL_0029:
		{
			; // IL_0029: leave IL_002e
		}
	} // end catch (depth: 1)

IL_002e:
	{
		AsyncSubject_1_t1536745524 * L_6 = (AsyncSubject_1_t1536745524 *)__this->get_subject_0();
		Il2CppObject * L_7 = V_0;
		NullCheck((AsyncSubject_1_t1536745524 *)L_6);
		((  void (*) (AsyncSubject_1_t1536745524 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)((AsyncSubject_1_t1536745524 *)L_6, (Il2CppObject *)L_7, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		AsyncSubject_1_t1536745524 * L_8 = (AsyncSubject_1_t1536745524 *)__this->get_subject_0();
		NullCheck((AsyncSubject_1_t1536745524 *)L_8);
		((  void (*) (AsyncSubject_1_t1536745524 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)((AsyncSubject_1_t1536745524 *)L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
	}

IL_0045:
	{
		return;
	}
}
// System.Void UniRx.Observable/<FromAsyncPattern>c__AnonStorey43`3/<FromAsyncPattern>c__AnonStorey44`3<System.Object,System.Object,UniRx.Unit>::.ctor()
extern "C"  void U3CFromAsyncPatternU3Ec__AnonStorey44_3__ctor_m1043641309_gshared (U3CFromAsyncPatternU3Ec__AnonStorey44_3_t2824760889 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UniRx.Observable/<FromAsyncPattern>c__AnonStorey43`3/<FromAsyncPattern>c__AnonStorey44`3<System.Object,System.Object,UniRx.Unit>::<>m__67(System.IAsyncResult)
extern Il2CppClass* Exception_t1967233988_il2cpp_TypeInfo_var;
extern const uint32_t U3CFromAsyncPatternU3Ec__AnonStorey44_3_U3CU3Em__67_m623568028_MetadataUsageId;
extern "C"  void U3CFromAsyncPatternU3Ec__AnonStorey44_3_U3CU3Em__67_m623568028_gshared (U3CFromAsyncPatternU3Ec__AnonStorey44_3_t2824760889 * __this, Il2CppObject * ___iar0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CFromAsyncPatternU3Ec__AnonStorey44_3_U3CU3Em__67_m623568028_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Unit_t2558286038  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Exception_t1967233988 * V_1 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		U3CFromAsyncPatternU3Ec__AnonStorey43_3_t3157141896 * L_0 = (U3CFromAsyncPatternU3Ec__AnonStorey43_3_t3157141896 *)__this->get_U3CU3Ef__refU2467_1();
		NullCheck(L_0);
		Func_2_t2571569893 * L_1 = (Func_2_t2571569893 *)L_0->get_end_1();
		Il2CppObject * L_2 = ___iar0;
		NullCheck((Func_2_t2571569893 *)L_1);
		Unit_t2558286038  L_3 = ((  Unit_t2558286038  (*) (Func_2_t2571569893 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((Func_2_t2571569893 *)L_1, (Il2CppObject *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		V_0 = (Unit_t2558286038 )L_3;
		goto IL_002e;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1967233988 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Exception_t1967233988_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_0017;
		throw e;
	}

CATCH_0017:
	{ // begin catch(System.Exception)
		{
			V_1 = (Exception_t1967233988 *)((Exception_t1967233988 *)__exception_local);
			AsyncSubject_1_t3257925142 * L_4 = (AsyncSubject_1_t3257925142 *)__this->get_subject_0();
			Exception_t1967233988 * L_5 = V_1;
			NullCheck((AsyncSubject_1_t3257925142 *)L_4);
			((  void (*) (AsyncSubject_1_t3257925142 *, Exception_t1967233988 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)((AsyncSubject_1_t3257925142 *)L_4, (Exception_t1967233988 *)L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
			goto IL_0045;
		}

IL_0029:
		{
			; // IL_0029: leave IL_002e
		}
	} // end catch (depth: 1)

IL_002e:
	{
		AsyncSubject_1_t3257925142 * L_6 = (AsyncSubject_1_t3257925142 *)__this->get_subject_0();
		Unit_t2558286038  L_7 = V_0;
		NullCheck((AsyncSubject_1_t3257925142 *)L_6);
		((  void (*) (AsyncSubject_1_t3257925142 *, Unit_t2558286038 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)((AsyncSubject_1_t3257925142 *)L_6, (Unit_t2558286038 )L_7, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		AsyncSubject_1_t3257925142 * L_8 = (AsyncSubject_1_t3257925142 *)__this->get_subject_0();
		NullCheck((AsyncSubject_1_t3257925142 *)L_8);
		((  void (*) (AsyncSubject_1_t3257925142 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)((AsyncSubject_1_t3257925142 *)L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
	}

IL_0045:
	{
		return;
	}
}
// System.Void UniRx.Observable/<FromAsyncPattern>c__AnonStorey43`3<System.Object,System.Object,System.Object>::.ctor()
extern "C"  void U3CFromAsyncPatternU3Ec__AnonStorey43_3__ctor_m2090340864_gshared (U3CFromAsyncPatternU3Ec__AnonStorey43_3_t1435962278 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// UniRx.IObservable`1<TResult> UniRx.Observable/<FromAsyncPattern>c__AnonStorey43`3<System.Object,System.Object,System.Object>::<>m__42(T1,T2)
extern Il2CppClass* AsyncCallback_t1363551830_il2cpp_TypeInfo_var;
extern Il2CppClass* Exception_t1967233988_il2cpp_TypeInfo_var;
extern Il2CppClass* Observable_t258071701_il2cpp_TypeInfo_var;
extern const uint32_t U3CFromAsyncPatternU3Ec__AnonStorey43_3_U3CU3Em__42_m1404897853_MetadataUsageId;
extern "C"  Il2CppObject* U3CFromAsyncPatternU3Ec__AnonStorey43_3_U3CU3Em__42_m1404897853_gshared (U3CFromAsyncPatternU3Ec__AnonStorey43_3_t1435962278 * __this, Il2CppObject * ___x0, Il2CppObject * ___y1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CFromAsyncPatternU3Ec__AnonStorey43_3_U3CU3Em__42_m1404897853_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Exception_t1967233988 * V_0 = NULL;
	U3CFromAsyncPatternU3Ec__AnonStorey44_3_t1103581271 * V_1 = NULL;
	Il2CppObject* V_2 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		U3CFromAsyncPatternU3Ec__AnonStorey44_3_t1103581271 * L_0 = (U3CFromAsyncPatternU3Ec__AnonStorey44_3_t1103581271 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		((  void (*) (U3CFromAsyncPatternU3Ec__AnonStorey44_3_t1103581271 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		V_1 = (U3CFromAsyncPatternU3Ec__AnonStorey44_3_t1103581271 *)L_0;
		U3CFromAsyncPatternU3Ec__AnonStorey44_3_t1103581271 * L_1 = V_1;
		NullCheck(L_1);
		L_1->set_U3CU3Ef__refU2467_1(__this);
		U3CFromAsyncPatternU3Ec__AnonStorey44_3_t1103581271 * L_2 = V_1;
		AsyncSubject_1_t1536745524 * L_3 = (AsyncSubject_1_t1536745524 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		((  void (*) (AsyncSubject_1_t1536745524 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)(L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		NullCheck(L_2);
		L_2->set_subject_0(L_3);
	}

IL_0018:
	try
	{ // begin try (depth: 1)
		Func_5_t2364630122 * L_4 = (Func_5_t2364630122 *)__this->get_begin_0();
		Il2CppObject * L_5 = ___x0;
		Il2CppObject * L_6 = ___y1;
		U3CFromAsyncPatternU3Ec__AnonStorey44_3_t1103581271 * L_7 = V_1;
		IntPtr_t L_8;
		L_8.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		AsyncCallback_t1363551830 * L_9 = (AsyncCallback_t1363551830 *)il2cpp_codegen_object_new(AsyncCallback_t1363551830_il2cpp_TypeInfo_var);
		AsyncCallback__ctor_m1986959762(L_9, (Il2CppObject *)L_7, (IntPtr_t)L_8, /*hidden argument*/NULL);
		NullCheck((Func_5_t2364630122 *)L_4);
		((  Il2CppObject * (*) (Func_5_t2364630122 *, Il2CppObject *, Il2CppObject *, AsyncCallback_t1363551830 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->method)((Func_5_t2364630122 *)L_4, (Il2CppObject *)L_5, (Il2CppObject *)L_6, (AsyncCallback_t1363551830 *)L_9, (Il2CppObject *)NULL, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		goto IL_004f;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1967233988 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Exception_t1967233988_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_0038;
		throw e;
	}

CATCH_0038:
	{ // begin catch(System.Exception)
		{
			V_0 = (Exception_t1967233988 *)((Exception_t1967233988 *)__exception_local);
			Exception_t1967233988 * L_10 = V_0;
			Il2CppObject * L_11 = DefaultSchedulers_get_AsyncConversions_m4176597513(NULL /*static, unused*/, /*hidden argument*/NULL);
			IL2CPP_RUNTIME_CLASS_INIT(Observable_t258071701_il2cpp_TypeInfo_var);
			Il2CppObject* L_12 = ((  Il2CppObject* (*) (Il2CppObject * /* static, unused */, Exception_t1967233988 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6)->method)(NULL /*static, unused*/, (Exception_t1967233988 *)L_10, (Il2CppObject *)L_11, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
			V_2 = (Il2CppObject*)L_12;
			goto IL_005b;
		}

IL_004a:
		{
			; // IL_004a: leave IL_004f
		}
	} // end catch (depth: 1)

IL_004f:
	{
		U3CFromAsyncPatternU3Ec__AnonStorey44_3_t1103581271 * L_13 = V_1;
		NullCheck(L_13);
		AsyncSubject_1_t1536745524 * L_14 = (AsyncSubject_1_t1536745524 *)L_13->get_subject_0();
		IL2CPP_RUNTIME_CLASS_INIT(Observable_t258071701_il2cpp_TypeInfo_var);
		Il2CppObject* L_15 = ((  Il2CppObject* (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7)->method)(NULL /*static, unused*/, (Il2CppObject*)L_14, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7));
		return L_15;
	}

IL_005b:
	{
		Il2CppObject* L_16 = V_2;
		return L_16;
	}
}
// System.Void UniRx.Observable/<FromAsyncPattern>c__AnonStorey43`3<System.Object,System.Object,UniRx.Unit>::.ctor()
extern "C"  void U3CFromAsyncPatternU3Ec__AnonStorey43_3__ctor_m2972212196_gshared (U3CFromAsyncPatternU3Ec__AnonStorey43_3_t3157141896 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// UniRx.IObservable`1<TResult> UniRx.Observable/<FromAsyncPattern>c__AnonStorey43`3<System.Object,System.Object,UniRx.Unit>::<>m__42(T1,T2)
extern Il2CppClass* AsyncCallback_t1363551830_il2cpp_TypeInfo_var;
extern Il2CppClass* Exception_t1967233988_il2cpp_TypeInfo_var;
extern Il2CppClass* Observable_t258071701_il2cpp_TypeInfo_var;
extern const uint32_t U3CFromAsyncPatternU3Ec__AnonStorey43_3_U3CU3Em__42_m4054626683_MetadataUsageId;
extern "C"  Il2CppObject* U3CFromAsyncPatternU3Ec__AnonStorey43_3_U3CU3Em__42_m4054626683_gshared (U3CFromAsyncPatternU3Ec__AnonStorey43_3_t3157141896 * __this, Il2CppObject * ___x0, Il2CppObject * ___y1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CFromAsyncPatternU3Ec__AnonStorey43_3_U3CU3Em__42_m4054626683_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Exception_t1967233988 * V_0 = NULL;
	U3CFromAsyncPatternU3Ec__AnonStorey44_3_t2824760889 * V_1 = NULL;
	Il2CppObject* V_2 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		U3CFromAsyncPatternU3Ec__AnonStorey44_3_t2824760889 * L_0 = (U3CFromAsyncPatternU3Ec__AnonStorey44_3_t2824760889 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		((  void (*) (U3CFromAsyncPatternU3Ec__AnonStorey44_3_t2824760889 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		V_1 = (U3CFromAsyncPatternU3Ec__AnonStorey44_3_t2824760889 *)L_0;
		U3CFromAsyncPatternU3Ec__AnonStorey44_3_t2824760889 * L_1 = V_1;
		NullCheck(L_1);
		L_1->set_U3CU3Ef__refU2467_1(__this);
		U3CFromAsyncPatternU3Ec__AnonStorey44_3_t2824760889 * L_2 = V_1;
		AsyncSubject_1_t3257925142 * L_3 = (AsyncSubject_1_t3257925142 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		((  void (*) (AsyncSubject_1_t3257925142 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)(L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		NullCheck(L_2);
		L_2->set_subject_0(L_3);
	}

IL_0018:
	try
	{ // begin try (depth: 1)
		Func_5_t2364630122 * L_4 = (Func_5_t2364630122 *)__this->get_begin_0();
		Il2CppObject * L_5 = ___x0;
		Il2CppObject * L_6 = ___y1;
		U3CFromAsyncPatternU3Ec__AnonStorey44_3_t2824760889 * L_7 = V_1;
		IntPtr_t L_8;
		L_8.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		AsyncCallback_t1363551830 * L_9 = (AsyncCallback_t1363551830 *)il2cpp_codegen_object_new(AsyncCallback_t1363551830_il2cpp_TypeInfo_var);
		AsyncCallback__ctor_m1986959762(L_9, (Il2CppObject *)L_7, (IntPtr_t)L_8, /*hidden argument*/NULL);
		NullCheck((Func_5_t2364630122 *)L_4);
		((  Il2CppObject * (*) (Func_5_t2364630122 *, Il2CppObject *, Il2CppObject *, AsyncCallback_t1363551830 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->method)((Func_5_t2364630122 *)L_4, (Il2CppObject *)L_5, (Il2CppObject *)L_6, (AsyncCallback_t1363551830 *)L_9, (Il2CppObject *)NULL, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		goto IL_004f;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1967233988 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Exception_t1967233988_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_0038;
		throw e;
	}

CATCH_0038:
	{ // begin catch(System.Exception)
		{
			V_0 = (Exception_t1967233988 *)((Exception_t1967233988 *)__exception_local);
			Exception_t1967233988 * L_10 = V_0;
			Il2CppObject * L_11 = DefaultSchedulers_get_AsyncConversions_m4176597513(NULL /*static, unused*/, /*hidden argument*/NULL);
			IL2CPP_RUNTIME_CLASS_INIT(Observable_t258071701_il2cpp_TypeInfo_var);
			Il2CppObject* L_12 = ((  Il2CppObject* (*) (Il2CppObject * /* static, unused */, Exception_t1967233988 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6)->method)(NULL /*static, unused*/, (Exception_t1967233988 *)L_10, (Il2CppObject *)L_11, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
			V_2 = (Il2CppObject*)L_12;
			goto IL_005b;
		}

IL_004a:
		{
			; // IL_004a: leave IL_004f
		}
	} // end catch (depth: 1)

IL_004f:
	{
		U3CFromAsyncPatternU3Ec__AnonStorey44_3_t2824760889 * L_13 = V_1;
		NullCheck(L_13);
		AsyncSubject_1_t3257925142 * L_14 = (AsyncSubject_1_t3257925142 *)L_13->get_subject_0();
		IL2CPP_RUNTIME_CLASS_INIT(Observable_t258071701_il2cpp_TypeInfo_var);
		Il2CppObject* L_15 = ((  Il2CppObject* (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7)->method)(NULL /*static, unused*/, (Il2CppObject*)L_14, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7));
		return L_15;
	}

IL_005b:
	{
		Il2CppObject* L_16 = V_2;
		return L_16;
	}
}
// System.Void UniRx.Observable/<FromAsyncPattern>c__AnonStorey46`1<System.Object>::.ctor()
extern "C"  void U3CFromAsyncPatternU3Ec__AnonStorey46_1__ctor_m2533392605_gshared (U3CFromAsyncPatternU3Ec__AnonStorey46_1_t3285299583 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// UniRx.Unit UniRx.Observable/<FromAsyncPattern>c__AnonStorey46`1<System.Object>::<>m__44(System.IAsyncResult)
extern Il2CppClass* Unit_t2558286038_il2cpp_TypeInfo_var;
extern const MethodInfo* Action_1_Invoke_m140874371_MethodInfo_var;
extern const uint32_t U3CFromAsyncPatternU3Ec__AnonStorey46_1_U3CU3Em__44_m3491607508_MetadataUsageId;
extern "C"  Unit_t2558286038  U3CFromAsyncPatternU3Ec__AnonStorey46_1_U3CU3Em__44_m3491607508_gshared (U3CFromAsyncPatternU3Ec__AnonStorey46_1_t3285299583 * __this, Il2CppObject * ___iar0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CFromAsyncPatternU3Ec__AnonStorey46_1_U3CU3Em__44_m3491607508_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Action_1_t686135974 * L_0 = (Action_1_t686135974 *)__this->get_end_0();
		Il2CppObject * L_1 = ___iar0;
		NullCheck((Action_1_t686135974 *)L_0);
		Action_1_Invoke_m140874371((Action_1_t686135974 *)L_0, (Il2CppObject *)L_1, /*hidden argument*/Action_1_Invoke_m140874371_MethodInfo_var);
		IL2CPP_RUNTIME_CLASS_INIT(Unit_t2558286038_il2cpp_TypeInfo_var);
		Unit_t2558286038  L_2 = Unit_get_Default_m3654553472(NULL /*static, unused*/, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Void UniRx.Observable/<FromAsyncPattern>c__AnonStorey47`2<System.Object,System.Object>::.ctor()
extern "C"  void U3CFromAsyncPatternU3Ec__AnonStorey47_2__ctor_m1152289233_gshared (U3CFromAsyncPatternU3Ec__AnonStorey47_2_t3822627359 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// UniRx.Unit UniRx.Observable/<FromAsyncPattern>c__AnonStorey47`2<System.Object,System.Object>::<>m__45(System.IAsyncResult)
extern Il2CppClass* Unit_t2558286038_il2cpp_TypeInfo_var;
extern const MethodInfo* Action_1_Invoke_m140874371_MethodInfo_var;
extern const uint32_t U3CFromAsyncPatternU3Ec__AnonStorey47_2_U3CU3Em__45_m630157759_MetadataUsageId;
extern "C"  Unit_t2558286038  U3CFromAsyncPatternU3Ec__AnonStorey47_2_U3CU3Em__45_m630157759_gshared (U3CFromAsyncPatternU3Ec__AnonStorey47_2_t3822627359 * __this, Il2CppObject * ___iar0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CFromAsyncPatternU3Ec__AnonStorey47_2_U3CU3Em__45_m630157759_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Action_1_t686135974 * L_0 = (Action_1_t686135974 *)__this->get_end_0();
		Il2CppObject * L_1 = ___iar0;
		NullCheck((Action_1_t686135974 *)L_0);
		Action_1_Invoke_m140874371((Action_1_t686135974 *)L_0, (Il2CppObject *)L_1, /*hidden argument*/Action_1_Invoke_m140874371_MethodInfo_var);
		IL2CPP_RUNTIME_CLASS_INIT(Unit_t2558286038_il2cpp_TypeInfo_var);
		Unit_t2558286038  L_2 = Unit_get_Default_m3654553472(NULL /*static, unused*/, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Void UniRx.Observable/<FromCoroutine>c__AnonStorey4C`1<System.Object>::.ctor()
extern "C"  void U3CFromCoroutineU3Ec__AnonStorey4C_1__ctor_m39769090_gshared (U3CFromCoroutineU3Ec__AnonStorey4C_1_t2050741468 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Collections.IEnumerator UniRx.Observable/<FromCoroutine>c__AnonStorey4C`1<System.Object>::<>m__4A(UniRx.IObserver`1<T>,UniRx.CancellationToken)
extern "C"  Il2CppObject * U3CFromCoroutineU3Ec__AnonStorey4C_1_U3CU3Em__4A_m3991795880_gshared (U3CFromCoroutineU3Ec__AnonStorey4C_1_t2050741468 * __this, Il2CppObject* ___observer0, CancellationToken_t1439151560  ____1, const MethodInfo* method)
{
	{
		Func_2_t1278048720 * L_0 = (Func_2_t1278048720 *)__this->get_coroutine_0();
		Il2CppObject* L_1 = ___observer0;
		NullCheck((Func_2_t1278048720 *)L_0);
		Il2CppObject * L_2 = ((  Il2CppObject * (*) (Func_2_t1278048720 *, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((Func_2_t1278048720 *)L_0, (Il2CppObject*)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		return L_2;
	}
}
// System.Void UniRx.Observable/<FromCoroutineValue>c__AnonStorey4B`1<System.Object>::.ctor()
extern "C"  void U3CFromCoroutineValueU3Ec__AnonStorey4B_1__ctor_m3265911342_gshared (U3CFromCoroutineValueU3Ec__AnonStorey4B_1_t3800936616 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Collections.IEnumerator UniRx.Observable/<FromCoroutineValue>c__AnonStorey4B`1<System.Object>::<>m__49(UniRx.IObserver`1<T>,UniRx.CancellationToken)
extern Il2CppClass* Observable_t258071701_il2cpp_TypeInfo_var;
extern const MethodInfo* Func_1_Invoke_m2952332176_MethodInfo_var;
extern const uint32_t U3CFromCoroutineValueU3Ec__AnonStorey4B_1_U3CU3Em__49_m3253753900_MetadataUsageId;
extern "C"  Il2CppObject * U3CFromCoroutineValueU3Ec__AnonStorey4B_1_U3CU3Em__49_m3253753900_gshared (U3CFromCoroutineValueU3Ec__AnonStorey4B_1_t3800936616 * __this, Il2CppObject* ___observer0, CancellationToken_t1439151560  ___cancellationToken1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CFromCoroutineValueU3Ec__AnonStorey4B_1_U3CU3Em__49_m3253753900_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Func_1_t1429988286 * L_0 = (Func_1_t1429988286 *)__this->get_coroutine_0();
		NullCheck((Func_1_t1429988286 *)L_0);
		Il2CppObject * L_1 = Func_1_Invoke_m2952332176((Func_1_t1429988286 *)L_0, /*hidden argument*/Func_1_Invoke_m2952332176_MethodInfo_var);
		Il2CppObject* L_2 = ___observer0;
		CancellationToken_t1439151560  L_3 = ___cancellationToken1;
		bool L_4 = (bool)__this->get_nullAsNextUpdate_1();
		IL2CPP_RUNTIME_CLASS_INIT(Observable_t258071701_il2cpp_TypeInfo_var);
		Il2CppObject * L_5 = ((  Il2CppObject * (*) (Il2CppObject * /* static, unused */, Il2CppObject *, Il2CppObject*, CancellationToken_t1439151560 , bool, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)(NULL /*static, unused*/, (Il2CppObject *)L_1, (Il2CppObject*)L_2, (CancellationToken_t1439151560 )L_3, (bool)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		return L_5;
	}
}
// System.Void UniRx.Observable/<OnErrorRetry>c__AnonStorey3D`2/<OnErrorRetry>c__AnonStorey3E`2<System.Object,System.Object>::.ctor()
extern "C"  void U3COnErrorRetryU3Ec__AnonStorey3E_2__ctor_m2508790297_gshared (U3COnErrorRetryU3Ec__AnonStorey3E_2_t1069553519 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// UniRx.IObservable`1<TSource> UniRx.Observable/<OnErrorRetry>c__AnonStorey3D`2/<OnErrorRetry>c__AnonStorey3E`2<System.Object,System.Object>::<>m__64(TException)
extern Il2CppClass* TimeSpan_t763862892_il2cpp_TypeInfo_var;
extern Il2CppClass* Scheduler_t103934925_il2cpp_TypeInfo_var;
extern Il2CppClass* Observable_t258071701_il2cpp_TypeInfo_var;
extern const uint32_t U3COnErrorRetryU3Ec__AnonStorey3E_2_U3CU3Em__64_m530446180_MetadataUsageId;
extern "C"  Il2CppObject* U3COnErrorRetryU3Ec__AnonStorey3E_2_U3CU3Em__64_m530446180_gshared (U3COnErrorRetryU3Ec__AnonStorey3E_2_t1069553519 * __this, Il2CppObject * ___ex0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3COnErrorRetryU3Ec__AnonStorey3E_2_U3CU3Em__64_m530446180_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	Il2CppObject* V_1 = NULL;
	Il2CppObject* G_B4_0 = NULL;
	Il2CppObject* G_B6_0 = NULL;
	{
		U3COnErrorRetryU3Ec__AnonStorey3D_2_t4266989836 * L_0 = (U3COnErrorRetryU3Ec__AnonStorey3D_2_t4266989836 *)__this->get_U3CU3Ef__refU2461_3();
		NullCheck(L_0);
		Action_1_t985559125 * L_1 = (Action_1_t985559125 *)L_0->get_onError_2();
		Il2CppObject * L_2 = ___ex0;
		NullCheck((Action_1_t985559125 *)L_1);
		((  void (*) (Action_1_t985559125 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((Action_1_t985559125 *)L_1, (Il2CppObject *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_3 = (int32_t)__this->get_count_0();
		int32_t L_4 = (int32_t)((int32_t)((int32_t)L_3+(int32_t)1));
		V_0 = (int32_t)L_4;
		__this->set_count_0(L_4);
		int32_t L_5 = V_0;
		U3COnErrorRetryU3Ec__AnonStorey3D_2_t4266989836 * L_6 = (U3COnErrorRetryU3Ec__AnonStorey3D_2_t4266989836 *)__this->get_U3CU3Ef__refU2461_3();
		NullCheck(L_6);
		int32_t L_7 = (int32_t)L_6->get_retryCount_3();
		if ((((int32_t)L_5) >= ((int32_t)L_7)))
		{
			goto IL_008b;
		}
	}
	{
		TimeSpan_t763862892  L_8 = (TimeSpan_t763862892 )__this->get_dueTime_1();
		IL2CPP_RUNTIME_CLASS_INIT(TimeSpan_t763862892_il2cpp_TypeInfo_var);
		TimeSpan_t763862892  L_9 = ((TimeSpan_t763862892_StaticFields*)TimeSpan_t763862892_il2cpp_TypeInfo_var->static_fields)->get_Zero_7();
		bool L_10 = TimeSpan_op_Equality_m2213378780(NULL /*static, unused*/, (TimeSpan_t763862892 )L_8, (TimeSpan_t763862892 )L_9, /*hidden argument*/NULL);
		if (!L_10)
		{
			goto IL_005e;
		}
	}
	{
		Il2CppObject* L_11 = (Il2CppObject*)__this->get_self_2();
		IL2CPP_RUNTIME_CLASS_INIT(Scheduler_t103934925_il2cpp_TypeInfo_var);
		Il2CppObject * L_12 = ((Scheduler_t103934925_StaticFields*)Scheduler_t103934925_il2cpp_TypeInfo_var->static_fields)->get_CurrentThread_0();
		IL2CPP_RUNTIME_CLASS_INIT(Observable_t258071701_il2cpp_TypeInfo_var);
		Il2CppObject* L_13 = ((  Il2CppObject* (*) (Il2CppObject * /* static, unused */, Il2CppObject*, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)(NULL /*static, unused*/, (Il2CppObject*)L_11, (Il2CppObject *)L_12, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		V_1 = (Il2CppObject*)L_13;
		Il2CppObject* L_14 = V_1;
		G_B4_0 = L_14;
		goto IL_0084;
	}

IL_005e:
	{
		Il2CppObject* L_15 = (Il2CppObject*)__this->get_self_2();
		TimeSpan_t763862892  L_16 = (TimeSpan_t763862892 )__this->get_dueTime_1();
		U3COnErrorRetryU3Ec__AnonStorey3D_2_t4266989836 * L_17 = (U3COnErrorRetryU3Ec__AnonStorey3D_2_t4266989836 *)__this->get_U3CU3Ef__refU2461_3();
		NullCheck(L_17);
		Il2CppObject * L_18 = (Il2CppObject *)L_17->get_delayScheduler_4();
		IL2CPP_RUNTIME_CLASS_INIT(Observable_t258071701_il2cpp_TypeInfo_var);
		Il2CppObject* L_19 = ((  Il2CppObject* (*) (Il2CppObject * /* static, unused */, Il2CppObject*, TimeSpan_t763862892 , Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)(NULL /*static, unused*/, (Il2CppObject*)L_15, (TimeSpan_t763862892 )L_16, (Il2CppObject *)L_18, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		IL2CPP_RUNTIME_CLASS_INIT(Scheduler_t103934925_il2cpp_TypeInfo_var);
		Il2CppObject * L_20 = ((Scheduler_t103934925_StaticFields*)Scheduler_t103934925_il2cpp_TypeInfo_var->static_fields)->get_CurrentThread_0();
		Il2CppObject* L_21 = ((  Il2CppObject* (*) (Il2CppObject * /* static, unused */, Il2CppObject*, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)(NULL /*static, unused*/, (Il2CppObject*)L_19, (Il2CppObject *)L_20, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		G_B4_0 = L_21;
	}

IL_0084:
	{
		V_1 = (Il2CppObject*)G_B4_0;
		Il2CppObject* L_22 = V_1;
		G_B6_0 = L_22;
		goto IL_0096;
	}

IL_008b:
	{
		Il2CppObject * L_23 = ___ex0;
		IL2CPP_RUNTIME_CLASS_INIT(Observable_t258071701_il2cpp_TypeInfo_var);
		Il2CppObject* L_24 = ((  Il2CppObject* (*) (Il2CppObject * /* static, unused */, Exception_t1967233988 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)(NULL /*static, unused*/, (Exception_t1967233988 *)L_23, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		G_B6_0 = L_24;
	}

IL_0096:
	{
		return G_B6_0;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
