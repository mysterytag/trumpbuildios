﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// AndSubstance
struct AndSubstance_t4061782393;
// SubstanceBase`2/Intrusion<System.Boolean,System.Boolean>
struct Intrusion_t1917188776;

#include "codegen/il2cpp-codegen.h"

// System.Void AndSubstance::.ctor()
extern "C"  void AndSubstance__ctor_m3841042754 (AndSubstance_t4061782393 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean AndSubstance::Result()
extern "C"  bool AndSubstance_Result_m1168346131 (AndSubstance_t4061782393 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean AndSubstance::<Result>m__1DD(SubstanceBase`2/Intrusion<System.Boolean,System.Boolean>)
extern "C"  bool AndSubstance_U3CResultU3Em__1DD_m462089180 (Il2CppObject * __this /* static, unused */, Intrusion_t1917188776 * ___processor0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
