﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.PlayFabClientAPI/<LoginWithPlayFab>c__AnonStorey67
struct U3CLoginWithPlayFabU3Ec__AnonStorey67_t2873555786;
// PlayFab.CallRequestContainer
struct CallRequestContainer_t432318609;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharpU2Dfirstpass_PlayFab_CallRequestCo432318609.h"

// System.Void PlayFab.PlayFabClientAPI/<LoginWithPlayFab>c__AnonStorey67::.ctor()
extern "C"  void U3CLoginWithPlayFabU3Ec__AnonStorey67__ctor_m4273234857 (U3CLoginWithPlayFabU3Ec__AnonStorey67_t2873555786 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/<LoginWithPlayFab>c__AnonStorey67::<>m__54(PlayFab.CallRequestContainer)
extern "C"  void U3CLoginWithPlayFabU3Ec__AnonStorey67_U3CU3Em__54_m2773633830 (U3CLoginWithPlayFabU3Ec__AnonStorey67_t2873555786 * __this, CallRequestContainer_t432318609 * ___requestContainer0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
