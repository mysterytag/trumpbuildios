﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Zenject.SingletonInstanceHelper
struct SingletonInstanceHelper_t833281251;
// System.Collections.Generic.List`1<Zenject.InitializableManager/InitializableInfo>
struct List_1_t332623990;
// System.Func`2<ModestTree.Util.Tuple`2<System.Type,System.Int32>,System.Int32>
struct Func_2_t1968156484;
// System.Func`2<Zenject.IInitializable,System.Type>
struct Func_2_t2292481712;
// System.Func`2<Zenject.InitializableManager/InitializableInfo,System.Int32>
struct Func_2_t4238809594;
// System.Func`2<Zenject.InitializableManager/InitializableInfo,Zenject.IInitializable>
struct Func_2_t2009286642;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.InitializableManager
struct  InitializableManager_t1859035635  : public Il2CppObject
{
public:
	// Zenject.SingletonInstanceHelper Zenject.InitializableManager::_singletonInstanceHelper
	SingletonInstanceHelper_t833281251 * ____singletonInstanceHelper_0;
	// System.Collections.Generic.List`1<Zenject.InitializableManager/InitializableInfo> Zenject.InitializableManager::_initializables
	List_1_t332623990 * ____initializables_1;

public:
	inline static int32_t get_offset_of__singletonInstanceHelper_0() { return static_cast<int32_t>(offsetof(InitializableManager_t1859035635, ____singletonInstanceHelper_0)); }
	inline SingletonInstanceHelper_t833281251 * get__singletonInstanceHelper_0() const { return ____singletonInstanceHelper_0; }
	inline SingletonInstanceHelper_t833281251 ** get_address_of__singletonInstanceHelper_0() { return &____singletonInstanceHelper_0; }
	inline void set__singletonInstanceHelper_0(SingletonInstanceHelper_t833281251 * value)
	{
		____singletonInstanceHelper_0 = value;
		Il2CppCodeGenWriteBarrier(&____singletonInstanceHelper_0, value);
	}

	inline static int32_t get_offset_of__initializables_1() { return static_cast<int32_t>(offsetof(InitializableManager_t1859035635, ____initializables_1)); }
	inline List_1_t332623990 * get__initializables_1() const { return ____initializables_1; }
	inline List_1_t332623990 ** get_address_of__initializables_1() { return &____initializables_1; }
	inline void set__initializables_1(List_1_t332623990 * value)
	{
		____initializables_1 = value;
		Il2CppCodeGenWriteBarrier(&____initializables_1, value);
	}
};

struct InitializableManager_t1859035635_StaticFields
{
public:
	// System.Func`2<ModestTree.Util.Tuple`2<System.Type,System.Int32>,System.Int32> Zenject.InitializableManager::<>f__am$cache2
	Func_2_t1968156484 * ___U3CU3Ef__amU24cache2_2;
	// System.Func`2<Zenject.IInitializable,System.Type> Zenject.InitializableManager::<>f__am$cache3
	Func_2_t2292481712 * ___U3CU3Ef__amU24cache3_3;
	// System.Func`2<Zenject.InitializableManager/InitializableInfo,System.Int32> Zenject.InitializableManager::<>f__am$cache4
	Func_2_t4238809594 * ___U3CU3Ef__amU24cache4_4;
	// System.Func`2<Zenject.InitializableManager/InitializableInfo,Zenject.IInitializable> Zenject.InitializableManager::<>f__am$cache5
	Func_2_t2009286642 * ___U3CU3Ef__amU24cache5_5;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache2_2() { return static_cast<int32_t>(offsetof(InitializableManager_t1859035635_StaticFields, ___U3CU3Ef__amU24cache2_2)); }
	inline Func_2_t1968156484 * get_U3CU3Ef__amU24cache2_2() const { return ___U3CU3Ef__amU24cache2_2; }
	inline Func_2_t1968156484 ** get_address_of_U3CU3Ef__amU24cache2_2() { return &___U3CU3Ef__amU24cache2_2; }
	inline void set_U3CU3Ef__amU24cache2_2(Func_2_t1968156484 * value)
	{
		___U3CU3Ef__amU24cache2_2 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache2_2, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache3_3() { return static_cast<int32_t>(offsetof(InitializableManager_t1859035635_StaticFields, ___U3CU3Ef__amU24cache3_3)); }
	inline Func_2_t2292481712 * get_U3CU3Ef__amU24cache3_3() const { return ___U3CU3Ef__amU24cache3_3; }
	inline Func_2_t2292481712 ** get_address_of_U3CU3Ef__amU24cache3_3() { return &___U3CU3Ef__amU24cache3_3; }
	inline void set_U3CU3Ef__amU24cache3_3(Func_2_t2292481712 * value)
	{
		___U3CU3Ef__amU24cache3_3 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache3_3, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache4_4() { return static_cast<int32_t>(offsetof(InitializableManager_t1859035635_StaticFields, ___U3CU3Ef__amU24cache4_4)); }
	inline Func_2_t4238809594 * get_U3CU3Ef__amU24cache4_4() const { return ___U3CU3Ef__amU24cache4_4; }
	inline Func_2_t4238809594 ** get_address_of_U3CU3Ef__amU24cache4_4() { return &___U3CU3Ef__amU24cache4_4; }
	inline void set_U3CU3Ef__amU24cache4_4(Func_2_t4238809594 * value)
	{
		___U3CU3Ef__amU24cache4_4 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache4_4, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache5_5() { return static_cast<int32_t>(offsetof(InitializableManager_t1859035635_StaticFields, ___U3CU3Ef__amU24cache5_5)); }
	inline Func_2_t2009286642 * get_U3CU3Ef__amU24cache5_5() const { return ___U3CU3Ef__amU24cache5_5; }
	inline Func_2_t2009286642 ** get_address_of_U3CU3Ef__amU24cache5_5() { return &___U3CU3Ef__amU24cache5_5; }
	inline void set_U3CU3Ef__amU24cache5_5(Func_2_t2009286642 * value)
	{
		___U3CU3Ef__amU24cache5_5 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache5_5, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
