﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UniRx.IScheduler
struct IScheduler_t2938318244;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Scheduler/DefaultSchedulers
struct  DefaultSchedulers_t3564383257  : public Il2CppObject
{
public:

public:
};

struct DefaultSchedulers_t3564383257_StaticFields
{
public:
	// UniRx.IScheduler UniRx.Scheduler/DefaultSchedulers::constantTime
	Il2CppObject * ___constantTime_0;
	// UniRx.IScheduler UniRx.Scheduler/DefaultSchedulers::tailRecursion
	Il2CppObject * ___tailRecursion_1;
	// UniRx.IScheduler UniRx.Scheduler/DefaultSchedulers::iteration
	Il2CppObject * ___iteration_2;
	// UniRx.IScheduler UniRx.Scheduler/DefaultSchedulers::timeBasedOperations
	Il2CppObject * ___timeBasedOperations_3;
	// UniRx.IScheduler UniRx.Scheduler/DefaultSchedulers::asyncConversions
	Il2CppObject * ___asyncConversions_4;

public:
	inline static int32_t get_offset_of_constantTime_0() { return static_cast<int32_t>(offsetof(DefaultSchedulers_t3564383257_StaticFields, ___constantTime_0)); }
	inline Il2CppObject * get_constantTime_0() const { return ___constantTime_0; }
	inline Il2CppObject ** get_address_of_constantTime_0() { return &___constantTime_0; }
	inline void set_constantTime_0(Il2CppObject * value)
	{
		___constantTime_0 = value;
		Il2CppCodeGenWriteBarrier(&___constantTime_0, value);
	}

	inline static int32_t get_offset_of_tailRecursion_1() { return static_cast<int32_t>(offsetof(DefaultSchedulers_t3564383257_StaticFields, ___tailRecursion_1)); }
	inline Il2CppObject * get_tailRecursion_1() const { return ___tailRecursion_1; }
	inline Il2CppObject ** get_address_of_tailRecursion_1() { return &___tailRecursion_1; }
	inline void set_tailRecursion_1(Il2CppObject * value)
	{
		___tailRecursion_1 = value;
		Il2CppCodeGenWriteBarrier(&___tailRecursion_1, value);
	}

	inline static int32_t get_offset_of_iteration_2() { return static_cast<int32_t>(offsetof(DefaultSchedulers_t3564383257_StaticFields, ___iteration_2)); }
	inline Il2CppObject * get_iteration_2() const { return ___iteration_2; }
	inline Il2CppObject ** get_address_of_iteration_2() { return &___iteration_2; }
	inline void set_iteration_2(Il2CppObject * value)
	{
		___iteration_2 = value;
		Il2CppCodeGenWriteBarrier(&___iteration_2, value);
	}

	inline static int32_t get_offset_of_timeBasedOperations_3() { return static_cast<int32_t>(offsetof(DefaultSchedulers_t3564383257_StaticFields, ___timeBasedOperations_3)); }
	inline Il2CppObject * get_timeBasedOperations_3() const { return ___timeBasedOperations_3; }
	inline Il2CppObject ** get_address_of_timeBasedOperations_3() { return &___timeBasedOperations_3; }
	inline void set_timeBasedOperations_3(Il2CppObject * value)
	{
		___timeBasedOperations_3 = value;
		Il2CppCodeGenWriteBarrier(&___timeBasedOperations_3, value);
	}

	inline static int32_t get_offset_of_asyncConversions_4() { return static_cast<int32_t>(offsetof(DefaultSchedulers_t3564383257_StaticFields, ___asyncConversions_4)); }
	inline Il2CppObject * get_asyncConversions_4() const { return ___asyncConversions_4; }
	inline Il2CppObject ** get_address_of_asyncConversions_4() { return &___asyncConversions_4; }
	inline void set_asyncConversions_4(Il2CppObject * value)
	{
		___asyncConversions_4 = value;
		Il2CppCodeGenWriteBarrier(&___asyncConversions_4, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
