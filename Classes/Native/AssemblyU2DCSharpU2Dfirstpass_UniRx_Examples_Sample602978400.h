﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UniRx.Diagnostics.Logger
struct Logger_t2118475020;
// System.Func`2<UniRx.Diagnostics.LogEntry,System.Boolean>
struct Func_2_t1476384563;
// System.Action`1<UniRx.Diagnostics.LogEntry>
struct Action_1_t2039608427;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Examples.Sample11_Logger
struct  Sample11_Logger_t602978400  : public Il2CppObject
{
public:

public:
};

struct Sample11_Logger_t602978400_StaticFields
{
public:
	// UniRx.Diagnostics.Logger UniRx.Examples.Sample11_Logger::logger
	Logger_t2118475020 * ___logger_0;
	// System.Func`2<UniRx.Diagnostics.LogEntry,System.Boolean> UniRx.Examples.Sample11_Logger::<>f__am$cache1
	Func_2_t1476384563 * ___U3CU3Ef__amU24cache1_1;
	// System.Action`1<UniRx.Diagnostics.LogEntry> UniRx.Examples.Sample11_Logger::<>f__am$cache2
	Action_1_t2039608427 * ___U3CU3Ef__amU24cache2_2;

public:
	inline static int32_t get_offset_of_logger_0() { return static_cast<int32_t>(offsetof(Sample11_Logger_t602978400_StaticFields, ___logger_0)); }
	inline Logger_t2118475020 * get_logger_0() const { return ___logger_0; }
	inline Logger_t2118475020 ** get_address_of_logger_0() { return &___logger_0; }
	inline void set_logger_0(Logger_t2118475020 * value)
	{
		___logger_0 = value;
		Il2CppCodeGenWriteBarrier(&___logger_0, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache1_1() { return static_cast<int32_t>(offsetof(Sample11_Logger_t602978400_StaticFields, ___U3CU3Ef__amU24cache1_1)); }
	inline Func_2_t1476384563 * get_U3CU3Ef__amU24cache1_1() const { return ___U3CU3Ef__amU24cache1_1; }
	inline Func_2_t1476384563 ** get_address_of_U3CU3Ef__amU24cache1_1() { return &___U3CU3Ef__amU24cache1_1; }
	inline void set_U3CU3Ef__amU24cache1_1(Func_2_t1476384563 * value)
	{
		___U3CU3Ef__amU24cache1_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache1_1, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache2_2() { return static_cast<int32_t>(offsetof(Sample11_Logger_t602978400_StaticFields, ___U3CU3Ef__amU24cache2_2)); }
	inline Action_1_t2039608427 * get_U3CU3Ef__amU24cache2_2() const { return ___U3CU3Ef__amU24cache2_2; }
	inline Action_1_t2039608427 ** get_address_of_U3CU3Ef__amU24cache2_2() { return &___U3CU3Ef__amU24cache2_2; }
	inline void set_U3CU3Ef__amU24cache2_2(Action_1_t2039608427 * value)
	{
		___U3CU3Ef__amU24cache2_2 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache2_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
