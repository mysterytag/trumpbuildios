﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Triggers.ObservableCanvasGroupChangedTrigger
struct ObservableCanvasGroupChangedTrigger_t2452559593;
// UniRx.IObservable`1<UniRx.Unit>
struct IObservable_1_t2317084402;

#include "codegen/il2cpp-codegen.h"

// System.Void UniRx.Triggers.ObservableCanvasGroupChangedTrigger::.ctor()
extern "C"  void ObservableCanvasGroupChangedTrigger__ctor_m1774048162 (ObservableCanvasGroupChangedTrigger_t2452559593 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.Triggers.ObservableCanvasGroupChangedTrigger::OnCanvasGroupChanged()
extern "C"  void ObservableCanvasGroupChangedTrigger_OnCanvasGroupChanged_m2885758286 (ObservableCanvasGroupChangedTrigger_t2452559593 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UniRx.IObservable`1<UniRx.Unit> UniRx.Triggers.ObservableCanvasGroupChangedTrigger::OnCanvasGroupChangedAsObservable()
extern "C"  Il2CppObject* ObservableCanvasGroupChangedTrigger_OnCanvasGroupChangedAsObservable_m391776651 (ObservableCanvasGroupChangedTrigger_t2452559593 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.Triggers.ObservableCanvasGroupChangedTrigger::RaiseOnCompletedOnDestroy()
extern "C"  void ObservableCanvasGroupChangedTrigger_RaiseOnCompletedOnDestroy_m1383499227 (ObservableCanvasGroupChangedTrigger_t2452559593 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
