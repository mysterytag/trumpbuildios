﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.SelectObservable`2<System.Int64,System.Int32>
struct SelectObservable_2_t3327812218;
// UniRx.IObservable`1<System.Int64>
struct IObservable_1_t2606213246;
// System.Func`2<System.Int64,System.Int32>
struct Func_2_t592778721;
// System.Func`3<System.Int64,System.Int32,System.Int32>
struct Func_3_t4248825967;
// System.IDisposable
struct IDisposable_t1628921374;
// UniRx.IObserver`1<System.Int32>
struct IObserver_1_t764446394;

#include "codegen/il2cpp-codegen.h"

// System.Void UniRx.Operators.SelectObservable`2<System.Int64,System.Int32>::.ctor(UniRx.IObservable`1<T>,System.Func`2<T,TR>)
extern "C"  void SelectObservable_2__ctor_m3445760380_gshared (SelectObservable_2_t3327812218 * __this, Il2CppObject* ___source0, Func_2_t592778721 * ___selector1, const MethodInfo* method);
#define SelectObservable_2__ctor_m3445760380(__this, ___source0, ___selector1, method) ((  void (*) (SelectObservable_2_t3327812218 *, Il2CppObject*, Func_2_t592778721 *, const MethodInfo*))SelectObservable_2__ctor_m3445760380_gshared)(__this, ___source0, ___selector1, method)
// System.Void UniRx.Operators.SelectObservable`2<System.Int64,System.Int32>::.ctor(UniRx.IObservable`1<T>,System.Func`3<T,System.Int32,TR>)
extern "C"  void SelectObservable_2__ctor_m3546943924_gshared (SelectObservable_2_t3327812218 * __this, Il2CppObject* ___source0, Func_3_t4248825967 * ___selector1, const MethodInfo* method);
#define SelectObservable_2__ctor_m3546943924(__this, ___source0, ___selector1, method) ((  void (*) (SelectObservable_2_t3327812218 *, Il2CppObject*, Func_3_t4248825967 *, const MethodInfo*))SelectObservable_2__ctor_m3546943924_gshared)(__this, ___source0, ___selector1, method)
// System.IDisposable UniRx.Operators.SelectObservable`2<System.Int64,System.Int32>::SubscribeCore(UniRx.IObserver`1<TR>,System.IDisposable)
extern "C"  Il2CppObject * SelectObservable_2_SubscribeCore_m564403482_gshared (SelectObservable_2_t3327812218 * __this, Il2CppObject* ___observer0, Il2CppObject * ___cancel1, const MethodInfo* method);
#define SelectObservable_2_SubscribeCore_m564403482(__this, ___observer0, ___cancel1, method) ((  Il2CppObject * (*) (SelectObservable_2_t3327812218 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))SelectObservable_2_SubscribeCore_m564403482_gshared)(__this, ___observer0, ___cancel1, method)
