﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Cell`1<System.Boolean>
struct Cell_1_t393102973;
// System.Collections.Generic.List`1<CoroMutex/LockProc>
struct List_1_t2770818106;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CoroMutex
struct  CoroMutex_t686805814  : public Il2CppObject
{
public:
	// Cell`1<System.Boolean> CoroMutex::locked
	Cell_1_t393102973 * ___locked_0;
	// System.Collections.Generic.List`1<CoroMutex/LockProc> CoroMutex::locks
	List_1_t2770818106 * ___locks_1;

public:
	inline static int32_t get_offset_of_locked_0() { return static_cast<int32_t>(offsetof(CoroMutex_t686805814, ___locked_0)); }
	inline Cell_1_t393102973 * get_locked_0() const { return ___locked_0; }
	inline Cell_1_t393102973 ** get_address_of_locked_0() { return &___locked_0; }
	inline void set_locked_0(Cell_1_t393102973 * value)
	{
		___locked_0 = value;
		Il2CppCodeGenWriteBarrier(&___locked_0, value);
	}

	inline static int32_t get_offset_of_locks_1() { return static_cast<int32_t>(offsetof(CoroMutex_t686805814, ___locks_1)); }
	inline List_1_t2770818106 * get_locks_1() const { return ___locks_1; }
	inline List_1_t2770818106 ** get_address_of_locks_1() { return &___locks_1; }
	inline void set_locks_1(List_1_t2770818106 * value)
	{
		___locks_1 = value;
		Il2CppCodeGenWriteBarrier(&___locks_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
