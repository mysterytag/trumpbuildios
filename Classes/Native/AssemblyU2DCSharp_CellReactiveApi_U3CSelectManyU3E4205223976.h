﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Func`2<System.Int32,ICell`1<System.Int32>>
struct Func_2_t3201214749;
// System.Func`3<System.Int32,System.Int32,System.Int32>
struct Func_3_t543102568;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CellReactiveApi/<SelectMany>c__AnonStorey119`3<System.Int32,System.Int32,System.Int32>
struct  U3CSelectManyU3Ec__AnonStorey119_3_t4205223976  : public Il2CppObject
{
public:
	// System.Func`2<T,ICell`1<TC>> CellReactiveApi/<SelectMany>c__AnonStorey119`3::collectionSelector
	Func_2_t3201214749 * ___collectionSelector_0;
	// System.Func`3<T,TC,TR> CellReactiveApi/<SelectMany>c__AnonStorey119`3::resultSelector
	Func_3_t543102568 * ___resultSelector_1;

public:
	inline static int32_t get_offset_of_collectionSelector_0() { return static_cast<int32_t>(offsetof(U3CSelectManyU3Ec__AnonStorey119_3_t4205223976, ___collectionSelector_0)); }
	inline Func_2_t3201214749 * get_collectionSelector_0() const { return ___collectionSelector_0; }
	inline Func_2_t3201214749 ** get_address_of_collectionSelector_0() { return &___collectionSelector_0; }
	inline void set_collectionSelector_0(Func_2_t3201214749 * value)
	{
		___collectionSelector_0 = value;
		Il2CppCodeGenWriteBarrier(&___collectionSelector_0, value);
	}

	inline static int32_t get_offset_of_resultSelector_1() { return static_cast<int32_t>(offsetof(U3CSelectManyU3Ec__AnonStorey119_3_t4205223976, ___resultSelector_1)); }
	inline Func_3_t543102568 * get_resultSelector_1() const { return ___resultSelector_1; }
	inline Func_3_t543102568 ** get_address_of_resultSelector_1() { return &___resultSelector_1; }
	inline void set_resultSelector_1(Func_3_t543102568 * value)
	{
		___resultSelector_1 = value;
		Il2CppCodeGenWriteBarrier(&___resultSelector_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
