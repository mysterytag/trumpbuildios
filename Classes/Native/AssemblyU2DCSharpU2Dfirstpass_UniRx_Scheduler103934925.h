﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UniRx.IScheduler
struct IScheduler_t2938318244;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Scheduler
struct  Scheduler_t103934925  : public Il2CppObject
{
public:

public:
};

struct Scheduler_t103934925_StaticFields
{
public:
	// UniRx.IScheduler UniRx.Scheduler::CurrentThread
	Il2CppObject * ___CurrentThread_0;
	// UniRx.IScheduler UniRx.Scheduler::Immediate
	Il2CppObject * ___Immediate_1;
	// UniRx.IScheduler UniRx.Scheduler::ThreadPool
	Il2CppObject * ___ThreadPool_2;
	// UniRx.IScheduler UniRx.Scheduler::mainThread
	Il2CppObject * ___mainThread_3;
	// UniRx.IScheduler UniRx.Scheduler::mainThreadIgnoreTimeScale
	Il2CppObject * ___mainThreadIgnoreTimeScale_4;

public:
	inline static int32_t get_offset_of_CurrentThread_0() { return static_cast<int32_t>(offsetof(Scheduler_t103934925_StaticFields, ___CurrentThread_0)); }
	inline Il2CppObject * get_CurrentThread_0() const { return ___CurrentThread_0; }
	inline Il2CppObject ** get_address_of_CurrentThread_0() { return &___CurrentThread_0; }
	inline void set_CurrentThread_0(Il2CppObject * value)
	{
		___CurrentThread_0 = value;
		Il2CppCodeGenWriteBarrier(&___CurrentThread_0, value);
	}

	inline static int32_t get_offset_of_Immediate_1() { return static_cast<int32_t>(offsetof(Scheduler_t103934925_StaticFields, ___Immediate_1)); }
	inline Il2CppObject * get_Immediate_1() const { return ___Immediate_1; }
	inline Il2CppObject ** get_address_of_Immediate_1() { return &___Immediate_1; }
	inline void set_Immediate_1(Il2CppObject * value)
	{
		___Immediate_1 = value;
		Il2CppCodeGenWriteBarrier(&___Immediate_1, value);
	}

	inline static int32_t get_offset_of_ThreadPool_2() { return static_cast<int32_t>(offsetof(Scheduler_t103934925_StaticFields, ___ThreadPool_2)); }
	inline Il2CppObject * get_ThreadPool_2() const { return ___ThreadPool_2; }
	inline Il2CppObject ** get_address_of_ThreadPool_2() { return &___ThreadPool_2; }
	inline void set_ThreadPool_2(Il2CppObject * value)
	{
		___ThreadPool_2 = value;
		Il2CppCodeGenWriteBarrier(&___ThreadPool_2, value);
	}

	inline static int32_t get_offset_of_mainThread_3() { return static_cast<int32_t>(offsetof(Scheduler_t103934925_StaticFields, ___mainThread_3)); }
	inline Il2CppObject * get_mainThread_3() const { return ___mainThread_3; }
	inline Il2CppObject ** get_address_of_mainThread_3() { return &___mainThread_3; }
	inline void set_mainThread_3(Il2CppObject * value)
	{
		___mainThread_3 = value;
		Il2CppCodeGenWriteBarrier(&___mainThread_3, value);
	}

	inline static int32_t get_offset_of_mainThreadIgnoreTimeScale_4() { return static_cast<int32_t>(offsetof(Scheduler_t103934925_StaticFields, ___mainThreadIgnoreTimeScale_4)); }
	inline Il2CppObject * get_mainThreadIgnoreTimeScale_4() const { return ___mainThreadIgnoreTimeScale_4; }
	inline Il2CppObject ** get_address_of_mainThreadIgnoreTimeScale_4() { return &___mainThreadIgnoreTimeScale_4; }
	inline void set_mainThreadIgnoreTimeScale_4(Il2CppObject * value)
	{
		___mainThreadIgnoreTimeScale_4 = value;
		Il2CppCodeGenWriteBarrier(&___mainThreadIgnoreTimeScale_4, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
