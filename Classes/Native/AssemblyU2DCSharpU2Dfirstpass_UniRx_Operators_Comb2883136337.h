﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UniRx.IObservable`1<System.Boolean>[]
struct IObservable_1U5BU5D_t1703862084;

#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Oper1441642626.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Operators.CombineLatestObservable`1<System.Boolean>
struct  CombineLatestObservable_1_t2883136337  : public OperatorObservableBase_1_t1441642626
{
public:
	// UniRx.IObservable`1<T>[] UniRx.Operators.CombineLatestObservable`1::sources
	IObservable_1U5BU5D_t1703862084* ___sources_1;

public:
	inline static int32_t get_offset_of_sources_1() { return static_cast<int32_t>(offsetof(CombineLatestObservable_1_t2883136337, ___sources_1)); }
	inline IObservable_1U5BU5D_t1703862084* get_sources_1() const { return ___sources_1; }
	inline IObservable_1U5BU5D_t1703862084** get_address_of_sources_1() { return &___sources_1; }
	inline void set_sources_1(IObservable_1U5BU5D_t1703862084* value)
	{
		___sources_1 = value;
		Il2CppCodeGenWriteBarrier(&___sources_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
