﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.PlayFabClientAPI/GetCurrentGamesRequestCallback
struct GetCurrentGamesRequestCallback_t2884599030;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;
// PlayFab.ClientModels.CurrentGamesRequest
struct CurrentGamesRequest_t3387974807;
// System.IAsyncResult
struct IAsyncResult_t537683269;
// System.AsyncCallback
struct AsyncCallback_t1363551830;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_IntPtr676692020.h"
#include "mscorlib_System_String968488902.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_CurrentGame3387974807.h"
#include "mscorlib_System_AsyncCallback1363551830.h"

// System.Void PlayFab.PlayFabClientAPI/GetCurrentGamesRequestCallback::.ctor(System.Object,System.IntPtr)
extern "C"  void GetCurrentGamesRequestCallback__ctor_m689981157 (GetCurrentGamesRequestCallback_t2884599030 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/GetCurrentGamesRequestCallback::Invoke(System.String,System.Int32,PlayFab.ClientModels.CurrentGamesRequest,System.Object)
extern "C"  void GetCurrentGamesRequestCallback_Invoke_m3079886743 (GetCurrentGamesRequestCallback_t2884599030 * __this, String_t* ___urlPath0, int32_t ___callId1, CurrentGamesRequest_t3387974807 * ___request2, Il2CppObject * ___customData3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
extern "C" void pinvoke_delegate_wrapper_GetCurrentGamesRequestCallback_t2884599030(Il2CppObject* delegate, String_t* ___urlPath0, int32_t ___callId1, CurrentGamesRequest_t3387974807 * ___request2, Il2CppObject * ___customData3);
// System.IAsyncResult PlayFab.PlayFabClientAPI/GetCurrentGamesRequestCallback::BeginInvoke(System.String,System.Int32,PlayFab.ClientModels.CurrentGamesRequest,System.Object,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * GetCurrentGamesRequestCallback_BeginInvoke_m1688459580 (GetCurrentGamesRequestCallback_t2884599030 * __this, String_t* ___urlPath0, int32_t ___callId1, CurrentGamesRequest_t3387974807 * ___request2, Il2CppObject * ___customData3, AsyncCallback_t1363551830 * ___callback4, Il2CppObject * ___object5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/GetCurrentGamesRequestCallback::EndInvoke(System.IAsyncResult)
extern "C"  void GetCurrentGamesRequestCallback_EndInvoke_m3062690677 (GetCurrentGamesRequestCallback_t2884599030 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
