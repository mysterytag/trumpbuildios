﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.PlayFabClientAPI/<UpdateCharacterData>c__AnonStoreyCF
struct U3CUpdateCharacterDataU3Ec__AnonStoreyCF_t1885105786;
// PlayFab.CallRequestContainer
struct CallRequestContainer_t432318609;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharpU2Dfirstpass_PlayFab_CallRequestCo432318609.h"

// System.Void PlayFab.PlayFabClientAPI/<UpdateCharacterData>c__AnonStoreyCF::.ctor()
extern "C"  void U3CUpdateCharacterDataU3Ec__AnonStoreyCF__ctor_m3965252121 (U3CUpdateCharacterDataU3Ec__AnonStoreyCF_t1885105786 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/<UpdateCharacterData>c__AnonStoreyCF::<>m__BC(PlayFab.CallRequestContainer)
extern "C"  void U3CUpdateCharacterDataU3Ec__AnonStoreyCF_U3CU3Em__BC_m2230906808 (U3CUpdateCharacterDataU3Ec__AnonStoreyCF_t1885105786 * __this, CallRequestContainer_t432318609 * ___requestContainer0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
