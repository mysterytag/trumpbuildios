﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UniRx.Operators.SampleObservable`1<System.Object>
struct SampleObservable_1_t2474383436;
// System.Object
struct Il2CppObject;
// UniRx.SingleAssignmentDisposable
struct SingleAssignmentDisposable_t2336378823;

#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Oper1187768149.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Operators.SampleObservable`1/Sample<System.Object>
struct  Sample_t2164013235  : public OperatorObserverBase_2_t1187768149
{
public:
	// UniRx.Operators.SampleObservable`1<T> UniRx.Operators.SampleObservable`1/Sample::parent
	SampleObservable_1_t2474383436 * ___parent_2;
	// System.Object UniRx.Operators.SampleObservable`1/Sample::gate
	Il2CppObject * ___gate_3;
	// T UniRx.Operators.SampleObservable`1/Sample::latestValue
	Il2CppObject * ___latestValue_4;
	// System.Boolean UniRx.Operators.SampleObservable`1/Sample::isUpdated
	bool ___isUpdated_5;
	// System.Boolean UniRx.Operators.SampleObservable`1/Sample::isCompleted
	bool ___isCompleted_6;
	// UniRx.SingleAssignmentDisposable UniRx.Operators.SampleObservable`1/Sample::sourceSubscription
	SingleAssignmentDisposable_t2336378823 * ___sourceSubscription_7;

public:
	inline static int32_t get_offset_of_parent_2() { return static_cast<int32_t>(offsetof(Sample_t2164013235, ___parent_2)); }
	inline SampleObservable_1_t2474383436 * get_parent_2() const { return ___parent_2; }
	inline SampleObservable_1_t2474383436 ** get_address_of_parent_2() { return &___parent_2; }
	inline void set_parent_2(SampleObservable_1_t2474383436 * value)
	{
		___parent_2 = value;
		Il2CppCodeGenWriteBarrier(&___parent_2, value);
	}

	inline static int32_t get_offset_of_gate_3() { return static_cast<int32_t>(offsetof(Sample_t2164013235, ___gate_3)); }
	inline Il2CppObject * get_gate_3() const { return ___gate_3; }
	inline Il2CppObject ** get_address_of_gate_3() { return &___gate_3; }
	inline void set_gate_3(Il2CppObject * value)
	{
		___gate_3 = value;
		Il2CppCodeGenWriteBarrier(&___gate_3, value);
	}

	inline static int32_t get_offset_of_latestValue_4() { return static_cast<int32_t>(offsetof(Sample_t2164013235, ___latestValue_4)); }
	inline Il2CppObject * get_latestValue_4() const { return ___latestValue_4; }
	inline Il2CppObject ** get_address_of_latestValue_4() { return &___latestValue_4; }
	inline void set_latestValue_4(Il2CppObject * value)
	{
		___latestValue_4 = value;
		Il2CppCodeGenWriteBarrier(&___latestValue_4, value);
	}

	inline static int32_t get_offset_of_isUpdated_5() { return static_cast<int32_t>(offsetof(Sample_t2164013235, ___isUpdated_5)); }
	inline bool get_isUpdated_5() const { return ___isUpdated_5; }
	inline bool* get_address_of_isUpdated_5() { return &___isUpdated_5; }
	inline void set_isUpdated_5(bool value)
	{
		___isUpdated_5 = value;
	}

	inline static int32_t get_offset_of_isCompleted_6() { return static_cast<int32_t>(offsetof(Sample_t2164013235, ___isCompleted_6)); }
	inline bool get_isCompleted_6() const { return ___isCompleted_6; }
	inline bool* get_address_of_isCompleted_6() { return &___isCompleted_6; }
	inline void set_isCompleted_6(bool value)
	{
		___isCompleted_6 = value;
	}

	inline static int32_t get_offset_of_sourceSubscription_7() { return static_cast<int32_t>(offsetof(Sample_t2164013235, ___sourceSubscription_7)); }
	inline SingleAssignmentDisposable_t2336378823 * get_sourceSubscription_7() const { return ___sourceSubscription_7; }
	inline SingleAssignmentDisposable_t2336378823 ** get_address_of_sourceSubscription_7() { return &___sourceSubscription_7; }
	inline void set_sourceSubscription_7(SingleAssignmentDisposable_t2336378823 * value)
	{
		___sourceSubscription_7 = value;
		Il2CppCodeGenWriteBarrier(&___sourceSubscription_7, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
