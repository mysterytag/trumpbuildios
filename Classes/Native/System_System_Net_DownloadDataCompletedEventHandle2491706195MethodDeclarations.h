﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Net.DownloadDataCompletedEventHandler
struct DownloadDataCompletedEventHandler_t2491706195;
// System.Object
struct Il2CppObject;
// System.Net.DownloadDataCompletedEventArgs
struct DownloadDataCompletedEventArgs_t3432986632;
// System.IAsyncResult
struct IAsyncResult_t537683269;
// System.AsyncCallback
struct AsyncCallback_t1363551830;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_IntPtr676692020.h"
#include "System_System_Net_DownloadDataCompletedEventArgs3432986632.h"
#include "mscorlib_System_AsyncCallback1363551830.h"

// System.Void System.Net.DownloadDataCompletedEventHandler::.ctor(System.Object,System.IntPtr)
extern "C"  void DownloadDataCompletedEventHandler__ctor_m365084767 (DownloadDataCompletedEventHandler_t2491706195 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.DownloadDataCompletedEventHandler::Invoke(System.Object,System.Net.DownloadDataCompletedEventArgs)
extern "C"  void DownloadDataCompletedEventHandler_Invoke_m3471237625 (DownloadDataCompletedEventHandler_t2491706195 * __this, Il2CppObject * ___sender0, DownloadDataCompletedEventArgs_t3432986632 * ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
extern "C" void pinvoke_delegate_wrapper_DownloadDataCompletedEventHandler_t2491706195(Il2CppObject* delegate, Il2CppObject * ___sender0, DownloadDataCompletedEventArgs_t3432986632 * ___e1);
// System.IAsyncResult System.Net.DownloadDataCompletedEventHandler::BeginInvoke(System.Object,System.Net.DownloadDataCompletedEventArgs,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * DownloadDataCompletedEventHandler_BeginInvoke_m1002964638 (DownloadDataCompletedEventHandler_t2491706195 * __this, Il2CppObject * ___sender0, DownloadDataCompletedEventArgs_t3432986632 * ___e1, AsyncCallback_t1363551830 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.DownloadDataCompletedEventHandler::EndInvoke(System.IAsyncResult)
extern "C"  void DownloadDataCompletedEventHandler_EndInvoke_m822819311 (DownloadDataCompletedEventHandler_t2491706195 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
