﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.PlayFabClientAPI/<UnlinkFacebookAccount>c__AnonStorey82
struct U3CUnlinkFacebookAccountU3Ec__AnonStorey82_t4076476891;
// PlayFab.CallRequestContainer
struct CallRequestContainer_t432318609;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharpU2Dfirstpass_PlayFab_CallRequestCo432318609.h"

// System.Void PlayFab.PlayFabClientAPI/<UnlinkFacebookAccount>c__AnonStorey82::.ctor()
extern "C"  void U3CUnlinkFacebookAccountU3Ec__AnonStorey82__ctor_m3652128728 (U3CUnlinkFacebookAccountU3Ec__AnonStorey82_t4076476891 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/<UnlinkFacebookAccount>c__AnonStorey82::<>m__6F(PlayFab.CallRequestContainer)
extern "C"  void U3CUnlinkFacebookAccountU3Ec__AnonStorey82_U3CU3Em__6F_m3433238406 (U3CUnlinkFacebookAccountU3Ec__AnonStorey82_t4076476891 * __this, CallRequestContainer_t432318609 * ___requestContainer0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
