﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Events.UnityEvent
struct UnityEvent_t2938797301;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.UnityEventExtensions/<AsObservable>c__AnonStorey95
struct  U3CAsObservableU3Ec__AnonStorey95_t3743595094  : public Il2CppObject
{
public:
	// UnityEngine.Events.UnityEvent UniRx.UnityEventExtensions/<AsObservable>c__AnonStorey95::unityEvent
	UnityEvent_t2938797301 * ___unityEvent_0;

public:
	inline static int32_t get_offset_of_unityEvent_0() { return static_cast<int32_t>(offsetof(U3CAsObservableU3Ec__AnonStorey95_t3743595094, ___unityEvent_0)); }
	inline UnityEvent_t2938797301 * get_unityEvent_0() const { return ___unityEvent_0; }
	inline UnityEvent_t2938797301 ** get_address_of_unityEvent_0() { return &___unityEvent_0; }
	inline void set_unityEvent_0(UnityEvent_t2938797301 * value)
	{
		___unityEvent_0 = value;
		Il2CppCodeGenWriteBarrier(&___unityEvent_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
