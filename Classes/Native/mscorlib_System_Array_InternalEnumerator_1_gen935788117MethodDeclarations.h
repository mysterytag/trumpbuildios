﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Array
struct Il2CppArray;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen935788117.h"
#include "mscorlib_System_Array2840145358.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_21028297614.h"

// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Int64>>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m554233656_gshared (InternalEnumerator_1_t935788117 * __this, Il2CppArray * ___array0, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m554233656(__this, ___array0, method) ((  void (*) (InternalEnumerator_1_t935788117 *, Il2CppArray *, const MethodInfo*))InternalEnumerator_1__ctor_m554233656_gshared)(__this, ___array0, method)
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Int64>>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3771714408_gshared (InternalEnumerator_1_t935788117 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3771714408(__this, method) ((  void (*) (InternalEnumerator_1_t935788117 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3771714408_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Int64>>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m611657246_gshared (InternalEnumerator_1_t935788117 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m611657246(__this, method) ((  Il2CppObject * (*) (InternalEnumerator_1_t935788117 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m611657246_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Int64>>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m1010147087_gshared (InternalEnumerator_1_t935788117 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m1010147087(__this, method) ((  void (*) (InternalEnumerator_1_t935788117 *, const MethodInfo*))InternalEnumerator_1_Dispose_m1010147087_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Int64>>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m2655555224_gshared (InternalEnumerator_1_t935788117 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m2655555224(__this, method) ((  bool (*) (InternalEnumerator_1_t935788117 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m2655555224_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Int64>>::get_Current()
extern "C"  KeyValuePair_2_t1028297614  InternalEnumerator_1_get_Current_m800971041_gshared (InternalEnumerator_1_t935788117 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m800971041(__this, method) ((  KeyValuePair_2_t1028297614  (*) (InternalEnumerator_1_t935788117 *, const MethodInfo*))InternalEnumerator_1_get_Current_m800971041_gshared)(__this, method)
