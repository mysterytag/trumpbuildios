﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.DoOnCompletedObservable`1<System.Object>
struct DoOnCompletedObservable_1_t3425980779;
// UniRx.IObservable`1<System.Object>
struct IObservable_1_t595904784;
// System.Action
struct Action_t437523947;
// System.IDisposable
struct IDisposable_t1628921374;
// UniRx.IObserver`1<System.Object>
struct IObserver_1_t3049105323;

#include "codegen/il2cpp-codegen.h"
#include "System_Core_System_Action437523947.h"

// System.Void UniRx.Operators.DoOnCompletedObservable`1<System.Object>::.ctor(UniRx.IObservable`1<T>,System.Action)
extern "C"  void DoOnCompletedObservable_1__ctor_m2098452999_gshared (DoOnCompletedObservable_1_t3425980779 * __this, Il2CppObject* ___source0, Action_t437523947 * ___onCompleted1, const MethodInfo* method);
#define DoOnCompletedObservable_1__ctor_m2098452999(__this, ___source0, ___onCompleted1, method) ((  void (*) (DoOnCompletedObservable_1_t3425980779 *, Il2CppObject*, Action_t437523947 *, const MethodInfo*))DoOnCompletedObservable_1__ctor_m2098452999_gshared)(__this, ___source0, ___onCompleted1, method)
// System.IDisposable UniRx.Operators.DoOnCompletedObservable`1<System.Object>::SubscribeCore(UniRx.IObserver`1<T>,System.IDisposable)
extern "C"  Il2CppObject * DoOnCompletedObservable_1_SubscribeCore_m2149812173_gshared (DoOnCompletedObservable_1_t3425980779 * __this, Il2CppObject* ___observer0, Il2CppObject * ___cancel1, const MethodInfo* method);
#define DoOnCompletedObservable_1_SubscribeCore_m2149812173(__this, ___observer0, ___cancel1, method) ((  Il2CppObject * (*) (DoOnCompletedObservable_1_t3425980779 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))DoOnCompletedObservable_1_SubscribeCore_m2149812173_gshared)(__this, ___observer0, ___cancel1, method)
