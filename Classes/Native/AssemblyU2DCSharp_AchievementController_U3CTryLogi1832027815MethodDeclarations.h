﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// AchievementController/<TryLoginOnce>c__AnonStoreyD7
struct U3CTryLoginOnceU3Ec__AnonStoreyD7_t1832027815;

#include "codegen/il2cpp-codegen.h"

// System.Void AchievementController/<TryLoginOnce>c__AnonStoreyD7::.ctor()
extern "C"  void U3CTryLoginOnceU3Ec__AnonStoreyD7__ctor_m2555643688 (U3CTryLoginOnceU3Ec__AnonStoreyD7_t1832027815 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AchievementController/<TryLoginOnce>c__AnonStoreyD7::<>m__C6(System.Boolean)
extern "C"  void U3CTryLoginOnceU3Ec__AnonStoreyD7_U3CU3Em__C6_m2964997627 (U3CTryLoginOnceU3Ec__AnonStoreyD7_t1832027815 * __this, bool ___ok0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
