﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.FromEventObservable`2/FromEvent<System.Object,UniRx.Tuple`2<System.Object,System.Object>>
struct FromEvent_t2074121294;
// UniRx.Operators.FromEventObservable`2<System.Object,UniRx.Tuple`2<System.Object,System.Object>>
struct FromEventObservable_2_t1784294996;
// UniRx.IObserver`1<UniRx.Tuple`2<System.Object,System.Object>>
struct IObserver_1_t2581260722;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Tuple_2_gen369261819.h"

// System.Void UniRx.Operators.FromEventObservable`2/FromEvent<System.Object,UniRx.Tuple`2<System.Object,System.Object>>::.ctor(UniRx.Operators.FromEventObservable`2<TDelegate,TEventArgs>,UniRx.IObserver`1<TEventArgs>)
extern "C"  void FromEvent__ctor_m3905763889_gshared (FromEvent_t2074121294 * __this, FromEventObservable_2_t1784294996 * ___parent0, Il2CppObject* ___observer1, const MethodInfo* method);
#define FromEvent__ctor_m3905763889(__this, ___parent0, ___observer1, method) ((  void (*) (FromEvent_t2074121294 *, FromEventObservable_2_t1784294996 *, Il2CppObject*, const MethodInfo*))FromEvent__ctor_m3905763889_gshared)(__this, ___parent0, ___observer1, method)
// System.Boolean UniRx.Operators.FromEventObservable`2/FromEvent<System.Object,UniRx.Tuple`2<System.Object,System.Object>>::Register()
extern "C"  bool FromEvent_Register_m2788819791_gshared (FromEvent_t2074121294 * __this, const MethodInfo* method);
#define FromEvent_Register_m2788819791(__this, method) ((  bool (*) (FromEvent_t2074121294 *, const MethodInfo*))FromEvent_Register_m2788819791_gshared)(__this, method)
// System.Void UniRx.Operators.FromEventObservable`2/FromEvent<System.Object,UniRx.Tuple`2<System.Object,System.Object>>::OnNext(TEventArgs)
extern "C"  void FromEvent_OnNext_m1816020663_gshared (FromEvent_t2074121294 * __this, Tuple_2_t369261819  ___args0, const MethodInfo* method);
#define FromEvent_OnNext_m1816020663(__this, ___args0, method) ((  void (*) (FromEvent_t2074121294 *, Tuple_2_t369261819 , const MethodInfo*))FromEvent_OnNext_m1816020663_gshared)(__this, ___args0, method)
// System.Void UniRx.Operators.FromEventObservable`2/FromEvent<System.Object,UniRx.Tuple`2<System.Object,System.Object>>::Dispose()
extern "C"  void FromEvent_Dispose_m3791647225_gshared (FromEvent_t2074121294 * __this, const MethodInfo* method);
#define FromEvent_Dispose_m3791647225(__this, method) ((  void (*) (FromEvent_t2074121294 *, const MethodInfo*))FromEvent_Dispose_m3791647225_gshared)(__this, method)
