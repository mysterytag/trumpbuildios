﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.UnityUIComponentExtensions/<OnValueChangeAsObservable>c__AnonStoreyAB
struct U3COnValueChangeAsObservableU3Ec__AnonStoreyAB_t1837341285;
// System.IDisposable
struct IDisposable_t1628921374;
// UniRx.IObserver`1<System.String>
struct IObserver_1_t3180487805;

#include "codegen/il2cpp-codegen.h"

// System.Void UniRx.UnityUIComponentExtensions/<OnValueChangeAsObservable>c__AnonStoreyAB::.ctor()
extern "C"  void U3COnValueChangeAsObservableU3Ec__AnonStoreyAB__ctor_m2301250083 (U3COnValueChangeAsObservableU3Ec__AnonStoreyAB_t1837341285 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IDisposable UniRx.UnityUIComponentExtensions/<OnValueChangeAsObservable>c__AnonStoreyAB::<>m__E9(UniRx.IObserver`1<System.String>)
extern "C"  Il2CppObject * U3COnValueChangeAsObservableU3Ec__AnonStoreyAB_U3CU3Em__E9_m2719238345 (U3COnValueChangeAsObservableU3Ec__AnonStoreyAB_t1837341285 * __this, Il2CppObject* ___observer0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
