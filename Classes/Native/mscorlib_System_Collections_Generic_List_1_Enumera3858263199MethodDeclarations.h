﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera4014815677MethodDeclarations.h"

// System.Void System.Collections.Generic.List`1/Enumerator<PlayFab.ClientModels.PSNAccountPlayFabIdPair>::.ctor(System.Collections.Generic.List`1<T>)
#define Enumerator__ctor_m1105711259(__this, ___l0, method) ((  void (*) (Enumerator_t3858263199 *, List_1_t1477512911 *, const MethodInfo*))Enumerator__ctor_m1029849669_gshared)(__this, ___l0, method)
// System.Void System.Collections.Generic.List`1/Enumerator<PlayFab.ClientModels.PSNAccountPlayFabIdPair>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m1837633239(__this, method) ((  void (*) (Enumerator_t3858263199 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m771996397_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1/Enumerator<PlayFab.ClientModels.PSNAccountPlayFabIdPair>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m1071581965(__this, method) ((  Il2CppObject * (*) (Enumerator_t3858263199 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m3561903705_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<PlayFab.ClientModels.PSNAccountPlayFabIdPair>::Dispose()
#define Enumerator_Dispose_m1404171584(__this, method) ((  void (*) (Enumerator_t3858263199 *, const MethodInfo*))Enumerator_Dispose_m2904289642_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<PlayFab.ClientModels.PSNAccountPlayFabIdPair>::VerifyState()
#define Enumerator_VerifyState_m4234744953(__this, method) ((  void (*) (Enumerator_t3858263199 *, const MethodInfo*))Enumerator_VerifyState_m1522854819_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<PlayFab.ClientModels.PSNAccountPlayFabIdPair>::MoveNext()
#define Enumerator_MoveNext_m346671623(__this, method) ((  bool (*) (Enumerator_t3858263199 *, const MethodInfo*))Enumerator_MoveNext_m302222300_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<PlayFab.ClientModels.PSNAccountPlayFabIdPair>::get_Current()
#define Enumerator_get_Current_m2864118930(__this, method) ((  PSNAccountPlayFabIdPair_t680553942 * (*) (Enumerator_t3858263199 *, const MethodInfo*))Enumerator_get_Current_m1113061044_gshared)(__this, method)
