﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Func`1<System.Single>
struct Func_1_t2100990268;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t537683269;
// System.AsyncCallback
struct AsyncCallback_t1363551830;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_IntPtr676692020.h"
#include "mscorlib_System_AsyncCallback1363551830.h"

// System.Void System.Func`1<System.Single>::.ctor(System.Object,System.IntPtr)
extern "C"  void Func_1__ctor_m2464158966_gshared (Func_1_t2100990268 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method);
#define Func_1__ctor_m2464158966(__this, ___object0, ___method1, method) ((  void (*) (Func_1_t2100990268 *, Il2CppObject *, IntPtr_t, const MethodInfo*))Func_1__ctor_m2464158966_gshared)(__this, ___object0, ___method1, method)
// TResult System.Func`1<System.Single>::Invoke()
extern "C"  float Func_1_Invoke_m4252412782_gshared (Func_1_t2100990268 * __this, const MethodInfo* method);
#define Func_1_Invoke_m4252412782(__this, method) ((  float (*) (Func_1_t2100990268 *, const MethodInfo*))Func_1_Invoke_m4252412782_gshared)(__this, method)
// System.IAsyncResult System.Func`1<System.Single>::BeginInvoke(System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * Func_1_BeginInvoke_m1122328627_gshared (Func_1_t2100990268 * __this, AsyncCallback_t1363551830 * ___callback0, Il2CppObject * ___object1, const MethodInfo* method);
#define Func_1_BeginInvoke_m1122328627(__this, ___callback0, ___object1, method) ((  Il2CppObject * (*) (Func_1_t2100990268 *, AsyncCallback_t1363551830 *, Il2CppObject *, const MethodInfo*))Func_1_BeginInvoke_m1122328627_gshared)(__this, ___callback0, ___object1, method)
// TResult System.Func`1<System.Single>::EndInvoke(System.IAsyncResult)
extern "C"  float Func_1_EndInvoke_m3577313448_gshared (Func_1_t2100990268 * __this, Il2CppObject * ___result0, const MethodInfo* method);
#define Func_1_EndInvoke_m3577313448(__this, ___result0, method) ((  float (*) (Func_1_t2100990268 *, Il2CppObject *, const MethodInfo*))Func_1_EndInvoke_m3577313448_gshared)(__this, ___result0, method)
