﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Zenject.TypeAnalyzer/<GetConstructorInjectables>c__AnonStorey181
struct U3CGetConstructorInjectablesU3Ec__AnonStorey181_t3473840153;
// Zenject.InjectableInfo
struct InjectableInfo_t1147709774;
// System.Reflection.ParameterInfo
struct ParameterInfo_t2610273829;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Reflection_ParameterInfo2610273829.h"

// System.Void Zenject.TypeAnalyzer/<GetConstructorInjectables>c__AnonStorey181::.ctor()
extern "C"  void U3CGetConstructorInjectablesU3Ec__AnonStorey181__ctor_m1614566642 (U3CGetConstructorInjectablesU3Ec__AnonStorey181_t3473840153 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Zenject.InjectableInfo Zenject.TypeAnalyzer/<GetConstructorInjectables>c__AnonStorey181::<>m__2B0(System.Reflection.ParameterInfo)
extern "C"  InjectableInfo_t1147709774 * U3CGetConstructorInjectablesU3Ec__AnonStorey181_U3CU3Em__2B0_m3973830771 (U3CGetConstructorInjectablesU3Ec__AnonStorey181_t3473840153 * __this, ParameterInfo_t2610273829 * ___paramInfo0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
