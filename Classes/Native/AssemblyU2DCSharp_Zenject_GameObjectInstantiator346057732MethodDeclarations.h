﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Zenject.GameObjectInstantiator
struct GameObjectInstantiator_t346057732;

#include "codegen/il2cpp-codegen.h"

// System.Void Zenject.GameObjectInstantiator::.ctor()
extern "C"  void GameObjectInstantiator__ctor_m884776685 (GameObjectInstantiator_t346057732 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
