﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_CollectionAddE1948677386.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Tuple_2_gen369261819.h"

// System.Void UniRx.CollectionAddEvent`1<UniRx.Tuple`2<System.Object,System.Object>>::.ctor(System.Int32,T)
extern "C"  void CollectionAddEvent_1__ctor_m1733868862_gshared (CollectionAddEvent_1_t1948677386 * __this, int32_t ___index0, Tuple_2_t369261819  ___value1, const MethodInfo* method);
#define CollectionAddEvent_1__ctor_m1733868862(__this, ___index0, ___value1, method) ((  void (*) (CollectionAddEvent_1_t1948677386 *, int32_t, Tuple_2_t369261819 , const MethodInfo*))CollectionAddEvent_1__ctor_m1733868862_gshared)(__this, ___index0, ___value1, method)
// System.Int32 UniRx.CollectionAddEvent`1<UniRx.Tuple`2<System.Object,System.Object>>::get_Index()
extern "C"  int32_t CollectionAddEvent_1_get_Index_m4141336102_gshared (CollectionAddEvent_1_t1948677386 * __this, const MethodInfo* method);
#define CollectionAddEvent_1_get_Index_m4141336102(__this, method) ((  int32_t (*) (CollectionAddEvent_1_t1948677386 *, const MethodInfo*))CollectionAddEvent_1_get_Index_m4141336102_gshared)(__this, method)
// System.Void UniRx.CollectionAddEvent`1<UniRx.Tuple`2<System.Object,System.Object>>::set_Index(System.Int32)
extern "C"  void CollectionAddEvent_1_set_Index_m2605776441_gshared (CollectionAddEvent_1_t1948677386 * __this, int32_t ___value0, const MethodInfo* method);
#define CollectionAddEvent_1_set_Index_m2605776441(__this, ___value0, method) ((  void (*) (CollectionAddEvent_1_t1948677386 *, int32_t, const MethodInfo*))CollectionAddEvent_1_set_Index_m2605776441_gshared)(__this, ___value0, method)
// T UniRx.CollectionAddEvent`1<UniRx.Tuple`2<System.Object,System.Object>>::get_Value()
extern "C"  Tuple_2_t369261819  CollectionAddEvent_1_get_Value_m4091107292_gshared (CollectionAddEvent_1_t1948677386 * __this, const MethodInfo* method);
#define CollectionAddEvent_1_get_Value_m4091107292(__this, method) ((  Tuple_2_t369261819  (*) (CollectionAddEvent_1_t1948677386 *, const MethodInfo*))CollectionAddEvent_1_get_Value_m4091107292_gshared)(__this, method)
// System.Void UniRx.CollectionAddEvent`1<UniRx.Tuple`2<System.Object,System.Object>>::set_Value(T)
extern "C"  void CollectionAddEvent_1_set_Value_m1420762231_gshared (CollectionAddEvent_1_t1948677386 * __this, Tuple_2_t369261819  ___value0, const MethodInfo* method);
#define CollectionAddEvent_1_set_Value_m1420762231(__this, ___value0, method) ((  void (*) (CollectionAddEvent_1_t1948677386 *, Tuple_2_t369261819 , const MethodInfo*))CollectionAddEvent_1_set_Value_m1420762231_gshared)(__this, ___value0, method)
// System.String UniRx.CollectionAddEvent`1<UniRx.Tuple`2<System.Object,System.Object>>::ToString()
extern "C"  String_t* CollectionAddEvent_1_ToString_m3083878430_gshared (CollectionAddEvent_1_t1948677386 * __this, const MethodInfo* method);
#define CollectionAddEvent_1_ToString_m3083878430(__this, method) ((  String_t* (*) (CollectionAddEvent_1_t1948677386 *, const MethodInfo*))CollectionAddEvent_1_ToString_m3083878430_gshared)(__this, method)
// System.Int32 UniRx.CollectionAddEvent`1<UniRx.Tuple`2<System.Object,System.Object>>::GetHashCode()
extern "C"  int32_t CollectionAddEvent_1_GetHashCode_m1678156814_gshared (CollectionAddEvent_1_t1948677386 * __this, const MethodInfo* method);
#define CollectionAddEvent_1_GetHashCode_m1678156814(__this, method) ((  int32_t (*) (CollectionAddEvent_1_t1948677386 *, const MethodInfo*))CollectionAddEvent_1_GetHashCode_m1678156814_gshared)(__this, method)
// System.Boolean UniRx.CollectionAddEvent`1<UniRx.Tuple`2<System.Object,System.Object>>::Equals(UniRx.CollectionAddEvent`1<T>)
extern "C"  bool CollectionAddEvent_1_Equals_m2717942978_gshared (CollectionAddEvent_1_t1948677386 * __this, CollectionAddEvent_1_t1948677386  ___other0, const MethodInfo* method);
#define CollectionAddEvent_1_Equals_m2717942978(__this, ___other0, method) ((  bool (*) (CollectionAddEvent_1_t1948677386 *, CollectionAddEvent_1_t1948677386 , const MethodInfo*))CollectionAddEvent_1_Equals_m2717942978_gshared)(__this, ___other0, method)
