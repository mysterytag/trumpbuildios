﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "AssemblyU2DCSharp_PlayFab_Internal_PlayFabResultCom379512675.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PlayFab.ClientModels.GrantCharacterToUserResult
struct  GrantCharacterToUserResult_t490373856  : public PlayFabResultCommon_t379512675
{
public:
	// System.String PlayFab.ClientModels.GrantCharacterToUserResult::<CharacterId>k__BackingField
	String_t* ___U3CCharacterIdU3Ek__BackingField_2;
	// System.String PlayFab.ClientModels.GrantCharacterToUserResult::<CharacterType>k__BackingField
	String_t* ___U3CCharacterTypeU3Ek__BackingField_3;
	// System.Boolean PlayFab.ClientModels.GrantCharacterToUserResult::<Result>k__BackingField
	bool ___U3CResultU3Ek__BackingField_4;

public:
	inline static int32_t get_offset_of_U3CCharacterIdU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(GrantCharacterToUserResult_t490373856, ___U3CCharacterIdU3Ek__BackingField_2)); }
	inline String_t* get_U3CCharacterIdU3Ek__BackingField_2() const { return ___U3CCharacterIdU3Ek__BackingField_2; }
	inline String_t** get_address_of_U3CCharacterIdU3Ek__BackingField_2() { return &___U3CCharacterIdU3Ek__BackingField_2; }
	inline void set_U3CCharacterIdU3Ek__BackingField_2(String_t* value)
	{
		___U3CCharacterIdU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier(&___U3CCharacterIdU3Ek__BackingField_2, value);
	}

	inline static int32_t get_offset_of_U3CCharacterTypeU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(GrantCharacterToUserResult_t490373856, ___U3CCharacterTypeU3Ek__BackingField_3)); }
	inline String_t* get_U3CCharacterTypeU3Ek__BackingField_3() const { return ___U3CCharacterTypeU3Ek__BackingField_3; }
	inline String_t** get_address_of_U3CCharacterTypeU3Ek__BackingField_3() { return &___U3CCharacterTypeU3Ek__BackingField_3; }
	inline void set_U3CCharacterTypeU3Ek__BackingField_3(String_t* value)
	{
		___U3CCharacterTypeU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier(&___U3CCharacterTypeU3Ek__BackingField_3, value);
	}

	inline static int32_t get_offset_of_U3CResultU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(GrantCharacterToUserResult_t490373856, ___U3CResultU3Ek__BackingField_4)); }
	inline bool get_U3CResultU3Ek__BackingField_4() const { return ___U3CResultU3Ek__BackingField_4; }
	inline bool* get_address_of_U3CResultU3Ek__BackingField_4() { return &___U3CResultU3Ek__BackingField_4; }
	inline void set_U3CResultU3Ek__BackingField_4(bool value)
	{
		___U3CResultU3Ek__BackingField_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
