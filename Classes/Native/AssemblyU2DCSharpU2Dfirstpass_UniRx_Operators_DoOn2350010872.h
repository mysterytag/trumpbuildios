﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UniRx.Operators.DoOnTerminateObservable`1<System.Object>
struct DoOnTerminateObservable_1_t3263525393;

#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Oper1187768149.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Operators.DoOnTerminateObservable`1/DoOnTerminate<System.Object>
struct  DoOnTerminate_t2350010872  : public OperatorObserverBase_2_t1187768149
{
public:
	// UniRx.Operators.DoOnTerminateObservable`1<T> UniRx.Operators.DoOnTerminateObservable`1/DoOnTerminate::parent
	DoOnTerminateObservable_1_t3263525393 * ___parent_2;

public:
	inline static int32_t get_offset_of_parent_2() { return static_cast<int32_t>(offsetof(DoOnTerminate_t2350010872, ___parent_2)); }
	inline DoOnTerminateObservable_1_t3263525393 * get_parent_2() const { return ___parent_2; }
	inline DoOnTerminateObservable_1_t3263525393 ** get_address_of_parent_2() { return &___parent_2; }
	inline void set_parent_2(DoOnTerminateObservable_1_t3263525393 * value)
	{
		___parent_2 = value;
		Il2CppCodeGenWriteBarrier(&___parent_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
