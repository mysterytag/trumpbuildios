﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2/Transform`1<System.Object,LitJson.PropertyMetadata,System.Collections.Generic.KeyValuePair`2<System.Object,LitJson.PropertyMetadata>>
struct Transform_1_t1793495238;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t537683269;
// System.AsyncCallback
struct AsyncCallback_t1363551830;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_IntPtr676692020.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_22123356821.h"
#include "AssemblyU2DCSharp_LitJson_PropertyMetadata3942474089.h"
#include "mscorlib_System_AsyncCallback1363551830.h"

// System.Void System.Collections.Generic.Dictionary`2/Transform`1<System.Object,LitJson.PropertyMetadata,System.Collections.Generic.KeyValuePair`2<System.Object,LitJson.PropertyMetadata>>::.ctor(System.Object,System.IntPtr)
extern "C"  void Transform_1__ctor_m3593258482_gshared (Transform_1_t1793495238 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method);
#define Transform_1__ctor_m3593258482(__this, ___object0, ___method1, method) ((  void (*) (Transform_1_t1793495238 *, Il2CppObject *, IntPtr_t, const MethodInfo*))Transform_1__ctor_m3593258482_gshared)(__this, ___object0, ___method1, method)
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Object,LitJson.PropertyMetadata,System.Collections.Generic.KeyValuePair`2<System.Object,LitJson.PropertyMetadata>>::Invoke(TKey,TValue)
extern "C"  KeyValuePair_2_t2123356821  Transform_1_Invoke_m1570214214_gshared (Transform_1_t1793495238 * __this, Il2CppObject * ___key0, PropertyMetadata_t3942474089  ___value1, const MethodInfo* method);
#define Transform_1_Invoke_m1570214214(__this, ___key0, ___value1, method) ((  KeyValuePair_2_t2123356821  (*) (Transform_1_t1793495238 *, Il2CppObject *, PropertyMetadata_t3942474089 , const MethodInfo*))Transform_1_Invoke_m1570214214_gshared)(__this, ___key0, ___value1, method)
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<System.Object,LitJson.PropertyMetadata,System.Collections.Generic.KeyValuePair`2<System.Object,LitJson.PropertyMetadata>>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * Transform_1_BeginInvoke_m3109135473_gshared (Transform_1_t1793495238 * __this, Il2CppObject * ___key0, PropertyMetadata_t3942474089  ___value1, AsyncCallback_t1363551830 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method);
#define Transform_1_BeginInvoke_m3109135473(__this, ___key0, ___value1, ___callback2, ___object3, method) ((  Il2CppObject * (*) (Transform_1_t1793495238 *, Il2CppObject *, PropertyMetadata_t3942474089 , AsyncCallback_t1363551830 *, Il2CppObject *, const MethodInfo*))Transform_1_BeginInvoke_m3109135473_gshared)(__this, ___key0, ___value1, ___callback2, ___object3, method)
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Object,LitJson.PropertyMetadata,System.Collections.Generic.KeyValuePair`2<System.Object,LitJson.PropertyMetadata>>::EndInvoke(System.IAsyncResult)
extern "C"  KeyValuePair_2_t2123356821  Transform_1_EndInvoke_m3522275204_gshared (Transform_1_t1793495238 * __this, Il2CppObject * ___result0, const MethodInfo* method);
#define Transform_1_EndInvoke_m3522275204(__this, ___result0, method) ((  KeyValuePair_2_t2123356821  (*) (Transform_1_t1793495238 *, Il2CppObject *, const MethodInfo*))Transform_1_EndInvoke_m3522275204_gshared)(__this, ___result0, method)
