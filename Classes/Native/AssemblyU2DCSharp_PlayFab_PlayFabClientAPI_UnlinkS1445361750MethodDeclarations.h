﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.PlayFabClientAPI/UnlinkSteamAccountResponseCallback
struct UnlinkSteamAccountResponseCallback_t1445361750;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;
// PlayFab.ClientModels.UnlinkSteamAccountRequest
struct UnlinkSteamAccountRequest_t775800815;
// PlayFab.ClientModels.UnlinkSteamAccountResult
struct UnlinkSteamAccountResult_t1030004957;
// PlayFab.PlayFabError
struct PlayFabError_t750598646;
// System.IAsyncResult
struct IAsyncResult_t537683269;
// System.AsyncCallback
struct AsyncCallback_t1363551830;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_IntPtr676692020.h"
#include "mscorlib_System_String968488902.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_UnlinkSteamA775800815.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_UnlinkSteam1030004957.h"
#include "AssemblyU2DCSharpU2Dfirstpass_PlayFab_PlayFabError750598646.h"
#include "mscorlib_System_AsyncCallback1363551830.h"

// System.Void PlayFab.PlayFabClientAPI/UnlinkSteamAccountResponseCallback::.ctor(System.Object,System.IntPtr)
extern "C"  void UnlinkSteamAccountResponseCallback__ctor_m2490890821 (UnlinkSteamAccountResponseCallback_t1445361750 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/UnlinkSteamAccountResponseCallback::Invoke(System.String,System.Int32,PlayFab.ClientModels.UnlinkSteamAccountRequest,PlayFab.ClientModels.UnlinkSteamAccountResult,PlayFab.PlayFabError,System.Object)
extern "C"  void UnlinkSteamAccountResponseCallback_Invoke_m1721563410 (UnlinkSteamAccountResponseCallback_t1445361750 * __this, String_t* ___urlPath0, int32_t ___callId1, UnlinkSteamAccountRequest_t775800815 * ___request2, UnlinkSteamAccountResult_t1030004957 * ___result3, PlayFabError_t750598646 * ___error4, Il2CppObject * ___customData5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
extern "C" void pinvoke_delegate_wrapper_UnlinkSteamAccountResponseCallback_t1445361750(Il2CppObject* delegate, String_t* ___urlPath0, int32_t ___callId1, UnlinkSteamAccountRequest_t775800815 * ___request2, UnlinkSteamAccountResult_t1030004957 * ___result3, PlayFabError_t750598646 * ___error4, Il2CppObject * ___customData5);
// System.IAsyncResult PlayFab.PlayFabClientAPI/UnlinkSteamAccountResponseCallback::BeginInvoke(System.String,System.Int32,PlayFab.ClientModels.UnlinkSteamAccountRequest,PlayFab.ClientModels.UnlinkSteamAccountResult,PlayFab.PlayFabError,System.Object,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * UnlinkSteamAccountResponseCallback_BeginInvoke_m2911629503 (UnlinkSteamAccountResponseCallback_t1445361750 * __this, String_t* ___urlPath0, int32_t ___callId1, UnlinkSteamAccountRequest_t775800815 * ___request2, UnlinkSteamAccountResult_t1030004957 * ___result3, PlayFabError_t750598646 * ___error4, Il2CppObject * ___customData5, AsyncCallback_t1363551830 * ___callback6, Il2CppObject * ___object7, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/UnlinkSteamAccountResponseCallback::EndInvoke(System.IAsyncResult)
extern "C"  void UnlinkSteamAccountResponseCallback_EndInvoke_m3850720981 (UnlinkSteamAccountResponseCallback_t1445361750 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
