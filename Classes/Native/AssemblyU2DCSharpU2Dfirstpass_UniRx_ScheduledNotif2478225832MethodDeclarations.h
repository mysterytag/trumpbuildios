﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.ScheduledNotifier`1<System.Single>
struct ScheduledNotifier_1_t2478225832;
// UniRx.IScheduler
struct IScheduler_t2938318244;
// System.IDisposable
struct IDisposable_t1628921374;
// UniRx.IObserver`1<System.Single>
struct IObserver_1_t3170207924;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_TimeSpan763862892.h"
#include "mscorlib_System_DateTimeOffset3712260035.h"

// System.Void UniRx.ScheduledNotifier`1<System.Single>::.ctor()
extern "C"  void ScheduledNotifier_1__ctor_m184940776_gshared (ScheduledNotifier_1_t2478225832 * __this, const MethodInfo* method);
#define ScheduledNotifier_1__ctor_m184940776(__this, method) ((  void (*) (ScheduledNotifier_1_t2478225832 *, const MethodInfo*))ScheduledNotifier_1__ctor_m184940776_gshared)(__this, method)
// System.Void UniRx.ScheduledNotifier`1<System.Single>::.ctor(UniRx.IScheduler)
extern "C"  void ScheduledNotifier_1__ctor_m4200316926_gshared (ScheduledNotifier_1_t2478225832 * __this, Il2CppObject * ___scheduler0, const MethodInfo* method);
#define ScheduledNotifier_1__ctor_m4200316926(__this, ___scheduler0, method) ((  void (*) (ScheduledNotifier_1_t2478225832 *, Il2CppObject *, const MethodInfo*))ScheduledNotifier_1__ctor_m4200316926_gshared)(__this, ___scheduler0, method)
// System.Void UniRx.ScheduledNotifier`1<System.Single>::Report(T)
extern "C"  void ScheduledNotifier_1_Report_m928447118_gshared (ScheduledNotifier_1_t2478225832 * __this, float ___value0, const MethodInfo* method);
#define ScheduledNotifier_1_Report_m928447118(__this, ___value0, method) ((  void (*) (ScheduledNotifier_1_t2478225832 *, float, const MethodInfo*))ScheduledNotifier_1_Report_m928447118_gshared)(__this, ___value0, method)
// System.IDisposable UniRx.ScheduledNotifier`1<System.Single>::Report(T,System.TimeSpan)
extern "C"  Il2CppObject * ScheduledNotifier_1_Report_m2567692745_gshared (ScheduledNotifier_1_t2478225832 * __this, float ___value0, TimeSpan_t763862892  ___dueTime1, const MethodInfo* method);
#define ScheduledNotifier_1_Report_m2567692745(__this, ___value0, ___dueTime1, method) ((  Il2CppObject * (*) (ScheduledNotifier_1_t2478225832 *, float, TimeSpan_t763862892 , const MethodInfo*))ScheduledNotifier_1_Report_m2567692745_gshared)(__this, ___value0, ___dueTime1, method)
// System.IDisposable UniRx.ScheduledNotifier`1<System.Single>::Report(T,System.DateTimeOffset)
extern "C"  Il2CppObject * ScheduledNotifier_1_Report_m3788465938_gshared (ScheduledNotifier_1_t2478225832 * __this, float ___value0, DateTimeOffset_t3712260035  ___dueTime1, const MethodInfo* method);
#define ScheduledNotifier_1_Report_m3788465938(__this, ___value0, ___dueTime1, method) ((  Il2CppObject * (*) (ScheduledNotifier_1_t2478225832 *, float, DateTimeOffset_t3712260035 , const MethodInfo*))ScheduledNotifier_1_Report_m3788465938_gshared)(__this, ___value0, ___dueTime1, method)
// System.IDisposable UniRx.ScheduledNotifier`1<System.Single>::Subscribe(UniRx.IObserver`1<T>)
extern "C"  Il2CppObject * ScheduledNotifier_1_Subscribe_m2331377831_gshared (ScheduledNotifier_1_t2478225832 * __this, Il2CppObject* ___observer0, const MethodInfo* method);
#define ScheduledNotifier_1_Subscribe_m2331377831(__this, ___observer0, method) ((  Il2CppObject * (*) (ScheduledNotifier_1_t2478225832 *, Il2CppObject*, const MethodInfo*))ScheduledNotifier_1_Subscribe_m2331377831_gshared)(__this, ___observer0, method)
