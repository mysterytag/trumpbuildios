﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// TagCollector
struct TagCollector_t573861427;
// IOnceEmptyStream
struct IOnceEmptyStream_t172682851;
// System.IDisposable
struct IDisposable_t1628921374;

#include "codegen/il2cpp-codegen.h"

// System.Void TagCollector::.ctor()
extern "C"  void TagCollector__ctor_m3645951432 (TagCollector_t573861427 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean TagCollector::get_isAlive()
extern "C"  bool TagCollector_get_isAlive_m1988918124 (TagCollector_t573861427 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// IOnceEmptyStream TagCollector::get_fellAsleep()
extern "C"  Il2CppObject * TagCollector_get_fellAsleep_m3412164692 (TagCollector_t573861427 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TagCollector::DisposeWhenAsleep(System.IDisposable)
extern "C"  void TagCollector_DisposeWhenAsleep_m400954091 (TagCollector_t573861427 * __this, Il2CppObject * ___disposable0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
