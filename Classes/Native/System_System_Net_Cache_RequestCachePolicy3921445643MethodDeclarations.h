﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Net.Cache.RequestCachePolicy
struct RequestCachePolicy_t3921445643;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"

// System.String System.Net.Cache.RequestCachePolicy::ToString()
extern "C"  String_t* RequestCachePolicy_ToString_m1738423012 (RequestCachePolicy_t3921445643 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
