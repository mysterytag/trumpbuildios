﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.ClientModels.CancelTradeResponse
struct CancelTradeResponse_t3042931035;
// PlayFab.ClientModels.TradeInfo
struct TradeInfo_t1933812770;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_TradeInfo1933812770.h"

// System.Void PlayFab.ClientModels.CancelTradeResponse::.ctor()
extern "C"  void CancelTradeResponse__ctor_m1358832046 (CancelTradeResponse_t3042931035 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PlayFab.ClientModels.TradeInfo PlayFab.ClientModels.CancelTradeResponse::get_Trade()
extern "C"  TradeInfo_t1933812770 * CancelTradeResponse_get_Trade_m699129022 (CancelTradeResponse_t3042931035 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.CancelTradeResponse::set_Trade(PlayFab.ClientModels.TradeInfo)
extern "C"  void CancelTradeResponse_set_Trade_m1043828479 (CancelTradeResponse_t3042931035 * __this, TradeInfo_t1933812770 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
