﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// RateScreenOpener
struct RateScreenOpener_t1572342051;

#include "codegen/il2cpp-codegen.h"

// System.Void RateScreenOpener::.ctor()
extern "C"  void RateScreenOpener__ctor_m1956754648 (RateScreenOpener_t1572342051 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void RateScreenOpener::Start()
extern "C"  void RateScreenOpener_Start_m903892440 (RateScreenOpener_t1572342051 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void RateScreenOpener::Update()
extern "C"  void RateScreenOpener_Update_m2256714037 (RateScreenOpener_t1572342051 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
