﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.ClientModels.UnlinkGoogleAccountRequest
struct UnlinkGoogleAccountRequest_t4045133086;

#include "codegen/il2cpp-codegen.h"

// System.Void PlayFab.ClientModels.UnlinkGoogleAccountRequest::.ctor()
extern "C"  void UnlinkGoogleAccountRequest__ctor_m531132511 (UnlinkGoogleAccountRequest_t4045133086 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
