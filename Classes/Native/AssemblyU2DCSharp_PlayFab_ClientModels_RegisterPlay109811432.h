﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// PlayFab.ClientModels.UserSettings
struct UserSettings_t8286526;

#include "AssemblyU2DCSharp_PlayFab_Internal_PlayFabResultCom379512675.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PlayFab.ClientModels.RegisterPlayFabUserResult
struct  RegisterPlayFabUserResult_t109811432  : public PlayFabResultCommon_t379512675
{
public:
	// System.String PlayFab.ClientModels.RegisterPlayFabUserResult::<PlayFabId>k__BackingField
	String_t* ___U3CPlayFabIdU3Ek__BackingField_2;
	// System.String PlayFab.ClientModels.RegisterPlayFabUserResult::<SessionTicket>k__BackingField
	String_t* ___U3CSessionTicketU3Ek__BackingField_3;
	// System.String PlayFab.ClientModels.RegisterPlayFabUserResult::<Username>k__BackingField
	String_t* ___U3CUsernameU3Ek__BackingField_4;
	// PlayFab.ClientModels.UserSettings PlayFab.ClientModels.RegisterPlayFabUserResult::<SettingsForUser>k__BackingField
	UserSettings_t8286526 * ___U3CSettingsForUserU3Ek__BackingField_5;

public:
	inline static int32_t get_offset_of_U3CPlayFabIdU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(RegisterPlayFabUserResult_t109811432, ___U3CPlayFabIdU3Ek__BackingField_2)); }
	inline String_t* get_U3CPlayFabIdU3Ek__BackingField_2() const { return ___U3CPlayFabIdU3Ek__BackingField_2; }
	inline String_t** get_address_of_U3CPlayFabIdU3Ek__BackingField_2() { return &___U3CPlayFabIdU3Ek__BackingField_2; }
	inline void set_U3CPlayFabIdU3Ek__BackingField_2(String_t* value)
	{
		___U3CPlayFabIdU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier(&___U3CPlayFabIdU3Ek__BackingField_2, value);
	}

	inline static int32_t get_offset_of_U3CSessionTicketU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(RegisterPlayFabUserResult_t109811432, ___U3CSessionTicketU3Ek__BackingField_3)); }
	inline String_t* get_U3CSessionTicketU3Ek__BackingField_3() const { return ___U3CSessionTicketU3Ek__BackingField_3; }
	inline String_t** get_address_of_U3CSessionTicketU3Ek__BackingField_3() { return &___U3CSessionTicketU3Ek__BackingField_3; }
	inline void set_U3CSessionTicketU3Ek__BackingField_3(String_t* value)
	{
		___U3CSessionTicketU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier(&___U3CSessionTicketU3Ek__BackingField_3, value);
	}

	inline static int32_t get_offset_of_U3CUsernameU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(RegisterPlayFabUserResult_t109811432, ___U3CUsernameU3Ek__BackingField_4)); }
	inline String_t* get_U3CUsernameU3Ek__BackingField_4() const { return ___U3CUsernameU3Ek__BackingField_4; }
	inline String_t** get_address_of_U3CUsernameU3Ek__BackingField_4() { return &___U3CUsernameU3Ek__BackingField_4; }
	inline void set_U3CUsernameU3Ek__BackingField_4(String_t* value)
	{
		___U3CUsernameU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier(&___U3CUsernameU3Ek__BackingField_4, value);
	}

	inline static int32_t get_offset_of_U3CSettingsForUserU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(RegisterPlayFabUserResult_t109811432, ___U3CSettingsForUserU3Ek__BackingField_5)); }
	inline UserSettings_t8286526 * get_U3CSettingsForUserU3Ek__BackingField_5() const { return ___U3CSettingsForUserU3Ek__BackingField_5; }
	inline UserSettings_t8286526 ** get_address_of_U3CSettingsForUserU3Ek__BackingField_5() { return &___U3CSettingsForUserU3Ek__BackingField_5; }
	inline void set_U3CSettingsForUserU3Ek__BackingField_5(UserSettings_t8286526 * value)
	{
		___U3CSettingsForUserU3Ek__BackingField_5 = value;
		Il2CppCodeGenWriteBarrier(&___U3CSettingsForUserU3Ek__BackingField_5, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
