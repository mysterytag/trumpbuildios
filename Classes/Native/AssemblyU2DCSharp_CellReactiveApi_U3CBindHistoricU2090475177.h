﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Action`2<System.Object,System.Object>
struct Action_2_t4105459918;
// CellReactiveApi/Carrier`1<System.Object>
struct Carrier_1_t2723215674;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CellReactiveApi/<BindHistoric>c__AnonStoreyFD`1<System.Object>
struct  U3CBindHistoricU3Ec__AnonStoreyFD_1_t2090475177  : public Il2CppObject
{
public:
	// System.Action`2<T,T> CellReactiveApi/<BindHistoric>c__AnonStoreyFD`1::reaction
	Action_2_t4105459918 * ___reaction_0;
	// CellReactiveApi/Carrier`1<T> CellReactiveApi/<BindHistoric>c__AnonStoreyFD`1::disp
	Carrier_1_t2723215674 * ___disp_1;

public:
	inline static int32_t get_offset_of_reaction_0() { return static_cast<int32_t>(offsetof(U3CBindHistoricU3Ec__AnonStoreyFD_1_t2090475177, ___reaction_0)); }
	inline Action_2_t4105459918 * get_reaction_0() const { return ___reaction_0; }
	inline Action_2_t4105459918 ** get_address_of_reaction_0() { return &___reaction_0; }
	inline void set_reaction_0(Action_2_t4105459918 * value)
	{
		___reaction_0 = value;
		Il2CppCodeGenWriteBarrier(&___reaction_0, value);
	}

	inline static int32_t get_offset_of_disp_1() { return static_cast<int32_t>(offsetof(U3CBindHistoricU3Ec__AnonStoreyFD_1_t2090475177, ___disp_1)); }
	inline Carrier_1_t2723215674 * get_disp_1() const { return ___disp_1; }
	inline Carrier_1_t2723215674 ** get_address_of_disp_1() { return &___disp_1; }
	inline void set_disp_1(Carrier_1_t2723215674 * value)
	{
		___disp_1 = value;
		Il2CppCodeGenWriteBarrier(&___disp_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
