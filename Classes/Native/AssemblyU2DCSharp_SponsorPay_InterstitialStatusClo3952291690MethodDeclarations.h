﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SponsorPay.InterstitialStatusCloseHandler
struct InterstitialStatusCloseHandler_t3952291690;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;
// System.IAsyncResult
struct IAsyncResult_t537683269;
// System.AsyncCallback
struct AsyncCallback_t1363551830;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_IntPtr676692020.h"
#include "mscorlib_System_String968488902.h"
#include "mscorlib_System_AsyncCallback1363551830.h"

// System.Void SponsorPay.InterstitialStatusCloseHandler::.ctor(System.Object,System.IntPtr)
extern "C"  void InterstitialStatusCloseHandler__ctor_m1177715239 (InterstitialStatusCloseHandler_t3952291690 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SponsorPay.InterstitialStatusCloseHandler::Invoke(System.String)
extern "C"  void InterstitialStatusCloseHandler_Invoke_m3493551969 (InterstitialStatusCloseHandler_t3952291690 * __this, String_t* ___closeReason0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
extern "C" void pinvoke_delegate_wrapper_InterstitialStatusCloseHandler_t3952291690(Il2CppObject* delegate, String_t* ___closeReason0);
// System.IAsyncResult SponsorPay.InterstitialStatusCloseHandler::BeginInvoke(System.String,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * InterstitialStatusCloseHandler_BeginInvoke_m2557616750 (InterstitialStatusCloseHandler_t3952291690 * __this, String_t* ___closeReason0, AsyncCallback_t1363551830 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SponsorPay.InterstitialStatusCloseHandler::EndInvoke(System.IAsyncResult)
extern "C"  void InterstitialStatusCloseHandler_EndInvoke_m116596663 (InterstitialStatusCloseHandler_t3952291690 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
