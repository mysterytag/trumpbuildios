﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.AsyncSubject`1/Subscription<System.Object>
struct Subscription_t2911320238;
// UniRx.AsyncSubject`1<System.Object>
struct AsyncSubject_1_t1536745524;
// UniRx.IObserver`1<System.Object>
struct IObserver_1_t3049105323;

#include "codegen/il2cpp-codegen.h"

// System.Void UniRx.AsyncSubject`1/Subscription<System.Object>::.ctor(UniRx.AsyncSubject`1<T>,UniRx.IObserver`1<T>)
extern "C"  void Subscription__ctor_m522143833_gshared (Subscription_t2911320238 * __this, AsyncSubject_1_t1536745524 * ___parent0, Il2CppObject* ___unsubscribeTarget1, const MethodInfo* method);
#define Subscription__ctor_m522143833(__this, ___parent0, ___unsubscribeTarget1, method) ((  void (*) (Subscription_t2911320238 *, AsyncSubject_1_t1536745524 *, Il2CppObject*, const MethodInfo*))Subscription__ctor_m522143833_gshared)(__this, ___parent0, ___unsubscribeTarget1, method)
// System.Void UniRx.AsyncSubject`1/Subscription<System.Object>::Dispose()
extern "C"  void Subscription_Dispose_m3345175043_gshared (Subscription_t2911320238 * __this, const MethodInfo* method);
#define Subscription_Dispose_m3345175043(__this, method) ((  void (*) (Subscription_t2911320238 *, const MethodInfo*))Subscription_Dispose_m3345175043_gshared)(__this, method)
