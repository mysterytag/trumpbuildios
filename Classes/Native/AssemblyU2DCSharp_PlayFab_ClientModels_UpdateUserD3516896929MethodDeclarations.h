﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.ClientModels.UpdateUserDataRequest
struct UpdateUserDataRequest_t3516896929;
// System.Collections.Generic.Dictionary`2<System.String,System.String>
struct Dictionary_2_t2606186806;
// System.Collections.Generic.List`1<System.String>
struct List_1_t1765447871;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Nullable_1_gen2611574664.h"

// System.Void PlayFab.ClientModels.UpdateUserDataRequest::.ctor()
extern "C"  void UpdateUserDataRequest__ctor_m356933032 (UpdateUserDataRequest_t3516896929 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.Dictionary`2<System.String,System.String> PlayFab.ClientModels.UpdateUserDataRequest::get_Data()
extern "C"  Dictionary_2_t2606186806 * UpdateUserDataRequest_get_Data_m1195434685 (UpdateUserDataRequest_t3516896929 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.UpdateUserDataRequest::set_Data(System.Collections.Generic.Dictionary`2<System.String,System.String>)
extern "C"  void UpdateUserDataRequest_set_Data_m1372090996 (UpdateUserDataRequest_t3516896929 * __this, Dictionary_2_t2606186806 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<System.String> PlayFab.ClientModels.UpdateUserDataRequest::get_KeysToRemove()
extern "C"  List_1_t1765447871 * UpdateUserDataRequest_get_KeysToRemove_m2447956105 (UpdateUserDataRequest_t3516896929 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.UpdateUserDataRequest::set_KeysToRemove(System.Collections.Generic.List`1<System.String>)
extern "C"  void UpdateUserDataRequest_set_KeysToRemove_m912505856 (UpdateUserDataRequest_t3516896929 * __this, List_1_t1765447871 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Nullable`1<PlayFab.ClientModels.UserDataPermission> PlayFab.ClientModels.UpdateUserDataRequest::get_Permission()
extern "C"  Nullable_1_t2611574664  UpdateUserDataRequest_get_Permission_m3603606058 (UpdateUserDataRequest_t3516896929 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.UpdateUserDataRequest::set_Permission(System.Nullable`1<PlayFab.ClientModels.UserDataPermission>)
extern "C"  void UpdateUserDataRequest_set_Permission_m2987771745 (UpdateUserDataRequest_t3516896929 * __this, Nullable_1_t2611574664  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
