﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Tuple_3_gen1650899102MethodDeclarations.h"

// System.Void UniRx.Tuple`3<UniRx.ICancelable,System.Object,System.Action`1<System.Object>>::.ctor(T1,T2,T3)
#define Tuple_3__ctor_m569045247(__this, ___item10, ___item21, ___item32, method) ((  void (*) (Tuple_3_t37583746 *, Il2CppObject *, Il2CppObject *, Action_1_t985559125 *, const MethodInfo*))Tuple_3__ctor_m1753573884_gshared)(__this, ___item10, ___item21, ___item32, method)
// System.Int32 UniRx.Tuple`3<UniRx.ICancelable,System.Object,System.Action`1<System.Object>>::System.IComparable.CompareTo(System.Object)
#define Tuple_3_System_IComparable_CompareTo_m82137389(__this, ___obj0, method) ((  int32_t (*) (Tuple_3_t37583746 *, Il2CppObject *, const MethodInfo*))Tuple_3_System_IComparable_CompareTo_m509379070_gshared)(__this, ___obj0, method)
// System.Int32 UniRx.Tuple`3<UniRx.ICancelable,System.Object,System.Action`1<System.Object>>::UniRx.IStructuralComparable.CompareTo(System.Object,System.Collections.IComparer)
#define Tuple_3_UniRx_IStructuralComparable_CompareTo_m1875836991(__this, ___other0, ___comparer1, method) ((  int32_t (*) (Tuple_3_t37583746 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))Tuple_3_UniRx_IStructuralComparable_CompareTo_m2886736_gshared)(__this, ___other0, ___comparer1, method)
// System.Boolean UniRx.Tuple`3<UniRx.ICancelable,System.Object,System.Action`1<System.Object>>::UniRx.IStructuralEquatable.Equals(System.Object,System.Collections.IEqualityComparer)
#define Tuple_3_UniRx_IStructuralEquatable_Equals_m891858358(__this, ___other0, ___comparer1, method) ((  bool (*) (Tuple_3_t37583746 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))Tuple_3_UniRx_IStructuralEquatable_Equals_m2030863419_gshared)(__this, ___other0, ___comparer1, method)
// System.Int32 UniRx.Tuple`3<UniRx.ICancelable,System.Object,System.Action`1<System.Object>>::UniRx.IStructuralEquatable.GetHashCode(System.Collections.IEqualityComparer)
#define Tuple_3_UniRx_IStructuralEquatable_GetHashCode_m584039364(__this, ___comparer0, method) ((  int32_t (*) (Tuple_3_t37583746 *, Il2CppObject *, const MethodInfo*))Tuple_3_UniRx_IStructuralEquatable_GetHashCode_m804104467_gshared)(__this, ___comparer0, method)
// System.String UniRx.Tuple`3<UniRx.ICancelable,System.Object,System.Action`1<System.Object>>::UniRx.ITuple.ToString()
#define Tuple_3_UniRx_ITuple_ToString_m2184228577(__this, method) ((  String_t* (*) (Tuple_3_t37583746 *, const MethodInfo*))Tuple_3_UniRx_ITuple_ToString_m363125048_gshared)(__this, method)
// T1 UniRx.Tuple`3<UniRx.ICancelable,System.Object,System.Action`1<System.Object>>::get_Item1()
#define Tuple_3_get_Item1_m3115589240(__this, method) ((  Il2CppObject * (*) (Tuple_3_t37583746 *, const MethodInfo*))Tuple_3_get_Item1_m1125131173_gshared)(__this, method)
// T2 UniRx.Tuple`3<UniRx.ICancelable,System.Object,System.Action`1<System.Object>>::get_Item2()
#define Tuple_3_get_Item2_m3101340056(__this, method) ((  Il2CppObject * (*) (Tuple_3_t37583746 *, const MethodInfo*))Tuple_3_get_Item2_m641456807_gshared)(__this, method)
// T3 UniRx.Tuple`3<UniRx.ICancelable,System.Object,System.Action`1<System.Object>>::get_Item3()
#define Tuple_3_get_Item3_m3087090872(__this, method) ((  Action_1_t985559125 * (*) (Tuple_3_t37583746 *, const MethodInfo*))Tuple_3_get_Item3_m157782441_gshared)(__this, method)
// System.Boolean UniRx.Tuple`3<UniRx.ICancelable,System.Object,System.Action`1<System.Object>>::Equals(System.Object)
#define Tuple_3_Equals_m2930796650(__this, ___obj0, method) ((  bool (*) (Tuple_3_t37583746 *, Il2CppObject *, const MethodInfo*))Tuple_3_Equals_m554362607_gshared)(__this, ___obj0, method)
// System.Int32 UniRx.Tuple`3<UniRx.ICancelable,System.Object,System.Action`1<System.Object>>::GetHashCode()
#define Tuple_3_GetHashCode_m2642358658(__this, method) ((  int32_t (*) (Tuple_3_t37583746 *, const MethodInfo*))Tuple_3_GetHashCode_m4073453075_gshared)(__this, method)
// System.String UniRx.Tuple`3<UniRx.ICancelable,System.Object,System.Action`1<System.Object>>::ToString()
#define Tuple_3_ToString_m3282866864(__this, method) ((  String_t* (*) (Tuple_3_t37583746 *, const MethodInfo*))Tuple_3_ToString_m3064948473_gshared)(__this, method)
// System.Boolean UniRx.Tuple`3<UniRx.ICancelable,System.Object,System.Action`1<System.Object>>::Equals(UniRx.Tuple`3<T1,T2,T3>)
#define Tuple_3_Equals_m2779278529(__this, ___other0, method) ((  bool (*) (Tuple_3_t37583746 *, Tuple_3_t37583746 , const MethodInfo*))Tuple_3_Equals_m1515079942_gshared)(__this, ___other0, method)
