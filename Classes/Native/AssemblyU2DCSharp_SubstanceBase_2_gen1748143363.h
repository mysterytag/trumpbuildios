﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// ICell`1<System.Single>
struct ICell_1_t2509839998;
// System.IDisposable
struct IDisposable_t1628921374;
// Stream`1<System.Single>
struct Stream_1_t3944315339;
// System.Collections.Generic.List`1<SubstanceBase`2/Intrusion<System.Single,System.Object>>
struct List_1_t754752792;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SubstanceBase`2<System.Single,System.Object>
struct  SubstanceBase_2_t1748143363  : public Il2CppObject
{
public:
	// T SubstanceBase`2::localBase
	float ___localBase_0;
	// ICell`1<T> SubstanceBase`2::externalBase
	Il2CppObject* ___externalBase_1;
	// System.IDisposable SubstanceBase`2::externalBaseConnection
	Il2CppObject * ___externalBaseConnection_2;
	// Stream`1<T> SubstanceBase`2::update
	Stream_1_t3944315339 * ___update_3;
	// T SubstanceBase`2::result
	float ___result_4;
	// System.Collections.Generic.List`1<SubstanceBase`2/Intrusion<T,InfT>> SubstanceBase`2::intrusions
	List_1_t754752792 * ___intrusions_5;

public:
	inline static int32_t get_offset_of_localBase_0() { return static_cast<int32_t>(offsetof(SubstanceBase_2_t1748143363, ___localBase_0)); }
	inline float get_localBase_0() const { return ___localBase_0; }
	inline float* get_address_of_localBase_0() { return &___localBase_0; }
	inline void set_localBase_0(float value)
	{
		___localBase_0 = value;
	}

	inline static int32_t get_offset_of_externalBase_1() { return static_cast<int32_t>(offsetof(SubstanceBase_2_t1748143363, ___externalBase_1)); }
	inline Il2CppObject* get_externalBase_1() const { return ___externalBase_1; }
	inline Il2CppObject** get_address_of_externalBase_1() { return &___externalBase_1; }
	inline void set_externalBase_1(Il2CppObject* value)
	{
		___externalBase_1 = value;
		Il2CppCodeGenWriteBarrier(&___externalBase_1, value);
	}

	inline static int32_t get_offset_of_externalBaseConnection_2() { return static_cast<int32_t>(offsetof(SubstanceBase_2_t1748143363, ___externalBaseConnection_2)); }
	inline Il2CppObject * get_externalBaseConnection_2() const { return ___externalBaseConnection_2; }
	inline Il2CppObject ** get_address_of_externalBaseConnection_2() { return &___externalBaseConnection_2; }
	inline void set_externalBaseConnection_2(Il2CppObject * value)
	{
		___externalBaseConnection_2 = value;
		Il2CppCodeGenWriteBarrier(&___externalBaseConnection_2, value);
	}

	inline static int32_t get_offset_of_update_3() { return static_cast<int32_t>(offsetof(SubstanceBase_2_t1748143363, ___update_3)); }
	inline Stream_1_t3944315339 * get_update_3() const { return ___update_3; }
	inline Stream_1_t3944315339 ** get_address_of_update_3() { return &___update_3; }
	inline void set_update_3(Stream_1_t3944315339 * value)
	{
		___update_3 = value;
		Il2CppCodeGenWriteBarrier(&___update_3, value);
	}

	inline static int32_t get_offset_of_result_4() { return static_cast<int32_t>(offsetof(SubstanceBase_2_t1748143363, ___result_4)); }
	inline float get_result_4() const { return ___result_4; }
	inline float* get_address_of_result_4() { return &___result_4; }
	inline void set_result_4(float value)
	{
		___result_4 = value;
	}

	inline static int32_t get_offset_of_intrusions_5() { return static_cast<int32_t>(offsetof(SubstanceBase_2_t1748143363, ___intrusions_5)); }
	inline List_1_t754752792 * get_intrusions_5() const { return ___intrusions_5; }
	inline List_1_t754752792 ** get_address_of_intrusions_5() { return &___intrusions_5; }
	inline void set_intrusions_5(List_1_t754752792 * value)
	{
		___intrusions_5 = value;
		Il2CppCodeGenWriteBarrier(&___intrusions_5, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
