﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.ObservableWWW/<LoadFromCacheOrDownload>c__AnonStorey8D
struct U3CLoadFromCacheOrDownloadU3Ec__AnonStorey8D_t1059502550;
// System.Collections.IEnumerator
struct IEnumerator_t287207039;
// UniRx.IObserver`1<UnityEngine.AssetBundle>
struct IObserver_1_t1876462710;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_CancellationTo1439151560.h"

// System.Void UniRx.ObservableWWW/<LoadFromCacheOrDownload>c__AnonStorey8D::.ctor()
extern "C"  void U3CLoadFromCacheOrDownloadU3Ec__AnonStorey8D__ctor_m2740786022 (U3CLoadFromCacheOrDownloadU3Ec__AnonStorey8D_t1059502550 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator UniRx.ObservableWWW/<LoadFromCacheOrDownload>c__AnonStorey8D::<>m__B9(UniRx.IObserver`1<UnityEngine.AssetBundle>,UniRx.CancellationToken)
extern "C"  Il2CppObject * U3CLoadFromCacheOrDownloadU3Ec__AnonStorey8D_U3CU3Em__B9_m4015006557 (U3CLoadFromCacheOrDownloadU3Ec__AnonStorey8D_t1059502550 * __this, Il2CppObject* ___observer0, CancellationToken_t1439151560  ___cancellation1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
