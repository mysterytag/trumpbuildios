﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// ConnectionCollector
struct ConnectionCollector_t444796719;
// System.String
struct String_t;
// System.IDisposable
struct IDisposable_t1628921374;
// EmptyStream
struct EmptyStream_t2850978573;

#include "UnityEngine_UnityEngine_MonoBehaviour3012272455.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Tacticsoft.TableViewCell
struct  TableViewCell_t776419755  : public MonoBehaviour_t3012272455
{
public:
	// ConnectionCollector Tacticsoft.TableViewCell::connections
	ConnectionCollector_t444796719 * ___connections_2;
	// System.String Tacticsoft.TableViewCell::Id
	String_t* ___Id_3;
	// System.IDisposable Tacticsoft.TableViewCell::connection
	Il2CppObject * ___connection_4;
	// EmptyStream Tacticsoft.TableViewCell::expandActivation
	EmptyStream_t2850978573 * ___expandActivation_5;
	// System.Boolean Tacticsoft.TableViewCell::deleted
	bool ___deleted_6;

public:
	inline static int32_t get_offset_of_connections_2() { return static_cast<int32_t>(offsetof(TableViewCell_t776419755, ___connections_2)); }
	inline ConnectionCollector_t444796719 * get_connections_2() const { return ___connections_2; }
	inline ConnectionCollector_t444796719 ** get_address_of_connections_2() { return &___connections_2; }
	inline void set_connections_2(ConnectionCollector_t444796719 * value)
	{
		___connections_2 = value;
		Il2CppCodeGenWriteBarrier(&___connections_2, value);
	}

	inline static int32_t get_offset_of_Id_3() { return static_cast<int32_t>(offsetof(TableViewCell_t776419755, ___Id_3)); }
	inline String_t* get_Id_3() const { return ___Id_3; }
	inline String_t** get_address_of_Id_3() { return &___Id_3; }
	inline void set_Id_3(String_t* value)
	{
		___Id_3 = value;
		Il2CppCodeGenWriteBarrier(&___Id_3, value);
	}

	inline static int32_t get_offset_of_connection_4() { return static_cast<int32_t>(offsetof(TableViewCell_t776419755, ___connection_4)); }
	inline Il2CppObject * get_connection_4() const { return ___connection_4; }
	inline Il2CppObject ** get_address_of_connection_4() { return &___connection_4; }
	inline void set_connection_4(Il2CppObject * value)
	{
		___connection_4 = value;
		Il2CppCodeGenWriteBarrier(&___connection_4, value);
	}

	inline static int32_t get_offset_of_expandActivation_5() { return static_cast<int32_t>(offsetof(TableViewCell_t776419755, ___expandActivation_5)); }
	inline EmptyStream_t2850978573 * get_expandActivation_5() const { return ___expandActivation_5; }
	inline EmptyStream_t2850978573 ** get_address_of_expandActivation_5() { return &___expandActivation_5; }
	inline void set_expandActivation_5(EmptyStream_t2850978573 * value)
	{
		___expandActivation_5 = value;
		Il2CppCodeGenWriteBarrier(&___expandActivation_5, value);
	}

	inline static int32_t get_offset_of_deleted_6() { return static_cast<int32_t>(offsetof(TableViewCell_t776419755, ___deleted_6)); }
	inline bool get_deleted_6() const { return ___deleted_6; }
	inline bool* get_address_of_deleted_6() { return &___deleted_6; }
	inline void set_deleted_6(bool value)
	{
		___deleted_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
