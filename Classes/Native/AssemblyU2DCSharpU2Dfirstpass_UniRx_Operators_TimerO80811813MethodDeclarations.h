﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.TimerObservable/Timer
struct Timer_t80811813;
// UniRx.IObserver`1<System.Int64>
struct IObserver_1_t764446489;
// System.IDisposable
struct IDisposable_t1628921374;
// System.Exception
struct Exception_t1967233988;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Exception1967233988.h"

// System.Void UniRx.Operators.TimerObservable/Timer::.ctor(UniRx.IObserver`1<System.Int64>,System.IDisposable)
extern "C"  void Timer__ctor_m970264198 (Timer_t80811813 * __this, Il2CppObject* ___observer0, Il2CppObject * ___cancel1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.Operators.TimerObservable/Timer::OnNext()
extern "C"  void Timer_OnNext_m2727522758 (Timer_t80811813 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.Operators.TimerObservable/Timer::OnNext(System.Int64)
extern "C"  void Timer_OnNext_m2130049432 (Timer_t80811813 * __this, int64_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.Operators.TimerObservable/Timer::OnError(System.Exception)
extern "C"  void Timer_OnError_m1423177575 (Timer_t80811813 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.Operators.TimerObservable/Timer::OnCompleted()
extern "C"  void Timer_OnCompleted_m621104442 (Timer_t80811813 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
