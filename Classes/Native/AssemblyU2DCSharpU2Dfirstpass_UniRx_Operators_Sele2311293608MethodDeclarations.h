﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.SelectManyObservable`2/SelectManyOuterObserver/SelectMany<System.Object,System.Object>
struct SelectMany_t2311293608;
// UniRx.Operators.SelectManyObservable`2/SelectManyOuterObserver<System.Object,System.Object>
struct SelectManyOuterObserver_t4093485561;
// System.IDisposable
struct IDisposable_t1628921374;
// System.Object
struct Il2CppObject;
// System.Exception
struct Exception_t1967233988;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_Exception1967233988.h"

// System.Void UniRx.Operators.SelectManyObservable`2/SelectManyOuterObserver/SelectMany<System.Object,System.Object>::.ctor(UniRx.Operators.SelectManyObservable`2/SelectManyOuterObserver<TSource,TResult>,System.IDisposable)
extern "C"  void SelectMany__ctor_m2910210691_gshared (SelectMany_t2311293608 * __this, SelectManyOuterObserver_t4093485561 * ___parent0, Il2CppObject * ___cancel1, const MethodInfo* method);
#define SelectMany__ctor_m2910210691(__this, ___parent0, ___cancel1, method) ((  void (*) (SelectMany_t2311293608 *, SelectManyOuterObserver_t4093485561 *, Il2CppObject *, const MethodInfo*))SelectMany__ctor_m2910210691_gshared)(__this, ___parent0, ___cancel1, method)
// System.Void UniRx.Operators.SelectManyObservable`2/SelectManyOuterObserver/SelectMany<System.Object,System.Object>::OnNext(TResult)
extern "C"  void SelectMany_OnNext_m2886469196_gshared (SelectMany_t2311293608 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define SelectMany_OnNext_m2886469196(__this, ___value0, method) ((  void (*) (SelectMany_t2311293608 *, Il2CppObject *, const MethodInfo*))SelectMany_OnNext_m2886469196_gshared)(__this, ___value0, method)
// System.Void UniRx.Operators.SelectManyObservable`2/SelectManyOuterObserver/SelectMany<System.Object,System.Object>::OnError(System.Exception)
extern "C"  void SelectMany_OnError_m307260408_gshared (SelectMany_t2311293608 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method);
#define SelectMany_OnError_m307260408(__this, ___error0, method) ((  void (*) (SelectMany_t2311293608 *, Exception_t1967233988 *, const MethodInfo*))SelectMany_OnError_m307260408_gshared)(__this, ___error0, method)
// System.Void UniRx.Operators.SelectManyObservable`2/SelectManyOuterObserver/SelectMany<System.Object,System.Object>::OnCompleted()
extern "C"  void SelectMany_OnCompleted_m1554469707_gshared (SelectMany_t2311293608 * __this, const MethodInfo* method);
#define SelectMany_OnCompleted_m1554469707(__this, method) ((  void (*) (SelectMany_t2311293608 *, const MethodInfo*))SelectMany_OnCompleted_m1554469707_gshared)(__this, method)
