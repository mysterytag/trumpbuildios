﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UniRx.Subject`1<System.Int32>
struct Subject_1_t490482111;
// UniRx.Subject`1<UniRx.Unit>
struct Subject_1_t201353362;
// UniRx.Subject`1<UniRx.CollectionAddEvent`1<System.Object>>
struct Subject_1_t59589311;
// UniRx.Subject`1<UniRx.CollectionMoveEvent`1<System.Object>>
struct Subject_1_t559501171;
// UniRx.Subject`1<UniRx.CollectionRemoveEvent`1<System.Object>>
struct Subject_1_t2180672344;
// UniRx.Subject`1<UniRx.CollectionReplaceEvent`1<System.Object>>
struct Subject_1_t3779785156;

#include "mscorlib_System_Collections_ObjectModel_Collection2806094150.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.ReactiveCollection`1<System.Object>
struct  ReactiveCollection_1_t2041251907  : public Collection_1_t2806094150
{
public:
	// System.Boolean UniRx.ReactiveCollection`1::isDisposed
	bool ___isDisposed_2;
	// UniRx.Subject`1<System.Int32> UniRx.ReactiveCollection`1::countChanged
	Subject_1_t490482111 * ___countChanged_3;
	// UniRx.Subject`1<UniRx.Unit> UniRx.ReactiveCollection`1::collectionReset
	Subject_1_t201353362 * ___collectionReset_4;
	// UniRx.Subject`1<UniRx.CollectionAddEvent`1<T>> UniRx.ReactiveCollection`1::collectionAdd
	Subject_1_t59589311 * ___collectionAdd_5;
	// UniRx.Subject`1<UniRx.CollectionMoveEvent`1<T>> UniRx.ReactiveCollection`1::collectionMove
	Subject_1_t559501171 * ___collectionMove_6;
	// UniRx.Subject`1<UniRx.CollectionRemoveEvent`1<T>> UniRx.ReactiveCollection`1::collectionRemove
	Subject_1_t2180672344 * ___collectionRemove_7;
	// UniRx.Subject`1<UniRx.CollectionReplaceEvent`1<T>> UniRx.ReactiveCollection`1::collectionReplace
	Subject_1_t3779785156 * ___collectionReplace_8;

public:
	inline static int32_t get_offset_of_isDisposed_2() { return static_cast<int32_t>(offsetof(ReactiveCollection_1_t2041251907, ___isDisposed_2)); }
	inline bool get_isDisposed_2() const { return ___isDisposed_2; }
	inline bool* get_address_of_isDisposed_2() { return &___isDisposed_2; }
	inline void set_isDisposed_2(bool value)
	{
		___isDisposed_2 = value;
	}

	inline static int32_t get_offset_of_countChanged_3() { return static_cast<int32_t>(offsetof(ReactiveCollection_1_t2041251907, ___countChanged_3)); }
	inline Subject_1_t490482111 * get_countChanged_3() const { return ___countChanged_3; }
	inline Subject_1_t490482111 ** get_address_of_countChanged_3() { return &___countChanged_3; }
	inline void set_countChanged_3(Subject_1_t490482111 * value)
	{
		___countChanged_3 = value;
		Il2CppCodeGenWriteBarrier(&___countChanged_3, value);
	}

	inline static int32_t get_offset_of_collectionReset_4() { return static_cast<int32_t>(offsetof(ReactiveCollection_1_t2041251907, ___collectionReset_4)); }
	inline Subject_1_t201353362 * get_collectionReset_4() const { return ___collectionReset_4; }
	inline Subject_1_t201353362 ** get_address_of_collectionReset_4() { return &___collectionReset_4; }
	inline void set_collectionReset_4(Subject_1_t201353362 * value)
	{
		___collectionReset_4 = value;
		Il2CppCodeGenWriteBarrier(&___collectionReset_4, value);
	}

	inline static int32_t get_offset_of_collectionAdd_5() { return static_cast<int32_t>(offsetof(ReactiveCollection_1_t2041251907, ___collectionAdd_5)); }
	inline Subject_1_t59589311 * get_collectionAdd_5() const { return ___collectionAdd_5; }
	inline Subject_1_t59589311 ** get_address_of_collectionAdd_5() { return &___collectionAdd_5; }
	inline void set_collectionAdd_5(Subject_1_t59589311 * value)
	{
		___collectionAdd_5 = value;
		Il2CppCodeGenWriteBarrier(&___collectionAdd_5, value);
	}

	inline static int32_t get_offset_of_collectionMove_6() { return static_cast<int32_t>(offsetof(ReactiveCollection_1_t2041251907, ___collectionMove_6)); }
	inline Subject_1_t559501171 * get_collectionMove_6() const { return ___collectionMove_6; }
	inline Subject_1_t559501171 ** get_address_of_collectionMove_6() { return &___collectionMove_6; }
	inline void set_collectionMove_6(Subject_1_t559501171 * value)
	{
		___collectionMove_6 = value;
		Il2CppCodeGenWriteBarrier(&___collectionMove_6, value);
	}

	inline static int32_t get_offset_of_collectionRemove_7() { return static_cast<int32_t>(offsetof(ReactiveCollection_1_t2041251907, ___collectionRemove_7)); }
	inline Subject_1_t2180672344 * get_collectionRemove_7() const { return ___collectionRemove_7; }
	inline Subject_1_t2180672344 ** get_address_of_collectionRemove_7() { return &___collectionRemove_7; }
	inline void set_collectionRemove_7(Subject_1_t2180672344 * value)
	{
		___collectionRemove_7 = value;
		Il2CppCodeGenWriteBarrier(&___collectionRemove_7, value);
	}

	inline static int32_t get_offset_of_collectionReplace_8() { return static_cast<int32_t>(offsetof(ReactiveCollection_1_t2041251907, ___collectionReplace_8)); }
	inline Subject_1_t3779785156 * get_collectionReplace_8() const { return ___collectionReplace_8; }
	inline Subject_1_t3779785156 ** get_address_of_collectionReplace_8() { return &___collectionReplace_8; }
	inline void set_collectionReplace_8(Subject_1_t3779785156 * value)
	{
		___collectionReplace_8 = value;
		Il2CppCodeGenWriteBarrier(&___collectionReplace_8, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
