﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// System.Collections.Generic.List`1<PlayFab.ClientModels.ItemInstance>
struct List_1_t1322103281;

#include "AssemblyU2DCSharp_PlayFab_Internal_PlayFabResultCom379512675.h"
#include "mscorlib_System_DateTime339033936.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PlayFab.ClientModels.ConfirmPurchaseResult
struct  ConfirmPurchaseResult_t633238350  : public PlayFabResultCommon_t379512675
{
public:
	// System.String PlayFab.ClientModels.ConfirmPurchaseResult::<OrderId>k__BackingField
	String_t* ___U3COrderIdU3Ek__BackingField_2;
	// System.DateTime PlayFab.ClientModels.ConfirmPurchaseResult::<PurchaseDate>k__BackingField
	DateTime_t339033936  ___U3CPurchaseDateU3Ek__BackingField_3;
	// System.Collections.Generic.List`1<PlayFab.ClientModels.ItemInstance> PlayFab.ClientModels.ConfirmPurchaseResult::<Items>k__BackingField
	List_1_t1322103281 * ___U3CItemsU3Ek__BackingField_4;

public:
	inline static int32_t get_offset_of_U3COrderIdU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(ConfirmPurchaseResult_t633238350, ___U3COrderIdU3Ek__BackingField_2)); }
	inline String_t* get_U3COrderIdU3Ek__BackingField_2() const { return ___U3COrderIdU3Ek__BackingField_2; }
	inline String_t** get_address_of_U3COrderIdU3Ek__BackingField_2() { return &___U3COrderIdU3Ek__BackingField_2; }
	inline void set_U3COrderIdU3Ek__BackingField_2(String_t* value)
	{
		___U3COrderIdU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier(&___U3COrderIdU3Ek__BackingField_2, value);
	}

	inline static int32_t get_offset_of_U3CPurchaseDateU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(ConfirmPurchaseResult_t633238350, ___U3CPurchaseDateU3Ek__BackingField_3)); }
	inline DateTime_t339033936  get_U3CPurchaseDateU3Ek__BackingField_3() const { return ___U3CPurchaseDateU3Ek__BackingField_3; }
	inline DateTime_t339033936 * get_address_of_U3CPurchaseDateU3Ek__BackingField_3() { return &___U3CPurchaseDateU3Ek__BackingField_3; }
	inline void set_U3CPurchaseDateU3Ek__BackingField_3(DateTime_t339033936  value)
	{
		___U3CPurchaseDateU3Ek__BackingField_3 = value;
	}

	inline static int32_t get_offset_of_U3CItemsU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(ConfirmPurchaseResult_t633238350, ___U3CItemsU3Ek__BackingField_4)); }
	inline List_1_t1322103281 * get_U3CItemsU3Ek__BackingField_4() const { return ___U3CItemsU3Ek__BackingField_4; }
	inline List_1_t1322103281 ** get_address_of_U3CItemsU3Ek__BackingField_4() { return &___U3CItemsU3Ek__BackingField_4; }
	inline void set_U3CItemsU3Ek__BackingField_4(List_1_t1322103281 * value)
	{
		___U3CItemsU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier(&___U3CItemsU3Ek__BackingField_4, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
