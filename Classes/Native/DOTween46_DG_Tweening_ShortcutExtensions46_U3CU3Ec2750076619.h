﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.RectTransform
struct RectTransform_t3317474837;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.ShortcutExtensions46/<>c__DisplayClass14_0
struct  U3CU3Ec__DisplayClass14_0_t2750076620  : public Il2CppObject
{
public:
	// UnityEngine.RectTransform DG.Tweening.ShortcutExtensions46/<>c__DisplayClass14_0::target
	RectTransform_t3317474837 * ___target_0;

public:
	inline static int32_t get_offset_of_target_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass14_0_t2750076620, ___target_0)); }
	inline RectTransform_t3317474837 * get_target_0() const { return ___target_0; }
	inline RectTransform_t3317474837 ** get_address_of_target_0() { return &___target_0; }
	inline void set_target_0(RectTransform_t3317474837 * value)
	{
		___target_0 = value;
		Il2CppCodeGenWriteBarrier(&___target_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
