﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Zenject.IFactoryBinder`5/<ToMethod>c__AnonStorey17C<System.Object,System.Object,System.Object,System.Object,System.Object>
struct U3CToMethodU3Ec__AnonStorey17C_t3805965893;
// Zenject.IFactory`5<System.Object,System.Object,System.Object,System.Object,System.Object>
struct IFactory_5_t363284204;
// Zenject.InjectContext
struct InjectContext_t3456483891;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Zenject_InjectContext3456483891.h"

// System.Void Zenject.IFactoryBinder`5/<ToMethod>c__AnonStorey17C<System.Object,System.Object,System.Object,System.Object,System.Object>::.ctor()
extern "C"  void U3CToMethodU3Ec__AnonStorey17C__ctor_m1814593907_gshared (U3CToMethodU3Ec__AnonStorey17C_t3805965893 * __this, const MethodInfo* method);
#define U3CToMethodU3Ec__AnonStorey17C__ctor_m1814593907(__this, method) ((  void (*) (U3CToMethodU3Ec__AnonStorey17C_t3805965893 *, const MethodInfo*))U3CToMethodU3Ec__AnonStorey17C__ctor_m1814593907_gshared)(__this, method)
// Zenject.IFactory`5<TParam1,TParam2,TParam3,TParam4,TContract> Zenject.IFactoryBinder`5/<ToMethod>c__AnonStorey17C<System.Object,System.Object,System.Object,System.Object,System.Object>::<>m__2A4(Zenject.InjectContext)
extern "C"  Il2CppObject* U3CToMethodU3Ec__AnonStorey17C_U3CU3Em__2A4_m2349341694_gshared (U3CToMethodU3Ec__AnonStorey17C_t3805965893 * __this, InjectContext_t3456483891 * ___ctx0, const MethodInfo* method);
#define U3CToMethodU3Ec__AnonStorey17C_U3CU3Em__2A4_m2349341694(__this, ___ctx0, method) ((  Il2CppObject* (*) (U3CToMethodU3Ec__AnonStorey17C_t3805965893 *, InjectContext_t3456483891 *, const MethodInfo*))U3CToMethodU3Ec__AnonStorey17C_U3CU3Em__2A4_m2349341694_gshared)(__this, ___ctx0, method)
