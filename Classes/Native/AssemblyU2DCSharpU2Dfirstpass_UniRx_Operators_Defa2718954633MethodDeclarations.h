﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.DefaultIfEmptyObservable`1<System.Object>
struct DefaultIfEmptyObservable_1_t2718954633;
// UniRx.IObservable`1<System.Object>
struct IObservable_1_t595904784;
// System.Object
struct Il2CppObject;
// System.IDisposable
struct IDisposable_t1628921374;
// UniRx.IObserver`1<System.Object>
struct IObserver_1_t3049105323;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"

// System.Void UniRx.Operators.DefaultIfEmptyObservable`1<System.Object>::.ctor(UniRx.IObservable`1<T>,T)
extern "C"  void DefaultIfEmptyObservable_1__ctor_m3746922412_gshared (DefaultIfEmptyObservable_1_t2718954633 * __this, Il2CppObject* ___source0, Il2CppObject * ___defaultValue1, const MethodInfo* method);
#define DefaultIfEmptyObservable_1__ctor_m3746922412(__this, ___source0, ___defaultValue1, method) ((  void (*) (DefaultIfEmptyObservable_1_t2718954633 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))DefaultIfEmptyObservable_1__ctor_m3746922412_gshared)(__this, ___source0, ___defaultValue1, method)
// System.IDisposable UniRx.Operators.DefaultIfEmptyObservable`1<System.Object>::SubscribeCore(UniRx.IObserver`1<T>,System.IDisposable)
extern "C"  Il2CppObject * DefaultIfEmptyObservable_1_SubscribeCore_m3634043251_gshared (DefaultIfEmptyObservable_1_t2718954633 * __this, Il2CppObject* ___observer0, Il2CppObject * ___cancel1, const MethodInfo* method);
#define DefaultIfEmptyObservable_1_SubscribeCore_m3634043251(__this, ___observer0, ___cancel1, method) ((  Il2CppObject * (*) (DefaultIfEmptyObservable_1_t2718954633 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))DefaultIfEmptyObservable_1_SubscribeCore_m3634043251_gshared)(__this, ___observer0, ___cancel1, method)
