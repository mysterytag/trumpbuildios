﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.InternalUtil.EmptyObserver`1<UniRx.Unit>
struct EmptyObserver_1_t1967639028;
// System.Exception
struct Exception_t1967233988;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Exception1967233988.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Unit2558286038.h"

// System.Void UniRx.InternalUtil.EmptyObserver`1<UniRx.Unit>::.ctor()
extern "C"  void EmptyObserver_1__ctor_m1910795276_gshared (EmptyObserver_1_t1967639028 * __this, const MethodInfo* method);
#define EmptyObserver_1__ctor_m1910795276(__this, method) ((  void (*) (EmptyObserver_1_t1967639028 *, const MethodInfo*))EmptyObserver_1__ctor_m1910795276_gshared)(__this, method)
// System.Void UniRx.InternalUtil.EmptyObserver`1<UniRx.Unit>::.cctor()
extern "C"  void EmptyObserver_1__cctor_m2917982497_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define EmptyObserver_1__cctor_m2917982497(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))EmptyObserver_1__cctor_m2917982497_gshared)(__this /* static, unused */, method)
// System.Void UniRx.InternalUtil.EmptyObserver`1<UniRx.Unit>::OnCompleted()
extern "C"  void EmptyObserver_1_OnCompleted_m2207868566_gshared (EmptyObserver_1_t1967639028 * __this, const MethodInfo* method);
#define EmptyObserver_1_OnCompleted_m2207868566(__this, method) ((  void (*) (EmptyObserver_1_t1967639028 *, const MethodInfo*))EmptyObserver_1_OnCompleted_m2207868566_gshared)(__this, method)
// System.Void UniRx.InternalUtil.EmptyObserver`1<UniRx.Unit>::OnError(System.Exception)
extern "C"  void EmptyObserver_1_OnError_m4162905795_gshared (EmptyObserver_1_t1967639028 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method);
#define EmptyObserver_1_OnError_m4162905795(__this, ___error0, method) ((  void (*) (EmptyObserver_1_t1967639028 *, Exception_t1967233988 *, const MethodInfo*))EmptyObserver_1_OnError_m4162905795_gshared)(__this, ___error0, method)
// System.Void UniRx.InternalUtil.EmptyObserver`1<UniRx.Unit>::OnNext(T)
extern "C"  void EmptyObserver_1_OnNext_m858960820_gshared (EmptyObserver_1_t1967639028 * __this, Unit_t2558286038  ___value0, const MethodInfo* method);
#define EmptyObserver_1_OnNext_m858960820(__this, ___value0, method) ((  void (*) (EmptyObserver_1_t1967639028 *, Unit_t2558286038 , const MethodInfo*))EmptyObserver_1_OnNext_m858960820_gshared)(__this, ___value0, method)
