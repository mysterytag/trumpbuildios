﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Func`1<UniRx.IObservable`1<System.Object>>
struct Func_1_t1738686031;

#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Oper4196218687.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Operators.DeferObservable`1<System.Object>
struct  DeferObservable_1_t3089955988  : public OperatorObservableBase_1_t4196218687
{
public:
	// System.Func`1<UniRx.IObservable`1<T>> UniRx.Operators.DeferObservable`1::observableFactory
	Func_1_t1738686031 * ___observableFactory_1;

public:
	inline static int32_t get_offset_of_observableFactory_1() { return static_cast<int32_t>(offsetof(DeferObservable_1_t3089955988, ___observableFactory_1)); }
	inline Func_1_t1738686031 * get_observableFactory_1() const { return ___observableFactory_1; }
	inline Func_1_t1738686031 ** get_address_of_observableFactory_1() { return &___observableFactory_1; }
	inline void set_observableFactory_1(Func_1_t1738686031 * value)
	{
		___observableFactory_1 = value;
		Il2CppCodeGenWriteBarrier(&___observableFactory_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
