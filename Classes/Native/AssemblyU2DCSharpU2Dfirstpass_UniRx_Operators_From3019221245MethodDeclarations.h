﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.FromEventPatternObservable`2<System.Object,System.Object>
struct FromEventPatternObservable_2_t3019221245;
// System.Func`2<System.EventHandler`1<System.Object>,System.Object>
struct Func_2_t2937104577;
// System.Action`1<System.Object>
struct Action_1_t985559125;
// System.IDisposable
struct IDisposable_t1628921374;
// UniRx.IObserver`1<UniRx.EventPattern`1<System.Object>>
struct IObserver_1_t820035585;

#include "codegen/il2cpp-codegen.h"

// System.Void UniRx.Operators.FromEventPatternObservable`2<System.Object,System.Object>::.ctor(System.Func`2<System.EventHandler`1<TEventArgs>,TDelegate>,System.Action`1<TDelegate>,System.Action`1<TDelegate>)
extern "C"  void FromEventPatternObservable_2__ctor_m4058073472_gshared (FromEventPatternObservable_2_t3019221245 * __this, Func_2_t2937104577 * ___conversion0, Action_1_t985559125 * ___addHandler1, Action_1_t985559125 * ___removeHandler2, const MethodInfo* method);
#define FromEventPatternObservable_2__ctor_m4058073472(__this, ___conversion0, ___addHandler1, ___removeHandler2, method) ((  void (*) (FromEventPatternObservable_2_t3019221245 *, Func_2_t2937104577 *, Action_1_t985559125 *, Action_1_t985559125 *, const MethodInfo*))FromEventPatternObservable_2__ctor_m4058073472_gshared)(__this, ___conversion0, ___addHandler1, ___removeHandler2, method)
// System.IDisposable UniRx.Operators.FromEventPatternObservable`2<System.Object,System.Object>::SubscribeCore(UniRx.IObserver`1<UniRx.EventPattern`1<TEventArgs>>,System.IDisposable)
extern "C"  Il2CppObject * FromEventPatternObservable_2_SubscribeCore_m1451785217_gshared (FromEventPatternObservable_2_t3019221245 * __this, Il2CppObject* ___observer0, Il2CppObject * ___cancel1, const MethodInfo* method);
#define FromEventPatternObservable_2_SubscribeCore_m1451785217(__this, ___observer0, ___cancel1, method) ((  Il2CppObject * (*) (FromEventPatternObservable_2_t3019221245 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))FromEventPatternObservable_2_SubscribeCore_m1451785217_gshared)(__this, ___observer0, ___cancel1, method)
