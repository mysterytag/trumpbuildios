﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UniRx.InternalUtil.PriorityQueue`1<UniRx.InternalUtil.ScheduledItem>
struct PriorityQueue_1_t2411179237;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.InternalUtil.SchedulerQueue
struct  SchedulerQueue_t3710535235  : public Il2CppObject
{
public:
	// UniRx.InternalUtil.PriorityQueue`1<UniRx.InternalUtil.ScheduledItem> UniRx.InternalUtil.SchedulerQueue::_queue
	PriorityQueue_1_t2411179237 * ____queue_0;

public:
	inline static int32_t get_offset_of__queue_0() { return static_cast<int32_t>(offsetof(SchedulerQueue_t3710535235, ____queue_0)); }
	inline PriorityQueue_1_t2411179237 * get__queue_0() const { return ____queue_0; }
	inline PriorityQueue_1_t2411179237 ** get_address_of__queue_0() { return &____queue_0; }
	inline void set__queue_0(PriorityQueue_1_t2411179237 * value)
	{
		____queue_0 = value;
		Il2CppCodeGenWriteBarrier(&____queue_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
