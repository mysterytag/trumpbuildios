﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.ClientModels.GetPlayFabIDsFromFacebookIDsRequest
struct GetPlayFabIDsFromFacebookIDsRequest_t270576050;
// System.Collections.Generic.List`1<System.String>
struct List_1_t1765447871;

#include "codegen/il2cpp-codegen.h"

// System.Void PlayFab.ClientModels.GetPlayFabIDsFromFacebookIDsRequest::.ctor()
extern "C"  void GetPlayFabIDsFromFacebookIDsRequest__ctor_m2985717559 (GetPlayFabIDsFromFacebookIDsRequest_t270576050 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<System.String> PlayFab.ClientModels.GetPlayFabIDsFromFacebookIDsRequest::get_FacebookIDs()
extern "C"  List_1_t1765447871 * GetPlayFabIDsFromFacebookIDsRequest_get_FacebookIDs_m2155500877 (GetPlayFabIDsFromFacebookIDsRequest_t270576050 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.GetPlayFabIDsFromFacebookIDsRequest::set_FacebookIDs(System.Collections.Generic.List`1<System.String>)
extern "C"  void GetPlayFabIDsFromFacebookIDsRequest_set_FacebookIDs_m1677113374 (GetPlayFabIDsFromFacebookIDsRequest_t270576050 * __this, List_1_t1765447871 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
