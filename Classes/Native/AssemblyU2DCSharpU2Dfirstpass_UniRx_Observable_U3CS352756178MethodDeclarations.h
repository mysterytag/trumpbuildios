﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Observable/<SelectMany>c__AnonStorey4D`1<System.Object>
struct U3CSelectManyU3Ec__AnonStorey4D_1_t352756178;
// System.Collections.IEnumerator
struct IEnumerator_t287207039;

#include "codegen/il2cpp-codegen.h"

// System.Void UniRx.Observable/<SelectMany>c__AnonStorey4D`1<System.Object>::.ctor()
extern "C"  void U3CSelectManyU3Ec__AnonStorey4D_1__ctor_m3157912424_gshared (U3CSelectManyU3Ec__AnonStorey4D_1_t352756178 * __this, const MethodInfo* method);
#define U3CSelectManyU3Ec__AnonStorey4D_1__ctor_m3157912424(__this, method) ((  void (*) (U3CSelectManyU3Ec__AnonStorey4D_1_t352756178 *, const MethodInfo*))U3CSelectManyU3Ec__AnonStorey4D_1__ctor_m3157912424_gshared)(__this, method)
// System.Collections.IEnumerator UniRx.Observable/<SelectMany>c__AnonStorey4D`1<System.Object>::<>m__4B()
extern "C"  Il2CppObject * U3CSelectManyU3Ec__AnonStorey4D_1_U3CU3Em__4B_m3947555951_gshared (U3CSelectManyU3Ec__AnonStorey4D_1_t352756178 * __this, const MethodInfo* method);
#define U3CSelectManyU3Ec__AnonStorey4D_1_U3CU3Em__4B_m3947555951(__this, method) ((  Il2CppObject * (*) (U3CSelectManyU3Ec__AnonStorey4D_1_t352756178 *, const MethodInfo*))U3CSelectManyU3Ec__AnonStorey4D_1_U3CU3Em__4B_m3947555951_gshared)(__this, method)
