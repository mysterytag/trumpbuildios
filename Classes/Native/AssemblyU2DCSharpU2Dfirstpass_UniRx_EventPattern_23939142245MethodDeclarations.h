﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.EventPattern`2<System.Object,System.Object>
struct EventPattern_2_t3939142245;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"

// System.Void UniRx.EventPattern`2<System.Object,System.Object>::.ctor(TSender,TEventArgs)
extern "C"  void EventPattern_2__ctor_m3419812743_gshared (EventPattern_2_t3939142245 * __this, Il2CppObject * ___sender0, Il2CppObject * ___e1, const MethodInfo* method);
#define EventPattern_2__ctor_m3419812743(__this, ___sender0, ___e1, method) ((  void (*) (EventPattern_2_t3939142245 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))EventPattern_2__ctor_m3419812743_gshared)(__this, ___sender0, ___e1, method)
// TSender UniRx.EventPattern`2<System.Object,System.Object>::get_Sender()
extern "C"  Il2CppObject * EventPattern_2_get_Sender_m2972234443_gshared (EventPattern_2_t3939142245 * __this, const MethodInfo* method);
#define EventPattern_2_get_Sender_m2972234443(__this, method) ((  Il2CppObject * (*) (EventPattern_2_t3939142245 *, const MethodInfo*))EventPattern_2_get_Sender_m2972234443_gshared)(__this, method)
// System.Void UniRx.EventPattern`2<System.Object,System.Object>::set_Sender(TSender)
extern "C"  void EventPattern_2_set_Sender_m3333931200_gshared (EventPattern_2_t3939142245 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define EventPattern_2_set_Sender_m3333931200(__this, ___value0, method) ((  void (*) (EventPattern_2_t3939142245 *, Il2CppObject *, const MethodInfo*))EventPattern_2_set_Sender_m3333931200_gshared)(__this, ___value0, method)
// TEventArgs UniRx.EventPattern`2<System.Object,System.Object>::get_EventArgs()
extern "C"  Il2CppObject * EventPattern_2_get_EventArgs_m3420313211_gshared (EventPattern_2_t3939142245 * __this, const MethodInfo* method);
#define EventPattern_2_get_EventArgs_m3420313211(__this, method) ((  Il2CppObject * (*) (EventPattern_2_t3939142245 *, const MethodInfo*))EventPattern_2_get_EventArgs_m3420313211_gshared)(__this, method)
// System.Void UniRx.EventPattern`2<System.Object,System.Object>::set_EventArgs(TEventArgs)
extern "C"  void EventPattern_2_set_EventArgs_m1298775714_gshared (EventPattern_2_t3939142245 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define EventPattern_2_set_EventArgs_m1298775714(__this, ___value0, method) ((  void (*) (EventPattern_2_t3939142245 *, Il2CppObject *, const MethodInfo*))EventPattern_2_set_EventArgs_m1298775714_gshared)(__this, ___value0, method)
// System.Boolean UniRx.EventPattern`2<System.Object,System.Object>::Equals(UniRx.EventPattern`2<TSender,TEventArgs>)
extern "C"  bool EventPattern_2_Equals_m2393598654_gshared (EventPattern_2_t3939142245 * __this, EventPattern_2_t3939142245 * ___other0, const MethodInfo* method);
#define EventPattern_2_Equals_m2393598654(__this, ___other0, method) ((  bool (*) (EventPattern_2_t3939142245 *, EventPattern_2_t3939142245 *, const MethodInfo*))EventPattern_2_Equals_m2393598654_gshared)(__this, ___other0, method)
// System.Boolean UniRx.EventPattern`2<System.Object,System.Object>::Equals(System.Object)
extern "C"  bool EventPattern_2_Equals_m4062083866_gshared (EventPattern_2_t3939142245 * __this, Il2CppObject * ___obj0, const MethodInfo* method);
#define EventPattern_2_Equals_m4062083866(__this, ___obj0, method) ((  bool (*) (EventPattern_2_t3939142245 *, Il2CppObject *, const MethodInfo*))EventPattern_2_Equals_m4062083866_gshared)(__this, ___obj0, method)
// System.Int32 UniRx.EventPattern`2<System.Object,System.Object>::GetHashCode()
extern "C"  int32_t EventPattern_2_GetHashCode_m2584518450_gshared (EventPattern_2_t3939142245 * __this, const MethodInfo* method);
#define EventPattern_2_GetHashCode_m2584518450(__this, method) ((  int32_t (*) (EventPattern_2_t3939142245 *, const MethodInfo*))EventPattern_2_GetHashCode_m2584518450_gshared)(__this, method)
// System.Boolean UniRx.EventPattern`2<System.Object,System.Object>::op_Equality(UniRx.EventPattern`2<TSender,TEventArgs>,UniRx.EventPattern`2<TSender,TEventArgs>)
extern "C"  bool EventPattern_2_op_Equality_m1906571143_gshared (Il2CppObject * __this /* static, unused */, EventPattern_2_t3939142245 * ___first0, EventPattern_2_t3939142245 * ___second1, const MethodInfo* method);
#define EventPattern_2_op_Equality_m1906571143(__this /* static, unused */, ___first0, ___second1, method) ((  bool (*) (Il2CppObject * /* static, unused */, EventPattern_2_t3939142245 *, EventPattern_2_t3939142245 *, const MethodInfo*))EventPattern_2_op_Equality_m1906571143_gshared)(__this /* static, unused */, ___first0, ___second1, method)
// System.Boolean UniRx.EventPattern`2<System.Object,System.Object>::op_Inequality(UniRx.EventPattern`2<TSender,TEventArgs>,UniRx.EventPattern`2<TSender,TEventArgs>)
extern "C"  bool EventPattern_2_op_Inequality_m3770633282_gshared (Il2CppObject * __this /* static, unused */, EventPattern_2_t3939142245 * ___first0, EventPattern_2_t3939142245 * ___second1, const MethodInfo* method);
#define EventPattern_2_op_Inequality_m3770633282(__this /* static, unused */, ___first0, ___second1, method) ((  bool (*) (Il2CppObject * /* static, unused */, EventPattern_2_t3939142245 *, EventPattern_2_t3939142245 *, const MethodInfo*))EventPattern_2_op_Inequality_m3770633282_gshared)(__this /* static, unused */, ___first0, ___second1, method)
