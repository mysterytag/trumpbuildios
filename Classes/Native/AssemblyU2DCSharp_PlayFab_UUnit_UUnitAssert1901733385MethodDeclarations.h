﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.String
struct String_t;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String968488902.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_Nullable_1_gen1446416676.h"
#include "mscorlib_System_Nullable_1_gen1369764433.h"
#include "mscorlib_System_Nullable_1_gen1438485341.h"
#include "mscorlib_System_Nullable_1_gen3871963176.h"
#include "mscorlib_System_Nullable_1_gen1438485399.h"
#include "mscorlib_System_Nullable_1_gen3871963234.h"
#include "mscorlib_System_Nullable_1_gen1438485494.h"
#include "mscorlib_System_Nullable_1_gen3871963329.h"
#include "mscorlib_System_Nullable_1_gen3844246929.h"
#include "mscorlib_System_Nullable_1_gen3420554522.h"

// System.Void PlayFab.UUnit.UUnitAssert::Skip()
extern "C"  void UUnitAssert_Skip_m783956039 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.UUnit.UUnitAssert::Fail(System.String)
extern "C"  void UUnitAssert_Fail_m1759776412 (Il2CppObject * __this /* static, unused */, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.UUnit.UUnitAssert::True(System.Boolean,System.String)
extern "C"  void UUnitAssert_True_m480535817 (Il2CppObject * __this /* static, unused */, bool ___boolean0, String_t* ___message1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.UUnit.UUnitAssert::False(System.Boolean,System.String)
extern "C"  void UUnitAssert_False_m383849648 (Il2CppObject * __this /* static, unused */, bool ___boolean0, String_t* ___message1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.UUnit.UUnitAssert::NotNull(System.Object,System.String)
extern "C"  void UUnitAssert_NotNull_m2302581692 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___something0, String_t* ___message1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.UUnit.UUnitAssert::IsNull(System.Object,System.String)
extern "C"  void UUnitAssert_IsNull_m3878880471 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___something0, String_t* ___message1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.UUnit.UUnitAssert::StringEquals(System.String,System.String,System.String)
extern "C"  void UUnitAssert_StringEquals_m1005068226 (Il2CppObject * __this /* static, unused */, String_t* ___wanted0, String_t* ___got1, String_t* ___message2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.UUnit.UUnitAssert::SbyteEquals(System.Nullable`1<System.SByte>,System.Nullable`1<System.SByte>,System.String)
extern "C"  void UUnitAssert_SbyteEquals_m2847154680 (Il2CppObject * __this /* static, unused */, Nullable_1_t1446416676  ___wanted0, Nullable_1_t1446416676  ___got1, String_t* ___message2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.UUnit.UUnitAssert::ByteEquals(System.Nullable`1<System.Byte>,System.Nullable`1<System.Byte>,System.String)
extern "C"  void UUnitAssert_ByteEquals_m1957062195 (Il2CppObject * __this /* static, unused */, Nullable_1_t1369764433  ___wanted0, Nullable_1_t1369764433  ___got1, String_t* ___message2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.UUnit.UUnitAssert::ShortEquals(System.Nullable`1<System.Int16>,System.Nullable`1<System.Int16>,System.String)
extern "C"  void UUnitAssert_ShortEquals_m1835814025 (Il2CppObject * __this /* static, unused */, Nullable_1_t1438485341  ___wanted0, Nullable_1_t1438485341  ___got1, String_t* ___message2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.UUnit.UUnitAssert::UshortEquals(System.Nullable`1<System.UInt16>,System.Nullable`1<System.UInt16>,System.String)
extern "C"  void UUnitAssert_UshortEquals_m3185254740 (Il2CppObject * __this /* static, unused */, Nullable_1_t3871963176  ___wanted0, Nullable_1_t3871963176  ___got1, String_t* ___message2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.UUnit.UUnitAssert::IntEquals(System.Nullable`1<System.Int32>,System.Nullable`1<System.Int32>,System.String)
extern "C"  void UUnitAssert_IntEquals_m3501624618 (Il2CppObject * __this /* static, unused */, Nullable_1_t1438485399  ___wanted0, Nullable_1_t1438485399  ___got1, String_t* ___message2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.UUnit.UUnitAssert::UintEquals(System.Nullable`1<System.UInt32>,System.Nullable`1<System.UInt32>,System.String)
extern "C"  void UUnitAssert_UintEquals_m1141436225 (Il2CppObject * __this /* static, unused */, Nullable_1_t3871963234  ___wanted0, Nullable_1_t3871963234  ___got1, String_t* ___message2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.UUnit.UUnitAssert::LongEquals(System.Nullable`1<System.Int64>,System.Nullable`1<System.Int64>,System.String)
extern "C"  void UUnitAssert_LongEquals_m1336379085 (Il2CppObject * __this /* static, unused */, Nullable_1_t1438485494  ___wanted0, Nullable_1_t1438485494  ___got1, String_t* ___message2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.UUnit.UUnitAssert::ULongEquals(System.Nullable`1<System.UInt64>,System.Nullable`1<System.UInt64>,System.String)
extern "C"  void UUnitAssert_ULongEquals_m4061628792 (Il2CppObject * __this /* static, unused */, Nullable_1_t3871963329  ___wanted0, Nullable_1_t3871963329  ___got1, String_t* ___message2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.UUnit.UUnitAssert::FloatEquals(System.Nullable`1<System.Single>,System.Nullable`1<System.Single>,System.Single,System.String)
extern "C"  void UUnitAssert_FloatEquals_m1879024050 (Il2CppObject * __this /* static, unused */, Nullable_1_t3844246929  ___wanted0, Nullable_1_t3844246929  ___got1, float ___precision2, String_t* ___message3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.UUnit.UUnitAssert::DoubleEquals(System.Nullable`1<System.Double>,System.Nullable`1<System.Double>,System.Double,System.String)
extern "C"  void UUnitAssert_DoubleEquals_m1454367910 (Il2CppObject * __this /* static, unused */, Nullable_1_t3420554522  ___wanted0, Nullable_1_t3420554522  ___got1, double ___precision2, String_t* ___message3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.UUnit.UUnitAssert::ObjEquals(System.Object,System.Object,System.String)
extern "C"  void UUnitAssert_ObjEquals_m1586779694 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___wanted0, Il2CppObject * ___got1, String_t* ___message2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
