﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Examples.Sample07_OrchestratIEnumerator
struct Sample07_OrchestratIEnumerator_t3702211539;
// System.Collections.IEnumerator
struct IEnumerator_t287207039;

#include "codegen/il2cpp-codegen.h"

// System.Void UniRx.Examples.Sample07_OrchestratIEnumerator::.ctor()
extern "C"  void Sample07_OrchestratIEnumerator__ctor_m2899177426 (Sample07_OrchestratIEnumerator_t3702211539 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator UniRx.Examples.Sample07_OrchestratIEnumerator::AsyncA()
extern "C"  Il2CppObject * Sample07_OrchestratIEnumerator_AsyncA_m3572713415 (Sample07_OrchestratIEnumerator_t3702211539 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator UniRx.Examples.Sample07_OrchestratIEnumerator::AsyncB()
extern "C"  Il2CppObject * Sample07_OrchestratIEnumerator_AsyncB_m3572714376 (Sample07_OrchestratIEnumerator_t3702211539 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.Examples.Sample07_OrchestratIEnumerator::Start()
extern "C"  void Sample07_OrchestratIEnumerator_Start_m1846315218 (Sample07_OrchestratIEnumerator_t3702211539 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
