﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UniRx.Operators.SingleObservable`1<System.Object>
struct SingleObservable_1_t427280954;
// System.Object
struct Il2CppObject;

#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Oper1187768149.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Operators.SingleObservable`1/Single_<System.Object>
struct  Single__t984311384  : public OperatorObserverBase_2_t1187768149
{
public:
	// UniRx.Operators.SingleObservable`1<T> UniRx.Operators.SingleObservable`1/Single_::parent
	SingleObservable_1_t427280954 * ___parent_2;
	// System.Boolean UniRx.Operators.SingleObservable`1/Single_::seenValue
	bool ___seenValue_3;
	// T UniRx.Operators.SingleObservable`1/Single_::lastValue
	Il2CppObject * ___lastValue_4;

public:
	inline static int32_t get_offset_of_parent_2() { return static_cast<int32_t>(offsetof(Single__t984311384, ___parent_2)); }
	inline SingleObservable_1_t427280954 * get_parent_2() const { return ___parent_2; }
	inline SingleObservable_1_t427280954 ** get_address_of_parent_2() { return &___parent_2; }
	inline void set_parent_2(SingleObservable_1_t427280954 * value)
	{
		___parent_2 = value;
		Il2CppCodeGenWriteBarrier(&___parent_2, value);
	}

	inline static int32_t get_offset_of_seenValue_3() { return static_cast<int32_t>(offsetof(Single__t984311384, ___seenValue_3)); }
	inline bool get_seenValue_3() const { return ___seenValue_3; }
	inline bool* get_address_of_seenValue_3() { return &___seenValue_3; }
	inline void set_seenValue_3(bool value)
	{
		___seenValue_3 = value;
	}

	inline static int32_t get_offset_of_lastValue_4() { return static_cast<int32_t>(offsetof(Single__t984311384, ___lastValue_4)); }
	inline Il2CppObject * get_lastValue_4() const { return ___lastValue_4; }
	inline Il2CppObject ** get_address_of_lastValue_4() { return &___lastValue_4; }
	inline void set_lastValue_4(Il2CppObject * value)
	{
		___lastValue_4 = value;
		Il2CppCodeGenWriteBarrier(&___lastValue_4, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
