﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// DateTimeCell
struct DateTimeCell_t773798845;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_DateTime339033936.h"

// System.Void DateTimeCell::.ctor(System.DateTime)
extern "C"  void DateTimeCell__ctor_m2201480954 (DateTimeCell_t773798845 * __this, DateTime_t339033936  ___val0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DateTimeCell::.ctor()
extern "C"  void DateTimeCell__ctor_m332635006 (DateTimeCell_t773798845 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
