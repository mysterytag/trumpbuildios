﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.InternalUtil.ThrowObserver`1<System.Int32>
struct ThrowObserver_1_t3499376402;
// System.Exception
struct Exception_t1967233988;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Exception1967233988.h"

// System.Void UniRx.InternalUtil.ThrowObserver`1<System.Int32>::.ctor()
extern "C"  void ThrowObserver_1__ctor_m255404998_gshared (ThrowObserver_1_t3499376402 * __this, const MethodInfo* method);
#define ThrowObserver_1__ctor_m255404998(__this, method) ((  void (*) (ThrowObserver_1_t3499376402 *, const MethodInfo*))ThrowObserver_1__ctor_m255404998_gshared)(__this, method)
// System.Void UniRx.InternalUtil.ThrowObserver`1<System.Int32>::.cctor()
extern "C"  void ThrowObserver_1__cctor_m3140491431_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define ThrowObserver_1__cctor_m3140491431(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))ThrowObserver_1__cctor_m3140491431_gshared)(__this /* static, unused */, method)
// System.Void UniRx.InternalUtil.ThrowObserver`1<System.Int32>::OnCompleted()
extern "C"  void ThrowObserver_1_OnCompleted_m124383952_gshared (ThrowObserver_1_t3499376402 * __this, const MethodInfo* method);
#define ThrowObserver_1_OnCompleted_m124383952(__this, method) ((  void (*) (ThrowObserver_1_t3499376402 *, const MethodInfo*))ThrowObserver_1_OnCompleted_m124383952_gshared)(__this, method)
// System.Void UniRx.InternalUtil.ThrowObserver`1<System.Int32>::OnError(System.Exception)
extern "C"  void ThrowObserver_1_OnError_m4113492989_gshared (ThrowObserver_1_t3499376402 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method);
#define ThrowObserver_1_OnError_m4113492989(__this, ___error0, method) ((  void (*) (ThrowObserver_1_t3499376402 *, Exception_t1967233988 *, const MethodInfo*))ThrowObserver_1_OnError_m4113492989_gshared)(__this, ___error0, method)
// System.Void UniRx.InternalUtil.ThrowObserver`1<System.Int32>::OnNext(T)
extern "C"  void ThrowObserver_1_OnNext_m3461770478_gshared (ThrowObserver_1_t3499376402 * __this, int32_t ___value0, const MethodInfo* method);
#define ThrowObserver_1_OnNext_m3461770478(__this, ___value0, method) ((  void (*) (ThrowObserver_1_t3499376402 *, int32_t, const MethodInfo*))ThrowObserver_1_OnNext_m3461770478_gshared)(__this, ___value0, method)
