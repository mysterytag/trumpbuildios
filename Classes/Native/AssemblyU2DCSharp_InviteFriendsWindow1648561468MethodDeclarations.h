﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// InviteFriendsWindow
struct InviteFriendsWindow_t1648561468;
// System.Collections.Generic.List`1<FriendInfo>
struct List_1_t1033429381;
// FriendInfo
struct FriendInfo_t236470412;
// FriendView
struct FriendView_t236852867;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_FriendInfo236470412.h"
#include "AssemblyU2DCSharp_FriendView236852867.h"

// System.Void InviteFriendsWindow::.ctor()
extern "C"  void InviteFriendsWindow__ctor_m389032367 (InviteFriendsWindow_t1648561468 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void InviteFriendsWindow::Start()
extern "C"  void InviteFriendsWindow_Start_m3631137455 (InviteFriendsWindow_t1648561468 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void InviteFriendsWindow::Init(System.Collections.Generic.List`1<FriendInfo>)
extern "C"  void InviteFriendsWindow_Init_m1797426521 (InviteFriendsWindow_t1648561468 * __this, List_1_t1033429381 * ___friends0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void InviteFriendsWindow::<Start>m__119(FriendInfo,FriendView)
extern "C"  void InviteFriendsWindow_U3CStartU3Em__119_m1558232254 (InviteFriendsWindow_t1648561468 * __this, FriendInfo_t236470412 * ___info0, FriendView_t236852867 * ___view1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
