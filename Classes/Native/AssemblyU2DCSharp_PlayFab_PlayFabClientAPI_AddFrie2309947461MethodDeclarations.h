﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.PlayFabClientAPI/AddFriendResponseCallback
struct AddFriendResponseCallback_t2309947461;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;
// PlayFab.ClientModels.AddFriendRequest
struct AddFriendRequest_t1861404448;
// PlayFab.ClientModels.AddFriendResult
struct AddFriendResult_t4251613068;
// PlayFab.PlayFabError
struct PlayFabError_t750598646;
// System.IAsyncResult
struct IAsyncResult_t537683269;
// System.AsyncCallback
struct AsyncCallback_t1363551830;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_IntPtr676692020.h"
#include "mscorlib_System_String968488902.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_AddFriendRe1861404448.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_AddFriendRe4251613068.h"
#include "AssemblyU2DCSharpU2Dfirstpass_PlayFab_PlayFabError750598646.h"
#include "mscorlib_System_AsyncCallback1363551830.h"

// System.Void PlayFab.PlayFabClientAPI/AddFriendResponseCallback::.ctor(System.Object,System.IntPtr)
extern "C"  void AddFriendResponseCallback__ctor_m337308244 (AddFriendResponseCallback_t2309947461 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/AddFriendResponseCallback::Invoke(System.String,System.Int32,PlayFab.ClientModels.AddFriendRequest,PlayFab.ClientModels.AddFriendResult,PlayFab.PlayFabError,System.Object)
extern "C"  void AddFriendResponseCallback_Invoke_m1578617917 (AddFriendResponseCallback_t2309947461 * __this, String_t* ___urlPath0, int32_t ___callId1, AddFriendRequest_t1861404448 * ___request2, AddFriendResult_t4251613068 * ___result3, PlayFabError_t750598646 * ___error4, Il2CppObject * ___customData5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
extern "C" void pinvoke_delegate_wrapper_AddFriendResponseCallback_t2309947461(Il2CppObject* delegate, String_t* ___urlPath0, int32_t ___callId1, AddFriendRequest_t1861404448 * ___request2, AddFriendResult_t4251613068 * ___result3, PlayFabError_t750598646 * ___error4, Il2CppObject * ___customData5);
// System.IAsyncResult PlayFab.PlayFabClientAPI/AddFriendResponseCallback::BeginInvoke(System.String,System.Int32,PlayFab.ClientModels.AddFriendRequest,PlayFab.ClientModels.AddFriendResult,PlayFab.PlayFabError,System.Object,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * AddFriendResponseCallback_BeginInvoke_m1942821114 (AddFriendResponseCallback_t2309947461 * __this, String_t* ___urlPath0, int32_t ___callId1, AddFriendRequest_t1861404448 * ___request2, AddFriendResult_t4251613068 * ___result3, PlayFabError_t750598646 * ___error4, Il2CppObject * ___customData5, AsyncCallback_t1363551830 * ___callback6, Il2CppObject * ___object7, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/AddFriendResponseCallback::EndInvoke(System.IAsyncResult)
extern "C"  void AddFriendResponseCallback_EndInvoke_m3179849316 (AddFriendResponseCallback_t2309947461 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
