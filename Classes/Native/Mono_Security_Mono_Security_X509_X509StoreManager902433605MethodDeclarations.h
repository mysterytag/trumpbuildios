﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Mono.Security.X509.X509StoreManager
struct X509StoreManager_t902433605;
// Mono.Security.X509.X509Stores
struct X509Stores_t2754714603;
// Mono.Security.X509.X509CertificateCollection
struct X509CertificateCollection_t3336811651;
// System.Collections.ArrayList
struct ArrayList_t2121638921;

#include "codegen/il2cpp-codegen.h"

// System.Void Mono.Security.X509.X509StoreManager::.ctor()
extern "C"  void X509StoreManager__ctor_m3901095863 (X509StoreManager_t902433605 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Mono.Security.X509.X509Stores Mono.Security.X509.X509StoreManager::get_CurrentUser()
extern "C"  X509Stores_t2754714603 * X509StoreManager_get_CurrentUser_m761909638 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Mono.Security.X509.X509Stores Mono.Security.X509.X509StoreManager::get_LocalMachine()
extern "C"  X509Stores_t2754714603 * X509StoreManager_get_LocalMachine_m812946396 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Mono.Security.X509.X509CertificateCollection Mono.Security.X509.X509StoreManager::get_IntermediateCACertificates()
extern "C"  X509CertificateCollection_t3336811651 * X509StoreManager_get_IntermediateCACertificates_m889867036 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.ArrayList Mono.Security.X509.X509StoreManager::get_IntermediateCACrls()
extern "C"  ArrayList_t2121638921 * X509StoreManager_get_IntermediateCACrls_m3179923595 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Mono.Security.X509.X509CertificateCollection Mono.Security.X509.X509StoreManager::get_TrustedRootCertificates()
extern "C"  X509CertificateCollection_t3336811651 * X509StoreManager_get_TrustedRootCertificates_m2593128238 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.ArrayList Mono.Security.X509.X509StoreManager::get_TrustedRootCACrls()
extern "C"  ArrayList_t2121638921 * X509StoreManager_get_TrustedRootCACrls_m544836785 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Mono.Security.X509.X509CertificateCollection Mono.Security.X509.X509StoreManager::get_UntrustedCertificates()
extern "C"  X509CertificateCollection_t3336811651 * X509StoreManager_get_UntrustedCertificates_m2880940595 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
