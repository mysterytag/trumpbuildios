﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Utils2/<OnPointerDown>c__AnonStoreyF0
struct U3COnPointerDownU3Ec__AnonStoreyF0_t1374650039;
// UnityEngine.EventSystems.BaseEventData
struct BaseEventData_t3547103726;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_BaseEventD3547103726.h"

// System.Void Utils2/<OnPointerDown>c__AnonStoreyF0::.ctor()
extern "C"  void U3COnPointerDownU3Ec__AnonStoreyF0__ctor_m2146491974 (U3COnPointerDownU3Ec__AnonStoreyF0_t1374650039 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Utils2/<OnPointerDown>c__AnonStoreyF0::<>m__156(UnityEngine.EventSystems.BaseEventData)
extern "C"  void U3COnPointerDownU3Ec__AnonStoreyF0_U3CU3Em__156_m1501428959 (U3COnPointerDownU3Ec__AnonStoreyF0_t1374650039 * __this, BaseEventData_t3547103726 * ___data0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Utils2/<OnPointerDown>c__AnonStoreyF0::<>m__157()
extern "C"  void U3COnPointerDownU3Ec__AnonStoreyF0_U3CU3Em__157_m1204253190 (U3COnPointerDownU3Ec__AnonStoreyF0_t1374650039 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
