﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.PlayFabClientAPI/GetLeaderboardAroundPlayerResponseCallback
struct GetLeaderboardAroundPlayerResponseCallback_t1316596955;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;
// PlayFab.ClientModels.GetLeaderboardAroundPlayerRequest
struct GetLeaderboardAroundPlayerRequest_t299203018;
// PlayFab.ClientModels.GetLeaderboardAroundPlayerResult
struct GetLeaderboardAroundPlayerResult_t460441506;
// PlayFab.PlayFabError
struct PlayFabError_t750598646;
// System.IAsyncResult
struct IAsyncResult_t537683269;
// System.AsyncCallback
struct AsyncCallback_t1363551830;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_IntPtr676692020.h"
#include "mscorlib_System_String968488902.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_GetLeaderboa299203018.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_GetLeaderboa460441506.h"
#include "AssemblyU2DCSharpU2Dfirstpass_PlayFab_PlayFabError750598646.h"
#include "mscorlib_System_AsyncCallback1363551830.h"

// System.Void PlayFab.PlayFabClientAPI/GetLeaderboardAroundPlayerResponseCallback::.ctor(System.Object,System.IntPtr)
extern "C"  void GetLeaderboardAroundPlayerResponseCallback__ctor_m4244365386 (GetLeaderboardAroundPlayerResponseCallback_t1316596955 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/GetLeaderboardAroundPlayerResponseCallback::Invoke(System.String,System.Int32,PlayFab.ClientModels.GetLeaderboardAroundPlayerRequest,PlayFab.ClientModels.GetLeaderboardAroundPlayerResult,PlayFab.PlayFabError,System.Object)
extern "C"  void GetLeaderboardAroundPlayerResponseCallback_Invoke_m463874167 (GetLeaderboardAroundPlayerResponseCallback_t1316596955 * __this, String_t* ___urlPath0, int32_t ___callId1, GetLeaderboardAroundPlayerRequest_t299203018 * ___request2, GetLeaderboardAroundPlayerResult_t460441506 * ___result3, PlayFabError_t750598646 * ___error4, Il2CppObject * ___customData5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
extern "C" void pinvoke_delegate_wrapper_GetLeaderboardAroundPlayerResponseCallback_t1316596955(Il2CppObject* delegate, String_t* ___urlPath0, int32_t ___callId1, GetLeaderboardAroundPlayerRequest_t299203018 * ___request2, GetLeaderboardAroundPlayerResult_t460441506 * ___result3, PlayFabError_t750598646 * ___error4, Il2CppObject * ___customData5);
// System.IAsyncResult PlayFab.PlayFabClientAPI/GetLeaderboardAroundPlayerResponseCallback::BeginInvoke(System.String,System.Int32,PlayFab.ClientModels.GetLeaderboardAroundPlayerRequest,PlayFab.ClientModels.GetLeaderboardAroundPlayerResult,PlayFab.PlayFabError,System.Object,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * GetLeaderboardAroundPlayerResponseCallback_BeginInvoke_m798055012 (GetLeaderboardAroundPlayerResponseCallback_t1316596955 * __this, String_t* ___urlPath0, int32_t ___callId1, GetLeaderboardAroundPlayerRequest_t299203018 * ___request2, GetLeaderboardAroundPlayerResult_t460441506 * ___result3, PlayFabError_t750598646 * ___error4, Il2CppObject * ___customData5, AsyncCallback_t1363551830 * ___callback6, Il2CppObject * ___object7, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/GetLeaderboardAroundPlayerResponseCallback::EndInvoke(System.IAsyncResult)
extern "C"  void GetLeaderboardAroundPlayerResponseCallback_EndInvoke_m2198800218 (GetLeaderboardAroundPlayerResponseCallback_t1316596955 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
