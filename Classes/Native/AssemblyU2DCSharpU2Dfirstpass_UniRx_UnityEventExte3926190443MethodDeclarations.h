﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.IObservable`1<UniRx.Unit>
struct IObservable_1_t2317084402;
// UnityEngine.Events.UnityEvent
struct UnityEvent_t2938797301;
// UnityEngine.Events.UnityAction
struct UnityAction_t909267611;
// System.Action
struct Action_t437523947;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Events_UnityEvent2938797301.h"
#include "System_Core_System_Action437523947.h"

// UniRx.IObservable`1<UniRx.Unit> UniRx.UnityEventExtensions::AsObservable(UnityEngine.Events.UnityEvent)
extern "C"  Il2CppObject* UnityEventExtensions_AsObservable_m3334905452 (Il2CppObject * __this /* static, unused */, UnityEvent_t2938797301 * ___unityEvent0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Events.UnityAction UniRx.UnityEventExtensions::<AsObservable>m__C6(System.Action)
extern "C"  UnityAction_t909267611 * UnityEventExtensions_U3CAsObservableU3Em__C6_m1223491398 (Il2CppObject * __this /* static, unused */, Action_t437523947 * ___h0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
