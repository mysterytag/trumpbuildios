﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// BarygaVkController/<JoinGroup>c__AnonStoreyDD
struct U3CJoinGroupU3Ec__AnonStoreyDD_t3333112034;

#include "codegen/il2cpp-codegen.h"

// System.Void BarygaVkController/<JoinGroup>c__AnonStoreyDD::.ctor()
extern "C"  void U3CJoinGroupU3Ec__AnonStoreyDD__ctor_m1014369867 (U3CJoinGroupU3Ec__AnonStoreyDD_t3333112034 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BarygaVkController/<JoinGroup>c__AnonStoreyDD::<>m__F7(System.Int64)
extern "C"  void U3CJoinGroupU3Ec__AnonStoreyDD_U3CU3Em__F7_m3337483287 (U3CJoinGroupU3Ec__AnonStoreyDD_t3333112034 * __this, int64_t ___l0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
