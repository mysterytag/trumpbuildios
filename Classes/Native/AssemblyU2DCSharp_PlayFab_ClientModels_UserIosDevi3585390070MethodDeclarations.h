﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.ClientModels.UserIosDeviceInfo
struct UserIosDeviceInfo_t3585390070;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String968488902.h"

// System.Void PlayFab.ClientModels.UserIosDeviceInfo::.ctor()
extern "C"  void UserIosDeviceInfo__ctor_m308308211 (UserIosDeviceInfo_t3585390070 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PlayFab.ClientModels.UserIosDeviceInfo::get_IosDeviceId()
extern "C"  String_t* UserIosDeviceInfo_get_IosDeviceId_m3304087011 (UserIosDeviceInfo_t3585390070 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.UserIosDeviceInfo::set_IosDeviceId(System.String)
extern "C"  void UserIosDeviceInfo_set_IosDeviceId_m338065808 (UserIosDeviceInfo_t3585390070 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
