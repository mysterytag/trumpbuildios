﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.OperatorObservableBase`1<UnityEngine.Vector2>
struct OperatorObservableBase_1_t2589474759;
// System.IDisposable
struct IDisposable_t1628921374;
// UniRx.IObserver`1<UnityEngine.Vector2>
struct IObserver_1_t1442361395;

#include "codegen/il2cpp-codegen.h"

// System.Void UniRx.Operators.OperatorObservableBase`1<UnityEngine.Vector2>::.ctor(System.Boolean)
extern "C"  void OperatorObservableBase_1__ctor_m2658942956_gshared (OperatorObservableBase_1_t2589474759 * __this, bool ___isRequiredSubscribeOnCurrentThread0, const MethodInfo* method);
#define OperatorObservableBase_1__ctor_m2658942956(__this, ___isRequiredSubscribeOnCurrentThread0, method) ((  void (*) (OperatorObservableBase_1_t2589474759 *, bool, const MethodInfo*))OperatorObservableBase_1__ctor_m2658942956_gshared)(__this, ___isRequiredSubscribeOnCurrentThread0, method)
// System.Boolean UniRx.Operators.OperatorObservableBase`1<UnityEngine.Vector2>::IsRequiredSubscribeOnCurrentThread()
extern "C"  bool OperatorObservableBase_1_IsRequiredSubscribeOnCurrentThread_m3648010678_gshared (OperatorObservableBase_1_t2589474759 * __this, const MethodInfo* method);
#define OperatorObservableBase_1_IsRequiredSubscribeOnCurrentThread_m3648010678(__this, method) ((  bool (*) (OperatorObservableBase_1_t2589474759 *, const MethodInfo*))OperatorObservableBase_1_IsRequiredSubscribeOnCurrentThread_m3648010678_gshared)(__this, method)
// System.IDisposable UniRx.Operators.OperatorObservableBase`1<UnityEngine.Vector2>::Subscribe(UniRx.IObserver`1<T>)
extern "C"  Il2CppObject * OperatorObservableBase_1_Subscribe_m943572682_gshared (OperatorObservableBase_1_t2589474759 * __this, Il2CppObject* ___observer0, const MethodInfo* method);
#define OperatorObservableBase_1_Subscribe_m943572682(__this, ___observer0, method) ((  Il2CppObject * (*) (OperatorObservableBase_1_t2589474759 *, Il2CppObject*, const MethodInfo*))OperatorObservableBase_1_Subscribe_m943572682_gshared)(__this, ___observer0, method)
