﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.IObservable`1<System.String>
struct IObservable_1_t727287266;
// System.String
struct String_t;
// System.Collections.Generic.Dictionary`2<System.String,System.String>
struct Dictionary_2_t2606186806;
// UniRx.IProgress`1<System.Single>
struct IProgress_1_t4173802291;
// UniRx.IObservable`1<System.Byte[]>
struct IObservable_1_t4112271820;
// UniRx.IObservable`1<UnityEngine.WWW>
struct IObservable_1_t1281770464;
// System.Byte[]
struct ByteU5BU5D_t58506160;
// UnityEngine.WWWForm
struct WWWForm_t3999572776;
// UniRx.IObservable`1<UnityEngine.AssetBundle>
struct IObservable_1_t3718229467;
// System.Collections.IEnumerator
struct IEnumerator_t287207039;
// UnityEngine.WWW
struct WWW_t1522972100;
// UniRx.IObserver`1<UnityEngine.WWW>
struct IObserver_1_t3734971003;
// UniRx.IObserver`1<System.String>
struct IObserver_1_t3180487805;
// UniRx.IObserver`1<System.Byte[]>
struct IObserver_1_t2270505063;
// UniRx.IObserver`1<UnityEngine.AssetBundle>
struct IObserver_1_t1876462710;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String968488902.h"
#include "UnityEngine_UnityEngine_WWWForm3999572776.h"
#include "UnityEngine_UnityEngine_Hash1283885020822.h"
#include "UnityEngine_UnityEngine_WWW1522972100.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_CancellationTo1439151560.h"

// UniRx.IObservable`1<System.String> UniRx.ObservableWWW::Get(System.String,System.Collections.Generic.Dictionary`2<System.String,System.String>,UniRx.IProgress`1<System.Single>)
extern "C"  Il2CppObject* ObservableWWW_Get_m2633702238 (Il2CppObject * __this /* static, unused */, String_t* ___url0, Dictionary_2_t2606186806 * ___headers1, Il2CppObject* ___progress2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UniRx.IObservable`1<System.Byte[]> UniRx.ObservableWWW::GetAndGetBytes(System.String,System.Collections.Generic.Dictionary`2<System.String,System.String>,UniRx.IProgress`1<System.Single>)
extern "C"  Il2CppObject* ObservableWWW_GetAndGetBytes_m2190868423 (Il2CppObject * __this /* static, unused */, String_t* ___url0, Dictionary_2_t2606186806 * ___headers1, Il2CppObject* ___progress2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UniRx.IObservable`1<UnityEngine.WWW> UniRx.ObservableWWW::GetWWW(System.String,System.Collections.Generic.Dictionary`2<System.String,System.String>,UniRx.IProgress`1<System.Single>)
extern "C"  Il2CppObject* ObservableWWW_GetWWW_m3629130099 (Il2CppObject * __this /* static, unused */, String_t* ___url0, Dictionary_2_t2606186806 * ___headers1, Il2CppObject* ___progress2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UniRx.IObservable`1<System.String> UniRx.ObservableWWW::Post(System.String,System.Byte[],UniRx.IProgress`1<System.Single>)
extern "C"  Il2CppObject* ObservableWWW_Post_m989780006 (Il2CppObject * __this /* static, unused */, String_t* ___url0, ByteU5BU5D_t58506160* ___postData1, Il2CppObject* ___progress2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UniRx.IObservable`1<System.String> UniRx.ObservableWWW::Post(System.String,System.Byte[],System.Collections.Generic.Dictionary`2<System.String,System.String>,UniRx.IProgress`1<System.Single>)
extern "C"  Il2CppObject* ObservableWWW_Post_m1174276551 (Il2CppObject * __this /* static, unused */, String_t* ___url0, ByteU5BU5D_t58506160* ___postData1, Dictionary_2_t2606186806 * ___headers2, Il2CppObject* ___progress3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UniRx.IObservable`1<System.String> UniRx.ObservableWWW::Post(System.String,UnityEngine.WWWForm,UniRx.IProgress`1<System.Single>)
extern "C"  Il2CppObject* ObservableWWW_Post_m265292097 (Il2CppObject * __this /* static, unused */, String_t* ___url0, WWWForm_t3999572776 * ___content1, Il2CppObject* ___progress2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UniRx.IObservable`1<System.String> UniRx.ObservableWWW::Post(System.String,UnityEngine.WWWForm,System.Collections.Generic.Dictionary`2<System.String,System.String>,UniRx.IProgress`1<System.Single>)
extern "C"  Il2CppObject* ObservableWWW_Post_m3321211276 (Il2CppObject * __this /* static, unused */, String_t* ___url0, WWWForm_t3999572776 * ___content1, Dictionary_2_t2606186806 * ___headers2, Il2CppObject* ___progress3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UniRx.IObservable`1<System.Byte[]> UniRx.ObservableWWW::PostAndGetBytes(System.String,System.Byte[],UniRx.IProgress`1<System.Single>)
extern "C"  Il2CppObject* ObservableWWW_PostAndGetBytes_m3304440251 (Il2CppObject * __this /* static, unused */, String_t* ___url0, ByteU5BU5D_t58506160* ___postData1, Il2CppObject* ___progress2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UniRx.IObservable`1<System.Byte[]> UniRx.ObservableWWW::PostAndGetBytes(System.String,System.Byte[],System.Collections.Generic.Dictionary`2<System.String,System.String>,UniRx.IProgress`1<System.Single>)
extern "C"  Il2CppObject* ObservableWWW_PostAndGetBytes_m2287009106 (Il2CppObject * __this /* static, unused */, String_t* ___url0, ByteU5BU5D_t58506160* ___postData1, Dictionary_2_t2606186806 * ___headers2, Il2CppObject* ___progress3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UniRx.IObservable`1<System.Byte[]> UniRx.ObservableWWW::PostAndGetBytes(System.String,UnityEngine.WWWForm,UniRx.IProgress`1<System.Single>)
extern "C"  Il2CppObject* ObservableWWW_PostAndGetBytes_m523546390 (Il2CppObject * __this /* static, unused */, String_t* ___url0, WWWForm_t3999572776 * ___content1, Il2CppObject* ___progress2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UniRx.IObservable`1<System.Byte[]> UniRx.ObservableWWW::PostAndGetBytes(System.String,UnityEngine.WWWForm,System.Collections.Generic.Dictionary`2<System.String,System.String>,UniRx.IProgress`1<System.Single>)
extern "C"  Il2CppObject* ObservableWWW_PostAndGetBytes_m3322448087 (Il2CppObject * __this /* static, unused */, String_t* ___url0, WWWForm_t3999572776 * ___content1, Dictionary_2_t2606186806 * ___headers2, Il2CppObject* ___progress3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UniRx.IObservable`1<UnityEngine.WWW> UniRx.ObservableWWW::PostWWW(System.String,System.Byte[],UniRx.IProgress`1<System.Single>)
extern "C"  Il2CppObject* ObservableWWW_PostWWW_m3467875933 (Il2CppObject * __this /* static, unused */, String_t* ___url0, ByteU5BU5D_t58506160* ___postData1, Il2CppObject* ___progress2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UniRx.IObservable`1<UnityEngine.WWW> UniRx.ObservableWWW::PostWWW(System.String,System.Byte[],System.Collections.Generic.Dictionary`2<System.String,System.String>,UniRx.IProgress`1<System.Single>)
extern "C"  Il2CppObject* ObservableWWW_PostWWW_m1119145456 (Il2CppObject * __this /* static, unused */, String_t* ___url0, ByteU5BU5D_t58506160* ___postData1, Dictionary_2_t2606186806 * ___headers2, Il2CppObject* ___progress3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UniRx.IObservable`1<UnityEngine.WWW> UniRx.ObservableWWW::PostWWW(System.String,UnityEngine.WWWForm,UniRx.IProgress`1<System.Single>)
extern "C"  Il2CppObject* ObservableWWW_PostWWW_m1240793656 (Il2CppObject * __this /* static, unused */, String_t* ___url0, WWWForm_t3999572776 * ___content1, Il2CppObject* ___progress2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UniRx.IObservable`1<UnityEngine.WWW> UniRx.ObservableWWW::PostWWW(System.String,UnityEngine.WWWForm,System.Collections.Generic.Dictionary`2<System.String,System.String>,UniRx.IProgress`1<System.Single>)
extern "C"  Il2CppObject* ObservableWWW_PostWWW_m2691230453 (Il2CppObject * __this /* static, unused */, String_t* ___url0, WWWForm_t3999572776 * ___content1, Dictionary_2_t2606186806 * ___headers2, Il2CppObject* ___progress3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UniRx.IObservable`1<UnityEngine.AssetBundle> UniRx.ObservableWWW::LoadFromCacheOrDownload(System.String,System.Int32,UniRx.IProgress`1<System.Single>)
extern "C"  Il2CppObject* ObservableWWW_LoadFromCacheOrDownload_m1210134636 (Il2CppObject * __this /* static, unused */, String_t* ___url0, int32_t ___version1, Il2CppObject* ___progress2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UniRx.IObservable`1<UnityEngine.AssetBundle> UniRx.ObservableWWW::LoadFromCacheOrDownload(System.String,System.Int32,System.UInt32,UniRx.IProgress`1<System.Single>)
extern "C"  Il2CppObject* ObservableWWW_LoadFromCacheOrDownload_m3883885368 (Il2CppObject * __this /* static, unused */, String_t* ___url0, int32_t ___version1, uint32_t ___crc2, Il2CppObject* ___progress3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UniRx.IObservable`1<UnityEngine.AssetBundle> UniRx.ObservableWWW::LoadFromCacheOrDownload(System.String,UnityEngine.Hash128,UniRx.IProgress`1<System.Single>)
extern "C"  Il2CppObject* ObservableWWW_LoadFromCacheOrDownload_m579420519 (Il2CppObject * __this /* static, unused */, String_t* ___url0, Hash128_t3885020822  ___hash1281, Il2CppObject* ___progress2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UniRx.IObservable`1<UnityEngine.AssetBundle> UniRx.ObservableWWW::LoadFromCacheOrDownload(System.String,UnityEngine.Hash128,System.UInt32,UniRx.IProgress`1<System.Single>)
extern "C"  Il2CppObject* ObservableWWW_LoadFromCacheOrDownload_m1017946355 (Il2CppObject * __this /* static, unused */, String_t* ___url0, Hash128_t3885020822  ___hash1281, uint32_t ___crc2, Il2CppObject* ___progress3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.Dictionary`2<System.String,System.String> UniRx.ObservableWWW::MergeHash(System.Collections.Generic.Dictionary`2<System.String,System.String>,System.Collections.Generic.Dictionary`2<System.String,System.String>)
extern "C"  Dictionary_2_t2606186806 * ObservableWWW_MergeHash_m4254025137 (Il2CppObject * __this /* static, unused */, Dictionary_2_t2606186806 * ___wwwFormHeaders0, Dictionary_2_t2606186806 * ___externalHeaders1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator UniRx.ObservableWWW::Fetch(UnityEngine.WWW,UniRx.IObserver`1<UnityEngine.WWW>,UniRx.IProgress`1<System.Single>,UniRx.CancellationToken)
extern "C"  Il2CppObject * ObservableWWW_Fetch_m4100415835 (Il2CppObject * __this /* static, unused */, WWW_t1522972100 * ___www0, Il2CppObject* ___observer1, Il2CppObject* ___reportProgress2, CancellationToken_t1439151560  ___cancel3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator UniRx.ObservableWWW::FetchText(UnityEngine.WWW,UniRx.IObserver`1<System.String>,UniRx.IProgress`1<System.Single>,UniRx.CancellationToken)
extern "C"  Il2CppObject * ObservableWWW_FetchText_m220208830 (Il2CppObject * __this /* static, unused */, WWW_t1522972100 * ___www0, Il2CppObject* ___observer1, Il2CppObject* ___reportProgress2, CancellationToken_t1439151560  ___cancel3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator UniRx.ObservableWWW::FetchBytes(UnityEngine.WWW,UniRx.IObserver`1<System.Byte[]>,UniRx.IProgress`1<System.Single>,UniRx.CancellationToken)
extern "C"  Il2CppObject * ObservableWWW_FetchBytes_m789535179 (Il2CppObject * __this /* static, unused */, WWW_t1522972100 * ___www0, Il2CppObject* ___observer1, Il2CppObject* ___reportProgress2, CancellationToken_t1439151560  ___cancel3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator UniRx.ObservableWWW::FetchAssetBundle(UnityEngine.WWW,UniRx.IObserver`1<UnityEngine.AssetBundle>,UniRx.IProgress`1<System.Single>,UniRx.CancellationToken)
extern "C"  Il2CppObject * ObservableWWW_FetchAssetBundle_m2288014994 (Il2CppObject * __this /* static, unused */, WWW_t1522972100 * ___www0, Il2CppObject* ___observer1, Il2CppObject* ___reportProgress2, CancellationToken_t1439151560  ___cancel3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
