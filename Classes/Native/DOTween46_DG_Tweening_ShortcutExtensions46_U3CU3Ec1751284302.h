﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.UI.Image
struct Image_t3354615620;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.ShortcutExtensions46/<>c__DisplayClass4_0
struct  U3CU3Ec__DisplayClass4_0_t1751284303  : public Il2CppObject
{
public:
	// UnityEngine.UI.Image DG.Tweening.ShortcutExtensions46/<>c__DisplayClass4_0::target
	Image_t3354615620 * ___target_0;

public:
	inline static int32_t get_offset_of_target_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass4_0_t1751284303, ___target_0)); }
	inline Image_t3354615620 * get_target_0() const { return ___target_0; }
	inline Image_t3354615620 ** get_address_of_target_0() { return &___target_0; }
	inline void set_target_0(Image_t3354615620 * value)
	{
		___target_0 = value;
		Il2CppCodeGenWriteBarrier(&___target_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
