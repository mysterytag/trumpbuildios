﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SubstanceBase`2/<Affect>c__AnonStorey144<System.Boolean,System.Boolean>
struct U3CAffectU3Ec__AnonStorey144_t386862328;

#include "codegen/il2cpp-codegen.h"

// System.Void SubstanceBase`2/<Affect>c__AnonStorey144<System.Boolean,System.Boolean>::.ctor()
extern "C"  void U3CAffectU3Ec__AnonStorey144__ctor_m2376956784_gshared (U3CAffectU3Ec__AnonStorey144_t386862328 * __this, const MethodInfo* method);
#define U3CAffectU3Ec__AnonStorey144__ctor_m2376956784(__this, method) ((  void (*) (U3CAffectU3Ec__AnonStorey144_t386862328 *, const MethodInfo*))U3CAffectU3Ec__AnonStorey144__ctor_m2376956784_gshared)(__this, method)
// System.Void SubstanceBase`2/<Affect>c__AnonStorey144<System.Boolean,System.Boolean>::<>m__1DA()
extern "C"  void U3CAffectU3Ec__AnonStorey144_U3CU3Em__1DA_m3624125367_gshared (U3CAffectU3Ec__AnonStorey144_t386862328 * __this, const MethodInfo* method);
#define U3CAffectU3Ec__AnonStorey144_U3CU3Em__1DA_m3624125367(__this, method) ((  void (*) (U3CAffectU3Ec__AnonStorey144_t386862328 *, const MethodInfo*))U3CAffectU3Ec__AnonStorey144_U3CU3Em__1DA_m3624125367_gshared)(__this, method)
