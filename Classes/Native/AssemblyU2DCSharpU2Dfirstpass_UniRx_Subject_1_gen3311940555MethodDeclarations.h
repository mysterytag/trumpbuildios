﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Subject`1<UniRx.CollectionReplaceEvent`1<UniRx.Tuple`2<System.Object,System.Object>>>
struct Subject_1_t3311940555;
// System.Exception
struct Exception_t1967233988;
// System.IDisposable
struct IDisposable_t1628921374;
// UniRx.IObserver`1<UniRx.CollectionReplaceEvent`1<UniRx.Tuple`2<System.Object,System.Object>>>
struct IObserver_1_t3585904838;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Exception1967233988.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_CollectionRepl1373905935.h"

// System.Void UniRx.Subject`1<UniRx.CollectionReplaceEvent`1<UniRx.Tuple`2<System.Object,System.Object>>>::.ctor()
extern "C"  void Subject_1__ctor_m2360697207_gshared (Subject_1_t3311940555 * __this, const MethodInfo* method);
#define Subject_1__ctor_m2360697207(__this, method) ((  void (*) (Subject_1_t3311940555 *, const MethodInfo*))Subject_1__ctor_m2360697207_gshared)(__this, method)
// System.Boolean UniRx.Subject`1<UniRx.CollectionReplaceEvent`1<UniRx.Tuple`2<System.Object,System.Object>>>::get_HasObservers()
extern "C"  bool Subject_1_get_HasObservers_m2818028541_gshared (Subject_1_t3311940555 * __this, const MethodInfo* method);
#define Subject_1_get_HasObservers_m2818028541(__this, method) ((  bool (*) (Subject_1_t3311940555 *, const MethodInfo*))Subject_1_get_HasObservers_m2818028541_gshared)(__this, method)
// System.Void UniRx.Subject`1<UniRx.CollectionReplaceEvent`1<UniRx.Tuple`2<System.Object,System.Object>>>::OnCompleted()
extern "C"  void Subject_1_OnCompleted_m3042533313_gshared (Subject_1_t3311940555 * __this, const MethodInfo* method);
#define Subject_1_OnCompleted_m3042533313(__this, method) ((  void (*) (Subject_1_t3311940555 *, const MethodInfo*))Subject_1_OnCompleted_m3042533313_gshared)(__this, method)
// System.Void UniRx.Subject`1<UniRx.CollectionReplaceEvent`1<UniRx.Tuple`2<System.Object,System.Object>>>::OnError(System.Exception)
extern "C"  void Subject_1_OnError_m79001454_gshared (Subject_1_t3311940555 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method);
#define Subject_1_OnError_m79001454(__this, ___error0, method) ((  void (*) (Subject_1_t3311940555 *, Exception_t1967233988 *, const MethodInfo*))Subject_1_OnError_m79001454_gshared)(__this, ___error0, method)
// System.Void UniRx.Subject`1<UniRx.CollectionReplaceEvent`1<UniRx.Tuple`2<System.Object,System.Object>>>::OnNext(T)
extern "C"  void Subject_1_OnNext_m3717986911_gshared (Subject_1_t3311940555 * __this, CollectionReplaceEvent_1_t1373905935  ___value0, const MethodInfo* method);
#define Subject_1_OnNext_m3717986911(__this, ___value0, method) ((  void (*) (Subject_1_t3311940555 *, CollectionReplaceEvent_1_t1373905935 , const MethodInfo*))Subject_1_OnNext_m3717986911_gshared)(__this, ___value0, method)
// System.IDisposable UniRx.Subject`1<UniRx.CollectionReplaceEvent`1<UniRx.Tuple`2<System.Object,System.Object>>>::Subscribe(UniRx.IObserver`1<T>)
extern "C"  Il2CppObject * Subject_1_Subscribe_m2277887436_gshared (Subject_1_t3311940555 * __this, Il2CppObject* ___observer0, const MethodInfo* method);
#define Subject_1_Subscribe_m2277887436(__this, ___observer0, method) ((  Il2CppObject * (*) (Subject_1_t3311940555 *, Il2CppObject*, const MethodInfo*))Subject_1_Subscribe_m2277887436_gshared)(__this, ___observer0, method)
// System.Void UniRx.Subject`1<UniRx.CollectionReplaceEvent`1<UniRx.Tuple`2<System.Object,System.Object>>>::Dispose()
extern "C"  void Subject_1_Dispose_m785566708_gshared (Subject_1_t3311940555 * __this, const MethodInfo* method);
#define Subject_1_Dispose_m785566708(__this, method) ((  void (*) (Subject_1_t3311940555 *, const MethodInfo*))Subject_1_Dispose_m785566708_gshared)(__this, method)
// System.Void UniRx.Subject`1<UniRx.CollectionReplaceEvent`1<UniRx.Tuple`2<System.Object,System.Object>>>::ThrowIfDisposed()
extern "C"  void Subject_1_ThrowIfDisposed_m3777970557_gshared (Subject_1_t3311940555 * __this, const MethodInfo* method);
#define Subject_1_ThrowIfDisposed_m3777970557(__this, method) ((  void (*) (Subject_1_t3311940555 *, const MethodInfo*))Subject_1_ThrowIfDisposed_m3777970557_gshared)(__this, method)
// System.Boolean UniRx.Subject`1<UniRx.CollectionReplaceEvent`1<UniRx.Tuple`2<System.Object,System.Object>>>::IsRequiredSubscribeOnCurrentThread()
extern "C"  bool Subject_1_IsRequiredSubscribeOnCurrentThread_m1893727156_gshared (Subject_1_t3311940555 * __this, const MethodInfo* method);
#define Subject_1_IsRequiredSubscribeOnCurrentThread_m1893727156(__this, method) ((  bool (*) (Subject_1_t3311940555 *, const MethodInfo*))Subject_1_IsRequiredSubscribeOnCurrentThread_m1893727156_gshared)(__this, method)
