﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Stream`1<System.Int32>
struct Stream_1_t1538553809;
// Stream`1<UniRx.Unit>
struct Stream_1_t1249425060;
// Stream`1<UniRx.CollectionAddEvent`1<SettingsButton>>
struct Stream_1_t1185811250;
// Stream`1<CollectionMoveEvent`1<SettingsButton>>
struct Stream_1_t2341344644;
// Stream`1<CollectionRemoveEvent`1<SettingsButton>>
struct Stream_1_t3962515817;
// Stream`1<CollectionReplaceEvent`1<SettingsButton>>
struct Stream_1_t1266661333;
// Stream`1<System.Collections.Generic.ICollection`1<SettingsButton>>
struct Stream_1_t72227069;

#include "mscorlib_System_Collections_ObjectModel_Collection2884244391.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ZergRush.InorganicCollection`1<SettingsButton>
struct  InorganicCollection_1_t3529127759  : public Collection_1_t2884244391
{
public:
	// Stream`1<System.Int32> ZergRush.InorganicCollection`1::countChanged
	Stream_1_t1538553809 * ___countChanged_2;
	// Stream`1<UniRx.Unit> ZergRush.InorganicCollection`1::collectionReset
	Stream_1_t1249425060 * ___collectionReset_3;
	// Stream`1<UniRx.CollectionAddEvent`1<T>> ZergRush.InorganicCollection`1::collectionAdd
	Stream_1_t1185811250 * ___collectionAdd_4;
	// Stream`1<CollectionMoveEvent`1<T>> ZergRush.InorganicCollection`1::collectionMove
	Stream_1_t2341344644 * ___collectionMove_5;
	// Stream`1<CollectionRemoveEvent`1<T>> ZergRush.InorganicCollection`1::collectionRemove
	Stream_1_t3962515817 * ___collectionRemove_6;
	// Stream`1<CollectionReplaceEvent`1<T>> ZergRush.InorganicCollection`1::collectionReplace
	Stream_1_t1266661333 * ___collectionReplace_7;
	// Stream`1<System.Collections.Generic.ICollection`1<T>> ZergRush.InorganicCollection`1::updates
	Stream_1_t72227069 * ___updates_8;

public:
	inline static int32_t get_offset_of_countChanged_2() { return static_cast<int32_t>(offsetof(InorganicCollection_1_t3529127759, ___countChanged_2)); }
	inline Stream_1_t1538553809 * get_countChanged_2() const { return ___countChanged_2; }
	inline Stream_1_t1538553809 ** get_address_of_countChanged_2() { return &___countChanged_2; }
	inline void set_countChanged_2(Stream_1_t1538553809 * value)
	{
		___countChanged_2 = value;
		Il2CppCodeGenWriteBarrier(&___countChanged_2, value);
	}

	inline static int32_t get_offset_of_collectionReset_3() { return static_cast<int32_t>(offsetof(InorganicCollection_1_t3529127759, ___collectionReset_3)); }
	inline Stream_1_t1249425060 * get_collectionReset_3() const { return ___collectionReset_3; }
	inline Stream_1_t1249425060 ** get_address_of_collectionReset_3() { return &___collectionReset_3; }
	inline void set_collectionReset_3(Stream_1_t1249425060 * value)
	{
		___collectionReset_3 = value;
		Il2CppCodeGenWriteBarrier(&___collectionReset_3, value);
	}

	inline static int32_t get_offset_of_collectionAdd_4() { return static_cast<int32_t>(offsetof(InorganicCollection_1_t3529127759, ___collectionAdd_4)); }
	inline Stream_1_t1185811250 * get_collectionAdd_4() const { return ___collectionAdd_4; }
	inline Stream_1_t1185811250 ** get_address_of_collectionAdd_4() { return &___collectionAdd_4; }
	inline void set_collectionAdd_4(Stream_1_t1185811250 * value)
	{
		___collectionAdd_4 = value;
		Il2CppCodeGenWriteBarrier(&___collectionAdd_4, value);
	}

	inline static int32_t get_offset_of_collectionMove_5() { return static_cast<int32_t>(offsetof(InorganicCollection_1_t3529127759, ___collectionMove_5)); }
	inline Stream_1_t2341344644 * get_collectionMove_5() const { return ___collectionMove_5; }
	inline Stream_1_t2341344644 ** get_address_of_collectionMove_5() { return &___collectionMove_5; }
	inline void set_collectionMove_5(Stream_1_t2341344644 * value)
	{
		___collectionMove_5 = value;
		Il2CppCodeGenWriteBarrier(&___collectionMove_5, value);
	}

	inline static int32_t get_offset_of_collectionRemove_6() { return static_cast<int32_t>(offsetof(InorganicCollection_1_t3529127759, ___collectionRemove_6)); }
	inline Stream_1_t3962515817 * get_collectionRemove_6() const { return ___collectionRemove_6; }
	inline Stream_1_t3962515817 ** get_address_of_collectionRemove_6() { return &___collectionRemove_6; }
	inline void set_collectionRemove_6(Stream_1_t3962515817 * value)
	{
		___collectionRemove_6 = value;
		Il2CppCodeGenWriteBarrier(&___collectionRemove_6, value);
	}

	inline static int32_t get_offset_of_collectionReplace_7() { return static_cast<int32_t>(offsetof(InorganicCollection_1_t3529127759, ___collectionReplace_7)); }
	inline Stream_1_t1266661333 * get_collectionReplace_7() const { return ___collectionReplace_7; }
	inline Stream_1_t1266661333 ** get_address_of_collectionReplace_7() { return &___collectionReplace_7; }
	inline void set_collectionReplace_7(Stream_1_t1266661333 * value)
	{
		___collectionReplace_7 = value;
		Il2CppCodeGenWriteBarrier(&___collectionReplace_7, value);
	}

	inline static int32_t get_offset_of_updates_8() { return static_cast<int32_t>(offsetof(InorganicCollection_1_t3529127759, ___updates_8)); }
	inline Stream_1_t72227069 * get_updates_8() const { return ___updates_8; }
	inline Stream_1_t72227069 ** get_address_of_updates_8() { return &___updates_8; }
	inline void set_updates_8(Stream_1_t72227069 * value)
	{
		___updates_8 = value;
		Il2CppCodeGenWriteBarrier(&___updates_8, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
