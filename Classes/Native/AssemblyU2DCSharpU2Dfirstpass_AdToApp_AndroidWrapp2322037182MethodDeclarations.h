﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// AdToApp.AndroidWrapper.AdToAppAndroidWrapper/<InitializeSDK>c__AnonStorey29
struct U3CInitializeSDKU3Ec__AnonStorey29_t2322037182;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"

// System.Void AdToApp.AndroidWrapper.AdToAppAndroidWrapper/<InitializeSDK>c__AnonStorey29::.ctor()
extern "C"  void U3CInitializeSDKU3Ec__AnonStorey29__ctor_m261930770 (U3CInitializeSDKU3Ec__AnonStorey29_t2322037182 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AdToApp.AndroidWrapper.AdToAppAndroidWrapper/<InitializeSDK>c__AnonStorey29::<>m__0(System.Object)
extern "C"  void U3CInitializeSDKU3Ec__AnonStorey29_U3CU3Em__0_m2814292317 (U3CInitializeSDKU3Ec__AnonStorey29_t2322037182 * __this, Il2CppObject * ___wrapperContext0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
