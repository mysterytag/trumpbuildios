﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.PlayFabClientAPI/GetStoreItemsRequestCallback
struct GetStoreItemsRequestCallback_t580285439;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;
// PlayFab.ClientModels.GetStoreItemsRequest
struct GetStoreItemsRequest_t3204724138;
// System.IAsyncResult
struct IAsyncResult_t537683269;
// System.AsyncCallback
struct AsyncCallback_t1363551830;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_IntPtr676692020.h"
#include "mscorlib_System_String968488902.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_GetStoreIte3204724138.h"
#include "mscorlib_System_AsyncCallback1363551830.h"

// System.Void PlayFab.PlayFabClientAPI/GetStoreItemsRequestCallback::.ctor(System.Object,System.IntPtr)
extern "C"  void GetStoreItemsRequestCallback__ctor_m4006689134 (GetStoreItemsRequestCallback_t580285439 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/GetStoreItemsRequestCallback::Invoke(System.String,System.Int32,PlayFab.ClientModels.GetStoreItemsRequest,System.Object)
extern "C"  void GetStoreItemsRequestCallback_Invoke_m2437536511 (GetStoreItemsRequestCallback_t580285439 * __this, String_t* ___urlPath0, int32_t ___callId1, GetStoreItemsRequest_t3204724138 * ___request2, Il2CppObject * ___customData3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
extern "C" void pinvoke_delegate_wrapper_GetStoreItemsRequestCallback_t580285439(Il2CppObject* delegate, String_t* ___urlPath0, int32_t ___callId1, GetStoreItemsRequest_t3204724138 * ___request2, Il2CppObject * ___customData3);
// System.IAsyncResult PlayFab.PlayFabClientAPI/GetStoreItemsRequestCallback::BeginInvoke(System.String,System.Int32,PlayFab.ClientModels.GetStoreItemsRequest,System.Object,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * GetStoreItemsRequestCallback_BeginInvoke_m3386494884 (GetStoreItemsRequestCallback_t580285439 * __this, String_t* ___urlPath0, int32_t ___callId1, GetStoreItemsRequest_t3204724138 * ___request2, Il2CppObject * ___customData3, AsyncCallback_t1363551830 * ___callback4, Il2CppObject * ___object5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/GetStoreItemsRequestCallback::EndInvoke(System.IAsyncResult)
extern "C"  void GetStoreItemsRequestCallback_EndInvoke_m4020671102 (GetStoreItemsRequestCallback_t580285439 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
