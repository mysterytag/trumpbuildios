﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// System.Collections.Generic.Dictionary`2<System.String,System.UInt32>
struct Dictionary_2_t2623623230;
// System.Collections.Generic.List`1<System.String>
struct List_1_t1765447871;
// PlayFab.ClientModels.CatalogItemConsumableInfo
struct CatalogItemConsumableInfo_t2956282765;
// PlayFab.ClientModels.CatalogItemContainerInfo
struct CatalogItemContainerInfo_t284142995;
// PlayFab.ClientModels.CatalogItemBundleInfo
struct CatalogItemBundleInfo_t1005829164;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PlayFab.ClientModels.CatalogItem
struct  CatalogItem_t4132589244  : public Il2CppObject
{
public:
	// System.String PlayFab.ClientModels.CatalogItem::<ItemId>k__BackingField
	String_t* ___U3CItemIdU3Ek__BackingField_0;
	// System.String PlayFab.ClientModels.CatalogItem::<ItemClass>k__BackingField
	String_t* ___U3CItemClassU3Ek__BackingField_1;
	// System.String PlayFab.ClientModels.CatalogItem::<CatalogVersion>k__BackingField
	String_t* ___U3CCatalogVersionU3Ek__BackingField_2;
	// System.String PlayFab.ClientModels.CatalogItem::<DisplayName>k__BackingField
	String_t* ___U3CDisplayNameU3Ek__BackingField_3;
	// System.String PlayFab.ClientModels.CatalogItem::<Description>k__BackingField
	String_t* ___U3CDescriptionU3Ek__BackingField_4;
	// System.Collections.Generic.Dictionary`2<System.String,System.UInt32> PlayFab.ClientModels.CatalogItem::<VirtualCurrencyPrices>k__BackingField
	Dictionary_2_t2623623230 * ___U3CVirtualCurrencyPricesU3Ek__BackingField_5;
	// System.Collections.Generic.Dictionary`2<System.String,System.UInt32> PlayFab.ClientModels.CatalogItem::<RealCurrencyPrices>k__BackingField
	Dictionary_2_t2623623230 * ___U3CRealCurrencyPricesU3Ek__BackingField_6;
	// System.Collections.Generic.List`1<System.String> PlayFab.ClientModels.CatalogItem::<Tags>k__BackingField
	List_1_t1765447871 * ___U3CTagsU3Ek__BackingField_7;
	// System.String PlayFab.ClientModels.CatalogItem::<CustomData>k__BackingField
	String_t* ___U3CCustomDataU3Ek__BackingField_8;
	// PlayFab.ClientModels.CatalogItemConsumableInfo PlayFab.ClientModels.CatalogItem::<Consumable>k__BackingField
	CatalogItemConsumableInfo_t2956282765 * ___U3CConsumableU3Ek__BackingField_9;
	// PlayFab.ClientModels.CatalogItemContainerInfo PlayFab.ClientModels.CatalogItem::<Container>k__BackingField
	CatalogItemContainerInfo_t284142995 * ___U3CContainerU3Ek__BackingField_10;
	// PlayFab.ClientModels.CatalogItemBundleInfo PlayFab.ClientModels.CatalogItem::<Bundle>k__BackingField
	CatalogItemBundleInfo_t1005829164 * ___U3CBundleU3Ek__BackingField_11;
	// System.Boolean PlayFab.ClientModels.CatalogItem::<CanBecomeCharacter>k__BackingField
	bool ___U3CCanBecomeCharacterU3Ek__BackingField_12;
	// System.Boolean PlayFab.ClientModels.CatalogItem::<IsStackable>k__BackingField
	bool ___U3CIsStackableU3Ek__BackingField_13;
	// System.Boolean PlayFab.ClientModels.CatalogItem::<IsTradable>k__BackingField
	bool ___U3CIsTradableU3Ek__BackingField_14;
	// System.String PlayFab.ClientModels.CatalogItem::<ItemImageUrl>k__BackingField
	String_t* ___U3CItemImageUrlU3Ek__BackingField_15;

public:
	inline static int32_t get_offset_of_U3CItemIdU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(CatalogItem_t4132589244, ___U3CItemIdU3Ek__BackingField_0)); }
	inline String_t* get_U3CItemIdU3Ek__BackingField_0() const { return ___U3CItemIdU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CItemIdU3Ek__BackingField_0() { return &___U3CItemIdU3Ek__BackingField_0; }
	inline void set_U3CItemIdU3Ek__BackingField_0(String_t* value)
	{
		___U3CItemIdU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier(&___U3CItemIdU3Ek__BackingField_0, value);
	}

	inline static int32_t get_offset_of_U3CItemClassU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(CatalogItem_t4132589244, ___U3CItemClassU3Ek__BackingField_1)); }
	inline String_t* get_U3CItemClassU3Ek__BackingField_1() const { return ___U3CItemClassU3Ek__BackingField_1; }
	inline String_t** get_address_of_U3CItemClassU3Ek__BackingField_1() { return &___U3CItemClassU3Ek__BackingField_1; }
	inline void set_U3CItemClassU3Ek__BackingField_1(String_t* value)
	{
		___U3CItemClassU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CItemClassU3Ek__BackingField_1, value);
	}

	inline static int32_t get_offset_of_U3CCatalogVersionU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(CatalogItem_t4132589244, ___U3CCatalogVersionU3Ek__BackingField_2)); }
	inline String_t* get_U3CCatalogVersionU3Ek__BackingField_2() const { return ___U3CCatalogVersionU3Ek__BackingField_2; }
	inline String_t** get_address_of_U3CCatalogVersionU3Ek__BackingField_2() { return &___U3CCatalogVersionU3Ek__BackingField_2; }
	inline void set_U3CCatalogVersionU3Ek__BackingField_2(String_t* value)
	{
		___U3CCatalogVersionU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier(&___U3CCatalogVersionU3Ek__BackingField_2, value);
	}

	inline static int32_t get_offset_of_U3CDisplayNameU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(CatalogItem_t4132589244, ___U3CDisplayNameU3Ek__BackingField_3)); }
	inline String_t* get_U3CDisplayNameU3Ek__BackingField_3() const { return ___U3CDisplayNameU3Ek__BackingField_3; }
	inline String_t** get_address_of_U3CDisplayNameU3Ek__BackingField_3() { return &___U3CDisplayNameU3Ek__BackingField_3; }
	inline void set_U3CDisplayNameU3Ek__BackingField_3(String_t* value)
	{
		___U3CDisplayNameU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier(&___U3CDisplayNameU3Ek__BackingField_3, value);
	}

	inline static int32_t get_offset_of_U3CDescriptionU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(CatalogItem_t4132589244, ___U3CDescriptionU3Ek__BackingField_4)); }
	inline String_t* get_U3CDescriptionU3Ek__BackingField_4() const { return ___U3CDescriptionU3Ek__BackingField_4; }
	inline String_t** get_address_of_U3CDescriptionU3Ek__BackingField_4() { return &___U3CDescriptionU3Ek__BackingField_4; }
	inline void set_U3CDescriptionU3Ek__BackingField_4(String_t* value)
	{
		___U3CDescriptionU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier(&___U3CDescriptionU3Ek__BackingField_4, value);
	}

	inline static int32_t get_offset_of_U3CVirtualCurrencyPricesU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(CatalogItem_t4132589244, ___U3CVirtualCurrencyPricesU3Ek__BackingField_5)); }
	inline Dictionary_2_t2623623230 * get_U3CVirtualCurrencyPricesU3Ek__BackingField_5() const { return ___U3CVirtualCurrencyPricesU3Ek__BackingField_5; }
	inline Dictionary_2_t2623623230 ** get_address_of_U3CVirtualCurrencyPricesU3Ek__BackingField_5() { return &___U3CVirtualCurrencyPricesU3Ek__BackingField_5; }
	inline void set_U3CVirtualCurrencyPricesU3Ek__BackingField_5(Dictionary_2_t2623623230 * value)
	{
		___U3CVirtualCurrencyPricesU3Ek__BackingField_5 = value;
		Il2CppCodeGenWriteBarrier(&___U3CVirtualCurrencyPricesU3Ek__BackingField_5, value);
	}

	inline static int32_t get_offset_of_U3CRealCurrencyPricesU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(CatalogItem_t4132589244, ___U3CRealCurrencyPricesU3Ek__BackingField_6)); }
	inline Dictionary_2_t2623623230 * get_U3CRealCurrencyPricesU3Ek__BackingField_6() const { return ___U3CRealCurrencyPricesU3Ek__BackingField_6; }
	inline Dictionary_2_t2623623230 ** get_address_of_U3CRealCurrencyPricesU3Ek__BackingField_6() { return &___U3CRealCurrencyPricesU3Ek__BackingField_6; }
	inline void set_U3CRealCurrencyPricesU3Ek__BackingField_6(Dictionary_2_t2623623230 * value)
	{
		___U3CRealCurrencyPricesU3Ek__BackingField_6 = value;
		Il2CppCodeGenWriteBarrier(&___U3CRealCurrencyPricesU3Ek__BackingField_6, value);
	}

	inline static int32_t get_offset_of_U3CTagsU3Ek__BackingField_7() { return static_cast<int32_t>(offsetof(CatalogItem_t4132589244, ___U3CTagsU3Ek__BackingField_7)); }
	inline List_1_t1765447871 * get_U3CTagsU3Ek__BackingField_7() const { return ___U3CTagsU3Ek__BackingField_7; }
	inline List_1_t1765447871 ** get_address_of_U3CTagsU3Ek__BackingField_7() { return &___U3CTagsU3Ek__BackingField_7; }
	inline void set_U3CTagsU3Ek__BackingField_7(List_1_t1765447871 * value)
	{
		___U3CTagsU3Ek__BackingField_7 = value;
		Il2CppCodeGenWriteBarrier(&___U3CTagsU3Ek__BackingField_7, value);
	}

	inline static int32_t get_offset_of_U3CCustomDataU3Ek__BackingField_8() { return static_cast<int32_t>(offsetof(CatalogItem_t4132589244, ___U3CCustomDataU3Ek__BackingField_8)); }
	inline String_t* get_U3CCustomDataU3Ek__BackingField_8() const { return ___U3CCustomDataU3Ek__BackingField_8; }
	inline String_t** get_address_of_U3CCustomDataU3Ek__BackingField_8() { return &___U3CCustomDataU3Ek__BackingField_8; }
	inline void set_U3CCustomDataU3Ek__BackingField_8(String_t* value)
	{
		___U3CCustomDataU3Ek__BackingField_8 = value;
		Il2CppCodeGenWriteBarrier(&___U3CCustomDataU3Ek__BackingField_8, value);
	}

	inline static int32_t get_offset_of_U3CConsumableU3Ek__BackingField_9() { return static_cast<int32_t>(offsetof(CatalogItem_t4132589244, ___U3CConsumableU3Ek__BackingField_9)); }
	inline CatalogItemConsumableInfo_t2956282765 * get_U3CConsumableU3Ek__BackingField_9() const { return ___U3CConsumableU3Ek__BackingField_9; }
	inline CatalogItemConsumableInfo_t2956282765 ** get_address_of_U3CConsumableU3Ek__BackingField_9() { return &___U3CConsumableU3Ek__BackingField_9; }
	inline void set_U3CConsumableU3Ek__BackingField_9(CatalogItemConsumableInfo_t2956282765 * value)
	{
		___U3CConsumableU3Ek__BackingField_9 = value;
		Il2CppCodeGenWriteBarrier(&___U3CConsumableU3Ek__BackingField_9, value);
	}

	inline static int32_t get_offset_of_U3CContainerU3Ek__BackingField_10() { return static_cast<int32_t>(offsetof(CatalogItem_t4132589244, ___U3CContainerU3Ek__BackingField_10)); }
	inline CatalogItemContainerInfo_t284142995 * get_U3CContainerU3Ek__BackingField_10() const { return ___U3CContainerU3Ek__BackingField_10; }
	inline CatalogItemContainerInfo_t284142995 ** get_address_of_U3CContainerU3Ek__BackingField_10() { return &___U3CContainerU3Ek__BackingField_10; }
	inline void set_U3CContainerU3Ek__BackingField_10(CatalogItemContainerInfo_t284142995 * value)
	{
		___U3CContainerU3Ek__BackingField_10 = value;
		Il2CppCodeGenWriteBarrier(&___U3CContainerU3Ek__BackingField_10, value);
	}

	inline static int32_t get_offset_of_U3CBundleU3Ek__BackingField_11() { return static_cast<int32_t>(offsetof(CatalogItem_t4132589244, ___U3CBundleU3Ek__BackingField_11)); }
	inline CatalogItemBundleInfo_t1005829164 * get_U3CBundleU3Ek__BackingField_11() const { return ___U3CBundleU3Ek__BackingField_11; }
	inline CatalogItemBundleInfo_t1005829164 ** get_address_of_U3CBundleU3Ek__BackingField_11() { return &___U3CBundleU3Ek__BackingField_11; }
	inline void set_U3CBundleU3Ek__BackingField_11(CatalogItemBundleInfo_t1005829164 * value)
	{
		___U3CBundleU3Ek__BackingField_11 = value;
		Il2CppCodeGenWriteBarrier(&___U3CBundleU3Ek__BackingField_11, value);
	}

	inline static int32_t get_offset_of_U3CCanBecomeCharacterU3Ek__BackingField_12() { return static_cast<int32_t>(offsetof(CatalogItem_t4132589244, ___U3CCanBecomeCharacterU3Ek__BackingField_12)); }
	inline bool get_U3CCanBecomeCharacterU3Ek__BackingField_12() const { return ___U3CCanBecomeCharacterU3Ek__BackingField_12; }
	inline bool* get_address_of_U3CCanBecomeCharacterU3Ek__BackingField_12() { return &___U3CCanBecomeCharacterU3Ek__BackingField_12; }
	inline void set_U3CCanBecomeCharacterU3Ek__BackingField_12(bool value)
	{
		___U3CCanBecomeCharacterU3Ek__BackingField_12 = value;
	}

	inline static int32_t get_offset_of_U3CIsStackableU3Ek__BackingField_13() { return static_cast<int32_t>(offsetof(CatalogItem_t4132589244, ___U3CIsStackableU3Ek__BackingField_13)); }
	inline bool get_U3CIsStackableU3Ek__BackingField_13() const { return ___U3CIsStackableU3Ek__BackingField_13; }
	inline bool* get_address_of_U3CIsStackableU3Ek__BackingField_13() { return &___U3CIsStackableU3Ek__BackingField_13; }
	inline void set_U3CIsStackableU3Ek__BackingField_13(bool value)
	{
		___U3CIsStackableU3Ek__BackingField_13 = value;
	}

	inline static int32_t get_offset_of_U3CIsTradableU3Ek__BackingField_14() { return static_cast<int32_t>(offsetof(CatalogItem_t4132589244, ___U3CIsTradableU3Ek__BackingField_14)); }
	inline bool get_U3CIsTradableU3Ek__BackingField_14() const { return ___U3CIsTradableU3Ek__BackingField_14; }
	inline bool* get_address_of_U3CIsTradableU3Ek__BackingField_14() { return &___U3CIsTradableU3Ek__BackingField_14; }
	inline void set_U3CIsTradableU3Ek__BackingField_14(bool value)
	{
		___U3CIsTradableU3Ek__BackingField_14 = value;
	}

	inline static int32_t get_offset_of_U3CItemImageUrlU3Ek__BackingField_15() { return static_cast<int32_t>(offsetof(CatalogItem_t4132589244, ___U3CItemImageUrlU3Ek__BackingField_15)); }
	inline String_t* get_U3CItemImageUrlU3Ek__BackingField_15() const { return ___U3CItemImageUrlU3Ek__BackingField_15; }
	inline String_t** get_address_of_U3CItemImageUrlU3Ek__BackingField_15() { return &___U3CItemImageUrlU3Ek__BackingField_15; }
	inline void set_U3CItemImageUrlU3Ek__BackingField_15(String_t* value)
	{
		___U3CItemImageUrlU3Ek__BackingField_15 = value;
		Il2CppCodeGenWriteBarrier(&___U3CItemImageUrlU3Ek__BackingField_15, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
