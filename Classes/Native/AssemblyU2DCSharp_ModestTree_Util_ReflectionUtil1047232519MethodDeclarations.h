﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Type
struct Type_t;
// System.Collections.IList
struct IList_t1612618265;
// System.Object[]
struct ObjectU5BU5D_t11523773;
// System.Collections.IDictionary
struct IDictionary_t1654916945;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Type2779229935.h"

// System.Boolean ModestTree.Util.ReflectionUtil::IsGenericList(System.Type)
extern "C"  bool ReflectionUtil_IsGenericList_m3481035286 (Il2CppObject * __this /* static, unused */, Type_t * ___type0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ModestTree.Util.ReflectionUtil::IsGenericList(System.Type,System.Type&)
extern "C"  bool ReflectionUtil_IsGenericList_m2361690275 (Il2CppObject * __this /* static, unused */, Type_t * ___type0, Type_t ** ___contentsType1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IList ModestTree.Util.ReflectionUtil::CreateGenericList(System.Type,System.Object[])
extern "C"  Il2CppObject * ReflectionUtil_CreateGenericList_m3532283214 (Il2CppObject * __this /* static, unused */, Type_t * ___elementType0, ObjectU5BU5D_t11523773* ___contentsAsObj1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IDictionary ModestTree.Util.ReflectionUtil::CreateGenericDictionary(System.Type,System.Type,System.Object[],System.Object[])
extern "C"  Il2CppObject * ReflectionUtil_CreateGenericDictionary_m2439713021 (Il2CppObject * __this /* static, unused */, Type_t * ___keyType0, Type_t * ___valueType1, ObjectU5BU5D_t11523773* ___keysAsObj2, ObjectU5BU5D_t11523773* ___valuesAsObj3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
