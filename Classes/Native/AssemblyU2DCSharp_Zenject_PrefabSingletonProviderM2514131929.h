﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Generic.Dictionary`2<Zenject.PrefabSingletonId,Zenject.PrefabSingletonLazyCreator>
struct Dictionary_2_t3888612577;
// Zenject.DiContainer
struct DiContainer_t2383114449;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.PrefabSingletonProviderMap
struct  PrefabSingletonProviderMap_t2514131929  : public Il2CppObject
{
public:
	// System.Collections.Generic.Dictionary`2<Zenject.PrefabSingletonId,Zenject.PrefabSingletonLazyCreator> Zenject.PrefabSingletonProviderMap::_creators
	Dictionary_2_t3888612577 * ____creators_0;
	// Zenject.DiContainer Zenject.PrefabSingletonProviderMap::_container
	DiContainer_t2383114449 * ____container_1;

public:
	inline static int32_t get_offset_of__creators_0() { return static_cast<int32_t>(offsetof(PrefabSingletonProviderMap_t2514131929, ____creators_0)); }
	inline Dictionary_2_t3888612577 * get__creators_0() const { return ____creators_0; }
	inline Dictionary_2_t3888612577 ** get_address_of__creators_0() { return &____creators_0; }
	inline void set__creators_0(Dictionary_2_t3888612577 * value)
	{
		____creators_0 = value;
		Il2CppCodeGenWriteBarrier(&____creators_0, value);
	}

	inline static int32_t get_offset_of__container_1() { return static_cast<int32_t>(offsetof(PrefabSingletonProviderMap_t2514131929, ____container_1)); }
	inline DiContainer_t2383114449 * get__container_1() const { return ____container_1; }
	inline DiContainer_t2383114449 ** get_address_of__container_1() { return &____container_1; }
	inline void set__container_1(DiContainer_t2383114449 * value)
	{
		____container_1 = value;
		Il2CppCodeGenWriteBarrier(&____container_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
