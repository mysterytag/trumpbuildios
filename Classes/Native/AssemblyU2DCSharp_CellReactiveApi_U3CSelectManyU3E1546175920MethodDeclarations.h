﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CellReactiveApi/<SelectMany>c__AnonStorey119`3/<SelectMany>c__AnonStorey11A`3<System.Int32,System.Int32,System.Int32>
struct U3CSelectManyU3Ec__AnonStorey11A_3_t1546175920;

#include "codegen/il2cpp-codegen.h"

// System.Void CellReactiveApi/<SelectMany>c__AnonStorey119`3/<SelectMany>c__AnonStorey11A`3<System.Int32,System.Int32,System.Int32>::.ctor()
extern "C"  void U3CSelectManyU3Ec__AnonStorey11A_3__ctor_m462548113_gshared (U3CSelectManyU3Ec__AnonStorey11A_3_t1546175920 * __this, const MethodInfo* method);
#define U3CSelectManyU3Ec__AnonStorey11A_3__ctor_m462548113(__this, method) ((  void (*) (U3CSelectManyU3Ec__AnonStorey11A_3_t1546175920 *, const MethodInfo*))U3CSelectManyU3Ec__AnonStorey11A_3__ctor_m462548113_gshared)(__this, method)
// TR CellReactiveApi/<SelectMany>c__AnonStorey119`3/<SelectMany>c__AnonStorey11A`3<System.Int32,System.Int32,System.Int32>::<>m__19F(TC)
extern "C"  int32_t U3CSelectManyU3Ec__AnonStorey11A_3_U3CU3Em__19F_m1015973816_gshared (U3CSelectManyU3Ec__AnonStorey11A_3_t1546175920 * __this, int32_t ___y0, const MethodInfo* method);
#define U3CSelectManyU3Ec__AnonStorey11A_3_U3CU3Em__19F_m1015973816(__this, ___y0, method) ((  int32_t (*) (U3CSelectManyU3Ec__AnonStorey11A_3_t1546175920 *, int32_t, const MethodInfo*))U3CSelectManyU3Ec__AnonStorey11A_3_U3CU3Em__19F_m1015973816_gshared)(__this, ___y0, method)
