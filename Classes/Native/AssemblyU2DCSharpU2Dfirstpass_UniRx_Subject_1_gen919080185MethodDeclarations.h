﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Subject_1_gen4044534903MethodDeclarations.h"

// System.Void UniRx.Subject`1<UniRx.Tuple`2<UnityEngine.BitStream,UnityEngine.NetworkMessageInfo>>::.ctor()
#define Subject_1__ctor_m1167901482(__this, method) ((  void (*) (Subject_1_t919080185 *, const MethodInfo*))Subject_1__ctor_m1110123634_gshared)(__this, method)
// System.Boolean UniRx.Subject`1<UniRx.Tuple`2<UnityEngine.BitStream,UnityEngine.NetworkMessageInfo>>::get_HasObservers()
#define Subject_1_get_HasObservers_m3031580450(__this, method) ((  bool (*) (Subject_1_t919080185 *, const MethodInfo*))Subject_1_get_HasObservers_m856497370_gshared)(__this, method)
// System.Void UniRx.Subject`1<UniRx.Tuple`2<UnityEngine.BitStream,UnityEngine.NetworkMessageInfo>>::OnCompleted()
#define Subject_1_OnCompleted_m510619444(__this, method) ((  void (*) (Subject_1_t919080185 *, const MethodInfo*))Subject_1_OnCompleted_m363969148_gshared)(__this, method)
// System.Void UniRx.Subject`1<UniRx.Tuple`2<UnityEngine.BitStream,UnityEngine.NetworkMessageInfo>>::OnError(System.Exception)
#define Subject_1_OnError_m1325986401(__this, ___error0, method) ((  void (*) (Subject_1_t919080185 *, Exception_t1967233988 *, const MethodInfo*))Subject_1_OnError_m3943469481_gshared)(__this, ___error0, method)
// System.Void UniRx.Subject`1<UniRx.Tuple`2<UnityEngine.BitStream,UnityEngine.NetworkMessageInfo>>::OnNext(T)
#define Subject_1_OnNext_m4197563218(__this, ___value0, method) ((  void (*) (Subject_1_t919080185 *, Tuple_2_t3276012861 , const MethodInfo*))Subject_1_OnNext_m212658842_gshared)(__this, ___value0, method)
// System.IDisposable UniRx.Subject`1<UniRx.Tuple`2<UnityEngine.BitStream,UnityEngine.NetworkMessageInfo>>::Subscribe(UniRx.IObserver`1<T>)
#define Subject_1_Subscribe_m2972729705(__this, ___observer0, method) ((  Il2CppObject * (*) (Subject_1_t919080185 *, Il2CppObject*, const MethodInfo*))Subject_1_Subscribe_m314425265_gshared)(__this, ___observer0, method)
// System.Void UniRx.Subject`1<UniRx.Tuple`2<UnityEngine.BitStream,UnityEngine.NetworkMessageInfo>>::Dispose()
#define Subject_1_Dispose_m1265143015(__this, method) ((  void (*) (Subject_1_t919080185 *, const MethodInfo*))Subject_1_Dispose_m1575205935_gshared)(__this, method)
// System.Void UniRx.Subject`1<UniRx.Tuple`2<UnityEngine.BitStream,UnityEngine.NetworkMessageInfo>>::ThrowIfDisposed()
#define Subject_1_ThrowIfDisposed_m2834980720(__this, method) ((  void (*) (Subject_1_t919080185 *, const MethodInfo*))Subject_1_ThrowIfDisposed_m1410713272_gshared)(__this, method)
// System.Boolean UniRx.Subject`1<UniRx.Tuple`2<UnityEngine.BitStream,UnityEngine.NetworkMessageInfo>>::IsRequiredSubscribeOnCurrentThread()
#define Subject_1_IsRequiredSubscribeOnCurrentThread_m3217942425(__this, method) ((  bool (*) (Subject_1_t919080185 *, const MethodInfo*))Subject_1_IsRequiredSubscribeOnCurrentThread_m75614033_gshared)(__this, method)
