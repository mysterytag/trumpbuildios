﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.SampleFrameObservable`1/SampleFrame<System.Object>
struct SampleFrame_t1580910916;
// UniRx.Operators.SampleFrameObservable`1<System.Object>
struct SampleFrameObservable_1_t1335449437;
// UniRx.IObserver`1<System.Object>
struct IObserver_1_t3049105323;
// System.IDisposable
struct IDisposable_t1628921374;
// System.Object
struct Il2CppObject;
// System.Exception
struct Exception_t1967233988;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_Exception1967233988.h"

// System.Void UniRx.Operators.SampleFrameObservable`1/SampleFrame<System.Object>::.ctor(UniRx.Operators.SampleFrameObservable`1<T>,UniRx.IObserver`1<T>,System.IDisposable)
extern "C"  void SampleFrame__ctor_m329503525_gshared (SampleFrame_t1580910916 * __this, SampleFrameObservable_1_t1335449437 * ___parent0, Il2CppObject* ___observer1, Il2CppObject * ___cancel2, const MethodInfo* method);
#define SampleFrame__ctor_m329503525(__this, ___parent0, ___observer1, ___cancel2, method) ((  void (*) (SampleFrame_t1580910916 *, SampleFrameObservable_1_t1335449437 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))SampleFrame__ctor_m329503525_gshared)(__this, ___parent0, ___observer1, ___cancel2, method)
// System.IDisposable UniRx.Operators.SampleFrameObservable`1/SampleFrame<System.Object>::Run()
extern "C"  Il2CppObject * SampleFrame_Run_m3436287477_gshared (SampleFrame_t1580910916 * __this, const MethodInfo* method);
#define SampleFrame_Run_m3436287477(__this, method) ((  Il2CppObject * (*) (SampleFrame_t1580910916 *, const MethodInfo*))SampleFrame_Run_m3436287477_gshared)(__this, method)
// System.Void UniRx.Operators.SampleFrameObservable`1/SampleFrame<System.Object>::OnNextTick(System.Int64)
extern "C"  void SampleFrame_OnNextTick_m2822841278_gshared (SampleFrame_t1580910916 * __this, int64_t ____0, const MethodInfo* method);
#define SampleFrame_OnNextTick_m2822841278(__this, ____0, method) ((  void (*) (SampleFrame_t1580910916 *, int64_t, const MethodInfo*))SampleFrame_OnNextTick_m2822841278_gshared)(__this, ____0, method)
// System.Void UniRx.Operators.SampleFrameObservable`1/SampleFrame<System.Object>::OnNext(T)
extern "C"  void SampleFrame_OnNext_m626930127_gshared (SampleFrame_t1580910916 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define SampleFrame_OnNext_m626930127(__this, ___value0, method) ((  void (*) (SampleFrame_t1580910916 *, Il2CppObject *, const MethodInfo*))SampleFrame_OnNext_m626930127_gshared)(__this, ___value0, method)
// System.Void UniRx.Operators.SampleFrameObservable`1/SampleFrame<System.Object>::OnError(System.Exception)
extern "C"  void SampleFrame_OnError_m1367002846_gshared (SampleFrame_t1580910916 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method);
#define SampleFrame_OnError_m1367002846(__this, ___error0, method) ((  void (*) (SampleFrame_t1580910916 *, Exception_t1967233988 *, const MethodInfo*))SampleFrame_OnError_m1367002846_gshared)(__this, ___error0, method)
// System.Void UniRx.Operators.SampleFrameObservable`1/SampleFrame<System.Object>::OnCompleted()
extern "C"  void SampleFrame_OnCompleted_m1498570545_gshared (SampleFrame_t1580910916 * __this, const MethodInfo* method);
#define SampleFrame_OnCompleted_m1498570545(__this, method) ((  void (*) (SampleFrame_t1580910916 *, const MethodInfo*))SampleFrame_OnCompleted_m1498570545_gshared)(__this, method)
