﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// System.Collections.Generic.List`1<System.String>
struct List_1_t1765447871;
// System.Collections.Generic.Dictionary`2<System.String,System.String>
struct Dictionary_2_t2606186806;

#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_Nullable_1_gen3225071844.h"
#include "mscorlib_System_Nullable_1_gen1438485399.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PlayFab.ClientModels.ItemInstance
struct  ItemInstance_t525144312  : public Il2CppObject
{
public:
	// System.String PlayFab.ClientModels.ItemInstance::<ItemId>k__BackingField
	String_t* ___U3CItemIdU3Ek__BackingField_0;
	// System.String PlayFab.ClientModels.ItemInstance::<ItemInstanceId>k__BackingField
	String_t* ___U3CItemInstanceIdU3Ek__BackingField_1;
	// System.String PlayFab.ClientModels.ItemInstance::<ItemClass>k__BackingField
	String_t* ___U3CItemClassU3Ek__BackingField_2;
	// System.Nullable`1<System.DateTime> PlayFab.ClientModels.ItemInstance::<PurchaseDate>k__BackingField
	Nullable_1_t3225071844  ___U3CPurchaseDateU3Ek__BackingField_3;
	// System.Nullable`1<System.DateTime> PlayFab.ClientModels.ItemInstance::<Expiration>k__BackingField
	Nullable_1_t3225071844  ___U3CExpirationU3Ek__BackingField_4;
	// System.Nullable`1<System.Int32> PlayFab.ClientModels.ItemInstance::<RemainingUses>k__BackingField
	Nullable_1_t1438485399  ___U3CRemainingUsesU3Ek__BackingField_5;
	// System.Nullable`1<System.Int32> PlayFab.ClientModels.ItemInstance::<UsesIncrementedBy>k__BackingField
	Nullable_1_t1438485399  ___U3CUsesIncrementedByU3Ek__BackingField_6;
	// System.String PlayFab.ClientModels.ItemInstance::<Annotation>k__BackingField
	String_t* ___U3CAnnotationU3Ek__BackingField_7;
	// System.String PlayFab.ClientModels.ItemInstance::<CatalogVersion>k__BackingField
	String_t* ___U3CCatalogVersionU3Ek__BackingField_8;
	// System.String PlayFab.ClientModels.ItemInstance::<BundleParent>k__BackingField
	String_t* ___U3CBundleParentU3Ek__BackingField_9;
	// System.String PlayFab.ClientModels.ItemInstance::<DisplayName>k__BackingField
	String_t* ___U3CDisplayNameU3Ek__BackingField_10;
	// System.String PlayFab.ClientModels.ItemInstance::<UnitCurrency>k__BackingField
	String_t* ___U3CUnitCurrencyU3Ek__BackingField_11;
	// System.UInt32 PlayFab.ClientModels.ItemInstance::<UnitPrice>k__BackingField
	uint32_t ___U3CUnitPriceU3Ek__BackingField_12;
	// System.Collections.Generic.List`1<System.String> PlayFab.ClientModels.ItemInstance::<BundleContents>k__BackingField
	List_1_t1765447871 * ___U3CBundleContentsU3Ek__BackingField_13;
	// System.Collections.Generic.Dictionary`2<System.String,System.String> PlayFab.ClientModels.ItemInstance::<CustomData>k__BackingField
	Dictionary_2_t2606186806 * ___U3CCustomDataU3Ek__BackingField_14;

public:
	inline static int32_t get_offset_of_U3CItemIdU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(ItemInstance_t525144312, ___U3CItemIdU3Ek__BackingField_0)); }
	inline String_t* get_U3CItemIdU3Ek__BackingField_0() const { return ___U3CItemIdU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CItemIdU3Ek__BackingField_0() { return &___U3CItemIdU3Ek__BackingField_0; }
	inline void set_U3CItemIdU3Ek__BackingField_0(String_t* value)
	{
		___U3CItemIdU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier(&___U3CItemIdU3Ek__BackingField_0, value);
	}

	inline static int32_t get_offset_of_U3CItemInstanceIdU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(ItemInstance_t525144312, ___U3CItemInstanceIdU3Ek__BackingField_1)); }
	inline String_t* get_U3CItemInstanceIdU3Ek__BackingField_1() const { return ___U3CItemInstanceIdU3Ek__BackingField_1; }
	inline String_t** get_address_of_U3CItemInstanceIdU3Ek__BackingField_1() { return &___U3CItemInstanceIdU3Ek__BackingField_1; }
	inline void set_U3CItemInstanceIdU3Ek__BackingField_1(String_t* value)
	{
		___U3CItemInstanceIdU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CItemInstanceIdU3Ek__BackingField_1, value);
	}

	inline static int32_t get_offset_of_U3CItemClassU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(ItemInstance_t525144312, ___U3CItemClassU3Ek__BackingField_2)); }
	inline String_t* get_U3CItemClassU3Ek__BackingField_2() const { return ___U3CItemClassU3Ek__BackingField_2; }
	inline String_t** get_address_of_U3CItemClassU3Ek__BackingField_2() { return &___U3CItemClassU3Ek__BackingField_2; }
	inline void set_U3CItemClassU3Ek__BackingField_2(String_t* value)
	{
		___U3CItemClassU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier(&___U3CItemClassU3Ek__BackingField_2, value);
	}

	inline static int32_t get_offset_of_U3CPurchaseDateU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(ItemInstance_t525144312, ___U3CPurchaseDateU3Ek__BackingField_3)); }
	inline Nullable_1_t3225071844  get_U3CPurchaseDateU3Ek__BackingField_3() const { return ___U3CPurchaseDateU3Ek__BackingField_3; }
	inline Nullable_1_t3225071844 * get_address_of_U3CPurchaseDateU3Ek__BackingField_3() { return &___U3CPurchaseDateU3Ek__BackingField_3; }
	inline void set_U3CPurchaseDateU3Ek__BackingField_3(Nullable_1_t3225071844  value)
	{
		___U3CPurchaseDateU3Ek__BackingField_3 = value;
	}

	inline static int32_t get_offset_of_U3CExpirationU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(ItemInstance_t525144312, ___U3CExpirationU3Ek__BackingField_4)); }
	inline Nullable_1_t3225071844  get_U3CExpirationU3Ek__BackingField_4() const { return ___U3CExpirationU3Ek__BackingField_4; }
	inline Nullable_1_t3225071844 * get_address_of_U3CExpirationU3Ek__BackingField_4() { return &___U3CExpirationU3Ek__BackingField_4; }
	inline void set_U3CExpirationU3Ek__BackingField_4(Nullable_1_t3225071844  value)
	{
		___U3CExpirationU3Ek__BackingField_4 = value;
	}

	inline static int32_t get_offset_of_U3CRemainingUsesU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(ItemInstance_t525144312, ___U3CRemainingUsesU3Ek__BackingField_5)); }
	inline Nullable_1_t1438485399  get_U3CRemainingUsesU3Ek__BackingField_5() const { return ___U3CRemainingUsesU3Ek__BackingField_5; }
	inline Nullable_1_t1438485399 * get_address_of_U3CRemainingUsesU3Ek__BackingField_5() { return &___U3CRemainingUsesU3Ek__BackingField_5; }
	inline void set_U3CRemainingUsesU3Ek__BackingField_5(Nullable_1_t1438485399  value)
	{
		___U3CRemainingUsesU3Ek__BackingField_5 = value;
	}

	inline static int32_t get_offset_of_U3CUsesIncrementedByU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(ItemInstance_t525144312, ___U3CUsesIncrementedByU3Ek__BackingField_6)); }
	inline Nullable_1_t1438485399  get_U3CUsesIncrementedByU3Ek__BackingField_6() const { return ___U3CUsesIncrementedByU3Ek__BackingField_6; }
	inline Nullable_1_t1438485399 * get_address_of_U3CUsesIncrementedByU3Ek__BackingField_6() { return &___U3CUsesIncrementedByU3Ek__BackingField_6; }
	inline void set_U3CUsesIncrementedByU3Ek__BackingField_6(Nullable_1_t1438485399  value)
	{
		___U3CUsesIncrementedByU3Ek__BackingField_6 = value;
	}

	inline static int32_t get_offset_of_U3CAnnotationU3Ek__BackingField_7() { return static_cast<int32_t>(offsetof(ItemInstance_t525144312, ___U3CAnnotationU3Ek__BackingField_7)); }
	inline String_t* get_U3CAnnotationU3Ek__BackingField_7() const { return ___U3CAnnotationU3Ek__BackingField_7; }
	inline String_t** get_address_of_U3CAnnotationU3Ek__BackingField_7() { return &___U3CAnnotationU3Ek__BackingField_7; }
	inline void set_U3CAnnotationU3Ek__BackingField_7(String_t* value)
	{
		___U3CAnnotationU3Ek__BackingField_7 = value;
		Il2CppCodeGenWriteBarrier(&___U3CAnnotationU3Ek__BackingField_7, value);
	}

	inline static int32_t get_offset_of_U3CCatalogVersionU3Ek__BackingField_8() { return static_cast<int32_t>(offsetof(ItemInstance_t525144312, ___U3CCatalogVersionU3Ek__BackingField_8)); }
	inline String_t* get_U3CCatalogVersionU3Ek__BackingField_8() const { return ___U3CCatalogVersionU3Ek__BackingField_8; }
	inline String_t** get_address_of_U3CCatalogVersionU3Ek__BackingField_8() { return &___U3CCatalogVersionU3Ek__BackingField_8; }
	inline void set_U3CCatalogVersionU3Ek__BackingField_8(String_t* value)
	{
		___U3CCatalogVersionU3Ek__BackingField_8 = value;
		Il2CppCodeGenWriteBarrier(&___U3CCatalogVersionU3Ek__BackingField_8, value);
	}

	inline static int32_t get_offset_of_U3CBundleParentU3Ek__BackingField_9() { return static_cast<int32_t>(offsetof(ItemInstance_t525144312, ___U3CBundleParentU3Ek__BackingField_9)); }
	inline String_t* get_U3CBundleParentU3Ek__BackingField_9() const { return ___U3CBundleParentU3Ek__BackingField_9; }
	inline String_t** get_address_of_U3CBundleParentU3Ek__BackingField_9() { return &___U3CBundleParentU3Ek__BackingField_9; }
	inline void set_U3CBundleParentU3Ek__BackingField_9(String_t* value)
	{
		___U3CBundleParentU3Ek__BackingField_9 = value;
		Il2CppCodeGenWriteBarrier(&___U3CBundleParentU3Ek__BackingField_9, value);
	}

	inline static int32_t get_offset_of_U3CDisplayNameU3Ek__BackingField_10() { return static_cast<int32_t>(offsetof(ItemInstance_t525144312, ___U3CDisplayNameU3Ek__BackingField_10)); }
	inline String_t* get_U3CDisplayNameU3Ek__BackingField_10() const { return ___U3CDisplayNameU3Ek__BackingField_10; }
	inline String_t** get_address_of_U3CDisplayNameU3Ek__BackingField_10() { return &___U3CDisplayNameU3Ek__BackingField_10; }
	inline void set_U3CDisplayNameU3Ek__BackingField_10(String_t* value)
	{
		___U3CDisplayNameU3Ek__BackingField_10 = value;
		Il2CppCodeGenWriteBarrier(&___U3CDisplayNameU3Ek__BackingField_10, value);
	}

	inline static int32_t get_offset_of_U3CUnitCurrencyU3Ek__BackingField_11() { return static_cast<int32_t>(offsetof(ItemInstance_t525144312, ___U3CUnitCurrencyU3Ek__BackingField_11)); }
	inline String_t* get_U3CUnitCurrencyU3Ek__BackingField_11() const { return ___U3CUnitCurrencyU3Ek__BackingField_11; }
	inline String_t** get_address_of_U3CUnitCurrencyU3Ek__BackingField_11() { return &___U3CUnitCurrencyU3Ek__BackingField_11; }
	inline void set_U3CUnitCurrencyU3Ek__BackingField_11(String_t* value)
	{
		___U3CUnitCurrencyU3Ek__BackingField_11 = value;
		Il2CppCodeGenWriteBarrier(&___U3CUnitCurrencyU3Ek__BackingField_11, value);
	}

	inline static int32_t get_offset_of_U3CUnitPriceU3Ek__BackingField_12() { return static_cast<int32_t>(offsetof(ItemInstance_t525144312, ___U3CUnitPriceU3Ek__BackingField_12)); }
	inline uint32_t get_U3CUnitPriceU3Ek__BackingField_12() const { return ___U3CUnitPriceU3Ek__BackingField_12; }
	inline uint32_t* get_address_of_U3CUnitPriceU3Ek__BackingField_12() { return &___U3CUnitPriceU3Ek__BackingField_12; }
	inline void set_U3CUnitPriceU3Ek__BackingField_12(uint32_t value)
	{
		___U3CUnitPriceU3Ek__BackingField_12 = value;
	}

	inline static int32_t get_offset_of_U3CBundleContentsU3Ek__BackingField_13() { return static_cast<int32_t>(offsetof(ItemInstance_t525144312, ___U3CBundleContentsU3Ek__BackingField_13)); }
	inline List_1_t1765447871 * get_U3CBundleContentsU3Ek__BackingField_13() const { return ___U3CBundleContentsU3Ek__BackingField_13; }
	inline List_1_t1765447871 ** get_address_of_U3CBundleContentsU3Ek__BackingField_13() { return &___U3CBundleContentsU3Ek__BackingField_13; }
	inline void set_U3CBundleContentsU3Ek__BackingField_13(List_1_t1765447871 * value)
	{
		___U3CBundleContentsU3Ek__BackingField_13 = value;
		Il2CppCodeGenWriteBarrier(&___U3CBundleContentsU3Ek__BackingField_13, value);
	}

	inline static int32_t get_offset_of_U3CCustomDataU3Ek__BackingField_14() { return static_cast<int32_t>(offsetof(ItemInstance_t525144312, ___U3CCustomDataU3Ek__BackingField_14)); }
	inline Dictionary_2_t2606186806 * get_U3CCustomDataU3Ek__BackingField_14() const { return ___U3CCustomDataU3Ek__BackingField_14; }
	inline Dictionary_2_t2606186806 ** get_address_of_U3CCustomDataU3Ek__BackingField_14() { return &___U3CCustomDataU3Ek__BackingField_14; }
	inline void set_U3CCustomDataU3Ek__BackingField_14(Dictionary_2_t2606186806 * value)
	{
		___U3CCustomDataU3Ek__BackingField_14 = value;
		Il2CppCodeGenWriteBarrier(&___U3CCustomDataU3Ek__BackingField_14, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
