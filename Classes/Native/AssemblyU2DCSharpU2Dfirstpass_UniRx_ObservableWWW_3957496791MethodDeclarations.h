﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.ObservableWWW/<PostAndGetBytes>c__AnonStorey86
struct U3CPostAndGetBytesU3Ec__AnonStorey86_t3957496791;
// System.Collections.IEnumerator
struct IEnumerator_t287207039;
// UniRx.IObserver`1<System.Byte[]>
struct IObserver_1_t2270505063;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_CancellationTo1439151560.h"

// System.Void UniRx.ObservableWWW/<PostAndGetBytes>c__AnonStorey86::.ctor()
extern "C"  void U3CPostAndGetBytesU3Ec__AnonStorey86__ctor_m4176800645 (U3CPostAndGetBytesU3Ec__AnonStorey86_t3957496791 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator UniRx.ObservableWWW/<PostAndGetBytes>c__AnonStorey86::<>m__B2(UniRx.IObserver`1<System.Byte[]>,UniRx.CancellationToken)
extern "C"  Il2CppObject * U3CPostAndGetBytesU3Ec__AnonStorey86_U3CU3Em__B2_m3151907907 (U3CPostAndGetBytesU3Ec__AnonStorey86_t3957496791 * __this, Il2CppObject* ___observer0, CancellationToken_t1439151560  ___cancellation1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
