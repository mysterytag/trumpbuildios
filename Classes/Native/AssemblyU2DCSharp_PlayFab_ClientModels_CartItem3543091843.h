﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// System.Collections.Generic.Dictionary`2<System.String,System.UInt32>
struct Dictionary_2_t2623623230;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PlayFab.ClientModels.CartItem
struct  CartItem_t3543091843  : public Il2CppObject
{
public:
	// System.String PlayFab.ClientModels.CartItem::<ItemId>k__BackingField
	String_t* ___U3CItemIdU3Ek__BackingField_0;
	// System.String PlayFab.ClientModels.CartItem::<ItemClass>k__BackingField
	String_t* ___U3CItemClassU3Ek__BackingField_1;
	// System.String PlayFab.ClientModels.CartItem::<ItemInstanceId>k__BackingField
	String_t* ___U3CItemInstanceIdU3Ek__BackingField_2;
	// System.String PlayFab.ClientModels.CartItem::<DisplayName>k__BackingField
	String_t* ___U3CDisplayNameU3Ek__BackingField_3;
	// System.String PlayFab.ClientModels.CartItem::<Description>k__BackingField
	String_t* ___U3CDescriptionU3Ek__BackingField_4;
	// System.Collections.Generic.Dictionary`2<System.String,System.UInt32> PlayFab.ClientModels.CartItem::<VirtualCurrencyPrices>k__BackingField
	Dictionary_2_t2623623230 * ___U3CVirtualCurrencyPricesU3Ek__BackingField_5;
	// System.Collections.Generic.Dictionary`2<System.String,System.UInt32> PlayFab.ClientModels.CartItem::<RealCurrencyPrices>k__BackingField
	Dictionary_2_t2623623230 * ___U3CRealCurrencyPricesU3Ek__BackingField_6;
	// System.Collections.Generic.Dictionary`2<System.String,System.UInt32> PlayFab.ClientModels.CartItem::<VCAmount>k__BackingField
	Dictionary_2_t2623623230 * ___U3CVCAmountU3Ek__BackingField_7;

public:
	inline static int32_t get_offset_of_U3CItemIdU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(CartItem_t3543091843, ___U3CItemIdU3Ek__BackingField_0)); }
	inline String_t* get_U3CItemIdU3Ek__BackingField_0() const { return ___U3CItemIdU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CItemIdU3Ek__BackingField_0() { return &___U3CItemIdU3Ek__BackingField_0; }
	inline void set_U3CItemIdU3Ek__BackingField_0(String_t* value)
	{
		___U3CItemIdU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier(&___U3CItemIdU3Ek__BackingField_0, value);
	}

	inline static int32_t get_offset_of_U3CItemClassU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(CartItem_t3543091843, ___U3CItemClassU3Ek__BackingField_1)); }
	inline String_t* get_U3CItemClassU3Ek__BackingField_1() const { return ___U3CItemClassU3Ek__BackingField_1; }
	inline String_t** get_address_of_U3CItemClassU3Ek__BackingField_1() { return &___U3CItemClassU3Ek__BackingField_1; }
	inline void set_U3CItemClassU3Ek__BackingField_1(String_t* value)
	{
		___U3CItemClassU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CItemClassU3Ek__BackingField_1, value);
	}

	inline static int32_t get_offset_of_U3CItemInstanceIdU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(CartItem_t3543091843, ___U3CItemInstanceIdU3Ek__BackingField_2)); }
	inline String_t* get_U3CItemInstanceIdU3Ek__BackingField_2() const { return ___U3CItemInstanceIdU3Ek__BackingField_2; }
	inline String_t** get_address_of_U3CItemInstanceIdU3Ek__BackingField_2() { return &___U3CItemInstanceIdU3Ek__BackingField_2; }
	inline void set_U3CItemInstanceIdU3Ek__BackingField_2(String_t* value)
	{
		___U3CItemInstanceIdU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier(&___U3CItemInstanceIdU3Ek__BackingField_2, value);
	}

	inline static int32_t get_offset_of_U3CDisplayNameU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(CartItem_t3543091843, ___U3CDisplayNameU3Ek__BackingField_3)); }
	inline String_t* get_U3CDisplayNameU3Ek__BackingField_3() const { return ___U3CDisplayNameU3Ek__BackingField_3; }
	inline String_t** get_address_of_U3CDisplayNameU3Ek__BackingField_3() { return &___U3CDisplayNameU3Ek__BackingField_3; }
	inline void set_U3CDisplayNameU3Ek__BackingField_3(String_t* value)
	{
		___U3CDisplayNameU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier(&___U3CDisplayNameU3Ek__BackingField_3, value);
	}

	inline static int32_t get_offset_of_U3CDescriptionU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(CartItem_t3543091843, ___U3CDescriptionU3Ek__BackingField_4)); }
	inline String_t* get_U3CDescriptionU3Ek__BackingField_4() const { return ___U3CDescriptionU3Ek__BackingField_4; }
	inline String_t** get_address_of_U3CDescriptionU3Ek__BackingField_4() { return &___U3CDescriptionU3Ek__BackingField_4; }
	inline void set_U3CDescriptionU3Ek__BackingField_4(String_t* value)
	{
		___U3CDescriptionU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier(&___U3CDescriptionU3Ek__BackingField_4, value);
	}

	inline static int32_t get_offset_of_U3CVirtualCurrencyPricesU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(CartItem_t3543091843, ___U3CVirtualCurrencyPricesU3Ek__BackingField_5)); }
	inline Dictionary_2_t2623623230 * get_U3CVirtualCurrencyPricesU3Ek__BackingField_5() const { return ___U3CVirtualCurrencyPricesU3Ek__BackingField_5; }
	inline Dictionary_2_t2623623230 ** get_address_of_U3CVirtualCurrencyPricesU3Ek__BackingField_5() { return &___U3CVirtualCurrencyPricesU3Ek__BackingField_5; }
	inline void set_U3CVirtualCurrencyPricesU3Ek__BackingField_5(Dictionary_2_t2623623230 * value)
	{
		___U3CVirtualCurrencyPricesU3Ek__BackingField_5 = value;
		Il2CppCodeGenWriteBarrier(&___U3CVirtualCurrencyPricesU3Ek__BackingField_5, value);
	}

	inline static int32_t get_offset_of_U3CRealCurrencyPricesU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(CartItem_t3543091843, ___U3CRealCurrencyPricesU3Ek__BackingField_6)); }
	inline Dictionary_2_t2623623230 * get_U3CRealCurrencyPricesU3Ek__BackingField_6() const { return ___U3CRealCurrencyPricesU3Ek__BackingField_6; }
	inline Dictionary_2_t2623623230 ** get_address_of_U3CRealCurrencyPricesU3Ek__BackingField_6() { return &___U3CRealCurrencyPricesU3Ek__BackingField_6; }
	inline void set_U3CRealCurrencyPricesU3Ek__BackingField_6(Dictionary_2_t2623623230 * value)
	{
		___U3CRealCurrencyPricesU3Ek__BackingField_6 = value;
		Il2CppCodeGenWriteBarrier(&___U3CRealCurrencyPricesU3Ek__BackingField_6, value);
	}

	inline static int32_t get_offset_of_U3CVCAmountU3Ek__BackingField_7() { return static_cast<int32_t>(offsetof(CartItem_t3543091843, ___U3CVCAmountU3Ek__BackingField_7)); }
	inline Dictionary_2_t2623623230 * get_U3CVCAmountU3Ek__BackingField_7() const { return ___U3CVCAmountU3Ek__BackingField_7; }
	inline Dictionary_2_t2623623230 ** get_address_of_U3CVCAmountU3Ek__BackingField_7() { return &___U3CVCAmountU3Ek__BackingField_7; }
	inline void set_U3CVCAmountU3Ek__BackingField_7(Dictionary_2_t2623623230 * value)
	{
		___U3CVCAmountU3Ek__BackingField_7 = value;
		Il2CppCodeGenWriteBarrier(&___U3CVCAmountU3Ek__BackingField_7, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
