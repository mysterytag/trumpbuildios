﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// DonateCell
struct DonateCell_t2798474385;
// TableButtons/<AddOpenIabButton>c__AnonStorey15C
struct U3CAddOpenIabButtonU3Ec__AnonStorey15C_t3212335977;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TableButtons/<AddOpenIabButton>c__AnonStorey15C/<AddOpenIabButton>c__AnonStorey15D
struct  U3CAddOpenIabButtonU3Ec__AnonStorey15D_t3212335978  : public Il2CppObject
{
public:
	// DonateCell TableButtons/<AddOpenIabButton>c__AnonStorey15C/<AddOpenIabButton>c__AnonStorey15D::cell
	DonateCell_t2798474385 * ___cell_0;
	// TableButtons/<AddOpenIabButton>c__AnonStorey15C TableButtons/<AddOpenIabButton>c__AnonStorey15C/<AddOpenIabButton>c__AnonStorey15D::<>f__ref$348
	U3CAddOpenIabButtonU3Ec__AnonStorey15C_t3212335977 * ___U3CU3Ef__refU24348_1;

public:
	inline static int32_t get_offset_of_cell_0() { return static_cast<int32_t>(offsetof(U3CAddOpenIabButtonU3Ec__AnonStorey15D_t3212335978, ___cell_0)); }
	inline DonateCell_t2798474385 * get_cell_0() const { return ___cell_0; }
	inline DonateCell_t2798474385 ** get_address_of_cell_0() { return &___cell_0; }
	inline void set_cell_0(DonateCell_t2798474385 * value)
	{
		___cell_0 = value;
		Il2CppCodeGenWriteBarrier(&___cell_0, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__refU24348_1() { return static_cast<int32_t>(offsetof(U3CAddOpenIabButtonU3Ec__AnonStorey15D_t3212335978, ___U3CU3Ef__refU24348_1)); }
	inline U3CAddOpenIabButtonU3Ec__AnonStorey15C_t3212335977 * get_U3CU3Ef__refU24348_1() const { return ___U3CU3Ef__refU24348_1; }
	inline U3CAddOpenIabButtonU3Ec__AnonStorey15C_t3212335977 ** get_address_of_U3CU3Ef__refU24348_1() { return &___U3CU3Ef__refU24348_1; }
	inline void set_U3CU3Ef__refU24348_1(U3CAddOpenIabButtonU3Ec__AnonStorey15C_t3212335977 * value)
	{
		___U3CU3Ef__refU24348_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__refU24348_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
