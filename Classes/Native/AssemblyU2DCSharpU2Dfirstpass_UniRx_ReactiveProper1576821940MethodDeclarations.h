﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_ReactiveProper1445439458MethodDeclarations.h"

// System.Void UniRx.ReactiveProperty`1<System.String>::.ctor()
#define ReactiveProperty_1__ctor_m3888404932(__this, method) ((  void (*) (ReactiveProperty_1_t1576821940 *, const MethodInfo*))ReactiveProperty_1__ctor_m2182085490_gshared)(__this, method)
// System.Void UniRx.ReactiveProperty`1<System.String>::.ctor(T)
#define ReactiveProperty_1__ctor_m281469978(__this, ___initialValue0, method) ((  void (*) (ReactiveProperty_1_t1576821940 *, String_t*, const MethodInfo*))ReactiveProperty_1__ctor_m3220142124_gshared)(__this, ___initialValue0, method)
// System.Void UniRx.ReactiveProperty`1<System.String>::.ctor(UniRx.IObservable`1<T>)
#define ReactiveProperty_1__ctor_m4184855715(__this, ___source0, method) ((  void (*) (ReactiveProperty_1_t1576821940 *, Il2CppObject*, const MethodInfo*))ReactiveProperty_1__ctor_m2559762385_gshared)(__this, ___source0, method)
// System.Void UniRx.ReactiveProperty`1<System.String>::.ctor(UniRx.IObservable`1<T>,T)
#define ReactiveProperty_1__ctor_m1556958587(__this, ___source0, ___initialValue1, method) ((  void (*) (ReactiveProperty_1_t1576821940 *, Il2CppObject*, String_t*, const MethodInfo*))ReactiveProperty_1__ctor_m3210364201_gshared)(__this, ___source0, ___initialValue1, method)
// System.Void UniRx.ReactiveProperty`1<System.String>::.cctor()
#define ReactiveProperty_1__cctor_m4094339689(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))ReactiveProperty_1__cctor_m2738044539_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.IEqualityComparer`1<T> UniRx.ReactiveProperty`1<System.String>::get_EqualityComparer()
#define ReactiveProperty_1_get_EqualityComparer_m2417480337(__this, method) ((  Il2CppObject* (*) (ReactiveProperty_1_t1576821940 *, const MethodInfo*))ReactiveProperty_1_get_EqualityComparer_m3290095395_gshared)(__this, method)
// T UniRx.ReactiveProperty`1<System.String>::get_Value()
#define ReactiveProperty_1_get_Value_m1129601193(__this, method) ((  String_t* (*) (ReactiveProperty_1_t1576821940 *, const MethodInfo*))ReactiveProperty_1_get_Value_m2793108311_gshared)(__this, method)
// System.Void UniRx.ReactiveProperty`1<System.String>::set_Value(T)
#define ReactiveProperty_1_set_Value_m1551592296(__this, ___value0, method) ((  void (*) (ReactiveProperty_1_t1576821940 *, String_t*, const MethodInfo*))ReactiveProperty_1_set_Value_m1580705402_gshared)(__this, ___value0, method)
// System.Void UniRx.ReactiveProperty`1<System.String>::SetValue(T)
#define ReactiveProperty_1_SetValue_m2491043151(__this, ___value0, method) ((  void (*) (ReactiveProperty_1_t1576821940 *, String_t*, const MethodInfo*))ReactiveProperty_1_SetValue_m4154550269_gshared)(__this, ___value0, method)
// System.Void UniRx.ReactiveProperty`1<System.String>::SetValueAndForceNotify(T)
#define ReactiveProperty_1_SetValueAndForceNotify_m3899284690(__this, ___value0, method) ((  void (*) (ReactiveProperty_1_t1576821940 *, String_t*, const MethodInfo*))ReactiveProperty_1_SetValueAndForceNotify_m2537434880_gshared)(__this, ___value0, method)
// System.IDisposable UniRx.ReactiveProperty`1<System.String>::Subscribe(UniRx.IObserver`1<T>)
#define ReactiveProperty_1_Subscribe_m3599566489(__this, ___observer0, method) ((  Il2CppObject * (*) (ReactiveProperty_1_t1576821940 *, Il2CppObject*, const MethodInfo*))ReactiveProperty_1_Subscribe_m958004807_gshared)(__this, ___observer0, method)
// System.Void UniRx.ReactiveProperty`1<System.String>::Dispose()
#define ReactiveProperty_1_Dispose_m33875201(__this, method) ((  void (*) (ReactiveProperty_1_t1576821940 *, const MethodInfo*))ReactiveProperty_1_Dispose_m938398511_gshared)(__this, method)
// System.Void UniRx.ReactiveProperty`1<System.String>::Dispose(System.Boolean)
#define ReactiveProperty_1_Dispose_m3444720824(__this, ___disposing0, method) ((  void (*) (ReactiveProperty_1_t1576821940 *, bool, const MethodInfo*))ReactiveProperty_1_Dispose_m431016550_gshared)(__this, ___disposing0, method)
// System.String UniRx.ReactiveProperty`1<System.String>::ToString()
#define ReactiveProperty_1_ToString_m451927785(__this, method) ((  String_t* (*) (ReactiveProperty_1_t1576821940 *, const MethodInfo*))ReactiveProperty_1_ToString_m2722346619_gshared)(__this, method)
// System.Boolean UniRx.ReactiveProperty`1<System.String>::IsRequiredSubscribeOnCurrentThread()
#define ReactiveProperty_1_IsRequiredSubscribeOnCurrentThread_m3883863175(__this, method) ((  bool (*) (ReactiveProperty_1_t1576821940 *, const MethodInfo*))ReactiveProperty_1_IsRequiredSubscribeOnCurrentThread_m3797922201_gshared)(__this, method)
