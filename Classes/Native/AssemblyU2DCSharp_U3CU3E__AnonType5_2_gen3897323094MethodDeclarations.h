﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// <>__AnonType5`2<System.DateTime,System.Int64>
struct U3CU3E__AnonType5_2_t3897323094;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_DateTime339033936.h"
#include "mscorlib_System_Object837106420.h"

// System.Void <>__AnonType5`2<System.DateTime,System.Int64>::.ctor(<time>__T,<_>__T)
extern "C"  void U3CU3E__AnonType5_2__ctor_m3089998308_gshared (U3CU3E__AnonType5_2_t3897323094 * __this, DateTime_t339033936  ___time0, int64_t ____1, const MethodInfo* method);
#define U3CU3E__AnonType5_2__ctor_m3089998308(__this, ___time0, ____1, method) ((  void (*) (U3CU3E__AnonType5_2_t3897323094 *, DateTime_t339033936 , int64_t, const MethodInfo*))U3CU3E__AnonType5_2__ctor_m3089998308_gshared)(__this, ___time0, ____1, method)
// <time>__T <>__AnonType5`2<System.DateTime,System.Int64>::get_time()
extern "C"  DateTime_t339033936  U3CU3E__AnonType5_2_get_time_m37887292_gshared (U3CU3E__AnonType5_2_t3897323094 * __this, const MethodInfo* method);
#define U3CU3E__AnonType5_2_get_time_m37887292(__this, method) ((  DateTime_t339033936  (*) (U3CU3E__AnonType5_2_t3897323094 *, const MethodInfo*))U3CU3E__AnonType5_2_get_time_m37887292_gshared)(__this, method)
// <_>__T <>__AnonType5`2<System.DateTime,System.Int64>::get__()
extern "C"  int64_t U3CU3E__AnonType5_2_get___m3540666626_gshared (U3CU3E__AnonType5_2_t3897323094 * __this, const MethodInfo* method);
#define U3CU3E__AnonType5_2_get___m3540666626(__this, method) ((  int64_t (*) (U3CU3E__AnonType5_2_t3897323094 *, const MethodInfo*))U3CU3E__AnonType5_2_get___m3540666626_gshared)(__this, method)
// System.Boolean <>__AnonType5`2<System.DateTime,System.Int64>::Equals(System.Object)
extern "C"  bool U3CU3E__AnonType5_2_Equals_m2860831791_gshared (U3CU3E__AnonType5_2_t3897323094 * __this, Il2CppObject * ___obj0, const MethodInfo* method);
#define U3CU3E__AnonType5_2_Equals_m2860831791(__this, ___obj0, method) ((  bool (*) (U3CU3E__AnonType5_2_t3897323094 *, Il2CppObject *, const MethodInfo*))U3CU3E__AnonType5_2_Equals_m2860831791_gshared)(__this, ___obj0, method)
// System.Int32 <>__AnonType5`2<System.DateTime,System.Int64>::GetHashCode()
extern "C"  int32_t U3CU3E__AnonType5_2_GetHashCode_m4155620679_gshared (U3CU3E__AnonType5_2_t3897323094 * __this, const MethodInfo* method);
#define U3CU3E__AnonType5_2_GetHashCode_m4155620679(__this, method) ((  int32_t (*) (U3CU3E__AnonType5_2_t3897323094 *, const MethodInfo*))U3CU3E__AnonType5_2_GetHashCode_m4155620679_gshared)(__this, method)
// System.String <>__AnonType5`2<System.DateTime,System.Int64>::ToString()
extern "C"  String_t* U3CU3E__AnonType5_2_ToString_m1408186147_gshared (U3CU3E__AnonType5_2_t3897323094 * __this, const MethodInfo* method);
#define U3CU3E__AnonType5_2_ToString_m1408186147(__this, method) ((  String_t* (*) (U3CU3E__AnonType5_2_t3897323094 *, const MethodInfo*))U3CU3E__AnonType5_2_ToString_m1408186147_gshared)(__this, method)
