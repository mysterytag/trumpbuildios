﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.PlayFabClientAPI/<UpdateUserTitleDisplayName>c__AnonStorey8A
struct U3CUpdateUserTitleDisplayNameU3Ec__AnonStorey8A_t1145575287;
// PlayFab.CallRequestContainer
struct CallRequestContainer_t432318609;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharpU2Dfirstpass_PlayFab_CallRequestCo432318609.h"

// System.Void PlayFab.PlayFabClientAPI/<UpdateUserTitleDisplayName>c__AnonStorey8A::.ctor()
extern "C"  void U3CUpdateUserTitleDisplayNameU3Ec__AnonStorey8A__ctor_m4117065244 (U3CUpdateUserTitleDisplayNameU3Ec__AnonStorey8A_t1145575287 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/<UpdateUserTitleDisplayName>c__AnonStorey8A::<>m__77(PlayFab.CallRequestContainer)
extern "C"  void U3CUpdateUserTitleDisplayNameU3Ec__AnonStorey8A_U3CU3Em__77_m1539482330 (U3CUpdateUserTitleDisplayNameU3Ec__AnonStorey8A_t1145575287 * __this, CallRequestContainer_t432318609 * ___requestContainer0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
