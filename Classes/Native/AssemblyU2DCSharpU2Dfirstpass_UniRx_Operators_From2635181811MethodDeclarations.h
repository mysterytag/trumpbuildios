﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.FromEventObservable_`1<System.Object>
struct FromEventObservable__1_t2635181811;
// System.Action`1<System.Action`1<System.Object>>
struct Action_1_t1134011830;
// System.IDisposable
struct IDisposable_t1628921374;
// UniRx.IObserver`1<System.Object>
struct IObserver_1_t3049105323;

#include "codegen/il2cpp-codegen.h"

// System.Void UniRx.Operators.FromEventObservable_`1<System.Object>::.ctor(System.Action`1<System.Action`1<T>>,System.Action`1<System.Action`1<T>>)
extern "C"  void FromEventObservable__1__ctor_m1630118911_gshared (FromEventObservable__1_t2635181811 * __this, Action_1_t1134011830 * ___addHandler0, Action_1_t1134011830 * ___removeHandler1, const MethodInfo* method);
#define FromEventObservable__1__ctor_m1630118911(__this, ___addHandler0, ___removeHandler1, method) ((  void (*) (FromEventObservable__1_t2635181811 *, Action_1_t1134011830 *, Action_1_t1134011830 *, const MethodInfo*))FromEventObservable__1__ctor_m1630118911_gshared)(__this, ___addHandler0, ___removeHandler1, method)
// System.IDisposable UniRx.Operators.FromEventObservable_`1<System.Object>::SubscribeCore(UniRx.IObserver`1<T>,System.IDisposable)
extern "C"  Il2CppObject * FromEventObservable__1_SubscribeCore_m3156356793_gshared (FromEventObservable__1_t2635181811 * __this, Il2CppObject* ___observer0, Il2CppObject * ___cancel1, const MethodInfo* method);
#define FromEventObservable__1_SubscribeCore_m3156356793(__this, ___observer0, ___cancel1, method) ((  Il2CppObject * (*) (FromEventObservable__1_t2635181811 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))FromEventObservable__1_SubscribeCore_m3156356793_gshared)(__this, ___observer0, ___cancel1, method)
