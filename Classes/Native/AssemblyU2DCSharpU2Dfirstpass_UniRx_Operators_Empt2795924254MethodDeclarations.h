﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.EmptyObservable`1/Empty<System.Object>
struct Empty_t2795924254;
// UniRx.IObserver`1<System.Object>
struct IObserver_1_t3049105323;
// System.IDisposable
struct IDisposable_t1628921374;
// System.Object
struct Il2CppObject;
// System.Exception
struct Exception_t1967233988;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_Exception1967233988.h"

// System.Void UniRx.Operators.EmptyObservable`1/Empty<System.Object>::.ctor(UniRx.IObserver`1<T>,System.IDisposable)
extern "C"  void Empty__ctor_m4181350287_gshared (Empty_t2795924254 * __this, Il2CppObject* ___observer0, Il2CppObject * ___cancel1, const MethodInfo* method);
#define Empty__ctor_m4181350287(__this, ___observer0, ___cancel1, method) ((  void (*) (Empty_t2795924254 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))Empty__ctor_m4181350287_gshared)(__this, ___observer0, ___cancel1, method)
// System.Void UniRx.Operators.EmptyObservable`1/Empty<System.Object>::OnNext(T)
extern "C"  void Empty_OnNext_m3325140643_gshared (Empty_t2795924254 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Empty_OnNext_m3325140643(__this, ___value0, method) ((  void (*) (Empty_t2795924254 *, Il2CppObject *, const MethodInfo*))Empty_OnNext_m3325140643_gshared)(__this, ___value0, method)
// System.Void UniRx.Operators.EmptyObservable`1/Empty<System.Object>::OnError(System.Exception)
extern "C"  void Empty_OnError_m1654789042_gshared (Empty_t2795924254 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method);
#define Empty_OnError_m1654789042(__this, ___error0, method) ((  void (*) (Empty_t2795924254 *, Exception_t1967233988 *, const MethodInfo*))Empty_OnError_m1654789042_gshared)(__this, ___error0, method)
// System.Void UniRx.Operators.EmptyObservable`1/Empty<System.Object>::OnCompleted()
extern "C"  void Empty_OnCompleted_m1446724101_gshared (Empty_t2795924254 * __this, const MethodInfo* method);
#define Empty_OnCompleted_m1446724101(__this, method) ((  void (*) (Empty_t2795924254 *, const MethodInfo*))Empty_OnCompleted_m1446724101_gshared)(__this, method)
