﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.ClientModels.CancelTradeRequest
struct CancelTradeRequest_t685728853;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String968488902.h"

// System.Void PlayFab.ClientModels.CancelTradeRequest::.ctor()
extern "C"  void CancelTradeRequest__ctor_m2438846728 (CancelTradeRequest_t685728853 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PlayFab.ClientModels.CancelTradeRequest::get_TradeId()
extern "C"  String_t* CancelTradeRequest_get_TradeId_m1129643135 (CancelTradeRequest_t685728853 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.CancelTradeRequest::set_TradeId(System.String)
extern "C"  void CancelTradeRequest_set_TradeId_m2597029722 (CancelTradeRequest_t685728853 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
