﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CellReactiveApi/<FaceControl>c__AnonStorey101`1<System.Object>
struct U3CFaceControlU3Ec__AnonStorey101_1_t4266084863;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"

// System.Void CellReactiveApi/<FaceControl>c__AnonStorey101`1<System.Object>::.ctor()
extern "C"  void U3CFaceControlU3Ec__AnonStorey101_1__ctor_m3255302693_gshared (U3CFaceControlU3Ec__AnonStorey101_1_t4266084863 * __this, const MethodInfo* method);
#define U3CFaceControlU3Ec__AnonStorey101_1__ctor_m3255302693(__this, method) ((  void (*) (U3CFaceControlU3Ec__AnonStorey101_1_t4266084863 *, const MethodInfo*))U3CFaceControlU3Ec__AnonStorey101_1__ctor_m3255302693_gshared)(__this, method)
// T CellReactiveApi/<FaceControl>c__AnonStorey101`1<System.Object>::<>m__177(T)
extern "C"  Il2CppObject * U3CFaceControlU3Ec__AnonStorey101_1_U3CU3Em__177_m165187226_gshared (U3CFaceControlU3Ec__AnonStorey101_1_t4266084863 * __this, Il2CppObject * ___val0, const MethodInfo* method);
#define U3CFaceControlU3Ec__AnonStorey101_1_U3CU3Em__177_m165187226(__this, ___val0, method) ((  Il2CppObject * (*) (U3CFaceControlU3Ec__AnonStorey101_1_t4266084863 *, Il2CppObject *, const MethodInfo*))U3CFaceControlU3Ec__AnonStorey101_1_U3CU3Em__177_m165187226_gshared)(__this, ___val0, method)
