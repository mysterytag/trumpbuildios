﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// WindowManager
struct WindowManager_t3316821373;
// System.Collections.IEnumerator
struct IEnumerator_t287207039;
// WindowBase
struct WindowBase_t3855465217;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_WindowBase3855465217.h"

// System.Void WindowManager::.ctor()
extern "C"  void WindowManager__ctor_m1385599118 (WindowManager_t3316821373 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WindowManager::PostInject()
extern "C"  void WindowManager_PostInject_m3242044743 (WindowManager_t3316821373 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean WindowManager::AnyWindowShowed()
extern "C"  bool WindowManager_AnyWindowShowed_m1896528152 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WindowManager::Sort()
extern "C"  void WindowManager_Sort_m3885947156 (WindowManager_t3316821373 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator WindowManager::unlockInput(System.Single)
extern "C"  Il2CppObject * WindowManager_unlockInput_m2126734001 (WindowManager_t3316821373 * __this, float ___inSeconds0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator WindowManager::recentCooldown(System.Single)
extern "C"  Il2CppObject * WindowManager_recentCooldown_m1908303735 (WindowManager_t3316821373 * __this, float ___inSeconds0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WindowManager::CloseTopmost()
extern "C"  void WindowManager_CloseTopmost_m2019334966 (WindowManager_t3316821373 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WindowManager::<PostInject>m__15D(System.Int64)
extern "C"  void WindowManager_U3CPostInjectU3Em__15D_m2302801612 (WindowManager_t3316821373 * __this, int64_t ___l0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WindowManager::<PostInject>m__15E(System.Boolean)
extern "C"  void WindowManager_U3CPostInjectU3Em__15E_m3918476018 (WindowManager_t3316821373 * __this, bool ___b0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean WindowManager::<AnyWindowShowed>m__15F(WindowBase)
extern "C"  bool WindowManager_U3CAnyWindowShowedU3Em__15F_m3253917110 (Il2CppObject * __this /* static, unused */, WindowBase_t3855465217 * ___base0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean WindowManager::<CloseTopmost>m__160(WindowBase)
extern "C"  bool WindowManager_U3CCloseTopmostU3Em__160_m2109439393 (Il2CppObject * __this /* static, unused */, WindowBase_t3855465217 * ___window0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WindowManager::<PostInject>m__161()
extern "C"  void WindowManager_U3CPostInjectU3Em__161_m2729052678 (WindowManager_t3316821373 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
