﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.ClientModels.UnlinkAndroidDeviceIDResult
struct UnlinkAndroidDeviceIDResult_t4233284954;

#include "codegen/il2cpp-codegen.h"

// System.Void PlayFab.ClientModels.UnlinkAndroidDeviceIDResult::.ctor()
extern "C"  void UnlinkAndroidDeviceIDResult__ctor_m869293711 (UnlinkAndroidDeviceIDResult_t4233284954 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
