﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.InternalUtil.DisposedObserver`1<UniRx.CollectionRemoveEvent`1<UniRx.Tuple`2<System.Object,System.Object>>>
struct DisposedObserver_1_t3244217881;
// System.Exception
struct Exception_t1967233988;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Exception1967233988.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_CollectionRemo4069760419.h"

// System.Void UniRx.InternalUtil.DisposedObserver`1<UniRx.CollectionRemoveEvent`1<UniRx.Tuple`2<System.Object,System.Object>>>::.ctor()
extern "C"  void DisposedObserver_1__ctor_m581939651_gshared (DisposedObserver_1_t3244217881 * __this, const MethodInfo* method);
#define DisposedObserver_1__ctor_m581939651(__this, method) ((  void (*) (DisposedObserver_1_t3244217881 *, const MethodInfo*))DisposedObserver_1__ctor_m581939651_gshared)(__this, method)
// System.Void UniRx.InternalUtil.DisposedObserver`1<UniRx.CollectionRemoveEvent`1<UniRx.Tuple`2<System.Object,System.Object>>>::.cctor()
extern "C"  void DisposedObserver_1__cctor_m378163786_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define DisposedObserver_1__cctor_m378163786(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))DisposedObserver_1__cctor_m378163786_gshared)(__this /* static, unused */, method)
// System.Void UniRx.InternalUtil.DisposedObserver`1<UniRx.CollectionRemoveEvent`1<UniRx.Tuple`2<System.Object,System.Object>>>::OnCompleted()
extern "C"  void DisposedObserver_1_OnCompleted_m246499085_gshared (DisposedObserver_1_t3244217881 * __this, const MethodInfo* method);
#define DisposedObserver_1_OnCompleted_m246499085(__this, method) ((  void (*) (DisposedObserver_1_t3244217881 *, const MethodInfo*))DisposedObserver_1_OnCompleted_m246499085_gshared)(__this, method)
// System.Void UniRx.InternalUtil.DisposedObserver`1<UniRx.CollectionRemoveEvent`1<UniRx.Tuple`2<System.Object,System.Object>>>::OnError(System.Exception)
extern "C"  void DisposedObserver_1_OnError_m491834554_gshared (DisposedObserver_1_t3244217881 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method);
#define DisposedObserver_1_OnError_m491834554(__this, ___error0, method) ((  void (*) (DisposedObserver_1_t3244217881 *, Exception_t1967233988 *, const MethodInfo*))DisposedObserver_1_OnError_m491834554_gshared)(__this, ___error0, method)
// System.Void UniRx.InternalUtil.DisposedObserver`1<UniRx.CollectionRemoveEvent`1<UniRx.Tuple`2<System.Object,System.Object>>>::OnNext(T)
extern "C"  void DisposedObserver_1_OnNext_m3728959403_gshared (DisposedObserver_1_t3244217881 * __this, CollectionRemoveEvent_1_t4069760419  ___value0, const MethodInfo* method);
#define DisposedObserver_1_OnNext_m3728959403(__this, ___value0, method) ((  void (*) (DisposedObserver_1_t3244217881 *, CollectionRemoveEvent_1_t4069760419 , const MethodInfo*))DisposedObserver_1_OnNext_m3728959403_gshared)(__this, ___value0, method)
