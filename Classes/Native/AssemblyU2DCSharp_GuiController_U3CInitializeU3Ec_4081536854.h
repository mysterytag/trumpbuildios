﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.RectTransform
struct RectTransform_t3317474837;
// UniRx.ReadOnlyReactiveProperty`1<UnityEngine.Color>
struct ReadOnlyReactiveProperty_1_t82610480;
// GuiController
struct GuiController_t1495593751;
// System.Action
struct Action_t437523947;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GuiController/<Initialize>c__AnonStorey56
struct  U3CInitializeU3Ec__AnonStorey56_t4081536854  : public Il2CppObject
{
public:
	// UnityEngine.RectTransform GuiController/<Initialize>c__AnonStorey56::bucketRectTransform
	RectTransform_t3317474837 * ___bucketRectTransform_0;
	// UnityEngine.RectTransform GuiController/<Initialize>c__AnonStorey56::bottomPanelRectTransform
	RectTransform_t3317474837 * ___bottomPanelRectTransform_1;
	// UnityEngine.RectTransform GuiController/<Initialize>c__AnonStorey56::tableParentRectTransform
	RectTransform_t3317474837 * ___tableParentRectTransform_2;
	// System.Single GuiController/<Initialize>c__AnonStorey56::startY
	float ___startY_3;
	// System.Boolean GuiController/<Initialize>c__AnonStorey56::first
	bool ___first_4;
	// UniRx.ReadOnlyReactiveProperty`1<UnityEngine.Color> GuiController/<Initialize>c__AnonStorey56::cityColor
	ReadOnlyReactiveProperty_1_t82610480 * ___cityColor_5;
	// GuiController GuiController/<Initialize>c__AnonStorey56::<>f__this
	GuiController_t1495593751 * ___U3CU3Ef__this_6;

public:
	inline static int32_t get_offset_of_bucketRectTransform_0() { return static_cast<int32_t>(offsetof(U3CInitializeU3Ec__AnonStorey56_t4081536854, ___bucketRectTransform_0)); }
	inline RectTransform_t3317474837 * get_bucketRectTransform_0() const { return ___bucketRectTransform_0; }
	inline RectTransform_t3317474837 ** get_address_of_bucketRectTransform_0() { return &___bucketRectTransform_0; }
	inline void set_bucketRectTransform_0(RectTransform_t3317474837 * value)
	{
		___bucketRectTransform_0 = value;
		Il2CppCodeGenWriteBarrier(&___bucketRectTransform_0, value);
	}

	inline static int32_t get_offset_of_bottomPanelRectTransform_1() { return static_cast<int32_t>(offsetof(U3CInitializeU3Ec__AnonStorey56_t4081536854, ___bottomPanelRectTransform_1)); }
	inline RectTransform_t3317474837 * get_bottomPanelRectTransform_1() const { return ___bottomPanelRectTransform_1; }
	inline RectTransform_t3317474837 ** get_address_of_bottomPanelRectTransform_1() { return &___bottomPanelRectTransform_1; }
	inline void set_bottomPanelRectTransform_1(RectTransform_t3317474837 * value)
	{
		___bottomPanelRectTransform_1 = value;
		Il2CppCodeGenWriteBarrier(&___bottomPanelRectTransform_1, value);
	}

	inline static int32_t get_offset_of_tableParentRectTransform_2() { return static_cast<int32_t>(offsetof(U3CInitializeU3Ec__AnonStorey56_t4081536854, ___tableParentRectTransform_2)); }
	inline RectTransform_t3317474837 * get_tableParentRectTransform_2() const { return ___tableParentRectTransform_2; }
	inline RectTransform_t3317474837 ** get_address_of_tableParentRectTransform_2() { return &___tableParentRectTransform_2; }
	inline void set_tableParentRectTransform_2(RectTransform_t3317474837 * value)
	{
		___tableParentRectTransform_2 = value;
		Il2CppCodeGenWriteBarrier(&___tableParentRectTransform_2, value);
	}

	inline static int32_t get_offset_of_startY_3() { return static_cast<int32_t>(offsetof(U3CInitializeU3Ec__AnonStorey56_t4081536854, ___startY_3)); }
	inline float get_startY_3() const { return ___startY_3; }
	inline float* get_address_of_startY_3() { return &___startY_3; }
	inline void set_startY_3(float value)
	{
		___startY_3 = value;
	}

	inline static int32_t get_offset_of_first_4() { return static_cast<int32_t>(offsetof(U3CInitializeU3Ec__AnonStorey56_t4081536854, ___first_4)); }
	inline bool get_first_4() const { return ___first_4; }
	inline bool* get_address_of_first_4() { return &___first_4; }
	inline void set_first_4(bool value)
	{
		___first_4 = value;
	}

	inline static int32_t get_offset_of_cityColor_5() { return static_cast<int32_t>(offsetof(U3CInitializeU3Ec__AnonStorey56_t4081536854, ___cityColor_5)); }
	inline ReadOnlyReactiveProperty_1_t82610480 * get_cityColor_5() const { return ___cityColor_5; }
	inline ReadOnlyReactiveProperty_1_t82610480 ** get_address_of_cityColor_5() { return &___cityColor_5; }
	inline void set_cityColor_5(ReadOnlyReactiveProperty_1_t82610480 * value)
	{
		___cityColor_5 = value;
		Il2CppCodeGenWriteBarrier(&___cityColor_5, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__this_6() { return static_cast<int32_t>(offsetof(U3CInitializeU3Ec__AnonStorey56_t4081536854, ___U3CU3Ef__this_6)); }
	inline GuiController_t1495593751 * get_U3CU3Ef__this_6() const { return ___U3CU3Ef__this_6; }
	inline GuiController_t1495593751 ** get_address_of_U3CU3Ef__this_6() { return &___U3CU3Ef__this_6; }
	inline void set_U3CU3Ef__this_6(GuiController_t1495593751 * value)
	{
		___U3CU3Ef__this_6 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__this_6, value);
	}
};

struct U3CInitializeU3Ec__AnonStorey56_t4081536854_StaticFields
{
public:
	// System.Action GuiController/<Initialize>c__AnonStorey56::<>f__am$cache7
	Action_t437523947 * ___U3CU3Ef__amU24cache7_7;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache7_7() { return static_cast<int32_t>(offsetof(U3CInitializeU3Ec__AnonStorey56_t4081536854_StaticFields, ___U3CU3Ef__amU24cache7_7)); }
	inline Action_t437523947 * get_U3CU3Ef__amU24cache7_7() const { return ___U3CU3Ef__amU24cache7_7; }
	inline Action_t437523947 ** get_address_of_U3CU3Ef__amU24cache7_7() { return &___U3CU3Ef__amU24cache7_7; }
	inline void set_U3CU3Ef__amU24cache7_7(Action_t437523947 * value)
	{
		___U3CU3Ef__amU24cache7_7 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache7_7, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
