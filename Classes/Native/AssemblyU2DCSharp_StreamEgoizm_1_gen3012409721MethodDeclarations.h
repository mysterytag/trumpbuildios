﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// StreamEgoizm`1<System.Object>
struct StreamEgoizm_1_t3012409721;
// System.IDisposable
struct IDisposable_t1628921374;
// System.Action
struct Action_t437523947;
// System.Action`1<System.Object>
struct Action_1_t985559125;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "System_Core_System_Action437523947.h"
#include "AssemblyU2DCSharp_Priority3194150340.h"
#include "mscorlib_System_Object837106420.h"

// System.Void StreamEgoizm`1<System.Object>::.ctor()
extern "C"  void StreamEgoizm_1__ctor_m3384859535_gshared (StreamEgoizm_1_t3012409721 * __this, const MethodInfo* method);
#define StreamEgoizm_1__ctor_m3384859535(__this, method) ((  void (*) (StreamEgoizm_1_t3012409721 *, const MethodInfo*))StreamEgoizm_1__ctor_m3384859535_gshared)(__this, method)
// System.IDisposable StreamEgoizm`1<System.Object>::Listen(System.Action,Priority)
extern "C"  Il2CppObject * StreamEgoizm_1_Listen_m3593449934_gshared (StreamEgoizm_1_t3012409721 * __this, Action_t437523947 * ___action0, int32_t ___p1, const MethodInfo* method);
#define StreamEgoizm_1_Listen_m3593449934(__this, ___action0, ___p1, method) ((  Il2CppObject * (*) (StreamEgoizm_1_t3012409721 *, Action_t437523947 *, int32_t, const MethodInfo*))StreamEgoizm_1_Listen_m3593449934_gshared)(__this, ___action0, ___p1, method)
// System.IDisposable StreamEgoizm`1<System.Object>::Listen(System.Action`1<T>,Priority)
extern "C"  Il2CppObject * StreamEgoizm_1_Listen_m2805505977_gshared (StreamEgoizm_1_t3012409721 * __this, Action_1_t985559125 * ___action0, int32_t ___priority1, const MethodInfo* method);
#define StreamEgoizm_1_Listen_m2805505977(__this, ___action0, ___priority1, method) ((  Il2CppObject * (*) (StreamEgoizm_1_t3012409721 *, Action_1_t985559125 *, int32_t, const MethodInfo*))StreamEgoizm_1_Listen_m2805505977_gshared)(__this, ___action0, ___priority1, method)
// System.IDisposable StreamEgoizm`1<System.Object>::DoThis(System.Action`1<T>)
extern "C"  Il2CppObject * StreamEgoizm_1_DoThis_m2826120107_gshared (StreamEgoizm_1_t3012409721 * __this, Action_1_t985559125 * ___action0, const MethodInfo* method);
#define StreamEgoizm_1_DoThis_m2826120107(__this, ___action0, method) ((  Il2CppObject * (*) (StreamEgoizm_1_t3012409721 *, Action_1_t985559125 *, const MethodInfo*))StreamEgoizm_1_DoThis_m2826120107_gshared)(__this, ___action0, method)
// System.Void StreamEgoizm`1<System.Object>::Send(T)
extern "C"  void StreamEgoizm_1_Send_m1914077089_gshared (StreamEgoizm_1_t3012409721 * __this, Il2CppObject * ___obj0, const MethodInfo* method);
#define StreamEgoizm_1_Send_m1914077089(__this, ___obj0, method) ((  void (*) (StreamEgoizm_1_t3012409721 *, Il2CppObject *, const MethodInfo*))StreamEgoizm_1_Send_m1914077089_gshared)(__this, ___obj0, method)
