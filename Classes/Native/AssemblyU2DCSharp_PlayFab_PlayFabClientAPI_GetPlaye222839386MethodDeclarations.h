﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.PlayFabClientAPI/GetPlayerStatisticsRequestCallback
struct GetPlayerStatisticsRequestCallback_t222839386;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;
// PlayFab.ClientModels.GetPlayerStatisticsRequest
struct GetPlayerStatisticsRequest_t3788214021;
// System.IAsyncResult
struct IAsyncResult_t537683269;
// System.AsyncCallback
struct AsyncCallback_t1363551830;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_IntPtr676692020.h"
#include "mscorlib_System_String968488902.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_GetPlayerSt3788214021.h"
#include "mscorlib_System_AsyncCallback1363551830.h"

// System.Void PlayFab.PlayFabClientAPI/GetPlayerStatisticsRequestCallback::.ctor(System.Object,System.IntPtr)
extern "C"  void GetPlayerStatisticsRequestCallback__ctor_m3399225929 (GetPlayerStatisticsRequestCallback_t222839386 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/GetPlayerStatisticsRequestCallback::Invoke(System.String,System.Int32,PlayFab.ClientModels.GetPlayerStatisticsRequest,System.Object)
extern "C"  void GetPlayerStatisticsRequestCallback_Invoke_m1635957343 (GetPlayerStatisticsRequestCallback_t222839386 * __this, String_t* ___urlPath0, int32_t ___callId1, GetPlayerStatisticsRequest_t3788214021 * ___request2, Il2CppObject * ___customData3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
extern "C" void pinvoke_delegate_wrapper_GetPlayerStatisticsRequestCallback_t222839386(Il2CppObject* delegate, String_t* ___urlPath0, int32_t ___callId1, GetPlayerStatisticsRequest_t3788214021 * ___request2, Il2CppObject * ___customData3);
// System.IAsyncResult PlayFab.PlayFabClientAPI/GetPlayerStatisticsRequestCallback::BeginInvoke(System.String,System.Int32,PlayFab.ClientModels.GetPlayerStatisticsRequest,System.Object,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * GetPlayerStatisticsRequestCallback_BeginInvoke_m3999745178 (GetPlayerStatisticsRequestCallback_t222839386 * __this, String_t* ___urlPath0, int32_t ___callId1, GetPlayerStatisticsRequest_t3788214021 * ___request2, Il2CppObject * ___customData3, AsyncCallback_t1363551830 * ___callback4, Il2CppObject * ___object5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/GetPlayerStatisticsRequestCallback::EndInvoke(System.IAsyncResult)
extern "C"  void GetPlayerStatisticsRequestCallback_EndInvoke_m173936857 (GetPlayerStatisticsRequestCallback_t222839386 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
