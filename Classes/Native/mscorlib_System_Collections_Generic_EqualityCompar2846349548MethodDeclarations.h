﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.EqualityComparer`1/DefaultComparer<TableButtons/StatkaPoTableViev>
struct DefaultComparer_t2846349548;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_TableButtons_StatkaPoTableViev3508395817.h"

// System.Void System.Collections.Generic.EqualityComparer`1/DefaultComparer<TableButtons/StatkaPoTableViev>::.ctor()
extern "C"  void DefaultComparer__ctor_m589656953_gshared (DefaultComparer_t2846349548 * __this, const MethodInfo* method);
#define DefaultComparer__ctor_m589656953(__this, method) ((  void (*) (DefaultComparer_t2846349548 *, const MethodInfo*))DefaultComparer__ctor_m589656953_gshared)(__this, method)
// System.Int32 System.Collections.Generic.EqualityComparer`1/DefaultComparer<TableButtons/StatkaPoTableViev>::GetHashCode(T)
extern "C"  int32_t DefaultComparer_GetHashCode_m3389086298_gshared (DefaultComparer_t2846349548 * __this, int32_t ___obj0, const MethodInfo* method);
#define DefaultComparer_GetHashCode_m3389086298(__this, ___obj0, method) ((  int32_t (*) (DefaultComparer_t2846349548 *, int32_t, const MethodInfo*))DefaultComparer_GetHashCode_m3389086298_gshared)(__this, ___obj0, method)
// System.Boolean System.Collections.Generic.EqualityComparer`1/DefaultComparer<TableButtons/StatkaPoTableViev>::Equals(T,T)
extern "C"  bool DefaultComparer_Equals_m573280142_gshared (DefaultComparer_t2846349548 * __this, int32_t ___x0, int32_t ___y1, const MethodInfo* method);
#define DefaultComparer_Equals_m573280142(__this, ___x0, ___y1, method) ((  bool (*) (DefaultComparer_t2846349548 *, int32_t, int32_t, const MethodInfo*))DefaultComparer_Equals_m573280142_gshared)(__this, ___x0, ___y1, method)
