﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Transaction/<Calculate>c__AnonStorey149`1/<Calculate>c__AnonStorey14A`1<System.Object>
struct U3CCalculateU3Ec__AnonStorey14A_1_t2571930805;
// System.IDisposable
struct IDisposable_t1628921374;
// ICell
struct ICell_t69513547;

#include "codegen/il2cpp-codegen.h"

// System.Void Transaction/<Calculate>c__AnonStorey149`1/<Calculate>c__AnonStorey14A`1<System.Object>::.ctor()
extern "C"  void U3CCalculateU3Ec__AnonStorey14A_1__ctor_m1769840975_gshared (U3CCalculateU3Ec__AnonStorey14A_1_t2571930805 * __this, const MethodInfo* method);
#define U3CCalculateU3Ec__AnonStorey14A_1__ctor_m1769840975(__this, method) ((  void (*) (U3CCalculateU3Ec__AnonStorey14A_1_t2571930805 *, const MethodInfo*))U3CCalculateU3Ec__AnonStorey14A_1__ctor_m1769840975_gshared)(__this, method)
// System.IDisposable Transaction/<Calculate>c__AnonStorey149`1/<Calculate>c__AnonStorey14A`1<System.Object>::<>m__1F9(ICell)
extern "C"  Il2CppObject * U3CCalculateU3Ec__AnonStorey14A_1_U3CU3Em__1F9_m3542568358_gshared (U3CCalculateU3Ec__AnonStorey14A_1_t2571930805 * __this, Il2CppObject * ___cell0, const MethodInfo* method);
#define U3CCalculateU3Ec__AnonStorey14A_1_U3CU3Em__1F9_m3542568358(__this, ___cell0, method) ((  Il2CppObject * (*) (U3CCalculateU3Ec__AnonStorey14A_1_t2571930805 *, Il2CppObject *, const MethodInfo*))U3CCalculateU3Ec__AnonStorey14A_1_U3CU3Em__1F9_m3542568358_gshared)(__this, ___cell0, method)
// System.Void Transaction/<Calculate>c__AnonStorey149`1/<Calculate>c__AnonStorey14A`1<System.Object>::<>m__1FA()
extern "C"  void U3CCalculateU3Ec__AnonStorey14A_1_U3CU3Em__1FA_m3144402486_gshared (U3CCalculateU3Ec__AnonStorey14A_1_t2571930805 * __this, const MethodInfo* method);
#define U3CCalculateU3Ec__AnonStorey14A_1_U3CU3Em__1FA_m3144402486(__this, method) ((  void (*) (U3CCalculateU3Ec__AnonStorey14A_1_t2571930805 *, const MethodInfo*))U3CCalculateU3Ec__AnonStorey14A_1_U3CU3Em__1FA_m3144402486_gshared)(__this, method)
