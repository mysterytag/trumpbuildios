﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// GooglePlayGames.PlayGamesPlatform modreq(System.Runtime.CompilerServices.IsVolatile)
struct PlayGamesPlatform_t930834684;
// GooglePlayGames.PlayGamesLocalUser
struct PlayGamesLocalUser_t2171236901;
// GooglePlayGames.BasicApi.IPlayGamesClient
struct IPlayGamesClient_t3354664049;
// GooglePlayGames.BasicApi.Nearby.INearbyConnectionClient modreq(System.Runtime.CompilerServices.IsVolatile)
struct INearbyConnectionClient_t2896752718;
// System.String
struct String_t;
// System.Collections.Generic.Dictionary`2<System.String,System.String>
struct Dictionary_2_t2606186806;

#include "mscorlib_System_Object837106420.h"
#include "AssemblyU2DCSharp_GooglePlayGames_BasicApi_PlayGame962255808.h"
#include "mscorlib_System_Boolean211005341.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GooglePlayGames.PlayGamesPlatform
struct  PlayGamesPlatform_t930834684  : public Il2CppObject
{
public:
	// GooglePlayGames.BasicApi.PlayGamesClientConfiguration GooglePlayGames.PlayGamesPlatform::mConfiguration
	PlayGamesClientConfiguration_t962255808  ___mConfiguration_1;
	// GooglePlayGames.PlayGamesLocalUser GooglePlayGames.PlayGamesPlatform::mLocalUser
	PlayGamesLocalUser_t2171236901 * ___mLocalUser_2;
	// GooglePlayGames.BasicApi.IPlayGamesClient GooglePlayGames.PlayGamesPlatform::mClient
	Il2CppObject * ___mClient_3;
	// System.String GooglePlayGames.PlayGamesPlatform::mDefaultLbUi
	String_t* ___mDefaultLbUi_6;
	// System.Collections.Generic.Dictionary`2<System.String,System.String> GooglePlayGames.PlayGamesPlatform::mIdMap
	Dictionary_2_t2606186806 * ___mIdMap_7;

public:
	inline static int32_t get_offset_of_mConfiguration_1() { return static_cast<int32_t>(offsetof(PlayGamesPlatform_t930834684, ___mConfiguration_1)); }
	inline PlayGamesClientConfiguration_t962255808  get_mConfiguration_1() const { return ___mConfiguration_1; }
	inline PlayGamesClientConfiguration_t962255808 * get_address_of_mConfiguration_1() { return &___mConfiguration_1; }
	inline void set_mConfiguration_1(PlayGamesClientConfiguration_t962255808  value)
	{
		___mConfiguration_1 = value;
	}

	inline static int32_t get_offset_of_mLocalUser_2() { return static_cast<int32_t>(offsetof(PlayGamesPlatform_t930834684, ___mLocalUser_2)); }
	inline PlayGamesLocalUser_t2171236901 * get_mLocalUser_2() const { return ___mLocalUser_2; }
	inline PlayGamesLocalUser_t2171236901 ** get_address_of_mLocalUser_2() { return &___mLocalUser_2; }
	inline void set_mLocalUser_2(PlayGamesLocalUser_t2171236901 * value)
	{
		___mLocalUser_2 = value;
		Il2CppCodeGenWriteBarrier(&___mLocalUser_2, value);
	}

	inline static int32_t get_offset_of_mClient_3() { return static_cast<int32_t>(offsetof(PlayGamesPlatform_t930834684, ___mClient_3)); }
	inline Il2CppObject * get_mClient_3() const { return ___mClient_3; }
	inline Il2CppObject ** get_address_of_mClient_3() { return &___mClient_3; }
	inline void set_mClient_3(Il2CppObject * value)
	{
		___mClient_3 = value;
		Il2CppCodeGenWriteBarrier(&___mClient_3, value);
	}

	inline static int32_t get_offset_of_mDefaultLbUi_6() { return static_cast<int32_t>(offsetof(PlayGamesPlatform_t930834684, ___mDefaultLbUi_6)); }
	inline String_t* get_mDefaultLbUi_6() const { return ___mDefaultLbUi_6; }
	inline String_t** get_address_of_mDefaultLbUi_6() { return &___mDefaultLbUi_6; }
	inline void set_mDefaultLbUi_6(String_t* value)
	{
		___mDefaultLbUi_6 = value;
		Il2CppCodeGenWriteBarrier(&___mDefaultLbUi_6, value);
	}

	inline static int32_t get_offset_of_mIdMap_7() { return static_cast<int32_t>(offsetof(PlayGamesPlatform_t930834684, ___mIdMap_7)); }
	inline Dictionary_2_t2606186806 * get_mIdMap_7() const { return ___mIdMap_7; }
	inline Dictionary_2_t2606186806 ** get_address_of_mIdMap_7() { return &___mIdMap_7; }
	inline void set_mIdMap_7(Dictionary_2_t2606186806 * value)
	{
		___mIdMap_7 = value;
		Il2CppCodeGenWriteBarrier(&___mIdMap_7, value);
	}
};

struct PlayGamesPlatform_t930834684_StaticFields
{
public:
	// GooglePlayGames.PlayGamesPlatform modreq(System.Runtime.CompilerServices.IsVolatile) GooglePlayGames.PlayGamesPlatform::sInstance
	PlayGamesPlatform_t930834684 * ___sInstance_0;
	// System.Boolean modreq(System.Runtime.CompilerServices.IsVolatile) GooglePlayGames.PlayGamesPlatform::sNearbyInitializePending
	bool ___sNearbyInitializePending_4;
	// GooglePlayGames.BasicApi.Nearby.INearbyConnectionClient modreq(System.Runtime.CompilerServices.IsVolatile) GooglePlayGames.PlayGamesPlatform::sNearbyConnectionClient
	Il2CppObject * ___sNearbyConnectionClient_5;

public:
	inline static int32_t get_offset_of_sInstance_0() { return static_cast<int32_t>(offsetof(PlayGamesPlatform_t930834684_StaticFields, ___sInstance_0)); }
	inline PlayGamesPlatform_t930834684 * get_sInstance_0() const { return ___sInstance_0; }
	inline PlayGamesPlatform_t930834684 ** get_address_of_sInstance_0() { return &___sInstance_0; }
	inline void set_sInstance_0(PlayGamesPlatform_t930834684 * value)
	{
		___sInstance_0 = value;
		Il2CppCodeGenWriteBarrier(&___sInstance_0, value);
	}

	inline static int32_t get_offset_of_sNearbyInitializePending_4() { return static_cast<int32_t>(offsetof(PlayGamesPlatform_t930834684_StaticFields, ___sNearbyInitializePending_4)); }
	inline bool get_sNearbyInitializePending_4() const { return ___sNearbyInitializePending_4; }
	inline bool* get_address_of_sNearbyInitializePending_4() { return &___sNearbyInitializePending_4; }
	inline void set_sNearbyInitializePending_4(bool value)
	{
		___sNearbyInitializePending_4 = value;
	}

	inline static int32_t get_offset_of_sNearbyConnectionClient_5() { return static_cast<int32_t>(offsetof(PlayGamesPlatform_t930834684_StaticFields, ___sNearbyConnectionClient_5)); }
	inline Il2CppObject * get_sNearbyConnectionClient_5() const { return ___sNearbyConnectionClient_5; }
	inline Il2CppObject ** get_address_of_sNearbyConnectionClient_5() { return &___sNearbyConnectionClient_5; }
	inline void set_sNearbyConnectionClient_5(Il2CppObject * value)
	{
		___sNearbyConnectionClient_5 = value;
		Il2CppCodeGenWriteBarrier(&___sNearbyConnectionClient_5, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
