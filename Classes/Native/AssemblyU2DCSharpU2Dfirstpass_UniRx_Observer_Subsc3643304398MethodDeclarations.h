﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Observer/Subscribe`1<System.Double>
struct Subscribe_1_t3643304398;
// System.Action`1<System.Double>
struct Action_1_t682969319;
// System.Action`1<System.Exception>
struct Action_1_t2115686693;
// System.Action
struct Action_t437523947;
// System.Exception
struct Exception_t1967233988;

#include "codegen/il2cpp-codegen.h"
#include "System_Core_System_Action437523947.h"
#include "mscorlib_System_Exception1967233988.h"

// System.Void UniRx.Observer/Subscribe`1<System.Double>::.ctor(System.Action`1<T>,System.Action`1<System.Exception>,System.Action)
extern "C"  void Subscribe_1__ctor_m3857391634_gshared (Subscribe_1_t3643304398 * __this, Action_1_t682969319 * ___onNext0, Action_1_t2115686693 * ___onError1, Action_t437523947 * ___onCompleted2, const MethodInfo* method);
#define Subscribe_1__ctor_m3857391634(__this, ___onNext0, ___onError1, ___onCompleted2, method) ((  void (*) (Subscribe_1_t3643304398 *, Action_1_t682969319 *, Action_1_t2115686693 *, Action_t437523947 *, const MethodInfo*))Subscribe_1__ctor_m3857391634_gshared)(__this, ___onNext0, ___onError1, ___onCompleted2, method)
// System.Void UniRx.Observer/Subscribe`1<System.Double>::OnNext(T)
extern "C"  void Subscribe_1_OnNext_m954062575_gshared (Subscribe_1_t3643304398 * __this, double ___value0, const MethodInfo* method);
#define Subscribe_1_OnNext_m954062575(__this, ___value0, method) ((  void (*) (Subscribe_1_t3643304398 *, double, const MethodInfo*))Subscribe_1_OnNext_m954062575_gshared)(__this, ___value0, method)
// System.Void UniRx.Observer/Subscribe`1<System.Double>::OnError(System.Exception)
extern "C"  void Subscribe_1_OnError_m407647230_gshared (Subscribe_1_t3643304398 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method);
#define Subscribe_1_OnError_m407647230(__this, ___error0, method) ((  void (*) (Subscribe_1_t3643304398 *, Exception_t1967233988 *, const MethodInfo*))Subscribe_1_OnError_m407647230_gshared)(__this, ___error0, method)
// System.Void UniRx.Observer/Subscribe`1<System.Double>::OnCompleted()
extern "C"  void Subscribe_1_OnCompleted_m2889512017_gshared (Subscribe_1_t3643304398 * __this, const MethodInfo* method);
#define Subscribe_1_OnCompleted_m2889512017(__this, method) ((  void (*) (Subscribe_1_t3643304398 *, const MethodInfo*))Subscribe_1_OnCompleted_m2889512017_gshared)(__this, method)
