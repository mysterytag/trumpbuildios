﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UniRx.IObserver`1<System.Object>
struct IObserver_1_t3049105323;
// UniRx.Notification`1/<ToObservable>c__AnonStorey55<System.Object>
struct U3CToObservableU3Ec__AnonStorey55_t1984678244;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Notification`1/<ToObservable>c__AnonStorey55/<ToObservable>c__AnonStorey56<System.Object>
struct  U3CToObservableU3Ec__AnonStorey56_t3651252413  : public Il2CppObject
{
public:
	// UniRx.IObserver`1<T> UniRx.Notification`1/<ToObservable>c__AnonStorey55/<ToObservable>c__AnonStorey56::observer
	Il2CppObject* ___observer_0;
	// UniRx.Notification`1/<ToObservable>c__AnonStorey55<T> UniRx.Notification`1/<ToObservable>c__AnonStorey55/<ToObservable>c__AnonStorey56::<>f__ref$85
	U3CToObservableU3Ec__AnonStorey55_t1984678244 * ___U3CU3Ef__refU2485_1;

public:
	inline static int32_t get_offset_of_observer_0() { return static_cast<int32_t>(offsetof(U3CToObservableU3Ec__AnonStorey56_t3651252413, ___observer_0)); }
	inline Il2CppObject* get_observer_0() const { return ___observer_0; }
	inline Il2CppObject** get_address_of_observer_0() { return &___observer_0; }
	inline void set_observer_0(Il2CppObject* value)
	{
		___observer_0 = value;
		Il2CppCodeGenWriteBarrier(&___observer_0, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__refU2485_1() { return static_cast<int32_t>(offsetof(U3CToObservableU3Ec__AnonStorey56_t3651252413, ___U3CU3Ef__refU2485_1)); }
	inline U3CToObservableU3Ec__AnonStorey55_t1984678244 * get_U3CU3Ef__refU2485_1() const { return ___U3CU3Ef__refU2485_1; }
	inline U3CToObservableU3Ec__AnonStorey55_t1984678244 ** get_address_of_U3CU3Ef__refU2485_1() { return &___U3CU3Ef__refU2485_1; }
	inline void set_U3CU3Ef__refU2485_1(U3CToObservableU3Ec__AnonStorey55_t1984678244 * value)
	{
		___U3CU3Ef__refU2485_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__refU2485_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
