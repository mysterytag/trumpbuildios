﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.PlayFabClientAPI/GetPhotonAuthenticationTokenRequestCallback
struct GetPhotonAuthenticationTokenRequestCallback_t1169441349;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;
// PlayFab.ClientModels.GetPhotonAuthenticationTokenRequest
struct GetPhotonAuthenticationTokenRequest_t2686429680;
// System.IAsyncResult
struct IAsyncResult_t537683269;
// System.AsyncCallback
struct AsyncCallback_t1363551830;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_IntPtr676692020.h"
#include "mscorlib_System_String968488902.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_GetPhotonAu2686429680.h"
#include "mscorlib_System_AsyncCallback1363551830.h"

// System.Void PlayFab.PlayFabClientAPI/GetPhotonAuthenticationTokenRequestCallback::.ctor(System.Object,System.IntPtr)
extern "C"  void GetPhotonAuthenticationTokenRequestCallback__ctor_m2601457748 (GetPhotonAuthenticationTokenRequestCallback_t1169441349 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/GetPhotonAuthenticationTokenRequestCallback::Invoke(System.String,System.Int32,PlayFab.ClientModels.GetPhotonAuthenticationTokenRequest,System.Object)
extern "C"  void GetPhotonAuthenticationTokenRequestCallback_Invoke_m2782241487 (GetPhotonAuthenticationTokenRequestCallback_t1169441349 * __this, String_t* ___urlPath0, int32_t ___callId1, GetPhotonAuthenticationTokenRequest_t2686429680 * ___request2, Il2CppObject * ___customData3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
extern "C" void pinvoke_delegate_wrapper_GetPhotonAuthenticationTokenRequestCallback_t1169441349(Il2CppObject* delegate, String_t* ___urlPath0, int32_t ___callId1, GetPhotonAuthenticationTokenRequest_t2686429680 * ___request2, Il2CppObject * ___customData3);
// System.IAsyncResult PlayFab.PlayFabClientAPI/GetPhotonAuthenticationTokenRequestCallback::BeginInvoke(System.String,System.Int32,PlayFab.ClientModels.GetPhotonAuthenticationTokenRequest,System.Object,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * GetPhotonAuthenticationTokenRequestCallback_BeginInvoke_m763820670 (GetPhotonAuthenticationTokenRequestCallback_t1169441349 * __this, String_t* ___urlPath0, int32_t ___callId1, GetPhotonAuthenticationTokenRequest_t2686429680 * ___request2, Il2CppObject * ___customData3, AsyncCallback_t1363551830 * ___callback4, Il2CppObject * ___object5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/GetPhotonAuthenticationTokenRequestCallback::EndInvoke(System.IAsyncResult)
extern "C"  void GetPhotonAuthenticationTokenRequestCallback_EndInvoke_m1371788388 (GetPhotonAuthenticationTokenRequestCallback_t1169441349 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
