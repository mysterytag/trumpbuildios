﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Facebook.Unity.Mobile.Android.AndroidFacebook/JavaMethodCall`1<System.Object>
struct JavaMethodCall_1_t3793613819;
// Facebook.Unity.Mobile.Android.AndroidFacebook
struct AndroidFacebook_t1604313921;
// System.String
struct String_t;
// Facebook.Unity.MethodArguments
struct MethodArguments_t3878806324;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Facebook_Unity_Mobile_Android_An1604313921.h"
#include "mscorlib_System_String968488902.h"
#include "AssemblyU2DCSharp_Facebook_Unity_MethodArguments3878806324.h"

// System.Void Facebook.Unity.Mobile.Android.AndroidFacebook/JavaMethodCall`1<System.Object>::.ctor(Facebook.Unity.Mobile.Android.AndroidFacebook,System.String)
extern "C"  void JavaMethodCall_1__ctor_m3083954302_gshared (JavaMethodCall_1_t3793613819 * __this, AndroidFacebook_t1604313921 * ___androidImpl0, String_t* ___methodName1, const MethodInfo* method);
#define JavaMethodCall_1__ctor_m3083954302(__this, ___androidImpl0, ___methodName1, method) ((  void (*) (JavaMethodCall_1_t3793613819 *, AndroidFacebook_t1604313921 *, String_t*, const MethodInfo*))JavaMethodCall_1__ctor_m3083954302_gshared)(__this, ___androidImpl0, ___methodName1, method)
// System.Void Facebook.Unity.Mobile.Android.AndroidFacebook/JavaMethodCall`1<System.Object>::Call(Facebook.Unity.MethodArguments)
extern "C"  void JavaMethodCall_1_Call_m1503093513_gshared (JavaMethodCall_1_t3793613819 * __this, MethodArguments_t3878806324 * ___args0, const MethodInfo* method);
#define JavaMethodCall_1_Call_m1503093513(__this, ___args0, method) ((  void (*) (JavaMethodCall_1_t3793613819 *, MethodArguments_t3878806324 *, const MethodInfo*))JavaMethodCall_1_Call_m1503093513_gshared)(__this, ___args0, method)
