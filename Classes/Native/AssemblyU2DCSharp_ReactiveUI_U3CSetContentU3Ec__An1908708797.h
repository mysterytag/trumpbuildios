﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.UI.Text
struct Text_t3286458198;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ReactiveUI/<SetContent>c__AnonStorey120`1<System.Object>
struct  U3CSetContentU3Ec__AnonStorey120_1_t1908708797  : public Il2CppObject
{
public:
	// UnityEngine.UI.Text ReactiveUI/<SetContent>c__AnonStorey120`1::text
	Text_t3286458198 * ___text_0;

public:
	inline static int32_t get_offset_of_text_0() { return static_cast<int32_t>(offsetof(U3CSetContentU3Ec__AnonStorey120_1_t1908708797, ___text_0)); }
	inline Text_t3286458198 * get_text_0() const { return ___text_0; }
	inline Text_t3286458198 ** get_address_of_text_0() { return &___text_0; }
	inline void set_text_0(Text_t3286458198 * value)
	{
		___text_0 = value;
		Il2CppCodeGenWriteBarrier(&___text_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
