﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UniRx.Operators.BufferObservable`1/BufferTC<System.Object>
struct BufferTC_t3470796400;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Operators.BufferObservable`1/BufferTC/<CreateTimer>c__AnonStorey5D<System.Object>
struct  U3CCreateTimerU3Ec__AnonStorey5D_t162307190  : public Il2CppObject
{
public:
	// System.Int64 UniRx.Operators.BufferObservable`1/BufferTC/<CreateTimer>c__AnonStorey5D::currentTimerId
	int64_t ___currentTimerId_0;
	// UniRx.Operators.BufferObservable`1/BufferTC<T> UniRx.Operators.BufferObservable`1/BufferTC/<CreateTimer>c__AnonStorey5D::<>f__this
	BufferTC_t3470796400 * ___U3CU3Ef__this_1;

public:
	inline static int32_t get_offset_of_currentTimerId_0() { return static_cast<int32_t>(offsetof(U3CCreateTimerU3Ec__AnonStorey5D_t162307190, ___currentTimerId_0)); }
	inline int64_t get_currentTimerId_0() const { return ___currentTimerId_0; }
	inline int64_t* get_address_of_currentTimerId_0() { return &___currentTimerId_0; }
	inline void set_currentTimerId_0(int64_t value)
	{
		___currentTimerId_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3Ef__this_1() { return static_cast<int32_t>(offsetof(U3CCreateTimerU3Ec__AnonStorey5D_t162307190, ___U3CU3Ef__this_1)); }
	inline BufferTC_t3470796400 * get_U3CU3Ef__this_1() const { return ___U3CU3Ef__this_1; }
	inline BufferTC_t3470796400 ** get_address_of_U3CU3Ef__this_1() { return &___U3CU3Ef__this_1; }
	inline void set_U3CU3Ef__this_1(BufferTC_t3470796400 * value)
	{
		___U3CU3Ef__this_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__this_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
