﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.Internal.Log
struct Log_t1190808428;
// System.String
struct String_t;
// System.Object[]
struct ObjectU5BU5D_t11523773;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String968488902.h"

// System.Void PlayFab.Internal.Log::.ctor()
extern "C"  void Log__ctor_m1836995277 (Log_t1190808428 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.Internal.Log::Debug(System.String,System.Object[])
extern "C"  void Log_Debug_m1011319984 (Il2CppObject * __this /* static, unused */, String_t* ___text0, ObjectU5BU5D_t11523773* ___args1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.Internal.Log::Info(System.String,System.Object[])
extern "C"  void Log_Info_m2677329289 (Il2CppObject * __this /* static, unused */, String_t* ___text0, ObjectU5BU5D_t11523773* ___args1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.Internal.Log::Warning(System.String,System.Object[])
extern "C"  void Log_Warning_m1139508423 (Il2CppObject * __this /* static, unused */, String_t* ___text0, ObjectU5BU5D_t11523773* ___args1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.Internal.Log::Error(System.String,System.Object[])
extern "C"  void Log_Error_m2726892123 (Il2CppObject * __this /* static, unused */, String_t* ___text0, ObjectU5BU5D_t11523773* ___args1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
