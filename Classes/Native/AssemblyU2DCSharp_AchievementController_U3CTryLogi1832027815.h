﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Action
struct Action_t437523947;
// AchievementController
struct AchievementController_t3677499403;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AchievementController/<TryLoginOnce>c__AnonStoreyD7
struct  U3CTryLoginOnceU3Ec__AnonStoreyD7_t1832027815  : public Il2CppObject
{
public:
	// System.Action AchievementController/<TryLoginOnce>c__AnonStoreyD7::action
	Action_t437523947 * ___action_0;
	// AchievementController AchievementController/<TryLoginOnce>c__AnonStoreyD7::<>f__this
	AchievementController_t3677499403 * ___U3CU3Ef__this_1;

public:
	inline static int32_t get_offset_of_action_0() { return static_cast<int32_t>(offsetof(U3CTryLoginOnceU3Ec__AnonStoreyD7_t1832027815, ___action_0)); }
	inline Action_t437523947 * get_action_0() const { return ___action_0; }
	inline Action_t437523947 ** get_address_of_action_0() { return &___action_0; }
	inline void set_action_0(Action_t437523947 * value)
	{
		___action_0 = value;
		Il2CppCodeGenWriteBarrier(&___action_0, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__this_1() { return static_cast<int32_t>(offsetof(U3CTryLoginOnceU3Ec__AnonStoreyD7_t1832027815, ___U3CU3Ef__this_1)); }
	inline AchievementController_t3677499403 * get_U3CU3Ef__this_1() const { return ___U3CU3Ef__this_1; }
	inline AchievementController_t3677499403 ** get_address_of_U3CU3Ef__this_1() { return &___U3CU3Ef__this_1; }
	inline void set_U3CU3Ef__this_1(AchievementController_t3677499403 * value)
	{
		___U3CU3Ef__this_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__this_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
