﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SponsorPay.ErrorHandler
struct ErrorHandler_t1258969084;
// System.Object
struct Il2CppObject;
// SponsorPay.RequestError
struct RequestError_t1597064339;
// System.IAsyncResult
struct IAsyncResult_t537683269;
// System.AsyncCallback
struct AsyncCallback_t1363551830;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_IntPtr676692020.h"
#include "AssemblyU2DCSharp_SponsorPay_RequestError1597064339.h"
#include "mscorlib_System_AsyncCallback1363551830.h"

// System.Void SponsorPay.ErrorHandler::.ctor(System.Object,System.IntPtr)
extern "C"  void ErrorHandler__ctor_m1901955001 (ErrorHandler_t1258969084 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SponsorPay.ErrorHandler::Invoke(SponsorPay.RequestError)
extern "C"  void ErrorHandler_Invoke_m3479145318 (ErrorHandler_t1258969084 * __this, RequestError_t1597064339 * ___error0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
extern "C" void pinvoke_delegate_wrapper_ErrorHandler_t1258969084(Il2CppObject* delegate, RequestError_t1597064339 * ___error0);
// System.IAsyncResult SponsorPay.ErrorHandler::BeginInvoke(SponsorPay.RequestError,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * ErrorHandler_BeginInvoke_m3070923045 (ErrorHandler_t1258969084 * __this, RequestError_t1597064339 * ___error0, AsyncCallback_t1363551830 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SponsorPay.ErrorHandler::EndInvoke(System.IAsyncResult)
extern "C"  void ErrorHandler_EndInvoke_m3348348489 (ErrorHandler_t1258969084 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
