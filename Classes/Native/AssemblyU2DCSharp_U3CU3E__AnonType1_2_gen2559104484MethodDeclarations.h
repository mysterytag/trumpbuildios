﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// <>__AnonType1`2<System.Boolean,System.Boolean>
struct U3CU3E__AnonType1_2_t2559104484;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"

// System.Void <>__AnonType1`2<System.Boolean,System.Boolean>::.ctor(<show>__T,<hidden>__T)
extern "C"  void U3CU3E__AnonType1_2__ctor_m2869477419_gshared (U3CU3E__AnonType1_2_t2559104484 * __this, bool ___show0, bool ___hidden1, const MethodInfo* method);
#define U3CU3E__AnonType1_2__ctor_m2869477419(__this, ___show0, ___hidden1, method) ((  void (*) (U3CU3E__AnonType1_2_t2559104484 *, bool, bool, const MethodInfo*))U3CU3E__AnonType1_2__ctor_m2869477419_gshared)(__this, ___show0, ___hidden1, method)
// <show>__T <>__AnonType1`2<System.Boolean,System.Boolean>::get_show()
extern "C"  bool U3CU3E__AnonType1_2_get_show_m1770788966_gshared (U3CU3E__AnonType1_2_t2559104484 * __this, const MethodInfo* method);
#define U3CU3E__AnonType1_2_get_show_m1770788966(__this, method) ((  bool (*) (U3CU3E__AnonType1_2_t2559104484 *, const MethodInfo*))U3CU3E__AnonType1_2_get_show_m1770788966_gshared)(__this, method)
// <hidden>__T <>__AnonType1`2<System.Boolean,System.Boolean>::get_hidden()
extern "C"  bool U3CU3E__AnonType1_2_get_hidden_m1310890246_gshared (U3CU3E__AnonType1_2_t2559104484 * __this, const MethodInfo* method);
#define U3CU3E__AnonType1_2_get_hidden_m1310890246(__this, method) ((  bool (*) (U3CU3E__AnonType1_2_t2559104484 *, const MethodInfo*))U3CU3E__AnonType1_2_get_hidden_m1310890246_gshared)(__this, method)
// System.Boolean <>__AnonType1`2<System.Boolean,System.Boolean>::Equals(System.Object)
extern "C"  bool U3CU3E__AnonType1_2_Equals_m2125273029_gshared (U3CU3E__AnonType1_2_t2559104484 * __this, Il2CppObject * ___obj0, const MethodInfo* method);
#define U3CU3E__AnonType1_2_Equals_m2125273029(__this, ___obj0, method) ((  bool (*) (U3CU3E__AnonType1_2_t2559104484 *, Il2CppObject *, const MethodInfo*))U3CU3E__AnonType1_2_Equals_m2125273029_gshared)(__this, ___obj0, method)
// System.Int32 <>__AnonType1`2<System.Boolean,System.Boolean>::GetHashCode()
extern "C"  int32_t U3CU3E__AnonType1_2_GetHashCode_m2036580713_gshared (U3CU3E__AnonType1_2_t2559104484 * __this, const MethodInfo* method);
#define U3CU3E__AnonType1_2_GetHashCode_m2036580713(__this, method) ((  int32_t (*) (U3CU3E__AnonType1_2_t2559104484 *, const MethodInfo*))U3CU3E__AnonType1_2_GetHashCode_m2036580713_gshared)(__this, method)
// System.String <>__AnonType1`2<System.Boolean,System.Boolean>::ToString()
extern "C"  String_t* U3CU3E__AnonType1_2_ToString_m851545227_gshared (U3CU3E__AnonType1_2_t2559104484 * __this, const MethodInfo* method);
#define U3CU3E__AnonType1_2_ToString_m851545227(__this, method) ((  String_t* (*) (U3CU3E__AnonType1_2_t2559104484 *, const MethodInfo*))U3CU3E__AnonType1_2_ToString_m851545227_gshared)(__this, method)
