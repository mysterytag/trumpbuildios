﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.CreateObservable`1/Create<UnityEngine.Vector2>
struct Create_t3330625213;
// UniRx.IObserver`1<UnityEngine.Vector2>
struct IObserver_1_t1442361395;
// System.IDisposable
struct IDisposable_t1628921374;
// System.Exception
struct Exception_t1967233988;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Vector23525329788.h"
#include "mscorlib_System_Exception1967233988.h"

// System.Void UniRx.Operators.CreateObservable`1/Create<UnityEngine.Vector2>::.ctor(UniRx.IObserver`1<T>,System.IDisposable)
extern "C"  void Create__ctor_m2830912633_gshared (Create_t3330625213 * __this, Il2CppObject* ___observer0, Il2CppObject * ___cancel1, const MethodInfo* method);
#define Create__ctor_m2830912633(__this, ___observer0, ___cancel1, method) ((  void (*) (Create_t3330625213 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))Create__ctor_m2830912633_gshared)(__this, ___observer0, ___cancel1, method)
// System.Void UniRx.Operators.CreateObservable`1/Create<UnityEngine.Vector2>::OnNext(T)
extern "C"  void Create_OnNext_m98146169_gshared (Create_t3330625213 * __this, Vector2_t3525329788  ___value0, const MethodInfo* method);
#define Create_OnNext_m98146169(__this, ___value0, method) ((  void (*) (Create_t3330625213 *, Vector2_t3525329788 , const MethodInfo*))Create_OnNext_m98146169_gshared)(__this, ___value0, method)
// System.Void UniRx.Operators.CreateObservable`1/Create<UnityEngine.Vector2>::OnError(System.Exception)
extern "C"  void Create_OnError_m1901699208_gshared (Create_t3330625213 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method);
#define Create_OnError_m1901699208(__this, ___error0, method) ((  void (*) (Create_t3330625213 *, Exception_t1967233988 *, const MethodInfo*))Create_OnError_m1901699208_gshared)(__this, ___error0, method)
// System.Void UniRx.Operators.CreateObservable`1/Create<UnityEngine.Vector2>::OnCompleted()
extern "C"  void Create_OnCompleted_m485416923_gshared (Create_t3330625213 * __this, const MethodInfo* method);
#define Create_OnCompleted_m485416923(__this, method) ((  void (*) (Create_t3330625213 *, const MethodInfo*))Create_OnCompleted_m485416923_gshared)(__this, method)
