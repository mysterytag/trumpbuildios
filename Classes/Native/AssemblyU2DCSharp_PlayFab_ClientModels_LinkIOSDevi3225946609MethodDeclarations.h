﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.ClientModels.LinkIOSDeviceIDResult
struct LinkIOSDeviceIDResult_t3225946609;

#include "codegen/il2cpp-codegen.h"

// System.Void PlayFab.ClientModels.LinkIOSDeviceIDResult::.ctor()
extern "C"  void LinkIOSDeviceIDResult__ctor_m2019730520 (LinkIOSDeviceIDResult_t3225946609 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
