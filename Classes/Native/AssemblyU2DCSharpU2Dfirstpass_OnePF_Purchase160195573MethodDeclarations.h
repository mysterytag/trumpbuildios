﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// OnePF.Purchase
struct Purchase_t160195573;
// System.String
struct String_t;
// OnePF.JSON
struct JSON_t2649481148;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String968488902.h"
#include "AssemblyU2DCSharpU2Dfirstpass_OnePF_JSON2649481148.h"
#include "AssemblyU2DCSharpU2Dfirstpass_OnePF_Purchase160195573.h"

// System.Void OnePF.Purchase::.ctor()
extern "C"  void Purchase__ctor_m3923332508 (Purchase_t160195573 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OnePF.Purchase::.ctor(System.String)
extern "C"  void Purchase__ctor_m3455340262 (Purchase_t160195573 * __this, String_t* ___jsonString0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OnePF.Purchase::.ctor(OnePF.JSON)
extern "C"  void Purchase__ctor_m2219165762 (Purchase_t160195573 * __this, JSON_t2649481148 * ___json0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String OnePF.Purchase::get_ItemType()
extern "C"  String_t* Purchase_get_ItemType_m4191074113 (Purchase_t160195573 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OnePF.Purchase::set_ItemType(System.String)
extern "C"  void Purchase_set_ItemType_m1162244464 (Purchase_t160195573 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String OnePF.Purchase::get_OrderId()
extern "C"  String_t* Purchase_get_OrderId_m4249747671 (Purchase_t160195573 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OnePF.Purchase::set_OrderId(System.String)
extern "C"  void Purchase_set_OrderId_m1618951580 (Purchase_t160195573 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String OnePF.Purchase::get_PackageName()
extern "C"  String_t* Purchase_get_PackageName_m260911935 (Purchase_t160195573 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OnePF.Purchase::set_PackageName(System.String)
extern "C"  void Purchase_set_PackageName_m1213580212 (Purchase_t160195573 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String OnePF.Purchase::get_Sku()
extern "C"  String_t* Purchase_get_Sku_m889168491 (Purchase_t160195573 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OnePF.Purchase::set_Sku(System.String)
extern "C"  void Purchase_set_Sku_m830831880 (Purchase_t160195573 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 OnePF.Purchase::get_PurchaseTime()
extern "C"  int64_t Purchase_get_PurchaseTime_m3484110166 (Purchase_t160195573 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OnePF.Purchase::set_PurchaseTime(System.Int64)
extern "C"  void Purchase_set_PurchaseTime_m1592977253 (Purchase_t160195573 * __this, int64_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 OnePF.Purchase::get_PurchaseState()
extern "C"  int32_t Purchase_get_PurchaseState_m3082064043 (Purchase_t160195573 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OnePF.Purchase::set_PurchaseState(System.Int32)
extern "C"  void Purchase_set_PurchaseState_m2841895230 (Purchase_t160195573 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String OnePF.Purchase::get_DeveloperPayload()
extern "C"  String_t* Purchase_get_DeveloperPayload_m2728153080 (Purchase_t160195573 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OnePF.Purchase::set_DeveloperPayload(System.String)
extern "C"  void Purchase_set_DeveloperPayload_m560298777 (Purchase_t160195573 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String OnePF.Purchase::get_Token()
extern "C"  String_t* Purchase_get_Token_m787097863 (Purchase_t160195573 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OnePF.Purchase::set_Token(System.String)
extern "C"  void Purchase_set_Token_m2755081132 (Purchase_t160195573 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String OnePF.Purchase::get_OriginalJson()
extern "C"  String_t* Purchase_get_OriginalJson_m3460393005 (Purchase_t160195573 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OnePF.Purchase::set_OriginalJson(System.String)
extern "C"  void Purchase_set_OriginalJson_m1816068228 (Purchase_t160195573 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String OnePF.Purchase::get_Signature()
extern "C"  String_t* Purchase_get_Signature_m3383715206 (Purchase_t160195573 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OnePF.Purchase::set_Signature(System.String)
extern "C"  void Purchase_set_Signature_m1181936909 (Purchase_t160195573 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String OnePF.Purchase::get_AppstoreName()
extern "C"  String_t* Purchase_get_AppstoreName_m1419219807 (Purchase_t160195573 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OnePF.Purchase::set_AppstoreName(System.String)
extern "C"  void Purchase_set_AppstoreName_m3218609810 (Purchase_t160195573 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String OnePF.Purchase::get_Receipt()
extern "C"  String_t* Purchase_get_Receipt_m1046015558 (Purchase_t160195573 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OnePF.Purchase::set_Receipt(System.String)
extern "C"  void Purchase_set_Receipt_m3800554637 (Purchase_t160195573 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// OnePF.Purchase OnePF.Purchase::CreateFromSku(System.String)
extern "C"  Purchase_t160195573 * Purchase_CreateFromSku_m2993707363 (Il2CppObject * __this /* static, unused */, String_t* ___sku0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// OnePF.Purchase OnePF.Purchase::CreateFromSku(System.String,System.String)
extern "C"  Purchase_t160195573 * Purchase_CreateFromSku_m3333675103 (Il2CppObject * __this /* static, unused */, String_t* ___sku0, String_t* ___developerPayload1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String OnePF.Purchase::ToString()
extern "C"  String_t* Purchase_ToString_m1003583863 (Purchase_t160195573 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OnePF.Purchase::AddIOSHack(OnePF.Purchase)
extern "C"  void Purchase_AddIOSHack_m2229156322 (Il2CppObject * __this /* static, unused */, Purchase_t160195573 * ___p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String OnePF.Purchase::Serialize()
extern "C"  String_t* Purchase_Serialize_m500090903 (Purchase_t160195573 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
