﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UniRx.IObservable`1<UniRx.IObservable`1<UniRx.Unit>>
struct IObservable_1_t2075882766;

#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Oper1622431009.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Operators.MergeObservable`1<UniRx.Unit>
struct  MergeObservable_1_t1064204364  : public OperatorObservableBase_1_t1622431009
{
public:
	// UniRx.IObservable`1<UniRx.IObservable`1<T>> UniRx.Operators.MergeObservable`1::sources
	Il2CppObject* ___sources_1;
	// System.Int32 UniRx.Operators.MergeObservable`1::maxConcurrent
	int32_t ___maxConcurrent_2;

public:
	inline static int32_t get_offset_of_sources_1() { return static_cast<int32_t>(offsetof(MergeObservable_1_t1064204364, ___sources_1)); }
	inline Il2CppObject* get_sources_1() const { return ___sources_1; }
	inline Il2CppObject** get_address_of_sources_1() { return &___sources_1; }
	inline void set_sources_1(Il2CppObject* value)
	{
		___sources_1 = value;
		Il2CppCodeGenWriteBarrier(&___sources_1, value);
	}

	inline static int32_t get_offset_of_maxConcurrent_2() { return static_cast<int32_t>(offsetof(MergeObservable_1_t1064204364, ___maxConcurrent_2)); }
	inline int32_t get_maxConcurrent_2() const { return ___maxConcurrent_2; }
	inline int32_t* get_address_of_maxConcurrent_2() { return &___maxConcurrent_2; }
	inline void set_maxConcurrent_2(int32_t value)
	{
		___maxConcurrent_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
