﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "System_Core_System_Action_2_gen3407553161MethodDeclarations.h"

// System.Void System.Action`2<GooglePlayGames.BasicApi.ResponseStatus,System.Collections.Generic.List`1<GooglePlayGames.BasicApi.Quests.IQuest>>::.ctor(System.Object,System.IntPtr)
#define Action_2__ctor_m620255913(__this, ___object0, ___method1, method) ((  void (*) (Action_2_t3127509898 *, Il2CppObject *, IntPtr_t, const MethodInfo*))Action_2__ctor_m884641853_gshared)(__this, ___object0, ___method1, method)
// System.Void System.Action`2<GooglePlayGames.BasicApi.ResponseStatus,System.Collections.Generic.List`1<GooglePlayGames.BasicApi.Quests.IQuest>>::Invoke(T1,T2)
#define Action_2_Invoke_m2619166178(__this, ___arg10, ___arg21, method) ((  void (*) (Action_2_t3127509898 *, int32_t, List_1_t557063157 *, const MethodInfo*))Action_2_Invoke_m3208528846_gshared)(__this, ___arg10, ___arg21, method)
// System.IAsyncResult System.Action`2<GooglePlayGames.BasicApi.ResponseStatus,System.Collections.Generic.List`1<GooglePlayGames.BasicApi.Quests.IQuest>>::BeginInvoke(T1,T2,System.AsyncCallback,System.Object)
#define Action_2_BeginInvoke_m769314305(__this, ___arg10, ___arg21, ___callback2, ___object3, method) ((  Il2CppObject * (*) (Action_2_t3127509898 *, int32_t, List_1_t557063157 *, AsyncCallback_t1363551830 *, Il2CppObject *, const MethodInfo*))Action_2_BeginInvoke_m3751373293_gshared)(__this, ___arg10, ___arg21, ___callback2, ___object3, method)
// System.Void System.Action`2<GooglePlayGames.BasicApi.ResponseStatus,System.Collections.Generic.List`1<GooglePlayGames.BasicApi.Quests.IQuest>>::EndInvoke(System.IAsyncResult)
#define Action_2_EndInvoke_m602769721(__this, ___result0, method) ((  void (*) (Action_2_t3127509898 *, Il2CppObject *, const MethodInfo*))Action_2_EndInvoke_m2024909005_gshared)(__this, ___result0, method)
