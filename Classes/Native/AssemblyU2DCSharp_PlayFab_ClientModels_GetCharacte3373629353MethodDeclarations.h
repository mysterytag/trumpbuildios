﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.ClientModels.GetCharacterStatisticsRequest
struct GetCharacterStatisticsRequest_t3373629353;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String968488902.h"

// System.Void PlayFab.ClientModels.GetCharacterStatisticsRequest::.ctor()
extern "C"  void GetCharacterStatisticsRequest__ctor_m3758040992 (GetCharacterStatisticsRequest_t3373629353 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PlayFab.ClientModels.GetCharacterStatisticsRequest::get_CharacterId()
extern "C"  String_t* GetCharacterStatisticsRequest_get_CharacterId_m3504203830 (GetCharacterStatisticsRequest_t3373629353 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.GetCharacterStatisticsRequest::set_CharacterId(System.String)
extern "C"  void GetCharacterStatisticsRequest_set_CharacterId_m3213052573 (GetCharacterStatisticsRequest_t3373629353 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
