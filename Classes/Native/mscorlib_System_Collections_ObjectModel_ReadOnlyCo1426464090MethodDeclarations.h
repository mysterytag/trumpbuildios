﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.ObjectModel.ReadOnlyCollection`1<UniRx.Unit>
struct ReadOnlyCollection_1_t1426464090;
// System.Collections.Generic.IList`1<UniRx.Unit>
struct IList_1_t429811056;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t287207039;
// System.Object
struct Il2CppObject;
// UniRx.Unit[]
struct UnitU5BU5D_t1267952403;
// System.Collections.Generic.IEnumerator`1<UniRx.Unit>
struct IEnumerator_1_t4041392486;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Unit2558286038.h"
#include "mscorlib_System_Array2840145358.h"
#include "mscorlib_System_Object837106420.h"

// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UniRx.Unit>::.ctor(System.Collections.Generic.IList`1<T>)
extern "C"  void ReadOnlyCollection_1__ctor_m1203125012_gshared (ReadOnlyCollection_1_t1426464090 * __this, Il2CppObject* ___list0, const MethodInfo* method);
#define ReadOnlyCollection_1__ctor_m1203125012(__this, ___list0, method) ((  void (*) (ReadOnlyCollection_1_t1426464090 *, Il2CppObject*, const MethodInfo*))ReadOnlyCollection_1__ctor_m1203125012_gshared)(__this, ___list0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UniRx.Unit>::System.Collections.Generic.ICollection<T>.Add(T)
extern "C"  void ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m3005137662_gshared (ReadOnlyCollection_1_t1426464090 * __this, Unit_t2558286038  ___item0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m3005137662(__this, ___item0, method) ((  void (*) (ReadOnlyCollection_1_t1426464090 *, Unit_t2558286038 , const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m3005137662_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UniRx.Unit>::System.Collections.Generic.ICollection<T>.Clear()
extern "C"  void ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m676678028_gshared (ReadOnlyCollection_1_t1426464090 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m676678028(__this, method) ((  void (*) (ReadOnlyCollection_1_t1426464090 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m676678028_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UniRx.Unit>::System.Collections.Generic.IList<T>.Insert(System.Int32,T)
extern "C"  void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m2440700005_gshared (ReadOnlyCollection_1_t1426464090 * __this, int32_t ___index0, Unit_t2558286038  ___item1, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m2440700005(__this, ___index0, ___item1, method) ((  void (*) (ReadOnlyCollection_1_t1426464090 *, int32_t, Unit_t2558286038 , const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m2440700005_gshared)(__this, ___index0, ___item1, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<UniRx.Unit>::System.Collections.Generic.ICollection<T>.Remove(T)
extern "C"  bool ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m2337563001_gshared (ReadOnlyCollection_1_t1426464090 * __this, Unit_t2558286038  ___item0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m2337563001(__this, ___item0, method) ((  bool (*) (ReadOnlyCollection_1_t1426464090 *, Unit_t2558286038 , const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m2337563001_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UniRx.Unit>::System.Collections.Generic.IList<T>.RemoveAt(System.Int32)
extern "C"  void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m314552875_gshared (ReadOnlyCollection_1_t1426464090 * __this, int32_t ___index0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m314552875(__this, ___index0, method) ((  void (*) (ReadOnlyCollection_1_t1426464090 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m314552875_gshared)(__this, ___index0, method)
// T System.Collections.ObjectModel.ReadOnlyCollection`1<UniRx.Unit>::System.Collections.Generic.IList<T>.get_Item(System.Int32)
extern "C"  Unit_t2558286038  ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m3770151377_gshared (ReadOnlyCollection_1_t1426464090 * __this, int32_t ___index0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m3770151377(__this, ___index0, method) ((  Unit_t2558286038  (*) (ReadOnlyCollection_1_t1426464090 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m3770151377_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UniRx.Unit>::System.Collections.Generic.IList<T>.set_Item(System.Int32,T)
extern "C"  void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m2270157820_gshared (ReadOnlyCollection_1_t1426464090 * __this, int32_t ___index0, Unit_t2558286038  ___value1, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m2270157820(__this, ___index0, ___value1, method) ((  void (*) (ReadOnlyCollection_1_t1426464090 *, int32_t, Unit_t2558286038 , const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m2270157820_gshared)(__this, ___index0, ___value1, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<UniRx.Unit>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C"  bool ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m2794615286_gshared (ReadOnlyCollection_1_t1426464090 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m2794615286(__this, method) ((  bool (*) (ReadOnlyCollection_1_t1426464090 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m2794615286_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UniRx.Unit>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m2843720643_gshared (ReadOnlyCollection_1_t1426464090 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m2843720643(__this, ___array0, ___index1, method) ((  void (*) (ReadOnlyCollection_1_t1426464090 *, Il2CppArray *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m2843720643_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.ObjectModel.ReadOnlyCollection`1<UniRx.Unit>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m4143504274_gshared (ReadOnlyCollection_1_t1426464090 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m4143504274(__this, method) ((  Il2CppObject * (*) (ReadOnlyCollection_1_t1426464090 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m4143504274_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<UniRx.Unit>::System.Collections.IList.Add(System.Object)
extern "C"  int32_t ReadOnlyCollection_1_System_Collections_IList_Add_m1615279851_gshared (ReadOnlyCollection_1_t1426464090 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Add_m1615279851(__this, ___value0, method) ((  int32_t (*) (ReadOnlyCollection_1_t1426464090 *, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Add_m1615279851_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UniRx.Unit>::System.Collections.IList.Clear()
extern "C"  void ReadOnlyCollection_1_System_Collections_IList_Clear_m510957265_gshared (ReadOnlyCollection_1_t1426464090 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Clear_m510957265(__this, method) ((  void (*) (ReadOnlyCollection_1_t1426464090 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Clear_m510957265_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<UniRx.Unit>::System.Collections.IList.Contains(System.Object)
extern "C"  bool ReadOnlyCollection_1_System_Collections_IList_Contains_m1047363637_gshared (ReadOnlyCollection_1_t1426464090 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Contains_m1047363637(__this, ___value0, method) ((  bool (*) (ReadOnlyCollection_1_t1426464090 *, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Contains_m1047363637_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<UniRx.Unit>::System.Collections.IList.IndexOf(System.Object)
extern "C"  int32_t ReadOnlyCollection_1_System_Collections_IList_IndexOf_m2582380483_gshared (ReadOnlyCollection_1_t1426464090 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_IndexOf_m2582380483(__this, ___value0, method) ((  int32_t (*) (ReadOnlyCollection_1_t1426464090 *, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_IndexOf_m2582380483_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UniRx.Unit>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C"  void ReadOnlyCollection_1_System_Collections_IList_Insert_m162249398_gshared (ReadOnlyCollection_1_t1426464090 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Insert_m162249398(__this, ___index0, ___value1, method) ((  void (*) (ReadOnlyCollection_1_t1426464090 *, int32_t, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Insert_m162249398_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UniRx.Unit>::System.Collections.IList.Remove(System.Object)
extern "C"  void ReadOnlyCollection_1_System_Collections_IList_Remove_m4250975922_gshared (ReadOnlyCollection_1_t1426464090 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Remove_m4250975922(__this, ___value0, method) ((  void (*) (ReadOnlyCollection_1_t1426464090 *, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Remove_m4250975922_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UniRx.Unit>::System.Collections.IList.RemoveAt(System.Int32)
extern "C"  void ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m221623110_gshared (ReadOnlyCollection_1_t1426464090 * __this, int32_t ___index0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m221623110(__this, ___index0, method) ((  void (*) (ReadOnlyCollection_1_t1426464090 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m221623110_gshared)(__this, ___index0, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<UniRx.Unit>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m3362647815_gshared (ReadOnlyCollection_1_t1426464090 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m3362647815(__this, method) ((  bool (*) (ReadOnlyCollection_1_t1426464090 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m3362647815_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.ReadOnlyCollection`1<UniRx.Unit>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m973496889_gshared (ReadOnlyCollection_1_t1426464090 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m973496889(__this, method) ((  Il2CppObject * (*) (ReadOnlyCollection_1_t1426464090 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m973496889_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<UniRx.Unit>::System.Collections.IList.get_IsFixedSize()
extern "C"  bool ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m4224891748_gshared (ReadOnlyCollection_1_t1426464090 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m4224891748(__this, method) ((  bool (*) (ReadOnlyCollection_1_t1426464090 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m4224891748_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<UniRx.Unit>::System.Collections.IList.get_IsReadOnly()
extern "C"  bool ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m1837221653_gshared (ReadOnlyCollection_1_t1426464090 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m1837221653(__this, method) ((  bool (*) (ReadOnlyCollection_1_t1426464090 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m1837221653_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.ReadOnlyCollection`1<UniRx.Unit>::System.Collections.IList.get_Item(System.Int32)
extern "C"  Il2CppObject * ReadOnlyCollection_1_System_Collections_IList_get_Item_m293251456_gshared (ReadOnlyCollection_1_t1426464090 * __this, int32_t ___index0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_get_Item_m293251456(__this, ___index0, method) ((  Il2CppObject * (*) (ReadOnlyCollection_1_t1426464090 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_Item_m293251456_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UniRx.Unit>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C"  void ReadOnlyCollection_1_System_Collections_IList_set_Item_m1916801293_gshared (ReadOnlyCollection_1_t1426464090 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_set_Item_m1916801293(__this, ___index0, ___value1, method) ((  void (*) (ReadOnlyCollection_1_t1426464090 *, int32_t, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_set_Item_m1916801293_gshared)(__this, ___index0, ___value1, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<UniRx.Unit>::Contains(T)
extern "C"  bool ReadOnlyCollection_1_Contains_m1342544830_gshared (ReadOnlyCollection_1_t1426464090 * __this, Unit_t2558286038  ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_Contains_m1342544830(__this, ___value0, method) ((  bool (*) (ReadOnlyCollection_1_t1426464090 *, Unit_t2558286038 , const MethodInfo*))ReadOnlyCollection_1_Contains_m1342544830_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UniRx.Unit>::CopyTo(T[],System.Int32)
extern "C"  void ReadOnlyCollection_1_CopyTo_m41791534_gshared (ReadOnlyCollection_1_t1426464090 * __this, UnitU5BU5D_t1267952403* ___array0, int32_t ___index1, const MethodInfo* method);
#define ReadOnlyCollection_1_CopyTo_m41791534(__this, ___array0, ___index1, method) ((  void (*) (ReadOnlyCollection_1_t1426464090 *, UnitU5BU5D_t1267952403*, int32_t, const MethodInfo*))ReadOnlyCollection_1_CopyTo_m41791534_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.ObjectModel.ReadOnlyCollection`1<UniRx.Unit>::GetEnumerator()
extern "C"  Il2CppObject* ReadOnlyCollection_1_GetEnumerator_m2009052181_gshared (ReadOnlyCollection_1_t1426464090 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_GetEnumerator_m2009052181(__this, method) ((  Il2CppObject* (*) (ReadOnlyCollection_1_t1426464090 *, const MethodInfo*))ReadOnlyCollection_1_GetEnumerator_m2009052181_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<UniRx.Unit>::IndexOf(T)
extern "C"  int32_t ReadOnlyCollection_1_IndexOf_m4064786042_gshared (ReadOnlyCollection_1_t1426464090 * __this, Unit_t2558286038  ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_IndexOf_m4064786042(__this, ___value0, method) ((  int32_t (*) (ReadOnlyCollection_1_t1426464090 *, Unit_t2558286038 , const MethodInfo*))ReadOnlyCollection_1_IndexOf_m4064786042_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<UniRx.Unit>::get_Count()
extern "C"  int32_t ReadOnlyCollection_1_get_Count_m1271623233_gshared (ReadOnlyCollection_1_t1426464090 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_get_Count_m1271623233(__this, method) ((  int32_t (*) (ReadOnlyCollection_1_t1426464090 *, const MethodInfo*))ReadOnlyCollection_1_get_Count_m1271623233_gshared)(__this, method)
// T System.Collections.ObjectModel.ReadOnlyCollection`1<UniRx.Unit>::get_Item(System.Int32)
extern "C"  Unit_t2558286038  ReadOnlyCollection_1_get_Item_m1477683217_gshared (ReadOnlyCollection_1_t1426464090 * __this, int32_t ___index0, const MethodInfo* method);
#define ReadOnlyCollection_1_get_Item_m1477683217(__this, ___index0, method) ((  Unit_t2558286038  (*) (ReadOnlyCollection_1_t1426464090 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_get_Item_m1477683217_gshared)(__this, ___index0, method)
