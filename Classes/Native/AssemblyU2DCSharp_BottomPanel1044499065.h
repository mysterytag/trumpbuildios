﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// TutorialArrow
struct TutorialArrow_t2479747883;
// TutorialPlayer
struct TutorialPlayer_t4281139199;
// GlobalStat
struct GlobalStat_t1134678199;
// TableButtons
struct TableButtons_t1868573683;
// GuiController
struct GuiController_t1495593751;
// GameController
struct GameController_t2782302542;
// UnityEngine.GameObject
struct GameObject_t4012695102;
// System.String
struct String_t;

#include "UnityEngine_UnityEngine_MonoBehaviour3012272455.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BottomPanel
struct  BottomPanel_t1044499065  : public MonoBehaviour_t3012272455
{
public:
	// TutorialArrow BottomPanel::TutorialArrow
	TutorialArrow_t2479747883 * ___TutorialArrow_2;
	// TutorialPlayer BottomPanel::TutorialPlayer
	TutorialPlayer_t4281139199 * ___TutorialPlayer_3;
	// GlobalStat BottomPanel::GlobalStat
	GlobalStat_t1134678199 * ___GlobalStat_4;
	// TableButtons BottomPanel::TableButtons
	TableButtons_t1868573683 * ___TableButtons_5;
	// GuiController BottomPanel::GuiController
	GuiController_t1495593751 * ___GuiController_6;
	// GameController BottomPanel::GameController
	GameController_t2782302542 * ___GameController_7;
	// UnityEngine.GameObject BottomPanel::bottomCase
	GameObject_t4012695102 * ___bottomCase_8;
	// UnityEngine.GameObject BottomPanel::bottomSafe
	GameObject_t4012695102 * ___bottomSafe_9;
	// UnityEngine.GameObject BottomPanel::bottomAbacus
	GameObject_t4012695102 * ___bottomAbacus_10;
	// UnityEngine.GameObject BottomPanel::bottomToilet
	GameObject_t4012695102 * ___bottomToilet_11;
	// UnityEngine.GameObject BottomPanel::settingMenu
	GameObject_t4012695102 * ___settingMenu_12;
	// System.String BottomPanel::Botname
	String_t* ___Botname_13;

public:
	inline static int32_t get_offset_of_TutorialArrow_2() { return static_cast<int32_t>(offsetof(BottomPanel_t1044499065, ___TutorialArrow_2)); }
	inline TutorialArrow_t2479747883 * get_TutorialArrow_2() const { return ___TutorialArrow_2; }
	inline TutorialArrow_t2479747883 ** get_address_of_TutorialArrow_2() { return &___TutorialArrow_2; }
	inline void set_TutorialArrow_2(TutorialArrow_t2479747883 * value)
	{
		___TutorialArrow_2 = value;
		Il2CppCodeGenWriteBarrier(&___TutorialArrow_2, value);
	}

	inline static int32_t get_offset_of_TutorialPlayer_3() { return static_cast<int32_t>(offsetof(BottomPanel_t1044499065, ___TutorialPlayer_3)); }
	inline TutorialPlayer_t4281139199 * get_TutorialPlayer_3() const { return ___TutorialPlayer_3; }
	inline TutorialPlayer_t4281139199 ** get_address_of_TutorialPlayer_3() { return &___TutorialPlayer_3; }
	inline void set_TutorialPlayer_3(TutorialPlayer_t4281139199 * value)
	{
		___TutorialPlayer_3 = value;
		Il2CppCodeGenWriteBarrier(&___TutorialPlayer_3, value);
	}

	inline static int32_t get_offset_of_GlobalStat_4() { return static_cast<int32_t>(offsetof(BottomPanel_t1044499065, ___GlobalStat_4)); }
	inline GlobalStat_t1134678199 * get_GlobalStat_4() const { return ___GlobalStat_4; }
	inline GlobalStat_t1134678199 ** get_address_of_GlobalStat_4() { return &___GlobalStat_4; }
	inline void set_GlobalStat_4(GlobalStat_t1134678199 * value)
	{
		___GlobalStat_4 = value;
		Il2CppCodeGenWriteBarrier(&___GlobalStat_4, value);
	}

	inline static int32_t get_offset_of_TableButtons_5() { return static_cast<int32_t>(offsetof(BottomPanel_t1044499065, ___TableButtons_5)); }
	inline TableButtons_t1868573683 * get_TableButtons_5() const { return ___TableButtons_5; }
	inline TableButtons_t1868573683 ** get_address_of_TableButtons_5() { return &___TableButtons_5; }
	inline void set_TableButtons_5(TableButtons_t1868573683 * value)
	{
		___TableButtons_5 = value;
		Il2CppCodeGenWriteBarrier(&___TableButtons_5, value);
	}

	inline static int32_t get_offset_of_GuiController_6() { return static_cast<int32_t>(offsetof(BottomPanel_t1044499065, ___GuiController_6)); }
	inline GuiController_t1495593751 * get_GuiController_6() const { return ___GuiController_6; }
	inline GuiController_t1495593751 ** get_address_of_GuiController_6() { return &___GuiController_6; }
	inline void set_GuiController_6(GuiController_t1495593751 * value)
	{
		___GuiController_6 = value;
		Il2CppCodeGenWriteBarrier(&___GuiController_6, value);
	}

	inline static int32_t get_offset_of_GameController_7() { return static_cast<int32_t>(offsetof(BottomPanel_t1044499065, ___GameController_7)); }
	inline GameController_t2782302542 * get_GameController_7() const { return ___GameController_7; }
	inline GameController_t2782302542 ** get_address_of_GameController_7() { return &___GameController_7; }
	inline void set_GameController_7(GameController_t2782302542 * value)
	{
		___GameController_7 = value;
		Il2CppCodeGenWriteBarrier(&___GameController_7, value);
	}

	inline static int32_t get_offset_of_bottomCase_8() { return static_cast<int32_t>(offsetof(BottomPanel_t1044499065, ___bottomCase_8)); }
	inline GameObject_t4012695102 * get_bottomCase_8() const { return ___bottomCase_8; }
	inline GameObject_t4012695102 ** get_address_of_bottomCase_8() { return &___bottomCase_8; }
	inline void set_bottomCase_8(GameObject_t4012695102 * value)
	{
		___bottomCase_8 = value;
		Il2CppCodeGenWriteBarrier(&___bottomCase_8, value);
	}

	inline static int32_t get_offset_of_bottomSafe_9() { return static_cast<int32_t>(offsetof(BottomPanel_t1044499065, ___bottomSafe_9)); }
	inline GameObject_t4012695102 * get_bottomSafe_9() const { return ___bottomSafe_9; }
	inline GameObject_t4012695102 ** get_address_of_bottomSafe_9() { return &___bottomSafe_9; }
	inline void set_bottomSafe_9(GameObject_t4012695102 * value)
	{
		___bottomSafe_9 = value;
		Il2CppCodeGenWriteBarrier(&___bottomSafe_9, value);
	}

	inline static int32_t get_offset_of_bottomAbacus_10() { return static_cast<int32_t>(offsetof(BottomPanel_t1044499065, ___bottomAbacus_10)); }
	inline GameObject_t4012695102 * get_bottomAbacus_10() const { return ___bottomAbacus_10; }
	inline GameObject_t4012695102 ** get_address_of_bottomAbacus_10() { return &___bottomAbacus_10; }
	inline void set_bottomAbacus_10(GameObject_t4012695102 * value)
	{
		___bottomAbacus_10 = value;
		Il2CppCodeGenWriteBarrier(&___bottomAbacus_10, value);
	}

	inline static int32_t get_offset_of_bottomToilet_11() { return static_cast<int32_t>(offsetof(BottomPanel_t1044499065, ___bottomToilet_11)); }
	inline GameObject_t4012695102 * get_bottomToilet_11() const { return ___bottomToilet_11; }
	inline GameObject_t4012695102 ** get_address_of_bottomToilet_11() { return &___bottomToilet_11; }
	inline void set_bottomToilet_11(GameObject_t4012695102 * value)
	{
		___bottomToilet_11 = value;
		Il2CppCodeGenWriteBarrier(&___bottomToilet_11, value);
	}

	inline static int32_t get_offset_of_settingMenu_12() { return static_cast<int32_t>(offsetof(BottomPanel_t1044499065, ___settingMenu_12)); }
	inline GameObject_t4012695102 * get_settingMenu_12() const { return ___settingMenu_12; }
	inline GameObject_t4012695102 ** get_address_of_settingMenu_12() { return &___settingMenu_12; }
	inline void set_settingMenu_12(GameObject_t4012695102 * value)
	{
		___settingMenu_12 = value;
		Il2CppCodeGenWriteBarrier(&___settingMenu_12, value);
	}

	inline static int32_t get_offset_of_Botname_13() { return static_cast<int32_t>(offsetof(BottomPanel_t1044499065, ___Botname_13)); }
	inline String_t* get_Botname_13() const { return ___Botname_13; }
	inline String_t** get_address_of_Botname_13() { return &___Botname_13; }
	inline void set_Botname_13(String_t* value)
	{
		___Botname_13 = value;
		Il2CppCodeGenWriteBarrier(&___Botname_13, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
