﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PopMult/<ShowCoroutine>c__Iterator33
struct U3CShowCoroutineU3Ec__Iterator33_t2818117362;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void PopMult/<ShowCoroutine>c__Iterator33::.ctor()
extern "C"  void U3CShowCoroutineU3Ec__Iterator33__ctor_m1815397211 (U3CShowCoroutineU3Ec__Iterator33_t2818117362 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object PopMult/<ShowCoroutine>c__Iterator33::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CShowCoroutineU3Ec__Iterator33_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1074738913 (U3CShowCoroutineU3Ec__Iterator33_t2818117362 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object PopMult/<ShowCoroutine>c__Iterator33::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CShowCoroutineU3Ec__Iterator33_System_Collections_IEnumerator_get_Current_m287644789 (U3CShowCoroutineU3Ec__Iterator33_t2818117362 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PopMult/<ShowCoroutine>c__Iterator33::MoveNext()
extern "C"  bool U3CShowCoroutineU3Ec__Iterator33_MoveNext_m3165169313 (U3CShowCoroutineU3Ec__Iterator33_t2818117362 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PopMult/<ShowCoroutine>c__Iterator33::Dispose()
extern "C"  void U3CShowCoroutineU3Ec__Iterator33_Dispose_m738280664 (U3CShowCoroutineU3Ec__Iterator33_t2818117362 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PopMult/<ShowCoroutine>c__Iterator33::Reset()
extern "C"  void U3CShowCoroutineU3Ec__Iterator33_Reset_m3756797448 (U3CShowCoroutineU3Ec__Iterator33_t2818117362 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
