﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.CombineLatestObservable`3/CombineLatest/RightObserver<System.Boolean,System.Boolean,System.Object>
struct RightObserver_t2610190823;
// UniRx.Operators.CombineLatestObservable`3/CombineLatest<System.Boolean,System.Boolean,System.Object>
struct CombineLatest_t4145300795;
// System.Exception
struct Exception_t1967233988;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Exception1967233988.h"

// System.Void UniRx.Operators.CombineLatestObservable`3/CombineLatest/RightObserver<System.Boolean,System.Boolean,System.Object>::.ctor(UniRx.Operators.CombineLatestObservable`3/CombineLatest<TLeft,TRight,TResult>)
extern "C"  void RightObserver__ctor_m466081504_gshared (RightObserver_t2610190823 * __this, CombineLatest_t4145300795 * ___parent0, const MethodInfo* method);
#define RightObserver__ctor_m466081504(__this, ___parent0, method) ((  void (*) (RightObserver_t2610190823 *, CombineLatest_t4145300795 *, const MethodInfo*))RightObserver__ctor_m466081504_gshared)(__this, ___parent0, method)
// System.Void UniRx.Operators.CombineLatestObservable`3/CombineLatest/RightObserver<System.Boolean,System.Boolean,System.Object>::OnNext(TRight)
extern "C"  void RightObserver_OnNext_m1921385308_gshared (RightObserver_t2610190823 * __this, bool ___value0, const MethodInfo* method);
#define RightObserver_OnNext_m1921385308(__this, ___value0, method) ((  void (*) (RightObserver_t2610190823 *, bool, const MethodInfo*))RightObserver_OnNext_m1921385308_gshared)(__this, ___value0, method)
// System.Void UniRx.Operators.CombineLatestObservable`3/CombineLatest/RightObserver<System.Boolean,System.Boolean,System.Object>::OnError(System.Exception)
extern "C"  void RightObserver_OnError_m4186505257_gshared (RightObserver_t2610190823 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method);
#define RightObserver_OnError_m4186505257(__this, ___error0, method) ((  void (*) (RightObserver_t2610190823 *, Exception_t1967233988 *, const MethodInfo*))RightObserver_OnError_m4186505257_gshared)(__this, ___error0, method)
// System.Void UniRx.Operators.CombineLatestObservable`3/CombineLatest/RightObserver<System.Boolean,System.Boolean,System.Object>::OnCompleted()
extern "C"  void RightObserver_OnCompleted_m478144764_gshared (RightObserver_t2610190823 * __this, const MethodInfo* method);
#define RightObserver_OnCompleted_m478144764(__this, method) ((  void (*) (RightObserver_t2610190823 *, const MethodInfo*))RightObserver_OnCompleted_m478144764_gshared)(__this, method)
