﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Array
struct Il2CppArray;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen615556045.h"
#include "mscorlib_System_Array2840145358.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_TimeInterval_1_708065542.h"

// System.Void System.Array/InternalEnumerator`1<UniRx.TimeInterval`1<System.Object>>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m513364838_gshared (InternalEnumerator_1_t615556045 * __this, Il2CppArray * ___array0, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m513364838(__this, ___array0, method) ((  void (*) (InternalEnumerator_1_t615556045 *, Il2CppArray *, const MethodInfo*))InternalEnumerator_1__ctor_m513364838_gshared)(__this, ___array0, method)
// System.Void System.Array/InternalEnumerator`1<UniRx.TimeInterval`1<System.Object>>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m806448250_gshared (InternalEnumerator_1_t615556045 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m806448250(__this, method) ((  void (*) (InternalEnumerator_1_t615556045 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m806448250_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<UniRx.TimeInterval`1<System.Object>>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m4215271152_gshared (InternalEnumerator_1_t615556045 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m4215271152(__this, method) ((  Il2CppObject * (*) (InternalEnumerator_1_t615556045 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m4215271152_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<UniRx.TimeInterval`1<System.Object>>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m264160445_gshared (InternalEnumerator_1_t615556045 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m264160445(__this, method) ((  void (*) (InternalEnumerator_1_t615556045 *, const MethodInfo*))InternalEnumerator_1_Dispose_m264160445_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<UniRx.TimeInterval`1<System.Object>>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m2397519658_gshared (InternalEnumerator_1_t615556045 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m2397519658(__this, method) ((  bool (*) (InternalEnumerator_1_t615556045 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m2397519658_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<UniRx.TimeInterval`1<System.Object>>::get_Current()
extern "C"  TimeInterval_1_t708065542  InternalEnumerator_1_get_Current_m655908879_gshared (InternalEnumerator_1_t615556045 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m655908879(__this, method) ((  TimeInterval_1_t708065542  (*) (InternalEnumerator_1_t615556045 *, const MethodInfo*))InternalEnumerator_1_get_Current_m655908879_gshared)(__this, method)
