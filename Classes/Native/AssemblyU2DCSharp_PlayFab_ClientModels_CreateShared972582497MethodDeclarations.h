﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.ClientModels.CreateSharedGroupRequest
struct CreateSharedGroupRequest_t972582497;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String968488902.h"

// System.Void PlayFab.ClientModels.CreateSharedGroupRequest::.ctor()
extern "C"  void CreateSharedGroupRequest__ctor_m3581596924 (CreateSharedGroupRequest_t972582497 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PlayFab.ClientModels.CreateSharedGroupRequest::get_SharedGroupId()
extern "C"  String_t* CreateSharedGroupRequest_get_SharedGroupId_m4116235689 (CreateSharedGroupRequest_t972582497 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.CreateSharedGroupRequest::set_SharedGroupId(System.String)
extern "C"  void CreateSharedGroupRequest_set_SharedGroupId_m3624410864 (CreateSharedGroupRequest_t972582497 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
