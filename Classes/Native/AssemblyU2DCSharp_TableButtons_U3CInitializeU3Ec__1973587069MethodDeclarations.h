﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// TableButtons/<Initialize>c__AnonStorey156
struct U3CInitializeU3Ec__AnonStorey156_t1973587069;
// UpgradeButton
struct UpgradeButton_t1475467342;
// UpgradeCell
struct UpgradeCell_t4117746046;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_UpgradeButton1475467342.h"
#include "AssemblyU2DCSharp_UpgradeCell4117746046.h"

// System.Void TableButtons/<Initialize>c__AnonStorey156::.ctor()
extern "C"  void U3CInitializeU3Ec__AnonStorey156__ctor_m2965505138 (U3CInitializeU3Ec__AnonStorey156_t1973587069 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TableButtons/<Initialize>c__AnonStorey156::<>m__254(UpgradeButton,UpgradeCell,System.Int32)
extern "C"  void U3CInitializeU3Ec__AnonStorey156_U3CU3Em__254_m804113821 (U3CInitializeU3Ec__AnonStorey156_t1973587069 * __this, UpgradeButton_t1475467342 * ___upgradeButton0, UpgradeCell_t4117746046 * ___upgradeCell1, int32_t ___arg32, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean TableButtons/<Initialize>c__AnonStorey156::<>m__255(System.Int64)
extern "C"  bool U3CInitializeU3Ec__AnonStorey156_U3CU3Em__255_m2755078007 (U3CInitializeU3Ec__AnonStorey156_t1973587069 * __this, int64_t ____0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TableButtons/<Initialize>c__AnonStorey156::<>m__256(System.Boolean)
extern "C"  void U3CInitializeU3Ec__AnonStorey156_U3CU3Em__256_m3411419153 (U3CInitializeU3Ec__AnonStorey156_t1973587069 * __this, bool ___b0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
