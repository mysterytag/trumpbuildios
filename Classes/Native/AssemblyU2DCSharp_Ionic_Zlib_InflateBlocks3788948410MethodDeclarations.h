﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Ionic.Zlib.InflateBlocks
struct InflateBlocks_t3788948410;
// Ionic.Zlib.ZlibCodec
struct ZlibCodec_t3910383704;
// System.Object
struct Il2CppObject;
// System.Byte[]
struct ByteU5BU5D_t58506160;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Ionic_Zlib_ZlibCodec3910383704.h"
#include "mscorlib_System_Object837106420.h"

// System.Void Ionic.Zlib.InflateBlocks::.ctor(Ionic.Zlib.ZlibCodec,System.Object,System.Int32)
extern "C"  void InflateBlocks__ctor_m2552389464 (InflateBlocks_t3788948410 * __this, ZlibCodec_t3910383704 * ___codec0, Il2CppObject * ___checkfn1, int32_t ___w2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Ionic.Zlib.InflateBlocks::.cctor()
extern "C"  void InflateBlocks__cctor_m2822506728 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 Ionic.Zlib.InflateBlocks::Reset()
extern "C"  uint32_t InflateBlocks_Reset_m3148004695 (InflateBlocks_t3788948410 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Ionic.Zlib.InflateBlocks::Process(System.Int32)
extern "C"  int32_t InflateBlocks_Process_m465382709 (InflateBlocks_t3788948410 * __this, int32_t ___r0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Ionic.Zlib.InflateBlocks::Free()
extern "C"  void InflateBlocks_Free_m87173291 (InflateBlocks_t3788948410 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Ionic.Zlib.InflateBlocks::SetDictionary(System.Byte[],System.Int32,System.Int32)
extern "C"  void InflateBlocks_SetDictionary_m2714673262 (InflateBlocks_t3788948410 * __this, ByteU5BU5D_t58506160* ___d0, int32_t ___start1, int32_t ___n2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Ionic.Zlib.InflateBlocks::SyncPoint()
extern "C"  int32_t InflateBlocks_SyncPoint_m4136553642 (InflateBlocks_t3788948410 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Ionic.Zlib.InflateBlocks::Flush(System.Int32)
extern "C"  int32_t InflateBlocks_Flush_m4103970314 (InflateBlocks_t3788948410 * __this, int32_t ___r0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
