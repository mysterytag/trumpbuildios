﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

// LitJson.WrapperFactory
struct WrapperFactory_t3863042620;
// System.Object
struct Il2CppObject;
// LitJson.IJsonWrapper
struct IJsonWrapper_t3273732007;
// System.IAsyncResult
struct IAsyncResult_t537683269;
// System.AsyncCallback
struct AsyncCallback_t1363551830;
// LitJson.WriterContext
struct WriterContext_t3079472833;
// Localithator
struct Localithator_t3481416136;
// UnityEngine.UI.Text
struct Text_t3286458198;
// UnityEngine.TextAsset
struct TextAsset_t2461560304;
// System.String
struct String_t;
// LocalizedProfitSetter
struct LocalizedProfitSetter_t3003809502;
// UnityEngine.TextMesh
struct TextMesh_t583678247;
// LocalizedSpriteSetter
struct LocalizedSpriteSetter_t2536447647;
// UnityEngine.SpriteRenderer
struct SpriteRenderer_t2223784725;
// LocalizedTextSetter
struct LocalizedTextSetter_t1100117447;
// Localizer
struct Localizer_t799400809;
// LocalNotificationManager
struct LocalNotificationManager_t980457207;
// INotificationManager
struct INotificationManager_t3349952825;
// LocalyticsUnity.Localytics
struct Localytics_t1579915497;
// LocalyticsUnity.Localytics/LocalyticsDidTagEvent
struct LocalyticsDidTagEvent_t1639420300;
// LocalyticsUnity.Localytics/LocalyticsSessionDidOpen
struct LocalyticsSessionDidOpen_t115241990;
// LocalyticsUnity.Localytics/LocalyticsSessionWillClose
struct LocalyticsSessionWillClose_t2690468515;
// LocalyticsUnity.Localytics/LocalyticsSessionWillOpen
struct LocalyticsSessionWillOpen_t2581002303;
// LocalyticsUnity.Localytics/LocalyticsDidDismissInAppMessage
struct LocalyticsDidDismissInAppMessage_t1872466793;
// LocalyticsUnity.Localytics/LocalyticsDidDisplayInAppMessage
struct LocalyticsDidDisplayInAppMessage_t3613222305;
// LocalyticsUnity.Localytics/LocalyticsWillDismissInAppMessage
struct LocalyticsWillDismissInAppMessage_t2580912496;
// LocalyticsUnity.Localytics/LocalyticsWillDisplayInAppMessage
struct LocalyticsWillDisplayInAppMessage_t26700712;
// System.Int64[]
struct Int64U5BU5D_t753178071;
// System.String[]
struct StringU5BU5D_t2956870243;
// System.Collections.Generic.Dictionary`2<System.String,System.String>
struct Dictionary_2_t2606186806;
// LocalyticsUnity.Localytics/ReceiveAnalyticsDelegate
struct ReceiveAnalyticsDelegate_t2804656808;
// LocalyticsUnity.Localytics/ReceiveMessagingDelegate
struct ReceiveMessagingDelegate_t3408128038;
// LocalyticsUnity.MiniJSON
struct MiniJSON_t1258204565;
// System.Collections.IDictionary
struct IDictionary_t1654916945;
// System.Char[]
struct CharU5BU5D_t3416858730;
// System.Collections.IList
struct IList_t1612618265;
// System.Text.StringBuilder
struct StringBuilder_t3822575854;
// System.Collections.Hashtable
struct Hashtable_t3875263730;
// System.Collections.Generic.Dictionary`2<System.String,System.Object>
struct Dictionary_2_t2474804324;
// System.Collections.ArrayList
struct ArrayList_t2121638921;
// System.Collections.Generic.List`1<System.Object>
struct List_1_t1634065389;
// MaterialContainer
struct MaterialContainer_t3323339514;
// MenuEntry
struct MenuEntry_t453027379;
// UnityEngine.Sprite
struct Sprite_t4006040370;
// Message
struct Message_t2619578343;
// System.IDisposable
struct IDisposable_t1628921374;
// UniRx.IObservable`1<System.Boolean>
struct IObservable_1_t4264771001;
// System.Action`1<System.Boolean>
struct Action_1_t359458046;
// UnityEngine.RectTransform
struct RectTransform_t3317474837;
// UniRx.IObservable`1<<>__AnonType3`2<System.Boolean,System.Int32>>
struct IObservable_1_t2854406956;
// UniRx.IObservable`1<System.Int32>
struct IObservable_1_t2606213151;
// System.Func`3<System.Boolean,System.Int32,<>__AnonType3`2<System.Boolean,System.Int32>>
struct Func_3_t3953209855;
// UniRx.IObservable`1<System.Object>
struct IObservable_1_t595904784;
// System.Func`3<System.Boolean,System.Int32,System.Object>
struct Func_3_t1694707683;
// System.Action`1<<>__AnonType3`2<System.Boolean,System.Int32>>
struct Action_1_t3244061297;
// System.Action`1<System.Object>
struct Action_1_t985559125;
// <>__AnonType3`2<System.Boolean,System.Int32>
struct U3CU3E__AnonType3_2_t3095608592;
// Message/<PostInject>c__AnonStorey58
struct U3CPostInjectU3Ec__AnonStorey58_t980490265;
// DG.Tweening.Tweener
struct Tweener_t1766303790;
// DG.Tweening.TweenCallback
struct TweenCallback_t3786476454;
// MetaProcessBase
struct MetaProcessBase_t2426506875;
// System.Collections.Generic.IEnumerable`1<IProcess>
struct IEnumerable_1_t2603424474;
// System.Collections.Generic.List`1<IProcess>
struct List_1_t528229087;
// System.Collections.Generic.IEnumerable`1<System.Object>
struct IEnumerable_1_t3709260776;
// IProcess
struct IProcess_t4026237414;
// System.Func`2<IProcess,System.Boolean>
struct Func_2_t3494101351;
// System.Func`2<System.Object,System.Boolean>
struct Func_2_t1509682273;
// System.Collections.IEnumerator
struct IEnumerator_t287207039;
// MetaProcessBase/<Tick>c__AnonStorey146
struct U3CTickU3Ec__AnonStorey146_t2344266993;
// System.Func`1<System.String>
struct Func_1_t2111270149;
// System.Object[]
struct ObjectU5BU5D_t11523773;
// System.Exception
struct Exception_t1967233988;
// System.Collections.Generic.IEnumerable`1<System.String>
struct IEnumerable_1_t3840643258;
// System.Type
struct Type_t;
// System.Collections.Generic.IEnumerable`1<System.Type>
struct IEnumerable_1_t1356416995;
// System.Func`2<System.Type,System.String>
struct Func_2_t1392394819;
// System.Func`2<System.Object,System.Object>
struct Func_2_t2135783352;
// System.Collections.Generic.IEnumerable`1<System.Reflection.FieldInfo>
struct IEnumerable_1_t4037084138;
// System.Collections.Generic.IEnumerable`1<System.Reflection.PropertyInfo>
struct IEnumerable_1_t67735429;
// System.Collections.Generic.IEnumerable`1<System.Reflection.MethodInfo>
struct IEnumerable_1_t2038408337;
// System.Reflection.ICustomAttributeProvider
struct ICustomAttributeProvider_t2334200065;
// System.Type[]
struct TypeU5BU5D_t3431720054;
// System.Collections.Generic.IEnumerable`1<System.Attribute>
struct IEnumerable_1_t3370848005;
// System.Collections.IEnumerable
struct IEnumerable_t287189635;
// System.Func`2<System.Attribute,System.Boolean>
struct Func_2_t1279580208;
// ModestTree.TypeExtensions/<AllAttributes>c__AnonStorey16D
struct U3CAllAttributesU3Ec__AnonStorey16D_t3765002394;
// System.Attribute
struct Attribute_t498693649;
// ModestTree.TypeExtensions/<GetAllFields>c__Iterator3F
struct U3CGetAllFieldsU3Ec__Iterator3F_t219406118;
// System.Reflection.FieldInfo
struct FieldInfo_t;
// System.Collections.Generic.IEnumerator`1<System.Reflection.FieldInfo>
struct IEnumerator_1_t2648036230;
// ModestTree.TypeExtensions/<GetAllMethods>c__Iterator41
struct U3CGetAllMethodsU3Ec__Iterator41_t3371665531;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.Collections.Generic.IEnumerator`1<System.Reflection.MethodInfo>
struct IEnumerator_1_t649360429;
// ModestTree.TypeExtensions/<GetAllProperties>c__Iterator40
struct U3CGetAllPropertiesU3Ec__Iterator40_t3557131433;
// System.Reflection.PropertyInfo
struct PropertyInfo_t;
// System.Collections.Generic.IEnumerator`1<System.Reflection.PropertyInfo>
struct IEnumerator_1_t2973654817;
// ModestTree.TypeExtensions/<GetParentTypes>c__Iterator3E
struct U3CGetParentTypesU3Ec__Iterator3E_t1503887130;
// System.Collections.Generic.IEnumerator`1<System.Type>
struct IEnumerator_1_t4262336383;
// ModestTree.Util.Debugging.ProfileBlock
struct ProfileBlock_t2731282750;
// UnityEngine.Transform
struct Transform_t284553113;
// System.Collections.Generic.List`1<UnityEngine.GameObject>
struct List_1_t514686775;
// UnityEngine.Transform[]
struct TransformU5BU5D_t3681339876;
// System.Collections.Generic.IEnumerable`1<UnityEngine.Transform>
struct IEnumerable_1_t3156707469;
// System.Func`2<UnityEngine.Transform,System.Boolean>
struct Func_2_t884531080;
// System.Collections.Generic.IEnumerable`1<UnityEngine.GameObject>
struct IEnumerable_1_t2589882162;
// System.Func`2<UnityEngine.Transform,UnityEngine.GameObject>
struct Func_2_t391253545;
// UnityEngine.GameObject
struct GameObject_t4012695102;
// MoneyCounter
struct MoneyCounter_t199543100;
// Counter
struct Counter_t2622483932;
// MoneyDisplay
struct MoneyDisplay_t913476610;
// UniRx.IObservable`1<System.Double>
struct IObservable_1_t293314978;
// System.Func`2<System.Double,UniRx.IObservable`1<System.Double>>
struct Func_2_t2628749132;
// System.Func`3<System.Double,System.Double,System.Double>
struct Func_3_t1933728903;
// System.Action`1<System.Double>
struct Action_1_t682969319;
// MoneyPack
struct MoneyPack_t290305753;
// UnityEngine.SpriteRenderer[]
struct SpriteRendererU5BU5D_t4109544696;
// MoneyPackInfo
struct MoneyPackInfo_t3013049127;
// MoreMoneyWindow
struct MoreMoneyWindow_t918259227;
// MultiplicativeSubstance
struct MultiplicativeSubstance_t581301658;
// System.Collections.Generic.IEnumerable`1<SubstanceBase`2/Intrusion<System.Single,System.Single>>
struct IEnumerable_1_t2951050780;
// System.Func`3<System.Single,SubstanceBase`2/Intrusion<System.Single,System.Single>,System.Single>
struct Func_3_t3383474419;
// System.Func`3<System.Single,System.Object,System.Single>
struct Func_3_t2574913143;
// SubstanceBase`2/Intrusion<System.Single,System.Single>
struct Intrusion_t78896424;
// NewTableCell
struct NewTableCell_t2774679728;
// NewTableTestCell
struct NewTableTestCell_t2791901538;
// NewTableView
struct NewTableView_t2775249395;
// UnityEngine.UI.ScrollRect
struct ScrollRect_t1048578170;
// ITableBlock
struct ITableBlock_t2428542600;
// System.Collections.Generic.IEnumerable`1<ITableBlock>
struct IEnumerable_1_t1005729660;
// System.Func`2<ITableBlock,System.Single>
struct Func_2_t4022284381;
// System.Func`2<System.Object,System.Single>
struct Func_2_t2256885953;
// NoL18n
struct NoL18n_t2337897660;
// OligarchParameters
struct OligarchParameters_t821144443;
// OnceEmptyStream
struct OnceEmptyStream_t3081939404;
// IOrganism
struct IOrganism_t2580843579;
// IRoot
struct IRoot_t69970123;
// OrSubstance
struct OrSubstance_t1381068461;
// System.Collections.Generic.IEnumerable`1<SubstanceBase`2/Intrusion<System.Boolean,System.Boolean>>
struct IEnumerable_1_t494375836;
// System.Func`2<SubstanceBase`2/Intrusion<System.Boolean,System.Boolean>,System.Boolean>
struct Func_2_t2346177373;
// SubstanceBase`2/Intrusion<System.Boolean,System.Boolean>
struct Intrusion_t1917188776;
// System.Func`2<System.String,System.String>
struct Func_2_t917545008;
// System.Func`2<System.String,System.Boolean>
struct Func_2_t160061447;
// System.Collections.Generic.List`1<System.String>
struct List_1_t1765447871;
// PersistentData
struct PersistentData_t2127272705;
// PlaneInfo
struct PlaneInfo_t4110763274;
// PlatformSpriteSetter
struct PlatformSpriteSetter_t3879007191;
// PlayFab.ClientModels.AcceptTradeRequest
struct AcceptTradeRequest_t1814136291;
// PlayFab.ClientModels.AcceptTradeResponse
struct AcceptTradeResponse_t3663823245;
// PlayFab.ClientModels.TradeInfo
struct TradeInfo_t1933812770;
// PlayFab.ClientModels.AddFriendRequest
struct AddFriendRequest_t1861404448;
// PlayFab.ClientModels.AddFriendResult
struct AddFriendResult_t4251613068;
// PlayFab.ClientModels.AddSharedGroupMembersRequest
struct AddSharedGroupMembersRequest_t3205265791;
// PlayFab.ClientModels.AddSharedGroupMembersResult
struct AddSharedGroupMembersResult_t3325132109;
// PlayFab.ClientModels.AddUsernamePasswordRequest
struct AddUsernamePasswordRequest_t1599161869;
// PlayFab.ClientModels.AddUsernamePasswordResult
struct AddUsernamePasswordResult_t1056564991;
// PlayFab.ClientModels.AddUserVirtualCurrencyRequest
struct AddUserVirtualCurrencyRequest_t2803224015;
// PlayFab.ClientModels.AndroidDevicePushNotificationRegistrationRequest
struct AndroidDevicePushNotificationRegistrationRequest_t28777084;
// PlayFab.ClientModels.AndroidDevicePushNotificationRegistrationResult
struct AndroidDevicePushNotificationRegistrationResult_t3915401392;
// PlayFab.ClientModels.AttributeInstallRequest
struct AttributeInstallRequest_t4240476704;
// PlayFab.ClientModels.AttributeInstallResult
struct AttributeInstallResult_t1834505356;
// PlayFab.ClientModels.CancelTradeRequest
struct CancelTradeRequest_t685728853;
// PlayFab.ClientModels.CancelTradeResponse
struct CancelTradeResponse_t3042931035;
// PlayFab.ClientModels.CartItem
struct CartItem_t3543091843;
// System.Collections.Generic.Dictionary`2<System.String,System.UInt32>
struct Dictionary_2_t2623623230;
// PlayFab.ClientModels.CatalogItem
struct CatalogItem_t4132589244;
// PlayFab.ClientModels.CatalogItemConsumableInfo
struct CatalogItemConsumableInfo_t2956282765;
// PlayFab.ClientModels.CatalogItemContainerInfo
struct CatalogItemContainerInfo_t284142995;
// PlayFab.ClientModels.CatalogItemBundleInfo
struct CatalogItemBundleInfo_t1005829164;
// PlayFab.ClientModels.CharacterLeaderboardEntry
struct CharacterLeaderboardEntry_t3117535278;
// PlayFab.ClientModels.CharacterResult
struct CharacterResult_t274624662;
// PlayFab.ClientModels.ConfirmPurchaseRequest
struct ConfirmPurchaseRequest_t1360937886;
// PlayFab.ClientModels.ConfirmPurchaseResult
struct ConfirmPurchaseResult_t633238350;
// System.Collections.Generic.List`1<PlayFab.ClientModels.ItemInstance>
struct List_1_t1322103281;
// PlayFab.ClientModels.ConsumeItemRequest
struct ConsumeItemRequest_t3677119120;
// PlayFab.ClientModels.ConsumeItemResult
struct ConsumeItemResult_t3201805852;
// PlayFab.ClientModels.ConsumePSNEntitlementsRequest
struct ConsumePSNEntitlementsRequest_t3123362058;
// PlayFab.ClientModels.ConsumePSNEntitlementsResult
struct ConsumePSNEntitlementsResult_t4292321378;
// PlayFab.ClientModels.CreateSharedGroupRequest
struct CreateSharedGroupRequest_t972582497;
// PlayFab.ClientModels.CreateSharedGroupResult
struct CreateSharedGroupResult_t2976015403;
// PlayFab.ClientModels.CurrentGamesRequest
struct CurrentGamesRequest_t3387974807;
// PlayFab.ClientModels.CurrentGamesResult
struct CurrentGamesResult_t4162309941;
// System.Collections.Generic.List`1<PlayFab.ClientModels.GameInfo>
struct List_1_t2564353257;

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array2840145358.h"
#include "AssemblyU2DCSharp_LitJson_PropertyMetadata3942474089.h"
#include "AssemblyU2DCSharp_LitJson_PropertyMetadata3942474089MethodDeclarations.h"
#include "AssemblyU2DCSharp_LitJson_WrapperFactory3863042620.h"
#include "AssemblyU2DCSharp_LitJson_WrapperFactory3863042620MethodDeclarations.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_IntPtr676692020.h"
#include "mscorlib_System_Void2779279689.h"
#include "mscorlib_System_AsyncCallback1363551830.h"
#include "AssemblyU2DCSharp_LitJson_WriterContext3079472833.h"
#include "AssemblyU2DCSharp_LitJson_WriterContext3079472833MethodDeclarations.h"
#include "mscorlib_System_Object837106420MethodDeclarations.h"
#include "AssemblyU2DCSharp_Localithator3481416136.h"
#include "AssemblyU2DCSharp_Localithator3481416136MethodDeclarations.h"
#include "UnityEngine_UnityEngine_MonoBehaviour3012272455MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Component2126946602MethodDeclarations.h"
#include "AssemblyU2DCSharp_LocalizationManager1078246068MethodDeclarations.h"
#include "UnityEngine_UI_UnityEngine_UI_Text3286458198.h"
#include "UnityEngine_UnityEngine_Component2126946602.h"
#include "mscorlib_System_String968488902.h"
#include "UnityEngine_UI_UnityEngine_UI_Text3286458198MethodDeclarations.h"
#include "AssemblyU2DCSharp_LocalizationManager1078246068.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g2606186806MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Application450040189MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Resources1543782994MethodDeclarations.h"
#include "System_Xml_System_Xml_XmlDocument3705263098MethodDeclarations.h"
#include "UnityEngine_UnityEngine_TextAsset2461560304MethodDeclarations.h"
#include "System_Xml_System_Xml_XmlNode3592213601MethodDeclarations.h"
#include "mscorlib_System_String968488902MethodDeclarations.h"
#include "UnityEngine_UnityEngine_TextAsset2461560304.h"
#include "System_Xml_System_Xml_XmlDocument3705263098.h"
#include "System_Xml_System_Xml_XmlNode3592213601.h"
#include "UnityEngine_UnityEngine_SystemLanguage1674836756.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g2606186806.h"
#include "UnityEngine_UnityEngine_Resources1543782994.h"
#include "System_Xml_System_Xml_XmlNodeList3966370975.h"
#include "System_Xml_System_Xml_XmlNodeList3966370975MethodDeclarations.h"
#include "mscorlib_System_Int322847414787.h"
#include "System_Xml_System_Xml_XmlAttributeCollection571717291.h"
#include "System_Xml_System_Xml_XmlNamedNodeMap1329997280MethodDeclarations.h"
#include "System_Xml_System_Xml_XmlNamedNodeMap1329997280.h"
#include "mscorlib_System_Boolean211005341.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_ge190145395MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_ge190145395.h"
#include "UnityEngine_UnityEngine_Debug1588791936MethodDeclarations.h"
#include "AssemblyU2DCSharp_LocalizedProfitSetter3003809502.h"
#include "AssemblyU2DCSharp_LocalizedProfitSetter3003809502MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Object3878351788MethodDeclarations.h"
#include "UnityEngine_UnityEngine_TextMesh583678247MethodDeclarations.h"
#include "UnityEngine_UnityEngine_TextMesh583678247.h"
#include "UnityEngine_UnityEngine_Object3878351788.h"
#include "AssemblyU2DCSharp_LocalizedSpriteSetter2536447647.h"
#include "AssemblyU2DCSharp_LocalizedSpriteSetter2536447647MethodDeclarations.h"
#include "UnityEngine_UnityEngine_SpriteRenderer2223784725MethodDeclarations.h"
#include "UnityEngine_UnityEngine_SpriteRenderer2223784725.h"
#include "UnityEngine_UnityEngine_Sprite4006040370.h"
#include "AssemblyU2DCSharp_LocalizedTextSetter1100117447.h"
#include "AssemblyU2DCSharp_LocalizedTextSetter1100117447MethodDeclarations.h"
#include "AssemblyU2DCSharp_Utils82059409MethodDeclarations.h"
#include "AssemblyU2DCSharp_Localizer799400809.h"
#include "AssemblyU2DCSharp_Localizer799400809MethodDeclarations.h"
#include "AssemblyU2DCSharp_LocalNotificationManager980457207.h"
#include "AssemblyU2DCSharp_LocalNotificationManager980457207MethodDeclarations.h"
#include "AssemblyU2DCSharp_IOSNotificationManager324309781MethodDeclarations.h"
#include "AssemblyU2DCSharp_IOSNotificationManager324309781.h"
#include "AssemblyU2DCSharp_LocalyticsUnity_Localytics1579915497.h"
#include "AssemblyU2DCSharp_LocalyticsUnity_Localytics1579915497MethodDeclarations.h"
#include "AssemblyU2DCSharp_LocalyticsUnity_Localytics_Local1639420300.h"
#include "mscorlib_System_Delegate3660574010MethodDeclarations.h"
#include "mscorlib_System_Delegate3660574010.h"
#include "AssemblyU2DCSharp_LocalyticsUnity_Localytics_Localy115241990.h"
#include "AssemblyU2DCSharp_LocalyticsUnity_Localytics_Local2690468515.h"
#include "AssemblyU2DCSharp_LocalyticsUnity_Localytics_Local2581002303.h"
#include "AssemblyU2DCSharp_LocalyticsUnity_Localytics_Local1872466793.h"
#include "AssemblyU2DCSharp_LocalyticsUnity_Localytics_Local3613222305.h"
#include "AssemblyU2DCSharp_LocalyticsUnity_Localytics_Local2580912496.h"
#include "AssemblyU2DCSharp_LocalyticsUnity_Localytics_Localyt26700712.h"
#include "AssemblyU2DCSharp_LocalyticsUnity_Localytics_InApp1063031078.h"
#include "mscorlib_System_UInt32985925326.h"
#include "mscorlib_System_Double534516614.h"
#include "AssemblyU2DCSharp_LocalyticsUnity_Localytics_Profi1376631371.h"
#include "mscorlib_ArrayTypes.h"
#include "mscorlib_System_Int642847414882.h"
#include "AssemblyU2DCSharp_LocalyticsUnity_MiniJSON1258204565MethodDeclarations.h"
#include "AssemblyU2DCSharp_LocalyticsUnity_Localytics_Recei2804656808MethodDeclarations.h"
#include "AssemblyU2DCSharp_LocalyticsUnity_Localytics_Recei2804656808.h"
#include "AssemblyU2DCSharp_LocalyticsUnity_Localytics_Recei3408128038MethodDeclarations.h"
#include "AssemblyU2DCSharp_LocalyticsUnity_Localytics_Recei3408128038.h"
#include "UnityEngine_UnityEngine_LocationInfo1628589680.h"
#include "UnityEngine_UnityEngine_LocationInfo1628589680MethodDeclarations.h"
#include "mscorlib_System_Single958209021.h"
#include "mscorlib_System_Boolean211005341MethodDeclarations.h"
#include "AssemblyU2DCSharp_LocalyticsUnity_Localytics_Local2581002303MethodDeclarations.h"
#include "AssemblyU2DCSharp_LocalyticsUnity_Localytics_Localy115241990MethodDeclarations.h"
#include "mscorlib_System_Int642847414882MethodDeclarations.h"
#include "AssemblyU2DCSharp_LocalyticsUnity_Localytics_Local1639420300MethodDeclarations.h"
#include "AssemblyU2DCSharp_LocalyticsUnity_Localytics_Local2690468515MethodDeclarations.h"
#include "mscorlib_System_Collections_Hashtable3875263730.h"
#include "mscorlib_System_Exception1967233988.h"
#include "mscorlib_System_Collections_Hashtable3875263730MethodDeclarations.h"
#include "mscorlib_System_Exception1967233988MethodDeclarations.h"
#include "AssemblyU2DCSharp_LocalyticsUnity_Localytics_Localyt26700712MethodDeclarations.h"
#include "AssemblyU2DCSharp_LocalyticsUnity_Localytics_Local3613222305MethodDeclarations.h"
#include "AssemblyU2DCSharp_LocalyticsUnity_Localytics_Local2580912496MethodDeclarations.h"
#include "AssemblyU2DCSharp_LocalyticsUnity_Localytics_Local1872466793MethodDeclarations.h"
#include "AssemblyU2DCSharp_LocalyticsUnity_Localytics_InApp1063031078MethodDeclarations.h"
#include "AssemblyU2DCSharp_LocalyticsUnity_Localytics_Profi1376631371MethodDeclarations.h"
#include "AssemblyU2DCSharp_LocalyticsUnity_MiniJSON1258204565.h"
#include "mscorlib_System_Char2778706699.h"
#include "mscorlib_System_Text_StringBuilder3822575854MethodDeclarations.h"
#include "mscorlib_System_Text_StringBuilder3822575854.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g2474804324MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g2474804324.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1634065389MethodDeclarations.h"
#include "mscorlib_System_Collections_ArrayList2121638921MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1634065389.h"
#include "mscorlib_System_Collections_ArrayList2121638921.h"
#include "mscorlib_System_Array2840145358MethodDeclarations.h"
#include "mscorlib_System_UInt32985925326MethodDeclarations.h"
#include "mscorlib_System_Char2778706699MethodDeclarations.h"
#include "mscorlib_System_Globalization_NumberStyles3988678145.h"
#include "mscorlib_System_Double534516614MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E2373214747MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_22094718104MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_22094718104.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E2373214747.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E2241832265MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_21963335622MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_21963335622.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E2241832265.h"
#include "mscorlib_System_Convert1097883944MethodDeclarations.h"
#include "mscorlib_System_DateTime339033936MethodDeclarations.h"
#include "mscorlib_System_TimeSpan763862892MethodDeclarations.h"
#include "mscorlib_System_Type2779229935.h"
#include "mscorlib_System_DateTime339033936.h"
#include "mscorlib_System_TimeSpan763862892.h"
#include "mscorlib_System_Type2779229935MethodDeclarations.h"
#include "mscorlib_System_Decimal1688557254.h"
#include "mscorlib_System_DateTimeKind3550648708.h"
#include "AssemblyU2DCSharp_LocalyticsUnity_MiniJsonExtensio1605058345.h"
#include "AssemblyU2DCSharp_LocalyticsUnity_MiniJsonExtensio1605058345MethodDeclarations.h"
#include "AssemblyU2DCSharp_MaterialContainer3323339514.h"
#include "AssemblyU2DCSharp_MaterialContainer3323339514MethodDeclarations.h"
#include "AssemblyU2DCSharp_MenuEntry453027379.h"
#include "AssemblyU2DCSharp_MenuEntry453027379MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Transform284553113MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Transform284553113.h"
#include "AssemblyU2DCSharp_Message2619578343.h"
#include "AssemblyU2DCSharp_Message2619578343MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_ReactiveCollec3392720430MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_BoolReactivePr3047538250MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_ReactiveCollec3392720430.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_BoolReactivePr3047538250.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Tuple_2_gen2188574943MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Tuple_2_gen2188574943.h"
#include "mscorlib_System_Collections_ObjectModel_Collection4157562673MethodDeclarations.h"
#include "mscorlib_System_Collections_ObjectModel_Collection4157562673.h"
#include "AssemblyU2DCSharp_Message_U3CPostInjectU3Ec__AnonSt980490265MethodDeclarations.h"
#include "mscorlib_System_Action_1_gen359458046MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_ObservableExte3095004425MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Color1588175760MethodDeclarations.h"
#include "UnityEngine_UI_UnityEngine_UI_Graphic933884113MethodDeclarations.h"
#include "UnityEngine_UnityEngine_GameObject4012695102MethodDeclarations.h"
#include "System_Core_System_Func_3_gen3953209855MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Observable258071701MethodDeclarations.h"
#include "mscorlib_System_Action_1_gen3244061297MethodDeclarations.h"
#include "AssemblyU2DCSharp_Message_U3CPostInjectU3Ec__AnonSt980490265.h"
#include "mscorlib_System_Action_1_gen359458046.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_ObservableExte3095004425.h"
#include "UnityEngine_UI_UnityEngine_UI_Image3354615620.h"
#include "UnityEngine_UnityEngine_RectTransform3317474837.h"
#include "UnityEngine_UnityEngine_Color1588175760.h"
#include "UnityEngine_UnityEngine_GameObject4012695102.h"
#include "System_Core_System_Func_3_gen3953209855.h"
#include "AssemblyU2DCSharp_U3CU3E__AnonType3_2_gen3095608592.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Observable258071701.h"
#include "mscorlib_System_Action_1_gen3244061297.h"
#include "UnityEngine_UnityEngine_RectTransform3317474837MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Vector23525329788.h"
#include "mscorlib_System_Int322847414787MethodDeclarations.h"
#include "AssemblyU2DCSharp_U3CU3E__AnonType3_2_gen3095608592MethodDeclarations.h"
#include "DOTween_DG_Tweening_ShortcutExtensions723807504MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Behaviour3120504042MethodDeclarations.h"
#include "DOTween46_DG_Tweening_ShortcutExtensions462084082962MethodDeclarations.h"
#include "DOTween_DG_Tweening_TweenCallback3786476454MethodDeclarations.h"
#include "DOTween_DG_Tweening_TweenSettingsExtensions2047694392MethodDeclarations.h"
#include "DOTween_DG_Tweening_Tweener1766303790.h"
#include "DOTween_DG_Tweening_TweenCallback3786476454.h"
#include "DOTween_DG_Tweening_TweenSettingsExtensions2047694392.h"
#include "UnityEngine_UI_UnityEngine_UI_Image3354615620MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_ReactivePropert819338379MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_ReactivePropert819338379.h"
#include "AssemblyU2DCSharp_MetaProcessBase2426506875.h"
#include "AssemblyU2DCSharp_MetaProcessBase2426506875MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen528229087MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen528229087.h"
#include "System_Core_System_Linq_Enumerable4285654829MethodDeclarations.h"
#include "System_Core_System_Linq_Enumerable4285654829.h"
#include "System_Core_System_Func_2_gen3494101351MethodDeclarations.h"
#include "System_Core_System_Func_2_gen3494101351.h"
#include "AssemblyU2DCSharp_MetaProcessBase_U3CTickU3Ec__Ano2344266993MethodDeclarations.h"
#include "mscorlib_System_Action_1_gen4174690119MethodDeclarations.h"
#include "AssemblyU2DCSharp_MetaProcessBase_U3CTickU3Ec__Ano2344266993.h"
#include "mscorlib_System_Action_1_gen4174690119.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera2908979375.h"
#include "AssemblyU2DCSharp_ModestTree_Assert1161478012.h"
#include "AssemblyU2DCSharp_ModestTree_Assert1161478012MethodDeclarations.h"
#include "System_Core_System_Func_1_gen2111270149.h"
#include "System_Core_System_Func_1_gen2111270149MethodDeclarations.h"
#include "AssemblyU2DCSharp_Zenject_ZenjectException523236501MethodDeclarations.h"
#include "AssemblyU2DCSharp_Zenject_ZenjectException523236501.h"
#include "AssemblyU2DCSharp_ModestTree_LinqExtensions3281724522.h"
#include "AssemblyU2DCSharp_ModestTree_LinqExtensions3281724522MethodDeclarations.h"
#include "AssemblyU2DCSharp_ModestTree_Log3485895482.h"
#include "AssemblyU2DCSharp_ModestTree_Log3485895482MethodDeclarations.h"
#include "AssemblyU2DCSharp_ModestTree_MiscExtensions2809094742.h"
#include "AssemblyU2DCSharp_ModestTree_MiscExtensions2809094742MethodDeclarations.h"
#include "AssemblyU2DCSharp_ModestTree_TypeExtensions3479487268.h"
#include "AssemblyU2DCSharp_ModestTree_TypeExtensions3479487268MethodDeclarations.h"
#include "mscorlib_System_Activator690001546MethodDeclarations.h"
#include "mscorlib_System_Reflection_MemberInfo2843033814MethodDeclarations.h"
#include "mscorlib_System_Reflection_MemberInfo2843033814.h"
#include "AssemblyU2DCSharp_ModestTree_TypeExtensions_U3CGet1503887130MethodDeclarations.h"
#include "AssemblyU2DCSharp_ModestTree_TypeExtensions_U3CGet1503887130.h"
#include "System_Core_System_Func_2_gen1392394819MethodDeclarations.h"
#include "System_Core_System_Func_2_gen1392394819.h"
#include "mscorlib_System_Reflection_BindingFlags2090192240.h"
#include "AssemblyU2DCSharp_ModestTree_TypeExtensions_U3CGetA219406118MethodDeclarations.h"
#include "AssemblyU2DCSharp_ModestTree_TypeExtensions_U3CGetA219406118.h"
#include "AssemblyU2DCSharp_ModestTree_TypeExtensions_U3CGet3557131433MethodDeclarations.h"
#include "AssemblyU2DCSharp_ModestTree_TypeExtensions_U3CGet3557131433.h"
#include "AssemblyU2DCSharp_ModestTree_TypeExtensions_U3CGet3371665531MethodDeclarations.h"
#include "AssemblyU2DCSharp_ModestTree_TypeExtensions_U3CGet3371665531.h"
#include "mscorlib_System_RuntimeTypeHandle1864875887.h"
#include "AssemblyU2DCSharp_ModestTree_TypeExtensions_U3CAll3765002394MethodDeclarations.h"
#include "System_Core_System_Func_2_gen1279580208MethodDeclarations.h"
#include "AssemblyU2DCSharp_ModestTree_TypeExtensions_U3CAll3765002394.h"
#include "mscorlib_System_Attribute498693649.h"
#include "System_Core_System_Func_2_gen1279580208.h"
#include "mscorlib_System_Reflection_FieldInfo1164929782.h"
#include "mscorlib_System_Threading_Interlocked3007803305MethodDeclarations.h"
#include "mscorlib_System_NotSupportedException1374155497MethodDeclarations.h"
#include "mscorlib_System_NotSupportedException1374155497.h"
#include "mscorlib_System_Reflection_MethodInfo3461221277.h"
#include "mscorlib_System_Reflection_PropertyInfo1490548369.h"
#include "AssemblyU2DCSharp_ModestTree_Util_Debugging_Profil2731282750.h"
#include "AssemblyU2DCSharp_ModestTree_Util_Debugging_Profil2731282750MethodDeclarations.h"
#include "AssemblyU2DCSharp_ModestTree_Util_MouseWheelScroll2927108753.h"
#include "AssemblyU2DCSharp_ModestTree_Util_MouseWheelScroll2927108753MethodDeclarations.h"
#include "AssemblyU2DCSharp_ModestTree_Util_ReflectionUtil1047232519.h"
#include "AssemblyU2DCSharp_ModestTree_Util_ReflectionUtil1047232519MethodDeclarations.h"
#include "AssemblyU2DCSharp_ModestTree_Util_Tuple2948270274.h"
#include "AssemblyU2DCSharp_ModestTree_Util_Tuple2948270274MethodDeclarations.h"
#include "AssemblyU2DCSharp_ModestTree_Util_UnityUtil126913041.h"
#include "AssemblyU2DCSharp_ModestTree_Util_UnityUtil126913041MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Input1593691127MethodDeclarations.h"
#include "UnityEngine_UnityEngine_KeyCode2371581209.h"
#include "UnityEngine_UnityEngine_Mathf1597001355MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen514686775.h"
#include "System_Core_System_Func_2_gen884531080MethodDeclarations.h"
#include "System_Core_System_Func_2_gen391253545MethodDeclarations.h"
#include "UnityEngine_ArrayTypes.h"
#include "System_Core_System_Func_2_gen884531080.h"
#include "System_Core_System_Func_2_gen391253545.h"
#include "UnityEngine_UnityEngine_Quaternion1891715979MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Quaternion1891715979.h"
#include "AssemblyU2DCSharp_MoneyCounter199543100.h"
#include "AssemblyU2DCSharp_MoneyCounter199543100MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen3419442901MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen3419442901.h"
#include "UnityEngine_UnityEngine_Vector33525329789MethodDeclarations.h"
#include "AssemblyU2DCSharp_Counter2622483932.h"
#include "UnityEngine_UnityEngine_Vector33525329789.h"
#include "AssemblyU2DCSharp_Counter2622483932MethodDeclarations.h"
#include "AssemblyU2DCSharp_GlobalStat1134678199.h"
#include "mscorlib_System_Math2778998461MethodDeclarations.h"
#include "AssemblyU2DCSharp_MoneyDisplay913476610.h"
#include "AssemblyU2DCSharp_MoneyDisplay913476610MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_ReactiveProper1142849652MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_ReactiveProper1142849652.h"
#include "System_Core_System_Func_2_gen2628749132MethodDeclarations.h"
#include "System_Core_System_Func_3_gen1933728903MethodDeclarations.h"
#include "mscorlib_System_Action_1_gen682969319MethodDeclarations.h"
#include "System_Core_System_Func_2_gen2628749132.h"
#include "System_Core_System_Func_3_gen1933728903.h"
#include "mscorlib_System_Action_1_gen682969319.h"
#include "AssemblyU2DCSharp_GlobalStat1134678199MethodDeclarations.h"
#include "AssemblyU2DCSharp_GameController2782302542.h"
#include "AssemblyU2DCSharp_OligarchParameters821144443.h"
#include "AssemblyU2DCSharp_MoneyPack290305753.h"
#include "AssemblyU2DCSharp_MoneyPack290305753MethodDeclarations.h"
#include "AssemblyU2DCSharp_MoneyPackInfo3013049127.h"
#include "AssemblyU2DCSharp_MoneyPackInfo3013049127MethodDeclarations.h"
#include "AssemblyU2DCSharp_MoreMoneyWindow918259227.h"
#include "AssemblyU2DCSharp_MoreMoneyWindow918259227MethodDeclarations.h"
#include "AssemblyU2DCSharp_WindowBase3855465217MethodDeclarations.h"
#include "AssemblyU2DCSharp_BarygaOpenIABManager3621535309MethodDeclarations.h"
#include "AssemblyU2DCSharp_IconsProvider3884200715MethodDeclarations.h"
#include "AssemblyU2DCSharp_IconsProvider3884200715.h"
#include "AssemblyU2DCSharp_TableButtons1868573683MethodDeclarations.h"
#include "AssemblyU2DCSharp_WindowManager3316821373MethodDeclarations.h"
#include "AssemblyU2DCSharp_TableButtons1868573683.h"
#include "AssemblyU2DCSharp_WindowBase3855465217.h"
#include "AssemblyU2DCSharp_WindowManager3316821373.h"
#include "AssemblyU2DCSharp_MultiplicativeSubstance581301658.h"
#include "AssemblyU2DCSharp_MultiplicativeSubstance581301658MethodDeclarations.h"
#include "AssemblyU2DCSharp_SubstanceBase_2_gen1869245964MethodDeclarations.h"
#include "System_Core_System_Func_3_gen3383474419MethodDeclarations.h"
#include "AssemblyU2DCSharp_SubstanceBase_2_gen1869245964.h"
#include "mscorlib_System_Collections_Generic_List_1_gen875855393.h"
#include "System_Core_System_Func_3_gen3383474419.h"
#include "AssemblyU2DCSharp_SubstanceBase_2_Intrusion_gen78896424.h"
#include "AssemblyU2DCSharp_NewTableCell2774679728.h"
#include "AssemblyU2DCSharp_NewTableCell2774679728MethodDeclarations.h"
#include "AssemblyU2DCSharp_InstantiatableInPool1882737686MethodDeclarations.h"
#include "AssemblyU2DCSharp_NewTableTestCell2791901538.h"
#include "AssemblyU2DCSharp_NewTableTestCell2791901538MethodDeclarations.h"
#include "AssemblyU2DCSharp_NewTableView2775249395.h"
#include "AssemblyU2DCSharp_NewTableView2775249395MethodDeclarations.h"
#include "AssemblyU2DCSharp_Cell_1_gen1140306653MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen3225501569MethodDeclarations.h"
#include "AssemblyU2DCSharp_Cell_1_gen1140306653.h"
#include "mscorlib_System_Collections_Generic_List_1_gen3225501569.h"
#include "UnityEngine_UI_UnityEngine_UI_ScrollRect1048578170.h"
#include "UnityEngine_UI_UnityEngine_UI_ScrollRect1048578170MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen3505791693MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Events_UnityEvent_1_gen2467784599MethodDeclarations.h"
#include "UnityEngine_UI_UnityEngine_UI_ScrollRect_ScrollRec2675965609.h"
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen3505791693.h"
#include "System_Core_System_Func_2_gen4022284381MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Rect1525428817MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera1311284561MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera1311284561.h"
#include "UnityEngine_UnityEngine_Rect1525428817.h"
#include "System_Core_System_Func_2_gen4022284381.h"
#include "AssemblyU2DCSharp_NoL18n2337897660.h"
#include "AssemblyU2DCSharp_NoL18n2337897660MethodDeclarations.h"
#include "AssemblyU2DCSharp_OligarchParameters821144443MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Screen3994030297MethodDeclarations.h"
#include "AssemblyU2DCSharp_OligarchType2745121163.h"
#include "AssemblyU2DCSharp_OligarchType2745121163MethodDeclarations.h"
#include "AssemblyU2DCSharp_OnceEmptyStream3081939404.h"
#include "AssemblyU2DCSharp_OnceEmptyStream3081939404MethodDeclarations.h"
#include "AssemblyU2DCSharp_EmptyStream2850978573MethodDeclarations.h"
#include "AssemblyU2DCSharp_Stream_1_gen1249425060.h"
#include "AssemblyU2DCSharp_OrganismExtension4039886701.h"
#include "AssemblyU2DCSharp_OrganismExtension4039886701MethodDeclarations.h"
#include "AssemblyU2DCSharp_OrganismWorkReactives4236551351.h"
#include "AssemblyU2DCSharp_OrganismWorkReactives4236551351MethodDeclarations.h"
#include "AssemblyU2DCSharp_OrSubstance1381068461.h"
#include "AssemblyU2DCSharp_OrSubstance1381068461MethodDeclarations.h"
#include "AssemblyU2DCSharp_SubstanceBase_2_gen3707538316MethodDeclarations.h"
#include "System_Core_System_Func_2_gen2346177373MethodDeclarations.h"
#include "AssemblyU2DCSharp_SubstanceBase_2_gen3707538316.h"
#include "mscorlib_System_Collections_Generic_List_1_gen2714147745.h"
#include "System_Core_System_Func_2_gen2346177373.h"
#include "AssemblyU2DCSharp_SubstanceBase_2_Intrusion_gen1917188776.h"
#include "AssemblyU2DCSharp_Params2383406502.h"
#include "AssemblyU2DCSharp_Params2383406502MethodDeclarations.h"
#include "System_Core_System_Func_2_gen917545008MethodDeclarations.h"
#include "System_Core_System_Func_2_gen160061447MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1765447871MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera4146198159MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1765447871.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera4146198159.h"
#include "System_Core_System_Func_2_gen917545008.h"
#include "System_Core_System_Func_2_gen160061447.h"
#include "AssemblyU2DCSharp_PersistentData2127272705.h"
#include "AssemblyU2DCSharp_PersistentData2127272705MethodDeclarations.h"
#include "AssemblyU2DCSharp_PlaneInfo4110763274.h"
#include "AssemblyU2DCSharp_PlaneInfo4110763274MethodDeclarations.h"
#include "AssemblyU2DCSharp_Platform1939328147.h"
#include "AssemblyU2DCSharp_Platform1939328147MethodDeclarations.h"
#include "UnityEngine_UnityEngine_RuntimePlatform1574985880.h"
#include "AssemblyU2DCSharp_PlatformSpriteSetter3879007191.h"
#include "AssemblyU2DCSharp_PlatformSpriteSetter3879007191MethodDeclarations.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_AcceptTrade1814136291.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_AcceptTrade1814136291MethodDeclarations.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_AcceptTrade3663823245.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_AcceptTrade3663823245MethodDeclarations.h"
#include "AssemblyU2DCSharp_PlayFab_Internal_PlayFabResultCom379512675MethodDeclarations.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_TradeInfo1933812770.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_AddFriendRe1861404448.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_AddFriendRe1861404448MethodDeclarations.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_AddFriendRe4251613068.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_AddFriendRe4251613068MethodDeclarations.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_AddSharedGr3205265791.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_AddSharedGr3205265791MethodDeclarations.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_AddSharedGr3325132109.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_AddSharedGr3325132109MethodDeclarations.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_AddUsername1599161869.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_AddUsername1599161869MethodDeclarations.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_AddUsername1056564991.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_AddUsername1056564991MethodDeclarations.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_AddUserVirt2803224015.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_AddUserVirt2803224015MethodDeclarations.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_AndroidDevice28777084.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_AndroidDevice28777084MethodDeclarations.h"
#include "mscorlib_System_Nullable_1_gen3097043249.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_AndroidDevi3915401392.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_AndroidDevi3915401392MethodDeclarations.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_AttributeIn4240476704.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_AttributeIn4240476704MethodDeclarations.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_AttributeIn1834505356.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_AttributeIn1834505356MethodDeclarations.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_CancelTradeR685728853.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_CancelTradeR685728853MethodDeclarations.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_CancelTrade3042931035.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_CancelTrade3042931035MethodDeclarations.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_CartItem3543091843.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_CartItem3543091843MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g2623623230.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_CatalogItem4132589244.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_CatalogItem4132589244MethodDeclarations.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_CatalogItem2956282765.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_CatalogItemC284142995.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_CatalogItem1005829164.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_CatalogItem1005829164MethodDeclarations.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_CatalogItem2956282765MethodDeclarations.h"
#include "mscorlib_System_Nullable_1_gen3871963234.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_CatalogItemC284142995MethodDeclarations.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_CharacterLe3117535278.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_CharacterLe3117535278MethodDeclarations.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_CharacterRes274624662.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_CharacterRes274624662MethodDeclarations.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_ConfirmPurc1360937886.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_ConfirmPurc1360937886MethodDeclarations.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_ConfirmPurch633238350.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_ConfirmPurch633238350MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1322103281.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_ConsumeItem3677119120.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_ConsumeItem3677119120MethodDeclarations.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_ConsumeItem3201805852.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_ConsumeItem3201805852MethodDeclarations.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_ConsumePSNE3123362058.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_ConsumePSNE3123362058MethodDeclarations.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_ConsumePSNE4292321378.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_ConsumePSNE4292321378MethodDeclarations.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_CreateShared972582497.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_CreateShared972582497MethodDeclarations.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_CreateShare2976015403.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_CreateShare2976015403MethodDeclarations.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_Currency4112277569.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_Currency4112277569MethodDeclarations.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_CurrentGame3387974807.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_CurrentGame3387974807MethodDeclarations.h"
#include "mscorlib_System_Nullable_1_gen212373688.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_CurrentGame4162309941.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_CurrentGame4162309941MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen2564353257.h"

// !!0 UnityEngine.Component::GetComponent<System.Object>()
extern "C"  Il2CppObject * Component_GetComponent_TisIl2CppObject_m267839954_gshared (Component_t2126946602 * __this, const MethodInfo* method);
#define Component_GetComponent_TisIl2CppObject_m267839954(__this, method) ((  Il2CppObject * (*) (Component_t2126946602 *, const MethodInfo*))Component_GetComponent_TisIl2CppObject_m267839954_gshared)(__this, method)
// !!0 UnityEngine.Component::GetComponent<UnityEngine.UI.Text>()
#define Component_GetComponent_TisText_t3286458198_m1610753993(__this, method) ((  Text_t3286458198 * (*) (Component_t2126946602 *, const MethodInfo*))Component_GetComponent_TisIl2CppObject_m267839954_gshared)(__this, method)
// !!0 UnityEngine.Resources::Load<System.Object>(System.String)
extern "C"  Il2CppObject * Resources_Load_TisIl2CppObject_m2208345422_gshared (Il2CppObject * __this /* static, unused */, String_t* p0, const MethodInfo* method);
#define Resources_Load_TisIl2CppObject_m2208345422(__this /* static, unused */, p0, method) ((  Il2CppObject * (*) (Il2CppObject * /* static, unused */, String_t*, const MethodInfo*))Resources_Load_TisIl2CppObject_m2208345422_gshared)(__this /* static, unused */, p0, method)
// !!0 UnityEngine.Resources::Load<UnityEngine.TextAsset>(System.String)
#define Resources_Load_TisTextAsset_t2461560304_m585700060(__this /* static, unused */, p0, method) ((  TextAsset_t2461560304 * (*) (Il2CppObject * /* static, unused */, String_t*, const MethodInfo*))Resources_Load_TisIl2CppObject_m2208345422_gshared)(__this /* static, unused */, p0, method)
// !!0 UnityEngine.Component::GetComponent<UnityEngine.TextMesh>()
#define Component_GetComponent_TisTextMesh_t583678247_m2237128948(__this, method) ((  TextMesh_t583678247 * (*) (Component_t2126946602 *, const MethodInfo*))Component_GetComponent_TisIl2CppObject_m267839954_gshared)(__this, method)
// !!0 UnityEngine.Component::GetComponent<UnityEngine.SpriteRenderer>()
#define Component_GetComponent_TisSpriteRenderer_t2223784725_m4090179846(__this, method) ((  SpriteRenderer_t2223784725 * (*) (Component_t2126946602 *, const MethodInfo*))Component_GetComponent_TisIl2CppObject_m267839954_gshared)(__this, method)
// !!0 UnityEngine.Object::FindObjectOfType<System.Object>()
extern "C"  Il2CppObject * Object_FindObjectOfType_TisIl2CppObject_m1765599783_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define Object_FindObjectOfType_TisIl2CppObject_m1765599783(__this /* static, unused */, method) ((  Il2CppObject * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))Object_FindObjectOfType_TisIl2CppObject_m1765599783_gshared)(__this /* static, unused */, method)
// !!0 UnityEngine.Object::FindObjectOfType<MaterialContainer>()
#define Object_FindObjectOfType_TisMaterialContainer_t3323339514_m169546711(__this /* static, unused */, method) ((  MaterialContainer_t3323339514 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))Object_FindObjectOfType_TisIl2CppObject_m1765599783_gshared)(__this /* static, unused */, method)
// System.IDisposable UniRx.ObservableExtensions::Subscribe<System.Boolean>(UniRx.IObservable`1<!!0>,System.Action`1<!!0>)
extern "C"  Il2CppObject * ObservableExtensions_Subscribe_TisBoolean_t211005341_m2888450145_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* p0, Action_1_t359458046 * p1, const MethodInfo* method);
#define ObservableExtensions_Subscribe_TisBoolean_t211005341_m2888450145(__this /* static, unused */, p0, p1, method) ((  Il2CppObject * (*) (Il2CppObject * /* static, unused */, Il2CppObject*, Action_1_t359458046 *, const MethodInfo*))ObservableExtensions_Subscribe_TisBoolean_t211005341_m2888450145_gshared)(__this /* static, unused */, p0, p1, method)
// !!0 UnityEngine.Component::GetComponent<UnityEngine.RectTransform>()
#define Component_GetComponent_TisRectTransform_t3317474837_m1940403147(__this, method) ((  RectTransform_t3317474837 * (*) (Component_t2126946602 *, const MethodInfo*))Component_GetComponent_TisIl2CppObject_m267839954_gshared)(__this, method)
// UniRx.IObservable`1<!!2> UniRx.Observable::CombineLatest<System.Boolean,System.Int32,System.Object>(UniRx.IObservable`1<!!0>,UniRx.IObservable`1<!!1>,System.Func`3<!!0,!!1,!!2>)
extern "C"  Il2CppObject* Observable_CombineLatest_TisBoolean_t211005341_TisInt32_t2847414787_TisIl2CppObject_m374310532_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* p0, Il2CppObject* p1, Func_3_t1694707683 * p2, const MethodInfo* method);
#define Observable_CombineLatest_TisBoolean_t211005341_TisInt32_t2847414787_TisIl2CppObject_m374310532(__this /* static, unused */, p0, p1, p2, method) ((  Il2CppObject* (*) (Il2CppObject * /* static, unused */, Il2CppObject*, Il2CppObject*, Func_3_t1694707683 *, const MethodInfo*))Observable_CombineLatest_TisBoolean_t211005341_TisInt32_t2847414787_TisIl2CppObject_m374310532_gshared)(__this /* static, unused */, p0, p1, p2, method)
// UniRx.IObservable`1<!!2> UniRx.Observable::CombineLatest<System.Boolean,System.Int32,<>__AnonType3`2<System.Boolean,System.Int32>>(UniRx.IObservable`1<!!0>,UniRx.IObservable`1<!!1>,System.Func`3<!!0,!!1,!!2>)
#define Observable_CombineLatest_TisBoolean_t211005341_TisInt32_t2847414787_TisU3CU3E__AnonType3_2_t3095608592_m3944907965(__this /* static, unused */, p0, p1, p2, method) ((  Il2CppObject* (*) (Il2CppObject * /* static, unused */, Il2CppObject*, Il2CppObject*, Func_3_t3953209855 *, const MethodInfo*))Observable_CombineLatest_TisBoolean_t211005341_TisInt32_t2847414787_TisIl2CppObject_m374310532_gshared)(__this /* static, unused */, p0, p1, p2, method)
// System.IDisposable UniRx.ObservableExtensions::Subscribe<System.Object>(UniRx.IObservable`1<!!0>,System.Action`1<!!0>)
extern "C"  Il2CppObject * ObservableExtensions_Subscribe_TisIl2CppObject_m2475506742_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* p0, Action_1_t985559125 * p1, const MethodInfo* method);
#define ObservableExtensions_Subscribe_TisIl2CppObject_m2475506742(__this /* static, unused */, p0, p1, method) ((  Il2CppObject * (*) (Il2CppObject * /* static, unused */, Il2CppObject*, Action_1_t985559125 *, const MethodInfo*))ObservableExtensions_Subscribe_TisIl2CppObject_m2475506742_gshared)(__this /* static, unused */, p0, p1, method)
// System.IDisposable UniRx.ObservableExtensions::Subscribe<<>__AnonType3`2<System.Boolean,System.Int32>>(UniRx.IObservable`1<!!0>,System.Action`1<!!0>)
#define ObservableExtensions_Subscribe_TisU3CU3E__AnonType3_2_t3095608592_m3781023631(__this /* static, unused */, p0, p1, method) ((  Il2CppObject * (*) (Il2CppObject * /* static, unused */, Il2CppObject*, Action_1_t3244061297 *, const MethodInfo*))ObservableExtensions_Subscribe_TisIl2CppObject_m2475506742_gshared)(__this /* static, unused */, p0, p1, method)
// !!0 UnityEngine.Resources::Load<UnityEngine.Sprite>(System.String)
#define Resources_Load_TisSprite_t4006040370_m3887230130(__this /* static, unused */, p0, method) ((  Sprite_t4006040370 * (*) (Il2CppObject * /* static, unused */, String_t*, const MethodInfo*))Resources_Load_TisIl2CppObject_m2208345422_gshared)(__this /* static, unused */, p0, method)
// !!0 DG.Tweening.TweenSettingsExtensions::OnComplete<System.Object>(!!0,DG.Tweening.TweenCallback)
extern "C"  Il2CppObject * TweenSettingsExtensions_OnComplete_TisIl2CppObject_m4170870576_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * p0, TweenCallback_t3786476454 * p1, const MethodInfo* method);
#define TweenSettingsExtensions_OnComplete_TisIl2CppObject_m4170870576(__this /* static, unused */, p0, p1, method) ((  Il2CppObject * (*) (Il2CppObject * /* static, unused */, Il2CppObject *, TweenCallback_t3786476454 *, const MethodInfo*))TweenSettingsExtensions_OnComplete_TisIl2CppObject_m4170870576_gshared)(__this /* static, unused */, p0, p1, method)
// !!0 DG.Tweening.TweenSettingsExtensions::OnComplete<DG.Tweening.Tweener>(!!0,DG.Tweening.TweenCallback)
#define TweenSettingsExtensions_OnComplete_TisTweener_t1766303790_m3265777438(__this /* static, unused */, p0, p1, method) ((  Tweener_t1766303790 * (*) (Il2CppObject * /* static, unused */, Tweener_t1766303790 *, TweenCallback_t3786476454 *, const MethodInfo*))TweenSettingsExtensions_OnComplete_TisIl2CppObject_m4170870576_gshared)(__this /* static, unused */, p0, p1, method)
// !!0 DG.Tweening.TweenSettingsExtensions::SetDelay<System.Object>(!!0,System.Single)
extern "C"  Il2CppObject * TweenSettingsExtensions_SetDelay_TisIl2CppObject_m3532516150_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * p0, float p1, const MethodInfo* method);
#define TweenSettingsExtensions_SetDelay_TisIl2CppObject_m3532516150(__this /* static, unused */, p0, p1, method) ((  Il2CppObject * (*) (Il2CppObject * /* static, unused */, Il2CppObject *, float, const MethodInfo*))TweenSettingsExtensions_SetDelay_TisIl2CppObject_m3532516150_gshared)(__this /* static, unused */, p0, p1, method)
// !!0 DG.Tweening.TweenSettingsExtensions::SetDelay<DG.Tweening.Tweener>(!!0,System.Single)
#define TweenSettingsExtensions_SetDelay_TisTweener_t1766303790_m3981276516(__this /* static, unused */, p0, p1, method) ((  Tweener_t1766303790 * (*) (Il2CppObject * /* static, unused */, Tweener_t1766303790 *, float, const MethodInfo*))TweenSettingsExtensions_SetDelay_TisIl2CppObject_m3532516150_gshared)(__this /* static, unused */, p0, p1, method)
// System.Collections.Generic.List`1<!!0> System.Linq.Enumerable::ToList<System.Object>(System.Collections.Generic.IEnumerable`1<!!0>)
extern "C"  List_1_t1634065389 * Enumerable_ToList_TisIl2CppObject_m4082139931_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* p0, const MethodInfo* method);
#define Enumerable_ToList_TisIl2CppObject_m4082139931(__this /* static, unused */, p0, method) ((  List_1_t1634065389 * (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Enumerable_ToList_TisIl2CppObject_m4082139931_gshared)(__this /* static, unused */, p0, method)
// System.Collections.Generic.List`1<!!0> System.Linq.Enumerable::ToList<IProcess>(System.Collections.Generic.IEnumerable`1<!!0>)
#define Enumerable_ToList_TisIProcess_t4026237414_m2912069109(__this /* static, unused */, p0, method) ((  List_1_t528229087 * (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Enumerable_ToList_TisIl2CppObject_m4082139931_gshared)(__this /* static, unused */, p0, method)
// System.Boolean System.Linq.Enumerable::All<System.Object>(System.Collections.Generic.IEnumerable`1<!!0>,System.Func`2<!!0,System.Boolean>)
extern "C"  bool Enumerable_All_TisIl2CppObject_m4156853638_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* p0, Func_2_t1509682273 * p1, const MethodInfo* method);
#define Enumerable_All_TisIl2CppObject_m4156853638(__this /* static, unused */, p0, p1, method) ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject*, Func_2_t1509682273 *, const MethodInfo*))Enumerable_All_TisIl2CppObject_m4156853638_gshared)(__this /* static, unused */, p0, p1, method)
// System.Boolean System.Linq.Enumerable::All<IProcess>(System.Collections.Generic.IEnumerable`1<!!0>,System.Func`2<!!0,System.Boolean>)
#define Enumerable_All_TisIProcess_t4026237414_m2691282026(__this /* static, unused */, p0, p1, method) ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject*, Func_2_t3494101351 *, const MethodInfo*))Enumerable_All_TisIl2CppObject_m4156853638_gshared)(__this /* static, unused */, p0, p1, method)
// !!0[] System.Linq.Enumerable::ToArray<System.Object>(System.Collections.Generic.IEnumerable`1<!!0>)
extern "C"  ObjectU5BU5D_t11523773* Enumerable_ToArray_TisIl2CppObject_m1058801104_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* p0, const MethodInfo* method);
#define Enumerable_ToArray_TisIl2CppObject_m1058801104(__this /* static, unused */, p0, method) ((  ObjectU5BU5D_t11523773* (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Enumerable_ToArray_TisIl2CppObject_m1058801104_gshared)(__this /* static, unused */, p0, method)
// !!0[] System.Linq.Enumerable::ToArray<System.String>(System.Collections.Generic.IEnumerable`1<!!0>)
#define Enumerable_ToArray_TisString_t_m2199204590(__this /* static, unused */, p0, method) ((  StringU5BU5D_t2956870243* (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Enumerable_ToArray_TisIl2CppObject_m1058801104_gshared)(__this /* static, unused */, p0, method)
// System.Collections.Generic.IEnumerable`1<!!0> ModestTree.LinqExtensions::Prepend<System.Object>(System.Collections.Generic.IEnumerable`1<!!0>,!!0)
extern "C"  Il2CppObject* LinqExtensions_Prepend_TisIl2CppObject_m1305813996_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* p0, Il2CppObject * p1, const MethodInfo* method);
#define LinqExtensions_Prepend_TisIl2CppObject_m1305813996(__this /* static, unused */, p0, p1, method) ((  Il2CppObject* (*) (Il2CppObject * /* static, unused */, Il2CppObject*, Il2CppObject *, const MethodInfo*))LinqExtensions_Prepend_TisIl2CppObject_m1305813996_gshared)(__this /* static, unused */, p0, p1, method)
// System.Collections.Generic.IEnumerable`1<!!0> ModestTree.LinqExtensions::Prepend<System.Type>(System.Collections.Generic.IEnumerable`1<!!0>,!!0)
#define LinqExtensions_Prepend_TisType_t_m2355200423(__this /* static, unused */, p0, p1, method) ((  Il2CppObject* (*) (Il2CppObject * /* static, unused */, Il2CppObject*, Type_t *, const MethodInfo*))LinqExtensions_Prepend_TisIl2CppObject_m1305813996_gshared)(__this /* static, unused */, p0, p1, method)
// System.Collections.Generic.IEnumerable`1<!!1> System.Linq.Enumerable::Select<System.Object,System.Object>(System.Collections.Generic.IEnumerable`1<!!0>,System.Func`2<!!0,!!1>)
extern "C"  Il2CppObject* Enumerable_Select_TisIl2CppObject_TisIl2CppObject_m2885164610_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* p0, Func_2_t2135783352 * p1, const MethodInfo* method);
#define Enumerable_Select_TisIl2CppObject_TisIl2CppObject_m2885164610(__this /* static, unused */, p0, p1, method) ((  Il2CppObject* (*) (Il2CppObject * /* static, unused */, Il2CppObject*, Func_2_t2135783352 *, const MethodInfo*))Enumerable_Select_TisIl2CppObject_TisIl2CppObject_m2885164610_gshared)(__this /* static, unused */, p0, p1, method)
// System.Collections.Generic.IEnumerable`1<!!1> System.Linq.Enumerable::Select<System.Type,System.String>(System.Collections.Generic.IEnumerable`1<!!0>,System.Func`2<!!0,!!1>)
#define Enumerable_Select_TisType_t_TisString_t_m322015137(__this /* static, unused */, p0, p1, method) ((  Il2CppObject* (*) (Il2CppObject * /* static, unused */, Il2CppObject*, Func_2_t1392394819 *, const MethodInfo*))Enumerable_Select_TisIl2CppObject_TisIl2CppObject_m2885164610_gshared)(__this /* static, unused */, p0, p1, method)
// !!0 System.Linq.Enumerable::Single<System.Object>(System.Collections.Generic.IEnumerable`1<!!0>)
extern "C"  Il2CppObject * Enumerable_Single_TisIl2CppObject_m3651905832_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* p0, const MethodInfo* method);
#define Enumerable_Single_TisIl2CppObject_m3651905832(__this /* static, unused */, p0, method) ((  Il2CppObject * (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Enumerable_Single_TisIl2CppObject_m3651905832_gshared)(__this /* static, unused */, p0, method)
// !!0 System.Linq.Enumerable::Single<System.Type>(System.Collections.Generic.IEnumerable`1<!!0>)
#define Enumerable_Single_TisType_t_m4159287907(__this /* static, unused */, p0, method) ((  Type_t * (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Enumerable_Single_TisIl2CppObject_m3651905832_gshared)(__this /* static, unused */, p0, method)
// System.Boolean System.Linq.Enumerable::Any<System.Object>(System.Collections.Generic.IEnumerable`1<!!0>)
extern "C"  bool Enumerable_Any_TisIl2CppObject_m3411867191_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* p0, const MethodInfo* method);
#define Enumerable_Any_TisIl2CppObject_m3411867191(__this /* static, unused */, p0, method) ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Enumerable_Any_TisIl2CppObject_m3411867191_gshared)(__this /* static, unused */, p0, method)
// System.Boolean System.Linq.Enumerable::Any<System.Attribute>(System.Collections.Generic.IEnumerable`1<!!0>)
#define Enumerable_Any_TisAttribute_t498693649_m1582154716(__this /* static, unused */, p0, method) ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Enumerable_Any_TisIl2CppObject_m3411867191_gshared)(__this /* static, unused */, p0, method)
// System.Collections.Generic.IEnumerable`1<!!0> System.Linq.Enumerable::Cast<System.Object>(System.Collections.IEnumerable)
extern "C"  Il2CppObject* Enumerable_Cast_TisIl2CppObject_m3904275306_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * p0, const MethodInfo* method);
#define Enumerable_Cast_TisIl2CppObject_m3904275306(__this /* static, unused */, p0, method) ((  Il2CppObject* (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))Enumerable_Cast_TisIl2CppObject_m3904275306_gshared)(__this /* static, unused */, p0, method)
// System.Collections.Generic.IEnumerable`1<!!0> System.Linq.Enumerable::Cast<System.Attribute>(System.Collections.IEnumerable)
#define Enumerable_Cast_TisAttribute_t498693649_m1153716551(__this /* static, unused */, p0, method) ((  Il2CppObject* (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))Enumerable_Cast_TisIl2CppObject_m3904275306_gshared)(__this /* static, unused */, p0, method)
// System.Collections.Generic.IEnumerable`1<!!0> System.Linq.Enumerable::Where<System.Object>(System.Collections.Generic.IEnumerable`1<!!0>,System.Func`2<!!0,System.Boolean>)
extern "C"  Il2CppObject* Enumerable_Where_TisIl2CppObject_m3480373697_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* p0, Func_2_t1509682273 * p1, const MethodInfo* method);
#define Enumerable_Where_TisIl2CppObject_m3480373697(__this /* static, unused */, p0, p1, method) ((  Il2CppObject* (*) (Il2CppObject * /* static, unused */, Il2CppObject*, Func_2_t1509682273 *, const MethodInfo*))Enumerable_Where_TisIl2CppObject_m3480373697_gshared)(__this /* static, unused */, p0, p1, method)
// System.Collections.Generic.IEnumerable`1<!!0> System.Linq.Enumerable::Where<System.Attribute>(System.Collections.Generic.IEnumerable`1<!!0>,System.Func`2<!!0,System.Boolean>)
#define Enumerable_Where_TisAttribute_t498693649_m3527660099(__this /* static, unused */, p0, p1, method) ((  Il2CppObject* (*) (Il2CppObject * /* static, unused */, Il2CppObject*, Func_2_t1279580208 *, const MethodInfo*))Enumerable_Where_TisIl2CppObject_m3480373697_gshared)(__this /* static, unused */, p0, p1, method)
// System.Boolean System.Linq.Enumerable::Contains<System.Object>(System.Collections.Generic.IEnumerable`1<!!0>,!!0)
extern "C"  bool Enumerable_Contains_TisIl2CppObject_m2362777420_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* p0, Il2CppObject * p1, const MethodInfo* method);
#define Enumerable_Contains_TisIl2CppObject_m2362777420(__this /* static, unused */, p0, p1, method) ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject*, Il2CppObject *, const MethodInfo*))Enumerable_Contains_TisIl2CppObject_m2362777420_gshared)(__this /* static, unused */, p0, p1, method)
// System.Boolean System.Linq.Enumerable::Contains<System.Type>(System.Collections.Generic.IEnumerable`1<!!0>,!!0)
#define Enumerable_Contains_TisType_t_m2414400775(__this /* static, unused */, p0, p1, method) ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject*, Type_t *, const MethodInfo*))Enumerable_Contains_TisIl2CppObject_m2362777420_gshared)(__this /* static, unused */, p0, p1, method)
// !!0[] UnityEngine.Object::FindObjectsOfType<System.Object>()
extern "C"  ObjectU5BU5D_t11523773* Object_FindObjectsOfType_TisIl2CppObject_m3928305500_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define Object_FindObjectsOfType_TisIl2CppObject_m3928305500(__this /* static, unused */, method) ((  ObjectU5BU5D_t11523773* (*) (Il2CppObject * /* static, unused */, const MethodInfo*))Object_FindObjectsOfType_TisIl2CppObject_m3928305500_gshared)(__this /* static, unused */, method)
// !!0[] UnityEngine.Object::FindObjectsOfType<UnityEngine.Transform>()
#define Object_FindObjectsOfType_TisTransform_t284553113_m2131108345(__this /* static, unused */, method) ((  TransformU5BU5D_t3681339876* (*) (Il2CppObject * /* static, unused */, const MethodInfo*))Object_FindObjectsOfType_TisIl2CppObject_m3928305500_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.IEnumerable`1<!!0> System.Linq.Enumerable::Where<UnityEngine.Transform>(System.Collections.Generic.IEnumerable`1<!!0>,System.Func`2<!!0,System.Boolean>)
#define Enumerable_Where_TisTransform_t284553113_m289571163(__this /* static, unused */, p0, p1, method) ((  Il2CppObject* (*) (Il2CppObject * /* static, unused */, Il2CppObject*, Func_2_t884531080 *, const MethodInfo*))Enumerable_Where_TisIl2CppObject_m3480373697_gshared)(__this /* static, unused */, p0, p1, method)
// System.Collections.Generic.IEnumerable`1<!!1> System.Linq.Enumerable::Select<UnityEngine.Transform,UnityEngine.GameObject>(System.Collections.Generic.IEnumerable`1<!!0>,System.Func`2<!!0,!!1>)
#define Enumerable_Select_TisTransform_t284553113_TisGameObject_t4012695102_m2957448055(__this /* static, unused */, p0, p1, method) ((  Il2CppObject* (*) (Il2CppObject * /* static, unused */, Il2CppObject*, Func_2_t391253545 *, const MethodInfo*))Enumerable_Select_TisIl2CppObject_TisIl2CppObject_m2885164610_gshared)(__this /* static, unused */, p0, p1, method)
// System.Collections.Generic.List`1<!!0> System.Linq.Enumerable::ToList<UnityEngine.GameObject>(System.Collections.Generic.IEnumerable`1<!!0>)
#define Enumerable_ToList_TisGameObject_t4012695102_m3323433239(__this /* static, unused */, p0, method) ((  List_1_t514686775 * (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Enumerable_ToList_TisIl2CppObject_m4082139931_gshared)(__this /* static, unused */, p0, method)
// !!0 UnityEngine.Object::Instantiate<System.Object>(!!0)
extern "C"  Il2CppObject * Object_Instantiate_TisIl2CppObject_m3133387403_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * p0, const MethodInfo* method);
#define Object_Instantiate_TisIl2CppObject_m3133387403(__this /* static, unused */, p0, method) ((  Il2CppObject * (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))Object_Instantiate_TisIl2CppObject_m3133387403_gshared)(__this /* static, unused */, p0, method)
// !!0 UnityEngine.Object::Instantiate<UnityEngine.GameObject>(!!0)
#define Object_Instantiate_TisGameObject_t4012695102_m3917608929(__this /* static, unused */, p0, method) ((  GameObject_t4012695102 * (*) (Il2CppObject * /* static, unused */, GameObject_t4012695102 *, const MethodInfo*))Object_Instantiate_TisIl2CppObject_m3133387403_gshared)(__this /* static, unused */, p0, method)
// !!0 UnityEngine.GameObject::GetComponent<System.Object>()
extern "C"  Il2CppObject * GameObject_GetComponent_TisIl2CppObject_m2447772384_gshared (GameObject_t4012695102 * __this, const MethodInfo* method);
#define GameObject_GetComponent_TisIl2CppObject_m2447772384(__this, method) ((  Il2CppObject * (*) (GameObject_t4012695102 *, const MethodInfo*))GameObject_GetComponent_TisIl2CppObject_m2447772384_gshared)(__this, method)
// !!0 UnityEngine.GameObject::GetComponent<Counter>()
#define GameObject_GetComponent_TisCounter_t2622483932_m3934559109(__this, method) ((  Counter_t2622483932 * (*) (GameObject_t4012695102 *, const MethodInfo*))GameObject_GetComponent_TisIl2CppObject_m2447772384_gshared)(__this, method)
// UniRx.IObservable`1<!!2> UniRx.Observable::SelectMany<System.Double,System.Double,System.Double>(UniRx.IObservable`1<!!0>,System.Func`2<!!0,UniRx.IObservable`1<!!1>>,System.Func`3<!!0,!!1,!!2>)
extern "C"  Il2CppObject* Observable_SelectMany_TisDouble_t534516614_TisDouble_t534516614_TisDouble_t534516614_m83073602_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* p0, Func_2_t2628749132 * p1, Func_3_t1933728903 * p2, const MethodInfo* method);
#define Observable_SelectMany_TisDouble_t534516614_TisDouble_t534516614_TisDouble_t534516614_m83073602(__this /* static, unused */, p0, p1, p2, method) ((  Il2CppObject* (*) (Il2CppObject * /* static, unused */, Il2CppObject*, Func_2_t2628749132 *, Func_3_t1933728903 *, const MethodInfo*))Observable_SelectMany_TisDouble_t534516614_TisDouble_t534516614_TisDouble_t534516614_m83073602_gshared)(__this /* static, unused */, p0, p1, p2, method)
// System.IDisposable UniRx.ObservableExtensions::Subscribe<System.Double>(UniRx.IObservable`1<!!0>,System.Action`1<!!0>)
extern "C"  Il2CppObject * ObservableExtensions_Subscribe_TisDouble_t534516614_m2613047548_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* p0, Action_1_t682969319 * p1, const MethodInfo* method);
#define ObservableExtensions_Subscribe_TisDouble_t534516614_m2613047548(__this /* static, unused */, p0, p1, method) ((  Il2CppObject * (*) (Il2CppObject * /* static, unused */, Il2CppObject*, Action_1_t682969319 *, const MethodInfo*))ObservableExtensions_Subscribe_TisDouble_t534516614_m2613047548_gshared)(__this /* static, unused */, p0, p1, method)
// !!0[] UnityEngine.Component::GetComponentsInChildren<System.Object>()
extern "C"  ObjectU5BU5D_t11523773* Component_GetComponentsInChildren_TisIl2CppObject_m2266473418_gshared (Component_t2126946602 * __this, const MethodInfo* method);
#define Component_GetComponentsInChildren_TisIl2CppObject_m2266473418(__this, method) ((  ObjectU5BU5D_t11523773* (*) (Component_t2126946602 *, const MethodInfo*))Component_GetComponentsInChildren_TisIl2CppObject_m2266473418_gshared)(__this, method)
// !!0[] UnityEngine.Component::GetComponentsInChildren<UnityEngine.SpriteRenderer>()
#define Component_GetComponentsInChildren_TisSpriteRenderer_t2223784725_m3166954935(__this, method) ((  SpriteRendererU5BU5D_t4109544696* (*) (Component_t2126946602 *, const MethodInfo*))Component_GetComponentsInChildren_TisIl2CppObject_m2266473418_gshared)(__this, method)
// !!1 System.Linq.Enumerable::Aggregate<System.Object,System.Single>(System.Collections.Generic.IEnumerable`1<!!0>,!!1,System.Func`3<!!1,!!0,!!1>)
extern "C"  float Enumerable_Aggregate_TisIl2CppObject_TisSingle_t958209021_m1653673482_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* p0, float p1, Func_3_t2574913143 * p2, const MethodInfo* method);
#define Enumerable_Aggregate_TisIl2CppObject_TisSingle_t958209021_m1653673482(__this /* static, unused */, p0, p1, p2, method) ((  float (*) (Il2CppObject * /* static, unused */, Il2CppObject*, float, Func_3_t2574913143 *, const MethodInfo*))Enumerable_Aggregate_TisIl2CppObject_TisSingle_t958209021_m1653673482_gshared)(__this /* static, unused */, p0, p1, p2, method)
// !!1 System.Linq.Enumerable::Aggregate<SubstanceBase`2/Intrusion<System.Single,System.Single>,System.Single>(System.Collections.Generic.IEnumerable`1<!!0>,!!1,System.Func`3<!!1,!!0,!!1>)
#define Enumerable_Aggregate_TisIntrusion_t78896424_TisSingle_t958209021_m718774751(__this /* static, unused */, p0, p1, p2, method) ((  float (*) (Il2CppObject * /* static, unused */, Il2CppObject*, float, Func_3_t3383474419 *, const MethodInfo*))Enumerable_Aggregate_TisIl2CppObject_TisSingle_t958209021_m1653673482_gshared)(__this /* static, unused */, p0, p1, p2, method)
// !!0 UnityEngine.Component::GetComponent<UnityEngine.UI.ScrollRect>()
#define Component_GetComponent_TisScrollRect_t1048578170_m2560181989(__this, method) ((  ScrollRect_t1048578170 * (*) (Component_t2126946602 *, const MethodInfo*))Component_GetComponent_TisIl2CppObject_m267839954_gshared)(__this, method)
// System.Single System.Linq.Enumerable::Sum<System.Object>(System.Collections.Generic.IEnumerable`1<!!0>,System.Func`2<!!0,System.Single>)
extern "C"  float Enumerable_Sum_TisIl2CppObject_m3330304526_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* p0, Func_2_t2256885953 * p1, const MethodInfo* method);
#define Enumerable_Sum_TisIl2CppObject_m3330304526(__this /* static, unused */, p0, p1, method) ((  float (*) (Il2CppObject * /* static, unused */, Il2CppObject*, Func_2_t2256885953 *, const MethodInfo*))Enumerable_Sum_TisIl2CppObject_m3330304526_gshared)(__this /* static, unused */, p0, p1, method)
// System.Single System.Linq.Enumerable::Sum<ITableBlock>(System.Collections.Generic.IEnumerable`1<!!0>,System.Func`2<!!0,System.Single>)
#define Enumerable_Sum_TisITableBlock_t2428542600_m3849749412(__this /* static, unused */, p0, p1, method) ((  float (*) (Il2CppObject * /* static, unused */, Il2CppObject*, Func_2_t4022284381 *, const MethodInfo*))Enumerable_Sum_TisIl2CppObject_m3330304526_gshared)(__this /* static, unused */, p0, p1, method)
// !!0 UnityEngine.GameObject::GetComponent<UnityEngine.UI.Text>()
#define GameObject_GetComponent_TisText_t3286458198_m202917489(__this, method) ((  Text_t3286458198 * (*) (GameObject_t4012695102 *, const MethodInfo*))GameObject_GetComponent_TisIl2CppObject_m2447772384_gshared)(__this, method)
// System.Boolean System.Linq.Enumerable::Any<System.Object>(System.Collections.Generic.IEnumerable`1<!!0>,System.Func`2<!!0,System.Boolean>)
extern "C"  bool Enumerable_Any_TisIl2CppObject_m4067477969_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* p0, Func_2_t1509682273 * p1, const MethodInfo* method);
#define Enumerable_Any_TisIl2CppObject_m4067477969(__this /* static, unused */, p0, p1, method) ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject*, Func_2_t1509682273 *, const MethodInfo*))Enumerable_Any_TisIl2CppObject_m4067477969_gshared)(__this /* static, unused */, p0, p1, method)
// System.Boolean System.Linq.Enumerable::Any<SubstanceBase`2/Intrusion<System.Boolean,System.Boolean>>(System.Collections.Generic.IEnumerable`1<!!0>,System.Func`2<!!0,System.Boolean>)
#define Enumerable_Any_TisIntrusion_t1917188776_m2092008156(__this /* static, unused */, p0, p1, method) ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject*, Func_2_t2346177373 *, const MethodInfo*))Enumerable_Any_TisIl2CppObject_m4067477969_gshared)(__this /* static, unused */, p0, p1, method)
// System.Collections.Generic.IEnumerable`1<!!1> System.Linq.Enumerable::Select<System.String,System.String>(System.Collections.Generic.IEnumerable`1<!!0>,System.Func`2<!!0,!!1>)
#define Enumerable_Select_TisString_t_TisString_t_m4109456874(__this /* static, unused */, p0, p1, method) ((  Il2CppObject* (*) (Il2CppObject * /* static, unused */, Il2CppObject*, Func_2_t917545008 *, const MethodInfo*))Enumerable_Select_TisIl2CppObject_TisIl2CppObject_m2885164610_gshared)(__this /* static, unused */, p0, p1, method)
// System.Collections.Generic.IEnumerable`1<!!0> System.Linq.Enumerable::Where<System.String>(System.Collections.Generic.IEnumerable`1<!!0>,System.Func`2<!!0,System.Boolean>)
#define Enumerable_Where_TisString_t_m178095414(__this /* static, unused */, p0, p1, method) ((  Il2CppObject* (*) (Il2CppObject * /* static, unused */, Il2CppObject*, Func_2_t160061447 *, const MethodInfo*))Enumerable_Where_TisIl2CppObject_m3480373697_gshared)(__this /* static, unused */, p0, p1, method)
// System.Collections.Generic.List`1<!!0> System.Linq.Enumerable::ToList<System.String>(System.Collections.Generic.IEnumerable`1<!!0>)
#define Enumerable_ToList_TisString_t_m790467565(__this /* static, unused */, p0, method) ((  List_1_t1765447871 * (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Enumerable_ToList_TisIl2CppObject_m4082139931_gshared)(__this /* static, unused */, p0, method)
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void LitJson.WrapperFactory::.ctor(System.Object,System.IntPtr)
extern "C"  void WrapperFactory__ctor_m2304375437 (WrapperFactory_t3863042620 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->method);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// LitJson.IJsonWrapper LitJson.WrapperFactory::Invoke()
extern "C"  Il2CppObject * WrapperFactory_Invoke_m568330355 (WrapperFactory_t3863042620 * __this, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		WrapperFactory_Invoke_m568330355((WrapperFactory_t3863042620 *)__this->get_prev_9(), method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if ((__this->get_m_target_2() != NULL || MethodHasParameters((MethodInfo*)(__this->get_method_3().get_m_value_0()))) && ___methodIsStatic)
	{
		typedef Il2CppObject * (*FunctionPointerType) (Il2CppObject *, void* __this, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef Il2CppObject * (*FunctionPointerType) (void* __this, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
extern "C" Il2CppObject * pinvoke_delegate_wrapper_WrapperFactory_t3863042620(Il2CppObject* delegate)
{
	typedef Il2CppObject * (STDCALL *native_function_ptr_type)();
	native_function_ptr_type _il2cpp_pinvoke_func = ((native_function_ptr_type)((Il2CppDelegate*)delegate)->method->method);
	// Native function invocation and marshaling of return value back from native representation
	Il2CppObject * _return_value = _il2cpp_pinvoke_func();
	Il2CppObject * __return_value_unmarshaled = NULL;
	IL2CPP_RAISE_MANAGED_EXCEPTION(il2cpp_codegen_get_not_supported_exception("Cannot marshal type 'LitJson.IJsonWrapper'."));

	return __return_value_unmarshaled;
}
// System.IAsyncResult LitJson.WrapperFactory::BeginInvoke(System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * WrapperFactory_BeginInvoke_m3918882044 (WrapperFactory_t3863042620 * __this, AsyncCallback_t1363551830 * ___callback0, Il2CppObject * ___object1, const MethodInfo* method)
{
	void *__d_args[1] = {0};
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback0, (Il2CppObject*)___object1);
}
// LitJson.IJsonWrapper LitJson.WrapperFactory::EndInvoke(System.IAsyncResult)
extern "C"  Il2CppObject * WrapperFactory_EndInvoke_m2609235305 (WrapperFactory_t3863042620 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return (Il2CppObject *)__result;
}
// System.Void LitJson.WriterContext::.ctor()
extern "C"  void WriterContext__ctor_m2158004766 (WriterContext_t3079472833 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Localithator::.ctor()
extern "C"  void Localithator__ctor_m4069343699 (Localithator_t3481416136 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m350695606(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Localithator::Start()
extern const MethodInfo* Component_GetComponent_TisText_t3286458198_m1610753993_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral1294018644;
extern const uint32_t Localithator_Start_m3016481491_MetadataUsageId;
extern "C"  void Localithator_Start_m3016481491 (Localithator_t3481416136 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Localithator_Start_m3016481491_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Text_t3286458198 * L_0 = Component_GetComponent_TisText_t3286458198_m1610753993(__this, /*hidden argument*/Component_GetComponent_TisText_t3286458198_m1610753993_MethodInfo_var);
		String_t* L_1 = LocalizationManager_GetString_m738567075(NULL /*static, unused*/, _stringLiteral1294018644, /*hidden argument*/NULL);
		NullCheck(L_0);
		VirtActionInvoker1< String_t* >::Invoke(65 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_0, L_1);
		return;
	}
}
// System.Void Localithator::Update()
extern "C"  void Localithator_Update_m3322465178 (Localithator_t3481416136 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void LocalizationManager::Initialize()
extern Il2CppClass* Dictionary_2_t2606186806_il2cpp_TypeInfo_var;
extern Il2CppClass* LocalizationManager_t1078246068_il2cpp_TypeInfo_var;
extern Il2CppClass* XmlDocument_t3705263098_il2cpp_TypeInfo_var;
extern Il2CppClass* IEnumerator_t287207039_il2cpp_TypeInfo_var;
extern Il2CppClass* XmlNode_t3592213601_il2cpp_TypeInfo_var;
extern Il2CppClass* IDisposable_t1628921374_il2cpp_TypeInfo_var;
extern const MethodInfo* Dictionary_2__ctor_m640701813_MethodInfo_var;
extern const MethodInfo* Resources_Load_TisTextAsset_t2461560304_m585700060_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral3087975801;
extern Il2CppCodeGenString* _stringLiteral1555550099;
extern Il2CppCodeGenString* _stringLiteral3087975391;
extern Il2CppCodeGenString* _stringLiteral2691209840;
extern Il2CppCodeGenString* _stringLiteral3482961561;
extern Il2CppCodeGenString* _stringLiteral3355;
extern const uint32_t LocalizationManager_Initialize_m1786158525_MetadataUsageId;
extern "C"  void LocalizationManager_Initialize_m1786158525 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (LocalizationManager_Initialize_m1786158525_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	TextAsset_t2461560304 * V_0 = NULL;
	XmlDocument_t3705263098 * V_1 = NULL;
	XmlNode_t3592213601 * V_2 = NULL;
	XmlNode_t3592213601 * V_3 = NULL;
	Il2CppObject * V_4 = NULL;
	String_t* V_5 = NULL;
	int32_t V_6 = 0;
	Il2CppObject * V_7 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Dictionary_2_t2606186806 * L_0 = (Dictionary_2_t2606186806 *)il2cpp_codegen_object_new(Dictionary_2_t2606186806_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m640701813(L_0, /*hidden argument*/Dictionary_2__ctor_m640701813_MethodInfo_var);
		((LocalizationManager_t1078246068_StaticFields*)LocalizationManager_t1078246068_il2cpp_TypeInfo_var->static_fields)->set_strings_0(L_0);
		int32_t L_1 = Application_get_systemLanguage_m1182442618(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_6 = L_1;
		int32_t L_2 = V_6;
		if ((((int32_t)L_2) == ((int32_t)3)))
		{
			goto IL_0030;
		}
	}
	{
		int32_t L_3 = V_6;
		if ((((int32_t)L_3) == ((int32_t)((int32_t)30))))
		{
			goto IL_0030;
		}
	}
	{
		int32_t L_4 = V_6;
		if ((((int32_t)L_4) == ((int32_t)((int32_t)38))))
		{
			goto IL_0030;
		}
	}
	{
		goto IL_004a;
	}

IL_0030:
	{
		TextAsset_t2461560304 * L_5 = Resources_Load_TisTextAsset_t2461560304_m585700060(NULL /*static, unused*/, _stringLiteral3087975801, /*hidden argument*/Resources_Load_TisTextAsset_t2461560304_m585700060_MethodInfo_var);
		V_0 = L_5;
		((LocalizationManager_t1078246068_StaticFields*)LocalizationManager_t1078246068_il2cpp_TypeInfo_var->static_fields)->set_language_1(_stringLiteral1555550099);
		goto IL_0064;
	}

IL_004a:
	{
		TextAsset_t2461560304 * L_6 = Resources_Load_TisTextAsset_t2461560304_m585700060(NULL /*static, unused*/, _stringLiteral3087975391, /*hidden argument*/Resources_Load_TisTextAsset_t2461560304_m585700060_MethodInfo_var);
		V_0 = L_6;
		((LocalizationManager_t1078246068_StaticFields*)LocalizationManager_t1078246068_il2cpp_TypeInfo_var->static_fields)->set_language_1(_stringLiteral2691209840);
		goto IL_0064;
	}

IL_0064:
	{
		XmlDocument_t3705263098 * L_7 = (XmlDocument_t3705263098 *)il2cpp_codegen_object_new(XmlDocument_t3705263098_il2cpp_TypeInfo_var);
		XmlDocument__ctor_m467220425(L_7, /*hidden argument*/NULL);
		V_1 = L_7;
		XmlDocument_t3705263098 * L_8 = V_1;
		TextAsset_t2461560304 * L_9 = V_0;
		NullCheck(L_9);
		String_t* L_10 = TextAsset_get_text_m655578209(L_9, /*hidden argument*/NULL);
		NullCheck(L_8);
		VirtActionInvoker1< String_t* >::Invoke(59 /* System.Void System.Xml.XmlDocument::LoadXml(System.String) */, L_8, L_10);
		XmlDocument_t3705263098 * L_11 = V_1;
		NullCheck(L_11);
		XmlNodeList_t3966370975 * L_12 = XmlNode_SelectNodes_m3515755997(L_11, _stringLiteral3482961561, /*hidden argument*/NULL);
		NullCheck(L_12);
		XmlNode_t3592213601 * L_13 = VirtFuncInvoker1< XmlNode_t3592213601 *, int32_t >::Invoke(6 /* System.Xml.XmlNode System.Xml.XmlNodeList::get_ItemOf(System.Int32) */, L_12, 0);
		V_2 = L_13;
		XmlNode_t3592213601 * L_14 = V_2;
		NullCheck(L_14);
		XmlNodeList_t3966370975 * L_15 = VirtFuncInvoker0< XmlNodeList_t3966370975 * >::Invoke(9 /* System.Xml.XmlNodeList System.Xml.XmlNode::get_ChildNodes() */, L_14);
		NullCheck(L_15);
		Il2CppObject * L_16 = VirtFuncInvoker0< Il2CppObject * >::Invoke(7 /* System.Collections.IEnumerator System.Xml.XmlNodeList::GetEnumerator() */, L_15);
		V_4 = L_16;
	}

IL_0095:
	try
	{ // begin try (depth: 1)
		{
			goto IL_00f3;
		}

IL_009a:
		{
			Il2CppObject * L_17 = V_4;
			NullCheck(L_17);
			Il2CppObject * L_18 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(0 /* System.Object System.Collections.IEnumerator::get_Current() */, IEnumerator_t287207039_il2cpp_TypeInfo_var, L_17);
			V_3 = ((XmlNode_t3592213601 *)CastclassClass(L_18, XmlNode_t3592213601_il2cpp_TypeInfo_var));
			XmlNode_t3592213601 * L_19 = V_3;
			NullCheck(L_19);
			XmlAttributeCollection_t571717291 * L_20 = VirtFuncInvoker0< XmlAttributeCollection_t571717291 * >::Invoke(6 /* System.Xml.XmlAttributeCollection System.Xml.XmlNode::get_Attributes() */, L_19);
			NullCheck(L_20);
			XmlNode_t3592213601 * L_21 = VirtFuncInvoker1< XmlNode_t3592213601 *, String_t* >::Invoke(7 /* System.Xml.XmlNode System.Xml.XmlNamedNodeMap::GetNamedItem(System.String) */, L_20, _stringLiteral3355);
			NullCheck(L_21);
			String_t* L_22 = VirtFuncInvoker0< String_t* >::Invoke(25 /* System.String System.Xml.XmlNode::get_Value() */, L_21);
			V_5 = L_22;
			Dictionary_2_t2606186806 * L_23 = ((LocalizationManager_t1078246068_StaticFields*)LocalizationManager_t1078246068_il2cpp_TypeInfo_var->static_fields)->get_strings_0();
			String_t* L_24 = V_5;
			NullCheck(L_23);
			bool L_25 = VirtFuncInvoker1< bool, String_t* >::Invoke(19 /* System.Boolean System.Collections.Generic.Dictionary`2<System.String,System.String>::ContainsKey(!0) */, L_23, L_24);
			if (!L_25)
			{
				goto IL_00dc;
			}
		}

IL_00cf:
		{
			Dictionary_2_t2606186806 * L_26 = ((LocalizationManager_t1078246068_StaticFields*)LocalizationManager_t1078246068_il2cpp_TypeInfo_var->static_fields)->get_strings_0();
			String_t* L_27 = V_5;
			NullCheck(L_26);
			VirtFuncInvoker1< bool, String_t* >::Invoke(20 /* System.Boolean System.Collections.Generic.Dictionary`2<System.String,System.String>::Remove(!0) */, L_26, L_27);
		}

IL_00dc:
		{
			Dictionary_2_t2606186806 * L_28 = ((LocalizationManager_t1078246068_StaticFields*)LocalizationManager_t1078246068_il2cpp_TypeInfo_var->static_fields)->get_strings_0();
			String_t* L_29 = V_5;
			XmlNode_t3592213601 * L_30 = V_3;
			NullCheck(L_30);
			String_t* L_31 = VirtFuncInvoker0< String_t* >::Invoke(12 /* System.String System.Xml.XmlNode::get_InnerText() */, L_30);
			NullCheck(L_31);
			String_t* L_32 = String_ToString_m1382284457(L_31, /*hidden argument*/NULL);
			NullCheck(L_28);
			VirtActionInvoker2< String_t*, String_t* >::Invoke(18 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.String>::Add(!0,!1) */, L_28, L_29, L_32);
		}

IL_00f3:
		{
			Il2CppObject * L_33 = V_4;
			NullCheck(L_33);
			bool L_34 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t287207039_il2cpp_TypeInfo_var, L_33);
			if (L_34)
			{
				goto IL_009a;
			}
		}

IL_00ff:
		{
			IL2CPP_LEAVE(0x11A, FINALLY_0104);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0104;
	}

FINALLY_0104:
	{ // begin finally (depth: 1)
		{
			Il2CppObject * L_35 = V_4;
			V_7 = ((Il2CppObject *)IsInst(L_35, IDisposable_t1628921374_il2cpp_TypeInfo_var));
			Il2CppObject * L_36 = V_7;
			if (L_36)
			{
				goto IL_0112;
			}
		}

IL_0111:
		{
			IL2CPP_END_FINALLY(260)
		}

IL_0112:
		{
			Il2CppObject * L_37 = V_7;
			NullCheck(L_37);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, L_37);
			IL2CPP_END_FINALLY(260)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(260)
	{
		IL2CPP_JUMP_TBL(0x11A, IL_011a)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_011a:
	{
		return;
	}
}
// System.String LocalizationManager::PathForLocalization(System.String)
extern Il2CppClass* LocalizationManager_t1078246068_il2cpp_TypeInfo_var;
extern Il2CppClass* Dictionary_2_t190145395_il2cpp_TypeInfo_var;
extern const MethodInfo* Dictionary_2__ctor_m1958628151_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral3651;
extern Il2CppCodeGenString* _stringLiteral3087975801;
extern Il2CppCodeGenString* _stringLiteral3087975398;
extern const uint32_t LocalizationManager_PathForLocalization_m2659746189_MetadataUsageId;
extern "C"  String_t* LocalizationManager_PathForLocalization_m2659746189 (Il2CppObject * __this /* static, unused */, String_t* ___langId0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (LocalizationManager_PathForLocalization_m2659746189_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	String_t* V_0 = NULL;
	Dictionary_2_t190145395 * V_1 = NULL;
	int32_t V_2 = 0;
	{
		String_t* L_0 = ___langId0;
		V_0 = L_0;
		String_t* L_1 = V_0;
		if (!L_1)
		{
			goto IL_004e;
		}
	}
	{
		Dictionary_2_t190145395 * L_2 = ((LocalizationManager_t1078246068_StaticFields*)LocalizationManager_t1078246068_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__switchU24mapD_2();
		if (L_2)
		{
			goto IL_002b;
		}
	}
	{
		Dictionary_2_t190145395 * L_3 = (Dictionary_2_t190145395 *)il2cpp_codegen_object_new(Dictionary_2_t190145395_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m1958628151(L_3, 1, /*hidden argument*/Dictionary_2__ctor_m1958628151_MethodInfo_var);
		V_1 = L_3;
		Dictionary_2_t190145395 * L_4 = V_1;
		NullCheck(L_4);
		VirtActionInvoker2< String_t*, int32_t >::Invoke(18 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::Add(!0,!1) */, L_4, _stringLiteral3651, 0);
		Dictionary_2_t190145395 * L_5 = V_1;
		((LocalizationManager_t1078246068_StaticFields*)LocalizationManager_t1078246068_il2cpp_TypeInfo_var->static_fields)->set_U3CU3Ef__switchU24mapD_2(L_5);
	}

IL_002b:
	{
		Dictionary_2_t190145395 * L_6 = ((LocalizationManager_t1078246068_StaticFields*)LocalizationManager_t1078246068_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__switchU24mapD_2();
		String_t* L_7 = V_0;
		NullCheck(L_6);
		bool L_8 = VirtFuncInvoker2< bool, String_t*, int32_t* >::Invoke(21 /* System.Boolean System.Collections.Generic.Dictionary`2<System.String,System.Int32>::TryGetValue(!0,!1&) */, L_6, L_7, (&V_2));
		if (!L_8)
		{
			goto IL_004e;
		}
	}
	{
		int32_t L_9 = V_2;
		if (!L_9)
		{
			goto IL_0048;
		}
	}
	{
		goto IL_004e;
	}

IL_0048:
	{
		return _stringLiteral3087975801;
	}

IL_004e:
	{
		return _stringLiteral3087975398;
	}
}
// System.String LocalizationManager::GetLanguage()
extern Il2CppClass* LocalizationManager_t1078246068_il2cpp_TypeInfo_var;
extern const uint32_t LocalizationManager_GetLanguage_m3001286662_MetadataUsageId;
extern "C"  String_t* LocalizationManager_GetLanguage_m3001286662 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (LocalizationManager_GetLanguage_m3001286662_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = ((LocalizationManager_t1078246068_StaticFields*)LocalizationManager_t1078246068_il2cpp_TypeInfo_var->static_fields)->get_language_1();
		return L_0;
	}
}
// System.Boolean LocalizationManager::UseVk()
extern "C"  bool LocalizationManager_UseVk_m310211749 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		int32_t L_0 = Application_get_systemLanguage_m1182442618(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_0;
		int32_t L_1 = V_0;
		if ((((int32_t)L_1) == ((int32_t)3)))
		{
			goto IL_0022;
		}
	}
	{
		int32_t L_2 = V_0;
		if ((((int32_t)L_2) == ((int32_t)((int32_t)30))))
		{
			goto IL_0022;
		}
	}
	{
		int32_t L_3 = V_0;
		if ((((int32_t)L_3) == ((int32_t)((int32_t)38))))
		{
			goto IL_0022;
		}
	}
	{
		goto IL_0024;
	}

IL_0022:
	{
		return (bool)1;
	}

IL_0024:
	{
		return (bool)0;
	}
}
// System.String LocalizationManager::GetString(System.String)
extern Il2CppClass* LocalizationManager_t1078246068_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t1588791936_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3603947877;
extern const uint32_t LocalizationManager_GetString_m738567075_MetadataUsageId;
extern "C"  String_t* LocalizationManager_GetString_m738567075 (Il2CppObject * __this /* static, unused */, String_t* ___id0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (LocalizationManager_GetString_m738567075_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Dictionary_2_t2606186806 * L_0 = ((LocalizationManager_t1078246068_StaticFields*)LocalizationManager_t1078246068_il2cpp_TypeInfo_var->static_fields)->get_strings_0();
		if (L_0)
		{
			goto IL_000f;
		}
	}
	{
		LocalizationManager_Initialize_m1786158525(NULL /*static, unused*/, /*hidden argument*/NULL);
	}

IL_000f:
	{
		String_t* L_1 = ___id0;
		if (!L_1)
		{
			goto IL_0031;
		}
	}
	{
		Dictionary_2_t2606186806 * L_2 = ((LocalizationManager_t1078246068_StaticFields*)LocalizationManager_t1078246068_il2cpp_TypeInfo_var->static_fields)->get_strings_0();
		String_t* L_3 = ___id0;
		NullCheck(L_2);
		bool L_4 = VirtFuncInvoker1< bool, String_t* >::Invoke(19 /* System.Boolean System.Collections.Generic.Dictionary`2<System.String,System.String>::ContainsKey(!0) */, L_2, L_3);
		if (!L_4)
		{
			goto IL_0031;
		}
	}
	{
		Dictionary_2_t2606186806 * L_5 = ((LocalizationManager_t1078246068_StaticFields*)LocalizationManager_t1078246068_il2cpp_TypeInfo_var->static_fields)->get_strings_0();
		String_t* L_6 = ___id0;
		NullCheck(L_5);
		String_t* L_7 = VirtFuncInvoker1< String_t*, String_t* >::Invoke(22 /* !1 System.Collections.Generic.Dictionary`2<System.String,System.String>::get_Item(!0) */, L_5, L_6);
		return L_7;
	}

IL_0031:
	{
		String_t* L_8 = ___id0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_9 = String_Concat_m138640077(NULL /*static, unused*/, _stringLiteral3603947877, L_8, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1588791936_il2cpp_TypeInfo_var);
		Debug_LogWarning_m539478453(NULL /*static, unused*/, L_9, /*hidden argument*/NULL);
		String_t* L_10 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		return L_10;
	}
}
// System.String LocalizationManager::GetEngLocString(System.String)
extern Il2CppClass* LocalizationManager_t1078246068_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const uint32_t LocalizationManager_GetEngLocString_m3629979905_MetadataUsageId;
extern "C"  String_t* LocalizationManager_GetEngLocString_m3629979905 (Il2CppObject * __this /* static, unused */, String_t* ___id0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (LocalizationManager_GetEngLocString_m3629979905_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Dictionary_2_t2606186806 * L_0 = ((LocalizationManager_t1078246068_StaticFields*)LocalizationManager_t1078246068_il2cpp_TypeInfo_var->static_fields)->get_strings_0();
		if (L_0)
		{
			goto IL_000f;
		}
	}
	{
		LocalizationManager_InitializeOnlyEng_m2286298551(NULL /*static, unused*/, /*hidden argument*/NULL);
	}

IL_000f:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_1 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		return L_1;
	}
}
// System.Void LocalizationManager::InitializeOnlyEng()
extern Il2CppClass* Dictionary_2_t2606186806_il2cpp_TypeInfo_var;
extern Il2CppClass* LocalizationManager_t1078246068_il2cpp_TypeInfo_var;
extern Il2CppClass* XmlDocument_t3705263098_il2cpp_TypeInfo_var;
extern Il2CppClass* IEnumerator_t287207039_il2cpp_TypeInfo_var;
extern Il2CppClass* XmlNode_t3592213601_il2cpp_TypeInfo_var;
extern Il2CppClass* IDisposable_t1628921374_il2cpp_TypeInfo_var;
extern const MethodInfo* Dictionary_2__ctor_m640701813_MethodInfo_var;
extern const MethodInfo* Resources_Load_TisTextAsset_t2461560304_m585700060_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral3087975801;
extern Il2CppCodeGenString* _stringLiteral1555550099;
extern Il2CppCodeGenString* _stringLiteral3087975391;
extern Il2CppCodeGenString* _stringLiteral2691209840;
extern Il2CppCodeGenString* _stringLiteral3482961561;
extern Il2CppCodeGenString* _stringLiteral3355;
extern const uint32_t LocalizationManager_InitializeOnlyEng_m2286298551_MetadataUsageId;
extern "C"  void LocalizationManager_InitializeOnlyEng_m2286298551 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (LocalizationManager_InitializeOnlyEng_m2286298551_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	TextAsset_t2461560304 * V_0 = NULL;
	XmlDocument_t3705263098 * V_1 = NULL;
	XmlNode_t3592213601 * V_2 = NULL;
	XmlNode_t3592213601 * V_3 = NULL;
	Il2CppObject * V_4 = NULL;
	String_t* V_5 = NULL;
	int32_t V_6 = 0;
	Il2CppObject * V_7 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Dictionary_2_t2606186806 * L_0 = (Dictionary_2_t2606186806 *)il2cpp_codegen_object_new(Dictionary_2_t2606186806_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m640701813(L_0, /*hidden argument*/Dictionary_2__ctor_m640701813_MethodInfo_var);
		((LocalizationManager_t1078246068_StaticFields*)LocalizationManager_t1078246068_il2cpp_TypeInfo_var->static_fields)->set_strings_0(L_0);
		int32_t L_1 = Application_get_systemLanguage_m1182442618(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_6 = L_1;
		int32_t L_2 = V_6;
		if ((((int32_t)L_2) == ((int32_t)3)))
		{
			goto IL_0030;
		}
	}
	{
		int32_t L_3 = V_6;
		if ((((int32_t)L_3) == ((int32_t)((int32_t)30))))
		{
			goto IL_0030;
		}
	}
	{
		int32_t L_4 = V_6;
		if ((((int32_t)L_4) == ((int32_t)((int32_t)38))))
		{
			goto IL_0030;
		}
	}
	{
		goto IL_004a;
	}

IL_0030:
	{
		TextAsset_t2461560304 * L_5 = Resources_Load_TisTextAsset_t2461560304_m585700060(NULL /*static, unused*/, _stringLiteral3087975801, /*hidden argument*/Resources_Load_TisTextAsset_t2461560304_m585700060_MethodInfo_var);
		V_0 = L_5;
		((LocalizationManager_t1078246068_StaticFields*)LocalizationManager_t1078246068_il2cpp_TypeInfo_var->static_fields)->set_language_1(_stringLiteral1555550099);
		goto IL_0064;
	}

IL_004a:
	{
		TextAsset_t2461560304 * L_6 = Resources_Load_TisTextAsset_t2461560304_m585700060(NULL /*static, unused*/, _stringLiteral3087975391, /*hidden argument*/Resources_Load_TisTextAsset_t2461560304_m585700060_MethodInfo_var);
		V_0 = L_6;
		((LocalizationManager_t1078246068_StaticFields*)LocalizationManager_t1078246068_il2cpp_TypeInfo_var->static_fields)->set_language_1(_stringLiteral2691209840);
		goto IL_0064;
	}

IL_0064:
	{
		TextAsset_t2461560304 * L_7 = Resources_Load_TisTextAsset_t2461560304_m585700060(NULL /*static, unused*/, _stringLiteral3087975391, /*hidden argument*/Resources_Load_TisTextAsset_t2461560304_m585700060_MethodInfo_var);
		V_0 = L_7;
		XmlDocument_t3705263098 * L_8 = (XmlDocument_t3705263098 *)il2cpp_codegen_object_new(XmlDocument_t3705263098_il2cpp_TypeInfo_var);
		XmlDocument__ctor_m467220425(L_8, /*hidden argument*/NULL);
		V_1 = L_8;
		XmlDocument_t3705263098 * L_9 = V_1;
		TextAsset_t2461560304 * L_10 = V_0;
		NullCheck(L_10);
		String_t* L_11 = TextAsset_get_text_m655578209(L_10, /*hidden argument*/NULL);
		NullCheck(L_9);
		VirtActionInvoker1< String_t* >::Invoke(59 /* System.Void System.Xml.XmlDocument::LoadXml(System.String) */, L_9, L_11);
		XmlDocument_t3705263098 * L_12 = V_1;
		NullCheck(L_12);
		XmlNodeList_t3966370975 * L_13 = XmlNode_SelectNodes_m3515755997(L_12, _stringLiteral3482961561, /*hidden argument*/NULL);
		NullCheck(L_13);
		XmlNode_t3592213601 * L_14 = VirtFuncInvoker1< XmlNode_t3592213601 *, int32_t >::Invoke(6 /* System.Xml.XmlNode System.Xml.XmlNodeList::get_ItemOf(System.Int32) */, L_13, 0);
		V_2 = L_14;
		XmlNode_t3592213601 * L_15 = V_2;
		NullCheck(L_15);
		XmlNodeList_t3966370975 * L_16 = VirtFuncInvoker0< XmlNodeList_t3966370975 * >::Invoke(9 /* System.Xml.XmlNodeList System.Xml.XmlNode::get_ChildNodes() */, L_15);
		NullCheck(L_16);
		Il2CppObject * L_17 = VirtFuncInvoker0< Il2CppObject * >::Invoke(7 /* System.Collections.IEnumerator System.Xml.XmlNodeList::GetEnumerator() */, L_16);
		V_4 = L_17;
	}

IL_00a0:
	try
	{ // begin try (depth: 1)
		{
			goto IL_00fe;
		}

IL_00a5:
		{
			Il2CppObject * L_18 = V_4;
			NullCheck(L_18);
			Il2CppObject * L_19 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(0 /* System.Object System.Collections.IEnumerator::get_Current() */, IEnumerator_t287207039_il2cpp_TypeInfo_var, L_18);
			V_3 = ((XmlNode_t3592213601 *)CastclassClass(L_19, XmlNode_t3592213601_il2cpp_TypeInfo_var));
			XmlNode_t3592213601 * L_20 = V_3;
			NullCheck(L_20);
			XmlAttributeCollection_t571717291 * L_21 = VirtFuncInvoker0< XmlAttributeCollection_t571717291 * >::Invoke(6 /* System.Xml.XmlAttributeCollection System.Xml.XmlNode::get_Attributes() */, L_20);
			NullCheck(L_21);
			XmlNode_t3592213601 * L_22 = VirtFuncInvoker1< XmlNode_t3592213601 *, String_t* >::Invoke(7 /* System.Xml.XmlNode System.Xml.XmlNamedNodeMap::GetNamedItem(System.String) */, L_21, _stringLiteral3355);
			NullCheck(L_22);
			String_t* L_23 = VirtFuncInvoker0< String_t* >::Invoke(25 /* System.String System.Xml.XmlNode::get_Value() */, L_22);
			V_5 = L_23;
			Dictionary_2_t2606186806 * L_24 = ((LocalizationManager_t1078246068_StaticFields*)LocalizationManager_t1078246068_il2cpp_TypeInfo_var->static_fields)->get_strings_0();
			String_t* L_25 = V_5;
			NullCheck(L_24);
			bool L_26 = VirtFuncInvoker1< bool, String_t* >::Invoke(19 /* System.Boolean System.Collections.Generic.Dictionary`2<System.String,System.String>::ContainsKey(!0) */, L_24, L_25);
			if (!L_26)
			{
				goto IL_00e7;
			}
		}

IL_00da:
		{
			Dictionary_2_t2606186806 * L_27 = ((LocalizationManager_t1078246068_StaticFields*)LocalizationManager_t1078246068_il2cpp_TypeInfo_var->static_fields)->get_strings_0();
			String_t* L_28 = V_5;
			NullCheck(L_27);
			VirtFuncInvoker1< bool, String_t* >::Invoke(20 /* System.Boolean System.Collections.Generic.Dictionary`2<System.String,System.String>::Remove(!0) */, L_27, L_28);
		}

IL_00e7:
		{
			Dictionary_2_t2606186806 * L_29 = ((LocalizationManager_t1078246068_StaticFields*)LocalizationManager_t1078246068_il2cpp_TypeInfo_var->static_fields)->get_strings_0();
			String_t* L_30 = V_5;
			XmlNode_t3592213601 * L_31 = V_3;
			NullCheck(L_31);
			String_t* L_32 = VirtFuncInvoker0< String_t* >::Invoke(12 /* System.String System.Xml.XmlNode::get_InnerText() */, L_31);
			NullCheck(L_32);
			String_t* L_33 = String_ToString_m1382284457(L_32, /*hidden argument*/NULL);
			NullCheck(L_29);
			VirtActionInvoker2< String_t*, String_t* >::Invoke(18 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.String>::Add(!0,!1) */, L_29, L_30, L_33);
		}

IL_00fe:
		{
			Il2CppObject * L_34 = V_4;
			NullCheck(L_34);
			bool L_35 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t287207039_il2cpp_TypeInfo_var, L_34);
			if (L_35)
			{
				goto IL_00a5;
			}
		}

IL_010a:
		{
			IL2CPP_LEAVE(0x125, FINALLY_010f);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_010f;
	}

FINALLY_010f:
	{ // begin finally (depth: 1)
		{
			Il2CppObject * L_36 = V_4;
			V_7 = ((Il2CppObject *)IsInst(L_36, IDisposable_t1628921374_il2cpp_TypeInfo_var));
			Il2CppObject * L_37 = V_7;
			if (L_37)
			{
				goto IL_011d;
			}
		}

IL_011c:
		{
			IL2CPP_END_FINALLY(271)
		}

IL_011d:
		{
			Il2CppObject * L_38 = V_7;
			NullCheck(L_38);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, L_38);
			IL2CPP_END_FINALLY(271)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(271)
	{
		IL2CPP_JUMP_TBL(0x125, IL_0125)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0125:
	{
		return;
	}
}
// System.Void LocalizedProfitSetter::.ctor()
extern "C"  void LocalizedProfitSetter__ctor_m1492800589 (LocalizedProfitSetter_t3003809502 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m350695606(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void LocalizedProfitSetter::Awake()
extern Il2CppClass* Debug_t1588791936_il2cpp_TypeInfo_var;
extern const MethodInfo* Component_GetComponent_TisTextMesh_t583678247_m2237128948_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral1086616;
extern Il2CppCodeGenString* _stringLiteral3208676;
extern Il2CppCodeGenString* _stringLiteral1679534045;
extern Il2CppCodeGenString* _stringLiteral4227142842;
extern Il2CppCodeGenString* _stringLiteral1080998;
extern Il2CppCodeGenString* _stringLiteral113745;
extern Il2CppCodeGenString* _stringLiteral1056;
extern Il2CppCodeGenString* _stringLiteral38;
extern Il2CppCodeGenString* _stringLiteral4244775916;
extern Il2CppCodeGenString* _stringLiteral3151468;
extern Il2CppCodeGenString* _stringLiteral4258938376;
extern const uint32_t LocalizedProfitSetter_Awake_m1730405808_MetadataUsageId;
extern "C"  void LocalizedProfitSetter_Awake_m1730405808 (LocalizedProfitSetter_t3003809502 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (LocalizedProfitSetter_Awake_m1730405808_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	TextMesh_t583678247 * V_0 = NULL;
	String_t* V_1 = NULL;
	{
		TextMesh_t583678247 * L_0 = Component_GetComponent_TisTextMesh_t583678247_m2237128948(__this, /*hidden argument*/Component_GetComponent_TisTextMesh_t583678247_m2237128948_MethodInfo_var);
		V_0 = L_0;
		TextMesh_t583678247 * L_1 = V_0;
		bool L_2 = Object_op_Implicit_m3627549526(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_008e;
		}
	}
	{
		TextMesh_t583678247 * L_3 = V_0;
		NullCheck(L_3);
		String_t* L_4 = TextMesh_get_text_m2892967978(L_3, /*hidden argument*/NULL);
		V_1 = L_4;
		String_t* L_5 = V_1;
		String_t* L_6 = LocalizationManager_GetString_m738567075(NULL /*static, unused*/, _stringLiteral3208676, /*hidden argument*/NULL);
		NullCheck(L_5);
		String_t* L_7 = String_Replace_m2915759397(L_5, _stringLiteral1086616, L_6, /*hidden argument*/NULL);
		V_1 = L_7;
		String_t* L_8 = V_1;
		String_t* L_9 = LocalizationManager_GetString_m738567075(NULL /*static, unused*/, _stringLiteral4227142842, /*hidden argument*/NULL);
		NullCheck(L_8);
		String_t* L_10 = String_Replace_m2915759397(L_8, _stringLiteral1679534045, L_9, /*hidden argument*/NULL);
		V_1 = L_10;
		String_t* L_11 = V_1;
		String_t* L_12 = LocalizationManager_GetString_m738567075(NULL /*static, unused*/, _stringLiteral113745, /*hidden argument*/NULL);
		NullCheck(L_11);
		String_t* L_13 = String_Replace_m2915759397(L_11, _stringLiteral1080998, L_12, /*hidden argument*/NULL);
		V_1 = L_13;
		String_t* L_14 = V_1;
		NullCheck(L_14);
		String_t* L_15 = String_Replace_m2915759397(L_14, _stringLiteral1056, _stringLiteral38, /*hidden argument*/NULL);
		V_1 = L_15;
		String_t* L_16 = V_1;
		String_t* L_17 = LocalizationManager_GetString_m738567075(NULL /*static, unused*/, _stringLiteral3151468, /*hidden argument*/NULL);
		NullCheck(L_16);
		String_t* L_18 = String_Replace_m2915759397(L_16, _stringLiteral4244775916, L_17, /*hidden argument*/NULL);
		V_1 = L_18;
		TextMesh_t583678247 * L_19 = V_0;
		String_t* L_20 = V_1;
		NullCheck(L_19);
		TextMesh_set_text_m3628430759(L_19, L_20, /*hidden argument*/NULL);
		goto IL_0098;
	}

IL_008e:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1588791936_il2cpp_TypeInfo_var);
		Debug_LogWarning_m539478453(NULL /*static, unused*/, _stringLiteral4258938376, /*hidden argument*/NULL);
	}

IL_0098:
	{
		return;
	}
}
// System.Void LocalizedSpriteSetter::.ctor()
extern "C"  void LocalizedSpriteSetter__ctor_m2784419244 (LocalizedSpriteSetter_t2536447647 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m350695606(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void LocalizedSpriteSetter::Awake()
extern const MethodInfo* Component_GetComponent_TisSpriteRenderer_t2223784725_m4090179846_MethodInfo_var;
extern const uint32_t LocalizedSpriteSetter_Awake_m3022024463_MetadataUsageId;
extern "C"  void LocalizedSpriteSetter_Awake_m3022024463 (LocalizedSpriteSetter_t2536447647 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (LocalizedSpriteSetter_Awake_m3022024463_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	SpriteRenderer_t2223784725 * V_0 = NULL;
	int32_t V_1 = 0;
	{
		SpriteRenderer_t2223784725 * L_0 = Component_GetComponent_TisSpriteRenderer_t2223784725_m4090179846(__this, /*hidden argument*/Component_GetComponent_TisSpriteRenderer_t2223784725_m4090179846_MethodInfo_var);
		V_0 = L_0;
		SpriteRenderer_t2223784725 * L_1 = V_0;
		bool L_2 = Object_op_Implicit_m3627549526(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0056;
		}
	}
	{
		int32_t L_3 = Application_get_systemLanguage_m1182442618(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_1 = L_3;
		int32_t L_4 = V_1;
		if ((((int32_t)L_4) == ((int32_t)3)))
		{
			goto IL_0034;
		}
	}
	{
		int32_t L_5 = V_1;
		if ((((int32_t)L_5) == ((int32_t)((int32_t)30))))
		{
			goto IL_0034;
		}
	}
	{
		int32_t L_6 = V_1;
		if ((((int32_t)L_6) == ((int32_t)((int32_t)38))))
		{
			goto IL_0034;
		}
	}
	{
		goto IL_0045;
	}

IL_0034:
	{
		SpriteRenderer_t2223784725 * L_7 = V_0;
		Sprite_t4006040370 * L_8 = __this->get_rusSprite_3();
		NullCheck(L_7);
		SpriteRenderer_set_sprite_m1519408453(L_7, L_8, /*hidden argument*/NULL);
		goto IL_0056;
	}

IL_0045:
	{
		SpriteRenderer_t2223784725 * L_9 = V_0;
		Sprite_t4006040370 * L_10 = __this->get_engSprite_2();
		NullCheck(L_9);
		SpriteRenderer_set_sprite_m1519408453(L_9, L_10, /*hidden argument*/NULL);
		goto IL_0056;
	}

IL_0056:
	{
		return;
	}
}
// System.Void LocalizedTextSetter::.ctor()
extern "C"  void LocalizedTextSetter__ctor_m2056662404 (LocalizedTextSetter_t1100117447 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m350695606(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void LocalizedTextSetter::Awake()
extern Il2CppClass* Utils_t82059409_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t1588791936_il2cpp_TypeInfo_var;
extern const MethodInfo* Component_GetComponent_TisTextMesh_t583678247_m2237128948_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral4258938376;
extern const uint32_t LocalizedTextSetter_Awake_m2294267623_MetadataUsageId;
extern "C"  void LocalizedTextSetter_Awake_m2294267623 (LocalizedTextSetter_t1100117447 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (LocalizedTextSetter_Awake_m2294267623_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	TextMesh_t583678247 * V_0 = NULL;
	{
		TextMesh_t583678247 * L_0 = Component_GetComponent_TisTextMesh_t583678247_m2237128948(__this, /*hidden argument*/Component_GetComponent_TisTextMesh_t583678247_m2237128948_MethodInfo_var);
		V_0 = L_0;
		TextMesh_t583678247 * L_1 = V_0;
		bool L_2 = Object_op_Implicit_m3627549526(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0055;
		}
	}
	{
		int32_t L_3 = __this->get_stringLength_3();
		if ((((int32_t)L_3) <= ((int32_t)0)))
		{
			goto IL_003f;
		}
	}
	{
		TextMesh_t583678247 * L_4 = V_0;
		String_t* L_5 = __this->get_stringID_2();
		String_t* L_6 = LocalizationManager_GetString_m738567075(NULL /*static, unused*/, L_5, /*hidden argument*/NULL);
		int32_t L_7 = __this->get_stringLength_3();
		IL2CPP_RUNTIME_CLASS_INIT(Utils_t82059409_il2cpp_TypeInfo_var);
		String_t* L_8 = Utils_FormatString_m1390523396(NULL /*static, unused*/, L_6, L_7, /*hidden argument*/NULL);
		NullCheck(L_4);
		TextMesh_set_text_m3628430759(L_4, L_8, /*hidden argument*/NULL);
		goto IL_0050;
	}

IL_003f:
	{
		TextMesh_t583678247 * L_9 = V_0;
		String_t* L_10 = __this->get_stringID_2();
		String_t* L_11 = LocalizationManager_GetString_m738567075(NULL /*static, unused*/, L_10, /*hidden argument*/NULL);
		NullCheck(L_9);
		TextMesh_set_text_m3628430759(L_9, L_11, /*hidden argument*/NULL);
	}

IL_0050:
	{
		goto IL_005f;
	}

IL_0055:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1588791936_il2cpp_TypeInfo_var);
		Debug_LogWarning_m539478453(NULL /*static, unused*/, _stringLiteral4258938376, /*hidden argument*/NULL);
	}

IL_005f:
	{
		return;
	}
}
// System.Void Localizer::.ctor()
extern "C"  void Localizer__ctor_m1753479202 (Localizer_t799400809 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m350695606(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Localizer::Awake()
extern Il2CppClass* Debug_t1588791936_il2cpp_TypeInfo_var;
extern const MethodInfo* Component_GetComponent_TisTextMesh_t583678247_m2237128948_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral4258938376;
extern const uint32_t Localizer_Awake_m1991084421_MetadataUsageId;
extern "C"  void Localizer_Awake_m1991084421 (Localizer_t799400809 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Localizer_Awake_m1991084421_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	TextMesh_t583678247 * V_0 = NULL;
	{
		TextMesh_t583678247 * L_0 = Component_GetComponent_TisTextMesh_t583678247_m2237128948(__this, /*hidden argument*/Component_GetComponent_TisTextMesh_t583678247_m2237128948_MethodInfo_var);
		V_0 = L_0;
		TextMesh_t583678247 * L_1 = V_0;
		bool L_2 = Object_op_Implicit_m3627549526(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0028;
		}
	}
	{
		TextMesh_t583678247 * L_3 = V_0;
		String_t* L_4 = __this->get_stringId_2();
		String_t* L_5 = LocalizationManager_GetString_m738567075(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		NullCheck(L_3);
		TextMesh_set_text_m3628430759(L_3, L_5, /*hidden argument*/NULL);
		goto IL_0032;
	}

IL_0028:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1588791936_il2cpp_TypeInfo_var);
		Debug_LogWarning_m539478453(NULL /*static, unused*/, _stringLiteral4258938376, /*hidden argument*/NULL);
	}

IL_0032:
	{
		return;
	}
}
// System.Void Localizer::Update()
extern "C"  void Localizer_Update_m250142507 (Localizer_t799400809 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void LocalNotificationManager::.ctor()
extern "C"  void LocalNotificationManager__ctor_m3220561284 (LocalNotificationManager_t980457207 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// INotificationManager LocalNotificationManager::get_manager()
extern Il2CppClass* LocalNotificationManager_t980457207_il2cpp_TypeInfo_var;
extern Il2CppClass* IOSNotificationManager_t324309781_il2cpp_TypeInfo_var;
extern const uint32_t LocalNotificationManager_get_manager_m1297565986_MetadataUsageId;
extern "C"  Il2CppObject * LocalNotificationManager_get_manager_m1297565986 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (LocalNotificationManager_get_manager_m1297565986_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject * L_0 = ((LocalNotificationManager_t980457207_StaticFields*)LocalNotificationManager_t980457207_il2cpp_TypeInfo_var->static_fields)->get__manager_0();
		if (L_0)
		{
			goto IL_0014;
		}
	}
	{
		IOSNotificationManager_t324309781 * L_1 = (IOSNotificationManager_t324309781 *)il2cpp_codegen_object_new(IOSNotificationManager_t324309781_il2cpp_TypeInfo_var);
		IOSNotificationManager__ctor_m1076054310(L_1, /*hidden argument*/NULL);
		((LocalNotificationManager_t980457207_StaticFields*)LocalNotificationManager_t980457207_il2cpp_TypeInfo_var->static_fields)->set__manager_0(L_1);
	}

IL_0014:
	{
		Il2CppObject * L_2 = ((LocalNotificationManager_t980457207_StaticFields*)LocalNotificationManager_t980457207_il2cpp_TypeInfo_var->static_fields)->get__manager_0();
		return L_2;
	}
}
// System.Void LocalNotificationManager::SetNotification(System.String,System.Int32,System.String)
extern Il2CppClass* INotificationManager_t3349952825_il2cpp_TypeInfo_var;
extern const uint32_t LocalNotificationManager_SetNotification_m388117024_MetadataUsageId;
extern "C"  void LocalNotificationManager_SetNotification_m388117024 (Il2CppObject * __this /* static, unused */, String_t* ___message0, int32_t ___delay1, String_t* ___title2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (LocalNotificationManager_SetNotification_m388117024_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject * L_0 = LocalNotificationManager_get_manager_m1297565986(NULL /*static, unused*/, /*hidden argument*/NULL);
		String_t* L_1 = ___message0;
		int32_t L_2 = ___delay1;
		String_t* L_3 = ___title2;
		NullCheck(L_0);
		InterfaceActionInvoker3< String_t*, int32_t, String_t* >::Invoke(0 /* System.Void INotificationManager::SetNotification(System.String,System.Int32,System.String) */, INotificationManager_t3349952825_il2cpp_TypeInfo_var, L_0, L_1, L_2, L_3);
		return;
	}
}
// System.Void LocalNotificationManager::CancelAllNotification()
extern Il2CppClass* INotificationManager_t3349952825_il2cpp_TypeInfo_var;
extern const uint32_t LocalNotificationManager_CancelAllNotification_m2926561140_MetadataUsageId;
extern "C"  void LocalNotificationManager_CancelAllNotification_m2926561140 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (LocalNotificationManager_CancelAllNotification_m2926561140_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject * L_0 = LocalNotificationManager_get_manager_m1297565986(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		InterfaceActionInvoker0::Invoke(1 /* System.Void INotificationManager::CancelAllNotification() */, INotificationManager_t3349952825_il2cpp_TypeInfo_var, L_0);
		return;
	}
}
// System.Void LocalyticsUnity.Localytics::.ctor()
extern "C"  void Localytics__ctor_m604636892 (Localytics_t1579915497 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void LocalyticsUnity.Localytics::add_OnLocalyticsDidTagEvent(LocalyticsUnity.Localytics/LocalyticsDidTagEvent)
extern Il2CppClass* Localytics_t1579915497_il2cpp_TypeInfo_var;
extern Il2CppClass* LocalyticsDidTagEvent_t1639420300_il2cpp_TypeInfo_var;
extern const uint32_t Localytics_add_OnLocalyticsDidTagEvent_m2904114765_MetadataUsageId;
extern "C"  void Localytics_add_OnLocalyticsDidTagEvent_m2904114765 (Il2CppObject * __this /* static, unused */, LocalyticsDidTagEvent_t1639420300 * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Localytics_add_OnLocalyticsDidTagEvent_m2904114765_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		LocalyticsDidTagEvent_t1639420300 * L_0 = ((Localytics_t1579915497_StaticFields*)Localytics_t1579915497_il2cpp_TypeInfo_var->static_fields)->get_OnLocalyticsDidTagEvent_0();
		LocalyticsDidTagEvent_t1639420300 * L_1 = ___value0;
		Delegate_t3660574010 * L_2 = Delegate_Combine_m1842362874(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		((Localytics_t1579915497_StaticFields*)Localytics_t1579915497_il2cpp_TypeInfo_var->static_fields)->set_OnLocalyticsDidTagEvent_0(((LocalyticsDidTagEvent_t1639420300 *)CastclassSealed(L_2, LocalyticsDidTagEvent_t1639420300_il2cpp_TypeInfo_var)));
		return;
	}
}
// System.Void LocalyticsUnity.Localytics::remove_OnLocalyticsDidTagEvent(LocalyticsUnity.Localytics/LocalyticsDidTagEvent)
extern Il2CppClass* Localytics_t1579915497_il2cpp_TypeInfo_var;
extern Il2CppClass* LocalyticsDidTagEvent_t1639420300_il2cpp_TypeInfo_var;
extern const uint32_t Localytics_remove_OnLocalyticsDidTagEvent_m3611443998_MetadataUsageId;
extern "C"  void Localytics_remove_OnLocalyticsDidTagEvent_m3611443998 (Il2CppObject * __this /* static, unused */, LocalyticsDidTagEvent_t1639420300 * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Localytics_remove_OnLocalyticsDidTagEvent_m3611443998_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		LocalyticsDidTagEvent_t1639420300 * L_0 = ((Localytics_t1579915497_StaticFields*)Localytics_t1579915497_il2cpp_TypeInfo_var->static_fields)->get_OnLocalyticsDidTagEvent_0();
		LocalyticsDidTagEvent_t1639420300 * L_1 = ___value0;
		Delegate_t3660574010 * L_2 = Delegate_Remove_m3898886541(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		((Localytics_t1579915497_StaticFields*)Localytics_t1579915497_il2cpp_TypeInfo_var->static_fields)->set_OnLocalyticsDidTagEvent_0(((LocalyticsDidTagEvent_t1639420300 *)CastclassSealed(L_2, LocalyticsDidTagEvent_t1639420300_il2cpp_TypeInfo_var)));
		return;
	}
}
// System.Void LocalyticsUnity.Localytics::add_OnLocalyticsSessionDidOpen(LocalyticsUnity.Localytics/LocalyticsSessionDidOpen)
extern Il2CppClass* Localytics_t1579915497_il2cpp_TypeInfo_var;
extern Il2CppClass* LocalyticsSessionDidOpen_t115241990_il2cpp_TypeInfo_var;
extern const uint32_t Localytics_add_OnLocalyticsSessionDidOpen_m3672933809_MetadataUsageId;
extern "C"  void Localytics_add_OnLocalyticsSessionDidOpen_m3672933809 (Il2CppObject * __this /* static, unused */, LocalyticsSessionDidOpen_t115241990 * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Localytics_add_OnLocalyticsSessionDidOpen_m3672933809_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		LocalyticsSessionDidOpen_t115241990 * L_0 = ((Localytics_t1579915497_StaticFields*)Localytics_t1579915497_il2cpp_TypeInfo_var->static_fields)->get_OnLocalyticsSessionDidOpen_1();
		LocalyticsSessionDidOpen_t115241990 * L_1 = ___value0;
		Delegate_t3660574010 * L_2 = Delegate_Combine_m1842362874(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		((Localytics_t1579915497_StaticFields*)Localytics_t1579915497_il2cpp_TypeInfo_var->static_fields)->set_OnLocalyticsSessionDidOpen_1(((LocalyticsSessionDidOpen_t115241990 *)CastclassSealed(L_2, LocalyticsSessionDidOpen_t115241990_il2cpp_TypeInfo_var)));
		return;
	}
}
// System.Void LocalyticsUnity.Localytics::remove_OnLocalyticsSessionDidOpen(LocalyticsUnity.Localytics/LocalyticsSessionDidOpen)
extern Il2CppClass* Localytics_t1579915497_il2cpp_TypeInfo_var;
extern Il2CppClass* LocalyticsSessionDidOpen_t115241990_il2cpp_TypeInfo_var;
extern const uint32_t Localytics_remove_OnLocalyticsSessionDidOpen_m2573132226_MetadataUsageId;
extern "C"  void Localytics_remove_OnLocalyticsSessionDidOpen_m2573132226 (Il2CppObject * __this /* static, unused */, LocalyticsSessionDidOpen_t115241990 * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Localytics_remove_OnLocalyticsSessionDidOpen_m2573132226_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		LocalyticsSessionDidOpen_t115241990 * L_0 = ((Localytics_t1579915497_StaticFields*)Localytics_t1579915497_il2cpp_TypeInfo_var->static_fields)->get_OnLocalyticsSessionDidOpen_1();
		LocalyticsSessionDidOpen_t115241990 * L_1 = ___value0;
		Delegate_t3660574010 * L_2 = Delegate_Remove_m3898886541(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		((Localytics_t1579915497_StaticFields*)Localytics_t1579915497_il2cpp_TypeInfo_var->static_fields)->set_OnLocalyticsSessionDidOpen_1(((LocalyticsSessionDidOpen_t115241990 *)CastclassSealed(L_2, LocalyticsSessionDidOpen_t115241990_il2cpp_TypeInfo_var)));
		return;
	}
}
// System.Void LocalyticsUnity.Localytics::add_OnLocalyticsSessionWillClose(LocalyticsUnity.Localytics/LocalyticsSessionWillClose)
extern Il2CppClass* Localytics_t1579915497_il2cpp_TypeInfo_var;
extern Il2CppClass* LocalyticsSessionWillClose_t2690468515_il2cpp_TypeInfo_var;
extern const uint32_t Localytics_add_OnLocalyticsSessionWillClose_m419650807_MetadataUsageId;
extern "C"  void Localytics_add_OnLocalyticsSessionWillClose_m419650807 (Il2CppObject * __this /* static, unused */, LocalyticsSessionWillClose_t2690468515 * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Localytics_add_OnLocalyticsSessionWillClose_m419650807_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		LocalyticsSessionWillClose_t2690468515 * L_0 = ((Localytics_t1579915497_StaticFields*)Localytics_t1579915497_il2cpp_TypeInfo_var->static_fields)->get_OnLocalyticsSessionWillClose_2();
		LocalyticsSessionWillClose_t2690468515 * L_1 = ___value0;
		Delegate_t3660574010 * L_2 = Delegate_Combine_m1842362874(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		((Localytics_t1579915497_StaticFields*)Localytics_t1579915497_il2cpp_TypeInfo_var->static_fields)->set_OnLocalyticsSessionWillClose_2(((LocalyticsSessionWillClose_t2690468515 *)CastclassSealed(L_2, LocalyticsSessionWillClose_t2690468515_il2cpp_TypeInfo_var)));
		return;
	}
}
// System.Void LocalyticsUnity.Localytics::remove_OnLocalyticsSessionWillClose(LocalyticsUnity.Localytics/LocalyticsSessionWillClose)
extern Il2CppClass* Localytics_t1579915497_il2cpp_TypeInfo_var;
extern Il2CppClass* LocalyticsSessionWillClose_t2690468515_il2cpp_TypeInfo_var;
extern const uint32_t Localytics_remove_OnLocalyticsSessionWillClose_m1607944328_MetadataUsageId;
extern "C"  void Localytics_remove_OnLocalyticsSessionWillClose_m1607944328 (Il2CppObject * __this /* static, unused */, LocalyticsSessionWillClose_t2690468515 * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Localytics_remove_OnLocalyticsSessionWillClose_m1607944328_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		LocalyticsSessionWillClose_t2690468515 * L_0 = ((Localytics_t1579915497_StaticFields*)Localytics_t1579915497_il2cpp_TypeInfo_var->static_fields)->get_OnLocalyticsSessionWillClose_2();
		LocalyticsSessionWillClose_t2690468515 * L_1 = ___value0;
		Delegate_t3660574010 * L_2 = Delegate_Remove_m3898886541(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		((Localytics_t1579915497_StaticFields*)Localytics_t1579915497_il2cpp_TypeInfo_var->static_fields)->set_OnLocalyticsSessionWillClose_2(((LocalyticsSessionWillClose_t2690468515 *)CastclassSealed(L_2, LocalyticsSessionWillClose_t2690468515_il2cpp_TypeInfo_var)));
		return;
	}
}
// System.Void LocalyticsUnity.Localytics::add_OnLocalyticsSessionWillOpen(LocalyticsUnity.Localytics/LocalyticsSessionWillOpen)
extern Il2CppClass* Localytics_t1579915497_il2cpp_TypeInfo_var;
extern Il2CppClass* LocalyticsSessionWillOpen_t2581002303_il2cpp_TypeInfo_var;
extern const uint32_t Localytics_add_OnLocalyticsSessionWillOpen_m1951830637_MetadataUsageId;
extern "C"  void Localytics_add_OnLocalyticsSessionWillOpen_m1951830637 (Il2CppObject * __this /* static, unused */, LocalyticsSessionWillOpen_t2581002303 * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Localytics_add_OnLocalyticsSessionWillOpen_m1951830637_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		LocalyticsSessionWillOpen_t2581002303 * L_0 = ((Localytics_t1579915497_StaticFields*)Localytics_t1579915497_il2cpp_TypeInfo_var->static_fields)->get_OnLocalyticsSessionWillOpen_3();
		LocalyticsSessionWillOpen_t2581002303 * L_1 = ___value0;
		Delegate_t3660574010 * L_2 = Delegate_Combine_m1842362874(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		((Localytics_t1579915497_StaticFields*)Localytics_t1579915497_il2cpp_TypeInfo_var->static_fields)->set_OnLocalyticsSessionWillOpen_3(((LocalyticsSessionWillOpen_t2581002303 *)CastclassSealed(L_2, LocalyticsSessionWillOpen_t2581002303_il2cpp_TypeInfo_var)));
		return;
	}
}
// System.Void LocalyticsUnity.Localytics::remove_OnLocalyticsSessionWillOpen(LocalyticsUnity.Localytics/LocalyticsSessionWillOpen)
extern Il2CppClass* Localytics_t1579915497_il2cpp_TypeInfo_var;
extern Il2CppClass* LocalyticsSessionWillOpen_t2581002303_il2cpp_TypeInfo_var;
extern const uint32_t Localytics_remove_OnLocalyticsSessionWillOpen_m1604464190_MetadataUsageId;
extern "C"  void Localytics_remove_OnLocalyticsSessionWillOpen_m1604464190 (Il2CppObject * __this /* static, unused */, LocalyticsSessionWillOpen_t2581002303 * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Localytics_remove_OnLocalyticsSessionWillOpen_m1604464190_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		LocalyticsSessionWillOpen_t2581002303 * L_0 = ((Localytics_t1579915497_StaticFields*)Localytics_t1579915497_il2cpp_TypeInfo_var->static_fields)->get_OnLocalyticsSessionWillOpen_3();
		LocalyticsSessionWillOpen_t2581002303 * L_1 = ___value0;
		Delegate_t3660574010 * L_2 = Delegate_Remove_m3898886541(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		((Localytics_t1579915497_StaticFields*)Localytics_t1579915497_il2cpp_TypeInfo_var->static_fields)->set_OnLocalyticsSessionWillOpen_3(((LocalyticsSessionWillOpen_t2581002303 *)CastclassSealed(L_2, LocalyticsSessionWillOpen_t2581002303_il2cpp_TypeInfo_var)));
		return;
	}
}
// System.Void LocalyticsUnity.Localytics::add_OnLocalyticsDidDismissInAppMessage(LocalyticsUnity.Localytics/LocalyticsDidDismissInAppMessage)
extern Il2CppClass* Localytics_t1579915497_il2cpp_TypeInfo_var;
extern Il2CppClass* LocalyticsDidDismissInAppMessage_t1872466793_il2cpp_TypeInfo_var;
extern const uint32_t Localytics_add_OnLocalyticsDidDismissInAppMessage_m652407851_MetadataUsageId;
extern "C"  void Localytics_add_OnLocalyticsDidDismissInAppMessage_m652407851 (Il2CppObject * __this /* static, unused */, LocalyticsDidDismissInAppMessage_t1872466793 * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Localytics_add_OnLocalyticsDidDismissInAppMessage_m652407851_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		LocalyticsDidDismissInAppMessage_t1872466793 * L_0 = ((Localytics_t1579915497_StaticFields*)Localytics_t1579915497_il2cpp_TypeInfo_var->static_fields)->get_OnLocalyticsDidDismissInAppMessage_4();
		LocalyticsDidDismissInAppMessage_t1872466793 * L_1 = ___value0;
		Delegate_t3660574010 * L_2 = Delegate_Combine_m1842362874(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		((Localytics_t1579915497_StaticFields*)Localytics_t1579915497_il2cpp_TypeInfo_var->static_fields)->set_OnLocalyticsDidDismissInAppMessage_4(((LocalyticsDidDismissInAppMessage_t1872466793 *)CastclassSealed(L_2, LocalyticsDidDismissInAppMessage_t1872466793_il2cpp_TypeInfo_var)));
		return;
	}
}
// System.Void LocalyticsUnity.Localytics::remove_OnLocalyticsDidDismissInAppMessage(LocalyticsUnity.Localytics/LocalyticsDidDismissInAppMessage)
extern Il2CppClass* Localytics_t1579915497_il2cpp_TypeInfo_var;
extern Il2CppClass* LocalyticsDidDismissInAppMessage_t1872466793_il2cpp_TypeInfo_var;
extern const uint32_t Localytics_remove_OnLocalyticsDidDismissInAppMessage_m677711420_MetadataUsageId;
extern "C"  void Localytics_remove_OnLocalyticsDidDismissInAppMessage_m677711420 (Il2CppObject * __this /* static, unused */, LocalyticsDidDismissInAppMessage_t1872466793 * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Localytics_remove_OnLocalyticsDidDismissInAppMessage_m677711420_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		LocalyticsDidDismissInAppMessage_t1872466793 * L_0 = ((Localytics_t1579915497_StaticFields*)Localytics_t1579915497_il2cpp_TypeInfo_var->static_fields)->get_OnLocalyticsDidDismissInAppMessage_4();
		LocalyticsDidDismissInAppMessage_t1872466793 * L_1 = ___value0;
		Delegate_t3660574010 * L_2 = Delegate_Remove_m3898886541(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		((Localytics_t1579915497_StaticFields*)Localytics_t1579915497_il2cpp_TypeInfo_var->static_fields)->set_OnLocalyticsDidDismissInAppMessage_4(((LocalyticsDidDismissInAppMessage_t1872466793 *)CastclassSealed(L_2, LocalyticsDidDismissInAppMessage_t1872466793_il2cpp_TypeInfo_var)));
		return;
	}
}
// System.Void LocalyticsUnity.Localytics::add_OnLocalyticsDidDisplayInAppMessage(LocalyticsUnity.Localytics/LocalyticsDidDisplayInAppMessage)
extern Il2CppClass* Localytics_t1579915497_il2cpp_TypeInfo_var;
extern Il2CppClass* LocalyticsDidDisplayInAppMessage_t3613222305_il2cpp_TypeInfo_var;
extern const uint32_t Localytics_add_OnLocalyticsDidDisplayInAppMessage_m2087928251_MetadataUsageId;
extern "C"  void Localytics_add_OnLocalyticsDidDisplayInAppMessage_m2087928251 (Il2CppObject * __this /* static, unused */, LocalyticsDidDisplayInAppMessage_t3613222305 * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Localytics_add_OnLocalyticsDidDisplayInAppMessage_m2087928251_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		LocalyticsDidDisplayInAppMessage_t3613222305 * L_0 = ((Localytics_t1579915497_StaticFields*)Localytics_t1579915497_il2cpp_TypeInfo_var->static_fields)->get_OnLocalyticsDidDisplayInAppMessage_5();
		LocalyticsDidDisplayInAppMessage_t3613222305 * L_1 = ___value0;
		Delegate_t3660574010 * L_2 = Delegate_Combine_m1842362874(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		((Localytics_t1579915497_StaticFields*)Localytics_t1579915497_il2cpp_TypeInfo_var->static_fields)->set_OnLocalyticsDidDisplayInAppMessage_5(((LocalyticsDidDisplayInAppMessage_t3613222305 *)CastclassSealed(L_2, LocalyticsDidDisplayInAppMessage_t3613222305_il2cpp_TypeInfo_var)));
		return;
	}
}
// System.Void LocalyticsUnity.Localytics::remove_OnLocalyticsDidDisplayInAppMessage(LocalyticsUnity.Localytics/LocalyticsDidDisplayInAppMessage)
extern Il2CppClass* Localytics_t1579915497_il2cpp_TypeInfo_var;
extern Il2CppClass* LocalyticsDidDisplayInAppMessage_t3613222305_il2cpp_TypeInfo_var;
extern const uint32_t Localytics_remove_OnLocalyticsDidDisplayInAppMessage_m2113231820_MetadataUsageId;
extern "C"  void Localytics_remove_OnLocalyticsDidDisplayInAppMessage_m2113231820 (Il2CppObject * __this /* static, unused */, LocalyticsDidDisplayInAppMessage_t3613222305 * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Localytics_remove_OnLocalyticsDidDisplayInAppMessage_m2113231820_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		LocalyticsDidDisplayInAppMessage_t3613222305 * L_0 = ((Localytics_t1579915497_StaticFields*)Localytics_t1579915497_il2cpp_TypeInfo_var->static_fields)->get_OnLocalyticsDidDisplayInAppMessage_5();
		LocalyticsDidDisplayInAppMessage_t3613222305 * L_1 = ___value0;
		Delegate_t3660574010 * L_2 = Delegate_Remove_m3898886541(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		((Localytics_t1579915497_StaticFields*)Localytics_t1579915497_il2cpp_TypeInfo_var->static_fields)->set_OnLocalyticsDidDisplayInAppMessage_5(((LocalyticsDidDisplayInAppMessage_t3613222305 *)CastclassSealed(L_2, LocalyticsDidDisplayInAppMessage_t3613222305_il2cpp_TypeInfo_var)));
		return;
	}
}
// System.Void LocalyticsUnity.Localytics::add_OnLocalyticsWillDismissInAppMessage(LocalyticsUnity.Localytics/LocalyticsWillDismissInAppMessage)
extern Il2CppClass* Localytics_t1579915497_il2cpp_TypeInfo_var;
extern Il2CppClass* LocalyticsWillDismissInAppMessage_t2580912496_il2cpp_TypeInfo_var;
extern const uint32_t Localytics_add_OnLocalyticsWillDismissInAppMessage_m2235619789_MetadataUsageId;
extern "C"  void Localytics_add_OnLocalyticsWillDismissInAppMessage_m2235619789 (Il2CppObject * __this /* static, unused */, LocalyticsWillDismissInAppMessage_t2580912496 * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Localytics_add_OnLocalyticsWillDismissInAppMessage_m2235619789_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		LocalyticsWillDismissInAppMessage_t2580912496 * L_0 = ((Localytics_t1579915497_StaticFields*)Localytics_t1579915497_il2cpp_TypeInfo_var->static_fields)->get_OnLocalyticsWillDismissInAppMessage_6();
		LocalyticsWillDismissInAppMessage_t2580912496 * L_1 = ___value0;
		Delegate_t3660574010 * L_2 = Delegate_Combine_m1842362874(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		((Localytics_t1579915497_StaticFields*)Localytics_t1579915497_il2cpp_TypeInfo_var->static_fields)->set_OnLocalyticsWillDismissInAppMessage_6(((LocalyticsWillDismissInAppMessage_t2580912496 *)CastclassSealed(L_2, LocalyticsWillDismissInAppMessage_t2580912496_il2cpp_TypeInfo_var)));
		return;
	}
}
// System.Void LocalyticsUnity.Localytics::remove_OnLocalyticsWillDismissInAppMessage(LocalyticsUnity.Localytics/LocalyticsWillDismissInAppMessage)
extern Il2CppClass* Localytics_t1579915497_il2cpp_TypeInfo_var;
extern Il2CppClass* LocalyticsWillDismissInAppMessage_t2580912496_il2cpp_TypeInfo_var;
extern const uint32_t Localytics_remove_OnLocalyticsWillDismissInAppMessage_m782545822_MetadataUsageId;
extern "C"  void Localytics_remove_OnLocalyticsWillDismissInAppMessage_m782545822 (Il2CppObject * __this /* static, unused */, LocalyticsWillDismissInAppMessage_t2580912496 * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Localytics_remove_OnLocalyticsWillDismissInAppMessage_m782545822_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		LocalyticsWillDismissInAppMessage_t2580912496 * L_0 = ((Localytics_t1579915497_StaticFields*)Localytics_t1579915497_il2cpp_TypeInfo_var->static_fields)->get_OnLocalyticsWillDismissInAppMessage_6();
		LocalyticsWillDismissInAppMessage_t2580912496 * L_1 = ___value0;
		Delegate_t3660574010 * L_2 = Delegate_Remove_m3898886541(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		((Localytics_t1579915497_StaticFields*)Localytics_t1579915497_il2cpp_TypeInfo_var->static_fields)->set_OnLocalyticsWillDismissInAppMessage_6(((LocalyticsWillDismissInAppMessage_t2580912496 *)CastclassSealed(L_2, LocalyticsWillDismissInAppMessage_t2580912496_il2cpp_TypeInfo_var)));
		return;
	}
}
// System.Void LocalyticsUnity.Localytics::add_OnLocalyticsWillDisplayInAppMessage(LocalyticsUnity.Localytics/LocalyticsWillDisplayInAppMessage)
extern Il2CppClass* Localytics_t1579915497_il2cpp_TypeInfo_var;
extern Il2CppClass* LocalyticsWillDisplayInAppMessage_t26700712_il2cpp_TypeInfo_var;
extern const uint32_t Localytics_add_OnLocalyticsWillDisplayInAppMessage_m4087123661_MetadataUsageId;
extern "C"  void Localytics_add_OnLocalyticsWillDisplayInAppMessage_m4087123661 (Il2CppObject * __this /* static, unused */, LocalyticsWillDisplayInAppMessage_t26700712 * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Localytics_add_OnLocalyticsWillDisplayInAppMessage_m4087123661_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		LocalyticsWillDisplayInAppMessage_t26700712 * L_0 = ((Localytics_t1579915497_StaticFields*)Localytics_t1579915497_il2cpp_TypeInfo_var->static_fields)->get_OnLocalyticsWillDisplayInAppMessage_7();
		LocalyticsWillDisplayInAppMessage_t26700712 * L_1 = ___value0;
		Delegate_t3660574010 * L_2 = Delegate_Combine_m1842362874(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		((Localytics_t1579915497_StaticFields*)Localytics_t1579915497_il2cpp_TypeInfo_var->static_fields)->set_OnLocalyticsWillDisplayInAppMessage_7(((LocalyticsWillDisplayInAppMessage_t26700712 *)CastclassSealed(L_2, LocalyticsWillDisplayInAppMessage_t26700712_il2cpp_TypeInfo_var)));
		return;
	}
}
// System.Void LocalyticsUnity.Localytics::remove_OnLocalyticsWillDisplayInAppMessage(LocalyticsUnity.Localytics/LocalyticsWillDisplayInAppMessage)
extern Il2CppClass* Localytics_t1579915497_il2cpp_TypeInfo_var;
extern Il2CppClass* LocalyticsWillDisplayInAppMessage_t26700712_il2cpp_TypeInfo_var;
extern const uint32_t Localytics_remove_OnLocalyticsWillDisplayInAppMessage_m2634049694_MetadataUsageId;
extern "C"  void Localytics_remove_OnLocalyticsWillDisplayInAppMessage_m2634049694 (Il2CppObject * __this /* static, unused */, LocalyticsWillDisplayInAppMessage_t26700712 * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Localytics_remove_OnLocalyticsWillDisplayInAppMessage_m2634049694_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		LocalyticsWillDisplayInAppMessage_t26700712 * L_0 = ((Localytics_t1579915497_StaticFields*)Localytics_t1579915497_il2cpp_TypeInfo_var->static_fields)->get_OnLocalyticsWillDisplayInAppMessage_7();
		LocalyticsWillDisplayInAppMessage_t26700712 * L_1 = ___value0;
		Delegate_t3660574010 * L_2 = Delegate_Remove_m3898886541(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		((Localytics_t1579915497_StaticFields*)Localytics_t1579915497_il2cpp_TypeInfo_var->static_fields)->set_OnLocalyticsWillDisplayInAppMessage_7(((LocalyticsWillDisplayInAppMessage_t26700712 *)CastclassSealed(L_2, LocalyticsWillDisplayInAppMessage_t26700712_il2cpp_TypeInfo_var)));
		return;
	}
}
// System.String LocalyticsUnity.Localytics::get_AnalyticsHost()
extern "C"  String_t* Localytics_get_AnalyticsHost_m1979565532 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		String_t* L_0 = Localytics__analyticsHost_m3387637818(NULL /*static, unused*/, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Void LocalyticsUnity.Localytics::set_AnalyticsHost(System.String)
extern "C"  void Localytics_set_AnalyticsHost_m3145765239 (Il2CppObject * __this /* static, unused */, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		Localytics__setAnalyticsHost_m2514892765(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.String LocalyticsUnity.Localytics::get_AppKey()
extern "C"  String_t* Localytics_get_AppKey_m819823762 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		String_t* L_0 = Localytics__appKey_m443537396(NULL /*static, unused*/, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.String LocalyticsUnity.Localytics::get_CustomerId()
extern "C"  String_t* Localytics_get_CustomerId_m1517405549 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		String_t* L_0 = Localytics__customerId_m3255277519(NULL /*static, unused*/, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Void LocalyticsUnity.Localytics::set_CustomerId(System.String)
extern "C"  void Localytics_set_CustomerId_m3487816068 (Il2CppObject * __this /* static, unused */, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		Localytics__setCustomerId_m2351447262(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return;
	}
}
// LocalyticsUnity.Localytics/InAppMessageDismissButtonLocation LocalyticsUnity.Localytics::get_InAppMessageDismissButtonLocationEnum()
extern "C"  int32_t Localytics_get_InAppMessageDismissButtonLocationEnum_m1519812625 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		uint32_t L_0 = Localytics__inAppMessageDismissButtonLocation_m3941093290(NULL /*static, unused*/, /*hidden argument*/NULL);
		return (int32_t)(L_0);
	}
}
// System.Void LocalyticsUnity.Localytics::set_InAppMessageDismissButtonLocationEnum(LocalyticsUnity.Localytics/InAppMessageDismissButtonLocation)
extern "C"  void Localytics_set_InAppMessageDismissButtonLocationEnum_m3542779598 (Il2CppObject * __this /* static, unused */, int32_t ___value0, const MethodInfo* method)
{
	{
		int32_t L_0 = ___value0;
		Localytics__setInAppMessageDismissButtonLocation_m26342109(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.String LocalyticsUnity.Localytics::get_InstallId()
extern "C"  String_t* Localytics_get_InstallId_m3132941348 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		String_t* L_0 = Localytics__installId_m3050454402(NULL /*static, unused*/, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.String LocalyticsUnity.Localytics::get_LibraryVersion()
extern "C"  String_t* Localytics_get_LibraryVersion_m2467127537 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		String_t* L_0 = Localytics__libraryVersion_m3167695443(NULL /*static, unused*/, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Boolean LocalyticsUnity.Localytics::get_LoggingEnabled()
extern "C"  bool Localytics_get_LoggingEnabled_m620943751 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		bool L_0 = Localytics__isLoggingEnabled_m1335200689(NULL /*static, unused*/, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Void LocalyticsUnity.Localytics::set_LoggingEnabled(System.Boolean)
extern "C"  void Localytics_set_LoggingEnabled_m257793150 (Il2CppObject * __this /* static, unused */, bool ___value0, const MethodInfo* method)
{
	{
		bool L_0 = ___value0;
		Localytics__setLoggingEnabled_m3874701668(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.String LocalyticsUnity.Localytics::get_MessagingHost()
extern "C"  String_t* Localytics_get_MessagingHost_m1430473434 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		String_t* L_0 = Localytics__messagingHost_m2838545720(NULL /*static, unused*/, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Void LocalyticsUnity.Localytics::set_MessagingHost(System.String)
extern "C"  void Localytics_set_MessagingHost_m174037305 (Il2CppObject * __this /* static, unused */, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		Localytics__setMessagingHost_m3838132127(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean LocalyticsUnity.Localytics::get_OptedOut()
extern "C"  bool Localytics_get_OptedOut_m839117441 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		bool L_0 = Localytics__isOptedOut_m2758411307(NULL /*static, unused*/, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Void LocalyticsUnity.Localytics::set_OptedOut(System.Boolean)
extern "C"  void Localytics_set_OptedOut_m1115668472 (Il2CppObject * __this /* static, unused */, bool ___value0, const MethodInfo* method)
{
	{
		bool L_0 = ___value0;
		Localytics__setOptedOut_m1356106078(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.String LocalyticsUnity.Localytics::get_ProfilesHost()
extern "C"  String_t* Localytics_get_ProfilesHost_m3564794246 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		String_t* L_0 = Localytics__profilesHost_m2917479272(NULL /*static, unused*/, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Void LocalyticsUnity.Localytics::set_ProfilesHost(System.String)
extern "C"  void Localytics_set_ProfilesHost_m3117737547 (Il2CppObject * __this /* static, unused */, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		Localytics__setProfilesHost_m1989008165(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean LocalyticsUnity.Localytics::get_TestModeEnabled()
extern "C"  bool Localytics_get_TestModeEnabled_m3712260233 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		bool L_0 = Localytics__testModeEnabled_m3725881001(NULL /*static, unused*/, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Void LocalyticsUnity.Localytics::set_TestModeEnabled(System.Boolean)
extern "C"  void Localytics_set_TestModeEnabled_m3092716672 (Il2CppObject * __this /* static, unused */, bool ___value0, const MethodInfo* method)
{
	{
		bool L_0 = ___value0;
		Localytics__setTestModeEnabled_m3547731034(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Double LocalyticsUnity.Localytics::get_SessionTimeoutInterval()
extern "C"  double Localytics_get_SessionTimeoutInterval_m1713454500 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		double L_0 = Localytics__sessionTimeoutInterval_m3711220870(NULL /*static, unused*/, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Void LocalyticsUnity.Localytics::set_SessionTimeoutInterval(System.Double)
extern "C"  void Localytics_set_SessionTimeoutInterval_m1562903725 (Il2CppObject * __this /* static, unused */, double ___value0, const MethodInfo* method)
{
	{
		double L_0 = ___value0;
		Localytics__setSessionTimeoutInterval_m1493184775(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void LocalyticsUnity.Localytics::AddProfileAttributesToSet(System.String,System.Int64[],LocalyticsUnity.Localytics/ProfileScope)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* MiniJSON_t1258204565_il2cpp_TypeInfo_var;
extern const uint32_t Localytics_AddProfileAttributesToSet_m2948220263_MetadataUsageId;
extern "C"  void Localytics_AddProfileAttributesToSet_m2948220263 (Il2CppObject * __this /* static, unused */, String_t* ___attributeName0, Int64U5BU5D_t753178071* ___attributeValue1, int32_t ___scope2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Localytics_AddProfileAttributesToSet_m2948220263_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	String_t* V_0 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_0 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		V_0 = L_0;
		Int64U5BU5D_t753178071* L_1 = ___attributeValue1;
		if (!L_1)
		{
			goto IL_0013;
		}
	}
	{
		Int64U5BU5D_t753178071* L_2 = ___attributeValue1;
		IL2CPP_RUNTIME_CLASS_INIT(MiniJSON_t1258204565_il2cpp_TypeInfo_var);
		String_t* L_3 = MiniJSON_jsonEncode_m2701702143(NULL /*static, unused*/, (Il2CppObject *)(Il2CppObject *)L_2, /*hidden argument*/NULL);
		V_0 = L_3;
	}

IL_0013:
	{
		String_t* L_4 = ___attributeName0;
		String_t* L_5 = V_0;
		int32_t L_6 = ___scope2;
		Localytics__addProfileAttributesToSet_m3002844298(NULL /*static, unused*/, L_4, L_5, L_6, /*hidden argument*/NULL);
		return;
	}
}
// System.Void LocalyticsUnity.Localytics::AddProfileAttributesToSet(System.String,System.String[],LocalyticsUnity.Localytics/ProfileScope)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* MiniJSON_t1258204565_il2cpp_TypeInfo_var;
extern const uint32_t Localytics_AddProfileAttributesToSet_m2199160331_MetadataUsageId;
extern "C"  void Localytics_AddProfileAttributesToSet_m2199160331 (Il2CppObject * __this /* static, unused */, String_t* ___attributeName0, StringU5BU5D_t2956870243* ___attributeValue1, int32_t ___scope2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Localytics_AddProfileAttributesToSet_m2199160331_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	String_t* V_0 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_0 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		V_0 = L_0;
		StringU5BU5D_t2956870243* L_1 = ___attributeValue1;
		if (!L_1)
		{
			goto IL_0013;
		}
	}
	{
		StringU5BU5D_t2956870243* L_2 = ___attributeValue1;
		IL2CPP_RUNTIME_CLASS_INIT(MiniJSON_t1258204565_il2cpp_TypeInfo_var);
		String_t* L_3 = MiniJSON_jsonEncode_m2701702143(NULL /*static, unused*/, (Il2CppObject *)(Il2CppObject *)L_2, /*hidden argument*/NULL);
		V_0 = L_3;
	}

IL_0013:
	{
		String_t* L_4 = ___attributeName0;
		String_t* L_5 = V_0;
		int32_t L_6 = ___scope2;
		Localytics__addProfileAttributesToSet_m3002844298(NULL /*static, unused*/, L_4, L_5, L_6, /*hidden argument*/NULL);
		return;
	}
}
// System.Void LocalyticsUnity.Localytics::CloseSession()
extern "C"  void Localytics_CloseSession_m643641830 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		Localytics__closeSession_m3849701175(NULL /*static, unused*/, /*hidden argument*/NULL);
		return;
	}
}
// System.Void LocalyticsUnity.Localytics::DecrementProfileAttribute(System.String,System.Int64,LocalyticsUnity.Localytics/ProfileScope)
extern "C"  void Localytics_DecrementProfileAttribute_m1540356871 (Il2CppObject * __this /* static, unused */, String_t* ___attributeName0, int64_t ___decrementValue1, int32_t ___scope2, const MethodInfo* method)
{
	{
		String_t* L_0 = ___attributeName0;
		int64_t L_1 = ___decrementValue1;
		int32_t L_2 = ___scope2;
		Localytics__decrementProfileAttribute_m2705617202(NULL /*static, unused*/, L_0, L_1, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void LocalyticsUnity.Localytics::DeleteProfileAttribute(System.String,LocalyticsUnity.Localytics/ProfileScope)
extern "C"  void Localytics_DeleteProfileAttribute_m860759341 (Il2CppObject * __this /* static, unused */, String_t* ___attributeName0, int32_t ___scope1, const MethodInfo* method)
{
	{
		String_t* L_0 = ___attributeName0;
		int32_t L_1 = ___scope1;
		Localytics__deleteProfileAttribute_m340220108(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void LocalyticsUnity.Localytics::SetCustomerEmail(System.String)
extern "C"  void Localytics_SetCustomerEmail_m3350313726 (Il2CppObject * __this /* static, unused */, String_t* ___email0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___email0;
		Localytics__setCustomerEmail_m912150989(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void LocalyticsUnity.Localytics::SetCustomerFirstName(System.String)
extern "C"  void Localytics_SetCustomerFirstName_m3155305183 (Il2CppObject * __this /* static, unused */, String_t* ___firstName0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___firstName0;
		Localytics__setCustomerFirstName_m1105771054(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void LocalyticsUnity.Localytics::SetCustomerLastName(System.String)
extern "C"  void Localytics_SetCustomerLastName_m2358470183 (Il2CppObject * __this /* static, unused */, String_t* ___lastName0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___lastName0;
		Localytics__setCustomerLastName_m3539282168(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void LocalyticsUnity.Localytics::SetCustomerFullName(System.String)
extern "C"  void Localytics_SetCustomerFullName_m3164535182 (Il2CppObject * __this /* static, unused */, String_t* ___fullName0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___fullName0;
		Localytics__setCustomerFullName_m50379871(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void LocalyticsUnity.Localytics::DismissCurrentInAppMessage()
extern "C"  void Localytics_DismissCurrentInAppMessage_m3819815938 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		Localytics__dismissCurrentInAppMessage_m2081044371(NULL /*static, unused*/, /*hidden argument*/NULL);
		return;
	}
}
// System.String LocalyticsUnity.Localytics::GetCustomDimension(System.Int32)
extern "C"  String_t* Localytics_GetCustomDimension_m6096923 (Il2CppObject * __this /* static, unused */, int32_t ___dimension0, const MethodInfo* method)
{
	{
		int32_t L_0 = ___dimension0;
		String_t* L_1 = Localytics__getCustomDimension_m640247878(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.String LocalyticsUnity.Localytics::GetIdentifier(System.String)
extern "C"  String_t* Localytics_GetIdentifier_m3747102956 (Il2CppObject * __this /* static, unused */, String_t* ___key0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___key0;
		String_t* L_1 = Localytics__getIdentifier_m3390664343(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Void LocalyticsUnity.Localytics::IncrementProfileAttribute(System.String,System.Int64,LocalyticsUnity.Localytics/ProfileScope)
extern "C"  void Localytics_IncrementProfileAttribute_m3498300387 (Il2CppObject * __this /* static, unused */, String_t* ___attributeName0, int64_t ___attributeValue1, int32_t ___scope2, const MethodInfo* method)
{
	{
		String_t* L_0 = ___attributeName0;
		int64_t L_1 = ___attributeValue1;
		int32_t L_2 = ___scope2;
		Localytics__incrementProfileAttribute_m1877823958(NULL /*static, unused*/, L_0, L_1, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void LocalyticsUnity.Localytics::OpenSession()
extern "C"  void Localytics_OpenSession_m2787520294 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		Localytics__openSession_m535636917(NULL /*static, unused*/, /*hidden argument*/NULL);
		return;
	}
}
// System.Void LocalyticsUnity.Localytics::RegisterForAnalyticsEvents()
extern Il2CppClass* ReceiveAnalyticsDelegate_t2804656808_il2cpp_TypeInfo_var;
extern const MethodInfo* Localytics_ReceiveAnalyticsMessage_m3745756772_MethodInfo_var;
extern const uint32_t Localytics_RegisterForAnalyticsEvents_m3824577953_MetadataUsageId;
extern "C"  void Localytics_RegisterForAnalyticsEvents_m3824577953 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Localytics_RegisterForAnalyticsEvents_m3824577953_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IntPtr_t L_0;
		L_0.set_m_value_0((void*)(void*)Localytics_ReceiveAnalyticsMessage_m3745756772_MethodInfo_var);
		ReceiveAnalyticsDelegate_t2804656808 * L_1 = (ReceiveAnalyticsDelegate_t2804656808 *)il2cpp_codegen_object_new(ReceiveAnalyticsDelegate_t2804656808_il2cpp_TypeInfo_var);
		ReceiveAnalyticsDelegate__ctor_m1029077999(L_1, NULL, L_0, /*hidden argument*/NULL);
		Localytics__registerReceiveAnalyticsCallback_m3272957910(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void LocalyticsUnity.Localytics::UnregisterForAnalyticsEvents()
extern "C"  void Localytics_UnregisterForAnalyticsEvents_m712507130 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		Localytics__removeAnalyticsCallback_m3153879504(NULL /*static, unused*/, /*hidden argument*/NULL);
		return;
	}
}
// System.Void LocalyticsUnity.Localytics::RegisterForMessagingEvents()
extern Il2CppClass* ReceiveMessagingDelegate_t3408128038_il2cpp_TypeInfo_var;
extern const MethodInfo* Localytics_ReceiveMessagingMessage_m864780130_MethodInfo_var;
extern const uint32_t Localytics_RegisterForMessagingEvents_m133081887_MetadataUsageId;
extern "C"  void Localytics_RegisterForMessagingEvents_m133081887 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Localytics_RegisterForMessagingEvents_m133081887_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IntPtr_t L_0;
		L_0.set_m_value_0((void*)(void*)Localytics_ReceiveMessagingMessage_m864780130_MethodInfo_var);
		ReceiveMessagingDelegate_t3408128038 * L_1 = (ReceiveMessagingDelegate_t3408128038 *)il2cpp_codegen_object_new(ReceiveMessagingDelegate_t3408128038_il2cpp_TypeInfo_var);
		ReceiveMessagingDelegate__ctor_m1867403885(L_1, NULL, L_0, /*hidden argument*/NULL);
		Localytics__registerReceiveMessagingCallback_m3957393242(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void LocalyticsUnity.Localytics::UnregisterForMessagingEvents()
extern "C"  void Localytics_UnregisterForMessagingEvents_m1315978360 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		Localytics__removeMessagingCallback_m3269146574(NULL /*static, unused*/, /*hidden argument*/NULL);
		return;
	}
}
// System.Void LocalyticsUnity.Localytics::RemoveProfileAttributesFromSet(System.String,System.Int64[],LocalyticsUnity.Localytics/ProfileScope)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* MiniJSON_t1258204565_il2cpp_TypeInfo_var;
extern const uint32_t Localytics_RemoveProfileAttributesFromSet_m1131843881_MetadataUsageId;
extern "C"  void Localytics_RemoveProfileAttributesFromSet_m1131843881 (Il2CppObject * __this /* static, unused */, String_t* ___attributeName0, Int64U5BU5D_t753178071* ___attributeValue1, int32_t ___scope2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Localytics_RemoveProfileAttributesFromSet_m1131843881_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	String_t* V_0 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_0 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		V_0 = L_0;
		Int64U5BU5D_t753178071* L_1 = ___attributeValue1;
		if (!L_1)
		{
			goto IL_0013;
		}
	}
	{
		Int64U5BU5D_t753178071* L_2 = ___attributeValue1;
		IL2CPP_RUNTIME_CLASS_INIT(MiniJSON_t1258204565_il2cpp_TypeInfo_var);
		String_t* L_3 = MiniJSON_jsonEncode_m2701702143(NULL /*static, unused*/, (Il2CppObject *)(Il2CppObject *)L_2, /*hidden argument*/NULL);
		V_0 = L_3;
	}

IL_0013:
	{
		String_t* L_4 = V_0;
		String_t* L_5 = ___attributeName0;
		int32_t L_6 = ___scope2;
		Localytics__removeProfileAttributesFromSet_m391145998(NULL /*static, unused*/, L_4, L_5, L_6, /*hidden argument*/NULL);
		return;
	}
}
// System.Void LocalyticsUnity.Localytics::RemoveProfileAttributesFromSet(System.String,System.String[],LocalyticsUnity.Localytics/ProfileScope)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* MiniJSON_t1258204565_il2cpp_TypeInfo_var;
extern const uint32_t Localytics_RemoveProfileAttributesFromSet_m1726067337_MetadataUsageId;
extern "C"  void Localytics_RemoveProfileAttributesFromSet_m1726067337 (Il2CppObject * __this /* static, unused */, String_t* ___attributeName0, StringU5BU5D_t2956870243* ___attributeValue1, int32_t ___scope2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Localytics_RemoveProfileAttributesFromSet_m1726067337_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	String_t* V_0 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_0 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		V_0 = L_0;
		StringU5BU5D_t2956870243* L_1 = ___attributeValue1;
		if (!L_1)
		{
			goto IL_0013;
		}
	}
	{
		StringU5BU5D_t2956870243* L_2 = ___attributeValue1;
		IL2CPP_RUNTIME_CLASS_INIT(MiniJSON_t1258204565_il2cpp_TypeInfo_var);
		String_t* L_3 = MiniJSON_jsonEncode_m2701702143(NULL /*static, unused*/, (Il2CppObject *)(Il2CppObject *)L_2, /*hidden argument*/NULL);
		V_0 = L_3;
	}

IL_0013:
	{
		String_t* L_4 = V_0;
		String_t* L_5 = ___attributeName0;
		int32_t L_6 = ___scope2;
		Localytics__removeProfileAttributesFromSet_m391145998(NULL /*static, unused*/, L_4, L_5, L_6, /*hidden argument*/NULL);
		return;
	}
}
// System.Void LocalyticsUnity.Localytics::SetCustomDimension(System.Int32,System.String)
extern "C"  void Localytics_SetCustomDimension_m2444359208 (Il2CppObject * __this /* static, unused */, int32_t ___dimension0, String_t* ___value1, const MethodInfo* method)
{
	{
		int32_t L_0 = ___dimension0;
		String_t* L_1 = ___value1;
		Localytics__setCustomDimension_m1383663481(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void LocalyticsUnity.Localytics::SetIdentifier(System.String,System.String)
extern "C"  void Localytics_SetIdentifier_m3659144633 (Il2CppObject * __this /* static, unused */, String_t* ___key0, String_t* ___value1, const MethodInfo* method)
{
	{
		String_t* L_0 = ___key0;
		String_t* L_1 = ___value1;
		Localytics__setIdentifier_m393772938(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void LocalyticsUnity.Localytics::SetLocation(UnityEngine.LocationInfo)
extern "C"  void Localytics_SetLocation_m288298039 (Il2CppObject * __this /* static, unused */, LocationInfo_t1628589680  ___location0, const MethodInfo* method)
{
	{
		float L_0 = LocationInfo_get_latitude_m1803811355((&___location0), /*hidden argument*/NULL);
		float L_1 = LocationInfo_get_longitude_m3505987842((&___location0), /*hidden argument*/NULL);
		Localytics__setLocation_m2200217086(NULL /*static, unused*/, (((double)((double)L_0))), (((double)((double)L_1))), /*hidden argument*/NULL);
		return;
	}
}
// System.Void LocalyticsUnity.Localytics::SetProfileAttribute(System.String,System.Int64,LocalyticsUnity.Localytics/ProfileScope)
extern Il2CppClass* Int64U5BU5D_t753178071_il2cpp_TypeInfo_var;
extern Il2CppClass* MiniJSON_t1258204565_il2cpp_TypeInfo_var;
extern const uint32_t Localytics_SetProfileAttribute_m211571734_MetadataUsageId;
extern "C"  void Localytics_SetProfileAttribute_m211571734 (Il2CppObject * __this /* static, unused */, String_t* ___attributeName0, int64_t ___attributeValue1, int32_t ___scope2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Localytics_SetProfileAttribute_m211571734_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	String_t* V_0 = NULL;
	{
		Int64U5BU5D_t753178071* L_0 = ((Int64U5BU5D_t753178071*)SZArrayNew(Int64U5BU5D_t753178071_il2cpp_TypeInfo_var, (uint32_t)1));
		int64_t L_1 = ___attributeValue1;
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (int64_t)L_1);
		IL2CPP_RUNTIME_CLASS_INIT(MiniJSON_t1258204565_il2cpp_TypeInfo_var);
		String_t* L_2 = MiniJSON_jsonEncode_m2701702143(NULL /*static, unused*/, (Il2CppObject *)(Il2CppObject *)L_0, /*hidden argument*/NULL);
		V_0 = L_2;
		String_t* L_3 = V_0;
		String_t* L_4 = ___attributeName0;
		int32_t L_5 = ___scope2;
		Localytics__removeProfileAttributesFromSet_m391145998(NULL /*static, unused*/, L_3, L_4, L_5, /*hidden argument*/NULL);
		return;
	}
}
// System.Void LocalyticsUnity.Localytics::SetProfileAttribute(System.String,System.Int64[],LocalyticsUnity.Localytics/ProfileScope)
extern Il2CppClass* MiniJSON_t1258204565_il2cpp_TypeInfo_var;
extern const uint32_t Localytics_SetProfileAttribute_m498919412_MetadataUsageId;
extern "C"  void Localytics_SetProfileAttribute_m498919412 (Il2CppObject * __this /* static, unused */, String_t* ___attributeName0, Int64U5BU5D_t753178071* ___attributeValue1, int32_t ___scope2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Localytics_SetProfileAttribute_m498919412_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	String_t* V_0 = NULL;
	{
		Int64U5BU5D_t753178071* L_0 = ___attributeValue1;
		IL2CPP_RUNTIME_CLASS_INIT(MiniJSON_t1258204565_il2cpp_TypeInfo_var);
		String_t* L_1 = MiniJSON_jsonEncode_m2701702143(NULL /*static, unused*/, (Il2CppObject *)(Il2CppObject *)L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		String_t* L_2 = V_0;
		String_t* L_3 = ___attributeName0;
		int32_t L_4 = ___scope2;
		Localytics__removeProfileAttributesFromSet_m391145998(NULL /*static, unused*/, L_2, L_3, L_4, /*hidden argument*/NULL);
		return;
	}
}
// System.Void LocalyticsUnity.Localytics::SetProfileAttribute(System.String,System.String,LocalyticsUnity.Localytics/ProfileScope)
extern Il2CppClass* StringU5BU5D_t2956870243_il2cpp_TypeInfo_var;
extern Il2CppClass* MiniJSON_t1258204565_il2cpp_TypeInfo_var;
extern const uint32_t Localytics_SetProfileAttribute_m3450528704_MetadataUsageId;
extern "C"  void Localytics_SetProfileAttribute_m3450528704 (Il2CppObject * __this /* static, unused */, String_t* ___attributeName0, String_t* ___attributeValue1, int32_t ___scope2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Localytics_SetProfileAttribute_m3450528704_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	String_t* V_0 = NULL;
	{
		StringU5BU5D_t2956870243* L_0 = ((StringU5BU5D_t2956870243*)SZArrayNew(StringU5BU5D_t2956870243_il2cpp_TypeInfo_var, (uint32_t)1));
		String_t* L_1 = ___attributeValue1;
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, L_1);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)L_1);
		IL2CPP_RUNTIME_CLASS_INIT(MiniJSON_t1258204565_il2cpp_TypeInfo_var);
		String_t* L_2 = MiniJSON_jsonEncode_m2701702143(NULL /*static, unused*/, (Il2CppObject *)(Il2CppObject *)L_0, /*hidden argument*/NULL);
		V_0 = L_2;
		String_t* L_3 = V_0;
		String_t* L_4 = ___attributeName0;
		int32_t L_5 = ___scope2;
		Localytics__removeProfileAttributesFromSet_m391145998(NULL /*static, unused*/, L_3, L_4, L_5, /*hidden argument*/NULL);
		return;
	}
}
// System.Void LocalyticsUnity.Localytics::SetProfileAttribute(System.String,System.String[],LocalyticsUnity.Localytics/ProfileScope)
extern Il2CppClass* MiniJSON_t1258204565_il2cpp_TypeInfo_var;
extern const uint32_t Localytics_SetProfileAttribute_m3580245278_MetadataUsageId;
extern "C"  void Localytics_SetProfileAttribute_m3580245278 (Il2CppObject * __this /* static, unused */, String_t* ___attributeName0, StringU5BU5D_t2956870243* ___attributeValue1, int32_t ___scope2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Localytics_SetProfileAttribute_m3580245278_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	String_t* V_0 = NULL;
	{
		StringU5BU5D_t2956870243* L_0 = ___attributeValue1;
		IL2CPP_RUNTIME_CLASS_INIT(MiniJSON_t1258204565_il2cpp_TypeInfo_var);
		String_t* L_1 = MiniJSON_jsonEncode_m2701702143(NULL /*static, unused*/, (Il2CppObject *)(Il2CppObject *)L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		String_t* L_2 = V_0;
		String_t* L_3 = ___attributeName0;
		int32_t L_4 = ___scope2;
		Localytics__removeProfileAttributesFromSet_m391145998(NULL /*static, unused*/, L_2, L_3, L_4, /*hidden argument*/NULL);
		return;
	}
}
// System.Void LocalyticsUnity.Localytics::TagEvent(System.String,System.Collections.Generic.Dictionary`2<System.String,System.String>,System.Int64)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* MiniJSON_t1258204565_il2cpp_TypeInfo_var;
extern const uint32_t Localytics_TagEvent_m1826336763_MetadataUsageId;
extern "C"  void Localytics_TagEvent_m1826336763 (Il2CppObject * __this /* static, unused */, String_t* ___eventName0, Dictionary_2_t2606186806 * ___attributes1, int64_t ___customerValueIncrease2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Localytics_TagEvent_m1826336763_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	String_t* V_0 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_0 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		V_0 = L_0;
		Dictionary_2_t2606186806 * L_1 = ___attributes1;
		if (!L_1)
		{
			goto IL_0013;
		}
	}
	{
		Dictionary_2_t2606186806 * L_2 = ___attributes1;
		IL2CPP_RUNTIME_CLASS_INIT(MiniJSON_t1258204565_il2cpp_TypeInfo_var);
		String_t* L_3 = MiniJSON_jsonEncode_m2701702143(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		V_0 = L_3;
	}

IL_0013:
	{
		String_t* L_4 = ___eventName0;
		String_t* L_5 = V_0;
		int64_t L_6 = ___customerValueIncrease2;
		Localytics__tagEvent_m2942919859(NULL /*static, unused*/, L_4, L_5, L_6, /*hidden argument*/NULL);
		return;
	}
}
// System.Void LocalyticsUnity.Localytics::TagScreen(System.String)
extern "C"  void Localytics_TagScreen_m3893025890 (Il2CppObject * __this /* static, unused */, String_t* ___screen0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___screen0;
		Localytics__tagScreen_m3783901299(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void LocalyticsUnity.Localytics::TriggerInAppMessage(System.String,System.Collections.Generic.Dictionary`2<System.String,System.String>)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* MiniJSON_t1258204565_il2cpp_TypeInfo_var;
extern const uint32_t Localytics_TriggerInAppMessage_m1408598194_MetadataUsageId;
extern "C"  void Localytics_TriggerInAppMessage_m1408598194 (Il2CppObject * __this /* static, unused */, String_t* ___triggerName0, Dictionary_2_t2606186806 * ___attributes1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Localytics_TriggerInAppMessage_m1408598194_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	String_t* V_0 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_0 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		V_0 = L_0;
		Dictionary_2_t2606186806 * L_1 = ___attributes1;
		if (!L_1)
		{
			goto IL_0013;
		}
	}
	{
		Dictionary_2_t2606186806 * L_2 = ___attributes1;
		IL2CPP_RUNTIME_CLASS_INIT(MiniJSON_t1258204565_il2cpp_TypeInfo_var);
		String_t* L_3 = MiniJSON_jsonEncode_m2701702143(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		V_0 = L_3;
	}

IL_0013:
	{
		String_t* L_4 = ___triggerName0;
		String_t* L_5 = V_0;
		Localytics__triggerInAppMessage_m318372786(NULL /*static, unused*/, L_4, L_5, /*hidden argument*/NULL);
		return;
	}
}
// System.Void LocalyticsUnity.Localytics::Upload()
extern "C"  void Localytics_Upload_m3532132073 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		Localytics__upload_m533411066(NULL /*static, unused*/, /*hidden argument*/NULL);
		return;
	}
}
// System.String LocalyticsUnity.Localytics::get_PushToken()
extern "C"  String_t* Localytics_get_PushToken_m4057863373 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		String_t* L_0 = Localytics__pushToken_m3975376427(NULL /*static, unused*/, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Boolean LocalyticsUnity.Localytics::get_IsCollectingAdvertisingIdentifier()
extern "C"  bool Localytics_get_IsCollectingAdvertisingIdentifier_m1756652136 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		bool L_0 = Localytics__isCollectingAdvertisingIdentifier_m2529620104(NULL /*static, unused*/, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Void LocalyticsUnity.Localytics::set_IsCollectingAdvertisingIdentifier(System.Boolean)
extern "C"  void Localytics_set_IsCollectingAdvertisingIdentifier_m1154786335 (Il2CppObject * __this /* static, unused */, bool ___value0, const MethodInfo* method)
{
	{
		bool L_0 = ___value0;
		Localytics__setCollectingAdvertisingIdentifier_m3536717187(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean LocalyticsUnity.Localytics::get_InAppAdIdParameterEnabled()
extern "C"  bool Localytics_get_InAppAdIdParameterEnabled_m1846304975 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		bool L_0 = Localytics__isInAppAdIdParameterEnabled_m475762789(NULL /*static, unused*/, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Void LocalyticsUnity.Localytics::set_InAppAdIdParameterEnabled(System.Boolean)
extern "C"  void Localytics_set_InAppAdIdParameterEnabled_m1757815366 (Il2CppObject * __this /* static, unused */, bool ___value0, const MethodInfo* method)
{
	{
		bool L_0 = ___value0;
		Localytics__setInAppAdIdParameterEnabled_m698126752(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.String LocalyticsUnity.Localytics::_appKey()
extern "C" char* DEFAULT_CALL _appKey();
extern "C"  String_t* Localytics__appKey_m443537396 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef char* (DEFAULT_CALL *PInvokeFunc) ();

	// Native function invocation and marshaling of return value back from native representation
	char* _return_value = reinterpret_cast<PInvokeFunc>(_appKey)();
	String_t* __return_value_unmarshaled = NULL;
	__return_value_unmarshaled = il2cpp_codegen_marshal_string_result(_return_value);
	il2cpp_codegen_marshal_free(_return_value);
	_return_value = NULL;

	return __return_value_unmarshaled;
}
// System.String LocalyticsUnity.Localytics::_analyticsHost()
extern "C" char* DEFAULT_CALL _analyticsHost();
extern "C"  String_t* Localytics__analyticsHost_m3387637818 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef char* (DEFAULT_CALL *PInvokeFunc) ();

	// Native function invocation and marshaling of return value back from native representation
	char* _return_value = reinterpret_cast<PInvokeFunc>(_analyticsHost)();
	String_t* __return_value_unmarshaled = NULL;
	__return_value_unmarshaled = il2cpp_codegen_marshal_string_result(_return_value);
	il2cpp_codegen_marshal_free(_return_value);
	_return_value = NULL;

	return __return_value_unmarshaled;
}
// System.Void LocalyticsUnity.Localytics::_setAnalyticsHost(System.String)
extern "C" void DEFAULT_CALL _setAnalyticsHost(char*);
extern "C"  void Localytics__setAnalyticsHost_m2514892765 (Il2CppObject * __this /* static, unused */, String_t* ___analyticsHost0, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (char*);

	// Marshaling of parameter '___analyticsHost0' to native representation
	char* ____analyticsHost0_marshaled = NULL;
	____analyticsHost0_marshaled = il2cpp_codegen_marshal_string(___analyticsHost0);

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(_setAnalyticsHost)(____analyticsHost0_marshaled);

	// Marshaling cleanup of parameter '___analyticsHost0' native representation
	il2cpp_codegen_marshal_free(____analyticsHost0_marshaled);
	____analyticsHost0_marshaled = NULL;

}
// System.String LocalyticsUnity.Localytics::_customerId()
extern "C" char* DEFAULT_CALL _customerId();
extern "C"  String_t* Localytics__customerId_m3255277519 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef char* (DEFAULT_CALL *PInvokeFunc) ();

	// Native function invocation and marshaling of return value back from native representation
	char* _return_value = reinterpret_cast<PInvokeFunc>(_customerId)();
	String_t* __return_value_unmarshaled = NULL;
	__return_value_unmarshaled = il2cpp_codegen_marshal_string_result(_return_value);
	il2cpp_codegen_marshal_free(_return_value);
	_return_value = NULL;

	return __return_value_unmarshaled;
}
// System.Void LocalyticsUnity.Localytics::_setCustomerId(System.String)
extern "C" void DEFAULT_CALL _setCustomerId(char*);
extern "C"  void Localytics__setCustomerId_m2351447262 (Il2CppObject * __this /* static, unused */, String_t* ___customerId0, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (char*);

	// Marshaling of parameter '___customerId0' to native representation
	char* ____customerId0_marshaled = NULL;
	____customerId0_marshaled = il2cpp_codegen_marshal_string(___customerId0);

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(_setCustomerId)(____customerId0_marshaled);

	// Marshaling cleanup of parameter '___customerId0' native representation
	il2cpp_codegen_marshal_free(____customerId0_marshaled);
	____customerId0_marshaled = NULL;

}
// System.UInt32 LocalyticsUnity.Localytics::_inAppMessageDismissButtonLocation()
extern "C" uint32_t DEFAULT_CALL _inAppMessageDismissButtonLocation();
extern "C"  uint32_t Localytics__inAppMessageDismissButtonLocation_m3941093290 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef uint32_t (DEFAULT_CALL *PInvokeFunc) ();

	// Native function invocation and marshaling of return value back from native representation
	uint32_t _return_value = reinterpret_cast<PInvokeFunc>(_inAppMessageDismissButtonLocation)();

	return _return_value;
}
// System.Void LocalyticsUnity.Localytics::_setInAppMessageDismissButtonLocation(System.UInt32)
extern "C" void DEFAULT_CALL _setInAppMessageDismissButtonLocation(uint32_t);
extern "C"  void Localytics__setInAppMessageDismissButtonLocation_m26342109 (Il2CppObject * __this /* static, unused */, uint32_t ___location0, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (uint32_t);

	// Marshaling of parameter '___location0' to native representation

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(_setInAppMessageDismissButtonLocation)(___location0);

	// Marshaling cleanup of parameter '___location0' native representation

}
// System.String LocalyticsUnity.Localytics::_installId()
extern "C" char* DEFAULT_CALL _installId();
extern "C"  String_t* Localytics__installId_m3050454402 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef char* (DEFAULT_CALL *PInvokeFunc) ();

	// Native function invocation and marshaling of return value back from native representation
	char* _return_value = reinterpret_cast<PInvokeFunc>(_installId)();
	String_t* __return_value_unmarshaled = NULL;
	__return_value_unmarshaled = il2cpp_codegen_marshal_string_result(_return_value);
	il2cpp_codegen_marshal_free(_return_value);
	_return_value = NULL;

	return __return_value_unmarshaled;
}
// System.Boolean LocalyticsUnity.Localytics::_isCollectingAdvertisingIdentifier()
extern "C" int32_t DEFAULT_CALL _isCollectingAdvertisingIdentifier();
extern "C"  bool Localytics__isCollectingAdvertisingIdentifier_m2529620104 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef int32_t (DEFAULT_CALL *PInvokeFunc) ();

	// Native function invocation and marshaling of return value back from native representation
	int32_t _return_value = reinterpret_cast<PInvokeFunc>(_isCollectingAdvertisingIdentifier)();

	return _return_value;
}
// System.Void LocalyticsUnity.Localytics::_setCollectingAdvertisingIdentifier(System.Boolean)
extern "C" void DEFAULT_CALL _setCollectingAdvertisingIdentifier(int32_t);
extern "C"  void Localytics__setCollectingAdvertisingIdentifier_m3536717187 (Il2CppObject * __this /* static, unused */, bool ___collectingAdvertisingIdentifier0, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (int32_t);

	// Marshaling of parameter '___collectingAdvertisingIdentifier0' to native representation

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(_setCollectingAdvertisingIdentifier)(___collectingAdvertisingIdentifier0);

	// Marshaling cleanup of parameter '___collectingAdvertisingIdentifier0' native representation

}
// System.String LocalyticsUnity.Localytics::_libraryVersion()
extern "C" char* DEFAULT_CALL _libraryVersion();
extern "C"  String_t* Localytics__libraryVersion_m3167695443 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef char* (DEFAULT_CALL *PInvokeFunc) ();

	// Native function invocation and marshaling of return value back from native representation
	char* _return_value = reinterpret_cast<PInvokeFunc>(_libraryVersion)();
	String_t* __return_value_unmarshaled = NULL;
	__return_value_unmarshaled = il2cpp_codegen_marshal_string_result(_return_value);
	il2cpp_codegen_marshal_free(_return_value);
	_return_value = NULL;

	return __return_value_unmarshaled;
}
// System.Boolean LocalyticsUnity.Localytics::_isLoggingEnabled()
extern "C" int32_t DEFAULT_CALL _isLoggingEnabled();
extern "C"  bool Localytics__isLoggingEnabled_m1335200689 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef int32_t (DEFAULT_CALL *PInvokeFunc) ();

	// Native function invocation and marshaling of return value back from native representation
	int32_t _return_value = reinterpret_cast<PInvokeFunc>(_isLoggingEnabled)();

	return _return_value;
}
// System.Void LocalyticsUnity.Localytics::_setLoggingEnabled(System.Boolean)
extern "C" void DEFAULT_CALL _setLoggingEnabled(int32_t);
extern "C"  void Localytics__setLoggingEnabled_m3874701668 (Il2CppObject * __this /* static, unused */, bool ___loggingEnabled0, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (int32_t);

	// Marshaling of parameter '___loggingEnabled0' to native representation

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(_setLoggingEnabled)(___loggingEnabled0);

	// Marshaling cleanup of parameter '___loggingEnabled0' native representation

}
// System.String LocalyticsUnity.Localytics::_messagingHost()
extern "C" char* DEFAULT_CALL _messagingHost();
extern "C"  String_t* Localytics__messagingHost_m2838545720 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef char* (DEFAULT_CALL *PInvokeFunc) ();

	// Native function invocation and marshaling of return value back from native representation
	char* _return_value = reinterpret_cast<PInvokeFunc>(_messagingHost)();
	String_t* __return_value_unmarshaled = NULL;
	__return_value_unmarshaled = il2cpp_codegen_marshal_string_result(_return_value);
	il2cpp_codegen_marshal_free(_return_value);
	_return_value = NULL;

	return __return_value_unmarshaled;
}
// System.Void LocalyticsUnity.Localytics::_setMessagingHost(System.String)
extern "C" void DEFAULT_CALL _setMessagingHost(char*);
extern "C"  void Localytics__setMessagingHost_m3838132127 (Il2CppObject * __this /* static, unused */, String_t* ___messagingHost0, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (char*);

	// Marshaling of parameter '___messagingHost0' to native representation
	char* ____messagingHost0_marshaled = NULL;
	____messagingHost0_marshaled = il2cpp_codegen_marshal_string(___messagingHost0);

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(_setMessagingHost)(____messagingHost0_marshaled);

	// Marshaling cleanup of parameter '___messagingHost0' native representation
	il2cpp_codegen_marshal_free(____messagingHost0_marshaled);
	____messagingHost0_marshaled = NULL;

}
// System.Boolean LocalyticsUnity.Localytics::_isOptedOut()
extern "C" int32_t DEFAULT_CALL _isOptedOut();
extern "C"  bool Localytics__isOptedOut_m2758411307 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef int32_t (DEFAULT_CALL *PInvokeFunc) ();

	// Native function invocation and marshaling of return value back from native representation
	int32_t _return_value = reinterpret_cast<PInvokeFunc>(_isOptedOut)();

	return _return_value;
}
// System.Void LocalyticsUnity.Localytics::_setOptedOut(System.Boolean)
extern "C" void DEFAULT_CALL _setOptedOut(int32_t);
extern "C"  void Localytics__setOptedOut_m1356106078 (Il2CppObject * __this /* static, unused */, bool ___optedOut0, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (int32_t);

	// Marshaling of parameter '___optedOut0' to native representation

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(_setOptedOut)(___optedOut0);

	// Marshaling cleanup of parameter '___optedOut0' native representation

}
// System.String LocalyticsUnity.Localytics::_profilesHost()
extern "C" char* DEFAULT_CALL _profilesHost();
extern "C"  String_t* Localytics__profilesHost_m2917479272 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef char* (DEFAULT_CALL *PInvokeFunc) ();

	// Native function invocation and marshaling of return value back from native representation
	char* _return_value = reinterpret_cast<PInvokeFunc>(_profilesHost)();
	String_t* __return_value_unmarshaled = NULL;
	__return_value_unmarshaled = il2cpp_codegen_marshal_string_result(_return_value);
	il2cpp_codegen_marshal_free(_return_value);
	_return_value = NULL;

	return __return_value_unmarshaled;
}
// System.Void LocalyticsUnity.Localytics::_setProfilesHost(System.String)
extern "C" void DEFAULT_CALL _setProfilesHost(char*);
extern "C"  void Localytics__setProfilesHost_m1989008165 (Il2CppObject * __this /* static, unused */, String_t* ___profilesHost0, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (char*);

	// Marshaling of parameter '___profilesHost0' to native representation
	char* ____profilesHost0_marshaled = NULL;
	____profilesHost0_marshaled = il2cpp_codegen_marshal_string(___profilesHost0);

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(_setProfilesHost)(____profilesHost0_marshaled);

	// Marshaling cleanup of parameter '___profilesHost0' native representation
	il2cpp_codegen_marshal_free(____profilesHost0_marshaled);
	____profilesHost0_marshaled = NULL;

}
// System.String LocalyticsUnity.Localytics::_pushToken()
extern "C" char* DEFAULT_CALL _pushToken();
extern "C"  String_t* Localytics__pushToken_m3975376427 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef char* (DEFAULT_CALL *PInvokeFunc) ();

	// Native function invocation and marshaling of return value back from native representation
	char* _return_value = reinterpret_cast<PInvokeFunc>(_pushToken)();
	String_t* __return_value_unmarshaled = NULL;
	__return_value_unmarshaled = il2cpp_codegen_marshal_string_result(_return_value);
	il2cpp_codegen_marshal_free(_return_value);
	_return_value = NULL;

	return __return_value_unmarshaled;
}
// System.Boolean LocalyticsUnity.Localytics::_testModeEnabled()
extern "C" int32_t DEFAULT_CALL _testModeEnabled();
extern "C"  bool Localytics__testModeEnabled_m3725881001 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef int32_t (DEFAULT_CALL *PInvokeFunc) ();

	// Native function invocation and marshaling of return value back from native representation
	int32_t _return_value = reinterpret_cast<PInvokeFunc>(_testModeEnabled)();

	return _return_value;
}
// System.Void LocalyticsUnity.Localytics::_setTestModeEnabled(System.Boolean)
extern "C" void DEFAULT_CALL _setTestModeEnabled(int32_t);
extern "C"  void Localytics__setTestModeEnabled_m3547731034 (Il2CppObject * __this /* static, unused */, bool ___enabled0, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (int32_t);

	// Marshaling of parameter '___enabled0' to native representation

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(_setTestModeEnabled)(___enabled0);

	// Marshaling cleanup of parameter '___enabled0' native representation

}
// System.Double LocalyticsUnity.Localytics::_sessionTimeoutInterval()
extern "C" double DEFAULT_CALL _sessionTimeoutInterval();
extern "C"  double Localytics__sessionTimeoutInterval_m3711220870 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef double (DEFAULT_CALL *PInvokeFunc) ();

	// Native function invocation and marshaling of return value back from native representation
	double _return_value = reinterpret_cast<PInvokeFunc>(_sessionTimeoutInterval)();

	return _return_value;
}
// System.Void LocalyticsUnity.Localytics::_setSessionTimeoutInterval(System.Double)
extern "C" void DEFAULT_CALL _setSessionTimeoutInterval(double);
extern "C"  void Localytics__setSessionTimeoutInterval_m1493184775 (Il2CppObject * __this /* static, unused */, double ___timeoutInterval0, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (double);

	// Marshaling of parameter '___timeoutInterval0' to native representation

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(_setSessionTimeoutInterval)(___timeoutInterval0);

	// Marshaling cleanup of parameter '___timeoutInterval0' native representation

}
// System.Void LocalyticsUnity.Localytics::_addProfileAttributesToSet(System.String,System.String,System.Int32)
extern "C" void DEFAULT_CALL _addProfileAttributesToSet(char*, char*, int32_t);
extern "C"  void Localytics__addProfileAttributesToSet_m3002844298 (Il2CppObject * __this /* static, unused */, String_t* ___attribute0, String_t* ___values1, int32_t ___scope2, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (char*, char*, int32_t);

	// Marshaling of parameter '___attribute0' to native representation
	char* ____attribute0_marshaled = NULL;
	____attribute0_marshaled = il2cpp_codegen_marshal_string(___attribute0);

	// Marshaling of parameter '___values1' to native representation
	char* ____values1_marshaled = NULL;
	____values1_marshaled = il2cpp_codegen_marshal_string(___values1);

	// Marshaling of parameter '___scope2' to native representation

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(_addProfileAttributesToSet)(____attribute0_marshaled, ____values1_marshaled, ___scope2);

	// Marshaling cleanup of parameter '___attribute0' native representation
	il2cpp_codegen_marshal_free(____attribute0_marshaled);
	____attribute0_marshaled = NULL;

	// Marshaling cleanup of parameter '___values1' native representation
	il2cpp_codegen_marshal_free(____values1_marshaled);
	____values1_marshaled = NULL;

	// Marshaling cleanup of parameter '___scope2' native representation

}
// System.Void LocalyticsUnity.Localytics::_closeSession()
extern "C" void DEFAULT_CALL _closeSession();
extern "C"  void Localytics__closeSession_m3849701175 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) ();

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(_closeSession)();

}
// System.Void LocalyticsUnity.Localytics::_decrementProfileAttribute(System.String,System.Int64,System.Int32)
extern "C" void DEFAULT_CALL _decrementProfileAttribute(char*, int64_t, int32_t);
extern "C"  void Localytics__decrementProfileAttribute_m2705617202 (Il2CppObject * __this /* static, unused */, String_t* ___attributeName0, int64_t ___value1, int32_t ___scope2, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (char*, int64_t, int32_t);

	// Marshaling of parameter '___attributeName0' to native representation
	char* ____attributeName0_marshaled = NULL;
	____attributeName0_marshaled = il2cpp_codegen_marshal_string(___attributeName0);

	// Marshaling of parameter '___value1' to native representation

	// Marshaling of parameter '___scope2' to native representation

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(_decrementProfileAttribute)(____attributeName0_marshaled, ___value1, ___scope2);

	// Marshaling cleanup of parameter '___attributeName0' native representation
	il2cpp_codegen_marshal_free(____attributeName0_marshaled);
	____attributeName0_marshaled = NULL;

	// Marshaling cleanup of parameter '___value1' native representation

	// Marshaling cleanup of parameter '___scope2' native representation

}
// System.Void LocalyticsUnity.Localytics::_deleteProfileAttribute(System.String,System.Int32)
extern "C" void DEFAULT_CALL _deleteProfileAttribute(char*, int32_t);
extern "C"  void Localytics__deleteProfileAttribute_m340220108 (Il2CppObject * __this /* static, unused */, String_t* ___attributeName0, int32_t ___scope1, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (char*, int32_t);

	// Marshaling of parameter '___attributeName0' to native representation
	char* ____attributeName0_marshaled = NULL;
	____attributeName0_marshaled = il2cpp_codegen_marshal_string(___attributeName0);

	// Marshaling of parameter '___scope1' to native representation

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(_deleteProfileAttribute)(____attributeName0_marshaled, ___scope1);

	// Marshaling cleanup of parameter '___attributeName0' native representation
	il2cpp_codegen_marshal_free(____attributeName0_marshaled);
	____attributeName0_marshaled = NULL;

	// Marshaling cleanup of parameter '___scope1' native representation

}
// System.Void LocalyticsUnity.Localytics::_setCustomerEmail(System.String)
extern "C" void DEFAULT_CALL _setCustomerEmail(char*);
extern "C"  void Localytics__setCustomerEmail_m912150989 (Il2CppObject * __this /* static, unused */, String_t* ___email0, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (char*);

	// Marshaling of parameter '___email0' to native representation
	char* ____email0_marshaled = NULL;
	____email0_marshaled = il2cpp_codegen_marshal_string(___email0);

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(_setCustomerEmail)(____email0_marshaled);

	// Marshaling cleanup of parameter '___email0' native representation
	il2cpp_codegen_marshal_free(____email0_marshaled);
	____email0_marshaled = NULL;

}
// System.Void LocalyticsUnity.Localytics::_setCustomerFirstName(System.String)
extern "C" void DEFAULT_CALL _setCustomerFirstName(char*);
extern "C"  void Localytics__setCustomerFirstName_m1105771054 (Il2CppObject * __this /* static, unused */, String_t* ___firstName0, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (char*);

	// Marshaling of parameter '___firstName0' to native representation
	char* ____firstName0_marshaled = NULL;
	____firstName0_marshaled = il2cpp_codegen_marshal_string(___firstName0);

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(_setCustomerFirstName)(____firstName0_marshaled);

	// Marshaling cleanup of parameter '___firstName0' native representation
	il2cpp_codegen_marshal_free(____firstName0_marshaled);
	____firstName0_marshaled = NULL;

}
// System.Void LocalyticsUnity.Localytics::_setCustomerLastName(System.String)
extern "C" void DEFAULT_CALL _setCustomerLastName(char*);
extern "C"  void Localytics__setCustomerLastName_m3539282168 (Il2CppObject * __this /* static, unused */, String_t* ___lastName0, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (char*);

	// Marshaling of parameter '___lastName0' to native representation
	char* ____lastName0_marshaled = NULL;
	____lastName0_marshaled = il2cpp_codegen_marshal_string(___lastName0);

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(_setCustomerLastName)(____lastName0_marshaled);

	// Marshaling cleanup of parameter '___lastName0' native representation
	il2cpp_codegen_marshal_free(____lastName0_marshaled);
	____lastName0_marshaled = NULL;

}
// System.Void LocalyticsUnity.Localytics::_setCustomerFullName(System.String)
extern "C" void DEFAULT_CALL _setCustomerFullName(char*);
extern "C"  void Localytics__setCustomerFullName_m50379871 (Il2CppObject * __this /* static, unused */, String_t* ___fullName0, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (char*);

	// Marshaling of parameter '___fullName0' to native representation
	char* ____fullName0_marshaled = NULL;
	____fullName0_marshaled = il2cpp_codegen_marshal_string(___fullName0);

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(_setCustomerFullName)(____fullName0_marshaled);

	// Marshaling cleanup of parameter '___fullName0' native representation
	il2cpp_codegen_marshal_free(____fullName0_marshaled);
	____fullName0_marshaled = NULL;

}
// System.Void LocalyticsUnity.Localytics::_dismissCurrentInAppMessage()
extern "C" void DEFAULT_CALL _dismissCurrentInAppMessage();
extern "C"  void Localytics__dismissCurrentInAppMessage_m2081044371 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) ();

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(_dismissCurrentInAppMessage)();

}
// System.String LocalyticsUnity.Localytics::_getCustomDimension(System.Int32)
extern "C" char* DEFAULT_CALL _getCustomDimension(int32_t);
extern "C"  String_t* Localytics__getCustomDimension_m640247878 (Il2CppObject * __this /* static, unused */, int32_t ___dimension0, const MethodInfo* method)
{
	typedef char* (DEFAULT_CALL *PInvokeFunc) (int32_t);

	// Marshaling of parameter '___dimension0' to native representation

	// Native function invocation and marshaling of return value back from native representation
	char* _return_value = reinterpret_cast<PInvokeFunc>(_getCustomDimension)(___dimension0);
	String_t* __return_value_unmarshaled = NULL;
	__return_value_unmarshaled = il2cpp_codegen_marshal_string_result(_return_value);
	il2cpp_codegen_marshal_free(_return_value);
	_return_value = NULL;

	// Marshaling cleanup of parameter '___dimension0' native representation

	return __return_value_unmarshaled;
}
// System.String LocalyticsUnity.Localytics::_getIdentifier(System.String)
extern "C" char* DEFAULT_CALL _getIdentifier(char*);
extern "C"  String_t* Localytics__getIdentifier_m3390664343 (Il2CppObject * __this /* static, unused */, String_t* ___key0, const MethodInfo* method)
{
	typedef char* (DEFAULT_CALL *PInvokeFunc) (char*);

	// Marshaling of parameter '___key0' to native representation
	char* ____key0_marshaled = NULL;
	____key0_marshaled = il2cpp_codegen_marshal_string(___key0);

	// Native function invocation and marshaling of return value back from native representation
	char* _return_value = reinterpret_cast<PInvokeFunc>(_getIdentifier)(____key0_marshaled);
	String_t* __return_value_unmarshaled = NULL;
	__return_value_unmarshaled = il2cpp_codegen_marshal_string_result(_return_value);
	il2cpp_codegen_marshal_free(_return_value);
	_return_value = NULL;

	// Marshaling cleanup of parameter '___key0' native representation
	il2cpp_codegen_marshal_free(____key0_marshaled);
	____key0_marshaled = NULL;

	return __return_value_unmarshaled;
}
// System.Void LocalyticsUnity.Localytics::_incrementProfileAttribute(System.String,System.Int64,System.Int32)
extern "C" void DEFAULT_CALL _incrementProfileAttribute(char*, int64_t, int32_t);
extern "C"  void Localytics__incrementProfileAttribute_m1877823958 (Il2CppObject * __this /* static, unused */, String_t* ___attributeName0, int64_t ___attributeValue1, int32_t ___scope2, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (char*, int64_t, int32_t);

	// Marshaling of parameter '___attributeName0' to native representation
	char* ____attributeName0_marshaled = NULL;
	____attributeName0_marshaled = il2cpp_codegen_marshal_string(___attributeName0);

	// Marshaling of parameter '___attributeValue1' to native representation

	// Marshaling of parameter '___scope2' to native representation

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(_incrementProfileAttribute)(____attributeName0_marshaled, ___attributeValue1, ___scope2);

	// Marshaling cleanup of parameter '___attributeName0' native representation
	il2cpp_codegen_marshal_free(____attributeName0_marshaled);
	____attributeName0_marshaled = NULL;

	// Marshaling cleanup of parameter '___attributeValue1' native representation

	// Marshaling cleanup of parameter '___scope2' native representation

}
// System.Void LocalyticsUnity.Localytics::_openSession()
extern "C" void DEFAULT_CALL _openSession();
extern "C"  void Localytics__openSession_m535636917 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) ();

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(_openSession)();

}
// System.Void LocalyticsUnity.Localytics::_registerReceiveAnalyticsCallback(LocalyticsUnity.Localytics/ReceiveAnalyticsDelegate)
extern "C" void DEFAULT_CALL _registerReceiveAnalyticsCallback(Il2CppMethodPointer);
extern "C"  void Localytics__registerReceiveAnalyticsCallback_m3272957910 (Il2CppObject * __this /* static, unused */, ReceiveAnalyticsDelegate_t2804656808 * ___callback0, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (Il2CppMethodPointer);

	// Marshaling of parameter '___callback0' to native representation
	Il2CppMethodPointer ____callback0_marshaled = NULL;
	____callback0_marshaled = il2cpp_codegen_marshal_delegate(reinterpret_cast<Il2CppCodeGenMulticastDelegate*>(___callback0));

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(_registerReceiveAnalyticsCallback)(____callback0_marshaled);

	// Marshaling cleanup of parameter '___callback0' native representation

}
// System.Void LocalyticsUnity.Localytics::_removeAnalyticsCallback()
extern "C" void DEFAULT_CALL _removeAnalyticsCallback();
extern "C"  void Localytics__removeAnalyticsCallback_m3153879504 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) ();

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(_removeAnalyticsCallback)();

}
// System.Boolean LocalyticsUnity.Localytics::_isInAppAdIdParameterEnabled()
extern "C" int32_t DEFAULT_CALL _isInAppAdIdParameterEnabled();
extern "C"  bool Localytics__isInAppAdIdParameterEnabled_m475762789 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef int32_t (DEFAULT_CALL *PInvokeFunc) ();

	// Native function invocation and marshaling of return value back from native representation
	int32_t _return_value = reinterpret_cast<PInvokeFunc>(_isInAppAdIdParameterEnabled)();

	return _return_value;
}
// System.Void LocalyticsUnity.Localytics::_setInAppAdIdParameterEnabled(System.Boolean)
extern "C" void DEFAULT_CALL _setInAppAdIdParameterEnabled(int32_t);
extern "C"  void Localytics__setInAppAdIdParameterEnabled_m698126752 (Il2CppObject * __this /* static, unused */, bool ___appAdIdEnabled0, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (int32_t);

	// Marshaling of parameter '___appAdIdEnabled0' to native representation

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(_setInAppAdIdParameterEnabled)(___appAdIdEnabled0);

	// Marshaling cleanup of parameter '___appAdIdEnabled0' native representation

}
// System.Void LocalyticsUnity.Localytics::_registerReceiveMessagingCallback(LocalyticsUnity.Localytics/ReceiveMessagingDelegate)
extern "C" void DEFAULT_CALL _registerReceiveMessagingCallback(Il2CppMethodPointer);
extern "C"  void Localytics__registerReceiveMessagingCallback_m3957393242 (Il2CppObject * __this /* static, unused */, ReceiveMessagingDelegate_t3408128038 * ___callback0, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (Il2CppMethodPointer);

	// Marshaling of parameter '___callback0' to native representation
	Il2CppMethodPointer ____callback0_marshaled = NULL;
	____callback0_marshaled = il2cpp_codegen_marshal_delegate(reinterpret_cast<Il2CppCodeGenMulticastDelegate*>(___callback0));

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(_registerReceiveMessagingCallback)(____callback0_marshaled);

	// Marshaling cleanup of parameter '___callback0' native representation

}
// System.Void LocalyticsUnity.Localytics::_removeMessagingCallback()
extern "C" void DEFAULT_CALL _removeMessagingCallback();
extern "C"  void Localytics__removeMessagingCallback_m3269146574 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) ();

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(_removeMessagingCallback)();

}
// System.Void LocalyticsUnity.Localytics::_removeProfileAttributesFromSet(System.String,System.String,System.Int32)
extern "C" void DEFAULT_CALL _removeProfileAttributesFromSet(char*, char*, int32_t);
extern "C"  void Localytics__removeProfileAttributesFromSet_m391145998 (Il2CppObject * __this /* static, unused */, String_t* ___attributeName0, String_t* ___attributeValue1, int32_t ___scope2, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (char*, char*, int32_t);

	// Marshaling of parameter '___attributeName0' to native representation
	char* ____attributeName0_marshaled = NULL;
	____attributeName0_marshaled = il2cpp_codegen_marshal_string(___attributeName0);

	// Marshaling of parameter '___attributeValue1' to native representation
	char* ____attributeValue1_marshaled = NULL;
	____attributeValue1_marshaled = il2cpp_codegen_marshal_string(___attributeValue1);

	// Marshaling of parameter '___scope2' to native representation

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(_removeProfileAttributesFromSet)(____attributeName0_marshaled, ____attributeValue1_marshaled, ___scope2);

	// Marshaling cleanup of parameter '___attributeName0' native representation
	il2cpp_codegen_marshal_free(____attributeName0_marshaled);
	____attributeName0_marshaled = NULL;

	// Marshaling cleanup of parameter '___attributeValue1' native representation
	il2cpp_codegen_marshal_free(____attributeValue1_marshaled);
	____attributeValue1_marshaled = NULL;

	// Marshaling cleanup of parameter '___scope2' native representation

}
// System.Void LocalyticsUnity.Localytics::_setCustomDimension(System.Int32,System.String)
extern "C" void DEFAULT_CALL _setCustomDimension(int32_t, char*);
extern "C"  void Localytics__setCustomDimension_m1383663481 (Il2CppObject * __this /* static, unused */, int32_t ___dimension0, String_t* ___value1, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (int32_t, char*);

	// Marshaling of parameter '___dimension0' to native representation

	// Marshaling of parameter '___value1' to native representation
	char* ____value1_marshaled = NULL;
	____value1_marshaled = il2cpp_codegen_marshal_string(___value1);

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(_setCustomDimension)(___dimension0, ____value1_marshaled);

	// Marshaling cleanup of parameter '___dimension0' native representation

	// Marshaling cleanup of parameter '___value1' native representation
	il2cpp_codegen_marshal_free(____value1_marshaled);
	____value1_marshaled = NULL;

}
// System.Void LocalyticsUnity.Localytics::_setIdentifier(System.String,System.String)
extern "C" void DEFAULT_CALL _setIdentifier(char*, char*);
extern "C"  void Localytics__setIdentifier_m393772938 (Il2CppObject * __this /* static, unused */, String_t* ___key0, String_t* ___value1, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (char*, char*);

	// Marshaling of parameter '___key0' to native representation
	char* ____key0_marshaled = NULL;
	____key0_marshaled = il2cpp_codegen_marshal_string(___key0);

	// Marshaling of parameter '___value1' to native representation
	char* ____value1_marshaled = NULL;
	____value1_marshaled = il2cpp_codegen_marshal_string(___value1);

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(_setIdentifier)(____key0_marshaled, ____value1_marshaled);

	// Marshaling cleanup of parameter '___key0' native representation
	il2cpp_codegen_marshal_free(____key0_marshaled);
	____key0_marshaled = NULL;

	// Marshaling cleanup of parameter '___value1' native representation
	il2cpp_codegen_marshal_free(____value1_marshaled);
	____value1_marshaled = NULL;

}
// System.Void LocalyticsUnity.Localytics::_setLocation(System.Double,System.Double)
extern "C" void DEFAULT_CALL _setLocation(double, double);
extern "C"  void Localytics__setLocation_m2200217086 (Il2CppObject * __this /* static, unused */, double ___latitude0, double ___longitude1, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (double, double);

	// Marshaling of parameter '___latitude0' to native representation

	// Marshaling of parameter '___longitude1' to native representation

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(_setLocation)(___latitude0, ___longitude1);

	// Marshaling cleanup of parameter '___latitude0' native representation

	// Marshaling cleanup of parameter '___longitude1' native representation

}
// System.Void LocalyticsUnity.Localytics::_setProfileAttribute(System.String,System.String,System.Int32)
extern "C" void DEFAULT_CALL _setProfileAttribute(char*, char*, int32_t);
extern "C"  void Localytics__setProfileAttribute_m1689924567 (Il2CppObject * __this /* static, unused */, String_t* ___attributeName0, String_t* ___values1, int32_t ___scope2, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (char*, char*, int32_t);

	// Marshaling of parameter '___attributeName0' to native representation
	char* ____attributeName0_marshaled = NULL;
	____attributeName0_marshaled = il2cpp_codegen_marshal_string(___attributeName0);

	// Marshaling of parameter '___values1' to native representation
	char* ____values1_marshaled = NULL;
	____values1_marshaled = il2cpp_codegen_marshal_string(___values1);

	// Marshaling of parameter '___scope2' to native representation

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(_setProfileAttribute)(____attributeName0_marshaled, ____values1_marshaled, ___scope2);

	// Marshaling cleanup of parameter '___attributeName0' native representation
	il2cpp_codegen_marshal_free(____attributeName0_marshaled);
	____attributeName0_marshaled = NULL;

	// Marshaling cleanup of parameter '___values1' native representation
	il2cpp_codegen_marshal_free(____values1_marshaled);
	____values1_marshaled = NULL;

	// Marshaling cleanup of parameter '___scope2' native representation

}
// System.Void LocalyticsUnity.Localytics::_tagEvent(System.String,System.String,System.Int64)
extern "C" void DEFAULT_CALL _tagEvent(char*, char*, int64_t);
extern "C"  void Localytics__tagEvent_m2942919859 (Il2CppObject * __this /* static, unused */, String_t* ___eventName0, String_t* ___attributes1, int64_t ___customerValueIncrease2, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (char*, char*, int64_t);

	// Marshaling of parameter '___eventName0' to native representation
	char* ____eventName0_marshaled = NULL;
	____eventName0_marshaled = il2cpp_codegen_marshal_string(___eventName0);

	// Marshaling of parameter '___attributes1' to native representation
	char* ____attributes1_marshaled = NULL;
	____attributes1_marshaled = il2cpp_codegen_marshal_string(___attributes1);

	// Marshaling of parameter '___customerValueIncrease2' to native representation

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(_tagEvent)(____eventName0_marshaled, ____attributes1_marshaled, ___customerValueIncrease2);

	// Marshaling cleanup of parameter '___eventName0' native representation
	il2cpp_codegen_marshal_free(____eventName0_marshaled);
	____eventName0_marshaled = NULL;

	// Marshaling cleanup of parameter '___attributes1' native representation
	il2cpp_codegen_marshal_free(____attributes1_marshaled);
	____attributes1_marshaled = NULL;

	// Marshaling cleanup of parameter '___customerValueIncrease2' native representation

}
// System.Void LocalyticsUnity.Localytics::_tagScreen(System.String)
extern "C" void DEFAULT_CALL _tagScreen(char*);
extern "C"  void Localytics__tagScreen_m3783901299 (Il2CppObject * __this /* static, unused */, String_t* ___screen0, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (char*);

	// Marshaling of parameter '___screen0' to native representation
	char* ____screen0_marshaled = NULL;
	____screen0_marshaled = il2cpp_codegen_marshal_string(___screen0);

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(_tagScreen)(____screen0_marshaled);

	// Marshaling cleanup of parameter '___screen0' native representation
	il2cpp_codegen_marshal_free(____screen0_marshaled);
	____screen0_marshaled = NULL;

}
// System.Void LocalyticsUnity.Localytics::_triggerInAppMessage(System.String,System.String)
extern "C" void DEFAULT_CALL _triggerInAppMessage(char*, char*);
extern "C"  void Localytics__triggerInAppMessage_m318372786 (Il2CppObject * __this /* static, unused */, String_t* ___triggerName0, String_t* ___attributes1, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (char*, char*);

	// Marshaling of parameter '___triggerName0' to native representation
	char* ____triggerName0_marshaled = NULL;
	____triggerName0_marshaled = il2cpp_codegen_marshal_string(___triggerName0);

	// Marshaling of parameter '___attributes1' to native representation
	char* ____attributes1_marshaled = NULL;
	____attributes1_marshaled = il2cpp_codegen_marshal_string(___attributes1);

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(_triggerInAppMessage)(____triggerName0_marshaled, ____attributes1_marshaled);

	// Marshaling cleanup of parameter '___triggerName0' native representation
	il2cpp_codegen_marshal_free(____triggerName0_marshaled);
	____triggerName0_marshaled = NULL;

	// Marshaling cleanup of parameter '___attributes1' native representation
	il2cpp_codegen_marshal_free(____attributes1_marshaled);
	____attributes1_marshaled = NULL;

}
// System.Void LocalyticsUnity.Localytics::_upload()
extern "C" void DEFAULT_CALL _upload();
extern "C"  void Localytics__upload_m533411066 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) ();

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(_upload)();

}
// System.Void LocalyticsUnity.Localytics::ReceiveAnalyticsMessage(System.String)
void STDCALL native_delegate_wrapper_Localytics_ReceiveAnalyticsMessage_m3745756772(char* ___message0)
{
	il2cpp_native_wrapper_vm_thread_attacher _vmThreadHelper;
	// Marshaling of parameter '___message0' to managed representation
	String_t* ____message0_unmarshaled = NULL;
	____message0_unmarshaled = il2cpp_codegen_marshal_string_result(___message0);

	Localytics_ReceiveAnalyticsMessage_m3745756772(NULL, ____message0_unmarshaled, NULL);

	// Marshaling of parameter '___message0' to native representation

}
extern const Il2CppMethodPointer* native_delegate_wrapper_Localytics_ReceiveAnalyticsMessage_m3745756772_indirect = (const Il2CppMethodPointer*)&native_delegate_wrapper_Localytics_ReceiveAnalyticsMessage_m3745756772;
extern Il2CppClass* MiniJSON_t1258204565_il2cpp_TypeInfo_var;
extern Il2CppClass* Hashtable_t3875263730_il2cpp_TypeInfo_var;
extern Il2CppClass* Localytics_t1579915497_il2cpp_TypeInfo_var;
extern Il2CppClass* Dictionary_2_t190145395_il2cpp_TypeInfo_var;
extern Il2CppClass* Boolean_t211005341_il2cpp_TypeInfo_var;
extern Il2CppClass* Dictionary_2_t2606186806_il2cpp_TypeInfo_var;
extern Il2CppClass* IEnumerable_t287189635_il2cpp_TypeInfo_var;
extern Il2CppClass* IEnumerator_t287207039_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* IDisposable_t1628921374_il2cpp_TypeInfo_var;
extern Il2CppClass* Exception_t1967233988_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t1588791936_il2cpp_TypeInfo_var;
extern const MethodInfo* Dictionary_2__ctor_m1958628151_MethodInfo_var;
extern const MethodInfo* Dictionary_2__ctor_m640701813_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral96891546;
extern Il2CppCodeGenString* _stringLiteral534812767;
extern Il2CppCodeGenString* _stringLiteral2265993190;
extern Il2CppCodeGenString* _stringLiteral4107682220;
extern Il2CppCodeGenString* _stringLiteral3683102339;
extern Il2CppCodeGenString* _stringLiteral3299539334;
extern Il2CppCodeGenString* _stringLiteral2058846118;
extern Il2CppCodeGenString* _stringLiteral3484017042;
extern Il2CppCodeGenString* _stringLiteral4034574839;
extern Il2CppCodeGenString* _stringLiteral31228997;
extern Il2CppCodeGenString* _stringLiteral405645655;
extern Il2CppCodeGenString* _stringLiteral1203919765;
extern Il2CppCodeGenString* _stringLiteral3811857701;
extern const uint32_t Localytics_ReceiveAnalyticsMessage_m3745756772_MetadataUsageId;
extern "C"  void Localytics_ReceiveAnalyticsMessage_m3745756772 (Il2CppObject * __this /* static, unused */, String_t* ___message0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Localytics_ReceiveAnalyticsMessage_m3745756772_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Hashtable_t3875263730 * V_0 = NULL;
	String_t* V_1 = NULL;
	Hashtable_t3875263730 * V_2 = NULL;
	bool V_3 = false;
	bool V_4 = false;
	bool V_5 = false;
	Hashtable_t3875263730 * V_6 = NULL;
	bool V_7 = false;
	bool V_8 = false;
	bool V_9 = false;
	Hashtable_t3875263730 * V_10 = NULL;
	String_t* V_11 = NULL;
	Dictionary_2_t2606186806 * V_12 = NULL;
	Hashtable_t3875263730 * V_13 = NULL;
	Il2CppObject * V_14 = NULL;
	Il2CppObject * V_15 = NULL;
	int64_t V_16 = 0;
	Exception_t1967233988 * V_17 = NULL;
	String_t* V_18 = NULL;
	Dictionary_2_t190145395 * V_19 = NULL;
	int32_t V_20 = 0;
	Il2CppObject * V_21 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		{
			String_t* L_0 = ___message0;
			IL2CPP_RUNTIME_CLASS_INIT(MiniJSON_t1258204565_il2cpp_TypeInfo_var);
			Il2CppObject * L_1 = MiniJSON_jsonDecode_m3895311043(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
			V_0 = ((Hashtable_t3875263730 *)CastclassClass(L_1, Hashtable_t3875263730_il2cpp_TypeInfo_var));
			Hashtable_t3875263730 * L_2 = V_0;
			NullCheck(L_2);
			Il2CppObject * L_3 = VirtFuncInvoker1< Il2CppObject *, Il2CppObject * >::Invoke(30 /* System.Object System.Collections.Hashtable::get_Item(System.Object) */, L_2, _stringLiteral96891546);
			NullCheck(L_3);
			String_t* L_4 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_3);
			V_1 = L_4;
			String_t* L_5 = V_1;
			V_18 = L_5;
			String_t* L_6 = V_18;
			if (!L_6)
			{
				goto IL_02a3;
			}
		}

IL_0027:
		{
			Dictionary_2_t190145395 * L_7 = ((Localytics_t1579915497_StaticFields*)Localytics_t1579915497_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__switchU24map1_8();
			if (L_7)
			{
				goto IL_0074;
			}
		}

IL_0031:
		{
			Dictionary_2_t190145395 * L_8 = (Dictionary_2_t190145395 *)il2cpp_codegen_object_new(Dictionary_2_t190145395_il2cpp_TypeInfo_var);
			Dictionary_2__ctor_m1958628151(L_8, 4, /*hidden argument*/Dictionary_2__ctor_m1958628151_MethodInfo_var);
			V_19 = L_8;
			Dictionary_2_t190145395 * L_9 = V_19;
			NullCheck(L_9);
			VirtActionInvoker2< String_t*, int32_t >::Invoke(18 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::Add(!0,!1) */, L_9, _stringLiteral534812767, 0);
			Dictionary_2_t190145395 * L_10 = V_19;
			NullCheck(L_10);
			VirtActionInvoker2< String_t*, int32_t >::Invoke(18 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::Add(!0,!1) */, L_10, _stringLiteral2265993190, 1);
			Dictionary_2_t190145395 * L_11 = V_19;
			NullCheck(L_11);
			VirtActionInvoker2< String_t*, int32_t >::Invoke(18 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::Add(!0,!1) */, L_11, _stringLiteral4107682220, 2);
			Dictionary_2_t190145395 * L_12 = V_19;
			NullCheck(L_12);
			VirtActionInvoker2< String_t*, int32_t >::Invoke(18 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::Add(!0,!1) */, L_12, _stringLiteral3683102339, 3);
			Dictionary_2_t190145395 * L_13 = V_19;
			((Localytics_t1579915497_StaticFields*)Localytics_t1579915497_il2cpp_TypeInfo_var->static_fields)->set_U3CU3Ef__switchU24map1_8(L_13);
		}

IL_0074:
		{
			Dictionary_2_t190145395 * L_14 = ((Localytics_t1579915497_StaticFields*)Localytics_t1579915497_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__switchU24map1_8();
			String_t* L_15 = V_18;
			NullCheck(L_14);
			bool L_16 = VirtFuncInvoker2< bool, String_t*, int32_t* >::Invoke(21 /* System.Boolean System.Collections.Generic.Dictionary`2<System.String,System.Int32>::TryGetValue(!0,!1&) */, L_14, L_15, (&V_20));
			if (!L_16)
			{
				goto IL_02a3;
			}
		}

IL_0087:
		{
			int32_t L_17 = V_20;
			if (L_17 == 0)
			{
				goto IL_00a3;
			}
			if (L_17 == 1)
			{
				goto IL_0116;
			}
			if (L_17 == 2)
			{
				goto IL_018f;
			}
			if (L_17 == 3)
			{
				goto IL_028a;
			}
		}

IL_009e:
		{
			goto IL_02a3;
		}

IL_00a3:
		{
			Hashtable_t3875263730 * L_18 = V_0;
			NullCheck(L_18);
			Il2CppObject * L_19 = VirtFuncInvoker1< Il2CppObject *, Il2CppObject * >::Invoke(30 /* System.Object System.Collections.Hashtable::get_Item(System.Object) */, L_18, _stringLiteral3299539334);
			V_2 = ((Hashtable_t3875263730 *)CastclassClass(L_19, Hashtable_t3875263730_il2cpp_TypeInfo_var));
			Hashtable_t3875263730 * L_20 = V_2;
			NullCheck(L_20);
			Il2CppObject * L_21 = VirtFuncInvoker1< Il2CppObject *, Il2CppObject * >::Invoke(30 /* System.Object System.Collections.Hashtable::get_Item(System.Object) */, L_20, _stringLiteral2058846118);
			NullCheck(L_21);
			String_t* L_22 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_21);
			IL2CPP_RUNTIME_CLASS_INIT(Boolean_t211005341_il2cpp_TypeInfo_var);
			bool L_23 = Boolean_Parse_m3007515274(NULL /*static, unused*/, L_22, /*hidden argument*/NULL);
			V_3 = L_23;
			Hashtable_t3875263730 * L_24 = V_2;
			NullCheck(L_24);
			Il2CppObject * L_25 = VirtFuncInvoker1< Il2CppObject *, Il2CppObject * >::Invoke(30 /* System.Object System.Collections.Hashtable::get_Item(System.Object) */, L_24, _stringLiteral3484017042);
			NullCheck(L_25);
			String_t* L_26 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_25);
			bool L_27 = Boolean_Parse_m3007515274(NULL /*static, unused*/, L_26, /*hidden argument*/NULL);
			V_4 = L_27;
			Hashtable_t3875263730 * L_28 = V_2;
			NullCheck(L_28);
			Il2CppObject * L_29 = VirtFuncInvoker1< Il2CppObject *, Il2CppObject * >::Invoke(30 /* System.Object System.Collections.Hashtable::get_Item(System.Object) */, L_28, _stringLiteral4034574839);
			NullCheck(L_29);
			String_t* L_30 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_29);
			bool L_31 = Boolean_Parse_m3007515274(NULL /*static, unused*/, L_30, /*hidden argument*/NULL);
			V_5 = L_31;
			LocalyticsSessionWillOpen_t2581002303 * L_32 = ((Localytics_t1579915497_StaticFields*)Localytics_t1579915497_il2cpp_TypeInfo_var->static_fields)->get_OnLocalyticsSessionWillOpen_3();
			if (!L_32)
			{
				goto IL_0111;
			}
		}

IL_0102:
		{
			LocalyticsSessionWillOpen_t2581002303 * L_33 = ((Localytics_t1579915497_StaticFields*)Localytics_t1579915497_il2cpp_TypeInfo_var->static_fields)->get_OnLocalyticsSessionWillOpen_3();
			bool L_34 = V_3;
			bool L_35 = V_4;
			bool L_36 = V_5;
			NullCheck(L_33);
			LocalyticsSessionWillOpen_Invoke_m2750767463(L_33, L_34, L_35, L_36, /*hidden argument*/NULL);
		}

IL_0111:
		{
			goto IL_02a3;
		}

IL_0116:
		{
			Hashtable_t3875263730 * L_37 = V_0;
			NullCheck(L_37);
			Il2CppObject * L_38 = VirtFuncInvoker1< Il2CppObject *, Il2CppObject * >::Invoke(30 /* System.Object System.Collections.Hashtable::get_Item(System.Object) */, L_37, _stringLiteral3299539334);
			V_6 = ((Hashtable_t3875263730 *)CastclassClass(L_38, Hashtable_t3875263730_il2cpp_TypeInfo_var));
			Hashtable_t3875263730 * L_39 = V_6;
			NullCheck(L_39);
			Il2CppObject * L_40 = VirtFuncInvoker1< Il2CppObject *, Il2CppObject * >::Invoke(30 /* System.Object System.Collections.Hashtable::get_Item(System.Object) */, L_39, _stringLiteral2058846118);
			NullCheck(L_40);
			String_t* L_41 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_40);
			IL2CPP_RUNTIME_CLASS_INIT(Boolean_t211005341_il2cpp_TypeInfo_var);
			bool L_42 = Boolean_Parse_m3007515274(NULL /*static, unused*/, L_41, /*hidden argument*/NULL);
			V_7 = L_42;
			Hashtable_t3875263730 * L_43 = V_6;
			NullCheck(L_43);
			Il2CppObject * L_44 = VirtFuncInvoker1< Il2CppObject *, Il2CppObject * >::Invoke(30 /* System.Object System.Collections.Hashtable::get_Item(System.Object) */, L_43, _stringLiteral3484017042);
			NullCheck(L_44);
			String_t* L_45 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_44);
			bool L_46 = Boolean_Parse_m3007515274(NULL /*static, unused*/, L_45, /*hidden argument*/NULL);
			V_8 = L_46;
			Hashtable_t3875263730 * L_47 = V_6;
			NullCheck(L_47);
			Il2CppObject * L_48 = VirtFuncInvoker1< Il2CppObject *, Il2CppObject * >::Invoke(30 /* System.Object System.Collections.Hashtable::get_Item(System.Object) */, L_47, _stringLiteral4034574839);
			NullCheck(L_48);
			String_t* L_49 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_48);
			bool L_50 = Boolean_Parse_m3007515274(NULL /*static, unused*/, L_49, /*hidden argument*/NULL);
			V_9 = L_50;
			LocalyticsSessionDidOpen_t115241990 * L_51 = ((Localytics_t1579915497_StaticFields*)Localytics_t1579915497_il2cpp_TypeInfo_var->static_fields)->get_OnLocalyticsSessionDidOpen_1();
			if (!L_51)
			{
				goto IL_018a;
			}
		}

IL_017a:
		{
			LocalyticsSessionDidOpen_t115241990 * L_52 = ((Localytics_t1579915497_StaticFields*)Localytics_t1579915497_il2cpp_TypeInfo_var->static_fields)->get_OnLocalyticsSessionDidOpen_1();
			bool L_53 = V_7;
			bool L_54 = V_8;
			bool L_55 = V_9;
			NullCheck(L_52);
			LocalyticsSessionDidOpen_Invoke_m2858247166(L_52, L_53, L_54, L_55, /*hidden argument*/NULL);
		}

IL_018a:
		{
			goto IL_02a3;
		}

IL_018f:
		{
			Hashtable_t3875263730 * L_56 = V_0;
			NullCheck(L_56);
			Il2CppObject * L_57 = VirtFuncInvoker1< Il2CppObject *, Il2CppObject * >::Invoke(30 /* System.Object System.Collections.Hashtable::get_Item(System.Object) */, L_56, _stringLiteral3299539334);
			V_10 = ((Hashtable_t3875263730 *)CastclassClass(L_57, Hashtable_t3875263730_il2cpp_TypeInfo_var));
			Hashtable_t3875263730 * L_58 = V_10;
			NullCheck(L_58);
			Il2CppObject * L_59 = VirtFuncInvoker1< Il2CppObject *, Il2CppObject * >::Invoke(30 /* System.Object System.Collections.Hashtable::get_Item(System.Object) */, L_58, _stringLiteral31228997);
			NullCheck(L_59);
			String_t* L_60 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_59);
			V_11 = L_60;
			Dictionary_2_t2606186806 * L_61 = (Dictionary_2_t2606186806 *)il2cpp_codegen_object_new(Dictionary_2_t2606186806_il2cpp_TypeInfo_var);
			Dictionary_2__ctor_m640701813(L_61, /*hidden argument*/Dictionary_2__ctor_m640701813_MethodInfo_var);
			V_12 = L_61;
			Hashtable_t3875263730 * L_62 = V_10;
			NullCheck(L_62);
			bool L_63 = VirtFuncInvoker1< bool, Il2CppObject * >::Invoke(38 /* System.Boolean System.Collections.Hashtable::ContainsKey(System.Object) */, L_62, _stringLiteral405645655);
			if (!L_63)
			{
				goto IL_023e;
			}
		}

IL_01cc:
		{
			Hashtable_t3875263730 * L_64 = V_10;
			NullCheck(L_64);
			Il2CppObject * L_65 = VirtFuncInvoker1< Il2CppObject *, Il2CppObject * >::Invoke(30 /* System.Object System.Collections.Hashtable::get_Item(System.Object) */, L_64, _stringLiteral405645655);
			V_13 = ((Hashtable_t3875263730 *)CastclassClass(L_65, Hashtable_t3875263730_il2cpp_TypeInfo_var));
			Hashtable_t3875263730 * L_66 = V_13;
			NullCheck(L_66);
			Il2CppObject * L_67 = VirtFuncInvoker0< Il2CppObject * >::Invoke(28 /* System.Collections.ICollection System.Collections.Hashtable::get_Keys() */, L_66);
			NullCheck(L_67);
			Il2CppObject * L_68 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(0 /* System.Collections.IEnumerator System.Collections.IEnumerable::GetEnumerator() */, IEnumerable_t287189635_il2cpp_TypeInfo_var, L_67);
			V_15 = L_68;
		}

IL_01ed:
		try
		{ // begin try (depth: 2)
			{
				goto IL_0217;
			}

IL_01f2:
			{
				Il2CppObject * L_69 = V_15;
				NullCheck(L_69);
				Il2CppObject * L_70 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(0 /* System.Object System.Collections.IEnumerator::get_Current() */, IEnumerator_t287207039_il2cpp_TypeInfo_var, L_69);
				V_14 = L_70;
				Dictionary_2_t2606186806 * L_71 = V_12;
				Il2CppObject * L_72 = V_14;
				Hashtable_t3875263730 * L_73 = V_13;
				Il2CppObject * L_74 = V_14;
				NullCheck(L_73);
				Il2CppObject * L_75 = VirtFuncInvoker1< Il2CppObject *, Il2CppObject * >::Invoke(30 /* System.Object System.Collections.Hashtable::get_Item(System.Object) */, L_73, L_74);
				NullCheck(L_71);
				VirtActionInvoker2< String_t*, String_t* >::Invoke(18 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.String>::Add(!0,!1) */, L_71, ((String_t*)CastclassSealed(L_72, String_t_il2cpp_TypeInfo_var)), ((String_t*)CastclassSealed(L_75, String_t_il2cpp_TypeInfo_var)));
			}

IL_0217:
			{
				Il2CppObject * L_76 = V_15;
				NullCheck(L_76);
				bool L_77 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t287207039_il2cpp_TypeInfo_var, L_76);
				if (L_77)
				{
					goto IL_01f2;
				}
			}

IL_0223:
			{
				IL2CPP_LEAVE(0x23E, FINALLY_0228);
			}
		} // end try (depth: 2)
		catch(Il2CppExceptionWrapper& e)
		{
			__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
			goto FINALLY_0228;
		}

FINALLY_0228:
		{ // begin finally (depth: 2)
			{
				Il2CppObject * L_78 = V_15;
				V_21 = ((Il2CppObject *)IsInst(L_78, IDisposable_t1628921374_il2cpp_TypeInfo_var));
				Il2CppObject * L_79 = V_21;
				if (L_79)
				{
					goto IL_0236;
				}
			}

IL_0235:
			{
				IL2CPP_END_FINALLY(552)
			}

IL_0236:
			{
				Il2CppObject * L_80 = V_21;
				NullCheck(L_80);
				InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, L_80);
				IL2CPP_END_FINALLY(552)
			}
		} // end finally (depth: 2)
		IL2CPP_CLEANUP(552)
		{
			IL2CPP_JUMP_TBL(0x23E, IL_023e)
			IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
		}

IL_023e:
		{
			V_16 = (((int64_t)((int64_t)0)));
			Hashtable_t3875263730 * L_81 = V_10;
			NullCheck(L_81);
			bool L_82 = VirtFuncInvoker1< bool, Il2CppObject * >::Invoke(38 /* System.Boolean System.Collections.Hashtable::ContainsKey(System.Object) */, L_81, _stringLiteral1203919765);
			if (!L_82)
			{
				goto IL_026b;
			}
		}

IL_0253:
		{
			Hashtable_t3875263730 * L_83 = V_10;
			NullCheck(L_83);
			Il2CppObject * L_84 = VirtFuncInvoker1< Il2CppObject *, Il2CppObject * >::Invoke(30 /* System.Object System.Collections.Hashtable::get_Item(System.Object) */, L_83, _stringLiteral1203919765);
			NullCheck(L_84);
			String_t* L_85 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_84);
			int64_t L_86 = Int64_Parse_m2426231402(NULL /*static, unused*/, L_85, /*hidden argument*/NULL);
			V_16 = L_86;
		}

IL_026b:
		{
			LocalyticsDidTagEvent_t1639420300 * L_87 = ((Localytics_t1579915497_StaticFields*)Localytics_t1579915497_il2cpp_TypeInfo_var->static_fields)->get_OnLocalyticsDidTagEvent_0();
			if (!L_87)
			{
				goto IL_0285;
			}
		}

IL_0275:
		{
			LocalyticsDidTagEvent_t1639420300 * L_88 = ((Localytics_t1579915497_StaticFields*)Localytics_t1579915497_il2cpp_TypeInfo_var->static_fields)->get_OnLocalyticsDidTagEvent_0();
			String_t* L_89 = V_11;
			Dictionary_2_t2606186806 * L_90 = V_12;
			int64_t L_91 = V_16;
			NullCheck(L_88);
			LocalyticsDidTagEvent_Invoke_m3644200710(L_88, L_89, L_90, L_91, /*hidden argument*/NULL);
		}

IL_0285:
		{
			goto IL_02a3;
		}

IL_028a:
		{
			LocalyticsSessionWillClose_t2690468515 * L_92 = ((Localytics_t1579915497_StaticFields*)Localytics_t1579915497_il2cpp_TypeInfo_var->static_fields)->get_OnLocalyticsSessionWillClose_2();
			if (!L_92)
			{
				goto IL_029e;
			}
		}

IL_0294:
		{
			LocalyticsSessionWillClose_t2690468515 * L_93 = ((Localytics_t1579915497_StaticFields*)Localytics_t1579915497_il2cpp_TypeInfo_var->static_fields)->get_OnLocalyticsSessionWillClose_2();
			NullCheck(L_93);
			LocalyticsSessionWillClose_Invoke_m2626000196(L_93, /*hidden argument*/NULL);
		}

IL_029e:
		{
			goto IL_02a3;
		}

IL_02a3:
		{
			goto IL_02c5;
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1967233988 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Exception_t1967233988_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_02a8;
		throw e;
	}

CATCH_02a8:
	{ // begin catch(System.Exception)
		V_17 = ((Exception_t1967233988 *)__exception_local);
		Exception_t1967233988 * L_94 = V_17;
		NullCheck(L_94);
		String_t* L_95 = VirtFuncInvoker0< String_t* >::Invoke(6 /* System.String System.Exception::get_Message() */, L_94);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_96 = String_Concat_m138640077(NULL /*static, unused*/, _stringLiteral3811857701, L_95, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1588791936_il2cpp_TypeInfo_var);
		Debug_LogError_m1836388681(NULL /*static, unused*/, L_96, /*hidden argument*/NULL);
		goto IL_02c5;
	} // end catch (depth: 1)

IL_02c5:
	{
		return;
	}
}
// System.Void LocalyticsUnity.Localytics::ReceiveMessagingMessage(System.String)
void STDCALL native_delegate_wrapper_Localytics_ReceiveMessagingMessage_m864780130(char* ___message0)
{
	il2cpp_native_wrapper_vm_thread_attacher _vmThreadHelper;
	// Marshaling of parameter '___message0' to managed representation
	String_t* ____message0_unmarshaled = NULL;
	____message0_unmarshaled = il2cpp_codegen_marshal_string_result(___message0);

	Localytics_ReceiveMessagingMessage_m864780130(NULL, ____message0_unmarshaled, NULL);

	// Marshaling of parameter '___message0' to native representation

}
extern const Il2CppMethodPointer* native_delegate_wrapper_Localytics_ReceiveMessagingMessage_m864780130_indirect = (const Il2CppMethodPointer*)&native_delegate_wrapper_Localytics_ReceiveMessagingMessage_m864780130;
extern Il2CppClass* MiniJSON_t1258204565_il2cpp_TypeInfo_var;
extern Il2CppClass* Hashtable_t3875263730_il2cpp_TypeInfo_var;
extern Il2CppClass* Localytics_t1579915497_il2cpp_TypeInfo_var;
extern Il2CppClass* Dictionary_2_t190145395_il2cpp_TypeInfo_var;
extern Il2CppClass* Exception_t1967233988_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t1588791936_il2cpp_TypeInfo_var;
extern const MethodInfo* Dictionary_2__ctor_m1958628151_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral96891546;
extern Il2CppCodeGenString* _stringLiteral3163483080;
extern Il2CppCodeGenString* _stringLiteral3714408833;
extern Il2CppCodeGenString* _stringLiteral1422727568;
extern Il2CppCodeGenString* _stringLiteral1973653321;
extern Il2CppCodeGenString* _stringLiteral3811857701;
extern const uint32_t Localytics_ReceiveMessagingMessage_m864780130_MetadataUsageId;
extern "C"  void Localytics_ReceiveMessagingMessage_m864780130 (Il2CppObject * __this /* static, unused */, String_t* ___message0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Localytics_ReceiveMessagingMessage_m864780130_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Hashtable_t3875263730 * V_0 = NULL;
	String_t* V_1 = NULL;
	Exception_t1967233988 * V_2 = NULL;
	String_t* V_3 = NULL;
	Dictionary_2_t190145395 * V_4 = NULL;
	int32_t V_5 = 0;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		{
			String_t* L_0 = ___message0;
			IL2CPP_RUNTIME_CLASS_INIT(MiniJSON_t1258204565_il2cpp_TypeInfo_var);
			Il2CppObject * L_1 = MiniJSON_jsonDecode_m3895311043(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
			V_0 = ((Hashtable_t3875263730 *)CastclassClass(L_1, Hashtable_t3875263730_il2cpp_TypeInfo_var));
			Hashtable_t3875263730 * L_2 = V_0;
			NullCheck(L_2);
			Il2CppObject * L_3 = VirtFuncInvoker1< Il2CppObject *, Il2CppObject * >::Invoke(30 /* System.Object System.Collections.Hashtable::get_Item(System.Object) */, L_2, _stringLiteral96891546);
			NullCheck(L_3);
			String_t* L_4 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_3);
			V_1 = L_4;
			String_t* L_5 = V_1;
			V_3 = L_5;
			String_t* L_6 = V_3;
			if (!L_6)
			{
				goto IL_0104;
			}
		}

IL_0025:
		{
			Dictionary_2_t190145395 * L_7 = ((Localytics_t1579915497_StaticFields*)Localytics_t1579915497_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__switchU24map2_9();
			if (L_7)
			{
				goto IL_0072;
			}
		}

IL_002f:
		{
			Dictionary_2_t190145395 * L_8 = (Dictionary_2_t190145395 *)il2cpp_codegen_object_new(Dictionary_2_t190145395_il2cpp_TypeInfo_var);
			Dictionary_2__ctor_m1958628151(L_8, 4, /*hidden argument*/Dictionary_2__ctor_m1958628151_MethodInfo_var);
			V_4 = L_8;
			Dictionary_2_t190145395 * L_9 = V_4;
			NullCheck(L_9);
			VirtActionInvoker2< String_t*, int32_t >::Invoke(18 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::Add(!0,!1) */, L_9, _stringLiteral3163483080, 0);
			Dictionary_2_t190145395 * L_10 = V_4;
			NullCheck(L_10);
			VirtActionInvoker2< String_t*, int32_t >::Invoke(18 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::Add(!0,!1) */, L_10, _stringLiteral3714408833, 1);
			Dictionary_2_t190145395 * L_11 = V_4;
			NullCheck(L_11);
			VirtActionInvoker2< String_t*, int32_t >::Invoke(18 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::Add(!0,!1) */, L_11, _stringLiteral1422727568, 2);
			Dictionary_2_t190145395 * L_12 = V_4;
			NullCheck(L_12);
			VirtActionInvoker2< String_t*, int32_t >::Invoke(18 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::Add(!0,!1) */, L_12, _stringLiteral1973653321, 3);
			Dictionary_2_t190145395 * L_13 = V_4;
			((Localytics_t1579915497_StaticFields*)Localytics_t1579915497_il2cpp_TypeInfo_var->static_fields)->set_U3CU3Ef__switchU24map2_9(L_13);
		}

IL_0072:
		{
			Dictionary_2_t190145395 * L_14 = ((Localytics_t1579915497_StaticFields*)Localytics_t1579915497_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__switchU24map2_9();
			String_t* L_15 = V_3;
			NullCheck(L_14);
			bool L_16 = VirtFuncInvoker2< bool, String_t*, int32_t* >::Invoke(21 /* System.Boolean System.Collections.Generic.Dictionary`2<System.String,System.Int32>::TryGetValue(!0,!1&) */, L_14, L_15, (&V_5));
			if (!L_16)
			{
				goto IL_0104;
			}
		}

IL_0084:
		{
			int32_t L_17 = V_5;
			if (L_17 == 0)
			{
				goto IL_00a0;
			}
			if (L_17 == 1)
			{
				goto IL_00b9;
			}
			if (L_17 == 2)
			{
				goto IL_00d2;
			}
			if (L_17 == 3)
			{
				goto IL_00eb;
			}
		}

IL_009b:
		{
			goto IL_0104;
		}

IL_00a0:
		{
			LocalyticsWillDisplayInAppMessage_t26700712 * L_18 = ((Localytics_t1579915497_StaticFields*)Localytics_t1579915497_il2cpp_TypeInfo_var->static_fields)->get_OnLocalyticsWillDisplayInAppMessage_7();
			if (!L_18)
			{
				goto IL_00b4;
			}
		}

IL_00aa:
		{
			LocalyticsWillDisplayInAppMessage_t26700712 * L_19 = ((Localytics_t1579915497_StaticFields*)Localytics_t1579915497_il2cpp_TypeInfo_var->static_fields)->get_OnLocalyticsWillDisplayInAppMessage_7();
			NullCheck(L_19);
			LocalyticsWillDisplayInAppMessage_Invoke_m3652641785(L_19, /*hidden argument*/NULL);
		}

IL_00b4:
		{
			goto IL_0104;
		}

IL_00b9:
		{
			LocalyticsDidDisplayInAppMessage_t3613222305 * L_20 = ((Localytics_t1579915497_StaticFields*)Localytics_t1579915497_il2cpp_TypeInfo_var->static_fields)->get_OnLocalyticsDidDisplayInAppMessage_5();
			if (!L_20)
			{
				goto IL_00cd;
			}
		}

IL_00c3:
		{
			LocalyticsDidDisplayInAppMessage_t3613222305 * L_21 = ((Localytics_t1579915497_StaticFields*)Localytics_t1579915497_il2cpp_TypeInfo_var->static_fields)->get_OnLocalyticsDidDisplayInAppMessage_5();
			NullCheck(L_21);
			LocalyticsDidDisplayInAppMessage_Invoke_m4043818178(L_21, /*hidden argument*/NULL);
		}

IL_00cd:
		{
			goto IL_0104;
		}

IL_00d2:
		{
			LocalyticsWillDismissInAppMessage_t2580912496 * L_22 = ((Localytics_t1579915497_StaticFields*)Localytics_t1579915497_il2cpp_TypeInfo_var->static_fields)->get_OnLocalyticsWillDismissInAppMessage_6();
			if (!L_22)
			{
				goto IL_00e6;
			}
		}

IL_00dc:
		{
			LocalyticsWillDismissInAppMessage_t2580912496 * L_23 = ((Localytics_t1579915497_StaticFields*)Localytics_t1579915497_il2cpp_TypeInfo_var->static_fields)->get_OnLocalyticsWillDismissInAppMessage_6();
			NullCheck(L_23);
			LocalyticsWillDismissInAppMessage_Invoke_m1116501953(L_23, /*hidden argument*/NULL);
		}

IL_00e6:
		{
			goto IL_0104;
		}

IL_00eb:
		{
			LocalyticsDidDismissInAppMessage_t1872466793 * L_24 = ((Localytics_t1579915497_StaticFields*)Localytics_t1579915497_il2cpp_TypeInfo_var->static_fields)->get_OnLocalyticsDidDismissInAppMessage_4();
			if (!L_24)
			{
				goto IL_00ff;
			}
		}

IL_00f5:
		{
			LocalyticsDidDismissInAppMessage_t1872466793 * L_25 = ((Localytics_t1579915497_StaticFields*)Localytics_t1579915497_il2cpp_TypeInfo_var->static_fields)->get_OnLocalyticsDidDismissInAppMessage_4();
			NullCheck(L_25);
			LocalyticsDidDismissInAppMessage_Invoke_m1507678346(L_25, /*hidden argument*/NULL);
		}

IL_00ff:
		{
			goto IL_0104;
		}

IL_0104:
		{
			goto IL_0124;
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1967233988 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Exception_t1967233988_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_0109;
		throw e;
	}

CATCH_0109:
	{ // begin catch(System.Exception)
		V_2 = ((Exception_t1967233988 *)__exception_local);
		Exception_t1967233988 * L_26 = V_2;
		NullCheck(L_26);
		String_t* L_27 = VirtFuncInvoker0< String_t* >::Invoke(6 /* System.String System.Exception::get_Message() */, L_26);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_28 = String_Concat_m138640077(NULL /*static, unused*/, _stringLiteral3811857701, L_27, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1588791936_il2cpp_TypeInfo_var);
		Debug_LogError_m1836388681(NULL /*static, unused*/, L_28, /*hidden argument*/NULL);
		goto IL_0124;
	} // end catch (depth: 1)

IL_0124:
	{
		return;
	}
}
// System.Void LocalyticsUnity.Localytics/LocalyticsDidDismissInAppMessage::.ctor(System.Object,System.IntPtr)
extern "C"  void LocalyticsDidDismissInAppMessage__ctor_m3791179824 (LocalyticsDidDismissInAppMessage_t1872466793 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->method);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void LocalyticsUnity.Localytics/LocalyticsDidDismissInAppMessage::Invoke()
extern "C"  void LocalyticsDidDismissInAppMessage_Invoke_m1507678346 (LocalyticsDidDismissInAppMessage_t1872466793 * __this, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		LocalyticsDidDismissInAppMessage_Invoke_m1507678346((LocalyticsDidDismissInAppMessage_t1872466793 *)__this->get_prev_9(), method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if ((__this->get_m_target_2() != NULL || MethodHasParameters((MethodInfo*)(__this->get_method_3().get_m_value_0()))) && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
extern "C" void pinvoke_delegate_wrapper_LocalyticsDidDismissInAppMessage_t1872466793(Il2CppObject* delegate)
{
	typedef void (STDCALL *native_function_ptr_type)();
	native_function_ptr_type _il2cpp_pinvoke_func = ((native_function_ptr_type)((Il2CppDelegate*)delegate)->method->method);
	// Native function invocation
	_il2cpp_pinvoke_func();

}
// System.IAsyncResult LocalyticsUnity.Localytics/LocalyticsDidDismissInAppMessage::BeginInvoke(System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * LocalyticsDidDismissInAppMessage_BeginInvoke_m621475265 (LocalyticsDidDismissInAppMessage_t1872466793 * __this, AsyncCallback_t1363551830 * ___callback0, Il2CppObject * ___object1, const MethodInfo* method)
{
	void *__d_args[1] = {0};
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback0, (Il2CppObject*)___object1);
}
// System.Void LocalyticsUnity.Localytics/LocalyticsDidDismissInAppMessage::EndInvoke(System.IAsyncResult)
extern "C"  void LocalyticsDidDismissInAppMessage_EndInvoke_m840686144 (LocalyticsDidDismissInAppMessage_t1872466793 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void LocalyticsUnity.Localytics/LocalyticsDidDisplayInAppMessage::.ctor(System.Object,System.IntPtr)
extern "C"  void LocalyticsDidDisplayInAppMessage__ctor_m896525928 (LocalyticsDidDisplayInAppMessage_t3613222305 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->method);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void LocalyticsUnity.Localytics/LocalyticsDidDisplayInAppMessage::Invoke()
extern "C"  void LocalyticsDidDisplayInAppMessage_Invoke_m4043818178 (LocalyticsDidDisplayInAppMessage_t3613222305 * __this, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		LocalyticsDidDisplayInAppMessage_Invoke_m4043818178((LocalyticsDidDisplayInAppMessage_t3613222305 *)__this->get_prev_9(), method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if ((__this->get_m_target_2() != NULL || MethodHasParameters((MethodInfo*)(__this->get_method_3().get_m_value_0()))) && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
extern "C" void pinvoke_delegate_wrapper_LocalyticsDidDisplayInAppMessage_t3613222305(Il2CppObject* delegate)
{
	typedef void (STDCALL *native_function_ptr_type)();
	native_function_ptr_type _il2cpp_pinvoke_func = ((native_function_ptr_type)((Il2CppDelegate*)delegate)->method->method);
	// Native function invocation
	_il2cpp_pinvoke_func();

}
// System.IAsyncResult LocalyticsUnity.Localytics/LocalyticsDidDisplayInAppMessage::BeginInvoke(System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * LocalyticsDidDisplayInAppMessage_BeginInvoke_m3805002377 (LocalyticsDidDisplayInAppMessage_t3613222305 * __this, AsyncCallback_t1363551830 * ___callback0, Il2CppObject * ___object1, const MethodInfo* method)
{
	void *__d_args[1] = {0};
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback0, (Il2CppObject*)___object1);
}
// System.Void LocalyticsUnity.Localytics/LocalyticsDidDisplayInAppMessage::EndInvoke(System.IAsyncResult)
extern "C"  void LocalyticsDidDisplayInAppMessage_EndInvoke_m3578186872 (LocalyticsDidDisplayInAppMessage_t3613222305 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void LocalyticsUnity.Localytics/LocalyticsDidTagEvent::.ctor(System.Object,System.IntPtr)
extern "C"  void LocalyticsDidTagEvent__ctor_m3437844163 (LocalyticsDidTagEvent_t1639420300 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->method);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void LocalyticsUnity.Localytics/LocalyticsDidTagEvent::Invoke(System.String,System.Collections.Generic.Dictionary`2<System.String,System.String>,System.Int64)
extern "C"  void LocalyticsDidTagEvent_Invoke_m3644200710 (LocalyticsDidTagEvent_t1639420300 * __this, String_t* ___eventName0, Dictionary_2_t2606186806 * ___attributes1, int64_t ___customerValueIncrease2, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		LocalyticsDidTagEvent_Invoke_m3644200710((LocalyticsDidTagEvent_t1639420300 *)__this->get_prev_9(),___eventName0, ___attributes1, ___customerValueIncrease2, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, String_t* ___eventName0, Dictionary_2_t2606186806 * ___attributes1, int64_t ___customerValueIncrease2, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___eventName0, ___attributes1, ___customerValueIncrease2,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (void* __this, String_t* ___eventName0, Dictionary_2_t2606186806 * ___attributes1, int64_t ___customerValueIncrease2, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___eventName0, ___attributes1, ___customerValueIncrease2,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, Dictionary_2_t2606186806 * ___attributes1, int64_t ___customerValueIncrease2, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(___eventName0, ___attributes1, ___customerValueIncrease2,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult LocalyticsUnity.Localytics/LocalyticsDidTagEvent::BeginInvoke(System.String,System.Collections.Generic.Dictionary`2<System.String,System.String>,System.Int64,System.AsyncCallback,System.Object)
extern Il2CppClass* Int64_t2847414882_il2cpp_TypeInfo_var;
extern const uint32_t LocalyticsDidTagEvent_BeginInvoke_m2045473321_MetadataUsageId;
extern "C"  Il2CppObject * LocalyticsDidTagEvent_BeginInvoke_m2045473321 (LocalyticsDidTagEvent_t1639420300 * __this, String_t* ___eventName0, Dictionary_2_t2606186806 * ___attributes1, int64_t ___customerValueIncrease2, AsyncCallback_t1363551830 * ___callback3, Il2CppObject * ___object4, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (LocalyticsDidTagEvent_BeginInvoke_m2045473321_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[4] = {0};
	__d_args[0] = ___eventName0;
	__d_args[1] = ___attributes1;
	__d_args[2] = Box(Int64_t2847414882_il2cpp_TypeInfo_var, &___customerValueIncrease2);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback3, (Il2CppObject*)___object4);
}
// System.Void LocalyticsUnity.Localytics/LocalyticsDidTagEvent::EndInvoke(System.IAsyncResult)
extern "C"  void LocalyticsDidTagEvent_EndInvoke_m497328723 (LocalyticsDidTagEvent_t1639420300 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void LocalyticsUnity.Localytics/LocalyticsSessionDidOpen::.ctor(System.Object,System.IntPtr)
extern "C"  void LocalyticsSessionDidOpen__ctor_m683175501 (LocalyticsSessionDidOpen_t115241990 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->method);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void LocalyticsUnity.Localytics/LocalyticsSessionDidOpen::Invoke(System.Boolean,System.Boolean,System.Boolean)
extern "C"  void LocalyticsSessionDidOpen_Invoke_m2858247166 (LocalyticsSessionDidOpen_t115241990 * __this, bool ___isFirst0, bool ___isUpgrade1, bool ___isResume2, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		LocalyticsSessionDidOpen_Invoke_m2858247166((LocalyticsSessionDidOpen_t115241990 *)__this->get_prev_9(),___isFirst0, ___isUpgrade1, ___isResume2, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, bool ___isFirst0, bool ___isUpgrade1, bool ___isResume2, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___isFirst0, ___isUpgrade1, ___isResume2,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, bool ___isFirst0, bool ___isUpgrade1, bool ___isResume2, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___isFirst0, ___isUpgrade1, ___isResume2,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
extern "C" void pinvoke_delegate_wrapper_LocalyticsSessionDidOpen_t115241990(Il2CppObject* delegate, bool ___isFirst0, bool ___isUpgrade1, bool ___isResume2)
{
	typedef void (STDCALL *native_function_ptr_type)(int32_t, int32_t, int32_t);
	native_function_ptr_type _il2cpp_pinvoke_func = ((native_function_ptr_type)((Il2CppDelegate*)delegate)->method->method);
	// Marshaling of parameter '___isFirst0' to native representation

	// Marshaling of parameter '___isUpgrade1' to native representation

	// Marshaling of parameter '___isResume2' to native representation

	// Native function invocation
	_il2cpp_pinvoke_func(___isFirst0, ___isUpgrade1, ___isResume2);

	// Marshaling cleanup of parameter '___isFirst0' native representation

	// Marshaling cleanup of parameter '___isUpgrade1' native representation

	// Marshaling cleanup of parameter '___isResume2' native representation

}
// System.IAsyncResult LocalyticsUnity.Localytics/LocalyticsSessionDidOpen::BeginInvoke(System.Boolean,System.Boolean,System.Boolean,System.AsyncCallback,System.Object)
extern Il2CppClass* Boolean_t211005341_il2cpp_TypeInfo_var;
extern const uint32_t LocalyticsSessionDidOpen_BeginInvoke_m521734563_MetadataUsageId;
extern "C"  Il2CppObject * LocalyticsSessionDidOpen_BeginInvoke_m521734563 (LocalyticsSessionDidOpen_t115241990 * __this, bool ___isFirst0, bool ___isUpgrade1, bool ___isResume2, AsyncCallback_t1363551830 * ___callback3, Il2CppObject * ___object4, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (LocalyticsSessionDidOpen_BeginInvoke_m521734563_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[4] = {0};
	__d_args[0] = Box(Boolean_t211005341_il2cpp_TypeInfo_var, &___isFirst0);
	__d_args[1] = Box(Boolean_t211005341_il2cpp_TypeInfo_var, &___isUpgrade1);
	__d_args[2] = Box(Boolean_t211005341_il2cpp_TypeInfo_var, &___isResume2);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback3, (Il2CppObject*)___object4);
}
// System.Void LocalyticsUnity.Localytics/LocalyticsSessionDidOpen::EndInvoke(System.IAsyncResult)
extern "C"  void LocalyticsSessionDidOpen_EndInvoke_m2247167709 (LocalyticsSessionDidOpen_t115241990 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void LocalyticsUnity.Localytics/LocalyticsSessionWillClose::.ctor(System.Object,System.IntPtr)
extern "C"  void LocalyticsSessionWillClose__ctor_m1198798954 (LocalyticsSessionWillClose_t2690468515 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->method);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void LocalyticsUnity.Localytics/LocalyticsSessionWillClose::Invoke()
extern "C"  void LocalyticsSessionWillClose_Invoke_m2626000196 (LocalyticsSessionWillClose_t2690468515 * __this, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		LocalyticsSessionWillClose_Invoke_m2626000196((LocalyticsSessionWillClose_t2690468515 *)__this->get_prev_9(), method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if ((__this->get_m_target_2() != NULL || MethodHasParameters((MethodInfo*)(__this->get_method_3().get_m_value_0()))) && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
extern "C" void pinvoke_delegate_wrapper_LocalyticsSessionWillClose_t2690468515(Il2CppObject* delegate)
{
	typedef void (STDCALL *native_function_ptr_type)();
	native_function_ptr_type _il2cpp_pinvoke_func = ((native_function_ptr_type)((Il2CppDelegate*)delegate)->method->method);
	// Native function invocation
	_il2cpp_pinvoke_func();

}
// System.IAsyncResult LocalyticsUnity.Localytics/LocalyticsSessionWillClose::BeginInvoke(System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * LocalyticsSessionWillClose_BeginInvoke_m776122055 (LocalyticsSessionWillClose_t2690468515 * __this, AsyncCallback_t1363551830 * ___callback0, Il2CppObject * ___object1, const MethodInfo* method)
{
	void *__d_args[1] = {0};
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback0, (Il2CppObject*)___object1);
}
// System.Void LocalyticsUnity.Localytics/LocalyticsSessionWillClose::EndInvoke(System.IAsyncResult)
extern "C"  void LocalyticsSessionWillClose_EndInvoke_m246968186 (LocalyticsSessionWillClose_t2690468515 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void LocalyticsUnity.Localytics/LocalyticsSessionWillOpen::.ctor(System.Object,System.IntPtr)
extern "C"  void LocalyticsSessionWillOpen__ctor_m632890358 (LocalyticsSessionWillOpen_t2581002303 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->method);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void LocalyticsUnity.Localytics/LocalyticsSessionWillOpen::Invoke(System.Boolean,System.Boolean,System.Boolean)
extern "C"  void LocalyticsSessionWillOpen_Invoke_m2750767463 (LocalyticsSessionWillOpen_t2581002303 * __this, bool ___isFirst0, bool ___isUpgrade1, bool ___isResume2, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		LocalyticsSessionWillOpen_Invoke_m2750767463((LocalyticsSessionWillOpen_t2581002303 *)__this->get_prev_9(),___isFirst0, ___isUpgrade1, ___isResume2, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, bool ___isFirst0, bool ___isUpgrade1, bool ___isResume2, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___isFirst0, ___isUpgrade1, ___isResume2,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, bool ___isFirst0, bool ___isUpgrade1, bool ___isResume2, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___isFirst0, ___isUpgrade1, ___isResume2,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
extern "C" void pinvoke_delegate_wrapper_LocalyticsSessionWillOpen_t2581002303(Il2CppObject* delegate, bool ___isFirst0, bool ___isUpgrade1, bool ___isResume2)
{
	typedef void (STDCALL *native_function_ptr_type)(int32_t, int32_t, int32_t);
	native_function_ptr_type _il2cpp_pinvoke_func = ((native_function_ptr_type)((Il2CppDelegate*)delegate)->method->method);
	// Marshaling of parameter '___isFirst0' to native representation

	// Marshaling of parameter '___isUpgrade1' to native representation

	// Marshaling of parameter '___isResume2' to native representation

	// Native function invocation
	_il2cpp_pinvoke_func(___isFirst0, ___isUpgrade1, ___isResume2);

	// Marshaling cleanup of parameter '___isFirst0' native representation

	// Marshaling cleanup of parameter '___isUpgrade1' native representation

	// Marshaling cleanup of parameter '___isResume2' native representation

}
// System.IAsyncResult LocalyticsUnity.Localytics/LocalyticsSessionWillOpen::BeginInvoke(System.Boolean,System.Boolean,System.Boolean,System.AsyncCallback,System.Object)
extern Il2CppClass* Boolean_t211005341_il2cpp_TypeInfo_var;
extern const uint32_t LocalyticsSessionWillOpen_BeginInvoke_m3906664340_MetadataUsageId;
extern "C"  Il2CppObject * LocalyticsSessionWillOpen_BeginInvoke_m3906664340 (LocalyticsSessionWillOpen_t2581002303 * __this, bool ___isFirst0, bool ___isUpgrade1, bool ___isResume2, AsyncCallback_t1363551830 * ___callback3, Il2CppObject * ___object4, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (LocalyticsSessionWillOpen_BeginInvoke_m3906664340_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[4] = {0};
	__d_args[0] = Box(Boolean_t211005341_il2cpp_TypeInfo_var, &___isFirst0);
	__d_args[1] = Box(Boolean_t211005341_il2cpp_TypeInfo_var, &___isUpgrade1);
	__d_args[2] = Box(Boolean_t211005341_il2cpp_TypeInfo_var, &___isResume2);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback3, (Il2CppObject*)___object4);
}
// System.Void LocalyticsUnity.Localytics/LocalyticsSessionWillOpen::EndInvoke(System.IAsyncResult)
extern "C"  void LocalyticsSessionWillOpen_EndInvoke_m2314797318 (LocalyticsSessionWillOpen_t2581002303 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void LocalyticsUnity.Localytics/LocalyticsWillDismissInAppMessage::.ctor(System.Object,System.IntPtr)
extern "C"  void LocalyticsWillDismissInAppMessage__ctor_m1599719591 (LocalyticsWillDismissInAppMessage_t2580912496 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->method);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void LocalyticsUnity.Localytics/LocalyticsWillDismissInAppMessage::Invoke()
extern "C"  void LocalyticsWillDismissInAppMessage_Invoke_m1116501953 (LocalyticsWillDismissInAppMessage_t2580912496 * __this, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		LocalyticsWillDismissInAppMessage_Invoke_m1116501953((LocalyticsWillDismissInAppMessage_t2580912496 *)__this->get_prev_9(), method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if ((__this->get_m_target_2() != NULL || MethodHasParameters((MethodInfo*)(__this->get_method_3().get_m_value_0()))) && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
extern "C" void pinvoke_delegate_wrapper_LocalyticsWillDismissInAppMessage_t2580912496(Il2CppObject* delegate)
{
	typedef void (STDCALL *native_function_ptr_type)();
	native_function_ptr_type _il2cpp_pinvoke_func = ((native_function_ptr_type)((Il2CppDelegate*)delegate)->method->method);
	// Native function invocation
	_il2cpp_pinvoke_func();

}
// System.IAsyncResult LocalyticsUnity.Localytics/LocalyticsWillDismissInAppMessage::BeginInvoke(System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * LocalyticsWillDismissInAppMessage_BeginInvoke_m4217893922 (LocalyticsWillDismissInAppMessage_t2580912496 * __this, AsyncCallback_t1363551830 * ___callback0, Il2CppObject * ___object1, const MethodInfo* method)
{
	void *__d_args[1] = {0};
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback0, (Il2CppObject*)___object1);
}
// System.Void LocalyticsUnity.Localytics/LocalyticsWillDismissInAppMessage::EndInvoke(System.IAsyncResult)
extern "C"  void LocalyticsWillDismissInAppMessage_EndInvoke_m3485574711 (LocalyticsWillDismissInAppMessage_t2580912496 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void LocalyticsUnity.Localytics/LocalyticsWillDisplayInAppMessage::.ctor(System.Object,System.IntPtr)
extern "C"  void LocalyticsWillDisplayInAppMessage__ctor_m3000032991 (LocalyticsWillDisplayInAppMessage_t26700712 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->method);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void LocalyticsUnity.Localytics/LocalyticsWillDisplayInAppMessage::Invoke()
extern "C"  void LocalyticsWillDisplayInAppMessage_Invoke_m3652641785 (LocalyticsWillDisplayInAppMessage_t26700712 * __this, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		LocalyticsWillDisplayInAppMessage_Invoke_m3652641785((LocalyticsWillDisplayInAppMessage_t26700712 *)__this->get_prev_9(), method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if ((__this->get_m_target_2() != NULL || MethodHasParameters((MethodInfo*)(__this->get_method_3().get_m_value_0()))) && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
extern "C" void pinvoke_delegate_wrapper_LocalyticsWillDisplayInAppMessage_t26700712(Il2CppObject* delegate)
{
	typedef void (STDCALL *native_function_ptr_type)();
	native_function_ptr_type _il2cpp_pinvoke_func = ((native_function_ptr_type)((Il2CppDelegate*)delegate)->method->method);
	// Native function invocation
	_il2cpp_pinvoke_func();

}
// System.IAsyncResult LocalyticsUnity.Localytics/LocalyticsWillDisplayInAppMessage::BeginInvoke(System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * LocalyticsWillDisplayInAppMessage_BeginInvoke_m3106453738 (LocalyticsWillDisplayInAppMessage_t26700712 * __this, AsyncCallback_t1363551830 * ___callback0, Il2CppObject * ___object1, const MethodInfo* method)
{
	void *__d_args[1] = {0};
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback0, (Il2CppObject*)___object1);
}
// System.Void LocalyticsUnity.Localytics/LocalyticsWillDisplayInAppMessage::EndInvoke(System.IAsyncResult)
extern "C"  void LocalyticsWillDisplayInAppMessage_EndInvoke_m1928108143 (LocalyticsWillDisplayInAppMessage_t26700712 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void LocalyticsUnity.Localytics/ReceiveAnalyticsDelegate::.ctor(System.Object,System.IntPtr)
extern "C"  void ReceiveAnalyticsDelegate__ctor_m1029077999 (ReceiveAnalyticsDelegate_t2804656808 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->method);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void LocalyticsUnity.Localytics/ReceiveAnalyticsDelegate::Invoke(System.String)
extern "C"  void ReceiveAnalyticsDelegate_Invoke_m1077747865 (ReceiveAnalyticsDelegate_t2804656808 * __this, String_t* ___message0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		ReceiveAnalyticsDelegate_Invoke_m1077747865((ReceiveAnalyticsDelegate_t2804656808 *)__this->get_prev_9(),___message0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, String_t* ___message0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___message0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (void* __this, String_t* ___message0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___message0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(___message0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
extern "C" void pinvoke_delegate_wrapper_ReceiveAnalyticsDelegate_t2804656808(Il2CppObject* delegate, String_t* ___message0)
{
	typedef void (STDCALL *native_function_ptr_type)(char*);
	native_function_ptr_type _il2cpp_pinvoke_func = ((native_function_ptr_type)((Il2CppDelegate*)delegate)->method->method);
	// Marshaling of parameter '___message0' to native representation
	char* ____message0_marshaled = NULL;
	____message0_marshaled = il2cpp_codegen_marshal_string(___message0);

	// Native function invocation
	_il2cpp_pinvoke_func(____message0_marshaled);

	// Marshaling cleanup of parameter '___message0' native representation
	il2cpp_codegen_marshal_free(____message0_marshaled);
	____message0_marshaled = NULL;

}
// System.IAsyncResult LocalyticsUnity.Localytics/ReceiveAnalyticsDelegate::BeginInvoke(System.String,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * ReceiveAnalyticsDelegate_BeginInvoke_m1459533478 (ReceiveAnalyticsDelegate_t2804656808 * __this, String_t* ___message0, AsyncCallback_t1363551830 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	void *__d_args[2] = {0};
	__d_args[0] = ___message0;
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Void LocalyticsUnity.Localytics/ReceiveAnalyticsDelegate::EndInvoke(System.IAsyncResult)
extern "C"  void ReceiveAnalyticsDelegate_EndInvoke_m3597352319 (ReceiveAnalyticsDelegate_t2804656808 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void LocalyticsUnity.Localytics/ReceiveMessagingDelegate::.ctor(System.Object,System.IntPtr)
extern "C"  void ReceiveMessagingDelegate__ctor_m1867403885 (ReceiveMessagingDelegate_t3408128038 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->method);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void LocalyticsUnity.Localytics/ReceiveMessagingDelegate::Invoke(System.String)
extern "C"  void ReceiveMessagingDelegate_Invoke_m2366936411 (ReceiveMessagingDelegate_t3408128038 * __this, String_t* ___message0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		ReceiveMessagingDelegate_Invoke_m2366936411((ReceiveMessagingDelegate_t3408128038 *)__this->get_prev_9(),___message0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, String_t* ___message0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___message0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (void* __this, String_t* ___message0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___message0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(___message0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
extern "C" void pinvoke_delegate_wrapper_ReceiveMessagingDelegate_t3408128038(Il2CppObject* delegate, String_t* ___message0)
{
	typedef void (STDCALL *native_function_ptr_type)(char*);
	native_function_ptr_type _il2cpp_pinvoke_func = ((native_function_ptr_type)((Il2CppDelegate*)delegate)->method->method);
	// Marshaling of parameter '___message0' to native representation
	char* ____message0_marshaled = NULL;
	____message0_marshaled = il2cpp_codegen_marshal_string(___message0);

	// Native function invocation
	_il2cpp_pinvoke_func(____message0_marshaled);

	// Marshaling cleanup of parameter '___message0' native representation
	il2cpp_codegen_marshal_free(____message0_marshaled);
	____message0_marshaled = NULL;

}
// System.IAsyncResult LocalyticsUnity.Localytics/ReceiveMessagingDelegate::BeginInvoke(System.String,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * ReceiveMessagingDelegate_BeginInvoke_m3853697384 (ReceiveMessagingDelegate_t3408128038 * __this, String_t* ___message0, AsyncCallback_t1363551830 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	void *__d_args[2] = {0};
	__d_args[0] = ___message0;
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Void LocalyticsUnity.Localytics/ReceiveMessagingDelegate::EndInvoke(System.IAsyncResult)
extern "C"  void ReceiveMessagingDelegate_EndInvoke_m1918763773 (ReceiveMessagingDelegate_t3408128038 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void LocalyticsUnity.MiniJSON::.ctor()
extern "C"  void MiniJSON__ctor_m2176978608 (MiniJSON_t1258204565 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void LocalyticsUnity.MiniJSON::.cctor()
extern Il2CppClass* MiniJSON_t1258204565_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const uint32_t MiniJSON__cctor_m2579731197_MetadataUsageId;
extern "C"  void MiniJSON__cctor_m2579731197 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (MiniJSON__cctor_m2579731197_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		((MiniJSON_t1258204565_StaticFields*)MiniJSON_t1258204565_il2cpp_TypeInfo_var->static_fields)->set_lastErrorIndex_13((-1));
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_0 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		((MiniJSON_t1258204565_StaticFields*)MiniJSON_t1258204565_il2cpp_TypeInfo_var->static_fields)->set_lastDecode_14(L_0);
		((MiniJSON_t1258204565_StaticFields*)MiniJSON_t1258204565_il2cpp_TypeInfo_var->static_fields)->set__useGenericContainer_15((bool)1);
		return;
	}
}
// System.Object LocalyticsUnity.MiniJSON::jsonDecode(System.String)
extern Il2CppClass* MiniJSON_t1258204565_il2cpp_TypeInfo_var;
extern const uint32_t MiniJSON_jsonDecode_m3895311043_MetadataUsageId;
extern "C"  Il2CppObject * MiniJSON_jsonDecode_m3895311043 (Il2CppObject * __this /* static, unused */, String_t* ___json0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (MiniJSON_jsonDecode_m3895311043_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = ___json0;
		IL2CPP_RUNTIME_CLASS_INIT(MiniJSON_t1258204565_il2cpp_TypeInfo_var);
		Il2CppObject * L_1 = MiniJSON_jsonDecode_m4039334490(NULL /*static, unused*/, L_0, (bool)0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Object LocalyticsUnity.MiniJSON::jsonDecode(System.String,System.Boolean)
extern Il2CppClass* MiniJSON_t1258204565_il2cpp_TypeInfo_var;
extern const uint32_t MiniJSON_jsonDecode_m4039334490_MetadataUsageId;
extern "C"  Il2CppObject * MiniJSON_jsonDecode_m4039334490 (Il2CppObject * __this /* static, unused */, String_t* ___json0, bool ___decodeUsingGenericContainers1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (MiniJSON_jsonDecode_m4039334490_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	CharU5BU5D_t3416858730* V_0 = NULL;
	int32_t V_1 = 0;
	bool V_2 = false;
	Il2CppObject * V_3 = NULL;
	{
		bool L_0 = ___decodeUsingGenericContainers1;
		IL2CPP_RUNTIME_CLASS_INIT(MiniJSON_t1258204565_il2cpp_TypeInfo_var);
		((MiniJSON_t1258204565_StaticFields*)MiniJSON_t1258204565_il2cpp_TypeInfo_var->static_fields)->set__useGenericContainer_15(L_0);
		String_t* L_1 = ___json0;
		((MiniJSON_t1258204565_StaticFields*)MiniJSON_t1258204565_il2cpp_TypeInfo_var->static_fields)->set_lastDecode_14(L_1);
		String_t* L_2 = ___json0;
		if (!L_2)
		{
			goto IL_0041;
		}
	}
	{
		String_t* L_3 = ___json0;
		NullCheck(L_3);
		CharU5BU5D_t3416858730* L_4 = String_ToCharArray_m1208288742(L_3, /*hidden argument*/NULL);
		V_0 = L_4;
		V_1 = 0;
		V_2 = (bool)1;
		CharU5BU5D_t3416858730* L_5 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(MiniJSON_t1258204565_il2cpp_TypeInfo_var);
		Il2CppObject * L_6 = MiniJSON_parseValue_m1644210360(NULL /*static, unused*/, L_5, (&V_1), (&V_2), /*hidden argument*/NULL);
		V_3 = L_6;
		bool L_7 = V_2;
		if (!L_7)
		{
			goto IL_0039;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(MiniJSON_t1258204565_il2cpp_TypeInfo_var);
		((MiniJSON_t1258204565_StaticFields*)MiniJSON_t1258204565_il2cpp_TypeInfo_var->static_fields)->set_lastErrorIndex_13((-1));
		goto IL_003f;
	}

IL_0039:
	{
		int32_t L_8 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(MiniJSON_t1258204565_il2cpp_TypeInfo_var);
		((MiniJSON_t1258204565_StaticFields*)MiniJSON_t1258204565_il2cpp_TypeInfo_var->static_fields)->set_lastErrorIndex_13(L_8);
	}

IL_003f:
	{
		Il2CppObject * L_9 = V_3;
		return L_9;
	}

IL_0041:
	{
		return NULL;
	}
}
// System.String LocalyticsUnity.MiniJSON::jsonEncode(System.Object)
extern Il2CppClass* StringBuilder_t3822575854_il2cpp_TypeInfo_var;
extern Il2CppClass* MiniJSON_t1258204565_il2cpp_TypeInfo_var;
extern const uint32_t MiniJSON_jsonEncode_m2701702143_MetadataUsageId;
extern "C"  String_t* MiniJSON_jsonEncode_m2701702143 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___json0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (MiniJSON_jsonEncode_m2701702143_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	StringBuilder_t3822575854 * V_0 = NULL;
	bool V_1 = false;
	String_t* G_B3_0 = NULL;
	{
		StringBuilder_t3822575854 * L_0 = (StringBuilder_t3822575854 *)il2cpp_codegen_object_new(StringBuilder_t3822575854_il2cpp_TypeInfo_var);
		StringBuilder__ctor_m3624398269(L_0, ((int32_t)2000), /*hidden argument*/NULL);
		V_0 = L_0;
		Il2CppObject * L_1 = ___json0;
		StringBuilder_t3822575854 * L_2 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(MiniJSON_t1258204565_il2cpp_TypeInfo_var);
		bool L_3 = MiniJSON_serializeObject_m2295199(NULL /*static, unused*/, L_1, L_2, /*hidden argument*/NULL);
		V_1 = L_3;
		bool L_4 = V_1;
		if (!L_4)
		{
			goto IL_0024;
		}
	}
	{
		StringBuilder_t3822575854 * L_5 = V_0;
		NullCheck(L_5);
		String_t* L_6 = StringBuilder_ToString_m350379841(L_5, /*hidden argument*/NULL);
		G_B3_0 = L_6;
		goto IL_0025;
	}

IL_0024:
	{
		G_B3_0 = ((String_t*)(NULL));
	}

IL_0025:
	{
		return G_B3_0;
	}
}
// System.Boolean LocalyticsUnity.MiniJSON::lastDecodeSuccessful()
extern Il2CppClass* MiniJSON_t1258204565_il2cpp_TypeInfo_var;
extern const uint32_t MiniJSON_lastDecodeSuccessful_m4144103302_MetadataUsageId;
extern "C"  bool MiniJSON_lastDecodeSuccessful_m4144103302 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (MiniJSON_lastDecodeSuccessful_m4144103302_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(MiniJSON_t1258204565_il2cpp_TypeInfo_var);
		int32_t L_0 = ((MiniJSON_t1258204565_StaticFields*)MiniJSON_t1258204565_il2cpp_TypeInfo_var->static_fields)->get_lastErrorIndex_13();
		return (bool)((((int32_t)L_0) == ((int32_t)(-1)))? 1 : 0);
	}
}
// System.Int32 LocalyticsUnity.MiniJSON::getLastErrorIndex()
extern Il2CppClass* MiniJSON_t1258204565_il2cpp_TypeInfo_var;
extern const uint32_t MiniJSON_getLastErrorIndex_m2143355766_MetadataUsageId;
extern "C"  int32_t MiniJSON_getLastErrorIndex_m2143355766 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (MiniJSON_getLastErrorIndex_m2143355766_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(MiniJSON_t1258204565_il2cpp_TypeInfo_var);
		int32_t L_0 = ((MiniJSON_t1258204565_StaticFields*)MiniJSON_t1258204565_il2cpp_TypeInfo_var->static_fields)->get_lastErrorIndex_13();
		return L_0;
	}
}
// System.String LocalyticsUnity.MiniJSON::getLastErrorSnippet()
extern Il2CppClass* MiniJSON_t1258204565_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const uint32_t MiniJSON_getLastErrorSnippet_m1590990732_MetadataUsageId;
extern "C"  String_t* MiniJSON_getLastErrorSnippet_m1590990732 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (MiniJSON_getLastErrorSnippet_m1590990732_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		IL2CPP_RUNTIME_CLASS_INIT(MiniJSON_t1258204565_il2cpp_TypeInfo_var);
		int32_t L_0 = ((MiniJSON_t1258204565_StaticFields*)MiniJSON_t1258204565_il2cpp_TypeInfo_var->static_fields)->get_lastErrorIndex_13();
		if ((!(((uint32_t)L_0) == ((uint32_t)(-1)))))
		{
			goto IL_0011;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_1 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		return L_1;
	}

IL_0011:
	{
		IL2CPP_RUNTIME_CLASS_INIT(MiniJSON_t1258204565_il2cpp_TypeInfo_var);
		int32_t L_2 = ((MiniJSON_t1258204565_StaticFields*)MiniJSON_t1258204565_il2cpp_TypeInfo_var->static_fields)->get_lastErrorIndex_13();
		V_0 = ((int32_t)((int32_t)L_2-(int32_t)5));
		int32_t L_3 = ((MiniJSON_t1258204565_StaticFields*)MiniJSON_t1258204565_il2cpp_TypeInfo_var->static_fields)->get_lastErrorIndex_13();
		V_1 = ((int32_t)((int32_t)L_3+(int32_t)((int32_t)15)));
		int32_t L_4 = V_0;
		if ((((int32_t)L_4) >= ((int32_t)0)))
		{
			goto IL_002b;
		}
	}
	{
		V_0 = 0;
	}

IL_002b:
	{
		int32_t L_5 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(MiniJSON_t1258204565_il2cpp_TypeInfo_var);
		String_t* L_6 = ((MiniJSON_t1258204565_StaticFields*)MiniJSON_t1258204565_il2cpp_TypeInfo_var->static_fields)->get_lastDecode_14();
		NullCheck(L_6);
		int32_t L_7 = String_get_Length_m2979997331(L_6, /*hidden argument*/NULL);
		if ((((int32_t)L_5) < ((int32_t)L_7)))
		{
			goto IL_0048;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(MiniJSON_t1258204565_il2cpp_TypeInfo_var);
		String_t* L_8 = ((MiniJSON_t1258204565_StaticFields*)MiniJSON_t1258204565_il2cpp_TypeInfo_var->static_fields)->get_lastDecode_14();
		NullCheck(L_8);
		int32_t L_9 = String_get_Length_m2979997331(L_8, /*hidden argument*/NULL);
		V_1 = ((int32_t)((int32_t)L_9-(int32_t)1));
	}

IL_0048:
	{
		IL2CPP_RUNTIME_CLASS_INIT(MiniJSON_t1258204565_il2cpp_TypeInfo_var);
		String_t* L_10 = ((MiniJSON_t1258204565_StaticFields*)MiniJSON_t1258204565_il2cpp_TypeInfo_var->static_fields)->get_lastDecode_14();
		int32_t L_11 = V_0;
		int32_t L_12 = V_1;
		int32_t L_13 = V_0;
		NullCheck(L_10);
		String_t* L_14 = String_Substring_m675079568(L_10, L_11, ((int32_t)((int32_t)((int32_t)((int32_t)L_12-(int32_t)L_13))+(int32_t)1)), /*hidden argument*/NULL);
		return L_14;
	}
}
// System.Collections.IDictionary LocalyticsUnity.MiniJSON::parseObject(System.Char[],System.Int32&)
extern Il2CppClass* MiniJSON_t1258204565_il2cpp_TypeInfo_var;
extern Il2CppClass* Dictionary_2_t2474804324_il2cpp_TypeInfo_var;
extern Il2CppClass* Hashtable_t3875263730_il2cpp_TypeInfo_var;
extern Il2CppClass* IDictionary_t1654916945_il2cpp_TypeInfo_var;
extern const MethodInfo* Dictionary_2__ctor_m2887377984_MethodInfo_var;
extern const uint32_t MiniJSON_parseObject_m3656828422_MetadataUsageId;
extern "C"  Il2CppObject * MiniJSON_parseObject_m3656828422 (Il2CppObject * __this /* static, unused */, CharU5BU5D_t3416858730* ___json0, int32_t* ___index1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (MiniJSON_parseObject_m3656828422_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Il2CppObject * V_0 = NULL;
	int32_t V_1 = 0;
	bool V_2 = false;
	String_t* V_3 = NULL;
	bool V_4 = false;
	Il2CppObject * V_5 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(MiniJSON_t1258204565_il2cpp_TypeInfo_var);
		bool L_0 = ((MiniJSON_t1258204565_StaticFields*)MiniJSON_t1258204565_il2cpp_TypeInfo_var->static_fields)->get__useGenericContainer_15();
		if (!L_0)
		{
			goto IL_0015;
		}
	}
	{
		Dictionary_2_t2474804324 * L_1 = (Dictionary_2_t2474804324 *)il2cpp_codegen_object_new(Dictionary_2_t2474804324_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m2887377984(L_1, /*hidden argument*/Dictionary_2__ctor_m2887377984_MethodInfo_var);
		V_0 = L_1;
		goto IL_001b;
	}

IL_0015:
	{
		Hashtable_t3875263730 * L_2 = (Hashtable_t3875263730 *)il2cpp_codegen_object_new(Hashtable_t3875263730_il2cpp_TypeInfo_var);
		Hashtable__ctor_m1514037738(L_2, /*hidden argument*/NULL);
		V_0 = L_2;
	}

IL_001b:
	{
		CharU5BU5D_t3416858730* L_3 = ___json0;
		int32_t* L_4 = ___index1;
		IL2CPP_RUNTIME_CLASS_INIT(MiniJSON_t1258204565_il2cpp_TypeInfo_var);
		MiniJSON_nextToken_m2445417514(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		V_2 = (bool)0;
		goto IL_00a1;
	}

IL_002a:
	{
		CharU5BU5D_t3416858730* L_5 = ___json0;
		int32_t* L_6 = ___index1;
		IL2CPP_RUNTIME_CLASS_INIT(MiniJSON_t1258204565_il2cpp_TypeInfo_var);
		int32_t L_7 = MiniJSON_lookAhead_m2404043134(NULL /*static, unused*/, L_5, (*((int32_t*)L_6)), /*hidden argument*/NULL);
		V_1 = L_7;
		int32_t L_8 = V_1;
		if (L_8)
		{
			goto IL_003b;
		}
	}
	{
		return (Il2CppObject *)NULL;
	}

IL_003b:
	{
		int32_t L_9 = V_1;
		if ((!(((uint32_t)L_9) == ((uint32_t)6))))
		{
			goto IL_004f;
		}
	}
	{
		CharU5BU5D_t3416858730* L_10 = ___json0;
		int32_t* L_11 = ___index1;
		IL2CPP_RUNTIME_CLASS_INIT(MiniJSON_t1258204565_il2cpp_TypeInfo_var);
		MiniJSON_nextToken_m2445417514(NULL /*static, unused*/, L_10, L_11, /*hidden argument*/NULL);
		goto IL_00a1;
	}

IL_004f:
	{
		int32_t L_12 = V_1;
		if ((!(((uint32_t)L_12) == ((uint32_t)2))))
		{
			goto IL_0060;
		}
	}
	{
		CharU5BU5D_t3416858730* L_13 = ___json0;
		int32_t* L_14 = ___index1;
		IL2CPP_RUNTIME_CLASS_INIT(MiniJSON_t1258204565_il2cpp_TypeInfo_var);
		MiniJSON_nextToken_m2445417514(NULL /*static, unused*/, L_13, L_14, /*hidden argument*/NULL);
		Il2CppObject * L_15 = V_0;
		return L_15;
	}

IL_0060:
	{
		CharU5BU5D_t3416858730* L_16 = ___json0;
		int32_t* L_17 = ___index1;
		IL2CPP_RUNTIME_CLASS_INIT(MiniJSON_t1258204565_il2cpp_TypeInfo_var);
		String_t* L_18 = MiniJSON_parseString_m3312926113(NULL /*static, unused*/, L_16, L_17, /*hidden argument*/NULL);
		V_3 = L_18;
		String_t* L_19 = V_3;
		if (L_19)
		{
			goto IL_0070;
		}
	}
	{
		return (Il2CppObject *)NULL;
	}

IL_0070:
	{
		CharU5BU5D_t3416858730* L_20 = ___json0;
		int32_t* L_21 = ___index1;
		IL2CPP_RUNTIME_CLASS_INIT(MiniJSON_t1258204565_il2cpp_TypeInfo_var);
		int32_t L_22 = MiniJSON_nextToken_m2445417514(NULL /*static, unused*/, L_20, L_21, /*hidden argument*/NULL);
		V_1 = L_22;
		int32_t L_23 = V_1;
		if ((((int32_t)L_23) == ((int32_t)5)))
		{
			goto IL_0081;
		}
	}
	{
		return (Il2CppObject *)NULL;
	}

IL_0081:
	{
		V_4 = (bool)1;
		CharU5BU5D_t3416858730* L_24 = ___json0;
		int32_t* L_25 = ___index1;
		IL2CPP_RUNTIME_CLASS_INIT(MiniJSON_t1258204565_il2cpp_TypeInfo_var);
		Il2CppObject * L_26 = MiniJSON_parseValue_m1644210360(NULL /*static, unused*/, L_24, L_25, (&V_4), /*hidden argument*/NULL);
		V_5 = L_26;
		bool L_27 = V_4;
		if (L_27)
		{
			goto IL_0098;
		}
	}
	{
		return (Il2CppObject *)NULL;
	}

IL_0098:
	{
		Il2CppObject * L_28 = V_0;
		String_t* L_29 = V_3;
		Il2CppObject * L_30 = V_5;
		NullCheck(L_28);
		InterfaceActionInvoker2< Il2CppObject *, Il2CppObject * >::Invoke(3 /* System.Void System.Collections.IDictionary::set_Item(System.Object,System.Object) */, IDictionary_t1654916945_il2cpp_TypeInfo_var, L_28, L_29, L_30);
	}

IL_00a1:
	{
		bool L_31 = V_2;
		if (!L_31)
		{
			goto IL_002a;
		}
	}
	{
		Il2CppObject * L_32 = V_0;
		return L_32;
	}
}
// System.Collections.IList LocalyticsUnity.MiniJSON::parseArray(System.Char[],System.Int32&)
extern Il2CppClass* MiniJSON_t1258204565_il2cpp_TypeInfo_var;
extern Il2CppClass* List_1_t1634065389_il2cpp_TypeInfo_var;
extern Il2CppClass* ArrayList_t2121638921_il2cpp_TypeInfo_var;
extern Il2CppClass* IList_t1612618265_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1__ctor_m348833073_MethodInfo_var;
extern const uint32_t MiniJSON_parseArray_m3688880904_MetadataUsageId;
extern "C"  Il2CppObject * MiniJSON_parseArray_m3688880904 (Il2CppObject * __this /* static, unused */, CharU5BU5D_t3416858730* ___json0, int32_t* ___index1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (MiniJSON_parseArray_m3688880904_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Il2CppObject * V_0 = NULL;
	bool V_1 = false;
	int32_t V_2 = 0;
	bool V_3 = false;
	Il2CppObject * V_4 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(MiniJSON_t1258204565_il2cpp_TypeInfo_var);
		bool L_0 = ((MiniJSON_t1258204565_StaticFields*)MiniJSON_t1258204565_il2cpp_TypeInfo_var->static_fields)->get__useGenericContainer_15();
		if (!L_0)
		{
			goto IL_0015;
		}
	}
	{
		List_1_t1634065389 * L_1 = (List_1_t1634065389 *)il2cpp_codegen_object_new(List_1_t1634065389_il2cpp_TypeInfo_var);
		List_1__ctor_m348833073(L_1, /*hidden argument*/List_1__ctor_m348833073_MethodInfo_var);
		V_0 = L_1;
		goto IL_001b;
	}

IL_0015:
	{
		ArrayList_t2121638921 * L_2 = (ArrayList_t2121638921 *)il2cpp_codegen_object_new(ArrayList_t2121638921_il2cpp_TypeInfo_var);
		ArrayList__ctor_m1878432947(L_2, /*hidden argument*/NULL);
		V_0 = L_2;
	}

IL_001b:
	{
		CharU5BU5D_t3416858730* L_3 = ___json0;
		int32_t* L_4 = ___index1;
		IL2CPP_RUNTIME_CLASS_INIT(MiniJSON_t1258204565_il2cpp_TypeInfo_var);
		MiniJSON_nextToken_m2445417514(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		V_1 = (bool)0;
		goto IL_0081;
	}

IL_002a:
	{
		CharU5BU5D_t3416858730* L_5 = ___json0;
		int32_t* L_6 = ___index1;
		IL2CPP_RUNTIME_CLASS_INIT(MiniJSON_t1258204565_il2cpp_TypeInfo_var);
		int32_t L_7 = MiniJSON_lookAhead_m2404043134(NULL /*static, unused*/, L_5, (*((int32_t*)L_6)), /*hidden argument*/NULL);
		V_2 = L_7;
		int32_t L_8 = V_2;
		if (L_8)
		{
			goto IL_003b;
		}
	}
	{
		return (Il2CppObject *)NULL;
	}

IL_003b:
	{
		int32_t L_9 = V_2;
		if ((!(((uint32_t)L_9) == ((uint32_t)6))))
		{
			goto IL_004f;
		}
	}
	{
		CharU5BU5D_t3416858730* L_10 = ___json0;
		int32_t* L_11 = ___index1;
		IL2CPP_RUNTIME_CLASS_INIT(MiniJSON_t1258204565_il2cpp_TypeInfo_var);
		MiniJSON_nextToken_m2445417514(NULL /*static, unused*/, L_10, L_11, /*hidden argument*/NULL);
		goto IL_0081;
	}

IL_004f:
	{
		int32_t L_12 = V_2;
		if ((!(((uint32_t)L_12) == ((uint32_t)4))))
		{
			goto IL_0063;
		}
	}
	{
		CharU5BU5D_t3416858730* L_13 = ___json0;
		int32_t* L_14 = ___index1;
		IL2CPP_RUNTIME_CLASS_INIT(MiniJSON_t1258204565_il2cpp_TypeInfo_var);
		MiniJSON_nextToken_m2445417514(NULL /*static, unused*/, L_13, L_14, /*hidden argument*/NULL);
		goto IL_0087;
	}

IL_0063:
	{
		V_3 = (bool)1;
		CharU5BU5D_t3416858730* L_15 = ___json0;
		int32_t* L_16 = ___index1;
		IL2CPP_RUNTIME_CLASS_INIT(MiniJSON_t1258204565_il2cpp_TypeInfo_var);
		Il2CppObject * L_17 = MiniJSON_parseValue_m1644210360(NULL /*static, unused*/, L_15, L_16, (&V_3), /*hidden argument*/NULL);
		V_4 = L_17;
		bool L_18 = V_3;
		if (L_18)
		{
			goto IL_0078;
		}
	}
	{
		return (Il2CppObject *)NULL;
	}

IL_0078:
	{
		Il2CppObject * L_19 = V_0;
		Il2CppObject * L_20 = V_4;
		NullCheck(L_19);
		InterfaceFuncInvoker1< int32_t, Il2CppObject * >::Invoke(4 /* System.Int32 System.Collections.IList::Add(System.Object) */, IList_t1612618265_il2cpp_TypeInfo_var, L_19, L_20);
	}

IL_0081:
	{
		bool L_21 = V_1;
		if (!L_21)
		{
			goto IL_002a;
		}
	}

IL_0087:
	{
		Il2CppObject * L_22 = V_0;
		return L_22;
	}
}
// System.Object LocalyticsUnity.MiniJSON::parseValue(System.Char[],System.Int32&,System.Boolean&)
extern Il2CppClass* MiniJSON_t1258204565_il2cpp_TypeInfo_var;
extern Il2CppClass* Double_t534516614_il2cpp_TypeInfo_var;
extern Il2CppClass* Boolean_t211005341_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2583950;
extern Il2CppCodeGenString* _stringLiteral66658563;
extern const uint32_t MiniJSON_parseValue_m1644210360_MetadataUsageId;
extern "C"  Il2CppObject * MiniJSON_parseValue_m1644210360 (Il2CppObject * __this /* static, unused */, CharU5BU5D_t3416858730* ___json0, int32_t* ___index1, bool* ___success2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (MiniJSON_parseValue_m1644210360_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	{
		CharU5BU5D_t3416858730* L_0 = ___json0;
		int32_t* L_1 = ___index1;
		IL2CPP_RUNTIME_CLASS_INIT(MiniJSON_t1258204565_il2cpp_TypeInfo_var);
		int32_t L_2 = MiniJSON_lookAhead_m2404043134(NULL /*static, unused*/, L_0, (*((int32_t*)L_1)), /*hidden argument*/NULL);
		V_0 = L_2;
		int32_t L_3 = V_0;
		if (L_3 == 0)
		{
			goto IL_00a3;
		}
		if (L_3 == 1)
		{
			goto IL_0059;
		}
		if (L_3 == 2)
		{
			goto IL_00a8;
		}
		if (L_3 == 3)
		{
			goto IL_0061;
		}
		if (L_3 == 4)
		{
			goto IL_00a8;
		}
		if (L_3 == 5)
		{
			goto IL_00a8;
		}
		if (L_3 == 6)
		{
			goto IL_00a8;
		}
		if (L_3 == 7)
		{
			goto IL_0044;
		}
		if (L_3 == 8)
		{
			goto IL_004c;
		}
		if (L_3 == 9)
		{
			goto IL_0069;
		}
		if (L_3 == 10)
		{
			goto IL_0081;
		}
		if (L_3 == 11)
		{
			goto IL_0099;
		}
	}
	{
		goto IL_00a8;
	}

IL_0044:
	{
		CharU5BU5D_t3416858730* L_4 = ___json0;
		int32_t* L_5 = ___index1;
		IL2CPP_RUNTIME_CLASS_INIT(MiniJSON_t1258204565_il2cpp_TypeInfo_var);
		String_t* L_6 = MiniJSON_parseString_m3312926113(NULL /*static, unused*/, L_4, L_5, /*hidden argument*/NULL);
		return L_6;
	}

IL_004c:
	{
		CharU5BU5D_t3416858730* L_7 = ___json0;
		int32_t* L_8 = ___index1;
		IL2CPP_RUNTIME_CLASS_INIT(MiniJSON_t1258204565_il2cpp_TypeInfo_var);
		double L_9 = MiniJSON_parseNumber_m3888675241(NULL /*static, unused*/, L_7, L_8, /*hidden argument*/NULL);
		double L_10 = L_9;
		Il2CppObject * L_11 = Box(Double_t534516614_il2cpp_TypeInfo_var, &L_10);
		return L_11;
	}

IL_0059:
	{
		CharU5BU5D_t3416858730* L_12 = ___json0;
		int32_t* L_13 = ___index1;
		IL2CPP_RUNTIME_CLASS_INIT(MiniJSON_t1258204565_il2cpp_TypeInfo_var);
		Il2CppObject * L_14 = MiniJSON_parseObject_m3656828422(NULL /*static, unused*/, L_12, L_13, /*hidden argument*/NULL);
		return L_14;
	}

IL_0061:
	{
		CharU5BU5D_t3416858730* L_15 = ___json0;
		int32_t* L_16 = ___index1;
		IL2CPP_RUNTIME_CLASS_INIT(MiniJSON_t1258204565_il2cpp_TypeInfo_var);
		Il2CppObject * L_17 = MiniJSON_parseArray_m3688880904(NULL /*static, unused*/, L_15, L_16, /*hidden argument*/NULL);
		return L_17;
	}

IL_0069:
	{
		CharU5BU5D_t3416858730* L_18 = ___json0;
		int32_t* L_19 = ___index1;
		IL2CPP_RUNTIME_CLASS_INIT(MiniJSON_t1258204565_il2cpp_TypeInfo_var);
		MiniJSON_nextToken_m2445417514(NULL /*static, unused*/, L_18, L_19, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Boolean_t211005341_il2cpp_TypeInfo_var);
		bool L_20 = Boolean_Parse_m3007515274(NULL /*static, unused*/, _stringLiteral2583950, /*hidden argument*/NULL);
		bool L_21 = L_20;
		Il2CppObject * L_22 = Box(Boolean_t211005341_il2cpp_TypeInfo_var, &L_21);
		return L_22;
	}

IL_0081:
	{
		CharU5BU5D_t3416858730* L_23 = ___json0;
		int32_t* L_24 = ___index1;
		IL2CPP_RUNTIME_CLASS_INIT(MiniJSON_t1258204565_il2cpp_TypeInfo_var);
		MiniJSON_nextToken_m2445417514(NULL /*static, unused*/, L_23, L_24, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Boolean_t211005341_il2cpp_TypeInfo_var);
		bool L_25 = Boolean_Parse_m3007515274(NULL /*static, unused*/, _stringLiteral66658563, /*hidden argument*/NULL);
		bool L_26 = L_25;
		Il2CppObject * L_27 = Box(Boolean_t211005341_il2cpp_TypeInfo_var, &L_26);
		return L_27;
	}

IL_0099:
	{
		CharU5BU5D_t3416858730* L_28 = ___json0;
		int32_t* L_29 = ___index1;
		IL2CPP_RUNTIME_CLASS_INIT(MiniJSON_t1258204565_il2cpp_TypeInfo_var);
		MiniJSON_nextToken_m2445417514(NULL /*static, unused*/, L_28, L_29, /*hidden argument*/NULL);
		return NULL;
	}

IL_00a3:
	{
		goto IL_00a8;
	}

IL_00a8:
	{
		bool* L_30 = ___success2;
		*((int8_t*)(L_30)) = (int8_t)0;
		return NULL;
	}
}
// System.String LocalyticsUnity.MiniJSON::parseString(System.Char[],System.Int32&)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* MiniJSON_t1258204565_il2cpp_TypeInfo_var;
extern Il2CppClass* Char_t2778706699_il2cpp_TypeInfo_var;
extern Il2CppClass* CharU5BU5D_t3416858730_il2cpp_TypeInfo_var;
extern const uint32_t MiniJSON_parseString_m3312926113_MetadataUsageId;
extern "C"  String_t* MiniJSON_parseString_m3312926113 (Il2CppObject * __this /* static, unused */, CharU5BU5D_t3416858730* ___json0, int32_t* ___index1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (MiniJSON_parseString_m3312926113_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	String_t* V_0 = NULL;
	uint16_t V_1 = 0x0;
	bool V_2 = false;
	int32_t V_3 = 0;
	CharU5BU5D_t3416858730* V_4 = NULL;
	uint32_t V_5 = 0;
	int32_t V_6 = 0;
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_0 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		V_0 = L_0;
		CharU5BU5D_t3416858730* L_1 = ___json0;
		int32_t* L_2 = ___index1;
		IL2CPP_RUNTIME_CLASS_INIT(MiniJSON_t1258204565_il2cpp_TypeInfo_var);
		MiniJSON_eatWhitespace_m1163578829(NULL /*static, unused*/, L_1, L_2, /*hidden argument*/NULL);
		CharU5BU5D_t3416858730* L_3 = ___json0;
		int32_t* L_4 = ___index1;
		int32_t* L_5 = ___index1;
		int32_t L_6 = (*((int32_t*)L_5));
		V_6 = L_6;
		*((int32_t*)(L_4)) = (int32_t)((int32_t)((int32_t)L_6+(int32_t)1));
		int32_t L_7 = V_6;
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, L_7);
		int32_t L_8 = L_7;
		V_1 = ((L_3)->GetAt(static_cast<il2cpp_array_size_t>(L_8)));
		V_2 = (bool)0;
		goto IL_01b7;
	}

IL_0022:
	{
		int32_t* L_9 = ___index1;
		CharU5BU5D_t3416858730* L_10 = ___json0;
		NullCheck(L_10);
		if ((!(((uint32_t)(*((int32_t*)L_9))) == ((uint32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_10)->max_length))))))))
		{
			goto IL_0031;
		}
	}
	{
		goto IL_01bd;
	}

IL_0031:
	{
		CharU5BU5D_t3416858730* L_11 = ___json0;
		int32_t* L_12 = ___index1;
		int32_t* L_13 = ___index1;
		int32_t L_14 = (*((int32_t*)L_13));
		V_6 = L_14;
		*((int32_t*)(L_12)) = (int32_t)((int32_t)((int32_t)L_14+(int32_t)1));
		int32_t L_15 = V_6;
		NullCheck(L_11);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_11, L_15);
		int32_t L_16 = L_15;
		V_1 = ((L_11)->GetAt(static_cast<il2cpp_array_size_t>(L_16)));
		uint16_t L_17 = V_1;
		if ((!(((uint32_t)L_17) == ((uint32_t)((int32_t)34)))))
		{
			goto IL_004e;
		}
	}
	{
		V_2 = (bool)1;
		goto IL_01bd;
	}

IL_004e:
	{
		uint16_t L_18 = V_1;
		if ((!(((uint32_t)L_18) == ((uint32_t)((int32_t)92)))))
		{
			goto IL_01aa;
		}
	}
	{
		int32_t* L_19 = ___index1;
		CharU5BU5D_t3416858730* L_20 = ___json0;
		NullCheck(L_20);
		if ((!(((uint32_t)(*((int32_t*)L_19))) == ((uint32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_20)->max_length))))))))
		{
			goto IL_0065;
		}
	}
	{
		goto IL_01bd;
	}

IL_0065:
	{
		CharU5BU5D_t3416858730* L_21 = ___json0;
		int32_t* L_22 = ___index1;
		int32_t* L_23 = ___index1;
		int32_t L_24 = (*((int32_t*)L_23));
		V_6 = L_24;
		*((int32_t*)(L_22)) = (int32_t)((int32_t)((int32_t)L_24+(int32_t)1));
		int32_t L_25 = V_6;
		NullCheck(L_21);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_21, L_25);
		int32_t L_26 = L_25;
		V_1 = ((L_21)->GetAt(static_cast<il2cpp_array_size_t>(L_26)));
		uint16_t L_27 = V_1;
		if ((!(((uint32_t)L_27) == ((uint32_t)((int32_t)34)))))
		{
			goto IL_008e;
		}
	}
	{
		String_t* L_28 = V_0;
		uint16_t L_29 = ((uint16_t)((int32_t)34));
		Il2CppObject * L_30 = Box(Char_t2778706699_il2cpp_TypeInfo_var, &L_29);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_31 = String_Concat_m389863537(NULL /*static, unused*/, L_28, L_30, /*hidden argument*/NULL);
		V_0 = L_31;
		goto IL_01a5;
	}

IL_008e:
	{
		uint16_t L_32 = V_1;
		if ((!(((uint32_t)L_32) == ((uint32_t)((int32_t)92)))))
		{
			goto IL_00a9;
		}
	}
	{
		String_t* L_33 = V_0;
		uint16_t L_34 = ((uint16_t)((int32_t)92));
		Il2CppObject * L_35 = Box(Char_t2778706699_il2cpp_TypeInfo_var, &L_34);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_36 = String_Concat_m389863537(NULL /*static, unused*/, L_33, L_35, /*hidden argument*/NULL);
		V_0 = L_36;
		goto IL_01a5;
	}

IL_00a9:
	{
		uint16_t L_37 = V_1;
		if ((!(((uint32_t)L_37) == ((uint32_t)((int32_t)47)))))
		{
			goto IL_00c4;
		}
	}
	{
		String_t* L_38 = V_0;
		uint16_t L_39 = ((uint16_t)((int32_t)47));
		Il2CppObject * L_40 = Box(Char_t2778706699_il2cpp_TypeInfo_var, &L_39);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_41 = String_Concat_m389863537(NULL /*static, unused*/, L_38, L_40, /*hidden argument*/NULL);
		V_0 = L_41;
		goto IL_01a5;
	}

IL_00c4:
	{
		uint16_t L_42 = V_1;
		if ((!(((uint32_t)L_42) == ((uint32_t)((int32_t)98)))))
		{
			goto IL_00de;
		}
	}
	{
		String_t* L_43 = V_0;
		uint16_t L_44 = ((uint16_t)8);
		Il2CppObject * L_45 = Box(Char_t2778706699_il2cpp_TypeInfo_var, &L_44);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_46 = String_Concat_m389863537(NULL /*static, unused*/, L_43, L_45, /*hidden argument*/NULL);
		V_0 = L_46;
		goto IL_01a5;
	}

IL_00de:
	{
		uint16_t L_47 = V_1;
		if ((!(((uint32_t)L_47) == ((uint32_t)((int32_t)102)))))
		{
			goto IL_00f9;
		}
	}
	{
		String_t* L_48 = V_0;
		uint16_t L_49 = ((uint16_t)((int32_t)12));
		Il2CppObject * L_50 = Box(Char_t2778706699_il2cpp_TypeInfo_var, &L_49);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_51 = String_Concat_m389863537(NULL /*static, unused*/, L_48, L_50, /*hidden argument*/NULL);
		V_0 = L_51;
		goto IL_01a5;
	}

IL_00f9:
	{
		uint16_t L_52 = V_1;
		if ((!(((uint32_t)L_52) == ((uint32_t)((int32_t)110)))))
		{
			goto IL_0114;
		}
	}
	{
		String_t* L_53 = V_0;
		uint16_t L_54 = ((uint16_t)((int32_t)10));
		Il2CppObject * L_55 = Box(Char_t2778706699_il2cpp_TypeInfo_var, &L_54);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_56 = String_Concat_m389863537(NULL /*static, unused*/, L_53, L_55, /*hidden argument*/NULL);
		V_0 = L_56;
		goto IL_01a5;
	}

IL_0114:
	{
		uint16_t L_57 = V_1;
		if ((!(((uint32_t)L_57) == ((uint32_t)((int32_t)114)))))
		{
			goto IL_012f;
		}
	}
	{
		String_t* L_58 = V_0;
		uint16_t L_59 = ((uint16_t)((int32_t)13));
		Il2CppObject * L_60 = Box(Char_t2778706699_il2cpp_TypeInfo_var, &L_59);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_61 = String_Concat_m389863537(NULL /*static, unused*/, L_58, L_60, /*hidden argument*/NULL);
		V_0 = L_61;
		goto IL_01a5;
	}

IL_012f:
	{
		uint16_t L_62 = V_1;
		if ((!(((uint32_t)L_62) == ((uint32_t)((int32_t)116)))))
		{
			goto IL_014a;
		}
	}
	{
		String_t* L_63 = V_0;
		uint16_t L_64 = ((uint16_t)((int32_t)9));
		Il2CppObject * L_65 = Box(Char_t2778706699_il2cpp_TypeInfo_var, &L_64);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_66 = String_Concat_m389863537(NULL /*static, unused*/, L_63, L_65, /*hidden argument*/NULL);
		V_0 = L_66;
		goto IL_01a5;
	}

IL_014a:
	{
		uint16_t L_67 = V_1;
		if ((!(((uint32_t)L_67) == ((uint32_t)((int32_t)117)))))
		{
			goto IL_01a5;
		}
	}
	{
		CharU5BU5D_t3416858730* L_68 = ___json0;
		NullCheck(L_68);
		int32_t* L_69 = ___index1;
		V_3 = ((int32_t)((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_68)->max_length))))-(int32_t)(*((int32_t*)L_69))));
		int32_t L_70 = V_3;
		if ((((int32_t)L_70) < ((int32_t)4)))
		{
			goto IL_01a0;
		}
	}
	{
		V_4 = ((CharU5BU5D_t3416858730*)SZArrayNew(CharU5BU5D_t3416858730_il2cpp_TypeInfo_var, (uint32_t)4));
		CharU5BU5D_t3416858730* L_71 = ___json0;
		int32_t* L_72 = ___index1;
		CharU5BU5D_t3416858730* L_73 = V_4;
		Array_Copy_m2598616668(NULL /*static, unused*/, (Il2CppArray *)(Il2CppArray *)L_71, (*((int32_t*)L_72)), (Il2CppArray *)(Il2CppArray *)L_73, 0, 4, /*hidden argument*/NULL);
		CharU5BU5D_t3416858730* L_74 = V_4;
		String_t* L_75 = String_CreateString_m578950865(NULL, L_74, /*hidden argument*/NULL);
		uint32_t L_76 = UInt32_Parse_m3754424175(NULL /*static, unused*/, L_75, ((int32_t)515), /*hidden argument*/NULL);
		V_5 = L_76;
		String_t* L_77 = V_0;
		uint32_t L_78 = V_5;
		IL2CPP_RUNTIME_CLASS_INIT(Char_t2778706699_il2cpp_TypeInfo_var);
		String_t* L_79 = Char_ConvertFromUtf32_m567781788(NULL /*static, unused*/, L_78, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_80 = String_Concat_m138640077(NULL /*static, unused*/, L_77, L_79, /*hidden argument*/NULL);
		V_0 = L_80;
		int32_t* L_81 = ___index1;
		int32_t* L_82 = ___index1;
		*((int32_t*)(L_81)) = (int32_t)((int32_t)((int32_t)(*((int32_t*)L_82))+(int32_t)4));
		goto IL_01a5;
	}

IL_01a0:
	{
		goto IL_01bd;
	}

IL_01a5:
	{
		goto IL_01b7;
	}

IL_01aa:
	{
		String_t* L_83 = V_0;
		uint16_t L_84 = V_1;
		uint16_t L_85 = L_84;
		Il2CppObject * L_86 = Box(Char_t2778706699_il2cpp_TypeInfo_var, &L_85);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_87 = String_Concat_m389863537(NULL /*static, unused*/, L_83, L_86, /*hidden argument*/NULL);
		V_0 = L_87;
	}

IL_01b7:
	{
		bool L_88 = V_2;
		if (!L_88)
		{
			goto IL_0022;
		}
	}

IL_01bd:
	{
		bool L_89 = V_2;
		if (L_89)
		{
			goto IL_01c5;
		}
	}
	{
		return (String_t*)NULL;
	}

IL_01c5:
	{
		String_t* L_90 = V_0;
		return L_90;
	}
}
// System.Double LocalyticsUnity.MiniJSON::parseNumber(System.Char[],System.Int32&)
extern Il2CppClass* MiniJSON_t1258204565_il2cpp_TypeInfo_var;
extern Il2CppClass* CharU5BU5D_t3416858730_il2cpp_TypeInfo_var;
extern const uint32_t MiniJSON_parseNumber_m3888675241_MetadataUsageId;
extern "C"  double MiniJSON_parseNumber_m3888675241 (Il2CppObject * __this /* static, unused */, CharU5BU5D_t3416858730* ___json0, int32_t* ___index1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (MiniJSON_parseNumber_m3888675241_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	CharU5BU5D_t3416858730* V_2 = NULL;
	{
		CharU5BU5D_t3416858730* L_0 = ___json0;
		int32_t* L_1 = ___index1;
		IL2CPP_RUNTIME_CLASS_INIT(MiniJSON_t1258204565_il2cpp_TypeInfo_var);
		MiniJSON_eatWhitespace_m1163578829(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		CharU5BU5D_t3416858730* L_2 = ___json0;
		int32_t* L_3 = ___index1;
		int32_t L_4 = MiniJSON_getLastIndexOfNumber_m2770042436(NULL /*static, unused*/, L_2, (*((int32_t*)L_3)), /*hidden argument*/NULL);
		V_0 = L_4;
		int32_t L_5 = V_0;
		int32_t* L_6 = ___index1;
		V_1 = ((int32_t)((int32_t)((int32_t)((int32_t)L_5-(int32_t)(*((int32_t*)L_6))))+(int32_t)1));
		int32_t L_7 = V_1;
		V_2 = ((CharU5BU5D_t3416858730*)SZArrayNew(CharU5BU5D_t3416858730_il2cpp_TypeInfo_var, (uint32_t)L_7));
		CharU5BU5D_t3416858730* L_8 = ___json0;
		int32_t* L_9 = ___index1;
		CharU5BU5D_t3416858730* L_10 = V_2;
		int32_t L_11 = V_1;
		Array_Copy_m2598616668(NULL /*static, unused*/, (Il2CppArray *)(Il2CppArray *)L_8, (*((int32_t*)L_9)), (Il2CppArray *)(Il2CppArray *)L_10, 0, L_11, /*hidden argument*/NULL);
		int32_t* L_12 = ___index1;
		int32_t L_13 = V_0;
		*((int32_t*)(L_12)) = (int32_t)((int32_t)((int32_t)L_13+(int32_t)1));
		CharU5BU5D_t3416858730* L_14 = V_2;
		String_t* L_15 = String_CreateString_m578950865(NULL, L_14, /*hidden argument*/NULL);
		double L_16 = Double_Parse_m3110783306(NULL /*static, unused*/, L_15, /*hidden argument*/NULL);
		return L_16;
	}
}
// System.Int32 LocalyticsUnity.MiniJSON::getLastIndexOfNumber(System.Char[],System.Int32)
extern Il2CppCodeGenString* _stringLiteral2451559847;
extern const uint32_t MiniJSON_getLastIndexOfNumber_m2770042436_MetadataUsageId;
extern "C"  int32_t MiniJSON_getLastIndexOfNumber_m2770042436 (Il2CppObject * __this /* static, unused */, CharU5BU5D_t3416858730* ___json0, int32_t ___index1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (MiniJSON_getLastIndexOfNumber_m2770042436_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	{
		int32_t L_0 = ___index1;
		V_0 = L_0;
		goto IL_0023;
	}

IL_0007:
	{
		CharU5BU5D_t3416858730* L_1 = ___json0;
		int32_t L_2 = V_0;
		NullCheck(L_1);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_1, L_2);
		int32_t L_3 = L_2;
		NullCheck(_stringLiteral2451559847);
		int32_t L_4 = String_IndexOf_m2775210486(_stringLiteral2451559847, ((L_1)->GetAt(static_cast<il2cpp_array_size_t>(L_3))), /*hidden argument*/NULL);
		if ((!(((uint32_t)L_4) == ((uint32_t)(-1)))))
		{
			goto IL_001f;
		}
	}
	{
		goto IL_002c;
	}

IL_001f:
	{
		int32_t L_5 = V_0;
		V_0 = ((int32_t)((int32_t)L_5+(int32_t)1));
	}

IL_0023:
	{
		int32_t L_6 = V_0;
		CharU5BU5D_t3416858730* L_7 = ___json0;
		NullCheck(L_7);
		if ((((int32_t)L_6) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_7)->max_length)))))))
		{
			goto IL_0007;
		}
	}

IL_002c:
	{
		int32_t L_8 = V_0;
		return ((int32_t)((int32_t)L_8-(int32_t)1));
	}
}
// System.Void LocalyticsUnity.MiniJSON::eatWhitespace(System.Char[],System.Int32&)
extern Il2CppCodeGenString* _stringLiteral962284;
extern const uint32_t MiniJSON_eatWhitespace_m1163578829_MetadataUsageId;
extern "C"  void MiniJSON_eatWhitespace_m1163578829 (Il2CppObject * __this /* static, unused */, CharU5BU5D_t3416858730* ___json0, int32_t* ___index1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (MiniJSON_eatWhitespace_m1163578829_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		goto IL_0024;
	}

IL_0005:
	{
		CharU5BU5D_t3416858730* L_0 = ___json0;
		int32_t* L_1 = ___index1;
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, (*((int32_t*)L_1)));
		int32_t L_2 = (*((int32_t*)L_1));
		NullCheck(_stringLiteral962284);
		int32_t L_3 = String_IndexOf_m2775210486(_stringLiteral962284, ((L_0)->GetAt(static_cast<il2cpp_array_size_t>(L_2))), /*hidden argument*/NULL);
		if ((!(((uint32_t)L_3) == ((uint32_t)(-1)))))
		{
			goto IL_001e;
		}
	}
	{
		goto IL_002e;
	}

IL_001e:
	{
		int32_t* L_4 = ___index1;
		int32_t* L_5 = ___index1;
		*((int32_t*)(L_4)) = (int32_t)((int32_t)((int32_t)(*((int32_t*)L_5))+(int32_t)1));
	}

IL_0024:
	{
		int32_t* L_6 = ___index1;
		CharU5BU5D_t3416858730* L_7 = ___json0;
		NullCheck(L_7);
		if ((((int32_t)(*((int32_t*)L_6))) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_7)->max_length)))))))
		{
			goto IL_0005;
		}
	}

IL_002e:
	{
		return;
	}
}
// System.Int32 LocalyticsUnity.MiniJSON::lookAhead(System.Char[],System.Int32)
extern Il2CppClass* MiniJSON_t1258204565_il2cpp_TypeInfo_var;
extern const uint32_t MiniJSON_lookAhead_m2404043134_MetadataUsageId;
extern "C"  int32_t MiniJSON_lookAhead_m2404043134 (Il2CppObject * __this /* static, unused */, CharU5BU5D_t3416858730* ___json0, int32_t ___index1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (MiniJSON_lookAhead_m2404043134_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	{
		int32_t L_0 = ___index1;
		V_0 = L_0;
		CharU5BU5D_t3416858730* L_1 = ___json0;
		IL2CPP_RUNTIME_CLASS_INIT(MiniJSON_t1258204565_il2cpp_TypeInfo_var);
		int32_t L_2 = MiniJSON_nextToken_m2445417514(NULL /*static, unused*/, L_1, (&V_0), /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Int32 LocalyticsUnity.MiniJSON::nextToken(System.Char[],System.Int32&)
extern Il2CppClass* MiniJSON_t1258204565_il2cpp_TypeInfo_var;
extern const uint32_t MiniJSON_nextToken_m2445417514_MetadataUsageId;
extern "C"  int32_t MiniJSON_nextToken_m2445417514 (Il2CppObject * __this /* static, unused */, CharU5BU5D_t3416858730* ___json0, int32_t* ___index1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (MiniJSON_nextToken_m2445417514_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	uint16_t V_0 = 0x0;
	int32_t V_1 = 0;
	uint16_t V_2 = 0x0;
	{
		CharU5BU5D_t3416858730* L_0 = ___json0;
		int32_t* L_1 = ___index1;
		IL2CPP_RUNTIME_CLASS_INIT(MiniJSON_t1258204565_il2cpp_TypeInfo_var);
		MiniJSON_eatWhitespace_m1163578829(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		int32_t* L_2 = ___index1;
		CharU5BU5D_t3416858730* L_3 = ___json0;
		NullCheck(L_3);
		if ((!(((uint32_t)(*((int32_t*)L_2))) == ((uint32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_3)->max_length))))))))
		{
			goto IL_0013;
		}
	}
	{
		return 0;
	}

IL_0013:
	{
		CharU5BU5D_t3416858730* L_4 = ___json0;
		int32_t* L_5 = ___index1;
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, (*((int32_t*)L_5)));
		int32_t L_6 = (*((int32_t*)L_5));
		V_0 = ((L_4)->GetAt(static_cast<il2cpp_array_size_t>(L_6)));
		int32_t* L_7 = ___index1;
		int32_t* L_8 = ___index1;
		*((int32_t*)(L_7)) = (int32_t)((int32_t)((int32_t)(*((int32_t*)L_8))+(int32_t)1));
		uint16_t L_9 = V_0;
		V_2 = L_9;
		uint16_t L_10 = V_2;
		if (((int32_t)((int32_t)L_10-(int32_t)((int32_t)34))) == 0)
		{
			goto IL_00c6;
		}
		if (((int32_t)((int32_t)L_10-(int32_t)((int32_t)34))) == 1)
		{
			goto IL_008d;
		}
		if (((int32_t)((int32_t)L_10-(int32_t)((int32_t)34))) == 2)
		{
			goto IL_008d;
		}
		if (((int32_t)((int32_t)L_10-(int32_t)((int32_t)34))) == 3)
		{
			goto IL_008d;
		}
		if (((int32_t)((int32_t)L_10-(int32_t)((int32_t)34))) == 4)
		{
			goto IL_008d;
		}
		if (((int32_t)((int32_t)L_10-(int32_t)((int32_t)34))) == 5)
		{
			goto IL_008d;
		}
		if (((int32_t)((int32_t)L_10-(int32_t)((int32_t)34))) == 6)
		{
			goto IL_008d;
		}
		if (((int32_t)((int32_t)L_10-(int32_t)((int32_t)34))) == 7)
		{
			goto IL_008d;
		}
		if (((int32_t)((int32_t)L_10-(int32_t)((int32_t)34))) == 8)
		{
			goto IL_008d;
		}
		if (((int32_t)((int32_t)L_10-(int32_t)((int32_t)34))) == 9)
		{
			goto IL_008d;
		}
		if (((int32_t)((int32_t)L_10-(int32_t)((int32_t)34))) == 10)
		{
			goto IL_00c4;
		}
		if (((int32_t)((int32_t)L_10-(int32_t)((int32_t)34))) == 11)
		{
			goto IL_00c8;
		}
		if (((int32_t)((int32_t)L_10-(int32_t)((int32_t)34))) == 12)
		{
			goto IL_008d;
		}
		if (((int32_t)((int32_t)L_10-(int32_t)((int32_t)34))) == 13)
		{
			goto IL_008d;
		}
		if (((int32_t)((int32_t)L_10-(int32_t)((int32_t)34))) == 14)
		{
			goto IL_00c8;
		}
		if (((int32_t)((int32_t)L_10-(int32_t)((int32_t)34))) == 15)
		{
			goto IL_00c8;
		}
		if (((int32_t)((int32_t)L_10-(int32_t)((int32_t)34))) == 16)
		{
			goto IL_00c8;
		}
		if (((int32_t)((int32_t)L_10-(int32_t)((int32_t)34))) == 17)
		{
			goto IL_00c8;
		}
		if (((int32_t)((int32_t)L_10-(int32_t)((int32_t)34))) == 18)
		{
			goto IL_00c8;
		}
		if (((int32_t)((int32_t)L_10-(int32_t)((int32_t)34))) == 19)
		{
			goto IL_00c8;
		}
		if (((int32_t)((int32_t)L_10-(int32_t)((int32_t)34))) == 20)
		{
			goto IL_00c8;
		}
		if (((int32_t)((int32_t)L_10-(int32_t)((int32_t)34))) == 21)
		{
			goto IL_00c8;
		}
		if (((int32_t)((int32_t)L_10-(int32_t)((int32_t)34))) == 22)
		{
			goto IL_00c8;
		}
		if (((int32_t)((int32_t)L_10-(int32_t)((int32_t)34))) == 23)
		{
			goto IL_00c8;
		}
		if (((int32_t)((int32_t)L_10-(int32_t)((int32_t)34))) == 24)
		{
			goto IL_00ca;
		}
	}

IL_008d:
	{
		uint16_t L_11 = V_2;
		if (((int32_t)((int32_t)L_11-(int32_t)((int32_t)91))) == 0)
		{
			goto IL_00c0;
		}
		if (((int32_t)((int32_t)L_11-(int32_t)((int32_t)91))) == 1)
		{
			goto IL_00a2;
		}
		if (((int32_t)((int32_t)L_11-(int32_t)((int32_t)91))) == 2)
		{
			goto IL_00c2;
		}
	}

IL_00a2:
	{
		uint16_t L_12 = V_2;
		if (((int32_t)((int32_t)L_12-(int32_t)((int32_t)123))) == 0)
		{
			goto IL_00bc;
		}
		if (((int32_t)((int32_t)L_12-(int32_t)((int32_t)123))) == 1)
		{
			goto IL_00cc;
		}
		if (((int32_t)((int32_t)L_12-(int32_t)((int32_t)123))) == 2)
		{
			goto IL_00be;
		}
	}
	{
		goto IL_00cc;
	}

IL_00bc:
	{
		return 1;
	}

IL_00be:
	{
		return 2;
	}

IL_00c0:
	{
		return 3;
	}

IL_00c2:
	{
		return 4;
	}

IL_00c4:
	{
		return 6;
	}

IL_00c6:
	{
		return 7;
	}

IL_00c8:
	{
		return 8;
	}

IL_00ca:
	{
		return 5;
	}

IL_00cc:
	{
		int32_t* L_13 = ___index1;
		int32_t* L_14 = ___index1;
		*((int32_t*)(L_13)) = (int32_t)((int32_t)((int32_t)(*((int32_t*)L_14))-(int32_t)1));
		CharU5BU5D_t3416858730* L_15 = ___json0;
		NullCheck(L_15);
		int32_t* L_16 = ___index1;
		V_1 = ((int32_t)((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_15)->max_length))))-(int32_t)(*((int32_t*)L_16))));
		int32_t L_17 = V_1;
		if ((((int32_t)L_17) < ((int32_t)5)))
		{
			goto IL_0128;
		}
	}
	{
		CharU5BU5D_t3416858730* L_18 = ___json0;
		int32_t* L_19 = ___index1;
		NullCheck(L_18);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_18, (*((int32_t*)L_19)));
		int32_t L_20 = (*((int32_t*)L_19));
		if ((!(((uint32_t)((L_18)->GetAt(static_cast<il2cpp_array_size_t>(L_20)))) == ((uint32_t)((int32_t)102)))))
		{
			goto IL_0128;
		}
	}
	{
		CharU5BU5D_t3416858730* L_21 = ___json0;
		int32_t* L_22 = ___index1;
		NullCheck(L_21);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_21, ((int32_t)((int32_t)(*((int32_t*)L_22))+(int32_t)1)));
		int32_t L_23 = ((int32_t)((int32_t)(*((int32_t*)L_22))+(int32_t)1));
		if ((!(((uint32_t)((L_21)->GetAt(static_cast<il2cpp_array_size_t>(L_23)))) == ((uint32_t)((int32_t)97)))))
		{
			goto IL_0128;
		}
	}
	{
		CharU5BU5D_t3416858730* L_24 = ___json0;
		int32_t* L_25 = ___index1;
		NullCheck(L_24);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_24, ((int32_t)((int32_t)(*((int32_t*)L_25))+(int32_t)2)));
		int32_t L_26 = ((int32_t)((int32_t)(*((int32_t*)L_25))+(int32_t)2));
		if ((!(((uint32_t)((L_24)->GetAt(static_cast<il2cpp_array_size_t>(L_26)))) == ((uint32_t)((int32_t)108)))))
		{
			goto IL_0128;
		}
	}
	{
		CharU5BU5D_t3416858730* L_27 = ___json0;
		int32_t* L_28 = ___index1;
		NullCheck(L_27);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_27, ((int32_t)((int32_t)(*((int32_t*)L_28))+(int32_t)3)));
		int32_t L_29 = ((int32_t)((int32_t)(*((int32_t*)L_28))+(int32_t)3));
		if ((!(((uint32_t)((L_27)->GetAt(static_cast<il2cpp_array_size_t>(L_29)))) == ((uint32_t)((int32_t)115)))))
		{
			goto IL_0128;
		}
	}
	{
		CharU5BU5D_t3416858730* L_30 = ___json0;
		int32_t* L_31 = ___index1;
		NullCheck(L_30);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_30, ((int32_t)((int32_t)(*((int32_t*)L_31))+(int32_t)4)));
		int32_t L_32 = ((int32_t)((int32_t)(*((int32_t*)L_31))+(int32_t)4));
		if ((!(((uint32_t)((L_30)->GetAt(static_cast<il2cpp_array_size_t>(L_32)))) == ((uint32_t)((int32_t)101)))))
		{
			goto IL_0128;
		}
	}
	{
		int32_t* L_33 = ___index1;
		int32_t* L_34 = ___index1;
		*((int32_t*)(L_33)) = (int32_t)((int32_t)((int32_t)(*((int32_t*)L_34))+(int32_t)5));
		return ((int32_t)10);
	}

IL_0128:
	{
		int32_t L_35 = V_1;
		if ((((int32_t)L_35) < ((int32_t)4)))
		{
			goto IL_016a;
		}
	}
	{
		CharU5BU5D_t3416858730* L_36 = ___json0;
		int32_t* L_37 = ___index1;
		NullCheck(L_36);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_36, (*((int32_t*)L_37)));
		int32_t L_38 = (*((int32_t*)L_37));
		if ((!(((uint32_t)((L_36)->GetAt(static_cast<il2cpp_array_size_t>(L_38)))) == ((uint32_t)((int32_t)116)))))
		{
			goto IL_016a;
		}
	}
	{
		CharU5BU5D_t3416858730* L_39 = ___json0;
		int32_t* L_40 = ___index1;
		NullCheck(L_39);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_39, ((int32_t)((int32_t)(*((int32_t*)L_40))+(int32_t)1)));
		int32_t L_41 = ((int32_t)((int32_t)(*((int32_t*)L_40))+(int32_t)1));
		if ((!(((uint32_t)((L_39)->GetAt(static_cast<il2cpp_array_size_t>(L_41)))) == ((uint32_t)((int32_t)114)))))
		{
			goto IL_016a;
		}
	}
	{
		CharU5BU5D_t3416858730* L_42 = ___json0;
		int32_t* L_43 = ___index1;
		NullCheck(L_42);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_42, ((int32_t)((int32_t)(*((int32_t*)L_43))+(int32_t)2)));
		int32_t L_44 = ((int32_t)((int32_t)(*((int32_t*)L_43))+(int32_t)2));
		if ((!(((uint32_t)((L_42)->GetAt(static_cast<il2cpp_array_size_t>(L_44)))) == ((uint32_t)((int32_t)117)))))
		{
			goto IL_016a;
		}
	}
	{
		CharU5BU5D_t3416858730* L_45 = ___json0;
		int32_t* L_46 = ___index1;
		NullCheck(L_45);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_45, ((int32_t)((int32_t)(*((int32_t*)L_46))+(int32_t)3)));
		int32_t L_47 = ((int32_t)((int32_t)(*((int32_t*)L_46))+(int32_t)3));
		if ((!(((uint32_t)((L_45)->GetAt(static_cast<il2cpp_array_size_t>(L_47)))) == ((uint32_t)((int32_t)101)))))
		{
			goto IL_016a;
		}
	}
	{
		int32_t* L_48 = ___index1;
		int32_t* L_49 = ___index1;
		*((int32_t*)(L_48)) = (int32_t)((int32_t)((int32_t)(*((int32_t*)L_49))+(int32_t)4));
		return ((int32_t)9);
	}

IL_016a:
	{
		int32_t L_50 = V_1;
		if ((((int32_t)L_50) < ((int32_t)4)))
		{
			goto IL_01ac;
		}
	}
	{
		CharU5BU5D_t3416858730* L_51 = ___json0;
		int32_t* L_52 = ___index1;
		NullCheck(L_51);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_51, (*((int32_t*)L_52)));
		int32_t L_53 = (*((int32_t*)L_52));
		if ((!(((uint32_t)((L_51)->GetAt(static_cast<il2cpp_array_size_t>(L_53)))) == ((uint32_t)((int32_t)110)))))
		{
			goto IL_01ac;
		}
	}
	{
		CharU5BU5D_t3416858730* L_54 = ___json0;
		int32_t* L_55 = ___index1;
		NullCheck(L_54);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_54, ((int32_t)((int32_t)(*((int32_t*)L_55))+(int32_t)1)));
		int32_t L_56 = ((int32_t)((int32_t)(*((int32_t*)L_55))+(int32_t)1));
		if ((!(((uint32_t)((L_54)->GetAt(static_cast<il2cpp_array_size_t>(L_56)))) == ((uint32_t)((int32_t)117)))))
		{
			goto IL_01ac;
		}
	}
	{
		CharU5BU5D_t3416858730* L_57 = ___json0;
		int32_t* L_58 = ___index1;
		NullCheck(L_57);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_57, ((int32_t)((int32_t)(*((int32_t*)L_58))+(int32_t)2)));
		int32_t L_59 = ((int32_t)((int32_t)(*((int32_t*)L_58))+(int32_t)2));
		if ((!(((uint32_t)((L_57)->GetAt(static_cast<il2cpp_array_size_t>(L_59)))) == ((uint32_t)((int32_t)108)))))
		{
			goto IL_01ac;
		}
	}
	{
		CharU5BU5D_t3416858730* L_60 = ___json0;
		int32_t* L_61 = ___index1;
		NullCheck(L_60);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_60, ((int32_t)((int32_t)(*((int32_t*)L_61))+(int32_t)3)));
		int32_t L_62 = ((int32_t)((int32_t)(*((int32_t*)L_61))+(int32_t)3));
		if ((!(((uint32_t)((L_60)->GetAt(static_cast<il2cpp_array_size_t>(L_62)))) == ((uint32_t)((int32_t)108)))))
		{
			goto IL_01ac;
		}
	}
	{
		int32_t* L_63 = ___index1;
		int32_t* L_64 = ___index1;
		*((int32_t*)(L_63)) = (int32_t)((int32_t)((int32_t)(*((int32_t*)L_64))+(int32_t)4));
		return ((int32_t)11);
	}

IL_01ac:
	{
		return 0;
	}
}
// System.Boolean LocalyticsUnity.MiniJSON::serializeHashtableOrArrayList(System.Object,System.Text.StringBuilder)
extern Il2CppClass* Hashtable_t3875263730_il2cpp_TypeInfo_var;
extern Il2CppClass* MiniJSON_t1258204565_il2cpp_TypeInfo_var;
extern Il2CppClass* ArrayList_t2121638921_il2cpp_TypeInfo_var;
extern const uint32_t MiniJSON_serializeHashtableOrArrayList_m269540330_MetadataUsageId;
extern "C"  bool MiniJSON_serializeHashtableOrArrayList_m269540330 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___objectOrArray0, StringBuilder_t3822575854 * ___builder1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (MiniJSON_serializeHashtableOrArrayList_m269540330_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject * L_0 = ___objectOrArray0;
		if (!((Hashtable_t3875263730 *)IsInstClass(L_0, Hashtable_t3875263730_il2cpp_TypeInfo_var)))
		{
			goto IL_0018;
		}
	}
	{
		Il2CppObject * L_1 = ___objectOrArray0;
		StringBuilder_t3822575854 * L_2 = ___builder1;
		IL2CPP_RUNTIME_CLASS_INIT(MiniJSON_t1258204565_il2cpp_TypeInfo_var);
		bool L_3 = MiniJSON_serializeHashtable_m391278148(NULL /*static, unused*/, ((Hashtable_t3875263730 *)CastclassClass(L_1, Hashtable_t3875263730_il2cpp_TypeInfo_var)), L_2, /*hidden argument*/NULL);
		return L_3;
	}

IL_0018:
	{
		Il2CppObject * L_4 = ___objectOrArray0;
		if (!((ArrayList_t2121638921 *)IsInstClass(L_4, ArrayList_t2121638921_il2cpp_TypeInfo_var)))
		{
			goto IL_0030;
		}
	}
	{
		Il2CppObject * L_5 = ___objectOrArray0;
		StringBuilder_t3822575854 * L_6 = ___builder1;
		IL2CPP_RUNTIME_CLASS_INIT(MiniJSON_t1258204565_il2cpp_TypeInfo_var);
		bool L_7 = MiniJSON_serializeIList_m1914476388(NULL /*static, unused*/, ((ArrayList_t2121638921 *)CastclassClass(L_5, ArrayList_t2121638921_il2cpp_TypeInfo_var)), L_6, /*hidden argument*/NULL);
		return L_7;
	}

IL_0030:
	{
		return (bool)0;
	}
}
// System.Boolean LocalyticsUnity.MiniJSON::serializeHashtable(System.Collections.Hashtable,System.Text.StringBuilder)
extern Il2CppClass* IDictionaryEnumerator_t1541724277_il2cpp_TypeInfo_var;
extern Il2CppClass* MiniJSON_t1258204565_il2cpp_TypeInfo_var;
extern Il2CppClass* IEnumerator_t287207039_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral123;
extern Il2CppCodeGenString* _stringLiteral1396;
extern Il2CppCodeGenString* _stringLiteral58;
extern Il2CppCodeGenString* _stringLiteral125;
extern const uint32_t MiniJSON_serializeHashtable_m391278148_MetadataUsageId;
extern "C"  bool MiniJSON_serializeHashtable_m391278148 (Il2CppObject * __this /* static, unused */, Hashtable_t3875263730 * ___anObject0, StringBuilder_t3822575854 * ___builder1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (MiniJSON_serializeHashtable_m391278148_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Il2CppObject * V_0 = NULL;
	bool V_1 = false;
	String_t* V_2 = NULL;
	Il2CppObject * V_3 = NULL;
	{
		StringBuilder_t3822575854 * L_0 = ___builder1;
		NullCheck(L_0);
		StringBuilder_Append_m3898090075(L_0, _stringLiteral123, /*hidden argument*/NULL);
		Hashtable_t3875263730 * L_1 = ___anObject0;
		NullCheck(L_1);
		Il2CppObject * L_2 = VirtFuncInvoker0< Il2CppObject * >::Invoke(36 /* System.Collections.IDictionaryEnumerator System.Collections.Hashtable::GetEnumerator() */, L_1);
		V_0 = L_2;
		V_1 = (bool)1;
		goto IL_0062;
	}

IL_001a:
	{
		Il2CppObject * L_3 = V_0;
		NullCheck(L_3);
		Il2CppObject * L_4 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(1 /* System.Object System.Collections.IDictionaryEnumerator::get_Key() */, IDictionaryEnumerator_t1541724277_il2cpp_TypeInfo_var, L_3);
		NullCheck(L_4);
		String_t* L_5 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_4);
		V_2 = L_5;
		Il2CppObject * L_6 = V_0;
		NullCheck(L_6);
		Il2CppObject * L_7 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(2 /* System.Object System.Collections.IDictionaryEnumerator::get_Value() */, IDictionaryEnumerator_t1541724277_il2cpp_TypeInfo_var, L_6);
		V_3 = L_7;
		bool L_8 = V_1;
		if (L_8)
		{
			goto IL_003f;
		}
	}
	{
		StringBuilder_t3822575854 * L_9 = ___builder1;
		NullCheck(L_9);
		StringBuilder_Append_m3898090075(L_9, _stringLiteral1396, /*hidden argument*/NULL);
	}

IL_003f:
	{
		String_t* L_10 = V_2;
		StringBuilder_t3822575854 * L_11 = ___builder1;
		IL2CPP_RUNTIME_CLASS_INIT(MiniJSON_t1258204565_il2cpp_TypeInfo_var);
		MiniJSON_serializeString_m3127747399(NULL /*static, unused*/, L_10, L_11, /*hidden argument*/NULL);
		StringBuilder_t3822575854 * L_12 = ___builder1;
		NullCheck(L_12);
		StringBuilder_Append_m3898090075(L_12, _stringLiteral58, /*hidden argument*/NULL);
		Il2CppObject * L_13 = V_3;
		StringBuilder_t3822575854 * L_14 = ___builder1;
		bool L_15 = MiniJSON_serializeObject_m2295199(NULL /*static, unused*/, L_13, L_14, /*hidden argument*/NULL);
		if (L_15)
		{
			goto IL_0060;
		}
	}
	{
		return (bool)0;
	}

IL_0060:
	{
		V_1 = (bool)0;
	}

IL_0062:
	{
		Il2CppObject * L_16 = V_0;
		NullCheck(L_16);
		bool L_17 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t287207039_il2cpp_TypeInfo_var, L_16);
		if (L_17)
		{
			goto IL_001a;
		}
	}
	{
		StringBuilder_t3822575854 * L_18 = ___builder1;
		NullCheck(L_18);
		StringBuilder_Append_m3898090075(L_18, _stringLiteral125, /*hidden argument*/NULL);
		return (bool)1;
	}
}
// System.Boolean LocalyticsUnity.MiniJSON::serializeStringStringDictionary(System.Collections.Generic.Dictionary`2<System.String,System.String>,System.Text.StringBuilder)
extern Il2CppClass* MiniJSON_t1258204565_il2cpp_TypeInfo_var;
extern Il2CppClass* Enumerator_t2373214747_il2cpp_TypeInfo_var;
extern Il2CppClass* IDisposable_t1628921374_il2cpp_TypeInfo_var;
extern const MethodInfo* Dictionary_2_GetEnumerator_m2759194411_MethodInfo_var;
extern const MethodInfo* Enumerator_get_Current_m2871721525_MethodInfo_var;
extern const MethodInfo* KeyValuePair_2_get_Key_m1739472607_MethodInfo_var;
extern const MethodInfo* KeyValuePair_2_get_Value_m730091314_MethodInfo_var;
extern const MethodInfo* Enumerator_MoveNext_m2577713898_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral123;
extern Il2CppCodeGenString* _stringLiteral1396;
extern Il2CppCodeGenString* _stringLiteral58;
extern Il2CppCodeGenString* _stringLiteral125;
extern const uint32_t MiniJSON_serializeStringStringDictionary_m3545383879_MetadataUsageId;
extern "C"  bool MiniJSON_serializeStringStringDictionary_m3545383879 (Il2CppObject * __this /* static, unused */, Dictionary_2_t2606186806 * ___dict0, StringBuilder_t3822575854 * ___builder1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (MiniJSON_serializeStringStringDictionary_m3545383879_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	bool V_0 = false;
	KeyValuePair_2_t2094718104  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Enumerator_t2373214747  V_2;
	memset(&V_2, 0, sizeof(V_2));
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		StringBuilder_t3822575854 * L_0 = ___builder1;
		NullCheck(L_0);
		StringBuilder_Append_m3898090075(L_0, _stringLiteral123, /*hidden argument*/NULL);
		V_0 = (bool)1;
		Dictionary_2_t2606186806 * L_1 = ___dict0;
		NullCheck(L_1);
		Enumerator_t2373214747  L_2 = Dictionary_2_GetEnumerator_m2759194411(L_1, /*hidden argument*/Dictionary_2_GetEnumerator_m2759194411_MethodInfo_var);
		V_2 = L_2;
	}

IL_0015:
	try
	{ // begin try (depth: 1)
		{
			goto IL_005c;
		}

IL_001a:
		{
			KeyValuePair_2_t2094718104  L_3 = Enumerator_get_Current_m2871721525((&V_2), /*hidden argument*/Enumerator_get_Current_m2871721525_MethodInfo_var);
			V_1 = L_3;
			bool L_4 = V_0;
			if (L_4)
			{
				goto IL_0034;
			}
		}

IL_0028:
		{
			StringBuilder_t3822575854 * L_5 = ___builder1;
			NullCheck(L_5);
			StringBuilder_Append_m3898090075(L_5, _stringLiteral1396, /*hidden argument*/NULL);
		}

IL_0034:
		{
			String_t* L_6 = KeyValuePair_2_get_Key_m1739472607((&V_1), /*hidden argument*/KeyValuePair_2_get_Key_m1739472607_MethodInfo_var);
			StringBuilder_t3822575854 * L_7 = ___builder1;
			IL2CPP_RUNTIME_CLASS_INIT(MiniJSON_t1258204565_il2cpp_TypeInfo_var);
			MiniJSON_serializeString_m3127747399(NULL /*static, unused*/, L_6, L_7, /*hidden argument*/NULL);
			StringBuilder_t3822575854 * L_8 = ___builder1;
			NullCheck(L_8);
			StringBuilder_Append_m3898090075(L_8, _stringLiteral58, /*hidden argument*/NULL);
			String_t* L_9 = KeyValuePair_2_get_Value_m730091314((&V_1), /*hidden argument*/KeyValuePair_2_get_Value_m730091314_MethodInfo_var);
			StringBuilder_t3822575854 * L_10 = ___builder1;
			MiniJSON_serializeString_m3127747399(NULL /*static, unused*/, L_9, L_10, /*hidden argument*/NULL);
			V_0 = (bool)0;
		}

IL_005c:
		{
			bool L_11 = Enumerator_MoveNext_m2577713898((&V_2), /*hidden argument*/Enumerator_MoveNext_m2577713898_MethodInfo_var);
			if (L_11)
			{
				goto IL_001a;
			}
		}

IL_0068:
		{
			IL2CPP_LEAVE(0x79, FINALLY_006d);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_006d;
	}

FINALLY_006d:
	{ // begin finally (depth: 1)
		Enumerator_t2373214747  L_12 = V_2;
		Enumerator_t2373214747  L_13 = L_12;
		Il2CppObject * L_14 = Box(Enumerator_t2373214747_il2cpp_TypeInfo_var, &L_13);
		NullCheck((Il2CppObject *)L_14);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, (Il2CppObject *)L_14);
		IL2CPP_END_FINALLY(109)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(109)
	{
		IL2CPP_JUMP_TBL(0x79, IL_0079)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0079:
	{
		StringBuilder_t3822575854 * L_15 = ___builder1;
		NullCheck(L_15);
		StringBuilder_Append_m3898090075(L_15, _stringLiteral125, /*hidden argument*/NULL);
		return (bool)1;
	}
}
// System.Boolean LocalyticsUnity.MiniJSON::serializeStringObjectDictionary(System.Collections.Generic.Dictionary`2<System.String,System.Object>,System.Text.StringBuilder)
extern Il2CppClass* MiniJSON_t1258204565_il2cpp_TypeInfo_var;
extern Il2CppClass* Enumerator_t2241832265_il2cpp_TypeInfo_var;
extern Il2CppClass* IDisposable_t1628921374_il2cpp_TypeInfo_var;
extern const MethodInfo* Dictionary_2_GetEnumerator_m3494254192_MethodInfo_var;
extern const MethodInfo* Enumerator_get_Current_m489173242_MethodInfo_var;
extern const MethodInfo* KeyValuePair_2_get_Key_m945524694_MethodInfo_var;
extern const MethodInfo* KeyValuePair_2_get_Value_m2250325609_MethodInfo_var;
extern const MethodInfo* Enumerator_MoveNext_m890755205_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral123;
extern Il2CppCodeGenString* _stringLiteral1396;
extern Il2CppCodeGenString* _stringLiteral58;
extern Il2CppCodeGenString* _stringLiteral125;
extern const uint32_t MiniJSON_serializeStringObjectDictionary_m4272623523_MetadataUsageId;
extern "C"  bool MiniJSON_serializeStringObjectDictionary_m4272623523 (Il2CppObject * __this /* static, unused */, Dictionary_2_t2474804324 * ___dict0, StringBuilder_t3822575854 * ___builder1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (MiniJSON_serializeStringObjectDictionary_m4272623523_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	bool V_0 = false;
	KeyValuePair_2_t1963335622  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Enumerator_t2241832265  V_2;
	memset(&V_2, 0, sizeof(V_2));
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		StringBuilder_t3822575854 * L_0 = ___builder1;
		NullCheck(L_0);
		StringBuilder_Append_m3898090075(L_0, _stringLiteral123, /*hidden argument*/NULL);
		V_0 = (bool)1;
		Dictionary_2_t2474804324 * L_1 = ___dict0;
		NullCheck(L_1);
		Enumerator_t2241832265  L_2 = Dictionary_2_GetEnumerator_m3494254192(L_1, /*hidden argument*/Dictionary_2_GetEnumerator_m3494254192_MethodInfo_var);
		V_2 = L_2;
	}

IL_0015:
	try
	{ // begin try (depth: 1)
		{
			goto IL_005d;
		}

IL_001a:
		{
			KeyValuePair_2_t1963335622  L_3 = Enumerator_get_Current_m489173242((&V_2), /*hidden argument*/Enumerator_get_Current_m489173242_MethodInfo_var);
			V_1 = L_3;
			bool L_4 = V_0;
			if (L_4)
			{
				goto IL_0034;
			}
		}

IL_0028:
		{
			StringBuilder_t3822575854 * L_5 = ___builder1;
			NullCheck(L_5);
			StringBuilder_Append_m3898090075(L_5, _stringLiteral1396, /*hidden argument*/NULL);
		}

IL_0034:
		{
			String_t* L_6 = KeyValuePair_2_get_Key_m945524694((&V_1), /*hidden argument*/KeyValuePair_2_get_Key_m945524694_MethodInfo_var);
			StringBuilder_t3822575854 * L_7 = ___builder1;
			IL2CPP_RUNTIME_CLASS_INIT(MiniJSON_t1258204565_il2cpp_TypeInfo_var);
			MiniJSON_serializeString_m3127747399(NULL /*static, unused*/, L_6, L_7, /*hidden argument*/NULL);
			StringBuilder_t3822575854 * L_8 = ___builder1;
			NullCheck(L_8);
			StringBuilder_Append_m3898090075(L_8, _stringLiteral58, /*hidden argument*/NULL);
			Il2CppObject * L_9 = KeyValuePair_2_get_Value_m2250325609((&V_1), /*hidden argument*/KeyValuePair_2_get_Value_m2250325609_MethodInfo_var);
			StringBuilder_t3822575854 * L_10 = ___builder1;
			MiniJSON_serializeObject_m2295199(NULL /*static, unused*/, L_9, L_10, /*hidden argument*/NULL);
			V_0 = (bool)0;
		}

IL_005d:
		{
			bool L_11 = Enumerator_MoveNext_m890755205((&V_2), /*hidden argument*/Enumerator_MoveNext_m890755205_MethodInfo_var);
			if (L_11)
			{
				goto IL_001a;
			}
		}

IL_0069:
		{
			IL2CPP_LEAVE(0x7A, FINALLY_006e);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_006e;
	}

FINALLY_006e:
	{ // begin finally (depth: 1)
		Enumerator_t2241832265  L_12 = V_2;
		Enumerator_t2241832265  L_13 = L_12;
		Il2CppObject * L_14 = Box(Enumerator_t2241832265_il2cpp_TypeInfo_var, &L_13);
		NullCheck((Il2CppObject *)L_14);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, (Il2CppObject *)L_14);
		IL2CPP_END_FINALLY(110)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(110)
	{
		IL2CPP_JUMP_TBL(0x7A, IL_007a)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_007a:
	{
		StringBuilder_t3822575854 * L_15 = ___builder1;
		NullCheck(L_15);
		StringBuilder_Append_m3898090075(L_15, _stringLiteral125, /*hidden argument*/NULL);
		return (bool)1;
	}
}
// System.Boolean LocalyticsUnity.MiniJSON::serializeIList(System.Collections.IList,System.Text.StringBuilder)
extern Il2CppClass* IList_t1612618265_il2cpp_TypeInfo_var;
extern Il2CppClass* MiniJSON_t1258204565_il2cpp_TypeInfo_var;
extern Il2CppClass* ICollection_t3761522009_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral91;
extern Il2CppCodeGenString* _stringLiteral1396;
extern Il2CppCodeGenString* _stringLiteral93;
extern const uint32_t MiniJSON_serializeIList_m1914476388_MetadataUsageId;
extern "C"  bool MiniJSON_serializeIList_m1914476388 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___anArray0, StringBuilder_t3822575854 * ___builder1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (MiniJSON_serializeIList_m1914476388_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	bool V_0 = false;
	int32_t V_1 = 0;
	Il2CppObject * V_2 = NULL;
	{
		StringBuilder_t3822575854 * L_0 = ___builder1;
		NullCheck(L_0);
		StringBuilder_Append_m3898090075(L_0, _stringLiteral91, /*hidden argument*/NULL);
		V_0 = (bool)1;
		V_1 = 0;
		goto IL_0043;
	}

IL_0015:
	{
		Il2CppObject * L_1 = ___anArray0;
		int32_t L_2 = V_1;
		NullCheck(L_1);
		Il2CppObject * L_3 = InterfaceFuncInvoker1< Il2CppObject *, int32_t >::Invoke(2 /* System.Object System.Collections.IList::get_Item(System.Int32) */, IList_t1612618265_il2cpp_TypeInfo_var, L_1, L_2);
		V_2 = L_3;
		bool L_4 = V_0;
		if (L_4)
		{
			goto IL_002f;
		}
	}
	{
		StringBuilder_t3822575854 * L_5 = ___builder1;
		NullCheck(L_5);
		StringBuilder_Append_m3898090075(L_5, _stringLiteral1396, /*hidden argument*/NULL);
	}

IL_002f:
	{
		Il2CppObject * L_6 = V_2;
		StringBuilder_t3822575854 * L_7 = ___builder1;
		IL2CPP_RUNTIME_CLASS_INIT(MiniJSON_t1258204565_il2cpp_TypeInfo_var);
		bool L_8 = MiniJSON_serializeObject_m2295199(NULL /*static, unused*/, L_6, L_7, /*hidden argument*/NULL);
		if (L_8)
		{
			goto IL_003d;
		}
	}
	{
		return (bool)0;
	}

IL_003d:
	{
		V_0 = (bool)0;
		int32_t L_9 = V_1;
		V_1 = ((int32_t)((int32_t)L_9+(int32_t)1));
	}

IL_0043:
	{
		int32_t L_10 = V_1;
		Il2CppObject * L_11 = ___anArray0;
		NullCheck(L_11);
		int32_t L_12 = InterfaceFuncInvoker0< int32_t >::Invoke(0 /* System.Int32 System.Collections.ICollection::get_Count() */, ICollection_t3761522009_il2cpp_TypeInfo_var, L_11);
		if ((((int32_t)L_10) < ((int32_t)L_12)))
		{
			goto IL_0015;
		}
	}
	{
		StringBuilder_t3822575854 * L_13 = ___builder1;
		NullCheck(L_13);
		StringBuilder_Append_m3898090075(L_13, _stringLiteral93, /*hidden argument*/NULL);
		return (bool)1;
	}
}
// System.Boolean LocalyticsUnity.MiniJSON::serializeObject(System.Object,System.Text.StringBuilder)
extern Il2CppClass* IList_t1612618265_il2cpp_TypeInfo_var;
extern Il2CppClass* MiniJSON_t1258204565_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Char_t2778706699_il2cpp_TypeInfo_var;
extern Il2CppClass* Convert_t1097883944_il2cpp_TypeInfo_var;
extern Il2CppClass* Decimal_t1688557254_il2cpp_TypeInfo_var;
extern Il2CppClass* Hashtable_t3875263730_il2cpp_TypeInfo_var;
extern Il2CppClass* Dictionary_2_t2606186806_il2cpp_TypeInfo_var;
extern Il2CppClass* Dictionary_2_t2474804324_il2cpp_TypeInfo_var;
extern Il2CppClass* ArrayList_t2121638921_il2cpp_TypeInfo_var;
extern Il2CppClass* Boolean_t211005341_il2cpp_TypeInfo_var;
extern Il2CppClass* DateTime_t339033936_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3392903;
extern Il2CppCodeGenString* _stringLiteral3569038;
extern Il2CppCodeGenString* _stringLiteral97196323;
extern const uint32_t MiniJSON_serializeObject_m2295199_MetadataUsageId;
extern "C"  bool MiniJSON_serializeObject_m2295199 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___value0, StringBuilder_t3822575854 * ___builder1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (MiniJSON_serializeObject_m2295199_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Type_t * V_0 = NULL;
	DateTime_t339033936  V_1;
	memset(&V_1, 0, sizeof(V_1));
	DateTime_t339033936  V_2;
	memset(&V_2, 0, sizeof(V_2));
	TimeSpan_t763862892  V_3;
	memset(&V_3, 0, sizeof(V_3));
	StringBuilder_t3822575854 * G_B23_0 = NULL;
	StringBuilder_t3822575854 * G_B22_0 = NULL;
	String_t* G_B24_0 = NULL;
	StringBuilder_t3822575854 * G_B24_1 = NULL;
	{
		Il2CppObject * L_0 = ___value0;
		NullCheck(L_0);
		Type_t * L_1 = Object_GetType_m2022236990(L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		Il2CppObject * L_2 = ___value0;
		if (L_2)
		{
			goto IL_001e;
		}
	}
	{
		StringBuilder_t3822575854 * L_3 = ___builder1;
		NullCheck(L_3);
		StringBuilder_Append_m3898090075(L_3, _stringLiteral3392903, /*hidden argument*/NULL);
		goto IL_01b1;
	}

IL_001e:
	{
		Type_t * L_4 = V_0;
		NullCheck(L_4);
		bool L_5 = VirtFuncInvoker0< bool >::Invoke(22 /* System.Boolean System.Type::get_IsArray() */, L_4);
		if (L_5)
		{
			goto IL_003f;
		}
	}
	{
		Type_t * L_6 = V_0;
		NullCheck(L_6);
		bool L_7 = VirtFuncInvoker0< bool >::Invoke(97 /* System.Boolean System.Type::get_IsGenericType() */, L_6);
		if (!L_7)
		{
			goto IL_0051;
		}
	}
	{
		Il2CppObject * L_8 = ___value0;
		if (!((Il2CppObject *)IsInst(L_8, IList_t1612618265_il2cpp_TypeInfo_var)))
		{
			goto IL_0051;
		}
	}

IL_003f:
	{
		Il2CppObject * L_9 = ___value0;
		StringBuilder_t3822575854 * L_10 = ___builder1;
		IL2CPP_RUNTIME_CLASS_INIT(MiniJSON_t1258204565_il2cpp_TypeInfo_var);
		MiniJSON_serializeIList_m1914476388(NULL /*static, unused*/, ((Il2CppObject *)Castclass(L_9, IList_t1612618265_il2cpp_TypeInfo_var)), L_10, /*hidden argument*/NULL);
		goto IL_01b1;
	}

IL_0051:
	{
		Il2CppObject * L_11 = ___value0;
		if (!((String_t*)IsInstSealed(L_11, String_t_il2cpp_TypeInfo_var)))
		{
			goto IL_006d;
		}
	}
	{
		Il2CppObject * L_12 = ___value0;
		StringBuilder_t3822575854 * L_13 = ___builder1;
		IL2CPP_RUNTIME_CLASS_INIT(MiniJSON_t1258204565_il2cpp_TypeInfo_var);
		MiniJSON_serializeString_m3127747399(NULL /*static, unused*/, ((String_t*)CastclassSealed(L_12, String_t_il2cpp_TypeInfo_var)), L_13, /*hidden argument*/NULL);
		goto IL_01b1;
	}

IL_006d:
	{
		Il2CppObject * L_14 = ___value0;
		if (!((Il2CppObject *)IsInstSealed(L_14, Char_t2778706699_il2cpp_TypeInfo_var)))
		{
			goto IL_008e;
		}
	}
	{
		Il2CppObject * L_15 = ___value0;
		IL2CPP_RUNTIME_CLASS_INIT(Convert_t1097883944_il2cpp_TypeInfo_var);
		String_t* L_16 = Convert_ToString_m3462155176(NULL /*static, unused*/, ((*(uint16_t*)((uint16_t*)UnBox (L_15, Char_t2778706699_il2cpp_TypeInfo_var)))), /*hidden argument*/NULL);
		StringBuilder_t3822575854 * L_17 = ___builder1;
		IL2CPP_RUNTIME_CLASS_INIT(MiniJSON_t1258204565_il2cpp_TypeInfo_var);
		MiniJSON_serializeString_m3127747399(NULL /*static, unused*/, L_16, L_17, /*hidden argument*/NULL);
		goto IL_01b1;
	}

IL_008e:
	{
		Il2CppObject * L_18 = ___value0;
		if (!((Il2CppObject *)IsInstSealed(L_18, Decimal_t1688557254_il2cpp_TypeInfo_var)))
		{
			goto IL_00af;
		}
	}
	{
		Il2CppObject * L_19 = ___value0;
		IL2CPP_RUNTIME_CLASS_INIT(Convert_t1097883944_il2cpp_TypeInfo_var);
		String_t* L_20 = Convert_ToString_m851570371(NULL /*static, unused*/, ((*(Decimal_t1688557254 *)((Decimal_t1688557254 *)UnBox (L_19, Decimal_t1688557254_il2cpp_TypeInfo_var)))), /*hidden argument*/NULL);
		StringBuilder_t3822575854 * L_21 = ___builder1;
		IL2CPP_RUNTIME_CLASS_INIT(MiniJSON_t1258204565_il2cpp_TypeInfo_var);
		MiniJSON_serializeString_m3127747399(NULL /*static, unused*/, L_20, L_21, /*hidden argument*/NULL);
		goto IL_01b1;
	}

IL_00af:
	{
		Il2CppObject * L_22 = ___value0;
		if (!((Hashtable_t3875263730 *)IsInstClass(L_22, Hashtable_t3875263730_il2cpp_TypeInfo_var)))
		{
			goto IL_00cc;
		}
	}
	{
		Il2CppObject * L_23 = ___value0;
		StringBuilder_t3822575854 * L_24 = ___builder1;
		IL2CPP_RUNTIME_CLASS_INIT(MiniJSON_t1258204565_il2cpp_TypeInfo_var);
		MiniJSON_serializeHashtable_m391278148(NULL /*static, unused*/, ((Hashtable_t3875263730 *)CastclassClass(L_23, Hashtable_t3875263730_il2cpp_TypeInfo_var)), L_24, /*hidden argument*/NULL);
		goto IL_01b1;
	}

IL_00cc:
	{
		Il2CppObject * L_25 = ___value0;
		if (!((Dictionary_2_t2606186806 *)IsInstClass(L_25, Dictionary_2_t2606186806_il2cpp_TypeInfo_var)))
		{
			goto IL_00e9;
		}
	}
	{
		Il2CppObject * L_26 = ___value0;
		StringBuilder_t3822575854 * L_27 = ___builder1;
		IL2CPP_RUNTIME_CLASS_INIT(MiniJSON_t1258204565_il2cpp_TypeInfo_var);
		MiniJSON_serializeStringStringDictionary_m3545383879(NULL /*static, unused*/, ((Dictionary_2_t2606186806 *)CastclassClass(L_26, Dictionary_2_t2606186806_il2cpp_TypeInfo_var)), L_27, /*hidden argument*/NULL);
		goto IL_01b1;
	}

IL_00e9:
	{
		Il2CppObject * L_28 = ___value0;
		if (!((Dictionary_2_t2474804324 *)IsInstClass(L_28, Dictionary_2_t2474804324_il2cpp_TypeInfo_var)))
		{
			goto IL_0106;
		}
	}
	{
		Il2CppObject * L_29 = ___value0;
		StringBuilder_t3822575854 * L_30 = ___builder1;
		IL2CPP_RUNTIME_CLASS_INIT(MiniJSON_t1258204565_il2cpp_TypeInfo_var);
		MiniJSON_serializeStringObjectDictionary_m4272623523(NULL /*static, unused*/, ((Dictionary_2_t2474804324 *)CastclassClass(L_29, Dictionary_2_t2474804324_il2cpp_TypeInfo_var)), L_30, /*hidden argument*/NULL);
		goto IL_01b1;
	}

IL_0106:
	{
		Il2CppObject * L_31 = ___value0;
		if (!((ArrayList_t2121638921 *)IsInstClass(L_31, ArrayList_t2121638921_il2cpp_TypeInfo_var)))
		{
			goto IL_0123;
		}
	}
	{
		Il2CppObject * L_32 = ___value0;
		StringBuilder_t3822575854 * L_33 = ___builder1;
		IL2CPP_RUNTIME_CLASS_INIT(MiniJSON_t1258204565_il2cpp_TypeInfo_var);
		MiniJSON_serializeIList_m1914476388(NULL /*static, unused*/, ((ArrayList_t2121638921 *)CastclassClass(L_32, ArrayList_t2121638921_il2cpp_TypeInfo_var)), L_33, /*hidden argument*/NULL);
		goto IL_01b1;
	}

IL_0123:
	{
		Il2CppObject * L_34 = ___value0;
		if (!((Il2CppObject *)IsInstSealed(L_34, Boolean_t211005341_il2cpp_TypeInfo_var)))
		{
			goto IL_0154;
		}
	}
	{
		StringBuilder_t3822575854 * L_35 = ___builder1;
		Il2CppObject * L_36 = ___value0;
		G_B22_0 = L_35;
		if (!((*(bool*)((bool*)UnBox (L_36, Boolean_t211005341_il2cpp_TypeInfo_var)))))
		{
			G_B23_0 = L_35;
			goto IL_0144;
		}
	}
	{
		G_B24_0 = _stringLiteral3569038;
		G_B24_1 = G_B22_0;
		goto IL_0149;
	}

IL_0144:
	{
		G_B24_0 = _stringLiteral97196323;
		G_B24_1 = G_B23_0;
	}

IL_0149:
	{
		NullCheck(G_B24_1);
		StringBuilder_Append_m3898090075(G_B24_1, G_B24_0, /*hidden argument*/NULL);
		goto IL_01b1;
	}

IL_0154:
	{
		Type_t * L_37 = V_0;
		NullCheck(L_37);
		bool L_38 = VirtFuncInvoker0< bool >::Invoke(32 /* System.Boolean System.Type::get_IsPrimitive() */, L_37);
		if (!L_38)
		{
			goto IL_0170;
		}
	}
	{
		Il2CppObject * L_39 = ___value0;
		IL2CPP_RUNTIME_CLASS_INIT(Convert_t1097883944_il2cpp_TypeInfo_var);
		double L_40 = Convert_ToDouble_m1941295007(NULL /*static, unused*/, L_39, /*hidden argument*/NULL);
		StringBuilder_t3822575854 * L_41 = ___builder1;
		IL2CPP_RUNTIME_CLASS_INIT(MiniJSON_t1258204565_il2cpp_TypeInfo_var);
		MiniJSON_serializeNumber_m236893007(NULL /*static, unused*/, L_40, L_41, /*hidden argument*/NULL);
		goto IL_01b1;
	}

IL_0170:
	{
		Il2CppObject * L_42 = ___value0;
		if (!((Il2CppObject *)IsInstSealed(L_42, DateTime_t339033936_il2cpp_TypeInfo_var)))
		{
			goto IL_01af;
		}
	}
	{
		DateTime__ctor_m3805233578((&V_1), ((int32_t)1970), 1, 1, 0, 0, 0, 1, /*hidden argument*/NULL);
		Il2CppObject * L_43 = ___value0;
		V_2 = ((*(DateTime_t339033936 *)((DateTime_t339033936 *)UnBox (L_43, DateTime_t339033936_il2cpp_TypeInfo_var))));
		DateTime_t339033936  L_44 = V_1;
		TimeSpan_t763862892  L_45 = DateTime_Subtract_m3859199767((&V_2), L_44, /*hidden argument*/NULL);
		V_3 = L_45;
		double L_46 = TimeSpan_get_TotalMilliseconds_m4053613548((&V_3), /*hidden argument*/NULL);
		StringBuilder_t3822575854 * L_47 = ___builder1;
		IL2CPP_RUNTIME_CLASS_INIT(MiniJSON_t1258204565_il2cpp_TypeInfo_var);
		MiniJSON_serializeNumber_m236893007(NULL /*static, unused*/, L_46, L_47, /*hidden argument*/NULL);
		goto IL_01b1;
	}

IL_01af:
	{
		return (bool)0;
	}

IL_01b1:
	{
		return (bool)1;
	}
}
// System.Void LocalyticsUnity.MiniJSON::serializeString(System.String,System.Text.StringBuilder)
extern Il2CppClass* Convert_t1097883944_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral34;
extern Il2CppCodeGenString* _stringLiteral2886;
extern Il2CppCodeGenString* _stringLiteral2944;
extern Il2CppCodeGenString* _stringLiteral2950;
extern Il2CppCodeGenString* _stringLiteral2954;
extern Il2CppCodeGenString* _stringLiteral2962;
extern Il2CppCodeGenString* _stringLiteral2966;
extern Il2CppCodeGenString* _stringLiteral2968;
extern Il2CppCodeGenString* _stringLiteral2969;
extern const uint32_t MiniJSON_serializeString_m3127747399_MetadataUsageId;
extern "C"  void MiniJSON_serializeString_m3127747399 (Il2CppObject * __this /* static, unused */, String_t* ___aString0, StringBuilder_t3822575854 * ___builder1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (MiniJSON_serializeString_m3127747399_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	CharU5BU5D_t3416858730* V_0 = NULL;
	int32_t V_1 = 0;
	uint16_t V_2 = 0x0;
	int32_t V_3 = 0;
	{
		StringBuilder_t3822575854 * L_0 = ___builder1;
		NullCheck(L_0);
		StringBuilder_Append_m3898090075(L_0, _stringLiteral34, /*hidden argument*/NULL);
		String_t* L_1 = ___aString0;
		NullCheck(L_1);
		CharU5BU5D_t3416858730* L_2 = String_ToCharArray_m1208288742(L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		V_1 = 0;
		goto IL_0115;
	}

IL_001a:
	{
		CharU5BU5D_t3416858730* L_3 = V_0;
		int32_t L_4 = V_1;
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, L_4);
		int32_t L_5 = L_4;
		V_2 = ((L_3)->GetAt(static_cast<il2cpp_array_size_t>(L_5)));
		uint16_t L_6 = V_2;
		if ((!(((uint32_t)L_6) == ((uint32_t)((int32_t)34)))))
		{
			goto IL_0037;
		}
	}
	{
		StringBuilder_t3822575854 * L_7 = ___builder1;
		NullCheck(L_7);
		StringBuilder_Append_m3898090075(L_7, _stringLiteral2886, /*hidden argument*/NULL);
		goto IL_0111;
	}

IL_0037:
	{
		uint16_t L_8 = V_2;
		if ((!(((uint32_t)L_8) == ((uint32_t)((int32_t)92)))))
		{
			goto IL_0050;
		}
	}
	{
		StringBuilder_t3822575854 * L_9 = ___builder1;
		NullCheck(L_9);
		StringBuilder_Append_m3898090075(L_9, _stringLiteral2944, /*hidden argument*/NULL);
		goto IL_0111;
	}

IL_0050:
	{
		uint16_t L_10 = V_2;
		if ((!(((uint32_t)L_10) == ((uint32_t)8))))
		{
			goto IL_0068;
		}
	}
	{
		StringBuilder_t3822575854 * L_11 = ___builder1;
		NullCheck(L_11);
		StringBuilder_Append_m3898090075(L_11, _stringLiteral2950, /*hidden argument*/NULL);
		goto IL_0111;
	}

IL_0068:
	{
		uint16_t L_12 = V_2;
		if ((!(((uint32_t)L_12) == ((uint32_t)((int32_t)12)))))
		{
			goto IL_0081;
		}
	}
	{
		StringBuilder_t3822575854 * L_13 = ___builder1;
		NullCheck(L_13);
		StringBuilder_Append_m3898090075(L_13, _stringLiteral2954, /*hidden argument*/NULL);
		goto IL_0111;
	}

IL_0081:
	{
		uint16_t L_14 = V_2;
		if ((!(((uint32_t)L_14) == ((uint32_t)((int32_t)10)))))
		{
			goto IL_009a;
		}
	}
	{
		StringBuilder_t3822575854 * L_15 = ___builder1;
		NullCheck(L_15);
		StringBuilder_Append_m3898090075(L_15, _stringLiteral2962, /*hidden argument*/NULL);
		goto IL_0111;
	}

IL_009a:
	{
		uint16_t L_16 = V_2;
		if ((!(((uint32_t)L_16) == ((uint32_t)((int32_t)13)))))
		{
			goto IL_00b3;
		}
	}
	{
		StringBuilder_t3822575854 * L_17 = ___builder1;
		NullCheck(L_17);
		StringBuilder_Append_m3898090075(L_17, _stringLiteral2966, /*hidden argument*/NULL);
		goto IL_0111;
	}

IL_00b3:
	{
		uint16_t L_18 = V_2;
		if ((!(((uint32_t)L_18) == ((uint32_t)((int32_t)9)))))
		{
			goto IL_00cc;
		}
	}
	{
		StringBuilder_t3822575854 * L_19 = ___builder1;
		NullCheck(L_19);
		StringBuilder_Append_m3898090075(L_19, _stringLiteral2968, /*hidden argument*/NULL);
		goto IL_0111;
	}

IL_00cc:
	{
		uint16_t L_20 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(Convert_t1097883944_il2cpp_TypeInfo_var);
		int32_t L_21 = Convert_ToInt32_m100832938(NULL /*static, unused*/, L_20, /*hidden argument*/NULL);
		V_3 = L_21;
		int32_t L_22 = V_3;
		if ((((int32_t)L_22) < ((int32_t)((int32_t)32))))
		{
			goto IL_00f0;
		}
	}
	{
		int32_t L_23 = V_3;
		if ((((int32_t)L_23) > ((int32_t)((int32_t)126))))
		{
			goto IL_00f0;
		}
	}
	{
		StringBuilder_t3822575854 * L_24 = ___builder1;
		uint16_t L_25 = V_2;
		NullCheck(L_24);
		StringBuilder_Append_m2143093878(L_24, L_25, /*hidden argument*/NULL);
		goto IL_0111;
	}

IL_00f0:
	{
		StringBuilder_t3822575854 * L_26 = ___builder1;
		int32_t L_27 = V_3;
		IL2CPP_RUNTIME_CLASS_INIT(Convert_t1097883944_il2cpp_TypeInfo_var);
		String_t* L_28 = Convert_ToString_m3908657329(NULL /*static, unused*/, L_27, ((int32_t)16), /*hidden argument*/NULL);
		NullCheck(L_28);
		String_t* L_29 = String_PadLeft_m3268206439(L_28, 4, ((int32_t)48), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_30 = String_Concat_m138640077(NULL /*static, unused*/, _stringLiteral2969, L_29, /*hidden argument*/NULL);
		NullCheck(L_26);
		StringBuilder_Append_m3898090075(L_26, L_30, /*hidden argument*/NULL);
	}

IL_0111:
	{
		int32_t L_31 = V_1;
		V_1 = ((int32_t)((int32_t)L_31+(int32_t)1));
	}

IL_0115:
	{
		int32_t L_32 = V_1;
		CharU5BU5D_t3416858730* L_33 = V_0;
		NullCheck(L_33);
		if ((((int32_t)L_32) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_33)->max_length)))))))
		{
			goto IL_001a;
		}
	}
	{
		StringBuilder_t3822575854 * L_34 = ___builder1;
		NullCheck(L_34);
		StringBuilder_Append_m3898090075(L_34, _stringLiteral34, /*hidden argument*/NULL);
		return;
	}
}
// System.Void LocalyticsUnity.MiniJSON::serializeNumber(System.Double,System.Text.StringBuilder)
extern Il2CppClass* Convert_t1097883944_il2cpp_TypeInfo_var;
extern const uint32_t MiniJSON_serializeNumber_m236893007_MetadataUsageId;
extern "C"  void MiniJSON_serializeNumber_m236893007 (Il2CppObject * __this /* static, unused */, double ___number0, StringBuilder_t3822575854 * ___builder1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (MiniJSON_serializeNumber_m236893007_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		StringBuilder_t3822575854 * L_0 = ___builder1;
		double L_1 = ___number0;
		IL2CPP_RUNTIME_CLASS_INIT(Convert_t1097883944_il2cpp_TypeInfo_var);
		String_t* L_2 = Convert_ToString_m3932406093(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		NullCheck(L_0);
		StringBuilder_Append_m3898090075(L_0, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.String LocalyticsUnity.MiniJsonExtensions::toJson(System.Collections.Hashtable)
extern Il2CppClass* MiniJSON_t1258204565_il2cpp_TypeInfo_var;
extern const uint32_t MiniJsonExtensions_toJson_m3446868454_MetadataUsageId;
extern "C"  String_t* MiniJsonExtensions_toJson_m3446868454 (Il2CppObject * __this /* static, unused */, Hashtable_t3875263730 * ___obj0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (MiniJsonExtensions_toJson_m3446868454_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Hashtable_t3875263730 * L_0 = ___obj0;
		IL2CPP_RUNTIME_CLASS_INIT(MiniJSON_t1258204565_il2cpp_TypeInfo_var);
		String_t* L_1 = MiniJSON_jsonEncode_m2701702143(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.String LocalyticsUnity.MiniJsonExtensions::toJson(System.Collections.ArrayList)
extern Il2CppClass* MiniJSON_t1258204565_il2cpp_TypeInfo_var;
extern const uint32_t MiniJsonExtensions_toJson_m624106927_MetadataUsageId;
extern "C"  String_t* MiniJsonExtensions_toJson_m624106927 (Il2CppObject * __this /* static, unused */, ArrayList_t2121638921 * ___obj0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (MiniJsonExtensions_toJson_m624106927_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		ArrayList_t2121638921 * L_0 = ___obj0;
		IL2CPP_RUNTIME_CLASS_INIT(MiniJSON_t1258204565_il2cpp_TypeInfo_var);
		String_t* L_1 = MiniJSON_jsonEncode_m2701702143(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.String LocalyticsUnity.MiniJsonExtensions::toJson(System.Collections.Generic.Dictionary`2<System.String,System.String>)
extern Il2CppClass* MiniJSON_t1258204565_il2cpp_TypeInfo_var;
extern const uint32_t MiniJsonExtensions_toJson_m237076063_MetadataUsageId;
extern "C"  String_t* MiniJsonExtensions_toJson_m237076063 (Il2CppObject * __this /* static, unused */, Dictionary_2_t2606186806 * ___obj0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (MiniJsonExtensions_toJson_m237076063_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Dictionary_2_t2606186806 * L_0 = ___obj0;
		IL2CPP_RUNTIME_CLASS_INIT(MiniJSON_t1258204565_il2cpp_TypeInfo_var);
		String_t* L_1 = MiniJSON_jsonEncode_m2701702143(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.String LocalyticsUnity.MiniJsonExtensions::toJson(System.Collections.Generic.Dictionary`2<System.String,System.Object>)
extern Il2CppClass* MiniJSON_t1258204565_il2cpp_TypeInfo_var;
extern const uint32_t MiniJsonExtensions_toJson_m2827529741_MetadataUsageId;
extern "C"  String_t* MiniJsonExtensions_toJson_m2827529741 (Il2CppObject * __this /* static, unused */, Dictionary_2_t2474804324 * ___obj0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (MiniJsonExtensions_toJson_m2827529741_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Dictionary_2_t2474804324 * L_0 = ___obj0;
		IL2CPP_RUNTIME_CLASS_INIT(MiniJSON_t1258204565_il2cpp_TypeInfo_var);
		String_t* L_1 = MiniJSON_jsonEncode_m2701702143(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Collections.ArrayList LocalyticsUnity.MiniJsonExtensions::arrayListFromJson(System.String)
extern Il2CppClass* MiniJSON_t1258204565_il2cpp_TypeInfo_var;
extern Il2CppClass* ArrayList_t2121638921_il2cpp_TypeInfo_var;
extern const uint32_t MiniJsonExtensions_arrayListFromJson_m2274644125_MetadataUsageId;
extern "C"  ArrayList_t2121638921 * MiniJsonExtensions_arrayListFromJson_m2274644125 (Il2CppObject * __this /* static, unused */, String_t* ___json0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (MiniJsonExtensions_arrayListFromJson_m2274644125_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = ___json0;
		IL2CPP_RUNTIME_CLASS_INIT(MiniJSON_t1258204565_il2cpp_TypeInfo_var);
		Il2CppObject * L_1 = MiniJSON_jsonDecode_m3895311043(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return ((ArrayList_t2121638921 *)IsInstClass(L_1, ArrayList_t2121638921_il2cpp_TypeInfo_var));
	}
}
// System.Collections.Generic.List`1<System.Object> LocalyticsUnity.MiniJsonExtensions::listFromJson(System.String)
extern Il2CppClass* MiniJSON_t1258204565_il2cpp_TypeInfo_var;
extern Il2CppClass* List_1_t1634065389_il2cpp_TypeInfo_var;
extern const uint32_t MiniJsonExtensions_listFromJson_m1921246219_MetadataUsageId;
extern "C"  List_1_t1634065389 * MiniJsonExtensions_listFromJson_m1921246219 (Il2CppObject * __this /* static, unused */, String_t* ___json0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (MiniJsonExtensions_listFromJson_m1921246219_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = ___json0;
		IL2CPP_RUNTIME_CLASS_INIT(MiniJSON_t1258204565_il2cpp_TypeInfo_var);
		Il2CppObject * L_1 = MiniJSON_jsonDecode_m4039334490(NULL /*static, unused*/, L_0, (bool)1, /*hidden argument*/NULL);
		return ((List_1_t1634065389 *)IsInstClass(L_1, List_1_t1634065389_il2cpp_TypeInfo_var));
	}
}
// System.Collections.Hashtable LocalyticsUnity.MiniJsonExtensions::hashtableFromJson(System.String)
extern Il2CppClass* MiniJSON_t1258204565_il2cpp_TypeInfo_var;
extern Il2CppClass* Hashtable_t3875263730_il2cpp_TypeInfo_var;
extern const uint32_t MiniJsonExtensions_hashtableFromJson_m2450713163_MetadataUsageId;
extern "C"  Hashtable_t3875263730 * MiniJsonExtensions_hashtableFromJson_m2450713163 (Il2CppObject * __this /* static, unused */, String_t* ___json0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (MiniJsonExtensions_hashtableFromJson_m2450713163_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = ___json0;
		IL2CPP_RUNTIME_CLASS_INIT(MiniJSON_t1258204565_il2cpp_TypeInfo_var);
		Il2CppObject * L_1 = MiniJSON_jsonDecode_m3895311043(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return ((Hashtable_t3875263730 *)IsInstClass(L_1, Hashtable_t3875263730_il2cpp_TypeInfo_var));
	}
}
// System.Collections.Generic.Dictionary`2<System.String,System.Object> LocalyticsUnity.MiniJsonExtensions::dictionaryFromJson(System.String)
extern Il2CppClass* MiniJSON_t1258204565_il2cpp_TypeInfo_var;
extern Il2CppClass* Dictionary_2_t2474804324_il2cpp_TypeInfo_var;
extern const uint32_t MiniJsonExtensions_dictionaryFromJson_m785051574_MetadataUsageId;
extern "C"  Dictionary_2_t2474804324 * MiniJsonExtensions_dictionaryFromJson_m785051574 (Il2CppObject * __this /* static, unused */, String_t* ___json0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (MiniJsonExtensions_dictionaryFromJson_m785051574_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = ___json0;
		IL2CPP_RUNTIME_CLASS_INIT(MiniJSON_t1258204565_il2cpp_TypeInfo_var);
		Il2CppObject * L_1 = MiniJSON_jsonDecode_m4039334490(NULL /*static, unused*/, L_0, (bool)1, /*hidden argument*/NULL);
		return ((Dictionary_2_t2474804324 *)IsInstClass(L_1, Dictionary_2_t2474804324_il2cpp_TypeInfo_var));
	}
}
// System.Void MaterialContainer::.ctor()
extern "C"  void MaterialContainer__ctor_m2604417969 (MaterialContainer_t3323339514 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m350695606(__this, /*hidden argument*/NULL);
		return;
	}
}
// MaterialContainer MaterialContainer::get_instance()
extern Il2CppClass* MaterialContainer_t3323339514_il2cpp_TypeInfo_var;
extern const MethodInfo* Object_FindObjectOfType_TisMaterialContainer_t3323339514_m169546711_MethodInfo_var;
extern const uint32_t MaterialContainer_get_instance_m1292130488_MetadataUsageId;
extern "C"  MaterialContainer_t3323339514 * MaterialContainer_get_instance_m1292130488 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (MaterialContainer_get_instance_m1292130488_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		MaterialContainer_t3323339514 * L_0 = ((MaterialContainer_t3323339514_StaticFields*)MaterialContainer_t3323339514_il2cpp_TypeInfo_var->static_fields)->get__instance_4();
		bool L_1 = Object_op_Equality_m1690293457(NULL /*static, unused*/, L_0, (Object_t3878351788 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_001a;
		}
	}
	{
		MaterialContainer_t3323339514 * L_2 = Object_FindObjectOfType_TisMaterialContainer_t3323339514_m169546711(NULL /*static, unused*/, /*hidden argument*/Object_FindObjectOfType_TisMaterialContainer_t3323339514_m169546711_MethodInfo_var);
		((MaterialContainer_t3323339514_StaticFields*)MaterialContainer_t3323339514_il2cpp_TypeInfo_var->static_fields)->set__instance_4(L_2);
	}

IL_001a:
	{
		MaterialContainer_t3323339514 * L_3 = ((MaterialContainer_t3323339514_StaticFields*)MaterialContainer_t3323339514_il2cpp_TypeInfo_var->static_fields)->get__instance_4();
		return L_3;
	}
}
// System.Void MaterialContainer::Start()
extern "C"  void MaterialContainer_Start_m1551555761 (MaterialContainer_t3323339514 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void MaterialContainer::Update()
extern "C"  void MaterialContainer_Update_m859440508 (MaterialContainer_t3323339514 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void MenuEntry::.ctor()
extern "C"  void MenuEntry__ctor_m202815640 (MenuEntry_t453027379 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m350695606(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void MenuEntry::SetIcon(UnityEngine.Sprite)
extern "C"  void MenuEntry_SetIcon_m445946997 (MenuEntry_t453027379 * __this, Sprite_t4006040370 * ___icon0, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void MenuEntry::SetEntryName(System.String)
extern const MethodInfo* Component_GetComponent_TisTextMesh_t583678247_m2237128948_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral3815403037;
extern const uint32_t MenuEntry_SetEntryName_m3889069723_MetadataUsageId;
extern "C"  void MenuEntry_SetEntryName_m3889069723 (MenuEntry_t453027379 * __this, String_t* ___entryName0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (MenuEntry_SetEntryName_m3889069723_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	TextMesh_t583678247 * V_0 = NULL;
	{
		Transform_t284553113 * L_0 = Component_get_transform_m2452535634(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		Transform_t284553113 * L_1 = Transform_Find_m3950449392(L_0, _stringLiteral3815403037, /*hidden argument*/NULL);
		NullCheck(L_1);
		TextMesh_t583678247 * L_2 = Component_GetComponent_TisTextMesh_t583678247_m2237128948(L_1, /*hidden argument*/Component_GetComponent_TisTextMesh_t583678247_m2237128948_MethodInfo_var);
		V_0 = L_2;
		TextMesh_t583678247 * L_3 = V_0;
		String_t* L_4 = ___entryName0;
		NullCheck(L_3);
		TextMesh_set_text_m3628430759(L_3, L_4, /*hidden argument*/NULL);
		return;
	}
}
// System.Void MenuEntry::SetEntryStatus(System.String)
extern const MethodInfo* Component_GetComponent_TisTextMesh_t583678247_m2237128948_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral3155568388;
extern const uint32_t MenuEntry_SetEntryStatus_m1051994388_MetadataUsageId;
extern "C"  void MenuEntry_SetEntryStatus_m1051994388 (MenuEntry_t453027379 * __this, String_t* ___entryStatus0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (MenuEntry_SetEntryStatus_m1051994388_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	TextMesh_t583678247 * V_0 = NULL;
	{
		Transform_t284553113 * L_0 = Component_get_transform_m2452535634(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		Transform_t284553113 * L_1 = Transform_Find_m3950449392(L_0, _stringLiteral3155568388, /*hidden argument*/NULL);
		NullCheck(L_1);
		TextMesh_t583678247 * L_2 = Component_GetComponent_TisTextMesh_t583678247_m2237128948(L_1, /*hidden argument*/Component_GetComponent_TisTextMesh_t583678247_m2237128948_MethodInfo_var);
		V_0 = L_2;
		TextMesh_t583678247 * L_3 = V_0;
		String_t* L_4 = ___entryStatus0;
		NullCheck(L_3);
		TextMesh_set_text_m3628430759(L_3, L_4, /*hidden argument*/NULL);
		return;
	}
}
// System.Void MenuEntry::SetButtonPrice(System.String)
extern const MethodInfo* Component_GetComponent_TisTextMesh_t583678247_m2237128948_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral2620026791;
extern const uint32_t MenuEntry_SetButtonPrice_m522451585_MetadataUsageId;
extern "C"  void MenuEntry_SetButtonPrice_m522451585 (MenuEntry_t453027379 * __this, String_t* ___buttonPrice0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (MenuEntry_SetButtonPrice_m522451585_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	TextMesh_t583678247 * V_0 = NULL;
	{
		Transform_t284553113 * L_0 = Component_get_transform_m2452535634(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		Transform_t284553113 * L_1 = Transform_Find_m3950449392(L_0, _stringLiteral2620026791, /*hidden argument*/NULL);
		NullCheck(L_1);
		TextMesh_t583678247 * L_2 = Component_GetComponent_TisTextMesh_t583678247_m2237128948(L_1, /*hidden argument*/Component_GetComponent_TisTextMesh_t583678247_m2237128948_MethodInfo_var);
		V_0 = L_2;
		TextMesh_t583678247 * L_3 = V_0;
		String_t* L_4 = ___buttonPrice0;
		NullCheck(L_3);
		TextMesh_set_text_m3628430759(L_3, L_4, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Message::.ctor()
extern Il2CppClass* ReactiveCollection_1_t3392720430_il2cpp_TypeInfo_var;
extern Il2CppClass* BoolReactiveProperty_t3047538250_il2cpp_TypeInfo_var;
extern const MethodInfo* ReactiveCollection_1__ctor_m405678365_MethodInfo_var;
extern const uint32_t Message__ctor_m3445918564_MetadataUsageId;
extern "C"  void Message__ctor_m3445918564 (Message_t2619578343 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Message__ctor_m3445918564_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		ReactiveCollection_1_t3392720430 * L_0 = (ReactiveCollection_1_t3392720430 *)il2cpp_codegen_object_new(ReactiveCollection_1_t3392720430_il2cpp_TypeInfo_var);
		ReactiveCollection_1__ctor_m405678365(L_0, /*hidden argument*/ReactiveCollection_1__ctor_m405678365_MethodInfo_var);
		__this->set__messages_6(L_0);
		BoolReactiveProperty_t3047538250 * L_1 = (BoolReactiveProperty_t3047538250 *)il2cpp_codegen_object_new(BoolReactiveProperty_t3047538250_il2cpp_TypeInfo_var);
		BoolReactiveProperty__ctor_m466976127(L_1, /*hidden argument*/NULL);
		__this->set_ShowMessage_7(L_1);
		MonoBehaviour__ctor_m350695606(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Message::Show(System.String)
extern const MethodInfo* Tuple_2__ctor_m3242450822_MethodInfo_var;
extern const uint32_t Message_Show_m3165151493_MetadataUsageId;
extern "C"  void Message_Show_m3165151493 (Message_t2619578343 * __this, String_t* ___text0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Message_Show_m3165151493_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		ReactiveCollection_1_t3392720430 * L_0 = __this->get__messages_6();
		String_t* L_1 = ___text0;
		Tuple_2_t2188574943  L_2;
		memset(&L_2, 0, sizeof(L_2));
		Tuple_2__ctor_m3242450822(&L_2, L_1, (Sprite_t4006040370 *)NULL, /*hidden argument*/Tuple_2__ctor_m3242450822_MethodInfo_var);
		NullCheck(L_0);
		VirtActionInvoker1< Tuple_2_t2188574943  >::Invoke(22 /* System.Void System.Collections.ObjectModel.Collection`1<UniRx.Tuple`2<System.String,UnityEngine.Sprite>>::Add(!0) */, L_0, L_2);
		return;
	}
}
// System.Void Message::Show(System.String,UnityEngine.Sprite)
extern const MethodInfo* Tuple_2__ctor_m3242450822_MethodInfo_var;
extern const uint32_t Message_Show_m452428517_MetadataUsageId;
extern "C"  void Message_Show_m452428517 (Message_t2619578343 * __this, String_t* ___text0, Sprite_t4006040370 * ___sprite1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Message_Show_m452428517_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		ReactiveCollection_1_t3392720430 * L_0 = __this->get__messages_6();
		String_t* L_1 = ___text0;
		Sprite_t4006040370 * L_2 = ___sprite1;
		Tuple_2_t2188574943  L_3;
		memset(&L_3, 0, sizeof(L_3));
		Tuple_2__ctor_m3242450822(&L_3, L_1, L_2, /*hidden argument*/Tuple_2__ctor_m3242450822_MethodInfo_var);
		NullCheck(L_0);
		VirtActionInvoker1< Tuple_2_t2188574943  >::Invoke(22 /* System.Void System.Collections.ObjectModel.Collection`1<UniRx.Tuple`2<System.String,UnityEngine.Sprite>>::Add(!0) */, L_0, L_3);
		return;
	}
}
// System.Void Message::PostInject()
extern Il2CppClass* U3CPostInjectU3Ec__AnonStorey58_t980490265_il2cpp_TypeInfo_var;
extern Il2CppClass* Action_1_t359458046_il2cpp_TypeInfo_var;
extern Il2CppClass* Message_t2619578343_il2cpp_TypeInfo_var;
extern Il2CppClass* Func_3_t3953209855_il2cpp_TypeInfo_var;
extern Il2CppClass* Observable_t258071701_il2cpp_TypeInfo_var;
extern Il2CppClass* Action_1_t3244061297_il2cpp_TypeInfo_var;
extern const MethodInfo* U3CPostInjectU3Ec__AnonStorey58_U3CU3Em__3D_m729826699_MethodInfo_var;
extern const MethodInfo* Action_1__ctor_m2036539306_MethodInfo_var;
extern const MethodInfo* ObservableExtensions_Subscribe_TisBoolean_t211005341_m2888450145_MethodInfo_var;
extern const MethodInfo* Component_GetComponent_TisRectTransform_t3317474837_m1940403147_MethodInfo_var;
extern const MethodInfo* Message_U3CPostInjectU3Em__3E_m3718947806_MethodInfo_var;
extern const MethodInfo* Func_3__ctor_m2490981866_MethodInfo_var;
extern const MethodInfo* Observable_CombineLatest_TisBoolean_t211005341_TisInt32_t2847414787_TisU3CU3E__AnonType3_2_t3095608592_m3944907965_MethodInfo_var;
extern const MethodInfo* U3CPostInjectU3Ec__AnonStorey58_U3CU3Em__3F_m1207743275_MethodInfo_var;
extern const MethodInfo* Action_1__ctor_m2875089364_MethodInfo_var;
extern const MethodInfo* ObservableExtensions_Subscribe_TisU3CU3E__AnonType3_2_t3095608592_m3781023631_MethodInfo_var;
extern const uint32_t Message_PostInject_m122226737_MetadataUsageId;
extern "C"  void Message_PostInject_m122226737 (Message_t2619578343 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Message_PostInject_m122226737_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	U3CPostInjectU3Ec__AnonStorey58_t980490265 * V_0 = NULL;
	Il2CppObject* G_B2_0 = NULL;
	BoolReactiveProperty_t3047538250 * G_B2_1 = NULL;
	Il2CppObject* G_B1_0 = NULL;
	BoolReactiveProperty_t3047538250 * G_B1_1 = NULL;
	{
		U3CPostInjectU3Ec__AnonStorey58_t980490265 * L_0 = (U3CPostInjectU3Ec__AnonStorey58_t980490265 *)il2cpp_codegen_object_new(U3CPostInjectU3Ec__AnonStorey58_t980490265_il2cpp_TypeInfo_var);
		U3CPostInjectU3Ec__AnonStorey58__ctor_m2947229850(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CPostInjectU3Ec__AnonStorey58_t980490265 * L_1 = V_0;
		NullCheck(L_1);
		L_1->set_U3CU3Ef__this_2(__this);
		BoolReactiveProperty_t3047538250 * L_2 = __this->get_ShowMessage_7();
		U3CPostInjectU3Ec__AnonStorey58_t980490265 * L_3 = V_0;
		IntPtr_t L_4;
		L_4.set_m_value_0((void*)(void*)U3CPostInjectU3Ec__AnonStorey58_U3CU3Em__3D_m729826699_MethodInfo_var);
		Action_1_t359458046 * L_5 = (Action_1_t359458046 *)il2cpp_codegen_object_new(Action_1_t359458046_il2cpp_TypeInfo_var);
		Action_1__ctor_m2036539306(L_5, L_3, L_4, /*hidden argument*/Action_1__ctor_m2036539306_MethodInfo_var);
		ObservableExtensions_Subscribe_TisBoolean_t211005341_m2888450145(NULL /*static, unused*/, L_2, L_5, /*hidden argument*/ObservableExtensions_Subscribe_TisBoolean_t211005341_m2888450145_MethodInfo_var);
		U3CPostInjectU3Ec__AnonStorey58_t980490265 * L_6 = V_0;
		Image_t3354615620 * L_7 = __this->get_Icon_3();
		NullCheck(L_7);
		RectTransform_t3317474837 * L_8 = Component_GetComponent_TisRectTransform_t3317474837_m1940403147(L_7, /*hidden argument*/Component_GetComponent_TisRectTransform_t3317474837_m1940403147_MethodInfo_var);
		NullCheck(L_6);
		L_6->set_iconRectTransform_0(L_8);
		U3CPostInjectU3Ec__AnonStorey58_t980490265 * L_9 = V_0;
		Text_t3286458198 * L_10 = __this->get_IconText_5();
		NullCheck(L_10);
		RectTransform_t3317474837 * L_11 = Component_GetComponent_TisRectTransform_t3317474837_m1940403147(L_10, /*hidden argument*/Component_GetComponent_TisRectTransform_t3317474837_m1940403147_MethodInfo_var);
		NullCheck(L_9);
		L_9->set_textRectTransform_1(L_11);
		Image_t3354615620 * L_12 = __this->get_Back_2();
		Color_t1588175760  L_13;
		memset(&L_13, 0, sizeof(L_13));
		Color__ctor_m4005717549(&L_13, (1.0f), (1.0f), (1.0f), (0.0f), /*hidden argument*/NULL);
		NullCheck(L_12);
		Graphic_set_color_m2024155608(L_12, L_13, /*hidden argument*/NULL);
		GameObject_t4012695102 * L_14 = Component_get_gameObject_m2112202034(__this, /*hidden argument*/NULL);
		NullCheck(L_14);
		GameObject_SetActive_m3538205401(L_14, (bool)0, /*hidden argument*/NULL);
		BoolReactiveProperty_t3047538250 * L_15 = __this->get_ShowMessage_7();
		ReactiveCollection_1_t3392720430 * L_16 = __this->get__messages_6();
		NullCheck(L_16);
		Il2CppObject* L_17 = VirtFuncInvoker1< Il2CppObject*, bool >::Invoke(43 /* UniRx.IObservable`1<System.Int32> UniRx.ReactiveCollection`1<UniRx.Tuple`2<System.String,UnityEngine.Sprite>>::ObserveCountChanged(System.Boolean) */, L_16, (bool)0);
		Func_3_t3953209855 * L_18 = ((Message_t2619578343_StaticFields*)Message_t2619578343_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__amU24cache6_8();
		G_B1_0 = L_17;
		G_B1_1 = L_15;
		if (L_18)
		{
			G_B2_0 = L_17;
			G_B2_1 = L_15;
			goto IL_00a1;
		}
	}
	{
		IntPtr_t L_19;
		L_19.set_m_value_0((void*)(void*)Message_U3CPostInjectU3Em__3E_m3718947806_MethodInfo_var);
		Func_3_t3953209855 * L_20 = (Func_3_t3953209855 *)il2cpp_codegen_object_new(Func_3_t3953209855_il2cpp_TypeInfo_var);
		Func_3__ctor_m2490981866(L_20, NULL, L_19, /*hidden argument*/Func_3__ctor_m2490981866_MethodInfo_var);
		((Message_t2619578343_StaticFields*)Message_t2619578343_il2cpp_TypeInfo_var->static_fields)->set_U3CU3Ef__amU24cache6_8(L_20);
		G_B2_0 = G_B1_0;
		G_B2_1 = G_B1_1;
	}

IL_00a1:
	{
		Func_3_t3953209855 * L_21 = ((Message_t2619578343_StaticFields*)Message_t2619578343_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__amU24cache6_8();
		IL2CPP_RUNTIME_CLASS_INIT(Observable_t258071701_il2cpp_TypeInfo_var);
		Il2CppObject* L_22 = Observable_CombineLatest_TisBoolean_t211005341_TisInt32_t2847414787_TisU3CU3E__AnonType3_2_t3095608592_m3944907965(NULL /*static, unused*/, G_B2_1, G_B2_0, L_21, /*hidden argument*/Observable_CombineLatest_TisBoolean_t211005341_TisInt32_t2847414787_TisU3CU3E__AnonType3_2_t3095608592_m3944907965_MethodInfo_var);
		U3CPostInjectU3Ec__AnonStorey58_t980490265 * L_23 = V_0;
		IntPtr_t L_24;
		L_24.set_m_value_0((void*)(void*)U3CPostInjectU3Ec__AnonStorey58_U3CU3Em__3F_m1207743275_MethodInfo_var);
		Action_1_t3244061297 * L_25 = (Action_1_t3244061297 *)il2cpp_codegen_object_new(Action_1_t3244061297_il2cpp_TypeInfo_var);
		Action_1__ctor_m2875089364(L_25, L_23, L_24, /*hidden argument*/Action_1__ctor_m2875089364_MethodInfo_var);
		ObservableExtensions_Subscribe_TisU3CU3E__AnonType3_2_t3095608592_m3781023631(NULL /*static, unused*/, L_22, L_25, /*hidden argument*/ObservableExtensions_Subscribe_TisU3CU3E__AnonType3_2_t3095608592_m3781023631_MethodInfo_var);
		return;
	}
}
// System.Void Message::SetAnchoredPositionX(UnityEngine.RectTransform,System.Int32)
extern "C"  void Message_SetAnchoredPositionX_m998189295 (Il2CppObject * __this /* static, unused */, RectTransform_t3317474837 * ___iconRectTransform0, int32_t ___x1, const MethodInfo* method)
{
	Vector2_t3525329788  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		RectTransform_t3317474837 * L_0 = ___iconRectTransform0;
		NullCheck(L_0);
		Vector2_t3525329788  L_1 = RectTransform_get_anchoredPosition_m2546422825(L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		int32_t L_2 = ___x1;
		(&V_0)->set_x_1((((float)((float)L_2))));
		RectTransform_t3317474837 * L_3 = ___iconRectTransform0;
		Vector2_t3525329788  L_4 = V_0;
		NullCheck(L_3);
		RectTransform_set_anchoredPosition_m1783392546(L_3, L_4, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Message::ShowErrorMessage()
extern Il2CppCodeGenString* _stringLiteral2352638387;
extern const uint32_t Message_ShowErrorMessage_m1857742940_MetadataUsageId;
extern "C"  void Message_ShowErrorMessage_m1857742940 (Message_t2619578343 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Message_ShowErrorMessage_m1857742940_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = LocalizationManager_GetString_m738567075(NULL /*static, unused*/, _stringLiteral2352638387, /*hidden argument*/NULL);
		Message_Show_m3165151493(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Message::ShowUpgrade(System.String,System.String)
extern Il2CppClass* CharU5BU5D_t3416858730_il2cpp_TypeInfo_var;
extern Il2CppClass* Message_t2619578343_il2cpp_TypeInfo_var;
extern Il2CppClass* Dictionary_2_t190145395_il2cpp_TypeInfo_var;
extern Il2CppClass* Int32_t2847414787_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Utils_t82059409_il2cpp_TypeInfo_var;
extern const MethodInfo* Dictionary_2__ctor_m1958628151_MethodInfo_var;
extern const MethodInfo* Resources_Load_TisSprite_t4006040370_m3887230130_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral881000859;
extern Il2CppCodeGenString* _stringLiteral3522445;
extern Il2CppCodeGenString* _stringLiteral3023879;
extern Il2CppCodeGenString* _stringLiteral3502927655;
extern Il2CppCodeGenString* _stringLiteral1265041466;
extern Il2CppCodeGenString* _stringLiteral1131698214;
extern Il2CppCodeGenString* _stringLiteral422031741;
extern Il2CppCodeGenString* _stringLiteral517674019;
extern Il2CppCodeGenString* _stringLiteral10;
extern const uint32_t Message_ShowUpgrade_m380028605_MetadataUsageId;
extern "C"  void Message_ShowUpgrade_m380028605 (Message_t2619578343 * __this, String_t* ___lasUpgrade0, String_t* ___upgradeText1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Message_ShowUpgrade_m380028605_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	String_t* V_0 = NULL;
	StringU5BU5D_t2956870243* V_1 = NULL;
	int32_t V_2 = 0;
	Sprite_t4006040370 * V_3 = NULL;
	String_t* V_4 = NULL;
	Dictionary_2_t190145395 * V_5 = NULL;
	int32_t V_6 = 0;
	{
		V_0 = _stringLiteral881000859;
		String_t* L_0 = ___lasUpgrade0;
		CharU5BU5D_t3416858730* L_1 = ((CharU5BU5D_t3416858730*)SZArrayNew(CharU5BU5D_t3416858730_il2cpp_TypeInfo_var, (uint32_t)1));
		NullCheck(L_1);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_1, 0);
		(L_1)->SetAt(static_cast<il2cpp_array_size_t>(0), (uint16_t)((int32_t)95));
		NullCheck(L_0);
		StringU5BU5D_t2956870243* L_2 = String_Split_m290179486(L_0, L_1, /*hidden argument*/NULL);
		V_1 = L_2;
		StringU5BU5D_t2956870243* L_3 = V_1;
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, 1);
		int32_t L_4 = 1;
		int32_t L_5 = Int32_Parse_m3837759498(NULL /*static, unused*/, ((L_3)->GetAt(static_cast<il2cpp_array_size_t>(L_4))), /*hidden argument*/NULL);
		V_2 = L_5;
		StringU5BU5D_t2956870243* L_6 = V_1;
		NullCheck(L_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_6, 0);
		int32_t L_7 = 0;
		V_4 = ((L_6)->GetAt(static_cast<il2cpp_array_size_t>(L_7)));
		String_t* L_8 = V_4;
		if (!L_8)
		{
			goto IL_00dd;
		}
	}
	{
		Dictionary_2_t190145395 * L_9 = ((Message_t2619578343_StaticFields*)Message_t2619578343_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__switchU24map3_9();
		if (L_9)
		{
			goto IL_006d;
		}
	}
	{
		Dictionary_2_t190145395 * L_10 = (Dictionary_2_t190145395 *)il2cpp_codegen_object_new(Dictionary_2_t190145395_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m1958628151(L_10, 3, /*hidden argument*/Dictionary_2__ctor_m1958628151_MethodInfo_var);
		V_5 = L_10;
		Dictionary_2_t190145395 * L_11 = V_5;
		NullCheck(L_11);
		VirtActionInvoker2< String_t*, int32_t >::Invoke(18 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::Add(!0,!1) */, L_11, _stringLiteral3522445, 0);
		Dictionary_2_t190145395 * L_12 = V_5;
		NullCheck(L_12);
		VirtActionInvoker2< String_t*, int32_t >::Invoke(18 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::Add(!0,!1) */, L_12, _stringLiteral3023879, 1);
		Dictionary_2_t190145395 * L_13 = V_5;
		NullCheck(L_13);
		VirtActionInvoker2< String_t*, int32_t >::Invoke(18 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::Add(!0,!1) */, L_13, _stringLiteral3502927655, 2);
		Dictionary_2_t190145395 * L_14 = V_5;
		((Message_t2619578343_StaticFields*)Message_t2619578343_il2cpp_TypeInfo_var->static_fields)->set_U3CU3Ef__switchU24map3_9(L_14);
	}

IL_006d:
	{
		Dictionary_2_t190145395 * L_15 = ((Message_t2619578343_StaticFields*)Message_t2619578343_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__switchU24map3_9();
		String_t* L_16 = V_4;
		NullCheck(L_15);
		bool L_17 = VirtFuncInvoker2< bool, String_t*, int32_t* >::Invoke(21 /* System.Boolean System.Collections.Generic.Dictionary`2<System.String,System.Int32>::TryGetValue(!0,!1&) */, L_15, L_16, (&V_6));
		if (!L_17)
		{
			goto IL_00dd;
		}
	}
	{
		int32_t L_18 = V_6;
		if (L_18 == 0)
		{
			goto IL_0098;
		}
		if (L_18 == 1)
		{
			goto IL_00af;
		}
		if (L_18 == 2)
		{
			goto IL_00c6;
		}
	}
	{
		goto IL_00dd;
	}

IL_0098:
	{
		String_t* L_19 = V_0;
		int32_t L_20 = V_2;
		int32_t L_21 = L_20;
		Il2CppObject * L_22 = Box(Int32_t2847414787_il2cpp_TypeInfo_var, &L_21);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_23 = String_Concat_m2809334143(NULL /*static, unused*/, L_19, _stringLiteral1265041466, L_22, /*hidden argument*/NULL);
		V_0 = L_23;
		goto IL_00dd;
	}

IL_00af:
	{
		String_t* L_24 = V_0;
		int32_t L_25 = V_2;
		int32_t L_26 = L_25;
		Il2CppObject * L_27 = Box(Int32_t2847414787_il2cpp_TypeInfo_var, &L_26);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_28 = String_Concat_m2809334143(NULL /*static, unused*/, L_24, _stringLiteral1131698214, L_27, /*hidden argument*/NULL);
		V_0 = L_28;
		goto IL_00dd;
	}

IL_00c6:
	{
		String_t* L_29 = V_0;
		int32_t L_30 = V_2;
		int32_t L_31 = L_30;
		Il2CppObject * L_32 = Box(Int32_t2847414787_il2cpp_TypeInfo_var, &L_31);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_33 = String_Concat_m2809334143(NULL /*static, unused*/, L_29, _stringLiteral422031741, L_32, /*hidden argument*/NULL);
		V_0 = L_33;
		goto IL_00dd;
	}

IL_00dd:
	{
		String_t* L_34 = V_0;
		Sprite_t4006040370 * L_35 = Resources_Load_TisSprite_t4006040370_m3887230130(NULL /*static, unused*/, L_34, /*hidden argument*/Resources_Load_TisSprite_t4006040370_m3887230130_MethodInfo_var);
		V_3 = L_35;
		String_t* L_36 = LocalizationManager_GetString_m738567075(NULL /*static, unused*/, _stringLiteral517674019, /*hidden argument*/NULL);
		String_t* L_37 = ___upgradeText1;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_38 = String_Concat_m1825781833(NULL /*static, unused*/, L_36, _stringLiteral10, L_37, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Utils_t82059409_il2cpp_TypeInfo_var);
		String_t* L_39 = Utils_FormatString_m1390523396(NULL /*static, unused*/, L_38, ((int32_t)20), /*hidden argument*/NULL);
		Sprite_t4006040370 * L_40 = V_3;
		Message_Show_m452428517(__this, L_39, L_40, /*hidden argument*/NULL);
		return;
	}
}
// <>__AnonType3`2<System.Boolean,System.Int32> Message::<PostInject>m__3E(System.Boolean,System.Int32)
extern Il2CppClass* U3CU3E__AnonType3_2_t3095608592_il2cpp_TypeInfo_var;
extern const MethodInfo* U3CU3E__AnonType3_2__ctor_m4134975535_MethodInfo_var;
extern const uint32_t Message_U3CPostInjectU3Em__3E_m3718947806_MetadataUsageId;
extern "C"  U3CU3E__AnonType3_2_t3095608592 * Message_U3CPostInjectU3Em__3E_m3718947806 (Il2CppObject * __this /* static, unused */, bool ___b0, int32_t ___i1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Message_U3CPostInjectU3Em__3E_m3718947806_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = ___b0;
		int32_t L_1 = ___i1;
		U3CU3E__AnonType3_2_t3095608592 * L_2 = (U3CU3E__AnonType3_2_t3095608592 *)il2cpp_codegen_object_new(U3CU3E__AnonType3_2_t3095608592_il2cpp_TypeInfo_var);
		U3CU3E__AnonType3_2__ctor_m4134975535(L_2, L_0, L_1, /*hidden argument*/U3CU3E__AnonType3_2__ctor_m4134975535_MethodInfo_var);
		return L_2;
	}
}
// System.Void Message/<PostInject>c__AnonStorey58::.ctor()
extern "C"  void U3CPostInjectU3Ec__AnonStorey58__ctor_m2947229850 (U3CPostInjectU3Ec__AnonStorey58_t980490265 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Message/<PostInject>c__AnonStorey58::<>m__3D(System.Boolean)
extern Il2CppClass* TweenCallback_t3786476454_il2cpp_TypeInfo_var;
extern const MethodInfo* U3CPostInjectU3Ec__AnonStorey58_U3CU3Em__40_m4212567135_MethodInfo_var;
extern const MethodInfo* TweenSettingsExtensions_OnComplete_TisTweener_t1766303790_m3265777438_MethodInfo_var;
extern const uint32_t U3CPostInjectU3Ec__AnonStorey58_U3CU3Em__3D_m729826699_MetadataUsageId;
extern "C"  void U3CPostInjectU3Ec__AnonStorey58_U3CU3Em__3D_m729826699 (U3CPostInjectU3Ec__AnonStorey58_t980490265 * __this, bool ___b0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CPostInjectU3Ec__AnonStorey58_U3CU3Em__3D_m729826699_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Message_t2619578343 * L_0 = __this->get_U3CU3Ef__this_2();
		NullCheck(L_0);
		Image_t3354615620 * L_1 = L_0->get_Back_2();
		ShortcutExtensions_DOKill_m2275247369(NULL /*static, unused*/, L_1, (bool)0, /*hidden argument*/NULL);
		bool L_2 = ___b0;
		if (!L_2)
		{
			goto IL_005a;
		}
	}
	{
		Message_t2619578343 * L_3 = __this->get_U3CU3Ef__this_2();
		NullCheck(L_3);
		GameObject_t4012695102 * L_4 = Component_get_gameObject_m2112202034(L_3, /*hidden argument*/NULL);
		NullCheck(L_4);
		GameObject_SetActive_m3538205401(L_4, (bool)1, /*hidden argument*/NULL);
		Message_t2619578343 * L_5 = __this->get_U3CU3Ef__this_2();
		NullCheck(L_5);
		Image_t3354615620 * L_6 = L_5->get_Back_2();
		NullCheck(L_6);
		Behaviour_set_enabled_m1432835224(L_6, (bool)1, /*hidden argument*/NULL);
		Message_t2619578343 * L_7 = __this->get_U3CU3Ef__this_2();
		NullCheck(L_7);
		Image_t3354615620 * L_8 = L_7->get_Back_2();
		ShortcutExtensions46_DOFade_m3953805469(NULL /*static, unused*/, L_8, (0.5f), (0.3f), /*hidden argument*/NULL);
		goto IL_0086;
	}

IL_005a:
	{
		Message_t2619578343 * L_9 = __this->get_U3CU3Ef__this_2();
		NullCheck(L_9);
		Image_t3354615620 * L_10 = L_9->get_Back_2();
		Tweener_t1766303790 * L_11 = ShortcutExtensions46_DOFade_m3953805469(NULL /*static, unused*/, L_10, (0.0f), (0.3f), /*hidden argument*/NULL);
		IntPtr_t L_12;
		L_12.set_m_value_0((void*)(void*)U3CPostInjectU3Ec__AnonStorey58_U3CU3Em__40_m4212567135_MethodInfo_var);
		TweenCallback_t3786476454 * L_13 = (TweenCallback_t3786476454 *)il2cpp_codegen_object_new(TweenCallback_t3786476454_il2cpp_TypeInfo_var);
		TweenCallback__ctor_m3440504173(L_13, __this, L_12, /*hidden argument*/NULL);
		TweenSettingsExtensions_OnComplete_TisTweener_t1766303790_m3265777438(NULL /*static, unused*/, L_11, L_13, /*hidden argument*/TweenSettingsExtensions_OnComplete_TisTweener_t1766303790_m3265777438_MethodInfo_var);
	}

IL_0086:
	{
		return;
	}
}
// System.Void Message/<PostInject>c__AnonStorey58::<>m__3F(<>__AnonType3`2<System.Boolean,System.Int32>)
extern Il2CppClass* TweenCallback_t3786476454_il2cpp_TypeInfo_var;
extern const MethodInfo* U3CU3E__AnonType3_2_get_b_m3615758246_MethodInfo_var;
extern const MethodInfo* U3CU3E__AnonType3_2_get_i_m1283246900_MethodInfo_var;
extern const MethodInfo* Tuple_2_get_Item1_m706235840_MethodInfo_var;
extern const MethodInfo* Tuple_2_get_Item2_m3193356576_MethodInfo_var;
extern const MethodInfo* TweenSettingsExtensions_SetDelay_TisTweener_t1766303790_m3981276516_MethodInfo_var;
extern const MethodInfo* U3CPostInjectU3Ec__AnonStorey58_U3CU3Em__41_m4212568096_MethodInfo_var;
extern const MethodInfo* TweenSettingsExtensions_OnComplete_TisTweener_t1766303790_m3265777438_MethodInfo_var;
extern const uint32_t U3CPostInjectU3Ec__AnonStorey58_U3CU3Em__3F_m1207743275_MetadataUsageId;
extern "C"  void U3CPostInjectU3Ec__AnonStorey58_U3CU3Em__3F_m1207743275 (U3CPostInjectU3Ec__AnonStorey58_t980490265 * __this, U3CU3E__AnonType3_2_t3095608592 * ___obj0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CPostInjectU3Ec__AnonStorey58_U3CU3Em__3F_m1207743275_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Tuple_2_t2188574943  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		U3CU3E__AnonType3_2_t3095608592 * L_0 = ___obj0;
		NullCheck(L_0);
		bool L_1 = U3CU3E__AnonType3_2_get_b_m3615758246(L_0, /*hidden argument*/U3CU3E__AnonType3_2_get_b_m3615758246_MethodInfo_var);
		if (L_1)
		{
			goto IL_0151;
		}
	}
	{
		U3CU3E__AnonType3_2_t3095608592 * L_2 = ___obj0;
		NullCheck(L_2);
		int32_t L_3 = U3CU3E__AnonType3_2_get_i_m1283246900(L_2, /*hidden argument*/U3CU3E__AnonType3_2_get_i_m1283246900_MethodInfo_var);
		if ((((int32_t)L_3) <= ((int32_t)0)))
		{
			goto IL_0151;
		}
	}
	{
		Message_t2619578343 * L_4 = __this->get_U3CU3Ef__this_2();
		NullCheck(L_4);
		BoolReactiveProperty_t3047538250 * L_5 = L_4->get_ShowMessage_7();
		NullCheck(L_5);
		VirtActionInvoker1< bool >::Invoke(7 /* System.Void UniRx.ReactiveProperty`1<System.Boolean>::set_Value(!0) */, L_5, (bool)1);
		Message_t2619578343 * L_6 = __this->get_U3CU3Ef__this_2();
		NullCheck(L_6);
		ReactiveCollection_1_t3392720430 * L_7 = L_6->get__messages_6();
		NullCheck(L_7);
		Tuple_2_t2188574943  L_8 = VirtFuncInvoker1< Tuple_2_t2188574943 , int32_t >::Invoke(31 /* !0 System.Collections.ObjectModel.Collection`1<UniRx.Tuple`2<System.String,UnityEngine.Sprite>>::get_Item(System.Int32) */, L_7, 0);
		V_0 = L_8;
		Message_t2619578343 * L_9 = __this->get_U3CU3Ef__this_2();
		NullCheck(L_9);
		ReactiveCollection_1_t3392720430 * L_10 = L_9->get__messages_6();
		NullCheck(L_10);
		VirtActionInvoker1< int32_t >::Invoke(30 /* System.Void System.Collections.ObjectModel.Collection`1<UniRx.Tuple`2<System.String,UnityEngine.Sprite>>::RemoveAt(System.Int32) */, L_10, 0);
		Message_t2619578343 * L_11 = __this->get_U3CU3Ef__this_2();
		NullCheck(L_11);
		Text_t3286458198 * L_12 = L_11->get_IconText_5();
		String_t* L_13 = Tuple_2_get_Item1_m706235840((&V_0), /*hidden argument*/Tuple_2_get_Item1_m706235840_MethodInfo_var);
		NullCheck(L_12);
		VirtActionInvoker1< String_t* >::Invoke(65 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_12, L_13);
		Message_t2619578343 * L_14 = __this->get_U3CU3Ef__this_2();
		NullCheck(L_14);
		Image_t3354615620 * L_15 = L_14->get_Icon_3();
		Sprite_t4006040370 * L_16 = Tuple_2_get_Item2_m3193356576((&V_0), /*hidden argument*/Tuple_2_get_Item2_m3193356576_MethodInfo_var);
		bool L_17 = Object_op_Inequality_m1834679180(NULL /*static, unused*/, L_16, (Object_t3878351788 *)NULL, /*hidden argument*/NULL);
		NullCheck(L_15);
		Behaviour_set_enabled_m1432835224(L_15, L_17, /*hidden argument*/NULL);
		Message_t2619578343 * L_18 = __this->get_U3CU3Ef__this_2();
		NullCheck(L_18);
		Image_t3354615620 * L_19 = L_18->get_Icon_3();
		Sprite_t4006040370 * L_20 = Tuple_2_get_Item2_m3193356576((&V_0), /*hidden argument*/Tuple_2_get_Item2_m3193356576_MethodInfo_var);
		NullCheck(L_19);
		Image_set_sprite_m572551402(L_19, L_20, /*hidden argument*/NULL);
		RectTransform_t3317474837 * L_21 = __this->get_iconRectTransform_0();
		ShortcutExtensions_DOKill_m2275247369(NULL /*static, unused*/, L_21, (bool)0, /*hidden argument*/NULL);
		RectTransform_t3317474837 * L_22 = __this->get_textRectTransform_1();
		ShortcutExtensions_DOKill_m2275247369(NULL /*static, unused*/, L_22, (bool)0, /*hidden argument*/NULL);
		RectTransform_t3317474837 * L_23 = __this->get_iconRectTransform_0();
		Message_SetAnchoredPositionX_m998189295(NULL /*static, unused*/, L_23, ((int32_t)519), /*hidden argument*/NULL);
		RectTransform_t3317474837 * L_24 = __this->get_textRectTransform_1();
		Message_SetAnchoredPositionX_m998189295(NULL /*static, unused*/, L_24, ((int32_t)519), /*hidden argument*/NULL);
		RectTransform_t3317474837 * L_25 = __this->get_iconRectTransform_0();
		ShortcutExtensions46_DOAnchorPosX_m1321998970(NULL /*static, unused*/, L_25, (0.0f), (0.4f), (bool)0, /*hidden argument*/NULL);
		RectTransform_t3317474837 * L_26 = __this->get_iconRectTransform_0();
		Tweener_t1766303790 * L_27 = ShortcutExtensions46_DOAnchorPosX_m1321998970(NULL /*static, unused*/, L_26, (-519.0f), (0.4f), (bool)0, /*hidden argument*/NULL);
		TweenSettingsExtensions_SetDelay_TisTweener_t1766303790_m3981276516(NULL /*static, unused*/, L_27, (1.4f), /*hidden argument*/TweenSettingsExtensions_SetDelay_TisTweener_t1766303790_m3981276516_MethodInfo_var);
		RectTransform_t3317474837 * L_28 = __this->get_textRectTransform_1();
		ShortcutExtensions46_DOAnchorPosX_m1321998970(NULL /*static, unused*/, L_28, (0.0f), (0.4f), (bool)0, /*hidden argument*/NULL);
		RectTransform_t3317474837 * L_29 = __this->get_textRectTransform_1();
		Tweener_t1766303790 * L_30 = ShortcutExtensions46_DOAnchorPosX_m1321998970(NULL /*static, unused*/, L_29, (-519.0f), (0.4f), (bool)0, /*hidden argument*/NULL);
		Tweener_t1766303790 * L_31 = TweenSettingsExtensions_SetDelay_TisTweener_t1766303790_m3981276516(NULL /*static, unused*/, L_30, (1.4f), /*hidden argument*/TweenSettingsExtensions_SetDelay_TisTweener_t1766303790_m3981276516_MethodInfo_var);
		IntPtr_t L_32;
		L_32.set_m_value_0((void*)(void*)U3CPostInjectU3Ec__AnonStorey58_U3CU3Em__41_m4212568096_MethodInfo_var);
		TweenCallback_t3786476454 * L_33 = (TweenCallback_t3786476454 *)il2cpp_codegen_object_new(TweenCallback_t3786476454_il2cpp_TypeInfo_var);
		TweenCallback__ctor_m3440504173(L_33, __this, L_32, /*hidden argument*/NULL);
		TweenSettingsExtensions_OnComplete_TisTweener_t1766303790_m3265777438(NULL /*static, unused*/, L_31, L_33, /*hidden argument*/TweenSettingsExtensions_OnComplete_TisTweener_t1766303790_m3265777438_MethodInfo_var);
	}

IL_0151:
	{
		return;
	}
}
// System.Void Message/<PostInject>c__AnonStorey58::<>m__40()
extern "C"  void U3CPostInjectU3Ec__AnonStorey58_U3CU3Em__40_m4212567135 (U3CPostInjectU3Ec__AnonStorey58_t980490265 * __this, const MethodInfo* method)
{
	{
		Message_t2619578343 * L_0 = __this->get_U3CU3Ef__this_2();
		NullCheck(L_0);
		Image_t3354615620 * L_1 = L_0->get_Back_2();
		NullCheck(L_1);
		Behaviour_set_enabled_m1432835224(L_1, (bool)0, /*hidden argument*/NULL);
		Message_t2619578343 * L_2 = __this->get_U3CU3Ef__this_2();
		NullCheck(L_2);
		GameObject_t4012695102 * L_3 = Component_get_gameObject_m2112202034(L_2, /*hidden argument*/NULL);
		NullCheck(L_3);
		GameObject_SetActive_m3538205401(L_3, (bool)0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Message/<PostInject>c__AnonStorey58::<>m__41()
extern "C"  void U3CPostInjectU3Ec__AnonStorey58_U3CU3Em__41_m4212568096 (U3CPostInjectU3Ec__AnonStorey58_t980490265 * __this, const MethodInfo* method)
{
	{
		Message_t2619578343 * L_0 = __this->get_U3CU3Ef__this_2();
		NullCheck(L_0);
		BoolReactiveProperty_t3047538250 * L_1 = L_0->get_ShowMessage_7();
		NullCheck(L_1);
		VirtActionInvoker1< bool >::Invoke(7 /* System.Void UniRx.ReactiveProperty`1<System.Boolean>::set_Value(!0) */, L_1, (bool)0);
		return;
	}
}
// System.Void MetaProcessBase::.ctor()
extern Il2CppClass* List_1_t528229087_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1__ctor_m2428883473_MethodInfo_var;
extern const MethodInfo* List_1__ctor_m4191942114_MethodInfo_var;
extern const uint32_t MetaProcessBase__ctor_m2192528208_MetadataUsageId;
extern "C"  void MetaProcessBase__ctor_m2192528208 (MetaProcessBase_t2426506875 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (MetaProcessBase__ctor_m2192528208_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		List_1_t528229087 * L_0 = (List_1_t528229087 *)il2cpp_codegen_object_new(List_1_t528229087_il2cpp_TypeInfo_var);
		List_1__ctor_m2428883473(L_0, /*hidden argument*/List_1__ctor_m2428883473_MethodInfo_var);
		__this->set_children_0(L_0);
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		List_1_t528229087 * L_1 = (List_1_t528229087 *)il2cpp_codegen_object_new(List_1_t528229087_il2cpp_TypeInfo_var);
		List_1__ctor_m4191942114(L_1, 2, /*hidden argument*/List_1__ctor_m4191942114_MethodInfo_var);
		__this->set_children_0(L_1);
		return;
	}
}
// System.Void MetaProcessBase::.ctor(System.Collections.Generic.IEnumerable`1<IProcess>)
extern Il2CppClass* List_1_t528229087_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1__ctor_m2428883473_MethodInfo_var;
extern const MethodInfo* Enumerable_ToList_TisIProcess_t4026237414_m2912069109_MethodInfo_var;
extern const uint32_t MetaProcessBase__ctor_m3869388257_MetadataUsageId;
extern "C"  void MetaProcessBase__ctor_m3869388257 (MetaProcessBase_t2426506875 * __this, Il2CppObject* ___coroutines0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (MetaProcessBase__ctor_m3869388257_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		List_1_t528229087 * L_0 = (List_1_t528229087 *)il2cpp_codegen_object_new(List_1_t528229087_il2cpp_TypeInfo_var);
		List_1__ctor_m2428883473(L_0, /*hidden argument*/List_1__ctor_m2428883473_MethodInfo_var);
		__this->set_children_0(L_0);
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		Il2CppObject* L_1 = ___coroutines0;
		List_1_t528229087 * L_2 = Enumerable_ToList_TisIProcess_t4026237414_m2912069109(NULL /*static, unused*/, L_1, /*hidden argument*/Enumerable_ToList_TisIProcess_t4026237414_m2912069109_MethodInfo_var);
		__this->set_children_0(L_2);
		return;
	}
}
// System.Void MetaProcessBase::Add(IProcess)
extern "C"  void MetaProcessBase_Add_m2990930153 (MetaProcessBase_t2426506875 * __this, Il2CppObject * ___proc0, const MethodInfo* method)
{
	{
		List_1_t528229087 * L_0 = __this->get_children_0();
		Il2CppObject * L_1 = ___proc0;
		NullCheck(L_0);
		VirtActionInvoker1< Il2CppObject * >::Invoke(22 /* System.Void System.Collections.Generic.List`1<IProcess>::Add(!0) */, L_0, L_1);
		return;
	}
}
// System.Boolean MetaProcessBase::get_finished()
extern Il2CppClass* MetaProcessBase_t2426506875_il2cpp_TypeInfo_var;
extern Il2CppClass* Func_2_t3494101351_il2cpp_TypeInfo_var;
extern const MethodInfo* MetaProcessBase_U3Cget_finishedU3Em__1E0_m485819824_MethodInfo_var;
extern const MethodInfo* Func_2__ctor_m3957380477_MethodInfo_var;
extern const MethodInfo* Enumerable_All_TisIProcess_t4026237414_m2691282026_MethodInfo_var;
extern const uint32_t MetaProcessBase_get_finished_m3307854875_MetadataUsageId;
extern "C"  bool MetaProcessBase_get_finished_m3307854875 (MetaProcessBase_t2426506875 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (MetaProcessBase_get_finished_m3307854875_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	List_1_t528229087 * G_B2_0 = NULL;
	List_1_t528229087 * G_B1_0 = NULL;
	{
		List_1_t528229087 * L_0 = __this->get_children_0();
		Func_2_t3494101351 * L_1 = ((MetaProcessBase_t2426506875_StaticFields*)MetaProcessBase_t2426506875_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__amU24cache1_1();
		G_B1_0 = L_0;
		if (L_1)
		{
			G_B2_0 = L_0;
			goto IL_001e;
		}
	}
	{
		IntPtr_t L_2;
		L_2.set_m_value_0((void*)(void*)MetaProcessBase_U3Cget_finishedU3Em__1E0_m485819824_MethodInfo_var);
		Func_2_t3494101351 * L_3 = (Func_2_t3494101351 *)il2cpp_codegen_object_new(Func_2_t3494101351_il2cpp_TypeInfo_var);
		Func_2__ctor_m3957380477(L_3, NULL, L_2, /*hidden argument*/Func_2__ctor_m3957380477_MethodInfo_var);
		((MetaProcessBase_t2426506875_StaticFields*)MetaProcessBase_t2426506875_il2cpp_TypeInfo_var->static_fields)->set_U3CU3Ef__amU24cache1_1(L_3);
		G_B2_0 = G_B1_0;
	}

IL_001e:
	{
		Func_2_t3494101351 * L_4 = ((MetaProcessBase_t2426506875_StaticFields*)MetaProcessBase_t2426506875_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__amU24cache1_1();
		bool L_5 = Enumerable_All_TisIProcess_t4026237414_m2691282026(NULL /*static, unused*/, G_B2_0, L_4, /*hidden argument*/Enumerable_All_TisIProcess_t4026237414_m2691282026_MethodInfo_var);
		return L_5;
	}
}
// System.Void MetaProcessBase::Tick(System.Single)
extern Il2CppClass* U3CTickU3Ec__AnonStorey146_t2344266993_il2cpp_TypeInfo_var;
extern Il2CppClass* Action_1_t4174690119_il2cpp_TypeInfo_var;
extern const MethodInfo* U3CTickU3Ec__AnonStorey146_U3CU3Em__1E1_m3186399834_MethodInfo_var;
extern const MethodInfo* Action_1__ctor_m1965293337_MethodInfo_var;
extern const MethodInfo* List_1_ForEach_m4211563328_MethodInfo_var;
extern const uint32_t MetaProcessBase_Tick_m2990243578_MetadataUsageId;
extern "C"  void MetaProcessBase_Tick_m2990243578 (MetaProcessBase_t2426506875 * __this, float ___dt0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (MetaProcessBase_Tick_m2990243578_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	U3CTickU3Ec__AnonStorey146_t2344266993 * V_0 = NULL;
	{
		U3CTickU3Ec__AnonStorey146_t2344266993 * L_0 = (U3CTickU3Ec__AnonStorey146_t2344266993 *)il2cpp_codegen_object_new(U3CTickU3Ec__AnonStorey146_t2344266993_il2cpp_TypeInfo_var);
		U3CTickU3Ec__AnonStorey146__ctor_m1395127094(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CTickU3Ec__AnonStorey146_t2344266993 * L_1 = V_0;
		float L_2 = ___dt0;
		NullCheck(L_1);
		L_1->set_dt_0(L_2);
		List_1_t528229087 * L_3 = __this->get_children_0();
		U3CTickU3Ec__AnonStorey146_t2344266993 * L_4 = V_0;
		IntPtr_t L_5;
		L_5.set_m_value_0((void*)(void*)U3CTickU3Ec__AnonStorey146_U3CU3Em__1E1_m3186399834_MethodInfo_var);
		Action_1_t4174690119 * L_6 = (Action_1_t4174690119 *)il2cpp_codegen_object_new(Action_1_t4174690119_il2cpp_TypeInfo_var);
		Action_1__ctor_m1965293337(L_6, L_4, L_5, /*hidden argument*/Action_1__ctor_m1965293337_MethodInfo_var);
		NullCheck(L_3);
		List_1_ForEach_m4211563328(L_3, L_6, /*hidden argument*/List_1_ForEach_m4211563328_MethodInfo_var);
		return;
	}
}
// System.Void MetaProcessBase::Dispose()
extern Il2CppClass* MetaProcessBase_t2426506875_il2cpp_TypeInfo_var;
extern Il2CppClass* Action_1_t4174690119_il2cpp_TypeInfo_var;
extern const MethodInfo* MetaProcessBase_U3CDisposeU3Em__1E2_m3847224724_MethodInfo_var;
extern const MethodInfo* Action_1__ctor_m1965293337_MethodInfo_var;
extern const MethodInfo* List_1_ForEach_m4211563328_MethodInfo_var;
extern const uint32_t MetaProcessBase_Dispose_m2383915917_MetadataUsageId;
extern "C"  void MetaProcessBase_Dispose_m2383915917 (MetaProcessBase_t2426506875 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (MetaProcessBase_Dispose_m2383915917_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	List_1_t528229087 * G_B2_0 = NULL;
	List_1_t528229087 * G_B1_0 = NULL;
	{
		List_1_t528229087 * L_0 = __this->get_children_0();
		Action_1_t4174690119 * L_1 = ((MetaProcessBase_t2426506875_StaticFields*)MetaProcessBase_t2426506875_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__amU24cache2_2();
		G_B1_0 = L_0;
		if (L_1)
		{
			G_B2_0 = L_0;
			goto IL_001e;
		}
	}
	{
		IntPtr_t L_2;
		L_2.set_m_value_0((void*)(void*)MetaProcessBase_U3CDisposeU3Em__1E2_m3847224724_MethodInfo_var);
		Action_1_t4174690119 * L_3 = (Action_1_t4174690119 *)il2cpp_codegen_object_new(Action_1_t4174690119_il2cpp_TypeInfo_var);
		Action_1__ctor_m1965293337(L_3, NULL, L_2, /*hidden argument*/Action_1__ctor_m1965293337_MethodInfo_var);
		((MetaProcessBase_t2426506875_StaticFields*)MetaProcessBase_t2426506875_il2cpp_TypeInfo_var->static_fields)->set_U3CU3Ef__amU24cache2_2(L_3);
		G_B2_0 = G_B1_0;
	}

IL_001e:
	{
		Action_1_t4174690119 * L_4 = ((MetaProcessBase_t2426506875_StaticFields*)MetaProcessBase_t2426506875_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__amU24cache2_2();
		NullCheck(G_B2_0);
		List_1_ForEach_m4211563328(G_B2_0, L_4, /*hidden argument*/List_1_ForEach_m4211563328_MethodInfo_var);
		return;
	}
}
// System.Collections.IEnumerator MetaProcessBase::GetEnumerator()
extern Il2CppClass* Enumerator_t2908979375_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1_GetEnumerator_m2058745630_MethodInfo_var;
extern const uint32_t MetaProcessBase_GetEnumerator_m2692735728_MetadataUsageId;
extern "C"  Il2CppObject * MetaProcessBase_GetEnumerator_m2692735728 (MetaProcessBase_t2426506875 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (MetaProcessBase_GetEnumerator_m2692735728_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		List_1_t528229087 * L_0 = __this->get_children_0();
		NullCheck(L_0);
		Enumerator_t2908979375  L_1 = List_1_GetEnumerator_m2058745630(L_0, /*hidden argument*/List_1_GetEnumerator_m2058745630_MethodInfo_var);
		Enumerator_t2908979375  L_2 = L_1;
		Il2CppObject * L_3 = Box(Enumerator_t2908979375_il2cpp_TypeInfo_var, &L_2);
		return (Il2CppObject *)L_3;
	}
}
// System.Boolean MetaProcessBase::<get_finished>m__1E0(IProcess)
extern Il2CppClass* IProcess_t4026237414_il2cpp_TypeInfo_var;
extern const uint32_t MetaProcessBase_U3Cget_finishedU3Em__1E0_m485819824_MetadataUsageId;
extern "C"  bool MetaProcessBase_U3Cget_finishedU3Em__1E0_m485819824 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___c0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (MetaProcessBase_U3Cget_finishedU3Em__1E0_m485819824_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject * L_0 = ___c0;
		NullCheck(L_0);
		bool L_1 = InterfaceFuncInvoker0< bool >::Invoke(0 /* System.Boolean IProcess::get_finished() */, IProcess_t4026237414_il2cpp_TypeInfo_var, L_0);
		return L_1;
	}
}
// System.Void MetaProcessBase::<Dispose>m__1E2(IProcess)
extern Il2CppClass* IProcess_t4026237414_il2cpp_TypeInfo_var;
extern const uint32_t MetaProcessBase_U3CDisposeU3Em__1E2_m3847224724_MetadataUsageId;
extern "C"  void MetaProcessBase_U3CDisposeU3Em__1E2_m3847224724 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___c0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (MetaProcessBase_U3CDisposeU3Em__1E2_m3847224724_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject * L_0 = ___c0;
		NullCheck(L_0);
		InterfaceActionInvoker0::Invoke(2 /* System.Void IProcess::Dispose() */, IProcess_t4026237414_il2cpp_TypeInfo_var, L_0);
		return;
	}
}
// System.Void MetaProcessBase/<Tick>c__AnonStorey146::.ctor()
extern "C"  void U3CTickU3Ec__AnonStorey146__ctor_m1395127094 (U3CTickU3Ec__AnonStorey146_t2344266993 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void MetaProcessBase/<Tick>c__AnonStorey146::<>m__1E1(IProcess)
extern Il2CppClass* IProcess_t4026237414_il2cpp_TypeInfo_var;
extern const uint32_t U3CTickU3Ec__AnonStorey146_U3CU3Em__1E1_m3186399834_MetadataUsageId;
extern "C"  void U3CTickU3Ec__AnonStorey146_U3CU3Em__1E1_m3186399834 (U3CTickU3Ec__AnonStorey146_t2344266993 * __this, Il2CppObject * ___c0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CTickU3Ec__AnonStorey146_U3CU3Em__1E1_m3186399834_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject * L_0 = ___c0;
		float L_1 = __this->get_dt_0();
		NullCheck(L_0);
		InterfaceActionInvoker1< float >::Invoke(1 /* System.Void IProcess::Tick(System.Single) */, IProcess_t4026237414_il2cpp_TypeInfo_var, L_0, L_1);
		return;
	}
}
// System.Boolean ModestTree.Assert::get_IsEnabled()
extern "C"  bool Assert_get_IsEnabled_m759776857 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		return (bool)0;
	}
}
// System.Void ModestTree.Assert::That(System.Boolean)
extern Il2CppCodeGenString* _stringLiteral2014322536;
extern const uint32_t Assert_That_m1320742921_MetadataUsageId;
extern "C"  void Assert_That_m1320742921 (Il2CppObject * __this /* static, unused */, bool ___condition0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Assert_That_m1320742921_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = ___condition0;
		if (L_0)
		{
			goto IL_0010;
		}
	}
	{
		Assert_Throw_m197124005(NULL /*static, unused*/, _stringLiteral2014322536, /*hidden argument*/NULL);
	}

IL_0010:
	{
		return;
	}
}
// System.Void ModestTree.Assert::IsEqual(System.Object,System.Object)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const uint32_t Assert_IsEqual_m3021801345_MetadataUsageId;
extern "C"  void Assert_IsEqual_m3021801345 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___left0, Il2CppObject * ___right1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Assert_IsEqual_m3021801345_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject * L_0 = ___left0;
		Il2CppObject * L_1 = ___right1;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_2 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		Assert_IsEqual_m357464573(NULL /*static, unused*/, L_0, L_1, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void ModestTree.Assert::IsEqual(System.Object,System.Object,System.Func`1<System.String>)
extern Il2CppClass* StringU5BU5D_t2956870243_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const MethodInfo* Func_1_Invoke_m3326986245_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral1792391387;
extern Il2CppCodeGenString* _stringLiteral990189687;
extern Il2CppCodeGenString* _stringLiteral4097406449;
extern Il2CppCodeGenString* _stringLiteral38937;
extern const uint32_t Assert_IsEqual_m3745255887_MetadataUsageId;
extern "C"  void Assert_IsEqual_m3745255887 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___left0, Il2CppObject * ___right1, Func_1_t2111270149 * ___messageGenerator2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Assert_IsEqual_m3745255887_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Il2CppObject * G_B3_0 = NULL;
	Il2CppObject * G_B2_0 = NULL;
	Il2CppObject * G_B5_0 = NULL;
	Il2CppObject * G_B4_0 = NULL;
	{
		Il2CppObject * L_0 = ___left0;
		Il2CppObject * L_1 = ___right1;
		bool L_2 = Object_Equals_m3175838359(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		if (L_2)
		{
			goto IL_006d;
		}
	}
	{
		Il2CppObject * L_3 = ___left0;
		Il2CppObject * L_4 = L_3;
		G_B2_0 = L_4;
		if (L_4)
		{
			G_B3_0 = L_4;
			goto IL_0019;
		}
	}
	{
		G_B3_0 = ((Il2CppObject *)(_stringLiteral1792391387));
	}

IL_0019:
	{
		___left0 = G_B3_0;
		Il2CppObject * L_5 = ___right1;
		Il2CppObject * L_6 = L_5;
		G_B4_0 = L_6;
		if (L_6)
		{
			G_B5_0 = L_6;
			goto IL_0028;
		}
	}
	{
		G_B5_0 = ((Il2CppObject *)(_stringLiteral1792391387));
	}

IL_0028:
	{
		___right1 = G_B5_0;
		StringU5BU5D_t2956870243* L_7 = ((StringU5BU5D_t2956870243*)SZArrayNew(StringU5BU5D_t2956870243_il2cpp_TypeInfo_var, (uint32_t)6));
		NullCheck(L_7);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_7, 0);
		ArrayElementTypeCheck (L_7, _stringLiteral990189687);
		(L_7)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)_stringLiteral990189687);
		StringU5BU5D_t2956870243* L_8 = L_7;
		Il2CppObject * L_9 = ___right1;
		NullCheck(L_9);
		String_t* L_10 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_9);
		NullCheck(L_8);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_8, 1);
		ArrayElementTypeCheck (L_8, L_10);
		(L_8)->SetAt(static_cast<il2cpp_array_size_t>(1), (String_t*)L_10);
		StringU5BU5D_t2956870243* L_11 = L_8;
		NullCheck(L_11);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_11, 2);
		ArrayElementTypeCheck (L_11, _stringLiteral4097406449);
		(L_11)->SetAt(static_cast<il2cpp_array_size_t>(2), (String_t*)_stringLiteral4097406449);
		StringU5BU5D_t2956870243* L_12 = L_11;
		Il2CppObject * L_13 = ___left0;
		NullCheck(L_13);
		String_t* L_14 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_13);
		NullCheck(L_12);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_12, 3);
		ArrayElementTypeCheck (L_12, L_14);
		(L_12)->SetAt(static_cast<il2cpp_array_size_t>(3), (String_t*)L_14);
		StringU5BU5D_t2956870243* L_15 = L_12;
		NullCheck(L_15);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_15, 4);
		ArrayElementTypeCheck (L_15, _stringLiteral38937);
		(L_15)->SetAt(static_cast<il2cpp_array_size_t>(4), (String_t*)_stringLiteral38937);
		StringU5BU5D_t2956870243* L_16 = L_15;
		Func_1_t2111270149 * L_17 = ___messageGenerator2;
		NullCheck(L_17);
		String_t* L_18 = Func_1_Invoke_m3326986245(L_17, /*hidden argument*/Func_1_Invoke_m3326986245_MethodInfo_var);
		NullCheck(L_16);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_16, 5);
		ArrayElementTypeCheck (L_16, L_18);
		(L_16)->SetAt(static_cast<il2cpp_array_size_t>(5), (String_t*)L_18);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_19 = String_Concat_m21867311(NULL /*static, unused*/, L_16, /*hidden argument*/NULL);
		Assert_Throw_m197124005(NULL /*static, unused*/, L_19, /*hidden argument*/NULL);
	}

IL_006d:
	{
		return;
	}
}
// System.Void ModestTree.Assert::IsEqual(System.Object,System.Object,System.String)
extern Il2CppClass* StringU5BU5D_t2956870243_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1792391387;
extern Il2CppCodeGenString* _stringLiteral990189687;
extern Il2CppCodeGenString* _stringLiteral4097406449;
extern Il2CppCodeGenString* _stringLiteral38937;
extern const uint32_t Assert_IsEqual_m357464573_MetadataUsageId;
extern "C"  void Assert_IsEqual_m357464573 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___left0, Il2CppObject * ___right1, String_t* ___message2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Assert_IsEqual_m357464573_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Il2CppObject * G_B3_0 = NULL;
	Il2CppObject * G_B2_0 = NULL;
	Il2CppObject * G_B5_0 = NULL;
	Il2CppObject * G_B4_0 = NULL;
	{
		Il2CppObject * L_0 = ___left0;
		Il2CppObject * L_1 = ___right1;
		bool L_2 = Object_Equals_m3175838359(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		if (L_2)
		{
			goto IL_0068;
		}
	}
	{
		Il2CppObject * L_3 = ___left0;
		Il2CppObject * L_4 = L_3;
		G_B2_0 = L_4;
		if (L_4)
		{
			G_B3_0 = L_4;
			goto IL_0019;
		}
	}
	{
		G_B3_0 = ((Il2CppObject *)(_stringLiteral1792391387));
	}

IL_0019:
	{
		___left0 = G_B3_0;
		Il2CppObject * L_5 = ___right1;
		Il2CppObject * L_6 = L_5;
		G_B4_0 = L_6;
		if (L_6)
		{
			G_B5_0 = L_6;
			goto IL_0028;
		}
	}
	{
		G_B5_0 = ((Il2CppObject *)(_stringLiteral1792391387));
	}

IL_0028:
	{
		___right1 = G_B5_0;
		StringU5BU5D_t2956870243* L_7 = ((StringU5BU5D_t2956870243*)SZArrayNew(StringU5BU5D_t2956870243_il2cpp_TypeInfo_var, (uint32_t)6));
		NullCheck(L_7);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_7, 0);
		ArrayElementTypeCheck (L_7, _stringLiteral990189687);
		(L_7)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)_stringLiteral990189687);
		StringU5BU5D_t2956870243* L_8 = L_7;
		Il2CppObject * L_9 = ___right1;
		NullCheck(L_9);
		String_t* L_10 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_9);
		NullCheck(L_8);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_8, 1);
		ArrayElementTypeCheck (L_8, L_10);
		(L_8)->SetAt(static_cast<il2cpp_array_size_t>(1), (String_t*)L_10);
		StringU5BU5D_t2956870243* L_11 = L_8;
		NullCheck(L_11);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_11, 2);
		ArrayElementTypeCheck (L_11, _stringLiteral4097406449);
		(L_11)->SetAt(static_cast<il2cpp_array_size_t>(2), (String_t*)_stringLiteral4097406449);
		StringU5BU5D_t2956870243* L_12 = L_11;
		Il2CppObject * L_13 = ___left0;
		NullCheck(L_13);
		String_t* L_14 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_13);
		NullCheck(L_12);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_12, 3);
		ArrayElementTypeCheck (L_12, L_14);
		(L_12)->SetAt(static_cast<il2cpp_array_size_t>(3), (String_t*)L_14);
		StringU5BU5D_t2956870243* L_15 = L_12;
		NullCheck(L_15);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_15, 4);
		ArrayElementTypeCheck (L_15, _stringLiteral38937);
		(L_15)->SetAt(static_cast<il2cpp_array_size_t>(4), (String_t*)_stringLiteral38937);
		StringU5BU5D_t2956870243* L_16 = L_15;
		String_t* L_17 = ___message2;
		NullCheck(L_16);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_16, 5);
		ArrayElementTypeCheck (L_16, L_17);
		(L_16)->SetAt(static_cast<il2cpp_array_size_t>(5), (String_t*)L_17);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_18 = String_Concat_m21867311(NULL /*static, unused*/, L_16, /*hidden argument*/NULL);
		Assert_Throw_m197124005(NULL /*static, unused*/, L_18, /*hidden argument*/NULL);
	}

IL_0068:
	{
		return;
	}
}
// System.Void ModestTree.Assert::IsNotEqual(System.Object,System.Object)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const uint32_t Assert_IsNotEqual_m1677822028_MetadataUsageId;
extern "C"  void Assert_IsNotEqual_m1677822028 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___left0, Il2CppObject * ___right1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Assert_IsNotEqual_m1677822028_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject * L_0 = ___left0;
		Il2CppObject * L_1 = ___right1;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_2 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		Assert_IsNotEqual_m475020168(NULL /*static, unused*/, L_0, L_1, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void ModestTree.Assert::IsNotEqual(System.Object,System.Object,System.Func`1<System.String>)
extern Il2CppClass* StringU5BU5D_t2956870243_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const MethodInfo* Func_1_Invoke_m3326986245_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral1792391387;
extern Il2CppCodeGenString* _stringLiteral990189687;
extern Il2CppCodeGenString* _stringLiteral2104341395;
extern Il2CppCodeGenString* _stringLiteral38937;
extern const uint32_t Assert_IsNotEqual_m1217636580_MetadataUsageId;
extern "C"  void Assert_IsNotEqual_m1217636580 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___left0, Il2CppObject * ___right1, Func_1_t2111270149 * ___messageGenerator2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Assert_IsNotEqual_m1217636580_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Il2CppObject * G_B3_0 = NULL;
	Il2CppObject * G_B2_0 = NULL;
	Il2CppObject * G_B5_0 = NULL;
	Il2CppObject * G_B4_0 = NULL;
	{
		Il2CppObject * L_0 = ___left0;
		Il2CppObject * L_1 = ___right1;
		bool L_2 = Object_Equals_m3175838359(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_006d;
		}
	}
	{
		Il2CppObject * L_3 = ___left0;
		Il2CppObject * L_4 = L_3;
		G_B2_0 = L_4;
		if (L_4)
		{
			G_B3_0 = L_4;
			goto IL_0019;
		}
	}
	{
		G_B3_0 = ((Il2CppObject *)(_stringLiteral1792391387));
	}

IL_0019:
	{
		___left0 = G_B3_0;
		Il2CppObject * L_5 = ___right1;
		Il2CppObject * L_6 = L_5;
		G_B4_0 = L_6;
		if (L_6)
		{
			G_B5_0 = L_6;
			goto IL_0028;
		}
	}
	{
		G_B5_0 = ((Il2CppObject *)(_stringLiteral1792391387));
	}

IL_0028:
	{
		___right1 = G_B5_0;
		StringU5BU5D_t2956870243* L_7 = ((StringU5BU5D_t2956870243*)SZArrayNew(StringU5BU5D_t2956870243_il2cpp_TypeInfo_var, (uint32_t)6));
		NullCheck(L_7);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_7, 0);
		ArrayElementTypeCheck (L_7, _stringLiteral990189687);
		(L_7)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)_stringLiteral990189687);
		StringU5BU5D_t2956870243* L_8 = L_7;
		Il2CppObject * L_9 = ___right1;
		NullCheck(L_9);
		String_t* L_10 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_9);
		NullCheck(L_8);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_8, 1);
		ArrayElementTypeCheck (L_8, L_10);
		(L_8)->SetAt(static_cast<il2cpp_array_size_t>(1), (String_t*)L_10);
		StringU5BU5D_t2956870243* L_11 = L_8;
		NullCheck(L_11);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_11, 2);
		ArrayElementTypeCheck (L_11, _stringLiteral2104341395);
		(L_11)->SetAt(static_cast<il2cpp_array_size_t>(2), (String_t*)_stringLiteral2104341395);
		StringU5BU5D_t2956870243* L_12 = L_11;
		Il2CppObject * L_13 = ___left0;
		NullCheck(L_13);
		String_t* L_14 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_13);
		NullCheck(L_12);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_12, 3);
		ArrayElementTypeCheck (L_12, L_14);
		(L_12)->SetAt(static_cast<il2cpp_array_size_t>(3), (String_t*)L_14);
		StringU5BU5D_t2956870243* L_15 = L_12;
		NullCheck(L_15);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_15, 4);
		ArrayElementTypeCheck (L_15, _stringLiteral38937);
		(L_15)->SetAt(static_cast<il2cpp_array_size_t>(4), (String_t*)_stringLiteral38937);
		StringU5BU5D_t2956870243* L_16 = L_15;
		Func_1_t2111270149 * L_17 = ___messageGenerator2;
		NullCheck(L_17);
		String_t* L_18 = Func_1_Invoke_m3326986245(L_17, /*hidden argument*/Func_1_Invoke_m3326986245_MethodInfo_var);
		NullCheck(L_16);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_16, 5);
		ArrayElementTypeCheck (L_16, L_18);
		(L_16)->SetAt(static_cast<il2cpp_array_size_t>(5), (String_t*)L_18);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_19 = String_Concat_m21867311(NULL /*static, unused*/, L_16, /*hidden argument*/NULL);
		Assert_Throw_m197124005(NULL /*static, unused*/, L_19, /*hidden argument*/NULL);
	}

IL_006d:
	{
		return;
	}
}
// System.Void ModestTree.Assert::IsNull(System.Object)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral661872832;
extern Il2CppCodeGenString* _stringLiteral39;
extern const uint32_t Assert_IsNull_m3103527384_MetadataUsageId;
extern "C"  void Assert_IsNull_m3103527384 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___val0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Assert_IsNull_m3103527384_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject * L_0 = ___val0;
		if (!L_0)
		{
			goto IL_0020;
		}
	}
	{
		Il2CppObject * L_1 = ___val0;
		NullCheck(L_1);
		String_t* L_2 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_1);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_3 = String_Concat_m1825781833(NULL /*static, unused*/, _stringLiteral661872832, L_2, _stringLiteral39, /*hidden argument*/NULL);
		Assert_Throw_m197124005(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
	}

IL_0020:
	{
		return;
	}
}
// System.Void ModestTree.Assert::IsNotNull(System.Object)
extern Il2CppCodeGenString* _stringLiteral3549005086;
extern const uint32_t Assert_IsNotNull_m3452793965_MetadataUsageId;
extern "C"  void Assert_IsNotNull_m3452793965 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___val0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Assert_IsNotNull_m3452793965_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject * L_0 = ___val0;
		if (L_0)
		{
			goto IL_0010;
		}
	}
	{
		Assert_Throw_m197124005(NULL /*static, unused*/, _stringLiteral3549005086, /*hidden argument*/NULL);
	}

IL_0010:
	{
		return;
	}
}
// System.Void ModestTree.Assert::IsNotNull(System.Object,System.String)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral389856080;
extern const uint32_t Assert_IsNotNull_m3793213929_MetadataUsageId;
extern "C"  void Assert_IsNotNull_m3793213929 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___val0, String_t* ___message1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Assert_IsNotNull_m3793213929_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject * L_0 = ___val0;
		if (L_0)
		{
			goto IL_0016;
		}
	}
	{
		String_t* L_1 = ___message1;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_2 = String_Concat_m138640077(NULL /*static, unused*/, _stringLiteral389856080, L_1, /*hidden argument*/NULL);
		Assert_Throw_m197124005(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
	}

IL_0016:
	{
		return;
	}
}
// System.Void ModestTree.Assert::IsNull(System.Object,System.String,System.Object[])
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral661872832;
extern Il2CppCodeGenString* _stringLiteral39309;
extern const uint32_t Assert_IsNull_m3959033472_MetadataUsageId;
extern "C"  void Assert_IsNull_m3959033472 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___val0, String_t* ___message1, ObjectU5BU5D_t11523773* ___parameters2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Assert_IsNull_m3959033472_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject * L_0 = ___val0;
		if (!L_0)
		{
			goto IL_0027;
		}
	}
	{
		Il2CppObject * L_1 = ___val0;
		NullCheck(L_1);
		String_t* L_2 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_1);
		String_t* L_3 = ___message1;
		ObjectU5BU5D_t11523773* L_4 = ___parameters2;
		String_t* L_5 = Assert_FormatString_m3297644574(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_6 = String_Concat_m2933632197(NULL /*static, unused*/, _stringLiteral661872832, L_2, _stringLiteral39309, L_5, /*hidden argument*/NULL);
		Assert_Throw_m197124005(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
	}

IL_0027:
	{
		return;
	}
}
// System.Void ModestTree.Assert::IsNotNull(System.Object,System.String,System.Object[])
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral389856080;
extern const uint32_t Assert_IsNotNull_m3308289621_MetadataUsageId;
extern "C"  void Assert_IsNotNull_m3308289621 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___val0, String_t* ___message1, ObjectU5BU5D_t11523773* ___parameters2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Assert_IsNotNull_m3308289621_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject * L_0 = ___val0;
		if (L_0)
		{
			goto IL_001c;
		}
	}
	{
		String_t* L_1 = ___message1;
		ObjectU5BU5D_t11523773* L_2 = ___parameters2;
		String_t* L_3 = Assert_FormatString_m3297644574(NULL /*static, unused*/, L_1, L_2, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_4 = String_Concat_m138640077(NULL /*static, unused*/, _stringLiteral389856080, L_3, /*hidden argument*/NULL);
		Assert_Throw_m197124005(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
	}

IL_001c:
	{
		return;
	}
}
// System.Void ModestTree.Assert::IsNotEqual(System.Object,System.Object,System.String)
extern Il2CppClass* StringU5BU5D_t2956870243_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1792391387;
extern Il2CppCodeGenString* _stringLiteral990189687;
extern Il2CppCodeGenString* _stringLiteral2104341395;
extern Il2CppCodeGenString* _stringLiteral38937;
extern const uint32_t Assert_IsNotEqual_m475020168_MetadataUsageId;
extern "C"  void Assert_IsNotEqual_m475020168 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___left0, Il2CppObject * ___right1, String_t* ___message2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Assert_IsNotEqual_m475020168_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Il2CppObject * G_B3_0 = NULL;
	Il2CppObject * G_B2_0 = NULL;
	Il2CppObject * G_B5_0 = NULL;
	Il2CppObject * G_B4_0 = NULL;
	{
		Il2CppObject * L_0 = ___left0;
		Il2CppObject * L_1 = ___right1;
		bool L_2 = Object_Equals_m3175838359(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0068;
		}
	}
	{
		Il2CppObject * L_3 = ___left0;
		Il2CppObject * L_4 = L_3;
		G_B2_0 = L_4;
		if (L_4)
		{
			G_B3_0 = L_4;
			goto IL_0019;
		}
	}
	{
		G_B3_0 = ((Il2CppObject *)(_stringLiteral1792391387));
	}

IL_0019:
	{
		___left0 = G_B3_0;
		Il2CppObject * L_5 = ___right1;
		Il2CppObject * L_6 = L_5;
		G_B4_0 = L_6;
		if (L_6)
		{
			G_B5_0 = L_6;
			goto IL_0028;
		}
	}
	{
		G_B5_0 = ((Il2CppObject *)(_stringLiteral1792391387));
	}

IL_0028:
	{
		___right1 = G_B5_0;
		StringU5BU5D_t2956870243* L_7 = ((StringU5BU5D_t2956870243*)SZArrayNew(StringU5BU5D_t2956870243_il2cpp_TypeInfo_var, (uint32_t)6));
		NullCheck(L_7);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_7, 0);
		ArrayElementTypeCheck (L_7, _stringLiteral990189687);
		(L_7)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)_stringLiteral990189687);
		StringU5BU5D_t2956870243* L_8 = L_7;
		Il2CppObject * L_9 = ___right1;
		NullCheck(L_9);
		String_t* L_10 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_9);
		NullCheck(L_8);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_8, 1);
		ArrayElementTypeCheck (L_8, L_10);
		(L_8)->SetAt(static_cast<il2cpp_array_size_t>(1), (String_t*)L_10);
		StringU5BU5D_t2956870243* L_11 = L_8;
		NullCheck(L_11);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_11, 2);
		ArrayElementTypeCheck (L_11, _stringLiteral2104341395);
		(L_11)->SetAt(static_cast<il2cpp_array_size_t>(2), (String_t*)_stringLiteral2104341395);
		StringU5BU5D_t2956870243* L_12 = L_11;
		Il2CppObject * L_13 = ___left0;
		NullCheck(L_13);
		String_t* L_14 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_13);
		NullCheck(L_12);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_12, 3);
		ArrayElementTypeCheck (L_12, L_14);
		(L_12)->SetAt(static_cast<il2cpp_array_size_t>(3), (String_t*)L_14);
		StringU5BU5D_t2956870243* L_15 = L_12;
		NullCheck(L_15);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_15, 4);
		ArrayElementTypeCheck (L_15, _stringLiteral38937);
		(L_15)->SetAt(static_cast<il2cpp_array_size_t>(4), (String_t*)_stringLiteral38937);
		StringU5BU5D_t2956870243* L_16 = L_15;
		String_t* L_17 = ___message2;
		NullCheck(L_16);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_16, 5);
		ArrayElementTypeCheck (L_16, L_17);
		(L_16)->SetAt(static_cast<il2cpp_array_size_t>(5), (String_t*)L_17);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_18 = String_Concat_m21867311(NULL /*static, unused*/, L_16, /*hidden argument*/NULL);
		Assert_Throw_m197124005(NULL /*static, unused*/, L_18, /*hidden argument*/NULL);
	}

IL_0068:
	{
		return;
	}
}
// System.Void ModestTree.Assert::That(System.Boolean,System.Func`1<System.String>)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const MethodInfo* Func_1_Invoke_m3326986245_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral2314456504;
extern const uint32_t Assert_That_m1170495047_MetadataUsageId;
extern "C"  void Assert_That_m1170495047 (Il2CppObject * __this /* static, unused */, bool ___condition0, Func_1_t2111270149 * ___messageGenerator1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Assert_That_m1170495047_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = ___condition0;
		if (L_0)
		{
			goto IL_001b;
		}
	}
	{
		Func_1_t2111270149 * L_1 = ___messageGenerator1;
		NullCheck(L_1);
		String_t* L_2 = Func_1_Invoke_m3326986245(L_1, /*hidden argument*/Func_1_Invoke_m3326986245_MethodInfo_var);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_3 = String_Concat_m138640077(NULL /*static, unused*/, _stringLiteral2314456504, L_2, /*hidden argument*/NULL);
		Assert_Throw_m197124005(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
	}

IL_001b:
	{
		return;
	}
}
// System.Void ModestTree.Assert::That(System.Boolean,System.String,System.Object[])
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2314456504;
extern const uint32_t Assert_That_m2934751473_MetadataUsageId;
extern "C"  void Assert_That_m2934751473 (Il2CppObject * __this /* static, unused */, bool ___condition0, String_t* ___message1, ObjectU5BU5D_t11523773* ___parameters2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Assert_That_m2934751473_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = ___condition0;
		if (L_0)
		{
			goto IL_001c;
		}
	}
	{
		String_t* L_1 = ___message1;
		ObjectU5BU5D_t11523773* L_2 = ___parameters2;
		String_t* L_3 = Assert_FormatString_m3297644574(NULL /*static, unused*/, L_1, L_2, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_4 = String_Concat_m138640077(NULL /*static, unused*/, _stringLiteral2314456504, L_3, /*hidden argument*/NULL);
		Assert_Throw_m197124005(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
	}

IL_001c:
	{
		return;
	}
}
// System.Void ModestTree.Assert::Throw(System.String)
extern Il2CppClass* ZenjectException_t523236501_il2cpp_TypeInfo_var;
extern const uint32_t Assert_Throw_m197124005_MetadataUsageId;
extern "C"  void Assert_Throw_m197124005 (Il2CppObject * __this /* static, unused */, String_t* ___message0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Assert_Throw_m197124005_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = ___message0;
		ZenjectException_t523236501 * L_1 = (ZenjectException_t523236501 *)il2cpp_codegen_object_new(ZenjectException_t523236501_il2cpp_TypeInfo_var);
		ZenjectException__ctor_m3982815302(L_1, L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}
}
// System.Void ModestTree.Assert::Throw(System.String,System.Object[])
extern Il2CppClass* ZenjectException_t523236501_il2cpp_TypeInfo_var;
extern const uint32_t Assert_Throw_m753765393_MetadataUsageId;
extern "C"  void Assert_Throw_m753765393 (Il2CppObject * __this /* static, unused */, String_t* ___message0, ObjectU5BU5D_t11523773* ___parameters1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Assert_Throw_m753765393_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = ___message0;
		ObjectU5BU5D_t11523773* L_1 = ___parameters1;
		String_t* L_2 = Assert_FormatString_m3297644574(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		ZenjectException_t523236501 * L_3 = (ZenjectException_t523236501 *)il2cpp_codegen_object_new(ZenjectException_t523236501_il2cpp_TypeInfo_var);
		ZenjectException__ctor_m3982815302(L_3, L_2, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}
}
// System.String ModestTree.Assert::FormatString(System.String,System.Object[])
extern Il2CppClass* ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2407815;
extern const uint32_t Assert_FormatString_m3297644574_MetadataUsageId;
extern "C"  String_t* Assert_FormatString_m3297644574 (Il2CppObject * __this /* static, unused */, String_t* ___format0, ObjectU5BU5D_t11523773* ___parameters1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Assert_FormatString_m3297644574_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	ObjectU5BU5D_t11523773* V_0 = NULL;
	Il2CppObject * V_1 = NULL;
	ObjectU5BU5D_t11523773* V_2 = NULL;
	int32_t V_3 = 0;
	int32_t V_4 = 0;
	Il2CppObject * G_B7_0 = NULL;
	int32_t G_B7_1 = 0;
	ObjectU5BU5D_t11523773* G_B7_2 = NULL;
	Il2CppObject * G_B6_0 = NULL;
	int32_t G_B6_1 = 0;
	ObjectU5BU5D_t11523773* G_B6_2 = NULL;
	{
		ObjectU5BU5D_t11523773* L_0 = ___parameters1;
		if (!L_0)
		{
			goto IL_0074;
		}
	}
	{
		ObjectU5BU5D_t11523773* L_1 = ___parameters1;
		NullCheck(L_1);
		if ((((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_1)->max_length))))) <= ((int32_t)0)))
		{
			goto IL_0074;
		}
	}
	{
		ObjectU5BU5D_t11523773* L_2 = ___parameters1;
		V_0 = L_2;
		ObjectU5BU5D_t11523773* L_3 = ___parameters1;
		V_2 = L_3;
		V_3 = 0;
		goto IL_0062;
	}

IL_001a:
	{
		ObjectU5BU5D_t11523773* L_4 = V_2;
		int32_t L_5 = V_3;
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, L_5);
		int32_t L_6 = L_5;
		V_1 = ((L_4)->GetAt(static_cast<il2cpp_array_size_t>(L_6)));
		Il2CppObject * L_7 = V_1;
		if (L_7)
		{
			goto IL_005e;
		}
	}
	{
		ObjectU5BU5D_t11523773* L_8 = ___parameters1;
		NullCheck(L_8);
		V_0 = ((ObjectU5BU5D_t11523773*)SZArrayNew(ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var, (uint32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_8)->max_length))))));
		V_4 = 0;
		goto IL_004f;
	}

IL_0035:
	{
		ObjectU5BU5D_t11523773* L_9 = V_0;
		int32_t L_10 = V_4;
		ObjectU5BU5D_t11523773* L_11 = ___parameters1;
		int32_t L_12 = V_4;
		NullCheck(L_11);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_11, L_12);
		int32_t L_13 = L_12;
		Il2CppObject * L_14 = ((L_11)->GetAt(static_cast<il2cpp_array_size_t>(L_13)));
		G_B6_0 = L_14;
		G_B6_1 = L_10;
		G_B6_2 = L_9;
		if (L_14)
		{
			G_B7_0 = L_14;
			G_B7_1 = L_10;
			G_B7_2 = L_9;
			goto IL_0048;
		}
	}
	{
		G_B7_0 = ((Il2CppObject *)(_stringLiteral2407815));
		G_B7_1 = G_B6_1;
		G_B7_2 = G_B6_2;
	}

IL_0048:
	{
		NullCheck(G_B7_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(G_B7_2, G_B7_1);
		ArrayElementTypeCheck (G_B7_2, G_B7_0);
		(G_B7_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B7_1), (Il2CppObject *)G_B7_0);
		int32_t L_15 = V_4;
		V_4 = ((int32_t)((int32_t)L_15+(int32_t)1));
	}

IL_004f:
	{
		int32_t L_16 = V_4;
		ObjectU5BU5D_t11523773* L_17 = ___parameters1;
		NullCheck(L_17);
		if ((((int32_t)L_16) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_17)->max_length)))))))
		{
			goto IL_0035;
		}
	}
	{
		goto IL_006b;
	}

IL_005e:
	{
		int32_t L_18 = V_3;
		V_3 = ((int32_t)((int32_t)L_18+(int32_t)1));
	}

IL_0062:
	{
		int32_t L_19 = V_3;
		ObjectU5BU5D_t11523773* L_20 = V_2;
		NullCheck(L_20);
		if ((((int32_t)L_19) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_20)->max_length)))))))
		{
			goto IL_001a;
		}
	}

IL_006b:
	{
		String_t* L_21 = ___format0;
		ObjectU5BU5D_t11523773* L_22 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_23 = String_Format_m4050103162(NULL /*static, unused*/, L_21, L_22, /*hidden argument*/NULL);
		___format0 = L_23;
	}

IL_0074:
	{
		String_t* L_24 = ___format0;
		return L_24;
	}
}
// System.Void ModestTree.Log::Debug(System.String,System.Object[])
extern "C"  void Log_Debug_m3985269754 (Il2CppObject * __this /* static, unused */, String_t* ___message0, ObjectU5BU5D_t11523773* ___args1, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void ModestTree.Log::Info(System.String,System.Object[])
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t1588791936_il2cpp_TypeInfo_var;
extern const uint32_t Log_Info_m1110695167_MetadataUsageId;
extern "C"  void Log_Info_m1110695167 (Il2CppObject * __this /* static, unused */, String_t* ___message0, ObjectU5BU5D_t11523773* ___args1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Log_Info_m1110695167_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = ___message0;
		ObjectU5BU5D_t11523773* L_1 = ___args1;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_2 = String_Format_m4050103162(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1588791936_il2cpp_TypeInfo_var);
		Debug_Log_m3349710197(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void ModestTree.Log::Warn(System.String,System.Object[])
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t1588791936_il2cpp_TypeInfo_var;
extern const uint32_t Log_Warn_m626914343_MetadataUsageId;
extern "C"  void Log_Warn_m626914343 (Il2CppObject * __this /* static, unused */, String_t* ___message0, ObjectU5BU5D_t11523773* ___args1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Log_Warn_m626914343_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = ___message0;
		ObjectU5BU5D_t11523773* L_1 = ___args1;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_2 = String_Format_m4050103162(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1588791936_il2cpp_TypeInfo_var);
		Debug_LogWarning_m539478453(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void ModestTree.Log::Trace(System.String,System.Object[])
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t1588791936_il2cpp_TypeInfo_var;
extern const uint32_t Log_Trace_m1137978056_MetadataUsageId;
extern "C"  void Log_Trace_m1137978056 (Il2CppObject * __this /* static, unused */, String_t* ___message0, ObjectU5BU5D_t11523773* ___args1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Log_Trace_m1137978056_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = ___message0;
		ObjectU5BU5D_t11523773* L_1 = ___args1;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_2 = String_Format_m4050103162(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1588791936_il2cpp_TypeInfo_var);
		Debug_Log_m3349710197(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void ModestTree.Log::ErrorException(System.Exception)
extern Il2CppClass* Debug_t1588791936_il2cpp_TypeInfo_var;
extern const uint32_t Log_ErrorException_m1940003768_MetadataUsageId;
extern "C"  void Log_ErrorException_m1940003768 (Il2CppObject * __this /* static, unused */, Exception_t1967233988 * ___e0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Log_ErrorException_m1940003768_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Exception_t1967233988 * L_0 = ___e0;
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1588791936_il2cpp_TypeInfo_var);
		Debug_LogException_m248970745(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void ModestTree.Log::ErrorException(System.String,System.Exception)
extern Il2CppClass* Debug_t1588791936_il2cpp_TypeInfo_var;
extern const uint32_t Log_ErrorException_m1080846204_MetadataUsageId;
extern "C"  void Log_ErrorException_m1080846204 (Il2CppObject * __this /* static, unused */, String_t* ___message0, Exception_t1967233988 * ___e1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Log_ErrorException_m1080846204_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = ___message0;
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1588791936_il2cpp_TypeInfo_var);
		Debug_LogError_m1836388681(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		Exception_t1967233988 * L_1 = ___e1;
		Debug_LogException_m248970745(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void ModestTree.Log::Error(System.String,System.Object[])
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t1588791936_il2cpp_TypeInfo_var;
extern const uint32_t Log_Error_m1405874597_MetadataUsageId;
extern "C"  void Log_Error_m1405874597 (Il2CppObject * __this /* static, unused */, String_t* ___message0, ObjectU5BU5D_t11523773* ___args1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Log_Error_m1405874597_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = ___message0;
		ObjectU5BU5D_t11523773* L_1 = ___args1;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_2 = String_Format_m4050103162(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1588791936_il2cpp_TypeInfo_var);
		Debug_LogError_m1836388681(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.String ModestTree.MiscExtensions::Fmt(System.String,System.Object[])
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const uint32_t MiscExtensions_Fmt_m1348433569_MetadataUsageId;
extern "C"  String_t* MiscExtensions_Fmt_m1348433569 (Il2CppObject * __this /* static, unused */, String_t* ___s0, ObjectU5BU5D_t11523773* ___args1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (MiscExtensions_Fmt_m1348433569_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = ___s0;
		ObjectU5BU5D_t11523773* L_1 = ___args1;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_2 = String_Format_m4050103162(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.String ModestTree.MiscExtensions::Join(System.Collections.Generic.IEnumerable`1<System.String>,System.String)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const MethodInfo* Enumerable_ToArray_TisString_t_m2199204590_MethodInfo_var;
extern const uint32_t MiscExtensions_Join_m3896482411_MetadataUsageId;
extern "C"  String_t* MiscExtensions_Join_m3896482411 (Il2CppObject * __this /* static, unused */, Il2CppObject* ___values0, String_t* ___separator1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (MiscExtensions_Join_m3896482411_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = ___separator1;
		Il2CppObject* L_1 = ___values0;
		StringU5BU5D_t2956870243* L_2 = Enumerable_ToArray_TisString_t_m2199204590(NULL /*static, unused*/, L_1, /*hidden argument*/Enumerable_ToArray_TisString_t_m2199204590_MethodInfo_var);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_3 = String_Join_m2789530325(NULL /*static, unused*/, L_0, L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
// System.Boolean ModestTree.TypeExtensions::DerivesFrom(System.Type,System.Type)
extern "C"  bool TypeExtensions_DerivesFrom_m1063347187 (Il2CppObject * __this /* static, unused */, Type_t * ___a0, Type_t * ___b1, const MethodInfo* method)
{
	int32_t G_B3_0 = 0;
	{
		Type_t * L_0 = ___b1;
		Type_t * L_1 = ___a0;
		if ((((Il2CppObject*)(Type_t *)L_0) == ((Il2CppObject*)(Type_t *)L_1)))
		{
			goto IL_0010;
		}
	}
	{
		Type_t * L_2 = ___b1;
		Type_t * L_3 = ___a0;
		NullCheck(L_2);
		bool L_4 = VirtFuncInvoker1< bool, Type_t * >::Invoke(46 /* System.Boolean System.Type::IsAssignableFrom(System.Type) */, L_2, L_3);
		G_B3_0 = ((int32_t)(L_4));
		goto IL_0011;
	}

IL_0010:
	{
		G_B3_0 = 0;
	}

IL_0011:
	{
		return (bool)G_B3_0;
	}
}
// System.Boolean ModestTree.TypeExtensions::DerivesFromOrEqual(System.Type,System.Type)
extern "C"  bool TypeExtensions_DerivesFromOrEqual_m3526502226 (Il2CppObject * __this /* static, unused */, Type_t * ___a0, Type_t * ___b1, const MethodInfo* method)
{
	int32_t G_B3_0 = 0;
	{
		Type_t * L_0 = ___b1;
		Type_t * L_1 = ___a0;
		if ((((Il2CppObject*)(Type_t *)L_0) == ((Il2CppObject*)(Type_t *)L_1)))
		{
			goto IL_0010;
		}
	}
	{
		Type_t * L_2 = ___b1;
		Type_t * L_3 = ___a0;
		NullCheck(L_2);
		bool L_4 = VirtFuncInvoker1< bool, Type_t * >::Invoke(46 /* System.Boolean System.Type::IsAssignableFrom(System.Type) */, L_2, L_3);
		G_B3_0 = ((int32_t)(L_4));
		goto IL_0011;
	}

IL_0010:
	{
		G_B3_0 = 1;
	}

IL_0011:
	{
		return (bool)G_B3_0;
	}
}
// System.Object ModestTree.TypeExtensions::GetDefaultValue(System.Type)
extern "C"  Il2CppObject * TypeExtensions_GetDefaultValue_m1549006447 (Il2CppObject * __this /* static, unused */, Type_t * ___type0, const MethodInfo* method)
{
	{
		Type_t * L_0 = ___type0;
		NullCheck(L_0);
		bool L_1 = VirtFuncInvoker0< bool >::Invoke(36 /* System.Boolean System.Type::get_IsValueType() */, L_0);
		if (!L_1)
		{
			goto IL_0012;
		}
	}
	{
		Type_t * L_2 = ___type0;
		Il2CppObject * L_3 = Activator_CreateInstance_m1399154923(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		return L_3;
	}

IL_0012:
	{
		return NULL;
	}
}
// System.String ModestTree.TypeExtensions::GetSimpleName(System.Type)
extern Il2CppCodeGenString* _stringLiteral96;
extern const uint32_t TypeExtensions_GetSimpleName_m3472407796_MetadataUsageId;
extern "C"  String_t* TypeExtensions_GetSimpleName_m3472407796 (Il2CppObject * __this /* static, unused */, Type_t * ___type0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TypeExtensions_GetSimpleName_m3472407796_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	String_t* V_0 = NULL;
	int32_t V_1 = 0;
	{
		Type_t * L_0 = ___type0;
		NullCheck(L_0);
		String_t* L_1 = VirtFuncInvoker0< String_t* >::Invoke(9 /* System.String System.Reflection.MemberInfo::get_Name() */, L_0);
		V_0 = L_1;
		String_t* L_2 = V_0;
		NullCheck(L_2);
		int32_t L_3 = String_IndexOf_m1476794331(L_2, _stringLiteral96, /*hidden argument*/NULL);
		V_1 = L_3;
		int32_t L_4 = V_1;
		if ((!(((uint32_t)L_4) == ((uint32_t)(-1)))))
		{
			goto IL_001c;
		}
	}
	{
		String_t* L_5 = V_0;
		return L_5;
	}

IL_001c:
	{
		String_t* L_6 = V_0;
		int32_t L_7 = V_1;
		NullCheck(L_6);
		String_t* L_8 = String_Substring_m675079568(L_6, 0, L_7, /*hidden argument*/NULL);
		return L_8;
	}
}
// System.Collections.Generic.IEnumerable`1<System.Type> ModestTree.TypeExtensions::GetParentTypes(System.Type)
extern Il2CppClass* U3CGetParentTypesU3Ec__Iterator3E_t1503887130_il2cpp_TypeInfo_var;
extern const uint32_t TypeExtensions_GetParentTypes_m3922980882_MetadataUsageId;
extern "C"  Il2CppObject* TypeExtensions_GetParentTypes_m3922980882 (Il2CppObject * __this /* static, unused */, Type_t * ___type0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TypeExtensions_GetParentTypes_m3922980882_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	U3CGetParentTypesU3Ec__Iterator3E_t1503887130 * V_0 = NULL;
	{
		U3CGetParentTypesU3Ec__Iterator3E_t1503887130 * L_0 = (U3CGetParentTypesU3Ec__Iterator3E_t1503887130 *)il2cpp_codegen_object_new(U3CGetParentTypesU3Ec__Iterator3E_t1503887130_il2cpp_TypeInfo_var);
		U3CGetParentTypesU3Ec__Iterator3E__ctor_m2754063334(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CGetParentTypesU3Ec__Iterator3E_t1503887130 * L_1 = V_0;
		Type_t * L_2 = ___type0;
		NullCheck(L_1);
		L_1->set_type_0(L_2);
		U3CGetParentTypesU3Ec__Iterator3E_t1503887130 * L_3 = V_0;
		Type_t * L_4 = ___type0;
		NullCheck(L_3);
		L_3->set_U3CU24U3Etype_5(L_4);
		U3CGetParentTypesU3Ec__Iterator3E_t1503887130 * L_5 = V_0;
		U3CGetParentTypesU3Ec__Iterator3E_t1503887130 * L_6 = L_5;
		NullCheck(L_6);
		L_6->set_U24PC_3(((int32_t)-2));
		return L_6;
	}
}
// System.String ModestTree.TypeExtensions::NameWithParents(System.Type)
extern Il2CppClass* TypeExtensions_t3479487268_il2cpp_TypeInfo_var;
extern Il2CppClass* Func_2_t1392394819_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const MethodInfo* LinqExtensions_Prepend_TisType_t_m2355200423_MethodInfo_var;
extern const MethodInfo* TypeExtensions_U3CNameWithParentsU3Em__289_m740484287_MethodInfo_var;
extern const MethodInfo* Func_2__ctor_m861039247_MethodInfo_var;
extern const MethodInfo* Enumerable_Select_TisType_t_TisString_t_m322015137_MethodInfo_var;
extern const MethodInfo* Enumerable_ToArray_TisString_t_m2199204590_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral58;
extern const uint32_t TypeExtensions_NameWithParents_m1799611087_MetadataUsageId;
extern "C"  String_t* TypeExtensions_NameWithParents_m1799611087 (Il2CppObject * __this /* static, unused */, Type_t * ___type0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TypeExtensions_NameWithParents_m1799611087_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	StringU5BU5D_t2956870243* V_0 = NULL;
	Il2CppObject* G_B2_0 = NULL;
	Il2CppObject* G_B1_0 = NULL;
	{
		Type_t * L_0 = ___type0;
		Il2CppObject* L_1 = TypeExtensions_GetParentTypes_m3922980882(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		Type_t * L_2 = ___type0;
		Il2CppObject* L_3 = LinqExtensions_Prepend_TisType_t_m2355200423(NULL /*static, unused*/, L_1, L_2, /*hidden argument*/LinqExtensions_Prepend_TisType_t_m2355200423_MethodInfo_var);
		Func_2_t1392394819 * L_4 = ((TypeExtensions_t3479487268_StaticFields*)TypeExtensions_t3479487268_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__amU24cache0_0();
		G_B1_0 = L_3;
		if (L_4)
		{
			G_B2_0 = L_3;
			goto IL_0024;
		}
	}
	{
		IntPtr_t L_5;
		L_5.set_m_value_0((void*)(void*)TypeExtensions_U3CNameWithParentsU3Em__289_m740484287_MethodInfo_var);
		Func_2_t1392394819 * L_6 = (Func_2_t1392394819 *)il2cpp_codegen_object_new(Func_2_t1392394819_il2cpp_TypeInfo_var);
		Func_2__ctor_m861039247(L_6, NULL, L_5, /*hidden argument*/Func_2__ctor_m861039247_MethodInfo_var);
		((TypeExtensions_t3479487268_StaticFields*)TypeExtensions_t3479487268_il2cpp_TypeInfo_var->static_fields)->set_U3CU3Ef__amU24cache0_0(L_6);
		G_B2_0 = G_B1_0;
	}

IL_0024:
	{
		Func_2_t1392394819 * L_7 = ((TypeExtensions_t3479487268_StaticFields*)TypeExtensions_t3479487268_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__amU24cache0_0();
		Il2CppObject* L_8 = Enumerable_Select_TisType_t_TisString_t_m322015137(NULL /*static, unused*/, G_B2_0, L_7, /*hidden argument*/Enumerable_Select_TisType_t_TisString_t_m322015137_MethodInfo_var);
		StringU5BU5D_t2956870243* L_9 = Enumerable_ToArray_TisString_t_m2199204590(NULL /*static, unused*/, L_8, /*hidden argument*/Enumerable_ToArray_TisString_t_m2199204590_MethodInfo_var);
		V_0 = L_9;
		StringU5BU5D_t2956870243* L_10 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_11 = String_Join_m2789530325(NULL /*static, unused*/, _stringLiteral58, L_10, /*hidden argument*/NULL);
		return L_11;
	}
}
// System.Boolean ModestTree.TypeExtensions::IsClosedGenericType(System.Type)
extern "C"  bool TypeExtensions_IsClosedGenericType_m1919514171 (Il2CppObject * __this /* static, unused */, Type_t * ___type0, const MethodInfo* method)
{
	int32_t G_B3_0 = 0;
	{
		Type_t * L_0 = ___type0;
		NullCheck(L_0);
		bool L_1 = VirtFuncInvoker0< bool >::Invoke(97 /* System.Boolean System.Type::get_IsGenericType() */, L_0);
		if (!L_1)
		{
			goto IL_0019;
		}
	}
	{
		Type_t * L_2 = ___type0;
		Type_t * L_3 = ___type0;
		NullCheck(L_3);
		Type_t * L_4 = VirtFuncInvoker0< Type_t * >::Invoke(96 /* System.Type System.Type::GetGenericTypeDefinition() */, L_3);
		G_B3_0 = ((((int32_t)((((Il2CppObject*)(Type_t *)L_2) == ((Il2CppObject*)(Type_t *)L_4))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_001a;
	}

IL_0019:
	{
		G_B3_0 = 0;
	}

IL_001a:
	{
		return (bool)G_B3_0;
	}
}
// System.Boolean ModestTree.TypeExtensions::IsOpenGenericType(System.Type)
extern "C"  bool TypeExtensions_IsOpenGenericType_m3853458201 (Il2CppObject * __this /* static, unused */, Type_t * ___type0, const MethodInfo* method)
{
	int32_t G_B3_0 = 0;
	{
		Type_t * L_0 = ___type0;
		NullCheck(L_0);
		bool L_1 = VirtFuncInvoker0< bool >::Invoke(97 /* System.Boolean System.Type::get_IsGenericType() */, L_0);
		if (!L_1)
		{
			goto IL_0016;
		}
	}
	{
		Type_t * L_2 = ___type0;
		Type_t * L_3 = ___type0;
		NullCheck(L_3);
		Type_t * L_4 = VirtFuncInvoker0< Type_t * >::Invoke(96 /* System.Type System.Type::GetGenericTypeDefinition() */, L_3);
		G_B3_0 = ((((Il2CppObject*)(Type_t *)L_2) == ((Il2CppObject*)(Type_t *)L_4))? 1 : 0);
		goto IL_0017;
	}

IL_0016:
	{
		G_B3_0 = 0;
	}

IL_0017:
	{
		return (bool)G_B3_0;
	}
}
// System.Collections.Generic.IEnumerable`1<System.Reflection.FieldInfo> ModestTree.TypeExtensions::GetAllFields(System.Type,System.Reflection.BindingFlags)
extern Il2CppClass* U3CGetAllFieldsU3Ec__Iterator3F_t219406118_il2cpp_TypeInfo_var;
extern const uint32_t TypeExtensions_GetAllFields_m1768734574_MetadataUsageId;
extern "C"  Il2CppObject* TypeExtensions_GetAllFields_m1768734574 (Il2CppObject * __this /* static, unused */, Type_t * ___type0, int32_t ___flags1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TypeExtensions_GetAllFields_m1768734574_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	U3CGetAllFieldsU3Ec__Iterator3F_t219406118 * V_0 = NULL;
	{
		U3CGetAllFieldsU3Ec__Iterator3F_t219406118 * L_0 = (U3CGetAllFieldsU3Ec__Iterator3F_t219406118 *)il2cpp_codegen_object_new(U3CGetAllFieldsU3Ec__Iterator3F_t219406118_il2cpp_TypeInfo_var);
		U3CGetAllFieldsU3Ec__Iterator3F__ctor_m1148109978(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CGetAllFieldsU3Ec__Iterator3F_t219406118 * L_1 = V_0;
		int32_t L_2 = ___flags1;
		NullCheck(L_1);
		L_1->set_flags_0(L_2);
		U3CGetAllFieldsU3Ec__Iterator3F_t219406118 * L_3 = V_0;
		Type_t * L_4 = ___type0;
		NullCheck(L_3);
		L_3->set_type_1(L_4);
		U3CGetAllFieldsU3Ec__Iterator3F_t219406118 * L_5 = V_0;
		int32_t L_6 = ___flags1;
		NullCheck(L_5);
		L_5->set_U3CU24U3Eflags_12(L_6);
		U3CGetAllFieldsU3Ec__Iterator3F_t219406118 * L_7 = V_0;
		Type_t * L_8 = ___type0;
		NullCheck(L_7);
		L_7->set_U3CU24U3Etype_13(L_8);
		U3CGetAllFieldsU3Ec__Iterator3F_t219406118 * L_9 = V_0;
		U3CGetAllFieldsU3Ec__Iterator3F_t219406118 * L_10 = L_9;
		NullCheck(L_10);
		L_10->set_U24PC_10(((int32_t)-2));
		return L_10;
	}
}
// System.Collections.Generic.IEnumerable`1<System.Reflection.PropertyInfo> ModestTree.TypeExtensions::GetAllProperties(System.Type,System.Reflection.BindingFlags)
extern Il2CppClass* U3CGetAllPropertiesU3Ec__Iterator40_t3557131433_il2cpp_TypeInfo_var;
extern const uint32_t TypeExtensions_GetAllProperties_m3364896073_MetadataUsageId;
extern "C"  Il2CppObject* TypeExtensions_GetAllProperties_m3364896073 (Il2CppObject * __this /* static, unused */, Type_t * ___type0, int32_t ___flags1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TypeExtensions_GetAllProperties_m3364896073_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	U3CGetAllPropertiesU3Ec__Iterator40_t3557131433 * V_0 = NULL;
	{
		U3CGetAllPropertiesU3Ec__Iterator40_t3557131433 * L_0 = (U3CGetAllPropertiesU3Ec__Iterator40_t3557131433 *)il2cpp_codegen_object_new(U3CGetAllPropertiesU3Ec__Iterator40_t3557131433_il2cpp_TypeInfo_var);
		U3CGetAllPropertiesU3Ec__Iterator40__ctor_m2990873591(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CGetAllPropertiesU3Ec__Iterator40_t3557131433 * L_1 = V_0;
		int32_t L_2 = ___flags1;
		NullCheck(L_1);
		L_1->set_flags_0(L_2);
		U3CGetAllPropertiesU3Ec__Iterator40_t3557131433 * L_3 = V_0;
		Type_t * L_4 = ___type0;
		NullCheck(L_3);
		L_3->set_type_1(L_4);
		U3CGetAllPropertiesU3Ec__Iterator40_t3557131433 * L_5 = V_0;
		int32_t L_6 = ___flags1;
		NullCheck(L_5);
		L_5->set_U3CU24U3Eflags_12(L_6);
		U3CGetAllPropertiesU3Ec__Iterator40_t3557131433 * L_7 = V_0;
		Type_t * L_8 = ___type0;
		NullCheck(L_7);
		L_7->set_U3CU24U3Etype_13(L_8);
		U3CGetAllPropertiesU3Ec__Iterator40_t3557131433 * L_9 = V_0;
		U3CGetAllPropertiesU3Ec__Iterator40_t3557131433 * L_10 = L_9;
		NullCheck(L_10);
		L_10->set_U24PC_10(((int32_t)-2));
		return L_10;
	}
}
// System.Collections.Generic.IEnumerable`1<System.Reflection.MethodInfo> ModestTree.TypeExtensions::GetAllMethods(System.Type,System.Reflection.BindingFlags)
extern Il2CppClass* U3CGetAllMethodsU3Ec__Iterator41_t3371665531_il2cpp_TypeInfo_var;
extern const uint32_t TypeExtensions_GetAllMethods_m2967502904_MetadataUsageId;
extern "C"  Il2CppObject* TypeExtensions_GetAllMethods_m2967502904 (Il2CppObject * __this /* static, unused */, Type_t * ___type0, int32_t ___flags1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TypeExtensions_GetAllMethods_m2967502904_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	U3CGetAllMethodsU3Ec__Iterator41_t3371665531 * V_0 = NULL;
	{
		U3CGetAllMethodsU3Ec__Iterator41_t3371665531 * L_0 = (U3CGetAllMethodsU3Ec__Iterator41_t3371665531 *)il2cpp_codegen_object_new(U3CGetAllMethodsU3Ec__Iterator41_t3371665531_il2cpp_TypeInfo_var);
		U3CGetAllMethodsU3Ec__Iterator41__ctor_m473758091(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CGetAllMethodsU3Ec__Iterator41_t3371665531 * L_1 = V_0;
		int32_t L_2 = ___flags1;
		NullCheck(L_1);
		L_1->set_flags_0(L_2);
		U3CGetAllMethodsU3Ec__Iterator41_t3371665531 * L_3 = V_0;
		Type_t * L_4 = ___type0;
		NullCheck(L_3);
		L_3->set_type_1(L_4);
		U3CGetAllMethodsU3Ec__Iterator41_t3371665531 * L_5 = V_0;
		int32_t L_6 = ___flags1;
		NullCheck(L_5);
		L_5->set_U3CU24U3Eflags_12(L_6);
		U3CGetAllMethodsU3Ec__Iterator41_t3371665531 * L_7 = V_0;
		Type_t * L_8 = ___type0;
		NullCheck(L_7);
		L_7->set_U3CU24U3Etype_13(L_8);
		U3CGetAllMethodsU3Ec__Iterator41_t3371665531 * L_9 = V_0;
		U3CGetAllMethodsU3Ec__Iterator41_t3371665531 * L_10 = L_9;
		NullCheck(L_10);
		L_10->set_U24PC_10(((int32_t)-2));
		return L_10;
	}
}
// System.String ModestTree.TypeExtensions::Name(System.Type)
extern const Il2CppType* Nullable_1_t2791603879_0_0_0_var;
extern const Il2CppType* ValueType_t4014882752_0_0_0_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern Il2CppClass* TypeExtensions_t3479487268_il2cpp_TypeInfo_var;
extern Il2CppClass* Func_2_t1392394819_il2cpp_TypeInfo_var;
extern const MethodInfo* Enumerable_Single_TisType_t_m4159287907_MethodInfo_var;
extern const MethodInfo* TypeExtensions_U3CNameU3Em__28A_m1906710308_MethodInfo_var;
extern const MethodInfo* Func_2__ctor_m861039247_MethodInfo_var;
extern const MethodInfo* Enumerable_Select_TisType_t_TisString_t_m322015137_MethodInfo_var;
extern const MethodInfo* Enumerable_ToArray_TisString_t_m2199204590_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral115146090;
extern Il2CppCodeGenString* _stringLiteral2453644161;
extern Il2CppCodeGenString* _stringLiteral63;
extern Il2CppCodeGenString* _stringLiteral96;
extern Il2CppCodeGenString* _stringLiteral44;
extern Il2CppCodeGenString* _stringLiteral60;
extern Il2CppCodeGenString* _stringLiteral62;
extern Il2CppCodeGenString* _stringLiteral46;
extern const uint32_t TypeExtensions_Name_m3754378430_MetadataUsageId;
extern "C"  String_t* TypeExtensions_Name_m3754378430 (Il2CppObject * __this /* static, unused */, Type_t * ___type0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TypeExtensions_Name_m3754378430_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	String_t* V_1 = NULL;
	String_t* V_2 = NULL;
	String_t* G_B11_0 = NULL;
	TypeU5BU5D_t3431720054* G_B13_0 = NULL;
	String_t* G_B13_1 = NULL;
	TypeU5BU5D_t3431720054* G_B12_0 = NULL;
	String_t* G_B12_1 = NULL;
	String_t* G_B16_0 = NULL;
	String_t* G_B20_0 = NULL;
	{
		Type_t * L_0 = ___type0;
		NullCheck(L_0);
		bool L_1 = VirtFuncInvoker0< bool >::Invoke(22 /* System.Boolean System.Type::get_IsArray() */, L_0);
		if (!L_1)
		{
			goto IL_0021;
		}
	}
	{
		Type_t * L_2 = ___type0;
		NullCheck(L_2);
		Type_t * L_3 = VirtFuncInvoker0< Type_t * >::Invoke(49 /* System.Type System.Type::GetElementType() */, L_2);
		String_t* L_4 = TypeExtensions_Name_m3754378430(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_5 = String_Format_m2471250780(NULL /*static, unused*/, _stringLiteral115146090, L_4, /*hidden argument*/NULL);
		return L_5;
	}

IL_0021:
	{
		Type_t * L_6 = ___type0;
		NullCheck(L_6);
		bool L_7 = VirtFuncInvoker0< bool >::Invoke(94 /* System.Boolean System.Type::get_ContainsGenericParameters() */, L_6);
		if (L_7)
		{
			goto IL_0037;
		}
	}
	{
		Type_t * L_8 = ___type0;
		NullCheck(L_8);
		bool L_9 = VirtFuncInvoker0< bool >::Invoke(97 /* System.Boolean System.Type::get_IsGenericType() */, L_8);
		if (!L_9)
		{
			goto IL_0127;
		}
	}

IL_0037:
	{
		Type_t * L_10 = ___type0;
		NullCheck(L_10);
		Type_t * L_11 = VirtFuncInvoker0< Type_t * >::Invoke(18 /* System.Type System.Type::get_BaseType() */, L_10);
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_12 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, LoadTypeToken(Nullable_1_t2791603879_0_0_0_var), /*hidden argument*/NULL);
		if ((((Il2CppObject*)(Type_t *)L_11) == ((Il2CppObject*)(Type_t *)L_12)))
		{
			goto IL_007b;
		}
	}
	{
		Type_t * L_13 = ___type0;
		NullCheck(L_13);
		Type_t * L_14 = VirtFuncInvoker0< Type_t * >::Invoke(18 /* System.Type System.Type::get_BaseType() */, L_13);
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_15 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, LoadTypeToken(ValueType_t4014882752_0_0_0_var), /*hidden argument*/NULL);
		if ((!(((Il2CppObject*)(Type_t *)L_14) == ((Il2CppObject*)(Type_t *)L_15))))
		{
			goto IL_009b;
		}
	}
	{
		Type_t * L_16 = ___type0;
		NullCheck(L_16);
		Type_t * L_17 = VirtFuncInvoker0< Type_t * >::Invoke(39 /* System.Type System.Type::get_UnderlyingSystemType() */, L_16);
		NullCheck(L_17);
		String_t* L_18 = VirtFuncInvoker0< String_t* >::Invoke(9 /* System.String System.Reflection.MemberInfo::get_Name() */, L_17);
		NullCheck(L_18);
		bool L_19 = String_StartsWith_m1500793453(L_18, _stringLiteral2453644161, /*hidden argument*/NULL);
		if (!L_19)
		{
			goto IL_009b;
		}
	}

IL_007b:
	{
		Type_t * L_20 = ___type0;
		NullCheck(L_20);
		TypeU5BU5D_t3431720054* L_21 = VirtFuncInvoker0< TypeU5BU5D_t3431720054* >::Invoke(93 /* System.Type[] System.Type::GetGenericArguments() */, L_20);
		Type_t * L_22 = Enumerable_Single_TisType_t_m4159287907(NULL /*static, unused*/, (Il2CppObject*)(Il2CppObject*)L_21, /*hidden argument*/Enumerable_Single_TisType_t_m4159287907_MethodInfo_var);
		NullCheck(L_22);
		String_t* L_23 = VirtFuncInvoker0< String_t* >::Invoke(9 /* System.String System.Reflection.MemberInfo::get_Name() */, L_22);
		String_t* L_24 = TypeExtensions_GetCSharpTypeName_m3989884270(NULL /*static, unused*/, L_23, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_25 = String_Concat_m138640077(NULL /*static, unused*/, L_24, _stringLiteral63, /*hidden argument*/NULL);
		return L_25;
	}

IL_009b:
	{
		Type_t * L_26 = ___type0;
		NullCheck(L_26);
		String_t* L_27 = VirtFuncInvoker0< String_t* >::Invoke(9 /* System.String System.Reflection.MemberInfo::get_Name() */, L_26);
		NullCheck(L_27);
		int32_t L_28 = String_IndexOf_m1476794331(L_27, _stringLiteral96, /*hidden argument*/NULL);
		V_0 = L_28;
		int32_t L_29 = V_0;
		if ((((int32_t)L_29) <= ((int32_t)0)))
		{
			goto IL_00c5;
		}
	}
	{
		Type_t * L_30 = ___type0;
		NullCheck(L_30);
		String_t* L_31 = VirtFuncInvoker0< String_t* >::Invoke(9 /* System.String System.Reflection.MemberInfo::get_Name() */, L_30);
		int32_t L_32 = V_0;
		NullCheck(L_31);
		String_t* L_33 = String_Substring_m675079568(L_31, 0, L_32, /*hidden argument*/NULL);
		G_B11_0 = L_33;
		goto IL_00cb;
	}

IL_00c5:
	{
		Type_t * L_34 = ___type0;
		NullCheck(L_34);
		String_t* L_35 = VirtFuncInvoker0< String_t* >::Invoke(9 /* System.String System.Reflection.MemberInfo::get_Name() */, L_34);
		G_B11_0 = L_35;
	}

IL_00cb:
	{
		V_1 = G_B11_0;
		Type_t * L_36 = ___type0;
		NullCheck(L_36);
		TypeU5BU5D_t3431720054* L_37 = VirtFuncInvoker0< TypeU5BU5D_t3431720054* >::Invoke(93 /* System.Type[] System.Type::GetGenericArguments() */, L_36);
		Func_2_t1392394819 * L_38 = ((TypeExtensions_t3479487268_StaticFields*)TypeExtensions_t3479487268_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__amU24cache1_1();
		G_B12_0 = L_37;
		G_B12_1 = _stringLiteral44;
		if (L_38)
		{
			G_B13_0 = L_37;
			G_B13_1 = _stringLiteral44;
			goto IL_00ef;
		}
	}
	{
		IntPtr_t L_39;
		L_39.set_m_value_0((void*)(void*)TypeExtensions_U3CNameU3Em__28A_m1906710308_MethodInfo_var);
		Func_2_t1392394819 * L_40 = (Func_2_t1392394819 *)il2cpp_codegen_object_new(Func_2_t1392394819_il2cpp_TypeInfo_var);
		Func_2__ctor_m861039247(L_40, NULL, L_39, /*hidden argument*/Func_2__ctor_m861039247_MethodInfo_var);
		((TypeExtensions_t3479487268_StaticFields*)TypeExtensions_t3479487268_il2cpp_TypeInfo_var->static_fields)->set_U3CU3Ef__amU24cache1_1(L_40);
		G_B13_0 = G_B12_0;
		G_B13_1 = G_B12_1;
	}

IL_00ef:
	{
		Func_2_t1392394819 * L_41 = ((TypeExtensions_t3479487268_StaticFields*)TypeExtensions_t3479487268_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__amU24cache1_1();
		Il2CppObject* L_42 = Enumerable_Select_TisType_t_TisString_t_m322015137(NULL /*static, unused*/, (Il2CppObject*)(Il2CppObject*)G_B13_0, L_41, /*hidden argument*/Enumerable_Select_TisType_t_TisString_t_m322015137_MethodInfo_var);
		StringU5BU5D_t2956870243* L_43 = Enumerable_ToArray_TisString_t_m2199204590(NULL /*static, unused*/, L_42, /*hidden argument*/Enumerable_ToArray_TisString_t_m2199204590_MethodInfo_var);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_44 = String_Join_m2789530325(NULL /*static, unused*/, G_B13_1, L_43, /*hidden argument*/NULL);
		V_2 = L_44;
		String_t* L_45 = V_2;
		NullCheck(L_45);
		int32_t L_46 = String_get_Length_m2979997331(L_45, /*hidden argument*/NULL);
		if (L_46)
		{
			goto IL_0115;
		}
	}
	{
		String_t* L_47 = V_1;
		G_B16_0 = L_47;
		goto IL_0126;
	}

IL_0115:
	{
		String_t* L_48 = V_1;
		String_t* L_49 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_50 = String_Concat_m2933632197(NULL /*static, unused*/, L_48, _stringLiteral60, L_49, _stringLiteral62, /*hidden argument*/NULL);
		G_B16_0 = L_50;
	}

IL_0126:
	{
		return G_B16_0;
	}

IL_0127:
	{
		Type_t * L_51 = ___type0;
		NullCheck(L_51);
		Type_t * L_52 = VirtFuncInvoker0< Type_t * >::Invoke(7 /* System.Type System.Type::get_DeclaringType() */, L_51);
		if (L_52)
		{
			goto IL_013c;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_53 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B20_0 = L_53;
		goto IL_0151;
	}

IL_013c:
	{
		Type_t * L_54 = ___type0;
		NullCheck(L_54);
		Type_t * L_55 = VirtFuncInvoker0< Type_t * >::Invoke(7 /* System.Type System.Type::get_DeclaringType() */, L_54);
		String_t* L_56 = TypeExtensions_Name_m3754378430(NULL /*static, unused*/, L_55, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_57 = String_Concat_m138640077(NULL /*static, unused*/, L_56, _stringLiteral46, /*hidden argument*/NULL);
		G_B20_0 = L_57;
	}

IL_0151:
	{
		Type_t * L_58 = ___type0;
		NullCheck(L_58);
		String_t* L_59 = VirtFuncInvoker0< String_t* >::Invoke(9 /* System.String System.Reflection.MemberInfo::get_Name() */, L_58);
		String_t* L_60 = TypeExtensions_GetCSharpTypeName_m3989884270(NULL /*static, unused*/, L_59, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_61 = String_Concat_m138640077(NULL /*static, unused*/, G_B20_0, L_60, /*hidden argument*/NULL);
		return L_61;
	}
}
// System.String ModestTree.TypeExtensions::GetCSharpTypeName(System.String)
extern Il2CppClass* TypeExtensions_t3479487268_il2cpp_TypeInfo_var;
extern Il2CppClass* Dictionary_2_t190145395_il2cpp_TypeInfo_var;
extern const MethodInfo* Dictionary_2__ctor_m1958628151_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral2486848561;
extern Il2CppCodeGenString* _stringLiteral2355466079;
extern Il2CppCodeGenString* _stringLiteral2672052;
extern Il2CppCodeGenString* _stringLiteral2086184;
extern Il2CppCodeGenString* _stringLiteral2052876273;
extern Il2CppCodeGenString* _stringLiteral3206916913;
extern Il2CppCodeGenString* _stringLiteral70807092;
extern Il2CppCodeGenString* _stringLiteral70807150;
extern Il2CppCodeGenString* _stringLiteral70807245;
extern Il2CppCodeGenString* _stringLiteral2476568680;
extern Il2CppCodeGenString* _stringLiteral1729365000;
extern Il2CppCodeGenString* _stringLiteral109413500;
extern Il2CppCodeGenString* _stringLiteral104431;
extern Il2CppCodeGenString* _stringLiteral3327612;
extern Il2CppCodeGenString* _stringLiteral97526364;
extern Il2CppCodeGenString* _stringLiteral3029738;
extern const uint32_t TypeExtensions_GetCSharpTypeName_m3989884270_MetadataUsageId;
extern "C"  String_t* TypeExtensions_GetCSharpTypeName_m3989884270 (Il2CppObject * __this /* static, unused */, String_t* ___typeName0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TypeExtensions_GetCSharpTypeName_m3989884270_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	String_t* V_0 = NULL;
	Dictionary_2_t190145395 * V_1 = NULL;
	int32_t V_2 = 0;
	{
		String_t* L_0 = ___typeName0;
		V_0 = L_0;
		String_t* L_1 = V_0;
		if (!L_1)
		{
			goto IL_00fe;
		}
	}
	{
		Dictionary_2_t190145395 * L_2 = ((TypeExtensions_t3479487268_StaticFields*)TypeExtensions_t3479487268_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__switchU24mapE_2();
		if (L_2)
		{
			goto IL_00a4;
		}
	}
	{
		Dictionary_2_t190145395 * L_3 = (Dictionary_2_t190145395 *)il2cpp_codegen_object_new(Dictionary_2_t190145395_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m1958628151(L_3, ((int32_t)11), /*hidden argument*/Dictionary_2__ctor_m1958628151_MethodInfo_var);
		V_1 = L_3;
		Dictionary_2_t190145395 * L_4 = V_1;
		NullCheck(L_4);
		VirtActionInvoker2< String_t*, int32_t >::Invoke(18 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::Add(!0,!1) */, L_4, _stringLiteral2486848561, 0);
		Dictionary_2_t190145395 * L_5 = V_1;
		NullCheck(L_5);
		VirtActionInvoker2< String_t*, int32_t >::Invoke(18 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::Add(!0,!1) */, L_5, _stringLiteral2355466079, 0);
		Dictionary_2_t190145395 * L_6 = V_1;
		NullCheck(L_6);
		VirtActionInvoker2< String_t*, int32_t >::Invoke(18 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::Add(!0,!1) */, L_6, _stringLiteral2672052, 0);
		Dictionary_2_t190145395 * L_7 = V_1;
		NullCheck(L_7);
		VirtActionInvoker2< String_t*, int32_t >::Invoke(18 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::Add(!0,!1) */, L_7, _stringLiteral2086184, 0);
		Dictionary_2_t190145395 * L_8 = V_1;
		NullCheck(L_8);
		VirtActionInvoker2< String_t*, int32_t >::Invoke(18 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::Add(!0,!1) */, L_8, _stringLiteral2052876273, 0);
		Dictionary_2_t190145395 * L_9 = V_1;
		NullCheck(L_9);
		VirtActionInvoker2< String_t*, int32_t >::Invoke(18 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::Add(!0,!1) */, L_9, _stringLiteral3206916913, 0);
		Dictionary_2_t190145395 * L_10 = V_1;
		NullCheck(L_10);
		VirtActionInvoker2< String_t*, int32_t >::Invoke(18 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::Add(!0,!1) */, L_10, _stringLiteral70807092, 1);
		Dictionary_2_t190145395 * L_11 = V_1;
		NullCheck(L_11);
		VirtActionInvoker2< String_t*, int32_t >::Invoke(18 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::Add(!0,!1) */, L_11, _stringLiteral70807150, 2);
		Dictionary_2_t190145395 * L_12 = V_1;
		NullCheck(L_12);
		VirtActionInvoker2< String_t*, int32_t >::Invoke(18 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::Add(!0,!1) */, L_12, _stringLiteral70807245, 3);
		Dictionary_2_t190145395 * L_13 = V_1;
		NullCheck(L_13);
		VirtActionInvoker2< String_t*, int32_t >::Invoke(18 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::Add(!0,!1) */, L_13, _stringLiteral2476568680, 4);
		Dictionary_2_t190145395 * L_14 = V_1;
		NullCheck(L_14);
		VirtActionInvoker2< String_t*, int32_t >::Invoke(18 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::Add(!0,!1) */, L_14, _stringLiteral1729365000, 5);
		Dictionary_2_t190145395 * L_15 = V_1;
		((TypeExtensions_t3479487268_StaticFields*)TypeExtensions_t3479487268_il2cpp_TypeInfo_var->static_fields)->set_U3CU3Ef__switchU24mapE_2(L_15);
	}

IL_00a4:
	{
		Dictionary_2_t190145395 * L_16 = ((TypeExtensions_t3479487268_StaticFields*)TypeExtensions_t3479487268_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__switchU24mapE_2();
		String_t* L_17 = V_0;
		NullCheck(L_16);
		bool L_18 = VirtFuncInvoker2< bool, String_t*, int32_t* >::Invoke(21 /* System.Boolean System.Collections.Generic.Dictionary`2<System.String,System.Int32>::TryGetValue(!0,!1&) */, L_16, L_17, (&V_2));
		if (!L_18)
		{
			goto IL_00fe;
		}
	}
	{
		int32_t L_19 = V_2;
		if (L_19 == 0)
		{
			goto IL_00d9;
		}
		if (L_19 == 1)
		{
			goto IL_00e0;
		}
		if (L_19 == 2)
		{
			goto IL_00e6;
		}
		if (L_19 == 3)
		{
			goto IL_00ec;
		}
		if (L_19 == 4)
		{
			goto IL_00f2;
		}
		if (L_19 == 5)
		{
			goto IL_00f8;
		}
	}
	{
		goto IL_00fe;
	}

IL_00d9:
	{
		String_t* L_20 = ___typeName0;
		NullCheck(L_20);
		String_t* L_21 = String_ToLower_m2421900555(L_20, /*hidden argument*/NULL);
		return L_21;
	}

IL_00e0:
	{
		return _stringLiteral109413500;
	}

IL_00e6:
	{
		return _stringLiteral104431;
	}

IL_00ec:
	{
		return _stringLiteral3327612;
	}

IL_00f2:
	{
		return _stringLiteral97526364;
	}

IL_00f8:
	{
		return _stringLiteral3029738;
	}

IL_00fe:
	{
		String_t* L_22 = ___typeName0;
		return L_22;
	}
}
// System.Boolean ModestTree.TypeExtensions::HasAttribute(System.Reflection.ICustomAttributeProvider,System.Type[])
extern const MethodInfo* Enumerable_Any_TisAttribute_t498693649_m1582154716_MethodInfo_var;
extern const uint32_t TypeExtensions_HasAttribute_m3951583835_MetadataUsageId;
extern "C"  bool TypeExtensions_HasAttribute_m3951583835 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___provider0, TypeU5BU5D_t3431720054* ___attributeTypes1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TypeExtensions_HasAttribute_m3951583835_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject * L_0 = ___provider0;
		TypeU5BU5D_t3431720054* L_1 = ___attributeTypes1;
		Il2CppObject* L_2 = TypeExtensions_AllAttributes_m2108720102(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		bool L_3 = Enumerable_Any_TisAttribute_t498693649_m1582154716(NULL /*static, unused*/, L_2, /*hidden argument*/Enumerable_Any_TisAttribute_t498693649_m1582154716_MethodInfo_var);
		return L_3;
	}
}
// System.Collections.Generic.IEnumerable`1<System.Attribute> ModestTree.TypeExtensions::AllAttributes(System.Reflection.ICustomAttributeProvider,System.Type[])
extern Il2CppClass* U3CAllAttributesU3Ec__AnonStorey16D_t3765002394_il2cpp_TypeInfo_var;
extern Il2CppClass* ICustomAttributeProvider_t2334200065_il2cpp_TypeInfo_var;
extern Il2CppClass* Func_2_t1279580208_il2cpp_TypeInfo_var;
extern const MethodInfo* Enumerable_Cast_TisAttribute_t498693649_m1153716551_MethodInfo_var;
extern const MethodInfo* U3CAllAttributesU3Ec__AnonStorey16D_U3CU3Em__28B_m3317124830_MethodInfo_var;
extern const MethodInfo* Func_2__ctor_m3600768180_MethodInfo_var;
extern const MethodInfo* Enumerable_Where_TisAttribute_t498693649_m3527660099_MethodInfo_var;
extern const uint32_t TypeExtensions_AllAttributes_m2108720102_MetadataUsageId;
extern "C"  Il2CppObject* TypeExtensions_AllAttributes_m2108720102 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___provider0, TypeU5BU5D_t3431720054* ___attributeTypes1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TypeExtensions_AllAttributes_m2108720102_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Il2CppObject* V_0 = NULL;
	U3CAllAttributesU3Ec__AnonStorey16D_t3765002394 * V_1 = NULL;
	{
		U3CAllAttributesU3Ec__AnonStorey16D_t3765002394 * L_0 = (U3CAllAttributesU3Ec__AnonStorey16D_t3765002394 *)il2cpp_codegen_object_new(U3CAllAttributesU3Ec__AnonStorey16D_t3765002394_il2cpp_TypeInfo_var);
		U3CAllAttributesU3Ec__AnonStorey16D__ctor_m2989170470(L_0, /*hidden argument*/NULL);
		V_1 = L_0;
		U3CAllAttributesU3Ec__AnonStorey16D_t3765002394 * L_1 = V_1;
		TypeU5BU5D_t3431720054* L_2 = ___attributeTypes1;
		NullCheck(L_1);
		L_1->set_attributeTypes_0(L_2);
		Il2CppObject * L_3 = ___provider0;
		NullCheck(L_3);
		ObjectU5BU5D_t11523773* L_4 = InterfaceFuncInvoker1< ObjectU5BU5D_t11523773*, bool >::Invoke(0 /* System.Object[] System.Reflection.ICustomAttributeProvider::GetCustomAttributes(System.Boolean) */, ICustomAttributeProvider_t2334200065_il2cpp_TypeInfo_var, L_3, (bool)1);
		Il2CppObject* L_5 = Enumerable_Cast_TisAttribute_t498693649_m1153716551(NULL /*static, unused*/, (Il2CppObject *)(Il2CppObject *)L_4, /*hidden argument*/Enumerable_Cast_TisAttribute_t498693649_m1153716551_MethodInfo_var);
		V_0 = L_5;
		U3CAllAttributesU3Ec__AnonStorey16D_t3765002394 * L_6 = V_1;
		NullCheck(L_6);
		TypeU5BU5D_t3431720054* L_7 = L_6->get_attributeTypes_0();
		NullCheck(L_7);
		if ((((int32_t)((int32_t)(((Il2CppArray *)L_7)->max_length)))))
		{
			goto IL_0029;
		}
	}
	{
		Il2CppObject* L_8 = V_0;
		return L_8;
	}

IL_0029:
	{
		Il2CppObject* L_9 = V_0;
		U3CAllAttributesU3Ec__AnonStorey16D_t3765002394 * L_10 = V_1;
		IntPtr_t L_11;
		L_11.set_m_value_0((void*)(void*)U3CAllAttributesU3Ec__AnonStorey16D_U3CU3Em__28B_m3317124830_MethodInfo_var);
		Func_2_t1279580208 * L_12 = (Func_2_t1279580208 *)il2cpp_codegen_object_new(Func_2_t1279580208_il2cpp_TypeInfo_var);
		Func_2__ctor_m3600768180(L_12, L_10, L_11, /*hidden argument*/Func_2__ctor_m3600768180_MethodInfo_var);
		Il2CppObject* L_13 = Enumerable_Where_TisAttribute_t498693649_m3527660099(NULL /*static, unused*/, L_9, L_12, /*hidden argument*/Enumerable_Where_TisAttribute_t498693649_m3527660099_MethodInfo_var);
		return L_13;
	}
}
// System.String ModestTree.TypeExtensions::<NameWithParents>m__289(System.Type)
extern "C"  String_t* TypeExtensions_U3CNameWithParentsU3Em__289_m740484287 (Il2CppObject * __this /* static, unused */, Type_t * ___x0, const MethodInfo* method)
{
	{
		Type_t * L_0 = ___x0;
		String_t* L_1 = TypeExtensions_Name_m3754378430(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.String ModestTree.TypeExtensions::<Name>m__28A(System.Type)
extern "C"  String_t* TypeExtensions_U3CNameU3Em__28A_m1906710308 (Il2CppObject * __this /* static, unused */, Type_t * ___t0, const MethodInfo* method)
{
	{
		Type_t * L_0 = ___t0;
		String_t* L_1 = TypeExtensions_Name_m3754378430(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Void ModestTree.TypeExtensions/<AllAttributes>c__AnonStorey16D::.ctor()
extern "C"  void U3CAllAttributesU3Ec__AnonStorey16D__ctor_m2989170470 (U3CAllAttributesU3Ec__AnonStorey16D_t3765002394 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean ModestTree.TypeExtensions/<AllAttributes>c__AnonStorey16D::<>m__28B(System.Attribute)
extern const MethodInfo* Enumerable_Contains_TisType_t_m2414400775_MethodInfo_var;
extern const uint32_t U3CAllAttributesU3Ec__AnonStorey16D_U3CU3Em__28B_m3317124830_MetadataUsageId;
extern "C"  bool U3CAllAttributesU3Ec__AnonStorey16D_U3CU3Em__28B_m3317124830 (U3CAllAttributesU3Ec__AnonStorey16D_t3765002394 * __this, Attribute_t498693649 * ___a0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CAllAttributesU3Ec__AnonStorey16D_U3CU3Em__28B_m3317124830_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		TypeU5BU5D_t3431720054* L_0 = __this->get_attributeTypes_0();
		Attribute_t498693649 * L_1 = ___a0;
		NullCheck(L_1);
		Type_t * L_2 = Object_GetType_m2022236990(L_1, /*hidden argument*/NULL);
		bool L_3 = Enumerable_Contains_TisType_t_m2414400775(NULL /*static, unused*/, (Il2CppObject*)(Il2CppObject*)L_0, L_2, /*hidden argument*/Enumerable_Contains_TisType_t_m2414400775_MethodInfo_var);
		return L_3;
	}
}
// System.Void ModestTree.TypeExtensions/<GetAllFields>c__Iterator3F::.ctor()
extern "C"  void U3CGetAllFieldsU3Ec__Iterator3F__ctor_m1148109978 (U3CGetAllFieldsU3Ec__Iterator3F_t219406118 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Reflection.FieldInfo ModestTree.TypeExtensions/<GetAllFields>c__Iterator3F::System.Collections.Generic.IEnumerator<System.Reflection.FieldInfo>.get_Current()
extern "C"  FieldInfo_t * U3CGetAllFieldsU3Ec__Iterator3F_System_Collections_Generic_IEnumeratorU3CSystem_Reflection_FieldInfoU3E_get_Current_m891825729 (U3CGetAllFieldsU3Ec__Iterator3F_t219406118 * __this, const MethodInfo* method)
{
	{
		FieldInfo_t * L_0 = __this->get_U24current_11();
		return L_0;
	}
}
// System.Object ModestTree.TypeExtensions/<GetAllFields>c__Iterator3F::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CGetAllFieldsU3Ec__Iterator3F_System_Collections_IEnumerator_get_Current_m2874874956 (U3CGetAllFieldsU3Ec__Iterator3F_t219406118 * __this, const MethodInfo* method)
{
	{
		FieldInfo_t * L_0 = __this->get_U24current_11();
		return L_0;
	}
}
// System.Collections.IEnumerator ModestTree.TypeExtensions/<GetAllFields>c__Iterator3F::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * U3CGetAllFieldsU3Ec__Iterator3F_System_Collections_IEnumerable_GetEnumerator_m1698845575 (U3CGetAllFieldsU3Ec__Iterator3F_t219406118 * __this, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = U3CGetAllFieldsU3Ec__Iterator3F_System_Collections_Generic_IEnumerableU3CSystem_Reflection_FieldInfoU3E_GetEnumerator_m2441111908(__this, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Collections.Generic.IEnumerator`1<System.Reflection.FieldInfo> ModestTree.TypeExtensions/<GetAllFields>c__Iterator3F::System.Collections.Generic.IEnumerable<System.Reflection.FieldInfo>.GetEnumerator()
extern Il2CppClass* U3CGetAllFieldsU3Ec__Iterator3F_t219406118_il2cpp_TypeInfo_var;
extern const uint32_t U3CGetAllFieldsU3Ec__Iterator3F_System_Collections_Generic_IEnumerableU3CSystem_Reflection_FieldInfoU3E_GetEnumerator_m2441111908_MetadataUsageId;
extern "C"  Il2CppObject* U3CGetAllFieldsU3Ec__Iterator3F_System_Collections_Generic_IEnumerableU3CSystem_Reflection_FieldInfoU3E_GetEnumerator_m2441111908 (U3CGetAllFieldsU3Ec__Iterator3F_t219406118 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CGetAllFieldsU3Ec__Iterator3F_System_Collections_Generic_IEnumerableU3CSystem_Reflection_FieldInfoU3E_GetEnumerator_m2441111908_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	U3CGetAllFieldsU3Ec__Iterator3F_t219406118 * V_0 = NULL;
	{
		int32_t* L_0 = __this->get_address_of_U24PC_10();
		int32_t L_1 = Interlocked_CompareExchange_m1859820752(NULL /*static, unused*/, L_0, 0, ((int32_t)-2), /*hidden argument*/NULL);
		if ((!(((uint32_t)L_1) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0014;
		}
	}
	{
		return __this;
	}

IL_0014:
	{
		U3CGetAllFieldsU3Ec__Iterator3F_t219406118 * L_2 = (U3CGetAllFieldsU3Ec__Iterator3F_t219406118 *)il2cpp_codegen_object_new(U3CGetAllFieldsU3Ec__Iterator3F_t219406118_il2cpp_TypeInfo_var);
		U3CGetAllFieldsU3Ec__Iterator3F__ctor_m1148109978(L_2, /*hidden argument*/NULL);
		V_0 = L_2;
		U3CGetAllFieldsU3Ec__Iterator3F_t219406118 * L_3 = V_0;
		int32_t L_4 = __this->get_U3CU24U3Eflags_12();
		NullCheck(L_3);
		L_3->set_flags_0(L_4);
		U3CGetAllFieldsU3Ec__Iterator3F_t219406118 * L_5 = V_0;
		Type_t * L_6 = __this->get_U3CU24U3Etype_13();
		NullCheck(L_5);
		L_5->set_type_1(L_6);
		U3CGetAllFieldsU3Ec__Iterator3F_t219406118 * L_7 = V_0;
		return L_7;
	}
}
// System.Boolean ModestTree.TypeExtensions/<GetAllFields>c__Iterator3F::MoveNext()
extern const Il2CppType* Il2CppObject_0_0_0_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern Il2CppClass* IEnumerable_1_t4037084138_il2cpp_TypeInfo_var;
extern Il2CppClass* IEnumerator_1_t2648036230_il2cpp_TypeInfo_var;
extern Il2CppClass* IEnumerator_t287207039_il2cpp_TypeInfo_var;
extern Il2CppClass* IDisposable_t1628921374_il2cpp_TypeInfo_var;
extern const uint32_t U3CGetAllFieldsU3Ec__Iterator3F_MoveNext_m1871247898_MetadataUsageId;
extern "C"  bool U3CGetAllFieldsU3Ec__Iterator3F_MoveNext_m1871247898 (U3CGetAllFieldsU3Ec__Iterator3F_t219406118 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CGetAllFieldsU3Ec__Iterator3F_MoveNext_m1871247898_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	uint32_t V_0 = 0;
	bool V_1 = false;
	bool V_2 = false;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		int32_t L_0 = __this->get_U24PC_10();
		V_0 = L_0;
		__this->set_U24PC_10((-1));
		V_1 = (bool)0;
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_002b;
		}
		if (L_1 == 1)
		{
			goto IL_0086;
		}
		if (L_1 == 2)
		{
			goto IL_00fc;
		}
		if (L_1 == 3)
		{
			goto IL_016b;
		}
	}
	{
		goto IL_01dc;
	}

IL_002b:
	{
		int32_t L_2 = __this->get_flags_0();
		if (!((int32_t)((int32_t)L_2&(int32_t)2)))
		{
			goto IL_00ac;
		}
	}
	{
		Type_t * L_3 = __this->get_type_1();
		int32_t L_4 = __this->get_flags_0();
		NullCheck(L_3);
		FieldInfoU5BU5D_t1144794227* L_5 = VirtFuncInvoker1< FieldInfoU5BU5D_t1144794227*, int32_t >::Invoke(57 /* System.Reflection.FieldInfo[] System.Type::GetFields(System.Reflection.BindingFlags) */, L_3, L_4);
		__this->set_U3CU24s_244U3E__0_2(L_5);
		__this->set_U3CU24s_245U3E__1_3(0);
		goto IL_0094;
	}

IL_005b:
	{
		FieldInfoU5BU5D_t1144794227* L_6 = __this->get_U3CU24s_244U3E__0_2();
		int32_t L_7 = __this->get_U3CU24s_245U3E__1_3();
		NullCheck(L_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_6, L_7);
		int32_t L_8 = L_7;
		__this->set_U3CfieldInfoU3E__2_4(((L_6)->GetAt(static_cast<il2cpp_array_size_t>(L_8))));
		FieldInfo_t * L_9 = __this->get_U3CfieldInfoU3E__2_4();
		__this->set_U24current_11(L_9);
		__this->set_U24PC_10(1);
		goto IL_01de;
	}

IL_0086:
	{
		int32_t L_10 = __this->get_U3CU24s_245U3E__1_3();
		__this->set_U3CU24s_245U3E__1_3(((int32_t)((int32_t)L_10+(int32_t)1)));
	}

IL_0094:
	{
		int32_t L_11 = __this->get_U3CU24s_245U3E__1_3();
		FieldInfoU5BU5D_t1144794227* L_12 = __this->get_U3CU24s_244U3E__0_2();
		NullCheck(L_12);
		if ((((int32_t)L_11) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_12)->max_length)))))))
		{
			goto IL_005b;
		}
	}
	{
		goto IL_01d5;
	}

IL_00ac:
	{
		Type_t * L_13 = __this->get_type_1();
		int32_t L_14 = __this->get_flags_0();
		NullCheck(L_13);
		FieldInfoU5BU5D_t1144794227* L_15 = VirtFuncInvoker1< FieldInfoU5BU5D_t1144794227*, int32_t >::Invoke(57 /* System.Reflection.FieldInfo[] System.Type::GetFields(System.Reflection.BindingFlags) */, L_13, ((int32_t)((int32_t)L_14|(int32_t)2)));
		__this->set_U3CU24s_246U3E__3_5(L_15);
		__this->set_U3CU24s_247U3E__4_6(0);
		goto IL_010a;
	}

IL_00d1:
	{
		FieldInfoU5BU5D_t1144794227* L_16 = __this->get_U3CU24s_246U3E__3_5();
		int32_t L_17 = __this->get_U3CU24s_247U3E__4_6();
		NullCheck(L_16);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_16, L_17);
		int32_t L_18 = L_17;
		__this->set_U3CfieldInfoU3E__5_7(((L_16)->GetAt(static_cast<il2cpp_array_size_t>(L_18))));
		FieldInfo_t * L_19 = __this->get_U3CfieldInfoU3E__5_7();
		__this->set_U24current_11(L_19);
		__this->set_U24PC_10(2);
		goto IL_01de;
	}

IL_00fc:
	{
		int32_t L_20 = __this->get_U3CU24s_247U3E__4_6();
		__this->set_U3CU24s_247U3E__4_6(((int32_t)((int32_t)L_20+(int32_t)1)));
	}

IL_010a:
	{
		int32_t L_21 = __this->get_U3CU24s_247U3E__4_6();
		FieldInfoU5BU5D_t1144794227* L_22 = __this->get_U3CU24s_246U3E__3_5();
		NullCheck(L_22);
		if ((((int32_t)L_21) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_22)->max_length)))))))
		{
			goto IL_00d1;
		}
	}
	{
		Type_t * L_23 = __this->get_type_1();
		NullCheck(L_23);
		Type_t * L_24 = VirtFuncInvoker0< Type_t * >::Invoke(18 /* System.Type System.Type::get_BaseType() */, L_23);
		if (!L_24)
		{
			goto IL_01d5;
		}
	}
	{
		Type_t * L_25 = __this->get_type_1();
		NullCheck(L_25);
		Type_t * L_26 = VirtFuncInvoker0< Type_t * >::Invoke(18 /* System.Type System.Type::get_BaseType() */, L_25);
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_27 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, LoadTypeToken(Il2CppObject_0_0_0_var), /*hidden argument*/NULL);
		if ((((Il2CppObject*)(Type_t *)L_26) == ((Il2CppObject*)(Type_t *)L_27)))
		{
			goto IL_01d5;
		}
	}
	{
		Type_t * L_28 = __this->get_type_1();
		NullCheck(L_28);
		Type_t * L_29 = VirtFuncInvoker0< Type_t * >::Invoke(18 /* System.Type System.Type::get_BaseType() */, L_28);
		int32_t L_30 = __this->get_flags_0();
		Il2CppObject* L_31 = TypeExtensions_GetAllFields_m1768734574(NULL /*static, unused*/, L_29, L_30, /*hidden argument*/NULL);
		NullCheck(L_31);
		Il2CppObject* L_32 = InterfaceFuncInvoker0< Il2CppObject* >::Invoke(0 /* System.Collections.Generic.IEnumerator`1<!0> System.Collections.Generic.IEnumerable`1<System.Reflection.FieldInfo>::GetEnumerator() */, IEnumerable_1_t4037084138_il2cpp_TypeInfo_var, L_31);
		__this->set_U3CU24s_248U3E__6_8(L_32);
		V_0 = ((int32_t)-3);
	}

IL_016b:
	try
	{ // begin try (depth: 1)
		{
			uint32_t L_33 = V_0;
			if (((int32_t)((int32_t)L_33-(int32_t)3)) == 0)
			{
				goto IL_01a7;
			}
		}

IL_0177:
		{
			goto IL_01a7;
		}

IL_017c:
		{
			Il2CppObject* L_34 = __this->get_U3CU24s_248U3E__6_8();
			NullCheck(L_34);
			FieldInfo_t * L_35 = InterfaceFuncInvoker0< FieldInfo_t * >::Invoke(0 /* !0 System.Collections.Generic.IEnumerator`1<System.Reflection.FieldInfo>::get_Current() */, IEnumerator_1_t2648036230_il2cpp_TypeInfo_var, L_34);
			__this->set_U3CfieldInfoU3E__7_9(L_35);
			FieldInfo_t * L_36 = __this->get_U3CfieldInfoU3E__7_9();
			__this->set_U24current_11(L_36);
			__this->set_U24PC_10(3);
			V_1 = (bool)1;
			IL2CPP_LEAVE(0x1DE, FINALLY_01bc);
		}

IL_01a7:
		{
			Il2CppObject* L_37 = __this->get_U3CU24s_248U3E__6_8();
			NullCheck(L_37);
			bool L_38 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t287207039_il2cpp_TypeInfo_var, L_37);
			if (L_38)
			{
				goto IL_017c;
			}
		}

IL_01b7:
		{
			IL2CPP_LEAVE(0x1D5, FINALLY_01bc);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_01bc;
	}

FINALLY_01bc:
	{ // begin finally (depth: 1)
		{
			bool L_39 = V_1;
			if (!L_39)
			{
				goto IL_01c0;
			}
		}

IL_01bf:
		{
			IL2CPP_END_FINALLY(444)
		}

IL_01c0:
		{
			Il2CppObject* L_40 = __this->get_U3CU24s_248U3E__6_8();
			if (L_40)
			{
				goto IL_01c9;
			}
		}

IL_01c8:
		{
			IL2CPP_END_FINALLY(444)
		}

IL_01c9:
		{
			Il2CppObject* L_41 = __this->get_U3CU24s_248U3E__6_8();
			NullCheck(L_41);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, L_41);
			IL2CPP_END_FINALLY(444)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(444)
	{
		IL2CPP_JUMP_TBL(0x1DE, IL_01de)
		IL2CPP_JUMP_TBL(0x1D5, IL_01d5)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_01d5:
	{
		__this->set_U24PC_10((-1));
	}

IL_01dc:
	{
		return (bool)0;
	}

IL_01de:
	{
		return (bool)1;
	}
	// Dead block : IL_01e0: ldloc.2
}
// System.Void ModestTree.TypeExtensions/<GetAllFields>c__Iterator3F::Dispose()
extern Il2CppClass* IDisposable_t1628921374_il2cpp_TypeInfo_var;
extern const uint32_t U3CGetAllFieldsU3Ec__Iterator3F_Dispose_m3720344151_MetadataUsageId;
extern "C"  void U3CGetAllFieldsU3Ec__Iterator3F_Dispose_m3720344151 (U3CGetAllFieldsU3Ec__Iterator3F_t219406118 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CGetAllFieldsU3Ec__Iterator3F_Dispose_m3720344151_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	uint32_t V_0 = 0;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		int32_t L_0 = __this->get_U24PC_10();
		V_0 = L_0;
		__this->set_U24PC_10((-1));
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0043;
		}
		if (L_1 == 1)
		{
			goto IL_0043;
		}
		if (L_1 == 2)
		{
			goto IL_0043;
		}
		if (L_1 == 3)
		{
			goto IL_0029;
		}
	}
	{
		goto IL_0043;
	}

IL_0029:
	try
	{ // begin try (depth: 1)
		IL2CPP_LEAVE(0x43, FINALLY_002e);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_002e;
	}

FINALLY_002e:
	{ // begin finally (depth: 1)
		{
			Il2CppObject* L_2 = __this->get_U3CU24s_248U3E__6_8();
			if (L_2)
			{
				goto IL_0037;
			}
		}

IL_0036:
		{
			IL2CPP_END_FINALLY(46)
		}

IL_0037:
		{
			Il2CppObject* L_3 = __this->get_U3CU24s_248U3E__6_8();
			NullCheck(L_3);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, L_3);
			IL2CPP_END_FINALLY(46)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(46)
	{
		IL2CPP_JUMP_TBL(0x43, IL_0043)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0043:
	{
		return;
	}
}
// System.Void ModestTree.TypeExtensions/<GetAllFields>c__Iterator3F::Reset()
extern Il2CppClass* NotSupportedException_t1374155497_il2cpp_TypeInfo_var;
extern const uint32_t U3CGetAllFieldsU3Ec__Iterator3F_Reset_m3089510215_MetadataUsageId;
extern "C"  void U3CGetAllFieldsU3Ec__Iterator3F_Reset_m3089510215 (U3CGetAllFieldsU3Ec__Iterator3F_t219406118 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CGetAllFieldsU3Ec__Iterator3F_Reset_m3089510215_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1374155497 * L_0 = (NotSupportedException_t1374155497 *)il2cpp_codegen_object_new(NotSupportedException_t1374155497_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m149930845(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void ModestTree.TypeExtensions/<GetAllMethods>c__Iterator41::.ctor()
extern "C"  void U3CGetAllMethodsU3Ec__Iterator41__ctor_m473758091 (U3CGetAllMethodsU3Ec__Iterator41_t3371665531 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Reflection.MethodInfo ModestTree.TypeExtensions/<GetAllMethods>c__Iterator41::System.Collections.Generic.IEnumerator<System.Reflection.MethodInfo>.get_Current()
extern "C"  MethodInfo_t * U3CGetAllMethodsU3Ec__Iterator41_System_Collections_Generic_IEnumeratorU3CSystem_Reflection_MethodInfoU3E_get_Current_m281929138 (U3CGetAllMethodsU3Ec__Iterator41_t3371665531 * __this, const MethodInfo* method)
{
	{
		MethodInfo_t * L_0 = __this->get_U24current_11();
		return L_0;
	}
}
// System.Object ModestTree.TypeExtensions/<GetAllMethods>c__Iterator41::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CGetAllMethodsU3Ec__Iterator41_System_Collections_IEnumerator_get_Current_m38826245 (U3CGetAllMethodsU3Ec__Iterator41_t3371665531 * __this, const MethodInfo* method)
{
	{
		MethodInfo_t * L_0 = __this->get_U24current_11();
		return L_0;
	}
}
// System.Collections.IEnumerator ModestTree.TypeExtensions/<GetAllMethods>c__Iterator41::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * U3CGetAllMethodsU3Ec__Iterator41_System_Collections_IEnumerable_GetEnumerator_m611982086 (U3CGetAllMethodsU3Ec__Iterator41_t3371665531 * __this, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = U3CGetAllMethodsU3Ec__Iterator41_System_Collections_Generic_IEnumerableU3CSystem_Reflection_MethodInfoU3E_GetEnumerator_m1670105365(__this, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Collections.Generic.IEnumerator`1<System.Reflection.MethodInfo> ModestTree.TypeExtensions/<GetAllMethods>c__Iterator41::System.Collections.Generic.IEnumerable<System.Reflection.MethodInfo>.GetEnumerator()
extern Il2CppClass* U3CGetAllMethodsU3Ec__Iterator41_t3371665531_il2cpp_TypeInfo_var;
extern const uint32_t U3CGetAllMethodsU3Ec__Iterator41_System_Collections_Generic_IEnumerableU3CSystem_Reflection_MethodInfoU3E_GetEnumerator_m1670105365_MetadataUsageId;
extern "C"  Il2CppObject* U3CGetAllMethodsU3Ec__Iterator41_System_Collections_Generic_IEnumerableU3CSystem_Reflection_MethodInfoU3E_GetEnumerator_m1670105365 (U3CGetAllMethodsU3Ec__Iterator41_t3371665531 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CGetAllMethodsU3Ec__Iterator41_System_Collections_Generic_IEnumerableU3CSystem_Reflection_MethodInfoU3E_GetEnumerator_m1670105365_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	U3CGetAllMethodsU3Ec__Iterator41_t3371665531 * V_0 = NULL;
	{
		int32_t* L_0 = __this->get_address_of_U24PC_10();
		int32_t L_1 = Interlocked_CompareExchange_m1859820752(NULL /*static, unused*/, L_0, 0, ((int32_t)-2), /*hidden argument*/NULL);
		if ((!(((uint32_t)L_1) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0014;
		}
	}
	{
		return __this;
	}

IL_0014:
	{
		U3CGetAllMethodsU3Ec__Iterator41_t3371665531 * L_2 = (U3CGetAllMethodsU3Ec__Iterator41_t3371665531 *)il2cpp_codegen_object_new(U3CGetAllMethodsU3Ec__Iterator41_t3371665531_il2cpp_TypeInfo_var);
		U3CGetAllMethodsU3Ec__Iterator41__ctor_m473758091(L_2, /*hidden argument*/NULL);
		V_0 = L_2;
		U3CGetAllMethodsU3Ec__Iterator41_t3371665531 * L_3 = V_0;
		int32_t L_4 = __this->get_U3CU24U3Eflags_12();
		NullCheck(L_3);
		L_3->set_flags_0(L_4);
		U3CGetAllMethodsU3Ec__Iterator41_t3371665531 * L_5 = V_0;
		Type_t * L_6 = __this->get_U3CU24U3Etype_13();
		NullCheck(L_5);
		L_5->set_type_1(L_6);
		U3CGetAllMethodsU3Ec__Iterator41_t3371665531 * L_7 = V_0;
		return L_7;
	}
}
// System.Boolean ModestTree.TypeExtensions/<GetAllMethods>c__Iterator41::MoveNext()
extern const Il2CppType* Il2CppObject_0_0_0_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern Il2CppClass* IEnumerable_1_t2038408337_il2cpp_TypeInfo_var;
extern Il2CppClass* IEnumerator_1_t649360429_il2cpp_TypeInfo_var;
extern Il2CppClass* IEnumerator_t287207039_il2cpp_TypeInfo_var;
extern Il2CppClass* IDisposable_t1628921374_il2cpp_TypeInfo_var;
extern const uint32_t U3CGetAllMethodsU3Ec__Iterator41_MoveNext_m3912088945_MetadataUsageId;
extern "C"  bool U3CGetAllMethodsU3Ec__Iterator41_MoveNext_m3912088945 (U3CGetAllMethodsU3Ec__Iterator41_t3371665531 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CGetAllMethodsU3Ec__Iterator41_MoveNext_m3912088945_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	uint32_t V_0 = 0;
	bool V_1 = false;
	bool V_2 = false;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		int32_t L_0 = __this->get_U24PC_10();
		V_0 = L_0;
		__this->set_U24PC_10((-1));
		V_1 = (bool)0;
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_002b;
		}
		if (L_1 == 1)
		{
			goto IL_0086;
		}
		if (L_1 == 2)
		{
			goto IL_00fc;
		}
		if (L_1 == 3)
		{
			goto IL_016b;
		}
	}
	{
		goto IL_01dc;
	}

IL_002b:
	{
		int32_t L_2 = __this->get_flags_0();
		if (!((int32_t)((int32_t)L_2&(int32_t)2)))
		{
			goto IL_00ac;
		}
	}
	{
		Type_t * L_3 = __this->get_type_1();
		int32_t L_4 = __this->get_flags_0();
		NullCheck(L_3);
		MethodInfoU5BU5D_t1668237648* L_5 = VirtFuncInvoker1< MethodInfoU5BU5D_t1668237648*, int32_t >::Invoke(66 /* System.Reflection.MethodInfo[] System.Type::GetMethods(System.Reflection.BindingFlags) */, L_3, L_4);
		__this->set_U3CU24s_254U3E__0_2(L_5);
		__this->set_U3CU24s_255U3E__1_3(0);
		goto IL_0094;
	}

IL_005b:
	{
		MethodInfoU5BU5D_t1668237648* L_6 = __this->get_U3CU24s_254U3E__0_2();
		int32_t L_7 = __this->get_U3CU24s_255U3E__1_3();
		NullCheck(L_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_6, L_7);
		int32_t L_8 = L_7;
		__this->set_U3CmethodInfoU3E__2_4(((L_6)->GetAt(static_cast<il2cpp_array_size_t>(L_8))));
		MethodInfo_t * L_9 = __this->get_U3CmethodInfoU3E__2_4();
		__this->set_U24current_11(L_9);
		__this->set_U24PC_10(1);
		goto IL_01de;
	}

IL_0086:
	{
		int32_t L_10 = __this->get_U3CU24s_255U3E__1_3();
		__this->set_U3CU24s_255U3E__1_3(((int32_t)((int32_t)L_10+(int32_t)1)));
	}

IL_0094:
	{
		int32_t L_11 = __this->get_U3CU24s_255U3E__1_3();
		MethodInfoU5BU5D_t1668237648* L_12 = __this->get_U3CU24s_254U3E__0_2();
		NullCheck(L_12);
		if ((((int32_t)L_11) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_12)->max_length)))))))
		{
			goto IL_005b;
		}
	}
	{
		goto IL_01d5;
	}

IL_00ac:
	{
		Type_t * L_13 = __this->get_type_1();
		int32_t L_14 = __this->get_flags_0();
		NullCheck(L_13);
		MethodInfoU5BU5D_t1668237648* L_15 = VirtFuncInvoker1< MethodInfoU5BU5D_t1668237648*, int32_t >::Invoke(66 /* System.Reflection.MethodInfo[] System.Type::GetMethods(System.Reflection.BindingFlags) */, L_13, ((int32_t)((int32_t)L_14|(int32_t)2)));
		__this->set_U3CU24s_256U3E__3_5(L_15);
		__this->set_U3CU24s_257U3E__4_6(0);
		goto IL_010a;
	}

IL_00d1:
	{
		MethodInfoU5BU5D_t1668237648* L_16 = __this->get_U3CU24s_256U3E__3_5();
		int32_t L_17 = __this->get_U3CU24s_257U3E__4_6();
		NullCheck(L_16);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_16, L_17);
		int32_t L_18 = L_17;
		__this->set_U3CmethodInfoU3E__5_7(((L_16)->GetAt(static_cast<il2cpp_array_size_t>(L_18))));
		MethodInfo_t * L_19 = __this->get_U3CmethodInfoU3E__5_7();
		__this->set_U24current_11(L_19);
		__this->set_U24PC_10(2);
		goto IL_01de;
	}

IL_00fc:
	{
		int32_t L_20 = __this->get_U3CU24s_257U3E__4_6();
		__this->set_U3CU24s_257U3E__4_6(((int32_t)((int32_t)L_20+(int32_t)1)));
	}

IL_010a:
	{
		int32_t L_21 = __this->get_U3CU24s_257U3E__4_6();
		MethodInfoU5BU5D_t1668237648* L_22 = __this->get_U3CU24s_256U3E__3_5();
		NullCheck(L_22);
		if ((((int32_t)L_21) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_22)->max_length)))))))
		{
			goto IL_00d1;
		}
	}
	{
		Type_t * L_23 = __this->get_type_1();
		NullCheck(L_23);
		Type_t * L_24 = VirtFuncInvoker0< Type_t * >::Invoke(18 /* System.Type System.Type::get_BaseType() */, L_23);
		if (!L_24)
		{
			goto IL_01d5;
		}
	}
	{
		Type_t * L_25 = __this->get_type_1();
		NullCheck(L_25);
		Type_t * L_26 = VirtFuncInvoker0< Type_t * >::Invoke(18 /* System.Type System.Type::get_BaseType() */, L_25);
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_27 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, LoadTypeToken(Il2CppObject_0_0_0_var), /*hidden argument*/NULL);
		if ((((Il2CppObject*)(Type_t *)L_26) == ((Il2CppObject*)(Type_t *)L_27)))
		{
			goto IL_01d5;
		}
	}
	{
		Type_t * L_28 = __this->get_type_1();
		NullCheck(L_28);
		Type_t * L_29 = VirtFuncInvoker0< Type_t * >::Invoke(18 /* System.Type System.Type::get_BaseType() */, L_28);
		int32_t L_30 = __this->get_flags_0();
		Il2CppObject* L_31 = TypeExtensions_GetAllMethods_m2967502904(NULL /*static, unused*/, L_29, L_30, /*hidden argument*/NULL);
		NullCheck(L_31);
		Il2CppObject* L_32 = InterfaceFuncInvoker0< Il2CppObject* >::Invoke(0 /* System.Collections.Generic.IEnumerator`1<!0> System.Collections.Generic.IEnumerable`1<System.Reflection.MethodInfo>::GetEnumerator() */, IEnumerable_1_t2038408337_il2cpp_TypeInfo_var, L_31);
		__this->set_U3CU24s_258U3E__6_8(L_32);
		V_0 = ((int32_t)-3);
	}

IL_016b:
	try
	{ // begin try (depth: 1)
		{
			uint32_t L_33 = V_0;
			if (((int32_t)((int32_t)L_33-(int32_t)3)) == 0)
			{
				goto IL_01a7;
			}
		}

IL_0177:
		{
			goto IL_01a7;
		}

IL_017c:
		{
			Il2CppObject* L_34 = __this->get_U3CU24s_258U3E__6_8();
			NullCheck(L_34);
			MethodInfo_t * L_35 = InterfaceFuncInvoker0< MethodInfo_t * >::Invoke(0 /* !0 System.Collections.Generic.IEnumerator`1<System.Reflection.MethodInfo>::get_Current() */, IEnumerator_1_t649360429_il2cpp_TypeInfo_var, L_34);
			__this->set_U3CmethodInfoU3E__7_9(L_35);
			MethodInfo_t * L_36 = __this->get_U3CmethodInfoU3E__7_9();
			__this->set_U24current_11(L_36);
			__this->set_U24PC_10(3);
			V_1 = (bool)1;
			IL2CPP_LEAVE(0x1DE, FINALLY_01bc);
		}

IL_01a7:
		{
			Il2CppObject* L_37 = __this->get_U3CU24s_258U3E__6_8();
			NullCheck(L_37);
			bool L_38 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t287207039_il2cpp_TypeInfo_var, L_37);
			if (L_38)
			{
				goto IL_017c;
			}
		}

IL_01b7:
		{
			IL2CPP_LEAVE(0x1D5, FINALLY_01bc);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_01bc;
	}

FINALLY_01bc:
	{ // begin finally (depth: 1)
		{
			bool L_39 = V_1;
			if (!L_39)
			{
				goto IL_01c0;
			}
		}

IL_01bf:
		{
			IL2CPP_END_FINALLY(444)
		}

IL_01c0:
		{
			Il2CppObject* L_40 = __this->get_U3CU24s_258U3E__6_8();
			if (L_40)
			{
				goto IL_01c9;
			}
		}

IL_01c8:
		{
			IL2CPP_END_FINALLY(444)
		}

IL_01c9:
		{
			Il2CppObject* L_41 = __this->get_U3CU24s_258U3E__6_8();
			NullCheck(L_41);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, L_41);
			IL2CPP_END_FINALLY(444)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(444)
	{
		IL2CPP_JUMP_TBL(0x1DE, IL_01de)
		IL2CPP_JUMP_TBL(0x1D5, IL_01d5)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_01d5:
	{
		__this->set_U24PC_10((-1));
	}

IL_01dc:
	{
		return (bool)0;
	}

IL_01de:
	{
		return (bool)1;
	}
	// Dead block : IL_01e0: ldloc.2
}
// System.Void ModestTree.TypeExtensions/<GetAllMethods>c__Iterator41::Dispose()
extern Il2CppClass* IDisposable_t1628921374_il2cpp_TypeInfo_var;
extern const uint32_t U3CGetAllMethodsU3Ec__Iterator41_Dispose_m4208242440_MetadataUsageId;
extern "C"  void U3CGetAllMethodsU3Ec__Iterator41_Dispose_m4208242440 (U3CGetAllMethodsU3Ec__Iterator41_t3371665531 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CGetAllMethodsU3Ec__Iterator41_Dispose_m4208242440_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	uint32_t V_0 = 0;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		int32_t L_0 = __this->get_U24PC_10();
		V_0 = L_0;
		__this->set_U24PC_10((-1));
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0043;
		}
		if (L_1 == 1)
		{
			goto IL_0043;
		}
		if (L_1 == 2)
		{
			goto IL_0043;
		}
		if (L_1 == 3)
		{
			goto IL_0029;
		}
	}
	{
		goto IL_0043;
	}

IL_0029:
	try
	{ // begin try (depth: 1)
		IL2CPP_LEAVE(0x43, FINALLY_002e);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_002e;
	}

FINALLY_002e:
	{ // begin finally (depth: 1)
		{
			Il2CppObject* L_2 = __this->get_U3CU24s_258U3E__6_8();
			if (L_2)
			{
				goto IL_0037;
			}
		}

IL_0036:
		{
			IL2CPP_END_FINALLY(46)
		}

IL_0037:
		{
			Il2CppObject* L_3 = __this->get_U3CU24s_258U3E__6_8();
			NullCheck(L_3);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, L_3);
			IL2CPP_END_FINALLY(46)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(46)
	{
		IL2CPP_JUMP_TBL(0x43, IL_0043)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0043:
	{
		return;
	}
}
// System.Void ModestTree.TypeExtensions/<GetAllMethods>c__Iterator41::Reset()
extern Il2CppClass* NotSupportedException_t1374155497_il2cpp_TypeInfo_var;
extern const uint32_t U3CGetAllMethodsU3Ec__Iterator41_Reset_m2415158328_MetadataUsageId;
extern "C"  void U3CGetAllMethodsU3Ec__Iterator41_Reset_m2415158328 (U3CGetAllMethodsU3Ec__Iterator41_t3371665531 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CGetAllMethodsU3Ec__Iterator41_Reset_m2415158328_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1374155497 * L_0 = (NotSupportedException_t1374155497 *)il2cpp_codegen_object_new(NotSupportedException_t1374155497_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m149930845(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void ModestTree.TypeExtensions/<GetAllProperties>c__Iterator40::.ctor()
extern "C"  void U3CGetAllPropertiesU3Ec__Iterator40__ctor_m2990873591 (U3CGetAllPropertiesU3Ec__Iterator40_t3557131433 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Reflection.PropertyInfo ModestTree.TypeExtensions/<GetAllProperties>c__Iterator40::System.Collections.Generic.IEnumerator<System.Reflection.PropertyInfo>.get_Current()
extern "C"  PropertyInfo_t * U3CGetAllPropertiesU3Ec__Iterator40_System_Collections_Generic_IEnumeratorU3CSystem_Reflection_PropertyInfoU3E_get_Current_m1430794738 (U3CGetAllPropertiesU3Ec__Iterator40_t3557131433 * __this, const MethodInfo* method)
{
	{
		PropertyInfo_t * L_0 = __this->get_U24current_11();
		return L_0;
	}
}
// System.Object ModestTree.TypeExtensions/<GetAllProperties>c__Iterator40::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CGetAllPropertiesU3Ec__Iterator40_System_Collections_IEnumerator_get_Current_m4088692111 (U3CGetAllPropertiesU3Ec__Iterator40_t3557131433 * __this, const MethodInfo* method)
{
	{
		PropertyInfo_t * L_0 = __this->get_U24current_11();
		return L_0;
	}
}
// System.Collections.IEnumerator ModestTree.TypeExtensions/<GetAllProperties>c__Iterator40::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * U3CGetAllPropertiesU3Ec__Iterator40_System_Collections_IEnumerable_GetEnumerator_m1184006538 (U3CGetAllPropertiesU3Ec__Iterator40_t3557131433 * __this, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = U3CGetAllPropertiesU3Ec__Iterator40_System_Collections_Generic_IEnumerableU3CSystem_Reflection_PropertyInfoU3E_GetEnumerator_m3135099747(__this, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Collections.Generic.IEnumerator`1<System.Reflection.PropertyInfo> ModestTree.TypeExtensions/<GetAllProperties>c__Iterator40::System.Collections.Generic.IEnumerable<System.Reflection.PropertyInfo>.GetEnumerator()
extern Il2CppClass* U3CGetAllPropertiesU3Ec__Iterator40_t3557131433_il2cpp_TypeInfo_var;
extern const uint32_t U3CGetAllPropertiesU3Ec__Iterator40_System_Collections_Generic_IEnumerableU3CSystem_Reflection_PropertyInfoU3E_GetEnumerator_m3135099747_MetadataUsageId;
extern "C"  Il2CppObject* U3CGetAllPropertiesU3Ec__Iterator40_System_Collections_Generic_IEnumerableU3CSystem_Reflection_PropertyInfoU3E_GetEnumerator_m3135099747 (U3CGetAllPropertiesU3Ec__Iterator40_t3557131433 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CGetAllPropertiesU3Ec__Iterator40_System_Collections_Generic_IEnumerableU3CSystem_Reflection_PropertyInfoU3E_GetEnumerator_m3135099747_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	U3CGetAllPropertiesU3Ec__Iterator40_t3557131433 * V_0 = NULL;
	{
		int32_t* L_0 = __this->get_address_of_U24PC_10();
		int32_t L_1 = Interlocked_CompareExchange_m1859820752(NULL /*static, unused*/, L_0, 0, ((int32_t)-2), /*hidden argument*/NULL);
		if ((!(((uint32_t)L_1) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0014;
		}
	}
	{
		return __this;
	}

IL_0014:
	{
		U3CGetAllPropertiesU3Ec__Iterator40_t3557131433 * L_2 = (U3CGetAllPropertiesU3Ec__Iterator40_t3557131433 *)il2cpp_codegen_object_new(U3CGetAllPropertiesU3Ec__Iterator40_t3557131433_il2cpp_TypeInfo_var);
		U3CGetAllPropertiesU3Ec__Iterator40__ctor_m2990873591(L_2, /*hidden argument*/NULL);
		V_0 = L_2;
		U3CGetAllPropertiesU3Ec__Iterator40_t3557131433 * L_3 = V_0;
		int32_t L_4 = __this->get_U3CU24U3Eflags_12();
		NullCheck(L_3);
		L_3->set_flags_0(L_4);
		U3CGetAllPropertiesU3Ec__Iterator40_t3557131433 * L_5 = V_0;
		Type_t * L_6 = __this->get_U3CU24U3Etype_13();
		NullCheck(L_5);
		L_5->set_type_1(L_6);
		U3CGetAllPropertiesU3Ec__Iterator40_t3557131433 * L_7 = V_0;
		return L_7;
	}
}
// System.Boolean ModestTree.TypeExtensions/<GetAllProperties>c__Iterator40::MoveNext()
extern const Il2CppType* Il2CppObject_0_0_0_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern Il2CppClass* IEnumerable_1_t67735429_il2cpp_TypeInfo_var;
extern Il2CppClass* IEnumerator_1_t2973654817_il2cpp_TypeInfo_var;
extern Il2CppClass* IEnumerator_t287207039_il2cpp_TypeInfo_var;
extern Il2CppClass* IDisposable_t1628921374_il2cpp_TypeInfo_var;
extern const uint32_t U3CGetAllPropertiesU3Ec__Iterator40_MoveNext_m1070121885_MetadataUsageId;
extern "C"  bool U3CGetAllPropertiesU3Ec__Iterator40_MoveNext_m1070121885 (U3CGetAllPropertiesU3Ec__Iterator40_t3557131433 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CGetAllPropertiesU3Ec__Iterator40_MoveNext_m1070121885_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	uint32_t V_0 = 0;
	bool V_1 = false;
	bool V_2 = false;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		int32_t L_0 = __this->get_U24PC_10();
		V_0 = L_0;
		__this->set_U24PC_10((-1));
		V_1 = (bool)0;
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_002b;
		}
		if (L_1 == 1)
		{
			goto IL_0086;
		}
		if (L_1 == 2)
		{
			goto IL_00fc;
		}
		if (L_1 == 3)
		{
			goto IL_016b;
		}
	}
	{
		goto IL_01dc;
	}

IL_002b:
	{
		int32_t L_2 = __this->get_flags_0();
		if (!((int32_t)((int32_t)L_2&(int32_t)2)))
		{
			goto IL_00ac;
		}
	}
	{
		Type_t * L_3 = __this->get_type_1();
		int32_t L_4 = __this->get_flags_0();
		NullCheck(L_3);
		PropertyInfoU5BU5D_t1348579340* L_5 = VirtFuncInvoker1< PropertyInfoU5BU5D_t1348579340*, int32_t >::Invoke(69 /* System.Reflection.PropertyInfo[] System.Type::GetProperties(System.Reflection.BindingFlags) */, L_3, L_4);
		__this->set_U3CU24s_249U3E__0_2(L_5);
		__this->set_U3CU24s_250U3E__1_3(0);
		goto IL_0094;
	}

IL_005b:
	{
		PropertyInfoU5BU5D_t1348579340* L_6 = __this->get_U3CU24s_249U3E__0_2();
		int32_t L_7 = __this->get_U3CU24s_250U3E__1_3();
		NullCheck(L_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_6, L_7);
		int32_t L_8 = L_7;
		__this->set_U3CpropertyInfoU3E__2_4(((L_6)->GetAt(static_cast<il2cpp_array_size_t>(L_8))));
		PropertyInfo_t * L_9 = __this->get_U3CpropertyInfoU3E__2_4();
		__this->set_U24current_11(L_9);
		__this->set_U24PC_10(1);
		goto IL_01de;
	}

IL_0086:
	{
		int32_t L_10 = __this->get_U3CU24s_250U3E__1_3();
		__this->set_U3CU24s_250U3E__1_3(((int32_t)((int32_t)L_10+(int32_t)1)));
	}

IL_0094:
	{
		int32_t L_11 = __this->get_U3CU24s_250U3E__1_3();
		PropertyInfoU5BU5D_t1348579340* L_12 = __this->get_U3CU24s_249U3E__0_2();
		NullCheck(L_12);
		if ((((int32_t)L_11) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_12)->max_length)))))))
		{
			goto IL_005b;
		}
	}
	{
		goto IL_01d5;
	}

IL_00ac:
	{
		Type_t * L_13 = __this->get_type_1();
		int32_t L_14 = __this->get_flags_0();
		NullCheck(L_13);
		PropertyInfoU5BU5D_t1348579340* L_15 = VirtFuncInvoker1< PropertyInfoU5BU5D_t1348579340*, int32_t >::Invoke(69 /* System.Reflection.PropertyInfo[] System.Type::GetProperties(System.Reflection.BindingFlags) */, L_13, ((int32_t)((int32_t)L_14|(int32_t)2)));
		__this->set_U3CU24s_251U3E__3_5(L_15);
		__this->set_U3CU24s_252U3E__4_6(0);
		goto IL_010a;
	}

IL_00d1:
	{
		PropertyInfoU5BU5D_t1348579340* L_16 = __this->get_U3CU24s_251U3E__3_5();
		int32_t L_17 = __this->get_U3CU24s_252U3E__4_6();
		NullCheck(L_16);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_16, L_17);
		int32_t L_18 = L_17;
		__this->set_U3CpropertyInfoU3E__5_7(((L_16)->GetAt(static_cast<il2cpp_array_size_t>(L_18))));
		PropertyInfo_t * L_19 = __this->get_U3CpropertyInfoU3E__5_7();
		__this->set_U24current_11(L_19);
		__this->set_U24PC_10(2);
		goto IL_01de;
	}

IL_00fc:
	{
		int32_t L_20 = __this->get_U3CU24s_252U3E__4_6();
		__this->set_U3CU24s_252U3E__4_6(((int32_t)((int32_t)L_20+(int32_t)1)));
	}

IL_010a:
	{
		int32_t L_21 = __this->get_U3CU24s_252U3E__4_6();
		PropertyInfoU5BU5D_t1348579340* L_22 = __this->get_U3CU24s_251U3E__3_5();
		NullCheck(L_22);
		if ((((int32_t)L_21) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_22)->max_length)))))))
		{
			goto IL_00d1;
		}
	}
	{
		Type_t * L_23 = __this->get_type_1();
		NullCheck(L_23);
		Type_t * L_24 = VirtFuncInvoker0< Type_t * >::Invoke(18 /* System.Type System.Type::get_BaseType() */, L_23);
		if (!L_24)
		{
			goto IL_01d5;
		}
	}
	{
		Type_t * L_25 = __this->get_type_1();
		NullCheck(L_25);
		Type_t * L_26 = VirtFuncInvoker0< Type_t * >::Invoke(18 /* System.Type System.Type::get_BaseType() */, L_25);
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_27 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, LoadTypeToken(Il2CppObject_0_0_0_var), /*hidden argument*/NULL);
		if ((((Il2CppObject*)(Type_t *)L_26) == ((Il2CppObject*)(Type_t *)L_27)))
		{
			goto IL_01d5;
		}
	}
	{
		Type_t * L_28 = __this->get_type_1();
		NullCheck(L_28);
		Type_t * L_29 = VirtFuncInvoker0< Type_t * >::Invoke(18 /* System.Type System.Type::get_BaseType() */, L_28);
		int32_t L_30 = __this->get_flags_0();
		Il2CppObject* L_31 = TypeExtensions_GetAllProperties_m3364896073(NULL /*static, unused*/, L_29, L_30, /*hidden argument*/NULL);
		NullCheck(L_31);
		Il2CppObject* L_32 = InterfaceFuncInvoker0< Il2CppObject* >::Invoke(0 /* System.Collections.Generic.IEnumerator`1<!0> System.Collections.Generic.IEnumerable`1<System.Reflection.PropertyInfo>::GetEnumerator() */, IEnumerable_1_t67735429_il2cpp_TypeInfo_var, L_31);
		__this->set_U3CU24s_253U3E__6_8(L_32);
		V_0 = ((int32_t)-3);
	}

IL_016b:
	try
	{ // begin try (depth: 1)
		{
			uint32_t L_33 = V_0;
			if (((int32_t)((int32_t)L_33-(int32_t)3)) == 0)
			{
				goto IL_01a7;
			}
		}

IL_0177:
		{
			goto IL_01a7;
		}

IL_017c:
		{
			Il2CppObject* L_34 = __this->get_U3CU24s_253U3E__6_8();
			NullCheck(L_34);
			PropertyInfo_t * L_35 = InterfaceFuncInvoker0< PropertyInfo_t * >::Invoke(0 /* !0 System.Collections.Generic.IEnumerator`1<System.Reflection.PropertyInfo>::get_Current() */, IEnumerator_1_t2973654817_il2cpp_TypeInfo_var, L_34);
			__this->set_U3CpropertyInfoU3E__7_9(L_35);
			PropertyInfo_t * L_36 = __this->get_U3CpropertyInfoU3E__7_9();
			__this->set_U24current_11(L_36);
			__this->set_U24PC_10(3);
			V_1 = (bool)1;
			IL2CPP_LEAVE(0x1DE, FINALLY_01bc);
		}

IL_01a7:
		{
			Il2CppObject* L_37 = __this->get_U3CU24s_253U3E__6_8();
			NullCheck(L_37);
			bool L_38 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t287207039_il2cpp_TypeInfo_var, L_37);
			if (L_38)
			{
				goto IL_017c;
			}
		}

IL_01b7:
		{
			IL2CPP_LEAVE(0x1D5, FINALLY_01bc);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_01bc;
	}

FINALLY_01bc:
	{ // begin finally (depth: 1)
		{
			bool L_39 = V_1;
			if (!L_39)
			{
				goto IL_01c0;
			}
		}

IL_01bf:
		{
			IL2CPP_END_FINALLY(444)
		}

IL_01c0:
		{
			Il2CppObject* L_40 = __this->get_U3CU24s_253U3E__6_8();
			if (L_40)
			{
				goto IL_01c9;
			}
		}

IL_01c8:
		{
			IL2CPP_END_FINALLY(444)
		}

IL_01c9:
		{
			Il2CppObject* L_41 = __this->get_U3CU24s_253U3E__6_8();
			NullCheck(L_41);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, L_41);
			IL2CPP_END_FINALLY(444)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(444)
	{
		IL2CPP_JUMP_TBL(0x1DE, IL_01de)
		IL2CPP_JUMP_TBL(0x1D5, IL_01d5)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_01d5:
	{
		__this->set_U24PC_10((-1));
	}

IL_01dc:
	{
		return (bool)0;
	}

IL_01de:
	{
		return (bool)1;
	}
	// Dead block : IL_01e0: ldloc.2
}
// System.Void ModestTree.TypeExtensions/<GetAllProperties>c__Iterator40::Dispose()
extern Il2CppClass* IDisposable_t1628921374_il2cpp_TypeInfo_var;
extern const uint32_t U3CGetAllPropertiesU3Ec__Iterator40_Dispose_m794682996_MetadataUsageId;
extern "C"  void U3CGetAllPropertiesU3Ec__Iterator40_Dispose_m794682996 (U3CGetAllPropertiesU3Ec__Iterator40_t3557131433 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CGetAllPropertiesU3Ec__Iterator40_Dispose_m794682996_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	uint32_t V_0 = 0;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		int32_t L_0 = __this->get_U24PC_10();
		V_0 = L_0;
		__this->set_U24PC_10((-1));
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0043;
		}
		if (L_1 == 1)
		{
			goto IL_0043;
		}
		if (L_1 == 2)
		{
			goto IL_0043;
		}
		if (L_1 == 3)
		{
			goto IL_0029;
		}
	}
	{
		goto IL_0043;
	}

IL_0029:
	try
	{ // begin try (depth: 1)
		IL2CPP_LEAVE(0x43, FINALLY_002e);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_002e;
	}

FINALLY_002e:
	{ // begin finally (depth: 1)
		{
			Il2CppObject* L_2 = __this->get_U3CU24s_253U3E__6_8();
			if (L_2)
			{
				goto IL_0037;
			}
		}

IL_0036:
		{
			IL2CPP_END_FINALLY(46)
		}

IL_0037:
		{
			Il2CppObject* L_3 = __this->get_U3CU24s_253U3E__6_8();
			NullCheck(L_3);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, L_3);
			IL2CPP_END_FINALLY(46)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(46)
	{
		IL2CPP_JUMP_TBL(0x43, IL_0043)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0043:
	{
		return;
	}
}
// System.Void ModestTree.TypeExtensions/<GetAllProperties>c__Iterator40::Reset()
extern Il2CppClass* NotSupportedException_t1374155497_il2cpp_TypeInfo_var;
extern const uint32_t U3CGetAllPropertiesU3Ec__Iterator40_Reset_m637306532_MetadataUsageId;
extern "C"  void U3CGetAllPropertiesU3Ec__Iterator40_Reset_m637306532 (U3CGetAllPropertiesU3Ec__Iterator40_t3557131433 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CGetAllPropertiesU3Ec__Iterator40_Reset_m637306532_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1374155497 * L_0 = (NotSupportedException_t1374155497 *)il2cpp_codegen_object_new(NotSupportedException_t1374155497_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m149930845(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void ModestTree.TypeExtensions/<GetParentTypes>c__Iterator3E::.ctor()
extern "C"  void U3CGetParentTypesU3Ec__Iterator3E__ctor_m2754063334 (U3CGetParentTypesU3Ec__Iterator3E_t1503887130 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Type ModestTree.TypeExtensions/<GetParentTypes>c__Iterator3E::System.Collections.Generic.IEnumerator<System.Type>.get_Current()
extern "C"  Type_t * U3CGetParentTypesU3Ec__Iterator3E_System_Collections_Generic_IEnumeratorU3CSystem_TypeU3E_get_Current_m3890211299 (U3CGetParentTypesU3Ec__Iterator3E_t1503887130 * __this, const MethodInfo* method)
{
	{
		Type_t * L_0 = __this->get_U24current_4();
		return L_0;
	}
}
// System.Object ModestTree.TypeExtensions/<GetParentTypes>c__Iterator3E::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CGetParentTypesU3Ec__Iterator3E_System_Collections_IEnumerator_get_Current_m2620388800 (U3CGetParentTypesU3Ec__Iterator3E_t1503887130 * __this, const MethodInfo* method)
{
	{
		Type_t * L_0 = __this->get_U24current_4();
		return L_0;
	}
}
// System.Collections.IEnumerator ModestTree.TypeExtensions/<GetParentTypes>c__Iterator3E::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * U3CGetParentTypesU3Ec__Iterator3E_System_Collections_IEnumerable_GetEnumerator_m2094940347 (U3CGetParentTypesU3Ec__Iterator3E_t1503887130 * __this, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = U3CGetParentTypesU3Ec__Iterator3E_System_Collections_Generic_IEnumerableU3CSystem_TypeU3E_GetEnumerator_m2282256272(__this, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Collections.Generic.IEnumerator`1<System.Type> ModestTree.TypeExtensions/<GetParentTypes>c__Iterator3E::System.Collections.Generic.IEnumerable<System.Type>.GetEnumerator()
extern Il2CppClass* U3CGetParentTypesU3Ec__Iterator3E_t1503887130_il2cpp_TypeInfo_var;
extern const uint32_t U3CGetParentTypesU3Ec__Iterator3E_System_Collections_Generic_IEnumerableU3CSystem_TypeU3E_GetEnumerator_m2282256272_MetadataUsageId;
extern "C"  Il2CppObject* U3CGetParentTypesU3Ec__Iterator3E_System_Collections_Generic_IEnumerableU3CSystem_TypeU3E_GetEnumerator_m2282256272 (U3CGetParentTypesU3Ec__Iterator3E_t1503887130 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CGetParentTypesU3Ec__Iterator3E_System_Collections_Generic_IEnumerableU3CSystem_TypeU3E_GetEnumerator_m2282256272_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	U3CGetParentTypesU3Ec__Iterator3E_t1503887130 * V_0 = NULL;
	{
		int32_t* L_0 = __this->get_address_of_U24PC_3();
		int32_t L_1 = Interlocked_CompareExchange_m1859820752(NULL /*static, unused*/, L_0, 0, ((int32_t)-2), /*hidden argument*/NULL);
		if ((!(((uint32_t)L_1) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0014;
		}
	}
	{
		return __this;
	}

IL_0014:
	{
		U3CGetParentTypesU3Ec__Iterator3E_t1503887130 * L_2 = (U3CGetParentTypesU3Ec__Iterator3E_t1503887130 *)il2cpp_codegen_object_new(U3CGetParentTypesU3Ec__Iterator3E_t1503887130_il2cpp_TypeInfo_var);
		U3CGetParentTypesU3Ec__Iterator3E__ctor_m2754063334(L_2, /*hidden argument*/NULL);
		V_0 = L_2;
		U3CGetParentTypesU3Ec__Iterator3E_t1503887130 * L_3 = V_0;
		Type_t * L_4 = __this->get_U3CU24U3Etype_5();
		NullCheck(L_3);
		L_3->set_type_0(L_4);
		U3CGetParentTypesU3Ec__Iterator3E_t1503887130 * L_5 = V_0;
		return L_5;
	}
}
// System.Boolean ModestTree.TypeExtensions/<GetParentTypes>c__Iterator3E::MoveNext()
extern const Il2CppType* Il2CppObject_0_0_0_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern Il2CppClass* IEnumerable_1_t1356416995_il2cpp_TypeInfo_var;
extern Il2CppClass* IEnumerator_1_t4262336383_il2cpp_TypeInfo_var;
extern Il2CppClass* IEnumerator_t287207039_il2cpp_TypeInfo_var;
extern Il2CppClass* IDisposable_t1628921374_il2cpp_TypeInfo_var;
extern const uint32_t U3CGetParentTypesU3Ec__Iterator3E_MoveNext_m1110604878_MetadataUsageId;
extern "C"  bool U3CGetParentTypesU3Ec__Iterator3E_MoveNext_m1110604878 (U3CGetParentTypesU3Ec__Iterator3E_t1503887130 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CGetParentTypesU3Ec__Iterator3E_MoveNext_m1110604878_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	uint32_t V_0 = 0;
	bool V_1 = false;
	bool V_2 = false;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		int32_t L_0 = __this->get_U24PC_3();
		V_0 = L_0;
		__this->set_U24PC_3((-1));
		V_1 = (bool)0;
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0027;
		}
		if (L_1 == 1)
		{
			goto IL_0093;
		}
		if (L_1 == 2)
		{
			goto IL_00b1;
		}
	}
	{
		goto IL_0122;
	}

IL_0027:
	{
		Type_t * L_2 = __this->get_type_0();
		if (!L_2)
		{
			goto IL_0071;
		}
	}
	{
		Type_t * L_3 = __this->get_type_0();
		NullCheck(L_3);
		Type_t * L_4 = VirtFuncInvoker0< Type_t * >::Invoke(18 /* System.Type System.Type::get_BaseType() */, L_3);
		if (!L_4)
		{
			goto IL_0071;
		}
	}
	{
		Type_t * L_5 = __this->get_type_0();
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_6 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, LoadTypeToken(Il2CppObject_0_0_0_var), /*hidden argument*/NULL);
		if ((((Il2CppObject*)(Type_t *)L_5) == ((Il2CppObject*)(Type_t *)L_6)))
		{
			goto IL_0071;
		}
	}
	{
		Type_t * L_7 = __this->get_type_0();
		NullCheck(L_7);
		Type_t * L_8 = VirtFuncInvoker0< Type_t * >::Invoke(18 /* System.Type System.Type::get_BaseType() */, L_7);
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_9 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, LoadTypeToken(Il2CppObject_0_0_0_var), /*hidden argument*/NULL);
		if ((!(((Il2CppObject*)(Type_t *)L_8) == ((Il2CppObject*)(Type_t *)L_9))))
		{
			goto IL_0076;
		}
	}

IL_0071:
	{
		goto IL_0122;
	}

IL_0076:
	{
		Type_t * L_10 = __this->get_type_0();
		NullCheck(L_10);
		Type_t * L_11 = VirtFuncInvoker0< Type_t * >::Invoke(18 /* System.Type System.Type::get_BaseType() */, L_10);
		__this->set_U24current_4(L_11);
		__this->set_U24PC_3(1);
		goto IL_0124;
	}

IL_0093:
	{
		Type_t * L_12 = __this->get_type_0();
		NullCheck(L_12);
		Type_t * L_13 = VirtFuncInvoker0< Type_t * >::Invoke(18 /* System.Type System.Type::get_BaseType() */, L_12);
		Il2CppObject* L_14 = TypeExtensions_GetParentTypes_m3922980882(NULL /*static, unused*/, L_13, /*hidden argument*/NULL);
		NullCheck(L_14);
		Il2CppObject* L_15 = InterfaceFuncInvoker0< Il2CppObject* >::Invoke(0 /* System.Collections.Generic.IEnumerator`1<!0> System.Collections.Generic.IEnumerable`1<System.Type>::GetEnumerator() */, IEnumerable_1_t1356416995_il2cpp_TypeInfo_var, L_14);
		__this->set_U3CU24s_243U3E__0_1(L_15);
		V_0 = ((int32_t)-3);
	}

IL_00b1:
	try
	{ // begin try (depth: 1)
		{
			uint32_t L_16 = V_0;
			if (((int32_t)((int32_t)L_16-(int32_t)2)) == 0)
			{
				goto IL_00ed;
			}
		}

IL_00bd:
		{
			goto IL_00ed;
		}

IL_00c2:
		{
			Il2CppObject* L_17 = __this->get_U3CU24s_243U3E__0_1();
			NullCheck(L_17);
			Type_t * L_18 = InterfaceFuncInvoker0< Type_t * >::Invoke(0 /* !0 System.Collections.Generic.IEnumerator`1<System.Type>::get_Current() */, IEnumerator_1_t4262336383_il2cpp_TypeInfo_var, L_17);
			__this->set_U3CancestorU3E__1_2(L_18);
			Type_t * L_19 = __this->get_U3CancestorU3E__1_2();
			__this->set_U24current_4(L_19);
			__this->set_U24PC_3(2);
			V_1 = (bool)1;
			IL2CPP_LEAVE(0x124, FINALLY_0102);
		}

IL_00ed:
		{
			Il2CppObject* L_20 = __this->get_U3CU24s_243U3E__0_1();
			NullCheck(L_20);
			bool L_21 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t287207039_il2cpp_TypeInfo_var, L_20);
			if (L_21)
			{
				goto IL_00c2;
			}
		}

IL_00fd:
		{
			IL2CPP_LEAVE(0x11B, FINALLY_0102);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0102;
	}

FINALLY_0102:
	{ // begin finally (depth: 1)
		{
			bool L_22 = V_1;
			if (!L_22)
			{
				goto IL_0106;
			}
		}

IL_0105:
		{
			IL2CPP_END_FINALLY(258)
		}

IL_0106:
		{
			Il2CppObject* L_23 = __this->get_U3CU24s_243U3E__0_1();
			if (L_23)
			{
				goto IL_010f;
			}
		}

IL_010e:
		{
			IL2CPP_END_FINALLY(258)
		}

IL_010f:
		{
			Il2CppObject* L_24 = __this->get_U3CU24s_243U3E__0_1();
			NullCheck(L_24);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, L_24);
			IL2CPP_END_FINALLY(258)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(258)
	{
		IL2CPP_JUMP_TBL(0x124, IL_0124)
		IL2CPP_JUMP_TBL(0x11B, IL_011b)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_011b:
	{
		__this->set_U24PC_3((-1));
	}

IL_0122:
	{
		return (bool)0;
	}

IL_0124:
	{
		return (bool)1;
	}
	// Dead block : IL_0126: ldloc.2
}
// System.Void ModestTree.TypeExtensions/<GetParentTypes>c__Iterator3E::Dispose()
extern Il2CppClass* IDisposable_t1628921374_il2cpp_TypeInfo_var;
extern const uint32_t U3CGetParentTypesU3Ec__Iterator3E_Dispose_m853292707_MetadataUsageId;
extern "C"  void U3CGetParentTypesU3Ec__Iterator3E_Dispose_m853292707 (U3CGetParentTypesU3Ec__Iterator3E_t1503887130 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CGetParentTypesU3Ec__Iterator3E_Dispose_m853292707_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	uint32_t V_0 = 0;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		int32_t L_0 = __this->get_U24PC_3();
		V_0 = L_0;
		__this->set_U24PC_3((-1));
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_003f;
		}
		if (L_1 == 1)
		{
			goto IL_003f;
		}
		if (L_1 == 2)
		{
			goto IL_0025;
		}
	}
	{
		goto IL_003f;
	}

IL_0025:
	try
	{ // begin try (depth: 1)
		IL2CPP_LEAVE(0x3F, FINALLY_002a);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_002a;
	}

FINALLY_002a:
	{ // begin finally (depth: 1)
		{
			Il2CppObject* L_2 = __this->get_U3CU24s_243U3E__0_1();
			if (L_2)
			{
				goto IL_0033;
			}
		}

IL_0032:
		{
			IL2CPP_END_FINALLY(42)
		}

IL_0033:
		{
			Il2CppObject* L_3 = __this->get_U3CU24s_243U3E__0_1();
			NullCheck(L_3);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, L_3);
			IL2CPP_END_FINALLY(42)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(42)
	{
		IL2CPP_JUMP_TBL(0x3F, IL_003f)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_003f:
	{
		return;
	}
}
// System.Void ModestTree.TypeExtensions/<GetParentTypes>c__Iterator3E::Reset()
extern Il2CppClass* NotSupportedException_t1374155497_il2cpp_TypeInfo_var;
extern const uint32_t U3CGetParentTypesU3Ec__Iterator3E_Reset_m400496275_MetadataUsageId;
extern "C"  void U3CGetParentTypesU3Ec__Iterator3E_Reset_m400496275 (U3CGetParentTypesU3Ec__Iterator3E_t1503887130 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CGetParentTypesU3Ec__Iterator3E_Reset_m400496275_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1374155497 * L_0 = (NotSupportedException_t1374155497 *)il2cpp_codegen_object_new(NotSupportedException_t1374155497_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m149930845(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void ModestTree.Util.Debugging.ProfileBlock::.ctor()
extern "C"  void ProfileBlock__ctor_m2697136375 (ProfileBlock_t2731282750 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// ModestTree.Util.Debugging.ProfileBlock ModestTree.Util.Debugging.ProfileBlock::Start()
extern "C"  ProfileBlock_t2731282750 * ProfileBlock_Start_m854692478 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		return (ProfileBlock_t2731282750 *)NULL;
	}
}
// ModestTree.Util.Debugging.ProfileBlock ModestTree.Util.Debugging.ProfileBlock::Start(System.String)
extern "C"  ProfileBlock_t2731282750 * ProfileBlock_Start_m1530300740 (Il2CppObject * __this /* static, unused */, String_t* ___sampleName0, const MethodInfo* method)
{
	{
		return (ProfileBlock_t2731282750 *)NULL;
	}
}
// ModestTree.Util.Debugging.ProfileBlock ModestTree.Util.Debugging.ProfileBlock::Start(System.String,System.Object)
extern "C"  ProfileBlock_t2731282750 * ProfileBlock_Start_m3968945426 (Il2CppObject * __this /* static, unused */, String_t* ___sampleNameFormat0, Il2CppObject * ___obj1, const MethodInfo* method)
{
	{
		return (ProfileBlock_t2731282750 *)NULL;
	}
}
// ModestTree.Util.Debugging.ProfileBlock ModestTree.Util.Debugging.ProfileBlock::Start(System.String,System.Object,System.Object)
extern "C"  ProfileBlock_t2731282750 * ProfileBlock_Start_m2297057376 (Il2CppObject * __this /* static, unused */, String_t* ___sampleNameFormat0, Il2CppObject * ___obj11, Il2CppObject * ___obj22, const MethodInfo* method)
{
	{
		return (ProfileBlock_t2731282750 *)NULL;
	}
}
// System.Void ModestTree.Util.Debugging.ProfileBlock::Dispose()
extern "C"  void ProfileBlock_Dispose_m1981059956 (ProfileBlock_t2731282750 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Boolean ModestTree.Util.ReflectionUtil::IsGenericList(System.Type)
extern const Il2CppType* List_1_t475681172_0_0_0_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern const uint32_t ReflectionUtil_IsGenericList_m3481035286_MetadataUsageId;
extern "C"  bool ReflectionUtil_IsGenericList_m3481035286 (Il2CppObject * __this /* static, unused */, Type_t * ___type0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ReflectionUtil_IsGenericList_m3481035286_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t G_B3_0 = 0;
	{
		Type_t * L_0 = ___type0;
		NullCheck(L_0);
		bool L_1 = VirtFuncInvoker0< bool >::Invoke(97 /* System.Boolean System.Type::get_IsGenericType() */, L_0);
		if (!L_1)
		{
			goto IL_001f;
		}
	}
	{
		Type_t * L_2 = ___type0;
		NullCheck(L_2);
		Type_t * L_3 = VirtFuncInvoker0< Type_t * >::Invoke(96 /* System.Type System.Type::GetGenericTypeDefinition() */, L_2);
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_4 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, LoadTypeToken(List_1_t475681172_0_0_0_var), /*hidden argument*/NULL);
		G_B3_0 = ((((Il2CppObject*)(Type_t *)L_3) == ((Il2CppObject*)(Type_t *)L_4))? 1 : 0);
		goto IL_0020;
	}

IL_001f:
	{
		G_B3_0 = 0;
	}

IL_0020:
	{
		return (bool)G_B3_0;
	}
}
// System.Boolean ModestTree.Util.ReflectionUtil::IsGenericList(System.Type,System.Type&)
extern const MethodInfo* Enumerable_Single_TisType_t_m4159287907_MethodInfo_var;
extern const uint32_t ReflectionUtil_IsGenericList_m2361690275_MetadataUsageId;
extern "C"  bool ReflectionUtil_IsGenericList_m2361690275 (Il2CppObject * __this /* static, unused */, Type_t * ___type0, Type_t ** ___contentsType1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ReflectionUtil_IsGenericList_m2361690275_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Type_t * L_0 = ___type0;
		bool L_1 = ReflectionUtil_IsGenericList_m3481035286(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_001a;
		}
	}
	{
		Type_t ** L_2 = ___contentsType1;
		Type_t * L_3 = ___type0;
		NullCheck(L_3);
		TypeU5BU5D_t3431720054* L_4 = VirtFuncInvoker0< TypeU5BU5D_t3431720054* >::Invoke(93 /* System.Type[] System.Type::GetGenericArguments() */, L_3);
		Type_t * L_5 = Enumerable_Single_TisType_t_m4159287907(NULL /*static, unused*/, (Il2CppObject*)(Il2CppObject*)L_4, /*hidden argument*/Enumerable_Single_TisType_t_m4159287907_MethodInfo_var);
		*((Il2CppObject **)(L_2)) = (Il2CppObject *)L_5;
		Il2CppCodeGenWriteBarrier((Il2CppObject **)(L_2), (Il2CppObject *)L_5);
		return (bool)1;
	}

IL_001a:
	{
		Type_t ** L_6 = ___contentsType1;
		*((Il2CppObject **)(L_6)) = (Il2CppObject *)NULL;
		Il2CppCodeGenWriteBarrier((Il2CppObject **)(L_6), (Il2CppObject *)NULL);
		return (bool)0;
	}
}
// System.Collections.IList ModestTree.Util.ReflectionUtil::CreateGenericList(System.Type,System.Object[])
extern const Il2CppType* List_1_t475681172_0_0_0_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern Il2CppClass* TypeU5BU5D_t3431720054_il2cpp_TypeInfo_var;
extern Il2CppClass* IList_t1612618265_il2cpp_TypeInfo_var;
extern Il2CppClass* ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3539835657;
extern Il2CppCodeGenString* _stringLiteral93474223;
extern Il2CppCodeGenString* _stringLiteral39;
extern const uint32_t ReflectionUtil_CreateGenericList_m3532283214_MetadataUsageId;
extern "C"  Il2CppObject * ReflectionUtil_CreateGenericList_m3532283214 (Il2CppObject * __this /* static, unused */, Type_t * ___elementType0, ObjectU5BU5D_t11523773* ___contentsAsObj1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ReflectionUtil_CreateGenericList_m3532283214_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Type_t * V_0 = NULL;
	Il2CppObject * V_1 = NULL;
	Il2CppObject * V_2 = NULL;
	ObjectU5BU5D_t11523773* V_3 = NULL;
	int32_t V_4 = 0;
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, LoadTypeToken(List_1_t475681172_0_0_0_var), /*hidden argument*/NULL);
		TypeU5BU5D_t3431720054* L_1 = ((TypeU5BU5D_t3431720054*)SZArrayNew(TypeU5BU5D_t3431720054_il2cpp_TypeInfo_var, (uint32_t)1));
		Type_t * L_2 = ___elementType0;
		NullCheck(L_1);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_1, 0);
		ArrayElementTypeCheck (L_1, L_2);
		(L_1)->SetAt(static_cast<il2cpp_array_size_t>(0), (Type_t *)L_2);
		NullCheck(L_0);
		Type_t * L_3 = VirtFuncInvoker1< Type_t *, TypeU5BU5D_t3431720054* >::Invoke(98 /* System.Type System.Type::MakeGenericType(System.Type[]) */, L_0, L_1);
		V_0 = L_3;
		Type_t * L_4 = V_0;
		Il2CppObject * L_5 = Activator_CreateInstance_m1399154923(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		V_1 = ((Il2CppObject *)Castclass(L_5, IList_t1612618265_il2cpp_TypeInfo_var));
		ObjectU5BU5D_t11523773* L_6 = ___contentsAsObj1;
		V_3 = L_6;
		V_4 = 0;
		goto IL_0090;
	}

IL_0030:
	{
		ObjectU5BU5D_t11523773* L_7 = V_3;
		int32_t L_8 = V_4;
		NullCheck(L_7);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_7, L_8);
		int32_t L_9 = L_8;
		V_2 = ((L_7)->GetAt(static_cast<il2cpp_array_size_t>(L_9)));
		Il2CppObject * L_10 = V_2;
		if (!L_10)
		{
			goto IL_0082;
		}
	}
	{
		Type_t * L_11 = ___elementType0;
		Il2CppObject * L_12 = V_2;
		NullCheck(L_12);
		Type_t * L_13 = Object_GetType_m2022236990(L_12, /*hidden argument*/NULL);
		NullCheck(L_11);
		bool L_14 = VirtFuncInvoker1< bool, Type_t * >::Invoke(46 /* System.Boolean System.Type::IsAssignableFrom(System.Type) */, L_11, L_13);
		ObjectU5BU5D_t11523773* L_15 = ((ObjectU5BU5D_t11523773*)SZArrayNew(ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var, (uint32_t)5));
		NullCheck(L_15);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_15, 0);
		ArrayElementTypeCheck (L_15, _stringLiteral3539835657);
		(L_15)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)_stringLiteral3539835657);
		ObjectU5BU5D_t11523773* L_16 = L_15;
		Type_t * L_17 = ___elementType0;
		NullCheck(L_16);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_16, 1);
		ArrayElementTypeCheck (L_16, L_17);
		(L_16)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_17);
		ObjectU5BU5D_t11523773* L_18 = L_16;
		NullCheck(L_18);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_18, 2);
		ArrayElementTypeCheck (L_18, _stringLiteral93474223);
		(L_18)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)_stringLiteral93474223);
		ObjectU5BU5D_t11523773* L_19 = L_18;
		Il2CppObject * L_20 = V_2;
		NullCheck(L_20);
		Type_t * L_21 = Object_GetType_m2022236990(L_20, /*hidden argument*/NULL);
		NullCheck(L_19);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_19, 3);
		ArrayElementTypeCheck (L_19, L_21);
		(L_19)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_21);
		ObjectU5BU5D_t11523773* L_22 = L_19;
		NullCheck(L_22);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_22, 4);
		ArrayElementTypeCheck (L_22, _stringLiteral39);
		(L_22)->SetAt(static_cast<il2cpp_array_size_t>(4), (Il2CppObject *)_stringLiteral39);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_23 = String_Concat_m3016520001(NULL /*static, unused*/, L_22, /*hidden argument*/NULL);
		Assert_That_m2934751473(NULL /*static, unused*/, L_14, L_23, ((ObjectU5BU5D_t11523773*)SZArrayNew(ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var, (uint32_t)0)), /*hidden argument*/NULL);
	}

IL_0082:
	{
		Il2CppObject * L_24 = V_1;
		Il2CppObject * L_25 = V_2;
		NullCheck(L_24);
		InterfaceFuncInvoker1< int32_t, Il2CppObject * >::Invoke(4 /* System.Int32 System.Collections.IList::Add(System.Object) */, IList_t1612618265_il2cpp_TypeInfo_var, L_24, L_25);
		int32_t L_26 = V_4;
		V_4 = ((int32_t)((int32_t)L_26+(int32_t)1));
	}

IL_0090:
	{
		int32_t L_27 = V_4;
		ObjectU5BU5D_t11523773* L_28 = V_3;
		NullCheck(L_28);
		if ((((int32_t)L_27) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_28)->max_length)))))))
		{
			goto IL_0030;
		}
	}
	{
		Il2CppObject * L_29 = V_1;
		return L_29;
	}
}
// System.Collections.IDictionary ModestTree.Util.ReflectionUtil::CreateGenericDictionary(System.Type,System.Type,System.Object[],System.Object[])
extern const Il2CppType* Dictionary_2_t2776849293_0_0_0_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern Il2CppClass* TypeU5BU5D_t3431720054_il2cpp_TypeInfo_var;
extern Il2CppClass* IDictionary_t1654916945_il2cpp_TypeInfo_var;
extern const uint32_t ReflectionUtil_CreateGenericDictionary_m2439713021_MetadataUsageId;
extern "C"  Il2CppObject * ReflectionUtil_CreateGenericDictionary_m2439713021 (Il2CppObject * __this /* static, unused */, Type_t * ___keyType0, Type_t * ___valueType1, ObjectU5BU5D_t11523773* ___keysAsObj2, ObjectU5BU5D_t11523773* ___valuesAsObj3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ReflectionUtil_CreateGenericDictionary_m2439713021_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Type_t * V_0 = NULL;
	Il2CppObject * V_1 = NULL;
	int32_t V_2 = 0;
	{
		ObjectU5BU5D_t11523773* L_0 = ___keysAsObj2;
		NullCheck(L_0);
		ObjectU5BU5D_t11523773* L_1 = ___valuesAsObj3;
		NullCheck(L_1);
		Assert_That_m1320742921(NULL /*static, unused*/, (bool)((((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_0)->max_length))))) == ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_1)->max_length))))))? 1 : 0), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_2 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, LoadTypeToken(Dictionary_2_t2776849293_0_0_0_var), /*hidden argument*/NULL);
		TypeU5BU5D_t3431720054* L_3 = ((TypeU5BU5D_t3431720054*)SZArrayNew(TypeU5BU5D_t3431720054_il2cpp_TypeInfo_var, (uint32_t)2));
		Type_t * L_4 = ___keyType0;
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, 0);
		ArrayElementTypeCheck (L_3, L_4);
		(L_3)->SetAt(static_cast<il2cpp_array_size_t>(0), (Type_t *)L_4);
		TypeU5BU5D_t3431720054* L_5 = L_3;
		Type_t * L_6 = ___valueType1;
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, 1);
		ArrayElementTypeCheck (L_5, L_6);
		(L_5)->SetAt(static_cast<il2cpp_array_size_t>(1), (Type_t *)L_6);
		NullCheck(L_2);
		Type_t * L_7 = VirtFuncInvoker1< Type_t *, TypeU5BU5D_t3431720054* >::Invoke(98 /* System.Type System.Type::MakeGenericType(System.Type[]) */, L_2, L_5);
		V_0 = L_7;
		Type_t * L_8 = V_0;
		Il2CppObject * L_9 = Activator_CreateInstance_m1399154923(NULL /*static, unused*/, L_8, /*hidden argument*/NULL);
		V_1 = ((Il2CppObject *)Castclass(L_9, IDictionary_t1654916945_il2cpp_TypeInfo_var));
		V_2 = 0;
		goto IL_004e;
	}

IL_003e:
	{
		Il2CppObject * L_10 = V_1;
		ObjectU5BU5D_t11523773* L_11 = ___keysAsObj2;
		int32_t L_12 = V_2;
		NullCheck(L_11);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_11, L_12);
		int32_t L_13 = L_12;
		ObjectU5BU5D_t11523773* L_14 = ___valuesAsObj3;
		int32_t L_15 = V_2;
		NullCheck(L_14);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_14, L_15);
		int32_t L_16 = L_15;
		NullCheck(L_10);
		InterfaceActionInvoker2< Il2CppObject *, Il2CppObject * >::Invoke(6 /* System.Void System.Collections.IDictionary::Add(System.Object,System.Object) */, IDictionary_t1654916945_il2cpp_TypeInfo_var, L_10, ((L_11)->GetAt(static_cast<il2cpp_array_size_t>(L_13))), ((L_14)->GetAt(static_cast<il2cpp_array_size_t>(L_16))));
		int32_t L_17 = V_2;
		V_2 = ((int32_t)((int32_t)L_17+(int32_t)1));
	}

IL_004e:
	{
		int32_t L_18 = V_2;
		ObjectU5BU5D_t11523773* L_19 = ___keysAsObj2;
		NullCheck(L_19);
		if ((((int32_t)L_18) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_19)->max_length)))))))
		{
			goto IL_003e;
		}
	}
	{
		Il2CppObject * L_20 = V_1;
		return L_20;
	}
}
// System.Boolean ModestTree.Util.UnityUtil::get_IsAltKeyDown()
extern Il2CppClass* Input_t1593691127_il2cpp_TypeInfo_var;
extern const uint32_t UnityUtil_get_IsAltKeyDown_m1269086535_MetadataUsageId;
extern "C"  bool UnityUtil_get_IsAltKeyDown_m1269086535 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (UnityUtil_get_IsAltKeyDown_m1269086535_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t G_B3_0 = 0;
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1593691127_il2cpp_TypeInfo_var);
		bool L_0 = Input_GetKey_m1349175653(NULL /*static, unused*/, ((int32_t)308), /*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_001b;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1593691127_il2cpp_TypeInfo_var);
		bool L_1 = Input_GetKey_m1349175653(NULL /*static, unused*/, ((int32_t)307), /*hidden argument*/NULL);
		G_B3_0 = ((int32_t)(L_1));
		goto IL_001c;
	}

IL_001b:
	{
		G_B3_0 = 1;
	}

IL_001c:
	{
		return (bool)G_B3_0;
	}
}
// System.Boolean ModestTree.Util.UnityUtil::get_IsControlKeyDown()
extern Il2CppClass* Input_t1593691127_il2cpp_TypeInfo_var;
extern const uint32_t UnityUtil_get_IsControlKeyDown_m3588342035_MetadataUsageId;
extern "C"  bool UnityUtil_get_IsControlKeyDown_m3588342035 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (UnityUtil_get_IsControlKeyDown_m3588342035_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t G_B3_0 = 0;
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1593691127_il2cpp_TypeInfo_var);
		bool L_0 = Input_GetKey_m1349175653(NULL /*static, unused*/, ((int32_t)306), /*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_001b;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1593691127_il2cpp_TypeInfo_var);
		bool L_1 = Input_GetKey_m1349175653(NULL /*static, unused*/, ((int32_t)305), /*hidden argument*/NULL);
		G_B3_0 = ((int32_t)(L_1));
		goto IL_001c;
	}

IL_001b:
	{
		G_B3_0 = 1;
	}

IL_001c:
	{
		return (bool)G_B3_0;
	}
}
// System.Boolean ModestTree.Util.UnityUtil::get_IsShiftKeyDown()
extern Il2CppClass* Input_t1593691127_il2cpp_TypeInfo_var;
extern const uint32_t UnityUtil_get_IsShiftKeyDown_m1184300814_MetadataUsageId;
extern "C"  bool UnityUtil_get_IsShiftKeyDown_m1184300814 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (UnityUtil_get_IsShiftKeyDown_m1184300814_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t G_B3_0 = 0;
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1593691127_il2cpp_TypeInfo_var);
		bool L_0 = Input_GetKey_m1349175653(NULL /*static, unused*/, ((int32_t)304), /*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_001b;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1593691127_il2cpp_TypeInfo_var);
		bool L_1 = Input_GetKey_m1349175653(NULL /*static, unused*/, ((int32_t)303), /*hidden argument*/NULL);
		G_B3_0 = ((int32_t)(L_1));
		goto IL_001c;
	}

IL_001b:
	{
		G_B3_0 = 1;
	}

IL_001c:
	{
		return (bool)G_B3_0;
	}
}
// System.Boolean ModestTree.Util.UnityUtil::get_WasShiftKeyJustPressed()
extern Il2CppClass* Input_t1593691127_il2cpp_TypeInfo_var;
extern const uint32_t UnityUtil_get_WasShiftKeyJustPressed_m3648197493_MetadataUsageId;
extern "C"  bool UnityUtil_get_WasShiftKeyJustPressed_m3648197493 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (UnityUtil_get_WasShiftKeyJustPressed_m3648197493_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t G_B3_0 = 0;
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1593691127_il2cpp_TypeInfo_var);
		bool L_0 = Input_GetKeyDown_m2928824675(NULL /*static, unused*/, ((int32_t)304), /*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_001b;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1593691127_il2cpp_TypeInfo_var);
		bool L_1 = Input_GetKeyDown_m2928824675(NULL /*static, unused*/, ((int32_t)303), /*hidden argument*/NULL);
		G_B3_0 = ((int32_t)(L_1));
		goto IL_001c;
	}

IL_001b:
	{
		G_B3_0 = 1;
	}

IL_001c:
	{
		return (bool)G_B3_0;
	}
}
// System.Boolean ModestTree.Util.UnityUtil::get_WasAltKeyJustPressed()
extern Il2CppClass* Input_t1593691127_il2cpp_TypeInfo_var;
extern const uint32_t UnityUtil_get_WasAltKeyJustPressed_m691645788_MetadataUsageId;
extern "C"  bool UnityUtil_get_WasAltKeyJustPressed_m691645788 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (UnityUtil_get_WasAltKeyJustPressed_m691645788_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t G_B3_0 = 0;
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1593691127_il2cpp_TypeInfo_var);
		bool L_0 = Input_GetKeyDown_m2928824675(NULL /*static, unused*/, ((int32_t)308), /*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_001b;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1593691127_il2cpp_TypeInfo_var);
		bool L_1 = Input_GetKeyDown_m2928824675(NULL /*static, unused*/, ((int32_t)307), /*hidden argument*/NULL);
		G_B3_0 = ((int32_t)(L_1));
		goto IL_001c;
	}

IL_001b:
	{
		G_B3_0 = 1;
	}

IL_001c:
	{
		return (bool)G_B3_0;
	}
}
// ModestTree.Util.MouseWheelScrollDirections ModestTree.Util.UnityUtil::CheckMouseScrollWheel()
extern Il2CppClass* Input_t1593691127_il2cpp_TypeInfo_var;
extern Il2CppClass* Mathf_t1597001355_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2268470611;
extern const uint32_t UnityUtil_CheckMouseScrollWheel_m3579850857_MetadataUsageId;
extern "C"  int32_t UnityUtil_CheckMouseScrollWheel_m3579850857 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (UnityUtil_CheckMouseScrollWheel_m3579850857_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	float V_0 = 0.0f;
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1593691127_il2cpp_TypeInfo_var);
		float L_0 = Input_GetAxis_m2027668530(NULL /*static, unused*/, _stringLiteral2268470611, /*hidden argument*/NULL);
		V_0 = L_0;
		float L_1 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t1597001355_il2cpp_TypeInfo_var);
		bool L_2 = Mathf_Approximately_m1395529776(NULL /*static, unused*/, L_1, (0.0f), /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_001d;
		}
	}
	{
		return (int32_t)(0);
	}

IL_001d:
	{
		float L_3 = V_0;
		if ((!(((float)L_3) < ((float)(0.0f)))))
		{
			goto IL_002a;
		}
	}
	{
		return (int32_t)(2);
	}

IL_002a:
	{
		return (int32_t)(1);
	}
}
// System.Int32 ModestTree.Util.UnityUtil::GetDepthLevel(UnityEngine.Transform)
extern "C"  int32_t UnityUtil_GetDepthLevel_m83980134 (Il2CppObject * __this /* static, unused */, Transform_t284553113 * ___transform0, const MethodInfo* method)
{
	{
		Transform_t284553113 * L_0 = ___transform0;
		bool L_1 = Object_op_Equality_m1690293457(NULL /*static, unused*/, L_0, (Object_t3878351788 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_000e;
		}
	}
	{
		return 0;
	}

IL_000e:
	{
		Transform_t284553113 * L_2 = ___transform0;
		NullCheck(L_2);
		Transform_t284553113 * L_3 = Transform_get_parent_m3742239125(L_2, /*hidden argument*/NULL);
		int32_t L_4 = UnityUtil_GetDepthLevel_m83980134(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		return ((int32_t)((int32_t)1+(int32_t)L_4));
	}
}
// System.Collections.Generic.List`1<UnityEngine.GameObject> ModestTree.Util.UnityUtil::GetRootGameObjects()
extern Il2CppClass* UnityUtil_t126913041_il2cpp_TypeInfo_var;
extern Il2CppClass* Func_2_t884531080_il2cpp_TypeInfo_var;
extern Il2CppClass* Func_2_t391253545_il2cpp_TypeInfo_var;
extern const MethodInfo* Object_FindObjectsOfType_TisTransform_t284553113_m2131108345_MethodInfo_var;
extern const MethodInfo* UnityUtil_U3CGetRootGameObjectsU3Em__30A_m3239353298_MethodInfo_var;
extern const MethodInfo* Func_2__ctor_m642311202_MethodInfo_var;
extern const MethodInfo* Enumerable_Where_TisTransform_t284553113_m289571163_MethodInfo_var;
extern const MethodInfo* UnityUtil_U3CGetRootGameObjectsU3Em__30B_m4070462770_MethodInfo_var;
extern const MethodInfo* Func_2__ctor_m3314268911_MethodInfo_var;
extern const MethodInfo* Enumerable_Select_TisTransform_t284553113_TisGameObject_t4012695102_m2957448055_MethodInfo_var;
extern const MethodInfo* Enumerable_ToList_TisGameObject_t4012695102_m3323433239_MethodInfo_var;
extern const uint32_t UnityUtil_GetRootGameObjects_m729052607_MetadataUsageId;
extern "C"  List_1_t514686775 * UnityUtil_GetRootGameObjects_m729052607 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (UnityUtil_GetRootGameObjects_m729052607_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	TransformU5BU5D_t3681339876* G_B2_0 = NULL;
	TransformU5BU5D_t3681339876* G_B1_0 = NULL;
	Il2CppObject* G_B4_0 = NULL;
	Il2CppObject* G_B3_0 = NULL;
	{
		TransformU5BU5D_t3681339876* L_0 = Object_FindObjectsOfType_TisTransform_t284553113_m2131108345(NULL /*static, unused*/, /*hidden argument*/Object_FindObjectsOfType_TisTransform_t284553113_m2131108345_MethodInfo_var);
		Func_2_t884531080 * L_1 = ((UnityUtil_t126913041_StaticFields*)UnityUtil_t126913041_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__amU24cache0_0();
		G_B1_0 = L_0;
		if (L_1)
		{
			G_B2_0 = L_0;
			goto IL_001d;
		}
	}
	{
		IntPtr_t L_2;
		L_2.set_m_value_0((void*)(void*)UnityUtil_U3CGetRootGameObjectsU3Em__30A_m3239353298_MethodInfo_var);
		Func_2_t884531080 * L_3 = (Func_2_t884531080 *)il2cpp_codegen_object_new(Func_2_t884531080_il2cpp_TypeInfo_var);
		Func_2__ctor_m642311202(L_3, NULL, L_2, /*hidden argument*/Func_2__ctor_m642311202_MethodInfo_var);
		((UnityUtil_t126913041_StaticFields*)UnityUtil_t126913041_il2cpp_TypeInfo_var->static_fields)->set_U3CU3Ef__amU24cache0_0(L_3);
		G_B2_0 = G_B1_0;
	}

IL_001d:
	{
		Func_2_t884531080 * L_4 = ((UnityUtil_t126913041_StaticFields*)UnityUtil_t126913041_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__amU24cache0_0();
		Il2CppObject* L_5 = Enumerable_Where_TisTransform_t284553113_m289571163(NULL /*static, unused*/, (Il2CppObject*)(Il2CppObject*)G_B2_0, L_4, /*hidden argument*/Enumerable_Where_TisTransform_t284553113_m289571163_MethodInfo_var);
		Func_2_t391253545 * L_6 = ((UnityUtil_t126913041_StaticFields*)UnityUtil_t126913041_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__amU24cache1_1();
		G_B3_0 = L_5;
		if (L_6)
		{
			G_B4_0 = L_5;
			goto IL_003f;
		}
	}
	{
		IntPtr_t L_7;
		L_7.set_m_value_0((void*)(void*)UnityUtil_U3CGetRootGameObjectsU3Em__30B_m4070462770_MethodInfo_var);
		Func_2_t391253545 * L_8 = (Func_2_t391253545 *)il2cpp_codegen_object_new(Func_2_t391253545_il2cpp_TypeInfo_var);
		Func_2__ctor_m3314268911(L_8, NULL, L_7, /*hidden argument*/Func_2__ctor_m3314268911_MethodInfo_var);
		((UnityUtil_t126913041_StaticFields*)UnityUtil_t126913041_il2cpp_TypeInfo_var->static_fields)->set_U3CU3Ef__amU24cache1_1(L_8);
		G_B4_0 = G_B3_0;
	}

IL_003f:
	{
		Func_2_t391253545 * L_9 = ((UnityUtil_t126913041_StaticFields*)UnityUtil_t126913041_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__amU24cache1_1();
		Il2CppObject* L_10 = Enumerable_Select_TisTransform_t284553113_TisGameObject_t4012695102_m2957448055(NULL /*static, unused*/, G_B4_0, L_9, /*hidden argument*/Enumerable_Select_TisTransform_t284553113_TisGameObject_t4012695102_m2957448055_MethodInfo_var);
		List_1_t514686775 * L_11 = Enumerable_ToList_TisGameObject_t4012695102_m3323433239(NULL /*static, unused*/, L_10, /*hidden argument*/Enumerable_ToList_TisGameObject_t4012695102_m3323433239_MethodInfo_var);
		return L_11;
	}
}
// System.Object ModestTree.Util.UnityUtil::GetSmartDefaultValue(System.Type)
extern const Il2CppType* String_t_0_0_0_var;
extern const Il2CppType* Quaternion_t1891715979_0_0_0_var;
extern const Il2CppType* List_1_t475681172_0_0_0_var;
extern const Il2CppType* Dictionary_2_t2776849293_0_0_0_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Quaternion_t1891715979_il2cpp_TypeInfo_var;
extern const uint32_t UnityUtil_GetSmartDefaultValue_m2015070267_MetadataUsageId;
extern "C"  Il2CppObject * UnityUtil_GetSmartDefaultValue_m2015070267 (Il2CppObject * __this /* static, unused */, Type_t * ___type0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (UnityUtil_GetSmartDefaultValue_m2015070267_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Type_t * V_0 = NULL;
	{
		Type_t * L_0 = ___type0;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_1 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, LoadTypeToken(String_t_0_0_0_var), /*hidden argument*/NULL);
		if ((!(((Il2CppObject*)(Type_t *)L_0) == ((Il2CppObject*)(Type_t *)L_1))))
		{
			goto IL_0016;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_2 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		return L_2;
	}

IL_0016:
	{
		Type_t * L_3 = ___type0;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_4 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, LoadTypeToken(Quaternion_t1891715979_0_0_0_var), /*hidden argument*/NULL);
		if ((!(((Il2CppObject*)(Type_t *)L_3) == ((Il2CppObject*)(Type_t *)L_4))))
		{
			goto IL_0031;
		}
	}
	{
		Quaternion_t1891715979  L_5 = Quaternion_get_identity_m1635708575(NULL /*static, unused*/, /*hidden argument*/NULL);
		Quaternion_t1891715979  L_6 = L_5;
		Il2CppObject * L_7 = Box(Quaternion_t1891715979_il2cpp_TypeInfo_var, &L_6);
		return L_7;
	}

IL_0031:
	{
		Type_t * L_8 = ___type0;
		NullCheck(L_8);
		bool L_9 = VirtFuncInvoker0< bool >::Invoke(97 /* System.Boolean System.Type::get_IsGenericType() */, L_8);
		if (!L_9)
		{
			goto IL_006a;
		}
	}
	{
		Type_t * L_10 = ___type0;
		NullCheck(L_10);
		Type_t * L_11 = VirtFuncInvoker0< Type_t * >::Invoke(96 /* System.Type System.Type::GetGenericTypeDefinition() */, L_10);
		V_0 = L_11;
		Type_t * L_12 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_13 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, LoadTypeToken(List_1_t475681172_0_0_0_var), /*hidden argument*/NULL);
		if ((((Il2CppObject*)(Type_t *)L_12) == ((Il2CppObject*)(Type_t *)L_13)))
		{
			goto IL_0063;
		}
	}
	{
		Type_t * L_14 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_15 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, LoadTypeToken(Dictionary_2_t2776849293_0_0_0_var), /*hidden argument*/NULL);
		if ((!(((Il2CppObject*)(Type_t *)L_14) == ((Il2CppObject*)(Type_t *)L_15))))
		{
			goto IL_006a;
		}
	}

IL_0063:
	{
		Type_t * L_16 = ___type0;
		Il2CppObject * L_17 = Activator_CreateInstance_m1399154923(NULL /*static, unused*/, L_16, /*hidden argument*/NULL);
		return L_17;
	}

IL_006a:
	{
		Type_t * L_18 = ___type0;
		Il2CppObject * L_19 = TypeExtensions_GetDefaultValue_m1549006447(NULL /*static, unused*/, L_18, /*hidden argument*/NULL);
		return L_19;
	}
}
// System.Boolean ModestTree.Util.UnityUtil::<GetRootGameObjects>m__30A(UnityEngine.Transform)
extern "C"  bool UnityUtil_U3CGetRootGameObjectsU3Em__30A_m3239353298 (Il2CppObject * __this /* static, unused */, Transform_t284553113 * ___x0, const MethodInfo* method)
{
	{
		Transform_t284553113 * L_0 = ___x0;
		NullCheck(L_0);
		Transform_t284553113 * L_1 = Transform_get_parent_m3742239125(L_0, /*hidden argument*/NULL);
		bool L_2 = Object_op_Equality_m1690293457(NULL /*static, unused*/, L_1, (Object_t3878351788 *)NULL, /*hidden argument*/NULL);
		return L_2;
	}
}
// UnityEngine.GameObject ModestTree.Util.UnityUtil::<GetRootGameObjects>m__30B(UnityEngine.Transform)
extern "C"  GameObject_t4012695102 * UnityUtil_U3CGetRootGameObjectsU3Em__30B_m4070462770 (Il2CppObject * __this /* static, unused */, Transform_t284553113 * ___x0, const MethodInfo* method)
{
	{
		Transform_t284553113 * L_0 = ___x0;
		NullCheck(L_0);
		GameObject_t4012695102 * L_1 = Component_get_gameObject_m2112202034(L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Void MoneyCounter::.ctor()
extern Il2CppClass* List_1_t3419442901_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1__ctor_m1044603515_MethodInfo_var;
extern const uint32_t MoneyCounter__ctor_m2271988959_MetadataUsageId;
extern "C"  void MoneyCounter__ctor_m2271988959 (MoneyCounter_t199543100 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (MoneyCounter__ctor_m2271988959_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		List_1_t3419442901 * L_0 = (List_1_t3419442901 *)il2cpp_codegen_object_new(List_1_t3419442901_il2cpp_TypeInfo_var);
		List_1__ctor_m1044603515(L_0, /*hidden argument*/List_1__ctor_m1044603515_MethodInfo_var);
		__this->set_counters_3(L_0);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_1 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		__this->set_currentValue_4(L_1);
		MonoBehaviour__ctor_m350695606(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void MoneyCounter::SetCountersNumber(System.Int32,System.Int32)
extern const MethodInfo* Object_Instantiate_TisGameObject_t4012695102_m3917608929_MethodInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisCounter_t2622483932_m3934559109_MethodInfo_var;
extern const uint32_t MoneyCounter_SetCountersNumber_m3722464103_MetadataUsageId;
extern "C"  void MoneyCounter_SetCountersNumber_m3722464103 (MoneyCounter_t199543100 * __this, int32_t ___quantity0, int32_t ___chars1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (MoneyCounter_SetCountersNumber_m3722464103_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Counter_t2622483932 * V_0 = NULL;
	Counter_t2622483932 * V_1 = NULL;
	float V_2 = 0.0f;
	int32_t V_3 = 0;
	int32_t V_4 = 0;
	int32_t V_5 = 0;
	int32_t V_6 = 0;
	{
		List_1_t3419442901 * L_0 = __this->get_counters_3();
		NullCheck(L_0);
		int32_t L_1 = VirtFuncInvoker0< int32_t >::Invoke(20 /* System.Int32 System.Collections.Generic.List`1<Counter>::get_Count() */, L_0);
		int32_t L_2 = ___quantity0;
		if ((((int32_t)L_1) >= ((int32_t)L_2)))
		{
			goto IL_0098;
		}
	}
	{
		goto IL_0082;
	}

IL_0016:
	{
		GameObject_t4012695102 * L_3 = __this->get_counterSample_2();
		GameObject_t4012695102 * L_4 = Object_Instantiate_TisGameObject_t4012695102_m3917608929(NULL /*static, unused*/, L_3, /*hidden argument*/Object_Instantiate_TisGameObject_t4012695102_m3917608929_MethodInfo_var);
		NullCheck(L_4);
		Counter_t2622483932 * L_5 = GameObject_GetComponent_TisCounter_t2622483932_m3934559109(L_4, /*hidden argument*/GameObject_GetComponent_TisCounter_t2622483932_m3934559109_MethodInfo_var);
		V_0 = L_5;
		List_1_t3419442901 * L_6 = __this->get_counters_3();
		Counter_t2622483932 * L_7 = V_0;
		NullCheck(L_6);
		VirtActionInvoker1< Counter_t2622483932 * >::Invoke(22 /* System.Void System.Collections.Generic.List`1<Counter>::Add(!0) */, L_6, L_7);
		Counter_t2622483932 * L_8 = V_0;
		NullCheck(L_8);
		Transform_t284553113 * L_9 = Component_get_transform_m2452535634(L_8, /*hidden argument*/NULL);
		Transform_t284553113 * L_10 = Component_get_transform_m2452535634(__this, /*hidden argument*/NULL);
		NullCheck(L_9);
		Transform_set_parent_m120112962(L_9, L_10, /*hidden argument*/NULL);
		Counter_t2622483932 * L_11 = V_0;
		NullCheck(L_11);
		Transform_t284553113 * L_12 = Component_get_transform_m2452535634(L_11, /*hidden argument*/NULL);
		Vector3_t3525329789  L_13 = Vector3_get_one_m886467710(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_12);
		Transform_set_localScale_m1776830461(L_12, L_13, /*hidden argument*/NULL);
		Counter_t2622483932 * L_14 = V_0;
		NullCheck(L_14);
		Transform_t284553113 * L_15 = Component_get_transform_m2452535634(L_14, /*hidden argument*/NULL);
		List_1_t3419442901 * L_16 = __this->get_counters_3();
		NullCheck(L_16);
		int32_t L_17 = VirtFuncInvoker0< int32_t >::Invoke(20 /* System.Int32 System.Collections.Generic.List`1<Counter>::get_Count() */, L_16);
		Vector3_t3525329789  L_18;
		memset(&L_18, 0, sizeof(L_18));
		Vector3__ctor_m1243251509(&L_18, ((float)((float)(0.27f)*(float)(((float)((float)((int32_t)((int32_t)L_17-(int32_t)1))))))), (0.0f), (0.0f), /*hidden argument*/NULL);
		NullCheck(L_15);
		Transform_set_localPosition_m3778340736(L_15, L_18, /*hidden argument*/NULL);
	}

IL_0082:
	{
		List_1_t3419442901 * L_19 = __this->get_counters_3();
		NullCheck(L_19);
		int32_t L_20 = VirtFuncInvoker0< int32_t >::Invoke(20 /* System.Int32 System.Collections.Generic.List`1<Counter>::get_Count() */, L_19);
		int32_t L_21 = ___quantity0;
		if ((((int32_t)L_20) < ((int32_t)L_21)))
		{
			goto IL_0016;
		}
	}
	{
		goto IL_00fb;
	}

IL_0098:
	{
		List_1_t3419442901 * L_22 = __this->get_counters_3();
		NullCheck(L_22);
		int32_t L_23 = VirtFuncInvoker0< int32_t >::Invoke(20 /* System.Int32 System.Collections.Generic.List`1<Counter>::get_Count() */, L_22);
		int32_t L_24 = ___quantity0;
		if ((((int32_t)L_23) <= ((int32_t)L_24)))
		{
			goto IL_00fb;
		}
	}
	{
		goto IL_00ea;
	}

IL_00ae:
	{
		List_1_t3419442901 * L_25 = __this->get_counters_3();
		List_1_t3419442901 * L_26 = __this->get_counters_3();
		NullCheck(L_26);
		int32_t L_27 = VirtFuncInvoker0< int32_t >::Invoke(20 /* System.Int32 System.Collections.Generic.List`1<Counter>::get_Count() */, L_26);
		NullCheck(L_25);
		Counter_t2622483932 * L_28 = VirtFuncInvoker1< Counter_t2622483932 *, int32_t >::Invoke(31 /* !0 System.Collections.Generic.List`1<Counter>::get_Item(System.Int32) */, L_25, ((int32_t)((int32_t)L_27-(int32_t)1)));
		V_1 = L_28;
		List_1_t3419442901 * L_29 = __this->get_counters_3();
		List_1_t3419442901 * L_30 = __this->get_counters_3();
		NullCheck(L_30);
		int32_t L_31 = VirtFuncInvoker0< int32_t >::Invoke(20 /* System.Int32 System.Collections.Generic.List`1<Counter>::get_Count() */, L_30);
		NullCheck(L_29);
		VirtActionInvoker1< int32_t >::Invoke(30 /* System.Void System.Collections.Generic.List`1<Counter>::RemoveAt(System.Int32) */, L_29, ((int32_t)((int32_t)L_31-(int32_t)1)));
		Counter_t2622483932 * L_32 = V_1;
		NullCheck(L_32);
		GameObject_t4012695102 * L_33 = Component_get_gameObject_m2112202034(L_32, /*hidden argument*/NULL);
		Object_Destroy_m3720418393(NULL /*static, unused*/, L_33, /*hidden argument*/NULL);
	}

IL_00ea:
	{
		List_1_t3419442901 * L_34 = __this->get_counters_3();
		NullCheck(L_34);
		int32_t L_35 = VirtFuncInvoker0< int32_t >::Invoke(20 /* System.Int32 System.Collections.Generic.List`1<Counter>::get_Count() */, L_34);
		int32_t L_36 = ___quantity0;
		if ((((int32_t)L_35) > ((int32_t)L_36)))
		{
			goto IL_00ae;
		}
	}

IL_00fb:
	{
		V_2 = (0.0f);
		int32_t L_37 = ___chars1;
		if (L_37)
		{
			goto IL_0182;
		}
	}
	{
		List_1_t3419442901 * L_38 = __this->get_counters_3();
		NullCheck(L_38);
		int32_t L_39 = VirtFuncInvoker0< int32_t >::Invoke(20 /* System.Int32 System.Collections.Generic.List`1<Counter>::get_Count() */, L_38);
		V_3 = ((int32_t)((int32_t)L_39-(int32_t)1));
		goto IL_0176;
	}

IL_011a:
	{
		List_1_t3419442901 * L_40 = __this->get_counters_3();
		int32_t L_41 = V_3;
		NullCheck(L_40);
		Counter_t2622483932 * L_42 = VirtFuncInvoker1< Counter_t2622483932 *, int32_t >::Invoke(31 /* !0 System.Collections.Generic.List`1<Counter>::get_Item(System.Int32) */, L_40, L_41);
		NullCheck(L_42);
		Transform_t284553113 * L_43 = Component_get_transform_m2452535634(L_42, /*hidden argument*/NULL);
		float L_44 = V_2;
		Vector3_t3525329789  L_45;
		memset(&L_45, 0, sizeof(L_45));
		Vector3__ctor_m1243251509(&L_45, L_44, (0.0f), (0.0f), /*hidden argument*/NULL);
		NullCheck(L_43);
		Transform_set_localPosition_m3778340736(L_43, L_45, /*hidden argument*/NULL);
		int32_t L_46 = V_3;
		if ((((int32_t)L_46) <= ((int32_t)0)))
		{
			goto IL_0172;
		}
	}
	{
		int32_t L_47 = ___quantity0;
		int32_t L_48 = V_3;
		V_4 = ((int32_t)((int32_t)L_47-(int32_t)L_48));
		int32_t L_49 = V_4;
		if ((((int32_t)L_49) <= ((int32_t)0)))
		{
			goto IL_016a;
		}
	}
	{
		int32_t L_50 = V_4;
		if (((int32_t)((int32_t)L_50%(int32_t)3)))
		{
			goto IL_016a;
		}
	}
	{
		float L_51 = V_2;
		V_2 = ((float)((float)L_51-(float)(0.32f)));
		goto IL_0172;
	}

IL_016a:
	{
		float L_52 = V_2;
		V_2 = ((float)((float)L_52-(float)(0.27f)));
	}

IL_0172:
	{
		int32_t L_53 = V_3;
		V_3 = ((int32_t)((int32_t)L_53-(int32_t)1));
	}

IL_0176:
	{
		int32_t L_54 = V_3;
		if ((((int32_t)L_54) >= ((int32_t)0)))
		{
			goto IL_011a;
		}
	}
	{
		goto IL_0247;
	}

IL_0182:
	{
		V_5 = 0;
		goto IL_01e4;
	}

IL_018a:
	{
		List_1_t3419442901 * L_55 = __this->get_counters_3();
		List_1_t3419442901 * L_56 = __this->get_counters_3();
		NullCheck(L_56);
		int32_t L_57 = VirtFuncInvoker0< int32_t >::Invoke(20 /* System.Int32 System.Collections.Generic.List`1<Counter>::get_Count() */, L_56);
		int32_t L_58 = V_5;
		NullCheck(L_55);
		Counter_t2622483932 * L_59 = VirtFuncInvoker1< Counter_t2622483932 *, int32_t >::Invoke(31 /* !0 System.Collections.Generic.List`1<Counter>::get_Item(System.Int32) */, L_55, ((int32_t)((int32_t)((int32_t)((int32_t)L_57-(int32_t)1))-(int32_t)L_58)));
		NullCheck(L_59);
		Transform_t284553113 * L_60 = Component_get_transform_m2452535634(L_59, /*hidden argument*/NULL);
		float L_61 = V_2;
		Vector3_t3525329789  L_62;
		memset(&L_62, 0, sizeof(L_62));
		Vector3__ctor_m1243251509(&L_62, L_61, (0.0f), (0.0f), /*hidden argument*/NULL);
		NullCheck(L_60);
		Transform_set_localPosition_m3778340736(L_60, L_62, /*hidden argument*/NULL);
		int32_t L_63 = V_5;
		int32_t L_64 = ___chars1;
		if ((!(((uint32_t)((int32_t)((int32_t)L_63+(int32_t)1))) == ((uint32_t)L_64))))
		{
			goto IL_01d6;
		}
	}
	{
		float L_65 = V_2;
		V_2 = ((float)((float)L_65-(float)(0.32f)));
		goto IL_01de;
	}

IL_01d6:
	{
		float L_66 = V_2;
		V_2 = ((float)((float)L_66-(float)(0.27f)));
	}

IL_01de:
	{
		int32_t L_67 = V_5;
		V_5 = ((int32_t)((int32_t)L_67+(int32_t)1));
	}

IL_01e4:
	{
		int32_t L_68 = V_5;
		int32_t L_69 = ___chars1;
		if ((((int32_t)L_68) < ((int32_t)L_69)))
		{
			goto IL_018a;
		}
	}
	{
		List_1_t3419442901 * L_70 = __this->get_counters_3();
		NullCheck(L_70);
		int32_t L_71 = VirtFuncInvoker0< int32_t >::Invoke(20 /* System.Int32 System.Collections.Generic.List`1<Counter>::get_Count() */, L_70);
		int32_t L_72 = ___chars1;
		V_6 = ((int32_t)((int32_t)((int32_t)((int32_t)L_71-(int32_t)1))-(int32_t)L_72));
		goto IL_023f;
	}

IL_0202:
	{
		List_1_t3419442901 * L_73 = __this->get_counters_3();
		int32_t L_74 = V_6;
		NullCheck(L_73);
		Counter_t2622483932 * L_75 = VirtFuncInvoker1< Counter_t2622483932 *, int32_t >::Invoke(31 /* !0 System.Collections.Generic.List`1<Counter>::get_Item(System.Int32) */, L_73, L_74);
		NullCheck(L_75);
		Transform_t284553113 * L_76 = Component_get_transform_m2452535634(L_75, /*hidden argument*/NULL);
		float L_77 = V_2;
		Vector3_t3525329789  L_78;
		memset(&L_78, 0, sizeof(L_78));
		Vector3__ctor_m1243251509(&L_78, L_77, (0.0f), (0.0f), /*hidden argument*/NULL);
		NullCheck(L_76);
		Transform_set_localPosition_m3778340736(L_76, L_78, /*hidden argument*/NULL);
		int32_t L_79 = V_6;
		if ((((int32_t)L_79) <= ((int32_t)0)))
		{
			goto IL_0239;
		}
	}
	{
		float L_80 = V_2;
		V_2 = ((float)((float)L_80-(float)(0.27f)));
	}

IL_0239:
	{
		int32_t L_81 = V_6;
		V_6 = ((int32_t)((int32_t)L_81-(int32_t)1));
	}

IL_023f:
	{
		int32_t L_82 = V_6;
		if ((((int32_t)L_82) >= ((int32_t)0)))
		{
			goto IL_0202;
		}
	}

IL_0247:
	{
		Transform_t284553113 * L_83 = Component_get_transform_m2452535634(__this, /*hidden argument*/NULL);
		float L_84 = V_2;
		Vector3_t3525329789  L_85;
		memset(&L_85, 0, sizeof(L_85));
		Vector3__ctor_m1243251509(&L_85, ((float)((float)(-0.5f)*(float)L_84)), (3.7f), (8.5f), /*hidden argument*/NULL);
		NullCheck(L_83);
		Transform_set_localPosition_m3778340736(L_83, L_85, /*hidden argument*/NULL);
		return;
	}
}
// System.Void MoneyCounter::UpdateCounter()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Mathf_t1597001355_il2cpp_TypeInfo_var;
extern Il2CppClass* Utils_t82059409_il2cpp_TypeInfo_var;
extern const uint32_t MoneyCounter_UpdateCounter_m3651466800_MetadataUsageId;
extern "C"  void MoneyCounter_UpdateCounter_m3651466800 (MoneyCounter_t199543100 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (MoneyCounter_UpdateCounter_m3651466800_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	String_t* V_0 = NULL;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	double V_3 = 0.0;
	uint16_t V_4 = 0x0;
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_0 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		V_0 = L_0;
		GlobalStat_t1134678199 * L_1 = __this->get_GlobalStat_5();
		NullCheck(L_1);
		double L_2 = L_1->get_money_27();
		double L_3 = fabs(L_2);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t1597001355_il2cpp_TypeInfo_var);
		float L_4 = powf((10.0f), (12.0f));
		if ((!(((double)L_3) < ((double)(((double)((double)L_4)))))))
		{
			goto IL_0049;
		}
	}
	{
		GlobalStat_t1134678199 * L_5 = __this->get_GlobalStat_5();
		NullCheck(L_5);
		double L_6 = L_5->get_money_27();
		double L_7 = bankers_round(L_6);
		V_3 = L_7;
		String_t* L_8 = Double_ToString_m3380246633((&V_3), /*hidden argument*/NULL);
		V_0 = L_8;
		goto IL_005a;
	}

IL_0049:
	{
		GlobalStat_t1134678199 * L_9 = __this->get_GlobalStat_5();
		NullCheck(L_9);
		double L_10 = L_9->get_money_27();
		IL2CPP_RUNTIME_CLASS_INIT(Utils_t82059409_il2cpp_TypeInfo_var);
		String_t* L_11 = Utils_GetNumberRepresentation_m4179076411(NULL /*static, unused*/, L_10, /*hidden argument*/NULL);
		V_0 = L_11;
	}

IL_005a:
	{
		String_t* L_12 = __this->get_currentValue_4();
		String_t* L_13 = V_0;
		NullCheck(L_12);
		bool L_14 = String_Equals_m3541721061(L_12, L_13, /*hidden argument*/NULL);
		if (!L_14)
		{
			goto IL_006c;
		}
	}
	{
		return;
	}

IL_006c:
	{
		String_t* L_15 = V_0;
		__this->set_currentValue_4(L_15);
		V_1 = 0;
		goto IL_007e;
	}

IL_007a:
	{
		int32_t L_16 = V_1;
		V_1 = ((int32_t)((int32_t)L_16+(int32_t)1));
	}

IL_007e:
	{
		String_t* L_17 = __this->get_currentValue_4();
		String_t* L_18 = __this->get_currentValue_4();
		NullCheck(L_18);
		int32_t L_19 = String_get_Length_m2979997331(L_18, /*hidden argument*/NULL);
		int32_t L_20 = V_1;
		NullCheck(L_17);
		uint16_t L_21 = String_get_Chars_m3015341861(L_17, ((int32_t)((int32_t)((int32_t)((int32_t)L_19-(int32_t)1))-(int32_t)L_20)), /*hidden argument*/NULL);
		bool L_22 = MoneyCounter_CheckSymbol_m4024240308(__this, L_21, /*hidden argument*/NULL);
		if (L_22)
		{
			goto IL_007a;
		}
	}
	{
		String_t* L_23 = __this->get_currentValue_4();
		NullCheck(L_23);
		int32_t L_24 = String_get_Length_m2979997331(L_23, /*hidden argument*/NULL);
		int32_t L_25 = V_1;
		MoneyCounter_SetCountersNumber_m3722464103(__this, L_24, L_25, /*hidden argument*/NULL);
		V_2 = 0;
		goto IL_00e6;
	}

IL_00bc:
	{
		List_1_t3419442901 * L_26 = __this->get_counters_3();
		int32_t L_27 = V_2;
		NullCheck(L_26);
		Counter_t2622483932 * L_28 = VirtFuncInvoker1< Counter_t2622483932 *, int32_t >::Invoke(31 /* !0 System.Collections.Generic.List`1<Counter>::get_Item(System.Int32) */, L_26, L_27);
		String_t* L_29 = __this->get_currentValue_4();
		int32_t L_30 = V_2;
		NullCheck(L_29);
		uint16_t L_31 = String_get_Chars_m3015341861(L_29, L_30, /*hidden argument*/NULL);
		V_4 = L_31;
		String_t* L_32 = Char_ToString_m2089191214((&V_4), /*hidden argument*/NULL);
		NullCheck(L_28);
		Counter_SetText_m2360067174(L_28, L_32, /*hidden argument*/NULL);
		int32_t L_33 = V_2;
		V_2 = ((int32_t)((int32_t)L_33+(int32_t)1));
	}

IL_00e6:
	{
		int32_t L_34 = V_2;
		String_t* L_35 = __this->get_currentValue_4();
		NullCheck(L_35);
		int32_t L_36 = String_get_Length_m2979997331(L_35, /*hidden argument*/NULL);
		if ((((int32_t)L_34) < ((int32_t)L_36)))
		{
			goto IL_00bc;
		}
	}
	{
		return;
	}
}
// System.Boolean MoneyCounter::CheckSymbol(System.Char)
extern "C"  bool MoneyCounter_CheckSymbol_m4024240308 (MoneyCounter_t199543100 * __this, uint16_t ___symbol0, const MethodInfo* method)
{
	{
		uint16_t L_0 = ___symbol0;
		if ((((int32_t)L_0) == ((int32_t)((int32_t)66))))
		{
			goto IL_0018;
		}
	}
	{
		uint16_t L_1 = ___symbol0;
		if ((((int32_t)L_1) == ((int32_t)((int32_t)75))))
		{
			goto IL_0018;
		}
	}
	{
		uint16_t L_2 = ___symbol0;
		if ((!(((uint32_t)L_2) == ((uint32_t)((int32_t)77)))))
		{
			goto IL_001a;
		}
	}

IL_0018:
	{
		return (bool)1;
	}

IL_001a:
	{
		return (bool)0;
	}
}
// System.Void MoneyDisplay::.ctor()
extern Il2CppClass* ReactiveProperty_1_t1142849652_il2cpp_TypeInfo_var;
extern const MethodInfo* ReactiveProperty_1__ctor_m3586573700_MethodInfo_var;
extern const uint32_t MoneyDisplay__ctor_m2214971609_MetadataUsageId;
extern "C"  void MoneyDisplay__ctor_m2214971609 (MoneyDisplay_t913476610 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (MoneyDisplay__ctor_m2214971609_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		ReactiveProperty_1_t1142849652 * L_0 = (ReactiveProperty_1_t1142849652 *)il2cpp_codegen_object_new(ReactiveProperty_1_t1142849652_il2cpp_TypeInfo_var);
		ReactiveProperty_1__ctor_m3586573700(L_0, /*hidden argument*/ReactiveProperty_1__ctor_m3586573700_MethodInfo_var);
		__this->set_PasInc_7(L_0);
		MonoBehaviour__ctor_m350695606(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void MoneyDisplay::PostInject()
extern "C"  void MoneyDisplay_PostInject_m747432924 (MoneyDisplay_t913476610 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void MoneyDisplay::Awake()
extern Il2CppClass* Func_2_t2628749132_il2cpp_TypeInfo_var;
extern Il2CppClass* MoneyDisplay_t913476610_il2cpp_TypeInfo_var;
extern Il2CppClass* Func_3_t1933728903_il2cpp_TypeInfo_var;
extern Il2CppClass* Observable_t258071701_il2cpp_TypeInfo_var;
extern Il2CppClass* Action_1_t682969319_il2cpp_TypeInfo_var;
extern const MethodInfo* MoneyDisplay_U3CAwakeU3Em__42_m2048272969_MethodInfo_var;
extern const MethodInfo* Func_2__ctor_m887039693_MethodInfo_var;
extern const MethodInfo* MoneyDisplay_U3CAwakeU3Em__43_m1050540313_MethodInfo_var;
extern const MethodInfo* Func_3__ctor_m2706065971_MethodInfo_var;
extern const MethodInfo* Observable_SelectMany_TisDouble_t534516614_TisDouble_t534516614_TisDouble_t534516614_m83073602_MethodInfo_var;
extern const MethodInfo* MoneyDisplay_U3CAwakeU3Em__44_m936231999_MethodInfo_var;
extern const MethodInfo* Action_1__ctor_m366957451_MethodInfo_var;
extern const MethodInfo* ObservableExtensions_Subscribe_TisDouble_t534516614_m2613047548_MethodInfo_var;
extern const uint32_t MoneyDisplay_Awake_m2452576828_MetadataUsageId;
extern "C"  void MoneyDisplay_Awake_m2452576828 (MoneyDisplay_t913476610 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (MoneyDisplay_Awake_m2452576828_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Il2CppObject* V_0 = NULL;
	Func_2_t2628749132 * G_B2_0 = NULL;
	ReactiveProperty_1_t1142849652 * G_B2_1 = NULL;
	Func_2_t2628749132 * G_B1_0 = NULL;
	ReactiveProperty_1_t1142849652 * G_B1_1 = NULL;
	{
		ReactiveProperty_1_t1142849652 * L_0 = __this->get_PasInc_7();
		IntPtr_t L_1;
		L_1.set_m_value_0((void*)(void*)MoneyDisplay_U3CAwakeU3Em__42_m2048272969_MethodInfo_var);
		Func_2_t2628749132 * L_2 = (Func_2_t2628749132 *)il2cpp_codegen_object_new(Func_2_t2628749132_il2cpp_TypeInfo_var);
		Func_2__ctor_m887039693(L_2, __this, L_1, /*hidden argument*/Func_2__ctor_m887039693_MethodInfo_var);
		Func_3_t1933728903 * L_3 = ((MoneyDisplay_t913476610_StaticFields*)MoneyDisplay_t913476610_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__amU24cache6_8();
		G_B1_0 = L_2;
		G_B1_1 = L_0;
		if (L_3)
		{
			G_B2_0 = L_2;
			G_B2_1 = L_0;
			goto IL_002a;
		}
	}
	{
		IntPtr_t L_4;
		L_4.set_m_value_0((void*)(void*)MoneyDisplay_U3CAwakeU3Em__43_m1050540313_MethodInfo_var);
		Func_3_t1933728903 * L_5 = (Func_3_t1933728903 *)il2cpp_codegen_object_new(Func_3_t1933728903_il2cpp_TypeInfo_var);
		Func_3__ctor_m2706065971(L_5, NULL, L_4, /*hidden argument*/Func_3__ctor_m2706065971_MethodInfo_var);
		((MoneyDisplay_t913476610_StaticFields*)MoneyDisplay_t913476610_il2cpp_TypeInfo_var->static_fields)->set_U3CU3Ef__amU24cache6_8(L_5);
		G_B2_0 = G_B1_0;
		G_B2_1 = G_B1_1;
	}

IL_002a:
	{
		Func_3_t1933728903 * L_6 = ((MoneyDisplay_t913476610_StaticFields*)MoneyDisplay_t913476610_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__amU24cache6_8();
		IL2CPP_RUNTIME_CLASS_INIT(Observable_t258071701_il2cpp_TypeInfo_var);
		Il2CppObject* L_7 = Observable_SelectMany_TisDouble_t534516614_TisDouble_t534516614_TisDouble_t534516614_m83073602(NULL /*static, unused*/, G_B2_1, G_B2_0, L_6, /*hidden argument*/Observable_SelectMany_TisDouble_t534516614_TisDouble_t534516614_TisDouble_t534516614_m83073602_MethodInfo_var);
		V_0 = L_7;
		Il2CppObject* L_8 = V_0;
		IntPtr_t L_9;
		L_9.set_m_value_0((void*)(void*)MoneyDisplay_U3CAwakeU3Em__44_m936231999_MethodInfo_var);
		Action_1_t682969319 * L_10 = (Action_1_t682969319 *)il2cpp_codegen_object_new(Action_1_t682969319_il2cpp_TypeInfo_var);
		Action_1__ctor_m366957451(L_10, __this, L_9, /*hidden argument*/Action_1__ctor_m366957451_MethodInfo_var);
		ObservableExtensions_Subscribe_TisDouble_t534516614_m2613047548(NULL /*static, unused*/, L_8, L_10, /*hidden argument*/ObservableExtensions_Subscribe_TisDouble_t534516614_m2613047548_MethodInfo_var);
		return;
	}
}
// System.Void MoneyDisplay::Update()
extern "C"  void MoneyDisplay_Update_m1671505236 (MoneyDisplay_t913476610 * __this, const MethodInfo* method)
{
	{
		ReactiveProperty_1_t1142849652 * L_0 = __this->get_PasInc_7();
		NullCheck(L_0);
		double L_1 = VirtFuncInvoker0< double >::Invoke(8 /* !0 UniRx.ReactiveProperty`1<System.Double>::get_Value() */, L_0);
		__this->set_aaaaasssss_6(L_1);
		double L_2 = __this->get_aaaaasssss_6();
		GlobalStat_t1134678199 * L_3 = __this->get_GlobalStat_4();
		NullCheck(L_3);
		double L_4 = GlobalStat_get_passiveIncome_m844851078(L_3, /*hidden argument*/NULL);
		if ((((double)L_2) == ((double)L_4)))
		{
			goto IL_003d;
		}
	}
	{
		ReactiveProperty_1_t1142849652 * L_5 = __this->get_PasInc_7();
		GlobalStat_t1134678199 * L_6 = __this->get_GlobalStat_4();
		NullCheck(L_6);
		double L_7 = GlobalStat_get_passiveIncome_m844851078(L_6, /*hidden argument*/NULL);
		NullCheck(L_5);
		VirtActionInvoker1< double >::Invoke(7 /* System.Void UniRx.ReactiveProperty`1<System.Double>::set_Value(!0) */, L_5, L_7);
	}

IL_003d:
	{
		return;
	}
}
// UniRx.IObservable`1<System.Double> MoneyDisplay::<Awake>m__42(System.Double)
extern "C"  Il2CppObject* MoneyDisplay_U3CAwakeU3Em__42_m2048272969 (MoneyDisplay_t913476610 * __this, double ___doubleass0, const MethodInfo* method)
{
	{
		GameController_t2782302542 * L_0 = __this->get_GameController_3();
		NullCheck(L_0);
		ReactiveProperty_1_t1142849652 * L_1 = L_0->get_calculatedIncome_54();
		return L_1;
	}
}
// System.Double MoneyDisplay::<Awake>m__43(System.Double,System.Double)
extern "C"  double MoneyDisplay_U3CAwakeU3Em__43_m1050540313 (Il2CppObject * __this /* static, unused */, double ___doubleass0, double ___ass1, const MethodInfo* method)
{
	{
		double L_0 = ___doubleass0;
		double L_1 = ___ass1;
		return ((double)((double)L_0+(double)L_1));
	}
}
// System.Void MoneyDisplay::<Awake>m__44(System.Double)
extern Il2CppClass* Utils_t82059409_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral32241;
extern Il2CppCodeGenString* _stringLiteral113745;
extern const uint32_t MoneyDisplay_U3CAwakeU3Em__44_m936231999_MetadataUsageId;
extern "C"  void MoneyDisplay_U3CAwakeU3Em__44_m936231999 (MoneyDisplay_t913476610 * __this, double ___varass0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (MoneyDisplay_U3CAwakeU3Em__44_m936231999_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Text_t3286458198 * L_0 = __this->get_MoneyIncome_2();
		OligarchParameters_t821144443 * L_1 = __this->get_OligarchParameters_5();
		NullCheck(L_1);
		String_t* L_2 = L_1->get_MoneySymbol_9();
		double L_3 = ___varass0;
		IL2CPP_RUNTIME_CLASS_INIT(Utils_t82059409_il2cpp_TypeInfo_var);
		String_t* L_4 = Utils_GetNumberRepresentation_m4179076411(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		String_t* L_5 = LocalizationManager_GetString_m738567075(NULL /*static, unused*/, _stringLiteral113745, /*hidden argument*/NULL);
		NullCheck(L_5);
		String_t* L_6 = String_ToUpper_m1841663596(L_5, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_7 = String_Concat_m2933632197(NULL /*static, unused*/, L_2, L_4, _stringLiteral32241, L_6, /*hidden argument*/NULL);
		NullCheck(L_0);
		VirtActionInvoker1< String_t* >::Invoke(65 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_0, L_7);
		return;
	}
}
// System.Void MoneyPack::.ctor()
extern "C"  void MoneyPack__ctor_m3675980466 (MoneyPack_t290305753 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m350695606(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void MoneyPack::Awake()
extern const MethodInfo* Component_GetComponentsInChildren_TisSpriteRenderer_t2223784725_m3166954935_MethodInfo_var;
extern const uint32_t MoneyPack_Awake_m3913585685_MetadataUsageId;
extern "C"  void MoneyPack_Awake_m3913585685 (MoneyPack_t290305753 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (MoneyPack_Awake_m3913585685_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		SpriteRendererU5BU5D_t4109544696* L_0 = Component_GetComponentsInChildren_TisSpriteRenderer_t2223784725_m3166954935(__this, /*hidden argument*/Component_GetComponentsInChildren_TisSpriteRenderer_t2223784725_m3166954935_MethodInfo_var);
		__this->set_notes_2(L_0);
		return;
	}
}
// System.Void MoneyPack::Start()
extern "C"  void MoneyPack_Start_m2623118258 (MoneyPack_t290305753 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void MoneyPack::UpdateSprite(UnityEngine.Sprite,UnityEngine.Sprite,UnityEngine.Sprite)
extern "C"  void MoneyPack_UpdateSprite_m367442564 (MoneyPack_t290305753 * __this, Sprite_t4006040370 * ___spr0, Sprite_t4006040370 * ___spriteMini1, Sprite_t4006040370 * ___spriteMini22, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		SpriteRendererU5BU5D_t4109544696* L_0 = __this->get_notes_2();
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		int32_t L_1 = 0;
		Sprite_t4006040370 * L_2 = ___spr0;
		NullCheck(((L_0)->GetAt(static_cast<il2cpp_array_size_t>(L_1))));
		SpriteRenderer_set_sprite_m1519408453(((L_0)->GetAt(static_cast<il2cpp_array_size_t>(L_1))), L_2, /*hidden argument*/NULL);
		V_0 = 1;
		goto IL_0037;
	}

IL_0015:
	{
		SpriteRendererU5BU5D_t4109544696* L_3 = __this->get_notes_2();
		int32_t L_4 = V_0;
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, L_4);
		int32_t L_5 = L_4;
		Sprite_t4006040370 * L_6 = ___spriteMini1;
		NullCheck(((L_3)->GetAt(static_cast<il2cpp_array_size_t>(L_5))));
		SpriteRenderer_set_sprite_m1519408453(((L_3)->GetAt(static_cast<il2cpp_array_size_t>(L_5))), L_6, /*hidden argument*/NULL);
		SpriteRendererU5BU5D_t4109544696* L_7 = __this->get_notes_2();
		int32_t L_8 = V_0;
		NullCheck(L_7);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_7, ((int32_t)((int32_t)L_8+(int32_t)1)));
		int32_t L_9 = ((int32_t)((int32_t)L_8+(int32_t)1));
		Sprite_t4006040370 * L_10 = ___spriteMini22;
		NullCheck(((L_7)->GetAt(static_cast<il2cpp_array_size_t>(L_9))));
		SpriteRenderer_set_sprite_m1519408453(((L_7)->GetAt(static_cast<il2cpp_array_size_t>(L_9))), L_10, /*hidden argument*/NULL);
		int32_t L_11 = V_0;
		V_0 = ((int32_t)((int32_t)L_11+(int32_t)2));
	}

IL_0037:
	{
		int32_t L_12 = V_0;
		SpriteRendererU5BU5D_t4109544696* L_13 = __this->get_notes_2();
		NullCheck(L_13);
		if ((((int32_t)L_12) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_13)->max_length)))))))
		{
			goto IL_0015;
		}
	}
	{
		return;
	}
}
// System.Void MoneyPackInfo::.ctor()
extern "C"  void MoneyPackInfo__ctor_m3448603172 (MoneyPackInfo_t3013049127 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void MoreMoneyWindow::.ctor()
extern "C"  void MoreMoneyWindow__ctor_m851570608 (MoreMoneyWindow_t918259227 * __this, const MethodInfo* method)
{
	{
		WindowBase__ctor_m2257170106(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void MoreMoneyWindow::Init(System.Int32)
extern "C"  void MoreMoneyWindow_Init_m542722037 (MoreMoneyWindow_t918259227 * __this, int32_t ___packNumber0, const MethodInfo* method)
{
	{
		int32_t L_0 = ___packNumber0;
		MoneyPackInfo_t3013049127 * L_1 = MoreMoneyWindow_GetMoneyPackInfo_m3854564394(__this, L_0, /*hidden argument*/NULL);
		__this->set_info_11(L_1);
		Text_t3286458198 * L_2 = __this->get_name_7();
		MoneyPackInfo_t3013049127 * L_3 = __this->get_info_11();
		NullCheck(L_3);
		String_t* L_4 = L_3->get_name_1();
		NullCheck(L_2);
		VirtActionInvoker1< String_t* >::Invoke(65 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_2, L_4);
		Text_t3286458198 * L_5 = __this->get_price_9();
		MoneyPackInfo_t3013049127 * L_6 = __this->get_info_11();
		NullCheck(L_6);
		String_t* L_7 = L_6->get_price_2();
		NullCheck(L_5);
		VirtActionInvoker1< String_t* >::Invoke(65 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_5, L_7);
		Image_t3354615620 * L_8 = __this->get_icon_8();
		MoneyPackInfo_t3013049127 * L_9 = __this->get_info_11();
		NullCheck(L_9);
		Sprite_t4006040370 * L_10 = L_9->get_icon_0();
		NullCheck(L_8);
		Image_set_sprite_m572551402(L_8, L_10, /*hidden argument*/NULL);
		return;
	}
}
// MoneyPackInfo MoreMoneyWindow::GetMoneyPackInfo(System.Int32)
extern Il2CppClass* MoneyPackInfo_t3013049127_il2cpp_TypeInfo_var;
extern Il2CppClass* StringU5BU5D_t2956870243_il2cpp_TypeInfo_var;
extern Il2CppClass* Utils_t82059409_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* BarygaOpenIABManager_t3621535309_il2cpp_TypeInfo_var;
extern Il2CppClass* ICell_1_t2520119879_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1983188389;
extern Il2CppCodeGenString* _stringLiteral3119644179;
extern Il2CppCodeGenString* _stringLiteral350;
extern Il2CppCodeGenString* _stringLiteral3073084803;
extern Il2CppCodeGenString* _stringLiteral1030;
extern Il2CppCodeGenString* _stringLiteral41;
extern Il2CppCodeGenString* _stringLiteral1860637845;
extern Il2CppCodeGenString* _stringLiteral2601038727;
extern Il2CppCodeGenString* _stringLiteral1477431189;
extern Il2CppCodeGenString* _stringLiteral1072653097;
extern Il2CppCodeGenString* _stringLiteral354670409;
extern Il2CppCodeGenString* _stringLiteral2889351424;
extern Il2CppCodeGenString* _stringLiteral1546678264;
extern Il2CppCodeGenString* _stringLiteral3322789258;
extern const uint32_t MoreMoneyWindow_GetMoneyPackInfo_m3854564394_MetadataUsageId;
extern "C"  MoneyPackInfo_t3013049127 * MoreMoneyWindow_GetMoneyPackInfo_m3854564394 (MoreMoneyWindow_t918259227 * __this, int32_t ___pack0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (MoreMoneyWindow_GetMoneyPackInfo_m3854564394_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	MoneyPackInfo_t3013049127 * V_0 = NULL;
	String_t* V_1 = NULL;
	String_t* V_2 = NULL;
	int32_t V_3 = 0;
	{
		MoneyPackInfo_t3013049127 * L_0 = (MoneyPackInfo_t3013049127 *)il2cpp_codegen_object_new(MoneyPackInfo_t3013049127_il2cpp_TypeInfo_var);
		MoneyPackInfo__ctor_m3448603172(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		V_2 = _stringLiteral1983188389;
		int32_t L_1 = ___pack0;
		V_3 = L_1;
		int32_t L_2 = V_3;
		if (L_2 == 0)
		{
			goto IL_0025;
		}
		if (L_2 == 1)
		{
			goto IL_00b9;
		}
		if (L_2 == 2)
		{
			goto IL_0157;
		}
	}
	{
		goto IL_01f5;
	}

IL_0025:
	{
		StringU5BU5D_t2956870243* L_3 = ((StringU5BU5D_t2956870243*)SZArrayNew(StringU5BU5D_t2956870243_il2cpp_TypeInfo_var, (uint32_t)6));
		String_t* L_4 = LocalizationManager_GetString_m738567075(NULL /*static, unused*/, _stringLiteral3119644179, /*hidden argument*/NULL);
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, 0);
		ArrayElementTypeCheck (L_3, L_4);
		(L_3)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)L_4);
		StringU5BU5D_t2956870243* L_5 = L_3;
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, 1);
		ArrayElementTypeCheck (L_5, _stringLiteral350);
		(L_5)->SetAt(static_cast<il2cpp_array_size_t>(1), (String_t*)_stringLiteral350);
		StringU5BU5D_t2956870243* L_6 = L_5;
		String_t* L_7 = LocalizationManager_GetString_m738567075(NULL /*static, unused*/, _stringLiteral3073084803, /*hidden argument*/NULL);
		NullCheck(L_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_6, 2);
		ArrayElementTypeCheck (L_6, L_7);
		(L_6)->SetAt(static_cast<il2cpp_array_size_t>(2), (String_t*)L_7);
		StringU5BU5D_t2956870243* L_8 = L_6;
		NullCheck(L_8);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_8, 3);
		ArrayElementTypeCheck (L_8, _stringLiteral1030);
		(L_8)->SetAt(static_cast<il2cpp_array_size_t>(3), (String_t*)_stringLiteral1030);
		StringU5BU5D_t2956870243* L_9 = L_8;
		GlobalStat_t1134678199 * L_10 = __this->get_GlobalStat_13();
		NullCheck(L_10);
		double L_11 = GlobalStat_GetBequestReward_m3567060091(L_10, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Utils_t82059409_il2cpp_TypeInfo_var);
		String_t* L_12 = Utils_GetNumberRepresentation_m4179076411(NULL /*static, unused*/, L_11, /*hidden argument*/NULL);
		NullCheck(L_9);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_9, 4);
		ArrayElementTypeCheck (L_9, L_12);
		(L_9)->SetAt(static_cast<il2cpp_array_size_t>(4), (String_t*)L_12);
		StringU5BU5D_t2956870243* L_13 = L_9;
		NullCheck(L_13);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_13, 5);
		ArrayElementTypeCheck (L_13, _stringLiteral41);
		(L_13)->SetAt(static_cast<il2cpp_array_size_t>(5), (String_t*)_stringLiteral41);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_14 = String_Concat_m21867311(NULL /*static, unused*/, L_13, /*hidden argument*/NULL);
		V_1 = L_14;
		MoneyPackInfo_t3013049127 * L_15 = V_0;
		String_t* L_16 = V_1;
		NullCheck(L_15);
		L_15->set_name_1(L_16);
		MoneyPackInfo_t3013049127 * L_17 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(BarygaOpenIABManager_t3621535309_il2cpp_TypeInfo_var);
		Il2CppObject* L_18 = BarygaOpenIABManager_PriceFormatted_m817356499(NULL /*static, unused*/, _stringLiteral1860637845, (bool)1, /*hidden argument*/NULL);
		NullCheck(L_18);
		String_t* L_19 = InterfaceFuncInvoker0< String_t* >::Invoke(2 /* T ICell`1<System.String>::get_value() */, ICell_1_t2520119879_il2cpp_TypeInfo_var, L_18);
		NullCheck(L_17);
		L_17->set_price_2(L_19);
		MoneyPackInfo_t3013049127 * L_20 = V_0;
		IconsProvider_t3884200715 * L_21 = __this->get_IconsProvider_12();
		NullCheck(L_21);
		Sprite_t4006040370 * L_22 = IconsProvider_Get_m1034217325(L_21, _stringLiteral2601038727, /*hidden argument*/NULL);
		NullCheck(L_20);
		L_20->set_icon_0(L_22);
		MoneyPackInfo_t3013049127 * L_23 = V_0;
		NullCheck(L_23);
		L_23->set_marketAction_3(_stringLiteral1860637845);
		goto IL_01f5;
	}

IL_00b9:
	{
		StringU5BU5D_t2956870243* L_24 = ((StringU5BU5D_t2956870243*)SZArrayNew(StringU5BU5D_t2956870243_il2cpp_TypeInfo_var, (uint32_t)6));
		String_t* L_25 = LocalizationManager_GetString_m738567075(NULL /*static, unused*/, _stringLiteral1477431189, /*hidden argument*/NULL);
		NullCheck(L_24);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_24, 0);
		ArrayElementTypeCheck (L_24, L_25);
		(L_24)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)L_25);
		StringU5BU5D_t2956870243* L_26 = L_24;
		NullCheck(L_26);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_26, 1);
		ArrayElementTypeCheck (L_26, _stringLiteral350);
		(L_26)->SetAt(static_cast<il2cpp_array_size_t>(1), (String_t*)_stringLiteral350);
		StringU5BU5D_t2956870243* L_27 = L_26;
		String_t* L_28 = LocalizationManager_GetString_m738567075(NULL /*static, unused*/, _stringLiteral3073084803, /*hidden argument*/NULL);
		NullCheck(L_27);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_27, 2);
		ArrayElementTypeCheck (L_27, L_28);
		(L_27)->SetAt(static_cast<il2cpp_array_size_t>(2), (String_t*)L_28);
		StringU5BU5D_t2956870243* L_29 = L_27;
		NullCheck(L_29);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_29, 3);
		ArrayElementTypeCheck (L_29, _stringLiteral1030);
		(L_29)->SetAt(static_cast<il2cpp_array_size_t>(3), (String_t*)_stringLiteral1030);
		StringU5BU5D_t2956870243* L_30 = L_29;
		GlobalStat_t1134678199 * L_31 = __this->get_GlobalStat_13();
		NullCheck(L_31);
		double L_32 = GlobalStat_GetBequestReward_m3567060091(L_31, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Utils_t82059409_il2cpp_TypeInfo_var);
		String_t* L_33 = Utils_GetNumberRepresentation_m4179076411(NULL /*static, unused*/, ((double)((double)L_32*(double)(5.0))), /*hidden argument*/NULL);
		NullCheck(L_30);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_30, 4);
		ArrayElementTypeCheck (L_30, L_33);
		(L_30)->SetAt(static_cast<il2cpp_array_size_t>(4), (String_t*)L_33);
		StringU5BU5D_t2956870243* L_34 = L_30;
		NullCheck(L_34);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_34, 5);
		ArrayElementTypeCheck (L_34, _stringLiteral41);
		(L_34)->SetAt(static_cast<il2cpp_array_size_t>(5), (String_t*)_stringLiteral41);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_35 = String_Concat_m21867311(NULL /*static, unused*/, L_34, /*hidden argument*/NULL);
		V_1 = L_35;
		MoneyPackInfo_t3013049127 * L_36 = V_0;
		String_t* L_37 = V_1;
		NullCheck(L_36);
		L_36->set_name_1(L_37);
		MoneyPackInfo_t3013049127 * L_38 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(BarygaOpenIABManager_t3621535309_il2cpp_TypeInfo_var);
		Il2CppObject* L_39 = BarygaOpenIABManager_PriceFormatted_m817356499(NULL /*static, unused*/, _stringLiteral1072653097, (bool)1, /*hidden argument*/NULL);
		NullCheck(L_39);
		String_t* L_40 = InterfaceFuncInvoker0< String_t* >::Invoke(2 /* T ICell`1<System.String>::get_value() */, ICell_1_t2520119879_il2cpp_TypeInfo_var, L_39);
		NullCheck(L_38);
		L_38->set_price_2(L_40);
		MoneyPackInfo_t3013049127 * L_41 = V_0;
		IconsProvider_t3884200715 * L_42 = __this->get_IconsProvider_12();
		NullCheck(L_42);
		Sprite_t4006040370 * L_43 = IconsProvider_Get_m1034217325(L_42, _stringLiteral354670409, /*hidden argument*/NULL);
		NullCheck(L_41);
		L_41->set_icon_0(L_43);
		MoneyPackInfo_t3013049127 * L_44 = V_0;
		NullCheck(L_44);
		L_44->set_marketAction_3(_stringLiteral1072653097);
		goto IL_01f5;
	}

IL_0157:
	{
		StringU5BU5D_t2956870243* L_45 = ((StringU5BU5D_t2956870243*)SZArrayNew(StringU5BU5D_t2956870243_il2cpp_TypeInfo_var, (uint32_t)6));
		String_t* L_46 = LocalizationManager_GetString_m738567075(NULL /*static, unused*/, _stringLiteral2889351424, /*hidden argument*/NULL);
		NullCheck(L_45);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_45, 0);
		ArrayElementTypeCheck (L_45, L_46);
		(L_45)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)L_46);
		StringU5BU5D_t2956870243* L_47 = L_45;
		NullCheck(L_47);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_47, 1);
		ArrayElementTypeCheck (L_47, _stringLiteral350);
		(L_47)->SetAt(static_cast<il2cpp_array_size_t>(1), (String_t*)_stringLiteral350);
		StringU5BU5D_t2956870243* L_48 = L_47;
		String_t* L_49 = LocalizationManager_GetString_m738567075(NULL /*static, unused*/, _stringLiteral3073084803, /*hidden argument*/NULL);
		NullCheck(L_48);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_48, 2);
		ArrayElementTypeCheck (L_48, L_49);
		(L_48)->SetAt(static_cast<il2cpp_array_size_t>(2), (String_t*)L_49);
		StringU5BU5D_t2956870243* L_50 = L_48;
		NullCheck(L_50);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_50, 3);
		ArrayElementTypeCheck (L_50, _stringLiteral1030);
		(L_50)->SetAt(static_cast<il2cpp_array_size_t>(3), (String_t*)_stringLiteral1030);
		StringU5BU5D_t2956870243* L_51 = L_50;
		GlobalStat_t1134678199 * L_52 = __this->get_GlobalStat_13();
		NullCheck(L_52);
		double L_53 = GlobalStat_GetBequestReward_m3567060091(L_52, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Utils_t82059409_il2cpp_TypeInfo_var);
		String_t* L_54 = Utils_GetNumberRepresentation_m4179076411(NULL /*static, unused*/, ((double)((double)L_53*(double)(20.0))), /*hidden argument*/NULL);
		NullCheck(L_51);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_51, 4);
		ArrayElementTypeCheck (L_51, L_54);
		(L_51)->SetAt(static_cast<il2cpp_array_size_t>(4), (String_t*)L_54);
		StringU5BU5D_t2956870243* L_55 = L_51;
		NullCheck(L_55);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_55, 5);
		ArrayElementTypeCheck (L_55, _stringLiteral41);
		(L_55)->SetAt(static_cast<il2cpp_array_size_t>(5), (String_t*)_stringLiteral41);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_56 = String_Concat_m21867311(NULL /*static, unused*/, L_55, /*hidden argument*/NULL);
		V_1 = L_56;
		MoneyPackInfo_t3013049127 * L_57 = V_0;
		String_t* L_58 = V_1;
		NullCheck(L_57);
		L_57->set_name_1(L_58);
		MoneyPackInfo_t3013049127 * L_59 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(BarygaOpenIABManager_t3621535309_il2cpp_TypeInfo_var);
		Il2CppObject* L_60 = BarygaOpenIABManager_PriceFormatted_m817356499(NULL /*static, unused*/, _stringLiteral1546678264, (bool)1, /*hidden argument*/NULL);
		NullCheck(L_60);
		String_t* L_61 = InterfaceFuncInvoker0< String_t* >::Invoke(2 /* T ICell`1<System.String>::get_value() */, ICell_1_t2520119879_il2cpp_TypeInfo_var, L_60);
		NullCheck(L_59);
		L_59->set_price_2(L_61);
		MoneyPackInfo_t3013049127 * L_62 = V_0;
		IconsProvider_t3884200715 * L_63 = __this->get_IconsProvider_12();
		NullCheck(L_63);
		Sprite_t4006040370 * L_64 = IconsProvider_Get_m1034217325(L_63, _stringLiteral3322789258, /*hidden argument*/NULL);
		NullCheck(L_62);
		L_62->set_icon_0(L_64);
		MoneyPackInfo_t3013049127 * L_65 = V_0;
		NullCheck(L_65);
		L_65->set_marketAction_3(_stringLiteral1546678264);
		goto IL_01f5;
	}

IL_01f5:
	{
		MoneyPackInfo_t3013049127 * L_66 = V_0;
		return L_66;
	}
}
// System.Void MoreMoneyWindow::Click()
extern "C"  void MoreMoneyWindow_Click_m2556418134 (MoreMoneyWindow_t918259227 * __this, const MethodInfo* method)
{
	{
		TableButtons_t1868573683 * L_0 = __this->get_tableButtons_6();
		MoneyPackInfo_t3013049127 * L_1 = __this->get_info_11();
		NullCheck(L_1);
		String_t* L_2 = L_1->get_marketAction_3();
		NullCheck(L_0);
		TableButtons_OpenIabRequest_m1009737559(L_0, L_2, /*hidden argument*/NULL);
		WindowManager_t3316821373 * L_3 = ((WindowBase_t3855465217 *)__this)->get_managerInstance_2();
		NullCheck(L_3);
		WindowManager_CloseTopmost_m2019334966(L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Void MultiplicativeSubstance::.ctor()
extern const MethodInfo* SubstanceBase_2__ctor_m3897707208_MethodInfo_var;
extern const uint32_t MultiplicativeSubstance__ctor_m4202305297_MetadataUsageId;
extern "C"  void MultiplicativeSubstance__ctor_m4202305297 (MultiplicativeSubstance_t581301658 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (MultiplicativeSubstance__ctor_m4202305297_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		SubstanceBase_2__ctor_m3897707208(__this, (1.0f), /*hidden argument*/SubstanceBase_2__ctor_m3897707208_MethodInfo_var);
		return;
	}
}
// System.Void MultiplicativeSubstance::.ctor(System.Single)
extern const MethodInfo* SubstanceBase_2__ctor_m3897707208_MethodInfo_var;
extern const uint32_t MultiplicativeSubstance__ctor_m201371194_MetadataUsageId;
extern "C"  void MultiplicativeSubstance__ctor_m201371194 (MultiplicativeSubstance_t581301658 * __this, float ___val0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (MultiplicativeSubstance__ctor_m201371194_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		float L_0 = ___val0;
		SubstanceBase_2__ctor_m3897707208(__this, L_0, /*hidden argument*/SubstanceBase_2__ctor_m3897707208_MethodInfo_var);
		return;
	}
}
// System.Single MultiplicativeSubstance::Result()
extern Il2CppClass* MultiplicativeSubstance_t581301658_il2cpp_TypeInfo_var;
extern Il2CppClass* Func_3_t3383474419_il2cpp_TypeInfo_var;
extern const MethodInfo* MultiplicativeSubstance_U3CResultU3Em__1DB_m2994426172_MethodInfo_var;
extern const MethodInfo* Func_3__ctor_m2959012258_MethodInfo_var;
extern const MethodInfo* Enumerable_Aggregate_TisIntrusion_t78896424_TisSingle_t958209021_m718774751_MethodInfo_var;
extern const uint32_t MultiplicativeSubstance_Result_m3752482084_MetadataUsageId;
extern "C"  float MultiplicativeSubstance_Result_m3752482084 (MultiplicativeSubstance_t581301658 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (MultiplicativeSubstance_Result_m3752482084_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	float G_B4_0 = 0.0f;
	List_1_t875855393 * G_B4_1 = NULL;
	float G_B3_0 = 0.0f;
	List_1_t875855393 * G_B3_1 = NULL;
	{
		List_1_t875855393 * L_0 = ((SubstanceBase_2_t1869245964 *)__this)->get_intrusions_5();
		if (L_0)
		{
			goto IL_0012;
		}
	}
	{
		float L_1 = ((SubstanceBase_2_t1869245964 *)__this)->get_localBase_0();
		return L_1;
	}

IL_0012:
	{
		List_1_t875855393 * L_2 = ((SubstanceBase_2_t1869245964 *)__this)->get_intrusions_5();
		float L_3 = ((SubstanceBase_2_t1869245964 *)__this)->get_localBase_0();
		Func_3_t3383474419 * L_4 = ((MultiplicativeSubstance_t581301658_StaticFields*)MultiplicativeSubstance_t581301658_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__amU24cache0_6();
		G_B3_0 = L_3;
		G_B3_1 = L_2;
		if (L_4)
		{
			G_B4_0 = L_3;
			G_B4_1 = L_2;
			goto IL_0036;
		}
	}
	{
		IntPtr_t L_5;
		L_5.set_m_value_0((void*)(void*)MultiplicativeSubstance_U3CResultU3Em__1DB_m2994426172_MethodInfo_var);
		Func_3_t3383474419 * L_6 = (Func_3_t3383474419 *)il2cpp_codegen_object_new(Func_3_t3383474419_il2cpp_TypeInfo_var);
		Func_3__ctor_m2959012258(L_6, NULL, L_5, /*hidden argument*/Func_3__ctor_m2959012258_MethodInfo_var);
		((MultiplicativeSubstance_t581301658_StaticFields*)MultiplicativeSubstance_t581301658_il2cpp_TypeInfo_var->static_fields)->set_U3CU3Ef__amU24cache0_6(L_6);
		G_B4_0 = G_B3_0;
		G_B4_1 = G_B3_1;
	}

IL_0036:
	{
		Func_3_t3383474419 * L_7 = ((MultiplicativeSubstance_t581301658_StaticFields*)MultiplicativeSubstance_t581301658_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__amU24cache0_6();
		float L_8 = Enumerable_Aggregate_TisIntrusion_t78896424_TisSingle_t958209021_m718774751(NULL /*static, unused*/, G_B4_1, G_B4_0, L_7, /*hidden argument*/Enumerable_Aggregate_TisIntrusion_t78896424_TisSingle_t958209021_m718774751_MethodInfo_var);
		return L_8;
	}
}
// System.Single MultiplicativeSubstance::<Result>m__1DB(System.Single,SubstanceBase`2/Intrusion<System.Single,System.Single>)
extern "C"  float MultiplicativeSubstance_U3CResultU3Em__1DB_m2994426172 (Il2CppObject * __this /* static, unused */, float ___current0, Intrusion_t78896424 * ___intr1, const MethodInfo* method)
{
	{
		float L_0 = ___current0;
		Intrusion_t78896424 * L_1 = ___intr1;
		NullCheck(L_1);
		float L_2 = L_1->get_influence_1();
		return ((float)((float)L_0*(float)L_2));
	}
}
// System.Void NewTableCell::.ctor()
extern "C"  void NewTableCell__ctor_m527136747 (NewTableCell_t2774679728 * __this, const MethodInfo* method)
{
	{
		InstantiatableInPool__ctor_m836060997(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.RectTransform NewTableCell::get_rect()
extern const MethodInfo* Component_GetComponent_TisRectTransform_t3317474837_m1940403147_MethodInfo_var;
extern const uint32_t NewTableCell_get_rect_m2800814120_MetadataUsageId;
extern "C"  RectTransform_t3317474837 * NewTableCell_get_rect_m2800814120 (NewTableCell_t2774679728 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (NewTableCell_get_rect_m2800814120_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		RectTransform_t3317474837 * L_0 = __this->get__rectTransform_5();
		bool L_1 = Object_op_Equality_m1690293457(NULL /*static, unused*/, L_0, (Object_t3878351788 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_001d;
		}
	}
	{
		RectTransform_t3317474837 * L_2 = Component_GetComponent_TisRectTransform_t3317474837_m1940403147(__this, /*hidden argument*/Component_GetComponent_TisRectTransform_t3317474837_m1940403147_MethodInfo_var);
		__this->set__rectTransform_5(L_2);
	}

IL_001d:
	{
		RectTransform_t3317474837 * L_3 = __this->get__rectTransform_5();
		return L_3;
	}
}
// System.Single NewTableCell::GetSize()
extern "C"  float NewTableCell_GetSize_m686112628 (NewTableCell_t2774679728 * __this, const MethodInfo* method)
{
	Vector2_t3525329788  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Vector3_t3525329789  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		RectTransform_t3317474837 * L_0 = NewTableCell_get_rect_m2800814120(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		Vector2_t3525329788  L_1 = RectTransform_get_sizeDelta_m4279424984(L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		float L_2 = (&V_0)->get_y_2();
		RectTransform_t3317474837 * L_3 = NewTableCell_get_rect_m2800814120(__this, /*hidden argument*/NULL);
		NullCheck(L_3);
		Vector3_t3525329789  L_4 = Transform_get_localScale_m3135622958(L_3, /*hidden argument*/NULL);
		V_1 = L_4;
		float L_5 = (&V_1)->get_y_2();
		return ((float)((float)L_2*(float)L_5));
	}
}
// System.Void NewTableTestCell::.ctor()
extern "C"  void NewTableTestCell__ctor_m2166590329 (NewTableTestCell_t2791901538 * __this, const MethodInfo* method)
{
	{
		NewTableCell__ctor_m527136747(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void NewTableView::.ctor()
extern Il2CppClass* Cell_1_t1140306653_il2cpp_TypeInfo_var;
extern Il2CppClass* List_1_t3225501569_il2cpp_TypeInfo_var;
extern Il2CppClass* Dictionary_2_t2474804324_il2cpp_TypeInfo_var;
extern const MethodInfo* Cell_1__ctor_m1359945515_MethodInfo_var;
extern const MethodInfo* List_1__ctor_m2826817319_MethodInfo_var;
extern const MethodInfo* Dictionary_2__ctor_m2887377984_MethodInfo_var;
extern const uint32_t NewTableView__ctor_m1590854152_MetadataUsageId;
extern "C"  void NewTableView__ctor_m1590854152 (NewTableView_t2775249395 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (NewTableView__ctor_m1590854152_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Cell_1_t1140306653 * L_0 = (Cell_1_t1140306653 *)il2cpp_codegen_object_new(Cell_1_t1140306653_il2cpp_TypeInfo_var);
		Cell_1__ctor_m1359945515(L_0, /*hidden argument*/Cell_1__ctor_m1359945515_MethodInfo_var);
		__this->set_scrollPosition_3(L_0);
		List_1_t3225501569 * L_1 = (List_1_t3225501569 *)il2cpp_codegen_object_new(List_1_t3225501569_il2cpp_TypeInfo_var);
		List_1__ctor_m2826817319(L_1, /*hidden argument*/List_1__ctor_m2826817319_MethodInfo_var);
		__this->set_blocks_4(L_1);
		Dictionary_2_t2474804324 * L_2 = (Dictionary_2_t2474804324 *)il2cpp_codegen_object_new(Dictionary_2_t2474804324_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m2887377984(L_2, /*hidden argument*/Dictionary_2__ctor_m2887377984_MethodInfo_var);
		__this->set_pools_5(L_2);
		MonoBehaviour__ctor_m350695606(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void NewTableView::Awake()
extern const MethodInfo* Component_GetComponent_TisScrollRect_t1048578170_m2560181989_MethodInfo_var;
extern const MethodInfo* Component_GetComponent_TisRectTransform_t3317474837_m1940403147_MethodInfo_var;
extern const uint32_t NewTableView_Awake_m1828459371_MetadataUsageId;
extern "C"  void NewTableView_Awake_m1828459371 (NewTableView_t2775249395 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (NewTableView_Awake_m1828459371_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		ScrollRect_t1048578170 * L_0 = Component_GetComponent_TisScrollRect_t1048578170_m2560181989(__this, /*hidden argument*/Component_GetComponent_TisScrollRect_t1048578170_m2560181989_MethodInfo_var);
		__this->set_scrollRect_2(L_0);
		RectTransform_t3317474837 * L_1 = Component_GetComponent_TisRectTransform_t3317474837_m1940403147(__this, /*hidden argument*/Component_GetComponent_TisRectTransform_t3317474837_m1940403147_MethodInfo_var);
		__this->set_scrollRectTransform_8(L_1);
		return;
	}
}
// System.Void NewTableView::OnEnable()
extern Il2CppClass* UnityAction_1_t3505791693_il2cpp_TypeInfo_var;
extern const MethodInfo* NewTableView_ScrollChanged_m2876431117_MethodInfo_var;
extern const MethodInfo* UnityAction_1__ctor_m3716797755_MethodInfo_var;
extern const MethodInfo* UnityEvent_1_AddListener_m1132237103_MethodInfo_var;
extern const uint32_t NewTableView_OnEnable_m243509374_MetadataUsageId;
extern "C"  void NewTableView_OnEnable_m243509374 (NewTableView_t2775249395 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (NewTableView_OnEnable_m243509374_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		ScrollRect_t1048578170 * L_0 = __this->get_scrollRect_2();
		NullCheck(L_0);
		ScrollRectEvent_t2675965609 * L_1 = ScrollRect_get_onValueChanged_m4119396560(L_0, /*hidden argument*/NULL);
		IntPtr_t L_2;
		L_2.set_m_value_0((void*)(void*)NewTableView_ScrollChanged_m2876431117_MethodInfo_var);
		UnityAction_1_t3505791693 * L_3 = (UnityAction_1_t3505791693 *)il2cpp_codegen_object_new(UnityAction_1_t3505791693_il2cpp_TypeInfo_var);
		UnityAction_1__ctor_m3716797755(L_3, __this, L_2, /*hidden argument*/UnityAction_1__ctor_m3716797755_MethodInfo_var);
		NullCheck(L_1);
		UnityEvent_1_AddListener_m1132237103(L_1, L_3, /*hidden argument*/UnityEvent_1_AddListener_m1132237103_MethodInfo_var);
		return;
	}
}
// System.Void NewTableView::OnDisable()
extern Il2CppClass* UnityAction_1_t3505791693_il2cpp_TypeInfo_var;
extern const MethodInfo* NewTableView_ScrollChanged_m2876431117_MethodInfo_var;
extern const MethodInfo* UnityAction_1__ctor_m3716797755_MethodInfo_var;
extern const MethodInfo* UnityEvent_1_RemoveListener_m2248001854_MethodInfo_var;
extern const uint32_t NewTableView_OnDisable_m3694760559_MetadataUsageId;
extern "C"  void NewTableView_OnDisable_m3694760559 (NewTableView_t2775249395 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (NewTableView_OnDisable_m3694760559_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		ScrollRect_t1048578170 * L_0 = __this->get_scrollRect_2();
		NullCheck(L_0);
		ScrollRectEvent_t2675965609 * L_1 = ScrollRect_get_onValueChanged_m4119396560(L_0, /*hidden argument*/NULL);
		IntPtr_t L_2;
		L_2.set_m_value_0((void*)(void*)NewTableView_ScrollChanged_m2876431117_MethodInfo_var);
		UnityAction_1_t3505791693 * L_3 = (UnityAction_1_t3505791693 *)il2cpp_codegen_object_new(UnityAction_1_t3505791693_il2cpp_TypeInfo_var);
		UnityAction_1__ctor_m3716797755(L_3, __this, L_2, /*hidden argument*/UnityAction_1__ctor_m3716797755_MethodInfo_var);
		NullCheck(L_1);
		UnityEvent_1_RemoveListener_m2248001854(L_1, L_3, /*hidden argument*/UnityEvent_1_RemoveListener_m2248001854_MethodInfo_var);
		return;
	}
}
// System.Void NewTableView::ScrollChanged(UnityEngine.Vector2)
extern "C"  void NewTableView_ScrollChanged_m2876431117 (NewTableView_t2775249395 * __this, Vector2_t3525329788  ___position0, const MethodInfo* method)
{
	{
		Cell_1_t1140306653 * L_0 = __this->get_scrollPosition_3();
		float L_1 = (&___position0)->get_y_2();
		NullCheck(L_0);
		VirtActionInvoker1< float >::Invoke(12 /* System.Void Cell`1<System.Single>::set_value(T) */, L_0, ((float)((float)(1.0f)-(float)L_1)));
		NewTableView_NeedRebuild_m2127486155(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void NewTableView::Clear()
extern Il2CppClass* ITableBlock_t2428542600_il2cpp_TypeInfo_var;
extern const uint32_t NewTableView_Clear_m3291954739_MetadataUsageId;
extern "C"  void NewTableView_Clear_m3291954739 (NewTableView_t2775249395 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (NewTableView_Clear_m3291954739_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	{
		List_1_t3225501569 * L_0 = __this->get_blocks_4();
		NullCheck(L_0);
		int32_t L_1 = VirtFuncInvoker0< int32_t >::Invoke(20 /* System.Int32 System.Collections.Generic.List`1<ITableBlock>::get_Count() */, L_0);
		V_0 = ((int32_t)((int32_t)L_1-(int32_t)1));
		goto IL_0028;
	}

IL_0013:
	{
		List_1_t3225501569 * L_2 = __this->get_blocks_4();
		int32_t L_3 = V_0;
		NullCheck(L_2);
		Il2CppObject * L_4 = VirtFuncInvoker1< Il2CppObject *, int32_t >::Invoke(31 /* !0 System.Collections.Generic.List`1<ITableBlock>::get_Item(System.Int32) */, L_2, L_3);
		NullCheck(L_4);
		InterfaceActionInvoker0::Invoke(0 /* System.Void ITableBlock::Dispose() */, ITableBlock_t2428542600_il2cpp_TypeInfo_var, L_4);
		int32_t L_5 = V_0;
		V_0 = ((int32_t)((int32_t)L_5-(int32_t)1));
	}

IL_0028:
	{
		int32_t L_6 = V_0;
		if ((((int32_t)L_6) >= ((int32_t)0)))
		{
			goto IL_0013;
		}
	}
	{
		return;
	}
}
// System.Void NewTableView::RemoveBlock(ITableBlock)
extern "C"  void NewTableView_RemoveBlock_m191244155 (NewTableView_t2775249395 * __this, Il2CppObject * ___tableBlock0, const MethodInfo* method)
{
	{
		List_1_t3225501569 * L_0 = __this->get_blocks_4();
		Il2CppObject * L_1 = ___tableBlock0;
		NullCheck(L_0);
		VirtFuncInvoker1< bool, Il2CppObject * >::Invoke(26 /* System.Boolean System.Collections.Generic.List`1<ITableBlock>::Remove(!0) */, L_0, L_1);
		return;
	}
}
// System.Void NewTableView::Update()
extern "C"  void NewTableView_Update_m3798700549 (NewTableView_t2775249395 * __this, const MethodInfo* method)
{
	{
		NewTableView_Rebuild_m4014602049(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void NewTableView::Rebuild()
extern Il2CppClass* NewTableView_t2775249395_il2cpp_TypeInfo_var;
extern Il2CppClass* Func_2_t4022284381_il2cpp_TypeInfo_var;
extern Il2CppClass* ITableBlock_t2428542600_il2cpp_TypeInfo_var;
extern Il2CppClass* Enumerator_t1311284561_il2cpp_TypeInfo_var;
extern Il2CppClass* IDisposable_t1628921374_il2cpp_TypeInfo_var;
extern const MethodInfo* NewTableView_U3CRebuildU3Em__132_m245028846_MethodInfo_var;
extern const MethodInfo* Func_2__ctor_m3726615913_MethodInfo_var;
extern const MethodInfo* Enumerable_Sum_TisITableBlock_t2428542600_m3849749412_MethodInfo_var;
extern const MethodInfo* List_1_GetEnumerator_m177192106_MethodInfo_var;
extern const MethodInfo* Enumerator_get_Current_m919133802_MethodInfo_var;
extern const MethodInfo* Enumerator_MoveNext_m1177741414_MethodInfo_var;
extern const uint32_t NewTableView_Rebuild_m4014602049_MetadataUsageId;
extern "C"  void NewTableView_Rebuild_m4014602049 (NewTableView_t2775249395 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (NewTableView_Rebuild_m4014602049_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	float V_0 = 0.0f;
	Vector2_t3525329788  V_1;
	memset(&V_1, 0, sizeof(V_1));
	float V_2 = 0.0f;
	float V_3 = 0.0f;
	int32_t V_4 = 0;
	Il2CppObject * V_5 = NULL;
	Enumerator_t1311284561  V_6;
	memset(&V_6, 0, sizeof(V_6));
	Rect_t1525428817  V_7;
	memset(&V_7, 0, sizeof(V_7));
	Vector2_t3525329788  V_8;
	memset(&V_8, 0, sizeof(V_8));
	Vector2_t3525329788  V_9;
	memset(&V_9, 0, sizeof(V_9));
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	List_1_t3225501569 * G_B2_0 = NULL;
	List_1_t3225501569 * G_B1_0 = NULL;
	{
		__this->set_needRebuild_7((bool)0);
		List_1_t3225501569 * L_0 = __this->get_blocks_4();
		Func_2_t4022284381 * L_1 = ((NewTableView_t2775249395_StaticFields*)NewTableView_t2775249395_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__amU24cache9_11();
		G_B1_0 = L_0;
		if (L_1)
		{
			G_B2_0 = L_0;
			goto IL_0025;
		}
	}
	{
		IntPtr_t L_2;
		L_2.set_m_value_0((void*)(void*)NewTableView_U3CRebuildU3Em__132_m245028846_MethodInfo_var);
		Func_2_t4022284381 * L_3 = (Func_2_t4022284381 *)il2cpp_codegen_object_new(Func_2_t4022284381_il2cpp_TypeInfo_var);
		Func_2__ctor_m3726615913(L_3, NULL, L_2, /*hidden argument*/Func_2__ctor_m3726615913_MethodInfo_var);
		((NewTableView_t2775249395_StaticFields*)NewTableView_t2775249395_il2cpp_TypeInfo_var->static_fields)->set_U3CU3Ef__amU24cache9_11(L_3);
		G_B2_0 = G_B1_0;
	}

IL_0025:
	{
		Func_2_t4022284381 * L_4 = ((NewTableView_t2775249395_StaticFields*)NewTableView_t2775249395_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__amU24cache9_11();
		float L_5 = Enumerable_Sum_TisITableBlock_t2428542600_m3849749412(NULL /*static, unused*/, G_B2_0, L_4, /*hidden argument*/Enumerable_Sum_TisITableBlock_t2428542600_m3849749412_MethodInfo_var);
		float L_6 = __this->get_topOffset_9();
		float L_7 = __this->get_bottomOffset_10();
		V_0 = ((float)((float)((float)((float)L_5+(float)L_6))+(float)L_7));
		RectTransform_t3317474837 * L_8 = __this->get_containerRect_6();
		NullCheck(L_8);
		Vector2_t3525329788  L_9 = RectTransform_get_sizeDelta_m4279424984(L_8, /*hidden argument*/NULL);
		V_1 = L_9;
		float L_10 = V_0;
		(&V_1)->set_y_2(L_10);
		RectTransform_t3317474837 * L_11 = __this->get_containerRect_6();
		Vector2_t3525329788  L_12 = V_1;
		NullCheck(L_11);
		RectTransform_set_sizeDelta_m1223846609(L_11, L_12, /*hidden argument*/NULL);
		float L_13 = __this->get_topOffset_9();
		V_2 = L_13;
		RectTransform_t3317474837 * L_14 = __this->get_scrollRectTransform_8();
		NullCheck(L_14);
		Rect_t1525428817  L_15 = RectTransform_get_rect_m1566017036(L_14, /*hidden argument*/NULL);
		V_7 = L_15;
		Vector2_t3525329788  L_16 = Rect_get_size_m136480416((&V_7), /*hidden argument*/NULL);
		V_8 = L_16;
		float L_17 = (&V_8)->get_y_2();
		V_3 = L_17;
		V_4 = 0;
		List_1_t3225501569 * L_18 = __this->get_blocks_4();
		NullCheck(L_18);
		Enumerator_t1311284561  L_19 = List_1_GetEnumerator_m177192106(L_18, /*hidden argument*/List_1_GetEnumerator_m177192106_MethodInfo_var);
		V_6 = L_19;
	}

IL_0093:
	try
	{ // begin try (depth: 1)
		{
			goto IL_00c3;
		}

IL_0098:
		{
			Il2CppObject * L_20 = Enumerator_get_Current_m919133802((&V_6), /*hidden argument*/Enumerator_get_Current_m919133802_MethodInfo_var);
			V_5 = L_20;
			Il2CppObject * L_21 = V_5;
			RectTransform_t3317474837 * L_22 = __this->get_containerRect_6();
			NullCheck(L_22);
			Vector2_t3525329788  L_23 = RectTransform_get_anchoredPosition_m2546422825(L_22, /*hidden argument*/NULL);
			V_9 = L_23;
			float L_24 = (&V_9)->get_y_2();
			NullCheck(L_21);
			InterfaceActionInvoker4< float*, float*, float, int32_t* >::Invoke(2 /* System.Void ITableBlock::TryShowCells(System.Single&,System.Single&,System.Single,System.Int32&) */, ITableBlock_t2428542600_il2cpp_TypeInfo_var, L_21, (&V_3), (&V_2), ((-L_24)), (&V_4));
		}

IL_00c3:
		{
			bool L_25 = Enumerator_MoveNext_m1177741414((&V_6), /*hidden argument*/Enumerator_MoveNext_m1177741414_MethodInfo_var);
			if (L_25)
			{
				goto IL_0098;
			}
		}

IL_00cf:
		{
			IL2CPP_LEAVE(0xE1, FINALLY_00d4);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_00d4;
	}

FINALLY_00d4:
	{ // begin finally (depth: 1)
		Enumerator_t1311284561  L_26 = V_6;
		Enumerator_t1311284561  L_27 = L_26;
		Il2CppObject * L_28 = Box(Enumerator_t1311284561_il2cpp_TypeInfo_var, &L_27);
		NullCheck((Il2CppObject *)L_28);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, (Il2CppObject *)L_28);
		IL2CPP_END_FINALLY(212)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(212)
	{
		IL2CPP_JUMP_TBL(0xE1, IL_00e1)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_00e1:
	{
		return;
	}
}
// System.Void NewTableView::NeedRebuild()
extern "C"  void NewTableView_NeedRebuild_m2127486155 (NewTableView_t2775249395 * __this, const MethodInfo* method)
{
	{
		__this->set_needRebuild_7((bool)1);
		return;
	}
}
// System.Single NewTableView::<Rebuild>m__132(ITableBlock)
extern Il2CppClass* ITableBlock_t2428542600_il2cpp_TypeInfo_var;
extern const uint32_t NewTableView_U3CRebuildU3Em__132_m245028846_MetadataUsageId;
extern "C"  float NewTableView_U3CRebuildU3Em__132_m245028846 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___block0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (NewTableView_U3CRebuildU3Em__132_m245028846_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject * L_0 = ___block0;
		NullCheck(L_0);
		float L_1 = InterfaceFuncInvoker0< float >::Invoke(1 /* System.Single ITableBlock::GetSize() */, ITableBlock_t2428542600_il2cpp_TypeInfo_var, L_0);
		return L_1;
	}
}
// System.Void NoL18n::.ctor()
extern "C"  void NoL18n__ctor_m1841855327 (NoL18n_t2337897660 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m350695606(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void NoL18n::Start()
extern const MethodInfo* GameObject_GetComponent_TisText_t3286458198_m202917489_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral110;
extern const uint32_t NoL18n_Start_m788993119_MetadataUsageId;
extern "C"  void NoL18n_Start_m788993119 (NoL18n_t2337897660 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (NoL18n_Start_m788993119_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		GameObject_t4012695102 * L_0 = Component_get_gameObject_m2112202034(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		Text_t3286458198 * L_1 = GameObject_GetComponent_TisText_t3286458198_m202917489(L_0, /*hidden argument*/GameObject_GetComponent_TisText_t3286458198_m202917489_MethodInfo_var);
		String_t* L_2 = LocalizationManager_GetString_m738567075(NULL /*static, unused*/, _stringLiteral110, /*hidden argument*/NULL);
		NullCheck(L_1);
		VirtActionInvoker1< String_t* >::Invoke(65 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_1, L_2);
		return;
	}
}
// System.Void NoL18n::Update()
extern "C"  void NoL18n_Update_m2989802382 (NoL18n_t2337897660 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void OligarchParameters::.ctor()
extern Il2CppCodeGenString* _stringLiteral1056;
extern const uint32_t OligarchParameters__ctor_m2486046592_MetadataUsageId;
extern "C"  void OligarchParameters__ctor_m2486046592 (OligarchParameters_t821144443 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (OligarchParameters__ctor_m2486046592_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		__this->set_ActivateTopColorChangingBySpeed_0((bool)1);
		__this->set_BucketIsActive_1((bool)1);
		__this->set_AchivIsActive_2((bool)1);
		__this->set_NeedDonateButtonsInTable_3((bool)1);
		__this->set_OffsetForTableWithPremium_7((-161.0f));
		__this->set_OffsetForTableWithoutPremium_8((-71.0f));
		__this->set_MoneySymbol_9(_stringLiteral1056);
		Color_t1588175760  L_0 = Color_get_white_m2348858526(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_TopPanelFromColor_10(L_0);
		Color_t1588175760  L_1;
		memset(&L_1, 0, sizeof(L_1));
		Color__ctor_m4005717549(&L_1, (1.0f), (0.25f), (0.25f), (1.0f), /*hidden argument*/NULL);
		__this->set_TopPanelToColor_11(L_1);
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean OligarchParameters::IsIpad()
extern "C"  bool OligarchParameters_IsIpad_m1177273484 (OligarchParameters_t821144443 * __this, const MethodInfo* method)
{
	bool V_0 = false;
	{
		int32_t L_0 = Screen_get_width_m1424489183(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_1 = Screen_get_height_m1713306064(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = (bool)((((int32_t)((!(((float)((float)((float)(((float)((float)L_0)))/(float)(((float)((float)L_1)))))) >= ((float)(0.749f))))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		bool L_2 = V_0;
		return L_2;
	}
}
// System.Void OnceEmptyStream::.ctor()
extern "C"  void OnceEmptyStream__ctor_m3370738463 (OnceEmptyStream_t3081939404 * __this, const MethodInfo* method)
{
	{
		EmptyStream__ctor_m2786205438(__this, /*hidden argument*/NULL);
		((Stream_1_t1249425060 *)__this)->set_autoDisconnectAfterEvent_4((bool)1);
		return;
	}
}
// System.Void OrganismExtension::Arise(IOrganism,IOrganism,IRoot)
extern Il2CppClass* IOrganism_t2580843579_il2cpp_TypeInfo_var;
extern const uint32_t OrganismExtension_Arise_m4189093251_MetadataUsageId;
extern "C"  void OrganismExtension_Arise_m4189093251 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___self0, Il2CppObject * ___carrier1, Il2CppObject * ___root2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (OrganismExtension_Arise_m4189093251_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject * L_0 = ___self0;
		Il2CppObject * L_1 = ___carrier1;
		Il2CppObject * L_2 = ___root2;
		NullCheck(L_0);
		InterfaceActionInvoker2< Il2CppObject *, Il2CppObject * >::Invoke(0 /* System.Void IOrganism::Setup(IOrganism,IRoot) */, IOrganism_t2580843579_il2cpp_TypeInfo_var, L_0, L_1, L_2);
		Il2CppObject * L_3 = ___self0;
		NullCheck(L_3);
		InterfaceActionInvoker0::Invoke(1 /* System.Void IOrganism::WakeUp() */, IOrganism_t2580843579_il2cpp_TypeInfo_var, L_3);
		Il2CppObject * L_4 = ___self0;
		NullCheck(L_4);
		InterfaceActionInvoker0::Invoke(2 /* System.Void IOrganism::Spread() */, IOrganism_t2580843579_il2cpp_TypeInfo_var, L_4);
		return;
	}
}
// System.Void OrSubstance::.ctor()
extern const MethodInfo* SubstanceBase_2__ctor_m743856414_MethodInfo_var;
extern const uint32_t OrSubstance__ctor_m2956410718_MetadataUsageId;
extern "C"  void OrSubstance__ctor_m2956410718 (OrSubstance_t1381068461 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (OrSubstance__ctor_m2956410718_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		SubstanceBase_2__ctor_m743856414(__this, (bool)0, /*hidden argument*/SubstanceBase_2__ctor_m743856414_MethodInfo_var);
		return;
	}
}
// System.Boolean OrSubstance::Result()
extern Il2CppClass* OrSubstance_t1381068461_il2cpp_TypeInfo_var;
extern Il2CppClass* Func_2_t2346177373_il2cpp_TypeInfo_var;
extern const MethodInfo* OrSubstance_U3CResultU3Em__1DC_m2339587735_MethodInfo_var;
extern const MethodInfo* Func_2__ctor_m3041450970_MethodInfo_var;
extern const MethodInfo* Enumerable_Any_TisIntrusion_t1917188776_m2092008156_MethodInfo_var;
extern const uint32_t OrSubstance_Result_m2705036175_MetadataUsageId;
extern "C"  bool OrSubstance_Result_m2705036175 (OrSubstance_t1381068461 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (OrSubstance_Result_m2705036175_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	List_1_t2714147745 * G_B2_0 = NULL;
	List_1_t2714147745 * G_B1_0 = NULL;
	{
		List_1_t2714147745 * L_0 = ((SubstanceBase_2_t3707538316 *)__this)->get_intrusions_5();
		Func_2_t2346177373 * L_1 = ((OrSubstance_t1381068461_StaticFields*)OrSubstance_t1381068461_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__amU24cache0_6();
		G_B1_0 = L_0;
		if (L_1)
		{
			G_B2_0 = L_0;
			goto IL_001e;
		}
	}
	{
		IntPtr_t L_2;
		L_2.set_m_value_0((void*)(void*)OrSubstance_U3CResultU3Em__1DC_m2339587735_MethodInfo_var);
		Func_2_t2346177373 * L_3 = (Func_2_t2346177373 *)il2cpp_codegen_object_new(Func_2_t2346177373_il2cpp_TypeInfo_var);
		Func_2__ctor_m3041450970(L_3, NULL, L_2, /*hidden argument*/Func_2__ctor_m3041450970_MethodInfo_var);
		((OrSubstance_t1381068461_StaticFields*)OrSubstance_t1381068461_il2cpp_TypeInfo_var->static_fields)->set_U3CU3Ef__amU24cache0_6(L_3);
		G_B2_0 = G_B1_0;
	}

IL_001e:
	{
		Func_2_t2346177373 * L_4 = ((OrSubstance_t1381068461_StaticFields*)OrSubstance_t1381068461_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__amU24cache0_6();
		bool L_5 = Enumerable_Any_TisIntrusion_t1917188776_m2092008156(NULL /*static, unused*/, G_B2_0, L_4, /*hidden argument*/Enumerable_Any_TisIntrusion_t1917188776_m2092008156_MethodInfo_var);
		return L_5;
	}
}
// System.Boolean OrSubstance::<Result>m__1DC(SubstanceBase`2/Intrusion<System.Boolean,System.Boolean>)
extern "C"  bool OrSubstance_U3CResultU3Em__1DC_m2339587735 (Il2CppObject * __this /* static, unused */, Intrusion_t1917188776 * ___processor0, const MethodInfo* method)
{
	{
		Intrusion_t1917188776 * L_0 = ___processor0;
		NullCheck(L_0);
		bool L_1 = L_0->get_influence_1();
		return L_1;
	}
}
// System.Void Params::.cctor()
extern Il2CppClass* Dictionary_2_t2606186806_il2cpp_TypeInfo_var;
extern Il2CppClass* Params_t2383406502_il2cpp_TypeInfo_var;
extern const MethodInfo* Dictionary_2__ctor_m640701813_MethodInfo_var;
extern const uint32_t Params__cctor_m3345647768_MetadataUsageId;
extern "C"  void Params__cctor_m3345647768 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Params__cctor_m3345647768_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Dictionary_2_t2606186806 * L_0 = (Dictionary_2_t2606186806 *)il2cpp_codegen_object_new(Dictionary_2_t2606186806_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m640701813(L_0, /*hidden argument*/Dictionary_2__ctor_m640701813_MethodInfo_var);
		((Params_t2383406502_StaticFields*)Params_t2383406502_il2cpp_TypeInfo_var->static_fields)->set__parameters_1(L_0);
		return;
	}
}
// System.String Params::ResolveForPlatform(System.String)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Params_t2383406502_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2902830;
extern const uint32_t Params_ResolveForPlatform_m1465200480_MetadataUsageId;
extern "C"  String_t* Params_ResolveForPlatform_m1465200480 (Il2CppObject * __this /* static, unused */, String_t* ___key0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Params_ResolveForPlatform_m1465200480_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = ___key0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_1 = String_Concat_m138640077(NULL /*static, unused*/, L_0, _stringLiteral2902830, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Params_t2383406502_il2cpp_TypeInfo_var);
		String_t* L_2 = Params_Resolve_m1418989318(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.String Params::Resolve(System.String)
extern Il2CppClass* Params_t2383406502_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t1588791936_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1051907705;
extern const uint32_t Params_Resolve_m1418989318_MetadataUsageId;
extern "C"  String_t* Params_Resolve_m1418989318 (Il2CppObject * __this /* static, unused */, String_t* ___key0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Params_Resolve_m1418989318_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	String_t* V_0 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(Params_t2383406502_il2cpp_TypeInfo_var);
		bool L_0 = ((Params_t2383406502_StaticFields*)Params_t2383406502_il2cpp_TypeInfo_var->static_fields)->get_initialized_0();
		if (L_0)
		{
			goto IL_000f;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Params_t2383406502_il2cpp_TypeInfo_var);
		Params_Initialize_m4223968767(NULL /*static, unused*/, /*hidden argument*/NULL);
	}

IL_000f:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Params_t2383406502_il2cpp_TypeInfo_var);
		Dictionary_2_t2606186806 * L_1 = ((Params_t2383406502_StaticFields*)Params_t2383406502_il2cpp_TypeInfo_var->static_fields)->get__parameters_1();
		String_t* L_2 = ___key0;
		NullCheck(L_1);
		bool L_3 = VirtFuncInvoker2< bool, String_t*, String_t** >::Invoke(21 /* System.Boolean System.Collections.Generic.Dictionary`2<System.String,System.String>::TryGetValue(!0,!1&) */, L_1, L_2, (&V_0));
		if (!L_3)
		{
			goto IL_0023;
		}
	}
	{
		String_t* L_4 = V_0;
		return L_4;
	}

IL_0023:
	{
		String_t* L_5 = ___key0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_6 = String_Concat_m138640077(NULL /*static, unused*/, _stringLiteral1051907705, L_5, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1588791936_il2cpp_TypeInfo_var);
		Debug_LogError_m1836388681(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
		String_t* L_7 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		return L_7;
	}
}
// System.Void Params::Initialize()
extern Il2CppClass* Params_t2383406502_il2cpp_TypeInfo_var;
extern Il2CppClass* CharU5BU5D_t3416858730_il2cpp_TypeInfo_var;
extern Il2CppClass* Func_2_t917545008_il2cpp_TypeInfo_var;
extern Il2CppClass* Func_2_t160061447_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t1588791936_il2cpp_TypeInfo_var;
extern Il2CppClass* Enumerator_t4146198159_il2cpp_TypeInfo_var;
extern Il2CppClass* IDisposable_t1628921374_il2cpp_TypeInfo_var;
extern const MethodInfo* Resources_Load_TisTextAsset_t2461560304_m585700060_MethodInfo_var;
extern const MethodInfo* Params_U3CInitializeU3Em__11B_m1409712169_MethodInfo_var;
extern const MethodInfo* Func_2__ctor_m1547295832_MethodInfo_var;
extern const MethodInfo* Enumerable_Select_TisString_t_TisString_t_m4109456874_MethodInfo_var;
extern const MethodInfo* Params_U3CInitializeU3Em__11C_m1650049175_MethodInfo_var;
extern const MethodInfo* Func_2__ctor_m3001890441_MethodInfo_var;
extern const MethodInfo* Enumerable_Where_TisString_t_m178095414_MethodInfo_var;
extern const MethodInfo* Enumerable_ToList_TisString_t_m790467565_MethodInfo_var;
extern const MethodInfo* List_1_GetEnumerator_m2066100017_MethodInfo_var;
extern const MethodInfo* Enumerator_get_Current_m3788713170_MethodInfo_var;
extern const MethodInfo* Params_U3CInitializeU3Em__11D_m1139514998_MethodInfo_var;
extern const MethodInfo* Enumerable_ToArray_TisString_t_m2199204590_MethodInfo_var;
extern const MethodInfo* Enumerator_MoveNext_m2014284926_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral835260333;
extern Il2CppCodeGenString* _stringLiteral35;
extern Il2CppCodeGenString* _stringLiteral1606372396;
extern const uint32_t Params_Initialize_m4223968767_MetadataUsageId;
extern "C"  void Params_Initialize_m4223968767 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Params_Initialize_m4223968767_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	TextAsset_t2461560304 * V_0 = NULL;
	String_t* V_1 = NULL;
	List_1_t1765447871 * V_2 = NULL;
	String_t* V_3 = NULL;
	Enumerator_t4146198159  V_4;
	memset(&V_4, 0, sizeof(V_4));
	StringU5BU5D_t2956870243* V_5 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	StringU5BU5D_t2956870243* G_B2_0 = NULL;
	StringU5BU5D_t2956870243* G_B1_0 = NULL;
	Il2CppObject* G_B4_0 = NULL;
	Il2CppObject* G_B3_0 = NULL;
	StringU5BU5D_t2956870243* G_B10_0 = NULL;
	StringU5BU5D_t2956870243* G_B9_0 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(Params_t2383406502_il2cpp_TypeInfo_var);
		((Params_t2383406502_StaticFields*)Params_t2383406502_il2cpp_TypeInfo_var->static_fields)->set_initialized_0((bool)1);
		TextAsset_t2461560304 * L_0 = Resources_Load_TisTextAsset_t2461560304_m585700060(NULL /*static, unused*/, _stringLiteral835260333, /*hidden argument*/Resources_Load_TisTextAsset_t2461560304_m585700060_MethodInfo_var);
		V_0 = L_0;
		TextAsset_t2461560304 * L_1 = V_0;
		NullCheck(L_1);
		String_t* L_2 = TextAsset_get_text_m655578209(L_1, /*hidden argument*/NULL);
		V_1 = L_2;
		String_t* L_3 = V_1;
		CharU5BU5D_t3416858730* L_4 = ((CharU5BU5D_t3416858730*)SZArrayNew(CharU5BU5D_t3416858730_il2cpp_TypeInfo_var, (uint32_t)1));
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, 0);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(0), (uint16_t)((int32_t)10));
		NullCheck(L_3);
		StringU5BU5D_t2956870243* L_5 = String_Split_m290179486(L_3, L_4, /*hidden argument*/NULL);
		Func_2_t917545008 * L_6 = ((Params_t2383406502_StaticFields*)Params_t2383406502_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__amU24cache2_2();
		G_B1_0 = L_5;
		if (L_6)
		{
			G_B2_0 = L_5;
			goto IL_0041;
		}
	}
	{
		IntPtr_t L_7;
		L_7.set_m_value_0((void*)(void*)Params_U3CInitializeU3Em__11B_m1409712169_MethodInfo_var);
		Func_2_t917545008 * L_8 = (Func_2_t917545008 *)il2cpp_codegen_object_new(Func_2_t917545008_il2cpp_TypeInfo_var);
		Func_2__ctor_m1547295832(L_8, NULL, L_7, /*hidden argument*/Func_2__ctor_m1547295832_MethodInfo_var);
		IL2CPP_RUNTIME_CLASS_INIT(Params_t2383406502_il2cpp_TypeInfo_var);
		((Params_t2383406502_StaticFields*)Params_t2383406502_il2cpp_TypeInfo_var->static_fields)->set_U3CU3Ef__amU24cache2_2(L_8);
		G_B2_0 = G_B1_0;
	}

IL_0041:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Params_t2383406502_il2cpp_TypeInfo_var);
		Func_2_t917545008 * L_9 = ((Params_t2383406502_StaticFields*)Params_t2383406502_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__amU24cache2_2();
		Il2CppObject* L_10 = Enumerable_Select_TisString_t_TisString_t_m4109456874(NULL /*static, unused*/, (Il2CppObject*)(Il2CppObject*)G_B2_0, L_9, /*hidden argument*/Enumerable_Select_TisString_t_TisString_t_m4109456874_MethodInfo_var);
		Func_2_t160061447 * L_11 = ((Params_t2383406502_StaticFields*)Params_t2383406502_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__amU24cache3_3();
		G_B3_0 = L_10;
		if (L_11)
		{
			G_B4_0 = L_10;
			goto IL_0063;
		}
	}
	{
		IntPtr_t L_12;
		L_12.set_m_value_0((void*)(void*)Params_U3CInitializeU3Em__11C_m1650049175_MethodInfo_var);
		Func_2_t160061447 * L_13 = (Func_2_t160061447 *)il2cpp_codegen_object_new(Func_2_t160061447_il2cpp_TypeInfo_var);
		Func_2__ctor_m3001890441(L_13, NULL, L_12, /*hidden argument*/Func_2__ctor_m3001890441_MethodInfo_var);
		IL2CPP_RUNTIME_CLASS_INIT(Params_t2383406502_il2cpp_TypeInfo_var);
		((Params_t2383406502_StaticFields*)Params_t2383406502_il2cpp_TypeInfo_var->static_fields)->set_U3CU3Ef__amU24cache3_3(L_13);
		G_B4_0 = G_B3_0;
	}

IL_0063:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Params_t2383406502_il2cpp_TypeInfo_var);
		Func_2_t160061447 * L_14 = ((Params_t2383406502_StaticFields*)Params_t2383406502_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__amU24cache3_3();
		Il2CppObject* L_15 = Enumerable_Where_TisString_t_m178095414(NULL /*static, unused*/, G_B4_0, L_14, /*hidden argument*/Enumerable_Where_TisString_t_m178095414_MethodInfo_var);
		List_1_t1765447871 * L_16 = Enumerable_ToList_TisString_t_m790467565(NULL /*static, unused*/, L_15, /*hidden argument*/Enumerable_ToList_TisString_t_m790467565_MethodInfo_var);
		V_2 = L_16;
		List_1_t1765447871 * L_17 = V_2;
		NullCheck(L_17);
		Enumerator_t4146198159  L_18 = List_1_GetEnumerator_m2066100017(L_17, /*hidden argument*/List_1_GetEnumerator_m2066100017_MethodInfo_var);
		V_4 = L_18;
	}

IL_007b:
	try
	{ // begin try (depth: 1)
		{
			goto IL_010d;
		}

IL_0080:
		{
			String_t* L_19 = Enumerator_get_Current_m3788713170((&V_4), /*hidden argument*/Enumerator_get_Current_m3788713170_MethodInfo_var);
			V_3 = L_19;
			String_t* L_20 = V_3;
			NullCheck(L_20);
			bool L_21 = String_StartsWith_m1500793453(L_20, _stringLiteral35, /*hidden argument*/NULL);
			if (!L_21)
			{
				goto IL_009d;
			}
		}

IL_0098:
		{
			goto IL_010d;
		}

IL_009d:
		{
			String_t* L_22 = V_3;
			CharU5BU5D_t3416858730* L_23 = ((CharU5BU5D_t3416858730*)SZArrayNew(CharU5BU5D_t3416858730_il2cpp_TypeInfo_var, (uint32_t)2));
			NullCheck(L_23);
			IL2CPP_ARRAY_BOUNDS_CHECK(L_23, 0);
			(L_23)->SetAt(static_cast<il2cpp_array_size_t>(0), (uint16_t)((int32_t)32));
			CharU5BU5D_t3416858730* L_24 = L_23;
			NullCheck(L_24);
			IL2CPP_ARRAY_BOUNDS_CHECK(L_24, 1);
			(L_24)->SetAt(static_cast<il2cpp_array_size_t>(1), (uint16_t)((int32_t)9));
			NullCheck(L_22);
			StringU5BU5D_t2956870243* L_25 = String_Split_m290179486(L_22, L_24, /*hidden argument*/NULL);
			IL2CPP_RUNTIME_CLASS_INIT(Params_t2383406502_il2cpp_TypeInfo_var);
			Func_2_t160061447 * L_26 = ((Params_t2383406502_StaticFields*)Params_t2383406502_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__amU24cache4_4();
			G_B9_0 = L_25;
			if (L_26)
			{
				G_B10_0 = L_25;
				goto IL_00cb;
			}
		}

IL_00ba:
		{
			IntPtr_t L_27;
			L_27.set_m_value_0((void*)(void*)Params_U3CInitializeU3Em__11D_m1139514998_MethodInfo_var);
			Func_2_t160061447 * L_28 = (Func_2_t160061447 *)il2cpp_codegen_object_new(Func_2_t160061447_il2cpp_TypeInfo_var);
			Func_2__ctor_m3001890441(L_28, NULL, L_27, /*hidden argument*/Func_2__ctor_m3001890441_MethodInfo_var);
			IL2CPP_RUNTIME_CLASS_INIT(Params_t2383406502_il2cpp_TypeInfo_var);
			((Params_t2383406502_StaticFields*)Params_t2383406502_il2cpp_TypeInfo_var->static_fields)->set_U3CU3Ef__amU24cache4_4(L_28);
			G_B10_0 = G_B9_0;
		}

IL_00cb:
		{
			IL2CPP_RUNTIME_CLASS_INIT(Params_t2383406502_il2cpp_TypeInfo_var);
			Func_2_t160061447 * L_29 = ((Params_t2383406502_StaticFields*)Params_t2383406502_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__amU24cache4_4();
			Il2CppObject* L_30 = Enumerable_Where_TisString_t_m178095414(NULL /*static, unused*/, (Il2CppObject*)(Il2CppObject*)G_B10_0, L_29, /*hidden argument*/Enumerable_Where_TisString_t_m178095414_MethodInfo_var);
			StringU5BU5D_t2956870243* L_31 = Enumerable_ToArray_TisString_t_m2199204590(NULL /*static, unused*/, L_30, /*hidden argument*/Enumerable_ToArray_TisString_t_m2199204590_MethodInfo_var);
			V_5 = L_31;
			StringU5BU5D_t2956870243* L_32 = V_5;
			NullCheck(L_32);
			if ((!(((uint32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_32)->max_length))))) == ((uint32_t)2))))
			{
				goto IL_00fd;
			}
		}

IL_00e6:
		{
			IL2CPP_RUNTIME_CLASS_INIT(Params_t2383406502_il2cpp_TypeInfo_var);
			Dictionary_2_t2606186806 * L_33 = ((Params_t2383406502_StaticFields*)Params_t2383406502_il2cpp_TypeInfo_var->static_fields)->get__parameters_1();
			StringU5BU5D_t2956870243* L_34 = V_5;
			NullCheck(L_34);
			IL2CPP_ARRAY_BOUNDS_CHECK(L_34, 0);
			int32_t L_35 = 0;
			StringU5BU5D_t2956870243* L_36 = V_5;
			NullCheck(L_36);
			IL2CPP_ARRAY_BOUNDS_CHECK(L_36, 1);
			int32_t L_37 = 1;
			NullCheck(L_33);
			VirtActionInvoker2< String_t*, String_t* >::Invoke(23 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.String>::set_Item(!0,!1) */, L_33, ((L_34)->GetAt(static_cast<il2cpp_array_size_t>(L_35))), ((L_36)->GetAt(static_cast<il2cpp_array_size_t>(L_37))));
			goto IL_010d;
		}

IL_00fd:
		{
			String_t* L_38 = V_3;
			IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
			String_t* L_39 = String_Format_m2471250780(NULL /*static, unused*/, _stringLiteral1606372396, L_38, /*hidden argument*/NULL);
			IL2CPP_RUNTIME_CLASS_INIT(Debug_t1588791936_il2cpp_TypeInfo_var);
			Debug_LogWarning_m539478453(NULL /*static, unused*/, L_39, /*hidden argument*/NULL);
		}

IL_010d:
		{
			bool L_40 = Enumerator_MoveNext_m2014284926((&V_4), /*hidden argument*/Enumerator_MoveNext_m2014284926_MethodInfo_var);
			if (L_40)
			{
				goto IL_0080;
			}
		}

IL_0119:
		{
			IL2CPP_LEAVE(0x12B, FINALLY_011e);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_011e;
	}

FINALLY_011e:
	{ // begin finally (depth: 1)
		Enumerator_t4146198159  L_41 = V_4;
		Enumerator_t4146198159  L_42 = L_41;
		Il2CppObject * L_43 = Box(Enumerator_t4146198159_il2cpp_TypeInfo_var, &L_42);
		NullCheck((Il2CppObject *)L_43);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, (Il2CppObject *)L_43);
		IL2CPP_END_FINALLY(286)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(286)
	{
		IL2CPP_JUMP_TBL(0x12B, IL_012b)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_012b:
	{
		return;
	}
}
// System.String Params::<Initialize>m__11B(System.String)
extern "C"  String_t* Params_U3CInitializeU3Em__11B_m1409712169 (Il2CppObject * __this /* static, unused */, String_t* ___s0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___s0;
		NullCheck(L_0);
		String_t* L_1 = String_Trim_m1030489823(L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Boolean Params::<Initialize>m__11C(System.String)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const uint32_t Params_U3CInitializeU3Em__11C_m1650049175_MetadataUsageId;
extern "C"  bool Params_U3CInitializeU3Em__11C_m1650049175 (Il2CppObject * __this /* static, unused */, String_t* ___s0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Params_U3CInitializeU3Em__11C_m1650049175_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = ___s0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_1 = String_IsNullOrEmpty_m1256468773(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return (bool)((((int32_t)L_1) == ((int32_t)0))? 1 : 0);
	}
}
// System.Boolean Params::<Initialize>m__11D(System.String)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const uint32_t Params_U3CInitializeU3Em__11D_m1139514998_MetadataUsageId;
extern "C"  bool Params_U3CInitializeU3Em__11D_m1139514998 (Il2CppObject * __this /* static, unused */, String_t* ___s0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Params_U3CInitializeU3Em__11D_m1139514998_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = ___s0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_1 = String_IsNullOrEmpty_m1256468773(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return (bool)((((int32_t)L_1) == ((int32_t)0))? 1 : 0);
	}
}
// System.Void PersistentData::.ctor()
extern "C"  void PersistentData__ctor_m3346137786 (PersistentData_t2127272705 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.String PersistentData::GetFilePath(System.String,System.Int32)
extern Il2CppClass* ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var;
extern Il2CppClass* Int32_t2847414787_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral47;
extern Il2CppCodeGenString* _stringLiteral95;
extern Il2CppCodeGenString* _stringLiteral1469609;
extern const uint32_t PersistentData_GetFilePath_m3905711329_MetadataUsageId;
extern "C"  String_t* PersistentData_GetFilePath_m3905711329 (Il2CppObject * __this /* static, unused */, String_t* ___prefix0, int32_t ___saveId1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (PersistentData_GetFilePath_m3905711329_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		ObjectU5BU5D_t11523773* L_0 = ((ObjectU5BU5D_t11523773*)SZArrayNew(ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var, (uint32_t)6));
		String_t* L_1 = Application_get_persistentDataPath_m2554537447(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, L_1);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)L_1);
		ObjectU5BU5D_t11523773* L_2 = L_0;
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, 1);
		ArrayElementTypeCheck (L_2, _stringLiteral47);
		(L_2)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)_stringLiteral47);
		ObjectU5BU5D_t11523773* L_3 = L_2;
		String_t* L_4 = ___prefix0;
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, 2);
		ArrayElementTypeCheck (L_3, L_4);
		(L_3)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)L_4);
		ObjectU5BU5D_t11523773* L_5 = L_3;
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, 3);
		ArrayElementTypeCheck (L_5, _stringLiteral95);
		(L_5)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)_stringLiteral95);
		ObjectU5BU5D_t11523773* L_6 = L_5;
		int32_t L_7 = ___saveId1;
		int32_t L_8 = L_7;
		Il2CppObject * L_9 = Box(Int32_t2847414787_il2cpp_TypeInfo_var, &L_8);
		NullCheck(L_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_6, 4);
		ArrayElementTypeCheck (L_6, L_9);
		(L_6)->SetAt(static_cast<il2cpp_array_size_t>(4), (Il2CppObject *)L_9);
		ObjectU5BU5D_t11523773* L_10 = L_6;
		NullCheck(L_10);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_10, 5);
		ArrayElementTypeCheck (L_10, _stringLiteral1469609);
		(L_10)->SetAt(static_cast<il2cpp_array_size_t>(5), (Il2CppObject *)_stringLiteral1469609);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_11 = String_Concat_m3016520001(NULL /*static, unused*/, L_10, /*hidden argument*/NULL);
		return L_11;
	}
}
// System.Void PlaneInfo::.ctor()
extern "C"  void PlaneInfo__ctor_m3021069217 (PlaneInfo_t4110763274 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean Platform::Is(UnityEngine.RuntimePlatform)
extern "C"  bool Platform_Is_m3845187556 (Il2CppObject * __this /* static, unused */, int32_t ___platform0, const MethodInfo* method)
{
	{
		int32_t L_0 = Application_get_platform_m2818272885(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_1 = ___platform0;
		return (bool)((((int32_t)L_0) == ((int32_t)L_1))? 1 : 0);
	}
}
// System.Void PlatformSpriteSetter::.ctor()
extern "C"  void PlatformSpriteSetter__ctor_m3934439588 (PlatformSpriteSetter_t3879007191 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m350695606(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void PlatformSpriteSetter::Awake()
extern const MethodInfo* Component_GetComponent_TisSpriteRenderer_t2223784725_m4090179846_MethodInfo_var;
extern const uint32_t PlatformSpriteSetter_Awake_m4172044807_MetadataUsageId;
extern "C"  void PlatformSpriteSetter_Awake_m4172044807 (PlatformSpriteSetter_t3879007191 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (PlatformSpriteSetter_Awake_m4172044807_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	SpriteRenderer_t2223784725 * V_0 = NULL;
	{
		SpriteRenderer_t2223784725 * L_0 = Component_GetComponent_TisSpriteRenderer_t2223784725_m4090179846(__this, /*hidden argument*/Component_GetComponent_TisSpriteRenderer_t2223784725_m4090179846_MethodInfo_var);
		V_0 = L_0;
		SpriteRenderer_t2223784725 * L_1 = V_0;
		bool L_2 = Object_op_Implicit_m3627549526(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_001e;
		}
	}
	{
		SpriteRenderer_t2223784725 * L_3 = V_0;
		Sprite_t4006040370 * L_4 = __this->get_iosSprite_3();
		NullCheck(L_3);
		SpriteRenderer_set_sprite_m1519408453(L_3, L_4, /*hidden argument*/NULL);
	}

IL_001e:
	{
		return;
	}
}
// System.Void PlayFab.ClientModels.AcceptTradeRequest::.ctor()
extern "C"  void AcceptTradeRequest__ctor_m3501037370 (AcceptTradeRequest_t1814136291 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.String PlayFab.ClientModels.AcceptTradeRequest::get_OfferingPlayerId()
extern "C"  String_t* AcceptTradeRequest_get_OfferingPlayerId_m2443613010 (AcceptTradeRequest_t1814136291 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_U3COfferingPlayerIdU3Ek__BackingField_0();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.AcceptTradeRequest::set_OfferingPlayerId(System.String)
extern "C"  void AcceptTradeRequest_set_OfferingPlayerId_m301693017 (AcceptTradeRequest_t1814136291 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3COfferingPlayerIdU3Ek__BackingField_0(L_0);
		return;
	}
}
// System.String PlayFab.ClientModels.AcceptTradeRequest::get_TradeId()
extern "C"  String_t* AcceptTradeRequest_get_TradeId_m3226848561 (AcceptTradeRequest_t1814136291 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_U3CTradeIdU3Ek__BackingField_1();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.AcceptTradeRequest::set_TradeId(System.String)
extern "C"  void AcceptTradeRequest_set_TradeId_m2110973928 (AcceptTradeRequest_t1814136291 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CTradeIdU3Ek__BackingField_1(L_0);
		return;
	}
}
// System.Collections.Generic.List`1<System.String> PlayFab.ClientModels.AcceptTradeRequest::get_AcceptedInventoryInstanceIds()
extern "C"  List_1_t1765447871 * AcceptTradeRequest_get_AcceptedInventoryInstanceIds_m1525545072 (AcceptTradeRequest_t1814136291 * __this, const MethodInfo* method)
{
	{
		List_1_t1765447871 * L_0 = __this->get_U3CAcceptedInventoryInstanceIdsU3Ek__BackingField_2();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.AcceptTradeRequest::set_AcceptedInventoryInstanceIds(System.Collections.Generic.List`1<System.String>)
extern "C"  void AcceptTradeRequest_set_AcceptedInventoryInstanceIds_m2225062569 (AcceptTradeRequest_t1814136291 * __this, List_1_t1765447871 * ___value0, const MethodInfo* method)
{
	{
		List_1_t1765447871 * L_0 = ___value0;
		__this->set_U3CAcceptedInventoryInstanceIdsU3Ek__BackingField_2(L_0);
		return;
	}
}
// System.Void PlayFab.ClientModels.AcceptTradeResponse::.ctor()
extern "C"  void AcceptTradeResponse__ctor_m4221970876 (AcceptTradeResponse_t3663823245 * __this, const MethodInfo* method)
{
	{
		PlayFabResultCommon__ctor_m4291338422(__this, /*hidden argument*/NULL);
		return;
	}
}
// PlayFab.ClientModels.TradeInfo PlayFab.ClientModels.AcceptTradeResponse::get_Trade()
extern "C"  TradeInfo_t1933812770 * AcceptTradeResponse_get_Trade_m2983538124 (AcceptTradeResponse_t3663823245 * __this, const MethodInfo* method)
{
	{
		TradeInfo_t1933812770 * L_0 = __this->get_U3CTradeU3Ek__BackingField_2();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.AcceptTradeResponse::set_Trade(PlayFab.ClientModels.TradeInfo)
extern "C"  void AcceptTradeResponse_set_Trade_m2763527053 (AcceptTradeResponse_t3663823245 * __this, TradeInfo_t1933812770 * ___value0, const MethodInfo* method)
{
	{
		TradeInfo_t1933812770 * L_0 = ___value0;
		__this->set_U3CTradeU3Ek__BackingField_2(L_0);
		return;
	}
}
// System.Void PlayFab.ClientModels.AddFriendRequest::.ctor()
extern "C"  void AddFriendRequest__ctor_m2100964637 (AddFriendRequest_t1861404448 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.String PlayFab.ClientModels.AddFriendRequest::get_FriendPlayFabId()
extern "C"  String_t* AddFriendRequest_get_FriendPlayFabId_m2085920357 (AddFriendRequest_t1861404448 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_U3CFriendPlayFabIdU3Ek__BackingField_0();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.AddFriendRequest::set_FriendPlayFabId(System.String)
extern "C"  void AddFriendRequest_set_FriendPlayFabId_m4139641076 (AddFriendRequest_t1861404448 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CFriendPlayFabIdU3Ek__BackingField_0(L_0);
		return;
	}
}
// System.String PlayFab.ClientModels.AddFriendRequest::get_FriendUsername()
extern "C"  String_t* AddFriendRequest_get_FriendUsername_m1083127905 (AddFriendRequest_t1861404448 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_U3CFriendUsernameU3Ek__BackingField_1();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.AddFriendRequest::set_FriendUsername(System.String)
extern "C"  void AddFriendRequest_set_FriendUsername_m4096216362 (AddFriendRequest_t1861404448 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CFriendUsernameU3Ek__BackingField_1(L_0);
		return;
	}
}
// System.String PlayFab.ClientModels.AddFriendRequest::get_FriendEmail()
extern "C"  String_t* AddFriendRequest_get_FriendEmail_m3920846291 (AddFriendRequest_t1861404448 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_U3CFriendEmailU3Ek__BackingField_2();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.AddFriendRequest::set_FriendEmail(System.String)
extern "C"  void AddFriendRequest_set_FriendEmail_m2877743046 (AddFriendRequest_t1861404448 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CFriendEmailU3Ek__BackingField_2(L_0);
		return;
	}
}
// System.String PlayFab.ClientModels.AddFriendRequest::get_FriendTitleDisplayName()
extern "C"  String_t* AddFriendRequest_get_FriendTitleDisplayName_m325611200 (AddFriendRequest_t1861404448 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_U3CFriendTitleDisplayNameU3Ek__BackingField_3();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.AddFriendRequest::set_FriendTitleDisplayName(System.String)
extern "C"  void AddFriendRequest_set_FriendTitleDisplayName_m1032630315 (AddFriendRequest_t1861404448 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CFriendTitleDisplayNameU3Ek__BackingField_3(L_0);
		return;
	}
}
// System.Void PlayFab.ClientModels.AddFriendResult::.ctor()
extern "C"  void AddFriendResult__ctor_m1370815901 (AddFriendResult_t4251613068 * __this, const MethodInfo* method)
{
	{
		PlayFabResultCommon__ctor_m4291338422(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean PlayFab.ClientModels.AddFriendResult::get_Created()
extern "C"  bool AddFriendResult_get_Created_m3884103494 (AddFriendResult_t4251613068 * __this, const MethodInfo* method)
{
	{
		bool L_0 = __this->get_U3CCreatedU3Ek__BackingField_2();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.AddFriendResult::set_Created(System.Boolean)
extern "C"  void AddFriendResult_set_Created_m3972103037 (AddFriendResult_t4251613068 * __this, bool ___value0, const MethodInfo* method)
{
	{
		bool L_0 = ___value0;
		__this->set_U3CCreatedU3Ek__BackingField_2(L_0);
		return;
	}
}
// System.Void PlayFab.ClientModels.AddSharedGroupMembersRequest::.ctor()
extern "C"  void AddSharedGroupMembersRequest__ctor_m372278942 (AddSharedGroupMembersRequest_t3205265791 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.String PlayFab.ClientModels.AddSharedGroupMembersRequest::get_SharedGroupId()
extern "C"  String_t* AddSharedGroupMembersRequest_get_SharedGroupId_m844081867 (AddSharedGroupMembersRequest_t3205265791 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_U3CSharedGroupIdU3Ek__BackingField_0();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.AddSharedGroupMembersRequest::set_SharedGroupId(System.String)
extern "C"  void AddSharedGroupMembersRequest_set_SharedGroupId_m3404867470 (AddSharedGroupMembersRequest_t3205265791 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CSharedGroupIdU3Ek__BackingField_0(L_0);
		return;
	}
}
// System.Collections.Generic.List`1<System.String> PlayFab.ClientModels.AddSharedGroupMembersRequest::get_PlayFabIds()
extern "C"  List_1_t1765447871 * AddSharedGroupMembersRequest_get_PlayFabIds_m2319733923 (AddSharedGroupMembersRequest_t3205265791 * __this, const MethodInfo* method)
{
	{
		List_1_t1765447871 * L_0 = __this->get_U3CPlayFabIdsU3Ek__BackingField_1();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.AddSharedGroupMembersRequest::set_PlayFabIds(System.Collections.Generic.List`1<System.String>)
extern "C"  void AddSharedGroupMembersRequest_set_PlayFabIds_m62989020 (AddSharedGroupMembersRequest_t3205265791 * __this, List_1_t1765447871 * ___value0, const MethodInfo* method)
{
	{
		List_1_t1765447871 * L_0 = ___value0;
		__this->set_U3CPlayFabIdsU3Ek__BackingField_1(L_0);
		return;
	}
}
// System.Void PlayFab.ClientModels.AddSharedGroupMembersResult::.ctor()
extern "C"  void AddSharedGroupMembersResult__ctor_m3116167164 (AddSharedGroupMembersResult_t3325132109 * __this, const MethodInfo* method)
{
	{
		PlayFabResultCommon__ctor_m4291338422(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void PlayFab.ClientModels.AddUsernamePasswordRequest::.ctor()
extern "C"  void AddUsernamePasswordRequest__ctor_m253860944 (AddUsernamePasswordRequest_t1599161869 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.String PlayFab.ClientModels.AddUsernamePasswordRequest::get_Username()
extern "C"  String_t* AddUsernamePasswordRequest_get_Username_m2021056848 (AddUsernamePasswordRequest_t1599161869 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_U3CUsernameU3Ek__BackingField_0();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.AddUsernamePasswordRequest::set_Username(System.String)
extern "C"  void AddUsernamePasswordRequest_set_Username_m1786913563 (AddUsernamePasswordRequest_t1599161869 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CUsernameU3Ek__BackingField_0(L_0);
		return;
	}
}
// System.String PlayFab.ClientModels.AddUsernamePasswordRequest::get_Email()
extern "C"  String_t* AddUsernamePasswordRequest_get_Email_m542831428 (AddUsernamePasswordRequest_t1599161869 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_U3CEmailU3Ek__BackingField_1();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.AddUsernamePasswordRequest::set_Email(System.String)
extern "C"  void AddUsernamePasswordRequest_set_Email_m90716021 (AddUsernamePasswordRequest_t1599161869 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CEmailU3Ek__BackingField_1(L_0);
		return;
	}
}
// System.String PlayFab.ClientModels.AddUsernamePasswordRequest::get_Password()
extern "C"  String_t* AddUsernamePasswordRequest_get_Password_m965850581 (AddUsernamePasswordRequest_t1599161869 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_U3CPasswordU3Ek__BackingField_2();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.AddUsernamePasswordRequest::set_Password(System.String)
extern "C"  void AddUsernamePasswordRequest_set_Password_m3336146614 (AddUsernamePasswordRequest_t1599161869 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CPasswordU3Ek__BackingField_2(L_0);
		return;
	}
}
// System.Void PlayFab.ClientModels.AddUsernamePasswordResult::.ctor()
extern "C"  void AddUsernamePasswordResult__ctor_m341400586 (AddUsernamePasswordResult_t1056564991 * __this, const MethodInfo* method)
{
	{
		PlayFabResultCommon__ctor_m4291338422(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.String PlayFab.ClientModels.AddUsernamePasswordResult::get_Username()
extern "C"  String_t* AddUsernamePasswordResult_get_Username_m1403100604 (AddUsernamePasswordResult_t1056564991 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_U3CUsernameU3Ek__BackingField_2();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.AddUsernamePasswordResult::set_Username(System.String)
extern "C"  void AddUsernamePasswordResult_set_Username_m4102472149 (AddUsernamePasswordResult_t1056564991 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CUsernameU3Ek__BackingField_2(L_0);
		return;
	}
}
// System.Void PlayFab.ClientModels.AddUserVirtualCurrencyRequest::.ctor()
extern "C"  void AddUserVirtualCurrencyRequest__ctor_m1700619322 (AddUserVirtualCurrencyRequest_t2803224015 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.String PlayFab.ClientModels.AddUserVirtualCurrencyRequest::get_VirtualCurrency()
extern "C"  String_t* AddUserVirtualCurrencyRequest_get_VirtualCurrency_m3931837160 (AddUserVirtualCurrencyRequest_t2803224015 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_U3CVirtualCurrencyU3Ek__BackingField_0();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.AddUserVirtualCurrencyRequest::set_VirtualCurrency(System.String)
extern "C"  void AddUserVirtualCurrencyRequest_set_VirtualCurrency_m4084867243 (AddUserVirtualCurrencyRequest_t2803224015 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CVirtualCurrencyU3Ek__BackingField_0(L_0);
		return;
	}
}
// System.Int32 PlayFab.ClientModels.AddUserVirtualCurrencyRequest::get_Amount()
extern "C"  int32_t AddUserVirtualCurrencyRequest_get_Amount_m1692548665 (AddUserVirtualCurrencyRequest_t2803224015 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get_U3CAmountU3Ek__BackingField_1();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.AddUserVirtualCurrencyRequest::set_Amount(System.Int32)
extern "C"  void AddUserVirtualCurrencyRequest_set_Amount_m2342980464 (AddUserVirtualCurrencyRequest_t2803224015 * __this, int32_t ___value0, const MethodInfo* method)
{
	{
		int32_t L_0 = ___value0;
		__this->set_U3CAmountU3Ek__BackingField_1(L_0);
		return;
	}
}
// System.Void PlayFab.ClientModels.AndroidDevicePushNotificationRegistrationRequest::.ctor()
extern "C"  void AndroidDevicePushNotificationRegistrationRequest__ctor_m3814111553 (AndroidDevicePushNotificationRegistrationRequest_t28777084 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.String PlayFab.ClientModels.AndroidDevicePushNotificationRegistrationRequest::get_DeviceToken()
extern "C"  String_t* AndroidDevicePushNotificationRegistrationRequest_get_DeviceToken_m3762215260 (AndroidDevicePushNotificationRegistrationRequest_t28777084 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_U3CDeviceTokenU3Ek__BackingField_0();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.AndroidDevicePushNotificationRegistrationRequest::set_DeviceToken(System.String)
extern "C"  void AndroidDevicePushNotificationRegistrationRequest_set_DeviceToken_m1272078045 (AndroidDevicePushNotificationRegistrationRequest_t28777084 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CDeviceTokenU3Ek__BackingField_0(L_0);
		return;
	}
}
// System.Nullable`1<System.Boolean> PlayFab.ClientModels.AndroidDevicePushNotificationRegistrationRequest::get_SendPushNotificationConfirmation()
extern "C"  Nullable_1_t3097043249  AndroidDevicePushNotificationRegistrationRequest_get_SendPushNotificationConfirmation_m3553732325 (AndroidDevicePushNotificationRegistrationRequest_t28777084 * __this, const MethodInfo* method)
{
	{
		Nullable_1_t3097043249  L_0 = __this->get_U3CSendPushNotificationConfirmationU3Ek__BackingField_1();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.AndroidDevicePushNotificationRegistrationRequest::set_SendPushNotificationConfirmation(System.Nullable`1<System.Boolean>)
extern "C"  void AndroidDevicePushNotificationRegistrationRequest_set_SendPushNotificationConfirmation_m1705727590 (AndroidDevicePushNotificationRegistrationRequest_t28777084 * __this, Nullable_1_t3097043249  ___value0, const MethodInfo* method)
{
	{
		Nullable_1_t3097043249  L_0 = ___value0;
		__this->set_U3CSendPushNotificationConfirmationU3Ek__BackingField_1(L_0);
		return;
	}
}
// System.String PlayFab.ClientModels.AndroidDevicePushNotificationRegistrationRequest::get_ConfirmationMessege()
extern "C"  String_t* AndroidDevicePushNotificationRegistrationRequest_get_ConfirmationMessege_m1535741039 (AndroidDevicePushNotificationRegistrationRequest_t28777084 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_U3CConfirmationMessegeU3Ek__BackingField_2();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.AndroidDevicePushNotificationRegistrationRequest::set_ConfirmationMessege(System.String)
extern "C"  void AndroidDevicePushNotificationRegistrationRequest_set_ConfirmationMessege_m1609620394 (AndroidDevicePushNotificationRegistrationRequest_t28777084 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CConfirmationMessegeU3Ek__BackingField_2(L_0);
		return;
	}
}
// System.Void PlayFab.ClientModels.AndroidDevicePushNotificationRegistrationResult::.ctor()
extern "C"  void AndroidDevicePushNotificationRegistrationResult__ctor_m1703173369 (AndroidDevicePushNotificationRegistrationResult_t3915401392 * __this, const MethodInfo* method)
{
	{
		PlayFabResultCommon__ctor_m4291338422(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void PlayFab.ClientModels.AttributeInstallRequest::.ctor()
extern "C"  void AttributeInstallRequest__ctor_m2766478217 (AttributeInstallRequest_t4240476704 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.String PlayFab.ClientModels.AttributeInstallRequest::get_Idfa()
extern "C"  String_t* AttributeInstallRequest_get_Idfa_m1578080093 (AttributeInstallRequest_t4240476704 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_U3CIdfaU3Ek__BackingField_0();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.AttributeInstallRequest::set_Idfa(System.String)
extern "C"  void AttributeInstallRequest_set_Idfa_m3260619284 (AttributeInstallRequest_t4240476704 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CIdfaU3Ek__BackingField_0(L_0);
		return;
	}
}
// System.String PlayFab.ClientModels.AttributeInstallRequest::get_Android_Id()
extern "C"  String_t* AttributeInstallRequest_get_Android_Id_m2546233234 (AttributeInstallRequest_t4240476704 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_U3CAndroid_IdU3Ek__BackingField_1();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.AttributeInstallRequest::set_Android_Id(System.String)
extern "C"  void AttributeInstallRequest_set_Android_Id_m412616895 (AttributeInstallRequest_t4240476704 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CAndroid_IdU3Ek__BackingField_1(L_0);
		return;
	}
}
// System.Void PlayFab.ClientModels.AttributeInstallResult::.ctor()
extern "C"  void AttributeInstallResult__ctor_m1392284081 (AttributeInstallResult_t1834505356 * __this, const MethodInfo* method)
{
	{
		PlayFabResultCommon__ctor_m4291338422(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void PlayFab.ClientModels.CancelTradeRequest::.ctor()
extern "C"  void CancelTradeRequest__ctor_m2438846728 (CancelTradeRequest_t685728853 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.String PlayFab.ClientModels.CancelTradeRequest::get_TradeId()
extern "C"  String_t* CancelTradeRequest_get_TradeId_m1129643135 (CancelTradeRequest_t685728853 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_U3CTradeIdU3Ek__BackingField_0();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.CancelTradeRequest::set_TradeId(System.String)
extern "C"  void CancelTradeRequest_set_TradeId_m2597029722 (CancelTradeRequest_t685728853 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CTradeIdU3Ek__BackingField_0(L_0);
		return;
	}
}
// System.Void PlayFab.ClientModels.CancelTradeResponse::.ctor()
extern "C"  void CancelTradeResponse__ctor_m1358832046 (CancelTradeResponse_t3042931035 * __this, const MethodInfo* method)
{
	{
		PlayFabResultCommon__ctor_m4291338422(__this, /*hidden argument*/NULL);
		return;
	}
}
// PlayFab.ClientModels.TradeInfo PlayFab.ClientModels.CancelTradeResponse::get_Trade()
extern "C"  TradeInfo_t1933812770 * CancelTradeResponse_get_Trade_m699129022 (CancelTradeResponse_t3042931035 * __this, const MethodInfo* method)
{
	{
		TradeInfo_t1933812770 * L_0 = __this->get_U3CTradeU3Ek__BackingField_2();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.CancelTradeResponse::set_Trade(PlayFab.ClientModels.TradeInfo)
extern "C"  void CancelTradeResponse_set_Trade_m1043828479 (CancelTradeResponse_t3042931035 * __this, TradeInfo_t1933812770 * ___value0, const MethodInfo* method)
{
	{
		TradeInfo_t1933812770 * L_0 = ___value0;
		__this->set_U3CTradeU3Ek__BackingField_2(L_0);
		return;
	}
}
// System.Void PlayFab.ClientModels.CartItem::.ctor()
extern "C"  void CartItem__ctor_m1216156442 (CartItem_t3543091843 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.String PlayFab.ClientModels.CartItem::get_ItemId()
extern "C"  String_t* CartItem_get_ItemId_m1977638142 (CartItem_t3543091843 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_U3CItemIdU3Ek__BackingField_0();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.CartItem::set_ItemId(System.String)
extern "C"  void CartItem_set_ItemId_m2360047021 (CartItem_t3543091843 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CItemIdU3Ek__BackingField_0(L_0);
		return;
	}
}
// System.String PlayFab.ClientModels.CartItem::get_ItemClass()
extern "C"  String_t* CartItem_get_ItemClass_m1005424535 (CartItem_t3543091843 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_U3CItemClassU3Ek__BackingField_1();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.CartItem::set_ItemClass(System.String)
extern "C"  void CartItem_set_ItemClass_m2441077058 (CartItem_t3543091843 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CItemClassU3Ek__BackingField_1(L_0);
		return;
	}
}
// System.String PlayFab.ClientModels.CartItem::get_ItemInstanceId()
extern "C"  String_t* CartItem_get_ItemInstanceId_m3792392499 (CartItem_t3543091843 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_U3CItemInstanceIdU3Ek__BackingField_2();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.CartItem::set_ItemInstanceId(System.String)
extern "C"  void CartItem_set_ItemInstanceId_m672843416 (CartItem_t3543091843 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CItemInstanceIdU3Ek__BackingField_2(L_0);
		return;
	}
}
// System.String PlayFab.ClientModels.CartItem::get_DisplayName()
extern "C"  String_t* CartItem_get_DisplayName_m4174551455 (CartItem_t3543091843 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_U3CDisplayNameU3Ek__BackingField_3();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.CartItem::set_DisplayName(System.String)
extern "C"  void CartItem_set_DisplayName_m825893498 (CartItem_t3543091843 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CDisplayNameU3Ek__BackingField_3(L_0);
		return;
	}
}
// System.String PlayFab.ClientModels.CartItem::get_Description()
extern "C"  String_t* CartItem_get_Description_m2418483054 (CartItem_t3543091843 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_U3CDescriptionU3Ek__BackingField_4();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.CartItem::set_Description(System.String)
extern "C"  void CartItem_set_Description_m4269524235 (CartItem_t3543091843 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CDescriptionU3Ek__BackingField_4(L_0);
		return;
	}
}
// System.Collections.Generic.Dictionary`2<System.String,System.UInt32> PlayFab.ClientModels.CartItem::get_VirtualCurrencyPrices()
extern "C"  Dictionary_2_t2623623230 * CartItem_get_VirtualCurrencyPrices_m2091401835 (CartItem_t3543091843 * __this, const MethodInfo* method)
{
	{
		Dictionary_2_t2623623230 * L_0 = __this->get_U3CVirtualCurrencyPricesU3Ek__BackingField_5();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.CartItem::set_VirtualCurrencyPrices(System.Collections.Generic.Dictionary`2<System.String,System.UInt32>)
extern "C"  void CartItem_set_VirtualCurrencyPrices_m2407124922 (CartItem_t3543091843 * __this, Dictionary_2_t2623623230 * ___value0, const MethodInfo* method)
{
	{
		Dictionary_2_t2623623230 * L_0 = ___value0;
		__this->set_U3CVirtualCurrencyPricesU3Ek__BackingField_5(L_0);
		return;
	}
}
// System.Collections.Generic.Dictionary`2<System.String,System.UInt32> PlayFab.ClientModels.CartItem::get_RealCurrencyPrices()
extern "C"  Dictionary_2_t2623623230 * CartItem_get_RealCurrencyPrices_m1851834102 (CartItem_t3543091843 * __this, const MethodInfo* method)
{
	{
		Dictionary_2_t2623623230 * L_0 = __this->get_U3CRealCurrencyPricesU3Ek__BackingField_6();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.CartItem::set_RealCurrencyPrices(System.Collections.Generic.Dictionary`2<System.String,System.UInt32>)
extern "C"  void CartItem_set_RealCurrencyPrices_m788490585 (CartItem_t3543091843 * __this, Dictionary_2_t2623623230 * ___value0, const MethodInfo* method)
{
	{
		Dictionary_2_t2623623230 * L_0 = ___value0;
		__this->set_U3CRealCurrencyPricesU3Ek__BackingField_6(L_0);
		return;
	}
}
// System.Collections.Generic.Dictionary`2<System.String,System.UInt32> PlayFab.ClientModels.CartItem::get_VCAmount()
extern "C"  Dictionary_2_t2623623230 * CartItem_get_VCAmount_m3461916482 (CartItem_t3543091843 * __this, const MethodInfo* method)
{
	{
		Dictionary_2_t2623623230 * L_0 = __this->get_U3CVCAmountU3Ek__BackingField_7();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.CartItem::set_VCAmount(System.Collections.Generic.Dictionary`2<System.String,System.UInt32>)
extern "C"  void CartItem_set_VCAmount_m2576381989 (CartItem_t3543091843 * __this, Dictionary_2_t2623623230 * ___value0, const MethodInfo* method)
{
	{
		Dictionary_2_t2623623230 * L_0 = ___value0;
		__this->set_U3CVCAmountU3Ek__BackingField_7(L_0);
		return;
	}
}
// System.Void PlayFab.ClientModels.CatalogItem::.ctor()
extern "C"  void CatalogItem__ctor_m2174105453 (CatalogItem_t4132589244 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.String PlayFab.ClientModels.CatalogItem::get_ItemId()
extern "C"  String_t* CatalogItem_get_ItemId_m3475438449 (CatalogItem_t4132589244 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_U3CItemIdU3Ek__BackingField_0();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.CatalogItem::set_ItemId(System.String)
extern "C"  void CatalogItem_set_ItemId_m3045149504 (CatalogItem_t4132589244 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CItemIdU3Ek__BackingField_0(L_0);
		return;
	}
}
// System.String PlayFab.ClientModels.CatalogItem::get_ItemClass()
extern "C"  String_t* CatalogItem_get_ItemClass_m1559132228 (CatalogItem_t4132589244 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_U3CItemClassU3Ek__BackingField_1();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.CatalogItem::set_ItemClass(System.String)
extern "C"  void CatalogItem_set_ItemClass_m2644557519 (CatalogItem_t4132589244 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CItemClassU3Ek__BackingField_1(L_0);
		return;
	}
}
// System.String PlayFab.ClientModels.CatalogItem::get_CatalogVersion()
extern "C"  String_t* CatalogItem_get_CatalogVersion_m3810864162 (CatalogItem_t4132589244 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_U3CCatalogVersionU3Ek__BackingField_2();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.CatalogItem::set_CatalogVersion(System.String)
extern "C"  void CatalogItem_set_CatalogVersion_m2233729839 (CatalogItem_t4132589244 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CCatalogVersionU3Ek__BackingField_2(L_0);
		return;
	}
}
// System.String PlayFab.ClientModels.CatalogItem::get_DisplayName()
extern "C"  String_t* CatalogItem_get_DisplayName_m3711699724 (CatalogItem_t4132589244 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_U3CDisplayNameU3Ek__BackingField_3();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.CatalogItem::set_DisplayName(System.String)
extern "C"  void CatalogItem_set_DisplayName_m3097088199 (CatalogItem_t4132589244 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CDisplayNameU3Ek__BackingField_3(L_0);
		return;
	}
}
// System.String PlayFab.ClientModels.CatalogItem::get_Description()
extern "C"  String_t* CatalogItem_get_Description_m1955631323 (CatalogItem_t4132589244 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_U3CDescriptionU3Ek__BackingField_4();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.CatalogItem::set_Description(System.String)
extern "C"  void CatalogItem_set_Description_m2245751640 (CatalogItem_t4132589244 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CDescriptionU3Ek__BackingField_4(L_0);
		return;
	}
}
// System.Collections.Generic.Dictionary`2<System.String,System.UInt32> PlayFab.ClientModels.CatalogItem::get_VirtualCurrencyPrices()
extern "C"  Dictionary_2_t2623623230 * CatalogItem_get_VirtualCurrencyPrices_m2816134290 (CatalogItem_t4132589244 * __this, const MethodInfo* method)
{
	{
		Dictionary_2_t2623623230 * L_0 = __this->get_U3CVirtualCurrencyPricesU3Ek__BackingField_5();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.CatalogItem::set_VirtualCurrencyPrices(System.Collections.Generic.Dictionary`2<System.String,System.UInt32>)
extern "C"  void CatalogItem_set_VirtualCurrencyPrices_m2865484557 (CatalogItem_t4132589244 * __this, Dictionary_2_t2623623230 * ___value0, const MethodInfo* method)
{
	{
		Dictionary_2_t2623623230 * L_0 = ___value0;
		__this->set_U3CVirtualCurrencyPricesU3Ek__BackingField_5(L_0);
		return;
	}
}
// System.Collections.Generic.Dictionary`2<System.String,System.UInt32> PlayFab.ClientModels.CatalogItem::get_RealCurrencyPrices()
extern "C"  Dictionary_2_t2623623230 * CatalogItem_get_RealCurrencyPrices_m3052650031 (CatalogItem_t4132589244 * __this, const MethodInfo* method)
{
	{
		Dictionary_2_t2623623230 * L_0 = __this->get_U3CRealCurrencyPricesU3Ek__BackingField_6();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.CatalogItem::set_RealCurrencyPrices(System.Collections.Generic.Dictionary`2<System.String,System.UInt32>)
extern "C"  void CatalogItem_set_RealCurrencyPrices_m1479368422 (CatalogItem_t4132589244 * __this, Dictionary_2_t2623623230 * ___value0, const MethodInfo* method)
{
	{
		Dictionary_2_t2623623230 * L_0 = ___value0;
		__this->set_U3CRealCurrencyPricesU3Ek__BackingField_6(L_0);
		return;
	}
}
// System.Collections.Generic.List`1<System.String> PlayFab.ClientModels.CatalogItem::get_Tags()
extern "C"  List_1_t1765447871 * CatalogItem_get_Tags_m762111114 (CatalogItem_t4132589244 * __this, const MethodInfo* method)
{
	{
		List_1_t1765447871 * L_0 = __this->get_U3CTagsU3Ek__BackingField_7();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.CatalogItem::set_Tags(System.Collections.Generic.List`1<System.String>)
extern "C"  void CatalogItem_set_Tags_m3396385729 (CatalogItem_t4132589244 * __this, List_1_t1765447871 * ___value0, const MethodInfo* method)
{
	{
		List_1_t1765447871 * L_0 = ___value0;
		__this->set_U3CTagsU3Ek__BackingField_7(L_0);
		return;
	}
}
// System.String PlayFab.ClientModels.CatalogItem::get_CustomData()
extern "C"  String_t* CatalogItem_get_CustomData_m4003440222 (CatalogItem_t4132589244 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_U3CCustomDataU3Ek__BackingField_8();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.CatalogItem::set_CustomData(System.String)
extern "C"  void CatalogItem_set_CustomData_m3866094067 (CatalogItem_t4132589244 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CCustomDataU3Ek__BackingField_8(L_0);
		return;
	}
}
// PlayFab.ClientModels.CatalogItemConsumableInfo PlayFab.ClientModels.CatalogItem::get_Consumable()
extern "C"  CatalogItemConsumableInfo_t2956282765 * CatalogItem_get_Consumable_m937351617 (CatalogItem_t4132589244 * __this, const MethodInfo* method)
{
	{
		CatalogItemConsumableInfo_t2956282765 * L_0 = __this->get_U3CConsumableU3Ek__BackingField_9();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.CatalogItem::set_Consumable(PlayFab.ClientModels.CatalogItemConsumableInfo)
extern "C"  void CatalogItem_set_Consumable_m2864583224 (CatalogItem_t4132589244 * __this, CatalogItemConsumableInfo_t2956282765 * ___value0, const MethodInfo* method)
{
	{
		CatalogItemConsumableInfo_t2956282765 * L_0 = ___value0;
		__this->set_U3CConsumableU3Ek__BackingField_9(L_0);
		return;
	}
}
// PlayFab.ClientModels.CatalogItemContainerInfo PlayFab.ClientModels.CatalogItem::get_Container()
extern "C"  CatalogItemContainerInfo_t284142995 * CatalogItem_get_Container_m3277564209 (CatalogItem_t4132589244 * __this, const MethodInfo* method)
{
	{
		CatalogItemContainerInfo_t284142995 * L_0 = __this->get_U3CContainerU3Ek__BackingField_10();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.CatalogItem::set_Container(PlayFab.ClientModels.CatalogItemContainerInfo)
extern "C"  void CatalogItem_set_Container_m2475862850 (CatalogItem_t4132589244 * __this, CatalogItemContainerInfo_t284142995 * ___value0, const MethodInfo* method)
{
	{
		CatalogItemContainerInfo_t284142995 * L_0 = ___value0;
		__this->set_U3CContainerU3Ek__BackingField_10(L_0);
		return;
	}
}
// PlayFab.ClientModels.CatalogItemBundleInfo PlayFab.ClientModels.CatalogItem::get_Bundle()
extern "C"  CatalogItemBundleInfo_t1005829164 * CatalogItem_get_Bundle_m4190914977 (CatalogItem_t4132589244 * __this, const MethodInfo* method)
{
	{
		CatalogItemBundleInfo_t1005829164 * L_0 = __this->get_U3CBundleU3Ek__BackingField_11();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.CatalogItem::set_Bundle(PlayFab.ClientModels.CatalogItemBundleInfo)
extern "C"  void CatalogItem_set_Bundle_m1182022808 (CatalogItem_t4132589244 * __this, CatalogItemBundleInfo_t1005829164 * ___value0, const MethodInfo* method)
{
	{
		CatalogItemBundleInfo_t1005829164 * L_0 = ___value0;
		__this->set_U3CBundleU3Ek__BackingField_11(L_0);
		return;
	}
}
// System.Boolean PlayFab.ClientModels.CatalogItem::get_CanBecomeCharacter()
extern "C"  bool CatalogItem_get_CanBecomeCharacter_m3742623846 (CatalogItem_t4132589244 * __this, const MethodInfo* method)
{
	{
		bool L_0 = __this->get_U3CCanBecomeCharacterU3Ek__BackingField_12();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.CatalogItem::set_CanBecomeCharacter(System.Boolean)
extern "C"  void CatalogItem_set_CanBecomeCharacter_m1912467485 (CatalogItem_t4132589244 * __this, bool ___value0, const MethodInfo* method)
{
	{
		bool L_0 = ___value0;
		__this->set_U3CCanBecomeCharacterU3Ek__BackingField_12(L_0);
		return;
	}
}
// System.Boolean PlayFab.ClientModels.CatalogItem::get_IsStackable()
extern "C"  bool CatalogItem_get_IsStackable_m1538119558 (CatalogItem_t4132589244 * __this, const MethodInfo* method)
{
	{
		bool L_0 = __this->get_U3CIsStackableU3Ek__BackingField_13();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.CatalogItem::set_IsStackable(System.Boolean)
extern "C"  void CatalogItem_set_IsStackable_m1760282045 (CatalogItem_t4132589244 * __this, bool ___value0, const MethodInfo* method)
{
	{
		bool L_0 = ___value0;
		__this->set_U3CIsStackableU3Ek__BackingField_13(L_0);
		return;
	}
}
// System.Boolean PlayFab.ClientModels.CatalogItem::get_IsTradable()
extern "C"  bool CatalogItem_get_IsTradable_m191111865 (CatalogItem_t4132589244 * __this, const MethodInfo* method)
{
	{
		bool L_0 = __this->get_U3CIsTradableU3Ek__BackingField_14();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.CatalogItem::set_IsTradable(System.Boolean)
extern "C"  void CatalogItem_set_IsTradable_m984407344 (CatalogItem_t4132589244 * __this, bool ___value0, const MethodInfo* method)
{
	{
		bool L_0 = ___value0;
		__this->set_U3CIsTradableU3Ek__BackingField_14(L_0);
		return;
	}
}
// System.String PlayFab.ClientModels.CatalogItem::get_ItemImageUrl()
extern "C"  String_t* CatalogItem_get_ItemImageUrl_m1223217770 (CatalogItem_t4132589244 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_U3CItemImageUrlU3Ek__BackingField_15();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.CatalogItem::set_ItemImageUrl(System.String)
extern "C"  void CatalogItem_set_ItemImageUrl_m2020329895 (CatalogItem_t4132589244 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CItemImageUrlU3Ek__BackingField_15(L_0);
		return;
	}
}
// System.Void PlayFab.ClientModels.CatalogItemBundleInfo::.ctor()
extern "C"  void CatalogItemBundleInfo__ctor_m1252803709 (CatalogItemBundleInfo_t1005829164 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Collections.Generic.List`1<System.String> PlayFab.ClientModels.CatalogItemBundleInfo::get_BundledItems()
extern "C"  List_1_t1765447871 * CatalogItemBundleInfo_get_BundledItems_m308254623 (CatalogItemBundleInfo_t1005829164 * __this, const MethodInfo* method)
{
	{
		List_1_t1765447871 * L_0 = __this->get_U3CBundledItemsU3Ek__BackingField_0();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.CatalogItemBundleInfo::set_BundledItems(System.Collections.Generic.List`1<System.String>)
extern "C"  void CatalogItemBundleInfo_set_BundledItems_m1385412374 (CatalogItemBundleInfo_t1005829164 * __this, List_1_t1765447871 * ___value0, const MethodInfo* method)
{
	{
		List_1_t1765447871 * L_0 = ___value0;
		__this->set_U3CBundledItemsU3Ek__BackingField_0(L_0);
		return;
	}
}
// System.Collections.Generic.List`1<System.String> PlayFab.ClientModels.CatalogItemBundleInfo::get_BundledResultTables()
extern "C"  List_1_t1765447871 * CatalogItemBundleInfo_get_BundledResultTables_m919145669 (CatalogItemBundleInfo_t1005829164 * __this, const MethodInfo* method)
{
	{
		List_1_t1765447871 * L_0 = __this->get_U3CBundledResultTablesU3Ek__BackingField_1();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.CatalogItemBundleInfo::set_BundledResultTables(System.Collections.Generic.List`1<System.String>)
extern "C"  void CatalogItemBundleInfo_set_BundledResultTables_m3766801494 (CatalogItemBundleInfo_t1005829164 * __this, List_1_t1765447871 * ___value0, const MethodInfo* method)
{
	{
		List_1_t1765447871 * L_0 = ___value0;
		__this->set_U3CBundledResultTablesU3Ek__BackingField_1(L_0);
		return;
	}
}
// System.Collections.Generic.Dictionary`2<System.String,System.UInt32> PlayFab.ClientModels.CatalogItemBundleInfo::get_BundledVirtualCurrencies()
extern "C"  Dictionary_2_t2623623230 * CatalogItemBundleInfo_get_BundledVirtualCurrencies_m2146316542 (CatalogItemBundleInfo_t1005829164 * __this, const MethodInfo* method)
{
	{
		Dictionary_2_t2623623230 * L_0 = __this->get_U3CBundledVirtualCurrenciesU3Ek__BackingField_2();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.CatalogItemBundleInfo::set_BundledVirtualCurrencies(System.Collections.Generic.Dictionary`2<System.String,System.UInt32>)
extern "C"  void CatalogItemBundleInfo_set_BundledVirtualCurrencies_m2271108917 (CatalogItemBundleInfo_t1005829164 * __this, Dictionary_2_t2623623230 * ___value0, const MethodInfo* method)
{
	{
		Dictionary_2_t2623623230 * L_0 = ___value0;
		__this->set_U3CBundledVirtualCurrenciesU3Ek__BackingField_2(L_0);
		return;
	}
}
// System.Void PlayFab.ClientModels.CatalogItemConsumableInfo::.ctor()
extern "C"  void CatalogItemConsumableInfo__ctor_m107689788 (CatalogItemConsumableInfo_t2956282765 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Nullable`1<System.UInt32> PlayFab.ClientModels.CatalogItemConsumableInfo::get_UsageCount()
extern "C"  Nullable_1_t3871963234  CatalogItemConsumableInfo_get_UsageCount_m4101587403 (CatalogItemConsumableInfo_t2956282765 * __this, const MethodInfo* method)
{
	{
		Nullable_1_t3871963234  L_0 = __this->get_U3CUsageCountU3Ek__BackingField_0();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.CatalogItemConsumableInfo::set_UsageCount(System.Nullable`1<System.UInt32>)
extern "C"  void CatalogItemConsumableInfo_set_UsageCount_m1306702466 (CatalogItemConsumableInfo_t2956282765 * __this, Nullable_1_t3871963234  ___value0, const MethodInfo* method)
{
	{
		Nullable_1_t3871963234  L_0 = ___value0;
		__this->set_U3CUsageCountU3Ek__BackingField_0(L_0);
		return;
	}
}
// System.Nullable`1<System.UInt32> PlayFab.ClientModels.CatalogItemConsumableInfo::get_UsagePeriod()
extern "C"  Nullable_1_t3871963234  CatalogItemConsumableInfo_get_UsagePeriod_m3401157223 (CatalogItemConsumableInfo_t2956282765 * __this, const MethodInfo* method)
{
	{
		Nullable_1_t3871963234  L_0 = __this->get_U3CUsagePeriodU3Ek__BackingField_1();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.CatalogItemConsumableInfo::set_UsagePeriod(System.Nullable`1<System.UInt32>)
extern "C"  void CatalogItemConsumableInfo_set_UsagePeriod_m504126926 (CatalogItemConsumableInfo_t2956282765 * __this, Nullable_1_t3871963234  ___value0, const MethodInfo* method)
{
	{
		Nullable_1_t3871963234  L_0 = ___value0;
		__this->set_U3CUsagePeriodU3Ek__BackingField_1(L_0);
		return;
	}
}
// System.String PlayFab.ClientModels.CatalogItemConsumableInfo::get_UsagePeriodGroup()
extern "C"  String_t* CatalogItemConsumableInfo_get_UsagePeriodGroup_m536027537 (CatalogItemConsumableInfo_t2956282765 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_U3CUsagePeriodGroupU3Ek__BackingField_2();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.CatalogItemConsumableInfo::set_UsagePeriodGroup(System.String)
extern "C"  void CatalogItemConsumableInfo_set_UsagePeriodGroup_m3129893152 (CatalogItemConsumableInfo_t2956282765 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CUsagePeriodGroupU3Ek__BackingField_2(L_0);
		return;
	}
}
// System.Void PlayFab.ClientModels.CatalogItemContainerInfo::.ctor()
extern "C"  void CatalogItemContainerInfo__ctor_m2912062986 (CatalogItemContainerInfo_t284142995 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.String PlayFab.ClientModels.CatalogItemContainerInfo::get_KeyItemId()
extern "C"  String_t* CatalogItemContainerInfo_get_KeyItemId_m975719215 (CatalogItemContainerInfo_t284142995 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_U3CKeyItemIdU3Ek__BackingField_0();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.CatalogItemContainerInfo::set_KeyItemId(System.String)
extern "C"  void CatalogItemContainerInfo_set_KeyItemId_m1235375786 (CatalogItemContainerInfo_t284142995 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CKeyItemIdU3Ek__BackingField_0(L_0);
		return;
	}
}
// System.Collections.Generic.List`1<System.String> PlayFab.ClientModels.CatalogItemContainerInfo::get_ItemContents()
extern "C"  List_1_t1765447871 * CatalogItemContainerInfo_get_ItemContents_m3624435903 (CatalogItemContainerInfo_t284142995 * __this, const MethodInfo* method)
{
	{
		List_1_t1765447871 * L_0 = __this->get_U3CItemContentsU3Ek__BackingField_1();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.CatalogItemContainerInfo::set_ItemContents(System.Collections.Generic.List`1<System.String>)
extern "C"  void CatalogItemContainerInfo_set_ItemContents_m335383096 (CatalogItemContainerInfo_t284142995 * __this, List_1_t1765447871 * ___value0, const MethodInfo* method)
{
	{
		List_1_t1765447871 * L_0 = ___value0;
		__this->set_U3CItemContentsU3Ek__BackingField_1(L_0);
		return;
	}
}
// System.Collections.Generic.List`1<System.String> PlayFab.ClientModels.CatalogItemContainerInfo::get_ResultTableContents()
extern "C"  List_1_t1765447871 * CatalogItemContainerInfo_get_ResultTableContents_m3221308763 (CatalogItemContainerInfo_t284142995 * __this, const MethodInfo* method)
{
	{
		List_1_t1765447871 * L_0 = __this->get_U3CResultTableContentsU3Ek__BackingField_2();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.CatalogItemContainerInfo::set_ResultTableContents(System.Collections.Generic.List`1<System.String>)
extern "C"  void CatalogItemContainerInfo_set_ResultTableContents_m1121499306 (CatalogItemContainerInfo_t284142995 * __this, List_1_t1765447871 * ___value0, const MethodInfo* method)
{
	{
		List_1_t1765447871 * L_0 = ___value0;
		__this->set_U3CResultTableContentsU3Ek__BackingField_2(L_0);
		return;
	}
}
// System.Collections.Generic.Dictionary`2<System.String,System.UInt32> PlayFab.ClientModels.CatalogItemContainerInfo::get_VirtualCurrencyContents()
extern "C"  Dictionary_2_t2623623230 * CatalogItemContainerInfo_get_VirtualCurrencyContents_m3342156843 (CatalogItemContainerInfo_t284142995 * __this, const MethodInfo* method)
{
	{
		Dictionary_2_t2623623230 * L_0 = __this->get_U3CVirtualCurrencyContentsU3Ek__BackingField_3();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.CatalogItemContainerInfo::set_VirtualCurrencyContents(System.Collections.Generic.Dictionary`2<System.String,System.UInt32>)
extern "C"  void CatalogItemContainerInfo_set_VirtualCurrencyContents_m2464960506 (CatalogItemContainerInfo_t284142995 * __this, Dictionary_2_t2623623230 * ___value0, const MethodInfo* method)
{
	{
		Dictionary_2_t2623623230 * L_0 = ___value0;
		__this->set_U3CVirtualCurrencyContentsU3Ek__BackingField_3(L_0);
		return;
	}
}
// System.Void PlayFab.ClientModels.CharacterLeaderboardEntry::.ctor()
extern "C"  void CharacterLeaderboardEntry__ctor_m2362660795 (CharacterLeaderboardEntry_t3117535278 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.String PlayFab.ClientModels.CharacterLeaderboardEntry::get_PlayFabId()
extern "C"  String_t* CharacterLeaderboardEntry_get_PlayFabId_m1462673179 (CharacterLeaderboardEntry_t3117535278 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_U3CPlayFabIdU3Ek__BackingField_0();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.CharacterLeaderboardEntry::set_PlayFabId(System.String)
extern "C"  void CharacterLeaderboardEntry_set_PlayFabId_m3756258968 (CharacterLeaderboardEntry_t3117535278 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CPlayFabIdU3Ek__BackingField_0(L_0);
		return;
	}
}
// System.String PlayFab.ClientModels.CharacterLeaderboardEntry::get_CharacterId()
extern "C"  String_t* CharacterLeaderboardEntry_get_CharacterId_m3092928785 (CharacterLeaderboardEntry_t3117535278 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_U3CCharacterIdU3Ek__BackingField_1();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.CharacterLeaderboardEntry::set_CharacterId(System.String)
extern "C"  void CharacterLeaderboardEntry_set_CharacterId_m3273862178 (CharacterLeaderboardEntry_t3117535278 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CCharacterIdU3Ek__BackingField_1(L_0);
		return;
	}
}
// System.String PlayFab.ClientModels.CharacterLeaderboardEntry::get_CharacterName()
extern "C"  String_t* CharacterLeaderboardEntry_get_CharacterName_m329683265 (CharacterLeaderboardEntry_t3117535278 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_U3CCharacterNameU3Ek__BackingField_2();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.CharacterLeaderboardEntry::set_CharacterName(System.String)
extern "C"  void CharacterLeaderboardEntry_set_CharacterName_m576283058 (CharacterLeaderboardEntry_t3117535278 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CCharacterNameU3Ek__BackingField_2(L_0);
		return;
	}
}
// System.String PlayFab.ClientModels.CharacterLeaderboardEntry::get_DisplayName()
extern "C"  String_t* CharacterLeaderboardEntry_get_DisplayName_m2912920346 (CharacterLeaderboardEntry_t3117535278 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_U3CDisplayNameU3Ek__BackingField_3();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.CharacterLeaderboardEntry::set_DisplayName(System.String)
extern "C"  void CharacterLeaderboardEntry_set_DisplayName_m1393703097 (CharacterLeaderboardEntry_t3117535278 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CDisplayNameU3Ek__BackingField_3(L_0);
		return;
	}
}
// System.String PlayFab.ClientModels.CharacterLeaderboardEntry::get_CharacterType()
extern "C"  String_t* CharacterLeaderboardEntry_get_CharacterType_m523712048 (CharacterLeaderboardEntry_t3117535278 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_U3CCharacterTypeU3Ek__BackingField_4();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.CharacterLeaderboardEntry::set_CharacterType(System.String)
extern "C"  void CharacterLeaderboardEntry_set_CharacterType_m1409448227 (CharacterLeaderboardEntry_t3117535278 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CCharacterTypeU3Ek__BackingField_4(L_0);
		return;
	}
}
// System.Int32 PlayFab.ClientModels.CharacterLeaderboardEntry::get_StatValue()
extern "C"  int32_t CharacterLeaderboardEntry_get_StatValue_m3415689695 (CharacterLeaderboardEntry_t3117535278 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get_U3CStatValueU3Ek__BackingField_5();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.CharacterLeaderboardEntry::set_StatValue(System.Int32)
extern "C"  void CharacterLeaderboardEntry_set_StatValue_m788977802 (CharacterLeaderboardEntry_t3117535278 * __this, int32_t ___value0, const MethodInfo* method)
{
	{
		int32_t L_0 = ___value0;
		__this->set_U3CStatValueU3Ek__BackingField_5(L_0);
		return;
	}
}
// System.Int32 PlayFab.ClientModels.CharacterLeaderboardEntry::get_Position()
extern "C"  int32_t CharacterLeaderboardEntry_get_Position_m2977898281 (CharacterLeaderboardEntry_t3117535278 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get_U3CPositionU3Ek__BackingField_6();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.CharacterLeaderboardEntry::set_Position(System.Int32)
extern "C"  void CharacterLeaderboardEntry_set_Position_m3540732896 (CharacterLeaderboardEntry_t3117535278 * __this, int32_t ___value0, const MethodInfo* method)
{
	{
		int32_t L_0 = ___value0;
		__this->set_U3CPositionU3Ek__BackingField_6(L_0);
		return;
	}
}
// System.Void PlayFab.ClientModels.CharacterResult::.ctor()
extern "C"  void CharacterResult__ctor_m3657928659 (CharacterResult_t274624662 * __this, const MethodInfo* method)
{
	{
		PlayFabResultCommon__ctor_m4291338422(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.String PlayFab.ClientModels.CharacterResult::get_CharacterId()
extern "C"  String_t* CharacterResult_get_CharacterId_m404130665 (CharacterResult_t274624662 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_U3CCharacterIdU3Ek__BackingField_2();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.CharacterResult::set_CharacterId(System.String)
extern "C"  void CharacterResult_set_CharacterId_m3537197322 (CharacterResult_t274624662 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CCharacterIdU3Ek__BackingField_2(L_0);
		return;
	}
}
// System.String PlayFab.ClientModels.CharacterResult::get_CharacterName()
extern "C"  String_t* CharacterResult_get_CharacterName_m1965002137 (CharacterResult_t274624662 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_U3CCharacterNameU3Ek__BackingField_3();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.CharacterResult::set_CharacterName(System.String)
extern "C"  void CharacterResult_set_CharacterName_m238285978 (CharacterResult_t274624662 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CCharacterNameU3Ek__BackingField_3(L_0);
		return;
	}
}
// System.String PlayFab.ClientModels.CharacterResult::get_CharacterType()
extern "C"  String_t* CharacterResult_get_CharacterType_m2159030920 (CharacterResult_t274624662 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_U3CCharacterTypeU3Ek__BackingField_4();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.CharacterResult::set_CharacterType(System.String)
extern "C"  void CharacterResult_set_CharacterType_m1071451147 (CharacterResult_t274624662 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CCharacterTypeU3Ek__BackingField_4(L_0);
		return;
	}
}
// System.Void PlayFab.ClientModels.ConfirmPurchaseRequest::.ctor()
extern "C"  void ConfirmPurchaseRequest__ctor_m2594010847 (ConfirmPurchaseRequest_t1360937886 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.String PlayFab.ClientModels.ConfirmPurchaseRequest::get_OrderId()
extern "C"  String_t* ConfirmPurchaseRequest_get_OrderId_m3715466368 (ConfirmPurchaseRequest_t1360937886 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_U3COrderIdU3Ek__BackingField_0();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.ConfirmPurchaseRequest::set_OrderId(System.String)
extern "C"  void ConfirmPurchaseRequest_set_OrderId_m1542164345 (ConfirmPurchaseRequest_t1360937886 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3COrderIdU3Ek__BackingField_0(L_0);
		return;
	}
}
// System.Void PlayFab.ClientModels.ConfirmPurchaseResult::.ctor()
extern "C"  void ConfirmPurchaseResult__ctor_m3049288603 (ConfirmPurchaseResult_t633238350 * __this, const MethodInfo* method)
{
	{
		PlayFabResultCommon__ctor_m4291338422(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.String PlayFab.ClientModels.ConfirmPurchaseResult::get_OrderId()
extern "C"  String_t* ConfirmPurchaseResult_get_OrderId_m407665430 (ConfirmPurchaseResult_t633238350 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_U3COrderIdU3Ek__BackingField_2();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.ConfirmPurchaseResult::set_OrderId(System.String)
extern "C"  void ConfirmPurchaseResult_set_OrderId_m1130922301 (ConfirmPurchaseResult_t633238350 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3COrderIdU3Ek__BackingField_2(L_0);
		return;
	}
}
// System.DateTime PlayFab.ClientModels.ConfirmPurchaseResult::get_PurchaseDate()
extern "C"  DateTime_t339033936  ConfirmPurchaseResult_get_PurchaseDate_m1385405946 (ConfirmPurchaseResult_t633238350 * __this, const MethodInfo* method)
{
	{
		DateTime_t339033936  L_0 = __this->get_U3CPurchaseDateU3Ek__BackingField_3();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.ConfirmPurchaseResult::set_PurchaseDate(System.DateTime)
extern "C"  void ConfirmPurchaseResult_set_PurchaseDate_m78305603 (ConfirmPurchaseResult_t633238350 * __this, DateTime_t339033936  ___value0, const MethodInfo* method)
{
	{
		DateTime_t339033936  L_0 = ___value0;
		__this->set_U3CPurchaseDateU3Ek__BackingField_3(L_0);
		return;
	}
}
// System.Collections.Generic.List`1<PlayFab.ClientModels.ItemInstance> PlayFab.ClientModels.ConfirmPurchaseResult::get_Items()
extern "C"  List_1_t1322103281 * ConfirmPurchaseResult_get_Items_m2678418825 (ConfirmPurchaseResult_t633238350 * __this, const MethodInfo* method)
{
	{
		List_1_t1322103281 * L_0 = __this->get_U3CItemsU3Ek__BackingField_4();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.ConfirmPurchaseResult::set_Items(System.Collections.Generic.List`1<PlayFab.ClientModels.ItemInstance>)
extern "C"  void ConfirmPurchaseResult_set_Items_m101409798 (ConfirmPurchaseResult_t633238350 * __this, List_1_t1322103281 * ___value0, const MethodInfo* method)
{
	{
		List_1_t1322103281 * L_0 = ___value0;
		__this->set_U3CItemsU3Ek__BackingField_4(L_0);
		return;
	}
}
// System.Void PlayFab.ClientModels.ConsumeItemRequest::.ctor()
extern "C"  void ConsumeItemRequest__ctor_m3753618733 (ConsumeItemRequest_t3677119120 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.String PlayFab.ClientModels.ConsumeItemRequest::get_ItemInstanceId()
extern "C"  String_t* ConsumeItemRequest_get_ItemInstanceId_m1500091840 (ConsumeItemRequest_t3677119120 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_U3CItemInstanceIdU3Ek__BackingField_0();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.ConsumeItemRequest::set_ItemInstanceId(System.String)
extern "C"  void ConsumeItemRequest_set_ItemInstanceId_m317791979 (ConsumeItemRequest_t3677119120 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CItemInstanceIdU3Ek__BackingField_0(L_0);
		return;
	}
}
// System.Int32 PlayFab.ClientModels.ConsumeItemRequest::get_ConsumeCount()
extern "C"  int32_t ConsumeItemRequest_get_ConsumeCount_m2531568741 (ConsumeItemRequest_t3677119120 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get_U3CConsumeCountU3Ek__BackingField_1();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.ConsumeItemRequest::set_ConsumeCount(System.Int32)
extern "C"  void ConsumeItemRequest_set_ConsumeCount_m3605101432 (ConsumeItemRequest_t3677119120 * __this, int32_t ___value0, const MethodInfo* method)
{
	{
		int32_t L_0 = ___value0;
		__this->set_U3CConsumeCountU3Ek__BackingField_1(L_0);
		return;
	}
}
// System.String PlayFab.ClientModels.ConsumeItemRequest::get_CharacterId()
extern "C"  String_t* ConsumeItemRequest_get_CharacterId_m896998953 (ConsumeItemRequest_t3677119120 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_U3CCharacterIdU3Ek__BackingField_2();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.ConsumeItemRequest::set_CharacterId(System.String)
extern "C"  void ConsumeItemRequest_set_CharacterId_m1750914672 (ConsumeItemRequest_t3677119120 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CCharacterIdU3Ek__BackingField_2(L_0);
		return;
	}
}
// System.Void PlayFab.ClientModels.ConsumeItemResult::.ctor()
extern "C"  void ConsumeItemResult__ctor_m3086695309 (ConsumeItemResult_t3201805852 * __this, const MethodInfo* method)
{
	{
		PlayFabResultCommon__ctor_m4291338422(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.String PlayFab.ClientModels.ConsumeItemResult::get_ItemInstanceId()
extern "C"  String_t* ConsumeItemResult_get_ItemInstanceId_m430327366 (ConsumeItemResult_t3201805852 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_U3CItemInstanceIdU3Ek__BackingField_2();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.ConsumeItemResult::set_ItemInstanceId(System.String)
extern "C"  void ConsumeItemResult_set_ItemInstanceId_m2149676363 (ConsumeItemResult_t3201805852 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CItemInstanceIdU3Ek__BackingField_2(L_0);
		return;
	}
}
// System.Int32 PlayFab.ClientModels.ConsumeItemResult::get_RemainingUses()
extern "C"  int32_t ConsumeItemResult_get_RemainingUses_m1918470742 (ConsumeItemResult_t3201805852 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get_U3CRemainingUsesU3Ek__BackingField_3();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.ConsumeItemResult::set_RemainingUses(System.Int32)
extern "C"  void ConsumeItemResult_set_RemainingUses_m3172814721 (ConsumeItemResult_t3201805852 * __this, int32_t ___value0, const MethodInfo* method)
{
	{
		int32_t L_0 = ___value0;
		__this->set_U3CRemainingUsesU3Ek__BackingField_3(L_0);
		return;
	}
}
// System.Void PlayFab.ClientModels.ConsumePSNEntitlementsRequest::.ctor()
extern "C"  void ConsumePSNEntitlementsRequest__ctor_m1083042655 (ConsumePSNEntitlementsRequest_t3123362058 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.String PlayFab.ClientModels.ConsumePSNEntitlementsRequest::get_CatalogVersion()
extern "C"  String_t* ConsumePSNEntitlementsRequest_get_CatalogVersion_m4193214128 (ConsumePSNEntitlementsRequest_t3123362058 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_U3CCatalogVersionU3Ek__BackingField_0();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.ConsumePSNEntitlementsRequest::set_CatalogVersion(System.String)
extern "C"  void ConsumePSNEntitlementsRequest_set_CatalogVersion_m3400780961 (ConsumePSNEntitlementsRequest_t3123362058 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CCatalogVersionU3Ek__BackingField_0(L_0);
		return;
	}
}
// System.Int32 PlayFab.ClientModels.ConsumePSNEntitlementsRequest::get_ServiceLabel()
extern "C"  int32_t ConsumePSNEntitlementsRequest_get_ServiceLabel_m1902926107 (ConsumePSNEntitlementsRequest_t3123362058 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get_U3CServiceLabelU3Ek__BackingField_1();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.ConsumePSNEntitlementsRequest::set_ServiceLabel(System.Int32)
extern "C"  void ConsumePSNEntitlementsRequest_set_ServiceLabel_m4197564114 (ConsumePSNEntitlementsRequest_t3123362058 * __this, int32_t ___value0, const MethodInfo* method)
{
	{
		int32_t L_0 = ___value0;
		__this->set_U3CServiceLabelU3Ek__BackingField_1(L_0);
		return;
	}
}
// System.Void PlayFab.ClientModels.ConsumePSNEntitlementsResult::.ctor()
extern "C"  void ConsumePSNEntitlementsResult__ctor_m229601051 (ConsumePSNEntitlementsResult_t4292321378 * __this, const MethodInfo* method)
{
	{
		PlayFabResultCommon__ctor_m4291338422(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Collections.Generic.List`1<PlayFab.ClientModels.ItemInstance> PlayFab.ClientModels.ConsumePSNEntitlementsResult::get_ItemsGranted()
extern "C"  List_1_t1322103281 * ConsumePSNEntitlementsResult_get_ItemsGranted_m1125965862 (ConsumePSNEntitlementsResult_t4292321378 * __this, const MethodInfo* method)
{
	{
		List_1_t1322103281 * L_0 = __this->get_U3CItemsGrantedU3Ek__BackingField_2();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.ConsumePSNEntitlementsResult::set_ItemsGranted(System.Collections.Generic.List`1<PlayFab.ClientModels.ItemInstance>)
extern "C"  void ConsumePSNEntitlementsResult_set_ItemsGranted_m2137990219 (ConsumePSNEntitlementsResult_t4292321378 * __this, List_1_t1322103281 * ___value0, const MethodInfo* method)
{
	{
		List_1_t1322103281 * L_0 = ___value0;
		__this->set_U3CItemsGrantedU3Ek__BackingField_2(L_0);
		return;
	}
}
// System.Void PlayFab.ClientModels.CreateSharedGroupRequest::.ctor()
extern "C"  void CreateSharedGroupRequest__ctor_m3581596924 (CreateSharedGroupRequest_t972582497 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.String PlayFab.ClientModels.CreateSharedGroupRequest::get_SharedGroupId()
extern "C"  String_t* CreateSharedGroupRequest_get_SharedGroupId_m4116235689 (CreateSharedGroupRequest_t972582497 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_U3CSharedGroupIdU3Ek__BackingField_0();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.CreateSharedGroupRequest::set_SharedGroupId(System.String)
extern "C"  void CreateSharedGroupRequest_set_SharedGroupId_m3624410864 (CreateSharedGroupRequest_t972582497 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CSharedGroupIdU3Ek__BackingField_0(L_0);
		return;
	}
}
// System.Void PlayFab.ClientModels.CreateSharedGroupResult::.ctor()
extern "C"  void CreateSharedGroupResult__ctor_m2665504222 (CreateSharedGroupResult_t2976015403 * __this, const MethodInfo* method)
{
	{
		PlayFabResultCommon__ctor_m4291338422(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.String PlayFab.ClientModels.CreateSharedGroupResult::get_SharedGroupId()
extern "C"  String_t* CreateSharedGroupResult_get_SharedGroupId_m2963015781 (CreateSharedGroupResult_t2976015403 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_U3CSharedGroupIdU3Ek__BackingField_2();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.CreateSharedGroupResult::set_SharedGroupId(System.String)
extern "C"  void CreateSharedGroupResult_set_SharedGroupId_m273086542 (CreateSharedGroupResult_t2976015403 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CSharedGroupIdU3Ek__BackingField_2(L_0);
		return;
	}
}
// System.Void PlayFab.ClientModels.CurrentGamesRequest::.ctor()
extern "C"  void CurrentGamesRequest__ctor_m1443172850 (CurrentGamesRequest_t3387974807 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Nullable`1<PlayFab.ClientModels.Region> PlayFab.ClientModels.CurrentGamesRequest::get_Region()
extern "C"  Nullable_1_t212373688  CurrentGamesRequest_get_Region_m75788213 (CurrentGamesRequest_t3387974807 * __this, const MethodInfo* method)
{
	{
		Nullable_1_t212373688  L_0 = __this->get_U3CRegionU3Ek__BackingField_0();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.CurrentGamesRequest::set_Region(System.Nullable`1<PlayFab.ClientModels.Region>)
extern "C"  void CurrentGamesRequest_set_Region_m3072301612 (CurrentGamesRequest_t3387974807 * __this, Nullable_1_t212373688  ___value0, const MethodInfo* method)
{
	{
		Nullable_1_t212373688  L_0 = ___value0;
		__this->set_U3CRegionU3Ek__BackingField_0(L_0);
		return;
	}
}
// System.String PlayFab.ClientModels.CurrentGamesRequest::get_BuildVersion()
extern "C"  String_t* CurrentGamesRequest_get_BuildVersion_m3772793672 (CurrentGamesRequest_t3387974807 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_U3CBuildVersionU3Ek__BackingField_1();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.CurrentGamesRequest::set_BuildVersion(System.String)
extern "C"  void CurrentGamesRequest_set_BuildVersion_m106086025 (CurrentGamesRequest_t3387974807 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CBuildVersionU3Ek__BackingField_1(L_0);
		return;
	}
}
// System.String PlayFab.ClientModels.CurrentGamesRequest::get_GameMode()
extern "C"  String_t* CurrentGamesRequest_get_GameMode_m640173363 (CurrentGamesRequest_t3387974807 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_U3CGameModeU3Ek__BackingField_2();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.CurrentGamesRequest::set_GameMode(System.String)
extern "C"  void CurrentGamesRequest_set_GameMode_m3345068542 (CurrentGamesRequest_t3387974807 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CGameModeU3Ek__BackingField_2(L_0);
		return;
	}
}
// System.String PlayFab.ClientModels.CurrentGamesRequest::get_StatisticName()
extern "C"  String_t* CurrentGamesRequest_get_StatisticName_m989280159 (CurrentGamesRequest_t3387974807 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_U3CStatisticNameU3Ek__BackingField_3();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.CurrentGamesRequest::set_StatisticName(System.String)
extern "C"  void CurrentGamesRequest_set_StatisticName_m2527718868 (CurrentGamesRequest_t3387974807 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CStatisticNameU3Ek__BackingField_3(L_0);
		return;
	}
}
// System.Void PlayFab.ClientModels.CurrentGamesResult::.ctor()
extern "C"  void CurrentGamesResult__ctor_m2319428136 (CurrentGamesResult_t4162309941 * __this, const MethodInfo* method)
{
	{
		PlayFabResultCommon__ctor_m4291338422(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Collections.Generic.List`1<PlayFab.ClientModels.GameInfo> PlayFab.ClientModels.CurrentGamesResult::get_Games()
extern "C"  List_1_t2564353257 * CurrentGamesResult_get_Games_m725776509 (CurrentGamesResult_t4162309941 * __this, const MethodInfo* method)
{
	{
		List_1_t2564353257 * L_0 = __this->get_U3CGamesU3Ek__BackingField_2();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.CurrentGamesResult::set_Games(System.Collections.Generic.List`1<PlayFab.ClientModels.GameInfo>)
extern "C"  void CurrentGamesResult_set_Games_m2923608396 (CurrentGamesResult_t4162309941 * __this, List_1_t2564353257 * ___value0, const MethodInfo* method)
{
	{
		List_1_t2564353257 * L_0 = ___value0;
		__this->set_U3CGamesU3Ek__BackingField_2(L_0);
		return;
	}
}
// System.Int32 PlayFab.ClientModels.CurrentGamesResult::get_PlayerCount()
extern "C"  int32_t CurrentGamesResult_get_PlayerCount_m1586154393 (CurrentGamesResult_t4162309941 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get_U3CPlayerCountU3Ek__BackingField_3();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.CurrentGamesResult::set_PlayerCount(System.Int32)
extern "C"  void CurrentGamesResult_set_PlayerCount_m2757241320 (CurrentGamesResult_t4162309941 * __this, int32_t ___value0, const MethodInfo* method)
{
	{
		int32_t L_0 = ___value0;
		__this->set_U3CPlayerCountU3Ek__BackingField_3(L_0);
		return;
	}
}
// System.Int32 PlayFab.ClientModels.CurrentGamesResult::get_GameCount()
extern "C"  int32_t CurrentGamesResult_get_GameCount_m1354024936 (CurrentGamesResult_t4162309941 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get_U3CGameCountU3Ek__BackingField_4();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.CurrentGamesResult::set_GameCount(System.Int32)
extern "C"  void CurrentGamesResult_set_GameCount_m2480058167 (CurrentGamesResult_t4162309941 * __this, int32_t ___value0, const MethodInfo* method)
{
	{
		int32_t L_0 = ___value0;
		__this->set_U3CGameCountU3Ek__BackingField_4(L_0);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
