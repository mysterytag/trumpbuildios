﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Zenject.DiContainer/<Install>c__AnonStorey18B
struct U3CInstallU3Ec__AnonStorey18B_t2166929523;
// Zenject.IInstaller
struct IInstaller_t2455223476;

#include "codegen/il2cpp-codegen.h"

// System.Void Zenject.DiContainer/<Install>c__AnonStorey18B::.ctor()
extern "C"  void U3CInstallU3Ec__AnonStorey18B__ctor_m2194630602 (U3CInstallU3Ec__AnonStorey18B_t2166929523 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Zenject.DiContainer/<Install>c__AnonStorey18B::<>m__2C8(Zenject.IInstaller)
extern "C"  bool U3CInstallU3Ec__AnonStorey18B_U3CU3Em__2C8_m1063900452 (U3CInstallU3Ec__AnonStorey18B_t2166929523 * __this, Il2CppObject * ___x0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
