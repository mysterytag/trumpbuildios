﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Queue`1<UniRx.Timestamped`1<System.Object>>
struct Queue_1_t4227271813;
// System.Array
struct Il2CppArray;
// System.Object
struct Il2CppObject;
// System.Collections.Generic.IEnumerator`1<UniRx.Timestamped`1<System.Object>>
struct IEnumerator_1_t4002290721;
// System.Collections.IEnumerator
struct IEnumerator_t287207039;
// UniRx.Timestamped`1<System.Object>[]
struct Timestamped_1U5BU5D_t3409075980;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array2840145358.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Timestamped_1_2519184273.h"
#include "System_System_Collections_Generic_Queue_1_Enumerat1401926234.h"

// System.Void System.Collections.Generic.Queue`1<UniRx.Timestamped`1<System.Object>>::.ctor()
extern "C"  void Queue_1__ctor_m3211090677_gshared (Queue_1_t4227271813 * __this, const MethodInfo* method);
#define Queue_1__ctor_m3211090677(__this, method) ((  void (*) (Queue_1_t4227271813 *, const MethodInfo*))Queue_1__ctor_m3211090677_gshared)(__this, method)
// System.Void System.Collections.Generic.Queue`1<UniRx.Timestamped`1<System.Object>>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void Queue_1_System_Collections_ICollection_CopyTo_m1150297071_gshared (Queue_1_t4227271813 * __this, Il2CppArray * ___array0, int32_t ___idx1, const MethodInfo* method);
#define Queue_1_System_Collections_ICollection_CopyTo_m1150297071(__this, ___array0, ___idx1, method) ((  void (*) (Queue_1_t4227271813 *, Il2CppArray *, int32_t, const MethodInfo*))Queue_1_System_Collections_ICollection_CopyTo_m1150297071_gshared)(__this, ___array0, ___idx1, method)
// System.Boolean System.Collections.Generic.Queue`1<UniRx.Timestamped`1<System.Object>>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool Queue_1_System_Collections_ICollection_get_IsSynchronized_m3951521539_gshared (Queue_1_t4227271813 * __this, const MethodInfo* method);
#define Queue_1_System_Collections_ICollection_get_IsSynchronized_m3951521539(__this, method) ((  bool (*) (Queue_1_t4227271813 *, const MethodInfo*))Queue_1_System_Collections_ICollection_get_IsSynchronized_m3951521539_gshared)(__this, method)
// System.Object System.Collections.Generic.Queue`1<UniRx.Timestamped`1<System.Object>>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * Queue_1_System_Collections_ICollection_get_SyncRoot_m2228925955_gshared (Queue_1_t4227271813 * __this, const MethodInfo* method);
#define Queue_1_System_Collections_ICollection_get_SyncRoot_m2228925955(__this, method) ((  Il2CppObject * (*) (Queue_1_t4227271813 *, const MethodInfo*))Queue_1_System_Collections_ICollection_get_SyncRoot_m2228925955_gshared)(__this, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.Queue`1<UniRx.Timestamped`1<System.Object>>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
extern "C"  Il2CppObject* Queue_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m2163761671_gshared (Queue_1_t4227271813 * __this, const MethodInfo* method);
#define Queue_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m2163761671(__this, method) ((  Il2CppObject* (*) (Queue_1_t4227271813 *, const MethodInfo*))Queue_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m2163761671_gshared)(__this, method)
// System.Collections.IEnumerator System.Collections.Generic.Queue`1<UniRx.Timestamped`1<System.Object>>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * Queue_1_System_Collections_IEnumerable_GetEnumerator_m2467565758_gshared (Queue_1_t4227271813 * __this, const MethodInfo* method);
#define Queue_1_System_Collections_IEnumerable_GetEnumerator_m2467565758(__this, method) ((  Il2CppObject * (*) (Queue_1_t4227271813 *, const MethodInfo*))Queue_1_System_Collections_IEnumerable_GetEnumerator_m2467565758_gshared)(__this, method)
// System.Void System.Collections.Generic.Queue`1<UniRx.Timestamped`1<System.Object>>::Clear()
extern "C"  void Queue_1_Clear_m617223968_gshared (Queue_1_t4227271813 * __this, const MethodInfo* method);
#define Queue_1_Clear_m617223968(__this, method) ((  void (*) (Queue_1_t4227271813 *, const MethodInfo*))Queue_1_Clear_m617223968_gshared)(__this, method)
// System.Void System.Collections.Generic.Queue`1<UniRx.Timestamped`1<System.Object>>::CopyTo(T[],System.Int32)
extern "C"  void Queue_1_CopyTo_m3529159770_gshared (Queue_1_t4227271813 * __this, Timestamped_1U5BU5D_t3409075980* ___array0, int32_t ___idx1, const MethodInfo* method);
#define Queue_1_CopyTo_m3529159770(__this, ___array0, ___idx1, method) ((  void (*) (Queue_1_t4227271813 *, Timestamped_1U5BU5D_t3409075980*, int32_t, const MethodInfo*))Queue_1_CopyTo_m3529159770_gshared)(__this, ___array0, ___idx1, method)
// T System.Collections.Generic.Queue`1<UniRx.Timestamped`1<System.Object>>::Dequeue()
extern "C"  Timestamped_1_t2519184273  Queue_1_Dequeue_m2561991780_gshared (Queue_1_t4227271813 * __this, const MethodInfo* method);
#define Queue_1_Dequeue_m2561991780(__this, method) ((  Timestamped_1_t2519184273  (*) (Queue_1_t4227271813 *, const MethodInfo*))Queue_1_Dequeue_m2561991780_gshared)(__this, method)
// T System.Collections.Generic.Queue`1<UniRx.Timestamped`1<System.Object>>::Peek()
extern "C"  Timestamped_1_t2519184273  Queue_1_Peek_m1270856265_gshared (Queue_1_t4227271813 * __this, const MethodInfo* method);
#define Queue_1_Peek_m1270856265(__this, method) ((  Timestamped_1_t2519184273  (*) (Queue_1_t4227271813 *, const MethodInfo*))Queue_1_Peek_m1270856265_gshared)(__this, method)
// System.Void System.Collections.Generic.Queue`1<UniRx.Timestamped`1<System.Object>>::Enqueue(T)
extern "C"  void Queue_1_Enqueue_m961120547_gshared (Queue_1_t4227271813 * __this, Timestamped_1_t2519184273  ___item0, const MethodInfo* method);
#define Queue_1_Enqueue_m961120547(__this, ___item0, method) ((  void (*) (Queue_1_t4227271813 *, Timestamped_1_t2519184273 , const MethodInfo*))Queue_1_Enqueue_m961120547_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.Queue`1<UniRx.Timestamped`1<System.Object>>::SetCapacity(System.Int32)
extern "C"  void Queue_1_SetCapacity_m3897259424_gshared (Queue_1_t4227271813 * __this, int32_t ___new_size0, const MethodInfo* method);
#define Queue_1_SetCapacity_m3897259424(__this, ___new_size0, method) ((  void (*) (Queue_1_t4227271813 *, int32_t, const MethodInfo*))Queue_1_SetCapacity_m3897259424_gshared)(__this, ___new_size0, method)
// System.Int32 System.Collections.Generic.Queue`1<UniRx.Timestamped`1<System.Object>>::get_Count()
extern "C"  int32_t Queue_1_get_Count_m722926153_gshared (Queue_1_t4227271813 * __this, const MethodInfo* method);
#define Queue_1_get_Count_m722926153(__this, method) ((  int32_t (*) (Queue_1_t4227271813 *, const MethodInfo*))Queue_1_get_Count_m722926153_gshared)(__this, method)
// System.Collections.Generic.Queue`1/Enumerator<T> System.Collections.Generic.Queue`1<UniRx.Timestamped`1<System.Object>>::GetEnumerator()
extern "C"  Enumerator_t1401926234  Queue_1_GetEnumerator_m1147866868_gshared (Queue_1_t4227271813 * __this, const MethodInfo* method);
#define Queue_1_GetEnumerator_m1147866868(__this, method) ((  Enumerator_t1401926234  (*) (Queue_1_t4227271813 *, const MethodInfo*))Queue_1_GetEnumerator_m1147866868_gshared)(__this, method)
