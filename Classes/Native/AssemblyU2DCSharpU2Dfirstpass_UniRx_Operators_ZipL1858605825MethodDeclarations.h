﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.ZipLatestFunc`7<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>
struct ZipLatestFunc_7_t1858605825;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t537683269;
// System.AsyncCallback
struct AsyncCallback_t1363551830;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_IntPtr676692020.h"
#include "mscorlib_System_AsyncCallback1363551830.h"

// System.Void UniRx.Operators.ZipLatestFunc`7<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>::.ctor(System.Object,System.IntPtr)
extern "C"  void ZipLatestFunc_7__ctor_m1118945527_gshared (ZipLatestFunc_7_t1858605825 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method);
#define ZipLatestFunc_7__ctor_m1118945527(__this, ___object0, ___method1, method) ((  void (*) (ZipLatestFunc_7_t1858605825 *, Il2CppObject *, IntPtr_t, const MethodInfo*))ZipLatestFunc_7__ctor_m1118945527_gshared)(__this, ___object0, ___method1, method)
// TR UniRx.Operators.ZipLatestFunc`7<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>::Invoke(T1,T2,T3,T4,T5,T6)
extern "C"  Il2CppObject * ZipLatestFunc_7_Invoke_m3731469579_gshared (ZipLatestFunc_7_t1858605825 * __this, Il2CppObject * ___arg10, Il2CppObject * ___arg21, Il2CppObject * ___arg32, Il2CppObject * ___arg43, Il2CppObject * ___arg54, Il2CppObject * ___arg65, const MethodInfo* method);
#define ZipLatestFunc_7_Invoke_m3731469579(__this, ___arg10, ___arg21, ___arg32, ___arg43, ___arg54, ___arg65, method) ((  Il2CppObject * (*) (ZipLatestFunc_7_t1858605825 *, Il2CppObject *, Il2CppObject *, Il2CppObject *, Il2CppObject *, Il2CppObject *, Il2CppObject *, const MethodInfo*))ZipLatestFunc_7_Invoke_m3731469579_gshared)(__this, ___arg10, ___arg21, ___arg32, ___arg43, ___arg54, ___arg65, method)
// System.IAsyncResult UniRx.Operators.ZipLatestFunc`7<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>::BeginInvoke(T1,T2,T3,T4,T5,T6,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * ZipLatestFunc_7_BeginInvoke_m578513205_gshared (ZipLatestFunc_7_t1858605825 * __this, Il2CppObject * ___arg10, Il2CppObject * ___arg21, Il2CppObject * ___arg32, Il2CppObject * ___arg43, Il2CppObject * ___arg54, Il2CppObject * ___arg65, AsyncCallback_t1363551830 * ___callback6, Il2CppObject * ___object7, const MethodInfo* method);
#define ZipLatestFunc_7_BeginInvoke_m578513205(__this, ___arg10, ___arg21, ___arg32, ___arg43, ___arg54, ___arg65, ___callback6, ___object7, method) ((  Il2CppObject * (*) (ZipLatestFunc_7_t1858605825 *, Il2CppObject *, Il2CppObject *, Il2CppObject *, Il2CppObject *, Il2CppObject *, Il2CppObject *, AsyncCallback_t1363551830 *, Il2CppObject *, const MethodInfo*))ZipLatestFunc_7_BeginInvoke_m578513205_gshared)(__this, ___arg10, ___arg21, ___arg32, ___arg43, ___arg54, ___arg65, ___callback6, ___object7, method)
// TR UniRx.Operators.ZipLatestFunc`7<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>::EndInvoke(System.IAsyncResult)
extern "C"  Il2CppObject * ZipLatestFunc_7_EndInvoke_m1815241582_gshared (ZipLatestFunc_7_t1858605825 * __this, Il2CppObject * ___result0, const MethodInfo* method);
#define ZipLatestFunc_7_EndInvoke_m1815241582(__this, ___result0, method) ((  Il2CppObject * (*) (ZipLatestFunc_7_t1858605825 *, Il2CppObject *, const MethodInfo*))ZipLatestFunc_7_EndInvoke_m1815241582_gshared)(__this, ___result0, method)
