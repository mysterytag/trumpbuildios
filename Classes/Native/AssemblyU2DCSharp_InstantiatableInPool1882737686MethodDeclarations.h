﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// InstantiatableInPool
struct InstantiatableInPool_t1882737686;

#include "codegen/il2cpp-codegen.h"

// System.Void InstantiatableInPool::.ctor()
extern "C"  void InstantiatableInPool__ctor_m836060997 (InstantiatableInPool_t1882737686 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void InstantiatableInPool::Recycle()
extern "C"  void InstantiatableInPool_Recycle_m1209365942 (InstantiatableInPool_t1882737686 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void InstantiatableInPool::RecycleWithDelay(System.Single)
extern "C"  void InstantiatableInPool_RecycleWithDelay_m3914287970 (InstantiatableInPool_t1882737686 * __this, float ___secs0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
