﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SponsorPay.AbstractResponse
struct AbstractResponse_t2918001245;

#include "codegen/il2cpp-codegen.h"

// System.Void SponsorPay.AbstractResponse::.ctor()
extern "C"  void AbstractResponse__ctor_m828474760 (AbstractResponse_t2918001245 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean SponsorPay.AbstractResponse::get_success()
extern "C"  bool AbstractResponse_get_success_m1694079572 (AbstractResponse_t2918001245 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SponsorPay.AbstractResponse::set_success(System.Boolean)
extern "C"  void AbstractResponse_set_success_m405812003 (AbstractResponse_t2918001245 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
