﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Reactor`1/Subscription<System.Object>
struct Subscription_t2911320242;

#include "codegen/il2cpp-codegen.h"

// System.Void Reactor`1/Subscription<System.Object>::.ctor()
extern "C"  void Subscription__ctor_m4053049736_gshared (Subscription_t2911320242 * __this, const MethodInfo* method);
#define Subscription__ctor_m4053049736(__this, method) ((  void (*) (Subscription_t2911320242 *, const MethodInfo*))Subscription__ctor_m4053049736_gshared)(__this, method)
// System.Void Reactor`1/Subscription<System.Object>::Dispose()
extern "C"  void Subscription_Dispose_m3638709189_gshared (Subscription_t2911320242 * __this, const MethodInfo* method);
#define Subscription_Dispose_m3638709189(__this, method) ((  void (*) (Subscription_t2911320242 *, const MethodInfo*))Subscription_Dispose_m3638709189_gshared)(__this, method)
