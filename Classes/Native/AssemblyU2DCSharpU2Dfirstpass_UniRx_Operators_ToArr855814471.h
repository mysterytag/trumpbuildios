﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Generic.List`1<System.Object>
struct List_1_t1634065389;

#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Opera362185502.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Operators.ToArrayObservable`1/ToArray<System.Object>
struct  ToArray_t855814471  : public OperatorObserverBase_2_t362185502
{
public:
	// System.Collections.Generic.List`1<TSource> UniRx.Operators.ToArrayObservable`1/ToArray::list
	List_1_t1634065389 * ___list_2;

public:
	inline static int32_t get_offset_of_list_2() { return static_cast<int32_t>(offsetof(ToArray_t855814471, ___list_2)); }
	inline List_1_t1634065389 * get_list_2() const { return ___list_2; }
	inline List_1_t1634065389 ** get_address_of_list_2() { return &___list_2; }
	inline void set_list_2(List_1_t1634065389 * value)
	{
		___list_2 = value;
		Il2CppCodeGenWriteBarrier(&___list_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
