﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.WhenAllObservable`1/WhenAll_<UniRx.Tuple`2<System.Object,System.Object>>
struct WhenAll__t2383258904;
// System.Collections.Generic.IList`1<UniRx.IObservable`1<UniRx.Tuple`2<System.Object,System.Object>>>
struct IList_1_t2294552497;
// UniRx.IObserver`1<UniRx.Tuple`2<System.Object,System.Object>[]>
struct IObserver_1_t1814965617;
// System.IDisposable
struct IDisposable_t1628921374;
// UniRx.Tuple`2<System.Object,System.Object>[]
struct Tuple_2U5BU5D_t3897934010;
// System.Exception
struct Exception_t1967233988;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Exception1967233988.h"

// System.Void UniRx.Operators.WhenAllObservable`1/WhenAll_<UniRx.Tuple`2<System.Object,System.Object>>::.ctor(System.Collections.Generic.IList`1<UniRx.IObservable`1<T>>,UniRx.IObserver`1<T[]>,System.IDisposable)
extern "C"  void WhenAll___ctor_m255663946_gshared (WhenAll__t2383258904 * __this, Il2CppObject* ___sources0, Il2CppObject* ___observer1, Il2CppObject * ___cancel2, const MethodInfo* method);
#define WhenAll___ctor_m255663946(__this, ___sources0, ___observer1, ___cancel2, method) ((  void (*) (WhenAll__t2383258904 *, Il2CppObject*, Il2CppObject*, Il2CppObject *, const MethodInfo*))WhenAll___ctor_m255663946_gshared)(__this, ___sources0, ___observer1, ___cancel2, method)
// System.IDisposable UniRx.Operators.WhenAllObservable`1/WhenAll_<UniRx.Tuple`2<System.Object,System.Object>>::Run()
extern "C"  Il2CppObject * WhenAll__Run_m3918839104_gshared (WhenAll__t2383258904 * __this, const MethodInfo* method);
#define WhenAll__Run_m3918839104(__this, method) ((  Il2CppObject * (*) (WhenAll__t2383258904 *, const MethodInfo*))WhenAll__Run_m3918839104_gshared)(__this, method)
// System.Void UniRx.Operators.WhenAllObservable`1/WhenAll_<UniRx.Tuple`2<System.Object,System.Object>>::OnNext(T[])
extern "C"  void WhenAll__OnNext_m3149975032_gshared (WhenAll__t2383258904 * __this, Tuple_2U5BU5D_t3897934010* ___value0, const MethodInfo* method);
#define WhenAll__OnNext_m3149975032(__this, ___value0, method) ((  void (*) (WhenAll__t2383258904 *, Tuple_2U5BU5D_t3897934010*, const MethodInfo*))WhenAll__OnNext_m3149975032_gshared)(__this, ___value0, method)
// System.Void UniRx.Operators.WhenAllObservable`1/WhenAll_<UniRx.Tuple`2<System.Object,System.Object>>::OnError(System.Exception)
extern "C"  void WhenAll__OnError_m3227130985_gshared (WhenAll__t2383258904 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method);
#define WhenAll__OnError_m3227130985(__this, ___error0, method) ((  void (*) (WhenAll__t2383258904 *, Exception_t1967233988 *, const MethodInfo*))WhenAll__OnError_m3227130985_gshared)(__this, ___error0, method)
// System.Void UniRx.Operators.WhenAllObservable`1/WhenAll_<UniRx.Tuple`2<System.Object,System.Object>>::OnCompleted()
extern "C"  void WhenAll__OnCompleted_m552166716_gshared (WhenAll__t2383258904 * __this, const MethodInfo* method);
#define WhenAll__OnCompleted_m552166716(__this, method) ((  void (*) (WhenAll__t2383258904 *, const MethodInfo*))WhenAll__OnCompleted_m552166716_gshared)(__this, method)
