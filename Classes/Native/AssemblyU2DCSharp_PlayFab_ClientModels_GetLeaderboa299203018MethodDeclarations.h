﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.ClientModels.GetLeaderboardAroundPlayerRequest
struct GetLeaderboardAroundPlayerRequest_t299203018;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String968488902.h"
#include "mscorlib_System_Nullable_1_gen1438485399.h"

// System.Void PlayFab.ClientModels.GetLeaderboardAroundPlayerRequest::.ctor()
extern "C"  void GetLeaderboardAroundPlayerRequest__ctor_m861325215 (GetLeaderboardAroundPlayerRequest_t299203018 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PlayFab.ClientModels.GetLeaderboardAroundPlayerRequest::get_PlayFabId()
extern "C"  String_t* GetLeaderboardAroundPlayerRequest_get_PlayFabId_m475209215 (GetLeaderboardAroundPlayerRequest_t299203018 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.GetLeaderboardAroundPlayerRequest::set_PlayFabId(System.String)
extern "C"  void GetLeaderboardAroundPlayerRequest_set_PlayFabId_m1145445684 (GetLeaderboardAroundPlayerRequest_t299203018 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PlayFab.ClientModels.GetLeaderboardAroundPlayerRequest::get_StatisticName()
extern "C"  String_t* GetLeaderboardAroundPlayerRequest_get_StatisticName_m1479295756 (GetLeaderboardAroundPlayerRequest_t299203018 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.GetLeaderboardAroundPlayerRequest::set_StatisticName(System.String)
extern "C"  void GetLeaderboardAroundPlayerRequest_set_StatisticName_m2384135367 (GetLeaderboardAroundPlayerRequest_t299203018 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Nullable`1<System.Int32> PlayFab.ClientModels.GetLeaderboardAroundPlayerRequest::get_MaxResultsCount()
extern "C"  Nullable_1_t1438485399  GetLeaderboardAroundPlayerRequest_get_MaxResultsCount_m1879864706 (GetLeaderboardAroundPlayerRequest_t299203018 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.GetLeaderboardAroundPlayerRequest::set_MaxResultsCount(System.Nullable`1<System.Int32>)
extern "C"  void GetLeaderboardAroundPlayerRequest_set_MaxResultsCount_m1547665041 (GetLeaderboardAroundPlayerRequest_t299203018 * __this, Nullable_1_t1438485399  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
