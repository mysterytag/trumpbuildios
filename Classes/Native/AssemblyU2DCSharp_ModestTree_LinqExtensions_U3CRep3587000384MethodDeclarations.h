﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ModestTree.LinqExtensions/<ReplaceOrAppend>c__Iterator39`1<System.Object>
struct U3CReplaceOrAppendU3Ec__Iterator39_1_t3587000384;
// System.Object
struct Il2CppObject;
// System.Collections.IEnumerator
struct IEnumerator_t287207039;
// System.Collections.Generic.IEnumerator`1<System.Object>
struct IEnumerator_1_t2320212868;

#include "codegen/il2cpp-codegen.h"

// System.Void ModestTree.LinqExtensions/<ReplaceOrAppend>c__Iterator39`1<System.Object>::.ctor()
extern "C"  void U3CReplaceOrAppendU3Ec__Iterator39_1__ctor_m559286845_gshared (U3CReplaceOrAppendU3Ec__Iterator39_1_t3587000384 * __this, const MethodInfo* method);
#define U3CReplaceOrAppendU3Ec__Iterator39_1__ctor_m559286845(__this, method) ((  void (*) (U3CReplaceOrAppendU3Ec__Iterator39_1_t3587000384 *, const MethodInfo*))U3CReplaceOrAppendU3Ec__Iterator39_1__ctor_m559286845_gshared)(__this, method)
// T ModestTree.LinqExtensions/<ReplaceOrAppend>c__Iterator39`1<System.Object>::System.Collections.Generic.IEnumerator<T>.get_Current()
extern "C"  Il2CppObject * U3CReplaceOrAppendU3Ec__Iterator39_1_System_Collections_Generic_IEnumeratorU3CTU3E_get_Current_m1668921988_gshared (U3CReplaceOrAppendU3Ec__Iterator39_1_t3587000384 * __this, const MethodInfo* method);
#define U3CReplaceOrAppendU3Ec__Iterator39_1_System_Collections_Generic_IEnumeratorU3CTU3E_get_Current_m1668921988(__this, method) ((  Il2CppObject * (*) (U3CReplaceOrAppendU3Ec__Iterator39_1_t3587000384 *, const MethodInfo*))U3CReplaceOrAppendU3Ec__Iterator39_1_System_Collections_Generic_IEnumeratorU3CTU3E_get_Current_m1668921988_gshared)(__this, method)
// System.Object ModestTree.LinqExtensions/<ReplaceOrAppend>c__Iterator39`1<System.Object>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CReplaceOrAppendU3Ec__Iterator39_1_System_Collections_IEnumerator_get_Current_m1694264585_gshared (U3CReplaceOrAppendU3Ec__Iterator39_1_t3587000384 * __this, const MethodInfo* method);
#define U3CReplaceOrAppendU3Ec__Iterator39_1_System_Collections_IEnumerator_get_Current_m1694264585(__this, method) ((  Il2CppObject * (*) (U3CReplaceOrAppendU3Ec__Iterator39_1_t3587000384 *, const MethodInfo*))U3CReplaceOrAppendU3Ec__Iterator39_1_System_Collections_IEnumerator_get_Current_m1694264585_gshared)(__this, method)
// System.Collections.IEnumerator ModestTree.LinqExtensions/<ReplaceOrAppend>c__Iterator39`1<System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * U3CReplaceOrAppendU3Ec__Iterator39_1_System_Collections_IEnumerable_GetEnumerator_m1351958660_gshared (U3CReplaceOrAppendU3Ec__Iterator39_1_t3587000384 * __this, const MethodInfo* method);
#define U3CReplaceOrAppendU3Ec__Iterator39_1_System_Collections_IEnumerable_GetEnumerator_m1351958660(__this, method) ((  Il2CppObject * (*) (U3CReplaceOrAppendU3Ec__Iterator39_1_t3587000384 *, const MethodInfo*))U3CReplaceOrAppendU3Ec__Iterator39_1_System_Collections_IEnumerable_GetEnumerator_m1351958660_gshared)(__this, method)
// System.Collections.Generic.IEnumerator`1<T> ModestTree.LinqExtensions/<ReplaceOrAppend>c__Iterator39`1<System.Object>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
extern "C"  Il2CppObject* U3CReplaceOrAppendU3Ec__Iterator39_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m3452241927_gshared (U3CReplaceOrAppendU3Ec__Iterator39_1_t3587000384 * __this, const MethodInfo* method);
#define U3CReplaceOrAppendU3Ec__Iterator39_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m3452241927(__this, method) ((  Il2CppObject* (*) (U3CReplaceOrAppendU3Ec__Iterator39_1_t3587000384 *, const MethodInfo*))U3CReplaceOrAppendU3Ec__Iterator39_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m3452241927_gshared)(__this, method)
// System.Boolean ModestTree.LinqExtensions/<ReplaceOrAppend>c__Iterator39`1<System.Object>::MoveNext()
extern "C"  bool U3CReplaceOrAppendU3Ec__Iterator39_1_MoveNext_m2932068247_gshared (U3CReplaceOrAppendU3Ec__Iterator39_1_t3587000384 * __this, const MethodInfo* method);
#define U3CReplaceOrAppendU3Ec__Iterator39_1_MoveNext_m2932068247(__this, method) ((  bool (*) (U3CReplaceOrAppendU3Ec__Iterator39_1_t3587000384 *, const MethodInfo*))U3CReplaceOrAppendU3Ec__Iterator39_1_MoveNext_m2932068247_gshared)(__this, method)
// System.Void ModestTree.LinqExtensions/<ReplaceOrAppend>c__Iterator39`1<System.Object>::Dispose()
extern "C"  void U3CReplaceOrAppendU3Ec__Iterator39_1_Dispose_m502029114_gshared (U3CReplaceOrAppendU3Ec__Iterator39_1_t3587000384 * __this, const MethodInfo* method);
#define U3CReplaceOrAppendU3Ec__Iterator39_1_Dispose_m502029114(__this, method) ((  void (*) (U3CReplaceOrAppendU3Ec__Iterator39_1_t3587000384 *, const MethodInfo*))U3CReplaceOrAppendU3Ec__Iterator39_1_Dispose_m502029114_gshared)(__this, method)
// System.Void ModestTree.LinqExtensions/<ReplaceOrAppend>c__Iterator39`1<System.Object>::Reset()
extern "C"  void U3CReplaceOrAppendU3Ec__Iterator39_1_Reset_m2500687082_gshared (U3CReplaceOrAppendU3Ec__Iterator39_1_t3587000384 * __this, const MethodInfo* method);
#define U3CReplaceOrAppendU3Ec__Iterator39_1_Reset_m2500687082(__this, method) ((  void (*) (U3CReplaceOrAppendU3Ec__Iterator39_1_t3587000384 *, const MethodInfo*))U3CReplaceOrAppendU3Ec__Iterator39_1_Reset_m2500687082_gshared)(__this, method)
