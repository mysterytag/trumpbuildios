﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_Nullable_1_gen3097043249.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PlayFab.ClientModels.AndroidDevicePushNotificationRegistrationRequest
struct  AndroidDevicePushNotificationRegistrationRequest_t28777084  : public Il2CppObject
{
public:
	// System.String PlayFab.ClientModels.AndroidDevicePushNotificationRegistrationRequest::<DeviceToken>k__BackingField
	String_t* ___U3CDeviceTokenU3Ek__BackingField_0;
	// System.Nullable`1<System.Boolean> PlayFab.ClientModels.AndroidDevicePushNotificationRegistrationRequest::<SendPushNotificationConfirmation>k__BackingField
	Nullable_1_t3097043249  ___U3CSendPushNotificationConfirmationU3Ek__BackingField_1;
	// System.String PlayFab.ClientModels.AndroidDevicePushNotificationRegistrationRequest::<ConfirmationMessege>k__BackingField
	String_t* ___U3CConfirmationMessegeU3Ek__BackingField_2;

public:
	inline static int32_t get_offset_of_U3CDeviceTokenU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(AndroidDevicePushNotificationRegistrationRequest_t28777084, ___U3CDeviceTokenU3Ek__BackingField_0)); }
	inline String_t* get_U3CDeviceTokenU3Ek__BackingField_0() const { return ___U3CDeviceTokenU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CDeviceTokenU3Ek__BackingField_0() { return &___U3CDeviceTokenU3Ek__BackingField_0; }
	inline void set_U3CDeviceTokenU3Ek__BackingField_0(String_t* value)
	{
		___U3CDeviceTokenU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier(&___U3CDeviceTokenU3Ek__BackingField_0, value);
	}

	inline static int32_t get_offset_of_U3CSendPushNotificationConfirmationU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(AndroidDevicePushNotificationRegistrationRequest_t28777084, ___U3CSendPushNotificationConfirmationU3Ek__BackingField_1)); }
	inline Nullable_1_t3097043249  get_U3CSendPushNotificationConfirmationU3Ek__BackingField_1() const { return ___U3CSendPushNotificationConfirmationU3Ek__BackingField_1; }
	inline Nullable_1_t3097043249 * get_address_of_U3CSendPushNotificationConfirmationU3Ek__BackingField_1() { return &___U3CSendPushNotificationConfirmationU3Ek__BackingField_1; }
	inline void set_U3CSendPushNotificationConfirmationU3Ek__BackingField_1(Nullable_1_t3097043249  value)
	{
		___U3CSendPushNotificationConfirmationU3Ek__BackingField_1 = value;
	}

	inline static int32_t get_offset_of_U3CConfirmationMessegeU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(AndroidDevicePushNotificationRegistrationRequest_t28777084, ___U3CConfirmationMessegeU3Ek__BackingField_2)); }
	inline String_t* get_U3CConfirmationMessegeU3Ek__BackingField_2() const { return ___U3CConfirmationMessegeU3Ek__BackingField_2; }
	inline String_t** get_address_of_U3CConfirmationMessegeU3Ek__BackingField_2() { return &___U3CConfirmationMessegeU3Ek__BackingField_2; }
	inline void set_U3CConfirmationMessegeU3Ek__BackingField_2(String_t* value)
	{
		___U3CConfirmationMessegeU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier(&___U3CConfirmationMessegeU3Ek__BackingField_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
