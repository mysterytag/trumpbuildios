﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Func`2<System.Object,System.String>
struct Func_2_t2267165834;
// UnityEngine.UI.Text
struct Text_t3286458198;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.UnityUIComponentExtensions/<SubscribeToText>c__AnonStoreyA5`1<System.Object>
struct  U3CSubscribeToTextU3Ec__AnonStoreyA5_1_t3655335453  : public Il2CppObject
{
public:
	// System.Func`2<T,System.String> UniRx.UnityUIComponentExtensions/<SubscribeToText>c__AnonStoreyA5`1::selector
	Func_2_t2267165834 * ___selector_0;
	// UnityEngine.UI.Text UniRx.UnityUIComponentExtensions/<SubscribeToText>c__AnonStoreyA5`1::text
	Text_t3286458198 * ___text_1;

public:
	inline static int32_t get_offset_of_selector_0() { return static_cast<int32_t>(offsetof(U3CSubscribeToTextU3Ec__AnonStoreyA5_1_t3655335453, ___selector_0)); }
	inline Func_2_t2267165834 * get_selector_0() const { return ___selector_0; }
	inline Func_2_t2267165834 ** get_address_of_selector_0() { return &___selector_0; }
	inline void set_selector_0(Func_2_t2267165834 * value)
	{
		___selector_0 = value;
		Il2CppCodeGenWriteBarrier(&___selector_0, value);
	}

	inline static int32_t get_offset_of_text_1() { return static_cast<int32_t>(offsetof(U3CSubscribeToTextU3Ec__AnonStoreyA5_1_t3655335453, ___text_1)); }
	inline Text_t3286458198 * get_text_1() const { return ___text_1; }
	inline Text_t3286458198 ** get_address_of_text_1() { return &___text_1; }
	inline void set_text_1(Text_t3286458198 * value)
	{
		___text_1 = value;
		Il2CppCodeGenWriteBarrier(&___text_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
