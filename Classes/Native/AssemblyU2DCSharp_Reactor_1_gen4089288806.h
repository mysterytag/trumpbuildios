﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Action`1<System.Double>
struct Action_1_t682969319;
// ZergRush.ImmutableList`1<System.Action`1<System.Double>>[]
struct ImmutableList_1U5BU5D_t1225238160;

#include "mscorlib_System_Object837106420.h"
#include "AssemblyU2DCSharp_Priority3194150340.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Reactor`1<System.Double>
struct  Reactor_1_t4089288806  : public Il2CppObject
{
public:
	// System.Action`1<T> Reactor`1::single
	Action_1_t682969319 * ___single_0;
	// Priority Reactor`1::singlePriority
	int32_t ___singlePriority_1;
	// ZergRush.ImmutableList`1<System.Action`1<T>>[] Reactor`1::multipriorityReactions
	ImmutableList_1U5BU5D_t1225238160* ___multipriorityReactions_2;

public:
	inline static int32_t get_offset_of_single_0() { return static_cast<int32_t>(offsetof(Reactor_1_t4089288806, ___single_0)); }
	inline Action_1_t682969319 * get_single_0() const { return ___single_0; }
	inline Action_1_t682969319 ** get_address_of_single_0() { return &___single_0; }
	inline void set_single_0(Action_1_t682969319 * value)
	{
		___single_0 = value;
		Il2CppCodeGenWriteBarrier(&___single_0, value);
	}

	inline static int32_t get_offset_of_singlePriority_1() { return static_cast<int32_t>(offsetof(Reactor_1_t4089288806, ___singlePriority_1)); }
	inline int32_t get_singlePriority_1() const { return ___singlePriority_1; }
	inline int32_t* get_address_of_singlePriority_1() { return &___singlePriority_1; }
	inline void set_singlePriority_1(int32_t value)
	{
		___singlePriority_1 = value;
	}

	inline static int32_t get_offset_of_multipriorityReactions_2() { return static_cast<int32_t>(offsetof(Reactor_1_t4089288806, ___multipriorityReactions_2)); }
	inline ImmutableList_1U5BU5D_t1225238160* get_multipriorityReactions_2() const { return ___multipriorityReactions_2; }
	inline ImmutableList_1U5BU5D_t1225238160** get_address_of_multipriorityReactions_2() { return &___multipriorityReactions_2; }
	inline void set_multipriorityReactions_2(ImmutableList_1U5BU5D_t1225238160* value)
	{
		___multipriorityReactions_2 = value;
		Il2CppCodeGenWriteBarrier(&___multipriorityReactions_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
