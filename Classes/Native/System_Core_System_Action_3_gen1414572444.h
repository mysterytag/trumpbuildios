﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// GooglePlayGames.BasicApi.Quests.IQuest
struct IQuest_t4055071484;
// GooglePlayGames.BasicApi.Quests.IQuestMilestone
struct IQuestMilestone_t1579525178;
// System.IAsyncResult
struct IAsyncResult_t537683269;
// System.AsyncCallback
struct AsyncCallback_t1363551830;
// System.Object
struct Il2CppObject;

#include "mscorlib_System_MulticastDelegate2585444626.h"
#include "mscorlib_System_Void2779279689.h"
#include "AssemblyU2DCSharp_GooglePlayGames_BasicApi_Quests_Q528418411.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Action`3<GooglePlayGames.BasicApi.Quests.QuestClaimMilestoneStatus,GooglePlayGames.BasicApi.Quests.IQuest,GooglePlayGames.BasicApi.Quests.IQuestMilestone>
struct  Action_3_t1414572444  : public MulticastDelegate_t2585444626
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
