﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_ValueType4014882752.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PlayFabNotificationPackage
struct  PlayFabNotificationPackage_t2037342920 
{
public:
	// System.String PlayFabNotificationPackage::Sound
	String_t* ___Sound_0;
	// System.String PlayFabNotificationPackage::Title
	String_t* ___Title_1;
	// System.String PlayFabNotificationPackage::Icon
	String_t* ___Icon_2;
	// System.String PlayFabNotificationPackage::Message
	String_t* ___Message_3;
	// System.String PlayFabNotificationPackage::CustomData
	String_t* ___CustomData_4;

public:
	inline static int32_t get_offset_of_Sound_0() { return static_cast<int32_t>(offsetof(PlayFabNotificationPackage_t2037342920, ___Sound_0)); }
	inline String_t* get_Sound_0() const { return ___Sound_0; }
	inline String_t** get_address_of_Sound_0() { return &___Sound_0; }
	inline void set_Sound_0(String_t* value)
	{
		___Sound_0 = value;
		Il2CppCodeGenWriteBarrier(&___Sound_0, value);
	}

	inline static int32_t get_offset_of_Title_1() { return static_cast<int32_t>(offsetof(PlayFabNotificationPackage_t2037342920, ___Title_1)); }
	inline String_t* get_Title_1() const { return ___Title_1; }
	inline String_t** get_address_of_Title_1() { return &___Title_1; }
	inline void set_Title_1(String_t* value)
	{
		___Title_1 = value;
		Il2CppCodeGenWriteBarrier(&___Title_1, value);
	}

	inline static int32_t get_offset_of_Icon_2() { return static_cast<int32_t>(offsetof(PlayFabNotificationPackage_t2037342920, ___Icon_2)); }
	inline String_t* get_Icon_2() const { return ___Icon_2; }
	inline String_t** get_address_of_Icon_2() { return &___Icon_2; }
	inline void set_Icon_2(String_t* value)
	{
		___Icon_2 = value;
		Il2CppCodeGenWriteBarrier(&___Icon_2, value);
	}

	inline static int32_t get_offset_of_Message_3() { return static_cast<int32_t>(offsetof(PlayFabNotificationPackage_t2037342920, ___Message_3)); }
	inline String_t* get_Message_3() const { return ___Message_3; }
	inline String_t** get_address_of_Message_3() { return &___Message_3; }
	inline void set_Message_3(String_t* value)
	{
		___Message_3 = value;
		Il2CppCodeGenWriteBarrier(&___Message_3, value);
	}

	inline static int32_t get_offset_of_CustomData_4() { return static_cast<int32_t>(offsetof(PlayFabNotificationPackage_t2037342920, ___CustomData_4)); }
	inline String_t* get_CustomData_4() const { return ___CustomData_4; }
	inline String_t** get_address_of_CustomData_4() { return &___CustomData_4; }
	inline void set_CustomData_4(String_t* value)
	{
		___CustomData_4 = value;
		Il2CppCodeGenWriteBarrier(&___CustomData_4, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for marshalling of: PlayFabNotificationPackage
struct PlayFabNotificationPackage_t2037342920_marshaled_pinvoke
{
	char* ___Sound_0;
	char* ___Title_1;
	char* ___Icon_2;
	char* ___Message_3;
	char* ___CustomData_4;
};
// Native definition for marshalling of: PlayFabNotificationPackage
struct PlayFabNotificationPackage_t2037342920_marshaled_com
{
	uint16_t* ___Sound_0;
	uint16_t* ___Title_1;
	uint16_t* ___Icon_2;
	uint16_t* ___Message_3;
	uint16_t* ___CustomData_4;
};
