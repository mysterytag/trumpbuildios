﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.ClientModels.UnlinkPSNAccountResult
struct UnlinkPSNAccountResult_t2391313410;

#include "codegen/il2cpp-codegen.h"

// System.Void PlayFab.ClientModels.UnlinkPSNAccountResult::.ctor()
extern "C"  void UnlinkPSNAccountResult__ctor_m3908329211 (UnlinkPSNAccountResult_t2391313410 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
