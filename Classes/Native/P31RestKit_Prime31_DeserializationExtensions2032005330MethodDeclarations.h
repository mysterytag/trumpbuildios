﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2<System.String,System.Object>
struct Dictionary_2_t2474804324;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"

// System.Collections.Generic.Dictionary`2<System.String,System.Object> Prime31.DeserializationExtensions::toDictionary(System.Object)
extern "C"  Dictionary_2_t2474804324 * DeserializationExtensions_toDictionary_m3409470010 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___self0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
