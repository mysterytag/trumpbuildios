﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.ClientModels.GetPhotonAuthenticationTokenResult
struct GetPhotonAuthenticationTokenResult_t3031300796;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String968488902.h"

// System.Void PlayFab.ClientModels.GetPhotonAuthenticationTokenResult::.ctor()
extern "C"  void GetPhotonAuthenticationTokenResult__ctor_m2806837889 (GetPhotonAuthenticationTokenResult_t3031300796 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PlayFab.ClientModels.GetPhotonAuthenticationTokenResult::get_PhotonCustomAuthenticationToken()
extern "C"  String_t* GetPhotonAuthenticationTokenResult_get_PhotonCustomAuthenticationToken_m181991533 (GetPhotonAuthenticationTokenResult_t3031300796 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.GetPhotonAuthenticationTokenResult::set_PhotonCustomAuthenticationToken(System.String)
extern "C"  void GetPhotonAuthenticationTokenResult_set_PhotonCustomAuthenticationToken_m1124090668 (GetPhotonAuthenticationTokenResult_t3031300796 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
