﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.ObservableYieldInstruction`1<System.Object>
struct ObservableYieldInstruction_1_t2959019304;
// UniRx.IObservable`1<System.Object>
struct IObservable_1_t595904784;
// System.Object
struct Il2CppObject;
// System.Exception
struct Exception_t1967233988;

#include "codegen/il2cpp-codegen.h"

// System.Void UniRx.ObservableYieldInstruction`1<System.Object>::.ctor(UniRx.IObservable`1<T>,System.Boolean)
extern "C"  void ObservableYieldInstruction_1__ctor_m1999879574_gshared (ObservableYieldInstruction_1_t2959019304 * __this, Il2CppObject* ___source0, bool ___reThrowOnError1, const MethodInfo* method);
#define ObservableYieldInstruction_1__ctor_m1999879574(__this, ___source0, ___reThrowOnError1, method) ((  void (*) (ObservableYieldInstruction_1_t2959019304 *, Il2CppObject*, bool, const MethodInfo*))ObservableYieldInstruction_1__ctor_m1999879574_gshared)(__this, ___source0, ___reThrowOnError1, method)
// T UniRx.ObservableYieldInstruction`1<System.Object>::System.Collections.Generic.IEnumerator<T>.get_Current()
extern "C"  Il2CppObject * ObservableYieldInstruction_1_System_Collections_Generic_IEnumeratorU3CTU3E_get_Current_m1250220399_gshared (ObservableYieldInstruction_1_t2959019304 * __this, const MethodInfo* method);
#define ObservableYieldInstruction_1_System_Collections_Generic_IEnumeratorU3CTU3E_get_Current_m1250220399(__this, method) ((  Il2CppObject * (*) (ObservableYieldInstruction_1_t2959019304 *, const MethodInfo*))ObservableYieldInstruction_1_System_Collections_Generic_IEnumeratorU3CTU3E_get_Current_m1250220399_gshared)(__this, method)
// System.Object UniRx.ObservableYieldInstruction`1<System.Object>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * ObservableYieldInstruction_1_System_Collections_IEnumerator_get_Current_m1496646398_gshared (ObservableYieldInstruction_1_t2959019304 * __this, const MethodInfo* method);
#define ObservableYieldInstruction_1_System_Collections_IEnumerator_get_Current_m1496646398(__this, method) ((  Il2CppObject * (*) (ObservableYieldInstruction_1_t2959019304 *, const MethodInfo*))ObservableYieldInstruction_1_System_Collections_IEnumerator_get_Current_m1496646398_gshared)(__this, method)
// System.Boolean UniRx.ObservableYieldInstruction`1<System.Object>::System.Collections.IEnumerator.MoveNext()
extern "C"  bool ObservableYieldInstruction_1_System_Collections_IEnumerator_MoveNext_m539323519_gshared (ObservableYieldInstruction_1_t2959019304 * __this, const MethodInfo* method);
#define ObservableYieldInstruction_1_System_Collections_IEnumerator_MoveNext_m539323519(__this, method) ((  bool (*) (ObservableYieldInstruction_1_t2959019304 *, const MethodInfo*))ObservableYieldInstruction_1_System_Collections_IEnumerator_MoveNext_m539323519_gshared)(__this, method)
// System.Void UniRx.ObservableYieldInstruction`1<System.Object>::System.Collections.IEnumerator.Reset()
extern "C"  void ObservableYieldInstruction_1_System_Collections_IEnumerator_Reset_m3085590738_gshared (ObservableYieldInstruction_1_t2959019304 * __this, const MethodInfo* method);
#define ObservableYieldInstruction_1_System_Collections_IEnumerator_Reset_m3085590738(__this, method) ((  void (*) (ObservableYieldInstruction_1_t2959019304 *, const MethodInfo*))ObservableYieldInstruction_1_System_Collections_IEnumerator_Reset_m3085590738_gshared)(__this, method)
// System.Boolean UniRx.ObservableYieldInstruction`1<System.Object>::get_HasError()
extern "C"  bool ObservableYieldInstruction_1_get_HasError_m685161623_gshared (ObservableYieldInstruction_1_t2959019304 * __this, const MethodInfo* method);
#define ObservableYieldInstruction_1_get_HasError_m685161623(__this, method) ((  bool (*) (ObservableYieldInstruction_1_t2959019304 *, const MethodInfo*))ObservableYieldInstruction_1_get_HasError_m685161623_gshared)(__this, method)
// System.Boolean UniRx.ObservableYieldInstruction`1<System.Object>::get_HasResult()
extern "C"  bool ObservableYieldInstruction_1_get_HasResult_m2328290320_gshared (ObservableYieldInstruction_1_t2959019304 * __this, const MethodInfo* method);
#define ObservableYieldInstruction_1_get_HasResult_m2328290320(__this, method) ((  bool (*) (ObservableYieldInstruction_1_t2959019304 *, const MethodInfo*))ObservableYieldInstruction_1_get_HasResult_m2328290320_gshared)(__this, method)
// T UniRx.ObservableYieldInstruction`1<System.Object>::get_Result()
extern "C"  Il2CppObject * ObservableYieldInstruction_1_get_Result_m751809059_gshared (ObservableYieldInstruction_1_t2959019304 * __this, const MethodInfo* method);
#define ObservableYieldInstruction_1_get_Result_m751809059(__this, method) ((  Il2CppObject * (*) (ObservableYieldInstruction_1_t2959019304 *, const MethodInfo*))ObservableYieldInstruction_1_get_Result_m751809059_gshared)(__this, method)
// System.Exception UniRx.ObservableYieldInstruction`1<System.Object>::get_Error()
extern "C"  Exception_t1967233988 * ObservableYieldInstruction_1_get_Error_m4242398074_gshared (ObservableYieldInstruction_1_t2959019304 * __this, const MethodInfo* method);
#define ObservableYieldInstruction_1_get_Error_m4242398074(__this, method) ((  Exception_t1967233988 * (*) (ObservableYieldInstruction_1_t2959019304 *, const MethodInfo*))ObservableYieldInstruction_1_get_Error_m4242398074_gshared)(__this, method)
// System.Void UniRx.ObservableYieldInstruction`1<System.Object>::Dispose()
extern "C"  void ObservableYieldInstruction_1_Dispose_m974516581_gshared (ObservableYieldInstruction_1_t2959019304 * __this, const MethodInfo* method);
#define ObservableYieldInstruction_1_Dispose_m974516581(__this, method) ((  void (*) (ObservableYieldInstruction_1_t2959019304 *, const MethodInfo*))ObservableYieldInstruction_1_Dispose_m974516581_gshared)(__this, method)
