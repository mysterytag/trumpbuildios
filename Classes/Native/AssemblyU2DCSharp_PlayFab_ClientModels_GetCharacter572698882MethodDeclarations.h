﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.ClientModels.GetCharacterDataRequest
struct GetCharacterDataRequest_t572698882;
// System.String
struct String_t;
// System.Collections.Generic.List`1<System.String>
struct List_1_t1765447871;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String968488902.h"
#include "mscorlib_System_Nullable_1_gen1438485399.h"

// System.Void PlayFab.ClientModels.GetCharacterDataRequest::.ctor()
extern "C"  void GetCharacterDataRequest__ctor_m4251161319 (GetCharacterDataRequest_t572698882 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PlayFab.ClientModels.GetCharacterDataRequest::get_PlayFabId()
extern "C"  String_t* GetCharacterDataRequest_get_PlayFabId_m3177589127 (GetCharacterDataRequest_t572698882 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.GetCharacterDataRequest::set_PlayFabId(System.String)
extern "C"  void GetCharacterDataRequest_set_PlayFabId_m1515368172 (GetCharacterDataRequest_t572698882 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PlayFab.ClientModels.GetCharacterDataRequest::get_CharacterId()
extern "C"  String_t* GetCharacterDataRequest_get_CharacterId_m1859713149 (GetCharacterDataRequest_t572698882 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.GetCharacterDataRequest::set_CharacterId(System.String)
extern "C"  void GetCharacterDataRequest_set_CharacterId_m1556422518 (GetCharacterDataRequest_t572698882 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<System.String> PlayFab.ClientModels.GetCharacterDataRequest::get_Keys()
extern "C"  List_1_t1765447871 * GetCharacterDataRequest_get_Keys_m2026630251 (GetCharacterDataRequest_t572698882 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.GetCharacterDataRequest::set_Keys(System.Collections.Generic.List`1<System.String>)
extern "C"  void GetCharacterDataRequest_set_Keys_m1758150434 (GetCharacterDataRequest_t572698882 * __this, List_1_t1765447871 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Nullable`1<System.Int32> PlayFab.ClientModels.GetCharacterDataRequest::get_IfChangedFromDataVersion()
extern "C"  Nullable_1_t1438485399  GetCharacterDataRequest_get_IfChangedFromDataVersion_m2303425314 (GetCharacterDataRequest_t572698882 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.GetCharacterDataRequest::set_IfChangedFromDataVersion(System.Nullable`1<System.Int32>)
extern "C"  void GetCharacterDataRequest_set_IfChangedFromDataVersion_m3310937415 (GetCharacterDataRequest_t572698882 * __this, Nullable_1_t1438485399  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
