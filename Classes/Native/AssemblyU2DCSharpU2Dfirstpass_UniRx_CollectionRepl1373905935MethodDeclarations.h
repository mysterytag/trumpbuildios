﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_CollectionRepl1373905935.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Tuple_2_gen369261819.h"

// System.Void UniRx.CollectionReplaceEvent`1<UniRx.Tuple`2<System.Object,System.Object>>::.ctor(System.Int32,T,T)
extern "C"  void CollectionReplaceEvent_1__ctor_m3073699305_gshared (CollectionReplaceEvent_1_t1373905935 * __this, int32_t ___index0, Tuple_2_t369261819  ___oldValue1, Tuple_2_t369261819  ___newValue2, const MethodInfo* method);
#define CollectionReplaceEvent_1__ctor_m3073699305(__this, ___index0, ___oldValue1, ___newValue2, method) ((  void (*) (CollectionReplaceEvent_1_t1373905935 *, int32_t, Tuple_2_t369261819 , Tuple_2_t369261819 , const MethodInfo*))CollectionReplaceEvent_1__ctor_m3073699305_gshared)(__this, ___index0, ___oldValue1, ___newValue2, method)
// System.Int32 UniRx.CollectionReplaceEvent`1<UniRx.Tuple`2<System.Object,System.Object>>::get_Index()
extern "C"  int32_t CollectionReplaceEvent_1_get_Index_m4036036665_gshared (CollectionReplaceEvent_1_t1373905935 * __this, const MethodInfo* method);
#define CollectionReplaceEvent_1_get_Index_m4036036665(__this, method) ((  int32_t (*) (CollectionReplaceEvent_1_t1373905935 *, const MethodInfo*))CollectionReplaceEvent_1_get_Index_m4036036665_gshared)(__this, method)
// System.Void UniRx.CollectionReplaceEvent`1<UniRx.Tuple`2<System.Object,System.Object>>::set_Index(System.Int32)
extern "C"  void CollectionReplaceEvent_1_set_Index_m1583837388_gshared (CollectionReplaceEvent_1_t1373905935 * __this, int32_t ___value0, const MethodInfo* method);
#define CollectionReplaceEvent_1_set_Index_m1583837388(__this, ___value0, method) ((  void (*) (CollectionReplaceEvent_1_t1373905935 *, int32_t, const MethodInfo*))CollectionReplaceEvent_1_set_Index_m1583837388_gshared)(__this, ___value0, method)
// T UniRx.CollectionReplaceEvent`1<UniRx.Tuple`2<System.Object,System.Object>>::get_OldValue()
extern "C"  Tuple_2_t369261819  CollectionReplaceEvent_1_get_OldValue_m2151637166_gshared (CollectionReplaceEvent_1_t1373905935 * __this, const MethodInfo* method);
#define CollectionReplaceEvent_1_get_OldValue_m2151637166(__this, method) ((  Tuple_2_t369261819  (*) (CollectionReplaceEvent_1_t1373905935 *, const MethodInfo*))CollectionReplaceEvent_1_get_OldValue_m2151637166_gshared)(__this, method)
// System.Void UniRx.CollectionReplaceEvent`1<UniRx.Tuple`2<System.Object,System.Object>>::set_OldValue(T)
extern "C"  void CollectionReplaceEvent_1_set_OldValue_m2601373531_gshared (CollectionReplaceEvent_1_t1373905935 * __this, Tuple_2_t369261819  ___value0, const MethodInfo* method);
#define CollectionReplaceEvent_1_set_OldValue_m2601373531(__this, ___value0, method) ((  void (*) (CollectionReplaceEvent_1_t1373905935 *, Tuple_2_t369261819 , const MethodInfo*))CollectionReplaceEvent_1_set_OldValue_m2601373531_gshared)(__this, ___value0, method)
// T UniRx.CollectionReplaceEvent`1<UniRx.Tuple`2<System.Object,System.Object>>::get_NewValue()
extern "C"  Tuple_2_t369261819  CollectionReplaceEvent_1_get_NewValue_m869088021_gshared (CollectionReplaceEvent_1_t1373905935 * __this, const MethodInfo* method);
#define CollectionReplaceEvent_1_get_NewValue_m869088021(__this, method) ((  Tuple_2_t369261819  (*) (CollectionReplaceEvent_1_t1373905935 *, const MethodInfo*))CollectionReplaceEvent_1_get_NewValue_m869088021_gshared)(__this, method)
// System.Void UniRx.CollectionReplaceEvent`1<UniRx.Tuple`2<System.Object,System.Object>>::set_NewValue(T)
extern "C"  void CollectionReplaceEvent_1_set_NewValue_m1497055700_gshared (CollectionReplaceEvent_1_t1373905935 * __this, Tuple_2_t369261819  ___value0, const MethodInfo* method);
#define CollectionReplaceEvent_1_set_NewValue_m1497055700(__this, ___value0, method) ((  void (*) (CollectionReplaceEvent_1_t1373905935 *, Tuple_2_t369261819 , const MethodInfo*))CollectionReplaceEvent_1_set_NewValue_m1497055700_gshared)(__this, ___value0, method)
// System.String UniRx.CollectionReplaceEvent`1<UniRx.Tuple`2<System.Object,System.Object>>::ToString()
extern "C"  String_t* CollectionReplaceEvent_1_ToString_m4234752299_gshared (CollectionReplaceEvent_1_t1373905935 * __this, const MethodInfo* method);
#define CollectionReplaceEvent_1_ToString_m4234752299(__this, method) ((  String_t* (*) (CollectionReplaceEvent_1_t1373905935 *, const MethodInfo*))CollectionReplaceEvent_1_ToString_m4234752299_gshared)(__this, method)
// System.Int32 UniRx.CollectionReplaceEvent`1<UniRx.Tuple`2<System.Object,System.Object>>::GetHashCode()
extern "C"  int32_t CollectionReplaceEvent_1_GetHashCode_m3564612961_gshared (CollectionReplaceEvent_1_t1373905935 * __this, const MethodInfo* method);
#define CollectionReplaceEvent_1_GetHashCode_m3564612961(__this, method) ((  int32_t (*) (CollectionReplaceEvent_1_t1373905935 *, const MethodInfo*))CollectionReplaceEvent_1_GetHashCode_m3564612961_gshared)(__this, method)
// System.Boolean UniRx.CollectionReplaceEvent`1<UniRx.Tuple`2<System.Object,System.Object>>::Equals(UniRx.CollectionReplaceEvent`1<T>)
extern "C"  bool CollectionReplaceEvent_1_Equals_m2277216290_gshared (CollectionReplaceEvent_1_t1373905935 * __this, CollectionReplaceEvent_1_t1373905935  ___other0, const MethodInfo* method);
#define CollectionReplaceEvent_1_Equals_m2277216290(__this, ___other0, method) ((  bool (*) (CollectionReplaceEvent_1_t1373905935 *, CollectionReplaceEvent_1_t1373905935 , const MethodInfo*))CollectionReplaceEvent_1_Equals_m2277216290_gshared)(__this, ___other0, method)
