﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.ToListObservable`1/ToList<System.Object>
struct ToList_t1718783722;
// UniRx.IObserver`1<System.Collections.Generic.IList`1<System.Object>>
struct IObserver_1_t920630341;
// System.IDisposable
struct IDisposable_t1628921374;
// System.Object
struct Il2CppObject;
// System.Exception
struct Exception_t1967233988;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_Exception1967233988.h"

// System.Void UniRx.Operators.ToListObservable`1/ToList<System.Object>::.ctor(UniRx.IObserver`1<System.Collections.Generic.IList`1<TSource>>,System.IDisposable)
extern "C"  void ToList__ctor_m936525349_gshared (ToList_t1718783722 * __this, Il2CppObject* ___observer0, Il2CppObject * ___cancel1, const MethodInfo* method);
#define ToList__ctor_m936525349(__this, ___observer0, ___cancel1, method) ((  void (*) (ToList_t1718783722 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))ToList__ctor_m936525349_gshared)(__this, ___observer0, ___cancel1, method)
// System.Void UniRx.Operators.ToListObservable`1/ToList<System.Object>::OnNext(TSource)
extern "C"  void ToList_OnNext_m370234020_gshared (ToList_t1718783722 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define ToList_OnNext_m370234020(__this, ___value0, method) ((  void (*) (ToList_t1718783722 *, Il2CppObject *, const MethodInfo*))ToList_OnNext_m370234020_gshared)(__this, ___value0, method)
// System.Void UniRx.Operators.ToListObservable`1/ToList<System.Object>::OnError(System.Exception)
extern "C"  void ToList_OnError_m894309646_gshared (ToList_t1718783722 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method);
#define ToList_OnError_m894309646(__this, ___error0, method) ((  void (*) (ToList_t1718783722 *, Exception_t1967233988 *, const MethodInfo*))ToList_OnError_m894309646_gshared)(__this, ___error0, method)
// System.Void UniRx.Operators.ToListObservable`1/ToList<System.Object>::OnCompleted()
extern "C"  void ToList_OnCompleted_m1018785121_gshared (ToList_t1718783722 * __this, const MethodInfo* method);
#define ToList_OnCompleted_m1018785121(__this, method) ((  void (*) (ToList_t1718783722 *, const MethodInfo*))ToList_OnCompleted_m1018785121_gshared)(__this, method)
