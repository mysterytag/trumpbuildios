﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Zenject.DecoratorInstaller
struct DecoratorInstaller_t3984396578;
// Zenject.DiContainer
struct DiContainer_t2383114449;

#include "codegen/il2cpp-codegen.h"

// System.Void Zenject.DecoratorInstaller::.ctor()
extern "C"  void DecoratorInstaller__ctor_m4124692495 (DecoratorInstaller_t3984396578 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Zenject.DiContainer Zenject.DecoratorInstaller::get_Container()
extern "C"  DiContainer_t2383114449 * DecoratorInstaller_get_Container_m2093953519 (DecoratorInstaller_t3984396578 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Zenject.DecoratorInstaller::PreInstallBindings()
extern "C"  void DecoratorInstaller_PreInstallBindings_m242743035 (DecoratorInstaller_t3984396578 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Zenject.DecoratorInstaller::PostInstallBindings()
extern "C"  void DecoratorInstaller_PostInstallBindings_m3056084598 (DecoratorInstaller_t3984396578 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
