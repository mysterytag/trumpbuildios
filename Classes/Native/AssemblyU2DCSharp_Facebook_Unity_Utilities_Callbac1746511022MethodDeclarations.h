﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Facebook_Unity_Utilities_Callbac2504244479MethodDeclarations.h"

// System.Void Facebook.Unity.Utilities/Callback`1<Facebook.Unity.ResultContainer>::.ctor(System.Object,System.IntPtr)
#define Callback_1__ctor_m535427666(__this, ___object0, ___method1, method) ((  void (*) (Callback_1_t1746511022 *, Il2CppObject *, IntPtr_t, const MethodInfo*))Callback_1__ctor_m1218655815_gshared)(__this, ___object0, ___method1, method)
// System.Void Facebook.Unity.Utilities/Callback`1<Facebook.Unity.ResultContainer>::Invoke(T)
#define Callback_1_Invoke_m2477212082(__this, ___obj0, method) ((  void (*) (Callback_1_t1746511022 *, ResultContainer_t79372963 *, const MethodInfo*))Callback_1_Invoke_m4211213341_gshared)(__this, ___obj0, method)
// System.IAsyncResult Facebook.Unity.Utilities/Callback`1<Facebook.Unity.ResultContainer>::BeginInvoke(T,System.AsyncCallback,System.Object)
#define Callback_1_BeginInvoke_m3547593479(__this, ___obj0, ___callback1, ___object2, method) ((  Il2CppObject * (*) (Callback_1_t1746511022 *, ResultContainer_t79372963 *, AsyncCallback_t1363551830 *, Il2CppObject *, const MethodInfo*))Callback_1_BeginInvoke_m1576322282_gshared)(__this, ___obj0, ___callback1, ___object2, method)
// System.Void Facebook.Unity.Utilities/Callback`1<Facebook.Unity.ResultContainer>::EndInvoke(System.IAsyncResult)
#define Callback_1_EndInvoke_m3337315682(__this, ___result0, method) ((  void (*) (Callback_1_t1746511022 *, Il2CppObject *, const MethodInfo*))Callback_1_EndInvoke_m3978486743_gshared)(__this, ___result0, method)
