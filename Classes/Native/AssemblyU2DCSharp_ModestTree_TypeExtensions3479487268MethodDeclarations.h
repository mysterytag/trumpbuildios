﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Type
struct Type_t;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;
// System.Collections.Generic.IEnumerable`1<System.Type>
struct IEnumerable_1_t1356416995;
// System.Collections.Generic.IEnumerable`1<System.Reflection.FieldInfo>
struct IEnumerable_1_t4037084138;
// System.Collections.Generic.IEnumerable`1<System.Reflection.PropertyInfo>
struct IEnumerable_1_t67735429;
// System.Collections.Generic.IEnumerable`1<System.Reflection.MethodInfo>
struct IEnumerable_1_t2038408337;
// System.Reflection.ICustomAttributeProvider
struct ICustomAttributeProvider_t2334200065;
// System.Type[]
struct TypeU5BU5D_t3431720054;
// System.Collections.Generic.IEnumerable`1<System.Attribute>
struct IEnumerable_1_t3370848005;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Type2779229935.h"
#include "mscorlib_System_Reflection_BindingFlags2090192240.h"
#include "mscorlib_System_String968488902.h"

// System.Boolean ModestTree.TypeExtensions::DerivesFrom(System.Type,System.Type)
extern "C"  bool TypeExtensions_DerivesFrom_m1063347187 (Il2CppObject * __this /* static, unused */, Type_t * ___a0, Type_t * ___b1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ModestTree.TypeExtensions::DerivesFromOrEqual(System.Type,System.Type)
extern "C"  bool TypeExtensions_DerivesFromOrEqual_m3526502226 (Il2CppObject * __this /* static, unused */, Type_t * ___a0, Type_t * ___b1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object ModestTree.TypeExtensions::GetDefaultValue(System.Type)
extern "C"  Il2CppObject * TypeExtensions_GetDefaultValue_m1549006447 (Il2CppObject * __this /* static, unused */, Type_t * ___type0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String ModestTree.TypeExtensions::GetSimpleName(System.Type)
extern "C"  String_t* TypeExtensions_GetSimpleName_m3472407796 (Il2CppObject * __this /* static, unused */, Type_t * ___type0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.IEnumerable`1<System.Type> ModestTree.TypeExtensions::GetParentTypes(System.Type)
extern "C"  Il2CppObject* TypeExtensions_GetParentTypes_m3922980882 (Il2CppObject * __this /* static, unused */, Type_t * ___type0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String ModestTree.TypeExtensions::NameWithParents(System.Type)
extern "C"  String_t* TypeExtensions_NameWithParents_m1799611087 (Il2CppObject * __this /* static, unused */, Type_t * ___type0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ModestTree.TypeExtensions::IsClosedGenericType(System.Type)
extern "C"  bool TypeExtensions_IsClosedGenericType_m1919514171 (Il2CppObject * __this /* static, unused */, Type_t * ___type0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ModestTree.TypeExtensions::IsOpenGenericType(System.Type)
extern "C"  bool TypeExtensions_IsOpenGenericType_m3853458201 (Il2CppObject * __this /* static, unused */, Type_t * ___type0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.IEnumerable`1<System.Reflection.FieldInfo> ModestTree.TypeExtensions::GetAllFields(System.Type,System.Reflection.BindingFlags)
extern "C"  Il2CppObject* TypeExtensions_GetAllFields_m1768734574 (Il2CppObject * __this /* static, unused */, Type_t * ___type0, int32_t ___flags1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.IEnumerable`1<System.Reflection.PropertyInfo> ModestTree.TypeExtensions::GetAllProperties(System.Type,System.Reflection.BindingFlags)
extern "C"  Il2CppObject* TypeExtensions_GetAllProperties_m3364896073 (Il2CppObject * __this /* static, unused */, Type_t * ___type0, int32_t ___flags1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.IEnumerable`1<System.Reflection.MethodInfo> ModestTree.TypeExtensions::GetAllMethods(System.Type,System.Reflection.BindingFlags)
extern "C"  Il2CppObject* TypeExtensions_GetAllMethods_m2967502904 (Il2CppObject * __this /* static, unused */, Type_t * ___type0, int32_t ___flags1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String ModestTree.TypeExtensions::Name(System.Type)
extern "C"  String_t* TypeExtensions_Name_m3754378430 (Il2CppObject * __this /* static, unused */, Type_t * ___type0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String ModestTree.TypeExtensions::GetCSharpTypeName(System.String)
extern "C"  String_t* TypeExtensions_GetCSharpTypeName_m3989884270 (Il2CppObject * __this /* static, unused */, String_t* ___typeName0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ModestTree.TypeExtensions::HasAttribute(System.Reflection.ICustomAttributeProvider,System.Type[])
extern "C"  bool TypeExtensions_HasAttribute_m3951583835 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___provider0, TypeU5BU5D_t3431720054* ___attributeTypes1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.IEnumerable`1<System.Attribute> ModestTree.TypeExtensions::AllAttributes(System.Reflection.ICustomAttributeProvider,System.Type[])
extern "C"  Il2CppObject* TypeExtensions_AllAttributes_m2108720102 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___provider0, TypeU5BU5D_t3431720054* ___attributeTypes1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String ModestTree.TypeExtensions::<NameWithParents>m__289(System.Type)
extern "C"  String_t* TypeExtensions_U3CNameWithParentsU3Em__289_m740484287 (Il2CppObject * __this /* static, unused */, Type_t * ___x0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String ModestTree.TypeExtensions::<Name>m__28A(System.Type)
extern "C"  String_t* TypeExtensions_U3CNameU3Em__28A_m1906710308 (Il2CppObject * __this /* static, unused */, Type_t * ___t0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
