﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Zenject.PrefabFactory`2<System.Object,System.Object>
struct PrefabFactory_2_t3259451326;
// System.Object
struct Il2CppObject;
// UnityEngine.GameObject
struct GameObject_t4012695102;
// System.String
struct String_t;
// System.Collections.Generic.IEnumerable`1<Zenject.ZenjectResolveException>
struct IEnumerable_1_t4073207355;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_GameObject4012695102.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_String968488902.h"

// System.Void Zenject.PrefabFactory`2<System.Object,System.Object>::.ctor()
extern "C"  void PrefabFactory_2__ctor_m1664150090_gshared (PrefabFactory_2_t3259451326 * __this, const MethodInfo* method);
#define PrefabFactory_2__ctor_m1664150090(__this, method) ((  void (*) (PrefabFactory_2_t3259451326 *, const MethodInfo*))PrefabFactory_2__ctor_m1664150090_gshared)(__this, method)
// T Zenject.PrefabFactory`2<System.Object,System.Object>::Create(UnityEngine.GameObject,P1)
extern "C"  Il2CppObject * PrefabFactory_2_Create_m4007342584_gshared (PrefabFactory_2_t3259451326 * __this, GameObject_t4012695102 * ___prefab0, Il2CppObject * ___param1, const MethodInfo* method);
#define PrefabFactory_2_Create_m4007342584(__this, ___prefab0, ___param1, method) ((  Il2CppObject * (*) (PrefabFactory_2_t3259451326 *, GameObject_t4012695102 *, Il2CppObject *, const MethodInfo*))PrefabFactory_2_Create_m4007342584_gshared)(__this, ___prefab0, ___param1, method)
// T Zenject.PrefabFactory`2<System.Object,System.Object>::Create(System.String,P1)
extern "C"  Il2CppObject * PrefabFactory_2_Create_m2719240184_gshared (PrefabFactory_2_t3259451326 * __this, String_t* ___prefabResourceName0, Il2CppObject * ___param1, const MethodInfo* method);
#define PrefabFactory_2_Create_m2719240184(__this, ___prefabResourceName0, ___param1, method) ((  Il2CppObject * (*) (PrefabFactory_2_t3259451326 *, String_t*, Il2CppObject *, const MethodInfo*))PrefabFactory_2_Create_m2719240184_gshared)(__this, ___prefabResourceName0, ___param1, method)
// System.Collections.Generic.IEnumerable`1<Zenject.ZenjectResolveException> Zenject.PrefabFactory`2<System.Object,System.Object>::Validate()
extern "C"  Il2CppObject* PrefabFactory_2_Validate_m1816703535_gshared (PrefabFactory_2_t3259451326 * __this, const MethodInfo* method);
#define PrefabFactory_2_Validate_m1816703535(__this, method) ((  Il2CppObject* (*) (PrefabFactory_2_t3259451326 *, const MethodInfo*))PrefabFactory_2_Validate_m1816703535_gshared)(__this, method)
