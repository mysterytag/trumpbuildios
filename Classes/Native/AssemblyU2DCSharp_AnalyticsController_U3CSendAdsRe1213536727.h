﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Generic.Dictionary`2<System.String,System.String>
struct Dictionary_2_t2606186806;
// System.String
struct String_t;
// System.Object
struct Il2CppObject;
// AnalyticsController
struct AnalyticsController_t2265609122;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AnalyticsController/<SendAdsRevenue>c__Iterator1E
struct  U3CSendAdsRevenueU3Ec__Iterator1E_t1213536727  : public Il2CppObject
{
public:
	// System.Single AnalyticsController/<SendAdsRevenue>c__Iterator1E::revenueUSD
	float ___revenueUSD_0;
	// System.Single AnalyticsController/<SendAdsRevenue>c__Iterator1E::<revenue>__0
	float ___U3CrevenueU3E__0_1;
	// System.Collections.Generic.Dictionary`2<System.String,System.String> AnalyticsController/<SendAdsRevenue>c__Iterator1E::<param>__1
	Dictionary_2_t2606186806 * ___U3CparamU3E__1_2;
	// System.String AnalyticsController/<SendAdsRevenue>c__Iterator1E::name
	String_t* ___name_3;
	// System.String AnalyticsController/<SendAdsRevenue>c__Iterator1E::Cur
	String_t* ___Cur_4;
	// System.Int32 AnalyticsController/<SendAdsRevenue>c__Iterator1E::<revenueRounded>__2
	int32_t ___U3CrevenueRoundedU3E__2_5;
	// System.Int32 AnalyticsController/<SendAdsRevenue>c__Iterator1E::$PC
	int32_t ___U24PC_6;
	// System.Object AnalyticsController/<SendAdsRevenue>c__Iterator1E::$current
	Il2CppObject * ___U24current_7;
	// System.Single AnalyticsController/<SendAdsRevenue>c__Iterator1E::<$>revenueUSD
	float ___U3CU24U3ErevenueUSD_8;
	// System.String AnalyticsController/<SendAdsRevenue>c__Iterator1E::<$>name
	String_t* ___U3CU24U3Ename_9;
	// System.String AnalyticsController/<SendAdsRevenue>c__Iterator1E::<$>Cur
	String_t* ___U3CU24U3ECur_10;
	// AnalyticsController AnalyticsController/<SendAdsRevenue>c__Iterator1E::<>f__this
	AnalyticsController_t2265609122 * ___U3CU3Ef__this_11;

public:
	inline static int32_t get_offset_of_revenueUSD_0() { return static_cast<int32_t>(offsetof(U3CSendAdsRevenueU3Ec__Iterator1E_t1213536727, ___revenueUSD_0)); }
	inline float get_revenueUSD_0() const { return ___revenueUSD_0; }
	inline float* get_address_of_revenueUSD_0() { return &___revenueUSD_0; }
	inline void set_revenueUSD_0(float value)
	{
		___revenueUSD_0 = value;
	}

	inline static int32_t get_offset_of_U3CrevenueU3E__0_1() { return static_cast<int32_t>(offsetof(U3CSendAdsRevenueU3Ec__Iterator1E_t1213536727, ___U3CrevenueU3E__0_1)); }
	inline float get_U3CrevenueU3E__0_1() const { return ___U3CrevenueU3E__0_1; }
	inline float* get_address_of_U3CrevenueU3E__0_1() { return &___U3CrevenueU3E__0_1; }
	inline void set_U3CrevenueU3E__0_1(float value)
	{
		___U3CrevenueU3E__0_1 = value;
	}

	inline static int32_t get_offset_of_U3CparamU3E__1_2() { return static_cast<int32_t>(offsetof(U3CSendAdsRevenueU3Ec__Iterator1E_t1213536727, ___U3CparamU3E__1_2)); }
	inline Dictionary_2_t2606186806 * get_U3CparamU3E__1_2() const { return ___U3CparamU3E__1_2; }
	inline Dictionary_2_t2606186806 ** get_address_of_U3CparamU3E__1_2() { return &___U3CparamU3E__1_2; }
	inline void set_U3CparamU3E__1_2(Dictionary_2_t2606186806 * value)
	{
		___U3CparamU3E__1_2 = value;
		Il2CppCodeGenWriteBarrier(&___U3CparamU3E__1_2, value);
	}

	inline static int32_t get_offset_of_name_3() { return static_cast<int32_t>(offsetof(U3CSendAdsRevenueU3Ec__Iterator1E_t1213536727, ___name_3)); }
	inline String_t* get_name_3() const { return ___name_3; }
	inline String_t** get_address_of_name_3() { return &___name_3; }
	inline void set_name_3(String_t* value)
	{
		___name_3 = value;
		Il2CppCodeGenWriteBarrier(&___name_3, value);
	}

	inline static int32_t get_offset_of_Cur_4() { return static_cast<int32_t>(offsetof(U3CSendAdsRevenueU3Ec__Iterator1E_t1213536727, ___Cur_4)); }
	inline String_t* get_Cur_4() const { return ___Cur_4; }
	inline String_t** get_address_of_Cur_4() { return &___Cur_4; }
	inline void set_Cur_4(String_t* value)
	{
		___Cur_4 = value;
		Il2CppCodeGenWriteBarrier(&___Cur_4, value);
	}

	inline static int32_t get_offset_of_U3CrevenueRoundedU3E__2_5() { return static_cast<int32_t>(offsetof(U3CSendAdsRevenueU3Ec__Iterator1E_t1213536727, ___U3CrevenueRoundedU3E__2_5)); }
	inline int32_t get_U3CrevenueRoundedU3E__2_5() const { return ___U3CrevenueRoundedU3E__2_5; }
	inline int32_t* get_address_of_U3CrevenueRoundedU3E__2_5() { return &___U3CrevenueRoundedU3E__2_5; }
	inline void set_U3CrevenueRoundedU3E__2_5(int32_t value)
	{
		___U3CrevenueRoundedU3E__2_5 = value;
	}

	inline static int32_t get_offset_of_U24PC_6() { return static_cast<int32_t>(offsetof(U3CSendAdsRevenueU3Ec__Iterator1E_t1213536727, ___U24PC_6)); }
	inline int32_t get_U24PC_6() const { return ___U24PC_6; }
	inline int32_t* get_address_of_U24PC_6() { return &___U24PC_6; }
	inline void set_U24PC_6(int32_t value)
	{
		___U24PC_6 = value;
	}

	inline static int32_t get_offset_of_U24current_7() { return static_cast<int32_t>(offsetof(U3CSendAdsRevenueU3Ec__Iterator1E_t1213536727, ___U24current_7)); }
	inline Il2CppObject * get_U24current_7() const { return ___U24current_7; }
	inline Il2CppObject ** get_address_of_U24current_7() { return &___U24current_7; }
	inline void set_U24current_7(Il2CppObject * value)
	{
		___U24current_7 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_7, value);
	}

	inline static int32_t get_offset_of_U3CU24U3ErevenueUSD_8() { return static_cast<int32_t>(offsetof(U3CSendAdsRevenueU3Ec__Iterator1E_t1213536727, ___U3CU24U3ErevenueUSD_8)); }
	inline float get_U3CU24U3ErevenueUSD_8() const { return ___U3CU24U3ErevenueUSD_8; }
	inline float* get_address_of_U3CU24U3ErevenueUSD_8() { return &___U3CU24U3ErevenueUSD_8; }
	inline void set_U3CU24U3ErevenueUSD_8(float value)
	{
		___U3CU24U3ErevenueUSD_8 = value;
	}

	inline static int32_t get_offset_of_U3CU24U3Ename_9() { return static_cast<int32_t>(offsetof(U3CSendAdsRevenueU3Ec__Iterator1E_t1213536727, ___U3CU24U3Ename_9)); }
	inline String_t* get_U3CU24U3Ename_9() const { return ___U3CU24U3Ename_9; }
	inline String_t** get_address_of_U3CU24U3Ename_9() { return &___U3CU24U3Ename_9; }
	inline void set_U3CU24U3Ename_9(String_t* value)
	{
		___U3CU24U3Ename_9 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU24U3Ename_9, value);
	}

	inline static int32_t get_offset_of_U3CU24U3ECur_10() { return static_cast<int32_t>(offsetof(U3CSendAdsRevenueU3Ec__Iterator1E_t1213536727, ___U3CU24U3ECur_10)); }
	inline String_t* get_U3CU24U3ECur_10() const { return ___U3CU24U3ECur_10; }
	inline String_t** get_address_of_U3CU24U3ECur_10() { return &___U3CU24U3ECur_10; }
	inline void set_U3CU24U3ECur_10(String_t* value)
	{
		___U3CU24U3ECur_10 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU24U3ECur_10, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__this_11() { return static_cast<int32_t>(offsetof(U3CSendAdsRevenueU3Ec__Iterator1E_t1213536727, ___U3CU3Ef__this_11)); }
	inline AnalyticsController_t2265609122 * get_U3CU3Ef__this_11() const { return ___U3CU3Ef__this_11; }
	inline AnalyticsController_t2265609122 ** get_address_of_U3CU3Ef__this_11() { return &___U3CU3Ef__this_11; }
	inline void set_U3CU3Ef__this_11(AnalyticsController_t2265609122 * value)
	{
		___U3CU3Ef__this_11 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__this_11, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
