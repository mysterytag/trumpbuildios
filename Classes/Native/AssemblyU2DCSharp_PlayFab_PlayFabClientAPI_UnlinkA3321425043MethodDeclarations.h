﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.PlayFabClientAPI/UnlinkAndroidDeviceIDResponseCallback
struct UnlinkAndroidDeviceIDResponseCallback_t3321425043;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;
// PlayFab.ClientModels.UnlinkAndroidDeviceIDRequest
struct UnlinkAndroidDeviceIDRequest_t1293232914;
// PlayFab.ClientModels.UnlinkAndroidDeviceIDResult
struct UnlinkAndroidDeviceIDResult_t4233284954;
// PlayFab.PlayFabError
struct PlayFabError_t750598646;
// System.IAsyncResult
struct IAsyncResult_t537683269;
// System.AsyncCallback
struct AsyncCallback_t1363551830;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_IntPtr676692020.h"
#include "mscorlib_System_String968488902.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_UnlinkAndro1293232914.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_UnlinkAndro4233284954.h"
#include "AssemblyU2DCSharpU2Dfirstpass_PlayFab_PlayFabError750598646.h"
#include "mscorlib_System_AsyncCallback1363551830.h"

// System.Void PlayFab.PlayFabClientAPI/UnlinkAndroidDeviceIDResponseCallback::.ctor(System.Object,System.IntPtr)
extern "C"  void UnlinkAndroidDeviceIDResponseCallback__ctor_m1636929442 (UnlinkAndroidDeviceIDResponseCallback_t3321425043 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/UnlinkAndroidDeviceIDResponseCallback::Invoke(System.String,System.Int32,PlayFab.ClientModels.UnlinkAndroidDeviceIDRequest,PlayFab.ClientModels.UnlinkAndroidDeviceIDResult,PlayFab.PlayFabError,System.Object)
extern "C"  void UnlinkAndroidDeviceIDResponseCallback_Invoke_m1076045863 (UnlinkAndroidDeviceIDResponseCallback_t3321425043 * __this, String_t* ___urlPath0, int32_t ___callId1, UnlinkAndroidDeviceIDRequest_t1293232914 * ___request2, UnlinkAndroidDeviceIDResult_t4233284954 * ___result3, PlayFabError_t750598646 * ___error4, Il2CppObject * ___customData5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
extern "C" void pinvoke_delegate_wrapper_UnlinkAndroidDeviceIDResponseCallback_t3321425043(Il2CppObject* delegate, String_t* ___urlPath0, int32_t ___callId1, UnlinkAndroidDeviceIDRequest_t1293232914 * ___request2, UnlinkAndroidDeviceIDResult_t4233284954 * ___result3, PlayFabError_t750598646 * ___error4, Il2CppObject * ___customData5);
// System.IAsyncResult PlayFab.PlayFabClientAPI/UnlinkAndroidDeviceIDResponseCallback::BeginInvoke(System.String,System.Int32,PlayFab.ClientModels.UnlinkAndroidDeviceIDRequest,PlayFab.ClientModels.UnlinkAndroidDeviceIDResult,PlayFab.PlayFabError,System.Object,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * UnlinkAndroidDeviceIDResponseCallback_BeginInvoke_m1051755052 (UnlinkAndroidDeviceIDResponseCallback_t3321425043 * __this, String_t* ___urlPath0, int32_t ___callId1, UnlinkAndroidDeviceIDRequest_t1293232914 * ___request2, UnlinkAndroidDeviceIDResult_t4233284954 * ___result3, PlayFabError_t750598646 * ___error4, Il2CppObject * ___customData5, AsyncCallback_t1363551830 * ___callback6, Il2CppObject * ___object7, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/UnlinkAndroidDeviceIDResponseCallback::EndInvoke(System.IAsyncResult)
extern "C"  void UnlinkAndroidDeviceIDResponseCallback_EndInvoke_m2144477874 (UnlinkAndroidDeviceIDResponseCallback_t3321425043 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
