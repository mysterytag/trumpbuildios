﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Func`2<System.Action,System.Object>
struct Func_2_t2670270213;
// System.Action`1<System.Object>
struct Action_1_t985559125;

#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Oper1622431009.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Operators.FromEventObservable`1<System.Object>
struct  FromEventObservable_1_t2790472066  : public OperatorObservableBase_1_t1622431009
{
public:
	// System.Func`2<System.Action,TDelegate> UniRx.Operators.FromEventObservable`1::conversion
	Func_2_t2670270213 * ___conversion_1;
	// System.Action`1<TDelegate> UniRx.Operators.FromEventObservable`1::addHandler
	Action_1_t985559125 * ___addHandler_2;
	// System.Action`1<TDelegate> UniRx.Operators.FromEventObservable`1::removeHandler
	Action_1_t985559125 * ___removeHandler_3;

public:
	inline static int32_t get_offset_of_conversion_1() { return static_cast<int32_t>(offsetof(FromEventObservable_1_t2790472066, ___conversion_1)); }
	inline Func_2_t2670270213 * get_conversion_1() const { return ___conversion_1; }
	inline Func_2_t2670270213 ** get_address_of_conversion_1() { return &___conversion_1; }
	inline void set_conversion_1(Func_2_t2670270213 * value)
	{
		___conversion_1 = value;
		Il2CppCodeGenWriteBarrier(&___conversion_1, value);
	}

	inline static int32_t get_offset_of_addHandler_2() { return static_cast<int32_t>(offsetof(FromEventObservable_1_t2790472066, ___addHandler_2)); }
	inline Action_1_t985559125 * get_addHandler_2() const { return ___addHandler_2; }
	inline Action_1_t985559125 ** get_address_of_addHandler_2() { return &___addHandler_2; }
	inline void set_addHandler_2(Action_1_t985559125 * value)
	{
		___addHandler_2 = value;
		Il2CppCodeGenWriteBarrier(&___addHandler_2, value);
	}

	inline static int32_t get_offset_of_removeHandler_3() { return static_cast<int32_t>(offsetof(FromEventObservable_1_t2790472066, ___removeHandler_3)); }
	inline Action_1_t985559125 * get_removeHandler_3() const { return ___removeHandler_3; }
	inline Action_1_t985559125 ** get_address_of_removeHandler_3() { return &___removeHandler_3; }
	inline void set_removeHandler_3(Action_1_t985559125 * value)
	{
		___removeHandler_3 = value;
		Il2CppCodeGenWriteBarrier(&___removeHandler_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
