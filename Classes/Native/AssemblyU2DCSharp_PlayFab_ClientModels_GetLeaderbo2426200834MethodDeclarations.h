﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.ClientModels.GetLeaderboardAroundCharacterResult
struct GetLeaderboardAroundCharacterResult_t2426200834;
// System.Collections.Generic.List`1<PlayFab.ClientModels.CharacterLeaderboardEntry>
struct List_1_t3914494247;

#include "codegen/il2cpp-codegen.h"

// System.Void PlayFab.ClientModels.GetLeaderboardAroundCharacterResult::.ctor()
extern "C"  void GetLeaderboardAroundCharacterResult__ctor_m2921181159 (GetLeaderboardAroundCharacterResult_t2426200834 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<PlayFab.ClientModels.CharacterLeaderboardEntry> PlayFab.ClientModels.GetLeaderboardAroundCharacterResult::get_Leaderboard()
extern "C"  List_1_t3914494247 * GetLeaderboardAroundCharacterResult_get_Leaderboard_m3439994694 (GetLeaderboardAroundCharacterResult_t2426200834 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.GetLeaderboardAroundCharacterResult::set_Leaderboard(System.Collections.Generic.List`1<PlayFab.ClientModels.CharacterLeaderboardEntry>)
extern "C"  void GetLeaderboardAroundCharacterResult_set_Leaderboard_m1471145805 (GetLeaderboardAroundCharacterResult_t2426200834 * __this, List_1_t3914494247 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
