﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// GlobalStat
struct GlobalStat_t1134678199;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GlobalStat/<PostInject>c__AnonStoreyE2
struct  U3CPostInjectU3Ec__AnonStoreyE2_t980490755  : public Il2CppObject
{
public:
	// System.Int32 GlobalStat/<PostInject>c__AnonStoreyE2::j1
	int32_t ___j1_0;
	// GlobalStat GlobalStat/<PostInject>c__AnonStoreyE2::<>f__this
	GlobalStat_t1134678199 * ___U3CU3Ef__this_1;

public:
	inline static int32_t get_offset_of_j1_0() { return static_cast<int32_t>(offsetof(U3CPostInjectU3Ec__AnonStoreyE2_t980490755, ___j1_0)); }
	inline int32_t get_j1_0() const { return ___j1_0; }
	inline int32_t* get_address_of_j1_0() { return &___j1_0; }
	inline void set_j1_0(int32_t value)
	{
		___j1_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3Ef__this_1() { return static_cast<int32_t>(offsetof(U3CPostInjectU3Ec__AnonStoreyE2_t980490755, ___U3CU3Ef__this_1)); }
	inline GlobalStat_t1134678199 * get_U3CU3Ef__this_1() const { return ___U3CU3Ef__this_1; }
	inline GlobalStat_t1134678199 ** get_address_of_U3CU3Ef__this_1() { return &___U3CU3Ef__this_1; }
	inline void set_U3CU3Ef__this_1(GlobalStat_t1134678199 * value)
	{
		___U3CU3Ef__this_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__this_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
