﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlaneInfo
struct PlaneInfo_t4110763274;

#include "codegen/il2cpp-codegen.h"

// System.Void PlaneInfo::.ctor()
extern "C"  void PlaneInfo__ctor_m3021069217 (PlaneInfo_t4110763274 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
