﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_Nullable_1_gen3097043249.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PlayFab.ClientModels.GetFriendsListRequest
struct  GetFriendsListRequest_t3925362978  : public Il2CppObject
{
public:
	// System.Nullable`1<System.Boolean> PlayFab.ClientModels.GetFriendsListRequest::<IncludeSteamFriends>k__BackingField
	Nullable_1_t3097043249  ___U3CIncludeSteamFriendsU3Ek__BackingField_0;
	// System.Nullable`1<System.Boolean> PlayFab.ClientModels.GetFriendsListRequest::<IncludeFacebookFriends>k__BackingField
	Nullable_1_t3097043249  ___U3CIncludeFacebookFriendsU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CIncludeSteamFriendsU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(GetFriendsListRequest_t3925362978, ___U3CIncludeSteamFriendsU3Ek__BackingField_0)); }
	inline Nullable_1_t3097043249  get_U3CIncludeSteamFriendsU3Ek__BackingField_0() const { return ___U3CIncludeSteamFriendsU3Ek__BackingField_0; }
	inline Nullable_1_t3097043249 * get_address_of_U3CIncludeSteamFriendsU3Ek__BackingField_0() { return &___U3CIncludeSteamFriendsU3Ek__BackingField_0; }
	inline void set_U3CIncludeSteamFriendsU3Ek__BackingField_0(Nullable_1_t3097043249  value)
	{
		___U3CIncludeSteamFriendsU3Ek__BackingField_0 = value;
	}

	inline static int32_t get_offset_of_U3CIncludeFacebookFriendsU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(GetFriendsListRequest_t3925362978, ___U3CIncludeFacebookFriendsU3Ek__BackingField_1)); }
	inline Nullable_1_t3097043249  get_U3CIncludeFacebookFriendsU3Ek__BackingField_1() const { return ___U3CIncludeFacebookFriendsU3Ek__BackingField_1; }
	inline Nullable_1_t3097043249 * get_address_of_U3CIncludeFacebookFriendsU3Ek__BackingField_1() { return &___U3CIncludeFacebookFriendsU3Ek__BackingField_1; }
	inline void set_U3CIncludeFacebookFriendsU3Ek__BackingField_1(Nullable_1_t3097043249  value)
	{
		___U3CIncludeFacebookFriendsU3Ek__BackingField_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
