﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.PlayFabClientAPI/<PurchaseItem>c__AnonStoreyA6
struct U3CPurchaseItemU3Ec__AnonStoreyA6_t3398525422;
// PlayFab.CallRequestContainer
struct CallRequestContainer_t432318609;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharpU2Dfirstpass_PlayFab_CallRequestCo432318609.h"

// System.Void PlayFab.PlayFabClientAPI/<PurchaseItem>c__AnonStoreyA6::.ctor()
extern "C"  void U3CPurchaseItemU3Ec__AnonStoreyA6__ctor_m4194979205 (U3CPurchaseItemU3Ec__AnonStoreyA6_t3398525422 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/<PurchaseItem>c__AnonStoreyA6::<>m__93(PlayFab.CallRequestContainer)
extern "C"  void U3CPurchaseItemU3Ec__AnonStoreyA6_U3CU3Em__93_m3222192957 (U3CPurchaseItemU3Ec__AnonStoreyA6_t3398525422 * __this, CallRequestContainer_t432318609 * ___requestContainer0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
