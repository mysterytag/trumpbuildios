﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.StartObservable`1/StartObserver<System.Int32>
struct StartObserver_t2832445184;
// UniRx.Operators.StartObservable`1<System.Int32>
struct StartObservable_1_t77870867;
// UniRx.IObserver`1<System.Int32>
struct IObserver_1_t764446394;
// System.IDisposable
struct IDisposable_t1628921374;
// System.Exception
struct Exception_t1967233988;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Exception1967233988.h"

// System.Void UniRx.Operators.StartObservable`1/StartObserver<System.Int32>::.ctor(UniRx.Operators.StartObservable`1<T>,UniRx.IObserver`1<T>,System.IDisposable)
extern "C"  void StartObserver__ctor_m2206815641_gshared (StartObserver_t2832445184 * __this, StartObservable_1_t77870867 * ___parent0, Il2CppObject* ___observer1, Il2CppObject * ___cancel2, const MethodInfo* method);
#define StartObserver__ctor_m2206815641(__this, ___parent0, ___observer1, ___cancel2, method) ((  void (*) (StartObserver_t2832445184 *, StartObservable_1_t77870867 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))StartObserver__ctor_m2206815641_gshared)(__this, ___parent0, ___observer1, ___cancel2, method)
// System.Void UniRx.Operators.StartObservable`1/StartObserver<System.Int32>::Run()
extern "C"  void StartObserver_Run_m2636809029_gshared (StartObserver_t2832445184 * __this, const MethodInfo* method);
#define StartObserver_Run_m2636809029(__this, method) ((  void (*) (StartObserver_t2832445184 *, const MethodInfo*))StartObserver_Run_m2636809029_gshared)(__this, method)
// System.Void UniRx.Operators.StartObservable`1/StartObserver<System.Int32>::OnNext(T)
extern "C"  void StartObserver_OnNext_m1843831748_gshared (StartObserver_t2832445184 * __this, int32_t ___value0, const MethodInfo* method);
#define StartObserver_OnNext_m1843831748(__this, ___value0, method) ((  void (*) (StartObserver_t2832445184 *, int32_t, const MethodInfo*))StartObserver_OnNext_m1843831748_gshared)(__this, ___value0, method)
// System.Void UniRx.Operators.StartObservable`1/StartObserver<System.Int32>::OnError(System.Exception)
extern "C"  void StartObserver_OnError_m3079845587_gshared (StartObserver_t2832445184 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method);
#define StartObserver_OnError_m3079845587(__this, ___error0, method) ((  void (*) (StartObserver_t2832445184 *, Exception_t1967233988 *, const MethodInfo*))StartObserver_OnError_m3079845587_gshared)(__this, ___error0, method)
// System.Void UniRx.Operators.StartObservable`1/StartObserver<System.Int32>::OnCompleted()
extern "C"  void StartObserver_OnCompleted_m1672924838_gshared (StartObserver_t2832445184 * __this, const MethodInfo* method);
#define StartObserver_OnCompleted_m1672924838(__this, method) ((  void (*) (StartObserver_t2832445184 *, const MethodInfo*))StartObserver_OnCompleted_m1672924838_gshared)(__this, method)
