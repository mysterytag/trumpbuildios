﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CellReactiveApi/DisposableContainer`1<System.Boolean>
struct DisposableContainer_1_t2879821764;

#include "codegen/il2cpp-codegen.h"

// System.Void CellReactiveApi/DisposableContainer`1<System.Boolean>::.ctor()
extern "C"  void DisposableContainer_1__ctor_m461451184_gshared (DisposableContainer_1_t2879821764 * __this, const MethodInfo* method);
#define DisposableContainer_1__ctor_m461451184(__this, method) ((  void (*) (DisposableContainer_1_t2879821764 *, const MethodInfo*))DisposableContainer_1__ctor_m461451184_gshared)(__this, method)
// System.Void CellReactiveApi/DisposableContainer`1<System.Boolean>::Dispose()
extern "C"  void DisposableContainer_1_Dispose_m971239405_gshared (DisposableContainer_1_t2879821764 * __this, const MethodInfo* method);
#define DisposableContainer_1_Dispose_m971239405(__this, method) ((  void (*) (DisposableContainer_1_t2879821764 *, const MethodInfo*))DisposableContainer_1_Dispose_m971239405_gshared)(__this, method)
