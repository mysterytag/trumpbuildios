﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.ToListObservable`1<System.Object>
struct ToListObservable_1_t977785859;
// UniRx.IObservable`1<System.Object>
struct IObservable_1_t595904784;
// System.IDisposable
struct IDisposable_t1628921374;
// UniRx.IObserver`1<System.Collections.Generic.IList`1<System.Object>>
struct IObserver_1_t920630341;

#include "codegen/il2cpp-codegen.h"

// System.Void UniRx.Operators.ToListObservable`1<System.Object>::.ctor(UniRx.IObservable`1<TSource>)
extern "C"  void ToListObservable_1__ctor_m640028121_gshared (ToListObservable_1_t977785859 * __this, Il2CppObject* ___source0, const MethodInfo* method);
#define ToListObservable_1__ctor_m640028121(__this, ___source0, method) ((  void (*) (ToListObservable_1_t977785859 *, Il2CppObject*, const MethodInfo*))ToListObservable_1__ctor_m640028121_gshared)(__this, ___source0, method)
// System.IDisposable UniRx.Operators.ToListObservable`1<System.Object>::SubscribeCore(UniRx.IObserver`1<System.Collections.Generic.IList`1<TSource>>,System.IDisposable)
extern "C"  Il2CppObject * ToListObservable_1_SubscribeCore_m2263040283_gshared (ToListObservable_1_t977785859 * __this, Il2CppObject* ___observer0, Il2CppObject * ___cancel1, const MethodInfo* method);
#define ToListObservable_1_SubscribeCore_m2263040283(__this, ___observer0, ___cancel1, method) ((  Il2CppObject * (*) (ToListObservable_1_t977785859 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))ToListObservable_1_SubscribeCore_m2263040283_gshared)(__this, ___observer0, ___cancel1, method)
