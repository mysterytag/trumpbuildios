﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Generic.Dictionary`2<System.String,UnityEngine.GameObject>
struct Dictionary_2_t1355425710;
// UnityEngine.Canvas
struct Canvas_t3534013893;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PreviewIconProvider
struct  PreviewIconProvider_t2704585042  : public Il2CppObject
{
public:
	// System.Collections.Generic.Dictionary`2<System.String,UnityEngine.GameObject> PreviewIconProvider::prefabs
	Dictionary_2_t1355425710 * ___prefabs_0;
	// UnityEngine.Canvas PreviewIconProvider::_canvas
	Canvas_t3534013893 * ____canvas_1;

public:
	inline static int32_t get_offset_of_prefabs_0() { return static_cast<int32_t>(offsetof(PreviewIconProvider_t2704585042, ___prefabs_0)); }
	inline Dictionary_2_t1355425710 * get_prefabs_0() const { return ___prefabs_0; }
	inline Dictionary_2_t1355425710 ** get_address_of_prefabs_0() { return &___prefabs_0; }
	inline void set_prefabs_0(Dictionary_2_t1355425710 * value)
	{
		___prefabs_0 = value;
		Il2CppCodeGenWriteBarrier(&___prefabs_0, value);
	}

	inline static int32_t get_offset_of__canvas_1() { return static_cast<int32_t>(offsetof(PreviewIconProvider_t2704585042, ____canvas_1)); }
	inline Canvas_t3534013893 * get__canvas_1() const { return ____canvas_1; }
	inline Canvas_t3534013893 ** get_address_of__canvas_1() { return &____canvas_1; }
	inline void set__canvas_1(Canvas_t3534013893 * value)
	{
		____canvas_1 = value;
		Il2CppCodeGenWriteBarrier(&____canvas_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
