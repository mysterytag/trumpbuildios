﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SubstanceBase`2<System.Single,System.Object>
struct SubstanceBase_2_t1748143363;
// ICell`1<System.Single>
struct ICell_1_t2509839998;
// System.IDisposable
struct IDisposable_t1628921374;
// System.Action`1<System.Single>
struct Action_1_t1106661726;
// System.Action
struct Action_t437523947;
// System.Object
struct Il2CppObject;
// IStream`1<System.Single>
struct IStream_1_t1510900012;
// SubstanceBase`2/Intrusion<System.Single,System.Object>
struct Intrusion_t4252761119;
// ILiving
struct ILiving_t2639664210;
// ICell`1<System.Object>
struct ICell_1_t2388737397;
// IEmptyStream
struct IEmptyStream_t3684082468;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Priority3194150340.h"
#include "System_Core_System_Action437523947.h"
#include "mscorlib_System_Object837106420.h"

// System.Void SubstanceBase`2<System.Single,System.Object>::.ctor()
extern "C"  void SubstanceBase_2__ctor_m190675853_gshared (SubstanceBase_2_t1748143363 * __this, const MethodInfo* method);
#define SubstanceBase_2__ctor_m190675853(__this, method) ((  void (*) (SubstanceBase_2_t1748143363 *, const MethodInfo*))SubstanceBase_2__ctor_m190675853_gshared)(__this, method)
// System.Void SubstanceBase`2<System.Single,System.Object>::.ctor(T)
extern "C"  void SubstanceBase_2__ctor_m1615985521_gshared (SubstanceBase_2_t1748143363 * __this, float ___baseVal0, const MethodInfo* method);
#define SubstanceBase_2__ctor_m1615985521(__this, ___baseVal0, method) ((  void (*) (SubstanceBase_2_t1748143363 *, float, const MethodInfo*))SubstanceBase_2__ctor_m1615985521_gshared)(__this, ___baseVal0, method)
// T SubstanceBase`2<System.Single,System.Object>::get_baseValue()
extern "C"  float SubstanceBase_2_get_baseValue_m2947284323_gshared (SubstanceBase_2_t1748143363 * __this, const MethodInfo* method);
#define SubstanceBase_2_get_baseValue_m2947284323(__this, method) ((  float (*) (SubstanceBase_2_t1748143363 *, const MethodInfo*))SubstanceBase_2_get_baseValue_m2947284323_gshared)(__this, method)
// System.Void SubstanceBase`2<System.Single,System.Object>::set_baseValue(T)
extern "C"  void SubstanceBase_2_set_baseValue_m1388286864_gshared (SubstanceBase_2_t1748143363 * __this, float ___value0, const MethodInfo* method);
#define SubstanceBase_2_set_baseValue_m1388286864(__this, ___value0, method) ((  void (*) (SubstanceBase_2_t1748143363 *, float, const MethodInfo*))SubstanceBase_2_set_baseValue_m1388286864_gshared)(__this, ___value0, method)
// System.Void SubstanceBase`2<System.Single,System.Object>::set_baseValueReactive(ICell`1<T>)
extern "C"  void SubstanceBase_2_set_baseValueReactive_m4252303197_gshared (SubstanceBase_2_t1748143363 * __this, Il2CppObject* ___value0, const MethodInfo* method);
#define SubstanceBase_2_set_baseValueReactive_m4252303197(__this, ___value0, method) ((  void (*) (SubstanceBase_2_t1748143363 *, Il2CppObject*, const MethodInfo*))SubstanceBase_2_set_baseValueReactive_m4252303197_gshared)(__this, ___value0, method)
// System.IDisposable SubstanceBase`2<System.Single,System.Object>::Bind(System.Action`1<T>,Priority)
extern "C"  Il2CppObject * SubstanceBase_2_Bind_m1838262843_gshared (SubstanceBase_2_t1748143363 * __this, Action_1_t1106661726 * ___action0, int32_t ___p1, const MethodInfo* method);
#define SubstanceBase_2_Bind_m1838262843(__this, ___action0, ___p1, method) ((  Il2CppObject * (*) (SubstanceBase_2_t1748143363 *, Action_1_t1106661726 *, int32_t, const MethodInfo*))SubstanceBase_2_Bind_m1838262843_gshared)(__this, ___action0, ___p1, method)
// System.IDisposable SubstanceBase`2<System.Single,System.Object>::OnChanged(System.Action,Priority)
extern "C"  Il2CppObject * SubstanceBase_2_OnChanged_m2407165170_gshared (SubstanceBase_2_t1748143363 * __this, Action_t437523947 * ___action0, int32_t ___p1, const MethodInfo* method);
#define SubstanceBase_2_OnChanged_m2407165170(__this, ___action0, ___p1, method) ((  Il2CppObject * (*) (SubstanceBase_2_t1748143363 *, Action_t437523947 *, int32_t, const MethodInfo*))SubstanceBase_2_OnChanged_m2407165170_gshared)(__this, ___action0, ___p1, method)
// System.Object SubstanceBase`2<System.Single,System.Object>::get_valueObject()
extern "C"  Il2CppObject * SubstanceBase_2_get_valueObject_m820874205_gshared (SubstanceBase_2_t1748143363 * __this, const MethodInfo* method);
#define SubstanceBase_2_get_valueObject_m820874205(__this, method) ((  Il2CppObject * (*) (SubstanceBase_2_t1748143363 *, const MethodInfo*))SubstanceBase_2_get_valueObject_m820874205_gshared)(__this, method)
// System.IDisposable SubstanceBase`2<System.Single,System.Object>::ListenUpdates(System.Action`1<T>,Priority)
extern "C"  Il2CppObject * SubstanceBase_2_ListenUpdates_m1765231655_gshared (SubstanceBase_2_t1748143363 * __this, Action_1_t1106661726 * ___action0, int32_t ___p1, const MethodInfo* method);
#define SubstanceBase_2_ListenUpdates_m1765231655(__this, ___action0, ___p1, method) ((  Il2CppObject * (*) (SubstanceBase_2_t1748143363 *, Action_1_t1106661726 *, int32_t, const MethodInfo*))SubstanceBase_2_ListenUpdates_m1765231655_gshared)(__this, ___action0, ___p1, method)
// IStream`1<T> SubstanceBase`2<System.Single,System.Object>::get_updates()
extern "C"  Il2CppObject* SubstanceBase_2_get_updates_m1848791611_gshared (SubstanceBase_2_t1748143363 * __this, const MethodInfo* method);
#define SubstanceBase_2_get_updates_m1848791611(__this, method) ((  Il2CppObject* (*) (SubstanceBase_2_t1748143363 *, const MethodInfo*))SubstanceBase_2_get_updates_m1848791611_gshared)(__this, method)
// T SubstanceBase`2<System.Single,System.Object>::get_value()
extern "C"  float SubstanceBase_2_get_value_m91912820_gshared (SubstanceBase_2_t1748143363 * __this, const MethodInfo* method);
#define SubstanceBase_2_get_value_m91912820(__this, method) ((  float (*) (SubstanceBase_2_t1748143363 *, const MethodInfo*))SubstanceBase_2_get_value_m91912820_gshared)(__this, method)
// SubstanceBase`2/Intrusion<T,InfT> SubstanceBase`2<System.Single,System.Object>::AffectInner(ILiving,InfT)
extern "C"  Intrusion_t4252761119 * SubstanceBase_2_AffectInner_m123032174_gshared (SubstanceBase_2_t1748143363 * __this, Il2CppObject * ___intruder0, Il2CppObject * ___influence1, const MethodInfo* method);
#define SubstanceBase_2_AffectInner_m123032174(__this, ___intruder0, ___influence1, method) ((  Intrusion_t4252761119 * (*) (SubstanceBase_2_t1748143363 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))SubstanceBase_2_AffectInner_m123032174_gshared)(__this, ___intruder0, ___influence1, method)
// System.IDisposable SubstanceBase`2<System.Single,System.Object>::AffectUnsafe(InfT)
extern "C"  Il2CppObject * SubstanceBase_2_AffectUnsafe_m3348397042_gshared (SubstanceBase_2_t1748143363 * __this, Il2CppObject * ___influence0, const MethodInfo* method);
#define SubstanceBase_2_AffectUnsafe_m3348397042(__this, ___influence0, method) ((  Il2CppObject * (*) (SubstanceBase_2_t1748143363 *, Il2CppObject *, const MethodInfo*))SubstanceBase_2_AffectUnsafe_m3348397042_gshared)(__this, ___influence0, method)
// System.IDisposable SubstanceBase`2<System.Single,System.Object>::AffectUnsafe(ICell`1<InfT>)
extern "C"  Il2CppObject * SubstanceBase_2_AffectUnsafe_m3878751842_gshared (SubstanceBase_2_t1748143363 * __this, Il2CppObject* ___influence0, const MethodInfo* method);
#define SubstanceBase_2_AffectUnsafe_m3878751842(__this, ___influence0, method) ((  Il2CppObject * (*) (SubstanceBase_2_t1748143363 *, Il2CppObject*, const MethodInfo*))SubstanceBase_2_AffectUnsafe_m3878751842_gshared)(__this, ___influence0, method)
// System.IDisposable SubstanceBase`2<System.Single,System.Object>::Affect(ILiving,InfT)
extern "C"  Il2CppObject * SubstanceBase_2_Affect_m1083739474_gshared (SubstanceBase_2_t1748143363 * __this, Il2CppObject * ___intruder0, Il2CppObject * ___influence1, const MethodInfo* method);
#define SubstanceBase_2_Affect_m1083739474(__this, ___intruder0, ___influence1, method) ((  Il2CppObject * (*) (SubstanceBase_2_t1748143363 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))SubstanceBase_2_Affect_m1083739474_gshared)(__this, ___intruder0, ___influence1, method)
// System.IDisposable SubstanceBase`2<System.Single,System.Object>::Affect(ILiving,ICell`1<InfT>)
extern "C"  Il2CppObject * SubstanceBase_2_Affect_m3468331266_gshared (SubstanceBase_2_t1748143363 * __this, Il2CppObject * ___intruder0, Il2CppObject* ___influence1, const MethodInfo* method);
#define SubstanceBase_2_Affect_m3468331266(__this, ___intruder0, ___influence1, method) ((  Il2CppObject * (*) (SubstanceBase_2_t1748143363 *, Il2CppObject *, Il2CppObject*, const MethodInfo*))SubstanceBase_2_Affect_m3468331266_gshared)(__this, ___intruder0, ___influence1, method)
// System.IDisposable SubstanceBase`2<System.Single,System.Object>::Affect(ILiving,InfT,IEmptyStream)
extern "C"  Il2CppObject * SubstanceBase_2_Affect_m878069328_gshared (SubstanceBase_2_t1748143363 * __this, Il2CppObject * ___intruder0, Il2CppObject * ___influence1, Il2CppObject * ___update2, const MethodInfo* method);
#define SubstanceBase_2_Affect_m878069328(__this, ___intruder0, ___influence1, ___update2, method) ((  Il2CppObject * (*) (SubstanceBase_2_t1748143363 *, Il2CppObject *, Il2CppObject *, Il2CppObject *, const MethodInfo*))SubstanceBase_2_Affect_m878069328_gshared)(__this, ___intruder0, ___influence1, ___update2, method)
// System.Void SubstanceBase`2<System.Single,System.Object>::OnAdd(SubstanceBase`2/Intrusion<T,InfT>)
extern "C"  void SubstanceBase_2_OnAdd_m1323237891_gshared (SubstanceBase_2_t1748143363 * __this, Intrusion_t4252761119 * ___intrusion0, const MethodInfo* method);
#define SubstanceBase_2_OnAdd_m1323237891(__this, ___intrusion0, method) ((  void (*) (SubstanceBase_2_t1748143363 *, Intrusion_t4252761119 *, const MethodInfo*))SubstanceBase_2_OnAdd_m1323237891_gshared)(__this, ___intrusion0, method)
// System.Void SubstanceBase`2<System.Single,System.Object>::OnRemove(SubstanceBase`2/Intrusion<T,InfT>)
extern "C"  void SubstanceBase_2_OnRemove_m3668150902_gshared (SubstanceBase_2_t1748143363 * __this, Intrusion_t4252761119 * ___intrusion0, const MethodInfo* method);
#define SubstanceBase_2_OnRemove_m3668150902(__this, ___intrusion0, method) ((  void (*) (SubstanceBase_2_t1748143363 *, Intrusion_t4252761119 *, const MethodInfo*))SubstanceBase_2_OnRemove_m3668150902_gshared)(__this, ___intrusion0, method)
// System.Void SubstanceBase`2<System.Single,System.Object>::OnUpdate(SubstanceBase`2/Intrusion<T,InfT>)
extern "C"  void SubstanceBase_2_OnUpdate_m3018882065_gshared (SubstanceBase_2_t1748143363 * __this, Intrusion_t4252761119 * ___intrusion0, const MethodInfo* method);
#define SubstanceBase_2_OnUpdate_m3018882065(__this, ___intrusion0, method) ((  void (*) (SubstanceBase_2_t1748143363 *, Intrusion_t4252761119 *, const MethodInfo*))SubstanceBase_2_OnUpdate_m3018882065_gshared)(__this, ___intrusion0, method)
// System.Void SubstanceBase`2<System.Single,System.Object>::UpdateAll()
extern "C"  void SubstanceBase_2_UpdateAll_m3645799875_gshared (SubstanceBase_2_t1748143363 * __this, const MethodInfo* method);
#define SubstanceBase_2_UpdateAll_m3645799875(__this, method) ((  void (*) (SubstanceBase_2_t1748143363 *, const MethodInfo*))SubstanceBase_2_UpdateAll_m3645799875_gshared)(__this, method)
// System.Void SubstanceBase`2<System.Single,System.Object>::ClearIntrusion(SubstanceBase`2/Intrusion<T,InfT>)
extern "C"  void SubstanceBase_2_ClearIntrusion_m4200609407_gshared (SubstanceBase_2_t1748143363 * __this, Intrusion_t4252761119 * ___intr0, const MethodInfo* method);
#define SubstanceBase_2_ClearIntrusion_m4200609407(__this, ___intr0, method) ((  void (*) (SubstanceBase_2_t1748143363 *, Intrusion_t4252761119 *, const MethodInfo*))SubstanceBase_2_ClearIntrusion_m4200609407_gshared)(__this, ___intr0, method)
// System.Void SubstanceBase`2<System.Single,System.Object>::<set_baseValueReactive>m__1D7(T)
extern "C"  void SubstanceBase_2_U3Cset_baseValueReactiveU3Em__1D7_m1323393214_gshared (SubstanceBase_2_t1748143363 * __this, float ___val0, const MethodInfo* method);
#define SubstanceBase_2_U3Cset_baseValueReactiveU3Em__1D7_m1323393214(__this, ___val0, method) ((  void (*) (SubstanceBase_2_t1748143363 *, float, const MethodInfo*))SubstanceBase_2_U3Cset_baseValueReactiveU3Em__1D7_m1323393214_gshared)(__this, ___val0, method)
