﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "System_System_Collections_Generic_LinkedList_1_gen2577235966MethodDeclarations.h"

// System.Void System.Collections.Generic.LinkedList`1<Zenject.TaskUpdater`1/TaskInfo<Zenject.ILateTickable>>::.ctor()
#define LinkedList_1__ctor_m2588663520(__this, method) ((  void (*) (LinkedList_1_t1138375077 *, const MethodInfo*))LinkedList_1__ctor_m2955457271_gshared)(__this, method)
// System.Void System.Collections.Generic.LinkedList`1<Zenject.TaskUpdater`1/TaskInfo<Zenject.ILateTickable>>::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
#define LinkedList_1__ctor_m704352289(__this, ___info0, ___context1, method) ((  void (*) (LinkedList_1_t1138375077 *, SerializationInfo_t2995724695 *, StreamingContext_t986364934 , const MethodInfo*))LinkedList_1__ctor_m3369579448_gshared)(__this, ___info0, ___context1, method)
// System.Void System.Collections.Generic.LinkedList`1<Zenject.TaskUpdater`1/TaskInfo<Zenject.ILateTickable>>::System.Collections.Generic.ICollection<T>.Add(T)
#define LinkedList_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m695051167(__this, ___value0, method) ((  void (*) (LinkedList_1_t1138375077 *, TaskInfo_t3693212827 *, const MethodInfo*))LinkedList_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m3576108392_gshared)(__this, ___value0, method)
// System.Void System.Collections.Generic.LinkedList`1<Zenject.TaskUpdater`1/TaskInfo<Zenject.ILateTickable>>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define LinkedList_1_System_Collections_ICollection_CopyTo_m551762532(__this, ___array0, ___index1, method) ((  void (*) (LinkedList_1_t1138375077 *, Il2CppArray *, int32_t, const MethodInfo*))LinkedList_1_System_Collections_ICollection_CopyTo_m2331638317_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.LinkedList`1<Zenject.TaskUpdater`1/TaskInfo<Zenject.ILateTickable>>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
#define LinkedList_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m2938538290(__this, method) ((  Il2CppObject* (*) (LinkedList_1_t1138375077 *, const MethodInfo*))LinkedList_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m2027502985_gshared)(__this, method)
// System.Collections.IEnumerator System.Collections.Generic.LinkedList`1<Zenject.TaskUpdater`1/TaskInfo<Zenject.ILateTickable>>::System.Collections.IEnumerable.GetEnumerator()
#define LinkedList_1_System_Collections_IEnumerable_GetEnumerator_m3319360627(__this, method) ((  Il2CppObject * (*) (LinkedList_1_t1138375077 *, const MethodInfo*))LinkedList_1_System_Collections_IEnumerable_GetEnumerator_m51916412_gshared)(__this, method)
// System.Boolean System.Collections.Generic.LinkedList`1<Zenject.TaskUpdater`1/TaskInfo<Zenject.ILateTickable>>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
#define LinkedList_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m3688378031(__this, method) ((  bool (*) (LinkedList_1_t1138375077 *, const MethodInfo*))LinkedList_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m2230847288_gshared)(__this, method)
// System.Boolean System.Collections.Generic.LinkedList`1<Zenject.TaskUpdater`1/TaskInfo<Zenject.ILateTickable>>::System.Collections.ICollection.get_IsSynchronized()
#define LinkedList_1_System_Collections_ICollection_get_IsSynchronized_m3306692718(__this, method) ((  bool (*) (LinkedList_1_t1138375077 *, const MethodInfo*))LinkedList_1_System_Collections_ICollection_get_IsSynchronized_m683991045_gshared)(__this, method)
// System.Object System.Collections.Generic.LinkedList`1<Zenject.TaskUpdater`1/TaskInfo<Zenject.ILateTickable>>::System.Collections.ICollection.get_SyncRoot()
#define LinkedList_1_System_Collections_ICollection_get_SyncRoot_m126148782(__this, method) ((  Il2CppObject * (*) (LinkedList_1_t1138375077 *, const MethodInfo*))LinkedList_1_System_Collections_ICollection_get_SyncRoot_m573420165_gshared)(__this, method)
// System.Void System.Collections.Generic.LinkedList`1<Zenject.TaskUpdater`1/TaskInfo<Zenject.ILateTickable>>::VerifyReferencedNode(System.Collections.Generic.LinkedListNode`1<T>)
#define LinkedList_1_VerifyReferencedNode_m3978592075(__this, ___node0, method) ((  void (*) (LinkedList_1_t1138375077 *, LinkedListNode_1_t3395203447 *, const MethodInfo*))LinkedList_1_VerifyReferencedNode_m3939775124_gshared)(__this, ___node0, method)
// System.Collections.Generic.LinkedListNode`1<T> System.Collections.Generic.LinkedList`1<Zenject.TaskUpdater`1/TaskInfo<Zenject.ILateTickable>>::AddBefore(System.Collections.Generic.LinkedListNode`1<T>,T)
#define LinkedList_1_AddBefore_m2352243575(__this, ___node0, ___value1, method) ((  LinkedListNode_1_t3395203447 * (*) (LinkedList_1_t1138375077 *, LinkedListNode_1_t3395203447 *, TaskInfo_t3693212827 *, const MethodInfo*))LinkedList_1_AddBefore_m1086189582_gshared)(__this, ___node0, ___value1, method)
// System.Collections.Generic.LinkedListNode`1<T> System.Collections.Generic.LinkedList`1<Zenject.TaskUpdater`1/TaskInfo<Zenject.ILateTickable>>::AddLast(T)
#define LinkedList_1_AddLast_m1770606523(__this, ___value0, method) ((  LinkedListNode_1_t3395203447 * (*) (LinkedList_1_t1138375077 *, TaskInfo_t3693212827 *, const MethodInfo*))LinkedList_1_AddLast_m4070107716_gshared)(__this, ___value0, method)
// System.Void System.Collections.Generic.LinkedList`1<Zenject.TaskUpdater`1/TaskInfo<Zenject.ILateTickable>>::Clear()
#define LinkedList_1_Clear_m4289764107(__this, method) ((  void (*) (LinkedList_1_t1138375077 *, const MethodInfo*))LinkedList_1_Clear_m361590562_gshared)(__this, method)
// System.Boolean System.Collections.Generic.LinkedList`1<Zenject.TaskUpdater`1/TaskInfo<Zenject.ILateTickable>>::Contains(T)
#define LinkedList_1_Contains_m1844609061(__this, ___value0, method) ((  bool (*) (LinkedList_1_t1138375077 *, TaskInfo_t3693212827 *, const MethodInfo*))LinkedList_1_Contains_m3484410556_gshared)(__this, ___value0, method)
// System.Void System.Collections.Generic.LinkedList`1<Zenject.TaskUpdater`1/TaskInfo<Zenject.ILateTickable>>::CopyTo(T[],System.Int32)
#define LinkedList_1_CopyTo_m2027234255(__this, ___array0, ___index1, method) ((  void (*) (LinkedList_1_t1138375077 *, TaskInfoU5BU5D_t2704561818*, int32_t, const MethodInfo*))LinkedList_1_CopyTo_m3470139544_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.LinkedListNode`1<T> System.Collections.Generic.LinkedList`1<Zenject.TaskUpdater`1/TaskInfo<Zenject.ILateTickable>>::Find(T)
#define LinkedList_1_Find_m1645225679(__this, ___value0, method) ((  LinkedListNode_1_t3395203447 * (*) (LinkedList_1_t1138375077 *, TaskInfo_t3693212827 *, const MethodInfo*))LinkedList_1_Find_m2643247334_gshared)(__this, ___value0, method)
// System.Collections.Generic.LinkedList`1/Enumerator<T> System.Collections.Generic.LinkedList`1<Zenject.TaskUpdater`1/TaskInfo<Zenject.ILateTickable>>::GetEnumerator()
#define LinkedList_1_GetEnumerator_m1054675951(__this, method) ((  Enumerator_t2575954788  (*) (LinkedList_1_t1138375077 *, const MethodInfo*))LinkedList_1_GetEnumerator_m3713737734_gshared)(__this, method)
// System.Void System.Collections.Generic.LinkedList`1<Zenject.TaskUpdater`1/TaskInfo<Zenject.ILateTickable>>::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
#define LinkedList_1_GetObjectData_m444540542(__this, ___info0, ___context1, method) ((  void (*) (LinkedList_1_t1138375077 *, SerializationInfo_t2995724695 *, StreamingContext_t986364934 , const MethodInfo*))LinkedList_1_GetObjectData_m3974480661_gshared)(__this, ___info0, ___context1, method)
// System.Void System.Collections.Generic.LinkedList`1<Zenject.TaskUpdater`1/TaskInfo<Zenject.ILateTickable>>::OnDeserialization(System.Object)
#define LinkedList_1_OnDeserialization_m2958458534(__this, ___sender0, method) ((  void (*) (LinkedList_1_t1138375077 *, Il2CppObject *, const MethodInfo*))LinkedList_1_OnDeserialization_m3445006959_gshared)(__this, ___sender0, method)
// System.Boolean System.Collections.Generic.LinkedList`1<Zenject.TaskUpdater`1/TaskInfo<Zenject.ILateTickable>>::Remove(T)
#define LinkedList_1_Remove_m698549600(__this, ___value0, method) ((  bool (*) (LinkedList_1_t1138375077 *, TaskInfo_t3693212827 *, const MethodInfo*))LinkedList_1_Remove_m3283493303_gshared)(__this, ___value0, method)
// System.Void System.Collections.Generic.LinkedList`1<Zenject.TaskUpdater`1/TaskInfo<Zenject.ILateTickable>>::Remove(System.Collections.Generic.LinkedListNode`1<T>)
#define LinkedList_1_Remove_m2325269531(__this, ___node0, method) ((  void (*) (LinkedList_1_t1138375077 *, LinkedListNode_1_t3395203447 *, const MethodInfo*))LinkedList_1_Remove_m4034790180_gshared)(__this, ___node0, method)
// System.Void System.Collections.Generic.LinkedList`1<Zenject.TaskUpdater`1/TaskInfo<Zenject.ILateTickable>>::RemoveFirst()
#define LinkedList_1_RemoveFirst_m3085433802(__this, method) ((  void (*) (LinkedList_1_t1138375077 *, const MethodInfo*))LinkedList_1_RemoveFirst_m1652892321_gshared)(__this, method)
// System.Void System.Collections.Generic.LinkedList`1<Zenject.TaskUpdater`1/TaskInfo<Zenject.ILateTickable>>::RemoveLast()
#define LinkedList_1_RemoveLast_m2065060574(__this, method) ((  void (*) (LinkedList_1_t1138375077 *, const MethodInfo*))LinkedList_1_RemoveLast_m2573038887_gshared)(__this, method)
// System.Int32 System.Collections.Generic.LinkedList`1<Zenject.TaskUpdater`1/TaskInfo<Zenject.ILateTickable>>::get_Count()
#define LinkedList_1_get_Count_m1475813556(__this, method) ((  int32_t (*) (LinkedList_1_t1138375077 *, const MethodInfo*))LinkedList_1_get_Count_m1368924491_gshared)(__this, method)
// System.Collections.Generic.LinkedListNode`1<T> System.Collections.Generic.LinkedList`1<Zenject.TaskUpdater`1/TaskInfo<Zenject.ILateTickable>>::get_First()
#define LinkedList_1_get_First_m713527539(__this, method) ((  LinkedListNode_1_t3395203447 * (*) (LinkedList_1_t1138375077 *, const MethodInfo*))LinkedList_1_get_First_m3278587786_gshared)(__this, method)
// System.Collections.Generic.LinkedListNode`1<T> System.Collections.Generic.LinkedList`1<Zenject.TaskUpdater`1/TaskInfo<Zenject.ILateTickable>>::get_Last()
#define LinkedList_1_get_Last_m2265642133(__this, method) ((  LinkedListNode_1_t3395203447 * (*) (LinkedList_1_t1138375077 *, const MethodInfo*))LinkedList_1_get_Last_m270176030_gshared)(__this, method)
