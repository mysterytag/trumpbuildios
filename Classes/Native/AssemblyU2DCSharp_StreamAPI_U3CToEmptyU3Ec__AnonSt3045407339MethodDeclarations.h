﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// StreamAPI/<ToEmpty>c__AnonStorey132`1<System.Object>
struct U3CToEmptyU3Ec__AnonStorey132_1_t3045407339;
// System.IDisposable
struct IDisposable_t1628921374;
// System.Action
struct Action_t437523947;

#include "codegen/il2cpp-codegen.h"
#include "System_Core_System_Action437523947.h"
#include "AssemblyU2DCSharp_Priority3194150340.h"

// System.Void StreamAPI/<ToEmpty>c__AnonStorey132`1<System.Object>::.ctor()
extern "C"  void U3CToEmptyU3Ec__AnonStorey132_1__ctor_m3593844684_gshared (U3CToEmptyU3Ec__AnonStorey132_1_t3045407339 * __this, const MethodInfo* method);
#define U3CToEmptyU3Ec__AnonStorey132_1__ctor_m3593844684(__this, method) ((  void (*) (U3CToEmptyU3Ec__AnonStorey132_1_t3045407339 *, const MethodInfo*))U3CToEmptyU3Ec__AnonStorey132_1__ctor_m3593844684_gshared)(__this, method)
// System.IDisposable StreamAPI/<ToEmpty>c__AnonStorey132`1<System.Object>::<>m__1C4(System.Action,Priority)
extern "C"  Il2CppObject * U3CToEmptyU3Ec__AnonStorey132_1_U3CU3Em__1C4_m1494221543_gshared (U3CToEmptyU3Ec__AnonStorey132_1_t3045407339 * __this, Action_t437523947 * ___reaction0, int32_t ___p1, const MethodInfo* method);
#define U3CToEmptyU3Ec__AnonStorey132_1_U3CU3Em__1C4_m1494221543(__this, ___reaction0, ___p1, method) ((  Il2CppObject * (*) (U3CToEmptyU3Ec__AnonStorey132_1_t3045407339 *, Action_t437523947 *, int32_t, const MethodInfo*))U3CToEmptyU3Ec__AnonStorey132_1_U3CU3Em__1C4_m1494221543_gshared)(__this, ___reaction0, ___p1, method)
