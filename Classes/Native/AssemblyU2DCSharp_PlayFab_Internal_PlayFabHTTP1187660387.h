﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Threading.Thread
struct Thread_t1674723085;
// System.Object
struct Il2CppObject;
// System.Collections.Generic.List`1<PlayFab.CallRequestContainer>
struct List_1_t1229277578;
// System.Collections.Generic.Queue`1<PlayFab.CallRequestContainer>
struct Queue_1_t2140406149;

#include "AssemblyU2DCSharp_PlayFab_Internal_SingletonMonoBeh662878960.h"
#include "mscorlib_System_DateTime339033936.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PlayFab.Internal.PlayFabHTTP
struct  PlayFabHTTP_t1187660387  : public SingletonMonoBehaviour_1_t662878960
{
public:
	// System.Collections.Generic.Queue`1<PlayFab.CallRequestContainer> PlayFab.Internal.PlayFabHTTP::_tempActions
	Queue_1_t2140406149 * ____tempActions_11;

public:
	inline static int32_t get_offset_of__tempActions_11() { return static_cast<int32_t>(offsetof(PlayFabHTTP_t1187660387, ____tempActions_11)); }
	inline Queue_1_t2140406149 * get__tempActions_11() const { return ____tempActions_11; }
	inline Queue_1_t2140406149 ** get_address_of__tempActions_11() { return &____tempActions_11; }
	inline void set__tempActions_11(Queue_1_t2140406149 * value)
	{
		____tempActions_11 = value;
		Il2CppCodeGenWriteBarrier(&____tempActions_11, value);
	}
};

struct PlayFabHTTP_t1187660387_StaticFields
{
public:
	// System.Int32 PlayFab.Internal.PlayFabHTTP::callIdGen
	int32_t ___callIdGen_4;
	// System.Threading.Thread PlayFab.Internal.PlayFabHTTP::_requestQueueThread
	Thread_t1674723085 * ____requestQueueThread_5;
	// System.DateTime PlayFab.Internal.PlayFabHTTP::_threadKillTime
	DateTime_t339033936  ____threadKillTime_6;
	// System.Object PlayFab.Internal.PlayFabHTTP::ThreadLock
	Il2CppObject * ___ThreadLock_7;
	// System.Collections.Generic.List`1<PlayFab.CallRequestContainer> PlayFab.Internal.PlayFabHTTP::ActiveRequests
	List_1_t1229277578 * ___ActiveRequests_8;
	// System.Collections.Generic.Queue`1<PlayFab.CallRequestContainer> PlayFab.Internal.PlayFabHTTP::ResultQueue
	Queue_1_t2140406149 * ___ResultQueue_9;
	// System.Int32 PlayFab.Internal.PlayFabHTTP::_pendingWwwMessages
	int32_t ____pendingWwwMessages_10;

public:
	inline static int32_t get_offset_of_callIdGen_4() { return static_cast<int32_t>(offsetof(PlayFabHTTP_t1187660387_StaticFields, ___callIdGen_4)); }
	inline int32_t get_callIdGen_4() const { return ___callIdGen_4; }
	inline int32_t* get_address_of_callIdGen_4() { return &___callIdGen_4; }
	inline void set_callIdGen_4(int32_t value)
	{
		___callIdGen_4 = value;
	}

	inline static int32_t get_offset_of__requestQueueThread_5() { return static_cast<int32_t>(offsetof(PlayFabHTTP_t1187660387_StaticFields, ____requestQueueThread_5)); }
	inline Thread_t1674723085 * get__requestQueueThread_5() const { return ____requestQueueThread_5; }
	inline Thread_t1674723085 ** get_address_of__requestQueueThread_5() { return &____requestQueueThread_5; }
	inline void set__requestQueueThread_5(Thread_t1674723085 * value)
	{
		____requestQueueThread_5 = value;
		Il2CppCodeGenWriteBarrier(&____requestQueueThread_5, value);
	}

	inline static int32_t get_offset_of__threadKillTime_6() { return static_cast<int32_t>(offsetof(PlayFabHTTP_t1187660387_StaticFields, ____threadKillTime_6)); }
	inline DateTime_t339033936  get__threadKillTime_6() const { return ____threadKillTime_6; }
	inline DateTime_t339033936 * get_address_of__threadKillTime_6() { return &____threadKillTime_6; }
	inline void set__threadKillTime_6(DateTime_t339033936  value)
	{
		____threadKillTime_6 = value;
	}

	inline static int32_t get_offset_of_ThreadLock_7() { return static_cast<int32_t>(offsetof(PlayFabHTTP_t1187660387_StaticFields, ___ThreadLock_7)); }
	inline Il2CppObject * get_ThreadLock_7() const { return ___ThreadLock_7; }
	inline Il2CppObject ** get_address_of_ThreadLock_7() { return &___ThreadLock_7; }
	inline void set_ThreadLock_7(Il2CppObject * value)
	{
		___ThreadLock_7 = value;
		Il2CppCodeGenWriteBarrier(&___ThreadLock_7, value);
	}

	inline static int32_t get_offset_of_ActiveRequests_8() { return static_cast<int32_t>(offsetof(PlayFabHTTP_t1187660387_StaticFields, ___ActiveRequests_8)); }
	inline List_1_t1229277578 * get_ActiveRequests_8() const { return ___ActiveRequests_8; }
	inline List_1_t1229277578 ** get_address_of_ActiveRequests_8() { return &___ActiveRequests_8; }
	inline void set_ActiveRequests_8(List_1_t1229277578 * value)
	{
		___ActiveRequests_8 = value;
		Il2CppCodeGenWriteBarrier(&___ActiveRequests_8, value);
	}

	inline static int32_t get_offset_of_ResultQueue_9() { return static_cast<int32_t>(offsetof(PlayFabHTTP_t1187660387_StaticFields, ___ResultQueue_9)); }
	inline Queue_1_t2140406149 * get_ResultQueue_9() const { return ___ResultQueue_9; }
	inline Queue_1_t2140406149 ** get_address_of_ResultQueue_9() { return &___ResultQueue_9; }
	inline void set_ResultQueue_9(Queue_1_t2140406149 * value)
	{
		___ResultQueue_9 = value;
		Il2CppCodeGenWriteBarrier(&___ResultQueue_9, value);
	}

	inline static int32_t get_offset_of__pendingWwwMessages_10() { return static_cast<int32_t>(offsetof(PlayFabHTTP_t1187660387_StaticFields, ____pendingWwwMessages_10)); }
	inline int32_t get__pendingWwwMessages_10() const { return ____pendingWwwMessages_10; }
	inline int32_t* get_address_of__pendingWwwMessages_10() { return &____pendingWwwMessages_10; }
	inline void set__pendingWwwMessages_10(int32_t value)
	{
		____pendingWwwMessages_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
