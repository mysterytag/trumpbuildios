﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.NeverObservable`1<System.Object>
struct NeverObservable_1_t958614430;
// System.IDisposable
struct IDisposable_t1628921374;
// UniRx.IObserver`1<System.Object>
struct IObserver_1_t3049105323;

#include "codegen/il2cpp-codegen.h"

// System.Void UniRx.Operators.NeverObservable`1<System.Object>::.ctor()
extern "C"  void NeverObservable_1__ctor_m206993564_gshared (NeverObservable_1_t958614430 * __this, const MethodInfo* method);
#define NeverObservable_1__ctor_m206993564(__this, method) ((  void (*) (NeverObservable_1_t958614430 *, const MethodInfo*))NeverObservable_1__ctor_m206993564_gshared)(__this, method)
// System.IDisposable UniRx.Operators.NeverObservable`1<System.Object>::SubscribeCore(UniRx.IObserver`1<T>,System.IDisposable)
extern "C"  Il2CppObject * NeverObservable_1_SubscribeCore_m2729017090_gshared (NeverObservable_1_t958614430 * __this, Il2CppObject* ___observer0, Il2CppObject * ___cancel1, const MethodInfo* method);
#define NeverObservable_1_SubscribeCore_m2729017090(__this, ___observer0, ___cancel1, method) ((  Il2CppObject * (*) (NeverObservable_1_t958614430 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))NeverObservable_1_SubscribeCore_m2729017090_gshared)(__this, ___observer0, ___cancel1, method)
