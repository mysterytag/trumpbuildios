﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Func`2<System.Type,System.String>
struct Func_2_t1392394819;
// System.Collections.Generic.Dictionary`2<System.String,System.Int32>
struct Dictionary_2_t190145395;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ModestTree.TypeExtensions
struct  TypeExtensions_t3479487268  : public Il2CppObject
{
public:

public:
};

struct TypeExtensions_t3479487268_StaticFields
{
public:
	// System.Func`2<System.Type,System.String> ModestTree.TypeExtensions::<>f__am$cache0
	Func_2_t1392394819 * ___U3CU3Ef__amU24cache0_0;
	// System.Func`2<System.Type,System.String> ModestTree.TypeExtensions::<>f__am$cache1
	Func_2_t1392394819 * ___U3CU3Ef__amU24cache1_1;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> ModestTree.TypeExtensions::<>f__switch$mapE
	Dictionary_2_t190145395 * ___U3CU3Ef__switchU24mapE_2;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_0() { return static_cast<int32_t>(offsetof(TypeExtensions_t3479487268_StaticFields, ___U3CU3Ef__amU24cache0_0)); }
	inline Func_2_t1392394819 * get_U3CU3Ef__amU24cache0_0() const { return ___U3CU3Ef__amU24cache0_0; }
	inline Func_2_t1392394819 ** get_address_of_U3CU3Ef__amU24cache0_0() { return &___U3CU3Ef__amU24cache0_0; }
	inline void set_U3CU3Ef__amU24cache0_0(Func_2_t1392394819 * value)
	{
		___U3CU3Ef__amU24cache0_0 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache0_0, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache1_1() { return static_cast<int32_t>(offsetof(TypeExtensions_t3479487268_StaticFields, ___U3CU3Ef__amU24cache1_1)); }
	inline Func_2_t1392394819 * get_U3CU3Ef__amU24cache1_1() const { return ___U3CU3Ef__amU24cache1_1; }
	inline Func_2_t1392394819 ** get_address_of_U3CU3Ef__amU24cache1_1() { return &___U3CU3Ef__amU24cache1_1; }
	inline void set_U3CU3Ef__amU24cache1_1(Func_2_t1392394819 * value)
	{
		___U3CU3Ef__amU24cache1_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache1_1, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__switchU24mapE_2() { return static_cast<int32_t>(offsetof(TypeExtensions_t3479487268_StaticFields, ___U3CU3Ef__switchU24mapE_2)); }
	inline Dictionary_2_t190145395 * get_U3CU3Ef__switchU24mapE_2() const { return ___U3CU3Ef__switchU24mapE_2; }
	inline Dictionary_2_t190145395 ** get_address_of_U3CU3Ef__switchU24mapE_2() { return &___U3CU3Ef__switchU24mapE_2; }
	inline void set_U3CU3Ef__switchU24mapE_2(Dictionary_2_t190145395 * value)
	{
		___U3CU3Ef__switchU24mapE_2 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__switchU24mapE_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
