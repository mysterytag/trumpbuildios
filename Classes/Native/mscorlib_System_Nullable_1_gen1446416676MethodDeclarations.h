﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Object
struct Il2CppObject;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Nullable_1_gen1446416676.h"
#include "mscorlib_System_Object837106420.h"

// System.Void System.Nullable`1<System.SByte>::.ctor(T)
extern "C"  void Nullable_1__ctor_m949022853_gshared (Nullable_1_t1446416676 * __this, int8_t ___value0, const MethodInfo* method);
#define Nullable_1__ctor_m949022853(__this, ___value0, method) ((  void (*) (Nullable_1_t1446416676 *, int8_t, const MethodInfo*))Nullable_1__ctor_m949022853_gshared)(__this, ___value0, method)
// System.Boolean System.Nullable`1<System.SByte>::get_HasValue()
extern "C"  bool Nullable_1_get_HasValue_m1660015292_gshared (Nullable_1_t1446416676 * __this, const MethodInfo* method);
#define Nullable_1_get_HasValue_m1660015292(__this, method) ((  bool (*) (Nullable_1_t1446416676 *, const MethodInfo*))Nullable_1_get_HasValue_m1660015292_gshared)(__this, method)
// T System.Nullable`1<System.SByte>::get_Value()
extern "C"  int8_t Nullable_1_get_Value_m802794946_gshared (Nullable_1_t1446416676 * __this, const MethodInfo* method);
#define Nullable_1_get_Value_m802794946(__this, method) ((  int8_t (*) (Nullable_1_t1446416676 *, const MethodInfo*))Nullable_1_get_Value_m802794946_gshared)(__this, method)
// System.Boolean System.Nullable`1<System.SByte>::Equals(System.Object)
extern "C"  bool Nullable_1_Equals_m3964676304_gshared (Nullable_1_t1446416676 * __this, Il2CppObject * ___other0, const MethodInfo* method);
#define Nullable_1_Equals_m3964676304(__this, ___other0, method) ((  bool (*) (Nullable_1_t1446416676 *, Il2CppObject *, const MethodInfo*))Nullable_1_Equals_m3964676304_gshared)(__this, ___other0, method)
// System.Boolean System.Nullable`1<System.SByte>::Equals(System.Nullable`1<T>)
extern "C"  bool Nullable_1_Equals_m2200494191_gshared (Nullable_1_t1446416676 * __this, Nullable_1_t1446416676  ___other0, const MethodInfo* method);
#define Nullable_1_Equals_m2200494191(__this, ___other0, method) ((  bool (*) (Nullable_1_t1446416676 *, Nullable_1_t1446416676 , const MethodInfo*))Nullable_1_Equals_m2200494191_gshared)(__this, ___other0, method)
// System.Int32 System.Nullable`1<System.SByte>::GetHashCode()
extern "C"  int32_t Nullable_1_GetHashCode_m784368168_gshared (Nullable_1_t1446416676 * __this, const MethodInfo* method);
#define Nullable_1_GetHashCode_m784368168(__this, method) ((  int32_t (*) (Nullable_1_t1446416676 *, const MethodInfo*))Nullable_1_GetHashCode_m784368168_gshared)(__this, method)
// T System.Nullable`1<System.SByte>::GetValueOrDefault()
extern "C"  int8_t Nullable_1_GetValueOrDefault_m707265795_gshared (Nullable_1_t1446416676 * __this, const MethodInfo* method);
#define Nullable_1_GetValueOrDefault_m707265795(__this, method) ((  int8_t (*) (Nullable_1_t1446416676 *, const MethodInfo*))Nullable_1_GetValueOrDefault_m707265795_gshared)(__this, method)
// System.String System.Nullable`1<System.SByte>::ToString()
extern "C"  String_t* Nullable_1_ToString_m2659803696_gshared (Nullable_1_t1446416676 * __this, const MethodInfo* method);
#define Nullable_1_ToString_m2659803696(__this, method) ((  String_t* (*) (Nullable_1_t1446416676 *, const MethodInfo*))Nullable_1_ToString_m2659803696_gshared)(__this, method)
// System.Nullable`1<T> System.Nullable`1<System.SByte>::op_Implicit(T)
extern "C"  Nullable_1_t1446416676  Nullable_1_op_Implicit_m3643449141_gshared (Il2CppObject * __this /* static, unused */, int8_t ___value0, const MethodInfo* method);
#define Nullable_1_op_Implicit_m3643449141(__this /* static, unused */, ___value0, method) ((  Nullable_1_t1446416676  (*) (Il2CppObject * /* static, unused */, int8_t, const MethodInfo*))Nullable_1_op_Implicit_m3643449141_gshared)(__this /* static, unused */, ___value0, method)
