﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.UnityEventExtensions/<AsObservable>c__AnonStorey96`1<System.Single>
struct U3CAsObservableU3Ec__AnonStorey96_1_t355892394;
// UnityEngine.Events.UnityAction`1<System.Single>
struct UnityAction_1_t938670926;

#include "codegen/il2cpp-codegen.h"

// System.Void UniRx.UnityEventExtensions/<AsObservable>c__AnonStorey96`1<System.Single>::.ctor()
extern "C"  void U3CAsObservableU3Ec__AnonStorey96_1__ctor_m1415301486_gshared (U3CAsObservableU3Ec__AnonStorey96_1_t355892394 * __this, const MethodInfo* method);
#define U3CAsObservableU3Ec__AnonStorey96_1__ctor_m1415301486(__this, method) ((  void (*) (U3CAsObservableU3Ec__AnonStorey96_1_t355892394 *, const MethodInfo*))U3CAsObservableU3Ec__AnonStorey96_1__ctor_m1415301486_gshared)(__this, method)
// System.Void UniRx.UnityEventExtensions/<AsObservable>c__AnonStorey96`1<System.Single>::<>m__CA(UnityEngine.Events.UnityAction`1<T>)
extern "C"  void U3CAsObservableU3Ec__AnonStorey96_1_U3CU3Em__CA_m230635509_gshared (U3CAsObservableU3Ec__AnonStorey96_1_t355892394 * __this, UnityAction_1_t938670926 * ___h0, const MethodInfo* method);
#define U3CAsObservableU3Ec__AnonStorey96_1_U3CU3Em__CA_m230635509(__this, ___h0, method) ((  void (*) (U3CAsObservableU3Ec__AnonStorey96_1_t355892394 *, UnityAction_1_t938670926 *, const MethodInfo*))U3CAsObservableU3Ec__AnonStorey96_1_U3CU3Em__CA_m230635509_gshared)(__this, ___h0, method)
// System.Void UniRx.UnityEventExtensions/<AsObservable>c__AnonStorey96`1<System.Single>::<>m__CB(UnityEngine.Events.UnityAction`1<T>)
extern "C"  void U3CAsObservableU3Ec__AnonStorey96_1_U3CU3Em__CB_m1863439508_gshared (U3CAsObservableU3Ec__AnonStorey96_1_t355892394 * __this, UnityAction_1_t938670926 * ___h0, const MethodInfo* method);
#define U3CAsObservableU3Ec__AnonStorey96_1_U3CU3Em__CB_m1863439508(__this, ___h0, method) ((  void (*) (U3CAsObservableU3Ec__AnonStorey96_1_t355892394 *, UnityAction_1_t938670926 *, const MethodInfo*))U3CAsObservableU3Ec__AnonStorey96_1_U3CU3Em__CB_m1863439508_gshared)(__this, ___h0, method)
