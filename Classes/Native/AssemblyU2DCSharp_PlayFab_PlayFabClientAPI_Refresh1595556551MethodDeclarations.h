﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.PlayFabClientAPI/RefreshPSNAuthTokenResponseCallback
struct RefreshPSNAuthTokenResponseCallback_t1595556551;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;
// PlayFab.ClientModels.RefreshPSNAuthTokenRequest
struct RefreshPSNAuthTokenRequest_t4115941982;
// PlayFab.ClientModels.EmptyResult
struct EmptyResult_t1985806266;
// PlayFab.PlayFabError
struct PlayFabError_t750598646;
// System.IAsyncResult
struct IAsyncResult_t537683269;
// System.AsyncCallback
struct AsyncCallback_t1363551830;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_IntPtr676692020.h"
#include "mscorlib_System_String968488902.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_RefreshPSNA4115941982.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_EmptyResult1985806266.h"
#include "AssemblyU2DCSharpU2Dfirstpass_PlayFab_PlayFabError750598646.h"
#include "mscorlib_System_AsyncCallback1363551830.h"

// System.Void PlayFab.PlayFabClientAPI/RefreshPSNAuthTokenResponseCallback::.ctor(System.Object,System.IntPtr)
extern "C"  void RefreshPSNAuthTokenResponseCallback__ctor_m4093530070 (RefreshPSNAuthTokenResponseCallback_t1595556551 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/RefreshPSNAuthTokenResponseCallback::Invoke(System.String,System.Int32,PlayFab.ClientModels.RefreshPSNAuthTokenRequest,PlayFab.ClientModels.EmptyResult,PlayFab.PlayFabError,System.Object)
extern "C"  void RefreshPSNAuthTokenResponseCallback_Invoke_m171649647 (RefreshPSNAuthTokenResponseCallback_t1595556551 * __this, String_t* ___urlPath0, int32_t ___callId1, RefreshPSNAuthTokenRequest_t4115941982 * ___request2, EmptyResult_t1985806266 * ___result3, PlayFabError_t750598646 * ___error4, Il2CppObject * ___customData5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
extern "C" void pinvoke_delegate_wrapper_RefreshPSNAuthTokenResponseCallback_t1595556551(Il2CppObject* delegate, String_t* ___urlPath0, int32_t ___callId1, RefreshPSNAuthTokenRequest_t4115941982 * ___request2, EmptyResult_t1985806266 * ___result3, PlayFabError_t750598646 * ___error4, Il2CppObject * ___customData5);
// System.IAsyncResult PlayFab.PlayFabClientAPI/RefreshPSNAuthTokenResponseCallback::BeginInvoke(System.String,System.Int32,PlayFab.ClientModels.RefreshPSNAuthTokenRequest,PlayFab.ClientModels.EmptyResult,PlayFab.PlayFabError,System.Object,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * RefreshPSNAuthTokenResponseCallback_BeginInvoke_m2003877644 (RefreshPSNAuthTokenResponseCallback_t1595556551 * __this, String_t* ___urlPath0, int32_t ___callId1, RefreshPSNAuthTokenRequest_t4115941982 * ___request2, EmptyResult_t1985806266 * ___result3, PlayFabError_t750598646 * ___error4, Il2CppObject * ___customData5, AsyncCallback_t1363551830 * ___callback6, Il2CppObject * ___object7, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/RefreshPSNAuthTokenResponseCallback::EndInvoke(System.IAsyncResult)
extern "C"  void RefreshPSNAuthTokenResponseCallback_EndInvoke_m4201585894 (RefreshPSNAuthTokenResponseCallback_t1595556551 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
