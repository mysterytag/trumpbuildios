﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.PlayFabClientAPI/LinkGoogleAccountResponseCallback
struct LinkGoogleAccountResponseCallback_t1554866048;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;
// PlayFab.ClientModels.LinkGoogleAccountRequest
struct LinkGoogleAccountRequest_t117459333;
// PlayFab.ClientModels.LinkGoogleAccountResult
struct LinkGoogleAccountResult_t1008768135;
// PlayFab.PlayFabError
struct PlayFabError_t750598646;
// System.IAsyncResult
struct IAsyncResult_t537683269;
// System.AsyncCallback
struct AsyncCallback_t1363551830;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_IntPtr676692020.h"
#include "mscorlib_System_String968488902.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_LinkGoogleAc117459333.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_LinkGoogleA1008768135.h"
#include "AssemblyU2DCSharpU2Dfirstpass_PlayFab_PlayFabError750598646.h"
#include "mscorlib_System_AsyncCallback1363551830.h"

// System.Void PlayFab.PlayFabClientAPI/LinkGoogleAccountResponseCallback::.ctor(System.Object,System.IntPtr)
extern "C"  void LinkGoogleAccountResponseCallback__ctor_m4117739535 (LinkGoogleAccountResponseCallback_t1554866048 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/LinkGoogleAccountResponseCallback::Invoke(System.String,System.Int32,PlayFab.ClientModels.LinkGoogleAccountRequest,PlayFab.ClientModels.LinkGoogleAccountResult,PlayFab.PlayFabError,System.Object)
extern "C"  void LinkGoogleAccountResponseCallback_Invoke_m1412294254 (LinkGoogleAccountResponseCallback_t1554866048 * __this, String_t* ___urlPath0, int32_t ___callId1, LinkGoogleAccountRequest_t117459333 * ___request2, LinkGoogleAccountResult_t1008768135 * ___result3, PlayFabError_t750598646 * ___error4, Il2CppObject * ___customData5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
extern "C" void pinvoke_delegate_wrapper_LinkGoogleAccountResponseCallback_t1554866048(Il2CppObject* delegate, String_t* ___urlPath0, int32_t ___callId1, LinkGoogleAccountRequest_t117459333 * ___request2, LinkGoogleAccountResult_t1008768135 * ___result3, PlayFabError_t750598646 * ___error4, Il2CppObject * ___customData5);
// System.IAsyncResult PlayFab.PlayFabClientAPI/LinkGoogleAccountResponseCallback::BeginInvoke(System.String,System.Int32,PlayFab.ClientModels.LinkGoogleAccountRequest,PlayFab.ClientModels.LinkGoogleAccountResult,PlayFab.PlayFabError,System.Object,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * LinkGoogleAccountResponseCallback_BeginInvoke_m668995327 (LinkGoogleAccountResponseCallback_t1554866048 * __this, String_t* ___urlPath0, int32_t ___callId1, LinkGoogleAccountRequest_t117459333 * ___request2, LinkGoogleAccountResult_t1008768135 * ___result3, PlayFabError_t750598646 * ___error4, Il2CppObject * ___customData5, AsyncCallback_t1363551830 * ___callback6, Il2CppObject * ___object7, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/LinkGoogleAccountResponseCallback::EndInvoke(System.IAsyncResult)
extern "C"  void LinkGoogleAccountResponseCallback_EndInvoke_m1808239007 (LinkGoogleAccountResponseCallback_t1554866048 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
