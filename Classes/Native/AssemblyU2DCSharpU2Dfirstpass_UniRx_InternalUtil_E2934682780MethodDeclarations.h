﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.InternalUtil.EmptyObserver`1<UnityEngine.Vector4>
struct EmptyObserver_1_t2934682780;
// System.Exception
struct Exception_t1967233988;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Exception1967233988.h"
#include "UnityEngine_UnityEngine_Vector43525329790.h"

// System.Void UniRx.InternalUtil.EmptyObserver`1<UnityEngine.Vector4>::.ctor()
extern "C"  void EmptyObserver_1__ctor_m630117492_gshared (EmptyObserver_1_t2934682780 * __this, const MethodInfo* method);
#define EmptyObserver_1__ctor_m630117492(__this, method) ((  void (*) (EmptyObserver_1_t2934682780 *, const MethodInfo*))EmptyObserver_1__ctor_m630117492_gshared)(__this, method)
// System.Void UniRx.InternalUtil.EmptyObserver`1<UnityEngine.Vector4>::.cctor()
extern "C"  void EmptyObserver_1__cctor_m1871676857_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define EmptyObserver_1__cctor_m1871676857(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))EmptyObserver_1__cctor_m1871676857_gshared)(__this /* static, unused */, method)
// System.Void UniRx.InternalUtil.EmptyObserver`1<UnityEngine.Vector4>::OnCompleted()
extern "C"  void EmptyObserver_1_OnCompleted_m1432215806_gshared (EmptyObserver_1_t2934682780 * __this, const MethodInfo* method);
#define EmptyObserver_1_OnCompleted_m1432215806(__this, method) ((  void (*) (EmptyObserver_1_t2934682780 *, const MethodInfo*))EmptyObserver_1_OnCompleted_m1432215806_gshared)(__this, method)
// System.Void UniRx.InternalUtil.EmptyObserver`1<UnityEngine.Vector4>::OnError(System.Exception)
extern "C"  void EmptyObserver_1_OnError_m2587428139_gshared (EmptyObserver_1_t2934682780 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method);
#define EmptyObserver_1_OnError_m2587428139(__this, ___error0, method) ((  void (*) (EmptyObserver_1_t2934682780 *, Exception_t1967233988 *, const MethodInfo*))EmptyObserver_1_OnError_m2587428139_gshared)(__this, ___error0, method)
// System.Void UniRx.InternalUtil.EmptyObserver`1<UnityEngine.Vector4>::OnNext(T)
extern "C"  void EmptyObserver_1_OnNext_m2783224348_gshared (EmptyObserver_1_t2934682780 * __this, Vector4_t3525329790  ___value0, const MethodInfo* method);
#define EmptyObserver_1_OnNext_m2783224348(__this, ___value0, method) ((  void (*) (EmptyObserver_1_t2934682780 *, Vector4_t3525329790 , const MethodInfo*))EmptyObserver_1_OnNext_m2783224348_gshared)(__this, ___value0, method)
