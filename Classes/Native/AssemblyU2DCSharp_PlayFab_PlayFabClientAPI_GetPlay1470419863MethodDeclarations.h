﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.PlayFabClientAPI/GetPlayFabIDsFromPSNAccountIDsResponseCallback
struct GetPlayFabIDsFromPSNAccountIDsResponseCallback_t1470419863;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;
// PlayFab.ClientModels.GetPlayFabIDsFromPSNAccountIDsRequest
struct GetPlayFabIDsFromPSNAccountIDsRequest_t3305934734;
// PlayFab.ClientModels.GetPlayFabIDsFromPSNAccountIDsResult
struct GetPlayFabIDsFromPSNAccountIDsResult_t3051284830;
// PlayFab.PlayFabError
struct PlayFabError_t750598646;
// System.IAsyncResult
struct IAsyncResult_t537683269;
// System.AsyncCallback
struct AsyncCallback_t1363551830;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_IntPtr676692020.h"
#include "mscorlib_System_String968488902.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_GetPlayFabI3305934734.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_GetPlayFabI3051284830.h"
#include "AssemblyU2DCSharpU2Dfirstpass_PlayFab_PlayFabError750598646.h"
#include "mscorlib_System_AsyncCallback1363551830.h"

// System.Void PlayFab.PlayFabClientAPI/GetPlayFabIDsFromPSNAccountIDsResponseCallback::.ctor(System.Object,System.IntPtr)
extern "C"  void GetPlayFabIDsFromPSNAccountIDsResponseCallback__ctor_m2969714950 (GetPlayFabIDsFromPSNAccountIDsResponseCallback_t1470419863 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/GetPlayFabIDsFromPSNAccountIDsResponseCallback::Invoke(System.String,System.Int32,PlayFab.ClientModels.GetPlayFabIDsFromPSNAccountIDsRequest,PlayFab.ClientModels.GetPlayFabIDsFromPSNAccountIDsResult,PlayFab.PlayFabError,System.Object)
extern "C"  void GetPlayFabIDsFromPSNAccountIDsResponseCallback_Invoke_m2771471283 (GetPlayFabIDsFromPSNAccountIDsResponseCallback_t1470419863 * __this, String_t* ___urlPath0, int32_t ___callId1, GetPlayFabIDsFromPSNAccountIDsRequest_t3305934734 * ___request2, GetPlayFabIDsFromPSNAccountIDsResult_t3051284830 * ___result3, PlayFabError_t750598646 * ___error4, Il2CppObject * ___customData5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
extern "C" void pinvoke_delegate_wrapper_GetPlayFabIDsFromPSNAccountIDsResponseCallback_t1470419863(Il2CppObject* delegate, String_t* ___urlPath0, int32_t ___callId1, GetPlayFabIDsFromPSNAccountIDsRequest_t3305934734 * ___request2, GetPlayFabIDsFromPSNAccountIDsResult_t3051284830 * ___result3, PlayFabError_t750598646 * ___error4, Il2CppObject * ___customData5);
// System.IAsyncResult PlayFab.PlayFabClientAPI/GetPlayFabIDsFromPSNAccountIDsResponseCallback::BeginInvoke(System.String,System.Int32,PlayFab.ClientModels.GetPlayFabIDsFromPSNAccountIDsRequest,PlayFab.ClientModels.GetPlayFabIDsFromPSNAccountIDsResult,PlayFab.PlayFabError,System.Object,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * GetPlayFabIDsFromPSNAccountIDsResponseCallback_BeginInvoke_m2782043552 (GetPlayFabIDsFromPSNAccountIDsResponseCallback_t1470419863 * __this, String_t* ___urlPath0, int32_t ___callId1, GetPlayFabIDsFromPSNAccountIDsRequest_t3305934734 * ___request2, GetPlayFabIDsFromPSNAccountIDsResult_t3051284830 * ___result3, PlayFabError_t750598646 * ___error4, Il2CppObject * ___customData5, AsyncCallback_t1363551830 * ___callback6, Il2CppObject * ___object7, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/GetPlayFabIDsFromPSNAccountIDsResponseCallback::EndInvoke(System.IAsyncResult)
extern "C"  void GetPlayFabIDsFromPSNAccountIDsResponseCallback_EndInvoke_m3690669590 (GetPlayFabIDsFromPSNAccountIDsResponseCallback_t1470419863 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
