﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "UnityEngine_UnityEngine_MonoBehaviour3012272455.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LocalizedTextSetter
struct  LocalizedTextSetter_t1100117447  : public MonoBehaviour_t3012272455
{
public:
	// System.String LocalizedTextSetter::stringID
	String_t* ___stringID_2;
	// System.Int32 LocalizedTextSetter::stringLength
	int32_t ___stringLength_3;

public:
	inline static int32_t get_offset_of_stringID_2() { return static_cast<int32_t>(offsetof(LocalizedTextSetter_t1100117447, ___stringID_2)); }
	inline String_t* get_stringID_2() const { return ___stringID_2; }
	inline String_t** get_address_of_stringID_2() { return &___stringID_2; }
	inline void set_stringID_2(String_t* value)
	{
		___stringID_2 = value;
		Il2CppCodeGenWriteBarrier(&___stringID_2, value);
	}

	inline static int32_t get_offset_of_stringLength_3() { return static_cast<int32_t>(offsetof(LocalizedTextSetter_t1100117447, ___stringLength_3)); }
	inline int32_t get_stringLength_3() const { return ___stringLength_3; }
	inline int32_t* get_address_of_stringLength_3() { return &___stringLength_3; }
	inline void set_stringLength_3(int32_t value)
	{
		___stringLength_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
