﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Comparer`1/DefaultComparer<UniRx.Tuple`2<System.Object,System.Object>>
struct DefaultComparer_t4002182847;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Tuple_2_gen369261819.h"

// System.Void System.Collections.Generic.Comparer`1/DefaultComparer<UniRx.Tuple`2<System.Object,System.Object>>::.ctor()
extern "C"  void DefaultComparer__ctor_m1805439982_gshared (DefaultComparer_t4002182847 * __this, const MethodInfo* method);
#define DefaultComparer__ctor_m1805439982(__this, method) ((  void (*) (DefaultComparer_t4002182847 *, const MethodInfo*))DefaultComparer__ctor_m1805439982_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Comparer`1/DefaultComparer<UniRx.Tuple`2<System.Object,System.Object>>::Compare(T,T)
extern "C"  int32_t DefaultComparer_Compare_m3725633609_gshared (DefaultComparer_t4002182847 * __this, Tuple_2_t369261819  ___x0, Tuple_2_t369261819  ___y1, const MethodInfo* method);
#define DefaultComparer_Compare_m3725633609(__this, ___x0, ___y1, method) ((  int32_t (*) (DefaultComparer_t4002182847 *, Tuple_2_t369261819 , Tuple_2_t369261819 , const MethodInfo*))DefaultComparer_Compare_m3725633609_gshared)(__this, ___x0, ___y1, method)
