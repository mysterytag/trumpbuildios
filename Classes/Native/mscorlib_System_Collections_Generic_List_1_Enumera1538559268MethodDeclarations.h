﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera4014815677MethodDeclarations.h"

// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.UI.LayoutElement>::.ctor(System.Collections.Generic.List`1<T>)
#define Enumerator__ctor_m439042786(__this, ___l0, method) ((  void (*) (Enumerator_t1538559268 *, List_1_t3452776276 *, const MethodInfo*))Enumerator__ctor_m1029849669_gshared)(__this, ___l0, method)
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.UI.LayoutElement>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m4057599024(__this, method) ((  void (*) (Enumerator_t1538559268 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m771996397_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1/Enumerator<UnityEngine.UI.LayoutElement>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m2062379942(__this, method) ((  Il2CppObject * (*) (Enumerator_t1538559268 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m3561903705_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.UI.LayoutElement>::Dispose()
#define Enumerator_Dispose_m2647992647(__this, method) ((  void (*) (Enumerator_t1538559268 *, const MethodInfo*))Enumerator_Dispose_m2904289642_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.UI.LayoutElement>::VerifyState()
#define Enumerator_VerifyState_m1513417984(__this, method) ((  void (*) (Enumerator_t1538559268 *, const MethodInfo*))Enumerator_VerifyState_m1522854819_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<UnityEngine.UI.LayoutElement>::MoveNext()
#define Enumerator_MoveNext_m3080827465(__this, method) ((  bool (*) (Enumerator_t1538559268 *, const MethodInfo*))Enumerator_MoveNext_m302222300_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<UnityEngine.UI.LayoutElement>::get_Current()
#define Enumerator_get_Current_m2633944563(__this, method) ((  LayoutElement_t2655817307 * (*) (Enumerator_t1538559268 *, const MethodInfo*))Enumerator_get_Current_m1113061044_gshared)(__this, method)
