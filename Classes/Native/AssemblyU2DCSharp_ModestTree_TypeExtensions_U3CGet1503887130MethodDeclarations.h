﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ModestTree.TypeExtensions/<GetParentTypes>c__Iterator3E
struct U3CGetParentTypesU3Ec__Iterator3E_t1503887130;
// System.Type
struct Type_t;
// System.Object
struct Il2CppObject;
// System.Collections.IEnumerator
struct IEnumerator_t287207039;
// System.Collections.Generic.IEnumerator`1<System.Type>
struct IEnumerator_1_t4262336383;

#include "codegen/il2cpp-codegen.h"

// System.Void ModestTree.TypeExtensions/<GetParentTypes>c__Iterator3E::.ctor()
extern "C"  void U3CGetParentTypesU3Ec__Iterator3E__ctor_m2754063334 (U3CGetParentTypesU3Ec__Iterator3E_t1503887130 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type ModestTree.TypeExtensions/<GetParentTypes>c__Iterator3E::System.Collections.Generic.IEnumerator<System.Type>.get_Current()
extern "C"  Type_t * U3CGetParentTypesU3Ec__Iterator3E_System_Collections_Generic_IEnumeratorU3CSystem_TypeU3E_get_Current_m3890211299 (U3CGetParentTypesU3Ec__Iterator3E_t1503887130 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object ModestTree.TypeExtensions/<GetParentTypes>c__Iterator3E::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CGetParentTypesU3Ec__Iterator3E_System_Collections_IEnumerator_get_Current_m2620388800 (U3CGetParentTypesU3Ec__Iterator3E_t1503887130 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator ModestTree.TypeExtensions/<GetParentTypes>c__Iterator3E::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * U3CGetParentTypesU3Ec__Iterator3E_System_Collections_IEnumerable_GetEnumerator_m2094940347 (U3CGetParentTypesU3Ec__Iterator3E_t1503887130 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.IEnumerator`1<System.Type> ModestTree.TypeExtensions/<GetParentTypes>c__Iterator3E::System.Collections.Generic.IEnumerable<System.Type>.GetEnumerator()
extern "C"  Il2CppObject* U3CGetParentTypesU3Ec__Iterator3E_System_Collections_Generic_IEnumerableU3CSystem_TypeU3E_GetEnumerator_m2282256272 (U3CGetParentTypesU3Ec__Iterator3E_t1503887130 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ModestTree.TypeExtensions/<GetParentTypes>c__Iterator3E::MoveNext()
extern "C"  bool U3CGetParentTypesU3Ec__Iterator3E_MoveNext_m1110604878 (U3CGetParentTypesU3Ec__Iterator3E_t1503887130 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ModestTree.TypeExtensions/<GetParentTypes>c__Iterator3E::Dispose()
extern "C"  void U3CGetParentTypesU3Ec__Iterator3E_Dispose_m853292707 (U3CGetParentTypesU3Ec__Iterator3E_t1503887130 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ModestTree.TypeExtensions/<GetParentTypes>c__Iterator3E::Reset()
extern "C"  void U3CGetParentTypesU3Ec__Iterator3E_Reset_m400496275 (U3CGetParentTypesU3Ec__Iterator3E_t1503887130 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
