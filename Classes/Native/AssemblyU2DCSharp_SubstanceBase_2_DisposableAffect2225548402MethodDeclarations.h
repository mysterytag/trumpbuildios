﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SubstanceBase`2/DisposableAffect<System.Boolean,System.Boolean>
struct DisposableAffect_t2225548402;

#include "codegen/il2cpp-codegen.h"

// System.Void SubstanceBase`2/DisposableAffect<System.Boolean,System.Boolean>::.ctor()
extern "C"  void DisposableAffect__ctor_m1383012526_gshared (DisposableAffect_t2225548402 * __this, const MethodInfo* method);
#define DisposableAffect__ctor_m1383012526(__this, method) ((  void (*) (DisposableAffect_t2225548402 *, const MethodInfo*))DisposableAffect__ctor_m1383012526_gshared)(__this, method)
// System.Void SubstanceBase`2/DisposableAffect<System.Boolean,System.Boolean>::Dispose()
extern "C"  void DisposableAffect_Dispose_m1828426091_gshared (DisposableAffect_t2225548402 * __this, const MethodInfo* method);
#define DisposableAffect_Dispose_m1828426091(__this, method) ((  void (*) (DisposableAffect_t2225548402 *, const MethodInfo*))DisposableAffect_Dispose_m1828426091_gshared)(__this, method)
