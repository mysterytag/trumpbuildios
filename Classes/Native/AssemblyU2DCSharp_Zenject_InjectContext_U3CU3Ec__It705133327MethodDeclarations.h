﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Zenject.InjectContext/<>c__Iterator44
struct U3CU3Ec__Iterator44_t705133327;
// System.Type
struct Type_t;
// System.Object
struct Il2CppObject;
// System.Collections.IEnumerator
struct IEnumerator_t287207039;
// System.Collections.Generic.IEnumerator`1<System.Type>
struct IEnumerator_1_t4262336383;

#include "codegen/il2cpp-codegen.h"

// System.Void Zenject.InjectContext/<>c__Iterator44::.ctor()
extern "C"  void U3CU3Ec__Iterator44__ctor_m199325772 (U3CU3Ec__Iterator44_t705133327 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type Zenject.InjectContext/<>c__Iterator44::System.Collections.Generic.IEnumerator<System.Type>.get_Current()
extern "C"  Type_t * U3CU3Ec__Iterator44_System_Collections_Generic_IEnumeratorU3CSystem_TypeU3E_get_Current_m2344328521 (U3CU3Ec__Iterator44_t705133327 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Zenject.InjectContext/<>c__Iterator44::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CU3Ec__Iterator44_System_Collections_IEnumerator_get_Current_m2045699546 (U3CU3Ec__Iterator44_t705133327 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator Zenject.InjectContext/<>c__Iterator44::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * U3CU3Ec__Iterator44_System_Collections_IEnumerable_GetEnumerator_m796011413 (U3CU3Ec__Iterator44_t705133327 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.IEnumerator`1<System.Type> Zenject.InjectContext/<>c__Iterator44::System.Collections.Generic.IEnumerable<System.Type>.GetEnumerator()
extern "C"  Il2CppObject* U3CU3Ec__Iterator44_System_Collections_Generic_IEnumerableU3CSystem_TypeU3E_GetEnumerator_m584559926 (U3CU3Ec__Iterator44_t705133327 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Zenject.InjectContext/<>c__Iterator44::MoveNext()
extern "C"  bool U3CU3Ec__Iterator44_MoveNext_m4098320680 (U3CU3Ec__Iterator44_t705133327 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Zenject.InjectContext/<>c__Iterator44::Dispose()
extern "C"  void U3CU3Ec__Iterator44_Dispose_m2471788937 (U3CU3Ec__Iterator44_t705133327 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Zenject.InjectContext/<>c__Iterator44::Reset()
extern "C"  void U3CU3Ec__Iterator44_Reset_m2140726009 (U3CU3Ec__Iterator44_t705133327 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
