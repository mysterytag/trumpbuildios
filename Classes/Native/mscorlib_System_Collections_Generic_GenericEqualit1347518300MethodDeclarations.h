﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.GenericEqualityComparer`1<UniRx.CollectionAddEvent`1<System.Object>>
struct GenericEqualityComparer_1_t1347518300;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_CollectionAddE2416521987.h"

// System.Void System.Collections.Generic.GenericEqualityComparer`1<UniRx.CollectionAddEvent`1<System.Object>>::.ctor()
extern "C"  void GenericEqualityComparer_1__ctor_m580759382_gshared (GenericEqualityComparer_1_t1347518300 * __this, const MethodInfo* method);
#define GenericEqualityComparer_1__ctor_m580759382(__this, method) ((  void (*) (GenericEqualityComparer_1_t1347518300 *, const MethodInfo*))GenericEqualityComparer_1__ctor_m580759382_gshared)(__this, method)
// System.Int32 System.Collections.Generic.GenericEqualityComparer`1<UniRx.CollectionAddEvent`1<System.Object>>::GetHashCode(T)
extern "C"  int32_t GenericEqualityComparer_1_GetHashCode_m158970589_gshared (GenericEqualityComparer_1_t1347518300 * __this, CollectionAddEvent_1_t2416521987  ___obj0, const MethodInfo* method);
#define GenericEqualityComparer_1_GetHashCode_m158970589(__this, ___obj0, method) ((  int32_t (*) (GenericEqualityComparer_1_t1347518300 *, CollectionAddEvent_1_t2416521987 , const MethodInfo*))GenericEqualityComparer_1_GetHashCode_m158970589_gshared)(__this, ___obj0, method)
// System.Boolean System.Collections.Generic.GenericEqualityComparer`1<UniRx.CollectionAddEvent`1<System.Object>>::Equals(T,T)
extern "C"  bool GenericEqualityComparer_1_Equals_m403668075_gshared (GenericEqualityComparer_1_t1347518300 * __this, CollectionAddEvent_1_t2416521987  ___x0, CollectionAddEvent_1_t2416521987  ___y1, const MethodInfo* method);
#define GenericEqualityComparer_1_Equals_m403668075(__this, ___x0, ___y1, method) ((  bool (*) (GenericEqualityComparer_1_t1347518300 *, CollectionAddEvent_1_t2416521987 , CollectionAddEvent_1_t2416521987 , const MethodInfo*))GenericEqualityComparer_1_Equals_m403668075_gshared)(__this, ___x0, ___y1, method)
