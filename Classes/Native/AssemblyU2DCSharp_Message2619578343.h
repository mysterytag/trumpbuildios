﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.UI.Image
struct Image_t3354615620;
// UnityEngine.UI.Text
struct Text_t3286458198;
// UniRx.ReactiveCollection`1<UniRx.Tuple`2<System.String,UnityEngine.Sprite>>
struct ReactiveCollection_1_t3392720430;
// UniRx.BoolReactiveProperty
struct BoolReactiveProperty_t3047538250;
// System.Func`3<System.Boolean,System.Int32,<>__AnonType3`2<System.Boolean,System.Int32>>
struct Func_3_t3953209855;
// System.Collections.Generic.Dictionary`2<System.String,System.Int32>
struct Dictionary_2_t190145395;

#include "UnityEngine_UnityEngine_MonoBehaviour3012272455.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Message
struct  Message_t2619578343  : public MonoBehaviour_t3012272455
{
public:
	// UnityEngine.UI.Image Message::Back
	Image_t3354615620 * ___Back_2;
	// UnityEngine.UI.Image Message::Icon
	Image_t3354615620 * ___Icon_3;
	// UnityEngine.UI.Text Message::CenterText
	Text_t3286458198 * ___CenterText_4;
	// UnityEngine.UI.Text Message::IconText
	Text_t3286458198 * ___IconText_5;
	// UniRx.ReactiveCollection`1<UniRx.Tuple`2<System.String,UnityEngine.Sprite>> Message::_messages
	ReactiveCollection_1_t3392720430 * ____messages_6;
	// UniRx.BoolReactiveProperty Message::ShowMessage
	BoolReactiveProperty_t3047538250 * ___ShowMessage_7;

public:
	inline static int32_t get_offset_of_Back_2() { return static_cast<int32_t>(offsetof(Message_t2619578343, ___Back_2)); }
	inline Image_t3354615620 * get_Back_2() const { return ___Back_2; }
	inline Image_t3354615620 ** get_address_of_Back_2() { return &___Back_2; }
	inline void set_Back_2(Image_t3354615620 * value)
	{
		___Back_2 = value;
		Il2CppCodeGenWriteBarrier(&___Back_2, value);
	}

	inline static int32_t get_offset_of_Icon_3() { return static_cast<int32_t>(offsetof(Message_t2619578343, ___Icon_3)); }
	inline Image_t3354615620 * get_Icon_3() const { return ___Icon_3; }
	inline Image_t3354615620 ** get_address_of_Icon_3() { return &___Icon_3; }
	inline void set_Icon_3(Image_t3354615620 * value)
	{
		___Icon_3 = value;
		Il2CppCodeGenWriteBarrier(&___Icon_3, value);
	}

	inline static int32_t get_offset_of_CenterText_4() { return static_cast<int32_t>(offsetof(Message_t2619578343, ___CenterText_4)); }
	inline Text_t3286458198 * get_CenterText_4() const { return ___CenterText_4; }
	inline Text_t3286458198 ** get_address_of_CenterText_4() { return &___CenterText_4; }
	inline void set_CenterText_4(Text_t3286458198 * value)
	{
		___CenterText_4 = value;
		Il2CppCodeGenWriteBarrier(&___CenterText_4, value);
	}

	inline static int32_t get_offset_of_IconText_5() { return static_cast<int32_t>(offsetof(Message_t2619578343, ___IconText_5)); }
	inline Text_t3286458198 * get_IconText_5() const { return ___IconText_5; }
	inline Text_t3286458198 ** get_address_of_IconText_5() { return &___IconText_5; }
	inline void set_IconText_5(Text_t3286458198 * value)
	{
		___IconText_5 = value;
		Il2CppCodeGenWriteBarrier(&___IconText_5, value);
	}

	inline static int32_t get_offset_of__messages_6() { return static_cast<int32_t>(offsetof(Message_t2619578343, ____messages_6)); }
	inline ReactiveCollection_1_t3392720430 * get__messages_6() const { return ____messages_6; }
	inline ReactiveCollection_1_t3392720430 ** get_address_of__messages_6() { return &____messages_6; }
	inline void set__messages_6(ReactiveCollection_1_t3392720430 * value)
	{
		____messages_6 = value;
		Il2CppCodeGenWriteBarrier(&____messages_6, value);
	}

	inline static int32_t get_offset_of_ShowMessage_7() { return static_cast<int32_t>(offsetof(Message_t2619578343, ___ShowMessage_7)); }
	inline BoolReactiveProperty_t3047538250 * get_ShowMessage_7() const { return ___ShowMessage_7; }
	inline BoolReactiveProperty_t3047538250 ** get_address_of_ShowMessage_7() { return &___ShowMessage_7; }
	inline void set_ShowMessage_7(BoolReactiveProperty_t3047538250 * value)
	{
		___ShowMessage_7 = value;
		Il2CppCodeGenWriteBarrier(&___ShowMessage_7, value);
	}
};

struct Message_t2619578343_StaticFields
{
public:
	// System.Func`3<System.Boolean,System.Int32,<>__AnonType3`2<System.Boolean,System.Int32>> Message::<>f__am$cache6
	Func_3_t3953209855 * ___U3CU3Ef__amU24cache6_8;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> Message::<>f__switch$map3
	Dictionary_2_t190145395 * ___U3CU3Ef__switchU24map3_9;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache6_8() { return static_cast<int32_t>(offsetof(Message_t2619578343_StaticFields, ___U3CU3Ef__amU24cache6_8)); }
	inline Func_3_t3953209855 * get_U3CU3Ef__amU24cache6_8() const { return ___U3CU3Ef__amU24cache6_8; }
	inline Func_3_t3953209855 ** get_address_of_U3CU3Ef__amU24cache6_8() { return &___U3CU3Ef__amU24cache6_8; }
	inline void set_U3CU3Ef__amU24cache6_8(Func_3_t3953209855 * value)
	{
		___U3CU3Ef__amU24cache6_8 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache6_8, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__switchU24map3_9() { return static_cast<int32_t>(offsetof(Message_t2619578343_StaticFields, ___U3CU3Ef__switchU24map3_9)); }
	inline Dictionary_2_t190145395 * get_U3CU3Ef__switchU24map3_9() const { return ___U3CU3Ef__switchU24map3_9; }
	inline Dictionary_2_t190145395 ** get_address_of_U3CU3Ef__switchU24map3_9() { return &___U3CU3Ef__switchU24map3_9; }
	inline void set_U3CU3Ef__switchU24map3_9(Dictionary_2_t190145395 * value)
	{
		___U3CU3Ef__switchU24map3_9 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__switchU24map3_9, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
