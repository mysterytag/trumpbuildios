﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.UUnit.JsonUnitTests
struct JsonUnitTests_t1854679231;

#include "codegen/il2cpp-codegen.h"

// System.Void PlayFab.UUnit.JsonUnitTests::.ctor()
extern "C"  void JsonUnitTests__ctor_m2641996614 (JsonUnitTests_t1854679231 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
