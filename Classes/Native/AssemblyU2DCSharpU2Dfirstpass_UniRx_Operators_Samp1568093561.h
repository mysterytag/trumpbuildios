﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UniRx.Operators.SampleFrameObservable`1/SampleFrame<System.Object>
struct SampleFrame_t1580910916;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Operators.SampleFrameObservable`1/SampleFrame/SampleFrameTick<System.Object>
struct  SampleFrameTick_t1568093561  : public Il2CppObject
{
public:
	// UniRx.Operators.SampleFrameObservable`1/SampleFrame<T> UniRx.Operators.SampleFrameObservable`1/SampleFrame/SampleFrameTick::parent
	SampleFrame_t1580910916 * ___parent_0;

public:
	inline static int32_t get_offset_of_parent_0() { return static_cast<int32_t>(offsetof(SampleFrameTick_t1568093561, ___parent_0)); }
	inline SampleFrame_t1580910916 * get_parent_0() const { return ___parent_0; }
	inline SampleFrame_t1580910916 ** get_address_of_parent_0() { return &___parent_0; }
	inline void set_parent_0(SampleFrame_t1580910916 * value)
	{
		___parent_0 = value;
		Il2CppCodeGenWriteBarrier(&___parent_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
