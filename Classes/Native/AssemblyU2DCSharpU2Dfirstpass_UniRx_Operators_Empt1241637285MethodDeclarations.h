﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.EmptyObservable`1<System.Int64>
struct EmptyObservable_1_t1241637285;
// UniRx.IScheduler
struct IScheduler_t2938318244;
// System.IDisposable
struct IDisposable_t1628921374;
// UniRx.IObserver`1<System.Int64>
struct IObserver_1_t764446489;

#include "codegen/il2cpp-codegen.h"

// System.Void UniRx.Operators.EmptyObservable`1<System.Int64>::.ctor(UniRx.IScheduler)
extern "C"  void EmptyObservable_1__ctor_m1509393999_gshared (EmptyObservable_1_t1241637285 * __this, Il2CppObject * ___scheduler0, const MethodInfo* method);
#define EmptyObservable_1__ctor_m1509393999(__this, ___scheduler0, method) ((  void (*) (EmptyObservable_1_t1241637285 *, Il2CppObject *, const MethodInfo*))EmptyObservable_1__ctor_m1509393999_gshared)(__this, ___scheduler0, method)
// System.IDisposable UniRx.Operators.EmptyObservable`1<System.Int64>::SubscribeCore(UniRx.IObserver`1<T>,System.IDisposable)
extern "C"  Il2CppObject * EmptyObservable_1_SubscribeCore_m4227965167_gshared (EmptyObservable_1_t1241637285 * __this, Il2CppObject* ___observer0, Il2CppObject * ___cancel1, const MethodInfo* method);
#define EmptyObservable_1_SubscribeCore_m4227965167(__this, ___observer0, ___cancel1, method) ((  Il2CppObject * (*) (EmptyObservable_1_t1241637285 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))EmptyObservable_1_SubscribeCore_m4227965167_gshared)(__this, ___observer0, ___cancel1, method)
