﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.InternalUtil.ListObserver`1<UniRx.DictionaryRemoveEvent`2<System.Object,System.Object>>
struct ListObserver_1_t3616905962;
// UniRx.InternalUtil.ImmutableList`1<UniRx.IObserver`1<UniRx.DictionaryRemoveEvent`2<System.Object,System.Object>>>
struct ImmutableList_1_t2299524397;
// System.Exception
struct Exception_t1967233988;
// UniRx.IObserver`1<UniRx.DictionaryRemoveEvent`2<System.Object,System.Object>>
struct IObserver_1_t1239319898;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Exception1967233988.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_DictionaryRemo3322288291.h"

// System.Void UniRx.InternalUtil.ListObserver`1<UniRx.DictionaryRemoveEvent`2<System.Object,System.Object>>::.ctor(UniRx.InternalUtil.ImmutableList`1<UniRx.IObserver`1<T>>)
extern "C"  void ListObserver_1__ctor_m1993395291_gshared (ListObserver_1_t3616905962 * __this, ImmutableList_1_t2299524397 * ___observers0, const MethodInfo* method);
#define ListObserver_1__ctor_m1993395291(__this, ___observers0, method) ((  void (*) (ListObserver_1_t3616905962 *, ImmutableList_1_t2299524397 *, const MethodInfo*))ListObserver_1__ctor_m1993395291_gshared)(__this, ___observers0, method)
// System.Void UniRx.InternalUtil.ListObserver`1<UniRx.DictionaryRemoveEvent`2<System.Object,System.Object>>::OnCompleted()
extern "C"  void ListObserver_1_OnCompleted_m2339017555_gshared (ListObserver_1_t3616905962 * __this, const MethodInfo* method);
#define ListObserver_1_OnCompleted_m2339017555(__this, method) ((  void (*) (ListObserver_1_t3616905962 *, const MethodInfo*))ListObserver_1_OnCompleted_m2339017555_gshared)(__this, method)
// System.Void UniRx.InternalUtil.ListObserver`1<UniRx.DictionaryRemoveEvent`2<System.Object,System.Object>>::OnError(System.Exception)
extern "C"  void ListObserver_1_OnError_m1783357440_gshared (ListObserver_1_t3616905962 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method);
#define ListObserver_1_OnError_m1783357440(__this, ___error0, method) ((  void (*) (ListObserver_1_t3616905962 *, Exception_t1967233988 *, const MethodInfo*))ListObserver_1_OnError_m1783357440_gshared)(__this, ___error0, method)
// System.Void UniRx.InternalUtil.ListObserver`1<UniRx.DictionaryRemoveEvent`2<System.Object,System.Object>>::OnNext(T)
extern "C"  void ListObserver_1_OnNext_m527025905_gshared (ListObserver_1_t3616905962 * __this, DictionaryRemoveEvent_2_t3322288291  ___value0, const MethodInfo* method);
#define ListObserver_1_OnNext_m527025905(__this, ___value0, method) ((  void (*) (ListObserver_1_t3616905962 *, DictionaryRemoveEvent_2_t3322288291 , const MethodInfo*))ListObserver_1_OnNext_m527025905_gshared)(__this, ___value0, method)
// UniRx.IObserver`1<T> UniRx.InternalUtil.ListObserver`1<UniRx.DictionaryRemoveEvent`2<System.Object,System.Object>>::Add(UniRx.IObserver`1<T>)
extern "C"  Il2CppObject* ListObserver_1_Add_m4259816849_gshared (ListObserver_1_t3616905962 * __this, Il2CppObject* ___observer0, const MethodInfo* method);
#define ListObserver_1_Add_m4259816849(__this, ___observer0, method) ((  Il2CppObject* (*) (ListObserver_1_t3616905962 *, Il2CppObject*, const MethodInfo*))ListObserver_1_Add_m4259816849_gshared)(__this, ___observer0, method)
// UniRx.IObserver`1<T> UniRx.InternalUtil.ListObserver`1<UniRx.DictionaryRemoveEvent`2<System.Object,System.Object>>::Remove(UniRx.IObserver`1<T>)
extern "C"  Il2CppObject* ListObserver_1_Remove_m1238499354_gshared (ListObserver_1_t3616905962 * __this, Il2CppObject* ___observer0, const MethodInfo* method);
#define ListObserver_1_Remove_m1238499354(__this, ___observer0, method) ((  Il2CppObject* (*) (ListObserver_1_t3616905962 *, Il2CppObject*, const MethodInfo*))ListObserver_1_Remove_m1238499354_gshared)(__this, ___observer0, method)
