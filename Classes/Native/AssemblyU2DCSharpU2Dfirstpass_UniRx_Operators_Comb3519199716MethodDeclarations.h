﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.CombineLatestObservable`3/CombineLatest<System.Boolean,System.Boolean,System.Boolean>
struct CombineLatest_t3519199716;
// UniRx.Operators.CombineLatestObservable`3<System.Boolean,System.Boolean,System.Boolean>
struct CombineLatestObservable_3_t3912295671;
// UniRx.IObserver`1<System.Boolean>
struct IObserver_1_t2423004244;
// System.IDisposable
struct IDisposable_t1628921374;
// System.Exception
struct Exception_t1967233988;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Exception1967233988.h"

// System.Void UniRx.Operators.CombineLatestObservable`3/CombineLatest<System.Boolean,System.Boolean,System.Boolean>::.ctor(UniRx.Operators.CombineLatestObservable`3<TLeft,TRight,TResult>,UniRx.IObserver`1<TResult>,System.IDisposable)
extern "C"  void CombineLatest__ctor_m1156244398_gshared (CombineLatest_t3519199716 * __this, CombineLatestObservable_3_t3912295671 * ___parent0, Il2CppObject* ___observer1, Il2CppObject * ___cancel2, const MethodInfo* method);
#define CombineLatest__ctor_m1156244398(__this, ___parent0, ___observer1, ___cancel2, method) ((  void (*) (CombineLatest_t3519199716 *, CombineLatestObservable_3_t3912295671 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))CombineLatest__ctor_m1156244398_gshared)(__this, ___parent0, ___observer1, ___cancel2, method)
// System.IDisposable UniRx.Operators.CombineLatestObservable`3/CombineLatest<System.Boolean,System.Boolean,System.Boolean>::Run()
extern "C"  Il2CppObject * CombineLatest_Run_m3023913830_gshared (CombineLatest_t3519199716 * __this, const MethodInfo* method);
#define CombineLatest_Run_m3023913830(__this, method) ((  Il2CppObject * (*) (CombineLatest_t3519199716 *, const MethodInfo*))CombineLatest_Run_m3023913830_gshared)(__this, method)
// System.Void UniRx.Operators.CombineLatestObservable`3/CombineLatest<System.Boolean,System.Boolean,System.Boolean>::Publish()
extern "C"  void CombineLatest_Publish_m4078538415_gshared (CombineLatest_t3519199716 * __this, const MethodInfo* method);
#define CombineLatest_Publish_m4078538415(__this, method) ((  void (*) (CombineLatest_t3519199716 *, const MethodInfo*))CombineLatest_Publish_m4078538415_gshared)(__this, method)
// System.Void UniRx.Operators.CombineLatestObservable`3/CombineLatest<System.Boolean,System.Boolean,System.Boolean>::OnNext(TResult)
extern "C"  void CombineLatest_OnNext_m2661404877_gshared (CombineLatest_t3519199716 * __this, bool ___value0, const MethodInfo* method);
#define CombineLatest_OnNext_m2661404877(__this, ___value0, method) ((  void (*) (CombineLatest_t3519199716 *, bool, const MethodInfo*))CombineLatest_OnNext_m2661404877_gshared)(__this, ___value0, method)
// System.Void UniRx.Operators.CombineLatestObservable`3/CombineLatest<System.Boolean,System.Boolean,System.Boolean>::OnError(System.Exception)
extern "C"  void CombineLatest_OnError_m183486777_gshared (CombineLatest_t3519199716 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method);
#define CombineLatest_OnError_m183486777(__this, ___error0, method) ((  void (*) (CombineLatest_t3519199716 *, Exception_t1967233988 *, const MethodInfo*))CombineLatest_OnError_m183486777_gshared)(__this, ___error0, method)
// System.Void UniRx.Operators.CombineLatestObservable`3/CombineLatest<System.Boolean,System.Boolean,System.Boolean>::OnCompleted()
extern "C"  void CombineLatest_OnCompleted_m2743061004_gshared (CombineLatest_t3519199716 * __this, const MethodInfo* method);
#define CombineLatest_OnCompleted_m2743061004(__this, method) ((  void (*) (CombineLatest_t3519199716 *, const MethodInfo*))CombineLatest_OnCompleted_m2743061004_gshared)(__this, method)
