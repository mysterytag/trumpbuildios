﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// TutorialTask
struct TutorialTask_t773278243;

#include "codegen/il2cpp-codegen.h"

// System.Void TutorialTask::.ctor()
extern "C"  void TutorialTask__ctor_m3830341592 (TutorialTask_t773278243 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
