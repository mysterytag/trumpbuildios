﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.IEnumerable`1<System.Int32>
struct IEnumerable_1_t1424601847;
// System.Collections.Generic.IEnumerable`1<System.Single>
struct IEnumerable_1_t3830363377;

#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.IEnumerable`1<System.Int32> System.Linq.Enumerable::Range(System.Int32,System.Int32)
extern "C"  Il2CppObject* Enumerable_Range_m212735726 (Il2CppObject * __this /* static, unused */, int32_t ___start0, int32_t ___count1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.IEnumerable`1<System.Int32> System.Linq.Enumerable::CreateRangeIterator(System.Int32,System.Int32)
extern "C"  Il2CppObject* Enumerable_CreateRangeIterator_m2026145084 (Il2CppObject * __this /* static, unused */, int32_t ___start0, int32_t ___upto1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single System.Linq.Enumerable::Sum(System.Collections.Generic.IEnumerable`1<System.Single>)
extern "C"  float Enumerable_Sum_m701247233 (Il2CppObject * __this /* static, unused */, Il2CppObject* ___source0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single System.Linq.Enumerable::<Sum>m__6F(System.Single,System.Single)
extern "C"  float Enumerable_U3CSumU3Em__6F_m1167496870 (Il2CppObject * __this /* static, unused */, float ___a0, float ___b1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
