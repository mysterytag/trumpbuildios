﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Animator
struct Animator_t792326996;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Triggers.ObservableStateMachineTrigger/OnStateMachineInfo
struct  OnStateMachineInfo_t865869923  : public Il2CppObject
{
public:
	// UnityEngine.Animator UniRx.Triggers.ObservableStateMachineTrigger/OnStateMachineInfo::<Animator>k__BackingField
	Animator_t792326996 * ___U3CAnimatorU3Ek__BackingField_0;
	// System.Int32 UniRx.Triggers.ObservableStateMachineTrigger/OnStateMachineInfo::<StateMachinePathHash>k__BackingField
	int32_t ___U3CStateMachinePathHashU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CAnimatorU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(OnStateMachineInfo_t865869923, ___U3CAnimatorU3Ek__BackingField_0)); }
	inline Animator_t792326996 * get_U3CAnimatorU3Ek__BackingField_0() const { return ___U3CAnimatorU3Ek__BackingField_0; }
	inline Animator_t792326996 ** get_address_of_U3CAnimatorU3Ek__BackingField_0() { return &___U3CAnimatorU3Ek__BackingField_0; }
	inline void set_U3CAnimatorU3Ek__BackingField_0(Animator_t792326996 * value)
	{
		___U3CAnimatorU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier(&___U3CAnimatorU3Ek__BackingField_0, value);
	}

	inline static int32_t get_offset_of_U3CStateMachinePathHashU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(OnStateMachineInfo_t865869923, ___U3CStateMachinePathHashU3Ek__BackingField_1)); }
	inline int32_t get_U3CStateMachinePathHashU3Ek__BackingField_1() const { return ___U3CStateMachinePathHashU3Ek__BackingField_1; }
	inline int32_t* get_address_of_U3CStateMachinePathHashU3Ek__BackingField_1() { return &___U3CStateMachinePathHashU3Ek__BackingField_1; }
	inline void set_U3CStateMachinePathHashU3Ek__BackingField_1(int32_t value)
	{
		___U3CStateMachinePathHashU3Ek__BackingField_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
