﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.MergeObservable`1<System.Object>
struct MergeObservable_1_t3637992042;
// UniRx.IObservable`1<UniRx.IObservable`1<System.Object>>
struct IObservable_1_t354703148;
// System.IDisposable
struct IDisposable_t1628921374;
// UniRx.IObserver`1<System.Object>
struct IObserver_1_t3049105323;

#include "codegen/il2cpp-codegen.h"

// System.Void UniRx.Operators.MergeObservable`1<System.Object>::.ctor(UniRx.IObservable`1<UniRx.IObservable`1<T>>,System.Boolean)
extern "C"  void MergeObservable_1__ctor_m951789753_gshared (MergeObservable_1_t3637992042 * __this, Il2CppObject* ___sources0, bool ___isRequiredSubscribeOnCurrentThread1, const MethodInfo* method);
#define MergeObservable_1__ctor_m951789753(__this, ___sources0, ___isRequiredSubscribeOnCurrentThread1, method) ((  void (*) (MergeObservable_1_t3637992042 *, Il2CppObject*, bool, const MethodInfo*))MergeObservable_1__ctor_m951789753_gshared)(__this, ___sources0, ___isRequiredSubscribeOnCurrentThread1, method)
// System.Void UniRx.Operators.MergeObservable`1<System.Object>::.ctor(UniRx.IObservable`1<UniRx.IObservable`1<T>>,System.Int32,System.Boolean)
extern "C"  void MergeObservable_1__ctor_m2030410250_gshared (MergeObservable_1_t3637992042 * __this, Il2CppObject* ___sources0, int32_t ___maxConcurrent1, bool ___isRequiredSubscribeOnCurrentThread2, const MethodInfo* method);
#define MergeObservable_1__ctor_m2030410250(__this, ___sources0, ___maxConcurrent1, ___isRequiredSubscribeOnCurrentThread2, method) ((  void (*) (MergeObservable_1_t3637992042 *, Il2CppObject*, int32_t, bool, const MethodInfo*))MergeObservable_1__ctor_m2030410250_gshared)(__this, ___sources0, ___maxConcurrent1, ___isRequiredSubscribeOnCurrentThread2, method)
// System.IDisposable UniRx.Operators.MergeObservable`1<System.Object>::SubscribeCore(UniRx.IObserver`1<T>,System.IDisposable)
extern "C"  Il2CppObject * MergeObservable_1_SubscribeCore_m4054478294_gshared (MergeObservable_1_t3637992042 * __this, Il2CppObject* ___observer0, Il2CppObject * ___cancel1, const MethodInfo* method);
#define MergeObservable_1_SubscribeCore_m4054478294(__this, ___observer0, ___cancel1, method) ((  Il2CppObject * (*) (MergeObservable_1_t3637992042 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))MergeObservable_1_SubscribeCore_m4054478294_gshared)(__this, ___observer0, ___cancel1, method)
