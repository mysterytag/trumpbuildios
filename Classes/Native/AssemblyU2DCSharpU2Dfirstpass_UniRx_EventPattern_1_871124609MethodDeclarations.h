﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_EventPattern_12903003978MethodDeclarations.h"

// System.Void UniRx.EventPattern`1<UniRx.Examples.Sample09_EventHandling/MyEventArgs>::.ctor(System.Object,TEventArgs)
#define EventPattern_1__ctor_m2431577799(__this, ___sender0, ___e1, method) ((  void (*) (EventPattern_1_t871124609 *, Il2CppObject *, MyEventArgs_t3100194347 *, const MethodInfo*))EventPattern_1__ctor_m3160314633_gshared)(__this, ___sender0, ___e1, method)
