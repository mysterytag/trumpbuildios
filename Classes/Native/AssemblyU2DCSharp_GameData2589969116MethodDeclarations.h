﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GameData
struct GameData_t2589969116;

#include "codegen/il2cpp-codegen.h"

// System.Void GameData::.ctor()
extern "C"  void GameData__ctor_m2338417471 (GameData_t2589969116 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
