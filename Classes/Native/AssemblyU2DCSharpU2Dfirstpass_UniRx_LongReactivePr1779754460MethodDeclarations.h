﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.LongReactiveProperty
struct LongReactiveProperty_t1779754460;

#include "codegen/il2cpp-codegen.h"

// System.Void UniRx.LongReactiveProperty::.ctor()
extern "C"  void LongReactiveProperty__ctor_m1986312749 (LongReactiveProperty_t1779754460 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.LongReactiveProperty::.ctor(System.Int64)
extern "C"  void LongReactiveProperty__ctor_m917741439 (LongReactiveProperty_t1779754460 * __this, int64_t ___initialValue0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
