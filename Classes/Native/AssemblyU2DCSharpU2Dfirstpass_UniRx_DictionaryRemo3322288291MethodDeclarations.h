﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Object
struct Il2CppObject;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_DictionaryRemo3322288291.h"
#include "mscorlib_System_Object837106420.h"

// System.Void UniRx.DictionaryRemoveEvent`2<System.Object,System.Object>::.ctor(TKey,TValue)
extern "C"  void DictionaryRemoveEvent_2__ctor_m686634581_gshared (DictionaryRemoveEvent_2_t3322288291 * __this, Il2CppObject * ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define DictionaryRemoveEvent_2__ctor_m686634581(__this, ___key0, ___value1, method) ((  void (*) (DictionaryRemoveEvent_2_t3322288291 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))DictionaryRemoveEvent_2__ctor_m686634581_gshared)(__this, ___key0, ___value1, method)
// TKey UniRx.DictionaryRemoveEvent`2<System.Object,System.Object>::get_Key()
extern "C"  Il2CppObject * DictionaryRemoveEvent_2_get_Key_m1804595929_gshared (DictionaryRemoveEvent_2_t3322288291 * __this, const MethodInfo* method);
#define DictionaryRemoveEvent_2_get_Key_m1804595929(__this, method) ((  Il2CppObject * (*) (DictionaryRemoveEvent_2_t3322288291 *, const MethodInfo*))DictionaryRemoveEvent_2_get_Key_m1804595929_gshared)(__this, method)
// System.Void UniRx.DictionaryRemoveEvent`2<System.Object,System.Object>::set_Key(TKey)
extern "C"  void DictionaryRemoveEvent_2_set_Key_m859095732_gshared (DictionaryRemoveEvent_2_t3322288291 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define DictionaryRemoveEvent_2_set_Key_m859095732(__this, ___value0, method) ((  void (*) (DictionaryRemoveEvent_2_t3322288291 *, Il2CppObject *, const MethodInfo*))DictionaryRemoveEvent_2_set_Key_m859095732_gshared)(__this, ___value0, method)
// TValue UniRx.DictionaryRemoveEvent`2<System.Object,System.Object>::get_Value()
extern "C"  Il2CppObject * DictionaryRemoveEvent_2_get_Value_m3315363709_gshared (DictionaryRemoveEvent_2_t3322288291 * __this, const MethodInfo* method);
#define DictionaryRemoveEvent_2_get_Value_m3315363709(__this, method) ((  Il2CppObject * (*) (DictionaryRemoveEvent_2_t3322288291 *, const MethodInfo*))DictionaryRemoveEvent_2_get_Value_m3315363709_gshared)(__this, method)
// System.Void UniRx.DictionaryRemoveEvent`2<System.Object,System.Object>::set_Value(TValue)
extern "C"  void DictionaryRemoveEvent_2_set_Value_m625450164_gshared (DictionaryRemoveEvent_2_t3322288291 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define DictionaryRemoveEvent_2_set_Value_m625450164(__this, ___value0, method) ((  void (*) (DictionaryRemoveEvent_2_t3322288291 *, Il2CppObject *, const MethodInfo*))DictionaryRemoveEvent_2_set_Value_m625450164_gshared)(__this, ___value0, method)
// System.String UniRx.DictionaryRemoveEvent`2<System.Object,System.Object>::ToString()
extern "C"  String_t* DictionaryRemoveEvent_2_ToString_m1055329428_gshared (DictionaryRemoveEvent_2_t3322288291 * __this, const MethodInfo* method);
#define DictionaryRemoveEvent_2_ToString_m1055329428(__this, method) ((  String_t* (*) (DictionaryRemoveEvent_2_t3322288291 *, const MethodInfo*))DictionaryRemoveEvent_2_ToString_m1055329428_gshared)(__this, method)
// System.Int32 UniRx.DictionaryRemoveEvent`2<System.Object,System.Object>::GetHashCode()
extern "C"  int32_t DictionaryRemoveEvent_2_GetHashCode_m2076469080_gshared (DictionaryRemoveEvent_2_t3322288291 * __this, const MethodInfo* method);
#define DictionaryRemoveEvent_2_GetHashCode_m2076469080(__this, method) ((  int32_t (*) (DictionaryRemoveEvent_2_t3322288291 *, const MethodInfo*))DictionaryRemoveEvent_2_GetHashCode_m2076469080_gshared)(__this, method)
// System.Boolean UniRx.DictionaryRemoveEvent`2<System.Object,System.Object>::Equals(UniRx.DictionaryRemoveEvent`2<TKey,TValue>)
extern "C"  bool DictionaryRemoveEvent_2_Equals_m3721337790_gshared (DictionaryRemoveEvent_2_t3322288291 * __this, DictionaryRemoveEvent_2_t3322288291  ___other0, const MethodInfo* method);
#define DictionaryRemoveEvent_2_Equals_m3721337790(__this, ___other0, method) ((  bool (*) (DictionaryRemoveEvent_2_t3322288291 *, DictionaryRemoveEvent_2_t3322288291 , const MethodInfo*))DictionaryRemoveEvent_2_Equals_m3721337790_gshared)(__this, ___other0, method)
