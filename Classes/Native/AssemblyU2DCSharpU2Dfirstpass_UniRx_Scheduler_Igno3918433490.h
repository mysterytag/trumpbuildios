﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UniRx.ICancelable
struct ICancelable_t4109686575;
// System.Action
struct Action_t437523947;
// System.Object
struct Il2CppObject;

#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_TimeSpan763862892.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Scheduler/IgnoreTimeScaleMainThreadScheduler/<PeriodicAction>c__Iterator1C
struct  U3CPeriodicActionU3Ec__Iterator1C_t3918433490  : public Il2CppObject
{
public:
	// System.TimeSpan UniRx.Scheduler/IgnoreTimeScaleMainThreadScheduler/<PeriodicAction>c__Iterator1C::period
	TimeSpan_t763862892  ___period_0;
	// UniRx.ICancelable UniRx.Scheduler/IgnoreTimeScaleMainThreadScheduler/<PeriodicAction>c__Iterator1C::cancellation
	Il2CppObject * ___cancellation_1;
	// System.Action UniRx.Scheduler/IgnoreTimeScaleMainThreadScheduler/<PeriodicAction>c__Iterator1C::action
	Action_t437523947 * ___action_2;
	// System.Single UniRx.Scheduler/IgnoreTimeScaleMainThreadScheduler/<PeriodicAction>c__Iterator1C::<startTime>__0
	float ___U3CstartTimeU3E__0_3;
	// System.Single UniRx.Scheduler/IgnoreTimeScaleMainThreadScheduler/<PeriodicAction>c__Iterator1C::<dt>__1
	float ___U3CdtU3E__1_4;
	// System.Single UniRx.Scheduler/IgnoreTimeScaleMainThreadScheduler/<PeriodicAction>c__Iterator1C::<elapsed>__2
	float ___U3CelapsedU3E__2_5;
	// System.Int32 UniRx.Scheduler/IgnoreTimeScaleMainThreadScheduler/<PeriodicAction>c__Iterator1C::$PC
	int32_t ___U24PC_6;
	// System.Object UniRx.Scheduler/IgnoreTimeScaleMainThreadScheduler/<PeriodicAction>c__Iterator1C::$current
	Il2CppObject * ___U24current_7;
	// System.TimeSpan UniRx.Scheduler/IgnoreTimeScaleMainThreadScheduler/<PeriodicAction>c__Iterator1C::<$>period
	TimeSpan_t763862892  ___U3CU24U3Eperiod_8;
	// UniRx.ICancelable UniRx.Scheduler/IgnoreTimeScaleMainThreadScheduler/<PeriodicAction>c__Iterator1C::<$>cancellation
	Il2CppObject * ___U3CU24U3Ecancellation_9;
	// System.Action UniRx.Scheduler/IgnoreTimeScaleMainThreadScheduler/<PeriodicAction>c__Iterator1C::<$>action
	Action_t437523947 * ___U3CU24U3Eaction_10;

public:
	inline static int32_t get_offset_of_period_0() { return static_cast<int32_t>(offsetof(U3CPeriodicActionU3Ec__Iterator1C_t3918433490, ___period_0)); }
	inline TimeSpan_t763862892  get_period_0() const { return ___period_0; }
	inline TimeSpan_t763862892 * get_address_of_period_0() { return &___period_0; }
	inline void set_period_0(TimeSpan_t763862892  value)
	{
		___period_0 = value;
	}

	inline static int32_t get_offset_of_cancellation_1() { return static_cast<int32_t>(offsetof(U3CPeriodicActionU3Ec__Iterator1C_t3918433490, ___cancellation_1)); }
	inline Il2CppObject * get_cancellation_1() const { return ___cancellation_1; }
	inline Il2CppObject ** get_address_of_cancellation_1() { return &___cancellation_1; }
	inline void set_cancellation_1(Il2CppObject * value)
	{
		___cancellation_1 = value;
		Il2CppCodeGenWriteBarrier(&___cancellation_1, value);
	}

	inline static int32_t get_offset_of_action_2() { return static_cast<int32_t>(offsetof(U3CPeriodicActionU3Ec__Iterator1C_t3918433490, ___action_2)); }
	inline Action_t437523947 * get_action_2() const { return ___action_2; }
	inline Action_t437523947 ** get_address_of_action_2() { return &___action_2; }
	inline void set_action_2(Action_t437523947 * value)
	{
		___action_2 = value;
		Il2CppCodeGenWriteBarrier(&___action_2, value);
	}

	inline static int32_t get_offset_of_U3CstartTimeU3E__0_3() { return static_cast<int32_t>(offsetof(U3CPeriodicActionU3Ec__Iterator1C_t3918433490, ___U3CstartTimeU3E__0_3)); }
	inline float get_U3CstartTimeU3E__0_3() const { return ___U3CstartTimeU3E__0_3; }
	inline float* get_address_of_U3CstartTimeU3E__0_3() { return &___U3CstartTimeU3E__0_3; }
	inline void set_U3CstartTimeU3E__0_3(float value)
	{
		___U3CstartTimeU3E__0_3 = value;
	}

	inline static int32_t get_offset_of_U3CdtU3E__1_4() { return static_cast<int32_t>(offsetof(U3CPeriodicActionU3Ec__Iterator1C_t3918433490, ___U3CdtU3E__1_4)); }
	inline float get_U3CdtU3E__1_4() const { return ___U3CdtU3E__1_4; }
	inline float* get_address_of_U3CdtU3E__1_4() { return &___U3CdtU3E__1_4; }
	inline void set_U3CdtU3E__1_4(float value)
	{
		___U3CdtU3E__1_4 = value;
	}

	inline static int32_t get_offset_of_U3CelapsedU3E__2_5() { return static_cast<int32_t>(offsetof(U3CPeriodicActionU3Ec__Iterator1C_t3918433490, ___U3CelapsedU3E__2_5)); }
	inline float get_U3CelapsedU3E__2_5() const { return ___U3CelapsedU3E__2_5; }
	inline float* get_address_of_U3CelapsedU3E__2_5() { return &___U3CelapsedU3E__2_5; }
	inline void set_U3CelapsedU3E__2_5(float value)
	{
		___U3CelapsedU3E__2_5 = value;
	}

	inline static int32_t get_offset_of_U24PC_6() { return static_cast<int32_t>(offsetof(U3CPeriodicActionU3Ec__Iterator1C_t3918433490, ___U24PC_6)); }
	inline int32_t get_U24PC_6() const { return ___U24PC_6; }
	inline int32_t* get_address_of_U24PC_6() { return &___U24PC_6; }
	inline void set_U24PC_6(int32_t value)
	{
		___U24PC_6 = value;
	}

	inline static int32_t get_offset_of_U24current_7() { return static_cast<int32_t>(offsetof(U3CPeriodicActionU3Ec__Iterator1C_t3918433490, ___U24current_7)); }
	inline Il2CppObject * get_U24current_7() const { return ___U24current_7; }
	inline Il2CppObject ** get_address_of_U24current_7() { return &___U24current_7; }
	inline void set_U24current_7(Il2CppObject * value)
	{
		___U24current_7 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_7, value);
	}

	inline static int32_t get_offset_of_U3CU24U3Eperiod_8() { return static_cast<int32_t>(offsetof(U3CPeriodicActionU3Ec__Iterator1C_t3918433490, ___U3CU24U3Eperiod_8)); }
	inline TimeSpan_t763862892  get_U3CU24U3Eperiod_8() const { return ___U3CU24U3Eperiod_8; }
	inline TimeSpan_t763862892 * get_address_of_U3CU24U3Eperiod_8() { return &___U3CU24U3Eperiod_8; }
	inline void set_U3CU24U3Eperiod_8(TimeSpan_t763862892  value)
	{
		___U3CU24U3Eperiod_8 = value;
	}

	inline static int32_t get_offset_of_U3CU24U3Ecancellation_9() { return static_cast<int32_t>(offsetof(U3CPeriodicActionU3Ec__Iterator1C_t3918433490, ___U3CU24U3Ecancellation_9)); }
	inline Il2CppObject * get_U3CU24U3Ecancellation_9() const { return ___U3CU24U3Ecancellation_9; }
	inline Il2CppObject ** get_address_of_U3CU24U3Ecancellation_9() { return &___U3CU24U3Ecancellation_9; }
	inline void set_U3CU24U3Ecancellation_9(Il2CppObject * value)
	{
		___U3CU24U3Ecancellation_9 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU24U3Ecancellation_9, value);
	}

	inline static int32_t get_offset_of_U3CU24U3Eaction_10() { return static_cast<int32_t>(offsetof(U3CPeriodicActionU3Ec__Iterator1C_t3918433490, ___U3CU24U3Eaction_10)); }
	inline Action_t437523947 * get_U3CU24U3Eaction_10() const { return ___U3CU24U3Eaction_10; }
	inline Action_t437523947 ** get_address_of_U3CU24U3Eaction_10() { return &___U3CU24U3Eaction_10; }
	inline void set_U3CU24U3Eaction_10(Action_t437523947 * value)
	{
		___U3CU24U3Eaction_10 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU24U3Eaction_10, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
