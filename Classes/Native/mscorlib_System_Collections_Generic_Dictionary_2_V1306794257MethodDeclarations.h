﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2<System.Object,System.Int64>
struct Dictionary_2_t1539766316;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_V1306794257.h"

// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,System.Int64>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void Enumerator__ctor_m861884875_gshared (Enumerator_t1306794259 * __this, Dictionary_2_t1539766316 * ___host0, const MethodInfo* method);
#define Enumerator__ctor_m861884875(__this, ___host0, method) ((  void (*) (Enumerator_t1306794259 *, Dictionary_2_t1539766316 *, const MethodInfo*))Enumerator__ctor_m861884875_gshared)(__this, ___host0, method)
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,System.Int64>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m362683776_gshared (Enumerator_t1306794259 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m362683776(__this, method) ((  Il2CppObject * (*) (Enumerator_t1306794259 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m362683776_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,System.Int64>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m1198937610_gshared (Enumerator_t1306794259 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_Reset_m1198937610(__this, method) ((  void (*) (Enumerator_t1306794259 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m1198937610_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,System.Int64>::Dispose()
extern "C"  void Enumerator_Dispose_m190083373_gshared (Enumerator_t1306794259 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m190083373(__this, method) ((  void (*) (Enumerator_t1306794259 *, const MethodInfo*))Enumerator_Dispose_m190083373_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,System.Int64>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m20462266_gshared (Enumerator_t1306794259 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m20462266(__this, method) ((  bool (*) (Enumerator_t1306794259 *, const MethodInfo*))Enumerator_MoveNext_m20462266_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,System.Int64>::get_Current()
extern "C"  int64_t Enumerator_get_Current_m3922059568_gshared (Enumerator_t1306794259 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m3922059568(__this, method) ((  int64_t (*) (Enumerator_t1306794259 *, const MethodInfo*))Enumerator_get_Current_m3922059568_gshared)(__this, method)
