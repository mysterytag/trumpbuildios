﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.InternalUtil.DisposedObserver`1<UniRx.CollectionRemoveEvent`1<System.Object>>
struct DisposedObserver_1_t3712062482;
// System.Exception
struct Exception_t1967233988;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Exception1967233988.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_CollectionRemov242637724.h"

// System.Void UniRx.InternalUtil.DisposedObserver`1<UniRx.CollectionRemoveEvent`1<System.Object>>::.ctor()
extern "C"  void DisposedObserver_1__ctor_m1283383485_gshared (DisposedObserver_1_t3712062482 * __this, const MethodInfo* method);
#define DisposedObserver_1__ctor_m1283383485(__this, method) ((  void (*) (DisposedObserver_1_t3712062482 *, const MethodInfo*))DisposedObserver_1__ctor_m1283383485_gshared)(__this, method)
// System.Void UniRx.InternalUtil.DisposedObserver`1<UniRx.CollectionRemoveEvent`1<System.Object>>::.cctor()
extern "C"  void DisposedObserver_1__cctor_m648086160_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define DisposedObserver_1__cctor_m648086160(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))DisposedObserver_1__cctor_m648086160_gshared)(__this /* static, unused */, method)
// System.Void UniRx.InternalUtil.DisposedObserver`1<UniRx.CollectionRemoveEvent`1<System.Object>>::OnCompleted()
extern "C"  void DisposedObserver_1_OnCompleted_m1757139591_gshared (DisposedObserver_1_t3712062482 * __this, const MethodInfo* method);
#define DisposedObserver_1_OnCompleted_m1757139591(__this, method) ((  void (*) (DisposedObserver_1_t3712062482 *, const MethodInfo*))DisposedObserver_1_OnCompleted_m1757139591_gshared)(__this, method)
// System.Void UniRx.InternalUtil.DisposedObserver`1<UniRx.CollectionRemoveEvent`1<System.Object>>::OnError(System.Exception)
extern "C"  void DisposedObserver_1_OnError_m2270425396_gshared (DisposedObserver_1_t3712062482 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method);
#define DisposedObserver_1_OnError_m2270425396(__this, ___error0, method) ((  void (*) (DisposedObserver_1_t3712062482 *, Exception_t1967233988 *, const MethodInfo*))DisposedObserver_1_OnError_m2270425396_gshared)(__this, ___error0, method)
// System.Void UniRx.InternalUtil.DisposedObserver`1<UniRx.CollectionRemoveEvent`1<System.Object>>::OnNext(T)
extern "C"  void DisposedObserver_1_OnNext_m3506618405_gshared (DisposedObserver_1_t3712062482 * __this, CollectionRemoveEvent_1_t242637724  ___value0, const MethodInfo* method);
#define DisposedObserver_1_OnNext_m3506618405(__this, ___value0, method) ((  void (*) (DisposedObserver_1_t3712062482 *, CollectionRemoveEvent_1_t242637724 , const MethodInfo*))DisposedObserver_1_OnNext_m3506618405_gshared)(__this, ___value0, method)
