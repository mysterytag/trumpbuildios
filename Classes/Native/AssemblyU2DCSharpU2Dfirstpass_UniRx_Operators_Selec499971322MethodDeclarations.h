﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.SelectManyObservable`2/SelectManyEnumerableObserver<UniRx.Unit,UniRx.Unit>
struct SelectManyEnumerableObserver_t499971322;
// UniRx.Operators.SelectManyObservable`2<UniRx.Unit,UniRx.Unit>
struct SelectManyObservable_2_t3540602214;
// UniRx.IObserver`1<UniRx.Unit>
struct IObserver_1_t475317645;
// System.IDisposable
struct IDisposable_t1628921374;
// System.Exception
struct Exception_t1967233988;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Unit2558286038.h"
#include "mscorlib_System_Exception1967233988.h"

// System.Void UniRx.Operators.SelectManyObservable`2/SelectManyEnumerableObserver<UniRx.Unit,UniRx.Unit>::.ctor(UniRx.Operators.SelectManyObservable`2<TSource,TResult>,UniRx.IObserver`1<TResult>,System.IDisposable)
extern "C"  void SelectManyEnumerableObserver__ctor_m1490003512_gshared (SelectManyEnumerableObserver_t499971322 * __this, SelectManyObservable_2_t3540602214 * ___parent0, Il2CppObject* ___observer1, Il2CppObject * ___cancel2, const MethodInfo* method);
#define SelectManyEnumerableObserver__ctor_m1490003512(__this, ___parent0, ___observer1, ___cancel2, method) ((  void (*) (SelectManyEnumerableObserver_t499971322 *, SelectManyObservable_2_t3540602214 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))SelectManyEnumerableObserver__ctor_m1490003512_gshared)(__this, ___parent0, ___observer1, ___cancel2, method)
// System.IDisposable UniRx.Operators.SelectManyObservable`2/SelectManyEnumerableObserver<UniRx.Unit,UniRx.Unit>::Run()
extern "C"  Il2CppObject * SelectManyEnumerableObserver_Run_m2080133232_gshared (SelectManyEnumerableObserver_t499971322 * __this, const MethodInfo* method);
#define SelectManyEnumerableObserver_Run_m2080133232(__this, method) ((  Il2CppObject * (*) (SelectManyEnumerableObserver_t499971322 *, const MethodInfo*))SelectManyEnumerableObserver_Run_m2080133232_gshared)(__this, method)
// System.Void UniRx.Operators.SelectManyObservable`2/SelectManyEnumerableObserver<UniRx.Unit,UniRx.Unit>::OnNext(TSource)
extern "C"  void SelectManyEnumerableObserver_OnNext_m4213807_gshared (SelectManyEnumerableObserver_t499971322 * __this, Unit_t2558286038  ___value0, const MethodInfo* method);
#define SelectManyEnumerableObserver_OnNext_m4213807(__this, ___value0, method) ((  void (*) (SelectManyEnumerableObserver_t499971322 *, Unit_t2558286038 , const MethodInfo*))SelectManyEnumerableObserver_OnNext_m4213807_gshared)(__this, ___value0, method)
// System.Void UniRx.Operators.SelectManyObservable`2/SelectManyEnumerableObserver<UniRx.Unit,UniRx.Unit>::OnError(System.Exception)
extern "C"  void SelectManyEnumerableObserver_OnError_m134667481_gshared (SelectManyEnumerableObserver_t499971322 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method);
#define SelectManyEnumerableObserver_OnError_m134667481(__this, ___error0, method) ((  void (*) (SelectManyEnumerableObserver_t499971322 *, Exception_t1967233988 *, const MethodInfo*))SelectManyEnumerableObserver_OnError_m134667481_gshared)(__this, ___error0, method)
// System.Void UniRx.Operators.SelectManyObservable`2/SelectManyEnumerableObserver<UniRx.Unit,UniRx.Unit>::OnCompleted()
extern "C"  void SelectManyEnumerableObserver_OnCompleted_m2564771244_gshared (SelectManyEnumerableObserver_t499971322 * __this, const MethodInfo* method);
#define SelectManyEnumerableObserver_OnCompleted_m2564771244(__this, method) ((  void (*) (SelectManyEnumerableObserver_t499971322 *, const MethodInfo*))SelectManyEnumerableObserver_OnCompleted_m2564771244_gshared)(__this, method)
