﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UniRx.IObserver`1<System.Object>
struct IObserver_1_t3049105323;
// UniRx.Operators.SkipUntilObservable`2/SkipUntilOuterObserver<System.Object,System.Object>
struct SkipUntilOuterObserver_t2866202317;
// System.IDisposable
struct IDisposable_t1628921374;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Operators.SkipUntilObservable`2/SkipUntilOuterObserver/SkipUntil<System.Object,System.Object>
struct  SkipUntil_t2756386132  : public Il2CppObject
{
public:
	// UniRx.IObserver`1<T> modreq(System.Runtime.CompilerServices.IsVolatile) UniRx.Operators.SkipUntilObservable`2/SkipUntilOuterObserver/SkipUntil::observer
	Il2CppObject* ___observer_0;
	// UniRx.Operators.SkipUntilObservable`2/SkipUntilOuterObserver<T,TOther> UniRx.Operators.SkipUntilObservable`2/SkipUntilOuterObserver/SkipUntil::parent
	SkipUntilOuterObserver_t2866202317 * ___parent_1;
	// System.IDisposable UniRx.Operators.SkipUntilObservable`2/SkipUntilOuterObserver/SkipUntil::subscription
	Il2CppObject * ___subscription_2;

public:
	inline static int32_t get_offset_of_observer_0() { return static_cast<int32_t>(offsetof(SkipUntil_t2756386132, ___observer_0)); }
	inline Il2CppObject* get_observer_0() const { return ___observer_0; }
	inline Il2CppObject** get_address_of_observer_0() { return &___observer_0; }
	inline void set_observer_0(Il2CppObject* value)
	{
		___observer_0 = value;
		Il2CppCodeGenWriteBarrier(&___observer_0, value);
	}

	inline static int32_t get_offset_of_parent_1() { return static_cast<int32_t>(offsetof(SkipUntil_t2756386132, ___parent_1)); }
	inline SkipUntilOuterObserver_t2866202317 * get_parent_1() const { return ___parent_1; }
	inline SkipUntilOuterObserver_t2866202317 ** get_address_of_parent_1() { return &___parent_1; }
	inline void set_parent_1(SkipUntilOuterObserver_t2866202317 * value)
	{
		___parent_1 = value;
		Il2CppCodeGenWriteBarrier(&___parent_1, value);
	}

	inline static int32_t get_offset_of_subscription_2() { return static_cast<int32_t>(offsetof(SkipUntil_t2756386132, ___subscription_2)); }
	inline Il2CppObject * get_subscription_2() const { return ___subscription_2; }
	inline Il2CppObject ** get_address_of_subscription_2() { return &___subscription_2; }
	inline void set_subscription_2(Il2CppObject * value)
	{
		___subscription_2 = value;
		Il2CppCodeGenWriteBarrier(&___subscription_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
