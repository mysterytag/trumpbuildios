﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.StartWithObservable`1/StartWith<System.Object>
struct StartWith_t4018594145;
// UniRx.Operators.StartWithObservable`1<System.Object>
struct StartWithObservable_1_t2623089658;
// UniRx.IObserver`1<System.Object>
struct IObserver_1_t3049105323;
// System.IDisposable
struct IDisposable_t1628921374;
// System.Object
struct Il2CppObject;
// System.Exception
struct Exception_t1967233988;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_Exception1967233988.h"

// System.Void UniRx.Operators.StartWithObservable`1/StartWith<System.Object>::.ctor(UniRx.Operators.StartWithObservable`1<T>,UniRx.IObserver`1<T>,System.IDisposable)
extern "C"  void StartWith__ctor_m516095444_gshared (StartWith_t4018594145 * __this, StartWithObservable_1_t2623089658 * ___parent0, Il2CppObject* ___observer1, Il2CppObject * ___cancel2, const MethodInfo* method);
#define StartWith__ctor_m516095444(__this, ___parent0, ___observer1, ___cancel2, method) ((  void (*) (StartWith_t4018594145 *, StartWithObservable_1_t2623089658 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))StartWith__ctor_m516095444_gshared)(__this, ___parent0, ___observer1, ___cancel2, method)
// System.IDisposable UniRx.Operators.StartWithObservable`1/StartWith<System.Object>::Run()
extern "C"  Il2CppObject * StartWith_Run_m4069786751_gshared (StartWith_t4018594145 * __this, const MethodInfo* method);
#define StartWith_Run_m4069786751(__this, method) ((  Il2CppObject * (*) (StartWith_t4018594145 *, const MethodInfo*))StartWith_Run_m4069786751_gshared)(__this, method)
// System.Void UniRx.Operators.StartWithObservable`1/StartWith<System.Object>::OnNext(T)
extern "C"  void StartWith_OnNext_m169881817_gshared (StartWith_t4018594145 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define StartWith_OnNext_m169881817(__this, ___value0, method) ((  void (*) (StartWith_t4018594145 *, Il2CppObject *, const MethodInfo*))StartWith_OnNext_m169881817_gshared)(__this, ___value0, method)
// System.Void UniRx.Operators.StartWithObservable`1/StartWith<System.Object>::OnError(System.Exception)
extern "C"  void StartWith_OnError_m4009851368_gshared (StartWith_t4018594145 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method);
#define StartWith_OnError_m4009851368(__this, ___error0, method) ((  void (*) (StartWith_t4018594145 *, Exception_t1967233988 *, const MethodInfo*))StartWith_OnError_m4009851368_gshared)(__this, ___error0, method)
// System.Void UniRx.Operators.StartWithObservable`1/StartWith<System.Object>::OnCompleted()
extern "C"  void StartWith_OnCompleted_m4287220027_gshared (StartWith_t4018594145 * __this, const MethodInfo* method);
#define StartWith_OnCompleted_m4287220027(__this, method) ((  void (*) (StartWith_t4018594145 *, const MethodInfo*))StartWith_OnCompleted_m4287220027_gshared)(__this, method)
