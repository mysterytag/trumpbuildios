﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.IObservable`1<UniRx.Unit>
struct IObservable_1_t2317084402;
// UniRx.IObservable`1<System.Int32>
struct IObservable_1_t2606213151;
// UniRx.IScheduler
struct IScheduler_t2938318244;
// System.Action
struct Action_t437523947;
// System.Func`1<UniRx.IObservable`1<UniRx.Unit>>
struct Func_1_t3459865649;
// System.Action`1<System.Action>
struct Action_1_t585976652;
// System.Func`3<System.AsyncCallback,System.Object,System.IAsyncResult>
struct Func_3_t3381739440;
// System.Action`1<System.IAsyncResult>
struct Action_1_t686135974;
// UniRx.IObservable`1<System.Int64>
struct IObservable_1_t2606213246;
// System.Func`1<System.Collections.IEnumerator>
struct Func_1_t1429988286;
// System.Collections.IEnumerator
struct IEnumerator_t287207039;
// UniRx.IObserver`1<UniRx.Unit>
struct IObserver_1_t475317645;
// UniRx.IObserver`1<System.Int64>
struct IObserver_1_t764446489;
// UniRx.IObservable`1<System.Boolean>
struct IObservable_1_t4264771001;

#include "codegen/il2cpp-codegen.h"
#include "System_Core_System_Action437523947.h"
#include "mscorlib_System_TimeSpan763862892.h"
#include "mscorlib_System_DateTimeOffset3712260035.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_CancellationTo1439151560.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_FrameCountType167759182.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Unit2558286038.h"

// System.Void UniRx.Observable::.cctor()
extern "C"  void Observable__cctor_m277909913 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UniRx.IObservable`1<UniRx.Unit> UniRx.Observable::ReturnUnit()
extern "C"  Il2CppObject* Observable_ReturnUnit_m2015160972 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UniRx.IObservable`1<System.Int32> UniRx.Observable::Range(System.Int32,System.Int32)
extern "C"  Il2CppObject* Observable_Range_m751165906 (Il2CppObject * __this /* static, unused */, int32_t ___start0, int32_t ___count1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UniRx.IObservable`1<System.Int32> UniRx.Observable::Range(System.Int32,System.Int32,UniRx.IScheduler)
extern "C"  Il2CppObject* Observable_Range_m4135749642 (Il2CppObject * __this /* static, unused */, int32_t ___start0, int32_t ___count1, Il2CppObject * ___scheduler2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UniRx.IObservable`1<UniRx.Unit> UniRx.Observable::Start(System.Action)
extern "C"  Il2CppObject* Observable_Start_m81987121 (Il2CppObject * __this /* static, unused */, Action_t437523947 * ___action0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UniRx.IObservable`1<UniRx.Unit> UniRx.Observable::Start(System.Action,System.TimeSpan)
extern "C"  Il2CppObject* Observable_Start_m775422087 (Il2CppObject * __this /* static, unused */, Action_t437523947 * ___action0, TimeSpan_t763862892  ___timeSpan1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UniRx.IObservable`1<UniRx.Unit> UniRx.Observable::Start(System.Action,UniRx.IScheduler)
extern "C"  Il2CppObject* Observable_Start_m838126987 (Il2CppObject * __this /* static, unused */, Action_t437523947 * ___action0, Il2CppObject * ___scheduler1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UniRx.IObservable`1<UniRx.Unit> UniRx.Observable::Start(System.Action,System.TimeSpan,UniRx.IScheduler)
extern "C"  Il2CppObject* Observable_Start_m3673491957 (Il2CppObject * __this /* static, unused */, Action_t437523947 * ___action0, TimeSpan_t763862892  ___timeSpan1, Il2CppObject * ___scheduler2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Func`1<UniRx.IObservable`1<UniRx.Unit>> UniRx.Observable::ToAsync(System.Action)
extern "C"  Func_1_t3459865649 * Observable_ToAsync_m2679241012 (Il2CppObject * __this /* static, unused */, Action_t437523947 * ___action0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Func`1<UniRx.IObservable`1<UniRx.Unit>> UniRx.Observable::ToAsync(System.Action,UniRx.IScheduler)
extern "C"  Func_1_t3459865649 * Observable_ToAsync_m2487214312 (Il2CppObject * __this /* static, unused */, Action_t437523947 * ___action0, Il2CppObject * ___scheduler1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UniRx.IObservable`1<UniRx.Unit> UniRx.Observable::FromEvent(System.Action`1<System.Action>,System.Action`1<System.Action>)
extern "C"  Il2CppObject* Observable_FromEvent_m3570768044 (Il2CppObject * __this /* static, unused */, Action_1_t585976652 * ___addHandler0, Action_1_t585976652 * ___removeHandler1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Func`1<UniRx.IObservable`1<UniRx.Unit>> UniRx.Observable::FromAsyncPattern(System.Func`3<System.AsyncCallback,System.Object,System.IAsyncResult>,System.Action`1<System.IAsyncResult>)
extern "C"  Func_1_t3459865649 * Observable_FromAsyncPattern_m1320550440 (Il2CppObject * __this /* static, unused */, Func_3_t3381739440 * ___begin0, Action_1_t686135974 * ___end1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UniRx.IObservable`1<System.Int64> UniRx.Observable::Interval(System.TimeSpan)
extern "C"  Il2CppObject* Observable_Interval_m2492905901 (Il2CppObject * __this /* static, unused */, TimeSpan_t763862892  ___period0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UniRx.IObservable`1<System.Int64> UniRx.Observable::Interval(System.TimeSpan,UniRx.IScheduler)
extern "C"  Il2CppObject* Observable_Interval_m3597074831 (Il2CppObject * __this /* static, unused */, TimeSpan_t763862892  ___period0, Il2CppObject * ___scheduler1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UniRx.IObservable`1<System.Int64> UniRx.Observable::Timer(System.TimeSpan)
extern "C"  Il2CppObject* Observable_Timer_m618408927 (Il2CppObject * __this /* static, unused */, TimeSpan_t763862892  ___dueTime0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UniRx.IObservable`1<System.Int64> UniRx.Observable::Timer(System.DateTimeOffset)
extern "C"  Il2CppObject* Observable_Timer_m1787381928 (Il2CppObject * __this /* static, unused */, DateTimeOffset_t3712260035  ___dueTime0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UniRx.IObservable`1<System.Int64> UniRx.Observable::Timer(System.TimeSpan,System.TimeSpan)
extern "C"  Il2CppObject* Observable_Timer_m2629569077 (Il2CppObject * __this /* static, unused */, TimeSpan_t763862892  ___dueTime0, TimeSpan_t763862892  ___period1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UniRx.IObservable`1<System.Int64> UniRx.Observable::Timer(System.DateTimeOffset,System.TimeSpan)
extern "C"  Il2CppObject* Observable_Timer_m3524293886 (Il2CppObject * __this /* static, unused */, DateTimeOffset_t3712260035  ___dueTime0, TimeSpan_t763862892  ___period1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UniRx.IObservable`1<System.Int64> UniRx.Observable::Timer(System.TimeSpan,UniRx.IScheduler)
extern "C"  Il2CppObject* Observable_Timer_m2482108829 (Il2CppObject * __this /* static, unused */, TimeSpan_t763862892  ___dueTime0, Il2CppObject * ___scheduler1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UniRx.IObservable`1<System.Int64> UniRx.Observable::Timer(System.DateTimeOffset,UniRx.IScheduler)
extern "C"  Il2CppObject* Observable_Timer_m153806836 (Il2CppObject * __this /* static, unused */, DateTimeOffset_t3712260035  ___dueTime0, Il2CppObject * ___scheduler1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UniRx.IObservable`1<System.Int64> UniRx.Observable::Timer(System.TimeSpan,System.TimeSpan,UniRx.IScheduler)
extern "C"  Il2CppObject* Observable_Timer_m2165035527 (Il2CppObject * __this /* static, unused */, TimeSpan_t763862892  ___dueTime0, TimeSpan_t763862892  ___period1, Il2CppObject * ___scheduler2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UniRx.IObservable`1<System.Int64> UniRx.Observable::Timer(System.DateTimeOffset,System.TimeSpan,UniRx.IScheduler)
extern "C"  Il2CppObject* Observable_Timer_m3697488478 (Il2CppObject * __this /* static, unused */, DateTimeOffset_t3712260035  ___dueTime0, TimeSpan_t763862892  ___period1, Il2CppObject * ___scheduler2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UniRx.IObservable`1<UniRx.Unit> UniRx.Observable::FromCoroutine(System.Func`1<System.Collections.IEnumerator>,System.Boolean)
extern "C"  Il2CppObject* Observable_FromCoroutine_m3913040964 (Il2CppObject * __this /* static, unused */, Func_1_t1429988286 * ___coroutine0, bool ___publishEveryYield1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator UniRx.Observable::WrapEnumerator(System.Collections.IEnumerator,UniRx.IObserver`1<UniRx.Unit>,UniRx.CancellationToken,System.Boolean)
extern "C"  Il2CppObject * Observable_WrapEnumerator_m2325795528 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___enumerator0, Il2CppObject* ___observer1, CancellationToken_t1439151560  ___cancellationToken2, bool ___publishEveryYield3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UniRx.IObservable`1<UniRx.Unit> UniRx.Observable::ToObservable(System.Collections.IEnumerator,System.Boolean)
extern "C"  Il2CppObject* Observable_ToObservable_m1825470780 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___coroutine0, bool ___publishEveryYield1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UniRx.IObservable`1<System.Int64> UniRx.Observable::EveryUpdate()
extern "C"  Il2CppObject* Observable_EveryUpdate_m1588154268 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator UniRx.Observable::EveryUpdateCore(UniRx.IObserver`1<System.Int64>,UniRx.CancellationToken)
extern "C"  Il2CppObject * Observable_EveryUpdateCore_m4013844463 (Il2CppObject * __this /* static, unused */, Il2CppObject* ___observer0, CancellationToken_t1439151560  ___cancellationToken1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UniRx.IObservable`1<System.Int64> UniRx.Observable::EveryFixedUpdate()
extern "C"  Il2CppObject* Observable_EveryFixedUpdate_m136372812 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator UniRx.Observable::EveryFixedUpdateCore(UniRx.IObserver`1<System.Int64>,UniRx.CancellationToken)
extern "C"  Il2CppObject * Observable_EveryFixedUpdateCore_m3711089491 (Il2CppObject * __this /* static, unused */, Il2CppObject* ___observer0, CancellationToken_t1439151560  ___cancellationToken1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UniRx.IObservable`1<System.Int64> UniRx.Observable::EveryEndOfFrame()
extern "C"  Il2CppObject* Observable_EveryEndOfFrame_m2645149134 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator UniRx.Observable::EveryEndOfFrameCore(UniRx.IObserver`1<System.Int64>,UniRx.CancellationToken)
extern "C"  Il2CppObject * Observable_EveryEndOfFrameCore_m470912893 (Il2CppObject * __this /* static, unused */, Il2CppObject* ___observer0, CancellationToken_t1439151560  ___cancellationToken1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UniRx.IObservable`1<System.Int64> UniRx.Observable::EveryGameObjectUpdate()
extern "C"  Il2CppObject* Observable_EveryGameObjectUpdate_m1953989133 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UniRx.IObservable`1<System.Int64> UniRx.Observable::EveryLateUpdate()
extern "C"  Il2CppObject* Observable_EveryLateUpdate_m4132562786 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UniRx.IObservable`1<System.Int64> UniRx.Observable::EveryAfterUpdate()
extern "C"  Il2CppObject* Observable_EveryAfterUpdate_m279451828 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UniRx.IObservable`1<UniRx.Unit> UniRx.Observable::NextFrame(UniRx.FrameCountType)
extern "C"  Il2CppObject* Observable_NextFrame_m1403194704 (Il2CppObject * __this /* static, unused */, int32_t ___frameCountType0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator UniRx.Observable::NextFrameCore(UniRx.IObserver`1<UniRx.Unit>,UniRx.FrameCountType,UniRx.CancellationToken)
extern "C"  Il2CppObject * Observable_NextFrameCore_m1980127623 (Il2CppObject * __this /* static, unused */, Il2CppObject* ___observer0, int32_t ___frameCountType1, CancellationToken_t1439151560  ___cancellation2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UniRx.IObservable`1<System.Int64> UniRx.Observable::IntervalFrame(System.Int32,UniRx.FrameCountType)
extern "C"  Il2CppObject* Observable_IntervalFrame_m1197807937 (Il2CppObject * __this /* static, unused */, int32_t ___intervalFrameCount0, int32_t ___frameCountType1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UniRx.IObservable`1<System.Int64> UniRx.Observable::TimerFrame(System.Int32,UniRx.FrameCountType)
extern "C"  Il2CppObject* Observable_TimerFrame_m594366159 (Il2CppObject * __this /* static, unused */, int32_t ___dueTimeFrameCount0, int32_t ___frameCountType1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UniRx.IObservable`1<System.Int64> UniRx.Observable::TimerFrame(System.Int32,System.Int32,UniRx.FrameCountType)
extern "C"  Il2CppObject* Observable_TimerFrame_m1110259006 (Il2CppObject * __this /* static, unused */, int32_t ___dueTimeFrameCount0, int32_t ___periodFrameCount1, int32_t ___frameCountType2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator UniRx.Observable::TimerFrameCore(UniRx.IObserver`1<System.Int64>,System.Int32,UniRx.FrameCountType,UniRx.CancellationToken)
extern "C"  Il2CppObject * Observable_TimerFrameCore_m1545514344 (Il2CppObject * __this /* static, unused */, Il2CppObject* ___observer0, int32_t ___dueTimeFrameCount1, int32_t ___frameCountType2, CancellationToken_t1439151560  ___cancel3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator UniRx.Observable::TimerFrameCore(UniRx.IObserver`1<System.Int64>,System.Int32,System.Int32,UniRx.FrameCountType,UniRx.CancellationToken)
extern "C"  Il2CppObject * Observable_TimerFrameCore_m2679907233 (Il2CppObject * __this /* static, unused */, Il2CppObject* ___observer0, int32_t ___dueTimeFrameCount1, int32_t ___periodFrameCount2, int32_t ___frameCountType3, CancellationToken_t1439151560  ___cancel4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UniRx.IObservable`1<System.Boolean> UniRx.Observable::EveryApplicationPause()
extern "C"  Il2CppObject* Observable_EveryApplicationPause_m3046827838 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UniRx.IObservable`1<System.Boolean> UniRx.Observable::EveryApplicationFocus()
extern "C"  Il2CppObject* Observable_EveryApplicationFocus_m3145983392 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UniRx.IObservable`1<UniRx.Unit> UniRx.Observable::OnceApplicationQuit()
extern "C"  Il2CppObject* Observable_OnceApplicationQuit_m328482472 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator UniRx.Observable::<EveryUpdate>m__4F(UniRx.IObserver`1<System.Int64>,UniRx.CancellationToken)
extern "C"  Il2CppObject * Observable_U3CEveryUpdateU3Em__4F_m2940893163 (Il2CppObject * __this /* static, unused */, Il2CppObject* ___observer0, CancellationToken_t1439151560  ___cancellationToken1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator UniRx.Observable::<EveryFixedUpdate>m__50(UniRx.IObserver`1<System.Int64>,UniRx.CancellationToken)
extern "C"  Il2CppObject * Observable_U3CEveryFixedUpdateU3Em__50_m2960750058 (Il2CppObject * __this /* static, unused */, Il2CppObject* ___observer0, CancellationToken_t1439151560  ___cancellationToken1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator UniRx.Observable::<EveryEndOfFrame>m__51(UniRx.IObserver`1<System.Int64>,UniRx.CancellationToken)
extern "C"  Il2CppObject * Observable_U3CEveryEndOfFrameU3Em__51_m2316557359 (Il2CppObject * __this /* static, unused */, Il2CppObject* ___observer0, CancellationToken_t1439151560  ___cancellationToken1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 UniRx.Observable::<EveryGameObjectUpdate>m__52(System.Int64,UniRx.Unit)
extern "C"  int64_t Observable_U3CEveryGameObjectUpdateU3Em__52_m2463372556 (Il2CppObject * __this /* static, unused */, int64_t ___x0, Unit_t2558286038  ___y1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 UniRx.Observable::<EveryLateUpdate>m__53(System.Int64,UniRx.Unit)
extern "C"  int64_t Observable_U3CEveryLateUpdateU3Em__53_m2395028662 (Il2CppObject * __this /* static, unused */, int64_t ___x0, Unit_t2558286038  ___y1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator UniRx.Observable::<EveryAfterUpdate>m__54(UniRx.IObserver`1<System.Int64>,UniRx.CancellationToken)
extern "C"  Il2CppObject * Observable_U3CEveryAfterUpdateU3Em__54_m502248702 (Il2CppObject * __this /* static, unused */, Il2CppObject* ___observer0, CancellationToken_t1439151560  ___cancellationToken1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
