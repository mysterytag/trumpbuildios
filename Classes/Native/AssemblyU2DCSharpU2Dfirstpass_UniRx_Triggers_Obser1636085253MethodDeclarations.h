﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Triggers.ObservableMoveTrigger
struct ObservableMoveTrigger_t1636085253;
// UnityEngine.EventSystems.AxisEventData
struct AxisEventData_t552897310;
// UniRx.IObservable`1<UnityEngine.EventSystems.AxisEventData>
struct IObservable_1_t311695674;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_AxisEventDa552897310.h"

// System.Void UniRx.Triggers.ObservableMoveTrigger::.ctor()
extern "C"  void ObservableMoveTrigger__ctor_m50459718 (ObservableMoveTrigger_t1636085253 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.Triggers.ObservableMoveTrigger::UnityEngine.EventSystems.IMoveHandler.OnMove(UnityEngine.EventSystems.AxisEventData)
extern "C"  void ObservableMoveTrigger_UnityEngine_EventSystems_IMoveHandler_OnMove_m4232033321 (ObservableMoveTrigger_t1636085253 * __this, AxisEventData_t552897310 * ___eventData0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UniRx.IObservable`1<UnityEngine.EventSystems.AxisEventData> UniRx.Triggers.ObservableMoveTrigger::OnMoveAsObservable()
extern "C"  Il2CppObject* ObservableMoveTrigger_OnMoveAsObservable_m4291158149 (ObservableMoveTrigger_t1636085253 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.Triggers.ObservableMoveTrigger::RaiseOnCompletedOnDestroy()
extern "C"  void ObservableMoveTrigger_RaiseOnCompletedOnDestroy_m3530026111 (ObservableMoveTrigger_t1636085253 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
