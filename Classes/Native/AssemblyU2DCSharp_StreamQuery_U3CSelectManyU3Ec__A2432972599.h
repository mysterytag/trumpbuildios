﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Object
struct Il2CppObject;
// StreamQuery/<SelectMany>c__AnonStorey13D`3<System.Object,System.Object,System.Object>
struct U3CSelectManyU3Ec__AnonStorey13D_3_t2765353606;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// StreamQuery/<SelectMany>c__AnonStorey13D`3/<SelectMany>c__AnonStorey13E`3<System.Object,System.Object,System.Object>
struct  U3CSelectManyU3Ec__AnonStorey13E_3_t2432972599  : public Il2CppObject
{
public:
	// T StreamQuery/<SelectMany>c__AnonStorey13D`3/<SelectMany>c__AnonStorey13E`3::x
	Il2CppObject * ___x_0;
	// StreamQuery/<SelectMany>c__AnonStorey13D`3<T,TC,TR> StreamQuery/<SelectMany>c__AnonStorey13D`3/<SelectMany>c__AnonStorey13E`3::<>f__ref$317
	U3CSelectManyU3Ec__AnonStorey13D_3_t2765353606 * ___U3CU3Ef__refU24317_1;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(U3CSelectManyU3Ec__AnonStorey13E_3_t2432972599, ___x_0)); }
	inline Il2CppObject * get_x_0() const { return ___x_0; }
	inline Il2CppObject ** get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(Il2CppObject * value)
	{
		___x_0 = value;
		Il2CppCodeGenWriteBarrier(&___x_0, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__refU24317_1() { return static_cast<int32_t>(offsetof(U3CSelectManyU3Ec__AnonStorey13E_3_t2432972599, ___U3CU3Ef__refU24317_1)); }
	inline U3CSelectManyU3Ec__AnonStorey13D_3_t2765353606 * get_U3CU3Ef__refU24317_1() const { return ___U3CU3Ef__refU24317_1; }
	inline U3CSelectManyU3Ec__AnonStorey13D_3_t2765353606 ** get_address_of_U3CU3Ef__refU24317_1() { return &___U3CU3Ef__refU24317_1; }
	inline void set_U3CU3Ef__refU24317_1(U3CSelectManyU3Ec__AnonStorey13D_3_t2765353606 * value)
	{
		___U3CU3Ef__refU24317_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__refU24317_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
