﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.OperatorObservableBase`1/<Subscribe>c__AnonStorey5B<UniRx.CollectionAddEvent`1<System.Object>>
struct U3CSubscribeU3Ec__AnonStorey5B_t2014528716;

#include "codegen/il2cpp-codegen.h"

// System.Void UniRx.Operators.OperatorObservableBase`1/<Subscribe>c__AnonStorey5B<UniRx.CollectionAddEvent`1<System.Object>>::.ctor()
extern "C"  void U3CSubscribeU3Ec__AnonStorey5B__ctor_m822342948_gshared (U3CSubscribeU3Ec__AnonStorey5B_t2014528716 * __this, const MethodInfo* method);
#define U3CSubscribeU3Ec__AnonStorey5B__ctor_m822342948(__this, method) ((  void (*) (U3CSubscribeU3Ec__AnonStorey5B_t2014528716 *, const MethodInfo*))U3CSubscribeU3Ec__AnonStorey5B__ctor_m822342948_gshared)(__this, method)
// System.Void UniRx.Operators.OperatorObservableBase`1/<Subscribe>c__AnonStorey5B<UniRx.CollectionAddEvent`1<System.Object>>::<>m__73()
extern "C"  void U3CSubscribeU3Ec__AnonStorey5B_U3CU3Em__73_m2305812169_gshared (U3CSubscribeU3Ec__AnonStorey5B_t2014528716 * __this, const MethodInfo* method);
#define U3CSubscribeU3Ec__AnonStorey5B_U3CU3Em__73_m2305812169(__this, method) ((  void (*) (U3CSubscribeU3Ec__AnonStorey5B_t2014528716 *, const MethodInfo*))U3CSubscribeU3Ec__AnonStorey5B_U3CU3Em__73_m2305812169_gshared)(__this, method)
