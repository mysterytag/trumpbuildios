﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.ClientModels.UserGoogleInfo
struct UserGoogleInfo_t1689260258;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String968488902.h"

// System.Void PlayFab.ClientModels.UserGoogleInfo::.ctor()
extern "C"  void UserGoogleInfo__ctor_m3176800283 (UserGoogleInfo_t1689260258 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PlayFab.ClientModels.UserGoogleInfo::get_GoogleId()
extern "C"  String_t* UserGoogleInfo_get_GoogleId_m646298499 (UserGoogleInfo_t1689260258 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.UserGoogleInfo::set_GoogleId(System.String)
extern "C"  void UserGoogleInfo_set_GoogleId_m2925238088 (UserGoogleInfo_t1689260258 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PlayFab.ClientModels.UserGoogleInfo::get_GoogleEmail()
extern "C"  String_t* UserGoogleInfo_get_GoogleEmail_m597463062 (UserGoogleInfo_t1689260258 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.UserGoogleInfo::set_GoogleEmail(System.String)
extern "C"  void UserGoogleInfo_set_GoogleEmail_m744810275 (UserGoogleInfo_t1689260258 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PlayFab.ClientModels.UserGoogleInfo::get_GoogleLocale()
extern "C"  String_t* UserGoogleInfo_get_GoogleLocale_m2481192322 (UserGoogleInfo_t1689260258 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.UserGoogleInfo::set_GoogleLocale(System.String)
extern "C"  void UserGoogleInfo_set_GoogleLocale_m1902849833 (UserGoogleInfo_t1689260258 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PlayFab.ClientModels.UserGoogleInfo::get_GoogleGender()
extern "C"  String_t* UserGoogleInfo_get_GoogleGender_m2389468201 (UserGoogleInfo_t1689260258 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.UserGoogleInfo::set_GoogleGender(System.String)
extern "C"  void UserGoogleInfo_set_GoogleGender_m2313621474 (UserGoogleInfo_t1689260258 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
