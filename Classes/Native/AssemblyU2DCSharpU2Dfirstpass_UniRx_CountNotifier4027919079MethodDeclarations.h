﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.CountNotifier
struct CountNotifier_t4027919079;
// System.IDisposable
struct IDisposable_t1628921374;
// UniRx.IObserver`1<UniRx.CountChangedStatus>
struct IObserver_1_t1688259328;

#include "codegen/il2cpp-codegen.h"

// System.Void UniRx.CountNotifier::.ctor(System.Int32)
extern "C"  void CountNotifier__ctor_m2875335723 (CountNotifier_t4027919079 * __this, int32_t ___max0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UniRx.CountNotifier::get_Max()
extern "C"  int32_t CountNotifier_get_Max_m681577193 (CountNotifier_t4027919079 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UniRx.CountNotifier::get_Count()
extern "C"  int32_t CountNotifier_get_Count_m2275747540 (CountNotifier_t4027919079 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.CountNotifier::set_Count(System.Int32)
extern "C"  void CountNotifier_set_Count_m2753574971 (CountNotifier_t4027919079 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IDisposable UniRx.CountNotifier::Increment(System.Int32)
extern "C"  Il2CppObject * CountNotifier_Increment_m2323311795 (CountNotifier_t4027919079 * __this, int32_t ___incrementCount0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.CountNotifier::Decrement(System.Int32)
extern "C"  void CountNotifier_Decrement_m482281500 (CountNotifier_t4027919079 * __this, int32_t ___decrementCount0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IDisposable UniRx.CountNotifier::Subscribe(UniRx.IObserver`1<UniRx.CountChangedStatus>)
extern "C"  Il2CppObject * CountNotifier_Subscribe_m1347680650 (CountNotifier_t4027919079 * __this, Il2CppObject* ___observer0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
