﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UniRx.Operators.ZipObservable`5<System.Object,System.Object,System.Object,System.Object,System.Object>
struct ZipObservable_5_t589463103;
// System.Object
struct Il2CppObject;
// System.Collections.Generic.Queue`1<System.Object>
struct Queue_1_t2545193960;

#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_NthZ3759589597.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Operators.ZipObservable`5/Zip<System.Object,System.Object,System.Object,System.Object,System.Object>
struct  Zip_t2920666770  : public NthZipObserverBase_1_t3759589597
{
public:
	// UniRx.Operators.ZipObservable`5<T1,T2,T3,T4,TR> UniRx.Operators.ZipObservable`5/Zip::parent
	ZipObservable_5_t589463103 * ___parent_5;
	// System.Object UniRx.Operators.ZipObservable`5/Zip::gate
	Il2CppObject * ___gate_6;
	// System.Collections.Generic.Queue`1<T1> UniRx.Operators.ZipObservable`5/Zip::q1
	Queue_1_t2545193960 * ___q1_7;
	// System.Collections.Generic.Queue`1<T2> UniRx.Operators.ZipObservable`5/Zip::q2
	Queue_1_t2545193960 * ___q2_8;
	// System.Collections.Generic.Queue`1<T3> UniRx.Operators.ZipObservable`5/Zip::q3
	Queue_1_t2545193960 * ___q3_9;
	// System.Collections.Generic.Queue`1<T4> UniRx.Operators.ZipObservable`5/Zip::q4
	Queue_1_t2545193960 * ___q4_10;

public:
	inline static int32_t get_offset_of_parent_5() { return static_cast<int32_t>(offsetof(Zip_t2920666770, ___parent_5)); }
	inline ZipObservable_5_t589463103 * get_parent_5() const { return ___parent_5; }
	inline ZipObservable_5_t589463103 ** get_address_of_parent_5() { return &___parent_5; }
	inline void set_parent_5(ZipObservable_5_t589463103 * value)
	{
		___parent_5 = value;
		Il2CppCodeGenWriteBarrier(&___parent_5, value);
	}

	inline static int32_t get_offset_of_gate_6() { return static_cast<int32_t>(offsetof(Zip_t2920666770, ___gate_6)); }
	inline Il2CppObject * get_gate_6() const { return ___gate_6; }
	inline Il2CppObject ** get_address_of_gate_6() { return &___gate_6; }
	inline void set_gate_6(Il2CppObject * value)
	{
		___gate_6 = value;
		Il2CppCodeGenWriteBarrier(&___gate_6, value);
	}

	inline static int32_t get_offset_of_q1_7() { return static_cast<int32_t>(offsetof(Zip_t2920666770, ___q1_7)); }
	inline Queue_1_t2545193960 * get_q1_7() const { return ___q1_7; }
	inline Queue_1_t2545193960 ** get_address_of_q1_7() { return &___q1_7; }
	inline void set_q1_7(Queue_1_t2545193960 * value)
	{
		___q1_7 = value;
		Il2CppCodeGenWriteBarrier(&___q1_7, value);
	}

	inline static int32_t get_offset_of_q2_8() { return static_cast<int32_t>(offsetof(Zip_t2920666770, ___q2_8)); }
	inline Queue_1_t2545193960 * get_q2_8() const { return ___q2_8; }
	inline Queue_1_t2545193960 ** get_address_of_q2_8() { return &___q2_8; }
	inline void set_q2_8(Queue_1_t2545193960 * value)
	{
		___q2_8 = value;
		Il2CppCodeGenWriteBarrier(&___q2_8, value);
	}

	inline static int32_t get_offset_of_q3_9() { return static_cast<int32_t>(offsetof(Zip_t2920666770, ___q3_9)); }
	inline Queue_1_t2545193960 * get_q3_9() const { return ___q3_9; }
	inline Queue_1_t2545193960 ** get_address_of_q3_9() { return &___q3_9; }
	inline void set_q3_9(Queue_1_t2545193960 * value)
	{
		___q3_9 = value;
		Il2CppCodeGenWriteBarrier(&___q3_9, value);
	}

	inline static int32_t get_offset_of_q4_10() { return static_cast<int32_t>(offsetof(Zip_t2920666770, ___q4_10)); }
	inline Queue_1_t2545193960 * get_q4_10() const { return ___q4_10; }
	inline Queue_1_t2545193960 ** get_address_of_q4_10() { return &___q4_10; }
	inline void set_q4_10(Queue_1_t2545193960 * value)
	{
		___q4_10 = value;
		Il2CppCodeGenWriteBarrier(&___q4_10, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
