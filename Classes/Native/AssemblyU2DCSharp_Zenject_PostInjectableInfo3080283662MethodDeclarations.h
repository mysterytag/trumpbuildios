﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Zenject.PostInjectableInfo
struct PostInjectableInfo_t3080283662;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.Collections.Generic.List`1<Zenject.InjectableInfo>
struct List_1_t1944668743;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Reflection_MethodInfo3461221277.h"

// System.Void Zenject.PostInjectableInfo::.ctor(System.Reflection.MethodInfo,System.Collections.Generic.List`1<Zenject.InjectableInfo>)
extern "C"  void PostInjectableInfo__ctor_m595808628 (PostInjectableInfo_t3080283662 * __this, MethodInfo_t * ___methodInfo0, List_1_t1944668743 * ___injectableInfo1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
