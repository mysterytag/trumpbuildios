﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.EmptyObservable`1<UniRx.CollectionMoveEvent`1<System.Object>>
struct EmptyObservable_1_t1310656250;
// UniRx.IScheduler
struct IScheduler_t2938318244;
// System.IDisposable
struct IDisposable_t1628921374;
// UniRx.IObserver`1<UniRx.CollectionMoveEvent`1<System.Object>>
struct IObserver_1_t833465454;

#include "codegen/il2cpp-codegen.h"

// System.Void UniRx.Operators.EmptyObservable`1<UniRx.CollectionMoveEvent`1<System.Object>>::.ctor(UniRx.IScheduler)
extern "C"  void EmptyObservable_1__ctor_m3323180857_gshared (EmptyObservable_1_t1310656250 * __this, Il2CppObject * ___scheduler0, const MethodInfo* method);
#define EmptyObservable_1__ctor_m3323180857(__this, ___scheduler0, method) ((  void (*) (EmptyObservable_1_t1310656250 *, Il2CppObject *, const MethodInfo*))EmptyObservable_1__ctor_m3323180857_gshared)(__this, ___scheduler0, method)
// System.IDisposable UniRx.Operators.EmptyObservable`1<UniRx.CollectionMoveEvent`1<System.Object>>::SubscribeCore(UniRx.IObserver`1<T>,System.IDisposable)
extern "C"  Il2CppObject * EmptyObservable_1_SubscribeCore_m2884555781_gshared (EmptyObservable_1_t1310656250 * __this, Il2CppObject* ___observer0, Il2CppObject * ___cancel1, const MethodInfo* method);
#define EmptyObservable_1_SubscribeCore_m2884555781(__this, ___observer0, ___cancel1, method) ((  Il2CppObject * (*) (EmptyObservable_1_t1310656250 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))EmptyObservable_1_SubscribeCore_m2884555781_gshared)(__this, ___observer0, ___cancel1, method)
