﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UniRx.Operators.TakeLastObservable`1<System.Object>
struct TakeLastObservable_1_t3265267047;
// System.Collections.Generic.Queue`1<UniRx.TimeInterval`1<System.Object>>
struct Queue_1_t2416153082;

#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Oper1187768149.h"
#include "mscorlib_System_DateTimeOffset3712260035.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Operators.TakeLastObservable`1/TakeLast_<System.Object>
struct  TakeLast__t2176345419  : public OperatorObserverBase_2_t1187768149
{
public:
	// System.DateTimeOffset UniRx.Operators.TakeLastObservable`1/TakeLast_::startTime
	DateTimeOffset_t3712260035  ___startTime_2;
	// UniRx.Operators.TakeLastObservable`1<T> UniRx.Operators.TakeLastObservable`1/TakeLast_::parent
	TakeLastObservable_1_t3265267047 * ___parent_3;
	// System.Collections.Generic.Queue`1<UniRx.TimeInterval`1<T>> UniRx.Operators.TakeLastObservable`1/TakeLast_::q
	Queue_1_t2416153082 * ___q_4;

public:
	inline static int32_t get_offset_of_startTime_2() { return static_cast<int32_t>(offsetof(TakeLast__t2176345419, ___startTime_2)); }
	inline DateTimeOffset_t3712260035  get_startTime_2() const { return ___startTime_2; }
	inline DateTimeOffset_t3712260035 * get_address_of_startTime_2() { return &___startTime_2; }
	inline void set_startTime_2(DateTimeOffset_t3712260035  value)
	{
		___startTime_2 = value;
	}

	inline static int32_t get_offset_of_parent_3() { return static_cast<int32_t>(offsetof(TakeLast__t2176345419, ___parent_3)); }
	inline TakeLastObservable_1_t3265267047 * get_parent_3() const { return ___parent_3; }
	inline TakeLastObservable_1_t3265267047 ** get_address_of_parent_3() { return &___parent_3; }
	inline void set_parent_3(TakeLastObservable_1_t3265267047 * value)
	{
		___parent_3 = value;
		Il2CppCodeGenWriteBarrier(&___parent_3, value);
	}

	inline static int32_t get_offset_of_q_4() { return static_cast<int32_t>(offsetof(TakeLast__t2176345419, ___q_4)); }
	inline Queue_1_t2416153082 * get_q_4() const { return ___q_4; }
	inline Queue_1_t2416153082 ** get_address_of_q_4() { return &___q_4; }
	inline void set_q_4(Queue_1_t2416153082 * value)
	{
		___q_4 = value;
		Il2CppCodeGenWriteBarrier(&___q_4, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
