﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// PlayfabConnector
struct PlayfabConnector_t194421338;
// GlobalStat
struct GlobalStat_t1134678199;
// BarygaOpenIABManager
struct BarygaOpenIABManager_t3621535309;

#include "UnityEngine_UnityEngine_MonoBehaviour3012272455.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PlayfabConnector
struct  PlayfabConnector_t194421338  : public MonoBehaviour_t3012272455
{
public:
	// System.Boolean PlayfabConnector::connected
	bool ___connected_3;
	// System.Boolean PlayfabConnector::inProcess
	bool ___inProcess_4;
	// GlobalStat PlayfabConnector::_globalStat
	GlobalStat_t1134678199 * ____globalStat_5;
	// System.Single PlayfabConnector::delay
	float ___delay_6;
	// BarygaOpenIABManager PlayfabConnector::BarygaOpenIABManager
	BarygaOpenIABManager_t3621535309 * ___BarygaOpenIABManager_7;

public:
	inline static int32_t get_offset_of_connected_3() { return static_cast<int32_t>(offsetof(PlayfabConnector_t194421338, ___connected_3)); }
	inline bool get_connected_3() const { return ___connected_3; }
	inline bool* get_address_of_connected_3() { return &___connected_3; }
	inline void set_connected_3(bool value)
	{
		___connected_3 = value;
	}

	inline static int32_t get_offset_of_inProcess_4() { return static_cast<int32_t>(offsetof(PlayfabConnector_t194421338, ___inProcess_4)); }
	inline bool get_inProcess_4() const { return ___inProcess_4; }
	inline bool* get_address_of_inProcess_4() { return &___inProcess_4; }
	inline void set_inProcess_4(bool value)
	{
		___inProcess_4 = value;
	}

	inline static int32_t get_offset_of__globalStat_5() { return static_cast<int32_t>(offsetof(PlayfabConnector_t194421338, ____globalStat_5)); }
	inline GlobalStat_t1134678199 * get__globalStat_5() const { return ____globalStat_5; }
	inline GlobalStat_t1134678199 ** get_address_of__globalStat_5() { return &____globalStat_5; }
	inline void set__globalStat_5(GlobalStat_t1134678199 * value)
	{
		____globalStat_5 = value;
		Il2CppCodeGenWriteBarrier(&____globalStat_5, value);
	}

	inline static int32_t get_offset_of_delay_6() { return static_cast<int32_t>(offsetof(PlayfabConnector_t194421338, ___delay_6)); }
	inline float get_delay_6() const { return ___delay_6; }
	inline float* get_address_of_delay_6() { return &___delay_6; }
	inline void set_delay_6(float value)
	{
		___delay_6 = value;
	}

	inline static int32_t get_offset_of_BarygaOpenIABManager_7() { return static_cast<int32_t>(offsetof(PlayfabConnector_t194421338, ___BarygaOpenIABManager_7)); }
	inline BarygaOpenIABManager_t3621535309 * get_BarygaOpenIABManager_7() const { return ___BarygaOpenIABManager_7; }
	inline BarygaOpenIABManager_t3621535309 ** get_address_of_BarygaOpenIABManager_7() { return &___BarygaOpenIABManager_7; }
	inline void set_BarygaOpenIABManager_7(BarygaOpenIABManager_t3621535309 * value)
	{
		___BarygaOpenIABManager_7 = value;
		Il2CppCodeGenWriteBarrier(&___BarygaOpenIABManager_7, value);
	}
};

struct PlayfabConnector_t194421338_StaticFields
{
public:
	// PlayfabConnector PlayfabConnector::_instance
	PlayfabConnector_t194421338 * ____instance_2;

public:
	inline static int32_t get_offset_of__instance_2() { return static_cast<int32_t>(offsetof(PlayfabConnector_t194421338_StaticFields, ____instance_2)); }
	inline PlayfabConnector_t194421338 * get__instance_2() const { return ____instance_2; }
	inline PlayfabConnector_t194421338 ** get_address_of__instance_2() { return &____instance_2; }
	inline void set__instance_2(PlayfabConnector_t194421338 * value)
	{
		____instance_2 = value;
		Il2CppCodeGenWriteBarrier(&____instance_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
