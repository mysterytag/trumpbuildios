﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Zenject.GameObjectFactory`2<System.Object,System.Object>
struct GameObjectFactory_2_t546746903;
// System.Object
struct Il2CppObject;
// System.Collections.Generic.IEnumerable`1<Zenject.ZenjectResolveException>
struct IEnumerable_1_t4073207355;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"

// System.Void Zenject.GameObjectFactory`2<System.Object,System.Object>::.ctor()
extern "C"  void GameObjectFactory_2__ctor_m2680023517_gshared (GameObjectFactory_2_t546746903 * __this, const MethodInfo* method);
#define GameObjectFactory_2__ctor_m2680023517(__this, method) ((  void (*) (GameObjectFactory_2_t546746903 *, const MethodInfo*))GameObjectFactory_2__ctor_m2680023517_gshared)(__this, method)
// TValue Zenject.GameObjectFactory`2<System.Object,System.Object>::Create(TParam1)
extern "C"  Il2CppObject * GameObjectFactory_2_Create_m1408180983_gshared (GameObjectFactory_2_t546746903 * __this, Il2CppObject * ___param0, const MethodInfo* method);
#define GameObjectFactory_2_Create_m1408180983(__this, ___param0, method) ((  Il2CppObject * (*) (GameObjectFactory_2_t546746903 *, Il2CppObject *, const MethodInfo*))GameObjectFactory_2_Create_m1408180983_gshared)(__this, ___param0, method)
// System.Collections.Generic.IEnumerable`1<Zenject.ZenjectResolveException> Zenject.GameObjectFactory`2<System.Object,System.Object>::Validate()
extern "C"  Il2CppObject* GameObjectFactory_2_Validate_m794844732_gshared (GameObjectFactory_2_t546746903 * __this, const MethodInfo* method);
#define GameObjectFactory_2_Validate_m794844732(__this, method) ((  Il2CppObject* (*) (GameObjectFactory_2_t546746903 *, const MethodInfo*))GameObjectFactory_2_Validate_m794844732_gshared)(__this, method)
