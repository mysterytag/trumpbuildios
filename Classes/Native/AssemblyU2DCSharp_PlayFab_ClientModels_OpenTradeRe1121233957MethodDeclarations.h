﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.ClientModels.OpenTradeRequest
struct OpenTradeRequest_t1121233957;
// System.Collections.Generic.List`1<System.String>
struct List_1_t1765447871;

#include "codegen/il2cpp-codegen.h"

// System.Void PlayFab.ClientModels.OpenTradeRequest::.ctor()
extern "C"  void OpenTradeRequest__ctor_m3994623416 (OpenTradeRequest_t1121233957 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<System.String> PlayFab.ClientModels.OpenTradeRequest::get_OfferedInventoryInstanceIds()
extern "C"  List_1_t1765447871 * OpenTradeRequest_get_OfferedInventoryInstanceIds_m4102934368 (OpenTradeRequest_t1121233957 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.OpenTradeRequest::set_OfferedInventoryInstanceIds(System.Collections.Generic.List`1<System.String>)
extern "C"  void OpenTradeRequest_set_OfferedInventoryInstanceIds_m188381359 (OpenTradeRequest_t1121233957 * __this, List_1_t1765447871 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<System.String> PlayFab.ClientModels.OpenTradeRequest::get_RequestedCatalogItemIds()
extern "C"  List_1_t1765447871 * OpenTradeRequest_get_RequestedCatalogItemIds_m2931863480 (OpenTradeRequest_t1121233957 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.OpenTradeRequest::set_RequestedCatalogItemIds(System.Collections.Generic.List`1<System.String>)
extern "C"  void OpenTradeRequest_set_RequestedCatalogItemIds_m1638993543 (OpenTradeRequest_t1121233957 * __this, List_1_t1765447871 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<System.String> PlayFab.ClientModels.OpenTradeRequest::get_AllowedPlayerIds()
extern "C"  List_1_t1765447871 * OpenTradeRequest_get_AllowedPlayerIds_m3849606483 (OpenTradeRequest_t1121233957 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.OpenTradeRequest::set_AllowedPlayerIds(System.Collections.Generic.List`1<System.String>)
extern "C"  void OpenTradeRequest_set_AllowedPlayerIds_m2852396364 (OpenTradeRequest_t1121233957 * __this, List_1_t1765447871 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
