﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.WaitForEndOfFrame
struct WaitForEndOfFrame_t1917318876;
// UnityEngine.WaitForFixedUpdate
struct WaitForFixedUpdate_t896427542;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.YieldInstructionCache
struct  YieldInstructionCache_t1940766803  : public Il2CppObject
{
public:

public:
};

struct YieldInstructionCache_t1940766803_StaticFields
{
public:
	// UnityEngine.WaitForEndOfFrame UniRx.YieldInstructionCache::WaitForEndOfFrame
	WaitForEndOfFrame_t1917318876 * ___WaitForEndOfFrame_0;
	// UnityEngine.WaitForFixedUpdate UniRx.YieldInstructionCache::WaitForFixedUpdate
	WaitForFixedUpdate_t896427542 * ___WaitForFixedUpdate_1;

public:
	inline static int32_t get_offset_of_WaitForEndOfFrame_0() { return static_cast<int32_t>(offsetof(YieldInstructionCache_t1940766803_StaticFields, ___WaitForEndOfFrame_0)); }
	inline WaitForEndOfFrame_t1917318876 * get_WaitForEndOfFrame_0() const { return ___WaitForEndOfFrame_0; }
	inline WaitForEndOfFrame_t1917318876 ** get_address_of_WaitForEndOfFrame_0() { return &___WaitForEndOfFrame_0; }
	inline void set_WaitForEndOfFrame_0(WaitForEndOfFrame_t1917318876 * value)
	{
		___WaitForEndOfFrame_0 = value;
		Il2CppCodeGenWriteBarrier(&___WaitForEndOfFrame_0, value);
	}

	inline static int32_t get_offset_of_WaitForFixedUpdate_1() { return static_cast<int32_t>(offsetof(YieldInstructionCache_t1940766803_StaticFields, ___WaitForFixedUpdate_1)); }
	inline WaitForFixedUpdate_t896427542 * get_WaitForFixedUpdate_1() const { return ___WaitForFixedUpdate_1; }
	inline WaitForFixedUpdate_t896427542 ** get_address_of_WaitForFixedUpdate_1() { return &___WaitForFixedUpdate_1; }
	inline void set_WaitForFixedUpdate_1(WaitForFixedUpdate_t896427542 * value)
	{
		___WaitForFixedUpdate_1 = value;
		Il2CppCodeGenWriteBarrier(&___WaitForFixedUpdate_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
