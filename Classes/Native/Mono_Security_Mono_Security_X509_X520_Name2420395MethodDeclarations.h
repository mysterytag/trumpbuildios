﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Mono.Security.X509.X520/Name
struct Name_t2420395;

#include "codegen/il2cpp-codegen.h"

// System.Void Mono.Security.X509.X520/Name::.ctor()
extern "C"  void Name__ctor_m1971138686 (Name_t2420395 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
