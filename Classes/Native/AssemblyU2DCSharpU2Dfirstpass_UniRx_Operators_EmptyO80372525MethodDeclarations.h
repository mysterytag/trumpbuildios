﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.EmptyObservable`1/Empty<UniRx.CollectionAddEvent`1<System.Object>>
struct Empty_t80372525;
// UniRx.IObserver`1<UniRx.CollectionAddEvent`1<System.Object>>
struct IObserver_1_t333553594;
// System.IDisposable
struct IDisposable_t1628921374;
// System.Exception
struct Exception_t1967233988;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_CollectionAddE2416521987.h"
#include "mscorlib_System_Exception1967233988.h"

// System.Void UniRx.Operators.EmptyObservable`1/Empty<UniRx.CollectionAddEvent`1<System.Object>>::.ctor(UniRx.IObserver`1<T>,System.IDisposable)
extern "C"  void Empty__ctor_m213159153_gshared (Empty_t80372525 * __this, Il2CppObject* ___observer0, Il2CppObject * ___cancel1, const MethodInfo* method);
#define Empty__ctor_m213159153(__this, ___observer0, ___cancel1, method) ((  void (*) (Empty_t80372525 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))Empty__ctor_m213159153_gshared)(__this, ___observer0, ___cancel1, method)
// System.Void UniRx.Operators.EmptyObservable`1/Empty<UniRx.CollectionAddEvent`1<System.Object>>::OnNext(T)
extern "C"  void Empty_OnNext_m2459765761_gshared (Empty_t80372525 * __this, CollectionAddEvent_1_t2416521987  ___value0, const MethodInfo* method);
#define Empty_OnNext_m2459765761(__this, ___value0, method) ((  void (*) (Empty_t80372525 *, CollectionAddEvent_1_t2416521987 , const MethodInfo*))Empty_OnNext_m2459765761_gshared)(__this, ___value0, method)
// System.Void UniRx.Operators.EmptyObservable`1/Empty<UniRx.CollectionAddEvent`1<System.Object>>::OnError(System.Exception)
extern "C"  void Empty_OnError_m3941795088_gshared (Empty_t80372525 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method);
#define Empty_OnError_m3941795088(__this, ___error0, method) ((  void (*) (Empty_t80372525 *, Exception_t1967233988 *, const MethodInfo*))Empty_OnError_m3941795088_gshared)(__this, ___error0, method)
// System.Void UniRx.Operators.EmptyObservable`1/Empty<UniRx.CollectionAddEvent`1<System.Object>>::OnCompleted()
extern "C"  void Empty_OnCompleted_m4199862371_gshared (Empty_t80372525 * __this, const MethodInfo* method);
#define Empty_OnCompleted_m4199862371(__this, method) ((  void (*) (Empty_t80372525 *, const MethodInfo*))Empty_OnCompleted_m4199862371_gshared)(__this, method)
