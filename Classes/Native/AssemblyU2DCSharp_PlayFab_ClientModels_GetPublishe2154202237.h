﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Generic.Dictionary`2<System.String,System.String>
struct Dictionary_2_t2606186806;

#include "AssemblyU2DCSharp_PlayFab_Internal_PlayFabResultCom379512675.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PlayFab.ClientModels.GetPublisherDataResult
struct  GetPublisherDataResult_t2154202237  : public PlayFabResultCommon_t379512675
{
public:
	// System.Collections.Generic.Dictionary`2<System.String,System.String> PlayFab.ClientModels.GetPublisherDataResult::<Data>k__BackingField
	Dictionary_2_t2606186806 * ___U3CDataU3Ek__BackingField_2;

public:
	inline static int32_t get_offset_of_U3CDataU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(GetPublisherDataResult_t2154202237, ___U3CDataU3Ek__BackingField_2)); }
	inline Dictionary_2_t2606186806 * get_U3CDataU3Ek__BackingField_2() const { return ___U3CDataU3Ek__BackingField_2; }
	inline Dictionary_2_t2606186806 ** get_address_of_U3CDataU3Ek__BackingField_2() { return &___U3CDataU3Ek__BackingField_2; }
	inline void set_U3CDataU3Ek__BackingField_2(Dictionary_2_t2606186806 * value)
	{
		___U3CDataU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier(&___U3CDataU3Ek__BackingField_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
