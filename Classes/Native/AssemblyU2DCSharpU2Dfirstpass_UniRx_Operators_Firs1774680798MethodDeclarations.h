﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.FirstObservable`1/First_<System.Int64>
struct First__t1774680798;
// UniRx.Operators.FirstObservable`1<System.Int64>
struct FirstObservable_1_t575002000;
// UniRx.IObserver`1<System.Int64>
struct IObserver_1_t764446489;
// System.IDisposable
struct IDisposable_t1628921374;
// System.Exception
struct Exception_t1967233988;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Exception1967233988.h"

// System.Void UniRx.Operators.FirstObservable`1/First_<System.Int64>::.ctor(UniRx.Operators.FirstObservable`1<T>,UniRx.IObserver`1<T>,System.IDisposable)
extern "C"  void First___ctor_m276491221_gshared (First__t1774680798 * __this, FirstObservable_1_t575002000 * ___parent0, Il2CppObject* ___observer1, Il2CppObject * ___cancel2, const MethodInfo* method);
#define First___ctor_m276491221(__this, ___parent0, ___observer1, ___cancel2, method) ((  void (*) (First__t1774680798 *, FirstObservable_1_t575002000 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))First___ctor_m276491221_gshared)(__this, ___parent0, ___observer1, ___cancel2, method)
// System.Void UniRx.Operators.FirstObservable`1/First_<System.Int64>::OnNext(T)
extern "C"  void First__OnNext_m3925266226_gshared (First__t1774680798 * __this, int64_t ___value0, const MethodInfo* method);
#define First__OnNext_m3925266226(__this, ___value0, method) ((  void (*) (First__t1774680798 *, int64_t, const MethodInfo*))First__OnNext_m3925266226_gshared)(__this, ___value0, method)
// System.Void UniRx.Operators.FirstObservable`1/First_<System.Int64>::OnError(System.Exception)
extern "C"  void First__OnError_m1156204097_gshared (First__t1774680798 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method);
#define First__OnError_m1156204097(__this, ___error0, method) ((  void (*) (First__t1774680798 *, Exception_t1967233988 *, const MethodInfo*))First__OnError_m1156204097_gshared)(__this, ___error0, method)
// System.Void UniRx.Operators.FirstObservable`1/First_<System.Int64>::OnCompleted()
extern "C"  void First__OnCompleted_m3150418708_gshared (First__t1774680798 * __this, const MethodInfo* method);
#define First__OnCompleted_m3150418708(__this, method) ((  void (*) (First__t1774680798 *, const MethodInfo*))First__OnCompleted_m3150418708_gshared)(__this, method)
