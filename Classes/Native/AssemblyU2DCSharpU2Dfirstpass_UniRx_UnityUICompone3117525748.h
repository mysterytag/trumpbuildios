﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.UI.ScrollRect
struct ScrollRect_t1048578170;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.UnityUIComponentExtensions/<OnValueChangedAsObservable>c__AnonStoreyA9
struct  U3COnValueChangedAsObservableU3Ec__AnonStoreyA9_t3117525748  : public Il2CppObject
{
public:
	// UnityEngine.UI.ScrollRect UniRx.UnityUIComponentExtensions/<OnValueChangedAsObservable>c__AnonStoreyA9::scrollRect
	ScrollRect_t1048578170 * ___scrollRect_0;

public:
	inline static int32_t get_offset_of_scrollRect_0() { return static_cast<int32_t>(offsetof(U3COnValueChangedAsObservableU3Ec__AnonStoreyA9_t3117525748, ___scrollRect_0)); }
	inline ScrollRect_t1048578170 * get_scrollRect_0() const { return ___scrollRect_0; }
	inline ScrollRect_t1048578170 ** get_address_of_scrollRect_0() { return &___scrollRect_0; }
	inline void set_scrollRect_0(ScrollRect_t1048578170 * value)
	{
		___scrollRect_0 = value;
		Il2CppCodeGenWriteBarrier(&___scrollRect_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
