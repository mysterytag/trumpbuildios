﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Zenject.InjectContext
struct InjectContext_t3456483891;
// Zenject.DiContainer
struct DiContainer_t2383114449;
// System.Type
struct Type_t;
// System.String
struct String_t;
// System.Object
struct Il2CppObject;
// System.Collections.Generic.IEnumerable`1<Zenject.InjectContext>
struct IEnumerable_1_t2033670951;
// System.Collections.Generic.IEnumerable`1<System.Type>
struct IEnumerable_1_t1356416995;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Zenject_DiContainer2383114449.h"
#include "mscorlib_System_Type2779229935.h"
#include "mscorlib_System_String968488902.h"
#include "mscorlib_System_Object837106420.h"
#include "AssemblyU2DCSharp_Zenject_InjectContext3456483891.h"

// System.Void Zenject.InjectContext::.ctor(Zenject.DiContainer,System.Type,System.String,System.Boolean,System.Type,System.Object,System.String,Zenject.InjectContext,System.String,System.Object)
extern "C"  void InjectContext__ctor_m3247737529 (InjectContext_t3456483891 * __this, DiContainer_t2383114449 * ___container0, Type_t * ___memberType1, String_t* ___identifier2, bool ___optional3, Type_t * ___objectType4, Il2CppObject * ___objectInstance5, String_t* ___memberName6, InjectContext_t3456483891 * ___parentContext7, String_t* ___concreteIdentifier8, Il2CppObject * ___fallBackValue9, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Zenject.InjectContext::.ctor(Zenject.DiContainer,System.Type,System.String,System.Boolean,System.Type,System.Object,System.String,Zenject.InjectContext)
extern "C"  void InjectContext__ctor_m2445327663 (InjectContext_t3456483891 * __this, DiContainer_t2383114449 * ___container0, Type_t * ___memberType1, String_t* ___identifier2, bool ___optional3, Type_t * ___objectType4, Il2CppObject * ___objectInstance5, String_t* ___memberName6, InjectContext_t3456483891 * ___parentContext7, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Zenject.InjectContext::.ctor(Zenject.DiContainer,System.Type,System.String,System.Boolean,System.Type,System.Object)
extern "C"  void InjectContext__ctor_m221806630 (InjectContext_t3456483891 * __this, DiContainer_t2383114449 * ___container0, Type_t * ___memberType1, String_t* ___identifier2, bool ___optional3, Type_t * ___objectType4, Il2CppObject * ___objectInstance5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Zenject.InjectContext::.ctor(Zenject.DiContainer,System.Type,System.String,System.Boolean)
extern "C"  void InjectContext__ctor_m3025121189 (InjectContext_t3456483891 * __this, DiContainer_t2383114449 * ___container0, Type_t * ___memberType1, String_t* ___identifier2, bool ___optional3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Zenject.InjectContext::.ctor(Zenject.DiContainer,System.Type,System.String)
extern "C"  void InjectContext__ctor_m2251872472 (InjectContext_t3456483891 * __this, DiContainer_t2383114449 * ___container0, Type_t * ___memberType1, String_t* ___identifier2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Zenject.InjectContext::.ctor(Zenject.DiContainer,System.Type)
extern "C"  void InjectContext__ctor_m311734684 (InjectContext_t3456483891 * __this, DiContainer_t2383114449 * ___container0, Type_t * ___memberType1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.IEnumerable`1<Zenject.InjectContext> Zenject.InjectContext::get_ParentContexts()
extern "C"  Il2CppObject* InjectContext_get_ParentContexts_m3916435460 (InjectContext_t3456483891 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.IEnumerable`1<Zenject.InjectContext> Zenject.InjectContext::get_ParentContextsAndSelf()
extern "C"  Il2CppObject* InjectContext_get_ParentContextsAndSelf_m43373953 (InjectContext_t3456483891 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.IEnumerable`1<System.Type> Zenject.InjectContext::get_AllObjectTypes()
extern "C"  Il2CppObject* InjectContext_get_AllObjectTypes_m2308027669 (InjectContext_t3456483891 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Zenject.InjectContext::GetObjectGraphString()
extern "C"  String_t* InjectContext_GetObjectGraphString_m708666943 (InjectContext_t3456483891 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Zenject.InjectContext Zenject.InjectContext::ChangeMemberType(System.Type)
extern "C"  InjectContext_t3456483891 * InjectContext_ChangeMemberType_m2204918513 (InjectContext_t3456483891 * __this, Type_t * ___newMemberType0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Zenject.InjectContext Zenject.InjectContext::ChangeConcreteIdentifier(System.String)
extern "C"  InjectContext_t3456483891 * InjectContext_ChangeConcreteIdentifier_m2227316768 (InjectContext_t3456483891 * __this, String_t* ___concreteIdentifier0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
