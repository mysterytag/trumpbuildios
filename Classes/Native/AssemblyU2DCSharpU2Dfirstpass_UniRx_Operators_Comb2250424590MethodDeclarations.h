﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.CombineLatestObservable`3/CombineLatest/LeftObserver<System.Object,System.Object,System.Object>
struct LeftObserver_t2250424590;
// UniRx.Operators.CombineLatestObservable`3/CombineLatest<System.Object,System.Object,System.Object>
struct CombineLatest_t2347858151;
// System.Object
struct Il2CppObject;
// System.Exception
struct Exception_t1967233988;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_Exception1967233988.h"

// System.Void UniRx.Operators.CombineLatestObservable`3/CombineLatest/LeftObserver<System.Object,System.Object,System.Object>::.ctor(UniRx.Operators.CombineLatestObservable`3/CombineLatest<TLeft,TRight,TResult>)
extern "C"  void LeftObserver__ctor_m353944227_gshared (LeftObserver_t2250424590 * __this, CombineLatest_t2347858151 * ___parent0, const MethodInfo* method);
#define LeftObserver__ctor_m353944227(__this, ___parent0, method) ((  void (*) (LeftObserver_t2250424590 *, CombineLatest_t2347858151 *, const MethodInfo*))LeftObserver__ctor_m353944227_gshared)(__this, ___parent0, method)
// System.Void UniRx.Operators.CombineLatestObservable`3/CombineLatest/LeftObserver<System.Object,System.Object,System.Object>::OnNext(TLeft)
extern "C"  void LeftObserver_OnNext_m705022704_gshared (LeftObserver_t2250424590 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define LeftObserver_OnNext_m705022704(__this, ___value0, method) ((  void (*) (LeftObserver_t2250424590 *, Il2CppObject *, const MethodInfo*))LeftObserver_OnNext_m705022704_gshared)(__this, ___value0, method)
// System.Void UniRx.Operators.CombineLatestObservable`3/CombineLatest/LeftObserver<System.Object,System.Object,System.Object>::OnError(System.Exception)
extern "C"  void LeftObserver_OnError_m3574816326_gshared (LeftObserver_t2250424590 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method);
#define LeftObserver_OnError_m3574816326(__this, ___error0, method) ((  void (*) (LeftObserver_t2250424590 *, Exception_t1967233988 *, const MethodInfo*))LeftObserver_OnError_m3574816326_gshared)(__this, ___error0, method)
// System.Void UniRx.Operators.CombineLatestObservable`3/CombineLatest/LeftObserver<System.Object,System.Object,System.Object>::OnCompleted()
extern "C"  void LeftObserver_OnCompleted_m2096679577_gshared (LeftObserver_t2250424590 * __this, const MethodInfo* method);
#define LeftObserver_OnCompleted_m2096679577(__this, method) ((  void (*) (LeftObserver_t2250424590 *, const MethodInfo*))LeftObserver_OnCompleted_m2096679577_gshared)(__this, method)
