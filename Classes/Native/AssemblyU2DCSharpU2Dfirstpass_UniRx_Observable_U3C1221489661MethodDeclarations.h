﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Observable/<ToAsync>c__AnonStorey38`1<System.Object>
struct U3CToAsyncU3Ec__AnonStorey38_1_t1221489661;
// UniRx.IObservable`1<System.Object>
struct IObservable_1_t595904784;

#include "codegen/il2cpp-codegen.h"

// System.Void UniRx.Observable/<ToAsync>c__AnonStorey38`1<System.Object>::.ctor()
extern "C"  void U3CToAsyncU3Ec__AnonStorey38_1__ctor_m266223115_gshared (U3CToAsyncU3Ec__AnonStorey38_1_t1221489661 * __this, const MethodInfo* method);
#define U3CToAsyncU3Ec__AnonStorey38_1__ctor_m266223115(__this, method) ((  void (*) (U3CToAsyncU3Ec__AnonStorey38_1_t1221489661 *, const MethodInfo*))U3CToAsyncU3Ec__AnonStorey38_1__ctor_m266223115_gshared)(__this, method)
// UniRx.IObservable`1<T> UniRx.Observable/<ToAsync>c__AnonStorey38`1<System.Object>::<>m__3C()
extern "C"  Il2CppObject* U3CToAsyncU3Ec__AnonStorey38_1_U3CU3Em__3C_m2161775360_gshared (U3CToAsyncU3Ec__AnonStorey38_1_t1221489661 * __this, const MethodInfo* method);
#define U3CToAsyncU3Ec__AnonStorey38_1_U3CU3Em__3C_m2161775360(__this, method) ((  Il2CppObject* (*) (U3CToAsyncU3Ec__AnonStorey38_1_t1221489661 *, const MethodInfo*))U3CToAsyncU3Ec__AnonStorey38_1_U3CU3Em__3C_m2161775360_gshared)(__this, method)
