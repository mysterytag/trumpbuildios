﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Observable/<ToObservable>c__AnonStorey51
struct U3CToObservableU3Ec__AnonStorey51_t3647079135;
// System.Collections.IEnumerator
struct IEnumerator_t287207039;
// UniRx.IObserver`1<UniRx.Unit>
struct IObserver_1_t475317645;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_CancellationTo1439151560.h"

// System.Void UniRx.Observable/<ToObservable>c__AnonStorey51::.ctor()
extern "C"  void U3CToObservableU3Ec__AnonStorey51__ctor_m3092923844 (U3CToObservableU3Ec__AnonStorey51_t3647079135 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator UniRx.Observable/<ToObservable>c__AnonStorey51::<>m__4E(UniRx.IObserver`1<UniRx.Unit>,UniRx.CancellationToken)
extern "C"  Il2CppObject * U3CToObservableU3Ec__AnonStorey51_U3CU3Em__4E_m3556682324 (U3CToObservableU3Ec__AnonStorey51_t3647079135 * __this, Il2CppObject* ___observer0, CancellationToken_t1439151560  ___cancellationToken1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
