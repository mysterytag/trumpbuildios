﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.PlayFabClientAPI/ProcessApiCallback`1<System.Object>
struct ProcessApiCallback_1_t3425006930;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t537683269;
// System.AsyncCallback
struct AsyncCallback_t1363551830;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_IntPtr676692020.h"
#include "mscorlib_System_AsyncCallback1363551830.h"

// System.Void PlayFab.PlayFabClientAPI/ProcessApiCallback`1<System.Object>::.ctor(System.Object,System.IntPtr)
extern "C"  void ProcessApiCallback_1__ctor_m1266910666_gshared (ProcessApiCallback_1_t3425006930 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method);
#define ProcessApiCallback_1__ctor_m1266910666(__this, ___object0, ___method1, method) ((  void (*) (ProcessApiCallback_1_t3425006930 *, Il2CppObject *, IntPtr_t, const MethodInfo*))ProcessApiCallback_1__ctor_m1266910666_gshared)(__this, ___object0, ___method1, method)
// System.Void PlayFab.PlayFabClientAPI/ProcessApiCallback`1<System.Object>::Invoke(TResult)
extern "C"  void ProcessApiCallback_1_Invoke_m1002635741_gshared (ProcessApiCallback_1_t3425006930 * __this, Il2CppObject * ___result0, const MethodInfo* method);
#define ProcessApiCallback_1_Invoke_m1002635741(__this, ___result0, method) ((  void (*) (ProcessApiCallback_1_t3425006930 *, Il2CppObject *, const MethodInfo*))ProcessApiCallback_1_Invoke_m1002635741_gshared)(__this, ___result0, method)
// System.IAsyncResult PlayFab.PlayFabClientAPI/ProcessApiCallback`1<System.Object>::BeginInvoke(TResult,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * ProcessApiCallback_1_BeginInvoke_m1317674820_gshared (ProcessApiCallback_1_t3425006930 * __this, Il2CppObject * ___result0, AsyncCallback_t1363551830 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method);
#define ProcessApiCallback_1_BeginInvoke_m1317674820(__this, ___result0, ___callback1, ___object2, method) ((  Il2CppObject * (*) (ProcessApiCallback_1_t3425006930 *, Il2CppObject *, AsyncCallback_t1363551830 *, Il2CppObject *, const MethodInfo*))ProcessApiCallback_1_BeginInvoke_m1317674820_gshared)(__this, ___result0, ___callback1, ___object2, method)
// System.Void PlayFab.PlayFabClientAPI/ProcessApiCallback`1<System.Object>::EndInvoke(System.IAsyncResult)
extern "C"  void ProcessApiCallback_1_EndInvoke_m3326438618_gshared (ProcessApiCallback_1_t3425006930 * __this, Il2CppObject * ___result0, const MethodInfo* method);
#define ProcessApiCallback_1_EndInvoke_m3326438618(__this, ___result0, method) ((  void (*) (ProcessApiCallback_1_t3425006930 *, Il2CppObject *, const MethodInfo*))ProcessApiCallback_1_EndInvoke_m3326438618_gshared)(__this, ___result0, method)
