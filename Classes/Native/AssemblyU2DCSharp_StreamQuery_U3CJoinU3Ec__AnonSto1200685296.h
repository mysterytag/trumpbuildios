﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Action`1<System.Object>
struct Action_1_t985559125;
// CellUtils.SingleAssignmentDisposable
struct SingleAssignmentDisposable_t1832432170;
// StreamQuery/<Join>c__AnonStorey13F`1<System.Object>
struct U3CJoinU3Ec__AnonStorey13F_1_t910942991;

#include "mscorlib_System_Object837106420.h"
#include "AssemblyU2DCSharp_Priority3194150340.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// StreamQuery/<Join>c__AnonStorey13F`1/<Join>c__AnonStorey140`1<System.Object>
struct  U3CJoinU3Ec__AnonStorey140_1_t1200685296  : public Il2CppObject
{
public:
	// Priority StreamQuery/<Join>c__AnonStorey13F`1/<Join>c__AnonStorey140`1::p
	int32_t ___p_0;
	// System.Action`1<T> StreamQuery/<Join>c__AnonStorey13F`1/<Join>c__AnonStorey140`1::reaction
	Action_1_t985559125 * ___reaction_1;
	// CellUtils.SingleAssignmentDisposable StreamQuery/<Join>c__AnonStorey13F`1/<Join>c__AnonStorey140`1::inner
	SingleAssignmentDisposable_t1832432170 * ___inner_2;
	// StreamQuery/<Join>c__AnonStorey13F`1<T> StreamQuery/<Join>c__AnonStorey13F`1/<Join>c__AnonStorey140`1::<>f__ref$319
	U3CJoinU3Ec__AnonStorey13F_1_t910942991 * ___U3CU3Ef__refU24319_3;

public:
	inline static int32_t get_offset_of_p_0() { return static_cast<int32_t>(offsetof(U3CJoinU3Ec__AnonStorey140_1_t1200685296, ___p_0)); }
	inline int32_t get_p_0() const { return ___p_0; }
	inline int32_t* get_address_of_p_0() { return &___p_0; }
	inline void set_p_0(int32_t value)
	{
		___p_0 = value;
	}

	inline static int32_t get_offset_of_reaction_1() { return static_cast<int32_t>(offsetof(U3CJoinU3Ec__AnonStorey140_1_t1200685296, ___reaction_1)); }
	inline Action_1_t985559125 * get_reaction_1() const { return ___reaction_1; }
	inline Action_1_t985559125 ** get_address_of_reaction_1() { return &___reaction_1; }
	inline void set_reaction_1(Action_1_t985559125 * value)
	{
		___reaction_1 = value;
		Il2CppCodeGenWriteBarrier(&___reaction_1, value);
	}

	inline static int32_t get_offset_of_inner_2() { return static_cast<int32_t>(offsetof(U3CJoinU3Ec__AnonStorey140_1_t1200685296, ___inner_2)); }
	inline SingleAssignmentDisposable_t1832432170 * get_inner_2() const { return ___inner_2; }
	inline SingleAssignmentDisposable_t1832432170 ** get_address_of_inner_2() { return &___inner_2; }
	inline void set_inner_2(SingleAssignmentDisposable_t1832432170 * value)
	{
		___inner_2 = value;
		Il2CppCodeGenWriteBarrier(&___inner_2, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__refU24319_3() { return static_cast<int32_t>(offsetof(U3CJoinU3Ec__AnonStorey140_1_t1200685296, ___U3CU3Ef__refU24319_3)); }
	inline U3CJoinU3Ec__AnonStorey13F_1_t910942991 * get_U3CU3Ef__refU24319_3() const { return ___U3CU3Ef__refU24319_3; }
	inline U3CJoinU3Ec__AnonStorey13F_1_t910942991 ** get_address_of_U3CU3Ef__refU24319_3() { return &___U3CU3Ef__refU24319_3; }
	inline void set_U3CU3Ef__refU24319_3(U3CJoinU3Ec__AnonStorey13F_1_t910942991 * value)
	{
		___U3CU3Ef__refU24319_3 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__refU24319_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
