﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.InternalUtil.EmptyObserver`1<UniRx.DictionaryReplaceEvent`2<System.Object,System.Object>>
struct EmptyObserver_1_t3188280053;
// System.Exception
struct Exception_t1967233988;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Exception1967233988.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_DictionaryRepl3778927063.h"

// System.Void UniRx.InternalUtil.EmptyObserver`1<UniRx.DictionaryReplaceEvent`2<System.Object,System.Object>>::.ctor()
extern "C"  void EmptyObserver_1__ctor_m2602292158_gshared (EmptyObserver_1_t3188280053 * __this, const MethodInfo* method);
#define EmptyObserver_1__ctor_m2602292158(__this, method) ((  void (*) (EmptyObserver_1_t3188280053 *, const MethodInfo*))EmptyObserver_1__ctor_m2602292158_gshared)(__this, method)
// System.Void UniRx.InternalUtil.EmptyObserver`1<UniRx.DictionaryReplaceEvent`2<System.Object,System.Object>>::.cctor()
extern "C"  void EmptyObserver_1__cctor_m2879549359_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define EmptyObserver_1__cctor_m2879549359(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))EmptyObserver_1__cctor_m2879549359_gshared)(__this /* static, unused */, method)
// System.Void UniRx.InternalUtil.EmptyObserver`1<UniRx.DictionaryReplaceEvent`2<System.Object,System.Object>>::OnCompleted()
extern "C"  void EmptyObserver_1_OnCompleted_m293388488_gshared (EmptyObserver_1_t3188280053 * __this, const MethodInfo* method);
#define EmptyObserver_1_OnCompleted_m293388488(__this, method) ((  void (*) (EmptyObserver_1_t3188280053 *, const MethodInfo*))EmptyObserver_1_OnCompleted_m293388488_gshared)(__this, method)
// System.Void UniRx.InternalUtil.EmptyObserver`1<UniRx.DictionaryReplaceEvent`2<System.Object,System.Object>>::OnError(System.Exception)
extern "C"  void EmptyObserver_1_OnError_m1868858869_gshared (EmptyObserver_1_t3188280053 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method);
#define EmptyObserver_1_OnError_m1868858869(__this, ___error0, method) ((  void (*) (EmptyObserver_1_t3188280053 *, Exception_t1967233988 *, const MethodInfo*))EmptyObserver_1_OnError_m1868858869_gshared)(__this, ___error0, method)
// System.Void UniRx.InternalUtil.EmptyObserver`1<UniRx.DictionaryReplaceEvent`2<System.Object,System.Object>>::OnNext(T)
extern "C"  void EmptyObserver_1_OnNext_m3962500838_gshared (EmptyObserver_1_t3188280053 * __this, DictionaryReplaceEvent_2_t3778927063  ___value0, const MethodInfo* method);
#define EmptyObserver_1_OnNext_m3962500838(__this, ___value0, method) ((  void (*) (EmptyObserver_1_t3188280053 *, DictionaryReplaceEvent_2_t3778927063 , const MethodInfo*))EmptyObserver_1_OnNext_m3962500838_gshared)(__this, ___value0, method)
