﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.ClientModels.GetPlayFabIDsFromPSNAccountIDsResult
struct GetPlayFabIDsFromPSNAccountIDsResult_t3051284830;
// System.Collections.Generic.List`1<PlayFab.ClientModels.PSNAccountPlayFabIdPair>
struct List_1_t1477512911;

#include "codegen/il2cpp-codegen.h"

// System.Void PlayFab.ClientModels.GetPlayFabIDsFromPSNAccountIDsResult::.ctor()
extern "C"  void GetPlayFabIDsFromPSNAccountIDsResult__ctor_m1719154591 (GetPlayFabIDsFromPSNAccountIDsResult_t3051284830 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<PlayFab.ClientModels.PSNAccountPlayFabIdPair> PlayFab.ClientModels.GetPlayFabIDsFromPSNAccountIDsResult::get_Data()
extern "C"  List_1_t1477512911 * GetPlayFabIDsFromPSNAccountIDsResult_get_Data_m1713345149 (GetPlayFabIDsFromPSNAccountIDsResult_t3051284830 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.GetPlayFabIDsFromPSNAccountIDsResult::set_Data(System.Collections.Generic.List`1<PlayFab.ClientModels.PSNAccountPlayFabIdPair>)
extern "C"  void GetPlayFabIDsFromPSNAccountIDsResult_set_Data_m2656781518 (GetPlayFabIDsFromPSNAccountIDsResult_t3051284830 * __this, List_1_t1477512911 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
