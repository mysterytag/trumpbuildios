﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Func`3<System.Single,System.Single,System.Single>
struct Func_3_t3782820650;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Linq.Enumerable
struct  Enumerable_t4285654829  : public Il2CppObject
{
public:

public:
};

struct Enumerable_t4285654829_StaticFields
{
public:
	// System.Func`3<System.Single,System.Single,System.Single> System.Linq.Enumerable::<>f__am$cache2E
	Func_3_t3782820650 * ___U3CU3Ef__amU24cache2E_0;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache2E_0() { return static_cast<int32_t>(offsetof(Enumerable_t4285654829_StaticFields, ___U3CU3Ef__amU24cache2E_0)); }
	inline Func_3_t3782820650 * get_U3CU3Ef__amU24cache2E_0() const { return ___U3CU3Ef__amU24cache2E_0; }
	inline Func_3_t3782820650 ** get_address_of_U3CU3Ef__amU24cache2E_0() { return &___U3CU3Ef__amU24cache2E_0; }
	inline void set_U3CU3Ef__amU24cache2E_0(Func_3_t3782820650 * value)
	{
		___U3CU3Ef__amU24cache2E_0 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache2E_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
