﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Net.HttpListenerResponse
struct HttpListenerResponse_t1293142343;
// System.IO.Stream
struct Stream_t219029575;
// System.Byte[]
struct ByteU5BU5D_t58506160;

#include "mscorlib_System_IO_Stream219029575.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.ResponseStream
struct  ResponseStream_t2907780779  : public Stream_t219029575
{
public:
	// System.Net.HttpListenerResponse System.Net.ResponseStream::response
	HttpListenerResponse_t1293142343 * ___response_1;
	// System.Boolean System.Net.ResponseStream::ignore_errors
	bool ___ignore_errors_2;
	// System.Boolean System.Net.ResponseStream::disposed
	bool ___disposed_3;
	// System.Boolean System.Net.ResponseStream::trailer_sent
	bool ___trailer_sent_4;
	// System.IO.Stream System.Net.ResponseStream::stream
	Stream_t219029575 * ___stream_5;

public:
	inline static int32_t get_offset_of_response_1() { return static_cast<int32_t>(offsetof(ResponseStream_t2907780779, ___response_1)); }
	inline HttpListenerResponse_t1293142343 * get_response_1() const { return ___response_1; }
	inline HttpListenerResponse_t1293142343 ** get_address_of_response_1() { return &___response_1; }
	inline void set_response_1(HttpListenerResponse_t1293142343 * value)
	{
		___response_1 = value;
		Il2CppCodeGenWriteBarrier(&___response_1, value);
	}

	inline static int32_t get_offset_of_ignore_errors_2() { return static_cast<int32_t>(offsetof(ResponseStream_t2907780779, ___ignore_errors_2)); }
	inline bool get_ignore_errors_2() const { return ___ignore_errors_2; }
	inline bool* get_address_of_ignore_errors_2() { return &___ignore_errors_2; }
	inline void set_ignore_errors_2(bool value)
	{
		___ignore_errors_2 = value;
	}

	inline static int32_t get_offset_of_disposed_3() { return static_cast<int32_t>(offsetof(ResponseStream_t2907780779, ___disposed_3)); }
	inline bool get_disposed_3() const { return ___disposed_3; }
	inline bool* get_address_of_disposed_3() { return &___disposed_3; }
	inline void set_disposed_3(bool value)
	{
		___disposed_3 = value;
	}

	inline static int32_t get_offset_of_trailer_sent_4() { return static_cast<int32_t>(offsetof(ResponseStream_t2907780779, ___trailer_sent_4)); }
	inline bool get_trailer_sent_4() const { return ___trailer_sent_4; }
	inline bool* get_address_of_trailer_sent_4() { return &___trailer_sent_4; }
	inline void set_trailer_sent_4(bool value)
	{
		___trailer_sent_4 = value;
	}

	inline static int32_t get_offset_of_stream_5() { return static_cast<int32_t>(offsetof(ResponseStream_t2907780779, ___stream_5)); }
	inline Stream_t219029575 * get_stream_5() const { return ___stream_5; }
	inline Stream_t219029575 ** get_address_of_stream_5() { return &___stream_5; }
	inline void set_stream_5(Stream_t219029575 * value)
	{
		___stream_5 = value;
		Il2CppCodeGenWriteBarrier(&___stream_5, value);
	}
};

struct ResponseStream_t2907780779_StaticFields
{
public:
	// System.Byte[] System.Net.ResponseStream::crlf
	ByteU5BU5D_t58506160* ___crlf_6;

public:
	inline static int32_t get_offset_of_crlf_6() { return static_cast<int32_t>(offsetof(ResponseStream_t2907780779_StaticFields, ___crlf_6)); }
	inline ByteU5BU5D_t58506160* get_crlf_6() const { return ___crlf_6; }
	inline ByteU5BU5D_t58506160** get_address_of_crlf_6() { return &___crlf_6; }
	inline void set_crlf_6(ByteU5BU5D_t58506160* value)
	{
		___crlf_6 = value;
		Il2CppCodeGenWriteBarrier(&___crlf_6, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
