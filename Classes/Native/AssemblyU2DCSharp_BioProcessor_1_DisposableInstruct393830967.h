﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// BioProcessor`1/Instruction<System.Object>
struct Instruction_t1241108407;
// BioProcessor`1<System.Object>
struct BioProcessor_1_t2501661084;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BioProcessor`1/DisposableInstruction<System.Object>
struct  DisposableInstruction_t393830967  : public Il2CppObject
{
public:
	// BioProcessor`1/Instruction<T> BioProcessor`1/DisposableInstruction::instruction
	Instruction_t1241108407 * ___instruction_0;
	// BioProcessor`1<T> BioProcessor`1/DisposableInstruction::substance
	BioProcessor_1_t2501661084 * ___substance_1;

public:
	inline static int32_t get_offset_of_instruction_0() { return static_cast<int32_t>(offsetof(DisposableInstruction_t393830967, ___instruction_0)); }
	inline Instruction_t1241108407 * get_instruction_0() const { return ___instruction_0; }
	inline Instruction_t1241108407 ** get_address_of_instruction_0() { return &___instruction_0; }
	inline void set_instruction_0(Instruction_t1241108407 * value)
	{
		___instruction_0 = value;
		Il2CppCodeGenWriteBarrier(&___instruction_0, value);
	}

	inline static int32_t get_offset_of_substance_1() { return static_cast<int32_t>(offsetof(DisposableInstruction_t393830967, ___substance_1)); }
	inline BioProcessor_1_t2501661084 * get_substance_1() const { return ___substance_1; }
	inline BioProcessor_1_t2501661084 ** get_address_of_substance_1() { return &___substance_1; }
	inline void set_substance_1(BioProcessor_1_t2501661084 * value)
	{
		___substance_1 = value;
		Il2CppCodeGenWriteBarrier(&___substance_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
