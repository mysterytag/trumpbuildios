﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Object
struct Il2CppObject;
// System.Collections.IComparer
struct IComparer_t2207526184;
// System.Collections.IEqualityComparer
struct IEqualityComparer_t1661793090;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Tuple_3_gen1650899102.h"
#include "mscorlib_System_Object837106420.h"

// System.Void UniRx.Tuple`3<System.Object,System.Object,System.Object>::.ctor(T1,T2,T3)
extern "C"  void Tuple_3__ctor_m1753573884_gshared (Tuple_3_t1650899102 * __this, Il2CppObject * ___item10, Il2CppObject * ___item21, Il2CppObject * ___item32, const MethodInfo* method);
#define Tuple_3__ctor_m1753573884(__this, ___item10, ___item21, ___item32, method) ((  void (*) (Tuple_3_t1650899102 *, Il2CppObject *, Il2CppObject *, Il2CppObject *, const MethodInfo*))Tuple_3__ctor_m1753573884_gshared)(__this, ___item10, ___item21, ___item32, method)
// System.Int32 UniRx.Tuple`3<System.Object,System.Object,System.Object>::System.IComparable.CompareTo(System.Object)
extern "C"  int32_t Tuple_3_System_IComparable_CompareTo_m509379070_gshared (Tuple_3_t1650899102 * __this, Il2CppObject * ___obj0, const MethodInfo* method);
#define Tuple_3_System_IComparable_CompareTo_m509379070(__this, ___obj0, method) ((  int32_t (*) (Tuple_3_t1650899102 *, Il2CppObject *, const MethodInfo*))Tuple_3_System_IComparable_CompareTo_m509379070_gshared)(__this, ___obj0, method)
// System.Int32 UniRx.Tuple`3<System.Object,System.Object,System.Object>::UniRx.IStructuralComparable.CompareTo(System.Object,System.Collections.IComparer)
extern "C"  int32_t Tuple_3_UniRx_IStructuralComparable_CompareTo_m2886736_gshared (Tuple_3_t1650899102 * __this, Il2CppObject * ___other0, Il2CppObject * ___comparer1, const MethodInfo* method);
#define Tuple_3_UniRx_IStructuralComparable_CompareTo_m2886736(__this, ___other0, ___comparer1, method) ((  int32_t (*) (Tuple_3_t1650899102 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))Tuple_3_UniRx_IStructuralComparable_CompareTo_m2886736_gshared)(__this, ___other0, ___comparer1, method)
// System.Boolean UniRx.Tuple`3<System.Object,System.Object,System.Object>::UniRx.IStructuralEquatable.Equals(System.Object,System.Collections.IEqualityComparer)
extern "C"  bool Tuple_3_UniRx_IStructuralEquatable_Equals_m2030863419_gshared (Tuple_3_t1650899102 * __this, Il2CppObject * ___other0, Il2CppObject * ___comparer1, const MethodInfo* method);
#define Tuple_3_UniRx_IStructuralEquatable_Equals_m2030863419(__this, ___other0, ___comparer1, method) ((  bool (*) (Tuple_3_t1650899102 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))Tuple_3_UniRx_IStructuralEquatable_Equals_m2030863419_gshared)(__this, ___other0, ___comparer1, method)
// System.Int32 UniRx.Tuple`3<System.Object,System.Object,System.Object>::UniRx.IStructuralEquatable.GetHashCode(System.Collections.IEqualityComparer)
extern "C"  int32_t Tuple_3_UniRx_IStructuralEquatable_GetHashCode_m804104467_gshared (Tuple_3_t1650899102 * __this, Il2CppObject * ___comparer0, const MethodInfo* method);
#define Tuple_3_UniRx_IStructuralEquatable_GetHashCode_m804104467(__this, ___comparer0, method) ((  int32_t (*) (Tuple_3_t1650899102 *, Il2CppObject *, const MethodInfo*))Tuple_3_UniRx_IStructuralEquatable_GetHashCode_m804104467_gshared)(__this, ___comparer0, method)
// System.String UniRx.Tuple`3<System.Object,System.Object,System.Object>::UniRx.ITuple.ToString()
extern "C"  String_t* Tuple_3_UniRx_ITuple_ToString_m363125048_gshared (Tuple_3_t1650899102 * __this, const MethodInfo* method);
#define Tuple_3_UniRx_ITuple_ToString_m363125048(__this, method) ((  String_t* (*) (Tuple_3_t1650899102 *, const MethodInfo*))Tuple_3_UniRx_ITuple_ToString_m363125048_gshared)(__this, method)
// T1 UniRx.Tuple`3<System.Object,System.Object,System.Object>::get_Item1()
extern "C"  Il2CppObject * Tuple_3_get_Item1_m1125131173_gshared (Tuple_3_t1650899102 * __this, const MethodInfo* method);
#define Tuple_3_get_Item1_m1125131173(__this, method) ((  Il2CppObject * (*) (Tuple_3_t1650899102 *, const MethodInfo*))Tuple_3_get_Item1_m1125131173_gshared)(__this, method)
// T2 UniRx.Tuple`3<System.Object,System.Object,System.Object>::get_Item2()
extern "C"  Il2CppObject * Tuple_3_get_Item2_m641456807_gshared (Tuple_3_t1650899102 * __this, const MethodInfo* method);
#define Tuple_3_get_Item2_m641456807(__this, method) ((  Il2CppObject * (*) (Tuple_3_t1650899102 *, const MethodInfo*))Tuple_3_get_Item2_m641456807_gshared)(__this, method)
// T3 UniRx.Tuple`3<System.Object,System.Object,System.Object>::get_Item3()
extern "C"  Il2CppObject * Tuple_3_get_Item3_m157782441_gshared (Tuple_3_t1650899102 * __this, const MethodInfo* method);
#define Tuple_3_get_Item3_m157782441(__this, method) ((  Il2CppObject * (*) (Tuple_3_t1650899102 *, const MethodInfo*))Tuple_3_get_Item3_m157782441_gshared)(__this, method)
// System.Boolean UniRx.Tuple`3<System.Object,System.Object,System.Object>::Equals(System.Object)
extern "C"  bool Tuple_3_Equals_m554362607_gshared (Tuple_3_t1650899102 * __this, Il2CppObject * ___obj0, const MethodInfo* method);
#define Tuple_3_Equals_m554362607(__this, ___obj0, method) ((  bool (*) (Tuple_3_t1650899102 *, Il2CppObject *, const MethodInfo*))Tuple_3_Equals_m554362607_gshared)(__this, ___obj0, method)
// System.Int32 UniRx.Tuple`3<System.Object,System.Object,System.Object>::GetHashCode()
extern "C"  int32_t Tuple_3_GetHashCode_m4073453075_gshared (Tuple_3_t1650899102 * __this, const MethodInfo* method);
#define Tuple_3_GetHashCode_m4073453075(__this, method) ((  int32_t (*) (Tuple_3_t1650899102 *, const MethodInfo*))Tuple_3_GetHashCode_m4073453075_gshared)(__this, method)
// System.String UniRx.Tuple`3<System.Object,System.Object,System.Object>::ToString()
extern "C"  String_t* Tuple_3_ToString_m3064948473_gshared (Tuple_3_t1650899102 * __this, const MethodInfo* method);
#define Tuple_3_ToString_m3064948473(__this, method) ((  String_t* (*) (Tuple_3_t1650899102 *, const MethodInfo*))Tuple_3_ToString_m3064948473_gshared)(__this, method)
// System.Boolean UniRx.Tuple`3<System.Object,System.Object,System.Object>::Equals(UniRx.Tuple`3<T1,T2,T3>)
extern "C"  bool Tuple_3_Equals_m1515079942_gshared (Tuple_3_t1650899102 * __this, Tuple_3_t1650899102  ___other0, const MethodInfo* method);
#define Tuple_3_Equals_m1515079942(__this, ___other0, method) ((  bool (*) (Tuple_3_t1650899102 *, Tuple_3_t1650899102 , const MethodInfo*))Tuple_3_Equals_m1515079942_gshared)(__this, ___other0, method)
