﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.ReactiveProperty`1/ReactivePropertyObserver<System.Single>
struct ReactivePropertyObserver_t1194191590;
// UniRx.ReactiveProperty`1<System.Single>
struct ReactiveProperty_1_t1566542059;
// System.Exception
struct Exception_t1967233988;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Exception1967233988.h"

// System.Void UniRx.ReactiveProperty`1/ReactivePropertyObserver<System.Single>::.ctor(UniRx.ReactiveProperty`1<T>)
extern "C"  void ReactivePropertyObserver__ctor_m3749029227_gshared (ReactivePropertyObserver_t1194191590 * __this, ReactiveProperty_1_t1566542059 * ___parent0, const MethodInfo* method);
#define ReactivePropertyObserver__ctor_m3749029227(__this, ___parent0, method) ((  void (*) (ReactivePropertyObserver_t1194191590 *, ReactiveProperty_1_t1566542059 *, const MethodInfo*))ReactivePropertyObserver__ctor_m3749029227_gshared)(__this, ___parent0, method)
// System.Void UniRx.ReactiveProperty`1/ReactivePropertyObserver<System.Single>::OnNext(T)
extern "C"  void ReactivePropertyObserver_OnNext_m1358961120_gshared (ReactivePropertyObserver_t1194191590 * __this, float ___value0, const MethodInfo* method);
#define ReactivePropertyObserver_OnNext_m1358961120(__this, ___value0, method) ((  void (*) (ReactivePropertyObserver_t1194191590 *, float, const MethodInfo*))ReactivePropertyObserver_OnNext_m1358961120_gshared)(__this, ___value0, method)
// System.Void UniRx.ReactiveProperty`1/ReactivePropertyObserver<System.Single>::OnError(System.Exception)
extern "C"  void ReactivePropertyObserver_OnError_m1737782511_gshared (ReactivePropertyObserver_t1194191590 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method);
#define ReactivePropertyObserver_OnError_m1737782511(__this, ___error0, method) ((  void (*) (ReactivePropertyObserver_t1194191590 *, Exception_t1967233988 *, const MethodInfo*))ReactivePropertyObserver_OnError_m1737782511_gshared)(__this, ___error0, method)
// System.Void UniRx.ReactiveProperty`1/ReactivePropertyObserver<System.Single>::OnCompleted()
extern "C"  void ReactivePropertyObserver_OnCompleted_m2460997314_gshared (ReactivePropertyObserver_t1194191590 * __this, const MethodInfo* method);
#define ReactivePropertyObserver_OnCompleted_m2460997314(__this, method) ((  void (*) (ReactivePropertyObserver_t1194191590 *, const MethodInfo*))ReactivePropertyObserver_OnCompleted_m2460997314_gshared)(__this, method)
