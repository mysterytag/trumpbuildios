﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.ReflectionUtils/<GetGetMethodByReflection>c__AnonStorey5B
struct U3CGetGetMethodByReflectionU3Ec__AnonStorey5B_t2453868661;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"

// System.Void PlayFab.ReflectionUtils/<GetGetMethodByReflection>c__AnonStorey5B::.ctor()
extern "C"  void U3CGetGetMethodByReflectionU3Ec__AnonStorey5B__ctor_m776160986 (U3CGetGetMethodByReflectionU3Ec__AnonStorey5B_t2453868661 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object PlayFab.ReflectionUtils/<GetGetMethodByReflection>c__AnonStorey5B::<>m__47(System.Object)
extern "C"  Il2CppObject * U3CGetGetMethodByReflectionU3Ec__AnonStorey5B_U3CU3Em__47_m2846991193 (U3CGetGetMethodByReflectionU3Ec__AnonStorey5B_t2453868661 * __this, Il2CppObject * ___source0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
