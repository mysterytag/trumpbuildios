﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.PlayFabClientAPI/GetCloudScriptUrlResponseCallback
struct GetCloudScriptUrlResponseCallback_t4019593355;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;
// PlayFab.ClientModels.GetCloudScriptUrlRequest
struct GetCloudScriptUrlRequest_t841424922;
// PlayFab.ClientModels.GetCloudScriptUrlResult
struct GetCloudScriptUrlResult_t3525973842;
// PlayFab.PlayFabError
struct PlayFabError_t750598646;
// System.IAsyncResult
struct IAsyncResult_t537683269;
// System.AsyncCallback
struct AsyncCallback_t1363551830;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_IntPtr676692020.h"
#include "mscorlib_System_String968488902.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_GetCloudScri841424922.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_GetCloudScr3525973842.h"
#include "AssemblyU2DCSharpU2Dfirstpass_PlayFab_PlayFabError750598646.h"
#include "mscorlib_System_AsyncCallback1363551830.h"

// System.Void PlayFab.PlayFabClientAPI/GetCloudScriptUrlResponseCallback::.ctor(System.Object,System.IntPtr)
extern "C"  void GetCloudScriptUrlResponseCallback__ctor_m2466385818 (GetCloudScriptUrlResponseCallback_t4019593355 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/GetCloudScriptUrlResponseCallback::Invoke(System.String,System.Int32,PlayFab.ClientModels.GetCloudScriptUrlRequest,PlayFab.ClientModels.GetCloudScriptUrlResult,PlayFab.PlayFabError,System.Object)
extern "C"  void GetCloudScriptUrlResponseCallback_Invoke_m916380687 (GetCloudScriptUrlResponseCallback_t4019593355 * __this, String_t* ___urlPath0, int32_t ___callId1, GetCloudScriptUrlRequest_t841424922 * ___request2, GetCloudScriptUrlResult_t3525973842 * ___result3, PlayFabError_t750598646 * ___error4, Il2CppObject * ___customData5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
extern "C" void pinvoke_delegate_wrapper_GetCloudScriptUrlResponseCallback_t4019593355(Il2CppObject* delegate, String_t* ___urlPath0, int32_t ___callId1, GetCloudScriptUrlRequest_t841424922 * ___request2, GetCloudScriptUrlResult_t3525973842 * ___result3, PlayFabError_t750598646 * ___error4, Il2CppObject * ___customData5);
// System.IAsyncResult PlayFab.PlayFabClientAPI/GetCloudScriptUrlResponseCallback::BeginInvoke(System.String,System.Int32,PlayFab.ClientModels.GetCloudScriptUrlRequest,PlayFab.ClientModels.GetCloudScriptUrlResult,PlayFab.PlayFabError,System.Object,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * GetCloudScriptUrlResponseCallback_BeginInvoke_m2133024564 (GetCloudScriptUrlResponseCallback_t4019593355 * __this, String_t* ___urlPath0, int32_t ___callId1, GetCloudScriptUrlRequest_t841424922 * ___request2, GetCloudScriptUrlResult_t3525973842 * ___result3, PlayFabError_t750598646 * ___error4, Il2CppObject * ___customData5, AsyncCallback_t1363551830 * ___callback6, Il2CppObject * ___object7, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/GetCloudScriptUrlResponseCallback::EndInvoke(System.IAsyncResult)
extern "C"  void GetCloudScriptUrlResponseCallback_EndInvoke_m1806786218 (GetCloudScriptUrlResponseCallback_t4019593355 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
