﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CellReactiveApi/<WhenOnce>c__AnonStorey107`1/<WhenOnce>c__AnonStorey108`1<System.Object>
struct U3CWhenOnceU3Ec__AnonStorey108_1_t2286074227;

#include "codegen/il2cpp-codegen.h"

// System.Void CellReactiveApi/<WhenOnce>c__AnonStorey107`1/<WhenOnce>c__AnonStorey108`1<System.Object>::.ctor()
extern "C"  void U3CWhenOnceU3Ec__AnonStorey108_1__ctor_m103729727_gshared (U3CWhenOnceU3Ec__AnonStorey108_1_t2286074227 * __this, const MethodInfo* method);
#define U3CWhenOnceU3Ec__AnonStorey108_1__ctor_m103729727(__this, method) ((  void (*) (U3CWhenOnceU3Ec__AnonStorey108_1_t2286074227 *, const MethodInfo*))U3CWhenOnceU3Ec__AnonStorey108_1__ctor_m103729727_gshared)(__this, method)
