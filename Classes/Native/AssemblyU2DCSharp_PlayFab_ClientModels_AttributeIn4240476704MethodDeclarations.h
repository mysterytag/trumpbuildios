﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.ClientModels.AttributeInstallRequest
struct AttributeInstallRequest_t4240476704;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String968488902.h"

// System.Void PlayFab.ClientModels.AttributeInstallRequest::.ctor()
extern "C"  void AttributeInstallRequest__ctor_m2766478217 (AttributeInstallRequest_t4240476704 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PlayFab.ClientModels.AttributeInstallRequest::get_Idfa()
extern "C"  String_t* AttributeInstallRequest_get_Idfa_m1578080093 (AttributeInstallRequest_t4240476704 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.AttributeInstallRequest::set_Idfa(System.String)
extern "C"  void AttributeInstallRequest_set_Idfa_m3260619284 (AttributeInstallRequest_t4240476704 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PlayFab.ClientModels.AttributeInstallRequest::get_Android_Id()
extern "C"  String_t* AttributeInstallRequest_get_Android_Id_m2546233234 (AttributeInstallRequest_t4240476704 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.AttributeInstallRequest::set_Android_Id(System.String)
extern "C"  void AttributeInstallRequest_set_Android_Id_m412616895 (AttributeInstallRequest_t4240476704 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
