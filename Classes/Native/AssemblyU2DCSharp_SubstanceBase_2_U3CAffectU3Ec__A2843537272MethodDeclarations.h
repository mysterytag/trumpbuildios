﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SubstanceBase`2/<Affect>c__AnonStorey144<System.Single,System.Single>
struct U3CAffectU3Ec__AnonStorey144_t2843537272;

#include "codegen/il2cpp-codegen.h"

// System.Void SubstanceBase`2/<Affect>c__AnonStorey144<System.Single,System.Single>::.ctor()
extern "C"  void U3CAffectU3Ec__AnonStorey144__ctor_m1728034950_gshared (U3CAffectU3Ec__AnonStorey144_t2843537272 * __this, const MethodInfo* method);
#define U3CAffectU3Ec__AnonStorey144__ctor_m1728034950(__this, method) ((  void (*) (U3CAffectU3Ec__AnonStorey144_t2843537272 *, const MethodInfo*))U3CAffectU3Ec__AnonStorey144__ctor_m1728034950_gshared)(__this, method)
// System.Void SubstanceBase`2/<Affect>c__AnonStorey144<System.Single,System.Single>::<>m__1DA()
extern "C"  void U3CAffectU3Ec__AnonStorey144_U3CU3Em__1DA_m3241567969_gshared (U3CAffectU3Ec__AnonStorey144_t2843537272 * __this, const MethodInfo* method);
#define U3CAffectU3Ec__AnonStorey144_U3CU3Em__1DA_m3241567969(__this, method) ((  void (*) (U3CAffectU3Ec__AnonStorey144_t2843537272 *, const MethodInfo*))U3CAffectU3Ec__AnonStorey144_U3CU3Em__1DA_m3241567969_gshared)(__this, method)
