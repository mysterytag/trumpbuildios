﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Zenject.FactoryMethod`3<System.Object,System.Object,System.Object>
struct FactoryMethod_3_t1233407108;
// System.Type
struct Type_t;
// System.Type[]
struct TypeU5BU5D_t3431720054;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"

// System.Void Zenject.FactoryMethod`3<System.Object,System.Object,System.Object>::.ctor()
extern "C"  void FactoryMethod_3__ctor_m373691810_gshared (FactoryMethod_3_t1233407108 * __this, const MethodInfo* method);
#define FactoryMethod_3__ctor_m373691810(__this, method) ((  void (*) (FactoryMethod_3_t1233407108 *, const MethodInfo*))FactoryMethod_3__ctor_m373691810_gshared)(__this, method)
// System.Type Zenject.FactoryMethod`3<System.Object,System.Object,System.Object>::get_ConstructedType()
extern "C"  Type_t * FactoryMethod_3_get_ConstructedType_m3445765389_gshared (FactoryMethod_3_t1233407108 * __this, const MethodInfo* method);
#define FactoryMethod_3_get_ConstructedType_m3445765389(__this, method) ((  Type_t * (*) (FactoryMethod_3_t1233407108 *, const MethodInfo*))FactoryMethod_3_get_ConstructedType_m3445765389_gshared)(__this, method)
// System.Type[] Zenject.FactoryMethod`3<System.Object,System.Object,System.Object>::get_ProvidedTypes()
extern "C"  TypeU5BU5D_t3431720054* FactoryMethod_3_get_ProvidedTypes_m4072671669_gshared (FactoryMethod_3_t1233407108 * __this, const MethodInfo* method);
#define FactoryMethod_3_get_ProvidedTypes_m4072671669(__this, method) ((  TypeU5BU5D_t3431720054* (*) (FactoryMethod_3_t1233407108 *, const MethodInfo*))FactoryMethod_3_get_ProvidedTypes_m4072671669_gshared)(__this, method)
// TValue Zenject.FactoryMethod`3<System.Object,System.Object,System.Object>::Create(TParam1,TParam2)
extern "C"  Il2CppObject * FactoryMethod_3_Create_m79999823_gshared (FactoryMethod_3_t1233407108 * __this, Il2CppObject * ___param10, Il2CppObject * ___param21, const MethodInfo* method);
#define FactoryMethod_3_Create_m79999823(__this, ___param10, ___param21, method) ((  Il2CppObject * (*) (FactoryMethod_3_t1233407108 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))FactoryMethod_3_Create_m79999823_gshared)(__this, ___param10, ___param21, method)
