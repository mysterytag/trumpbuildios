﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.ToObservableObservable`1/ToObservable<System.Object>
struct ToObservable_t1484005639;
// UniRx.Operators.ToObservableObservable`1<System.Object>
struct ToObservableObservable_1_t4048954016;
// UniRx.IObserver`1<System.Object>
struct IObserver_1_t3049105323;
// System.IDisposable
struct IDisposable_t1628921374;
// System.Object
struct Il2CppObject;
// System.Exception
struct Exception_t1967233988;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_Exception1967233988.h"

// System.Void UniRx.Operators.ToObservableObservable`1/ToObservable<System.Object>::.ctor(UniRx.Operators.ToObservableObservable`1<T>,UniRx.IObserver`1<T>,System.IDisposable)
extern "C"  void ToObservable__ctor_m1473176610_gshared (ToObservable_t1484005639 * __this, ToObservableObservable_1_t4048954016 * ___parent0, Il2CppObject* ___observer1, Il2CppObject * ___cancel2, const MethodInfo* method);
#define ToObservable__ctor_m1473176610(__this, ___parent0, ___observer1, ___cancel2, method) ((  void (*) (ToObservable_t1484005639 *, ToObservableObservable_1_t4048954016 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))ToObservable__ctor_m1473176610_gshared)(__this, ___parent0, ___observer1, ___cancel2, method)
// System.IDisposable UniRx.Operators.ToObservableObservable`1/ToObservable<System.Object>::Run()
extern "C"  Il2CppObject * ToObservable_Run_m219419013_gshared (ToObservable_t1484005639 * __this, const MethodInfo* method);
#define ToObservable_Run_m219419013(__this, method) ((  Il2CppObject * (*) (ToObservable_t1484005639 *, const MethodInfo*))ToObservable_Run_m219419013_gshared)(__this, method)
// System.Void UniRx.Operators.ToObservableObservable`1/ToObservable<System.Object>::OnNext(T)
extern "C"  void ToObservable_OnNext_m684784287_gshared (ToObservable_t1484005639 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define ToObservable_OnNext_m684784287(__this, ___value0, method) ((  void (*) (ToObservable_t1484005639 *, Il2CppObject *, const MethodInfo*))ToObservable_OnNext_m684784287_gshared)(__this, ___value0, method)
// System.Void UniRx.Operators.ToObservableObservable`1/ToObservable<System.Object>::OnError(System.Exception)
extern "C"  void ToObservable_OnError_m1885468590_gshared (ToObservable_t1484005639 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method);
#define ToObservable_OnError_m1885468590(__this, ___error0, method) ((  void (*) (ToObservable_t1484005639 *, Exception_t1967233988 *, const MethodInfo*))ToObservable_OnError_m1885468590_gshared)(__this, ___error0, method)
// System.Void UniRx.Operators.ToObservableObservable`1/ToObservable<System.Object>::OnCompleted()
extern "C"  void ToObservable_OnCompleted_m1637105665_gshared (ToObservable_t1484005639 * __this, const MethodInfo* method);
#define ToObservable_OnCompleted_m1637105665(__this, method) ((  void (*) (ToObservable_t1484005639 *, const MethodInfo*))ToObservable_OnCompleted_m1637105665_gshared)(__this, method)
