﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.ClientModels.GetLeaderboardForUsersCharactersResult
struct GetLeaderboardForUsersCharactersResult_t1964611997;
// System.Collections.Generic.List`1<PlayFab.ClientModels.CharacterLeaderboardEntry>
struct List_1_t3914494247;

#include "codegen/il2cpp-codegen.h"

// System.Void PlayFab.ClientModels.GetLeaderboardForUsersCharactersResult::.ctor()
extern "C"  void GetLeaderboardForUsersCharactersResult__ctor_m4274772928 (GetLeaderboardForUsersCharactersResult_t1964611997 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<PlayFab.ClientModels.CharacterLeaderboardEntry> PlayFab.ClientModels.GetLeaderboardForUsersCharactersResult::get_Leaderboard()
extern "C"  List_1_t3914494247 * GetLeaderboardForUsersCharactersResult_get_Leaderboard_m1402436709 (GetLeaderboardForUsersCharactersResult_t1964611997 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.GetLeaderboardForUsersCharactersResult::set_Leaderboard(System.Collections.Generic.List`1<PlayFab.ClientModels.CharacterLeaderboardEntry>)
extern "C"  void GetLeaderboardForUsersCharactersResult_set_Leaderboard_m1007051732 (GetLeaderboardForUsersCharactersResult_t1964611997 * __this, List_1_t3914494247 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
