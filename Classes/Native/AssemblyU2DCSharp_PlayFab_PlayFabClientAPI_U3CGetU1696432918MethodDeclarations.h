﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.PlayFabClientAPI/<GetUserReadOnlyData>c__AnonStorey95
struct U3CGetUserReadOnlyDataU3Ec__AnonStorey95_t1696432918;
// PlayFab.CallRequestContainer
struct CallRequestContainer_t432318609;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharpU2Dfirstpass_PlayFab_CallRequestCo432318609.h"

// System.Void PlayFab.PlayFabClientAPI/<GetUserReadOnlyData>c__AnonStorey95::.ctor()
extern "C"  void U3CGetUserReadOnlyDataU3Ec__AnonStorey95__ctor_m1515146493 (U3CGetUserReadOnlyDataU3Ec__AnonStorey95_t1696432918 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/<GetUserReadOnlyData>c__AnonStorey95::<>m__82(PlayFab.CallRequestContainer)
extern "C"  void U3CGetUserReadOnlyDataU3Ec__AnonStorey95_U3CU3Em__82_m1624266901 (U3CGetUserReadOnlyDataU3Ec__AnonStorey95_t1696432918 * __this, CallRequestContainer_t432318609 * ___requestContainer0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
