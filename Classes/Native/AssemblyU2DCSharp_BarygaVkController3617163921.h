﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// GlobalStat
struct GlobalStat_t1134678199;
// TutorialPlayer
struct TutorialPlayer_t4281139199;
// UniRx.ReadOnlyReactiveProperty`1<System.Boolean>
struct ReadOnlyReactiveProperty_1_t3000407357;
// IVkConnector
struct IVkConnector_t2786758927;
// Message
struct Message_t2619578343;
// System.Func`3<System.Boolean,System.Boolean,System.Boolean>
struct Func_3_t3063550794;
// System.Func`2<System.Boolean,System.Boolean>
struct Func_2_t1008118516;

#include "UnityEngine_UnityEngine_MonoBehaviour3012272455.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BarygaVkController
struct  BarygaVkController_t3617163921  : public MonoBehaviour_t3012272455
{
public:
	// System.Boolean BarygaVkController::inviting
	bool ___inviting_2;
	// GlobalStat BarygaVkController::GlobalStat
	GlobalStat_t1134678199 * ___GlobalStat_3;
	// TutorialPlayer BarygaVkController::TutorialPlayer
	TutorialPlayer_t4281139199 * ___TutorialPlayer_4;
	// UniRx.ReadOnlyReactiveProperty`1<System.Boolean> BarygaVkController::JoinGroupRewardApplied
	ReadOnlyReactiveProperty_1_t3000407357 * ___JoinGroupRewardApplied_5;
	// UniRx.ReadOnlyReactiveProperty`1<System.Boolean> BarygaVkController::vkWallPostRewardApplied
	ReadOnlyReactiveProperty_1_t3000407357 * ___vkWallPostRewardApplied_6;
	// IVkConnector BarygaVkController::VkConnector
	Il2CppObject * ___VkConnector_7;
	// Message BarygaVkController::Message
	Message_t2619578343 * ___Message_8;

public:
	inline static int32_t get_offset_of_inviting_2() { return static_cast<int32_t>(offsetof(BarygaVkController_t3617163921, ___inviting_2)); }
	inline bool get_inviting_2() const { return ___inviting_2; }
	inline bool* get_address_of_inviting_2() { return &___inviting_2; }
	inline void set_inviting_2(bool value)
	{
		___inviting_2 = value;
	}

	inline static int32_t get_offset_of_GlobalStat_3() { return static_cast<int32_t>(offsetof(BarygaVkController_t3617163921, ___GlobalStat_3)); }
	inline GlobalStat_t1134678199 * get_GlobalStat_3() const { return ___GlobalStat_3; }
	inline GlobalStat_t1134678199 ** get_address_of_GlobalStat_3() { return &___GlobalStat_3; }
	inline void set_GlobalStat_3(GlobalStat_t1134678199 * value)
	{
		___GlobalStat_3 = value;
		Il2CppCodeGenWriteBarrier(&___GlobalStat_3, value);
	}

	inline static int32_t get_offset_of_TutorialPlayer_4() { return static_cast<int32_t>(offsetof(BarygaVkController_t3617163921, ___TutorialPlayer_4)); }
	inline TutorialPlayer_t4281139199 * get_TutorialPlayer_4() const { return ___TutorialPlayer_4; }
	inline TutorialPlayer_t4281139199 ** get_address_of_TutorialPlayer_4() { return &___TutorialPlayer_4; }
	inline void set_TutorialPlayer_4(TutorialPlayer_t4281139199 * value)
	{
		___TutorialPlayer_4 = value;
		Il2CppCodeGenWriteBarrier(&___TutorialPlayer_4, value);
	}

	inline static int32_t get_offset_of_JoinGroupRewardApplied_5() { return static_cast<int32_t>(offsetof(BarygaVkController_t3617163921, ___JoinGroupRewardApplied_5)); }
	inline ReadOnlyReactiveProperty_1_t3000407357 * get_JoinGroupRewardApplied_5() const { return ___JoinGroupRewardApplied_5; }
	inline ReadOnlyReactiveProperty_1_t3000407357 ** get_address_of_JoinGroupRewardApplied_5() { return &___JoinGroupRewardApplied_5; }
	inline void set_JoinGroupRewardApplied_5(ReadOnlyReactiveProperty_1_t3000407357 * value)
	{
		___JoinGroupRewardApplied_5 = value;
		Il2CppCodeGenWriteBarrier(&___JoinGroupRewardApplied_5, value);
	}

	inline static int32_t get_offset_of_vkWallPostRewardApplied_6() { return static_cast<int32_t>(offsetof(BarygaVkController_t3617163921, ___vkWallPostRewardApplied_6)); }
	inline ReadOnlyReactiveProperty_1_t3000407357 * get_vkWallPostRewardApplied_6() const { return ___vkWallPostRewardApplied_6; }
	inline ReadOnlyReactiveProperty_1_t3000407357 ** get_address_of_vkWallPostRewardApplied_6() { return &___vkWallPostRewardApplied_6; }
	inline void set_vkWallPostRewardApplied_6(ReadOnlyReactiveProperty_1_t3000407357 * value)
	{
		___vkWallPostRewardApplied_6 = value;
		Il2CppCodeGenWriteBarrier(&___vkWallPostRewardApplied_6, value);
	}

	inline static int32_t get_offset_of_VkConnector_7() { return static_cast<int32_t>(offsetof(BarygaVkController_t3617163921, ___VkConnector_7)); }
	inline Il2CppObject * get_VkConnector_7() const { return ___VkConnector_7; }
	inline Il2CppObject ** get_address_of_VkConnector_7() { return &___VkConnector_7; }
	inline void set_VkConnector_7(Il2CppObject * value)
	{
		___VkConnector_7 = value;
		Il2CppCodeGenWriteBarrier(&___VkConnector_7, value);
	}

	inline static int32_t get_offset_of_Message_8() { return static_cast<int32_t>(offsetof(BarygaVkController_t3617163921, ___Message_8)); }
	inline Message_t2619578343 * get_Message_8() const { return ___Message_8; }
	inline Message_t2619578343 ** get_address_of_Message_8() { return &___Message_8; }
	inline void set_Message_8(Message_t2619578343 * value)
	{
		___Message_8 = value;
		Il2CppCodeGenWriteBarrier(&___Message_8, value);
	}
};

struct BarygaVkController_t3617163921_StaticFields
{
public:
	// System.Func`3<System.Boolean,System.Boolean,System.Boolean> BarygaVkController::<>f__am$cache7
	Func_3_t3063550794 * ___U3CU3Ef__amU24cache7_9;
	// System.Func`2<System.Boolean,System.Boolean> BarygaVkController::<>f__am$cache8
	Func_2_t1008118516 * ___U3CU3Ef__amU24cache8_10;
	// System.Func`2<System.Boolean,System.Boolean> BarygaVkController::<>f__am$cache9
	Func_2_t1008118516 * ___U3CU3Ef__amU24cache9_11;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache7_9() { return static_cast<int32_t>(offsetof(BarygaVkController_t3617163921_StaticFields, ___U3CU3Ef__amU24cache7_9)); }
	inline Func_3_t3063550794 * get_U3CU3Ef__amU24cache7_9() const { return ___U3CU3Ef__amU24cache7_9; }
	inline Func_3_t3063550794 ** get_address_of_U3CU3Ef__amU24cache7_9() { return &___U3CU3Ef__amU24cache7_9; }
	inline void set_U3CU3Ef__amU24cache7_9(Func_3_t3063550794 * value)
	{
		___U3CU3Ef__amU24cache7_9 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache7_9, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache8_10() { return static_cast<int32_t>(offsetof(BarygaVkController_t3617163921_StaticFields, ___U3CU3Ef__amU24cache8_10)); }
	inline Func_2_t1008118516 * get_U3CU3Ef__amU24cache8_10() const { return ___U3CU3Ef__amU24cache8_10; }
	inline Func_2_t1008118516 ** get_address_of_U3CU3Ef__amU24cache8_10() { return &___U3CU3Ef__amU24cache8_10; }
	inline void set_U3CU3Ef__amU24cache8_10(Func_2_t1008118516 * value)
	{
		___U3CU3Ef__amU24cache8_10 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache8_10, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache9_11() { return static_cast<int32_t>(offsetof(BarygaVkController_t3617163921_StaticFields, ___U3CU3Ef__amU24cache9_11)); }
	inline Func_2_t1008118516 * get_U3CU3Ef__amU24cache9_11() const { return ___U3CU3Ef__amU24cache9_11; }
	inline Func_2_t1008118516 ** get_address_of_U3CU3Ef__amU24cache9_11() { return &___U3CU3Ef__amU24cache9_11; }
	inline void set_U3CU3Ef__amU24cache9_11(Func_2_t1008118516 * value)
	{
		___U3CU3Ef__amU24cache9_11 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache9_11, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
