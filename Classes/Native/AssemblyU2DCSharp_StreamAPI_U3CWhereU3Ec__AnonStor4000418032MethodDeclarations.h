﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// StreamAPI/<Where>c__AnonStorey12E
struct U3CWhereU3Ec__AnonStorey12E_t4000418032;
// System.IDisposable
struct IDisposable_t1628921374;
// System.Action
struct Action_t437523947;

#include "codegen/il2cpp-codegen.h"
#include "System_Core_System_Action437523947.h"
#include "AssemblyU2DCSharp_Priority3194150340.h"

// System.Void StreamAPI/<Where>c__AnonStorey12E::.ctor()
extern "C"  void U3CWhereU3Ec__AnonStorey12E__ctor_m317651856 (U3CWhereU3Ec__AnonStorey12E_t4000418032 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IDisposable StreamAPI/<Where>c__AnonStorey12E::<>m__1C2(System.Action,Priority)
extern "C"  Il2CppObject * U3CWhereU3Ec__AnonStorey12E_U3CU3Em__1C2_m2842740827 (U3CWhereU3Ec__AnonStorey12E_t4000418032 * __this, Action_t437523947 * ___reaction0, int32_t ___p1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
