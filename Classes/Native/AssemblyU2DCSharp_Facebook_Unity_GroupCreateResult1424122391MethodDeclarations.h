﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Facebook.Unity.GroupCreateResult
struct GroupCreateResult_t1424122391;
// Facebook.Unity.ResultContainer
struct ResultContainer_t79372963;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Facebook_Unity_ResultContainer79372963.h"
#include "mscorlib_System_String968488902.h"

// System.Void Facebook.Unity.GroupCreateResult::.ctor(Facebook.Unity.ResultContainer)
extern "C"  void GroupCreateResult__ctor_m1256436129 (GroupCreateResult_t1424122391 * __this, ResultContainer_t79372963 * ___resultContainer0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Facebook.Unity.GroupCreateResult::get_GroupId()
extern "C"  String_t* GroupCreateResult_get_GroupId_m3329014096 (GroupCreateResult_t1424122391 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Facebook.Unity.GroupCreateResult::set_GroupId(System.String)
extern "C"  void GroupCreateResult_set_GroupId_m2694889859 (GroupCreateResult_t1424122391 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Facebook.Unity.GroupCreateResult::ToString()
extern "C"  String_t* GroupCreateResult_ToString_m3451307183 (GroupCreateResult_t1424122391 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
