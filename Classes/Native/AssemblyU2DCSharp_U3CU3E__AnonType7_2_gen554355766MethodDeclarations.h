﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// <>__AnonType7`2<System.Boolean,System.Boolean>
struct U3CU3E__AnonType7_2_t554355766;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"

// System.Void <>__AnonType7`2<System.Boolean,System.Boolean>::.ctor(<reward>__T,<loggedIn>__T)
extern "C"  void U3CU3E__AnonType7_2__ctor_m1817581750_gshared (U3CU3E__AnonType7_2_t554355766 * __this, bool ___reward0, bool ___loggedIn1, const MethodInfo* method);
#define U3CU3E__AnonType7_2__ctor_m1817581750(__this, ___reward0, ___loggedIn1, method) ((  void (*) (U3CU3E__AnonType7_2_t554355766 *, bool, bool, const MethodInfo*))U3CU3E__AnonType7_2__ctor_m1817581750_gshared)(__this, ___reward0, ___loggedIn1, method)
// <reward>__T <>__AnonType7`2<System.Boolean,System.Boolean>::get_reward()
extern "C"  bool U3CU3E__AnonType7_2_get_reward_m390852512_gshared (U3CU3E__AnonType7_2_t554355766 * __this, const MethodInfo* method);
#define U3CU3E__AnonType7_2_get_reward_m390852512(__this, method) ((  bool (*) (U3CU3E__AnonType7_2_t554355766 *, const MethodInfo*))U3CU3E__AnonType7_2_get_reward_m390852512_gshared)(__this, method)
// <loggedIn>__T <>__AnonType7`2<System.Boolean,System.Boolean>::get_loggedIn()
extern "C"  bool U3CU3E__AnonType7_2_get_loggedIn_m3813944032_gshared (U3CU3E__AnonType7_2_t554355766 * __this, const MethodInfo* method);
#define U3CU3E__AnonType7_2_get_loggedIn_m3813944032(__this, method) ((  bool (*) (U3CU3E__AnonType7_2_t554355766 *, const MethodInfo*))U3CU3E__AnonType7_2_get_loggedIn_m3813944032_gshared)(__this, method)
// System.Boolean <>__AnonType7`2<System.Boolean,System.Boolean>::Equals(System.Object)
extern "C"  bool U3CU3E__AnonType7_2_Equals_m1613057483_gshared (U3CU3E__AnonType7_2_t554355766 * __this, Il2CppObject * ___obj0, const MethodInfo* method);
#define U3CU3E__AnonType7_2_Equals_m1613057483(__this, ___obj0, method) ((  bool (*) (U3CU3E__AnonType7_2_t554355766 *, Il2CppObject *, const MethodInfo*))U3CU3E__AnonType7_2_Equals_m1613057483_gshared)(__this, ___obj0, method)
// System.Int32 <>__AnonType7`2<System.Boolean,System.Boolean>::GetHashCode()
extern "C"  int32_t U3CU3E__AnonType7_2_GetHashCode_m1990024559_gshared (U3CU3E__AnonType7_2_t554355766 * __this, const MethodInfo* method);
#define U3CU3E__AnonType7_2_GetHashCode_m1990024559(__this, method) ((  int32_t (*) (U3CU3E__AnonType7_2_t554355766 *, const MethodInfo*))U3CU3E__AnonType7_2_GetHashCode_m1990024559_gshared)(__this, method)
// System.String <>__AnonType7`2<System.Boolean,System.Boolean>::ToString()
extern "C"  String_t* U3CU3E__AnonType7_2_ToString_m2341396037_gshared (U3CU3E__AnonType7_2_t554355766 * __this, const MethodInfo* method);
#define U3CU3E__AnonType7_2_ToString_m2341396037(__this, method) ((  String_t* (*) (U3CU3E__AnonType7_2_t554355766 *, const MethodInfo*))U3CU3E__AnonType7_2_ToString_m2341396037_gshared)(__this, method)
