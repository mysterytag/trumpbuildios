﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.PlayFabClientAPI/GetPlayFabIDsFromGoogleIDsRequestCallback
struct GetPlayFabIDsFromGoogleIDsRequestCallback_t4137431546;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;
// PlayFab.ClientModels.GetPlayFabIDsFromGoogleIDsRequest
struct GetPlayFabIDsFromGoogleIDsRequest_t3277397669;
// System.IAsyncResult
struct IAsyncResult_t537683269;
// System.AsyncCallback
struct AsyncCallback_t1363551830;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_IntPtr676692020.h"
#include "mscorlib_System_String968488902.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_GetPlayFabI3277397669.h"
#include "mscorlib_System_AsyncCallback1363551830.h"

// System.Void PlayFab.PlayFabClientAPI/GetPlayFabIDsFromGoogleIDsRequestCallback::.ctor(System.Object,System.IntPtr)
extern "C"  void GetPlayFabIDsFromGoogleIDsRequestCallback__ctor_m148145545 (GetPlayFabIDsFromGoogleIDsRequestCallback_t4137431546 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/GetPlayFabIDsFromGoogleIDsRequestCallback::Invoke(System.String,System.Int32,PlayFab.ClientModels.GetPlayFabIDsFromGoogleIDsRequest,System.Object)
extern "C"  void GetPlayFabIDsFromGoogleIDsRequestCallback_Invoke_m3569827685 (GetPlayFabIDsFromGoogleIDsRequestCallback_t4137431546 * __this, String_t* ___urlPath0, int32_t ___callId1, GetPlayFabIDsFromGoogleIDsRequest_t3277397669 * ___request2, Il2CppObject * ___customData3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
extern "C" void pinvoke_delegate_wrapper_GetPlayFabIDsFromGoogleIDsRequestCallback_t4137431546(Il2CppObject* delegate, String_t* ___urlPath0, int32_t ___callId1, GetPlayFabIDsFromGoogleIDsRequest_t3277397669 * ___request2, Il2CppObject * ___customData3);
// System.IAsyncResult PlayFab.PlayFabClientAPI/GetPlayFabIDsFromGoogleIDsRequestCallback::BeginInvoke(System.String,System.Int32,PlayFab.ClientModels.GetPlayFabIDsFromGoogleIDsRequest,System.Object,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * GetPlayFabIDsFromGoogleIDsRequestCallback_BeginInvoke_m2051307678 (GetPlayFabIDsFromGoogleIDsRequestCallback_t4137431546 * __this, String_t* ___urlPath0, int32_t ___callId1, GetPlayFabIDsFromGoogleIDsRequest_t3277397669 * ___request2, Il2CppObject * ___customData3, AsyncCallback_t1363551830 * ___callback4, Il2CppObject * ___object5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/GetPlayFabIDsFromGoogleIDsRequestCallback::EndInvoke(System.IAsyncResult)
extern "C"  void GetPlayFabIDsFromGoogleIDsRequestCallback_EndInvoke_m3545375769 (GetPlayFabIDsFromGoogleIDsRequestCallback_t4137431546 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
