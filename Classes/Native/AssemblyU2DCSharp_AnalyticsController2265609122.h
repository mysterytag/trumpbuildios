﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// AnalyticsController
struct AnalyticsController_t2265609122;
// FbController
struct FbController_t3069175192;
// GlobalStat
struct GlobalStat_t1134678199;
// GameController
struct GameController_t2782302542;
// BarygaVkController
struct BarygaVkController_t3617163921;
// IVkConnector
struct IVkConnector_t2786758927;
// System.Collections.Generic.Dictionary`2<System.Int32,System.String>
struct Dictionary_2_t1459299685;
// System.Collections.Generic.Dictionary`2<System.Double,System.String>
struct Dictionary_2_t697597558;
// System.Collections.Generic.Dictionary`2<System.String,System.Int32>
struct Dictionary_2_t190145395;
// System.Func`2<System.Boolean,System.Boolean>
struct Func_2_t1008118516;

#include "UnityEngine_UnityEngine_MonoBehaviour3012272455.h"
#include "mscorlib_System_DateTime339033936.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AnalyticsController
struct  AnalyticsController_t2265609122  : public MonoBehaviour_t3012272455
{
public:
	// System.String AnalyticsController::Social
	String_t* ___Social_3;
	// System.Boolean AnalyticsController::localyticsEnabled
	bool ___localyticsEnabled_4;
	// System.Boolean AnalyticsController::mixpanelEnabled
	bool ___mixpanelEnabled_5;
	// System.Boolean AnalyticsController::flurryEnabled
	bool ___flurryEnabled_6;
	// System.Boolean AnalyticsController::appsFlyerEnabled
	bool ___appsFlyerEnabled_7;
	// System.Boolean AnalyticsController::gameanalyticsEnabled
	bool ___gameanalyticsEnabled_8;
	// System.Boolean AnalyticsController::loggingEnabled
	bool ___loggingEnabled_9;
	// System.Boolean AnalyticsController::checksession
	bool ___checksession_10;
	// System.Single AnalyticsController::time
	float ___time_11;
	// FbController AnalyticsController::fbConnector
	FbController_t3069175192 * ___fbConnector_13;
	// GlobalStat AnalyticsController::GlobalStat
	GlobalStat_t1134678199 * ___GlobalStat_14;
	// System.DateTime AnalyticsController::timeLoaded
	DateTime_t339033936  ___timeLoaded_15;
	// GameController AnalyticsController::GameController
	GameController_t2782302542 * ___GameController_16;
	// BarygaVkController AnalyticsController::BarygaVkController
	BarygaVkController_t3617163921 * ___BarygaVkController_17;
	// IVkConnector AnalyticsController::VkConnector
	Il2CppObject * ___VkConnector_18;

public:
	inline static int32_t get_offset_of_Social_3() { return static_cast<int32_t>(offsetof(AnalyticsController_t2265609122, ___Social_3)); }
	inline String_t* get_Social_3() const { return ___Social_3; }
	inline String_t** get_address_of_Social_3() { return &___Social_3; }
	inline void set_Social_3(String_t* value)
	{
		___Social_3 = value;
		Il2CppCodeGenWriteBarrier(&___Social_3, value);
	}

	inline static int32_t get_offset_of_localyticsEnabled_4() { return static_cast<int32_t>(offsetof(AnalyticsController_t2265609122, ___localyticsEnabled_4)); }
	inline bool get_localyticsEnabled_4() const { return ___localyticsEnabled_4; }
	inline bool* get_address_of_localyticsEnabled_4() { return &___localyticsEnabled_4; }
	inline void set_localyticsEnabled_4(bool value)
	{
		___localyticsEnabled_4 = value;
	}

	inline static int32_t get_offset_of_mixpanelEnabled_5() { return static_cast<int32_t>(offsetof(AnalyticsController_t2265609122, ___mixpanelEnabled_5)); }
	inline bool get_mixpanelEnabled_5() const { return ___mixpanelEnabled_5; }
	inline bool* get_address_of_mixpanelEnabled_5() { return &___mixpanelEnabled_5; }
	inline void set_mixpanelEnabled_5(bool value)
	{
		___mixpanelEnabled_5 = value;
	}

	inline static int32_t get_offset_of_flurryEnabled_6() { return static_cast<int32_t>(offsetof(AnalyticsController_t2265609122, ___flurryEnabled_6)); }
	inline bool get_flurryEnabled_6() const { return ___flurryEnabled_6; }
	inline bool* get_address_of_flurryEnabled_6() { return &___flurryEnabled_6; }
	inline void set_flurryEnabled_6(bool value)
	{
		___flurryEnabled_6 = value;
	}

	inline static int32_t get_offset_of_appsFlyerEnabled_7() { return static_cast<int32_t>(offsetof(AnalyticsController_t2265609122, ___appsFlyerEnabled_7)); }
	inline bool get_appsFlyerEnabled_7() const { return ___appsFlyerEnabled_7; }
	inline bool* get_address_of_appsFlyerEnabled_7() { return &___appsFlyerEnabled_7; }
	inline void set_appsFlyerEnabled_7(bool value)
	{
		___appsFlyerEnabled_7 = value;
	}

	inline static int32_t get_offset_of_gameanalyticsEnabled_8() { return static_cast<int32_t>(offsetof(AnalyticsController_t2265609122, ___gameanalyticsEnabled_8)); }
	inline bool get_gameanalyticsEnabled_8() const { return ___gameanalyticsEnabled_8; }
	inline bool* get_address_of_gameanalyticsEnabled_8() { return &___gameanalyticsEnabled_8; }
	inline void set_gameanalyticsEnabled_8(bool value)
	{
		___gameanalyticsEnabled_8 = value;
	}

	inline static int32_t get_offset_of_loggingEnabled_9() { return static_cast<int32_t>(offsetof(AnalyticsController_t2265609122, ___loggingEnabled_9)); }
	inline bool get_loggingEnabled_9() const { return ___loggingEnabled_9; }
	inline bool* get_address_of_loggingEnabled_9() { return &___loggingEnabled_9; }
	inline void set_loggingEnabled_9(bool value)
	{
		___loggingEnabled_9 = value;
	}

	inline static int32_t get_offset_of_checksession_10() { return static_cast<int32_t>(offsetof(AnalyticsController_t2265609122, ___checksession_10)); }
	inline bool get_checksession_10() const { return ___checksession_10; }
	inline bool* get_address_of_checksession_10() { return &___checksession_10; }
	inline void set_checksession_10(bool value)
	{
		___checksession_10 = value;
	}

	inline static int32_t get_offset_of_time_11() { return static_cast<int32_t>(offsetof(AnalyticsController_t2265609122, ___time_11)); }
	inline float get_time_11() const { return ___time_11; }
	inline float* get_address_of_time_11() { return &___time_11; }
	inline void set_time_11(float value)
	{
		___time_11 = value;
	}

	inline static int32_t get_offset_of_fbConnector_13() { return static_cast<int32_t>(offsetof(AnalyticsController_t2265609122, ___fbConnector_13)); }
	inline FbController_t3069175192 * get_fbConnector_13() const { return ___fbConnector_13; }
	inline FbController_t3069175192 ** get_address_of_fbConnector_13() { return &___fbConnector_13; }
	inline void set_fbConnector_13(FbController_t3069175192 * value)
	{
		___fbConnector_13 = value;
		Il2CppCodeGenWriteBarrier(&___fbConnector_13, value);
	}

	inline static int32_t get_offset_of_GlobalStat_14() { return static_cast<int32_t>(offsetof(AnalyticsController_t2265609122, ___GlobalStat_14)); }
	inline GlobalStat_t1134678199 * get_GlobalStat_14() const { return ___GlobalStat_14; }
	inline GlobalStat_t1134678199 ** get_address_of_GlobalStat_14() { return &___GlobalStat_14; }
	inline void set_GlobalStat_14(GlobalStat_t1134678199 * value)
	{
		___GlobalStat_14 = value;
		Il2CppCodeGenWriteBarrier(&___GlobalStat_14, value);
	}

	inline static int32_t get_offset_of_timeLoaded_15() { return static_cast<int32_t>(offsetof(AnalyticsController_t2265609122, ___timeLoaded_15)); }
	inline DateTime_t339033936  get_timeLoaded_15() const { return ___timeLoaded_15; }
	inline DateTime_t339033936 * get_address_of_timeLoaded_15() { return &___timeLoaded_15; }
	inline void set_timeLoaded_15(DateTime_t339033936  value)
	{
		___timeLoaded_15 = value;
	}

	inline static int32_t get_offset_of_GameController_16() { return static_cast<int32_t>(offsetof(AnalyticsController_t2265609122, ___GameController_16)); }
	inline GameController_t2782302542 * get_GameController_16() const { return ___GameController_16; }
	inline GameController_t2782302542 ** get_address_of_GameController_16() { return &___GameController_16; }
	inline void set_GameController_16(GameController_t2782302542 * value)
	{
		___GameController_16 = value;
		Il2CppCodeGenWriteBarrier(&___GameController_16, value);
	}

	inline static int32_t get_offset_of_BarygaVkController_17() { return static_cast<int32_t>(offsetof(AnalyticsController_t2265609122, ___BarygaVkController_17)); }
	inline BarygaVkController_t3617163921 * get_BarygaVkController_17() const { return ___BarygaVkController_17; }
	inline BarygaVkController_t3617163921 ** get_address_of_BarygaVkController_17() { return &___BarygaVkController_17; }
	inline void set_BarygaVkController_17(BarygaVkController_t3617163921 * value)
	{
		___BarygaVkController_17 = value;
		Il2CppCodeGenWriteBarrier(&___BarygaVkController_17, value);
	}

	inline static int32_t get_offset_of_VkConnector_18() { return static_cast<int32_t>(offsetof(AnalyticsController_t2265609122, ___VkConnector_18)); }
	inline Il2CppObject * get_VkConnector_18() const { return ___VkConnector_18; }
	inline Il2CppObject ** get_address_of_VkConnector_18() { return &___VkConnector_18; }
	inline void set_VkConnector_18(Il2CppObject * value)
	{
		___VkConnector_18 = value;
		Il2CppCodeGenWriteBarrier(&___VkConnector_18, value);
	}
};

struct AnalyticsController_t2265609122_StaticFields
{
public:
	// AnalyticsController AnalyticsController::Instance
	AnalyticsController_t2265609122 * ___Instance_12;
	// System.Collections.Generic.Dictionary`2<System.Int32,System.String> AnalyticsController::ageDimDict
	Dictionary_2_t1459299685 * ___ageDimDict_19;
	// System.Collections.Generic.Dictionary`2<System.Int32,System.String> AnalyticsController::ageDimDictGameAnalytics
	Dictionary_2_t1459299685 * ___ageDimDictGameAnalytics_20;
	// System.Collections.Generic.Dictionary`2<System.Int32,System.String> AnalyticsController::heavyActionDim
	Dictionary_2_t1459299685 * ___heavyActionDim_21;
	// System.Collections.Generic.Dictionary`2<System.Int32,System.String> AnalyticsController::paymentDimDict
	Dictionary_2_t1459299685 * ___paymentDimDict_22;
	// System.Collections.Generic.Dictionary`2<System.Int32,System.String> AnalyticsController::sessionNumDict
	Dictionary_2_t1459299685 * ___sessionNumDict_23;
	// System.Collections.Generic.Dictionary`2<System.Double,System.String> AnalyticsController::cashDimDict
	Dictionary_2_t697597558 * ___cashDimDict_24;
	// System.Collections.Generic.Dictionary`2<System.Int32,System.String> AnalyticsController::friendInviteDimDict
	Dictionary_2_t1459299685 * ___friendInviteDimDict_25;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> AnalyticsController::<>f__switch$map7
	Dictionary_2_t190145395 * ___U3CU3Ef__switchU24map7_26;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> AnalyticsController::<>f__switch$map8
	Dictionary_2_t190145395 * ___U3CU3Ef__switchU24map8_27;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> AnalyticsController::<>f__switch$map9
	Dictionary_2_t190145395 * ___U3CU3Ef__switchU24map9_28;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> AnalyticsController::<>f__switch$mapA
	Dictionary_2_t190145395 * ___U3CU3Ef__switchU24mapA_29;
	// System.Func`2<System.Boolean,System.Boolean> AnalyticsController::<>f__am$cache1B
	Func_2_t1008118516 * ___U3CU3Ef__amU24cache1B_30;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> AnalyticsController::<>f__switch$mapB
	Dictionary_2_t190145395 * ___U3CU3Ef__switchU24mapB_31;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> AnalyticsController::<>f__switch$mapC
	Dictionary_2_t190145395 * ___U3CU3Ef__switchU24mapC_32;

public:
	inline static int32_t get_offset_of_Instance_12() { return static_cast<int32_t>(offsetof(AnalyticsController_t2265609122_StaticFields, ___Instance_12)); }
	inline AnalyticsController_t2265609122 * get_Instance_12() const { return ___Instance_12; }
	inline AnalyticsController_t2265609122 ** get_address_of_Instance_12() { return &___Instance_12; }
	inline void set_Instance_12(AnalyticsController_t2265609122 * value)
	{
		___Instance_12 = value;
		Il2CppCodeGenWriteBarrier(&___Instance_12, value);
	}

	inline static int32_t get_offset_of_ageDimDict_19() { return static_cast<int32_t>(offsetof(AnalyticsController_t2265609122_StaticFields, ___ageDimDict_19)); }
	inline Dictionary_2_t1459299685 * get_ageDimDict_19() const { return ___ageDimDict_19; }
	inline Dictionary_2_t1459299685 ** get_address_of_ageDimDict_19() { return &___ageDimDict_19; }
	inline void set_ageDimDict_19(Dictionary_2_t1459299685 * value)
	{
		___ageDimDict_19 = value;
		Il2CppCodeGenWriteBarrier(&___ageDimDict_19, value);
	}

	inline static int32_t get_offset_of_ageDimDictGameAnalytics_20() { return static_cast<int32_t>(offsetof(AnalyticsController_t2265609122_StaticFields, ___ageDimDictGameAnalytics_20)); }
	inline Dictionary_2_t1459299685 * get_ageDimDictGameAnalytics_20() const { return ___ageDimDictGameAnalytics_20; }
	inline Dictionary_2_t1459299685 ** get_address_of_ageDimDictGameAnalytics_20() { return &___ageDimDictGameAnalytics_20; }
	inline void set_ageDimDictGameAnalytics_20(Dictionary_2_t1459299685 * value)
	{
		___ageDimDictGameAnalytics_20 = value;
		Il2CppCodeGenWriteBarrier(&___ageDimDictGameAnalytics_20, value);
	}

	inline static int32_t get_offset_of_heavyActionDim_21() { return static_cast<int32_t>(offsetof(AnalyticsController_t2265609122_StaticFields, ___heavyActionDim_21)); }
	inline Dictionary_2_t1459299685 * get_heavyActionDim_21() const { return ___heavyActionDim_21; }
	inline Dictionary_2_t1459299685 ** get_address_of_heavyActionDim_21() { return &___heavyActionDim_21; }
	inline void set_heavyActionDim_21(Dictionary_2_t1459299685 * value)
	{
		___heavyActionDim_21 = value;
		Il2CppCodeGenWriteBarrier(&___heavyActionDim_21, value);
	}

	inline static int32_t get_offset_of_paymentDimDict_22() { return static_cast<int32_t>(offsetof(AnalyticsController_t2265609122_StaticFields, ___paymentDimDict_22)); }
	inline Dictionary_2_t1459299685 * get_paymentDimDict_22() const { return ___paymentDimDict_22; }
	inline Dictionary_2_t1459299685 ** get_address_of_paymentDimDict_22() { return &___paymentDimDict_22; }
	inline void set_paymentDimDict_22(Dictionary_2_t1459299685 * value)
	{
		___paymentDimDict_22 = value;
		Il2CppCodeGenWriteBarrier(&___paymentDimDict_22, value);
	}

	inline static int32_t get_offset_of_sessionNumDict_23() { return static_cast<int32_t>(offsetof(AnalyticsController_t2265609122_StaticFields, ___sessionNumDict_23)); }
	inline Dictionary_2_t1459299685 * get_sessionNumDict_23() const { return ___sessionNumDict_23; }
	inline Dictionary_2_t1459299685 ** get_address_of_sessionNumDict_23() { return &___sessionNumDict_23; }
	inline void set_sessionNumDict_23(Dictionary_2_t1459299685 * value)
	{
		___sessionNumDict_23 = value;
		Il2CppCodeGenWriteBarrier(&___sessionNumDict_23, value);
	}

	inline static int32_t get_offset_of_cashDimDict_24() { return static_cast<int32_t>(offsetof(AnalyticsController_t2265609122_StaticFields, ___cashDimDict_24)); }
	inline Dictionary_2_t697597558 * get_cashDimDict_24() const { return ___cashDimDict_24; }
	inline Dictionary_2_t697597558 ** get_address_of_cashDimDict_24() { return &___cashDimDict_24; }
	inline void set_cashDimDict_24(Dictionary_2_t697597558 * value)
	{
		___cashDimDict_24 = value;
		Il2CppCodeGenWriteBarrier(&___cashDimDict_24, value);
	}

	inline static int32_t get_offset_of_friendInviteDimDict_25() { return static_cast<int32_t>(offsetof(AnalyticsController_t2265609122_StaticFields, ___friendInviteDimDict_25)); }
	inline Dictionary_2_t1459299685 * get_friendInviteDimDict_25() const { return ___friendInviteDimDict_25; }
	inline Dictionary_2_t1459299685 ** get_address_of_friendInviteDimDict_25() { return &___friendInviteDimDict_25; }
	inline void set_friendInviteDimDict_25(Dictionary_2_t1459299685 * value)
	{
		___friendInviteDimDict_25 = value;
		Il2CppCodeGenWriteBarrier(&___friendInviteDimDict_25, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__switchU24map7_26() { return static_cast<int32_t>(offsetof(AnalyticsController_t2265609122_StaticFields, ___U3CU3Ef__switchU24map7_26)); }
	inline Dictionary_2_t190145395 * get_U3CU3Ef__switchU24map7_26() const { return ___U3CU3Ef__switchU24map7_26; }
	inline Dictionary_2_t190145395 ** get_address_of_U3CU3Ef__switchU24map7_26() { return &___U3CU3Ef__switchU24map7_26; }
	inline void set_U3CU3Ef__switchU24map7_26(Dictionary_2_t190145395 * value)
	{
		___U3CU3Ef__switchU24map7_26 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__switchU24map7_26, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__switchU24map8_27() { return static_cast<int32_t>(offsetof(AnalyticsController_t2265609122_StaticFields, ___U3CU3Ef__switchU24map8_27)); }
	inline Dictionary_2_t190145395 * get_U3CU3Ef__switchU24map8_27() const { return ___U3CU3Ef__switchU24map8_27; }
	inline Dictionary_2_t190145395 ** get_address_of_U3CU3Ef__switchU24map8_27() { return &___U3CU3Ef__switchU24map8_27; }
	inline void set_U3CU3Ef__switchU24map8_27(Dictionary_2_t190145395 * value)
	{
		___U3CU3Ef__switchU24map8_27 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__switchU24map8_27, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__switchU24map9_28() { return static_cast<int32_t>(offsetof(AnalyticsController_t2265609122_StaticFields, ___U3CU3Ef__switchU24map9_28)); }
	inline Dictionary_2_t190145395 * get_U3CU3Ef__switchU24map9_28() const { return ___U3CU3Ef__switchU24map9_28; }
	inline Dictionary_2_t190145395 ** get_address_of_U3CU3Ef__switchU24map9_28() { return &___U3CU3Ef__switchU24map9_28; }
	inline void set_U3CU3Ef__switchU24map9_28(Dictionary_2_t190145395 * value)
	{
		___U3CU3Ef__switchU24map9_28 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__switchU24map9_28, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__switchU24mapA_29() { return static_cast<int32_t>(offsetof(AnalyticsController_t2265609122_StaticFields, ___U3CU3Ef__switchU24mapA_29)); }
	inline Dictionary_2_t190145395 * get_U3CU3Ef__switchU24mapA_29() const { return ___U3CU3Ef__switchU24mapA_29; }
	inline Dictionary_2_t190145395 ** get_address_of_U3CU3Ef__switchU24mapA_29() { return &___U3CU3Ef__switchU24mapA_29; }
	inline void set_U3CU3Ef__switchU24mapA_29(Dictionary_2_t190145395 * value)
	{
		___U3CU3Ef__switchU24mapA_29 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__switchU24mapA_29, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache1B_30() { return static_cast<int32_t>(offsetof(AnalyticsController_t2265609122_StaticFields, ___U3CU3Ef__amU24cache1B_30)); }
	inline Func_2_t1008118516 * get_U3CU3Ef__amU24cache1B_30() const { return ___U3CU3Ef__amU24cache1B_30; }
	inline Func_2_t1008118516 ** get_address_of_U3CU3Ef__amU24cache1B_30() { return &___U3CU3Ef__amU24cache1B_30; }
	inline void set_U3CU3Ef__amU24cache1B_30(Func_2_t1008118516 * value)
	{
		___U3CU3Ef__amU24cache1B_30 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache1B_30, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__switchU24mapB_31() { return static_cast<int32_t>(offsetof(AnalyticsController_t2265609122_StaticFields, ___U3CU3Ef__switchU24mapB_31)); }
	inline Dictionary_2_t190145395 * get_U3CU3Ef__switchU24mapB_31() const { return ___U3CU3Ef__switchU24mapB_31; }
	inline Dictionary_2_t190145395 ** get_address_of_U3CU3Ef__switchU24mapB_31() { return &___U3CU3Ef__switchU24mapB_31; }
	inline void set_U3CU3Ef__switchU24mapB_31(Dictionary_2_t190145395 * value)
	{
		___U3CU3Ef__switchU24mapB_31 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__switchU24mapB_31, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__switchU24mapC_32() { return static_cast<int32_t>(offsetof(AnalyticsController_t2265609122_StaticFields, ___U3CU3Ef__switchU24mapC_32)); }
	inline Dictionary_2_t190145395 * get_U3CU3Ef__switchU24mapC_32() const { return ___U3CU3Ef__switchU24mapC_32; }
	inline Dictionary_2_t190145395 ** get_address_of_U3CU3Ef__switchU24mapC_32() { return &___U3CU3Ef__switchU24mapC_32; }
	inline void set_U3CU3Ef__switchU24mapC_32(Dictionary_2_t190145395 * value)
	{
		___U3CU3Ef__switchU24mapC_32 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__switchU24mapC_32, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
