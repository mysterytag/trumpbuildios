﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Disposable/AnonymousDisposable
struct AnonymousDisposable_t1367871885;
// System.Action
struct Action_t437523947;

#include "codegen/il2cpp-codegen.h"
#include "System_Core_System_Action437523947.h"

// System.Void UniRx.Disposable/AnonymousDisposable::.ctor(System.Action)
extern "C"  void AnonymousDisposable__ctor_m1501829700 (AnonymousDisposable_t1367871885 * __this, Action_t437523947 * ___dispose0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.Disposable/AnonymousDisposable::Dispose()
extern "C"  void AnonymousDisposable_Dispose_m1091325398 (AnonymousDisposable_t1367871885 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
