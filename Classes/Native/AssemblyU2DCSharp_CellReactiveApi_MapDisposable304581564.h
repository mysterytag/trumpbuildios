﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Object
struct Il2CppObject;

#include "AssemblyU2DCSharp_CellUtils_SingleAssignmentDispos1832432170.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CellReactiveApi/MapDisposable
struct  MapDisposable_t304581564  : public SingleAssignmentDisposable_t1832432170
{
public:
	// System.Object CellReactiveApi/MapDisposable::last
	Il2CppObject * ___last_1;

public:
	inline static int32_t get_offset_of_last_1() { return static_cast<int32_t>(offsetof(MapDisposable_t304581564, ___last_1)); }
	inline Il2CppObject * get_last_1() const { return ___last_1; }
	inline Il2CppObject ** get_address_of_last_1() { return &___last_1; }
	inline void set_last_1(Il2CppObject * value)
	{
		___last_1 = value;
		Il2CppCodeGenWriteBarrier(&___last_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
