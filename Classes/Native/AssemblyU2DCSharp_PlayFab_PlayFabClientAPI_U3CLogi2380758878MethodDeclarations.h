﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.PlayFabClientAPI/<LoginWithFacebook>c__AnonStorey62
struct U3CLoginWithFacebookU3Ec__AnonStorey62_t2380758878;
// PlayFab.CallRequestContainer
struct CallRequestContainer_t432318609;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharpU2Dfirstpass_PlayFab_CallRequestCo432318609.h"

// System.Void PlayFab.PlayFabClientAPI/<LoginWithFacebook>c__AnonStorey62::.ctor()
extern "C"  void U3CLoginWithFacebookU3Ec__AnonStorey62__ctor_m2219185589 (U3CLoginWithFacebookU3Ec__AnonStorey62_t2380758878 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/<LoginWithFacebook>c__AnonStorey62::<>m__4F(PlayFab.CallRequestContainer)
extern "C"  void U3CLoginWithFacebookU3Ec__AnonStorey62_U3CU3Em__4F_m2641868005 (U3CLoginWithFacebookU3Ec__AnonStorey62_t2380758878 * __this, CallRequestContainer_t432318609 * ___requestContainer0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
