﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.EmptyObservable`1/Empty<UniRx.DictionaryReplaceEvent`2<System.Object,System.Object>>
struct Empty_t1442777601;
// UniRx.IObserver`1<UniRx.DictionaryReplaceEvent`2<System.Object,System.Object>>
struct IObserver_1_t1695958670;
// System.IDisposable
struct IDisposable_t1628921374;
// System.Exception
struct Exception_t1967233988;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_DictionaryRepl3778927063.h"
#include "mscorlib_System_Exception1967233988.h"

// System.Void UniRx.Operators.EmptyObservable`1/Empty<UniRx.DictionaryReplaceEvent`2<System.Object,System.Object>>::.ctor(UniRx.IObserver`1<T>,System.IDisposable)
extern "C"  void Empty__ctor_m2762808105_gshared (Empty_t1442777601 * __this, Il2CppObject* ___observer0, Il2CppObject * ___cancel1, const MethodInfo* method);
#define Empty__ctor_m2762808105(__this, ___observer0, ___cancel1, method) ((  void (*) (Empty_t1442777601 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))Empty__ctor_m2762808105_gshared)(__this, ___observer0, ___cancel1, method)
// System.Void UniRx.Operators.EmptyObservable`1/Empty<UniRx.DictionaryReplaceEvent`2<System.Object,System.Object>>::OnNext(T)
extern "C"  void Empty_OnNext_m4249852617_gshared (Empty_t1442777601 * __this, DictionaryReplaceEvent_2_t3778927063  ___value0, const MethodInfo* method);
#define Empty_OnNext_m4249852617(__this, ___value0, method) ((  void (*) (Empty_t1442777601 *, DictionaryReplaceEvent_2_t3778927063 , const MethodInfo*))Empty_OnNext_m4249852617_gshared)(__this, ___value0, method)
// System.Void UniRx.Operators.EmptyObservable`1/Empty<UniRx.DictionaryReplaceEvent`2<System.Object,System.Object>>::OnError(System.Exception)
extern "C"  void Empty_OnError_m4018078680_gshared (Empty_t1442777601 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method);
#define Empty_OnError_m4018078680(__this, ___error0, method) ((  void (*) (Empty_t1442777601 *, Exception_t1967233988 *, const MethodInfo*))Empty_OnError_m4018078680_gshared)(__this, ___error0, method)
// System.Void UniRx.Operators.EmptyObservable`1/Empty<UniRx.DictionaryReplaceEvent`2<System.Object,System.Object>>::OnCompleted()
extern "C"  void Empty_OnCompleted_m2551364395_gshared (Empty_t1442777601 * __this, const MethodInfo* method);
#define Empty_OnCompleted_m2551364395(__this, method) ((  void (*) (Empty_t1442777601 *, const MethodInfo*))Empty_OnCompleted_m2551364395_gshared)(__this, method)
