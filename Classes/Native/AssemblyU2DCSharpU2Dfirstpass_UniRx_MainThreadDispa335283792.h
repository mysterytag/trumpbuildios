﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.IEnumerator
struct IEnumerator_t287207039;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.MainThreadDispatcher/<SendStartCoroutine>c__AnonStorey7D
struct  U3CSendStartCoroutineU3Ec__AnonStorey7D_t335283792  : public Il2CppObject
{
public:
	// System.Collections.IEnumerator UniRx.MainThreadDispatcher/<SendStartCoroutine>c__AnonStorey7D::routine
	Il2CppObject * ___routine_0;

public:
	inline static int32_t get_offset_of_routine_0() { return static_cast<int32_t>(offsetof(U3CSendStartCoroutineU3Ec__AnonStorey7D_t335283792, ___routine_0)); }
	inline Il2CppObject * get_routine_0() const { return ___routine_0; }
	inline Il2CppObject ** get_address_of_routine_0() { return &___routine_0; }
	inline void set_routine_0(Il2CppObject * value)
	{
		___routine_0 = value;
		Il2CppCodeGenWriteBarrier(&___routine_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
