﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.ReactiveProperty`1<UnityEngine.Vector4>
struct ReactiveProperty_1_t4133662828;
// UniRx.IObservable`1<UnityEngine.Vector4>
struct IObservable_1_t3284128154;
// System.Collections.Generic.IEqualityComparer`1<UnityEngine.Vector4>
struct IEqualityComparer_1_t1554629145;
// System.IDisposable
struct IDisposable_t1628921374;
// UniRx.IObserver`1<UnityEngine.Vector4>
struct IObserver_1_t1442361397;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Vector43525329790.h"

// System.Void UniRx.ReactiveProperty`1<UnityEngine.Vector4>::.ctor()
extern "C"  void ReactiveProperty_1__ctor_m4209689486_gshared (ReactiveProperty_1_t4133662828 * __this, const MethodInfo* method);
#define ReactiveProperty_1__ctor_m4209689486(__this, method) ((  void (*) (ReactiveProperty_1_t4133662828 *, const MethodInfo*))ReactiveProperty_1__ctor_m4209689486_gshared)(__this, method)
// System.Void UniRx.ReactiveProperty`1<UnityEngine.Vector4>::.ctor(T)
extern "C"  void ReactiveProperty_1__ctor_m1651356560_gshared (ReactiveProperty_1_t4133662828 * __this, Vector4_t3525329790  ___initialValue0, const MethodInfo* method);
#define ReactiveProperty_1__ctor_m1651356560(__this, ___initialValue0, method) ((  void (*) (ReactiveProperty_1_t4133662828 *, Vector4_t3525329790 , const MethodInfo*))ReactiveProperty_1__ctor_m1651356560_gshared)(__this, ___initialValue0, method)
// System.Void UniRx.ReactiveProperty`1<UnityEngine.Vector4>::.ctor(UniRx.IObservable`1<T>)
extern "C"  void ReactiveProperty_1__ctor_m1863609069_gshared (ReactiveProperty_1_t4133662828 * __this, Il2CppObject* ___source0, const MethodInfo* method);
#define ReactiveProperty_1__ctor_m1863609069(__this, ___source0, method) ((  void (*) (ReactiveProperty_1_t4133662828 *, Il2CppObject*, const MethodInfo*))ReactiveProperty_1__ctor_m1863609069_gshared)(__this, ___source0, method)
// System.Void UniRx.ReactiveProperty`1<UnityEngine.Vector4>::.ctor(UniRx.IObservable`1<T>,T)
extern "C"  void ReactiveProperty_1__ctor_m4221925701_gshared (ReactiveProperty_1_t4133662828 * __this, Il2CppObject* ___source0, Vector4_t3525329790  ___initialValue1, const MethodInfo* method);
#define ReactiveProperty_1__ctor_m4221925701(__this, ___source0, ___initialValue1, method) ((  void (*) (ReactiveProperty_1_t4133662828 *, Il2CppObject*, Vector4_t3525329790 , const MethodInfo*))ReactiveProperty_1__ctor_m4221925701_gshared)(__this, ___source0, ___initialValue1, method)
// System.Void UniRx.ReactiveProperty`1<UnityEngine.Vector4>::.cctor()
extern "C"  void ReactiveProperty_1__cctor_m1169258975_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define ReactiveProperty_1__cctor_m1169258975(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))ReactiveProperty_1__cctor_m1169258975_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.IEqualityComparer`1<T> UniRx.ReactiveProperty`1<UnityEngine.Vector4>::get_EqualityComparer()
extern "C"  Il2CppObject* ReactiveProperty_1_get_EqualityComparer_m395523783_gshared (ReactiveProperty_1_t4133662828 * __this, const MethodInfo* method);
#define ReactiveProperty_1_get_EqualityComparer_m395523783(__this, method) ((  Il2CppObject* (*) (ReactiveProperty_1_t4133662828 *, const MethodInfo*))ReactiveProperty_1_get_EqualityComparer_m395523783_gshared)(__this, method)
// T UniRx.ReactiveProperty`1<UnityEngine.Vector4>::get_Value()
extern "C"  Vector4_t3525329790  ReactiveProperty_1_get_Value_m1542181427_gshared (ReactiveProperty_1_t4133662828 * __this, const MethodInfo* method);
#define ReactiveProperty_1_get_Value_m1542181427(__this, method) ((  Vector4_t3525329790  (*) (ReactiveProperty_1_t4133662828 *, const MethodInfo*))ReactiveProperty_1_get_Value_m1542181427_gshared)(__this, method)
// System.Void UniRx.ReactiveProperty`1<UnityEngine.Vector4>::set_Value(T)
extern "C"  void ReactiveProperty_1_set_Value_m3600912350_gshared (ReactiveProperty_1_t4133662828 * __this, Vector4_t3525329790  ___value0, const MethodInfo* method);
#define ReactiveProperty_1_set_Value_m3600912350(__this, ___value0, method) ((  void (*) (ReactiveProperty_1_t4133662828 *, Vector4_t3525329790 , const MethodInfo*))ReactiveProperty_1_set_Value_m3600912350_gshared)(__this, ___value0, method)
// System.Void UniRx.ReactiveProperty`1<UnityEngine.Vector4>::SetValue(T)
extern "C"  void ReactiveProperty_1_SetValue_m2002960921_gshared (ReactiveProperty_1_t4133662828 * __this, Vector4_t3525329790  ___value0, const MethodInfo* method);
#define ReactiveProperty_1_SetValue_m2002960921(__this, ___value0, method) ((  void (*) (ReactiveProperty_1_t4133662828 *, Vector4_t3525329790 , const MethodInfo*))ReactiveProperty_1_SetValue_m2002960921_gshared)(__this, ___value0, method)
// System.Void UniRx.ReactiveProperty`1<UnityEngine.Vector4>::SetValueAndForceNotify(T)
extern "C"  void ReactiveProperty_1_SetValueAndForceNotify_m3902384156_gshared (ReactiveProperty_1_t4133662828 * __this, Vector4_t3525329790  ___value0, const MethodInfo* method);
#define ReactiveProperty_1_SetValueAndForceNotify_m3902384156(__this, ___value0, method) ((  void (*) (ReactiveProperty_1_t4133662828 *, Vector4_t3525329790 , const MethodInfo*))ReactiveProperty_1_SetValueAndForceNotify_m3902384156_gshared)(__this, ___value0, method)
// System.IDisposable UniRx.ReactiveProperty`1<UnityEngine.Vector4>::Subscribe(UniRx.IObserver`1<T>)
extern "C"  Il2CppObject * ReactiveProperty_1_Subscribe_m369416739_gshared (ReactiveProperty_1_t4133662828 * __this, Il2CppObject* ___observer0, const MethodInfo* method);
#define ReactiveProperty_1_Subscribe_m369416739(__this, ___observer0, method) ((  Il2CppObject * (*) (ReactiveProperty_1_t4133662828 *, Il2CppObject*, const MethodInfo*))ReactiveProperty_1_Subscribe_m369416739_gshared)(__this, ___observer0, method)
// System.Void UniRx.ReactiveProperty`1<UnityEngine.Vector4>::Dispose()
extern "C"  void ReactiveProperty_1_Dispose_m3845653579_gshared (ReactiveProperty_1_t4133662828 * __this, const MethodInfo* method);
#define ReactiveProperty_1_Dispose_m3845653579(__this, method) ((  void (*) (ReactiveProperty_1_t4133662828 *, const MethodInfo*))ReactiveProperty_1_Dispose_m3845653579_gshared)(__this, method)
// System.Void UniRx.ReactiveProperty`1<UnityEngine.Vector4>::Dispose(System.Boolean)
extern "C"  void ReactiveProperty_1_Dispose_m646961794_gshared (ReactiveProperty_1_t4133662828 * __this, bool ___disposing0, const MethodInfo* method);
#define ReactiveProperty_1_Dispose_m646961794(__this, ___disposing0, method) ((  void (*) (ReactiveProperty_1_t4133662828 *, bool, const MethodInfo*))ReactiveProperty_1_Dispose_m646961794_gshared)(__this, ___disposing0, method)
// System.String UniRx.ReactiveProperty`1<UnityEngine.Vector4>::ToString()
extern "C"  String_t* ReactiveProperty_1_ToString_m2969237279_gshared (ReactiveProperty_1_t4133662828 * __this, const MethodInfo* method);
#define ReactiveProperty_1_ToString_m2969237279(__this, method) ((  String_t* (*) (ReactiveProperty_1_t4133662828 *, const MethodInfo*))ReactiveProperty_1_ToString_m2969237279_gshared)(__this, method)
// System.Boolean UniRx.ReactiveProperty`1<UnityEngine.Vector4>::IsRequiredSubscribeOnCurrentThread()
extern "C"  bool ReactiveProperty_1_IsRequiredSubscribeOnCurrentThread_m3879089149_gshared (ReactiveProperty_1_t4133662828 * __this, const MethodInfo* method);
#define ReactiveProperty_1_IsRequiredSubscribeOnCurrentThread_m3879089149(__this, method) ((  bool (*) (ReactiveProperty_1_t4133662828 *, const MethodInfo*))ReactiveProperty_1_IsRequiredSubscribeOnCurrentThread_m3879089149_gshared)(__this, method)
