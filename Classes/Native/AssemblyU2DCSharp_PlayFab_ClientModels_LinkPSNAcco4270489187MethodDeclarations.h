﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.ClientModels.LinkPSNAccountRequest
struct LinkPSNAccountRequest_t4270489187;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String968488902.h"
#include "mscorlib_System_Nullable_1_gen1438485399.h"

// System.Void PlayFab.ClientModels.LinkPSNAccountRequest::.ctor()
extern "C"  void LinkPSNAccountRequest__ctor_m878120998 (LinkPSNAccountRequest_t4270489187 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PlayFab.ClientModels.LinkPSNAccountRequest::get_AuthCode()
extern "C"  String_t* LinkPSNAccountRequest_get_AuthCode_m3422249663 (LinkPSNAccountRequest_t4270489187 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.LinkPSNAccountRequest::set_AuthCode(System.String)
extern "C"  void LinkPSNAccountRequest_set_AuthCode_m1905259314 (LinkPSNAccountRequest_t4270489187 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PlayFab.ClientModels.LinkPSNAccountRequest::get_RedirectUri()
extern "C"  String_t* LinkPSNAccountRequest_get_RedirectUri_m2914223368 (LinkPSNAccountRequest_t4270489187 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.LinkPSNAccountRequest::set_RedirectUri(System.String)
extern "C"  void LinkPSNAccountRequest_set_RedirectUri_m1960804363 (LinkPSNAccountRequest_t4270489187 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Nullable`1<System.Int32> PlayFab.ClientModels.LinkPSNAccountRequest::get_IssuerId()
extern "C"  Nullable_1_t1438485399  LinkPSNAccountRequest_get_IssuerId_m833707210 (LinkPSNAccountRequest_t4270489187 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.LinkPSNAccountRequest::set_IssuerId(System.Nullable`1<System.Int32>)
extern "C"  void LinkPSNAccountRequest_set_IssuerId_m1280060511 (LinkPSNAccountRequest_t4270489187 * __this, Nullable_1_t1438485399  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
