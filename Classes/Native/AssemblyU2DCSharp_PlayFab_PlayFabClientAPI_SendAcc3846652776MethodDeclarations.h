﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.PlayFabClientAPI/SendAccountRecoveryEmailResponseCallback
struct SendAccountRecoveryEmailResponseCallback_t3846652776;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;
// PlayFab.ClientModels.SendAccountRecoveryEmailRequest
struct SendAccountRecoveryEmailRequest_t558031005;
// PlayFab.ClientModels.SendAccountRecoveryEmailResult
struct SendAccountRecoveryEmailResult_t3793926767;
// PlayFab.PlayFabError
struct PlayFabError_t750598646;
// System.IAsyncResult
struct IAsyncResult_t537683269;
// System.AsyncCallback
struct AsyncCallback_t1363551830;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_IntPtr676692020.h"
#include "mscorlib_System_String968488902.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_SendAccountR558031005.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_SendAccount3793926767.h"
#include "AssemblyU2DCSharpU2Dfirstpass_PlayFab_PlayFabError750598646.h"
#include "mscorlib_System_AsyncCallback1363551830.h"

// System.Void PlayFab.PlayFabClientAPI/SendAccountRecoveryEmailResponseCallback::.ctor(System.Object,System.IntPtr)
extern "C"  void SendAccountRecoveryEmailResponseCallback__ctor_m1183252567 (SendAccountRecoveryEmailResponseCallback_t3846652776 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/SendAccountRecoveryEmailResponseCallback::Invoke(System.String,System.Int32,PlayFab.ClientModels.SendAccountRecoveryEmailRequest,PlayFab.ClientModels.SendAccountRecoveryEmailResult,PlayFab.PlayFabError,System.Object)
extern "C"  void SendAccountRecoveryEmailResponseCallback_Invoke_m3227190564 (SendAccountRecoveryEmailResponseCallback_t3846652776 * __this, String_t* ___urlPath0, int32_t ___callId1, SendAccountRecoveryEmailRequest_t558031005 * ___request2, SendAccountRecoveryEmailResult_t3793926767 * ___result3, PlayFabError_t750598646 * ___error4, Il2CppObject * ___customData5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
extern "C" void pinvoke_delegate_wrapper_SendAccountRecoveryEmailResponseCallback_t3846652776(Il2CppObject* delegate, String_t* ___urlPath0, int32_t ___callId1, SendAccountRecoveryEmailRequest_t558031005 * ___request2, SendAccountRecoveryEmailResult_t3793926767 * ___result3, PlayFabError_t750598646 * ___error4, Il2CppObject * ___customData5);
// System.IAsyncResult PlayFab.PlayFabClientAPI/SendAccountRecoveryEmailResponseCallback::BeginInvoke(System.String,System.Int32,PlayFab.ClientModels.SendAccountRecoveryEmailRequest,PlayFab.ClientModels.SendAccountRecoveryEmailResult,PlayFab.PlayFabError,System.Object,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * SendAccountRecoveryEmailResponseCallback_BeginInvoke_m4096130385 (SendAccountRecoveryEmailResponseCallback_t3846652776 * __this, String_t* ___urlPath0, int32_t ___callId1, SendAccountRecoveryEmailRequest_t558031005 * ___request2, SendAccountRecoveryEmailResult_t3793926767 * ___result3, PlayFabError_t750598646 * ___error4, Il2CppObject * ___customData5, AsyncCallback_t1363551830 * ___callback6, Il2CppObject * ___object7, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/SendAccountRecoveryEmailResponseCallback::EndInvoke(System.IAsyncResult)
extern "C"  void SendAccountRecoveryEmailResponseCallback_EndInvoke_m1545483751 (SendAccountRecoveryEmailResponseCallback_t3846652776 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
