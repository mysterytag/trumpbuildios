﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_AnonymousCell_1_gen103592921MethodDeclarations.h"

// System.Void AnonymousCell`1<System.Collections.Generic.IEnumerable`1<System.Object>>::.ctor()
#define AnonymousCell_1__ctor_m2257448330(__this, method) ((  void (*) (AnonymousCell_1_t2975747277 *, const MethodInfo*))AnonymousCell_1__ctor_m2866128927_gshared)(__this, method)
// System.Void AnonymousCell`1<System.Collections.Generic.IEnumerable`1<System.Object>>::.ctor(System.Func`3<System.Action`1<T>,Priority,System.IDisposable>,System.Func`1<T>)
#define AnonymousCell_1__ctor_m2873554494(__this, ___subscribe0, ___current1, method) ((  void (*) (AnonymousCell_1_t2975747277 *, Func_3_t3368438772 *, Func_1_t557074727 *, const MethodInfo*))AnonymousCell_1__ctor_m1853677587_gshared)(__this, ___subscribe0, ___current1, method)
// T AnonymousCell`1<System.Collections.Generic.IEnumerable`1<System.Object>>::get_value()
#define AnonymousCell_1_get_value_m2670180977(__this, method) ((  Il2CppObject* (*) (AnonymousCell_1_t2975747277 *, const MethodInfo*))AnonymousCell_1_get_value_m2881526726_gshared)(__this, method)
// System.IDisposable AnonymousCell`1<System.Collections.Generic.IEnumerable`1<System.Object>>::ListenUpdates(System.Action`1<T>,Priority)
#define AnonymousCell_1_ListenUpdates_m2305132170(__this, ___reaction0, ___p1, method) ((  Il2CppObject * (*) (AnonymousCell_1_t2975747277 *, Action_1_t3857713481 *, int32_t, const MethodInfo*))AnonymousCell_1_ListenUpdates_m2243511189_gshared)(__this, ___reaction0, ___p1, method)
// System.IDisposable AnonymousCell`1<System.Collections.Generic.IEnumerable`1<System.Object>>::Bind(System.Action`1<T>,Priority)
#define AnonymousCell_1_Bind_m3332154488(__this, ___reaction0, ___p1, method) ((  Il2CppObject * (*) (AnonymousCell_1_t2975747277 *, Action_1_t3857713481 *, int32_t, const MethodInfo*))AnonymousCell_1_Bind_m1087176461_gshared)(__this, ___reaction0, ___p1, method)
// System.IDisposable AnonymousCell`1<System.Collections.Generic.IEnumerable`1<System.Object>>::Subscribe(UniRx.IObserver`1<T>)
#define AnonymousCell_1_Subscribe_m1631767345(__this, ___observer0, method) ((  Il2CppObject * (*) (AnonymousCell_1_t2975747277 *, Il2CppObject*, const MethodInfo*))AnonymousCell_1_Subscribe_m1803732742_gshared)(__this, ___observer0, method)
// System.IDisposable AnonymousCell`1<System.Collections.Generic.IEnumerable`1<System.Object>>::Listen(System.Action`1<T>,Priority)
#define AnonymousCell_1_Listen_m3337749038(__this, ___reaction0, ___priority1, method) ((  Il2CppObject * (*) (AnonymousCell_1_t2975747277 *, Action_1_t3857713481 *, int32_t, const MethodInfo*))AnonymousCell_1_Listen_m1987447683_gshared)(__this, ___reaction0, ___priority1, method)
// System.IDisposable AnonymousCell`1<System.Collections.Generic.IEnumerable`1<System.Object>>::Listen(System.Action,Priority)
#define AnonymousCell_1_Listen_m3366312505(__this, ___action0, ___priority1, method) ((  Il2CppObject * (*) (AnonymousCell_1_t2975747277 *, Action_t437523947 *, int32_t, const MethodInfo*))AnonymousCell_1_Listen_m3510407108_gshared)(__this, ___action0, ___priority1, method)
// System.IDisposable AnonymousCell`1<System.Collections.Generic.IEnumerable`1<System.Object>>::OnChanged(System.Action,Priority)
#define AnonymousCell_1_OnChanged_m3901056815(__this, ___action0, ___p1, method) ((  Il2CppObject * (*) (AnonymousCell_1_t2975747277 *, Action_t437523947 *, int32_t, const MethodInfo*))AnonymousCell_1_OnChanged_m1656078788_gshared)(__this, ___action0, ___p1, method)
// System.Object AnonymousCell`1<System.Collections.Generic.IEnumerable`1<System.Object>>::get_valueObject()
#define AnonymousCell_1_get_valueObject_m2811692826(__this, method) ((  Il2CppObject * (*) (AnonymousCell_1_t2975747277 *, const MethodInfo*))AnonymousCell_1_get_valueObject_m701915439_gshared)(__this, method)
