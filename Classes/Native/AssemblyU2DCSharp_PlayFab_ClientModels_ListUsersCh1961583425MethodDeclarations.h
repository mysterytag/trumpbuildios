﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.ClientModels.ListUsersCharactersResult
struct ListUsersCharactersResult_t1961583425;
// System.Collections.Generic.List`1<PlayFab.ClientModels.CharacterResult>
struct List_1_t1071583631;

#include "codegen/il2cpp-codegen.h"

// System.Void PlayFab.ClientModels.ListUsersCharactersResult::.ctor()
extern "C"  void ListUsersCharactersResult__ctor_m2336393736 (ListUsersCharactersResult_t1961583425 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<PlayFab.ClientModels.CharacterResult> PlayFab.ClientModels.ListUsersCharactersResult::get_Characters()
extern "C"  List_1_t1071583631 * ListUsersCharactersResult_get_Characters_m2847187146 (ListUsersCharactersResult_t1961583425 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.ListUsersCharactersResult::set_Characters(System.Collections.Generic.List`1<PlayFab.ClientModels.CharacterResult>)
extern "C"  void ListUsersCharactersResult_set_Characters_m2919384823 (ListUsersCharactersResult_t1961583425 * __this, List_1_t1071583631 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
