﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SafeScreenController/<DisappearingCoroutine>c__Iterator1B
struct U3CDisappearingCoroutineU3Ec__Iterator1B_t2803193377;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void SafeScreenController/<DisappearingCoroutine>c__Iterator1B::.ctor()
extern "C"  void U3CDisappearingCoroutineU3Ec__Iterator1B__ctor_m1953670864 (U3CDisappearingCoroutineU3Ec__Iterator1B_t2803193377 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object SafeScreenController/<DisappearingCoroutine>c__Iterator1B::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CDisappearingCoroutineU3Ec__Iterator1B_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1719676610 (U3CDisappearingCoroutineU3Ec__Iterator1B_t2803193377 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object SafeScreenController/<DisappearingCoroutine>c__Iterator1B::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CDisappearingCoroutineU3Ec__Iterator1B_System_Collections_IEnumerator_get_Current_m18609750 (U3CDisappearingCoroutineU3Ec__Iterator1B_t2803193377 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean SafeScreenController/<DisappearingCoroutine>c__Iterator1B::MoveNext()
extern "C"  bool U3CDisappearingCoroutineU3Ec__Iterator1B_MoveNext_m4053376548 (U3CDisappearingCoroutineU3Ec__Iterator1B_t2803193377 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SafeScreenController/<DisappearingCoroutine>c__Iterator1B::Dispose()
extern "C"  void U3CDisappearingCoroutineU3Ec__Iterator1B_Dispose_m475275021 (U3CDisappearingCoroutineU3Ec__Iterator1B_t2803193377 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SafeScreenController/<DisappearingCoroutine>c__Iterator1B::Reset()
extern "C"  void U3CDisappearingCoroutineU3Ec__Iterator1B_Reset_m3895071101 (U3CDisappearingCoroutineU3Ec__Iterator1B_t2803193377 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
