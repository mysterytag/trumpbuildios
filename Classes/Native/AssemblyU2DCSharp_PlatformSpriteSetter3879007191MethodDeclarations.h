﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlatformSpriteSetter
struct PlatformSpriteSetter_t3879007191;

#include "codegen/il2cpp-codegen.h"

// System.Void PlatformSpriteSetter::.ctor()
extern "C"  void PlatformSpriteSetter__ctor_m3934439588 (PlatformSpriteSetter_t3879007191 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlatformSpriteSetter::Awake()
extern "C"  void PlatformSpriteSetter_Awake_m4172044807 (PlatformSpriteSetter_t3879007191 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
