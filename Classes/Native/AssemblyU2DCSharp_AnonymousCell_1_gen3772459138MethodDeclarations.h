﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// AnonymousCell`1<System.Boolean>
struct AnonymousCell_1_t3772459138;
// System.Func`3<System.Action`1<System.Boolean>,Priority,System.IDisposable>
struct Func_3_t3884450017;
// System.Func`1<System.Boolean>
struct Func_1_t1353786588;
// System.IDisposable
struct IDisposable_t1628921374;
// System.Action`1<System.Boolean>
struct Action_1_t359458046;
// UniRx.IObserver`1<System.Boolean>
struct IObserver_1_t2423004244;
// System.Action
struct Action_t437523947;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Priority3194150340.h"
#include "System_Core_System_Action437523947.h"

// System.Void AnonymousCell`1<System.Boolean>::.ctor()
extern "C"  void AnonymousCell_1__ctor_m3473318386_gshared (AnonymousCell_1_t3772459138 * __this, const MethodInfo* method);
#define AnonymousCell_1__ctor_m3473318386(__this, method) ((  void (*) (AnonymousCell_1_t3772459138 *, const MethodInfo*))AnonymousCell_1__ctor_m3473318386_gshared)(__this, method)
// System.Void AnonymousCell`1<System.Boolean>::.ctor(System.Func`3<System.Action`1<T>,Priority,System.IDisposable>,System.Func`1<T>)
extern "C"  void AnonymousCell_1__ctor_m2842727590_gshared (AnonymousCell_1_t3772459138 * __this, Func_3_t3884450017 * ___subscribe0, Func_1_t1353786588 * ___current1, const MethodInfo* method);
#define AnonymousCell_1__ctor_m2842727590(__this, ___subscribe0, ___current1, method) ((  void (*) (AnonymousCell_1_t3772459138 *, Func_3_t3884450017 *, Func_1_t1353786588 *, const MethodInfo*))AnonymousCell_1__ctor_m2842727590_gshared)(__this, ___subscribe0, ___current1, method)
// T AnonymousCell`1<System.Boolean>::get_value()
extern "C"  bool AnonymousCell_1_get_value_m729637367_gshared (AnonymousCell_1_t3772459138 * __this, const MethodInfo* method);
#define AnonymousCell_1_get_value_m729637367(__this, method) ((  bool (*) (AnonymousCell_1_t3772459138 *, const MethodInfo*))AnonymousCell_1_get_value_m729637367_gshared)(__this, method)
// System.IDisposable AnonymousCell`1<System.Boolean>::ListenUpdates(System.Action`1<T>,Priority)
extern "C"  Il2CppObject * AnonymousCell_1_ListenUpdates_m3927268892_gshared (AnonymousCell_1_t3772459138 * __this, Action_1_t359458046 * ___reaction0, int32_t ___p1, const MethodInfo* method);
#define AnonymousCell_1_ListenUpdates_m3927268892(__this, ___reaction0, ___p1, method) ((  Il2CppObject * (*) (AnonymousCell_1_t3772459138 *, Action_1_t359458046 *, int32_t, const MethodInfo*))AnonymousCell_1_ListenUpdates_m3927268892_gshared)(__this, ___reaction0, ___p1, method)
// System.IDisposable AnonymousCell`1<System.Boolean>::Bind(System.Action`1<T>,Priority)
extern "C"  Il2CppObject * AnonymousCell_1_Bind_m1394253990_gshared (AnonymousCell_1_t3772459138 * __this, Action_1_t359458046 * ___reaction0, int32_t ___p1, const MethodInfo* method);
#define AnonymousCell_1_Bind_m1394253990(__this, ___reaction0, ___p1, method) ((  Il2CppObject * (*) (AnonymousCell_1_t3772459138 *, Action_1_t359458046 *, int32_t, const MethodInfo*))AnonymousCell_1_Bind_m1394253990_gshared)(__this, ___reaction0, ___p1, method)
// System.IDisposable AnonymousCell`1<System.Boolean>::Subscribe(UniRx.IObserver`1<T>)
extern "C"  Il2CppObject * AnonymousCell_1_Subscribe_m1674443487_gshared (AnonymousCell_1_t3772459138 * __this, Il2CppObject* ___observer0, const MethodInfo* method);
#define AnonymousCell_1_Subscribe_m1674443487(__this, ___observer0, method) ((  Il2CppObject * (*) (AnonymousCell_1_t3772459138 *, Il2CppObject*, const MethodInfo*))AnonymousCell_1_Subscribe_m1674443487_gshared)(__this, ___observer0, method)
// System.IDisposable AnonymousCell`1<System.Boolean>::Listen(System.Action`1<T>,Priority)
extern "C"  Il2CppObject * AnonymousCell_1_Listen_m736209628_gshared (AnonymousCell_1_t3772459138 * __this, Action_1_t359458046 * ___reaction0, int32_t ___priority1, const MethodInfo* method);
#define AnonymousCell_1_Listen_m736209628(__this, ___reaction0, ___priority1, method) ((  Il2CppObject * (*) (AnonymousCell_1_t3772459138 *, Action_1_t359458046 *, int32_t, const MethodInfo*))AnonymousCell_1_Listen_m736209628_gshared)(__this, ___reaction0, ___priority1, method)
// System.IDisposable AnonymousCell`1<System.Boolean>::Listen(System.Action,Priority)
extern "C"  Il2CppObject * AnonymousCell_1_Listen_m3644783819_gshared (AnonymousCell_1_t3772459138 * __this, Action_t437523947 * ___action0, int32_t ___priority1, const MethodInfo* method);
#define AnonymousCell_1_Listen_m3644783819(__this, ___action0, ___priority1, method) ((  Il2CppObject * (*) (AnonymousCell_1_t3772459138 *, Action_t437523947 *, int32_t, const MethodInfo*))AnonymousCell_1_Listen_m3644783819_gshared)(__this, ___action0, ___priority1, method)
// System.IDisposable AnonymousCell`1<System.Boolean>::OnChanged(System.Action,Priority)
extern "C"  Il2CppObject * AnonymousCell_1_OnChanged_m1963156317_gshared (AnonymousCell_1_t3772459138 * __this, Action_t437523947 * ___action0, int32_t ___p1, const MethodInfo* method);
#define AnonymousCell_1_OnChanged_m1963156317(__this, ___action0, ___p1, method) ((  Il2CppObject * (*) (AnonymousCell_1_t3772459138 *, Action_t437523947 *, int32_t, const MethodInfo*))AnonymousCell_1_OnChanged_m1963156317_gshared)(__this, ___action0, ___p1, method)
// System.Object AnonymousCell`1<System.Boolean>::get_valueObject()
extern "C"  Il2CppObject * AnonymousCell_1_get_valueObject_m3942661324_gshared (AnonymousCell_1_t3772459138 * __this, const MethodInfo* method);
#define AnonymousCell_1_get_valueObject_m3942661324(__this, method) ((  Il2CppObject * (*) (AnonymousCell_1_t3772459138 *, const MethodInfo*))AnonymousCell_1_get_valueObject_m3942661324_gshared)(__this, method)
