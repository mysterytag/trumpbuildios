﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.AsUnitObservableObservable`1/AsUnitObservable<System.Object>
struct AsUnitObservable_t2008788970;
// UniRx.IObserver`1<UniRx.Unit>
struct IObserver_1_t475317645;
// System.IDisposable
struct IDisposable_t1628921374;
// System.Object
struct Il2CppObject;
// System.Exception
struct Exception_t1967233988;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_Exception1967233988.h"

// System.Void UniRx.Operators.AsUnitObservableObservable`1/AsUnitObservable<System.Object>::.ctor(UniRx.IObserver`1<UniRx.Unit>,System.IDisposable)
extern "C"  void AsUnitObservable__ctor_m841429375_gshared (AsUnitObservable_t2008788970 * __this, Il2CppObject* ___observer0, Il2CppObject * ___cancel1, const MethodInfo* method);
#define AsUnitObservable__ctor_m841429375(__this, ___observer0, ___cancel1, method) ((  void (*) (AsUnitObservable_t2008788970 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))AsUnitObservable__ctor_m841429375_gshared)(__this, ___observer0, ___cancel1, method)
// System.Void UniRx.Operators.AsUnitObservableObservable`1/AsUnitObservable<System.Object>::OnNext(T)
extern "C"  void AsUnitObservable_OnNext_m2456150271_gshared (AsUnitObservable_t2008788970 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define AsUnitObservable_OnNext_m2456150271(__this, ___value0, method) ((  void (*) (AsUnitObservable_t2008788970 *, Il2CppObject *, const MethodInfo*))AsUnitObservable_OnNext_m2456150271_gshared)(__this, ___value0, method)
// System.Void UniRx.Operators.AsUnitObservableObservable`1/AsUnitObservable<System.Object>::OnError(System.Exception)
extern "C"  void AsUnitObservable_OnError_m253632014_gshared (AsUnitObservable_t2008788970 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method);
#define AsUnitObservable_OnError_m253632014(__this, ___error0, method) ((  void (*) (AsUnitObservable_t2008788970 *, Exception_t1967233988 *, const MethodInfo*))AsUnitObservable_OnError_m253632014_gshared)(__this, ___error0, method)
// System.Void UniRx.Operators.AsUnitObservableObservable`1/AsUnitObservable<System.Object>::OnCompleted()
extern "C"  void AsUnitObservable_OnCompleted_m2408511073_gshared (AsUnitObservable_t2008788970 * __this, const MethodInfo* method);
#define AsUnitObservable_OnCompleted_m2408511073(__this, method) ((  void (*) (AsUnitObservable_t2008788970 *, const MethodInfo*))AsUnitObservable_OnCompleted_m2408511073_gshared)(__this, method)
