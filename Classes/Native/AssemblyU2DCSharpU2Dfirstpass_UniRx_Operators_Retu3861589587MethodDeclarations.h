﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.ReturnObservable`1/<SubscribeCore>c__AnonStorey68<UniRx.Unit>
struct U3CSubscribeCoreU3Ec__AnonStorey68_t3861589587;

#include "codegen/il2cpp-codegen.h"

// System.Void UniRx.Operators.ReturnObservable`1/<SubscribeCore>c__AnonStorey68<UniRx.Unit>::.ctor()
extern "C"  void U3CSubscribeCoreU3Ec__AnonStorey68__ctor_m3408801829_gshared (U3CSubscribeCoreU3Ec__AnonStorey68_t3861589587 * __this, const MethodInfo* method);
#define U3CSubscribeCoreU3Ec__AnonStorey68__ctor_m3408801829(__this, method) ((  void (*) (U3CSubscribeCoreU3Ec__AnonStorey68_t3861589587 *, const MethodInfo*))U3CSubscribeCoreU3Ec__AnonStorey68__ctor_m3408801829_gshared)(__this, method)
// System.Void UniRx.Operators.ReturnObservable`1/<SubscribeCore>c__AnonStorey68<UniRx.Unit>::<>m__86()
extern "C"  void U3CSubscribeCoreU3Ec__AnonStorey68_U3CU3Em__86_m1106765100_gshared (U3CSubscribeCoreU3Ec__AnonStorey68_t3861589587 * __this, const MethodInfo* method);
#define U3CSubscribeCoreU3Ec__AnonStorey68_U3CU3Em__86_m1106765100(__this, method) ((  void (*) (U3CSubscribeCoreU3Ec__AnonStorey68_t3861589587 *, const MethodInfo*))U3CSubscribeCoreU3Ec__AnonStorey68_U3CU3Em__86_m1106765100_gshared)(__this, method)
