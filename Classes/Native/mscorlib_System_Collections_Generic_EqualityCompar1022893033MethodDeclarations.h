﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.EqualityComparer`1/DefaultComparer<PlayFab.Internal.GMFB_327/testRegion>
struct DefaultComparer_t1022893033;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_PlayFab_Internal_GMFB_327_testRe1684939302.h"

// System.Void System.Collections.Generic.EqualityComparer`1/DefaultComparer<PlayFab.Internal.GMFB_327/testRegion>::.ctor()
extern "C"  void DefaultComparer__ctor_m2987852400_gshared (DefaultComparer_t1022893033 * __this, const MethodInfo* method);
#define DefaultComparer__ctor_m2987852400(__this, method) ((  void (*) (DefaultComparer_t1022893033 *, const MethodInfo*))DefaultComparer__ctor_m2987852400_gshared)(__this, method)
// System.Int32 System.Collections.Generic.EqualityComparer`1/DefaultComparer<PlayFab.Internal.GMFB_327/testRegion>::GetHashCode(T)
extern "C"  int32_t DefaultComparer_GetHashCode_m3503340675_gshared (DefaultComparer_t1022893033 * __this, int32_t ___obj0, const MethodInfo* method);
#define DefaultComparer_GetHashCode_m3503340675(__this, ___obj0, method) ((  int32_t (*) (DefaultComparer_t1022893033 *, int32_t, const MethodInfo*))DefaultComparer_GetHashCode_m3503340675_gshared)(__this, ___obj0, method)
// System.Boolean System.Collections.Generic.EqualityComparer`1/DefaultComparer<PlayFab.Internal.GMFB_327/testRegion>::Equals(T,T)
extern "C"  bool DefaultComparer_Equals_m753394053_gshared (DefaultComparer_t1022893033 * __this, int32_t ___x0, int32_t ___y1, const MethodInfo* method);
#define DefaultComparer_Equals_m753394053(__this, ___x0, ___y1, method) ((  bool (*) (DefaultComparer_t1022893033 *, int32_t, int32_t, const MethodInfo*))DefaultComparer_Equals_m753394053_gshared)(__this, ___x0, ___y1, method)
