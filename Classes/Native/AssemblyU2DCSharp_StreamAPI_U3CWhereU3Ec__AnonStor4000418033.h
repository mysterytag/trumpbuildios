﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Action
struct Action_t437523947;
// StreamAPI/<Where>c__AnonStorey12E
struct U3CWhereU3Ec__AnonStorey12E_t4000418032;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// StreamAPI/<Where>c__AnonStorey12E/<Where>c__AnonStorey12F
struct  U3CWhereU3Ec__AnonStorey12F_t4000418033  : public Il2CppObject
{
public:
	// System.Action StreamAPI/<Where>c__AnonStorey12E/<Where>c__AnonStorey12F::reaction
	Action_t437523947 * ___reaction_0;
	// StreamAPI/<Where>c__AnonStorey12E StreamAPI/<Where>c__AnonStorey12E/<Where>c__AnonStorey12F::<>f__ref$302
	U3CWhereU3Ec__AnonStorey12E_t4000418032 * ___U3CU3Ef__refU24302_1;

public:
	inline static int32_t get_offset_of_reaction_0() { return static_cast<int32_t>(offsetof(U3CWhereU3Ec__AnonStorey12F_t4000418033, ___reaction_0)); }
	inline Action_t437523947 * get_reaction_0() const { return ___reaction_0; }
	inline Action_t437523947 ** get_address_of_reaction_0() { return &___reaction_0; }
	inline void set_reaction_0(Action_t437523947 * value)
	{
		___reaction_0 = value;
		Il2CppCodeGenWriteBarrier(&___reaction_0, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__refU24302_1() { return static_cast<int32_t>(offsetof(U3CWhereU3Ec__AnonStorey12F_t4000418033, ___U3CU3Ef__refU24302_1)); }
	inline U3CWhereU3Ec__AnonStorey12E_t4000418032 * get_U3CU3Ef__refU24302_1() const { return ___U3CU3Ef__refU24302_1; }
	inline U3CWhereU3Ec__AnonStorey12E_t4000418032 ** get_address_of_U3CU3Ef__refU24302_1() { return &___U3CU3Ef__refU24302_1; }
	inline void set_U3CU3Ef__refU24302_1(U3CWhereU3Ec__AnonStorey12E_t4000418032 * value)
	{
		___U3CU3Ef__refU24302_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__refU24302_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
