﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.LazyTask
struct LazyTask_t1365889643;
// UnityEngine.Coroutine
struct Coroutine_t2246592261;
struct Coroutine_t2246592261_marshaled_pinvoke;
// UniRx.LazyTask[]
struct LazyTaskU5BU5D_t3442259850;
// System.Collections.Generic.IEnumerable`1<UniRx.LazyTask>
struct IEnumerable_1_t4238043999;
// System.Collections.IEnumerator
struct IEnumerator_t287207039;
// UnityEngine.Coroutine[]
struct CoroutineU5BU5D_t1904733000;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_LazyTask_TaskS2963002871.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_LazyTask1365889643.h"

// System.Void UniRx.LazyTask::.ctor()
extern "C"  void LazyTask__ctor_m1095466110 (LazyTask_t1365889643 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UniRx.LazyTask/TaskStatus UniRx.LazyTask::get_Status()
extern "C"  int32_t LazyTask_get_Status_m3296471199 (LazyTask_t1365889643 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.LazyTask::set_Status(UniRx.LazyTask/TaskStatus)
extern "C"  void LazyTask_set_Status_m280253608 (LazyTask_t1365889643 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.LazyTask::Cancel()
extern "C"  void LazyTask_Cancel_m4175770560 (LazyTask_t1365889643 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Coroutine UniRx.LazyTask::WhenAll(UniRx.LazyTask[])
extern "C"  Coroutine_t2246592261 * LazyTask_WhenAll_m1432274494 (Il2CppObject * __this /* static, unused */, LazyTaskU5BU5D_t3442259850* ___tasks0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Coroutine UniRx.LazyTask::WhenAll(System.Collections.Generic.IEnumerable`1<UniRx.LazyTask>)
extern "C"  Coroutine_t2246592261 * LazyTask_WhenAll_m935861197 (Il2CppObject * __this /* static, unused */, Il2CppObject* ___tasks0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator UniRx.LazyTask::WhenAllCore(UnityEngine.Coroutine[])
extern "C"  Il2CppObject * LazyTask_WhenAllCore_m3243040509 (Il2CppObject * __this /* static, unused */, CoroutineU5BU5D_t1904733000* ___coroutines0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Coroutine UniRx.LazyTask::<WhenAll>m__A5(UniRx.LazyTask)
extern "C"  Coroutine_t2246592261 * LazyTask_U3CWhenAllU3Em__A5_m3212194645 (Il2CppObject * __this /* static, unused */, LazyTask_t1365889643 * ___x0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
