﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.RepeatObservable`1/<SubscribeCore>c__AnonStorey67<System.Object>
struct U3CSubscribeCoreU3Ec__AnonStorey67_t473835800;
// System.Action
struct Action_t437523947;

#include "codegen/il2cpp-codegen.h"
#include "System_Core_System_Action437523947.h"

// System.Void UniRx.Operators.RepeatObservable`1/<SubscribeCore>c__AnonStorey67<System.Object>::.ctor()
extern "C"  void U3CSubscribeCoreU3Ec__AnonStorey67__ctor_m3459483859_gshared (U3CSubscribeCoreU3Ec__AnonStorey67_t473835800 * __this, const MethodInfo* method);
#define U3CSubscribeCoreU3Ec__AnonStorey67__ctor_m3459483859(__this, method) ((  void (*) (U3CSubscribeCoreU3Ec__AnonStorey67_t473835800 *, const MethodInfo*))U3CSubscribeCoreU3Ec__AnonStorey67__ctor_m3459483859_gshared)(__this, method)
// System.Void UniRx.Operators.RepeatObservable`1/<SubscribeCore>c__AnonStorey67<System.Object>::<>m__84(System.Action)
extern "C"  void U3CSubscribeCoreU3Ec__AnonStorey67_U3CU3Em__84_m3301892517_gshared (U3CSubscribeCoreU3Ec__AnonStorey67_t473835800 * __this, Action_t437523947 * ___self0, const MethodInfo* method);
#define U3CSubscribeCoreU3Ec__AnonStorey67_U3CU3Em__84_m3301892517(__this, ___self0, method) ((  void (*) (U3CSubscribeCoreU3Ec__AnonStorey67_t473835800 *, Action_t437523947 *, const MethodInfo*))U3CSubscribeCoreU3Ec__AnonStorey67_U3CU3Em__84_m3301892517_gshared)(__this, ___self0, method)
