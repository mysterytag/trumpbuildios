﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E3591453091MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,AchievementDescription>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define Enumerator__ctor_m331278328(__this, ___dictionary0, method) ((  void (*) (Enumerator_t3728060354 *, Dictionary_2_t3961032413 *, const MethodInfo*))Enumerator__ctor_m3920831137_gshared)(__this, ___dictionary0, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.String,AchievementDescription>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m3187027635(__this, method) ((  Il2CppObject * (*) (Enumerator_t3728060354 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m3262087712_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,AchievementDescription>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m4091190717(__this, method) ((  void (*) (Enumerator_t3728060354 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m2959141748_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<System.String,AchievementDescription>::System.Collections.IDictionaryEnumerator.get_Entry()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m1852115188(__this, method) ((  DictionaryEntry_t130027246  (*) (Enumerator_t3728060354 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m2279524093_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.String,AchievementDescription>::System.Collections.IDictionaryEnumerator.get_Key()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m380000591(__this, method) ((  Il2CppObject * (*) (Enumerator_t3728060354 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m1201448700_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.String,AchievementDescription>::System.Collections.IDictionaryEnumerator.get_Value()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m1156784161(__this, method) ((  Il2CppObject * (*) (Enumerator_t3728060354 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m294434446_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.String,AchievementDescription>::MoveNext()
#define Enumerator_MoveNext_m2712471149(__this, method) ((  bool (*) (Enumerator_t3728060354 *, const MethodInfo*))Enumerator_MoveNext_m217327200_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<System.String,AchievementDescription>::get_Current()
#define Enumerator_get_Current_m1979721647(__this, method) ((  KeyValuePair_2_t3449563711  (*) (Enumerator_t3728060354 *, const MethodInfo*))Enumerator_get_Current_m4240003024_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<System.String,AchievementDescription>::get_CurrentKey()
#define Enumerator_get_CurrentKey_m2044342(__this, method) ((  String_t* (*) (Enumerator_t3728060354 *, const MethodInfo*))Enumerator_get_CurrentKey_m3062159917_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.String,AchievementDescription>::get_CurrentValue()
#define Enumerator_get_CurrentValue_m3717196022(__this, method) ((  AchievementDescription_t2323334509 * (*) (Enumerator_t3728060354 *, const MethodInfo*))Enumerator_get_CurrentValue_m592783249_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,AchievementDescription>::Reset()
#define Enumerator_Reset_m3250261578(__this, method) ((  void (*) (Enumerator_t3728060354 *, const MethodInfo*))Enumerator_Reset_m3001375603_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,AchievementDescription>::VerifyState()
#define Enumerator_VerifyState_m1566113491(__this, method) ((  void (*) (Enumerator_t3728060354 *, const MethodInfo*))Enumerator_VerifyState_m4290054460_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,AchievementDescription>::VerifyCurrent()
#define Enumerator_VerifyCurrent_m246415739(__this, method) ((  void (*) (Enumerator_t3728060354 *, const MethodInfo*))Enumerator_VerifyCurrent_m2318603684_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,AchievementDescription>::Dispose()
#define Enumerator_Dispose_m3583581338(__this, method) ((  void (*) (Enumerator_t3728060354 *, const MethodInfo*))Enumerator_Dispose_m627360643_gshared)(__this, method)
