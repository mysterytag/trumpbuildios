﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Func`5<System.Object,System.Int32,UniRx.Unit,System.Int32,System.Object>
struct Func_5_t2200276091;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t537683269;
// System.AsyncCallback
struct AsyncCallback_t1363551830;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_IntPtr676692020.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Unit2558286038.h"
#include "mscorlib_System_AsyncCallback1363551830.h"

// System.Void System.Func`5<System.Object,System.Int32,UniRx.Unit,System.Int32,System.Object>::.ctor(System.Object,System.IntPtr)
extern "C"  void Func_5__ctor_m3352026815_gshared (Func_5_t2200276091 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method);
#define Func_5__ctor_m3352026815(__this, ___object0, ___method1, method) ((  void (*) (Func_5_t2200276091 *, Il2CppObject *, IntPtr_t, const MethodInfo*))Func_5__ctor_m3352026815_gshared)(__this, ___object0, ___method1, method)
// TResult System.Func`5<System.Object,System.Int32,UniRx.Unit,System.Int32,System.Object>::Invoke(T1,T2,T3,T4)
extern "C"  Il2CppObject * Func_5_Invoke_m2934399597_gshared (Func_5_t2200276091 * __this, Il2CppObject * ___arg10, int32_t ___arg21, Unit_t2558286038  ___arg32, int32_t ___arg43, const MethodInfo* method);
#define Func_5_Invoke_m2934399597(__this, ___arg10, ___arg21, ___arg32, ___arg43, method) ((  Il2CppObject * (*) (Func_5_t2200276091 *, Il2CppObject *, int32_t, Unit_t2558286038 , int32_t, const MethodInfo*))Func_5_Invoke_m2934399597_gshared)(__this, ___arg10, ___arg21, ___arg32, ___arg43, method)
// System.IAsyncResult System.Func`5<System.Object,System.Int32,UniRx.Unit,System.Int32,System.Object>::BeginInvoke(T1,T2,T3,T4,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * Func_5_BeginInvoke_m2075150804_gshared (Func_5_t2200276091 * __this, Il2CppObject * ___arg10, int32_t ___arg21, Unit_t2558286038  ___arg32, int32_t ___arg43, AsyncCallback_t1363551830 * ___callback4, Il2CppObject * ___object5, const MethodInfo* method);
#define Func_5_BeginInvoke_m2075150804(__this, ___arg10, ___arg21, ___arg32, ___arg43, ___callback4, ___object5, method) ((  Il2CppObject * (*) (Func_5_t2200276091 *, Il2CppObject *, int32_t, Unit_t2558286038 , int32_t, AsyncCallback_t1363551830 *, Il2CppObject *, const MethodInfo*))Func_5_BeginInvoke_m2075150804_gshared)(__this, ___arg10, ___arg21, ___arg32, ___arg43, ___callback4, ___object5, method)
// TResult System.Func`5<System.Object,System.Int32,UniRx.Unit,System.Int32,System.Object>::EndInvoke(System.IAsyncResult)
extern "C"  Il2CppObject * Func_5_EndInvoke_m1496937069_gshared (Func_5_t2200276091 * __this, Il2CppObject * ___result0, const MethodInfo* method);
#define Func_5_EndInvoke_m1496937069(__this, ___result0, method) ((  Il2CppObject * (*) (Func_5_t2200276091 *, Il2CppObject *, const MethodInfo*))Func_5_EndInvoke_m1496937069_gshared)(__this, ___result0, method)
