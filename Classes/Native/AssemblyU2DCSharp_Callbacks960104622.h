﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UniRx.Subject`1<UniRx.Tuple`2<System.String,System.String>>
struct Subject_1_t1089058095;

#include "UnityEngine_UnityEngine_MonoBehaviour3012272455.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Callbacks
struct  Callbacks_t960104622  : public MonoBehaviour_t3012272455
{
public:
	// UniRx.Subject`1<UniRx.Tuple`2<System.String,System.String>> Callbacks::VkEvents
	Subject_1_t1089058095 * ___VkEvents_2;

public:
	inline static int32_t get_offset_of_VkEvents_2() { return static_cast<int32_t>(offsetof(Callbacks_t960104622, ___VkEvents_2)); }
	inline Subject_1_t1089058095 * get_VkEvents_2() const { return ___VkEvents_2; }
	inline Subject_1_t1089058095 ** get_address_of_VkEvents_2() { return &___VkEvents_2; }
	inline void set_VkEvents_2(Subject_1_t1089058095 * value)
	{
		___VkEvents_2 = value;
		Il2CppCodeGenWriteBarrier(&___VkEvents_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
