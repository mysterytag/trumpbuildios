﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.ObservableWWW/<Post>c__AnonStorey81
struct U3CPostU3Ec__AnonStorey81_t213725630;
// System.Collections.IEnumerator
struct IEnumerator_t287207039;
// UniRx.IObserver`1<System.String>
struct IObserver_1_t3180487805;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_CancellationTo1439151560.h"

// System.Void UniRx.ObservableWWW/<Post>c__AnonStorey81::.ctor()
extern "C"  void U3CPostU3Ec__AnonStorey81__ctor_m3231068460 (U3CPostU3Ec__AnonStorey81_t213725630 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator UniRx.ObservableWWW/<Post>c__AnonStorey81::<>m__AD(UniRx.IObserver`1<System.String>,UniRx.CancellationToken)
extern "C"  Il2CppObject * U3CPostU3Ec__AnonStorey81_U3CU3Em__AD_m2809010340 (U3CPostU3Ec__AnonStorey81_t213725630 * __this, Il2CppObject* ___observer0, CancellationToken_t1439151560  ___cancellation1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
