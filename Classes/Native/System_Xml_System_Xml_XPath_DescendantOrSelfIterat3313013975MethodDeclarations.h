﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Xml.XPath.DescendantOrSelfIterator
struct DescendantOrSelfIterator_t3313013975;
// System.Xml.XPath.BaseIterator
struct BaseIterator_t3696600956;
// System.Xml.XPath.XPathNodeIterator
struct XPathNodeIterator_t2394191562;

#include "codegen/il2cpp-codegen.h"
#include "System_Xml_System_Xml_XPath_BaseIterator3696600956.h"
#include "System_Xml_System_Xml_XPath_DescendantOrSelfIterat3313013975.h"

// System.Void System.Xml.XPath.DescendantOrSelfIterator::.ctor(System.Xml.XPath.BaseIterator)
extern "C"  void DescendantOrSelfIterator__ctor_m135009740 (DescendantOrSelfIterator_t3313013975 * __this, BaseIterator_t3696600956 * ___iter0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Xml.XPath.DescendantOrSelfIterator::.ctor(System.Xml.XPath.DescendantOrSelfIterator)
extern "C"  void DescendantOrSelfIterator__ctor_m1458340049 (DescendantOrSelfIterator_t3313013975 * __this, DescendantOrSelfIterator_t3313013975 * ___other0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Xml.XPath.XPathNodeIterator System.Xml.XPath.DescendantOrSelfIterator::Clone()
extern "C"  XPathNodeIterator_t2394191562 * DescendantOrSelfIterator_Clone_m3088273414 (DescendantOrSelfIterator_t3313013975 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Xml.XPath.DescendantOrSelfIterator::MoveNextCore()
extern "C"  bool DescendantOrSelfIterator_MoveNextCore_m34430975 (DescendantOrSelfIterator_t3313013975 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
