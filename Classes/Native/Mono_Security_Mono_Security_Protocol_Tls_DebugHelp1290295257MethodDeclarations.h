﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Mono.Security.Protocol.Tls.DebugHelper
struct DebugHelper_t1290295257;
// System.String
struct String_t;
// System.Object[]
struct ObjectU5BU5D_t11523773;
// System.Byte[]
struct ByteU5BU5D_t58506160;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String968488902.h"

// System.Void Mono.Security.Protocol.Tls.DebugHelper::.ctor()
extern "C"  void DebugHelper__ctor_m3265278781 (DebugHelper_t1290295257 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Security.Protocol.Tls.DebugHelper::Initialize()
extern "C"  void DebugHelper_Initialize_m2232131959 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Security.Protocol.Tls.DebugHelper::WriteLine(System.String,System.Object[])
extern "C"  void DebugHelper_WriteLine_m2306356512 (Il2CppObject * __this /* static, unused */, String_t* ___format0, ObjectU5BU5D_t11523773* ___args1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Security.Protocol.Tls.DebugHelper::WriteLine(System.String)
extern "C"  void DebugHelper_WriteLine_m3867444916 (Il2CppObject * __this /* static, unused */, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Security.Protocol.Tls.DebugHelper::WriteLine(System.String,System.Byte[])
extern "C"  void DebugHelper_WriteLine_m3443147959 (Il2CppObject * __this /* static, unused */, String_t* ___message0, ByteU5BU5D_t58506160* ___buffer1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Security.Protocol.Tls.DebugHelper::WriteBuffer(System.Byte[])
extern "C"  void DebugHelper_WriteBuffer_m1702697967 (Il2CppObject * __this /* static, unused */, ByteU5BU5D_t58506160* ___buffer0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Security.Protocol.Tls.DebugHelper::WriteBuffer(System.Byte[],System.Int32,System.Int32)
extern "C"  void DebugHelper_WriteBuffer_m1189205135 (Il2CppObject * __this /* static, unused */, ByteU5BU5D_t58506160* ___buffer0, int32_t ___index1, int32_t ___length2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
