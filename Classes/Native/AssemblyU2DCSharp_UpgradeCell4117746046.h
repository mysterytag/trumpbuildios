﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.UI.Image
struct Image_t3354615620;
// UnityEngine.UI.Text
struct Text_t3286458198;
// UniRx.Subject`1<System.String>
struct Subject_1_t2906523522;
// UnityEngine.Sprite[]
struct SpriteU5BU5D_t503173063;

#include "AssemblyU2DCSharp_TableViewCellBase2057844710.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UpgradeCell
struct  UpgradeCell_t4117746046  : public TableViewCellBase_t2057844710
{
public:
	// UnityEngine.UI.Image UpgradeCell::TimerImage
	Image_t3354615620 * ___TimerImage_13;
	// UnityEngine.UI.Text UpgradeCell::TimerText
	Text_t3286458198 * ___TimerText_14;
	// UnityEngine.UI.Text UpgradeCell::TimerCostText
	Text_t3286458198 * ___TimerCostText_15;
	// UnityEngine.UI.Text UpgradeCell::Title
	Text_t3286458198 * ___Title_16;
	// UnityEngine.UI.Image UpgradeCell::BackImage
	Image_t3354615620 * ___BackImage_17;
	// UnityEngine.UI.Image UpgradeCell::Circle
	Image_t3354615620 * ___Circle_18;
	// UnityEngine.UI.Text UpgradeCell::Level
	Text_t3286458198 * ___Level_20;
	// UnityEngine.UI.Text UpgradeCell::VmestimostText
	Text_t3286458198 * ___VmestimostText_21;
	// UnityEngine.UI.Text UpgradeCell::UpgradeBoostText
	Text_t3286458198 * ___UpgradeBoostText_22;
	// UnityEngine.UI.Text UpgradeCell::Price
	Text_t3286458198 * ___Price_23;
	// UnityEngine.Sprite[] UpgradeCell::PriceButtonSprites
	SpriteU5BU5D_t503173063* ___PriceButtonSprites_24;
	// UnityEngine.UI.Text UpgradeCell::BillText
	Text_t3286458198 * ___BillText_25;
	// UnityEngine.UI.Image UpgradeCell::PriceButtonImage
	Image_t3354615620 * ___PriceButtonImage_26;

public:
	inline static int32_t get_offset_of_TimerImage_13() { return static_cast<int32_t>(offsetof(UpgradeCell_t4117746046, ___TimerImage_13)); }
	inline Image_t3354615620 * get_TimerImage_13() const { return ___TimerImage_13; }
	inline Image_t3354615620 ** get_address_of_TimerImage_13() { return &___TimerImage_13; }
	inline void set_TimerImage_13(Image_t3354615620 * value)
	{
		___TimerImage_13 = value;
		Il2CppCodeGenWriteBarrier(&___TimerImage_13, value);
	}

	inline static int32_t get_offset_of_TimerText_14() { return static_cast<int32_t>(offsetof(UpgradeCell_t4117746046, ___TimerText_14)); }
	inline Text_t3286458198 * get_TimerText_14() const { return ___TimerText_14; }
	inline Text_t3286458198 ** get_address_of_TimerText_14() { return &___TimerText_14; }
	inline void set_TimerText_14(Text_t3286458198 * value)
	{
		___TimerText_14 = value;
		Il2CppCodeGenWriteBarrier(&___TimerText_14, value);
	}

	inline static int32_t get_offset_of_TimerCostText_15() { return static_cast<int32_t>(offsetof(UpgradeCell_t4117746046, ___TimerCostText_15)); }
	inline Text_t3286458198 * get_TimerCostText_15() const { return ___TimerCostText_15; }
	inline Text_t3286458198 ** get_address_of_TimerCostText_15() { return &___TimerCostText_15; }
	inline void set_TimerCostText_15(Text_t3286458198 * value)
	{
		___TimerCostText_15 = value;
		Il2CppCodeGenWriteBarrier(&___TimerCostText_15, value);
	}

	inline static int32_t get_offset_of_Title_16() { return static_cast<int32_t>(offsetof(UpgradeCell_t4117746046, ___Title_16)); }
	inline Text_t3286458198 * get_Title_16() const { return ___Title_16; }
	inline Text_t3286458198 ** get_address_of_Title_16() { return &___Title_16; }
	inline void set_Title_16(Text_t3286458198 * value)
	{
		___Title_16 = value;
		Il2CppCodeGenWriteBarrier(&___Title_16, value);
	}

	inline static int32_t get_offset_of_BackImage_17() { return static_cast<int32_t>(offsetof(UpgradeCell_t4117746046, ___BackImage_17)); }
	inline Image_t3354615620 * get_BackImage_17() const { return ___BackImage_17; }
	inline Image_t3354615620 ** get_address_of_BackImage_17() { return &___BackImage_17; }
	inline void set_BackImage_17(Image_t3354615620 * value)
	{
		___BackImage_17 = value;
		Il2CppCodeGenWriteBarrier(&___BackImage_17, value);
	}

	inline static int32_t get_offset_of_Circle_18() { return static_cast<int32_t>(offsetof(UpgradeCell_t4117746046, ___Circle_18)); }
	inline Image_t3354615620 * get_Circle_18() const { return ___Circle_18; }
	inline Image_t3354615620 ** get_address_of_Circle_18() { return &___Circle_18; }
	inline void set_Circle_18(Image_t3354615620 * value)
	{
		___Circle_18 = value;
		Il2CppCodeGenWriteBarrier(&___Circle_18, value);
	}

	inline static int32_t get_offset_of_Level_20() { return static_cast<int32_t>(offsetof(UpgradeCell_t4117746046, ___Level_20)); }
	inline Text_t3286458198 * get_Level_20() const { return ___Level_20; }
	inline Text_t3286458198 ** get_address_of_Level_20() { return &___Level_20; }
	inline void set_Level_20(Text_t3286458198 * value)
	{
		___Level_20 = value;
		Il2CppCodeGenWriteBarrier(&___Level_20, value);
	}

	inline static int32_t get_offset_of_VmestimostText_21() { return static_cast<int32_t>(offsetof(UpgradeCell_t4117746046, ___VmestimostText_21)); }
	inline Text_t3286458198 * get_VmestimostText_21() const { return ___VmestimostText_21; }
	inline Text_t3286458198 ** get_address_of_VmestimostText_21() { return &___VmestimostText_21; }
	inline void set_VmestimostText_21(Text_t3286458198 * value)
	{
		___VmestimostText_21 = value;
		Il2CppCodeGenWriteBarrier(&___VmestimostText_21, value);
	}

	inline static int32_t get_offset_of_UpgradeBoostText_22() { return static_cast<int32_t>(offsetof(UpgradeCell_t4117746046, ___UpgradeBoostText_22)); }
	inline Text_t3286458198 * get_UpgradeBoostText_22() const { return ___UpgradeBoostText_22; }
	inline Text_t3286458198 ** get_address_of_UpgradeBoostText_22() { return &___UpgradeBoostText_22; }
	inline void set_UpgradeBoostText_22(Text_t3286458198 * value)
	{
		___UpgradeBoostText_22 = value;
		Il2CppCodeGenWriteBarrier(&___UpgradeBoostText_22, value);
	}

	inline static int32_t get_offset_of_Price_23() { return static_cast<int32_t>(offsetof(UpgradeCell_t4117746046, ___Price_23)); }
	inline Text_t3286458198 * get_Price_23() const { return ___Price_23; }
	inline Text_t3286458198 ** get_address_of_Price_23() { return &___Price_23; }
	inline void set_Price_23(Text_t3286458198 * value)
	{
		___Price_23 = value;
		Il2CppCodeGenWriteBarrier(&___Price_23, value);
	}

	inline static int32_t get_offset_of_PriceButtonSprites_24() { return static_cast<int32_t>(offsetof(UpgradeCell_t4117746046, ___PriceButtonSprites_24)); }
	inline SpriteU5BU5D_t503173063* get_PriceButtonSprites_24() const { return ___PriceButtonSprites_24; }
	inline SpriteU5BU5D_t503173063** get_address_of_PriceButtonSprites_24() { return &___PriceButtonSprites_24; }
	inline void set_PriceButtonSprites_24(SpriteU5BU5D_t503173063* value)
	{
		___PriceButtonSprites_24 = value;
		Il2CppCodeGenWriteBarrier(&___PriceButtonSprites_24, value);
	}

	inline static int32_t get_offset_of_BillText_25() { return static_cast<int32_t>(offsetof(UpgradeCell_t4117746046, ___BillText_25)); }
	inline Text_t3286458198 * get_BillText_25() const { return ___BillText_25; }
	inline Text_t3286458198 ** get_address_of_BillText_25() { return &___BillText_25; }
	inline void set_BillText_25(Text_t3286458198 * value)
	{
		___BillText_25 = value;
		Il2CppCodeGenWriteBarrier(&___BillText_25, value);
	}

	inline static int32_t get_offset_of_PriceButtonImage_26() { return static_cast<int32_t>(offsetof(UpgradeCell_t4117746046, ___PriceButtonImage_26)); }
	inline Image_t3354615620 * get_PriceButtonImage_26() const { return ___PriceButtonImage_26; }
	inline Image_t3354615620 ** get_address_of_PriceButtonImage_26() { return &___PriceButtonImage_26; }
	inline void set_PriceButtonImage_26(Image_t3354615620 * value)
	{
		___PriceButtonImage_26 = value;
		Il2CppCodeGenWriteBarrier(&___PriceButtonImage_26, value);
	}
};

struct UpgradeCell_t4117746046_StaticFields
{
public:
	// UniRx.Subject`1<System.String> UpgradeCell::TutorialClickSubject
	Subject_1_t2906523522 * ___TutorialClickSubject_19;

public:
	inline static int32_t get_offset_of_TutorialClickSubject_19() { return static_cast<int32_t>(offsetof(UpgradeCell_t4117746046_StaticFields, ___TutorialClickSubject_19)); }
	inline Subject_1_t2906523522 * get_TutorialClickSubject_19() const { return ___TutorialClickSubject_19; }
	inline Subject_1_t2906523522 ** get_address_of_TutorialClickSubject_19() { return &___TutorialClickSubject_19; }
	inline void set_TutorialClickSubject_19(Subject_1_t2906523522 * value)
	{
		___TutorialClickSubject_19 = value;
		Il2CppCodeGenWriteBarrier(&___TutorialClickSubject_19, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
