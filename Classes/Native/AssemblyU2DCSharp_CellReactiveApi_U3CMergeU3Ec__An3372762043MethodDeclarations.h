﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CellReactiveApi/<Merge>c__AnonStorey112`3/<Merge>c__AnonStorey113`3<System.Double,System.Int32,System.Double>
struct U3CMergeU3Ec__AnonStorey113_3_t3372762043;

#include "codegen/il2cpp-codegen.h"

// System.Void CellReactiveApi/<Merge>c__AnonStorey112`3/<Merge>c__AnonStorey113`3<System.Double,System.Int32,System.Double>::.ctor()
extern "C"  void U3CMergeU3Ec__AnonStorey113_3__ctor_m1555042902_gshared (U3CMergeU3Ec__AnonStorey113_3_t3372762043 * __this, const MethodInfo* method);
#define U3CMergeU3Ec__AnonStorey113_3__ctor_m1555042902(__this, method) ((  void (*) (U3CMergeU3Ec__AnonStorey113_3_t3372762043 *, const MethodInfo*))U3CMergeU3Ec__AnonStorey113_3__ctor_m1555042902_gshared)(__this, method)
// System.Void CellReactiveApi/<Merge>c__AnonStorey112`3/<Merge>c__AnonStorey113`3<System.Double,System.Int32,System.Double>::<>m__19A(T)
extern "C"  void U3CMergeU3Ec__AnonStorey113_3_U3CU3Em__19A_m4098517474_gshared (U3CMergeU3Ec__AnonStorey113_3_t3372762043 * __this, double ___val0, const MethodInfo* method);
#define U3CMergeU3Ec__AnonStorey113_3_U3CU3Em__19A_m4098517474(__this, ___val0, method) ((  void (*) (U3CMergeU3Ec__AnonStorey113_3_t3372762043 *, double, const MethodInfo*))U3CMergeU3Ec__AnonStorey113_3_U3CU3Em__19A_m4098517474_gshared)(__this, ___val0, method)
// System.Void CellReactiveApi/<Merge>c__AnonStorey112`3/<Merge>c__AnonStorey113`3<System.Double,System.Int32,System.Double>::<>m__19B(T2)
extern "C"  void U3CMergeU3Ec__AnonStorey113_3_U3CU3Em__19B_m2500913951_gshared (U3CMergeU3Ec__AnonStorey113_3_t3372762043 * __this, int32_t ___val0, const MethodInfo* method);
#define U3CMergeU3Ec__AnonStorey113_3_U3CU3Em__19B_m2500913951(__this, ___val0, method) ((  void (*) (U3CMergeU3Ec__AnonStorey113_3_t3372762043 *, int32_t, const MethodInfo*))U3CMergeU3Ec__AnonStorey113_3_U3CU3Em__19B_m2500913951_gshared)(__this, ___val0, method)
