﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Triggers.ObservableInitializePotentialDragTrigger
struct ObservableInitializePotentialDragTrigger_t1384823706;
// UnityEngine.EventSystems.PointerEventData
struct PointerEventData_t3205101634;
// UniRx.IObservable`1<UnityEngine.EventSystems.PointerEventData>
struct IObservable_1_t2963899998;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_PointerEve3205101634.h"

// System.Void UniRx.Triggers.ObservableInitializePotentialDragTrigger::.ctor()
extern "C"  void ObservableInitializePotentialDragTrigger__ctor_m4004212195 (ObservableInitializePotentialDragTrigger_t1384823706 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.Triggers.ObservableInitializePotentialDragTrigger::UnityEngine.EventSystems.IInitializePotentialDragHandler.OnInitializePotentialDrag(UnityEngine.EventSystems.PointerEventData)
extern "C"  void ObservableInitializePotentialDragTrigger_UnityEngine_EventSystems_IInitializePotentialDragHandler_OnInitializePotentialDrag_m33200132 (ObservableInitializePotentialDragTrigger_t1384823706 * __this, PointerEventData_t3205101634 * ___eventData0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UniRx.IObservable`1<UnityEngine.EventSystems.PointerEventData> UniRx.Triggers.ObservableInitializePotentialDragTrigger::OnInitializePotentialDragAsObservable()
extern "C"  Il2CppObject* ObservableInitializePotentialDragTrigger_OnInitializePotentialDragAsObservable_m432309373 (ObservableInitializePotentialDragTrigger_t1384823706 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.Triggers.ObservableInitializePotentialDragTrigger::RaiseOnCompletedOnDestroy()
extern "C"  void ObservableInitializePotentialDragTrigger_RaiseOnCompletedOnDestroy_m1432974748 (ObservableInitializePotentialDragTrigger_t1384823706 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
