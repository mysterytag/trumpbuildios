﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.ClientModels.CharacterResult
struct CharacterResult_t274624662;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String968488902.h"

// System.Void PlayFab.ClientModels.CharacterResult::.ctor()
extern "C"  void CharacterResult__ctor_m3657928659 (CharacterResult_t274624662 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PlayFab.ClientModels.CharacterResult::get_CharacterId()
extern "C"  String_t* CharacterResult_get_CharacterId_m404130665 (CharacterResult_t274624662 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.CharacterResult::set_CharacterId(System.String)
extern "C"  void CharacterResult_set_CharacterId_m3537197322 (CharacterResult_t274624662 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PlayFab.ClientModels.CharacterResult::get_CharacterName()
extern "C"  String_t* CharacterResult_get_CharacterName_m1965002137 (CharacterResult_t274624662 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.CharacterResult::set_CharacterName(System.String)
extern "C"  void CharacterResult_set_CharacterName_m238285978 (CharacterResult_t274624662 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PlayFab.ClientModels.CharacterResult::get_CharacterType()
extern "C"  String_t* CharacterResult_get_CharacterType_m2159030920 (CharacterResult_t274624662 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.CharacterResult::set_CharacterType(System.String)
extern "C"  void CharacterResult_set_CharacterType_m1071451147 (CharacterResult_t274624662 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
