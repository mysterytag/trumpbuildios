﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.List`1<UnityEngine.Quaternion>
struct List_1_t2688674948;
// System.Collections.Generic.IEnumerable`1<UnityEngine.Quaternion>
struct IEnumerable_1_t468903039;
// System.Collections.Generic.IEnumerator`1<UnityEngine.Quaternion>
struct IEnumerator_1_t3374822427;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t287207039;
// System.Object
struct Il2CppObject;
// System.Collections.Generic.ICollection`1<UnityEngine.Quaternion>
struct ICollection_1_t2357547365;
// System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Quaternion>
struct ReadOnlyCollection_1_t759894031;
// UnityEngine.Quaternion[]
struct QuaternionU5BU5D_t3236402666;
// System.Predicate`1<UnityEngine.Quaternion>
struct Predicate_1_t2462679877;
// System.Action`1<UnityEngine.Quaternion>
struct Action_1_t2040168684;
// System.Collections.Generic.IComparer`1<UnityEngine.Quaternion>
struct IComparer_1_t296456092;
// System.Comparison`1<UnityEngine.Quaternion>
struct Comparison_1_t300423559;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array2840145358.h"
#include "mscorlib_System_Object837106420.h"
#include "UnityEngine_UnityEngine_Quaternion1891715979.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumerat774457940.h"

// System.Void System.Collections.Generic.List`1<UnityEngine.Quaternion>::.ctor()
extern "C"  void List_1__ctor_m2870613961_gshared (List_1_t2688674948 * __this, const MethodInfo* method);
#define List_1__ctor_m2870613961(__this, method) ((  void (*) (List_1_t2688674948 *, const MethodInfo*))List_1__ctor_m2870613961_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Quaternion>::.ctor(System.Collections.Generic.IEnumerable`1<T>)
extern "C"  void List_1__ctor_m1006776598_gshared (List_1_t2688674948 * __this, Il2CppObject* ___collection0, const MethodInfo* method);
#define List_1__ctor_m1006776598(__this, ___collection0, method) ((  void (*) (List_1_t2688674948 *, Il2CppObject*, const MethodInfo*))List_1__ctor_m1006776598_gshared)(__this, ___collection0, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Quaternion>::.ctor(System.Int32)
extern "C"  void List_1__ctor_m4112344986_gshared (List_1_t2688674948 * __this, int32_t ___capacity0, const MethodInfo* method);
#define List_1__ctor_m4112344986(__this, ___capacity0, method) ((  void (*) (List_1_t2688674948 *, int32_t, const MethodInfo*))List_1__ctor_m4112344986_gshared)(__this, ___capacity0, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Quaternion>::.cctor()
extern "C"  void List_1__cctor_m2607590660_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define List_1__cctor_m2607590660(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))List_1__cctor_m2607590660_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.List`1<UnityEngine.Quaternion>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
extern "C"  Il2CppObject* List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m2276945555_gshared (List_1_t2688674948 * __this, const MethodInfo* method);
#define List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m2276945555(__this, method) ((  Il2CppObject* (*) (List_1_t2688674948 *, const MethodInfo*))List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m2276945555_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Quaternion>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void List_1_System_Collections_ICollection_CopyTo_m3433958811_gshared (List_1_t2688674948 * __this, Il2CppArray * ___array0, int32_t ___arrayIndex1, const MethodInfo* method);
#define List_1_System_Collections_ICollection_CopyTo_m3433958811(__this, ___array0, ___arrayIndex1, method) ((  void (*) (List_1_t2688674948 *, Il2CppArray *, int32_t, const MethodInfo*))List_1_System_Collections_ICollection_CopyTo_m3433958811_gshared)(__this, ___array0, ___arrayIndex1, method)
// System.Collections.IEnumerator System.Collections.Generic.List`1<UnityEngine.Quaternion>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * List_1_System_Collections_IEnumerable_GetEnumerator_m3653941994_gshared (List_1_t2688674948 * __this, const MethodInfo* method);
#define List_1_System_Collections_IEnumerable_GetEnumerator_m3653941994(__this, method) ((  Il2CppObject * (*) (List_1_t2688674948 *, const MethodInfo*))List_1_System_Collections_IEnumerable_GetEnumerator_m3653941994_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Quaternion>::System.Collections.IList.Add(System.Object)
extern "C"  int32_t List_1_System_Collections_IList_Add_m4151915283_gshared (List_1_t2688674948 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define List_1_System_Collections_IList_Add_m4151915283(__this, ___item0, method) ((  int32_t (*) (List_1_t2688674948 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Add_m4151915283_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.Quaternion>::System.Collections.IList.Contains(System.Object)
extern "C"  bool List_1_System_Collections_IList_Contains_m183552141_gshared (List_1_t2688674948 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define List_1_System_Collections_IList_Contains_m183552141(__this, ___item0, method) ((  bool (*) (List_1_t2688674948 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Contains_m183552141_gshared)(__this, ___item0, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Quaternion>::System.Collections.IList.IndexOf(System.Object)
extern "C"  int32_t List_1_System_Collections_IList_IndexOf_m301180907_gshared (List_1_t2688674948 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define List_1_System_Collections_IList_IndexOf_m301180907(__this, ___item0, method) ((  int32_t (*) (List_1_t2688674948 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_IndexOf_m301180907_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Quaternion>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C"  void List_1_System_Collections_IList_Insert_m3306660318_gshared (List_1_t2688674948 * __this, int32_t ___index0, Il2CppObject * ___item1, const MethodInfo* method);
#define List_1_System_Collections_IList_Insert_m3306660318(__this, ___index0, ___item1, method) ((  void (*) (List_1_t2688674948 *, int32_t, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Insert_m3306660318_gshared)(__this, ___index0, ___item1, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Quaternion>::System.Collections.IList.Remove(System.Object)
extern "C"  void List_1_System_Collections_IList_Remove_m2481215114_gshared (List_1_t2688674948 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define List_1_System_Collections_IList_Remove_m2481215114(__this, ___item0, method) ((  void (*) (List_1_t2688674948 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Remove_m2481215114_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.Quaternion>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C"  bool List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m1815686222_gshared (List_1_t2688674948 * __this, const MethodInfo* method);
#define List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m1815686222(__this, method) ((  bool (*) (List_1_t2688674948 *, const MethodInfo*))List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m1815686222_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.Quaternion>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool List_1_System_Collections_ICollection_get_IsSynchronized_m703440815_gshared (List_1_t2688674948 * __this, const MethodInfo* method);
#define List_1_System_Collections_ICollection_get_IsSynchronized_m703440815(__this, method) ((  bool (*) (List_1_t2688674948 *, const MethodInfo*))List_1_System_Collections_ICollection_get_IsSynchronized_m703440815_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<UnityEngine.Quaternion>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * List_1_System_Collections_ICollection_get_SyncRoot_m1383406369_gshared (List_1_t2688674948 * __this, const MethodInfo* method);
#define List_1_System_Collections_ICollection_get_SyncRoot_m1383406369(__this, method) ((  Il2CppObject * (*) (List_1_t2688674948 *, const MethodInfo*))List_1_System_Collections_ICollection_get_SyncRoot_m1383406369_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.Quaternion>::System.Collections.IList.get_IsFixedSize()
extern "C"  bool List_1_System_Collections_IList_get_IsFixedSize_m489235900_gshared (List_1_t2688674948 * __this, const MethodInfo* method);
#define List_1_System_Collections_IList_get_IsFixedSize_m489235900(__this, method) ((  bool (*) (List_1_t2688674948 *, const MethodInfo*))List_1_System_Collections_IList_get_IsFixedSize_m489235900_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.Quaternion>::System.Collections.IList.get_IsReadOnly()
extern "C"  bool List_1_System_Collections_IList_get_IsReadOnly_m1023979965_gshared (List_1_t2688674948 * __this, const MethodInfo* method);
#define List_1_System_Collections_IList_get_IsReadOnly_m1023979965(__this, method) ((  bool (*) (List_1_t2688674948 *, const MethodInfo*))List_1_System_Collections_IList_get_IsReadOnly_m1023979965_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<UnityEngine.Quaternion>::System.Collections.IList.get_Item(System.Int32)
extern "C"  Il2CppObject * List_1_System_Collections_IList_get_Item_m3374237800_gshared (List_1_t2688674948 * __this, int32_t ___index0, const MethodInfo* method);
#define List_1_System_Collections_IList_get_Item_m3374237800(__this, ___index0, method) ((  Il2CppObject * (*) (List_1_t2688674948 *, int32_t, const MethodInfo*))List_1_System_Collections_IList_get_Item_m3374237800_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Quaternion>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C"  void List_1_System_Collections_IList_set_Item_m38719029_gshared (List_1_t2688674948 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method);
#define List_1_System_Collections_IList_set_Item_m38719029(__this, ___index0, ___value1, method) ((  void (*) (List_1_t2688674948 *, int32_t, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_set_Item_m38719029_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Quaternion>::Add(T)
extern "C"  void List_1_Add_m3823520662_gshared (List_1_t2688674948 * __this, Quaternion_t1891715979  ___item0, const MethodInfo* method);
#define List_1_Add_m3823520662(__this, ___item0, method) ((  void (*) (List_1_t2688674948 *, Quaternion_t1891715979 , const MethodInfo*))List_1_Add_m3823520662_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Quaternion>::GrowIfNeeded(System.Int32)
extern "C"  void List_1_GrowIfNeeded_m3525694161_gshared (List_1_t2688674948 * __this, int32_t ___newCount0, const MethodInfo* method);
#define List_1_GrowIfNeeded_m3525694161(__this, ___newCount0, method) ((  void (*) (List_1_t2688674948 *, int32_t, const MethodInfo*))List_1_GrowIfNeeded_m3525694161_gshared)(__this, ___newCount0, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Quaternion>::AddCollection(System.Collections.Generic.ICollection`1<T>)
extern "C"  void List_1_AddCollection_m3765347791_gshared (List_1_t2688674948 * __this, Il2CppObject* ___collection0, const MethodInfo* method);
#define List_1_AddCollection_m3765347791(__this, ___collection0, method) ((  void (*) (List_1_t2688674948 *, Il2CppObject*, const MethodInfo*))List_1_AddCollection_m3765347791_gshared)(__this, ___collection0, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Quaternion>::AddEnumerable(System.Collections.Generic.IEnumerable`1<T>)
extern "C"  void List_1_AddEnumerable_m3026320015_gshared (List_1_t2688674948 * __this, Il2CppObject* ___enumerable0, const MethodInfo* method);
#define List_1_AddEnumerable_m3026320015(__this, ___enumerable0, method) ((  void (*) (List_1_t2688674948 *, Il2CppObject*, const MethodInfo*))List_1_AddEnumerable_m3026320015_gshared)(__this, ___enumerable0, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Quaternion>::AddRange(System.Collections.Generic.IEnumerable`1<T>)
extern "C"  void List_1_AddRange_m880957096_gshared (List_1_t2688674948 * __this, Il2CppObject* ___collection0, const MethodInfo* method);
#define List_1_AddRange_m880957096(__this, ___collection0, method) ((  void (*) (List_1_t2688674948 *, Il2CppObject*, const MethodInfo*))List_1_AddRange_m880957096_gshared)(__this, ___collection0, method)
// System.Collections.ObjectModel.ReadOnlyCollection`1<T> System.Collections.Generic.List`1<UnityEngine.Quaternion>::AsReadOnly()
extern "C"  ReadOnlyCollection_1_t759894031 * List_1_AsReadOnly_m1938556637_gshared (List_1_t2688674948 * __this, const MethodInfo* method);
#define List_1_AsReadOnly_m1938556637(__this, method) ((  ReadOnlyCollection_1_t759894031 * (*) (List_1_t2688674948 *, const MethodInfo*))List_1_AsReadOnly_m1938556637_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Quaternion>::Clear()
extern "C"  void List_1_Clear_m276747252_gshared (List_1_t2688674948 * __this, const MethodInfo* method);
#define List_1_Clear_m276747252(__this, method) ((  void (*) (List_1_t2688674948 *, const MethodInfo*))List_1_Clear_m276747252_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.Quaternion>::Contains(T)
extern "C"  bool List_1_Contains_m837360742_gshared (List_1_t2688674948 * __this, Quaternion_t1891715979  ___item0, const MethodInfo* method);
#define List_1_Contains_m837360742(__this, ___item0, method) ((  bool (*) (List_1_t2688674948 *, Quaternion_t1891715979 , const MethodInfo*))List_1_Contains_m837360742_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Quaternion>::CopyTo(T[],System.Int32)
extern "C"  void List_1_CopyTo_m805616134_gshared (List_1_t2688674948 * __this, QuaternionU5BU5D_t3236402666* ___array0, int32_t ___arrayIndex1, const MethodInfo* method);
#define List_1_CopyTo_m805616134(__this, ___array0, ___arrayIndex1, method) ((  void (*) (List_1_t2688674948 *, QuaternionU5BU5D_t3236402666*, int32_t, const MethodInfo*))List_1_CopyTo_m805616134_gshared)(__this, ___array0, ___arrayIndex1, method)
// T System.Collections.Generic.List`1<UnityEngine.Quaternion>::Find(System.Predicate`1<T>)
extern "C"  Quaternion_t1891715979  List_1_Find_m2281100800_gshared (List_1_t2688674948 * __this, Predicate_1_t2462679877 * ___match0, const MethodInfo* method);
#define List_1_Find_m2281100800(__this, ___match0, method) ((  Quaternion_t1891715979  (*) (List_1_t2688674948 *, Predicate_1_t2462679877 *, const MethodInfo*))List_1_Find_m2281100800_gshared)(__this, ___match0, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Quaternion>::CheckMatch(System.Predicate`1<T>)
extern "C"  void List_1_CheckMatch_m413617053_gshared (Il2CppObject * __this /* static, unused */, Predicate_1_t2462679877 * ___match0, const MethodInfo* method);
#define List_1_CheckMatch_m413617053(__this /* static, unused */, ___match0, method) ((  void (*) (Il2CppObject * /* static, unused */, Predicate_1_t2462679877 *, const MethodInfo*))List_1_CheckMatch_m413617053_gshared)(__this /* static, unused */, ___match0, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Quaternion>::GetIndex(System.Int32,System.Int32,System.Predicate`1<T>)
extern "C"  int32_t List_1_GetIndex_m991063930_gshared (List_1_t2688674948 * __this, int32_t ___startIndex0, int32_t ___count1, Predicate_1_t2462679877 * ___match2, const MethodInfo* method);
#define List_1_GetIndex_m991063930(__this, ___startIndex0, ___count1, ___match2, method) ((  int32_t (*) (List_1_t2688674948 *, int32_t, int32_t, Predicate_1_t2462679877 *, const MethodInfo*))List_1_GetIndex_m991063930_gshared)(__this, ___startIndex0, ___count1, ___match2, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Quaternion>::ForEach(System.Action`1<T>)
extern "C"  void List_1_ForEach_m706109649_gshared (List_1_t2688674948 * __this, Action_1_t2040168684 * ___action0, const MethodInfo* method);
#define List_1_ForEach_m706109649(__this, ___action0, method) ((  void (*) (List_1_t2688674948 *, Action_1_t2040168684 *, const MethodInfo*))List_1_ForEach_m706109649_gshared)(__this, ___action0, method)
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<UnityEngine.Quaternion>::GetEnumerator()
extern "C"  Enumerator_t774457940  List_1_GetEnumerator_m2844375715_gshared (List_1_t2688674948 * __this, const MethodInfo* method);
#define List_1_GetEnumerator_m2844375715(__this, method) ((  Enumerator_t774457940  (*) (List_1_t2688674948 *, const MethodInfo*))List_1_GetEnumerator_m2844375715_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Quaternion>::IndexOf(T)
extern "C"  int32_t List_1_IndexOf_m3670209874_gshared (List_1_t2688674948 * __this, Quaternion_t1891715979  ___item0, const MethodInfo* method);
#define List_1_IndexOf_m3670209874(__this, ___item0, method) ((  int32_t (*) (List_1_t2688674948 *, Quaternion_t1891715979 , const MethodInfo*))List_1_IndexOf_m3670209874_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Quaternion>::Shift(System.Int32,System.Int32)
extern "C"  void List_1_Shift_m2325555805_gshared (List_1_t2688674948 * __this, int32_t ___start0, int32_t ___delta1, const MethodInfo* method);
#define List_1_Shift_m2325555805(__this, ___start0, ___delta1, method) ((  void (*) (List_1_t2688674948 *, int32_t, int32_t, const MethodInfo*))List_1_Shift_m2325555805_gshared)(__this, ___start0, ___delta1, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Quaternion>::CheckIndex(System.Int32)
extern "C"  void List_1_CheckIndex_m551983318_gshared (List_1_t2688674948 * __this, int32_t ___index0, const MethodInfo* method);
#define List_1_CheckIndex_m551983318(__this, ___index0, method) ((  void (*) (List_1_t2688674948 *, int32_t, const MethodInfo*))List_1_CheckIndex_m551983318_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Quaternion>::Insert(System.Int32,T)
extern "C"  void List_1_Insert_m2641730045_gshared (List_1_t2688674948 * __this, int32_t ___index0, Quaternion_t1891715979  ___item1, const MethodInfo* method);
#define List_1_Insert_m2641730045(__this, ___index0, ___item1, method) ((  void (*) (List_1_t2688674948 *, int32_t, Quaternion_t1891715979 , const MethodInfo*))List_1_Insert_m2641730045_gshared)(__this, ___index0, ___item1, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Quaternion>::CheckCollection(System.Collections.Generic.IEnumerable`1<T>)
extern "C"  void List_1_CheckCollection_m2663432370_gshared (List_1_t2688674948 * __this, Il2CppObject* ___collection0, const MethodInfo* method);
#define List_1_CheckCollection_m2663432370(__this, ___collection0, method) ((  void (*) (List_1_t2688674948 *, Il2CppObject*, const MethodInfo*))List_1_CheckCollection_m2663432370_gshared)(__this, ___collection0, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.Quaternion>::Remove(T)
extern "C"  bool List_1_Remove_m1399176673_gshared (List_1_t2688674948 * __this, Quaternion_t1891715979  ___item0, const MethodInfo* method);
#define List_1_Remove_m1399176673(__this, ___item0, method) ((  bool (*) (List_1_t2688674948 *, Quaternion_t1891715979 , const MethodInfo*))List_1_Remove_m1399176673_gshared)(__this, ___item0, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Quaternion>::RemoveAll(System.Predicate`1<T>)
extern "C"  int32_t List_1_RemoveAll_m4125823893_gshared (List_1_t2688674948 * __this, Predicate_1_t2462679877 * ___match0, const MethodInfo* method);
#define List_1_RemoveAll_m4125823893(__this, ___match0, method) ((  int32_t (*) (List_1_t2688674948 *, Predicate_1_t2462679877 *, const MethodInfo*))List_1_RemoveAll_m4125823893_gshared)(__this, ___match0, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Quaternion>::RemoveAt(System.Int32)
extern "C"  void List_1_RemoveAt_m515582915_gshared (List_1_t2688674948 * __this, int32_t ___index0, const MethodInfo* method);
#define List_1_RemoveAt_m515582915(__this, ___index0, method) ((  void (*) (List_1_t2688674948 *, int32_t, const MethodInfo*))List_1_RemoveAt_m515582915_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Quaternion>::Reverse()
extern "C"  void List_1_Reverse_m1328823753_gshared (List_1_t2688674948 * __this, const MethodInfo* method);
#define List_1_Reverse_m1328823753(__this, method) ((  void (*) (List_1_t2688674948 *, const MethodInfo*))List_1_Reverse_m1328823753_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Quaternion>::Sort()
extern "C"  void List_1_Sort_m54525561_gshared (List_1_t2688674948 * __this, const MethodInfo* method);
#define List_1_Sort_m54525561(__this, method) ((  void (*) (List_1_t2688674948 *, const MethodInfo*))List_1_Sort_m54525561_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Quaternion>::Sort(System.Collections.Generic.IComparer`1<T>)
extern "C"  void List_1_Sort_m3906020235_gshared (List_1_t2688674948 * __this, Il2CppObject* ___comparer0, const MethodInfo* method);
#define List_1_Sort_m3906020235(__this, ___comparer0, method) ((  void (*) (List_1_t2688674948 *, Il2CppObject*, const MethodInfo*))List_1_Sort_m3906020235_gshared)(__this, ___comparer0, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Quaternion>::Sort(System.Comparison`1<T>)
extern "C"  void List_1_Sort_m1795488268_gshared (List_1_t2688674948 * __this, Comparison_1_t300423559 * ___comparison0, const MethodInfo* method);
#define List_1_Sort_m1795488268(__this, ___comparison0, method) ((  void (*) (List_1_t2688674948 *, Comparison_1_t300423559 *, const MethodInfo*))List_1_Sort_m1795488268_gshared)(__this, ___comparison0, method)
// T[] System.Collections.Generic.List`1<UnityEngine.Quaternion>::ToArray()
extern "C"  QuaternionU5BU5D_t3236402666* List_1_ToArray_m2046485922_gshared (List_1_t2688674948 * __this, const MethodInfo* method);
#define List_1_ToArray_m2046485922(__this, method) ((  QuaternionU5BU5D_t3236402666* (*) (List_1_t2688674948 *, const MethodInfo*))List_1_ToArray_m2046485922_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Quaternion>::TrimExcess()
extern "C"  void List_1_TrimExcess_m2949291858_gshared (List_1_t2688674948 * __this, const MethodInfo* method);
#define List_1_TrimExcess_m2949291858(__this, method) ((  void (*) (List_1_t2688674948 *, const MethodInfo*))List_1_TrimExcess_m2949291858_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Quaternion>::get_Capacity()
extern "C"  int32_t List_1_get_Capacity_m182711426_gshared (List_1_t2688674948 * __this, const MethodInfo* method);
#define List_1_get_Capacity_m182711426(__this, method) ((  int32_t (*) (List_1_t2688674948 *, const MethodInfo*))List_1_get_Capacity_m182711426_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Quaternion>::set_Capacity(System.Int32)
extern "C"  void List_1_set_Capacity_m735882979_gshared (List_1_t2688674948 * __this, int32_t ___value0, const MethodInfo* method);
#define List_1_set_Capacity_m735882979(__this, ___value0, method) ((  void (*) (List_1_t2688674948 *, int32_t, const MethodInfo*))List_1_set_Capacity_m735882979_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Quaternion>::get_Count()
extern "C"  int32_t List_1_get_Count_m1924663913_gshared (List_1_t2688674948 * __this, const MethodInfo* method);
#define List_1_get_Count_m1924663913(__this, method) ((  int32_t (*) (List_1_t2688674948 *, const MethodInfo*))List_1_get_Count_m1924663913_gshared)(__this, method)
// T System.Collections.Generic.List`1<UnityEngine.Quaternion>::get_Item(System.Int32)
extern "C"  Quaternion_t1891715979  List_1_get_Item_m1943397545_gshared (List_1_t2688674948 * __this, int32_t ___index0, const MethodInfo* method);
#define List_1_get_Item_m1943397545(__this, ___index0, method) ((  Quaternion_t1891715979  (*) (List_1_t2688674948 *, int32_t, const MethodInfo*))List_1_get_Item_m1943397545_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Quaternion>::set_Item(System.Int32,T)
extern "C"  void List_1_set_Item_m2186497940_gshared (List_1_t2688674948 * __this, int32_t ___index0, Quaternion_t1891715979  ___value1, const MethodInfo* method);
#define List_1_set_Item_m2186497940(__this, ___index0, ___value1, method) ((  void (*) (List_1_t2688674948 *, int32_t, Quaternion_t1891715979 , const MethodInfo*))List_1_set_Item_m2186497940_gshared)(__this, ___index0, ___value1, method)
