﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "UnityEngine_UnityEngine_PropertyAttribute3076083828.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.RangeReactivePropertyAttribute
struct  RangeReactivePropertyAttribute_t252243683  : public PropertyAttribute_t3076083828
{
public:
	// System.Single UniRx.RangeReactivePropertyAttribute::<Min>k__BackingField
	float ___U3CMinU3Ek__BackingField_0;
	// System.Single UniRx.RangeReactivePropertyAttribute::<Max>k__BackingField
	float ___U3CMaxU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CMinU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(RangeReactivePropertyAttribute_t252243683, ___U3CMinU3Ek__BackingField_0)); }
	inline float get_U3CMinU3Ek__BackingField_0() const { return ___U3CMinU3Ek__BackingField_0; }
	inline float* get_address_of_U3CMinU3Ek__BackingField_0() { return &___U3CMinU3Ek__BackingField_0; }
	inline void set_U3CMinU3Ek__BackingField_0(float value)
	{
		___U3CMinU3Ek__BackingField_0 = value;
	}

	inline static int32_t get_offset_of_U3CMaxU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(RangeReactivePropertyAttribute_t252243683, ___U3CMaxU3Ek__BackingField_1)); }
	inline float get_U3CMaxU3Ek__BackingField_1() const { return ___U3CMaxU3Ek__BackingField_1; }
	inline float* get_address_of_U3CMaxU3Ek__BackingField_1() { return &___U3CMaxU3Ek__BackingField_1; }
	inline void set_U3CMaxU3Ek__BackingField_1(float value)
	{
		___U3CMaxU3Ek__BackingField_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
