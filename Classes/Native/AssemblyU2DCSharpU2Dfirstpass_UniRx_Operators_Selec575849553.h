﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UniRx.IObservable`1<System.Object>
struct IObservable_1_t595904784;
// System.Func`2<System.Object,System.Object>
struct Func_2_t2135783352;
// System.Func`3<System.Object,System.Int32,System.Object>
struct Func_3_t3690668578;

#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Oper4196218687.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Operators.SelectObservable`2<System.Object,System.Object>
struct  SelectObservable_2_t575849553  : public OperatorObservableBase_1_t4196218687
{
public:
	// UniRx.IObservable`1<T> UniRx.Operators.SelectObservable`2::source
	Il2CppObject* ___source_1;
	// System.Func`2<T,TR> UniRx.Operators.SelectObservable`2::selector
	Func_2_t2135783352 * ___selector_2;
	// System.Func`3<T,System.Int32,TR> UniRx.Operators.SelectObservable`2::selectorWithIndex
	Func_3_t3690668578 * ___selectorWithIndex_3;

public:
	inline static int32_t get_offset_of_source_1() { return static_cast<int32_t>(offsetof(SelectObservable_2_t575849553, ___source_1)); }
	inline Il2CppObject* get_source_1() const { return ___source_1; }
	inline Il2CppObject** get_address_of_source_1() { return &___source_1; }
	inline void set_source_1(Il2CppObject* value)
	{
		___source_1 = value;
		Il2CppCodeGenWriteBarrier(&___source_1, value);
	}

	inline static int32_t get_offset_of_selector_2() { return static_cast<int32_t>(offsetof(SelectObservable_2_t575849553, ___selector_2)); }
	inline Func_2_t2135783352 * get_selector_2() const { return ___selector_2; }
	inline Func_2_t2135783352 ** get_address_of_selector_2() { return &___selector_2; }
	inline void set_selector_2(Func_2_t2135783352 * value)
	{
		___selector_2 = value;
		Il2CppCodeGenWriteBarrier(&___selector_2, value);
	}

	inline static int32_t get_offset_of_selectorWithIndex_3() { return static_cast<int32_t>(offsetof(SelectObservable_2_t575849553, ___selectorWithIndex_3)); }
	inline Func_3_t3690668578 * get_selectorWithIndex_3() const { return ___selectorWithIndex_3; }
	inline Func_3_t3690668578 ** get_address_of_selectorWithIndex_3() { return &___selectorWithIndex_3; }
	inline void set_selectorWithIndex_3(Func_3_t3690668578 * value)
	{
		___selectorWithIndex_3 = value;
		Il2CppCodeGenWriteBarrier(&___selectorWithIndex_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
