﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Predicate_1_gen1408070318MethodDeclarations.h"

// System.Void System.Predicate`1<PlayFab.ClientModels.KongregatePlayFabIdPair>::.ctor(System.Object,System.IntPtr)
#define Predicate_1__ctor_m3588925740(__this, ___object0, ___method1, method) ((  void (*) (Predicate_1_t3556145463 *, Il2CppObject *, IntPtr_t, const MethodInfo*))Predicate_1__ctor_m982040097_gshared)(__this, ___object0, ___method1, method)
// System.Boolean System.Predicate`1<PlayFab.ClientModels.KongregatePlayFabIdPair>::Invoke(T)
#define Predicate_1_Invoke_m145800534(__this, ___obj0, method) ((  bool (*) (Predicate_1_t3556145463 *, KongregatePlayFabIdPair_t2985181565 *, const MethodInfo*))Predicate_1_Invoke_m4106178309_gshared)(__this, ___obj0, method)
// System.IAsyncResult System.Predicate`1<PlayFab.ClientModels.KongregatePlayFabIdPair>::BeginInvoke(T,System.AsyncCallback,System.Object)
#define Predicate_1_BeginInvoke_m511957861(__this, ___obj0, ___callback1, ___object2, method) ((  Il2CppObject * (*) (Predicate_1_t3556145463 *, KongregatePlayFabIdPair_t2985181565 *, AsyncCallback_t1363551830 *, Il2CppObject *, const MethodInfo*))Predicate_1_BeginInvoke_m2038073176_gshared)(__this, ___obj0, ___callback1, ___object2, method)
// System.Boolean System.Predicate`1<PlayFab.ClientModels.KongregatePlayFabIdPair>::EndInvoke(System.IAsyncResult)
#define Predicate_1_EndInvoke_m1876449342(__this, ___result0, method) ((  bool (*) (Predicate_1_t3556145463 *, Il2CppObject *, const MethodInfo*))Predicate_1_EndInvoke_m3970497007_gshared)(__this, ___result0, method)
