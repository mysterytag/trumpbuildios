﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.PlayFabClientAPI/LinkAndroidDeviceIDRequestCallback
struct LinkAndroidDeviceIDRequestCallback_t3969310990;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;
// PlayFab.ClientModels.LinkAndroidDeviceIDRequest
struct LinkAndroidDeviceIDRequest_t2075009465;
// System.IAsyncResult
struct IAsyncResult_t537683269;
// System.AsyncCallback
struct AsyncCallback_t1363551830;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_IntPtr676692020.h"
#include "mscorlib_System_String968488902.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_LinkAndroid2075009465.h"
#include "mscorlib_System_AsyncCallback1363551830.h"

// System.Void PlayFab.PlayFabClientAPI/LinkAndroidDeviceIDRequestCallback::.ctor(System.Object,System.IntPtr)
extern "C"  void LinkAndroidDeviceIDRequestCallback__ctor_m24405757 (LinkAndroidDeviceIDRequestCallback_t3969310990 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/LinkAndroidDeviceIDRequestCallback::Invoke(System.String,System.Int32,PlayFab.ClientModels.LinkAndroidDeviceIDRequest,System.Object)
extern "C"  void LinkAndroidDeviceIDRequestCallback_Invoke_m4154473183 (LinkAndroidDeviceIDRequestCallback_t3969310990 * __this, String_t* ___urlPath0, int32_t ___callId1, LinkAndroidDeviceIDRequest_t2075009465 * ___request2, Il2CppObject * ___customData3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
extern "C" void pinvoke_delegate_wrapper_LinkAndroidDeviceIDRequestCallback_t3969310990(Il2CppObject* delegate, String_t* ___urlPath0, int32_t ___callId1, LinkAndroidDeviceIDRequest_t2075009465 * ___request2, Il2CppObject * ___customData3);
// System.IAsyncResult PlayFab.PlayFabClientAPI/LinkAndroidDeviceIDRequestCallback::BeginInvoke(System.String,System.Int32,PlayFab.ClientModels.LinkAndroidDeviceIDRequest,System.Object,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * LinkAndroidDeviceIDRequestCallback_BeginInvoke_m4126265346 (LinkAndroidDeviceIDRequestCallback_t3969310990 * __this, String_t* ___urlPath0, int32_t ___callId1, LinkAndroidDeviceIDRequest_t2075009465 * ___request2, Il2CppObject * ___customData3, AsyncCallback_t1363551830 * ___callback4, Il2CppObject * ___object5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/LinkAndroidDeviceIDRequestCallback::EndInvoke(System.IAsyncResult)
extern "C"  void LinkAndroidDeviceIDRequestCallback_EndInvoke_m3546603405 (LinkAndroidDeviceIDRequestCallback_t3969310990 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
