﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.InternalUtil.ListObserver`1<UniRx.CountChangedStatus>
struct ListObserver_1_t4065845392;
// UniRx.InternalUtil.ImmutableList`1<UniRx.IObserver`1<UniRx.CountChangedStatus>>
struct ImmutableList_1_t2748463827;
// System.Exception
struct Exception_t1967233988;
// UniRx.IObserver`1<UniRx.CountChangedStatus>
struct IObserver_1_t1688259328;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Exception1967233988.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_CountChangedSt3771227721.h"

// System.Void UniRx.InternalUtil.ListObserver`1<UniRx.CountChangedStatus>::.ctor(UniRx.InternalUtil.ImmutableList`1<UniRx.IObserver`1<T>>)
extern "C"  void ListObserver_1__ctor_m900567354_gshared (ListObserver_1_t4065845392 * __this, ImmutableList_1_t2748463827 * ___observers0, const MethodInfo* method);
#define ListObserver_1__ctor_m900567354(__this, ___observers0, method) ((  void (*) (ListObserver_1_t4065845392 *, ImmutableList_1_t2748463827 *, const MethodInfo*))ListObserver_1__ctor_m900567354_gshared)(__this, ___observers0, method)
// System.Void UniRx.InternalUtil.ListObserver`1<UniRx.CountChangedStatus>::OnCompleted()
extern "C"  void ListObserver_1_OnCompleted_m1445354994_gshared (ListObserver_1_t4065845392 * __this, const MethodInfo* method);
#define ListObserver_1_OnCompleted_m1445354994(__this, method) ((  void (*) (ListObserver_1_t4065845392 *, const MethodInfo*))ListObserver_1_OnCompleted_m1445354994_gshared)(__this, method)
// System.Void UniRx.InternalUtil.ListObserver`1<UniRx.CountChangedStatus>::OnError(System.Exception)
extern "C"  void ListObserver_1_OnError_m2064233503_gshared (ListObserver_1_t4065845392 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method);
#define ListObserver_1_OnError_m2064233503(__this, ___error0, method) ((  void (*) (ListObserver_1_t4065845392 *, Exception_t1967233988 *, const MethodInfo*))ListObserver_1_OnError_m2064233503_gshared)(__this, ___error0, method)
// System.Void UniRx.InternalUtil.ListObserver`1<UniRx.CountChangedStatus>::OnNext(T)
extern "C"  void ListObserver_1_OnNext_m1438234896_gshared (ListObserver_1_t4065845392 * __this, int32_t ___value0, const MethodInfo* method);
#define ListObserver_1_OnNext_m1438234896(__this, ___value0, method) ((  void (*) (ListObserver_1_t4065845392 *, int32_t, const MethodInfo*))ListObserver_1_OnNext_m1438234896_gshared)(__this, ___value0, method)
// UniRx.IObserver`1<T> UniRx.InternalUtil.ListObserver`1<UniRx.CountChangedStatus>::Add(UniRx.IObserver`1<T>)
extern "C"  Il2CppObject* ListObserver_1_Add_m3716145008_gshared (ListObserver_1_t4065845392 * __this, Il2CppObject* ___observer0, const MethodInfo* method);
#define ListObserver_1_Add_m3716145008(__this, ___observer0, method) ((  Il2CppObject* (*) (ListObserver_1_t4065845392 *, Il2CppObject*, const MethodInfo*))ListObserver_1_Add_m3716145008_gshared)(__this, ___observer0, method)
// UniRx.IObserver`1<T> UniRx.InternalUtil.ListObserver`1<UniRx.CountChangedStatus>::Remove(UniRx.IObserver`1<T>)
extern "C"  Il2CppObject* ListObserver_1_Remove_m1032357339_gshared (ListObserver_1_t4065845392 * __this, Il2CppObject* ___observer0, const MethodInfo* method);
#define ListObserver_1_Remove_m1032357339(__this, ___observer0, method) ((  Il2CppObject* (*) (ListObserver_1_t4065845392 *, Il2CppObject*, const MethodInfo*))ListObserver_1_Remove_m1032357339_gshared)(__this, ___observer0, method)
