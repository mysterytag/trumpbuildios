﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.SingleAssignmentDisposable
struct SingleAssignmentDisposable_t2336378823;
// System.IDisposable
struct IDisposable_t1628921374;

#include "codegen/il2cpp-codegen.h"

// System.Void UniRx.SingleAssignmentDisposable::.ctor()
extern "C"  void SingleAssignmentDisposable__ctor_m917449122 (SingleAssignmentDisposable_t2336378823 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UniRx.SingleAssignmentDisposable::get_IsDisposed()
extern "C"  bool SingleAssignmentDisposable_get_IsDisposed_m2273201334 (SingleAssignmentDisposable_t2336378823 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IDisposable UniRx.SingleAssignmentDisposable::get_Disposable()
extern "C"  Il2CppObject * SingleAssignmentDisposable_get_Disposable_m2372676678 (SingleAssignmentDisposable_t2336378823 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.SingleAssignmentDisposable::set_Disposable(System.IDisposable)
extern "C"  void SingleAssignmentDisposable_set_Disposable_m1599028885 (SingleAssignmentDisposable_t2336378823 * __this, Il2CppObject * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.SingleAssignmentDisposable::Dispose()
extern "C"  void SingleAssignmentDisposable_Dispose_m1098593631 (SingleAssignmentDisposable_t2336378823 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
