﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Generic.List`1<TutorialTask>
struct List_1_t1570237212;
// TutorialTask
struct TutorialTask_t773278243;
// System.IDisposable
struct IDisposable_t1628921374;
// System.Collections.Generic.Dictionary`2<System.String,System.String>
struct Dictionary_2_t2606186806;
// System.Object
struct Il2CppObject;
// TutorialPlayer
struct TutorialPlayer_t4281139199;

#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera3950987500.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TutorialPlayer/<StartPlay>c__Iterator21
struct  U3CStartPlayU3Ec__Iterator21_t2971317228  : public Il2CppObject
{
public:
	// System.Collections.Generic.List`1<TutorialTask> TutorialPlayer/<StartPlay>c__Iterator21::tasks
	List_1_t1570237212 * ___tasks_0;
	// System.Int32 TutorialPlayer/<StartPlay>c__Iterator21::<tasknum>__0
	int32_t ___U3CtasknumU3E__0_1;
	// System.Boolean TutorialPlayer/<StartPlay>c__Iterator21::<showHand>__1
	bool ___U3CshowHandU3E__1_2;
	// System.Collections.Generic.List`1/Enumerator<TutorialTask> TutorialPlayer/<StartPlay>c__Iterator21::<$s_122>__2
	Enumerator_t3950987500  ___U3CU24s_122U3E__2_3;
	// TutorialTask TutorialPlayer/<StartPlay>c__Iterator21::<task>__3
	TutorialTask_t773278243 * ___U3CtaskU3E__3_4;
	// TutorialTask TutorialPlayer/<StartPlay>c__Iterator21::<task1>__4
	TutorialTask_t773278243 * ___U3Ctask1U3E__4_5;
	// System.IDisposable TutorialPlayer/<StartPlay>c__Iterator21::<disposable>__5
	Il2CppObject * ___U3CdisposableU3E__5_6;
	// System.Collections.Generic.Dictionary`2<System.String,System.String> TutorialPlayer/<StartPlay>c__Iterator21::<purchaseEvent>__6
	Dictionary_2_t2606186806 * ___U3CpurchaseEventU3E__6_7;
	// System.Int32 TutorialPlayer/<StartPlay>c__Iterator21::$PC
	int32_t ___U24PC_8;
	// System.Object TutorialPlayer/<StartPlay>c__Iterator21::$current
	Il2CppObject * ___U24current_9;
	// System.Collections.Generic.List`1<TutorialTask> TutorialPlayer/<StartPlay>c__Iterator21::<$>tasks
	List_1_t1570237212 * ___U3CU24U3Etasks_10;
	// TutorialPlayer TutorialPlayer/<StartPlay>c__Iterator21::<>f__this
	TutorialPlayer_t4281139199 * ___U3CU3Ef__this_11;

public:
	inline static int32_t get_offset_of_tasks_0() { return static_cast<int32_t>(offsetof(U3CStartPlayU3Ec__Iterator21_t2971317228, ___tasks_0)); }
	inline List_1_t1570237212 * get_tasks_0() const { return ___tasks_0; }
	inline List_1_t1570237212 ** get_address_of_tasks_0() { return &___tasks_0; }
	inline void set_tasks_0(List_1_t1570237212 * value)
	{
		___tasks_0 = value;
		Il2CppCodeGenWriteBarrier(&___tasks_0, value);
	}

	inline static int32_t get_offset_of_U3CtasknumU3E__0_1() { return static_cast<int32_t>(offsetof(U3CStartPlayU3Ec__Iterator21_t2971317228, ___U3CtasknumU3E__0_1)); }
	inline int32_t get_U3CtasknumU3E__0_1() const { return ___U3CtasknumU3E__0_1; }
	inline int32_t* get_address_of_U3CtasknumU3E__0_1() { return &___U3CtasknumU3E__0_1; }
	inline void set_U3CtasknumU3E__0_1(int32_t value)
	{
		___U3CtasknumU3E__0_1 = value;
	}

	inline static int32_t get_offset_of_U3CshowHandU3E__1_2() { return static_cast<int32_t>(offsetof(U3CStartPlayU3Ec__Iterator21_t2971317228, ___U3CshowHandU3E__1_2)); }
	inline bool get_U3CshowHandU3E__1_2() const { return ___U3CshowHandU3E__1_2; }
	inline bool* get_address_of_U3CshowHandU3E__1_2() { return &___U3CshowHandU3E__1_2; }
	inline void set_U3CshowHandU3E__1_2(bool value)
	{
		___U3CshowHandU3E__1_2 = value;
	}

	inline static int32_t get_offset_of_U3CU24s_122U3E__2_3() { return static_cast<int32_t>(offsetof(U3CStartPlayU3Ec__Iterator21_t2971317228, ___U3CU24s_122U3E__2_3)); }
	inline Enumerator_t3950987500  get_U3CU24s_122U3E__2_3() const { return ___U3CU24s_122U3E__2_3; }
	inline Enumerator_t3950987500 * get_address_of_U3CU24s_122U3E__2_3() { return &___U3CU24s_122U3E__2_3; }
	inline void set_U3CU24s_122U3E__2_3(Enumerator_t3950987500  value)
	{
		___U3CU24s_122U3E__2_3 = value;
	}

	inline static int32_t get_offset_of_U3CtaskU3E__3_4() { return static_cast<int32_t>(offsetof(U3CStartPlayU3Ec__Iterator21_t2971317228, ___U3CtaskU3E__3_4)); }
	inline TutorialTask_t773278243 * get_U3CtaskU3E__3_4() const { return ___U3CtaskU3E__3_4; }
	inline TutorialTask_t773278243 ** get_address_of_U3CtaskU3E__3_4() { return &___U3CtaskU3E__3_4; }
	inline void set_U3CtaskU3E__3_4(TutorialTask_t773278243 * value)
	{
		___U3CtaskU3E__3_4 = value;
		Il2CppCodeGenWriteBarrier(&___U3CtaskU3E__3_4, value);
	}

	inline static int32_t get_offset_of_U3Ctask1U3E__4_5() { return static_cast<int32_t>(offsetof(U3CStartPlayU3Ec__Iterator21_t2971317228, ___U3Ctask1U3E__4_5)); }
	inline TutorialTask_t773278243 * get_U3Ctask1U3E__4_5() const { return ___U3Ctask1U3E__4_5; }
	inline TutorialTask_t773278243 ** get_address_of_U3Ctask1U3E__4_5() { return &___U3Ctask1U3E__4_5; }
	inline void set_U3Ctask1U3E__4_5(TutorialTask_t773278243 * value)
	{
		___U3Ctask1U3E__4_5 = value;
		Il2CppCodeGenWriteBarrier(&___U3Ctask1U3E__4_5, value);
	}

	inline static int32_t get_offset_of_U3CdisposableU3E__5_6() { return static_cast<int32_t>(offsetof(U3CStartPlayU3Ec__Iterator21_t2971317228, ___U3CdisposableU3E__5_6)); }
	inline Il2CppObject * get_U3CdisposableU3E__5_6() const { return ___U3CdisposableU3E__5_6; }
	inline Il2CppObject ** get_address_of_U3CdisposableU3E__5_6() { return &___U3CdisposableU3E__5_6; }
	inline void set_U3CdisposableU3E__5_6(Il2CppObject * value)
	{
		___U3CdisposableU3E__5_6 = value;
		Il2CppCodeGenWriteBarrier(&___U3CdisposableU3E__5_6, value);
	}

	inline static int32_t get_offset_of_U3CpurchaseEventU3E__6_7() { return static_cast<int32_t>(offsetof(U3CStartPlayU3Ec__Iterator21_t2971317228, ___U3CpurchaseEventU3E__6_7)); }
	inline Dictionary_2_t2606186806 * get_U3CpurchaseEventU3E__6_7() const { return ___U3CpurchaseEventU3E__6_7; }
	inline Dictionary_2_t2606186806 ** get_address_of_U3CpurchaseEventU3E__6_7() { return &___U3CpurchaseEventU3E__6_7; }
	inline void set_U3CpurchaseEventU3E__6_7(Dictionary_2_t2606186806 * value)
	{
		___U3CpurchaseEventU3E__6_7 = value;
		Il2CppCodeGenWriteBarrier(&___U3CpurchaseEventU3E__6_7, value);
	}

	inline static int32_t get_offset_of_U24PC_8() { return static_cast<int32_t>(offsetof(U3CStartPlayU3Ec__Iterator21_t2971317228, ___U24PC_8)); }
	inline int32_t get_U24PC_8() const { return ___U24PC_8; }
	inline int32_t* get_address_of_U24PC_8() { return &___U24PC_8; }
	inline void set_U24PC_8(int32_t value)
	{
		___U24PC_8 = value;
	}

	inline static int32_t get_offset_of_U24current_9() { return static_cast<int32_t>(offsetof(U3CStartPlayU3Ec__Iterator21_t2971317228, ___U24current_9)); }
	inline Il2CppObject * get_U24current_9() const { return ___U24current_9; }
	inline Il2CppObject ** get_address_of_U24current_9() { return &___U24current_9; }
	inline void set_U24current_9(Il2CppObject * value)
	{
		___U24current_9 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_9, value);
	}

	inline static int32_t get_offset_of_U3CU24U3Etasks_10() { return static_cast<int32_t>(offsetof(U3CStartPlayU3Ec__Iterator21_t2971317228, ___U3CU24U3Etasks_10)); }
	inline List_1_t1570237212 * get_U3CU24U3Etasks_10() const { return ___U3CU24U3Etasks_10; }
	inline List_1_t1570237212 ** get_address_of_U3CU24U3Etasks_10() { return &___U3CU24U3Etasks_10; }
	inline void set_U3CU24U3Etasks_10(List_1_t1570237212 * value)
	{
		___U3CU24U3Etasks_10 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU24U3Etasks_10, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__this_11() { return static_cast<int32_t>(offsetof(U3CStartPlayU3Ec__Iterator21_t2971317228, ___U3CU3Ef__this_11)); }
	inline TutorialPlayer_t4281139199 * get_U3CU3Ef__this_11() const { return ___U3CU3Ef__this_11; }
	inline TutorialPlayer_t4281139199 ** get_address_of_U3CU3Ef__this_11() { return &___U3CU3Ef__this_11; }
	inline void set_U3CU3Ef__this_11(TutorialPlayer_t4281139199 * value)
	{
		___U3CU3Ef__this_11 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__this_11, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
