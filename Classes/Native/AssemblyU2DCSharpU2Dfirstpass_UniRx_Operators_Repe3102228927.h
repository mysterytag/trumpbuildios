﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UniRx.IObserver`1<System.Object>
struct IObserver_1_t3049105323;
// UniRx.Operators.RepeatObservable`1<System.Object>
struct RepeatObservable_1_t4255784085;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Operators.RepeatObservable`1/<SubscribeCore>c__AnonStorey66<System.Object>
struct  U3CSubscribeCoreU3Ec__AnonStorey66_t3102228927  : public Il2CppObject
{
public:
	// UniRx.IObserver`1<T> UniRx.Operators.RepeatObservable`1/<SubscribeCore>c__AnonStorey66::observer
	Il2CppObject* ___observer_0;
	// UniRx.Operators.RepeatObservable`1<T> UniRx.Operators.RepeatObservable`1/<SubscribeCore>c__AnonStorey66::<>f__this
	RepeatObservable_1_t4255784085 * ___U3CU3Ef__this_1;

public:
	inline static int32_t get_offset_of_observer_0() { return static_cast<int32_t>(offsetof(U3CSubscribeCoreU3Ec__AnonStorey66_t3102228927, ___observer_0)); }
	inline Il2CppObject* get_observer_0() const { return ___observer_0; }
	inline Il2CppObject** get_address_of_observer_0() { return &___observer_0; }
	inline void set_observer_0(Il2CppObject* value)
	{
		___observer_0 = value;
		Il2CppCodeGenWriteBarrier(&___observer_0, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__this_1() { return static_cast<int32_t>(offsetof(U3CSubscribeCoreU3Ec__AnonStorey66_t3102228927, ___U3CU3Ef__this_1)); }
	inline RepeatObservable_1_t4255784085 * get_U3CU3Ef__this_1() const { return ___U3CU3Ef__this_1; }
	inline RepeatObservable_1_t4255784085 ** get_address_of_U3CU3Ef__this_1() { return &___U3CU3Ef__this_1; }
	inline void set_U3CU3Ef__this_1(RepeatObservable_1_t4255784085 * value)
	{
		___U3CU3Ef__this_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__this_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
