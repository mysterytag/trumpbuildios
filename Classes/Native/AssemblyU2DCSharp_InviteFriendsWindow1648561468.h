﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Tacticsoft.TableView
struct TableView_t692333993;
// UnityEngine.GameObject
struct GameObject_t4012695102;
// GenericDataSource`2<FriendInfo,FriendView>
struct GenericDataSource_2_t4294355128;
// System.Action`1<FriendInfo>
struct Action_1_t384923117;

#include "AssemblyU2DCSharp_WindowBase3855465217.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// InviteFriendsWindow
struct  InviteFriendsWindow_t1648561468  : public WindowBase_t3855465217
{
public:
	// Tacticsoft.TableView InviteFriendsWindow::table
	TableView_t692333993 * ___table_6;
	// UnityEngine.GameObject InviteFriendsWindow::cellPrefab
	GameObject_t4012695102 * ___cellPrefab_7;
	// GenericDataSource`2<FriendInfo,FriendView> InviteFriendsWindow::source
	GenericDataSource_2_t4294355128 * ___source_8;
	// System.Action`1<FriendInfo> InviteFriendsWindow::inviteRequested
	Action_1_t384923117 * ___inviteRequested_9;

public:
	inline static int32_t get_offset_of_table_6() { return static_cast<int32_t>(offsetof(InviteFriendsWindow_t1648561468, ___table_6)); }
	inline TableView_t692333993 * get_table_6() const { return ___table_6; }
	inline TableView_t692333993 ** get_address_of_table_6() { return &___table_6; }
	inline void set_table_6(TableView_t692333993 * value)
	{
		___table_6 = value;
		Il2CppCodeGenWriteBarrier(&___table_6, value);
	}

	inline static int32_t get_offset_of_cellPrefab_7() { return static_cast<int32_t>(offsetof(InviteFriendsWindow_t1648561468, ___cellPrefab_7)); }
	inline GameObject_t4012695102 * get_cellPrefab_7() const { return ___cellPrefab_7; }
	inline GameObject_t4012695102 ** get_address_of_cellPrefab_7() { return &___cellPrefab_7; }
	inline void set_cellPrefab_7(GameObject_t4012695102 * value)
	{
		___cellPrefab_7 = value;
		Il2CppCodeGenWriteBarrier(&___cellPrefab_7, value);
	}

	inline static int32_t get_offset_of_source_8() { return static_cast<int32_t>(offsetof(InviteFriendsWindow_t1648561468, ___source_8)); }
	inline GenericDataSource_2_t4294355128 * get_source_8() const { return ___source_8; }
	inline GenericDataSource_2_t4294355128 ** get_address_of_source_8() { return &___source_8; }
	inline void set_source_8(GenericDataSource_2_t4294355128 * value)
	{
		___source_8 = value;
		Il2CppCodeGenWriteBarrier(&___source_8, value);
	}

	inline static int32_t get_offset_of_inviteRequested_9() { return static_cast<int32_t>(offsetof(InviteFriendsWindow_t1648561468, ___inviteRequested_9)); }
	inline Action_1_t384923117 * get_inviteRequested_9() const { return ___inviteRequested_9; }
	inline Action_1_t384923117 ** get_address_of_inviteRequested_9() { return &___inviteRequested_9; }
	inline void set_inviteRequested_9(Action_1_t384923117 * value)
	{
		___inviteRequested_9 = value;
		Il2CppCodeGenWriteBarrier(&___inviteRequested_9, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
