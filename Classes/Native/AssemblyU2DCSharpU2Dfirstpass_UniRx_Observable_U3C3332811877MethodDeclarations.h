﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Observable/<AddRef>c__AnonStorey37`1<System.Object>
struct U3CAddRefU3Ec__AnonStorey37_1_t3332811877;
// System.IDisposable
struct IDisposable_t1628921374;
// UniRx.IObserver`1<System.Object>
struct IObserver_1_t3049105323;

#include "codegen/il2cpp-codegen.h"

// System.Void UniRx.Observable/<AddRef>c__AnonStorey37`1<System.Object>::.ctor()
extern "C"  void U3CAddRefU3Ec__AnonStorey37_1__ctor_m3687565875_gshared (U3CAddRefU3Ec__AnonStorey37_1_t3332811877 * __this, const MethodInfo* method);
#define U3CAddRefU3Ec__AnonStorey37_1__ctor_m3687565875(__this, method) ((  void (*) (U3CAddRefU3Ec__AnonStorey37_1_t3332811877 *, const MethodInfo*))U3CAddRefU3Ec__AnonStorey37_1__ctor_m3687565875_gshared)(__this, method)
// System.IDisposable UniRx.Observable/<AddRef>c__AnonStorey37`1<System.Object>::<>m__3B(UniRx.IObserver`1<T>)
extern "C"  Il2CppObject * U3CAddRefU3Ec__AnonStorey37_1_U3CU3Em__3B_m4227423448_gshared (U3CAddRefU3Ec__AnonStorey37_1_t3332811877 * __this, Il2CppObject* ___observer0, const MethodInfo* method);
#define U3CAddRefU3Ec__AnonStorey37_1_U3CU3Em__3B_m4227423448(__this, ___observer0, method) ((  Il2CppObject * (*) (U3CAddRefU3Ec__AnonStorey37_1_t3332811877 *, Il2CppObject*, const MethodInfo*))U3CAddRefU3Ec__AnonStorey37_1_U3CU3Em__3B_m4227423448_gshared)(__this, ___observer0, method)
