﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.TextMesh
struct TextMesh_t583678247;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String968488902.h"
#include "UnityEngine_UnityEngine_Color1588175760.h"

// System.Void UnityEngine.TextMesh::.ctor()
extern "C"  void TextMesh__ctor_m3932301811 (TextMesh_t583678247 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String UnityEngine.TextMesh::get_text()
extern "C"  String_t* TextMesh_get_text_m2892967978 (TextMesh_t583678247 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.TextMesh::set_text(System.String)
extern "C"  void TextMesh_set_text_m3628430759 (TextMesh_t583678247 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.TextMesh::set_color(UnityEngine.Color)
extern "C"  void TextMesh_set_color_m115266575 (TextMesh_t583678247 * __this, Color_t1588175760  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.TextMesh::INTERNAL_set_color(UnityEngine.Color&)
extern "C"  void TextMesh_INTERNAL_set_color_m2700543291 (TextMesh_t583678247 * __this, Color_t1588175760 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
