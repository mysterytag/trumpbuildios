﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// System.Collections.Generic.List`1<System.String>
struct List_1_t1765447871;
// PlayFab.ClientModels.UserFacebookInfo
struct UserFacebookInfo_t1491589167;
// PlayFab.ClientModels.UserSteamInfo
struct UserSteamInfo_t2867463811;
// PlayFab.ClientModels.UserGameCenterInfo
struct UserGameCenterInfo_t386684176;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PlayFab.ClientModels.FriendInfo
struct  FriendInfo_t3708701852  : public Il2CppObject
{
public:
	// System.String PlayFab.ClientModels.FriendInfo::<FriendPlayFabId>k__BackingField
	String_t* ___U3CFriendPlayFabIdU3Ek__BackingField_0;
	// System.String PlayFab.ClientModels.FriendInfo::<Username>k__BackingField
	String_t* ___U3CUsernameU3Ek__BackingField_1;
	// System.String PlayFab.ClientModels.FriendInfo::<TitleDisplayName>k__BackingField
	String_t* ___U3CTitleDisplayNameU3Ek__BackingField_2;
	// System.Collections.Generic.List`1<System.String> PlayFab.ClientModels.FriendInfo::<Tags>k__BackingField
	List_1_t1765447871 * ___U3CTagsU3Ek__BackingField_3;
	// System.String PlayFab.ClientModels.FriendInfo::<CurrentMatchmakerLobbyId>k__BackingField
	String_t* ___U3CCurrentMatchmakerLobbyIdU3Ek__BackingField_4;
	// PlayFab.ClientModels.UserFacebookInfo PlayFab.ClientModels.FriendInfo::<FacebookInfo>k__BackingField
	UserFacebookInfo_t1491589167 * ___U3CFacebookInfoU3Ek__BackingField_5;
	// PlayFab.ClientModels.UserSteamInfo PlayFab.ClientModels.FriendInfo::<SteamInfo>k__BackingField
	UserSteamInfo_t2867463811 * ___U3CSteamInfoU3Ek__BackingField_6;
	// PlayFab.ClientModels.UserGameCenterInfo PlayFab.ClientModels.FriendInfo::<GameCenterInfo>k__BackingField
	UserGameCenterInfo_t386684176 * ___U3CGameCenterInfoU3Ek__BackingField_7;

public:
	inline static int32_t get_offset_of_U3CFriendPlayFabIdU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(FriendInfo_t3708701852, ___U3CFriendPlayFabIdU3Ek__BackingField_0)); }
	inline String_t* get_U3CFriendPlayFabIdU3Ek__BackingField_0() const { return ___U3CFriendPlayFabIdU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CFriendPlayFabIdU3Ek__BackingField_0() { return &___U3CFriendPlayFabIdU3Ek__BackingField_0; }
	inline void set_U3CFriendPlayFabIdU3Ek__BackingField_0(String_t* value)
	{
		___U3CFriendPlayFabIdU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier(&___U3CFriendPlayFabIdU3Ek__BackingField_0, value);
	}

	inline static int32_t get_offset_of_U3CUsernameU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(FriendInfo_t3708701852, ___U3CUsernameU3Ek__BackingField_1)); }
	inline String_t* get_U3CUsernameU3Ek__BackingField_1() const { return ___U3CUsernameU3Ek__BackingField_1; }
	inline String_t** get_address_of_U3CUsernameU3Ek__BackingField_1() { return &___U3CUsernameU3Ek__BackingField_1; }
	inline void set_U3CUsernameU3Ek__BackingField_1(String_t* value)
	{
		___U3CUsernameU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CUsernameU3Ek__BackingField_1, value);
	}

	inline static int32_t get_offset_of_U3CTitleDisplayNameU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(FriendInfo_t3708701852, ___U3CTitleDisplayNameU3Ek__BackingField_2)); }
	inline String_t* get_U3CTitleDisplayNameU3Ek__BackingField_2() const { return ___U3CTitleDisplayNameU3Ek__BackingField_2; }
	inline String_t** get_address_of_U3CTitleDisplayNameU3Ek__BackingField_2() { return &___U3CTitleDisplayNameU3Ek__BackingField_2; }
	inline void set_U3CTitleDisplayNameU3Ek__BackingField_2(String_t* value)
	{
		___U3CTitleDisplayNameU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier(&___U3CTitleDisplayNameU3Ek__BackingField_2, value);
	}

	inline static int32_t get_offset_of_U3CTagsU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(FriendInfo_t3708701852, ___U3CTagsU3Ek__BackingField_3)); }
	inline List_1_t1765447871 * get_U3CTagsU3Ek__BackingField_3() const { return ___U3CTagsU3Ek__BackingField_3; }
	inline List_1_t1765447871 ** get_address_of_U3CTagsU3Ek__BackingField_3() { return &___U3CTagsU3Ek__BackingField_3; }
	inline void set_U3CTagsU3Ek__BackingField_3(List_1_t1765447871 * value)
	{
		___U3CTagsU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier(&___U3CTagsU3Ek__BackingField_3, value);
	}

	inline static int32_t get_offset_of_U3CCurrentMatchmakerLobbyIdU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(FriendInfo_t3708701852, ___U3CCurrentMatchmakerLobbyIdU3Ek__BackingField_4)); }
	inline String_t* get_U3CCurrentMatchmakerLobbyIdU3Ek__BackingField_4() const { return ___U3CCurrentMatchmakerLobbyIdU3Ek__BackingField_4; }
	inline String_t** get_address_of_U3CCurrentMatchmakerLobbyIdU3Ek__BackingField_4() { return &___U3CCurrentMatchmakerLobbyIdU3Ek__BackingField_4; }
	inline void set_U3CCurrentMatchmakerLobbyIdU3Ek__BackingField_4(String_t* value)
	{
		___U3CCurrentMatchmakerLobbyIdU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier(&___U3CCurrentMatchmakerLobbyIdU3Ek__BackingField_4, value);
	}

	inline static int32_t get_offset_of_U3CFacebookInfoU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(FriendInfo_t3708701852, ___U3CFacebookInfoU3Ek__BackingField_5)); }
	inline UserFacebookInfo_t1491589167 * get_U3CFacebookInfoU3Ek__BackingField_5() const { return ___U3CFacebookInfoU3Ek__BackingField_5; }
	inline UserFacebookInfo_t1491589167 ** get_address_of_U3CFacebookInfoU3Ek__BackingField_5() { return &___U3CFacebookInfoU3Ek__BackingField_5; }
	inline void set_U3CFacebookInfoU3Ek__BackingField_5(UserFacebookInfo_t1491589167 * value)
	{
		___U3CFacebookInfoU3Ek__BackingField_5 = value;
		Il2CppCodeGenWriteBarrier(&___U3CFacebookInfoU3Ek__BackingField_5, value);
	}

	inline static int32_t get_offset_of_U3CSteamInfoU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(FriendInfo_t3708701852, ___U3CSteamInfoU3Ek__BackingField_6)); }
	inline UserSteamInfo_t2867463811 * get_U3CSteamInfoU3Ek__BackingField_6() const { return ___U3CSteamInfoU3Ek__BackingField_6; }
	inline UserSteamInfo_t2867463811 ** get_address_of_U3CSteamInfoU3Ek__BackingField_6() { return &___U3CSteamInfoU3Ek__BackingField_6; }
	inline void set_U3CSteamInfoU3Ek__BackingField_6(UserSteamInfo_t2867463811 * value)
	{
		___U3CSteamInfoU3Ek__BackingField_6 = value;
		Il2CppCodeGenWriteBarrier(&___U3CSteamInfoU3Ek__BackingField_6, value);
	}

	inline static int32_t get_offset_of_U3CGameCenterInfoU3Ek__BackingField_7() { return static_cast<int32_t>(offsetof(FriendInfo_t3708701852, ___U3CGameCenterInfoU3Ek__BackingField_7)); }
	inline UserGameCenterInfo_t386684176 * get_U3CGameCenterInfoU3Ek__BackingField_7() const { return ___U3CGameCenterInfoU3Ek__BackingField_7; }
	inline UserGameCenterInfo_t386684176 ** get_address_of_U3CGameCenterInfoU3Ek__BackingField_7() { return &___U3CGameCenterInfoU3Ek__BackingField_7; }
	inline void set_U3CGameCenterInfoU3Ek__BackingField_7(UserGameCenterInfo_t386684176 * value)
	{
		___U3CGameCenterInfoU3Ek__BackingField_7 = value;
		Il2CppCodeGenWriteBarrier(&___U3CGameCenterInfoU3Ek__BackingField_7, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
