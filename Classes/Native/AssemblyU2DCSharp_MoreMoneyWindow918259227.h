﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// TableButtons
struct TableButtons_t1868573683;
// UnityEngine.UI.Text
struct Text_t3286458198;
// UnityEngine.UI.Image
struct Image_t3354615620;
// System.Action
struct Action_t437523947;
// MoneyPackInfo
struct MoneyPackInfo_t3013049127;
// IconsProvider
struct IconsProvider_t3884200715;
// GlobalStat
struct GlobalStat_t1134678199;

#include "AssemblyU2DCSharp_WindowBase3855465217.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MoreMoneyWindow
struct  MoreMoneyWindow_t918259227  : public WindowBase_t3855465217
{
public:
	// TableButtons MoreMoneyWindow::tableButtons
	TableButtons_t1868573683 * ___tableButtons_6;
	// UnityEngine.UI.Text MoreMoneyWindow::name
	Text_t3286458198 * ___name_7;
	// UnityEngine.UI.Image MoreMoneyWindow::icon
	Image_t3354615620 * ___icon_8;
	// UnityEngine.UI.Text MoreMoneyWindow::price
	Text_t3286458198 * ___price_9;
	// System.Action MoreMoneyWindow::callback
	Action_t437523947 * ___callback_10;
	// MoneyPackInfo MoreMoneyWindow::info
	MoneyPackInfo_t3013049127 * ___info_11;
	// IconsProvider MoreMoneyWindow::IconsProvider
	IconsProvider_t3884200715 * ___IconsProvider_12;
	// GlobalStat MoreMoneyWindow::GlobalStat
	GlobalStat_t1134678199 * ___GlobalStat_13;

public:
	inline static int32_t get_offset_of_tableButtons_6() { return static_cast<int32_t>(offsetof(MoreMoneyWindow_t918259227, ___tableButtons_6)); }
	inline TableButtons_t1868573683 * get_tableButtons_6() const { return ___tableButtons_6; }
	inline TableButtons_t1868573683 ** get_address_of_tableButtons_6() { return &___tableButtons_6; }
	inline void set_tableButtons_6(TableButtons_t1868573683 * value)
	{
		___tableButtons_6 = value;
		Il2CppCodeGenWriteBarrier(&___tableButtons_6, value);
	}

	inline static int32_t get_offset_of_name_7() { return static_cast<int32_t>(offsetof(MoreMoneyWindow_t918259227, ___name_7)); }
	inline Text_t3286458198 * get_name_7() const { return ___name_7; }
	inline Text_t3286458198 ** get_address_of_name_7() { return &___name_7; }
	inline void set_name_7(Text_t3286458198 * value)
	{
		___name_7 = value;
		Il2CppCodeGenWriteBarrier(&___name_7, value);
	}

	inline static int32_t get_offset_of_icon_8() { return static_cast<int32_t>(offsetof(MoreMoneyWindow_t918259227, ___icon_8)); }
	inline Image_t3354615620 * get_icon_8() const { return ___icon_8; }
	inline Image_t3354615620 ** get_address_of_icon_8() { return &___icon_8; }
	inline void set_icon_8(Image_t3354615620 * value)
	{
		___icon_8 = value;
		Il2CppCodeGenWriteBarrier(&___icon_8, value);
	}

	inline static int32_t get_offset_of_price_9() { return static_cast<int32_t>(offsetof(MoreMoneyWindow_t918259227, ___price_9)); }
	inline Text_t3286458198 * get_price_9() const { return ___price_9; }
	inline Text_t3286458198 ** get_address_of_price_9() { return &___price_9; }
	inline void set_price_9(Text_t3286458198 * value)
	{
		___price_9 = value;
		Il2CppCodeGenWriteBarrier(&___price_9, value);
	}

	inline static int32_t get_offset_of_callback_10() { return static_cast<int32_t>(offsetof(MoreMoneyWindow_t918259227, ___callback_10)); }
	inline Action_t437523947 * get_callback_10() const { return ___callback_10; }
	inline Action_t437523947 ** get_address_of_callback_10() { return &___callback_10; }
	inline void set_callback_10(Action_t437523947 * value)
	{
		___callback_10 = value;
		Il2CppCodeGenWriteBarrier(&___callback_10, value);
	}

	inline static int32_t get_offset_of_info_11() { return static_cast<int32_t>(offsetof(MoreMoneyWindow_t918259227, ___info_11)); }
	inline MoneyPackInfo_t3013049127 * get_info_11() const { return ___info_11; }
	inline MoneyPackInfo_t3013049127 ** get_address_of_info_11() { return &___info_11; }
	inline void set_info_11(MoneyPackInfo_t3013049127 * value)
	{
		___info_11 = value;
		Il2CppCodeGenWriteBarrier(&___info_11, value);
	}

	inline static int32_t get_offset_of_IconsProvider_12() { return static_cast<int32_t>(offsetof(MoreMoneyWindow_t918259227, ___IconsProvider_12)); }
	inline IconsProvider_t3884200715 * get_IconsProvider_12() const { return ___IconsProvider_12; }
	inline IconsProvider_t3884200715 ** get_address_of_IconsProvider_12() { return &___IconsProvider_12; }
	inline void set_IconsProvider_12(IconsProvider_t3884200715 * value)
	{
		___IconsProvider_12 = value;
		Il2CppCodeGenWriteBarrier(&___IconsProvider_12, value);
	}

	inline static int32_t get_offset_of_GlobalStat_13() { return static_cast<int32_t>(offsetof(MoreMoneyWindow_t918259227, ___GlobalStat_13)); }
	inline GlobalStat_t1134678199 * get_GlobalStat_13() const { return ___GlobalStat_13; }
	inline GlobalStat_t1134678199 ** get_address_of_GlobalStat_13() { return &___GlobalStat_13; }
	inline void set_GlobalStat_13(GlobalStat_t1134678199 * value)
	{
		___GlobalStat_13 = value;
		Il2CppCodeGenWriteBarrier(&___GlobalStat_13, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
