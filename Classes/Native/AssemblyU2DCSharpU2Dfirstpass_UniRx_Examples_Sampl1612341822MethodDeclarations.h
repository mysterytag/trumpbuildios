﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Examples.Sample06_ConvertToCoroutine/<LazyTaskTest>c__Iterator8
struct U3CLazyTaskTestU3Ec__Iterator8_t1612341822;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void UniRx.Examples.Sample06_ConvertToCoroutine/<LazyTaskTest>c__Iterator8::.ctor()
extern "C"  void U3CLazyTaskTestU3Ec__Iterator8__ctor_m244270134 (U3CLazyTaskTestU3Ec__Iterator8_t1612341822 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object UniRx.Examples.Sample06_ConvertToCoroutine/<LazyTaskTest>c__Iterator8::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CLazyTaskTestU3Ec__Iterator8_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3516293020 (U3CLazyTaskTestU3Ec__Iterator8_t1612341822 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object UniRx.Examples.Sample06_ConvertToCoroutine/<LazyTaskTest>c__Iterator8::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CLazyTaskTestU3Ec__Iterator8_System_Collections_IEnumerator_get_Current_m642980656 (U3CLazyTaskTestU3Ec__Iterator8_t1612341822 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UniRx.Examples.Sample06_ConvertToCoroutine/<LazyTaskTest>c__Iterator8::MoveNext()
extern "C"  bool U3CLazyTaskTestU3Ec__Iterator8_MoveNext_m1380846358 (U3CLazyTaskTestU3Ec__Iterator8_t1612341822 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.Examples.Sample06_ConvertToCoroutine/<LazyTaskTest>c__Iterator8::Dispose()
extern "C"  void U3CLazyTaskTestU3Ec__Iterator8_Dispose_m2713647859 (U3CLazyTaskTestU3Ec__Iterator8_t1612341822 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.Examples.Sample06_ConvertToCoroutine/<LazyTaskTest>c__Iterator8::Reset()
extern "C"  void U3CLazyTaskTestU3Ec__Iterator8_Reset_m2185670371 (U3CLazyTaskTestU3Ec__Iterator8_t1612341822 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UniRx.Examples.Sample06_ConvertToCoroutine/<LazyTaskTest>c__Iterator8::<>m__1E()
extern "C"  int32_t U3CLazyTaskTestU3Ec__Iterator8_U3CU3Em__1E_m4042962089 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
