﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Observable/ConnectableObservable`1/Connection<System.Object>
struct Connection_t217434055;
// UniRx.Observable/ConnectableObservable`1<System.Object>
struct ConnectableObservable_1_t608944977;
// System.IDisposable
struct IDisposable_t1628921374;

#include "codegen/il2cpp-codegen.h"

// System.Void UniRx.Observable/ConnectableObservable`1/Connection<System.Object>::.ctor(UniRx.Observable/ConnectableObservable`1<T>,System.IDisposable)
extern "C"  void Connection__ctor_m4285632808_gshared (Connection_t217434055 * __this, ConnectableObservable_1_t608944977 * ___parent0, Il2CppObject * ___subscription1, const MethodInfo* method);
#define Connection__ctor_m4285632808(__this, ___parent0, ___subscription1, method) ((  void (*) (Connection_t217434055 *, ConnectableObservable_1_t608944977 *, Il2CppObject *, const MethodInfo*))Connection__ctor_m4285632808_gshared)(__this, ___parent0, ___subscription1, method)
// System.Void UniRx.Observable/ConnectableObservable`1/Connection<System.Object>::Dispose()
extern "C"  void Connection_Dispose_m1210712569_gshared (Connection_t217434055 * __this, const MethodInfo* method);
#define Connection_Dispose_m1210712569(__this, method) ((  void (*) (Connection_t217434055 *, const MethodInfo*))Connection_Dispose_m1210712569_gshared)(__this, method)
