﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Zenject.MonoInstaller
struct MonoInstaller_t3807575866;
// Zenject.DiContainer
struct DiContainer_t2383114449;

#include "codegen/il2cpp-codegen.h"

// System.Void Zenject.MonoInstaller::.ctor()
extern "C"  void MonoInstaller__ctor_m3200056357 (MonoInstaller_t3807575866 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Zenject.DiContainer Zenject.MonoInstaller::get_Container()
extern "C"  DiContainer_t2383114449 * MonoInstaller_get_Container_m519718385 (MonoInstaller_t3807575866 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Zenject.MonoInstaller::get_IsEnabled()
extern "C"  bool MonoInstaller_get_IsEnabled_m496489605 (MonoInstaller_t3807575866 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Zenject.MonoInstaller::Start()
extern "C"  void MonoInstaller_Start_m2147194149 (MonoInstaller_t3807575866 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
