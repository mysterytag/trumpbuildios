﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// IStream`1<System.Object>
struct IStream_1_t1389797411;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// StreamAPI/<FirstOnly>c__AnonStorey136`1<System.Object>
struct  U3CFirstOnlyU3Ec__AnonStorey136_1_t276735029  : public Il2CppObject
{
public:
	// IStream`1<T> StreamAPI/<FirstOnly>c__AnonStorey136`1::stream
	Il2CppObject* ___stream_0;

public:
	inline static int32_t get_offset_of_stream_0() { return static_cast<int32_t>(offsetof(U3CFirstOnlyU3Ec__AnonStorey136_1_t276735029, ___stream_0)); }
	inline Il2CppObject* get_stream_0() const { return ___stream_0; }
	inline Il2CppObject** get_address_of_stream_0() { return &___stream_0; }
	inline void set_stream_0(Il2CppObject* value)
	{
		___stream_0 = value;
		Il2CppCodeGenWriteBarrier(&___stream_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
