﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.ContinueWithObservable`2/ContinueWith<System.Object,System.Object>
struct ContinueWith_t1869588638;
// UniRx.Operators.ContinueWithObservable`2<System.Object,System.Object>
struct ContinueWithObservable_2_t4007622692;
// UniRx.IObserver`1<System.Object>
struct IObserver_1_t3049105323;
// System.IDisposable
struct IDisposable_t1628921374;
// System.Object
struct Il2CppObject;
// System.Exception
struct Exception_t1967233988;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_Exception1967233988.h"

// System.Void UniRx.Operators.ContinueWithObservable`2/ContinueWith<System.Object,System.Object>::.ctor(UniRx.Operators.ContinueWithObservable`2<TSource,TResult>,UniRx.IObserver`1<TResult>,System.IDisposable)
extern "C"  void ContinueWith__ctor_m4278276356_gshared (ContinueWith_t1869588638 * __this, ContinueWithObservable_2_t4007622692 * ___parent0, Il2CppObject* ___observer1, Il2CppObject * ___cancel2, const MethodInfo* method);
#define ContinueWith__ctor_m4278276356(__this, ___parent0, ___observer1, ___cancel2, method) ((  void (*) (ContinueWith_t1869588638 *, ContinueWithObservable_2_t4007622692 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))ContinueWith__ctor_m4278276356_gshared)(__this, ___parent0, ___observer1, ___cancel2, method)
// System.IDisposable UniRx.Operators.ContinueWithObservable`2/ContinueWith<System.Object,System.Object>::Run()
extern "C"  Il2CppObject * ContinueWith_Run_m3777762166_gshared (ContinueWith_t1869588638 * __this, const MethodInfo* method);
#define ContinueWith_Run_m3777762166(__this, method) ((  Il2CppObject * (*) (ContinueWith_t1869588638 *, const MethodInfo*))ContinueWith_Run_m3777762166_gshared)(__this, method)
// System.Void UniRx.Operators.ContinueWithObservable`2/ContinueWith<System.Object,System.Object>::OnNext(TSource)
extern "C"  void ContinueWith_OnNext_m2111425205_gshared (ContinueWith_t1869588638 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define ContinueWith_OnNext_m2111425205(__this, ___value0, method) ((  void (*) (ContinueWith_t1869588638 *, Il2CppObject *, const MethodInfo*))ContinueWith_OnNext_m2111425205_gshared)(__this, ___value0, method)
// System.Void UniRx.Operators.ContinueWithObservable`2/ContinueWith<System.Object,System.Object>::OnError(System.Exception)
extern "C"  void ContinueWith_OnError_m2584767455_gshared (ContinueWith_t1869588638 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method);
#define ContinueWith_OnError_m2584767455(__this, ___error0, method) ((  void (*) (ContinueWith_t1869588638 *, Exception_t1967233988 *, const MethodInfo*))ContinueWith_OnError_m2584767455_gshared)(__this, ___error0, method)
// System.Void UniRx.Operators.ContinueWithObservable`2/ContinueWith<System.Object,System.Object>::OnCompleted()
extern "C"  void ContinueWith_OnCompleted_m4256347570_gshared (ContinueWith_t1869588638 * __this, const MethodInfo* method);
#define ContinueWith_OnCompleted_m4256347570(__this, method) ((  void (*) (ContinueWith_t1869588638 *, const MethodInfo*))ContinueWith_OnCompleted_m4256347570_gshared)(__this, method)
