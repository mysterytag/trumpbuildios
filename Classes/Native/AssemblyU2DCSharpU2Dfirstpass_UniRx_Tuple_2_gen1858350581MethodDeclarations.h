﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Tuple_2_gen369261819MethodDeclarations.h"

// System.Void UniRx.Tuple`2<ZergRush.IObservableCollection`1<System.Object>,System.IDisposable>::.ctor(T1,T2)
#define Tuple_2__ctor_m4024756755(__this, ___item10, ___item21, method) ((  void (*) (Tuple_2_t1858350581 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))Tuple_2__ctor_m101027774_gshared)(__this, ___item10, ___item21, method)
// System.Int32 UniRx.Tuple`2<ZergRush.IObservableCollection`1<System.Object>,System.IDisposable>::System.IComparable.CompareTo(System.Object)
#define Tuple_2_System_IComparable_CompareTo_m3269749634(__this, ___obj0, method) ((  int32_t (*) (Tuple_2_t1858350581 *, Il2CppObject *, const MethodInfo*))Tuple_2_System_IComparable_CompareTo_m964946507_gshared)(__this, ___obj0, method)
// System.Int32 UniRx.Tuple`2<ZergRush.IObservableCollection`1<System.Object>,System.IDisposable>::UniRx.IStructuralComparable.CompareTo(System.Object,System.Collections.IComparer)
#define Tuple_2_UniRx_IStructuralComparable_CompareTo_m2365371092(__this, ___other0, ___comparer1, method) ((  int32_t (*) (Tuple_2_t1858350581 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))Tuple_2_UniRx_IStructuralComparable_CompareTo_m1607455453_gshared)(__this, ___other0, ___comparer1, method)
// System.Boolean UniRx.Tuple`2<ZergRush.IObservableCollection`1<System.Object>,System.IDisposable>::UniRx.IStructuralEquatable.Equals(System.Object,System.Collections.IEqualityComparer)
#define Tuple_2_UniRx_IStructuralEquatable_Equals_m2387233483(__this, ___other0, ___comparer1, method) ((  bool (*) (Tuple_2_t1858350581 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))Tuple_2_UniRx_IStructuralEquatable_Equals_m2678862280_gshared)(__this, ___other0, ___comparer1, method)
// System.Int32 UniRx.Tuple`2<ZergRush.IObservableCollection`1<System.Object>,System.IDisposable>::UniRx.IStructuralEquatable.GetHashCode(System.Collections.IEqualityComparer)
#define Tuple_2_UniRx_IStructuralEquatable_GetHashCode_m3861381135(__this, ___comparer0, method) ((  int32_t (*) (Tuple_2_t1858350581 *, Il2CppObject *, const MethodInfo*))Tuple_2_UniRx_IStructuralEquatable_GetHashCode_m2682338918_gshared)(__this, ___comparer0, method)
// System.String UniRx.Tuple`2<ZergRush.IObservableCollection`1<System.Object>,System.IDisposable>::UniRx.ITuple.ToString()
#define Tuple_2_UniRx_ITuple_ToString_m1639909174(__this, method) ((  String_t* (*) (Tuple_2_t1858350581 *, const MethodInfo*))Tuple_2_UniRx_ITuple_ToString_m2079956805_gshared)(__this, method)
// T1 UniRx.Tuple`2<ZergRush.IObservableCollection`1<System.Object>,System.IDisposable>::get_Item1()
#define Tuple_2_get_Item1_m417162189(__this, method) ((  Il2CppObject* (*) (Tuple_2_t1858350581 *, const MethodInfo*))Tuple_2_get_Item1_m425160818_gshared)(__this, method)
// T2 UniRx.Tuple`2<ZergRush.IObservableCollection`1<System.Object>,System.IDisposable>::get_Item2()
#define Tuple_2_get_Item2_m4183764845(__this, method) ((  Il2CppObject * (*) (Tuple_2_t1858350581 *, const MethodInfo*))Tuple_2_get_Item2_m1055620404_gshared)(__this, method)
// System.Boolean UniRx.Tuple`2<ZergRush.IObservableCollection`1<System.Object>,System.IDisposable>::Equals(System.Object)
#define Tuple_2_Equals_m4022676863(__this, ___obj0, method) ((  bool (*) (Tuple_2_t1858350581 *, Il2CppObject *, const MethodInfo*))Tuple_2_Equals_m2489154684_gshared)(__this, ___obj0, method)
// System.Int32 UniRx.Tuple`2<ZergRush.IObservableCollection`1<System.Object>,System.IDisposable>::GetHashCode()
#define Tuple_2_GetHashCode_m4133658263(__this, method) ((  int32_t (*) (Tuple_2_t1858350581 *, const MethodInfo*))Tuple_2_GetHashCode_m1023013664_gshared)(__this, method)
// System.String UniRx.Tuple`2<ZergRush.IObservableCollection`1<System.Object>,System.IDisposable>::ToString()
#define Tuple_2_ToString_m3136050235(__this, method) ((  String_t* (*) (Tuple_2_t1858350581 *, const MethodInfo*))Tuple_2_ToString_m3182481356_gshared)(__this, method)
// System.Boolean UniRx.Tuple`2<ZergRush.IObservableCollection`1<System.Object>,System.IDisposable>::Equals(UniRx.Tuple`2<T1,T2>)
#define Tuple_2_Equals_m2585514378(__this, ___other0, method) ((  bool (*) (Tuple_2_t1858350581 *, Tuple_2_t1858350581 , const MethodInfo*))Tuple_2_Equals_m1605966829_gshared)(__this, ___other0, method)
