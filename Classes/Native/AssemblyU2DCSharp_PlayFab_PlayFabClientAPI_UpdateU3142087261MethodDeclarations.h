﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.PlayFabClientAPI/UpdateUserStatisticsRequestCallback
struct UpdateUserStatisticsRequestCallback_t3142087261;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;
// PlayFab.ClientModels.UpdateUserStatisticsRequest
struct UpdateUserStatisticsRequest_t3113316872;
// System.IAsyncResult
struct IAsyncResult_t537683269;
// System.AsyncCallback
struct AsyncCallback_t1363551830;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_IntPtr676692020.h"
#include "mscorlib_System_String968488902.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_UpdateUserS3113316872.h"
#include "mscorlib_System_AsyncCallback1363551830.h"

// System.Void PlayFab.PlayFabClientAPI/UpdateUserStatisticsRequestCallback::.ctor(System.Object,System.IntPtr)
extern "C"  void UpdateUserStatisticsRequestCallback__ctor_m2773937260 (UpdateUserStatisticsRequestCallback_t3142087261 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/UpdateUserStatisticsRequestCallback::Invoke(System.String,System.Int32,PlayFab.ClientModels.UpdateUserStatisticsRequest,System.Object)
extern "C"  void UpdateUserStatisticsRequestCallback_Invoke_m831432863 (UpdateUserStatisticsRequestCallback_t3142087261 * __this, String_t* ___urlPath0, int32_t ___callId1, UpdateUserStatisticsRequest_t3113316872 * ___request2, Il2CppObject * ___customData3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
extern "C" void pinvoke_delegate_wrapper_UpdateUserStatisticsRequestCallback_t3142087261(Il2CppObject* delegate, String_t* ___urlPath0, int32_t ___callId1, UpdateUserStatisticsRequest_t3113316872 * ___request2, Il2CppObject * ___customData3);
// System.IAsyncResult PlayFab.PlayFabClientAPI/UpdateUserStatisticsRequestCallback::BeginInvoke(System.String,System.Int32,PlayFab.ClientModels.UpdateUserStatisticsRequest,System.Object,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * UpdateUserStatisticsRequestCallback_BeginInvoke_m1000766590 (UpdateUserStatisticsRequestCallback_t3142087261 * __this, String_t* ___urlPath0, int32_t ___callId1, UpdateUserStatisticsRequest_t3113316872 * ___request2, Il2CppObject * ___customData3, AsyncCallback_t1363551830 * ___callback4, Il2CppObject * ___object5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/UpdateUserStatisticsRequestCallback::EndInvoke(System.IAsyncResult)
extern "C"  void UpdateUserStatisticsRequestCallback_EndInvoke_m1988719740 (UpdateUserStatisticsRequestCallback_t3142087261 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
