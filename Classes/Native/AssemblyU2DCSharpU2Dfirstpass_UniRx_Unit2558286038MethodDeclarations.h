﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Object
struct Il2CppObject;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Unit2558286038.h"
#include "mscorlib_System_Object837106420.h"

// System.Void UniRx.Unit::.cctor()
extern "C"  void Unit__cctor_m2627613082 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UniRx.Unit UniRx.Unit::get_Default()
extern "C"  Unit_t2558286038  Unit_get_Default_m3654553472 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UniRx.Unit::Equals(UniRx.Unit)
extern "C"  bool Unit_Equals_m663267440 (Unit_t2558286038 * __this, Unit_t2558286038  ___other0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UniRx.Unit::Equals(System.Object)
extern "C"  bool Unit_Equals_m991036168 (Unit_t2558286038 * __this, Il2CppObject * ___obj0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UniRx.Unit::GetHashCode()
extern "C"  int32_t Unit_GetHashCode_m922803628 (Unit_t2558286038 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String UniRx.Unit::ToString()
extern "C"  String_t* Unit_ToString_m316785856 (Unit_t2558286038 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UniRx.Unit::op_Equality(UniRx.Unit,UniRx.Unit)
extern "C"  bool Unit_op_Equality_m3803338073 (Il2CppObject * __this /* static, unused */, Unit_t2558286038  ___first0, Unit_t2558286038  ___second1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UniRx.Unit::op_Inequality(UniRx.Unit,UniRx.Unit)
extern "C"  bool Unit_op_Inequality_m1044299796 (Il2CppObject * __this /* static, unused */, Unit_t2558286038  ___first0, Unit_t2558286038  ___second1, const MethodInfo* method) IL2CPP_METHOD_ATTR;

// Methods for marshaling
struct Unit_t2558286038;
struct Unit_t2558286038_marshaled_pinvoke;

extern "C" void Unit_t2558286038_marshal_pinvoke(const Unit_t2558286038& unmarshaled, Unit_t2558286038_marshaled_pinvoke& marshaled);
extern "C" void Unit_t2558286038_marshal_pinvoke_back(const Unit_t2558286038_marshaled_pinvoke& marshaled, Unit_t2558286038& unmarshaled);
extern "C" void Unit_t2558286038_marshal_pinvoke_cleanup(Unit_t2558286038_marshaled_pinvoke& marshaled);

// Methods for marshaling
struct Unit_t2558286038;
struct Unit_t2558286038_marshaled_com;

extern "C" void Unit_t2558286038_marshal_com(const Unit_t2558286038& unmarshaled, Unit_t2558286038_marshaled_com& marshaled);
extern "C" void Unit_t2558286038_marshal_com_back(const Unit_t2558286038_marshaled_com& marshaled, Unit_t2558286038& unmarshaled);
extern "C" void Unit_t2558286038_marshal_com_cleanup(Unit_t2558286038_marshaled_com& marshaled);
