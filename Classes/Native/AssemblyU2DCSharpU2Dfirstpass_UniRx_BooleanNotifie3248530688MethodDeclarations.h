﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.BooleanNotifier
struct BooleanNotifier_t3248530688;
// System.IDisposable
struct IDisposable_t1628921374;
// UniRx.IObserver`1<System.Boolean>
struct IObserver_1_t2423004244;

#include "codegen/il2cpp-codegen.h"

// System.Void UniRx.BooleanNotifier::.ctor(System.Boolean)
extern "C"  void BooleanNotifier__ctor_m2255594648 (BooleanNotifier_t3248530688 * __this, bool ___initialValue0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UniRx.BooleanNotifier::get_Value()
extern "C"  bool BooleanNotifier_get_Value_m3857580771 (BooleanNotifier_t3248530688 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.BooleanNotifier::set_Value(System.Boolean)
extern "C"  void BooleanNotifier_set_Value_m509900170 (BooleanNotifier_t3248530688 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.BooleanNotifier::TurnOn()
extern "C"  void BooleanNotifier_TurnOn_m3132110847 (BooleanNotifier_t3248530688 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.BooleanNotifier::TurnOff()
extern "C"  void BooleanNotifier_TurnOff_m2605977009 (BooleanNotifier_t3248530688 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.BooleanNotifier::SwitchValue()
extern "C"  void BooleanNotifier_SwitchValue_m2897531612 (BooleanNotifier_t3248530688 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IDisposable UniRx.BooleanNotifier::Subscribe(UniRx.IObserver`1<System.Boolean>)
extern "C"  Il2CppObject * BooleanNotifier_Subscribe_m1364813245 (BooleanNotifier_t3248530688 * __this, Il2CppObject* ___observer0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
