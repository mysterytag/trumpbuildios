﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// LitJson.JsonMapper/<RegisterImporter>c__AnonStorey154`2<System.Object,System.Object>
struct U3CRegisterImporterU3Ec__AnonStorey154_2_t3095985135;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"

// System.Void LitJson.JsonMapper/<RegisterImporter>c__AnonStorey154`2<System.Object,System.Object>::.ctor()
extern "C"  void U3CRegisterImporterU3Ec__AnonStorey154_2__ctor_m4172800208_gshared (U3CRegisterImporterU3Ec__AnonStorey154_2_t3095985135 * __this, const MethodInfo* method);
#define U3CRegisterImporterU3Ec__AnonStorey154_2__ctor_m4172800208(__this, method) ((  void (*) (U3CRegisterImporterU3Ec__AnonStorey154_2_t3095985135 *, const MethodInfo*))U3CRegisterImporterU3Ec__AnonStorey154_2__ctor_m4172800208_gshared)(__this, method)
// System.Object LitJson.JsonMapper/<RegisterImporter>c__AnonStorey154`2<System.Object,System.Object>::<>m__222(System.Object)
extern "C"  Il2CppObject * U3CRegisterImporterU3Ec__AnonStorey154_2_U3CU3Em__222_m248240132_gshared (U3CRegisterImporterU3Ec__AnonStorey154_2_t3095985135 * __this, Il2CppObject * ___input0, const MethodInfo* method);
#define U3CRegisterImporterU3Ec__AnonStorey154_2_U3CU3Em__222_m248240132(__this, ___input0, method) ((  Il2CppObject * (*) (U3CRegisterImporterU3Ec__AnonStorey154_2_t3095985135 *, Il2CppObject *, const MethodInfo*))U3CRegisterImporterU3Ec__AnonStorey154_2_U3CU3Em__222_m248240132_gshared)(__this, ___input0, method)
