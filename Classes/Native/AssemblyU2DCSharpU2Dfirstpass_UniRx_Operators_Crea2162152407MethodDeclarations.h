﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.CreateObservable`1<System.Single>
struct CreateObservable_1_t2162152407;
// System.Func`2<UniRx.IObserver`1<System.Single>,System.IDisposable>
struct Func_2_t3827670562;
// System.IDisposable
struct IDisposable_t1628921374;
// UniRx.IObserver`1<System.Single>
struct IObserver_1_t3170207924;

#include "codegen/il2cpp-codegen.h"

// System.Void UniRx.Operators.CreateObservable`1<System.Single>::.ctor(System.Func`2<UniRx.IObserver`1<T>,System.IDisposable>)
extern "C"  void CreateObservable_1__ctor_m2366941070_gshared (CreateObservable_1_t2162152407 * __this, Func_2_t3827670562 * ___subscribe0, const MethodInfo* method);
#define CreateObservable_1__ctor_m2366941070(__this, ___subscribe0, method) ((  void (*) (CreateObservable_1_t2162152407 *, Func_2_t3827670562 *, const MethodInfo*))CreateObservable_1__ctor_m2366941070_gshared)(__this, ___subscribe0, method)
// System.Void UniRx.Operators.CreateObservable`1<System.Single>::.ctor(System.Func`2<UniRx.IObserver`1<T>,System.IDisposable>,System.Boolean)
extern "C"  void CreateObservable_1__ctor_m2052185647_gshared (CreateObservable_1_t2162152407 * __this, Func_2_t3827670562 * ___subscribe0, bool ___isRequiredSubscribeOnCurrentThread1, const MethodInfo* method);
#define CreateObservable_1__ctor_m2052185647(__this, ___subscribe0, ___isRequiredSubscribeOnCurrentThread1, method) ((  void (*) (CreateObservable_1_t2162152407 *, Func_2_t3827670562 *, bool, const MethodInfo*))CreateObservable_1__ctor_m2052185647_gshared)(__this, ___subscribe0, ___isRequiredSubscribeOnCurrentThread1, method)
// System.IDisposable UniRx.Operators.CreateObservable`1<System.Single>::SubscribeCore(UniRx.IObserver`1<T>,System.IDisposable)
extern "C"  Il2CppObject * CreateObservable_1_SubscribeCore_m3312336157_gshared (CreateObservable_1_t2162152407 * __this, Il2CppObject* ___observer0, Il2CppObject * ___cancel1, const MethodInfo* method);
#define CreateObservable_1_SubscribeCore_m3312336157(__this, ___observer0, ___cancel1, method) ((  Il2CppObject * (*) (CreateObservable_1_t2162152407 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))CreateObservable_1_SubscribeCore_m3312336157_gshared)(__this, ___observer0, ___cancel1, method)
