﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SponsorPay.InterstitialAdStatus
struct InterstitialAdStatus_t2696000795;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String968488902.h"

// System.Void SponsorPay.InterstitialAdStatus::.ctor()
extern "C"  void InterstitialAdStatus__ctor_m3724594314 (InterstitialAdStatus_t2696000795 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String SponsorPay.InterstitialAdStatus::get_error()
extern "C"  String_t* InterstitialAdStatus_get_error_m4230946826 (InterstitialAdStatus_t2696000795 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SponsorPay.InterstitialAdStatus::set_error(System.String)
extern "C"  void InterstitialAdStatus_set_error_m125919855 (InterstitialAdStatus_t2696000795 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String SponsorPay.InterstitialAdStatus::get_closeReason()
extern "C"  String_t* InterstitialAdStatus_get_closeReason_m1377404574 (InterstitialAdStatus_t2696000795 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SponsorPay.InterstitialAdStatus::set_closeReason(System.String)
extern "C"  void InterstitialAdStatus_set_closeReason_m1338885915 (InterstitialAdStatus_t2696000795 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
