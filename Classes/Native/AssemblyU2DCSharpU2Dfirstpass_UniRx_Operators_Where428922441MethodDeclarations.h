﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.WhereObservable`1<UnityEngine.Vector2>
struct WhereObservable_1_t428922441;
// UniRx.IObservable`1<UnityEngine.Vector2>
struct IObservable_1_t3284128152;
// System.Func`2<UnityEngine.Vector2,System.Boolean>
struct Func_2_t4239542457;
// System.Func`3<UnityEngine.Vector2,System.Int32,System.Boolean>
struct Func_3_t487212563;
// System.IDisposable
struct IDisposable_t1628921374;
// UniRx.IObserver`1<UnityEngine.Vector2>
struct IObserver_1_t1442361395;

#include "codegen/il2cpp-codegen.h"

// System.Void UniRx.Operators.WhereObservable`1<UnityEngine.Vector2>::.ctor(UniRx.IObservable`1<T>,System.Func`2<T,System.Boolean>)
extern "C"  void WhereObservable_1__ctor_m831884166_gshared (WhereObservable_1_t428922441 * __this, Il2CppObject* ___source0, Func_2_t4239542457 * ___predicate1, const MethodInfo* method);
#define WhereObservable_1__ctor_m831884166(__this, ___source0, ___predicate1, method) ((  void (*) (WhereObservable_1_t428922441 *, Il2CppObject*, Func_2_t4239542457 *, const MethodInfo*))WhereObservable_1__ctor_m831884166_gshared)(__this, ___source0, ___predicate1, method)
// System.Void UniRx.Operators.WhereObservable`1<UnityEngine.Vector2>::.ctor(UniRx.IObservable`1<T>,System.Func`3<T,System.Int32,System.Boolean>)
extern "C"  void WhereObservable_1__ctor_m4218900832_gshared (WhereObservable_1_t428922441 * __this, Il2CppObject* ___source0, Func_3_t487212563 * ___predicateWithIndex1, const MethodInfo* method);
#define WhereObservable_1__ctor_m4218900832(__this, ___source0, ___predicateWithIndex1, method) ((  void (*) (WhereObservable_1_t428922441 *, Il2CppObject*, Func_3_t487212563 *, const MethodInfo*))WhereObservable_1__ctor_m4218900832_gshared)(__this, ___source0, ___predicateWithIndex1, method)
// UniRx.IObservable`1<T> UniRx.Operators.WhereObservable`1<UnityEngine.Vector2>::CombinePredicate(System.Func`2<T,System.Boolean>)
extern "C"  Il2CppObject* WhereObservable_1_CombinePredicate_m1290245997_gshared (WhereObservable_1_t428922441 * __this, Func_2_t4239542457 * ___combinePredicate0, const MethodInfo* method);
#define WhereObservable_1_CombinePredicate_m1290245997(__this, ___combinePredicate0, method) ((  Il2CppObject* (*) (WhereObservable_1_t428922441 *, Func_2_t4239542457 *, const MethodInfo*))WhereObservable_1_CombinePredicate_m1290245997_gshared)(__this, ___combinePredicate0, method)
// System.IDisposable UniRx.Operators.WhereObservable`1<UnityEngine.Vector2>::SubscribeCore(UniRx.IObserver`1<T>,System.IDisposable)
extern "C"  Il2CppObject * WhereObservable_1_SubscribeCore_m1997934189_gshared (WhereObservable_1_t428922441 * __this, Il2CppObject* ___observer0, Il2CppObject * ___cancel1, const MethodInfo* method);
#define WhereObservable_1_SubscribeCore_m1997934189(__this, ___observer0, ___cancel1, method) ((  Il2CppObject * (*) (WhereObservable_1_t428922441 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))WhereObservable_1_SubscribeCore_m1997934189_gshared)(__this, ___observer0, ___cancel1, method)
