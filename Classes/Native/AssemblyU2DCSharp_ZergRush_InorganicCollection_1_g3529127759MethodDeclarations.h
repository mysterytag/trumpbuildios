﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_ZergRush_InorganicCollection_1_g3450977518MethodDeclarations.h"

// System.Void ZergRush.InorganicCollection`1<SettingsButton>::.ctor()
#define InorganicCollection_1__ctor_m3809608943(__this, method) ((  void (*) (InorganicCollection_1_t3529127759 *, const MethodInfo*))InorganicCollection_1__ctor_m2562722286_gshared)(__this, method)
// System.Void ZergRush.InorganicCollection`1<SettingsButton>::.ctor(System.Collections.Generic.IEnumerable`1<T>)
#define InorganicCollection_1__ctor_m474990640(__this, ___collection0, method) ((  void (*) (InorganicCollection_1_t3529127759 *, Il2CppObject*, const MethodInfo*))InorganicCollection_1__ctor_m3238279633_gshared)(__this, ___collection0, method)
// System.Void ZergRush.InorganicCollection`1<SettingsButton>::.ctor(System.Collections.Generic.List`1<T>)
#define InorganicCollection_1__ctor_m1080836551(__this, ___list0, method) ((  void (*) (InorganicCollection_1_t3529127759 *, List_1_t1712215630 *, const MethodInfo*))InorganicCollection_1__ctor_m1788080966_gshared)(__this, ___list0, method)
// System.Void ZergRush.InorganicCollection`1<SettingsButton>::ClearItems()
#define InorganicCollection_1_ClearItems_m1818460200(__this, method) ((  void (*) (InorganicCollection_1_t3529127759 *, const MethodInfo*))InorganicCollection_1_ClearItems_m2583413385_gshared)(__this, method)
// System.Void ZergRush.InorganicCollection`1<SettingsButton>::InsertItem(System.Int32,T)
#define InorganicCollection_1_InsertItem_m717899466(__this, ___index0, ___item1, method) ((  void (*) (InorganicCollection_1_t3529127759 *, int32_t, SettingsButton_t915256661 *, const MethodInfo*))InorganicCollection_1_InsertItem_m1273645931_gshared)(__this, ___index0, ___item1, method)
// System.Void ZergRush.InorganicCollection`1<SettingsButton>::Move(System.Int32,System.Int32)
#define InorganicCollection_1_Move_m591014880(__this, ___oldIndex0, ___newIndex1, method) ((  void (*) (InorganicCollection_1_t3529127759 *, int32_t, int32_t, const MethodInfo*))InorganicCollection_1_Move_m2620974047_gshared)(__this, ___oldIndex0, ___newIndex1, method)
// System.Void ZergRush.InorganicCollection`1<SettingsButton>::MoveItem(System.Int32,System.Int32)
#define InorganicCollection_1_MoveItem_m2137100109(__this, ___oldIndex0, ___newIndex1, method) ((  void (*) (InorganicCollection_1_t3529127759 *, int32_t, int32_t, const MethodInfo*))InorganicCollection_1_MoveItem_m1781936076_gshared)(__this, ___oldIndex0, ___newIndex1, method)
// System.Void ZergRush.InorganicCollection`1<SettingsButton>::RemoveItem(System.Int32)
#define InorganicCollection_1_RemoveItem_m442191549(__this, ___index0, method) ((  void (*) (InorganicCollection_1_t3529127759 *, int32_t, const MethodInfo*))InorganicCollection_1_RemoveItem_m3410364318_gshared)(__this, ___index0, method)
// System.Void ZergRush.InorganicCollection`1<SettingsButton>::RemoveItem(System.Func`2<T,System.Boolean>)
#define InorganicCollection_1_RemoveItem_m1659702384(__this, ___filter0, method) ((  void (*) (InorganicCollection_1_t3529127759 *, Func_2_t306708316 *, const MethodInfo*))InorganicCollection_1_RemoveItem_m2366946799_gshared)(__this, ___filter0, method)
// System.Void ZergRush.InorganicCollection`1<SettingsButton>::SetItem(System.Int32,T)
#define InorganicCollection_1_SetItem_m90969995(__this, ___index0, ___item1, method) ((  void (*) (InorganicCollection_1_t3529127759 *, int32_t, SettingsButton_t915256661 *, const MethodInfo*))InorganicCollection_1_SetItem_m2680569482_gshared)(__this, ___index0, ___item1, method)
// IStream`1<System.Int32> ZergRush.InorganicCollection`1<SettingsButton>::ObserveCountChanged()
#define InorganicCollection_1_ObserveCountChanged_m2533846088(__this, method) ((  Il2CppObject* (*) (InorganicCollection_1_t3529127759 *, const MethodInfo*))InorganicCollection_1_ObserveCountChanged_m3361259667_gshared)(__this, method)
// IStream`1<UniRx.Unit> ZergRush.InorganicCollection`1<SettingsButton>::ObserveReset()
#define InorganicCollection_1_ObserveReset_m2596106619(__this, method) ((  Il2CppObject* (*) (InorganicCollection_1_t3529127759 *, const MethodInfo*))InorganicCollection_1_ObserveReset_m884902102_gshared)(__this, method)
// IStream`1<UniRx.CollectionAddEvent`1<T>> ZergRush.InorganicCollection`1<SettingsButton>::ObserveAdd()
#define InorganicCollection_1_ObserveAdd_m184742365(__this, method) ((  Il2CppObject* (*) (InorganicCollection_1_t3529127759 *, const MethodInfo*))InorganicCollection_1_ObserveAdd_m500474968_gshared)(__this, method)
// IStream`1<CollectionMoveEvent`1<T>> ZergRush.InorganicCollection`1<SettingsButton>::ObserveMove()
#define InorganicCollection_1_ObserveMove_m310047161(__this, method) ((  Il2CppObject* (*) (InorganicCollection_1_t3529127759 *, const MethodInfo*))InorganicCollection_1_ObserveMove_m2797955546_gshared)(__this, method)
// IStream`1<CollectionRemoveEvent`1<T>> ZergRush.InorganicCollection`1<SettingsButton>::ObserveRemove()
#define InorganicCollection_1_ObserveRemove_m1165830169(__this, method) ((  Il2CppObject* (*) (InorganicCollection_1_t3529127759 *, const MethodInfo*))InorganicCollection_1_ObserveRemove_m3795636736_gshared)(__this, method)
// IStream`1<CollectionReplaceEvent`1<T>> ZergRush.InorganicCollection`1<SettingsButton>::ObserveReplace()
#define InorganicCollection_1_ObserveReplace_m3598009275(__this, method) ((  Il2CppObject* (*) (InorganicCollection_1_t3529127759 *, const MethodInfo*))InorganicCollection_1_ObserveReplace_m3125593920_gshared)(__this, method)
// System.IDisposable ZergRush.InorganicCollection`1<SettingsButton>::OnChanged(System.Action,Priority)
#define InorganicCollection_1_OnChanged_m814547092(__this, ___action0, ___p1, method) ((  Il2CppObject * (*) (InorganicCollection_1_t3529127759 *, Action_t437523947 *, int32_t, const MethodInfo*))InorganicCollection_1_OnChanged_m1222232857_gshared)(__this, ___action0, ___p1, method)
// System.IDisposable ZergRush.InorganicCollection`1<SettingsButton>::Bind(System.Action`1<System.Collections.Generic.ICollection`1<T>>,Priority)
#define InorganicCollection_1_Bind_m2749669716(__this, ___action0, ___p1, method) ((  Il2CppObject * (*) (InorganicCollection_1_t3529127759 *, Action_1_t1529540752 *, int32_t, const MethodInfo*))InorganicCollection_1_Bind_m1502836121_gshared)(__this, ___action0, ___p1, method)
// System.IDisposable ZergRush.InorganicCollection`1<SettingsButton>::ListenUpdates(System.Action`1<System.Collections.Generic.ICollection`1<T>>,Priority)
#define InorganicCollection_1_ListenUpdates_m3977539644(__this, ___reaction0, ___p1, method) ((  Il2CppObject * (*) (InorganicCollection_1_t3529127759 *, Action_1_t1529540752 *, int32_t, const MethodInfo*))InorganicCollection_1_ListenUpdates_m1688181143_gshared)(__this, ___reaction0, ___p1, method)
// System.Collections.Generic.ICollection`1<T> ZergRush.InorganicCollection`1<SettingsButton>::get_value()
#define InorganicCollection_1_get_value_m1515984191(__this, method) ((  Il2CppObject* (*) (InorganicCollection_1_t3529127759 *, const MethodInfo*))InorganicCollection_1_get_value_m3124552170_gshared)(__this, method)
// System.Object ZergRush.InorganicCollection`1<SettingsButton>::get_valueObject()
#define InorganicCollection_1_get_valueObject_m2583806975(__this, method) ((  Il2CppObject * (*) (InorganicCollection_1_t3529127759 *, const MethodInfo*))InorganicCollection_1_get_valueObject_m2207754504_gshared)(__this, method)
