﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,LitJson.ObjectMetadata>
struct KeyCollection_t3623674029;
// System.Collections.Generic.Dictionary`2<System.Object,LitJson.ObjectMetadata>
struct Dictionary_2_t1300398749;
// System.Object
struct Il2CppObject;
// System.Collections.Generic.IEnumerator`1<System.Object>
struct IEnumerator_1_t2320212868;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t287207039;
// System.Object[]
struct ObjectU5BU5D_t11523773;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_Array2840145358.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_K1067426690.h"

// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,LitJson.ObjectMetadata>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void KeyCollection__ctor_m2443897875_gshared (KeyCollection_t3623674029 * __this, Dictionary_2_t1300398749 * ___dictionary0, const MethodInfo* method);
#define KeyCollection__ctor_m2443897875(__this, ___dictionary0, method) ((  void (*) (KeyCollection_t3623674029 *, Dictionary_2_t1300398749 *, const MethodInfo*))KeyCollection__ctor_m2443897875_gshared)(__this, ___dictionary0, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,LitJson.ObjectMetadata>::System.Collections.Generic.ICollection<TKey>.Add(TKey)
extern "C"  void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m3936742755_gshared (KeyCollection_t3623674029 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m3936742755(__this, ___item0, method) ((  void (*) (KeyCollection_t3623674029 *, Il2CppObject *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m3936742755_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,LitJson.ObjectMetadata>::System.Collections.Generic.ICollection<TKey>.Clear()
extern "C"  void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m1094586266_gshared (KeyCollection_t3623674029 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m1094586266(__this, method) ((  void (*) (KeyCollection_t3623674029 *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m1094586266_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,LitJson.ObjectMetadata>::System.Collections.Generic.ICollection<TKey>.Contains(TKey)
extern "C"  bool KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m344040395_gshared (KeyCollection_t3623674029 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m344040395(__this, ___item0, method) ((  bool (*) (KeyCollection_t3623674029 *, Il2CppObject *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m344040395_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,LitJson.ObjectMetadata>::System.Collections.Generic.ICollection<TKey>.Remove(TKey)
extern "C"  bool KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m99257328_gshared (KeyCollection_t3623674029 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m99257328(__this, ___item0, method) ((  bool (*) (KeyCollection_t3623674029 *, Il2CppObject *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m99257328_gshared)(__this, ___item0, method)
// System.Collections.Generic.IEnumerator`1<TKey> System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,LitJson.ObjectMetadata>::System.Collections.Generic.IEnumerable<TKey>.GetEnumerator()
extern "C"  Il2CppObject* KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m4176062764_gshared (KeyCollection_t3623674029 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m4176062764(__this, method) ((  Il2CppObject* (*) (KeyCollection_t3623674029 *, const MethodInfo*))KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m4176062764_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,LitJson.ObjectMetadata>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void KeyCollection_System_Collections_ICollection_CopyTo_m1020484876_gshared (KeyCollection_t3623674029 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define KeyCollection_System_Collections_ICollection_CopyTo_m1020484876(__this, ___array0, ___index1, method) ((  void (*) (KeyCollection_t3623674029 *, Il2CppArray *, int32_t, const MethodInfo*))KeyCollection_System_Collections_ICollection_CopyTo_m1020484876_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,LitJson.ObjectMetadata>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * KeyCollection_System_Collections_IEnumerable_GetEnumerator_m2957126171_gshared (KeyCollection_t3623674029 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_IEnumerable_GetEnumerator_m2957126171(__this, method) ((  Il2CppObject * (*) (KeyCollection_t3623674029 *, const MethodInfo*))KeyCollection_System_Collections_IEnumerable_GetEnumerator_m2957126171_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,LitJson.ObjectMetadata>::System.Collections.Generic.ICollection<TKey>.get_IsReadOnly()
extern "C"  bool KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m2791396460_gshared (KeyCollection_t3623674029 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m2791396460(__this, method) ((  bool (*) (KeyCollection_t3623674029 *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m2791396460_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,LitJson.ObjectMetadata>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool KeyCollection_System_Collections_ICollection_get_IsSynchronized_m3833705502_gshared (KeyCollection_t3623674029 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_ICollection_get_IsSynchronized_m3833705502(__this, method) ((  bool (*) (KeyCollection_t3623674029 *, const MethodInfo*))KeyCollection_System_Collections_ICollection_get_IsSynchronized_m3833705502_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,LitJson.ObjectMetadata>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * KeyCollection_System_Collections_ICollection_get_SyncRoot_m562760208_gshared (KeyCollection_t3623674029 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_ICollection_get_SyncRoot_m562760208(__this, method) ((  Il2CppObject * (*) (KeyCollection_t3623674029 *, const MethodInfo*))KeyCollection_System_Collections_ICollection_get_SyncRoot_m562760208_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,LitJson.ObjectMetadata>::CopyTo(TKey[],System.Int32)
extern "C"  void KeyCollection_CopyTo_m2121621192_gshared (KeyCollection_t3623674029 * __this, ObjectU5BU5D_t11523773* ___array0, int32_t ___index1, const MethodInfo* method);
#define KeyCollection_CopyTo_m2121621192(__this, ___array0, ___index1, method) ((  void (*) (KeyCollection_t3623674029 *, ObjectU5BU5D_t11523773*, int32_t, const MethodInfo*))KeyCollection_CopyTo_m2121621192_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,LitJson.ObjectMetadata>::GetEnumerator()
extern "C"  Enumerator_t1067426690  KeyCollection_GetEnumerator_m4166739669_gshared (KeyCollection_t3623674029 * __this, const MethodInfo* method);
#define KeyCollection_GetEnumerator_m4166739669(__this, method) ((  Enumerator_t1067426690  (*) (KeyCollection_t3623674029 *, const MethodInfo*))KeyCollection_GetEnumerator_m4166739669_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,LitJson.ObjectMetadata>::get_Count()
extern "C"  int32_t KeyCollection_get_Count_m1912834904_gshared (KeyCollection_t3623674029 * __this, const MethodInfo* method);
#define KeyCollection_get_Count_m1912834904(__this, method) ((  int32_t (*) (KeyCollection_t3623674029 *, const MethodInfo*))KeyCollection_get_Count_m1912834904_gshared)(__this, method)
