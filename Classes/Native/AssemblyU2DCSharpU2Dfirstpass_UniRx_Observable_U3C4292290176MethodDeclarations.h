﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Observable/<CombineSources>c__IteratorD`1<UniRx.Unit>
struct U3CCombineSourcesU3Ec__IteratorD_1_t4292290176;
// UniRx.IObservable`1<UniRx.Unit>
struct IObservable_1_t2317084402;
// System.Object
struct Il2CppObject;
// System.Collections.IEnumerator
struct IEnumerator_t287207039;
// System.Collections.Generic.IEnumerator`1<UniRx.IObservable`1<UniRx.Unit>>
struct IEnumerator_1_t3800190850;

#include "codegen/il2cpp-codegen.h"

// System.Void UniRx.Observable/<CombineSources>c__IteratorD`1<UniRx.Unit>::.ctor()
extern "C"  void U3CCombineSourcesU3Ec__IteratorD_1__ctor_m689220816_gshared (U3CCombineSourcesU3Ec__IteratorD_1_t4292290176 * __this, const MethodInfo* method);
#define U3CCombineSourcesU3Ec__IteratorD_1__ctor_m689220816(__this, method) ((  void (*) (U3CCombineSourcesU3Ec__IteratorD_1_t4292290176 *, const MethodInfo*))U3CCombineSourcesU3Ec__IteratorD_1__ctor_m689220816_gshared)(__this, method)
// UniRx.IObservable`1<T> UniRx.Observable/<CombineSources>c__IteratorD`1<UniRx.Unit>::System.Collections.Generic.IEnumerator<UniRx.IObservable<T>>.get_Current()
extern "C"  Il2CppObject* U3CCombineSourcesU3Ec__IteratorD_1_System_Collections_Generic_IEnumeratorU3CUniRx_IObservableU3CTU3EU3E_get_Current_m972461888_gshared (U3CCombineSourcesU3Ec__IteratorD_1_t4292290176 * __this, const MethodInfo* method);
#define U3CCombineSourcesU3Ec__IteratorD_1_System_Collections_Generic_IEnumeratorU3CUniRx_IObservableU3CTU3EU3E_get_Current_m972461888(__this, method) ((  Il2CppObject* (*) (U3CCombineSourcesU3Ec__IteratorD_1_t4292290176 *, const MethodInfo*))U3CCombineSourcesU3Ec__IteratorD_1_System_Collections_Generic_IEnumeratorU3CUniRx_IObservableU3CTU3EU3E_get_Current_m972461888_gshared)(__this, method)
// System.Object UniRx.Observable/<CombineSources>c__IteratorD`1<UniRx.Unit>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CCombineSourcesU3Ec__IteratorD_1_System_Collections_IEnumerator_get_Current_m3628668054_gshared (U3CCombineSourcesU3Ec__IteratorD_1_t4292290176 * __this, const MethodInfo* method);
#define U3CCombineSourcesU3Ec__IteratorD_1_System_Collections_IEnumerator_get_Current_m3628668054(__this, method) ((  Il2CppObject * (*) (U3CCombineSourcesU3Ec__IteratorD_1_t4292290176 *, const MethodInfo*))U3CCombineSourcesU3Ec__IteratorD_1_System_Collections_IEnumerator_get_Current_m3628668054_gshared)(__this, method)
// System.Collections.IEnumerator UniRx.Observable/<CombineSources>c__IteratorD`1<UniRx.Unit>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * U3CCombineSourcesU3Ec__IteratorD_1_System_Collections_IEnumerable_GetEnumerator_m1741645993_gshared (U3CCombineSourcesU3Ec__IteratorD_1_t4292290176 * __this, const MethodInfo* method);
#define U3CCombineSourcesU3Ec__IteratorD_1_System_Collections_IEnumerable_GetEnumerator_m1741645993(__this, method) ((  Il2CppObject * (*) (U3CCombineSourcesU3Ec__IteratorD_1_t4292290176 *, const MethodInfo*))U3CCombineSourcesU3Ec__IteratorD_1_System_Collections_IEnumerable_GetEnumerator_m1741645993_gshared)(__this, method)
// System.Collections.Generic.IEnumerator`1<UniRx.IObservable`1<T>> UniRx.Observable/<CombineSources>c__IteratorD`1<UniRx.Unit>::System.Collections.Generic.IEnumerable<UniRx.IObservable<T>>.GetEnumerator()
extern "C"  Il2CppObject* U3CCombineSourcesU3Ec__IteratorD_1_System_Collections_Generic_IEnumerableU3CUniRx_IObservableU3CTU3EU3E_GetEnumerator_m3455808573_gshared (U3CCombineSourcesU3Ec__IteratorD_1_t4292290176 * __this, const MethodInfo* method);
#define U3CCombineSourcesU3Ec__IteratorD_1_System_Collections_Generic_IEnumerableU3CUniRx_IObservableU3CTU3EU3E_GetEnumerator_m3455808573(__this, method) ((  Il2CppObject* (*) (U3CCombineSourcesU3Ec__IteratorD_1_t4292290176 *, const MethodInfo*))U3CCombineSourcesU3Ec__IteratorD_1_System_Collections_Generic_IEnumerableU3CUniRx_IObservableU3CTU3EU3E_GetEnumerator_m3455808573_gshared)(__this, method)
// System.Boolean UniRx.Observable/<CombineSources>c__IteratorD`1<UniRx.Unit>::MoveNext()
extern "C"  bool U3CCombineSourcesU3Ec__IteratorD_1_MoveNext_m2433675580_gshared (U3CCombineSourcesU3Ec__IteratorD_1_t4292290176 * __this, const MethodInfo* method);
#define U3CCombineSourcesU3Ec__IteratorD_1_MoveNext_m2433675580(__this, method) ((  bool (*) (U3CCombineSourcesU3Ec__IteratorD_1_t4292290176 *, const MethodInfo*))U3CCombineSourcesU3Ec__IteratorD_1_MoveNext_m2433675580_gshared)(__this, method)
// System.Void UniRx.Observable/<CombineSources>c__IteratorD`1<UniRx.Unit>::Dispose()
extern "C"  void U3CCombineSourcesU3Ec__IteratorD_1_Dispose_m814523661_gshared (U3CCombineSourcesU3Ec__IteratorD_1_t4292290176 * __this, const MethodInfo* method);
#define U3CCombineSourcesU3Ec__IteratorD_1_Dispose_m814523661(__this, method) ((  void (*) (U3CCombineSourcesU3Ec__IteratorD_1_t4292290176 *, const MethodInfo*))U3CCombineSourcesU3Ec__IteratorD_1_Dispose_m814523661_gshared)(__this, method)
// System.Void UniRx.Observable/<CombineSources>c__IteratorD`1<UniRx.Unit>::Reset()
extern "C"  void U3CCombineSourcesU3Ec__IteratorD_1_Reset_m2630621053_gshared (U3CCombineSourcesU3Ec__IteratorD_1_t4292290176 * __this, const MethodInfo* method);
#define U3CCombineSourcesU3Ec__IteratorD_1_Reset_m2630621053(__this, method) ((  void (*) (U3CCombineSourcesU3Ec__IteratorD_1_t4292290176 *, const MethodInfo*))U3CCombineSourcesU3Ec__IteratorD_1_Reset_m2630621053_gshared)(__this, method)
