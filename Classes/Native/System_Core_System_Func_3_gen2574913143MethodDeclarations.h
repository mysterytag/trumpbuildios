﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Func`3<System.Single,System.Object,System.Single>
struct Func_3_t2574913143;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t537683269;
// System.AsyncCallback
struct AsyncCallback_t1363551830;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_IntPtr676692020.h"
#include "mscorlib_System_AsyncCallback1363551830.h"

// System.Void System.Func`3<System.Single,System.Object,System.Single>::.ctor(System.Object,System.IntPtr)
extern "C"  void Func_3__ctor_m2429120871_gshared (Func_3_t2574913143 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method);
#define Func_3__ctor_m2429120871(__this, ___object0, ___method1, method) ((  void (*) (Func_3_t2574913143 *, Il2CppObject *, IntPtr_t, const MethodInfo*))Func_3__ctor_m2429120871_gshared)(__this, ___object0, ___method1, method)
// TResult System.Func`3<System.Single,System.Object,System.Single>::Invoke(T1,T2)
extern "C"  float Func_3_Invoke_m3675757698_gshared (Func_3_t2574913143 * __this, float ___arg10, Il2CppObject * ___arg21, const MethodInfo* method);
#define Func_3_Invoke_m3675757698(__this, ___arg10, ___arg21, method) ((  float (*) (Func_3_t2574913143 *, float, Il2CppObject *, const MethodInfo*))Func_3_Invoke_m3675757698_gshared)(__this, ___arg10, ___arg21, method)
// System.IAsyncResult System.Func`3<System.Single,System.Object,System.Single>::BeginInvoke(T1,T2,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * Func_3_BeginInvoke_m3116481923_gshared (Func_3_t2574913143 * __this, float ___arg10, Il2CppObject * ___arg21, AsyncCallback_t1363551830 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method);
#define Func_3_BeginInvoke_m3116481923(__this, ___arg10, ___arg21, ___callback2, ___object3, method) ((  Il2CppObject * (*) (Func_3_t2574913143 *, float, Il2CppObject *, AsyncCallback_t1363551830 *, Il2CppObject *, const MethodInfo*))Func_3_BeginInvoke_m3116481923_gshared)(__this, ___arg10, ___arg21, ___callback2, ___object3, method)
// TResult System.Func`3<System.Single,System.Object,System.Single>::EndInvoke(System.IAsyncResult)
extern "C"  float Func_3_EndInvoke_m3326496409_gshared (Func_3_t2574913143 * __this, Il2CppObject * ___result0, const MethodInfo* method);
#define Func_3_EndInvoke_m3326496409(__this, ___result0, method) ((  float (*) (Func_3_t2574913143 *, Il2CppObject *, const MethodInfo*))Func_3_EndInvoke_m3326496409_gshared)(__this, ___result0, method)
