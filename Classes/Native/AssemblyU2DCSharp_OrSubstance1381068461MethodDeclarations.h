﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// OrSubstance
struct OrSubstance_t1381068461;
// SubstanceBase`2/Intrusion<System.Boolean,System.Boolean>
struct Intrusion_t1917188776;

#include "codegen/il2cpp-codegen.h"

// System.Void OrSubstance::.ctor()
extern "C"  void OrSubstance__ctor_m2956410718 (OrSubstance_t1381068461 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean OrSubstance::Result()
extern "C"  bool OrSubstance_Result_m2705036175 (OrSubstance_t1381068461 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean OrSubstance::<Result>m__1DC(SubstanceBase`2/Intrusion<System.Boolean,System.Boolean>)
extern "C"  bool OrSubstance_U3CResultU3Em__1DC_m2339587735 (Il2CppObject * __this /* static, unused */, Intrusion_t1917188776 * ___processor0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
