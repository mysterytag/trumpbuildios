﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.OperatorObserverBase`2<System.Double,System.Double>
struct OperatorObserverBase_2_t1921935565;
// UniRx.IObserver`1<System.Double>
struct IObserver_1_t2746515517;
// System.IDisposable
struct IDisposable_t1628921374;

#include "codegen/il2cpp-codegen.h"

// System.Void UniRx.Operators.OperatorObserverBase`2<System.Double,System.Double>::.ctor(UniRx.IObserver`1<TResult>,System.IDisposable)
extern "C"  void OperatorObserverBase_2__ctor_m3632559528_gshared (OperatorObserverBase_2_t1921935565 * __this, Il2CppObject* ___observer0, Il2CppObject * ___cancel1, const MethodInfo* method);
#define OperatorObserverBase_2__ctor_m3632559528(__this, ___observer0, ___cancel1, method) ((  void (*) (OperatorObserverBase_2_t1921935565 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))OperatorObserverBase_2__ctor_m3632559528_gshared)(__this, ___observer0, ___cancel1, method)
// System.Void UniRx.Operators.OperatorObserverBase`2<System.Double,System.Double>::Dispose()
extern "C"  void OperatorObserverBase_2_Dispose_m3668090178_gshared (OperatorObserverBase_2_t1921935565 * __this, const MethodInfo* method);
#define OperatorObserverBase_2_Dispose_m3668090178(__this, method) ((  void (*) (OperatorObserverBase_2_t1921935565 *, const MethodInfo*))OperatorObserverBase_2_Dispose_m3668090178_gshared)(__this, method)
