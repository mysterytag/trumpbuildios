﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UniRx.Operators.DoOnCancelObservable`1<System.Object>
struct DoOnCancelObservable_1_t3202318454;

#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Oper1187768149.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Operators.DoOnCancelObservable`1/DoOnCancel<System.Object>
struct  DoOnCancel_t3043692509  : public OperatorObserverBase_2_t1187768149
{
public:
	// UniRx.Operators.DoOnCancelObservable`1<T> UniRx.Operators.DoOnCancelObservable`1/DoOnCancel::parent
	DoOnCancelObservable_1_t3202318454 * ___parent_2;
	// System.Boolean UniRx.Operators.DoOnCancelObservable`1/DoOnCancel::isCompletedCall
	bool ___isCompletedCall_3;

public:
	inline static int32_t get_offset_of_parent_2() { return static_cast<int32_t>(offsetof(DoOnCancel_t3043692509, ___parent_2)); }
	inline DoOnCancelObservable_1_t3202318454 * get_parent_2() const { return ___parent_2; }
	inline DoOnCancelObservable_1_t3202318454 ** get_address_of_parent_2() { return &___parent_2; }
	inline void set_parent_2(DoOnCancelObservable_1_t3202318454 * value)
	{
		___parent_2 = value;
		Il2CppCodeGenWriteBarrier(&___parent_2, value);
	}

	inline static int32_t get_offset_of_isCompletedCall_3() { return static_cast<int32_t>(offsetof(DoOnCancel_t3043692509, ___isCompletedCall_3)); }
	inline bool get_isCompletedCall_3() const { return ___isCompletedCall_3; }
	inline bool* get_address_of_isCompletedCall_3() { return &___isCompletedCall_3; }
	inline void set_isCompletedCall_3(bool value)
	{
		___isCompletedCall_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
