﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Zenject.GameObjectSingletonProvider
struct GameObjectSingletonProvider_t177180832;
// System.Type
struct Type_t;
// Zenject.DiContainer
struct DiContainer_t2383114449;
// System.String
struct String_t;
// System.Object
struct Il2CppObject;
// Zenject.InjectContext
struct InjectContext_t3456483891;
// System.Collections.Generic.IEnumerable`1<Zenject.ZenjectResolveException>
struct IEnumerable_1_t4073207355;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Type2779229935.h"
#include "AssemblyU2DCSharp_Zenject_DiContainer2383114449.h"
#include "mscorlib_System_String968488902.h"
#include "AssemblyU2DCSharp_Zenject_InjectContext3456483891.h"

// System.Void Zenject.GameObjectSingletonProvider::.ctor(System.Type,Zenject.DiContainer,System.String)
extern "C"  void GameObjectSingletonProvider__ctor_m2455014085 (GameObjectSingletonProvider_t177180832 * __this, Type_t * ___componentType0, DiContainer_t2383114449 * ___container1, String_t* ___name2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type Zenject.GameObjectSingletonProvider::GetInstanceType()
extern "C"  Type_t * GameObjectSingletonProvider_GetInstanceType_m40118684 (GameObjectSingletonProvider_t177180832 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Zenject.GameObjectSingletonProvider::GetInstance(Zenject.InjectContext)
extern "C"  Il2CppObject * GameObjectSingletonProvider_GetInstance_m854078358 (GameObjectSingletonProvider_t177180832 * __this, InjectContext_t3456483891 * ___context0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.IEnumerable`1<Zenject.ZenjectResolveException> Zenject.GameObjectSingletonProvider::ValidateBinding(Zenject.InjectContext)
extern "C"  Il2CppObject* GameObjectSingletonProvider_ValidateBinding_m2811024936 (GameObjectSingletonProvider_t177180832 * __this, InjectContext_t3456483891 * ___context0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
