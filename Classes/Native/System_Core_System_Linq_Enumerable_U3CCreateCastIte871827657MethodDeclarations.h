﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Linq.Enumerable/<CreateCastIterator>c__Iterator0`1<System.Int32>
struct U3CCreateCastIteratorU3Ec__Iterator0_1_t871827657;
// System.Object
struct Il2CppObject;
// System.Collections.IEnumerator
struct IEnumerator_t287207039;
// System.Collections.Generic.IEnumerator`1<System.Int32>
struct IEnumerator_1_t35553939;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Linq.Enumerable/<CreateCastIterator>c__Iterator0`1<System.Int32>::.ctor()
extern "C"  void U3CCreateCastIteratorU3Ec__Iterator0_1__ctor_m217981447_gshared (U3CCreateCastIteratorU3Ec__Iterator0_1_t871827657 * __this, const MethodInfo* method);
#define U3CCreateCastIteratorU3Ec__Iterator0_1__ctor_m217981447(__this, method) ((  void (*) (U3CCreateCastIteratorU3Ec__Iterator0_1_t871827657 *, const MethodInfo*))U3CCreateCastIteratorU3Ec__Iterator0_1__ctor_m217981447_gshared)(__this, method)
// TResult System.Linq.Enumerable/<CreateCastIterator>c__Iterator0`1<System.Int32>::System.Collections.Generic.IEnumerator<TResult>.get_Current()
extern "C"  int32_t U3CCreateCastIteratorU3Ec__Iterator0_1_System_Collections_Generic_IEnumeratorU3CTResultU3E_get_Current_m3542886996_gshared (U3CCreateCastIteratorU3Ec__Iterator0_1_t871827657 * __this, const MethodInfo* method);
#define U3CCreateCastIteratorU3Ec__Iterator0_1_System_Collections_Generic_IEnumeratorU3CTResultU3E_get_Current_m3542886996(__this, method) ((  int32_t (*) (U3CCreateCastIteratorU3Ec__Iterator0_1_t871827657 *, const MethodInfo*))U3CCreateCastIteratorU3Ec__Iterator0_1_System_Collections_Generic_IEnumeratorU3CTResultU3E_get_Current_m3542886996_gshared)(__this, method)
// System.Object System.Linq.Enumerable/<CreateCastIterator>c__Iterator0`1<System.Int32>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CCreateCastIteratorU3Ec__Iterator0_1_System_Collections_IEnumerator_get_Current_m2272640575_gshared (U3CCreateCastIteratorU3Ec__Iterator0_1_t871827657 * __this, const MethodInfo* method);
#define U3CCreateCastIteratorU3Ec__Iterator0_1_System_Collections_IEnumerator_get_Current_m2272640575(__this, method) ((  Il2CppObject * (*) (U3CCreateCastIteratorU3Ec__Iterator0_1_t871827657 *, const MethodInfo*))U3CCreateCastIteratorU3Ec__Iterator0_1_System_Collections_IEnumerator_get_Current_m2272640575_gshared)(__this, method)
// System.Collections.IEnumerator System.Linq.Enumerable/<CreateCastIterator>c__Iterator0`1<System.Int32>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * U3CCreateCastIteratorU3Ec__Iterator0_1_System_Collections_IEnumerable_GetEnumerator_m1515167642_gshared (U3CCreateCastIteratorU3Ec__Iterator0_1_t871827657 * __this, const MethodInfo* method);
#define U3CCreateCastIteratorU3Ec__Iterator0_1_System_Collections_IEnumerable_GetEnumerator_m1515167642(__this, method) ((  Il2CppObject * (*) (U3CCreateCastIteratorU3Ec__Iterator0_1_t871827657 *, const MethodInfo*))U3CCreateCastIteratorU3Ec__Iterator0_1_System_Collections_IEnumerable_GetEnumerator_m1515167642_gshared)(__this, method)
// System.Collections.Generic.IEnumerator`1<TResult> System.Linq.Enumerable/<CreateCastIterator>c__Iterator0`1<System.Int32>::System.Collections.Generic.IEnumerable<TResult>.GetEnumerator()
extern "C"  Il2CppObject* U3CCreateCastIteratorU3Ec__Iterator0_1_System_Collections_Generic_IEnumerableU3CTResultU3E_GetEnumerator_m300677041_gshared (U3CCreateCastIteratorU3Ec__Iterator0_1_t871827657 * __this, const MethodInfo* method);
#define U3CCreateCastIteratorU3Ec__Iterator0_1_System_Collections_Generic_IEnumerableU3CTResultU3E_GetEnumerator_m300677041(__this, method) ((  Il2CppObject* (*) (U3CCreateCastIteratorU3Ec__Iterator0_1_t871827657 *, const MethodInfo*))U3CCreateCastIteratorU3Ec__Iterator0_1_System_Collections_Generic_IEnumerableU3CTResultU3E_GetEnumerator_m300677041_gshared)(__this, method)
// System.Boolean System.Linq.Enumerable/<CreateCastIterator>c__Iterator0`1<System.Int32>::MoveNext()
extern "C"  bool U3CCreateCastIteratorU3Ec__Iterator0_1_MoveNext_m2803816109_gshared (U3CCreateCastIteratorU3Ec__Iterator0_1_t871827657 * __this, const MethodInfo* method);
#define U3CCreateCastIteratorU3Ec__Iterator0_1_MoveNext_m2803816109(__this, method) ((  bool (*) (U3CCreateCastIteratorU3Ec__Iterator0_1_t871827657 *, const MethodInfo*))U3CCreateCastIteratorU3Ec__Iterator0_1_MoveNext_m2803816109_gshared)(__this, method)
// System.Void System.Linq.Enumerable/<CreateCastIterator>c__Iterator0`1<System.Int32>::Dispose()
extern "C"  void U3CCreateCastIteratorU3Ec__Iterator0_1_Dispose_m3220023428_gshared (U3CCreateCastIteratorU3Ec__Iterator0_1_t871827657 * __this, const MethodInfo* method);
#define U3CCreateCastIteratorU3Ec__Iterator0_1_Dispose_m3220023428(__this, method) ((  void (*) (U3CCreateCastIteratorU3Ec__Iterator0_1_t871827657 *, const MethodInfo*))U3CCreateCastIteratorU3Ec__Iterator0_1_Dispose_m3220023428_gshared)(__this, method)
// System.Void System.Linq.Enumerable/<CreateCastIterator>c__Iterator0`1<System.Int32>::Reset()
extern "C"  void U3CCreateCastIteratorU3Ec__Iterator0_1_Reset_m2159381684_gshared (U3CCreateCastIteratorU3Ec__Iterator0_1_t871827657 * __this, const MethodInfo* method);
#define U3CCreateCastIteratorU3Ec__Iterator0_1_Reset_m2159381684(__this, method) ((  void (*) (U3CCreateCastIteratorU3Ec__Iterator0_1_t871827657 *, const MethodInfo*))U3CCreateCastIteratorU3Ec__Iterator0_1_Reset_m2159381684_gshared)(__this, method)
