﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Events.UnityEvent`2<System.Object,System.Object>
struct UnityEvent_2_t3775219180;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.UnityEventExtensions/<AsObservable>c__AnonStorey98`2<System.Object,System.Object>
struct  U3CAsObservableU3Ec__AnonStorey98_2_t1661826968  : public Il2CppObject
{
public:
	// UnityEngine.Events.UnityEvent`2<T0,T1> UniRx.UnityEventExtensions/<AsObservable>c__AnonStorey98`2::unityEvent
	UnityEvent_2_t3775219180 * ___unityEvent_0;

public:
	inline static int32_t get_offset_of_unityEvent_0() { return static_cast<int32_t>(offsetof(U3CAsObservableU3Ec__AnonStorey98_2_t1661826968, ___unityEvent_0)); }
	inline UnityEvent_2_t3775219180 * get_unityEvent_0() const { return ___unityEvent_0; }
	inline UnityEvent_2_t3775219180 ** get_address_of_unityEvent_0() { return &___unityEvent_0; }
	inline void set_unityEvent_0(UnityEvent_2_t3775219180 * value)
	{
		___unityEvent_0 = value;
		Il2CppCodeGenWriteBarrier(&___unityEvent_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
