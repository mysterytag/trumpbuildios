﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Stream_1_gen1107661009MethodDeclarations.h"

// System.Void Stream`1<UniRx.CollectionAddEvent`1<DonateButton>>::.ctor()
#define Stream_1__ctor_m997411625(__this, method) ((  void (*) (Stream_1_t941308030 *, const MethodInfo*))Stream_1__ctor_m3235287902_gshared)(__this, method)
// System.Void Stream`1<UniRx.CollectionAddEvent`1<DonateButton>>::Send(T)
#define Stream_1_Send_m3821596475(__this, ___value0, method) ((  void (*) (Stream_1_t941308030 *, CollectionAddEvent_1_t2250169008 , const MethodInfo*))Stream_1_Send_m1764505456_gshared)(__this, ___value0, method)
// System.Void Stream`1<UniRx.CollectionAddEvent`1<DonateButton>>::SendInTransaction(T,System.Int32)
#define Stream_1_SendInTransaction_m4026646289(__this, ___value0, ___p1, method) ((  void (*) (Stream_1_t941308030 *, CollectionAddEvent_1_t2250169008 , int32_t, const MethodInfo*))Stream_1_SendInTransaction_m1512275718_gshared)(__this, ___value0, ___p1, method)
// System.IDisposable Stream`1<UniRx.CollectionAddEvent`1<DonateButton>>::Listen(System.Action`1<T>,Priority)
#define Stream_1_Listen_m1305654029(__this, ___action0, ___priority1, method) ((  Il2CppObject * (*) (Stream_1_t941308030 *, Action_1_t2398621713 *, int32_t, const MethodInfo*))Stream_1_Listen_m1890769608_gshared)(__this, ___action0, ___priority1, method)
// System.IDisposable Stream`1<UniRx.CollectionAddEvent`1<DonateButton>>::Listen(System.Action,Priority)
#define Stream_1_Listen_m1627642362(__this, ___action0, ___p1, method) ((  Il2CppObject * (*) (Stream_1_t941308030 *, Action_t437523947 *, int32_t, const MethodInfo*))Stream_1_Listen_m3411183967_gshared)(__this, ___action0, ___p1, method)
// System.Void Stream`1<UniRx.CollectionAddEvent`1<DonateButton>>::Dispose()
#define Stream_1_Dispose_m633147686(__this, method) ((  void (*) (Stream_1_t941308030 *, const MethodInfo*))Stream_1_Dispose_m3748601883_gshared)(__this, method)
// System.Void Stream`1<UniRx.CollectionAddEvent`1<DonateButton>>::SetInputStream(IStream`1<T>)
#define Stream_1_SetInputStream_m3731212407(__this, ___stream0, method) ((  void (*) (Stream_1_t941308030 *, Il2CppObject*, const MethodInfo*))Stream_1_SetInputStream_m453976930_gshared)(__this, ___stream0, method)
// System.Void Stream`1<UniRx.CollectionAddEvent`1<DonateButton>>::ClearInputStream()
#define Stream_1_ClearInputStream_m2197312024(__this, method) ((  void (*) (Stream_1_t941308030 *, const MethodInfo*))Stream_1_ClearInputStream_m1610565315_gshared)(__this, method)
// System.Void Stream`1<UniRx.CollectionAddEvent`1<DonateButton>>::TransactionIterationFinished()
#define Stream_1_TransactionIterationFinished_m1689890284(__this, method) ((  void (*) (Stream_1_t941308030 *, const MethodInfo*))Stream_1_TransactionIterationFinished_m477624855_gshared)(__this, method)
// System.Void Stream`1<UniRx.CollectionAddEvent`1<DonateButton>>::Unpack(System.Int32)
#define Stream_1_Unpack_m3069760510(__this, ___p0, method) ((  void (*) (Stream_1_t941308030 *, int32_t, const MethodInfo*))Stream_1_Unpack_m1846888937_gshared)(__this, ___p0, method)
// System.Void Stream`1<UniRx.CollectionAddEvent`1<DonateButton>>::<SetInputStream>m__1BA(T)
#define Stream_1_U3CSetInputStreamU3Em__1BA_m1772990854(__this, ___val0, method) ((  void (*) (Stream_1_t941308030 *, CollectionAddEvent_1_t2250169008 , const MethodInfo*))Stream_1_U3CSetInputStreamU3Em__1BA_m4229248635_gshared)(__this, ___val0, method)
