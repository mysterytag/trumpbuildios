﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Zenject.TaskUpdater`1/<RemoveTask>c__AnonStorey191<System.Object>
struct U3CRemoveTaskU3Ec__AnonStorey191_t949964860;
// Zenject.TaskUpdater`1/TaskInfo<System.Object>
struct TaskInfo_t1064508404;

#include "codegen/il2cpp-codegen.h"

// System.Void Zenject.TaskUpdater`1/<RemoveTask>c__AnonStorey191<System.Object>::.ctor()
extern "C"  void U3CRemoveTaskU3Ec__AnonStorey191__ctor_m414817283_gshared (U3CRemoveTaskU3Ec__AnonStorey191_t949964860 * __this, const MethodInfo* method);
#define U3CRemoveTaskU3Ec__AnonStorey191__ctor_m414817283(__this, method) ((  void (*) (U3CRemoveTaskU3Ec__AnonStorey191_t949964860 *, const MethodInfo*))U3CRemoveTaskU3Ec__AnonStorey191__ctor_m414817283_gshared)(__this, method)
// System.Boolean Zenject.TaskUpdater`1/<RemoveTask>c__AnonStorey191<System.Object>::<>m__2E6(Zenject.TaskUpdater`1/TaskInfo<TTask>)
extern "C"  bool U3CRemoveTaskU3Ec__AnonStorey191_U3CU3Em__2E6_m1444281368_gshared (U3CRemoveTaskU3Ec__AnonStorey191_t949964860 * __this, TaskInfo_t1064508404 * ___x0, const MethodInfo* method);
#define U3CRemoveTaskU3Ec__AnonStorey191_U3CU3Em__2E6_m1444281368(__this, ___x0, method) ((  bool (*) (U3CRemoveTaskU3Ec__AnonStorey191_t949964860 *, TaskInfo_t1064508404 *, const MethodInfo*))U3CRemoveTaskU3Ec__AnonStorey191_U3CU3Em__2E6_m1444281368_gshared)(__this, ___x0, method)
