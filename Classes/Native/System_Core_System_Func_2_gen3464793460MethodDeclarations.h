﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Func`2<System.Single,System.Single>
struct Func_2_t3464793460;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t537683269;
// System.AsyncCallback
struct AsyncCallback_t1363551830;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_IntPtr676692020.h"
#include "mscorlib_System_AsyncCallback1363551830.h"

// System.Void System.Func`2<System.Single,System.Single>::.ctor(System.Object,System.IntPtr)
extern "C"  void Func_2__ctor_m2021156074_gshared (Func_2_t3464793460 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method);
#define Func_2__ctor_m2021156074(__this, ___object0, ___method1, method) ((  void (*) (Func_2_t3464793460 *, Il2CppObject *, IntPtr_t, const MethodInfo*))Func_2__ctor_m2021156074_gshared)(__this, ___object0, ___method1, method)
// TResult System.Func`2<System.Single,System.Single>::Invoke(T)
extern "C"  float Func_2_Invoke_m248194216_gshared (Func_2_t3464793460 * __this, float ___arg10, const MethodInfo* method);
#define Func_2_Invoke_m248194216(__this, ___arg10, method) ((  float (*) (Func_2_t3464793460 *, float, const MethodInfo*))Func_2_Invoke_m248194216_gshared)(__this, ___arg10, method)
// System.IAsyncResult System.Func`2<System.Single,System.Single>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * Func_2_BeginInvoke_m3674748119_gshared (Func_2_t3464793460 * __this, float ___arg10, AsyncCallback_t1363551830 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method);
#define Func_2_BeginInvoke_m3674748119(__this, ___arg10, ___callback1, ___object2, method) ((  Il2CppObject * (*) (Func_2_t3464793460 *, float, AsyncCallback_t1363551830 *, Il2CppObject *, const MethodInfo*))Func_2_BeginInvoke_m3674748119_gshared)(__this, ___arg10, ___callback1, ___object2, method)
// TResult System.Func`2<System.Single,System.Single>::EndInvoke(System.IAsyncResult)
extern "C"  float Func_2_EndInvoke_m48896044_gshared (Func_2_t3464793460 * __this, Il2CppObject * ___result0, const MethodInfo* method);
#define Func_2_EndInvoke_m48896044(__this, ___result0, method) ((  float (*) (Func_2_t3464793460 *, Il2CppObject *, const MethodInfo*))Func_2_EndInvoke_m48896044_gshared)(__this, ___result0, method)
