﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UniRx.Operators.SelectManyObservable`3<System.Object,UniRx.Unit,System.Object>
struct SelectManyObservable_3_t1259497621;
// UniRx.CompositeDisposable
struct CompositeDisposable_t1894629977;
// System.Object
struct Il2CppObject;
// UniRx.SingleAssignmentDisposable
struct SingleAssignmentDisposable_t2336378823;

#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Oper1187768149.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Operators.SelectManyObservable`3/SelectManyObserverWithIndex<System.Object,UniRx.Unit,System.Object>
struct  SelectManyObserverWithIndex_t2099930242  : public OperatorObserverBase_2_t1187768149
{
public:
	// UniRx.Operators.SelectManyObservable`3<TSource,TCollection,TResult> UniRx.Operators.SelectManyObservable`3/SelectManyObserverWithIndex::parent
	SelectManyObservable_3_t1259497621 * ___parent_2;
	// UniRx.CompositeDisposable UniRx.Operators.SelectManyObservable`3/SelectManyObserverWithIndex::collectionDisposable
	CompositeDisposable_t1894629977 * ___collectionDisposable_3;
	// System.Object UniRx.Operators.SelectManyObservable`3/SelectManyObserverWithIndex::gate
	Il2CppObject * ___gate_4;
	// System.Boolean UniRx.Operators.SelectManyObservable`3/SelectManyObserverWithIndex::isStopped
	bool ___isStopped_5;
	// UniRx.SingleAssignmentDisposable UniRx.Operators.SelectManyObservable`3/SelectManyObserverWithIndex::sourceDisposable
	SingleAssignmentDisposable_t2336378823 * ___sourceDisposable_6;
	// System.Int32 UniRx.Operators.SelectManyObservable`3/SelectManyObserverWithIndex::index
	int32_t ___index_7;

public:
	inline static int32_t get_offset_of_parent_2() { return static_cast<int32_t>(offsetof(SelectManyObserverWithIndex_t2099930242, ___parent_2)); }
	inline SelectManyObservable_3_t1259497621 * get_parent_2() const { return ___parent_2; }
	inline SelectManyObservable_3_t1259497621 ** get_address_of_parent_2() { return &___parent_2; }
	inline void set_parent_2(SelectManyObservable_3_t1259497621 * value)
	{
		___parent_2 = value;
		Il2CppCodeGenWriteBarrier(&___parent_2, value);
	}

	inline static int32_t get_offset_of_collectionDisposable_3() { return static_cast<int32_t>(offsetof(SelectManyObserverWithIndex_t2099930242, ___collectionDisposable_3)); }
	inline CompositeDisposable_t1894629977 * get_collectionDisposable_3() const { return ___collectionDisposable_3; }
	inline CompositeDisposable_t1894629977 ** get_address_of_collectionDisposable_3() { return &___collectionDisposable_3; }
	inline void set_collectionDisposable_3(CompositeDisposable_t1894629977 * value)
	{
		___collectionDisposable_3 = value;
		Il2CppCodeGenWriteBarrier(&___collectionDisposable_3, value);
	}

	inline static int32_t get_offset_of_gate_4() { return static_cast<int32_t>(offsetof(SelectManyObserverWithIndex_t2099930242, ___gate_4)); }
	inline Il2CppObject * get_gate_4() const { return ___gate_4; }
	inline Il2CppObject ** get_address_of_gate_4() { return &___gate_4; }
	inline void set_gate_4(Il2CppObject * value)
	{
		___gate_4 = value;
		Il2CppCodeGenWriteBarrier(&___gate_4, value);
	}

	inline static int32_t get_offset_of_isStopped_5() { return static_cast<int32_t>(offsetof(SelectManyObserverWithIndex_t2099930242, ___isStopped_5)); }
	inline bool get_isStopped_5() const { return ___isStopped_5; }
	inline bool* get_address_of_isStopped_5() { return &___isStopped_5; }
	inline void set_isStopped_5(bool value)
	{
		___isStopped_5 = value;
	}

	inline static int32_t get_offset_of_sourceDisposable_6() { return static_cast<int32_t>(offsetof(SelectManyObserverWithIndex_t2099930242, ___sourceDisposable_6)); }
	inline SingleAssignmentDisposable_t2336378823 * get_sourceDisposable_6() const { return ___sourceDisposable_6; }
	inline SingleAssignmentDisposable_t2336378823 ** get_address_of_sourceDisposable_6() { return &___sourceDisposable_6; }
	inline void set_sourceDisposable_6(SingleAssignmentDisposable_t2336378823 * value)
	{
		___sourceDisposable_6 = value;
		Il2CppCodeGenWriteBarrier(&___sourceDisposable_6, value);
	}

	inline static int32_t get_offset_of_index_7() { return static_cast<int32_t>(offsetof(SelectManyObserverWithIndex_t2099930242, ___index_7)); }
	inline int32_t get_index_7() const { return ___index_7; }
	inline int32_t* get_address_of_index_7() { return &___index_7; }
	inline void set_index_7(int32_t value)
	{
		___index_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
