﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Zenject.BindingValidator/<ValidateObjectGraph>c__Iterator4A
struct U3CValidateObjectGraphU3Ec__Iterator4A_t3045653917;
// Zenject.ZenjectResolveException
struct ZenjectResolveException_t1201052999;
// System.Object
struct Il2CppObject;
// System.Collections.IEnumerator
struct IEnumerator_t287207039;
// System.Collections.Generic.IEnumerator`1<Zenject.ZenjectResolveException>
struct IEnumerator_1_t2684159447;
// System.String
struct String_t;
// System.Type
struct Type_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Type2779229935.h"

// System.Void Zenject.BindingValidator/<ValidateObjectGraph>c__Iterator4A::.ctor()
extern "C"  void U3CValidateObjectGraphU3Ec__Iterator4A__ctor_m4181872491 (U3CValidateObjectGraphU3Ec__Iterator4A_t3045653917 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Zenject.ZenjectResolveException Zenject.BindingValidator/<ValidateObjectGraph>c__Iterator4A::System.Collections.Generic.IEnumerator<Zenject.ZenjectResolveException>.get_Current()
extern "C"  ZenjectResolveException_t1201052999 * U3CValidateObjectGraphU3Ec__Iterator4A_System_Collections_Generic_IEnumeratorU3CZenject_ZenjectResolveExceptionU3E_get_Current_m2523824308 (U3CValidateObjectGraphU3Ec__Iterator4A_t3045653917 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Zenject.BindingValidator/<ValidateObjectGraph>c__Iterator4A::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CValidateObjectGraphU3Ec__Iterator4A_System_Collections_IEnumerator_get_Current_m4032455643 (U3CValidateObjectGraphU3Ec__Iterator4A_t3045653917 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator Zenject.BindingValidator/<ValidateObjectGraph>c__Iterator4A::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * U3CValidateObjectGraphU3Ec__Iterator4A_System_Collections_IEnumerable_GetEnumerator_m1873888662 (U3CValidateObjectGraphU3Ec__Iterator4A_t3045653917 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.IEnumerator`1<Zenject.ZenjectResolveException> Zenject.BindingValidator/<ValidateObjectGraph>c__Iterator4A::System.Collections.Generic.IEnumerable<Zenject.ZenjectResolveException>.GetEnumerator()
extern "C"  Il2CppObject* U3CValidateObjectGraphU3Ec__Iterator4A_System_Collections_Generic_IEnumerableU3CZenject_ZenjectResolveExceptionU3E_GetEnumerator_m1596683861 (U3CValidateObjectGraphU3Ec__Iterator4A_t3045653917 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Zenject.BindingValidator/<ValidateObjectGraph>c__Iterator4A::MoveNext()
extern "C"  bool U3CValidateObjectGraphU3Ec__Iterator4A_MoveNext_m3429247145 (U3CValidateObjectGraphU3Ec__Iterator4A_t3045653917 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Zenject.BindingValidator/<ValidateObjectGraph>c__Iterator4A::Dispose()
extern "C"  void U3CValidateObjectGraphU3Ec__Iterator4A_Dispose_m2883325160 (U3CValidateObjectGraphU3Ec__Iterator4A_t3045653917 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Zenject.BindingValidator/<ValidateObjectGraph>c__Iterator4A::Reset()
extern "C"  void U3CValidateObjectGraphU3Ec__Iterator4A_Reset_m1828305432 (U3CValidateObjectGraphU3Ec__Iterator4A_t3045653917 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Zenject.BindingValidator/<ValidateObjectGraph>c__Iterator4A::<>m__2BA(System.Type)
extern "C"  String_t* U3CValidateObjectGraphU3Ec__Iterator4A_U3CU3Em__2BA_m123886813 (Il2CppObject * __this /* static, unused */, Type_t * ___x0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
