﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SponsorPay.IOSSponsorPayPlugin/<LaunchOfferWall>c__AnonStorey152
struct  U3CLaunchOfferWallU3Ec__AnonStorey152_t299200718  : public Il2CppObject
{
public:
	// System.String SponsorPay.IOSSponsorPayPlugin/<LaunchOfferWall>c__AnonStorey152::credentialsToken
	String_t* ___credentialsToken_0;
	// System.String SponsorPay.IOSSponsorPayPlugin/<LaunchOfferWall>c__AnonStorey152::currencyName
	String_t* ___currencyName_1;
	// System.String SponsorPay.IOSSponsorPayPlugin/<LaunchOfferWall>c__AnonStorey152::placementId
	String_t* ___placementId_2;

public:
	inline static int32_t get_offset_of_credentialsToken_0() { return static_cast<int32_t>(offsetof(U3CLaunchOfferWallU3Ec__AnonStorey152_t299200718, ___credentialsToken_0)); }
	inline String_t* get_credentialsToken_0() const { return ___credentialsToken_0; }
	inline String_t** get_address_of_credentialsToken_0() { return &___credentialsToken_0; }
	inline void set_credentialsToken_0(String_t* value)
	{
		___credentialsToken_0 = value;
		Il2CppCodeGenWriteBarrier(&___credentialsToken_0, value);
	}

	inline static int32_t get_offset_of_currencyName_1() { return static_cast<int32_t>(offsetof(U3CLaunchOfferWallU3Ec__AnonStorey152_t299200718, ___currencyName_1)); }
	inline String_t* get_currencyName_1() const { return ___currencyName_1; }
	inline String_t** get_address_of_currencyName_1() { return &___currencyName_1; }
	inline void set_currencyName_1(String_t* value)
	{
		___currencyName_1 = value;
		Il2CppCodeGenWriteBarrier(&___currencyName_1, value);
	}

	inline static int32_t get_offset_of_placementId_2() { return static_cast<int32_t>(offsetof(U3CLaunchOfferWallU3Ec__AnonStorey152_t299200718, ___placementId_2)); }
	inline String_t* get_placementId_2() const { return ___placementId_2; }
	inline String_t** get_address_of_placementId_2() { return &___placementId_2; }
	inline void set_placementId_2(String_t* value)
	{
		___placementId_2 = value;
		Il2CppCodeGenWriteBarrier(&___placementId_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
