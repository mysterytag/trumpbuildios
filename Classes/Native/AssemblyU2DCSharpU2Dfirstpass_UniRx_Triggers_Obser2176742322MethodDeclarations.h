﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Triggers.ObservableUpdateSelectedTrigger
struct ObservableUpdateSelectedTrigger_t2176742322;
// UnityEngine.EventSystems.BaseEventData
struct BaseEventData_t3547103726;
// UniRx.IObservable`1<UnityEngine.EventSystems.BaseEventData>
struct IObservable_1_t3305902090;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_BaseEventD3547103726.h"

// System.Void UniRx.Triggers.ObservableUpdateSelectedTrigger::.ctor()
extern "C"  void ObservableUpdateSelectedTrigger__ctor_m1728316537 (ObservableUpdateSelectedTrigger_t2176742322 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.Triggers.ObservableUpdateSelectedTrigger::UnityEngine.EventSystems.IUpdateSelectedHandler.OnUpdateSelected(UnityEngine.EventSystems.BaseEventData)
extern "C"  void ObservableUpdateSelectedTrigger_UnityEngine_EventSystems_IUpdateSelectedHandler_OnUpdateSelected_m2890696812 (ObservableUpdateSelectedTrigger_t2176742322 * __this, BaseEventData_t3547103726 * ___eventData0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UniRx.IObservable`1<UnityEngine.EventSystems.BaseEventData> UniRx.Triggers.ObservableUpdateSelectedTrigger::OnUpdateSelectedAsObservable()
extern "C"  Il2CppObject* ObservableUpdateSelectedTrigger_OnUpdateSelectedAsObservable_m1989980981 (ObservableUpdateSelectedTrigger_t2176742322 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.Triggers.ObservableUpdateSelectedTrigger::RaiseOnCompletedOnDestroy()
extern "C"  void ObservableUpdateSelectedTrigger_RaiseOnCompletedOnDestroy_m859497778 (ObservableUpdateSelectedTrigger_t2176742322 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
