﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.ClientModels.GetCharacterDataResult
struct GetCharacterDataResult_t1300547946;
// System.String
struct String_t;
// System.Collections.Generic.Dictionary`2<System.String,PlayFab.ClientModels.UserDataRecord>
struct Dictionary_2_t1801537638;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String968488902.h"

// System.Void PlayFab.ClientModels.GetCharacterDataResult::.ctor()
extern "C"  void GetCharacterDataResult__ctor_m54703763 (GetCharacterDataResult_t1300547946 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PlayFab.ClientModels.GetCharacterDataResult::get_CharacterId()
extern "C"  String_t* GetCharacterDataResult_get_CharacterId_m2587702927 (GetCharacterDataResult_t1300547946 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.GetCharacterDataResult::set_CharacterId(System.String)
extern "C"  void GetCharacterDataResult_set_CharacterId_m3013253706 (GetCharacterDataResult_t1300547946 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.Dictionary`2<System.String,PlayFab.ClientModels.UserDataRecord> PlayFab.ClientModels.GetCharacterDataResult::get_Data()
extern "C"  Dictionary_2_t1801537638 * GetCharacterDataResult_get_Data_m763931554 (GetCharacterDataResult_t1300547946 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.GetCharacterDataResult::set_Data(System.Collections.Generic.Dictionary`2<System.String,PlayFab.ClientModels.UserDataRecord>)
extern "C"  void GetCharacterDataResult_set_Data_m2932110941 (GetCharacterDataResult_t1300547946 * __this, Dictionary_2_t1801537638 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 PlayFab.ClientModels.GetCharacterDataResult::get_DataVersion()
extern "C"  uint32_t GetCharacterDataResult_get_DataVersion_m3983620017 (GetCharacterDataResult_t1300547946 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.GetCharacterDataResult::set_DataVersion(System.UInt32)
extern "C"  void GetCharacterDataResult_set_DataVersion_m2692042584 (GetCharacterDataResult_t1300547946 * __this, uint32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
