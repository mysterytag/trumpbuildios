﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// FriendInfo
struct FriendInfo_t236470412;
// InviteFriendsWindow
struct InviteFriendsWindow_t1648561468;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// InviteFriendsWindow/<Start>c__AnonStoreyE9
struct  U3CStartU3Ec__AnonStoreyE9_t1152510435  : public Il2CppObject
{
public:
	// FriendInfo InviteFriendsWindow/<Start>c__AnonStoreyE9::info
	FriendInfo_t236470412 * ___info_0;
	// InviteFriendsWindow InviteFriendsWindow/<Start>c__AnonStoreyE9::<>f__this
	InviteFriendsWindow_t1648561468 * ___U3CU3Ef__this_1;

public:
	inline static int32_t get_offset_of_info_0() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__AnonStoreyE9_t1152510435, ___info_0)); }
	inline FriendInfo_t236470412 * get_info_0() const { return ___info_0; }
	inline FriendInfo_t236470412 ** get_address_of_info_0() { return &___info_0; }
	inline void set_info_0(FriendInfo_t236470412 * value)
	{
		___info_0 = value;
		Il2CppCodeGenWriteBarrier(&___info_0, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__this_1() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__AnonStoreyE9_t1152510435, ___U3CU3Ef__this_1)); }
	inline InviteFriendsWindow_t1648561468 * get_U3CU3Ef__this_1() const { return ___U3CU3Ef__this_1; }
	inline InviteFriendsWindow_t1648561468 ** get_address_of_U3CU3Ef__this_1() { return &___U3CU3Ef__this_1; }
	inline void set_U3CU3Ef__this_1(InviteFriendsWindow_t1648561468 * value)
	{
		___U3CU3Ef__this_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__this_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
