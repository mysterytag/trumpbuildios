﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Zenject.GameObjectFactory
struct GameObjectFactory_t3309736270;

#include "codegen/il2cpp-codegen.h"

// System.Void Zenject.GameObjectFactory::.ctor()
extern "C"  void GameObjectFactory__ctor_m1998996753 (GameObjectFactory_t3309736270 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
