﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// ILiving
struct ILiving_t2639664210;
// System.IDisposable
struct IDisposable_t1628921374;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SubstanceBase`2/Intrusion<System.Single,System.Single>
struct  Intrusion_t78896424  : public Il2CppObject
{
public:
	// ILiving SubstanceBase`2/Intrusion::intruder
	Il2CppObject * ___intruder_0;
	// InfT SubstanceBase`2/Intrusion::influence
	float ___influence_1;
	// System.IDisposable SubstanceBase`2/Intrusion::awakeConnection
	Il2CppObject * ___awakeConnection_2;
	// System.IDisposable SubstanceBase`2/Intrusion::updateConnection
	Il2CppObject * ___updateConnection_3;

public:
	inline static int32_t get_offset_of_intruder_0() { return static_cast<int32_t>(offsetof(Intrusion_t78896424, ___intruder_0)); }
	inline Il2CppObject * get_intruder_0() const { return ___intruder_0; }
	inline Il2CppObject ** get_address_of_intruder_0() { return &___intruder_0; }
	inline void set_intruder_0(Il2CppObject * value)
	{
		___intruder_0 = value;
		Il2CppCodeGenWriteBarrier(&___intruder_0, value);
	}

	inline static int32_t get_offset_of_influence_1() { return static_cast<int32_t>(offsetof(Intrusion_t78896424, ___influence_1)); }
	inline float get_influence_1() const { return ___influence_1; }
	inline float* get_address_of_influence_1() { return &___influence_1; }
	inline void set_influence_1(float value)
	{
		___influence_1 = value;
	}

	inline static int32_t get_offset_of_awakeConnection_2() { return static_cast<int32_t>(offsetof(Intrusion_t78896424, ___awakeConnection_2)); }
	inline Il2CppObject * get_awakeConnection_2() const { return ___awakeConnection_2; }
	inline Il2CppObject ** get_address_of_awakeConnection_2() { return &___awakeConnection_2; }
	inline void set_awakeConnection_2(Il2CppObject * value)
	{
		___awakeConnection_2 = value;
		Il2CppCodeGenWriteBarrier(&___awakeConnection_2, value);
	}

	inline static int32_t get_offset_of_updateConnection_3() { return static_cast<int32_t>(offsetof(Intrusion_t78896424, ___updateConnection_3)); }
	inline Il2CppObject * get_updateConnection_3() const { return ___updateConnection_3; }
	inline Il2CppObject ** get_address_of_updateConnection_3() { return &___updateConnection_3; }
	inline void set_updateConnection_3(Il2CppObject * value)
	{
		___updateConnection_3 = value;
		Il2CppCodeGenWriteBarrier(&___updateConnection_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
