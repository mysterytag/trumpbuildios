﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameAnalyticsSDK.Game
struct  Game_t3902568756  : public Il2CppObject
{
public:
	// System.String GameAnalyticsSDK.Game::<Name>k__BackingField
	String_t* ___U3CNameU3Ek__BackingField_0;
	// System.Int32 GameAnalyticsSDK.Game::<ID>k__BackingField
	int32_t ___U3CIDU3Ek__BackingField_1;
	// System.String GameAnalyticsSDK.Game::<GameKey>k__BackingField
	String_t* ___U3CGameKeyU3Ek__BackingField_2;
	// System.String GameAnalyticsSDK.Game::<SecretKey>k__BackingField
	String_t* ___U3CSecretKeyU3Ek__BackingField_3;

public:
	inline static int32_t get_offset_of_U3CNameU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(Game_t3902568756, ___U3CNameU3Ek__BackingField_0)); }
	inline String_t* get_U3CNameU3Ek__BackingField_0() const { return ___U3CNameU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CNameU3Ek__BackingField_0() { return &___U3CNameU3Ek__BackingField_0; }
	inline void set_U3CNameU3Ek__BackingField_0(String_t* value)
	{
		___U3CNameU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier(&___U3CNameU3Ek__BackingField_0, value);
	}

	inline static int32_t get_offset_of_U3CIDU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(Game_t3902568756, ___U3CIDU3Ek__BackingField_1)); }
	inline int32_t get_U3CIDU3Ek__BackingField_1() const { return ___U3CIDU3Ek__BackingField_1; }
	inline int32_t* get_address_of_U3CIDU3Ek__BackingField_1() { return &___U3CIDU3Ek__BackingField_1; }
	inline void set_U3CIDU3Ek__BackingField_1(int32_t value)
	{
		___U3CIDU3Ek__BackingField_1 = value;
	}

	inline static int32_t get_offset_of_U3CGameKeyU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(Game_t3902568756, ___U3CGameKeyU3Ek__BackingField_2)); }
	inline String_t* get_U3CGameKeyU3Ek__BackingField_2() const { return ___U3CGameKeyU3Ek__BackingField_2; }
	inline String_t** get_address_of_U3CGameKeyU3Ek__BackingField_2() { return &___U3CGameKeyU3Ek__BackingField_2; }
	inline void set_U3CGameKeyU3Ek__BackingField_2(String_t* value)
	{
		___U3CGameKeyU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier(&___U3CGameKeyU3Ek__BackingField_2, value);
	}

	inline static int32_t get_offset_of_U3CSecretKeyU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(Game_t3902568756, ___U3CSecretKeyU3Ek__BackingField_3)); }
	inline String_t* get_U3CSecretKeyU3Ek__BackingField_3() const { return ___U3CSecretKeyU3Ek__BackingField_3; }
	inline String_t** get_address_of_U3CSecretKeyU3Ek__BackingField_3() { return &___U3CSecretKeyU3Ek__BackingField_3; }
	inline void set_U3CSecretKeyU3Ek__BackingField_3(String_t* value)
	{
		___U3CSecretKeyU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier(&___U3CSecretKeyU3Ek__BackingField_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
