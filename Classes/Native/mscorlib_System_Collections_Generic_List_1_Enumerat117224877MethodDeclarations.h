﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera4014815677MethodDeclarations.h"

// System.Void System.Collections.Generic.List`1/Enumerator<System.Collections.Generic.List`1<System.Action>>::.ctor(System.Collections.Generic.List`1<T>)
#define Enumerator__ctor_m2996858394(__this, ___l0, method) ((  void (*) (Enumerator_t117224877 *, List_1_t2031441885 *, const MethodInfo*))Enumerator__ctor_m1029849669_gshared)(__this, ___l0, method)
// System.Void System.Collections.Generic.List`1/Enumerator<System.Collections.Generic.List`1<System.Action>>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m4038376952(__this, method) ((  void (*) (Enumerator_t117224877 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m771996397_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1/Enumerator<System.Collections.Generic.List`1<System.Action>>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m675175150(__this, method) ((  Il2CppObject * (*) (Enumerator_t117224877 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m3561903705_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<System.Collections.Generic.List`1<System.Action>>::Dispose()
#define Enumerator_Dispose_m2927534719(__this, method) ((  void (*) (Enumerator_t117224877 *, const MethodInfo*))Enumerator_Dispose_m2904289642_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<System.Collections.Generic.List`1<System.Action>>::VerifyState()
#define Enumerator_VerifyState_m2593065528(__this, method) ((  void (*) (Enumerator_t117224877 *, const MethodInfo*))Enumerator_VerifyState_m1522854819_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<System.Collections.Generic.List`1<System.Action>>::MoveNext()
#define Enumerator_MoveNext_m3679763601(__this, method) ((  bool (*) (Enumerator_t117224877 *, const MethodInfo*))Enumerator_MoveNext_m302222300_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<System.Collections.Generic.List`1<System.Action>>::get_Current()
#define Enumerator_get_Current_m3696336811(__this, method) ((  List_1_t1234482916 * (*) (Enumerator_t117224877 *, const MethodInfo*))Enumerator_get_Current_m1113061044_gshared)(__this, method)
