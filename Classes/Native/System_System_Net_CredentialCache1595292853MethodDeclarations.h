﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Net.CredentialCache
struct CredentialCache_t1595292853;
// System.Net.ICredentials
struct ICredentials_t2307785309;
// System.Net.NetworkCredential
struct NetworkCredential_t2360216687;
// System.Uri
struct Uri_t2776692961;
// System.String
struct String_t;
// System.Collections.IEnumerator
struct IEnumerator_t287207039;

#include "codegen/il2cpp-codegen.h"
#include "System_System_Uri2776692961.h"
#include "mscorlib_System_String968488902.h"
#include "System_System_Net_NetworkCredential2360216687.h"

// System.Void System.Net.CredentialCache::.ctor()
extern "C"  void CredentialCache__ctor_m3746433793 (CredentialCache_t1595292853 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.CredentialCache::.cctor()
extern "C"  void CredentialCache__cctor_m3988201676 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Net.ICredentials System.Net.CredentialCache::get_DefaultCredentials()
extern "C"  Il2CppObject * CredentialCache_get_DefaultCredentials_m2813166999 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Net.NetworkCredential System.Net.CredentialCache::get_DefaultNetworkCredentials()
extern "C"  NetworkCredential_t2360216687 * CredentialCache_get_DefaultNetworkCredentials_m1430092859 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Net.NetworkCredential System.Net.CredentialCache::GetCredential(System.Uri,System.String)
extern "C"  NetworkCredential_t2360216687 * CredentialCache_GetCredential_m2314780433 (CredentialCache_t1595292853 * __this, Uri_t2776692961 * ___uriPrefix0, String_t* ___authType1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator System.Net.CredentialCache::GetEnumerator()
extern "C"  Il2CppObject * CredentialCache_GetEnumerator_m612413167 (CredentialCache_t1595292853 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.CredentialCache::Add(System.Uri,System.String,System.Net.NetworkCredential)
extern "C"  void CredentialCache_Add_m2867450082 (CredentialCache_t1595292853 * __this, Uri_t2776692961 * ___uriPrefix0, String_t* ___authType1, NetworkCredential_t2360216687 * ___cred2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.CredentialCache::Remove(System.Uri,System.String)
extern "C"  void CredentialCache_Remove_m1512440534 (CredentialCache_t1595292853 * __this, Uri_t2776692961 * ___uriPrefix0, String_t* ___authType1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Net.NetworkCredential System.Net.CredentialCache::GetCredential(System.String,System.Int32,System.String)
extern "C"  NetworkCredential_t2360216687 * CredentialCache_GetCredential_m4266649843 (CredentialCache_t1595292853 * __this, String_t* ___host0, int32_t ___port1, String_t* ___authenticationType2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.CredentialCache::Add(System.String,System.Int32,System.String,System.Net.NetworkCredential)
extern "C"  void CredentialCache_Add_m3949217344 (CredentialCache_t1595292853 * __this, String_t* ___host0, int32_t ___port1, String_t* ___authenticationType2, NetworkCredential_t2360216687 * ___credential3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.CredentialCache::Remove(System.String,System.Int32,System.String)
extern "C"  void CredentialCache_Remove_m1548681912 (CredentialCache_t1595292853 * __this, String_t* ___host0, int32_t ___port1, String_t* ___authenticationType2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
