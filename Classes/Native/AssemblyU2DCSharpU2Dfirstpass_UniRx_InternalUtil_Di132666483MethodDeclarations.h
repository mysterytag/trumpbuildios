﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.InternalUtil.DisposedObserver`1<System.Single>
struct DisposedObserver_1_t132666483;
// System.Exception
struct Exception_t1967233988;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Exception1967233988.h"

// System.Void UniRx.InternalUtil.DisposedObserver`1<System.Single>::.ctor()
extern "C"  void DisposedObserver_1__ctor_m2403532959_gshared (DisposedObserver_1_t132666483 * __this, const MethodInfo* method);
#define DisposedObserver_1__ctor_m2403532959(__this, method) ((  void (*) (DisposedObserver_1_t132666483 *, const MethodInfo*))DisposedObserver_1__ctor_m2403532959_gshared)(__this, method)
// System.Void UniRx.InternalUtil.DisposedObserver`1<System.Single>::.cctor()
extern "C"  void DisposedObserver_1__cctor_m1012981486_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define DisposedObserver_1__cctor_m1012981486(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))DisposedObserver_1__cctor_m1012981486_gshared)(__this /* static, unused */, method)
// System.Void UniRx.InternalUtil.DisposedObserver`1<System.Single>::OnCompleted()
extern "C"  void DisposedObserver_1_OnCompleted_m485294313_gshared (DisposedObserver_1_t132666483 * __this, const MethodInfo* method);
#define DisposedObserver_1_OnCompleted_m485294313(__this, method) ((  void (*) (DisposedObserver_1_t132666483 *, const MethodInfo*))DisposedObserver_1_OnCompleted_m485294313_gshared)(__this, method)
// System.Void UniRx.InternalUtil.DisposedObserver`1<System.Single>::OnError(System.Exception)
extern "C"  void DisposedObserver_1_OnError_m1398316182_gshared (DisposedObserver_1_t132666483 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method);
#define DisposedObserver_1_OnError_m1398316182(__this, ___error0, method) ((  void (*) (DisposedObserver_1_t132666483 *, Exception_t1967233988 *, const MethodInfo*))DisposedObserver_1_OnError_m1398316182_gshared)(__this, ___error0, method)
// System.Void UniRx.InternalUtil.DisposedObserver`1<System.Single>::OnNext(T)
extern "C"  void DisposedObserver_1_OnNext_m1933471623_gshared (DisposedObserver_1_t132666483 * __this, float ___value0, const MethodInfo* method);
#define DisposedObserver_1_OnNext_m1933471623(__this, ___value0, method) ((  void (*) (DisposedObserver_1_t132666483 *, float, const MethodInfo*))DisposedObserver_1_OnNext_m1933471623_gshared)(__this, ___value0, method)
