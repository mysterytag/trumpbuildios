﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Type
struct Type_t;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.DiContainer/<Install>c__AnonStorey18B
struct  U3CInstallU3Ec__AnonStorey18B_t2166929523  : public Il2CppObject
{
public:
	// System.Type Zenject.DiContainer/<Install>c__AnonStorey18B::installerType
	Type_t * ___installerType_0;

public:
	inline static int32_t get_offset_of_installerType_0() { return static_cast<int32_t>(offsetof(U3CInstallU3Ec__AnonStorey18B_t2166929523, ___installerType_0)); }
	inline Type_t * get_installerType_0() const { return ___installerType_0; }
	inline Type_t ** get_address_of_installerType_0() { return &___installerType_0; }
	inline void set_installerType_0(Type_t * value)
	{
		___installerType_0 = value;
		Il2CppCodeGenWriteBarrier(&___installerType_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
