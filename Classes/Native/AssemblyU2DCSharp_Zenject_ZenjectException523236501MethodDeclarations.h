﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Zenject.ZenjectException
struct ZenjectException_t523236501;
// System.String
struct String_t;
// System.Exception
struct Exception_t1967233988;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String968488902.h"
#include "mscorlib_System_Exception1967233988.h"

// System.Void Zenject.ZenjectException::.ctor(System.String)
extern "C"  void ZenjectException__ctor_m3982815302 (ZenjectException_t523236501 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Zenject.ZenjectException::.ctor(System.String,System.Exception)
extern "C"  void ZenjectException__ctor_m2645600688 (ZenjectException_t523236501 * __this, String_t* ___message0, Exception_t1967233988 * ___innerException1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
