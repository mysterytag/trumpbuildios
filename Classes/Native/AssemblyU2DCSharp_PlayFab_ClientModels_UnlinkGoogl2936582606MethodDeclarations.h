﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.ClientModels.UnlinkGoogleAccountResult
struct UnlinkGoogleAccountResult_t2936582606;

#include "codegen/il2cpp-codegen.h"

// System.Void PlayFab.ClientModels.UnlinkGoogleAccountResult::.ctor()
extern "C"  void UnlinkGoogleAccountResult__ctor_m1320176155 (UnlinkGoogleAccountResult_t2936582606 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
