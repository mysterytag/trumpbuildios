﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Int32[]
struct Int32U5BU5D_t1809983122;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Ionic.Zlib.InfTree
struct  InfTree_t1872341100  : public Il2CppObject
{
public:
	// System.Int32[] Ionic.Zlib.InfTree::hn
	Int32U5BU5D_t1809983122* ___hn_19;
	// System.Int32[] Ionic.Zlib.InfTree::v
	Int32U5BU5D_t1809983122* ___v_20;
	// System.Int32[] Ionic.Zlib.InfTree::c
	Int32U5BU5D_t1809983122* ___c_21;
	// System.Int32[] Ionic.Zlib.InfTree::r
	Int32U5BU5D_t1809983122* ___r_22;
	// System.Int32[] Ionic.Zlib.InfTree::u
	Int32U5BU5D_t1809983122* ___u_23;
	// System.Int32[] Ionic.Zlib.InfTree::x
	Int32U5BU5D_t1809983122* ___x_24;

public:
	inline static int32_t get_offset_of_hn_19() { return static_cast<int32_t>(offsetof(InfTree_t1872341100, ___hn_19)); }
	inline Int32U5BU5D_t1809983122* get_hn_19() const { return ___hn_19; }
	inline Int32U5BU5D_t1809983122** get_address_of_hn_19() { return &___hn_19; }
	inline void set_hn_19(Int32U5BU5D_t1809983122* value)
	{
		___hn_19 = value;
		Il2CppCodeGenWriteBarrier(&___hn_19, value);
	}

	inline static int32_t get_offset_of_v_20() { return static_cast<int32_t>(offsetof(InfTree_t1872341100, ___v_20)); }
	inline Int32U5BU5D_t1809983122* get_v_20() const { return ___v_20; }
	inline Int32U5BU5D_t1809983122** get_address_of_v_20() { return &___v_20; }
	inline void set_v_20(Int32U5BU5D_t1809983122* value)
	{
		___v_20 = value;
		Il2CppCodeGenWriteBarrier(&___v_20, value);
	}

	inline static int32_t get_offset_of_c_21() { return static_cast<int32_t>(offsetof(InfTree_t1872341100, ___c_21)); }
	inline Int32U5BU5D_t1809983122* get_c_21() const { return ___c_21; }
	inline Int32U5BU5D_t1809983122** get_address_of_c_21() { return &___c_21; }
	inline void set_c_21(Int32U5BU5D_t1809983122* value)
	{
		___c_21 = value;
		Il2CppCodeGenWriteBarrier(&___c_21, value);
	}

	inline static int32_t get_offset_of_r_22() { return static_cast<int32_t>(offsetof(InfTree_t1872341100, ___r_22)); }
	inline Int32U5BU5D_t1809983122* get_r_22() const { return ___r_22; }
	inline Int32U5BU5D_t1809983122** get_address_of_r_22() { return &___r_22; }
	inline void set_r_22(Int32U5BU5D_t1809983122* value)
	{
		___r_22 = value;
		Il2CppCodeGenWriteBarrier(&___r_22, value);
	}

	inline static int32_t get_offset_of_u_23() { return static_cast<int32_t>(offsetof(InfTree_t1872341100, ___u_23)); }
	inline Int32U5BU5D_t1809983122* get_u_23() const { return ___u_23; }
	inline Int32U5BU5D_t1809983122** get_address_of_u_23() { return &___u_23; }
	inline void set_u_23(Int32U5BU5D_t1809983122* value)
	{
		___u_23 = value;
		Il2CppCodeGenWriteBarrier(&___u_23, value);
	}

	inline static int32_t get_offset_of_x_24() { return static_cast<int32_t>(offsetof(InfTree_t1872341100, ___x_24)); }
	inline Int32U5BU5D_t1809983122* get_x_24() const { return ___x_24; }
	inline Int32U5BU5D_t1809983122** get_address_of_x_24() { return &___x_24; }
	inline void set_x_24(Int32U5BU5D_t1809983122* value)
	{
		___x_24 = value;
		Il2CppCodeGenWriteBarrier(&___x_24, value);
	}
};

struct InfTree_t1872341100_StaticFields
{
public:
	// System.Int32[] Ionic.Zlib.InfTree::fixed_tl
	Int32U5BU5D_t1809983122* ___fixed_tl_13;
	// System.Int32[] Ionic.Zlib.InfTree::fixed_td
	Int32U5BU5D_t1809983122* ___fixed_td_14;
	// System.Int32[] Ionic.Zlib.InfTree::cplens
	Int32U5BU5D_t1809983122* ___cplens_15;
	// System.Int32[] Ionic.Zlib.InfTree::cplext
	Int32U5BU5D_t1809983122* ___cplext_16;
	// System.Int32[] Ionic.Zlib.InfTree::cpdist
	Int32U5BU5D_t1809983122* ___cpdist_17;
	// System.Int32[] Ionic.Zlib.InfTree::cpdext
	Int32U5BU5D_t1809983122* ___cpdext_18;

public:
	inline static int32_t get_offset_of_fixed_tl_13() { return static_cast<int32_t>(offsetof(InfTree_t1872341100_StaticFields, ___fixed_tl_13)); }
	inline Int32U5BU5D_t1809983122* get_fixed_tl_13() const { return ___fixed_tl_13; }
	inline Int32U5BU5D_t1809983122** get_address_of_fixed_tl_13() { return &___fixed_tl_13; }
	inline void set_fixed_tl_13(Int32U5BU5D_t1809983122* value)
	{
		___fixed_tl_13 = value;
		Il2CppCodeGenWriteBarrier(&___fixed_tl_13, value);
	}

	inline static int32_t get_offset_of_fixed_td_14() { return static_cast<int32_t>(offsetof(InfTree_t1872341100_StaticFields, ___fixed_td_14)); }
	inline Int32U5BU5D_t1809983122* get_fixed_td_14() const { return ___fixed_td_14; }
	inline Int32U5BU5D_t1809983122** get_address_of_fixed_td_14() { return &___fixed_td_14; }
	inline void set_fixed_td_14(Int32U5BU5D_t1809983122* value)
	{
		___fixed_td_14 = value;
		Il2CppCodeGenWriteBarrier(&___fixed_td_14, value);
	}

	inline static int32_t get_offset_of_cplens_15() { return static_cast<int32_t>(offsetof(InfTree_t1872341100_StaticFields, ___cplens_15)); }
	inline Int32U5BU5D_t1809983122* get_cplens_15() const { return ___cplens_15; }
	inline Int32U5BU5D_t1809983122** get_address_of_cplens_15() { return &___cplens_15; }
	inline void set_cplens_15(Int32U5BU5D_t1809983122* value)
	{
		___cplens_15 = value;
		Il2CppCodeGenWriteBarrier(&___cplens_15, value);
	}

	inline static int32_t get_offset_of_cplext_16() { return static_cast<int32_t>(offsetof(InfTree_t1872341100_StaticFields, ___cplext_16)); }
	inline Int32U5BU5D_t1809983122* get_cplext_16() const { return ___cplext_16; }
	inline Int32U5BU5D_t1809983122** get_address_of_cplext_16() { return &___cplext_16; }
	inline void set_cplext_16(Int32U5BU5D_t1809983122* value)
	{
		___cplext_16 = value;
		Il2CppCodeGenWriteBarrier(&___cplext_16, value);
	}

	inline static int32_t get_offset_of_cpdist_17() { return static_cast<int32_t>(offsetof(InfTree_t1872341100_StaticFields, ___cpdist_17)); }
	inline Int32U5BU5D_t1809983122* get_cpdist_17() const { return ___cpdist_17; }
	inline Int32U5BU5D_t1809983122** get_address_of_cpdist_17() { return &___cpdist_17; }
	inline void set_cpdist_17(Int32U5BU5D_t1809983122* value)
	{
		___cpdist_17 = value;
		Il2CppCodeGenWriteBarrier(&___cpdist_17, value);
	}

	inline static int32_t get_offset_of_cpdext_18() { return static_cast<int32_t>(offsetof(InfTree_t1872341100_StaticFields, ___cpdext_18)); }
	inline Int32U5BU5D_t1809983122* get_cpdext_18() const { return ___cpdext_18; }
	inline Int32U5BU5D_t1809983122** get_address_of_cpdext_18() { return &___cpdext_18; }
	inline void set_cpdext_18(Int32U5BU5D_t1809983122* value)
	{
		___cpdext_18 = value;
		Il2CppCodeGenWriteBarrier(&___cpdext_18, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
