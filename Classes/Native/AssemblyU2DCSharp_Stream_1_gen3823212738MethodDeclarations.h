﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Stream`1<System.Object>
struct Stream_1_t3823212738;
// System.Object
struct Il2CppObject;
// System.IDisposable
struct IDisposable_t1628921374;
// System.Action`1<System.Object>
struct Action_1_t985559125;
// System.Action
struct Action_t437523947;
// IStream`1<System.Object>
struct IStream_1_t1389797411;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "AssemblyU2DCSharp_Priority3194150340.h"
#include "System_Core_System_Action437523947.h"

// System.Void Stream`1<System.Object>::.ctor()
extern "C"  void Stream_1__ctor_m2034388992_gshared (Stream_1_t3823212738 * __this, const MethodInfo* method);
#define Stream_1__ctor_m2034388992(__this, method) ((  void (*) (Stream_1_t3823212738 *, const MethodInfo*))Stream_1__ctor_m2034388992_gshared)(__this, method)
// System.Void Stream`1<System.Object>::Send(T)
extern "C"  void Stream_1_Send_m563606546_gshared (Stream_1_t3823212738 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Stream_1_Send_m563606546(__this, ___value0, method) ((  void (*) (Stream_1_t3823212738 *, Il2CppObject *, const MethodInfo*))Stream_1_Send_m563606546_gshared)(__this, ___value0, method)
// System.Void Stream`1<System.Object>::SendInTransaction(T,System.Int32)
extern "C"  void Stream_1_SendInTransaction_m3764966696_gshared (Stream_1_t3823212738 * __this, Il2CppObject * ___value0, int32_t ___p1, const MethodInfo* method);
#define Stream_1_SendInTransaction_m3764966696(__this, ___value0, ___p1, method) ((  void (*) (Stream_1_t3823212738 *, Il2CppObject *, int32_t, const MethodInfo*))Stream_1_SendInTransaction_m3764966696_gshared)(__this, ___value0, ___p1, method)
// System.IDisposable Stream`1<System.Object>::Listen(System.Action`1<T>,Priority)
extern "C"  Il2CppObject * Stream_1_Listen_m889871082_gshared (Stream_1_t3823212738 * __this, Action_1_t985559125 * ___action0, int32_t ___priority1, const MethodInfo* method);
#define Stream_1_Listen_m889871082(__this, ___action0, ___priority1, method) ((  Il2CppObject * (*) (Stream_1_t3823212738 *, Action_1_t985559125 *, int32_t, const MethodInfo*))Stream_1_Listen_m889871082_gshared)(__this, ___action0, ___priority1, method)
// System.IDisposable Stream`1<System.Object>::Listen(System.Action,Priority)
extern "C"  Il2CppObject * Stream_1_Listen_m2133851133_gshared (Stream_1_t3823212738 * __this, Action_t437523947 * ___action0, int32_t ___p1, const MethodInfo* method);
#define Stream_1_Listen_m2133851133(__this, ___action0, ___p1, method) ((  Il2CppObject * (*) (Stream_1_t3823212738 *, Action_t437523947 *, int32_t, const MethodInfo*))Stream_1_Listen_m2133851133_gshared)(__this, ___action0, ___p1, method)
// System.Void Stream`1<System.Object>::Dispose()
extern "C"  void Stream_1_Dispose_m735984701_gshared (Stream_1_t3823212738 * __this, const MethodInfo* method);
#define Stream_1_Dispose_m735984701(__this, method) ((  void (*) (Stream_1_t3823212738 *, const MethodInfo*))Stream_1_Dispose_m735984701_gshared)(__this, method)
// System.Void Stream`1<System.Object>::SetInputStream(IStream`1<T>)
extern "C"  void Stream_1_SetInputStream_m3106996224_gshared (Stream_1_t3823212738 * __this, Il2CppObject* ___stream0, const MethodInfo* method);
#define Stream_1_SetInputStream_m3106996224(__this, ___stream0, method) ((  void (*) (Stream_1_t3823212738 *, Il2CppObject*, const MethodInfo*))Stream_1_SetInputStream_m3106996224_gshared)(__this, ___stream0, method)
// System.Void Stream`1<System.Object>::ClearInputStream()
extern "C"  void Stream_1_ClearInputStream_m445690081_gshared (Stream_1_t3823212738 * __this, const MethodInfo* method);
#define Stream_1_ClearInputStream_m445690081(__this, method) ((  void (*) (Stream_1_t3823212738 *, const MethodInfo*))Stream_1_ClearInputStream_m445690081_gshared)(__this, method)
// System.Void Stream`1<System.Object>::TransactionIterationFinished()
extern "C"  void Stream_1_TransactionIterationFinished_m3113559861_gshared (Stream_1_t3823212738 * __this, const MethodInfo* method);
#define Stream_1_TransactionIterationFinished_m3113559861(__this, method) ((  void (*) (Stream_1_t3823212738 *, const MethodInfo*))Stream_1_TransactionIterationFinished_m3113559861_gshared)(__this, method)
// System.Void Stream`1<System.Object>::Unpack(System.Int32)
extern "C"  void Stream_1_Unpack_m3388253319_gshared (Stream_1_t3823212738 * __this, int32_t ___p0, const MethodInfo* method);
#define Stream_1_Unpack_m3388253319(__this, ___p0, method) ((  void (*) (Stream_1_t3823212738 *, int32_t, const MethodInfo*))Stream_1_Unpack_m3388253319_gshared)(__this, ___p0, method)
// System.Void Stream`1<System.Object>::<SetInputStream>m__1BA(T)
extern "C"  void Stream_1_U3CSetInputStreamU3Em__1BA_m3579563677_gshared (Stream_1_t3823212738 * __this, Il2CppObject * ___val0, const MethodInfo* method);
#define Stream_1_U3CSetInputStreamU3Em__1BA_m3579563677(__this, ___val0, method) ((  void (*) (Stream_1_t3823212738 *, Il2CppObject *, const MethodInfo*))Stream_1_U3CSetInputStreamU3Em__1BA_m3579563677_gshared)(__this, ___val0, method)
