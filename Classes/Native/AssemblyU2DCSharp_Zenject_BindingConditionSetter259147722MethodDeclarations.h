﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Zenject.BindingConditionSetter
struct BindingConditionSetter_t259147722;
// Zenject.ProviderBase
struct ProviderBase_t1627494391;
// Zenject.BindingCondition
struct BindingCondition_t1528286123;
// System.Object
struct Il2CppObject;
// System.Type[]
struct TypeU5BU5D_t3431720054;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Zenject_ProviderBase1627494391.h"
#include "AssemblyU2DCSharp_Zenject_BindingCondition1528286123.h"
#include "mscorlib_System_Object837106420.h"

// System.Void Zenject.BindingConditionSetter::.ctor(Zenject.ProviderBase)
extern "C"  void BindingConditionSetter__ctor_m1119992550 (BindingConditionSetter_t259147722 * __this, ProviderBase_t1627494391 * ___provider0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Zenject.BindingConditionSetter::When(Zenject.BindingCondition)
extern "C"  void BindingConditionSetter_When_m3615771522 (BindingConditionSetter_t259147722 * __this, BindingCondition_t1528286123 * ___condition0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Zenject.BindingConditionSetter::WhenInjectedIntoInstance(System.Object)
extern "C"  void BindingConditionSetter_WhenInjectedIntoInstance_m2567654104 (BindingConditionSetter_t259147722 * __this, Il2CppObject * ___instance0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Zenject.BindingConditionSetter::WhenInjectedInto(System.Type[])
extern "C"  void BindingConditionSetter_WhenInjectedInto_m4084140272 (BindingConditionSetter_t259147722 * __this, TypeU5BU5D_t3431720054* ___targets0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
