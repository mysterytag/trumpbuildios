﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.InternalUtil.ListObserver`1<UnityEngine.Quaternion>
struct ListObserver_1_t2186333650;
// UniRx.InternalUtil.ImmutableList`1<UniRx.IObserver`1<UnityEngine.Quaternion>>
struct ImmutableList_1_t868952085;
// System.Exception
struct Exception_t1967233988;
// UniRx.IObserver`1<UnityEngine.Quaternion>
struct IObserver_1_t4103714882;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Exception1967233988.h"
#include "UnityEngine_UnityEngine_Quaternion1891715979.h"

// System.Void UniRx.InternalUtil.ListObserver`1<UnityEngine.Quaternion>::.ctor(UniRx.InternalUtil.ImmutableList`1<UniRx.IObserver`1<T>>)
extern "C"  void ListObserver_1__ctor_m4053348224_gshared (ListObserver_1_t2186333650 * __this, ImmutableList_1_t868952085 * ___observers0, const MethodInfo* method);
#define ListObserver_1__ctor_m4053348224(__this, ___observers0, method) ((  void (*) (ListObserver_1_t2186333650 *, ImmutableList_1_t868952085 *, const MethodInfo*))ListObserver_1__ctor_m4053348224_gshared)(__this, ___observers0, method)
// System.Void UniRx.InternalUtil.ListObserver`1<UnityEngine.Quaternion>::OnCompleted()
extern "C"  void ListObserver_1_OnCompleted_m2661661112_gshared (ListObserver_1_t2186333650 * __this, const MethodInfo* method);
#define ListObserver_1_OnCompleted_m2661661112(__this, method) ((  void (*) (ListObserver_1_t2186333650 *, const MethodInfo*))ListObserver_1_OnCompleted_m2661661112_gshared)(__this, method)
// System.Void UniRx.InternalUtil.ListObserver`1<UnityEngine.Quaternion>::OnError(System.Exception)
extern "C"  void ListObserver_1_OnError_m162502373_gshared (ListObserver_1_t2186333650 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method);
#define ListObserver_1_OnError_m162502373(__this, ___error0, method) ((  void (*) (ListObserver_1_t2186333650 *, Exception_t1967233988 *, const MethodInfo*))ListObserver_1_OnError_m162502373_gshared)(__this, ___error0, method)
// System.Void UniRx.InternalUtil.ListObserver`1<UnityEngine.Quaternion>::OnNext(T)
extern "C"  void ListObserver_1_OnNext_m3139037142_gshared (ListObserver_1_t2186333650 * __this, Quaternion_t1891715979  ___value0, const MethodInfo* method);
#define ListObserver_1_OnNext_m3139037142(__this, ___value0, method) ((  void (*) (ListObserver_1_t2186333650 *, Quaternion_t1891715979 , const MethodInfo*))ListObserver_1_OnNext_m3139037142_gshared)(__this, ___value0, method)
// UniRx.IObserver`1<T> UniRx.InternalUtil.ListObserver`1<UnityEngine.Quaternion>::Add(UniRx.IObserver`1<T>)
extern "C"  Il2CppObject* ListObserver_1_Add_m1768324086_gshared (ListObserver_1_t2186333650 * __this, Il2CppObject* ___observer0, const MethodInfo* method);
#define ListObserver_1_Add_m1768324086(__this, ___observer0, method) ((  Il2CppObject* (*) (ListObserver_1_t2186333650 *, Il2CppObject*, const MethodInfo*))ListObserver_1_Add_m1768324086_gshared)(__this, ___observer0, method)
// UniRx.IObserver`1<T> UniRx.InternalUtil.ListObserver`1<UnityEngine.Quaternion>::Remove(UniRx.IObserver`1<T>)
extern "C"  Il2CppObject* ListObserver_1_Remove_m2802406293_gshared (ListObserver_1_t2186333650 * __this, Il2CppObject* ___observer0, const MethodInfo* method);
#define ListObserver_1_Remove_m2802406293(__this, ___observer0, method) ((  Il2CppObject* (*) (ListObserver_1_t2186333650 *, Il2CppObject*, const MethodInfo*))ListObserver_1_Remove_m2802406293_gshared)(__this, ___observer0, method)
