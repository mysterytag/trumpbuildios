﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Subject_1_gen2775141040MethodDeclarations.h"

// System.Void UniRx.Subject`1<UnityEngine.Collider2D>::.ctor()
#define Subject_1__ctor_m538481901(__this, method) ((  void (*) (Subject_1_t3828072815 *, const MethodInfo*))Subject_1__ctor_m3752525464_gshared)(__this, method)
// System.Boolean UniRx.Subject`1<UnityEngine.Collider2D>::get_HasObservers()
#define Subject_1_get_HasObservers_m908772551(__this, method) ((  bool (*) (Subject_1_t3828072815 *, const MethodInfo*))Subject_1_get_HasObservers_m2673131508_gshared)(__this, method)
// System.Void UniRx.Subject`1<UnityEngine.Collider2D>::OnCompleted()
#define Subject_1_OnCompleted_m845783735(__this, method) ((  void (*) (Subject_1_t3828072815 *, const MethodInfo*))Subject_1_OnCompleted_m1083367458_gshared)(__this, method)
// System.Void UniRx.Subject`1<UnityEngine.Collider2D>::OnError(System.Exception)
#define Subject_1_OnError_m1666906468(__this, ___error0, method) ((  void (*) (Subject_1_t3828072815 *, Exception_t1967233988 *, const MethodInfo*))Subject_1_OnError_m1700032079_gshared)(__this, ___error0, method)
// System.Void UniRx.Subject`1<UnityEngine.Collider2D>::OnNext(T)
#define Subject_1_OnNext_m620767317(__this, ___value0, method) ((  void (*) (Subject_1_t3828072815 *, Collider2D_t1890038195 *, const MethodInfo*))Subject_1_OnNext_m1235145536_gshared)(__this, ___value0, method)
// System.IDisposable UniRx.Subject`1<UnityEngine.Collider2D>::Subscribe(UniRx.IObserver`1<T>)
#define Subject_1_Subscribe_m2690422466(__this, ___observer0, method) ((  Il2CppObject * (*) (Subject_1_t3828072815 *, Il2CppObject*, const MethodInfo*))Subject_1_Subscribe_m2428743831_gshared)(__this, ___observer0, method)
// System.Void UniRx.Subject`1<UnityEngine.Collider2D>::Dispose()
#define Subject_1_Dispose_m1983314410(__this, method) ((  void (*) (Subject_1_t3828072815 *, const MethodInfo*))Subject_1_Dispose_m2597692629_gshared)(__this, method)
// System.Void UniRx.Subject`1<UnityEngine.Collider2D>::ThrowIfDisposed()
#define Subject_1_ThrowIfDisposed_m98113907(__this, method) ((  void (*) (Subject_1_t3828072815 *, const MethodInfo*))Subject_1_ThrowIfDisposed_m956279134_gshared)(__this, method)
// System.Boolean UniRx.Subject`1<UnityEngine.Collider2D>::IsRequiredSubscribeOnCurrentThread()
#define Subject_1_IsRequiredSubscribeOnCurrentThread_m3873451518(__this, method) ((  bool (*) (Subject_1_t3828072815 *, const MethodInfo*))Subject_1_IsRequiredSubscribeOnCurrentThread_m3969161195_gshared)(__this, method)
