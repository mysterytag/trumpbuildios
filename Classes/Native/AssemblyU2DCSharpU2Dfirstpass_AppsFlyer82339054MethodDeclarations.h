﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// AppsFlyer
struct AppsFlyer_t82339054;
// System.String
struct String_t;
// System.Collections.Generic.Dictionary`2<System.String,System.String>
struct Dictionary_2_t2606186806;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String968488902.h"

// System.Void AppsFlyer::.ctor()
extern "C"  void AppsFlyer__ctor_m3363854345 (AppsFlyer_t82339054 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AppsFlyer::mTrackEvent(System.String,System.String)
extern "C"  void AppsFlyer_mTrackEvent_m2810694587 (Il2CppObject * __this /* static, unused */, String_t* ___eventName0, String_t* ___eventValue1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AppsFlyer::mSetCurrencyCode(System.String)
extern "C"  void AppsFlyer_mSetCurrencyCode_m3911200404 (Il2CppObject * __this /* static, unused */, String_t* ___currencyCode0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AppsFlyer::mSetCustomerUserID(System.String)
extern "C"  void AppsFlyer_mSetCustomerUserID_m919353230 (Il2CppObject * __this /* static, unused */, String_t* ___customerUserID0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AppsFlyer::mSetAppsFlyerDevKey(System.String)
extern "C"  void AppsFlyer_mSetAppsFlyerDevKey_m130938360 (Il2CppObject * __this /* static, unused */, String_t* ___devKey0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AppsFlyer::mTrackAppLaunch()
extern "C"  void AppsFlyer_mTrackAppLaunch_m62097981 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AppsFlyer::mSetAppID(System.String)
extern "C"  void AppsFlyer_mSetAppID_m328268276 (Il2CppObject * __this /* static, unused */, String_t* ___appleAppId0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AppsFlyer::mTrackRichEvent(System.String,System.String)
extern "C"  void AppsFlyer_mTrackRichEvent_m808304055 (Il2CppObject * __this /* static, unused */, String_t* ___eventName0, String_t* ___eventValues1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AppsFlyer::mValidateReceipt(System.String,System.String,System.String,System.String,System.Double,System.String)
extern "C"  void AppsFlyer_mValidateReceipt_m4292413214 (Il2CppObject * __this /* static, unused */, String_t* ___eventName0, String_t* ___failedEventName1, String_t* ___eventValue2, String_t* ___productIdentifier3, double ___price4, String_t* ___currency5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AppsFlyer::trackEvent(System.String,System.String)
extern "C"  void AppsFlyer_trackEvent_m3305581748 (Il2CppObject * __this /* static, unused */, String_t* ___eventName0, String_t* ___eventValue1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AppsFlyer::setCurrencyCode(System.String)
extern "C"  void AppsFlyer_setCurrencyCode_m3598066043 (Il2CppObject * __this /* static, unused */, String_t* ___currencyCode0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AppsFlyer::setCustomerUserID(System.String)
extern "C"  void AppsFlyer_setCustomerUserID_m644943029 (Il2CppObject * __this /* static, unused */, String_t* ___customerUserID0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AppsFlyer::setAppsFlyerKey(System.String)
extern "C"  void AppsFlyer_setAppsFlyerKey_m1333593160 (Il2CppObject * __this /* static, unused */, String_t* ___key0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AppsFlyer::trackAppLaunch()
extern "C"  void AppsFlyer_trackAppLaunch_m1071484260 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AppsFlyer::setAppID(System.String)
extern "C"  void AppsFlyer_setAppID_m721120493 (Il2CppObject * __this /* static, unused */, String_t* ___appleAppId0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AppsFlyer::trackRichEvent(System.String,System.Collections.Generic.Dictionary`2<System.String,System.String>)
extern "C"  void AppsFlyer_trackRichEvent_m3321556035 (Il2CppObject * __this /* static, unused */, String_t* ___eventName0, Dictionary_2_t2606186806 * ___eventValues1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AppsFlyer::validateReceipt(System.String,System.String,System.String,System.String,System.Double,System.String)
extern "C"  void AppsFlyer_validateReceipt_m4249893061 (Il2CppObject * __this /* static, unused */, String_t* ___eventName0, String_t* ___failedEventName1, String_t* ___eventValue2, String_t* ___productIdentifier3, double ___price4, String_t* ___currency5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
