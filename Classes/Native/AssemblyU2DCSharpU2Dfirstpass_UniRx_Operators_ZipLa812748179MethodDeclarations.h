﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.ZipLatestObservable`3/ZipLatest/RightObserver<System.Object,System.Object,System.Object>
struct RightObserver_t812748180;
// UniRx.Operators.ZipLatestObservable`3/ZipLatest<System.Object,System.Object,System.Object>
struct ZipLatest_t2793383209;
// System.Object
struct Il2CppObject;
// System.Exception
struct Exception_t1967233988;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_Exception1967233988.h"

// System.Void UniRx.Operators.ZipLatestObservable`3/ZipLatest/RightObserver<System.Object,System.Object,System.Object>::.ctor(UniRx.Operators.ZipLatestObservable`3/ZipLatest<TLeft,TRight,TResult>)
extern "C"  void RightObserver__ctor_m1096178580_gshared (RightObserver_t812748180 * __this, ZipLatest_t2793383209 * ___parent0, const MethodInfo* method);
#define RightObserver__ctor_m1096178580(__this, ___parent0, method) ((  void (*) (RightObserver_t812748180 *, ZipLatest_t2793383209 *, const MethodInfo*))RightObserver__ctor_m1096178580_gshared)(__this, ___parent0, method)
// System.Void UniRx.Operators.ZipLatestObservable`3/ZipLatest/RightObserver<System.Object,System.Object,System.Object>::OnNext(TRight)
extern "C"  void RightObserver_OnNext_m3440317012_gshared (RightObserver_t812748180 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define RightObserver_OnNext_m3440317012(__this, ___value0, method) ((  void (*) (RightObserver_t812748180 *, Il2CppObject *, const MethodInfo*))RightObserver_OnNext_m3440317012_gshared)(__this, ___value0, method)
// System.Void UniRx.Operators.ZipLatestObservable`3/ZipLatest/RightObserver<System.Object,System.Object,System.Object>::OnError(System.Exception)
extern "C"  void RightObserver_OnError_m176383537_gshared (RightObserver_t812748180 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method);
#define RightObserver_OnError_m176383537(__this, ___error0, method) ((  void (*) (RightObserver_t812748180 *, Exception_t1967233988 *, const MethodInfo*))RightObserver_OnError_m176383537_gshared)(__this, ___error0, method)
// System.Void UniRx.Operators.ZipLatestObservable`3/ZipLatest/RightObserver<System.Object,System.Object,System.Object>::OnCompleted()
extern "C"  void RightObserver_OnCompleted_m3852278532_gshared (RightObserver_t812748180 * __this, const MethodInfo* method);
#define RightObserver_OnCompleted_m3852278532(__this, method) ((  void (*) (RightObserver_t812748180 *, const MethodInfo*))RightObserver_OnCompleted_m3852278532_gshared)(__this, method)
