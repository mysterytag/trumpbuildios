﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_PlayFab_PlayFabClientAPI_Process3425006930MethodDeclarations.h"

// System.Void PlayFab.PlayFabClientAPI/ProcessApiCallback`1<PlayFab.ClientModels.AcceptTradeResponse>::.ctor(System.Object,System.IntPtr)
#define ProcessApiCallback_1__ctor_m3796436947(__this, ___object0, ___method1, method) ((  void (*) (ProcessApiCallback_1_t1956756459 *, Il2CppObject *, IntPtr_t, const MethodInfo*))ProcessApiCallback_1__ctor_m1266910666_gshared)(__this, ___object0, ___method1, method)
// System.Void PlayFab.PlayFabClientAPI/ProcessApiCallback`1<PlayFab.ClientModels.AcceptTradeResponse>::Invoke(TResult)
#define ProcessApiCallback_1_Invoke_m130871924(__this, ___result0, method) ((  void (*) (ProcessApiCallback_1_t1956756459 *, AcceptTradeResponse_t3663823245 *, const MethodInfo*))ProcessApiCallback_1_Invoke_m1002635741_gshared)(__this, ___result0, method)
// System.IAsyncResult PlayFab.PlayFabClientAPI/ProcessApiCallback`1<PlayFab.ClientModels.AcceptTradeResponse>::BeginInvoke(TResult,System.AsyncCallback,System.Object)
#define ProcessApiCallback_1_BeginInvoke_m3729029475(__this, ___result0, ___callback1, ___object2, method) ((  Il2CppObject * (*) (ProcessApiCallback_1_t1956756459 *, AcceptTradeResponse_t3663823245 *, AsyncCallback_t1363551830 *, Il2CppObject *, const MethodInfo*))ProcessApiCallback_1_BeginInvoke_m1317674820_gshared)(__this, ___result0, ___callback1, ___object2, method)
// System.Void PlayFab.PlayFabClientAPI/ProcessApiCallback`1<PlayFab.ClientModels.AcceptTradeResponse>::EndInvoke(System.IAsyncResult)
#define ProcessApiCallback_1_EndInvoke_m338123619(__this, ___result0, method) ((  void (*) (ProcessApiCallback_1_t1956756459 *, Il2CppObject *, const MethodInfo*))ProcessApiCallback_1_EndInvoke_m3326438618_gshared)(__this, ___result0, method)
