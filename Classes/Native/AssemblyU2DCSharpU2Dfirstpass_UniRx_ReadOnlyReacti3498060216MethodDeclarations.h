﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.ReadOnlyReactiveProperty`1/ReadOnlyReactivePropertyObserver<System.Boolean>
struct ReadOnlyReactivePropertyObserver_t3498060216;
// UniRx.ReadOnlyReactiveProperty`1<System.Boolean>
struct ReadOnlyReactiveProperty_1_t3000407357;
// System.Exception
struct Exception_t1967233988;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Exception1967233988.h"

// System.Void UniRx.ReadOnlyReactiveProperty`1/ReadOnlyReactivePropertyObserver<System.Boolean>::.ctor(UniRx.ReadOnlyReactiveProperty`1<T>)
extern "C"  void ReadOnlyReactivePropertyObserver__ctor_m4256084099_gshared (ReadOnlyReactivePropertyObserver_t3498060216 * __this, ReadOnlyReactiveProperty_1_t3000407357 * ___parent0, const MethodInfo* method);
#define ReadOnlyReactivePropertyObserver__ctor_m4256084099(__this, ___parent0, method) ((  void (*) (ReadOnlyReactivePropertyObserver_t3498060216 *, ReadOnlyReactiveProperty_1_t3000407357 *, const MethodInfo*))ReadOnlyReactivePropertyObserver__ctor_m4256084099_gshared)(__this, ___parent0, method)
// System.Void UniRx.ReadOnlyReactiveProperty`1/ReadOnlyReactivePropertyObserver<System.Boolean>::OnNext(T)
extern "C"  void ReadOnlyReactivePropertyObserver_OnNext_m2074741770_gshared (ReadOnlyReactivePropertyObserver_t3498060216 * __this, bool ___value0, const MethodInfo* method);
#define ReadOnlyReactivePropertyObserver_OnNext_m2074741770(__this, ___value0, method) ((  void (*) (ReadOnlyReactivePropertyObserver_t3498060216 *, bool, const MethodInfo*))ReadOnlyReactivePropertyObserver_OnNext_m2074741770_gshared)(__this, ___value0, method)
// System.Void UniRx.ReadOnlyReactiveProperty`1/ReadOnlyReactivePropertyObserver<System.Boolean>::OnError(System.Exception)
extern "C"  void ReadOnlyReactivePropertyObserver_OnError_m4026061593_gshared (ReadOnlyReactivePropertyObserver_t3498060216 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method);
#define ReadOnlyReactivePropertyObserver_OnError_m4026061593(__this, ___error0, method) ((  void (*) (ReadOnlyReactivePropertyObserver_t3498060216 *, Exception_t1967233988 *, const MethodInfo*))ReadOnlyReactivePropertyObserver_OnError_m4026061593_gshared)(__this, ___error0, method)
// System.Void UniRx.ReadOnlyReactiveProperty`1/ReadOnlyReactivePropertyObserver<System.Boolean>::OnCompleted()
extern "C"  void ReadOnlyReactivePropertyObserver_OnCompleted_m2506138604_gshared (ReadOnlyReactivePropertyObserver_t3498060216 * __this, const MethodInfo* method);
#define ReadOnlyReactivePropertyObserver_OnCompleted_m2506138604(__this, method) ((  void (*) (ReadOnlyReactivePropertyObserver_t3498060216 *, const MethodInfo*))ReadOnlyReactivePropertyObserver_OnCompleted_m2506138604_gshared)(__this, method)
