﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.ClientModels.GetCloudScriptUrlResult
struct GetCloudScriptUrlResult_t3525973842;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String968488902.h"

// System.Void PlayFab.ClientModels.GetCloudScriptUrlResult::.ctor()
extern "C"  void GetCloudScriptUrlResult__ctor_m4282533527 (GetCloudScriptUrlResult_t3525973842 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PlayFab.ClientModels.GetCloudScriptUrlResult::get_Url()
extern "C"  String_t* GetCloudScriptUrlResult_get_Url_m859101368 (GetCloudScriptUrlResult_t3525973842 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.GetCloudScriptUrlResult::set_Url(System.String)
extern "C"  void GetCloudScriptUrlResult_set_Url_m3887732763 (GetCloudScriptUrlResult_t3525973842 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
