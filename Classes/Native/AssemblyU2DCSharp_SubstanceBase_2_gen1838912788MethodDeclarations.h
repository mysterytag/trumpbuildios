﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_SubstanceBase_2_gen540235856MethodDeclarations.h"

// System.Void SubstanceBase`2<System.Object,System.Func`2<System.Object,System.Object>>::.ctor()
#define SubstanceBase_2__ctor_m3032729113(__this, method) ((  void (*) (SubstanceBase_2_t1838912788 *, const MethodInfo*))SubstanceBase_2__ctor_m1606804868_gshared)(__this, method)
// System.Void SubstanceBase`2<System.Object,System.Func`2<System.Object,System.Object>>::.ctor(T)
#define SubstanceBase_2__ctor_m3820290661(__this, ___baseVal0, method) ((  void (*) (SubstanceBase_2_t1838912788 *, Il2CppObject *, const MethodInfo*))SubstanceBase_2__ctor_m2566312026_gshared)(__this, ___baseVal0, method)
// T SubstanceBase`2<System.Object,System.Func`2<System.Object,System.Object>>::get_baseValue()
#define SubstanceBase_2_get_baseValue_m2190485965(__this, method) ((  Il2CppObject * (*) (SubstanceBase_2_t1838912788 *, const MethodInfo*))SubstanceBase_2_get_baseValue_m1000505434_gshared)(__this, method)
// System.Void SubstanceBase`2<System.Object,System.Func`2<System.Object,System.Object>>::set_baseValue(T)
#define SubstanceBase_2_set_baseValue_m2935383684(__this, ___value0, method) ((  void (*) (SubstanceBase_2_t1838912788 *, Il2CppObject *, const MethodInfo*))SubstanceBase_2_set_baseValue_m1167683449_gshared)(__this, ___value0, method)
// System.Void SubstanceBase`2<System.Object,System.Func`2<System.Object,System.Object>>::set_baseValueReactive(ICell`1<T>)
#define SubstanceBase_2_set_baseValueReactive_m3666342633(__this, ___value0, method) ((  void (*) (SubstanceBase_2_t1838912788 *, Il2CppObject*, const MethodInfo*))SubstanceBase_2_set_baseValueReactive_m2901627540_gshared)(__this, ___value0, method)
// System.IDisposable SubstanceBase`2<System.Object,System.Func`2<System.Object,System.Object>>::Bind(System.Action`1<T>,Priority)
#define SubstanceBase_2_Bind_m483306061(__this, ___action0, ___p1, method) ((  Il2CppObject * (*) (SubstanceBase_2_t1838912788 *, Action_1_t985559125 *, int32_t, const MethodInfo*))SubstanceBase_2_Bind_m487587186_gshared)(__this, ___action0, ___p1, method)
// System.IDisposable SubstanceBase`2<System.Object,System.Func`2<System.Object,System.Object>>::OnChanged(System.Action,Priority)
#define SubstanceBase_2_OnChanged_m1052208388(__this, ___action0, ___p1, method) ((  Il2CppObject * (*) (SubstanceBase_2_t1838912788 *, Action_t437523947 *, int32_t, const MethodInfo*))SubstanceBase_2_OnChanged_m1056489513_gshared)(__this, ___action0, ___p1, method)
// System.Object SubstanceBase`2<System.Object,System.Func`2<System.Object,System.Object>>::get_valueObject()
#define SubstanceBase_2_get_valueObject_m1622211059(__this, method) ((  Il2CppObject * (*) (SubstanceBase_2_t1838912788 *, const MethodInfo*))SubstanceBase_2_get_valueObject_m2572102932_gshared)(__this, method)
// System.IDisposable SubstanceBase`2<System.Object,System.Func`2<System.Object,System.Object>>::ListenUpdates(System.Action`1<T>,Priority)
#define SubstanceBase_2_ListenUpdates_m3150200917(__this, ___action0, ___p1, method) ((  Il2CppObject * (*) (SubstanceBase_2_t1838912788 *, Action_1_t985559125 *, int32_t, const MethodInfo*))SubstanceBase_2_ListenUpdates_m2465048272_gshared)(__this, ___action0, ___p1, method)
// IStream`1<T> SubstanceBase`2<System.Object,System.Func`2<System.Object,System.Object>>::get_updates()
#define SubstanceBase_2_get_updates_m2662351241(__this, method) ((  Il2CppObject* (*) (SubstanceBase_2_t1838912788 *, const MethodInfo*))SubstanceBase_2_get_updates_m1292576498_gshared)(__this, method)
// T SubstanceBase`2<System.Object,System.Func`2<System.Object,System.Object>>::get_value()
#define SubstanceBase_2_get_value_m896101342(__this, method) ((  Il2CppObject * (*) (SubstanceBase_2_t1838912788 *, const MethodInfo*))SubstanceBase_2_get_value_m3139375339_gshared)(__this, method)
// SubstanceBase`2/Intrusion<T,InfT> SubstanceBase`2<System.Object,System.Func`2<System.Object,System.Object>>::AffectInner(ILiving,InfT)
#define SubstanceBase_2_AffectInner_m195670140(__this, ___intruder0, ___influence1, method) ((  Intrusion_t48563248 * (*) (SubstanceBase_2_t1838912788 *, Il2CppObject *, Func_2_t2135783352 *, const MethodInfo*))SubstanceBase_2_AffectInner_m1664732325_gshared)(__this, ___intruder0, ___influence1, method)
// System.IDisposable SubstanceBase`2<System.Object,System.Func`2<System.Object,System.Object>>::AffectUnsafe(InfT)
#define SubstanceBase_2_AffectUnsafe_m1802344992(__this, ___influence0, method) ((  Il2CppObject * (*) (SubstanceBase_2_t1838912788 *, Func_2_t2135783352 *, const MethodInfo*))SubstanceBase_2_AffectUnsafe_m1801912731_gshared)(__this, ___influence0, method)
// System.IDisposable SubstanceBase`2<System.Object,System.Func`2<System.Object,System.Object>>::AffectUnsafe(ICell`1<InfT>)
#define SubstanceBase_2_AffectUnsafe_m132908020(__this, ___influence0, method) ((  Il2CppObject * (*) (SubstanceBase_2_t1838912788 *, Il2CppObject*, const MethodInfo*))SubstanceBase_2_AffectUnsafe_m3688879833_gshared)(__this, ___influence0, method)
// System.IDisposable SubstanceBase`2<System.Object,System.Func`2<System.Object,System.Object>>::Affect(ILiving,InfT)
#define SubstanceBase_2_Affect_m1386403840(__this, ___intruder0, ___influence1, method) ((  Il2CppObject * (*) (SubstanceBase_2_t1838912788 *, Il2CppObject *, Func_2_t2135783352 *, const MethodInfo*))SubstanceBase_2_Affect_m971001019_gshared)(__this, ___intruder0, ___influence1, method)
// System.IDisposable SubstanceBase`2<System.Object,System.Func`2<System.Object,System.Object>>::Affect(ILiving,ICell`1<InfT>)
#define SubstanceBase_2_Affect_m2895012372(__this, ___intruder0, ___influence1, method) ((  Il2CppObject * (*) (SubstanceBase_2_t1838912788 *, Il2CppObject *, Il2CppObject*, const MethodInfo*))SubstanceBase_2_Affect_m1389957049_gshared)(__this, ___intruder0, ___influence1, method)
// System.IDisposable SubstanceBase`2<System.Object,System.Func`2<System.Object,System.Object>>::Affect(ILiving,InfT,IEmptyStream)
#define SubstanceBase_2_Affect_m3818079842(__this, ___intruder0, ___influence1, ___update2, method) ((  Il2CppObject * (*) (SubstanceBase_2_t1838912788 *, Il2CppObject *, Func_2_t2135783352 *, Il2CppObject *, const MethodInfo*))SubstanceBase_2_Affect_m3822360967_gshared)(__this, ___intruder0, ___influence1, ___update2, method)
// System.Void SubstanceBase`2<System.Object,System.Func`2<System.Object,System.Object>>::OnAdd(SubstanceBase`2/Intrusion<T,InfT>)
#define SubstanceBase_2_OnAdd_m2252945655(__this, ___intrusion0, method) ((  void (*) (SubstanceBase_2_t1838912788 *, Intrusion_t48563248 *, const MethodInfo*))SubstanceBase_2_OnAdd_m680391404_gshared)(__this, ___intrusion0, method)
// System.Void SubstanceBase`2<System.Object,System.Func`2<System.Object,System.Object>>::OnRemove(SubstanceBase`2/Intrusion<T,InfT>)
#define SubstanceBase_2_OnRemove_m2348056322(__this, ___intrusion0, method) ((  void (*) (SubstanceBase_2_t1838912788 *, Intrusion_t48563248 *, const MethodInfo*))SubstanceBase_2_OnRemove_m3887629549_gshared)(__this, ___intrusion0, method)
// System.Void SubstanceBase`2<System.Object,System.Func`2<System.Object,System.Object>>::OnUpdate(SubstanceBase`2/Intrusion<T,InfT>)
#define SubstanceBase_2_OnUpdate_m1698787485(__this, ___intrusion0, method) ((  void (*) (SubstanceBase_2_t1838912788 *, Intrusion_t48563248 *, const MethodInfo*))SubstanceBase_2_OnUpdate_m3238360712_gshared)(__this, ___intrusion0, method)
// System.Void SubstanceBase`2<System.Object,System.Func`2<System.Object,System.Object>>::UpdateAll()
#define SubstanceBase_2_UpdateAll_m2050269775(__this, method) ((  void (*) (SubstanceBase_2_t1838912788 *, const MethodInfo*))SubstanceBase_2_UpdateAll_m2398295098_gshared)(__this, method)
// System.Void SubstanceBase`2<System.Object,System.Func`2<System.Object,System.Object>>::ClearIntrusion(SubstanceBase`2/Intrusion<T,InfT>)
#define SubstanceBase_2_ClearIntrusion_m3457323531(__this, ___intr0, method) ((  void (*) (SubstanceBase_2_t1838912788 *, Intrusion_t48563248 *, const MethodInfo*))SubstanceBase_2_ClearIntrusion_m1431780278_gshared)(__this, ___intr0, method)
// System.Void SubstanceBase`2<System.Object,System.Func`2<System.Object,System.Object>>::<set_baseValueReactive>m__1D7(T)
#define SubstanceBase_2_U3Cset_baseValueReactiveU3Em__1D7_m3105606578(__this, ___val0, method) ((  void (*) (SubstanceBase_2_t1838912788 *, Il2CppObject *, const MethodInfo*))SubstanceBase_2_U3Cset_baseValueReactiveU3Em__1D7_m725633703_gshared)(__this, ___val0, method)
