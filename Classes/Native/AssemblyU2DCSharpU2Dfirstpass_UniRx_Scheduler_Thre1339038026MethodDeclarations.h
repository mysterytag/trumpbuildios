﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Scheduler/ThreadPoolScheduler/PeriodicTimer
struct PeriodicTimer_t1339038026;
// System.Action
struct Action_t437523947;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_TimeSpan763862892.h"
#include "System_Core_System_Action437523947.h"
#include "mscorlib_System_Object837106420.h"

// System.Void UniRx.Scheduler/ThreadPoolScheduler/PeriodicTimer::.ctor(System.TimeSpan,System.Action)
extern "C"  void PeriodicTimer__ctor_m272837120 (PeriodicTimer_t1339038026 * __this, TimeSpan_t763862892  ___period0, Action_t437523947 * ___action1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.Scheduler/ThreadPoolScheduler/PeriodicTimer::.cctor()
extern "C"  void PeriodicTimer__cctor_m4070876122 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.Scheduler/ThreadPoolScheduler/PeriodicTimer::Tick(System.Object)
extern "C"  void PeriodicTimer_Tick_m2895945030 (PeriodicTimer_t1339038026 * __this, Il2CppObject * ___state0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.Scheduler/ThreadPoolScheduler/PeriodicTimer::Dispose()
extern "C"  void PeriodicTimer_Dispose_m3601471920 (PeriodicTimer_t1339038026 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.Scheduler/ThreadPoolScheduler/PeriodicTimer::<Tick>m__A2()
extern "C"  void PeriodicTimer_U3CTickU3Em__A2_m3267683018 (PeriodicTimer_t1339038026 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
