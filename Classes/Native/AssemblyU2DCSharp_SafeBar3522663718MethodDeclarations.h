﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SafeBar
struct SafeBar_t3522663718;

#include "codegen/il2cpp-codegen.h"

// System.Void SafeBar::.ctor()
extern "C"  void SafeBar__ctor_m667944197 (SafeBar_t3522663718 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SafeBar::Awake()
extern "C"  void SafeBar_Awake_m905549416 (SafeBar_t3522663718 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SafeBar::SetFillersQuantity(System.Int32)
extern "C"  void SafeBar_SetFillersQuantity_m2544581340 (SafeBar_t3522663718 * __this, int32_t ___quantity0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SafeBar::SetRatio(System.Single)
extern "C"  void SafeBar_SetRatio_m2604592003 (SafeBar_t3522663718 * __this, float ___ratio0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
