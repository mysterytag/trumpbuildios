﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UniRx.Operators.SkipWhileObservable`1<System.Object>
struct SkipWhileObservable_1_t2542074388;

#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Oper1187768149.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Operators.SkipWhileObservable`1/SkipWhile_<System.Object>
struct  SkipWhile__t3820853950  : public OperatorObserverBase_2_t1187768149
{
public:
	// UniRx.Operators.SkipWhileObservable`1<T> UniRx.Operators.SkipWhileObservable`1/SkipWhile_::parent
	SkipWhileObservable_1_t2542074388 * ___parent_2;
	// System.Boolean UniRx.Operators.SkipWhileObservable`1/SkipWhile_::endSkip
	bool ___endSkip_3;
	// System.Int32 UniRx.Operators.SkipWhileObservable`1/SkipWhile_::index
	int32_t ___index_4;

public:
	inline static int32_t get_offset_of_parent_2() { return static_cast<int32_t>(offsetof(SkipWhile__t3820853950, ___parent_2)); }
	inline SkipWhileObservable_1_t2542074388 * get_parent_2() const { return ___parent_2; }
	inline SkipWhileObservable_1_t2542074388 ** get_address_of_parent_2() { return &___parent_2; }
	inline void set_parent_2(SkipWhileObservable_1_t2542074388 * value)
	{
		___parent_2 = value;
		Il2CppCodeGenWriteBarrier(&___parent_2, value);
	}

	inline static int32_t get_offset_of_endSkip_3() { return static_cast<int32_t>(offsetof(SkipWhile__t3820853950, ___endSkip_3)); }
	inline bool get_endSkip_3() const { return ___endSkip_3; }
	inline bool* get_address_of_endSkip_3() { return &___endSkip_3; }
	inline void set_endSkip_3(bool value)
	{
		___endSkip_3 = value;
	}

	inline static int32_t get_offset_of_index_4() { return static_cast<int32_t>(offsetof(SkipWhile__t3820853950, ___index_4)); }
	inline int32_t get_index_4() const { return ___index_4; }
	inline int32_t* get_address_of_index_4() { return &___index_4; }
	inline void set_index_4(int32_t value)
	{
		___index_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
