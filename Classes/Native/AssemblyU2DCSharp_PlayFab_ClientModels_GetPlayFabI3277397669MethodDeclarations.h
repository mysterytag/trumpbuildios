﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.ClientModels.GetPlayFabIDsFromGoogleIDsRequest
struct GetPlayFabIDsFromGoogleIDsRequest_t3277397669;
// System.Collections.Generic.List`1<System.String>
struct List_1_t1765447871;

#include "codegen/il2cpp-codegen.h"

// System.Void PlayFab.ClientModels.GetPlayFabIDsFromGoogleIDsRequest::.ctor()
extern "C"  void GetPlayFabIDsFromGoogleIDsRequest__ctor_m2357160228 (GetPlayFabIDsFromGoogleIDsRequest_t3277397669 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<System.String> PlayFab.ClientModels.GetPlayFabIDsFromGoogleIDsRequest::get_GoogleIDs()
extern "C"  List_1_t1765447871 * GetPlayFabIDsFromGoogleIDsRequest_get_GoogleIDs_m1138810087 (GetPlayFabIDsFromGoogleIDsRequest_t3277397669 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.GetPlayFabIDsFromGoogleIDsRequest::set_GoogleIDs(System.Collections.Generic.List`1<System.String>)
extern "C"  void GetPlayFabIDsFromGoogleIDsRequest_set_GoogleIDs_m3866847032 (GetPlayFabIDsFromGoogleIDsRequest_t3277397669 * __this, List_1_t1765447871 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
