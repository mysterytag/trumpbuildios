﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.Json.JsonLegacyTest
struct JsonLegacyTest_t3019859124;

#include "codegen/il2cpp-codegen.h"

// System.Void PlayFab.Json.JsonLegacyTest::.ctor()
extern "C"  void JsonLegacyTest__ctor_m3136130493 (JsonLegacyTest_t3019859124 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.Json.JsonLegacyTest::TestLegacySignature()
extern "C"  void JsonLegacyTest_TestLegacySignature_m412489240 (JsonLegacyTest_t3019859124 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
