﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Zenject.KeyedFactory`4<System.Object,System.Object,System.Object,System.Object>
struct KeyedFactory_4_t2209276758;
// System.Type[]
struct TypeU5BU5D_t3431720054;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"

// System.Void Zenject.KeyedFactory`4<System.Object,System.Object,System.Object,System.Object>::.ctor()
extern "C"  void KeyedFactory_4__ctor_m1649020324_gshared (KeyedFactory_4_t2209276758 * __this, const MethodInfo* method);
#define KeyedFactory_4__ctor_m1649020324(__this, method) ((  void (*) (KeyedFactory_4_t2209276758 *, const MethodInfo*))KeyedFactory_4__ctor_m1649020324_gshared)(__this, method)
// System.Type[] Zenject.KeyedFactory`4<System.Object,System.Object,System.Object,System.Object>::get_ProvidedTypes()
extern "C"  TypeU5BU5D_t3431720054* KeyedFactory_4_get_ProvidedTypes_m2568415399_gshared (KeyedFactory_4_t2209276758 * __this, const MethodInfo* method);
#define KeyedFactory_4_get_ProvidedTypes_m2568415399(__this, method) ((  TypeU5BU5D_t3431720054* (*) (KeyedFactory_4_t2209276758 *, const MethodInfo*))KeyedFactory_4_get_ProvidedTypes_m2568415399_gshared)(__this, method)
// TBase Zenject.KeyedFactory`4<System.Object,System.Object,System.Object,System.Object>::Create(TKey,TParam1,TParam2)
extern "C"  Il2CppObject * KeyedFactory_4_Create_m2897902698_gshared (KeyedFactory_4_t2209276758 * __this, Il2CppObject * ___key0, Il2CppObject * ___param11, Il2CppObject * ___param22, const MethodInfo* method);
#define KeyedFactory_4_Create_m2897902698(__this, ___key0, ___param11, ___param22, method) ((  Il2CppObject * (*) (KeyedFactory_4_t2209276758 *, Il2CppObject *, Il2CppObject *, Il2CppObject *, const MethodInfo*))KeyedFactory_4_Create_m2897902698_gshared)(__this, ___key0, ___param11, ___param22, method)
