﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.SkipObservable`1<System.Boolean>
struct SkipObservable_1_t197279106;
// UniRx.IObservable`1<System.Boolean>
struct IObservable_1_t4264771001;
// UniRx.IScheduler
struct IScheduler_t2938318244;
// System.IDisposable
struct IDisposable_t1628921374;
// UniRx.IObserver`1<System.Boolean>
struct IObserver_1_t2423004244;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_TimeSpan763862892.h"

// System.Void UniRx.Operators.SkipObservable`1<System.Boolean>::.ctor(UniRx.IObservable`1<T>,System.Int32)
extern "C"  void SkipObservable_1__ctor_m1792269452_gshared (SkipObservable_1_t197279106 * __this, Il2CppObject* ___source0, int32_t ___count1, const MethodInfo* method);
#define SkipObservable_1__ctor_m1792269452(__this, ___source0, ___count1, method) ((  void (*) (SkipObservable_1_t197279106 *, Il2CppObject*, int32_t, const MethodInfo*))SkipObservable_1__ctor_m1792269452_gshared)(__this, ___source0, ___count1, method)
// System.Void UniRx.Operators.SkipObservable`1<System.Boolean>::.ctor(UniRx.IObservable`1<T>,System.TimeSpan,UniRx.IScheduler)
extern "C"  void SkipObservable_1__ctor_m3503003963_gshared (SkipObservable_1_t197279106 * __this, Il2CppObject* ___source0, TimeSpan_t763862892  ___duration1, Il2CppObject * ___scheduler2, const MethodInfo* method);
#define SkipObservable_1__ctor_m3503003963(__this, ___source0, ___duration1, ___scheduler2, method) ((  void (*) (SkipObservable_1_t197279106 *, Il2CppObject*, TimeSpan_t763862892 , Il2CppObject *, const MethodInfo*))SkipObservable_1__ctor_m3503003963_gshared)(__this, ___source0, ___duration1, ___scheduler2, method)
// UniRx.IObservable`1<T> UniRx.Operators.SkipObservable`1<System.Boolean>::Combine(System.Int32)
extern "C"  Il2CppObject* SkipObservable_1_Combine_m400455926_gshared (SkipObservable_1_t197279106 * __this, int32_t ___count0, const MethodInfo* method);
#define SkipObservable_1_Combine_m400455926(__this, ___count0, method) ((  Il2CppObject* (*) (SkipObservable_1_t197279106 *, int32_t, const MethodInfo*))SkipObservable_1_Combine_m400455926_gshared)(__this, ___count0, method)
// UniRx.IObservable`1<T> UniRx.Operators.SkipObservable`1<System.Boolean>::Combine(System.TimeSpan)
extern "C"  Il2CppObject* SkipObservable_1_Combine_m3873617879_gshared (SkipObservable_1_t197279106 * __this, TimeSpan_t763862892  ___duration0, const MethodInfo* method);
#define SkipObservable_1_Combine_m3873617879(__this, ___duration0, method) ((  Il2CppObject* (*) (SkipObservable_1_t197279106 *, TimeSpan_t763862892 , const MethodInfo*))SkipObservable_1_Combine_m3873617879_gshared)(__this, ___duration0, method)
// System.IDisposable UniRx.Operators.SkipObservable`1<System.Boolean>::SubscribeCore(UniRx.IObserver`1<T>,System.IDisposable)
extern "C"  Il2CppObject * SkipObservable_1_SubscribeCore_m4094822482_gshared (SkipObservable_1_t197279106 * __this, Il2CppObject* ___observer0, Il2CppObject * ___cancel1, const MethodInfo* method);
#define SkipObservable_1_SubscribeCore_m4094822482(__this, ___observer0, ___cancel1, method) ((  Il2CppObject * (*) (SkipObservable_1_t197279106 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))SkipObservable_1_SubscribeCore_m4094822482_gshared)(__this, ___observer0, ___cancel1, method)
