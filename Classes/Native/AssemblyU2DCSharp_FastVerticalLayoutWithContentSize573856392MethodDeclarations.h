﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// FastVerticalLayoutWithContentSizeFitter
struct FastVerticalLayoutWithContentSizeFitter_t573856392;

#include "codegen/il2cpp-codegen.h"

// System.Void FastVerticalLayoutWithContentSizeFitter::.ctor()
extern "C"  void FastVerticalLayoutWithContentSizeFitter__ctor_m3668479459 (FastVerticalLayoutWithContentSizeFitter_t573856392 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FastVerticalLayoutWithContentSizeFitter::OnTransformChildrenChanged()
extern "C"  void FastVerticalLayoutWithContentSizeFitter_OnTransformChildrenChanged_m3461089449 (FastVerticalLayoutWithContentSizeFitter_t573856392 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FastVerticalLayoutWithContentSizeFitter::LateUpdate()
extern "C"  void FastVerticalLayoutWithContentSizeFitter_LateUpdate_m634974800 (FastVerticalLayoutWithContentSizeFitter_t573856392 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
