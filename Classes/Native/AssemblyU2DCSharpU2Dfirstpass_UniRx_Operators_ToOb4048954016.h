﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Generic.IEnumerable`1<System.Object>
struct IEnumerable_1_t3709260776;
// UniRx.IScheduler
struct IScheduler_t2938318244;

#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Oper4196218687.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Operators.ToObservableObservable`1<System.Object>
struct  ToObservableObservable_1_t4048954016  : public OperatorObservableBase_1_t4196218687
{
public:
	// System.Collections.Generic.IEnumerable`1<T> UniRx.Operators.ToObservableObservable`1::source
	Il2CppObject* ___source_1;
	// UniRx.IScheduler UniRx.Operators.ToObservableObservable`1::scheduler
	Il2CppObject * ___scheduler_2;

public:
	inline static int32_t get_offset_of_source_1() { return static_cast<int32_t>(offsetof(ToObservableObservable_1_t4048954016, ___source_1)); }
	inline Il2CppObject* get_source_1() const { return ___source_1; }
	inline Il2CppObject** get_address_of_source_1() { return &___source_1; }
	inline void set_source_1(Il2CppObject* value)
	{
		___source_1 = value;
		Il2CppCodeGenWriteBarrier(&___source_1, value);
	}

	inline static int32_t get_offset_of_scheduler_2() { return static_cast<int32_t>(offsetof(ToObservableObservable_1_t4048954016, ___scheduler_2)); }
	inline Il2CppObject * get_scheduler_2() const { return ___scheduler_2; }
	inline Il2CppObject ** get_address_of_scheduler_2() { return &___scheduler_2; }
	inline void set_scheduler_2(Il2CppObject * value)
	{
		___scheduler_2 = value;
		Il2CppCodeGenWriteBarrier(&___scheduler_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
