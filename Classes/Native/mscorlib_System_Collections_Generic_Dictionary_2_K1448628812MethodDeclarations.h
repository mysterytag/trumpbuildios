﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Key33615866MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.String,System.Collections.Generic.KeyValuePair`2<System.Type,PlayFab.ReflectionUtils/SetDelegate>>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define KeyCollection__ctor_m2031734945(__this, ___dictionary0, method) ((  void (*) (KeyCollection_t1448628812 *, Dictionary_2_t3420320828 *, const MethodInfo*))KeyCollection__ctor_m2104670387_gshared)(__this, ___dictionary0, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.String,System.Collections.Generic.KeyValuePair`2<System.Type,PlayFab.ReflectionUtils/SetDelegate>>::System.Collections.Generic.ICollection<TKey>.Add(TKey)
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m190965781(__this, ___item0, method) ((  void (*) (KeyCollection_t1448628812 *, String_t*, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m2501426371_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.String,System.Collections.Generic.KeyValuePair`2<System.Type,PlayFab.ReflectionUtils/SetDelegate>>::System.Collections.Generic.ICollection<TKey>.Clear()
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m2243759820(__this, method) ((  void (*) (KeyCollection_t1448628812 *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m3604821754_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.String,System.Collections.Generic.KeyValuePair`2<System.Type,PlayFab.ReflectionUtils/SetDelegate>>::System.Collections.Generic.ICollection<TKey>.Contains(TKey)
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m3468109657(__this, ___item0, method) ((  bool (*) (KeyCollection_t1448628812 *, String_t*, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m2766578795_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.String,System.Collections.Generic.KeyValuePair`2<System.Type,PlayFab.ReflectionUtils/SetDelegate>>::System.Collections.Generic.ICollection<TKey>.Remove(TKey)
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m455580414(__this, ___item0, method) ((  bool (*) (KeyCollection_t1448628812 *, String_t*, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m517420176_gshared)(__this, ___item0, method)
// System.Collections.Generic.IEnumerator`1<TKey> System.Collections.Generic.Dictionary`2/KeyCollection<System.String,System.Collections.Generic.KeyValuePair`2<System.Type,PlayFab.ReflectionUtils/SetDelegate>>::System.Collections.Generic.IEnumerable<TKey>.GetEnumerator()
#define KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m264677598(__this, method) ((  Il2CppObject* (*) (KeyCollection_t1448628812 *, const MethodInfo*))KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m667479692_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.String,System.Collections.Generic.KeyValuePair`2<System.Type,PlayFab.ReflectionUtils/SetDelegate>>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define KeyCollection_System_Collections_ICollection_CopyTo_m3445825854(__this, ___array0, ___index1, method) ((  void (*) (KeyCollection_t1448628812 *, Il2CppArray *, int32_t, const MethodInfo*))KeyCollection_System_Collections_ICollection_CopyTo_m3272766572_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2/KeyCollection<System.String,System.Collections.Generic.KeyValuePair`2<System.Type,PlayFab.ReflectionUtils/SetDelegate>>::System.Collections.IEnumerable.GetEnumerator()
#define KeyCollection_System_Collections_IEnumerable_GetEnumerator_m922468813(__this, method) ((  Il2CppObject * (*) (KeyCollection_t1448628812 *, const MethodInfo*))KeyCollection_System_Collections_IEnumerable_GetEnumerator_m1858903419_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.String,System.Collections.Generic.KeyValuePair`2<System.Type,PlayFab.ReflectionUtils/SetDelegate>>::System.Collections.Generic.ICollection<TKey>.get_IsReadOnly()
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m2839817338(__this, method) ((  bool (*) (KeyCollection_t1448628812 *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m2978524428_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.String,System.Collections.Generic.KeyValuePair`2<System.Type,PlayFab.ReflectionUtils/SetDelegate>>::System.Collections.ICollection.get_IsSynchronized()
#define KeyCollection_System_Collections_ICollection_get_IsSynchronized_m3024266668(__this, method) ((  bool (*) (KeyCollection_t1448628812 *, const MethodInfo*))KeyCollection_System_Collections_ICollection_get_IsSynchronized_m3015791806_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/KeyCollection<System.String,System.Collections.Generic.KeyValuePair`2<System.Type,PlayFab.ReflectionUtils/SetDelegate>>::System.Collections.ICollection.get_SyncRoot()
#define KeyCollection_System_Collections_ICollection_get_SyncRoot_m1286968478(__this, method) ((  Il2CppObject * (*) (KeyCollection_t1448628812 *, const MethodInfo*))KeyCollection_System_Collections_ICollection_get_SyncRoot_m3643560112_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.String,System.Collections.Generic.KeyValuePair`2<System.Type,PlayFab.ReflectionUtils/SetDelegate>>::CopyTo(TKey[],System.Int32)
#define KeyCollection_CopyTo_m4054448982(__this, ___array0, ___index1, method) ((  void (*) (KeyCollection_t1448628812 *, StringU5BU5D_t2956870243*, int32_t, const MethodInfo*))KeyCollection_CopyTo_m3214388072_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/KeyCollection<System.String,System.Collections.Generic.KeyValuePair`2<System.Type,PlayFab.ReflectionUtils/SetDelegate>>::GetEnumerator()
#define KeyCollection_GetEnumerator_m1358992867(__this, method) ((  Enumerator_t3187348770  (*) (KeyCollection_t1448628812 *, const MethodInfo*))KeyCollection_GetEnumerator_m1263936885_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2/KeyCollection<System.String,System.Collections.Generic.KeyValuePair`2<System.Type,PlayFab.ReflectionUtils/SetDelegate>>::get_Count()
#define KeyCollection_get_Count_m1034073574(__this, method) ((  int32_t (*) (KeyCollection_t1448628812 *, const MethodInfo*))KeyCollection_get_Count_m3825201144_gshared)(__this, method)
