﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Camera
struct Camera_t3533968274;
// PreviewIconProvider
struct PreviewIconProvider_t2704585042;
// UnityEngine.GameObject
struct GameObject_t4012695102;
// Tacticsoft.IEndDragHandlerEx
struct IEndDragHandlerEx_t3278221901;

#include "UnityEngine_UI_UnityEngine_UI_ScrollRect1048578170.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Tacticsoft.ScrollRectWithDrag
struct  ScrollRectWithDrag_t261095137  : public ScrollRect_t1048578170
{
public:
	// UnityEngine.Camera Tacticsoft.ScrollRectWithDrag::worldCamera
	Camera_t3533968274 * ___worldCamera_38;
	// PreviewIconProvider Tacticsoft.ScrollRectWithDrag::previewIconProvider
	PreviewIconProvider_t2704585042 * ___previewIconProvider_39;
	// UnityEngine.GameObject Tacticsoft.ScrollRectWithDrag::DraggableIcon
	GameObject_t4012695102 * ___DraggableIcon_40;
	// Tacticsoft.IEndDragHandlerEx Tacticsoft.ScrollRectWithDrag::endDragHandler
	Il2CppObject * ___endDragHandler_41;
	// UnityEngine.GameObject Tacticsoft.ScrollRectWithDrag::previewIcon
	GameObject_t4012695102 * ___previewIcon_42;

public:
	inline static int32_t get_offset_of_worldCamera_38() { return static_cast<int32_t>(offsetof(ScrollRectWithDrag_t261095137, ___worldCamera_38)); }
	inline Camera_t3533968274 * get_worldCamera_38() const { return ___worldCamera_38; }
	inline Camera_t3533968274 ** get_address_of_worldCamera_38() { return &___worldCamera_38; }
	inline void set_worldCamera_38(Camera_t3533968274 * value)
	{
		___worldCamera_38 = value;
		Il2CppCodeGenWriteBarrier(&___worldCamera_38, value);
	}

	inline static int32_t get_offset_of_previewIconProvider_39() { return static_cast<int32_t>(offsetof(ScrollRectWithDrag_t261095137, ___previewIconProvider_39)); }
	inline PreviewIconProvider_t2704585042 * get_previewIconProvider_39() const { return ___previewIconProvider_39; }
	inline PreviewIconProvider_t2704585042 ** get_address_of_previewIconProvider_39() { return &___previewIconProvider_39; }
	inline void set_previewIconProvider_39(PreviewIconProvider_t2704585042 * value)
	{
		___previewIconProvider_39 = value;
		Il2CppCodeGenWriteBarrier(&___previewIconProvider_39, value);
	}

	inline static int32_t get_offset_of_DraggableIcon_40() { return static_cast<int32_t>(offsetof(ScrollRectWithDrag_t261095137, ___DraggableIcon_40)); }
	inline GameObject_t4012695102 * get_DraggableIcon_40() const { return ___DraggableIcon_40; }
	inline GameObject_t4012695102 ** get_address_of_DraggableIcon_40() { return &___DraggableIcon_40; }
	inline void set_DraggableIcon_40(GameObject_t4012695102 * value)
	{
		___DraggableIcon_40 = value;
		Il2CppCodeGenWriteBarrier(&___DraggableIcon_40, value);
	}

	inline static int32_t get_offset_of_endDragHandler_41() { return static_cast<int32_t>(offsetof(ScrollRectWithDrag_t261095137, ___endDragHandler_41)); }
	inline Il2CppObject * get_endDragHandler_41() const { return ___endDragHandler_41; }
	inline Il2CppObject ** get_address_of_endDragHandler_41() { return &___endDragHandler_41; }
	inline void set_endDragHandler_41(Il2CppObject * value)
	{
		___endDragHandler_41 = value;
		Il2CppCodeGenWriteBarrier(&___endDragHandler_41, value);
	}

	inline static int32_t get_offset_of_previewIcon_42() { return static_cast<int32_t>(offsetof(ScrollRectWithDrag_t261095137, ___previewIcon_42)); }
	inline GameObject_t4012695102 * get_previewIcon_42() const { return ___previewIcon_42; }
	inline GameObject_t4012695102 ** get_address_of_previewIcon_42() { return &___previewIcon_42; }
	inline void set_previewIcon_42(GameObject_t4012695102 * value)
	{
		___previewIcon_42 = value;
		Il2CppCodeGenWriteBarrier(&___previewIcon_42, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
