﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Object
struct Il2CppObject;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_DictionaryAddEv250981008.h"
#include "mscorlib_System_Object837106420.h"

// System.Void UniRx.DictionaryAddEvent`2<System.Object,System.Object>::.ctor(TKey,TValue)
extern "C"  void DictionaryAddEvent_2__ctor_m3319540014_gshared (DictionaryAddEvent_2_t250981008 * __this, Il2CppObject * ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define DictionaryAddEvent_2__ctor_m3319540014(__this, ___key0, ___value1, method) ((  void (*) (DictionaryAddEvent_2_t250981008 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))DictionaryAddEvent_2__ctor_m3319540014_gshared)(__this, ___key0, ___value1, method)
// TKey UniRx.DictionaryAddEvent`2<System.Object,System.Object>::get_Key()
extern "C"  Il2CppObject * DictionaryAddEvent_2_get_Key_m4011898708_gshared (DictionaryAddEvent_2_t250981008 * __this, const MethodInfo* method);
#define DictionaryAddEvent_2_get_Key_m4011898708(__this, method) ((  Il2CppObject * (*) (DictionaryAddEvent_2_t250981008 *, const MethodInfo*))DictionaryAddEvent_2_get_Key_m4011898708_gshared)(__this, method)
// System.Void UniRx.DictionaryAddEvent`2<System.Object,System.Object>::set_Key(TKey)
extern "C"  void DictionaryAddEvent_2_set_Key_m1422765115_gshared (DictionaryAddEvent_2_t250981008 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define DictionaryAddEvent_2_set_Key_m1422765115(__this, ___value0, method) ((  void (*) (DictionaryAddEvent_2_t250981008 *, Il2CppObject *, const MethodInfo*))DictionaryAddEvent_2_set_Key_m1422765115_gshared)(__this, ___value0, method)
// TValue UniRx.DictionaryAddEvent`2<System.Object,System.Object>::get_Value()
extern "C"  Il2CppObject * DictionaryAddEvent_2_get_Value_m3482472916_gshared (DictionaryAddEvent_2_t250981008 * __this, const MethodInfo* method);
#define DictionaryAddEvent_2_get_Value_m3482472916(__this, method) ((  Il2CppObject * (*) (DictionaryAddEvent_2_t250981008 *, const MethodInfo*))DictionaryAddEvent_2_get_Value_m3482472916_gshared)(__this, method)
// System.Void UniRx.DictionaryAddEvent`2<System.Object,System.Object>::set_Value(TValue)
extern "C"  void DictionaryAddEvent_2_set_Value_m2511497915_gshared (DictionaryAddEvent_2_t250981008 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define DictionaryAddEvent_2_set_Value_m2511497915(__this, ___value0, method) ((  void (*) (DictionaryAddEvent_2_t250981008 *, Il2CppObject *, const MethodInfo*))DictionaryAddEvent_2_set_Value_m2511497915_gshared)(__this, ___value0, method)
// System.String UniRx.DictionaryAddEvent`2<System.Object,System.Object>::ToString()
extern "C"  String_t* DictionaryAddEvent_2_ToString_m208558471_gshared (DictionaryAddEvent_2_t250981008 * __this, const MethodInfo* method);
#define DictionaryAddEvent_2_ToString_m208558471(__this, method) ((  String_t* (*) (DictionaryAddEvent_2_t250981008 *, const MethodInfo*))DictionaryAddEvent_2_ToString_m208558471_gshared)(__this, method)
// System.Int32 UniRx.DictionaryAddEvent`2<System.Object,System.Object>::GetHashCode()
extern "C"  int32_t DictionaryAddEvent_2_GetHashCode_m1247635339_gshared (DictionaryAddEvent_2_t250981008 * __this, const MethodInfo* method);
#define DictionaryAddEvent_2_GetHashCode_m1247635339(__this, method) ((  int32_t (*) (DictionaryAddEvent_2_t250981008 *, const MethodInfo*))DictionaryAddEvent_2_GetHashCode_m1247635339_gshared)(__this, method)
// System.Boolean UniRx.DictionaryAddEvent`2<System.Object,System.Object>::Equals(UniRx.DictionaryAddEvent`2<TKey,TValue>)
extern "C"  bool DictionaryAddEvent_2_Equals_m913248142_gshared (DictionaryAddEvent_2_t250981008 * __this, DictionaryAddEvent_2_t250981008  ___other0, const MethodInfo* method);
#define DictionaryAddEvent_2_Equals_m913248142(__this, ___other0, method) ((  bool (*) (DictionaryAddEvent_2_t250981008 *, DictionaryAddEvent_2_t250981008 , const MethodInfo*))DictionaryAddEvent_2_Equals_m913248142_gshared)(__this, ___other0, method)
