﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.FromEventObservable`1<System.Object>
struct FromEventObservable_1_t2790472066;
// System.Func`2<System.Action,System.Object>
struct Func_2_t2670270213;
// System.Action`1<System.Object>
struct Action_1_t985559125;
// System.IDisposable
struct IDisposable_t1628921374;
// UniRx.IObserver`1<UniRx.Unit>
struct IObserver_1_t475317645;

#include "codegen/il2cpp-codegen.h"

// System.Void UniRx.Operators.FromEventObservable`1<System.Object>::.ctor(System.Func`2<System.Action,TDelegate>,System.Action`1<TDelegate>,System.Action`1<TDelegate>)
extern "C"  void FromEventObservable_1__ctor_m1522889437_gshared (FromEventObservable_1_t2790472066 * __this, Func_2_t2670270213 * ___conversion0, Action_1_t985559125 * ___addHandler1, Action_1_t985559125 * ___removeHandler2, const MethodInfo* method);
#define FromEventObservable_1__ctor_m1522889437(__this, ___conversion0, ___addHandler1, ___removeHandler2, method) ((  void (*) (FromEventObservable_1_t2790472066 *, Func_2_t2670270213 *, Action_1_t985559125 *, Action_1_t985559125 *, const MethodInfo*))FromEventObservable_1__ctor_m1522889437_gshared)(__this, ___conversion0, ___addHandler1, ___removeHandler2, method)
// System.IDisposable UniRx.Operators.FromEventObservable`1<System.Object>::SubscribeCore(UniRx.IObserver`1<UniRx.Unit>,System.IDisposable)
extern "C"  Il2CppObject * FromEventObservable_1_SubscribeCore_m3939969428_gshared (FromEventObservable_1_t2790472066 * __this, Il2CppObject* ___observer0, Il2CppObject * ___cancel1, const MethodInfo* method);
#define FromEventObservable_1_SubscribeCore_m3939969428(__this, ___observer0, ___cancel1, method) ((  Il2CppObject * (*) (FromEventObservable_1_t2790472066 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))FromEventObservable_1_SubscribeCore_m3939969428_gshared)(__this, ___observer0, ___cancel1, method)
