﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Object
struct Il2CppObject;
// System.IDisposable
struct IDisposable_t1628921374;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CellReactiveApi/DisposableContainer`1<System.Object>
struct  DisposableContainer_1_t3505922843  : public Il2CppObject
{
public:
	// T CellReactiveApi/DisposableContainer`1::value
	Il2CppObject * ___value_0;
	// System.IDisposable CellReactiveApi/DisposableContainer`1::disp
	Il2CppObject * ___disp_1;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(DisposableContainer_1_t3505922843, ___value_0)); }
	inline Il2CppObject * get_value_0() const { return ___value_0; }
	inline Il2CppObject ** get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(Il2CppObject * value)
	{
		___value_0 = value;
		Il2CppCodeGenWriteBarrier(&___value_0, value);
	}

	inline static int32_t get_offset_of_disp_1() { return static_cast<int32_t>(offsetof(DisposableContainer_1_t3505922843, ___disp_1)); }
	inline Il2CppObject * get_disp_1() const { return ___disp_1; }
	inline Il2CppObject ** get_address_of_disp_1() { return &___disp_1; }
	inline void set_disp_1(Il2CppObject * value)
	{
		___disp_1 = value;
		Il2CppCodeGenWriteBarrier(&___disp_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
