﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UniRx.Operators.DefaultIfEmptyObservable`1<System.Object>
struct DefaultIfEmptyObservable_1_t2718954633;

#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Oper1187768149.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Operators.DefaultIfEmptyObservable`1/DefaultIfEmpty<System.Object>
struct  DefaultIfEmpty_t3912419440  : public OperatorObserverBase_2_t1187768149
{
public:
	// UniRx.Operators.DefaultIfEmptyObservable`1<T> UniRx.Operators.DefaultIfEmptyObservable`1/DefaultIfEmpty::parent
	DefaultIfEmptyObservable_1_t2718954633 * ___parent_2;
	// System.Boolean UniRx.Operators.DefaultIfEmptyObservable`1/DefaultIfEmpty::hasValue
	bool ___hasValue_3;

public:
	inline static int32_t get_offset_of_parent_2() { return static_cast<int32_t>(offsetof(DefaultIfEmpty_t3912419440, ___parent_2)); }
	inline DefaultIfEmptyObservable_1_t2718954633 * get_parent_2() const { return ___parent_2; }
	inline DefaultIfEmptyObservable_1_t2718954633 ** get_address_of_parent_2() { return &___parent_2; }
	inline void set_parent_2(DefaultIfEmptyObservable_1_t2718954633 * value)
	{
		___parent_2 = value;
		Il2CppCodeGenWriteBarrier(&___parent_2, value);
	}

	inline static int32_t get_offset_of_hasValue_3() { return static_cast<int32_t>(offsetof(DefaultIfEmpty_t3912419440, ___hasValue_3)); }
	inline bool get_hasValue_3() const { return ___hasValue_3; }
	inline bool* get_address_of_hasValue_3() { return &___hasValue_3; }
	inline void set_hasValue_3(bool value)
	{
		___hasValue_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
