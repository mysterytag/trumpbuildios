﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.BufferObservable`1<System.Object>
struct BufferObservable_1_t574294898;
// UniRx.IObservable`1<System.Object>
struct IObservable_1_t595904784;
// UniRx.IScheduler
struct IScheduler_t2938318244;
// System.IDisposable
struct IDisposable_t1628921374;
// UniRx.IObserver`1<System.Collections.Generic.IList`1<System.Object>>
struct IObserver_1_t920630341;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_TimeSpan763862892.h"

// System.Void UniRx.Operators.BufferObservable`1<System.Object>::.ctor(UniRx.IObservable`1<T>,System.Int32,System.Int32)
extern "C"  void BufferObservable_1__ctor_m1904451461_gshared (BufferObservable_1_t574294898 * __this, Il2CppObject* ___source0, int32_t ___count1, int32_t ___skip2, const MethodInfo* method);
#define BufferObservable_1__ctor_m1904451461(__this, ___source0, ___count1, ___skip2, method) ((  void (*) (BufferObservable_1_t574294898 *, Il2CppObject*, int32_t, int32_t, const MethodInfo*))BufferObservable_1__ctor_m1904451461_gshared)(__this, ___source0, ___count1, ___skip2, method)
// System.Void UniRx.Operators.BufferObservable`1<System.Object>::.ctor(UniRx.IObservable`1<T>,System.TimeSpan,System.TimeSpan,UniRx.IScheduler)
extern "C"  void BufferObservable_1__ctor_m1677543595_gshared (BufferObservable_1_t574294898 * __this, Il2CppObject* ___source0, TimeSpan_t763862892  ___timeSpan1, TimeSpan_t763862892  ___timeShift2, Il2CppObject * ___scheduler3, const MethodInfo* method);
#define BufferObservable_1__ctor_m1677543595(__this, ___source0, ___timeSpan1, ___timeShift2, ___scheduler3, method) ((  void (*) (BufferObservable_1_t574294898 *, Il2CppObject*, TimeSpan_t763862892 , TimeSpan_t763862892 , Il2CppObject *, const MethodInfo*))BufferObservable_1__ctor_m1677543595_gshared)(__this, ___source0, ___timeSpan1, ___timeShift2, ___scheduler3, method)
// System.Void UniRx.Operators.BufferObservable`1<System.Object>::.ctor(UniRx.IObservable`1<T>,System.TimeSpan,System.Int32,UniRx.IScheduler)
extern "C"  void BufferObservable_1__ctor_m2289049376_gshared (BufferObservable_1_t574294898 * __this, Il2CppObject* ___source0, TimeSpan_t763862892  ___timeSpan1, int32_t ___count2, Il2CppObject * ___scheduler3, const MethodInfo* method);
#define BufferObservable_1__ctor_m2289049376(__this, ___source0, ___timeSpan1, ___count2, ___scheduler3, method) ((  void (*) (BufferObservable_1_t574294898 *, Il2CppObject*, TimeSpan_t763862892 , int32_t, Il2CppObject *, const MethodInfo*))BufferObservable_1__ctor_m2289049376_gshared)(__this, ___source0, ___timeSpan1, ___count2, ___scheduler3, method)
// System.IDisposable UniRx.Operators.BufferObservable`1<System.Object>::SubscribeCore(UniRx.IObserver`1<System.Collections.Generic.IList`1<T>>,System.IDisposable)
extern "C"  Il2CppObject * BufferObservable_1_SubscribeCore_m2908667513_gshared (BufferObservable_1_t574294898 * __this, Il2CppObject* ___observer0, Il2CppObject * ___cancel1, const MethodInfo* method);
#define BufferObservable_1_SubscribeCore_m2908667513(__this, ___observer0, ___cancel1, method) ((  Il2CppObject * (*) (BufferObservable_1_t574294898 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))BufferObservable_1_SubscribeCore_m2908667513_gshared)(__this, ___observer0, ___cancel1, method)
