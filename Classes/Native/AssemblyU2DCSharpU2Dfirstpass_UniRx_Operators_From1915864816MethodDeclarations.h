﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.FromEventObservable`2/FromEvent<System.Object,System.Boolean>
struct FromEvent_t1915864816;
// UniRx.Operators.FromEventObservable`2<System.Object,System.Boolean>
struct FromEventObservable_2_t1626038518;
// UniRx.IObserver`1<System.Boolean>
struct IObserver_1_t2423004244;

#include "codegen/il2cpp-codegen.h"

// System.Void UniRx.Operators.FromEventObservable`2/FromEvent<System.Object,System.Boolean>::.ctor(UniRx.Operators.FromEventObservable`2<TDelegate,TEventArgs>,UniRx.IObserver`1<TEventArgs>)
extern "C"  void FromEvent__ctor_m3420522768_gshared (FromEvent_t1915864816 * __this, FromEventObservable_2_t1626038518 * ___parent0, Il2CppObject* ___observer1, const MethodInfo* method);
#define FromEvent__ctor_m3420522768(__this, ___parent0, ___observer1, method) ((  void (*) (FromEvent_t1915864816 *, FromEventObservable_2_t1626038518 *, Il2CppObject*, const MethodInfo*))FromEvent__ctor_m3420522768_gshared)(__this, ___parent0, ___observer1, method)
// System.Boolean UniRx.Operators.FromEventObservable`2/FromEvent<System.Object,System.Boolean>::Register()
extern "C"  bool FromEvent_Register_m3962354030_gshared (FromEvent_t1915864816 * __this, const MethodInfo* method);
#define FromEvent_Register_m3962354030(__this, method) ((  bool (*) (FromEvent_t1915864816 *, const MethodInfo*))FromEvent_Register_m3962354030_gshared)(__this, method)
// System.Void UniRx.Operators.FromEventObservable`2/FromEvent<System.Object,System.Boolean>::OnNext(TEventArgs)
extern "C"  void FromEvent_OnNext_m2753720278_gshared (FromEvent_t1915864816 * __this, bool ___args0, const MethodInfo* method);
#define FromEvent_OnNext_m2753720278(__this, ___args0, method) ((  void (*) (FromEvent_t1915864816 *, bool, const MethodInfo*))FromEvent_OnNext_m2753720278_gshared)(__this, ___args0, method)
// System.Void UniRx.Operators.FromEventObservable`2/FromEvent<System.Object,System.Boolean>::Dispose()
extern "C"  void FromEvent_Dispose_m4210440698_gshared (FromEvent_t1915864816 * __this, const MethodInfo* method);
#define FromEvent_Dispose_m4210440698(__this, method) ((  void (*) (FromEvent_t1915864816 *, const MethodInfo*))FromEvent_Dispose_m4210440698_gshared)(__this, method)
