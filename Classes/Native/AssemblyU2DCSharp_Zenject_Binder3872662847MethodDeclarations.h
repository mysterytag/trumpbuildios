﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Zenject.Binder
struct Binder_t3872662847;
// Zenject.DiContainer
struct DiContainer_t2383114449;
// System.Type
struct Type_t;
// System.String
struct String_t;
// Zenject.SingletonProviderMap
struct SingletonProviderMap_t1557411893;
// Zenject.BindingConditionSetter
struct BindingConditionSetter_t259147722;
// Zenject.ProviderBase
struct ProviderBase_t1627494391;
// UnityEngine.GameObject
struct GameObject_t4012695102;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Zenject_DiContainer2383114449.h"
#include "mscorlib_System_Type2779229935.h"
#include "mscorlib_System_String968488902.h"
#include "AssemblyU2DCSharp_Zenject_SingletonProviderMap1557411893.h"
#include "AssemblyU2DCSharp_Zenject_ProviderBase1627494391.h"
#include "UnityEngine_UnityEngine_GameObject4012695102.h"
#include "mscorlib_System_Object837106420.h"

// System.Void Zenject.Binder::.ctor(Zenject.DiContainer,System.Type,System.String,Zenject.SingletonProviderMap)
extern "C"  void Binder__ctor_m406646005 (Binder_t3872662847 * __this, DiContainer_t2383114449 * ___container0, Type_t * ___contractType1, String_t* ___bindIdentifier2, SingletonProviderMap_t1557411893 * ___singletonMap3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type Zenject.Binder::get_ContractType()
extern "C"  Type_t * Binder_get_ContractType_m756953217 (Binder_t3872662847 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Zenject.BindingConditionSetter Zenject.Binder::ToTransient()
extern "C"  BindingConditionSetter_t259147722 * Binder_ToTransient_m3507354326 (Binder_t3872662847 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Zenject.BindingConditionSetter Zenject.Binder::ToTransient(System.Type)
extern "C"  BindingConditionSetter_t259147722 * Binder_ToTransient_m1128596195 (Binder_t3872662847 * __this, Type_t * ___concreteType0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Zenject.BindingConditionSetter Zenject.Binder::ToSingle()
extern "C"  BindingConditionSetter_t259147722 * Binder_ToSingle_m85759326 (Binder_t3872662847 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Zenject.BindingConditionSetter Zenject.Binder::ToSingle(System.String)
extern "C"  BindingConditionSetter_t259147722 * Binder_ToSingle_m76493412 (Binder_t3872662847 * __this, String_t* ___concreteIdentifier0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Zenject.BindingConditionSetter Zenject.Binder::ToSingle(System.Type)
extern "C"  BindingConditionSetter_t259147722 * Binder_ToSingle_m2387739483 (Binder_t3872662847 * __this, Type_t * ___concreteType0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Zenject.BindingConditionSetter Zenject.Binder::ToSingle(System.String,System.Type)
extern "C"  BindingConditionSetter_t259147722 * Binder_ToSingle_m3555871639 (Binder_t3872662847 * __this, String_t* ___concreteIdentifier0, Type_t * ___concreteType1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Zenject.BindingConditionSetter Zenject.Binder::ToSingle(System.Type,System.String)
extern "C"  BindingConditionSetter_t259147722 * Binder_ToSingle_m2861084759 (Binder_t3872662847 * __this, Type_t * ___concreteType0, String_t* ___concreteIdentifier1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Zenject.BindingConditionSetter Zenject.Binder::ToProvider(Zenject.ProviderBase)
extern "C"  BindingConditionSetter_t259147722 * Binder_ToProvider_m2864916614 (Binder_t3872662847 * __this, ProviderBase_t1627494391 * ___provider0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Zenject.BindingConditionSetter Zenject.Binder::ToSinglePrefab(System.Type,System.String,UnityEngine.GameObject)
extern "C"  BindingConditionSetter_t259147722 * Binder_ToSinglePrefab_m3074881579 (Binder_t3872662847 * __this, Type_t * ___concreteType0, String_t* ___concreteIdentifier1, GameObject_t4012695102 * ___prefab2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Zenject.BindingConditionSetter Zenject.Binder::ToTransientPrefab(System.Type,UnityEngine.GameObject)
extern "C"  BindingConditionSetter_t259147722 * Binder_ToTransientPrefab_m2568629535 (Binder_t3872662847 * __this, Type_t * ___concreteType0, GameObject_t4012695102 * ___prefab1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Zenject.BindingConditionSetter Zenject.Binder::ToSingleGameObject()
extern "C"  BindingConditionSetter_t259147722 * Binder_ToSingleGameObject_m640566991 (Binder_t3872662847 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Zenject.BindingConditionSetter Zenject.Binder::ToSingleGameObject(System.String)
extern "C"  BindingConditionSetter_t259147722 * Binder_ToSingleGameObject_m4067106963 (Binder_t3872662847 * __this, String_t* ___name0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Zenject.BindingConditionSetter Zenject.Binder::ToSingleGameObject(System.Type,System.String)
extern "C"  BindingConditionSetter_t259147722 * Binder_ToSingleGameObject_m1107152902 (Binder_t3872662847 * __this, Type_t * ___concreteType0, String_t* ___name1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Zenject.BindingConditionSetter Zenject.Binder::ToTransientPrefabResource(System.String)
extern "C"  BindingConditionSetter_t259147722 * Binder_ToTransientPrefabResource_m454791322 (Binder_t3872662847 * __this, String_t* ___resourcePath0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Zenject.BindingConditionSetter Zenject.Binder::ToTransientPrefabResource(System.Type,System.String)
extern "C"  BindingConditionSetter_t259147722 * Binder_ToTransientPrefabResource_m3139136909 (Binder_t3872662847 * __this, Type_t * ___concreteType0, String_t* ___resourcePath1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Zenject.BindingConditionSetter Zenject.Binder::ToSinglePrefabResource(System.Type,System.String,System.String)
extern "C"  BindingConditionSetter_t259147722 * Binder_ToSinglePrefabResource_m1273251201 (Binder_t3872662847 * __this, Type_t * ___concreteType0, String_t* ___concreteIdentifier1, String_t* ___resourcePath2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Zenject.BindingConditionSetter Zenject.Binder::ToSinglePrefabResource(System.String)
extern "C"  BindingConditionSetter_t259147722 * Binder_ToSinglePrefabResource_m2076746002 (Binder_t3872662847 * __this, String_t* ___resourcePath0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Zenject.BindingConditionSetter Zenject.Binder::ToSinglePrefabResource(System.String,System.String)
extern "C"  BindingConditionSetter_t259147722 * Binder_ToSinglePrefabResource_m2642510286 (Binder_t3872662847 * __this, String_t* ___identifier0, String_t* ___resourcePath1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Zenject.BindingConditionSetter Zenject.Binder::ToTransientPrefab(UnityEngine.GameObject)
extern "C"  BindingConditionSetter_t259147722 * Binder_ToTransientPrefab_m1796199346 (Binder_t3872662847 * __this, GameObject_t4012695102 * ___prefab0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Zenject.BindingConditionSetter Zenject.Binder::ToSinglePrefab(UnityEngine.GameObject)
extern "C"  BindingConditionSetter_t259147722 * Binder_ToSinglePrefab_m537186874 (Binder_t3872662847 * __this, GameObject_t4012695102 * ___prefab0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Zenject.BindingConditionSetter Zenject.Binder::ToSinglePrefab(System.String,UnityEngine.GameObject)
extern "C"  BindingConditionSetter_t259147722 * Binder_ToSinglePrefab_m883731838 (Binder_t3872662847 * __this, String_t* ___identifier0, GameObject_t4012695102 * ___prefab1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Zenject.BindingConditionSetter Zenject.Binder::ToInstance(System.Type,System.Object)
extern "C"  BindingConditionSetter_t259147722 * Binder_ToInstance_m2103854012 (Binder_t3872662847 * __this, Type_t * ___concreteType0, Il2CppObject * ___instance1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Zenject.BindingConditionSetter Zenject.Binder::ToSingleInstance(System.Type,System.String,System.Object)
extern "C"  BindingConditionSetter_t259147722 * Binder_ToSingleInstance_m1681717968 (Binder_t3872662847 * __this, Type_t * ___concreteType0, String_t* ___concreteIdentifier1, Il2CppObject * ___instance2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Zenject.BindingConditionSetter Zenject.Binder::ToResource(System.String)
extern "C"  BindingConditionSetter_t259147722 * Binder_ToResource_m700765982 (Binder_t3872662847 * __this, String_t* ___resourcePath0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Zenject.BindingConditionSetter Zenject.Binder::ToResource(System.Type,System.String)
extern "C"  BindingConditionSetter_t259147722 * Binder_ToResource_m3818589713 (Binder_t3872662847 * __this, Type_t * ___concreteType0, String_t* ___resourcePath1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
