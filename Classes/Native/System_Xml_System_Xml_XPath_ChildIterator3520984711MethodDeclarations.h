﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Xml.XPath.ChildIterator
struct ChildIterator_t3520984711;
// System.Xml.XPath.BaseIterator
struct BaseIterator_t3696600956;
// System.Xml.XPath.XPathNodeIterator
struct XPathNodeIterator_t2394191562;
// System.Xml.XPath.XPathNavigator
struct XPathNavigator_t1624538935;

#include "codegen/il2cpp-codegen.h"
#include "System_Xml_System_Xml_XPath_BaseIterator3696600956.h"
#include "System_Xml_System_Xml_XPath_ChildIterator3520984711.h"

// System.Void System.Xml.XPath.ChildIterator::.ctor(System.Xml.XPath.BaseIterator)
extern "C"  void ChildIterator__ctor_m1815034314 (ChildIterator_t3520984711 * __this, BaseIterator_t3696600956 * ___iter0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Xml.XPath.ChildIterator::.ctor(System.Xml.XPath.ChildIterator)
extern "C"  void ChildIterator__ctor_m2771727263 (ChildIterator_t3520984711 * __this, ChildIterator_t3520984711 * ___other0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Xml.XPath.XPathNodeIterator System.Xml.XPath.ChildIterator::Clone()
extern "C"  XPathNodeIterator_t2394191562 * ChildIterator_Clone_m2923315154 (ChildIterator_t3520984711 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Xml.XPath.ChildIterator::MoveNextCore()
extern "C"  bool ChildIterator_MoveNextCore_m3551044493 (ChildIterator_t3520984711 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Xml.XPath.XPathNavigator System.Xml.XPath.ChildIterator::get_Current()
extern "C"  XPathNavigator_t1624538935 * ChildIterator_get_Current_m3213955934 (ChildIterator_t3520984711 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
