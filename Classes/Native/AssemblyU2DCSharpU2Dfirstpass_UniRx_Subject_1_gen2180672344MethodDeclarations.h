﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Subject`1<UniRx.CollectionRemoveEvent`1<System.Object>>
struct Subject_1_t2180672344;
// System.Exception
struct Exception_t1967233988;
// System.IDisposable
struct IDisposable_t1628921374;
// UniRx.IObserver`1<UniRx.CollectionRemoveEvent`1<System.Object>>
struct IObserver_1_t2454636627;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Exception1967233988.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_CollectionRemov242637724.h"

// System.Void UniRx.Subject`1<UniRx.CollectionRemoveEvent`1<System.Object>>::.ctor()
extern "C"  void Subject_1__ctor_m1311604923_gshared (Subject_1_t2180672344 * __this, const MethodInfo* method);
#define Subject_1__ctor_m1311604923(__this, method) ((  void (*) (Subject_1_t2180672344 *, const MethodInfo*))Subject_1__ctor_m1311604923_gshared)(__this, method)
// System.Boolean UniRx.Subject`1<UniRx.CollectionRemoveEvent`1<System.Object>>::get_HasObservers()
extern "C"  bool Subject_1_get_HasObservers_m3784870201_gshared (Subject_1_t2180672344 * __this, const MethodInfo* method);
#define Subject_1_get_HasObservers_m3784870201(__this, method) ((  bool (*) (Subject_1_t2180672344 *, const MethodInfo*))Subject_1_get_HasObservers_m3784870201_gshared)(__this, method)
// System.Void UniRx.Subject`1<UniRx.CollectionRemoveEvent`1<System.Object>>::OnCompleted()
extern "C"  void Subject_1_OnCompleted_m1797651461_gshared (Subject_1_t2180672344 * __this, const MethodInfo* method);
#define Subject_1_OnCompleted_m1797651461(__this, method) ((  void (*) (Subject_1_t2180672344 *, const MethodInfo*))Subject_1_OnCompleted_m1797651461_gshared)(__this, method)
// System.Void UniRx.Subject`1<UniRx.CollectionRemoveEvent`1<System.Object>>::OnError(System.Exception)
extern "C"  void Subject_1_OnError_m2605043122_gshared (Subject_1_t2180672344 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method);
#define Subject_1_OnError_m2605043122(__this, ___error0, method) ((  void (*) (Subject_1_t2180672344 *, Exception_t1967233988 *, const MethodInfo*))Subject_1_OnError_m2605043122_gshared)(__this, ___error0, method)
// System.Void UniRx.Subject`1<UniRx.CollectionRemoveEvent`1<System.Object>>::OnNext(T)
extern "C"  void Subject_1_OnNext_m562649251_gshared (Subject_1_t2180672344 * __this, CollectionRemoveEvent_1_t242637724  ___value0, const MethodInfo* method);
#define Subject_1_OnNext_m562649251(__this, ___value0, method) ((  void (*) (Subject_1_t2180672344 *, CollectionRemoveEvent_1_t242637724 , const MethodInfo*))Subject_1_OnNext_m562649251_gshared)(__this, ___value0, method)
// System.IDisposable UniRx.Subject`1<UniRx.CollectionRemoveEvent`1<System.Object>>::Subscribe(UniRx.IObserver`1<T>)
extern "C"  Il2CppObject * Subject_1_Subscribe_m712821328_gshared (Subject_1_t2180672344 * __this, Il2CppObject* ___observer0, const MethodInfo* method);
#define Subject_1_Subscribe_m712821328(__this, ___observer0, method) ((  Il2CppObject * (*) (Subject_1_t2180672344 *, Il2CppObject*, const MethodInfo*))Subject_1_Subscribe_m712821328_gshared)(__this, ___observer0, method)
// System.Void UniRx.Subject`1<UniRx.CollectionRemoveEvent`1<System.Object>>::Dispose()
extern "C"  void Subject_1_Dispose_m1925196344_gshared (Subject_1_t2180672344 * __this, const MethodInfo* method);
#define Subject_1_Dispose_m1925196344(__this, method) ((  void (*) (Subject_1_t2180672344 *, const MethodInfo*))Subject_1_Dispose_m1925196344_gshared)(__this, method)
// System.Void UniRx.Subject`1<UniRx.CollectionRemoveEvent`1<System.Object>>::ThrowIfDisposed()
extern "C"  void Subject_1_ThrowIfDisposed_m1795955649_gshared (Subject_1_t2180672344 * __this, const MethodInfo* method);
#define Subject_1_ThrowIfDisposed_m1795955649(__this, method) ((  void (*) (Subject_1_t2180672344 *, const MethodInfo*))Subject_1_ThrowIfDisposed_m1795955649_gshared)(__this, method)
// System.Boolean UniRx.Subject`1<UniRx.CollectionRemoveEvent`1<System.Object>>::IsRequiredSubscribeOnCurrentThread()
extern "C"  bool Subject_1_IsRequiredSubscribeOnCurrentThread_m2042919408_gshared (Subject_1_t2180672344 * __this, const MethodInfo* method);
#define Subject_1_IsRequiredSubscribeOnCurrentThread_m2042919408(__this, method) ((  bool (*) (Subject_1_t2180672344 *, const MethodInfo*))Subject_1_IsRequiredSubscribeOnCurrentThread_m2042919408_gshared)(__this, method)
