﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_CollectionAddE2416521987MethodDeclarations.h"

// System.Void UniRx.CollectionAddEvent`1<DonateButton>::.ctor(System.Int32,T)
#define CollectionAddEvent_1__ctor_m2693807223(__this, ___index0, ___value1, method) ((  void (*) (CollectionAddEvent_1_t2250169008 *, int32_t, DonateButton_t670753441 *, const MethodInfo*))CollectionAddEvent_1__ctor_m4121910756_gshared)(__this, ___index0, ___value1, method)
// System.Int32 UniRx.CollectionAddEvent`1<DonateButton>::get_Index()
#define CollectionAddEvent_1_get_Index_m231441951(__this, method) ((  int32_t (*) (CollectionAddEvent_1_t2250169008 *, const MethodInfo*))CollectionAddEvent_1_get_Index_m1649808760_gshared)(__this, method)
// System.Void UniRx.CollectionAddEvent`1<DonateButton>::set_Index(System.Int32)
#define CollectionAddEvent_1_set_Index_m1688572722(__this, ___value0, method) ((  void (*) (CollectionAddEvent_1_t2250169008 *, int32_t, const MethodInfo*))CollectionAddEvent_1_set_Index_m4001500511_gshared)(__this, ___value0, method)
// T UniRx.CollectionAddEvent`1<DonateButton>::get_Value()
#define CollectionAddEvent_1_get_Value_m937777685(__this, method) ((  DonateButton_t670753441 * (*) (CollectionAddEvent_1_t2250169008 *, const MethodInfo*))CollectionAddEvent_1_get_Value_m1882365472_gshared)(__this, method)
// System.Void UniRx.CollectionAddEvent`1<DonateButton>::set_Value(T)
#define CollectionAddEvent_1_set_Value_m1715857694(__this, ___value0, method) ((  void (*) (CollectionAddEvent_1_t2250169008 *, DonateButton_t670753441 *, const MethodInfo*))CollectionAddEvent_1_set_Value_m1389365521_gshared)(__this, ___value0, method)
// System.String UniRx.CollectionAddEvent`1<DonateButton>::ToString()
#define CollectionAddEvent_1_ToString_m551668165(__this, method) ((  String_t* (*) (CollectionAddEvent_1_t2250169008 *, const MethodInfo*))CollectionAddEvent_1_ToString_m3034630034_gshared)(__this, method)
// System.Int32 UniRx.CollectionAddEvent`1<DonateButton>::GetHashCode()
#define CollectionAddEvent_1_GetHashCode_m2366261703(__this, method) ((  int32_t (*) (CollectionAddEvent_1_t2250169008 *, const MethodInfo*))CollectionAddEvent_1_GetHashCode_m3912132320_gshared)(__this, method)
// System.Boolean UniRx.CollectionAddEvent`1<DonateButton>::Equals(UniRx.CollectionAddEvent`1<T>)
#define CollectionAddEvent_1_Equals_m3816593659(__this, ___other0, method) ((  bool (*) (CollectionAddEvent_1_t2250169008 *, CollectionAddEvent_1_t2250169008 , const MethodInfo*))CollectionAddEvent_1_Equals_m2095755040_gshared)(__this, ___other0, method)
