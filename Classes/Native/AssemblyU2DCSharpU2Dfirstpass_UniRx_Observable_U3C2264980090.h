﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.YieldInstruction
struct YieldInstruction_t3557331758;
struct YieldInstruction_t3557331758_marshaled_pinvoke;
// UniRx.IObserver`1<System.Int64>
struct IObserver_1_t764446489;
// System.Object
struct Il2CppObject;

#include "mscorlib_System_Object837106420.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_FrameCountType167759182.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_CancellationTo1439151560.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Observable/<TimerFrameCore>c__Iterator15
struct  U3CTimerFrameCoreU3Ec__Iterator15_t2264980090  : public Il2CppObject
{
public:
	// System.Int32 UniRx.Observable/<TimerFrameCore>c__Iterator15::dueTimeFrameCount
	int32_t ___dueTimeFrameCount_0;
	// System.Int32 UniRx.Observable/<TimerFrameCore>c__Iterator15::<currentFrame>__0
	int32_t ___U3CcurrentFrameU3E__0_1;
	// UniRx.FrameCountType UniRx.Observable/<TimerFrameCore>c__Iterator15::frameCountType
	int32_t ___frameCountType_2;
	// UnityEngine.YieldInstruction UniRx.Observable/<TimerFrameCore>c__Iterator15::<yieldInstruction>__1
	YieldInstruction_t3557331758 * ___U3CyieldInstructionU3E__1_3;
	// UniRx.CancellationToken UniRx.Observable/<TimerFrameCore>c__Iterator15::cancel
	CancellationToken_t1439151560  ___cancel_4;
	// UniRx.IObserver`1<System.Int64> UniRx.Observable/<TimerFrameCore>c__Iterator15::observer
	Il2CppObject* ___observer_5;
	// System.Int32 UniRx.Observable/<TimerFrameCore>c__Iterator15::$PC
	int32_t ___U24PC_6;
	// System.Object UniRx.Observable/<TimerFrameCore>c__Iterator15::$current
	Il2CppObject * ___U24current_7;
	// System.Int32 UniRx.Observable/<TimerFrameCore>c__Iterator15::<$>dueTimeFrameCount
	int32_t ___U3CU24U3EdueTimeFrameCount_8;
	// UniRx.FrameCountType UniRx.Observable/<TimerFrameCore>c__Iterator15::<$>frameCountType
	int32_t ___U3CU24U3EframeCountType_9;
	// UniRx.CancellationToken UniRx.Observable/<TimerFrameCore>c__Iterator15::<$>cancel
	CancellationToken_t1439151560  ___U3CU24U3Ecancel_10;
	// UniRx.IObserver`1<System.Int64> UniRx.Observable/<TimerFrameCore>c__Iterator15::<$>observer
	Il2CppObject* ___U3CU24U3Eobserver_11;

public:
	inline static int32_t get_offset_of_dueTimeFrameCount_0() { return static_cast<int32_t>(offsetof(U3CTimerFrameCoreU3Ec__Iterator15_t2264980090, ___dueTimeFrameCount_0)); }
	inline int32_t get_dueTimeFrameCount_0() const { return ___dueTimeFrameCount_0; }
	inline int32_t* get_address_of_dueTimeFrameCount_0() { return &___dueTimeFrameCount_0; }
	inline void set_dueTimeFrameCount_0(int32_t value)
	{
		___dueTimeFrameCount_0 = value;
	}

	inline static int32_t get_offset_of_U3CcurrentFrameU3E__0_1() { return static_cast<int32_t>(offsetof(U3CTimerFrameCoreU3Ec__Iterator15_t2264980090, ___U3CcurrentFrameU3E__0_1)); }
	inline int32_t get_U3CcurrentFrameU3E__0_1() const { return ___U3CcurrentFrameU3E__0_1; }
	inline int32_t* get_address_of_U3CcurrentFrameU3E__0_1() { return &___U3CcurrentFrameU3E__0_1; }
	inline void set_U3CcurrentFrameU3E__0_1(int32_t value)
	{
		___U3CcurrentFrameU3E__0_1 = value;
	}

	inline static int32_t get_offset_of_frameCountType_2() { return static_cast<int32_t>(offsetof(U3CTimerFrameCoreU3Ec__Iterator15_t2264980090, ___frameCountType_2)); }
	inline int32_t get_frameCountType_2() const { return ___frameCountType_2; }
	inline int32_t* get_address_of_frameCountType_2() { return &___frameCountType_2; }
	inline void set_frameCountType_2(int32_t value)
	{
		___frameCountType_2 = value;
	}

	inline static int32_t get_offset_of_U3CyieldInstructionU3E__1_3() { return static_cast<int32_t>(offsetof(U3CTimerFrameCoreU3Ec__Iterator15_t2264980090, ___U3CyieldInstructionU3E__1_3)); }
	inline YieldInstruction_t3557331758 * get_U3CyieldInstructionU3E__1_3() const { return ___U3CyieldInstructionU3E__1_3; }
	inline YieldInstruction_t3557331758 ** get_address_of_U3CyieldInstructionU3E__1_3() { return &___U3CyieldInstructionU3E__1_3; }
	inline void set_U3CyieldInstructionU3E__1_3(YieldInstruction_t3557331758 * value)
	{
		___U3CyieldInstructionU3E__1_3 = value;
		Il2CppCodeGenWriteBarrier(&___U3CyieldInstructionU3E__1_3, value);
	}

	inline static int32_t get_offset_of_cancel_4() { return static_cast<int32_t>(offsetof(U3CTimerFrameCoreU3Ec__Iterator15_t2264980090, ___cancel_4)); }
	inline CancellationToken_t1439151560  get_cancel_4() const { return ___cancel_4; }
	inline CancellationToken_t1439151560 * get_address_of_cancel_4() { return &___cancel_4; }
	inline void set_cancel_4(CancellationToken_t1439151560  value)
	{
		___cancel_4 = value;
	}

	inline static int32_t get_offset_of_observer_5() { return static_cast<int32_t>(offsetof(U3CTimerFrameCoreU3Ec__Iterator15_t2264980090, ___observer_5)); }
	inline Il2CppObject* get_observer_5() const { return ___observer_5; }
	inline Il2CppObject** get_address_of_observer_5() { return &___observer_5; }
	inline void set_observer_5(Il2CppObject* value)
	{
		___observer_5 = value;
		Il2CppCodeGenWriteBarrier(&___observer_5, value);
	}

	inline static int32_t get_offset_of_U24PC_6() { return static_cast<int32_t>(offsetof(U3CTimerFrameCoreU3Ec__Iterator15_t2264980090, ___U24PC_6)); }
	inline int32_t get_U24PC_6() const { return ___U24PC_6; }
	inline int32_t* get_address_of_U24PC_6() { return &___U24PC_6; }
	inline void set_U24PC_6(int32_t value)
	{
		___U24PC_6 = value;
	}

	inline static int32_t get_offset_of_U24current_7() { return static_cast<int32_t>(offsetof(U3CTimerFrameCoreU3Ec__Iterator15_t2264980090, ___U24current_7)); }
	inline Il2CppObject * get_U24current_7() const { return ___U24current_7; }
	inline Il2CppObject ** get_address_of_U24current_7() { return &___U24current_7; }
	inline void set_U24current_7(Il2CppObject * value)
	{
		___U24current_7 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_7, value);
	}

	inline static int32_t get_offset_of_U3CU24U3EdueTimeFrameCount_8() { return static_cast<int32_t>(offsetof(U3CTimerFrameCoreU3Ec__Iterator15_t2264980090, ___U3CU24U3EdueTimeFrameCount_8)); }
	inline int32_t get_U3CU24U3EdueTimeFrameCount_8() const { return ___U3CU24U3EdueTimeFrameCount_8; }
	inline int32_t* get_address_of_U3CU24U3EdueTimeFrameCount_8() { return &___U3CU24U3EdueTimeFrameCount_8; }
	inline void set_U3CU24U3EdueTimeFrameCount_8(int32_t value)
	{
		___U3CU24U3EdueTimeFrameCount_8 = value;
	}

	inline static int32_t get_offset_of_U3CU24U3EframeCountType_9() { return static_cast<int32_t>(offsetof(U3CTimerFrameCoreU3Ec__Iterator15_t2264980090, ___U3CU24U3EframeCountType_9)); }
	inline int32_t get_U3CU24U3EframeCountType_9() const { return ___U3CU24U3EframeCountType_9; }
	inline int32_t* get_address_of_U3CU24U3EframeCountType_9() { return &___U3CU24U3EframeCountType_9; }
	inline void set_U3CU24U3EframeCountType_9(int32_t value)
	{
		___U3CU24U3EframeCountType_9 = value;
	}

	inline static int32_t get_offset_of_U3CU24U3Ecancel_10() { return static_cast<int32_t>(offsetof(U3CTimerFrameCoreU3Ec__Iterator15_t2264980090, ___U3CU24U3Ecancel_10)); }
	inline CancellationToken_t1439151560  get_U3CU24U3Ecancel_10() const { return ___U3CU24U3Ecancel_10; }
	inline CancellationToken_t1439151560 * get_address_of_U3CU24U3Ecancel_10() { return &___U3CU24U3Ecancel_10; }
	inline void set_U3CU24U3Ecancel_10(CancellationToken_t1439151560  value)
	{
		___U3CU24U3Ecancel_10 = value;
	}

	inline static int32_t get_offset_of_U3CU24U3Eobserver_11() { return static_cast<int32_t>(offsetof(U3CTimerFrameCoreU3Ec__Iterator15_t2264980090, ___U3CU24U3Eobserver_11)); }
	inline Il2CppObject* get_U3CU24U3Eobserver_11() const { return ___U3CU24U3Eobserver_11; }
	inline Il2CppObject** get_address_of_U3CU24U3Eobserver_11() { return &___U3CU24U3Eobserver_11; }
	inline void set_U3CU24U3Eobserver_11(Il2CppObject* value)
	{
		___U3CU24U3Eobserver_11 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU24U3Eobserver_11, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
