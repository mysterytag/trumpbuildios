﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Zenject_TaskUpdater_1_gen1705995667MethodDeclarations.h"

// System.Void Zenject.TaskUpdater`1<Zenject.IFixedTickable>::.ctor(System.Action`1<TTask>)
#define TaskUpdater_1__ctor_m2133350137(__this, ___updateFunc0, method) ((  void (*) (TaskUpdater_1_t4197280854 *, Action_1_t3476844312 *, const MethodInfo*))TaskUpdater_1__ctor_m1842795290_gshared)(__this, ___updateFunc0, method)
// System.Collections.Generic.IEnumerable`1<Zenject.TaskUpdater`1/TaskInfo<TTask>> Zenject.TaskUpdater`1<Zenject.IFixedTickable>::get_AllTasks()
#define TaskUpdater_1_get_AllTasks_m1291996805(__this, method) ((  Il2CppObject* (*) (TaskUpdater_1_t4197280854 *, const MethodInfo*))TaskUpdater_1_get_AllTasks_m2200620774_gshared)(__this, method)
// System.Collections.Generic.IEnumerable`1<Zenject.TaskUpdater`1/TaskInfo<TTask>> Zenject.TaskUpdater`1<Zenject.IFixedTickable>::get_ActiveTasks()
#define TaskUpdater_1_get_ActiveTasks_m2877498738(__this, method) ((  Il2CppObject* (*) (TaskUpdater_1_t4197280854 *, const MethodInfo*))TaskUpdater_1_get_ActiveTasks_m515292529_gshared)(__this, method)
// System.Void Zenject.TaskUpdater`1<Zenject.IFixedTickable>::AddTask(TTask,System.Int32)
#define TaskUpdater_1_AddTask_m1413840662(__this, ___task0, ___priority1, method) ((  void (*) (TaskUpdater_1_t4197280854 *, Il2CppObject *, int32_t, const MethodInfo*))TaskUpdater_1_AddTask_m720801655_gshared)(__this, ___task0, ___priority1, method)
// System.Void Zenject.TaskUpdater`1<Zenject.IFixedTickable>::AddTaskInternal(TTask,System.Int32)
#define TaskUpdater_1_AddTaskInternal_m1480454771(__this, ___task0, ___priority1, method) ((  void (*) (TaskUpdater_1_t4197280854 *, Il2CppObject *, int32_t, const MethodInfo*))TaskUpdater_1_AddTaskInternal_m1657606612_gshared)(__this, ___task0, ___priority1, method)
// System.Void Zenject.TaskUpdater`1<Zenject.IFixedTickable>::RemoveTask(TTask)
#define TaskUpdater_1_RemoveTask_m3152719968(__this, ___task0, method) ((  void (*) (TaskUpdater_1_t4197280854 *, Il2CppObject *, const MethodInfo*))TaskUpdater_1_RemoveTask_m1986106881_gshared)(__this, ___task0, method)
// System.Void Zenject.TaskUpdater`1<Zenject.IFixedTickable>::OnFrameStart()
#define TaskUpdater_1_OnFrameStart_m1383007396(__this, method) ((  void (*) (TaskUpdater_1_t4197280854 *, const MethodInfo*))TaskUpdater_1_OnFrameStart_m1489365667_gshared)(__this, method)
// System.Void Zenject.TaskUpdater`1<Zenject.IFixedTickable>::UpdateAll()
#define TaskUpdater_1_UpdateAll_m2750832394(__this, method) ((  void (*) (TaskUpdater_1_t4197280854 *, const MethodInfo*))TaskUpdater_1_UpdateAll_m611209579_gshared)(__this, method)
// System.Void Zenject.TaskUpdater`1<Zenject.IFixedTickable>::UpdateRange(System.Int32,System.Int32)
#define TaskUpdater_1_UpdateRange_m2068692032(__this, ___minPriority0, ___maxPriority1, method) ((  void (*) (TaskUpdater_1_t4197280854 *, int32_t, int32_t, const MethodInfo*))TaskUpdater_1_UpdateRange_m1084380479_gshared)(__this, ___minPriority0, ___maxPriority1, method)
// System.Void Zenject.TaskUpdater`1<Zenject.IFixedTickable>::ClearRemovedTasks(System.Collections.Generic.LinkedList`1<Zenject.TaskUpdater`1/TaskInfo<TTask>>)
#define TaskUpdater_1_ClearRemovedTasks_m4151630687(__this, ___tasks0, method) ((  void (*) (TaskUpdater_1_t4197280854 *, LinkedList_1_t1000955841 *, const MethodInfo*))TaskUpdater_1_ClearRemovedTasks_m204003072_gshared)(__this, ___tasks0, method)
// System.Void Zenject.TaskUpdater`1<Zenject.IFixedTickable>::AddQueuedTasks()
#define TaskUpdater_1_AddQueuedTasks_m2897962922(__this, method) ((  void (*) (TaskUpdater_1_t4197280854 *, const MethodInfo*))TaskUpdater_1_AddQueuedTasks_m2029046249_gshared)(__this, method)
// System.Void Zenject.TaskUpdater`1<Zenject.IFixedTickable>::InsertTaskSorted(Zenject.TaskUpdater`1/TaskInfo<TTask>)
#define TaskUpdater_1_InsertTaskSorted_m1689222418(__this, ___task0, method) ((  void (*) (TaskUpdater_1_t4197280854 *, TaskInfo_t3555793591 *, const MethodInfo*))TaskUpdater_1_InsertTaskSorted_m3714982387_gshared)(__this, ___task0, method)
// TTask Zenject.TaskUpdater`1<Zenject.IFixedTickable>::<AddTaskInternal>m__2E5(Zenject.TaskUpdater`1/TaskInfo<TTask>)
#define TaskUpdater_1_U3CAddTaskInternalU3Em__2E5_m1742162981(__this /* static, unused */, ___x0, method) ((  Il2CppObject * (*) (Il2CppObject * /* static, unused */, TaskInfo_t3555793591 *, const MethodInfo*))TaskUpdater_1_U3CAddTaskInternalU3Em__2E5_m2398513880_gshared)(__this /* static, unused */, ___x0, method)
