﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Zenject.ZenjectTypeInfo
struct ZenjectTypeInfo_t283213708;
// System.Type
struct Type_t;
// System.Collections.Generic.List`1<Zenject.PostInjectableInfo>
struct List_1_t3877242631;
// System.Reflection.ConstructorInfo
struct ConstructorInfo_t3542137334;
// System.Collections.Generic.List`1<Zenject.InjectableInfo>
struct List_1_t1944668743;
// System.Collections.Generic.IEnumerable`1<Zenject.PostInjectableInfo>
struct IEnumerable_1_t1657470722;
// System.Collections.Generic.IEnumerable`1<Zenject.InjectableInfo>
struct IEnumerable_1_t4019864130;
// Zenject.PostInjectableInfo
struct PostInjectableInfo_t3080283662;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Type2779229935.h"
#include "mscorlib_System_Reflection_ConstructorInfo3542137334.h"
#include "AssemblyU2DCSharp_Zenject_PostInjectableInfo3080283662.h"

// System.Void Zenject.ZenjectTypeInfo::.ctor(System.Type,System.Collections.Generic.List`1<Zenject.PostInjectableInfo>,System.Reflection.ConstructorInfo,System.Collections.Generic.List`1<Zenject.InjectableInfo>,System.Collections.Generic.List`1<Zenject.InjectableInfo>,System.Collections.Generic.List`1<Zenject.InjectableInfo>)
extern "C"  void ZenjectTypeInfo__ctor_m4156201214 (ZenjectTypeInfo_t283213708 * __this, Type_t * ___typeAnalyzed0, List_1_t3877242631 * ___postInjectMethods1, ConstructorInfo_t3542137334 * ___injectConstructor2, List_1_t1944668743 * ___fieldInjectables3, List_1_t1944668743 * ___propertyInjectables4, List_1_t1944668743 * ___constructorInjectables5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type Zenject.ZenjectTypeInfo::get_TypeAnalyzed()
extern "C"  Type_t * ZenjectTypeInfo_get_TypeAnalyzed_m89817778 (ZenjectTypeInfo_t283213708 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.IEnumerable`1<Zenject.PostInjectableInfo> Zenject.ZenjectTypeInfo::get_PostInjectMethods()
extern "C"  Il2CppObject* ZenjectTypeInfo_get_PostInjectMethods_m2187525833 (ZenjectTypeInfo_t283213708 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.IEnumerable`1<Zenject.InjectableInfo> Zenject.ZenjectTypeInfo::get_AllInjectables()
extern "C"  Il2CppObject* ZenjectTypeInfo_get_AllInjectables_m3519834113 (ZenjectTypeInfo_t283213708 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.IEnumerable`1<Zenject.InjectableInfo> Zenject.ZenjectTypeInfo::get_FieldInjectables()
extern "C"  Il2CppObject* ZenjectTypeInfo_get_FieldInjectables_m870985480 (ZenjectTypeInfo_t283213708 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.IEnumerable`1<Zenject.InjectableInfo> Zenject.ZenjectTypeInfo::get_PropertyInjectables()
extern "C"  Il2CppObject* ZenjectTypeInfo_get_PropertyInjectables_m202125435 (ZenjectTypeInfo_t283213708 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.IEnumerable`1<Zenject.InjectableInfo> Zenject.ZenjectTypeInfo::get_ConstructorInjectables()
extern "C"  Il2CppObject* ZenjectTypeInfo_get_ConstructorInjectables_m3437893960 (ZenjectTypeInfo_t283213708 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.ConstructorInfo Zenject.ZenjectTypeInfo::get_InjectConstructor()
extern "C"  ConstructorInfo_t3542137334 * ZenjectTypeInfo_get_InjectConstructor_m103263552 (ZenjectTypeInfo_t283213708 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.IEnumerable`1<Zenject.InjectableInfo> Zenject.ZenjectTypeInfo::<get_AllInjectables>m__2B9(Zenject.PostInjectableInfo)
extern "C"  Il2CppObject* ZenjectTypeInfo_U3Cget_AllInjectablesU3Em__2B9_m2318910823 (Il2CppObject * __this /* static, unused */, PostInjectableInfo_t3080283662 * ___x0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
