﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// BarygaVkController/<ReportAchievement>c__AnonStoreyDE
struct U3CReportAchievementU3Ec__AnonStoreyDE_t1572853449;

#include "codegen/il2cpp-codegen.h"

// System.Void BarygaVkController/<ReportAchievement>c__AnonStoreyDE::.ctor()
extern "C"  void U3CReportAchievementU3Ec__AnonStoreyDE__ctor_m971776580 (U3CReportAchievementU3Ec__AnonStoreyDE_t1572853449 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BarygaVkController/<ReportAchievement>c__AnonStoreyDE::<>m__F3(System.Boolean)
extern "C"  void U3CReportAchievementU3Ec__AnonStoreyDE_U3CU3Em__F3_m990426481 (U3CReportAchievementU3Ec__AnonStoreyDE_t1572853449 * __this, bool ____0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
