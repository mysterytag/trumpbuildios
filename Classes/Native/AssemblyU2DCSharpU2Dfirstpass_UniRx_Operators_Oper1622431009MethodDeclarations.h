﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.OperatorObservableBase`1<UniRx.Unit>
struct OperatorObservableBase_1_t1622431009;
// System.IDisposable
struct IDisposable_t1628921374;
// UniRx.IObserver`1<UniRx.Unit>
struct IObserver_1_t475317645;

#include "codegen/il2cpp-codegen.h"

// System.Void UniRx.Operators.OperatorObservableBase`1<UniRx.Unit>::.ctor(System.Boolean)
extern "C"  void OperatorObservableBase_1__ctor_m284357152_gshared (OperatorObservableBase_1_t1622431009 * __this, bool ___isRequiredSubscribeOnCurrentThread0, const MethodInfo* method);
#define OperatorObservableBase_1__ctor_m284357152(__this, ___isRequiredSubscribeOnCurrentThread0, method) ((  void (*) (OperatorObservableBase_1_t1622431009 *, bool, const MethodInfo*))OperatorObservableBase_1__ctor_m284357152_gshared)(__this, ___isRequiredSubscribeOnCurrentThread0, method)
// System.Boolean UniRx.Operators.OperatorObservableBase`1<UniRx.Unit>::IsRequiredSubscribeOnCurrentThread()
extern "C"  bool OperatorObservableBase_1_IsRequiredSubscribeOnCurrentThread_m1449971386_gshared (OperatorObservableBase_1_t1622431009 * __this, const MethodInfo* method);
#define OperatorObservableBase_1_IsRequiredSubscribeOnCurrentThread_m1449971386(__this, method) ((  bool (*) (OperatorObservableBase_1_t1622431009 *, const MethodInfo*))OperatorObservableBase_1_IsRequiredSubscribeOnCurrentThread_m1449971386_gshared)(__this, method)
// System.IDisposable UniRx.Operators.OperatorObservableBase`1<UniRx.Unit>::Subscribe(UniRx.IObserver`1<T>)
extern "C"  Il2CppObject * OperatorObservableBase_1_Subscribe_m469429672_gshared (OperatorObservableBase_1_t1622431009 * __this, Il2CppObject* ___observer0, const MethodInfo* method);
#define OperatorObservableBase_1_Subscribe_m469429672(__this, ___observer0, method) ((  Il2CppObject * (*) (OperatorObservableBase_1_t1622431009 *, Il2CppObject*, const MethodInfo*))OperatorObservableBase_1_Subscribe_m469429672_gshared)(__this, ___observer0, method)
