﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "System_System_Collections_Generic_Queue_1_gen2545193960MethodDeclarations.h"

// System.Void System.Collections.Generic.Queue`1<System.Collections.Generic.IList`1<System.Object>>::.ctor()
#define Queue_1__ctor_m1975779254(__this, method) ((  void (*) (Queue_1_t416718978 *, const MethodInfo*))Queue_1__ctor_m3042804833_gshared)(__this, method)
// System.Void System.Collections.Generic.Queue`1<System.Collections.Generic.IList`1<System.Object>>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define Queue_1_System_Collections_ICollection_CopyTo_m3984617038(__this, ___array0, ___idx1, method) ((  void (*) (Queue_1_t416718978 *, Il2CppArray *, int32_t, const MethodInfo*))Queue_1_System_Collections_ICollection_CopyTo_m3260144643_gshared)(__this, ___array0, ___idx1, method)
// System.Boolean System.Collections.Generic.Queue`1<System.Collections.Generic.IList`1<System.Object>>::System.Collections.ICollection.get_IsSynchronized()
#define Queue_1_System_Collections_ICollection_get_IsSynchronized_m3880389040(__this, method) ((  bool (*) (Queue_1_t416718978 *, const MethodInfo*))Queue_1_System_Collections_ICollection_get_IsSynchronized_m63917275_gshared)(__this, method)
// System.Object System.Collections.Generic.Queue`1<System.Collections.Generic.IList`1<System.Object>>::System.Collections.ICollection.get_SyncRoot()
#define Queue_1_System_Collections_ICollection_get_SyncRoot_m68082254(__this, method) ((  Il2CppObject * (*) (Queue_1_t416718978 *, const MethodInfo*))Queue_1_System_Collections_ICollection_get_SyncRoot_m2093948217_gshared)(__this, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.Queue`1<System.Collections.Generic.IList`1<System.Object>>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
#define Queue_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m3558786880(__this, method) ((  Il2CppObject* (*) (Queue_1_t416718978 *, const MethodInfo*))Queue_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m472615211_gshared)(__this, method)
// System.Collections.IEnumerator System.Collections.Generic.Queue`1<System.Collections.Generic.IList`1<System.Object>>::System.Collections.IEnumerable.GetEnumerator()
#define Queue_1_System_Collections_IEnumerable_GetEnumerator_m2808518217(__this, method) ((  Il2CppObject * (*) (Queue_1_t416718978 *, const MethodInfo*))Queue_1_System_Collections_IEnumerable_GetEnumerator_m3688614462_gshared)(__this, method)
// System.Void System.Collections.Generic.Queue`1<System.Collections.Generic.IList`1<System.Object>>::Clear()
#define Queue_1_Clear_m3676879841(__this, method) ((  void (*) (Queue_1_t416718978 *, const MethodInfo*))Queue_1_Clear_m448938124_gshared)(__this, method)
// System.Void System.Collections.Generic.Queue`1<System.Collections.Generic.IList`1<System.Object>>::CopyTo(T[],System.Int32)
#define Queue_1_CopyTo_m2324957625(__this, ___array0, ___idx1, method) ((  void (*) (Queue_1_t416718978 *, IList_1U5BU5D_t724439675*, int32_t, const MethodInfo*))Queue_1_CopyTo_m3592753262_gshared)(__this, ___array0, ___idx1, method)
// T System.Collections.Generic.Queue`1<System.Collections.Generic.IList`1<System.Object>>::Dequeue()
#define Queue_1_Dequeue_m3787331459(__this, method) ((  Il2CppObject* (*) (Queue_1_t416718978 *, const MethodInfo*))Queue_1_Dequeue_m102813934_gshared)(__this, method)
// T System.Collections.Generic.Queue`1<System.Collections.Generic.IList`1<System.Object>>::Peek()
#define Queue_1_Peek_m3217912714(__this, method) ((  Il2CppObject* (*) (Queue_1_t416718978 *, const MethodInfo*))Queue_1_Peek_m3013356031_gshared)(__this, method)
// System.Void System.Collections.Generic.Queue`1<System.Collections.Generic.IList`1<System.Object>>::Enqueue(T)
#define Queue_1_Enqueue_m3373277378(__this, ___item0, method) ((  void (*) (Queue_1_t416718978 *, Il2CppObject*, const MethodInfo*))Queue_1_Enqueue_m4079343671_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.Queue`1<System.Collections.Generic.IList`1<System.Object>>::SetCapacity(System.Int32)
#define Queue_1_SetCapacity_m926731297(__this, ___new_size0, method) ((  void (*) (Queue_1_t416718978 *, int32_t, const MethodInfo*))Queue_1_SetCapacity_m1573690380_gshared)(__this, ___new_size0, method)
// System.Int32 System.Collections.Generic.Queue`1<System.Collections.Generic.IList`1<System.Object>>::get_Count()
#define Queue_1_get_Count_m21612778(__this, method) ((  int32_t (*) (Queue_1_t416718978 *, const MethodInfo*))Queue_1_get_Count_m1429559317_gshared)(__this, method)
// System.Collections.Generic.Queue`1/Enumerator<T> System.Collections.Generic.Queue`1<System.Collections.Generic.IList`1<System.Object>>::GetEnumerator()
#define Queue_1_GetEnumerator_m334778503(__this, method) ((  Enumerator_t1886340695  (*) (Queue_1_t416718978 *, const MethodInfo*))Queue_1_GetEnumerator_m3965043378_gshared)(__this, method)
