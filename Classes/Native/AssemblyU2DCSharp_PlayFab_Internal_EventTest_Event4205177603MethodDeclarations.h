﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.Internal.EventTest/EventInstanceListener
struct EventInstanceListener_t4205177603;
// System.String
struct String_t;
// System.Object
struct Il2CppObject;
// PlayFab.PlayFabError
struct PlayFabError_t750598646;
// PlayFab.ClientModels.LoginWithCustomIDRequest
struct LoginWithCustomIDRequest_t3362330884;
// PlayFab.ClientModels.LoginResult
struct LoginResult_t2117559510;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String968488902.h"
#include "mscorlib_System_Object837106420.h"
#include "AssemblyU2DCSharpU2Dfirstpass_PlayFab_PlayFabError750598646.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_LoginWithCu3362330884.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_LoginResult2117559510.h"

// System.Void PlayFab.Internal.EventTest/EventInstanceListener::.ctor()
extern "C"  void EventInstanceListener__ctor_m1066335505 (EventInstanceListener_t4205177603 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.Internal.EventTest/EventInstanceListener::Register()
extern "C"  void EventInstanceListener_Register_m1707377398 (EventInstanceListener_t4205177603 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.Internal.EventTest/EventInstanceListener::Unregister()
extern "C"  void EventInstanceListener_Unregister_m2072979919 (EventInstanceListener_t4205177603 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.Internal.EventTest/EventInstanceListener::OnRequest_InstGl(System.String,System.Int32,System.Object,System.Object)
extern "C"  void EventInstanceListener_OnRequest_InstGl_m3936298718 (EventInstanceListener_t4205177603 * __this, String_t* ___url0, int32_t ___callId1, Il2CppObject * ___request2, Il2CppObject * ___customData3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.Internal.EventTest/EventInstanceListener::OnResponse_InstGl(System.String,System.Int32,System.Object,System.Object,PlayFab.PlayFabError,System.Object)
extern "C"  void EventInstanceListener_OnResponse_InstGl_m403445242 (EventInstanceListener_t4205177603 * __this, String_t* ___url0, int32_t ___callId1, Il2CppObject * ___request2, Il2CppObject * ___result3, PlayFabError_t750598646 * ___error4, Il2CppObject * ___customData5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.Internal.EventTest/EventInstanceListener::OnRequest_InstLogin(System.String,System.Int32,PlayFab.ClientModels.LoginWithCustomIDRequest,System.Object)
extern "C"  void EventInstanceListener_OnRequest_InstLogin_m560511328 (EventInstanceListener_t4205177603 * __this, String_t* ___url0, int32_t ___callId1, LoginWithCustomIDRequest_t3362330884 * ___request2, Il2CppObject * ___customData3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.Internal.EventTest/EventInstanceListener::OnResponse_InstLogin(System.String,System.Int32,PlayFab.ClientModels.LoginWithCustomIDRequest,PlayFab.ClientModels.LoginResult,PlayFab.PlayFabError,System.Object)
extern "C"  void EventInstanceListener_OnResponse_InstLogin_m2058615404 (EventInstanceListener_t4205177603 * __this, String_t* ___url0, int32_t ___callId1, LoginWithCustomIDRequest_t3362330884 * ___request2, LoginResult_t2117559510 * ___result3, PlayFabError_t750598646 * ___error4, Il2CppObject * ___customData5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.Internal.EventTest/EventInstanceListener::<Register>m__4A(PlayFab.PlayFabError)
extern "C"  void EventInstanceListener_U3CRegisterU3Em__4A_m75157114 (Il2CppObject * __this /* static, unused */, PlayFabError_t750598646 * ___error0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
