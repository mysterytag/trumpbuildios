﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.ClientModels.PurchaseItemRequest
struct PurchaseItemRequest_t3673358379;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String968488902.h"

// System.Void PlayFab.ClientModels.PurchaseItemRequest::.ctor()
extern "C"  void PurchaseItemRequest__ctor_m4181081310 (PurchaseItemRequest_t3673358379 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PlayFab.ClientModels.PurchaseItemRequest::get_ItemId()
extern "C"  String_t* PurchaseItemRequest_get_ItemId_m2626691488 (PurchaseItemRequest_t3673358379 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.PurchaseItemRequest::set_ItemId(System.String)
extern "C"  void PurchaseItemRequest_set_ItemId_m2705274481 (PurchaseItemRequest_t3673358379 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PlayFab.ClientModels.PurchaseItemRequest::get_VirtualCurrency()
extern "C"  String_t* PurchaseItemRequest_get_VirtualCurrency_m1506473164 (PurchaseItemRequest_t3673358379 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.PurchaseItemRequest::set_VirtualCurrency(System.String)
extern "C"  void PurchaseItemRequest_set_VirtualCurrency_m1556283783 (PurchaseItemRequest_t3673358379 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 PlayFab.ClientModels.PurchaseItemRequest::get_Price()
extern "C"  int32_t PurchaseItemRequest_get_Price_m83012174 (PurchaseItemRequest_t3673358379 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.PurchaseItemRequest::set_Price(System.Int32)
extern "C"  void PurchaseItemRequest_set_Price_m302381561 (PurchaseItemRequest_t3673358379 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PlayFab.ClientModels.PurchaseItemRequest::get_CatalogVersion()
extern "C"  String_t* PurchaseItemRequest_get_CatalogVersion_m2560144209 (PurchaseItemRequest_t3673358379 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.PurchaseItemRequest::set_CatalogVersion(System.String)
extern "C"  void PurchaseItemRequest_set_CatalogVersion_m2531274080 (PurchaseItemRequest_t3673358379 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PlayFab.ClientModels.PurchaseItemRequest::get_StoreId()
extern "C"  String_t* PurchaseItemRequest_get_StoreId_m3676709228 (PurchaseItemRequest_t3673358379 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.PurchaseItemRequest::set_StoreId(System.String)
extern "C"  void PurchaseItemRequest_set_StoreId_m1744484839 (PurchaseItemRequest_t3673358379 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PlayFab.ClientModels.PurchaseItemRequest::get_CharacterId()
extern "C"  String_t* PurchaseItemRequest_get_CharacterId_m1015619636 (PurchaseItemRequest_t3673358379 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.PurchaseItemRequest::set_CharacterId(System.String)
extern "C"  void PurchaseItemRequest_set_CharacterId_m1636118431 (PurchaseItemRequest_t3673358379 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
