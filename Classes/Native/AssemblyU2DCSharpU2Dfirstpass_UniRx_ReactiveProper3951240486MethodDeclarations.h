﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_ReactiveProper1445439458MethodDeclarations.h"

// System.Void UniRx.ReactiveProperty`1<UnityEngine.AnimationCurve>::.ctor()
#define ReactiveProperty_1__ctor_m2070470040(__this, method) ((  void (*) (ReactiveProperty_1_t3951240486 *, const MethodInfo*))ReactiveProperty_1__ctor_m2182085490_gshared)(__this, method)
// System.Void UniRx.ReactiveProperty`1<UnityEngine.AnimationCurve>::.ctor(T)
#define ReactiveProperty_1__ctor_m4055030470(__this, ___initialValue0, method) ((  void (*) (ReactiveProperty_1_t3951240486 *, AnimationCurve_t3342907448 *, const MethodInfo*))ReactiveProperty_1__ctor_m3220142124_gshared)(__this, ___initialValue0, method)
// System.Void UniRx.ReactiveProperty`1<UnityEngine.AnimationCurve>::.ctor(UniRx.IObservable`1<T>)
#define ReactiveProperty_1__ctor_m1284702583(__this, ___source0, method) ((  void (*) (ReactiveProperty_1_t3951240486 *, Il2CppObject*, const MethodInfo*))ReactiveProperty_1__ctor_m2559762385_gshared)(__this, ___source0, method)
// System.Void UniRx.ReactiveProperty`1<UnityEngine.AnimationCurve>::.ctor(UniRx.IObservable`1<T>,T)
#define ReactiveProperty_1__ctor_m1943573839(__this, ___source0, ___initialValue1, method) ((  void (*) (ReactiveProperty_1_t3951240486 *, Il2CppObject*, AnimationCurve_t3342907448 *, const MethodInfo*))ReactiveProperty_1__ctor_m3210364201_gshared)(__this, ___source0, ___initialValue1, method)
// System.Void UniRx.ReactiveProperty`1<UnityEngine.AnimationCurve>::.cctor()
#define ReactiveProperty_1__cctor_m3572932885(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))ReactiveProperty_1__cctor_m2738044539_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.IEqualityComparer`1<T> UniRx.ReactiveProperty`1<UnityEngine.AnimationCurve>::get_EqualityComparer()
#define ReactiveProperty_1_get_EqualityComparer_m1974904027(__this, method) ((  Il2CppObject* (*) (ReactiveProperty_1_t3951240486 *, const MethodInfo*))ReactiveProperty_1_get_EqualityComparer_m3290095395_gshared)(__this, method)
// T UniRx.ReactiveProperty`1<UnityEngine.AnimationCurve>::get_Value()
#define ReactiveProperty_1_get_Value_m3552548575(__this, method) ((  AnimationCurve_t3342907448 * (*) (ReactiveProperty_1_t3951240486 *, const MethodInfo*))ReactiveProperty_1_get_Value_m2793108311_gshared)(__this, method)
// System.Void UniRx.ReactiveProperty`1<UnityEngine.AnimationCurve>::set_Value(T)
#define ReactiveProperty_1_set_Value_m1676946452(__this, ___value0, method) ((  void (*) (ReactiveProperty_1_t3951240486 *, AnimationCurve_t3342907448 *, const MethodInfo*))ReactiveProperty_1_set_Value_m1580705402_gshared)(__this, ___value0, method)
// System.Void UniRx.ReactiveProperty`1<UnityEngine.AnimationCurve>::SetValue(T)
#define ReactiveProperty_1_SetValue_m4157654819(__this, ___value0, method) ((  void (*) (ReactiveProperty_1_t3951240486 *, AnimationCurve_t3342907448 *, const MethodInfo*))ReactiveProperty_1_SetValue_m4154550269_gshared)(__this, ___value0, method)
// System.Void UniRx.ReactiveProperty`1<UnityEngine.AnimationCurve>::SetValueAndForceNotify(T)
#define ReactiveProperty_1_SetValueAndForceNotify_m3693941670(__this, ___value0, method) ((  void (*) (ReactiveProperty_1_t3951240486 *, AnimationCurve_t3342907448 *, const MethodInfo*))ReactiveProperty_1_SetValueAndForceNotify_m2537434880_gshared)(__this, ___value0, method)
// System.IDisposable UniRx.ReactiveProperty`1<UnityEngine.AnimationCurve>::Subscribe(UniRx.IObserver`1<T>)
#define ReactiveProperty_1_Subscribe_m3659482583(__this, ___observer0, method) ((  Il2CppObject * (*) (ReactiveProperty_1_t3951240486 *, Il2CppObject*, const MethodInfo*))ReactiveProperty_1_Subscribe_m958004807_gshared)(__this, ___observer0, method)
// System.Void UniRx.ReactiveProperty`1<UnityEngine.AnimationCurve>::Dispose()
#define ReactiveProperty_1_Dispose_m1050133461(__this, method) ((  void (*) (ReactiveProperty_1_t3951240486 *, const MethodInfo*))ReactiveProperty_1_Dispose_m938398511_gshared)(__this, method)
// System.Void UniRx.ReactiveProperty`1<UnityEngine.AnimationCurve>::Dispose(System.Boolean)
#define ReactiveProperty_1_Dispose_m1379704972(__this, ___disposing0, method) ((  void (*) (ReactiveProperty_1_t3951240486 *, bool, const MethodInfo*))ReactiveProperty_1_Dispose_m431016550_gshared)(__this, ___disposing0, method)
// System.String UniRx.ReactiveProperty`1<UnityEngine.AnimationCurve>::ToString()
#define ReactiveProperty_1_ToString_m4032624059(__this, method) ((  String_t* (*) (ReactiveProperty_1_t3951240486 *, const MethodInfo*))ReactiveProperty_1_ToString_m2722346619_gshared)(__this, method)
// System.Boolean UniRx.ReactiveProperty`1<UnityEngine.AnimationCurve>::IsRequiredSubscribeOnCurrentThread()
#define ReactiveProperty_1_IsRequiredSubscribeOnCurrentThread_m1232288747(__this, method) ((  bool (*) (ReactiveProperty_1_t3951240486 *, const MethodInfo*))ReactiveProperty_1_IsRequiredSubscribeOnCurrentThread_m3797922201_gshared)(__this, method)
