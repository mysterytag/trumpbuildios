﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

// Zenject.BindingConditionSetter
struct BindingConditionSetter_t259147722;
// System.String
struct String_t;
// System.Func`2<Zenject.InjectContext,System.Object>
struct Func_2_t2621245597;
// Zenject.BinderUntyped
struct BinderUntyped_t2430239676;
// UnityEngine.GameObject
struct GameObject_t4012695102;
// System.Object
struct Il2CppObject;
// Zenject.BindScope
struct BindScope_t2945157996;
// Zenject.BinderGeneric`1<System.Object>
struct BinderGeneric_1_t520705716;
// Zenject.DiContainer
struct DiContainer_t2383114449;
// Zenject.IFactoryBinder`1<System.Object>
struct IFactoryBinder_1_t1064970850;
// Zenject.BinderGeneric`1<Zenject.IFactory`1<System.Object>>
struct BinderGeneric_1_t1807883816;
// Zenject.IFactory`1<System.Object>
struct IFactory_1_t2124284520;
// Zenject.InjectContext
struct InjectContext_t3456483891;
// Zenject.IFactoryBinder`2<System.Object,System.Object>
struct IFactoryBinder_2_t3921329133;
// Zenject.BinderGeneric`1<Zenject.IFactory`2<System.Object,System.Object>>
struct BinderGeneric_1_t655913167;
// Zenject.IFactory`2<System.Object,System.Object>
struct IFactory_2_t972313871;
// Zenject.IFactoryBinder`3<System.Object,System.Object,System.Object>
struct IFactoryBinder_3_t153990564;
// Zenject.BinderGeneric`1<Zenject.IFactory`3<System.Object,System.Object,System.Object>>
struct BinderGeneric_1_t1303598458;
// Zenject.IFactory`3<System.Object,System.Object,System.Object>
struct IFactory_3_t1619999162;
// Zenject.IFactoryBinder`4<System.Object,System.Object,System.Object,System.Object>
struct IFactoryBinder_4_t2507049579;
// Zenject.BinderGeneric`1<Zenject.IFactory`4<System.Object,System.Object,System.Object,System.Object>>
struct BinderGeneric_1_t1578143997;
// Zenject.IFactory`4<System.Object,System.Object,System.Object,System.Object>
struct IFactory_4_t1894544701;
// Zenject.IFactoryBinder`5<System.Object,System.Object,System.Object,System.Object,System.Object>
struct IFactoryBinder_5_t1497530694;
// Zenject.BinderGeneric`1<Zenject.IFactory`5<System.Object,System.Object,System.Object,System.Object,System.Object>>
struct BinderGeneric_1_t46883500;
// Zenject.IFactory`5<System.Object,System.Object,System.Object,System.Object,System.Object>
struct IFactory_5_t363284204;
// Zenject.IFactoryUntypedBinder`1<System.Object>
struct IFactoryUntypedBinder_1_t486203833;
// Zenject.BinderGeneric`1<Zenject.IFactoryUntyped`1<System.Object>>
struct BinderGeneric_1_t1217979071;
// Zenject.BinderGeneric`1<ModestTree.Util.Tuple`2<System.Object,System.Type>>
struct BinderGeneric_1_t4209734510;
// ModestTree.Util.Tuple`2<System.Object,System.Type>
struct Tuple_2_t231167918;
// System.Type
struct Type_t;
// ModestTree.Util.Tuple`2<System.Object,System.Object>
struct Tuple_2_t2584011699;
// System.Object[]
struct ObjectU5BU5D_t11523773;
// Zenject.ProviderBase
struct ProviderBase_t1627494391;
// Zenject.SingletonProviderMap
struct SingletonProviderMap_t1557411893;
// Zenject.SingletonLazyCreator
struct SingletonLazyCreator_t2762284194;
// Zenject.TypeValuePair
struct TypeValuePair_t620932390;
// ZergRush.IReactiveCollection`1<System.Object>
struct IReactiveCollection_1_t4089814762;
// System.Func`2<System.Object,System.Boolean>
struct Func_2_t1509682273;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Zenject_BinderUntyped2430239676.h"
#include "mscorlib_System_String968488902.h"
#include "AssemblyU2DCSharp_Zenject_BindingConditionSetter259147722.h"
#include "System_Core_System_Func_2_gen2621245597.h"
#include "AssemblyU2DCSharp_Zenject_Binder3872662847MethodDeclarations.h"
#include "AssemblyU2DCSharp_Zenject_Binder3872662847.h"
#include "UnityEngine_UnityEngine_GameObject4012695102.h"
#include "mscorlib_System_Type2779229935MethodDeclarations.h"
#include "mscorlib_System_Type2779229935.h"
#include "mscorlib_System_RuntimeTypeHandle1864875887.h"
#include "AssemblyU2DCSharp_Zenject_BindScope2945157996.h"
#include "mscorlib_System_Object837106420.h"
#include "AssemblyU2DCSharp_Zenject_BindScope2945157996MethodDeclarations.h"
#include "AssemblyU2DCSharp_Zenject_BinderGeneric_1_gen520705716.h"
#include "AssemblyU2DCSharp_Zenject_BinderGeneric_1_gen520705716MethodDeclarations.h"
#include "AssemblyU2DCSharp_Zenject_DiContainer2383114449.h"
#include "UnityEngine_UnityEngine_Object3878351788MethodDeclarations.h"
#include "AssemblyU2DCSharp_ModestTree_TypeExtensions3479487268MethodDeclarations.h"
#include "AssemblyU2DCSharp_ModestTree_MiscExtensions2809094742MethodDeclarations.h"
#include "AssemblyU2DCSharp_Zenject_ZenjectBindException3550319704MethodDeclarations.h"
#include "AssemblyU2DCSharp_Zenject_DiContainer_U3CBindGameOb866492662.h"
#include "AssemblyU2DCSharp_Zenject_DiContainer_U3CBindGameOb866492662MethodDeclarations.h"
#include "mscorlib_System_Void2779279689.h"
#include "mscorlib_System_Boolean211005341.h"
#include "UnityEngine_UnityEngine_Object3878351788.h"
#include "mscorlib_ArrayTypes.h"
#include "AssemblyU2DCSharp_Zenject_ZenjectBindException3550319704.h"
#include "AssemblyU2DCSharp_Zenject_DiContainer2383114449MethodDeclarations.h"
#include "AssemblyU2DCSharp_Zenject_InjectContext3456483891.h"
#include "System_Core_System_Func_2_gen2621245597MethodDeclarations.h"
#include "mscorlib_System_IntPtr676692020.h"
#include "AssemblyU2DCSharp_Zenject_IFactoryBinder_1_gen1064970850.h"
#include "AssemblyU2DCSharp_Zenject_BinderGeneric_1_gen1807883816.h"
#include "AssemblyU2DCSharp_Zenject_IFactoryBinder_1_gen1064970850MethodDeclarations.h"
#include "System_Core_System_Func_2_gen3908423697.h"
#include "System_Core_System_Func_2_gen3908423697MethodDeclarations.h"
#include "AssemblyU2DCSharp_Zenject_BinderGeneric_1_gen1807883816MethodDeclarations.h"
#include "AssemblyU2DCSharp_Zenject_IFactoryBinder_2_gen3921329133.h"
#include "AssemblyU2DCSharp_Zenject_BinderGeneric_1_gen655913167.h"
#include "AssemblyU2DCSharp_Zenject_IFactoryBinder_2_gen3921329133MethodDeclarations.h"
#include "System_Core_System_Func_2_gen2756453048.h"
#include "System_Core_System_Func_2_gen2756453048MethodDeclarations.h"
#include "AssemblyU2DCSharp_Zenject_BinderGeneric_1_gen655913167MethodDeclarations.h"
#include "AssemblyU2DCSharp_Zenject_IFactoryBinder_3_gen153990564.h"
#include "AssemblyU2DCSharp_Zenject_BinderGeneric_1_gen1303598458.h"
#include "AssemblyU2DCSharp_Zenject_IFactoryBinder_3_gen153990564MethodDeclarations.h"
#include "System_Core_System_Func_2_gen3404138339.h"
#include "System_Core_System_Func_2_gen3404138339MethodDeclarations.h"
#include "AssemblyU2DCSharp_Zenject_BinderGeneric_1_gen1303598458MethodDeclarations.h"
#include "AssemblyU2DCSharp_Zenject_IFactoryBinder_4_gen2507049579.h"
#include "AssemblyU2DCSharp_Zenject_BinderGeneric_1_gen1578143997.h"
#include "AssemblyU2DCSharp_Zenject_IFactoryBinder_4_gen2507049579MethodDeclarations.h"
#include "System_Core_System_Func_2_gen3678683878.h"
#include "System_Core_System_Func_2_gen3678683878MethodDeclarations.h"
#include "AssemblyU2DCSharp_Zenject_BinderGeneric_1_gen1578143997MethodDeclarations.h"
#include "AssemblyU2DCSharp_Zenject_IFactoryBinder_5_gen1497530694.h"
#include "AssemblyU2DCSharp_Zenject_BinderGeneric_1_gen46883500.h"
#include "AssemblyU2DCSharp_Zenject_IFactoryBinder_5_gen1497530694MethodDeclarations.h"
#include "System_Core_System_Func_2_gen2147423381.h"
#include "System_Core_System_Func_2_gen2147423381MethodDeclarations.h"
#include "AssemblyU2DCSharp_Zenject_BinderGeneric_1_gen46883500MethodDeclarations.h"
#include "AssemblyU2DCSharp_Zenject_IFactoryUntypedBinder_1_g486203833.h"
#include "AssemblyU2DCSharp_Zenject_BinderGeneric_1_gen1217979071.h"
#include "AssemblyU2DCSharp_Zenject_BinderGeneric_1_gen1217979071MethodDeclarations.h"
#include "AssemblyU2DCSharp_Zenject_KeyedFactoryBase_2_gen2864689411.h"
#include "AssemblyU2DCSharp_Zenject_BinderGeneric_1_gen4209734510.h"
#include "AssemblyU2DCSharp_ModestTree_Util_Tuple2948270274MethodDeclarations.h"
#include "AssemblyU2DCSharp_ModestTree_Util_Tuple_2_gen231167918.h"
#include "AssemblyU2DCSharp_ModestTree_Util_Tuple2948270274.h"
#include "AssemblyU2DCSharp_Zenject_BinderGeneric_1_gen4209734510MethodDeclarations.h"
#include "AssemblyU2DCSharp_Zenject_FactoryNested_2_gen2234585871.h"
#include "AssemblyU2DCSharp_Zenject_FactoryNested_2_gen2234585871MethodDeclarations.h"
#include "AssemblyU2DCSharp_Zenject_FactoryNested_3_gen3116483514.h"
#include "AssemblyU2DCSharp_Zenject_FactoryNested_3_gen3116483514MethodDeclarations.h"
#include "AssemblyU2DCSharp_Zenject_FactoryNested_4_gen2122089277.h"
#include "AssemblyU2DCSharp_Zenject_FactoryNested_4_gen2122089277MethodDeclarations.h"
#include "AssemblyU2DCSharp_Zenject_FactoryNested_5_gen2227126508.h"
#include "AssemblyU2DCSharp_Zenject_FactoryNested_5_gen2227126508MethodDeclarations.h"
#include "AssemblyU2DCSharp_Zenject_FactoryNested_6_gen1301445835.h"
#include "AssemblyU2DCSharp_Zenject_FactoryNested_6_gen1301445835MethodDeclarations.h"
#include "AssemblyU2DCSharp_Zenject_IFactoryUntypedBinder_1_g486203833MethodDeclarations.h"
#include "AssemblyU2DCSharp_Zenject_SingletonProviderMap1557411893.h"
#include "AssemblyU2DCSharp_Zenject_ProviderBase1627494391.h"
#include "AssemblyU2DCSharp_Zenject_SingletonProvider585108081MethodDeclarations.h"
#include "AssemblyU2DCSharp_Zenject_SingletonProviderMap1557411893MethodDeclarations.h"
#include "AssemblyU2DCSharp_Zenject_SingletonLazyCreator2762284194.h"
#include "AssemblyU2DCSharp_Zenject_SingletonProvider585108081.h"
#include "AssemblyU2DCSharp_Zenject_SingletonId1838183899MethodDeclarations.h"
#include "AssemblyU2DCSharp_Zenject_SingletonLazyCreator2762284194MethodDeclarations.h"
#include "AssemblyU2DCSharp_Zenject_SingletonId1838183899.h"
#include "AssemblyU2DCSharp_Zenject_SingletonProviderMap_U3C3364390262.h"
#include "AssemblyU2DCSharp_Zenject_SingletonProviderMap_U3C3364390262MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g2265680905.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g2265680905MethodDeclarations.h"
#include "AssemblyU2DCSharp_Zenject_InstantiateUtil3253484897.h"
#include "AssemblyU2DCSharp_Zenject_TypeValuePair620932390.h"
#include "mscorlib_System_Object837106420MethodDeclarations.h"
#include "AssemblyU2DCSharp_Zenject_TypeValuePair620932390MethodDeclarations.h"
#include "AssemblyU2DCSharp_ZergRush_ReactiveCollectionExten3620204240.h"
#include "System_Core_System_Func_2_gen1509682273.h"

// Zenject.BindingConditionSetter Zenject.BinderUntyped::ToSingleMethod<System.Object>(System.String,System.Func`2<Zenject.InjectContext,TConcrete>)
extern "C"  BindingConditionSetter_t259147722 * BinderUntyped_ToSingleMethod_TisIl2CppObject_m255892173_gshared (BinderUntyped_t2430239676 * __this, String_t* ___concreteIdentifier0, Func_2_t2621245597 * ___method1, const MethodInfo* method);
#define BinderUntyped_ToSingleMethod_TisIl2CppObject_m255892173(__this, ___concreteIdentifier0, ___method1, method) ((  BindingConditionSetter_t259147722 * (*) (BinderUntyped_t2430239676 *, String_t*, Func_2_t2621245597 *, const MethodInfo*))BinderUntyped_ToSingleMethod_TisIl2CppObject_m255892173_gshared)(__this, ___concreteIdentifier0, ___method1, method)
// Zenject.BindingConditionSetter Zenject.Binder::ToSingleMethodBase<System.Object>(System.String,System.Func`2<Zenject.InjectContext,!!0>)
extern "C"  BindingConditionSetter_t259147722 * Binder_ToSingleMethodBase_TisIl2CppObject_m352975161_gshared (Binder_t3872662847 * __this, String_t* p0, Func_2_t2621245597 * p1, const MethodInfo* method);
#define Binder_ToSingleMethodBase_TisIl2CppObject_m352975161(__this, p0, p1, method) ((  BindingConditionSetter_t259147722 * (*) (Binder_t3872662847 *, String_t*, Func_2_t2621245597 *, const MethodInfo*))Binder_ToSingleMethodBase_TisIl2CppObject_m352975161_gshared)(__this, p0, p1, method)
// Zenject.BindingConditionSetter Zenject.BinderUntyped::ToSingleMonoBehaviour<System.Object>(UnityEngine.GameObject)
extern "C"  BindingConditionSetter_t259147722 * BinderUntyped_ToSingleMonoBehaviour_TisIl2CppObject_m4105677077_gshared (BinderUntyped_t2430239676 * __this, GameObject_t4012695102 * ___gameObject0, const MethodInfo* method);
#define BinderUntyped_ToSingleMonoBehaviour_TisIl2CppObject_m4105677077(__this, ___gameObject0, method) ((  BindingConditionSetter_t259147722 * (*) (BinderUntyped_t2430239676 *, GameObject_t4012695102 *, const MethodInfo*))BinderUntyped_ToSingleMonoBehaviour_TisIl2CppObject_m4105677077_gshared)(__this, ___gameObject0, method)
// Zenject.BindingConditionSetter Zenject.Binder::ToSingleMonoBehaviourBase<System.Object>(UnityEngine.GameObject)
extern "C"  BindingConditionSetter_t259147722 * Binder_ToSingleMonoBehaviourBase_TisIl2CppObject_m468953191_gshared (Binder_t3872662847 * __this, GameObject_t4012695102 * p0, const MethodInfo* method);
#define Binder_ToSingleMonoBehaviourBase_TisIl2CppObject_m468953191(__this, p0, method) ((  BindingConditionSetter_t259147722 * (*) (Binder_t3872662847 *, GameObject_t4012695102 *, const MethodInfo*))Binder_ToSingleMonoBehaviourBase_TisIl2CppObject_m468953191_gshared)(__this, p0, method)
// Zenject.BindingConditionSetter Zenject.BinderUntyped::ToSinglePrefab<System.Object>(System.String,UnityEngine.GameObject)
extern "C"  BindingConditionSetter_t259147722 * BinderUntyped_ToSinglePrefab_TisIl2CppObject_m3655072379_gshared (BinderUntyped_t2430239676 * __this, String_t* ___identifier0, GameObject_t4012695102 * ___prefab1, const MethodInfo* method);
#define BinderUntyped_ToSinglePrefab_TisIl2CppObject_m3655072379(__this, ___identifier0, ___prefab1, method) ((  BindingConditionSetter_t259147722 * (*) (BinderUntyped_t2430239676 *, String_t*, GameObject_t4012695102 *, const MethodInfo*))BinderUntyped_ToSinglePrefab_TisIl2CppObject_m3655072379_gshared)(__this, ___identifier0, ___prefab1, method)
// Zenject.BindingConditionSetter Zenject.BinderUntyped::ToSinglePrefab<System.Object>(UnityEngine.GameObject)
extern "C"  BindingConditionSetter_t259147722 * BinderUntyped_ToSinglePrefab_TisIl2CppObject_m3636588023_gshared (BinderUntyped_t2430239676 * __this, GameObject_t4012695102 * ___prefab0, const MethodInfo* method);
#define BinderUntyped_ToSinglePrefab_TisIl2CppObject_m3636588023(__this, ___prefab0, method) ((  BindingConditionSetter_t259147722 * (*) (BinderUntyped_t2430239676 *, GameObject_t4012695102 *, const MethodInfo*))BinderUntyped_ToSinglePrefab_TisIl2CppObject_m3636588023_gshared)(__this, ___prefab0, method)
// Zenject.BindingConditionSetter Zenject.BinderUntyped::ToSinglePrefabResource<System.Object>(System.String)
extern "C"  BindingConditionSetter_t259147722 * BinderUntyped_ToSinglePrefabResource_TisIl2CppObject_m2578279249_gshared (BinderUntyped_t2430239676 * __this, String_t* ___resourcePath0, const MethodInfo* method);
#define BinderUntyped_ToSinglePrefabResource_TisIl2CppObject_m2578279249(__this, ___resourcePath0, method) ((  BindingConditionSetter_t259147722 * (*) (BinderUntyped_t2430239676 *, String_t*, const MethodInfo*))BinderUntyped_ToSinglePrefabResource_TisIl2CppObject_m2578279249_gshared)(__this, ___resourcePath0, method)
// Zenject.BindingConditionSetter Zenject.BinderUntyped::ToSinglePrefabResource<System.Object>(System.String,System.String)
extern "C"  BindingConditionSetter_t259147722 * BinderUntyped_ToSinglePrefabResource_TisIl2CppObject_m3678138829_gshared (BinderUntyped_t2430239676 * __this, String_t* ___identifier0, String_t* ___resourcePath1, const MethodInfo* method);
#define BinderUntyped_ToSinglePrefabResource_TisIl2CppObject_m3678138829(__this, ___identifier0, ___resourcePath1, method) ((  BindingConditionSetter_t259147722 * (*) (BinderUntyped_t2430239676 *, String_t*, String_t*, const MethodInfo*))BinderUntyped_ToSinglePrefabResource_TisIl2CppObject_m3678138829_gshared)(__this, ___identifier0, ___resourcePath1, method)
// Zenject.BindingConditionSetter Zenject.BinderUntyped::ToTransient<System.Object>()
extern "C"  BindingConditionSetter_t259147722 * BinderUntyped_ToTransient_TisIl2CppObject_m1236544005_gshared (BinderUntyped_t2430239676 * __this, const MethodInfo* method);
#define BinderUntyped_ToTransient_TisIl2CppObject_m1236544005(__this, method) ((  BindingConditionSetter_t259147722 * (*) (BinderUntyped_t2430239676 *, const MethodInfo*))BinderUntyped_ToTransient_TisIl2CppObject_m1236544005_gshared)(__this, method)
// Zenject.BindingConditionSetter Zenject.BinderUntyped::ToTransientPrefab<System.Object>(UnityEngine.GameObject)
extern "C"  BindingConditionSetter_t259147722 * BinderUntyped_ToTransientPrefab_TisIl2CppObject_m2159315865_gshared (BinderUntyped_t2430239676 * __this, GameObject_t4012695102 * ___prefab0, const MethodInfo* method);
#define BinderUntyped_ToTransientPrefab_TisIl2CppObject_m2159315865(__this, ___prefab0, method) ((  BindingConditionSetter_t259147722 * (*) (BinderUntyped_t2430239676 *, GameObject_t4012695102 *, const MethodInfo*))BinderUntyped_ToTransientPrefab_TisIl2CppObject_m2159315865_gshared)(__this, ___prefab0, method)
// Zenject.BindingConditionSetter Zenject.BinderUntyped::ToTransientPrefabResource<System.Object>(System.String)
extern "C"  BindingConditionSetter_t259147722 * BinderUntyped_ToTransientPrefabResource_TisIl2CppObject_m1006604655_gshared (BinderUntyped_t2430239676 * __this, String_t* ___resourcePath0, const MethodInfo* method);
#define BinderUntyped_ToTransientPrefabResource_TisIl2CppObject_m1006604655(__this, ___resourcePath0, method) ((  BindingConditionSetter_t259147722 * (*) (BinderUntyped_t2430239676 *, String_t*, const MethodInfo*))BinderUntyped_ToTransientPrefabResource_TisIl2CppObject_m1006604655_gshared)(__this, ___resourcePath0, method)
// Zenject.BindingConditionSetter Zenject.BindScope::BindInstance<System.Object>(TContract)
extern "C"  BindingConditionSetter_t259147722 * BindScope_BindInstance_TisIl2CppObject_m2632817704_gshared (BindScope_t2945157996 * __this, Il2CppObject * ___obj0, const MethodInfo* method);
#define BindScope_BindInstance_TisIl2CppObject_m2632817704(__this, ___obj0, method) ((  BindingConditionSetter_t259147722 * (*) (BindScope_t2945157996 *, Il2CppObject *, const MethodInfo*))BindScope_BindInstance_TisIl2CppObject_m2632817704_gshared)(__this, ___obj0, method)
// Zenject.BinderGeneric`1<!!0> Zenject.BindScope::Bind<System.Object>(System.String)
extern "C"  BinderGeneric_1_t520705716 * BindScope_Bind_TisIl2CppObject_m199803498_gshared (BindScope_t2945157996 * __this, String_t* p0, const MethodInfo* method);
#define BindScope_Bind_TisIl2CppObject_m199803498(__this, p0, method) ((  BinderGeneric_1_t520705716 * (*) (BindScope_t2945157996 *, String_t*, const MethodInfo*))BindScope_Bind_TisIl2CppObject_m199803498_gshared)(__this, p0, method)
// Zenject.BindingConditionSetter Zenject.BinderGeneric`1<System.Object>::ToInstance<System.Object>(!!0)
extern "C"  BindingConditionSetter_t259147722 * BinderGeneric_1_ToInstance_TisIl2CppObject_m1818222243_gshared (BinderGeneric_1_t520705716 * __this, Il2CppObject * p0, const MethodInfo* method);
#define BinderGeneric_1_ToInstance_TisIl2CppObject_m1818222243(__this, p0, method) ((  BindingConditionSetter_t259147722 * (*) (BinderGeneric_1_t520705716 *, Il2CppObject *, const MethodInfo*))BinderGeneric_1_ToInstance_TisIl2CppObject_m1818222243_gshared)(__this, p0, method)
// Zenject.BindingConditionSetter Zenject.DiContainer::BindGameObjectFactory<System.Object>(UnityEngine.GameObject)
extern "C"  BindingConditionSetter_t259147722 * DiContainer_BindGameObjectFactory_TisIl2CppObject_m1857473477_gshared (DiContainer_t2383114449 * __this, GameObject_t4012695102 * ___prefab0, const MethodInfo* method);
#define DiContainer_BindGameObjectFactory_TisIl2CppObject_m1857473477(__this, ___prefab0, method) ((  BindingConditionSetter_t259147722 * (*) (DiContainer_t2383114449 *, GameObject_t4012695102 *, const MethodInfo*))DiContainer_BindGameObjectFactory_TisIl2CppObject_m1857473477_gshared)(__this, ___prefab0, method)
// Zenject.BinderGeneric`1<!!0> Zenject.DiContainer::Bind<System.Object>()
extern "C"  BinderGeneric_1_t520705716 * DiContainer_Bind_TisIl2CppObject_m3651981917_gshared (DiContainer_t2383114449 * __this, const MethodInfo* method);
#define DiContainer_Bind_TisIl2CppObject_m3651981917(__this, method) ((  BinderGeneric_1_t520705716 * (*) (DiContainer_t2383114449 *, const MethodInfo*))DiContainer_Bind_TisIl2CppObject_m3651981917_gshared)(__this, method)
// Zenject.BindingConditionSetter Zenject.DiContainer::BindInstance<System.Object>(System.String,TContract)
extern "C"  BindingConditionSetter_t259147722 * DiContainer_BindInstance_TisIl2CppObject_m1662892425_gshared (DiContainer_t2383114449 * __this, String_t* ___identifier0, Il2CppObject * ___obj1, const MethodInfo* method);
#define DiContainer_BindInstance_TisIl2CppObject_m1662892425(__this, ___identifier0, ___obj1, method) ((  BindingConditionSetter_t259147722 * (*) (DiContainer_t2383114449 *, String_t*, Il2CppObject *, const MethodInfo*))DiContainer_BindInstance_TisIl2CppObject_m1662892425_gshared)(__this, ___identifier0, ___obj1, method)
// Zenject.BinderGeneric`1<!!0> Zenject.DiContainer::Bind<System.Object>(System.String)
extern "C"  BinderGeneric_1_t520705716 * DiContainer_Bind_TisIl2CppObject_m288115653_gshared (DiContainer_t2383114449 * __this, String_t* p0, const MethodInfo* method);
#define DiContainer_Bind_TisIl2CppObject_m288115653(__this, p0, method) ((  BinderGeneric_1_t520705716 * (*) (DiContainer_t2383114449 *, String_t*, const MethodInfo*))DiContainer_Bind_TisIl2CppObject_m288115653_gshared)(__this, p0, method)
// Zenject.BindingConditionSetter Zenject.DiContainer::BindInstance<System.Object>(TContract)
extern "C"  BindingConditionSetter_t259147722 * DiContainer_BindInstance_TisIl2CppObject_m3957247117_gshared (DiContainer_t2383114449 * __this, Il2CppObject * ___obj0, const MethodInfo* method);
#define DiContainer_BindInstance_TisIl2CppObject_m3957247117(__this, ___obj0, method) ((  BindingConditionSetter_t259147722 * (*) (DiContainer_t2383114449 *, Il2CppObject *, const MethodInfo*))DiContainer_BindInstance_TisIl2CppObject_m3957247117_gshared)(__this, ___obj0, method)
// Zenject.BindingConditionSetter Zenject.IBinder::BindGameObjectFactory<System.Object>(UnityEngine.GameObject)
extern "C"  BindingConditionSetter_t259147722 * IBinder_BindGameObjectFactory_TisIl2CppObject_m2067765116_gshared (Il2CppObject * __this, GameObject_t4012695102 * ___prefab0, const MethodInfo* method);
#define IBinder_BindGameObjectFactory_TisIl2CppObject_m2067765116(__this, ___prefab0, method) ((  BindingConditionSetter_t259147722 * (*) (Il2CppObject *, GameObject_t4012695102 *, const MethodInfo*))IBinder_BindGameObjectFactory_TisIl2CppObject_m2067765116_gshared)(__this, ___prefab0, method)
// Zenject.BindingConditionSetter Zenject.IBinder::BindInstance<System.Object>(System.String,TContract)
extern "C"  BindingConditionSetter_t259147722 * IBinder_BindInstance_TisIl2CppObject_m2555194432_gshared (Il2CppObject * __this, String_t* ___identifier0, Il2CppObject * ___obj1, const MethodInfo* method);
#define IBinder_BindInstance_TisIl2CppObject_m2555194432(__this, ___identifier0, ___obj1, method) ((  BindingConditionSetter_t259147722 * (*) (Il2CppObject *, String_t*, Il2CppObject *, const MethodInfo*))IBinder_BindInstance_TisIl2CppObject_m2555194432_gshared)(__this, ___identifier0, ___obj1, method)
// Zenject.BindingConditionSetter Zenject.IBinder::BindInstance<System.Object>(TContract)
extern "C"  BindingConditionSetter_t259147722 * IBinder_BindInstance_TisIl2CppObject_m3920837508_gshared (Il2CppObject * __this, Il2CppObject * ___obj0, const MethodInfo* method);
#define IBinder_BindInstance_TisIl2CppObject_m3920837508(__this, ___obj0, method) ((  BindingConditionSetter_t259147722 * (*) (Il2CppObject *, Il2CppObject *, const MethodInfo*))IBinder_BindInstance_TisIl2CppObject_m3920837508_gshared)(__this, ___obj0, method)
// Zenject.BindingConditionSetter Zenject.IFactoryBinder`1<System.Object>::ToCustomFactory<System.Object,System.Object>()
extern "C"  BindingConditionSetter_t259147722 * IFactoryBinder_1_ToCustomFactory_TisIl2CppObject_TisIl2CppObject_m1625714013_gshared (IFactoryBinder_1_t1064970850 * __this, const MethodInfo* method);
#define IFactoryBinder_1_ToCustomFactory_TisIl2CppObject_TisIl2CppObject_m1625714013(__this, method) ((  BindingConditionSetter_t259147722 * (*) (IFactoryBinder_1_t1064970850 *, const MethodInfo*))IFactoryBinder_1_ToCustomFactory_TisIl2CppObject_TisIl2CppObject_m1625714013_gshared)(__this, method)
// Zenject.BinderGeneric`1<!!0> Zenject.DiContainer::Bind<Zenject.IFactory`1<System.Object>>(System.String)
#define DiContainer_Bind_TisIFactory_1_t2124284520_m2089390002(__this, p0, method) ((  BinderGeneric_1_t1807883816 * (*) (DiContainer_t2383114449 *, String_t*, const MethodInfo*))DiContainer_Bind_TisIl2CppObject_m288115653_gshared)(__this, p0, method)
// Zenject.IFactory`1<TContract> Zenject.IFactoryBinder`1<System.Object>::<ToCustomFactory`2>m__296<System.Object,System.Object>(Zenject.InjectContext)
extern "C"  Il2CppObject* IFactoryBinder_1_U3CToCustomFactory_2U3Em__296_TisIl2CppObject_TisIl2CppObject_m3882463147_gshared (Il2CppObject * __this /* static, unused */, InjectContext_t3456483891 * p0, const MethodInfo* method);
#define IFactoryBinder_1_U3CToCustomFactory_2U3Em__296_TisIl2CppObject_TisIl2CppObject_m3882463147(__this /* static, unused */, p0, method) ((  Il2CppObject* (*) (Il2CppObject * /* static, unused */, InjectContext_t3456483891 *, const MethodInfo*))IFactoryBinder_1_U3CToCustomFactory_2U3Em__296_TisIl2CppObject_TisIl2CppObject_m3882463147_gshared)(__this /* static, unused */, p0, method)
// Zenject.BindingConditionSetter Zenject.IFactoryBinder`1<System.Object>::ToCustomFactory<System.Object>()
extern "C"  BindingConditionSetter_t259147722 * IFactoryBinder_1_ToCustomFactory_TisIl2CppObject_m2469856719_gshared (IFactoryBinder_1_t1064970850 * __this, const MethodInfo* method);
#define IFactoryBinder_1_ToCustomFactory_TisIl2CppObject_m2469856719(__this, method) ((  BindingConditionSetter_t259147722 * (*) (IFactoryBinder_1_t1064970850 *, const MethodInfo*))IFactoryBinder_1_ToCustomFactory_TisIl2CppObject_m2469856719_gshared)(__this, method)
// Zenject.BindingConditionSetter Zenject.BinderGeneric`1<System.Object>::ToTransient<System.Object>()
extern "C"  BindingConditionSetter_t259147722 * BinderGeneric_1_ToTransient_TisIl2CppObject_m4254792828_gshared (BinderGeneric_1_t520705716 * __this, const MethodInfo* method);
#define BinderGeneric_1_ToTransient_TisIl2CppObject_m4254792828(__this, method) ((  BindingConditionSetter_t259147722 * (*) (BinderGeneric_1_t520705716 *, const MethodInfo*))BinderGeneric_1_ToTransient_TisIl2CppObject_m4254792828_gshared)(__this, method)
// Zenject.BindingConditionSetter Zenject.BinderGeneric`1<Zenject.IFactory`1<System.Object>>::ToTransient<System.Object>()
#define BinderGeneric_1_ToTransient_TisIl2CppObject_m3582600175(__this, method) ((  BindingConditionSetter_t259147722 * (*) (BinderGeneric_1_t1807883816 *, const MethodInfo*))BinderGeneric_1_ToTransient_TisIl2CppObject_m4254792828_gshared)(__this, method)
// Zenject.BindingConditionSetter Zenject.IFactoryBinder`1<System.Object>::ToFactory<System.Object>()
extern "C"  BindingConditionSetter_t259147722 * IFactoryBinder_1_ToFactory_TisIl2CppObject_m2101097854_gshared (IFactoryBinder_1_t1064970850 * __this, const MethodInfo* method);
#define IFactoryBinder_1_ToFactory_TisIl2CppObject_m2101097854(__this, method) ((  BindingConditionSetter_t259147722 * (*) (IFactoryBinder_1_t1064970850 *, const MethodInfo*))IFactoryBinder_1_ToFactory_TisIl2CppObject_m2101097854_gshared)(__this, method)
// Zenject.BindingConditionSetter Zenject.IFactoryBinder`1<System.Object>::ToCustomFactory<System.Object,Zenject.Factory`1<System.Object>>()
#define IFactoryBinder_1_ToCustomFactory_TisIl2CppObject_TisFactory_1_t250748329_m2234405667(__this, method) ((  BindingConditionSetter_t259147722 * (*) (IFactoryBinder_1_t1064970850 *, const MethodInfo*))IFactoryBinder_1_ToCustomFactory_TisIl2CppObject_TisIl2CppObject_m1625714013_gshared)(__this, method)
// Zenject.BindingConditionSetter Zenject.IFactoryBinder`1<System.Object>::ToIFactory<System.Object>()
extern "C"  BindingConditionSetter_t259147722 * IFactoryBinder_1_ToIFactory_TisIl2CppObject_m2329947281_gshared (IFactoryBinder_1_t1064970850 * __this, const MethodInfo* method);
#define IFactoryBinder_1_ToIFactory_TisIl2CppObject_m2329947281(__this, method) ((  BindingConditionSetter_t259147722 * (*) (IFactoryBinder_1_t1064970850 *, const MethodInfo*))IFactoryBinder_1_ToIFactory_TisIl2CppObject_m2329947281_gshared)(__this, method)
// Zenject.IFactory`1<TContract> Zenject.IFactoryBinder`1<System.Object>::<ToIFactory`1>m__295<System.Object>(Zenject.InjectContext)
extern "C"  Il2CppObject* IFactoryBinder_1_U3CToIFactory_1U3Em__295_TisIl2CppObject_m119911285_gshared (Il2CppObject * __this /* static, unused */, InjectContext_t3456483891 * p0, const MethodInfo* method);
#define IFactoryBinder_1_U3CToIFactory_1U3Em__295_TisIl2CppObject_m119911285(__this /* static, unused */, p0, method) ((  Il2CppObject* (*) (Il2CppObject * /* static, unused */, InjectContext_t3456483891 *, const MethodInfo*))IFactoryBinder_1_U3CToIFactory_1U3Em__295_TisIl2CppObject_m119911285_gshared)(__this /* static, unused */, p0, method)
// Zenject.BindingConditionSetter Zenject.IFactoryBinder`2<System.Object,System.Object>::ToCustomFactory<System.Object,System.Object>()
extern "C"  BindingConditionSetter_t259147722 * IFactoryBinder_2_ToCustomFactory_TisIl2CppObject_TisIl2CppObject_m3193428618_gshared (IFactoryBinder_2_t3921329133 * __this, const MethodInfo* method);
#define IFactoryBinder_2_ToCustomFactory_TisIl2CppObject_TisIl2CppObject_m3193428618(__this, method) ((  BindingConditionSetter_t259147722 * (*) (IFactoryBinder_2_t3921329133 *, const MethodInfo*))IFactoryBinder_2_ToCustomFactory_TisIl2CppObject_TisIl2CppObject_m3193428618_gshared)(__this, method)
// Zenject.BinderGeneric`1<!!0> Zenject.DiContainer::Bind<Zenject.IFactory`2<System.Object,System.Object>>(System.String)
#define DiContainer_Bind_TisIFactory_2_t972313871_m3824815775(__this, p0, method) ((  BinderGeneric_1_t655913167 * (*) (DiContainer_t2383114449 *, String_t*, const MethodInfo*))DiContainer_Bind_TisIl2CppObject_m288115653_gshared)(__this, p0, method)
// Zenject.IFactory`2<TParam1,TContract> Zenject.IFactoryBinder`2<System.Object,System.Object>::<ToCustomFactory`2>m__29A<System.Object,System.Object>(Zenject.InjectContext)
extern "C"  Il2CppObject* IFactoryBinder_2_U3CToCustomFactory_2U3Em__29A_TisIl2CppObject_TisIl2CppObject_m4242004790_gshared (Il2CppObject * __this /* static, unused */, InjectContext_t3456483891 * p0, const MethodInfo* method);
#define IFactoryBinder_2_U3CToCustomFactory_2U3Em__29A_TisIl2CppObject_TisIl2CppObject_m4242004790(__this /* static, unused */, p0, method) ((  Il2CppObject* (*) (Il2CppObject * /* static, unused */, InjectContext_t3456483891 *, const MethodInfo*))IFactoryBinder_2_U3CToCustomFactory_2U3Em__29A_TisIl2CppObject_TisIl2CppObject_m4242004790_gshared)(__this /* static, unused */, p0, method)
// Zenject.BindingConditionSetter Zenject.IFactoryBinder`2<System.Object,System.Object>::ToCustomFactory<System.Object>()
extern "C"  BindingConditionSetter_t259147722 * IFactoryBinder_2_ToCustomFactory_TisIl2CppObject_m222123452_gshared (IFactoryBinder_2_t3921329133 * __this, const MethodInfo* method);
#define IFactoryBinder_2_ToCustomFactory_TisIl2CppObject_m222123452(__this, method) ((  BindingConditionSetter_t259147722 * (*) (IFactoryBinder_2_t3921329133 *, const MethodInfo*))IFactoryBinder_2_ToCustomFactory_TisIl2CppObject_m222123452_gshared)(__this, method)
// Zenject.BindingConditionSetter Zenject.BinderGeneric`1<Zenject.IFactory`2<System.Object,System.Object>>::ToTransient<System.Object>()
#define BinderGeneric_1_ToTransient_TisIl2CppObject_m3484595682(__this, method) ((  BindingConditionSetter_t259147722 * (*) (BinderGeneric_1_t655913167 *, const MethodInfo*))BinderGeneric_1_ToTransient_TisIl2CppObject_m4254792828_gshared)(__this, method)
// Zenject.BindingConditionSetter Zenject.IFactoryBinder`2<System.Object,System.Object>::ToFactory<System.Object>()
extern "C"  BindingConditionSetter_t259147722 * IFactoryBinder_2_ToFactory_TisIl2CppObject_m3464772395_gshared (IFactoryBinder_2_t3921329133 * __this, const MethodInfo* method);
#define IFactoryBinder_2_ToFactory_TisIl2CppObject_m3464772395(__this, method) ((  BindingConditionSetter_t259147722 * (*) (IFactoryBinder_2_t3921329133 *, const MethodInfo*))IFactoryBinder_2_ToFactory_TisIl2CppObject_m3464772395_gshared)(__this, method)
// Zenject.BindingConditionSetter Zenject.IFactoryBinder`2<System.Object,System.Object>::ToCustomFactory<System.Object,Zenject.Factory`2<System.Object,System.Object>>()
#define IFactoryBinder_2_ToCustomFactory_TisIl2CppObject_TisFactory_2_t2762000298_m894015049(__this, method) ((  BindingConditionSetter_t259147722 * (*) (IFactoryBinder_2_t3921329133 *, const MethodInfo*))IFactoryBinder_2_ToCustomFactory_TisIl2CppObject_TisIl2CppObject_m3193428618_gshared)(__this, method)
// Zenject.BindingConditionSetter Zenject.IFactoryBinder`2<System.Object,System.Object>::ToIFactory<System.Object>()
extern "C"  BindingConditionSetter_t259147722 * IFactoryBinder_2_ToIFactory_TisIl2CppObject_m1654185092_gshared (IFactoryBinder_2_t3921329133 * __this, const MethodInfo* method);
#define IFactoryBinder_2_ToIFactory_TisIl2CppObject_m1654185092(__this, method) ((  BindingConditionSetter_t259147722 * (*) (IFactoryBinder_2_t3921329133 *, const MethodInfo*))IFactoryBinder_2_ToIFactory_TisIl2CppObject_m1654185092_gshared)(__this, method)
// Zenject.IFactory`2<TParam1,TContract> Zenject.IFactoryBinder`2<System.Object,System.Object>::<ToIFactory`1>m__299<System.Object>(Zenject.InjectContext)
extern "C"  Il2CppObject* IFactoryBinder_2_U3CToIFactory_1U3Em__299_TisIl2CppObject_m1599006073_gshared (Il2CppObject * __this /* static, unused */, InjectContext_t3456483891 * p0, const MethodInfo* method);
#define IFactoryBinder_2_U3CToIFactory_1U3Em__299_TisIl2CppObject_m1599006073(__this /* static, unused */, p0, method) ((  Il2CppObject* (*) (Il2CppObject * /* static, unused */, InjectContext_t3456483891 *, const MethodInfo*))IFactoryBinder_2_U3CToIFactory_1U3Em__299_TisIl2CppObject_m1599006073_gshared)(__this /* static, unused */, p0, method)
// Zenject.BindingConditionSetter Zenject.IFactoryBinder`3<System.Object,System.Object,System.Object>::ToCustomFactory<System.Object,System.Object>()
extern "C"  BindingConditionSetter_t259147722 * IFactoryBinder_3_ToCustomFactory_TisIl2CppObject_TisIl2CppObject_m1610445495_gshared (IFactoryBinder_3_t153990564 * __this, const MethodInfo* method);
#define IFactoryBinder_3_ToCustomFactory_TisIl2CppObject_TisIl2CppObject_m1610445495(__this, method) ((  BindingConditionSetter_t259147722 * (*) (IFactoryBinder_3_t153990564 *, const MethodInfo*))IFactoryBinder_3_ToCustomFactory_TisIl2CppObject_TisIl2CppObject_m1610445495_gshared)(__this, method)
// Zenject.BinderGeneric`1<!!0> Zenject.DiContainer::Bind<Zenject.IFactory`3<System.Object,System.Object,System.Object>>(System.String)
#define DiContainer_Bind_TisIFactory_3_t1619999162_m40085644(__this, p0, method) ((  BinderGeneric_1_t1303598458 * (*) (DiContainer_t2383114449 *, String_t*, const MethodInfo*))DiContainer_Bind_TisIl2CppObject_m288115653_gshared)(__this, p0, method)
// Zenject.IFactory`3<TParam1,TParam2,TContract> Zenject.IFactoryBinder`3<System.Object,System.Object,System.Object>::<ToCustomFactory`2>m__29E<System.Object,System.Object>(Zenject.InjectContext)
extern "C"  Il2CppObject* IFactoryBinder_3_U3CToCustomFactory_2U3Em__29E_TisIl2CppObject_TisIl2CppObject_m1558455803_gshared (Il2CppObject * __this /* static, unused */, InjectContext_t3456483891 * p0, const MethodInfo* method);
#define IFactoryBinder_3_U3CToCustomFactory_2U3Em__29E_TisIl2CppObject_TisIl2CppObject_m1558455803(__this /* static, unused */, p0, method) ((  Il2CppObject* (*) (Il2CppObject * /* static, unused */, InjectContext_t3456483891 *, const MethodInfo*))IFactoryBinder_3_U3CToCustomFactory_2U3Em__29E_TisIl2CppObject_TisIl2CppObject_m1558455803_gshared)(__this /* static, unused */, p0, method)
// Zenject.BindingConditionSetter Zenject.IFactoryBinder`3<System.Object,System.Object,System.Object>::ToCustomFactory<System.Object>()
extern "C"  BindingConditionSetter_t259147722 * IFactoryBinder_3_ToCustomFactory_TisIl2CppObject_m89067689_gshared (IFactoryBinder_3_t153990564 * __this, const MethodInfo* method);
#define IFactoryBinder_3_ToCustomFactory_TisIl2CppObject_m89067689(__this, method) ((  BindingConditionSetter_t259147722 * (*) (IFactoryBinder_3_t153990564 *, const MethodInfo*))IFactoryBinder_3_ToCustomFactory_TisIl2CppObject_m89067689_gshared)(__this, method)
// Zenject.BindingConditionSetter Zenject.BinderGeneric`1<Zenject.IFactory`3<System.Object,System.Object,System.Object>>::ToTransient<System.Object>()
#define BinderGeneric_1_ToTransient_TisIl2CppObject_m4018662613(__this, method) ((  BindingConditionSetter_t259147722 * (*) (BinderGeneric_1_t1303598458 *, const MethodInfo*))BinderGeneric_1_ToTransient_TisIl2CppObject_m4254792828_gshared)(__this, method)
// Zenject.BindingConditionSetter Zenject.IFactoryBinder`3<System.Object,System.Object,System.Object>::ToFactory<System.Object>()
extern "C"  BindingConditionSetter_t259147722 * IFactoryBinder_3_ToFactory_TisIl2CppObject_m964946904_gshared (IFactoryBinder_3_t153990564 * __this, const MethodInfo* method);
#define IFactoryBinder_3_ToFactory_TisIl2CppObject_m964946904(__this, method) ((  BindingConditionSetter_t259147722 * (*) (IFactoryBinder_3_t153990564 *, const MethodInfo*))IFactoryBinder_3_ToFactory_TisIl2CppObject_m964946904_gshared)(__this, method)
// Zenject.BindingConditionSetter Zenject.IFactoryBinder`3<System.Object,System.Object,System.Object>::ToCustomFactory<System.Object,Zenject.Factory`3<System.Object,System.Object,System.Object>>()
#define IFactoryBinder_3_ToCustomFactory_TisIl2CppObject_TisFactory_3_t2717473363_m3352819951(__this, method) ((  BindingConditionSetter_t259147722 * (*) (IFactoryBinder_3_t153990564 *, const MethodInfo*))IFactoryBinder_3_ToCustomFactory_TisIl2CppObject_TisIl2CppObject_m1610445495_gshared)(__this, method)
// Zenject.BindingConditionSetter Zenject.IFactoryBinder`3<System.Object,System.Object,System.Object>::ToIFactory<System.Object>()
extern "C"  BindingConditionSetter_t259147722 * IFactoryBinder_3_ToIFactory_TisIl2CppObject_m1469006199_gshared (IFactoryBinder_3_t153990564 * __this, const MethodInfo* method);
#define IFactoryBinder_3_ToIFactory_TisIl2CppObject_m1469006199(__this, method) ((  BindingConditionSetter_t259147722 * (*) (IFactoryBinder_3_t153990564 *, const MethodInfo*))IFactoryBinder_3_ToIFactory_TisIl2CppObject_m1469006199_gshared)(__this, method)
// Zenject.IFactory`3<TParam1,TParam2,TContract> Zenject.IFactoryBinder`3<System.Object,System.Object,System.Object>::<ToIFactory`1>m__29D<System.Object>(Zenject.InjectContext)
extern "C"  Il2CppObject* IFactoryBinder_3_U3CToIFactory_1U3Em__29D_TisIl2CppObject_m1022803491_gshared (Il2CppObject * __this /* static, unused */, InjectContext_t3456483891 * p0, const MethodInfo* method);
#define IFactoryBinder_3_U3CToIFactory_1U3Em__29D_TisIl2CppObject_m1022803491(__this /* static, unused */, p0, method) ((  Il2CppObject* (*) (Il2CppObject * /* static, unused */, InjectContext_t3456483891 *, const MethodInfo*))IFactoryBinder_3_U3CToIFactory_1U3Em__29D_TisIl2CppObject_m1022803491_gshared)(__this /* static, unused */, p0, method)
// Zenject.BindingConditionSetter Zenject.IFactoryBinder`4<System.Object,System.Object,System.Object,System.Object>::ToCustomFactory<System.Object,System.Object>()
extern "C"  BindingConditionSetter_t259147722 * IFactoryBinder_4_ToCustomFactory_TisIl2CppObject_TisIl2CppObject_m1037100516_gshared (IFactoryBinder_4_t2507049579 * __this, const MethodInfo* method);
#define IFactoryBinder_4_ToCustomFactory_TisIl2CppObject_TisIl2CppObject_m1037100516(__this, method) ((  BindingConditionSetter_t259147722 * (*) (IFactoryBinder_4_t2507049579 *, const MethodInfo*))IFactoryBinder_4_ToCustomFactory_TisIl2CppObject_TisIl2CppObject_m1037100516_gshared)(__this, method)
// Zenject.BinderGeneric`1<!!0> Zenject.DiContainer::Bind<Zenject.IFactory`4<System.Object,System.Object,System.Object,System.Object>>(System.String)
#define DiContainer_Bind_TisIFactory_4_t1894544701_m2619084153(__this, p0, method) ((  BinderGeneric_1_t1578143997 * (*) (DiContainer_t2383114449 *, String_t*, const MethodInfo*))DiContainer_Bind_TisIl2CppObject_m288115653_gshared)(__this, p0, method)
// Zenject.IFactory`4<TParam1,TParam2,TParam3,TContract> Zenject.IFactoryBinder`4<System.Object,System.Object,System.Object,System.Object>::<ToCustomFactory`2>m__2A2<System.Object,System.Object>(Zenject.InjectContext)
extern "C"  Il2CppObject* IFactoryBinder_4_U3CToCustomFactory_2U3Em__2A2_TisIl2CppObject_TisIl2CppObject_m3386608290_gshared (Il2CppObject * __this /* static, unused */, InjectContext_t3456483891 * p0, const MethodInfo* method);
#define IFactoryBinder_4_U3CToCustomFactory_2U3Em__2A2_TisIl2CppObject_TisIl2CppObject_m3386608290(__this /* static, unused */, p0, method) ((  Il2CppObject* (*) (Il2CppObject * /* static, unused */, InjectContext_t3456483891 *, const MethodInfo*))IFactoryBinder_4_U3CToCustomFactory_2U3Em__2A2_TisIl2CppObject_TisIl2CppObject_m3386608290_gshared)(__this /* static, unused */, p0, method)
// Zenject.BindingConditionSetter Zenject.IFactoryBinder`4<System.Object,System.Object,System.Object,System.Object>::ToCustomFactory<System.Object>()
extern "C"  BindingConditionSetter_t259147722 * IFactoryBinder_4_ToCustomFactory_TisIl2CppObject_m2367284886_gshared (IFactoryBinder_4_t2507049579 * __this, const MethodInfo* method);
#define IFactoryBinder_4_ToCustomFactory_TisIl2CppObject_m2367284886(__this, method) ((  BindingConditionSetter_t259147722 * (*) (IFactoryBinder_4_t2507049579 *, const MethodInfo*))IFactoryBinder_4_ToCustomFactory_TisIl2CppObject_m2367284886_gshared)(__this, method)
// Zenject.BindingConditionSetter Zenject.BinderGeneric`1<Zenject.IFactory`4<System.Object,System.Object,System.Object,System.Object>>::ToTransient<System.Object>()
#define BinderGeneric_1_ToTransient_TisIl2CppObject_m2141591752(__this, method) ((  BindingConditionSetter_t259147722 * (*) (BinderGeneric_1_t1578143997 *, const MethodInfo*))BinderGeneric_1_ToTransient_TisIl2CppObject_m4254792828_gshared)(__this, method)
// Zenject.BindingConditionSetter Zenject.IFactoryBinder`4<System.Object,System.Object,System.Object,System.Object>::ToFactory<System.Object>()
extern "C"  BindingConditionSetter_t259147722 * IFactoryBinder_4_ToFactory_TisIl2CppObject_m1340929925_gshared (IFactoryBinder_4_t2507049579 * __this, const MethodInfo* method);
#define IFactoryBinder_4_ToFactory_TisIl2CppObject_m1340929925(__this, method) ((  BindingConditionSetter_t259147722 * (*) (IFactoryBinder_4_t2507049579 *, const MethodInfo*))IFactoryBinder_4_ToFactory_TisIl2CppObject_m1340929925_gshared)(__this, method)
// Zenject.BindingConditionSetter Zenject.IFactoryBinder`4<System.Object,System.Object,System.Object,System.Object>::ToCustomFactory<System.Object,Zenject.Factory`4<System.Object,System.Object,System.Object,System.Object>>()
#define IFactoryBinder_4_ToCustomFactory_TisIl2CppObject_TisFactory_4_t549530144_m2743757589(__this, method) ((  BindingConditionSetter_t259147722 * (*) (IFactoryBinder_4_t2507049579 *, const MethodInfo*))IFactoryBinder_4_ToCustomFactory_TisIl2CppObject_TisIl2CppObject_m1037100516_gshared)(__this, method)
// Zenject.BindingConditionSetter Zenject.IFactoryBinder`4<System.Object,System.Object,System.Object,System.Object>::ToIFactory<System.Object>()
extern "C"  BindingConditionSetter_t259147722 * IFactoryBinder_4_ToIFactory_TisIl2CppObject_m239577962_gshared (IFactoryBinder_4_t2507049579 * __this, const MethodInfo* method);
#define IFactoryBinder_4_ToIFactory_TisIl2CppObject_m239577962(__this, method) ((  BindingConditionSetter_t259147722 * (*) (IFactoryBinder_4_t2507049579 *, const MethodInfo*))IFactoryBinder_4_ToIFactory_TisIl2CppObject_m239577962_gshared)(__this, method)
// Zenject.IFactory`4<TParam1,TParam2,TParam3,TContract> Zenject.IFactoryBinder`4<System.Object,System.Object,System.Object,System.Object>::<ToIFactory`1>m__2A1<System.Object>(Zenject.InjectContext)
extern "C"  Il2CppObject* IFactoryBinder_4_U3CToIFactory_1U3Em__2A1_TisIl2CppObject_m3488891462_gshared (Il2CppObject * __this /* static, unused */, InjectContext_t3456483891 * p0, const MethodInfo* method);
#define IFactoryBinder_4_U3CToIFactory_1U3Em__2A1_TisIl2CppObject_m3488891462(__this /* static, unused */, p0, method) ((  Il2CppObject* (*) (Il2CppObject * /* static, unused */, InjectContext_t3456483891 *, const MethodInfo*))IFactoryBinder_4_U3CToIFactory_1U3Em__2A1_TisIl2CppObject_m3488891462_gshared)(__this /* static, unused */, p0, method)
// Zenject.BindingConditionSetter Zenject.IFactoryBinder`5<System.Object,System.Object,System.Object,System.Object,System.Object>::ToCustomFactory<System.Object,System.Object>()
extern "C"  BindingConditionSetter_t259147722 * IFactoryBinder_5_ToCustomFactory_TisIl2CppObject_TisIl2CppObject_m1018422289_gshared (IFactoryBinder_5_t1497530694 * __this, const MethodInfo* method);
#define IFactoryBinder_5_ToCustomFactory_TisIl2CppObject_TisIl2CppObject_m1018422289(__this, method) ((  BindingConditionSetter_t259147722 * (*) (IFactoryBinder_5_t1497530694 *, const MethodInfo*))IFactoryBinder_5_ToCustomFactory_TisIl2CppObject_TisIl2CppObject_m1018422289_gshared)(__this, method)
// Zenject.BinderGeneric`1<!!0> Zenject.DiContainer::Bind<Zenject.IFactory`5<System.Object,System.Object,System.Object,System.Object,System.Object>>(System.String)
#define DiContainer_Bind_TisIFactory_5_t363284204_m23129446(__this, p0, method) ((  BinderGeneric_1_t46883500 * (*) (DiContainer_t2383114449 *, String_t*, const MethodInfo*))DiContainer_Bind_TisIl2CppObject_m288115653_gshared)(__this, p0, method)
// Zenject.IFactory`5<TParam1,TParam2,TParam3,TParam4,TContract> Zenject.IFactoryBinder`5<System.Object,System.Object,System.Object,System.Object,System.Object>::<ToCustomFactory`2>m__2A6<System.Object,System.Object>(Zenject.InjectContext)
extern "C"  Il2CppObject* IFactoryBinder_5_U3CToCustomFactory_2U3Em__2A6_TisIl2CppObject_TisIl2CppObject_m2410447401_gshared (Il2CppObject * __this /* static, unused */, InjectContext_t3456483891 * p0, const MethodInfo* method);
#define IFactoryBinder_5_U3CToCustomFactory_2U3Em__2A6_TisIl2CppObject_TisIl2CppObject_m2410447401(__this /* static, unused */, p0, method) ((  Il2CppObject* (*) (Il2CppObject * /* static, unused */, InjectContext_t3456483891 *, const MethodInfo*))IFactoryBinder_5_U3CToCustomFactory_2U3Em__2A6_TisIl2CppObject_TisIl2CppObject_m2410447401_gshared)(__this /* static, unused */, p0, method)
// Zenject.BindingConditionSetter Zenject.IFactoryBinder`5<System.Object,System.Object,System.Object,System.Object,System.Object>::ToCustomFactory<System.Object>()
extern "C"  BindingConditionSetter_t259147722 * IFactoryBinder_5_ToCustomFactory_TisIl2CppObject_m3644032899_gshared (IFactoryBinder_5_t1497530694 * __this, const MethodInfo* method);
#define IFactoryBinder_5_ToCustomFactory_TisIl2CppObject_m3644032899(__this, method) ((  BindingConditionSetter_t259147722 * (*) (IFactoryBinder_5_t1497530694 *, const MethodInfo*))IFactoryBinder_5_ToCustomFactory_TisIl2CppObject_m3644032899_gshared)(__this, method)
// Zenject.BindingConditionSetter Zenject.BinderGeneric`1<Zenject.IFactory`5<System.Object,System.Object,System.Object,System.Object,System.Object>>::ToTransient<System.Object>()
#define BinderGeneric_1_ToTransient_TisIl2CppObject_m63015355(__this, method) ((  BindingConditionSetter_t259147722 * (*) (BinderGeneric_1_t46883500 *, const MethodInfo*))BinderGeneric_1_ToTransient_TisIl2CppObject_m4254792828_gshared)(__this, method)
// Zenject.BindingConditionSetter Zenject.IFactoryBinder`5<System.Object,System.Object,System.Object,System.Object,System.Object>::ToFactory<System.Object>()
extern "C"  BindingConditionSetter_t259147722 * IFactoryBinder_5_ToFactory_TisIl2CppObject_m341380658_gshared (IFactoryBinder_5_t1497530694 * __this, const MethodInfo* method);
#define IFactoryBinder_5_ToFactory_TisIl2CppObject_m341380658(__this, method) ((  BindingConditionSetter_t259147722 * (*) (IFactoryBinder_5_t1497530694 *, const MethodInfo*))IFactoryBinder_5_ToFactory_TisIl2CppObject_m341380658_gshared)(__this, method)
// Zenject.BindingConditionSetter Zenject.IFactoryBinder`5<System.Object,System.Object,System.Object,System.Object,System.Object>::ToCustomFactory<System.Object,Zenject.Factory`5<System.Object,System.Object,System.Object,System.Object,System.Object>>()
#define IFactoryBinder_5_ToCustomFactory_TisIl2CppObject_TisFactory_5_t2495851293_m866245819(__this, method) ((  BindingConditionSetter_t259147722 * (*) (IFactoryBinder_5_t1497530694 *, const MethodInfo*))IFactoryBinder_5_ToCustomFactory_TisIl2CppObject_TisIl2CppObject_m1018422289_gshared)(__this, method)
// Zenject.BindingConditionSetter Zenject.IFactoryBinder`5<System.Object,System.Object,System.Object,System.Object,System.Object>::ToIFactory<System.Object>()
extern "C"  BindingConditionSetter_t259147722 * IFactoryBinder_5_ToIFactory_TisIl2CppObject_m3613289053_gshared (IFactoryBinder_5_t1497530694 * __this, const MethodInfo* method);
#define IFactoryBinder_5_ToIFactory_TisIl2CppObject_m3613289053(__this, method) ((  BindingConditionSetter_t259147722 * (*) (IFactoryBinder_5_t1497530694 *, const MethodInfo*))IFactoryBinder_5_ToIFactory_TisIl2CppObject_m3613289053_gshared)(__this, method)
// Zenject.IFactory`5<TParam1,TParam2,TParam3,TParam4,TContract> Zenject.IFactoryBinder`5<System.Object,System.Object,System.Object,System.Object,System.Object>::<ToIFactory`1>m__2A5<System.Object>(Zenject.InjectContext)
extern "C"  Il2CppObject* IFactoryBinder_5_U3CToIFactory_1U3Em__2A5_TisIl2CppObject_m1988390567_gshared (Il2CppObject * __this /* static, unused */, InjectContext_t3456483891 * p0, const MethodInfo* method);
#define IFactoryBinder_5_U3CToIFactory_1U3Em__2A5_TisIl2CppObject_m1988390567(__this /* static, unused */, p0, method) ((  Il2CppObject* (*) (Il2CppObject * /* static, unused */, InjectContext_t3456483891 *, const MethodInfo*))IFactoryBinder_5_U3CToIFactory_1U3Em__2A5_TisIl2CppObject_m1988390567_gshared)(__this /* static, unused */, p0, method)
// Zenject.BindingConditionSetter Zenject.IFactoryUntypedBinder`1<System.Object>::ToCustomFactory<System.Object>()
extern "C"  BindingConditionSetter_t259147722 * IFactoryUntypedBinder_1_ToCustomFactory_TisIl2CppObject_m188900896_gshared (IFactoryUntypedBinder_1_t486203833 * __this, const MethodInfo* method);
#define IFactoryUntypedBinder_1_ToCustomFactory_TisIl2CppObject_m188900896(__this, method) ((  BindingConditionSetter_t259147722 * (*) (IFactoryUntypedBinder_1_t486203833 *, const MethodInfo*))IFactoryUntypedBinder_1_ToCustomFactory_TisIl2CppObject_m188900896_gshared)(__this, method)
// Zenject.BinderGeneric`1<!!0> Zenject.DiContainer::Bind<Zenject.IFactoryUntyped`1<System.Object>>(System.String)
#define DiContainer_Bind_TisIFactoryUntyped_1_t1534379775_m2400808633(__this, p0, method) ((  BinderGeneric_1_t1217979071 * (*) (DiContainer_t2383114449 *, String_t*, const MethodInfo*))DiContainer_Bind_TisIl2CppObject_m288115653_gshared)(__this, p0, method)
// Zenject.BindingConditionSetter Zenject.BinderGeneric`1<Zenject.IFactoryUntyped`1<System.Object>>::ToTransient<System.Object>()
#define BinderGeneric_1_ToTransient_TisIl2CppObject_m714459684(__this, method) ((  BindingConditionSetter_t259147722 * (*) (BinderGeneric_1_t1217979071 *, const MethodInfo*))BinderGeneric_1_ToTransient_TisIl2CppObject_m4254792828_gshared)(__this, method)
// Zenject.BindingConditionSetter Zenject.IFactoryUntypedBinder`1<System.Object>::ToFactory<System.Object>()
extern "C"  BindingConditionSetter_t259147722 * IFactoryUntypedBinder_1_ToFactory_TisIl2CppObject_m3089524367_gshared (IFactoryUntypedBinder_1_t486203833 * __this, const MethodInfo* method);
#define IFactoryUntypedBinder_1_ToFactory_TisIl2CppObject_m3089524367(__this, method) ((  BindingConditionSetter_t259147722 * (*) (IFactoryUntypedBinder_1_t486203833 *, const MethodInfo*))IFactoryUntypedBinder_1_ToFactory_TisIl2CppObject_m3089524367_gshared)(__this, method)
// Zenject.BinderGeneric`1<!!0> Zenject.DiContainer::Bind<Zenject.IFactoryUntyped`1<System.Object>>()
#define DiContainer_Bind_TisIFactoryUntyped_1_t1534379775_m836629225(__this, method) ((  BinderGeneric_1_t1217979071 * (*) (DiContainer_t2383114449 *, const MethodInfo*))DiContainer_Bind_TisIl2CppObject_m3651981917_gshared)(__this, method)
// Zenject.BindingConditionSetter Zenject.BinderGeneric`1<System.Object>::ToSingle<System.Object>()
extern "C"  BindingConditionSetter_t259147722 * BinderGeneric_1_ToSingle_TisIl2CppObject_m3066969676_gshared (BinderGeneric_1_t520705716 * __this, const MethodInfo* method);
#define BinderGeneric_1_ToSingle_TisIl2CppObject_m3066969676(__this, method) ((  BindingConditionSetter_t259147722 * (*) (BinderGeneric_1_t520705716 *, const MethodInfo*))BinderGeneric_1_ToSingle_TisIl2CppObject_m3066969676_gshared)(__this, method)
// Zenject.BindingConditionSetter Zenject.BinderGeneric`1<Zenject.IFactoryUntyped`1<System.Object>>::ToSingle<Zenject.FactoryUntyped`2<System.Object,System.Object>>()
#define BinderGeneric_1_ToSingle_TisFactoryUntyped_2_t884050241_m1241864304(__this, method) ((  BindingConditionSetter_t259147722 * (*) (BinderGeneric_1_t1217979071 *, const MethodInfo*))BinderGeneric_1_ToSingle_TisIl2CppObject_m3066969676_gshared)(__this, method)
// Zenject.BindingConditionSetter Zenject.KeyedFactoryBase`2<System.Object,System.Object>::AddBindingInternal<System.Object>(Zenject.DiContainer,TKey)
extern "C"  BindingConditionSetter_t259147722 * KeyedFactoryBase_2_AddBindingInternal_TisIl2CppObject_m4111664247_gshared (Il2CppObject * __this /* static, unused */, DiContainer_t2383114449 * ___container0, Il2CppObject * ___key1, const MethodInfo* method);
#define KeyedFactoryBase_2_AddBindingInternal_TisIl2CppObject_m4111664247(__this /* static, unused */, ___container0, ___key1, method) ((  BindingConditionSetter_t259147722 * (*) (Il2CppObject * /* static, unused */, DiContainer_t2383114449 *, Il2CppObject *, const MethodInfo*))KeyedFactoryBase_2_AddBindingInternal_TisIl2CppObject_m4111664247_gshared)(__this /* static, unused */, ___container0, ___key1, method)
// Zenject.BinderGeneric`1<!!0> Zenject.DiContainer::Bind<ModestTree.Util.Tuple`2<System.Object,System.Type>>()
#define DiContainer_Bind_TisTuple_2_t231167918_m4046535438(__this, method) ((  BinderGeneric_1_t4209734510 * (*) (DiContainer_t2383114449 *, const MethodInfo*))DiContainer_Bind_TisIl2CppObject_m3651981917_gshared)(__this, method)
// ModestTree.Util.Tuple`2<!!0,!!1> ModestTree.Util.Tuple::New<System.Object,System.Object>(!!0,!!1)
extern "C"  Tuple_2_t2584011699 * Tuple_New_TisIl2CppObject_TisIl2CppObject_m24005828_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * p0, Il2CppObject * p1, const MethodInfo* method);
#define Tuple_New_TisIl2CppObject_TisIl2CppObject_m24005828(__this /* static, unused */, p0, p1, method) ((  Tuple_2_t2584011699 * (*) (Il2CppObject * /* static, unused */, Il2CppObject *, Il2CppObject *, const MethodInfo*))Tuple_New_TisIl2CppObject_TisIl2CppObject_m24005828_gshared)(__this /* static, unused */, p0, p1, method)
// ModestTree.Util.Tuple`2<!!0,!!1> ModestTree.Util.Tuple::New<System.Object,System.Type>(!!0,!!1)
#define Tuple_New_TisIl2CppObject_TisType_t_m3247516927(__this /* static, unused */, p0, p1, method) ((  Tuple_2_t231167918 * (*) (Il2CppObject * /* static, unused */, Il2CppObject *, Type_t *, const MethodInfo*))Tuple_New_TisIl2CppObject_TisIl2CppObject_m24005828_gshared)(__this /* static, unused */, p0, p1, method)
// Zenject.BindingConditionSetter Zenject.BinderGeneric`1<ModestTree.Util.Tuple`2<System.Object,System.Type>>::ToInstance<ModestTree.Util.Tuple`2<System.Object,System.Type>>(!!0)
#define BinderGeneric_1_ToInstance_TisTuple_2_t231167918_m1680719145(__this, p0, method) ((  BindingConditionSetter_t259147722 * (*) (BinderGeneric_1_t4209734510 *, Tuple_2_t231167918 *, const MethodInfo*))BinderGeneric_1_ToInstance_TisIl2CppObject_m1818222243_gshared)(__this, p0, method)
// !!0 Zenject.DiContainer::Instantiate<System.Object>(System.Object[])
extern "C"  Il2CppObject * DiContainer_Instantiate_TisIl2CppObject_m1682369513_gshared (DiContainer_t2383114449 * __this, ObjectU5BU5D_t11523773* p0, const MethodInfo* method);
#define DiContainer_Instantiate_TisIl2CppObject_m1682369513(__this, p0, method) ((  Il2CppObject * (*) (DiContainer_t2383114449 *, ObjectU5BU5D_t11523773*, const MethodInfo*))DiContainer_Instantiate_TisIl2CppObject_m1682369513_gshared)(__this, p0, method)
// !!0 Zenject.DiContainer::Resolve<System.Object>()
extern "C"  Il2CppObject * DiContainer_Resolve_TisIl2CppObject_m1270155875_gshared (DiContainer_t2383114449 * __this, const MethodInfo* method);
#define DiContainer_Resolve_TisIl2CppObject_m1270155875(__this, method) ((  Il2CppObject * (*) (DiContainer_t2383114449 *, const MethodInfo*))DiContainer_Resolve_TisIl2CppObject_m1270155875_gshared)(__this, method)
// !!0 Zenject.DiContainer::Resolve<Zenject.IFactory`1<System.Object>>()
#define DiContainer_Resolve_TisIFactory_1_t2124284520_m1436592726(__this, method) ((  Il2CppObject* (*) (DiContainer_t2383114449 *, const MethodInfo*))DiContainer_Resolve_TisIl2CppObject_m1270155875_gshared)(__this, method)
// !!0 Zenject.DiContainer::Resolve<Zenject.IFactory`2<System.Object,System.Object>>()
#define DiContainer_Resolve_TisIFactory_2_t972313871_m3024628681(__this, method) ((  Il2CppObject* (*) (DiContainer_t2383114449 *, const MethodInfo*))DiContainer_Resolve_TisIl2CppObject_m1270155875_gshared)(__this, method)
// !!0 Zenject.DiContainer::Resolve<Zenject.IFactory`3<System.Object,System.Object,System.Object>>()
#define DiContainer_Resolve_TisIFactory_3_t1619999162_m3362591292(__this, method) ((  Il2CppObject* (*) (DiContainer_t2383114449 *, const MethodInfo*))DiContainer_Resolve_TisIl2CppObject_m1270155875_gshared)(__this, method)
// !!0 Zenject.DiContainer::Resolve<Zenject.IFactory`4<System.Object,System.Object,System.Object,System.Object>>()
#define DiContainer_Resolve_TisIFactory_4_t1894544701_m2442374575(__this, method) ((  Il2CppObject* (*) (DiContainer_t2383114449 *, const MethodInfo*))DiContainer_Resolve_TisIl2CppObject_m1270155875_gshared)(__this, method)
// !!0 Zenject.DiContainer::Resolve<Zenject.IFactory`5<System.Object,System.Object,System.Object,System.Object,System.Object>>()
#define DiContainer_Resolve_TisIFactory_5_t363284204_m274222626(__this, method) ((  Il2CppObject* (*) (DiContainer_t2383114449 *, const MethodInfo*))DiContainer_Resolve_TisIl2CppObject_m1270155875_gshared)(__this, method)
// Zenject.IFactoryBinder`1<TContract> Zenject.DiContainer::BindIFactory<System.Object>()
extern "C"  IFactoryBinder_1_t1064970850 * DiContainer_BindIFactory_TisIl2CppObject_m638121348_gshared (DiContainer_t2383114449 * __this, const MethodInfo* method);
#define DiContainer_BindIFactory_TisIl2CppObject_m638121348(__this, method) ((  IFactoryBinder_1_t1064970850 * (*) (DiContainer_t2383114449 *, const MethodInfo*))DiContainer_BindIFactory_TisIl2CppObject_m638121348_gshared)(__this, method)
// Zenject.IFactoryBinder`1<!!0> Zenject.DiContainer::BindIFactory<System.Object>(System.String)
extern "C"  IFactoryBinder_1_t1064970850 * DiContainer_BindIFactory_TisIl2CppObject_m1974124542_gshared (DiContainer_t2383114449 * __this, String_t* p0, const MethodInfo* method);
#define DiContainer_BindIFactory_TisIl2CppObject_m1974124542(__this, p0, method) ((  IFactoryBinder_1_t1064970850 * (*) (DiContainer_t2383114449 *, String_t*, const MethodInfo*))DiContainer_BindIFactory_TisIl2CppObject_m1974124542_gshared)(__this, p0, method)
// Zenject.IFactoryBinder`1<TContract> Zenject.IBinder::BindIFactory<System.Object>()
extern "C"  IFactoryBinder_1_t1064970850 * IBinder_BindIFactory_TisIl2CppObject_m921646381_gshared (Il2CppObject * __this, const MethodInfo* method);
#define IBinder_BindIFactory_TisIl2CppObject_m921646381(__this, method) ((  IFactoryBinder_1_t1064970850 * (*) (Il2CppObject *, const MethodInfo*))IBinder_BindIFactory_TisIl2CppObject_m921646381_gshared)(__this, method)
// Zenject.IFactoryBinder`1<TContract> Zenject.IBinder::BindIFactory<System.Object>(System.String)
extern "C"  IFactoryBinder_1_t1064970850 * IBinder_BindIFactory_TisIl2CppObject_m982317813_gshared (Il2CppObject * __this, String_t* ___identifier0, const MethodInfo* method);
#define IBinder_BindIFactory_TisIl2CppObject_m982317813(__this, ___identifier0, method) ((  IFactoryBinder_1_t1064970850 * (*) (Il2CppObject *, String_t*, const MethodInfo*))IBinder_BindIFactory_TisIl2CppObject_m982317813_gshared)(__this, ___identifier0, method)
// Zenject.IFactoryBinder`2<TParam1,TContract> Zenject.DiContainer::BindIFactory<System.Object,System.Object>()
extern "C"  IFactoryBinder_2_t3921329133 * DiContainer_BindIFactory_TisIl2CppObject_TisIl2CppObject_m2904768223_gshared (DiContainer_t2383114449 * __this, const MethodInfo* method);
#define DiContainer_BindIFactory_TisIl2CppObject_TisIl2CppObject_m2904768223(__this, method) ((  IFactoryBinder_2_t3921329133 * (*) (DiContainer_t2383114449 *, const MethodInfo*))DiContainer_BindIFactory_TisIl2CppObject_TisIl2CppObject_m2904768223_gshared)(__this, method)
// Zenject.IFactoryBinder`2<!!0,!!1> Zenject.DiContainer::BindIFactory<System.Object,System.Object>(System.String)
extern "C"  IFactoryBinder_2_t3921329133 * DiContainer_BindIFactory_TisIl2CppObject_TisIl2CppObject_m1361429123_gshared (DiContainer_t2383114449 * __this, String_t* p0, const MethodInfo* method);
#define DiContainer_BindIFactory_TisIl2CppObject_TisIl2CppObject_m1361429123(__this, p0, method) ((  IFactoryBinder_2_t3921329133 * (*) (DiContainer_t2383114449 *, String_t*, const MethodInfo*))DiContainer_BindIFactory_TisIl2CppObject_TisIl2CppObject_m1361429123_gshared)(__this, p0, method)
// Zenject.IFactoryBinder`2<TParam1,TContract> Zenject.IBinder::BindIFactory<System.Object,System.Object>()
extern "C"  IFactoryBinder_2_t3921329133 * IBinder_BindIFactory_TisIl2CppObject_TisIl2CppObject_m3865162056_gshared (Il2CppObject * __this, const MethodInfo* method);
#define IBinder_BindIFactory_TisIl2CppObject_TisIl2CppObject_m3865162056(__this, method) ((  IFactoryBinder_2_t3921329133 * (*) (Il2CppObject *, const MethodInfo*))IBinder_BindIFactory_TisIl2CppObject_TisIl2CppObject_m3865162056_gshared)(__this, method)
// Zenject.IFactoryBinder`2<TParam1,TContract> Zenject.IBinder::BindIFactory<System.Object,System.Object>(System.String)
extern "C"  IFactoryBinder_2_t3921329133 * IBinder_BindIFactory_TisIl2CppObject_TisIl2CppObject_m4100473530_gshared (Il2CppObject * __this, String_t* ___identifier0, const MethodInfo* method);
#define IBinder_BindIFactory_TisIl2CppObject_TisIl2CppObject_m4100473530(__this, ___identifier0, method) ((  IFactoryBinder_2_t3921329133 * (*) (Il2CppObject *, String_t*, const MethodInfo*))IBinder_BindIFactory_TisIl2CppObject_TisIl2CppObject_m4100473530_gshared)(__this, ___identifier0, method)
// Zenject.IFactoryBinder`3<TParam1,TParam2,TContract> Zenject.DiContainer::BindIFactory<System.Object,System.Object,System.Object>()
extern "C"  IFactoryBinder_3_t153990564 * DiContainer_BindIFactory_TisIl2CppObject_TisIl2CppObject_TisIl2CppObject_m2649206011_gshared (DiContainer_t2383114449 * __this, const MethodInfo* method);
#define DiContainer_BindIFactory_TisIl2CppObject_TisIl2CppObject_TisIl2CppObject_m2649206011(__this, method) ((  IFactoryBinder_3_t153990564 * (*) (DiContainer_t2383114449 *, const MethodInfo*))DiContainer_BindIFactory_TisIl2CppObject_TisIl2CppObject_TisIl2CppObject_m2649206011_gshared)(__this, method)
// Zenject.IFactoryBinder`3<!!0,!!1,!!2> Zenject.DiContainer::BindIFactory<System.Object,System.Object,System.Object>(System.String)
extern "C"  IFactoryBinder_3_t153990564 * DiContainer_BindIFactory_TisIl2CppObject_TisIl2CppObject_TisIl2CppObject_m3217433063_gshared (DiContainer_t2383114449 * __this, String_t* p0, const MethodInfo* method);
#define DiContainer_BindIFactory_TisIl2CppObject_TisIl2CppObject_TisIl2CppObject_m3217433063(__this, p0, method) ((  IFactoryBinder_3_t153990564 * (*) (DiContainer_t2383114449 *, String_t*, const MethodInfo*))DiContainer_BindIFactory_TisIl2CppObject_TisIl2CppObject_TisIl2CppObject_m3217433063_gshared)(__this, p0, method)
// Zenject.IFactoryBinder`3<TParam1,TParam2,TContract> Zenject.IBinder::BindIFactory<System.Object,System.Object,System.Object>()
extern "C"  IFactoryBinder_3_t153990564 * IBinder_BindIFactory_TisIl2CppObject_TisIl2CppObject_TisIl2CppObject_m3281562276_gshared (Il2CppObject * __this, const MethodInfo* method);
#define IBinder_BindIFactory_TisIl2CppObject_TisIl2CppObject_TisIl2CppObject_m3281562276(__this, method) ((  IFactoryBinder_3_t153990564 * (*) (Il2CppObject *, const MethodInfo*))IBinder_BindIFactory_TisIl2CppObject_TisIl2CppObject_TisIl2CppObject_m3281562276_gshared)(__this, method)
// Zenject.IFactoryBinder`3<TParam1,TParam2,TContract> Zenject.IBinder::BindIFactory<System.Object,System.Object,System.Object>(System.String)
extern "C"  IFactoryBinder_3_t153990564 * IBinder_BindIFactory_TisIl2CppObject_TisIl2CppObject_TisIl2CppObject_m2747424478_gshared (Il2CppObject * __this, String_t* ___identifier0, const MethodInfo* method);
#define IBinder_BindIFactory_TisIl2CppObject_TisIl2CppObject_TisIl2CppObject_m2747424478(__this, ___identifier0, method) ((  IFactoryBinder_3_t153990564 * (*) (Il2CppObject *, String_t*, const MethodInfo*))IBinder_BindIFactory_TisIl2CppObject_TisIl2CppObject_TisIl2CppObject_m2747424478_gshared)(__this, ___identifier0, method)
// Zenject.IFactoryBinder`4<TParam1,TParam2,TParam3,TContract> Zenject.DiContainer::BindIFactory<System.Object,System.Object,System.Object,System.Object>()
extern "C"  IFactoryBinder_4_t2507049579 * DiContainer_BindIFactory_TisIl2CppObject_TisIl2CppObject_TisIl2CppObject_TisIl2CppObject_m3446394264_gshared (DiContainer_t2383114449 * __this, const MethodInfo* method);
#define DiContainer_BindIFactory_TisIl2CppObject_TisIl2CppObject_TisIl2CppObject_TisIl2CppObject_m3446394264(__this, method) ((  IFactoryBinder_4_t2507049579 * (*) (DiContainer_t2383114449 *, const MethodInfo*))DiContainer_BindIFactory_TisIl2CppObject_TisIl2CppObject_TisIl2CppObject_TisIl2CppObject_m3446394264_gshared)(__this, method)
// Zenject.IFactoryBinder`4<!!0,!!1,!!2,!!3> Zenject.DiContainer::BindIFactory<System.Object,System.Object,System.Object,System.Object>(System.String)
extern "C"  IFactoryBinder_4_t2507049579 * DiContainer_BindIFactory_TisIl2CppObject_TisIl2CppObject_TisIl2CppObject_TisIl2CppObject_m620406890_gshared (DiContainer_t2383114449 * __this, String_t* p0, const MethodInfo* method);
#define DiContainer_BindIFactory_TisIl2CppObject_TisIl2CppObject_TisIl2CppObject_TisIl2CppObject_m620406890(__this, p0, method) ((  IFactoryBinder_4_t2507049579 * (*) (DiContainer_t2383114449 *, String_t*, const MethodInfo*))DiContainer_BindIFactory_TisIl2CppObject_TisIl2CppObject_TisIl2CppObject_TisIl2CppObject_m620406890_gshared)(__this, p0, method)
// Zenject.IFactoryBinder`4<TParam1,TParam2,TParam3,TContract> Zenject.IBinder::BindIFactory<System.Object,System.Object,System.Object,System.Object>()
extern "C"  IFactoryBinder_4_t2507049579 * IBinder_BindIFactory_TisIl2CppObject_TisIl2CppObject_TisIl2CppObject_TisIl2CppObject_m2318044929_gshared (Il2CppObject * __this, const MethodInfo* method);
#define IBinder_BindIFactory_TisIl2CppObject_TisIl2CppObject_TisIl2CppObject_TisIl2CppObject_m2318044929(__this, method) ((  IFactoryBinder_4_t2507049579 * (*) (Il2CppObject *, const MethodInfo*))IBinder_BindIFactory_TisIl2CppObject_TisIl2CppObject_TisIl2CppObject_TisIl2CppObject_m2318044929_gshared)(__this, method)
// Zenject.IFactoryBinder`4<TParam1,TParam2,TParam3,TContract> Zenject.IBinder::BindIFactory<System.Object,System.Object,System.Object,System.Object>(System.String)
extern "C"  IFactoryBinder_4_t2507049579 * IBinder_BindIFactory_TisIl2CppObject_TisIl2CppObject_TisIl2CppObject_TisIl2CppObject_m1710569889_gshared (Il2CppObject * __this, String_t* ___identifier0, const MethodInfo* method);
#define IBinder_BindIFactory_TisIl2CppObject_TisIl2CppObject_TisIl2CppObject_TisIl2CppObject_m1710569889(__this, ___identifier0, method) ((  IFactoryBinder_4_t2507049579 * (*) (Il2CppObject *, String_t*, const MethodInfo*))IBinder_BindIFactory_TisIl2CppObject_TisIl2CppObject_TisIl2CppObject_TisIl2CppObject_m1710569889_gshared)(__this, ___identifier0, method)
// Zenject.IFactoryBinder`5<TParam1,TParam2,TParam3,TParam4,TContract> Zenject.DiContainer::BindIFactory<System.Object,System.Object,System.Object,System.Object,System.Object>()
extern "C"  IFactoryBinder_5_t1497530694 * DiContainer_BindIFactory_TisIl2CppObject_TisIl2CppObject_TisIl2CppObject_TisIl2CppObject_TisIl2CppObject_m3025374838_gshared (DiContainer_t2383114449 * __this, const MethodInfo* method);
#define DiContainer_BindIFactory_TisIl2CppObject_TisIl2CppObject_TisIl2CppObject_TisIl2CppObject_TisIl2CppObject_m3025374838(__this, method) ((  IFactoryBinder_5_t1497530694 * (*) (DiContainer_t2383114449 *, const MethodInfo*))DiContainer_BindIFactory_TisIl2CppObject_TisIl2CppObject_TisIl2CppObject_TisIl2CppObject_TisIl2CppObject_m3025374838_gshared)(__this, method)
// Zenject.IFactoryBinder`5<!!0,!!1,!!2,!!3,!!4> Zenject.DiContainer::BindIFactory<System.Object,System.Object,System.Object,System.Object,System.Object>(System.String)
extern "C"  IFactoryBinder_5_t1497530694 * DiContainer_BindIFactory_TisIl2CppObject_TisIl2CppObject_TisIl2CppObject_TisIl2CppObject_TisIl2CppObject_m587186252_gshared (DiContainer_t2383114449 * __this, String_t* p0, const MethodInfo* method);
#define DiContainer_BindIFactory_TisIl2CppObject_TisIl2CppObject_TisIl2CppObject_TisIl2CppObject_TisIl2CppObject_m587186252(__this, p0, method) ((  IFactoryBinder_5_t1497530694 * (*) (DiContainer_t2383114449 *, String_t*, const MethodInfo*))DiContainer_BindIFactory_TisIl2CppObject_TisIl2CppObject_TisIl2CppObject_TisIl2CppObject_TisIl2CppObject_m587186252_gshared)(__this, p0, method)
// Zenject.IFactoryBinder`5<TParam1,TParam2,TParam3,TParam4,TContract> Zenject.IBinder::BindIFactory<System.Object,System.Object,System.Object,System.Object,System.Object>()
extern "C"  IFactoryBinder_5_t1497530694 * IBinder_BindIFactory_TisIl2CppObject_TisIl2CppObject_TisIl2CppObject_TisIl2CppObject_TisIl2CppObject_m2892770335_gshared (Il2CppObject * __this, const MethodInfo* method);
#define IBinder_BindIFactory_TisIl2CppObject_TisIl2CppObject_TisIl2CppObject_TisIl2CppObject_TisIl2CppObject_m2892770335(__this, method) ((  IFactoryBinder_5_t1497530694 * (*) (Il2CppObject *, const MethodInfo*))IBinder_BindIFactory_TisIl2CppObject_TisIl2CppObject_TisIl2CppObject_TisIl2CppObject_TisIl2CppObject_m2892770335_gshared)(__this, method)
// Zenject.IFactoryBinder`5<TParam1,TParam2,TParam3,TParam4,TContract> Zenject.IBinder::BindIFactory<System.Object,System.Object,System.Object,System.Object,System.Object>(System.String)
extern "C"  IFactoryBinder_5_t1497530694 * IBinder_BindIFactory_TisIl2CppObject_TisIl2CppObject_TisIl2CppObject_TisIl2CppObject_TisIl2CppObject_m838360387_gshared (Il2CppObject * __this, String_t* ___identifier0, const MethodInfo* method);
#define IBinder_BindIFactory_TisIl2CppObject_TisIl2CppObject_TisIl2CppObject_TisIl2CppObject_TisIl2CppObject_m838360387(__this, ___identifier0, method) ((  IFactoryBinder_5_t1497530694 * (*) (Il2CppObject *, String_t*, const MethodInfo*))IBinder_BindIFactory_TisIl2CppObject_TisIl2CppObject_TisIl2CppObject_TisIl2CppObject_TisIl2CppObject_m838360387_gshared)(__this, ___identifier0, method)
// Zenject.IFactoryUntypedBinder`1<TContract> Zenject.DiContainer::BindIFactoryUntyped<System.Object>()
extern "C"  IFactoryUntypedBinder_1_t486203833 * DiContainer_BindIFactoryUntyped_TisIl2CppObject_m1378532874_gshared (DiContainer_t2383114449 * __this, const MethodInfo* method);
#define DiContainer_BindIFactoryUntyped_TisIl2CppObject_m1378532874(__this, method) ((  IFactoryUntypedBinder_1_t486203833 * (*) (DiContainer_t2383114449 *, const MethodInfo*))DiContainer_BindIFactoryUntyped_TisIl2CppObject_m1378532874_gshared)(__this, method)
// Zenject.IFactoryUntypedBinder`1<!!0> Zenject.DiContainer::BindIFactoryUntyped<System.Object>(System.String)
extern "C"  IFactoryUntypedBinder_1_t486203833 * DiContainer_BindIFactoryUntyped_TisIl2CppObject_m1187348792_gshared (DiContainer_t2383114449 * __this, String_t* p0, const MethodInfo* method);
#define DiContainer_BindIFactoryUntyped_TisIl2CppObject_m1187348792(__this, p0, method) ((  IFactoryUntypedBinder_1_t486203833 * (*) (DiContainer_t2383114449 *, String_t*, const MethodInfo*))DiContainer_BindIFactoryUntyped_TisIl2CppObject_m1187348792_gshared)(__this, p0, method)
// Zenject.IFactoryUntypedBinder`1<TContract> Zenject.IBinder::BindIFactoryUntyped<System.Object>()
extern "C"  IFactoryUntypedBinder_1_t486203833 * IBinder_BindIFactoryUntyped_TisIl2CppObject_m2787637441_gshared (Il2CppObject * __this, const MethodInfo* method);
#define IBinder_BindIFactoryUntyped_TisIl2CppObject_m2787637441(__this, method) ((  IFactoryUntypedBinder_1_t486203833 * (*) (Il2CppObject *, const MethodInfo*))IBinder_BindIFactoryUntyped_TisIl2CppObject_m2787637441_gshared)(__this, method)
// Zenject.IFactoryUntypedBinder`1<TContract> Zenject.IBinder::BindIFactoryUntyped<System.Object>(System.String)
extern "C"  IFactoryUntypedBinder_1_t486203833 * IBinder_BindIFactoryUntyped_TisIl2CppObject_m3407980513_gshared (Il2CppObject * __this, String_t* ___identifier0, const MethodInfo* method);
#define IBinder_BindIFactoryUntyped_TisIl2CppObject_m3407980513(__this, ___identifier0, method) ((  IFactoryUntypedBinder_1_t486203833 * (*) (Il2CppObject *, String_t*, const MethodInfo*))IBinder_BindIFactoryUntyped_TisIl2CppObject_m3407980513_gshared)(__this, ___identifier0, method)
// Zenject.ProviderBase Zenject.SingletonProviderMap::CreateProviderFromMethod<System.Object>(System.String,System.Func`2<Zenject.InjectContext,TConcrete>)
extern "C"  ProviderBase_t1627494391 * SingletonProviderMap_CreateProviderFromMethod_TisIl2CppObject_m4159376484_gshared (SingletonProviderMap_t1557411893 * __this, String_t* ___identifier0, Func_2_t2621245597 * ___method1, const MethodInfo* method);
#define SingletonProviderMap_CreateProviderFromMethod_TisIl2CppObject_m4159376484(__this, ___identifier0, ___method1, method) ((  ProviderBase_t1627494391 * (*) (SingletonProviderMap_t1557411893 *, String_t*, Func_2_t2621245597 *, const MethodInfo*))SingletonProviderMap_CreateProviderFromMethod_TisIl2CppObject_m4159376484_gshared)(__this, ___identifier0, ___method1, method)
// Zenject.SingletonLazyCreator Zenject.SingletonProviderMap::AddCreatorFromMethod<System.Object>(System.String,System.Func`2<Zenject.InjectContext,!!0>)
extern "C"  SingletonLazyCreator_t2762284194 * SingletonProviderMap_AddCreatorFromMethod_TisIl2CppObject_m2008774778_gshared (SingletonProviderMap_t1557411893 * __this, String_t* p0, Func_2_t2621245597 * p1, const MethodInfo* method);
#define SingletonProviderMap_AddCreatorFromMethod_TisIl2CppObject_m2008774778(__this, p0, p1, method) ((  SingletonLazyCreator_t2762284194 * (*) (SingletonProviderMap_t1557411893 *, String_t*, Func_2_t2621245597 *, const MethodInfo*))SingletonProviderMap_AddCreatorFromMethod_TisIl2CppObject_m2008774778_gshared)(__this, p0, p1, method)
// Zenject.TypeValuePair Zenject.InstantiateUtil::CreateTypePair<System.Object>(T)
extern "C"  TypeValuePair_t620932390 * InstantiateUtil_CreateTypePair_TisIl2CppObject_m1693637397_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * ___param0, const MethodInfo* method);
#define InstantiateUtil_CreateTypePair_TisIl2CppObject_m1693637397(__this /* static, unused */, ___param0, method) ((  TypeValuePair_t620932390 * (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))InstantiateUtil_CreateTypePair_TisIl2CppObject_m1693637397_gshared)(__this /* static, unused */, ___param0, method)
// ZergRush.IReactiveCollection`1<T> ZergRush.ReactiveCollectionExtension::Filter<System.Object>(ZergRush.IReactiveCollection`1<T>,System.Func`2<T,System.Boolean>)
extern "C"  Il2CppObject* ReactiveCollectionExtension_Filter_TisIl2CppObject_m68123642_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* ___coll0, Func_2_t1509682273 * ___predicate1, const MethodInfo* method);
#define ReactiveCollectionExtension_Filter_TisIl2CppObject_m68123642(__this /* static, unused */, ___coll0, ___predicate1, method) ((  Il2CppObject* (*) (Il2CppObject * /* static, unused */, Il2CppObject*, Func_2_t1509682273 *, const MethodInfo*))ReactiveCollectionExtension_Filter_TisIl2CppObject_m68123642_gshared)(__this /* static, unused */, ___coll0, ___predicate1, method)
// Zenject.BindingConditionSetter Zenject.BinderUntyped::ToSingleMethod<System.Object>(System.String,System.Func`2<Zenject.InjectContext,TConcrete>)
// Zenject.BindingConditionSetter Zenject.BinderUntyped::ToSingleMethod<System.Object>(System.String,System.Func`2<Zenject.InjectContext,TConcrete>)
extern "C"  BindingConditionSetter_t259147722 * BinderUntyped_ToSingleMethod_TisIl2CppObject_m255892173_gshared (BinderUntyped_t2430239676 * __this, String_t* ___concreteIdentifier0, Func_2_t2621245597 * ___method1, const MethodInfo* method)
{
	{
		String_t* L_0 = ___concreteIdentifier0;
		Func_2_t2621245597 * L_1 = ___method1;
		NullCheck((Binder_t3872662847 *)__this);
		BindingConditionSetter_t259147722 * L_2 = ((  BindingConditionSetter_t259147722 * (*) (Binder_t3872662847 *, String_t*, Func_2_t2621245597 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0)->method)((Binder_t3872662847 *)__this, (String_t*)L_0, (Func_2_t2621245597 *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0));
		return L_2;
	}
}
// Zenject.BindingConditionSetter Zenject.BinderUntyped::ToSingleMonoBehaviour<System.Object>(UnityEngine.GameObject)
// Zenject.BindingConditionSetter Zenject.BinderUntyped::ToSingleMonoBehaviour<System.Object>(UnityEngine.GameObject)
extern "C"  BindingConditionSetter_t259147722 * BinderUntyped_ToSingleMonoBehaviour_TisIl2CppObject_m4105677077_gshared (BinderUntyped_t2430239676 * __this, GameObject_t4012695102 * ___gameObject0, const MethodInfo* method)
{
	{
		GameObject_t4012695102 * L_0 = ___gameObject0;
		NullCheck((Binder_t3872662847 *)__this);
		BindingConditionSetter_t259147722 * L_1 = ((  BindingConditionSetter_t259147722 * (*) (Binder_t3872662847 *, GameObject_t4012695102 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0)->method)((Binder_t3872662847 *)__this, (GameObject_t4012695102 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0));
		return L_1;
	}
}
// Zenject.BindingConditionSetter Zenject.BinderUntyped::ToSinglePrefab<System.Object>(System.String,UnityEngine.GameObject)
// Zenject.BindingConditionSetter Zenject.BinderUntyped::ToSinglePrefab<System.Object>(System.String,UnityEngine.GameObject)
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern const uint32_t BinderUntyped_ToSinglePrefab_TisIl2CppObject_m3655072379_MetadataUsageId;
extern "C"  BindingConditionSetter_t259147722 * BinderUntyped_ToSinglePrefab_TisIl2CppObject_m3655072379_gshared (BinderUntyped_t2430239676 * __this, String_t* ___identifier0, GameObject_t4012695102 * ___prefab1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (BinderUntyped_ToSinglePrefab_TisIl2CppObject_m3655072379_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t1864875887 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->rgctx_data, 0)), /*hidden argument*/NULL);
		String_t* L_1 = ___identifier0;
		GameObject_t4012695102 * L_2 = ___prefab1;
		NullCheck((Binder_t3872662847 *)__this);
		BindingConditionSetter_t259147722 * L_3 = Binder_ToSinglePrefab_m3074881579((Binder_t3872662847 *)__this, (Type_t *)L_0, (String_t*)L_1, (GameObject_t4012695102 *)L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
// Zenject.BindingConditionSetter Zenject.BinderUntyped::ToSinglePrefab<System.Object>(UnityEngine.GameObject)
// Zenject.BindingConditionSetter Zenject.BinderUntyped::ToSinglePrefab<System.Object>(UnityEngine.GameObject)
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern const uint32_t BinderUntyped_ToSinglePrefab_TisIl2CppObject_m3636588023_MetadataUsageId;
extern "C"  BindingConditionSetter_t259147722 * BinderUntyped_ToSinglePrefab_TisIl2CppObject_m3636588023_gshared (BinderUntyped_t2430239676 * __this, GameObject_t4012695102 * ___prefab0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (BinderUntyped_ToSinglePrefab_TisIl2CppObject_m3636588023_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t1864875887 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->rgctx_data, 0)), /*hidden argument*/NULL);
		GameObject_t4012695102 * L_1 = ___prefab0;
		NullCheck((Binder_t3872662847 *)__this);
		BindingConditionSetter_t259147722 * L_2 = Binder_ToSinglePrefab_m3074881579((Binder_t3872662847 *)__this, (Type_t *)L_0, (String_t*)NULL, (GameObject_t4012695102 *)L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// Zenject.BindingConditionSetter Zenject.BinderUntyped::ToSinglePrefabResource<System.Object>(System.String)
// Zenject.BindingConditionSetter Zenject.BinderUntyped::ToSinglePrefabResource<System.Object>(System.String)
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern const uint32_t BinderUntyped_ToSinglePrefabResource_TisIl2CppObject_m2578279249_MetadataUsageId;
extern "C"  BindingConditionSetter_t259147722 * BinderUntyped_ToSinglePrefabResource_TisIl2CppObject_m2578279249_gshared (BinderUntyped_t2430239676 * __this, String_t* ___resourcePath0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (BinderUntyped_ToSinglePrefabResource_TisIl2CppObject_m2578279249_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t1864875887 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->rgctx_data, 0)), /*hidden argument*/NULL);
		String_t* L_1 = ___resourcePath0;
		NullCheck((Binder_t3872662847 *)__this);
		BindingConditionSetter_t259147722 * L_2 = Binder_ToSinglePrefabResource_m1273251201((Binder_t3872662847 *)__this, (Type_t *)L_0, (String_t*)NULL, (String_t*)L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// Zenject.BindingConditionSetter Zenject.BinderUntyped::ToSinglePrefabResource<System.Object>(System.String,System.String)
// Zenject.BindingConditionSetter Zenject.BinderUntyped::ToSinglePrefabResource<System.Object>(System.String,System.String)
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern const uint32_t BinderUntyped_ToSinglePrefabResource_TisIl2CppObject_m3678138829_MetadataUsageId;
extern "C"  BindingConditionSetter_t259147722 * BinderUntyped_ToSinglePrefabResource_TisIl2CppObject_m3678138829_gshared (BinderUntyped_t2430239676 * __this, String_t* ___identifier0, String_t* ___resourcePath1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (BinderUntyped_ToSinglePrefabResource_TisIl2CppObject_m3678138829_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t1864875887 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->rgctx_data, 0)), /*hidden argument*/NULL);
		String_t* L_1 = ___identifier0;
		String_t* L_2 = ___resourcePath1;
		NullCheck((Binder_t3872662847 *)__this);
		BindingConditionSetter_t259147722 * L_3 = Binder_ToSinglePrefabResource_m1273251201((Binder_t3872662847 *)__this, (Type_t *)L_0, (String_t*)L_1, (String_t*)L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
// Zenject.BindingConditionSetter Zenject.BinderUntyped::ToTransient<System.Object>()
// Zenject.BindingConditionSetter Zenject.BinderUntyped::ToTransient<System.Object>()
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern const uint32_t BinderUntyped_ToTransient_TisIl2CppObject_m1236544005_MetadataUsageId;
extern "C"  BindingConditionSetter_t259147722 * BinderUntyped_ToTransient_TisIl2CppObject_m1236544005_gshared (BinderUntyped_t2430239676 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (BinderUntyped_ToTransient_TisIl2CppObject_m1236544005_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t1864875887 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->rgctx_data, 0)), /*hidden argument*/NULL);
		NullCheck((Binder_t3872662847 *)__this);
		BindingConditionSetter_t259147722 * L_1 = Binder_ToTransient_m1128596195((Binder_t3872662847 *)__this, (Type_t *)L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// Zenject.BindingConditionSetter Zenject.BinderUntyped::ToTransientPrefab<System.Object>(UnityEngine.GameObject)
// Zenject.BindingConditionSetter Zenject.BinderUntyped::ToTransientPrefab<System.Object>(UnityEngine.GameObject)
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern const uint32_t BinderUntyped_ToTransientPrefab_TisIl2CppObject_m2159315865_MetadataUsageId;
extern "C"  BindingConditionSetter_t259147722 * BinderUntyped_ToTransientPrefab_TisIl2CppObject_m2159315865_gshared (BinderUntyped_t2430239676 * __this, GameObject_t4012695102 * ___prefab0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (BinderUntyped_ToTransientPrefab_TisIl2CppObject_m2159315865_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t1864875887 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->rgctx_data, 0)), /*hidden argument*/NULL);
		GameObject_t4012695102 * L_1 = ___prefab0;
		NullCheck((Binder_t3872662847 *)__this);
		BindingConditionSetter_t259147722 * L_2 = Binder_ToTransientPrefab_m2568629535((Binder_t3872662847 *)__this, (Type_t *)L_0, (GameObject_t4012695102 *)L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// Zenject.BindingConditionSetter Zenject.BinderUntyped::ToTransientPrefabResource<System.Object>(System.String)
// Zenject.BindingConditionSetter Zenject.BinderUntyped::ToTransientPrefabResource<System.Object>(System.String)
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern const uint32_t BinderUntyped_ToTransientPrefabResource_TisIl2CppObject_m1006604655_MetadataUsageId;
extern "C"  BindingConditionSetter_t259147722 * BinderUntyped_ToTransientPrefabResource_TisIl2CppObject_m1006604655_gshared (BinderUntyped_t2430239676 * __this, String_t* ___resourcePath0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (BinderUntyped_ToTransientPrefabResource_TisIl2CppObject_m1006604655_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t1864875887 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->rgctx_data, 0)), /*hidden argument*/NULL);
		String_t* L_1 = ___resourcePath0;
		NullCheck((Binder_t3872662847 *)__this);
		BindingConditionSetter_t259147722 * L_2 = Binder_ToTransientPrefabResource_m3139136909((Binder_t3872662847 *)__this, (Type_t *)L_0, (String_t*)L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// Zenject.BindingConditionSetter Zenject.BindScope::BindInstance<System.Object>(TContract)
// Zenject.BindingConditionSetter Zenject.BindScope::BindInstance<System.Object>(TContract)
extern "C"  BindingConditionSetter_t259147722 * BindScope_BindInstance_TisIl2CppObject_m2632817704_gshared (BindScope_t2945157996 * __this, Il2CppObject * ___obj0, const MethodInfo* method)
{
	{
		NullCheck((BindScope_t2945157996 *)__this);
		BinderGeneric_1_t520705716 * L_0 = ((  BinderGeneric_1_t520705716 * (*) (BindScope_t2945157996 *, String_t*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0)->method)((BindScope_t2945157996 *)__this, (String_t*)NULL, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0));
		Il2CppObject * L_1 = ___obj0;
		NullCheck((BinderGeneric_1_t520705716 *)L_0);
		BindingConditionSetter_t259147722 * L_2 = ((  BindingConditionSetter_t259147722 * (*) (BinderGeneric_1_t520705716 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1)->method)((BinderGeneric_1_t520705716 *)L_0, (Il2CppObject *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1));
		return L_2;
	}
}
// Zenject.BindingConditionSetter Zenject.DiContainer::BindGameObjectFactory<System.Object>(UnityEngine.GameObject)
// Zenject.BindingConditionSetter Zenject.DiContainer::BindGameObjectFactory<System.Object>(UnityEngine.GameObject)
extern Il2CppClass* ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern Il2CppClass* ZenjectBindException_t3550319704_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral532731782;
extern const uint32_t DiContainer_BindGameObjectFactory_TisIl2CppObject_m1857473477_MetadataUsageId;
extern "C"  BindingConditionSetter_t259147722 * DiContainer_BindGameObjectFactory_TisIl2CppObject_m1857473477_gshared (DiContainer_t2383114449 * __this, GameObject_t4012695102 * ___prefab0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DiContainer_BindGameObjectFactory_TisIl2CppObject_m1857473477_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	U3CBindGameObjectFactoryU3Ec__AnonStorey18D_1_t866492662 * V_0 = NULL;
	{
		U3CBindGameObjectFactoryU3Ec__AnonStorey18D_1_t866492662 * L_0 = (U3CBindGameObjectFactoryU3Ec__AnonStorey18D_1_t866492662 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->rgctx_data, 0));
		((  void (*) (U3CBindGameObjectFactoryU3Ec__AnonStorey18D_1_t866492662 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1)->method)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1));
		V_0 = (U3CBindGameObjectFactoryU3Ec__AnonStorey18D_1_t866492662 *)L_0;
		U3CBindGameObjectFactoryU3Ec__AnonStorey18D_1_t866492662 * L_1 = V_0;
		GameObject_t4012695102 * L_2 = ___prefab0;
		NullCheck(L_1);
		L_1->set_prefab_0(L_2);
		U3CBindGameObjectFactoryU3Ec__AnonStorey18D_1_t866492662 * L_3 = V_0;
		NullCheck(L_3);
		GameObject_t4012695102 * L_4 = (GameObject_t4012695102 *)L_3->get_prefab_0();
		bool L_5 = Object_op_Equality_m1690293457(NULL /*static, unused*/, (Object_t3878351788 *)L_4, (Object_t3878351788 *)NULL, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_0046;
		}
	}
	{
		ObjectU5BU5D_t11523773* L_6 = (ObjectU5BU5D_t11523773*)((ObjectU5BU5D_t11523773*)SZArrayNew(ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var, (uint32_t)1));
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_7 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t1864875887 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->rgctx_data, 2)), /*hidden argument*/NULL);
		String_t* L_8 = TypeExtensions_Name_m3754378430(NULL /*static, unused*/, (Type_t *)L_7, /*hidden argument*/NULL);
		NullCheck(L_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_6, 0);
		ArrayElementTypeCheck (L_6, L_8);
		(L_6)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)L_8);
		String_t* L_9 = MiscExtensions_Fmt_m1348433569(NULL /*static, unused*/, (String_t*)_stringLiteral532731782, (ObjectU5BU5D_t11523773*)L_6, /*hidden argument*/NULL);
		ZenjectBindException_t3550319704 * L_10 = (ZenjectBindException_t3550319704 *)il2cpp_codegen_object_new(ZenjectBindException_t3550319704_il2cpp_TypeInfo_var);
		ZenjectBindException__ctor_m3054227529(L_10, (String_t*)L_9, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_10);
	}

IL_0046:
	{
		NullCheck((DiContainer_t2383114449 *)__this);
		BinderGeneric_1_t520705716 * L_11 = GenericVirtFuncInvoker0< BinderGeneric_1_t520705716 * >::Invoke(IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 3), (DiContainer_t2383114449 *)__this);
		U3CBindGameObjectFactoryU3Ec__AnonStorey18D_1_t866492662 * L_12 = V_0;
		IntPtr_t L_13;
		L_13.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 4));
		Func_2_t2621245597 * L_14 = (Func_2_t2621245597 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->rgctx_data, 5));
		((  void (*) (Func_2_t2621245597 *, Il2CppObject *, IntPtr_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 6)->method)(L_14, (Il2CppObject *)L_12, (IntPtr_t)L_13, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 6));
		NullCheck((BinderGeneric_1_t520705716 *)L_11);
		BindingConditionSetter_t259147722 * L_15 = ((  BindingConditionSetter_t259147722 * (*) (BinderGeneric_1_t520705716 *, Func_2_t2621245597 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 7)->method)((BinderGeneric_1_t520705716 *)L_11, (Func_2_t2621245597 *)L_14, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 7));
		return L_15;
	}
}
// Zenject.BindingConditionSetter Zenject.DiContainer::BindInstance<System.Object>(System.String,TContract)
// Zenject.BindingConditionSetter Zenject.DiContainer::BindInstance<System.Object>(System.String,TContract)
extern "C"  BindingConditionSetter_t259147722 * DiContainer_BindInstance_TisIl2CppObject_m1662892425_gshared (DiContainer_t2383114449 * __this, String_t* ___identifier0, Il2CppObject * ___obj1, const MethodInfo* method)
{
	{
		String_t* L_0 = ___identifier0;
		NullCheck((DiContainer_t2383114449 *)__this);
		BinderGeneric_1_t520705716 * L_1 = GenericVirtFuncInvoker1< BinderGeneric_1_t520705716 *, String_t* >::Invoke(IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0), (DiContainer_t2383114449 *)__this, (String_t*)L_0);
		Il2CppObject * L_2 = ___obj1;
		NullCheck((BinderGeneric_1_t520705716 *)L_1);
		BindingConditionSetter_t259147722 * L_3 = ((  BindingConditionSetter_t259147722 * (*) (BinderGeneric_1_t520705716 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1)->method)((BinderGeneric_1_t520705716 *)L_1, (Il2CppObject *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1));
		return L_3;
	}
}
// Zenject.BindingConditionSetter Zenject.DiContainer::BindInstance<System.Object>(TContract)
// Zenject.BindingConditionSetter Zenject.DiContainer::BindInstance<System.Object>(TContract)
extern "C"  BindingConditionSetter_t259147722 * DiContainer_BindInstance_TisIl2CppObject_m3957247117_gshared (DiContainer_t2383114449 * __this, Il2CppObject * ___obj0, const MethodInfo* method)
{
	{
		NullCheck((DiContainer_t2383114449 *)__this);
		BinderGeneric_1_t520705716 * L_0 = GenericVirtFuncInvoker0< BinderGeneric_1_t520705716 * >::Invoke(IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0), (DiContainer_t2383114449 *)__this);
		Il2CppObject * L_1 = ___obj0;
		NullCheck((BinderGeneric_1_t520705716 *)L_0);
		BindingConditionSetter_t259147722 * L_2 = ((  BindingConditionSetter_t259147722 * (*) (BinderGeneric_1_t520705716 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1)->method)((BinderGeneric_1_t520705716 *)L_0, (Il2CppObject *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1));
		return L_2;
	}
}
// Zenject.BindingConditionSetter Zenject.IBinder::BindGameObjectFactory<System.Object>(UnityEngine.GameObject)
// Zenject.BindingConditionSetter Zenject.IBinder::BindInstance<System.Object>(System.String,TContract)
// Zenject.BindingConditionSetter Zenject.IBinder::BindInstance<System.Object>(TContract)
// Zenject.BindingConditionSetter Zenject.IFactoryBinder`1<System.Object>::ToCustomFactory<System.Object,System.Object>()
// Zenject.BindingConditionSetter Zenject.IFactoryBinder`1<System.Object>::ToCustomFactory<System.Object,System.Object>()
extern "C"  BindingConditionSetter_t259147722 * IFactoryBinder_1_ToCustomFactory_TisIl2CppObject_TisIl2CppObject_m1625714013_gshared (IFactoryBinder_1_t1064970850 * __this, const MethodInfo* method)
{
	{
		DiContainer_t2383114449 * L_0 = (DiContainer_t2383114449 *)__this->get__container_0();
		String_t* L_1 = (String_t*)__this->get__identifier_1();
		NullCheck((DiContainer_t2383114449 *)L_0);
		BinderGeneric_1_t1807883816 * L_2 = GenericVirtFuncInvoker1< BinderGeneric_1_t1807883816 *, String_t* >::Invoke(IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8), (DiContainer_t2383114449 *)L_0, (String_t*)L_1);
		IntPtr_t L_3;
		L_3.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0));
		Func_2_t3908423697 * L_4 = (Func_2_t3908423697 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10));
		((  void (*) (Func_2_t3908423697 *, Il2CppObject *, IntPtr_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 11)->method)(L_4, (Il2CppObject *)NULL, (IntPtr_t)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 11));
		NullCheck((BinderGeneric_1_t1807883816 *)L_2);
		BindingConditionSetter_t259147722 * L_5 = ((  BindingConditionSetter_t259147722 * (*) (BinderGeneric_1_t1807883816 *, Func_2_t3908423697 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 12)->method)((BinderGeneric_1_t1807883816 *)L_2, (Func_2_t3908423697 *)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 12));
		return L_5;
	}
}
// Zenject.BindingConditionSetter Zenject.IFactoryBinder`1<System.Object>::ToCustomFactory<System.Object>()
// Zenject.BindingConditionSetter Zenject.IFactoryBinder`1<System.Object>::ToCustomFactory<System.Object>()
extern "C"  BindingConditionSetter_t259147722 * IFactoryBinder_1_ToCustomFactory_TisIl2CppObject_m2469856719_gshared (IFactoryBinder_1_t1064970850 * __this, const MethodInfo* method)
{
	{
		DiContainer_t2383114449 * L_0 = (DiContainer_t2383114449 *)__this->get__container_0();
		String_t* L_1 = (String_t*)__this->get__identifier_1();
		NullCheck((DiContainer_t2383114449 *)L_0);
		BinderGeneric_1_t1807883816 * L_2 = GenericVirtFuncInvoker1< BinderGeneric_1_t1807883816 *, String_t* >::Invoke(IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8), (DiContainer_t2383114449 *)L_0, (String_t*)L_1);
		NullCheck((BinderGeneric_1_t1807883816 *)L_2);
		BindingConditionSetter_t259147722 * L_3 = ((  BindingConditionSetter_t259147722 * (*) (BinderGeneric_1_t1807883816 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0)->method)((BinderGeneric_1_t1807883816 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0));
		return L_3;
	}
}
// Zenject.BindingConditionSetter Zenject.IFactoryBinder`1<System.Object>::ToFactory<System.Object>()
// Zenject.BindingConditionSetter Zenject.IFactoryBinder`1<System.Object>::ToFactory<System.Object>()
extern "C"  BindingConditionSetter_t259147722 * IFactoryBinder_1_ToFactory_TisIl2CppObject_m2101097854_gshared (IFactoryBinder_1_t1064970850 * __this, const MethodInfo* method)
{
	{
		NullCheck((IFactoryBinder_1_t1064970850 *)__this);
		BindingConditionSetter_t259147722 * L_0 = ((  BindingConditionSetter_t259147722 * (*) (IFactoryBinder_1_t1064970850 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0)->method)((IFactoryBinder_1_t1064970850 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0));
		return L_0;
	}
}
// Zenject.BindingConditionSetter Zenject.IFactoryBinder`1<System.Object>::ToIFactory<System.Object>()
// Zenject.BindingConditionSetter Zenject.IFactoryBinder`1<System.Object>::ToIFactory<System.Object>()
extern "C"  BindingConditionSetter_t259147722 * IFactoryBinder_1_ToIFactory_TisIl2CppObject_m2329947281_gshared (IFactoryBinder_1_t1064970850 * __this, const MethodInfo* method)
{
	{
		DiContainer_t2383114449 * L_0 = (DiContainer_t2383114449 *)__this->get__container_0();
		String_t* L_1 = (String_t*)__this->get__identifier_1();
		NullCheck((DiContainer_t2383114449 *)L_0);
		BinderGeneric_1_t1807883816 * L_2 = GenericVirtFuncInvoker1< BinderGeneric_1_t1807883816 *, String_t* >::Invoke(IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8), (DiContainer_t2383114449 *)L_0, (String_t*)L_1);
		IntPtr_t L_3;
		L_3.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0));
		Func_2_t3908423697 * L_4 = (Func_2_t3908423697 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10));
		((  void (*) (Func_2_t3908423697 *, Il2CppObject *, IntPtr_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 11)->method)(L_4, (Il2CppObject *)NULL, (IntPtr_t)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 11));
		NullCheck((BinderGeneric_1_t1807883816 *)L_2);
		BindingConditionSetter_t259147722 * L_5 = ((  BindingConditionSetter_t259147722 * (*) (BinderGeneric_1_t1807883816 *, Func_2_t3908423697 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 12)->method)((BinderGeneric_1_t1807883816 *)L_2, (Func_2_t3908423697 *)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 12));
		return L_5;
	}
}
// Zenject.BindingConditionSetter Zenject.IFactoryBinder`2<System.Object,System.Object>::ToCustomFactory<System.Object,System.Object>()
// Zenject.BindingConditionSetter Zenject.IFactoryBinder`2<System.Object,System.Object>::ToCustomFactory<System.Object,System.Object>()
extern "C"  BindingConditionSetter_t259147722 * IFactoryBinder_2_ToCustomFactory_TisIl2CppObject_TisIl2CppObject_m3193428618_gshared (IFactoryBinder_2_t3921329133 * __this, const MethodInfo* method)
{
	{
		DiContainer_t2383114449 * L_0 = (DiContainer_t2383114449 *)__this->get__container_0();
		String_t* L_1 = (String_t*)__this->get__identifier_1();
		NullCheck((DiContainer_t2383114449 *)L_0);
		BinderGeneric_1_t655913167 * L_2 = GenericVirtFuncInvoker1< BinderGeneric_1_t655913167 *, String_t* >::Invoke(IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), (DiContainer_t2383114449 *)L_0, (String_t*)L_1);
		IntPtr_t L_3;
		L_3.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0));
		Func_2_t2756453048 * L_4 = (Func_2_t2756453048 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		((  void (*) (Func_2_t2756453048 *, Il2CppObject *, IntPtr_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->method)(L_4, (Il2CppObject *)NULL, (IntPtr_t)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		NullCheck((BinderGeneric_1_t655913167 *)L_2);
		BindingConditionSetter_t259147722 * L_5 = ((  BindingConditionSetter_t259147722 * (*) (BinderGeneric_1_t655913167 *, Func_2_t2756453048 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6)->method)((BinderGeneric_1_t655913167 *)L_2, (Func_2_t2756453048 *)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		return L_5;
	}
}
// Zenject.BindingConditionSetter Zenject.IFactoryBinder`2<System.Object,System.Object>::ToCustomFactory<System.Object>()
// Zenject.BindingConditionSetter Zenject.IFactoryBinder`2<System.Object,System.Object>::ToCustomFactory<System.Object>()
extern "C"  BindingConditionSetter_t259147722 * IFactoryBinder_2_ToCustomFactory_TisIl2CppObject_m222123452_gshared (IFactoryBinder_2_t3921329133 * __this, const MethodInfo* method)
{
	{
		DiContainer_t2383114449 * L_0 = (DiContainer_t2383114449 *)__this->get__container_0();
		String_t* L_1 = (String_t*)__this->get__identifier_1();
		NullCheck((DiContainer_t2383114449 *)L_0);
		BinderGeneric_1_t655913167 * L_2 = GenericVirtFuncInvoker1< BinderGeneric_1_t655913167 *, String_t* >::Invoke(IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), (DiContainer_t2383114449 *)L_0, (String_t*)L_1);
		NullCheck((BinderGeneric_1_t655913167 *)L_2);
		BindingConditionSetter_t259147722 * L_3 = ((  BindingConditionSetter_t259147722 * (*) (BinderGeneric_1_t655913167 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0)->method)((BinderGeneric_1_t655913167 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0));
		return L_3;
	}
}
// Zenject.BindingConditionSetter Zenject.IFactoryBinder`2<System.Object,System.Object>::ToFactory<System.Object>()
// Zenject.BindingConditionSetter Zenject.IFactoryBinder`2<System.Object,System.Object>::ToFactory<System.Object>()
extern "C"  BindingConditionSetter_t259147722 * IFactoryBinder_2_ToFactory_TisIl2CppObject_m3464772395_gshared (IFactoryBinder_2_t3921329133 * __this, const MethodInfo* method)
{
	{
		NullCheck((IFactoryBinder_2_t3921329133 *)__this);
		BindingConditionSetter_t259147722 * L_0 = ((  BindingConditionSetter_t259147722 * (*) (IFactoryBinder_2_t3921329133 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0)->method)((IFactoryBinder_2_t3921329133 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0));
		return L_0;
	}
}
// Zenject.BindingConditionSetter Zenject.IFactoryBinder`2<System.Object,System.Object>::ToIFactory<System.Object>()
// Zenject.BindingConditionSetter Zenject.IFactoryBinder`2<System.Object,System.Object>::ToIFactory<System.Object>()
extern "C"  BindingConditionSetter_t259147722 * IFactoryBinder_2_ToIFactory_TisIl2CppObject_m1654185092_gshared (IFactoryBinder_2_t3921329133 * __this, const MethodInfo* method)
{
	{
		DiContainer_t2383114449 * L_0 = (DiContainer_t2383114449 *)__this->get__container_0();
		String_t* L_1 = (String_t*)__this->get__identifier_1();
		NullCheck((DiContainer_t2383114449 *)L_0);
		BinderGeneric_1_t655913167 * L_2 = GenericVirtFuncInvoker1< BinderGeneric_1_t655913167 *, String_t* >::Invoke(IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), (DiContainer_t2383114449 *)L_0, (String_t*)L_1);
		IntPtr_t L_3;
		L_3.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0));
		Func_2_t2756453048 * L_4 = (Func_2_t2756453048 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		((  void (*) (Func_2_t2756453048 *, Il2CppObject *, IntPtr_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->method)(L_4, (Il2CppObject *)NULL, (IntPtr_t)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		NullCheck((BinderGeneric_1_t655913167 *)L_2);
		BindingConditionSetter_t259147722 * L_5 = ((  BindingConditionSetter_t259147722 * (*) (BinderGeneric_1_t655913167 *, Func_2_t2756453048 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6)->method)((BinderGeneric_1_t655913167 *)L_2, (Func_2_t2756453048 *)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		return L_5;
	}
}
// Zenject.BindingConditionSetter Zenject.IFactoryBinder`3<System.Object,System.Object,System.Object>::ToCustomFactory<System.Object,System.Object>()
// Zenject.BindingConditionSetter Zenject.IFactoryBinder`3<System.Object,System.Object,System.Object>::ToCustomFactory<System.Object,System.Object>()
extern "C"  BindingConditionSetter_t259147722 * IFactoryBinder_3_ToCustomFactory_TisIl2CppObject_TisIl2CppObject_m1610445495_gshared (IFactoryBinder_3_t153990564 * __this, const MethodInfo* method)
{
	{
		DiContainer_t2383114449 * L_0 = (DiContainer_t2383114449 *)__this->get__container_0();
		String_t* L_1 = (String_t*)__this->get__identifier_1();
		NullCheck((DiContainer_t2383114449 *)L_0);
		BinderGeneric_1_t1303598458 * L_2 = GenericVirtFuncInvoker1< BinderGeneric_1_t1303598458 *, String_t* >::Invoke(IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), (DiContainer_t2383114449 *)L_0, (String_t*)L_1);
		IntPtr_t L_3;
		L_3.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0));
		Func_2_t3404138339 * L_4 = (Func_2_t3404138339 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		((  void (*) (Func_2_t3404138339 *, Il2CppObject *, IntPtr_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->method)(L_4, (Il2CppObject *)NULL, (IntPtr_t)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		NullCheck((BinderGeneric_1_t1303598458 *)L_2);
		BindingConditionSetter_t259147722 * L_5 = ((  BindingConditionSetter_t259147722 * (*) (BinderGeneric_1_t1303598458 *, Func_2_t3404138339 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6)->method)((BinderGeneric_1_t1303598458 *)L_2, (Func_2_t3404138339 *)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		return L_5;
	}
}
// Zenject.BindingConditionSetter Zenject.IFactoryBinder`3<System.Object,System.Object,System.Object>::ToCustomFactory<System.Object>()
// Zenject.BindingConditionSetter Zenject.IFactoryBinder`3<System.Object,System.Object,System.Object>::ToCustomFactory<System.Object>()
extern "C"  BindingConditionSetter_t259147722 * IFactoryBinder_3_ToCustomFactory_TisIl2CppObject_m89067689_gshared (IFactoryBinder_3_t153990564 * __this, const MethodInfo* method)
{
	{
		DiContainer_t2383114449 * L_0 = (DiContainer_t2383114449 *)__this->get__container_0();
		String_t* L_1 = (String_t*)__this->get__identifier_1();
		NullCheck((DiContainer_t2383114449 *)L_0);
		BinderGeneric_1_t1303598458 * L_2 = GenericVirtFuncInvoker1< BinderGeneric_1_t1303598458 *, String_t* >::Invoke(IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), (DiContainer_t2383114449 *)L_0, (String_t*)L_1);
		NullCheck((BinderGeneric_1_t1303598458 *)L_2);
		BindingConditionSetter_t259147722 * L_3 = ((  BindingConditionSetter_t259147722 * (*) (BinderGeneric_1_t1303598458 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0)->method)((BinderGeneric_1_t1303598458 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0));
		return L_3;
	}
}
// Zenject.BindingConditionSetter Zenject.IFactoryBinder`3<System.Object,System.Object,System.Object>::ToFactory<System.Object>()
// Zenject.BindingConditionSetter Zenject.IFactoryBinder`3<System.Object,System.Object,System.Object>::ToFactory<System.Object>()
extern "C"  BindingConditionSetter_t259147722 * IFactoryBinder_3_ToFactory_TisIl2CppObject_m964946904_gshared (IFactoryBinder_3_t153990564 * __this, const MethodInfo* method)
{
	{
		NullCheck((IFactoryBinder_3_t153990564 *)__this);
		BindingConditionSetter_t259147722 * L_0 = ((  BindingConditionSetter_t259147722 * (*) (IFactoryBinder_3_t153990564 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0)->method)((IFactoryBinder_3_t153990564 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0));
		return L_0;
	}
}
// Zenject.BindingConditionSetter Zenject.IFactoryBinder`3<System.Object,System.Object,System.Object>::ToIFactory<System.Object>()
// Zenject.BindingConditionSetter Zenject.IFactoryBinder`3<System.Object,System.Object,System.Object>::ToIFactory<System.Object>()
extern "C"  BindingConditionSetter_t259147722 * IFactoryBinder_3_ToIFactory_TisIl2CppObject_m1469006199_gshared (IFactoryBinder_3_t153990564 * __this, const MethodInfo* method)
{
	{
		DiContainer_t2383114449 * L_0 = (DiContainer_t2383114449 *)__this->get__container_0();
		String_t* L_1 = (String_t*)__this->get__identifier_1();
		NullCheck((DiContainer_t2383114449 *)L_0);
		BinderGeneric_1_t1303598458 * L_2 = GenericVirtFuncInvoker1< BinderGeneric_1_t1303598458 *, String_t* >::Invoke(IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), (DiContainer_t2383114449 *)L_0, (String_t*)L_1);
		IntPtr_t L_3;
		L_3.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0));
		Func_2_t3404138339 * L_4 = (Func_2_t3404138339 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		((  void (*) (Func_2_t3404138339 *, Il2CppObject *, IntPtr_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->method)(L_4, (Il2CppObject *)NULL, (IntPtr_t)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		NullCheck((BinderGeneric_1_t1303598458 *)L_2);
		BindingConditionSetter_t259147722 * L_5 = ((  BindingConditionSetter_t259147722 * (*) (BinderGeneric_1_t1303598458 *, Func_2_t3404138339 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6)->method)((BinderGeneric_1_t1303598458 *)L_2, (Func_2_t3404138339 *)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		return L_5;
	}
}
// Zenject.BindingConditionSetter Zenject.IFactoryBinder`4<System.Object,System.Object,System.Object,System.Object>::ToCustomFactory<System.Object,System.Object>()
// Zenject.BindingConditionSetter Zenject.IFactoryBinder`4<System.Object,System.Object,System.Object,System.Object>::ToCustomFactory<System.Object,System.Object>()
extern "C"  BindingConditionSetter_t259147722 * IFactoryBinder_4_ToCustomFactory_TisIl2CppObject_TisIl2CppObject_m1037100516_gshared (IFactoryBinder_4_t2507049579 * __this, const MethodInfo* method)
{
	{
		DiContainer_t2383114449 * L_0 = (DiContainer_t2383114449 *)__this->get__container_0();
		String_t* L_1 = (String_t*)__this->get__identifier_1();
		NullCheck((DiContainer_t2383114449 *)L_0);
		BinderGeneric_1_t1578143997 * L_2 = GenericVirtFuncInvoker1< BinderGeneric_1_t1578143997 *, String_t* >::Invoke(IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), (DiContainer_t2383114449 *)L_0, (String_t*)L_1);
		IntPtr_t L_3;
		L_3.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0));
		Func_2_t3678683878 * L_4 = (Func_2_t3678683878 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		((  void (*) (Func_2_t3678683878 *, Il2CppObject *, IntPtr_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->method)(L_4, (Il2CppObject *)NULL, (IntPtr_t)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		NullCheck((BinderGeneric_1_t1578143997 *)L_2);
		BindingConditionSetter_t259147722 * L_5 = ((  BindingConditionSetter_t259147722 * (*) (BinderGeneric_1_t1578143997 *, Func_2_t3678683878 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6)->method)((BinderGeneric_1_t1578143997 *)L_2, (Func_2_t3678683878 *)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		return L_5;
	}
}
// Zenject.BindingConditionSetter Zenject.IFactoryBinder`4<System.Object,System.Object,System.Object,System.Object>::ToCustomFactory<System.Object>()
// Zenject.BindingConditionSetter Zenject.IFactoryBinder`4<System.Object,System.Object,System.Object,System.Object>::ToCustomFactory<System.Object>()
extern "C"  BindingConditionSetter_t259147722 * IFactoryBinder_4_ToCustomFactory_TisIl2CppObject_m2367284886_gshared (IFactoryBinder_4_t2507049579 * __this, const MethodInfo* method)
{
	{
		DiContainer_t2383114449 * L_0 = (DiContainer_t2383114449 *)__this->get__container_0();
		String_t* L_1 = (String_t*)__this->get__identifier_1();
		NullCheck((DiContainer_t2383114449 *)L_0);
		BinderGeneric_1_t1578143997 * L_2 = GenericVirtFuncInvoker1< BinderGeneric_1_t1578143997 *, String_t* >::Invoke(IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), (DiContainer_t2383114449 *)L_0, (String_t*)L_1);
		NullCheck((BinderGeneric_1_t1578143997 *)L_2);
		BindingConditionSetter_t259147722 * L_3 = ((  BindingConditionSetter_t259147722 * (*) (BinderGeneric_1_t1578143997 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0)->method)((BinderGeneric_1_t1578143997 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0));
		return L_3;
	}
}
// Zenject.BindingConditionSetter Zenject.IFactoryBinder`4<System.Object,System.Object,System.Object,System.Object>::ToFactory<System.Object>()
// Zenject.BindingConditionSetter Zenject.IFactoryBinder`4<System.Object,System.Object,System.Object,System.Object>::ToFactory<System.Object>()
extern "C"  BindingConditionSetter_t259147722 * IFactoryBinder_4_ToFactory_TisIl2CppObject_m1340929925_gshared (IFactoryBinder_4_t2507049579 * __this, const MethodInfo* method)
{
	{
		NullCheck((IFactoryBinder_4_t2507049579 *)__this);
		BindingConditionSetter_t259147722 * L_0 = ((  BindingConditionSetter_t259147722 * (*) (IFactoryBinder_4_t2507049579 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0)->method)((IFactoryBinder_4_t2507049579 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0));
		return L_0;
	}
}
// Zenject.BindingConditionSetter Zenject.IFactoryBinder`4<System.Object,System.Object,System.Object,System.Object>::ToIFactory<System.Object>()
// Zenject.BindingConditionSetter Zenject.IFactoryBinder`4<System.Object,System.Object,System.Object,System.Object>::ToIFactory<System.Object>()
extern "C"  BindingConditionSetter_t259147722 * IFactoryBinder_4_ToIFactory_TisIl2CppObject_m239577962_gshared (IFactoryBinder_4_t2507049579 * __this, const MethodInfo* method)
{
	{
		DiContainer_t2383114449 * L_0 = (DiContainer_t2383114449 *)__this->get__container_0();
		String_t* L_1 = (String_t*)__this->get__identifier_1();
		NullCheck((DiContainer_t2383114449 *)L_0);
		BinderGeneric_1_t1578143997 * L_2 = GenericVirtFuncInvoker1< BinderGeneric_1_t1578143997 *, String_t* >::Invoke(IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), (DiContainer_t2383114449 *)L_0, (String_t*)L_1);
		IntPtr_t L_3;
		L_3.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0));
		Func_2_t3678683878 * L_4 = (Func_2_t3678683878 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		((  void (*) (Func_2_t3678683878 *, Il2CppObject *, IntPtr_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->method)(L_4, (Il2CppObject *)NULL, (IntPtr_t)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		NullCheck((BinderGeneric_1_t1578143997 *)L_2);
		BindingConditionSetter_t259147722 * L_5 = ((  BindingConditionSetter_t259147722 * (*) (BinderGeneric_1_t1578143997 *, Func_2_t3678683878 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6)->method)((BinderGeneric_1_t1578143997 *)L_2, (Func_2_t3678683878 *)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		return L_5;
	}
}
// Zenject.BindingConditionSetter Zenject.IFactoryBinder`5<System.Object,System.Object,System.Object,System.Object,System.Object>::ToCustomFactory<System.Object,System.Object>()
// Zenject.BindingConditionSetter Zenject.IFactoryBinder`5<System.Object,System.Object,System.Object,System.Object,System.Object>::ToCustomFactory<System.Object,System.Object>()
extern "C"  BindingConditionSetter_t259147722 * IFactoryBinder_5_ToCustomFactory_TisIl2CppObject_TisIl2CppObject_m1018422289_gshared (IFactoryBinder_5_t1497530694 * __this, const MethodInfo* method)
{
	{
		DiContainer_t2383114449 * L_0 = (DiContainer_t2383114449 *)__this->get__container_0();
		String_t* L_1 = (String_t*)__this->get__identifier_1();
		NullCheck((DiContainer_t2383114449 *)L_0);
		BinderGeneric_1_t46883500 * L_2 = GenericVirtFuncInvoker1< BinderGeneric_1_t46883500 *, String_t* >::Invoke(IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), (DiContainer_t2383114449 *)L_0, (String_t*)L_1);
		IntPtr_t L_3;
		L_3.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0));
		Func_2_t2147423381 * L_4 = (Func_2_t2147423381 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		((  void (*) (Func_2_t2147423381 *, Il2CppObject *, IntPtr_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->method)(L_4, (Il2CppObject *)NULL, (IntPtr_t)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		NullCheck((BinderGeneric_1_t46883500 *)L_2);
		BindingConditionSetter_t259147722 * L_5 = ((  BindingConditionSetter_t259147722 * (*) (BinderGeneric_1_t46883500 *, Func_2_t2147423381 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6)->method)((BinderGeneric_1_t46883500 *)L_2, (Func_2_t2147423381 *)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		return L_5;
	}
}
// Zenject.BindingConditionSetter Zenject.IFactoryBinder`5<System.Object,System.Object,System.Object,System.Object,System.Object>::ToCustomFactory<System.Object>()
// Zenject.BindingConditionSetter Zenject.IFactoryBinder`5<System.Object,System.Object,System.Object,System.Object,System.Object>::ToCustomFactory<System.Object>()
extern "C"  BindingConditionSetter_t259147722 * IFactoryBinder_5_ToCustomFactory_TisIl2CppObject_m3644032899_gshared (IFactoryBinder_5_t1497530694 * __this, const MethodInfo* method)
{
	{
		DiContainer_t2383114449 * L_0 = (DiContainer_t2383114449 *)__this->get__container_0();
		String_t* L_1 = (String_t*)__this->get__identifier_1();
		NullCheck((DiContainer_t2383114449 *)L_0);
		BinderGeneric_1_t46883500 * L_2 = GenericVirtFuncInvoker1< BinderGeneric_1_t46883500 *, String_t* >::Invoke(IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), (DiContainer_t2383114449 *)L_0, (String_t*)L_1);
		NullCheck((BinderGeneric_1_t46883500 *)L_2);
		BindingConditionSetter_t259147722 * L_3 = ((  BindingConditionSetter_t259147722 * (*) (BinderGeneric_1_t46883500 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0)->method)((BinderGeneric_1_t46883500 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0));
		return L_3;
	}
}
// Zenject.BindingConditionSetter Zenject.IFactoryBinder`5<System.Object,System.Object,System.Object,System.Object,System.Object>::ToFactory<System.Object>()
// Zenject.BindingConditionSetter Zenject.IFactoryBinder`5<System.Object,System.Object,System.Object,System.Object,System.Object>::ToFactory<System.Object>()
extern "C"  BindingConditionSetter_t259147722 * IFactoryBinder_5_ToFactory_TisIl2CppObject_m341380658_gshared (IFactoryBinder_5_t1497530694 * __this, const MethodInfo* method)
{
	{
		NullCheck((IFactoryBinder_5_t1497530694 *)__this);
		BindingConditionSetter_t259147722 * L_0 = ((  BindingConditionSetter_t259147722 * (*) (IFactoryBinder_5_t1497530694 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0)->method)((IFactoryBinder_5_t1497530694 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0));
		return L_0;
	}
}
// Zenject.BindingConditionSetter Zenject.IFactoryBinder`5<System.Object,System.Object,System.Object,System.Object,System.Object>::ToIFactory<System.Object>()
// Zenject.BindingConditionSetter Zenject.IFactoryBinder`5<System.Object,System.Object,System.Object,System.Object,System.Object>::ToIFactory<System.Object>()
extern "C"  BindingConditionSetter_t259147722 * IFactoryBinder_5_ToIFactory_TisIl2CppObject_m3613289053_gshared (IFactoryBinder_5_t1497530694 * __this, const MethodInfo* method)
{
	{
		DiContainer_t2383114449 * L_0 = (DiContainer_t2383114449 *)__this->get__container_0();
		String_t* L_1 = (String_t*)__this->get__identifier_1();
		NullCheck((DiContainer_t2383114449 *)L_0);
		BinderGeneric_1_t46883500 * L_2 = GenericVirtFuncInvoker1< BinderGeneric_1_t46883500 *, String_t* >::Invoke(IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), (DiContainer_t2383114449 *)L_0, (String_t*)L_1);
		IntPtr_t L_3;
		L_3.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0));
		Func_2_t2147423381 * L_4 = (Func_2_t2147423381 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		((  void (*) (Func_2_t2147423381 *, Il2CppObject *, IntPtr_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->method)(L_4, (Il2CppObject *)NULL, (IntPtr_t)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		NullCheck((BinderGeneric_1_t46883500 *)L_2);
		BindingConditionSetter_t259147722 * L_5 = ((  BindingConditionSetter_t259147722 * (*) (BinderGeneric_1_t46883500 *, Func_2_t2147423381 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6)->method)((BinderGeneric_1_t46883500 *)L_2, (Func_2_t2147423381 *)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		return L_5;
	}
}
// Zenject.BindingConditionSetter Zenject.IFactoryUntypedBinder`1<System.Object>::ToCustomFactory<System.Object>()
// Zenject.BindingConditionSetter Zenject.IFactoryUntypedBinder`1<System.Object>::ToCustomFactory<System.Object>()
extern "C"  BindingConditionSetter_t259147722 * IFactoryUntypedBinder_1_ToCustomFactory_TisIl2CppObject_m188900896_gshared (IFactoryUntypedBinder_1_t486203833 * __this, const MethodInfo* method)
{
	{
		DiContainer_t2383114449 * L_0 = (DiContainer_t2383114449 *)__this->get__container_0();
		String_t* L_1 = (String_t*)__this->get__identifier_1();
		NullCheck((DiContainer_t2383114449 *)L_0);
		BinderGeneric_1_t1217979071 * L_2 = GenericVirtFuncInvoker1< BinderGeneric_1_t1217979071 *, String_t* >::Invoke(IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 13), (DiContainer_t2383114449 *)L_0, (String_t*)L_1);
		NullCheck((BinderGeneric_1_t1217979071 *)L_2);
		BindingConditionSetter_t259147722 * L_3 = ((  BindingConditionSetter_t259147722 * (*) (BinderGeneric_1_t1217979071 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0)->method)((BinderGeneric_1_t1217979071 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0));
		return L_3;
	}
}
// Zenject.BindingConditionSetter Zenject.IFactoryUntypedBinder`1<System.Object>::ToFactory<System.Object>()
// Zenject.BindingConditionSetter Zenject.IFactoryUntypedBinder`1<System.Object>::ToFactory<System.Object>()
extern "C"  BindingConditionSetter_t259147722 * IFactoryUntypedBinder_1_ToFactory_TisIl2CppObject_m3089524367_gshared (IFactoryUntypedBinder_1_t486203833 * __this, const MethodInfo* method)
{
	{
		DiContainer_t2383114449 * L_0 = (DiContainer_t2383114449 *)__this->get__container_0();
		NullCheck((DiContainer_t2383114449 *)L_0);
		BinderGeneric_1_t1217979071 * L_1 = GenericVirtFuncInvoker0< BinderGeneric_1_t1217979071 * >::Invoke(IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8), (DiContainer_t2383114449 *)L_0);
		NullCheck((BinderGeneric_1_t1217979071 *)L_1);
		BindingConditionSetter_t259147722 * L_2 = ((  BindingConditionSetter_t259147722 * (*) (BinderGeneric_1_t1217979071 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0)->method)((BinderGeneric_1_t1217979071 *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0));
		return L_2;
	}
}
// Zenject.BindingConditionSetter Zenject.KeyedFactoryBase`2<System.Object,System.Object>::AddBindingInternal<System.Object>(Zenject.DiContainer,TKey)
// Zenject.BindingConditionSetter Zenject.KeyedFactoryBase`2<System.Object,System.Object>::AddBindingInternal<System.Object>(Zenject.DiContainer,TKey)
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern const uint32_t KeyedFactoryBase_2_AddBindingInternal_TisIl2CppObject_m4111664247_MetadataUsageId;
extern "C"  BindingConditionSetter_t259147722 * KeyedFactoryBase_2_AddBindingInternal_TisIl2CppObject_m4111664247_gshared (Il2CppObject * __this /* static, unused */, DiContainer_t2383114449 * ___container0, Il2CppObject * ___key1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (KeyedFactoryBase_2_AddBindingInternal_TisIl2CppObject_m4111664247_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		DiContainer_t2383114449 * L_0 = ___container0;
		NullCheck((DiContainer_t2383114449 *)L_0);
		BinderGeneric_1_t4209734510 * L_1 = GenericVirtFuncInvoker0< BinderGeneric_1_t4209734510 * >::Invoke(IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 25), (DiContainer_t2383114449 *)L_0);
		Il2CppObject * L_2 = ___key1;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_3 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t1864875887 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->rgctx_data, 0)), /*hidden argument*/NULL);
		Tuple_2_t231167918 * L_4 = ((  Tuple_2_t231167918 * (*) (Il2CppObject * /* static, unused */, Il2CppObject *, Type_t *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 26)->method)(NULL /*static, unused*/, (Il2CppObject *)L_2, (Type_t *)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 26));
		NullCheck((BinderGeneric_1_t4209734510 *)L_1);
		BindingConditionSetter_t259147722 * L_5 = ((  BindingConditionSetter_t259147722 * (*) (BinderGeneric_1_t4209734510 *, Tuple_2_t231167918 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 27)->method)((BinderGeneric_1_t4209734510 *)L_1, (Tuple_2_t231167918 *)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 27));
		return L_5;
	}
}
// Zenject.IFactory`1<TContract> Zenject.IFactoryBinder`1<System.Object>::<ToCustomFactory`2>m__296<System.Object,System.Object>(Zenject.InjectContext)
// Zenject.IFactory`1<TContract> Zenject.IFactoryBinder`1<System.Object>::<ToCustomFactory`2>m__296<System.Object,System.Object>(Zenject.InjectContext)
extern Il2CppClass* ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var;
extern const uint32_t IFactoryBinder_1_U3CToCustomFactory_2U3Em__296_TisIl2CppObject_TisIl2CppObject_m3882463147_MetadataUsageId;
extern "C"  Il2CppObject* IFactoryBinder_1_U3CToCustomFactory_2U3Em__296_TisIl2CppObject_TisIl2CppObject_m3882463147_gshared (Il2CppObject * __this /* static, unused */, InjectContext_t3456483891 * ___c0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (IFactoryBinder_1_U3CToCustomFactory_2U3Em__296_TisIl2CppObject_TisIl2CppObject_m3882463147_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		InjectContext_t3456483891 * L_0 = ___c0;
		NullCheck(L_0);
		DiContainer_t2383114449 * L_1 = (DiContainer_t2383114449 *)L_0->get_Container_9();
		NullCheck((DiContainer_t2383114449 *)L_1);
		Il2CppObject * L_2 = GenericVirtFuncInvoker1< Il2CppObject *, ObjectU5BU5D_t11523773* >::Invoke(IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0), (DiContainer_t2383114449 *)L_1, (ObjectU5BU5D_t11523773*)((ObjectU5BU5D_t11523773*)SZArrayNew(ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var, (uint32_t)0)));
		FactoryNested_2_t2234585871 * L_3 = (FactoryNested_2_t2234585871 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->rgctx_data, 2));
		((  void (*) (FactoryNested_2_t2234585871 *, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 3)->method)(L_3, (Il2CppObject*)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 3));
		return L_3;
	}
}
// Zenject.IFactory`1<TContract> Zenject.IFactoryBinder`1<System.Object>::<ToIFactory`1>m__295<System.Object>(Zenject.InjectContext)
// Zenject.IFactory`1<TContract> Zenject.IFactoryBinder`1<System.Object>::<ToIFactory`1>m__295<System.Object>(Zenject.InjectContext)
extern "C"  Il2CppObject* IFactoryBinder_1_U3CToIFactory_1U3Em__295_TisIl2CppObject_m119911285_gshared (Il2CppObject * __this /* static, unused */, InjectContext_t3456483891 * ___c0, const MethodInfo* method)
{
	{
		InjectContext_t3456483891 * L_0 = ___c0;
		NullCheck(L_0);
		DiContainer_t2383114449 * L_1 = (DiContainer_t2383114449 *)L_0->get_Container_9();
		NullCheck((DiContainer_t2383114449 *)L_1);
		Il2CppObject* L_2 = GenericVirtFuncInvoker0< Il2CppObject* >::Invoke(IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0), (DiContainer_t2383114449 *)L_1);
		FactoryNested_2_t2234585871 * L_3 = (FactoryNested_2_t2234585871 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->rgctx_data, 1));
		((  void (*) (FactoryNested_2_t2234585871 *, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 2)->method)(L_3, (Il2CppObject*)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 2));
		return L_3;
	}
}
// Zenject.IFactory`2<TParam1,TContract> Zenject.IFactoryBinder`2<System.Object,System.Object>::<ToCustomFactory`2>m__29A<System.Object,System.Object>(Zenject.InjectContext)
// Zenject.IFactory`2<TParam1,TContract> Zenject.IFactoryBinder`2<System.Object,System.Object>::<ToCustomFactory`2>m__29A<System.Object,System.Object>(Zenject.InjectContext)
extern Il2CppClass* ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var;
extern const uint32_t IFactoryBinder_2_U3CToCustomFactory_2U3Em__29A_TisIl2CppObject_TisIl2CppObject_m4242004790_MetadataUsageId;
extern "C"  Il2CppObject* IFactoryBinder_2_U3CToCustomFactory_2U3Em__29A_TisIl2CppObject_TisIl2CppObject_m4242004790_gshared (Il2CppObject * __this /* static, unused */, InjectContext_t3456483891 * ___c0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (IFactoryBinder_2_U3CToCustomFactory_2U3Em__29A_TisIl2CppObject_TisIl2CppObject_m4242004790_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		InjectContext_t3456483891 * L_0 = ___c0;
		NullCheck(L_0);
		DiContainer_t2383114449 * L_1 = (DiContainer_t2383114449 *)L_0->get_Container_9();
		NullCheck((DiContainer_t2383114449 *)L_1);
		Il2CppObject * L_2 = GenericVirtFuncInvoker1< Il2CppObject *, ObjectU5BU5D_t11523773* >::Invoke(IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0), (DiContainer_t2383114449 *)L_1, (ObjectU5BU5D_t11523773*)((ObjectU5BU5D_t11523773*)SZArrayNew(ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var, (uint32_t)0)));
		FactoryNested_3_t3116483514 * L_3 = (FactoryNested_3_t3116483514 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->rgctx_data, 2));
		((  void (*) (FactoryNested_3_t3116483514 *, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 3)->method)(L_3, (Il2CppObject*)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 3));
		return L_3;
	}
}
// Zenject.IFactory`2<TParam1,TContract> Zenject.IFactoryBinder`2<System.Object,System.Object>::<ToIFactory`1>m__299<System.Object>(Zenject.InjectContext)
// Zenject.IFactory`2<TParam1,TContract> Zenject.IFactoryBinder`2<System.Object,System.Object>::<ToIFactory`1>m__299<System.Object>(Zenject.InjectContext)
extern "C"  Il2CppObject* IFactoryBinder_2_U3CToIFactory_1U3Em__299_TisIl2CppObject_m1599006073_gshared (Il2CppObject * __this /* static, unused */, InjectContext_t3456483891 * ___c0, const MethodInfo* method)
{
	{
		InjectContext_t3456483891 * L_0 = ___c0;
		NullCheck(L_0);
		DiContainer_t2383114449 * L_1 = (DiContainer_t2383114449 *)L_0->get_Container_9();
		NullCheck((DiContainer_t2383114449 *)L_1);
		Il2CppObject* L_2 = GenericVirtFuncInvoker0< Il2CppObject* >::Invoke(IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0), (DiContainer_t2383114449 *)L_1);
		FactoryNested_3_t3116483514 * L_3 = (FactoryNested_3_t3116483514 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->rgctx_data, 1));
		((  void (*) (FactoryNested_3_t3116483514 *, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 2)->method)(L_3, (Il2CppObject*)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 2));
		return L_3;
	}
}
// Zenject.IFactory`3<TParam1,TParam2,TContract> Zenject.IFactoryBinder`3<System.Object,System.Object,System.Object>::<ToCustomFactory`2>m__29E<System.Object,System.Object>(Zenject.InjectContext)
// Zenject.IFactory`3<TParam1,TParam2,TContract> Zenject.IFactoryBinder`3<System.Object,System.Object,System.Object>::<ToCustomFactory`2>m__29E<System.Object,System.Object>(Zenject.InjectContext)
extern Il2CppClass* ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var;
extern const uint32_t IFactoryBinder_3_U3CToCustomFactory_2U3Em__29E_TisIl2CppObject_TisIl2CppObject_m1558455803_MetadataUsageId;
extern "C"  Il2CppObject* IFactoryBinder_3_U3CToCustomFactory_2U3Em__29E_TisIl2CppObject_TisIl2CppObject_m1558455803_gshared (Il2CppObject * __this /* static, unused */, InjectContext_t3456483891 * ___c0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (IFactoryBinder_3_U3CToCustomFactory_2U3Em__29E_TisIl2CppObject_TisIl2CppObject_m1558455803_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		InjectContext_t3456483891 * L_0 = ___c0;
		NullCheck(L_0);
		DiContainer_t2383114449 * L_1 = (DiContainer_t2383114449 *)L_0->get_Container_9();
		NullCheck((DiContainer_t2383114449 *)L_1);
		Il2CppObject * L_2 = GenericVirtFuncInvoker1< Il2CppObject *, ObjectU5BU5D_t11523773* >::Invoke(IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0), (DiContainer_t2383114449 *)L_1, (ObjectU5BU5D_t11523773*)((ObjectU5BU5D_t11523773*)SZArrayNew(ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var, (uint32_t)0)));
		FactoryNested_4_t2122089277 * L_3 = (FactoryNested_4_t2122089277 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->rgctx_data, 2));
		((  void (*) (FactoryNested_4_t2122089277 *, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 3)->method)(L_3, (Il2CppObject*)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 3));
		return L_3;
	}
}
// Zenject.IFactory`3<TParam1,TParam2,TContract> Zenject.IFactoryBinder`3<System.Object,System.Object,System.Object>::<ToIFactory`1>m__29D<System.Object>(Zenject.InjectContext)
// Zenject.IFactory`3<TParam1,TParam2,TContract> Zenject.IFactoryBinder`3<System.Object,System.Object,System.Object>::<ToIFactory`1>m__29D<System.Object>(Zenject.InjectContext)
extern "C"  Il2CppObject* IFactoryBinder_3_U3CToIFactory_1U3Em__29D_TisIl2CppObject_m1022803491_gshared (Il2CppObject * __this /* static, unused */, InjectContext_t3456483891 * ___c0, const MethodInfo* method)
{
	{
		InjectContext_t3456483891 * L_0 = ___c0;
		NullCheck(L_0);
		DiContainer_t2383114449 * L_1 = (DiContainer_t2383114449 *)L_0->get_Container_9();
		NullCheck((DiContainer_t2383114449 *)L_1);
		Il2CppObject* L_2 = GenericVirtFuncInvoker0< Il2CppObject* >::Invoke(IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0), (DiContainer_t2383114449 *)L_1);
		FactoryNested_4_t2122089277 * L_3 = (FactoryNested_4_t2122089277 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->rgctx_data, 1));
		((  void (*) (FactoryNested_4_t2122089277 *, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 2)->method)(L_3, (Il2CppObject*)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 2));
		return L_3;
	}
}
// Zenject.IFactory`4<TParam1,TParam2,TParam3,TContract> Zenject.IFactoryBinder`4<System.Object,System.Object,System.Object,System.Object>::<ToCustomFactory`2>m__2A2<System.Object,System.Object>(Zenject.InjectContext)
// Zenject.IFactory`4<TParam1,TParam2,TParam3,TContract> Zenject.IFactoryBinder`4<System.Object,System.Object,System.Object,System.Object>::<ToCustomFactory`2>m__2A2<System.Object,System.Object>(Zenject.InjectContext)
extern Il2CppClass* ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var;
extern const uint32_t IFactoryBinder_4_U3CToCustomFactory_2U3Em__2A2_TisIl2CppObject_TisIl2CppObject_m3386608290_MetadataUsageId;
extern "C"  Il2CppObject* IFactoryBinder_4_U3CToCustomFactory_2U3Em__2A2_TisIl2CppObject_TisIl2CppObject_m3386608290_gshared (Il2CppObject * __this /* static, unused */, InjectContext_t3456483891 * ___c0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (IFactoryBinder_4_U3CToCustomFactory_2U3Em__2A2_TisIl2CppObject_TisIl2CppObject_m3386608290_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		InjectContext_t3456483891 * L_0 = ___c0;
		NullCheck(L_0);
		DiContainer_t2383114449 * L_1 = (DiContainer_t2383114449 *)L_0->get_Container_9();
		NullCheck((DiContainer_t2383114449 *)L_1);
		Il2CppObject * L_2 = GenericVirtFuncInvoker1< Il2CppObject *, ObjectU5BU5D_t11523773* >::Invoke(IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0), (DiContainer_t2383114449 *)L_1, (ObjectU5BU5D_t11523773*)((ObjectU5BU5D_t11523773*)SZArrayNew(ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var, (uint32_t)0)));
		FactoryNested_5_t2227126508 * L_3 = (FactoryNested_5_t2227126508 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->rgctx_data, 2));
		((  void (*) (FactoryNested_5_t2227126508 *, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 3)->method)(L_3, (Il2CppObject*)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 3));
		return L_3;
	}
}
// Zenject.IFactory`4<TParam1,TParam2,TParam3,TContract> Zenject.IFactoryBinder`4<System.Object,System.Object,System.Object,System.Object>::<ToIFactory`1>m__2A1<System.Object>(Zenject.InjectContext)
// Zenject.IFactory`4<TParam1,TParam2,TParam3,TContract> Zenject.IFactoryBinder`4<System.Object,System.Object,System.Object,System.Object>::<ToIFactory`1>m__2A1<System.Object>(Zenject.InjectContext)
extern "C"  Il2CppObject* IFactoryBinder_4_U3CToIFactory_1U3Em__2A1_TisIl2CppObject_m3488891462_gshared (Il2CppObject * __this /* static, unused */, InjectContext_t3456483891 * ___c0, const MethodInfo* method)
{
	{
		InjectContext_t3456483891 * L_0 = ___c0;
		NullCheck(L_0);
		DiContainer_t2383114449 * L_1 = (DiContainer_t2383114449 *)L_0->get_Container_9();
		NullCheck((DiContainer_t2383114449 *)L_1);
		Il2CppObject* L_2 = GenericVirtFuncInvoker0< Il2CppObject* >::Invoke(IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0), (DiContainer_t2383114449 *)L_1);
		FactoryNested_5_t2227126508 * L_3 = (FactoryNested_5_t2227126508 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->rgctx_data, 1));
		((  void (*) (FactoryNested_5_t2227126508 *, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 2)->method)(L_3, (Il2CppObject*)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 2));
		return L_3;
	}
}
// Zenject.IFactory`5<TParam1,TParam2,TParam3,TParam4,TContract> Zenject.IFactoryBinder`5<System.Object,System.Object,System.Object,System.Object,System.Object>::<ToCustomFactory`2>m__2A6<System.Object,System.Object>(Zenject.InjectContext)
// Zenject.IFactory`5<TParam1,TParam2,TParam3,TParam4,TContract> Zenject.IFactoryBinder`5<System.Object,System.Object,System.Object,System.Object,System.Object>::<ToCustomFactory`2>m__2A6<System.Object,System.Object>(Zenject.InjectContext)
extern Il2CppClass* ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var;
extern const uint32_t IFactoryBinder_5_U3CToCustomFactory_2U3Em__2A6_TisIl2CppObject_TisIl2CppObject_m2410447401_MetadataUsageId;
extern "C"  Il2CppObject* IFactoryBinder_5_U3CToCustomFactory_2U3Em__2A6_TisIl2CppObject_TisIl2CppObject_m2410447401_gshared (Il2CppObject * __this /* static, unused */, InjectContext_t3456483891 * ___c0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (IFactoryBinder_5_U3CToCustomFactory_2U3Em__2A6_TisIl2CppObject_TisIl2CppObject_m2410447401_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		InjectContext_t3456483891 * L_0 = ___c0;
		NullCheck(L_0);
		DiContainer_t2383114449 * L_1 = (DiContainer_t2383114449 *)L_0->get_Container_9();
		NullCheck((DiContainer_t2383114449 *)L_1);
		Il2CppObject * L_2 = GenericVirtFuncInvoker1< Il2CppObject *, ObjectU5BU5D_t11523773* >::Invoke(IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0), (DiContainer_t2383114449 *)L_1, (ObjectU5BU5D_t11523773*)((ObjectU5BU5D_t11523773*)SZArrayNew(ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var, (uint32_t)0)));
		FactoryNested_6_t1301445835 * L_3 = (FactoryNested_6_t1301445835 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->rgctx_data, 2));
		((  void (*) (FactoryNested_6_t1301445835 *, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 3)->method)(L_3, (Il2CppObject*)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 3));
		return L_3;
	}
}
// Zenject.IFactory`5<TParam1,TParam2,TParam3,TParam4,TContract> Zenject.IFactoryBinder`5<System.Object,System.Object,System.Object,System.Object,System.Object>::<ToIFactory`1>m__2A5<System.Object>(Zenject.InjectContext)
// Zenject.IFactory`5<TParam1,TParam2,TParam3,TParam4,TContract> Zenject.IFactoryBinder`5<System.Object,System.Object,System.Object,System.Object,System.Object>::<ToIFactory`1>m__2A5<System.Object>(Zenject.InjectContext)
extern "C"  Il2CppObject* IFactoryBinder_5_U3CToIFactory_1U3Em__2A5_TisIl2CppObject_m1988390567_gshared (Il2CppObject * __this /* static, unused */, InjectContext_t3456483891 * ___c0, const MethodInfo* method)
{
	{
		InjectContext_t3456483891 * L_0 = ___c0;
		NullCheck(L_0);
		DiContainer_t2383114449 * L_1 = (DiContainer_t2383114449 *)L_0->get_Container_9();
		NullCheck((DiContainer_t2383114449 *)L_1);
		Il2CppObject* L_2 = GenericVirtFuncInvoker0< Il2CppObject* >::Invoke(IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0), (DiContainer_t2383114449 *)L_1);
		FactoryNested_6_t1301445835 * L_3 = (FactoryNested_6_t1301445835 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->rgctx_data, 1));
		((  void (*) (FactoryNested_6_t1301445835 *, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 2)->method)(L_3, (Il2CppObject*)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 2));
		return L_3;
	}
}
// Zenject.IFactoryBinder`1<TContract> Zenject.DiContainer::BindIFactory<System.Object>()
// Zenject.IFactoryBinder`1<TContract> Zenject.DiContainer::BindIFactory<System.Object>()
extern "C"  IFactoryBinder_1_t1064970850 * DiContainer_BindIFactory_TisIl2CppObject_m638121348_gshared (DiContainer_t2383114449 * __this, const MethodInfo* method)
{
	{
		NullCheck((DiContainer_t2383114449 *)__this);
		IFactoryBinder_1_t1064970850 * L_0 = GenericVirtFuncInvoker1< IFactoryBinder_1_t1064970850 *, String_t* >::Invoke(IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0), (DiContainer_t2383114449 *)__this, (String_t*)NULL);
		return L_0;
	}
}
// Zenject.IFactoryBinder`1<TContract> Zenject.DiContainer::BindIFactory<System.Object>(System.String)
// Zenject.IFactoryBinder`1<TContract> Zenject.DiContainer::BindIFactory<System.Object>(System.String)
extern "C"  IFactoryBinder_1_t1064970850 * DiContainer_BindIFactory_TisIl2CppObject_m1974124542_gshared (DiContainer_t2383114449 * __this, String_t* ___identifier0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___identifier0;
		IFactoryBinder_1_t1064970850 * L_1 = (IFactoryBinder_1_t1064970850 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->rgctx_data, 0));
		((  void (*) (IFactoryBinder_1_t1064970850 *, DiContainer_t2383114449 *, String_t*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1)->method)(L_1, (DiContainer_t2383114449 *)__this, (String_t*)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1));
		return L_1;
	}
}
// Zenject.IFactoryBinder`1<TContract> Zenject.IBinder::BindIFactory<System.Object>()
// Zenject.IFactoryBinder`1<TContract> Zenject.IBinder::BindIFactory<System.Object>(System.String)
// Zenject.IFactoryBinder`2<TParam1,TContract> Zenject.DiContainer::BindIFactory<System.Object,System.Object>()
// Zenject.IFactoryBinder`2<TParam1,TContract> Zenject.DiContainer::BindIFactory<System.Object,System.Object>()
extern "C"  IFactoryBinder_2_t3921329133 * DiContainer_BindIFactory_TisIl2CppObject_TisIl2CppObject_m2904768223_gshared (DiContainer_t2383114449 * __this, const MethodInfo* method)
{
	{
		NullCheck((DiContainer_t2383114449 *)__this);
		IFactoryBinder_2_t3921329133 * L_0 = GenericVirtFuncInvoker1< IFactoryBinder_2_t3921329133 *, String_t* >::Invoke(IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0), (DiContainer_t2383114449 *)__this, (String_t*)NULL);
		return L_0;
	}
}
// Zenject.IFactoryBinder`2<TParam1,TContract> Zenject.DiContainer::BindIFactory<System.Object,System.Object>(System.String)
// Zenject.IFactoryBinder`2<TParam1,TContract> Zenject.DiContainer::BindIFactory<System.Object,System.Object>(System.String)
extern "C"  IFactoryBinder_2_t3921329133 * DiContainer_BindIFactory_TisIl2CppObject_TisIl2CppObject_m1361429123_gshared (DiContainer_t2383114449 * __this, String_t* ___identifier0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___identifier0;
		IFactoryBinder_2_t3921329133 * L_1 = (IFactoryBinder_2_t3921329133 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->rgctx_data, 0));
		((  void (*) (IFactoryBinder_2_t3921329133 *, DiContainer_t2383114449 *, String_t*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1)->method)(L_1, (DiContainer_t2383114449 *)__this, (String_t*)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1));
		return L_1;
	}
}
// Zenject.IFactoryBinder`2<TParam1,TContract> Zenject.IBinder::BindIFactory<System.Object,System.Object>()
// Zenject.IFactoryBinder`2<TParam1,TContract> Zenject.IBinder::BindIFactory<System.Object,System.Object>(System.String)
// Zenject.IFactoryBinder`3<TParam1,TParam2,TContract> Zenject.DiContainer::BindIFactory<System.Object,System.Object,System.Object>()
// Zenject.IFactoryBinder`3<TParam1,TParam2,TContract> Zenject.DiContainer::BindIFactory<System.Object,System.Object,System.Object>()
extern "C"  IFactoryBinder_3_t153990564 * DiContainer_BindIFactory_TisIl2CppObject_TisIl2CppObject_TisIl2CppObject_m2649206011_gshared (DiContainer_t2383114449 * __this, const MethodInfo* method)
{
	{
		NullCheck((DiContainer_t2383114449 *)__this);
		IFactoryBinder_3_t153990564 * L_0 = GenericVirtFuncInvoker1< IFactoryBinder_3_t153990564 *, String_t* >::Invoke(IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0), (DiContainer_t2383114449 *)__this, (String_t*)NULL);
		return L_0;
	}
}
// Zenject.IFactoryBinder`3<TParam1,TParam2,TContract> Zenject.DiContainer::BindIFactory<System.Object,System.Object,System.Object>(System.String)
// Zenject.IFactoryBinder`3<TParam1,TParam2,TContract> Zenject.DiContainer::BindIFactory<System.Object,System.Object,System.Object>(System.String)
extern "C"  IFactoryBinder_3_t153990564 * DiContainer_BindIFactory_TisIl2CppObject_TisIl2CppObject_TisIl2CppObject_m3217433063_gshared (DiContainer_t2383114449 * __this, String_t* ___identifier0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___identifier0;
		IFactoryBinder_3_t153990564 * L_1 = (IFactoryBinder_3_t153990564 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->rgctx_data, 0));
		((  void (*) (IFactoryBinder_3_t153990564 *, DiContainer_t2383114449 *, String_t*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1)->method)(L_1, (DiContainer_t2383114449 *)__this, (String_t*)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1));
		return L_1;
	}
}
// Zenject.IFactoryBinder`3<TParam1,TParam2,TContract> Zenject.IBinder::BindIFactory<System.Object,System.Object,System.Object>()
// Zenject.IFactoryBinder`3<TParam1,TParam2,TContract> Zenject.IBinder::BindIFactory<System.Object,System.Object,System.Object>(System.String)
// Zenject.IFactoryBinder`4<TParam1,TParam2,TParam3,TContract> Zenject.DiContainer::BindIFactory<System.Object,System.Object,System.Object,System.Object>()
// Zenject.IFactoryBinder`4<TParam1,TParam2,TParam3,TContract> Zenject.DiContainer::BindIFactory<System.Object,System.Object,System.Object,System.Object>()
extern "C"  IFactoryBinder_4_t2507049579 * DiContainer_BindIFactory_TisIl2CppObject_TisIl2CppObject_TisIl2CppObject_TisIl2CppObject_m3446394264_gshared (DiContainer_t2383114449 * __this, const MethodInfo* method)
{
	{
		NullCheck((DiContainer_t2383114449 *)__this);
		IFactoryBinder_4_t2507049579 * L_0 = GenericVirtFuncInvoker1< IFactoryBinder_4_t2507049579 *, String_t* >::Invoke(IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0), (DiContainer_t2383114449 *)__this, (String_t*)NULL);
		return L_0;
	}
}
// Zenject.IFactoryBinder`4<TParam1,TParam2,TParam3,TContract> Zenject.DiContainer::BindIFactory<System.Object,System.Object,System.Object,System.Object>(System.String)
// Zenject.IFactoryBinder`4<TParam1,TParam2,TParam3,TContract> Zenject.DiContainer::BindIFactory<System.Object,System.Object,System.Object,System.Object>(System.String)
extern "C"  IFactoryBinder_4_t2507049579 * DiContainer_BindIFactory_TisIl2CppObject_TisIl2CppObject_TisIl2CppObject_TisIl2CppObject_m620406890_gshared (DiContainer_t2383114449 * __this, String_t* ___identifier0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___identifier0;
		IFactoryBinder_4_t2507049579 * L_1 = (IFactoryBinder_4_t2507049579 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->rgctx_data, 0));
		((  void (*) (IFactoryBinder_4_t2507049579 *, DiContainer_t2383114449 *, String_t*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1)->method)(L_1, (DiContainer_t2383114449 *)__this, (String_t*)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1));
		return L_1;
	}
}
// Zenject.IFactoryBinder`4<TParam1,TParam2,TParam3,TContract> Zenject.IBinder::BindIFactory<System.Object,System.Object,System.Object,System.Object>()
// Zenject.IFactoryBinder`4<TParam1,TParam2,TParam3,TContract> Zenject.IBinder::BindIFactory<System.Object,System.Object,System.Object,System.Object>(System.String)
// Zenject.IFactoryBinder`5<TParam1,TParam2,TParam3,TParam4,TContract> Zenject.DiContainer::BindIFactory<System.Object,System.Object,System.Object,System.Object,System.Object>()
// Zenject.IFactoryBinder`5<TParam1,TParam2,TParam3,TParam4,TContract> Zenject.DiContainer::BindIFactory<System.Object,System.Object,System.Object,System.Object,System.Object>()
extern "C"  IFactoryBinder_5_t1497530694 * DiContainer_BindIFactory_TisIl2CppObject_TisIl2CppObject_TisIl2CppObject_TisIl2CppObject_TisIl2CppObject_m3025374838_gshared (DiContainer_t2383114449 * __this, const MethodInfo* method)
{
	{
		NullCheck((DiContainer_t2383114449 *)__this);
		IFactoryBinder_5_t1497530694 * L_0 = GenericVirtFuncInvoker1< IFactoryBinder_5_t1497530694 *, String_t* >::Invoke(IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0), (DiContainer_t2383114449 *)__this, (String_t*)NULL);
		return L_0;
	}
}
// Zenject.IFactoryBinder`5<TParam1,TParam2,TParam3,TParam4,TContract> Zenject.DiContainer::BindIFactory<System.Object,System.Object,System.Object,System.Object,System.Object>(System.String)
// Zenject.IFactoryBinder`5<TParam1,TParam2,TParam3,TParam4,TContract> Zenject.DiContainer::BindIFactory<System.Object,System.Object,System.Object,System.Object,System.Object>(System.String)
extern "C"  IFactoryBinder_5_t1497530694 * DiContainer_BindIFactory_TisIl2CppObject_TisIl2CppObject_TisIl2CppObject_TisIl2CppObject_TisIl2CppObject_m587186252_gshared (DiContainer_t2383114449 * __this, String_t* ___identifier0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___identifier0;
		IFactoryBinder_5_t1497530694 * L_1 = (IFactoryBinder_5_t1497530694 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->rgctx_data, 0));
		((  void (*) (IFactoryBinder_5_t1497530694 *, DiContainer_t2383114449 *, String_t*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1)->method)(L_1, (DiContainer_t2383114449 *)__this, (String_t*)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1));
		return L_1;
	}
}
// Zenject.IFactoryBinder`5<TParam1,TParam2,TParam3,TParam4,TContract> Zenject.IBinder::BindIFactory<System.Object,System.Object,System.Object,System.Object,System.Object>()
// Zenject.IFactoryBinder`5<TParam1,TParam2,TParam3,TParam4,TContract> Zenject.IBinder::BindIFactory<System.Object,System.Object,System.Object,System.Object,System.Object>(System.String)
// Zenject.IFactoryUntypedBinder`1<TContract> Zenject.DiContainer::BindIFactoryUntyped<System.Object>()
// Zenject.IFactoryUntypedBinder`1<TContract> Zenject.DiContainer::BindIFactoryUntyped<System.Object>()
extern "C"  IFactoryUntypedBinder_1_t486203833 * DiContainer_BindIFactoryUntyped_TisIl2CppObject_m1378532874_gshared (DiContainer_t2383114449 * __this, const MethodInfo* method)
{
	{
		NullCheck((DiContainer_t2383114449 *)__this);
		IFactoryUntypedBinder_1_t486203833 * L_0 = GenericVirtFuncInvoker1< IFactoryUntypedBinder_1_t486203833 *, String_t* >::Invoke(IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0), (DiContainer_t2383114449 *)__this, (String_t*)NULL);
		return L_0;
	}
}
// Zenject.IFactoryUntypedBinder`1<TContract> Zenject.DiContainer::BindIFactoryUntyped<System.Object>(System.String)
// Zenject.IFactoryUntypedBinder`1<TContract> Zenject.DiContainer::BindIFactoryUntyped<System.Object>(System.String)
extern "C"  IFactoryUntypedBinder_1_t486203833 * DiContainer_BindIFactoryUntyped_TisIl2CppObject_m1187348792_gshared (DiContainer_t2383114449 * __this, String_t* ___identifier0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___identifier0;
		IFactoryUntypedBinder_1_t486203833 * L_1 = (IFactoryUntypedBinder_1_t486203833 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->rgctx_data, 0));
		((  void (*) (IFactoryUntypedBinder_1_t486203833 *, DiContainer_t2383114449 *, String_t*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1)->method)(L_1, (DiContainer_t2383114449 *)__this, (String_t*)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1));
		return L_1;
	}
}
// Zenject.IFactoryUntypedBinder`1<TContract> Zenject.IBinder::BindIFactoryUntyped<System.Object>()
// Zenject.IFactoryUntypedBinder`1<TContract> Zenject.IBinder::BindIFactoryUntyped<System.Object>(System.String)
// Zenject.ProviderBase Zenject.SingletonProviderMap::CreateProviderFromMethod<System.Object>(System.String,System.Func`2<Zenject.InjectContext,TConcrete>)
// Zenject.ProviderBase Zenject.SingletonProviderMap::CreateProviderFromMethod<System.Object>(System.String,System.Func`2<Zenject.InjectContext,TConcrete>)
extern Il2CppClass* SingletonProvider_t585108081_il2cpp_TypeInfo_var;
extern const uint32_t SingletonProviderMap_CreateProviderFromMethod_TisIl2CppObject_m4159376484_MetadataUsageId;
extern "C"  ProviderBase_t1627494391 * SingletonProviderMap_CreateProviderFromMethod_TisIl2CppObject_m4159376484_gshared (SingletonProviderMap_t1557411893 * __this, String_t* ___identifier0, Func_2_t2621245597 * ___method1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SingletonProviderMap_CreateProviderFromMethod_TisIl2CppObject_m4159376484_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		DiContainer_t2383114449 * L_0 = (DiContainer_t2383114449 *)__this->get__container_1();
		String_t* L_1 = ___identifier0;
		Func_2_t2621245597 * L_2 = ___method1;
		NullCheck((SingletonProviderMap_t1557411893 *)__this);
		SingletonLazyCreator_t2762284194 * L_3 = ((  SingletonLazyCreator_t2762284194 * (*) (SingletonProviderMap_t1557411893 *, String_t*, Func_2_t2621245597 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0)->method)((SingletonProviderMap_t1557411893 *)__this, (String_t*)L_1, (Func_2_t2621245597 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0));
		SingletonProvider_t585108081 * L_4 = (SingletonProvider_t585108081 *)il2cpp_codegen_object_new(SingletonProvider_t585108081_il2cpp_TypeInfo_var);
		SingletonProvider__ctor_m1411291507(L_4, (DiContainer_t2383114449 *)L_0, (SingletonLazyCreator_t2762284194 *)L_3, /*hidden argument*/NULL);
		return L_4;
	}
}
// Zenject.SingletonLazyCreator Zenject.SingletonProviderMap::AddCreatorFromMethod<System.Object>(System.String,System.Func`2<Zenject.InjectContext,TConcrete>)
// Zenject.SingletonLazyCreator Zenject.SingletonProviderMap::AddCreatorFromMethod<System.Object>(System.String,System.Func`2<Zenject.InjectContext,TConcrete>)
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern Il2CppClass* SingletonId_t1838183899_il2cpp_TypeInfo_var;
extern Il2CppClass* ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var;
extern Il2CppClass* ZenjectBindException_t3550319704_il2cpp_TypeInfo_var;
extern Il2CppClass* Func_2_t2621245597_il2cpp_TypeInfo_var;
extern Il2CppClass* SingletonLazyCreator_t2762284194_il2cpp_TypeInfo_var;
extern const MethodInfo* Func_2__ctor_m3648137547_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral1248571898;
extern const uint32_t SingletonProviderMap_AddCreatorFromMethod_TisIl2CppObject_m2008774778_MetadataUsageId;
extern "C"  SingletonLazyCreator_t2762284194 * SingletonProviderMap_AddCreatorFromMethod_TisIl2CppObject_m2008774778_gshared (SingletonProviderMap_t1557411893 * __this, String_t* ___identifier0, Func_2_t2621245597 * ___method1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SingletonProviderMap_AddCreatorFromMethod_TisIl2CppObject_m2008774778_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	SingletonLazyCreator_t2762284194 * V_0 = NULL;
	SingletonId_t1838183899 * V_1 = NULL;
	U3CAddCreatorFromMethodU3Ec__AnonStorey197_1_t3364390262 * V_2 = NULL;
	{
		U3CAddCreatorFromMethodU3Ec__AnonStorey197_1_t3364390262 * L_0 = (U3CAddCreatorFromMethodU3Ec__AnonStorey197_1_t3364390262 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->rgctx_data, 0));
		((  void (*) (U3CAddCreatorFromMethodU3Ec__AnonStorey197_1_t3364390262 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1)->method)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1));
		V_2 = (U3CAddCreatorFromMethodU3Ec__AnonStorey197_1_t3364390262 *)L_0;
		U3CAddCreatorFromMethodU3Ec__AnonStorey197_1_t3364390262 * L_1 = V_2;
		Func_2_t2621245597 * L_2 = ___method1;
		NullCheck(L_1);
		L_1->set_method_0(L_2);
		String_t* L_3 = ___identifier0;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_4 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t1864875887 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->rgctx_data, 2)), /*hidden argument*/NULL);
		SingletonId_t1838183899 * L_5 = (SingletonId_t1838183899 *)il2cpp_codegen_object_new(SingletonId_t1838183899_il2cpp_TypeInfo_var);
		SingletonId__ctor_m351994705(L_5, (String_t*)L_3, (Type_t *)L_4, /*hidden argument*/NULL);
		V_1 = (SingletonId_t1838183899 *)L_5;
		Dictionary_2_t2265680905 * L_6 = (Dictionary_2_t2265680905 *)__this->get__creators_0();
		SingletonId_t1838183899 * L_7 = V_1;
		NullCheck((Dictionary_2_t2265680905 *)L_6);
		bool L_8 = VirtFuncInvoker1< bool, SingletonId_t1838183899 * >::Invoke(19 /* System.Boolean System.Collections.Generic.Dictionary`2<Zenject.SingletonId,Zenject.SingletonLazyCreator>::ContainsKey(!0) */, (Dictionary_2_t2265680905 *)L_6, (SingletonId_t1838183899 *)L_7);
		if (!L_8)
		{
			goto IL_0052;
		}
	}
	{
		ObjectU5BU5D_t11523773* L_9 = (ObjectU5BU5D_t11523773*)((ObjectU5BU5D_t11523773*)SZArrayNew(ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var, (uint32_t)1));
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_10 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t1864875887 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->rgctx_data, 2)), /*hidden argument*/NULL);
		NullCheck(L_9);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_9, 0);
		ArrayElementTypeCheck (L_9, L_10);
		(L_9)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)L_10);
		String_t* L_11 = MiscExtensions_Fmt_m1348433569(NULL /*static, unused*/, (String_t*)_stringLiteral1248571898, (ObjectU5BU5D_t11523773*)L_9, /*hidden argument*/NULL);
		ZenjectBindException_t3550319704 * L_12 = (ZenjectBindException_t3550319704 *)il2cpp_codegen_object_new(ZenjectBindException_t3550319704_il2cpp_TypeInfo_var);
		ZenjectBindException__ctor_m3054227529(L_12, (String_t*)L_11, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_12);
	}

IL_0052:
	{
		DiContainer_t2383114449 * L_13 = (DiContainer_t2383114449 *)__this->get__container_1();
		SingletonId_t1838183899 * L_14 = V_1;
		U3CAddCreatorFromMethodU3Ec__AnonStorey197_1_t3364390262 * L_15 = V_2;
		IntPtr_t L_16;
		L_16.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 3));
		Func_2_t2621245597 * L_17 = (Func_2_t2621245597 *)il2cpp_codegen_object_new(Func_2_t2621245597_il2cpp_TypeInfo_var);
		Func_2__ctor_m3648137547(L_17, (Il2CppObject *)L_15, (IntPtr_t)L_16, /*hidden argument*/Func_2__ctor_m3648137547_MethodInfo_var);
		SingletonLazyCreator_t2762284194 * L_18 = (SingletonLazyCreator_t2762284194 *)il2cpp_codegen_object_new(SingletonLazyCreator_t2762284194_il2cpp_TypeInfo_var);
		SingletonLazyCreator__ctor_m2760220428(L_18, (DiContainer_t2383114449 *)L_13, (SingletonProviderMap_t1557411893 *)__this, (SingletonId_t1838183899 *)L_14, (Func_2_t2621245597 *)L_17, /*hidden argument*/NULL);
		V_0 = (SingletonLazyCreator_t2762284194 *)L_18;
		Dictionary_2_t2265680905 * L_19 = (Dictionary_2_t2265680905 *)__this->get__creators_0();
		SingletonId_t1838183899 * L_20 = V_1;
		SingletonLazyCreator_t2762284194 * L_21 = V_0;
		NullCheck((Dictionary_2_t2265680905 *)L_19);
		VirtActionInvoker2< SingletonId_t1838183899 *, SingletonLazyCreator_t2762284194 * >::Invoke(18 /* System.Void System.Collections.Generic.Dictionary`2<Zenject.SingletonId,Zenject.SingletonLazyCreator>::Add(!0,!1) */, (Dictionary_2_t2265680905 *)L_19, (SingletonId_t1838183899 *)L_20, (SingletonLazyCreator_t2762284194 *)L_21);
		SingletonLazyCreator_t2762284194 * L_22 = V_0;
		NullCheck((SingletonLazyCreator_t2762284194 *)L_22);
		SingletonLazyCreator_IncRefCount_m1541995047((SingletonLazyCreator_t2762284194 *)L_22, /*hidden argument*/NULL);
		SingletonLazyCreator_t2762284194 * L_23 = V_0;
		return L_23;
	}
}
// Zenject.TypeValuePair Zenject.InstantiateUtil::CreateTypePair<System.Object>(T)
// Zenject.TypeValuePair Zenject.InstantiateUtil::CreateTypePair<System.Object>(T)
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern Il2CppClass* TypeValuePair_t620932390_il2cpp_TypeInfo_var;
extern const uint32_t InstantiateUtil_CreateTypePair_TisIl2CppObject_m1693637397_MetadataUsageId;
extern "C"  TypeValuePair_t620932390 * InstantiateUtil_CreateTypePair_TisIl2CppObject_m1693637397_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * ___param0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InstantiateUtil_CreateTypePair_TisIl2CppObject_m1693637397_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Type_t * G_B3_0 = NULL;
	{
		Il2CppObject * L_0 = ___param0;
		if (L_0)
		{
			goto IL_001a;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_1 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t1864875887 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->rgctx_data, 1)), /*hidden argument*/NULL);
		G_B3_0 = L_1;
		goto IL_0027;
	}

IL_001a:
	{
		NullCheck((Il2CppObject *)(*(&___param0)));
		Type_t * L_2 = Object_GetType_m2022236990((Il2CppObject *)(*(&___param0)), /*hidden argument*/NULL);
		G_B3_0 = L_2;
	}

IL_0027:
	{
		Il2CppObject * L_3 = ___param0;
		TypeValuePair_t620932390 * L_4 = (TypeValuePair_t620932390 *)il2cpp_codegen_object_new(TypeValuePair_t620932390_il2cpp_TypeInfo_var);
		TypeValuePair__ctor_m1625881838(L_4, (Type_t *)G_B3_0, (Il2CppObject *)L_3, /*hidden argument*/NULL);
		return L_4;
	}
}
// ZergRush.IReactiveCollection`1<T> ZergRush.ReactiveCollectionExtension::Filter<System.Object>(ZergRush.IReactiveCollection`1<T>,System.Func`2<T,System.Boolean>)
// ZergRush.IReactiveCollection`1<T> ZergRush.ReactiveCollectionExtension::Filter<System.Object>(ZergRush.IReactiveCollection`1<T>,System.Func`2<T,System.Boolean>)
extern "C"  Il2CppObject* ReactiveCollectionExtension_Filter_TisIl2CppObject_m68123642_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* ___coll0, Func_2_t1509682273 * ___predicate1, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = ___coll0;
		return L_0;
	}
}
