﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.ClientModels.FriendInfo
struct FriendInfo_t3708701852;
// System.String
struct String_t;
// System.Collections.Generic.List`1<System.String>
struct List_1_t1765447871;
// PlayFab.ClientModels.UserFacebookInfo
struct UserFacebookInfo_t1491589167;
// PlayFab.ClientModels.UserSteamInfo
struct UserSteamInfo_t2867463811;
// PlayFab.ClientModels.UserGameCenterInfo
struct UserGameCenterInfo_t386684176;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String968488902.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_UserFaceboo1491589167.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_UserSteamIn2867463811.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_UserGameCent386684176.h"

// System.Void PlayFab.ClientModels.FriendInfo::.ctor()
extern "C"  void FriendInfo__ctor_m1180506785 (FriendInfo_t3708701852 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PlayFab.ClientModels.FriendInfo::get_FriendPlayFabId()
extern "C"  String_t* FriendInfo_get_FriendPlayFabId_m872270633 (FriendInfo_t3708701852 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.FriendInfo::set_FriendPlayFabId(System.String)
extern "C"  void FriendInfo_set_FriendPlayFabId_m938911984 (FriendInfo_t3708701852 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PlayFab.ClientModels.FriendInfo::get_Username()
extern "C"  String_t* FriendInfo_get_Username_m1030582239 (FriendInfo_t3708701852 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.FriendInfo::set_Username(System.String)
extern "C"  void FriendInfo_set_Username_m4218965740 (FriendInfo_t3708701852 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PlayFab.ClientModels.FriendInfo::get_TitleDisplayName()
extern "C"  String_t* FriendInfo_get_TitleDisplayName_m4291708990 (FriendInfo_t3708701852 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.FriendInfo::set_TitleDisplayName(System.String)
extern "C"  void FriendInfo_set_TitleDisplayName_m69783533 (FriendInfo_t3708701852 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<System.String> PlayFab.ClientModels.FriendInfo::get_Tags()
extern "C"  List_1_t1765447871 * FriendInfo_get_Tags_m3880108500 (FriendInfo_t3708701852 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.FriendInfo::set_Tags(System.Collections.Generic.List`1<System.String>)
extern "C"  void FriendInfo_set_Tags_m1288459277 (FriendInfo_t3708701852 * __this, List_1_t1765447871 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PlayFab.ClientModels.FriendInfo::get_CurrentMatchmakerLobbyId()
extern "C"  String_t* FriendInfo_get_CurrentMatchmakerLobbyId_m3246713538 (FriendInfo_t3708701852 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.FriendInfo::set_CurrentMatchmakerLobbyId(System.String)
extern "C"  void FriendInfo_set_CurrentMatchmakerLobbyId_m944958185 (FriendInfo_t3708701852 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PlayFab.ClientModels.UserFacebookInfo PlayFab.ClientModels.FriendInfo::get_FacebookInfo()
extern "C"  UserFacebookInfo_t1491589167 * FriendInfo_get_FacebookInfo_m1029124362 (FriendInfo_t3708701852 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.FriendInfo::set_FacebookInfo(PlayFab.ClientModels.UserFacebookInfo)
extern "C"  void FriendInfo_set_FacebookInfo_m3869497633 (FriendInfo_t3708701852 * __this, UserFacebookInfo_t1491589167 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PlayFab.ClientModels.UserSteamInfo PlayFab.ClientModels.FriendInfo::get_SteamInfo()
extern "C"  UserSteamInfo_t2867463811 * FriendInfo_get_SteamInfo_m131235292 (FriendInfo_t3708701852 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.FriendInfo::set_SteamInfo(PlayFab.ClientModels.UserSteamInfo)
extern "C"  void FriendInfo_set_SteamInfo_m1500091051 (FriendInfo_t3708701852 * __this, UserSteamInfo_t2867463811 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PlayFab.ClientModels.UserGameCenterInfo PlayFab.ClientModels.FriendInfo::get_GameCenterInfo()
extern "C"  UserGameCenterInfo_t386684176 * FriendInfo_get_GameCenterInfo_m3422190732 (FriendInfo_t3708701852 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.FriendInfo::set_GameCenterInfo(PlayFab.ClientModels.UserGameCenterInfo)
extern "C"  void FriendInfo_set_GameCenterInfo_m2788939743 (FriendInfo_t3708701852 * __this, UserGameCenterInfo_t386684176 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
