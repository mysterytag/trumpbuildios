﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.IDisposable
struct IDisposable_t1628921374;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Disposable
struct  Disposable_t3736388786  : public Il2CppObject
{
public:

public:
};

struct Disposable_t3736388786_StaticFields
{
public:
	// System.IDisposable UniRx.Disposable::Empty
	Il2CppObject * ___Empty_0;

public:
	inline static int32_t get_offset_of_Empty_0() { return static_cast<int32_t>(offsetof(Disposable_t3736388786_StaticFields, ___Empty_0)); }
	inline Il2CppObject * get_Empty_0() const { return ___Empty_0; }
	inline Il2CppObject ** get_address_of_Empty_0() { return &___Empty_0; }
	inline void set_Empty_0(Il2CppObject * value)
	{
		___Empty_0 = value;
		Il2CppCodeGenWriteBarrier(&___Empty_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
