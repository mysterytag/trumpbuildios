﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.CombineLatestObservable`4<System.Object,System.Object,System.Object,System.Object>
struct CombineLatestObservable_4_t1056168445;
// UniRx.IObservable`1<System.Object>
struct IObservable_1_t595904784;
// UniRx.Operators.CombineLatestFunc`4<System.Object,System.Object,System.Object,System.Object>
struct CombineLatestFunc_4_t382088552;
// System.IDisposable
struct IDisposable_t1628921374;
// UniRx.IObserver`1<System.Object>
struct IObserver_1_t3049105323;

#include "codegen/il2cpp-codegen.h"

// System.Void UniRx.Operators.CombineLatestObservable`4<System.Object,System.Object,System.Object,System.Object>::.ctor(UniRx.IObservable`1<T1>,UniRx.IObservable`1<T2>,UniRx.IObservable`1<T3>,UniRx.Operators.CombineLatestFunc`4<T1,T2,T3,TR>)
extern "C"  void CombineLatestObservable_4__ctor_m3167615775_gshared (CombineLatestObservable_4_t1056168445 * __this, Il2CppObject* ___source10, Il2CppObject* ___source21, Il2CppObject* ___source32, CombineLatestFunc_4_t382088552 * ___resultSelector3, const MethodInfo* method);
#define CombineLatestObservable_4__ctor_m3167615775(__this, ___source10, ___source21, ___source32, ___resultSelector3, method) ((  void (*) (CombineLatestObservable_4_t1056168445 *, Il2CppObject*, Il2CppObject*, Il2CppObject*, CombineLatestFunc_4_t382088552 *, const MethodInfo*))CombineLatestObservable_4__ctor_m3167615775_gshared)(__this, ___source10, ___source21, ___source32, ___resultSelector3, method)
// System.IDisposable UniRx.Operators.CombineLatestObservable`4<System.Object,System.Object,System.Object,System.Object>::SubscribeCore(UniRx.IObserver`1<TR>,System.IDisposable)
extern "C"  Il2CppObject * CombineLatestObservable_4_SubscribeCore_m2687735457_gshared (CombineLatestObservable_4_t1056168445 * __this, Il2CppObject* ___observer0, Il2CppObject * ___cancel1, const MethodInfo* method);
#define CombineLatestObservable_4_SubscribeCore_m2687735457(__this, ___observer0, ___cancel1, method) ((  Il2CppObject * (*) (CombineLatestObservable_4_t1056168445 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))CombineLatestObservable_4_SubscribeCore_m2687735457_gshared)(__this, ___observer0, ___cancel1, method)
