﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.OperatorObservableBase`1<UniRx.DictionaryRemoveEvent`2<System.Object,System.Object>>
struct OperatorObservableBase_1_t2386433262;
// System.IDisposable
struct IDisposable_t1628921374;
// UniRx.IObserver`1<UniRx.DictionaryRemoveEvent`2<System.Object,System.Object>>
struct IObserver_1_t1239319898;

#include "codegen/il2cpp-codegen.h"

// System.Void UniRx.Operators.OperatorObservableBase`1<UniRx.DictionaryRemoveEvent`2<System.Object,System.Object>>::.ctor(System.Boolean)
extern "C"  void OperatorObservableBase_1__ctor_m3444905460_gshared (OperatorObservableBase_1_t2386433262 * __this, bool ___isRequiredSubscribeOnCurrentThread0, const MethodInfo* method);
#define OperatorObservableBase_1__ctor_m3444905460(__this, ___isRequiredSubscribeOnCurrentThread0, method) ((  void (*) (OperatorObservableBase_1_t2386433262 *, bool, const MethodInfo*))OperatorObservableBase_1__ctor_m3444905460_gshared)(__this, ___isRequiredSubscribeOnCurrentThread0, method)
// System.Boolean UniRx.Operators.OperatorObservableBase`1<UniRx.DictionaryRemoveEvent`2<System.Object,System.Object>>::IsRequiredSubscribeOnCurrentThread()
extern "C"  bool OperatorObservableBase_1_IsRequiredSubscribeOnCurrentThread_m3625574502_gshared (OperatorObservableBase_1_t2386433262 * __this, const MethodInfo* method);
#define OperatorObservableBase_1_IsRequiredSubscribeOnCurrentThread_m3625574502(__this, method) ((  bool (*) (OperatorObservableBase_1_t2386433262 *, const MethodInfo*))OperatorObservableBase_1_IsRequiredSubscribeOnCurrentThread_m3625574502_gshared)(__this, method)
// System.IDisposable UniRx.Operators.OperatorObservableBase`1<UniRx.DictionaryRemoveEvent`2<System.Object,System.Object>>::Subscribe(UniRx.IObserver`1<T>)
extern "C"  Il2CppObject * OperatorObservableBase_1_Subscribe_m2541197948_gshared (OperatorObservableBase_1_t2386433262 * __this, Il2CppObject* ___observer0, const MethodInfo* method);
#define OperatorObservableBase_1_Subscribe_m2541197948(__this, ___observer0, method) ((  Il2CppObject * (*) (OperatorObservableBase_1_t2386433262 *, Il2CppObject*, const MethodInfo*))OperatorObservableBase_1_Subscribe_m2541197948_gshared)(__this, ___observer0, method)
