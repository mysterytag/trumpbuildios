﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// System.Action`1<System.Object>
struct Action_1_t985559125;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AdToApp.AndroidWrapper.AdToAppAndroidWrapper
struct  AdToAppAndroidWrapper_t877850908  : public Il2CppObject
{
public:

public:
};

struct AdToAppAndroidWrapper_t877850908_StaticFields
{
public:
	// System.Action`1<System.Object> AdToApp.AndroidWrapper.AdToAppAndroidWrapper::<>f__am$cache0
	Action_1_t985559125 * ___U3CU3Ef__amU24cache0_1;
	// System.Action`1<System.Object> AdToApp.AndroidWrapper.AdToAppAndroidWrapper::<>f__am$cache1
	Action_1_t985559125 * ___U3CU3Ef__amU24cache1_2;
	// System.Action`1<System.Object> AdToApp.AndroidWrapper.AdToAppAndroidWrapper::<>f__am$cache2
	Action_1_t985559125 * ___U3CU3Ef__amU24cache2_3;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_1() { return static_cast<int32_t>(offsetof(AdToAppAndroidWrapper_t877850908_StaticFields, ___U3CU3Ef__amU24cache0_1)); }
	inline Action_1_t985559125 * get_U3CU3Ef__amU24cache0_1() const { return ___U3CU3Ef__amU24cache0_1; }
	inline Action_1_t985559125 ** get_address_of_U3CU3Ef__amU24cache0_1() { return &___U3CU3Ef__amU24cache0_1; }
	inline void set_U3CU3Ef__amU24cache0_1(Action_1_t985559125 * value)
	{
		___U3CU3Ef__amU24cache0_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache0_1, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache1_2() { return static_cast<int32_t>(offsetof(AdToAppAndroidWrapper_t877850908_StaticFields, ___U3CU3Ef__amU24cache1_2)); }
	inline Action_1_t985559125 * get_U3CU3Ef__amU24cache1_2() const { return ___U3CU3Ef__amU24cache1_2; }
	inline Action_1_t985559125 ** get_address_of_U3CU3Ef__amU24cache1_2() { return &___U3CU3Ef__amU24cache1_2; }
	inline void set_U3CU3Ef__amU24cache1_2(Action_1_t985559125 * value)
	{
		___U3CU3Ef__amU24cache1_2 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache1_2, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache2_3() { return static_cast<int32_t>(offsetof(AdToAppAndroidWrapper_t877850908_StaticFields, ___U3CU3Ef__amU24cache2_3)); }
	inline Action_1_t985559125 * get_U3CU3Ef__amU24cache2_3() const { return ___U3CU3Ef__amU24cache2_3; }
	inline Action_1_t985559125 ** get_address_of_U3CU3Ef__amU24cache2_3() { return &___U3CU3Ef__amU24cache2_3; }
	inline void set_U3CU3Ef__amU24cache2_3(Action_1_t985559125 * value)
	{
		___U3CU3Ef__amU24cache2_3 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache2_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
