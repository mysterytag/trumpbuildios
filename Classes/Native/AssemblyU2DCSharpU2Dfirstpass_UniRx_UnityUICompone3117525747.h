﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.UI.Scrollbar
struct Scrollbar_t1410649103;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.UnityUIComponentExtensions/<OnValueChangedAsObservable>c__AnonStoreyA8
struct  U3COnValueChangedAsObservableU3Ec__AnonStoreyA8_t3117525747  : public Il2CppObject
{
public:
	// UnityEngine.UI.Scrollbar UniRx.UnityUIComponentExtensions/<OnValueChangedAsObservable>c__AnonStoreyA8::scrollbar
	Scrollbar_t1410649103 * ___scrollbar_0;

public:
	inline static int32_t get_offset_of_scrollbar_0() { return static_cast<int32_t>(offsetof(U3COnValueChangedAsObservableU3Ec__AnonStoreyA8_t3117525747, ___scrollbar_0)); }
	inline Scrollbar_t1410649103 * get_scrollbar_0() const { return ___scrollbar_0; }
	inline Scrollbar_t1410649103 ** get_address_of_scrollbar_0() { return &___scrollbar_0; }
	inline void set_scrollbar_0(Scrollbar_t1410649103 * value)
	{
		___scrollbar_0 = value;
		Il2CppCodeGenWriteBarrier(&___scrollbar_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
