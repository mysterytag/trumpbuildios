﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// TestCoro/<Test2>c__Iterator2E
struct U3CTest2U3Ec__Iterator2E_t1832984490;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void TestCoro/<Test2>c__Iterator2E::.ctor()
extern "C"  void U3CTest2U3Ec__Iterator2E__ctor_m3359317357 (U3CTest2U3Ec__Iterator2E_t1832984490 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 TestCoro/<Test2>c__Iterator2E::System.Collections.Generic.IEnumerator<int>.get_Current()
extern "C"  int32_t U3CTest2U3Ec__Iterator2E_System_Collections_Generic_IEnumeratorU3CintU3E_get_Current_m1536441096 (U3CTest2U3Ec__Iterator2E_t1832984490 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object TestCoro/<Test2>c__Iterator2E::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CTest2U3Ec__Iterator2E_System_Collections_IEnumerator_get_Current_m1384639577 (U3CTest2U3Ec__Iterator2E_t1832984490 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean TestCoro/<Test2>c__Iterator2E::MoveNext()
extern "C"  bool U3CTest2U3Ec__Iterator2E_MoveNext_m4264636519 (U3CTest2U3Ec__Iterator2E_t1832984490 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TestCoro/<Test2>c__Iterator2E::Dispose()
extern "C"  void U3CTest2U3Ec__Iterator2E_Dispose_m2681823850 (U3CTest2U3Ec__Iterator2E_t1832984490 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TestCoro/<Test2>c__Iterator2E::Reset()
extern "C"  void U3CTest2U3Ec__Iterator2E_Reset_m1005750298 (U3CTest2U3Ec__Iterator2E_t1832984490 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
