﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// BarygaOpenIABManager
struct BarygaOpenIABManager_t3621535309;
// Cell`1<System.Single>
struct Cell_1_t1140306653;
// System.String
struct String_t;
// ICell`1<System.String>
struct ICell_1_t2520119879;
// OnePF.Purchase
struct Purchase_t160195573;
// OnePF.Inventory
struct Inventory_t2630562832;
// <>__AnonType4`2<System.Single,System.String>
struct U3CU3E__AnonType4_2_t4023684950;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String968488902.h"
#include "AssemblyU2DCSharpU2Dfirstpass_OnePF_Purchase160195573.h"
#include "AssemblyU2DCSharpU2Dfirstpass_OnePF_Inventory2630562832.h"

// System.Void BarygaOpenIABManager::.ctor()
extern "C"  void BarygaOpenIABManager__ctor_m2588467950 (BarygaOpenIABManager_t3621535309 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BarygaOpenIABManager::.cctor()
extern "C"  void BarygaOpenIABManager__cctor_m2450998911 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BarygaOpenIABManager::OnEnable()
extern "C"  void BarygaOpenIABManager_OnEnable_m3277444568 (BarygaOpenIABManager_t3621535309 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Cell`1<System.Single> BarygaOpenIABManager::Price(System.String)
extern "C"  Cell_1_t1140306653 * BarygaOpenIABManager_Price_m2542666208 (Il2CppObject * __this /* static, unused */, String_t* ___sku0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ICell`1<System.String> BarygaOpenIABManager::PriceFormatted(System.String,System.Boolean)
extern "C"  Il2CppObject* BarygaOpenIABManager_PriceFormatted_m817356499 (Il2CppObject * __this /* static, unused */, String_t* ___sku0, bool ___withCurrency1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BarygaOpenIABManager::Awake()
extern "C"  void BarygaOpenIABManager_Awake_m2826073169 (BarygaOpenIABManager_t3621535309 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BarygaOpenIABManager::Purchase(System.String)
extern "C"  void BarygaOpenIABManager_Purchase_m1913686411 (Il2CppObject * __this /* static, unused */, String_t* ___sku0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BarygaOpenIABManager::HandleconsumePurchaseSucceededEvent(OnePF.Purchase)
extern "C"  void BarygaOpenIABManager_HandleconsumePurchaseSucceededEvent_m3815734759 (BarygaOpenIABManager_t3621535309 * __this, Purchase_t160195573 * ___purchase0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean BarygaOpenIABManager::SkuEqual(System.String,System.String)
extern "C"  bool BarygaOpenIABManager_SkuEqual_m670741501 (BarygaOpenIABManager_t3621535309 * __this, String_t* ___sku0, String_t* ___equalTo1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BarygaOpenIABManager::PurchaseVerifiedWithPlayfab(System.String,System.String,System.String)
extern "C"  void BarygaOpenIABManager_PurchaseVerifiedWithPlayfab_m727525290 (BarygaOpenIABManager_t3621535309 * __this, String_t* ___originalJson0, String_t* ___base64Signature1, String_t* ___purchaseSku2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BarygaOpenIABManager::HandlebillingNotSupportedEvent(System.String)
extern "C"  void BarygaOpenIABManager_HandlebillingNotSupportedEvent_m2010201024 (BarygaOpenIABManager_t3621535309 * __this, String_t* ___obj0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BarygaOpenIABManager::HandlebillingSupportedEvent()
extern "C"  void BarygaOpenIABManager_HandlebillingSupportedEvent_m2801584843 (BarygaOpenIABManager_t3621535309 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BarygaOpenIABManager::OnDisable()
extern "C"  void BarygaOpenIABManager_OnDisable_m3257471061 (BarygaOpenIABManager_t3621535309 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BarygaOpenIABManager::HandlequeryInventoryFailedEvent(System.String)
extern "C"  void BarygaOpenIABManager_HandlequeryInventoryFailedEvent_m597549813 (BarygaOpenIABManager_t3621535309 * __this, String_t* ___obj0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BarygaOpenIABManager::HandlequeryInventorySucceededEvent(OnePF.Inventory)
extern "C"  void BarygaOpenIABManager_HandlequeryInventorySucceededEvent_m2897434301 (BarygaOpenIABManager_t3621535309 * __this, Inventory_t2630562832 * ___obj0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BarygaOpenIABManager::PurchaseFailedHandler(System.Int32,System.String)
extern "C"  void BarygaOpenIABManager_PurchaseFailedHandler_m2588183973 (BarygaOpenIABManager_t3621535309 * __this, int32_t ___arg10, String_t* ___arg21, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BarygaOpenIABManager::PurchaseSucceededHandler(OnePF.Purchase)
extern "C"  void BarygaOpenIABManager_PurchaseSucceededHandler_m1808011373 (BarygaOpenIABManager_t3621535309 * __this, Purchase_t160195573 * ___purchase0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BarygaOpenIABManager::Start()
extern "C"  void BarygaOpenIABManager_Start_m1535605742 (BarygaOpenIABManager_t3621535309 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BarygaOpenIABManager::MapPurchases()
extern "C"  void BarygaOpenIABManager_MapPurchases_m3812925356 (BarygaOpenIABManager_t3621535309 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BarygaOpenIABManager::InitOptions()
extern "C"  void BarygaOpenIABManager_InitOptions_m966481850 (BarygaOpenIABManager_t3621535309 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// <>__AnonType4`2<System.Single,System.String> BarygaOpenIABManager::<PriceFormatted>m__12D(System.Single,System.String)
extern "C"  U3CU3E__AnonType4_2_t4023684950 * BarygaOpenIABManager_U3CPriceFormattedU3Em__12D_m2628350432 (Il2CppObject * __this /* static, unused */, float ___f0, String_t* ___valuta1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
