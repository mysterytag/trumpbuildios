﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// IphoneVkConnector
struct IphoneVkConnector_t442788115;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String968488902.h"

// System.Void IphoneVkConnector::.ctor()
extern "C"  void IphoneVkConnector__ctor_m784910776 (IphoneVkConnector_t442788115 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IphoneVkConnector::VKLogin()
extern "C"  void IphoneVkConnector_VKLogin_m3369905770 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IphoneVkConnector::VKLogout()
extern "C"  void IphoneVkConnector_VKLogout_m1393686475 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean IphoneVkConnector::VKIsLoggedIn()
extern "C"  bool IphoneVkConnector_VKIsLoggedIn_m2109481438 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IphoneVkConnector::VKGetFriendList()
extern "C"  void IphoneVkConnector_VKGetFriendList_m2125484307 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IphoneVkConnector::VKGetProfile()
extern "C"  void IphoneVkConnector_VKGetProfile_m2950132084 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IphoneVkConnector::VKInvite(System.String)
extern "C"  void IphoneVkConnector_VKInvite_m3141910968 (Il2CppObject * __this /* static, unused */, String_t* ___idd0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IphoneVkConnector::VKPostAchievement(System.String,System.String)
extern "C"  void IphoneVkConnector_VKPostAchievement_m1594762702 (Il2CppObject * __this /* static, unused */, String_t* ___msg0, String_t* ___attach1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IphoneVkConnector::VKJoinGroup(System.String)
extern "C"  void IphoneVkConnector_VKJoinGroup_m4007743308 (Il2CppObject * __this /* static, unused */, String_t* ___idd0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IphoneVkConnector::CallUserProfile()
extern "C"  void IphoneVkConnector_CallUserProfile_m1977594550 (IphoneVkConnector_t442788115 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IphoneVkConnector::CallPostAchievement(System.String,System.String)
extern "C"  void IphoneVkConnector_CallPostAchievement_m3086668119 (IphoneVkConnector_t442788115 * __this, String_t* ___format0, String_t* ___s1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean IphoneVkConnector::IsLoggedIn()
extern "C"  bool IphoneVkConnector_IsLoggedIn_m3461750889 (IphoneVkConnector_t442788115 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IphoneVkConnector::CallLogin()
extern "C"  void IphoneVkConnector_CallLogin_m3160165697 (IphoneVkConnector_t442788115 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IphoneVkConnector::CallJoinGroup(System.String)
extern "C"  void IphoneVkConnector_CallJoinGroup_m2841008469 (IphoneVkConnector_t442788115 * __this, String_t* ___groupId0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IphoneVkConnector::CallGetFriendList()
extern "C"  void IphoneVkConnector_CallGetFriendList_m1160456426 (IphoneVkConnector_t442788115 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IphoneVkConnector::CallInviteFriend(System.String)
extern "C"  void IphoneVkConnector_CallInviteFriend_m1444304721 (IphoneVkConnector_t442788115 * __this, String_t* ___friendId0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IphoneVkConnector::CallLogout()
extern "C"  void IphoneVkConnector_CallLogout_m3481678804 (IphoneVkConnector_t442788115 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
