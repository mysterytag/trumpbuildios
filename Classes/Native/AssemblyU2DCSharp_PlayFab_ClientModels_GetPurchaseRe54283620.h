﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// System.Collections.Generic.List`1<PlayFab.ClientModels.ItemInstance>
struct List_1_t1322103281;

#include "AssemblyU2DCSharp_PlayFab_Internal_PlayFabResultCom379512675.h"
#include "mscorlib_System_DateTime339033936.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PlayFab.ClientModels.GetPurchaseResult
struct  GetPurchaseResult_t54283620  : public PlayFabResultCommon_t379512675
{
public:
	// System.String PlayFab.ClientModels.GetPurchaseResult::<OrderId>k__BackingField
	String_t* ___U3COrderIdU3Ek__BackingField_2;
	// System.String PlayFab.ClientModels.GetPurchaseResult::<PaymentProvider>k__BackingField
	String_t* ___U3CPaymentProviderU3Ek__BackingField_3;
	// System.String PlayFab.ClientModels.GetPurchaseResult::<TransactionId>k__BackingField
	String_t* ___U3CTransactionIdU3Ek__BackingField_4;
	// System.String PlayFab.ClientModels.GetPurchaseResult::<TransactionStatus>k__BackingField
	String_t* ___U3CTransactionStatusU3Ek__BackingField_5;
	// System.DateTime PlayFab.ClientModels.GetPurchaseResult::<PurchaseDate>k__BackingField
	DateTime_t339033936  ___U3CPurchaseDateU3Ek__BackingField_6;
	// System.Collections.Generic.List`1<PlayFab.ClientModels.ItemInstance> PlayFab.ClientModels.GetPurchaseResult::<Items>k__BackingField
	List_1_t1322103281 * ___U3CItemsU3Ek__BackingField_7;

public:
	inline static int32_t get_offset_of_U3COrderIdU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(GetPurchaseResult_t54283620, ___U3COrderIdU3Ek__BackingField_2)); }
	inline String_t* get_U3COrderIdU3Ek__BackingField_2() const { return ___U3COrderIdU3Ek__BackingField_2; }
	inline String_t** get_address_of_U3COrderIdU3Ek__BackingField_2() { return &___U3COrderIdU3Ek__BackingField_2; }
	inline void set_U3COrderIdU3Ek__BackingField_2(String_t* value)
	{
		___U3COrderIdU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier(&___U3COrderIdU3Ek__BackingField_2, value);
	}

	inline static int32_t get_offset_of_U3CPaymentProviderU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(GetPurchaseResult_t54283620, ___U3CPaymentProviderU3Ek__BackingField_3)); }
	inline String_t* get_U3CPaymentProviderU3Ek__BackingField_3() const { return ___U3CPaymentProviderU3Ek__BackingField_3; }
	inline String_t** get_address_of_U3CPaymentProviderU3Ek__BackingField_3() { return &___U3CPaymentProviderU3Ek__BackingField_3; }
	inline void set_U3CPaymentProviderU3Ek__BackingField_3(String_t* value)
	{
		___U3CPaymentProviderU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier(&___U3CPaymentProviderU3Ek__BackingField_3, value);
	}

	inline static int32_t get_offset_of_U3CTransactionIdU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(GetPurchaseResult_t54283620, ___U3CTransactionIdU3Ek__BackingField_4)); }
	inline String_t* get_U3CTransactionIdU3Ek__BackingField_4() const { return ___U3CTransactionIdU3Ek__BackingField_4; }
	inline String_t** get_address_of_U3CTransactionIdU3Ek__BackingField_4() { return &___U3CTransactionIdU3Ek__BackingField_4; }
	inline void set_U3CTransactionIdU3Ek__BackingField_4(String_t* value)
	{
		___U3CTransactionIdU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier(&___U3CTransactionIdU3Ek__BackingField_4, value);
	}

	inline static int32_t get_offset_of_U3CTransactionStatusU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(GetPurchaseResult_t54283620, ___U3CTransactionStatusU3Ek__BackingField_5)); }
	inline String_t* get_U3CTransactionStatusU3Ek__BackingField_5() const { return ___U3CTransactionStatusU3Ek__BackingField_5; }
	inline String_t** get_address_of_U3CTransactionStatusU3Ek__BackingField_5() { return &___U3CTransactionStatusU3Ek__BackingField_5; }
	inline void set_U3CTransactionStatusU3Ek__BackingField_5(String_t* value)
	{
		___U3CTransactionStatusU3Ek__BackingField_5 = value;
		Il2CppCodeGenWriteBarrier(&___U3CTransactionStatusU3Ek__BackingField_5, value);
	}

	inline static int32_t get_offset_of_U3CPurchaseDateU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(GetPurchaseResult_t54283620, ___U3CPurchaseDateU3Ek__BackingField_6)); }
	inline DateTime_t339033936  get_U3CPurchaseDateU3Ek__BackingField_6() const { return ___U3CPurchaseDateU3Ek__BackingField_6; }
	inline DateTime_t339033936 * get_address_of_U3CPurchaseDateU3Ek__BackingField_6() { return &___U3CPurchaseDateU3Ek__BackingField_6; }
	inline void set_U3CPurchaseDateU3Ek__BackingField_6(DateTime_t339033936  value)
	{
		___U3CPurchaseDateU3Ek__BackingField_6 = value;
	}

	inline static int32_t get_offset_of_U3CItemsU3Ek__BackingField_7() { return static_cast<int32_t>(offsetof(GetPurchaseResult_t54283620, ___U3CItemsU3Ek__BackingField_7)); }
	inline List_1_t1322103281 * get_U3CItemsU3Ek__BackingField_7() const { return ___U3CItemsU3Ek__BackingField_7; }
	inline List_1_t1322103281 ** get_address_of_U3CItemsU3Ek__BackingField_7() { return &___U3CItemsU3Ek__BackingField_7; }
	inline void set_U3CItemsU3Ek__BackingField_7(List_1_t1322103281 * value)
	{
		___U3CItemsU3Ek__BackingField_7 = value;
		Il2CppCodeGenWriteBarrier(&___U3CItemsU3Ek__BackingField_7, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
