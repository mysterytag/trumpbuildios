﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.PlayFabClientAPI/GrantCharacterToUserRequestCallback
struct GrantCharacterToUserRequestCallback_t420782753;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;
// PlayFab.ClientModels.GrantCharacterToUserRequest
struct GrantCharacterToUserRequest_t1227105868;
// System.IAsyncResult
struct IAsyncResult_t537683269;
// System.AsyncCallback
struct AsyncCallback_t1363551830;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_IntPtr676692020.h"
#include "mscorlib_System_String968488902.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_GrantCharac1227105868.h"
#include "mscorlib_System_AsyncCallback1363551830.h"

// System.Void PlayFab.PlayFabClientAPI/GrantCharacterToUserRequestCallback::.ctor(System.Object,System.IntPtr)
extern "C"  void GrantCharacterToUserRequestCallback__ctor_m3177138864 (GrantCharacterToUserRequestCallback_t420782753 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/GrantCharacterToUserRequestCallback::Invoke(System.String,System.Int32,PlayFab.ClientModels.GrantCharacterToUserRequest,System.Object)
extern "C"  void GrantCharacterToUserRequestCallback_Invoke_m4272246039 (GrantCharacterToUserRequestCallback_t420782753 * __this, String_t* ___urlPath0, int32_t ___callId1, GrantCharacterToUserRequest_t1227105868 * ___request2, Il2CppObject * ___customData3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
extern "C" void pinvoke_delegate_wrapper_GrantCharacterToUserRequestCallback_t420782753(Il2CppObject* delegate, String_t* ___urlPath0, int32_t ___callId1, GrantCharacterToUserRequest_t1227105868 * ___request2, Il2CppObject * ___customData3);
// System.IAsyncResult PlayFab.PlayFabClientAPI/GrantCharacterToUserRequestCallback::BeginInvoke(System.String,System.Int32,PlayFab.ClientModels.GrantCharacterToUserRequest,System.Object,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * GrantCharacterToUserRequestCallback_BeginInvoke_m1903039742 (GrantCharacterToUserRequestCallback_t420782753 * __this, String_t* ___urlPath0, int32_t ___callId1, GrantCharacterToUserRequest_t1227105868 * ___request2, Il2CppObject * ___customData3, AsyncCallback_t1363551830 * ___callback4, Il2CppObject * ___object5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/GrantCharacterToUserRequestCallback::EndInvoke(System.IAsyncResult)
extern "C"  void GrantCharacterToUserRequestCallback_EndInvoke_m2229507264 (GrantCharacterToUserRequestCallback_t420782753 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
