﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.BehaviorSubject`1/Subscription<System.Object>
struct Subscription_t2911320240;
// UniRx.BehaviorSubject`1<System.Object>
struct BehaviorSubject_1_t2662307150;
// UniRx.IObserver`1<System.Object>
struct IObserver_1_t3049105323;

#include "codegen/il2cpp-codegen.h"

// System.Void UniRx.BehaviorSubject`1/Subscription<System.Object>::.ctor(UniRx.BehaviorSubject`1<T>,UniRx.IObserver`1<T>)
extern "C"  void Subscription__ctor_m3932765271_gshared (Subscription_t2911320240 * __this, BehaviorSubject_1_t2662307150 * ___parent0, Il2CppObject* ___unsubscribeTarget1, const MethodInfo* method);
#define Subscription__ctor_m3932765271(__this, ___parent0, ___unsubscribeTarget1, method) ((  void (*) (Subscription_t2911320240 *, BehaviorSubject_1_t2662307150 *, Il2CppObject*, const MethodInfo*))Subscription__ctor_m3932765271_gshared)(__this, ___parent0, ___unsubscribeTarget1, method)
// System.Void UniRx.BehaviorSubject`1/Subscription<System.Object>::Dispose()
extern "C"  void Subscription_Dispose_m587048817_gshared (Subscription_t2911320240 * __this, const MethodInfo* method);
#define Subscription_Dispose_m587048817(__this, method) ((  void (*) (Subscription_t2911320240 *, const MethodInfo*))Subscription_Dispose_m587048817_gshared)(__this, method)
