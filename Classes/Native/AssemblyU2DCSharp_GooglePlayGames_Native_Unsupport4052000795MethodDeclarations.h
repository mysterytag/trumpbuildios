﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GooglePlayGames.Native.UnsupportedAppStateClient
struct UnsupportedAppStateClient_t4052000795;
// System.String
struct String_t;
// GooglePlayGames.BasicApi.OnStateLoadedListener
struct OnStateLoadedListener_t1907554349;
// System.Byte[]
struct ByteU5BU5D_t58506160;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String968488902.h"

// System.Void GooglePlayGames.Native.UnsupportedAppStateClient::.ctor(System.String)
extern "C"  void UnsupportedAppStateClient__ctor_m3648017018 (UnsupportedAppStateClient_t4052000795 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.UnsupportedAppStateClient::LoadState(System.Int32,GooglePlayGames.BasicApi.OnStateLoadedListener)
extern "C"  void UnsupportedAppStateClient_LoadState_m885952705 (UnsupportedAppStateClient_t4052000795 * __this, int32_t ___slot0, Il2CppObject * ___listener1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.UnsupportedAppStateClient::UpdateState(System.Int32,System.Byte[],GooglePlayGames.BasicApi.OnStateLoadedListener)
extern "C"  void UnsupportedAppStateClient_UpdateState_m1618234369 (UnsupportedAppStateClient_t4052000795 * __this, int32_t ___slot0, ByteU5BU5D_t58506160* ___data1, Il2CppObject * ___listener2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
