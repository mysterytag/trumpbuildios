﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.DelayObservable`1/Delay<System.Object>
struct Delay_t3380693988;
// UniRx.Operators.DelayObservable`1<System.Object>
struct DelayObservable_1_t2635131389;
// UniRx.IObserver`1<System.Object>
struct IObserver_1_t3049105323;
// System.IDisposable
struct IDisposable_t1628921374;
// System.Object
struct Il2CppObject;
// System.Exception
struct Exception_t1967233988;
// System.Action`1<System.TimeSpan>
struct Action_1_t912315597;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_Exception1967233988.h"

// System.Void UniRx.Operators.DelayObservable`1/Delay<System.Object>::.ctor(UniRx.Operators.DelayObservable`1<T>,UniRx.IObserver`1<T>,System.IDisposable)
extern "C"  void Delay__ctor_m1727646917_gshared (Delay_t3380693988 * __this, DelayObservable_1_t2635131389 * ___parent0, Il2CppObject* ___observer1, Il2CppObject * ___cancel2, const MethodInfo* method);
#define Delay__ctor_m1727646917(__this, ___parent0, ___observer1, ___cancel2, method) ((  void (*) (Delay_t3380693988 *, DelayObservable_1_t2635131389 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))Delay__ctor_m1727646917_gshared)(__this, ___parent0, ___observer1, ___cancel2, method)
// System.IDisposable UniRx.Operators.DelayObservable`1/Delay<System.Object>::Run()
extern "C"  Il2CppObject * Delay_Run_m2426087989_gshared (Delay_t3380693988 * __this, const MethodInfo* method);
#define Delay_Run_m2426087989(__this, method) ((  Il2CppObject * (*) (Delay_t3380693988 *, const MethodInfo*))Delay_Run_m2426087989_gshared)(__this, method)
// System.Void UniRx.Operators.DelayObservable`1/Delay<System.Object>::OnNext(T)
extern "C"  void Delay_OnNext_m3212416655_gshared (Delay_t3380693988 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Delay_OnNext_m3212416655(__this, ___value0, method) ((  void (*) (Delay_t3380693988 *, Il2CppObject *, const MethodInfo*))Delay_OnNext_m3212416655_gshared)(__this, ___value0, method)
// System.Void UniRx.Operators.DelayObservable`1/Delay<System.Object>::OnError(System.Exception)
extern "C"  void Delay_OnError_m800371614_gshared (Delay_t3380693988 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method);
#define Delay_OnError_m800371614(__this, ___error0, method) ((  void (*) (Delay_t3380693988 *, Exception_t1967233988 *, const MethodInfo*))Delay_OnError_m800371614_gshared)(__this, ___error0, method)
// System.Void UniRx.Operators.DelayObservable`1/Delay<System.Object>::OnCompleted()
extern "C"  void Delay_OnCompleted_m4188890097_gshared (Delay_t3380693988 * __this, const MethodInfo* method);
#define Delay_OnCompleted_m4188890097(__this, method) ((  void (*) (Delay_t3380693988 *, const MethodInfo*))Delay_OnCompleted_m4188890097_gshared)(__this, method)
// System.Void UniRx.Operators.DelayObservable`1/Delay<System.Object>::DrainQueue(System.Action`1<System.TimeSpan>)
extern "C"  void Delay_DrainQueue_m1278116664_gshared (Delay_t3380693988 * __this, Action_1_t912315597 * ___recurse0, const MethodInfo* method);
#define Delay_DrainQueue_m1278116664(__this, ___recurse0, method) ((  void (*) (Delay_t3380693988 *, Action_1_t912315597 *, const MethodInfo*))Delay_DrainQueue_m1278116664_gshared)(__this, ___recurse0, method)
