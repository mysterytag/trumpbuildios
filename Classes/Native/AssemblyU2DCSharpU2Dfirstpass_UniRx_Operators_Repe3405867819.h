﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UniRx.SingleAssignmentDisposable
struct SingleAssignmentDisposable_t2336378823;
// UnityEngine.GameObject
struct GameObject_t4012695102;
// UniRx.IObservable`1<System.Object>
struct IObservable_1_t595904784;
// UniRx.IObserver`1<System.Object>
struct IObserver_1_t3049105323;
// System.Object
struct Il2CppObject;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Operators.RepeatUntilObservable`1/RepeatUntil/<SubscribeAfterEndOfFrame>c__Iterator28<System.Object>
struct  U3CSubscribeAfterEndOfFrameU3Ec__Iterator28_t3405867819  : public Il2CppObject
{
public:
	// UniRx.SingleAssignmentDisposable UniRx.Operators.RepeatUntilObservable`1/RepeatUntil/<SubscribeAfterEndOfFrame>c__Iterator28::d
	SingleAssignmentDisposable_t2336378823 * ___d_0;
	// UnityEngine.GameObject UniRx.Operators.RepeatUntilObservable`1/RepeatUntil/<SubscribeAfterEndOfFrame>c__Iterator28::lifeTimeChecker
	GameObject_t4012695102 * ___lifeTimeChecker_1;
	// UniRx.IObservable`1<T> UniRx.Operators.RepeatUntilObservable`1/RepeatUntil/<SubscribeAfterEndOfFrame>c__Iterator28::source
	Il2CppObject* ___source_2;
	// UniRx.IObserver`1<T> UniRx.Operators.RepeatUntilObservable`1/RepeatUntil/<SubscribeAfterEndOfFrame>c__Iterator28::observer
	Il2CppObject* ___observer_3;
	// System.Int32 UniRx.Operators.RepeatUntilObservable`1/RepeatUntil/<SubscribeAfterEndOfFrame>c__Iterator28::$PC
	int32_t ___U24PC_4;
	// System.Object UniRx.Operators.RepeatUntilObservable`1/RepeatUntil/<SubscribeAfterEndOfFrame>c__Iterator28::$current
	Il2CppObject * ___U24current_5;
	// UniRx.SingleAssignmentDisposable UniRx.Operators.RepeatUntilObservable`1/RepeatUntil/<SubscribeAfterEndOfFrame>c__Iterator28::<$>d
	SingleAssignmentDisposable_t2336378823 * ___U3CU24U3Ed_6;
	// UnityEngine.GameObject UniRx.Operators.RepeatUntilObservable`1/RepeatUntil/<SubscribeAfterEndOfFrame>c__Iterator28::<$>lifeTimeChecker
	GameObject_t4012695102 * ___U3CU24U3ElifeTimeChecker_7;
	// UniRx.IObservable`1<T> UniRx.Operators.RepeatUntilObservable`1/RepeatUntil/<SubscribeAfterEndOfFrame>c__Iterator28::<$>source
	Il2CppObject* ___U3CU24U3Esource_8;
	// UniRx.IObserver`1<T> UniRx.Operators.RepeatUntilObservable`1/RepeatUntil/<SubscribeAfterEndOfFrame>c__Iterator28::<$>observer
	Il2CppObject* ___U3CU24U3Eobserver_9;

public:
	inline static int32_t get_offset_of_d_0() { return static_cast<int32_t>(offsetof(U3CSubscribeAfterEndOfFrameU3Ec__Iterator28_t3405867819, ___d_0)); }
	inline SingleAssignmentDisposable_t2336378823 * get_d_0() const { return ___d_0; }
	inline SingleAssignmentDisposable_t2336378823 ** get_address_of_d_0() { return &___d_0; }
	inline void set_d_0(SingleAssignmentDisposable_t2336378823 * value)
	{
		___d_0 = value;
		Il2CppCodeGenWriteBarrier(&___d_0, value);
	}

	inline static int32_t get_offset_of_lifeTimeChecker_1() { return static_cast<int32_t>(offsetof(U3CSubscribeAfterEndOfFrameU3Ec__Iterator28_t3405867819, ___lifeTimeChecker_1)); }
	inline GameObject_t4012695102 * get_lifeTimeChecker_1() const { return ___lifeTimeChecker_1; }
	inline GameObject_t4012695102 ** get_address_of_lifeTimeChecker_1() { return &___lifeTimeChecker_1; }
	inline void set_lifeTimeChecker_1(GameObject_t4012695102 * value)
	{
		___lifeTimeChecker_1 = value;
		Il2CppCodeGenWriteBarrier(&___lifeTimeChecker_1, value);
	}

	inline static int32_t get_offset_of_source_2() { return static_cast<int32_t>(offsetof(U3CSubscribeAfterEndOfFrameU3Ec__Iterator28_t3405867819, ___source_2)); }
	inline Il2CppObject* get_source_2() const { return ___source_2; }
	inline Il2CppObject** get_address_of_source_2() { return &___source_2; }
	inline void set_source_2(Il2CppObject* value)
	{
		___source_2 = value;
		Il2CppCodeGenWriteBarrier(&___source_2, value);
	}

	inline static int32_t get_offset_of_observer_3() { return static_cast<int32_t>(offsetof(U3CSubscribeAfterEndOfFrameU3Ec__Iterator28_t3405867819, ___observer_3)); }
	inline Il2CppObject* get_observer_3() const { return ___observer_3; }
	inline Il2CppObject** get_address_of_observer_3() { return &___observer_3; }
	inline void set_observer_3(Il2CppObject* value)
	{
		___observer_3 = value;
		Il2CppCodeGenWriteBarrier(&___observer_3, value);
	}

	inline static int32_t get_offset_of_U24PC_4() { return static_cast<int32_t>(offsetof(U3CSubscribeAfterEndOfFrameU3Ec__Iterator28_t3405867819, ___U24PC_4)); }
	inline int32_t get_U24PC_4() const { return ___U24PC_4; }
	inline int32_t* get_address_of_U24PC_4() { return &___U24PC_4; }
	inline void set_U24PC_4(int32_t value)
	{
		___U24PC_4 = value;
	}

	inline static int32_t get_offset_of_U24current_5() { return static_cast<int32_t>(offsetof(U3CSubscribeAfterEndOfFrameU3Ec__Iterator28_t3405867819, ___U24current_5)); }
	inline Il2CppObject * get_U24current_5() const { return ___U24current_5; }
	inline Il2CppObject ** get_address_of_U24current_5() { return &___U24current_5; }
	inline void set_U24current_5(Il2CppObject * value)
	{
		___U24current_5 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_5, value);
	}

	inline static int32_t get_offset_of_U3CU24U3Ed_6() { return static_cast<int32_t>(offsetof(U3CSubscribeAfterEndOfFrameU3Ec__Iterator28_t3405867819, ___U3CU24U3Ed_6)); }
	inline SingleAssignmentDisposable_t2336378823 * get_U3CU24U3Ed_6() const { return ___U3CU24U3Ed_6; }
	inline SingleAssignmentDisposable_t2336378823 ** get_address_of_U3CU24U3Ed_6() { return &___U3CU24U3Ed_6; }
	inline void set_U3CU24U3Ed_6(SingleAssignmentDisposable_t2336378823 * value)
	{
		___U3CU24U3Ed_6 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU24U3Ed_6, value);
	}

	inline static int32_t get_offset_of_U3CU24U3ElifeTimeChecker_7() { return static_cast<int32_t>(offsetof(U3CSubscribeAfterEndOfFrameU3Ec__Iterator28_t3405867819, ___U3CU24U3ElifeTimeChecker_7)); }
	inline GameObject_t4012695102 * get_U3CU24U3ElifeTimeChecker_7() const { return ___U3CU24U3ElifeTimeChecker_7; }
	inline GameObject_t4012695102 ** get_address_of_U3CU24U3ElifeTimeChecker_7() { return &___U3CU24U3ElifeTimeChecker_7; }
	inline void set_U3CU24U3ElifeTimeChecker_7(GameObject_t4012695102 * value)
	{
		___U3CU24U3ElifeTimeChecker_7 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU24U3ElifeTimeChecker_7, value);
	}

	inline static int32_t get_offset_of_U3CU24U3Esource_8() { return static_cast<int32_t>(offsetof(U3CSubscribeAfterEndOfFrameU3Ec__Iterator28_t3405867819, ___U3CU24U3Esource_8)); }
	inline Il2CppObject* get_U3CU24U3Esource_8() const { return ___U3CU24U3Esource_8; }
	inline Il2CppObject** get_address_of_U3CU24U3Esource_8() { return &___U3CU24U3Esource_8; }
	inline void set_U3CU24U3Esource_8(Il2CppObject* value)
	{
		___U3CU24U3Esource_8 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU24U3Esource_8, value);
	}

	inline static int32_t get_offset_of_U3CU24U3Eobserver_9() { return static_cast<int32_t>(offsetof(U3CSubscribeAfterEndOfFrameU3Ec__Iterator28_t3405867819, ___U3CU24U3Eobserver_9)); }
	inline Il2CppObject* get_U3CU24U3Eobserver_9() const { return ___U3CU24U3Eobserver_9; }
	inline Il2CppObject** get_address_of_U3CU24U3Eobserver_9() { return &___U3CU24U3Eobserver_9; }
	inline void set_U3CU24U3Eobserver_9(Il2CppObject* value)
	{
		___U3CU24U3Eobserver_9 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU24U3Eobserver_9, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
