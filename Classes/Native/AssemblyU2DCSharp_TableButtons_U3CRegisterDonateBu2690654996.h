﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// SettingsCell
struct SettingsCell_t698172485;
// TableButtons/<RegisterDonateButtons>c__AnonStorey169
struct U3CRegisterDonateButtonsU3Ec__AnonStorey169_t2690655000;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TableButtons/<RegisterDonateButtons>c__AnonStorey169/<RegisterDonateButtons>c__AnonStorey165
struct  U3CRegisterDonateButtonsU3Ec__AnonStorey165_t2690654996  : public Il2CppObject
{
public:
	// SettingsCell TableButtons/<RegisterDonateButtons>c__AnonStorey169/<RegisterDonateButtons>c__AnonStorey165::cell
	SettingsCell_t698172485 * ___cell_0;
	// TableButtons/<RegisterDonateButtons>c__AnonStorey169 TableButtons/<RegisterDonateButtons>c__AnonStorey169/<RegisterDonateButtons>c__AnonStorey165::<>f__ref$361
	U3CRegisterDonateButtonsU3Ec__AnonStorey169_t2690655000 * ___U3CU3Ef__refU24361_1;

public:
	inline static int32_t get_offset_of_cell_0() { return static_cast<int32_t>(offsetof(U3CRegisterDonateButtonsU3Ec__AnonStorey165_t2690654996, ___cell_0)); }
	inline SettingsCell_t698172485 * get_cell_0() const { return ___cell_0; }
	inline SettingsCell_t698172485 ** get_address_of_cell_0() { return &___cell_0; }
	inline void set_cell_0(SettingsCell_t698172485 * value)
	{
		___cell_0 = value;
		Il2CppCodeGenWriteBarrier(&___cell_0, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__refU24361_1() { return static_cast<int32_t>(offsetof(U3CRegisterDonateButtonsU3Ec__AnonStorey165_t2690654996, ___U3CU3Ef__refU24361_1)); }
	inline U3CRegisterDonateButtonsU3Ec__AnonStorey169_t2690655000 * get_U3CU3Ef__refU24361_1() const { return ___U3CU3Ef__refU24361_1; }
	inline U3CRegisterDonateButtonsU3Ec__AnonStorey169_t2690655000 ** get_address_of_U3CU3Ef__refU24361_1() { return &___U3CU3Ef__refU24361_1; }
	inline void set_U3CU3Ef__refU24361_1(U3CRegisterDonateButtonsU3Ec__AnonStorey169_t2690655000 * value)
	{
		___U3CU3Ef__refU24361_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__refU24361_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
