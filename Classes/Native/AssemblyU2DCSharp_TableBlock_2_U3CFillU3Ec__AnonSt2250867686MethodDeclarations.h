﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// TableBlock`2/<Fill>c__AnonStoreyEB<System.Object,System.Object>
struct U3CFillU3Ec__AnonStoreyEB_t2250867686;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"

// System.Void TableBlock`2/<Fill>c__AnonStoreyEB<System.Object,System.Object>::.ctor()
extern "C"  void U3CFillU3Ec__AnonStoreyEB__ctor_m143819720_gshared (U3CFillU3Ec__AnonStoreyEB_t2250867686 * __this, const MethodInfo* method);
#define U3CFillU3Ec__AnonStoreyEB__ctor_m143819720(__this, method) ((  void (*) (U3CFillU3Ec__AnonStoreyEB_t2250867686 *, const MethodInfo*))U3CFillU3Ec__AnonStoreyEB__ctor_m143819720_gshared)(__this, method)
// System.Boolean TableBlock`2/<Fill>c__AnonStoreyEB<System.Object,System.Object>::<>m__13C(TPrefab)
extern "C"  bool U3CFillU3Ec__AnonStoreyEB_U3CU3Em__13C_m3020003420_gshared (U3CFillU3Ec__AnonStoreyEB_t2250867686 * __this, Il2CppObject * ___p0, const MethodInfo* method);
#define U3CFillU3Ec__AnonStoreyEB_U3CU3Em__13C_m3020003420(__this, ___p0, method) ((  bool (*) (U3CFillU3Ec__AnonStoreyEB_t2250867686 *, Il2CppObject *, const MethodInfo*))U3CFillU3Ec__AnonStoreyEB_U3CU3Em__13C_m3020003420_gshared)(__this, ___p0, method)
// System.Void TableBlock`2/<Fill>c__AnonStoreyEB<System.Object,System.Object>::<>m__13D()
extern "C"  void U3CFillU3Ec__AnonStoreyEB_U3CU3Em__13D_m985795923_gshared (U3CFillU3Ec__AnonStoreyEB_t2250867686 * __this, const MethodInfo* method);
#define U3CFillU3Ec__AnonStoreyEB_U3CU3Em__13D_m985795923(__this, method) ((  void (*) (U3CFillU3Ec__AnonStoreyEB_t2250867686 *, const MethodInfo*))U3CFillU3Ec__AnonStoreyEB_U3CU3Em__13D_m985795923_gshared)(__this, method)
