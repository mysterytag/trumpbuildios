﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// AnonymousCell`1<System.Object>
struct AnonymousCell_1_t103592921;
// System.Func`3<System.Action`1<System.Object>,Priority,System.IDisposable>
struct Func_3_t1585443616;
// System.Func`1<System.Object>
struct Func_1_t1979887667;
// System.Object
struct Il2CppObject;
// System.IDisposable
struct IDisposable_t1628921374;
// System.Action`1<System.Object>
struct Action_1_t985559125;
// UniRx.IObserver`1<System.Object>
struct IObserver_1_t3049105323;
// System.Action
struct Action_t437523947;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Priority3194150340.h"
#include "System_Core_System_Action437523947.h"

// System.Void AnonymousCell`1<System.Object>::.ctor()
extern "C"  void AnonymousCell_1__ctor_m2866128927_gshared (AnonymousCell_1_t103592921 * __this, const MethodInfo* method);
#define AnonymousCell_1__ctor_m2866128927(__this, method) ((  void (*) (AnonymousCell_1_t103592921 *, const MethodInfo*))AnonymousCell_1__ctor_m2866128927_gshared)(__this, method)
// System.Void AnonymousCell`1<System.Object>::.ctor(System.Func`3<System.Action`1<T>,Priority,System.IDisposable>,System.Func`1<T>)
extern "C"  void AnonymousCell_1__ctor_m1853677587_gshared (AnonymousCell_1_t103592921 * __this, Func_3_t1585443616 * ___subscribe0, Func_1_t1979887667 * ___current1, const MethodInfo* method);
#define AnonymousCell_1__ctor_m1853677587(__this, ___subscribe0, ___current1, method) ((  void (*) (AnonymousCell_1_t103592921 *, Func_3_t1585443616 *, Func_1_t1979887667 *, const MethodInfo*))AnonymousCell_1__ctor_m1853677587_gshared)(__this, ___subscribe0, ___current1, method)
// T AnonymousCell`1<System.Object>::get_value()
extern "C"  Il2CppObject * AnonymousCell_1_get_value_m2881526726_gshared (AnonymousCell_1_t103592921 * __this, const MethodInfo* method);
#define AnonymousCell_1_get_value_m2881526726(__this, method) ((  Il2CppObject * (*) (AnonymousCell_1_t103592921 *, const MethodInfo*))AnonymousCell_1_get_value_m2881526726_gshared)(__this, method)
// System.IDisposable AnonymousCell`1<System.Object>::ListenUpdates(System.Action`1<T>,Priority)
extern "C"  Il2CppObject * AnonymousCell_1_ListenUpdates_m2243511189_gshared (AnonymousCell_1_t103592921 * __this, Action_1_t985559125 * ___reaction0, int32_t ___p1, const MethodInfo* method);
#define AnonymousCell_1_ListenUpdates_m2243511189(__this, ___reaction0, ___p1, method) ((  Il2CppObject * (*) (AnonymousCell_1_t103592921 *, Action_1_t985559125 *, int32_t, const MethodInfo*))AnonymousCell_1_ListenUpdates_m2243511189_gshared)(__this, ___reaction0, ___p1, method)
// System.IDisposable AnonymousCell`1<System.Object>::Bind(System.Action`1<T>,Priority)
extern "C"  Il2CppObject * AnonymousCell_1_Bind_m1087176461_gshared (AnonymousCell_1_t103592921 * __this, Action_1_t985559125 * ___reaction0, int32_t ___p1, const MethodInfo* method);
#define AnonymousCell_1_Bind_m1087176461(__this, ___reaction0, ___p1, method) ((  Il2CppObject * (*) (AnonymousCell_1_t103592921 *, Action_1_t985559125 *, int32_t, const MethodInfo*))AnonymousCell_1_Bind_m1087176461_gshared)(__this, ___reaction0, ___p1, method)
// System.IDisposable AnonymousCell`1<System.Object>::Subscribe(UniRx.IObserver`1<T>)
extern "C"  Il2CppObject * AnonymousCell_1_Subscribe_m1803732742_gshared (AnonymousCell_1_t103592921 * __this, Il2CppObject* ___observer0, const MethodInfo* method);
#define AnonymousCell_1_Subscribe_m1803732742(__this, ___observer0, method) ((  Il2CppObject * (*) (AnonymousCell_1_t103592921 *, Il2CppObject*, const MethodInfo*))AnonymousCell_1_Subscribe_m1803732742_gshared)(__this, ___observer0, method)
// System.IDisposable AnonymousCell`1<System.Object>::Listen(System.Action`1<T>,Priority)
extern "C"  Il2CppObject * AnonymousCell_1_Listen_m1987447683_gshared (AnonymousCell_1_t103592921 * __this, Action_1_t985559125 * ___reaction0, int32_t ___priority1, const MethodInfo* method);
#define AnonymousCell_1_Listen_m1987447683(__this, ___reaction0, ___priority1, method) ((  Il2CppObject * (*) (AnonymousCell_1_t103592921 *, Action_1_t985559125 *, int32_t, const MethodInfo*))AnonymousCell_1_Listen_m1987447683_gshared)(__this, ___reaction0, ___priority1, method)
// System.IDisposable AnonymousCell`1<System.Object>::Listen(System.Action,Priority)
extern "C"  Il2CppObject * AnonymousCell_1_Listen_m3510407108_gshared (AnonymousCell_1_t103592921 * __this, Action_t437523947 * ___action0, int32_t ___priority1, const MethodInfo* method);
#define AnonymousCell_1_Listen_m3510407108(__this, ___action0, ___priority1, method) ((  Il2CppObject * (*) (AnonymousCell_1_t103592921 *, Action_t437523947 *, int32_t, const MethodInfo*))AnonymousCell_1_Listen_m3510407108_gshared)(__this, ___action0, ___priority1, method)
// System.IDisposable AnonymousCell`1<System.Object>::OnChanged(System.Action,Priority)
extern "C"  Il2CppObject * AnonymousCell_1_OnChanged_m1656078788_gshared (AnonymousCell_1_t103592921 * __this, Action_t437523947 * ___action0, int32_t ___p1, const MethodInfo* method);
#define AnonymousCell_1_OnChanged_m1656078788(__this, ___action0, ___p1, method) ((  Il2CppObject * (*) (AnonymousCell_1_t103592921 *, Action_t437523947 *, int32_t, const MethodInfo*))AnonymousCell_1_OnChanged_m1656078788_gshared)(__this, ___action0, ___p1, method)
// System.Object AnonymousCell`1<System.Object>::get_valueObject()
extern "C"  Il2CppObject * AnonymousCell_1_get_valueObject_m701915439_gshared (AnonymousCell_1_t103592921 * __this, const MethodInfo* method);
#define AnonymousCell_1_get_valueObject_m701915439(__this, method) ((  Il2CppObject * (*) (AnonymousCell_1_t103592921 *, const MethodInfo*))AnonymousCell_1_get_valueObject_m701915439_gshared)(__this, method)
