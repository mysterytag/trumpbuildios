﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CellReactiveApi/<Map>c__AnonStorey10E`2/<Map>c__AnonStorey10F`2<System.Boolean,System.Boolean>
struct U3CMapU3Ec__AnonStorey10F_2_t985129795;

#include "codegen/il2cpp-codegen.h"

// System.Void CellReactiveApi/<Map>c__AnonStorey10E`2/<Map>c__AnonStorey10F`2<System.Boolean,System.Boolean>::.ctor()
extern "C"  void U3CMapU3Ec__AnonStorey10F_2__ctor_m1454680429_gshared (U3CMapU3Ec__AnonStorey10F_2_t985129795 * __this, const MethodInfo* method);
#define U3CMapU3Ec__AnonStorey10F_2__ctor_m1454680429(__this, method) ((  void (*) (U3CMapU3Ec__AnonStorey10F_2_t985129795 *, const MethodInfo*))U3CMapU3Ec__AnonStorey10F_2__ctor_m1454680429_gshared)(__this, method)
// System.Void CellReactiveApi/<Map>c__AnonStorey10E`2/<Map>c__AnonStorey10F`2<System.Boolean,System.Boolean>::<>m__197(T)
extern "C"  void U3CMapU3Ec__AnonStorey10F_2_U3CU3Em__197_m2641039811_gshared (U3CMapU3Ec__AnonStorey10F_2_t985129795 * __this, bool ___val0, const MethodInfo* method);
#define U3CMapU3Ec__AnonStorey10F_2_U3CU3Em__197_m2641039811(__this, ___val0, method) ((  void (*) (U3CMapU3Ec__AnonStorey10F_2_t985129795 *, bool, const MethodInfo*))U3CMapU3Ec__AnonStorey10F_2_U3CU3Em__197_m2641039811_gshared)(__this, ___val0, method)
