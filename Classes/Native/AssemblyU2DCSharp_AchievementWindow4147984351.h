﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.UI.Text
struct Text_t3286458198;
// AchievementDescription
struct AchievementDescription_t2323334509;
// BarygaVkController
struct BarygaVkController_t3617163921;
// FbController
struct FbController_t3069175192;

#include "AssemblyU2DCSharp_WindowBase3855465217.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AchievementWindow
struct  AchievementWindow_t4147984351  : public WindowBase_t3855465217
{
public:
	// UnityEngine.UI.Text AchievementWindow::title
	Text_t3286458198 * ___title_6;
	// UnityEngine.UI.Text AchievementWindow::youGot
	Text_t3286458198 * ___youGot_7;
	// UnityEngine.UI.Text AchievementWindow::name
	Text_t3286458198 * ___name_8;
	// UnityEngine.UI.Text AchievementWindow::wannaShare
	Text_t3286458198 * ___wannaShare_9;
	// AchievementDescription AchievementWindow::achievement
	AchievementDescription_t2323334509 * ___achievement_10;
	// BarygaVkController AchievementWindow::BarygaVkController
	BarygaVkController_t3617163921 * ___BarygaVkController_11;
	// FbController AchievementWindow::FbController
	FbController_t3069175192 * ___FbController_12;

public:
	inline static int32_t get_offset_of_title_6() { return static_cast<int32_t>(offsetof(AchievementWindow_t4147984351, ___title_6)); }
	inline Text_t3286458198 * get_title_6() const { return ___title_6; }
	inline Text_t3286458198 ** get_address_of_title_6() { return &___title_6; }
	inline void set_title_6(Text_t3286458198 * value)
	{
		___title_6 = value;
		Il2CppCodeGenWriteBarrier(&___title_6, value);
	}

	inline static int32_t get_offset_of_youGot_7() { return static_cast<int32_t>(offsetof(AchievementWindow_t4147984351, ___youGot_7)); }
	inline Text_t3286458198 * get_youGot_7() const { return ___youGot_7; }
	inline Text_t3286458198 ** get_address_of_youGot_7() { return &___youGot_7; }
	inline void set_youGot_7(Text_t3286458198 * value)
	{
		___youGot_7 = value;
		Il2CppCodeGenWriteBarrier(&___youGot_7, value);
	}

	inline static int32_t get_offset_of_name_8() { return static_cast<int32_t>(offsetof(AchievementWindow_t4147984351, ___name_8)); }
	inline Text_t3286458198 * get_name_8() const { return ___name_8; }
	inline Text_t3286458198 ** get_address_of_name_8() { return &___name_8; }
	inline void set_name_8(Text_t3286458198 * value)
	{
		___name_8 = value;
		Il2CppCodeGenWriteBarrier(&___name_8, value);
	}

	inline static int32_t get_offset_of_wannaShare_9() { return static_cast<int32_t>(offsetof(AchievementWindow_t4147984351, ___wannaShare_9)); }
	inline Text_t3286458198 * get_wannaShare_9() const { return ___wannaShare_9; }
	inline Text_t3286458198 ** get_address_of_wannaShare_9() { return &___wannaShare_9; }
	inline void set_wannaShare_9(Text_t3286458198 * value)
	{
		___wannaShare_9 = value;
		Il2CppCodeGenWriteBarrier(&___wannaShare_9, value);
	}

	inline static int32_t get_offset_of_achievement_10() { return static_cast<int32_t>(offsetof(AchievementWindow_t4147984351, ___achievement_10)); }
	inline AchievementDescription_t2323334509 * get_achievement_10() const { return ___achievement_10; }
	inline AchievementDescription_t2323334509 ** get_address_of_achievement_10() { return &___achievement_10; }
	inline void set_achievement_10(AchievementDescription_t2323334509 * value)
	{
		___achievement_10 = value;
		Il2CppCodeGenWriteBarrier(&___achievement_10, value);
	}

	inline static int32_t get_offset_of_BarygaVkController_11() { return static_cast<int32_t>(offsetof(AchievementWindow_t4147984351, ___BarygaVkController_11)); }
	inline BarygaVkController_t3617163921 * get_BarygaVkController_11() const { return ___BarygaVkController_11; }
	inline BarygaVkController_t3617163921 ** get_address_of_BarygaVkController_11() { return &___BarygaVkController_11; }
	inline void set_BarygaVkController_11(BarygaVkController_t3617163921 * value)
	{
		___BarygaVkController_11 = value;
		Il2CppCodeGenWriteBarrier(&___BarygaVkController_11, value);
	}

	inline static int32_t get_offset_of_FbController_12() { return static_cast<int32_t>(offsetof(AchievementWindow_t4147984351, ___FbController_12)); }
	inline FbController_t3069175192 * get_FbController_12() const { return ___FbController_12; }
	inline FbController_t3069175192 ** get_address_of_FbController_12() { return &___FbController_12; }
	inline void set_FbController_12(FbController_t3069175192 * value)
	{
		___FbController_12 = value;
		Il2CppCodeGenWriteBarrier(&___FbController_12, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
