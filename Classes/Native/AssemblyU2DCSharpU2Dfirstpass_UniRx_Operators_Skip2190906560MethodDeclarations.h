﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.SkipObservable`1/Skip<System.Object>
struct Skip_t2190906560;
// UniRx.Operators.SkipObservable`1<System.Object>
struct SkipObservable_1_t823380185;
// UniRx.IObserver`1<System.Object>
struct IObserver_1_t3049105323;
// System.IDisposable
struct IDisposable_t1628921374;
// System.Object
struct Il2CppObject;
// System.Exception
struct Exception_t1967233988;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_Exception1967233988.h"

// System.Void UniRx.Operators.SkipObservable`1/Skip<System.Object>::.ctor(UniRx.Operators.SkipObservable`1<T>,UniRx.IObserver`1<T>,System.IDisposable)
extern "C"  void Skip__ctor_m41513059_gshared (Skip_t2190906560 * __this, SkipObservable_1_t823380185 * ___parent0, Il2CppObject* ___observer1, Il2CppObject * ___cancel2, const MethodInfo* method);
#define Skip__ctor_m41513059(__this, ___parent0, ___observer1, ___cancel2, method) ((  void (*) (Skip_t2190906560 *, SkipObservable_1_t823380185 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))Skip__ctor_m41513059_gshared)(__this, ___parent0, ___observer1, ___cancel2, method)
// System.Void UniRx.Operators.SkipObservable`1/Skip<System.Object>::OnNext(T)
extern "C"  void Skip_OnNext_m301676479_gshared (Skip_t2190906560 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Skip_OnNext_m301676479(__this, ___value0, method) ((  void (*) (Skip_t2190906560 *, Il2CppObject *, const MethodInfo*))Skip_OnNext_m301676479_gshared)(__this, ___value0, method)
// System.Void UniRx.Operators.SkipObservable`1/Skip<System.Object>::OnError(System.Exception)
extern "C"  void Skip_OnError_m1420686542_gshared (Skip_t2190906560 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method);
#define Skip_OnError_m1420686542(__this, ___error0, method) ((  void (*) (Skip_t2190906560 *, Exception_t1967233988 *, const MethodInfo*))Skip_OnError_m1420686542_gshared)(__this, ___error0, method)
// System.Void UniRx.Operators.SkipObservable`1/Skip<System.Object>::OnCompleted()
extern "C"  void Skip_OnCompleted_m52096289_gshared (Skip_t2190906560 * __this, const MethodInfo* method);
#define Skip_OnCompleted_m52096289(__this, method) ((  void (*) (Skip_t2190906560 *, const MethodInfo*))Skip_OnCompleted_m52096289_gshared)(__this, method)
