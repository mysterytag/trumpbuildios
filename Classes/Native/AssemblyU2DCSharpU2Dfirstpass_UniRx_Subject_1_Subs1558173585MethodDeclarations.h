﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Subject`1/Subscription<UniRx.DictionaryReplaceEvent`2<System.Object,System.Object>>
struct Subscription_t1558173585;
// UniRx.Subject`1<UniRx.DictionaryReplaceEvent`2<System.Object,System.Object>>
struct Subject_1_t1421994387;
// UniRx.IObserver`1<UniRx.DictionaryReplaceEvent`2<System.Object,System.Object>>
struct IObserver_1_t1695958670;

#include "codegen/il2cpp-codegen.h"

// System.Void UniRx.Subject`1/Subscription<UniRx.DictionaryReplaceEvent`2<System.Object,System.Object>>::.ctor(UniRx.Subject`1<T>,UniRx.IObserver`1<T>)
extern "C"  void Subscription__ctor_m334052977_gshared (Subscription_t1558173585 * __this, Subject_1_t1421994387 * ___parent0, Il2CppObject* ___unsubscribeTarget1, const MethodInfo* method);
#define Subscription__ctor_m334052977(__this, ___parent0, ___unsubscribeTarget1, method) ((  void (*) (Subscription_t1558173585 *, Subject_1_t1421994387 *, Il2CppObject*, const MethodInfo*))Subscription__ctor_m334052977_gshared)(__this, ___parent0, ___unsubscribeTarget1, method)
// System.Void UniRx.Subject`1/Subscription<UniRx.DictionaryReplaceEvent`2<System.Object,System.Object>>::Dispose()
extern "C"  void Subscription_Dispose_m341106917_gshared (Subscription_t1558173585 * __this, const MethodInfo* method);
#define Subscription_Dispose_m341106917(__this, method) ((  void (*) (Subscription_t1558173585 *, const MethodInfo*))Subscription_Dispose_m341106917_gshared)(__this, method)
