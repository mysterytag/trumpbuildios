﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.SpriteRenderer[]
struct SpriteRendererU5BU5D_t4109544696;

#include "UnityEngine_UnityEngine_MonoBehaviour3012272455.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MoneyPack
struct  MoneyPack_t290305753  : public MonoBehaviour_t3012272455
{
public:
	// UnityEngine.SpriteRenderer[] MoneyPack::notes
	SpriteRendererU5BU5D_t4109544696* ___notes_2;

public:
	inline static int32_t get_offset_of_notes_2() { return static_cast<int32_t>(offsetof(MoneyPack_t290305753, ___notes_2)); }
	inline SpriteRendererU5BU5D_t4109544696* get_notes_2() const { return ___notes_2; }
	inline SpriteRendererU5BU5D_t4109544696** get_address_of_notes_2() { return &___notes_2; }
	inline void set_notes_2(SpriteRendererU5BU5D_t4109544696* value)
	{
		___notes_2 = value;
		Il2CppCodeGenWriteBarrier(&___notes_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
