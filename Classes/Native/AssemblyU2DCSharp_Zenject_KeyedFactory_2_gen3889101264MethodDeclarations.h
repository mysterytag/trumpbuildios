﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Zenject.KeyedFactory`2<System.Object,System.Object>
struct KeyedFactory_2_t3889101264;
// System.Type[]
struct TypeU5BU5D_t3431720054;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"

// System.Void Zenject.KeyedFactory`2<System.Object,System.Object>::.ctor()
extern "C"  void KeyedFactory_2__ctor_m3172930494_gshared (KeyedFactory_2_t3889101264 * __this, const MethodInfo* method);
#define KeyedFactory_2__ctor_m3172930494(__this, method) ((  void (*) (KeyedFactory_2_t3889101264 *, const MethodInfo*))KeyedFactory_2__ctor_m3172930494_gshared)(__this, method)
// System.Type[] Zenject.KeyedFactory`2<System.Object,System.Object>::get_ProvidedTypes()
extern "C"  TypeU5BU5D_t3431720054* KeyedFactory_2_get_ProvidedTypes_m3187342273_gshared (KeyedFactory_2_t3889101264 * __this, const MethodInfo* method);
#define KeyedFactory_2_get_ProvidedTypes_m3187342273(__this, method) ((  TypeU5BU5D_t3431720054* (*) (KeyedFactory_2_t3889101264 *, const MethodInfo*))KeyedFactory_2_get_ProvidedTypes_m3187342273_gshared)(__this, method)
// TBase Zenject.KeyedFactory`2<System.Object,System.Object>::Create(TKey)
extern "C"  Il2CppObject * KeyedFactory_2_Create_m1040458889_gshared (KeyedFactory_2_t3889101264 * __this, Il2CppObject * ___key0, const MethodInfo* method);
#define KeyedFactory_2_Create_m1040458889(__this, ___key0, method) ((  Il2CppObject * (*) (KeyedFactory_2_t3889101264 *, Il2CppObject *, const MethodInfo*))KeyedFactory_2_Create_m1040458889_gshared)(__this, ___key0, method)
