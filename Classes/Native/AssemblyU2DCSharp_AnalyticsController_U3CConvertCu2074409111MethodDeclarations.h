﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// AnalyticsController/<ConvertCurrencyCoro>c__Iterator1D
struct U3CConvertCurrencyCoroU3Ec__Iterator1D_t2074409111;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void AnalyticsController/<ConvertCurrencyCoro>c__Iterator1D::.ctor()
extern "C"  void U3CConvertCurrencyCoroU3Ec__Iterator1D__ctor_m3990222583 (U3CConvertCurrencyCoroU3Ec__Iterator1D_t2074409111 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object AnalyticsController/<ConvertCurrencyCoro>c__Iterator1D::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CConvertCurrencyCoroU3Ec__Iterator1D_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3301177733 (U3CConvertCurrencyCoroU3Ec__Iterator1D_t2074409111 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object AnalyticsController/<ConvertCurrencyCoro>c__Iterator1D::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CConvertCurrencyCoroU3Ec__Iterator1D_System_Collections_IEnumerator_get_Current_m2203821849 (U3CConvertCurrencyCoroU3Ec__Iterator1D_t2074409111 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean AnalyticsController/<ConvertCurrencyCoro>c__Iterator1D::MoveNext()
extern "C"  bool U3CConvertCurrencyCoroU3Ec__Iterator1D_MoveNext_m63486085 (U3CConvertCurrencyCoroU3Ec__Iterator1D_t2074409111 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AnalyticsController/<ConvertCurrencyCoro>c__Iterator1D::Dispose()
extern "C"  void U3CConvertCurrencyCoroU3Ec__Iterator1D_Dispose_m3391357300 (U3CConvertCurrencyCoroU3Ec__Iterator1D_t2074409111 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AnalyticsController/<ConvertCurrencyCoro>c__Iterator1D::Reset()
extern "C"  void U3CConvertCurrencyCoroU3Ec__Iterator1D_Reset_m1636655524 (U3CConvertCurrencyCoroU3Ec__Iterator1D_t2074409111 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
