﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// StreamAPI/<Filter>c__AnonStorey134`1/<Filter>c__AnonStorey135`1<System.Object>
struct U3CFilterU3Ec__AnonStorey135_1_t999690120;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"

// System.Void StreamAPI/<Filter>c__AnonStorey134`1/<Filter>c__AnonStorey135`1<System.Object>::.ctor()
extern "C"  void U3CFilterU3Ec__AnonStorey135_1__ctor_m3374779706_gshared (U3CFilterU3Ec__AnonStorey135_1_t999690120 * __this, const MethodInfo* method);
#define U3CFilterU3Ec__AnonStorey135_1__ctor_m3374779706(__this, method) ((  void (*) (U3CFilterU3Ec__AnonStorey135_1_t999690120 *, const MethodInfo*))U3CFilterU3Ec__AnonStorey135_1__ctor_m3374779706_gshared)(__this, method)
// System.Void StreamAPI/<Filter>c__AnonStorey134`1/<Filter>c__AnonStorey135`1<System.Object>::<>m__1CD(T)
extern "C"  void U3CFilterU3Ec__AnonStorey135_1_U3CU3Em__1CD_m97491693_gshared (U3CFilterU3Ec__AnonStorey135_1_t999690120 * __this, Il2CppObject * ___val0, const MethodInfo* method);
#define U3CFilterU3Ec__AnonStorey135_1_U3CU3Em__1CD_m97491693(__this, ___val0, method) ((  void (*) (U3CFilterU3Ec__AnonStorey135_1_t999690120 *, Il2CppObject *, const MethodInfo*))U3CFilterU3Ec__AnonStorey135_1_U3CU3Em__1CD_m97491693_gshared)(__this, ___val0, method)
