﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

// UniRx.Observable/<OnErrorRetry>c__AnonStorey3D`2<System.Object,System.Object>
struct U3COnErrorRetryU3Ec__AnonStorey3D_2_t4266989836;
// UniRx.IObservable`1<System.Object>
struct IObservable_1_t595904784;
// System.Func`2<System.Object,UniRx.IObservable`1<System.Object>>
struct Func_2_t1894581716;
// UniRx.Observable/<RepeatInfinite>c__IteratorE`1<System.Object>
struct U3CRepeatInfiniteU3Ec__IteratorE_1_t1925948945;
// System.Object
struct Il2CppObject;
// System.Collections.IEnumerator
struct IEnumerator_t287207039;
// System.Collections.Generic.IEnumerator`1<UniRx.IObservable`1<System.Object>>
struct IEnumerator_1_t2079011232;
// UniRx.Observable/<SelectMany>c__AnonStorey49`2<System.Object,System.Object>
struct U3CSelectManyU3Ec__AnonStorey49_2_t2420169340;
// UniRx.Observable/<SelectMany>c__AnonStorey49`2<System.Object,UniRx.Unit>
struct U3CSelectManyU3Ec__AnonStorey49_2_t4141348958;
// UniRx.IObservable`1<UniRx.Unit>
struct IObservable_1_t2317084402;
// UniRx.Observable/<SelectMany>c__AnonStorey49`2<UniRx.Unit,UniRx.Unit>
struct U3CSelectManyU3Ec__AnonStorey49_2_t1102810292;
// UniRx.Observable/<SelectMany>c__AnonStorey4D`1<System.Object>
struct U3CSelectManyU3Ec__AnonStorey4D_1_t352756178;
// UniRx.Observable/<SelectMany>c__AnonStorey4E`1<System.Object>
struct U3CSelectManyU3Ec__AnonStorey4E_1_t4202698475;
// UniRx.Observable/<SelectMany>c__AnonStorey4E`1<UniRx.Unit>
struct U3CSelectManyU3Ec__AnonStorey4E_1_t1628910797;
// UniRx.Observable/<SelectMany>c__AnonStorey4F`1/<SelectMany>c__AnonStorey50`1<System.Object>
struct U3CSelectManyU3Ec__AnonStorey50_1_t4047415781;
// UniRx.Observable/<SelectMany>c__AnonStorey4F`1<System.Object>
struct U3CSelectManyU3Ec__AnonStorey4F_1_t3757673476;
// UniRx.Observable/<ToAsync>c__AnonStorey38`1/<ToAsync>c__AnonStorey39`1<System.Object>
struct U3CToAsyncU3Ec__AnonStorey39_1_t776464662;
// UniRx.Observable/<ToAsync>c__AnonStorey38`1<System.Object>
struct U3CToAsyncU3Ec__AnonStorey38_1_t1221489661;
// UniRx.Observable/<ToAwaitableEnumerator>c__Iterator17`1<System.Int32>
struct U3CToAwaitableEnumeratorU3Ec__Iterator17_1_t3131167031;
// UniRx.Observable/<ToAwaitableEnumerator>c__Iterator17`1<System.Object>
struct U3CToAwaitableEnumeratorU3Ec__Iterator17_1_t1120858664;
// UniRx.Observable/<WrapEnumeratorYieldValue>c__Iterator10`1<System.Object>
struct U3CWrapEnumeratorYieldValueU3Ec__Iterator10_1_t896010658;
// UniRx.Observable/ConnectableObservable`1/Connection<System.Object>
struct Connection_t217434055;
// UniRx.Observable/ConnectableObservable`1<System.Object>
struct ConnectableObservable_1_t608944977;
// System.IDisposable
struct IDisposable_t1628921374;
// UniRx.ISubject`1<System.Object>
struct ISubject_1_t353709935;
// UniRx.IObserver`1<System.Object>
struct IObserver_1_t3049105323;
// UniRx.ObservableYieldInstruction`1/ToYieldInstruction<System.Int32>
struct ToYieldInstruction_t3682831524;
// UniRx.ObservableYieldInstruction`1<System.Int32>
struct ObservableYieldInstruction_1_t674360375;
// System.Exception
struct Exception_t1967233988;
// UniRx.ObservableYieldInstruction`1/ToYieldInstruction<System.Int64>
struct ToYieldInstruction_t3682831619;
// UniRx.ObservableYieldInstruction`1<System.Int64>
struct ObservableYieldInstruction_1_t674360470;
// UniRx.ObservableYieldInstruction`1/ToYieldInstruction<System.Object>
struct ToYieldInstruction_t1672523157;
// UniRx.ObservableYieldInstruction`1<System.Object>
struct ObservableYieldInstruction_1_t2959019304;
// UniRx.IObservable`1<System.Int32>
struct IObservable_1_t2606213151;
// UniRx.IObservable`1<System.Int64>
struct IObservable_1_t2606213246;
// UniRx.ObserveExtensions/<ObserveEveryValueChanged>c__AnonStorey91`2<System.Object,System.Double>
struct U3CObserveEveryValueChangedU3Ec__AnonStorey91_2_t1160447004;
// UniRx.IObserver`1<System.Double>
struct IObserver_1_t2746515517;
// UnityEngine.Object
struct Object_t3878351788;
struct Object_t3878351788_marshaled_pinvoke;
// System.Func`2<System.Object,System.Double>
struct Func_2_t1833193546;
// UniRx.ObserveExtensions/<ObserveEveryValueChanged>c__AnonStorey91`2<System.Object,System.Object>
struct U3CObserveEveryValueChangedU3Ec__AnonStorey91_2_t1463036810;
// System.Func`2<System.Object,System.Object>
struct Func_2_t2135783352;
// UniRx.ObserveExtensions/<ObserveEveryValueChanged>c__AnonStorey92`2<System.Object,System.Double>
struct U3CObserveEveryValueChangedU3Ec__AnonStorey92_2_t2257977983;
// System.WeakReference
struct WeakReference_t2193916456;
// UniRx.ObserveExtensions/<ObserveEveryValueChanged>c__AnonStorey92`2<System.Object,System.Object>
struct U3CObserveEveryValueChangedU3Ec__AnonStorey92_2_t2560567789;
// UniRx.ObserveExtensions/<PublishPocoValueChanged>c__Iterator24`2<System.Object,System.Double>
struct U3CPublishPocoValueChangedU3Ec__Iterator24_2_t2868476001;
// System.Collections.Generic.IEqualityComparer`1<System.Double>
struct IEqualityComparer_1_t2858783265;
// UniRx.ObserveExtensions/<PublishPocoValueChanged>c__Iterator24`2<System.Object,System.Object>
struct U3CPublishPocoValueChangedU3Ec__Iterator24_2_t3171065807;
// System.Collections.Generic.IEqualityComparer`1<System.Object>
struct IEqualityComparer_1_t3161373071;
// UniRx.ObserveExtensions/<PublishUnityObjectValueChanged>c__Iterator25`2<System.Object,System.Double>
struct U3CPublishUnityObjectValueChangedU3Ec__Iterator25_2_t42621133;
// UniRx.ObserveExtensions/<PublishUnityObjectValueChanged>c__Iterator25`2<System.Object,System.Object>
struct U3CPublishUnityObjectValueChangedU3Ec__Iterator25_2_t345210939;
// UniRx.Observer/AnonymousObserver`1<System.Object>
struct AnonymousObserver_1_t4218636717;
// System.Action`1<System.Object>
struct Action_1_t985559125;
// System.Action`1<System.Exception>
struct Action_1_t2115686693;
// System.Action
struct Action_t437523947;
// UniRx.Observer/AutoDetachObserver`1<System.Object>
struct AutoDetachObserver_1_t2328747674;
// UniRx.Observer/EmptyOnNextAnonymousObserver`1<System.Object>
struct EmptyOnNextAnonymousObserver_1_t1584423078;
// UniRx.Observer/Subscribe_`1<System.Boolean>
struct Subscribe__1_t2955937592;
// UniRx.Observer/Subscribe_`1<System.Double>
struct Subscribe__1_t3279448865;
// UniRx.Observer/Subscribe_`1<System.Int64>
struct Subscribe__1_t1297379837;
// UniRx.Observer/Subscribe_`1<System.Object>
struct Subscribe__1_t3582038671;
// UniRx.Observer/Subscribe_`1<System.Single>
struct Subscribe__1_t3703141272;
// UniRx.Observer/Subscribe_`1<UniRx.Tuple`2<System.Object,System.Object>>
struct Subscribe__1_t3114194070;
// UniRx.Observer/Subscribe_`1<UniRx.Unit>
struct Subscribe__1_t1008250993;
// UniRx.Observer/Subscribe_`1<UnityEngine.Color>
struct Subscribe__1_t38140715;
// UniRx.Observer/Subscribe`1<System.Boolean>
struct Subscribe_1_t3319793125;
// System.Action`1<System.Boolean>
struct Action_1_t359458046;
// UniRx.Observer/Subscribe`1<System.Double>
struct Subscribe_1_t3643304398;
// System.Action`1<System.Double>
struct Action_1_t682969319;
// UniRx.Observer/Subscribe`1<System.Int64>
struct Subscribe_1_t1661235370;
// System.Action`1<System.Int64>
struct Action_1_t2995867587;
// UniRx.Observer/Subscribe`1<System.Object>
struct Subscribe_1_t3945894204;
// UniRx.Observer/Subscribe`1<System.Single>
struct Subscribe_1_t4066996805;
// System.Action`1<System.Single>
struct Action_1_t1106661726;
// UniRx.Observer/Subscribe`1<UniRx.Tuple`2<System.Object,System.Object>>
struct Subscribe_1_t3478049603;
// System.Action`1<UniRx.Tuple`2<System.Object,System.Object>>
struct Action_1_t517714524;
// UniRx.Observer/Subscribe`1<UniRx.Unit>
struct Subscribe_1_t1372106526;
// System.Action`1<UniRx.Unit>
struct Action_1_t2706738743;
// UniRx.Observer/Subscribe`1<UnityEngine.Color>
struct Subscribe_1_t401996248;
// System.Action`1<UnityEngine.Color>
struct Action_1_t1736628465;
// UniRx.Operators.AggregateObservable`1/Aggregate<System.Object>
struct Aggregate_t515487936;
// UniRx.Operators.AggregateObservable`1<System.Object>
struct AggregateObservable_1_t497085657;
// System.Func`3<System.Object,System.Object,System.Object>
struct Func_3_t1892209229;
// UniRx.Operators.AggregateObservable`2/Aggregate<System.Object,System.Object>
struct Aggregate_t1831847732;
// UniRx.Operators.AggregateObservable`2<System.Object,System.Object>
struct AggregateObservable_2_t1747477946;
// UniRx.Operators.AggregateObservable`3/Aggregate<System.Object,System.Object,System.Object>
struct Aggregate_t245075056;
// UniRx.Operators.AggregateObservable`3<System.Object,System.Object,System.Object>
struct AggregateObservable_3_t3567497731;
// UniRx.Operators.AmbObservable`1/AmbOuterObserver/Amb<System.Object>
struct Amb_t634706591;
// UniRx.Operators.AmbObservable`1/AmbOuterObserver/AmbDecisionObserver<System.Object>
struct AmbDecisionObserver_t973332161;
// UniRx.Operators.AmbObservable`1/AmbOuterObserver<System.Object>
struct AmbOuterObserver_t2827192636;
// UniRx.Operators.AmbObservable`1<System.Object>
struct AmbObservable_1_t4231346232;
// UniRx.Operators.AsObservableObservable`1/AsObservable<System.Boolean>
struct AsObservable_t3870929039;
// UniRx.IObserver`1<System.Boolean>
struct IObserver_1_t2423004244;
// UniRx.Operators.AsObservableObservable`1/AsObservable<System.Object>
struct AsObservable_t202062822;
// UniRx.Operators.AsObservableObservable`1/AsObservable<UniRx.Unit>
struct AsObservable_t1923242440;
// UniRx.IObserver`1<UniRx.Unit>
struct IObserver_1_t475317645;
// UniRx.Operators.AsObservableObservable`1<System.Boolean>
struct AsObservableObservable_1_t3890699176;
// UniRx.IObservable`1<System.Boolean>
struct IObservable_1_t4264771001;
// UniRx.Operators.AsObservableObservable`1<System.Object>
struct AsObservableObservable_1_t221832959;
// UniRx.Operators.AsObservableObservable`1<UniRx.Unit>
struct AsObservableObservable_1_t1943012577;
// UniRx.Operators.AsUnitObservableObservable`1/AsUnitObservable<System.Object>
struct AsUnitObservable_t2008788970;
// UniRx.Operators.AsUnitObservableObservable`1<System.Object>
struct AsUnitObservableObservable_1_t2313647363;
// UniRx.Operators.BufferObservable`1/Buffer_<System.Object>
struct Buffer__t1281669152;
// UniRx.Operators.BufferObservable`1<System.Object>
struct BufferObservable_1_t574294898;
// UniRx.IObserver`1<System.Collections.Generic.IList`1<System.Object>>
struct IObserver_1_t920630341;
// UniRx.Operators.BufferObservable`1/Buffer<System.Object>
struct Buffer_t492975321;
// UniRx.Operators.BufferObservable`1/BufferT/Buffer<System.Object>
struct Buffer_t492975322;
// UniRx.Operators.BufferObservable`1/BufferT<System.Object>
struct BufferT_t129222477;
// UniRx.Operators.BufferObservable`1/BufferTC/<CreateTimer>c__AnonStorey5D<System.Object>
struct U3CCreateTimerU3Ec__AnonStorey5D_t162307190;
// System.Action`1<System.TimeSpan>
struct Action_1_t912315597;
// UniRx.Operators.BufferObservable`1/BufferTC<System.Object>
struct BufferTC_t3470796400;
// UniRx.Operators.BufferObservable`1/BufferTS/<CreateTimer>c__AnonStorey5C<System.Object>
struct U3CCreateTimerU3Ec__AnonStorey5C_t2790700317;
// UniRx.Operators.BufferObservable`1/BufferTS<System.Object>
struct BufferTS_t71212032;
// UniRx.IScheduler
struct IScheduler_t2938318244;
// UniRx.Operators.BufferObservable`2/Buffer/Buffer_<System.Int64,System.Int64>
struct Buffer__t1004182748;
// UniRx.Operators.BufferObservable`2/Buffer<System.Int64,System.Int64>
struct Buffer_t1984651551;
// UniRx.Operators.BufferObservable`2/Buffer/Buffer_<System.Object,System.Object>
struct Buffer__t2547187284;
// UniRx.Operators.BufferObservable`2/Buffer<System.Object,System.Object>
struct Buffer_t3527656087;
// UniRx.Operators.BufferObservable`2<System.Int64,System.Int64>
struct BufferObservable_2_t550437669;
// UniRx.IObserver`1<System.Collections.Generic.IList`1<System.Int64>>
struct IObserver_1_t2930938803;
// UniRx.Operators.BufferObservable`2<System.Object,System.Object>
struct BufferObservable_2_t2093442205;
// UniRx.Operators.CastObservable`2/Cast<System.Object,System.Object>
struct Cast_t3935810676;
// UniRx.Operators.CastObservable`2<System.Object,System.Object>
struct CastObservable_2_t4040585978;
// UniRx.Operators.CatchObservable`1/Catch<System.Object>
struct Catch_t3617674332;
// UniRx.Operators.CatchObservable`1<System.Object>
struct CatchObservable_1_t1624243829;
// System.Collections.Generic.IEnumerable`1<UniRx.IObservable`1<System.Object>>
struct IEnumerable_1_t3468059140;
// UniRx.Operators.CatchObservable`2/Catch<System.Object,System.Object>
struct Catch_t841129384;
// UniRx.Operators.CatchObservable`2<System.Object,System.Object>
struct CatchObservable_2_t3974704686;
// UniRx.Operators.CombineLatestFunc`4<System.Object,System.Object,System.Object,System.Object>
struct CombineLatestFunc_4_t382088552;
// System.IAsyncResult
struct IAsyncResult_t537683269;
// System.AsyncCallback
struct AsyncCallback_t1363551830;
// UniRx.Operators.CombineLatestFunc`5<System.Boolean,System.Boolean,System.Boolean,System.Boolean,System.Object>
struct CombineLatestFunc_5_t2231862077;
// UniRx.Operators.CombineLatestFunc`5<System.Object,System.Object,System.Object,System.Object,System.Object>
struct CombineLatestFunc_5_t545776053;
// UniRx.Operators.CombineLatestFunc`6<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>
struct CombineLatestFunc_6_t2285714110;
// UniRx.Operators.CombineLatestFunc`7<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>
struct CombineLatestFunc_7_t929534559;
// UniRx.Operators.CombineLatestFunc`8<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>
struct CombineLatestFunc_8_t584396980;
// UniRx.Operators.CombineLatestObservable`1/CombineLatest/CombineLatestObserver<System.Boolean>
struct CombineLatestObserver_t3970536766;
// UniRx.Operators.CombineLatestObservable`1/CombineLatest<System.Boolean>
struct CombineLatest_t242152888;
// UniRx.Operators.CombineLatestObservable`1/CombineLatest/CombineLatestObserver<System.Object>
struct CombineLatestObserver_t301670549;
// UniRx.Operators.CombineLatestObservable`1/CombineLatest<System.Object>
struct CombineLatest_t868253967;
// UniRx.Operators.CombineLatestObservable`1<System.Boolean>
struct CombineLatestObservable_1_t2883136337;
// UniRx.IObserver`1<System.Collections.Generic.IList`1<System.Boolean>>
struct IObserver_1_t294529262;
// System.Collections.Generic.IList`1<System.Boolean>
struct IList_1_t2377497655;
// UniRx.Operators.CombineLatestObservable`1<System.Object>
struct CombineLatestObservable_1_t3509237416;
// System.Collections.Generic.IList`1<System.Object>
struct IList_1_t3003598734;

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array2840145358.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Observable_U3C4266989836.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Observable_U3C4266989836MethodDeclarations.h"
#include "mscorlib_System_Void2779279689.h"
#include "mscorlib_System_Object837106420MethodDeclarations.h"
#include "mscorlib_System_TimeSpan763862892MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Observable_U3C1069553519.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Observable_U3C1069553519MethodDeclarations.h"
#include "mscorlib_System_TimeSpan763862892.h"
#include "mscorlib_System_Int642847414882.h"
#include "mscorlib_System_Int322847414787.h"
#include "mscorlib_System_Object837106420.h"
#include "System_Core_System_Func_2_gen1894581716.h"
#include "System_Core_System_Func_2_gen1894581716MethodDeclarations.h"
#include "mscorlib_System_IntPtr676692020.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Observable258071701MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Observable258071701.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Observable_U3C1925948945.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Observable_U3C1925948945MethodDeclarations.h"
#include "mscorlib_System_Threading_Interlocked3007803305MethodDeclarations.h"
#include "mscorlib_System_Boolean211005341.h"
#include "mscorlib_System_UInt32985925326.h"
#include "mscorlib_System_NotSupportedException1374155497MethodDeclarations.h"
#include "mscorlib_System_NotSupportedException1374155497.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Observable_U3C2420169340.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Observable_U3C2420169340MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Observable_U3C4141348958.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Observable_U3C4141348958MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Observable_U3C1102810292.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Observable_U3C1102810292MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Unit2558286038.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Observable_U3CS352756178.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Observable_U3CS352756178MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Observable_U3C4202698475.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Observable_U3C4202698475MethodDeclarations.h"
#include "System_Core_System_Func_1_gen1429988286MethodDeclarations.h"
#include "System_Core_System_Func_1_gen1429988286.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Observable_U3C1628910797.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Observable_U3C1628910797MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Observable_U3C4047415781.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Observable_U3C4047415781MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Observable_U3C3757673476.h"
#include "System_Core_System_Func_2_gen1585883971.h"
#include "System_Core_System_Func_2_gen1585883971MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Observable_U3C3757673476MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Observable_U3CT776464662.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Observable_U3CT776464662MethodDeclarations.h"
#include "mscorlib_System_Exception1967233988.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Observable_U3C1221489661.h"
#include "System_Core_System_Func_1_gen1979887667.h"
#include "System_Core_System_Func_1_gen1979887667MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_AsyncSubject_11536745524.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_AsyncSubject_11536745524MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Observable_U3C1221489661MethodDeclarations.h"
#include "System_Core_System_Action437523947MethodDeclarations.h"
#include "System_Core_System_Action437523947.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Observable_U3C3131167031.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Observable_U3C3131167031MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_CancellationTo1439151560MethodDeclarations.h"
#include "mscorlib_System_Action_1_gen2115686693MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_ObservableYield674360375.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_ObservableYield674360375MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_CancellationTo1439151560.h"
#include "mscorlib_System_Action_1_gen2995867492.h"
#include "mscorlib_System_Action_1_gen2995867492MethodDeclarations.h"
#include "mscorlib_System_Action_1_gen2115686693.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Observable_U3C1120858664.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Observable_U3C1120858664MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_ObservableYiel2959019304.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_ObservableYiel2959019304MethodDeclarations.h"
#include "mscorlib_System_Action_1_gen985559125.h"
#include "mscorlib_System_Action_1_gen985559125MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Observable_U3CW896010658.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Observable_U3CW896010658MethodDeclarations.h"
#include "System_Core_System_Collections_Generic_HashSet_1_g1182951310.h"
#include "mscorlib_System_Type2779229935.h"
#include "System_Core_System_Collections_Generic_HashSet_1_g1182951310MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Observable_Conn217434055.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Observable_Conn217434055MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Observable_Conn608944977.h"
#include "mscorlib_System_Threading_Monitor2071304733MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Observable_Conn608944977MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_ObservableYiel3682831524.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_ObservableYiel3682831524MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_ObservableYiel3682831619.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_ObservableYiel3682831619MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_ObservableYield674360470.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_ObservableYiel1672523157.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_ObservableYiel1672523157MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_ObservableYield674360470MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_ObserveExtensi1160447004.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_ObserveExtensi1160447004MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Object3878351788.h"
#include "System_Core_System_Func_2_gen1833193546.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_FrameCountType167759182.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_ObserveExtensi3692409186MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_ObserveExtensi3692409186.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_ObserveExtensi1463036810.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_ObserveExtensi1463036810MethodDeclarations.h"
#include "System_Core_System_Func_2_gen2135783352.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_ObserveExtensi2257977983.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_ObserveExtensi2257977983MethodDeclarations.h"
#include "mscorlib_System_WeakReference2193916456.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_ObserveExtensi2560567789.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_ObserveExtensi2560567789MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_ObserveExtensi2868476001.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_ObserveExtensi2868476001MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_FrameCountType3551601282MethodDeclarations.h"
#include "mscorlib_System_Double534516614.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_UnityEqualityC3801418734MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_UnityEqualityC3801418734.h"
#include "UnityEngine_UnityEngine_YieldInstruction3557331758.h"
#include "mscorlib_System_WeakReference2193916456MethodDeclarations.h"
#include "System_Core_System_Func_2_gen1833193546MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_ObserveExtensi3171065807.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_ObserveExtensi3171065807MethodDeclarations.h"
#include "System_Core_System_Func_2_gen2135783352MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_ObserveExtension42621133.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_ObserveExtension42621133MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Object3878351788MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_ObserveExtensio345210939.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_ObserveExtensio345210939MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Observer_Anony4218636717.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Observer_Anony4218636717MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Observer_AutoD2328747674.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Observer_AutoD2328747674MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Oper1187768149MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Oper1187768149.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Observer_Empty1584423078.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Observer_Empty1584423078MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Observer_Subsc2955937592.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Observer_Subsc2955937592MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Observer_Subsc3279448865.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Observer_Subsc3279448865MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Observer_Subsc1297379837.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Observer_Subsc1297379837MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Observer_Subsc3582038671.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Observer_Subsc3582038671MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Observer_Subsc3703141272.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Observer_Subsc3703141272MethodDeclarations.h"
#include "mscorlib_System_Single958209021.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Observer_Subsc3114194070.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Observer_Subsc3114194070MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Tuple_2_gen369261819.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Observer_Subsc1008250993.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Observer_Subsc1008250993MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Observer_Subscri38140715.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Observer_Subscri38140715MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Color1588175760.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Observer_Subsc3319793125.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Observer_Subsc3319793125MethodDeclarations.h"
#include "mscorlib_System_Action_1_gen359458046.h"
#include "mscorlib_System_Action_1_gen359458046MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Observer_Subsc3643304398.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Observer_Subsc3643304398MethodDeclarations.h"
#include "mscorlib_System_Action_1_gen682969319.h"
#include "mscorlib_System_Action_1_gen682969319MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Observer_Subsc1661235370.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Observer_Subsc1661235370MethodDeclarations.h"
#include "mscorlib_System_Action_1_gen2995867587.h"
#include "mscorlib_System_Action_1_gen2995867587MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Observer_Subsc3945894204.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Observer_Subsc3945894204MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Observer_Subsc4066996805.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Observer_Subsc4066996805MethodDeclarations.h"
#include "mscorlib_System_Action_1_gen1106661726.h"
#include "mscorlib_System_Action_1_gen1106661726MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Observer_Subsc3478049603.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Observer_Subsc3478049603MethodDeclarations.h"
#include "mscorlib_System_Action_1_gen517714524.h"
#include "mscorlib_System_Action_1_gen517714524MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Observer_Subsc1372106526.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Observer_Subsc1372106526MethodDeclarations.h"
#include "mscorlib_System_Action_1_gen2706738743.h"
#include "mscorlib_System_Action_1_gen2706738743MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Observer_Subscr401996248.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Observer_Subscr401996248MethodDeclarations.h"
#include "mscorlib_System_Action_1_gen1736628465.h"
#include "mscorlib_System_Action_1_gen1736628465MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Aggre515487936.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Aggre515487936MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Aggre497085657.h"
#include "System_Core_System_Func_3_gen1892209229.h"
#include "System_Core_System_Func_3_gen1892209229MethodDeclarations.h"
#include "mscorlib_System_InvalidOperationException2420574324MethodDeclarations.h"
#include "mscorlib_System_String968488902.h"
#include "mscorlib_System_InvalidOperationException2420574324.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Aggre497085657MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_OptimizedObser2379048080MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_OptimizedObser2379048080.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Oper4196218687MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Aggr1831847732.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Aggr1831847732MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Aggr1747477946.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Aggr1747477946MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Aggre245075056.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Aggre245075056MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Aggr3567497731.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Aggr3567497731MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_AmbOb634706591.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_AmbOb634706591MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_Em246459410.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_Em246459410MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_AmbOb973332161.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_AmbOb973332161MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_AmbO2663999068.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_AmbO2827192636.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_AmbO2663999068MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_AmbO2827192636MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_AmbO4231346232.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_SingleAssignme2336378823MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_StableComposit3433635614MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_SingleAssignme2336378823.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_AmbO4231346232MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_AsOb3870929039.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_AsOb3870929039MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Operat60103313MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Operat60103313.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_AsObs202062822.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_AsObs202062822MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_AsOb1923242440.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_AsOb1923242440MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Oper4165376397MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Oper4165376397.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_AsOb3890699176.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_AsOb3890699176MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Oper3570117608MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_AsObs221832959.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_AsObs221832959MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_AsOb1943012577.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_AsOb1943012577MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Oper1622431009MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_AsUn2008788970.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_AsUn2008788970MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Oper2908947767MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Unit2558286038MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Oper2908947767.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_AsUn2313647363.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_AsUn2313647363MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Buff1281669152.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Buff1281669152MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Buffe574294898.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Oper3354260463MethodDeclarations.h"
#include "System_System_Collections_Generic_Queue_1_gen3342152929.h"
#include "System_System_Collections_Generic_Queue_1_gen3342152929MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1634065389.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1634065389MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Oper3354260463.h"
#include "System_System_Collections_Generic_Queue_1_Enumerato516807350.h"
#include "System_System_Collections_Generic_Queue_1_Enumerato516807350MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Buffe492975321.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Buffe492975321MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Buffe_0.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Buffe_0MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Buffe129222477.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Buffe129222477MethodDeclarations.h"
#include "mscorlib_ArrayTypes.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Buffe162307190.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Buffe162307190MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Buff3470796400.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Buff3470796400MethodDeclarations.h"
#include "mscorlib_System_Action_1_gen912315597.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_SerialDisposab2547852742MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_SerialDisposab2547852742.h"
#include "mscorlib_System_Action_1_gen1060768302MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Scheduler103934925MethodDeclarations.h"
#include "mscorlib_System_Action_1_gen1060768302.h"
#include "mscorlib_System_Action_1_gen912315597MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Buff2790700317.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Buff2790700317MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Buffer71212032.h"
#include "System_System_Collections_Generic_Queue_1_gen416718978.h"
#include "System_System_Collections_Generic_Queue_1_gen416718978MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Buffer71212032MethodDeclarations.h"
#include "System_System_Collections_Generic_Queue_1_Enumerat1886340695.h"
#include "System_System_Collections_Generic_Queue_1_Enumerat1886340695MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Buffe574294898MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Oper2067743705MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Scheduler103934925.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Buff1004182748.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Buff1004182748MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Buff1984651551.h"
#include "mscorlib_System_Collections_Generic_List_1_gen3644373851.h"
#include "mscorlib_System_Collections_Generic_List_1_gen3644373851MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Oper1811255927.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Buff1984651551MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Buff2547187284.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Buff2547187284MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Buff3527656087.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Buff3527656087MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Buffe550437669.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Oper1811255927MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Buff2093442205.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Buffe550437669MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Oper4078052167MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Buff2093442205MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Cast3935810676.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Cast3935810676MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Cast4040585978.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Cast4040585978MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Catc3617674332.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Catc3617674332MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Catc1624243829.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Scheduler_Defa3564383257MethodDeclarations.h"
#include "mscorlib_System_Action_1_gen585976652MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Disposable3736388786MethodDeclarations.h"
#include "mscorlib_System_Action_1_gen585976652.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Catc1624243829MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Catch841129384.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Catch841129384MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Catc3974704686.h"
#include "mscorlib_System_MulticastDelegate2585444626MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Stubs2635868311.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Stubs2635868311MethodDeclarations.h"
#include "mscorlib_System_MulticastDelegate2585444626.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Catc3974704686MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Combi382088552.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Combi382088552MethodDeclarations.h"
#include "mscorlib_System_AsyncCallback1363551830.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Comb2231862077.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Comb2231862077MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Combi545776053.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Combi545776053MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Comb2285714110.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Comb2285714110MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Combi929534559.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Combi929534559MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Combi584396980.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Combi584396980MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Comb3970536766.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Comb3970536766MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Combi242152888.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Combi242152888MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Combi301670549.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Combi301670549MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Combi868253967.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Combi868253967MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Comb2883136337.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Oper2939511529MethodDeclarations.h"
#include "Assembly-CSharp-firstpass_ArrayTypes.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1007964310.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1007964310MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Oper2939511529.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Comb3509237416.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Oper4067176365MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Oper4067176365.h"

// UniRx.IObservable`1<!!0> UniRx.Observable::Catch<System.Object,System.Object>(UniRx.IObservable`1<!!0>,System.Func`2<!!1,UniRx.IObservable`1<!!0>>)
extern "C"  Il2CppObject* Observable_Catch_TisIl2CppObject_TisIl2CppObject_m238479185_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* p0, Func_2_t1894581716 * p1, const MethodInfo* method);
#define Observable_Catch_TisIl2CppObject_TisIl2CppObject_m238479185(__this /* static, unused */, p0, p1, method) ((  Il2CppObject* (*) (Il2CppObject * /* static, unused */, Il2CppObject*, Func_2_t1894581716 *, const MethodInfo*))Observable_Catch_TisIl2CppObject_TisIl2CppObject_m238479185_gshared)(__this /* static, unused */, p0, p1, method)
// UniRx.IObservable`1<!!0> UniRx.Observable::AsObservable<System.Object>(UniRx.IObservable`1<!!0>)
extern "C"  Il2CppObject* Observable_AsObservable_TisIl2CppObject_m961297524_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* p0, const MethodInfo* method);
#define Observable_AsObservable_TisIl2CppObject_m961297524(__this /* static, unused */, p0, method) ((  Il2CppObject* (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Observable_AsObservable_TisIl2CppObject_m961297524_gshared)(__this /* static, unused */, p0, method)
// System.Collections.IEnumerator UniRx.ObserveExtensions::PublishUnityObjectValueChanged<System.Object,System.Double>(UnityEngine.Object,System.Func`2<!!0,!!1>,UniRx.FrameCountType,UniRx.IObserver`1<!!1>,UniRx.CancellationToken)
extern "C"  Il2CppObject * ObserveExtensions_PublishUnityObjectValueChanged_TisIl2CppObject_TisDouble_t534516614_m2889431759_gshared (Il2CppObject * __this /* static, unused */, Object_t3878351788 * p0, Func_2_t1833193546 * p1, int32_t p2, Il2CppObject* p3, CancellationToken_t1439151560  p4, const MethodInfo* method);
#define ObserveExtensions_PublishUnityObjectValueChanged_TisIl2CppObject_TisDouble_t534516614_m2889431759(__this /* static, unused */, p0, p1, p2, p3, p4, method) ((  Il2CppObject * (*) (Il2CppObject * /* static, unused */, Object_t3878351788 *, Func_2_t1833193546 *, int32_t, Il2CppObject*, CancellationToken_t1439151560 , const MethodInfo*))ObserveExtensions_PublishUnityObjectValueChanged_TisIl2CppObject_TisDouble_t534516614_m2889431759_gshared)(__this /* static, unused */, p0, p1, p2, p3, p4, method)
// System.Collections.IEnumerator UniRx.ObserveExtensions::PublishUnityObjectValueChanged<System.Object,System.Object>(UnityEngine.Object,System.Func`2<!!0,!!1>,UniRx.FrameCountType,UniRx.IObserver`1<!!1>,UniRx.CancellationToken)
extern "C"  Il2CppObject * ObserveExtensions_PublishUnityObjectValueChanged_TisIl2CppObject_TisIl2CppObject_m551387709_gshared (Il2CppObject * __this /* static, unused */, Object_t3878351788 * p0, Func_2_t2135783352 * p1, int32_t p2, Il2CppObject* p3, CancellationToken_t1439151560  p4, const MethodInfo* method);
#define ObserveExtensions_PublishUnityObjectValueChanged_TisIl2CppObject_TisIl2CppObject_m551387709(__this /* static, unused */, p0, p1, p2, p3, p4, method) ((  Il2CppObject * (*) (Il2CppObject * /* static, unused */, Object_t3878351788 *, Func_2_t2135783352 *, int32_t, Il2CppObject*, CancellationToken_t1439151560 , const MethodInfo*))ObserveExtensions_PublishUnityObjectValueChanged_TisIl2CppObject_TisIl2CppObject_m551387709_gshared)(__this /* static, unused */, p0, p1, p2, p3, p4, method)
// System.Collections.IEnumerator UniRx.ObserveExtensions::PublishPocoValueChanged<System.Object,System.Double>(System.WeakReference,System.Func`2<!!0,!!1>,UniRx.FrameCountType,UniRx.IObserver`1<!!1>,UniRx.CancellationToken)
extern "C"  Il2CppObject * ObserveExtensions_PublishPocoValueChanged_TisIl2CppObject_TisDouble_t534516614_m259756538_gshared (Il2CppObject * __this /* static, unused */, WeakReference_t2193916456 * p0, Func_2_t1833193546 * p1, int32_t p2, Il2CppObject* p3, CancellationToken_t1439151560  p4, const MethodInfo* method);
#define ObserveExtensions_PublishPocoValueChanged_TisIl2CppObject_TisDouble_t534516614_m259756538(__this /* static, unused */, p0, p1, p2, p3, p4, method) ((  Il2CppObject * (*) (Il2CppObject * /* static, unused */, WeakReference_t2193916456 *, Func_2_t1833193546 *, int32_t, Il2CppObject*, CancellationToken_t1439151560 , const MethodInfo*))ObserveExtensions_PublishPocoValueChanged_TisIl2CppObject_TisDouble_t534516614_m259756538_gshared)(__this /* static, unused */, p0, p1, p2, p3, p4, method)
// System.Collections.IEnumerator UniRx.ObserveExtensions::PublishPocoValueChanged<System.Object,System.Object>(System.WeakReference,System.Func`2<!!0,!!1>,UniRx.FrameCountType,UniRx.IObserver`1<!!1>,UniRx.CancellationToken)
extern "C"  Il2CppObject * ObserveExtensions_PublishPocoValueChanged_TisIl2CppObject_TisIl2CppObject_m3962287592_gshared (Il2CppObject * __this /* static, unused */, WeakReference_t2193916456 * p0, Func_2_t2135783352 * p1, int32_t p2, Il2CppObject* p3, CancellationToken_t1439151560  p4, const MethodInfo* method);
#define ObserveExtensions_PublishPocoValueChanged_TisIl2CppObject_TisIl2CppObject_m3962287592(__this /* static, unused */, p0, p1, p2, p3, p4, method) ((  Il2CppObject * (*) (Il2CppObject * /* static, unused */, WeakReference_t2193916456 *, Func_2_t2135783352 *, int32_t, Il2CppObject*, CancellationToken_t1439151560 , const MethodInfo*))ObserveExtensions_PublishPocoValueChanged_TisIl2CppObject_TisIl2CppObject_m3962287592_gshared)(__this /* static, unused */, p0, p1, p2, p3, p4, method)
// System.Collections.Generic.IEqualityComparer`1<!!0> UniRx.UnityEqualityComparer::GetDefault<System.Double>()
extern "C"  Il2CppObject* UnityEqualityComparer_GetDefault_TisDouble_t534516614_m1655977039_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define UnityEqualityComparer_GetDefault_TisDouble_t534516614_m1655977039(__this /* static, unused */, method) ((  Il2CppObject* (*) (Il2CppObject * /* static, unused */, const MethodInfo*))UnityEqualityComparer_GetDefault_TisDouble_t534516614_m1655977039_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.IEqualityComparer`1<!!0> UniRx.UnityEqualityComparer::GetDefault<System.Object>()
extern "C"  Il2CppObject* UnityEqualityComparer_GetDefault_TisIl2CppObject_m972533281_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define UnityEqualityComparer_GetDefault_TisIl2CppObject_m972533281(__this /* static, unused */, method) ((  Il2CppObject* (*) (Il2CppObject * /* static, unused */, const MethodInfo*))UnityEqualityComparer_GetDefault_TisIl2CppObject_m972533281_gshared)(__this /* static, unused */, method)
// System.Boolean UniRx.OptimizedObservableExtensions::IsRequiredSubscribeOnCurrentThread<System.Object>(UniRx.IObservable`1<!!0>)
extern "C"  bool OptimizedObservableExtensions_IsRequiredSubscribeOnCurrentThread_TisIl2CppObject_m1909184259_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* p0, const MethodInfo* method);
#define OptimizedObservableExtensions_IsRequiredSubscribeOnCurrentThread_TisIl2CppObject_m1909184259(__this /* static, unused */, p0, method) ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))OptimizedObservableExtensions_IsRequiredSubscribeOnCurrentThread_TisIl2CppObject_m1909184259_gshared)(__this /* static, unused */, p0, method)
// System.Boolean UniRx.OptimizedObservableExtensions::IsRequiredSubscribeOnCurrentThread<System.Boolean>(UniRx.IObservable`1<!!0>)
extern "C"  bool OptimizedObservableExtensions_IsRequiredSubscribeOnCurrentThread_TisBoolean_t211005341_m545890482_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* p0, const MethodInfo* method);
#define OptimizedObservableExtensions_IsRequiredSubscribeOnCurrentThread_TisBoolean_t211005341_m545890482(__this /* static, unused */, p0, method) ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))OptimizedObservableExtensions_IsRequiredSubscribeOnCurrentThread_TisBoolean_t211005341_m545890482_gshared)(__this /* static, unused */, p0, method)
// System.Boolean UniRx.OptimizedObservableExtensions::IsRequiredSubscribeOnCurrentThread<UniRx.Unit>(UniRx.IObservable`1<!!0>)
extern "C"  bool OptimizedObservableExtensions_IsRequiredSubscribeOnCurrentThread_TisUnit_t2558286038_m849542303_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* p0, const MethodInfo* method);
#define OptimizedObservableExtensions_IsRequiredSubscribeOnCurrentThread_TisUnit_t2558286038_m849542303(__this /* static, unused */, p0, method) ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))OptimizedObservableExtensions_IsRequiredSubscribeOnCurrentThread_TisUnit_t2558286038_m849542303_gshared)(__this /* static, unused */, p0, method)
// System.Boolean UniRx.OptimizedObservableExtensions::IsRequiredSubscribeOnCurrentThread<System.Int64>(UniRx.IObservable`1<!!0>)
extern "C"  bool OptimizedObservableExtensions_IsRequiredSubscribeOnCurrentThread_TisInt64_t2847414882_m2547913869_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* p0, const MethodInfo* method);
#define OptimizedObservableExtensions_IsRequiredSubscribeOnCurrentThread_TisInt64_t2847414882_m2547913869(__this /* static, unused */, p0, method) ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))OptimizedObservableExtensions_IsRequiredSubscribeOnCurrentThread_TisInt64_t2847414882_m2547913869_gshared)(__this /* static, unused */, p0, method)
// UniRx.IObservable`1<!!0> UniRx.Stubs::CatchIgnore<System.Object>(System.Exception)
extern "C"  Il2CppObject* Stubs_CatchIgnore_TisIl2CppObject_m2918437690_gshared (Il2CppObject * __this /* static, unused */, Exception_t1967233988 * p0, const MethodInfo* method);
#define Stubs_CatchIgnore_TisIl2CppObject_m2918437690(__this /* static, unused */, p0, method) ((  Il2CppObject* (*) (Il2CppObject * /* static, unused */, Exception_t1967233988 *, const MethodInfo*))Stubs_CatchIgnore_TisIl2CppObject_m2918437690_gshared)(__this /* static, unused */, p0, method)
// UniRx.IObservable`1<!!0> UniRx.Observable::Empty<System.Object>()
extern "C"  Il2CppObject* Observable_Empty_TisIl2CppObject_m3547328287_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define Observable_Empty_TisIl2CppObject_m3547328287(__this /* static, unused */, method) ((  Il2CppObject* (*) (Il2CppObject * /* static, unused */, const MethodInfo*))Observable_Empty_TisIl2CppObject_m3547328287_gshared)(__this /* static, unused */, method)
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void UniRx.Observable/<OnErrorRetry>c__AnonStorey3D`2<System.Object,System.Object>::.ctor()
extern "C"  void U3COnErrorRetryU3Ec__AnonStorey3D_2__ctor_m3988424000_gshared (U3COnErrorRetryU3Ec__AnonStorey3D_2_t4266989836 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// UniRx.IObservable`1<TSource> UniRx.Observable/<OnErrorRetry>c__AnonStorey3D`2<System.Object,System.Object>::<>m__3F()
extern Il2CppClass* TimeSpan_t763862892_il2cpp_TypeInfo_var;
extern Il2CppClass* Observable_t258071701_il2cpp_TypeInfo_var;
extern const uint32_t U3COnErrorRetryU3Ec__AnonStorey3D_2_U3CU3Em__3F_m2928952763_MetadataUsageId;
extern "C"  Il2CppObject* U3COnErrorRetryU3Ec__AnonStorey3D_2_U3CU3Em__3F_m2928952763_gshared (U3COnErrorRetryU3Ec__AnonStorey3D_2_t4266989836 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3COnErrorRetryU3Ec__AnonStorey3D_2_U3CU3Em__3F_m2928952763_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	U3COnErrorRetryU3Ec__AnonStorey3E_2_t1069553519 * V_0 = NULL;
	U3COnErrorRetryU3Ec__AnonStorey3E_2_t1069553519 * G_B2_0 = NULL;
	U3COnErrorRetryU3Ec__AnonStorey3E_2_t1069553519 * G_B1_0 = NULL;
	TimeSpan_t763862892  G_B3_0;
	memset(&G_B3_0, 0, sizeof(G_B3_0));
	U3COnErrorRetryU3Ec__AnonStorey3E_2_t1069553519 * G_B3_1 = NULL;
	{
		U3COnErrorRetryU3Ec__AnonStorey3E_2_t1069553519 * L_0 = (U3COnErrorRetryU3Ec__AnonStorey3E_2_t1069553519 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		((  void (*) (U3COnErrorRetryU3Ec__AnonStorey3E_2_t1069553519 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		V_0 = (U3COnErrorRetryU3Ec__AnonStorey3E_2_t1069553519 *)L_0;
		U3COnErrorRetryU3Ec__AnonStorey3E_2_t1069553519 * L_1 = V_0;
		NullCheck(L_1);
		L_1->set_U3CU3Ef__refU2461_3(__this);
		U3COnErrorRetryU3Ec__AnonStorey3E_2_t1069553519 * L_2 = V_0;
		TimeSpan_t763862892 * L_3 = (TimeSpan_t763862892 *)__this->get_address_of_delay_0();
		int64_t L_4 = TimeSpan_get_Ticks_m315930342((TimeSpan_t763862892 *)L_3, /*hidden argument*/NULL);
		G_B1_0 = L_2;
		if ((((int64_t)L_4) >= ((int64_t)(((int64_t)((int64_t)0))))))
		{
			G_B2_0 = L_2;
			goto IL_002a;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(TimeSpan_t763862892_il2cpp_TypeInfo_var);
		TimeSpan_t763862892  L_5 = ((TimeSpan_t763862892_StaticFields*)TimeSpan_t763862892_il2cpp_TypeInfo_var->static_fields)->get_Zero_7();
		G_B3_0 = L_5;
		G_B3_1 = G_B1_0;
		goto IL_0030;
	}

IL_002a:
	{
		TimeSpan_t763862892  L_6 = (TimeSpan_t763862892 )__this->get_delay_0();
		G_B3_0 = L_6;
		G_B3_1 = G_B2_0;
	}

IL_0030:
	{
		NullCheck(G_B3_1);
		G_B3_1->set_dueTime_1(G_B3_0);
		U3COnErrorRetryU3Ec__AnonStorey3E_2_t1069553519 * L_7 = V_0;
		NullCheck(L_7);
		L_7->set_count_0(0);
		U3COnErrorRetryU3Ec__AnonStorey3E_2_t1069553519 * L_8 = V_0;
		NullCheck(L_8);
		L_8->set_self_2((Il2CppObject*)NULL);
		U3COnErrorRetryU3Ec__AnonStorey3E_2_t1069553519 * L_9 = V_0;
		Il2CppObject* L_10 = (Il2CppObject*)__this->get_source_1();
		U3COnErrorRetryU3Ec__AnonStorey3E_2_t1069553519 * L_11 = V_0;
		IntPtr_t L_12;
		L_12.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		Func_2_t1894581716 * L_13 = (Func_2_t1894581716 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		((  void (*) (Func_2_t1894581716 *, Il2CppObject *, IntPtr_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)(L_13, (Il2CppObject *)L_11, (IntPtr_t)L_12, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		IL2CPP_RUNTIME_CLASS_INIT(Observable_t258071701_il2cpp_TypeInfo_var);
		Il2CppObject* L_14 = ((  Il2CppObject* (*) (Il2CppObject * /* static, unused */, Il2CppObject*, Func_2_t1894581716 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->method)(NULL /*static, unused*/, (Il2CppObject*)L_10, (Func_2_t1894581716 *)L_13, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		NullCheck(L_9);
		L_9->set_self_2(L_14);
		U3COnErrorRetryU3Ec__AnonStorey3E_2_t1069553519 * L_15 = V_0;
		NullCheck(L_15);
		Il2CppObject* L_16 = (Il2CppObject*)L_15->get_self_2();
		return L_16;
	}
}
// System.Void UniRx.Observable/<RepeatInfinite>c__IteratorE`1<System.Object>::.ctor()
extern "C"  void U3CRepeatInfiniteU3Ec__IteratorE_1__ctor_m4193162815_gshared (U3CRepeatInfiniteU3Ec__IteratorE_1_t1925948945 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// UniRx.IObservable`1<T> UniRx.Observable/<RepeatInfinite>c__IteratorE`1<System.Object>::System.Collections.Generic.IEnumerator<UniRx.IObservable<T>>.get_Current()
extern "C"  Il2CppObject* U3CRepeatInfiniteU3Ec__IteratorE_1_System_Collections_Generic_IEnumeratorU3CUniRx_IObservableU3CTU3EU3E_get_Current_m2072144633_gshared (U3CRepeatInfiniteU3Ec__IteratorE_1_t1925948945 * __this, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = (Il2CppObject*)__this->get_U24current_2();
		return L_0;
	}
}
// System.Object UniRx.Observable/<RepeatInfinite>c__IteratorE`1<System.Object>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CRepeatInfiniteU3Ec__IteratorE_1_System_Collections_IEnumerator_get_Current_m836332753_gshared (U3CRepeatInfiniteU3Ec__IteratorE_1_t1925948945 * __this, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = (Il2CppObject*)__this->get_U24current_2();
		return L_0;
	}
}
// System.Collections.IEnumerator UniRx.Observable/<RepeatInfinite>c__IteratorE`1<System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * U3CRepeatInfiniteU3Ec__IteratorE_1_System_Collections_IEnumerable_GetEnumerator_m4037698746_gshared (U3CRepeatInfiniteU3Ec__IteratorE_1_t1925948945 * __this, const MethodInfo* method)
{
	{
		NullCheck((U3CRepeatInfiniteU3Ec__IteratorE_1_t1925948945 *)__this);
		Il2CppObject* L_0 = ((  Il2CppObject* (*) (U3CRepeatInfiniteU3Ec__IteratorE_1_t1925948945 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((U3CRepeatInfiniteU3Ec__IteratorE_1_t1925948945 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		return L_0;
	}
}
// System.Collections.Generic.IEnumerator`1<UniRx.IObservable`1<T>> UniRx.Observable/<RepeatInfinite>c__IteratorE`1<System.Object>::System.Collections.Generic.IEnumerable<UniRx.IObservable<T>>.GetEnumerator()
extern "C"  Il2CppObject* U3CRepeatInfiniteU3Ec__IteratorE_1_System_Collections_Generic_IEnumerableU3CUniRx_IObservableU3CTU3EU3E_GetEnumerator_m3722258232_gshared (U3CRepeatInfiniteU3Ec__IteratorE_1_t1925948945 * __this, const MethodInfo* method)
{
	U3CRepeatInfiniteU3Ec__IteratorE_1_t1925948945 * V_0 = NULL;
	{
		int32_t* L_0 = (int32_t*)__this->get_address_of_U24PC_1();
		int32_t L_1 = Interlocked_CompareExchange_m1859820752(NULL /*static, unused*/, (int32_t*)L_0, (int32_t)0, (int32_t)((int32_t)-2), /*hidden argument*/NULL);
		if ((!(((uint32_t)L_1) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0014;
		}
	}
	{
		return __this;
	}

IL_0014:
	{
		U3CRepeatInfiniteU3Ec__IteratorE_1_t1925948945 * L_2 = (U3CRepeatInfiniteU3Ec__IteratorE_1_t1925948945 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		((  void (*) (U3CRepeatInfiniteU3Ec__IteratorE_1_t1925948945 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)(L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		V_0 = (U3CRepeatInfiniteU3Ec__IteratorE_1_t1925948945 *)L_2;
		U3CRepeatInfiniteU3Ec__IteratorE_1_t1925948945 * L_3 = V_0;
		Il2CppObject* L_4 = (Il2CppObject*)__this->get_U3CU24U3Esource_3();
		NullCheck(L_3);
		L_3->set_source_0(L_4);
		U3CRepeatInfiniteU3Ec__IteratorE_1_t1925948945 * L_5 = V_0;
		return L_5;
	}
}
// System.Boolean UniRx.Observable/<RepeatInfinite>c__IteratorE`1<System.Object>::MoveNext()
extern "C"  bool U3CRepeatInfiniteU3Ec__IteratorE_1_MoveNext_m3993799973_gshared (U3CRepeatInfiniteU3Ec__IteratorE_1_t1925948945 * __this, const MethodInfo* method)
{
	uint32_t V_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_U24PC_1();
		V_0 = (uint32_t)L_0;
		__this->set_U24PC_1((-1));
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0021;
		}
		if (L_1 == 1)
		{
			goto IL_0039;
		}
	}
	{
		goto IL_0045;
	}

IL_0021:
	{
		Il2CppObject* L_2 = (Il2CppObject*)__this->get_source_0();
		__this->set_U24current_2(L_2);
		__this->set_U24PC_1(1);
		goto IL_0047;
	}

IL_0039:
	{
		goto IL_0021;
	}
	// Dead block : IL_003e: ldarg.0

IL_0045:
	{
		return (bool)0;
	}

IL_0047:
	{
		return (bool)1;
	}
}
// System.Void UniRx.Observable/<RepeatInfinite>c__IteratorE`1<System.Object>::Dispose()
extern "C"  void U3CRepeatInfiniteU3Ec__IteratorE_1_Dispose_m848424636_gshared (U3CRepeatInfiniteU3Ec__IteratorE_1_t1925948945 * __this, const MethodInfo* method)
{
	{
		__this->set_U24PC_1((-1));
		return;
	}
}
// System.Void UniRx.Observable/<RepeatInfinite>c__IteratorE`1<System.Object>::Reset()
extern Il2CppClass* NotSupportedException_t1374155497_il2cpp_TypeInfo_var;
extern const uint32_t U3CRepeatInfiniteU3Ec__IteratorE_1_Reset_m1839595756_MetadataUsageId;
extern "C"  void U3CRepeatInfiniteU3Ec__IteratorE_1_Reset_m1839595756_gshared (U3CRepeatInfiniteU3Ec__IteratorE_1_t1925948945 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CRepeatInfiniteU3Ec__IteratorE_1_Reset_m1839595756_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1374155497 * L_0 = (NotSupportedException_t1374155497 *)il2cpp_codegen_object_new(NotSupportedException_t1374155497_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m149930845(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void UniRx.Observable/<SelectMany>c__AnonStorey49`2<System.Object,System.Object>::.ctor()
extern "C"  void U3CSelectManyU3Ec__AnonStorey49_2__ctor_m1590108432_gshared (U3CSelectManyU3Ec__AnonStorey49_2_t2420169340 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// UniRx.IObservable`1<TR> UniRx.Observable/<SelectMany>c__AnonStorey49`2<System.Object,System.Object>::<>m__47(T)
extern "C"  Il2CppObject* U3CSelectManyU3Ec__AnonStorey49_2_U3CU3Em__47_m2471360920_gshared (U3CSelectManyU3Ec__AnonStorey49_2_t2420169340 * __this, Il2CppObject * ____0, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = (Il2CppObject*)__this->get_other_0();
		return L_0;
	}
}
// System.Void UniRx.Observable/<SelectMany>c__AnonStorey49`2<System.Object,UniRx.Unit>::.ctor()
extern "C"  void U3CSelectManyU3Ec__AnonStorey49_2__ctor_m2308581076_gshared (U3CSelectManyU3Ec__AnonStorey49_2_t4141348958 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// UniRx.IObservable`1<TR> UniRx.Observable/<SelectMany>c__AnonStorey49`2<System.Object,UniRx.Unit>::<>m__47(T)
extern "C"  Il2CppObject* U3CSelectManyU3Ec__AnonStorey49_2_U3CU3Em__47_m3377694824_gshared (U3CSelectManyU3Ec__AnonStorey49_2_t4141348958 * __this, Il2CppObject * ____0, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = (Il2CppObject*)__this->get_other_0();
		return L_0;
	}
}
// System.Void UniRx.Observable/<SelectMany>c__AnonStorey49`2<UniRx.Unit,UniRx.Unit>::.ctor()
extern "C"  void U3CSelectManyU3Ec__AnonStorey49_2__ctor_m1605157740_gshared (U3CSelectManyU3Ec__AnonStorey49_2_t1102810292 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// UniRx.IObservable`1<TR> UniRx.Observable/<SelectMany>c__AnonStorey49`2<UniRx.Unit,UniRx.Unit>::<>m__47(T)
extern "C"  Il2CppObject* U3CSelectManyU3Ec__AnonStorey49_2_U3CU3Em__47_m3016679996_gshared (U3CSelectManyU3Ec__AnonStorey49_2_t1102810292 * __this, Unit_t2558286038  ____0, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = (Il2CppObject*)__this->get_other_0();
		return L_0;
	}
}
// System.Void UniRx.Observable/<SelectMany>c__AnonStorey4D`1<System.Object>::.ctor()
extern "C"  void U3CSelectManyU3Ec__AnonStorey4D_1__ctor_m3157912424_gshared (U3CSelectManyU3Ec__AnonStorey4D_1_t352756178 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Collections.IEnumerator UniRx.Observable/<SelectMany>c__AnonStorey4D`1<System.Object>::<>m__4B()
extern "C"  Il2CppObject * U3CSelectManyU3Ec__AnonStorey4D_1_U3CU3Em__4B_m3947555951_gshared (U3CSelectManyU3Ec__AnonStorey4D_1_t352756178 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_coroutine_0();
		return L_0;
	}
}
// System.Void UniRx.Observable/<SelectMany>c__AnonStorey4E`1<System.Object>::.ctor()
extern "C"  void U3CSelectManyU3Ec__AnonStorey4E_1__ctor_m4119526441_gshared (U3CSelectManyU3Ec__AnonStorey4E_1_t4202698475 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Collections.IEnumerator UniRx.Observable/<SelectMany>c__AnonStorey4E`1<System.Object>::<>m__4C()
extern const MethodInfo* Func_1_Invoke_m2952332176_MethodInfo_var;
extern const uint32_t U3CSelectManyU3Ec__AnonStorey4E_1_U3CU3Em__4C_m345691313_MetadataUsageId;
extern "C"  Il2CppObject * U3CSelectManyU3Ec__AnonStorey4E_1_U3CU3Em__4C_m345691313_gshared (U3CSelectManyU3Ec__AnonStorey4E_1_t4202698475 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CSelectManyU3Ec__AnonStorey4E_1_U3CU3Em__4C_m345691313_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Func_1_t1429988286 * L_0 = (Func_1_t1429988286 *)__this->get_selector_0();
		NullCheck((Func_1_t1429988286 *)L_0);
		Il2CppObject * L_1 = Func_1_Invoke_m2952332176((Func_1_t1429988286 *)L_0, /*hidden argument*/Func_1_Invoke_m2952332176_MethodInfo_var);
		return L_1;
	}
}
// System.Void UniRx.Observable/<SelectMany>c__AnonStorey4E`1<UniRx.Unit>::.ctor()
extern "C"  void U3CSelectManyU3Ec__AnonStorey4E_1__ctor_m1139591771_gshared (U3CSelectManyU3Ec__AnonStorey4E_1_t1628910797 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Collections.IEnumerator UniRx.Observable/<SelectMany>c__AnonStorey4E`1<UniRx.Unit>::<>m__4C()
extern const MethodInfo* Func_1_Invoke_m2952332176_MethodInfo_var;
extern const uint32_t U3CSelectManyU3Ec__AnonStorey4E_1_U3CU3Em__4C_m3577479683_MetadataUsageId;
extern "C"  Il2CppObject * U3CSelectManyU3Ec__AnonStorey4E_1_U3CU3Em__4C_m3577479683_gshared (U3CSelectManyU3Ec__AnonStorey4E_1_t1628910797 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CSelectManyU3Ec__AnonStorey4E_1_U3CU3Em__4C_m3577479683_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Func_1_t1429988286 * L_0 = (Func_1_t1429988286 *)__this->get_selector_0();
		NullCheck((Func_1_t1429988286 *)L_0);
		Il2CppObject * L_1 = Func_1_Invoke_m2952332176((Func_1_t1429988286 *)L_0, /*hidden argument*/Func_1_Invoke_m2952332176_MethodInfo_var);
		return L_1;
	}
}
// System.Void UniRx.Observable/<SelectMany>c__AnonStorey4F`1/<SelectMany>c__AnonStorey50`1<System.Object>::.ctor()
extern "C"  void U3CSelectManyU3Ec__AnonStorey50_1__ctor_m2643319079_gshared (U3CSelectManyU3Ec__AnonStorey50_1_t4047415781 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Collections.IEnumerator UniRx.Observable/<SelectMany>c__AnonStorey4F`1/<SelectMany>c__AnonStorey50`1<System.Object>::<>m__68()
extern "C"  Il2CppObject * U3CSelectManyU3Ec__AnonStorey50_1_U3CU3Em__68_m722859426_gshared (U3CSelectManyU3Ec__AnonStorey50_1_t4047415781 * __this, const MethodInfo* method)
{
	{
		U3CSelectManyU3Ec__AnonStorey4F_1_t3757673476 * L_0 = (U3CSelectManyU3Ec__AnonStorey4F_1_t3757673476 *)__this->get_U3CU3Ef__refU2479_1();
		NullCheck(L_0);
		Func_2_t1585883971 * L_1 = (Func_2_t1585883971 *)L_0->get_selector_0();
		Il2CppObject * L_2 = (Il2CppObject *)__this->get_x_0();
		NullCheck((Func_2_t1585883971 *)L_1);
		Il2CppObject * L_3 = ((  Il2CppObject * (*) (Func_2_t1585883971 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((Func_2_t1585883971 *)L_1, (Il2CppObject *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		return L_3;
	}
}
// System.Void UniRx.Observable/<SelectMany>c__AnonStorey4F`1<System.Object>::.ctor()
extern "C"  void U3CSelectManyU3Ec__AnonStorey4F_1__ctor_m786173162_gshared (U3CSelectManyU3Ec__AnonStorey4F_1_t3757673476 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// UniRx.IObservable`1<UniRx.Unit> UniRx.Observable/<SelectMany>c__AnonStorey4F`1<System.Object>::<>m__4D(T)
extern Il2CppClass* Func_1_t1429988286_il2cpp_TypeInfo_var;
extern Il2CppClass* Observable_t258071701_il2cpp_TypeInfo_var;
extern const MethodInfo* Func_1__ctor_m4104442892_MethodInfo_var;
extern const uint32_t U3CSelectManyU3Ec__AnonStorey4F_1_U3CU3Em__4D_m2236246835_MetadataUsageId;
extern "C"  Il2CppObject* U3CSelectManyU3Ec__AnonStorey4F_1_U3CU3Em__4D_m2236246835_gshared (U3CSelectManyU3Ec__AnonStorey4F_1_t3757673476 * __this, Il2CppObject * ___x0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CSelectManyU3Ec__AnonStorey4F_1_U3CU3Em__4D_m2236246835_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	U3CSelectManyU3Ec__AnonStorey50_1_t4047415781 * V_0 = NULL;
	{
		U3CSelectManyU3Ec__AnonStorey50_1_t4047415781 * L_0 = (U3CSelectManyU3Ec__AnonStorey50_1_t4047415781 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		((  void (*) (U3CSelectManyU3Ec__AnonStorey50_1_t4047415781 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		V_0 = (U3CSelectManyU3Ec__AnonStorey50_1_t4047415781 *)L_0;
		U3CSelectManyU3Ec__AnonStorey50_1_t4047415781 * L_1 = V_0;
		NullCheck(L_1);
		L_1->set_U3CU3Ef__refU2479_1(__this);
		U3CSelectManyU3Ec__AnonStorey50_1_t4047415781 * L_2 = V_0;
		Il2CppObject * L_3 = ___x0;
		NullCheck(L_2);
		L_2->set_x_0(L_3);
		U3CSelectManyU3Ec__AnonStorey50_1_t4047415781 * L_4 = V_0;
		IntPtr_t L_5;
		L_5.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		Func_1_t1429988286 * L_6 = (Func_1_t1429988286 *)il2cpp_codegen_object_new(Func_1_t1429988286_il2cpp_TypeInfo_var);
		Func_1__ctor_m4104442892(L_6, (Il2CppObject *)L_4, (IntPtr_t)L_5, /*hidden argument*/Func_1__ctor_m4104442892_MethodInfo_var);
		IL2CPP_RUNTIME_CLASS_INIT(Observable_t258071701_il2cpp_TypeInfo_var);
		Il2CppObject* L_7 = Observable_FromCoroutine_m3913040964(NULL /*static, unused*/, (Func_1_t1429988286 *)L_6, (bool)0, /*hidden argument*/NULL);
		return L_7;
	}
}
// System.Void UniRx.Observable/<ToAsync>c__AnonStorey38`1/<ToAsync>c__AnonStorey39`1<System.Object>::.ctor()
extern "C"  void U3CToAsyncU3Ec__AnonStorey39_1__ctor_m4082454551_gshared (U3CToAsyncU3Ec__AnonStorey39_1_t776464662 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UniRx.Observable/<ToAsync>c__AnonStorey38`1/<ToAsync>c__AnonStorey39`1<System.Object>::<>m__62()
extern Il2CppClass* Il2CppObject_il2cpp_TypeInfo_var;
extern Il2CppClass* Exception_t1967233988_il2cpp_TypeInfo_var;
extern const uint32_t U3CToAsyncU3Ec__AnonStorey39_1_U3CU3Em__62_m4241873116_MetadataUsageId;
extern "C"  void U3CToAsyncU3Ec__AnonStorey39_1_U3CU3Em__62_m4241873116_gshared (U3CToAsyncU3Ec__AnonStorey39_1_t776464662 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CToAsyncU3Ec__AnonStorey39_1_U3CU3Em__62_m4241873116_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Il2CppObject * V_0 = NULL;
	Exception_t1967233988 * V_1 = NULL;
	Il2CppObject * V_2 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Initobj (Il2CppObject_il2cpp_TypeInfo_var, (&V_2));
		Il2CppObject * L_0 = V_2;
		V_0 = (Il2CppObject *)L_0;
	}

IL_000a:
	try
	{ // begin try (depth: 1)
		U3CToAsyncU3Ec__AnonStorey38_1_t1221489661 * L_1 = (U3CToAsyncU3Ec__AnonStorey38_1_t1221489661 *)__this->get_U3CU3Ef__refU2456_1();
		NullCheck(L_1);
		Func_1_t1979887667 * L_2 = (Func_1_t1979887667 *)L_1->get_function_1();
		NullCheck((Func_1_t1979887667 *)L_2);
		Il2CppObject * L_3 = ((  Il2CppObject * (*) (Func_1_t1979887667 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((Func_1_t1979887667 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		V_0 = (Il2CppObject *)L_3;
		goto IL_0037;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1967233988 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Exception_t1967233988_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_0020;
		throw e;
	}

CATCH_0020:
	{ // begin catch(System.Exception)
		{
			V_1 = (Exception_t1967233988 *)((Exception_t1967233988 *)__exception_local);
			AsyncSubject_1_t1536745524 * L_4 = (AsyncSubject_1_t1536745524 *)__this->get_subject_0();
			Exception_t1967233988 * L_5 = V_1;
			NullCheck((AsyncSubject_1_t1536745524 *)L_4);
			((  void (*) (AsyncSubject_1_t1536745524 *, Exception_t1967233988 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)((AsyncSubject_1_t1536745524 *)L_4, (Exception_t1967233988 *)L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
			goto IL_004e;
		}

IL_0032:
		{
			; // IL_0032: leave IL_0037
		}
	} // end catch (depth: 1)

IL_0037:
	{
		AsyncSubject_1_t1536745524 * L_6 = (AsyncSubject_1_t1536745524 *)__this->get_subject_0();
		Il2CppObject * L_7 = V_0;
		NullCheck((AsyncSubject_1_t1536745524 *)L_6);
		((  void (*) (AsyncSubject_1_t1536745524 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)((AsyncSubject_1_t1536745524 *)L_6, (Il2CppObject *)L_7, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		AsyncSubject_1_t1536745524 * L_8 = (AsyncSubject_1_t1536745524 *)__this->get_subject_0();
		NullCheck((AsyncSubject_1_t1536745524 *)L_8);
		((  void (*) (AsyncSubject_1_t1536745524 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)((AsyncSubject_1_t1536745524 *)L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
	}

IL_004e:
	{
		return;
	}
}
// System.Void UniRx.Observable/<ToAsync>c__AnonStorey38`1<System.Object>::.ctor()
extern "C"  void U3CToAsyncU3Ec__AnonStorey38_1__ctor_m266223115_gshared (U3CToAsyncU3Ec__AnonStorey38_1_t1221489661 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// UniRx.IObservable`1<T> UniRx.Observable/<ToAsync>c__AnonStorey38`1<System.Object>::<>m__3C()
extern Il2CppClass* Action_t437523947_il2cpp_TypeInfo_var;
extern Il2CppClass* IScheduler_t2938318244_il2cpp_TypeInfo_var;
extern Il2CppClass* Observable_t258071701_il2cpp_TypeInfo_var;
extern const uint32_t U3CToAsyncU3Ec__AnonStorey38_1_U3CU3Em__3C_m2161775360_MetadataUsageId;
extern "C"  Il2CppObject* U3CToAsyncU3Ec__AnonStorey38_1_U3CU3Em__3C_m2161775360_gshared (U3CToAsyncU3Ec__AnonStorey38_1_t1221489661 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CToAsyncU3Ec__AnonStorey38_1_U3CU3Em__3C_m2161775360_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	U3CToAsyncU3Ec__AnonStorey39_1_t776464662 * V_0 = NULL;
	{
		U3CToAsyncU3Ec__AnonStorey39_1_t776464662 * L_0 = (U3CToAsyncU3Ec__AnonStorey39_1_t776464662 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		((  void (*) (U3CToAsyncU3Ec__AnonStorey39_1_t776464662 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		V_0 = (U3CToAsyncU3Ec__AnonStorey39_1_t776464662 *)L_0;
		U3CToAsyncU3Ec__AnonStorey39_1_t776464662 * L_1 = V_0;
		NullCheck(L_1);
		L_1->set_U3CU3Ef__refU2456_1(__this);
		U3CToAsyncU3Ec__AnonStorey39_1_t776464662 * L_2 = V_0;
		AsyncSubject_1_t1536745524 * L_3 = (AsyncSubject_1_t1536745524 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		((  void (*) (AsyncSubject_1_t1536745524 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)(L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		NullCheck(L_2);
		L_2->set_subject_0(L_3);
		Il2CppObject * L_4 = (Il2CppObject *)__this->get_scheduler_0();
		U3CToAsyncU3Ec__AnonStorey39_1_t776464662 * L_5 = V_0;
		IntPtr_t L_6;
		L_6.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		Action_t437523947 * L_7 = (Action_t437523947 *)il2cpp_codegen_object_new(Action_t437523947_il2cpp_TypeInfo_var);
		Action__ctor_m2957240604(L_7, (Il2CppObject *)L_5, (IntPtr_t)L_6, /*hidden argument*/NULL);
		NullCheck((Il2CppObject *)L_4);
		InterfaceFuncInvoker1< Il2CppObject *, Action_t437523947 * >::Invoke(1 /* System.IDisposable UniRx.IScheduler::Schedule(System.Action) */, IScheduler_t2938318244_il2cpp_TypeInfo_var, (Il2CppObject *)L_4, (Action_t437523947 *)L_7);
		U3CToAsyncU3Ec__AnonStorey39_1_t776464662 * L_8 = V_0;
		NullCheck(L_8);
		AsyncSubject_1_t1536745524 * L_9 = (AsyncSubject_1_t1536745524 *)L_8->get_subject_0();
		IL2CPP_RUNTIME_CLASS_INIT(Observable_t258071701_il2cpp_TypeInfo_var);
		Il2CppObject* L_10 = ((  Il2CppObject* (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->method)(NULL /*static, unused*/, (Il2CppObject*)L_9, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		return L_10;
	}
}
// System.Void UniRx.Observable/<ToAwaitableEnumerator>c__Iterator17`1<System.Int32>::.ctor()
extern "C"  void U3CToAwaitableEnumeratorU3Ec__Iterator17_1__ctor_m3534264873_gshared (U3CToAwaitableEnumeratorU3Ec__Iterator17_1_t3131167031 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Object UniRx.Observable/<ToAwaitableEnumerator>c__Iterator17`1<System.Int32>::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CToAwaitableEnumeratorU3Ec__Iterator17_1_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3561053065_gshared (U3CToAwaitableEnumeratorU3Ec__Iterator17_1_t3131167031 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_U24current_7();
		return L_0;
	}
}
// System.Object UniRx.Observable/<ToAwaitableEnumerator>c__Iterator17`1<System.Int32>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CToAwaitableEnumeratorU3Ec__Iterator17_1_System_Collections_IEnumerator_get_Current_m4249194781_gshared (U3CToAwaitableEnumeratorU3Ec__Iterator17_1_t3131167031 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_U24current_7();
		return L_0;
	}
}
// System.Boolean UniRx.Observable/<ToAwaitableEnumerator>c__Iterator17`1<System.Int32>::MoveNext()
extern Il2CppClass* IEnumerator_t287207039_il2cpp_TypeInfo_var;
extern const MethodInfo* Action_1_Invoke_m1286428014_MethodInfo_var;
extern const uint32_t U3CToAwaitableEnumeratorU3Ec__Iterator17_1_MoveNext_m2460421187_MetadataUsageId;
extern "C"  bool U3CToAwaitableEnumeratorU3Ec__Iterator17_1_MoveNext_m2460421187_gshared (U3CToAwaitableEnumeratorU3Ec__Iterator17_1_t3131167031 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CToAwaitableEnumeratorU3Ec__Iterator17_1_MoveNext_m2460421187_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	uint32_t V_0 = 0;
	bool V_1 = false;
	{
		int32_t L_0 = (int32_t)__this->get_U24PC_6();
		V_0 = (uint32_t)L_0;
		__this->set_U24PC_6((-1));
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0021;
		}
		if (L_1 == 1)
		{
			goto IL_0057;
		}
	}
	{
		goto IL_00ef;
	}

IL_0021:
	{
		Il2CppObject* L_2 = (Il2CppObject*)__this->get_source_0();
		ObservableYieldInstruction_1_t674360375 * L_3 = (ObservableYieldInstruction_1_t674360375 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		((  void (*) (ObservableYieldInstruction_1_t674360375 *, Il2CppObject*, bool, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)(L_3, (Il2CppObject*)L_2, (bool)0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		__this->set_U3CenumeratorU3E__0_1(L_3);
		ObservableYieldInstruction_1_t674360375 * L_4 = (ObservableYieldInstruction_1_t674360375 *)__this->get_U3CenumeratorU3E__0_1();
		__this->set_U3CeU3E__1_2(L_4);
		goto IL_0057;
	}

IL_0044:
	{
		__this->set_U24current_7(NULL);
		__this->set_U24PC_6(1);
		goto IL_00f1;
	}

IL_0057:
	{
		Il2CppObject* L_5 = (Il2CppObject*)__this->get_U3CeU3E__1_2();
		NullCheck((Il2CppObject *)L_5);
		bool L_6 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t287207039_il2cpp_TypeInfo_var, (Il2CppObject *)L_5);
		if (!L_6)
		{
			goto IL_0077;
		}
	}
	{
		CancellationToken_t1439151560 * L_7 = (CancellationToken_t1439151560 *)__this->get_address_of_cancel_3();
		bool L_8 = CancellationToken_get_IsCancellationRequested_m1021497867((CancellationToken_t1439151560 *)L_7, /*hidden argument*/NULL);
		if (!L_8)
		{
			goto IL_0044;
		}
	}

IL_0077:
	{
		CancellationToken_t1439151560 * L_9 = (CancellationToken_t1439151560 *)__this->get_address_of_cancel_3();
		bool L_10 = CancellationToken_get_IsCancellationRequested_m1021497867((CancellationToken_t1439151560 *)L_9, /*hidden argument*/NULL);
		if (!L_10)
		{
			goto IL_0097;
		}
	}
	{
		ObservableYieldInstruction_1_t674360375 * L_11 = (ObservableYieldInstruction_1_t674360375 *)__this->get_U3CenumeratorU3E__0_1();
		NullCheck((ObservableYieldInstruction_1_t674360375 *)L_11);
		VirtActionInvoker0::Invoke(4 /* System.Void UniRx.ObservableYieldInstruction`1<System.Int32>::Dispose() */, (ObservableYieldInstruction_1_t674360375 *)L_11);
		goto IL_00ef;
	}

IL_0097:
	{
		ObservableYieldInstruction_1_t674360375 * L_12 = (ObservableYieldInstruction_1_t674360375 *)__this->get_U3CenumeratorU3E__0_1();
		NullCheck((ObservableYieldInstruction_1_t674360375 *)L_12);
		bool L_13 = ((  bool (*) (ObservableYieldInstruction_1_t674360375 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)((ObservableYieldInstruction_1_t674360375 *)L_12, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		if (!L_13)
		{
			goto IL_00c2;
		}
	}
	{
		Action_1_t2995867492 * L_14 = (Action_1_t2995867492 *)__this->get_onResult_4();
		ObservableYieldInstruction_1_t674360375 * L_15 = (ObservableYieldInstruction_1_t674360375 *)__this->get_U3CenumeratorU3E__0_1();
		NullCheck((ObservableYieldInstruction_1_t674360375 *)L_15);
		int32_t L_16 = ((  int32_t (*) (ObservableYieldInstruction_1_t674360375 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)((ObservableYieldInstruction_1_t674360375 *)L_15, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		NullCheck((Action_1_t2995867492 *)L_14);
		((  void (*) (Action_1_t2995867492 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->method)((Action_1_t2995867492 *)L_14, (int32_t)L_16, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		goto IL_00e8;
	}

IL_00c2:
	{
		ObservableYieldInstruction_1_t674360375 * L_17 = (ObservableYieldInstruction_1_t674360375 *)__this->get_U3CenumeratorU3E__0_1();
		NullCheck((ObservableYieldInstruction_1_t674360375 *)L_17);
		bool L_18 = ((  bool (*) (ObservableYieldInstruction_1_t674360375 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6)->method)((ObservableYieldInstruction_1_t674360375 *)L_17, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		if (!L_18)
		{
			goto IL_00e8;
		}
	}
	{
		Action_1_t2115686693 * L_19 = (Action_1_t2115686693 *)__this->get_onError_5();
		ObservableYieldInstruction_1_t674360375 * L_20 = (ObservableYieldInstruction_1_t674360375 *)__this->get_U3CenumeratorU3E__0_1();
		NullCheck((ObservableYieldInstruction_1_t674360375 *)L_20);
		Exception_t1967233988 * L_21 = ((  Exception_t1967233988 * (*) (ObservableYieldInstruction_1_t674360375 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7)->method)((ObservableYieldInstruction_1_t674360375 *)L_20, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7));
		NullCheck((Action_1_t2115686693 *)L_19);
		Action_1_Invoke_m1286428014((Action_1_t2115686693 *)L_19, (Exception_t1967233988 *)L_21, /*hidden argument*/Action_1_Invoke_m1286428014_MethodInfo_var);
	}

IL_00e8:
	{
		__this->set_U24PC_6((-1));
	}

IL_00ef:
	{
		return (bool)0;
	}

IL_00f1:
	{
		return (bool)1;
	}
	// Dead block : IL_00f3: ldloc.1
}
// System.Void UniRx.Observable/<ToAwaitableEnumerator>c__Iterator17`1<System.Int32>::Dispose()
extern "C"  void U3CToAwaitableEnumeratorU3Ec__Iterator17_1_Dispose_m3302662182_gshared (U3CToAwaitableEnumeratorU3Ec__Iterator17_1_t3131167031 * __this, const MethodInfo* method)
{
	{
		__this->set_U24PC_6((-1));
		return;
	}
}
// System.Void UniRx.Observable/<ToAwaitableEnumerator>c__Iterator17`1<System.Int32>::Reset()
extern Il2CppClass* NotSupportedException_t1374155497_il2cpp_TypeInfo_var;
extern const uint32_t U3CToAwaitableEnumeratorU3Ec__Iterator17_1_Reset_m1180697814_MetadataUsageId;
extern "C"  void U3CToAwaitableEnumeratorU3Ec__Iterator17_1_Reset_m1180697814_gshared (U3CToAwaitableEnumeratorU3Ec__Iterator17_1_t3131167031 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CToAwaitableEnumeratorU3Ec__Iterator17_1_Reset_m1180697814_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1374155497 * L_0 = (NotSupportedException_t1374155497 *)il2cpp_codegen_object_new(NotSupportedException_t1374155497_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m149930845(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void UniRx.Observable/<ToAwaitableEnumerator>c__Iterator17`1<System.Object>::.ctor()
extern "C"  void U3CToAwaitableEnumeratorU3Ec__Iterator17_1__ctor_m3641051694_gshared (U3CToAwaitableEnumeratorU3Ec__Iterator17_1_t1120858664 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Object UniRx.Observable/<ToAwaitableEnumerator>c__Iterator17`1<System.Object>::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CToAwaitableEnumeratorU3Ec__Iterator17_1_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3896765870_gshared (U3CToAwaitableEnumeratorU3Ec__Iterator17_1_t1120858664 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_U24current_7();
		return L_0;
	}
}
// System.Object UniRx.Observable/<ToAwaitableEnumerator>c__Iterator17`1<System.Object>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CToAwaitableEnumeratorU3Ec__Iterator17_1_System_Collections_IEnumerator_get_Current_m2603814210_gshared (U3CToAwaitableEnumeratorU3Ec__Iterator17_1_t1120858664 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_U24current_7();
		return L_0;
	}
}
// System.Boolean UniRx.Observable/<ToAwaitableEnumerator>c__Iterator17`1<System.Object>::MoveNext()
extern Il2CppClass* IEnumerator_t287207039_il2cpp_TypeInfo_var;
extern const MethodInfo* Action_1_Invoke_m1286428014_MethodInfo_var;
extern const uint32_t U3CToAwaitableEnumeratorU3Ec__Iterator17_1_MoveNext_m3369239254_MetadataUsageId;
extern "C"  bool U3CToAwaitableEnumeratorU3Ec__Iterator17_1_MoveNext_m3369239254_gshared (U3CToAwaitableEnumeratorU3Ec__Iterator17_1_t1120858664 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CToAwaitableEnumeratorU3Ec__Iterator17_1_MoveNext_m3369239254_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	uint32_t V_0 = 0;
	bool V_1 = false;
	{
		int32_t L_0 = (int32_t)__this->get_U24PC_6();
		V_0 = (uint32_t)L_0;
		__this->set_U24PC_6((-1));
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0021;
		}
		if (L_1 == 1)
		{
			goto IL_0057;
		}
	}
	{
		goto IL_00ef;
	}

IL_0021:
	{
		Il2CppObject* L_2 = (Il2CppObject*)__this->get_source_0();
		ObservableYieldInstruction_1_t2959019304 * L_3 = (ObservableYieldInstruction_1_t2959019304 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		((  void (*) (ObservableYieldInstruction_1_t2959019304 *, Il2CppObject*, bool, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)(L_3, (Il2CppObject*)L_2, (bool)0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		__this->set_U3CenumeratorU3E__0_1(L_3);
		ObservableYieldInstruction_1_t2959019304 * L_4 = (ObservableYieldInstruction_1_t2959019304 *)__this->get_U3CenumeratorU3E__0_1();
		__this->set_U3CeU3E__1_2(L_4);
		goto IL_0057;
	}

IL_0044:
	{
		__this->set_U24current_7(NULL);
		__this->set_U24PC_6(1);
		goto IL_00f1;
	}

IL_0057:
	{
		Il2CppObject* L_5 = (Il2CppObject*)__this->get_U3CeU3E__1_2();
		NullCheck((Il2CppObject *)L_5);
		bool L_6 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t287207039_il2cpp_TypeInfo_var, (Il2CppObject *)L_5);
		if (!L_6)
		{
			goto IL_0077;
		}
	}
	{
		CancellationToken_t1439151560 * L_7 = (CancellationToken_t1439151560 *)__this->get_address_of_cancel_3();
		bool L_8 = CancellationToken_get_IsCancellationRequested_m1021497867((CancellationToken_t1439151560 *)L_7, /*hidden argument*/NULL);
		if (!L_8)
		{
			goto IL_0044;
		}
	}

IL_0077:
	{
		CancellationToken_t1439151560 * L_9 = (CancellationToken_t1439151560 *)__this->get_address_of_cancel_3();
		bool L_10 = CancellationToken_get_IsCancellationRequested_m1021497867((CancellationToken_t1439151560 *)L_9, /*hidden argument*/NULL);
		if (!L_10)
		{
			goto IL_0097;
		}
	}
	{
		ObservableYieldInstruction_1_t2959019304 * L_11 = (ObservableYieldInstruction_1_t2959019304 *)__this->get_U3CenumeratorU3E__0_1();
		NullCheck((ObservableYieldInstruction_1_t2959019304 *)L_11);
		VirtActionInvoker0::Invoke(4 /* System.Void UniRx.ObservableYieldInstruction`1<System.Object>::Dispose() */, (ObservableYieldInstruction_1_t2959019304 *)L_11);
		goto IL_00ef;
	}

IL_0097:
	{
		ObservableYieldInstruction_1_t2959019304 * L_12 = (ObservableYieldInstruction_1_t2959019304 *)__this->get_U3CenumeratorU3E__0_1();
		NullCheck((ObservableYieldInstruction_1_t2959019304 *)L_12);
		bool L_13 = ((  bool (*) (ObservableYieldInstruction_1_t2959019304 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)((ObservableYieldInstruction_1_t2959019304 *)L_12, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		if (!L_13)
		{
			goto IL_00c2;
		}
	}
	{
		Action_1_t985559125 * L_14 = (Action_1_t985559125 *)__this->get_onResult_4();
		ObservableYieldInstruction_1_t2959019304 * L_15 = (ObservableYieldInstruction_1_t2959019304 *)__this->get_U3CenumeratorU3E__0_1();
		NullCheck((ObservableYieldInstruction_1_t2959019304 *)L_15);
		Il2CppObject * L_16 = ((  Il2CppObject * (*) (ObservableYieldInstruction_1_t2959019304 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)((ObservableYieldInstruction_1_t2959019304 *)L_15, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		NullCheck((Action_1_t985559125 *)L_14);
		((  void (*) (Action_1_t985559125 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->method)((Action_1_t985559125 *)L_14, (Il2CppObject *)L_16, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		goto IL_00e8;
	}

IL_00c2:
	{
		ObservableYieldInstruction_1_t2959019304 * L_17 = (ObservableYieldInstruction_1_t2959019304 *)__this->get_U3CenumeratorU3E__0_1();
		NullCheck((ObservableYieldInstruction_1_t2959019304 *)L_17);
		bool L_18 = ((  bool (*) (ObservableYieldInstruction_1_t2959019304 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6)->method)((ObservableYieldInstruction_1_t2959019304 *)L_17, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		if (!L_18)
		{
			goto IL_00e8;
		}
	}
	{
		Action_1_t2115686693 * L_19 = (Action_1_t2115686693 *)__this->get_onError_5();
		ObservableYieldInstruction_1_t2959019304 * L_20 = (ObservableYieldInstruction_1_t2959019304 *)__this->get_U3CenumeratorU3E__0_1();
		NullCheck((ObservableYieldInstruction_1_t2959019304 *)L_20);
		Exception_t1967233988 * L_21 = ((  Exception_t1967233988 * (*) (ObservableYieldInstruction_1_t2959019304 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7)->method)((ObservableYieldInstruction_1_t2959019304 *)L_20, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7));
		NullCheck((Action_1_t2115686693 *)L_19);
		Action_1_Invoke_m1286428014((Action_1_t2115686693 *)L_19, (Exception_t1967233988 *)L_21, /*hidden argument*/Action_1_Invoke_m1286428014_MethodInfo_var);
	}

IL_00e8:
	{
		__this->set_U24PC_6((-1));
	}

IL_00ef:
	{
		return (bool)0;
	}

IL_00f1:
	{
		return (bool)1;
	}
	// Dead block : IL_00f3: ldloc.1
}
// System.Void UniRx.Observable/<ToAwaitableEnumerator>c__Iterator17`1<System.Object>::Dispose()
extern "C"  void U3CToAwaitableEnumeratorU3Ec__Iterator17_1_Dispose_m2845582059_gshared (U3CToAwaitableEnumeratorU3Ec__Iterator17_1_t1120858664 * __this, const MethodInfo* method)
{
	{
		__this->set_U24PC_6((-1));
		return;
	}
}
// System.Void UniRx.Observable/<ToAwaitableEnumerator>c__Iterator17`1<System.Object>::Reset()
extern Il2CppClass* NotSupportedException_t1374155497_il2cpp_TypeInfo_var;
extern const uint32_t U3CToAwaitableEnumeratorU3Ec__Iterator17_1_Reset_m1287484635_MetadataUsageId;
extern "C"  void U3CToAwaitableEnumeratorU3Ec__Iterator17_1_Reset_m1287484635_gshared (U3CToAwaitableEnumeratorU3Ec__Iterator17_1_t1120858664 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CToAwaitableEnumeratorU3Ec__Iterator17_1_Reset_m1287484635_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1374155497 * L_0 = (NotSupportedException_t1374155497 *)il2cpp_codegen_object_new(NotSupportedException_t1374155497_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m149930845(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void UniRx.Observable/<WrapEnumeratorYieldValue>c__Iterator10`1<System.Object>::.ctor()
extern "C"  void U3CWrapEnumeratorYieldValueU3Ec__Iterator10_1__ctor_m3165280184_gshared (U3CWrapEnumeratorYieldValueU3Ec__Iterator10_1_t896010658 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Object UniRx.Observable/<WrapEnumeratorYieldValue>c__Iterator10`1<System.Object>::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CWrapEnumeratorYieldValueU3Ec__Iterator10_1_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1595304922_gshared (U3CWrapEnumeratorYieldValueU3Ec__Iterator10_1_t896010658 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_U24current_12();
		return L_0;
	}
}
// System.Object UniRx.Observable/<WrapEnumeratorYieldValue>c__Iterator10`1<System.Object>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CWrapEnumeratorYieldValueU3Ec__Iterator10_1_System_Collections_IEnumerator_get_Current_m3158270830_gshared (U3CWrapEnumeratorYieldValueU3Ec__Iterator10_1_t896010658 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_U24current_12();
		return L_0;
	}
}
// System.Boolean UniRx.Observable/<WrapEnumeratorYieldValue>c__Iterator10`1<System.Object>::MoveNext()
extern Il2CppClass* IEnumerator_t287207039_il2cpp_TypeInfo_var;
extern Il2CppClass* Exception_t1967233988_il2cpp_TypeInfo_var;
extern Il2CppClass* Observable_t258071701_il2cpp_TypeInfo_var;
extern Il2CppClass* Il2CppObject_il2cpp_TypeInfo_var;
extern Il2CppClass* IDisposable_t1628921374_il2cpp_TypeInfo_var;
extern const uint32_t U3CWrapEnumeratorYieldValueU3Ec__Iterator10_1_MoveNext_m1303354964_MetadataUsageId;
extern "C"  bool U3CWrapEnumeratorYieldValueU3Ec__Iterator10_1_MoveNext_m1303354964_gshared (U3CWrapEnumeratorYieldValueU3Ec__Iterator10_1_t896010658 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CWrapEnumeratorYieldValueU3Ec__Iterator10_1_MoveNext_m1303354964_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	uint32_t V_0 = 0;
	Exception_t1967233988 * V_1 = NULL;
	bool V_2 = false;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		int32_t L_0 = (int32_t)__this->get_U24PC_11();
		V_0 = (uint32_t)L_0;
		__this->set_U24PC_11((-1));
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0029;
		}
		if (L_1 == 1)
		{
			goto IL_00ee;
		}
		if (L_1 == 2)
		{
			goto IL_011b;
		}
		if (L_1 == 3)
		{
			goto IL_0149;
		}
	}
	{
		goto IL_01ec;
	}

IL_0029:
	{
		__this->set_U3ChasNextU3E__0_0((bool)0);
		__this->set_U3CcurrentU3E__1_1(NULL);
		__this->set_U3CraisedErrorU3E__2_2((bool)0);
	}

IL_003e:
	try
	{ // begin try (depth: 1)
		{
			Il2CppObject * L_2 = (Il2CppObject *)__this->get_enumerator_3();
			NullCheck((Il2CppObject *)L_2);
			bool L_3 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t287207039_il2cpp_TypeInfo_var, (Il2CppObject *)L_2);
			__this->set_U3ChasNextU3E__0_0(L_3);
			bool L_4 = (bool)__this->get_U3ChasNextU3E__0_0();
			if (!L_4)
			{
				goto IL_006b;
			}
		}

IL_005a:
		{
			Il2CppObject * L_5 = (Il2CppObject *)__this->get_enumerator_3();
			NullCheck((Il2CppObject *)L_5);
			Il2CppObject * L_6 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(0 /* System.Object System.Collections.IEnumerator::get_Current() */, IEnumerator_t287207039_il2cpp_TypeInfo_var, (Il2CppObject *)L_5);
			__this->set_U3CcurrentU3E__1_1(L_6);
		}

IL_006b:
		{
			goto IL_00a6;
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1967233988 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Exception_t1967233988_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_0070;
		throw e;
	}

CATCH_0070:
	{ // begin catch(System.Exception)
		{
			V_1 = (Exception_t1967233988 *)((Exception_t1967233988 *)__exception_local);
			Exception_t1967233988 * L_7 = V_1;
			__this->set_U3CexU3E__3_5(L_7);
		}

IL_0078:
		try
		{ // begin try (depth: 2)
			__this->set_U3CraisedErrorU3E__2_2((bool)1);
			Il2CppObject* L_8 = (Il2CppObject*)__this->get_observer_4();
			Exception_t1967233988 * L_9 = (Exception_t1967233988 *)__this->get_U3CexU3E__3_5();
			NullCheck((Il2CppObject*)L_8);
			InterfaceActionInvoker1< Exception_t1967233988 * >::Invoke(1 /* System.Void UniRx.IObserver`1<System.Object>::OnError(System.Exception) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0), (Il2CppObject*)L_8, (Exception_t1967233988 *)L_9);
			IL2CPP_LEAVE(0x9C, FINALLY_0095);
		} // end try (depth: 2)
		catch(Il2CppExceptionWrapper& e)
		{
			__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
			goto FINALLY_0095;
		}

FINALLY_0095:
		{ // begin finally (depth: 2)
			NullCheck((U3CWrapEnumeratorYieldValueU3Ec__Iterator10_1_t896010658 *)__this);
			((  void (*) (U3CWrapEnumeratorYieldValueU3Ec__Iterator10_1_t896010658 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)((U3CWrapEnumeratorYieldValueU3Ec__Iterator10_1_t896010658 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
			IL2CPP_END_FINALLY(149)
		} // end finally (depth: 2)
		IL2CPP_CLEANUP(149)
		{
			IL2CPP_JUMP_TBL(0x9C, IL_009c)
			IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
		}

IL_009c:
		{
			goto IL_01ec;
		}

IL_00a1:
		{
			; // IL_00a1: leave IL_00a6
		}
	} // end catch (depth: 1)

IL_00a6:
	{
		bool L_10 = (bool)__this->get_U3ChasNextU3E__0_0();
		if (!L_10)
		{
			goto IL_0198;
		}
	}
	{
		Il2CppObject * L_11 = (Il2CppObject *)__this->get_U3CcurrentU3E__1_1();
		if (!L_11)
		{
			goto IL_00f3;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Observable_t258071701_il2cpp_TypeInfo_var);
		HashSet_1_t1182951310 * L_12 = ((Observable_t258071701_StaticFields*)Observable_t258071701_il2cpp_TypeInfo_var->static_fields)->get_YieldInstructionTypes_1();
		Il2CppObject * L_13 = (Il2CppObject *)__this->get_U3CcurrentU3E__1_1();
		NullCheck((Il2CppObject *)L_13);
		Type_t * L_14 = Object_GetType_m2022236990((Il2CppObject *)L_13, /*hidden argument*/NULL);
		NullCheck((HashSet_1_t1182951310 *)L_12);
		bool L_15 = VirtFuncInvoker1< bool, Type_t * >::Invoke(8 /* System.Boolean System.Collections.Generic.HashSet`1<System.Type>::Contains(!0) */, (HashSet_1_t1182951310 *)L_12, (Type_t *)L_14);
		if (!L_15)
		{
			goto IL_00f3;
		}
	}
	{
		Il2CppObject * L_16 = (Il2CppObject *)__this->get_U3CcurrentU3E__1_1();
		__this->set_U24current_12(L_16);
		__this->set_U24PC_11(1);
		goto IL_01ee;
	}

IL_00ee:
	{
		goto IL_0198;
	}

IL_00f3:
	{
		Il2CppObject * L_17 = (Il2CppObject *)__this->get_U3CcurrentU3E__1_1();
		if (!((Il2CppObject *)IsInst(L_17, IEnumerator_t287207039_il2cpp_TypeInfo_var)))
		{
			goto IL_0120;
		}
	}
	{
		Il2CppObject * L_18 = (Il2CppObject *)__this->get_U3CcurrentU3E__1_1();
		__this->set_U24current_12(L_18);
		__this->set_U24PC_11(2);
		goto IL_01ee;
	}

IL_011b:
	{
		goto IL_0198;
	}

IL_0120:
	{
		Il2CppObject * L_19 = (Il2CppObject *)__this->get_U3CcurrentU3E__1_1();
		if (L_19)
		{
			goto IL_014e;
		}
	}
	{
		bool L_20 = (bool)__this->get_nullAsNextUpdate_7();
		if (!L_20)
		{
			goto IL_014e;
		}
	}
	{
		__this->set_U24current_12(NULL);
		__this->set_U24PC_11(3);
		goto IL_01ee;
	}

IL_0149:
	{
		goto IL_0198;
	}

IL_014e:
	try
	{ // begin try (depth: 1)
		Il2CppObject* L_21 = (Il2CppObject*)__this->get_observer_4();
		Il2CppObject * L_22 = (Il2CppObject *)__this->get_U3CcurrentU3E__1_1();
		NullCheck((Il2CppObject*)L_21);
		InterfaceActionInvoker1< Il2CppObject * >::Invoke(2 /* System.Void UniRx.IObserver`1<System.Object>::OnNext(T) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0), (Il2CppObject*)L_21, (Il2CppObject *)((Il2CppObject *)Castclass(L_22, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2))));
		goto IL_0198;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1967233988 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Il2CppObject_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_0169;
		throw e;
	}

CATCH_0169:
	{ // begin catch(System.Object)
		{
			Il2CppObject * L_23 = (Il2CppObject *)__this->get_enumerator_3();
			__this->set_U3CdU3E__5_8(((Il2CppObject *)IsInst(L_23, IDisposable_t1628921374_il2cpp_TypeInfo_var)));
			Il2CppObject * L_24 = (Il2CppObject *)__this->get_U3CdU3E__5_8();
			if (!L_24)
			{
				goto IL_0191;
			}
		}

IL_0186:
		{
			Il2CppObject * L_25 = (Il2CppObject *)__this->get_U3CdU3E__5_8();
			NullCheck((Il2CppObject *)L_25);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, (Il2CppObject *)L_25);
		}

IL_0191:
		{
			IL2CPP_RAISE_MANAGED_EXCEPTION(__exception_local);
		}

IL_0193:
		{
			goto IL_0198;
		}
	} // end catch (depth: 1)

IL_0198:
	{
		bool L_26 = (bool)__this->get_U3ChasNextU3E__0_0();
		if (!L_26)
		{
			goto IL_01b3;
		}
	}
	{
		CancellationToken_t1439151560 * L_27 = (CancellationToken_t1439151560 *)__this->get_address_of_cancellationToken_9();
		bool L_28 = CancellationToken_get_IsCancellationRequested_m1021497867((CancellationToken_t1439151560 *)L_27, /*hidden argument*/NULL);
		if (!L_28)
		{
			goto IL_003e;
		}
	}

IL_01b3:
	try
	{ // begin try (depth: 1)
		{
			bool L_29 = (bool)__this->get_U3CraisedErrorU3E__2_2();
			if (L_29)
			{
				goto IL_01d9;
			}
		}

IL_01be:
		{
			CancellationToken_t1439151560 * L_30 = (CancellationToken_t1439151560 *)__this->get_address_of_cancellationToken_9();
			bool L_31 = CancellationToken_get_IsCancellationRequested_m1021497867((CancellationToken_t1439151560 *)L_30, /*hidden argument*/NULL);
			if (L_31)
			{
				goto IL_01d9;
			}
		}

IL_01ce:
		{
			Il2CppObject* L_32 = (Il2CppObject*)__this->get_observer_4();
			NullCheck((Il2CppObject*)L_32);
			InterfaceActionInvoker0::Invoke(0 /* System.Void UniRx.IObserver`1<System.Object>::OnCompleted() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0), (Il2CppObject*)L_32);
		}

IL_01d9:
		{
			IL2CPP_LEAVE(0x1E5, FINALLY_01de);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_01de;
	}

FINALLY_01de:
	{ // begin finally (depth: 1)
		NullCheck((U3CWrapEnumeratorYieldValueU3Ec__Iterator10_1_t896010658 *)__this);
		((  void (*) (U3CWrapEnumeratorYieldValueU3Ec__Iterator10_1_t896010658 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)((U3CWrapEnumeratorYieldValueU3Ec__Iterator10_1_t896010658 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		IL2CPP_END_FINALLY(478)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(478)
	{
		IL2CPP_JUMP_TBL(0x1E5, IL_01e5)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_01e5:
	{
		__this->set_U24PC_11((-1));
	}

IL_01ec:
	{
		return (bool)0;
	}

IL_01ee:
	{
		return (bool)1;
	}
	// Dead block : IL_01f0: ldloc.2
}
// System.Void UniRx.Observable/<WrapEnumeratorYieldValue>c__Iterator10`1<System.Object>::Dispose()
extern "C"  void U3CWrapEnumeratorYieldValueU3Ec__Iterator10_1_Dispose_m895694325_gshared (U3CWrapEnumeratorYieldValueU3Ec__Iterator10_1_t896010658 * __this, const MethodInfo* method)
{
	{
		__this->set_U24PC_11((-1));
		return;
	}
}
// System.Void UniRx.Observable/<WrapEnumeratorYieldValue>c__Iterator10`1<System.Object>::Reset()
extern Il2CppClass* NotSupportedException_t1374155497_il2cpp_TypeInfo_var;
extern const uint32_t U3CWrapEnumeratorYieldValueU3Ec__Iterator10_1_Reset_m811713125_MetadataUsageId;
extern "C"  void U3CWrapEnumeratorYieldValueU3Ec__Iterator10_1_Reset_m811713125_gshared (U3CWrapEnumeratorYieldValueU3Ec__Iterator10_1_t896010658 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CWrapEnumeratorYieldValueU3Ec__Iterator10_1_Reset_m811713125_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1374155497 * L_0 = (NotSupportedException_t1374155497 *)il2cpp_codegen_object_new(NotSupportedException_t1374155497_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m149930845(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void UniRx.Observable/<WrapEnumeratorYieldValue>c__Iterator10`1<System.Object>::<>__Finally0()
extern Il2CppClass* IDisposable_t1628921374_il2cpp_TypeInfo_var;
extern const uint32_t U3CWrapEnumeratorYieldValueU3Ec__Iterator10_1_U3CU3E__Finally0_m597996475_MetadataUsageId;
extern "C"  void U3CWrapEnumeratorYieldValueU3Ec__Iterator10_1_U3CU3E__Finally0_m597996475_gshared (U3CWrapEnumeratorYieldValueU3Ec__Iterator10_1_t896010658 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CWrapEnumeratorYieldValueU3Ec__Iterator10_1_U3CU3E__Finally0_m597996475_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_enumerator_3();
		__this->set_U3CdU3E__4_6(((Il2CppObject *)IsInst(L_0, IDisposable_t1628921374_il2cpp_TypeInfo_var)));
		Il2CppObject * L_1 = (Il2CppObject *)__this->get_U3CdU3E__4_6();
		if (!L_1)
		{
			goto IL_0027;
		}
	}
	{
		Il2CppObject * L_2 = (Il2CppObject *)__this->get_U3CdU3E__4_6();
		NullCheck((Il2CppObject *)L_2);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, (Il2CppObject *)L_2);
	}

IL_0027:
	{
		return;
	}
}
// System.Void UniRx.Observable/<WrapEnumeratorYieldValue>c__Iterator10`1<System.Object>::<>__Finally1()
extern Il2CppClass* IDisposable_t1628921374_il2cpp_TypeInfo_var;
extern const uint32_t U3CWrapEnumeratorYieldValueU3Ec__Iterator10_1_U3CU3E__Finally1_m597997436_MetadataUsageId;
extern "C"  void U3CWrapEnumeratorYieldValueU3Ec__Iterator10_1_U3CU3E__Finally1_m597997436_gshared (U3CWrapEnumeratorYieldValueU3Ec__Iterator10_1_t896010658 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CWrapEnumeratorYieldValueU3Ec__Iterator10_1_U3CU3E__Finally1_m597997436_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_enumerator_3();
		__this->set_U3CdU3E__6_10(((Il2CppObject *)IsInst(L_0, IDisposable_t1628921374_il2cpp_TypeInfo_var)));
		Il2CppObject * L_1 = (Il2CppObject *)__this->get_U3CdU3E__6_10();
		if (!L_1)
		{
			goto IL_0027;
		}
	}
	{
		Il2CppObject * L_2 = (Il2CppObject *)__this->get_U3CdU3E__6_10();
		NullCheck((Il2CppObject *)L_2);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, (Il2CppObject *)L_2);
	}

IL_0027:
	{
		return;
	}
}
// System.Void UniRx.Observable/ConnectableObservable`1/Connection<System.Object>::.ctor(UniRx.Observable/ConnectableObservable`1<T>,System.IDisposable)
extern "C"  void Connection__ctor_m4285632808_gshared (Connection_t217434055 * __this, ConnectableObservable_1_t608944977 * ___parent0, Il2CppObject * ___subscription1, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		ConnectableObservable_1_t608944977 * L_0 = ___parent0;
		__this->set_parent_0(L_0);
		Il2CppObject * L_1 = ___subscription1;
		__this->set_subscription_1(L_1);
		return;
	}
}
// System.Void UniRx.Observable/ConnectableObservable`1/Connection<System.Object>::Dispose()
extern Il2CppClass* IDisposable_t1628921374_il2cpp_TypeInfo_var;
extern const uint32_t Connection_Dispose_m1210712569_MetadataUsageId;
extern "C"  void Connection_Dispose_m1210712569_gshared (Connection_t217434055 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Connection_Dispose_m1210712569_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Il2CppObject * V_0 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		ConnectableObservable_1_t608944977 * L_0 = (ConnectableObservable_1_t608944977 *)__this->get_parent_0();
		NullCheck(L_0);
		Il2CppObject * L_1 = (Il2CppObject *)L_0->get_gate_2();
		V_0 = (Il2CppObject *)L_1;
		Il2CppObject * L_2 = V_0;
		Monitor_Enter_m476686225(NULL /*static, unused*/, (Il2CppObject *)L_2, /*hidden argument*/NULL);
	}

IL_0012:
	try
	{ // begin try (depth: 1)
		{
			Il2CppObject * L_3 = (Il2CppObject *)__this->get_subscription_1();
			if (!L_3)
			{
				goto IL_003b;
			}
		}

IL_001d:
		{
			Il2CppObject * L_4 = (Il2CppObject *)__this->get_subscription_1();
			NullCheck((Il2CppObject *)L_4);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, (Il2CppObject *)L_4);
			__this->set_subscription_1((Il2CppObject *)NULL);
			ConnectableObservable_1_t608944977 * L_5 = (ConnectableObservable_1_t608944977 *)__this->get_parent_0();
			NullCheck(L_5);
			L_5->set_connection_3((Connection_t217434055 *)NULL);
		}

IL_003b:
		{
			IL2CPP_LEAVE(0x47, FINALLY_0040);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0040;
	}

FINALLY_0040:
	{ // begin finally (depth: 1)
		Il2CppObject * L_6 = V_0;
		Monitor_Exit_m2088237919(NULL /*static, unused*/, (Il2CppObject *)L_6, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(64)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(64)
	{
		IL2CPP_JUMP_TBL(0x47, IL_0047)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0047:
	{
		return;
	}
}
// System.Void UniRx.Observable/ConnectableObservable`1<System.Object>::.ctor(UniRx.IObservable`1<T>,UniRx.ISubject`1<T>)
extern Il2CppClass* Il2CppObject_il2cpp_TypeInfo_var;
extern Il2CppClass* Observable_t258071701_il2cpp_TypeInfo_var;
extern const uint32_t ConnectableObservable_1__ctor_m4204051920_MetadataUsageId;
extern "C"  void ConnectableObservable_1__ctor_m4204051920_gshared (ConnectableObservable_1_t608944977 * __this, Il2CppObject* ___source0, Il2CppObject* ___subject1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ConnectableObservable_1__ctor_m4204051920_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject * L_0 = (Il2CppObject *)il2cpp_codegen_object_new(Il2CppObject_il2cpp_TypeInfo_var);
		Object__ctor_m1772956182(L_0, /*hidden argument*/NULL);
		__this->set_gate_2(L_0);
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		Il2CppObject* L_1 = ___source0;
		IL2CPP_RUNTIME_CLASS_INIT(Observable_t258071701_il2cpp_TypeInfo_var);
		Il2CppObject* L_2 = ((  Il2CppObject* (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)(NULL /*static, unused*/, (Il2CppObject*)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		__this->set_source_0(L_2);
		Il2CppObject* L_3 = ___subject1;
		__this->set_subject_1(L_3);
		return;
	}
}
// System.IDisposable UniRx.Observable/ConnectableObservable`1<System.Object>::Connect()
extern "C"  Il2CppObject * ConnectableObservable_1_Connect_m4186812418_gshared (ConnectableObservable_1_t608944977 * __this, const MethodInfo* method)
{
	Il2CppObject * V_0 = NULL;
	Il2CppObject * V_1 = NULL;
	Il2CppObject * V_2 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_gate_2();
		V_0 = (Il2CppObject *)L_0;
		Il2CppObject * L_1 = V_0;
		Monitor_Enter_m476686225(NULL /*static, unused*/, (Il2CppObject *)L_1, /*hidden argument*/NULL);
	}

IL_000d:
	try
	{ // begin try (depth: 1)
		{
			Connection_t217434055 * L_2 = (Connection_t217434055 *)__this->get_connection_3();
			if (L_2)
			{
				goto IL_0037;
			}
		}

IL_0018:
		{
			Il2CppObject* L_3 = (Il2CppObject*)__this->get_source_0();
			Il2CppObject* L_4 = (Il2CppObject*)__this->get_subject_1();
			NullCheck((Il2CppObject*)L_3);
			Il2CppObject * L_5 = InterfaceFuncInvoker1< Il2CppObject *, Il2CppObject* >::Invoke(0 /* System.IDisposable UniRx.IObservable`1<System.Object>::Subscribe(UniRx.IObserver`1<T>) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), (Il2CppObject*)L_3, (Il2CppObject*)L_4);
			V_1 = (Il2CppObject *)L_5;
			Il2CppObject * L_6 = V_1;
			Connection_t217434055 * L_7 = (Connection_t217434055 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
			((  void (*) (Connection_t217434055 *, ConnectableObservable_1_t608944977 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)(L_7, (ConnectableObservable_1_t608944977 *)__this, (Il2CppObject *)L_6, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
			__this->set_connection_3(L_7);
		}

IL_0037:
		{
			Connection_t217434055 * L_8 = (Connection_t217434055 *)__this->get_connection_3();
			V_2 = (Il2CppObject *)L_8;
			IL2CPP_LEAVE(0x4F, FINALLY_0048);
		}

IL_0043:
		{
			; // IL_0043: leave IL_004f
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0048;
	}

FINALLY_0048:
	{ // begin finally (depth: 1)
		Il2CppObject * L_9 = V_0;
		Monitor_Exit_m2088237919(NULL /*static, unused*/, (Il2CppObject *)L_9, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(72)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(72)
	{
		IL2CPP_JUMP_TBL(0x4F, IL_004f)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_004f:
	{
		Il2CppObject * L_10 = V_2;
		return L_10;
	}
}
// System.IDisposable UniRx.Observable/ConnectableObservable`1<System.Object>::Subscribe(UniRx.IObserver`1<T>)
extern "C"  Il2CppObject * ConnectableObservable_1_Subscribe_m1737984212_gshared (ConnectableObservable_1_t608944977 * __this, Il2CppObject* ___observer0, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = (Il2CppObject*)__this->get_subject_1();
		Il2CppObject* L_1 = ___observer0;
		NullCheck((Il2CppObject*)L_0);
		Il2CppObject * L_2 = InterfaceFuncInvoker1< Il2CppObject *, Il2CppObject* >::Invoke(0 /* System.IDisposable UniRx.IObservable`1<System.Object>::Subscribe(UniRx.IObserver`1<T>) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), (Il2CppObject*)L_0, (Il2CppObject*)L_1);
		return L_2;
	}
}
// System.Void UniRx.ObservableYieldInstruction`1/ToYieldInstruction<System.Int32>::.ctor(UniRx.ObservableYieldInstruction`1<T>)
extern "C"  void ToYieldInstruction__ctor_m482937683_gshared (ToYieldInstruction_t3682831524 * __this, ObservableYieldInstruction_1_t674360375 * ___parent0, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		ObservableYieldInstruction_1_t674360375 * L_0 = ___parent0;
		__this->set_parent_0(L_0);
		return;
	}
}
// System.Void UniRx.ObservableYieldInstruction`1/ToYieldInstruction<System.Int32>::OnNext(T)
extern "C"  void ToYieldInstruction_OnNext_m4105776046_gshared (ToYieldInstruction_t3682831524 * __this, int32_t ___value0, const MethodInfo* method)
{
	{
		ObservableYieldInstruction_1_t674360375 * L_0 = (ObservableYieldInstruction_1_t674360375 *)__this->get_parent_0();
		int32_t L_1 = ___value0;
		NullCheck(L_0);
		L_0->set_current_2(L_1);
		return;
	}
}
// System.Void UniRx.ObservableYieldInstruction`1/ToYieldInstruction<System.Int32>::OnError(System.Exception)
extern "C"  void ToYieldInstruction_OnError_m2847681213_gshared (ToYieldInstruction_t3682831524 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method)
{
	{
		ObservableYieldInstruction_1_t674360375 * L_0 = (ObservableYieldInstruction_1_t674360375 *)__this->get_parent_0();
		NullCheck(L_0);
		L_0->set_moveNext_4((bool)0);
		ObservableYieldInstruction_1_t674360375 * L_1 = (ObservableYieldInstruction_1_t674360375 *)__this->get_parent_0();
		Exception_t1967233988 * L_2 = ___error0;
		NullCheck(L_1);
		L_1->set_error_6(L_2);
		ObservableYieldInstruction_1_t674360375 * L_3 = (ObservableYieldInstruction_1_t674360375 *)__this->get_parent_0();
		NullCheck(L_3);
		bool L_4 = (bool)L_3->get_reThrowOnError_1();
		if (!L_4)
		{
			goto IL_002a;
		}
	}
	{
		Exception_t1967233988 * L_5 = ___error0;
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_5);
	}

IL_002a:
	{
		return;
	}
}
// System.Void UniRx.ObservableYieldInstruction`1/ToYieldInstruction<System.Int32>::OnCompleted()
extern "C"  void ToYieldInstruction_OnCompleted_m2899267984_gshared (ToYieldInstruction_t3682831524 * __this, const MethodInfo* method)
{
	{
		ObservableYieldInstruction_1_t674360375 * L_0 = (ObservableYieldInstruction_1_t674360375 *)__this->get_parent_0();
		NullCheck(L_0);
		L_0->set_moveNext_4((bool)0);
		ObservableYieldInstruction_1_t674360375 * L_1 = (ObservableYieldInstruction_1_t674360375 *)__this->get_parent_0();
		NullCheck(L_1);
		L_1->set_hasResult_5((bool)1);
		ObservableYieldInstruction_1_t674360375 * L_2 = (ObservableYieldInstruction_1_t674360375 *)__this->get_parent_0();
		ObservableYieldInstruction_1_t674360375 * L_3 = (ObservableYieldInstruction_1_t674360375 *)__this->get_parent_0();
		NullCheck(L_3);
		int32_t L_4 = (int32_t)L_3->get_current_2();
		NullCheck(L_2);
		L_2->set_result_3(L_4);
		return;
	}
}
// System.Void UniRx.ObservableYieldInstruction`1/ToYieldInstruction<System.Int64>::.ctor(UniRx.ObservableYieldInstruction`1<T>)
extern "C"  void ToYieldInstruction__ctor_m2398821652_gshared (ToYieldInstruction_t3682831619 * __this, ObservableYieldInstruction_1_t674360470 * ___parent0, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		ObservableYieldInstruction_1_t674360470 * L_0 = ___parent0;
		__this->set_parent_0(L_0);
		return;
	}
}
// System.Void UniRx.ObservableYieldInstruction`1/ToYieldInstruction<System.Int64>::OnNext(T)
extern "C"  void ToYieldInstruction_OnNext_m2002294157_gshared (ToYieldInstruction_t3682831619 * __this, int64_t ___value0, const MethodInfo* method)
{
	{
		ObservableYieldInstruction_1_t674360470 * L_0 = (ObservableYieldInstruction_1_t674360470 *)__this->get_parent_0();
		int64_t L_1 = ___value0;
		NullCheck(L_0);
		L_0->set_current_2(L_1);
		return;
	}
}
// System.Void UniRx.ObservableYieldInstruction`1/ToYieldInstruction<System.Int64>::OnError(System.Exception)
extern "C"  void ToYieldInstruction_OnError_m4267832988_gshared (ToYieldInstruction_t3682831619 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method)
{
	{
		ObservableYieldInstruction_1_t674360470 * L_0 = (ObservableYieldInstruction_1_t674360470 *)__this->get_parent_0();
		NullCheck(L_0);
		L_0->set_moveNext_4((bool)0);
		ObservableYieldInstruction_1_t674360470 * L_1 = (ObservableYieldInstruction_1_t674360470 *)__this->get_parent_0();
		Exception_t1967233988 * L_2 = ___error0;
		NullCheck(L_1);
		L_1->set_error_6(L_2);
		ObservableYieldInstruction_1_t674360470 * L_3 = (ObservableYieldInstruction_1_t674360470 *)__this->get_parent_0();
		NullCheck(L_3);
		bool L_4 = (bool)L_3->get_reThrowOnError_1();
		if (!L_4)
		{
			goto IL_002a;
		}
	}
	{
		Exception_t1967233988 * L_5 = ___error0;
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_5);
	}

IL_002a:
	{
		return;
	}
}
// System.Void UniRx.ObservableYieldInstruction`1/ToYieldInstruction<System.Int64>::OnCompleted()
extern "C"  void ToYieldInstruction_OnCompleted_m2614670319_gshared (ToYieldInstruction_t3682831619 * __this, const MethodInfo* method)
{
	{
		ObservableYieldInstruction_1_t674360470 * L_0 = (ObservableYieldInstruction_1_t674360470 *)__this->get_parent_0();
		NullCheck(L_0);
		L_0->set_moveNext_4((bool)0);
		ObservableYieldInstruction_1_t674360470 * L_1 = (ObservableYieldInstruction_1_t674360470 *)__this->get_parent_0();
		NullCheck(L_1);
		L_1->set_hasResult_5((bool)1);
		ObservableYieldInstruction_1_t674360470 * L_2 = (ObservableYieldInstruction_1_t674360470 *)__this->get_parent_0();
		ObservableYieldInstruction_1_t674360470 * L_3 = (ObservableYieldInstruction_1_t674360470 *)__this->get_parent_0();
		NullCheck(L_3);
		int64_t L_4 = (int64_t)L_3->get_current_2();
		NullCheck(L_2);
		L_2->set_result_3(L_4);
		return;
	}
}
// System.Void UniRx.ObservableYieldInstruction`1/ToYieldInstruction<System.Object>::.ctor(UniRx.ObservableYieldInstruction`1<T>)
extern "C"  void ToYieldInstruction__ctor_m2997720904_gshared (ToYieldInstruction_t1672523157 * __this, ObservableYieldInstruction_1_t2959019304 * ___parent0, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		ObservableYieldInstruction_1_t2959019304 * L_0 = ___parent0;
		__this->set_parent_0(L_0);
		return;
	}
}
// System.Void UniRx.ObservableYieldInstruction`1/ToYieldInstruction<System.Object>::OnNext(T)
extern "C"  void ToYieldInstruction_OnNext_m4194015193_gshared (ToYieldInstruction_t1672523157 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	{
		ObservableYieldInstruction_1_t2959019304 * L_0 = (ObservableYieldInstruction_1_t2959019304 *)__this->get_parent_0();
		Il2CppObject * L_1 = ___value0;
		NullCheck(L_0);
		L_0->set_current_2(L_1);
		return;
	}
}
// System.Void UniRx.ObservableYieldInstruction`1/ToYieldInstruction<System.Object>::OnError(System.Exception)
extern "C"  void ToYieldInstruction_OnError_m556720360_gshared (ToYieldInstruction_t1672523157 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method)
{
	{
		ObservableYieldInstruction_1_t2959019304 * L_0 = (ObservableYieldInstruction_1_t2959019304 *)__this->get_parent_0();
		NullCheck(L_0);
		L_0->set_moveNext_4((bool)0);
		ObservableYieldInstruction_1_t2959019304 * L_1 = (ObservableYieldInstruction_1_t2959019304 *)__this->get_parent_0();
		Exception_t1967233988 * L_2 = ___error0;
		NullCheck(L_1);
		L_1->set_error_6(L_2);
		ObservableYieldInstruction_1_t2959019304 * L_3 = (ObservableYieldInstruction_1_t2959019304 *)__this->get_parent_0();
		NullCheck(L_3);
		bool L_4 = (bool)L_3->get_reThrowOnError_1();
		if (!L_4)
		{
			goto IL_002a;
		}
	}
	{
		Exception_t1967233988 * L_5 = ___error0;
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_5);
	}

IL_002a:
	{
		return;
	}
}
// System.Void UniRx.ObservableYieldInstruction`1/ToYieldInstruction<System.Object>::OnCompleted()
extern "C"  void ToYieldInstruction_OnCompleted_m895070267_gshared (ToYieldInstruction_t1672523157 * __this, const MethodInfo* method)
{
	{
		ObservableYieldInstruction_1_t2959019304 * L_0 = (ObservableYieldInstruction_1_t2959019304 *)__this->get_parent_0();
		NullCheck(L_0);
		L_0->set_moveNext_4((bool)0);
		ObservableYieldInstruction_1_t2959019304 * L_1 = (ObservableYieldInstruction_1_t2959019304 *)__this->get_parent_0();
		NullCheck(L_1);
		L_1->set_hasResult_5((bool)1);
		ObservableYieldInstruction_1_t2959019304 * L_2 = (ObservableYieldInstruction_1_t2959019304 *)__this->get_parent_0();
		ObservableYieldInstruction_1_t2959019304 * L_3 = (ObservableYieldInstruction_1_t2959019304 *)__this->get_parent_0();
		NullCheck(L_3);
		Il2CppObject * L_4 = (Il2CppObject *)L_3->get_current_2();
		NullCheck(L_2);
		L_2->set_result_3(L_4);
		return;
	}
}
// System.Void UniRx.ObservableYieldInstruction`1<System.Int32>::.ctor(UniRx.IObservable`1<T>,System.Boolean)
extern Il2CppClass* Il2CppObject_il2cpp_TypeInfo_var;
extern const uint32_t ObservableYieldInstruction_1__ctor_m297500463_MetadataUsageId;
extern "C"  void ObservableYieldInstruction_1__ctor_m297500463_gshared (ObservableYieldInstruction_1_t674360375 * __this, Il2CppObject* ___source0, bool ___reThrowOnError1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ObservableYieldInstruction_1__ctor_m297500463_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		__this->set_moveNext_4((bool)1);
		bool L_0 = ___reThrowOnError1;
		__this->set_reThrowOnError_1(L_0);
	}

IL_0014:
	try
	{ // begin try (depth: 1)
		Il2CppObject* L_1 = ___source0;
		ToYieldInstruction_t3682831524 * L_2 = (ToYieldInstruction_t3682831524 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		((  void (*) (ToYieldInstruction_t3682831524 *, ObservableYieldInstruction_1_t674360375 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)(L_2, (ObservableYieldInstruction_1_t674360375 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		NullCheck((Il2CppObject*)L_1);
		Il2CppObject * L_3 = InterfaceFuncInvoker1< Il2CppObject *, Il2CppObject* >::Invoke(0 /* System.IDisposable UniRx.IObservable`1<System.Int32>::Subscribe(UniRx.IObserver`1<T>) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), (Il2CppObject*)L_1, (Il2CppObject*)L_2);
		__this->set_subscription_0(L_3);
		goto IL_003a;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1967233988 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Il2CppObject_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_002b;
		throw e;
	}

CATCH_002b:
	{ // begin catch(System.Object)
		{
			__this->set_moveNext_4((bool)0);
			IL2CPP_RAISE_MANAGED_EXCEPTION(__exception_local);
		}

IL_0035:
		{
			goto IL_003a;
		}
	} // end catch (depth: 1)

IL_003a:
	{
		return;
	}
}
// T UniRx.ObservableYieldInstruction`1<System.Int32>::System.Collections.Generic.IEnumerator<T>.get_Current()
extern "C"  int32_t ObservableYieldInstruction_1_System_Collections_Generic_IEnumeratorU3CTU3E_get_Current_m1920955096_gshared (ObservableYieldInstruction_1_t674360375 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (int32_t)__this->get_current_2();
		return L_0;
	}
}
// System.Object UniRx.ObservableYieldInstruction`1<System.Int32>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * ObservableYieldInstruction_1_System_Collections_IEnumerator_get_Current_m2828006369_gshared (ObservableYieldInstruction_1_t674360375 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (int32_t)__this->get_current_2();
		int32_t L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3), &L_1);
		return L_2;
	}
}
// System.Boolean UniRx.ObservableYieldInstruction`1<System.Int32>::System.Collections.IEnumerator.MoveNext()
extern "C"  bool ObservableYieldInstruction_1_System_Collections_IEnumerator_MoveNext_m994877134_gshared (ObservableYieldInstruction_1_t674360375 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (bool)__this->get_moveNext_4();
		return L_0;
	}
}
// System.Void UniRx.ObservableYieldInstruction`1<System.Int32>::System.Collections.IEnumerator.Reset()
extern Il2CppClass* NotSupportedException_t1374155497_il2cpp_TypeInfo_var;
extern const uint32_t ObservableYieldInstruction_1_System_Collections_IEnumerator_Reset_m1030064427_MetadataUsageId;
extern "C"  void ObservableYieldInstruction_1_System_Collections_IEnumerator_Reset_m1030064427_gshared (ObservableYieldInstruction_1_t674360375 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ObservableYieldInstruction_1_System_Collections_IEnumerator_Reset_m1030064427_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1374155497 * L_0 = (NotSupportedException_t1374155497 *)il2cpp_codegen_object_new(NotSupportedException_t1374155497_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m149930845(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Boolean UniRx.ObservableYieldInstruction`1<System.Int32>::get_HasError()
extern "C"  bool ObservableYieldInstruction_1_get_HasError_m3158714280_gshared (ObservableYieldInstruction_1_t674360375 * __this, const MethodInfo* method)
{
	{
		Exception_t1967233988 * L_0 = (Exception_t1967233988 *)__this->get_error_6();
		return (bool)((((int32_t)((((Il2CppObject*)(Exception_t1967233988 *)L_0) == ((Il2CppObject*)(Il2CppObject *)NULL))? 1 : 0)) == ((int32_t)0))? 1 : 0);
	}
}
// System.Boolean UniRx.ObservableYieldInstruction`1<System.Int32>::get_HasResult()
extern "C"  bool ObservableYieldInstruction_1_get_HasResult_m1699011359_gshared (ObservableYieldInstruction_1_t674360375 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (bool)__this->get_hasResult_5();
		return L_0;
	}
}
// T UniRx.ObservableYieldInstruction`1<System.Int32>::get_Result()
extern "C"  int32_t ObservableYieldInstruction_1_get_Result_m497384538_gshared (ObservableYieldInstruction_1_t674360375 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (int32_t)__this->get_result_3();
		return L_0;
	}
}
// System.Exception UniRx.ObservableYieldInstruction`1<System.Int32>::get_Error()
extern "C"  Exception_t1967233988 * ObservableYieldInstruction_1_get_Error_m2986063095_gshared (ObservableYieldInstruction_1_t674360375 * __this, const MethodInfo* method)
{
	{
		Exception_t1967233988 * L_0 = (Exception_t1967233988 *)__this->get_error_6();
		return L_0;
	}
}
// System.Void UniRx.ObservableYieldInstruction`1<System.Int32>::Dispose()
extern Il2CppClass* IDisposable_t1628921374_il2cpp_TypeInfo_var;
extern const uint32_t ObservableYieldInstruction_1_Dispose_m4212136556_MetadataUsageId;
extern "C"  void ObservableYieldInstruction_1_Dispose_m4212136556_gshared (ObservableYieldInstruction_1_t674360375 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ObservableYieldInstruction_1_Dispose_m4212136556_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_subscription_0();
		NullCheck((Il2CppObject *)L_0);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, (Il2CppObject *)L_0);
		return;
	}
}
// System.Void UniRx.ObservableYieldInstruction`1<System.Int64>::.ctor(UniRx.IObservable`1<T>,System.Boolean)
extern Il2CppClass* Il2CppObject_il2cpp_TypeInfo_var;
extern const uint32_t ObservableYieldInstruction_1__ctor_m2213384432_MetadataUsageId;
extern "C"  void ObservableYieldInstruction_1__ctor_m2213384432_gshared (ObservableYieldInstruction_1_t674360470 * __this, Il2CppObject* ___source0, bool ___reThrowOnError1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ObservableYieldInstruction_1__ctor_m2213384432_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		__this->set_moveNext_4((bool)1);
		bool L_0 = ___reThrowOnError1;
		__this->set_reThrowOnError_1(L_0);
	}

IL_0014:
	try
	{ // begin try (depth: 1)
		Il2CppObject* L_1 = ___source0;
		ToYieldInstruction_t3682831619 * L_2 = (ToYieldInstruction_t3682831619 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		((  void (*) (ToYieldInstruction_t3682831619 *, ObservableYieldInstruction_1_t674360470 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)(L_2, (ObservableYieldInstruction_1_t674360470 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		NullCheck((Il2CppObject*)L_1);
		Il2CppObject * L_3 = InterfaceFuncInvoker1< Il2CppObject *, Il2CppObject* >::Invoke(0 /* System.IDisposable UniRx.IObservable`1<System.Int64>::Subscribe(UniRx.IObserver`1<T>) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), (Il2CppObject*)L_1, (Il2CppObject*)L_2);
		__this->set_subscription_0(L_3);
		goto IL_003a;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1967233988 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Il2CppObject_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_002b;
		throw e;
	}

CATCH_002b:
	{ // begin catch(System.Object)
		{
			__this->set_moveNext_4((bool)0);
			IL2CPP_RAISE_MANAGED_EXCEPTION(__exception_local);
		}

IL_0035:
		{
			goto IL_003a;
		}
	} // end catch (depth: 1)

IL_003a:
	{
		return;
	}
}
// T UniRx.ObservableYieldInstruction`1<System.Int64>::System.Collections.Generic.IEnumerator<T>.get_Current()
extern "C"  int64_t ObservableYieldInstruction_1_System_Collections_Generic_IEnumeratorU3CTU3E_get_Current_m2784390775_gshared (ObservableYieldInstruction_1_t674360470 * __this, const MethodInfo* method)
{
	{
		int64_t L_0 = (int64_t)__this->get_current_2();
		return L_0;
	}
}
// System.Object UniRx.ObservableYieldInstruction`1<System.Int64>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * ObservableYieldInstruction_1_System_Collections_IEnumerator_get_Current_m448923042_gshared (ObservableYieldInstruction_1_t674360470 * __this, const MethodInfo* method)
{
	{
		int64_t L_0 = (int64_t)__this->get_current_2();
		int64_t L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3), &L_1);
		return L_2;
	}
}
// System.Boolean UniRx.ObservableYieldInstruction`1<System.Int64>::System.Collections.IEnumerator.MoveNext()
extern "C"  bool ObservableYieldInstruction_1_System_Collections_IEnumerator_MoveNext_m2633288877_gshared (ObservableYieldInstruction_1_t674360470 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (bool)__this->get_moveNext_4();
		return L_0;
	}
}
// System.Void UniRx.ObservableYieldInstruction`1<System.Int64>::System.Collections.IEnumerator.Reset()
extern Il2CppClass* NotSupportedException_t1374155497_il2cpp_TypeInfo_var;
extern const uint32_t ObservableYieldInstruction_1_System_Collections_IEnumerator_Reset_m4227088300_MetadataUsageId;
extern "C"  void ObservableYieldInstruction_1_System_Collections_IEnumerator_Reset_m4227088300_gshared (ObservableYieldInstruction_1_t674360470 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ObservableYieldInstruction_1_System_Collections_IEnumerator_Reset_m4227088300_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1374155497 * L_0 = (NotSupportedException_t1374155497 *)il2cpp_codegen_object_new(NotSupportedException_t1374155497_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m149930845(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Boolean UniRx.ObservableYieldInstruction`1<System.Int64>::get_HasError()
extern "C"  bool ObservableYieldInstruction_1_get_HasError_m2926121257_gshared (ObservableYieldInstruction_1_t674360470 * __this, const MethodInfo* method)
{
	{
		Exception_t1967233988 * L_0 = (Exception_t1967233988 *)__this->get_error_6();
		return (bool)((((int32_t)((((Il2CppObject*)(Exception_t1967233988 *)L_0) == ((Il2CppObject*)(Il2CppObject *)NULL))? 1 : 0)) == ((int32_t)0))? 1 : 0);
	}
}
// System.Boolean UniRx.ObservableYieldInstruction`1<System.Int64>::get_HasResult()
extern "C"  bool ObservableYieldInstruction_1_get_HasResult_m3078562238_gshared (ObservableYieldInstruction_1_t674360470 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (bool)__this->get_hasResult_5();
		return L_0;
	}
}
// T UniRx.ObservableYieldInstruction`1<System.Int64>::get_Result()
extern "C"  int64_t ObservableYieldInstruction_1_get_Result_m3536245275_gshared (ObservableYieldInstruction_1_t674360470 * __this, const MethodInfo* method)
{
	{
		int64_t L_0 = (int64_t)__this->get_result_3();
		return L_0;
	}
}
// System.Exception UniRx.ObservableYieldInstruction`1<System.Int64>::get_Error()
extern "C"  Exception_t1967233988 * ObservableYieldInstruction_1_get_Error_m174596886_gshared (ObservableYieldInstruction_1_t674360470 * __this, const MethodInfo* method)
{
	{
		Exception_t1967233988 * L_0 = (Exception_t1967233988 *)__this->get_error_6();
		return L_0;
	}
}
// System.Void UniRx.ObservableYieldInstruction`1<System.Int64>::Dispose()
extern Il2CppClass* IDisposable_t1628921374_il2cpp_TypeInfo_var;
extern const uint32_t ObservableYieldInstruction_1_Dispose_m2108654667_MetadataUsageId;
extern "C"  void ObservableYieldInstruction_1_Dispose_m2108654667_gshared (ObservableYieldInstruction_1_t674360470 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ObservableYieldInstruction_1_Dispose_m2108654667_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_subscription_0();
		NullCheck((Il2CppObject *)L_0);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, (Il2CppObject *)L_0);
		return;
	}
}
// System.Void UniRx.ObservableYieldInstruction`1<System.Object>::.ctor(UniRx.IObservable`1<T>,System.Boolean)
extern Il2CppClass* Il2CppObject_il2cpp_TypeInfo_var;
extern const uint32_t ObservableYieldInstruction_1__ctor_m1999879574_MetadataUsageId;
extern "C"  void ObservableYieldInstruction_1__ctor_m1999879574_gshared (ObservableYieldInstruction_1_t2959019304 * __this, Il2CppObject* ___source0, bool ___reThrowOnError1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ObservableYieldInstruction_1__ctor_m1999879574_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		__this->set_moveNext_4((bool)1);
		bool L_0 = ___reThrowOnError1;
		__this->set_reThrowOnError_1(L_0);
	}

IL_0014:
	try
	{ // begin try (depth: 1)
		Il2CppObject* L_1 = ___source0;
		ToYieldInstruction_t1672523157 * L_2 = (ToYieldInstruction_t1672523157 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		((  void (*) (ToYieldInstruction_t1672523157 *, ObservableYieldInstruction_1_t2959019304 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)(L_2, (ObservableYieldInstruction_1_t2959019304 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		NullCheck((Il2CppObject*)L_1);
		Il2CppObject * L_3 = InterfaceFuncInvoker1< Il2CppObject *, Il2CppObject* >::Invoke(0 /* System.IDisposable UniRx.IObservable`1<System.Object>::Subscribe(UniRx.IObserver`1<T>) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), (Il2CppObject*)L_1, (Il2CppObject*)L_2);
		__this->set_subscription_0(L_3);
		goto IL_003a;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1967233988 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Il2CppObject_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_002b;
		throw e;
	}

CATCH_002b:
	{ // begin catch(System.Object)
		{
			__this->set_moveNext_4((bool)0);
			IL2CPP_RAISE_MANAGED_EXCEPTION(__exception_local);
		}

IL_0035:
		{
			goto IL_003a;
		}
	} // end catch (depth: 1)

IL_003a:
	{
		return;
	}
}
// T UniRx.ObservableYieldInstruction`1<System.Object>::System.Collections.Generic.IEnumerator<T>.get_Current()
extern "C"  Il2CppObject * ObservableYieldInstruction_1_System_Collections_Generic_IEnumeratorU3CTU3E_get_Current_m1250220399_gshared (ObservableYieldInstruction_1_t2959019304 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_current_2();
		return L_0;
	}
}
// System.Object UniRx.ObservableYieldInstruction`1<System.Object>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * ObservableYieldInstruction_1_System_Collections_IEnumerator_get_Current_m1496646398_gshared (ObservableYieldInstruction_1_t2959019304 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_current_2();
		return L_0;
	}
}
// System.Boolean UniRx.ObservableYieldInstruction`1<System.Object>::System.Collections.IEnumerator.MoveNext()
extern "C"  bool ObservableYieldInstruction_1_System_Collections_IEnumerator_MoveNext_m539323519_gshared (ObservableYieldInstruction_1_t2959019304 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (bool)__this->get_moveNext_4();
		return L_0;
	}
}
// System.Void UniRx.ObservableYieldInstruction`1<System.Object>::System.Collections.IEnumerator.Reset()
extern Il2CppClass* NotSupportedException_t1374155497_il2cpp_TypeInfo_var;
extern const uint32_t ObservableYieldInstruction_1_System_Collections_IEnumerator_Reset_m3085590738_MetadataUsageId;
extern "C"  void ObservableYieldInstruction_1_System_Collections_IEnumerator_Reset_m3085590738_gshared (ObservableYieldInstruction_1_t2959019304 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ObservableYieldInstruction_1_System_Collections_IEnumerator_Reset_m3085590738_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1374155497 * L_0 = (NotSupportedException_t1374155497 *)il2cpp_codegen_object_new(NotSupportedException_t1374155497_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m149930845(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Boolean UniRx.ObservableYieldInstruction`1<System.Object>::get_HasError()
extern "C"  bool ObservableYieldInstruction_1_get_HasError_m685161623_gshared (ObservableYieldInstruction_1_t2959019304 * __this, const MethodInfo* method)
{
	{
		Exception_t1967233988 * L_0 = (Exception_t1967233988 *)__this->get_error_6();
		return (bool)((((int32_t)((((Il2CppObject*)(Exception_t1967233988 *)L_0) == ((Il2CppObject*)(Il2CppObject *)NULL))? 1 : 0)) == ((int32_t)0))? 1 : 0);
	}
}
// System.Boolean UniRx.ObservableYieldInstruction`1<System.Object>::get_HasResult()
extern "C"  bool ObservableYieldInstruction_1_get_HasResult_m2328290320_gshared (ObservableYieldInstruction_1_t2959019304 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (bool)__this->get_hasResult_5();
		return L_0;
	}
}
// T UniRx.ObservableYieldInstruction`1<System.Object>::get_Result()
extern "C"  Il2CppObject * ObservableYieldInstruction_1_get_Result_m751809059_gshared (ObservableYieldInstruction_1_t2959019304 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_result_3();
		return L_0;
	}
}
// System.Exception UniRx.ObservableYieldInstruction`1<System.Object>::get_Error()
extern "C"  Exception_t1967233988 * ObservableYieldInstruction_1_get_Error_m4242398074_gshared (ObservableYieldInstruction_1_t2959019304 * __this, const MethodInfo* method)
{
	{
		Exception_t1967233988 * L_0 = (Exception_t1967233988 *)__this->get_error_6();
		return L_0;
	}
}
// System.Void UniRx.ObservableYieldInstruction`1<System.Object>::Dispose()
extern Il2CppClass* IDisposable_t1628921374_il2cpp_TypeInfo_var;
extern const uint32_t ObservableYieldInstruction_1_Dispose_m974516581_MetadataUsageId;
extern "C"  void ObservableYieldInstruction_1_Dispose_m974516581_gshared (ObservableYieldInstruction_1_t2959019304 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ObservableYieldInstruction_1_Dispose_m974516581_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_subscription_0();
		NullCheck((Il2CppObject *)L_0);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, (Il2CppObject *)L_0);
		return;
	}
}
// System.Void UniRx.ObserveExtensions/<ObserveEveryValueChanged>c__AnonStorey91`2<System.Object,System.Double>::.ctor()
extern "C"  void U3CObserveEveryValueChangedU3Ec__AnonStorey91_2__ctor_m220767089_gshared (U3CObserveEveryValueChangedU3Ec__AnonStorey91_2_t1160447004 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Collections.IEnumerator UniRx.ObserveExtensions/<ObserveEveryValueChanged>c__AnonStorey91`2<System.Object,System.Double>::<>m__BD(UniRx.IObserver`1<TProperty>,UniRx.CancellationToken)
extern "C"  Il2CppObject * U3CObserveEveryValueChangedU3Ec__AnonStorey91_2_U3CU3Em__BD_m974877057_gshared (U3CObserveEveryValueChangedU3Ec__AnonStorey91_2_t1160447004 * __this, Il2CppObject* ___observer0, CancellationToken_t1439151560  ___cancellationToken1, const MethodInfo* method)
{
	{
		Object_t3878351788 * L_0 = (Object_t3878351788 *)__this->get_unityObject_0();
		Func_2_t1833193546 * L_1 = (Func_2_t1833193546 *)__this->get_propertySelector_1();
		int32_t L_2 = (int32_t)__this->get_frameCountType_2();
		Il2CppObject* L_3 = ___observer0;
		CancellationToken_t1439151560  L_4 = ___cancellationToken1;
		Il2CppObject * L_5 = ((  Il2CppObject * (*) (Il2CppObject * /* static, unused */, Object_t3878351788 *, Func_2_t1833193546 *, int32_t, Il2CppObject*, CancellationToken_t1439151560 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)(NULL /*static, unused*/, (Object_t3878351788 *)L_0, (Func_2_t1833193546 *)L_1, (int32_t)L_2, (Il2CppObject*)L_3, (CancellationToken_t1439151560 )L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		return L_5;
	}
}
// System.Void UniRx.ObserveExtensions/<ObserveEveryValueChanged>c__AnonStorey91`2<System.Object,System.Object>::.ctor()
extern "C"  void U3CObserveEveryValueChangedU3Ec__AnonStorey91_2__ctor_m3111246175_gshared (U3CObserveEveryValueChangedU3Ec__AnonStorey91_2_t1463036810 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Collections.IEnumerator UniRx.ObserveExtensions/<ObserveEveryValueChanged>c__AnonStorey91`2<System.Object,System.Object>::<>m__BD(UniRx.IObserver`1<TProperty>,UniRx.CancellationToken)
extern "C"  Il2CppObject * U3CObserveEveryValueChangedU3Ec__AnonStorey91_2_U3CU3Em__BD_m397551855_gshared (U3CObserveEveryValueChangedU3Ec__AnonStorey91_2_t1463036810 * __this, Il2CppObject* ___observer0, CancellationToken_t1439151560  ___cancellationToken1, const MethodInfo* method)
{
	{
		Object_t3878351788 * L_0 = (Object_t3878351788 *)__this->get_unityObject_0();
		Func_2_t2135783352 * L_1 = (Func_2_t2135783352 *)__this->get_propertySelector_1();
		int32_t L_2 = (int32_t)__this->get_frameCountType_2();
		Il2CppObject* L_3 = ___observer0;
		CancellationToken_t1439151560  L_4 = ___cancellationToken1;
		Il2CppObject * L_5 = ((  Il2CppObject * (*) (Il2CppObject * /* static, unused */, Object_t3878351788 *, Func_2_t2135783352 *, int32_t, Il2CppObject*, CancellationToken_t1439151560 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)(NULL /*static, unused*/, (Object_t3878351788 *)L_0, (Func_2_t2135783352 *)L_1, (int32_t)L_2, (Il2CppObject*)L_3, (CancellationToken_t1439151560 )L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		return L_5;
	}
}
// System.Void UniRx.ObserveExtensions/<ObserveEveryValueChanged>c__AnonStorey92`2<System.Object,System.Double>::.ctor()
extern "C"  void U3CObserveEveryValueChangedU3Ec__AnonStorey92_2__ctor_m2580074098_gshared (U3CObserveEveryValueChangedU3Ec__AnonStorey92_2_t2257977983 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Collections.IEnumerator UniRx.ObserveExtensions/<ObserveEveryValueChanged>c__AnonStorey92`2<System.Object,System.Double>::<>m__BE(UniRx.IObserver`1<TProperty>,UniRx.CancellationToken)
extern "C"  Il2CppObject * U3CObserveEveryValueChangedU3Ec__AnonStorey92_2_U3CU3Em__BE_m2343962371_gshared (U3CObserveEveryValueChangedU3Ec__AnonStorey92_2_t2257977983 * __this, Il2CppObject* ___observer0, CancellationToken_t1439151560  ___cancellationToken1, const MethodInfo* method)
{
	{
		WeakReference_t2193916456 * L_0 = (WeakReference_t2193916456 *)__this->get_reference_0();
		U3CObserveEveryValueChangedU3Ec__AnonStorey91_2_t1160447004 * L_1 = (U3CObserveEveryValueChangedU3Ec__AnonStorey91_2_t1160447004 *)__this->get_U3CU3Ef__refU24145_1();
		NullCheck(L_1);
		Func_2_t1833193546 * L_2 = (Func_2_t1833193546 *)L_1->get_propertySelector_1();
		U3CObserveEveryValueChangedU3Ec__AnonStorey91_2_t1160447004 * L_3 = (U3CObserveEveryValueChangedU3Ec__AnonStorey91_2_t1160447004 *)__this->get_U3CU3Ef__refU24145_1();
		NullCheck(L_3);
		int32_t L_4 = (int32_t)L_3->get_frameCountType_2();
		Il2CppObject* L_5 = ___observer0;
		CancellationToken_t1439151560  L_6 = ___cancellationToken1;
		Il2CppObject * L_7 = ((  Il2CppObject * (*) (Il2CppObject * /* static, unused */, WeakReference_t2193916456 *, Func_2_t1833193546 *, int32_t, Il2CppObject*, CancellationToken_t1439151560 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)(NULL /*static, unused*/, (WeakReference_t2193916456 *)L_0, (Func_2_t1833193546 *)L_2, (int32_t)L_4, (Il2CppObject*)L_5, (CancellationToken_t1439151560 )L_6, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		return L_7;
	}
}
// System.Void UniRx.ObserveExtensions/<ObserveEveryValueChanged>c__AnonStorey92`2<System.Object,System.Object>::.ctor()
extern "C"  void U3CObserveEveryValueChangedU3Ec__AnonStorey92_2__ctor_m1175585888_gshared (U3CObserveEveryValueChangedU3Ec__AnonStorey92_2_t2560567789 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Collections.IEnumerator UniRx.ObserveExtensions/<ObserveEveryValueChanged>c__AnonStorey92`2<System.Object,System.Object>::<>m__BE(UniRx.IObserver`1<TProperty>,UniRx.CancellationToken)
extern "C"  Il2CppObject * U3CObserveEveryValueChangedU3Ec__AnonStorey92_2_U3CU3Em__BE_m1766637169_gshared (U3CObserveEveryValueChangedU3Ec__AnonStorey92_2_t2560567789 * __this, Il2CppObject* ___observer0, CancellationToken_t1439151560  ___cancellationToken1, const MethodInfo* method)
{
	{
		WeakReference_t2193916456 * L_0 = (WeakReference_t2193916456 *)__this->get_reference_0();
		U3CObserveEveryValueChangedU3Ec__AnonStorey91_2_t1463036810 * L_1 = (U3CObserveEveryValueChangedU3Ec__AnonStorey91_2_t1463036810 *)__this->get_U3CU3Ef__refU24145_1();
		NullCheck(L_1);
		Func_2_t2135783352 * L_2 = (Func_2_t2135783352 *)L_1->get_propertySelector_1();
		U3CObserveEveryValueChangedU3Ec__AnonStorey91_2_t1463036810 * L_3 = (U3CObserveEveryValueChangedU3Ec__AnonStorey91_2_t1463036810 *)__this->get_U3CU3Ef__refU24145_1();
		NullCheck(L_3);
		int32_t L_4 = (int32_t)L_3->get_frameCountType_2();
		Il2CppObject* L_5 = ___observer0;
		CancellationToken_t1439151560  L_6 = ___cancellationToken1;
		Il2CppObject * L_7 = ((  Il2CppObject * (*) (Il2CppObject * /* static, unused */, WeakReference_t2193916456 *, Func_2_t2135783352 *, int32_t, Il2CppObject*, CancellationToken_t1439151560 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)(NULL /*static, unused*/, (WeakReference_t2193916456 *)L_0, (Func_2_t2135783352 *)L_2, (int32_t)L_4, (Il2CppObject*)L_5, (CancellationToken_t1439151560 )L_6, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		return L_7;
	}
}
// System.Void UniRx.ObserveExtensions/<PublishPocoValueChanged>c__Iterator24`2<System.Object,System.Double>::.ctor()
extern "C"  void U3CPublishPocoValueChangedU3Ec__Iterator24_2__ctor_m4291736542_gshared (U3CPublishPocoValueChangedU3Ec__Iterator24_2_t2868476001 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Object UniRx.ObserveExtensions/<PublishPocoValueChanged>c__Iterator24`2<System.Object,System.Double>::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CPublishPocoValueChangedU3Ec__Iterator24_2_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m944527348_gshared (U3CPublishPocoValueChangedU3Ec__Iterator24_2_t2868476001 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_U24current_13();
		return L_0;
	}
}
// System.Object UniRx.ObserveExtensions/<PublishPocoValueChanged>c__Iterator24`2<System.Object,System.Double>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CPublishPocoValueChangedU3Ec__Iterator24_2_System_Collections_IEnumerator_get_Current_m3136603016_gshared (U3CPublishPocoValueChangedU3Ec__Iterator24_2_t2868476001 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_U24current_13();
		return L_0;
	}
}
// System.Boolean UniRx.ObserveExtensions/<PublishPocoValueChanged>c__Iterator24`2<System.Object,System.Double>::MoveNext()
extern Il2CppClass* UnityEqualityComparer_t3801418734_il2cpp_TypeInfo_var;
extern Il2CppClass* Double_t534516614_il2cpp_TypeInfo_var;
extern Il2CppClass* Exception_t1967233988_il2cpp_TypeInfo_var;
extern const uint32_t U3CPublishPocoValueChangedU3Ec__Iterator24_2_MoveNext_m786312814_MetadataUsageId;
extern "C"  bool U3CPublishPocoValueChangedU3Ec__Iterator24_2_MoveNext_m786312814_gshared (U3CPublishPocoValueChangedU3Ec__Iterator24_2_t2868476001 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CPublishPocoValueChangedU3Ec__Iterator24_2_MoveNext_m786312814_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	uint32_t V_0 = 0;
	double V_1 = 0.0;
	double V_2 = 0.0;
	Exception_t1967233988 * V_3 = NULL;
	bool V_4 = false;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		int32_t L_0 = (int32_t)__this->get_U24PC_12();
		V_0 = (uint32_t)L_0;
		__this->set_U24PC_12((-1));
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0021;
		}
		if (L_1 == 1)
		{
			goto IL_0146;
		}
	}
	{
		goto IL_015d;
	}

IL_0021:
	{
		IL2CPP_RUNTIME_CLASS_INIT(UnityEqualityComparer_t3801418734_il2cpp_TypeInfo_var);
		Il2CppObject* L_2 = ((  Il2CppObject* (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		__this->set_U3CcomparerU3E__0_0(L_2);
		__this->set_U3CisFirstU3E__1_1((bool)1);
		Initobj (Double_t534516614_il2cpp_TypeInfo_var, (&V_1));
		double L_3 = V_1;
		__this->set_U3CcurrentValueU3E__2_2(L_3);
		Initobj (Double_t534516614_il2cpp_TypeInfo_var, (&V_2));
		double L_4 = V_2;
		__this->set_U3CprevValueU3E__3_3(L_4);
		int32_t L_5 = (int32_t)__this->get_frameCountType_4();
		YieldInstruction_t3557331758 * L_6 = FrameCountTypeExtensions_GetYieldInstruction_m1644022139(NULL /*static, unused*/, (int32_t)L_5, /*hidden argument*/NULL);
		__this->set_U3CyieldInstructionU3E__4_5(L_6);
		goto IL_0146;
	}

IL_0067:
	{
		WeakReference_t2193916456 * L_7 = (WeakReference_t2193916456 *)__this->get_sourceReference_7();
		NullCheck((WeakReference_t2193916456 *)L_7);
		Il2CppObject * L_8 = VirtFuncInvoker0< Il2CppObject * >::Invoke(6 /* System.Object System.WeakReference::get_Target() */, (WeakReference_t2193916456 *)L_7);
		__this->set_U3CtargetU3E__5_8(L_8);
		Il2CppObject * L_9 = (Il2CppObject *)__this->get_U3CtargetU3E__5_8();
		if (!L_9)
		{
			goto IL_00d3;
		}
	}

IL_0083:
	try
	{ // begin try (depth: 1)
		try
		{ // begin try (depth: 2)
			Func_2_t1833193546 * L_10 = (Func_2_t1833193546 *)__this->get_propertySelector_9();
			Il2CppObject * L_11 = (Il2CppObject *)__this->get_U3CtargetU3E__5_8();
			NullCheck((Func_2_t1833193546 *)L_10);
			double L_12 = ((  double (*) (Func_2_t1833193546 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)((Func_2_t1833193546 *)L_10, (Il2CppObject *)((Il2CppObject *)Castclass(L_11, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
			__this->set_U3CcurrentValueU3E__2_2(L_12);
			IL2CPP_LEAVE(0xCE, FINALLY_00c7);
		} // end try (depth: 2)
		catch(Il2CppExceptionWrapper& e)
		{
			__exception_local = (Exception_t1967233988 *)e.ex;
			if(il2cpp_codegen_class_is_assignable_from (Exception_t1967233988_il2cpp_TypeInfo_var, e.ex->object.klass))
				goto CATCH_00a4;
			throw e;
		}

CATCH_00a4:
		{ // begin catch(System.Exception)
			{
				V_3 = (Exception_t1967233988 *)((Exception_t1967233988 *)__exception_local);
				Exception_t1967233988 * L_13 = V_3;
				__this->set_U3CexU3E__6_11(L_13);
				Il2CppObject* L_14 = (Il2CppObject*)__this->get_observer_10();
				Exception_t1967233988 * L_15 = (Exception_t1967233988 *)__this->get_U3CexU3E__6_11();
				NullCheck((Il2CppObject*)L_14);
				InterfaceActionInvoker1< Exception_t1967233988 * >::Invoke(1 /* System.Void UniRx.IObserver`1<System.Double>::OnError(System.Exception) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3), (Il2CppObject*)L_14, (Exception_t1967233988 *)L_15);
				IL2CPP_LEAVE(0x15D, FINALLY_00c7);
			}

IL_00c2:
			{
				; // IL_00c2: leave IL_00ce
			}
		} // end catch (depth: 2)
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_00c7;
	}

FINALLY_00c7:
	{ // begin finally (depth: 1)
		NullCheck((U3CPublishPocoValueChangedU3Ec__Iterator24_2_t2868476001 *)__this);
		((  void (*) (U3CPublishPocoValueChangedU3Ec__Iterator24_2_t2868476001 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)((U3CPublishPocoValueChangedU3Ec__Iterator24_2_t2868476001 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		IL2CPP_END_FINALLY(199)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(199)
	{
		IL2CPP_JUMP_TBL(0xCE, IL_00ce)
		IL2CPP_JUMP_TBL(0x15D, IL_015d)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_00ce:
	{
		goto IL_00e3;
	}

IL_00d3:
	{
		Il2CppObject* L_16 = (Il2CppObject*)__this->get_observer_10();
		NullCheck((Il2CppObject*)L_16);
		InterfaceActionInvoker0::Invoke(0 /* System.Void UniRx.IObserver`1<System.Double>::OnCompleted() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3), (Il2CppObject*)L_16);
		goto IL_015d;
	}

IL_00e3:
	{
		bool L_17 = (bool)__this->get_U3CisFirstU3E__1_1();
		if (L_17)
		{
			goto IL_010a;
		}
	}
	{
		Il2CppObject* L_18 = (Il2CppObject*)__this->get_U3CcomparerU3E__0_0();
		double L_19 = (double)__this->get_U3CcurrentValueU3E__2_2();
		double L_20 = (double)__this->get_U3CprevValueU3E__3_3();
		NullCheck((Il2CppObject*)L_18);
		bool L_21 = InterfaceFuncInvoker2< bool, double, double >::Invoke(0 /* System.Boolean System.Collections.Generic.IEqualityComparer`1<System.Double>::Equals(!0,!0) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5), (Il2CppObject*)L_18, (double)L_19, (double)L_20);
		if (L_21)
		{
			goto IL_012e;
		}
	}

IL_010a:
	{
		__this->set_U3CisFirstU3E__1_1((bool)0);
		Il2CppObject* L_22 = (Il2CppObject*)__this->get_observer_10();
		double L_23 = (double)__this->get_U3CcurrentValueU3E__2_2();
		NullCheck((Il2CppObject*)L_22);
		InterfaceActionInvoker1< double >::Invoke(2 /* System.Void UniRx.IObserver`1<System.Double>::OnNext(T) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3), (Il2CppObject*)L_22, (double)L_23);
		double L_24 = (double)__this->get_U3CcurrentValueU3E__2_2();
		__this->set_U3CprevValueU3E__3_3(L_24);
	}

IL_012e:
	{
		YieldInstruction_t3557331758 * L_25 = (YieldInstruction_t3557331758 *)__this->get_U3CyieldInstructionU3E__4_5();
		__this->set_U24current_13(L_25);
		__this->set_U24PC_12(1);
		goto IL_015f;
	}

IL_0146:
	{
		CancellationToken_t1439151560 * L_26 = (CancellationToken_t1439151560 *)__this->get_address_of_cancellationToken_6();
		bool L_27 = CancellationToken_get_IsCancellationRequested_m1021497867((CancellationToken_t1439151560 *)L_26, /*hidden argument*/NULL);
		if (!L_27)
		{
			goto IL_0067;
		}
	}
	{
		__this->set_U24PC_12((-1));
	}

IL_015d:
	{
		return (bool)0;
	}

IL_015f:
	{
		return (bool)1;
	}
	// Dead block : IL_0161: ldloc.s V_4
}
// System.Void UniRx.ObserveExtensions/<PublishPocoValueChanged>c__Iterator24`2<System.Object,System.Double>::Dispose()
extern "C"  void U3CPublishPocoValueChangedU3Ec__Iterator24_2_Dispose_m1088495771_gshared (U3CPublishPocoValueChangedU3Ec__Iterator24_2_t2868476001 * __this, const MethodInfo* method)
{
	{
		__this->set_U24PC_12((-1));
		return;
	}
}
// System.Void UniRx.ObserveExtensions/<PublishPocoValueChanged>c__Iterator24`2<System.Object,System.Double>::Reset()
extern Il2CppClass* NotSupportedException_t1374155497_il2cpp_TypeInfo_var;
extern const uint32_t U3CPublishPocoValueChangedU3Ec__Iterator24_2_Reset_m1938169483_MetadataUsageId;
extern "C"  void U3CPublishPocoValueChangedU3Ec__Iterator24_2_Reset_m1938169483_gshared (U3CPublishPocoValueChangedU3Ec__Iterator24_2_t2868476001 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CPublishPocoValueChangedU3Ec__Iterator24_2_Reset_m1938169483_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1374155497 * L_0 = (NotSupportedException_t1374155497 *)il2cpp_codegen_object_new(NotSupportedException_t1374155497_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m149930845(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void UniRx.ObserveExtensions/<PublishPocoValueChanged>c__Iterator24`2<System.Object,System.Double>::<>__Finally0()
extern "C"  void U3CPublishPocoValueChangedU3Ec__Iterator24_2_U3CU3E__Finally0_m663584981_gshared (U3CPublishPocoValueChangedU3Ec__Iterator24_2_t2868476001 * __this, const MethodInfo* method)
{
	{
		__this->set_U3CtargetU3E__5_8(NULL);
		return;
	}
}
// System.Void UniRx.ObserveExtensions/<PublishPocoValueChanged>c__Iterator24`2<System.Object,System.Object>::.ctor()
extern "C"  void U3CPublishPocoValueChangedU3Ec__Iterator24_2__ctor_m2887248332_gshared (U3CPublishPocoValueChangedU3Ec__Iterator24_2_t3171065807 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Object UniRx.ObserveExtensions/<PublishPocoValueChanged>c__Iterator24`2<System.Object,System.Object>::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CPublishPocoValueChangedU3Ec__Iterator24_2_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m510261958_gshared (U3CPublishPocoValueChangedU3Ec__Iterator24_2_t3171065807 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_U24current_13();
		return L_0;
	}
}
// System.Object UniRx.ObserveExtensions/<PublishPocoValueChanged>c__Iterator24`2<System.Object,System.Object>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CPublishPocoValueChangedU3Ec__Iterator24_2_System_Collections_IEnumerator_get_Current_m3448130138_gshared (U3CPublishPocoValueChangedU3Ec__Iterator24_2_t3171065807 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_U24current_13();
		return L_0;
	}
}
// System.Boolean UniRx.ObserveExtensions/<PublishPocoValueChanged>c__Iterator24`2<System.Object,System.Object>::MoveNext()
extern Il2CppClass* UnityEqualityComparer_t3801418734_il2cpp_TypeInfo_var;
extern Il2CppClass* Il2CppObject_il2cpp_TypeInfo_var;
extern Il2CppClass* Exception_t1967233988_il2cpp_TypeInfo_var;
extern const uint32_t U3CPublishPocoValueChangedU3Ec__Iterator24_2_MoveNext_m1249446336_MetadataUsageId;
extern "C"  bool U3CPublishPocoValueChangedU3Ec__Iterator24_2_MoveNext_m1249446336_gshared (U3CPublishPocoValueChangedU3Ec__Iterator24_2_t3171065807 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CPublishPocoValueChangedU3Ec__Iterator24_2_MoveNext_m1249446336_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	uint32_t V_0 = 0;
	Il2CppObject * V_1 = NULL;
	Il2CppObject * V_2 = NULL;
	Exception_t1967233988 * V_3 = NULL;
	bool V_4 = false;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		int32_t L_0 = (int32_t)__this->get_U24PC_12();
		V_0 = (uint32_t)L_0;
		__this->set_U24PC_12((-1));
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0021;
		}
		if (L_1 == 1)
		{
			goto IL_0146;
		}
	}
	{
		goto IL_015d;
	}

IL_0021:
	{
		IL2CPP_RUNTIME_CLASS_INIT(UnityEqualityComparer_t3801418734_il2cpp_TypeInfo_var);
		Il2CppObject* L_2 = ((  Il2CppObject* (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		__this->set_U3CcomparerU3E__0_0(L_2);
		__this->set_U3CisFirstU3E__1_1((bool)1);
		Initobj (Il2CppObject_il2cpp_TypeInfo_var, (&V_1));
		Il2CppObject * L_3 = V_1;
		__this->set_U3CcurrentValueU3E__2_2(L_3);
		Initobj (Il2CppObject_il2cpp_TypeInfo_var, (&V_2));
		Il2CppObject * L_4 = V_2;
		__this->set_U3CprevValueU3E__3_3(L_4);
		int32_t L_5 = (int32_t)__this->get_frameCountType_4();
		YieldInstruction_t3557331758 * L_6 = FrameCountTypeExtensions_GetYieldInstruction_m1644022139(NULL /*static, unused*/, (int32_t)L_5, /*hidden argument*/NULL);
		__this->set_U3CyieldInstructionU3E__4_5(L_6);
		goto IL_0146;
	}

IL_0067:
	{
		WeakReference_t2193916456 * L_7 = (WeakReference_t2193916456 *)__this->get_sourceReference_7();
		NullCheck((WeakReference_t2193916456 *)L_7);
		Il2CppObject * L_8 = VirtFuncInvoker0< Il2CppObject * >::Invoke(6 /* System.Object System.WeakReference::get_Target() */, (WeakReference_t2193916456 *)L_7);
		__this->set_U3CtargetU3E__5_8(L_8);
		Il2CppObject * L_9 = (Il2CppObject *)__this->get_U3CtargetU3E__5_8();
		if (!L_9)
		{
			goto IL_00d3;
		}
	}

IL_0083:
	try
	{ // begin try (depth: 1)
		try
		{ // begin try (depth: 2)
			Func_2_t2135783352 * L_10 = (Func_2_t2135783352 *)__this->get_propertySelector_9();
			Il2CppObject * L_11 = (Il2CppObject *)__this->get_U3CtargetU3E__5_8();
			NullCheck((Func_2_t2135783352 *)L_10);
			Il2CppObject * L_12 = ((  Il2CppObject * (*) (Func_2_t2135783352 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)((Func_2_t2135783352 *)L_10, (Il2CppObject *)((Il2CppObject *)Castclass(L_11, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
			__this->set_U3CcurrentValueU3E__2_2(L_12);
			IL2CPP_LEAVE(0xCE, FINALLY_00c7);
		} // end try (depth: 2)
		catch(Il2CppExceptionWrapper& e)
		{
			__exception_local = (Exception_t1967233988 *)e.ex;
			if(il2cpp_codegen_class_is_assignable_from (Exception_t1967233988_il2cpp_TypeInfo_var, e.ex->object.klass))
				goto CATCH_00a4;
			throw e;
		}

CATCH_00a4:
		{ // begin catch(System.Exception)
			{
				V_3 = (Exception_t1967233988 *)((Exception_t1967233988 *)__exception_local);
				Exception_t1967233988 * L_13 = V_3;
				__this->set_U3CexU3E__6_11(L_13);
				Il2CppObject* L_14 = (Il2CppObject*)__this->get_observer_10();
				Exception_t1967233988 * L_15 = (Exception_t1967233988 *)__this->get_U3CexU3E__6_11();
				NullCheck((Il2CppObject*)L_14);
				InterfaceActionInvoker1< Exception_t1967233988 * >::Invoke(1 /* System.Void UniRx.IObserver`1<System.Object>::OnError(System.Exception) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3), (Il2CppObject*)L_14, (Exception_t1967233988 *)L_15);
				IL2CPP_LEAVE(0x15D, FINALLY_00c7);
			}

IL_00c2:
			{
				; // IL_00c2: leave IL_00ce
			}
		} // end catch (depth: 2)
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_00c7;
	}

FINALLY_00c7:
	{ // begin finally (depth: 1)
		NullCheck((U3CPublishPocoValueChangedU3Ec__Iterator24_2_t3171065807 *)__this);
		((  void (*) (U3CPublishPocoValueChangedU3Ec__Iterator24_2_t3171065807 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)((U3CPublishPocoValueChangedU3Ec__Iterator24_2_t3171065807 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		IL2CPP_END_FINALLY(199)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(199)
	{
		IL2CPP_JUMP_TBL(0xCE, IL_00ce)
		IL2CPP_JUMP_TBL(0x15D, IL_015d)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_00ce:
	{
		goto IL_00e3;
	}

IL_00d3:
	{
		Il2CppObject* L_16 = (Il2CppObject*)__this->get_observer_10();
		NullCheck((Il2CppObject*)L_16);
		InterfaceActionInvoker0::Invoke(0 /* System.Void UniRx.IObserver`1<System.Object>::OnCompleted() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3), (Il2CppObject*)L_16);
		goto IL_015d;
	}

IL_00e3:
	{
		bool L_17 = (bool)__this->get_U3CisFirstU3E__1_1();
		if (L_17)
		{
			goto IL_010a;
		}
	}
	{
		Il2CppObject* L_18 = (Il2CppObject*)__this->get_U3CcomparerU3E__0_0();
		Il2CppObject * L_19 = (Il2CppObject *)__this->get_U3CcurrentValueU3E__2_2();
		Il2CppObject * L_20 = (Il2CppObject *)__this->get_U3CprevValueU3E__3_3();
		NullCheck((Il2CppObject*)L_18);
		bool L_21 = InterfaceFuncInvoker2< bool, Il2CppObject *, Il2CppObject * >::Invoke(0 /* System.Boolean System.Collections.Generic.IEqualityComparer`1<System.Object>::Equals(!0,!0) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5), (Il2CppObject*)L_18, (Il2CppObject *)L_19, (Il2CppObject *)L_20);
		if (L_21)
		{
			goto IL_012e;
		}
	}

IL_010a:
	{
		__this->set_U3CisFirstU3E__1_1((bool)0);
		Il2CppObject* L_22 = (Il2CppObject*)__this->get_observer_10();
		Il2CppObject * L_23 = (Il2CppObject *)__this->get_U3CcurrentValueU3E__2_2();
		NullCheck((Il2CppObject*)L_22);
		InterfaceActionInvoker1< Il2CppObject * >::Invoke(2 /* System.Void UniRx.IObserver`1<System.Object>::OnNext(T) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3), (Il2CppObject*)L_22, (Il2CppObject *)L_23);
		Il2CppObject * L_24 = (Il2CppObject *)__this->get_U3CcurrentValueU3E__2_2();
		__this->set_U3CprevValueU3E__3_3(L_24);
	}

IL_012e:
	{
		YieldInstruction_t3557331758 * L_25 = (YieldInstruction_t3557331758 *)__this->get_U3CyieldInstructionU3E__4_5();
		__this->set_U24current_13(L_25);
		__this->set_U24PC_12(1);
		goto IL_015f;
	}

IL_0146:
	{
		CancellationToken_t1439151560 * L_26 = (CancellationToken_t1439151560 *)__this->get_address_of_cancellationToken_6();
		bool L_27 = CancellationToken_get_IsCancellationRequested_m1021497867((CancellationToken_t1439151560 *)L_26, /*hidden argument*/NULL);
		if (!L_27)
		{
			goto IL_0067;
		}
	}
	{
		__this->set_U24PC_12((-1));
	}

IL_015d:
	{
		return (bool)0;
	}

IL_015f:
	{
		return (bool)1;
	}
	// Dead block : IL_0161: ldloc.s V_4
}
// System.Void UniRx.ObserveExtensions/<PublishPocoValueChanged>c__Iterator24`2<System.Object,System.Object>::Dispose()
extern "C"  void U3CPublishPocoValueChangedU3Ec__Iterator24_2_Dispose_m4290024201_gshared (U3CPublishPocoValueChangedU3Ec__Iterator24_2_t3171065807 * __this, const MethodInfo* method)
{
	{
		__this->set_U24PC_12((-1));
		return;
	}
}
// System.Void UniRx.ObserveExtensions/<PublishPocoValueChanged>c__Iterator24`2<System.Object,System.Object>::Reset()
extern Il2CppClass* NotSupportedException_t1374155497_il2cpp_TypeInfo_var;
extern const uint32_t U3CPublishPocoValueChangedU3Ec__Iterator24_2_Reset_m533681273_MetadataUsageId;
extern "C"  void U3CPublishPocoValueChangedU3Ec__Iterator24_2_Reset_m533681273_gshared (U3CPublishPocoValueChangedU3Ec__Iterator24_2_t3171065807 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CPublishPocoValueChangedU3Ec__Iterator24_2_Reset_m533681273_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1374155497 * L_0 = (NotSupportedException_t1374155497 *)il2cpp_codegen_object_new(NotSupportedException_t1374155497_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m149930845(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void UniRx.ObserveExtensions/<PublishPocoValueChanged>c__Iterator24`2<System.Object,System.Object>::<>__Finally0()
extern "C"  void U3CPublishPocoValueChangedU3Ec__Iterator24_2_U3CU3E__Finally0_m4173751079_gshared (U3CPublishPocoValueChangedU3Ec__Iterator24_2_t3171065807 * __this, const MethodInfo* method)
{
	{
		__this->set_U3CtargetU3E__5_8(NULL);
		return;
	}
}
// System.Void UniRx.ObserveExtensions/<PublishUnityObjectValueChanged>c__Iterator25`2<System.Object,System.Double>::.ctor()
extern "C"  void U3CPublishUnityObjectValueChangedU3Ec__Iterator25_2__ctor_m2826616012_gshared (U3CPublishUnityObjectValueChangedU3Ec__Iterator25_2_t42621133 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Object UniRx.ObserveExtensions/<PublishUnityObjectValueChanged>c__Iterator25`2<System.Object,System.Double>::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CPublishUnityObjectValueChangedU3Ec__Iterator25_2_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1065735952_gshared (U3CPublishUnityObjectValueChangedU3Ec__Iterator25_2_t42621133 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_U24current_13();
		return L_0;
	}
}
// System.Object UniRx.ObserveExtensions/<PublishUnityObjectValueChanged>c__Iterator25`2<System.Object,System.Double>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CPublishUnityObjectValueChangedU3Ec__Iterator25_2_System_Collections_IEnumerator_get_Current_m3068656292_gshared (U3CPublishUnityObjectValueChangedU3Ec__Iterator25_2_t42621133 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_U24current_13();
		return L_0;
	}
}
// System.Boolean UniRx.ObserveExtensions/<PublishUnityObjectValueChanged>c__Iterator25`2<System.Object,System.Double>::MoveNext()
extern Il2CppClass* UnityEqualityComparer_t3801418734_il2cpp_TypeInfo_var;
extern Il2CppClass* Double_t534516614_il2cpp_TypeInfo_var;
extern Il2CppClass* Exception_t1967233988_il2cpp_TypeInfo_var;
extern const uint32_t U3CPublishUnityObjectValueChangedU3Ec__Iterator25_2_MoveNext_m832957048_MetadataUsageId;
extern "C"  bool U3CPublishUnityObjectValueChangedU3Ec__Iterator25_2_MoveNext_m832957048_gshared (U3CPublishUnityObjectValueChangedU3Ec__Iterator25_2_t42621133 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CPublishUnityObjectValueChangedU3Ec__Iterator25_2_MoveNext_m832957048_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	uint32_t V_0 = 0;
	double V_1 = 0.0;
	double V_2 = 0.0;
	Exception_t1967233988 * V_3 = NULL;
	bool V_4 = false;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		int32_t L_0 = (int32_t)__this->get_U24PC_12();
		V_0 = (uint32_t)L_0;
		__this->set_U24PC_12((-1));
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0021;
		}
		if (L_1 == 1)
		{
			goto IL_0140;
		}
	}
	{
		goto IL_0157;
	}

IL_0021:
	{
		IL2CPP_RUNTIME_CLASS_INIT(UnityEqualityComparer_t3801418734_il2cpp_TypeInfo_var);
		Il2CppObject* L_2 = ((  Il2CppObject* (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		__this->set_U3CcomparerU3E__0_0(L_2);
		__this->set_U3CisFirstU3E__1_1((bool)1);
		Initobj (Double_t534516614_il2cpp_TypeInfo_var, (&V_1));
		double L_3 = V_1;
		__this->set_U3CcurrentValueU3E__2_2(L_3);
		Initobj (Double_t534516614_il2cpp_TypeInfo_var, (&V_2));
		double L_4 = V_2;
		__this->set_U3CprevValueU3E__3_3(L_4);
		int32_t L_5 = (int32_t)__this->get_frameCountType_4();
		YieldInstruction_t3557331758 * L_6 = FrameCountTypeExtensions_GetYieldInstruction_m1644022139(NULL /*static, unused*/, (int32_t)L_5, /*hidden argument*/NULL);
		__this->set_U3CyieldInstructionU3E__4_5(L_6);
		Object_t3878351788 * L_7 = (Object_t3878351788 *)__this->get_unityObject_6();
		__this->set_U3CsourceU3E__5_7(((Il2CppObject *)Castclass(L_7, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1))));
		goto IL_0140;
	}

IL_0078:
	{
		Object_t3878351788 * L_8 = (Object_t3878351788 *)__this->get_unityObject_6();
		bool L_9 = Object_op_Inequality_m1834679180(NULL /*static, unused*/, (Object_t3878351788 *)L_8, (Object_t3878351788 *)NULL, /*hidden argument*/NULL);
		if (!L_9)
		{
			goto IL_00cd;
		}
	}

IL_0089:
	try
	{ // begin try (depth: 1)
		Func_2_t1833193546 * L_10 = (Func_2_t1833193546 *)__this->get_propertySelector_9();
		Il2CppObject * L_11 = (Il2CppObject *)__this->get_U3CsourceU3E__5_7();
		NullCheck((Func_2_t1833193546 *)L_10);
		double L_12 = ((  double (*) (Func_2_t1833193546 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)((Func_2_t1833193546 *)L_10, (Il2CppObject *)L_11, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		__this->set_U3CcurrentValueU3E__2_2(L_12);
		goto IL_00c8;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1967233988 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Exception_t1967233988_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_00a5;
		throw e;
	}

CATCH_00a5:
	{ // begin catch(System.Exception)
		{
			V_3 = (Exception_t1967233988 *)((Exception_t1967233988 *)__exception_local);
			Exception_t1967233988 * L_13 = V_3;
			__this->set_U3CexU3E__6_11(L_13);
			Il2CppObject* L_14 = (Il2CppObject*)__this->get_observer_10();
			Exception_t1967233988 * L_15 = (Exception_t1967233988 *)__this->get_U3CexU3E__6_11();
			NullCheck((Il2CppObject*)L_14);
			InterfaceActionInvoker1< Exception_t1967233988 * >::Invoke(1 /* System.Void UniRx.IObserver`1<System.Double>::OnError(System.Exception) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3), (Il2CppObject*)L_14, (Exception_t1967233988 *)L_15);
			goto IL_0157;
		}

IL_00c3:
		{
			; // IL_00c3: leave IL_00c8
		}
	} // end catch (depth: 1)

IL_00c8:
	{
		goto IL_00dd;
	}

IL_00cd:
	{
		Il2CppObject* L_16 = (Il2CppObject*)__this->get_observer_10();
		NullCheck((Il2CppObject*)L_16);
		InterfaceActionInvoker0::Invoke(0 /* System.Void UniRx.IObserver`1<System.Double>::OnCompleted() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3), (Il2CppObject*)L_16);
		goto IL_0157;
	}

IL_00dd:
	{
		bool L_17 = (bool)__this->get_U3CisFirstU3E__1_1();
		if (L_17)
		{
			goto IL_0104;
		}
	}
	{
		Il2CppObject* L_18 = (Il2CppObject*)__this->get_U3CcomparerU3E__0_0();
		double L_19 = (double)__this->get_U3CcurrentValueU3E__2_2();
		double L_20 = (double)__this->get_U3CprevValueU3E__3_3();
		NullCheck((Il2CppObject*)L_18);
		bool L_21 = InterfaceFuncInvoker2< bool, double, double >::Invoke(0 /* System.Boolean System.Collections.Generic.IEqualityComparer`1<System.Double>::Equals(!0,!0) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4), (Il2CppObject*)L_18, (double)L_19, (double)L_20);
		if (L_21)
		{
			goto IL_0128;
		}
	}

IL_0104:
	{
		__this->set_U3CisFirstU3E__1_1((bool)0);
		Il2CppObject* L_22 = (Il2CppObject*)__this->get_observer_10();
		double L_23 = (double)__this->get_U3CcurrentValueU3E__2_2();
		NullCheck((Il2CppObject*)L_22);
		InterfaceActionInvoker1< double >::Invoke(2 /* System.Void UniRx.IObserver`1<System.Double>::OnNext(T) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3), (Il2CppObject*)L_22, (double)L_23);
		double L_24 = (double)__this->get_U3CcurrentValueU3E__2_2();
		__this->set_U3CprevValueU3E__3_3(L_24);
	}

IL_0128:
	{
		YieldInstruction_t3557331758 * L_25 = (YieldInstruction_t3557331758 *)__this->get_U3CyieldInstructionU3E__4_5();
		__this->set_U24current_13(L_25);
		__this->set_U24PC_12(1);
		goto IL_0159;
	}

IL_0140:
	{
		CancellationToken_t1439151560 * L_26 = (CancellationToken_t1439151560 *)__this->get_address_of_cancellationToken_8();
		bool L_27 = CancellationToken_get_IsCancellationRequested_m1021497867((CancellationToken_t1439151560 *)L_26, /*hidden argument*/NULL);
		if (!L_27)
		{
			goto IL_0078;
		}
	}
	{
		__this->set_U24PC_12((-1));
	}

IL_0157:
	{
		return (bool)0;
	}

IL_0159:
	{
		return (bool)1;
	}
	// Dead block : IL_015b: ldloc.s V_4
}
// System.Void UniRx.ObserveExtensions/<PublishUnityObjectValueChanged>c__Iterator25`2<System.Object,System.Double>::Dispose()
extern "C"  void U3CPublishUnityObjectValueChangedU3Ec__Iterator25_2_Dispose_m1856939529_gshared (U3CPublishUnityObjectValueChangedU3Ec__Iterator25_2_t42621133 * __this, const MethodInfo* method)
{
	{
		__this->set_U24PC_12((-1));
		return;
	}
}
// System.Void UniRx.ObserveExtensions/<PublishUnityObjectValueChanged>c__Iterator25`2<System.Object,System.Double>::Reset()
extern Il2CppClass* NotSupportedException_t1374155497_il2cpp_TypeInfo_var;
extern const uint32_t U3CPublishUnityObjectValueChangedU3Ec__Iterator25_2_Reset_m473048953_MetadataUsageId;
extern "C"  void U3CPublishUnityObjectValueChangedU3Ec__Iterator25_2_Reset_m473048953_gshared (U3CPublishUnityObjectValueChangedU3Ec__Iterator25_2_t42621133 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CPublishUnityObjectValueChangedU3Ec__Iterator25_2_Reset_m473048953_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1374155497 * L_0 = (NotSupportedException_t1374155497 *)il2cpp_codegen_object_new(NotSupportedException_t1374155497_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m149930845(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void UniRx.ObserveExtensions/<PublishUnityObjectValueChanged>c__Iterator25`2<System.Object,System.Object>::.ctor()
extern "C"  void U3CPublishUnityObjectValueChangedU3Ec__Iterator25_2__ctor_m1422127802_gshared (U3CPublishUnityObjectValueChangedU3Ec__Iterator25_2_t345210939 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Object UniRx.ObserveExtensions/<PublishUnityObjectValueChanged>c__Iterator25`2<System.Object,System.Object>::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CPublishUnityObjectValueChangedU3Ec__Iterator25_2_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m631470562_gshared (U3CPublishUnityObjectValueChangedU3Ec__Iterator25_2_t345210939 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_U24current_13();
		return L_0;
	}
}
// System.Object UniRx.ObserveExtensions/<PublishUnityObjectValueChanged>c__Iterator25`2<System.Object,System.Object>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CPublishUnityObjectValueChangedU3Ec__Iterator25_2_System_Collections_IEnumerator_get_Current_m3380183414_gshared (U3CPublishUnityObjectValueChangedU3Ec__Iterator25_2_t345210939 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_U24current_13();
		return L_0;
	}
}
// System.Boolean UniRx.ObserveExtensions/<PublishUnityObjectValueChanged>c__Iterator25`2<System.Object,System.Object>::MoveNext()
extern Il2CppClass* UnityEqualityComparer_t3801418734_il2cpp_TypeInfo_var;
extern Il2CppClass* Il2CppObject_il2cpp_TypeInfo_var;
extern Il2CppClass* Exception_t1967233988_il2cpp_TypeInfo_var;
extern const uint32_t U3CPublishUnityObjectValueChangedU3Ec__Iterator25_2_MoveNext_m1296090570_MetadataUsageId;
extern "C"  bool U3CPublishUnityObjectValueChangedU3Ec__Iterator25_2_MoveNext_m1296090570_gshared (U3CPublishUnityObjectValueChangedU3Ec__Iterator25_2_t345210939 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CPublishUnityObjectValueChangedU3Ec__Iterator25_2_MoveNext_m1296090570_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	uint32_t V_0 = 0;
	Il2CppObject * V_1 = NULL;
	Il2CppObject * V_2 = NULL;
	Exception_t1967233988 * V_3 = NULL;
	bool V_4 = false;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		int32_t L_0 = (int32_t)__this->get_U24PC_12();
		V_0 = (uint32_t)L_0;
		__this->set_U24PC_12((-1));
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0021;
		}
		if (L_1 == 1)
		{
			goto IL_0140;
		}
	}
	{
		goto IL_0157;
	}

IL_0021:
	{
		IL2CPP_RUNTIME_CLASS_INIT(UnityEqualityComparer_t3801418734_il2cpp_TypeInfo_var);
		Il2CppObject* L_2 = ((  Il2CppObject* (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		__this->set_U3CcomparerU3E__0_0(L_2);
		__this->set_U3CisFirstU3E__1_1((bool)1);
		Initobj (Il2CppObject_il2cpp_TypeInfo_var, (&V_1));
		Il2CppObject * L_3 = V_1;
		__this->set_U3CcurrentValueU3E__2_2(L_3);
		Initobj (Il2CppObject_il2cpp_TypeInfo_var, (&V_2));
		Il2CppObject * L_4 = V_2;
		__this->set_U3CprevValueU3E__3_3(L_4);
		int32_t L_5 = (int32_t)__this->get_frameCountType_4();
		YieldInstruction_t3557331758 * L_6 = FrameCountTypeExtensions_GetYieldInstruction_m1644022139(NULL /*static, unused*/, (int32_t)L_5, /*hidden argument*/NULL);
		__this->set_U3CyieldInstructionU3E__4_5(L_6);
		Object_t3878351788 * L_7 = (Object_t3878351788 *)__this->get_unityObject_6();
		__this->set_U3CsourceU3E__5_7(((Il2CppObject *)Castclass(L_7, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1))));
		goto IL_0140;
	}

IL_0078:
	{
		Object_t3878351788 * L_8 = (Object_t3878351788 *)__this->get_unityObject_6();
		bool L_9 = Object_op_Inequality_m1834679180(NULL /*static, unused*/, (Object_t3878351788 *)L_8, (Object_t3878351788 *)NULL, /*hidden argument*/NULL);
		if (!L_9)
		{
			goto IL_00cd;
		}
	}

IL_0089:
	try
	{ // begin try (depth: 1)
		Func_2_t2135783352 * L_10 = (Func_2_t2135783352 *)__this->get_propertySelector_9();
		Il2CppObject * L_11 = (Il2CppObject *)__this->get_U3CsourceU3E__5_7();
		NullCheck((Func_2_t2135783352 *)L_10);
		Il2CppObject * L_12 = ((  Il2CppObject * (*) (Func_2_t2135783352 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)((Func_2_t2135783352 *)L_10, (Il2CppObject *)L_11, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		__this->set_U3CcurrentValueU3E__2_2(L_12);
		goto IL_00c8;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1967233988 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Exception_t1967233988_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_00a5;
		throw e;
	}

CATCH_00a5:
	{ // begin catch(System.Exception)
		{
			V_3 = (Exception_t1967233988 *)((Exception_t1967233988 *)__exception_local);
			Exception_t1967233988 * L_13 = V_3;
			__this->set_U3CexU3E__6_11(L_13);
			Il2CppObject* L_14 = (Il2CppObject*)__this->get_observer_10();
			Exception_t1967233988 * L_15 = (Exception_t1967233988 *)__this->get_U3CexU3E__6_11();
			NullCheck((Il2CppObject*)L_14);
			InterfaceActionInvoker1< Exception_t1967233988 * >::Invoke(1 /* System.Void UniRx.IObserver`1<System.Object>::OnError(System.Exception) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3), (Il2CppObject*)L_14, (Exception_t1967233988 *)L_15);
			goto IL_0157;
		}

IL_00c3:
		{
			; // IL_00c3: leave IL_00c8
		}
	} // end catch (depth: 1)

IL_00c8:
	{
		goto IL_00dd;
	}

IL_00cd:
	{
		Il2CppObject* L_16 = (Il2CppObject*)__this->get_observer_10();
		NullCheck((Il2CppObject*)L_16);
		InterfaceActionInvoker0::Invoke(0 /* System.Void UniRx.IObserver`1<System.Object>::OnCompleted() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3), (Il2CppObject*)L_16);
		goto IL_0157;
	}

IL_00dd:
	{
		bool L_17 = (bool)__this->get_U3CisFirstU3E__1_1();
		if (L_17)
		{
			goto IL_0104;
		}
	}
	{
		Il2CppObject* L_18 = (Il2CppObject*)__this->get_U3CcomparerU3E__0_0();
		Il2CppObject * L_19 = (Il2CppObject *)__this->get_U3CcurrentValueU3E__2_2();
		Il2CppObject * L_20 = (Il2CppObject *)__this->get_U3CprevValueU3E__3_3();
		NullCheck((Il2CppObject*)L_18);
		bool L_21 = InterfaceFuncInvoker2< bool, Il2CppObject *, Il2CppObject * >::Invoke(0 /* System.Boolean System.Collections.Generic.IEqualityComparer`1<System.Object>::Equals(!0,!0) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4), (Il2CppObject*)L_18, (Il2CppObject *)L_19, (Il2CppObject *)L_20);
		if (L_21)
		{
			goto IL_0128;
		}
	}

IL_0104:
	{
		__this->set_U3CisFirstU3E__1_1((bool)0);
		Il2CppObject* L_22 = (Il2CppObject*)__this->get_observer_10();
		Il2CppObject * L_23 = (Il2CppObject *)__this->get_U3CcurrentValueU3E__2_2();
		NullCheck((Il2CppObject*)L_22);
		InterfaceActionInvoker1< Il2CppObject * >::Invoke(2 /* System.Void UniRx.IObserver`1<System.Object>::OnNext(T) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3), (Il2CppObject*)L_22, (Il2CppObject *)L_23);
		Il2CppObject * L_24 = (Il2CppObject *)__this->get_U3CcurrentValueU3E__2_2();
		__this->set_U3CprevValueU3E__3_3(L_24);
	}

IL_0128:
	{
		YieldInstruction_t3557331758 * L_25 = (YieldInstruction_t3557331758 *)__this->get_U3CyieldInstructionU3E__4_5();
		__this->set_U24current_13(L_25);
		__this->set_U24PC_12(1);
		goto IL_0159;
	}

IL_0140:
	{
		CancellationToken_t1439151560 * L_26 = (CancellationToken_t1439151560 *)__this->get_address_of_cancellationToken_8();
		bool L_27 = CancellationToken_get_IsCancellationRequested_m1021497867((CancellationToken_t1439151560 *)L_26, /*hidden argument*/NULL);
		if (!L_27)
		{
			goto IL_0078;
		}
	}
	{
		__this->set_U24PC_12((-1));
	}

IL_0157:
	{
		return (bool)0;
	}

IL_0159:
	{
		return (bool)1;
	}
	// Dead block : IL_015b: ldloc.s V_4
}
// System.Void UniRx.ObserveExtensions/<PublishUnityObjectValueChanged>c__Iterator25`2<System.Object,System.Object>::Dispose()
extern "C"  void U3CPublishUnityObjectValueChangedU3Ec__Iterator25_2_Dispose_m763500663_gshared (U3CPublishUnityObjectValueChangedU3Ec__Iterator25_2_t345210939 * __this, const MethodInfo* method)
{
	{
		__this->set_U24PC_12((-1));
		return;
	}
}
// System.Void UniRx.ObserveExtensions/<PublishUnityObjectValueChanged>c__Iterator25`2<System.Object,System.Object>::Reset()
extern Il2CppClass* NotSupportedException_t1374155497_il2cpp_TypeInfo_var;
extern const uint32_t U3CPublishUnityObjectValueChangedU3Ec__Iterator25_2_Reset_m3363528039_MetadataUsageId;
extern "C"  void U3CPublishUnityObjectValueChangedU3Ec__Iterator25_2_Reset_m3363528039_gshared (U3CPublishUnityObjectValueChangedU3Ec__Iterator25_2_t345210939 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CPublishUnityObjectValueChangedU3Ec__Iterator25_2_Reset_m3363528039_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1374155497 * L_0 = (NotSupportedException_t1374155497 *)il2cpp_codegen_object_new(NotSupportedException_t1374155497_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m149930845(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void UniRx.Observer/AnonymousObserver`1<System.Object>::.ctor(System.Action`1<T>,System.Action`1<System.Exception>,System.Action)
extern "C"  void AnonymousObserver_1__ctor_m43751705_gshared (AnonymousObserver_1_t4218636717 * __this, Action_1_t985559125 * ___onNext0, Action_1_t2115686693 * ___onError1, Action_t437523947 * ___onCompleted2, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		Action_1_t985559125 * L_0 = ___onNext0;
		__this->set_onNext_0(L_0);
		Action_1_t2115686693 * L_1 = ___onError1;
		__this->set_onError_1(L_1);
		Action_t437523947 * L_2 = ___onCompleted2;
		__this->set_onCompleted_2(L_2);
		return;
	}
}
// System.Void UniRx.Observer/AnonymousObserver`1<System.Object>::OnNext(T)
extern "C"  void AnonymousObserver_1_OnNext_m1868171254_gshared (AnonymousObserver_1_t4218636717 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	{
		int32_t L_0 = (int32_t)__this->get_isStopped_3();
		if (L_0)
		{
			goto IL_0017;
		}
	}
	{
		Action_1_t985559125 * L_1 = (Action_1_t985559125 *)__this->get_onNext_0();
		Il2CppObject * L_2 = ___value0;
		NullCheck((Action_1_t985559125 *)L_1);
		((  void (*) (Action_1_t985559125 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((Action_1_t985559125 *)L_1, (Il2CppObject *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
	}

IL_0017:
	{
		return;
	}
}
// System.Void UniRx.Observer/AnonymousObserver`1<System.Object>::OnError(System.Exception)
extern const MethodInfo* Action_1_Invoke_m1286428014_MethodInfo_var;
extern const uint32_t AnonymousObserver_1_OnError_m2397796101_MetadataUsageId;
extern "C"  void AnonymousObserver_1_OnError_m2397796101_gshared (AnonymousObserver_1_t4218636717 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (AnonymousObserver_1_OnError_m2397796101_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t* L_0 = (int32_t*)__this->get_address_of_isStopped_3();
		int32_t L_1 = Interlocked_Increment_m2829412809(NULL /*static, unused*/, (int32_t*)L_0, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_1) == ((uint32_t)1))))
		{
			goto IL_001d;
		}
	}
	{
		Action_1_t2115686693 * L_2 = (Action_1_t2115686693 *)__this->get_onError_1();
		Exception_t1967233988 * L_3 = ___error0;
		NullCheck((Action_1_t2115686693 *)L_2);
		Action_1_Invoke_m1286428014((Action_1_t2115686693 *)L_2, (Exception_t1967233988 *)L_3, /*hidden argument*/Action_1_Invoke_m1286428014_MethodInfo_var);
	}

IL_001d:
	{
		return;
	}
}
// System.Void UniRx.Observer/AnonymousObserver`1<System.Object>::OnCompleted()
extern "C"  void AnonymousObserver_1_OnCompleted_m4153985496_gshared (AnonymousObserver_1_t4218636717 * __this, const MethodInfo* method)
{
	{
		int32_t* L_0 = (int32_t*)__this->get_address_of_isStopped_3();
		int32_t L_1 = Interlocked_Increment_m2829412809(NULL /*static, unused*/, (int32_t*)L_0, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_1) == ((uint32_t)1))))
		{
			goto IL_001c;
		}
	}
	{
		Action_t437523947 * L_2 = (Action_t437523947 *)__this->get_onCompleted_2();
		NullCheck((Action_t437523947 *)L_2);
		Action_Invoke_m1445970038((Action_t437523947 *)L_2, /*hidden argument*/NULL);
	}

IL_001c:
	{
		return;
	}
}
// System.Void UniRx.Observer/AutoDetachObserver`1<System.Object>::.ctor(UniRx.IObserver`1<T>,System.IDisposable)
extern "C"  void AutoDetachObserver_1__ctor_m2085995981_gshared (AutoDetachObserver_1_t2328747674 * __this, Il2CppObject* ___observer0, Il2CppObject * ___cancel1, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = ___observer0;
		Il2CppObject * L_1 = ___cancel1;
		NullCheck((OperatorObserverBase_2_t1187768149 *)__this);
		((  void (*) (OperatorObserverBase_2_t1187768149 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((OperatorObserverBase_2_t1187768149 *)__this, (Il2CppObject*)L_0, (Il2CppObject *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		return;
	}
}
// System.Void UniRx.Observer/AutoDetachObserver`1<System.Object>::OnNext(T)
extern Il2CppClass* Il2CppObject_il2cpp_TypeInfo_var;
extern const uint32_t AutoDetachObserver_1_OnNext_m224020901_MetadataUsageId;
extern "C"  void AutoDetachObserver_1_OnNext_m224020901_gshared (AutoDetachObserver_1_t2328747674 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (AutoDetachObserver_1_OnNext_m224020901_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		Il2CppObject* L_0 = (Il2CppObject*)((OperatorObserverBase_2_t1187768149 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		Il2CppObject * L_1 = ___value0;
		NullCheck((Il2CppObject*)L_0);
		InterfaceActionInvoker1< Il2CppObject * >::Invoke(2 /* System.Void UniRx.IObserver`1<System.Object>::OnNext(T) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), (Il2CppObject*)L_0, (Il2CppObject *)L_1);
		goto IL_0021;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1967233988 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Il2CppObject_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_0013;
		throw e;
	}

CATCH_0013:
	{ // begin catch(System.Object)
		{
			NullCheck((OperatorObserverBase_2_t1187768149 *)__this);
			VirtActionInvoker0::Invoke(4 /* System.Void UniRx.Operators.OperatorObserverBase`2<System.Object,System.Object>::Dispose() */, (OperatorObserverBase_2_t1187768149 *)__this);
			IL2CPP_RAISE_MANAGED_EXCEPTION(__exception_local);
		}

IL_001c:
		{
			goto IL_0021;
		}
	} // end catch (depth: 1)

IL_0021:
	{
		return;
	}
}
// System.Void UniRx.Observer/AutoDetachObserver`1<System.Object>::OnError(System.Exception)
extern "C"  void AutoDetachObserver_1_OnError_m1919449780_gshared (AutoDetachObserver_1_t2328747674 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method)
{
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		Il2CppObject* L_0 = (Il2CppObject*)((OperatorObserverBase_2_t1187768149 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		Exception_t1967233988 * L_1 = ___error0;
		NullCheck((Il2CppObject*)L_0);
		InterfaceActionInvoker1< Exception_t1967233988 * >::Invoke(1 /* System.Void UniRx.IObserver`1<System.Object>::OnError(System.Exception) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), (Il2CppObject*)L_0, (Exception_t1967233988 *)L_1);
		IL2CPP_LEAVE(0x1A, FINALLY_0013);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0013;
	}

FINALLY_0013:
	{ // begin finally (depth: 1)
		NullCheck((OperatorObserverBase_2_t1187768149 *)__this);
		VirtActionInvoker0::Invoke(4 /* System.Void UniRx.Operators.OperatorObserverBase`2<System.Object,System.Object>::Dispose() */, (OperatorObserverBase_2_t1187768149 *)__this);
		IL2CPP_END_FINALLY(19)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(19)
	{
		IL2CPP_JUMP_TBL(0x1A, IL_001a)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_001a:
	{
		return;
	}
}
// System.Void UniRx.Observer/AutoDetachObserver`1<System.Object>::OnCompleted()
extern "C"  void AutoDetachObserver_1_OnCompleted_m858954759_gshared (AutoDetachObserver_1_t2328747674 * __this, const MethodInfo* method)
{
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		Il2CppObject* L_0 = (Il2CppObject*)((OperatorObserverBase_2_t1187768149 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		NullCheck((Il2CppObject*)L_0);
		InterfaceActionInvoker0::Invoke(0 /* System.Void UniRx.IObserver`1<System.Object>::OnCompleted() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), (Il2CppObject*)L_0);
		IL2CPP_LEAVE(0x19, FINALLY_0012);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0012;
	}

FINALLY_0012:
	{ // begin finally (depth: 1)
		NullCheck((OperatorObserverBase_2_t1187768149 *)__this);
		VirtActionInvoker0::Invoke(4 /* System.Void UniRx.Operators.OperatorObserverBase`2<System.Object,System.Object>::Dispose() */, (OperatorObserverBase_2_t1187768149 *)__this);
		IL2CPP_END_FINALLY(18)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(18)
	{
		IL2CPP_JUMP_TBL(0x19, IL_0019)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0019:
	{
		return;
	}
}
// System.Void UniRx.Observer/EmptyOnNextAnonymousObserver`1<System.Object>::.ctor(System.Action`1<System.Exception>,System.Action)
extern "C"  void EmptyOnNextAnonymousObserver_1__ctor_m2864327496_gshared (EmptyOnNextAnonymousObserver_1_t1584423078 * __this, Action_1_t2115686693 * ___onError0, Action_t437523947 * ___onCompleted1, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		Action_1_t2115686693 * L_0 = ___onError0;
		__this->set_onError_0(L_0);
		Action_t437523947 * L_1 = ___onCompleted1;
		__this->set_onCompleted_1(L_1);
		return;
	}
}
// System.Void UniRx.Observer/EmptyOnNextAnonymousObserver`1<System.Object>::OnNext(T)
extern "C"  void EmptyOnNextAnonymousObserver_1_OnNext_m230038353_gshared (EmptyOnNextAnonymousObserver_1_t1584423078 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void UniRx.Observer/EmptyOnNextAnonymousObserver`1<System.Object>::OnError(System.Exception)
extern const MethodInfo* Action_1_Invoke_m1286428014_MethodInfo_var;
extern const uint32_t EmptyOnNextAnonymousObserver_1_OnError_m1025733728_MetadataUsageId;
extern "C"  void EmptyOnNextAnonymousObserver_1_OnError_m1025733728_gshared (EmptyOnNextAnonymousObserver_1_t1584423078 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (EmptyOnNextAnonymousObserver_1_OnError_m1025733728_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t* L_0 = (int32_t*)__this->get_address_of_isStopped_2();
		int32_t L_1 = Interlocked_Increment_m2829412809(NULL /*static, unused*/, (int32_t*)L_0, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_1) == ((uint32_t)1))))
		{
			goto IL_001d;
		}
	}
	{
		Action_1_t2115686693 * L_2 = (Action_1_t2115686693 *)__this->get_onError_0();
		Exception_t1967233988 * L_3 = ___error0;
		NullCheck((Action_1_t2115686693 *)L_2);
		Action_1_Invoke_m1286428014((Action_1_t2115686693 *)L_2, (Exception_t1967233988 *)L_3, /*hidden argument*/Action_1_Invoke_m1286428014_MethodInfo_var);
	}

IL_001d:
	{
		return;
	}
}
// System.Void UniRx.Observer/EmptyOnNextAnonymousObserver`1<System.Object>::OnCompleted()
extern "C"  void EmptyOnNextAnonymousObserver_1_OnCompleted_m414562227_gshared (EmptyOnNextAnonymousObserver_1_t1584423078 * __this, const MethodInfo* method)
{
	{
		int32_t* L_0 = (int32_t*)__this->get_address_of_isStopped_2();
		int32_t L_1 = Interlocked_Increment_m2829412809(NULL /*static, unused*/, (int32_t*)L_0, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_1) == ((uint32_t)1))))
		{
			goto IL_001c;
		}
	}
	{
		Action_t437523947 * L_2 = (Action_t437523947 *)__this->get_onCompleted_1();
		NullCheck((Action_t437523947 *)L_2);
		Action_Invoke_m1445970038((Action_t437523947 *)L_2, /*hidden argument*/NULL);
	}

IL_001c:
	{
		return;
	}
}
// System.Void UniRx.Observer/Subscribe_`1<System.Boolean>::.ctor(System.Action`1<System.Exception>,System.Action)
extern "C"  void Subscribe__1__ctor_m4185702714_gshared (Subscribe__1_t2955937592 * __this, Action_1_t2115686693 * ___onError0, Action_t437523947 * ___onCompleted1, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		Action_1_t2115686693 * L_0 = ___onError0;
		__this->set_onError_0(L_0);
		Action_t437523947 * L_1 = ___onCompleted1;
		__this->set_onCompleted_1(L_1);
		return;
	}
}
// System.Void UniRx.Observer/Subscribe_`1<System.Boolean>::OnNext(T)
extern "C"  void Subscribe__1_OnNext_m660831263_gshared (Subscribe__1_t2955937592 * __this, bool ___value0, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void UniRx.Observer/Subscribe_`1<System.Boolean>::OnError(System.Exception)
extern const MethodInfo* Action_1_Invoke_m1286428014_MethodInfo_var;
extern const uint32_t Subscribe__1_OnError_m4173429038_MetadataUsageId;
extern "C"  void Subscribe__1_OnError_m4173429038_gshared (Subscribe__1_t2955937592 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Subscribe__1_OnError_m4173429038_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t* L_0 = (int32_t*)__this->get_address_of_isStopped_2();
		int32_t L_1 = Interlocked_Increment_m2829412809(NULL /*static, unused*/, (int32_t*)L_0, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_1) == ((uint32_t)1))))
		{
			goto IL_001d;
		}
	}
	{
		Action_1_t2115686693 * L_2 = (Action_1_t2115686693 *)__this->get_onError_0();
		Exception_t1967233988 * L_3 = ___error0;
		NullCheck((Action_1_t2115686693 *)L_2);
		Action_1_Invoke_m1286428014((Action_1_t2115686693 *)L_2, (Exception_t1967233988 *)L_3, /*hidden argument*/Action_1_Invoke_m1286428014_MethodInfo_var);
	}

IL_001d:
	{
		return;
	}
}
// System.Void UniRx.Observer/Subscribe_`1<System.Boolean>::OnCompleted()
extern "C"  void Subscribe__1_OnCompleted_m3892969857_gshared (Subscribe__1_t2955937592 * __this, const MethodInfo* method)
{
	{
		int32_t* L_0 = (int32_t*)__this->get_address_of_isStopped_2();
		int32_t L_1 = Interlocked_Increment_m2829412809(NULL /*static, unused*/, (int32_t*)L_0, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_1) == ((uint32_t)1))))
		{
			goto IL_001c;
		}
	}
	{
		Action_t437523947 * L_2 = (Action_t437523947 *)__this->get_onCompleted_1();
		NullCheck((Action_t437523947 *)L_2);
		Action_Invoke_m1445970038((Action_t437523947 *)L_2, /*hidden argument*/NULL);
	}

IL_001c:
	{
		return;
	}
}
// System.Void UniRx.Observer/Subscribe_`1<System.Double>::.ctor(System.Action`1<System.Exception>,System.Action)
extern "C"  void Subscribe__1__ctor_m1518811013_gshared (Subscribe__1_t3279448865 * __this, Action_1_t2115686693 * ___onError0, Action_t437523947 * ___onCompleted1, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		Action_1_t2115686693 * L_0 = ___onError0;
		__this->set_onError_0(L_0);
		Action_t437523947 * L_1 = ___onCompleted1;
		__this->set_onCompleted_1(L_1);
		return;
	}
}
// System.Void UniRx.Observer/Subscribe_`1<System.Double>::OnNext(T)
extern "C"  void Subscribe__1_OnNext_m813448180_gshared (Subscribe__1_t3279448865 * __this, double ___value0, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void UniRx.Observer/Subscribe_`1<System.Double>::OnError(System.Exception)
extern const MethodInfo* Action_1_Invoke_m1286428014_MethodInfo_var;
extern const uint32_t Subscribe__1_OnError_m2882662147_MetadataUsageId;
extern "C"  void Subscribe__1_OnError_m2882662147_gshared (Subscribe__1_t3279448865 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Subscribe__1_OnError_m2882662147_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t* L_0 = (int32_t*)__this->get_address_of_isStopped_2();
		int32_t L_1 = Interlocked_Increment_m2829412809(NULL /*static, unused*/, (int32_t*)L_0, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_1) == ((uint32_t)1))))
		{
			goto IL_001d;
		}
	}
	{
		Action_1_t2115686693 * L_2 = (Action_1_t2115686693 *)__this->get_onError_0();
		Exception_t1967233988 * L_3 = ___error0;
		NullCheck((Action_1_t2115686693 *)L_2);
		Action_1_Invoke_m1286428014((Action_1_t2115686693 *)L_2, (Exception_t1967233988 *)L_3, /*hidden argument*/Action_1_Invoke_m1286428014_MethodInfo_var);
	}

IL_001d:
	{
		return;
	}
}
// System.Void UniRx.Observer/Subscribe_`1<System.Double>::OnCompleted()
extern "C"  void Subscribe__1_OnCompleted_m879021782_gshared (Subscribe__1_t3279448865 * __this, const MethodInfo* method)
{
	{
		int32_t* L_0 = (int32_t*)__this->get_address_of_isStopped_2();
		int32_t L_1 = Interlocked_Increment_m2829412809(NULL /*static, unused*/, (int32_t*)L_0, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_1) == ((uint32_t)1))))
		{
			goto IL_001c;
		}
	}
	{
		Action_t437523947 * L_2 = (Action_t437523947 *)__this->get_onCompleted_1();
		NullCheck((Action_t437523947 *)L_2);
		Action_Invoke_m1445970038((Action_t437523947 *)L_2, /*hidden argument*/NULL);
	}

IL_001c:
	{
		return;
	}
}
// System.Void UniRx.Observer/Subscribe_`1<System.Int64>::.ctor(System.Action`1<System.Exception>,System.Action)
extern "C"  void Subscribe__1__ctor_m3387838357_gshared (Subscribe__1_t1297379837 * __this, Action_1_t2115686693 * ___onError0, Action_t437523947 * ___onCompleted1, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		Action_1_t2115686693 * L_0 = ___onError0;
		__this->set_onError_0(L_0);
		Action_t437523947 * L_1 = ___onCompleted1;
		__this->set_onCompleted_1(L_1);
		return;
	}
}
// System.Void UniRx.Observer/Subscribe_`1<System.Int64>::OnNext(T)
extern "C"  void Subscribe__1_OnNext_m3243444708_gshared (Subscribe__1_t1297379837 * __this, int64_t ___value0, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void UniRx.Observer/Subscribe_`1<System.Int64>::OnError(System.Exception)
extern const MethodInfo* Action_1_Invoke_m1286428014_MethodInfo_var;
extern const uint32_t Subscribe__1_OnError_m1991171315_MetadataUsageId;
extern "C"  void Subscribe__1_OnError_m1991171315_gshared (Subscribe__1_t1297379837 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Subscribe__1_OnError_m1991171315_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t* L_0 = (int32_t*)__this->get_address_of_isStopped_2();
		int32_t L_1 = Interlocked_Increment_m2829412809(NULL /*static, unused*/, (int32_t*)L_0, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_1) == ((uint32_t)1))))
		{
			goto IL_001d;
		}
	}
	{
		Action_1_t2115686693 * L_2 = (Action_1_t2115686693 *)__this->get_onError_0();
		Exception_t1967233988 * L_3 = ___error0;
		NullCheck((Action_1_t2115686693 *)L_2);
		Action_1_Invoke_m1286428014((Action_1_t2115686693 *)L_2, (Exception_t1967233988 *)L_3, /*hidden argument*/Action_1_Invoke_m1286428014_MethodInfo_var);
	}

IL_001d:
	{
		return;
	}
}
// System.Void UniRx.Observer/Subscribe_`1<System.Int64>::OnCompleted()
extern "C"  void Subscribe__1_OnCompleted_m3225625798_gshared (Subscribe__1_t1297379837 * __this, const MethodInfo* method)
{
	{
		int32_t* L_0 = (int32_t*)__this->get_address_of_isStopped_2();
		int32_t L_1 = Interlocked_Increment_m2829412809(NULL /*static, unused*/, (int32_t*)L_0, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_1) == ((uint32_t)1))))
		{
			goto IL_001c;
		}
	}
	{
		Action_t437523947 * L_2 = (Action_t437523947 *)__this->get_onCompleted_1();
		NullCheck((Action_t437523947 *)L_2);
		Action_Invoke_m1445970038((Action_t437523947 *)L_2, /*hidden argument*/NULL);
	}

IL_001c:
	{
		return;
	}
}
// System.Void UniRx.Observer/Subscribe_`1<System.Object>::.ctor(System.Action`1<System.Exception>,System.Action)
extern "C"  void Subscribe__1__ctor_m84139991_gshared (Subscribe__1_t3582038671 * __this, Action_1_t2115686693 * ___onError0, Action_t437523947 * ___onCompleted1, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		Action_1_t2115686693 * L_0 = ___onError0;
		__this->set_onError_0(L_0);
		Action_t437523947 * L_1 = ___onCompleted1;
		__this->set_onCompleted_1(L_1);
		return;
	}
}
// System.Void UniRx.Observer/Subscribe_`1<System.Object>::OnNext(T)
extern "C"  void Subscribe__1_OnNext_m4014976610_gshared (Subscribe__1_t3582038671 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void UniRx.Observer/Subscribe_`1<System.Object>::OnError(System.Exception)
extern const MethodInfo* Action_1_Invoke_m1286428014_MethodInfo_var;
extern const uint32_t Subscribe__1_OnError_m2994652529_MetadataUsageId;
extern "C"  void Subscribe__1_OnError_m2994652529_gshared (Subscribe__1_t3582038671 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Subscribe__1_OnError_m2994652529_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t* L_0 = (int32_t*)__this->get_address_of_isStopped_2();
		int32_t L_1 = Interlocked_Increment_m2829412809(NULL /*static, unused*/, (int32_t*)L_0, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_1) == ((uint32_t)1))))
		{
			goto IL_001d;
		}
	}
	{
		Action_1_t2115686693 * L_2 = (Action_1_t2115686693 *)__this->get_onError_0();
		Exception_t1967233988 * L_3 = ___error0;
		NullCheck((Action_1_t2115686693 *)L_2);
		Action_1_Invoke_m1286428014((Action_1_t2115686693 *)L_2, (Exception_t1967233988 *)L_3, /*hidden argument*/Action_1_Invoke_m1286428014_MethodInfo_var);
	}

IL_001d:
	{
		return;
	}
}
// System.Void UniRx.Observer/Subscribe_`1<System.Object>::OnCompleted()
extern "C"  void Subscribe__1_OnCompleted_m2654820932_gshared (Subscribe__1_t3582038671 * __this, const MethodInfo* method)
{
	{
		int32_t* L_0 = (int32_t*)__this->get_address_of_isStopped_2();
		int32_t L_1 = Interlocked_Increment_m2829412809(NULL /*static, unused*/, (int32_t*)L_0, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_1) == ((uint32_t)1))))
		{
			goto IL_001c;
		}
	}
	{
		Action_t437523947 * L_2 = (Action_t437523947 *)__this->get_onCompleted_1();
		NullCheck((Action_t437523947 *)L_2);
		Action_Invoke_m1445970038((Action_t437523947 *)L_2, /*hidden argument*/NULL);
	}

IL_001c:
	{
		return;
	}
}
// System.Void UniRx.Observer/Subscribe_`1<System.Single>::.ctor(System.Action`1<System.Exception>,System.Action)
extern "C"  void Subscribe__1__ctor_m726986478_gshared (Subscribe__1_t3703141272 * __this, Action_1_t2115686693 * ___onError0, Action_t437523947 * ___onCompleted1, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		Action_1_t2115686693 * L_0 = ___onError0;
		__this->set_onError_0(L_0);
		Action_t437523947 * L_1 = ___onCompleted1;
		__this->set_onCompleted_1(L_1);
		return;
	}
}
// System.Void UniRx.Observer/Subscribe_`1<System.Single>::OnNext(T)
extern "C"  void Subscribe__1_OnNext_m1733904875_gshared (Subscribe__1_t3703141272 * __this, float ___value0, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void UniRx.Observer/Subscribe_`1<System.Single>::OnError(System.Exception)
extern const MethodInfo* Action_1_Invoke_m1286428014_MethodInfo_var;
extern const uint32_t Subscribe__1_OnError_m4242157306_MetadataUsageId;
extern "C"  void Subscribe__1_OnError_m4242157306_gshared (Subscribe__1_t3703141272 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Subscribe__1_OnError_m4242157306_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t* L_0 = (int32_t*)__this->get_address_of_isStopped_2();
		int32_t L_1 = Interlocked_Increment_m2829412809(NULL /*static, unused*/, (int32_t*)L_0, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_1) == ((uint32_t)1))))
		{
			goto IL_001d;
		}
	}
	{
		Action_1_t2115686693 * L_2 = (Action_1_t2115686693 *)__this->get_onError_0();
		Exception_t1967233988 * L_3 = ___error0;
		NullCheck((Action_1_t2115686693 *)L_2);
		Action_1_Invoke_m1286428014((Action_1_t2115686693 *)L_2, (Exception_t1967233988 *)L_3, /*hidden argument*/Action_1_Invoke_m1286428014_MethodInfo_var);
	}

IL_001d:
	{
		return;
	}
}
// System.Void UniRx.Observer/Subscribe_`1<System.Single>::OnCompleted()
extern "C"  void Subscribe__1_OnCompleted_m2039220557_gshared (Subscribe__1_t3703141272 * __this, const MethodInfo* method)
{
	{
		int32_t* L_0 = (int32_t*)__this->get_address_of_isStopped_2();
		int32_t L_1 = Interlocked_Increment_m2829412809(NULL /*static, unused*/, (int32_t*)L_0, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_1) == ((uint32_t)1))))
		{
			goto IL_001c;
		}
	}
	{
		Action_t437523947 * L_2 = (Action_t437523947 *)__this->get_onCompleted_1();
		NullCheck((Action_t437523947 *)L_2);
		Action_Invoke_m1445970038((Action_t437523947 *)L_2, /*hidden argument*/NULL);
	}

IL_001c:
	{
		return;
	}
}
// System.Void UniRx.Observer/Subscribe_`1<UniRx.Tuple`2<System.Object,System.Object>>::.ctor(System.Action`1<System.Exception>,System.Action)
extern "C"  void Subscribe__1__ctor_m4154005147_gshared (Subscribe__1_t3114194070 * __this, Action_1_t2115686693 * ___onError0, Action_t437523947 * ___onCompleted1, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		Action_1_t2115686693 * L_0 = ___onError0;
		__this->set_onError_0(L_0);
		Action_t437523947 * L_1 = ___onCompleted1;
		__this->set_onCompleted_1(L_1);
		return;
	}
}
// System.Void UniRx.Observer/Subscribe_`1<UniRx.Tuple`2<System.Object,System.Object>>::OnNext(T)
extern "C"  void Subscribe__1_OnNext_m3798907166_gshared (Subscribe__1_t3114194070 * __this, Tuple_2_t369261819  ___value0, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void UniRx.Observer/Subscribe_`1<UniRx.Tuple`2<System.Object,System.Object>>::OnError(System.Exception)
extern const MethodInfo* Action_1_Invoke_m1286428014_MethodInfo_var;
extern const uint32_t Subscribe__1_OnError_m1728632877_MetadataUsageId;
extern "C"  void Subscribe__1_OnError_m1728632877_gshared (Subscribe__1_t3114194070 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Subscribe__1_OnError_m1728632877_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t* L_0 = (int32_t*)__this->get_address_of_isStopped_2();
		int32_t L_1 = Interlocked_Increment_m2829412809(NULL /*static, unused*/, (int32_t*)L_0, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_1) == ((uint32_t)1))))
		{
			goto IL_001d;
		}
	}
	{
		Action_1_t2115686693 * L_2 = (Action_1_t2115686693 *)__this->get_onError_0();
		Exception_t1967233988 * L_3 = ___error0;
		NullCheck((Action_1_t2115686693 *)L_2);
		Action_1_Invoke_m1286428014((Action_1_t2115686693 *)L_2, (Exception_t1967233988 *)L_3, /*hidden argument*/Action_1_Invoke_m1286428014_MethodInfo_var);
	}

IL_001d:
	{
		return;
	}
}
// System.Void UniRx.Observer/Subscribe_`1<UniRx.Tuple`2<System.Object,System.Object>>::OnCompleted()
extern "C"  void Subscribe__1_OnCompleted_m2166400768_gshared (Subscribe__1_t3114194070 * __this, const MethodInfo* method)
{
	{
		int32_t* L_0 = (int32_t*)__this->get_address_of_isStopped_2();
		int32_t L_1 = Interlocked_Increment_m2829412809(NULL /*static, unused*/, (int32_t*)L_0, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_1) == ((uint32_t)1))))
		{
			goto IL_001c;
		}
	}
	{
		Action_t437523947 * L_2 = (Action_t437523947 *)__this->get_onCompleted_1();
		NullCheck((Action_t437523947 *)L_2);
		Action_Invoke_m1445970038((Action_t437523947 *)L_2, /*hidden argument*/NULL);
	}

IL_001c:
	{
		return;
	}
}
// System.Void UniRx.Observer/Subscribe_`1<UniRx.Unit>::.ctor(System.Action`1<System.Exception>,System.Action)
extern "C"  void Subscribe__1__ctor_m2876855847_gshared (Subscribe__1_t1008250993 * __this, Action_1_t2115686693 * ___onError0, Action_t437523947 * ___onCompleted1, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		Action_1_t2115686693 * L_0 = ___onError0;
		__this->set_onError_0(L_0);
		Action_t437523947 * L_1 = ___onCompleted1;
		__this->set_onCompleted_1(L_1);
		return;
	}
}
// System.Void UniRx.Observer/Subscribe_`1<UniRx.Unit>::OnNext(T)
extern "C"  void Subscribe__1_OnNext_m2285041170_gshared (Subscribe__1_t1008250993 * __this, Unit_t2558286038  ___value0, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void UniRx.Observer/Subscribe_`1<UniRx.Unit>::OnError(System.Exception)
extern const MethodInfo* Action_1_Invoke_m1286428014_MethodInfo_var;
extern const uint32_t Subscribe__1_OnError_m3065818401_MetadataUsageId;
extern "C"  void Subscribe__1_OnError_m3065818401_gshared (Subscribe__1_t1008250993 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Subscribe__1_OnError_m3065818401_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t* L_0 = (int32_t*)__this->get_address_of_isStopped_2();
		int32_t L_1 = Interlocked_Increment_m2829412809(NULL /*static, unused*/, (int32_t*)L_0, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_1) == ((uint32_t)1))))
		{
			goto IL_001d;
		}
	}
	{
		Action_1_t2115686693 * L_2 = (Action_1_t2115686693 *)__this->get_onError_0();
		Exception_t1967233988 * L_3 = ___error0;
		NullCheck((Action_1_t2115686693 *)L_2);
		Action_1_Invoke_m1286428014((Action_1_t2115686693 *)L_2, (Exception_t1967233988 *)L_3, /*hidden argument*/Action_1_Invoke_m1286428014_MethodInfo_var);
	}

IL_001d:
	{
		return;
	}
}
// System.Void UniRx.Observer/Subscribe_`1<UniRx.Unit>::OnCompleted()
extern "C"  void Subscribe__1_OnCompleted_m4292168180_gshared (Subscribe__1_t1008250993 * __this, const MethodInfo* method)
{
	{
		int32_t* L_0 = (int32_t*)__this->get_address_of_isStopped_2();
		int32_t L_1 = Interlocked_Increment_m2829412809(NULL /*static, unused*/, (int32_t*)L_0, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_1) == ((uint32_t)1))))
		{
			goto IL_001c;
		}
	}
	{
		Action_t437523947 * L_2 = (Action_t437523947 *)__this->get_onCompleted_1();
		NullCheck((Action_t437523947 *)L_2);
		Action_Invoke_m1445970038((Action_t437523947 *)L_2, /*hidden argument*/NULL);
	}

IL_001c:
	{
		return;
	}
}
// System.Void UniRx.Observer/Subscribe_`1<UnityEngine.Color>::.ctor(System.Action`1<System.Exception>,System.Action)
extern "C"  void Subscribe__1__ctor_m3979438697_gshared (Subscribe__1_t38140715 * __this, Action_1_t2115686693 * ___onError0, Action_t437523947 * ___onCompleted1, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		Action_1_t2115686693 * L_0 = ___onError0;
		__this->set_onError_0(L_0);
		Action_t437523947 * L_1 = ___onCompleted1;
		__this->set_onCompleted_1(L_1);
		return;
	}
}
// System.Void UniRx.Observer/Subscribe_`1<UnityEngine.Color>::OnNext(T)
extern "C"  void Subscribe__1_OnNext_m3233191568_gshared (Subscribe__1_t38140715 * __this, Color_t1588175760  ___value0, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void UniRx.Observer/Subscribe_`1<UnityEngine.Color>::OnError(System.Exception)
extern const MethodInfo* Action_1_Invoke_m1286428014_MethodInfo_var;
extern const uint32_t Subscribe__1_OnError_m1738248607_MetadataUsageId;
extern "C"  void Subscribe__1_OnError_m1738248607_gshared (Subscribe__1_t38140715 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Subscribe__1_OnError_m1738248607_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t* L_0 = (int32_t*)__this->get_address_of_isStopped_2();
		int32_t L_1 = Interlocked_Increment_m2829412809(NULL /*static, unused*/, (int32_t*)L_0, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_1) == ((uint32_t)1))))
		{
			goto IL_001d;
		}
	}
	{
		Action_1_t2115686693 * L_2 = (Action_1_t2115686693 *)__this->get_onError_0();
		Exception_t1967233988 * L_3 = ___error0;
		NullCheck((Action_1_t2115686693 *)L_2);
		Action_1_Invoke_m1286428014((Action_1_t2115686693 *)L_2, (Exception_t1967233988 *)L_3, /*hidden argument*/Action_1_Invoke_m1286428014_MethodInfo_var);
	}

IL_001d:
	{
		return;
	}
}
// System.Void UniRx.Observer/Subscribe_`1<UnityEngine.Color>::OnCompleted()
extern "C"  void Subscribe__1_OnCompleted_m343440242_gshared (Subscribe__1_t38140715 * __this, const MethodInfo* method)
{
	{
		int32_t* L_0 = (int32_t*)__this->get_address_of_isStopped_2();
		int32_t L_1 = Interlocked_Increment_m2829412809(NULL /*static, unused*/, (int32_t*)L_0, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_1) == ((uint32_t)1))))
		{
			goto IL_001c;
		}
	}
	{
		Action_t437523947 * L_2 = (Action_t437523947 *)__this->get_onCompleted_1();
		NullCheck((Action_t437523947 *)L_2);
		Action_Invoke_m1445970038((Action_t437523947 *)L_2, /*hidden argument*/NULL);
	}

IL_001c:
	{
		return;
	}
}
// System.Void UniRx.Observer/Subscribe`1<System.Boolean>::.ctor(System.Action`1<T>,System.Action`1<System.Exception>,System.Action)
extern "C"  void Subscribe_1__ctor_m1760256423_gshared (Subscribe_1_t3319793125 * __this, Action_1_t359458046 * ___onNext0, Action_1_t2115686693 * ___onError1, Action_t437523947 * ___onCompleted2, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		Action_1_t359458046 * L_0 = ___onNext0;
		__this->set_onNext_0(L_0);
		Action_1_t2115686693 * L_1 = ___onError1;
		__this->set_onError_1(L_1);
		Action_t437523947 * L_2 = ___onCompleted2;
		__this->set_onCompleted_2(L_2);
		return;
	}
}
// System.Void UniRx.Observer/Subscribe`1<System.Boolean>::OnNext(T)
extern "C"  void Subscribe_1_OnNext_m724910212_gshared (Subscribe_1_t3319793125 * __this, bool ___value0, const MethodInfo* method)
{
	{
		int32_t L_0 = (int32_t)__this->get_isStopped_3();
		if (L_0)
		{
			goto IL_0017;
		}
	}
	{
		Action_1_t359458046 * L_1 = (Action_1_t359458046 *)__this->get_onNext_0();
		bool L_2 = ___value0;
		NullCheck((Action_1_t359458046 *)L_1);
		((  void (*) (Action_1_t359458046 *, bool, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((Action_1_t359458046 *)L_1, (bool)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
	}

IL_0017:
	{
		return;
	}
}
// System.Void UniRx.Observer/Subscribe`1<System.Boolean>::OnError(System.Exception)
extern const MethodInfo* Action_1_Invoke_m1286428014_MethodInfo_var;
extern const uint32_t Subscribe_1_OnError_m462410643_MetadataUsageId;
extern "C"  void Subscribe_1_OnError_m462410643_gshared (Subscribe_1_t3319793125 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Subscribe_1_OnError_m462410643_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t* L_0 = (int32_t*)__this->get_address_of_isStopped_3();
		int32_t L_1 = Interlocked_Increment_m2829412809(NULL /*static, unused*/, (int32_t*)L_0, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_1) == ((uint32_t)1))))
		{
			goto IL_001d;
		}
	}
	{
		Action_1_t2115686693 * L_2 = (Action_1_t2115686693 *)__this->get_onError_1();
		Exception_t1967233988 * L_3 = ___error0;
		NullCheck((Action_1_t2115686693 *)L_2);
		Action_1_Invoke_m1286428014((Action_1_t2115686693 *)L_2, (Exception_t1967233988 *)L_3, /*hidden argument*/Action_1_Invoke_m1286428014_MethodInfo_var);
	}

IL_001d:
	{
		return;
	}
}
// System.Void UniRx.Observer/Subscribe`1<System.Boolean>::OnCompleted()
extern "C"  void Subscribe_1_OnCompleted_m1793657702_gshared (Subscribe_1_t3319793125 * __this, const MethodInfo* method)
{
	{
		int32_t* L_0 = (int32_t*)__this->get_address_of_isStopped_3();
		int32_t L_1 = Interlocked_Increment_m2829412809(NULL /*static, unused*/, (int32_t*)L_0, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_1) == ((uint32_t)1))))
		{
			goto IL_001c;
		}
	}
	{
		Action_t437523947 * L_2 = (Action_t437523947 *)__this->get_onCompleted_2();
		NullCheck((Action_t437523947 *)L_2);
		Action_Invoke_m1445970038((Action_t437523947 *)L_2, /*hidden argument*/NULL);
	}

IL_001c:
	{
		return;
	}
}
// System.Void UniRx.Observer/Subscribe`1<System.Double>::.ctor(System.Action`1<T>,System.Action`1<System.Exception>,System.Action)
extern "C"  void Subscribe_1__ctor_m3857391634_gshared (Subscribe_1_t3643304398 * __this, Action_1_t682969319 * ___onNext0, Action_1_t2115686693 * ___onError1, Action_t437523947 * ___onCompleted2, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		Action_1_t682969319 * L_0 = ___onNext0;
		__this->set_onNext_0(L_0);
		Action_1_t2115686693 * L_1 = ___onError1;
		__this->set_onError_1(L_1);
		Action_t437523947 * L_2 = ___onCompleted2;
		__this->set_onCompleted_2(L_2);
		return;
	}
}
// System.Void UniRx.Observer/Subscribe`1<System.Double>::OnNext(T)
extern "C"  void Subscribe_1_OnNext_m954062575_gshared (Subscribe_1_t3643304398 * __this, double ___value0, const MethodInfo* method)
{
	{
		int32_t L_0 = (int32_t)__this->get_isStopped_3();
		if (L_0)
		{
			goto IL_0017;
		}
	}
	{
		Action_1_t682969319 * L_1 = (Action_1_t682969319 *)__this->get_onNext_0();
		double L_2 = ___value0;
		NullCheck((Action_1_t682969319 *)L_1);
		((  void (*) (Action_1_t682969319 *, double, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((Action_1_t682969319 *)L_1, (double)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
	}

IL_0017:
	{
		return;
	}
}
// System.Void UniRx.Observer/Subscribe`1<System.Double>::OnError(System.Exception)
extern const MethodInfo* Action_1_Invoke_m1286428014_MethodInfo_var;
extern const uint32_t Subscribe_1_OnError_m407647230_MetadataUsageId;
extern "C"  void Subscribe_1_OnError_m407647230_gshared (Subscribe_1_t3643304398 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Subscribe_1_OnError_m407647230_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t* L_0 = (int32_t*)__this->get_address_of_isStopped_3();
		int32_t L_1 = Interlocked_Increment_m2829412809(NULL /*static, unused*/, (int32_t*)L_0, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_1) == ((uint32_t)1))))
		{
			goto IL_001d;
		}
	}
	{
		Action_1_t2115686693 * L_2 = (Action_1_t2115686693 *)__this->get_onError_1();
		Exception_t1967233988 * L_3 = ___error0;
		NullCheck((Action_1_t2115686693 *)L_2);
		Action_1_Invoke_m1286428014((Action_1_t2115686693 *)L_2, (Exception_t1967233988 *)L_3, /*hidden argument*/Action_1_Invoke_m1286428014_MethodInfo_var);
	}

IL_001d:
	{
		return;
	}
}
// System.Void UniRx.Observer/Subscribe`1<System.Double>::OnCompleted()
extern "C"  void Subscribe_1_OnCompleted_m2889512017_gshared (Subscribe_1_t3643304398 * __this, const MethodInfo* method)
{
	{
		int32_t* L_0 = (int32_t*)__this->get_address_of_isStopped_3();
		int32_t L_1 = Interlocked_Increment_m2829412809(NULL /*static, unused*/, (int32_t*)L_0, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_1) == ((uint32_t)1))))
		{
			goto IL_001c;
		}
	}
	{
		Action_t437523947 * L_2 = (Action_t437523947 *)__this->get_onCompleted_2();
		NullCheck((Action_t437523947 *)L_2);
		Action_Invoke_m1445970038((Action_t437523947 *)L_2, /*hidden argument*/NULL);
	}

IL_001c:
	{
		return;
	}
}
// System.Void UniRx.Observer/Subscribe`1<System.Int64>::.ctor(System.Action`1<T>,System.Action`1<System.Exception>,System.Action)
extern "C"  void Subscribe_1__ctor_m3372227756_gshared (Subscribe_1_t1661235370 * __this, Action_1_t2995867587 * ___onNext0, Action_1_t2115686693 * ___onError1, Action_t437523947 * ___onCompleted2, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		Action_1_t2995867587 * L_0 = ___onNext0;
		__this->set_onNext_0(L_0);
		Action_1_t2115686693 * L_1 = ___onError1;
		__this->set_onError_1(L_1);
		Action_t437523947 * L_2 = ___onCompleted2;
		__this->set_onCompleted_2(L_2);
		return;
	}
}
// System.Void UniRx.Observer/Subscribe`1<System.Int64>::OnNext(T)
extern "C"  void Subscribe_1_OnNext_m4079264649_gshared (Subscribe_1_t1661235370 * __this, int64_t ___value0, const MethodInfo* method)
{
	{
		int32_t L_0 = (int32_t)__this->get_isStopped_3();
		if (L_0)
		{
			goto IL_0017;
		}
	}
	{
		Action_1_t2995867587 * L_1 = (Action_1_t2995867587 *)__this->get_onNext_0();
		int64_t L_2 = ___value0;
		NullCheck((Action_1_t2995867587 *)L_1);
		((  void (*) (Action_1_t2995867587 *, int64_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((Action_1_t2995867587 *)L_1, (int64_t)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
	}

IL_0017:
	{
		return;
	}
}
// System.Void UniRx.Observer/Subscribe`1<System.Int64>::OnError(System.Exception)
extern const MethodInfo* Action_1_Invoke_m1286428014_MethodInfo_var;
extern const uint32_t Subscribe_1_OnError_m1772784792_MetadataUsageId;
extern "C"  void Subscribe_1_OnError_m1772784792_gshared (Subscribe_1_t1661235370 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Subscribe_1_OnError_m1772784792_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t* L_0 = (int32_t*)__this->get_address_of_isStopped_3();
		int32_t L_1 = Interlocked_Increment_m2829412809(NULL /*static, unused*/, (int32_t*)L_0, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_1) == ((uint32_t)1))))
		{
			goto IL_001d;
		}
	}
	{
		Action_1_t2115686693 * L_2 = (Action_1_t2115686693 *)__this->get_onError_1();
		Exception_t1967233988 * L_3 = ___error0;
		NullCheck((Action_1_t2115686693 *)L_2);
		Action_1_Invoke_m1286428014((Action_1_t2115686693 *)L_2, (Exception_t1967233988 *)L_3, /*hidden argument*/Action_1_Invoke_m1286428014_MethodInfo_var);
	}

IL_001d:
	{
		return;
	}
}
// System.Void UniRx.Observer/Subscribe`1<System.Int64>::OnCompleted()
extern "C"  void Subscribe_1_OnCompleted_m380986347_gshared (Subscribe_1_t1661235370 * __this, const MethodInfo* method)
{
	{
		int32_t* L_0 = (int32_t*)__this->get_address_of_isStopped_3();
		int32_t L_1 = Interlocked_Increment_m2829412809(NULL /*static, unused*/, (int32_t*)L_0, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_1) == ((uint32_t)1))))
		{
			goto IL_001c;
		}
	}
	{
		Action_t437523947 * L_2 = (Action_t437523947 *)__this->get_onCompleted_2();
		NullCheck((Action_t437523947 *)L_2);
		Action_Invoke_m1445970038((Action_t437523947 *)L_2, /*hidden argument*/NULL);
	}

IL_001c:
	{
		return;
	}
}
// System.Void UniRx.Observer/Subscribe`1<System.Object>::.ctor(System.Action`1<T>,System.Action`1<System.Exception>,System.Action)
extern "C"  void Subscribe_1__ctor_m947135104_gshared (Subscribe_1_t3945894204 * __this, Action_1_t985559125 * ___onNext0, Action_1_t2115686693 * ___onError1, Action_t437523947 * ___onCompleted2, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		Action_1_t985559125 * L_0 = ___onNext0;
		__this->set_onNext_0(L_0);
		Action_1_t2115686693 * L_1 = ___onError1;
		__this->set_onError_1(L_1);
		Action_t437523947 * L_2 = ___onCompleted2;
		__this->set_onCompleted_2(L_2);
		return;
	}
}
// System.Void UniRx.Observer/Subscribe`1<System.Object>::OnNext(T)
extern "C"  void Subscribe_1_OnNext_m4155591005_gshared (Subscribe_1_t3945894204 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	{
		int32_t L_0 = (int32_t)__this->get_isStopped_3();
		if (L_0)
		{
			goto IL_0017;
		}
	}
	{
		Action_1_t985559125 * L_1 = (Action_1_t985559125 *)__this->get_onNext_0();
		Il2CppObject * L_2 = ___value0;
		NullCheck((Action_1_t985559125 *)L_1);
		((  void (*) (Action_1_t985559125 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((Action_1_t985559125 *)L_1, (Il2CppObject *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
	}

IL_0017:
	{
		return;
	}
}
// System.Void UniRx.Observer/Subscribe`1<System.Object>::OnError(System.Exception)
extern const MethodInfo* Action_1_Invoke_m1286428014_MethodInfo_var;
extern const uint32_t Subscribe_1_OnError_m519637612_MetadataUsageId;
extern "C"  void Subscribe_1_OnError_m519637612_gshared (Subscribe_1_t3945894204 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Subscribe_1_OnError_m519637612_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t* L_0 = (int32_t*)__this->get_address_of_isStopped_3();
		int32_t L_1 = Interlocked_Increment_m2829412809(NULL /*static, unused*/, (int32_t*)L_0, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_1) == ((uint32_t)1))))
		{
			goto IL_001d;
		}
	}
	{
		Action_1_t2115686693 * L_2 = (Action_1_t2115686693 *)__this->get_onError_1();
		Exception_t1967233988 * L_3 = ___error0;
		NullCheck((Action_1_t2115686693 *)L_2);
		Action_1_Invoke_m1286428014((Action_1_t2115686693 *)L_2, (Exception_t1967233988 *)L_3, /*hidden argument*/Action_1_Invoke_m1286428014_MethodInfo_var);
	}

IL_001d:
	{
		return;
	}
}
// System.Void UniRx.Observer/Subscribe`1<System.Object>::OnCompleted()
extern "C"  void Subscribe_1_OnCompleted_m370343871_gshared (Subscribe_1_t3945894204 * __this, const MethodInfo* method)
{
	{
		int32_t* L_0 = (int32_t*)__this->get_address_of_isStopped_3();
		int32_t L_1 = Interlocked_Increment_m2829412809(NULL /*static, unused*/, (int32_t*)L_0, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_1) == ((uint32_t)1))))
		{
			goto IL_001c;
		}
	}
	{
		Action_t437523947 * L_2 = (Action_t437523947 *)__this->get_onCompleted_2();
		NullCheck((Action_t437523947 *)L_2);
		Action_Invoke_m1445970038((Action_t437523947 *)L_2, /*hidden argument*/NULL);
	}

IL_001c:
	{
		return;
	}
}
// System.Void UniRx.Observer/Subscribe`1<System.Single>::.ctor(System.Action`1<T>,System.Action`1<System.Exception>,System.Action)
extern "C"  void Subscribe_1__ctor_m1906112009_gshared (Subscribe_1_t4066996805 * __this, Action_1_t1106661726 * ___onNext0, Action_1_t2115686693 * ___onError1, Action_t437523947 * ___onCompleted2, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		Action_1_t1106661726 * L_0 = ___onNext0;
		__this->set_onNext_0(L_0);
		Action_1_t2115686693 * L_1 = ___onError1;
		__this->set_onError_1(L_1);
		Action_t437523947 * L_2 = ___onCompleted2;
		__this->set_onCompleted_2(L_2);
		return;
	}
}
// System.Void UniRx.Observer/Subscribe`1<System.Single>::OnNext(T)
extern "C"  void Subscribe_1_OnNext_m1874519270_gshared (Subscribe_1_t4066996805 * __this, float ___value0, const MethodInfo* method)
{
	{
		int32_t L_0 = (int32_t)__this->get_isStopped_3();
		if (L_0)
		{
			goto IL_0017;
		}
	}
	{
		Action_1_t1106661726 * L_1 = (Action_1_t1106661726 *)__this->get_onNext_0();
		float L_2 = ___value0;
		NullCheck((Action_1_t1106661726 *)L_1);
		((  void (*) (Action_1_t1106661726 *, float, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((Action_1_t1106661726 *)L_1, (float)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
	}

IL_0017:
	{
		return;
	}
}
// System.Void UniRx.Observer/Subscribe`1<System.Single>::OnError(System.Exception)
extern const MethodInfo* Action_1_Invoke_m1286428014_MethodInfo_var;
extern const uint32_t Subscribe_1_OnError_m1767142389_MetadataUsageId;
extern "C"  void Subscribe_1_OnError_m1767142389_gshared (Subscribe_1_t4066996805 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Subscribe_1_OnError_m1767142389_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t* L_0 = (int32_t*)__this->get_address_of_isStopped_3();
		int32_t L_1 = Interlocked_Increment_m2829412809(NULL /*static, unused*/, (int32_t*)L_0, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_1) == ((uint32_t)1))))
		{
			goto IL_001d;
		}
	}
	{
		Action_1_t2115686693 * L_2 = (Action_1_t2115686693 *)__this->get_onError_1();
		Exception_t1967233988 * L_3 = ___error0;
		NullCheck((Action_1_t2115686693 *)L_2);
		Action_1_Invoke_m1286428014((Action_1_t2115686693 *)L_2, (Exception_t1967233988 *)L_3, /*hidden argument*/Action_1_Invoke_m1286428014_MethodInfo_var);
	}

IL_001d:
	{
		return;
	}
}
// System.Void UniRx.Observer/Subscribe`1<System.Single>::OnCompleted()
extern "C"  void Subscribe_1_OnCompleted_m4049710792_gshared (Subscribe_1_t4066996805 * __this, const MethodInfo* method)
{
	{
		int32_t* L_0 = (int32_t*)__this->get_address_of_isStopped_3();
		int32_t L_1 = Interlocked_Increment_m2829412809(NULL /*static, unused*/, (int32_t*)L_0, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_1) == ((uint32_t)1))))
		{
			goto IL_001c;
		}
	}
	{
		Action_t437523947 * L_2 = (Action_t437523947 *)__this->get_onCompleted_2();
		NullCheck((Action_t437523947 *)L_2);
		Action_Invoke_m1445970038((Action_t437523947 *)L_2, /*hidden argument*/NULL);
	}

IL_001c:
	{
		return;
	}
}
// System.Void UniRx.Observer/Subscribe`1<UniRx.Tuple`2<System.Object,System.Object>>::.ctor(System.Action`1<T>,System.Action`1<System.Exception>,System.Action)
extern "C"  void Subscribe_1__ctor_m894586150_gshared (Subscribe_1_t3478049603 * __this, Action_1_t517714524 * ___onNext0, Action_1_t2115686693 * ___onError1, Action_t437523947 * ___onCompleted2, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		Action_1_t517714524 * L_0 = ___onNext0;
		__this->set_onNext_0(L_0);
		Action_1_t2115686693 * L_1 = ___onError1;
		__this->set_onError_1(L_1);
		Action_t437523947 * L_2 = ___onCompleted2;
		__this->set_onCompleted_2(L_2);
		return;
	}
}
// System.Void UniRx.Observer/Subscribe`1<UniRx.Tuple`2<System.Object,System.Object>>::OnNext(T)
extern "C"  void Subscribe_1_OnNext_m1782031363_gshared (Subscribe_1_t3478049603 * __this, Tuple_2_t369261819  ___value0, const MethodInfo* method)
{
	{
		int32_t L_0 = (int32_t)__this->get_isStopped_3();
		if (L_0)
		{
			goto IL_0017;
		}
	}
	{
		Action_1_t517714524 * L_1 = (Action_1_t517714524 *)__this->get_onNext_0();
		Tuple_2_t369261819  L_2 = ___value0;
		NullCheck((Action_1_t517714524 *)L_1);
		((  void (*) (Action_1_t517714524 *, Tuple_2_t369261819 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((Action_1_t517714524 *)L_1, (Tuple_2_t369261819 )L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
	}

IL_0017:
	{
		return;
	}
}
// System.Void UniRx.Observer/Subscribe`1<UniRx.Tuple`2<System.Object,System.Object>>::OnError(System.Exception)
extern const MethodInfo* Action_1_Invoke_m1286428014_MethodInfo_var;
extern const uint32_t Subscribe_1_OnError_m3417397522_MetadataUsageId;
extern "C"  void Subscribe_1_OnError_m3417397522_gshared (Subscribe_1_t3478049603 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Subscribe_1_OnError_m3417397522_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t* L_0 = (int32_t*)__this->get_address_of_isStopped_3();
		int32_t L_1 = Interlocked_Increment_m2829412809(NULL /*static, unused*/, (int32_t*)L_0, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_1) == ((uint32_t)1))))
		{
			goto IL_001d;
		}
	}
	{
		Action_1_t2115686693 * L_2 = (Action_1_t2115686693 *)__this->get_onError_1();
		Exception_t1967233988 * L_3 = ___error0;
		NullCheck((Action_1_t2115686693 *)L_2);
		Action_1_Invoke_m1286428014((Action_1_t2115686693 *)L_2, (Exception_t1967233988 *)L_3, /*hidden argument*/Action_1_Invoke_m1286428014_MethodInfo_var);
	}

IL_001d:
	{
		return;
	}
}
// System.Void UniRx.Observer/Subscribe`1<UniRx.Tuple`2<System.Object,System.Object>>::OnCompleted()
extern "C"  void Subscribe_1_OnCompleted_m3539965797_gshared (Subscribe_1_t3478049603 * __this, const MethodInfo* method)
{
	{
		int32_t* L_0 = (int32_t*)__this->get_address_of_isStopped_3();
		int32_t L_1 = Interlocked_Increment_m2829412809(NULL /*static, unused*/, (int32_t*)L_0, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_1) == ((uint32_t)1))))
		{
			goto IL_001c;
		}
	}
	{
		Action_t437523947 * L_2 = (Action_t437523947 *)__this->get_onCompleted_2();
		NullCheck((Action_t437523947 *)L_2);
		Action_Invoke_m1445970038((Action_t437523947 *)L_2, /*hidden argument*/NULL);
	}

IL_001c:
	{
		return;
	}
}
// System.Void UniRx.Observer/Subscribe`1<UniRx.Unit>::.ctor(System.Action`1<T>,System.Action`1<System.Exception>,System.Action)
extern "C"  void Subscribe_1__ctor_m160546330_gshared (Subscribe_1_t1372106526 * __this, Action_1_t2706738743 * ___onNext0, Action_1_t2115686693 * ___onError1, Action_t437523947 * ___onCompleted2, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		Action_1_t2706738743 * L_0 = ___onNext0;
		__this->set_onNext_0(L_0);
		Action_1_t2115686693 * L_1 = ___onError1;
		__this->set_onError_1(L_1);
		Action_t437523947 * L_2 = ___onCompleted2;
		__this->set_onCompleted_2(L_2);
		return;
	}
}
// System.Void UniRx.Observer/Subscribe`1<UniRx.Unit>::OnNext(T)
extern "C"  void Subscribe_1_OnNext_m3027809527_gshared (Subscribe_1_t1372106526 * __this, Unit_t2558286038  ___value0, const MethodInfo* method)
{
	{
		int32_t L_0 = (int32_t)__this->get_isStopped_3();
		if (L_0)
		{
			goto IL_0017;
		}
	}
	{
		Action_1_t2706738743 * L_1 = (Action_1_t2706738743 *)__this->get_onNext_0();
		Unit_t2558286038  L_2 = ___value0;
		NullCheck((Action_1_t2706738743 *)L_1);
		((  void (*) (Action_1_t2706738743 *, Unit_t2558286038 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((Action_1_t2706738743 *)L_1, (Unit_t2558286038 )L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
	}

IL_0017:
	{
		return;
	}
}
// System.Void UniRx.Observer/Subscribe`1<UniRx.Unit>::OnError(System.Exception)
extern const MethodInfo* Action_1_Invoke_m1286428014_MethodInfo_var;
extern const uint32_t Subscribe_1_OnError_m4089053702_MetadataUsageId;
extern "C"  void Subscribe_1_OnError_m4089053702_gshared (Subscribe_1_t1372106526 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Subscribe_1_OnError_m4089053702_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t* L_0 = (int32_t*)__this->get_address_of_isStopped_3();
		int32_t L_1 = Interlocked_Increment_m2829412809(NULL /*static, unused*/, (int32_t*)L_0, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_1) == ((uint32_t)1))))
		{
			goto IL_001d;
		}
	}
	{
		Action_1_t2115686693 * L_2 = (Action_1_t2115686693 *)__this->get_onError_1();
		Exception_t1967233988 * L_3 = ___error0;
		NullCheck((Action_1_t2115686693 *)L_2);
		Action_1_Invoke_m1286428014((Action_1_t2115686693 *)L_2, (Exception_t1967233988 *)L_3, /*hidden argument*/Action_1_Invoke_m1286428014_MethodInfo_var);
	}

IL_001d:
	{
		return;
	}
}
// System.Void UniRx.Observer/Subscribe`1<UniRx.Unit>::OnCompleted()
extern "C"  void Subscribe_1_OnCompleted_m61279833_gshared (Subscribe_1_t1372106526 * __this, const MethodInfo* method)
{
	{
		int32_t* L_0 = (int32_t*)__this->get_address_of_isStopped_3();
		int32_t L_1 = Interlocked_Increment_m2829412809(NULL /*static, unused*/, (int32_t*)L_0, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_1) == ((uint32_t)1))))
		{
			goto IL_001c;
		}
	}
	{
		Action_t437523947 * L_2 = (Action_t437523947 *)__this->get_onCompleted_2();
		NullCheck((Action_t437523947 *)L_2);
		Action_Invoke_m1445970038((Action_t437523947 *)L_2, /*hidden argument*/NULL);
	}

IL_001c:
	{
		return;
	}
}
// System.Void UniRx.Observer/Subscribe`1<UnityEngine.Color>::.ctor(System.Action`1<T>,System.Action`1<System.Exception>,System.Action)
extern "C"  void Subscribe_1__ctor_m2162337070_gshared (Subscribe_1_t401996248 * __this, Action_1_t1736628465 * ___onNext0, Action_1_t2115686693 * ___onError1, Action_t437523947 * ___onCompleted2, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		Action_1_t1736628465 * L_0 = ___onNext0;
		__this->set_onNext_0(L_0);
		Action_1_t2115686693 * L_1 = ___onError1;
		__this->set_onError_1(L_1);
		Action_t437523947 * L_2 = ___onCompleted2;
		__this->set_onCompleted_2(L_2);
		return;
	}
}
// System.Void UniRx.Observer/Subscribe`1<UnityEngine.Color>::OnNext(T)
extern "C"  void Subscribe_1_OnNext_m948714507_gshared (Subscribe_1_t401996248 * __this, Color_t1588175760  ___value0, const MethodInfo* method)
{
	{
		int32_t L_0 = (int32_t)__this->get_isStopped_3();
		if (L_0)
		{
			goto IL_0017;
		}
	}
	{
		Action_1_t1736628465 * L_1 = (Action_1_t1736628465 *)__this->get_onNext_0();
		Color_t1588175760  L_2 = ___value0;
		NullCheck((Action_1_t1736628465 *)L_1);
		((  void (*) (Action_1_t1736628465 *, Color_t1588175760 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((Action_1_t1736628465 *)L_1, (Color_t1588175760 )L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
	}

IL_0017:
	{
		return;
	}
}
// System.Void UniRx.Observer/Subscribe`1<UnityEngine.Color>::OnError(System.Exception)
extern const MethodInfo* Action_1_Invoke_m1286428014_MethodInfo_var;
extern const uint32_t Subscribe_1_OnError_m3542409498_MetadataUsageId;
extern "C"  void Subscribe_1_OnError_m3542409498_gshared (Subscribe_1_t401996248 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Subscribe_1_OnError_m3542409498_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t* L_0 = (int32_t*)__this->get_address_of_isStopped_3();
		int32_t L_1 = Interlocked_Increment_m2829412809(NULL /*static, unused*/, (int32_t*)L_0, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_1) == ((uint32_t)1))))
		{
			goto IL_001d;
		}
	}
	{
		Action_1_t2115686693 * L_2 = (Action_1_t2115686693 *)__this->get_onError_1();
		Exception_t1967233988 * L_3 = ___error0;
		NullCheck((Action_1_t2115686693 *)L_2);
		Action_1_Invoke_m1286428014((Action_1_t2115686693 *)L_2, (Exception_t1967233988 *)L_3, /*hidden argument*/Action_1_Invoke_m1286428014_MethodInfo_var);
	}

IL_001d:
	{
		return;
	}
}
// System.Void UniRx.Observer/Subscribe`1<UnityEngine.Color>::OnCompleted()
extern "C"  void Subscribe_1_OnCompleted_m3048794989_gshared (Subscribe_1_t401996248 * __this, const MethodInfo* method)
{
	{
		int32_t* L_0 = (int32_t*)__this->get_address_of_isStopped_3();
		int32_t L_1 = Interlocked_Increment_m2829412809(NULL /*static, unused*/, (int32_t*)L_0, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_1) == ((uint32_t)1))))
		{
			goto IL_001c;
		}
	}
	{
		Action_t437523947 * L_2 = (Action_t437523947 *)__this->get_onCompleted_2();
		NullCheck((Action_t437523947 *)L_2);
		Action_Invoke_m1445970038((Action_t437523947 *)L_2, /*hidden argument*/NULL);
	}

IL_001c:
	{
		return;
	}
}
// System.Void UniRx.Operators.AggregateObservable`1/Aggregate<System.Object>::.ctor(UniRx.Operators.AggregateObservable`1<TSource>,UniRx.IObserver`1<TSource>,System.IDisposable)
extern "C"  void Aggregate__ctor_m969920345_gshared (Aggregate_t515487936 * __this, AggregateObservable_1_t497085657 * ___parent0, Il2CppObject* ___observer1, Il2CppObject * ___cancel2, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = ___observer1;
		Il2CppObject * L_1 = ___cancel2;
		NullCheck((OperatorObserverBase_2_t1187768149 *)__this);
		((  void (*) (OperatorObserverBase_2_t1187768149 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((OperatorObserverBase_2_t1187768149 *)__this, (Il2CppObject*)L_0, (Il2CppObject *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		AggregateObservable_1_t497085657 * L_2 = ___parent0;
		__this->set_parent_2(L_2);
		__this->set_seenValue_4((bool)0);
		return;
	}
}
// System.Void UniRx.Operators.AggregateObservable`1/Aggregate<System.Object>::OnNext(TSource)
extern Il2CppClass* Exception_t1967233988_il2cpp_TypeInfo_var;
extern const uint32_t Aggregate_OnNext_m475303852_MetadataUsageId;
extern "C"  void Aggregate_OnNext_m475303852_gshared (Aggregate_t515487936 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Aggregate_OnNext_m475303852_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Exception_t1967233988 * V_0 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		bool L_0 = (bool)__this->get_seenValue_4();
		if (L_0)
		{
			goto IL_001e;
		}
	}
	{
		__this->set_seenValue_4((bool)1);
		Il2CppObject * L_1 = ___value0;
		__this->set_accumulation_3(L_1);
		goto IL_0065;
	}

IL_001e:
	try
	{ // begin try (depth: 1)
		AggregateObservable_1_t497085657 * L_2 = (AggregateObservable_1_t497085657 *)__this->get_parent_2();
		NullCheck(L_2);
		Func_3_t1892209229 * L_3 = (Func_3_t1892209229 *)L_2->get_accumulator_2();
		Il2CppObject * L_4 = (Il2CppObject *)__this->get_accumulation_3();
		Il2CppObject * L_5 = ___value0;
		NullCheck((Func_3_t1892209229 *)L_3);
		Il2CppObject * L_6 = ((  Il2CppObject * (*) (Func_3_t1892209229 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)((Func_3_t1892209229 *)L_3, (Il2CppObject *)L_4, (Il2CppObject *)L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		__this->set_accumulation_3(L_6);
		goto IL_0065;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1967233988 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Exception_t1967233988_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_0040;
		throw e;
	}

CATCH_0040:
	{ // begin catch(System.Exception)
		{
			V_0 = (Exception_t1967233988 *)((Exception_t1967233988 *)__exception_local);
		}

IL_0041:
		try
		{ // begin try (depth: 2)
			Il2CppObject* L_7 = (Il2CppObject*)((OperatorObserverBase_2_t1187768149 *)__this)->get_observer_0();
			il2cpp_codegen_memory_barrier();
			Exception_t1967233988 * L_8 = V_0;
			NullCheck((Il2CppObject*)L_7);
			InterfaceActionInvoker1< Exception_t1967233988 * >::Invoke(1 /* System.Void UniRx.IObserver`1<System.Object>::OnError(System.Exception) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3), (Il2CppObject*)L_7, (Exception_t1967233988 *)L_8);
			IL2CPP_LEAVE(0x5B, FINALLY_0054);
		} // end try (depth: 2)
		catch(Il2CppExceptionWrapper& e)
		{
			__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
			goto FINALLY_0054;
		}

FINALLY_0054:
		{ // begin finally (depth: 2)
			NullCheck((OperatorObserverBase_2_t1187768149 *)__this);
			VirtActionInvoker0::Invoke(4 /* System.Void UniRx.Operators.OperatorObserverBase`2<System.Object,System.Object>::Dispose() */, (OperatorObserverBase_2_t1187768149 *)__this);
			IL2CPP_END_FINALLY(84)
		} // end finally (depth: 2)
		IL2CPP_CLEANUP(84)
		{
			IL2CPP_JUMP_TBL(0x5B, IL_005b)
			IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
		}

IL_005b:
		{
			goto IL_0065;
		}

IL_0060:
		{
			; // IL_0060: leave IL_0065
		}
	} // end catch (depth: 1)

IL_0065:
	{
		return;
	}
}
// System.Void UniRx.Operators.AggregateObservable`1/Aggregate<System.Object>::OnError(System.Exception)
extern "C"  void Aggregate_OnError_m2844698646_gshared (Aggregate_t515487936 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method)
{
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		Il2CppObject* L_0 = (Il2CppObject*)((OperatorObserverBase_2_t1187768149 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		Exception_t1967233988 * L_1 = ___error0;
		NullCheck((Il2CppObject*)L_0);
		InterfaceActionInvoker1< Exception_t1967233988 * >::Invoke(1 /* System.Void UniRx.IObserver`1<System.Object>::OnError(System.Exception) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3), (Il2CppObject*)L_0, (Exception_t1967233988 *)L_1);
		IL2CPP_LEAVE(0x1A, FINALLY_0013);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0013;
	}

FINALLY_0013:
	{ // begin finally (depth: 1)
		NullCheck((OperatorObserverBase_2_t1187768149 *)__this);
		VirtActionInvoker0::Invoke(4 /* System.Void UniRx.Operators.OperatorObserverBase`2<System.Object,System.Object>::Dispose() */, (OperatorObserverBase_2_t1187768149 *)__this);
		IL2CPP_END_FINALLY(19)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(19)
	{
		IL2CPP_JUMP_TBL(0x1A, IL_001a)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_001a:
	{
		return;
	}
}
// System.Void UniRx.Operators.AggregateObservable`1/Aggregate<System.Object>::OnCompleted()
extern Il2CppClass* InvalidOperationException_t2420574324_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral613824474;
extern const uint32_t Aggregate_OnCompleted_m3879226473_MetadataUsageId;
extern "C"  void Aggregate_OnCompleted_m3879226473_gshared (Aggregate_t515487936 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Aggregate_OnCompleted_m3879226473_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		bool L_0 = (bool)__this->get_seenValue_4();
		if (L_0)
		{
			goto IL_0016;
		}
	}
	{
		InvalidOperationException_t2420574324 * L_1 = (InvalidOperationException_t2420574324 *)il2cpp_codegen_object_new(InvalidOperationException_t2420574324_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_1, (String_t*)_stringLiteral613824474, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0016:
	{
		Il2CppObject* L_2 = (Il2CppObject*)((OperatorObserverBase_2_t1187768149 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		Il2CppObject * L_3 = (Il2CppObject *)__this->get_accumulation_3();
		NullCheck((Il2CppObject*)L_2);
		InterfaceActionInvoker1< Il2CppObject * >::Invoke(2 /* System.Void UniRx.IObserver`1<System.Object>::OnNext(T) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3), (Il2CppObject*)L_2, (Il2CppObject *)L_3);
	}

IL_0029:
	try
	{ // begin try (depth: 1)
		Il2CppObject* L_4 = (Il2CppObject*)((OperatorObserverBase_2_t1187768149 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		NullCheck((Il2CppObject*)L_4);
		InterfaceActionInvoker0::Invoke(0 /* System.Void UniRx.IObserver`1<System.Object>::OnCompleted() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3), (Il2CppObject*)L_4);
		IL2CPP_LEAVE(0x42, FINALLY_003b);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_003b;
	}

FINALLY_003b:
	{ // begin finally (depth: 1)
		NullCheck((OperatorObserverBase_2_t1187768149 *)__this);
		VirtActionInvoker0::Invoke(4 /* System.Void UniRx.Operators.OperatorObserverBase`2<System.Object,System.Object>::Dispose() */, (OperatorObserverBase_2_t1187768149 *)__this);
		IL2CPP_END_FINALLY(59)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(59)
	{
		IL2CPP_JUMP_TBL(0x42, IL_0042)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0042:
	{
		return;
	}
}
// System.Void UniRx.Operators.AggregateObservable`1<System.Object>::.ctor(UniRx.IObservable`1<TSource>,System.Func`3<TSource,TSource,TSource>)
extern "C"  void AggregateObservable_1__ctor_m2888832910_gshared (AggregateObservable_1_t497085657 * __this, Il2CppObject* ___source0, Func_3_t1892209229 * ___accumulator1, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = ___source0;
		bool L_1 = ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)(NULL /*static, unused*/, (Il2CppObject*)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		NullCheck((OperatorObservableBase_1_t4196218687 *)__this);
		((  void (*) (OperatorObservableBase_1_t4196218687 *, bool, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)((OperatorObservableBase_1_t4196218687 *)__this, (bool)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		Il2CppObject* L_2 = ___source0;
		__this->set_source_1(L_2);
		Func_3_t1892209229 * L_3 = ___accumulator1;
		__this->set_accumulator_2(L_3);
		return;
	}
}
// System.IDisposable UniRx.Operators.AggregateObservable`1<System.Object>::SubscribeCore(UniRx.IObserver`1<TSource>,System.IDisposable)
extern "C"  Il2CppObject * AggregateObservable_1_SubscribeCore_m3472909844_gshared (AggregateObservable_1_t497085657 * __this, Il2CppObject* ___observer0, Il2CppObject * ___cancel1, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = (Il2CppObject*)__this->get_source_1();
		Il2CppObject* L_1 = ___observer0;
		Il2CppObject * L_2 = ___cancel1;
		Aggregate_t515487936 * L_3 = (Aggregate_t515487936 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		((  void (*) (Aggregate_t515487936 *, AggregateObservable_1_t497085657 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)(L_3, (AggregateObservable_1_t497085657 *)__this, (Il2CppObject*)L_1, (Il2CppObject *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		NullCheck((Il2CppObject*)L_0);
		Il2CppObject * L_4 = InterfaceFuncInvoker1< Il2CppObject *, Il2CppObject* >::Invoke(0 /* System.IDisposable UniRx.IObservable`1<System.Object>::Subscribe(UniRx.IObserver`1<T>) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5), (Il2CppObject*)L_0, (Il2CppObject*)L_3);
		return L_4;
	}
}
// System.Void UniRx.Operators.AggregateObservable`2/Aggregate<System.Object,System.Object>::.ctor(UniRx.Operators.AggregateObservable`2<TSource,TAccumulate>,UniRx.IObserver`1<TAccumulate>,System.IDisposable)
extern "C"  void Aggregate__ctor_m1480151888_gshared (Aggregate_t1831847732 * __this, AggregateObservable_2_t1747477946 * ___parent0, Il2CppObject* ___observer1, Il2CppObject * ___cancel2, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = ___observer1;
		Il2CppObject * L_1 = ___cancel2;
		NullCheck((OperatorObserverBase_2_t1187768149 *)__this);
		((  void (*) (OperatorObserverBase_2_t1187768149 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((OperatorObserverBase_2_t1187768149 *)__this, (Il2CppObject*)L_0, (Il2CppObject *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		AggregateObservable_2_t1747477946 * L_2 = ___parent0;
		__this->set_parent_2(L_2);
		AggregateObservable_2_t1747477946 * L_3 = ___parent0;
		NullCheck(L_3);
		Il2CppObject * L_4 = (Il2CppObject *)L_3->get_seed_2();
		__this->set_accumulation_3(L_4);
		return;
	}
}
// System.Void UniRx.Operators.AggregateObservable`2/Aggregate<System.Object,System.Object>::OnNext(TSource)
extern Il2CppClass* Exception_t1967233988_il2cpp_TypeInfo_var;
extern const uint32_t Aggregate_OnNext_m1410486751_MetadataUsageId;
extern "C"  void Aggregate_OnNext_m1410486751_gshared (Aggregate_t1831847732 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Aggregate_OnNext_m1410486751_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Exception_t1967233988 * V_0 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		AggregateObservable_2_t1747477946 * L_0 = (AggregateObservable_2_t1747477946 *)__this->get_parent_2();
		NullCheck(L_0);
		Func_3_t1892209229 * L_1 = (Func_3_t1892209229 *)L_0->get_accumulator_3();
		Il2CppObject * L_2 = (Il2CppObject *)__this->get_accumulation_3();
		Il2CppObject * L_3 = ___value0;
		NullCheck((Func_3_t1892209229 *)L_1);
		Il2CppObject * L_4 = ((  Il2CppObject * (*) (Func_3_t1892209229 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)((Func_3_t1892209229 *)L_1, (Il2CppObject *)L_2, (Il2CppObject *)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		__this->set_accumulation_3(L_4);
		goto IL_0047;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1967233988 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Exception_t1967233988_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_0022;
		throw e;
	}

CATCH_0022:
	{ // begin catch(System.Exception)
		{
			V_0 = (Exception_t1967233988 *)((Exception_t1967233988 *)__exception_local);
		}

IL_0023:
		try
		{ // begin try (depth: 2)
			Il2CppObject* L_5 = (Il2CppObject*)((OperatorObserverBase_2_t1187768149 *)__this)->get_observer_0();
			il2cpp_codegen_memory_barrier();
			Exception_t1967233988 * L_6 = V_0;
			NullCheck((Il2CppObject*)L_5);
			InterfaceActionInvoker1< Exception_t1967233988 * >::Invoke(1 /* System.Void UniRx.IObserver`1<System.Object>::OnError(System.Exception) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3), (Il2CppObject*)L_5, (Exception_t1967233988 *)L_6);
			IL2CPP_LEAVE(0x3D, FINALLY_0036);
		} // end try (depth: 2)
		catch(Il2CppExceptionWrapper& e)
		{
			__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
			goto FINALLY_0036;
		}

FINALLY_0036:
		{ // begin finally (depth: 2)
			NullCheck((OperatorObserverBase_2_t1187768149 *)__this);
			VirtActionInvoker0::Invoke(4 /* System.Void UniRx.Operators.OperatorObserverBase`2<System.Object,System.Object>::Dispose() */, (OperatorObserverBase_2_t1187768149 *)__this);
			IL2CPP_END_FINALLY(54)
		} // end finally (depth: 2)
		IL2CPP_CLEANUP(54)
		{
			IL2CPP_JUMP_TBL(0x3D, IL_003d)
			IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
		}

IL_003d:
		{
			goto IL_0047;
		}

IL_0042:
		{
			; // IL_0042: leave IL_0047
		}
	} // end catch (depth: 1)

IL_0047:
	{
		return;
	}
}
// System.Void UniRx.Operators.AggregateObservable`2/Aggregate<System.Object,System.Object>::OnError(System.Exception)
extern "C"  void Aggregate_OnError_m303138441_gshared (Aggregate_t1831847732 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method)
{
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		Il2CppObject* L_0 = (Il2CppObject*)((OperatorObserverBase_2_t1187768149 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		Exception_t1967233988 * L_1 = ___error0;
		NullCheck((Il2CppObject*)L_0);
		InterfaceActionInvoker1< Exception_t1967233988 * >::Invoke(1 /* System.Void UniRx.IObserver`1<System.Object>::OnError(System.Exception) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3), (Il2CppObject*)L_0, (Exception_t1967233988 *)L_1);
		IL2CPP_LEAVE(0x1A, FINALLY_0013);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0013;
	}

FINALLY_0013:
	{ // begin finally (depth: 1)
		NullCheck((OperatorObserverBase_2_t1187768149 *)__this);
		VirtActionInvoker0::Invoke(4 /* System.Void UniRx.Operators.OperatorObserverBase`2<System.Object,System.Object>::Dispose() */, (OperatorObserverBase_2_t1187768149 *)__this);
		IL2CPP_END_FINALLY(19)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(19)
	{
		IL2CPP_JUMP_TBL(0x1A, IL_001a)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_001a:
	{
		return;
	}
}
// System.Void UniRx.Operators.AggregateObservable`2/Aggregate<System.Object,System.Object>::OnCompleted()
extern "C"  void Aggregate_OnCompleted_m3419864924_gshared (Aggregate_t1831847732 * __this, const MethodInfo* method)
{
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Il2CppObject* L_0 = (Il2CppObject*)((OperatorObserverBase_2_t1187768149 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		Il2CppObject * L_1 = (Il2CppObject *)__this->get_accumulation_3();
		NullCheck((Il2CppObject*)L_0);
		InterfaceActionInvoker1< Il2CppObject * >::Invoke(2 /* System.Void UniRx.IObserver`1<System.Object>::OnNext(T) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3), (Il2CppObject*)L_0, (Il2CppObject *)L_1);
	}

IL_0013:
	try
	{ // begin try (depth: 1)
		Il2CppObject* L_2 = (Il2CppObject*)((OperatorObserverBase_2_t1187768149 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		NullCheck((Il2CppObject*)L_2);
		InterfaceActionInvoker0::Invoke(0 /* System.Void UniRx.IObserver`1<System.Object>::OnCompleted() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3), (Il2CppObject*)L_2);
		IL2CPP_LEAVE(0x2C, FINALLY_0025);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0025;
	}

FINALLY_0025:
	{ // begin finally (depth: 1)
		NullCheck((OperatorObserverBase_2_t1187768149 *)__this);
		VirtActionInvoker0::Invoke(4 /* System.Void UniRx.Operators.OperatorObserverBase`2<System.Object,System.Object>::Dispose() */, (OperatorObserverBase_2_t1187768149 *)__this);
		IL2CPP_END_FINALLY(37)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(37)
	{
		IL2CPP_JUMP_TBL(0x2C, IL_002c)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_002c:
	{
		return;
	}
}
// System.Void UniRx.Operators.AggregateObservable`2<System.Object,System.Object>::.ctor(UniRx.IObservable`1<TSource>,TAccumulate,System.Func`3<TAccumulate,TSource,TAccumulate>)
extern "C"  void AggregateObservable_2__ctor_m1690294643_gshared (AggregateObservable_2_t1747477946 * __this, Il2CppObject* ___source0, Il2CppObject * ___seed1, Func_3_t1892209229 * ___accumulator2, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = ___source0;
		bool L_1 = ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)(NULL /*static, unused*/, (Il2CppObject*)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		NullCheck((OperatorObservableBase_1_t4196218687 *)__this);
		((  void (*) (OperatorObservableBase_1_t4196218687 *, bool, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)((OperatorObservableBase_1_t4196218687 *)__this, (bool)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		Il2CppObject* L_2 = ___source0;
		__this->set_source_1(L_2);
		Il2CppObject * L_3 = ___seed1;
		__this->set_seed_2(L_3);
		Func_3_t1892209229 * L_4 = ___accumulator2;
		__this->set_accumulator_3(L_4);
		return;
	}
}
// System.IDisposable UniRx.Operators.AggregateObservable`2<System.Object,System.Object>::SubscribeCore(UniRx.IObserver`1<TAccumulate>,System.IDisposable)
extern "C"  Il2CppObject * AggregateObservable_2_SubscribeCore_m3556924890_gshared (AggregateObservable_2_t1747477946 * __this, Il2CppObject* ___observer0, Il2CppObject * ___cancel1, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = (Il2CppObject*)__this->get_source_1();
		Il2CppObject* L_1 = ___observer0;
		Il2CppObject * L_2 = ___cancel1;
		Aggregate_t1831847732 * L_3 = (Aggregate_t1831847732 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		((  void (*) (Aggregate_t1831847732 *, AggregateObservable_2_t1747477946 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)(L_3, (AggregateObservable_2_t1747477946 *)__this, (Il2CppObject*)L_1, (Il2CppObject *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		NullCheck((Il2CppObject*)L_0);
		Il2CppObject * L_4 = InterfaceFuncInvoker1< Il2CppObject *, Il2CppObject* >::Invoke(0 /* System.IDisposable UniRx.IObservable`1<System.Object>::Subscribe(UniRx.IObserver`1<T>) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5), (Il2CppObject*)L_0, (Il2CppObject*)L_3);
		return L_4;
	}
}
// System.Void UniRx.Operators.AggregateObservable`3/Aggregate<System.Object,System.Object,System.Object>::.ctor(UniRx.Operators.AggregateObservable`3<TSource,TAccumulate,TResult>,UniRx.IObserver`1<TResult>,System.IDisposable)
extern "C"  void Aggregate__ctor_m876770702_gshared (Aggregate_t245075056 * __this, AggregateObservable_3_t3567497731 * ___parent0, Il2CppObject* ___observer1, Il2CppObject * ___cancel2, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = ___observer1;
		Il2CppObject * L_1 = ___cancel2;
		NullCheck((OperatorObserverBase_2_t1187768149 *)__this);
		((  void (*) (OperatorObserverBase_2_t1187768149 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((OperatorObserverBase_2_t1187768149 *)__this, (Il2CppObject*)L_0, (Il2CppObject *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		AggregateObservable_3_t3567497731 * L_2 = ___parent0;
		__this->set_parent_2(L_2);
		AggregateObservable_3_t3567497731 * L_3 = ___parent0;
		NullCheck(L_3);
		Il2CppObject * L_4 = (Il2CppObject *)L_3->get_seed_2();
		__this->set_accumulation_3(L_4);
		return;
	}
}
// System.Void UniRx.Operators.AggregateObservable`3/Aggregate<System.Object,System.Object,System.Object>::OnNext(TSource)
extern Il2CppClass* Exception_t1967233988_il2cpp_TypeInfo_var;
extern const uint32_t Aggregate_OnNext_m2816633106_MetadataUsageId;
extern "C"  void Aggregate_OnNext_m2816633106_gshared (Aggregate_t245075056 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Aggregate_OnNext_m2816633106_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Exception_t1967233988 * V_0 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		AggregateObservable_3_t3567497731 * L_0 = (AggregateObservable_3_t3567497731 *)__this->get_parent_2();
		NullCheck(L_0);
		Func_3_t1892209229 * L_1 = (Func_3_t1892209229 *)L_0->get_accumulator_3();
		Il2CppObject * L_2 = (Il2CppObject *)__this->get_accumulation_3();
		Il2CppObject * L_3 = ___value0;
		NullCheck((Func_3_t1892209229 *)L_1);
		Il2CppObject * L_4 = ((  Il2CppObject * (*) (Func_3_t1892209229 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)((Func_3_t1892209229 *)L_1, (Il2CppObject *)L_2, (Il2CppObject *)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		__this->set_accumulation_3(L_4);
		goto IL_0047;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1967233988 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Exception_t1967233988_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_0022;
		throw e;
	}

CATCH_0022:
	{ // begin catch(System.Exception)
		{
			V_0 = (Exception_t1967233988 *)((Exception_t1967233988 *)__exception_local);
		}

IL_0023:
		try
		{ // begin try (depth: 2)
			Il2CppObject* L_5 = (Il2CppObject*)((OperatorObserverBase_2_t1187768149 *)__this)->get_observer_0();
			il2cpp_codegen_memory_barrier();
			Exception_t1967233988 * L_6 = V_0;
			NullCheck((Il2CppObject*)L_5);
			InterfaceActionInvoker1< Exception_t1967233988 * >::Invoke(1 /* System.Void UniRx.IObserver`1<System.Object>::OnError(System.Exception) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3), (Il2CppObject*)L_5, (Exception_t1967233988 *)L_6);
			IL2CPP_LEAVE(0x3D, FINALLY_0036);
		} // end try (depth: 2)
		catch(Il2CppExceptionWrapper& e)
		{
			__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
			goto FINALLY_0036;
		}

FINALLY_0036:
		{ // begin finally (depth: 2)
			NullCheck((OperatorObserverBase_2_t1187768149 *)__this);
			VirtActionInvoker0::Invoke(4 /* System.Void UniRx.Operators.OperatorObserverBase`2<System.Object,System.Object>::Dispose() */, (OperatorObserverBase_2_t1187768149 *)__this);
			IL2CPP_END_FINALLY(54)
		} // end finally (depth: 2)
		IL2CPP_CLEANUP(54)
		{
			IL2CPP_JUMP_TBL(0x3D, IL_003d)
			IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
		}

IL_003d:
		{
			goto IL_0047;
		}

IL_0042:
		{
			; // IL_0042: leave IL_0047
		}
	} // end catch (depth: 1)

IL_0047:
	{
		return;
	}
}
// System.Void UniRx.Operators.AggregateObservable`3/Aggregate<System.Object,System.Object,System.Object>::OnError(System.Exception)
extern "C"  void Aggregate_OnError_m394295804_gshared (Aggregate_t245075056 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method)
{
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		Il2CppObject* L_0 = (Il2CppObject*)((OperatorObserverBase_2_t1187768149 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		Exception_t1967233988 * L_1 = ___error0;
		NullCheck((Il2CppObject*)L_0);
		InterfaceActionInvoker1< Exception_t1967233988 * >::Invoke(1 /* System.Void UniRx.IObserver`1<System.Object>::OnError(System.Exception) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3), (Il2CppObject*)L_0, (Exception_t1967233988 *)L_1);
		IL2CPP_LEAVE(0x1A, FINALLY_0013);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0013;
	}

FINALLY_0013:
	{ // begin finally (depth: 1)
		NullCheck((OperatorObserverBase_2_t1187768149 *)__this);
		VirtActionInvoker0::Invoke(4 /* System.Void UniRx.Operators.OperatorObserverBase`2<System.Object,System.Object>::Dispose() */, (OperatorObserverBase_2_t1187768149 *)__this);
		IL2CPP_END_FINALLY(19)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(19)
	{
		IL2CPP_JUMP_TBL(0x1A, IL_001a)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_001a:
	{
		return;
	}
}
// System.Void UniRx.Operators.AggregateObservable`3/Aggregate<System.Object,System.Object,System.Object>::OnCompleted()
extern Il2CppClass* Exception_t1967233988_il2cpp_TypeInfo_var;
extern const uint32_t Aggregate_OnCompleted_m3877193551_MetadataUsageId;
extern "C"  void Aggregate_OnCompleted_m3877193551_gshared (Aggregate_t245075056 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Aggregate_OnCompleted_m3877193551_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Il2CppObject * V_0 = NULL;
	Exception_t1967233988 * V_1 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		AggregateObservable_3_t3567497731 * L_0 = (AggregateObservable_3_t3567497731 *)__this->get_parent_2();
		NullCheck(L_0);
		Func_2_t2135783352 * L_1 = (Func_2_t2135783352 *)L_0->get_resultSelector_4();
		Il2CppObject * L_2 = (Il2CppObject *)__this->get_accumulation_3();
		NullCheck((Func_2_t2135783352 *)L_1);
		Il2CppObject * L_3 = ((  Il2CppObject * (*) (Func_2_t2135783352 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->method)((Func_2_t2135783352 *)L_1, (Il2CppObject *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		V_0 = (Il2CppObject *)L_3;
		goto IL_002e;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1967233988 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Exception_t1967233988_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_001c;
		throw e;
	}

CATCH_001c:
	{ // begin catch(System.Exception)
		{
			V_1 = (Exception_t1967233988 *)((Exception_t1967233988 *)__exception_local);
			Exception_t1967233988 * L_4 = V_1;
			NullCheck((Aggregate_t245075056 *)__this);
			VirtActionInvoker1< Exception_t1967233988 * >::Invoke(9 /* System.Void UniRx.Operators.AggregateObservable`3/Aggregate<System.Object,System.Object,System.Object>::OnError(System.Exception) */, (Aggregate_t245075056 *)__this, (Exception_t1967233988 *)L_4);
			goto IL_0055;
		}

IL_0029:
		{
			; // IL_0029: leave IL_002e
		}
	} // end catch (depth: 1)

IL_002e:
	{
		Il2CppObject* L_5 = (Il2CppObject*)((OperatorObserverBase_2_t1187768149 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		Il2CppObject * L_6 = V_0;
		NullCheck((Il2CppObject*)L_5);
		InterfaceActionInvoker1< Il2CppObject * >::Invoke(2 /* System.Void UniRx.IObserver`1<System.Object>::OnNext(T) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3), (Il2CppObject*)L_5, (Il2CppObject *)L_6);
	}

IL_003c:
	try
	{ // begin try (depth: 1)
		Il2CppObject* L_7 = (Il2CppObject*)((OperatorObserverBase_2_t1187768149 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		NullCheck((Il2CppObject*)L_7);
		InterfaceActionInvoker0::Invoke(0 /* System.Void UniRx.IObserver`1<System.Object>::OnCompleted() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3), (Il2CppObject*)L_7);
		IL2CPP_LEAVE(0x55, FINALLY_004e);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_004e;
	}

FINALLY_004e:
	{ // begin finally (depth: 1)
		NullCheck((OperatorObserverBase_2_t1187768149 *)__this);
		VirtActionInvoker0::Invoke(4 /* System.Void UniRx.Operators.OperatorObserverBase`2<System.Object,System.Object>::Dispose() */, (OperatorObserverBase_2_t1187768149 *)__this);
		IL2CPP_END_FINALLY(78)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(78)
	{
		IL2CPP_JUMP_TBL(0x55, IL_0055)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0055:
	{
		return;
	}
}
// System.Void UniRx.Operators.AggregateObservable`3<System.Object,System.Object,System.Object>::.ctor(UniRx.IObservable`1<TSource>,TAccumulate,System.Func`3<TAccumulate,TSource,TAccumulate>,System.Func`2<TAccumulate,TResult>)
extern "C"  void AggregateObservable_3__ctor_m1316974044_gshared (AggregateObservable_3_t3567497731 * __this, Il2CppObject* ___source0, Il2CppObject * ___seed1, Func_3_t1892209229 * ___accumulator2, Func_2_t2135783352 * ___resultSelector3, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = ___source0;
		bool L_1 = ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)(NULL /*static, unused*/, (Il2CppObject*)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		NullCheck((OperatorObservableBase_1_t4196218687 *)__this);
		((  void (*) (OperatorObservableBase_1_t4196218687 *, bool, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)((OperatorObservableBase_1_t4196218687 *)__this, (bool)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		Il2CppObject* L_2 = ___source0;
		__this->set_source_1(L_2);
		Il2CppObject * L_3 = ___seed1;
		__this->set_seed_2(L_3);
		Func_3_t1892209229 * L_4 = ___accumulator2;
		__this->set_accumulator_3(L_4);
		Func_2_t2135783352 * L_5 = ___resultSelector3;
		__this->set_resultSelector_4(L_5);
		return;
	}
}
// System.IDisposable UniRx.Operators.AggregateObservable`3<System.Object,System.Object,System.Object>::SubscribeCore(UniRx.IObserver`1<TResult>,System.IDisposable)
extern "C"  Il2CppObject * AggregateObservable_3_SubscribeCore_m3786217132_gshared (AggregateObservable_3_t3567497731 * __this, Il2CppObject* ___observer0, Il2CppObject * ___cancel1, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = (Il2CppObject*)__this->get_source_1();
		Il2CppObject* L_1 = ___observer0;
		Il2CppObject * L_2 = ___cancel1;
		Aggregate_t245075056 * L_3 = (Aggregate_t245075056 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		((  void (*) (Aggregate_t245075056 *, AggregateObservable_3_t3567497731 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)(L_3, (AggregateObservable_3_t3567497731 *)__this, (Il2CppObject*)L_1, (Il2CppObject *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		NullCheck((Il2CppObject*)L_0);
		Il2CppObject * L_4 = InterfaceFuncInvoker1< Il2CppObject *, Il2CppObject* >::Invoke(0 /* System.IDisposable UniRx.IObservable`1<System.Object>::Subscribe(UniRx.IObserver`1<T>) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5), (Il2CppObject*)L_0, (Il2CppObject*)L_3);
		return L_4;
	}
}
// System.Void UniRx.Operators.AmbObservable`1/AmbOuterObserver/Amb<System.Object>::.ctor()
extern "C"  void Amb__ctor_m305749593_gshared (Amb_t634706591 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UniRx.Operators.AmbObservable`1/AmbOuterObserver/Amb<System.Object>::OnNext(T)
extern "C"  void Amb_OnNext_m303318721_gshared (Amb_t634706591 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = (Il2CppObject*)__this->get_targetObserver_0();
		Il2CppObject * L_1 = ___value0;
		NullCheck((Il2CppObject*)L_0);
		InterfaceActionInvoker1< Il2CppObject * >::Invoke(2 /* System.Void UniRx.IObserver`1<System.Object>::OnNext(T) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0), (Il2CppObject*)L_0, (Il2CppObject *)L_1);
		return;
	}
}
// System.Void UniRx.Operators.AmbObservable`1/AmbOuterObserver/Amb<System.Object>::OnError(System.Exception)
extern Il2CppClass* IDisposable_t1628921374_il2cpp_TypeInfo_var;
extern const uint32_t Amb_OnError_m2552020944_MetadataUsageId;
extern "C"  void Amb_OnError_m2552020944_gshared (Amb_t634706591 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Amb_OnError_m2552020944_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		Il2CppObject* L_0 = (Il2CppObject*)__this->get_targetObserver_0();
		Exception_t1967233988 * L_1 = ___error0;
		NullCheck((Il2CppObject*)L_0);
		InterfaceActionInvoker1< Exception_t1967233988 * >::Invoke(1 /* System.Void UniRx.IObserver`1<System.Object>::OnError(System.Exception) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0), (Il2CppObject*)L_0, (Exception_t1967233988 *)L_1);
		IL2CPP_LEAVE(0x28, FINALLY_0011);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0011;
	}

FINALLY_0011:
	{ // begin finally (depth: 1)
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		EmptyObserver_1_t246459410 * L_2 = ((EmptyObserver_1_t246459410_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->static_fields)->get_Instance_0();
		__this->set_targetObserver_0(L_2);
		Il2CppObject * L_3 = (Il2CppObject *)__this->get_targetDisposable_1();
		NullCheck((Il2CppObject *)L_3);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, (Il2CppObject *)L_3);
		IL2CPP_END_FINALLY(17)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(17)
	{
		IL2CPP_JUMP_TBL(0x28, IL_0028)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0028:
	{
		return;
	}
}
// System.Void UniRx.Operators.AmbObservable`1/AmbOuterObserver/Amb<System.Object>::OnCompleted()
extern Il2CppClass* IDisposable_t1628921374_il2cpp_TypeInfo_var;
extern const uint32_t Amb_OnCompleted_m573614883_MetadataUsageId;
extern "C"  void Amb_OnCompleted_m573614883_gshared (Amb_t634706591 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Amb_OnCompleted_m573614883_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		Il2CppObject* L_0 = (Il2CppObject*)__this->get_targetObserver_0();
		NullCheck((Il2CppObject*)L_0);
		InterfaceActionInvoker0::Invoke(0 /* System.Void UniRx.IObserver`1<System.Object>::OnCompleted() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0), (Il2CppObject*)L_0);
		IL2CPP_LEAVE(0x27, FINALLY_0010);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0010;
	}

FINALLY_0010:
	{ // begin finally (depth: 1)
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		EmptyObserver_1_t246459410 * L_1 = ((EmptyObserver_1_t246459410_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->static_fields)->get_Instance_0();
		__this->set_targetObserver_0(L_1);
		Il2CppObject * L_2 = (Il2CppObject *)__this->get_targetDisposable_1();
		NullCheck((Il2CppObject *)L_2);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, (Il2CppObject *)L_2);
		IL2CPP_END_FINALLY(16)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(16)
	{
		IL2CPP_JUMP_TBL(0x27, IL_0027)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0027:
	{
		return;
	}
}
// System.Void UniRx.Operators.AmbObservable`1/AmbOuterObserver/AmbDecisionObserver<System.Object>::.ctor(UniRx.Operators.AmbObservable`1/AmbOuterObserver<T>,UniRx.Operators.AmbObservable`1/AmbOuterObserver/AmbState<T>,System.IDisposable,UniRx.Operators.AmbObservable`1/AmbOuterObserver/Amb<T>)
extern "C"  void AmbDecisionObserver__ctor_m347170949_gshared (AmbDecisionObserver_t973332161 * __this, AmbOuterObserver_t2827192636 * ___parent0, int32_t ___me1, Il2CppObject * ___otherSubscription2, Amb_t634706591 * ___self3, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		AmbOuterObserver_t2827192636 * L_0 = ___parent0;
		__this->set_parent_0(L_0);
		int32_t L_1 = ___me1;
		__this->set_me_1(L_1);
		Il2CppObject * L_2 = ___otherSubscription2;
		__this->set_otherSubscription_2(L_2);
		Amb_t634706591 * L_3 = ___self3;
		__this->set_self_3(L_3);
		return;
	}
}
// System.Void UniRx.Operators.AmbObservable`1/AmbOuterObserver/AmbDecisionObserver<System.Object>::OnNext(T)
extern Il2CppClass* IDisposable_t1628921374_il2cpp_TypeInfo_var;
extern const uint32_t AmbDecisionObserver_OnNext_m4246898483_MetadataUsageId;
extern "C"  void AmbDecisionObserver_OnNext_m4246898483_gshared (AmbDecisionObserver_t973332161 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (AmbDecisionObserver_OnNext_m4246898483_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Il2CppObject * V_0 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		AmbOuterObserver_t2827192636 * L_0 = (AmbOuterObserver_t2827192636 *)__this->get_parent_0();
		NullCheck(L_0);
		Il2CppObject * L_1 = (Il2CppObject *)L_0->get_gate_3();
		V_0 = (Il2CppObject *)L_1;
		Il2CppObject * L_2 = V_0;
		Monitor_Enter_m476686225(NULL /*static, unused*/, (Il2CppObject *)L_2, /*hidden argument*/NULL);
	}

IL_0012:
	try
	{ // begin try (depth: 1)
		{
			AmbOuterObserver_t2827192636 * L_3 = (AmbOuterObserver_t2827192636 *)__this->get_parent_0();
			NullCheck(L_3);
			int32_t L_4 = (int32_t)L_3->get_choice_6();
			if ((!(((uint32_t)L_4) == ((uint32_t)2))))
			{
				goto IL_0057;
			}
		}

IL_0023:
		{
			AmbOuterObserver_t2827192636 * L_5 = (AmbOuterObserver_t2827192636 *)__this->get_parent_0();
			int32_t L_6 = (int32_t)__this->get_me_1();
			NullCheck(L_5);
			L_5->set_choice_6(L_6);
			Il2CppObject * L_7 = (Il2CppObject *)__this->get_otherSubscription_2();
			NullCheck((Il2CppObject *)L_7);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, (Il2CppObject *)L_7);
			Amb_t634706591 * L_8 = (Amb_t634706591 *)__this->get_self_3();
			AmbOuterObserver_t2827192636 * L_9 = (AmbOuterObserver_t2827192636 *)__this->get_parent_0();
			NullCheck(L_9);
			Il2CppObject* L_10 = (Il2CppObject*)((OperatorObserverBase_2_t1187768149 *)L_9)->get_observer_0();
			il2cpp_codegen_memory_barrier();
			NullCheck(L_8);
			L_8->set_targetObserver_0(L_10);
		}

IL_0057:
		{
			AmbOuterObserver_t2827192636 * L_11 = (AmbOuterObserver_t2827192636 *)__this->get_parent_0();
			NullCheck(L_11);
			int32_t L_12 = (int32_t)L_11->get_choice_6();
			int32_t L_13 = (int32_t)__this->get_me_1();
			if ((!(((uint32_t)L_12) == ((uint32_t)L_13))))
			{
				goto IL_007e;
			}
		}

IL_006d:
		{
			Amb_t634706591 * L_14 = (Amb_t634706591 *)__this->get_self_3();
			NullCheck(L_14);
			Il2CppObject* L_15 = (Il2CppObject*)L_14->get_targetObserver_0();
			Il2CppObject * L_16 = ___value0;
			NullCheck((Il2CppObject*)L_15);
			InterfaceActionInvoker1< Il2CppObject * >::Invoke(2 /* System.Void UniRx.IObserver`1<System.Object>::OnNext(T) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0), (Il2CppObject*)L_15, (Il2CppObject *)L_16);
		}

IL_007e:
		{
			IL2CPP_LEAVE(0x8A, FINALLY_0083);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0083;
	}

FINALLY_0083:
	{ // begin finally (depth: 1)
		Il2CppObject * L_17 = V_0;
		Monitor_Exit_m2088237919(NULL /*static, unused*/, (Il2CppObject *)L_17, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(131)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(131)
	{
		IL2CPP_JUMP_TBL(0x8A, IL_008a)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_008a:
	{
		return;
	}
}
// System.Void UniRx.Operators.AmbObservable`1/AmbOuterObserver/AmbDecisionObserver<System.Object>::OnError(System.Exception)
extern Il2CppClass* IDisposable_t1628921374_il2cpp_TypeInfo_var;
extern const uint32_t AmbDecisionObserver_OnError_m173005890_MetadataUsageId;
extern "C"  void AmbDecisionObserver_OnError_m173005890_gshared (AmbDecisionObserver_t973332161 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (AmbDecisionObserver_OnError_m173005890_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Il2CppObject * V_0 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		AmbOuterObserver_t2827192636 * L_0 = (AmbOuterObserver_t2827192636 *)__this->get_parent_0();
		NullCheck(L_0);
		Il2CppObject * L_1 = (Il2CppObject *)L_0->get_gate_3();
		V_0 = (Il2CppObject *)L_1;
		Il2CppObject * L_2 = V_0;
		Monitor_Enter_m476686225(NULL /*static, unused*/, (Il2CppObject *)L_2, /*hidden argument*/NULL);
	}

IL_0012:
	try
	{ // begin try (depth: 1)
		{
			AmbOuterObserver_t2827192636 * L_3 = (AmbOuterObserver_t2827192636 *)__this->get_parent_0();
			NullCheck(L_3);
			int32_t L_4 = (int32_t)L_3->get_choice_6();
			if ((!(((uint32_t)L_4) == ((uint32_t)2))))
			{
				goto IL_0057;
			}
		}

IL_0023:
		{
			AmbOuterObserver_t2827192636 * L_5 = (AmbOuterObserver_t2827192636 *)__this->get_parent_0();
			int32_t L_6 = (int32_t)__this->get_me_1();
			NullCheck(L_5);
			L_5->set_choice_6(L_6);
			Il2CppObject * L_7 = (Il2CppObject *)__this->get_otherSubscription_2();
			NullCheck((Il2CppObject *)L_7);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, (Il2CppObject *)L_7);
			Amb_t634706591 * L_8 = (Amb_t634706591 *)__this->get_self_3();
			AmbOuterObserver_t2827192636 * L_9 = (AmbOuterObserver_t2827192636 *)__this->get_parent_0();
			NullCheck(L_9);
			Il2CppObject* L_10 = (Il2CppObject*)((OperatorObserverBase_2_t1187768149 *)L_9)->get_observer_0();
			il2cpp_codegen_memory_barrier();
			NullCheck(L_8);
			L_8->set_targetObserver_0(L_10);
		}

IL_0057:
		{
			AmbOuterObserver_t2827192636 * L_11 = (AmbOuterObserver_t2827192636 *)__this->get_parent_0();
			NullCheck(L_11);
			int32_t L_12 = (int32_t)L_11->get_choice_6();
			int32_t L_13 = (int32_t)__this->get_me_1();
			if ((!(((uint32_t)L_12) == ((uint32_t)L_13))))
			{
				goto IL_007e;
			}
		}

IL_006d:
		{
			Amb_t634706591 * L_14 = (Amb_t634706591 *)__this->get_self_3();
			NullCheck(L_14);
			Il2CppObject* L_15 = (Il2CppObject*)L_14->get_targetObserver_0();
			Exception_t1967233988 * L_16 = ___error0;
			NullCheck((Il2CppObject*)L_15);
			InterfaceActionInvoker1< Exception_t1967233988 * >::Invoke(1 /* System.Void UniRx.IObserver`1<System.Object>::OnError(System.Exception) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0), (Il2CppObject*)L_15, (Exception_t1967233988 *)L_16);
		}

IL_007e:
		{
			IL2CPP_LEAVE(0x8A, FINALLY_0083);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0083;
	}

FINALLY_0083:
	{ // begin finally (depth: 1)
		Il2CppObject * L_17 = V_0;
		Monitor_Exit_m2088237919(NULL /*static, unused*/, (Il2CppObject *)L_17, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(131)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(131)
	{
		IL2CPP_JUMP_TBL(0x8A, IL_008a)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_008a:
	{
		return;
	}
}
// System.Void UniRx.Operators.AmbObservable`1/AmbOuterObserver/AmbDecisionObserver<System.Object>::OnCompleted()
extern Il2CppClass* IDisposable_t1628921374_il2cpp_TypeInfo_var;
extern const uint32_t AmbDecisionObserver_OnCompleted_m1650811541_MetadataUsageId;
extern "C"  void AmbDecisionObserver_OnCompleted_m1650811541_gshared (AmbDecisionObserver_t973332161 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (AmbDecisionObserver_OnCompleted_m1650811541_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Il2CppObject * V_0 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		AmbOuterObserver_t2827192636 * L_0 = (AmbOuterObserver_t2827192636 *)__this->get_parent_0();
		NullCheck(L_0);
		Il2CppObject * L_1 = (Il2CppObject *)L_0->get_gate_3();
		V_0 = (Il2CppObject *)L_1;
		Il2CppObject * L_2 = V_0;
		Monitor_Enter_m476686225(NULL /*static, unused*/, (Il2CppObject *)L_2, /*hidden argument*/NULL);
	}

IL_0012:
	try
	{ // begin try (depth: 1)
		{
			AmbOuterObserver_t2827192636 * L_3 = (AmbOuterObserver_t2827192636 *)__this->get_parent_0();
			NullCheck(L_3);
			int32_t L_4 = (int32_t)L_3->get_choice_6();
			if ((!(((uint32_t)L_4) == ((uint32_t)2))))
			{
				goto IL_0057;
			}
		}

IL_0023:
		{
			AmbOuterObserver_t2827192636 * L_5 = (AmbOuterObserver_t2827192636 *)__this->get_parent_0();
			int32_t L_6 = (int32_t)__this->get_me_1();
			NullCheck(L_5);
			L_5->set_choice_6(L_6);
			Il2CppObject * L_7 = (Il2CppObject *)__this->get_otherSubscription_2();
			NullCheck((Il2CppObject *)L_7);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, (Il2CppObject *)L_7);
			Amb_t634706591 * L_8 = (Amb_t634706591 *)__this->get_self_3();
			AmbOuterObserver_t2827192636 * L_9 = (AmbOuterObserver_t2827192636 *)__this->get_parent_0();
			NullCheck(L_9);
			Il2CppObject* L_10 = (Il2CppObject*)((OperatorObserverBase_2_t1187768149 *)L_9)->get_observer_0();
			il2cpp_codegen_memory_barrier();
			NullCheck(L_8);
			L_8->set_targetObserver_0(L_10);
		}

IL_0057:
		{
			AmbOuterObserver_t2827192636 * L_11 = (AmbOuterObserver_t2827192636 *)__this->get_parent_0();
			NullCheck(L_11);
			int32_t L_12 = (int32_t)L_11->get_choice_6();
			int32_t L_13 = (int32_t)__this->get_me_1();
			if ((!(((uint32_t)L_12) == ((uint32_t)L_13))))
			{
				goto IL_007d;
			}
		}

IL_006d:
		{
			Amb_t634706591 * L_14 = (Amb_t634706591 *)__this->get_self_3();
			NullCheck(L_14);
			Il2CppObject* L_15 = (Il2CppObject*)L_14->get_targetObserver_0();
			NullCheck((Il2CppObject*)L_15);
			InterfaceActionInvoker0::Invoke(0 /* System.Void UniRx.IObserver`1<System.Object>::OnCompleted() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0), (Il2CppObject*)L_15);
		}

IL_007d:
		{
			IL2CPP_LEAVE(0x89, FINALLY_0082);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0082;
	}

FINALLY_0082:
	{ // begin finally (depth: 1)
		Il2CppObject * L_16 = V_0;
		Monitor_Exit_m2088237919(NULL /*static, unused*/, (Il2CppObject *)L_16, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(130)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(130)
	{
		IL2CPP_JUMP_TBL(0x89, IL_0089)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0089:
	{
		return;
	}
}
// System.Void UniRx.Operators.AmbObservable`1/AmbOuterObserver<System.Object>::.ctor(UniRx.Operators.AmbObservable`1<T>,UniRx.IObserver`1<T>,System.IDisposable)
extern Il2CppClass* Il2CppObject_il2cpp_TypeInfo_var;
extern const uint32_t AmbOuterObserver__ctor_m971803683_MetadataUsageId;
extern "C"  void AmbOuterObserver__ctor_m971803683_gshared (AmbOuterObserver_t2827192636 * __this, AmbObservable_1_t4231346232 * ___parent0, Il2CppObject* ___observer1, Il2CppObject * ___cancel2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (AmbOuterObserver__ctor_m971803683_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject * L_0 = (Il2CppObject *)il2cpp_codegen_object_new(Il2CppObject_il2cpp_TypeInfo_var);
		Object__ctor_m1772956182(L_0, /*hidden argument*/NULL);
		__this->set_gate_3(L_0);
		__this->set_choice_6(2);
		Il2CppObject* L_1 = ___observer1;
		Il2CppObject * L_2 = ___cancel2;
		NullCheck((OperatorObserverBase_2_t1187768149 *)__this);
		((  void (*) (OperatorObserverBase_2_t1187768149 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((OperatorObserverBase_2_t1187768149 *)__this, (Il2CppObject*)L_1, (Il2CppObject *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		AmbObservable_1_t4231346232 * L_3 = ___parent0;
		__this->set_parent_2(L_3);
		return;
	}
}
// System.IDisposable UniRx.Operators.AmbObservable`1/AmbOuterObserver<System.Object>::Run()
extern Il2CppClass* SingleAssignmentDisposable_t2336378823_il2cpp_TypeInfo_var;
extern const uint32_t AmbOuterObserver_Run_m1474616694_MetadataUsageId;
extern "C"  Il2CppObject * AmbOuterObserver_Run_m1474616694_gshared (AmbOuterObserver_t2827192636 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (AmbOuterObserver_Run_m1474616694_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Il2CppObject * V_0 = NULL;
	Amb_t634706591 * V_1 = NULL;
	Amb_t634706591 * V_2 = NULL;
	{
		SingleAssignmentDisposable_t2336378823 * L_0 = (SingleAssignmentDisposable_t2336378823 *)il2cpp_codegen_object_new(SingleAssignmentDisposable_t2336378823_il2cpp_TypeInfo_var);
		SingleAssignmentDisposable__ctor_m917449122(L_0, /*hidden argument*/NULL);
		__this->set_leftSubscription_4(L_0);
		SingleAssignmentDisposable_t2336378823 * L_1 = (SingleAssignmentDisposable_t2336378823 *)il2cpp_codegen_object_new(SingleAssignmentDisposable_t2336378823_il2cpp_TypeInfo_var);
		SingleAssignmentDisposable__ctor_m917449122(L_1, /*hidden argument*/NULL);
		__this->set_rightSubscription_5(L_1);
		SingleAssignmentDisposable_t2336378823 * L_2 = (SingleAssignmentDisposable_t2336378823 *)__this->get_leftSubscription_4();
		SingleAssignmentDisposable_t2336378823 * L_3 = (SingleAssignmentDisposable_t2336378823 *)__this->get_rightSubscription_5();
		Il2CppObject * L_4 = StableCompositeDisposable_Create_m3764788535(NULL /*static, unused*/, (Il2CppObject *)L_2, (Il2CppObject *)L_3, /*hidden argument*/NULL);
		V_0 = (Il2CppObject *)L_4;
		Amb_t634706591 * L_5 = (Amb_t634706591 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		((  void (*) (Amb_t634706591 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)(L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		V_1 = (Amb_t634706591 *)L_5;
		Amb_t634706591 * L_6 = V_1;
		Il2CppObject * L_7 = V_0;
		NullCheck(L_6);
		L_6->set_targetDisposable_1(L_7);
		Amb_t634706591 * L_8 = V_1;
		SingleAssignmentDisposable_t2336378823 * L_9 = (SingleAssignmentDisposable_t2336378823 *)__this->get_rightSubscription_5();
		Amb_t634706591 * L_10 = V_1;
		AmbDecisionObserver_t973332161 * L_11 = (AmbDecisionObserver_t973332161 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		((  void (*) (AmbDecisionObserver_t973332161 *, AmbOuterObserver_t2827192636 *, int32_t, Il2CppObject *, Amb_t634706591 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->method)(L_11, (AmbOuterObserver_t2827192636 *)__this, (int32_t)0, (Il2CppObject *)L_9, (Amb_t634706591 *)L_10, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		NullCheck(L_8);
		L_8->set_targetObserver_0(L_11);
		Amb_t634706591 * L_12 = (Amb_t634706591 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		((  void (*) (Amb_t634706591 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)(L_12, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		V_2 = (Amb_t634706591 *)L_12;
		Amb_t634706591 * L_13 = V_2;
		Il2CppObject * L_14 = V_0;
		NullCheck(L_13);
		L_13->set_targetDisposable_1(L_14);
		Amb_t634706591 * L_15 = V_2;
		SingleAssignmentDisposable_t2336378823 * L_16 = (SingleAssignmentDisposable_t2336378823 *)__this->get_leftSubscription_4();
		Amb_t634706591 * L_17 = V_2;
		AmbDecisionObserver_t973332161 * L_18 = (AmbDecisionObserver_t973332161 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		((  void (*) (AmbDecisionObserver_t973332161 *, AmbOuterObserver_t2827192636 *, int32_t, Il2CppObject *, Amb_t634706591 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->method)(L_18, (AmbOuterObserver_t2827192636 *)__this, (int32_t)1, (Il2CppObject *)L_16, (Amb_t634706591 *)L_17, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		NullCheck(L_15);
		L_15->set_targetObserver_0(L_18);
		SingleAssignmentDisposable_t2336378823 * L_19 = (SingleAssignmentDisposable_t2336378823 *)__this->get_leftSubscription_4();
		AmbObservable_1_t4231346232 * L_20 = (AmbObservable_1_t4231346232 *)__this->get_parent_2();
		NullCheck(L_20);
		Il2CppObject* L_21 = (Il2CppObject*)L_20->get_source_1();
		Amb_t634706591 * L_22 = V_1;
		NullCheck((Il2CppObject*)L_21);
		Il2CppObject * L_23 = InterfaceFuncInvoker1< Il2CppObject *, Il2CppObject* >::Invoke(0 /* System.IDisposable UniRx.IObservable`1<System.Object>::Subscribe(UniRx.IObserver`1<T>) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6), (Il2CppObject*)L_21, (Il2CppObject*)L_22);
		NullCheck((SingleAssignmentDisposable_t2336378823 *)L_19);
		SingleAssignmentDisposable_set_Disposable_m1599028885((SingleAssignmentDisposable_t2336378823 *)L_19, (Il2CppObject *)L_23, /*hidden argument*/NULL);
		SingleAssignmentDisposable_t2336378823 * L_24 = (SingleAssignmentDisposable_t2336378823 *)__this->get_rightSubscription_5();
		AmbObservable_1_t4231346232 * L_25 = (AmbObservable_1_t4231346232 *)__this->get_parent_2();
		NullCheck(L_25);
		Il2CppObject* L_26 = (Il2CppObject*)L_25->get_second_2();
		Amb_t634706591 * L_27 = V_2;
		NullCheck((Il2CppObject*)L_26);
		Il2CppObject * L_28 = InterfaceFuncInvoker1< Il2CppObject *, Il2CppObject* >::Invoke(0 /* System.IDisposable UniRx.IObservable`1<System.Object>::Subscribe(UniRx.IObserver`1<T>) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6), (Il2CppObject*)L_26, (Il2CppObject*)L_27);
		NullCheck((SingleAssignmentDisposable_t2336378823 *)L_24);
		SingleAssignmentDisposable_set_Disposable_m1599028885((SingleAssignmentDisposable_t2336378823 *)L_24, (Il2CppObject *)L_28, /*hidden argument*/NULL);
		Il2CppObject * L_29 = V_0;
		return L_29;
	}
}
// System.Void UniRx.Operators.AmbObservable`1/AmbOuterObserver<System.Object>::OnNext(T)
extern "C"  void AmbOuterObserver_OnNext_m4113981434_gshared (AmbOuterObserver_t2827192636 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void UniRx.Operators.AmbObservable`1/AmbOuterObserver<System.Object>::OnError(System.Exception)
extern "C"  void AmbOuterObserver_OnError_m2210350857_gshared (AmbOuterObserver_t2827192636 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void UniRx.Operators.AmbObservable`1/AmbOuterObserver<System.Object>::OnCompleted()
extern "C"  void AmbOuterObserver_OnCompleted_m130121692_gshared (AmbOuterObserver_t2827192636 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void UniRx.Operators.AmbObservable`1<System.Object>::.ctor(UniRx.IObservable`1<T>,UniRx.IObservable`1<T>)
extern "C"  void AmbObservable_1__ctor_m3833350880_gshared (AmbObservable_1_t4231346232 * __this, Il2CppObject* ___source0, Il2CppObject* ___second1, const MethodInfo* method)
{
	AmbObservable_1_t4231346232 * G_B2_0 = NULL;
	AmbObservable_1_t4231346232 * G_B1_0 = NULL;
	int32_t G_B3_0 = 0;
	AmbObservable_1_t4231346232 * G_B3_1 = NULL;
	{
		Il2CppObject* L_0 = ___source0;
		bool L_1 = ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)(NULL /*static, unused*/, (Il2CppObject*)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		G_B1_0 = ((AmbObservable_1_t4231346232 *)(__this));
		if (L_1)
		{
			G_B2_0 = ((AmbObservable_1_t4231346232 *)(__this));
			goto IL_0014;
		}
	}
	{
		Il2CppObject* L_2 = ___second1;
		bool L_3 = ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)(NULL /*static, unused*/, (Il2CppObject*)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		G_B3_0 = ((int32_t)(L_3));
		G_B3_1 = ((AmbObservable_1_t4231346232 *)(G_B1_0));
		goto IL_0015;
	}

IL_0014:
	{
		G_B3_0 = 1;
		G_B3_1 = ((AmbObservable_1_t4231346232 *)(G_B2_0));
	}

IL_0015:
	{
		NullCheck((OperatorObservableBase_1_t4196218687 *)G_B3_1);
		((  void (*) (OperatorObservableBase_1_t4196218687 *, bool, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)((OperatorObservableBase_1_t4196218687 *)G_B3_1, (bool)G_B3_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		Il2CppObject* L_4 = ___source0;
		__this->set_source_1(L_4);
		Il2CppObject* L_5 = ___second1;
		__this->set_second_2(L_5);
		return;
	}
}
// System.IDisposable UniRx.Operators.AmbObservable`1<System.Object>::SubscribeCore(UniRx.IObserver`1<T>,System.IDisposable)
extern "C"  Il2CppObject * AmbObservable_1_SubscribeCore_m1940249880_gshared (AmbObservable_1_t4231346232 * __this, Il2CppObject* ___observer0, Il2CppObject * ___cancel1, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = ___observer0;
		Il2CppObject * L_1 = ___cancel1;
		AmbOuterObserver_t2827192636 * L_2 = (AmbOuterObserver_t2827192636 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		((  void (*) (AmbOuterObserver_t2827192636 *, AmbObservable_1_t4231346232 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)(L_2, (AmbObservable_1_t4231346232 *)__this, (Il2CppObject*)L_0, (Il2CppObject *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		NullCheck((AmbOuterObserver_t2827192636 *)L_2);
		Il2CppObject * L_3 = ((  Il2CppObject * (*) (AmbOuterObserver_t2827192636 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->method)((AmbOuterObserver_t2827192636 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		return L_3;
	}
}
// System.Void UniRx.Operators.AsObservableObservable`1/AsObservable<System.Boolean>::.ctor(UniRx.IObserver`1<T>,System.IDisposable)
extern "C"  void AsObservable__ctor_m346186224_gshared (AsObservable_t3870929039 * __this, Il2CppObject* ___observer0, Il2CppObject * ___cancel1, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = ___observer0;
		Il2CppObject * L_1 = ___cancel1;
		NullCheck((OperatorObserverBase_2_t60103313 *)__this);
		((  void (*) (OperatorObserverBase_2_t60103313 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((OperatorObserverBase_2_t60103313 *)__this, (Il2CppObject*)L_0, (Il2CppObject *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		return;
	}
}
// System.Void UniRx.Operators.AsObservableObservable`1/AsObservable<System.Boolean>::OnNext(T)
extern "C"  void AsObservable_OnNext_m625907618_gshared (AsObservable_t3870929039 * __this, bool ___value0, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = (Il2CppObject*)((OperatorObserverBase_2_t60103313 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		bool L_1 = ___value0;
		NullCheck((Il2CppObject*)L_0);
		InterfaceActionInvoker1< bool >::Invoke(2 /* System.Void UniRx.IObserver`1<System.Boolean>::OnNext(T) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), (Il2CppObject*)L_0, (bool)L_1);
		return;
	}
}
// System.Void UniRx.Operators.AsObservableObservable`1/AsObservable<System.Boolean>::OnError(System.Exception)
extern "C"  void AsObservable_OnError_m4060294833_gshared (AsObservable_t3870929039 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method)
{
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		Il2CppObject* L_0 = (Il2CppObject*)((OperatorObserverBase_2_t60103313 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		Exception_t1967233988 * L_1 = ___error0;
		NullCheck((Il2CppObject*)L_0);
		InterfaceActionInvoker1< Exception_t1967233988 * >::Invoke(1 /* System.Void UniRx.IObserver`1<System.Boolean>::OnError(System.Exception) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), (Il2CppObject*)L_0, (Exception_t1967233988 *)L_1);
		IL2CPP_LEAVE(0x1A, FINALLY_0013);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0013;
	}

FINALLY_0013:
	{ // begin finally (depth: 1)
		NullCheck((OperatorObserverBase_2_t60103313 *)__this);
		VirtActionInvoker0::Invoke(4 /* System.Void UniRx.Operators.OperatorObserverBase`2<System.Boolean,System.Boolean>::Dispose() */, (OperatorObserverBase_2_t60103313 *)__this);
		IL2CPP_END_FINALLY(19)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(19)
	{
		IL2CPP_JUMP_TBL(0x1A, IL_001a)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_001a:
	{
		return;
	}
}
// System.Void UniRx.Operators.AsObservableObservable`1/AsObservable<System.Boolean>::OnCompleted()
extern "C"  void AsObservable_OnCompleted_m2082841476_gshared (AsObservable_t3870929039 * __this, const MethodInfo* method)
{
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		Il2CppObject* L_0 = (Il2CppObject*)((OperatorObserverBase_2_t60103313 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		NullCheck((Il2CppObject*)L_0);
		InterfaceActionInvoker0::Invoke(0 /* System.Void UniRx.IObserver`1<System.Boolean>::OnCompleted() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), (Il2CppObject*)L_0);
		IL2CPP_LEAVE(0x19, FINALLY_0012);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0012;
	}

FINALLY_0012:
	{ // begin finally (depth: 1)
		NullCheck((OperatorObserverBase_2_t60103313 *)__this);
		VirtActionInvoker0::Invoke(4 /* System.Void UniRx.Operators.OperatorObserverBase`2<System.Boolean,System.Boolean>::Dispose() */, (OperatorObserverBase_2_t60103313 *)__this);
		IL2CPP_END_FINALLY(18)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(18)
	{
		IL2CPP_JUMP_TBL(0x19, IL_0019)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0019:
	{
		return;
	}
}
// System.Void UniRx.Operators.AsObservableObservable`1/AsObservable<System.Object>::.ctor(UniRx.IObserver`1<T>,System.IDisposable)
extern "C"  void AsObservable__ctor_m2772916531_gshared (AsObservable_t202062822 * __this, Il2CppObject* ___observer0, Il2CppObject * ___cancel1, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = ___observer0;
		Il2CppObject * L_1 = ___cancel1;
		NullCheck((OperatorObserverBase_2_t1187768149 *)__this);
		((  void (*) (OperatorObserverBase_2_t1187768149 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((OperatorObserverBase_2_t1187768149 *)__this, (Il2CppObject*)L_0, (Il2CppObject *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		return;
	}
}
// System.Void UniRx.Operators.AsObservableObservable`1/AsObservable<System.Object>::OnNext(T)
extern "C"  void AsObservable_OnNext_m2074187391_gshared (AsObservable_t202062822 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = (Il2CppObject*)((OperatorObserverBase_2_t1187768149 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		Il2CppObject * L_1 = ___value0;
		NullCheck((Il2CppObject*)L_0);
		InterfaceActionInvoker1< Il2CppObject * >::Invoke(2 /* System.Void UniRx.IObserver`1<System.Object>::OnNext(T) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), (Il2CppObject*)L_0, (Il2CppObject *)L_1);
		return;
	}
}
// System.Void UniRx.Operators.AsObservableObservable`1/AsObservable<System.Object>::OnError(System.Exception)
extern "C"  void AsObservable_OnError_m2436813710_gshared (AsObservable_t202062822 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method)
{
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		Il2CppObject* L_0 = (Il2CppObject*)((OperatorObserverBase_2_t1187768149 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		Exception_t1967233988 * L_1 = ___error0;
		NullCheck((Il2CppObject*)L_0);
		InterfaceActionInvoker1< Exception_t1967233988 * >::Invoke(1 /* System.Void UniRx.IObserver`1<System.Object>::OnError(System.Exception) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), (Il2CppObject*)L_0, (Exception_t1967233988 *)L_1);
		IL2CPP_LEAVE(0x1A, FINALLY_0013);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0013;
	}

FINALLY_0013:
	{ // begin finally (depth: 1)
		NullCheck((OperatorObserverBase_2_t1187768149 *)__this);
		VirtActionInvoker0::Invoke(4 /* System.Void UniRx.Operators.OperatorObserverBase`2<System.Object,System.Object>::Dispose() */, (OperatorObserverBase_2_t1187768149 *)__this);
		IL2CPP_END_FINALLY(19)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(19)
	{
		IL2CPP_JUMP_TBL(0x1A, IL_001a)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_001a:
	{
		return;
	}
}
// System.Void UniRx.Operators.AsObservableObservable`1/AsObservable<System.Object>::OnCompleted()
extern "C"  void AsObservable_OnCompleted_m1626598369_gshared (AsObservable_t202062822 * __this, const MethodInfo* method)
{
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		Il2CppObject* L_0 = (Il2CppObject*)((OperatorObserverBase_2_t1187768149 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		NullCheck((Il2CppObject*)L_0);
		InterfaceActionInvoker0::Invoke(0 /* System.Void UniRx.IObserver`1<System.Object>::OnCompleted() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), (Il2CppObject*)L_0);
		IL2CPP_LEAVE(0x19, FINALLY_0012);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0012;
	}

FINALLY_0012:
	{ // begin finally (depth: 1)
		NullCheck((OperatorObserverBase_2_t1187768149 *)__this);
		VirtActionInvoker0::Invoke(4 /* System.Void UniRx.Operators.OperatorObserverBase`2<System.Object,System.Object>::Dispose() */, (OperatorObserverBase_2_t1187768149 *)__this);
		IL2CPP_END_FINALLY(18)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(18)
	{
		IL2CPP_JUMP_TBL(0x19, IL_0019)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0019:
	{
		return;
	}
}
// System.Void UniRx.Operators.AsObservableObservable`1/AsObservable<UniRx.Unit>::.ctor(UniRx.IObserver`1<T>,System.IDisposable)
extern "C"  void AsObservable__ctor_m2789927517_gshared (AsObservable_t1923242440 * __this, Il2CppObject* ___observer0, Il2CppObject * ___cancel1, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = ___observer0;
		Il2CppObject * L_1 = ___cancel1;
		NullCheck((OperatorObserverBase_2_t4165376397 *)__this);
		((  void (*) (OperatorObserverBase_2_t4165376397 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((OperatorObserverBase_2_t4165376397 *)__this, (Il2CppObject*)L_0, (Il2CppObject *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		return;
	}
}
// System.Void UniRx.Operators.AsObservableObservable`1/AsObservable<UniRx.Unit>::OnNext(T)
extern "C"  void AsObservable_OnNext_m3383118613_gshared (AsObservable_t1923242440 * __this, Unit_t2558286038  ___value0, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = (Il2CppObject*)((OperatorObserverBase_2_t4165376397 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		Unit_t2558286038  L_1 = ___value0;
		NullCheck((Il2CppObject*)L_0);
		InterfaceActionInvoker1< Unit_t2558286038  >::Invoke(2 /* System.Void UniRx.IObserver`1<UniRx.Unit>::OnNext(T) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), (Il2CppObject*)L_0, (Unit_t2558286038 )L_1);
		return;
	}
}
// System.Void UniRx.Operators.AsObservableObservable`1/AsObservable<UniRx.Unit>::OnError(System.Exception)
extern "C"  void AsObservable_OnError_m182688804_gshared (AsObservable_t1923242440 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method)
{
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		Il2CppObject* L_0 = (Il2CppObject*)((OperatorObserverBase_2_t4165376397 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		Exception_t1967233988 * L_1 = ___error0;
		NullCheck((Il2CppObject*)L_0);
		InterfaceActionInvoker1< Exception_t1967233988 * >::Invoke(1 /* System.Void UniRx.IObserver`1<UniRx.Unit>::OnError(System.Exception) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), (Il2CppObject*)L_0, (Exception_t1967233988 *)L_1);
		IL2CPP_LEAVE(0x1A, FINALLY_0013);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0013;
	}

FINALLY_0013:
	{ // begin finally (depth: 1)
		NullCheck((OperatorObserverBase_2_t4165376397 *)__this);
		VirtActionInvoker0::Invoke(4 /* System.Void UniRx.Operators.OperatorObserverBase`2<UniRx.Unit,UniRx.Unit>::Dispose() */, (OperatorObserverBase_2_t4165376397 *)__this);
		IL2CPP_END_FINALLY(19)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(19)
	{
		IL2CPP_JUMP_TBL(0x1A, IL_001a)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_001a:
	{
		return;
	}
}
// System.Void UniRx.Operators.AsObservableObservable`1/AsObservable<UniRx.Unit>::OnCompleted()
extern "C"  void AsObservable_OnCompleted_m4257244535_gshared (AsObservable_t1923242440 * __this, const MethodInfo* method)
{
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		Il2CppObject* L_0 = (Il2CppObject*)((OperatorObserverBase_2_t4165376397 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		NullCheck((Il2CppObject*)L_0);
		InterfaceActionInvoker0::Invoke(0 /* System.Void UniRx.IObserver`1<UniRx.Unit>::OnCompleted() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), (Il2CppObject*)L_0);
		IL2CPP_LEAVE(0x19, FINALLY_0012);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0012;
	}

FINALLY_0012:
	{ // begin finally (depth: 1)
		NullCheck((OperatorObserverBase_2_t4165376397 *)__this);
		VirtActionInvoker0::Invoke(4 /* System.Void UniRx.Operators.OperatorObserverBase`2<UniRx.Unit,UniRx.Unit>::Dispose() */, (OperatorObserverBase_2_t4165376397 *)__this);
		IL2CPP_END_FINALLY(18)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(18)
	{
		IL2CPP_JUMP_TBL(0x19, IL_0019)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0019:
	{
		return;
	}
}
// System.Void UniRx.Operators.AsObservableObservable`1<System.Boolean>::.ctor(UniRx.IObservable`1<T>)
extern "C"  void AsObservableObservable_1__ctor_m417038805_gshared (AsObservableObservable_1_t3890699176 * __this, Il2CppObject* ___source0, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = ___source0;
		bool L_1 = ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)(NULL /*static, unused*/, (Il2CppObject*)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		NullCheck((OperatorObservableBase_1_t3570117608 *)__this);
		((  void (*) (OperatorObservableBase_1_t3570117608 *, bool, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)((OperatorObservableBase_1_t3570117608 *)__this, (bool)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		Il2CppObject* L_2 = ___source0;
		__this->set_source_1(L_2);
		return;
	}
}
// System.IDisposable UniRx.Operators.AsObservableObservable`1<System.Boolean>::SubscribeCore(UniRx.IObserver`1<T>,System.IDisposable)
extern "C"  Il2CppObject * AsObservableObservable_1_SubscribeCore_m286231784_gshared (AsObservableObservable_1_t3890699176 * __this, Il2CppObject* ___observer0, Il2CppObject * ___cancel1, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = (Il2CppObject*)__this->get_source_1();
		Il2CppObject* L_1 = ___observer0;
		Il2CppObject * L_2 = ___cancel1;
		AsObservable_t3870929039 * L_3 = (AsObservable_t3870929039 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		((  void (*) (AsObservable_t3870929039 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)(L_3, (Il2CppObject*)L_1, (Il2CppObject *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		NullCheck((Il2CppObject*)L_0);
		Il2CppObject * L_4 = InterfaceFuncInvoker1< Il2CppObject *, Il2CppObject* >::Invoke(0 /* System.IDisposable UniRx.IObservable`1<System.Boolean>::Subscribe(UniRx.IObserver`1<T>) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5), (Il2CppObject*)L_0, (Il2CppObject*)L_3);
		return L_4;
	}
}
// System.Void UniRx.Operators.AsObservableObservable`1<System.Object>::.ctor(UniRx.IObservable`1<T>)
extern "C"  void AsObservableObservable_1__ctor_m2118303930_gshared (AsObservableObservable_1_t221832959 * __this, Il2CppObject* ___source0, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = ___source0;
		bool L_1 = ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)(NULL /*static, unused*/, (Il2CppObject*)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		NullCheck((OperatorObservableBase_1_t4196218687 *)__this);
		((  void (*) (OperatorObservableBase_1_t4196218687 *, bool, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)((OperatorObservableBase_1_t4196218687 *)__this, (bool)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		Il2CppObject* L_2 = ___source0;
		__this->set_source_1(L_2);
		return;
	}
}
// System.IDisposable UniRx.Operators.AsObservableObservable`1<System.Object>::SubscribeCore(UniRx.IObserver`1<T>,System.IDisposable)
extern "C"  Il2CppObject * AsObservableObservable_1_SubscribeCore_m3746005965_gshared (AsObservableObservable_1_t221832959 * __this, Il2CppObject* ___observer0, Il2CppObject * ___cancel1, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = (Il2CppObject*)__this->get_source_1();
		Il2CppObject* L_1 = ___observer0;
		Il2CppObject * L_2 = ___cancel1;
		AsObservable_t202062822 * L_3 = (AsObservable_t202062822 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		((  void (*) (AsObservable_t202062822 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)(L_3, (Il2CppObject*)L_1, (Il2CppObject *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		NullCheck((Il2CppObject*)L_0);
		Il2CppObject * L_4 = InterfaceFuncInvoker1< Il2CppObject *, Il2CppObject* >::Invoke(0 /* System.IDisposable UniRx.IObservable`1<System.Object>::Subscribe(UniRx.IObserver`1<T>) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5), (Il2CppObject*)L_0, (Il2CppObject*)L_3);
		return L_4;
	}
}
// System.Void UniRx.Operators.AsObservableObservable`1<UniRx.Unit>::.ctor(UniRx.IObservable`1<T>)
extern "C"  void AsObservableObservable_1__ctor_m2128174024_gshared (AsObservableObservable_1_t1943012577 * __this, Il2CppObject* ___source0, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = ___source0;
		bool L_1 = ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)(NULL /*static, unused*/, (Il2CppObject*)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		NullCheck((OperatorObservableBase_1_t1622431009 *)__this);
		((  void (*) (OperatorObservableBase_1_t1622431009 *, bool, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)((OperatorObservableBase_1_t1622431009 *)__this, (bool)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		Il2CppObject* L_2 = ___source0;
		__this->set_source_1(L_2);
		return;
	}
}
// System.IDisposable UniRx.Operators.AsObservableObservable`1<UniRx.Unit>::SubscribeCore(UniRx.IObserver`1<T>,System.IDisposable)
extern "C"  Il2CppObject * AsObservableObservable_1_SubscribeCore_m1658163157_gshared (AsObservableObservable_1_t1943012577 * __this, Il2CppObject* ___observer0, Il2CppObject * ___cancel1, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = (Il2CppObject*)__this->get_source_1();
		Il2CppObject* L_1 = ___observer0;
		Il2CppObject * L_2 = ___cancel1;
		AsObservable_t1923242440 * L_3 = (AsObservable_t1923242440 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		((  void (*) (AsObservable_t1923242440 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)(L_3, (Il2CppObject*)L_1, (Il2CppObject *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		NullCheck((Il2CppObject*)L_0);
		Il2CppObject * L_4 = InterfaceFuncInvoker1< Il2CppObject *, Il2CppObject* >::Invoke(0 /* System.IDisposable UniRx.IObservable`1<UniRx.Unit>::Subscribe(UniRx.IObserver`1<T>) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5), (Il2CppObject*)L_0, (Il2CppObject*)L_3);
		return L_4;
	}
}
// System.Void UniRx.Operators.AsUnitObservableObservable`1/AsUnitObservable<System.Object>::.ctor(UniRx.IObserver`1<UniRx.Unit>,System.IDisposable)
extern "C"  void AsUnitObservable__ctor_m841429375_gshared (AsUnitObservable_t2008788970 * __this, Il2CppObject* ___observer0, Il2CppObject * ___cancel1, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = ___observer0;
		Il2CppObject * L_1 = ___cancel1;
		NullCheck((OperatorObserverBase_2_t2908947767 *)__this);
		((  void (*) (OperatorObserverBase_2_t2908947767 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((OperatorObserverBase_2_t2908947767 *)__this, (Il2CppObject*)L_0, (Il2CppObject *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		return;
	}
}
// System.Void UniRx.Operators.AsUnitObservableObservable`1/AsUnitObservable<System.Object>::OnNext(T)
extern Il2CppClass* Unit_t2558286038_il2cpp_TypeInfo_var;
extern Il2CppClass* IObserver_1_t475317645_il2cpp_TypeInfo_var;
extern const uint32_t AsUnitObservable_OnNext_m2456150271_MetadataUsageId;
extern "C"  void AsUnitObservable_OnNext_m2456150271_gshared (AsUnitObservable_t2008788970 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (AsUnitObservable_OnNext_m2456150271_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject* L_0 = (Il2CppObject*)((OperatorObserverBase_2_t2908947767 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		IL2CPP_RUNTIME_CLASS_INIT(Unit_t2558286038_il2cpp_TypeInfo_var);
		Unit_t2558286038  L_1 = Unit_get_Default_m3654553472(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck((Il2CppObject*)L_0);
		InterfaceActionInvoker1< Unit_t2558286038  >::Invoke(2 /* System.Void UniRx.IObserver`1<UniRx.Unit>::OnNext(T) */, IObserver_1_t475317645_il2cpp_TypeInfo_var, (Il2CppObject*)L_0, (Unit_t2558286038 )L_1);
		return;
	}
}
// System.Void UniRx.Operators.AsUnitObservableObservable`1/AsUnitObservable<System.Object>::OnError(System.Exception)
extern Il2CppClass* IObserver_1_t475317645_il2cpp_TypeInfo_var;
extern const uint32_t AsUnitObservable_OnError_m253632014_MetadataUsageId;
extern "C"  void AsUnitObservable_OnError_m253632014_gshared (AsUnitObservable_t2008788970 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (AsUnitObservable_OnError_m253632014_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		Il2CppObject* L_0 = (Il2CppObject*)((OperatorObserverBase_2_t2908947767 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		Exception_t1967233988 * L_1 = ___error0;
		NullCheck((Il2CppObject*)L_0);
		InterfaceActionInvoker1< Exception_t1967233988 * >::Invoke(1 /* System.Void UniRx.IObserver`1<UniRx.Unit>::OnError(System.Exception) */, IObserver_1_t475317645_il2cpp_TypeInfo_var, (Il2CppObject*)L_0, (Exception_t1967233988 *)L_1);
		IL2CPP_LEAVE(0x1A, FINALLY_0013);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0013;
	}

FINALLY_0013:
	{ // begin finally (depth: 1)
		NullCheck((OperatorObserverBase_2_t2908947767 *)__this);
		VirtActionInvoker0::Invoke(4 /* System.Void UniRx.Operators.OperatorObserverBase`2<System.Object,UniRx.Unit>::Dispose() */, (OperatorObserverBase_2_t2908947767 *)__this);
		IL2CPP_END_FINALLY(19)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(19)
	{
		IL2CPP_JUMP_TBL(0x1A, IL_001a)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_001a:
	{
		return;
	}
}
// System.Void UniRx.Operators.AsUnitObservableObservable`1/AsUnitObservable<System.Object>::OnCompleted()
extern Il2CppClass* IObserver_1_t475317645_il2cpp_TypeInfo_var;
extern const uint32_t AsUnitObservable_OnCompleted_m2408511073_MetadataUsageId;
extern "C"  void AsUnitObservable_OnCompleted_m2408511073_gshared (AsUnitObservable_t2008788970 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (AsUnitObservable_OnCompleted_m2408511073_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		Il2CppObject* L_0 = (Il2CppObject*)((OperatorObserverBase_2_t2908947767 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		NullCheck((Il2CppObject*)L_0);
		InterfaceActionInvoker0::Invoke(0 /* System.Void UniRx.IObserver`1<UniRx.Unit>::OnCompleted() */, IObserver_1_t475317645_il2cpp_TypeInfo_var, (Il2CppObject*)L_0);
		IL2CPP_LEAVE(0x19, FINALLY_0012);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0012;
	}

FINALLY_0012:
	{ // begin finally (depth: 1)
		NullCheck((OperatorObserverBase_2_t2908947767 *)__this);
		VirtActionInvoker0::Invoke(4 /* System.Void UniRx.Operators.OperatorObserverBase`2<System.Object,UniRx.Unit>::Dispose() */, (OperatorObserverBase_2_t2908947767 *)__this);
		IL2CPP_END_FINALLY(18)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(18)
	{
		IL2CPP_JUMP_TBL(0x19, IL_0019)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0019:
	{
		return;
	}
}
// System.Void UniRx.Operators.AsUnitObservableObservable`1<System.Object>::.ctor(UniRx.IObservable`1<T>)
extern const MethodInfo* OperatorObservableBase_1__ctor_m284357152_MethodInfo_var;
extern const uint32_t AsUnitObservableObservable_1__ctor_m2657289694_MetadataUsageId;
extern "C"  void AsUnitObservableObservable_1__ctor_m2657289694_gshared (AsUnitObservableObservable_1_t2313647363 * __this, Il2CppObject* ___source0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (AsUnitObservableObservable_1__ctor_m2657289694_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject* L_0 = ___source0;
		bool L_1 = ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)(NULL /*static, unused*/, (Il2CppObject*)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		NullCheck((OperatorObservableBase_1_t1622431009 *)__this);
		OperatorObservableBase_1__ctor_m284357152((OperatorObservableBase_1_t1622431009 *)__this, (bool)L_1, /*hidden argument*/OperatorObservableBase_1__ctor_m284357152_MethodInfo_var);
		Il2CppObject* L_2 = ___source0;
		__this->set_source_1(L_2);
		return;
	}
}
// System.IDisposable UniRx.Operators.AsUnitObservableObservable`1<System.Object>::SubscribeCore(UniRx.IObserver`1<UniRx.Unit>,System.IDisposable)
extern "C"  Il2CppObject * AsUnitObservableObservable_1_SubscribeCore_m2239287369_gshared (AsUnitObservableObservable_1_t2313647363 * __this, Il2CppObject* ___observer0, Il2CppObject * ___cancel1, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = (Il2CppObject*)__this->get_source_1();
		Il2CppObject* L_1 = ___observer0;
		Il2CppObject * L_2 = ___cancel1;
		AsUnitObservable_t2008788970 * L_3 = (AsUnitObservable_t2008788970 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		((  void (*) (AsUnitObservable_t2008788970 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)(L_3, (Il2CppObject*)L_1, (Il2CppObject *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		NullCheck((Il2CppObject*)L_0);
		Il2CppObject * L_4 = InterfaceFuncInvoker1< Il2CppObject *, Il2CppObject* >::Invoke(0 /* System.IDisposable UniRx.IObservable`1<System.Object>::Subscribe(UniRx.IObserver`1<T>) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3), (Il2CppObject*)L_0, (Il2CppObject*)L_3);
		return L_4;
	}
}
// System.Void UniRx.Operators.BufferObservable`1/Buffer_<System.Object>::.ctor(UniRx.Operators.BufferObservable`1<T>,UniRx.IObserver`1<System.Collections.Generic.IList`1<T>>,System.IDisposable)
extern "C"  void Buffer___ctor_m13317340_gshared (Buffer__t1281669152 * __this, BufferObservable_1_t574294898 * ___parent0, Il2CppObject* ___observer1, Il2CppObject * ___cancel2, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = ___observer1;
		Il2CppObject * L_1 = ___cancel2;
		NullCheck((OperatorObserverBase_2_t3354260463 *)__this);
		((  void (*) (OperatorObserverBase_2_t3354260463 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((OperatorObserverBase_2_t3354260463 *)__this, (Il2CppObject*)L_0, (Il2CppObject *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		BufferObservable_1_t574294898 * L_2 = ___parent0;
		__this->set_parent_2(L_2);
		return;
	}
}
// System.IDisposable UniRx.Operators.BufferObservable`1/Buffer_<System.Object>::Run()
extern "C"  Il2CppObject * Buffer__Run_m4082178010_gshared (Buffer__t1281669152 * __this, const MethodInfo* method)
{
	{
		Queue_1_t3342152929 * L_0 = (Queue_1_t3342152929 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		((  void (*) (Queue_1_t3342152929 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		__this->set_q_3(L_0);
		__this->set_index_4((-1));
		BufferObservable_1_t574294898 * L_1 = (BufferObservable_1_t574294898 *)__this->get_parent_2();
		NullCheck(L_1);
		Il2CppObject* L_2 = (Il2CppObject*)L_1->get_source_1();
		NullCheck((Il2CppObject*)L_2);
		Il2CppObject * L_3 = InterfaceFuncInvoker1< Il2CppObject *, Il2CppObject* >::Invoke(0 /* System.IDisposable UniRx.IObservable`1<System.Object>::Subscribe(UniRx.IObserver`1<T>) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4), (Il2CppObject*)L_2, (Il2CppObject*)__this);
		return L_3;
	}
}
// System.Void UniRx.Operators.BufferObservable`1/Buffer_<System.Object>::OnNext(T)
extern "C"  void Buffer__OnNext_m2510311454_gshared (Buffer__t1281669152 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	List_1_t1634065389 * V_2 = NULL;
	{
		int32_t L_0 = (int32_t)__this->get_index_4();
		__this->set_index_4(((int32_t)((int32_t)L_0+(int32_t)1)));
		int32_t L_1 = (int32_t)__this->get_index_4();
		BufferObservable_1_t574294898 * L_2 = (BufferObservable_1_t574294898 *)__this->get_parent_2();
		NullCheck(L_2);
		int32_t L_3 = (int32_t)L_2->get_skip_3();
		if (((int32_t)((int32_t)L_1%(int32_t)L_3)))
		{
			goto IL_0040;
		}
	}
	{
		Queue_1_t3342152929 * L_4 = (Queue_1_t3342152929 *)__this->get_q_3();
		BufferObservable_1_t574294898 * L_5 = (BufferObservable_1_t574294898 *)__this->get_parent_2();
		NullCheck(L_5);
		int32_t L_6 = (int32_t)L_5->get_count_2();
		List_1_t1634065389 * L_7 = (List_1_t1634065389 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		((  void (*) (List_1_t1634065389 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6)->method)(L_7, (int32_t)L_6, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		NullCheck((Queue_1_t3342152929 *)L_4);
		((  void (*) (Queue_1_t3342152929 *, List_1_t1634065389 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7)->method)((Queue_1_t3342152929 *)L_4, (List_1_t1634065389 *)L_7, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7));
	}

IL_0040:
	{
		Queue_1_t3342152929 * L_8 = (Queue_1_t3342152929 *)__this->get_q_3();
		NullCheck((Queue_1_t3342152929 *)L_8);
		int32_t L_9 = VirtFuncInvoker0< int32_t >::Invoke(6 /* System.Int32 System.Collections.Generic.Queue`1<System.Collections.Generic.List`1<System.Object>>::get_Count() */, (Queue_1_t3342152929 *)L_8);
		V_0 = (int32_t)L_9;
		V_1 = (int32_t)0;
		goto IL_009f;
	}

IL_0053:
	{
		Queue_1_t3342152929 * L_10 = (Queue_1_t3342152929 *)__this->get_q_3();
		NullCheck((Queue_1_t3342152929 *)L_10);
		List_1_t1634065389 * L_11 = ((  List_1_t1634065389 * (*) (Queue_1_t3342152929 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9)->method)((Queue_1_t3342152929 *)L_10, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9));
		V_2 = (List_1_t1634065389 *)L_11;
		List_1_t1634065389 * L_12 = V_2;
		Il2CppObject * L_13 = ___value0;
		NullCheck((List_1_t1634065389 *)L_12);
		VirtActionInvoker1< Il2CppObject * >::Invoke(22 /* System.Void System.Collections.Generic.List`1<System.Object>::Add(!0) */, (List_1_t1634065389 *)L_12, (Il2CppObject *)L_13);
		List_1_t1634065389 * L_14 = V_2;
		NullCheck((List_1_t1634065389 *)L_14);
		int32_t L_15 = VirtFuncInvoker0< int32_t >::Invoke(20 /* System.Int32 System.Collections.Generic.List`1<System.Object>::get_Count() */, (List_1_t1634065389 *)L_14);
		BufferObservable_1_t574294898 * L_16 = (BufferObservable_1_t574294898 *)__this->get_parent_2();
		NullCheck(L_16);
		int32_t L_17 = (int32_t)L_16->get_count_2();
		if ((!(((uint32_t)L_15) == ((uint32_t)L_17))))
		{
			goto IL_008f;
		}
	}
	{
		Il2CppObject* L_18 = (Il2CppObject*)((OperatorObserverBase_2_t3354260463 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		List_1_t1634065389 * L_19 = V_2;
		NullCheck((Il2CppObject*)L_18);
		InterfaceActionInvoker1< Il2CppObject* >::Invoke(2 /* System.Void UniRx.IObserver`1<System.Collections.Generic.IList`1<System.Object>>::OnNext(T) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 12), (Il2CppObject*)L_18, (Il2CppObject*)L_19);
		goto IL_009b;
	}

IL_008f:
	{
		Queue_1_t3342152929 * L_20 = (Queue_1_t3342152929 *)__this->get_q_3();
		List_1_t1634065389 * L_21 = V_2;
		NullCheck((Queue_1_t3342152929 *)L_20);
		((  void (*) (Queue_1_t3342152929 *, List_1_t1634065389 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7)->method)((Queue_1_t3342152929 *)L_20, (List_1_t1634065389 *)L_21, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7));
	}

IL_009b:
	{
		int32_t L_22 = V_1;
		V_1 = (int32_t)((int32_t)((int32_t)L_22+(int32_t)1));
	}

IL_009f:
	{
		int32_t L_23 = V_1;
		int32_t L_24 = V_0;
		if ((((int32_t)L_23) < ((int32_t)L_24)))
		{
			goto IL_0053;
		}
	}
	{
		return;
	}
}
// System.Void UniRx.Operators.BufferObservable`1/Buffer_<System.Object>::OnError(System.Exception)
extern "C"  void Buffer__OnError_m3391909677_gshared (Buffer__t1281669152 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method)
{
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		Il2CppObject* L_0 = (Il2CppObject*)((OperatorObserverBase_2_t3354260463 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		Exception_t1967233988 * L_1 = ___error0;
		NullCheck((Il2CppObject*)L_0);
		InterfaceActionInvoker1< Exception_t1967233988 * >::Invoke(1 /* System.Void UniRx.IObserver`1<System.Collections.Generic.IList`1<System.Object>>::OnError(System.Exception) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 12), (Il2CppObject*)L_0, (Exception_t1967233988 *)L_1);
		IL2CPP_LEAVE(0x1A, FINALLY_0013);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0013;
	}

FINALLY_0013:
	{ // begin finally (depth: 1)
		NullCheck((OperatorObserverBase_2_t3354260463 *)__this);
		VirtActionInvoker0::Invoke(4 /* System.Void UniRx.Operators.OperatorObserverBase`2<System.Object,System.Collections.Generic.IList`1<System.Object>>::Dispose() */, (OperatorObserverBase_2_t3354260463 *)__this);
		IL2CPP_END_FINALLY(19)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(19)
	{
		IL2CPP_JUMP_TBL(0x1A, IL_001a)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_001a:
	{
		return;
	}
}
// System.Void UniRx.Operators.BufferObservable`1/Buffer_<System.Object>::OnCompleted()
extern Il2CppClass* IDisposable_t1628921374_il2cpp_TypeInfo_var;
extern const uint32_t Buffer__OnCompleted_m2209267200_MetadataUsageId;
extern "C"  void Buffer__OnCompleted_m2209267200_gshared (Buffer__t1281669152 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Buffer__OnCompleted_m2209267200_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	List_1_t1634065389 * V_0 = NULL;
	Enumerator_t516807350  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Queue_1_t3342152929 * L_0 = (Queue_1_t3342152929 *)__this->get_q_3();
		NullCheck((Queue_1_t3342152929 *)L_0);
		Enumerator_t516807350  L_1 = ((  Enumerator_t516807350  (*) (Queue_1_t3342152929 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 14)->method)((Queue_1_t3342152929 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 14));
		V_1 = (Enumerator_t516807350 )L_1;
	}

IL_000c:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0027;
		}

IL_0011:
		{
			List_1_t1634065389 * L_2 = ((  List_1_t1634065389 * (*) (Enumerator_t516807350 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 15)->method)((Enumerator_t516807350 *)(&V_1), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 15));
			V_0 = (List_1_t1634065389 *)L_2;
			Il2CppObject* L_3 = (Il2CppObject*)((OperatorObserverBase_2_t3354260463 *)__this)->get_observer_0();
			il2cpp_codegen_memory_barrier();
			List_1_t1634065389 * L_4 = V_0;
			NullCheck((Il2CppObject*)L_3);
			InterfaceActionInvoker1< Il2CppObject* >::Invoke(2 /* System.Void UniRx.IObserver`1<System.Collections.Generic.IList`1<System.Object>>::OnNext(T) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 12), (Il2CppObject*)L_3, (Il2CppObject*)L_4);
		}

IL_0027:
		{
			bool L_5 = ((  bool (*) (Enumerator_t516807350 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 16)->method)((Enumerator_t516807350 *)(&V_1), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 16));
			if (L_5)
			{
				goto IL_0011;
			}
		}

IL_0033:
		{
			IL2CPP_LEAVE(0x44, FINALLY_0038);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0038;
	}

FINALLY_0038:
	{ // begin finally (depth: 1)
		Enumerator_t516807350  L_6 = V_1;
		Enumerator_t516807350  L_7 = L_6;
		Il2CppObject * L_8 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 17), &L_7);
		NullCheck((Il2CppObject *)L_8);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, (Il2CppObject *)L_8);
		IL2CPP_END_FINALLY(56)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(56)
	{
		IL2CPP_JUMP_TBL(0x44, IL_0044)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0044:
	try
	{ // begin try (depth: 1)
		Il2CppObject* L_9 = (Il2CppObject*)((OperatorObserverBase_2_t3354260463 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		NullCheck((Il2CppObject*)L_9);
		InterfaceActionInvoker0::Invoke(0 /* System.Void UniRx.IObserver`1<System.Collections.Generic.IList`1<System.Object>>::OnCompleted() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 12), (Il2CppObject*)L_9);
		IL2CPP_LEAVE(0x5D, FINALLY_0056);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0056;
	}

FINALLY_0056:
	{ // begin finally (depth: 1)
		NullCheck((OperatorObserverBase_2_t3354260463 *)__this);
		VirtActionInvoker0::Invoke(4 /* System.Void UniRx.Operators.OperatorObserverBase`2<System.Object,System.Collections.Generic.IList`1<System.Object>>::Dispose() */, (OperatorObserverBase_2_t3354260463 *)__this);
		IL2CPP_END_FINALLY(86)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(86)
	{
		IL2CPP_JUMP_TBL(0x5D, IL_005d)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_005d:
	{
		return;
	}
}
// System.Void UniRx.Operators.BufferObservable`1/Buffer<System.Object>::.ctor(UniRx.Operators.BufferObservable`1<T>,UniRx.IObserver`1<System.Collections.Generic.IList`1<T>>,System.IDisposable)
extern "C"  void Buffer__ctor_m594510395_gshared (Buffer_t492975321 * __this, BufferObservable_1_t574294898 * ___parent0, Il2CppObject* ___observer1, Il2CppObject * ___cancel2, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = ___observer1;
		Il2CppObject * L_1 = ___cancel2;
		NullCheck((OperatorObserverBase_2_t3354260463 *)__this);
		((  void (*) (OperatorObserverBase_2_t3354260463 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((OperatorObserverBase_2_t3354260463 *)__this, (Il2CppObject*)L_0, (Il2CppObject *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		BufferObservable_1_t574294898 * L_2 = ___parent0;
		__this->set_parent_2(L_2);
		return;
	}
}
// System.IDisposable UniRx.Operators.BufferObservable`1/Buffer<System.Object>::Run()
extern "C"  Il2CppObject * Buffer_Run_m1469555461_gshared (Buffer_t492975321 * __this, const MethodInfo* method)
{
	{
		BufferObservable_1_t574294898 * L_0 = (BufferObservable_1_t574294898 *)__this->get_parent_2();
		NullCheck(L_0);
		int32_t L_1 = (int32_t)L_0->get_count_2();
		List_1_t1634065389 * L_2 = (List_1_t1634065389 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		((  void (*) (List_1_t1634065389 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)(L_2, (int32_t)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		__this->set_list_3(L_2);
		BufferObservable_1_t574294898 * L_3 = (BufferObservable_1_t574294898 *)__this->get_parent_2();
		NullCheck(L_3);
		Il2CppObject* L_4 = (Il2CppObject*)L_3->get_source_1();
		NullCheck((Il2CppObject*)L_4);
		Il2CppObject * L_5 = InterfaceFuncInvoker1< Il2CppObject *, Il2CppObject* >::Invoke(0 /* System.IDisposable UniRx.IObservable`1<System.Object>::Subscribe(UniRx.IObserver`1<T>) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4), (Il2CppObject*)L_4, (Il2CppObject*)__this);
		return L_5;
	}
}
// System.Void UniRx.Operators.BufferObservable`1/Buffer<System.Object>::OnNext(T)
extern "C"  void Buffer_OnNext_m2687212703_gshared (Buffer_t492975321 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	{
		List_1_t1634065389 * L_0 = (List_1_t1634065389 *)__this->get_list_3();
		Il2CppObject * L_1 = ___value0;
		NullCheck((List_1_t1634065389 *)L_0);
		VirtActionInvoker1< Il2CppObject * >::Invoke(22 /* System.Void System.Collections.Generic.List`1<System.Object>::Add(!0) */, (List_1_t1634065389 *)L_0, (Il2CppObject *)L_1);
		List_1_t1634065389 * L_2 = (List_1_t1634065389 *)__this->get_list_3();
		NullCheck((List_1_t1634065389 *)L_2);
		int32_t L_3 = VirtFuncInvoker0< int32_t >::Invoke(20 /* System.Int32 System.Collections.Generic.List`1<System.Object>::get_Count() */, (List_1_t1634065389 *)L_2);
		BufferObservable_1_t574294898 * L_4 = (BufferObservable_1_t574294898 *)__this->get_parent_2();
		NullCheck(L_4);
		int32_t L_5 = (int32_t)L_4->get_count_2();
		if ((!(((uint32_t)L_3) == ((uint32_t)L_5))))
		{
			goto IL_0050;
		}
	}
	{
		Il2CppObject* L_6 = (Il2CppObject*)((OperatorObserverBase_2_t3354260463 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		List_1_t1634065389 * L_7 = (List_1_t1634065389 *)__this->get_list_3();
		NullCheck((Il2CppObject*)L_6);
		InterfaceActionInvoker1< Il2CppObject* >::Invoke(2 /* System.Void UniRx.IObserver`1<System.Collections.Generic.IList`1<System.Object>>::OnNext(T) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7), (Il2CppObject*)L_6, (Il2CppObject*)L_7);
		BufferObservable_1_t574294898 * L_8 = (BufferObservable_1_t574294898 *)__this->get_parent_2();
		NullCheck(L_8);
		int32_t L_9 = (int32_t)L_8->get_count_2();
		List_1_t1634065389 * L_10 = (List_1_t1634065389 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		((  void (*) (List_1_t1634065389 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)(L_10, (int32_t)L_9, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		__this->set_list_3(L_10);
	}

IL_0050:
	{
		return;
	}
}
// System.Void UniRx.Operators.BufferObservable`1/Buffer<System.Object>::OnError(System.Exception)
extern "C"  void Buffer_OnError_m1916311982_gshared (Buffer_t492975321 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method)
{
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		Il2CppObject* L_0 = (Il2CppObject*)((OperatorObserverBase_2_t3354260463 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		Exception_t1967233988 * L_1 = ___error0;
		NullCheck((Il2CppObject*)L_0);
		InterfaceActionInvoker1< Exception_t1967233988 * >::Invoke(1 /* System.Void UniRx.IObserver`1<System.Collections.Generic.IList`1<System.Object>>::OnError(System.Exception) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7), (Il2CppObject*)L_0, (Exception_t1967233988 *)L_1);
		IL2CPP_LEAVE(0x1A, FINALLY_0013);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0013;
	}

FINALLY_0013:
	{ // begin finally (depth: 1)
		NullCheck((OperatorObserverBase_2_t3354260463 *)__this);
		VirtActionInvoker0::Invoke(4 /* System.Void UniRx.Operators.OperatorObserverBase`2<System.Object,System.Collections.Generic.IList`1<System.Object>>::Dispose() */, (OperatorObserverBase_2_t3354260463 *)__this);
		IL2CPP_END_FINALLY(19)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(19)
	{
		IL2CPP_JUMP_TBL(0x1A, IL_001a)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_001a:
	{
		return;
	}
}
// System.Void UniRx.Operators.BufferObservable`1/Buffer<System.Object>::OnCompleted()
extern "C"  void Buffer_OnCompleted_m2261639681_gshared (Buffer_t492975321 * __this, const MethodInfo* method)
{
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		List_1_t1634065389 * L_0 = (List_1_t1634065389 *)__this->get_list_3();
		NullCheck((List_1_t1634065389 *)L_0);
		int32_t L_1 = VirtFuncInvoker0< int32_t >::Invoke(20 /* System.Int32 System.Collections.Generic.List`1<System.Object>::get_Count() */, (List_1_t1634065389 *)L_0);
		if ((((int32_t)L_1) <= ((int32_t)0)))
		{
			goto IL_0024;
		}
	}
	{
		Il2CppObject* L_2 = (Il2CppObject*)((OperatorObserverBase_2_t3354260463 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		List_1_t1634065389 * L_3 = (List_1_t1634065389 *)__this->get_list_3();
		NullCheck((Il2CppObject*)L_2);
		InterfaceActionInvoker1< Il2CppObject* >::Invoke(2 /* System.Void UniRx.IObserver`1<System.Collections.Generic.IList`1<System.Object>>::OnNext(T) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7), (Il2CppObject*)L_2, (Il2CppObject*)L_3);
	}

IL_0024:
	try
	{ // begin try (depth: 1)
		Il2CppObject* L_4 = (Il2CppObject*)((OperatorObserverBase_2_t3354260463 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		NullCheck((Il2CppObject*)L_4);
		InterfaceActionInvoker0::Invoke(0 /* System.Void UniRx.IObserver`1<System.Collections.Generic.IList`1<System.Object>>::OnCompleted() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7), (Il2CppObject*)L_4);
		IL2CPP_LEAVE(0x3D, FINALLY_0036);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0036;
	}

FINALLY_0036:
	{ // begin finally (depth: 1)
		NullCheck((OperatorObserverBase_2_t3354260463 *)__this);
		VirtActionInvoker0::Invoke(4 /* System.Void UniRx.Operators.OperatorObserverBase`2<System.Object,System.Collections.Generic.IList`1<System.Object>>::Dispose() */, (OperatorObserverBase_2_t3354260463 *)__this);
		IL2CPP_END_FINALLY(54)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(54)
	{
		IL2CPP_JUMP_TBL(0x3D, IL_003d)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_003d:
	{
		return;
	}
}
// System.Void UniRx.Operators.BufferObservable`1/BufferT/Buffer<System.Object>::.ctor(UniRx.Operators.BufferObservable`1/BufferT<T>)
extern "C"  void Buffer__ctor_m2214677866_gshared (Buffer_t492975322 * __this, BufferT_t129222477 * ___parent0, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		BufferT_t129222477 * L_0 = ___parent0;
		__this->set_parent_0(L_0);
		return;
	}
}
// System.Void UniRx.Operators.BufferObservable`1/BufferT/Buffer<System.Object>::OnNext(System.Int64)
extern "C"  void Buffer_OnNext_m3663740246_gshared (Buffer_t492975322 * __this, int64_t ___value0, const MethodInfo* method)
{
	bool V_0 = false;
	List_1_t1634065389 * V_1 = NULL;
	Il2CppObject * V_2 = NULL;
	Il2CppObject* V_3 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	Il2CppObject* G_B8_0 = NULL;
	Il2CppObject* G_B7_0 = NULL;
	Il2CppObject* G_B9_0 = NULL;
	Il2CppObject* G_B9_1 = NULL;
	{
		V_0 = (bool)0;
		BufferT_t129222477 * L_0 = (BufferT_t129222477 *)__this->get_parent_0();
		NullCheck(L_0);
		Il2CppObject * L_1 = (Il2CppObject *)L_0->get_gate_4();
		V_2 = (Il2CppObject *)L_1;
		Il2CppObject * L_2 = V_2;
		Monitor_Enter_m476686225(NULL /*static, unused*/, (Il2CppObject *)L_2, /*hidden argument*/NULL);
	}

IL_0014:
	try
	{ // begin try (depth: 1)
		{
			BufferT_t129222477 * L_3 = (BufferT_t129222477 *)__this->get_parent_0();
			NullCheck(L_3);
			List_1_t1634065389 * L_4 = (List_1_t1634065389 *)L_3->get_list_5();
			V_1 = (List_1_t1634065389 *)L_4;
			List_1_t1634065389 * L_5 = V_1;
			NullCheck((List_1_t1634065389 *)L_5);
			int32_t L_6 = VirtFuncInvoker0< int32_t >::Invoke(20 /* System.Int32 System.Collections.Generic.List`1<System.Object>::get_Count() */, (List_1_t1634065389 *)L_5);
			if (!L_6)
			{
				goto IL_0040;
			}
		}

IL_002b:
		{
			BufferT_t129222477 * L_7 = (BufferT_t129222477 *)__this->get_parent_0();
			List_1_t1634065389 * L_8 = (List_1_t1634065389 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
			((  void (*) (List_1_t1634065389 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)(L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
			NullCheck(L_7);
			L_7->set_list_5(L_8);
			goto IL_0042;
		}

IL_0040:
		{
			V_0 = (bool)1;
		}

IL_0042:
		{
			IL2CPP_LEAVE(0x4E, FINALLY_0047);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0047;
	}

FINALLY_0047:
	{ // begin finally (depth: 1)
		Il2CppObject * L_9 = V_2;
		Monitor_Exit_m2088237919(NULL /*static, unused*/, (Il2CppObject *)L_9, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(71)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(71)
	{
		IL2CPP_JUMP_TBL(0x4E, IL_004e)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_004e:
	{
		BufferT_t129222477 * L_10 = (BufferT_t129222477 *)__this->get_parent_0();
		NullCheck(L_10);
		Il2CppObject* L_11 = (Il2CppObject*)((OperatorObserverBase_2_t3354260463 *)L_10)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		bool L_12 = V_0;
		G_B7_0 = L_11;
		if (!L_12)
		{
			G_B8_0 = L_11;
			goto IL_006d;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		ObjectU5BU5D_t11523773* L_13 = ((BufferT_t129222477_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->static_fields)->get_EmptyArray_2();
		V_3 = (Il2CppObject*)L_13;
		Il2CppObject* L_14 = V_3;
		G_B9_0 = L_14;
		G_B9_1 = G_B7_0;
		goto IL_006e;
	}

IL_006d:
	{
		List_1_t1634065389 * L_15 = V_1;
		G_B9_0 = ((Il2CppObject*)(L_15));
		G_B9_1 = G_B8_0;
	}

IL_006e:
	{
		NullCheck((Il2CppObject*)G_B9_1);
		InterfaceActionInvoker1< Il2CppObject* >::Invoke(2 /* System.Void UniRx.IObserver`1<System.Collections.Generic.IList`1<System.Object>>::OnNext(T) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5), (Il2CppObject*)G_B9_1, (Il2CppObject*)G_B9_0);
		return;
	}
}
// System.Void UniRx.Operators.BufferObservable`1/BufferT/Buffer<System.Object>::OnError(System.Exception)
extern "C"  void Buffer_OnError_m613702249_gshared (Buffer_t492975322 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void UniRx.Operators.BufferObservable`1/BufferT/Buffer<System.Object>::OnCompleted()
extern "C"  void Buffer_OnCompleted_m1276158780_gshared (Buffer_t492975322 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void UniRx.Operators.BufferObservable`1/BufferT<System.Object>::.ctor(UniRx.Operators.BufferObservable`1<T>,UniRx.IObserver`1<System.Collections.Generic.IList`1<T>>,System.IDisposable)
extern Il2CppClass* Il2CppObject_il2cpp_TypeInfo_var;
extern const uint32_t BufferT__ctor_m1023256711_MetadataUsageId;
extern "C"  void BufferT__ctor_m1023256711_gshared (BufferT_t129222477 * __this, BufferObservable_1_t574294898 * ___parent0, Il2CppObject* ___observer1, Il2CppObject * ___cancel2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (BufferT__ctor_m1023256711_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject * L_0 = (Il2CppObject *)il2cpp_codegen_object_new(Il2CppObject_il2cpp_TypeInfo_var);
		Object__ctor_m1772956182(L_0, /*hidden argument*/NULL);
		__this->set_gate_4(L_0);
		Il2CppObject* L_1 = ___observer1;
		Il2CppObject * L_2 = ___cancel2;
		NullCheck((OperatorObserverBase_2_t3354260463 *)__this);
		((  void (*) (OperatorObserverBase_2_t3354260463 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((OperatorObserverBase_2_t3354260463 *)__this, (Il2CppObject*)L_1, (Il2CppObject *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		BufferObservable_1_t574294898 * L_3 = ___parent0;
		__this->set_parent_3(L_3);
		return;
	}
}
// System.Void UniRx.Operators.BufferObservable`1/BufferT<System.Object>::.cctor()
extern "C"  void BufferT__cctor_m1244560674_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		((BufferT_t129222477_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->static_fields)->set_EmptyArray_2(((ObjectU5BU5D_t11523773*)SZArrayNew(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), (uint32_t)0)));
		return;
	}
}
// System.IDisposable UniRx.Operators.BufferObservable`1/BufferT<System.Object>::Run()
extern Il2CppClass* Observable_t258071701_il2cpp_TypeInfo_var;
extern Il2CppClass* IObservable_1_t2606213246_il2cpp_TypeInfo_var;
extern const uint32_t BufferT_Run_m3274670607_MetadataUsageId;
extern "C"  Il2CppObject * BufferT_Run_m3274670607_gshared (BufferT_t129222477 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (BufferT_Run_m3274670607_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Il2CppObject * V_0 = NULL;
	Il2CppObject * V_1 = NULL;
	{
		List_1_t1634065389 * L_0 = (List_1_t1634065389 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		((  void (*) (List_1_t1634065389 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->method)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		__this->set_list_5(L_0);
		BufferObservable_1_t574294898 * L_1 = (BufferObservable_1_t574294898 *)__this->get_parent_3();
		NullCheck(L_1);
		TimeSpan_t763862892  L_2 = (TimeSpan_t763862892 )L_1->get_timeSpan_4();
		BufferObservable_1_t574294898 * L_3 = (BufferObservable_1_t574294898 *)__this->get_parent_3();
		NullCheck(L_3);
		Il2CppObject * L_4 = (Il2CppObject *)L_3->get_scheduler_6();
		IL2CPP_RUNTIME_CLASS_INIT(Observable_t258071701_il2cpp_TypeInfo_var);
		Il2CppObject* L_5 = Observable_Interval_m3597074831(NULL /*static, unused*/, (TimeSpan_t763862892 )L_2, (Il2CppObject *)L_4, /*hidden argument*/NULL);
		Buffer_t492975322 * L_6 = (Buffer_t492975322 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		((  void (*) (Buffer_t492975322 *, BufferT_t129222477 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7)->method)(L_6, (BufferT_t129222477 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7));
		NullCheck((Il2CppObject*)L_5);
		Il2CppObject * L_7 = InterfaceFuncInvoker1< Il2CppObject *, Il2CppObject* >::Invoke(0 /* System.IDisposable UniRx.IObservable`1<System.Int64>::Subscribe(UniRx.IObserver`1<T>) */, IObservable_1_t2606213246_il2cpp_TypeInfo_var, (Il2CppObject*)L_5, (Il2CppObject*)L_6);
		V_0 = (Il2CppObject *)L_7;
		BufferObservable_1_t574294898 * L_8 = (BufferObservable_1_t574294898 *)__this->get_parent_3();
		NullCheck(L_8);
		Il2CppObject* L_9 = (Il2CppObject*)L_8->get_source_1();
		NullCheck((Il2CppObject*)L_9);
		Il2CppObject * L_10 = InterfaceFuncInvoker1< Il2CppObject *, Il2CppObject* >::Invoke(0 /* System.IDisposable UniRx.IObservable`1<System.Object>::Subscribe(UniRx.IObserver`1<T>) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8), (Il2CppObject*)L_9, (Il2CppObject*)__this);
		V_1 = (Il2CppObject *)L_10;
		Il2CppObject * L_11 = V_0;
		Il2CppObject * L_12 = V_1;
		Il2CppObject * L_13 = StableCompositeDisposable_Create_m3764788535(NULL /*static, unused*/, (Il2CppObject *)L_11, (Il2CppObject *)L_12, /*hidden argument*/NULL);
		return L_13;
	}
}
// System.Void UniRx.Operators.BufferObservable`1/BufferT<System.Object>::OnNext(T)
extern "C"  void BufferT_OnNext_m522491859_gshared (BufferT_t129222477 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	Il2CppObject * V_0 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_gate_4();
		V_0 = (Il2CppObject *)L_0;
		Il2CppObject * L_1 = V_0;
		Monitor_Enter_m476686225(NULL /*static, unused*/, (Il2CppObject *)L_1, /*hidden argument*/NULL);
	}

IL_000d:
	try
	{ // begin try (depth: 1)
		List_1_t1634065389 * L_2 = (List_1_t1634065389 *)__this->get_list_5();
		Il2CppObject * L_3 = ___value0;
		NullCheck((List_1_t1634065389 *)L_2);
		VirtActionInvoker1< Il2CppObject * >::Invoke(22 /* System.Void System.Collections.Generic.List`1<System.Object>::Add(!0) */, (List_1_t1634065389 *)L_2, (Il2CppObject *)L_3);
		IL2CPP_LEAVE(0x25, FINALLY_001e);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_001e;
	}

FINALLY_001e:
	{ // begin finally (depth: 1)
		Il2CppObject * L_4 = V_0;
		Monitor_Exit_m2088237919(NULL /*static, unused*/, (Il2CppObject *)L_4, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(30)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(30)
	{
		IL2CPP_JUMP_TBL(0x25, IL_0025)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0025:
	{
		return;
	}
}
// System.Void UniRx.Operators.BufferObservable`1/BufferT<System.Object>::OnError(System.Exception)
extern "C"  void BufferT_OnError_m4032605410_gshared (BufferT_t129222477 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method)
{
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		Il2CppObject* L_0 = (Il2CppObject*)((OperatorObserverBase_2_t3354260463 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		Exception_t1967233988 * L_1 = ___error0;
		NullCheck((Il2CppObject*)L_0);
		InterfaceActionInvoker1< Exception_t1967233988 * >::Invoke(1 /* System.Void UniRx.IObserver`1<System.Collections.Generic.IList`1<System.Object>>::OnError(System.Exception) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10), (Il2CppObject*)L_0, (Exception_t1967233988 *)L_1);
		IL2CPP_LEAVE(0x1A, FINALLY_0013);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0013;
	}

FINALLY_0013:
	{ // begin finally (depth: 1)
		NullCheck((OperatorObserverBase_2_t3354260463 *)__this);
		VirtActionInvoker0::Invoke(4 /* System.Void UniRx.Operators.OperatorObserverBase`2<System.Object,System.Collections.Generic.IList`1<System.Object>>::Dispose() */, (OperatorObserverBase_2_t3354260463 *)__this);
		IL2CPP_END_FINALLY(19)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(19)
	{
		IL2CPP_JUMP_TBL(0x1A, IL_001a)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_001a:
	{
		return;
	}
}
// System.Void UniRx.Operators.BufferObservable`1/BufferT<System.Object>::OnCompleted()
extern "C"  void BufferT_OnCompleted_m2645435189_gshared (BufferT_t129222477 * __this, const MethodInfo* method)
{
	List_1_t1634065389 * V_0 = NULL;
	Il2CppObject * V_1 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_gate_4();
		V_1 = (Il2CppObject *)L_0;
		Il2CppObject * L_1 = V_1;
		Monitor_Enter_m476686225(NULL /*static, unused*/, (Il2CppObject *)L_1, /*hidden argument*/NULL);
	}

IL_000d:
	try
	{ // begin try (depth: 1)
		List_1_t1634065389 * L_2 = (List_1_t1634065389 *)__this->get_list_5();
		V_0 = (List_1_t1634065389 *)L_2;
		IL2CPP_LEAVE(0x20, FINALLY_0019);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0019;
	}

FINALLY_0019:
	{ // begin finally (depth: 1)
		Il2CppObject * L_3 = V_1;
		Monitor_Exit_m2088237919(NULL /*static, unused*/, (Il2CppObject *)L_3, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(25)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(25)
	{
		IL2CPP_JUMP_TBL(0x20, IL_0020)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0020:
	{
		Il2CppObject* L_4 = (Il2CppObject*)((OperatorObserverBase_2_t3354260463 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		List_1_t1634065389 * L_5 = V_0;
		NullCheck((Il2CppObject*)L_4);
		InterfaceActionInvoker1< Il2CppObject* >::Invoke(2 /* System.Void UniRx.IObserver`1<System.Collections.Generic.IList`1<System.Object>>::OnNext(T) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10), (Il2CppObject*)L_4, (Il2CppObject*)L_5);
	}

IL_002e:
	try
	{ // begin try (depth: 1)
		Il2CppObject* L_6 = (Il2CppObject*)((OperatorObserverBase_2_t3354260463 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		NullCheck((Il2CppObject*)L_6);
		InterfaceActionInvoker0::Invoke(0 /* System.Void UniRx.IObserver`1<System.Collections.Generic.IList`1<System.Object>>::OnCompleted() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10), (Il2CppObject*)L_6);
		IL2CPP_LEAVE(0x47, FINALLY_0040);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0040;
	}

FINALLY_0040:
	{ // begin finally (depth: 1)
		NullCheck((OperatorObserverBase_2_t3354260463 *)__this);
		VirtActionInvoker0::Invoke(4 /* System.Void UniRx.Operators.OperatorObserverBase`2<System.Object,System.Collections.Generic.IList`1<System.Object>>::Dispose() */, (OperatorObserverBase_2_t3354260463 *)__this);
		IL2CPP_END_FINALLY(64)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(64)
	{
		IL2CPP_JUMP_TBL(0x47, IL_0047)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0047:
	{
		return;
	}
}
// System.Void UniRx.Operators.BufferObservable`1/BufferTC/<CreateTimer>c__AnonStorey5D<System.Object>::.ctor()
extern "C"  void U3CCreateTimerU3Ec__AnonStorey5D__ctor_m1070085756_gshared (U3CCreateTimerU3Ec__AnonStorey5D_t162307190 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UniRx.Operators.BufferObservable`1/BufferTC/<CreateTimer>c__AnonStorey5D<System.Object>::<>m__75()
extern "C"  void U3CCreateTimerU3Ec__AnonStorey5D_U3CU3Em__75_m4163451299_gshared (U3CCreateTimerU3Ec__AnonStorey5D_t162307190 * __this, const MethodInfo* method)
{
	{
		BufferTC_t3470796400 * L_0 = (BufferTC_t3470796400 *)__this->get_U3CU3Ef__this_1();
		int64_t L_1 = (int64_t)__this->get_currentTimerId_0();
		NullCheck((BufferTC_t3470796400 *)L_0);
		((  void (*) (BufferTC_t3470796400 *, int64_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((BufferTC_t3470796400 *)L_0, (int64_t)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		return;
	}
}
// System.Void UniRx.Operators.BufferObservable`1/BufferTC/<CreateTimer>c__AnonStorey5D<System.Object>::<>m__76(System.Action`1<System.TimeSpan>)
extern "C"  void U3CCreateTimerU3Ec__AnonStorey5D_U3CU3Em__76_m4291568422_gshared (U3CCreateTimerU3Ec__AnonStorey5D_t162307190 * __this, Action_1_t912315597 * ___self0, const MethodInfo* method)
{
	{
		BufferTC_t3470796400 * L_0 = (BufferTC_t3470796400 *)__this->get_U3CU3Ef__this_1();
		int64_t L_1 = (int64_t)__this->get_currentTimerId_0();
		Action_1_t912315597 * L_2 = ___self0;
		NullCheck((BufferTC_t3470796400 *)L_0);
		((  void (*) (BufferTC_t3470796400 *, int64_t, Action_1_t912315597 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)((BufferTC_t3470796400 *)L_0, (int64_t)L_1, (Action_1_t912315597 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		return;
	}
}
// System.Void UniRx.Operators.BufferObservable`1/BufferTC<System.Object>::.ctor(UniRx.Operators.BufferObservable`1<T>,UniRx.IObserver`1<System.Collections.Generic.IList`1<T>>,System.IDisposable)
extern Il2CppClass* Il2CppObject_il2cpp_TypeInfo_var;
extern const uint32_t BufferTC__ctor_m257145004_MetadataUsageId;
extern "C"  void BufferTC__ctor_m257145004_gshared (BufferTC_t3470796400 * __this, BufferObservable_1_t574294898 * ___parent0, Il2CppObject* ___observer1, Il2CppObject * ___cancel2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (BufferTC__ctor_m257145004_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject * L_0 = (Il2CppObject *)il2cpp_codegen_object_new(Il2CppObject_il2cpp_TypeInfo_var);
		Object__ctor_m1772956182(L_0, /*hidden argument*/NULL);
		__this->set_gate_4(L_0);
		Il2CppObject* L_1 = ___observer1;
		Il2CppObject * L_2 = ___cancel2;
		NullCheck((OperatorObserverBase_2_t3354260463 *)__this);
		((  void (*) (OperatorObserverBase_2_t3354260463 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((OperatorObserverBase_2_t3354260463 *)__this, (Il2CppObject*)L_1, (Il2CppObject *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		BufferObservable_1_t574294898 * L_3 = ___parent0;
		__this->set_parent_3(L_3);
		return;
	}
}
// System.Void UniRx.Operators.BufferObservable`1/BufferTC<System.Object>::.cctor()
extern "C"  void BufferTC__cctor_m2847470407_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		((BufferTC_t3470796400_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->static_fields)->set_EmptyArray_2(((ObjectU5BU5D_t11523773*)SZArrayNew(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), (uint32_t)0)));
		return;
	}
}
// System.IDisposable UniRx.Operators.BufferObservable`1/BufferTC<System.Object>::Run()
extern Il2CppClass* SerialDisposable_t2547852742_il2cpp_TypeInfo_var;
extern const uint32_t BufferTC_Run_m3712505588_MetadataUsageId;
extern "C"  Il2CppObject * BufferTC_Run_m3712505588_gshared (BufferTC_t3470796400 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (BufferTC_Run_m3712505588_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Il2CppObject * V_0 = NULL;
	{
		List_1_t1634065389 * L_0 = (List_1_t1634065389 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		((  void (*) (List_1_t1634065389 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->method)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		__this->set_list_5(L_0);
		__this->set_timerId_6((((int64_t)((int64_t)0))));
		SerialDisposable_t2547852742 * L_1 = (SerialDisposable_t2547852742 *)il2cpp_codegen_object_new(SerialDisposable_t2547852742_il2cpp_TypeInfo_var);
		SerialDisposable__ctor_m2977852291(L_1, /*hidden argument*/NULL);
		__this->set_timerD_7(L_1);
		NullCheck((BufferTC_t3470796400 *)__this);
		((  void (*) (BufferTC_t3470796400 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6)->method)((BufferTC_t3470796400 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		BufferObservable_1_t574294898 * L_2 = (BufferObservable_1_t574294898 *)__this->get_parent_3();
		NullCheck(L_2);
		Il2CppObject* L_3 = (Il2CppObject*)L_2->get_source_1();
		NullCheck((Il2CppObject*)L_3);
		Il2CppObject * L_4 = InterfaceFuncInvoker1< Il2CppObject *, Il2CppObject* >::Invoke(0 /* System.IDisposable UniRx.IObservable`1<System.Object>::Subscribe(UniRx.IObserver`1<T>) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7), (Il2CppObject*)L_3, (Il2CppObject*)__this);
		V_0 = (Il2CppObject *)L_4;
		Il2CppObject * L_5 = V_0;
		SerialDisposable_t2547852742 * L_6 = (SerialDisposable_t2547852742 *)__this->get_timerD_7();
		Il2CppObject * L_7 = StableCompositeDisposable_Create_m3764788535(NULL /*static, unused*/, (Il2CppObject *)L_5, (Il2CppObject *)L_6, /*hidden argument*/NULL);
		return L_7;
	}
}
// System.Void UniRx.Operators.BufferObservable`1/BufferTC<System.Object>::CreateTimer()
extern Il2CppClass* SingleAssignmentDisposable_t2336378823_il2cpp_TypeInfo_var;
extern Il2CppClass* ISchedulerPeriodic_t2870089311_il2cpp_TypeInfo_var;
extern Il2CppClass* Action_t437523947_il2cpp_TypeInfo_var;
extern Il2CppClass* Action_1_t1060768302_il2cpp_TypeInfo_var;
extern Il2CppClass* Scheduler_t103934925_il2cpp_TypeInfo_var;
extern const MethodInfo* Action_1__ctor_m3957693621_MethodInfo_var;
extern const uint32_t BufferTC_CreateTimer_m40636781_MetadataUsageId;
extern "C"  void BufferTC_CreateTimer_m40636781_gshared (BufferTC_t3470796400 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (BufferTC_CreateTimer_m40636781_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	SingleAssignmentDisposable_t2336378823 * V_0 = NULL;
	Il2CppObject * V_1 = NULL;
	U3CCreateTimerU3Ec__AnonStorey5D_t162307190 * V_2 = NULL;
	{
		U3CCreateTimerU3Ec__AnonStorey5D_t162307190 * L_0 = (U3CCreateTimerU3Ec__AnonStorey5D_t162307190 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8));
		((  void (*) (U3CCreateTimerU3Ec__AnonStorey5D_t162307190 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9)->method)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9));
		V_2 = (U3CCreateTimerU3Ec__AnonStorey5D_t162307190 *)L_0;
		U3CCreateTimerU3Ec__AnonStorey5D_t162307190 * L_1 = V_2;
		NullCheck(L_1);
		L_1->set_U3CU3Ef__this_1(__this);
		U3CCreateTimerU3Ec__AnonStorey5D_t162307190 * L_2 = V_2;
		int64_t L_3 = (int64_t)__this->get_timerId_6();
		NullCheck(L_2);
		L_2->set_currentTimerId_0(L_3);
		SingleAssignmentDisposable_t2336378823 * L_4 = (SingleAssignmentDisposable_t2336378823 *)il2cpp_codegen_object_new(SingleAssignmentDisposable_t2336378823_il2cpp_TypeInfo_var);
		SingleAssignmentDisposable__ctor_m917449122(L_4, /*hidden argument*/NULL);
		V_0 = (SingleAssignmentDisposable_t2336378823 *)L_4;
		SerialDisposable_t2547852742 * L_5 = (SerialDisposable_t2547852742 *)__this->get_timerD_7();
		SingleAssignmentDisposable_t2336378823 * L_6 = V_0;
		NullCheck((SerialDisposable_t2547852742 *)L_5);
		SerialDisposable_set_Disposable_m1438046228((SerialDisposable_t2547852742 *)L_5, (Il2CppObject *)L_6, /*hidden argument*/NULL);
		BufferObservable_1_t574294898 * L_7 = (BufferObservable_1_t574294898 *)__this->get_parent_3();
		NullCheck(L_7);
		Il2CppObject * L_8 = (Il2CppObject *)L_7->get_scheduler_6();
		V_1 = (Il2CppObject *)((Il2CppObject *)IsInst(L_8, ISchedulerPeriodic_t2870089311_il2cpp_TypeInfo_var));
		Il2CppObject * L_9 = V_1;
		if (!L_9)
		{
			goto IL_006a;
		}
	}
	{
		SingleAssignmentDisposable_t2336378823 * L_10 = V_0;
		Il2CppObject * L_11 = V_1;
		BufferObservable_1_t574294898 * L_12 = (BufferObservable_1_t574294898 *)__this->get_parent_3();
		NullCheck(L_12);
		TimeSpan_t763862892  L_13 = (TimeSpan_t763862892 )L_12->get_timeSpan_4();
		U3CCreateTimerU3Ec__AnonStorey5D_t162307190 * L_14 = V_2;
		IntPtr_t L_15;
		L_15.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10));
		Action_t437523947 * L_16 = (Action_t437523947 *)il2cpp_codegen_object_new(Action_t437523947_il2cpp_TypeInfo_var);
		Action__ctor_m2957240604(L_16, (Il2CppObject *)L_14, (IntPtr_t)L_15, /*hidden argument*/NULL);
		NullCheck((Il2CppObject *)L_11);
		Il2CppObject * L_17 = InterfaceFuncInvoker2< Il2CppObject *, TimeSpan_t763862892 , Action_t437523947 * >::Invoke(0 /* System.IDisposable UniRx.ISchedulerPeriodic::SchedulePeriodic(System.TimeSpan,System.Action) */, ISchedulerPeriodic_t2870089311_il2cpp_TypeInfo_var, (Il2CppObject *)L_11, (TimeSpan_t763862892 )L_13, (Action_t437523947 *)L_16);
		NullCheck((SingleAssignmentDisposable_t2336378823 *)L_10);
		SingleAssignmentDisposable_set_Disposable_m1599028885((SingleAssignmentDisposable_t2336378823 *)L_10, (Il2CppObject *)L_17, /*hidden argument*/NULL);
		goto IL_0097;
	}

IL_006a:
	{
		SingleAssignmentDisposable_t2336378823 * L_18 = V_0;
		BufferObservable_1_t574294898 * L_19 = (BufferObservable_1_t574294898 *)__this->get_parent_3();
		NullCheck(L_19);
		Il2CppObject * L_20 = (Il2CppObject *)L_19->get_scheduler_6();
		BufferObservable_1_t574294898 * L_21 = (BufferObservable_1_t574294898 *)__this->get_parent_3();
		NullCheck(L_21);
		TimeSpan_t763862892  L_22 = (TimeSpan_t763862892 )L_21->get_timeSpan_4();
		U3CCreateTimerU3Ec__AnonStorey5D_t162307190 * L_23 = V_2;
		IntPtr_t L_24;
		L_24.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 11));
		Action_1_t1060768302 * L_25 = (Action_1_t1060768302 *)il2cpp_codegen_object_new(Action_1_t1060768302_il2cpp_TypeInfo_var);
		Action_1__ctor_m3957693621(L_25, (Il2CppObject *)L_23, (IntPtr_t)L_24, /*hidden argument*/Action_1__ctor_m3957693621_MethodInfo_var);
		IL2CPP_RUNTIME_CLASS_INIT(Scheduler_t103934925_il2cpp_TypeInfo_var);
		Il2CppObject * L_26 = Scheduler_Schedule_m279164218(NULL /*static, unused*/, (Il2CppObject *)L_20, (TimeSpan_t763862892 )L_22, (Action_1_t1060768302 *)L_25, /*hidden argument*/NULL);
		NullCheck((SingleAssignmentDisposable_t2336378823 *)L_18);
		SingleAssignmentDisposable_set_Disposable_m1599028885((SingleAssignmentDisposable_t2336378823 *)L_18, (Il2CppObject *)L_26, /*hidden argument*/NULL);
	}

IL_0097:
	{
		return;
	}
}
// System.Void UniRx.Operators.BufferObservable`1/BufferTC<System.Object>::OnNextTick(System.Int64)
extern "C"  void BufferTC_OnNextTick_m3470066015_gshared (BufferTC_t3470796400 * __this, int64_t ___currentTimerId0, const MethodInfo* method)
{
	bool V_0 = false;
	List_1_t1634065389 * V_1 = NULL;
	Il2CppObject * V_2 = NULL;
	Il2CppObject* V_3 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	Il2CppObject* G_B10_0 = NULL;
	Il2CppObject* G_B9_0 = NULL;
	Il2CppObject* G_B11_0 = NULL;
	Il2CppObject* G_B11_1 = NULL;
	{
		V_0 = (bool)0;
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_gate_4();
		V_2 = (Il2CppObject *)L_0;
		Il2CppObject * L_1 = V_2;
		Monitor_Enter_m476686225(NULL /*static, unused*/, (Il2CppObject *)L_1, /*hidden argument*/NULL);
	}

IL_000f:
	try
	{ // begin try (depth: 1)
		{
			int64_t L_2 = ___currentTimerId0;
			int64_t L_3 = (int64_t)__this->get_timerId_6();
			if ((((int64_t)L_2) == ((int64_t)L_3)))
			{
				goto IL_0020;
			}
		}

IL_001b:
		{
			IL2CPP_LEAVE(0x70, FINALLY_0049);
		}

IL_0020:
		{
			List_1_t1634065389 * L_4 = (List_1_t1634065389 *)__this->get_list_5();
			V_1 = (List_1_t1634065389 *)L_4;
			List_1_t1634065389 * L_5 = V_1;
			NullCheck((List_1_t1634065389 *)L_5);
			int32_t L_6 = VirtFuncInvoker0< int32_t >::Invoke(20 /* System.Int32 System.Collections.Generic.List`1<System.Object>::get_Count() */, (List_1_t1634065389 *)L_5);
			if (!L_6)
			{
				goto IL_0042;
			}
		}

IL_0032:
		{
			List_1_t1634065389 * L_7 = (List_1_t1634065389 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
			((  void (*) (List_1_t1634065389 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->method)(L_7, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
			__this->set_list_5(L_7);
			goto IL_0044;
		}

IL_0042:
		{
			V_0 = (bool)1;
		}

IL_0044:
		{
			IL2CPP_LEAVE(0x50, FINALLY_0049);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0049;
	}

FINALLY_0049:
	{ // begin finally (depth: 1)
		Il2CppObject * L_8 = V_2;
		Monitor_Exit_m2088237919(NULL /*static, unused*/, (Il2CppObject *)L_8, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(73)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(73)
	{
		IL2CPP_JUMP_TBL(0x70, IL_0070)
		IL2CPP_JUMP_TBL(0x50, IL_0050)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0050:
	{
		Il2CppObject* L_9 = (Il2CppObject*)((OperatorObserverBase_2_t3354260463 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		bool L_10 = V_0;
		G_B9_0 = L_9;
		if (!L_10)
		{
			G_B10_0 = L_9;
			goto IL_006a;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		ObjectU5BU5D_t11523773* L_11 = ((BufferTC_t3470796400_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->static_fields)->get_EmptyArray_2();
		V_3 = (Il2CppObject*)L_11;
		Il2CppObject* L_12 = V_3;
		G_B11_0 = L_12;
		G_B11_1 = G_B9_0;
		goto IL_006b;
	}

IL_006a:
	{
		List_1_t1634065389 * L_13 = V_1;
		G_B11_0 = ((Il2CppObject*)(L_13));
		G_B11_1 = G_B10_0;
	}

IL_006b:
	{
		NullCheck((Il2CppObject*)G_B11_1);
		InterfaceActionInvoker1< Il2CppObject* >::Invoke(2 /* System.Void UniRx.IObserver`1<System.Collections.Generic.IList`1<System.Object>>::OnNext(T) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 13), (Il2CppObject*)G_B11_1, (Il2CppObject*)G_B11_0);
	}

IL_0070:
	{
		return;
	}
}
// System.Void UniRx.Operators.BufferObservable`1/BufferTC<System.Object>::OnNextRecursive(System.Int64,System.Action`1<System.TimeSpan>)
extern const MethodInfo* Action_1_Invoke_m613956924_MethodInfo_var;
extern const uint32_t BufferTC_OnNextRecursive_m2259042514_MetadataUsageId;
extern "C"  void BufferTC_OnNextRecursive_m2259042514_gshared (BufferTC_t3470796400 * __this, int64_t ___currentTimerId0, Action_1_t912315597 * ___self1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (BufferTC_OnNextRecursive_m2259042514_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	bool V_0 = false;
	List_1_t1634065389 * V_1 = NULL;
	Il2CppObject * V_2 = NULL;
	Il2CppObject* V_3 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	Il2CppObject* G_B10_0 = NULL;
	Il2CppObject* G_B9_0 = NULL;
	Il2CppObject* G_B11_0 = NULL;
	Il2CppObject* G_B11_1 = NULL;
	{
		V_0 = (bool)0;
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_gate_4();
		V_2 = (Il2CppObject *)L_0;
		Il2CppObject * L_1 = V_2;
		Monitor_Enter_m476686225(NULL /*static, unused*/, (Il2CppObject *)L_1, /*hidden argument*/NULL);
	}

IL_000f:
	try
	{ // begin try (depth: 1)
		{
			int64_t L_2 = ___currentTimerId0;
			int64_t L_3 = (int64_t)__this->get_timerId_6();
			if ((((int64_t)L_2) == ((int64_t)L_3)))
			{
				goto IL_0020;
			}
		}

IL_001b:
		{
			IL2CPP_LEAVE(0x81, FINALLY_0049);
		}

IL_0020:
		{
			List_1_t1634065389 * L_4 = (List_1_t1634065389 *)__this->get_list_5();
			V_1 = (List_1_t1634065389 *)L_4;
			List_1_t1634065389 * L_5 = V_1;
			NullCheck((List_1_t1634065389 *)L_5);
			int32_t L_6 = VirtFuncInvoker0< int32_t >::Invoke(20 /* System.Int32 System.Collections.Generic.List`1<System.Object>::get_Count() */, (List_1_t1634065389 *)L_5);
			if (!L_6)
			{
				goto IL_0042;
			}
		}

IL_0032:
		{
			List_1_t1634065389 * L_7 = (List_1_t1634065389 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
			((  void (*) (List_1_t1634065389 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->method)(L_7, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
			__this->set_list_5(L_7);
			goto IL_0044;
		}

IL_0042:
		{
			V_0 = (bool)1;
		}

IL_0044:
		{
			IL2CPP_LEAVE(0x50, FINALLY_0049);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0049;
	}

FINALLY_0049:
	{ // begin finally (depth: 1)
		Il2CppObject * L_8 = V_2;
		Monitor_Exit_m2088237919(NULL /*static, unused*/, (Il2CppObject *)L_8, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(73)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(73)
	{
		IL2CPP_JUMP_TBL(0x81, IL_0081)
		IL2CPP_JUMP_TBL(0x50, IL_0050)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0050:
	{
		Il2CppObject* L_9 = (Il2CppObject*)((OperatorObserverBase_2_t3354260463 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		bool L_10 = V_0;
		G_B9_0 = L_9;
		if (!L_10)
		{
			G_B10_0 = L_9;
			goto IL_006a;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		ObjectU5BU5D_t11523773* L_11 = ((BufferTC_t3470796400_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->static_fields)->get_EmptyArray_2();
		V_3 = (Il2CppObject*)L_11;
		Il2CppObject* L_12 = V_3;
		G_B11_0 = L_12;
		G_B11_1 = G_B9_0;
		goto IL_006b;
	}

IL_006a:
	{
		List_1_t1634065389 * L_13 = V_1;
		G_B11_0 = ((Il2CppObject*)(L_13));
		G_B11_1 = G_B10_0;
	}

IL_006b:
	{
		NullCheck((Il2CppObject*)G_B11_1);
		InterfaceActionInvoker1< Il2CppObject* >::Invoke(2 /* System.Void UniRx.IObserver`1<System.Collections.Generic.IList`1<System.Object>>::OnNext(T) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 13), (Il2CppObject*)G_B11_1, (Il2CppObject*)G_B11_0);
		Action_1_t912315597 * L_14 = ___self1;
		BufferObservable_1_t574294898 * L_15 = (BufferObservable_1_t574294898 *)__this->get_parent_3();
		NullCheck(L_15);
		TimeSpan_t763862892  L_16 = (TimeSpan_t763862892 )L_15->get_timeSpan_4();
		NullCheck((Action_1_t912315597 *)L_14);
		Action_1_Invoke_m613956924((Action_1_t912315597 *)L_14, (TimeSpan_t763862892 )L_16, /*hidden argument*/Action_1_Invoke_m613956924_MethodInfo_var);
	}

IL_0081:
	{
		return;
	}
}
// System.Void UniRx.Operators.BufferObservable`1/BufferTC<System.Object>::OnNext(T)
extern "C"  void BufferTC_OnNext_m2968053326_gshared (BufferTC_t3470796400 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	List_1_t1634065389 * V_0 = NULL;
	Il2CppObject * V_1 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		V_0 = (List_1_t1634065389 *)NULL;
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_gate_4();
		V_1 = (Il2CppObject *)L_0;
		Il2CppObject * L_1 = V_1;
		Monitor_Enter_m476686225(NULL /*static, unused*/, (Il2CppObject *)L_1, /*hidden argument*/NULL);
	}

IL_000f:
	try
	{ // begin try (depth: 1)
		{
			List_1_t1634065389 * L_2 = (List_1_t1634065389 *)__this->get_list_5();
			Il2CppObject * L_3 = ___value0;
			NullCheck((List_1_t1634065389 *)L_2);
			VirtActionInvoker1< Il2CppObject * >::Invoke(22 /* System.Void System.Collections.Generic.List`1<System.Object>::Add(!0) */, (List_1_t1634065389 *)L_2, (Il2CppObject *)L_3);
			List_1_t1634065389 * L_4 = (List_1_t1634065389 *)__this->get_list_5();
			NullCheck((List_1_t1634065389 *)L_4);
			int32_t L_5 = VirtFuncInvoker0< int32_t >::Invoke(20 /* System.Int32 System.Collections.Generic.List`1<System.Object>::get_Count() */, (List_1_t1634065389 *)L_4);
			BufferObservable_1_t574294898 * L_6 = (BufferObservable_1_t574294898 *)__this->get_parent_3();
			NullCheck(L_6);
			int32_t L_7 = (int32_t)L_6->get_count_2();
			if ((!(((uint32_t)L_5) == ((uint32_t)L_7))))
			{
				goto IL_005d;
			}
		}

IL_0036:
		{
			List_1_t1634065389 * L_8 = (List_1_t1634065389 *)__this->get_list_5();
			V_0 = (List_1_t1634065389 *)L_8;
			List_1_t1634065389 * L_9 = (List_1_t1634065389 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
			((  void (*) (List_1_t1634065389 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->method)(L_9, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
			__this->set_list_5(L_9);
			int64_t L_10 = (int64_t)__this->get_timerId_6();
			__this->set_timerId_6(((int64_t)((int64_t)L_10+(int64_t)(((int64_t)((int64_t)1))))));
			NullCheck((BufferTC_t3470796400 *)__this);
			((  void (*) (BufferTC_t3470796400 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6)->method)((BufferTC_t3470796400 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		}

IL_005d:
		{
			IL2CPP_LEAVE(0x69, FINALLY_0062);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0062;
	}

FINALLY_0062:
	{ // begin finally (depth: 1)
		Il2CppObject * L_11 = V_1;
		Monitor_Exit_m2088237919(NULL /*static, unused*/, (Il2CppObject *)L_11, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(98)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(98)
	{
		IL2CPP_JUMP_TBL(0x69, IL_0069)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0069:
	{
		List_1_t1634065389 * L_12 = V_0;
		if (!L_12)
		{
			goto IL_007d;
		}
	}
	{
		Il2CppObject* L_13 = (Il2CppObject*)((OperatorObserverBase_2_t3354260463 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		List_1_t1634065389 * L_14 = V_0;
		NullCheck((Il2CppObject*)L_13);
		InterfaceActionInvoker1< Il2CppObject* >::Invoke(2 /* System.Void UniRx.IObserver`1<System.Collections.Generic.IList`1<System.Object>>::OnNext(T) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 13), (Il2CppObject*)L_13, (Il2CppObject*)L_14);
	}

IL_007d:
	{
		return;
	}
}
// System.Void UniRx.Operators.BufferObservable`1/BufferTC<System.Object>::OnError(System.Exception)
extern "C"  void BufferTC_OnError_m4251100509_gshared (BufferTC_t3470796400 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method)
{
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		Il2CppObject* L_0 = (Il2CppObject*)((OperatorObserverBase_2_t3354260463 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		Exception_t1967233988 * L_1 = ___error0;
		NullCheck((Il2CppObject*)L_0);
		InterfaceActionInvoker1< Exception_t1967233988 * >::Invoke(1 /* System.Void UniRx.IObserver`1<System.Collections.Generic.IList`1<System.Object>>::OnError(System.Exception) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 13), (Il2CppObject*)L_0, (Exception_t1967233988 *)L_1);
		IL2CPP_LEAVE(0x1A, FINALLY_0013);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0013;
	}

FINALLY_0013:
	{ // begin finally (depth: 1)
		NullCheck((OperatorObserverBase_2_t3354260463 *)__this);
		VirtActionInvoker0::Invoke(4 /* System.Void UniRx.Operators.OperatorObserverBase`2<System.Object,System.Collections.Generic.IList`1<System.Object>>::Dispose() */, (OperatorObserverBase_2_t3354260463 *)__this);
		IL2CPP_END_FINALLY(19)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(19)
	{
		IL2CPP_JUMP_TBL(0x1A, IL_001a)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_001a:
	{
		return;
	}
}
// System.Void UniRx.Operators.BufferObservable`1/BufferTC<System.Object>::OnCompleted()
extern "C"  void BufferTC_OnCompleted_m4284529712_gshared (BufferTC_t3470796400 * __this, const MethodInfo* method)
{
	List_1_t1634065389 * V_0 = NULL;
	Il2CppObject * V_1 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_gate_4();
		V_1 = (Il2CppObject *)L_0;
		Il2CppObject * L_1 = V_1;
		Monitor_Enter_m476686225(NULL /*static, unused*/, (Il2CppObject *)L_1, /*hidden argument*/NULL);
	}

IL_000d:
	try
	{ // begin try (depth: 1)
		int64_t L_2 = (int64_t)__this->get_timerId_6();
		__this->set_timerId_6(((int64_t)((int64_t)L_2+(int64_t)(((int64_t)((int64_t)1))))));
		List_1_t1634065389 * L_3 = (List_1_t1634065389 *)__this->get_list_5();
		V_0 = (List_1_t1634065389 *)L_3;
		IL2CPP_LEAVE(0x2F, FINALLY_0028);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0028;
	}

FINALLY_0028:
	{ // begin finally (depth: 1)
		Il2CppObject * L_4 = V_1;
		Monitor_Exit_m2088237919(NULL /*static, unused*/, (Il2CppObject *)L_4, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(40)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(40)
	{
		IL2CPP_JUMP_TBL(0x2F, IL_002f)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_002f:
	{
		Il2CppObject* L_5 = (Il2CppObject*)((OperatorObserverBase_2_t3354260463 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		List_1_t1634065389 * L_6 = V_0;
		NullCheck((Il2CppObject*)L_5);
		InterfaceActionInvoker1< Il2CppObject* >::Invoke(2 /* System.Void UniRx.IObserver`1<System.Collections.Generic.IList`1<System.Object>>::OnNext(T) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 13), (Il2CppObject*)L_5, (Il2CppObject*)L_6);
	}

IL_003d:
	try
	{ // begin try (depth: 1)
		Il2CppObject* L_7 = (Il2CppObject*)((OperatorObserverBase_2_t3354260463 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		NullCheck((Il2CppObject*)L_7);
		InterfaceActionInvoker0::Invoke(0 /* System.Void UniRx.IObserver`1<System.Collections.Generic.IList`1<System.Object>>::OnCompleted() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 13), (Il2CppObject*)L_7);
		IL2CPP_LEAVE(0x56, FINALLY_004f);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_004f;
	}

FINALLY_004f:
	{ // begin finally (depth: 1)
		NullCheck((OperatorObserverBase_2_t3354260463 *)__this);
		VirtActionInvoker0::Invoke(4 /* System.Void UniRx.Operators.OperatorObserverBase`2<System.Object,System.Collections.Generic.IList`1<System.Object>>::Dispose() */, (OperatorObserverBase_2_t3354260463 *)__this);
		IL2CPP_END_FINALLY(79)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(79)
	{
		IL2CPP_JUMP_TBL(0x56, IL_0056)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0056:
	{
		return;
	}
}
// System.Void UniRx.Operators.BufferObservable`1/BufferTS/<CreateTimer>c__AnonStorey5C<System.Object>::.ctor()
extern "C"  void U3CCreateTimerU3Ec__AnonStorey5C__ctor_m1565512555_gshared (U3CCreateTimerU3Ec__AnonStorey5C_t2790700317 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UniRx.Operators.BufferObservable`1/BufferTS/<CreateTimer>c__AnonStorey5C<System.Object>::<>m__74()
extern "C"  void U3CCreateTimerU3Ec__AnonStorey5C_U3CU3Em__74_m3527234321_gshared (U3CCreateTimerU3Ec__AnonStorey5C_t2790700317 * __this, const MethodInfo* method)
{
	Il2CppObject * V_0 = NULL;
	List_1_t1634065389 * V_1 = NULL;
	Il2CppObject* V_2 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		BufferTS_t71212032 * L_0 = (BufferTS_t71212032 *)__this->get_U3CU3Ef__this_2();
		NullCheck(L_0);
		Il2CppObject * L_1 = (Il2CppObject *)L_0->get_gate_3();
		V_0 = (Il2CppObject *)L_1;
		Il2CppObject * L_2 = V_0;
		Monitor_Enter_m476686225(NULL /*static, unused*/, (Il2CppObject *)L_2, /*hidden argument*/NULL);
	}

IL_0012:
	try
	{ // begin try (depth: 1)
		{
			bool L_3 = (bool)__this->get_isShift_0();
			if (!L_3)
			{
				goto IL_0034;
			}
		}

IL_001d:
		{
			List_1_t1634065389 * L_4 = (List_1_t1634065389 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
			((  void (*) (List_1_t1634065389 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)(L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
			V_1 = (List_1_t1634065389 *)L_4;
			BufferTS_t71212032 * L_5 = (BufferTS_t71212032 *)__this->get_U3CU3Ef__this_2();
			NullCheck(L_5);
			Queue_1_t416718978 * L_6 = (Queue_1_t416718978 *)L_5->get_q_4();
			List_1_t1634065389 * L_7 = V_1;
			NullCheck((Queue_1_t416718978 *)L_6);
			((  void (*) (Queue_1_t416718978 *, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)((Queue_1_t416718978 *)L_6, (Il2CppObject*)L_7, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		}

IL_0034:
		{
			bool L_8 = (bool)__this->get_isSpan_1();
			if (!L_8)
			{
				goto IL_0063;
			}
		}

IL_003f:
		{
			BufferTS_t71212032 * L_9 = (BufferTS_t71212032 *)__this->get_U3CU3Ef__this_2();
			NullCheck(L_9);
			Queue_1_t416718978 * L_10 = (Queue_1_t416718978 *)L_9->get_q_4();
			NullCheck((Queue_1_t416718978 *)L_10);
			Il2CppObject* L_11 = ((  Il2CppObject* (*) (Queue_1_t416718978 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)((Queue_1_t416718978 *)L_10, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
			V_2 = (Il2CppObject*)L_11;
			BufferTS_t71212032 * L_12 = (BufferTS_t71212032 *)__this->get_U3CU3Ef__this_2();
			NullCheck(L_12);
			Il2CppObject* L_13 = (Il2CppObject*)((OperatorObserverBase_2_t3354260463 *)L_12)->get_observer_0();
			il2cpp_codegen_memory_barrier();
			Il2CppObject* L_14 = V_2;
			NullCheck((Il2CppObject*)L_13);
			InterfaceActionInvoker1< Il2CppObject* >::Invoke(2 /* System.Void UniRx.IObserver`1<System.Collections.Generic.IList`1<System.Object>>::OnNext(T) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4), (Il2CppObject*)L_13, (Il2CppObject*)L_14);
		}

IL_0063:
		{
			IL2CPP_LEAVE(0x6F, FINALLY_0068);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0068;
	}

FINALLY_0068:
	{ // begin finally (depth: 1)
		Il2CppObject * L_15 = V_0;
		Monitor_Exit_m2088237919(NULL /*static, unused*/, (Il2CppObject *)L_15, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(104)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(104)
	{
		IL2CPP_JUMP_TBL(0x6F, IL_006f)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_006f:
	{
		BufferTS_t71212032 * L_16 = (BufferTS_t71212032 *)__this->get_U3CU3Ef__this_2();
		NullCheck((BufferTS_t71212032 *)L_16);
		((  void (*) (BufferTS_t71212032 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->method)((BufferTS_t71212032 *)L_16, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		return;
	}
}
// System.Void UniRx.Operators.BufferObservable`1/BufferTS<System.Object>::.ctor(UniRx.Operators.BufferObservable`1<T>,UniRx.IObserver`1<System.Collections.Generic.IList`1<T>>,System.IDisposable)
extern Il2CppClass* Il2CppObject_il2cpp_TypeInfo_var;
extern const uint32_t BufferTS__ctor_m349948572_MetadataUsageId;
extern "C"  void BufferTS__ctor_m349948572_gshared (BufferTS_t71212032 * __this, BufferObservable_1_t574294898 * ___parent0, Il2CppObject* ___observer1, Il2CppObject * ___cancel2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (BufferTS__ctor_m349948572_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject * L_0 = (Il2CppObject *)il2cpp_codegen_object_new(Il2CppObject_il2cpp_TypeInfo_var);
		Object__ctor_m1772956182(L_0, /*hidden argument*/NULL);
		__this->set_gate_3(L_0);
		Il2CppObject* L_1 = ___observer1;
		Il2CppObject * L_2 = ___cancel2;
		NullCheck((OperatorObserverBase_2_t3354260463 *)__this);
		((  void (*) (OperatorObserverBase_2_t3354260463 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((OperatorObserverBase_2_t3354260463 *)__this, (Il2CppObject*)L_1, (Il2CppObject *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		BufferObservable_1_t574294898 * L_3 = ___parent0;
		__this->set_parent_2(L_3);
		return;
	}
}
// System.IDisposable UniRx.Operators.BufferObservable`1/BufferTS<System.Object>::Run()
extern Il2CppClass* TimeSpan_t763862892_il2cpp_TypeInfo_var;
extern Il2CppClass* SerialDisposable_t2547852742_il2cpp_TypeInfo_var;
extern const uint32_t BufferTS_Run_m2153900804_MetadataUsageId;
extern "C"  Il2CppObject * BufferTS_Run_m2153900804_gshared (BufferTS_t71212032 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (BufferTS_Run_m2153900804_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Il2CppObject * V_0 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(TimeSpan_t763862892_il2cpp_TypeInfo_var);
		TimeSpan_t763862892  L_0 = ((TimeSpan_t763862892_StaticFields*)TimeSpan_t763862892_il2cpp_TypeInfo_var->static_fields)->get_Zero_7();
		__this->set_totalTime_5(L_0);
		BufferObservable_1_t574294898 * L_1 = (BufferObservable_1_t574294898 *)__this->get_parent_2();
		NullCheck(L_1);
		TimeSpan_t763862892  L_2 = (TimeSpan_t763862892 )L_1->get_timeShift_5();
		__this->set_nextShift_6(L_2);
		BufferObservable_1_t574294898 * L_3 = (BufferObservable_1_t574294898 *)__this->get_parent_2();
		NullCheck(L_3);
		TimeSpan_t763862892  L_4 = (TimeSpan_t763862892 )L_3->get_timeSpan_4();
		__this->set_nextSpan_7(L_4);
		Queue_1_t416718978 * L_5 = (Queue_1_t416718978 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		((  void (*) (Queue_1_t416718978 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)(L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		__this->set_q_4(L_5);
		SerialDisposable_t2547852742 * L_6 = (SerialDisposable_t2547852742 *)il2cpp_codegen_object_new(SerialDisposable_t2547852742_il2cpp_TypeInfo_var);
		SerialDisposable__ctor_m2977852291(L_6, /*hidden argument*/NULL);
		__this->set_timerD_8(L_6);
		Queue_1_t416718978 * L_7 = (Queue_1_t416718978 *)__this->get_q_4();
		List_1_t1634065389 * L_8 = (List_1_t1634065389 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		((  void (*) (List_1_t1634065389 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->method)(L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		NullCheck((Queue_1_t416718978 *)L_7);
		((  void (*) (Queue_1_t416718978 *, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6)->method)((Queue_1_t416718978 *)L_7, (Il2CppObject*)L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		NullCheck((BufferTS_t71212032 *)__this);
		((  void (*) (BufferTS_t71212032 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7)->method)((BufferTS_t71212032 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7));
		BufferObservable_1_t574294898 * L_9 = (BufferObservable_1_t574294898 *)__this->get_parent_2();
		NullCheck(L_9);
		Il2CppObject* L_10 = (Il2CppObject*)L_9->get_source_1();
		NullCheck((Il2CppObject*)L_10);
		Il2CppObject * L_11 = InterfaceFuncInvoker1< Il2CppObject *, Il2CppObject* >::Invoke(0 /* System.IDisposable UniRx.IObservable`1<System.Object>::Subscribe(UniRx.IObserver`1<T>) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8), (Il2CppObject*)L_10, (Il2CppObject*)__this);
		V_0 = (Il2CppObject *)L_11;
		Il2CppObject * L_12 = V_0;
		SerialDisposable_t2547852742 * L_13 = (SerialDisposable_t2547852742 *)__this->get_timerD_8();
		Il2CppObject * L_14 = StableCompositeDisposable_Create_m3764788535(NULL /*static, unused*/, (Il2CppObject *)L_12, (Il2CppObject *)L_13, /*hidden argument*/NULL);
		return L_14;
	}
}
// System.Void UniRx.Operators.BufferObservable`1/BufferTS<System.Object>::CreateTimer()
extern Il2CppClass* SingleAssignmentDisposable_t2336378823_il2cpp_TypeInfo_var;
extern Il2CppClass* TimeSpan_t763862892_il2cpp_TypeInfo_var;
extern Il2CppClass* Action_t437523947_il2cpp_TypeInfo_var;
extern Il2CppClass* IScheduler_t2938318244_il2cpp_TypeInfo_var;
extern const uint32_t BufferTS_CreateTimer_m1358468477_MetadataUsageId;
extern "C"  void BufferTS_CreateTimer_m1358468477_gshared (BufferTS_t71212032 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (BufferTS_CreateTimer_m1358468477_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	SingleAssignmentDisposable_t2336378823 * V_0 = NULL;
	TimeSpan_t763862892  V_1;
	memset(&V_1, 0, sizeof(V_1));
	TimeSpan_t763862892  V_2;
	memset(&V_2, 0, sizeof(V_2));
	U3CCreateTimerU3Ec__AnonStorey5C_t2790700317 * V_3 = NULL;
	TimeSpan_t763862892  G_B8_0;
	memset(&G_B8_0, 0, sizeof(G_B8_0));
	{
		U3CCreateTimerU3Ec__AnonStorey5C_t2790700317 * L_0 = (U3CCreateTimerU3Ec__AnonStorey5C_t2790700317 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9));
		((  void (*) (U3CCreateTimerU3Ec__AnonStorey5C_t2790700317 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10)->method)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10));
		V_3 = (U3CCreateTimerU3Ec__AnonStorey5C_t2790700317 *)L_0;
		U3CCreateTimerU3Ec__AnonStorey5C_t2790700317 * L_1 = V_3;
		NullCheck(L_1);
		L_1->set_U3CU3Ef__this_2(__this);
		SingleAssignmentDisposable_t2336378823 * L_2 = (SingleAssignmentDisposable_t2336378823 *)il2cpp_codegen_object_new(SingleAssignmentDisposable_t2336378823_il2cpp_TypeInfo_var);
		SingleAssignmentDisposable__ctor_m917449122(L_2, /*hidden argument*/NULL);
		V_0 = (SingleAssignmentDisposable_t2336378823 *)L_2;
		SerialDisposable_t2547852742 * L_3 = (SerialDisposable_t2547852742 *)__this->get_timerD_8();
		SingleAssignmentDisposable_t2336378823 * L_4 = V_0;
		NullCheck((SerialDisposable_t2547852742 *)L_3);
		SerialDisposable_set_Disposable_m1438046228((SerialDisposable_t2547852742 *)L_3, (Il2CppObject *)L_4, /*hidden argument*/NULL);
		U3CCreateTimerU3Ec__AnonStorey5C_t2790700317 * L_5 = V_3;
		NullCheck(L_5);
		L_5->set_isSpan_1((bool)0);
		U3CCreateTimerU3Ec__AnonStorey5C_t2790700317 * L_6 = V_3;
		NullCheck(L_6);
		L_6->set_isShift_0((bool)0);
		TimeSpan_t763862892  L_7 = (TimeSpan_t763862892 )__this->get_nextSpan_7();
		TimeSpan_t763862892  L_8 = (TimeSpan_t763862892 )__this->get_nextShift_6();
		IL2CPP_RUNTIME_CLASS_INIT(TimeSpan_t763862892_il2cpp_TypeInfo_var);
		bool L_9 = TimeSpan_op_Equality_m2213378780(NULL /*static, unused*/, (TimeSpan_t763862892 )L_7, (TimeSpan_t763862892 )L_8, /*hidden argument*/NULL);
		if (!L_9)
		{
			goto IL_0056;
		}
	}
	{
		U3CCreateTimerU3Ec__AnonStorey5C_t2790700317 * L_10 = V_3;
		NullCheck(L_10);
		L_10->set_isSpan_1((bool)1);
		U3CCreateTimerU3Ec__AnonStorey5C_t2790700317 * L_11 = V_3;
		NullCheck(L_11);
		L_11->set_isShift_0((bool)1);
		goto IL_007f;
	}

IL_0056:
	{
		TimeSpan_t763862892  L_12 = (TimeSpan_t763862892 )__this->get_nextSpan_7();
		TimeSpan_t763862892  L_13 = (TimeSpan_t763862892 )__this->get_nextShift_6();
		IL2CPP_RUNTIME_CLASS_INIT(TimeSpan_t763862892_il2cpp_TypeInfo_var);
		bool L_14 = TimeSpan_op_LessThan_m4265983228(NULL /*static, unused*/, (TimeSpan_t763862892 )L_12, (TimeSpan_t763862892 )L_13, /*hidden argument*/NULL);
		if (!L_14)
		{
			goto IL_0078;
		}
	}
	{
		U3CCreateTimerU3Ec__AnonStorey5C_t2790700317 * L_15 = V_3;
		NullCheck(L_15);
		L_15->set_isSpan_1((bool)1);
		goto IL_007f;
	}

IL_0078:
	{
		U3CCreateTimerU3Ec__AnonStorey5C_t2790700317 * L_16 = V_3;
		NullCheck(L_16);
		L_16->set_isShift_0((bool)1);
	}

IL_007f:
	{
		U3CCreateTimerU3Ec__AnonStorey5C_t2790700317 * L_17 = V_3;
		NullCheck(L_17);
		bool L_18 = (bool)L_17->get_isSpan_1();
		if (!L_18)
		{
			goto IL_0095;
		}
	}
	{
		TimeSpan_t763862892  L_19 = (TimeSpan_t763862892 )__this->get_nextSpan_7();
		G_B8_0 = L_19;
		goto IL_009b;
	}

IL_0095:
	{
		TimeSpan_t763862892  L_20 = (TimeSpan_t763862892 )__this->get_nextShift_6();
		G_B8_0 = L_20;
	}

IL_009b:
	{
		V_1 = (TimeSpan_t763862892 )G_B8_0;
		TimeSpan_t763862892  L_21 = V_1;
		TimeSpan_t763862892  L_22 = (TimeSpan_t763862892 )__this->get_totalTime_5();
		IL2CPP_RUNTIME_CLASS_INIT(TimeSpan_t763862892_il2cpp_TypeInfo_var);
		TimeSpan_t763862892  L_23 = TimeSpan_op_Subtraction_m3686790579(NULL /*static, unused*/, (TimeSpan_t763862892 )L_21, (TimeSpan_t763862892 )L_22, /*hidden argument*/NULL);
		V_2 = (TimeSpan_t763862892 )L_23;
		TimeSpan_t763862892  L_24 = V_1;
		__this->set_totalTime_5(L_24);
		U3CCreateTimerU3Ec__AnonStorey5C_t2790700317 * L_25 = V_3;
		NullCheck(L_25);
		bool L_26 = (bool)L_25->get_isSpan_1();
		if (!L_26)
		{
			goto IL_00d7;
		}
	}
	{
		TimeSpan_t763862892  L_27 = (TimeSpan_t763862892 )__this->get_nextSpan_7();
		BufferObservable_1_t574294898 * L_28 = (BufferObservable_1_t574294898 *)__this->get_parent_2();
		NullCheck(L_28);
		TimeSpan_t763862892  L_29 = (TimeSpan_t763862892 )L_28->get_timeShift_5();
		IL2CPP_RUNTIME_CLASS_INIT(TimeSpan_t763862892_il2cpp_TypeInfo_var);
		TimeSpan_t763862892  L_30 = TimeSpan_op_Addition_m141072959(NULL /*static, unused*/, (TimeSpan_t763862892 )L_27, (TimeSpan_t763862892 )L_29, /*hidden argument*/NULL);
		__this->set_nextSpan_7(L_30);
	}

IL_00d7:
	{
		U3CCreateTimerU3Ec__AnonStorey5C_t2790700317 * L_31 = V_3;
		NullCheck(L_31);
		bool L_32 = (bool)L_31->get_isShift_0();
		if (!L_32)
		{
			goto IL_00fe;
		}
	}
	{
		TimeSpan_t763862892  L_33 = (TimeSpan_t763862892 )__this->get_nextShift_6();
		BufferObservable_1_t574294898 * L_34 = (BufferObservable_1_t574294898 *)__this->get_parent_2();
		NullCheck(L_34);
		TimeSpan_t763862892  L_35 = (TimeSpan_t763862892 )L_34->get_timeShift_5();
		IL2CPP_RUNTIME_CLASS_INIT(TimeSpan_t763862892_il2cpp_TypeInfo_var);
		TimeSpan_t763862892  L_36 = TimeSpan_op_Addition_m141072959(NULL /*static, unused*/, (TimeSpan_t763862892 )L_33, (TimeSpan_t763862892 )L_35, /*hidden argument*/NULL);
		__this->set_nextShift_6(L_36);
	}

IL_00fe:
	{
		SingleAssignmentDisposable_t2336378823 * L_37 = V_0;
		BufferObservable_1_t574294898 * L_38 = (BufferObservable_1_t574294898 *)__this->get_parent_2();
		NullCheck(L_38);
		Il2CppObject * L_39 = (Il2CppObject *)L_38->get_scheduler_6();
		TimeSpan_t763862892  L_40 = V_2;
		U3CCreateTimerU3Ec__AnonStorey5C_t2790700317 * L_41 = V_3;
		IntPtr_t L_42;
		L_42.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 11));
		Action_t437523947 * L_43 = (Action_t437523947 *)il2cpp_codegen_object_new(Action_t437523947_il2cpp_TypeInfo_var);
		Action__ctor_m2957240604(L_43, (Il2CppObject *)L_41, (IntPtr_t)L_42, /*hidden argument*/NULL);
		NullCheck((Il2CppObject *)L_39);
		Il2CppObject * L_44 = InterfaceFuncInvoker2< Il2CppObject *, TimeSpan_t763862892 , Action_t437523947 * >::Invoke(2 /* System.IDisposable UniRx.IScheduler::Schedule(System.TimeSpan,System.Action) */, IScheduler_t2938318244_il2cpp_TypeInfo_var, (Il2CppObject *)L_39, (TimeSpan_t763862892 )L_40, (Action_t437523947 *)L_43);
		NullCheck((SingleAssignmentDisposable_t2336378823 *)L_37);
		SingleAssignmentDisposable_set_Disposable_m1599028885((SingleAssignmentDisposable_t2336378823 *)L_37, (Il2CppObject *)L_44, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UniRx.Operators.BufferObservable`1/BufferTS<System.Object>::OnNext(T)
extern Il2CppClass* IDisposable_t1628921374_il2cpp_TypeInfo_var;
extern const uint32_t BufferTS_OnNext_m1174008414_MetadataUsageId;
extern "C"  void BufferTS_OnNext_m1174008414_gshared (BufferTS_t71212032 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (BufferTS_OnNext_m1174008414_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Il2CppObject * V_0 = NULL;
	Il2CppObject* V_1 = NULL;
	Enumerator_t1886340695  V_2;
	memset(&V_2, 0, sizeof(V_2));
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_gate_3();
		V_0 = (Il2CppObject *)L_0;
		Il2CppObject * L_1 = V_0;
		Monitor_Enter_m476686225(NULL /*static, unused*/, (Il2CppObject *)L_1, /*hidden argument*/NULL);
	}

IL_000d:
	try
	{ // begin try (depth: 1)
		{
			Queue_1_t416718978 * L_2 = (Queue_1_t416718978 *)__this->get_q_4();
			NullCheck((Queue_1_t416718978 *)L_2);
			Enumerator_t1886340695  L_3 = ((  Enumerator_t1886340695  (*) (Queue_1_t416718978 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 12)->method)((Queue_1_t416718978 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 12));
			V_2 = (Enumerator_t1886340695 )L_3;
		}

IL_0019:
		try
		{ // begin try (depth: 2)
			{
				goto IL_002d;
			}

IL_001e:
			{
				Il2CppObject* L_4 = ((  Il2CppObject* (*) (Enumerator_t1886340695 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 13)->method)((Enumerator_t1886340695 *)(&V_2), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 13));
				V_1 = (Il2CppObject*)L_4;
				Il2CppObject* L_5 = V_1;
				Il2CppObject * L_6 = ___value0;
				NullCheck((Il2CppObject*)L_5);
				InterfaceActionInvoker1< Il2CppObject * >::Invoke(2 /* System.Void System.Collections.Generic.ICollection`1<System.Object>::Add(!0) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 14), (Il2CppObject*)L_5, (Il2CppObject *)L_6);
			}

IL_002d:
			{
				bool L_7 = ((  bool (*) (Enumerator_t1886340695 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 15)->method)((Enumerator_t1886340695 *)(&V_2), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 15));
				if (L_7)
				{
					goto IL_001e;
				}
			}

IL_0039:
			{
				IL2CPP_LEAVE(0x4A, FINALLY_003e);
			}
		} // end try (depth: 2)
		catch(Il2CppExceptionWrapper& e)
		{
			__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
			goto FINALLY_003e;
		}

FINALLY_003e:
		{ // begin finally (depth: 2)
			Enumerator_t1886340695  L_8 = V_2;
			Enumerator_t1886340695  L_9 = L_8;
			Il2CppObject * L_10 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 16), &L_9);
			NullCheck((Il2CppObject *)L_10);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, (Il2CppObject *)L_10);
			IL2CPP_END_FINALLY(62)
		} // end finally (depth: 2)
		IL2CPP_CLEANUP(62)
		{
			IL2CPP_JUMP_TBL(0x4A, IL_004a)
			IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
		}

IL_004a:
		{
			IL2CPP_LEAVE(0x56, FINALLY_004f);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_004f;
	}

FINALLY_004f:
	{ // begin finally (depth: 1)
		Il2CppObject * L_11 = V_0;
		Monitor_Exit_m2088237919(NULL /*static, unused*/, (Il2CppObject *)L_11, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(79)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(79)
	{
		IL2CPP_JUMP_TBL(0x56, IL_0056)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0056:
	{
		return;
	}
}
// System.Void UniRx.Operators.BufferObservable`1/BufferTS<System.Object>::OnError(System.Exception)
extern "C"  void BufferTS_OnError_m1366921581_gshared (BufferTS_t71212032 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method)
{
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		Il2CppObject* L_0 = (Il2CppObject*)((OperatorObserverBase_2_t3354260463 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		Exception_t1967233988 * L_1 = ___error0;
		NullCheck((Il2CppObject*)L_0);
		InterfaceActionInvoker1< Exception_t1967233988 * >::Invoke(1 /* System.Void UniRx.IObserver`1<System.Collections.Generic.IList`1<System.Object>>::OnError(System.Exception) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 17), (Il2CppObject*)L_0, (Exception_t1967233988 *)L_1);
		IL2CPP_LEAVE(0x1A, FINALLY_0013);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0013;
	}

FINALLY_0013:
	{ // begin finally (depth: 1)
		NullCheck((OperatorObserverBase_2_t3354260463 *)__this);
		VirtActionInvoker0::Invoke(4 /* System.Void UniRx.Operators.OperatorObserverBase`2<System.Object,System.Collections.Generic.IList`1<System.Object>>::Dispose() */, (OperatorObserverBase_2_t3354260463 *)__this);
		IL2CPP_END_FINALLY(19)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(19)
	{
		IL2CPP_JUMP_TBL(0x1A, IL_001a)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_001a:
	{
		return;
	}
}
// System.Void UniRx.Operators.BufferObservable`1/BufferTS<System.Object>::OnCompleted()
extern Il2CppClass* IDisposable_t1628921374_il2cpp_TypeInfo_var;
extern const uint32_t BufferTS_OnCompleted_m1307394112_MetadataUsageId;
extern "C"  void BufferTS_OnCompleted_m1307394112_gshared (BufferTS_t71212032 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (BufferTS_OnCompleted_m1307394112_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Il2CppObject * V_0 = NULL;
	Il2CppObject* V_1 = NULL;
	Enumerator_t1886340695  V_2;
	memset(&V_2, 0, sizeof(V_2));
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_gate_3();
		V_0 = (Il2CppObject *)L_0;
		Il2CppObject * L_1 = V_0;
		Monitor_Enter_m476686225(NULL /*static, unused*/, (Il2CppObject *)L_1, /*hidden argument*/NULL);
	}

IL_000d:
	try
	{ // begin try (depth: 1)
		{
			Queue_1_t416718978 * L_2 = (Queue_1_t416718978 *)__this->get_q_4();
			NullCheck((Queue_1_t416718978 *)L_2);
			Enumerator_t1886340695  L_3 = ((  Enumerator_t1886340695  (*) (Queue_1_t416718978 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 12)->method)((Queue_1_t416718978 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 12));
			V_2 = (Enumerator_t1886340695 )L_3;
		}

IL_0019:
		try
		{ // begin try (depth: 2)
			{
				goto IL_0034;
			}

IL_001e:
			{
				Il2CppObject* L_4 = ((  Il2CppObject* (*) (Enumerator_t1886340695 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 13)->method)((Enumerator_t1886340695 *)(&V_2), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 13));
				V_1 = (Il2CppObject*)L_4;
				Il2CppObject* L_5 = (Il2CppObject*)((OperatorObserverBase_2_t3354260463 *)__this)->get_observer_0();
				il2cpp_codegen_memory_barrier();
				Il2CppObject* L_6 = V_1;
				NullCheck((Il2CppObject*)L_5);
				InterfaceActionInvoker1< Il2CppObject* >::Invoke(2 /* System.Void UniRx.IObserver`1<System.Collections.Generic.IList`1<System.Object>>::OnNext(T) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 17), (Il2CppObject*)L_5, (Il2CppObject*)L_6);
			}

IL_0034:
			{
				bool L_7 = ((  bool (*) (Enumerator_t1886340695 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 15)->method)((Enumerator_t1886340695 *)(&V_2), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 15));
				if (L_7)
				{
					goto IL_001e;
				}
			}

IL_0040:
			{
				IL2CPP_LEAVE(0x51, FINALLY_0045);
			}
		} // end try (depth: 2)
		catch(Il2CppExceptionWrapper& e)
		{
			__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
			goto FINALLY_0045;
		}

FINALLY_0045:
		{ // begin finally (depth: 2)
			Enumerator_t1886340695  L_8 = V_2;
			Enumerator_t1886340695  L_9 = L_8;
			Il2CppObject * L_10 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 16), &L_9);
			NullCheck((Il2CppObject *)L_10);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, (Il2CppObject *)L_10);
			IL2CPP_END_FINALLY(69)
		} // end finally (depth: 2)
		IL2CPP_CLEANUP(69)
		{
			IL2CPP_JUMP_TBL(0x51, IL_0051)
			IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
		}

IL_0051:
		try
		{ // begin try (depth: 2)
			Il2CppObject* L_11 = (Il2CppObject*)((OperatorObserverBase_2_t3354260463 *)__this)->get_observer_0();
			il2cpp_codegen_memory_barrier();
			NullCheck((Il2CppObject*)L_11);
			InterfaceActionInvoker0::Invoke(0 /* System.Void UniRx.IObserver`1<System.Collections.Generic.IList`1<System.Object>>::OnCompleted() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 17), (Il2CppObject*)L_11);
			IL2CPP_LEAVE(0x6A, FINALLY_0063);
		} // end try (depth: 2)
		catch(Il2CppExceptionWrapper& e)
		{
			__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
			goto FINALLY_0063;
		}

FINALLY_0063:
		{ // begin finally (depth: 2)
			NullCheck((OperatorObserverBase_2_t3354260463 *)__this);
			VirtActionInvoker0::Invoke(4 /* System.Void UniRx.Operators.OperatorObserverBase`2<System.Object,System.Collections.Generic.IList`1<System.Object>>::Dispose() */, (OperatorObserverBase_2_t3354260463 *)__this);
			IL2CPP_END_FINALLY(99)
		} // end finally (depth: 2)
		IL2CPP_CLEANUP(99)
		{
			IL2CPP_JUMP_TBL(0x6A, IL_006a)
			IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
		}

IL_006a:
		{
			IL2CPP_LEAVE(0x76, FINALLY_006f);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_006f;
	}

FINALLY_006f:
	{ // begin finally (depth: 1)
		Il2CppObject * L_12 = V_0;
		Monitor_Exit_m2088237919(NULL /*static, unused*/, (Il2CppObject *)L_12, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(111)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(111)
	{
		IL2CPP_JUMP_TBL(0x76, IL_0076)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0076:
	{
		return;
	}
}
// System.Void UniRx.Operators.BufferObservable`1<System.Object>::.ctor(UniRx.IObservable`1<T>,System.Int32,System.Int32)
extern "C"  void BufferObservable_1__ctor_m1904451461_gshared (BufferObservable_1_t574294898 * __this, Il2CppObject* ___source0, int32_t ___count1, int32_t ___skip2, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = ___source0;
		bool L_1 = ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)(NULL /*static, unused*/, (Il2CppObject*)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		NullCheck((OperatorObservableBase_1_t2067743705 *)__this);
		((  void (*) (OperatorObservableBase_1_t2067743705 *, bool, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)((OperatorObservableBase_1_t2067743705 *)__this, (bool)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		Il2CppObject* L_2 = ___source0;
		__this->set_source_1(L_2);
		int32_t L_3 = ___count1;
		__this->set_count_2(L_3);
		int32_t L_4 = ___skip2;
		__this->set_skip_3(L_4);
		return;
	}
}
// System.Void UniRx.Operators.BufferObservable`1<System.Object>::.ctor(UniRx.IObservable`1<T>,System.TimeSpan,System.TimeSpan,UniRx.IScheduler)
extern Il2CppClass* Scheduler_t103934925_il2cpp_TypeInfo_var;
extern const uint32_t BufferObservable_1__ctor_m1677543595_MetadataUsageId;
extern "C"  void BufferObservable_1__ctor_m1677543595_gshared (BufferObservable_1_t574294898 * __this, Il2CppObject* ___source0, TimeSpan_t763862892  ___timeSpan1, TimeSpan_t763862892  ___timeShift2, Il2CppObject * ___scheduler3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (BufferObservable_1__ctor_m1677543595_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	BufferObservable_1_t574294898 * G_B2_0 = NULL;
	BufferObservable_1_t574294898 * G_B1_0 = NULL;
	int32_t G_B3_0 = 0;
	BufferObservable_1_t574294898 * G_B3_1 = NULL;
	{
		Il2CppObject * L_0 = ___scheduler3;
		IL2CPP_RUNTIME_CLASS_INIT(Scheduler_t103934925_il2cpp_TypeInfo_var);
		Il2CppObject * L_1 = ((Scheduler_t103934925_StaticFields*)Scheduler_t103934925_il2cpp_TypeInfo_var->static_fields)->get_CurrentThread_0();
		G_B1_0 = ((BufferObservable_1_t574294898 *)(__this));
		if ((((Il2CppObject*)(Il2CppObject *)L_0) == ((Il2CppObject*)(Il2CppObject *)L_1)))
		{
			G_B2_0 = ((BufferObservable_1_t574294898 *)(__this));
			goto IL_0015;
		}
	}
	{
		Il2CppObject* L_2 = ___source0;
		bool L_3 = ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)(NULL /*static, unused*/, (Il2CppObject*)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		G_B3_0 = ((int32_t)(L_3));
		G_B3_1 = ((BufferObservable_1_t574294898 *)(G_B1_0));
		goto IL_0016;
	}

IL_0015:
	{
		G_B3_0 = 1;
		G_B3_1 = ((BufferObservable_1_t574294898 *)(G_B2_0));
	}

IL_0016:
	{
		NullCheck((OperatorObservableBase_1_t2067743705 *)G_B3_1);
		((  void (*) (OperatorObservableBase_1_t2067743705 *, bool, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)((OperatorObservableBase_1_t2067743705 *)G_B3_1, (bool)G_B3_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		Il2CppObject* L_4 = ___source0;
		__this->set_source_1(L_4);
		TimeSpan_t763862892  L_5 = ___timeSpan1;
		__this->set_timeSpan_4(L_5);
		TimeSpan_t763862892  L_6 = ___timeShift2;
		__this->set_timeShift_5(L_6);
		Il2CppObject * L_7 = ___scheduler3;
		__this->set_scheduler_6(L_7);
		return;
	}
}
// System.Void UniRx.Operators.BufferObservable`1<System.Object>::.ctor(UniRx.IObservable`1<T>,System.TimeSpan,System.Int32,UniRx.IScheduler)
extern Il2CppClass* Scheduler_t103934925_il2cpp_TypeInfo_var;
extern const uint32_t BufferObservable_1__ctor_m2289049376_MetadataUsageId;
extern "C"  void BufferObservable_1__ctor_m2289049376_gshared (BufferObservable_1_t574294898 * __this, Il2CppObject* ___source0, TimeSpan_t763862892  ___timeSpan1, int32_t ___count2, Il2CppObject * ___scheduler3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (BufferObservable_1__ctor_m2289049376_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	BufferObservable_1_t574294898 * G_B2_0 = NULL;
	BufferObservable_1_t574294898 * G_B1_0 = NULL;
	int32_t G_B3_0 = 0;
	BufferObservable_1_t574294898 * G_B3_1 = NULL;
	{
		Il2CppObject * L_0 = ___scheduler3;
		IL2CPP_RUNTIME_CLASS_INIT(Scheduler_t103934925_il2cpp_TypeInfo_var);
		Il2CppObject * L_1 = ((Scheduler_t103934925_StaticFields*)Scheduler_t103934925_il2cpp_TypeInfo_var->static_fields)->get_CurrentThread_0();
		G_B1_0 = ((BufferObservable_1_t574294898 *)(__this));
		if ((((Il2CppObject*)(Il2CppObject *)L_0) == ((Il2CppObject*)(Il2CppObject *)L_1)))
		{
			G_B2_0 = ((BufferObservable_1_t574294898 *)(__this));
			goto IL_0015;
		}
	}
	{
		Il2CppObject* L_2 = ___source0;
		bool L_3 = ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)(NULL /*static, unused*/, (Il2CppObject*)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		G_B3_0 = ((int32_t)(L_3));
		G_B3_1 = ((BufferObservable_1_t574294898 *)(G_B1_0));
		goto IL_0016;
	}

IL_0015:
	{
		G_B3_0 = 1;
		G_B3_1 = ((BufferObservable_1_t574294898 *)(G_B2_0));
	}

IL_0016:
	{
		NullCheck((OperatorObservableBase_1_t2067743705 *)G_B3_1);
		((  void (*) (OperatorObservableBase_1_t2067743705 *, bool, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)((OperatorObservableBase_1_t2067743705 *)G_B3_1, (bool)G_B3_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		Il2CppObject* L_4 = ___source0;
		__this->set_source_1(L_4);
		TimeSpan_t763862892  L_5 = ___timeSpan1;
		__this->set_timeSpan_4(L_5);
		int32_t L_6 = ___count2;
		__this->set_count_2(L_6);
		Il2CppObject * L_7 = ___scheduler3;
		__this->set_scheduler_6(L_7);
		return;
	}
}
// System.IDisposable UniRx.Operators.BufferObservable`1<System.Object>::SubscribeCore(UniRx.IObserver`1<System.Collections.Generic.IList`1<T>>,System.IDisposable)
extern Il2CppClass* TimeSpan_t763862892_il2cpp_TypeInfo_var;
extern const uint32_t BufferObservable_1_SubscribeCore_m2908667513_MetadataUsageId;
extern "C"  Il2CppObject * BufferObservable_1_SubscribeCore_m2908667513_gshared (BufferObservable_1_t574294898 * __this, Il2CppObject* ___observer0, Il2CppObject * ___cancel1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (BufferObservable_1_SubscribeCore_m2908667513_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_scheduler_6();
		if (L_0)
		{
			goto IL_0032;
		}
	}
	{
		int32_t L_1 = (int32_t)__this->get_skip_3();
		if (L_1)
		{
			goto IL_0024;
		}
	}
	{
		Il2CppObject* L_2 = ___observer0;
		Il2CppObject * L_3 = ___cancel1;
		Buffer_t492975321 * L_4 = (Buffer_t492975321 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		((  void (*) (Buffer_t492975321 *, BufferObservable_1_t574294898 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)(L_4, (BufferObservable_1_t574294898 *)__this, (Il2CppObject*)L_2, (Il2CppObject *)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		NullCheck((Buffer_t492975321 *)L_4);
		Il2CppObject * L_5 = ((  Il2CppObject * (*) (Buffer_t492975321 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->method)((Buffer_t492975321 *)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		return L_5;
	}

IL_0024:
	{
		Il2CppObject* L_6 = ___observer0;
		Il2CppObject * L_7 = ___cancel1;
		Buffer__t1281669152 * L_8 = (Buffer__t1281669152 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		((  void (*) (Buffer__t1281669152 *, BufferObservable_1_t574294898 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7)->method)(L_8, (BufferObservable_1_t574294898 *)__this, (Il2CppObject*)L_6, (Il2CppObject *)L_7, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7));
		NullCheck((Buffer__t1281669152 *)L_8);
		Il2CppObject * L_9 = ((  Il2CppObject * (*) (Buffer__t1281669152 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8)->method)((Buffer__t1281669152 *)L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8));
		return L_9;
	}

IL_0032:
	{
		int32_t L_10 = (int32_t)__this->get_count_2();
		if ((((int32_t)L_10) <= ((int32_t)0)))
		{
			goto IL_004c;
		}
	}
	{
		Il2CppObject* L_11 = ___observer0;
		Il2CppObject * L_12 = ___cancel1;
		BufferTC_t3470796400 * L_13 = (BufferTC_t3470796400 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9));
		((  void (*) (BufferTC_t3470796400 *, BufferObservable_1_t574294898 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10)->method)(L_13, (BufferObservable_1_t574294898 *)__this, (Il2CppObject*)L_11, (Il2CppObject *)L_12, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10));
		NullCheck((BufferTC_t3470796400 *)L_13);
		Il2CppObject * L_14 = ((  Il2CppObject * (*) (BufferTC_t3470796400 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 11)->method)((BufferTC_t3470796400 *)L_13, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 11));
		return L_14;
	}

IL_004c:
	{
		TimeSpan_t763862892  L_15 = (TimeSpan_t763862892 )__this->get_timeSpan_4();
		TimeSpan_t763862892  L_16 = (TimeSpan_t763862892 )__this->get_timeShift_5();
		IL2CPP_RUNTIME_CLASS_INIT(TimeSpan_t763862892_il2cpp_TypeInfo_var);
		bool L_17 = TimeSpan_op_Equality_m2213378780(NULL /*static, unused*/, (TimeSpan_t763862892 )L_15, (TimeSpan_t763862892 )L_16, /*hidden argument*/NULL);
		if (!L_17)
		{
			goto IL_0070;
		}
	}
	{
		Il2CppObject* L_18 = ___observer0;
		Il2CppObject * L_19 = ___cancel1;
		BufferT_t129222477 * L_20 = (BufferT_t129222477 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 12));
		((  void (*) (BufferT_t129222477 *, BufferObservable_1_t574294898 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 13)->method)(L_20, (BufferObservable_1_t574294898 *)__this, (Il2CppObject*)L_18, (Il2CppObject *)L_19, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 13));
		NullCheck((BufferT_t129222477 *)L_20);
		Il2CppObject * L_21 = ((  Il2CppObject * (*) (BufferT_t129222477 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 14)->method)((BufferT_t129222477 *)L_20, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 14));
		return L_21;
	}

IL_0070:
	{
		Il2CppObject* L_22 = ___observer0;
		Il2CppObject * L_23 = ___cancel1;
		BufferTS_t71212032 * L_24 = (BufferTS_t71212032 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 15));
		((  void (*) (BufferTS_t71212032 *, BufferObservable_1_t574294898 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 16)->method)(L_24, (BufferObservable_1_t574294898 *)__this, (Il2CppObject*)L_22, (Il2CppObject *)L_23, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 16));
		NullCheck((BufferTS_t71212032 *)L_24);
		Il2CppObject * L_25 = ((  Il2CppObject * (*) (BufferTS_t71212032 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 17)->method)((BufferTS_t71212032 *)L_24, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 17));
		return L_25;
	}
}
// System.Void UniRx.Operators.BufferObservable`2/Buffer/Buffer_<System.Int64,System.Int64>::.ctor(UniRx.Operators.BufferObservable`2/Buffer<TSource,TWindowBoundary>)
extern "C"  void Buffer___ctor_m3639263566_gshared (Buffer__t1004182748 * __this, Buffer_t1984651551 * ___parent0, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		Buffer_t1984651551 * L_0 = ___parent0;
		__this->set_parent_0(L_0);
		return;
	}
}
// System.Void UniRx.Operators.BufferObservable`2/Buffer/Buffer_<System.Int64,System.Int64>::OnNext(TWindowBoundary)
extern "C"  void Buffer__OnNext_m2846136290_gshared (Buffer__t1004182748 * __this, int64_t ___value0, const MethodInfo* method)
{
	bool V_0 = false;
	List_1_t3644373851 * V_1 = NULL;
	Il2CppObject * V_2 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		V_0 = (bool)0;
		Buffer_t1984651551 * L_0 = (Buffer_t1984651551 *)__this->get_parent_0();
		NullCheck(L_0);
		Il2CppObject * L_1 = (Il2CppObject *)L_0->get_gate_4();
		V_2 = (Il2CppObject *)L_1;
		Il2CppObject * L_2 = V_2;
		Monitor_Enter_m476686225(NULL /*static, unused*/, (Il2CppObject *)L_2, /*hidden argument*/NULL);
	}

IL_0014:
	try
	{ // begin try (depth: 1)
		{
			Buffer_t1984651551 * L_3 = (Buffer_t1984651551 *)__this->get_parent_0();
			NullCheck(L_3);
			List_1_t3644373851 * L_4 = (List_1_t3644373851 *)L_3->get_list_5();
			V_1 = (List_1_t3644373851 *)L_4;
			List_1_t3644373851 * L_5 = V_1;
			NullCheck((List_1_t3644373851 *)L_5);
			int32_t L_6 = VirtFuncInvoker0< int32_t >::Invoke(20 /* System.Int32 System.Collections.Generic.List`1<System.Int64>::get_Count() */, (List_1_t3644373851 *)L_5);
			if (!L_6)
			{
				goto IL_0040;
			}
		}

IL_002b:
		{
			Buffer_t1984651551 * L_7 = (Buffer_t1984651551 *)__this->get_parent_0();
			List_1_t3644373851 * L_8 = (List_1_t3644373851 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
			((  void (*) (List_1_t3644373851 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)(L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
			NullCheck(L_7);
			L_7->set_list_5(L_8);
			goto IL_0042;
		}

IL_0040:
		{
			V_0 = (bool)1;
		}

IL_0042:
		{
			IL2CPP_LEAVE(0x4E, FINALLY_0047);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0047;
	}

FINALLY_0047:
	{ // begin finally (depth: 1)
		Il2CppObject * L_9 = V_2;
		Monitor_Exit_m2088237919(NULL /*static, unused*/, (Il2CppObject *)L_9, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(71)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(71)
	{
		IL2CPP_JUMP_TBL(0x4E, IL_004e)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_004e:
	{
		bool L_10 = V_0;
		if (!L_10)
		{
			goto IL_0070;
		}
	}
	{
		Buffer_t1984651551 * L_11 = (Buffer_t1984651551 *)__this->get_parent_0();
		NullCheck(L_11);
		Il2CppObject* L_12 = (Il2CppObject*)((OperatorObserverBase_2_t1811255927 *)L_11)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		Int64U5BU5D_t753178071* L_13 = ((Buffer_t1984651551_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->static_fields)->get_EmptyArray_2();
		NullCheck((Il2CppObject*)L_12);
		InterfaceActionInvoker1< Il2CppObject* >::Invoke(2 /* System.Void UniRx.IObserver`1<System.Collections.Generic.IList`1<System.Int64>>::OnNext(T) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5), (Il2CppObject*)L_12, (Il2CppObject*)(Il2CppObject*)L_13);
		goto IL_0083;
	}

IL_0070:
	{
		Buffer_t1984651551 * L_14 = (Buffer_t1984651551 *)__this->get_parent_0();
		NullCheck(L_14);
		Il2CppObject* L_15 = (Il2CppObject*)((OperatorObserverBase_2_t1811255927 *)L_14)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		List_1_t3644373851 * L_16 = V_1;
		NullCheck((Il2CppObject*)L_15);
		InterfaceActionInvoker1< Il2CppObject* >::Invoke(2 /* System.Void UniRx.IObserver`1<System.Collections.Generic.IList`1<System.Int64>>::OnNext(T) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5), (Il2CppObject*)L_15, (Il2CppObject*)L_16);
	}

IL_0083:
	{
		return;
	}
}
// System.Void UniRx.Operators.BufferObservable`2/Buffer/Buffer_<System.Int64,System.Int64>::OnError(System.Exception)
extern "C"  void Buffer__OnError_m3669021003_gshared (Buffer__t1004182748 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method)
{
	{
		Buffer_t1984651551 * L_0 = (Buffer_t1984651551 *)__this->get_parent_0();
		Exception_t1967233988 * L_1 = ___error0;
		NullCheck((Buffer_t1984651551 *)L_0);
		VirtActionInvoker1< Exception_t1967233988 * >::Invoke(9 /* System.Void UniRx.Operators.BufferObservable`2/Buffer<System.Int64,System.Int64>::OnError(System.Exception) */, (Buffer_t1984651551 *)L_0, (Exception_t1967233988 *)L_1);
		return;
	}
}
// System.Void UniRx.Operators.BufferObservable`2/Buffer/Buffer_<System.Int64,System.Int64>::OnCompleted()
extern "C"  void Buffer__OnCompleted_m3540966686_gshared (Buffer__t1004182748 * __this, const MethodInfo* method)
{
	{
		Buffer_t1984651551 * L_0 = (Buffer_t1984651551 *)__this->get_parent_0();
		NullCheck((Buffer_t1984651551 *)L_0);
		VirtActionInvoker0::Invoke(10 /* System.Void UniRx.Operators.BufferObservable`2/Buffer<System.Int64,System.Int64>::OnCompleted() */, (Buffer_t1984651551 *)L_0);
		return;
	}
}
// System.Void UniRx.Operators.BufferObservable`2/Buffer/Buffer_<System.Object,System.Object>::.ctor(UniRx.Operators.BufferObservable`2/Buffer<TSource,TWindowBoundary>)
extern "C"  void Buffer___ctor_m1018972210_gshared (Buffer__t2547187284 * __this, Buffer_t3527656087 * ___parent0, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		Buffer_t3527656087 * L_0 = ___parent0;
		__this->set_parent_0(L_0);
		return;
	}
}
// System.Void UniRx.Operators.BufferObservable`2/Buffer/Buffer_<System.Object,System.Object>::OnNext(TWindowBoundary)
extern "C"  void Buffer__OnNext_m2817482694_gshared (Buffer__t2547187284 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	bool V_0 = false;
	List_1_t1634065389 * V_1 = NULL;
	Il2CppObject * V_2 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		V_0 = (bool)0;
		Buffer_t3527656087 * L_0 = (Buffer_t3527656087 *)__this->get_parent_0();
		NullCheck(L_0);
		Il2CppObject * L_1 = (Il2CppObject *)L_0->get_gate_4();
		V_2 = (Il2CppObject *)L_1;
		Il2CppObject * L_2 = V_2;
		Monitor_Enter_m476686225(NULL /*static, unused*/, (Il2CppObject *)L_2, /*hidden argument*/NULL);
	}

IL_0014:
	try
	{ // begin try (depth: 1)
		{
			Buffer_t3527656087 * L_3 = (Buffer_t3527656087 *)__this->get_parent_0();
			NullCheck(L_3);
			List_1_t1634065389 * L_4 = (List_1_t1634065389 *)L_3->get_list_5();
			V_1 = (List_1_t1634065389 *)L_4;
			List_1_t1634065389 * L_5 = V_1;
			NullCheck((List_1_t1634065389 *)L_5);
			int32_t L_6 = VirtFuncInvoker0< int32_t >::Invoke(20 /* System.Int32 System.Collections.Generic.List`1<System.Object>::get_Count() */, (List_1_t1634065389 *)L_5);
			if (!L_6)
			{
				goto IL_0040;
			}
		}

IL_002b:
		{
			Buffer_t3527656087 * L_7 = (Buffer_t3527656087 *)__this->get_parent_0();
			List_1_t1634065389 * L_8 = (List_1_t1634065389 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
			((  void (*) (List_1_t1634065389 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)(L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
			NullCheck(L_7);
			L_7->set_list_5(L_8);
			goto IL_0042;
		}

IL_0040:
		{
			V_0 = (bool)1;
		}

IL_0042:
		{
			IL2CPP_LEAVE(0x4E, FINALLY_0047);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0047;
	}

FINALLY_0047:
	{ // begin finally (depth: 1)
		Il2CppObject * L_9 = V_2;
		Monitor_Exit_m2088237919(NULL /*static, unused*/, (Il2CppObject *)L_9, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(71)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(71)
	{
		IL2CPP_JUMP_TBL(0x4E, IL_004e)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_004e:
	{
		bool L_10 = V_0;
		if (!L_10)
		{
			goto IL_0070;
		}
	}
	{
		Buffer_t3527656087 * L_11 = (Buffer_t3527656087 *)__this->get_parent_0();
		NullCheck(L_11);
		Il2CppObject* L_12 = (Il2CppObject*)((OperatorObserverBase_2_t3354260463 *)L_11)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		ObjectU5BU5D_t11523773* L_13 = ((Buffer_t3527656087_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->static_fields)->get_EmptyArray_2();
		NullCheck((Il2CppObject*)L_12);
		InterfaceActionInvoker1< Il2CppObject* >::Invoke(2 /* System.Void UniRx.IObserver`1<System.Collections.Generic.IList`1<System.Object>>::OnNext(T) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5), (Il2CppObject*)L_12, (Il2CppObject*)(Il2CppObject*)L_13);
		goto IL_0083;
	}

IL_0070:
	{
		Buffer_t3527656087 * L_14 = (Buffer_t3527656087 *)__this->get_parent_0();
		NullCheck(L_14);
		Il2CppObject* L_15 = (Il2CppObject*)((OperatorObserverBase_2_t3354260463 *)L_14)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		List_1_t1634065389 * L_16 = V_1;
		NullCheck((Il2CppObject*)L_15);
		InterfaceActionInvoker1< Il2CppObject* >::Invoke(2 /* System.Void UniRx.IObserver`1<System.Collections.Generic.IList`1<System.Object>>::OnNext(T) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5), (Il2CppObject*)L_15, (Il2CppObject*)L_16);
	}

IL_0083:
	{
		return;
	}
}
// System.Void UniRx.Operators.BufferObservable`2/Buffer/Buffer_<System.Object,System.Object>::OnError(System.Exception)
extern "C"  void Buffer__OnError_m1902719023_gshared (Buffer__t2547187284 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method)
{
	{
		Buffer_t3527656087 * L_0 = (Buffer_t3527656087 *)__this->get_parent_0();
		Exception_t1967233988 * L_1 = ___error0;
		NullCheck((Buffer_t3527656087 *)L_0);
		VirtActionInvoker1< Exception_t1967233988 * >::Invoke(9 /* System.Void UniRx.Operators.BufferObservable`2/Buffer<System.Object,System.Object>::OnError(System.Exception) */, (Buffer_t3527656087 *)L_0, (Exception_t1967233988 *)L_1);
		return;
	}
}
// System.Void UniRx.Operators.BufferObservable`2/Buffer/Buffer_<System.Object,System.Object>::OnCompleted()
extern "C"  void Buffer__OnCompleted_m2203283970_gshared (Buffer__t2547187284 * __this, const MethodInfo* method)
{
	{
		Buffer_t3527656087 * L_0 = (Buffer_t3527656087 *)__this->get_parent_0();
		NullCheck((Buffer_t3527656087 *)L_0);
		VirtActionInvoker0::Invoke(10 /* System.Void UniRx.Operators.BufferObservable`2/Buffer<System.Object,System.Object>::OnCompleted() */, (Buffer_t3527656087 *)L_0);
		return;
	}
}
// System.Void UniRx.Operators.BufferObservable`2/Buffer<System.Int64,System.Int64>::.ctor(UniRx.Operators.BufferObservable`2<TSource,TWindowBoundary>,UniRx.IObserver`1<System.Collections.Generic.IList`1<TSource>>,System.IDisposable)
extern Il2CppClass* Il2CppObject_il2cpp_TypeInfo_var;
extern const uint32_t Buffer__ctor_m2649861863_MetadataUsageId;
extern "C"  void Buffer__ctor_m2649861863_gshared (Buffer_t1984651551 * __this, BufferObservable_2_t550437669 * ___parent0, Il2CppObject* ___observer1, Il2CppObject * ___cancel2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Buffer__ctor_m2649861863_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject * L_0 = (Il2CppObject *)il2cpp_codegen_object_new(Il2CppObject_il2cpp_TypeInfo_var);
		Object__ctor_m1772956182(L_0, /*hidden argument*/NULL);
		__this->set_gate_4(L_0);
		Il2CppObject* L_1 = ___observer1;
		Il2CppObject * L_2 = ___cancel2;
		NullCheck((OperatorObserverBase_2_t1811255927 *)__this);
		((  void (*) (OperatorObserverBase_2_t1811255927 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((OperatorObserverBase_2_t1811255927 *)__this, (Il2CppObject*)L_1, (Il2CppObject *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		BufferObservable_2_t550437669 * L_3 = ___parent0;
		__this->set_parent_3(L_3);
		return;
	}
}
// System.Void UniRx.Operators.BufferObservable`2/Buffer<System.Int64,System.Int64>::.cctor()
extern "C"  void Buffer__cctor_m1290824073_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		((Buffer_t1984651551_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->static_fields)->set_EmptyArray_2(((Int64U5BU5D_t753178071*)SZArrayNew(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), (uint32_t)0)));
		return;
	}
}
// System.IDisposable UniRx.Operators.BufferObservable`2/Buffer<System.Int64,System.Int64>::Run()
extern "C"  Il2CppObject * Buffer_Run_m2773953970_gshared (Buffer_t1984651551 * __this, const MethodInfo* method)
{
	Il2CppObject * V_0 = NULL;
	Il2CppObject * V_1 = NULL;
	{
		List_1_t3644373851 * L_0 = (List_1_t3644373851 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		((  void (*) (List_1_t3644373851 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->method)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		__this->set_list_5(L_0);
		BufferObservable_2_t550437669 * L_1 = (BufferObservable_2_t550437669 *)__this->get_parent_3();
		NullCheck(L_1);
		Il2CppObject* L_2 = (Il2CppObject*)L_1->get_source_1();
		NullCheck((Il2CppObject*)L_2);
		Il2CppObject * L_3 = InterfaceFuncInvoker1< Il2CppObject *, Il2CppObject* >::Invoke(0 /* System.IDisposable UniRx.IObservable`1<System.Int64>::Subscribe(UniRx.IObserver`1<T>) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6), (Il2CppObject*)L_2, (Il2CppObject*)__this);
		V_0 = (Il2CppObject *)L_3;
		BufferObservable_2_t550437669 * L_4 = (BufferObservable_2_t550437669 *)__this->get_parent_3();
		NullCheck(L_4);
		Il2CppObject* L_5 = (Il2CppObject*)L_4->get_windowBoundaries_2();
		Buffer__t1004182748 * L_6 = (Buffer__t1004182748 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7));
		((  void (*) (Buffer__t1004182748 *, Buffer_t1984651551 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8)->method)(L_6, (Buffer_t1984651551 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8));
		NullCheck((Il2CppObject*)L_5);
		Il2CppObject * L_7 = InterfaceFuncInvoker1< Il2CppObject *, Il2CppObject* >::Invoke(0 /* System.IDisposable UniRx.IObservable`1<System.Int64>::Subscribe(UniRx.IObserver`1<T>) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9), (Il2CppObject*)L_5, (Il2CppObject*)L_6);
		V_1 = (Il2CppObject *)L_7;
		Il2CppObject * L_8 = V_0;
		Il2CppObject * L_9 = V_1;
		Il2CppObject * L_10 = StableCompositeDisposable_Create_m3764788535(NULL /*static, unused*/, (Il2CppObject *)L_8, (Il2CppObject *)L_9, /*hidden argument*/NULL);
		return L_10;
	}
}
// System.Void UniRx.Operators.BufferObservable`2/Buffer<System.Int64,System.Int64>::OnNext(TSource)
extern "C"  void Buffer_OnNext_m4291134769_gshared (Buffer_t1984651551 * __this, int64_t ___value0, const MethodInfo* method)
{
	Il2CppObject * V_0 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_gate_4();
		V_0 = (Il2CppObject *)L_0;
		Il2CppObject * L_1 = V_0;
		Monitor_Enter_m476686225(NULL /*static, unused*/, (Il2CppObject *)L_1, /*hidden argument*/NULL);
	}

IL_000d:
	try
	{ // begin try (depth: 1)
		List_1_t3644373851 * L_2 = (List_1_t3644373851 *)__this->get_list_5();
		int64_t L_3 = ___value0;
		NullCheck((List_1_t3644373851 *)L_2);
		VirtActionInvoker1< int64_t >::Invoke(22 /* System.Void System.Collections.Generic.List`1<System.Int64>::Add(!0) */, (List_1_t3644373851 *)L_2, (int64_t)L_3);
		IL2CPP_LEAVE(0x25, FINALLY_001e);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_001e;
	}

FINALLY_001e:
	{ // begin finally (depth: 1)
		Il2CppObject * L_4 = V_0;
		Monitor_Exit_m2088237919(NULL /*static, unused*/, (Il2CppObject *)L_4, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(30)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(30)
	{
		IL2CPP_JUMP_TBL(0x25, IL_0025)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0025:
	{
		return;
	}
}
// System.Void UniRx.Operators.BufferObservable`2/Buffer<System.Int64,System.Int64>::OnError(System.Exception)
extern "C"  void Buffer_OnError_m987904859_gshared (Buffer_t1984651551 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method)
{
	Il2CppObject * V_0 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_gate_4();
		V_0 = (Il2CppObject *)L_0;
		Il2CppObject * L_1 = V_0;
		Monitor_Enter_m476686225(NULL /*static, unused*/, (Il2CppObject *)L_1, /*hidden argument*/NULL);
	}

IL_000d:
	try
	{ // begin try (depth: 1)
		try
		{ // begin try (depth: 2)
			Il2CppObject* L_2 = (Il2CppObject*)((OperatorObserverBase_2_t1811255927 *)__this)->get_observer_0();
			il2cpp_codegen_memory_barrier();
			Exception_t1967233988 * L_3 = ___error0;
			NullCheck((Il2CppObject*)L_2);
			InterfaceActionInvoker1< Exception_t1967233988 * >::Invoke(1 /* System.Void UniRx.IObserver`1<System.Collections.Generic.IList`1<System.Int64>>::OnError(System.Exception) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 11), (Il2CppObject*)L_2, (Exception_t1967233988 *)L_3);
			IL2CPP_LEAVE(0x27, FINALLY_0020);
		} // end try (depth: 2)
		catch(Il2CppExceptionWrapper& e)
		{
			__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
			goto FINALLY_0020;
		}

FINALLY_0020:
		{ // begin finally (depth: 2)
			NullCheck((OperatorObserverBase_2_t1811255927 *)__this);
			VirtActionInvoker0::Invoke(4 /* System.Void UniRx.Operators.OperatorObserverBase`2<System.Int64,System.Collections.Generic.IList`1<System.Int64>>::Dispose() */, (OperatorObserverBase_2_t1811255927 *)__this);
			IL2CPP_END_FINALLY(32)
		} // end finally (depth: 2)
		IL2CPP_CLEANUP(32)
		{
			IL2CPP_JUMP_TBL(0x27, IL_0027)
			IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
		}

IL_0027:
		{
			IL2CPP_LEAVE(0x33, FINALLY_002c);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_002c;
	}

FINALLY_002c:
	{ // begin finally (depth: 1)
		Il2CppObject * L_4 = V_0;
		Monitor_Exit_m2088237919(NULL /*static, unused*/, (Il2CppObject *)L_4, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(44)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(44)
	{
		IL2CPP_JUMP_TBL(0x33, IL_0033)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0033:
	{
		return;
	}
}
// System.Void UniRx.Operators.BufferObservable`2/Buffer<System.Int64,System.Int64>::OnCompleted()
extern "C"  void Buffer_OnCompleted_m2466438958_gshared (Buffer_t1984651551 * __this, const MethodInfo* method)
{
	Il2CppObject * V_0 = NULL;
	List_1_t3644373851 * V_1 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_gate_4();
		V_0 = (Il2CppObject *)L_0;
		Il2CppObject * L_1 = V_0;
		Monitor_Enter_m476686225(NULL /*static, unused*/, (Il2CppObject *)L_1, /*hidden argument*/NULL);
	}

IL_000d:
	try
	{ // begin try (depth: 1)
		{
			List_1_t3644373851 * L_2 = (List_1_t3644373851 *)__this->get_list_5();
			V_1 = (List_1_t3644373851 *)L_2;
			List_1_t3644373851 * L_3 = (List_1_t3644373851 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
			((  void (*) (List_1_t3644373851 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->method)(L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
			__this->set_list_5(L_3);
			Il2CppObject* L_4 = (Il2CppObject*)((OperatorObserverBase_2_t1811255927 *)__this)->get_observer_0();
			il2cpp_codegen_memory_barrier();
			List_1_t3644373851 * L_5 = V_1;
			NullCheck((Il2CppObject*)L_4);
			InterfaceActionInvoker1< Il2CppObject* >::Invoke(2 /* System.Void UniRx.IObserver`1<System.Collections.Generic.IList`1<System.Int64>>::OnNext(T) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 11), (Il2CppObject*)L_4, (Il2CppObject*)L_5);
		}

IL_002d:
		try
		{ // begin try (depth: 2)
			Il2CppObject* L_6 = (Il2CppObject*)((OperatorObserverBase_2_t1811255927 *)__this)->get_observer_0();
			il2cpp_codegen_memory_barrier();
			NullCheck((Il2CppObject*)L_6);
			InterfaceActionInvoker0::Invoke(0 /* System.Void UniRx.IObserver`1<System.Collections.Generic.IList`1<System.Int64>>::OnCompleted() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 11), (Il2CppObject*)L_6);
			IL2CPP_LEAVE(0x46, FINALLY_003f);
		} // end try (depth: 2)
		catch(Il2CppExceptionWrapper& e)
		{
			__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
			goto FINALLY_003f;
		}

FINALLY_003f:
		{ // begin finally (depth: 2)
			NullCheck((OperatorObserverBase_2_t1811255927 *)__this);
			VirtActionInvoker0::Invoke(4 /* System.Void UniRx.Operators.OperatorObserverBase`2<System.Int64,System.Collections.Generic.IList`1<System.Int64>>::Dispose() */, (OperatorObserverBase_2_t1811255927 *)__this);
			IL2CPP_END_FINALLY(63)
		} // end finally (depth: 2)
		IL2CPP_CLEANUP(63)
		{
			IL2CPP_JUMP_TBL(0x46, IL_0046)
			IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
		}

IL_0046:
		{
			IL2CPP_LEAVE(0x52, FINALLY_004b);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_004b;
	}

FINALLY_004b:
	{ // begin finally (depth: 1)
		Il2CppObject * L_7 = V_0;
		Monitor_Exit_m2088237919(NULL /*static, unused*/, (Il2CppObject *)L_7, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(75)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(75)
	{
		IL2CPP_JUMP_TBL(0x52, IL_0052)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0052:
	{
		return;
	}
}
// System.Void UniRx.Operators.BufferObservable`2/Buffer<System.Object,System.Object>::.ctor(UniRx.Operators.BufferObservable`2<TSource,TWindowBoundary>,UniRx.IObserver`1<System.Collections.Generic.IList`1<TSource>>,System.IDisposable)
extern Il2CppClass* Il2CppObject_il2cpp_TypeInfo_var;
extern const uint32_t Buffer__ctor_m934385027_MetadataUsageId;
extern "C"  void Buffer__ctor_m934385027_gshared (Buffer_t3527656087 * __this, BufferObservable_2_t2093442205 * ___parent0, Il2CppObject* ___observer1, Il2CppObject * ___cancel2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Buffer__ctor_m934385027_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject * L_0 = (Il2CppObject *)il2cpp_codegen_object_new(Il2CppObject_il2cpp_TypeInfo_var);
		Object__ctor_m1772956182(L_0, /*hidden argument*/NULL);
		__this->set_gate_4(L_0);
		Il2CppObject* L_1 = ___observer1;
		Il2CppObject * L_2 = ___cancel2;
		NullCheck((OperatorObserverBase_2_t3354260463 *)__this);
		((  void (*) (OperatorObserverBase_2_t3354260463 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((OperatorObserverBase_2_t3354260463 *)__this, (Il2CppObject*)L_1, (Il2CppObject *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		BufferObservable_2_t2093442205 * L_3 = ___parent0;
		__this->set_parent_3(L_3);
		return;
	}
}
// System.Void UniRx.Operators.BufferObservable`2/Buffer<System.Object,System.Object>::.cctor()
extern "C"  void Buffer__cctor_m4192606757_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		((Buffer_t3527656087_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->static_fields)->set_EmptyArray_2(((ObjectU5BU5D_t11523773*)SZArrayNew(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), (uint32_t)0)));
		return;
	}
}
// System.IDisposable UniRx.Operators.BufferObservable`2/Buffer<System.Object,System.Object>::Run()
extern "C"  Il2CppObject * Buffer_Run_m1539816790_gshared (Buffer_t3527656087 * __this, const MethodInfo* method)
{
	Il2CppObject * V_0 = NULL;
	Il2CppObject * V_1 = NULL;
	{
		List_1_t1634065389 * L_0 = (List_1_t1634065389 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		((  void (*) (List_1_t1634065389 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->method)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		__this->set_list_5(L_0);
		BufferObservable_2_t2093442205 * L_1 = (BufferObservable_2_t2093442205 *)__this->get_parent_3();
		NullCheck(L_1);
		Il2CppObject* L_2 = (Il2CppObject*)L_1->get_source_1();
		NullCheck((Il2CppObject*)L_2);
		Il2CppObject * L_3 = InterfaceFuncInvoker1< Il2CppObject *, Il2CppObject* >::Invoke(0 /* System.IDisposable UniRx.IObservable`1<System.Object>::Subscribe(UniRx.IObserver`1<T>) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6), (Il2CppObject*)L_2, (Il2CppObject*)__this);
		V_0 = (Il2CppObject *)L_3;
		BufferObservable_2_t2093442205 * L_4 = (BufferObservable_2_t2093442205 *)__this->get_parent_3();
		NullCheck(L_4);
		Il2CppObject* L_5 = (Il2CppObject*)L_4->get_windowBoundaries_2();
		Buffer__t2547187284 * L_6 = (Buffer__t2547187284 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7));
		((  void (*) (Buffer__t2547187284 *, Buffer_t3527656087 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8)->method)(L_6, (Buffer_t3527656087 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8));
		NullCheck((Il2CppObject*)L_5);
		Il2CppObject * L_7 = InterfaceFuncInvoker1< Il2CppObject *, Il2CppObject* >::Invoke(0 /* System.IDisposable UniRx.IObservable`1<System.Object>::Subscribe(UniRx.IObserver`1<T>) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9), (Il2CppObject*)L_5, (Il2CppObject*)L_6);
		V_1 = (Il2CppObject *)L_7;
		Il2CppObject * L_8 = V_0;
		Il2CppObject * L_9 = V_1;
		Il2CppObject * L_10 = StableCompositeDisposable_Create_m3764788535(NULL /*static, unused*/, (Il2CppObject *)L_8, (Il2CppObject *)L_9, /*hidden argument*/NULL);
		return L_10;
	}
}
// System.Void UniRx.Operators.BufferObservable`2/Buffer<System.Object,System.Object>::OnNext(TSource)
extern "C"  void Buffer_OnNext_m3779144981_gshared (Buffer_t3527656087 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	Il2CppObject * V_0 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_gate_4();
		V_0 = (Il2CppObject *)L_0;
		Il2CppObject * L_1 = V_0;
		Monitor_Enter_m476686225(NULL /*static, unused*/, (Il2CppObject *)L_1, /*hidden argument*/NULL);
	}

IL_000d:
	try
	{ // begin try (depth: 1)
		List_1_t1634065389 * L_2 = (List_1_t1634065389 *)__this->get_list_5();
		Il2CppObject * L_3 = ___value0;
		NullCheck((List_1_t1634065389 *)L_2);
		VirtActionInvoker1< Il2CppObject * >::Invoke(22 /* System.Void System.Collections.Generic.List`1<System.Object>::Add(!0) */, (List_1_t1634065389 *)L_2, (Il2CppObject *)L_3);
		IL2CPP_LEAVE(0x25, FINALLY_001e);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_001e;
	}

FINALLY_001e:
	{ // begin finally (depth: 1)
		Il2CppObject * L_4 = V_0;
		Monitor_Exit_m2088237919(NULL /*static, unused*/, (Il2CppObject *)L_4, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(30)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(30)
	{
		IL2CPP_JUMP_TBL(0x25, IL_0025)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0025:
	{
		return;
	}
}
// System.Void UniRx.Operators.BufferObservable`2/Buffer<System.Object,System.Object>::OnError(System.Exception)
extern "C"  void Buffer_OnError_m2330482239_gshared (Buffer_t3527656087 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method)
{
	Il2CppObject * V_0 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_gate_4();
		V_0 = (Il2CppObject *)L_0;
		Il2CppObject * L_1 = V_0;
		Monitor_Enter_m476686225(NULL /*static, unused*/, (Il2CppObject *)L_1, /*hidden argument*/NULL);
	}

IL_000d:
	try
	{ // begin try (depth: 1)
		try
		{ // begin try (depth: 2)
			Il2CppObject* L_2 = (Il2CppObject*)((OperatorObserverBase_2_t3354260463 *)__this)->get_observer_0();
			il2cpp_codegen_memory_barrier();
			Exception_t1967233988 * L_3 = ___error0;
			NullCheck((Il2CppObject*)L_2);
			InterfaceActionInvoker1< Exception_t1967233988 * >::Invoke(1 /* System.Void UniRx.IObserver`1<System.Collections.Generic.IList`1<System.Object>>::OnError(System.Exception) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 11), (Il2CppObject*)L_2, (Exception_t1967233988 *)L_3);
			IL2CPP_LEAVE(0x27, FINALLY_0020);
		} // end try (depth: 2)
		catch(Il2CppExceptionWrapper& e)
		{
			__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
			goto FINALLY_0020;
		}

FINALLY_0020:
		{ // begin finally (depth: 2)
			NullCheck((OperatorObserverBase_2_t3354260463 *)__this);
			VirtActionInvoker0::Invoke(4 /* System.Void UniRx.Operators.OperatorObserverBase`2<System.Object,System.Collections.Generic.IList`1<System.Object>>::Dispose() */, (OperatorObserverBase_2_t3354260463 *)__this);
			IL2CPP_END_FINALLY(32)
		} // end finally (depth: 2)
		IL2CPP_CLEANUP(32)
		{
			IL2CPP_JUMP_TBL(0x27, IL_0027)
			IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
		}

IL_0027:
		{
			IL2CPP_LEAVE(0x33, FINALLY_002c);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_002c;
	}

FINALLY_002c:
	{ // begin finally (depth: 1)
		Il2CppObject * L_4 = V_0;
		Monitor_Exit_m2088237919(NULL /*static, unused*/, (Il2CppObject *)L_4, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(44)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(44)
	{
		IL2CPP_JUMP_TBL(0x33, IL_0033)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0033:
	{
		return;
	}
}
// System.Void UniRx.Operators.BufferObservable`2/Buffer<System.Object,System.Object>::OnCompleted()
extern "C"  void Buffer_OnCompleted_m374288402_gshared (Buffer_t3527656087 * __this, const MethodInfo* method)
{
	Il2CppObject * V_0 = NULL;
	List_1_t1634065389 * V_1 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_gate_4();
		V_0 = (Il2CppObject *)L_0;
		Il2CppObject * L_1 = V_0;
		Monitor_Enter_m476686225(NULL /*static, unused*/, (Il2CppObject *)L_1, /*hidden argument*/NULL);
	}

IL_000d:
	try
	{ // begin try (depth: 1)
		{
			List_1_t1634065389 * L_2 = (List_1_t1634065389 *)__this->get_list_5();
			V_1 = (List_1_t1634065389 *)L_2;
			List_1_t1634065389 * L_3 = (List_1_t1634065389 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
			((  void (*) (List_1_t1634065389 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->method)(L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
			__this->set_list_5(L_3);
			Il2CppObject* L_4 = (Il2CppObject*)((OperatorObserverBase_2_t3354260463 *)__this)->get_observer_0();
			il2cpp_codegen_memory_barrier();
			List_1_t1634065389 * L_5 = V_1;
			NullCheck((Il2CppObject*)L_4);
			InterfaceActionInvoker1< Il2CppObject* >::Invoke(2 /* System.Void UniRx.IObserver`1<System.Collections.Generic.IList`1<System.Object>>::OnNext(T) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 11), (Il2CppObject*)L_4, (Il2CppObject*)L_5);
		}

IL_002d:
		try
		{ // begin try (depth: 2)
			Il2CppObject* L_6 = (Il2CppObject*)((OperatorObserverBase_2_t3354260463 *)__this)->get_observer_0();
			il2cpp_codegen_memory_barrier();
			NullCheck((Il2CppObject*)L_6);
			InterfaceActionInvoker0::Invoke(0 /* System.Void UniRx.IObserver`1<System.Collections.Generic.IList`1<System.Object>>::OnCompleted() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 11), (Il2CppObject*)L_6);
			IL2CPP_LEAVE(0x46, FINALLY_003f);
		} // end try (depth: 2)
		catch(Il2CppExceptionWrapper& e)
		{
			__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
			goto FINALLY_003f;
		}

FINALLY_003f:
		{ // begin finally (depth: 2)
			NullCheck((OperatorObserverBase_2_t3354260463 *)__this);
			VirtActionInvoker0::Invoke(4 /* System.Void UniRx.Operators.OperatorObserverBase`2<System.Object,System.Collections.Generic.IList`1<System.Object>>::Dispose() */, (OperatorObserverBase_2_t3354260463 *)__this);
			IL2CPP_END_FINALLY(63)
		} // end finally (depth: 2)
		IL2CPP_CLEANUP(63)
		{
			IL2CPP_JUMP_TBL(0x46, IL_0046)
			IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
		}

IL_0046:
		{
			IL2CPP_LEAVE(0x52, FINALLY_004b);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_004b;
	}

FINALLY_004b:
	{ // begin finally (depth: 1)
		Il2CppObject * L_7 = V_0;
		Monitor_Exit_m2088237919(NULL /*static, unused*/, (Il2CppObject *)L_7, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(75)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(75)
	{
		IL2CPP_JUMP_TBL(0x52, IL_0052)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0052:
	{
		return;
	}
}
// System.Void UniRx.Operators.BufferObservable`2<System.Int64,System.Int64>::.ctor(UniRx.IObservable`1<TSource>,UniRx.IObservable`1<TWindowBoundary>)
extern "C"  void BufferObservable_2__ctor_m1284182320_gshared (BufferObservable_2_t550437669 * __this, Il2CppObject* ___source0, Il2CppObject* ___windowBoundaries1, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = ___source0;
		bool L_1 = ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)(NULL /*static, unused*/, (Il2CppObject*)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		NullCheck((OperatorObservableBase_1_t4078052167 *)__this);
		((  void (*) (OperatorObservableBase_1_t4078052167 *, bool, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)((OperatorObservableBase_1_t4078052167 *)__this, (bool)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		Il2CppObject* L_2 = ___source0;
		__this->set_source_1(L_2);
		Il2CppObject* L_3 = ___windowBoundaries1;
		__this->set_windowBoundaries_2(L_3);
		return;
	}
}
// System.IDisposable UniRx.Operators.BufferObservable`2<System.Int64,System.Int64>::SubscribeCore(UniRx.IObserver`1<System.Collections.Generic.IList`1<TSource>>,System.IDisposable)
extern "C"  Il2CppObject * BufferObservable_2_SubscribeCore_m666888389_gshared (BufferObservable_2_t550437669 * __this, Il2CppObject* ___observer0, Il2CppObject * ___cancel1, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = ___observer0;
		Il2CppObject * L_1 = ___cancel1;
		Buffer_t1984651551 * L_2 = (Buffer_t1984651551 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		((  void (*) (Buffer_t1984651551 *, BufferObservable_2_t550437669 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)(L_2, (BufferObservable_2_t550437669 *)__this, (Il2CppObject*)L_0, (Il2CppObject *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		NullCheck((Buffer_t1984651551 *)L_2);
		Il2CppObject * L_3 = ((  Il2CppObject * (*) (Buffer_t1984651551 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->method)((Buffer_t1984651551 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		return L_3;
	}
}
// System.Void UniRx.Operators.BufferObservable`2<System.Object,System.Object>::.ctor(UniRx.IObservable`1<TSource>,UniRx.IObservable`1<TWindowBoundary>)
extern "C"  void BufferObservable_2__ctor_m673927692_gshared (BufferObservable_2_t2093442205 * __this, Il2CppObject* ___source0, Il2CppObject* ___windowBoundaries1, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = ___source0;
		bool L_1 = ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)(NULL /*static, unused*/, (Il2CppObject*)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		NullCheck((OperatorObservableBase_1_t2067743705 *)__this);
		((  void (*) (OperatorObservableBase_1_t2067743705 *, bool, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)((OperatorObservableBase_1_t2067743705 *)__this, (bool)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		Il2CppObject* L_2 = ___source0;
		__this->set_source_1(L_2);
		Il2CppObject* L_3 = ___windowBoundaries1;
		__this->set_windowBoundaries_2(L_3);
		return;
	}
}
// System.IDisposable UniRx.Operators.BufferObservable`2<System.Object,System.Object>::SubscribeCore(UniRx.IObserver`1<System.Collections.Generic.IList`1<TSource>>,System.IDisposable)
extern "C"  Il2CppObject * BufferObservable_2_SubscribeCore_m2188808289_gshared (BufferObservable_2_t2093442205 * __this, Il2CppObject* ___observer0, Il2CppObject * ___cancel1, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = ___observer0;
		Il2CppObject * L_1 = ___cancel1;
		Buffer_t3527656087 * L_2 = (Buffer_t3527656087 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		((  void (*) (Buffer_t3527656087 *, BufferObservable_2_t2093442205 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)(L_2, (BufferObservable_2_t2093442205 *)__this, (Il2CppObject*)L_0, (Il2CppObject *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		NullCheck((Buffer_t3527656087 *)L_2);
		Il2CppObject * L_3 = ((  Il2CppObject * (*) (Buffer_t3527656087 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->method)((Buffer_t3527656087 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		return L_3;
	}
}
// System.Void UniRx.Operators.CastObservable`2/Cast<System.Object,System.Object>::.ctor(UniRx.IObserver`1<TResult>,System.IDisposable)
extern "C"  void Cast__ctor_m799924069_gshared (Cast_t3935810676 * __this, Il2CppObject* ___observer0, Il2CppObject * ___cancel1, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = ___observer0;
		Il2CppObject * L_1 = ___cancel1;
		NullCheck((OperatorObserverBase_2_t1187768149 *)__this);
		((  void (*) (OperatorObserverBase_2_t1187768149 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((OperatorObserverBase_2_t1187768149 *)__this, (Il2CppObject*)L_0, (Il2CppObject *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		return;
	}
}
// System.Void UniRx.Operators.CastObservable`2/Cast<System.Object,System.Object>::OnNext(TSource)
extern Il2CppClass* Il2CppObject_il2cpp_TypeInfo_var;
extern Il2CppClass* Exception_t1967233988_il2cpp_TypeInfo_var;
extern const uint32_t Cast_OnNext_m3153415157_MetadataUsageId;
extern "C"  void Cast_OnNext_m3153415157_gshared (Cast_t3935810676 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Cast_OnNext_m3153415157_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Il2CppObject * V_0 = NULL;
	Exception_t1967233988 * V_1 = NULL;
	Il2CppObject * V_2 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Initobj (Il2CppObject_il2cpp_TypeInfo_var, (&V_2));
		Il2CppObject * L_0 = V_2;
		V_0 = (Il2CppObject *)L_0;
	}

IL_000a:
	try
	{ // begin try (depth: 1)
		Il2CppObject * L_1 = ___value0;
		V_0 = (Il2CppObject *)((Il2CppObject *)Castclass(L_1, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)));
		goto IL_0040;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1967233988 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Exception_t1967233988_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_001b;
		throw e;
	}

CATCH_001b:
	{ // begin catch(System.Exception)
		{
			V_1 = (Exception_t1967233988 *)((Exception_t1967233988 *)__exception_local);
		}

IL_001c:
		try
		{ // begin try (depth: 2)
			Il2CppObject* L_2 = (Il2CppObject*)((OperatorObserverBase_2_t1187768149 *)__this)->get_observer_0();
			il2cpp_codegen_memory_barrier();
			Exception_t1967233988 * L_3 = V_1;
			NullCheck((Il2CppObject*)L_2);
			InterfaceActionInvoker1< Exception_t1967233988 * >::Invoke(1 /* System.Void UniRx.IObserver`1<System.Object>::OnError(System.Exception) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4), (Il2CppObject*)L_2, (Exception_t1967233988 *)L_3);
			IL2CPP_LEAVE(0x36, FINALLY_002f);
		} // end try (depth: 2)
		catch(Il2CppExceptionWrapper& e)
		{
			__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
			goto FINALLY_002f;
		}

FINALLY_002f:
		{ // begin finally (depth: 2)
			NullCheck((OperatorObserverBase_2_t1187768149 *)__this);
			VirtActionInvoker0::Invoke(4 /* System.Void UniRx.Operators.OperatorObserverBase`2<System.Object,System.Object>::Dispose() */, (OperatorObserverBase_2_t1187768149 *)__this);
			IL2CPP_END_FINALLY(47)
		} // end finally (depth: 2)
		IL2CPP_CLEANUP(47)
		{
			IL2CPP_JUMP_TBL(0x36, IL_0036)
			IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
		}

IL_0036:
		{
			goto IL_004e;
		}

IL_003b:
		{
			; // IL_003b: leave IL_0040
		}
	} // end catch (depth: 1)

IL_0040:
	{
		Il2CppObject* L_4 = (Il2CppObject*)((OperatorObserverBase_2_t1187768149 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		Il2CppObject * L_5 = V_0;
		NullCheck((Il2CppObject*)L_4);
		InterfaceActionInvoker1< Il2CppObject * >::Invoke(2 /* System.Void UniRx.IObserver`1<System.Object>::OnNext(T) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4), (Il2CppObject*)L_4, (Il2CppObject *)L_5);
	}

IL_004e:
	{
		return;
	}
}
// System.Void UniRx.Operators.CastObservable`2/Cast<System.Object,System.Object>::OnError(System.Exception)
extern "C"  void Cast_OnError_m2846579999_gshared (Cast_t3935810676 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method)
{
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		Il2CppObject* L_0 = (Il2CppObject*)((OperatorObserverBase_2_t1187768149 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		Exception_t1967233988 * L_1 = ___error0;
		NullCheck((Il2CppObject*)L_0);
		InterfaceActionInvoker1< Exception_t1967233988 * >::Invoke(1 /* System.Void UniRx.IObserver`1<System.Object>::OnError(System.Exception) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4), (Il2CppObject*)L_0, (Exception_t1967233988 *)L_1);
		IL2CPP_LEAVE(0x1A, FINALLY_0013);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0013;
	}

FINALLY_0013:
	{ // begin finally (depth: 1)
		NullCheck((OperatorObserverBase_2_t1187768149 *)__this);
		VirtActionInvoker0::Invoke(4 /* System.Void UniRx.Operators.OperatorObserverBase`2<System.Object,System.Object>::Dispose() */, (OperatorObserverBase_2_t1187768149 *)__this);
		IL2CPP_END_FINALLY(19)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(19)
	{
		IL2CPP_JUMP_TBL(0x1A, IL_001a)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_001a:
	{
		return;
	}
}
// System.Void UniRx.Operators.CastObservable`2/Cast<System.Object,System.Object>::OnCompleted()
extern "C"  void Cast_OnCompleted_m1933412082_gshared (Cast_t3935810676 * __this, const MethodInfo* method)
{
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		Il2CppObject* L_0 = (Il2CppObject*)((OperatorObserverBase_2_t1187768149 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		NullCheck((Il2CppObject*)L_0);
		InterfaceActionInvoker0::Invoke(0 /* System.Void UniRx.IObserver`1<System.Object>::OnCompleted() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4), (Il2CppObject*)L_0);
		IL2CPP_LEAVE(0x19, FINALLY_0012);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0012;
	}

FINALLY_0012:
	{ // begin finally (depth: 1)
		NullCheck((OperatorObserverBase_2_t1187768149 *)__this);
		VirtActionInvoker0::Invoke(4 /* System.Void UniRx.Operators.OperatorObserverBase`2<System.Object,System.Object>::Dispose() */, (OperatorObserverBase_2_t1187768149 *)__this);
		IL2CPP_END_FINALLY(18)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(18)
	{
		IL2CPP_JUMP_TBL(0x19, IL_0019)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0019:
	{
		return;
	}
}
// System.Void UniRx.Operators.CastObservable`2<System.Object,System.Object>::.ctor(UniRx.IObservable`1<TSource>)
extern "C"  void CastObservable_2__ctor_m3763410834_gshared (CastObservable_2_t4040585978 * __this, Il2CppObject* ___source0, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = ___source0;
		bool L_1 = ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)(NULL /*static, unused*/, (Il2CppObject*)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		NullCheck((OperatorObservableBase_1_t4196218687 *)__this);
		((  void (*) (OperatorObservableBase_1_t4196218687 *, bool, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)((OperatorObservableBase_1_t4196218687 *)__this, (bool)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		Il2CppObject* L_2 = ___source0;
		__this->set_source_1(L_2);
		return;
	}
}
// System.IDisposable UniRx.Operators.CastObservable`2<System.Object,System.Object>::SubscribeCore(UniRx.IObserver`1<TResult>,System.IDisposable)
extern "C"  Il2CppObject * CastObservable_2_SubscribeCore_m3071551731_gshared (CastObservable_2_t4040585978 * __this, Il2CppObject* ___observer0, Il2CppObject * ___cancel1, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = (Il2CppObject*)__this->get_source_1();
		Il2CppObject* L_1 = ___observer0;
		Il2CppObject * L_2 = ___cancel1;
		Cast_t3935810676 * L_3 = (Cast_t3935810676 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		((  void (*) (Cast_t3935810676 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)(L_3, (Il2CppObject*)L_1, (Il2CppObject *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		NullCheck((Il2CppObject*)L_0);
		Il2CppObject * L_4 = InterfaceFuncInvoker1< Il2CppObject *, Il2CppObject* >::Invoke(0 /* System.IDisposable UniRx.IObservable`1<System.Object>::Subscribe(UniRx.IObserver`1<T>) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5), (Il2CppObject*)L_0, (Il2CppObject*)L_3);
		return L_4;
	}
}
// System.Void UniRx.Operators.CatchObservable`1/Catch<System.Object>::.ctor(UniRx.Operators.CatchObservable`1<T>,UniRx.IObserver`1<T>,System.IDisposable)
extern Il2CppClass* Il2CppObject_il2cpp_TypeInfo_var;
extern const uint32_t Catch__ctor_m1707511405_MetadataUsageId;
extern "C"  void Catch__ctor_m1707511405_gshared (Catch_t3617674332 * __this, CatchObservable_1_t1624243829 * ___parent0, Il2CppObject* ___observer1, Il2CppObject * ___cancel2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Catch__ctor_m1707511405_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject * L_0 = (Il2CppObject *)il2cpp_codegen_object_new(Il2CppObject_il2cpp_TypeInfo_var);
		Object__ctor_m1772956182(L_0, /*hidden argument*/NULL);
		__this->set_gate_3(L_0);
		Il2CppObject* L_1 = ___observer1;
		Il2CppObject * L_2 = ___cancel2;
		NullCheck((OperatorObserverBase_2_t1187768149 *)__this);
		((  void (*) (OperatorObserverBase_2_t1187768149 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((OperatorObserverBase_2_t1187768149 *)__this, (Il2CppObject*)L_1, (Il2CppObject *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		CatchObservable_1_t1624243829 * L_3 = ___parent0;
		__this->set_parent_2(L_3);
		return;
	}
}
// System.IDisposable UniRx.Operators.CatchObservable`1/Catch<System.Object>::Run()
extern Il2CppClass* SerialDisposable_t2547852742_il2cpp_TypeInfo_var;
extern Il2CppClass* Action_1_t585976652_il2cpp_TypeInfo_var;
extern Il2CppClass* Scheduler_t103934925_il2cpp_TypeInfo_var;
extern Il2CppClass* Action_t437523947_il2cpp_TypeInfo_var;
extern Il2CppClass* Disposable_t3736388786_il2cpp_TypeInfo_var;
extern const MethodInfo* Action_1__ctor_m3589476370_MethodInfo_var;
extern const uint32_t Catch_Run_m3086992037_MetadataUsageId;
extern "C"  Il2CppObject * Catch_Run_m3086992037_gshared (Catch_t3617674332 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Catch_Run_m3086992037_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Il2CppObject * V_0 = NULL;
	{
		__this->set_isDisposed_4((bool)0);
		CatchObservable_1_t1624243829 * L_0 = (CatchObservable_1_t1624243829 *)__this->get_parent_2();
		NullCheck(L_0);
		Il2CppObject* L_1 = (Il2CppObject*)L_0->get_sources_1();
		NullCheck((Il2CppObject*)L_1);
		Il2CppObject* L_2 = InterfaceFuncInvoker0< Il2CppObject* >::Invoke(0 /* System.Collections.Generic.IEnumerator`1<!0> System.Collections.Generic.IEnumerable`1<UniRx.IObservable`1<System.Object>>::GetEnumerator() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), (Il2CppObject*)L_1);
		__this->set_e_5(L_2);
		SerialDisposable_t2547852742 * L_3 = (SerialDisposable_t2547852742 *)il2cpp_codegen_object_new(SerialDisposable_t2547852742_il2cpp_TypeInfo_var);
		SerialDisposable__ctor_m2977852291(L_3, /*hidden argument*/NULL);
		__this->set_subscription_6(L_3);
		Il2CppObject * L_4 = DefaultSchedulers_get_TailRecursion_m2924964748(NULL /*static, unused*/, /*hidden argument*/NULL);
		IntPtr_t L_5;
		L_5.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		Action_1_t585976652 * L_6 = (Action_1_t585976652 *)il2cpp_codegen_object_new(Action_1_t585976652_il2cpp_TypeInfo_var);
		Action_1__ctor_m3589476370(L_6, (Il2CppObject *)__this, (IntPtr_t)L_5, /*hidden argument*/Action_1__ctor_m3589476370_MethodInfo_var);
		IL2CPP_RUNTIME_CLASS_INIT(Scheduler_t103934925_il2cpp_TypeInfo_var);
		Il2CppObject * L_7 = Scheduler_Schedule_m2919836901(NULL /*static, unused*/, (Il2CppObject *)L_4, (Action_1_t585976652 *)L_6, /*hidden argument*/NULL);
		V_0 = (Il2CppObject *)L_7;
		Il2CppObject * L_8 = V_0;
		SerialDisposable_t2547852742 * L_9 = (SerialDisposable_t2547852742 *)__this->get_subscription_6();
		IntPtr_t L_10;
		L_10.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		Action_t437523947 * L_11 = (Action_t437523947 *)il2cpp_codegen_object_new(Action_t437523947_il2cpp_TypeInfo_var);
		Action__ctor_m2957240604(L_11, (Il2CppObject *)__this, (IntPtr_t)L_10, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Disposable_t3736388786_il2cpp_TypeInfo_var);
		Il2CppObject * L_12 = Disposable_Create_m2910846009(NULL /*static, unused*/, (Action_t437523947 *)L_11, /*hidden argument*/NULL);
		Il2CppObject * L_13 = StableCompositeDisposable_Create_m2505029765(NULL /*static, unused*/, (Il2CppObject *)L_8, (Il2CppObject *)L_9, (Il2CppObject *)L_12, /*hidden argument*/NULL);
		return L_13;
	}
}
// System.Void UniRx.Operators.CatchObservable`1/Catch<System.Object>::RecursiveRun(System.Action)
extern Il2CppClass* IEnumerator_t287207039_il2cpp_TypeInfo_var;
extern Il2CppClass* InvalidOperationException_t2420574324_il2cpp_TypeInfo_var;
extern Il2CppClass* IDisposable_t1628921374_il2cpp_TypeInfo_var;
extern Il2CppClass* Exception_t1967233988_il2cpp_TypeInfo_var;
extern Il2CppClass* SingleAssignmentDisposable_t2336378823_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2696517264;
extern const uint32_t Catch_RecursiveRun_m2866324535_MetadataUsageId;
extern "C"  void Catch_RecursiveRun_m2866324535_gshared (Catch_t3617674332 * __this, Action_t437523947 * ___self0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Catch_RecursiveRun_m2866324535_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Il2CppObject * V_0 = NULL;
	Il2CppObject* V_1 = NULL;
	bool V_2 = false;
	Exception_t1967233988 * V_3 = NULL;
	Exception_t1967233988 * V_4 = NULL;
	Il2CppObject* V_5 = NULL;
	SingleAssignmentDisposable_t2336378823 * V_6 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_gate_3();
		V_0 = (Il2CppObject *)L_0;
		Il2CppObject * L_1 = V_0;
		Monitor_Enter_m476686225(NULL /*static, unused*/, (Il2CppObject *)L_1, /*hidden argument*/NULL);
	}

IL_000d:
	try
	{ // begin try (depth: 1)
		{
			Action_t437523947 * L_2 = ___self0;
			__this->set_nextSelf_8(L_2);
			bool L_3 = (bool)__this->get_isDisposed_4();
			if (!L_3)
			{
				goto IL_0024;
			}
		}

IL_001f:
		{
			IL2CPP_LEAVE(0x12D, FINALLY_0126);
		}

IL_0024:
		{
			V_1 = (Il2CppObject*)NULL;
			V_2 = (bool)0;
			V_3 = (Exception_t1967233988 *)NULL;
		}

IL_002a:
		try
		{ // begin try (depth: 2)
			{
				Il2CppObject* L_4 = (Il2CppObject*)__this->get_e_5();
				NullCheck((Il2CppObject *)L_4);
				bool L_5 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t287207039_il2cpp_TypeInfo_var, (Il2CppObject *)L_4);
				V_2 = (bool)L_5;
				bool L_6 = V_2;
				if (!L_6)
				{
					goto IL_005e;
				}
			}

IL_003c:
			{
				Il2CppObject* L_7 = (Il2CppObject*)__this->get_e_5();
				NullCheck((Il2CppObject*)L_7);
				Il2CppObject* L_8 = InterfaceFuncInvoker0< Il2CppObject* >::Invoke(0 /* !0 System.Collections.Generic.IEnumerator`1<UniRx.IObservable`1<System.Object>>::get_Current() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5), (Il2CppObject*)L_7);
				V_1 = (Il2CppObject*)L_8;
				Il2CppObject* L_9 = V_1;
				if (L_9)
				{
					goto IL_0059;
				}
			}

IL_004e:
			{
				InvalidOperationException_t2420574324 * L_10 = (InvalidOperationException_t2420574324 *)il2cpp_codegen_object_new(InvalidOperationException_t2420574324_il2cpp_TypeInfo_var);
				InvalidOperationException__ctor_m1485483280(L_10, (String_t*)_stringLiteral2696517264, /*hidden argument*/NULL);
				IL2CPP_RAISE_MANAGED_EXCEPTION(L_10);
			}

IL_0059:
			{
				goto IL_0069;
			}

IL_005e:
			{
				Il2CppObject* L_11 = (Il2CppObject*)__this->get_e_5();
				NullCheck((Il2CppObject *)L_11);
				InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, (Il2CppObject *)L_11);
			}

IL_0069:
			{
				goto IL_0083;
			}
		} // end try (depth: 2)
		catch(Il2CppExceptionWrapper& e)
		{
			__exception_local = (Exception_t1967233988 *)e.ex;
			if(il2cpp_codegen_class_is_assignable_from (Exception_t1967233988_il2cpp_TypeInfo_var, e.ex->object.klass))
				goto CATCH_006e;
			throw e;
		}

CATCH_006e:
		{ // begin catch(System.Exception)
			V_4 = (Exception_t1967233988 *)((Exception_t1967233988 *)__exception_local);
			Exception_t1967233988 * L_12 = V_4;
			V_3 = (Exception_t1967233988 *)L_12;
			Il2CppObject* L_13 = (Il2CppObject*)__this->get_e_5();
			NullCheck((Il2CppObject *)L_13);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, (Il2CppObject *)L_13);
			goto IL_0083;
		} // end catch (depth: 2)

IL_0083:
		{
			Exception_t1967233988 * L_14 = V_3;
			if (!L_14)
			{
				goto IL_00a8;
			}
		}

IL_0089:
		try
		{ // begin try (depth: 2)
			Il2CppObject* L_15 = (Il2CppObject*)((OperatorObserverBase_2_t1187768149 *)__this)->get_observer_0();
			il2cpp_codegen_memory_barrier();
			Exception_t1967233988 * L_16 = V_3;
			NullCheck((Il2CppObject*)L_15);
			InterfaceActionInvoker1< Exception_t1967233988 * >::Invoke(1 /* System.Void UniRx.IObserver`1<System.Object>::OnError(System.Exception) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6), (Il2CppObject*)L_15, (Exception_t1967233988 *)L_16);
			IL2CPP_LEAVE(0xA3, FINALLY_009c);
		} // end try (depth: 2)
		catch(Il2CppExceptionWrapper& e)
		{
			__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
			goto FINALLY_009c;
		}

FINALLY_009c:
		{ // begin finally (depth: 2)
			NullCheck((OperatorObserverBase_2_t1187768149 *)__this);
			VirtActionInvoker0::Invoke(4 /* System.Void UniRx.Operators.OperatorObserverBase`2<System.Object,System.Object>::Dispose() */, (OperatorObserverBase_2_t1187768149 *)__this);
			IL2CPP_END_FINALLY(156)
		} // end finally (depth: 2)
		IL2CPP_CLEANUP(156)
		{
			IL2CPP_JUMP_TBL(0xA3, IL_00a3)
			IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
		}

IL_00a3:
		{
			IL2CPP_LEAVE(0x12D, FINALLY_0126);
		}

IL_00a8:
		{
			bool L_17 = V_2;
			if (L_17)
			{
				goto IL_00fb;
			}
		}

IL_00ae:
		{
			Exception_t1967233988 * L_18 = (Exception_t1967233988 *)__this->get_lastException_7();
			if (!L_18)
			{
				goto IL_00dd;
			}
		}

IL_00b9:
		try
		{ // begin try (depth: 2)
			Il2CppObject* L_19 = (Il2CppObject*)((OperatorObserverBase_2_t1187768149 *)__this)->get_observer_0();
			il2cpp_codegen_memory_barrier();
			Exception_t1967233988 * L_20 = (Exception_t1967233988 *)__this->get_lastException_7();
			NullCheck((Il2CppObject*)L_19);
			InterfaceActionInvoker1< Exception_t1967233988 * >::Invoke(1 /* System.Void UniRx.IObserver`1<System.Object>::OnError(System.Exception) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6), (Il2CppObject*)L_19, (Exception_t1967233988 *)L_20);
			IL2CPP_LEAVE(0xD8, FINALLY_00d1);
		} // end try (depth: 2)
		catch(Il2CppExceptionWrapper& e)
		{
			__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
			goto FINALLY_00d1;
		}

FINALLY_00d1:
		{ // begin finally (depth: 2)
			NullCheck((OperatorObserverBase_2_t1187768149 *)__this);
			VirtActionInvoker0::Invoke(4 /* System.Void UniRx.Operators.OperatorObserverBase`2<System.Object,System.Object>::Dispose() */, (OperatorObserverBase_2_t1187768149 *)__this);
			IL2CPP_END_FINALLY(209)
		} // end finally (depth: 2)
		IL2CPP_CLEANUP(209)
		{
			IL2CPP_JUMP_TBL(0xD8, IL_00d8)
			IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
		}

IL_00d8:
		{
			goto IL_00f6;
		}

IL_00dd:
		try
		{ // begin try (depth: 2)
			Il2CppObject* L_21 = (Il2CppObject*)((OperatorObserverBase_2_t1187768149 *)__this)->get_observer_0();
			il2cpp_codegen_memory_barrier();
			NullCheck((Il2CppObject*)L_21);
			InterfaceActionInvoker0::Invoke(0 /* System.Void UniRx.IObserver`1<System.Object>::OnCompleted() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6), (Il2CppObject*)L_21);
			IL2CPP_LEAVE(0xF6, FINALLY_00ef);
		} // end try (depth: 2)
		catch(Il2CppExceptionWrapper& e)
		{
			__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
			goto FINALLY_00ef;
		}

FINALLY_00ef:
		{ // begin finally (depth: 2)
			NullCheck((OperatorObserverBase_2_t1187768149 *)__this);
			VirtActionInvoker0::Invoke(4 /* System.Void UniRx.Operators.OperatorObserverBase`2<System.Object,System.Object>::Dispose() */, (OperatorObserverBase_2_t1187768149 *)__this);
			IL2CPP_END_FINALLY(239)
		} // end finally (depth: 2)
		IL2CPP_CLEANUP(239)
		{
			IL2CPP_JUMP_TBL(0xF6, IL_00f6)
			IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
		}

IL_00f6:
		{
			IL2CPP_LEAVE(0x12D, FINALLY_0126);
		}

IL_00fb:
		{
			Il2CppObject* L_22 = V_1;
			V_5 = (Il2CppObject*)L_22;
			SingleAssignmentDisposable_t2336378823 * L_23 = (SingleAssignmentDisposable_t2336378823 *)il2cpp_codegen_object_new(SingleAssignmentDisposable_t2336378823_il2cpp_TypeInfo_var);
			SingleAssignmentDisposable__ctor_m917449122(L_23, /*hidden argument*/NULL);
			V_6 = (SingleAssignmentDisposable_t2336378823 *)L_23;
			SerialDisposable_t2547852742 * L_24 = (SerialDisposable_t2547852742 *)__this->get_subscription_6();
			SingleAssignmentDisposable_t2336378823 * L_25 = V_6;
			NullCheck((SerialDisposable_t2547852742 *)L_24);
			SerialDisposable_set_Disposable_m1438046228((SerialDisposable_t2547852742 *)L_24, (Il2CppObject *)L_25, /*hidden argument*/NULL);
			SingleAssignmentDisposable_t2336378823 * L_26 = V_6;
			Il2CppObject* L_27 = V_5;
			NullCheck((Il2CppObject*)L_27);
			Il2CppObject * L_28 = InterfaceFuncInvoker1< Il2CppObject *, Il2CppObject* >::Invoke(0 /* System.IDisposable UniRx.IObservable`1<System.Object>::Subscribe(UniRx.IObserver`1<T>) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8), (Il2CppObject*)L_27, (Il2CppObject*)__this);
			NullCheck((SingleAssignmentDisposable_t2336378823 *)L_26);
			SingleAssignmentDisposable_set_Disposable_m1599028885((SingleAssignmentDisposable_t2336378823 *)L_26, (Il2CppObject *)L_28, /*hidden argument*/NULL);
			IL2CPP_LEAVE(0x12D, FINALLY_0126);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0126;
	}

FINALLY_0126:
	{ // begin finally (depth: 1)
		Il2CppObject * L_29 = V_0;
		Monitor_Exit_m2088237919(NULL /*static, unused*/, (Il2CppObject *)L_29, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(294)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(294)
	{
		IL2CPP_JUMP_TBL(0x12D, IL_012d)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_012d:
	{
		return;
	}
}
// System.Void UniRx.Operators.CatchObservable`1/Catch<System.Object>::OnNext(T)
extern "C"  void Catch_OnNext_m4177295103_gshared (Catch_t3617674332 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = (Il2CppObject*)((OperatorObserverBase_2_t1187768149 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		Il2CppObject * L_1 = ___value0;
		NullCheck((Il2CppObject*)L_0);
		InterfaceActionInvoker1< Il2CppObject * >::Invoke(2 /* System.Void UniRx.IObserver`1<System.Object>::OnNext(T) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6), (Il2CppObject*)L_0, (Il2CppObject *)L_1);
		return;
	}
}
// System.Void UniRx.Operators.CatchObservable`1/Catch<System.Object>::OnError(System.Exception)
extern "C"  void Catch_OnError_m298365966_gshared (Catch_t3617674332 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method)
{
	{
		Exception_t1967233988 * L_0 = ___error0;
		__this->set_lastException_7(L_0);
		Action_t437523947 * L_1 = (Action_t437523947 *)__this->get_nextSelf_8();
		NullCheck((Action_t437523947 *)L_1);
		Action_Invoke_m1445970038((Action_t437523947 *)L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UniRx.Operators.CatchObservable`1/Catch<System.Object>::OnCompleted()
extern "C"  void Catch_OnCompleted_m4243229793_gshared (Catch_t3617674332 * __this, const MethodInfo* method)
{
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		Il2CppObject* L_0 = (Il2CppObject*)((OperatorObserverBase_2_t1187768149 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		NullCheck((Il2CppObject*)L_0);
		InterfaceActionInvoker0::Invoke(0 /* System.Void UniRx.IObserver`1<System.Object>::OnCompleted() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6), (Il2CppObject*)L_0);
		IL2CPP_LEAVE(0x19, FINALLY_0012);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0012;
	}

FINALLY_0012:
	{ // begin finally (depth: 1)
		NullCheck((OperatorObserverBase_2_t1187768149 *)__this);
		VirtActionInvoker0::Invoke(4 /* System.Void UniRx.Operators.OperatorObserverBase`2<System.Object,System.Object>::Dispose() */, (OperatorObserverBase_2_t1187768149 *)__this);
		IL2CPP_END_FINALLY(18)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(18)
	{
		IL2CPP_JUMP_TBL(0x19, IL_0019)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0019:
	{
		return;
	}
}
// System.Void UniRx.Operators.CatchObservable`1/Catch<System.Object>::<Run>m__77()
extern Il2CppClass* IDisposable_t1628921374_il2cpp_TypeInfo_var;
extern const uint32_t Catch_U3CRunU3Em__77_m3241015371_MetadataUsageId;
extern "C"  void Catch_U3CRunU3Em__77_m3241015371_gshared (Catch_t3617674332 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Catch_U3CRunU3Em__77_m3241015371_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Il2CppObject * V_0 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_gate_3();
		V_0 = (Il2CppObject *)L_0;
		Il2CppObject * L_1 = V_0;
		Monitor_Enter_m476686225(NULL /*static, unused*/, (Il2CppObject *)L_1, /*hidden argument*/NULL);
	}

IL_000d:
	try
	{ // begin try (depth: 1)
		__this->set_isDisposed_4((bool)1);
		Il2CppObject* L_2 = (Il2CppObject*)__this->get_e_5();
		NullCheck((Il2CppObject *)L_2);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, (Il2CppObject *)L_2);
		IL2CPP_LEAVE(0x2B, FINALLY_0024);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0024;
	}

FINALLY_0024:
	{ // begin finally (depth: 1)
		Il2CppObject * L_3 = V_0;
		Monitor_Exit_m2088237919(NULL /*static, unused*/, (Il2CppObject *)L_3, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(36)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(36)
	{
		IL2CPP_JUMP_TBL(0x2B, IL_002b)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_002b:
	{
		return;
	}
}
// System.Void UniRx.Operators.CatchObservable`1<System.Object>::.ctor(System.Collections.Generic.IEnumerable`1<UniRx.IObservable`1<T>>)
extern "C"  void CatchObservable_1__ctor_m727030327_gshared (CatchObservable_1_t1624243829 * __this, Il2CppObject* ___sources0, const MethodInfo* method)
{
	{
		NullCheck((OperatorObservableBase_1_t4196218687 *)__this);
		((  void (*) (OperatorObservableBase_1_t4196218687 *, bool, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((OperatorObservableBase_1_t4196218687 *)__this, (bool)1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		Il2CppObject* L_0 = ___sources0;
		__this->set_sources_1(L_0);
		return;
	}
}
// System.IDisposable UniRx.Operators.CatchObservable`1<System.Object>::SubscribeCore(UniRx.IObserver`1<T>,System.IDisposable)
extern "C"  Il2CppObject * CatchObservable_1_SubscribeCore_m3817746035_gshared (CatchObservable_1_t1624243829 * __this, Il2CppObject* ___observer0, Il2CppObject * ___cancel1, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = ___observer0;
		Il2CppObject * L_1 = ___cancel1;
		Catch_t3617674332 * L_2 = (Catch_t3617674332 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		((  void (*) (Catch_t3617674332 *, CatchObservable_1_t1624243829 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)(L_2, (CatchObservable_1_t1624243829 *)__this, (Il2CppObject*)L_0, (Il2CppObject *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		NullCheck((Catch_t3617674332 *)L_2);
		Il2CppObject * L_3 = ((  Il2CppObject * (*) (Catch_t3617674332 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)((Catch_t3617674332 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		return L_3;
	}
}
// System.Void UniRx.Operators.CatchObservable`2/Catch<System.Object,System.Object>::.ctor(UniRx.Operators.CatchObservable`2<T,TException>,UniRx.IObserver`1<T>,System.IDisposable)
extern "C"  void Catch__ctor_m3596504964_gshared (Catch_t841129384 * __this, CatchObservable_2_t3974704686 * ___parent0, Il2CppObject* ___observer1, Il2CppObject * ___cancel2, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = ___observer1;
		Il2CppObject * L_1 = ___cancel2;
		NullCheck((OperatorObserverBase_2_t1187768149 *)__this);
		((  void (*) (OperatorObserverBase_2_t1187768149 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((OperatorObserverBase_2_t1187768149 *)__this, (Il2CppObject*)L_0, (Il2CppObject *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		CatchObservable_2_t3974704686 * L_2 = ___parent0;
		__this->set_parent_2(L_2);
		return;
	}
}
// System.IDisposable UniRx.Operators.CatchObservable`2/Catch<System.Object,System.Object>::Run()
extern Il2CppClass* SerialDisposable_t2547852742_il2cpp_TypeInfo_var;
extern const uint32_t Catch_Run_m3280276056_MetadataUsageId;
extern "C"  Il2CppObject * Catch_Run_m3280276056_gshared (Catch_t841129384 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Catch_Run_m3280276056_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		SerialDisposable_t2547852742 * L_0 = (SerialDisposable_t2547852742 *)il2cpp_codegen_object_new(SerialDisposable_t2547852742_il2cpp_TypeInfo_var);
		SerialDisposable__ctor_m2977852291(L_0, /*hidden argument*/NULL);
		__this->set_serialDisposable_3(L_0);
		SerialDisposable_t2547852742 * L_1 = (SerialDisposable_t2547852742 *)__this->get_serialDisposable_3();
		CatchObservable_2_t3974704686 * L_2 = (CatchObservable_2_t3974704686 *)__this->get_parent_2();
		NullCheck(L_2);
		Il2CppObject* L_3 = (Il2CppObject*)L_2->get_source_1();
		NullCheck((Il2CppObject*)L_3);
		Il2CppObject * L_4 = InterfaceFuncInvoker1< Il2CppObject *, Il2CppObject* >::Invoke(0 /* System.IDisposable UniRx.IObservable`1<System.Object>::Subscribe(UniRx.IObserver`1<T>) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), (Il2CppObject*)L_3, (Il2CppObject*)__this);
		NullCheck((SerialDisposable_t2547852742 *)L_1);
		SerialDisposable_set_Disposable_m1438046228((SerialDisposable_t2547852742 *)L_1, (Il2CppObject *)L_4, /*hidden argument*/NULL);
		SerialDisposable_t2547852742 * L_5 = (SerialDisposable_t2547852742 *)__this->get_serialDisposable_3();
		return L_5;
	}
}
// System.Void UniRx.Operators.CatchObservable`2/Catch<System.Object,System.Object>::OnNext(T)
extern "C"  void Catch_OnNext_m3651359218_gshared (Catch_t841129384 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = (Il2CppObject*)((OperatorObserverBase_2_t1187768149 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		Il2CppObject * L_1 = ___value0;
		NullCheck((Il2CppObject*)L_0);
		InterfaceActionInvoker1< Il2CppObject * >::Invoke(2 /* System.Void UniRx.IObserver`1<System.Object>::OnNext(T) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3), (Il2CppObject*)L_0, (Il2CppObject *)L_1);
		return;
	}
}
// System.Void UniRx.Operators.CatchObservable`2/Catch<System.Object,System.Object>::OnError(System.Exception)
extern Il2CppClass* Observable_t258071701_il2cpp_TypeInfo_var;
extern Il2CppClass* Exception_t1967233988_il2cpp_TypeInfo_var;
extern Il2CppClass* SingleAssignmentDisposable_t2336378823_il2cpp_TypeInfo_var;
extern const uint32_t Catch_OnError_m515459329_MetadataUsageId;
extern "C"  void Catch_OnError_m515459329_gshared (Catch_t841129384 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Catch_OnError_m515459329_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Il2CppObject * V_0 = NULL;
	Il2CppObject* V_1 = NULL;
	Exception_t1967233988 * V_2 = NULL;
	SingleAssignmentDisposable_t2336378823 * V_3 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Exception_t1967233988 * L_0 = ___error0;
		V_0 = (Il2CppObject *)((Il2CppObject *)Castclass(((Il2CppObject *)IsInst(L_0, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4))), IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)));
		Il2CppObject * L_1 = V_0;
		if (!L_1)
		{
			goto IL_00aa;
		}
	}

IL_0017:
	try
	{ // begin try (depth: 1)
		{
			CatchObservable_2_t3974704686 * L_2 = (CatchObservable_2_t3974704686 *)__this->get_parent_2();
			NullCheck(L_2);
			Func_2_t1894581716 * L_3 = (Func_2_t1894581716 *)L_2->get_errorHandler_2();
			IntPtr_t L_4;
			L_4.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
			Func_2_t1894581716 * L_5 = (Func_2_t1894581716 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
			((  void (*) (Func_2_t1894581716 *, Il2CppObject *, IntPtr_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7)->method)(L_5, (Il2CppObject *)NULL, (IntPtr_t)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7));
			bool L_6 = MulticastDelegate_op_Equality_m654490750(NULL /*static, unused*/, (MulticastDelegate_t2585444626 *)L_3, (MulticastDelegate_t2585444626 *)L_5, /*hidden argument*/NULL);
			if (!L_6)
			{
				goto IL_0043;
			}
		}

IL_0038:
		{
			IL2CPP_RUNTIME_CLASS_INIT(Observable_t258071701_il2cpp_TypeInfo_var);
			Il2CppObject* L_7 = ((  Il2CppObject* (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8)->method)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8));
			V_1 = (Il2CppObject*)L_7;
			goto IL_0055;
		}

IL_0043:
		{
			CatchObservable_2_t3974704686 * L_8 = (CatchObservable_2_t3974704686 *)__this->get_parent_2();
			NullCheck(L_8);
			Func_2_t1894581716 * L_9 = (Func_2_t1894581716 *)L_8->get_errorHandler_2();
			Il2CppObject * L_10 = V_0;
			NullCheck((Func_2_t1894581716 *)L_9);
			Il2CppObject* L_11 = ((  Il2CppObject* (*) (Func_2_t1894581716 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9)->method)((Func_2_t1894581716 *)L_9, (Il2CppObject *)L_10, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9));
			V_1 = (Il2CppObject*)L_11;
		}

IL_0055:
		{
			goto IL_007f;
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1967233988 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Exception_t1967233988_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_005a;
		throw e;
	}

CATCH_005a:
	{ // begin catch(System.Exception)
		{
			V_2 = (Exception_t1967233988 *)((Exception_t1967233988 *)__exception_local);
		}

IL_005b:
		try
		{ // begin try (depth: 2)
			Il2CppObject* L_12 = (Il2CppObject*)((OperatorObserverBase_2_t1187768149 *)__this)->get_observer_0();
			il2cpp_codegen_memory_barrier();
			Exception_t1967233988 * L_13 = V_2;
			NullCheck((Il2CppObject*)L_12);
			InterfaceActionInvoker1< Exception_t1967233988 * >::Invoke(1 /* System.Void UniRx.IObserver`1<System.Object>::OnError(System.Exception) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3), (Il2CppObject*)L_12, (Exception_t1967233988 *)L_13);
			IL2CPP_LEAVE(0x75, FINALLY_006e);
		} // end try (depth: 2)
		catch(Il2CppExceptionWrapper& e)
		{
			__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
			goto FINALLY_006e;
		}

FINALLY_006e:
		{ // begin finally (depth: 2)
			NullCheck((OperatorObserverBase_2_t1187768149 *)__this);
			VirtActionInvoker0::Invoke(4 /* System.Void UniRx.Operators.OperatorObserverBase`2<System.Object,System.Object>::Dispose() */, (OperatorObserverBase_2_t1187768149 *)__this);
			IL2CPP_END_FINALLY(110)
		} // end finally (depth: 2)
		IL2CPP_CLEANUP(110)
		{
			IL2CPP_JUMP_TBL(0x75, IL_0075)
			IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
		}

IL_0075:
		{
			goto IL_00c5;
		}

IL_007a:
		{
			; // IL_007a: leave IL_007f
		}
	} // end catch (depth: 1)

IL_007f:
	{
		SingleAssignmentDisposable_t2336378823 * L_14 = (SingleAssignmentDisposable_t2336378823 *)il2cpp_codegen_object_new(SingleAssignmentDisposable_t2336378823_il2cpp_TypeInfo_var);
		SingleAssignmentDisposable__ctor_m917449122(L_14, /*hidden argument*/NULL);
		V_3 = (SingleAssignmentDisposable_t2336378823 *)L_14;
		SerialDisposable_t2547852742 * L_15 = (SerialDisposable_t2547852742 *)__this->get_serialDisposable_3();
		SingleAssignmentDisposable_t2336378823 * L_16 = V_3;
		NullCheck((SerialDisposable_t2547852742 *)L_15);
		SerialDisposable_set_Disposable_m1438046228((SerialDisposable_t2547852742 *)L_15, (Il2CppObject *)L_16, /*hidden argument*/NULL);
		SingleAssignmentDisposable_t2336378823 * L_17 = V_3;
		Il2CppObject* L_18 = V_1;
		Il2CppObject* L_19 = (Il2CppObject*)((OperatorObserverBase_2_t1187768149 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		NullCheck((Il2CppObject*)L_18);
		Il2CppObject * L_20 = InterfaceFuncInvoker1< Il2CppObject *, Il2CppObject* >::Invoke(0 /* System.IDisposable UniRx.IObservable`1<System.Object>::Subscribe(UniRx.IObserver`1<T>) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), (Il2CppObject*)L_18, (Il2CppObject*)L_19);
		NullCheck((SingleAssignmentDisposable_t2336378823 *)L_17);
		SingleAssignmentDisposable_set_Disposable_m1599028885((SingleAssignmentDisposable_t2336378823 *)L_17, (Il2CppObject *)L_20, /*hidden argument*/NULL);
		goto IL_00c5;
	}

IL_00aa:
	try
	{ // begin try (depth: 1)
		Il2CppObject* L_21 = (Il2CppObject*)((OperatorObserverBase_2_t1187768149 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		Exception_t1967233988 * L_22 = ___error0;
		NullCheck((Il2CppObject*)L_21);
		InterfaceActionInvoker1< Exception_t1967233988 * >::Invoke(1 /* System.Void UniRx.IObserver`1<System.Object>::OnError(System.Exception) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3), (Il2CppObject*)L_21, (Exception_t1967233988 *)L_22);
		IL2CPP_LEAVE(0xC4, FINALLY_00bd);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_00bd;
	}

FINALLY_00bd:
	{ // begin finally (depth: 1)
		NullCheck((OperatorObserverBase_2_t1187768149 *)__this);
		VirtActionInvoker0::Invoke(4 /* System.Void UniRx.Operators.OperatorObserverBase`2<System.Object,System.Object>::Dispose() */, (OperatorObserverBase_2_t1187768149 *)__this);
		IL2CPP_END_FINALLY(189)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(189)
	{
		IL2CPP_JUMP_TBL(0xC4, IL_00c4)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_00c4:
	{
		return;
	}

IL_00c5:
	{
		return;
	}
}
// System.Void UniRx.Operators.CatchObservable`2/Catch<System.Object,System.Object>::OnCompleted()
extern "C"  void Catch_OnCompleted_m670348756_gshared (Catch_t841129384 * __this, const MethodInfo* method)
{
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		Il2CppObject* L_0 = (Il2CppObject*)((OperatorObserverBase_2_t1187768149 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		NullCheck((Il2CppObject*)L_0);
		InterfaceActionInvoker0::Invoke(0 /* System.Void UniRx.IObserver`1<System.Object>::OnCompleted() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3), (Il2CppObject*)L_0);
		IL2CPP_LEAVE(0x19, FINALLY_0012);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0012;
	}

FINALLY_0012:
	{ // begin finally (depth: 1)
		NullCheck((OperatorObserverBase_2_t1187768149 *)__this);
		VirtActionInvoker0::Invoke(4 /* System.Void UniRx.Operators.OperatorObserverBase`2<System.Object,System.Object>::Dispose() */, (OperatorObserverBase_2_t1187768149 *)__this);
		IL2CPP_END_FINALLY(18)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(18)
	{
		IL2CPP_JUMP_TBL(0x19, IL_0019)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0019:
	{
		return;
	}
}
// System.Void UniRx.Operators.CatchObservable`2<System.Object,System.Object>::.ctor(UniRx.IObservable`1<T>,System.Func`2<TException,UniRx.IObservable`1<T>>)
extern "C"  void CatchObservable_2__ctor_m2365847670_gshared (CatchObservable_2_t3974704686 * __this, Il2CppObject* ___source0, Func_2_t1894581716 * ___errorHandler1, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = ___source0;
		bool L_1 = ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)(NULL /*static, unused*/, (Il2CppObject*)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		NullCheck((OperatorObservableBase_1_t4196218687 *)__this);
		((  void (*) (OperatorObservableBase_1_t4196218687 *, bool, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)((OperatorObservableBase_1_t4196218687 *)__this, (bool)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		Il2CppObject* L_2 = ___source0;
		__this->set_source_1(L_2);
		Func_2_t1894581716 * L_3 = ___errorHandler1;
		__this->set_errorHandler_2(L_3);
		return;
	}
}
// System.IDisposable UniRx.Operators.CatchObservable`2<System.Object,System.Object>::SubscribeCore(UniRx.IObserver`1<T>,System.IDisposable)
extern "C"  Il2CppObject * CatchObservable_2_SubscribeCore_m1675312480_gshared (CatchObservable_2_t3974704686 * __this, Il2CppObject* ___observer0, Il2CppObject * ___cancel1, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = ___observer0;
		Il2CppObject * L_1 = ___cancel1;
		Catch_t841129384 * L_2 = (Catch_t841129384 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		((  void (*) (Catch_t841129384 *, CatchObservable_2_t3974704686 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)(L_2, (CatchObservable_2_t3974704686 *)__this, (Il2CppObject*)L_0, (Il2CppObject *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		NullCheck((Catch_t841129384 *)L_2);
		Il2CppObject * L_3 = ((  Il2CppObject * (*) (Catch_t841129384 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->method)((Catch_t841129384 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		return L_3;
	}
}
// System.Void UniRx.Operators.CombineLatestFunc`4<System.Object,System.Object,System.Object,System.Object>::.ctor(System.Object,System.IntPtr)
extern "C"  void CombineLatestFunc_4__ctor_m3400937874_gshared (CombineLatestFunc_4_t382088552 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->method);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TR UniRx.Operators.CombineLatestFunc`4<System.Object,System.Object,System.Object,System.Object>::Invoke(T1,T2,T3)
extern "C"  Il2CppObject * CombineLatestFunc_4_Invoke_m3024523061_gshared (CombineLatestFunc_4_t382088552 * __this, Il2CppObject * ___arg10, Il2CppObject * ___arg21, Il2CppObject * ___arg32, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		CombineLatestFunc_4_Invoke_m3024523061((CombineLatestFunc_4_t382088552 *)__this->get_prev_9(),___arg10, ___arg21, ___arg32, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef Il2CppObject * (*FunctionPointerType) (Il2CppObject *, void* __this, Il2CppObject * ___arg10, Il2CppObject * ___arg21, Il2CppObject * ___arg32, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___arg10, ___arg21, ___arg32,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef Il2CppObject * (*FunctionPointerType) (void* __this, Il2CppObject * ___arg10, Il2CppObject * ___arg21, Il2CppObject * ___arg32, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___arg10, ___arg21, ___arg32,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef Il2CppObject * (*FunctionPointerType) (void* __this, Il2CppObject * ___arg21, Il2CppObject * ___arg32, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(___arg10, ___arg21, ___arg32,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult UniRx.Operators.CombineLatestFunc`4<System.Object,System.Object,System.Object,System.Object>::BeginInvoke(T1,T2,T3,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * CombineLatestFunc_4_BeginInvoke_m818044421_gshared (CombineLatestFunc_4_t382088552 * __this, Il2CppObject * ___arg10, Il2CppObject * ___arg21, Il2CppObject * ___arg32, AsyncCallback_t1363551830 * ___callback3, Il2CppObject * ___object4, const MethodInfo* method)
{
	void *__d_args[4] = {0};
	__d_args[0] = ___arg10;
	__d_args[1] = ___arg21;
	__d_args[2] = ___arg32;
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback3, (Il2CppObject*)___object4);
}
// TR UniRx.Operators.CombineLatestFunc`4<System.Object,System.Object,System.Object,System.Object>::EndInvoke(System.IAsyncResult)
extern "C"  Il2CppObject * CombineLatestFunc_4_EndInvoke_m193277641_gshared (CombineLatestFunc_4_t382088552 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return (Il2CppObject *)__result;
}
// System.Void UniRx.Operators.CombineLatestFunc`5<System.Boolean,System.Boolean,System.Boolean,System.Boolean,System.Object>::.ctor(System.Object,System.IntPtr)
extern "C"  void CombineLatestFunc_5__ctor_m1266321991_gshared (CombineLatestFunc_5_t2231862077 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->method);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TR UniRx.Operators.CombineLatestFunc`5<System.Boolean,System.Boolean,System.Boolean,System.Boolean,System.Object>::Invoke(T1,T2,T3,T4)
extern "C"  Il2CppObject * CombineLatestFunc_5_Invoke_m1701543740_gshared (CombineLatestFunc_5_t2231862077 * __this, bool ___arg10, bool ___arg21, bool ___arg32, bool ___arg43, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		CombineLatestFunc_5_Invoke_m1701543740((CombineLatestFunc_5_t2231862077 *)__this->get_prev_9(),___arg10, ___arg21, ___arg32, ___arg43, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef Il2CppObject * (*FunctionPointerType) (Il2CppObject *, void* __this, bool ___arg10, bool ___arg21, bool ___arg32, bool ___arg43, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___arg10, ___arg21, ___arg32, ___arg43,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef Il2CppObject * (*FunctionPointerType) (void* __this, bool ___arg10, bool ___arg21, bool ___arg32, bool ___arg43, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___arg10, ___arg21, ___arg32, ___arg43,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult UniRx.Operators.CombineLatestFunc`5<System.Boolean,System.Boolean,System.Boolean,System.Boolean,System.Object>::BeginInvoke(T1,T2,T3,T4,System.AsyncCallback,System.Object)
extern Il2CppClass* Boolean_t211005341_il2cpp_TypeInfo_var;
extern const uint32_t CombineLatestFunc_5_BeginInvoke_m3236708292_MetadataUsageId;
extern "C"  Il2CppObject * CombineLatestFunc_5_BeginInvoke_m3236708292_gshared (CombineLatestFunc_5_t2231862077 * __this, bool ___arg10, bool ___arg21, bool ___arg32, bool ___arg43, AsyncCallback_t1363551830 * ___callback4, Il2CppObject * ___object5, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (CombineLatestFunc_5_BeginInvoke_m3236708292_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[5] = {0};
	__d_args[0] = Box(Boolean_t211005341_il2cpp_TypeInfo_var, &___arg10);
	__d_args[1] = Box(Boolean_t211005341_il2cpp_TypeInfo_var, &___arg21);
	__d_args[2] = Box(Boolean_t211005341_il2cpp_TypeInfo_var, &___arg32);
	__d_args[3] = Box(Boolean_t211005341_il2cpp_TypeInfo_var, &___arg43);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback4, (Il2CppObject*)___object5);
}
// TR UniRx.Operators.CombineLatestFunc`5<System.Boolean,System.Boolean,System.Boolean,System.Boolean,System.Object>::EndInvoke(System.IAsyncResult)
extern "C"  Il2CppObject * CombineLatestFunc_5_EndInvoke_m3471871550_gshared (CombineLatestFunc_5_t2231862077 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return (Il2CppObject *)__result;
}
// System.Void UniRx.Operators.CombineLatestFunc`5<System.Object,System.Object,System.Object,System.Object,System.Object>::.ctor(System.Object,System.IntPtr)
extern "C"  void CombineLatestFunc_5__ctor_m2424101439_gshared (CombineLatestFunc_5_t545776053 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->method);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TR UniRx.Operators.CombineLatestFunc`5<System.Object,System.Object,System.Object,System.Object,System.Object>::Invoke(T1,T2,T3,T4)
extern "C"  Il2CppObject * CombineLatestFunc_5_Invoke_m2577212100_gshared (CombineLatestFunc_5_t545776053 * __this, Il2CppObject * ___arg10, Il2CppObject * ___arg21, Il2CppObject * ___arg32, Il2CppObject * ___arg43, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		CombineLatestFunc_5_Invoke_m2577212100((CombineLatestFunc_5_t545776053 *)__this->get_prev_9(),___arg10, ___arg21, ___arg32, ___arg43, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef Il2CppObject * (*FunctionPointerType) (Il2CppObject *, void* __this, Il2CppObject * ___arg10, Il2CppObject * ___arg21, Il2CppObject * ___arg32, Il2CppObject * ___arg43, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___arg10, ___arg21, ___arg32, ___arg43,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef Il2CppObject * (*FunctionPointerType) (void* __this, Il2CppObject * ___arg10, Il2CppObject * ___arg21, Il2CppObject * ___arg32, Il2CppObject * ___arg43, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___arg10, ___arg21, ___arg32, ___arg43,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef Il2CppObject * (*FunctionPointerType) (void* __this, Il2CppObject * ___arg21, Il2CppObject * ___arg32, Il2CppObject * ___arg43, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(___arg10, ___arg21, ___arg32, ___arg43,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult UniRx.Operators.CombineLatestFunc`5<System.Object,System.Object,System.Object,System.Object,System.Object>::BeginInvoke(T1,T2,T3,T4,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * CombineLatestFunc_5_BeginInvoke_m3875124428_gshared (CombineLatestFunc_5_t545776053 * __this, Il2CppObject * ___arg10, Il2CppObject * ___arg21, Il2CppObject * ___arg32, Il2CppObject * ___arg43, AsyncCallback_t1363551830 * ___callback4, Il2CppObject * ___object5, const MethodInfo* method)
{
	void *__d_args[5] = {0};
	__d_args[0] = ___arg10;
	__d_args[1] = ___arg21;
	__d_args[2] = ___arg32;
	__d_args[3] = ___arg43;
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback4, (Il2CppObject*)___object5);
}
// TR UniRx.Operators.CombineLatestFunc`5<System.Object,System.Object,System.Object,System.Object,System.Object>::EndInvoke(System.IAsyncResult)
extern "C"  Il2CppObject * CombineLatestFunc_5_EndInvoke_m4106937782_gshared (CombineLatestFunc_5_t545776053 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return (Il2CppObject *)__result;
}
// System.Void UniRx.Operators.CombineLatestFunc`6<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>::.ctor(System.Object,System.IntPtr)
extern "C"  void CombineLatestFunc_6__ctor_m4168547820_gshared (CombineLatestFunc_6_t2285714110 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->method);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TR UniRx.Operators.CombineLatestFunc`6<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>::Invoke(T1,T2,T3,T4,T5)
extern "C"  Il2CppObject * CombineLatestFunc_6_Invoke_m1574352046_gshared (CombineLatestFunc_6_t2285714110 * __this, Il2CppObject * ___arg10, Il2CppObject * ___arg21, Il2CppObject * ___arg32, Il2CppObject * ___arg43, Il2CppObject * ___arg54, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		CombineLatestFunc_6_Invoke_m1574352046((CombineLatestFunc_6_t2285714110 *)__this->get_prev_9(),___arg10, ___arg21, ___arg32, ___arg43, ___arg54, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef Il2CppObject * (*FunctionPointerType) (Il2CppObject *, void* __this, Il2CppObject * ___arg10, Il2CppObject * ___arg21, Il2CppObject * ___arg32, Il2CppObject * ___arg43, Il2CppObject * ___arg54, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___arg10, ___arg21, ___arg32, ___arg43, ___arg54,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef Il2CppObject * (*FunctionPointerType) (void* __this, Il2CppObject * ___arg10, Il2CppObject * ___arg21, Il2CppObject * ___arg32, Il2CppObject * ___arg43, Il2CppObject * ___arg54, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___arg10, ___arg21, ___arg32, ___arg43, ___arg54,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef Il2CppObject * (*FunctionPointerType) (void* __this, Il2CppObject * ___arg21, Il2CppObject * ___arg32, Il2CppObject * ___arg43, Il2CppObject * ___arg54, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(___arg10, ___arg21, ___arg32, ___arg43, ___arg54,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult UniRx.Operators.CombineLatestFunc`6<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>::BeginInvoke(T1,T2,T3,T4,T5,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * CombineLatestFunc_6_BeginInvoke_m49947744_gshared (CombineLatestFunc_6_t2285714110 * __this, Il2CppObject * ___arg10, Il2CppObject * ___arg21, Il2CppObject * ___arg32, Il2CppObject * ___arg43, Il2CppObject * ___arg54, AsyncCallback_t1363551830 * ___callback5, Il2CppObject * ___object6, const MethodInfo* method)
{
	void *__d_args[6] = {0};
	__d_args[0] = ___arg10;
	__d_args[1] = ___arg21;
	__d_args[2] = ___arg32;
	__d_args[3] = ___arg43;
	__d_args[4] = ___arg54;
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback5, (Il2CppObject*)___object6);
}
// TR UniRx.Operators.CombineLatestFunc`6<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>::EndInvoke(System.IAsyncResult)
extern "C"  Il2CppObject * CombineLatestFunc_6_EndInvoke_m246089635_gshared (CombineLatestFunc_6_t2285714110 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return (Il2CppObject *)__result;
}
// System.Void UniRx.Operators.CombineLatestFunc`7<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>::.ctor(System.Object,System.IntPtr)
extern "C"  void CombineLatestFunc_7__ctor_m3847113881_gshared (CombineLatestFunc_7_t929534559 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->method);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TR UniRx.Operators.CombineLatestFunc`7<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>::Invoke(T1,T2,T3,T4,T5,T6)
extern "C"  Il2CppObject * CombineLatestFunc_7_Invoke_m2111121065_gshared (CombineLatestFunc_7_t929534559 * __this, Il2CppObject * ___arg10, Il2CppObject * ___arg21, Il2CppObject * ___arg32, Il2CppObject * ___arg43, Il2CppObject * ___arg54, Il2CppObject * ___arg65, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		CombineLatestFunc_7_Invoke_m2111121065((CombineLatestFunc_7_t929534559 *)__this->get_prev_9(),___arg10, ___arg21, ___arg32, ___arg43, ___arg54, ___arg65, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef Il2CppObject * (*FunctionPointerType) (Il2CppObject *, void* __this, Il2CppObject * ___arg10, Il2CppObject * ___arg21, Il2CppObject * ___arg32, Il2CppObject * ___arg43, Il2CppObject * ___arg54, Il2CppObject * ___arg65, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___arg10, ___arg21, ___arg32, ___arg43, ___arg54, ___arg65,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef Il2CppObject * (*FunctionPointerType) (void* __this, Il2CppObject * ___arg10, Il2CppObject * ___arg21, Il2CppObject * ___arg32, Il2CppObject * ___arg43, Il2CppObject * ___arg54, Il2CppObject * ___arg65, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___arg10, ___arg21, ___arg32, ___arg43, ___arg54, ___arg65,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef Il2CppObject * (*FunctionPointerType) (void* __this, Il2CppObject * ___arg21, Il2CppObject * ___arg32, Il2CppObject * ___arg43, Il2CppObject * ___arg54, Il2CppObject * ___arg65, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(___arg10, ___arg21, ___arg32, ___arg43, ___arg54, ___arg65,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult UniRx.Operators.CombineLatestFunc`7<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>::BeginInvoke(T1,T2,T3,T4,T5,T6,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * CombineLatestFunc_7_BeginInvoke_m1651379027_gshared (CombineLatestFunc_7_t929534559 * __this, Il2CppObject * ___arg10, Il2CppObject * ___arg21, Il2CppObject * ___arg32, Il2CppObject * ___arg43, Il2CppObject * ___arg54, Il2CppObject * ___arg65, AsyncCallback_t1363551830 * ___callback6, Il2CppObject * ___object7, const MethodInfo* method)
{
	void *__d_args[7] = {0};
	__d_args[0] = ___arg10;
	__d_args[1] = ___arg21;
	__d_args[2] = ___arg32;
	__d_args[3] = ___arg43;
	__d_args[4] = ___arg54;
	__d_args[5] = ___arg65;
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback6, (Il2CppObject*)___object7);
}
// TR UniRx.Operators.CombineLatestFunc`7<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>::EndInvoke(System.IAsyncResult)
extern "C"  Il2CppObject * CombineLatestFunc_7_EndInvoke_m2565031056_gshared (CombineLatestFunc_7_t929534559 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return (Il2CppObject *)__result;
}
// System.Void UniRx.Operators.CombineLatestFunc`8<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>::.ctor(System.Object,System.IntPtr)
extern "C"  void CombineLatestFunc_8__ctor_m2039772742_gshared (CombineLatestFunc_8_t584396980 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->method);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TR UniRx.Operators.CombineLatestFunc`8<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>::Invoke(T1,T2,T3,T4,T5,T6,T7)
extern "C"  Il2CppObject * CombineLatestFunc_8_Invoke_m950119207_gshared (CombineLatestFunc_8_t584396980 * __this, Il2CppObject * ___arg10, Il2CppObject * ___arg21, Il2CppObject * ___arg32, Il2CppObject * ___arg43, Il2CppObject * ___arg54, Il2CppObject * ___arg65, Il2CppObject * ___arg76, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		CombineLatestFunc_8_Invoke_m950119207((CombineLatestFunc_8_t584396980 *)__this->get_prev_9(),___arg10, ___arg21, ___arg32, ___arg43, ___arg54, ___arg65, ___arg76, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef Il2CppObject * (*FunctionPointerType) (Il2CppObject *, void* __this, Il2CppObject * ___arg10, Il2CppObject * ___arg21, Il2CppObject * ___arg32, Il2CppObject * ___arg43, Il2CppObject * ___arg54, Il2CppObject * ___arg65, Il2CppObject * ___arg76, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___arg10, ___arg21, ___arg32, ___arg43, ___arg54, ___arg65, ___arg76,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef Il2CppObject * (*FunctionPointerType) (void* __this, Il2CppObject * ___arg10, Il2CppObject * ___arg21, Il2CppObject * ___arg32, Il2CppObject * ___arg43, Il2CppObject * ___arg54, Il2CppObject * ___arg65, Il2CppObject * ___arg76, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___arg10, ___arg21, ___arg32, ___arg43, ___arg54, ___arg65, ___arg76,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef Il2CppObject * (*FunctionPointerType) (void* __this, Il2CppObject * ___arg21, Il2CppObject * ___arg32, Il2CppObject * ___arg43, Il2CppObject * ___arg54, Il2CppObject * ___arg65, Il2CppObject * ___arg76, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(___arg10, ___arg21, ___arg32, ___arg43, ___arg54, ___arg65, ___arg76,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult UniRx.Operators.CombineLatestFunc`8<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>::BeginInvoke(T1,T2,T3,T4,T5,T6,T7,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * CombineLatestFunc_8_BeginInvoke_m2895725499_gshared (CombineLatestFunc_8_t584396980 * __this, Il2CppObject * ___arg10, Il2CppObject * ___arg21, Il2CppObject * ___arg32, Il2CppObject * ___arg43, Il2CppObject * ___arg54, Il2CppObject * ___arg65, Il2CppObject * ___arg76, AsyncCallback_t1363551830 * ___callback7, Il2CppObject * ___object8, const MethodInfo* method)
{
	void *__d_args[8] = {0};
	__d_args[0] = ___arg10;
	__d_args[1] = ___arg21;
	__d_args[2] = ___arg32;
	__d_args[3] = ___arg43;
	__d_args[4] = ___arg54;
	__d_args[5] = ___arg65;
	__d_args[6] = ___arg76;
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback7, (Il2CppObject*)___object8);
}
// TR UniRx.Operators.CombineLatestFunc`8<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>::EndInvoke(System.IAsyncResult)
extern "C"  Il2CppObject * CombineLatestFunc_8_EndInvoke_m1310280829_gshared (CombineLatestFunc_8_t584396980 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return (Il2CppObject *)__result;
}
// System.Void UniRx.Operators.CombineLatestObservable`1/CombineLatest/CombineLatestObserver<System.Boolean>::.ctor(UniRx.Operators.CombineLatestObservable`1/CombineLatest<T>,System.Int32)
extern "C"  void CombineLatestObserver__ctor_m1836044108_gshared (CombineLatestObserver_t3970536766 * __this, CombineLatest_t242152888 * ___parent0, int32_t ___index1, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		CombineLatest_t242152888 * L_0 = ___parent0;
		__this->set_parent_0(L_0);
		int32_t L_1 = ___index1;
		__this->set_index_1(L_1);
		return;
	}
}
// System.Void UniRx.Operators.CombineLatestObservable`1/CombineLatest/CombineLatestObserver<System.Boolean>::OnNext(T)
extern "C"  void CombineLatestObserver_OnNext_m279410687_gshared (CombineLatestObserver_t3970536766 * __this, bool ___value0, const MethodInfo* method)
{
	Il2CppObject * V_0 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		CombineLatest_t242152888 * L_0 = (CombineLatest_t242152888 *)__this->get_parent_0();
		NullCheck(L_0);
		Il2CppObject * L_1 = (Il2CppObject *)L_0->get_gate_3();
		V_0 = (Il2CppObject *)L_1;
		Il2CppObject * L_2 = V_0;
		Monitor_Enter_m476686225(NULL /*static, unused*/, (Il2CppObject *)L_2, /*hidden argument*/NULL);
	}

IL_0012:
	try
	{ // begin try (depth: 1)
		CombineLatest_t242152888 * L_3 = (CombineLatest_t242152888 *)__this->get_parent_0();
		NullCheck(L_3);
		BooleanU5BU5D_t3804927312* L_4 = (BooleanU5BU5D_t3804927312*)L_3->get_values_5();
		int32_t L_5 = (int32_t)__this->get_index_1();
		bool L_6 = ___value0;
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, L_5);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(L_5), (bool)L_6);
		CombineLatest_t242152888 * L_7 = (CombineLatest_t242152888 *)__this->get_parent_0();
		int32_t L_8 = (int32_t)__this->get_index_1();
		NullCheck((CombineLatest_t242152888 *)L_7);
		((  void (*) (CombineLatest_t242152888 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((CombineLatest_t242152888 *)L_7, (int32_t)L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		IL2CPP_LEAVE(0x46, FINALLY_003f);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_003f;
	}

FINALLY_003f:
	{ // begin finally (depth: 1)
		Il2CppObject * L_9 = V_0;
		Monitor_Exit_m2088237919(NULL /*static, unused*/, (Il2CppObject *)L_9, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(63)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(63)
	{
		IL2CPP_JUMP_TBL(0x46, IL_0046)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0046:
	{
		return;
	}
}
// System.Void UniRx.Operators.CombineLatestObservable`1/CombineLatest/CombineLatestObserver<System.Boolean>::OnError(System.Exception)
extern "C"  void CombineLatestObserver_OnError_m491894030_gshared (CombineLatestObserver_t3970536766 * __this, Exception_t1967233988 * ___ex0, const MethodInfo* method)
{
	Il2CppObject * V_0 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		CombineLatest_t242152888 * L_0 = (CombineLatest_t242152888 *)__this->get_parent_0();
		NullCheck(L_0);
		Il2CppObject * L_1 = (Il2CppObject *)L_0->get_gate_3();
		V_0 = (Il2CppObject *)L_1;
		Il2CppObject * L_2 = V_0;
		Monitor_Enter_m476686225(NULL /*static, unused*/, (Il2CppObject *)L_2, /*hidden argument*/NULL);
	}

IL_0012:
	try
	{ // begin try (depth: 1)
		CombineLatest_t242152888 * L_3 = (CombineLatest_t242152888 *)__this->get_parent_0();
		Exception_t1967233988 * L_4 = ___ex0;
		NullCheck((CombineLatest_t242152888 *)L_3);
		VirtActionInvoker1< Exception_t1967233988 * >::Invoke(9 /* System.Void UniRx.Operators.CombineLatestObservable`1/CombineLatest<System.Boolean>::OnError(System.Exception) */, (CombineLatest_t242152888 *)L_3, (Exception_t1967233988 *)L_4);
		IL2CPP_LEAVE(0x2A, FINALLY_0023);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0023;
	}

FINALLY_0023:
	{ // begin finally (depth: 1)
		Il2CppObject * L_5 = V_0;
		Monitor_Exit_m2088237919(NULL /*static, unused*/, (Il2CppObject *)L_5, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(35)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(35)
	{
		IL2CPP_JUMP_TBL(0x2A, IL_002a)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_002a:
	{
		return;
	}
}
// System.Void UniRx.Operators.CombineLatestObservable`1/CombineLatest/CombineLatestObserver<System.Boolean>::OnCompleted()
extern "C"  void CombineLatestObserver_OnCompleted_m1429015905_gshared (CombineLatestObserver_t3970536766 * __this, const MethodInfo* method)
{
	Il2CppObject * V_0 = NULL;
	bool V_1 = false;
	int32_t V_2 = 0;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		CombineLatest_t242152888 * L_0 = (CombineLatest_t242152888 *)__this->get_parent_0();
		NullCheck(L_0);
		Il2CppObject * L_1 = (Il2CppObject *)L_0->get_gate_3();
		V_0 = (Il2CppObject *)L_1;
		Il2CppObject * L_2 = V_0;
		Monitor_Enter_m476686225(NULL /*static, unused*/, (Il2CppObject *)L_2, /*hidden argument*/NULL);
	}

IL_0012:
	try
	{ // begin try (depth: 1)
		{
			CombineLatest_t242152888 * L_3 = (CombineLatest_t242152888 *)__this->get_parent_0();
			NullCheck(L_3);
			BooleanU5BU5D_t3804927312* L_4 = (BooleanU5BU5D_t3804927312*)L_3->get_isCompleted_7();
			int32_t L_5 = (int32_t)__this->get_index_1();
			NullCheck(L_4);
			IL2CPP_ARRAY_BOUNDS_CHECK(L_4, L_5);
			(L_4)->SetAt(static_cast<il2cpp_array_size_t>(L_5), (bool)1);
			V_1 = (bool)1;
			V_2 = (int32_t)0;
			goto IL_004b;
		}

IL_002e:
		{
			CombineLatest_t242152888 * L_6 = (CombineLatest_t242152888 *)__this->get_parent_0();
			NullCheck(L_6);
			BooleanU5BU5D_t3804927312* L_7 = (BooleanU5BU5D_t3804927312*)L_6->get_isCompleted_7();
			int32_t L_8 = V_2;
			NullCheck(L_7);
			IL2CPP_ARRAY_BOUNDS_CHECK(L_7, L_8);
			int32_t L_9 = L_8;
			if (((L_7)->GetAt(static_cast<il2cpp_array_size_t>(L_9))))
			{
				goto IL_0047;
			}
		}

IL_0040:
		{
			V_1 = (bool)0;
			goto IL_005c;
		}

IL_0047:
		{
			int32_t L_10 = V_2;
			V_2 = (int32_t)((int32_t)((int32_t)L_10+(int32_t)1));
		}

IL_004b:
		{
			int32_t L_11 = V_2;
			CombineLatest_t242152888 * L_12 = (CombineLatest_t242152888 *)__this->get_parent_0();
			NullCheck(L_12);
			int32_t L_13 = (int32_t)L_12->get_length_4();
			if ((((int32_t)L_11) < ((int32_t)L_13)))
			{
				goto IL_002e;
			}
		}

IL_005c:
		{
			bool L_14 = V_1;
			if (!L_14)
			{
				goto IL_006d;
			}
		}

IL_0062:
		{
			CombineLatest_t242152888 * L_15 = (CombineLatest_t242152888 *)__this->get_parent_0();
			NullCheck((CombineLatest_t242152888 *)L_15);
			VirtActionInvoker0::Invoke(10 /* System.Void UniRx.Operators.CombineLatestObservable`1/CombineLatest<System.Boolean>::OnCompleted() */, (CombineLatest_t242152888 *)L_15);
		}

IL_006d:
		{
			IL2CPP_LEAVE(0x79, FINALLY_0072);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0072;
	}

FINALLY_0072:
	{ // begin finally (depth: 1)
		Il2CppObject * L_16 = V_0;
		Monitor_Exit_m2088237919(NULL /*static, unused*/, (Il2CppObject *)L_16, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(114)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(114)
	{
		IL2CPP_JUMP_TBL(0x79, IL_0079)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0079:
	{
		return;
	}
}
// System.Void UniRx.Operators.CombineLatestObservable`1/CombineLatest/CombineLatestObserver<System.Object>::.ctor(UniRx.Operators.CombineLatestObservable`1/CombineLatest<T>,System.Int32)
extern "C"  void CombineLatestObserver__ctor_m566411945_gshared (CombineLatestObserver_t301670549 * __this, CombineLatest_t868253967 * ___parent0, int32_t ___index1, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		CombineLatest_t868253967 * L_0 = ___parent0;
		__this->set_parent_0(L_0);
		int32_t L_1 = ___index1;
		__this->set_index_1(L_1);
		return;
	}
}
// System.Void UniRx.Operators.CombineLatestObservable`1/CombineLatest/CombineLatestObserver<System.Object>::OnNext(T)
extern "C"  void CombineLatestObserver_OnNext_m1370273410_gshared (CombineLatestObserver_t301670549 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	Il2CppObject * V_0 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		CombineLatest_t868253967 * L_0 = (CombineLatest_t868253967 *)__this->get_parent_0();
		NullCheck(L_0);
		Il2CppObject * L_1 = (Il2CppObject *)L_0->get_gate_3();
		V_0 = (Il2CppObject *)L_1;
		Il2CppObject * L_2 = V_0;
		Monitor_Enter_m476686225(NULL /*static, unused*/, (Il2CppObject *)L_2, /*hidden argument*/NULL);
	}

IL_0012:
	try
	{ // begin try (depth: 1)
		CombineLatest_t868253967 * L_3 = (CombineLatest_t868253967 *)__this->get_parent_0();
		NullCheck(L_3);
		ObjectU5BU5D_t11523773* L_4 = (ObjectU5BU5D_t11523773*)L_3->get_values_5();
		int32_t L_5 = (int32_t)__this->get_index_1();
		Il2CppObject * L_6 = ___value0;
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, L_5);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(L_5), (Il2CppObject *)L_6);
		CombineLatest_t868253967 * L_7 = (CombineLatest_t868253967 *)__this->get_parent_0();
		int32_t L_8 = (int32_t)__this->get_index_1();
		NullCheck((CombineLatest_t868253967 *)L_7);
		((  void (*) (CombineLatest_t868253967 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((CombineLatest_t868253967 *)L_7, (int32_t)L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		IL2CPP_LEAVE(0x46, FINALLY_003f);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_003f;
	}

FINALLY_003f:
	{ // begin finally (depth: 1)
		Il2CppObject * L_9 = V_0;
		Monitor_Exit_m2088237919(NULL /*static, unused*/, (Il2CppObject *)L_9, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(63)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(63)
	{
		IL2CPP_JUMP_TBL(0x46, IL_0046)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0046:
	{
		return;
	}
}
// System.Void UniRx.Operators.CombineLatestObservable`1/CombineLatest/CombineLatestObserver<System.Object>::OnError(System.Exception)
extern "C"  void CombineLatestObserver_OnError_m520588689_gshared (CombineLatestObserver_t301670549 * __this, Exception_t1967233988 * ___ex0, const MethodInfo* method)
{
	Il2CppObject * V_0 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		CombineLatest_t868253967 * L_0 = (CombineLatest_t868253967 *)__this->get_parent_0();
		NullCheck(L_0);
		Il2CppObject * L_1 = (Il2CppObject *)L_0->get_gate_3();
		V_0 = (Il2CppObject *)L_1;
		Il2CppObject * L_2 = V_0;
		Monitor_Enter_m476686225(NULL /*static, unused*/, (Il2CppObject *)L_2, /*hidden argument*/NULL);
	}

IL_0012:
	try
	{ // begin try (depth: 1)
		CombineLatest_t868253967 * L_3 = (CombineLatest_t868253967 *)__this->get_parent_0();
		Exception_t1967233988 * L_4 = ___ex0;
		NullCheck((CombineLatest_t868253967 *)L_3);
		VirtActionInvoker1< Exception_t1967233988 * >::Invoke(9 /* System.Void UniRx.Operators.CombineLatestObservable`1/CombineLatest<System.Object>::OnError(System.Exception) */, (CombineLatest_t868253967 *)L_3, (Exception_t1967233988 *)L_4);
		IL2CPP_LEAVE(0x2A, FINALLY_0023);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0023;
	}

FINALLY_0023:
	{ // begin finally (depth: 1)
		Il2CppObject * L_5 = V_0;
		Monitor_Exit_m2088237919(NULL /*static, unused*/, (Il2CppObject *)L_5, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(35)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(35)
	{
		IL2CPP_JUMP_TBL(0x2A, IL_002a)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_002a:
	{
		return;
	}
}
// System.Void UniRx.Operators.CombineLatestObservable`1/CombineLatest/CombineLatestObserver<System.Object>::OnCompleted()
extern "C"  void CombineLatestObserver_OnCompleted_m4237906532_gshared (CombineLatestObserver_t301670549 * __this, const MethodInfo* method)
{
	Il2CppObject * V_0 = NULL;
	bool V_1 = false;
	int32_t V_2 = 0;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		CombineLatest_t868253967 * L_0 = (CombineLatest_t868253967 *)__this->get_parent_0();
		NullCheck(L_0);
		Il2CppObject * L_1 = (Il2CppObject *)L_0->get_gate_3();
		V_0 = (Il2CppObject *)L_1;
		Il2CppObject * L_2 = V_0;
		Monitor_Enter_m476686225(NULL /*static, unused*/, (Il2CppObject *)L_2, /*hidden argument*/NULL);
	}

IL_0012:
	try
	{ // begin try (depth: 1)
		{
			CombineLatest_t868253967 * L_3 = (CombineLatest_t868253967 *)__this->get_parent_0();
			NullCheck(L_3);
			BooleanU5BU5D_t3804927312* L_4 = (BooleanU5BU5D_t3804927312*)L_3->get_isCompleted_7();
			int32_t L_5 = (int32_t)__this->get_index_1();
			NullCheck(L_4);
			IL2CPP_ARRAY_BOUNDS_CHECK(L_4, L_5);
			(L_4)->SetAt(static_cast<il2cpp_array_size_t>(L_5), (bool)1);
			V_1 = (bool)1;
			V_2 = (int32_t)0;
			goto IL_004b;
		}

IL_002e:
		{
			CombineLatest_t868253967 * L_6 = (CombineLatest_t868253967 *)__this->get_parent_0();
			NullCheck(L_6);
			BooleanU5BU5D_t3804927312* L_7 = (BooleanU5BU5D_t3804927312*)L_6->get_isCompleted_7();
			int32_t L_8 = V_2;
			NullCheck(L_7);
			IL2CPP_ARRAY_BOUNDS_CHECK(L_7, L_8);
			int32_t L_9 = L_8;
			if (((L_7)->GetAt(static_cast<il2cpp_array_size_t>(L_9))))
			{
				goto IL_0047;
			}
		}

IL_0040:
		{
			V_1 = (bool)0;
			goto IL_005c;
		}

IL_0047:
		{
			int32_t L_10 = V_2;
			V_2 = (int32_t)((int32_t)((int32_t)L_10+(int32_t)1));
		}

IL_004b:
		{
			int32_t L_11 = V_2;
			CombineLatest_t868253967 * L_12 = (CombineLatest_t868253967 *)__this->get_parent_0();
			NullCheck(L_12);
			int32_t L_13 = (int32_t)L_12->get_length_4();
			if ((((int32_t)L_11) < ((int32_t)L_13)))
			{
				goto IL_002e;
			}
		}

IL_005c:
		{
			bool L_14 = V_1;
			if (!L_14)
			{
				goto IL_006d;
			}
		}

IL_0062:
		{
			CombineLatest_t868253967 * L_15 = (CombineLatest_t868253967 *)__this->get_parent_0();
			NullCheck((CombineLatest_t868253967 *)L_15);
			VirtActionInvoker0::Invoke(10 /* System.Void UniRx.Operators.CombineLatestObservable`1/CombineLatest<System.Object>::OnCompleted() */, (CombineLatest_t868253967 *)L_15);
		}

IL_006d:
		{
			IL2CPP_LEAVE(0x79, FINALLY_0072);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0072;
	}

FINALLY_0072:
	{ // begin finally (depth: 1)
		Il2CppObject * L_16 = V_0;
		Monitor_Exit_m2088237919(NULL /*static, unused*/, (Il2CppObject *)L_16, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(114)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(114)
	{
		IL2CPP_JUMP_TBL(0x79, IL_0079)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0079:
	{
		return;
	}
}
// System.Void UniRx.Operators.CombineLatestObservable`1/CombineLatest<System.Boolean>::.ctor(UniRx.Operators.CombineLatestObservable`1<T>,UniRx.IObserver`1<System.Collections.Generic.IList`1<T>>,System.IDisposable)
extern Il2CppClass* Il2CppObject_il2cpp_TypeInfo_var;
extern const uint32_t CombineLatest__ctor_m2966111004_MetadataUsageId;
extern "C"  void CombineLatest__ctor_m2966111004_gshared (CombineLatest_t242152888 * __this, CombineLatestObservable_1_t2883136337 * ___parent0, Il2CppObject* ___observer1, Il2CppObject * ___cancel2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (CombineLatest__ctor_m2966111004_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject * L_0 = (Il2CppObject *)il2cpp_codegen_object_new(Il2CppObject_il2cpp_TypeInfo_var);
		Object__ctor_m1772956182(L_0, /*hidden argument*/NULL);
		__this->set_gate_3(L_0);
		Il2CppObject* L_1 = ___observer1;
		Il2CppObject * L_2 = ___cancel2;
		NullCheck((OperatorObserverBase_2_t2939511529 *)__this);
		((  void (*) (OperatorObserverBase_2_t2939511529 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((OperatorObserverBase_2_t2939511529 *)__this, (Il2CppObject*)L_1, (Il2CppObject *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		CombineLatestObservable_1_t2883136337 * L_3 = ___parent0;
		__this->set_parent_2(L_3);
		return;
	}
}
// System.IDisposable UniRx.Operators.CombineLatestObservable`1/CombineLatest<System.Boolean>::Run()
extern Il2CppClass* BooleanU5BU5D_t3804927312_il2cpp_TypeInfo_var;
extern Il2CppClass* IDisposableU5BU5D_t165183403_il2cpp_TypeInfo_var;
extern const uint32_t CombineLatest_Run_m1253482696_MetadataUsageId;
extern "C"  Il2CppObject * CombineLatest_Run_m1253482696_gshared (CombineLatest_t242152888 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (CombineLatest_Run_m1253482696_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	IDisposableU5BU5D_t165183403* V_0 = NULL;
	int32_t V_1 = 0;
	Il2CppObject* V_2 = NULL;
	{
		CombineLatestObservable_1_t2883136337 * L_0 = (CombineLatestObservable_1_t2883136337 *)__this->get_parent_2();
		NullCheck(L_0);
		IObservable_1U5BU5D_t1703862084* L_1 = (IObservable_1U5BU5D_t1703862084*)L_0->get_sources_1();
		NullCheck(L_1);
		__this->set_length_4((((int32_t)((int32_t)(((Il2CppArray *)L_1)->max_length)))));
		int32_t L_2 = (int32_t)__this->get_length_4();
		__this->set_values_5(((BooleanU5BU5D_t3804927312*)SZArrayNew(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), (uint32_t)L_2)));
		int32_t L_3 = (int32_t)__this->get_length_4();
		__this->set_isStarted_6(((BooleanU5BU5D_t3804927312*)SZArrayNew(BooleanU5BU5D_t3804927312_il2cpp_TypeInfo_var, (uint32_t)L_3)));
		int32_t L_4 = (int32_t)__this->get_length_4();
		__this->set_isCompleted_7(((BooleanU5BU5D_t3804927312*)SZArrayNew(BooleanU5BU5D_t3804927312_il2cpp_TypeInfo_var, (uint32_t)L_4)));
		__this->set_isAllValueStarted_8((bool)0);
		int32_t L_5 = (int32_t)__this->get_length_4();
		V_0 = (IDisposableU5BU5D_t165183403*)((IDisposableU5BU5D_t165183403*)SZArrayNew(IDisposableU5BU5D_t165183403_il2cpp_TypeInfo_var, (uint32_t)L_5));
		V_1 = (int32_t)0;
		goto IL_0082;
	}

IL_0060:
	{
		CombineLatestObservable_1_t2883136337 * L_6 = (CombineLatestObservable_1_t2883136337 *)__this->get_parent_2();
		NullCheck(L_6);
		IObservable_1U5BU5D_t1703862084* L_7 = (IObservable_1U5BU5D_t1703862084*)L_6->get_sources_1();
		int32_t L_8 = V_1;
		NullCheck(L_7);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_7, L_8);
		int32_t L_9 = L_8;
		V_2 = (Il2CppObject*)((L_7)->GetAt(static_cast<il2cpp_array_size_t>(L_9)));
		IDisposableU5BU5D_t165183403* L_10 = V_0;
		int32_t L_11 = V_1;
		Il2CppObject* L_12 = V_2;
		int32_t L_13 = V_1;
		CombineLatestObserver_t3970536766 * L_14 = (CombineLatestObserver_t3970536766 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		((  void (*) (CombineLatestObserver_t3970536766 *, CombineLatest_t242152888 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)(L_14, (CombineLatest_t242152888 *)__this, (int32_t)L_13, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		NullCheck((Il2CppObject*)L_12);
		Il2CppObject * L_15 = InterfaceFuncInvoker1< Il2CppObject *, Il2CppObject* >::Invoke(0 /* System.IDisposable UniRx.IObservable`1<System.Boolean>::Subscribe(UniRx.IObserver`1<T>) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5), (Il2CppObject*)L_12, (Il2CppObject*)L_14);
		NullCheck(L_10);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_10, L_11);
		ArrayElementTypeCheck (L_10, L_15);
		(L_10)->SetAt(static_cast<il2cpp_array_size_t>(L_11), (Il2CppObject *)L_15);
		int32_t L_16 = V_1;
		V_1 = (int32_t)((int32_t)((int32_t)L_16+(int32_t)1));
	}

IL_0082:
	{
		int32_t L_17 = V_1;
		int32_t L_18 = (int32_t)__this->get_length_4();
		if ((((int32_t)L_17) < ((int32_t)L_18)))
		{
			goto IL_0060;
		}
	}
	{
		IDisposableU5BU5D_t165183403* L_19 = V_0;
		Il2CppObject * L_20 = StableCompositeDisposable_CreateUnsafe_m1071468137(NULL /*static, unused*/, (IDisposableU5BU5D_t165183403*)L_19, /*hidden argument*/NULL);
		return L_20;
	}
}
// System.Void UniRx.Operators.CombineLatestObservable`1/CombineLatest<System.Boolean>::Publish(System.Int32)
extern "C"  void CombineLatest_Publish_m761439650_gshared (CombineLatest_t242152888 * __this, int32_t ___index0, const MethodInfo* method)
{
	bool V_0 = false;
	int32_t V_1 = 0;
	bool V_2 = false;
	int32_t V_3 = 0;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		BooleanU5BU5D_t3804927312* L_0 = (BooleanU5BU5D_t3804927312*)__this->get_isStarted_6();
		int32_t L_1 = ___index0;
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, L_1);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(L_1), (bool)1);
		bool L_2 = (bool)__this->get_isAllValueStarted_8();
		if (!L_2)
		{
			goto IL_0026;
		}
	}
	{
		BooleanU5BU5D_t3804927312* L_3 = (BooleanU5BU5D_t3804927312*)__this->get_values_5();
		List_1_t1007964310 * L_4 = (List_1_t1007964310 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		((  void (*) (List_1_t1007964310 *, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7)->method)(L_4, (Il2CppObject*)(Il2CppObject*)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7));
		NullCheck((CombineLatest_t242152888 *)__this);
		VirtActionInvoker1< Il2CppObject* >::Invoke(8 /* System.Void UniRx.Operators.CombineLatestObservable`1/CombineLatest<System.Boolean>::OnNext(System.Collections.Generic.IList`1<T>) */, (CombineLatest_t242152888 *)__this, (Il2CppObject*)L_4);
		return;
	}

IL_0026:
	{
		V_0 = (bool)1;
		V_1 = (int32_t)0;
		goto IL_0047;
	}

IL_002f:
	{
		BooleanU5BU5D_t3804927312* L_5 = (BooleanU5BU5D_t3804927312*)__this->get_isStarted_6();
		int32_t L_6 = V_1;
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, L_6);
		int32_t L_7 = L_6;
		if (((L_5)->GetAt(static_cast<il2cpp_array_size_t>(L_7))))
		{
			goto IL_0043;
		}
	}
	{
		V_0 = (bool)0;
		goto IL_0053;
	}

IL_0043:
	{
		int32_t L_8 = V_1;
		V_1 = (int32_t)((int32_t)((int32_t)L_8+(int32_t)1));
	}

IL_0047:
	{
		int32_t L_9 = V_1;
		int32_t L_10 = (int32_t)__this->get_length_4();
		if ((((int32_t)L_9) < ((int32_t)L_10)))
		{
			goto IL_002f;
		}
	}

IL_0053:
	{
		bool L_11 = V_0;
		__this->set_isAllValueStarted_8(L_11);
		bool L_12 = (bool)__this->get_isAllValueStarted_8();
		if (!L_12)
		{
			goto IL_0077;
		}
	}
	{
		BooleanU5BU5D_t3804927312* L_13 = (BooleanU5BU5D_t3804927312*)__this->get_values_5();
		List_1_t1007964310 * L_14 = (List_1_t1007964310 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		((  void (*) (List_1_t1007964310 *, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7)->method)(L_14, (Il2CppObject*)(Il2CppObject*)L_13, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7));
		NullCheck((CombineLatest_t242152888 *)__this);
		VirtActionInvoker1< Il2CppObject* >::Invoke(8 /* System.Void UniRx.Operators.CombineLatestObservable`1/CombineLatest<System.Boolean>::OnNext(System.Collections.Generic.IList`1<T>) */, (CombineLatest_t242152888 *)__this, (Il2CppObject*)L_14);
		return;
	}

IL_0077:
	{
		V_2 = (bool)1;
		V_3 = (int32_t)0;
		goto IL_00a4;
	}

IL_0080:
	{
		int32_t L_15 = V_3;
		int32_t L_16 = ___index0;
		if ((!(((uint32_t)L_15) == ((uint32_t)L_16))))
		{
			goto IL_008c;
		}
	}
	{
		goto IL_00a0;
	}

IL_008c:
	{
		BooleanU5BU5D_t3804927312* L_17 = (BooleanU5BU5D_t3804927312*)__this->get_isCompleted_7();
		int32_t L_18 = V_3;
		NullCheck(L_17);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_17, L_18);
		int32_t L_19 = L_18;
		if (((L_17)->GetAt(static_cast<il2cpp_array_size_t>(L_19))))
		{
			goto IL_00a0;
		}
	}
	{
		V_2 = (bool)0;
		goto IL_00b0;
	}

IL_00a0:
	{
		int32_t L_20 = V_3;
		V_3 = (int32_t)((int32_t)((int32_t)L_20+(int32_t)1));
	}

IL_00a4:
	{
		int32_t L_21 = V_3;
		int32_t L_22 = (int32_t)__this->get_length_4();
		if ((((int32_t)L_21) < ((int32_t)L_22)))
		{
			goto IL_0080;
		}
	}

IL_00b0:
	{
		bool L_23 = V_2;
		if (!L_23)
		{
			goto IL_00d0;
		}
	}

IL_00b6:
	try
	{ // begin try (depth: 1)
		Il2CppObject* L_24 = (Il2CppObject*)((OperatorObserverBase_2_t2939511529 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		NullCheck((Il2CppObject*)L_24);
		InterfaceActionInvoker0::Invoke(0 /* System.Void UniRx.IObserver`1<System.Collections.Generic.IList`1<System.Boolean>>::OnCompleted() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9), (Il2CppObject*)L_24);
		IL2CPP_LEAVE(0xCF, FINALLY_00c8);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_00c8;
	}

FINALLY_00c8:
	{ // begin finally (depth: 1)
		NullCheck((OperatorObserverBase_2_t2939511529 *)__this);
		VirtActionInvoker0::Invoke(4 /* System.Void UniRx.Operators.OperatorObserverBase`2<System.Collections.Generic.IList`1<System.Boolean>,System.Collections.Generic.IList`1<System.Boolean>>::Dispose() */, (OperatorObserverBase_2_t2939511529 *)__this);
		IL2CPP_END_FINALLY(200)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(200)
	{
		IL2CPP_JUMP_TBL(0xCF, IL_00cf)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_00cf:
	{
		return;
	}

IL_00d0:
	{
		return;
	}
}
// System.Void UniRx.Operators.CombineLatestObservable`1/CombineLatest<System.Boolean>::OnNext(System.Collections.Generic.IList`1<T>)
extern "C"  void CombineLatest_OnNext_m4100036611_gshared (CombineLatest_t242152888 * __this, Il2CppObject* ___value0, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = (Il2CppObject*)((OperatorObserverBase_2_t2939511529 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		Il2CppObject* L_1 = ___value0;
		NullCheck((Il2CppObject*)L_0);
		InterfaceActionInvoker1< Il2CppObject* >::Invoke(2 /* System.Void UniRx.IObserver`1<System.Collections.Generic.IList`1<System.Boolean>>::OnNext(T) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9), (Il2CppObject*)L_0, (Il2CppObject*)L_1);
		return;
	}
}
// System.Void UniRx.Operators.CombineLatestObservable`1/CombineLatest<System.Boolean>::OnError(System.Exception)
extern "C"  void CombineLatest_OnError_m3555055707_gshared (CombineLatest_t242152888 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method)
{
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		Il2CppObject* L_0 = (Il2CppObject*)((OperatorObserverBase_2_t2939511529 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		Exception_t1967233988 * L_1 = ___error0;
		NullCheck((Il2CppObject*)L_0);
		InterfaceActionInvoker1< Exception_t1967233988 * >::Invoke(1 /* System.Void UniRx.IObserver`1<System.Collections.Generic.IList`1<System.Boolean>>::OnError(System.Exception) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9), (Il2CppObject*)L_0, (Exception_t1967233988 *)L_1);
		IL2CPP_LEAVE(0x1A, FINALLY_0013);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0013;
	}

FINALLY_0013:
	{ // begin finally (depth: 1)
		NullCheck((OperatorObserverBase_2_t2939511529 *)__this);
		VirtActionInvoker0::Invoke(4 /* System.Void UniRx.Operators.OperatorObserverBase`2<System.Collections.Generic.IList`1<System.Boolean>,System.Collections.Generic.IList`1<System.Boolean>>::Dispose() */, (OperatorObserverBase_2_t2939511529 *)__this);
		IL2CPP_END_FINALLY(19)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(19)
	{
		IL2CPP_JUMP_TBL(0x1A, IL_001a)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_001a:
	{
		return;
	}
}
// System.Void UniRx.Operators.CombineLatestObservable`1/CombineLatest<System.Boolean>::OnCompleted()
extern "C"  void CombineLatest_OnCompleted_m3335126062_gshared (CombineLatest_t242152888 * __this, const MethodInfo* method)
{
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		Il2CppObject* L_0 = (Il2CppObject*)((OperatorObserverBase_2_t2939511529 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		NullCheck((Il2CppObject*)L_0);
		InterfaceActionInvoker0::Invoke(0 /* System.Void UniRx.IObserver`1<System.Collections.Generic.IList`1<System.Boolean>>::OnCompleted() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9), (Il2CppObject*)L_0);
		IL2CPP_LEAVE(0x19, FINALLY_0012);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0012;
	}

FINALLY_0012:
	{ // begin finally (depth: 1)
		NullCheck((OperatorObserverBase_2_t2939511529 *)__this);
		VirtActionInvoker0::Invoke(4 /* System.Void UniRx.Operators.OperatorObserverBase`2<System.Collections.Generic.IList`1<System.Boolean>,System.Collections.Generic.IList`1<System.Boolean>>::Dispose() */, (OperatorObserverBase_2_t2939511529 *)__this);
		IL2CPP_END_FINALLY(18)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(18)
	{
		IL2CPP_JUMP_TBL(0x19, IL_0019)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0019:
	{
		return;
	}
}
// System.Void UniRx.Operators.CombineLatestObservable`1/CombineLatest<System.Object>::.ctor(UniRx.Operators.CombineLatestObservable`1<T>,UniRx.IObserver`1<System.Collections.Generic.IList`1<T>>,System.IDisposable)
extern Il2CppClass* Il2CppObject_il2cpp_TypeInfo_var;
extern const uint32_t CombineLatest__ctor_m279933093_MetadataUsageId;
extern "C"  void CombineLatest__ctor_m279933093_gshared (CombineLatest_t868253967 * __this, CombineLatestObservable_1_t3509237416 * ___parent0, Il2CppObject* ___observer1, Il2CppObject * ___cancel2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (CombineLatest__ctor_m279933093_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject * L_0 = (Il2CppObject *)il2cpp_codegen_object_new(Il2CppObject_il2cpp_TypeInfo_var);
		Object__ctor_m1772956182(L_0, /*hidden argument*/NULL);
		__this->set_gate_3(L_0);
		Il2CppObject* L_1 = ___observer1;
		Il2CppObject * L_2 = ___cancel2;
		NullCheck((OperatorObserverBase_2_t4067176365 *)__this);
		((  void (*) (OperatorObserverBase_2_t4067176365 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((OperatorObserverBase_2_t4067176365 *)__this, (Il2CppObject*)L_1, (Il2CppObject *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		CombineLatestObservable_1_t3509237416 * L_3 = ___parent0;
		__this->set_parent_2(L_3);
		return;
	}
}
// System.IDisposable UniRx.Operators.CombineLatestObservable`1/CombineLatest<System.Object>::Run()
extern Il2CppClass* BooleanU5BU5D_t3804927312_il2cpp_TypeInfo_var;
extern Il2CppClass* IDisposableU5BU5D_t165183403_il2cpp_TypeInfo_var;
extern const uint32_t CombineLatest_Run_m447223867_MetadataUsageId;
extern "C"  Il2CppObject * CombineLatest_Run_m447223867_gshared (CombineLatest_t868253967 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (CombineLatest_Run_m447223867_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	IDisposableU5BU5D_t165183403* V_0 = NULL;
	int32_t V_1 = 0;
	Il2CppObject* V_2 = NULL;
	{
		CombineLatestObservable_1_t3509237416 * L_0 = (CombineLatestObservable_1_t3509237416 *)__this->get_parent_2();
		NullCheck(L_0);
		IObservable_1U5BU5D_t2205425841* L_1 = (IObservable_1U5BU5D_t2205425841*)L_0->get_sources_1();
		NullCheck(L_1);
		__this->set_length_4((((int32_t)((int32_t)(((Il2CppArray *)L_1)->max_length)))));
		int32_t L_2 = (int32_t)__this->get_length_4();
		__this->set_values_5(((ObjectU5BU5D_t11523773*)SZArrayNew(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), (uint32_t)L_2)));
		int32_t L_3 = (int32_t)__this->get_length_4();
		__this->set_isStarted_6(((BooleanU5BU5D_t3804927312*)SZArrayNew(BooleanU5BU5D_t3804927312_il2cpp_TypeInfo_var, (uint32_t)L_3)));
		int32_t L_4 = (int32_t)__this->get_length_4();
		__this->set_isCompleted_7(((BooleanU5BU5D_t3804927312*)SZArrayNew(BooleanU5BU5D_t3804927312_il2cpp_TypeInfo_var, (uint32_t)L_4)));
		__this->set_isAllValueStarted_8((bool)0);
		int32_t L_5 = (int32_t)__this->get_length_4();
		V_0 = (IDisposableU5BU5D_t165183403*)((IDisposableU5BU5D_t165183403*)SZArrayNew(IDisposableU5BU5D_t165183403_il2cpp_TypeInfo_var, (uint32_t)L_5));
		V_1 = (int32_t)0;
		goto IL_0082;
	}

IL_0060:
	{
		CombineLatestObservable_1_t3509237416 * L_6 = (CombineLatestObservable_1_t3509237416 *)__this->get_parent_2();
		NullCheck(L_6);
		IObservable_1U5BU5D_t2205425841* L_7 = (IObservable_1U5BU5D_t2205425841*)L_6->get_sources_1();
		int32_t L_8 = V_1;
		NullCheck(L_7);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_7, L_8);
		int32_t L_9 = L_8;
		V_2 = (Il2CppObject*)((L_7)->GetAt(static_cast<il2cpp_array_size_t>(L_9)));
		IDisposableU5BU5D_t165183403* L_10 = V_0;
		int32_t L_11 = V_1;
		Il2CppObject* L_12 = V_2;
		int32_t L_13 = V_1;
		CombineLatestObserver_t301670549 * L_14 = (CombineLatestObserver_t301670549 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		((  void (*) (CombineLatestObserver_t301670549 *, CombineLatest_t868253967 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)(L_14, (CombineLatest_t868253967 *)__this, (int32_t)L_13, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		NullCheck((Il2CppObject*)L_12);
		Il2CppObject * L_15 = InterfaceFuncInvoker1< Il2CppObject *, Il2CppObject* >::Invoke(0 /* System.IDisposable UniRx.IObservable`1<System.Object>::Subscribe(UniRx.IObserver`1<T>) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5), (Il2CppObject*)L_12, (Il2CppObject*)L_14);
		NullCheck(L_10);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_10, L_11);
		ArrayElementTypeCheck (L_10, L_15);
		(L_10)->SetAt(static_cast<il2cpp_array_size_t>(L_11), (Il2CppObject *)L_15);
		int32_t L_16 = V_1;
		V_1 = (int32_t)((int32_t)((int32_t)L_16+(int32_t)1));
	}

IL_0082:
	{
		int32_t L_17 = V_1;
		int32_t L_18 = (int32_t)__this->get_length_4();
		if ((((int32_t)L_17) < ((int32_t)L_18)))
		{
			goto IL_0060;
		}
	}
	{
		IDisposableU5BU5D_t165183403* L_19 = V_0;
		Il2CppObject * L_20 = StableCompositeDisposable_CreateUnsafe_m1071468137(NULL /*static, unused*/, (IDisposableU5BU5D_t165183403*)L_19, /*hidden argument*/NULL);
		return L_20;
	}
}
// System.Void UniRx.Operators.CombineLatestObservable`1/CombineLatest<System.Object>::Publish(System.Int32)
extern "C"  void CombineLatest_Publish_m3050808939_gshared (CombineLatest_t868253967 * __this, int32_t ___index0, const MethodInfo* method)
{
	bool V_0 = false;
	int32_t V_1 = 0;
	bool V_2 = false;
	int32_t V_3 = 0;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		BooleanU5BU5D_t3804927312* L_0 = (BooleanU5BU5D_t3804927312*)__this->get_isStarted_6();
		int32_t L_1 = ___index0;
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, L_1);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(L_1), (bool)1);
		bool L_2 = (bool)__this->get_isAllValueStarted_8();
		if (!L_2)
		{
			goto IL_0026;
		}
	}
	{
		ObjectU5BU5D_t11523773* L_3 = (ObjectU5BU5D_t11523773*)__this->get_values_5();
		List_1_t1634065389 * L_4 = (List_1_t1634065389 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		((  void (*) (List_1_t1634065389 *, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7)->method)(L_4, (Il2CppObject*)(Il2CppObject*)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7));
		NullCheck((CombineLatest_t868253967 *)__this);
		VirtActionInvoker1< Il2CppObject* >::Invoke(8 /* System.Void UniRx.Operators.CombineLatestObservable`1/CombineLatest<System.Object>::OnNext(System.Collections.Generic.IList`1<T>) */, (CombineLatest_t868253967 *)__this, (Il2CppObject*)L_4);
		return;
	}

IL_0026:
	{
		V_0 = (bool)1;
		V_1 = (int32_t)0;
		goto IL_0047;
	}

IL_002f:
	{
		BooleanU5BU5D_t3804927312* L_5 = (BooleanU5BU5D_t3804927312*)__this->get_isStarted_6();
		int32_t L_6 = V_1;
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, L_6);
		int32_t L_7 = L_6;
		if (((L_5)->GetAt(static_cast<il2cpp_array_size_t>(L_7))))
		{
			goto IL_0043;
		}
	}
	{
		V_0 = (bool)0;
		goto IL_0053;
	}

IL_0043:
	{
		int32_t L_8 = V_1;
		V_1 = (int32_t)((int32_t)((int32_t)L_8+(int32_t)1));
	}

IL_0047:
	{
		int32_t L_9 = V_1;
		int32_t L_10 = (int32_t)__this->get_length_4();
		if ((((int32_t)L_9) < ((int32_t)L_10)))
		{
			goto IL_002f;
		}
	}

IL_0053:
	{
		bool L_11 = V_0;
		__this->set_isAllValueStarted_8(L_11);
		bool L_12 = (bool)__this->get_isAllValueStarted_8();
		if (!L_12)
		{
			goto IL_0077;
		}
	}
	{
		ObjectU5BU5D_t11523773* L_13 = (ObjectU5BU5D_t11523773*)__this->get_values_5();
		List_1_t1634065389 * L_14 = (List_1_t1634065389 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		((  void (*) (List_1_t1634065389 *, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7)->method)(L_14, (Il2CppObject*)(Il2CppObject*)L_13, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7));
		NullCheck((CombineLatest_t868253967 *)__this);
		VirtActionInvoker1< Il2CppObject* >::Invoke(8 /* System.Void UniRx.Operators.CombineLatestObservable`1/CombineLatest<System.Object>::OnNext(System.Collections.Generic.IList`1<T>) */, (CombineLatest_t868253967 *)__this, (Il2CppObject*)L_14);
		return;
	}

IL_0077:
	{
		V_2 = (bool)1;
		V_3 = (int32_t)0;
		goto IL_00a4;
	}

IL_0080:
	{
		int32_t L_15 = V_3;
		int32_t L_16 = ___index0;
		if ((!(((uint32_t)L_15) == ((uint32_t)L_16))))
		{
			goto IL_008c;
		}
	}
	{
		goto IL_00a0;
	}

IL_008c:
	{
		BooleanU5BU5D_t3804927312* L_17 = (BooleanU5BU5D_t3804927312*)__this->get_isCompleted_7();
		int32_t L_18 = V_3;
		NullCheck(L_17);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_17, L_18);
		int32_t L_19 = L_18;
		if (((L_17)->GetAt(static_cast<il2cpp_array_size_t>(L_19))))
		{
			goto IL_00a0;
		}
	}
	{
		V_2 = (bool)0;
		goto IL_00b0;
	}

IL_00a0:
	{
		int32_t L_20 = V_3;
		V_3 = (int32_t)((int32_t)((int32_t)L_20+(int32_t)1));
	}

IL_00a4:
	{
		int32_t L_21 = V_3;
		int32_t L_22 = (int32_t)__this->get_length_4();
		if ((((int32_t)L_21) < ((int32_t)L_22)))
		{
			goto IL_0080;
		}
	}

IL_00b0:
	{
		bool L_23 = V_2;
		if (!L_23)
		{
			goto IL_00d0;
		}
	}

IL_00b6:
	try
	{ // begin try (depth: 1)
		Il2CppObject* L_24 = (Il2CppObject*)((OperatorObserverBase_2_t4067176365 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		NullCheck((Il2CppObject*)L_24);
		InterfaceActionInvoker0::Invoke(0 /* System.Void UniRx.IObserver`1<System.Collections.Generic.IList`1<System.Object>>::OnCompleted() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9), (Il2CppObject*)L_24);
		IL2CPP_LEAVE(0xCF, FINALLY_00c8);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_00c8;
	}

FINALLY_00c8:
	{ // begin finally (depth: 1)
		NullCheck((OperatorObserverBase_2_t4067176365 *)__this);
		VirtActionInvoker0::Invoke(4 /* System.Void UniRx.Operators.OperatorObserverBase`2<System.Collections.Generic.IList`1<System.Object>,System.Collections.Generic.IList`1<System.Object>>::Dispose() */, (OperatorObserverBase_2_t4067176365 *)__this);
		IL2CPP_END_FINALLY(200)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(200)
	{
		IL2CPP_JUMP_TBL(0xCF, IL_00cf)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_00cf:
	{
		return;
	}

IL_00d0:
	{
		return;
	}
}
// System.Void UniRx.Operators.CombineLatestObservable`1/CombineLatest<System.Object>::OnNext(System.Collections.Generic.IList`1<T>)
extern "C"  void CombineLatest_OnNext_m2009271756_gshared (CombineLatest_t868253967 * __this, Il2CppObject* ___value0, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = (Il2CppObject*)((OperatorObserverBase_2_t4067176365 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		Il2CppObject* L_1 = ___value0;
		NullCheck((Il2CppObject*)L_0);
		InterfaceActionInvoker1< Il2CppObject* >::Invoke(2 /* System.Void UniRx.IObserver`1<System.Collections.Generic.IList`1<System.Object>>::OnNext(T) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9), (Il2CppObject*)L_0, (Il2CppObject*)L_1);
		return;
	}
}
// System.Void UniRx.Operators.CombineLatestObservable`1/CombineLatest<System.Object>::OnError(System.Exception)
extern "C"  void CombineLatest_OnError_m619400356_gshared (CombineLatest_t868253967 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method)
{
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		Il2CppObject* L_0 = (Il2CppObject*)((OperatorObserverBase_2_t4067176365 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		Exception_t1967233988 * L_1 = ___error0;
		NullCheck((Il2CppObject*)L_0);
		InterfaceActionInvoker1< Exception_t1967233988 * >::Invoke(1 /* System.Void UniRx.IObserver`1<System.Collections.Generic.IList`1<System.Object>>::OnError(System.Exception) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9), (Il2CppObject*)L_0, (Exception_t1967233988 *)L_1);
		IL2CPP_LEAVE(0x1A, FINALLY_0013);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0013;
	}

FINALLY_0013:
	{ // begin finally (depth: 1)
		NullCheck((OperatorObserverBase_2_t4067176365 *)__this);
		VirtActionInvoker0::Invoke(4 /* System.Void UniRx.Operators.OperatorObserverBase`2<System.Collections.Generic.IList`1<System.Object>,System.Collections.Generic.IList`1<System.Object>>::Dispose() */, (OperatorObserverBase_2_t4067176365 *)__this);
		IL2CPP_END_FINALLY(19)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(19)
	{
		IL2CPP_JUMP_TBL(0x1A, IL_001a)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_001a:
	{
		return;
	}
}
// System.Void UniRx.Operators.CombineLatestObservable`1/CombineLatest<System.Object>::OnCompleted()
extern "C"  void CombineLatest_OnCompleted_m2775373303_gshared (CombineLatest_t868253967 * __this, const MethodInfo* method)
{
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		Il2CppObject* L_0 = (Il2CppObject*)((OperatorObserverBase_2_t4067176365 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		NullCheck((Il2CppObject*)L_0);
		InterfaceActionInvoker0::Invoke(0 /* System.Void UniRx.IObserver`1<System.Collections.Generic.IList`1<System.Object>>::OnCompleted() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9), (Il2CppObject*)L_0);
		IL2CPP_LEAVE(0x19, FINALLY_0012);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0012;
	}

FINALLY_0012:
	{ // begin finally (depth: 1)
		NullCheck((OperatorObserverBase_2_t4067176365 *)__this);
		VirtActionInvoker0::Invoke(4 /* System.Void UniRx.Operators.OperatorObserverBase`2<System.Collections.Generic.IList`1<System.Object>,System.Collections.Generic.IList`1<System.Object>>::Dispose() */, (OperatorObserverBase_2_t4067176365 *)__this);
		IL2CPP_END_FINALLY(18)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(18)
	{
		IL2CPP_JUMP_TBL(0x19, IL_0019)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0019:
	{
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
