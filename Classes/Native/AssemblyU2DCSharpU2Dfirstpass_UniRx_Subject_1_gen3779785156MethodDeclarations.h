﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Subject`1<UniRx.CollectionReplaceEvent`1<System.Object>>
struct Subject_1_t3779785156;
// System.Exception
struct Exception_t1967233988;
// System.IDisposable
struct IDisposable_t1628921374;
// UniRx.IObserver`1<UniRx.CollectionReplaceEvent`1<System.Object>>
struct IObserver_1_t4053749439;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Exception1967233988.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_CollectionRepl1841750536.h"

// System.Void UniRx.Subject`1<UniRx.CollectionReplaceEvent`1<System.Object>>::.ctor()
extern "C"  void Subject_1__ctor_m1423627401_gshared (Subject_1_t3779785156 * __this, const MethodInfo* method);
#define Subject_1__ctor_m1423627401(__this, method) ((  void (*) (Subject_1_t3779785156 *, const MethodInfo*))Subject_1__ctor_m1423627401_gshared)(__this, method)
// System.Boolean UniRx.Subject`1<UniRx.CollectionReplaceEvent`1<System.Object>>::get_HasObservers()
extern "C"  bool Subject_1_get_HasObservers_m689506659_gshared (Subject_1_t3779785156 * __this, const MethodInfo* method);
#define Subject_1_get_HasObservers_m689506659(__this, method) ((  bool (*) (Subject_1_t3779785156 *, const MethodInfo*))Subject_1_get_HasObservers_m689506659_gshared)(__this, method)
// System.Void UniRx.Subject`1<UniRx.CollectionReplaceEvent`1<System.Object>>::OnCompleted()
extern "C"  void Subject_1_OnCompleted_m848084307_gshared (Subject_1_t3779785156 * __this, const MethodInfo* method);
#define Subject_1_OnCompleted_m848084307(__this, method) ((  void (*) (Subject_1_t3779785156 *, const MethodInfo*))Subject_1_OnCompleted_m848084307_gshared)(__this, method)
// System.Void UniRx.Subject`1<UniRx.CollectionReplaceEvent`1<System.Object>>::OnError(System.Exception)
extern "C"  void Subject_1_OnError_m3984525824_gshared (Subject_1_t3779785156 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method);
#define Subject_1_OnError_m3984525824(__this, ___error0, method) ((  void (*) (Subject_1_t3779785156 *, Exception_t1967233988 *, const MethodInfo*))Subject_1_OnError_m3984525824_gshared)(__this, ___error0, method)
// System.Void UniRx.Subject`1<UniRx.CollectionReplaceEvent`1<System.Object>>::OnNext(T)
extern "C"  void Subject_1_OnNext_m842068209_gshared (Subject_1_t3779785156 * __this, CollectionReplaceEvent_1_t1841750536  ___value0, const MethodInfo* method);
#define Subject_1_OnNext_m842068209(__this, ___value0, method) ((  void (*) (Subject_1_t3779785156 *, CollectionReplaceEvent_1_t1841750536 , const MethodInfo*))Subject_1_OnNext_m842068209_gshared)(__this, ___value0, method)
// System.IDisposable UniRx.Subject`1<UniRx.CollectionReplaceEvent`1<System.Object>>::Subscribe(UniRx.IObserver`1<T>)
extern "C"  Il2CppObject * Subject_1_Subscribe_m3126210440_gshared (Subject_1_t3779785156 * __this, Il2CppObject* ___observer0, const MethodInfo* method);
#define Subject_1_Subscribe_m3126210440(__this, ___observer0, method) ((  Il2CppObject * (*) (Subject_1_t3779785156 *, Il2CppObject*, const MethodInfo*))Subject_1_Subscribe_m3126210440_gshared)(__this, ___observer0, method)
// System.Void UniRx.Subject`1<UniRx.CollectionReplaceEvent`1<System.Object>>::Dispose()
extern "C"  void Subject_1_Dispose_m2204615302_gshared (Subject_1_t3779785156 * __this, const MethodInfo* method);
#define Subject_1_Dispose_m2204615302(__this, method) ((  void (*) (Subject_1_t3779785156 *, const MethodInfo*))Subject_1_Dispose_m2204615302_gshared)(__this, method)
// System.Void UniRx.Subject`1<UniRx.CollectionReplaceEvent`1<System.Object>>::ThrowIfDisposed()
extern "C"  void Subject_1_ThrowIfDisposed_m3010823695_gshared (Subject_1_t3779785156 * __this, const MethodInfo* method);
#define Subject_1_ThrowIfDisposed_m3010823695(__this, method) ((  void (*) (Subject_1_t3779785156 *, const MethodInfo*))Subject_1_ThrowIfDisposed_m3010823695_gshared)(__this, method)
// System.Boolean UniRx.Subject`1<UniRx.CollectionReplaceEvent`1<System.Object>>::IsRequiredSubscribeOnCurrentThread()
extern "C"  bool Subject_1_IsRequiredSubscribeOnCurrentThread_m3491300250_gshared (Subject_1_t3779785156 * __this, const MethodInfo* method);
#define Subject_1_IsRequiredSubscribeOnCurrentThread_m3491300250(__this, method) ((  bool (*) (Subject_1_t3779785156 *, const MethodInfo*))Subject_1_IsRequiredSubscribeOnCurrentThread_m3491300250_gshared)(__this, method)
