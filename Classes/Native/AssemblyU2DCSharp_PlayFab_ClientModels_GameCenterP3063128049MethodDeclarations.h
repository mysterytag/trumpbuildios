﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.ClientModels.GameCenterPlayFabIdPair
struct GameCenterPlayFabIdPair_t3063128049;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String968488902.h"

// System.Void PlayFab.ClientModels.GameCenterPlayFabIdPair::.ctor()
extern "C"  void GameCenterPlayFabIdPair__ctor_m3199140056 (GameCenterPlayFabIdPair_t3063128049 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PlayFab.ClientModels.GameCenterPlayFabIdPair::get_GameCenterId()
extern "C"  String_t* GameCenterPlayFabIdPair_get_GameCenterId_m1283802362 (GameCenterPlayFabIdPair_t3063128049 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.GameCenterPlayFabIdPair::set_GameCenterId(System.String)
extern "C"  void GameCenterPlayFabIdPair_set_GameCenterId_m2286187671 (GameCenterPlayFabIdPair_t3063128049 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PlayFab.ClientModels.GameCenterPlayFabIdPair::get_PlayFabId()
extern "C"  String_t* GameCenterPlayFabIdPair_get_PlayFabId_m3143702136 (GameCenterPlayFabIdPair_t3063128049 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.GameCenterPlayFabIdPair::set_PlayFabId(System.String)
extern "C"  void GameCenterPlayFabIdPair_set_PlayFabId_m2051150235 (GameCenterPlayFabIdPair_t3063128049 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
