﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Zenject.Factory`1<System.Object>
struct Factory_1_t250748329;
// System.Type
struct Type_t;
// System.Type[]
struct TypeU5BU5D_t3431720054;
// Zenject.DiContainer
struct DiContainer_t2383114449;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void Zenject.Factory`1<System.Object>::.ctor()
extern "C"  void Factory_1__ctor_m2283776443_gshared (Factory_1_t250748329 * __this, const MethodInfo* method);
#define Factory_1__ctor_m2283776443(__this, method) ((  void (*) (Factory_1_t250748329 *, const MethodInfo*))Factory_1__ctor_m2283776443_gshared)(__this, method)
// System.Type Zenject.Factory`1<System.Object>::get_ConstructedType()
extern "C"  Type_t * Factory_1_get_ConstructedType_m2970522342_gshared (Factory_1_t250748329 * __this, const MethodInfo* method);
#define Factory_1_get_ConstructedType_m2970522342(__this, method) ((  Type_t * (*) (Factory_1_t250748329 *, const MethodInfo*))Factory_1_get_ConstructedType_m2970522342_gshared)(__this, method)
// System.Type[] Zenject.Factory`1<System.Object>::get_ProvidedTypes()
extern "C"  TypeU5BU5D_t3431720054* Factory_1_get_ProvidedTypes_m1882595918_gshared (Factory_1_t250748329 * __this, const MethodInfo* method);
#define Factory_1_get_ProvidedTypes_m1882595918(__this, method) ((  TypeU5BU5D_t3431720054* (*) (Factory_1_t250748329 *, const MethodInfo*))Factory_1_get_ProvidedTypes_m1882595918_gshared)(__this, method)
// Zenject.DiContainer Zenject.Factory`1<System.Object>::get_Container()
extern "C"  DiContainer_t2383114449 * Factory_1_get_Container_m3180103707_gshared (Factory_1_t250748329 * __this, const MethodInfo* method);
#define Factory_1_get_Container_m3180103707(__this, method) ((  DiContainer_t2383114449 * (*) (Factory_1_t250748329 *, const MethodInfo*))Factory_1_get_Container_m3180103707_gshared)(__this, method)
// T Zenject.Factory`1<System.Object>::Create()
extern "C"  Il2CppObject * Factory_1_Create_m3665880292_gshared (Factory_1_t250748329 * __this, const MethodInfo* method);
#define Factory_1_Create_m3665880292(__this, method) ((  Il2CppObject * (*) (Factory_1_t250748329 *, const MethodInfo*))Factory_1_Create_m3665880292_gshared)(__this, method)
