﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Observer/AnonymousObserver`1<System.Object>
struct AnonymousObserver_1_t4218636717;
// System.Action`1<System.Object>
struct Action_1_t985559125;
// System.Action`1<System.Exception>
struct Action_1_t2115686693;
// System.Action
struct Action_t437523947;
// System.Object
struct Il2CppObject;
// System.Exception
struct Exception_t1967233988;

#include "codegen/il2cpp-codegen.h"
#include "System_Core_System_Action437523947.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_Exception1967233988.h"

// System.Void UniRx.Observer/AnonymousObserver`1<System.Object>::.ctor(System.Action`1<T>,System.Action`1<System.Exception>,System.Action)
extern "C"  void AnonymousObserver_1__ctor_m43751705_gshared (AnonymousObserver_1_t4218636717 * __this, Action_1_t985559125 * ___onNext0, Action_1_t2115686693 * ___onError1, Action_t437523947 * ___onCompleted2, const MethodInfo* method);
#define AnonymousObserver_1__ctor_m43751705(__this, ___onNext0, ___onError1, ___onCompleted2, method) ((  void (*) (AnonymousObserver_1_t4218636717 *, Action_1_t985559125 *, Action_1_t2115686693 *, Action_t437523947 *, const MethodInfo*))AnonymousObserver_1__ctor_m43751705_gshared)(__this, ___onNext0, ___onError1, ___onCompleted2, method)
// System.Void UniRx.Observer/AnonymousObserver`1<System.Object>::OnNext(T)
extern "C"  void AnonymousObserver_1_OnNext_m1868171254_gshared (AnonymousObserver_1_t4218636717 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define AnonymousObserver_1_OnNext_m1868171254(__this, ___value0, method) ((  void (*) (AnonymousObserver_1_t4218636717 *, Il2CppObject *, const MethodInfo*))AnonymousObserver_1_OnNext_m1868171254_gshared)(__this, ___value0, method)
// System.Void UniRx.Observer/AnonymousObserver`1<System.Object>::OnError(System.Exception)
extern "C"  void AnonymousObserver_1_OnError_m2397796101_gshared (AnonymousObserver_1_t4218636717 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method);
#define AnonymousObserver_1_OnError_m2397796101(__this, ___error0, method) ((  void (*) (AnonymousObserver_1_t4218636717 *, Exception_t1967233988 *, const MethodInfo*))AnonymousObserver_1_OnError_m2397796101_gshared)(__this, ___error0, method)
// System.Void UniRx.Observer/AnonymousObserver`1<System.Object>::OnCompleted()
extern "C"  void AnonymousObserver_1_OnCompleted_m4153985496_gshared (AnonymousObserver_1_t4218636717 * __this, const MethodInfo* method);
#define AnonymousObserver_1_OnCompleted_m4153985496(__this, method) ((  void (*) (AnonymousObserver_1_t4218636717 *, const MethodInfo*))AnonymousObserver_1_OnCompleted_m4153985496_gshared)(__this, method)
