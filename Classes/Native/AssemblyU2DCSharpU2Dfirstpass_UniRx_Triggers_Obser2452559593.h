﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UniRx.Subject`1<UniRx.Unit>
struct Subject_1_t201353362;

#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Triggers_Obser1883237895.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Triggers.ObservableCanvasGroupChangedTrigger
struct  ObservableCanvasGroupChangedTrigger_t2452559593  : public ObservableTriggerBase_t1883237895
{
public:
	// UniRx.Subject`1<UniRx.Unit> UniRx.Triggers.ObservableCanvasGroupChangedTrigger::onCanvasGroupChanged
	Subject_1_t201353362 * ___onCanvasGroupChanged_8;

public:
	inline static int32_t get_offset_of_onCanvasGroupChanged_8() { return static_cast<int32_t>(offsetof(ObservableCanvasGroupChangedTrigger_t2452559593, ___onCanvasGroupChanged_8)); }
	inline Subject_1_t201353362 * get_onCanvasGroupChanged_8() const { return ___onCanvasGroupChanged_8; }
	inline Subject_1_t201353362 ** get_address_of_onCanvasGroupChanged_8() { return &___onCanvasGroupChanged_8; }
	inline void set_onCanvasGroupChanged_8(Subject_1_t201353362 * value)
	{
		___onCanvasGroupChanged_8 = value;
		Il2CppCodeGenWriteBarrier(&___onCanvasGroupChanged_8, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
