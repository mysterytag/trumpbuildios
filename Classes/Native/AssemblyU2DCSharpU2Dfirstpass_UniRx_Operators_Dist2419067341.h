﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UniRx.Operators.DistinctUntilChangedObservable`2<System.Object,System.Object>
struct DistinctUntilChangedObservable_2_t1596230355;
// System.Object
struct Il2CppObject;

#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Oper1187768149.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Operators.DistinctUntilChangedObservable`2/DistinctUntilChanged<System.Object,System.Object>
struct  DistinctUntilChanged_t2419067341  : public OperatorObserverBase_2_t1187768149
{
public:
	// UniRx.Operators.DistinctUntilChangedObservable`2<T,TKey> UniRx.Operators.DistinctUntilChangedObservable`2/DistinctUntilChanged::parent
	DistinctUntilChangedObservable_2_t1596230355 * ___parent_2;
	// System.Boolean UniRx.Operators.DistinctUntilChangedObservable`2/DistinctUntilChanged::isFirst
	bool ___isFirst_3;
	// TKey UniRx.Operators.DistinctUntilChangedObservable`2/DistinctUntilChanged::prevKey
	Il2CppObject * ___prevKey_4;

public:
	inline static int32_t get_offset_of_parent_2() { return static_cast<int32_t>(offsetof(DistinctUntilChanged_t2419067341, ___parent_2)); }
	inline DistinctUntilChangedObservable_2_t1596230355 * get_parent_2() const { return ___parent_2; }
	inline DistinctUntilChangedObservable_2_t1596230355 ** get_address_of_parent_2() { return &___parent_2; }
	inline void set_parent_2(DistinctUntilChangedObservable_2_t1596230355 * value)
	{
		___parent_2 = value;
		Il2CppCodeGenWriteBarrier(&___parent_2, value);
	}

	inline static int32_t get_offset_of_isFirst_3() { return static_cast<int32_t>(offsetof(DistinctUntilChanged_t2419067341, ___isFirst_3)); }
	inline bool get_isFirst_3() const { return ___isFirst_3; }
	inline bool* get_address_of_isFirst_3() { return &___isFirst_3; }
	inline void set_isFirst_3(bool value)
	{
		___isFirst_3 = value;
	}

	inline static int32_t get_offset_of_prevKey_4() { return static_cast<int32_t>(offsetof(DistinctUntilChanged_t2419067341, ___prevKey_4)); }
	inline Il2CppObject * get_prevKey_4() const { return ___prevKey_4; }
	inline Il2CppObject ** get_address_of_prevKey_4() { return &___prevKey_4; }
	inline void set_prevKey_4(Il2CppObject * value)
	{
		___prevKey_4 = value;
		Il2CppCodeGenWriteBarrier(&___prevKey_4, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
