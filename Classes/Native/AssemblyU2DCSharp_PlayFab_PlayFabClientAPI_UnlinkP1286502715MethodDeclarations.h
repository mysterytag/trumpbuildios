﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.PlayFabClientAPI/UnlinkPSNAccountResponseCallback
struct UnlinkPSNAccountResponseCallback_t1286502715;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;
// PlayFab.ClientModels.UnlinkPSNAccountRequest
struct UnlinkPSNAccountRequest_t26689898;
// PlayFab.ClientModels.UnlinkPSNAccountResult
struct UnlinkPSNAccountResult_t2391313410;
// PlayFab.PlayFabError
struct PlayFabError_t750598646;
// System.IAsyncResult
struct IAsyncResult_t537683269;
// System.AsyncCallback
struct AsyncCallback_t1363551830;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_IntPtr676692020.h"
#include "mscorlib_System_String968488902.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_UnlinkPSNAcco26689898.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_UnlinkPSNAc2391313410.h"
#include "AssemblyU2DCSharpU2Dfirstpass_PlayFab_PlayFabError750598646.h"
#include "mscorlib_System_AsyncCallback1363551830.h"

// System.Void PlayFab.PlayFabClientAPI/UnlinkPSNAccountResponseCallback::.ctor(System.Object,System.IntPtr)
extern "C"  void UnlinkPSNAccountResponseCallback__ctor_m1875720362 (UnlinkPSNAccountResponseCallback_t1286502715 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/UnlinkPSNAccountResponseCallback::Invoke(System.String,System.Int32,PlayFab.ClientModels.UnlinkPSNAccountRequest,PlayFab.ClientModels.UnlinkPSNAccountResult,PlayFab.PlayFabError,System.Object)
extern "C"  void UnlinkPSNAccountResponseCallback_Invoke_m2190112471 (UnlinkPSNAccountResponseCallback_t1286502715 * __this, String_t* ___urlPath0, int32_t ___callId1, UnlinkPSNAccountRequest_t26689898 * ___request2, UnlinkPSNAccountResult_t2391313410 * ___result3, PlayFabError_t750598646 * ___error4, Il2CppObject * ___customData5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
extern "C" void pinvoke_delegate_wrapper_UnlinkPSNAccountResponseCallback_t1286502715(Il2CppObject* delegate, String_t* ___urlPath0, int32_t ___callId1, UnlinkPSNAccountRequest_t26689898 * ___request2, UnlinkPSNAccountResult_t2391313410 * ___result3, PlayFabError_t750598646 * ___error4, Il2CppObject * ___customData5);
// System.IAsyncResult PlayFab.PlayFabClientAPI/UnlinkPSNAccountResponseCallback::BeginInvoke(System.String,System.Int32,PlayFab.ClientModels.UnlinkPSNAccountRequest,PlayFab.ClientModels.UnlinkPSNAccountResult,PlayFab.PlayFabError,System.Object,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * UnlinkPSNAccountResponseCallback_BeginInvoke_m1884874308 (UnlinkPSNAccountResponseCallback_t1286502715 * __this, String_t* ___urlPath0, int32_t ___callId1, UnlinkPSNAccountRequest_t26689898 * ___request2, UnlinkPSNAccountResult_t2391313410 * ___result3, PlayFabError_t750598646 * ___error4, Il2CppObject * ___customData5, AsyncCallback_t1363551830 * ___callback6, Il2CppObject * ___object7, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/UnlinkPSNAccountResponseCallback::EndInvoke(System.IAsyncResult)
extern "C"  void UnlinkPSNAccountResponseCallback_EndInvoke_m3659239354 (UnlinkPSNAccountResponseCallback_t1286502715 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
