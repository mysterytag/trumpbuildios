﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Zenject.PrefabSingletonId
struct PrefabSingletonId_t3643845047;
// System.String
struct String_t;
// UnityEngine.GameObject
struct GameObject_t4012695102;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String968488902.h"
#include "UnityEngine_UnityEngine_GameObject4012695102.h"
#include "mscorlib_System_Object837106420.h"
#include "AssemblyU2DCSharp_Zenject_PrefabSingletonId3643845047.h"

// System.Void Zenject.PrefabSingletonId::.ctor(System.String,UnityEngine.GameObject,System.String)
extern "C"  void PrefabSingletonId__ctor_m1967924800 (PrefabSingletonId_t3643845047 * __this, String_t* ___identifier0, GameObject_t4012695102 * ___prefab1, String_t* ___resourcePath2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Zenject.PrefabSingletonId::GetHashCode()
extern "C"  int32_t PrefabSingletonId_GetHashCode_m781106565 (PrefabSingletonId_t3643845047 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Zenject.PrefabSingletonId::Equals(System.Object)
extern "C"  bool PrefabSingletonId_Equals_m547980653 (PrefabSingletonId_t3643845047 * __this, Il2CppObject * ___other0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Zenject.PrefabSingletonId::Equals(Zenject.PrefabSingletonId)
extern "C"  bool PrefabSingletonId_Equals_m230715624 (PrefabSingletonId_t3643845047 * __this, PrefabSingletonId_t3643845047 * ___that0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Zenject.PrefabSingletonId::op_Equality(Zenject.PrefabSingletonId,Zenject.PrefabSingletonId)
extern "C"  bool PrefabSingletonId_op_Equality_m3022805670 (Il2CppObject * __this /* static, unused */, PrefabSingletonId_t3643845047 * ___left0, PrefabSingletonId_t3643845047 * ___right1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Zenject.PrefabSingletonId::op_Inequality(Zenject.PrefabSingletonId,Zenject.PrefabSingletonId)
extern "C"  bool PrefabSingletonId_op_Inequality_m2327533921 (Il2CppObject * __this /* static, unused */, PrefabSingletonId_t3643845047 * ___left0, PrefabSingletonId_t3643845047 * ___right1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
