﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Reactor`1/<TransactionUnpackReact>c__AnonStorey141<System.DateTime>
struct U3CTransactionUnpackReactU3Ec__AnonStorey141_t2666977247;

#include "codegen/il2cpp-codegen.h"

// System.Void Reactor`1/<TransactionUnpackReact>c__AnonStorey141<System.DateTime>::.ctor()
extern "C"  void U3CTransactionUnpackReactU3Ec__AnonStorey141__ctor_m644935041_gshared (U3CTransactionUnpackReactU3Ec__AnonStorey141_t2666977247 * __this, const MethodInfo* method);
#define U3CTransactionUnpackReactU3Ec__AnonStorey141__ctor_m644935041(__this, method) ((  void (*) (U3CTransactionUnpackReactU3Ec__AnonStorey141_t2666977247 *, const MethodInfo*))U3CTransactionUnpackReactU3Ec__AnonStorey141__ctor_m644935041_gshared)(__this, method)
// System.Void Reactor`1/<TransactionUnpackReact>c__AnonStorey141<System.DateTime>::<>m__1D6()
extern "C"  void U3CTransactionUnpackReactU3Ec__AnonStorey141_U3CU3Em__1D6_m406495931_gshared (U3CTransactionUnpackReactU3Ec__AnonStorey141_t2666977247 * __this, const MethodInfo* method);
#define U3CTransactionUnpackReactU3Ec__AnonStorey141_U3CU3Em__1D6_m406495931(__this, method) ((  void (*) (U3CTransactionUnpackReactU3Ec__AnonStorey141_t2666977247 *, const MethodInfo*))U3CTransactionUnpackReactU3Ec__AnonStorey141_U3CU3Em__1D6_m406495931_gshared)(__this, method)
