﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.ClientModels.SubtractUserVirtualCurrencyRequest
struct SubtractUserVirtualCurrencyRequest_t3004138178;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String968488902.h"

// System.Void PlayFab.ClientModels.SubtractUserVirtualCurrencyRequest::.ctor()
extern "C"  void SubtractUserVirtualCurrencyRequest__ctor_m64819515 (SubtractUserVirtualCurrencyRequest_t3004138178 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PlayFab.ClientModels.SubtractUserVirtualCurrencyRequest::get_VirtualCurrency()
extern "C"  String_t* SubtractUserVirtualCurrencyRequest_get_VirtualCurrency_m3608477135 (SubtractUserVirtualCurrencyRequest_t3004138178 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.SubtractUserVirtualCurrencyRequest::set_VirtualCurrency(System.String)
extern "C"  void SubtractUserVirtualCurrencyRequest_set_VirtualCurrency_m1885821706 (SubtractUserVirtualCurrencyRequest_t3004138178 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 PlayFab.ClientModels.SubtractUserVirtualCurrencyRequest::get_Amount()
extern "C"  int32_t SubtractUserVirtualCurrencyRequest_get_Amount_m1785151100 (SubtractUserVirtualCurrencyRequest_t3004138178 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.SubtractUserVirtualCurrencyRequest::set_Amount(System.Int32)
extern "C"  void SubtractUserVirtualCurrencyRequest_set_Amount_m896065679 (SubtractUserVirtualCurrencyRequest_t3004138178 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
