﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Generic.List`1<System.String>
struct List_1_t1765447871;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PlayFab.ClientModels.GetPublisherDataRequest
struct  GetPublisherDataRequest_t1266178127  : public Il2CppObject
{
public:
	// System.Collections.Generic.List`1<System.String> PlayFab.ClientModels.GetPublisherDataRequest::<Keys>k__BackingField
	List_1_t1765447871 * ___U3CKeysU3Ek__BackingField_0;

public:
	inline static int32_t get_offset_of_U3CKeysU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(GetPublisherDataRequest_t1266178127, ___U3CKeysU3Ek__BackingField_0)); }
	inline List_1_t1765447871 * get_U3CKeysU3Ek__BackingField_0() const { return ___U3CKeysU3Ek__BackingField_0; }
	inline List_1_t1765447871 ** get_address_of_U3CKeysU3Ek__BackingField_0() { return &___U3CKeysU3Ek__BackingField_0; }
	inline void set_U3CKeysU3Ek__BackingField_0(List_1_t1765447871 * value)
	{
		___U3CKeysU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier(&___U3CKeysU3Ek__BackingField_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
