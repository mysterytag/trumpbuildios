﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// LocalyticsUnity.Localytics/ReceiveAnalyticsDelegate
struct ReceiveAnalyticsDelegate_t2804656808;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;
// System.IAsyncResult
struct IAsyncResult_t537683269;
// System.AsyncCallback
struct AsyncCallback_t1363551830;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_IntPtr676692020.h"
#include "mscorlib_System_String968488902.h"
#include "mscorlib_System_AsyncCallback1363551830.h"

// System.Void LocalyticsUnity.Localytics/ReceiveAnalyticsDelegate::.ctor(System.Object,System.IntPtr)
extern "C"  void ReceiveAnalyticsDelegate__ctor_m1029077999 (ReceiveAnalyticsDelegate_t2804656808 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LocalyticsUnity.Localytics/ReceiveAnalyticsDelegate::Invoke(System.String)
extern "C"  void ReceiveAnalyticsDelegate_Invoke_m1077747865 (ReceiveAnalyticsDelegate_t2804656808 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
extern "C" void pinvoke_delegate_wrapper_ReceiveAnalyticsDelegate_t2804656808(Il2CppObject* delegate, String_t* ___message0);
// System.IAsyncResult LocalyticsUnity.Localytics/ReceiveAnalyticsDelegate::BeginInvoke(System.String,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * ReceiveAnalyticsDelegate_BeginInvoke_m1459533478 (ReceiveAnalyticsDelegate_t2804656808 * __this, String_t* ___message0, AsyncCallback_t1363551830 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LocalyticsUnity.Localytics/ReceiveAnalyticsDelegate::EndInvoke(System.IAsyncResult)
extern "C"  void ReceiveAnalyticsDelegate_EndInvoke_m3597352319 (ReceiveAnalyticsDelegate_t2804656808 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
