﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "System_System_Collections_Generic_LinkedList_1_gen2577235966MethodDeclarations.h"

// System.Void System.Collections.Generic.LinkedList`1<Zenject.TaskUpdater`1/TaskInfo<Zenject.ITickable>>::.ctor()
#define LinkedList_1__ctor_m3677729894(__this, method) ((  void (*) (LinkedList_1_t2757043551 *, const MethodInfo*))LinkedList_1__ctor_m2955457271_gshared)(__this, method)
// System.Void System.Collections.Generic.LinkedList`1<Zenject.TaskUpdater`1/TaskInfo<Zenject.ITickable>>::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
#define LinkedList_1__ctor_m1728991399(__this, ___info0, ___context1, method) ((  void (*) (LinkedList_1_t2757043551 *, SerializationInfo_t2995724695 *, StreamingContext_t986364934 , const MethodInfo*))LinkedList_1__ctor_m3369579448_gshared)(__this, ___info0, ___context1, method)
// System.Void System.Collections.Generic.LinkedList`1<Zenject.TaskUpdater`1/TaskInfo<Zenject.ITickable>>::System.Collections.Generic.ICollection<T>.Add(T)
#define LinkedList_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m1456553177(__this, ___value0, method) ((  void (*) (LinkedList_1_t2757043551 *, TaskInfo_t1016914005 *, const MethodInfo*))LinkedList_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m3576108392_gshared)(__this, ___value0, method)
// System.Void System.Collections.Generic.LinkedList`1<Zenject.TaskUpdater`1/TaskInfo<Zenject.ITickable>>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define LinkedList_1_System_Collections_ICollection_CopyTo_m4101054366(__this, ___array0, ___index1, method) ((  void (*) (LinkedList_1_t2757043551 *, Il2CppArray *, int32_t, const MethodInfo*))LinkedList_1_System_Collections_ICollection_CopyTo_m2331638317_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.LinkedList`1<Zenject.TaskUpdater`1/TaskInfo<Zenject.ITickable>>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
#define LinkedList_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m2912584504(__this, method) ((  Il2CppObject* (*) (LinkedList_1_t2757043551 *, const MethodInfo*))LinkedList_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m2027502985_gshared)(__this, method)
// System.Collections.IEnumerator System.Collections.Generic.LinkedList`1<Zenject.TaskUpdater`1/TaskInfo<Zenject.ITickable>>::System.Collections.IEnumerable.GetEnumerator()
#define LinkedList_1_System_Collections_IEnumerable_GetEnumerator_m3442929965(__this, method) ((  Il2CppObject * (*) (LinkedList_1_t2757043551 *, const MethodInfo*))LinkedList_1_System_Collections_IEnumerable_GetEnumerator_m51916412_gshared)(__this, method)
// System.Boolean System.Collections.Generic.LinkedList`1<Zenject.TaskUpdater`1/TaskInfo<Zenject.ITickable>>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
#define LinkedList_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m1102381673(__this, method) ((  bool (*) (LinkedList_1_t2757043551 *, const MethodInfo*))LinkedList_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m2230847288_gshared)(__this, method)
// System.Boolean System.Collections.Generic.LinkedList`1<Zenject.TaskUpdater`1/TaskInfo<Zenject.ITickable>>::System.Collections.ICollection.get_IsSynchronized()
#define LinkedList_1_System_Collections_ICollection_get_IsSynchronized_m2744953332(__this, method) ((  bool (*) (LinkedList_1_t2757043551 *, const MethodInfo*))LinkedList_1_System_Collections_ICollection_get_IsSynchronized_m683991045_gshared)(__this, method)
// System.Object System.Collections.Generic.LinkedList`1<Zenject.TaskUpdater`1/TaskInfo<Zenject.ITickable>>::System.Collections.ICollection.get_SyncRoot()
#define LinkedList_1_System_Collections_ICollection_get_SyncRoot_m147304244(__this, method) ((  Il2CppObject * (*) (LinkedList_1_t2757043551 *, const MethodInfo*))LinkedList_1_System_Collections_ICollection_get_SyncRoot_m573420165_gshared)(__this, method)
// System.Void System.Collections.Generic.LinkedList`1<Zenject.TaskUpdater`1/TaskInfo<Zenject.ITickable>>::VerifyReferencedNode(System.Collections.Generic.LinkedListNode`1<T>)
#define LinkedList_1_VerifyReferencedNode_m3496556421(__this, ___node0, method) ((  void (*) (LinkedList_1_t2757043551 *, LinkedListNode_1_t718904625 *, const MethodInfo*))LinkedList_1_VerifyReferencedNode_m3939775124_gshared)(__this, ___node0, method)
// System.Collections.Generic.LinkedListNode`1<T> System.Collections.Generic.LinkedList`1<Zenject.TaskUpdater`1/TaskInfo<Zenject.ITickable>>::AddBefore(System.Collections.Generic.LinkedListNode`1<T>,T)
#define LinkedList_1_AddBefore_m2072127741(__this, ___node0, ___value1, method) ((  LinkedListNode_1_t718904625 * (*) (LinkedList_1_t2757043551 *, LinkedListNode_1_t718904625 *, TaskInfo_t1016914005 *, const MethodInfo*))LinkedList_1_AddBefore_m1086189582_gshared)(__this, ___node0, ___value1, method)
// System.Collections.Generic.LinkedListNode`1<T> System.Collections.Generic.LinkedList`1<Zenject.TaskUpdater`1/TaskInfo<Zenject.ITickable>>::AddLast(T)
#define LinkedList_1_AddLast_m1451089269(__this, ___value0, method) ((  LinkedListNode_1_t718904625 * (*) (LinkedList_1_t2757043551 *, TaskInfo_t1016914005 *, const MethodInfo*))LinkedList_1_AddLast_m4070107716_gshared)(__this, ___value0, method)
// System.Void System.Collections.Generic.LinkedList`1<Zenject.TaskUpdater`1/TaskInfo<Zenject.ITickable>>::Clear()
#define LinkedList_1_Clear_m1083863185(__this, method) ((  void (*) (LinkedList_1_t2757043551 *, const MethodInfo*))LinkedList_1_Clear_m361590562_gshared)(__this, method)
// System.Boolean System.Collections.Generic.LinkedList`1<Zenject.TaskUpdater`1/TaskInfo<Zenject.ITickable>>::Contains(T)
#define LinkedList_1_Contains_m2759034795(__this, ___value0, method) ((  bool (*) (LinkedList_1_t2757043551 *, TaskInfo_t1016914005 *, const MethodInfo*))LinkedList_1_Contains_m3484410556_gshared)(__this, ___value0, method)
// System.Void System.Collections.Generic.LinkedList`1<Zenject.TaskUpdater`1/TaskInfo<Zenject.ITickable>>::CopyTo(T[],System.Int32)
#define LinkedList_1_CopyTo_m1284262665(__this, ___array0, ___index1, method) ((  void (*) (LinkedList_1_t2757043551 *, TaskInfoU5BU5D_t2133570232*, int32_t, const MethodInfo*))LinkedList_1_CopyTo_m3470139544_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.LinkedListNode`1<T> System.Collections.Generic.LinkedList`1<Zenject.TaskUpdater`1/TaskInfo<Zenject.ITickable>>::Find(T)
#define LinkedList_1_Find_m2233140053(__this, ___value0, method) ((  LinkedListNode_1_t718904625 * (*) (LinkedList_1_t2757043551 *, TaskInfo_t1016914005 *, const MethodInfo*))LinkedList_1_Find_m2643247334_gshared)(__this, ___value0, method)
// System.Collections.Generic.LinkedList`1/Enumerator<T> System.Collections.Generic.LinkedList`1<Zenject.TaskUpdater`1/TaskInfo<Zenject.ITickable>>::GetEnumerator()
#define LinkedList_1_GetEnumerator_m1858631413(__this, method) ((  Enumerator_t4194623262  (*) (LinkedList_1_t2757043551 *, const MethodInfo*))LinkedList_1_GetEnumerator_m3713737734_gshared)(__this, method)
// System.Void System.Collections.Generic.LinkedList`1<Zenject.TaskUpdater`1/TaskInfo<Zenject.ITickable>>::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
#define LinkedList_1_GetObjectData_m346292484(__this, ___info0, ___context1, method) ((  void (*) (LinkedList_1_t2757043551 *, SerializationInfo_t2995724695 *, StreamingContext_t986364934 , const MethodInfo*))LinkedList_1_GetObjectData_m3974480661_gshared)(__this, ___info0, ___context1, method)
// System.Void System.Collections.Generic.LinkedList`1<Zenject.TaskUpdater`1/TaskInfo<Zenject.ITickable>>::OnDeserialization(System.Object)
#define LinkedList_1_OnDeserialization_m3782724576(__this, ___sender0, method) ((  void (*) (LinkedList_1_t2757043551 *, Il2CppObject *, const MethodInfo*))LinkedList_1_OnDeserialization_m3445006959_gshared)(__this, ___sender0, method)
// System.Boolean System.Collections.Generic.LinkedList`1<Zenject.TaskUpdater`1/TaskInfo<Zenject.ITickable>>::Remove(T)
#define LinkedList_1_Remove_m1034696294(__this, ___value0, method) ((  bool (*) (LinkedList_1_t2757043551 *, TaskInfo_t1016914005 *, const MethodInfo*))LinkedList_1_Remove_m3283493303_gshared)(__this, ___value0, method)
// System.Void System.Collections.Generic.LinkedList`1<Zenject.TaskUpdater`1/TaskInfo<Zenject.ITickable>>::Remove(System.Collections.Generic.LinkedListNode`1<T>)
#define LinkedList_1_Remove_m2001670613(__this, ___node0, method) ((  void (*) (LinkedList_1_t2757043551 *, LinkedListNode_1_t718904625 *, const MethodInfo*))LinkedList_1_Remove_m4034790180_gshared)(__this, ___node0, method)
// System.Void System.Collections.Generic.LinkedList`1<Zenject.TaskUpdater`1/TaskInfo<Zenject.ITickable>>::RemoveFirst()
#define LinkedList_1_RemoveFirst_m635507920(__this, method) ((  void (*) (LinkedList_1_t2757043551 *, const MethodInfo*))LinkedList_1_RemoveFirst_m1652892321_gshared)(__this, method)
// System.Void System.Collections.Generic.LinkedList`1<Zenject.TaskUpdater`1/TaskInfo<Zenject.ITickable>>::RemoveLast()
#define LinkedList_1_RemoveLast_m3232956696(__this, method) ((  void (*) (LinkedList_1_t2757043551 *, const MethodInfo*))LinkedList_1_RemoveLast_m2573038887_gshared)(__this, method)
// System.Int32 System.Collections.Generic.LinkedList`1<Zenject.TaskUpdater`1/TaskInfo<Zenject.ITickable>>::get_Count()
#define LinkedList_1_get_Count_m3572247354(__this, method) ((  int32_t (*) (LinkedList_1_t2757043551 *, const MethodInfo*))LinkedList_1_get_Count_m1368924491_gshared)(__this, method)
// System.Collections.Generic.LinkedListNode`1<T> System.Collections.Generic.LinkedList`1<Zenject.TaskUpdater`1/TaskInfo<Zenject.ITickable>>::get_First()
#define LinkedList_1_get_First_m3693394553(__this, method) ((  LinkedListNode_1_t718904625 * (*) (LinkedList_1_t2757043551 *, const MethodInfo*))LinkedList_1_get_First_m3278587786_gshared)(__this, method)
// System.Collections.Generic.LinkedListNode`1<T> System.Collections.Generic.LinkedList`1<Zenject.TaskUpdater`1/TaskInfo<Zenject.ITickable>>::get_Last()
#define LinkedList_1_get_Last_m1946124879(__this, method) ((  LinkedListNode_1_t718904625 * (*) (LinkedList_1_t2757043551 *, const MethodInfo*))LinkedList_1_get_Last_m270176030_gshared)(__this, method)
