﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// System.Collections.Generic.List`1<System.String>
struct List_1_t1765447871;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PlayFab.ClientModels.ItemPurchaseRequest
struct  ItemPurchaseRequest_t731462315  : public Il2CppObject
{
public:
	// System.String PlayFab.ClientModels.ItemPurchaseRequest::<ItemId>k__BackingField
	String_t* ___U3CItemIdU3Ek__BackingField_0;
	// System.UInt32 PlayFab.ClientModels.ItemPurchaseRequest::<Quantity>k__BackingField
	uint32_t ___U3CQuantityU3Ek__BackingField_1;
	// System.String PlayFab.ClientModels.ItemPurchaseRequest::<Annotation>k__BackingField
	String_t* ___U3CAnnotationU3Ek__BackingField_2;
	// System.Collections.Generic.List`1<System.String> PlayFab.ClientModels.ItemPurchaseRequest::<UpgradeFromItems>k__BackingField
	List_1_t1765447871 * ___U3CUpgradeFromItemsU3Ek__BackingField_3;

public:
	inline static int32_t get_offset_of_U3CItemIdU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(ItemPurchaseRequest_t731462315, ___U3CItemIdU3Ek__BackingField_0)); }
	inline String_t* get_U3CItemIdU3Ek__BackingField_0() const { return ___U3CItemIdU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CItemIdU3Ek__BackingField_0() { return &___U3CItemIdU3Ek__BackingField_0; }
	inline void set_U3CItemIdU3Ek__BackingField_0(String_t* value)
	{
		___U3CItemIdU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier(&___U3CItemIdU3Ek__BackingField_0, value);
	}

	inline static int32_t get_offset_of_U3CQuantityU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(ItemPurchaseRequest_t731462315, ___U3CQuantityU3Ek__BackingField_1)); }
	inline uint32_t get_U3CQuantityU3Ek__BackingField_1() const { return ___U3CQuantityU3Ek__BackingField_1; }
	inline uint32_t* get_address_of_U3CQuantityU3Ek__BackingField_1() { return &___U3CQuantityU3Ek__BackingField_1; }
	inline void set_U3CQuantityU3Ek__BackingField_1(uint32_t value)
	{
		___U3CQuantityU3Ek__BackingField_1 = value;
	}

	inline static int32_t get_offset_of_U3CAnnotationU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(ItemPurchaseRequest_t731462315, ___U3CAnnotationU3Ek__BackingField_2)); }
	inline String_t* get_U3CAnnotationU3Ek__BackingField_2() const { return ___U3CAnnotationU3Ek__BackingField_2; }
	inline String_t** get_address_of_U3CAnnotationU3Ek__BackingField_2() { return &___U3CAnnotationU3Ek__BackingField_2; }
	inline void set_U3CAnnotationU3Ek__BackingField_2(String_t* value)
	{
		___U3CAnnotationU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier(&___U3CAnnotationU3Ek__BackingField_2, value);
	}

	inline static int32_t get_offset_of_U3CUpgradeFromItemsU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(ItemPurchaseRequest_t731462315, ___U3CUpgradeFromItemsU3Ek__BackingField_3)); }
	inline List_1_t1765447871 * get_U3CUpgradeFromItemsU3Ek__BackingField_3() const { return ___U3CUpgradeFromItemsU3Ek__BackingField_3; }
	inline List_1_t1765447871 ** get_address_of_U3CUpgradeFromItemsU3Ek__BackingField_3() { return &___U3CUpgradeFromItemsU3Ek__BackingField_3; }
	inline void set_U3CUpgradeFromItemsU3Ek__BackingField_3(List_1_t1765447871 * value)
	{
		___U3CUpgradeFromItemsU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier(&___U3CUpgradeFromItemsU3Ek__BackingField_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
