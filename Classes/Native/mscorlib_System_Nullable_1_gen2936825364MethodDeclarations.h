﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Object
struct Il2CppObject;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Nullable_1_gen2936825364.h"
#include "AssemblyU2DCSharp_SponsorPay_SPUserConnection50787456.h"
#include "mscorlib_System_Object837106420.h"

// System.Void System.Nullable`1<SponsorPay.SPUserConnection>::.ctor(T)
extern "C"  void Nullable_1__ctor_m2083074639_gshared (Nullable_1_t2936825364 * __this, int32_t ___value0, const MethodInfo* method);
#define Nullable_1__ctor_m2083074639(__this, ___value0, method) ((  void (*) (Nullable_1_t2936825364 *, int32_t, const MethodInfo*))Nullable_1__ctor_m2083074639_gshared)(__this, ___value0, method)
// System.Boolean System.Nullable`1<SponsorPay.SPUserConnection>::get_HasValue()
extern "C"  bool Nullable_1_get_HasValue_m1427609527_gshared (Nullable_1_t2936825364 * __this, const MethodInfo* method);
#define Nullable_1_get_HasValue_m1427609527(__this, method) ((  bool (*) (Nullable_1_t2936825364 *, const MethodInfo*))Nullable_1_get_HasValue_m1427609527_gshared)(__this, method)
// T System.Nullable`1<SponsorPay.SPUserConnection>::get_Value()
extern "C"  int32_t Nullable_1_get_Value_m4092822326_gshared (Nullable_1_t2936825364 * __this, const MethodInfo* method);
#define Nullable_1_get_Value_m4092822326(__this, method) ((  int32_t (*) (Nullable_1_t2936825364 *, const MethodInfo*))Nullable_1_get_Value_m4092822326_gshared)(__this, method)
// System.Boolean System.Nullable`1<SponsorPay.SPUserConnection>::Equals(System.Object)
extern "C"  bool Nullable_1_Equals_m3811712734_gshared (Nullable_1_t2936825364 * __this, Il2CppObject * ___other0, const MethodInfo* method);
#define Nullable_1_Equals_m3811712734(__this, ___other0, method) ((  bool (*) (Nullable_1_t2936825364 *, Il2CppObject *, const MethodInfo*))Nullable_1_Equals_m3811712734_gshared)(__this, ___other0, method)
// System.Boolean System.Nullable`1<SponsorPay.SPUserConnection>::Equals(System.Nullable`1<T>)
extern "C"  bool Nullable_1_Equals_m1284939425_gshared (Nullable_1_t2936825364 * __this, Nullable_1_t2936825364  ___other0, const MethodInfo* method);
#define Nullable_1_Equals_m1284939425(__this, ___other0, method) ((  bool (*) (Nullable_1_t2936825364 *, Nullable_1_t2936825364 , const MethodInfo*))Nullable_1_Equals_m1284939425_gshared)(__this, ___other0, method)
// System.Int32 System.Nullable`1<SponsorPay.SPUserConnection>::GetHashCode()
extern "C"  int32_t Nullable_1_GetHashCode_m3645063106_gshared (Nullable_1_t2936825364 * __this, const MethodInfo* method);
#define Nullable_1_GetHashCode_m3645063106(__this, method) ((  int32_t (*) (Nullable_1_t2936825364 *, const MethodInfo*))Nullable_1_GetHashCode_m3645063106_gshared)(__this, method)
// T System.Nullable`1<SponsorPay.SPUserConnection>::GetValueOrDefault()
extern "C"  int32_t Nullable_1_GetValueOrDefault_m2713716113_gshared (Nullable_1_t2936825364 * __this, const MethodInfo* method);
#define Nullable_1_GetValueOrDefault_m2713716113(__this, method) ((  int32_t (*) (Nullable_1_t2936825364 *, const MethodInfo*))Nullable_1_GetValueOrDefault_m2713716113_gshared)(__this, method)
// System.String System.Nullable`1<SponsorPay.SPUserConnection>::ToString()
extern "C"  String_t* Nullable_1_ToString_m567589380_gshared (Nullable_1_t2936825364 * __this, const MethodInfo* method);
#define Nullable_1_ToString_m567589380(__this, method) ((  String_t* (*) (Nullable_1_t2936825364 *, const MethodInfo*))Nullable_1_ToString_m567589380_gshared)(__this, method)
// System.Nullable`1<T> System.Nullable`1<SponsorPay.SPUserConnection>::op_Implicit(T)
extern "C"  Nullable_1_t2936825364  Nullable_1_op_Implicit_m2740305604_gshared (Il2CppObject * __this /* static, unused */, int32_t ___value0, const MethodInfo* method);
#define Nullable_1_op_Implicit_m2740305604(__this /* static, unused */, ___value0, method) ((  Nullable_1_t2936825364  (*) (Il2CppObject * /* static, unused */, int32_t, const MethodInfo*))Nullable_1_op_Implicit_m2740305604_gshared)(__this /* static, unused */, ___value0, method)
