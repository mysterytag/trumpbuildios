﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// FBEngine
struct FBEngine_t2263511582;
// Facebook.Unity.ILoginResult
struct ILoginResult_t1998157500;
// Facebook.Unity.IShareResult
struct IShareResult_t747644594;
// Facebook.Unity.IGroupCreateResult
struct IGroupCreateResult_t1652284654;
// Facebook.Unity.IGroupJoinResult
struct IGroupJoinResult_t1264024604;
// Facebook.Unity.IAppRequestResult
struct IAppRequestResult_t2121186099;

#include "codegen/il2cpp-codegen.h"

// System.Void FBEngine::.ctor()
extern "C"  void FBEngine__ctor_m4258385469 (FBEngine_t2263511582 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FBEngine::Start()
extern "C"  void FBEngine_Start_m3205523261 (FBEngine_t2263511582 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FBEngine::Awake()
extern "C"  void FBEngine_Awake_m201023392 (FBEngine_t2263511582 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FBEngine::InitCallback()
extern "C"  void FBEngine_InitCallback_m4114492828 (FBEngine_t2263511582 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FBEngine::OnHideUnity(System.Boolean)
extern "C"  void FBEngine_OnHideUnity_m3160128070 (FBEngine_t2263511582 * __this, bool ___isGameShown0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FBEngine::AuthCallback(Facebook.Unity.ILoginResult)
extern "C"  void FBEngine_AuthCallback_m1360912928 (FBEngine_t2263511582 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FBEngine::Login()
extern "C"  void FBEngine_Login_m1150086276 (FBEngine_t2263511582 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FBEngine::ShareCallBack(Facebook.Unity.IShareResult)
extern "C"  void FBEngine_ShareCallBack_m1687548031 (FBEngine_t2263511582 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FBEngine::Shared_FB()
extern "C"  void FBEngine_Shared_FB_m1114522545 (FBEngine_t2263511582 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FBEngine::LogOutFB()
extern "C"  void FBEngine_LogOutFB_m4232356493 (FBEngine_t2263511582 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FBEngine::GroupJoinCallBack(Facebook.Unity.IGroupCreateResult)
extern "C"  void FBEngine_GroupJoinCallBack_m3442550105 (FBEngine_t2263511582 * __this, Il2CppObject * ___res0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FBEngine::JoinGroupFB()
extern "C"  void FBEngine_JoinGroupFB_m4171588236 (FBEngine_t2263511582 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FBEngine::InviteFrendsFB()
extern "C"  void FBEngine_InviteFrendsFB_m3285881168 (FBEngine_t2263511582 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FBEngine::Update()
extern "C"  void FBEngine_Update_m592825456 (FBEngine_t2263511582 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FBEngine::<JoinGroupFB>m__7(Facebook.Unity.IGroupJoinResult)
extern "C"  void FBEngine_U3CJoinGroupFBU3Em__7_m2144670182 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FBEngine::<InviteFrendsFB>m__8(Facebook.Unity.IAppRequestResult)
extern "C"  void FBEngine_U3CInviteFrendsFBU3Em__8_m4159973080 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
