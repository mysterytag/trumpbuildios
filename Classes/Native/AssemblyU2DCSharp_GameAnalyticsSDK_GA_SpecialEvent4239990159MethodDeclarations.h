﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GameAnalyticsSDK.GA_SpecialEvents
struct GA_SpecialEvents_t4239990159;
// System.Collections.IEnumerator
struct IEnumerator_t287207039;

#include "codegen/il2cpp-codegen.h"

// System.Void GameAnalyticsSDK.GA_SpecialEvents::.ctor()
extern "C"  void GA_SpecialEvents__ctor_m531804086 (GA_SpecialEvents_t4239990159 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameAnalyticsSDK.GA_SpecialEvents::.cctor()
extern "C"  void GA_SpecialEvents__cctor_m3118928567 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameAnalyticsSDK.GA_SpecialEvents::Start()
extern "C"  void GA_SpecialEvents_Start_m3773909174 (GA_SpecialEvents_t4239990159 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator GameAnalyticsSDK.GA_SpecialEvents::SubmitFPSRoutine()
extern "C"  Il2CppObject * GA_SpecialEvents_SubmitFPSRoutine_m640401177 (GA_SpecialEvents_t4239990159 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator GameAnalyticsSDK.GA_SpecialEvents::CheckCriticalFPSRoutine()
extern "C"  Il2CppObject * GA_SpecialEvents_CheckCriticalFPSRoutine_m116191134 (GA_SpecialEvents_t4239990159 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameAnalyticsSDK.GA_SpecialEvents::Update()
extern "C"  void GA_SpecialEvents_Update_m1032919575 (GA_SpecialEvents_t4239990159 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameAnalyticsSDK.GA_SpecialEvents::SubmitFPS()
extern "C"  void GA_SpecialEvents_SubmitFPS_m3217294533 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameAnalyticsSDK.GA_SpecialEvents::CheckCriticalFPS()
extern "C"  void GA_SpecialEvents_CheckCriticalFPS_m1577390448 (GA_SpecialEvents_t4239990159 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
