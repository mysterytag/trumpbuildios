﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.WhereObservable`1/Where_<System.Boolean>
struct Where__t272287194;
// UniRx.Operators.WhereObservable`1<System.Boolean>
struct WhereObservable_1_t1409565290;
// UniRx.IObserver`1<System.Boolean>
struct IObserver_1_t2423004244;
// System.IDisposable
struct IDisposable_t1628921374;
// System.Exception
struct Exception_t1967233988;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Exception1967233988.h"

// System.Void UniRx.Operators.WhereObservable`1/Where_<System.Boolean>::.ctor(UniRx.Operators.WhereObservable`1<T>,UniRx.IObserver`1<T>,System.IDisposable)
extern "C"  void Where___ctor_m774817845_gshared (Where__t272287194 * __this, WhereObservable_1_t1409565290 * ___parent0, Il2CppObject* ___observer1, Il2CppObject * ___cancel2, const MethodInfo* method);
#define Where___ctor_m774817845(__this, ___parent0, ___observer1, ___cancel2, method) ((  void (*) (Where__t272287194 *, WhereObservable_1_t1409565290 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))Where___ctor_m774817845_gshared)(__this, ___parent0, ___observer1, ___cancel2, method)
// System.Void UniRx.Operators.WhereObservable`1/Where_<System.Boolean>::OnNext(T)
extern "C"  void Where__OnNext_m2177157531_gshared (Where__t272287194 * __this, bool ___value0, const MethodInfo* method);
#define Where__OnNext_m2177157531(__this, ___value0, method) ((  void (*) (Where__t272287194 *, bool, const MethodInfo*))Where__OnNext_m2177157531_gshared)(__this, ___value0, method)
// System.Void UniRx.Operators.WhereObservable`1/Where_<System.Boolean>::OnError(System.Exception)
extern "C"  void Where__OnError_m2603634346_gshared (Where__t272287194 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method);
#define Where__OnError_m2603634346(__this, ___error0, method) ((  void (*) (Where__t272287194 *, Exception_t1967233988 *, const MethodInfo*))Where__OnError_m2603634346_gshared)(__this, ___error0, method)
// System.Void UniRx.Operators.WhereObservable`1/Where_<System.Boolean>::OnCompleted()
extern "C"  void Where__OnCompleted_m1842360573_gshared (Where__t272287194 * __this, const MethodInfo* method);
#define Where__OnCompleted_m1842360573(__this, method) ((  void (*) (Where__t272287194 *, const MethodInfo*))Where__OnCompleted_m1842360573_gshared)(__this, method)
