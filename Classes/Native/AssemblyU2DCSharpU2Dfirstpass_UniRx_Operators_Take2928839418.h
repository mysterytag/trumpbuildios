﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UniRx.IObservable`1<UniRx.Unit>
struct IObservable_1_t2317084402;

#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Oper1622431009.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Operators.TakeUntilObservable`2<UniRx.Unit,UniRx.Unit>
struct  TakeUntilObservable_2_t2928839418  : public OperatorObservableBase_1_t1622431009
{
public:
	// UniRx.IObservable`1<T> UniRx.Operators.TakeUntilObservable`2::source
	Il2CppObject* ___source_1;
	// UniRx.IObservable`1<TOther> UniRx.Operators.TakeUntilObservable`2::other
	Il2CppObject* ___other_2;

public:
	inline static int32_t get_offset_of_source_1() { return static_cast<int32_t>(offsetof(TakeUntilObservable_2_t2928839418, ___source_1)); }
	inline Il2CppObject* get_source_1() const { return ___source_1; }
	inline Il2CppObject** get_address_of_source_1() { return &___source_1; }
	inline void set_source_1(Il2CppObject* value)
	{
		___source_1 = value;
		Il2CppCodeGenWriteBarrier(&___source_1, value);
	}

	inline static int32_t get_offset_of_other_2() { return static_cast<int32_t>(offsetof(TakeUntilObservable_2_t2928839418, ___other_2)); }
	inline Il2CppObject* get_other_2() const { return ___other_2; }
	inline Il2CppObject** get_address_of_other_2() { return &___other_2; }
	inline void set_other_2(Il2CppObject* value)
	{
		___other_2 = value;
		Il2CppCodeGenWriteBarrier(&___other_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
