﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.InternalUtil.ListObserver`1<System.Boolean>
struct ListObserver_1_t505623012;
// UniRx.InternalUtil.ImmutableList`1<UniRx.IObserver`1<System.Boolean>>
struct ImmutableList_1_t3483208743;
// System.Exception
struct Exception_t1967233988;
// UniRx.IObserver`1<System.Boolean>
struct IObserver_1_t2423004244;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Exception1967233988.h"

// System.Void UniRx.InternalUtil.ListObserver`1<System.Boolean>::.ctor(UniRx.InternalUtil.ImmutableList`1<UniRx.IObserver`1<T>>)
extern "C"  void ListObserver_1__ctor_m1222637140_gshared (ListObserver_1_t505623012 * __this, ImmutableList_1_t3483208743 * ___observers0, const MethodInfo* method);
#define ListObserver_1__ctor_m1222637140(__this, ___observers0, method) ((  void (*) (ListObserver_1_t505623012 *, ImmutableList_1_t3483208743 *, const MethodInfo*))ListObserver_1__ctor_m1222637140_gshared)(__this, ___observers0, method)
// System.Void UniRx.InternalUtil.ListObserver`1<System.Boolean>::OnCompleted()
extern "C"  void ListObserver_1_OnCompleted_m928423308_gshared (ListObserver_1_t505623012 * __this, const MethodInfo* method);
#define ListObserver_1_OnCompleted_m928423308(__this, method) ((  void (*) (ListObserver_1_t505623012 *, const MethodInfo*))ListObserver_1_OnCompleted_m928423308_gshared)(__this, method)
// System.Void UniRx.InternalUtil.ListObserver`1<System.Boolean>::OnError(System.Exception)
extern "C"  void ListObserver_1_OnError_m3450166457_gshared (ListObserver_1_t505623012 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method);
#define ListObserver_1_OnError_m3450166457(__this, ___error0, method) ((  void (*) (ListObserver_1_t505623012 *, Exception_t1967233988 *, const MethodInfo*))ListObserver_1_OnError_m3450166457_gshared)(__this, ___error0, method)
// System.Void UniRx.InternalUtil.ListObserver`1<System.Boolean>::OnNext(T)
extern "C"  void ListObserver_1_OnNext_m1440601514_gshared (ListObserver_1_t505623012 * __this, bool ___value0, const MethodInfo* method);
#define ListObserver_1_OnNext_m1440601514(__this, ___value0, method) ((  void (*) (ListObserver_1_t505623012 *, bool, const MethodInfo*))ListObserver_1_OnNext_m1440601514_gshared)(__this, ___value0, method)
// UniRx.IObserver`1<T> UniRx.InternalUtil.ListObserver`1<System.Boolean>::Add(UniRx.IObserver`1<T>)
extern "C"  Il2CppObject* ListObserver_1_Add_m3730409674_gshared (ListObserver_1_t505623012 * __this, Il2CppObject* ___observer0, const MethodInfo* method);
#define ListObserver_1_Add_m3730409674(__this, ___observer0, method) ((  Il2CppObject* (*) (ListObserver_1_t505623012 *, Il2CppObject*, const MethodInfo*))ListObserver_1_Add_m3730409674_gshared)(__this, ___observer0, method)
// UniRx.IObserver`1<T> UniRx.InternalUtil.ListObserver`1<System.Boolean>::Remove(UniRx.IObserver`1<T>)
extern "C"  Il2CppObject* ListObserver_1_Remove_m789259841_gshared (ListObserver_1_t505623012 * __this, Il2CppObject* ___observer0, const MethodInfo* method);
#define ListObserver_1_Remove_m789259841(__this, ___observer0, method) ((  Il2CppObject* (*) (ListObserver_1_t505623012 *, Il2CppObject*, const MethodInfo*))ListObserver_1_Remove_m789259841_gshared)(__this, ___observer0, method)
