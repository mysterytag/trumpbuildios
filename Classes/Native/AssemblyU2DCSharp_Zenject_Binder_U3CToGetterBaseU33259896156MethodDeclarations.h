﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Zenject.Binder/<ToGetterBase>c__AnonStorey16F`2<System.Object,System.Object>
struct U3CToGetterBaseU3Ec__AnonStorey16F_2_t3259896156;
// System.Object
struct Il2CppObject;
// Zenject.InjectContext
struct InjectContext_t3456483891;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Zenject_InjectContext3456483891.h"

// System.Void Zenject.Binder/<ToGetterBase>c__AnonStorey16F`2<System.Object,System.Object>::.ctor()
extern "C"  void U3CToGetterBaseU3Ec__AnonStorey16F_2__ctor_m3687156782_gshared (U3CToGetterBaseU3Ec__AnonStorey16F_2_t3259896156 * __this, const MethodInfo* method);
#define U3CToGetterBaseU3Ec__AnonStorey16F_2__ctor_m3687156782(__this, method) ((  void (*) (U3CToGetterBaseU3Ec__AnonStorey16F_2_t3259896156 *, const MethodInfo*))U3CToGetterBaseU3Ec__AnonStorey16F_2__ctor_m3687156782_gshared)(__this, method)
// TResult Zenject.Binder/<ToGetterBase>c__AnonStorey16F`2<System.Object,System.Object>::<>m__28D(Zenject.InjectContext)
extern "C"  Il2CppObject * U3CToGetterBaseU3Ec__AnonStorey16F_2_U3CU3Em__28D_m1322718792_gshared (U3CToGetterBaseU3Ec__AnonStorey16F_2_t3259896156 * __this, InjectContext_t3456483891 * ___ctx0, const MethodInfo* method);
#define U3CToGetterBaseU3Ec__AnonStorey16F_2_U3CU3Em__28D_m1322718792(__this, ___ctx0, method) ((  Il2CppObject * (*) (U3CToGetterBaseU3Ec__AnonStorey16F_2_t3259896156 *, InjectContext_t3456483891 *, const MethodInfo*))U3CToGetterBaseU3Ec__AnonStorey16F_2_U3CU3Em__28D_m1322718792_gshared)(__this, ___ctx0, method)
