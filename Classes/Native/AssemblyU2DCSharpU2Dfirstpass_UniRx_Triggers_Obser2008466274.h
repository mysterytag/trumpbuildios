﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UniRx.Subject`1<UniRx.Unit>
struct Subject_1_t201353362;

#include "UnityEngine_UnityEngine_MonoBehaviour3012272455.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Triggers.ObservableDestroyTrigger
struct  ObservableDestroyTrigger_t2008466274  : public MonoBehaviour_t3012272455
{
public:
	// System.Boolean UniRx.Triggers.ObservableDestroyTrigger::calledDestroy
	bool ___calledDestroy_2;
	// UniRx.Subject`1<UniRx.Unit> UniRx.Triggers.ObservableDestroyTrigger::onDestroy
	Subject_1_t201353362 * ___onDestroy_3;

public:
	inline static int32_t get_offset_of_calledDestroy_2() { return static_cast<int32_t>(offsetof(ObservableDestroyTrigger_t2008466274, ___calledDestroy_2)); }
	inline bool get_calledDestroy_2() const { return ___calledDestroy_2; }
	inline bool* get_address_of_calledDestroy_2() { return &___calledDestroy_2; }
	inline void set_calledDestroy_2(bool value)
	{
		___calledDestroy_2 = value;
	}

	inline static int32_t get_offset_of_onDestroy_3() { return static_cast<int32_t>(offsetof(ObservableDestroyTrigger_t2008466274, ___onDestroy_3)); }
	inline Subject_1_t201353362 * get_onDestroy_3() const { return ___onDestroy_3; }
	inline Subject_1_t201353362 ** get_address_of_onDestroy_3() { return &___onDestroy_3; }
	inline void set_onDestroy_3(Subject_1_t201353362 * value)
	{
		___onDestroy_3 = value;
		Il2CppCodeGenWriteBarrier(&___onDestroy_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
