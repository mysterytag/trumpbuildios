﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Func`2<System.Int32,System.Boolean>
struct Func_2_t3308141622;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Linq.Enumerable/PredicateOf`1<System.Int32>
struct  PredicateOf_1_t2617073743  : public Il2CppObject
{
public:

public:
};

struct PredicateOf_1_t2617073743_StaticFields
{
public:
	// System.Func`2<T,System.Boolean> System.Linq.Enumerable/PredicateOf`1::Always
	Func_2_t3308141622 * ___Always_0;
	// System.Func`2<T,System.Boolean> System.Linq.Enumerable/PredicateOf`1::<>f__am$cache1
	Func_2_t3308141622 * ___U3CU3Ef__amU24cache1_1;

public:
	inline static int32_t get_offset_of_Always_0() { return static_cast<int32_t>(offsetof(PredicateOf_1_t2617073743_StaticFields, ___Always_0)); }
	inline Func_2_t3308141622 * get_Always_0() const { return ___Always_0; }
	inline Func_2_t3308141622 ** get_address_of_Always_0() { return &___Always_0; }
	inline void set_Always_0(Func_2_t3308141622 * value)
	{
		___Always_0 = value;
		Il2CppCodeGenWriteBarrier(&___Always_0, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache1_1() { return static_cast<int32_t>(offsetof(PredicateOf_1_t2617073743_StaticFields, ___U3CU3Ef__amU24cache1_1)); }
	inline Func_2_t3308141622 * get_U3CU3Ef__amU24cache1_1() const { return ___U3CU3Ef__amU24cache1_1; }
	inline Func_2_t3308141622 ** get_address_of_U3CU3Ef__amU24cache1_1() { return &___U3CU3Ef__amU24cache1_1; }
	inline void set_U3CU3Ef__amU24cache1_1(Func_2_t3308141622 * value)
	{
		___U3CU3Ef__amU24cache1_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache1_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
