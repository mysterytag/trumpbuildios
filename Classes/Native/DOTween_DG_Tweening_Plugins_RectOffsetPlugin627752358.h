﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.RectOffset
struct RectOffset_t3394170884;

#include "DOTween_DG_Tweening_Plugins_Core_ABSTweenPlugin_3_g466620280.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.Plugins.RectOffsetPlugin
struct  RectOffsetPlugin_t627752358  : public ABSTweenPlugin_3_t466620280
{
public:

public:
};

struct RectOffsetPlugin_t627752358_StaticFields
{
public:
	// UnityEngine.RectOffset DG.Tweening.Plugins.RectOffsetPlugin::_r
	RectOffset_t3394170884 * ____r_0;

public:
	inline static int32_t get_offset_of__r_0() { return static_cast<int32_t>(offsetof(RectOffsetPlugin_t627752358_StaticFields, ____r_0)); }
	inline RectOffset_t3394170884 * get__r_0() const { return ____r_0; }
	inline RectOffset_t3394170884 ** get_address_of__r_0() { return &____r_0; }
	inline void set__r_0(RectOffset_t3394170884 * value)
	{
		____r_0 = value;
		Il2CppCodeGenWriteBarrier(&____r_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
