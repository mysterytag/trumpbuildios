﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SponsorPay.UnsupportedPlatformSPUser
struct UnsupportedPlatformSPUser_t3389097770;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String968488902.h"

// System.Void SponsorPay.UnsupportedPlatformSPUser::.ctor()
extern "C"  void UnsupportedPlatformSPUser__ctor_m749326283 (UnsupportedPlatformSPUser_t3389097770 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SponsorPay.UnsupportedPlatformSPUser::NativePut(System.String)
extern "C"  void UnsupportedPlatformSPUser_NativePut_m3279544897 (UnsupportedPlatformSPUser_t3389097770 * __this, String_t* ___json0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SponsorPay.UnsupportedPlatformSPUser::NativeReset()
extern "C"  void UnsupportedPlatformSPUser_NativeReset_m2870934625 (UnsupportedPlatformSPUser_t3389097770 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String SponsorPay.UnsupportedPlatformSPUser::GetJsonMessage(System.String)
extern "C"  String_t* UnsupportedPlatformSPUser_GetJsonMessage_m2814932125 (UnsupportedPlatformSPUser_t3389097770 * __this, String_t* ___key0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
