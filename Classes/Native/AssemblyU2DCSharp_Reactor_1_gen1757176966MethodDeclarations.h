﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Reactor_1_gen96911316MethodDeclarations.h"

// System.Void Reactor`1<CollectionReplaceEvent`1<System.Object>>::.ctor()
#define Reactor_1__ctor_m2527109755(__this, method) ((  void (*) (Reactor_1_t1757176966 *, const MethodInfo*))Reactor_1__ctor_m1161914514_gshared)(__this, method)
// System.Void Reactor`1<CollectionReplaceEvent`1<System.Object>>::React(T)
#define Reactor_1_React_m567685894(__this, ___t0, method) ((  void (*) (Reactor_1_t1757176966 *, CollectionReplaceEvent_1_t2497372070 *, const MethodInfo*))Reactor_1_React_m1196306383_gshared)(__this, ___t0, method)
// System.Void Reactor`1<CollectionReplaceEvent`1<System.Object>>::TransactionUnpackReact(T,System.Int32)
#define Reactor_1_TransactionUnpackReact_m817272465(__this, ___t0, ___i1, method) ((  void (*) (Reactor_1_t1757176966 *, CollectionReplaceEvent_1_t2497372070 *, int32_t, const MethodInfo*))Reactor_1_TransactionUnpackReact_m1003670938_gshared)(__this, ___t0, ___i1, method)
// System.Boolean Reactor`1<CollectionReplaceEvent`1<System.Object>>::Empty()
#define Reactor_1_Empty_m1536605810(__this, method) ((  bool (*) (Reactor_1_t1757176966 *, const MethodInfo*))Reactor_1_Empty_m132443017_gshared)(__this, method)
// System.IDisposable Reactor`1<CollectionReplaceEvent`1<System.Object>>::AddReaction(System.Action`1<T>,Priority)
#define Reactor_1_AddReaction_m1215523698(__this, ___reaction0, ___priority1, method) ((  Il2CppObject * (*) (Reactor_1_t1757176966 *, Action_1_t2645824775 *, int32_t, const MethodInfo*))Reactor_1_AddReaction_m1495770619_gshared)(__this, ___reaction0, ___priority1, method)
// System.Void Reactor`1<CollectionReplaceEvent`1<System.Object>>::UpgradeToMultyPriority()
#define Reactor_1_UpgradeToMultyPriority_m1393210751(__this, method) ((  void (*) (Reactor_1_t1757176966 *, const MethodInfo*))Reactor_1_UpgradeToMultyPriority_m912916552_gshared)(__this, method)
// System.Void Reactor`1<CollectionReplaceEvent`1<System.Object>>::AddToMultipriority(System.Action`1<T>,Priority)
#define Reactor_1_AddToMultipriority_m3074663128(__this, ___reaction0, ___p1, method) ((  void (*) (Reactor_1_t1757176966 *, Action_1_t2645824775 *, int32_t, const MethodInfo*))Reactor_1_AddToMultipriority_m3381282287_gshared)(__this, ___reaction0, ___p1, method)
// System.Void Reactor`1<CollectionReplaceEvent`1<System.Object>>::RemoveReaction(System.Action`1<T>,Priority)
#define Reactor_1_RemoveReaction_m2343299020(__this, ___reaction0, ___p1, method) ((  void (*) (Reactor_1_t1757176966 *, Action_1_t2645824775 *, int32_t, const MethodInfo*))Reactor_1_RemoveReaction_m4227182179_gshared)(__this, ___reaction0, ___p1, method)
// System.Void Reactor`1<CollectionReplaceEvent`1<System.Object>>::Clear()
#define Reactor_1_Clear_m4228210342(__this, method) ((  void (*) (Reactor_1_t1757176966 *, const MethodInfo*))Reactor_1_Clear_m2863015101_gshared)(__this, method)
