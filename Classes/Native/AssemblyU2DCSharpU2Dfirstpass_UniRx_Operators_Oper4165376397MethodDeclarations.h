﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.OperatorObserverBase`2<UniRx.Unit,UniRx.Unit>
struct OperatorObserverBase_2_t4165376397;
// UniRx.IObserver`1<UniRx.Unit>
struct IObserver_1_t475317645;
// System.IDisposable
struct IDisposable_t1628921374;

#include "codegen/il2cpp-codegen.h"

// System.Void UniRx.Operators.OperatorObserverBase`2<UniRx.Unit,UniRx.Unit>::.ctor(UniRx.IObserver`1<TResult>,System.IDisposable)
extern "C"  void OperatorObserverBase_2__ctor_m3770319408_gshared (OperatorObserverBase_2_t4165376397 * __this, Il2CppObject* ___observer0, Il2CppObject * ___cancel1, const MethodInfo* method);
#define OperatorObserverBase_2__ctor_m3770319408(__this, ___observer0, ___cancel1, method) ((  void (*) (OperatorObserverBase_2_t4165376397 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))OperatorObserverBase_2__ctor_m3770319408_gshared)(__this, ___observer0, ___cancel1, method)
// System.Void UniRx.Operators.OperatorObserverBase`2<UniRx.Unit,UniRx.Unit>::Dispose()
extern "C"  void OperatorObserverBase_2_Dispose_m1224144314_gshared (OperatorObserverBase_2_t4165376397 * __this, const MethodInfo* method);
#define OperatorObserverBase_2_Dispose_m1224144314(__this, method) ((  void (*) (OperatorObserverBase_2_t4165376397 *, const MethodInfo*))OperatorObserverBase_2_Dispose_m1224144314_gshared)(__this, method)
