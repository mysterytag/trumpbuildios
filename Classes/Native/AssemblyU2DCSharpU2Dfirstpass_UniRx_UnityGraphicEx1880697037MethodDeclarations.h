﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.UnityGraphicExtensions/<DirtyMaterialCallbackAsObservable>c__AnonStoreyA0/<DirtyMaterialCallbackAsObservable>c__AnonStorey9F
struct U3CDirtyMaterialCallbackAsObservableU3Ec__AnonStorey9F_t1880697037;

#include "codegen/il2cpp-codegen.h"

// System.Void UniRx.UnityGraphicExtensions/<DirtyMaterialCallbackAsObservable>c__AnonStoreyA0/<DirtyMaterialCallbackAsObservable>c__AnonStorey9F::.ctor()
extern "C"  void U3CDirtyMaterialCallbackAsObservableU3Ec__AnonStorey9F__ctor_m3100163026 (U3CDirtyMaterialCallbackAsObservableU3Ec__AnonStorey9F_t1880697037 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.UnityGraphicExtensions/<DirtyMaterialCallbackAsObservable>c__AnonStoreyA0/<DirtyMaterialCallbackAsObservable>c__AnonStorey9F::<>m__DD()
extern "C"  void U3CDirtyMaterialCallbackAsObservableU3Ec__AnonStorey9F_U3CU3Em__DD_m857989787 (U3CDirtyMaterialCallbackAsObservableU3Ec__AnonStorey9F_t1880697037 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.UnityGraphicExtensions/<DirtyMaterialCallbackAsObservable>c__AnonStoreyA0/<DirtyMaterialCallbackAsObservable>c__AnonStorey9F::<>m__DE()
extern "C"  void U3CDirtyMaterialCallbackAsObservableU3Ec__AnonStorey9F_U3CU3Em__DE_m857990748 (U3CDirtyMaterialCallbackAsObservableU3Ec__AnonStorey9F_t1880697037 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
