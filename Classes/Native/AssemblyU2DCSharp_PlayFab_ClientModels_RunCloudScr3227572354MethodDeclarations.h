﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.ClientModels.RunCloudScriptResult
struct RunCloudScriptResult_t3227572354;
// System.String
struct String_t;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String968488902.h"
#include "mscorlib_System_Object837106420.h"

// System.Void PlayFab.ClientModels.RunCloudScriptResult::.ctor()
extern "C"  void RunCloudScriptResult__ctor_m139044091 (RunCloudScriptResult_t3227572354 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PlayFab.ClientModels.RunCloudScriptResult::get_ActionId()
extern "C"  String_t* RunCloudScriptResult_get_ActionId_m3102958176 (RunCloudScriptResult_t3227572354 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.RunCloudScriptResult::set_ActionId(System.String)
extern "C"  void RunCloudScriptResult_set_ActionId_m1697261003 (RunCloudScriptResult_t3227572354 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 PlayFab.ClientModels.RunCloudScriptResult::get_Version()
extern "C"  int32_t RunCloudScriptResult_get_Version_m3398206806 (RunCloudScriptResult_t3227572354 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.RunCloudScriptResult::set_Version(System.Int32)
extern "C"  void RunCloudScriptResult_set_Version_m3183119013 (RunCloudScriptResult_t3227572354 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 PlayFab.ClientModels.RunCloudScriptResult::get_Revision()
extern "C"  int32_t RunCloudScriptResult_get_Revision_m1428301887 (RunCloudScriptResult_t3227572354 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.RunCloudScriptResult::set_Revision(System.Int32)
extern "C"  void RunCloudScriptResult_set_Revision_m1596650962 (RunCloudScriptResult_t3227572354 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object PlayFab.ClientModels.RunCloudScriptResult::get_Results()
extern "C"  Il2CppObject * RunCloudScriptResult_get_Results_m706932379 (RunCloudScriptResult_t3227572354 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.RunCloudScriptResult::set_Results(System.Object)
extern "C"  void RunCloudScriptResult_set_Results_m3553400802 (RunCloudScriptResult_t3227572354 * __this, Il2CppObject * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PlayFab.ClientModels.RunCloudScriptResult::get_ResultsEncoded()
extern "C"  String_t* RunCloudScriptResult_get_ResultsEncoded_m2537930855 (RunCloudScriptResult_t3227572354 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.RunCloudScriptResult::set_ResultsEncoded(System.String)
extern "C"  void RunCloudScriptResult_set_ResultsEncoded_m4266838116 (RunCloudScriptResult_t3227572354 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PlayFab.ClientModels.RunCloudScriptResult::get_ActionLog()
extern "C"  String_t* RunCloudScriptResult_get_ActionLog_m1705581761 (RunCloudScriptResult_t3227572354 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.RunCloudScriptResult::set_ActionLog(System.String)
extern "C"  void RunCloudScriptResult_set_ActionLog_m993762264 (RunCloudScriptResult_t3227572354 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Double PlayFab.ClientModels.RunCloudScriptResult::get_ExecutionTime()
extern "C"  double RunCloudScriptResult_get_ExecutionTime_m1792762648 (RunCloudScriptResult_t3227572354 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.RunCloudScriptResult::set_ExecutionTime(System.Double)
extern "C"  void RunCloudScriptResult_set_ExecutionTime_m1963501537 (RunCloudScriptResult_t3227572354 * __this, double ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
