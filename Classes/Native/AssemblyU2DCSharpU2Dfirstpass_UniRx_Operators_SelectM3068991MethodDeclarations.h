﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.SelectManyObservable`3<System.Object,System.Object,System.Object>
struct SelectManyObservable_3_t3068991;
// UniRx.IObservable`1<System.Object>
struct IObservable_1_t595904784;
// System.Func`2<System.Object,UniRx.IObservable`1<System.Object>>
struct Func_2_t1894581716;
// System.Func`3<System.Object,System.Object,System.Object>
struct Func_3_t1892209229;
// System.Func`3<System.Object,System.Int32,UniRx.IObservable`1<System.Object>>
struct Func_3_t3449466942;
// System.Func`5<System.Object,System.Int32,System.Object,System.Int32,System.Object>
struct Func_5_t714816233;
// System.Func`2<System.Object,System.Collections.Generic.IEnumerable`1<System.Object>>
struct Func_2_t712970412;
// System.Func`3<System.Object,System.Int32,System.Collections.Generic.IEnumerable`1<System.Object>>
struct Func_3_t2267855638;
// System.IDisposable
struct IDisposable_t1628921374;
// UniRx.IObserver`1<System.Object>
struct IObserver_1_t3049105323;

#include "codegen/il2cpp-codegen.h"

// System.Void UniRx.Operators.SelectManyObservable`3<System.Object,System.Object,System.Object>::.ctor(UniRx.IObservable`1<TSource>,System.Func`2<TSource,UniRx.IObservable`1<TCollection>>,System.Func`3<TSource,TCollection,TResult>)
extern "C"  void SelectManyObservable_3__ctor_m4116447878_gshared (SelectManyObservable_3_t3068991 * __this, Il2CppObject* ___source0, Func_2_t1894581716 * ___collectionSelector1, Func_3_t1892209229 * ___resultSelector2, const MethodInfo* method);
#define SelectManyObservable_3__ctor_m4116447878(__this, ___source0, ___collectionSelector1, ___resultSelector2, method) ((  void (*) (SelectManyObservable_3_t3068991 *, Il2CppObject*, Func_2_t1894581716 *, Func_3_t1892209229 *, const MethodInfo*))SelectManyObservable_3__ctor_m4116447878_gshared)(__this, ___source0, ___collectionSelector1, ___resultSelector2, method)
// System.Void UniRx.Operators.SelectManyObservable`3<System.Object,System.Object,System.Object>::.ctor(UniRx.IObservable`1<TSource>,System.Func`3<TSource,System.Int32,UniRx.IObservable`1<TCollection>>,System.Func`5<TSource,System.Int32,TCollection,System.Int32,TResult>)
extern "C"  void SelectManyObservable_3__ctor_m2938409262_gshared (SelectManyObservable_3_t3068991 * __this, Il2CppObject* ___source0, Func_3_t3449466942 * ___collectionSelector1, Func_5_t714816233 * ___resultSelector2, const MethodInfo* method);
#define SelectManyObservable_3__ctor_m2938409262(__this, ___source0, ___collectionSelector1, ___resultSelector2, method) ((  void (*) (SelectManyObservable_3_t3068991 *, Il2CppObject*, Func_3_t3449466942 *, Func_5_t714816233 *, const MethodInfo*))SelectManyObservable_3__ctor_m2938409262_gshared)(__this, ___source0, ___collectionSelector1, ___resultSelector2, method)
// System.Void UniRx.Operators.SelectManyObservable`3<System.Object,System.Object,System.Object>::.ctor(UniRx.IObservable`1<TSource>,System.Func`2<TSource,System.Collections.Generic.IEnumerable`1<TCollection>>,System.Func`3<TSource,TCollection,TResult>)
extern "C"  void SelectManyObservable_3__ctor_m2084890308_gshared (SelectManyObservable_3_t3068991 * __this, Il2CppObject* ___source0, Func_2_t712970412 * ___collectionSelector1, Func_3_t1892209229 * ___resultSelector2, const MethodInfo* method);
#define SelectManyObservable_3__ctor_m2084890308(__this, ___source0, ___collectionSelector1, ___resultSelector2, method) ((  void (*) (SelectManyObservable_3_t3068991 *, Il2CppObject*, Func_2_t712970412 *, Func_3_t1892209229 *, const MethodInfo*))SelectManyObservable_3__ctor_m2084890308_gshared)(__this, ___source0, ___collectionSelector1, ___resultSelector2, method)
// System.Void UniRx.Operators.SelectManyObservable`3<System.Object,System.Object,System.Object>::.ctor(UniRx.IObservable`1<TSource>,System.Func`3<TSource,System.Int32,System.Collections.Generic.IEnumerable`1<TCollection>>,System.Func`5<TSource,System.Int32,TCollection,System.Int32,TResult>)
extern "C"  void SelectManyObservable_3__ctor_m1690245768_gshared (SelectManyObservable_3_t3068991 * __this, Il2CppObject* ___source0, Func_3_t2267855638 * ___collectionSelector1, Func_5_t714816233 * ___resultSelector2, const MethodInfo* method);
#define SelectManyObservable_3__ctor_m1690245768(__this, ___source0, ___collectionSelector1, ___resultSelector2, method) ((  void (*) (SelectManyObservable_3_t3068991 *, Il2CppObject*, Func_3_t2267855638 *, Func_5_t714816233 *, const MethodInfo*))SelectManyObservable_3__ctor_m1690245768_gshared)(__this, ___source0, ___collectionSelector1, ___resultSelector2, method)
// System.IDisposable UniRx.Operators.SelectManyObservable`3<System.Object,System.Object,System.Object>::SubscribeCore(UniRx.IObserver`1<TResult>,System.IDisposable)
extern "C"  Il2CppObject * SelectManyObservable_3_SubscribeCore_m2349712612_gshared (SelectManyObservable_3_t3068991 * __this, Il2CppObject* ___observer0, Il2CppObject * ___cancel1, const MethodInfo* method);
#define SelectManyObservable_3_SubscribeCore_m2349712612(__this, ___observer0, ___cancel1, method) ((  Il2CppObject * (*) (SelectManyObservable_3_t3068991 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))SelectManyObservable_3_SubscribeCore_m2349712612_gshared)(__this, ___observer0, ___cancel1, method)
