﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.PlayFabClientAPI/GetPlayerStatisticsResponseCallback
struct GetPlayerStatisticsResponseCallback_t1719159296;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;
// PlayFab.ClientModels.GetPlayerStatisticsRequest
struct GetPlayerStatisticsRequest_t3788214021;
// PlayFab.ClientModels.GetPlayerStatisticsResult
struct GetPlayerStatisticsResult_t3759578887;
// PlayFab.PlayFabError
struct PlayFabError_t750598646;
// System.IAsyncResult
struct IAsyncResult_t537683269;
// System.AsyncCallback
struct AsyncCallback_t1363551830;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_IntPtr676692020.h"
#include "mscorlib_System_String968488902.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_GetPlayerSt3788214021.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_GetPlayerSt3759578887.h"
#include "AssemblyU2DCSharpU2Dfirstpass_PlayFab_PlayFabError750598646.h"
#include "mscorlib_System_AsyncCallback1363551830.h"

// System.Void PlayFab.PlayFabClientAPI/GetPlayerStatisticsResponseCallback::.ctor(System.Object,System.IntPtr)
extern "C"  void GetPlayerStatisticsResponseCallback__ctor_m461565583 (GetPlayerStatisticsResponseCallback_t1719159296 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/GetPlayerStatisticsResponseCallback::Invoke(System.String,System.Int32,PlayFab.ClientModels.GetPlayerStatisticsRequest,PlayFab.ClientModels.GetPlayerStatisticsResult,PlayFab.PlayFabError,System.Object)
extern "C"  void GetPlayerStatisticsResponseCallback_Invoke_m3414939182 (GetPlayerStatisticsResponseCallback_t1719159296 * __this, String_t* ___urlPath0, int32_t ___callId1, GetPlayerStatisticsRequest_t3788214021 * ___request2, GetPlayerStatisticsResult_t3759578887 * ___result3, PlayFabError_t750598646 * ___error4, Il2CppObject * ___customData5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
extern "C" void pinvoke_delegate_wrapper_GetPlayerStatisticsResponseCallback_t1719159296(Il2CppObject* delegate, String_t* ___urlPath0, int32_t ___callId1, GetPlayerStatisticsRequest_t3788214021 * ___request2, GetPlayerStatisticsResult_t3759578887 * ___result3, PlayFabError_t750598646 * ___error4, Il2CppObject * ___customData5);
// System.IAsyncResult PlayFab.PlayFabClientAPI/GetPlayerStatisticsResponseCallback::BeginInvoke(System.String,System.Int32,PlayFab.ClientModels.GetPlayerStatisticsRequest,PlayFab.ClientModels.GetPlayerStatisticsResult,PlayFab.PlayFabError,System.Object,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * GetPlayerStatisticsResponseCallback_BeginInvoke_m2206219711 (GetPlayerStatisticsResponseCallback_t1719159296 * __this, String_t* ___urlPath0, int32_t ___callId1, GetPlayerStatisticsRequest_t3788214021 * ___request2, GetPlayerStatisticsResult_t3759578887 * ___result3, PlayFabError_t750598646 * ___error4, Il2CppObject * ___customData5, AsyncCallback_t1363551830 * ___callback6, Il2CppObject * ___object7, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/GetPlayerStatisticsResponseCallback::EndInvoke(System.IAsyncResult)
extern "C"  void GetPlayerStatisticsResponseCallback_EndInvoke_m4110024735 (GetPlayerStatisticsResponseCallback_t1719159296 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
