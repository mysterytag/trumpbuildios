﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.OperatorObservableBase`1/<Subscribe>c__AnonStorey5B<System.Boolean>
struct U3CSubscribeU3Ec__AnonStorey5B_t4103979366;

#include "codegen/il2cpp-codegen.h"

// System.Void UniRx.Operators.OperatorObservableBase`1/<Subscribe>c__AnonStorey5B<System.Boolean>::.ctor()
extern "C"  void U3CSubscribeU3Ec__AnonStorey5B__ctor_m1315711019_gshared (U3CSubscribeU3Ec__AnonStorey5B_t4103979366 * __this, const MethodInfo* method);
#define U3CSubscribeU3Ec__AnonStorey5B__ctor_m1315711019(__this, method) ((  void (*) (U3CSubscribeU3Ec__AnonStorey5B_t4103979366 *, const MethodInfo*))U3CSubscribeU3Ec__AnonStorey5B__ctor_m1315711019_gshared)(__this, method)
// System.Void UniRx.Operators.OperatorObservableBase`1/<Subscribe>c__AnonStorey5B<System.Boolean>::<>m__73()
extern "C"  void U3CSubscribeU3Ec__AnonStorey5B_U3CU3Em__73_m3986125840_gshared (U3CSubscribeU3Ec__AnonStorey5B_t4103979366 * __this, const MethodInfo* method);
#define U3CSubscribeU3Ec__AnonStorey5B_U3CU3Em__73_m3986125840(__this, method) ((  void (*) (U3CSubscribeU3Ec__AnonStorey5B_t4103979366 *, const MethodInfo*))U3CSubscribeU3Ec__AnonStorey5B_U3CU3Em__73_m3986125840_gshared)(__this, method)
