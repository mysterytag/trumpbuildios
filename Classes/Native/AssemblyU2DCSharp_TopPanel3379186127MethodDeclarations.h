﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// TopPanel
struct TopPanel_t3379186127;

#include "codegen/il2cpp-codegen.h"

// System.Void TopPanel::.ctor()
extern "C"  void TopPanel__ctor_m2935688620 (TopPanel_t3379186127 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TopPanel::PostInject()
extern "C"  void TopPanel_PostInject_m72397545 (TopPanel_t3379186127 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TopPanel::<PostInject>m__278(System.Double)
extern "C"  void TopPanel_U3CPostInjectU3Em__278_m2675371987 (TopPanel_t3379186127 * __this, double ___hard0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
