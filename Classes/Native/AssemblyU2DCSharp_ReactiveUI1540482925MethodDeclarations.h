﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.UI.Text
struct Text_t3286458198;
// System.Object
struct Il2CppObject;
// System.IDisposable
struct IDisposable_t1628921374;
// UnityEngine.UI.Button
struct Button_t990034267;
// ICell`1<System.Boolean>
struct ICell_1_t1762636318;
// UnityEngine.GameObject
struct GameObject_t4012695102;
// UnityEngine.Component
struct Component_t2126946602;
// UnityEngine.UI.Image
struct Image_t3354615620;
// ICell`1<UnityEngine.Sprite>
struct ICell_1_t1262704051;
// UnityEngine.Sprite
struct Sprite_t4006040370;
// ICell`1<System.Single>
struct ICell_1_t2509839998;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UI_UnityEngine_UI_Text3286458198.h"
#include "mscorlib_System_Object837106420.h"
#include "UnityEngine_UI_UnityEngine_UI_Button990034267.h"
#include "UnityEngine_UnityEngine_GameObject4012695102.h"
#include "UnityEngine_UnityEngine_Component2126946602.h"
#include "UnityEngine_UI_UnityEngine_UI_Image3354615620.h"
#include "UnityEngine_UnityEngine_Sprite4006040370.h"

// System.Void ReactiveUI::SetContent(UnityEngine.UI.Text,System.Object)
extern "C"  void ReactiveUI_SetContent_m1148940931 (Il2CppObject * __this /* static, unused */, Text_t3286458198 * ___text0, Il2CppObject * ___val1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IDisposable ReactiveUI::SetEnabled(UnityEngine.UI.Button,ICell`1<System.Boolean>)
extern "C"  Il2CppObject * ReactiveUI_SetEnabled_m1712790476 (Il2CppObject * __this /* static, unused */, Button_t990034267 * ___button0, Il2CppObject* ___enabled1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IDisposable ReactiveUI::SetActive(UnityEngine.GameObject,ICell`1<System.Boolean>)
extern "C"  Il2CppObject * ReactiveUI_SetActive_m2639071920 (Il2CppObject * __this /* static, unused */, GameObject_t4012695102 * ___obj0, Il2CppObject* ___active1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ReactiveUI::SetActiveSafe(UnityEngine.GameObject,System.Boolean)
extern "C"  void ReactiveUI_SetActiveSafe_m3764874052 (Il2CppObject * __this /* static, unused */, GameObject_t4012695102 * ___obj0, bool ___active1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ReactiveUI::SetActiveSafe(UnityEngine.Component,System.Boolean)
extern "C"  void ReactiveUI_SetActiveSafe_m1384171538 (Il2CppObject * __this /* static, unused */, Component_t2126946602 * ___obj0, bool ___active1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IDisposable ReactiveUI::SetSprite(UnityEngine.UI.Image,ICell`1<UnityEngine.Sprite>)
extern "C"  Il2CppObject * ReactiveUI_SetSprite_m3086397458 (Il2CppObject * __this /* static, unused */, Image_t3354615620 * ___obj0, Il2CppObject* ___sprite1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ReactiveUI::SetSprite(UnityEngine.UI.Image,UnityEngine.Sprite)
extern "C"  void ReactiveUI_SetSprite_m493729327 (Il2CppObject * __this /* static, unused */, Image_t3354615620 * ___obj0, Sprite_t4006040370 * ___sprite1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IDisposable ReactiveUI::SetFill(UnityEngine.UI.Image,ICell`1<System.Single>)
extern "C"  Il2CppObject * ReactiveUI_SetFill_m1975529161 (Il2CppObject * __this /* static, unused */, Image_t3354615620 * ___obj0, Il2CppObject* ___fill1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
