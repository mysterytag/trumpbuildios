﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Scheduler/ThreadPoolScheduler/Timer
struct Timer_t80811814;
// System.Action
struct Action_t437523947;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_TimeSpan763862892.h"
#include "System_Core_System_Action437523947.h"
#include "mscorlib_System_Object837106420.h"

// System.Void UniRx.Scheduler/ThreadPoolScheduler/Timer::.ctor(System.TimeSpan,System.Action)
extern "C"  void Timer__ctor_m2133578139 (Timer_t80811814 * __this, TimeSpan_t763862892  ___dueTime0, Action_t437523947 * ___action1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.Scheduler/ThreadPoolScheduler/Timer::.cctor()
extern "C"  void Timer__cctor_m3881906677 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.Scheduler/ThreadPoolScheduler/Timer::Tick(System.Object)
extern "C"  void Timer_Tick_m64471115 (Timer_t80811814 * __this, Il2CppObject * ___state0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.Scheduler/ThreadPoolScheduler/Timer::Unroot()
extern "C"  void Timer_Unroot_m434493191 (Timer_t80811814 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.Scheduler/ThreadPoolScheduler/Timer::Dispose()
extern "C"  void Timer_Dispose_m2038386421 (Timer_t80811814 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
