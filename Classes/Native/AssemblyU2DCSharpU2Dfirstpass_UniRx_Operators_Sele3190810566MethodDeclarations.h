﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.SelectManyObservable`3/SelectManyOuterObserver/SelectMany<System.Object,System.Int64,System.Object>
struct SelectMany_t3190810566;
// UniRx.Operators.SelectManyObservable`3/SelectManyOuterObserver<System.Object,System.Int64,System.Object>
struct SelectManyOuterObserver_t29170705;
// System.Object
struct Il2CppObject;
// System.IDisposable
struct IDisposable_t1628921374;
// System.Exception
struct Exception_t1967233988;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_Exception1967233988.h"

// System.Void UniRx.Operators.SelectManyObservable`3/SelectManyOuterObserver/SelectMany<System.Object,System.Int64,System.Object>::.ctor(UniRx.Operators.SelectManyObservable`3/SelectManyOuterObserver<TSource,TCollection,TResult>,TSource,System.IDisposable)
extern "C"  void SelectMany__ctor_m1729473830_gshared (SelectMany_t3190810566 * __this, SelectManyOuterObserver_t29170705 * ___parent0, Il2CppObject * ___value1, Il2CppObject * ___cancel2, const MethodInfo* method);
#define SelectMany__ctor_m1729473830(__this, ___parent0, ___value1, ___cancel2, method) ((  void (*) (SelectMany_t3190810566 *, SelectManyOuterObserver_t29170705 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))SelectMany__ctor_m1729473830_gshared)(__this, ___parent0, ___value1, ___cancel2, method)
// System.Void UniRx.Operators.SelectManyObservable`3/SelectManyOuterObserver/SelectMany<System.Object,System.Int64,System.Object>::OnNext(TCollection)
extern "C"  void SelectMany_OnNext_m409869810_gshared (SelectMany_t3190810566 * __this, int64_t ___value0, const MethodInfo* method);
#define SelectMany_OnNext_m409869810(__this, ___value0, method) ((  void (*) (SelectMany_t3190810566 *, int64_t, const MethodInfo*))SelectMany_OnNext_m409869810_gshared)(__this, ___value0, method)
// System.Void UniRx.Operators.SelectManyObservable`3/SelectManyOuterObserver/SelectMany<System.Object,System.Int64,System.Object>::OnError(System.Exception)
extern "C"  void SelectMany_OnError_m997787839_gshared (SelectMany_t3190810566 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method);
#define SelectMany_OnError_m997787839(__this, ___error0, method) ((  void (*) (SelectMany_t3190810566 *, Exception_t1967233988 *, const MethodInfo*))SelectMany_OnError_m997787839_gshared)(__this, ___error0, method)
// System.Void UniRx.Operators.SelectManyObservable`3/SelectManyOuterObserver/SelectMany<System.Object,System.Int64,System.Object>::OnCompleted()
extern "C"  void SelectMany_OnCompleted_m678929042_gshared (SelectMany_t3190810566 * __this, const MethodInfo* method);
#define SelectMany_OnCompleted_m678929042(__this, method) ((  void (*) (SelectMany_t3190810566 *, const MethodInfo*))SelectMany_OnCompleted_m678929042_gshared)(__this, method)
