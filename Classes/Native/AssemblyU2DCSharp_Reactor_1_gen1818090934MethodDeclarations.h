﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Reactor`1<UniRx.Unit>
struct Reactor_1_t1818090934;
// System.IDisposable
struct IDisposable_t1628921374;
// System.Action`1<UniRx.Unit>
struct Action_1_t2706738743;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Unit2558286038.h"
#include "AssemblyU2DCSharp_Priority3194150340.h"

// System.Void Reactor`1<UniRx.Unit>::.ctor()
extern "C"  void Reactor_1__ctor_m4213628562_gshared (Reactor_1_t1818090934 * __this, const MethodInfo* method);
#define Reactor_1__ctor_m4213628562(__this, method) ((  void (*) (Reactor_1_t1818090934 *, const MethodInfo*))Reactor_1__ctor_m4213628562_gshared)(__this, method)
// System.Void Reactor`1<UniRx.Unit>::React(T)
extern "C"  void Reactor_1_React_m1310161359_gshared (Reactor_1_t1818090934 * __this, Unit_t2558286038  ___t0, const MethodInfo* method);
#define Reactor_1_React_m1310161359(__this, ___t0, method) ((  void (*) (Reactor_1_t1818090934 *, Unit_t2558286038 , const MethodInfo*))Reactor_1_React_m1310161359_gshared)(__this, ___t0, method)
// System.Void Reactor`1<UniRx.Unit>::TransactionUnpackReact(T,System.Int32)
extern "C"  void Reactor_1_TransactionUnpackReact_m1186895770_gshared (Reactor_1_t1818090934 * __this, Unit_t2558286038  ___t0, int32_t ___i1, const MethodInfo* method);
#define Reactor_1_TransactionUnpackReact_m1186895770(__this, ___t0, ___i1, method) ((  void (*) (Reactor_1_t1818090934 *, Unit_t2558286038 , int32_t, const MethodInfo*))Reactor_1_TransactionUnpackReact_m1186895770_gshared)(__this, ___t0, ___i1, method)
// System.Boolean Reactor`1<UniRx.Unit>::Empty()
extern "C"  bool Reactor_1_Empty_m2860180849_gshared (Reactor_1_t1818090934 * __this, const MethodInfo* method);
#define Reactor_1_Empty_m2860180849(__this, method) ((  bool (*) (Reactor_1_t1818090934 *, const MethodInfo*))Reactor_1_Empty_m2860180849_gshared)(__this, method)
// System.IDisposable Reactor`1<UniRx.Unit>::AddReaction(System.Action`1<T>,Priority)
extern "C"  Il2CppObject * Reactor_1_AddReaction_m4170151349_gshared (Reactor_1_t1818090934 * __this, Action_1_t2706738743 * ___reaction0, int32_t ___priority1, const MethodInfo* method);
#define Reactor_1_AddReaction_m4170151349(__this, ___reaction0, ___priority1, method) ((  Il2CppObject * (*) (Reactor_1_t1818090934 *, Action_1_t2706738743 *, int32_t, const MethodInfo*))Reactor_1_AddReaction_m4170151349_gshared)(__this, ___reaction0, ___priority1, method)
// System.Void Reactor`1<UniRx.Unit>::UpgradeToMultyPriority()
extern "C"  void Reactor_1_UpgradeToMultyPriority_m2774485576_gshared (Reactor_1_t1818090934 * __this, const MethodInfo* method);
#define Reactor_1_UpgradeToMultyPriority_m2774485576(__this, method) ((  void (*) (Reactor_1_t1818090934 *, const MethodInfo*))Reactor_1_UpgradeToMultyPriority_m2774485576_gshared)(__this, method)
// System.Void Reactor`1<UniRx.Unit>::AddToMultipriority(System.Action`1<T>,Priority)
extern "C"  void Reactor_1_AddToMultipriority_m3393305583_gshared (Reactor_1_t1818090934 * __this, Action_1_t2706738743 * ___reaction0, int32_t ___p1, const MethodInfo* method);
#define Reactor_1_AddToMultipriority_m3393305583(__this, ___reaction0, ___p1, method) ((  void (*) (Reactor_1_t1818090934 *, Action_1_t2706738743 *, int32_t, const MethodInfo*))Reactor_1_AddToMultipriority_m3393305583_gshared)(__this, ___reaction0, ___p1, method)
// System.Void Reactor`1<UniRx.Unit>::RemoveReaction(System.Action`1<T>,Priority)
extern "C"  void Reactor_1_RemoveReaction_m3201836131_gshared (Reactor_1_t1818090934 * __this, Action_1_t2706738743 * ___reaction0, int32_t ___p1, const MethodInfo* method);
#define Reactor_1_RemoveReaction_m3201836131(__this, ___reaction0, ___p1, method) ((  void (*) (Reactor_1_t1818090934 *, Action_1_t2706738743 *, int32_t, const MethodInfo*))Reactor_1_RemoveReaction_m3201836131_gshared)(__this, ___reaction0, ___p1, method)
// System.Void Reactor`1<UniRx.Unit>::Clear()
extern "C"  void Reactor_1_Clear_m1619761853_gshared (Reactor_1_t1818090934 * __this, const MethodInfo* method);
#define Reactor_1_Clear_m1619761853(__this, method) ((  void (*) (Reactor_1_t1818090934 *, const MethodInfo*))Reactor_1_Clear_m1619761853_gshared)(__this, method)
