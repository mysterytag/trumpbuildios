﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.ReactiveProperty`1/ReactivePropertyObserver<System.Boolean>
struct ReactivePropertyObserver_t446987910;
// UniRx.ReactiveProperty`1<System.Boolean>
struct ReactiveProperty_1_t819338379;
// System.Exception
struct Exception_t1967233988;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Exception1967233988.h"

// System.Void UniRx.ReactiveProperty`1/ReactivePropertyObserver<System.Boolean>::.ctor(UniRx.ReactiveProperty`1<T>)
extern "C"  void ReactivePropertyObserver__ctor_m2181500865_gshared (ReactivePropertyObserver_t446987910 * __this, ReactiveProperty_1_t819338379 * ___parent0, const MethodInfo* method);
#define ReactivePropertyObserver__ctor_m2181500865(__this, ___parent0, method) ((  void (*) (ReactivePropertyObserver_t446987910 *, ReactiveProperty_1_t819338379 *, const MethodInfo*))ReactivePropertyObserver__ctor_m2181500865_gshared)(__this, ___parent0, method)
// System.Void UniRx.ReactiveProperty`1/ReactivePropertyObserver<System.Boolean>::OnNext(T)
extern "C"  void ReactivePropertyObserver_OnNext_m1922476746_gshared (ReactivePropertyObserver_t446987910 * __this, bool ___value0, const MethodInfo* method);
#define ReactivePropertyObserver_OnNext_m1922476746(__this, ___value0, method) ((  void (*) (ReactivePropertyObserver_t446987910 *, bool, const MethodInfo*))ReactivePropertyObserver_OnNext_m1922476746_gshared)(__this, ___value0, method)
// System.Void UniRx.ReactiveProperty`1/ReactivePropertyObserver<System.Boolean>::OnError(System.Exception)
extern "C"  void ReactivePropertyObserver_OnError_m3847221721_gshared (ReactivePropertyObserver_t446987910 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method);
#define ReactivePropertyObserver_OnError_m3847221721(__this, ___error0, method) ((  void (*) (ReactivePropertyObserver_t446987910 *, Exception_t1967233988 *, const MethodInfo*))ReactivePropertyObserver_OnError_m3847221721_gshared)(__this, ___error0, method)
// System.Void UniRx.ReactiveProperty`1/ReactivePropertyObserver<System.Boolean>::OnCompleted()
extern "C"  void ReactivePropertyObserver_OnCompleted_m4083147436_gshared (ReactivePropertyObserver_t446987910 * __this, const MethodInfo* method);
#define ReactivePropertyObserver_OnCompleted_m4083147436(__this, method) ((  void (*) (ReactivePropertyObserver_t446987910 *, const MethodInfo*))ReactivePropertyObserver_OnCompleted_m4083147436_gshared)(__this, method)
