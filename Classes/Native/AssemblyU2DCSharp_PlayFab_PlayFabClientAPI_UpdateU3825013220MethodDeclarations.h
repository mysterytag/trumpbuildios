﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.PlayFabClientAPI/UpdateUserDataResponseCallback
struct UpdateUserDataResponseCallback_t3825013220;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;
// PlayFab.ClientModels.UpdateUserDataRequest
struct UpdateUserDataRequest_t3516896929;
// PlayFab.ClientModels.UpdateUserDataResult
struct UpdateUserDataResult_t4027921387;
// PlayFab.PlayFabError
struct PlayFabError_t750598646;
// System.IAsyncResult
struct IAsyncResult_t537683269;
// System.AsyncCallback
struct AsyncCallback_t1363551830;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_IntPtr676692020.h"
#include "mscorlib_System_String968488902.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_UpdateUserD3516896929.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_UpdateUserD4027921387.h"
#include "AssemblyU2DCSharpU2Dfirstpass_PlayFab_PlayFabError750598646.h"
#include "mscorlib_System_AsyncCallback1363551830.h"

// System.Void PlayFab.PlayFabClientAPI/UpdateUserDataResponseCallback::.ctor(System.Object,System.IntPtr)
extern "C"  void UpdateUserDataResponseCallback__ctor_m1794800851 (UpdateUserDataResponseCallback_t3825013220 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/UpdateUserDataResponseCallback::Invoke(System.String,System.Int32,PlayFab.ClientModels.UpdateUserDataRequest,PlayFab.ClientModels.UpdateUserDataResult,PlayFab.PlayFabError,System.Object)
extern "C"  void UpdateUserDataResponseCallback_Invoke_m3494590048 (UpdateUserDataResponseCallback_t3825013220 * __this, String_t* ___urlPath0, int32_t ___callId1, UpdateUserDataRequest_t3516896929 * ___request2, UpdateUserDataResult_t4027921387 * ___result3, PlayFabError_t750598646 * ___error4, Il2CppObject * ___customData5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
extern "C" void pinvoke_delegate_wrapper_UpdateUserDataResponseCallback_t3825013220(Il2CppObject* delegate, String_t* ___urlPath0, int32_t ___callId1, UpdateUserDataRequest_t3516896929 * ___request2, UpdateUserDataResult_t4027921387 * ___result3, PlayFabError_t750598646 * ___error4, Il2CppObject * ___customData5);
// System.IAsyncResult PlayFab.PlayFabClientAPI/UpdateUserDataResponseCallback::BeginInvoke(System.String,System.Int32,PlayFab.ClientModels.UpdateUserDataRequest,PlayFab.ClientModels.UpdateUserDataResult,PlayFab.PlayFabError,System.Object,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * UpdateUserDataResponseCallback_BeginInvoke_m3032327053 (UpdateUserDataResponseCallback_t3825013220 * __this, String_t* ___urlPath0, int32_t ___callId1, UpdateUserDataRequest_t3516896929 * ___request2, UpdateUserDataResult_t4027921387 * ___result3, PlayFabError_t750598646 * ___error4, Il2CppObject * ___customData5, AsyncCallback_t1363551830 * ___callback6, Il2CppObject * ___object7, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/UpdateUserDataResponseCallback::EndInvoke(System.IAsyncResult)
extern "C"  void UpdateUserDataResponseCallback_EndInvoke_m836456547 (UpdateUserDataResponseCallback_t3825013220 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
