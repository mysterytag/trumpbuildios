﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.SelectManyObservable`2/SelectManyOuterObserver<System.Object,System.Object>
struct SelectManyOuterObserver_t4093485561;
// UniRx.Operators.SelectManyObservable`2<System.Object,System.Object>
struct SelectManyObservable_2_t562993966;
// UniRx.IObserver`1<System.Object>
struct IObserver_1_t3049105323;
// System.IDisposable
struct IDisposable_t1628921374;
// System.Object
struct Il2CppObject;
// System.Exception
struct Exception_t1967233988;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_Exception1967233988.h"

// System.Void UniRx.Operators.SelectManyObservable`2/SelectManyOuterObserver<System.Object,System.Object>::.ctor(UniRx.Operators.SelectManyObservable`2<TSource,TResult>,UniRx.IObserver`1<TResult>,System.IDisposable)
extern "C"  void SelectManyOuterObserver__ctor_m3211563007_gshared (SelectManyOuterObserver_t4093485561 * __this, SelectManyObservable_2_t562993966 * ___parent0, Il2CppObject* ___observer1, Il2CppObject * ___cancel2, const MethodInfo* method);
#define SelectManyOuterObserver__ctor_m3211563007(__this, ___parent0, ___observer1, ___cancel2, method) ((  void (*) (SelectManyOuterObserver_t4093485561 *, SelectManyObservable_2_t562993966 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))SelectManyOuterObserver__ctor_m3211563007_gshared)(__this, ___parent0, ___observer1, ___cancel2, method)
// System.IDisposable UniRx.Operators.SelectManyObservable`2/SelectManyOuterObserver<System.Object,System.Object>::Run()
extern "C"  Il2CppObject * SelectManyOuterObserver_Run_m1456531679_gshared (SelectManyOuterObserver_t4093485561 * __this, const MethodInfo* method);
#define SelectManyOuterObserver_Run_m1456531679(__this, method) ((  Il2CppObject * (*) (SelectManyOuterObserver_t4093485561 *, const MethodInfo*))SelectManyOuterObserver_Run_m1456531679_gshared)(__this, method)
// System.Void UniRx.Operators.SelectManyObservable`2/SelectManyOuterObserver<System.Object,System.Object>::OnNext(TSource)
extern "C"  void SelectManyOuterObserver_OnNext_m3292407048_gshared (SelectManyOuterObserver_t4093485561 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define SelectManyOuterObserver_OnNext_m3292407048(__this, ___value0, method) ((  void (*) (SelectManyOuterObserver_t4093485561 *, Il2CppObject *, const MethodInfo*))SelectManyOuterObserver_OnNext_m3292407048_gshared)(__this, ___value0, method)
// System.Void UniRx.Operators.SelectManyObservable`2/SelectManyOuterObserver<System.Object,System.Object>::OnError(System.Exception)
extern "C"  void SelectManyOuterObserver_OnError_m3765582450_gshared (SelectManyOuterObserver_t4093485561 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method);
#define SelectManyOuterObserver_OnError_m3765582450(__this, ___error0, method) ((  void (*) (SelectManyOuterObserver_t4093485561 *, Exception_t1967233988 *, const MethodInfo*))SelectManyOuterObserver_OnError_m3765582450_gshared)(__this, ___error0, method)
// System.Void UniRx.Operators.SelectManyObservable`2/SelectManyOuterObserver<System.Object,System.Object>::OnCompleted()
extern "C"  void SelectManyOuterObserver_OnCompleted_m2751432901_gshared (SelectManyOuterObserver_t4093485561 * __this, const MethodInfo* method);
#define SelectManyOuterObserver_OnCompleted_m2751432901(__this, method) ((  void (*) (SelectManyOuterObserver_t4093485561 *, const MethodInfo*))SelectManyOuterObserver_OnCompleted_m2751432901_gshared)(__this, method)
