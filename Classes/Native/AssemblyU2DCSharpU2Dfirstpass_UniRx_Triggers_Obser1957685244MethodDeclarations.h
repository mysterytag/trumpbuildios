﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Triggers.ObservableCancelTrigger
struct ObservableCancelTrigger_t1957685244;
// UnityEngine.EventSystems.BaseEventData
struct BaseEventData_t3547103726;
// UniRx.IObservable`1<UnityEngine.EventSystems.BaseEventData>
struct IObservable_1_t3305902090;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_BaseEventD3547103726.h"

// System.Void UniRx.Triggers.ObservableCancelTrigger::.ctor()
extern "C"  void ObservableCancelTrigger__ctor_m2299886191 (ObservableCancelTrigger_t1957685244 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.Triggers.ObservableCancelTrigger::UnityEngine.EventSystems.ICancelHandler.OnCancel(UnityEngine.EventSystems.BaseEventData)
extern "C"  void ObservableCancelTrigger_UnityEngine_EventSystems_ICancelHandler_OnCancel_m571095074 (ObservableCancelTrigger_t1957685244 * __this, BaseEventData_t3547103726 * ___eventData0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UniRx.IObservable`1<UnityEngine.EventSystems.BaseEventData> UniRx.Triggers.ObservableCancelTrigger::OnCancelAsObservable()
extern "C"  Il2CppObject* ObservableCancelTrigger_OnCancelAsObservable_m69386485 (ObservableCancelTrigger_t1957685244 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.Triggers.ObservableCancelTrigger::RaiseOnCompletedOnDestroy()
extern "C"  void ObservableCancelTrigger_RaiseOnCompletedOnDestroy_m2784740392 (ObservableCancelTrigger_t1957685244 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
