﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "System_Core_System_Func_2_gen1833193546MethodDeclarations.h"

// System.Void System.Func`2<GlobalStat,System.Double>::.ctor(System.Object,System.IntPtr)
#define Func_2__ctor_m1223607411(__this, ___object0, ___method1, method) ((  void (*) (Func_2_t2227950619 *, Il2CppObject *, IntPtr_t, const MethodInfo*))Func_2__ctor_m3559531450_gshared)(__this, ___object0, ___method1, method)
// TResult System.Func`2<GlobalStat,System.Double>::Invoke(T)
#define Func_2_Invoke_m850325155(__this, ___arg10, method) ((  double (*) (Func_2_t2227950619 *, GlobalStat_t1134678199 *, const MethodInfo*))Func_2_Invoke_m3018055400_gshared)(__this, ___arg10, method)
// System.IAsyncResult System.Func`2<GlobalStat,System.Double>::BeginInvoke(T,System.AsyncCallback,System.Object)
#define Func_2_BeginInvoke_m2387214806(__this, ___arg10, ___callback1, ___object2, method) ((  Il2CppObject * (*) (Func_2_t2227950619 *, GlobalStat_t1134678199 *, AsyncCallback_t1363551830 *, Il2CppObject *, const MethodInfo*))Func_2_BeginInvoke_m258584343_gshared)(__this, ___arg10, ___callback1, ___object2, method)
// TResult System.Func`2<GlobalStat,System.Double>::EndInvoke(System.IAsyncResult)
#define Func_2_EndInvoke_m529262865(__this, ___result0, method) ((  double (*) (Func_2_t2227950619 *, Il2CppObject *, const MethodInfo*))Func_2_EndInvoke_m3352839660_gshared)(__this, ___result0, method)
