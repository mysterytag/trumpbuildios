﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_PlayFab_ReflectionUtils_ThreadSa1771086527MethodDeclarations.h"

// System.Void PlayFab.ReflectionUtils/ThreadSafeDictionaryValueFactory`2<System.Type,PlayFab.ReflectionUtils/ConstructorDelegate>::.ctor(System.Object,System.IntPtr)
#define ThreadSafeDictionaryValueFactory_2__ctor_m628202317(__this, ___object0, ___method1, method) ((  void (*) (ThreadSafeDictionaryValueFactory_2_t4132158723 *, Il2CppObject *, IntPtr_t, const MethodInfo*))ThreadSafeDictionaryValueFactory_2__ctor_m191619557_gshared)(__this, ___object0, ___method1, method)
// TValue PlayFab.ReflectionUtils/ThreadSafeDictionaryValueFactory`2<System.Type,PlayFab.ReflectionUtils/ConstructorDelegate>::Invoke(TKey)
#define ThreadSafeDictionaryValueFactory_2_Invoke_m1642878652(__this, ___key0, method) ((  ConstructorDelegate_t4072949631 * (*) (ThreadSafeDictionaryValueFactory_2_t4132158723 *, Type_t *, const MethodInfo*))ThreadSafeDictionaryValueFactory_2_Invoke_m808333140_gshared)(__this, ___key0, method)
// System.IAsyncResult PlayFab.ReflectionUtils/ThreadSafeDictionaryValueFactory`2<System.Type,PlayFab.ReflectionUtils/ConstructorDelegate>::BeginInvoke(TKey,System.AsyncCallback,System.Object)
#define ThreadSafeDictionaryValueFactory_2_BeginInvoke_m3032035077(__this, ___key0, ___callback1, ___object2, method) ((  Il2CppObject * (*) (ThreadSafeDictionaryValueFactory_2_t4132158723 *, Type_t *, AsyncCallback_t1363551830 *, Il2CppObject *, const MethodInfo*))ThreadSafeDictionaryValueFactory_2_BeginInvoke_m3493335453_gshared)(__this, ___key0, ___callback1, ___object2, method)
// TValue PlayFab.ReflectionUtils/ThreadSafeDictionaryValueFactory`2<System.Type,PlayFab.ReflectionUtils/ConstructorDelegate>::EndInvoke(System.IAsyncResult)
#define ThreadSafeDictionaryValueFactory_2_EndInvoke_m2178106301(__this, ___result0, method) ((  ConstructorDelegate_t4072949631 * (*) (ThreadSafeDictionaryValueFactory_2_t4132158723 *, Il2CppObject *, const MethodInfo*))ThreadSafeDictionaryValueFactory_2_EndInvoke_m2172200533_gshared)(__this, ___result0, method)
