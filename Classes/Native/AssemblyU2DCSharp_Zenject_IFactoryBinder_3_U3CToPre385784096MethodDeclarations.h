﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Zenject.IFactoryBinder`3/<ToPrefab>c__AnonStorey179<System.Object,System.Object,System.Object>
struct U3CToPrefabU3Ec__AnonStorey179_t385784096;
// Zenject.IFactory`3<System.Object,System.Object,System.Object>
struct IFactory_3_t1619999162;
// Zenject.InjectContext
struct InjectContext_t3456483891;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Zenject_InjectContext3456483891.h"

// System.Void Zenject.IFactoryBinder`3/<ToPrefab>c__AnonStorey179<System.Object,System.Object,System.Object>::.ctor()
extern "C"  void U3CToPrefabU3Ec__AnonStorey179__ctor_m575200516_gshared (U3CToPrefabU3Ec__AnonStorey179_t385784096 * __this, const MethodInfo* method);
#define U3CToPrefabU3Ec__AnonStorey179__ctor_m575200516(__this, method) ((  void (*) (U3CToPrefabU3Ec__AnonStorey179_t385784096 *, const MethodInfo*))U3CToPrefabU3Ec__AnonStorey179__ctor_m575200516_gshared)(__this, method)
// Zenject.IFactory`3<TParam1,TParam2,TContract> Zenject.IFactoryBinder`3/<ToPrefab>c__AnonStorey179<System.Object,System.Object,System.Object>::<>m__29F(Zenject.InjectContext)
extern "C"  Il2CppObject* U3CToPrefabU3Ec__AnonStorey179_U3CU3Em__29F_m1079674036_gshared (U3CToPrefabU3Ec__AnonStorey179_t385784096 * __this, InjectContext_t3456483891 * ___ctx0, const MethodInfo* method);
#define U3CToPrefabU3Ec__AnonStorey179_U3CU3Em__29F_m1079674036(__this, ___ctx0, method) ((  Il2CppObject* (*) (U3CToPrefabU3Ec__AnonStorey179_t385784096 *, InjectContext_t3456483891 *, const MethodInfo*))U3CToPrefabU3Ec__AnonStorey179_U3CU3Em__29F_m1079674036_gshared)(__this, ___ctx0, method)
