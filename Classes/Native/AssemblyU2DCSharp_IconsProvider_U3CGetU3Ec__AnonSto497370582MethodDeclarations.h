﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// IconsProvider/<Get>c__AnonStoreyE8
struct U3CGetU3Ec__AnonStoreyE8_t497370582;
// UnityEngine.Sprite
struct Sprite_t4006040370;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Sprite4006040370.h"

// System.Void IconsProvider/<Get>c__AnonStoreyE8::.ctor()
extern "C"  void U3CGetU3Ec__AnonStoreyE8__ctor_m3978087297 (U3CGetU3Ec__AnonStoreyE8_t497370582 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean IconsProvider/<Get>c__AnonStoreyE8::<>m__118(UnityEngine.Sprite)
extern "C"  bool U3CGetU3Ec__AnonStoreyE8_U3CU3Em__118_m1531639016 (U3CGetU3Ec__AnonStoreyE8_t497370582 * __this, Sprite_t4006040370 * ___sprite0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
