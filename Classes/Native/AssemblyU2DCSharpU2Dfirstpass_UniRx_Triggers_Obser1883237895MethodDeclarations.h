﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Triggers.ObservableTriggerBase
struct ObservableTriggerBase_t1883237895;
// UniRx.IObservable`1<UniRx.Unit>
struct IObservable_1_t2317084402;

#include "codegen/il2cpp-codegen.h"

// System.Void UniRx.Triggers.ObservableTriggerBase::.ctor()
extern "C"  void ObservableTriggerBase__ctor_m2608961156 (ObservableTriggerBase_t1883237895 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.Triggers.ObservableTriggerBase::Awake()
extern "C"  void ObservableTriggerBase_Awake_m2846566375 (ObservableTriggerBase_t1883237895 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UniRx.IObservable`1<UniRx.Unit> UniRx.Triggers.ObservableTriggerBase::AwakeAsObservable()
extern "C"  Il2CppObject* ObservableTriggerBase_AwakeAsObservable_m1492518420 (ObservableTriggerBase_t1883237895 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.Triggers.ObservableTriggerBase::Start()
extern "C"  void ObservableTriggerBase_Start_m1556098948 (ObservableTriggerBase_t1883237895 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UniRx.IObservable`1<UniRx.Unit> UniRx.Triggers.ObservableTriggerBase::StartAsObservable()
extern "C"  Il2CppObject* ObservableTriggerBase_StartAsObservable_m4027812401 (ObservableTriggerBase_t1883237895 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.Triggers.ObservableTriggerBase::OnDestroy()
extern "C"  void ObservableTriggerBase_OnDestroy_m3420227965 (ObservableTriggerBase_t1883237895 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UniRx.IObservable`1<UniRx.Unit> UniRx.Triggers.ObservableTriggerBase::OnDestroyAsObservable()
extern "C"  Il2CppObject* ObservableTriggerBase_OnDestroyAsObservable_m2566806186 (ObservableTriggerBase_t1883237895 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
