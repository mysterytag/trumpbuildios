﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.CatchObservable`2<System.Object,System.Object>
struct CatchObservable_2_t3974704686;
// UniRx.IObservable`1<System.Object>
struct IObservable_1_t595904784;
// System.Func`2<System.Object,UniRx.IObservable`1<System.Object>>
struct Func_2_t1894581716;
// System.IDisposable
struct IDisposable_t1628921374;
// UniRx.IObserver`1<System.Object>
struct IObserver_1_t3049105323;

#include "codegen/il2cpp-codegen.h"

// System.Void UniRx.Operators.CatchObservable`2<System.Object,System.Object>::.ctor(UniRx.IObservable`1<T>,System.Func`2<TException,UniRx.IObservable`1<T>>)
extern "C"  void CatchObservable_2__ctor_m2365847670_gshared (CatchObservable_2_t3974704686 * __this, Il2CppObject* ___source0, Func_2_t1894581716 * ___errorHandler1, const MethodInfo* method);
#define CatchObservable_2__ctor_m2365847670(__this, ___source0, ___errorHandler1, method) ((  void (*) (CatchObservable_2_t3974704686 *, Il2CppObject*, Func_2_t1894581716 *, const MethodInfo*))CatchObservable_2__ctor_m2365847670_gshared)(__this, ___source0, ___errorHandler1, method)
// System.IDisposable UniRx.Operators.CatchObservable`2<System.Object,System.Object>::SubscribeCore(UniRx.IObserver`1<T>,System.IDisposable)
extern "C"  Il2CppObject * CatchObservable_2_SubscribeCore_m1675312480_gshared (CatchObservable_2_t3974704686 * __this, Il2CppObject* ___observer0, Il2CppObject * ___cancel1, const MethodInfo* method);
#define CatchObservable_2_SubscribeCore_m1675312480(__this, ___observer0, ___cancel1, method) ((  Il2CppObject * (*) (CatchObservable_2_t3974704686 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))CatchObservable_2_SubscribeCore_m1675312480_gshared)(__this, ___observer0, ___cancel1, method)
