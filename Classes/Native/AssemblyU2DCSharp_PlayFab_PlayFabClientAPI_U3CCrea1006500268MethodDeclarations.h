﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.PlayFabClientAPI/<CreateSharedGroup>c__AnonStoreyBC
struct U3CCreateSharedGroupU3Ec__AnonStoreyBC_t1006500268;
// PlayFab.CallRequestContainer
struct CallRequestContainer_t432318609;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharpU2Dfirstpass_PlayFab_CallRequestCo432318609.h"

// System.Void PlayFab.PlayFabClientAPI/<CreateSharedGroup>c__AnonStoreyBC::.ctor()
extern "C"  void U3CCreateSharedGroupU3Ec__AnonStoreyBC__ctor_m3174650663 (U3CCreateSharedGroupU3Ec__AnonStoreyBC_t1006500268 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/<CreateSharedGroup>c__AnonStoreyBC::<>m__A9(PlayFab.CallRequestContainer)
extern "C"  void U3CCreateSharedGroupU3Ec__AnonStoreyBC_U3CU3Em__A9_m2368500189 (U3CCreateSharedGroupU3Ec__AnonStoreyBC_t1006500268 * __this, CallRequestContainer_t432318609 * ___requestContainer0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
