﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// AdToApp.AdToAppSDKDelegate/AndroidBannerListener
struct AndroidBannerListener_t3507331119;
// AdToApp.AdToAppSDKDelegate
struct AdToAppSDKDelegate_t2853395885;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharpU2Dfirstpass_AdToApp_AdToAppSDKDe2853395885.h"

// System.Void AdToApp.AdToAppSDKDelegate/AndroidBannerListener::.ctor(AdToApp.AdToAppSDKDelegate)
extern "C"  void AndroidBannerListener__ctor_m2581129825 (AndroidBannerListener_t3507331119 * __this, AdToAppSDKDelegate_t2853395885 * ___sdkDelegate0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AdToApp.AdToAppSDKDelegate/AndroidBannerListener::onBannerLoad()
extern "C"  void AndroidBannerListener_onBannerLoad_m1677428397 (AndroidBannerListener_t3507331119 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AdToApp.AdToAppSDKDelegate/AndroidBannerListener::onBannerFailedToLoad()
extern "C"  void AndroidBannerListener_onBannerFailedToLoad_m3131063909 (AndroidBannerListener_t3507331119 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AdToApp.AdToAppSDKDelegate/AndroidBannerListener::onBannerClicked()
extern "C"  void AndroidBannerListener_onBannerClicked_m1318837090 (AndroidBannerListener_t3507331119 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
