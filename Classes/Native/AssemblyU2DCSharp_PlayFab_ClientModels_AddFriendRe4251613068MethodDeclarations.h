﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.ClientModels.AddFriendResult
struct AddFriendResult_t4251613068;

#include "codegen/il2cpp-codegen.h"

// System.Void PlayFab.ClientModels.AddFriendResult::.ctor()
extern "C"  void AddFriendResult__ctor_m1370815901 (AddFriendResult_t4251613068 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PlayFab.ClientModels.AddFriendResult::get_Created()
extern "C"  bool AddFriendResult_get_Created_m3884103494 (AddFriendResult_t4251613068 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.AddFriendResult::set_Created(System.Boolean)
extern "C"  void AddFriendResult_set_Created_m3972103037 (AddFriendResult_t4251613068 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
