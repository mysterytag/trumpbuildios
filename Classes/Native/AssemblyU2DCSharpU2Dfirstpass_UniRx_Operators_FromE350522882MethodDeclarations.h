﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.FromEventObservable_`1<System.Int32>
struct FromEventObservable__1_t350522882;
// System.Action`1<System.Action`1<System.Int32>>
struct Action_1_t3144320197;
// System.IDisposable
struct IDisposable_t1628921374;
// UniRx.IObserver`1<System.Int32>
struct IObserver_1_t764446394;

#include "codegen/il2cpp-codegen.h"

// System.Void UniRx.Operators.FromEventObservable_`1<System.Int32>::.ctor(System.Action`1<System.Action`1<T>>,System.Action`1<System.Action`1<T>>)
extern "C"  void FromEventObservable__1__ctor_m3339292550_gshared (FromEventObservable__1_t350522882 * __this, Action_1_t3144320197 * ___addHandler0, Action_1_t3144320197 * ___removeHandler1, const MethodInfo* method);
#define FromEventObservable__1__ctor_m3339292550(__this, ___addHandler0, ___removeHandler1, method) ((  void (*) (FromEventObservable__1_t350522882 *, Action_1_t3144320197 *, Action_1_t3144320197 *, const MethodInfo*))FromEventObservable__1__ctor_m3339292550_gshared)(__this, ___addHandler0, ___removeHandler1, method)
// System.IDisposable UniRx.Operators.FromEventObservable_`1<System.Int32>::SubscribeCore(UniRx.IObserver`1<T>,System.IDisposable)
extern "C"  Il2CppObject * FromEventObservable__1_SubscribeCore_m1012114774_gshared (FromEventObservable__1_t350522882 * __this, Il2CppObject* ___observer0, Il2CppObject * ___cancel1, const MethodInfo* method);
#define FromEventObservable__1_SubscribeCore_m1012114774(__this, ___observer0, ___cancel1, method) ((  Il2CppObject * (*) (FromEventObservable__1_t350522882 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))FromEventObservable__1_SubscribeCore_m1012114774_gshared)(__this, ___observer0, ___cancel1, method)
