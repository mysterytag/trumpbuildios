﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_PlayFab_ReflectionUtils_ThreadSa1163962996MethodDeclarations.h"

// System.Void PlayFab.ReflectionUtils/ThreadSafeDictionary`2<System.Type,System.Collections.Generic.IDictionary`2<System.String,PlayFab.ReflectionUtils/GetDelegate>>::.ctor(PlayFab.ReflectionUtils/ThreadSafeDictionaryValueFactory`2<TKey,TValue>)
#define ThreadSafeDictionary_2__ctor_m1399909364(__this, ___valueFactory0, method) ((  void (*) (ThreadSafeDictionary_2_t2535572991 *, ThreadSafeDictionaryValueFactory_2_t3142696522 *, const MethodInfo*))ThreadSafeDictionary_2__ctor_m145995086_gshared)(__this, ___valueFactory0, method)
// System.Collections.IEnumerator PlayFab.ReflectionUtils/ThreadSafeDictionary`2<System.Type,System.Collections.Generic.IDictionary`2<System.String,PlayFab.ReflectionUtils/GetDelegate>>::System.Collections.IEnumerable.GetEnumerator()
#define ThreadSafeDictionary_2_System_Collections_IEnumerable_GetEnumerator_m3159015779(__this, method) ((  Il2CppObject * (*) (ThreadSafeDictionary_2_t2535572991 *, const MethodInfo*))ThreadSafeDictionary_2_System_Collections_IEnumerable_GetEnumerator_m647792317_gshared)(__this, method)
// TValue PlayFab.ReflectionUtils/ThreadSafeDictionary`2<System.Type,System.Collections.Generic.IDictionary`2<System.String,PlayFab.ReflectionUtils/GetDelegate>>::Get(TKey)
#define ThreadSafeDictionary_2_Get_m1817227655(__this, ___key0, method) ((  Il2CppObject* (*) (ThreadSafeDictionary_2_t2535572991 *, Type_t *, const MethodInfo*))ThreadSafeDictionary_2_Get_m3585103853_gshared)(__this, ___key0, method)
// TValue PlayFab.ReflectionUtils/ThreadSafeDictionary`2<System.Type,System.Collections.Generic.IDictionary`2<System.String,PlayFab.ReflectionUtils/GetDelegate>>::AddValue(TKey)
#define ThreadSafeDictionary_2_AddValue_m2781736299(__this, ___key0, method) ((  Il2CppObject* (*) (ThreadSafeDictionary_2_t2535572991 *, Type_t *, const MethodInfo*))ThreadSafeDictionary_2_AddValue_m844388037_gshared)(__this, ___key0, method)
// System.Void PlayFab.ReflectionUtils/ThreadSafeDictionary`2<System.Type,System.Collections.Generic.IDictionary`2<System.String,PlayFab.ReflectionUtils/GetDelegate>>::Add(TKey,TValue)
#define ThreadSafeDictionary_2_Add_m2324283959(__this, ___key0, ___value1, method) ((  void (*) (ThreadSafeDictionary_2_t2535572991 *, Type_t *, Il2CppObject*, const MethodInfo*))ThreadSafeDictionary_2_Add_m3884150033_gshared)(__this, ___key0, ___value1, method)
// System.Boolean PlayFab.ReflectionUtils/ThreadSafeDictionary`2<System.Type,System.Collections.Generic.IDictionary`2<System.String,PlayFab.ReflectionUtils/GetDelegate>>::ContainsKey(TKey)
#define ThreadSafeDictionary_2_ContainsKey_m946092293(__this, ___key0, method) ((  bool (*) (ThreadSafeDictionary_2_t2535572991 *, Type_t *, const MethodInfo*))ThreadSafeDictionary_2_ContainsKey_m2111272811_gshared)(__this, ___key0, method)
// System.Collections.Generic.ICollection`1<TKey> PlayFab.ReflectionUtils/ThreadSafeDictionary`2<System.Type,System.Collections.Generic.IDictionary`2<System.String,PlayFab.ReflectionUtils/GetDelegate>>::get_Keys()
#define ThreadSafeDictionary_2_get_Keys_m4031362982(__this, method) ((  Il2CppObject* (*) (ThreadSafeDictionary_2_t2535572991 *, const MethodInfo*))ThreadSafeDictionary_2_get_Keys_m79078016_gshared)(__this, method)
// System.Boolean PlayFab.ReflectionUtils/ThreadSafeDictionary`2<System.Type,System.Collections.Generic.IDictionary`2<System.String,PlayFab.ReflectionUtils/GetDelegate>>::Remove(TKey)
#define ThreadSafeDictionary_2_Remove_m2655236299(__this, ___key0, method) ((  bool (*) (ThreadSafeDictionary_2_t2535572991 *, Type_t *, const MethodInfo*))ThreadSafeDictionary_2_Remove_m3287321253_gshared)(__this, ___key0, method)
// System.Boolean PlayFab.ReflectionUtils/ThreadSafeDictionary`2<System.Type,System.Collections.Generic.IDictionary`2<System.String,PlayFab.ReflectionUtils/GetDelegate>>::TryGetValue(TKey,TValue&)
#define ThreadSafeDictionary_2_TryGetValue_m3321740382(__this, ___key0, ___value1, method) ((  bool (*) (ThreadSafeDictionary_2_t2535572991 *, Type_t *, Il2CppObject**, const MethodInfo*))ThreadSafeDictionary_2_TryGetValue_m2392274116_gshared)(__this, ___key0, ___value1, method)
// System.Collections.Generic.ICollection`1<TValue> PlayFab.ReflectionUtils/ThreadSafeDictionary`2<System.Type,System.Collections.Generic.IDictionary`2<System.String,PlayFab.ReflectionUtils/GetDelegate>>::get_Values()
#define ThreadSafeDictionary_2_get_Values_m2316574914(__this, method) ((  Il2CppObject* (*) (ThreadSafeDictionary_2_t2535572991 *, const MethodInfo*))ThreadSafeDictionary_2_get_Values_m2528035356_gshared)(__this, method)
// TValue PlayFab.ReflectionUtils/ThreadSafeDictionary`2<System.Type,System.Collections.Generic.IDictionary`2<System.String,PlayFab.ReflectionUtils/GetDelegate>>::get_Item(TKey)
#define ThreadSafeDictionary_2_get_Item_m3066128759(__this, ___key0, method) ((  Il2CppObject* (*) (ThreadSafeDictionary_2_t2535572991 *, Type_t *, const MethodInfo*))ThreadSafeDictionary_2_get_Item_m1128780497_gshared)(__this, ___key0, method)
// System.Void PlayFab.ReflectionUtils/ThreadSafeDictionary`2<System.Type,System.Collections.Generic.IDictionary`2<System.String,PlayFab.ReflectionUtils/GetDelegate>>::set_Item(TKey,TValue)
#define ThreadSafeDictionary_2_set_Item_m4225223038(__this, ___key0, ___value1, method) ((  void (*) (ThreadSafeDictionary_2_t2535572991 *, Type_t *, Il2CppObject*, const MethodInfo*))ThreadSafeDictionary_2_set_Item_m1582880484_gshared)(__this, ___key0, ___value1, method)
// System.Void PlayFab.ReflectionUtils/ThreadSafeDictionary`2<System.Type,System.Collections.Generic.IDictionary`2<System.String,PlayFab.ReflectionUtils/GetDelegate>>::Add(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
#define ThreadSafeDictionary_2_Add_m3301379022(__this, ___item0, method) ((  void (*) (ThreadSafeDictionary_2_t2535572991 *, KeyValuePair_2_t389599147 , const MethodInfo*))ThreadSafeDictionary_2_Add_m1635310772_gshared)(__this, ___item0, method)
// System.Void PlayFab.ReflectionUtils/ThreadSafeDictionary`2<System.Type,System.Collections.Generic.IDictionary`2<System.String,PlayFab.ReflectionUtils/GetDelegate>>::Clear()
#define ThreadSafeDictionary_2_Clear_m205427817(__this, method) ((  void (*) (ThreadSafeDictionary_2_t2535572991 *, const MethodInfo*))ThreadSafeDictionary_2_Clear_m3600968783_gshared)(__this, method)
// System.Boolean PlayFab.ReflectionUtils/ThreadSafeDictionary`2<System.Type,System.Collections.Generic.IDictionary`2<System.String,PlayFab.ReflectionUtils/GetDelegate>>::Contains(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
#define ThreadSafeDictionary_2_Contains_m412502594(__this, ___item0, method) ((  bool (*) (ThreadSafeDictionary_2_t2535572991 *, KeyValuePair_2_t389599147 , const MethodInfo*))ThreadSafeDictionary_2_Contains_m78891804_gshared)(__this, ___item0, method)
// System.Void PlayFab.ReflectionUtils/ThreadSafeDictionary`2<System.Type,System.Collections.Generic.IDictionary`2<System.String,PlayFab.ReflectionUtils/GetDelegate>>::CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
#define ThreadSafeDictionary_2_CopyTo_m1601299506(__this, ___array0, ___arrayIndex1, method) ((  void (*) (ThreadSafeDictionary_2_t2535572991 *, KeyValuePair_2U5BU5D_t1375853898*, int32_t, const MethodInfo*))ThreadSafeDictionary_2_CopyTo_m3361965976_gshared)(__this, ___array0, ___arrayIndex1, method)
// System.Int32 PlayFab.ReflectionUtils/ThreadSafeDictionary`2<System.Type,System.Collections.Generic.IDictionary`2<System.String,PlayFab.ReflectionUtils/GetDelegate>>::get_Count()
#define ThreadSafeDictionary_2_get_Count_m2421302352(__this, method) ((  int32_t (*) (ThreadSafeDictionary_2_t2535572991 *, const MethodInfo*))ThreadSafeDictionary_2_get_Count_m187477558_gshared)(__this, method)
// System.Boolean PlayFab.ReflectionUtils/ThreadSafeDictionary`2<System.Type,System.Collections.Generic.IDictionary`2<System.String,PlayFab.ReflectionUtils/GetDelegate>>::get_IsReadOnly()
#define ThreadSafeDictionary_2_get_IsReadOnly_m1905693639(__this, method) ((  bool (*) (ThreadSafeDictionary_2_t2535572991 *, const MethodInfo*))ThreadSafeDictionary_2_get_IsReadOnly_m3744395425_gshared)(__this, method)
// System.Boolean PlayFab.ReflectionUtils/ThreadSafeDictionary`2<System.Type,System.Collections.Generic.IDictionary`2<System.String,PlayFab.ReflectionUtils/GetDelegate>>::Remove(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
#define ThreadSafeDictionary_2_Remove_m2656442855(__this, ___item0, method) ((  bool (*) (ThreadSafeDictionary_2_t2535572991 *, KeyValuePair_2_t389599147 , const MethodInfo*))ThreadSafeDictionary_2_Remove_m4054976833_gshared)(__this, ___item0, method)
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<TKey,TValue>> PlayFab.ReflectionUtils/ThreadSafeDictionary`2<System.Type,System.Collections.Generic.IDictionary`2<System.String,PlayFab.ReflectionUtils/GetDelegate>>::GetEnumerator()
#define ThreadSafeDictionary_2_GetEnumerator_m199380689(__this, method) ((  Il2CppObject* (*) (ThreadSafeDictionary_2_t2535572991 *, const MethodInfo*))ThreadSafeDictionary_2_GetEnumerator_m2011494711_gshared)(__this, method)
