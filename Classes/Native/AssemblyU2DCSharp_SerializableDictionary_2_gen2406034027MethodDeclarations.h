﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_SerializableDictionary_2_gen2269426764MethodDeclarations.h"

// System.Void SerializableDictionary`2<System.String,AchievementDescription>::.ctor()
#define SerializableDictionary_2__ctor_m2957325179(__this, method) ((  void (*) (SerializableDictionary_2_t2406034027 *, const MethodInfo*))SerializableDictionary_2__ctor_m3570888488_gshared)(__this, method)
// System.Void SerializableDictionary`2<System.String,AchievementDescription>::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
#define SerializableDictionary_2__ctor_m26207804(__this, ___info0, ___context1, method) ((  void (*) (SerializableDictionary_2_t2406034027 *, SerializationInfo_t2995724695 *, StreamingContext_t986364934 , const MethodInfo*))SerializableDictionary_2__ctor_m2587960425_gshared)(__this, ___info0, ___context1, method)
// System.Xml.Schema.XmlSchema SerializableDictionary`2<System.String,AchievementDescription>::GetSchema()
#define SerializableDictionary_2_GetSchema_m2345426526(__this, method) ((  XmlSchema_t1932230565 * (*) (SerializableDictionary_2_t2406034027 *, const MethodInfo*))SerializableDictionary_2_GetSchema_m992727535_gshared)(__this, method)
// System.Void SerializableDictionary`2<System.String,AchievementDescription>::ReadXml(System.Xml.XmlReader)
#define SerializableDictionary_2_ReadXml_m2652945910(__this, ___reader0, method) ((  void (*) (SerializableDictionary_2_t2406034027 *, XmlReader_t4229084514 *, const MethodInfo*))SerializableDictionary_2_ReadXml_m3137948131_gshared)(__this, ___reader0, method)
// System.Void SerializableDictionary`2<System.String,AchievementDescription>::WriteXml(System.Xml.XmlWriter)
#define SerializableDictionary_2_WriteXml_m1660939757(__this, ___writer0, method) ((  void (*) (SerializableDictionary_2_t2406034027 *, XmlWriter_t89522450 *, const MethodInfo*))SerializableDictionary_2_WriteXml_m3811106720_gshared)(__this, ___writer0, method)
