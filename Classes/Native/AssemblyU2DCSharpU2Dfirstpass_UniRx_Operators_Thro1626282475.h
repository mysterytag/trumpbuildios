﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UniRx.IObserver`1<System.Object>
struct IObserver_1_t3049105323;
// UniRx.Operators.ThrowObservable`1<System.Object>
struct ThrowObservable_1_t3672276456;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Operators.ThrowObservable`1/<SubscribeCore>c__AnonStorey6B<System.Object>
struct  U3CSubscribeCoreU3Ec__AnonStorey6B_t1626282475  : public Il2CppObject
{
public:
	// UniRx.IObserver`1<T> UniRx.Operators.ThrowObservable`1/<SubscribeCore>c__AnonStorey6B::observer
	Il2CppObject* ___observer_0;
	// UniRx.Operators.ThrowObservable`1<T> UniRx.Operators.ThrowObservable`1/<SubscribeCore>c__AnonStorey6B::<>f__this
	ThrowObservable_1_t3672276456 * ___U3CU3Ef__this_1;

public:
	inline static int32_t get_offset_of_observer_0() { return static_cast<int32_t>(offsetof(U3CSubscribeCoreU3Ec__AnonStorey6B_t1626282475, ___observer_0)); }
	inline Il2CppObject* get_observer_0() const { return ___observer_0; }
	inline Il2CppObject** get_address_of_observer_0() { return &___observer_0; }
	inline void set_observer_0(Il2CppObject* value)
	{
		___observer_0 = value;
		Il2CppCodeGenWriteBarrier(&___observer_0, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__this_1() { return static_cast<int32_t>(offsetof(U3CSubscribeCoreU3Ec__AnonStorey6B_t1626282475, ___U3CU3Ef__this_1)); }
	inline ThrowObservable_1_t3672276456 * get_U3CU3Ef__this_1() const { return ___U3CU3Ef__this_1; }
	inline ThrowObservable_1_t3672276456 ** get_address_of_U3CU3Ef__this_1() { return &___U3CU3Ef__this_1; }
	inline void set_U3CU3Ef__this_1(ThrowObservable_1_t3672276456 * value)
	{
		___U3CU3Ef__this_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__this_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
