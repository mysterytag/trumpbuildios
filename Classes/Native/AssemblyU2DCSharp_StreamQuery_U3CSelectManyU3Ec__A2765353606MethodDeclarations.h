﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// StreamQuery/<SelectMany>c__AnonStorey13D`3<System.Object,System.Object,System.Object>
struct U3CSelectManyU3Ec__AnonStorey13D_3_t2765353606;
// IStream`1<System.Object>
struct IStream_1_t1389797411;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"

// System.Void StreamQuery/<SelectMany>c__AnonStorey13D`3<System.Object,System.Object,System.Object>::.ctor()
extern "C"  void U3CSelectManyU3Ec__AnonStorey13D_3__ctor_m117789039_gshared (U3CSelectManyU3Ec__AnonStorey13D_3_t2765353606 * __this, const MethodInfo* method);
#define U3CSelectManyU3Ec__AnonStorey13D_3__ctor_m117789039(__this, method) ((  void (*) (U3CSelectManyU3Ec__AnonStorey13D_3_t2765353606 *, const MethodInfo*))U3CSelectManyU3Ec__AnonStorey13D_3__ctor_m117789039_gshared)(__this, method)
// IStream`1<TR> StreamQuery/<SelectMany>c__AnonStorey13D`3<System.Object,System.Object,System.Object>::<>m__1D1(T)
extern "C"  Il2CppObject* U3CSelectManyU3Ec__AnonStorey13D_3_U3CU3Em__1D1_m1180917483_gshared (U3CSelectManyU3Ec__AnonStorey13D_3_t2765353606 * __this, Il2CppObject * ___x0, const MethodInfo* method);
#define U3CSelectManyU3Ec__AnonStorey13D_3_U3CU3Em__1D1_m1180917483(__this, ___x0, method) ((  Il2CppObject* (*) (U3CSelectManyU3Ec__AnonStorey13D_3_t2765353606 *, Il2CppObject *, const MethodInfo*))U3CSelectManyU3Ec__AnonStorey13D_3_U3CU3Em__1D1_m1180917483_gshared)(__this, ___x0, method)
