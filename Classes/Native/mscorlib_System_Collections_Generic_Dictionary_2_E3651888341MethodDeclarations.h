﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E3591453091MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Reflection.MethodInfo,System.Collections.Generic.HashSet`1<System.Object>>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define Enumerator__ctor_m72652380(__this, ___dictionary0, method) ((  void (*) (Enumerator_t3651888341 *, Dictionary_2_t3884860400 *, const MethodInfo*))Enumerator__ctor_m3920831137_gshared)(__this, ___dictionary0, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Reflection.MethodInfo,System.Collections.Generic.HashSet`1<System.Object>>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m940345679(__this, method) ((  Il2CppObject * (*) (Enumerator_t3651888341 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m3262087712_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Reflection.MethodInfo,System.Collections.Generic.HashSet`1<System.Object>>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m2240035033(__this, method) ((  void (*) (Enumerator_t3651888341 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m2959141748_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<System.Reflection.MethodInfo,System.Collections.Generic.HashSet`1<System.Object>>::System.Collections.IDictionaryEnumerator.get_Entry()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m2784343184(__this, method) ((  DictionaryEntry_t130027246  (*) (Enumerator_t3651888341 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m2279524093_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Reflection.MethodInfo,System.Collections.Generic.HashSet`1<System.Object>>::System.Collections.IDictionaryEnumerator.get_Key()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m1432178411(__this, method) ((  Il2CppObject * (*) (Enumerator_t3651888341 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m1201448700_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Reflection.MethodInfo,System.Collections.Generic.HashSet`1<System.Object>>::System.Collections.IDictionaryEnumerator.get_Value()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m2982354621(__this, method) ((  Il2CppObject * (*) (Enumerator_t3651888341 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m294434446_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.Reflection.MethodInfo,System.Collections.Generic.HashSet`1<System.Object>>::MoveNext()
#define Enumerator_MoveNext_m1851472562(__this, method) ((  bool (*) (Enumerator_t3651888341 *, const MethodInfo*))Enumerator_MoveNext_m217327200_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<System.Reflection.MethodInfo,System.Collections.Generic.HashSet`1<System.Object>>::get_Current()
#define Enumerator_get_Current_m1073493563(__this, method) ((  KeyValuePair_2_t3373391698  (*) (Enumerator_t3651888341 *, const MethodInfo*))Enumerator_get_Current_m4240003024_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<System.Reflection.MethodInfo,System.Collections.Generic.HashSet`1<System.Object>>::get_CurrentKey()
#define Enumerator_get_CurrentKey_m123644370(__this, method) ((  MethodInfo_t * (*) (Enumerator_t3651888341 *, const MethodInfo*))Enumerator_get_CurrentKey_m3062159917_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.Reflection.MethodInfo,System.Collections.Generic.HashSet`1<System.Object>>::get_CurrentValue()
#define Enumerator_get_CurrentValue_m3678911762(__this, method) ((  HashSet_1_t3535795091 * (*) (Enumerator_t3651888341 *, const MethodInfo*))Enumerator_get_CurrentValue_m592783249_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Reflection.MethodInfo,System.Collections.Generic.HashSet`1<System.Object>>::Reset()
#define Enumerator_Reset_m2757715630(__this, method) ((  void (*) (Enumerator_t3651888341 *, const MethodInfo*))Enumerator_Reset_m3001375603_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Reflection.MethodInfo,System.Collections.Generic.HashSet`1<System.Object>>::VerifyState()
#define Enumerator_VerifyState_m2202696759(__this, method) ((  void (*) (Enumerator_t3651888341 *, const MethodInfo*))Enumerator_VerifyState_m4290054460_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Reflection.MethodInfo,System.Collections.Generic.HashSet`1<System.Object>>::VerifyCurrent()
#define Enumerator_VerifyCurrent_m2117580255(__this, method) ((  void (*) (Enumerator_t3651888341 *, const MethodInfo*))Enumerator_VerifyCurrent_m2318603684_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Reflection.MethodInfo,System.Collections.Generic.HashSet`1<System.Object>>::Dispose()
#define Enumerator_Dispose_m2693327870(__this, method) ((  void (*) (Enumerator_t3651888341 *, const MethodInfo*))Enumerator_Dispose_m627360643_gshared)(__this, method)
