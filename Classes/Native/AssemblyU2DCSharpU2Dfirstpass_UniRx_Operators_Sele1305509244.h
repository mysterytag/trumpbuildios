﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UniRx.Operators.SelectManyObservable`3/SelectManyObserverWithIndex<System.Double,System.Double,System.Double>
struct SelectManyObserverWithIndex_t885021286;
// System.IDisposable
struct IDisposable_t1628921374;

#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Oper1921935565.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Operators.SelectManyObservable`3/SelectManyObserverWithIndex/SelectManyObserver<System.Double,System.Double,System.Double>
struct  SelectManyObserver_t1305509244  : public OperatorObserverBase_2_t1921935565
{
public:
	// UniRx.Operators.SelectManyObservable`3/SelectManyObserverWithIndex<TSource,TCollection,TResult> UniRx.Operators.SelectManyObservable`3/SelectManyObserverWithIndex/SelectManyObserver::parent
	SelectManyObserverWithIndex_t885021286 * ___parent_2;
	// TSource UniRx.Operators.SelectManyObservable`3/SelectManyObserverWithIndex/SelectManyObserver::sourceValue
	double ___sourceValue_3;
	// System.Int32 UniRx.Operators.SelectManyObservable`3/SelectManyObserverWithIndex/SelectManyObserver::sourceIndex
	int32_t ___sourceIndex_4;
	// System.IDisposable UniRx.Operators.SelectManyObservable`3/SelectManyObserverWithIndex/SelectManyObserver::cancel
	Il2CppObject * ___cancel_5;
	// System.Int32 UniRx.Operators.SelectManyObservable`3/SelectManyObserverWithIndex/SelectManyObserver::index
	int32_t ___index_6;

public:
	inline static int32_t get_offset_of_parent_2() { return static_cast<int32_t>(offsetof(SelectManyObserver_t1305509244, ___parent_2)); }
	inline SelectManyObserverWithIndex_t885021286 * get_parent_2() const { return ___parent_2; }
	inline SelectManyObserverWithIndex_t885021286 ** get_address_of_parent_2() { return &___parent_2; }
	inline void set_parent_2(SelectManyObserverWithIndex_t885021286 * value)
	{
		___parent_2 = value;
		Il2CppCodeGenWriteBarrier(&___parent_2, value);
	}

	inline static int32_t get_offset_of_sourceValue_3() { return static_cast<int32_t>(offsetof(SelectManyObserver_t1305509244, ___sourceValue_3)); }
	inline double get_sourceValue_3() const { return ___sourceValue_3; }
	inline double* get_address_of_sourceValue_3() { return &___sourceValue_3; }
	inline void set_sourceValue_3(double value)
	{
		___sourceValue_3 = value;
	}

	inline static int32_t get_offset_of_sourceIndex_4() { return static_cast<int32_t>(offsetof(SelectManyObserver_t1305509244, ___sourceIndex_4)); }
	inline int32_t get_sourceIndex_4() const { return ___sourceIndex_4; }
	inline int32_t* get_address_of_sourceIndex_4() { return &___sourceIndex_4; }
	inline void set_sourceIndex_4(int32_t value)
	{
		___sourceIndex_4 = value;
	}

	inline static int32_t get_offset_of_cancel_5() { return static_cast<int32_t>(offsetof(SelectManyObserver_t1305509244, ___cancel_5)); }
	inline Il2CppObject * get_cancel_5() const { return ___cancel_5; }
	inline Il2CppObject ** get_address_of_cancel_5() { return &___cancel_5; }
	inline void set_cancel_5(Il2CppObject * value)
	{
		___cancel_5 = value;
		Il2CppCodeGenWriteBarrier(&___cancel_5, value);
	}

	inline static int32_t get_offset_of_index_6() { return static_cast<int32_t>(offsetof(SelectManyObserver_t1305509244, ___index_6)); }
	inline int32_t get_index_6() const { return ___index_6; }
	inline int32_t* get_address_of_index_6() { return &___index_6; }
	inline void set_index_6(int32_t value)
	{
		___index_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
