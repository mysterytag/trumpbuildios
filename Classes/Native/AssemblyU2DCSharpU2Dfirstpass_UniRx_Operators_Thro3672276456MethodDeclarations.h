﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.ThrowObservable`1<System.Object>
struct ThrowObservable_1_t3672276456;
// System.Exception
struct Exception_t1967233988;
// UniRx.IScheduler
struct IScheduler_t2938318244;
// System.IDisposable
struct IDisposable_t1628921374;
// UniRx.IObserver`1<System.Object>
struct IObserver_1_t3049105323;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Exception1967233988.h"

// System.Void UniRx.Operators.ThrowObservable`1<System.Object>::.ctor(System.Exception,UniRx.IScheduler)
extern "C"  void ThrowObservable_1__ctor_m2390513590_gshared (ThrowObservable_1_t3672276456 * __this, Exception_t1967233988 * ___error0, Il2CppObject * ___scheduler1, const MethodInfo* method);
#define ThrowObservable_1__ctor_m2390513590(__this, ___error0, ___scheduler1, method) ((  void (*) (ThrowObservable_1_t3672276456 *, Exception_t1967233988 *, Il2CppObject *, const MethodInfo*))ThrowObservable_1__ctor_m2390513590_gshared)(__this, ___error0, ___scheduler1, method)
// System.IDisposable UniRx.Operators.ThrowObservable`1<System.Object>::SubscribeCore(UniRx.IObserver`1<T>,System.IDisposable)
extern "C"  Il2CppObject * ThrowObservable_1_SubscribeCore_m3172290664_gshared (ThrowObservable_1_t3672276456 * __this, Il2CppObject* ___observer0, Il2CppObject * ___cancel1, const MethodInfo* method);
#define ThrowObservable_1_SubscribeCore_m3172290664(__this, ___observer0, ___cancel1, method) ((  Il2CppObject * (*) (ThrowObservable_1_t3672276456 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))ThrowObservable_1_SubscribeCore_m3172290664_gshared)(__this, ___observer0, ___cancel1, method)
