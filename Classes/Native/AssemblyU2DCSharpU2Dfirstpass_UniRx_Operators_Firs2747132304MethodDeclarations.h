﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.FirstObservable`1/First<UniRx.Tuple`2<System.Object,System.Object>>
struct First_t2747132304;
// UniRx.Operators.FirstObservable`1<UniRx.Tuple`2<System.Object,System.Object>>
struct FirstObservable_1_t2391816233;
// UniRx.IObserver`1<UniRx.Tuple`2<System.Object,System.Object>>
struct IObserver_1_t2581260722;
// System.IDisposable
struct IDisposable_t1628921374;
// System.Exception
struct Exception_t1967233988;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Tuple_2_gen369261819.h"
#include "mscorlib_System_Exception1967233988.h"

// System.Void UniRx.Operators.FirstObservable`1/First<UniRx.Tuple`2<System.Object,System.Object>>::.ctor(UniRx.Operators.FirstObservable`1<T>,UniRx.IObserver`1<T>,System.IDisposable)
extern "C"  void First__ctor_m243071002_gshared (First_t2747132304 * __this, FirstObservable_1_t2391816233 * ___parent0, Il2CppObject* ___observer1, Il2CppObject * ___cancel2, const MethodInfo* method);
#define First__ctor_m243071002(__this, ___parent0, ___observer1, ___cancel2, method) ((  void (*) (First_t2747132304 *, FirstObservable_1_t2391816233 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))First__ctor_m243071002_gshared)(__this, ___parent0, ___observer1, ___cancel2, method)
// System.Void UniRx.Operators.FirstObservable`1/First<UniRx.Tuple`2<System.Object,System.Object>>::OnNext(T)
extern "C"  void First_OnNext_m1130374583_gshared (First_t2747132304 * __this, Tuple_2_t369261819  ___value0, const MethodInfo* method);
#define First_OnNext_m1130374583(__this, ___value0, method) ((  void (*) (First_t2747132304 *, Tuple_2_t369261819 , const MethodInfo*))First_OnNext_m1130374583_gshared)(__this, ___value0, method)
// System.Void UniRx.Operators.FirstObservable`1/First<UniRx.Tuple`2<System.Object,System.Object>>::OnError(System.Exception)
extern "C"  void First_OnError_m2770858694_gshared (First_t2747132304 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method);
#define First_OnError_m2770858694(__this, ___error0, method) ((  void (*) (First_t2747132304 *, Exception_t1967233988 *, const MethodInfo*))First_OnError_m2770858694_gshared)(__this, ___error0, method)
// System.Void UniRx.Operators.FirstObservable`1/First<UniRx.Tuple`2<System.Object,System.Object>>::OnCompleted()
extern "C"  void First_OnCompleted_m4226293529_gshared (First_t2747132304 * __this, const MethodInfo* method);
#define First_OnCompleted_m4226293529(__this, method) ((  void (*) (First_t2747132304 *, const MethodInfo*))First_OnCompleted_m4226293529_gshared)(__this, method)
