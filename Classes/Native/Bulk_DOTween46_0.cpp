﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

// DG.Tweening.Tweener
struct Tweener_t1766303790;
// UnityEngine.UI.Image
struct Image_t3354615620;
// System.Object
struct Il2CppObject;
// UnityEngine.RectTransform
struct RectTransform_t3317474837;
// DG.Tweening.ShortcutExtensions46/<>c__DisplayClass14_0
struct U3CU3Ec__DisplayClass14_0_t2750076620;
// DG.Tweening.ShortcutExtensions46/<>c__DisplayClass15_0
struct U3CU3Ec__DisplayClass15_0_t2750077581;
// DG.Tweening.ShortcutExtensions46/<>c__DisplayClass4_0
struct U3CU3Ec__DisplayClass4_0_t1751284303;

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array2840145358.h"
#include "DOTween46_U3CModuleU3E86524790.h"
#include "DOTween46_U3CModuleU3E86524790MethodDeclarations.h"
#include "DOTween46_DG_Tweening_ShortcutExtensions462084082962.h"
#include "DOTween46_DG_Tweening_ShortcutExtensions462084082962MethodDeclarations.h"
#include "UnityEngine_UI_UnityEngine_UI_Image3354615620.h"
#include "DOTween_DG_Tweening_Tweener1766303790.h"
#include "mscorlib_System_Single958209021.h"
#include "DOTween46_DG_Tweening_ShortcutExtensions46_U3CU3Ec1751284302MethodDeclarations.h"
#include "DOTween_DG_Tweening_Core_DOGetter_1_gen282336741MethodDeclarations.h"
#include "DOTween_DG_Tweening_Core_DOSetter_1_gen1318976569MethodDeclarations.h"
#include "DOTween_DG_Tweening_DOTween3585775766MethodDeclarations.h"
#include "DOTween_DG_Tweening_TweenSettingsExtensions2047694392MethodDeclarations.h"
#include "DOTween46_DG_Tweening_ShortcutExtensions46_U3CU3Ec1751284302.h"
#include "mscorlib_System_Void2779279689.h"
#include "UnityEngine_UnityEngine_Color1588175760.h"
#include "DOTween_DG_Tweening_Core_DOGetter_1_gen282336741.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_IntPtr676692020.h"
#include "DOTween_DG_Tweening_Core_DOSetter_1_gen1318976569.h"
#include "DOTween_DG_Tweening_TweenSettingsExtensions2047694392.h"
#include "UnityEngine_UnityEngine_RectTransform3317474837.h"
#include "mscorlib_System_Boolean211005341.h"
#include "DOTween46_DG_Tweening_ShortcutExtensions46_U3CU3Ec2750076619MethodDeclarations.h"
#include "DOTween_DG_Tweening_Core_DOGetter_1_gen2219490769MethodDeclarations.h"
#include "DOTween_DG_Tweening_Core_DOSetter_1_gen3256130597MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Vector23525329788MethodDeclarations.h"
#include "DOTween46_DG_Tweening_ShortcutExtensions46_U3CU3Ec2750076619.h"
#include "UnityEngine_UnityEngine_Vector23525329788.h"
#include "DOTween_DG_Tweening_Core_DOGetter_1_gen2219490769.h"
#include "DOTween_DG_Tweening_Core_DOSetter_1_gen3256130597.h"
#include "DOTween_DG_Tweening_Core_TweenerCore_3_gen753146263.h"
#include "DOTween_DG_Tweening_AxisConstraint3652844660.h"
#include "DOTween46_DG_Tweening_ShortcutExtensions46_U3CU3Ec2750077580MethodDeclarations.h"
#include "DOTween46_DG_Tweening_ShortcutExtensions46_U3CU3Ec2750077580.h"
#include "mscorlib_System_Object837106420MethodDeclarations.h"
#include "UnityEngine_UnityEngine_RectTransform3317474837MethodDeclarations.h"
#include "UnityEngine_UI_UnityEngine_UI_Graphic933884113MethodDeclarations.h"

// !!0 DG.Tweening.TweenSettingsExtensions::SetTarget<System.Object>(!!0,System.Object)
extern "C"  Il2CppObject * TweenSettingsExtensions_SetTarget_TisIl2CppObject_m170956001_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * p0, Il2CppObject * p1, const MethodInfo* method);
#define TweenSettingsExtensions_SetTarget_TisIl2CppObject_m170956001(__this /* static, unused */, p0, p1, method) ((  Il2CppObject * (*) (Il2CppObject * /* static, unused */, Il2CppObject *, Il2CppObject *, const MethodInfo*))TweenSettingsExtensions_SetTarget_TisIl2CppObject_m170956001_gshared)(__this /* static, unused */, p0, p1, method)
// !!0 DG.Tweening.TweenSettingsExtensions::SetTarget<DG.Tweening.Tweener>(!!0,System.Object)
#define TweenSettingsExtensions_SetTarget_TisTweener_t1766303790_m3621180943(__this /* static, unused */, p0, p1, method) ((  Tweener_t1766303790 * (*) (Il2CppObject * /* static, unused */, Tweener_t1766303790 *, Il2CppObject *, const MethodInfo*))TweenSettingsExtensions_SetTarget_TisIl2CppObject_m170956001_gshared)(__this /* static, unused */, p0, p1, method)
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// DG.Tweening.Tweener DG.Tweening.ShortcutExtensions46::DOFade(UnityEngine.UI.Image,System.Single,System.Single)
extern Il2CppClass* U3CU3Ec__DisplayClass4_0_t1751284303_il2cpp_TypeInfo_var;
extern Il2CppClass* DOGetter_1_t282336741_il2cpp_TypeInfo_var;
extern Il2CppClass* DOSetter_1_t1318976569_il2cpp_TypeInfo_var;
extern Il2CppClass* DOTween_t3585775766_il2cpp_TypeInfo_var;
extern const MethodInfo* U3CU3Ec__DisplayClass4_0_U3CDOFadeU3Eb__0_m4123970660_MethodInfo_var;
extern const MethodInfo* DOGetter_1__ctor_m602983897_MethodInfo_var;
extern const MethodInfo* U3CU3Ec__DisplayClass4_0_U3CDOFadeU3Eb__1_m1298142362_MethodInfo_var;
extern const MethodInfo* DOSetter_1__ctor_m2393571557_MethodInfo_var;
extern const MethodInfo* TweenSettingsExtensions_SetTarget_TisTweener_t1766303790_m3621180943_MethodInfo_var;
extern const uint32_t ShortcutExtensions46_DOFade_m3953805469_MetadataUsageId;
extern "C"  Tweener_t1766303790 * ShortcutExtensions46_DOFade_m3953805469 (Il2CppObject * __this /* static, unused */, Image_t3354615620 * ___target0, float ___endValue1, float ___duration2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ShortcutExtensions46_DOFade_m3953805469_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	U3CU3Ec__DisplayClass4_0_t1751284303 * V_0 = NULL;
	{
		U3CU3Ec__DisplayClass4_0_t1751284303 * L_0 = (U3CU3Ec__DisplayClass4_0_t1751284303 *)il2cpp_codegen_object_new(U3CU3Ec__DisplayClass4_0_t1751284303_il2cpp_TypeInfo_var);
		U3CU3Ec__DisplayClass4_0__ctor_m3288738114(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CU3Ec__DisplayClass4_0_t1751284303 * L_1 = V_0;
		Image_t3354615620 * L_2 = ___target0;
		NullCheck(L_1);
		L_1->set_target_0(L_2);
		U3CU3Ec__DisplayClass4_0_t1751284303 * L_3 = V_0;
		IntPtr_t L_4;
		L_4.set_m_value_0((void*)(void*)U3CU3Ec__DisplayClass4_0_U3CDOFadeU3Eb__0_m4123970660_MethodInfo_var);
		DOGetter_1_t282336741 * L_5 = (DOGetter_1_t282336741 *)il2cpp_codegen_object_new(DOGetter_1_t282336741_il2cpp_TypeInfo_var);
		DOGetter_1__ctor_m602983897(L_5, L_3, L_4, /*hidden argument*/DOGetter_1__ctor_m602983897_MethodInfo_var);
		U3CU3Ec__DisplayClass4_0_t1751284303 * L_6 = V_0;
		IntPtr_t L_7;
		L_7.set_m_value_0((void*)(void*)U3CU3Ec__DisplayClass4_0_U3CDOFadeU3Eb__1_m1298142362_MethodInfo_var);
		DOSetter_1_t1318976569 * L_8 = (DOSetter_1_t1318976569 *)il2cpp_codegen_object_new(DOSetter_1_t1318976569_il2cpp_TypeInfo_var);
		DOSetter_1__ctor_m2393571557(L_8, L_6, L_7, /*hidden argument*/DOSetter_1__ctor_m2393571557_MethodInfo_var);
		float L_9 = ___endValue1;
		float L_10 = ___duration2;
		IL2CPP_RUNTIME_CLASS_INIT(DOTween_t3585775766_il2cpp_TypeInfo_var);
		Tweener_t1766303790 * L_11 = DOTween_ToAlpha_m3396472119(NULL /*static, unused*/, L_5, L_8, L_9, L_10, /*hidden argument*/NULL);
		U3CU3Ec__DisplayClass4_0_t1751284303 * L_12 = V_0;
		NullCheck(L_12);
		Image_t3354615620 * L_13 = L_12->get_target_0();
		Tweener_t1766303790 * L_14 = TweenSettingsExtensions_SetTarget_TisTweener_t1766303790_m3621180943(NULL /*static, unused*/, L_11, L_13, /*hidden argument*/TweenSettingsExtensions_SetTarget_TisTweener_t1766303790_m3621180943_MethodInfo_var);
		return L_14;
	}
}
// DG.Tweening.Tweener DG.Tweening.ShortcutExtensions46::DOAnchorPosX(UnityEngine.RectTransform,System.Single,System.Single,System.Boolean)
extern Il2CppClass* U3CU3Ec__DisplayClass14_0_t2750076620_il2cpp_TypeInfo_var;
extern Il2CppClass* DOGetter_1_t2219490769_il2cpp_TypeInfo_var;
extern Il2CppClass* DOSetter_1_t3256130597_il2cpp_TypeInfo_var;
extern Il2CppClass* DOTween_t3585775766_il2cpp_TypeInfo_var;
extern const MethodInfo* U3CU3Ec__DisplayClass14_0_U3CDOAnchorPosXU3Eb__0_m3972882208_MethodInfo_var;
extern const MethodInfo* DOGetter_1__ctor_m2194922925_MethodInfo_var;
extern const MethodInfo* U3CU3Ec__DisplayClass14_0_U3CDOAnchorPosXU3Eb__1_m543544308_MethodInfo_var;
extern const MethodInfo* DOSetter_1__ctor_m667778489_MethodInfo_var;
extern const MethodInfo* TweenSettingsExtensions_SetTarget_TisTweener_t1766303790_m3621180943_MethodInfo_var;
extern const uint32_t ShortcutExtensions46_DOAnchorPosX_m1321998970_MetadataUsageId;
extern "C"  Tweener_t1766303790 * ShortcutExtensions46_DOAnchorPosX_m1321998970 (Il2CppObject * __this /* static, unused */, RectTransform_t3317474837 * ___target0, float ___endValue1, float ___duration2, bool ___snapping3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ShortcutExtensions46_DOAnchorPosX_m1321998970_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	U3CU3Ec__DisplayClass14_0_t2750076620 * V_0 = NULL;
	{
		U3CU3Ec__DisplayClass14_0_t2750076620 * L_0 = (U3CU3Ec__DisplayClass14_0_t2750076620 *)il2cpp_codegen_object_new(U3CU3Ec__DisplayClass14_0_t2750076620_il2cpp_TypeInfo_var);
		U3CU3Ec__DisplayClass14_0__ctor_m1067789419(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CU3Ec__DisplayClass14_0_t2750076620 * L_1 = V_0;
		RectTransform_t3317474837 * L_2 = ___target0;
		NullCheck(L_1);
		L_1->set_target_0(L_2);
		U3CU3Ec__DisplayClass14_0_t2750076620 * L_3 = V_0;
		IntPtr_t L_4;
		L_4.set_m_value_0((void*)(void*)U3CU3Ec__DisplayClass14_0_U3CDOAnchorPosXU3Eb__0_m3972882208_MethodInfo_var);
		DOGetter_1_t2219490769 * L_5 = (DOGetter_1_t2219490769 *)il2cpp_codegen_object_new(DOGetter_1_t2219490769_il2cpp_TypeInfo_var);
		DOGetter_1__ctor_m2194922925(L_5, L_3, L_4, /*hidden argument*/DOGetter_1__ctor_m2194922925_MethodInfo_var);
		U3CU3Ec__DisplayClass14_0_t2750076620 * L_6 = V_0;
		IntPtr_t L_7;
		L_7.set_m_value_0((void*)(void*)U3CU3Ec__DisplayClass14_0_U3CDOAnchorPosXU3Eb__1_m543544308_MethodInfo_var);
		DOSetter_1_t3256130597 * L_8 = (DOSetter_1_t3256130597 *)il2cpp_codegen_object_new(DOSetter_1_t3256130597_il2cpp_TypeInfo_var);
		DOSetter_1__ctor_m667778489(L_8, L_6, L_7, /*hidden argument*/DOSetter_1__ctor_m667778489_MethodInfo_var);
		float L_9 = ___endValue1;
		Vector2_t3525329788  L_10;
		memset(&L_10, 0, sizeof(L_10));
		Vector2__ctor_m3620597967(&L_10, L_9, (0.0f), /*hidden argument*/NULL);
		float L_11 = ___duration2;
		IL2CPP_RUNTIME_CLASS_INIT(DOTween_t3585775766_il2cpp_TypeInfo_var);
		TweenerCore_3_t753146263 * L_12 = DOTween_To_m1242219842(NULL /*static, unused*/, L_5, L_8, L_10, L_11, /*hidden argument*/NULL);
		bool L_13 = ___snapping3;
		Tweener_t1766303790 * L_14 = TweenSettingsExtensions_SetOptions_m1757104211(NULL /*static, unused*/, L_12, 2, L_13, /*hidden argument*/NULL);
		U3CU3Ec__DisplayClass14_0_t2750076620 * L_15 = V_0;
		NullCheck(L_15);
		RectTransform_t3317474837 * L_16 = L_15->get_target_0();
		Tweener_t1766303790 * L_17 = TweenSettingsExtensions_SetTarget_TisTweener_t1766303790_m3621180943(NULL /*static, unused*/, L_14, L_16, /*hidden argument*/TweenSettingsExtensions_SetTarget_TisTweener_t1766303790_m3621180943_MethodInfo_var);
		return L_17;
	}
}
// DG.Tweening.Tweener DG.Tweening.ShortcutExtensions46::DOAnchorPosY(UnityEngine.RectTransform,System.Single,System.Single,System.Boolean)
extern Il2CppClass* U3CU3Ec__DisplayClass15_0_t2750077581_il2cpp_TypeInfo_var;
extern Il2CppClass* DOGetter_1_t2219490769_il2cpp_TypeInfo_var;
extern Il2CppClass* DOSetter_1_t3256130597_il2cpp_TypeInfo_var;
extern Il2CppClass* DOTween_t3585775766_il2cpp_TypeInfo_var;
extern const MethodInfo* U3CU3Ec__DisplayClass15_0_U3CDOAnchorPosYU3Eb__0_m3638483200_MethodInfo_var;
extern const MethodInfo* DOGetter_1__ctor_m2194922925_MethodInfo_var;
extern const MethodInfo* U3CU3Ec__DisplayClass15_0_U3CDOAnchorPosYU3Eb__1_m480464404_MethodInfo_var;
extern const MethodInfo* DOSetter_1__ctor_m667778489_MethodInfo_var;
extern const MethodInfo* TweenSettingsExtensions_SetTarget_TisTweener_t1766303790_m3621180943_MethodInfo_var;
extern const uint32_t ShortcutExtensions46_DOAnchorPosY_m838323643_MetadataUsageId;
extern "C"  Tweener_t1766303790 * ShortcutExtensions46_DOAnchorPosY_m838323643 (Il2CppObject * __this /* static, unused */, RectTransform_t3317474837 * ___target0, float ___endValue1, float ___duration2, bool ___snapping3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ShortcutExtensions46_DOAnchorPosY_m838323643_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	U3CU3Ec__DisplayClass15_0_t2750077581 * V_0 = NULL;
	{
		U3CU3Ec__DisplayClass15_0_t2750077581 * L_0 = (U3CU3Ec__DisplayClass15_0_t2750077581 *)il2cpp_codegen_object_new(U3CU3Ec__DisplayClass15_0_t2750077581_il2cpp_TypeInfo_var);
		U3CU3Ec__DisplayClass15_0__ctor_m1196872138(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CU3Ec__DisplayClass15_0_t2750077581 * L_1 = V_0;
		RectTransform_t3317474837 * L_2 = ___target0;
		NullCheck(L_1);
		L_1->set_target_0(L_2);
		U3CU3Ec__DisplayClass15_0_t2750077581 * L_3 = V_0;
		IntPtr_t L_4;
		L_4.set_m_value_0((void*)(void*)U3CU3Ec__DisplayClass15_0_U3CDOAnchorPosYU3Eb__0_m3638483200_MethodInfo_var);
		DOGetter_1_t2219490769 * L_5 = (DOGetter_1_t2219490769 *)il2cpp_codegen_object_new(DOGetter_1_t2219490769_il2cpp_TypeInfo_var);
		DOGetter_1__ctor_m2194922925(L_5, L_3, L_4, /*hidden argument*/DOGetter_1__ctor_m2194922925_MethodInfo_var);
		U3CU3Ec__DisplayClass15_0_t2750077581 * L_6 = V_0;
		IntPtr_t L_7;
		L_7.set_m_value_0((void*)(void*)U3CU3Ec__DisplayClass15_0_U3CDOAnchorPosYU3Eb__1_m480464404_MethodInfo_var);
		DOSetter_1_t3256130597 * L_8 = (DOSetter_1_t3256130597 *)il2cpp_codegen_object_new(DOSetter_1_t3256130597_il2cpp_TypeInfo_var);
		DOSetter_1__ctor_m667778489(L_8, L_6, L_7, /*hidden argument*/DOSetter_1__ctor_m667778489_MethodInfo_var);
		float L_9 = ___endValue1;
		Vector2_t3525329788  L_10;
		memset(&L_10, 0, sizeof(L_10));
		Vector2__ctor_m3620597967(&L_10, (0.0f), L_9, /*hidden argument*/NULL);
		float L_11 = ___duration2;
		IL2CPP_RUNTIME_CLASS_INIT(DOTween_t3585775766_il2cpp_TypeInfo_var);
		TweenerCore_3_t753146263 * L_12 = DOTween_To_m1242219842(NULL /*static, unused*/, L_5, L_8, L_10, L_11, /*hidden argument*/NULL);
		bool L_13 = ___snapping3;
		Tweener_t1766303790 * L_14 = TweenSettingsExtensions_SetOptions_m1757104211(NULL /*static, unused*/, L_12, 4, L_13, /*hidden argument*/NULL);
		U3CU3Ec__DisplayClass15_0_t2750077581 * L_15 = V_0;
		NullCheck(L_15);
		RectTransform_t3317474837 * L_16 = L_15->get_target_0();
		Tweener_t1766303790 * L_17 = TweenSettingsExtensions_SetTarget_TisTweener_t1766303790_m3621180943(NULL /*static, unused*/, L_14, L_16, /*hidden argument*/TweenSettingsExtensions_SetTarget_TisTweener_t1766303790_m3621180943_MethodInfo_var);
		return L_17;
	}
}
// System.Void DG.Tweening.ShortcutExtensions46/<>c__DisplayClass14_0::.ctor()
extern "C"  void U3CU3Ec__DisplayClass14_0__ctor_m1067789419 (U3CU3Ec__DisplayClass14_0_t2750076620 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Vector2 DG.Tweening.ShortcutExtensions46/<>c__DisplayClass14_0::<DOAnchorPosX>b__0()
extern "C"  Vector2_t3525329788  U3CU3Ec__DisplayClass14_0_U3CDOAnchorPosXU3Eb__0_m3972882208 (U3CU3Ec__DisplayClass14_0_t2750076620 * __this, const MethodInfo* method)
{
	{
		RectTransform_t3317474837 * L_0 = __this->get_target_0();
		NullCheck(L_0);
		Vector2_t3525329788  L_1 = RectTransform_get_anchoredPosition_m2546422825(L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Void DG.Tweening.ShortcutExtensions46/<>c__DisplayClass14_0::<DOAnchorPosX>b__1(UnityEngine.Vector2)
extern "C"  void U3CU3Ec__DisplayClass14_0_U3CDOAnchorPosXU3Eb__1_m543544308 (U3CU3Ec__DisplayClass14_0_t2750076620 * __this, Vector2_t3525329788  ___x0, const MethodInfo* method)
{
	{
		RectTransform_t3317474837 * L_0 = __this->get_target_0();
		Vector2_t3525329788  L_1 = ___x0;
		NullCheck(L_0);
		RectTransform_set_anchoredPosition_m1783392546(L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void DG.Tweening.ShortcutExtensions46/<>c__DisplayClass15_0::.ctor()
extern "C"  void U3CU3Ec__DisplayClass15_0__ctor_m1196872138 (U3CU3Ec__DisplayClass15_0_t2750077581 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Vector2 DG.Tweening.ShortcutExtensions46/<>c__DisplayClass15_0::<DOAnchorPosY>b__0()
extern "C"  Vector2_t3525329788  U3CU3Ec__DisplayClass15_0_U3CDOAnchorPosYU3Eb__0_m3638483200 (U3CU3Ec__DisplayClass15_0_t2750077581 * __this, const MethodInfo* method)
{
	{
		RectTransform_t3317474837 * L_0 = __this->get_target_0();
		NullCheck(L_0);
		Vector2_t3525329788  L_1 = RectTransform_get_anchoredPosition_m2546422825(L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Void DG.Tweening.ShortcutExtensions46/<>c__DisplayClass15_0::<DOAnchorPosY>b__1(UnityEngine.Vector2)
extern "C"  void U3CU3Ec__DisplayClass15_0_U3CDOAnchorPosYU3Eb__1_m480464404 (U3CU3Ec__DisplayClass15_0_t2750077581 * __this, Vector2_t3525329788  ___x0, const MethodInfo* method)
{
	{
		RectTransform_t3317474837 * L_0 = __this->get_target_0();
		Vector2_t3525329788  L_1 = ___x0;
		NullCheck(L_0);
		RectTransform_set_anchoredPosition_m1783392546(L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void DG.Tweening.ShortcutExtensions46/<>c__DisplayClass4_0::.ctor()
extern "C"  void U3CU3Ec__DisplayClass4_0__ctor_m3288738114 (U3CU3Ec__DisplayClass4_0_t1751284303 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Color DG.Tweening.ShortcutExtensions46/<>c__DisplayClass4_0::<DOFade>b__0()
extern "C"  Color_t1588175760  U3CU3Ec__DisplayClass4_0_U3CDOFadeU3Eb__0_m4123970660 (U3CU3Ec__DisplayClass4_0_t1751284303 * __this, const MethodInfo* method)
{
	{
		Image_t3354615620 * L_0 = __this->get_target_0();
		NullCheck(L_0);
		Color_t1588175760  L_1 = Graphic_get_color_m1938232411(L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Void DG.Tweening.ShortcutExtensions46/<>c__DisplayClass4_0::<DOFade>b__1(UnityEngine.Color)
extern "C"  void U3CU3Ec__DisplayClass4_0_U3CDOFadeU3Eb__1_m1298142362 (U3CU3Ec__DisplayClass4_0_t1751284303 * __this, Color_t1588175760  ___x0, const MethodInfo* method)
{
	{
		Image_t3354615620 * L_0 = __this->get_target_0();
		Color_t1588175760  L_1 = ___x0;
		NullCheck(L_0);
		Graphic_set_color_m2024155608(L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
