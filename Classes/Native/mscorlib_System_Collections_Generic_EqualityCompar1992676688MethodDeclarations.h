﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.EqualityComparer`1<UniRx.Unit>
struct EqualityComparer_1_t1992676688;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"

// System.Void System.Collections.Generic.EqualityComparer`1<UniRx.Unit>::.ctor()
extern "C"  void EqualityComparer_1__ctor_m683049863_gshared (EqualityComparer_1_t1992676688 * __this, const MethodInfo* method);
#define EqualityComparer_1__ctor_m683049863(__this, method) ((  void (*) (EqualityComparer_1_t1992676688 *, const MethodInfo*))EqualityComparer_1__ctor_m683049863_gshared)(__this, method)
// System.Void System.Collections.Generic.EqualityComparer`1<UniRx.Unit>::.cctor()
extern "C"  void EqualityComparer_1__cctor_m3512580358_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define EqualityComparer_1__cctor_m3512580358(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))EqualityComparer_1__cctor_m3512580358_gshared)(__this /* static, unused */, method)
// System.Int32 System.Collections.Generic.EqualityComparer`1<UniRx.Unit>::System.Collections.IEqualityComparer.GetHashCode(System.Object)
extern "C"  int32_t EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m1252039448_gshared (EqualityComparer_1_t1992676688 * __this, Il2CppObject * ___obj0, const MethodInfo* method);
#define EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m1252039448(__this, ___obj0, method) ((  int32_t (*) (EqualityComparer_1_t1992676688 *, Il2CppObject *, const MethodInfo*))EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m1252039448_gshared)(__this, ___obj0, method)
// System.Boolean System.Collections.Generic.EqualityComparer`1<UniRx.Unit>::System.Collections.IEqualityComparer.Equals(System.Object,System.Object)
extern "C"  bool EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m1288219110_gshared (EqualityComparer_1_t1992676688 * __this, Il2CppObject * ___x0, Il2CppObject * ___y1, const MethodInfo* method);
#define EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m1288219110(__this, ___x0, ___y1, method) ((  bool (*) (EqualityComparer_1_t1992676688 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m1288219110_gshared)(__this, ___x0, ___y1, method)
// System.Collections.Generic.EqualityComparer`1<T> System.Collections.Generic.EqualityComparer`1<UniRx.Unit>::get_Default()
extern "C"  EqualityComparer_1_t1992676688 * EqualityComparer_1_get_Default_m4068636041_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define EqualityComparer_1_get_Default_m4068636041(__this /* static, unused */, method) ((  EqualityComparer_1_t1992676688 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))EqualityComparer_1_get_Default_m4068636041_gshared)(__this /* static, unused */, method)
