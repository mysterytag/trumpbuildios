﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// GameController
struct GameController_t2782302542;
// GlobalStat
struct GlobalStat_t1134678199;
// UnityEngine.UI.Text
struct Text_t3286458198;

#include "UnityEngine_UnityEngine_MonoBehaviour3012272455.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TopPanel
struct  TopPanel_t3379186127  : public MonoBehaviour_t3012272455
{
public:
	// GameController TopPanel::GameController
	GameController_t2782302542 * ___GameController_2;
	// GlobalStat TopPanel::GlobalStat
	GlobalStat_t1134678199 * ___GlobalStat_3;
	// UnityEngine.UI.Text TopPanel::HardCurrency
	Text_t3286458198 * ___HardCurrency_4;

public:
	inline static int32_t get_offset_of_GameController_2() { return static_cast<int32_t>(offsetof(TopPanel_t3379186127, ___GameController_2)); }
	inline GameController_t2782302542 * get_GameController_2() const { return ___GameController_2; }
	inline GameController_t2782302542 ** get_address_of_GameController_2() { return &___GameController_2; }
	inline void set_GameController_2(GameController_t2782302542 * value)
	{
		___GameController_2 = value;
		Il2CppCodeGenWriteBarrier(&___GameController_2, value);
	}

	inline static int32_t get_offset_of_GlobalStat_3() { return static_cast<int32_t>(offsetof(TopPanel_t3379186127, ___GlobalStat_3)); }
	inline GlobalStat_t1134678199 * get_GlobalStat_3() const { return ___GlobalStat_3; }
	inline GlobalStat_t1134678199 ** get_address_of_GlobalStat_3() { return &___GlobalStat_3; }
	inline void set_GlobalStat_3(GlobalStat_t1134678199 * value)
	{
		___GlobalStat_3 = value;
		Il2CppCodeGenWriteBarrier(&___GlobalStat_3, value);
	}

	inline static int32_t get_offset_of_HardCurrency_4() { return static_cast<int32_t>(offsetof(TopPanel_t3379186127, ___HardCurrency_4)); }
	inline Text_t3286458198 * get_HardCurrency_4() const { return ___HardCurrency_4; }
	inline Text_t3286458198 ** get_address_of_HardCurrency_4() { return &___HardCurrency_4; }
	inline void set_HardCurrency_4(Text_t3286458198 * value)
	{
		___HardCurrency_4 = value;
		Il2CppCodeGenWriteBarrier(&___HardCurrency_4, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
