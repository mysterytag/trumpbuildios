﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Zenject.IFactoryBinder`2<System.Object,System.Object>
struct IFactoryBinder_2_t3921329133;
// Zenject.DiContainer
struct DiContainer_t2383114449;
// System.String
struct String_t;
// Zenject.BindingConditionSetter
struct BindingConditionSetter_t259147722;
// System.Func`3<Zenject.DiContainer,System.Object,System.Object>
struct Func_3_t1309472546;
// UnityEngine.GameObject
struct GameObject_t4012695102;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Zenject_DiContainer2383114449.h"
#include "mscorlib_System_String968488902.h"
#include "UnityEngine_UnityEngine_GameObject4012695102.h"

// System.Void Zenject.IFactoryBinder`2<System.Object,System.Object>::.ctor(Zenject.DiContainer,System.String)
extern "C"  void IFactoryBinder_2__ctor_m1276753748_gshared (IFactoryBinder_2_t3921329133 * __this, DiContainer_t2383114449 * ___container0, String_t* ___identifier1, const MethodInfo* method);
#define IFactoryBinder_2__ctor_m1276753748(__this, ___container0, ___identifier1, method) ((  void (*) (IFactoryBinder_2_t3921329133 *, DiContainer_t2383114449 *, String_t*, const MethodInfo*))IFactoryBinder_2__ctor_m1276753748_gshared)(__this, ___container0, ___identifier1, method)
// Zenject.BindingConditionSetter Zenject.IFactoryBinder`2<System.Object,System.Object>::ToMethod(System.Func`3<Zenject.DiContainer,TParam1,TContract>)
extern "C"  BindingConditionSetter_t259147722 * IFactoryBinder_2_ToMethod_m28043641_gshared (IFactoryBinder_2_t3921329133 * __this, Func_3_t1309472546 * ___method0, const MethodInfo* method);
#define IFactoryBinder_2_ToMethod_m28043641(__this, ___method0, method) ((  BindingConditionSetter_t259147722 * (*) (IFactoryBinder_2_t3921329133 *, Func_3_t1309472546 *, const MethodInfo*))IFactoryBinder_2_ToMethod_m28043641_gshared)(__this, ___method0, method)
// Zenject.BindingConditionSetter Zenject.IFactoryBinder`2<System.Object,System.Object>::ToFactory()
extern "C"  BindingConditionSetter_t259147722 * IFactoryBinder_2_ToFactory_m1579464947_gshared (IFactoryBinder_2_t3921329133 * __this, const MethodInfo* method);
#define IFactoryBinder_2_ToFactory_m1579464947(__this, method) ((  BindingConditionSetter_t259147722 * (*) (IFactoryBinder_2_t3921329133 *, const MethodInfo*))IFactoryBinder_2_ToFactory_m1579464947_gshared)(__this, method)
// Zenject.BindingConditionSetter Zenject.IFactoryBinder`2<System.Object,System.Object>::ToPrefab(UnityEngine.GameObject)
extern "C"  BindingConditionSetter_t259147722 * IFactoryBinder_2_ToPrefab_m1058408149_gshared (IFactoryBinder_2_t3921329133 * __this, GameObject_t4012695102 * ___prefab0, const MethodInfo* method);
#define IFactoryBinder_2_ToPrefab_m1058408149(__this, ___prefab0, method) ((  BindingConditionSetter_t259147722 * (*) (IFactoryBinder_2_t3921329133 *, GameObject_t4012695102 *, const MethodInfo*))IFactoryBinder_2_ToPrefab_m1058408149_gshared)(__this, ___prefab0, method)
