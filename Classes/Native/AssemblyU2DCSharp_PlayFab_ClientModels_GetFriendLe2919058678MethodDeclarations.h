﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.ClientModels.GetFriendLeaderboardRequest
struct GetFriendLeaderboardRequest_t2919058678;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String968488902.h"
#include "mscorlib_System_Nullable_1_gen1438485399.h"
#include "mscorlib_System_Nullable_1_gen3097043249.h"

// System.Void PlayFab.ClientModels.GetFriendLeaderboardRequest::.ctor()
extern "C"  void GetFriendLeaderboardRequest__ctor_m222670451 (GetFriendLeaderboardRequest_t2919058678 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PlayFab.ClientModels.GetFriendLeaderboardRequest::get_StatisticName()
extern "C"  String_t* GetFriendLeaderboardRequest_get_StatisticName_m3396472224 (GetFriendLeaderboardRequest_t2919058678 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.GetFriendLeaderboardRequest::set_StatisticName(System.String)
extern "C"  void GetFriendLeaderboardRequest_set_StatisticName_m3834885235 (GetFriendLeaderboardRequest_t2919058678 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 PlayFab.ClientModels.GetFriendLeaderboardRequest::get_StartPosition()
extern "C"  int32_t GetFriendLeaderboardRequest_get_StartPosition_m1841859653 (GetFriendLeaderboardRequest_t2919058678 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.GetFriendLeaderboardRequest::set_StartPosition(System.Int32)
extern "C"  void GetFriendLeaderboardRequest_set_StartPosition_m133527920 (GetFriendLeaderboardRequest_t2919058678 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Nullable`1<System.Int32> PlayFab.ClientModels.GetFriendLeaderboardRequest::get_MaxResultsCount()
extern "C"  Nullable_1_t1438485399  GetFriendLeaderboardRequest_get_MaxResultsCount_m716341782 (GetFriendLeaderboardRequest_t2919058678 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.GetFriendLeaderboardRequest::set_MaxResultsCount(System.Nullable`1<System.Int32>)
extern "C"  void GetFriendLeaderboardRequest_set_MaxResultsCount_m3444547645 (GetFriendLeaderboardRequest_t2919058678 * __this, Nullable_1_t1438485399  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Nullable`1<System.Boolean> PlayFab.ClientModels.GetFriendLeaderboardRequest::get_IncludeSteamFriends()
extern "C"  Nullable_1_t3097043249  GetFriendLeaderboardRequest_get_IncludeSteamFriends_m3244027180 (GetFriendLeaderboardRequest_t2919058678 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.GetFriendLeaderboardRequest::set_IncludeSteamFriends(System.Nullable`1<System.Boolean>)
extern "C"  void GetFriendLeaderboardRequest_set_IncludeSteamFriends_m1071280423 (GetFriendLeaderboardRequest_t2919058678 * __this, Nullable_1_t3097043249  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Nullable`1<System.Boolean> PlayFab.ClientModels.GetFriendLeaderboardRequest::get_IncludeFacebookFriends()
extern "C"  Nullable_1_t3097043249  GetFriendLeaderboardRequest_get_IncludeFacebookFriends_m728108074 (GetFriendLeaderboardRequest_t2919058678 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.GetFriendLeaderboardRequest::set_IncludeFacebookFriends(System.Nullable`1<System.Boolean>)
extern "C"  void GetFriendLeaderboardRequest_set_IncludeFacebookFriends_m3251399859 (GetFriendLeaderboardRequest_t2919058678 * __this, Nullable_1_t3097043249  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
