//
//  VKWrapper.m
//  Unity-iPhone
//
//  Created by rab on 29/09/15.
//
//

#import "VKWrapper.h"
#import "UnityAppController.h"

static VKWrapper *instance;

UIViewController*	UnityGetVController()	{ return ((UnityAppController*)[UIApplication sharedApplication].delegate).rootViewController; }

void SendUnity(const char * method, NSString * text)
{
    UnitySendMessage("callbacks", "Event", [[NSString stringWithFormat: @"%1s~%@", method,text] UTF8String]);
    
}

void VKLogin() { [instance login]; NSLog(@"VKLogin"); }
void VKLogout() { [instance logout]; NSLog(@"VKLogout"); }
bool VKIsLoggedIn() {  NSLog(@"VKIsLoggedIn");  return [instance isLoggedIn]; }
void VKGetFriendList() { [instance getFriendList];}
void VKGetProfile() { [instance getProfile];}
void VKInvite(const char * idd) {[instance inviteFriend:[NSString stringWithCString:idd encoding:NSUTF8StringEncoding]];}
void VKPostAchievement(const char * msg, const char * attach)
{
    [instance postAchievementWithMessage:[NSString stringWithUTF8String:msg] andAttachment:[NSString stringWithUTF8String:attach]];
}
void VKJoinGroup(const char * gId) {
    [instance joinGroup:[NSString stringWithCString:gId encoding:NSUTF8StringEncoding]];
}

@implementation VKWrapper

UIViewController * appViewController = nil;
VKAccessToken * accessToken = nil;

+(void) initAll
{
    instance = [[VKWrapper alloc] init];
}

- (id)init
{
    self = [super init];
    
    instance = self;
    
    [VKSdk initializeWithAppId: @"5557641"];
    [[VKSdk instance] registerDelegate:self];
    [[VKSdk instance] setUiDelegate:self];
    NSArray *SCOPE = @[VK_PER_FRIENDS, VK_PER_WALL, VK_PER_GROUPS];
    
    [VKSdk wakeUpSession:SCOPE completeBlock:^(VKAuthorizationState state, NSError *error) {
        if (state == VKAuthorizationAuthorized) {
            [instance login];
        } else if (error) {
            NSLog(@"vk login error: %@",error);
            // Some error happend, but you may try later
        }
    }];

    NSLog(@"!vkSdkInitialized");
    
    return self;
}

- (bool) isLoggedIn { return [VKSdk isLoggedIn]; }

- (void) login {
    NSLog(@"%@", VK_PER_WALL);
    [VKSdk authorize: @[VK_PER_FRIENDS, VK_PER_WALL, VK_PER_GROUPS] withOptions: VKAuthorizationOptionsUnlimitedToken];
}
- (void) logout {
    [VKSdk forceLogout];
}

//- (BOOL) getUserOrigin() {
//
//}

- (void) setViewController:(UIViewController *)controller {
    appViewController = controller;
}



- (void) vkSdkTokenHasExpired:(VKAccessToken *)expiredToken
{
    NSLog(@"!vkSdkTokenHasExpired");
}

- (void) getFriendList {
    VKRequest *request = [VKRequest requestWithMethod:@"friends.get" andParameters:@{VK_API_FIELDS : @"name, photo_100"}];
    VKRequest *request2 = [VKRequest requestWithMethod:@"friends.getAppUsers" andParameters:@{}];
    
    [request executeWithResultBlock:^(VKResponse *response) {
        SendUnity( "friends", response.responseString  );
    } errorBlock:^(NSError *error) {
        SendUnity("friends", [ NSString stringWithFormat:@"error %ld %@", (long)error.vkError.errorCode, error.vkError.errorMessage] );
    }];
    
    [request2 executeWithResultBlock:^(VKResponse *response) {
        SendUnity("friends_app",  response.responseString  );
    } errorBlock:^(NSError *error) {
        SendUnity( "friends_app",  [NSString stringWithFormat:@"error %ld %@", (long)error.vkError.errorCode, error.vkError.errorMessage] );
    }];
    
    NSLog(@"!vkGetFriendList");
}

- (void) getProfile {
    VKRequest *request = [[VKApi users] get: @{VK_API_FIELDS: @"first_name, last_name, bdate, country, sex"} ];
    [request executeWithResultBlock:^(VKResponse *response) {
        SendUnity( "user_profile",  response.responseString   );
    } errorBlock:^(NSError *error) {
        SendUnity("user_profile",  [NSString stringWithFormat:@"error %ld %@", (long)error.vkError.errorCode, error.vkError.errorMessage]  );
    }];
}

- (void) joinGroup:(NSString *)groupId
{
    VKRequest *request = [VKRequest requestWithMethod:@"groups.join" andParameters:@{VK_API_GROUP_ID : groupId} ];
    
    [request executeWithResultBlock:^(VKResponse *response) {
        SendUnity( "join_group",  response.responseString  );
    } errorBlock:^(NSError *error) {
        SendUnity( "join_group",  [NSString stringWithFormat:@"error %ld %@", (long)error.vkError.errorCode, error.vkError.errorMessage]  );
    }];
}

- (void) postAchievementWithMessage:(NSString *)message andAttachment:(NSString *)attachment
{
    VKRequest *request = [[VKApi wall] post:@{VK_API_MESSAGE : message, VK_API_ATTACHMENTS : attachment}];
    
    [request executeWithResultBlock:^(VKResponse *response) {
        //UnitySendMessage("VKWrapper", "InviteFriendResponse", [response.responseString  UTF8String]);
    } errorBlock:^(NSError *error) {
        //UnitySendMessage("VKWrapper", "InviteFriendResponse", [[NSString stringWithFormat:@"error %ld %@", (long)error.vkError.errorCode, error.vkError.errorMessage] UTF8String]);
    }];
}

- (void) inviteFriend:(NSString *)friendId
{
    VKRequest *request = [VKRequest requestWithMethod:@"apps.sendRequest" andParameters:@{VK_API_USER_ID : friendId, @"type" : @"invite", VK_API_OWNER_ID : @"5557641"}];
    
    [request executeWithResultBlock:^(VKResponse *response) {
        SendUnity( "invite_friend", response.responseString);
    } errorBlock:^(NSError *error) {
        SendUnity( "invite_friend", [NSString stringWithFormat:@"error %ld %@", (long)error.vkError.errorCode, error.vkError.errorMessage]);
    }];
}

- (void) vkSdkAccessAuthorizationFinishedWithResult:(VKAuthorizationResult *)result
{
    accessToken = [result token];
    SendUnity("login", accessToken != NULL ? [[VKSdk accessToken] userId] : @"timeout");
    NSLog(@"!vkSdkReceivedNewToken");
}

- (void) vkSdkAccessTokenUpdated:(VKAccessToken *)newToken oldToken:(VKAccessToken *)oldToken
{
    accessToken = newToken;
    SendUnity("login", accessToken != NULL ? [accessToken userId] : @"timeout");
}

- (void) vkSdkUserAuthorizationFailed
{
    
    SendUnity( "login", @"error");
}

- (void) vkSdkShouldPresentViewController:(UIViewController *)controller
{
    NSLog(@"!vkSdkShouldPresentViewController");
    [[UIApplication sharedApplication].keyWindow.rootViewController presentViewController:controller animated:YES completion:^{
        NSLog(@"vk presented");
    }];
}


- (void) vkSdkNeedCaptchaEnter:(VKError *)captchaError
{
    VKCaptchaViewController * vc = [VKCaptchaViewController captchaControllerWithError:captchaError];
    [vc presentIn:UnityGetVController().navigationController.topViewController];
    NSLog(@"!vkSdkNeedCaptchaEnter");
}

@end

