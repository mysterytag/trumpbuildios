//
//  VKWrapper.h
//  Unity-iPhone
//
//  Created by rab on 29/09/15.
//
//

#import <Foundation/Foundation.h>
#import <VKSdk.h>


@interface VKWrapper : NSObject<VKSdkDelegate, VKSdkUIDelegate>

+ (void) initAll;

- (bool) isLoggedIn;
- (void) login;
- (void) logout;
- (void) getFriendList;
- (void) getProfile;
- (void) joinGroup: (NSString *) gId;
- (void) postAchievementWithMessage: (NSString *) message andAttachment: (NSString *) attachment;
- (void) inviteFriend: (NSString *) friendId;
- (void)vkSdkNeedCaptchaEnter:(VKError *)captchaError;

/**
 Notifies delegate about authorization was completed, and returns authorization result which presents new token or error.
 @param result contains new token or error, retrieved after VK authorization
 */
- (void)vkSdkAccessAuthorizationFinishedWithResult:(VKAuthorizationResult *)result;

/**
 Notifies delegate about access error, mostly connected with user deauthorized application
 */
- (void)vkSdkUserAuthorizationFailed;

/**
 Notifies delegate about access token changed
 @param newToken new token for API requests
 @param oldToken previous used token
 */
- (void)vkSdkAccessTokenUpdated:(VKAccessToken *)newToken oldToken:(VKAccessToken *)oldToken;

/**
 Notifies delegate about existing token has expired
 @param expiredToken old token that has expired
 */
- (void)vkSdkTokenHasExpired:(VKAccessToken *)expiredToken;
/**
 Pass view controller that should be presented to user. Usually, it's an authorization window
 @param controller view controller that must be shown to user
 */
- (void)vkSdkShouldPresentViewController:(UIViewController *)controller;

@end
